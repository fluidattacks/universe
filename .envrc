set -o errexit
set -o nounset

echo ---
export CI_COMMIT_SHA="$(git rev-parse HEAD)"
echo "[INFO] CI_COMMIT_SHA=${CI_COMMIT_SHA}"
export CI_COMMIT_REF_NAME="$(git rev-parse --abbrev-ref HEAD)"
echo "[INFO] CI_COMMIT_REF_NAME=${CI_COMMIT_REF_NAME}"
export CI_COMMIT_MESSAGE="$(git log -1 --pretty=format:%B | tr -d '\n')"
echo "[INFO] CI_COMMIT_MESSAGE=${CI_COMMIT_MESSAGE}"
export CI_DEFAULT_BRANCH="trunk"
echo "[INFO] CI_DEFAULT_BRANCH=${CI_DEFAULT_BRANCH}"
export AWS_DEFAULT_REGION=us-east-1
echo "[INFO] AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION}"

echo "[INFO] Setting git hooksPath to .githooks"
git config core.hooksPath .githooks
echo "[INFO] Setting git commit.template to .git-commit-template"
git config commit.template .git-commit-template

echo "[INFO] Logging in to AWS via Okta using saml2aws"
if ! which saml2aws &>/dev/null; then
  echo "[INFO] saml2aws is not installed. Trying to install it via Nix..."
  nix-env -iA nixpkgs.saml2aws
fi
saml2aws_args=(
  --url https://fluidattacks.okta.com/home/amazon_aws/0oa9ahz3rfx1SpStS357/272
  --idp-provider Okta
  --mfa PUSH
  --profile default
  --session-duration 32400
)
saml2aws login "${saml2aws_args[@]}" --disable-sessions --force
eval "$(saml2aws script "${saml2aws_args[@]}")"

echo ---
echo "[INFO] Select the development environment you want to load:"
echo
echo "Once the environment has finished loading,"
echo "please close your code editor if it is open,"
echo "and then open it by invoking it from this terminal."
echo
echo "You can reload the environment at any moment with: $ direnv allow"
echo
PS3="Selection: "
envs=(
  airs
  common
  forces
  integratesBack
  integratesStreams
  matches
  melts
  skimsSbom
  skims
  sorts
  none
)
select output in "${envs[@]}"; do
  if test "${output}" = "none"; then
    break
  fi
  if test -n "${output}"; then
    m . "/dev/common"
    source "${HOME}/.cache/makes/out-dev-common/template"
    if test "${output}" != common; then
      m . "/dev/${output}"
      source "${HOME}/.cache/makes/out-dev-${output}/template"
    fi
    break
  fi

  echo Unrecognized option, please enter a valid number
done
export PATH="${PATH}:${HOME}/.bin"
