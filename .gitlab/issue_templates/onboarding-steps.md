# Onboarding Steps Checklist for Developers

## Basic Onboarding

---

- [ ] Complete [basic onboarding](https://docs.fluidattacks.com/talent/everyone/onboarding)
      for every talent at Fluid Attacks.
  - [ ] Receive your Fluid Attacks credentials.
  - [ ] Attend the induction meeting.
  - [ ] Configure Okta to access your corporate email.
  - [ ] Login with your Fluid Attacks credentials to your corporate email.
  - [ ] Watch Health & Security videos shared with you.
  - [ ] Setup your email signature.
  - [ ] Send the presentation email.
  - [ ] Define your tentative work schedule.
  - [ ] Register in TimeDoctor using your Fluid Attacks credentials.
  - [ ] Setup the Vanta agent on your work computer (not necessary for MacOS).
  - [ ] Complete the EasyLlama training courses.

---

## Product Onboarding

- [ ] Complete the [product onboarding](https://docs.fluidattacks.com/talent/engineering/onboarding) guide.
  - [ ] Configure your [local machine](https://docs.fluidattacks.com/talent/engineering/onboarding#basic-machine-configuration).
    - [ ] Install Linux.
    - [ ] Run TimeDoctor.
    - [ ] Install Nix.
    - [ ] Install Makes.
    - [ ] Install Git, Direnv, Sops and VSCode.
    - [ ] Create a Gitlab account.
  - [ ] Configure your [Terminal](https://docs.fluidattacks.com/talent/engineering/onboarding#terminal).
    - [ ] Clone the repository using SSH.
    - [ ] Configure Git username and email.
    - [ ] Customize your terminal configuration file for Okta.
    - [ ] Customize your terminal configuration file for Direnv.
    - [ ] Load a development environment
- [ ] Configure your [editor](https://docs.fluidattacks.com/talent/engineering/onboarding#editor)
  - [ ] Install workspace recommended extensions
  - [ ] Test automatic formatting is working correctly
- [ ] Add your username and email to [.mailmap](https://gitlab.com/fluidattacks/universe/-/blob/trunk/.mailmap)

---

## Any comments on this process?

/label ~"onboarding-steps"
