### Onboarding Steps Complement
Create a guide for new developers in which there are internal and external links that serve as support to access the information more precisely

### Test plan
N/A

### Steps

Follow ONLY the initial steps: https://gitlab.com/fluidattacks/universe/-/blob/trunk/.gitlab/issue_templates/onboarding-steps.md

Configure your machine:
- Install Linux: https://www.youtube.com/watch?v=ycTh_x-hzro&t=159s.
- Install timedoctor. Your business email receives the invitation to use timedoctor; After following the steps, it will download automatically.
- Unzip the timedoctor file. https://www.garron.me/es/cortos/dpkg.html.
- Install vscode. https://code.visualstudio.com/download.
- Unzip vscode.
- Install Git in linux command: https://git-scm.com/download/linux.
- Create SSH key: https://docs.gitlab.com/ee/user/ssh.html.
- Clone the repository with SSH.
- Set your name and mail in git local with console. It must already be in the root file of the repose shot://linuxize.com/post/how-to-configure-git-username-and-email/https://linuxize.com/post/how-to-configure-git-username-and-email/
- Make sure the name and email changes are the same in gitlab, in profile.
- Install nix. choose single-user installation: https://nixos.org/download.html.
- Install Makes: https://makes.fluidattacks.tech/getting-started/.
- read the guide to do local test with makes: https://help.fluidattacks.com/portal/en/kb/articles/stack-makes
- Configure aws session acquisition using Okta: https://docs.fluidattacks.com/development/setup/terminal/.
- To access the file .bashrc enter command in console: code ~/.bashrc (the file is by default in home/name).
- Enter this command to run the changes in the file: source ~/.bashrc.
- return to repository address with cd universe and follow the steps that appear on the guide: https://docs.fluidattacks.com/development/setup/terminal/.
- choose the common environment to start.
- Install sops: https://help.fluidattacks.com/portal/en/kb/articles/stack-sops.
- To make sure that the okta configuration works you can do the following:
* Look at this file: https://gitlab.com/fluidattacks/universe/-/blob/trunk/common/secrets/dev.yaml (this file is encrypted by sops tool)
* Enter command: sops common/secrets/dev.yaml.
* If you already have an aws session acquired and you sop the file, you will no longer see it encrypted.
- Configure the mailmap, adding your name to the file. (look for the file in the root folder of the repository as .mailmap)
You have finished configuring your machine.

- You will receive a issue.
- Read and work it out .
- When you have resolved the issue add the file.
- Make a commit following the syntax rules specified in the guide: https://help.fluidattacks.com/portal/en/kb/articles/commitlint-commit.
- To choose the product you must pay attention to the main folder in which the file is.
- When you have the commit ready run: m . /common/test/commitlint (to check if the commit message is valid).
- when you push and the commit passes all the pipeline filters do merge request.
- If a filter doesn't pass, you must read in the box where the X appears what the error is and correct it.

### What does success look like, and how can we measure that?

- [ ] Decide where we want to keep the checklist for developer onboarding (Issue template or docs).
- [ ] Document all the steps.
- [ ] Make sure that you do not repeat documentation. Let's prioritize pointing to external or existing guides.
- [ ] Test the new guideline with a new talent.
- [ ] Get a new round of feedback so we can perfect the guide.

### Links / references
