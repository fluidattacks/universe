import logging
from typing import (
    Any,
)

from src.logger import (
    LOGGER,
)


def test_logger_setup(caplog: Any) -> None:
    caplog.set_level(logging.DEBUG)

    LOGGER.debug("Debug message")
    LOGGER.info("Info message")
    LOGGER.warning("Warning message")
    LOGGER.error("Error message")

    log_records = caplog.records
    assert len(log_records) == 4
