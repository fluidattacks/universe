import os
from unittest.mock import (
    MagicMock,
    patch,
)

from pytest import (
    LogCaptureFixture,
)

from src.cli import (
    API_TOKEN_MSSING_MSG,
    GROUPS_DIR_MISSING_MSG,
    main,
)
from test.conftest import (
    SyncCliRunner,
)


def test_main_no_api_token(caplog: LogCaptureFixture, runner: SyncCliRunner) -> None:
    with patch("src.cli.constants.API_TOKEN", None):
        result = runner.invoke(
            main,
            ["pull-repos", "--help"],
            env={k: v for k, v in os.environ.items() if k != "INTEGRATES_API_TOKEN"},
        )
        assert result.exit_code == 1
        assert any(
            record.message == API_TOKEN_MSSING_MSG and record.levelname == "ERROR"
            for record in caplog.records
        )


def test_main_not_groups(
    caplog: LogCaptureFixture, runner: SyncCliRunner, clean_working_dir: str
) -> None:
    result = runner.invoke(main, ["clone", "--group", "group_name"])
    expected_message_1 = GROUPS_DIR_MISSING_MSG % (f"{os.path.sep}private{clean_working_dir}")
    expected_message_2 = GROUPS_DIR_MISSING_MSG % clean_working_dir

    assert result.exit_code == 1
    assert any(
        record.message
        in (
            expected_message_1,
            expected_message_2,
        )
        and record.levelname == "ERROR"
        for record in caplog.records
    )


@patch("aiohttp.client.ClientSession.post", new_callable=MagicMock)
@patch("os.listdir", return_value=["file1", "file2", "file3"])
@patch("os.getcwd", return_value="/mock_directory/groups/project")
def test_main_groups(
    mock_os_getcwd: MagicMock,
    mock_os_listdir: MagicMock,
    mock_client_post: MagicMock,
    runner: SyncCliRunner,
) -> None:
    mock_client_post.return_value.__aenter__.return_value.status = 200
    mock_client_post.return_value.__aenter__.return_value.json.return_value = {
        "data": {
            "group": {
                "roots": [
                    {"id": "1", "branch": "master", "nickname": "root1"},
                    {"id": "2", "branch": "dev", "nickname": "root2"},
                ]
            }
        }
    }

    runner.invoke(main, ["--init", "clone", "--group", "group_name"])
    assert mock_os_getcwd.call_count == 1
    assert mock_os_listdir.call_count == 1
