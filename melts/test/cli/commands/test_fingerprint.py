from unittest.mock import (
    MagicMock,
    patch,
)

from src.cli.commands.fingerprint import (
    get_fingerprint,
)
from test.conftest import (
    SyncCliRunner,
)


def test_get_fingerprint(runner: SyncCliRunner) -> None:
    result = runner.invoke(
        get_fingerprint,
        ["--group", "group_name", "--root", "git_root_nickname"],
    )
    assert not result.exception
    assert result.output == ""


@patch("git.repo.Repo")
@patch("os.path.exists")
@patch("os.listdir")
def test_get_fingerprint_true(
    mock_os_listdir: MagicMock,
    mock_os_path_exists: MagicMock,
    mock_repo: MagicMock,
    runner: SyncCliRunner,
) -> None:
    mock_os_path_exists.return_value = True
    mock_os_listdir.return_value = ["repo1", "repo2"]

    mock_git_repo = MagicMock()
    mock_git_repo.head.commit.hexsha = "abc123"
    mock_repo.return_value = mock_git_repo

    result = runner.invoke(
        get_fingerprint,
        ["--group", "group_name", "--root", "git_root_nickname"],
    )

    assert not result.exception
    assert result.output == ""


@patch("git.repo.Repo")
@patch("os.path.exists")
@patch("os.listdir")
@patch("os.path.isdir")
def test_get_fingerprint_not_folder(
    mock_os_isdir: MagicMock,
    mock_os_listdir: MagicMock,
    mock_os_path_exists: MagicMock,
    mock_repo: MagicMock,
    runner: SyncCliRunner,
) -> None:
    mock_os_path_exists.return_value = True
    mock_os_isdir.return_value = True
    mock_os_listdir.return_value = ["repo1", "repo2"]

    mock_git_repo = MagicMock()
    mock_git_repo.head.commit.hexsha = "abc123"
    mock_repo.return_value = mock_git_repo

    result = runner.invoke(
        get_fingerprint,
        ["--group", "group_name"],
    )

    assert not result.exception
    assert result.output == ""
