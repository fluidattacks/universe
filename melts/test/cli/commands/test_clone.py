from unittest.mock import (
    ANY,
    AsyncMock,
    MagicMock,
    patch,
)

import pytest
from aioextensions import (
    collect,
)

from src.cli.commands.clone import (
    _clone_repo,
    clone,
)
from src.utils import (
    GitRootNicknameNotFound,
    RepositoryCredentialsNotFound,
)
from test.conftest import (
    SyncCliRunner,
)


@patch("aiohttp.client.ClientSession.post", new_callable=MagicMock)
@patch("src.cli.commands.clone.LOGGER.error", new_callable=MagicMock)
@pytest.mark.asyncio
async def test_clone_repo_false(mock_logger_error: MagicMock, mock_client_post: MagicMock) -> None:
    mock_client_post.return_value.__aenter__.return_value.status = 200
    mock_client_post.return_value.__aenter__.return_value.json.return_value = {
        "data": {
            "root": {
                "id": "root_id",
                "nickname": "root_nickname",
                "credentials": None,
            }
        }
    }
    group_name = "example_group"
    git_root_id = "1"
    git_root_nickname = "root1"
    repo_url = "https://fake-repo-url.com"
    repo_branch = "main"

    with pytest.raises(RepositoryCredentialsNotFound):
        await _clone_repo(group_name, git_root_id, git_root_nickname, repo_url, repo_branch)

    mock_logger_error.assert_called_once_with("no credentials found for %s", ANY)


@patch("aiohttp.client.ClientSession.post", new_callable=MagicMock)
@patch("src.cli.commands.clone.LOGGER.error", new_callable=MagicMock)
@pytest.mark.asyncio
async def test_clone_repo(mock_logger_error: MagicMock, mock_client_post: MagicMock) -> None:
    mock_client_post.return_value.__aenter__.return_value.status = 200
    mock_client_post.return_value.__aenter__.return_value.json.return_value = {
        "data": {
            "root": {
                "id": "root_id",
                "nickname": "root_nickname",
                "credentials": {
                    "oauthType": "oauth",
                    "user": "user123",
                    "password": "password123",
                    "isPat": False,
                    "key": None,
                    "token": "token123",
                    "type": "type123",
                },
            }
        }
    }
    group_name = "example_group"
    git_root_id = "1"
    git_root_nickname = "root1"
    repo_url = "https://fake-repo-url.com"
    repo_branch = "main"

    await _clone_repo(group_name, git_root_id, git_root_nickname, repo_url, repo_branch)

    mock_logger_error.assert_called_once_with("failed to clone: %s", ANY)


@patch("aiohttp.client.ClientSession.post", new_callable=MagicMock)
@patch("src.cli.commands.clone._clone_repo", new_callable=AsyncMock)
def test_clone(
    mock_clone_repo: AsyncMock,
    mock_client_post: MagicMock,
    runner: SyncCliRunner,
) -> None:
    mock_client_post.return_value.__aenter__.return_value.status = 200
    mock_client_post.return_value.__aenter__.return_value.json.return_value = {
        "data": {
            "group": {
                "gitRoots": {
                    "edges": [
                        {
                            "node": {
                                "id": "123456",
                                "branch": "dev",
                                "nickname": "root1",
                                "state": "ACTIVE",
                                "gitignore": ["*.log"],
                                "url": "https://example.com/_git/repo",
                                "useEgress": False,
                                "useVpn": False,
                                "useZtna": False,
                            }
                        },
                        {
                            "node": {
                                "id": "456789",
                                "branch": "dev",
                                "nickname": "root2",
                                "state": "ACTIVE",
                                "gitignore": ["*.log"],
                                "url": "https://example.com/_git/repo",
                                "useEgress": False,
                                "useVpn": False,
                                "useZtna": True,
                            }
                        },
                    ],
                    "pageInfo": {
                        "hasNextPage": False,
                        "endCursor": "ROOT#456789#GROUP#example_group",
                    },
                },
            },
        }
    }
    result = runner.invoke(clone, ["--group", "group_name"])
    mock_clone_repo.assert_called()
    assert mock_clone_repo.call_count == 2
    assert not result.exception
    assert result.output == ""


@patch("aiohttp.client.ClientSession.post", new_callable=MagicMock)
@patch("src.cli.commands.clone._clone_repo", new_callable=AsyncMock)
def test_clone_single_root(
    mock_clone_repo: AsyncMock,
    mock_client_post: MagicMock,
    runner: SyncCliRunner,
) -> None:
    mock_client_post.return_value.__aenter__.return_value.status = 200
    mock_client_post.return_value.__aenter__.return_value.json.return_value = {
        "data": {
            "group": {
                "gitRoots": {
                    "edges": [
                        {
                            "node": {
                                "id": "123456",
                                "branch": "dev",
                                "nickname": "test",
                                "state": "ACTIVE",
                                "gitignore": ["*.log"],
                                "url": "https://example.com/_git/repo",
                                "useEgress": False,
                                "useVpn": False,
                                "useZtna": False,
                            }
                        },
                        {
                            "node": {
                                "id": "456789",
                                "branch": "dev",
                                "nickname": "test-for-qa",
                                "state": "ACTIVE",
                                "gitignore": ["*.log"],
                                "url": "https://example.com/_git/repo_2",
                                "useEgress": True,
                                "useVpn": False,
                                "useZtna": False,
                            }
                        },
                    ],
                    "pageInfo": {
                        "hasNextPage": False,
                        "endCursor": "ROOT#123456#GROUP#example_group",
                    },
                },
            },
        }
    }
    result = runner.invoke(clone, ["--group", "group_name", "--root", "test"])
    mock_clone_repo.assert_called()
    assert mock_clone_repo.call_count == 1
    assert not result.exception
    assert result.output == ""


@patch("aiohttp.client.ClientSession.post", new_callable=MagicMock)
@patch("src.cli.commands.clone.LOGGER.error", new_callable=MagicMock)
@pytest.mark.asyncio
async def test_clone_repo_multiple(
    mock_logger_error: MagicMock, mock_client_post: MagicMock
) -> None:
    mock_client_post.return_value.__aenter__.return_value.status = 200
    mock_client_post.return_value.__aenter__.return_value.json.return_value = {
        "data": {
            "root": {
                "id": "root_id",
                "nickname": "root_nickname",
                "credentials": {
                    "oauthType": "oauth",
                    "user": "user123",
                    "password": "password123",
                    "isPat": False,
                    "key": None,
                    "token": "token123",
                    "type": "type123",
                },
            }
        }
    }
    git_roots = [
        {
            "id": "123456",
            "branch": "dev",
            "nickname": "test",
            "state": "ACTIVE",
            "gitignore": ["*.log"],
            "url": "https://example.com/_git/repo",
        },
        {
            "id": "123456",
            "branch": "dev",
            "nickname": "test2",
            "state": "ACTIVE",
            "gitignore": ["*.log"],
            "url": "https://example.com/_git/repo_2",
        },
        {
            "id": "123456",
            "branch": "dev",
            "nickname": "test3",
            "state": "ACTIVE",
            "gitignore": ["*.log"],
            "url": "https://example.com/_git/repo_3",
        },
    ]
    group_name = "test"

    await collect(
        [
            _clone_repo(
                group_name,
                str(root["id"]),
                str(root["nickname"]),
                str(root["url"]),
                str(root["branch"]),
            )
            for root in git_roots
        ],
        workers=1,
    )

    assert mock_logger_error.call_count == 3


@patch("aiohttp.client.ClientSession.post", new_callable=MagicMock)
@patch("src.cli.commands.clone._clone_repo", new_callable=AsyncMock)
def test_clone_git_root_nickname_not_found(
    _mock_clone_repo: AsyncMock,
    mock_client_post: MagicMock,
    runner: SyncCliRunner,
) -> None:
    mock_client_post.return_value.__aenter__.return_value.status = 200
    mock_client_post.return_value.__aenter__.return_value.json.return_value = {
        "data": {
            "group": {
                "gitRoots": {
                    "edges": [
                        {
                            "node": {
                                "id": "123456",
                                "branch": "dev",
                                "nickname": "test",
                                "state": "ACTIVE",
                                "gitignore": ["*.log"],
                                "url": "https://example.com/_git/repo",
                            }
                        },
                        {
                            "node": {
                                "id": "456789",
                                "branch": "dev",
                                "nickname": "test-for-qa",
                                "state": "ACTIVE",
                                "gitignore": ["*.log"],
                                "url": "https://example.com/_git/repo_2",
                            }
                        },
                    ],
                    "pageInfo": {
                        "hasNextPage": False,
                        "endCursor": "ROOT#123456#GROUP#example_group",
                    },
                },
            },
        }
    }
    result = runner.invoke(clone, ["--group", "group_name", "--root", "fakeroot"])
    assert str(result.exception) == str(GitRootNicknameNotFound("fakeroot"))
