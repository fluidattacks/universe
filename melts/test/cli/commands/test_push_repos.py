from typing import (
    Any,
)
from unittest.mock import (
    ANY,
    AsyncMock,
    patch,
)

from freezegun.api import (
    freeze_time,
)
from pytest import (
    LogCaptureFixture,
)

from src.cli.commands.push_repos import (
    push_repos,
)
from test.conftest import (
    SyncCliRunner,
)

MOCK_API_RESPONSES: Any = [
    {
        "data": {
            "group": {
                "gitRoots": {
                    "edges": [
                        {
                            "node": {
                                "branch": "master",
                                "cloningStatus": {
                                    "commit": "12345",
                                    "modifiedDate": "2024-04-01T10:00:00+0000",
                                },
                                "gitignore": [],
                                "id": "123456",
                                "nickname": "example",
                                "state": "ACTIVE",
                                "url": "https://example.git",
                            }
                        },
                    ],
                    "pageInfo": {
                        "hasNextPage": False,
                        "endCursor": "ROOT#123456#GROUP#example_group",
                    },
                }
            }
        }
    },
    {
        "data": {
            "root": {
                "uploadUrl": "https://upload_url",
                "cloningStatus": {
                    "hasMirrorInS3": True,
                },
            }
        }
    },
]


def _no_upload(caplog: LogCaptureFixture, runner: SyncCliRunner, commit: str) -> bool:
    group_name = "example"

    with (
        patch(
            "src.api.integrates.aiohttp.ClientSession.post",
        ) as post_mock,
        patch(
            "src.cli.commands.push_repos.Repo",
        ) as repo_mock,
        patch(
            "src.cli.commands.push_repos.update_git_root_cloning_status",
            new=AsyncMock(),
        ) as update_mock,
    ):
        post_mock.return_value.__aenter__.return_value.json.side_effect = MOCK_API_RESPONSES
        post_mock.return_value.__aenter__.return_value.status = 200

        repo_mock.return_value.head.commit.authored_date = 1709287200
        repo_mock.return_value.head.commit.hexsha = commit

        result = runner.invoke(push_repos, ["--group", group_name])
        expected_log = (
            "Repository example was not uploaded "
            "since no new changes were detected "
            "and the last upload was less than 3 days ago"
        )

        assert result.exit_code == 0
        assert any(record.message == expected_log for record in caplog.records)
        update_mock.assert_awaited_once_with(
            group_name=group_name,
            git_root_id=MOCK_API_RESPONSES[0]["data"]["group"]["gitRoots"]["edges"][0]["node"][
                "id"
            ],
            status="OK",
            commit=commit,
            commit_date=ANY,
            message=("[Melts] The repository was not cloned as no new changes were detected"),
        )

    return True


def _no_upload_url(caplog: LogCaptureFixture, runner: SyncCliRunner) -> bool:
    with patch(
        "src.api.integrates.aiohttp.ClientSession.post",
    ) as post_mock:
        post_mock.return_value.__aenter__.return_value.json.side_effect = [
            MOCK_API_RESPONSES[0],
            {
                "data": {
                    "root": {
                        "uploadUrl": "",
                        "cloningStatus": {
                            "hasMirrorInS3": True,
                        },
                    }
                }
            },
        ]
        post_mock.return_value.__aenter__.return_value.status = 200

        result = runner.invoke(push_repos, ["--group", "example"])
        assert result.exit_code == 0
        expected_log = "Could not get presigned URL to upload repository to S3"
        assert any(record.message == expected_log for record in caplog.records)

    return True


def _successful_upload(
    caplog: LogCaptureFixture,
    runner: SyncCliRunner,
    commit: str,
    force: bool = False,
    has_mirror_in_s3: bool = True,
) -> bool:
    group_name = "example"

    with (
        patch(
            "src.api.integrates.aiohttp.ClientSession.post",
        ) as post_mock,
        patch(
            "src.cli.commands.push_repos.aiohttp.ClientSession.put",
            new=AsyncMock(),
        ) as put_mock,
        patch(
            "src.cli.commands.push_repos.Repo",
        ) as repo_mock,
        patch(
            "src.cli.commands.push_repos.update_git_root_cloning_status",
            new=AsyncMock(),
        ) as update_mock,
    ):
        post_mock.return_value.__aenter__.return_value.json.side_effect = [
            MOCK_API_RESPONSES[0],
            {
                "data": {
                    "root": {
                        "uploadUrl": (MOCK_API_RESPONSES[1]["data"]["root"]["uploadUrl"]),
                        "cloningStatus": {
                            "hasMirrorInS3": has_mirror_in_s3,
                        },
                    }
                },
            },
        ]
        post_mock.return_value.__aenter__.return_value.status = 200

        put_mock.return_value.status = 200

        repo_mock.return_value.head.commit.authored_date = 1709287200
        repo_mock.return_value.head.commit.hexsha = commit

        args = ["--group", group_name]
        if force:
            args.append("--force")
        result = runner.invoke(push_repos, args)

        assert result.exit_code == 0
        assert any(
            record.message == "Repository example uploaded successfully"
            for record in caplog.records
        )
        update_mock.assert_awaited_once_with(
            group_name=group_name,
            git_root_id=MOCK_API_RESPONSES[0]["data"]["group"]["gitRoots"]["edges"][0]["node"][
                "id"
            ],
            status="OK",
            commit=commit,
            commit_date=ANY,
            message="[Melts] Cloned successfully",
        )

    return True


def test_push_repos_new_commit(
    caplog: LogCaptureFixture, groups_working_dir: str, runner: SyncCliRunner
) -> None:
    assert groups_working_dir

    commit = "67890"
    assert _successful_upload(caplog, runner, commit)


@freeze_time("2024-04-03")
def test_push_repos_same_commit_recent_upload(
    caplog: LogCaptureFixture, groups_working_dir: str, runner: SyncCliRunner
) -> None:
    assert groups_working_dir

    commit = MOCK_API_RESPONSES[0]["data"]["group"]["gitRoots"]["edges"][0]["node"][
        "cloningStatus"
    ]["commit"]
    assert _no_upload(caplog, runner, commit)


@freeze_time("2024-04-03")
def test_push_repos_diff_commit_recent_upload(
    caplog: LogCaptureFixture, groups_working_dir: str, runner: SyncCliRunner
) -> None:
    assert groups_working_dir

    commit = "67890"
    assert _successful_upload(caplog, runner, commit)


@freeze_time("2024-04-03")
def test_push_repos_same_commit_recent_upload_force(
    caplog: LogCaptureFixture, groups_working_dir: str, runner: SyncCliRunner
) -> None:
    assert groups_working_dir

    commit = MOCK_API_RESPONSES[0]["data"]["group"]["gitRoots"]["edges"][0]["node"][
        "cloningStatus"
    ]["commit"]
    assert _successful_upload(caplog, runner, commit, force=True)


@freeze_time("2024-04-03")
def test_push_repos_same_commit_no_mirror_in_s3(
    caplog: LogCaptureFixture, groups_working_dir: str, runner: SyncCliRunner
) -> None:
    assert groups_working_dir

    commit = MOCK_API_RESPONSES[0]["data"]["group"]["gitRoots"]["edges"][0]["node"][
        "cloningStatus"
    ]["commit"]
    assert _successful_upload(caplog, runner, commit, has_mirror_in_s3=False)


def test_push_repos_no_upload_url(
    caplog: LogCaptureFixture, groups_working_dir: str, runner: SyncCliRunner
) -> None:
    assert groups_working_dir
    assert _no_upload_url(caplog, runner)
