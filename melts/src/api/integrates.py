import asyncio
from datetime import (
    UTC,
    datetime,
)
from typing import (
    Any,
)

import aiohttp

from src.constants import (
    API_TOKEN,
)
from src.logger import (
    LOGGER,
)
from src.utils.exceptions import (
    InvalidIntegratesAPIToken,
)

INTEGRATES_API_URL = "https://app.fluidattacks.com/api"


class RequestError(Exception):
    pass


async def make_graphql_request(
    url: str, query: str, variables: dict, max_retries: int = 3
) -> dict[str, Any]:
    retry_count = 0
    while retry_count < max_retries:
        try:
            async with aiohttp.ClientSession(
                headers={"Authorization": f"Bearer {API_TOKEN}"}
            ) as session:
                payload = {"query": query, "variables": variables}

                async with session.post(url, json=payload) as response:
                    if response.status == 429:
                        retry_after = int(response.headers["Retry-After"])
                        LOGGER.debug(
                            "Rate limit exceeded. Retrying after %s seconds.",
                            retry_after,
                        )
                        await asyncio.sleep(retry_after)
                        continue

                    data = await response.json()

                    if data.get("errors"):
                        msg_error = data["errors"][0]["message"]
                        if msg_error == "Login required":
                            LOGGER.error(
                                "Login required - Integrates API token is expired or broken"
                            )
                            raise InvalidIntegratesAPIToken

                    return data
        except (TimeoutError, aiohttp.ClientError) as exc:
            retry_count += 1
            LOGGER.info("Retry #%s due to network error.", retry_count)
            LOGGER.error("%s", exc)

    raise RequestError(f"Failed to make GraphQL request after {max_retries} retries.")


async def get_group_git_roots(group_names: str) -> list[dict[str, Any]]:
    query = """
            query MeltsGetGitRoots($groupName: String!) {
              group(groupName: $groupName){
                roots {
                  ...on GitRoot{
                    branch
                    cloningStatus {
                        commit
                        modifiedDate
                    }
                    gitignore
                    id
                    nickname
                    state
                    url
                  }
                }
              }
            }
        """

    variables = {"groupName": group_names}
    data = await make_graphql_request(INTEGRATES_API_URL, query, variables)
    if not data["data"]["group"]:
        LOGGER.warning("The group %s does not exist", group_names)
        return []

    return data["data"]["group"]["roots"]


async def get_group_git_roots_paginated(
    group_name: str,
) -> list[dict[str, Any]]:
    query = """
            query MeltsGetGitRoots($groupName: String!, $after: String!) {
                group(groupName: $groupName) {
                    gitRoots(after: $after){
                        edges {
                            node {
                            branch
                            cloningStatus {
                                commit
                                modifiedDate
                            }
                            gitignore
                            id
                            nickname
                            state
                            url
                            useEgress
                            useVpn
                            useZtna
                            }
                        }
                        pageInfo {
                            hasNextPage
                            endCursor
                        }
                    }
                }
            }
        """
    roots = []
    after = ""
    has_next_page = True
    while has_next_page:
        variables = {"groupName": group_name, "after": after}
        data = await make_graphql_request(INTEGRATES_API_URL, query, variables)
        if not data["data"]:
            break

        roots += [item["node"] for item in data["data"]["group"]["gitRoots"]["edges"]]
        has_next_page = data["data"]["group"]["gitRoots"]["pageInfo"]["hasNextPage"]
        after = data["data"]["group"]["gitRoots"]["pageInfo"]["endCursor"]

    if not roots:
        LOGGER.warning(
            "The group %s does not exist or it does not have any Git Roots",
            group_name,
        )

    return roots


async def get_git_root_download_url(group_name: str, git_root_id: str) -> str | None:
    query = """
            query MeltsGetGitRootDownloadUrl($groupName: String!, $rootId: ID!)
            {
              root(groupName: $groupName, rootId: $rootId) {
                ... on GitRoot {
                  id
                  nickname
                  downloadUrl
                }
              }
            }
        """
    params: dict = {"groupName": group_name, "rootId": git_root_id}

    data = await make_graphql_request(INTEGRATES_API_URL, query, params)
    if not data["data"]["root"]:
        LOGGER.warning("The root %s does not exist", git_root_id)
        return None

    return data["data"]["root"]["downloadUrl"]


async def get_git_root_credentials(group_name: str, git_root_id: str) -> dict[str, Any] | None:
    query = """
            query MeltsGetGitRootCredentials($groupName: String!, $rootId: ID!)
            {
              root(groupName: $groupName, rootId: $rootId) {
                ... on GitRoot {
                  id
                  nickname
                  credentials {
                    oauthType
                    user
                    password
                    isPat
                    key
                    token
                    type
                    arn
                    organization {
                        awsExternalId
                    }
                  }
                }
              }
            }
        """
    params: dict = {"groupName": group_name, "rootId": git_root_id}

    data = await make_graphql_request(INTEGRATES_API_URL, query, params)
    if not data["data"]["root"]:
        LOGGER.warning("The root %s does not exist", git_root_id)
        return None

    return data["data"]["root"]["credentials"]


async def get_git_root_upload_url(
    group_name: str, git_root_id: str
) -> tuple[str | None, bool | None]:
    query = """
            query MeltsGetGitRootUploadUrl($groupName: String!, $rootId: ID!) {
              root(groupName: $groupName, rootId: $rootId) {
                ... on GitRoot {
                  uploadUrl
                  cloningStatus {
                    hasMirrorInS3
                  }
                }
              }
            }
        """
    params: dict = {"groupName": group_name, "rootId": git_root_id}

    data = await make_graphql_request(INTEGRATES_API_URL, query, params)
    if not data["data"]["root"]:
        LOGGER.warning("The root %s does not exist", git_root_id)
        return None, None

    return (
        data["data"]["root"]["uploadUrl"],
        data["data"]["root"]["cloningStatus"]["hasMirrorInS3"],
    )


async def update_git_root_cloning_status(
    *,
    group_name: str,
    git_root_id: str,
    status: str,
    commit: str | None = None,
    commit_date: datetime | None = None,
    message: str | None = None,
) -> None:
    query = """
            mutation MeltsUpdateRootCloningStatus(
                $groupName: String!
                $rootId: ID!
                $status: CloningStatus!
                $message: String!
                $commit: String
                $commitDate: DateTime
            ) {
              updateRootCloningStatus(
                groupName: $groupName
                id: $rootId
                status: $status
                message: $message
                commit: $commit
                commitDate: $commitDate
              ) {
                success
              }
            }
        """
    params = {
        "groupName": group_name,
        "rootId": git_root_id,
        "status": status,
        "message": message,
        "commit": commit,
        "commitDate": commit_date.astimezone(tz=UTC).isoformat() if commit_date else None,
    }
    await make_graphql_request(INTEGRATES_API_URL, query, params)


async def get_group_git_root(group_name: str, git_root_nickname: str) -> dict | None:
    query = """
            query MeltsGetGitRoot($groupName: String!, $rootNickname: String!)
            {
                group(groupName: $groupName) {
                    gitRoots(nickname: $rootNickname){
                        edges {
                            node {
                                branch
                                cloningStatus {
                                    commit
                                    modifiedDate
                                }
                                gitignore
                                id
                                nickname
                                state
                                url
                                downloadUrl
                                useEgress
                                useVpn
                                useZtna
                            }
                        }
                    }
                }
            }
        """
    params: dict = {"groupName": group_name, "rootNickname": git_root_nickname}
    data = await make_graphql_request(INTEGRATES_API_URL, query, params)
    if not data["data"] or not (roots_nodes := data["data"]["group"]["gitRoots"]["edges"]):
        LOGGER.warning(
            "The group %s does not have a git root with nickname %s",
            group_name,
            git_root_nickname,
        )
        return None

    return next(
        (root["node"] for root in roots_nodes if root["node"]["nickname"] == git_root_nickname),
        None,
    )


async def get_group_git_root_env_apks(group_name: str, git_root_nickname: str) -> set[str] | None:
    query = """
            query MeltsGetGitRootEnvUrls(
                $groupName: String!, $rootNickname: String!
            ) {
                group(groupName: $groupName) {
                    gitRoots(nickname: $rootNickname){
                        edges {
                            node {
                                gitEnvironmentUrls {
                                    url
                                    id
                                    include
                                    urlType
                                }
                                id
                                nickname
                            }
                        }
                    }
                }
            }
        """
    params: dict = {"groupName": group_name, "rootNickname": git_root_nickname}
    data = await make_graphql_request(INTEGRATES_API_URL, query, params)
    if (
        not data["data"]
        or not (roots_nodes := data["data"]["group"]["gitRoots"]["edges"])
        or not (
            git_root := next(
                (
                    root["node"]
                    for root in roots_nodes
                    if root["node"]["nickname"] == git_root_nickname
                ),
                None,
            )
        )
    ):
        LOGGER.warning(
            "The group %s does not have a git root with nickname %s",
            group_name,
            git_root_nickname,
        )
        return None

    apk_file_names = {
        environment_url["url"]
        for environment_url in git_root["gitEnvironmentUrls"]
        if environment_url["urlType"] == "APK" and environment_url["include"]
    }

    return apk_file_names if len(apk_file_names) > 0 else None


async def get_resource_download_url(group_name: str, file_name: str) -> str | None:
    query = """
        mutation MachineDownloadFile($groupName: String!, $fileName: String!) {
          downloadFile(filesDataInput: $fileName, groupName: $groupName) {
            url
          }
        }
    """
    params = {"groupName": group_name, "fileName": file_name}
    response = await make_graphql_request(INTEGRATES_API_URL, query, params)
    if not response["data"]:
        LOGGER.warning(
            "Failed to fetch download url for file %s in group %s",
            file_name,
            group_name,
        )
        return None

    return response["data"]["downloadFile"]["url"]
