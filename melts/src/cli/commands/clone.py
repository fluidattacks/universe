import os
import shutil
from pathlib import Path

import asyncclick as click
from aioextensions import collect
from fluidattacks_core.git import clone as _clone_from_client

from src.api.integrates import (
    get_git_root_credentials,
    get_group_git_root,
    get_group_git_roots_paginated,
)
from src.logger import LOGGER
from src.utils import GitRootNicknameNotFound, RepositoryCredentialsNotFound


async def _clone_repo(
    group_name: str,
    git_root_id: str,
    git_root_nickname: str,
    repo_url: str,
    repo_branch: str,
    *,
    follow_redirects: bool = False,
) -> None:
    repo_path = Path("groups") / group_name / git_root_nickname
    os.makedirs(repo_path, exist_ok=True)
    credentials = await get_git_root_credentials(group_name, git_root_id)
    if not credentials:
        LOGGER.error("no credentials found for %s", git_root_nickname)
        raise RepositoryCredentialsNotFound(git_root_nickname)

    folder_to_clone_root, stderr = await _clone_from_client(
        repo_url,
        repo_branch,
        credential_key=credentials.get("key"),
        user=credentials.get("user"),
        password=credentials.get("password"),
        provider=credentials.get("oauthType"),
        token=credentials.get("token"),
        is_pat=credentials.get("isPat", False),
        temp_dir=str(repo_path.parent.absolute()),
        arn=credentials.get("arn"),
        org_external_id=credentials.get("organization", {}).get("awsExternalId"),
        follow_redirects=follow_redirects,
    )
    shutil.rmtree(repo_path, ignore_errors=True)
    if folder_to_clone_root:
        os.rename(folder_to_clone_root, repo_path)
    else:
        LOGGER.error("failed to clone: %s", stderr)


async def clone_all_repositories(group_name: str) -> None:
    """Allows you to clone repositories from the client's servers."""
    git_roots = await get_group_git_roots_paginated(group_name)
    git_roots = [git_root for git_root in git_roots if git_root.get("state") == "ACTIVE"]

    await collect(
        [
            _clone_repo(
                group_name,
                root["id"],
                root["nickname"],
                root["url"],
                root["branch"],
                follow_redirects=root["useEgress"] or root["useVpn"] or root["useZtna"],
            )
            for root in git_roots
        ],
        workers=4,
    )


async def clone_repository(group_name: str, repository_nickname: str) -> None:
    """Allows you to clone specified repository from the client's servers."""
    root = await get_group_git_root(group_name, repository_nickname)

    if not root or root.get("state") != "ACTIVE":
        LOGGER.error("git root %s was not found", repository_nickname)
        raise GitRootNicknameNotFound(str(repository_nickname))

    await _clone_repo(
        group_name,
        root["id"],
        root["nickname"],
        root["url"],
        root["branch"],
        follow_redirects=root["useEgress"] or root["useVpn"] or root["useZtna"],
    )


@click.command()
@click.option(
    "--group",
    "group_name",
    required=True,
    help="Specify the name of the group you want to clone.",
)
@click.option(
    "--root",
    "git_root_nickname",
    required=False,
    help=("Clone a specific repository. Provide the name of the repository you want to clone."),
)
async def clone(group_name: str, git_root_nickname: str | None) -> None:
    """Allows you to clone repositories from the client's servers."""
    if git_root_nickname:
        return await clone_repository(group_name, git_root_nickname)

    return await clone_all_repositories(group_name)
