let
  arch = let
    commit = "2f84be48f7ec2581332535567901939174b56ea3";
    sha256 = "sha256:0gbsq448np9g7bhgn7f1k9p5hf5dvavqwg521hs7l75l815dh96r";
    url =
      "https://gitlab.com/fluidattacks/universe/-/raw/${commit}/common/ci/arch.nix";
    src = builtins.fetchurl { inherit sha256 url; };
  in import src;

  rules = {
    default = arch.core.rules.titleRule {
      products = [ "all" "melts" ];
      types = [ ];
    };
    noChore = arch.core.rules.titleRule {
      products = [ "all" "melts" ];
      types = [ "feat" "fix" "refac" ];
    };
  };
in {
  pipelines = {
    melts = {
      gitlabPath = "/melts/pipeline/default.yaml";
      jobs = [
        {
          output = "/melts/coverage";
          gitlabExtra = arch.extras.default // {
            needs = [ "/melts/test" ];
            rules = arch.rules.dev ++ [ rules.noChore ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.melts ];
            variables.GIT_DEPTH = 1000;
          };
        }
        {
          output = "/melts/lint";
          gitlabExtra = arch.extras.default // {
            cache = {
              key = "$CI_COMMIT_REF_NAME-melts-lint";
              paths = [ "melts/.ruff_cache" "melts/.mypy_cache" ];
            };
            rules = arch.rules.dev ++ [ rules.noChore ];
            stage = arch.stages.test;
            tags = [ arch.tags.melts ];
          };
        }
        {
          output = "/melts/test";
          gitlabExtra = arch.extras.default // {
            artifacts = {
              paths = [ "melts/coverage.xml" ];
              expire_in = "1 day";
            };
            rules = arch.rules.dev ++ [ rules.noChore ];
            stage = arch.stages.test;
            tags = [ arch.tags.melts ];
          };
        }
      ];
    };
  };
}
