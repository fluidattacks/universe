{ makeScript, outputs, ... }:
makeScript {
  name = "forces-lint";
  entrypoint = ''
    pushd forces

    if test -n "''${CI:-}"; then
      ruff format --config ruff.toml --diff
      ruff check --config ruff.toml
    else
      ruff format --config ruff.toml
      ruff check --config ruff.toml --fix
    fi

    mypy --config-file mypy.ini forces
    mypy --config-file mypy.ini test
  '';
  searchPaths.source = [ outputs."/forces/env" ];
}
