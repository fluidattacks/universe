{ makeScript, outputs, ... }:
makeScript {
  name = "forces";
  searchPaths = { source = [ outputs."/forces/env" ]; };
  entrypoint = ./entrypoint.sh;
}
