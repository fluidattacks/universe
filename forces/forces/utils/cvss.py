from typing import (
    Literal,
)

from forces.model import (
    CVSS,
)


def get_exploitability_from_vector(cvss_vector: str) -> str:
    exploitability_section = next(
        (item for item in cvss_vector.split("/") if item.startswith("E:")),
        "E:ND",
    )

    return {
        "ND": "Not Defined",
        "X": "Not Defined",
        "U": "Unreported",
        "P": "Proof-of-concept",
        "F": "Functional",
        "H": "High",
        "A": "Attacked",
    }[exploitability_section[2:]]


def get_cvss_fields(
    cvss_version: CVSS,
    item_type: Literal["finding", "vulnerability"],
    field: Literal["severity", "vector"],
) -> str:
    if cvss_version == "3.1":
        if item_type == "finding":
            return {"severity": "severityScore", "vector": "severityVector"}[field]
        # Vulnerability
        return "severityTemporalScore"
    # 4.0
    if item_type == "finding":
        return {
            "severity": "severityScoreV4",
            "vector": "severityVectorV4",
        }[field]
    # Vulnerability
    return "severityThreatScore"


def get_exploitability_measure(exploit: str) -> float:
    return {
        "Not Defined": 1.0,
        "Unproven": 0.91,
        "Unreported": 0.91,
        "Proof-of-concept": 0.94,
        "Functional": 0.97,
        "High": 1.0,
        "Attacked": 1.0,
    }[exploit]
