"""Fluidattacks Forces package."""

import uuid
from pathlib import (
    Path,
)

import simplejson
from aioextensions import (
    in_thread,
)

from forces.apis.git import (
    check_remotes,
    get_repository_metadata,
)
from forces.apis.integrates import (
    set_api_token,
)
from forces.apis.integrates.api import (
    upload_report,
)
from forces.model import (
    ForcesConfig,
    ForcesData,
    ForcesReport,
    KindEnum,
    StatusCode,
)
from forces.report import (
    format_forces_report,
)
from forces.report.data import (
    compile_raw_report,
)
from forces.utils.logs import (
    LOG_FILE,
    log,
    rich_log,
    sanitized_log_file,
)
from forces.utils.strict_mode import (
    set_forces_exit_code,
)
from forces.utils.tree import (
    show_repo_path_logs,
)


async def entrypoint(
    token: str,
    config: ForcesConfig,
) -> int:
    """Entrypoint function"""
    set_api_token(token)
    temp_file = LOG_FILE.get()

    metadata = await get_repository_metadata(
        repo_path=".",
    )
    metadata["git_repo"] = config.repository_name or metadata["git_repo"]
    tasks: dict[str, str] = {
        "gathering": "Gathering findings data",
        "processing": "Processing findings data",
        "formatting": "Formatting findings data",
        "uploading": "Uploading Report to Fluid Attacks",
    }
    footer: str = ": [green]Complete[/]"
    await log(
        "info",
        f"Vuln severities will be calculated with CVSS v{config.cvss}",
    )

    if config.repository_name:
        # Check if repo has been registered and is active
        if not await check_remotes(config):
            return StatusCode.ERROR
        await log(
            "info",
            (
                f"Looking for {config.kind} vulnerabilities "
                f"associated with the repo: "
                f"[bright_yellow]{config.repository_name}[/] "
                f"of group {config.group}."
            ),
        )
        if config.kind != KindEnum.STATIC:
            await log(
                "info",
                (
                    "Dynamic vulnerabilities in this group not associated "
                    "with any repositories will also be included in the "
                    "report"
                ),
            )
    else:
        await log(
            "warning",
            (
                "No specific repository name has been set. "
                "Looking for vulnerabilities in [bright_yellow]all[/] "
                f"repositories registered in {config.group}"
            ),
        )

    await show_repo_path_logs(config)

    await log("info", f"{tasks['gathering']}{footer}")
    report: ForcesData = await compile_raw_report(config)

    if (
        config.verbose_level == 2 and report.summary.total > 0
    ) or not report.summary.overall_compliance:
        await log("info", f"{tasks['processing']}{footer}")
        forces_report: ForcesReport = format_forces_report(
            config,
            report,
        )
        await log("info", f"{tasks['formatting']}{footer}")
        rich_log(forces_report.findings_report)
        rich_log(forces_report.summary_report)
    elif not config.strict:
        await log(
            "info",
            (
                "[green]Congratulations! The Agent didn't find "
                "vulnerable locations that aren't compliant with your "
                "policies[/]"
            ),
        )

    if config.output:
        await in_thread(
            config.output.write,
            simplejson.dumps(
                report,
                default=str,
                indent=1,
                namedtuple_as_object=True,
                use_decimal=True,
            ),
        )
    exit_code = await set_forces_exit_code(config, report.findings)
    uploaded = await upload_report(
        config=config,
        execution_id=str(uuid.uuid4()).replace("-", ""),
        exit_code=str(exit_code),
        report=report,
        log_file=sanitized_log_file(Path(temp_file.name)),
        git_metadata=metadata,
    )
    if uploaded:
        await log("info", f"{tasks['uploading']}{footer}")
    else:
        await log(
            "warning",
            "Unable to upload the execution log to the platform...",
        )
    await log("info", f"Success execution: {exit_code == StatusCode.SUCCESS}")

    return exit_code
