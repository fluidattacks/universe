import asyncio
from random import (
    randint,
)
from typing import (
    Any,
)

from gql import (
    Client,
)
from gql.transport.exceptions import (
    TransportError,
)
from gql.transport.httpx import (
    HTTPXAsyncTransport,
)
from graphql import (
    DocumentNode,
    GraphQLError,
)
from httpx import (
    ReadTimeout,
    Timeout,
)

from forces.apis.integrates import (
    get_api_token,
)
from forces.utils import (
    env,
)
from forces.utils.errors import (
    handle_client_error,
)


async def send(
    operation: DocumentNode,
    operation_name: str,
    variables: dict[str, Any] | None = None,
    attempt: int = 1,
    **kwargs: str,
) -> dict[str, Any]:
    token = kwargs.get("api_token") or get_api_token()
    seconds_between_5xx_retries = randint(20, 20 + min(60, 3 * 2**attempt))
    max_retries = 5
    transport = HTTPXAsyncTransport(
        url=env.ENDPOINT,
        headers={"authorization": f"Bearer {token}"},
        timeout=Timeout(env.TIMEOUT),
        proxy=env.get_proxy(),
        # A local integrates uses self-signed certificates,
        # but other than that the certificate should be valid,
        # particularly in production.
        verify=env.guess_environment() == "production" and env.get_verify_ssl(),
    )

    async with Client(
        transport=transport,
    ) as session:
        try:
            # Manually set the timeout threshold and avoid the retry cool down
            # being roped with the timeout countdown
            async with asyncio.timeout(env.TIMEOUT):
                if variables and "log" in variables and variables["log"].closed:
                    with open(variables["log"].name, "rb") as log_file:
                        variables["log"] = log_file
                        result = await session.execute(
                            operation,
                            variables,
                            operation_name,
                            upload_files=True,
                            get_execution_result=True,
                        )
                else:
                    result = await session.execute(
                        operation,
                        variables,
                        operation_name,
                        upload_files=operation_name == "ForcesUploadReport",
                        get_execution_result=True,
                    )
            return result.data
        except (
            TransportError,
            GraphQLError,
            ReadTimeout,
            TimeoutError,
        ) as client_error:
            # Can be caused by timeouts
            if "This execution already exists" in str(client_error):
                return {"addForcesExecution": {"success": True}}
            await handle_client_error(operation_name, client_error)
            if attempt <= max_retries:
                retry_after: int = (
                    transport.response_headers.get("Retry-After", seconds_between_5xx_retries)
                    if transport.response_headers
                    else seconds_between_5xx_retries
                )
                await asyncio.sleep(retry_after)
                return await send(
                    operation=operation,
                    operation_name=operation_name,
                    variables=variables,
                    attempt=attempt + 1,
                    **kwargs,
                )

        return {}
