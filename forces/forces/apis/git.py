import os
import re
from contextlib import (
    suppress,
)

from git.exc import (
    InvalidGitRepositoryError,
    NoSuchPathError,
)
from git.repo import (
    Repo,
)

from forces.apis.integrates.api import (
    get_git_remotes,
)
from forces.model import (
    ForcesConfig,
    GitRoot,
)
from forces.utils.logs import (
    log,
    log_to_remote,
)

# Constants
DEFAULT_COLUMN_VALUE: str = "unable to retrieve"
REGEXES_GIT_REPO_FROM_ORIGIN = [
    # https://xxxx.visualstudio.com/xxx/_git/repo_name
    re.compile(r"^.*visualstudio.com\/.*\/_git\/(.*)$"),
    # git@ssh.dev.azure.com:v3/xxx/repo_name
    re.compile(r"^.*azure.com:.*\/(.*)"),
    # https://xxx@gitlab.com/xxx/repo_name.git
    re.compile(r"^.*(?:gitlab|github|bitbucket).(?:com|org)(?::|\/).*\/(?:(.*?)(?:\.git)?)$"),
]


def get_commit_from_vars() -> str | None:
    common_variables = {
        "BUILD_SOURCEVERSION",  # Azure devops
        "CI_COMMIT_SHA",  # gitlab-ci
        "CIRCLE_SHA1",  # circleci
        "BITBUCKET_COMMIT",  # bitbucket
        "COMMIT_SHA",  # google cloud
        "GITHUB_SHA",  # GitHub Actions
    }
    for env_var in common_variables:
        if name := os.environ.get(env_var):
            return name

    return None


def get_repo_name_from_vars() -> str | None:
    common_variables = {
        "BUILD_REPOSITORY_NAME",  # Azure devops
        "CI_PROJECT_NAME",  # gitlab-ci
        "CIRCLE_PROJECT_REPONAME",  # circleci
        "BITBUCKET_REPO_FULL_NAME",  # bitbucket
        "REPO_NAME",  # google cloud
        "GITHUB_REPOSITORY",  # GitHub Actions
    }
    for env_var in common_variables:
        if name := os.environ.get(env_var):
            return name

    return None


def get_source_branch_from_vars() -> str | None:
    common_variables = {
        "BUILD_SOURCEBRANCHNAME",  # Azure devops
        "CI_COMMIT_BRANCH",  # gitlab-ci
        "CI_COMMIT_REF_NAME",  # gitlab-ci
        "CIRCLE_BRANCH",  # circleci
        "BITBUCKET_BRANCH",  # bitbucket
        "BRANCH_NAME",  # google cloud
        "GITHUB_REF_NAME",  # GitHub Actions
    }
    for env_var in common_variables:
        if name := os.environ.get(env_var):
            return name

    return None


def extract_repo_name(pattern: str | None) -> str | None:
    if not pattern:
        return None

    for regex in REGEXES_GIT_REPO_FROM_ORIGIN:
        match = regex.match(pattern)
        if match and match.group(1):
            return match.group(1)

    with suppress(IndexError):
        return pattern.split("/")[-1].split(".")[0]

    return None


async def get_repository_metadata(repo_path: str = ".") -> dict[str, str]:
    git_origin = None
    git_branch = get_source_branch_from_vars()
    git_commit = get_commit_from_vars()
    git_repo = get_repo_name_from_vars()

    try:
        repo = Repo(repo_path, search_parent_directories=True)
        git_branch = (
            f"(HEAD detached at {repo.head.commit.hexsha[:11]})"
            if repo.head.is_detached
            else repo.active_branch.name
        )
        git_commit = repo.head.commit.hexsha
        repo_remote_urls = list(repo.remote().urls)
        git_origin = repo_remote_urls[0] if repo_remote_urls else None
        git_repo = (
            git_repo
            or extract_repo_name(git_origin)
            or os.path.basename(os.path.split(repo.git_dir)[0])
        )
    except (
        IndexError,
        InvalidGitRepositoryError,
        NoSuchPathError,
        ValueError,
    ) as ex:
        await log_to_remote(
            ex,
            git_branch=str(git_branch),
            git_commit=str(git_commit),
            git_origin=str(git_origin),
            git_repo=str(git_repo),
        )

    return {
        "git_branch": git_branch or DEFAULT_COLUMN_VALUE,
        "git_commit": git_commit or DEFAULT_COLUMN_VALUE,
        "git_origin": git_origin or DEFAULT_COLUMN_VALUE,
        "git_repo": git_repo or DEFAULT_COLUMN_VALUE,
    }


async def check_remotes(config: ForcesConfig) -> bool:
    if not config.repository_name:
        # if the repo is not specified, it is not required to validate
        # the remotes
        return True

    remotes_dict = await get_git_remotes(config.group)
    api_remotes = tuple(
        GitRoot(
            nickname=remote["nickname"],
            state="ACTIVE" if remote["state"].upper() == "ACTIVE" else "INACTIVE",
            url=remote["url"],
        )
        for remote in remotes_dict
    )

    remote = next(
        (
            remote
            for remote in api_remotes
            if config.repository_name.lower() == remote.nickname.lower()
        ),
        None,
    )
    if remote is None:
        await log(
            "error",
            (
                f"The [bright_yellow]{config.repository_name}[/] repository "
                "has not been registered in the scope of "
                f"[bright_yellow]{config.group}[/]. repo_name can only be "
                "used with active, registered repositories."
            ),
        )
        return False

    is_active_remote = remote.state == "ACTIVE"
    if not is_active_remote:
        await log(
            "error",
            (
                f"The [bright_yellow]{config.repository_name}[/] repository "
                f"registered in [bright_yellow]{config.group}[/] is "
                "inactive. repo_name can only be used with active, "
                "registered repositories."
            ),
        )

    return is_active_remote
