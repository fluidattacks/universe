from decimal import (
    Decimal,
)
from enum import (
    StrEnum,
)
from typing import (
    NamedTuple,
)

from forces.model.vulnerability import (
    Vulnerability,
)


class FindingStatus(StrEnum):
    VULNERABLE: str = "vulnerable"
    SAFE: str = "safe"


class Finding(NamedTuple):
    identifier: str
    title: str
    status: FindingStatus
    exploitability: str
    severity: Decimal
    url: str
    vulnerabilities: list[Vulnerability]
    managed_vulns: int = 0
    unmanaged_vulns: int = 0
