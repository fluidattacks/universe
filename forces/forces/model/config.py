from decimal import (
    Decimal,
)
from enum import (
    StrEnum,
)
from io import (
    TextIOWrapper,
)
from typing import (
    Literal,
    NamedTuple,
    TypeAlias,
)

CVSS: TypeAlias = Literal["3.1", "4.0"]


class KindEnum(StrEnum):
    """DYNAMIC / STATIC vulnerabilities mode"""

    ALL = "all"
    DYNAMIC = "dynamic"
    STATIC = "static"


class ForcesConfig(NamedTuple):
    """Forces user config"""

    organization: str
    group: str
    cvss: CVSS = "3.1"
    kind: KindEnum = KindEnum.ALL
    output: TextIOWrapper | None = None
    repository_paths: tuple[str, ...] = tuple()
    repository_name: str | None = None
    strict: bool | None = False
    verbose_level: int = 2
    breaking_severity: Decimal = Decimal("0.0")
    grace_period: int = 0
    days_until_it_breaks: int | None = None
    feature_preview: bool = False


class Policy(NamedTuple):
    organization: str
    group: str
    arm_severity_policy: float | None
    vuln_grace_period: int
    days_until_it_breaks: int | None
