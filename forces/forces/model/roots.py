from typing import (
    Literal,
    NamedTuple,
)


class GitRoot(NamedTuple):
    nickname: str
    state: Literal["ACTIVE", "INACTIVE"]
    url: str
