from timeit import (
    default_timer as timer,
)
from typing import (
    Any,
)

from forces.apis.integrates.api import (
    get_findings,
    get_vulnerabilities,
    get_vulnerabilities_fallback,
)
from forces.model import (
    Finding,
    ForcesConfig,
    ForcesData,
    ReportSummary,
    SummaryItem,
    VulnerabilitiesClassification,
    VulnerabilityState,
    VulnerabilityType,
)
from forces.model.vulnerability import (
    Vulnerability,
)
from forces.utils.logs import (
    log_to_remote,
)

from .data_parsers import (
    parse_finding,
    parse_location,
)
from .filters import (
    filter_findings,
    filter_kind,
    filter_repo,
    filter_vulnerability,
)


def create_summary_report(
    summary_dict: dict[VulnerabilityState, VulnerabilitiesClassification],
    overall_compliance: bool,
    start_time: float,
) -> ReportSummary:
    """
    Helper method to check instances and create the summary report

    Args:
        `summary_dict
        (dict[VulnerabilityState,  VulnerabilitiesClassification])`:
        Summary dictionary
        `overall_compliance (bool)`: Overall compliance of the report
        `start_time (float)`: Start time of the raw report compiling process

    Returns:
        `ReportSummary`: A named tuple with the summary information

    """
    state = VulnerabilityState.VULNERABLE

    total_dynamic = summary_dict[state]["DYNAMIC"]["total"]
    total_static = summary_dict[state]["STATIC"]["total"]
    techniques_dynamic = summary_dict[state]["DYNAMIC"]["techniques"]
    techniques_sast = summary_dict[state]["STATIC"]["techniques"]

    if (
        isinstance(total_static, int)
        and isinstance(total_dynamic, int)
        and isinstance(techniques_dynamic, dict)
        and isinstance(techniques_sast, dict)
    ):
        summary_item = SummaryItem(
            dynamic=techniques_dynamic,
            total_dynamic=total_dynamic,
            static=techniques_sast,
            total_static=total_static,
        )

        total_vulns = total_static + total_dynamic

    return ReportSummary(
        vulnerable=summary_item,
        overall_compliance=overall_compliance,
        total=total_vulns,
        elapsed_time=f"{(timer() - start_time):.4f} seconds",
    )


def create_classification_structure() -> VulnerabilitiesClassification:
    """
    Helper method to create a dictionary and group vulnerabilities by type
    and technique

    Returns:
        `VulnerabilitiesClassification`: A dictionary with the number of
        vulnerabilities grouped by type and technique

    """
    return {
        "DYNAMIC": {
            "techniques": {"CSPM": 0, "DAST": 0, "PTAAS": 0, "RE": 0},
            "total": 0,
        },
        "STATIC": {
            "techniques": {"CSPM": 0, "SAST": 0, "SCA": 0, "SCR": 0},
            "total": 0,
        },
    }


def update_technique_summary(
    summary_dict: dict[VulnerabilityState, dict[str, dict[str, dict[str, int] | int]]],
    vulnerability: Vulnerability,
) -> None:
    """
    Helper method to update the summary_dict based on the given
    vulnerability
    """
    state = vulnerability.state

    if vulnerability.type == VulnerabilityType.STATIC:
        total_static = summary_dict[state]["STATIC"]["total"]
        data_technique = summary_dict[state]["STATIC"]["techniques"]
        if isinstance(total_static, int) and isinstance(data_technique, dict):
            total_static += 1
            summary_dict[state]["STATIC"]["total"] = total_static
            techniques = data_technique
    else:
        total_dynamic = summary_dict[state]["DYNAMIC"]["total"]
        data_technique = summary_dict[state]["DYNAMIC"]["techniques"]
        if isinstance(total_dynamic, int) and isinstance(data_technique, dict):
            total_dynamic += 1
            summary_dict[state]["DYNAMIC"]["total"] = total_dynamic
            techniques = data_technique

    technique_name = vulnerability.technique.value

    if technique_name in techniques:
        techniques[technique_name] += 1


async def get_group_findings_info(
    config: ForcesConfig,
    **kwargs: str,
) -> dict[str, Finding]:
    """
    Format the findings of a group into a dictionary

    Args:
        `organization (str)`: Organization name
        `group (str)`: Group name

    Returns:
        `dict[str, Finding]`: A dictionary containing the findings of a group
        with their identifier as key

    """
    findings_dict: dict[str, Finding] = {}
    findings = await get_findings(config.group, **kwargs)
    for finding_dict in findings:
        try:
            findings_dict[finding_dict["id"]] = parse_finding(
                config=config,
                finding_dict=finding_dict,
            )
        except (ArithmeticError, KeyError, TypeError, ValueError) as exc:
            await log_to_remote(exc)
            continue
    return findings_dict


def calculate_vulnerabilities(
    vulnerability: Vulnerability,
    finding: Finding,
    config: ForcesConfig,
) -> Finding:
    """Helper method to calculate managed/unmanaged vulnerabilities"""
    if filter_kind(vulnerability, config.kind) and filter_repo(
        vulnerability,
        config.kind,
        config.repository_name,
    ):
        if vulnerability.compliance:
            return finding._replace(managed_vulns=finding.managed_vulns + 1)

        return finding._replace(unmanaged_vulns=finding.unmanaged_vulns + 1)

    return finding


async def compile_raw_report(
    config: ForcesConfig,
    **kwargs: Any,
) -> ForcesData:
    """
    Parses and compiles the data needed for the Forces Report.

    Args:
        `config (ForcesConfig)`: Valid Forces config

    Returns:
        `ForcesData`: A namedtuple with the findings data and a summary

    """
    _start_time: float = timer()

    _summary_dict: dict[VulnerabilityState, VulnerabilitiesClassification] = {
        state: create_classification_structure() for state in VulnerabilityState
    }
    findings_dict: dict[str, Finding] = await get_group_findings_info(
        config,
        **kwargs,
    )
    group_vulnerabilities: tuple[dict[str, Any], ...] = (
        await get_vulnerabilities_fallback(config, **kwargs)
        if config.feature_preview
        else await get_vulnerabilities(config, **kwargs)
    )
    overall_compliance: bool = True

    for vuln_dict in group_vulnerabilities:
        try:
            finding_id: str = str(vuln_dict["findingId"])

            vulnerability = parse_location(
                vuln_dict=vuln_dict,
                config=config,
                exploitability=findings_dict[finding_id].exploitability,
            )

            findings_dict[finding_id] = calculate_vulnerabilities(
                vulnerability,
                findings_dict[finding_id],
                config,
            )
            if not filter_vulnerability(vulnerability, config):
                continue

            update_technique_summary(_summary_dict, vulnerability)

            findings_dict[finding_id].vulnerabilities.append(vulnerability)
            if not vulnerability.compliance:
                overall_compliance = False

        except (ArithmeticError, KeyError, TypeError, ValueError) as exc:
            await log_to_remote(exc)
            continue

    for finding in findings_dict.values():
        finding.vulnerabilities.sort(key=lambda vuln: vuln.severity, reverse=True)

    return ForcesData(
        findings=filter_findings(findings_dict.values()),
        summary=create_summary_report(_summary_dict, overall_compliance, _start_time),
    )
