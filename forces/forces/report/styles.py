from decimal import (
    Decimal,
)

from forces.model import (
    AcceptanceStatus,
    FindingStatus,
    Treatment,
    TreatmentStatus,
    VulnerabilityState,
    VulnerabilityTechnique,
    VulnerabilityType,
)


def style_severity(severity: Decimal) -> str:
    if Decimal("0.0") < severity < Decimal("4.0"):
        return f"[yellow3]{severity}[/]"
    if Decimal("4.0") <= severity < Decimal("7.0"):
        return f"[orange3]{severity}[/]"
    if Decimal("7.0") <= severity < Decimal("9.0"):
        return f"[bright_red]{severity}[/]"
    return f"[bright_red]{severity}[/]"


def style_report(key: str, value: str) -> str:
    """Adds styles as rich console markup to the report values"""
    style_data = {
        "compliance": {
            "Compliant": "[green]",
            "No, breaks build": "[bright_red]",
        },
        "title": "[yellow]",
        "state": {
            VulnerabilityState.VULNERABLE: "[bright_red]",
            VulnerabilityState.SAFE: "[green]",
        },
        "status": {
            FindingStatus.VULNERABLE: "[bright_red]",
            FindingStatus.SAFE: "[green]",
        },
        "exploit": {
            "Not Defined": "[grey66]",
            "Unproven": "[green]",
            "Unreported": "[green]",
            "Proof-of-concept": "[yellow3]",
            "Functional": "[orange3]",
            "High": "[bright_red]",
            "Attacked": "[bright_red]",
        },
        "type": {
            VulnerabilityType.DYNAMIC: "[medium_orchid]",
            VulnerabilityType.STATIC: "[cornflower_blue]",
        },
        "technique": {
            VulnerabilityTechnique.CLOUD: "[light_steel_blue]",
            VulnerabilityTechnique.CSPM: "[dodger_blue2]",
            VulnerabilityTechnique.DAST: "[slate_blue1]",
            VulnerabilityTechnique.PTAAS: "[royal_blue1]",
            VulnerabilityTechnique.RE: "[purple]",
            VulnerabilityTechnique.SAST: "[blue_violet]",
            VulnerabilityTechnique.SCA: "[dodger_blue1]",
            VulnerabilityTechnique.SCR: "[violet]",
        },
    }
    if key == "severity":
        return style_severity(Decimal(value))
    if key in style_data:
        value_style = style_data[key]
        if isinstance(value_style, dict):
            if value in value_style:
                return f"{value_style[value]}{value}[/]"
            return value
        return f"{value_style}{value}[/]"
    return str(value)


def style_summary(key: VulnerabilityState, value: int) -> str:
    """Adds styles as rich console markup to the summary values"""
    markup: str = ""
    if key == VulnerabilityState.ACCEPTED:
        return str(value)
    if key == VulnerabilityState.VULNERABLE:
        if value == 0:
            markup = "[green]"
        elif value < 10:
            markup = "[yellow3]"
        elif value < 20:
            markup = "[orange3]"
        else:
            markup = "[bright_red]"
    elif key == VulnerabilityState.SAFE:
        markup = "[green]"
    return f"{markup}{value!s}[/]"


def style_treatment(treatment: Treatment) -> str:
    match treatment:
        case Treatment(
            TreatmentStatus.ACCEPTED,
            AcceptanceStatus.SUBMITTED,
        ):
            return " [yellow3](pending temp treatment approval)[/]"
        case Treatment(
            TreatmentStatus.ACCEPTED_UNDEFINED,
            AcceptanceStatus.SUBMITTED,
        ):
            return " [yellow3](pending perm treatment approval)[/]"
        case _:
            return ""
