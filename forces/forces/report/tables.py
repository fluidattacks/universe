from collections import (
    Counter,
)

from rich.box import (
    MINIMAL,
)
from rich.table import (
    Table,
)

from forces.model import (
    Finding,
    ForcesConfig,
    KindEnum,
    ReportSummary,
    Vulnerability,
    VulnerabilityState,
)
from forces.report.styles import (
    style_report,
    style_summary,
    style_treatment,
)


def format_vuln_table(config: ForcesConfig, vulns: list[Vulnerability]) -> Table:
    """
    Helper method to create the nested vulns table

    Args:
        `config (ForcesConfig)`: Valid Forces config
        `vulns (tuple[Vulnerability, ...])`: Finding vulnerabilities

    Returns:
        `Table`: A vuln table that gets nested within the respective Finding
        table

    """
    vuln_table = Table(
        show_header=False,
        highlight=True,
        box=MINIMAL,
        border_style="gold1",
    )
    vuln_table.add_column("Vuln attr", style="cyan")
    vuln_table.add_column("Vuln attr values", style="honeydew2", overflow="fold")
    for vuln in vulns:
        vuln_table.add_row("type", style_report("type", vuln.type))
        vuln_table.add_row("technique", style_report("technique", vuln.technique))
        vuln_table.add_row("where", style_report("where", vuln.where))
        vuln_table.add_row("specific", style_report("specific", vuln.specific))
        vuln_table.add_row(
            "state",
            (f"{style_report('state', vuln.state)}{style_treatment(vuln.treatment)}"),
        )
        vuln_table.add_row(
            "severity",
            style_report("severity", str(vuln.severity)),
            end_section=not config.strict,
        )
        if config.strict:
            vuln_table.add_row(
                "compliance",
                style_report(
                    "compliance",
                    "Compliant" if vuln.compliance else "No, breaks build",
                ),
                end_section=True,
            )
    return vuln_table


def format_finding_table(
    config: ForcesConfig,
    finding: Finding,
    table: Table,
) -> Table:
    """
    Helper method to create and append the Finding tables to the Group
    Report

    Args:
        `config (ForcesConfig)`: Valid Forces config
        `finding (Finding)`: A tuple containing a Finding's data
        `table (Table)`: The Group Report table to be appended

    Returns:
        `Table`: The Group Report table with the Finding info appended to it

    """
    finding_summary: Counter = Counter([vuln.state for vuln in finding.vulnerabilities])
    table.add_row("title", style_report("title", finding.title))
    table.add_row("url", finding.url)
    table.add_row("state", style_report("state", finding.status))
    table.add_row(
        "exploit",
        style_report("exploit", finding.exploitability),
    )
    vuln_state = VulnerabilityState.VULNERABLE
    table.add_row(
        vuln_state,
        style_report(vuln_state, str(finding_summary[vuln_state])),
    )
    vulns_data: Table = format_vuln_table(config, finding.vulnerabilities)
    table.add_row("locations", vulns_data, end_section=True)

    return table


def format_summary_report(summary: ReportSummary, kind: KindEnum) -> Table:
    """
    Helper method to create the findings summary table from the report's
    summary data

    Args:
        `summary (ReportSummary)`: A tuple with the raw summary data
        `kind (KindEnum)`: The kind of vulnerabilities forces should focus on
        to

    Returns:
        `Table`: The summary table that gets outputted after the Findings table

    """
    summary_table = Table(
        title="Summary",
        show_header=False,
        highlight=True,
        box=MINIMAL,
        border_style="gold1",
        caption=(f"Total: {summary.total} vulnerabilities\nElapsed time: {summary.elapsed_time}"),
    )

    summary_child_table = Table(
        show_header=False,
        highlight=True,
        box=MINIMAL,
        border_style="gold1",
    )

    # vulnerable, safe and accepted
    summary_table.add_column("Vuln state", style="cyan", justify="center", vertical="middle")
    state = VulnerabilityState.VULNERABLE
    if kind == KindEnum.ALL:
        # DYNAMIC, STATIC and total vulns for each technique
        vuln_types = ["DYNAMIC", "STATIC"]
        summary_table.add_row(
            state.value,
            summary_child_table,
            end_section=True,
        )
        summary_child_table.add_column("Vuln type", justify="center")
        summary_child_table.add_column("Technique", style="yellow")
        summary_child_table.add_column("Value")

        for vuln_type in vuln_types:
            vuln_type_render = False
            vuln_dict = getattr(summary.vulnerable, vuln_type.lower())

            for technique, value in vuln_dict.items():
                if not vuln_type_render:
                    summary_child_table.add_row(
                        style_report("type", vuln_type),
                        style_report("technique", technique),
                        str(value),
                    )
                    vuln_type_render = True
                else:
                    summary_child_table.add_row(
                        "",
                        style_report("technique", technique),
                        str(value),
                    )

            summary_child_table.add_row(
                "",
                "Total",
                style_summary(
                    state,
                    getattr(summary.vulnerable, f"total_{vuln_type.lower()}"),
                ),
                end_section=True,
            )
    else:
        summary_table.add_column("Value")
        summary_table.add_row(
            state,
            style_summary(
                state,
                summary.total,
            ),
            end_section=True,
        )
    return summary_table
