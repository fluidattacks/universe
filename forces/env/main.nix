{ inputs, makePythonEnvironment, makeTemplate, projectPath, ... }:
makeTemplate {
  replace = { __argSrcForces__ = projectPath "/forces"; };
  name = "forces-config-runtime";
  searchPaths = {
    bin = [ inputs.nixpkgs.git ];
    source = [
      (makePythonEnvironment {
        pythonProjectDir = projectPath "/forces";
        pythonVersion = "3.11";
      })
    ];
    pythonPackage = [ (projectPath "/forces") ];
  };
  template = ./template.sh;
}
