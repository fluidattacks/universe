import os
from collections.abc import (
    Iterator,
)

import pytest

from forces.model import (
    ForcesConfig,
)


@pytest.fixture(scope="session")
def test_org() -> str:
    return "okada"


@pytest.fixture(scope="session")
def test_group() -> str:
    return "unittesting"


@pytest.fixture(scope="session")
def test_finding() -> str:
    return "422286126"


@pytest.fixture(scope="session")
def test_token() -> str:
    return os.environ["TEST_FORCES_TOKEN"]


@pytest.fixture(scope="session")
def test_endpoint() -> str:
    return "https://127.0.0.1:8001/api"


@pytest.fixture(scope="session")
def test_config() -> Iterator[ForcesConfig]:
    return ForcesConfig(  # type: ignore[return-value]
        organization="okada",
        group="unittesting",
    )
