import pytest
from gql import (
    gql,
)

from forces.apis.integrates.client import (
    send,
)
from forces.utils.errors import (
    ApiError,
)


@pytest.mark.asyncio
async def test_session_bad_query(test_token: str, test_group: str) -> None:
    query = gql(
        """
            query ForcesTestGetGroup($name: String!){
                groupss(groupName: $name){
                    name
                }
            }
            """,
    )
    try:
        await send(
            operation=query,
            variables={"name": test_group},
            operation_name="ForcesTestGetGroup",
            api_token=test_token,
        )
    except ApiError as exc:
        assert exc.messages
        assert "Cannot query field" in exc.messages[0]


@pytest.mark.asyncio
async def test_session_bad_token(test_group: str) -> None:
    query = gql(
        """
            query ForcesTestGetGroup($name: String!){
                group(groupName: $name){
                    name
                }
            }
            """,
    )

    try:
        await send(
            operation=query,
            variables={"name": test_group},
            operation_name="ForcesTestGetGroup",
            api_token="bad_token",
        )
    except ApiError as exc:
        assert "Login required" in str(exc) or "Token format unrecognized" in str(exc)
