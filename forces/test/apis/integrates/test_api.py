import os
import random
import string
import uuid
from decimal import Decimal
from pathlib import Path

import pytest
from gql.transport.exceptions import TransportError
from graphql import GraphQLError
from httpx import ReadTimeout

from forces.apis.integrates.api import (
    get_findings,
    get_groups_access,
    get_vulnerabilities,
    get_vulnerabilities_fallback,
    upload_report,
)
from forces.model import ForcesConfig, ForcesData, KindEnum, ReportSummary, SummaryItem
from forces.utils.errors import ApiError
from forces.utils.logs import create_temp_file


@pytest.mark.asyncio
async def test_get_findings(
    test_group: str,
    test_token: str,
    test_finding: str,
) -> None:
    result = await get_findings(test_group, api_token=test_token)
    assert test_finding in list(map(lambda x: x["id"], result))
    assert (
        len(tuple(finding for finding in result if str(finding["status"]).upper() == "DRAFT")) == 0
    )
    assert (
        len(tuple(finding for finding in result if str(finding["status"]).upper() != "DRAFT")) == 11
    )


@pytest.mark.asyncio
async def test_get_vulnerabilities(test_token: str, test_config: ForcesConfig) -> None:
    result = await get_vulnerabilities(test_config, api_token=test_token)
    assert len(result) == 39
    assert "192.168.100.105" in result[0]["where"]


@pytest.mark.asyncio
async def test_get_vulnerabilities_fallback(test_token: str, test_config: ForcesConfig) -> None:
    result = await get_vulnerabilities_fallback(test_config, api_token=test_token)
    assert len(result) == 41
    assert "192.168.100.113" in result[0]["where"]


@pytest.mark.asyncio
async def test_compare_vulnerability_fetchers(test_token: str, test_config: ForcesConfig) -> None:
    current_result = await get_vulnerabilities(test_config, api_token=test_token)
    preview_result = await get_vulnerabilities_fallback(test_config, api_token=test_token)
    assert len(tuple(filter(lambda vuln: vuln["state"] != "ACCEPTED", preview_result))) == 38
    assert len(current_result) == 39


@pytest.mark.asyncio
async def test_vulnerabilities_api_filter_static(test_token: str) -> None:
    test_config = ForcesConfig(
        organization="okada",
        group="unittesting",
        kind=KindEnum.STATIC,
    )
    result = await get_vulnerabilities_fallback(test_config, api_token=test_token)
    for vuln in result:
        assert vuln["vulnerabilityType"] == "lines"


@pytest.mark.asyncio
@pytest.mark.skip(
    reason="""
    The severity filter is not linked to the new severityTemporalScore value
    """,
)
async def test_vulnerabilities_api_filter_severity(test_token: str) -> None:
    test_config = ForcesConfig(
        organization="okada",
        group="unittesting",
        breaking_severity=Decimal("3.2"),
        verbose_level=1,
    )
    result = await get_vulnerabilities_fallback(test_config, api_token=test_token)
    for vuln in result:
        if vuln["severityTemporalScore"] is not None:
            assert vuln["severityTemporalScore"] >= 3.0


@pytest.mark.asyncio
async def test_vulnerabilities_api_filter_open(test_token: str) -> None:
    test_config = ForcesConfig(
        organization="okada",
        group="unittesting",
        verbose_level=2,
    )
    result = await get_vulnerabilities_fallback(test_config, api_token=test_token)
    for vuln in result:
        assert vuln["state"] in ["VULNERABLE", "ACCEPTED"]  # ZRs & Treatments


@pytest.mark.asyncio
async def test_get_group_access() -> None:
    try:
        await get_groups_access(api_token="bad_token")
    except ApiError as exc:
        assert "Login required" in exc.messages or "Token format unrecognized" in exc.messages


@pytest.mark.asyncio
async def test_upload_report(test_token: str, test_config: ForcesConfig) -> None:
    test_metadata = {
        "git_branch": "trunk",
        "git_commit": "unable to retrieve",
        "git_origin": "https://gitlab.com/universe",
        "git_repo": "universe",
    }
    test_report = ForcesData(
        findings=tuple(),
        summary=ReportSummary(
            vulnerable=SummaryItem(
                dynamic={"DAST": 0},
                total_dynamic=0,
                static={"PTAAS": 1000},
                total_static=1000,
            ),
            overall_compliance=True,
            elapsed_time="10 seconds",
            total=1000,
        ),
    )
    # Simulate large log
    chars = "".join(random.choices(string.ascii_letters + string.digits, k=1024))
    size = 1024 * 1024 * 10  # 10 MB

    with create_temp_file() as temp_file:
        while os.path.getsize(temp_file.name) < size:
            temp_file.write(chars)
        try:
            result = await upload_report(
                config=test_config,
                execution_id=str(uuid.uuid4()).replace("-", ""),
                exit_code=str(0),
                report=test_report,
                log_file=Path(temp_file.name),
                git_metadata=test_metadata,
                api_token=test_token,
            )
            assert result is True
        except (
            TransportError,
            GraphQLError,
            ReadTimeout,
            TimeoutError,
        ) as client_error:
            assert False, client_error
