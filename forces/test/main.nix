{ makeScript, outputs, projectPath, ... }:
makeScript {
  entrypoint = ./entrypoint.sh;
  name = "forces-test";
  replace = {
    __argForcesRuntime__ = outputs."/forces/env";
    __argSecretsFile__ = projectPath "/integrates/secrets/dev.yaml";
  };
  searchPaths = {
    bin = [ outputs."/integrates/back" outputs."/integrates/db" ];
    source = [
      outputs."/common/utils/aws"
      outputs."/common/utils/sops"
      outputs."/forces/env"
    ];
  };
}
