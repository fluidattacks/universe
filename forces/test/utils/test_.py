from datetime import (
    datetime,
    timedelta,
)
from decimal import (
    Decimal,
)
from typing import (
    Any,
)
from unittest.mock import (
    AsyncMock,
)
from zoneinfo import (
    ZoneInfo,
)

import pytest
from gql.transport.exceptions import (
    TransportError,
    TransportProtocolError,
    TransportQueryError,
    TransportServerError,
)
from graphql import (
    GraphQLError,
)
from httpx import (
    ReadTimeout,
)

from forces.model import (
    Finding,
    FindingStatus,
    ForcesConfig,
    StatusCode,
    Treatment,
    TreatmentStatus,
    Vulnerability,
    VulnerabilityState,
    VulnerabilityTechnique,
    VulnerabilityType,
)
from forces.utils.cvss import (
    get_exploitability_from_vector,
)
from forces.utils.errors import (
    ApiError,
    handle_client_error,
)
from forces.utils.function import (
    RAISE,
    shield,
)
from forces.utils.strict_mode import (
    get_policy_compliance,
    set_breaking_severity,
    set_forces_exit_code,
)
from forces.utils.tree import (
    build_tree_dict,
    get_tree_from_config,
    normalize_paths,
)

# Constants
TIMEZONE: ZoneInfo = ZoneInfo("America/Bogota")


def test_get_exploitability() -> None:
    # CVSS 3.1 vectors
    assert (
        get_exploitability_from_vector("CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:L/A:N")
        == "Not Defined"
    )
    assert (
        get_exploitability_from_vector("CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/E:ND/C:N/I:L/A:N")
        == "Not Defined"
    )
    assert (
        get_exploitability_from_vector("CVSS:3.1/AV:N/AC:H/PR:N/UI:R/S:U/C:N/I:L/A:N/E:U/RL:O/RC:R")
        == "Unreported"
    )
    assert (
        get_exploitability_from_vector("CVSS:3.1/AV:N/AC:H/PR:N/UI:R/S:U/C:N/I:L/A:N/E:P/RL:O/RC:C")
        == "Proof-of-concept"
    )
    assert (
        get_exploitability_from_vector("CVSS:3.1/AV:A/AC:H/PR:N/UI:N/S:U/C:L/I:N/A:N/E:F/RL:O")
        == "Functional"
    )
    assert (
        get_exploitability_from_vector(
            "CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:N/A:N/E:H/RL:U/RC:C/IR:L",
        )
        == "High"
    )

    # CVSS 4.0 vectors
    assert (
        get_exploitability_from_vector(
            "CVSS:4.0/AV:N/AC:L/AT:N/PR:L/UI:N/VC:L/VI:H/VA:L/SC:N/SI:N/SA:N",
        )
        == "Not Defined"
    )
    assert (
        get_exploitability_from_vector(
            "CVSS:4.0/AV:N/AC:L/AT:N/PR:L/UI:N/VC:L/VI:H/VA:L/SC:N/SI:N/E:X",
        )
        == "Not Defined"
    )
    unreported_vector = "CVSS:4.0/AV:N/AC:H/AT:N/PR:N/UI:N/VC:L/VI:N/VA:N/SC:N/SI:N/SA:N/E:U"
    assert get_exploitability_from_vector(unreported_vector) == "Unreported"
    proof_of_concept_vector = "CVSS:4.0/AV:N/AC:L/AT:N/PR:L/UI:N/VC:H/VI:N/VA:N/SC:N/SI:N/SA:N/E:P"
    assert get_exploitability_from_vector(proof_of_concept_vector) == "Proof-of-concept"
    attacked_vector = "CVSS:4.0/AV:N/AC:L/AT:N/PR:H/UI:N/VC:N/VI:L/VA:N/SC:N/SI:N/SA:N/E:A"
    assert get_exploitability_from_vector(attacked_vector) == "Attacked"


def test_set_breaking_severity() -> None:
    assert set_breaking_severity(0.0, 5.0) == Decimal("0.0")
    assert set_breaking_severity(0.1, 5.0) == Decimal("0.1")
    assert set_breaking_severity(6.0, 4.0) == Decimal("4.0")
    assert set_breaking_severity(None, 0.0) == Decimal("0.0")
    assert set_breaking_severity(None, 5.0) == Decimal("5.0")
    assert set_breaking_severity(None, None) == Decimal("0.0")


def test_check_policy_compliance() -> None:
    test_config = ForcesConfig(
        organization="test_org",
        group="test_group",
        strict=True,
        breaking_severity=Decimal(5.0),
        grace_period=5,
    )
    compliant_vuln = Vulnerability(
        type=VulnerabilityType.DYNAMIC,
        technique=VulnerabilityTechnique.SCA,
        where="somewhere",
        specific="port 21",
        state=VulnerabilityState.VULNERABLE,
        severity=Decimal("6.0"),
        report_date=(datetime.now(tz=TIMEZONE) - timedelta(hours=5)),
        exploitability="Proof-of-concept",
        root_nickname=None,
        treatment=Treatment(status=TreatmentStatus.UNTREATED),
        compliance=True,
    )
    assert get_policy_compliance(
        config=test_config,
        report_date=compliant_vuln.report_date,
        severity=compliant_vuln.severity,
        state=compliant_vuln.state,
    )
    non_compliant_vuln = Vulnerability(
        type=VulnerabilityType.DYNAMIC,
        technique=VulnerabilityTechnique.SCA,
        where="somewhere",
        specific="port 21",
        state=VulnerabilityState.VULNERABLE,
        severity=Decimal("6.0"),
        report_date=(datetime.now(tz=TIMEZONE) - timedelta(days=10)),
        exploitability="Unreported",
        root_nickname=None,
        treatment=Treatment(status=TreatmentStatus.UNTREATED),
        compliance=False,
    )
    assert not get_policy_compliance(
        config=test_config,
        report_date=non_compliant_vuln.report_date,
        severity=non_compliant_vuln.severity,
        state=non_compliant_vuln.state,
    )
    second_config = ForcesConfig(
        organization="test_org",
        group="test_group",
        strict=True,
        breaking_severity=Decimal(5.0),
        grace_period=5,
        days_until_it_breaks=10,
    )
    first_non_compliant_vuln = Vulnerability(
        type=VulnerabilityType.DYNAMIC,
        technique=VulnerabilityTechnique.SCA,
        where="somewhere",
        specific="port 21",
        state=VulnerabilityState.VULNERABLE,
        severity=Decimal("6.0"),
        report_date=(datetime.now(tz=TIMEZONE) - timedelta(days=10)),
        exploitability="Unreported",
        root_nickname=None,
        treatment=Treatment(status=TreatmentStatus.UNTREATED),
        compliance=False,
    )
    assert not get_policy_compliance(
        config=second_config,
        report_date=first_non_compliant_vuln.report_date,
        severity=first_non_compliant_vuln.severity,
        state=first_non_compliant_vuln.state,
    )
    second_non_compliant_vuln = Vulnerability(
        type=VulnerabilityType.DYNAMIC,
        technique=VulnerabilityTechnique.SCA,
        where="somewhere",
        specific="port 21",
        state=VulnerabilityState.VULNERABLE,
        severity=Decimal("6.0"),
        report_date=(datetime.now(tz=TIMEZONE) - timedelta(days=12)),
        exploitability="Unreported",
        root_nickname=None,
        treatment=Treatment(status=TreatmentStatus.UNTREATED),
        compliance=False,
    )
    assert get_policy_compliance(
        config=second_config,
        report_date=second_non_compliant_vuln.report_date,
        severity=second_non_compliant_vuln.severity,
        state=second_non_compliant_vuln.state,
    )


@pytest.mark.asyncio
async def test_set_exit_code() -> None:
    test_finding = Finding(
        identifier="dummy identifier",
        title="dummy title",
        status=FindingStatus.VULNERABLE,
        exploitability="High",
        severity=Decimal("5.1"),
        url="https://dummy-url.com",
        vulnerabilities=[
            Vulnerability(
                type=VulnerabilityType.DYNAMIC,
                technique=VulnerabilityTechnique.SCA,
                where="somewhere",
                specific="port 21",
                state=VulnerabilityState.VULNERABLE,
                severity=Decimal("5.1"),
                report_date=(datetime.now(tz=TIMEZONE) - timedelta(days=15)),
                exploitability="High",
                root_nickname=None,
                treatment=Treatment(status=TreatmentStatus.UNTREATED),
                compliance=False,
            ),
        ],
    )
    test_config = ForcesConfig(
        organization="test_org",
        group="test_group",
        strict=True,
        breaking_severity=Decimal(5.0),
        grace_period=0,
    )
    assert (
        await set_forces_exit_code(config=test_config, findings=(test_finding,))
        == StatusCode.BREAK_BUILD
    )


@pytest.mark.asyncio
async def test_shield_success() -> None:
    mock_function = AsyncMock(return_value="success")

    @shield(retries=3, sleep_between_retries=1)
    async def test_function() -> Any:
        return await mock_function()

    result = await test_function()
    assert result == "success"
    mock_function.assert_called_once()


@pytest.mark.asyncio
async def test_shield_retry() -> None:
    mock_function = AsyncMock(side_effect=[Exception("fail"), "success"])

    @shield(retries=3, sleep_between_retries=1)
    async def test_function() -> Any:
        return await mock_function()

    result = await test_function()
    assert result == "success"
    assert mock_function.call_count == 2


@pytest.mark.asyncio
async def test_shield_raise() -> None:
    mock_function = AsyncMock(side_effect=Exception("fail"))

    @shield(retries=3, sleep_between_retries=1, on_error_return=RAISE)
    async def test_func() -> Any:
        return await mock_function()

    with pytest.raises(Exception):
        await test_func()
    assert mock_function.call_count == 3


def test_normalize_paths() -> None:
    paths = (
        "universe/common",
        "makes/*",
        "observes/src/types.py",
        "skims/test/**",
    )
    normalized_paths = normalize_paths(paths, "universe")
    assert normalized_paths == (
        "universe/common",
        "universe/makes/*",
        "universe/observes/src/types.py",
        "universe/skims/test/**",
    )


def test_build_tree_dict() -> None:
    paths = (
        "universe/common",
        "makes/*",
        "observes/src/types.py",
        "skims/test/**",
    )
    normalized_paths = normalize_paths(paths, "universe")
    tree_dict = build_tree_dict(normalized_paths)
    assert tree_dict == {
        "universe": {
            "common": {},
            "makes": {"*": {}},
            "observes": {
                "src": {"types.py": {}},
            },
            "skims": {"test": {"**": {}}},
        },
    }


def test_get_tree_from_config() -> None:
    test_config = ForcesConfig(
        organization="test_org",
        group="test_group",
        repository_name="universe",
        repository_paths=(
            "universe/common",
            "makes/*",
            "observes/src/types.py",
            "skims/test/**",
        ),
    )
    tree = get_tree_from_config(test_config)
    assert tree.label == "🫚 universe"
    assert len(tree.children) == len(test_config.repository_paths)
    for subtree in tree.children:
        assert subtree.label in (
            "📁 common",
            "📁 makes",
            "📁 observes",
            "📁 skims",
        )
        if subtree.children:
            assert len(subtree.children) <= 1


@pytest.mark.asyncio
async def test_handle_client_error() -> None:
    operation = "ForcesDoSomething"
    timeout_error = TimeoutError("Timeout error")
    httpx_timeout_error = ReadTimeout(message="Timeout error")
    token_error = TransportQueryError("Login required")
    query_error = TransportQueryError(
        "Unknown argument 'stateStatus' on field 'Group.vulnerabilities'.",
    )
    protocol_error = TransportProtocolError("Weird error")
    client_error = TransportServerError("Client error", 400)
    server_error = TransportServerError("Server error", 500)
    gql_error = GraphQLError("Unknown argument 'stateStatus' on field 'Group.vulnerabilities'.")
    random_exception = TransportError("Random exception")

    # Should not raise an exception here
    try:
        await handle_client_error(operation, timeout_error)
        await handle_client_error(operation, httpx_timeout_error)
        await handle_client_error(operation, protocol_error)
        await handle_client_error(operation, client_error)
        await handle_client_error(operation, server_error)
    except (
        TimeoutError,
        ReadTimeout,
        TransportError,
        ApiError,
    ) as exc:
        assert False, exc

    with pytest.raises(ApiError):
        await handle_client_error(operation, token_error)
        await handle_client_error(operation, query_error)
        await handle_client_error(operation, gql_error)
    with pytest.raises(TransportError):
        await handle_client_error(operation, random_exception)
