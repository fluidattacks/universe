from fa_purity.utils import (
    raise_exception,
)
from tap_gitlab.intervals import (
    ChainedOpenLeft,
)
from tap_gitlab.intervals.interval import (
    IntervalPoint,
)
from tap_gitlab.intervals.progress import (
    FragmentedProgressInterval,
)


def _greatter(item_1: int, item_2: int) -> bool:
    return item_1 > item_2


def test_squash() -> None:
    fragment_1: FragmentedProgressInterval[int] = (
        FragmentedProgressInterval.new(
            ChainedOpenLeft.new(
                _greatter,
                (
                    IntervalPoint.point(1),
                    IntervalPoint.point(2),
                    IntervalPoint.point(3),
                    IntervalPoint.point(4),
                    IntervalPoint.point(5),
                    IntervalPoint.point(6),
                    IntervalPoint.point(7),
                ),
            )
            .alt(raise_exception)
            .to_union(),
            (
                True,
                True,
                False,
                False,
                False,
                True,
            ),
        )
        .alt(raise_exception)
        .to_union()
    )
    expected: FragmentedProgressInterval[int] = (
        FragmentedProgressInterval.new(
            ChainedOpenLeft.new(
                _greatter,
                (
                    IntervalPoint.point(1),
                    IntervalPoint.point(3),
                    IntervalPoint.point(6),
                    IntervalPoint.point(7),
                ),
            )
            .alt(raise_exception)
            .to_union(),
            (
                True,
                False,
                True,
            ),
        )
        .alt(raise_exception)
        .to_union()
    )
    assert fragment_1.squash() == expected


def test_squash_2() -> None:
    fragment_1: FragmentedProgressInterval[int] = (
        FragmentedProgressInterval.new(
            ChainedOpenLeft.new(
                _greatter,
                (
                    IntervalPoint.point(1),
                    IntervalPoint.point(2),
                    IntervalPoint.point(3),
                    IntervalPoint.point(4),
                    IntervalPoint.point(5),
                    IntervalPoint.point(6),
                    IntervalPoint.point(7),
                ),
            )
            .alt(raise_exception)
            .to_union(),
            (
                False,
                True,
                True,
                True,
                True,
                True,
            ),
        )
        .alt(raise_exception)
        .to_union()
    )
    expected: FragmentedProgressInterval[int] = (
        FragmentedProgressInterval.new(
            ChainedOpenLeft.new(
                _greatter,
                (
                    IntervalPoint.point(1),
                    IntervalPoint.point(2),
                    IntervalPoint.point(7),
                ),
            )
            .alt(raise_exception)
            .to_union(),
            (
                False,
                True,
            ),
        )
        .alt(raise_exception)
        .to_union()
    )
    assert fragment_1.squash() == expected
