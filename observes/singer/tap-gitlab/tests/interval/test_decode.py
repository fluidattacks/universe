from datetime import (
    datetime,
    timezone,
)
from fa_purity.json_2.value import (
    JsonValueFactory,
    Unfolder,
)
from tap_gitlab.intervals.interval import (
    IntervalPoint,
)
from tap_gitlab.state.decoder._core import (
    i_decoder,
)


def test_decode() -> None:
    raw = {
        "type": "IntervalPoint",
        "obj": {"point": {"datetime": "2020-08-26T00:49:16.901999+00:00"}},
    }
    date_time = datetime(2020, 8, 26, 0, 49, 16, 901999, tzinfo=timezone.utc)
    expected = IntervalPoint.point(date_time)
    point = i_decoder.decode_interval_point(
        Unfolder.to_json(JsonValueFactory.from_any(raw).unwrap()).unwrap()
    ).unwrap()
    assert point == expected


def test_decode_min() -> None:
    raw = {"type": "IntervalPoint", "obj": {"point": "MIN"}}
    assert (
        i_decoder.decode_interval_point(
            Unfolder.to_json(JsonValueFactory.from_any(raw).unwrap()).unwrap()
        ).unwrap()
        == IntervalPoint.min()
    )


def test_decode_max() -> None:
    raw = {"type": "IntervalPoint", "obj": {"point": "MAX"}}
    assert (
        i_decoder.decode_interval_point(
            Unfolder.to_json(JsonValueFactory.from_any(raw).unwrap()).unwrap()
        ).unwrap()
        == IntervalPoint.max()
    )
