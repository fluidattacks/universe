from __future__ import (
    annotations,
)

from dataclasses import (
    dataclass,
    field,
)
from fa_purity import (
    Result,
    ResultE,
)
from fa_purity._core.coproduct import (
    Coproduct,
)
from typing import (
    Callable,
    Generic,
    Type,
    TypeVar,
    Union,
)


@dataclass(frozen=True)
class _Private:
    pass


@dataclass(frozen=True)
class MIN:
    def __str__(self) -> str:
        return "MIN"


@dataclass(frozen=True)
class MAX:
    def __str__(self) -> str:
        return "MAX"


class InvalidInterval(Exception):
    pass


_P = TypeVar("_P")
_T = TypeVar("_T")


@dataclass(frozen=True)
class IntervalPoint(Generic[_P]):
    _inner_value: Coproduct[_P, Coproduct[MIN, MAX]]

    @staticmethod
    def min(_type: Type[_P] | None = None) -> IntervalPoint[_P]:
        return IntervalPoint(Coproduct.inr(Coproduct.inl(MIN())))

    @staticmethod
    def max(_type: Type[_P] | None = None) -> IntervalPoint[_P]:
        return IntervalPoint(Coproduct.inr(Coproduct.inr(MAX())))

    @staticmethod
    def point(value: _P) -> IntervalPoint[_P]:
        return IntervalPoint(Coproduct.inl(value))

    @classmethod
    def lower_to_interval(cls, value: Coproduct[_P, MIN]) -> IntervalPoint[_P]:
        return value.map(
            cls.point,
            lambda _: cls.min(),
        )

    @classmethod
    def upper_to_interval(cls, value: Coproduct[_P, MAX]) -> IntervalPoint[_P]:
        return value.map(
            cls.point,
            lambda _: cls.max(),
        )

    def map(
        self,
        min_case: Callable[[MIN], _T],
        max_case: Callable[[MAX], _T],
        value_case: Callable[[_P], _T],
    ) -> _T:
        return self._inner_value.map(
            value_case,
            lambda c: c.map(
                min_case,
                max_case,
            ),
        )

    def __repr__(self) -> str:
        return "IntervalPoint." + self.map(
            lambda _: "MIN", lambda _: "MAX", lambda v: f"value({v})"
        )


Comparison = Callable[[_P, _P], bool]


def greater_point(
    greater: Comparison[_P], _x: IntervalPoint[_P], _y: IntervalPoint[_P]
) -> bool:
    return _x.map(
        lambda _: False,  # MIN > anything
        lambda _: _y.map(
            lambda _: True,  # MAX > MIN
            lambda _: False,  # MAX > MAX
            lambda _: True,  # MAX > any_value
        ),
        lambda x: _y.map(
            lambda _: True,  # any_value > MIN,
            lambda _: False,  # any_value > MAX,
            lambda y: greater(x, y),
        ),
    )


@dataclass(frozen=True)
class ClosedInterval(Generic[_P]):
    _private: _Private = field(repr=False, hash=False, compare=False)
    greater: Comparison[_P]
    lower: _P
    upper: _P

    @staticmethod
    def new(
        greater_than: Comparison[_P],
        lower: _P,
        upper: _P,
    ) -> ResultE[ClosedInterval[_P]]:
        if not greater_than(upper, lower):
            err = InvalidInterval(f"{upper} <= {lower}")
            return Result.failure(Exception(err))
        obj = ClosedInterval(_Private(), greater_than, lower, upper)
        return Result.success(obj)

    @property
    def lower_point(self) -> IntervalPoint[_P]:
        return IntervalPoint.point(self.lower)

    @property
    def upper_point(self) -> IntervalPoint[_P]:
        return IntervalPoint.point(self.upper)

    def __contains__(self, point: IntervalPoint[_P]) -> bool:
        return (
            greater_point(self.greater, point, IntervalPoint.point(self.lower))
            or point == self.lower
        ) and (
            greater_point(self.greater, IntervalPoint.point(self.upper), point)
            or point == self.upper
        )


@dataclass(frozen=True)
class OpenInterval(Generic[_P]):
    _private: _Private = field(repr=False, hash=False, compare=False)
    greater: Comparison[_P]
    lower: Coproduct[_P, MIN]
    upper: Coproduct[_P, MAX]

    @staticmethod
    def new(
        greater_than: Comparison[_P],
        lower: Coproduct[_P, MIN],
        upper: Coproduct[_P, MAX],
    ) -> ResultE[OpenInterval[_P]]:
        if not greater_point(
            greater_than,
            IntervalPoint.upper_to_interval(upper),
            IntervalPoint.lower_to_interval(lower),
        ):
            err = InvalidInterval(f"{upper} <= {lower}")
            return Result.failure(Exception(err))
        obj = OpenInterval(_Private(), greater_than, lower, upper)
        return Result.success(obj)

    @property
    def lower_point(self) -> IntervalPoint[_P]:
        return IntervalPoint.lower_to_interval(self.lower)

    @property
    def upper_point(self) -> IntervalPoint[_P]:
        return IntervalPoint.upper_to_interval(self.upper)

    def __contains__(self, point: IntervalPoint[_P]) -> bool:
        return greater_point(
            self.greater, point, IntervalPoint.lower_to_interval(self.lower)
        ) and greater_point(
            self.greater, IntervalPoint.upper_to_interval(self.upper), point
        )


@dataclass(frozen=True)
class OpenLeftInterval(Generic[_P]):
    _private: _Private = field(repr=False, hash=False, compare=False)
    greater: Comparison[_P]
    lower: Coproduct[_P, MIN]
    upper: _P

    @staticmethod
    def new(
        greater_than: Comparison[_P],
        lower: Coproduct[_P, MIN],
        upper: _P,
    ) -> ResultE[OpenLeftInterval[_P]]:
        if not greater_point(
            greater_than,
            IntervalPoint.point(upper),
            IntervalPoint.lower_to_interval(lower),
        ):
            err = InvalidInterval(f"{upper} <= {lower}")
            return Result.failure(Exception(err))
        obj = OpenLeftInterval(_Private(), greater_than, lower, upper)
        return Result.success(obj)

    @property
    def lower_point(self) -> IntervalPoint[_P]:
        return IntervalPoint.lower_to_interval(self.lower)

    @property
    def upper_point(self) -> IntervalPoint[_P]:
        return IntervalPoint.point(self.upper)

    def __contains__(self, point: IntervalPoint[_P]) -> bool:
        return greater_point(
            self.greater, point, IntervalPoint.lower_to_interval(self.lower)
        ) and (
            greater_point(self.greater, IntervalPoint.point(self.upper), point)
            or point == self.upper
        )


@dataclass(frozen=True)
class OpenRightInterval(Generic[_P]):
    _private: _Private = field(repr=False, hash=False, compare=False)
    greater: Comparison[_P]
    lower: _P
    upper: Coproduct[_P, MAX]

    @staticmethod
    def new(
        greater_than: Comparison[_P],
        lower: _P,
        upper: Coproduct[_P, MAX],
    ) -> ResultE[OpenRightInterval[_P]]:
        if not greater_point(
            greater_than,
            IntervalPoint.upper_to_interval(upper),
            IntervalPoint.point(lower),
        ):
            err = InvalidInterval(f"{upper} <= {lower}")
            return Result.failure(Exception(err))
        obj = OpenRightInterval(_Private(), greater_than, lower, upper)
        return Result.success(obj)

    @property
    def lower_point(self) -> IntervalPoint[_P]:
        return IntervalPoint.point(self.lower)

    @property
    def upper_point(self) -> IntervalPoint[_P]:
        return IntervalPoint.upper_to_interval(self.upper)

    def __contains__(self, point: IntervalPoint[_P]) -> bool:
        return (
            greater_point(self.greater, point, IntervalPoint.point(self.lower))
            or point == self.lower
        ) and greater_point(
            self.greater, IntervalPoint.upper_to_interval(self.upper), point
        )


Interval = Union[
    ClosedInterval[_P],
    OpenInterval[_P],
    OpenLeftInterval[_P],
    OpenRightInterval[_P],
]
