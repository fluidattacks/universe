from .interval import (
    OpenLeftInterval,
)
from .progress import (
    FragmentedProgressInterval,
    ProgressInterval,
)
from dataclasses import (
    dataclass,
)
from datetime import (
    datetime,
    timedelta,
)
from fa_purity import (
    ResultE,
)
from fa_purity._core.coproduct import (
    Coproduct,
)
from fa_purity.pure_iter.factory import (
    from_flist,
)
from fa_purity.pure_iter.transform import (
    chain,
)
from typing import (
    Generic,
    TypeVar,
)

_T = TypeVar("_T")


@dataclass(frozen=True)
class SplittedInterval(Generic[_T]):
    lower: _T
    upper: _T


def split_open_left_interval(
    interval: OpenLeftInterval[_T],
    break_point: _T,
) -> ResultE[SplittedInterval[OpenLeftInterval[_T]]]:
    """
    Split `OpenLeftInterval` into two intervals
    - The breakpoint must be inside the interval
    """
    lower_seg = OpenLeftInterval.new(
        interval.greater,
        interval.lower,
        break_point,
    )
    upper_seg = OpenLeftInterval.new(
        interval.greater,
        Coproduct.inl(break_point),
        interval.upper,
    )
    return lower_seg.bind(
        lambda low: upper_seg.map(lambda up: SplittedInterval(low, up))
    )


def split_progress_backwards(
    interval: OpenLeftInterval[_T],
    break_point: _T,
) -> ResultE[SplittedInterval[ProgressInterval[OpenLeftInterval[_T]]]]:
    """
    Split a `OpenLeftInterval` into two progress intervals
    - The breakpoint must be inside the interval
    - The upper result interval will be completed
    - The lower result interval will be incomplete
    """
    return split_open_left_interval(interval, break_point).map(
        lambda s: SplittedInterval(
            ProgressInterval(s.lower, False),
            ProgressInterval(s.upper, True),
        )
    )


def split_fragmented_progress_backwards(
    prev: FragmentedProgressInterval[_T],
    segment: ProgressInterval[OpenLeftInterval[_T]],
    break_point: _T,
) -> ResultE[FragmentedProgressInterval[_T]]:
    """
    - breaks the segment at the supplied break_point backwards
    - returns a new `FragmentedProgressInterval` overriding the supplied
    segment (if found) by its splitting results
    """
    splitted = split_progress_backwards(segment.interval, break_point)
    return splitted.map(
        lambda s: from_flist(prev.progress_intervals)
        .map(
            lambda p: (s.lower, s.upper) if p == segment else (p,)
        )  # the segment override
        .map(lambda x: from_flist(x))
        .transform(lambda x: chain(x))
    ).bind(
        lambda i: FragmentedProgressInterval.from_progress_intervals(
            i.to_list()
        )
    )


def split_fragmented_progress_datetime_backwards(
    prev: FragmentedProgressInterval[datetime],
    seg: ProgressInterval[OpenLeftInterval[datetime]],
    item_datetime: datetime,
) -> ResultE[FragmentedProgressInterval[datetime]]:
    return split_fragmented_progress_backwards(
        prev, seg, item_datetime - timedelta(microseconds=1)
    )
