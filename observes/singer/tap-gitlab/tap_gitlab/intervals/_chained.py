from __future__ import (
    annotations,
)

from dataclasses import (
    dataclass,
    field,
)
from fa_purity import (
    FrozenList,
    Result,
    ResultE,
)
from fa_purity._core.coproduct import (
    Coproduct,
)
from fa_purity.pure_iter.factory import (
    PureIterFactory,
)
from fa_purity.utils import (
    raise_exception,
)
from tap_gitlab import (
    _utils,
)
from tap_gitlab.intervals.interval import (
    Comparison,
    greater_point,
    IntervalFactory,
    IntervalPoint,
    InvalidInterval,
    OpenLeftInterval,
)
from typing import (
    Generic,
    Optional,
    TypeVar,
)

_P = TypeVar("_P")


@dataclass(frozen=True)
class _Private:
    pass


class InvalidEndpoints(Exception):
    pass


def _to_endpoints(
    intervals: FrozenList[OpenLeftInterval[_P]],
) -> FrozenList[IntervalPoint[_P]]:
    endpoints: FrozenList[IntervalPoint[_P]] = tuple()
    if not intervals:
        raise InvalidInterval("Empty intervals")
    for interval in intervals:
        if not endpoints:
            endpoints = endpoints + (
                interval.lower_point,
                interval.upper_point,
            )
        else:
            if endpoints[-1] == interval.lower_point:
                endpoints = endpoints + (interval.upper_point,)
            else:
                raise InvalidInterval(
                    f"discontinuous: {endpoints[-1]} + {interval}"
                )
    return endpoints


def _to_intervals(
    factory: IntervalFactory[_P],
    endpoints: FrozenList[IntervalPoint[_P]],
) -> FrozenList[OpenLeftInterval[_P]]:
    def _new_interval(
        p_1: Optional[IntervalPoint[_P]],
        p_2: Optional[IntervalPoint[_P]],
    ) -> OpenLeftInterval[_P]:
        if p_1 and p_2:
            point_2 = p_2.map(
                lambda _: raise_exception(TypeError("Unexpected MIN at p2")),
                lambda _: raise_exception(TypeError("Unexpected MAX at p2")),
                lambda x: x,
            )
            return p_1.map(
                lambda p1: factory.new_left_open(Coproduct.inr(p1), point_2),
                lambda _: raise_exception(TypeError("Unexpected MAX at p1")),
                lambda p1: factory.new_left_open(Coproduct.inl(p1), point_2),
            )
        raise InvalidEndpoints("Some point is None")

    return tuple(
        _new_interval(p_1, p_2)
        for p_1, p_2 in _utils.in_pairs(PureIterFactory.from_list(endpoints))
    )


@dataclass(frozen=True)
class ChainedOpenLeft(Generic[_P]):
    _private: _Private = field(repr=False, hash=False, compare=False)
    greater: Comparison[_P]
    endpoints: FrozenList[IntervalPoint[_P]]
    intervals: FrozenList[OpenLeftInterval[_P]]

    @staticmethod
    def new(
        greater: Comparison[_P],
        endpoints: FrozenList[IntervalPoint[_P]],
    ) -> ResultE[ChainedOpenLeft[_P]]:
        """
        - endpoints must be in ascendant order **up to** the greater function
        - avoid providing different instances of the greater function.
        Even if equivalent, two functions are not equal if they are not the
        same instance e.g. using a lambda expression
        ```
            f = lambda x: x
            g = lambda x: x # same definition as f but a different instance
            h = f
            g == f # False
            h == f # True
        ```
        """
        illegal = (
            _utils.in_pairs(PureIterFactory.from_list(endpoints))
            .map(lambda t: (greater_point(greater, t[0], t[1]), t))
            .find_first(lambda p: p[0])
        )

        def _fail(
            interval_1: IntervalPoint[_P], interval_2: IntervalPoint[_P]
        ) -> ResultE[ChainedOpenLeft[_P]]:
            err = Exception(
                f"Break points are not in ascendant order i.e. {interval_1} > {interval_2}"
            )
            return Result.failure(err)

        return illegal.map(lambda i: _fail(i[1][0], i[1][1])).or_else_call(
            lambda: Result.success(
                ChainedOpenLeft(
                    _Private(),
                    greater,
                    endpoints,
                    _to_intervals(IntervalFactory(greater), endpoints),
                )
            )
        )

    @classmethod
    def from_intervals(
        cls, intervals: FrozenList[OpenLeftInterval[_P]]
    ) -> ResultE[ChainedOpenLeft[_P]]:
        """
        - intervals should not be an empty list
        - intervals must share the same greater function instance
        - interval end must coincide with the start of the next interval
        - intervals must be in ascendant order **up to** the greater function
        """
        try:
            base = intervals[0]
        except KeyError as err:
            return Result.failure(Exception(err))
        common_greater = all(
            PureIterFactory.from_list(intervals).map(
                lambda i: i.greater == base.greater
            )
        )
        if not common_greater:
            return Result.failure(
                Exception(
                    "Not all intervals share the same greater "
                    "function **reference** i.e. " + str(intervals)
                )
            )
        endpoints = _to_endpoints(intervals)
        return cls.new(base.greater, endpoints)

    def append(self, item: _P) -> ResultE[ChainedOpenLeft[_P]]:
        """item must be greater than any endpoint present in the chained interval"""
        return self.new(
            self.greater, self.endpoints + (IntervalPoint.point(item),)
        )
