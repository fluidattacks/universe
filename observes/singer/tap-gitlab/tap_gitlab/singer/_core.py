from enum import (
    Enum,
)


class SingerStreams(Enum):
    ISSUE_ASSIGNEES = "issue_assignees"
    ISSUE_LABELS = "issue_labels"
    ISSUE = "issue"
    JOBS = "jobs"
    JOB_TAGS = "job_tags"
    MEMBERS = "members"
    MERGE_REQUESTS = "merge_requests"
