from __future__ import (
    annotations,
)

from ._page import (
    InvalidPage,
    Page,
)
from dataclasses import (
    dataclass,
)
from fa_purity import (
    Cmd,
    FrozenDict,
    FrozenList,
    Result,
)
from fa_purity.json import (
    JsonObj as LegacyJsonObj,
)
from fa_purity.json_2 import (
    JsonObj,
    JsonPrimitiveFactory,
    JsonValue,
    LegacyAdapter,
    Unfolder,
)
from fa_purity.result import (
    ResultFactory,
)
from fa_purity.union import (
    Coproduct,
)
from fa_purity.utils import (
    cast_exception,
    raise_exception,
)
import logging
from pure_requests import (
    response,
    retry as _retry,
)
from pure_requests.basic import (
    Data,
    Endpoint,
    HttpClientFactory,
    Params,
)
from pure_requests.retry import (
    HandledError,
    MaxRetriesReached,
)
from requests.exceptions import (
    ChunkedEncodingError,
    ConnectionError as RequestsConnectionError,
    HTTPError,
    JSONDecodeError,
    RequestException,
)
from typing import (
    TypeVar,
)

LOG = logging.getLogger(__name__)


@dataclass(frozen=True)
class Credentials:
    api_key: str

    def __repr__(self) -> str:
        return "Credentials([masked])"

    def __str__(self) -> str:
        return "Credentials([masked])"


_T = TypeVar("_T")
_S = TypeVar("_S")
_F = TypeVar("_F")


class UnexpectedType(Exception):
    def __init__(self, obj: _T, expected: str) -> None:
        super().__init__(
            f"Expected `{expected}` but got `{type(obj).__name__}`"
        )


def _retry_cmd(retry: int, item: Result[_S, _F]) -> Cmd[Result[_S, _F]]:
    log = Cmd.from_cmd(lambda: LOG.info("retry #%2s waiting...", retry))
    return _retry.cmd_if_fail(item, log + _retry.sleep_cmd(retry ^ 2))


def _http_error_handler(
    error: HTTPError,
) -> HandledError[HTTPError, HTTPError]:
    err_code: int = error.response.status_code  # type: ignore[misc]
    handled = (
        409,
        429,
    )
    if err_code in range(500, 600) or err_code in handled:
        return HandledError.handled(error)
    return HandledError.unhandled(error)


Errors = HTTPError | ChunkedEncodingError | RequestsConnectionError


def _cast_err(error: Errors) -> Errors:
    return error


def _handled_errors(
    error: RequestException,
) -> HandledError[Errors, RequestException]:
    """wrap handled errors, raise unhandled errors"""
    if isinstance(error, HTTPError):
        return _http_error_handler(error).value.map(
            lambda h: HandledError.handled(_cast_err(h)),
            lambda u: HandledError.unhandled(RequestException(u)),
        )
    if isinstance(error, ChunkedEncodingError):
        return HandledError.handled(_cast_err(error))
    if isinstance(error, RequestsConnectionError):
        return HandledError.handled(_cast_err(error))
    raise error


def _cast_http(err: HTTPError) -> RequestException:
    return err


def _cast_json(err: JSONDecodeError) -> RequestException:
    return err


@dataclass(frozen=True)
class HttpJsonClient:
    _creds: Credentials
    _max_retries: int

    def _full_endpoint(self, endpoint: str) -> Endpoint:
        return Endpoint("https://gitlab.com/api/v4" + endpoint)

    @staticmethod
    def new(creds: Credentials) -> HttpJsonClient:
        return HttpJsonClient(
            creds,
            150,
        )

    @property
    def _headers(self) -> JsonObj:
        return FrozenDict(
            {
                "Private-Token": JsonValue.from_primitive(
                    JsonPrimitiveFactory.from_raw(self._creds.api_key)
                )
            }
        )

    def get(
        self, relative_endpoint: str, params: JsonObj
    ) -> Cmd[Coproduct[JsonObj, FrozenList[JsonObj]]]:
        _full = self._full_endpoint(relative_endpoint)
        log = Cmd.from_cmd(
            lambda: LOG.info(
                "API call (get): %s\nparams = %s",
                _full,
                Unfolder.dumps(params),
            )
        )
        client = HttpClientFactory.new_client(None, self._headers, False)
        handled = log + client.get(_full, Params(params)).map(
            lambda r: r.bind(
                lambda r1: response.handle_status(r1).alt(_cast_err)
            )
            .bind(lambda r2: response.json_decode(r2).alt(_cast_json))
            .alt(_handled_errors)
        )
        return _retry.retry_cmd(
            handled,
            _retry_cmd,
            self._max_retries,
        ).map(lambda r: r.alt(raise_exception).unwrap())

    def get_list(
        self, relative_endpoint: str, params: JsonObj
    ) -> Cmd[FrozenList[JsonObj]]:
        _factory: ResultFactory[
            FrozenList[JsonObj], Exception
        ] = ResultFactory()
        return (
            self.get(relative_endpoint, params)
            .map(
                lambda c: c.map(
                    lambda _: _factory.failure(TypeError("Expected list")).alt(
                        cast_exception
                    ),
                    lambda i: _factory.success(i),
                )
            )
            .map(
                lambda r: r.alt(raise_exception).to_union(),
            )
        )

    def get_item(
        self, relative_endpoint: str, params: JsonObj
    ) -> Cmd[JsonObj]:
        _factory: ResultFactory[JsonObj, Exception] = ResultFactory()
        return (
            self.get(relative_endpoint, params)
            .map(
                lambda c: c.map(
                    lambda j: _factory.success(j),
                    lambda _: _factory.failure(
                        TypeError("Expected non-list")
                    ).alt(cast_exception),
                )
            )
            .map(
                lambda r: r.alt(raise_exception).to_union(),
            )
        )

    def legacy_get_list(
        self, relative_endpoint: str, params: LegacyJsonObj
    ) -> Cmd[FrozenList[LegacyJsonObj]]:
        return self.get_list(
            relative_endpoint, LegacyAdapter.json(params)
        ).map(
            lambda items: tuple(LegacyAdapter.to_legacy_json(i) for i in items)
        )

    def legacy_get_item(
        self, relative_endpoint: str, params: LegacyJsonObj
    ) -> Cmd[LegacyJsonObj]:
        return self.get_item(
            relative_endpoint, LegacyAdapter.json(params)
        ).map(LegacyAdapter.to_legacy_json)

    def post(self, relative_endpoint: str) -> Cmd[None]:
        _full = self._full_endpoint(relative_endpoint)
        log = Cmd.from_cmd(lambda: LOG.info("API call (post): %s", _full))
        client = HttpClientFactory.new_client(None, self._headers, False)
        _handled = log + client.post(
            self._full_endpoint(relative_endpoint),
            Params(FrozenDict({})),
            Data(FrozenDict({})),
        ).map(
            lambda c: c.bind(
                lambda r: response.handle_status(r).alt(_cast_http)
            )
            .map(lambda _: None)
            .alt(_handled_errors)
        )

        def _inner_retry(
            retry: int,
            item: Result[None, HandledError[Errors, RequestException]],
        ) -> Cmd[Result[None, HandledError[Errors, RequestException]]]:
            # This function is redundant but mypy needs it to be able to infer the `retry_cmd` types
            return _retry_cmd(retry, item)

        return _retry.retry_cmd(
            _handled,
            _inner_retry,
            self._max_retries,
        ).map(lambda r: r.alt(raise_exception).to_union())


__all__ = [
    "Page",
    "InvalidPage",
]
