from __future__ import (
    annotations,
)

from .ids import (
    UserId,
)
from dataclasses import (
    dataclass,
)
from datetime import (
    datetime,
)
from decimal import (
    Decimal,
)
from enum import (
    Enum,
)
from fa_purity import (
    Maybe,
    Result,
    ResultE,
)


class PipelineStatus(Enum):
    CREATED = "created"
    WAITING_FOR_RESOURCE = "waiting_for_resource"
    PREPARING = "preparing"
    PENDING = "pending"
    RUNNING = "running"
    SUCCESS = "success"
    FAILED = "failed"
    CANCELED = "canceled"
    SKIPPED = "skipped"
    MANUAL = "manual"
    SCHEDULED = "scheduled"

    @staticmethod
    def from_raw(raw: str) -> ResultE[PipelineStatus]:
        try:
            return Result.success(PipelineStatus(raw))
        except ValueError as err:
            return Result.failure(Exception(err))


@dataclass(frozen=True)
class Pipeline:
    sha: str
    before_sha: Maybe[str]
    ref: str
    status: PipelineStatus
    source: str
    duration: Maybe[Decimal]
    queued_duration: Maybe[Decimal]
    user: UserId
    created_at: datetime
    updated_at: Maybe[datetime]
    started_at: Maybe[datetime]
    finished_at: Maybe[datetime]
