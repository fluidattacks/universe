{ lib, makes_inputs, nixpkgs, python_pkgs, python_version, }@args:
let
  bundle = import (makes_inputs.projectPath
    "${makes_inputs.inputs.observesIndex.common.python_pkgs}/fa_singer_io/1.7.0+1.nix")
    args;
in bundle.build_bundle (default: required_deps: builder:
  builder lib (required_deps (python_pkgs // {
    inherit (default.python_pkgs) types-jsonschema types-pyRFC3339;
    purity = python_pkgs.fa-purity;
  })))
