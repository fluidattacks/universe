{ inputs, projectPath, ... }@makes_inputs:
let
  std_jobs = import (projectPath "/observes/common/std_jobs.nix");
  pkg_index = inputs.observesIndex.tap.bugsnag;
in { jobs = std_jobs { inherit pkg_index makes_inputs; }; }
