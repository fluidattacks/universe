from __future__ import (
    annotations,
)

from dataclasses import (
    dataclass,
)
from threading import (
    Lock as _Lock,
)
from typing import (
    TypeVar,
)

from fa_purity import (
    Cmd,
    CmdUnwrapper,
)

_T = TypeVar("_T")


@dataclass(frozen=True)
class ThreadLock:
    _inner: _Lock

    @staticmethod
    def new() -> Cmd[ThreadLock]:
        return Cmd.wrap_impure(lambda: ThreadLock(_Lock()))

    def execute_with_lock(self, cmd: Cmd[_T]) -> Cmd[_T]:
        def _action(unwrapper: CmdUnwrapper) -> _T:
            with self._inner:
                return unwrapper.act(cmd)

        return Cmd.new_cmd(_action)
