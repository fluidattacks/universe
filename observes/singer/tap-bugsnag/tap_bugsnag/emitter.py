import sys
from collections.abc import Callable
from dataclasses import (
    dataclass,
)
from enum import (
    Enum,
)
from typing import (
    IO,
)

from etl_utils.parallel import (
    ThreadPool,
)
from fa_purity import (
    Cmd,
    FrozenList,
    PureIterFactory,
    Stream,
    StreamTransform,
)
from fa_purity.json import (
    JsonObj,
)
from fa_purity.lock import (
    ThreadLock,
)
from fa_singer_io.singer import (
    SingerMessage,
    SingerRecord,
)
from fa_singer_io.singer import (
    emitter as singer_emitter,
)

from .api import (
    ApiClient,
    new_client,
)
from .api.auth import (
    Credentials,
)
from .api.projects import (
    ProjectsApi,
)


class SupportedStreams(Enum):
    COLLABORATORS = "COLLABORATORS"
    ERRORS = "ERRORS"
    EVENTS = "EVENTS"
    EVENT_FIELDS = "EVENT_FIELDS"
    ORGS = "ORGS"
    PIVOTS = "PIVOTS"
    PROJECTS = "PROJECTS"
    RELEASES = "RELEASES"
    STABILITY_TREND = "STABILITY_TREND"
    ALL = "ALL"


@dataclass(frozen=True)
class Emitter:
    _client: ApiClient
    _target: IO[str]
    _pool: ThreadPool
    _lock: ThreadLock

    def _emit(self, record: SingerMessage) -> Cmd[None]:
        return self._lock.acquire + singer_emitter.emit(self._target, record) + self._lock.release

    def _generic_emit_proj_items(
        self,
        stream: SupportedStreams,
        get: Callable[[ProjectsApi], Stream[FrozenList[JsonObj]]],
    ) -> Cmd[None]:
        records = (
            self._client.users.list_orgs_ids.bind(
                lambda o: self._client.orgs(o).list_projects_ids.bind(
                    lambda p: get(self._client.projects(p)),
                ),
            )
            .map(lambda x: PureIterFactory.from_list(x))
            .transform(lambda x: StreamTransform.chain(x))
            .map(
                lambda j: SingerRecord(
                    stream.value,
                    j,
                    None,
                ),
            )
            .map(SingerMessage.from_record)
            .map(self._emit)
        )
        return StreamTransform.consume(records)

    def _emit_collaborators(self) -> Cmd[None]:
        records = (
            self._client.users.list_orgs_ids.bind(lambda o: self._client.orgs(o).list_collaborators)
            .map(lambda x: PureIterFactory.from_list(x))
            .transform(lambda x: StreamTransform.chain(x))
            .map(
                lambda j: SingerRecord(
                    SupportedStreams.COLLABORATORS.value,
                    j,
                    None,
                ),
            )
            .map(SingerMessage.from_record)
            .map(self._emit)
        )
        return StreamTransform.consume(records)

    def _emit_organizations(self) -> Cmd[None]:
        records = (
            self._client.users.list_orgs.map(lambda x: PureIterFactory.from_list(x))
            .transform(lambda x: StreamTransform.chain(x))
            .map(
                lambda j: SingerRecord(
                    SupportedStreams.ORGS.value,
                    j,
                    None,
                ),
            )
            .map(SingerMessage.from_record)
            .map(self._emit)
        )
        return StreamTransform.consume(records)

    def _emit_projects(self) -> Cmd[None]:
        records = (
            self._client.users.list_orgs_ids.bind(lambda o: self._client.orgs(o).list_projects)
            .map(lambda x: PureIterFactory.from_list(x))
            .transform(lambda x: StreamTransform.chain(x))
            .map(
                lambda j: SingerRecord(
                    SupportedStreams.PROJECTS.value,
                    j,
                    None,
                ),
            )
            .map(SingerMessage.from_record)
            .map(self._emit)
        )
        return StreamTransform.consume(records)

    def _emit_errors(self) -> Cmd[None]:
        return self._generic_emit_proj_items(SupportedStreams.ERRORS, lambda p: p.list_errors)

    def _emit_events(self) -> Cmd[None]:
        return self._generic_emit_proj_items(SupportedStreams.EVENTS, lambda p: p.list_events)

    def _emit_event_fields(self) -> Cmd[None]:
        return self._generic_emit_proj_items(
            SupportedStreams.EVENT_FIELDS,
            lambda p: p.list_event_fields,
        )

    def _emit_pivots(self) -> Cmd[None]:
        return self._generic_emit_proj_items(SupportedStreams.PIVOTS, lambda p: p.list_pivots)

    def _emit_releases(self) -> Cmd[None]:
        return self._generic_emit_proj_items(SupportedStreams.RELEASES, lambda p: p.list_releases)

    def _emit_trends(self) -> Cmd[None]:
        records = (
            self._client.users.list_orgs_ids.bind(
                lambda o: self._client.orgs(o).list_projects_ids.map(
                    lambda p: self._client.projects(p).get_stability_trend,
                ),
            )
            .transform(lambda x: StreamTransform.squash(x))
            .map(
                lambda j: SingerRecord(
                    SupportedStreams.STABILITY_TREND.value,
                    j,
                    None,
                ),
            )
            .map(SingerMessage.from_record)
            .map(self._emit)
        )
        return StreamTransform.consume(records)

    def emit(  # noqa: C901, PLR0911
        self,
        selection: SupportedStreams,
    ) -> Cmd[None]:
        match selection:
            case SupportedStreams.COLLABORATORS:
                return self._emit_collaborators()
            case SupportedStreams.ERRORS:
                return self._emit_errors()
            case SupportedStreams.EVENTS:
                return self._emit_events()
            case SupportedStreams.EVENT_FIELDS:
                return self._emit_event_fields()
            case SupportedStreams.ORGS:
                return self._emit_organizations()
            case SupportedStreams.PIVOTS:
                return self._emit_pivots()
            case SupportedStreams.PROJECTS:
                return self._emit_projects()
            case SupportedStreams.RELEASES:
                return self._emit_releases()
            case SupportedStreams.STABILITY_TREND:
                return self._emit_trends()
            case SupportedStreams.ALL:
                commands = (
                    self._emit_collaborators(),
                    self._emit_errors(),
                    self._emit_events(),
                    self._emit_event_fields(),
                    self._emit_organizations(),
                    self._emit_pivots(),
                    self._emit_projects(),
                    self._emit_releases(),
                    self._emit_trends(),
                )
                return self._pool.in_threads_none(PureIterFactory.from_list(commands))


def new_emitter(creds: Credentials, pool: ThreadPool) -> Cmd[Emitter]:
    client = new_client(creds)
    return ThreadLock.new().map(lambda lock: Emitter(client, sys.stdout, pool, lock))
