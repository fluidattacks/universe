from dataclasses import (
    dataclass,
)

from fa_purity import (
    FrozenList,
    PureIterFactory,
    ResultE,
    Stream,
    StreamTransform,
    Unsafe,
)
from fa_purity.json import (
    JsonObj,
)
from pure_requests.basic import (
    HttpClient,
)

from . import (
    _common,
    _utils,
)
from .core.ids import (
    OrganizationId,
)


@dataclass(frozen=True)
class UserApi:
    list_orgs: Stream[FrozenList[JsonObj]]
    list_orgs_ids: Stream[OrganizationId]


def _list_orgs(
    client: HttpClient,
    per_page: int,
) -> Stream[FrozenList[JsonObj]]:
    return _common.generic_stream(
        client,
        _common.api_endpoint("user/organizations"),
        per_page,
    )


def _decode_org_id(raw: JsonObj) -> ResultE[OrganizationId]:
    return _utils.require_id(raw).map(OrganizationId)


def new_users_api(client: HttpClient) -> UserApi:
    return UserApi(
        _list_orgs(client, 100),
        _list_orgs(client, 100)
        .map(lambda x: PureIterFactory.from_list(x))
        .transform(lambda x: StreamTransform.chain(x))
        .map(lambda j: _decode_org_id(j).alt(Unsafe.raise_exception).to_union()),
    )
