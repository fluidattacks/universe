from fa_purity import (
    ResultE,
)
from fa_purity.json import (
    JsonObj,
    JsonPrimitiveUnfolder,
    JsonUnfolder,
    JsonValue,
    Unfolder,
)


def to_str(item: JsonValue) -> ResultE[str]:
    return Unfolder.to_primitive(item).bind(JsonPrimitiveUnfolder.to_str)


def require_id(item: JsonObj) -> ResultE[str]:
    return JsonUnfolder.require(item, "id", to_str)
