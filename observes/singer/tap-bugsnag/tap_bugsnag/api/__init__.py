import logging
from collections.abc import Callable
from dataclasses import (
    dataclass,
)

from fa_purity import (
    Cmd,
    Result,
)
from fa_purity.json import (
    JsonUnfolder,
    Primitive,
    UnfoldedFactory,
)
from pure_requests.basic import (
    Data,
    Endpoint,
    HttpClient,
    HttpClientFactory,
    Params,
)
from requests import (
    RequestException,
    Response,
)

from .auth import (
    Credentials,
)
from .core.ids import (
    OrganizationId,
    ProjectId,
)
from .orgs import (
    OrgsApi,
    new_org_api,
)
from .projects import (
    ProjectsApi,
    new_proj_api,
)
from .user import (
    UserApi,
    new_users_api,
)

LOG = logging.getLogger(__name__)


@dataclass(frozen=True)
class ApiClient:
    orgs: Callable[[OrganizationId], OrgsApi]
    projects: Callable[[ProjectId], ProjectsApi]
    users: UserApi


def _api_call_log_injection(client: HttpClient) -> HttpClient:
    def _get(endpoint: Endpoint, params: Params) -> Cmd[Result[Response, RequestException]]:
        msg = Cmd.wrap_impure(
            lambda: LOG.info(
                "[API] GET endpoint=%s params=%s",
                endpoint.raw,
                JsonUnfolder.dumps(params.raw),
            ),
        )
        return msg + client.get(endpoint, params)

    def _post(
        endpoint: Endpoint,
        params: Params,
        data: Data,
    ) -> Cmd[Result[Response, RequestException]]:
        msg = Cmd.wrap_impure(
            lambda: LOG.info(
                "[API] POST endpoint=%s params=%s data=%s",
                endpoint.raw,
                JsonUnfolder.dumps(params.raw),
                data.raw if isinstance(data.raw, str) else JsonUnfolder.dumps(data.raw),
            ),
        )
        return msg + client.post(endpoint, params, data)

    return HttpClient(_get, _post)


def new_client(creds: Credentials) -> ApiClient:
    headers: dict[str, Primitive] = {
        "Authorization": f"token {creds.api_key}",
        "X-Version": "2",
    }
    client = _api_call_log_injection(
        HttpClientFactory.new_client(None, UnfoldedFactory.from_dict(headers), None),
    )
    return ApiClient(
        lambda o: new_org_api(client, o),
        lambda p: new_proj_api(client, p),
        new_users_api(client),
    )
