from dataclasses import (
    dataclass,
)


@dataclass(frozen=True)
class OrganizationId:
    id_str: str


@dataclass(frozen=True)
class ProjectId:
    id_str: str
