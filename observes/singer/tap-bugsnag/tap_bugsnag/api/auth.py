from dataclasses import (
    dataclass,
)


@dataclass(frozen=True)
class Credentials:
    api_key: str

    def __repr__(self) -> str:
        """Masked str representation."""
        return "[masked]"
