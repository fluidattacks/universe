from dataclasses import (
    dataclass,
)
from typing import (
    Generic,
    TypeVar,
)

from fa_purity import (
    FrozenList,
    Maybe,
)

_T = TypeVar("_T")


@dataclass(frozen=True)
class ResponsePage(Generic[_T]):
    data: FrozenList[_T]
    next_item: Maybe[str]
