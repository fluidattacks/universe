from fa_purity import (
    Cmd,
    Result,
    ResultE,
)
from pure_requests.response import (
    handle_status,
)
from pure_requests.retry import (
    HandledError,
    retry_cmd,
    sleep_cmd,
)
from requests import (
    RequestException,
    Response,
)
from requests.models import (
    HTTPError,
)

TOO_MANY_REQUESTS = 429


def _extract_wait_time(retry: int, error: HTTPError) -> float:
    if error.response is not None and error.response.status_code == TOO_MANY_REQUESTS:  # type: ignore[misc]
        wait_time = error.response.headers["Retry-After"]  # type: ignore[misc]
        return int(wait_time)  # type: ignore[misc]
    return retry**2


def _errors_handler(
    item: RequestException | HTTPError,
) -> HandledError[HTTPError, Exception]:
    if (
        isinstance(item, HTTPError)
        and item.response is not None  # type: ignore[misc]
        and item.response.status_code == TOO_MANY_REQUESTS  # type: ignore[misc]
    ):
        return HandledError.handled(item)
    return HandledError.unhandled(Exception(item))


def _common_handler(
    item: Result[Response, RequestException],
) -> Result[Response, HandledError[HTTPError, Exception]]:
    return item.alt(_errors_handler).bind(lambda r: handle_status(r).alt(_errors_handler))


_nothing = Cmd.wrap_value(None)


def _sleep(
    retry: int,
    item: Result[Response, HandledError[HTTPError, Exception]],
) -> Cmd[Result[Response, HandledError[HTTPError, Exception]]]:
    return (
        item.map(lambda _: _nothing)
        .alt(
            lambda h: h.value.map(
                lambda e: sleep_cmd(_extract_wait_time(retry, e)),
                lambda _: _nothing,
            ),
        )
        .to_union()
        .map(lambda _: item)
    )


def handle_response(item: Cmd[Result[Response, RequestException]]) -> Cmd[ResultE[Response]]:
    cmd = item.map(lambda r: _common_handler(r))
    return retry_cmd(cmd, _sleep, 10).map(lambda r: r.alt(Exception))
