from typing import (
    NoReturn,
)

import click
from etl_utils.parallel import (
    ThreadPool,
)
from fa_purity import (
    Cmd,
    Maybe,
    Unsafe,
)

from .api.auth import (
    Credentials,
)
from .emitter import (
    SupportedStreams,
    new_emitter,
)


@click.command()
@click.option("--api-key", type=str, required=True)
@click.option("--all-streams", is_flag=True, default=False)
@click.argument(
    "name",
    type=click.Choice([x.value for x in iter(SupportedStreams)], case_sensitive=False),
    required=False,
    default=None,
)
def stream(name: str | None, api_key: str, all_streams: bool) -> NoReturn:
    creds = Credentials(api_key)
    emitter = ThreadPool.new(100).bind(lambda p: new_emitter(creds, p))
    selection = (
        SupportedStreams.ALL
        if all_streams
        else Maybe.from_optional(name)
        .map(SupportedStreams)
        .or_else_call(lambda: Unsafe.raise_exception(KeyError(name)))
    )
    cmd: Cmd[None] = emitter.bind(lambda e: e.emit(selection))
    cmd.compute()


@click.group()
def main() -> None:
    # cli group entrypoint
    pass


main.add_command(stream)
