from __future__ import (
    annotations,
)

from dataclasses import (
    dataclass,
)
from datetime import (
    UTC,
    datetime,
)

from fa_purity import (
    FrozenDict,
)

from tap_ce.s3 import (
    S3Extractor,
)


@dataclass(frozen=True)
class SingerState:
    state_uri: str
    parsed_state: FrozenDict[str, str] | None

    @staticmethod
    def new(state_uri: str) -> SingerState:
        s3_extractor = S3Extractor.new(state_uri)
        if s3_extractor.obj_exist() and not s3_extractor.obj_is_empty():
            return SingerState(state_uri, s3_extractor.get_state_content())
        return SingerState(state_uri, None)

    def is_valid_state(self) -> bool:
        return self.parsed_state is not None

    def get_state_datetime(self) -> datetime | None:
        if self.parsed_state:
            str_date: str = self.parsed_state["granularity_value"]
            return datetime.strptime(str_date, "%Y-%m-%dT%H:%M:%S.%fZ").replace(tzinfo=UTC)
        return None
