from __future__ import (
    annotations,
)

from dataclasses import (
    dataclass,
)
from datetime import (
    UTC,
    datetime,
)
from typing import (
    Literal,
    NamedTuple,
)

import boto3
from fa_purity import (
    FrozenList,
)
from mypy_boto3_ce import (
    CostExplorerClient,
)

from tap_ce.state import (
    SingerState,
)


class MetricRecord(NamedTuple):
    service: str
    tag: str
    tag_value: str
    granularity: str
    granularity_value: datetime
    cost: float


@dataclass(frozen=True)
class CostExplorerExtractor:
    client: CostExplorerClient
    state: str | None
    services: bool
    tags: bool
    time_period_start: str
    time_period_end: str

    @staticmethod
    def new(
        state: str | None,
        services: bool,
        tags: bool,
    ) -> CostExplorerExtractor:
        batch_client = boto3.client("ce", region_name="us-east-1")
        return CostExplorerExtractor(
            batch_client,
            state,
            services,
            tags,
            "2024-09-01",
            datetime.now(UTC).replace(day=1).strftime("%Y-%m-%d"),
        )

    def get_unprocessed_service_metrics(self) -> FrozenList[MetricRecord]:
        granularity: Literal["MONTHLY"] = "MONTHLY"

        ce_response = self.client.get_cost_and_usage(
            TimePeriod={
                "Start": self.time_period_start,
                "End": self.time_period_end,
            },
            Granularity=granularity,
            Metrics=["UnblendedCost"],
            GroupBy=[{"Type": "DIMENSION", "Key": "SERVICE"}],
        )

        metrics_list: FrozenList[MetricRecord] = tuple(
            MetricRecord(
                service=group["Keys"][0],
                tag="ALL",
                tag_value="ALL",
                granularity=granularity.lower(),
                granularity_value=datetime.strptime(
                    result["TimePeriod"]["Start"],
                    "%Y-%m-%d",
                ).replace(tzinfo=UTC),
                cost=float("{:.20f}".format(float(group["Metrics"]["UnblendedCost"]["Amount"]))),
            )
            for result in ce_response["ResultsByTime"]
            for group in result["Groups"]
            if float(group["Metrics"]["UnblendedCost"]["Amount"]) > 0
        )

        return metrics_list

    def get_sorted_service_metrics(
        self,
        metrics_list: FrozenList[MetricRecord],
    ) -> FrozenList[MetricRecord]:
        sorted_list: FrozenList[MetricRecord] = tuple(
            sorted(
                metrics_list,
                key=lambda metric: (  # type: ignore[misc]
                    metric.granularity_value,
                    metric.service,
                    metric.cost,
                ),
            ),
        )
        return sorted_list

    def get_filtered_service_metrics(
        self,
        metrics_list: FrozenList[MetricRecord],
        state_datetime: datetime,
    ) -> FrozenList[MetricRecord]:
        filtered_metrics: FrozenList[MetricRecord] = tuple(
            job for job in metrics_list if job.granularity_value > state_datetime
        )
        return filtered_metrics

    def get_services_metrics(self) -> FrozenList[MetricRecord]:
        state_tuple = (
            singer_state.get_state_datetime()
            if self.state
            and (singer_state := SingerState.new(self.state))
            and singer_state.is_valid_state()
            else None
        )

        unprocessed_metrics: FrozenList[MetricRecord] = self.get_unprocessed_service_metrics()
        sorted_metrics: FrozenList[MetricRecord] = self.get_sorted_service_metrics(
            unprocessed_metrics,
        )

        filtered_metrics: FrozenList[MetricRecord] = (
            self.get_filtered_service_metrics(sorted_metrics, state_tuple)
            if state_tuple
            else sorted_metrics
        )

        return filtered_metrics

    def get_unprocessed_tags_metrics(self) -> FrozenList[MetricRecord]:
        metrics_list: list[MetricRecord] = []

        tag_keys: set[str] = {
            "fluidattacks:line",
            "fluidattacks:comp",
            "fluidattacks:stack",
        }

        for tag_key in tag_keys:
            response_tags = self.client.get_tags(
                TimePeriod={"Start": self.time_period_start, "End": self.time_period_end},
                TagKey=tag_key,
            )
            tag_values = response_tags.get("Tags", [])

            response = self.client.get_cost_and_usage(
                TimePeriod={"Start": self.time_period_start, "End": self.time_period_end},
                Granularity="MONTHLY",
                Metrics=["UnblendedCost"],
                Filter={  # type: ignore[misc]
                    "Tags": {
                        "Key": tag_key,
                        "Values": tag_values,
                        "MatchOptions": ["EQUALS"],
                    },
                },
                GroupBy=[
                    {
                        "Type": "TAG",
                        "Key": tag_key,
                    },
                    {
                        "Type": "DIMENSION",
                        "Key": "SERVICE",
                    },
                ],
            )

            for result in response["ResultsByTime"]:
                metrics_list.extend(
                    [
                        MetricRecord(
                            service=group["Keys"][1],
                            tag=tag_key,
                            tag_value=(
                                group["Keys"][0].split("$")[1]
                                if group["Keys"][0].split("$")[1]
                                else "null"
                            ),
                            granularity="monthly",
                            granularity_value=datetime.strptime(
                                result["TimePeriod"]["Start"],
                                "%Y-%m-%d",
                            ).replace(tzinfo=UTC),
                            cost=float(
                                "{:.4f}".format(float(group["Metrics"]["UnblendedCost"]["Amount"])),
                            ),
                        )
                        for group in result["Groups"]
                        if float(group["Metrics"]["UnblendedCost"]["Amount"]) > 0
                    ],
                )

        return tuple(metrics_list)

    def get_sorted_tags_metrics(
        self,
        metrics_list: FrozenList[MetricRecord],
    ) -> FrozenList[MetricRecord]:
        sorted_list: FrozenList[MetricRecord] = tuple(
            sorted(
                metrics_list,
                key=lambda metric: (  # type: ignore[misc]
                    metric.granularity_value,
                    metric.service,
                    metric.cost,
                    metric.tag,
                    metric.tag_value,
                ),
            ),
        )
        return sorted_list

    def get_filtered_tags_metrics(
        self,
        metrics_list: FrozenList[MetricRecord],
        state_datetime: datetime,
    ) -> FrozenList[MetricRecord]:
        filtered_metrics: FrozenList[MetricRecord] = tuple(
            job for job in metrics_list if job.granularity_value > state_datetime
        )
        return filtered_metrics

    def get_tags_metrics(self) -> FrozenList[MetricRecord]:
        state_datetime = (
            singer_state.get_state_datetime()
            if self.state
            and (singer_state := SingerState.new(self.state))
            and singer_state.is_valid_state()
            else None
        )

        unprocessed_metrics: FrozenList[MetricRecord] = self.get_unprocessed_tags_metrics()
        sorted_metrics: FrozenList[MetricRecord] = self.get_sorted_tags_metrics(unprocessed_metrics)

        filtered_metrics: FrozenList[MetricRecord] = (
            self.get_filtered_tags_metrics(sorted_metrics, state_datetime)
            if state_datetime
            else sorted_metrics
        )

        return filtered_metrics

    def get_services_no_tag(self) -> frozenset[str]:
        response = self.client.get_cost_and_usage(
            TimePeriod={"Start": self.time_period_start, "End": self.time_period_end},
            Granularity="MONTHLY",
            Metrics=["UnblendedCost"],
            Filter={  # type: ignore[misc]
                "Not": {
                    "Tags": {  # type: ignore[misc]
                        "Key": "fluidattacks:comp",
                        "Values": ["*"],  # type: ignore[misc]
                        "MatchOptions": ["EQUALS"],  # type: ignore[misc]
                    },
                },
            },
            GroupBy=[
                {
                    "Type": "DIMENSION",
                    "Key": "SERVICE",
                },
            ],
        )

        services_no_tags: frozenset[str] = frozenset(
            group["Keys"][0]
            for result in response["ResultsByTime"]
            for group in result["Groups"]
            if float(group["Metrics"]["UnblendedCost"]["Amount"]) > 0
        )

        return services_no_tags

    def handle_request(self) -> FrozenList[MetricRecord]:
        return (
            *(self.get_services_metrics() if self.services else ()),
            *(self.get_tags_metrics() if self.tags else ()),
        )
