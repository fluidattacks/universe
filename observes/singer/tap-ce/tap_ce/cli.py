import json
import sys
from datetime import (
    datetime,
)
from typing import (
    NoReturn,
    TypedDict,
)

import click

from tap_ce.ce import (
    CostExplorerExtractor,
    MetricRecord,
)
from tap_ce.logs import (
    info,
)

_TYPE_DATE = {"type": "string", "format": "date-time"}
_TYPE_STRING = {"type": "string"}
_TYPE_NUMBER = {"type": "number"}


class SchemaProperties(TypedDict):
    properties: dict[str, dict[str, str] | dict[str, list[str]]]


class Schema(TypedDict):
    key_properties: list[str]
    schema: SchemaProperties
    stream: str
    type: str


def get_schema() -> str:
    schema: Schema = {
        "type": "SCHEMA",
        "stream": "ServicesCost",
        "key_properties": [
            "service",
            "tag",
            "tag_value",
            "granularity_value",
        ],
        "schema": {
            "properties": {
                "service": _TYPE_STRING,
                "tag": _TYPE_STRING,
                "tag_value": _TYPE_STRING,
                "granularity": _TYPE_STRING,
                "granularity_value": _TYPE_DATE,
                "cost": _TYPE_NUMBER,
            },
        },
    }

    return json.dumps(schema)


class Record(TypedDict):
    record: dict[str, str]
    stream: str
    type: str


def encode_record(metric_record: MetricRecord) -> str:
    record: Record = {
        "type": "RECORD",
        "stream": "ServicesCost",
        "record": {
            k: (v if not isinstance(v, datetime) else datetime.strftime(v, "%Y-%m-%dT%H:%M:%S.%fZ"))
            for k, v in metric_record._asdict().items()
        },
    }

    return json.dumps(record)


class State(TypedDict):
    type: str
    value: dict[str, str]


def get_state(job_record: MetricRecord) -> str:
    state: State = {
        "type": "STATE",
        "value": {
            "service": job_record.service,
            "granularity_value": str(job_record.granularity_value),
        },
    }

    return json.dumps(state)


@click.command()
@click.option(
    "--services",
    default=False,
    is_flag=True,
)
@click.option(
    "--tags",
    default=False,
    is_flag=True,
)
@click.option(
    "--state",
    type=str,
    required=False,
    default=None,
    help="json file S3 URI; e.g. s3://mybucket/folder/state.json",
)
def main(
    state: str | None,
    services: bool,
    tags: bool,
) -> NoReturn:
    ce_extractor = CostExplorerExtractor.new(
        state=state,
        services=services,
        tags=tags,
    )
    info(get_schema())
    records = ce_extractor.handle_request()
    for record in iter(records):
        info(encode_record(record))
    if len(records) > 0:
        info(get_state(records[-1]))
    sys.exit(0)
