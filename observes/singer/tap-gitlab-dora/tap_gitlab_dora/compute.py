from dataclasses import (
    dataclass,
)
from datetime import (
    datetime,
    timedelta,
)
from tap_gitlab_dora.exceptions import (
    IssueDatesError,
    MergeRequestDatesError,
)
from tap_gitlab_dora.types import (
    Issue,
    MergeRequest,
    MetricsResponse,
)
from tap_gitlab_dora.utils import (
    Utils,
)

utils: Utils = Utils()


@dataclass(frozen=True)
class Metrics:
    issues: list[Issue]
    merges: list[MergeRequest]

    @staticmethod
    def change_failure_rate(failures: list[Issue], deploys: int) -> str:
        """
        Calculates the change failure rate as a percentage.

        Args:
            failures (list[Issue]): A list of issues labeled as incidents or
                bugs.
            deploys (int): The total number of deployments.

        Returns:
            str: The change failure rate formatted as a percentage.
        """
        return (
            "0.00%"
            if deploys == 0
            else f"{round((len(failures) / deploys) * 100, 2)}%"
        )

    @staticmethod
    def daily_deployment_frequency(total_deploys: int) -> str:
        """
        Calculates the daily deployment frequency based on total deployments,
        assuming the total number of deployments is spread over 20 business
        days.

        Args:
            total_deploys (int): The total number of deployments.

        Returns:
            str: The average number of deployments per business day, rounded to
                the nearest whole number.
        """
        return f"{round(total_deploys / 20)}/day"

    def get_metrics(self, date: datetime) -> MetricsResponse:
        """
        Retrieves metrics including change failure rate, daily deployment
        frequency, mean time to repair, and average deployment time.

        Args:
            date (datetime): The reference date to use for metric calculations.

        Returns:
            MetricsResponse: An object containing various calculated metrics:
                - last_day_deploys (int): The number of deployments that
                occurred on the last business day relative to the given date.
                - total_deploys (int): The total number of deployments.
                - daily_deployment_frequency (str): The average number of
                deployments per business day, calculated as the total number of
                deployments divided by 20.
                - average_deployment_time (str): The average time taken to
                deploy a merge request, formatted as a string.
                - CFR (str): Change Failure Rate, expressed as a percentage.
                - MTR (str): Mean Time to Repair, formatted as a string.
        """
        total_failures = utils.get_failures(self.issues)
        deployments = utils.get_deployments(self.merges)
        total_deploys = len(deployments)

        change_failure_rate = self.change_failure_rate(
            total_failures, total_deploys
        )
        daily_deployment_frequency = self.daily_deployment_frequency(
            total_deploys
        )
        last_day_deploys = self.last_day_deploys(deployments, date)
        mean_time_to_repair = self.mean_time_to_repair(self.issues)
        average_deployment_time = self.average_deploy_time(self.merges)

        return MetricsResponse(
            last_day_deploys=last_day_deploys,
            total_deploys=total_deploys,
            daily_deployment_frequency=daily_deployment_frequency,
            average_deployment_time=average_deployment_time,
            CFR=change_failure_rate,
            MTR=mean_time_to_repair,
        )

    def last_day_deploys(
        self, deployments: list[MergeRequest], date: datetime
    ) -> int:
        """
        Counts the number of deployments that occurred on the last day relative
        to the given date.

        Args:
            deployments (list[MergeRequest]): A list of merge requests.
                date (datetime): The reference date to use for filtering
                deployments.

        Returns:
            int: The number of deployments on the last day.
        """
        return len(Utils.get_last_day_deployments(deployments, date))

    def mean_time_to_repair(self, issues: list[Issue]) -> str:
        """
        Calculates the mean time to repair (MTR) for a list of issues.

        Args:
            issues (list[Issue]): A list of issues.

        Returns:
            str: The mean time to repair formatted as a string.
        """
        issue_times = self.get_issue_times(issues)
        return Utils.get_median(issue_times)

    def average_deploy_time(self, merge_requests: list[MergeRequest]) -> str:
        """
        Calculates the average deployment time for a list of merge requests.

        Args:
            merge_requests (list[MergeRequest]): A list of merge requests.

        Returns:
            str: The average deployment time formatted as a string.

        Raises:
            MergeRequestDatesError: If a merge request does not have valid
            dates.
        """
        merged_merge_requests = utils.get_deployments(merge_requests)

        total_deploy_time: int | float = 0

        for mr in merged_merge_requests:
            created_at_dt = datetime.fromisoformat(mr.created_at)
            if mr.merged_at is not None:
                merged_at_dt = datetime.fromisoformat(mr.merged_at)
            else:
                raise MergeRequestDatesError()
            deploy_time_delta = merged_at_dt - created_at_dt
            total_deploy_time += deploy_time_delta.total_seconds()

        average_deploy_time_seconds = total_deploy_time / len(
            merged_merge_requests
        )
        average_deploy_time_delta = timedelta(
            seconds=average_deploy_time_seconds
        )

        formatted_average_time = average_deploy_time_delta - timedelta(
            microseconds=average_deploy_time_delta.microseconds % 1000000
        )

        return str(formatted_average_time)

    def get_issue_times(self, issues: list[Issue]) -> list[int]:
        """
        Retrieves the duration in seconds for each issue from creation to
        closure.

        Args:
            issues (list[Issue]): A list of issues.

        Returns:
            list[int]: A list of durations in seconds for each issue.

        Raises:
            IssueDatesError: If an issue does not have valid dates.
        """

        def calculate_duration(issue: Issue) -> int:
            created_at = datetime.fromisoformat(issue.created_at)
            if issue.closed_at is not None:
                closed_at = datetime.fromisoformat(issue.closed_at)
            else:
                raise IssueDatesError()

            return int((closed_at - created_at).total_seconds())

        return [
            calculate_duration(issue)
            for issue in issues
            if Utils.filter_issues(issue)
        ]
