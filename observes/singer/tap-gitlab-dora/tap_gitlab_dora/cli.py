import click
from datetime import (
    datetime,
)
from dateutil.relativedelta import (
    relativedelta,
)
import os
import sys
from tap_gitlab_dora import (
    logs,
)
from tap_gitlab_dora.factories import (
    Factory,
)
from tap_gitlab_dora.utils import (
    Utils,
)
from typing import (
    NoReturn,
)


@click.command()
@click.option(
    "--json",
    is_flag=True,
    help=("Generate just the record part in json format."),
)
def main(
    json: bool = False,  # pylint: disable=redefined-outer-name
) -> NoReturn:
    """
    Main entry point for the script.

    This script generates and logs metrics from GitLab API data. It calculates
    metrics such as change failure rate, daily deployment frequency, mean time
    to repair, and average deployment time. The output can be either in JSON
    format or as a schema with records.

    Args:
        json (bool): If set, the script will generate and log only the record
                     part in JSON format. If not set, it logs both the schema
                     and the record part.

    Returns:
        NoReturn: This function exits the program with a status code of 0.
    """
    date = datetime.now() - relativedelta(months=1)
    gitlab_api = Factory.default_api(
        token=os.environ["OBSERVES_UNIVERSE_API_TOKEN"]
    )
    request_time = datetime.now()
    request_time_str = request_time.strftime("%Y-%m-%d %H:%M:%S")

    dora_metrics = Factory.default_metrics(
        gitlab_api.get_issues(date),
        gitlab_api.get_merge_request(date),
    )
    response = dora_metrics.get_metrics(request_time)

    if json:
        logs.info(Utils.get_record(response, request_time_str))
    else:
        logs.info(Utils.get_schema())
        logs.info(Utils.get_record(response, request_time_str))

    sys.exit(0)
