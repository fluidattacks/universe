from ._objs import (
    BulkData,
    BulkJobId,
)
from fa_purity import (
    cast_exception,
    Cmd,
    FrozenDict,
    Result,
    ResultE,
)
from fa_purity.json import (
    JsonObj,
    Primitive,
    UnfoldedFactory,
)
import inspect
import logging
from pure_requests.basic import (
    Endpoint,
    HttpClientFactory,
    Params,
)
from pure_requests.rate_limit import (
    RateLimiter,
)
from requests import (
    Response,
)
from tap_zoho_crm.api.common import (
    API_URL,
    ApiBug,
    Token,
)
import tempfile
from typing import (
    Dict,
    IO,
)
from zipfile import (
    ZipFile,
)

API_ENDPOINT = API_URL + "/crm/bulk/v2/read"
LOG = logging.getLogger(__name__)


def _unzip_one(
    job_id: BulkJobId, response: Response
) -> Cmd[ResultE[BulkData]]:
    def _action() -> ResultE[BulkData]:
        # pylint: disable=consider-using-with
        # need refac of BulkData for enabling the above check
        tmp_zipdir = tempfile.mkdtemp()
        file_zip: IO[bytes] = tempfile.NamedTemporaryFile(mode="wb+")
        file_unzip: IO[str] = tempfile.NamedTemporaryFile(
            mode="w+", encoding="utf-8"
        )
        file_zip.write(response.content)
        file_zip.seek(0)
        LOG.debug("Unzipping file")
        with ZipFile(file_zip, "r") as zip_obj:
            files = zip_obj.namelist()
            if len(files) > 1:
                err = ValueError(
                    f"Expected only one file. Decompressed {len(files)} files."
                )
                return Result.failure(cast_exception(err))
            zip_obj.extract(files[0], tmp_zipdir)
        LOG.debug("Generating BulkData")
        with open(
            tmp_zipdir + f"/{files[0]}", "r", encoding="UTF-8"
        ) as unzipped:
            file_unzip.write(unzipped.read())
        LOG.debug("Unzipped size: %s", file_unzip.tell())
        return Result.success(BulkData(job_id, file_unzip))

    return Cmd.wrap_impure(_action)


def download_result(
    rate_limiter: RateLimiter, token: Token, job_id: BulkJobId
) -> Cmd[BulkData]:
    msg = Cmd.wrap_impure(
        lambda: LOG.info("API: Download bulk job #%s", job_id)
    )
    endpoint = Endpoint(f"{API_ENDPOINT}/" + job_id.job_id + "/result")
    headers: Dict[str, Primitive] = {
        "Authorization": "Zoho-oauthtoken " + token.raw_token
    }
    empty: JsonObj = FrozenDict({})
    client = HttpClientFactory.new_client(
        None, UnfoldedFactory.from_dict(headers), None
    )
    cmd = msg + (
        client.get(endpoint, Params(empty))
        .map(
            lambda r: ApiBug.assume_success(
                "BulkDownloadError.response",
                inspect.currentframe(),
                (str(client), str(endpoint), str(Params(empty))),
                r.alt(cast_exception),
            )
        )
        .bind(lambda x: _unzip_one(job_id, x))
        .map(
            lambda r: ApiBug.assume_success(
                "BulkDownloadError.unzip",
                inspect.currentframe(),
                (str(client), str(endpoint), str(Params(empty))),
                r,
            )
        )
    )
    return rate_limiter.call_or_wait(cmd)
