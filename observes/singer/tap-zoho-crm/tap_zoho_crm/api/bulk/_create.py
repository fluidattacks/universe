from ._decode import (
    decode_bulk_job_obj,
)
from ._objs import (
    BulkJobObj,
    ModuleName,
)
from fa_purity import (
    cast_exception,
    Cmd,
    FrozenDict,
    FrozenTools,
    ResultE,
)
from fa_purity.json import (
    JsonObj,
    JsonUnfolder,
    JsonValue,
    JsonValueFactory,
    Primitive,
    UnfoldedFactory,
    Unfolder,
)
import inspect
import logging
from pure_requests.basic import (
    Data,
    Endpoint,
    HttpClientFactory,
    Params,
)
from pure_requests.rate_limit import (
    RateLimiter,
)
from tap_zoho_crm import (
    _decode,
)
from tap_zoho_crm.api.common import (
    API_URL,
    ApiBug,
    Token,
)
from typing import (
    Dict,
)

API_ENDPOINT = API_URL + "/crm/bulk/v2/read"
LOG = logging.getLogger(__name__)


def _details_unfolder(raw: JsonObj) -> ResultE[JsonObj]:
    return (
        JsonUnfolder.require(raw, "data", Unfolder.to_list)
        .bind(lambda i: _decode.require_index(i, 0))
        .bind(Unfolder.to_json)
        .bind(lambda j: JsonUnfolder.require(j, "details", Unfolder.to_json))
    )


def create_bulk_read_job(
    rate_limiter: RateLimiter, token: Token, module: ModuleName, page: int
) -> Cmd[BulkJobObj]:
    msg = Cmd.wrap_impure(
        lambda: LOG.info("API: Create bulk job for %s @page:%s", module, page)
    )
    endpoint = Endpoint(API_ENDPOINT)
    headers: Dict[str, Primitive] = {
        "Authorization": "Zoho-oauthtoken " + token.raw_token
    }
    _data: JsonObj = FrozenTools.freeze(
        {
            "module": JsonValueFactory.from_unfolded(module.value),
            "page": JsonValueFactory.from_unfolded(page),
        }
    )
    data: JsonObj = FrozenTools.freeze({"query": JsonValue.from_json(_data)})
    empty: JsonObj = FrozenDict({})
    client = HttpClientFactory.new_client(
        None, UnfoldedFactory.from_dict(headers), None
    )
    response = client.post(endpoint, Params(empty), Data(data))
    response_json = response.map(
        lambda r: ApiBug.assume_success(
            "BulkCreateError.response",
            inspect.currentframe(),
            (endpoint.raw, JsonUnfolder.dumps(data)),
            r.alt(cast_exception),
        )
    ).map(
        lambda r: ApiBug.assume_success(
            "BulkCreateError.decode_json",
            inspect.currentframe(),
            (endpoint.raw, JsonUnfolder.dumps(data), r.text),
            _decode.decode_json(r).alt(
                lambda x: x.map(
                    cast_exception,
                    lambda y: y.map(cast_exception, cast_exception),
                )
            ),
        )
    )

    cmd: Cmd[BulkJobObj] = msg + response_json.map(
        lambda j: ApiBug.assume_success(
            "BulkCreateError.decode_details",
            inspect.currentframe(),
            (JsonUnfolder.dumps(j),),
            _details_unfolder(j),
        )
    ).map(
        lambda j: ApiBug.assume_success(
            "BulkCreateError.decode_bulk_job",
            inspect.currentframe(),
            (JsonUnfolder.dumps(j), str(module), str(page)),
            decode_bulk_job_obj(j, module, page, None),
        )
    )

    return rate_limiter.call_or_wait(cmd)
