from . import (
    _get,
)
from ._objs import (
    UsersDataPage,
    UserType,
)
from dataclasses import (
    dataclass,
)
from fa_purity import (
    Cmd,
    Unsafe,
)
from fa_purity.lock import (
    ThreadLock,
)
import inspect
import logging
from pure_requests.rate_limit import (
    RateLimiter,
)
from tap_zoho_crm.api.common import (
    API_URL,
    ApiBug,
    PageIndex,
    Token,
)
from typing import (
    Callable,
)

API_ENDPOINT = API_URL + "/crm/v2/users"
LOG = logging.getLogger(__name__)
_rate_limiter = ThreadLock.new().map(
    lambda lock: ApiBug.assume_success(
        "users_lock",
        inspect.currentframe(),
        tuple([]),
        RateLimiter.new(10, 60, lock),
    )
)
rate_limiter = Unsafe.compute(_rate_limiter)


@dataclass(frozen=True)
class UsersApi:
    get_users: Callable[[UserType, PageIndex], Cmd[UsersDataPage]]


@dataclass(frozen=True)
class UsersApiFactory:
    @staticmethod
    def users_api(token: Token) -> UsersApi:
        return UsersApi(
            lambda ut, p: _get.get_users(rate_limiter, token, ut, p)
        )
