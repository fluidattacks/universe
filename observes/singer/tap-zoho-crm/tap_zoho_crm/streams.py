from etl_utils.bug import (
    Bug,
)
from etl_utils.parallel import (
    ThreadPool,
)
from etl_utils.retry import (
    MaxRetriesReached,
)
from fa_purity import (
    Cmd,
    CmdUnwrapper,
    Coproduct,
    FrozenDict,
    FrozenTools,
    PureIterFactory,
    PureIterTransform,
    Result,
)
from fa_purity.json import (
    JsonObj,
    JsonPrimitive,
    JsonValue,
)
from fa_singer_io.singer import (
    emitter,
    SingerMessage,
    SingerRecord,
)
import inspect
import logging
import sys
from tap_zoho_crm.api import (
    ApiClientFactory,
    UsersApi,
)
from tap_zoho_crm.api.auth import (
    Credentials,
)
from tap_zoho_crm.api.bulk import (
    BulkData,
    BulkJob,
    BulkJobApi,
    BulkJobId,
    ExpiredBulkJob,
    ModuleName,
)
from tap_zoho_crm.api.common import (
    PageIndex,
)
from tap_zoho_crm.api.users import (
    UserType,
)
from tap_zoho_crm.extractor import (
    get_module_data,
)
import tempfile
from typing import (
    Dict,
    FrozenSet,
)

LOG = logging.getLogger(__name__)


def _emit_bulk_data(module: ModuleName, data: BulkData) -> Cmd[None]:
    def _action(unwrap: CmdUnwrapper) -> None:
        with tempfile.NamedTemporaryFile(
            "w+", delete=False
        ) as persistent_file:
            data.file.seek(0)
            persistent_file.write(data.file.read())
            module_name: str = module.value
            options: Dict[str, JsonValue] = {
                "quote_nonnum": JsonValue.from_primitive(
                    JsonPrimitive.from_bool(True)
                ),
                "add_default_types": JsonValue.from_primitive(
                    JsonPrimitive.from_bool(True)
                ),
                "pkeys_present": JsonValue.from_primitive(
                    JsonPrimitive.from_bool(False)
                ),
                "only_records": JsonValue.from_primitive(
                    JsonPrimitive.from_bool(True)
                ),
            }
            raw: Dict[str, JsonValue] = {
                "csv_path": JsonValue.from_primitive(
                    JsonPrimitive.from_str(persistent_file.name)
                ),
                "options": JsonValue.from_json(
                    FrozenTools.freeze(options),
                ),
            }
            record = SingerRecord(module_name, FrozenTools.freeze(raw), None)
            unwrap.act(
                emitter.emit(sys.stdout, SingerMessage.from_record(record))
            )

    return Cmd.new_cmd(_action)


def _emit_bulk_data_set(
    data: FrozenSet[BulkData],
    id_job_map: FrozenDict[BulkJobId, BulkJob],
) -> Cmd[None]:
    return (
        PureIterFactory.from_list(tuple(data))
        .map(lambda x: _emit_bulk_data(id_job_map[x.job_id].module, x))
        .transform(PureIterTransform.consume)
    )


def _emit_user_data(client: UsersApi) -> Cmd[None]:
    def _emit(user_data: JsonObj) -> Cmd[None]:
        record = SingerRecord("users", user_data, None)
        return emitter.emit(sys.stdout, SingerMessage.from_record(record))

    return client.get_users(UserType.ANY, PageIndex(1, 200)).bind(
        lambda d: PureIterFactory.from_list(tuple(d.data))
        .map(_emit)
        .transform(lambda x: PureIterTransform.consume(x))
    )


def _emit_module_data(
    module: ModuleName,
    result: Result[BulkData, Coproduct[ExpiredBulkJob, MaxRetriesReached]],
) -> Cmd[None]:
    msg = Cmd.wrap_impure(
        lambda: LOG.info("Emitting module `%s` data to singer...", module)
    )
    done = Cmd.wrap_impure(
        lambda: LOG.info("Module `%s` data emitted!", module)
    )
    data = Bug.assume_success(
        "_emit_module_data", inspect.currentframe(), tuple(), result
    )
    return msg + _emit_bulk_data(module, data) + done


def _emit_modules(
    client: BulkJobApi, target_modules: FrozenSet[ModuleName]
) -> Cmd[None]:
    emissions = PureIterFactory.from_list(tuple(target_modules)).map(
        lambda m: get_module_data(client, m).bind(
            lambda r: _emit_module_data(m, r)
        )
    )
    return ThreadPool.new(10).bind(
        lambda pool: pool.in_threads_none(emissions)
    )


def stream_data(
    crm_creds: Credentials, target_modules: FrozenSet[ModuleName]
) -> Cmd[None]:
    return ThreadPool.new(2).bind(
        lambda pool: pool.in_threads_none(
            PureIterFactory.from_list(
                [
                    ApiClientFactory.new_client(crm_creds).bind(
                        lambda c: _emit_modules(c.bulk, target_modules)
                    ),
                    ApiClientFactory.new_client(crm_creds).bind(
                        lambda c: _emit_user_data(c.users)
                    ),
                ]
            )
        )
    )
