from ._logger import (
    set_logger,
)
from fa_purity import (
    Unsafe,
)

__version__ = "1.3.0"

Unsafe.compute(set_logger(__name__, __version__))
