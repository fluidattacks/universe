import boto3
from collections.abc import (
    Iterator,
)
from datetime import (
    datetime,
    timedelta,
)
from typing import (
    NamedTuple,
    Tuple,
)


class MetricRecord(NamedTuple):
    metric: str
    data_type: str
    timestamp: datetime
    value: float


class CloudwatchExtraction:
    namespace: str
    static: str

    def __init__(self, namespace: str, static: str) -> None:
        self.namespace = namespace
        self.static = static
        self.client = boto3.client("cloudwatch", region_name="us-east-1")
        self.start_time = datetime.utcnow() - timedelta(days=15 * 30)
        self.end_time = datetime.utcnow()

    def get_metrics(self) -> Tuple[str, ...]:
        metrics_paginator = self.client.get_paginator("list_metrics")
        response_iterator = metrics_paginator.paginate(
            Namespace=self.namespace
        )

        return tuple(
            metric["MetricName"]
            for response in response_iterator
            for metric in response.get("Metrics", [])
        )

    def get_metrics_data(self) -> Iterator[MetricRecord]:
        for metric in self.get_metrics():
            response = self.client.get_metric_data(
                MetricDataQueries=[
                    {
                        "Id": str(metric).lower(),
                        "MetricStat": {
                            "Metric": {
                                "Namespace": self.namespace,
                                "MetricName": metric,
                                "Dimensions": [],
                            },
                            "Period": 86400,
                            "Stat": self.static,
                        },
                        "ReturnData": True,
                    },
                ],
                StartTime=self.start_time,
                EndTime=self.end_time,
            )

            metric_data_results = response["MetricDataResults"]

            for result in metric_data_results:
                for timestamp, value in zip(
                    result["Timestamps"], result["Values"]
                ):
                    yield MetricRecord(
                        metric=result["Id"],
                        data_type=self.namespace,
                        timestamp=timestamp,
                        value=value,
                    )
