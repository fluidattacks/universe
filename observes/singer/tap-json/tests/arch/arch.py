from arch_lint.dag import (
    DAG,
    DagMap,
)
from arch_lint.graph import (
    FullPathModule,
)
from fa_purity import (
    FrozenList,
)
from typing import (
    TypeVar,
)

_T = TypeVar("_T")
_dag: dict[str, FrozenList[FrozenList[str] | str]] = {
    "tap_json": (
        "cli",
        "emitter",
        ("linearize", "auto_schema"),
        (
            "cast",
            "_core",
            "_logger",
            "env",
            "_utils",
        ),
    ),
    "tap_json.auto_schema": (
        "_schema",
        ("combine", "_types"),
        "config",
        "jschema",
    ),
    "tap_json.auto_schema.jschema": (
        ("decode", "encode"),
        "object",
        ("number", "string"),
    ),
    "tap_json.auto_schema.jschema.decode": (
        "_object",
        ("_number", "_string"),
        "_core",
    ),
    "tap_json.auto_schema.jschema.encode": (
        "_object",
        ("_number", "_string"),
    ),
    "tap_json.linearize": (
        ("_to_table", "_flattener"),
        "_nested_id",
        "_core",
    ),
    "tap_json.emitter": (
        "_transform",
        "_input",
    ),
    "tap_json._utils": (
        "temp_file",
        ("io_file", "dict", "singer", "iter"),
    ),
}


def raise_if_exception(item: _T | Exception) -> _T:
    if isinstance(item, Exception):
        raise item
    return item


def project_dag() -> DagMap:
    return raise_if_exception(DagMap.new(_dag))


def forbidden_allowlist() -> dict[FullPathModule, frozenset[FullPathModule]]:
    _raw: dict[str, frozenset[str]] = {}
    return {
        FullPathModule.assert_module(k): frozenset(
            FullPathModule.assert_module(i) for i in v
        )
        for k, v in _raw.items()
    }
