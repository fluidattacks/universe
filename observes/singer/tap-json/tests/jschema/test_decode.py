from fa_purity.json_2 import (
    JsonValueFactory,
    Unfolder,
)
from tap_json.auto_schema.jschema.decode import (
    DEFAULT_DECODER,
)
from tap_json.auto_schema.jschema.number import (
    IntSizes,
    IntType,
    NumberType,
)
from tap_json.auto_schema.jschema.object import (
    ObjectPropertyType,
)


def test_decode() -> None:
    raw = (
        JsonValueFactory.from_any(
            {"type": ["null", "integer"], "size": "small"}
        )
        .bind(Unfolder.to_json)
        .unwrap()
    )
    expected = ObjectPropertyType.from_number_type(
        NumberType.from_int(IntType(IntSizes.SMALL))
    )
    assert DEFAULT_DECODER.decode_obj_prop_type(raw).unwrap() == expected
