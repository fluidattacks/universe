from fa_purity import (
    FrozenDict,
    Maybe,
    ResultE,
)
from fa_purity.result import (
    ResultFactory,
)
from typing import (
    TypeVar,
)

_K = TypeVar("_K")
_V = TypeVar("_V")


def require_key(items: FrozenDict[_K, _V], key: _K) -> ResultE[_V]:
    _factory: ResultFactory[_V, Exception] = ResultFactory()
    if key in items:
        return _factory.success(items[key])
    return _factory.failure(KeyError(key))


def get_key(items: FrozenDict[_K, _V], key: _K) -> Maybe[_V]:
    return Maybe(require_key(items, key).alt(lambda _: None))


def merge(
    items_1: FrozenDict[_K, _V], items_2: FrozenDict[_K, _V]
) -> FrozenDict[_K, _V]:
    return FrozenDict(dict(items_1) | dict(items_2))
