from fa_singer_io.singer import (
    SingerMessage,
    SingerRecord,
    SingerSchema,
    SingerState,
)
from typing import (
    Callable,
    TypeVar,
)

_T = TypeVar("_T")


def handle_singer(
    msg: SingerMessage,
    record: Callable[[SingerRecord], _T],
    schema: Callable[[SingerSchema], _T],
    state: Callable[[SingerState], _T],
) -> _T:
    if isinstance(msg, SingerRecord):
        return record(msg)
    if isinstance(msg, SingerSchema):
        return schema(msg)
    if isinstance(msg, SingerState):
        return state(msg)
