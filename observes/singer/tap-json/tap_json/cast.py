from datetime import (
    datetime,
)
from dateutil.parser import (
    parse as date_parser,
    ParserError,
)
from fa_purity import (
    Result,
    ResultE,
)
from fa_purity.json_2 import (
    JsonPrimitive,
)
from fa_purity.result import (
    ResultFactory,
)
from fa_purity.utils import (
    cast_exception,
)


def str_to_float(raw: str) -> ResultE[float]:
    try:
        return Result.success(float(raw))
    except ValueError as err:
        return Result.failure(err, float).alt(cast_exception)


def str_to_date(raw: str) -> ResultE[datetime]:
    """Manipulate a date to provide a RFC339 compatible date."""
    try:
        is_float = str_to_float(raw).map(lambda _: True).value_or(False)
        if not is_float:
            return Result.success(date_parser(raw))
        return Result.failure(
            ValueError("Value is a float not a datetime"), datetime
        ).alt(cast_exception)
    except ParserError as err:
        return Result.failure(cast_exception(err))
    except OverflowError as err:
        return Result.failure(cast_exception(err))
    except Exception as err:
        raise ValueError(raw) from err


def primitive_to_date(raw: JsonPrimitive) -> ResultE[datetime]:
    _factory: ResultFactory[datetime, Exception] = ResultFactory()
    return raw.map(
        str_to_date,
        lambda _: _factory.failure(TypeError("Cannot cast int to datetime")),
        lambda _: _factory.failure(TypeError("Cannot cast float to datetime")),
        lambda _: _factory.failure(
            TypeError("Cannot cast Decimal to datetime")
        ),
        lambda _: _factory.failure(TypeError("Cannot cast bool to datetime")),
        lambda: _factory.failure(TypeError("Cannot cast None to datetime")),
    )
