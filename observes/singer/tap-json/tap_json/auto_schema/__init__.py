from ._schema import (
    MutableSchema,
    MutableSchemaMap,
)
from ._types import (
    infer_item_type,
)
from .jschema.encode import (
    DEFAULT_ENCODER,
)
from collections.abc import (
    Callable,
)
from dataclasses import (
    dataclass,
)
from fa_purity import (
    Cmd,
    FrozenDict,
    PureIter,
    Stream,
)
from fa_purity.json import (
    JsonValue as LegacyJsonValue,
)
from fa_purity.json_2 import (
    JsonPrimitive,
)
from fa_purity.pure_iter import (
    PureIterFactory,
    PureIterTransform,
)
from fa_purity.stream import (
    StreamTransform,
)
from fa_singer_io.json_schema import (
    JSchemaFactory,
    JsonSchema,
)
from fa_singer_io.singer import (
    SingerSchema,
)
from tap_json._core import (
    TableRecordPair,
)
from tap_json.auto_schema.combine import (
    merge_types,
)
from tap_json.auto_schema.config import (
    InferenceConfig,
)
from tap_json.auto_schema.jschema.number import (
    IntSizes,
    IntType,
    NumberType,
)
from tap_json.auto_schema.jschema.object import (
    ObjectPropertyType,
)
from tap_json.auto_schema.jschema.string import (
    StringFormat,
    StringType,
)
from typing import (
    Tuple,
    TypeVar,
)

TableName = str
_K = TypeVar("_K")
_V = TypeVar("_V")
_A = TypeVar("_A")
_B = TypeVar("_B")


def _map_dict(
    items: FrozenDict[_K, _V],
    keys_fx: Callable[[_K], _A],
    values_fx: Callable[[_V], _B],
) -> FrozenDict[_A, _B]:
    return (
        PureIterFactory.from_list(tuple(items.items()))
        .map(lambda t: (keys_fx(t[0]), values_fx(t[1])))
        .transform(lambda p: FrozenDict(dict(p)))
    )


def _infer_type(
    conf: InferenceConfig, key: str, value: JsonPrimitive
) -> ObjectPropertyType:
    if key in conf.custom_properties_types:
        return conf.custom_properties_types[key]
    return infer_item_type(value).map(
        lambda n: n.map(
            lambda d: ObjectPropertyType.from_number_type(
                NumberType.from_decimal(d)
            ),
            lambda i: ObjectPropertyType.from_number_type(
                NumberType.from_int(
                    IntType(
                        i.size
                        if i.size > conf.min_int_size
                        else conf.min_int_size
                    )
                )
            ),
            lambda f: ObjectPropertyType.from_number_type(
                NumberType.from_float(f)
            ),
        ),
        lambda s: ObjectPropertyType.from_str(
            StringType.new(
                s.meta_type,
                s.format,
                s.precision
                if s.precision >= conf.min_str_len
                else conf.min_str_len,
            ).unwrap()
        ),
        ObjectPropertyType.bool_type(),
        ObjectPropertyType.null_type(),
    )


def _append_infered_schema(
    schema: MutableSchema,
    item: FrozenDict[str, JsonPrimitive],
    infer_type: Callable[[str, JsonPrimitive], ObjectPropertyType],
) -> Cmd[None]:
    return (
        PureIterFactory.from_list(tuple(item.items()))
        .map(lambda t: schema.append(t[0], infer_type(t[0], t[1])))
        .transform(PureIterTransform.consume)
    )


def property_type_to_postfix(_type: ObjectPropertyType) -> str:
    return _type.map(
        lambda n: n.map(
            lambda _: "float",
            lambda _: "int",
            lambda _: "float",
        ),
        lambda s: "datetime"
        if s.format.map(lambda f: f is StringFormat.DATE_TIME).value_or(False)
        else "str",
        "bool",
        "null",
    )


def _select_type(
    field: str, types: frozenset[ObjectPropertyType]
) -> Tuple[str, JsonSchema]:
    _selected_type = merge_types(tuple(types))
    selected_type_name = property_type_to_postfix(_selected_type)
    new_key: str = field + "_" + selected_type_name
    return (
        new_key,
        DEFAULT_ENCODER.encode_obj_prop_type(_selected_type).unwrap(),
    )


def _to_singer_schema(
    table: str, fields: FrozenDict[str, frozenset[ObjectPropertyType]]
) -> SingerSchema:
    props = (
        PureIterFactory.from_list(tuple(fields.items()))
        .map(lambda t: _select_type(*t))
        .transform(lambda p: FrozenDict(dict(p)))
    )
    raw = {
        "properties": LegacyJsonValue(
            _map_dict(
                props, lambda k: k, lambda v: LegacyJsonValue(v.encode())
            )
        ),
    }
    j_schema = JSchemaFactory.from_json(FrozenDict(raw)).unwrap()
    return SingerSchema.new(table, j_schema, frozenset(), None).unwrap()


def infer_schemas(
    config: InferenceConfig,
    items: Stream[TableRecordPair],
) -> Cmd[PureIter[SingerSchema]]:
    _schemas = MutableSchemaMap.new()
    return _schemas.bind(
        lambda schemas: items.map(
            lambda i: schemas.get_or_create(i.table).bind(
                lambda table_schema: _append_infered_schema(
                    table_schema,
                    _map_dict(i.record, lambda k: k.raw, lambda v: v),
                    lambda k, p: _infer_type(config, k, p),
                )
            )
        ).transform(StreamTransform.consume)
        + schemas.get_full_map().map(
            lambda m: PureIterFactory.from_list(tuple(m.items())).map(
                lambda t: _to_singer_schema(*t)
            )
        )
    )
