from __future__ import (
    annotations,
)

from dataclasses import (
    dataclass,
    field,
)
from fa_purity import (
    Cmd,
    FrozenDict,
)
from fa_purity.cmd.core import (
    CmdUnwrapper,
)
from fa_purity.frozen import (
    freeze,
)
from tap_json.auto_schema.jschema.object import (
    ObjectPropertyType,
)


@dataclass(frozen=True)
class _Private:
    pass


@dataclass(frozen=True)
class MutableSchema:
    _private: _Private = field(repr=False, hash=False, compare=False)
    _data: dict[str, frozenset[ObjectPropertyType]]

    @staticmethod
    def new() -> Cmd[MutableSchema]:
        return Cmd.from_cmd(lambda: MutableSchema(_Private(), {}))

    def append(self, key: str, _type: ObjectPropertyType) -> Cmd[None]:
        def _action() -> None:
            if key not in self._data:
                self._data[key] = frozenset([_type])
            else:
                self._data[key] = self._data[key].union([_type])

        return Cmd.from_cmd(_action)

    def get_schema(
        self,
    ) -> Cmd[FrozenDict[str, frozenset[ObjectPropertyType]]]:
        def _action() -> FrozenDict[str, frozenset[ObjectPropertyType]]:
            return freeze(self._data)

        return Cmd.from_cmd(_action)


@dataclass(frozen=True)
class MutableSchemaMap:
    _private: _Private = field(repr=False, hash=False, compare=False)
    _data: dict[str, MutableSchema]

    @staticmethod
    def new() -> Cmd[MutableSchemaMap]:
        return Cmd.from_cmd(lambda: MutableSchemaMap(_Private(), {}))

    def get_or_create(self, table: str) -> Cmd[MutableSchema]:
        def _action(unwrapper: CmdUnwrapper) -> MutableSchema:
            if table not in self._data:
                self._data[table] = unwrapper.act(MutableSchema.new())
            return self._data[table]

        return Cmd.new_cmd(_action)

    def append(
        self, table: str, key: str, _type: ObjectPropertyType
    ) -> Cmd[None]:
        return self.get_or_create(table).bind(lambda ms: ms.append(key, _type))

    def get_map(self) -> Cmd[FrozenDict[str, MutableSchema]]:
        def _action() -> FrozenDict[str, MutableSchema]:
            return freeze(self._data)

        return Cmd.from_cmd(_action)

    def get_full_map(
        self,
    ) -> Cmd[FrozenDict[str, FrozenDict[str, frozenset[ObjectPropertyType]]]]:
        def _action(
            schemas: FrozenDict[str, MutableSchema], unwrapper: CmdUnwrapper
        ) -> FrozenDict[str, FrozenDict[str, frozenset[ObjectPropertyType]]]:
            return FrozenDict(
                {k: unwrapper.act(v.get_schema()) for k, v in schemas.items()}
            )

        return self.get_map().bind(
            lambda d: Cmd.new_cmd(lambda u: _action(d, u))
        )
