from fa_purity import (
    Maybe,
    Result,
    ResultE,
)
from fa_purity.json_2 import (
    JsonPrimitive,
)
from fa_purity.utils import (
    cast_exception,
)
from tap_json import (
    cast,
)
from tap_json.auto_schema.jschema.number import (
    FloatType,
    IntSizes,
    IntType,
    NumberType,
    NumSizes,
)
from tap_json.auto_schema.jschema.object import (
    ObjectPropertyType,
)
from tap_json.auto_schema.jschema.string import (
    MetaType,
    StringFormat,
    StringType,
)


def _infer_int_type(value: int) -> ResultE[IntType]:
    if value in range(-32768, 32768):
        return Result.success(IntType(IntSizes.SMALL))
    if value in range(-2147483648, 2147483648):
        return Result.success(IntType(IntSizes.NORMAL))
    if value in range(-9223372036854775808, 9223372036854775807):
        return Result.success(IntType(IntSizes.BIG))
    return Result.failure(
        ValueError("Int value is to big to fit in a big integer (8 bytes)"),
        IntType,
    ).alt(cast_exception)


def infer_item_type(
    item: JsonPrimitive,
) -> ObjectPropertyType:
    """Return the python type of a structure."""
    if cast.primitive_to_date(item).map(lambda _: True).value_or(False):
        return ObjectPropertyType.from_str(
            StringType.new(
                MetaType.DYNAMIC, Maybe.from_value(StringFormat.DATE_TIME), 256
            ).unwrap()
        )
    return item.map(
        lambda s: StringType.new(MetaType.DYNAMIC, Maybe.empty(), len(s))
        .map(ObjectPropertyType.from_str)
        .value_or(ObjectPropertyType.null_type()),
        lambda i: _infer_int_type(i)
        .map(NumberType.from_int)
        .map(ObjectPropertyType.from_number_type)
        .value_or(
            ObjectPropertyType.from_str(
                StringType.new(MetaType.DYNAMIC, Maybe.empty(), 256).unwrap()
            )
        ),
        lambda _: ObjectPropertyType.from_number_type(
            NumberType.from_float(FloatType(NumSizes.FLOAT))
        ),
        lambda _: ObjectPropertyType.from_number_type(
            NumberType.from_float(FloatType(NumSizes.FLOAT))
        ),
        lambda _: ObjectPropertyType.bool_type(),
        lambda: ObjectPropertyType.null_type(),
    )
