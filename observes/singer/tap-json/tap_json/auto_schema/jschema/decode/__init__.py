from . import (
    _number,
    _object,
    _string,
)
from collections.abc import (
    Callable,
)
from dataclasses import (
    dataclass,
)
from fa_purity import (
    ResultE,
)
from fa_purity.json_2 import (
    JsonObj,
)
from fa_singer_io.singer import (
    SingerSchema,
)
from tap_json.auto_schema.jschema.number import (
    NumberType,
)
from tap_json.auto_schema.jschema.object import (
    ObjectPropertyType,
    ObjectType,
)
from tap_json.auto_schema.jschema.string import (
    StringType,
)


@dataclass(frozen=True)
class TypeDecoder:
    decode_number: Callable[[JsonObj], ResultE[NumberType]]
    decode_string_type: Callable[[JsonObj], ResultE[StringType]]
    decode_obj_prop_type: Callable[[JsonObj], ResultE[ObjectPropertyType]]
    decode_obj_type: Callable[[SingerSchema], ResultE[ObjectType]]


DEFAULT_DECODER = TypeDecoder(
    _number.decode_number,
    _string.decode_string_type,
    _object.decode_obj_prop_type,
    _object.decode_obj_type,
)
