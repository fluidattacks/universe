from __future__ import (
    annotations,
)

from . import (
    _core,
)
from ._core import (
    RawType,
)
from fa_purity import (
    Result,
    ResultE,
)
from fa_purity._core.coproduct import (
    Coproduct,
)
from fa_purity.json_2 import (
    JsonObj,
    JsonPrimitiveUnfolder,
    JsonUnfolder,
    Unfolder,
)
from fa_purity.utils import (
    cast_exception,
)
from tap_json.auto_schema.jschema.number import (
    DecimalType,
    FloatType,
    IntSizes,
    IntType,
    NumberType,
    NumSizes,
)


def decode_decimal_type(encoded: JsonObj) -> ResultE[DecimalType]:
    _precision: ResultE[int] = JsonUnfolder.optional(
        encoded,
        "precision",
        lambda v: Unfolder.to_primitive(v).bind(JsonPrimitiveUnfolder.to_int),
    ).map(lambda m: m.value_or(18))
    _scale: ResultE[int] = JsonUnfolder.optional(
        encoded,
        "scale",
        lambda v: Unfolder.to_primitive(v).bind(JsonPrimitiveUnfolder.to_int),
    ).map(lambda m: m.value_or(0))
    return _precision.bind(
        lambda precision: _scale.bind(
            lambda scale: DecimalType.new(precision, scale)
        )
    )


def decode_int(encoded: JsonObj) -> ResultE[IntType]:
    _size: ResultE[IntSizes] = JsonUnfolder.optional(
        encoded,
        "size",
        lambda v: Unfolder.to_primitive(v)
        .bind(JsonPrimitiveUnfolder.to_str)
        .bind(IntSizes.from_raw),
    ).map(lambda m: m.value_or(IntSizes.NORMAL))
    return _size.map(lambda s: IntType(s))


def _decode_sized_num(
    size: NumSizes, encoded: JsonObj
) -> ResultE[Coproduct[DecimalType, FloatType]]:
    if size is NumSizes.EXACT:
        return decode_decimal_type(encoded).map(lambda x: Coproduct.inl(x))
    return Result.success(Coproduct.inr(FloatType(size)))


def _decode_number(raw: RawType, encoded: JsonObj) -> ResultE[NumberType]:
    if raw is RawType.INTEGER:
        return decode_int(encoded).map(NumberType.from_int)
    if raw is RawType.NUMBER:
        _size: ResultE[NumSizes] = JsonUnfolder.optional(
            encoded,
            "size",
            lambda v: Unfolder.to_primitive(v)
            .bind(JsonPrimitiveUnfolder.to_str)
            .bind(NumSizes.from_raw),
        ).map(lambda m: m.value_or(NumSizes.FLOAT))
        return _size.bind(
            lambda s: _decode_sized_num(s, encoded).map(
                lambda c: c.map(NumberType.from_decimal, NumberType.from_float)
            )
        )
    return Result.failure(
        TypeError("Expected int or number as raw type"), NumberType
    ).alt(cast_exception)


def decode_number(encoded: JsonObj) -> ResultE[NumberType]:
    _type: ResultE[RawType] = JsonUnfolder.require(
        encoded,
        "type",
        _core.decode_type,
    )
    return _type.bind(lambda t: _decode_number(t, encoded))
