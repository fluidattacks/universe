from __future__ import (
    annotations,
)

from . import (
    _core,
)
from ._core import (
    RawType,
)
from ._number import (
    decode_number,
)
from ._string import (
    decode_string_type,
)
from fa_purity import (
    FrozenDict,
    Result,
    ResultE,
)
from fa_purity.json_2 import (
    JsonObj,
    JsonUnfolder,
    LegacyAdapter,
    Unfolder,
)
from fa_purity.pure_iter.factory import (
    PureIterFactory,
)
from fa_purity.result.transform import (
    all_ok,
)
from fa_purity.utils import (
    cast_exception,
)
from fa_singer_io.singer import (
    SingerSchema,
)
from tap_json.auto_schema.jschema.object import (
    ObjectPropertyType,
    ObjectType,
)


def _ensure_bool_type(raw: RawType) -> ResultE[RawType]:
    if raw is RawType.BOOLEAN:
        return Result.success(raw)
    return Result.failure(ValueError("Expected a str type"), RawType).alt(
        cast_exception
    )


def decode_bool_type(encoded: JsonObj) -> ResultE[ObjectPropertyType]:
    _type: ResultE[RawType] = JsonUnfolder.require(
        encoded,
        "type",
        _core.decode_type,
    ).bind(_ensure_bool_type)
    return _type.map(lambda _: ObjectPropertyType.bool_type())


def _ensure_null_type(raw: RawType) -> ResultE[RawType]:
    if raw is RawType.NULL:
        return Result.success(raw)
    return Result.failure(ValueError("Expected a str type"), RawType).alt(
        cast_exception
    )


def decode_null_type(encoded: JsonObj) -> ResultE[ObjectPropertyType]:
    _type: ResultE[RawType] = JsonUnfolder.require(
        encoded,
        "type",
        _core.decode_type,
    ).bind(_ensure_null_type)
    return _type.map(lambda _: ObjectPropertyType.null_type())


def decode_obj_prop_type(encoded: JsonObj) -> ResultE[ObjectPropertyType]:
    return (
        decode_number(encoded)
        .map(ObjectPropertyType.from_number_type)
        .lash(
            lambda _: decode_string_type(encoded).map(
                ObjectPropertyType.from_str
            )
        )
        .lash(lambda _: decode_bool_type(encoded))
        .lash(lambda _: decode_null_type(encoded))
    )


def _transform_properties(
    properties: JsonObj,
) -> ResultE[ObjectType]:
    return (
        PureIterFactory.from_list(tuple(properties.items()))
        .map(
            lambda t: Unfolder.to_json(t[1])
            .bind(decode_obj_prop_type)
            .map(lambda c: (t[0], c))
        )
        .transform(lambda p: all_ok(p.to_list()))
    ).map(lambda i: ObjectType(FrozenDict(dict(i))))


def decode_obj_type(
    schema: SingerSchema,
) -> ResultE[ObjectType]:
    encoded = schema.schema.encode()
    _properties = JsonUnfolder.require(
        LegacyAdapter.json(encoded), "properties", Unfolder.to_json
    )
    return _properties.bind(_transform_properties)
