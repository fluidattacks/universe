from __future__ import (
    annotations,
)

from dataclasses import (
    dataclass,
    field,
)
from enum import (
    Enum,
)
from fa_purity import (
    Maybe,
    Result,
    ResultE,
)
from fa_purity.utils import (
    cast_exception,
)


class MetaType(Enum):
    STATIC = "static"
    DYNAMIC = "dynamic"

    @staticmethod
    def from_raw(raw: str) -> ResultE[MetaType]:
        try:
            return Result.success(MetaType(raw.lower()))
        except ValueError as err:
            return Result.failure(err, MetaType).alt(Exception)


class StringFormat(Enum):
    DATE_TIME = "date-time"
    TIME = "time"
    DATE = "date"

    @staticmethod
    def from_raw(raw: str) -> ResultE[StringFormat]:
        try:
            return Result.success(StringFormat(raw.lower()))
        except ValueError as err:
            return Result.failure(err, StringFormat).alt(Exception)


@dataclass(frozen=True)
class StringType:
    _private: StringType._Private = field(
        repr=False, hash=False, compare=False
    )
    meta_type: MetaType
    format: Maybe[StringFormat]
    precision: int

    @dataclass(frozen=True)
    class _Private:
        pass

    @staticmethod
    def new(
        meta_type: MetaType,
        _format: Maybe[StringFormat],
        precision: int,
    ) -> ResultE[StringType]:
        if precision > 0:
            return Result.success(
                StringType(
                    StringType._Private(), meta_type, _format, precision
                )
            )
        return Result.failure(ValueError("precision"), StringType).alt(
            cast_exception
        )
