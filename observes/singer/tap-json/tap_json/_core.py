from __future__ import (
    annotations,
)

from dataclasses import (
    dataclass,
)
from fa_purity import (
    FrozenDict,
)
from fa_purity.json_2 import (
    JsonPrimitive,
)
import re


@dataclass(frozen=True)
class _Private:
    pass


@dataclass(frozen=True)
class CleanString:
    _private: _Private
    raw: str

    @staticmethod
    def new(raw: str) -> CleanString:
        """Clean invalid chars from a string."""
        return CleanString(_Private(), re.sub(r"[^ _a-zA-Z0-9]", "", raw))

    def __lt__(self, other: CleanString) -> bool:
        return self.raw < other.raw


FlatRecord = FrozenDict[CleanString, JsonPrimitive]


@dataclass(frozen=True)
class TableRecordPair:
    table: str
    record: FlatRecord
