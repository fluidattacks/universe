from ._core import (
    PreFlatJsonValue,
)
from ._nested_id import (
    struct_hash,
)
from collections.abc import (
    Callable,
)
from dataclasses import (
    dataclass,
)
from fa_purity import (
    Maybe,
    PureIter,
)
from fa_purity.frozen import (
    freeze,
    FrozenDict,
    FrozenList,
)
from fa_purity.json_2 import (
    JsonPrimitive,
)
from fa_purity.pure_iter import (
    PureIterFactory,
)
from fa_purity.union import (
    Coproduct,
)
from tap_json._core import (
    CleanString,
    FlatRecord,
    TableRecordPair,
)
from tap_json._utils.dict import (
    merge as merge_dict,
)
from tap_json._utils.iter import (
    append as append_iter,
)
from typing import (
    Tuple,
)

TABLE_SEP: str = "____"


def _list_encode(
    index: int,
    max_index: int,
    value: PreFlatJsonValue,
    ids: FrozenList[str],
) -> FrozenDict[
    CleanString, Coproduct[FrozenList[PreFlatJsonValue], JsonPrimitive]
]:
    wrapped_value: FrozenDict[
        CleanString, Coproduct[FrozenList[PreFlatJsonValue], JsonPrimitive]
    ] = value.map(
        lambda v: freeze({CleanString.new("val"): Coproduct.inr(v)}),
        lambda v: freeze({CleanString.new("val"): Coproduct.inl(v)}),
        lambda v: v,
    )
    metadata: dict[
        CleanString, Coproduct[FrozenList[PreFlatJsonValue], JsonPrimitive]
    ] = {}
    for lvl, this_id in enumerate(ids):
        metadata[CleanString.new(f"sid{lvl}")] = Coproduct.inr(
            JsonPrimitive.from_str(this_id)
        )
        metadata[CleanString.new("forward_index")] = Coproduct.inr(
            JsonPrimitive.from_int(index)
        )
        metadata[CleanString.new("backward_index")] = Coproduct.inr(
            JsonPrimitive.from_int(max_index - 1 - index)
        )
    return freeze(dict(wrapped_value) | metadata)


_ToTablesFunction = Callable[
    [str, PreFlatJsonValue, FrozenList[str]],
    PureIter[TableRecordPair],
]


def _merge(
    prev: Maybe[Tuple[FlatRecord, PureIter[TableRecordPair]]],
    current: Tuple[FlatRecord, PureIter[TableRecordPair]],
) -> Maybe[Tuple[FlatRecord, PureIter[TableRecordPair]]]:
    result = prev.map(
        lambda p: (merge_dict(p[0], current[0]), append_iter(p[1], current[1]))
    ).value_or(current)
    return Maybe.from_value(result)


@dataclass(frozen=True)
class _ToTableRecordsTransform:
    table: str
    ids: FrozenList[str]
    _to_table: _ToTablesFunction

    def list_case(
        self,
        items: FrozenList[PreFlatJsonValue],
    ) -> PureIter[TableRecordPair]:
        return (
            PureIterFactory.from_list(items)
            .enumerate(0)
            .map(lambda t: _list_encode(t[0], len(items), t[1], self.ids))
            .map(PreFlatJsonValue.from_dict)
            .map(lambda j: self._to_table(self.table, j, self.ids))
            .bind(lambda x: x)
        )

    def _key_val_to_table(
        self,
        key: CleanString,
        value: Coproduct[FrozenList[PreFlatJsonValue], JsonPrimitive],
    ) -> Tuple[FlatRecord, PureIter[TableRecordPair]]:
        def _list_case(
            item: FrozenList[PreFlatJsonValue],
        ) -> Tuple[FlatRecord, PureIter[TableRecordPair]]:
            ref_id = struct_hash(PreFlatJsonValue.from_list(item))
            new_table = f"{self.table}{TABLE_SEP}{key.raw}"
            ref: FlatRecord = freeze(
                {CleanString.new(new_table): JsonPrimitive.from_str(ref_id)}
            )
            new_ids = self.ids + (ref_id,)
            items = self._to_table(
                new_table, PreFlatJsonValue.from_list(item), new_ids
            )
            return (ref, items)

        empty: PureIter[TableRecordPair] = PureIterFactory.from_list([])
        return value.map(
            _list_case,
            lambda p: (FrozenDict({key: p}), empty),
        )

    def dict_case(
        self,
        items: FrozenDict[
            CleanString, Coproduct[FrozenList[PreFlatJsonValue], JsonPrimitive]
        ],
    ) -> PureIter[TableRecordPair]:
        empty: Maybe[
            Tuple[FlatRecord, PureIter[TableRecordPair]]
        ] = Maybe.empty()
        empty_2: PureIter[TableRecordPair] = PureIterFactory.from_list([])
        return (
            (
                PureIterFactory.from_list(tuple(items.items()))
                .map(lambda t: self._key_val_to_table(t[0], t[1]))
                .reduce(_merge, empty)
            )
            .map(
                lambda t: append_iter(
                    PureIterFactory.from_list(
                        [TableRecordPair(self.table, t[0])]
                    ),
                    t[1],
                )
            )
            .value_or(empty_2)
        )

    def primitive_case(
        self,
        item: JsonPrimitive,
    ) -> PureIter[TableRecordPair]:
        return self._to_table(
            self.table,
            PreFlatJsonValue.from_list(
                tuple([PreFlatJsonValue.from_primitive(item)])
            ),
            self.ids,
        )


def to_table_records(
    table: str,
    value: PreFlatJsonValue,
    ids: FrozenList[str],
) -> PureIter[TableRecordPair]:
    transform = _ToTableRecordsTransform(table, ids, to_table_records)
    return value.map(
        transform.primitive_case,
        transform.list_case,
        transform.dict_case,
    )
