from ._core import (
    PreFlatJsonValue,
)
from collections.abc import (
    Callable,
)
from fa_purity import (
    FrozenDict,
    FrozenList,
)
from fa_purity.frozen import (
    freeze,
)
from fa_purity.json_2 import (
    JsonObj,
    JsonPrimitive,
    JsonValue,
)
from fa_purity.union import (
    Coproduct,
)
from tap_json._core import (
    CleanString,
)
from typing import (
    TypeVar,
)

FIELD_SEP: str = "__"

_T = TypeVar("_T")
_K = TypeVar("_K")
_V = TypeVar("_V")


def _prefix_key(
    prefix: str, items: FrozenDict[str, _T]
) -> FrozenDict[str, _T]:
    return FrozenDict({prefix + k: v for k, v in items.items()})


def _merge_dicts(items: FrozenList[FrozenDict[_K, _V]]) -> FrozenDict[_K, _V]:
    result: dict[_K, _V] = {}
    for i in items:
        result = result | dict(i)
    return freeze(result)


def _extract_dict(
    key: str,
    value: JsonValue,
    transform: Callable[[JsonValue], PreFlatJsonValue],
) -> FrozenDict[
    CleanString, Coproduct[FrozenList[PreFlatJsonValue], JsonPrimitive]
]:
    return value.map(
        lambda x: FrozenDict({CleanString.new(key): Coproduct.inr(x)}),
        lambda items: FrozenDict(
            {
                CleanString.new(key): Coproduct.inl(
                    tuple(transform(i) for i in items)
                )
            }
        ),
        lambda x: _merge_dicts(
            tuple(
                _extract_dict(k, v, transform)
                for k, v in _prefix_key(key + FIELD_SEP, x).items()
            )
        ),
    )


def flatten_nested_dict(value: JsonValue) -> PreFlatJsonValue:
    return value.map(
        PreFlatJsonValue.from_primitive,
        lambda items: PreFlatJsonValue.from_list(
            tuple(flatten_nested_dict(i) for i in items)
        ),
        lambda x: PreFlatJsonValue.from_dict(
            _merge_dicts(
                tuple(
                    _extract_dict(k, v, flatten_nested_dict)
                    for k, v in x.items()
                )
            )
        ),
    )


def simplify_json(
    data: JsonObj,
) -> PreFlatJsonValue:
    unfolded = _merge_dicts(
        tuple(
            _extract_dict(k, v, flatten_nested_dict) for k, v in data.items()
        )
    )
    return PreFlatJsonValue.from_dict(unfolded)
