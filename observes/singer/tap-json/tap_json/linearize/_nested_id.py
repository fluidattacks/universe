from ._core import (
    PreFlatJsonValue,
)
from collections.abc import (
    Callable,
)
from fa_purity import (
    FrozenDict,
    FrozenList,
)
from fa_purity.json_2 import (
    JsonPrimitive,
)
from fa_purity.union import (
    Coproduct,
)
import hashlib
from tap_json._core import (
    CleanString,
)


def _data_hash(data: JsonPrimitive) -> str:
    hash_id = hashlib.sha256()
    raw = data.map(
        str,
        str,
        str,
        str,
        str,
        lambda: str(None),
    )
    hash_id.update(bytes(raw, "utf-8"))
    return hash_id.hexdigest()


def _list_hash(
    data: FrozenList[PreFlatJsonValue],
    hash_fx: Callable[[PreFlatJsonValue], str],
) -> str:
    hash_id = hashlib.sha256()
    for item in data:
        hash_id.update(bytes(hash_fx(item), "utf-8"))
    return hash_id.hexdigest()


def _dict_hash(
    data: FrozenDict[
        CleanString, Coproduct[FrozenList[PreFlatJsonValue], JsonPrimitive]
    ],
    hash_fx: Callable[[PreFlatJsonValue], str],
) -> str:
    hash_id = hashlib.sha256()
    for key, val in sorted(data.items()):
        hash_id.update(
            bytes(_data_hash(JsonPrimitive.from_str(key.raw)), "utf-8")
        )
        hash_val = val.map(lambda v: _list_hash(v, hash_fx), _data_hash)
        hash_id.update(bytes(hash_val, "utf-8"))
    return hash_id.hexdigest()


def struct_hash(value: PreFlatJsonValue) -> str:
    return value.map(
        lambda x: _data_hash(x),
        lambda x: _list_hash(x, struct_hash),
        lambda x: _dict_hash(x, struct_hash),
    )
