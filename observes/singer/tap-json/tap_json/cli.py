from . import (
    emitter,
)
import click
from fa_purity import (
    Cmd,
    FrozenDict,
    PureIter,
    ResultE,
)
from fa_purity.json_2 import (
    JsonValueFactory,
    LegacyAdapter,
)
from fa_purity.pure_iter import (
    PureIterFactory,
)
from fa_purity.result import (
    ResultFactory,
)
from fa_purity.result.transform import (
    all_ok,
)
from fa_purity.utils import (
    cast_exception,
)
from fa_singer_io.singer import (
    SingerSchema,
)
from fa_singer_io.singer.deserializer import (
    deserialize,
)
from tap_json._utils.singer import (
    handle_singer,
)
from tap_json.auto_schema.config import (
    decode_config,
    InferenceConfig,
)
from typing import (
    IO,
    NoReturn,
)


def _decode_conf(schema: IO[str] | None) -> InferenceConfig:
    "[WARNING] Assumes file is read-only"
    if schema:
        return JsonValueFactory.load(schema).bind(decode_config).unwrap()
    return decode_config(FrozenDict({})).unwrap()


@click.command(
    help=(
        "Emit the inferred schema from raw json data. "
        "The input data should be formated as singer records."
    )
)
@click.option(
    "--inference-conf",
    type=click.File("r", "utf-8"),
    default=None,
    help="`InferenceConfig` object encoded as JSON file.",
)
def only_emit_schema(inference_conf: IO[str] | None) -> NoReturn:
    cmd: Cmd[None] = emitter.only_emit_schema(_decode_conf(inference_conf))
    cmd.compute()


def _decode_schemas(schema: IO[str]) -> ResultE[PureIter[SingerSchema]]:
    "[WARNING] Assumes file is read-only"
    _factory: ResultFactory[SingerSchema, Exception] = ResultFactory()
    items = (
        PureIterFactory.from_list(schema.readlines())
        .map(
            lambda s: JsonValueFactory.loads(s)
            .map(LegacyAdapter.to_legacy_json)
            .bind(deserialize)
        )
        .map(
            lambda r: r.bind(
                lambda m: handle_singer(
                    m,
                    lambda _: _factory.failure(
                        TypeError("Expected only SingerSchema not records")
                    ).alt(cast_exception),
                    lambda s: _factory.success(s),
                    lambda _: _factory.failure(
                        TypeError("Expected only SingerSchema not states")
                    ).alt(cast_exception),
                )
            )
        )
    )
    return all_ok(items.to_list()).map(PureIterFactory.from_list)


@click.command(
    help="Emit the inferred schema from raw json data. And emit adjusted flat records."
)
@click.option(
    "--schemas",
    type=click.File("r", "utf-8"),
    default=None,
    help=(
        "JSON-lines file that has the inferred schema of a previous execution. "
        "Provide it to avoid re inferring it from data."
    ),
)
@click.option(
    "--inference-conf",
    type=click.File("r", "utf-8"),
    default=None,
    help="`InferenceConfig` object encoded as JSON file. Only apply if `schemas` is not supplied.",
)
def infer_and_adjust(
    schemas: IO[str] | None, inference_conf: IO[str] | None
) -> NoReturn:
    cmd: Cmd[None] = (
        emitter.no_schema_inference(_decode_schemas(schemas).unwrap())
        if schemas is not None
        else emitter.full_execution(_decode_conf(inference_conf))
    )
    cmd.compute()


@click.group()
def main() -> None:
    # main cli group
    pass


main.add_command(only_emit_schema)
main.add_command(infer_and_adjust)
