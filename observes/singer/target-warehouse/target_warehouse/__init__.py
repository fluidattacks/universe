from ._logger import (
    set_logger,
)
from fa_purity import (
    Unsafe,
)

__version__ = "2.0.0"

Unsafe.compute(set_logger(__name__, __version__))
