from dataclasses import (
    dataclass,
)
from etl_utils.bug import (
    Bug,
)
from fa_purity import (
    Cmd,
    PureIter,
    Stream,
    Unsafe,
)
from fa_singer_io.singer import (
    SingerMessage,
)
from fa_singer_io.singer.deserializer import (
    from_file_ignore_failed,
    try_from_file,
)
import inspect
from io import (
    TextIOWrapper,
)
import sys
from typing import (
    IO,
)


@dataclass(frozen=True)
class InputEmitter:
    ignore_failed: bool

    @property
    def input_stream(self) -> Stream[SingerMessage]:
        def deserializer(file: IO[str]) -> PureIter[SingerMessage]:
            if self.ignore_failed:
                return from_file_ignore_failed(file)
            return try_from_file(file).map(
                lambda r: Bug.assume_success(
                    "input_singer_decode_from_file",
                    inspect.currentframe(),
                    tuple(),
                    r,
                )
            )

        cmd = Cmd.wrap_impure(
            lambda: TextIOWrapper(sys.stdin.buffer, encoding="utf-8")  # type: ignore[misc]
        )
        return Unsafe.stream_from_cmd(
            cmd.map(deserializer).map(lambda x: iter(x))
        )
