"""
This module exposes:
- a `client` submodule that defines the `ApiClient` interface
- a `core` submodule that defines the Api objects
- a function `new_client` that implements and return an `ApiClient`
"""
from ._client_1 import (
    new_client,
)

__all__ = [
    "new_client",
]
