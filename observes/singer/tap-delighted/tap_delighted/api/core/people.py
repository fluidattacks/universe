from .ids import (
    PersonId,
)
from dataclasses import (
    dataclass,
)
from fa_purity import (
    Maybe,
)
from tap_delighted._typing import (
    Tuple,
)


@dataclass(frozen=True)
class BouncedPerson:
    person_id: PersonId
    email: str
    name: Maybe[str]
    bounced_at: int


@dataclass(frozen=True)
class UnsubscribedPerson:
    person_id: PersonId
    email: str
    name: str
    unsubscribed_at: int


@dataclass(frozen=True)
class Person:
    email: str
    name: Maybe[str]
    phone_number: Maybe[str]
    created_at: int
    last_sent_at: Maybe[int]
    last_responded_at: Maybe[int]
    next_survey_scheduled_at: Maybe[int]


PersonObj = Tuple[PersonId, Person]
