from .ids import (
    PersonId,
    SurveyId,
)
from dataclasses import (
    dataclass,
)
from tap_delighted._typing import (
    Tuple,
)


@dataclass(frozen=True)
class Metrics:
    nps: int
    promoter_count: int
    promoter_percent: float
    passive_count: int
    passive_percent: float
    detractor_count: int
    detractor_percent: float
    response_count: int


@dataclass(frozen=True)
class SurveyPersonProperties:
    company: str
    visitor_type: str
    page: str
    page_url: str
    device_type: str
    operating_system: str
    browser: str
    country: str
    source: str
    referrer_url: str


@dataclass(frozen=True)
class SurveyResponse:
    person: PersonId
    survey_type: str
    score: int
    permalink: str
    created_at: int
    updated_at: int
    comment: str
    properties: SurveyPersonProperties


SurveyObj = Tuple[SurveyId, SurveyResponse]
