from __future__ import (
    annotations,
)

from dataclasses import (
    dataclass,
)
from fa_purity.json_2 import (
    JsonValue,
    Unfolder,
)
from fa_purity.pure_iter import (
    PureIterFactory,
)
from fa_purity.union import (
    Coproduct,
    CoproductFactory,
)
from tap_delighted._typing import (
    Generic,
    TypeVar,
)

_T = TypeVar("_T")


def cast_exception(err: Exception) -> Exception:
    return err


def print_json_value(value: JsonValue) -> str:
    return value.map(
        lambda p: p.map(
            lambda x: x,
            lambda x: str(x),
            lambda x: str(x),
            lambda x: str(x),
            lambda x: str(x),
            lambda: str(None),
        ),
        lambda i: str(
            PureIterFactory.from_list(i).map(print_json_value).to_list()
        ),
        Unfolder.dumps,
    )


@dataclass
class ApiBug(Exception, Generic[_T]):
    module: str
    name: str
    parent: Exception | None
    input_data: Coproduct[_T, JsonValue]

    @staticmethod
    def new_generic(
        module: str,
        name: str,
        parent: Exception | None,
        input_data: _T,
    ) -> ApiBug[_T]:
        factory: CoproductFactory[_T, JsonValue] = CoproductFactory()
        return ApiBug(module, name, parent, factory.inl(input_data))

    @staticmethod
    def new_json_input(
        module: str,
        name: str,
        parent: Exception | None,
        input_data: JsonValue,
    ) -> ApiBug[None]:
        factory: CoproductFactory[None, JsonValue] = CoproductFactory()
        return ApiBug(module, name, parent, factory.inr(input_data))

    def __str__(self) -> str:
        return (
            self.module
            + " > "
            + self.name
            + "\n i.e. "
            + str(self.parent)
            + "\n @ "
            + self.input_data.map(str, print_json_value)
        )
