from ._common import (
    optional_json,
    optional_str,
    require_int,
    require_str,
)
from dataclasses import (
    dataclass,
)
from fa_purity import (
    Maybe,
    Result,
    ResultE,
)
from fa_purity.json_2 import (
    JsonObj,
    JsonValue,
)
from tap_delighted.api._utils import (
    ApiBug,
)
from tap_delighted.api.core import (
    SurveyObj,
    SurveyPersonProperties,
    SurveyResponse,
)
from tap_delighted.api.core.ids import (
    PersonId,
    SurveyId,
)


@dataclass(frozen=True)
class SurveyDecoder:
    @staticmethod
    def decode_properties(raw: JsonObj) -> ResultE[SurveyPersonProperties]:
        return optional_str(raw, "company").bind(
            lambda company: optional_str(raw, "Delighted Visitor Type").bind(
                lambda visitor_type: optional_str(raw, "Delighted Page").bind(
                    lambda page: optional_str(raw, "Delighted Page URL").bind(
                        lambda page_url: optional_str(
                            raw, "Delighted Device Type"
                        ).bind(
                            lambda device_type: optional_str(
                                raw, "Delighted Operating System"
                            ).bind(
                                lambda operating_system: optional_str(
                                    raw, "Delighted Browser"
                                ).bind(
                                    lambda browser: optional_str(
                                        raw, "Delighted Country"
                                    ).bind(
                                        lambda country: optional_str(
                                            raw, "Delighted Source"
                                        ).bind(
                                            lambda source: optional_str(
                                                raw,
                                                "Delighted Referrer URL",
                                            ).map(
                                                lambda referrer_url: SurveyPersonProperties(
                                                    company.value_or(""),
                                                    visitor_type.value_or(""),
                                                    page.value_or(""),
                                                    page_url.value_or(""),
                                                    device_type.value_or(""),
                                                    operating_system.value_or(
                                                        ""
                                                    ),
                                                    browser.value_or(""),
                                                    country.value_or(""),
                                                    source.value_or(""),
                                                    referrer_url.value_or(""),
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    )
                )
            )
        )

    @classmethod
    def _decode_props(
        cls, raw: Maybe[JsonObj]
    ) -> ResultE[Maybe[SurveyPersonProperties]]:
        _empty: Maybe[SurveyPersonProperties] = Maybe.empty()
        empty: ResultE[Maybe[SurveyPersonProperties]] = Result.success(_empty)
        return raw.map(
            lambda j: cls.decode_properties(j).map(
                lambda v: Maybe.from_value(v)
            )
        ).value_or(empty)

    @classmethod
    def decode(cls, raw: JsonObj) -> ResultE[SurveyObj]:
        return (
            require_str(raw, "id")
            .bind(
                lambda _id: require_str(raw, "person").bind(
                    lambda person: optional_str(raw, "survey_type").bind(
                        lambda survey_type: require_int(raw, "score").bind(
                            lambda score: require_str(raw, "permalink").bind(
                                lambda permalink: require_int(
                                    raw,
                                    "created_at",
                                ).bind(
                                    lambda created_at: require_int(
                                        raw, "updated_at"
                                    ).bind(
                                        lambda updated_at: optional_str(
                                            raw, "comment"
                                        ).bind(
                                            lambda comment: optional_json(
                                                raw, "person_properties"
                                            ).bind(
                                                lambda properties: cls._decode_props(
                                                    properties
                                                ).map(
                                                    lambda props: (
                                                        SurveyId(_id),
                                                        SurveyResponse(
                                                            PersonId(person),
                                                            survey_type.value_or(
                                                                ""
                                                            ),
                                                            score,
                                                            permalink,
                                                            created_at,
                                                            updated_at,
                                                            comment.value_or(
                                                                ""
                                                            ),
                                                            props.value_or(
                                                                SurveyPersonProperties(
                                                                    "",
                                                                    "",
                                                                    "",
                                                                    "",
                                                                    "",
                                                                    "",
                                                                    "",
                                                                    "",
                                                                    "",
                                                                    "",
                                                                )
                                                            ),
                                                        ),
                                                    )
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    )
                )
            )
            .alt(
                lambda e: ApiBug.new_json_input(
                    __file__,
                    "SurveyDecoder.decode",
                    e,
                    JsonValue.from_json(raw),
                )
            )
        )
