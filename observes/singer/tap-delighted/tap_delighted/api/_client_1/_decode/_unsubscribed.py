from ._common import (
    require_int,
    require_str,
)
from dataclasses import (
    dataclass,
)
from fa_purity import (
    ResultE,
)
from fa_purity.json_2 import (
    JsonObj,
    JsonValue,
)
from tap_delighted.api._utils import (
    ApiBug,
)
from tap_delighted.api.core.ids import (
    PersonId,
)
from tap_delighted.api.core.people import (
    UnsubscribedPerson,
)


@dataclass(frozen=True)
class UnsubscribedDecoder:
    @staticmethod
    def decode(raw: JsonObj) -> ResultE[UnsubscribedPerson]:
        return (
            require_str(raw, "person_id")
            .bind(
                lambda person_id: require_str(raw, "email").bind(
                    lambda email: require_str(raw, "name").bind(
                        lambda name: require_int(raw, "unsubscribed_at").map(
                            lambda unsubscribed_at: UnsubscribedPerson(
                                PersonId(person_id),
                                email,
                                name,
                                unsubscribed_at,
                            )
                        )
                    )
                )
            )
            .alt(
                lambda e: ApiBug.new_json_input(
                    __file__,
                    "UnsubscribedDecoder.decode",
                    e,
                    JsonValue.from_json(raw),
                )
            )
        )
