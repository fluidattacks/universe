from ._common import (
    optional_int,
    optional_str,
    require_int,
    require_str,
)
from dataclasses import (
    dataclass,
)
from fa_purity import (
    ResultE,
)
from fa_purity.json_2 import (
    JsonObj,
    JsonValue,
)
from tap_delighted.api._utils import (
    ApiBug,
)
from tap_delighted.api.core.ids import (
    PersonId,
)
from tap_delighted.api.core.people import (
    Person,
    PersonObj,
)


@dataclass(frozen=True)
class PersonDecoder:
    @classmethod
    def decode(cls, raw: JsonObj) -> ResultE[PersonObj]:
        return (
            require_str(raw, "id")
            .bind(
                lambda _id: require_str(raw, "email").bind(
                    lambda email: optional_str(raw, "name").bind(
                        lambda name: optional_str(raw, "phone_number").bind(
                            lambda phone_number: require_int(
                                raw, "created_at"
                            ).bind(
                                lambda created_at: optional_int(
                                    raw,
                                    "last_sent_at",
                                ).bind(
                                    lambda last_sent_at: optional_int(
                                        raw, "last_responded_at"
                                    ).bind(
                                        lambda last_responded_at: optional_int(
                                            raw, "next_survey_scheduled_at"
                                        ).map(
                                            lambda next_survey_scheduled_at: (
                                                PersonId(_id),
                                                Person(
                                                    email,
                                                    name,
                                                    phone_number,
                                                    created_at,
                                                    last_sent_at,
                                                    last_responded_at,
                                                    next_survey_scheduled_at,
                                                ),
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    )
                )
            )
            .alt(
                lambda e: ApiBug.new_json_input(
                    __file__,
                    "PersonDecoder.decode",
                    e,
                    JsonValue.from_json(raw),
                )
            )
        )
