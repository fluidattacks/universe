from ._common import (
    optional_str,
    require_int,
    require_str,
)
from dataclasses import (
    dataclass,
)
from fa_purity import (
    ResultE,
)
from fa_purity.json_2 import (
    JsonObj,
    JsonValue,
)
from tap_delighted.api._utils import (
    ApiBug,
)
from tap_delighted.api.core.ids import (
    PersonId,
)
from tap_delighted.api.core.people import (
    BouncedPerson,
)


@dataclass(frozen=True)
class BouncedDecoder:
    @staticmethod
    def decode(raw: JsonObj) -> ResultE[BouncedPerson]:
        return (
            require_str(raw, "person_id")
            .bind(
                lambda person_id: require_str(raw, "email").bind(
                    lambda email: optional_str(raw, "name").bind(
                        lambda name: require_int(raw, "bounced_at").map(
                            lambda bounced_at: BouncedPerson(
                                PersonId(person_id), email, name, bounced_at
                            )
                        )
                    )
                )
            )
            .alt(
                lambda e: ApiBug.new_json_input(
                    __file__,
                    "BouncedDecoder.decode",
                    e,
                    JsonValue.from_json(raw),
                )
            )
        )
