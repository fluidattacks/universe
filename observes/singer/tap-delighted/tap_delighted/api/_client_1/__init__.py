"`ApiClient` implementation"

from __future__ import (
    annotations,
)

import logging
import re
from dataclasses import (
    dataclass,
)
from urllib.parse import (
    parse_qs,
    urlparse,
)

from fa_purity import (
    Cmd,
    Maybe,
    Result,
    ResultE,
    Stream,
)
from fa_purity.cmd import (
    unsafe_unwrap,
)
from fa_purity.frozen import (
    FrozenList,
    freeze,
)
from fa_purity.json_2 import (
    JsonObj,
    JsonValue,
    UnfoldedFactory,
)
from fa_purity.pure_iter.factory import (
    PureIterFactory,
    from_flist,
    infinite_range,
)
from fa_purity.result import (
    ResultFactory,
)
from fa_purity.result.transform import (
    all_ok,
)
from fa_purity.stream.factory import (
    from_piter,
    unsafe_from_cmd,
)
from fa_purity.stream.transform import (
    chain,
    until_none,
)
from fa_purity.union import (
    Coproduct,
)
from fa_purity.utils import (
    raise_exception,
)
from pure_requests.basic import (
    Authentication,
    AuthMethod,
    Endpoint,
    HttpClient,
    HttpClientFactory,
    Params,
)
from pure_requests.response import (
    handle_status,
    json_decode,
)
from requests import (
    Response,
)

from tap_delighted._typing import (
    Callable,
    Iterable,
    Tuple,
    TypeVar,
)
from tap_delighted.api._client_1._decode import (
    BouncedDecoder,
    MetricsDecoder,
    PersonDecoder,
    SurveyDecoder,
    UnsubscribedDecoder,
)
from tap_delighted.api._utils import (
    ApiBug,
    cast_exception,
)
from tap_delighted.api.client import (
    ApiClient,
)
from tap_delighted.api.core import (
    Metrics,
    SurveyObj,
)
from tap_delighted.api.core.people import (
    BouncedPerson,
    PersonObj,
    UnsubscribedPerson,
)
from tap_delighted.auth import (
    Credentials,
)

LOG = logging.getLogger(__name__)
_T = TypeVar("_T")


def _api_endpoint(relative: str) -> Endpoint:
    return Endpoint("https://api.delighted.com/" + relative.lstrip("/"))


def _generic_decode(
    decode: Callable[[JsonObj], ResultE[_T]],
    item: Response,
) -> FrozenList[_T]:
    def _to_list(
        item: Coproduct[JsonObj, FrozenList[JsonObj]],
    ) -> ResultE[FrozenList[_T]]:
        _factory: ResultFactory[FrozenList[_T], Exception] = ResultFactory()
        return item.map(
            lambda _: _factory.failure(TypeError("Expected a list")).alt(
                cast_exception
            ),
            lambda i: all_ok(PureIterFactory.from_list(i).map(decode).to_list()),
        )

    return (
        handle_status(item)
        .alt(cast_exception)
        .bind(lambda x: json_decode(x).alt(cast_exception).bind(_to_list))
        .alt(raise_exception)
        .to_union()
    )


def _generic_page(
    client: HttpClient,
    endpoint: Endpoint,
    decode: Callable[[JsonObj], ResultE[_T]],
    page: int,
) -> Cmd[FrozenList[_T]]:
    params: JsonObj = UnfoldedFactory.from_dict(
        {
            "per_page": 100,
            "page": page,
        }
    )
    info = Cmd.from_cmd(
        lambda: LOG.info("[API call] %s with params %s", endpoint, params)
    )
    return info + client.get(
        endpoint,
        Params(params),
    ).map(lambda x: _generic_decode(decode, x.alt(raise_exception).to_union()))


def _extract_next(item: Response) -> Maybe[str]:
    link = Maybe.from_optional(item.headers.get("link"))
    _match: Maybe[str] = link.bind(
        lambda k: Maybe.from_optional(re.match("<(.*)>;.*", k))
    ).map(
        lambda m: m.group(1)  # type: ignore[misc]
    )
    return _match.map(urlparse).map(lambda u: parse_qs(u.query)["page_info"][0])


def _generic_indexed_page(
    client: HttpClient,
    endpoint: Endpoint,
    decode: Callable[[JsonObj], ResultE[_T]],
    next_id: Maybe[str],
) -> Cmd[Tuple[FrozenList[_T], Maybe[str]]]:
    empty: JsonObj = freeze({})
    params: JsonObj = next_id.map(
        lambda n: UnfoldedFactory.from_dict(
            {
                "page_info": n,
            }
        )
    ).value_or(empty)
    info = Cmd.from_cmd(
        lambda: LOG.info("[API call] %s with params %s", endpoint, params)
    )
    return info + client.get(
        endpoint,
        Params(params),
    ).map(lambda r: r.alt(raise_exception).to_union()).map(
        lambda r: (_generic_decode(decode, r), _extract_next(r))
    )


def _generic_int_pagination(
    start: int, get_page: Callable[[int], Cmd[FrozenList[_T]]]
) -> Stream[_T]:
    return (
        infinite_range(start, 1)
        .map(get_page)
        .transform(lambda x: from_piter(x))
        .map(lambda i: i if len(i) > 0 else None)
        .transform(lambda x: until_none(x))
        .map(from_flist)
        .transform(chain)
    )


@dataclass(frozen=True)
class _ApiClient1:
    _client: HttpClient

    @staticmethod
    def new(creds: Credentials) -> _ApiClient1:
        headers = UnfoldedFactory.from_unfolded_dict(
            {"Content-Type": "application/json"}
        )
        auth = Authentication(creds.api_key, "", AuthMethod.BASIC)
        client = HttpClientFactory.new_client(auth, headers, False)
        return _ApiClient1(client)

    def get_metrics(self) -> Cmd[Metrics]:
        return self._client.get(
            _api_endpoint("v1/metrics.json"),
            Params(freeze({})),
        ).map(
            lambda r: r.alt(cast_exception)
            .map(lambda x: handle_status(x).alt(raise_exception).to_union())
            .bind(
                lambda x: json_decode(x)
                .alt(cast_exception)
                .bind(
                    lambda c: c.map(
                        MetricsDecoder.decode,
                        lambda _: Result.failure(
                            TypeError("Expected non-list"), Metrics
                        ).alt(cast_exception),
                    )
                )
            )
            .alt(raise_exception)
            .to_union()
        )

    def _unsubscribed_page(self, page: int) -> Cmd[FrozenList[UnsubscribedPerson]]:
        return _generic_page(
            self._client,
            _api_endpoint("v1/unsubscribes.json"),
            UnsubscribedDecoder.decode,
            page,
        )

    def list_all_unsubscribed(self) -> Stream[UnsubscribedPerson]:
        return _generic_int_pagination(1, self._unsubscribed_page)

    def _bounced_page(self, page: int) -> Cmd[FrozenList[BouncedPerson]]:
        return _generic_page(
            self._client,
            _api_endpoint("v1/bounces.json"),
            BouncedDecoder.decode,
            page,
        )

    def list_all_bounced(self) -> Stream[BouncedPerson]:
        return _generic_int_pagination(1, self._bounced_page)

    def _survey_page(self, page: int) -> Cmd[FrozenList[SurveyObj]]:
        return _generic_page(
            self._client,
            _api_endpoint("v1/survey_responses.json"),
            SurveyDecoder.decode,
            page,
        )

    def list_all_surveys(self) -> Stream[SurveyObj]:
        return _generic_int_pagination(1, self._survey_page)

    def _people_page(
        self, item_index: Maybe[str]
    ) -> Cmd[Tuple[FrozenList[PersonObj], Maybe[str]]]:
        return _generic_indexed_page(
            self._client,
            _api_endpoint("v1/people.json"),
            PersonDecoder.decode,
            item_index,
        )

    def list_all_people(self) -> Stream[PersonObj]:
        def _list() -> Iterable[FrozenList[PersonObj]]:
            start = True
            next_item: Maybe[str] = Maybe.empty()
            while start or next_item.map(lambda _: True).value_or(False):
                start = False
                items, next_index = unsafe_unwrap(self._people_page(next_item))
                next_item = next_index
                yield items

        cmd: Cmd[Iterable[FrozenList[PersonObj]]] = Cmd.from_cmd(lambda: _list())
        return unsafe_from_cmd(cmd).map(lambda i: from_flist(i)).transform(chain)


def new_client(creds: Credentials) -> ApiClient:
    client = _ApiClient1.new(creds)
    return ApiClient(
        client.get_metrics(),
        client.list_all_people(),
        client.list_all_unsubscribed(),
        client.list_all_bounced(),
        client.list_all_surveys(),
    )
