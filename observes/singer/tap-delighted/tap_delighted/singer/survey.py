from __future__ import (
    annotations,
)

from dataclasses import (
    dataclass,
    field,
)
from fa_purity import (
    FrozenDict,
    JsonValue,
    UnfoldedJVal,
)
from fa_purity.json.factory import (
    from_unfolded_dict,
)
from fa_purity.utils import (
    raise_exception,
)
from fa_singer_io.json_schema.factory import (
    from_json,
    from_prim_type,
)
from fa_singer_io.singer import (
    SingerRecord,
    SingerSchema,
)
from tap_delighted.api.core import (
    SurveyObj,
)
from tap_delighted.streams import (
    SupportedStreams,
)

_STREAM = SupportedStreams.SURVEY_RESPONSE.value


def _schema() -> SingerSchema:
    properties = FrozenDict(
        {
            "id_str": JsonValue(from_prim_type(str).encode()),
            "person_str": JsonValue(from_prim_type(str).encode()),
            "survey_type_str": JsonValue(from_prim_type(str).encode()),
            "score_int": JsonValue(from_prim_type(int).encode()),
            "permalink_str": JsonValue(from_prim_type(str).encode()),
            "created_at_int": JsonValue(from_prim_type(int).encode()),
            "updated_at_int": JsonValue(from_prim_type(int).encode()),
            "comment_str": JsonValue(from_prim_type(str).encode()),
            "person_properties__company_str": JsonValue(
                from_prim_type(str).encode()
            ),
            "person_properties__delighted visitor type_str": JsonValue(
                from_prim_type(str).encode()
            ),
            "person_properties__delighted device type_str": JsonValue(
                from_prim_type(str).encode()
            ),
            "person_properties__delighted operating system_str": JsonValue(
                from_prim_type(str).encode()
            ),
            "person_properties__delighted browser_str": JsonValue(
                from_prim_type(str).encode()
            ),
            "person_properties__delighted country_str": JsonValue(
                from_prim_type(str).encode()
            ),
            "person_properties__delighted source_str": JsonValue(
                from_prim_type(str).encode()
            ),
            "person_properties__delighted referrer url_str": JsonValue(
                from_prim_type(str).encode()
            ),
        }
    )

    schema = FrozenDict({"properties": JsonValue(properties)})
    return (
        SingerSchema.new(
            _STREAM,
            from_json(schema).alt(raise_exception).to_union(),
            frozenset(["id_str"]),
            None,
        )
        .alt(raise_exception)
        .to_union()
    )


@dataclass(frozen=True)
class _Private:
    pass


@dataclass(frozen=True)
class EncodedSurveyObj:
    _private: _Private = field(repr=False, hash=False, compare=False)
    record: SingerRecord

    @staticmethod
    def schema() -> SingerSchema:
        return _schema()

    @staticmethod
    def encode(item: SurveyObj) -> EncodedSurveyObj:
        encoded: FrozenDict[str, UnfoldedJVal] = FrozenDict(
            {
                "id_str": item[0].value,
                "person_str": item[1].person.value,
                "survey_type_str": item[1].survey_type,
                "score_int": item[1].score,
                "permalink_str": item[1].permalink,
                "created_at_int": item[1].created_at,
                "updated_at_int": item[1].updated_at,
                "comment_str": item[1].comment,
                "person_properties__company_str": item[1].properties.company,
                "person_properties__delighted visitor type_str": item[
                    1
                ].properties.visitor_type,
                "person_properties__delighted device type_str": item[
                    1
                ].properties.device_type,
                "person_properties__delighted operating system_str": item[
                    1
                ].properties.operating_system,
                "person_properties__delighted browser_str": item[
                    1
                ].properties.browser,
                "person_properties__delighted country_str": item[
                    1
                ].properties.country,
                "person_properties__delighted source_str": item[
                    1
                ].properties.source,
                "person_properties__delighted referrer url_str": item[
                    1
                ].properties.referrer_url,
            }
        )
        record = SingerRecord(_STREAM, from_unfolded_dict(encoded), None)
        return EncodedSurveyObj(_Private(), record)
