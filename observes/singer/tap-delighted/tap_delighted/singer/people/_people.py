from __future__ import (
    annotations,
)

from dataclasses import (
    dataclass,
    field,
)
from fa_purity import (
    FrozenDict,
    JsonValue,
    UnfoldedJVal,
)
from fa_purity.json.factory import (
    from_unfolded_dict,
)
from fa_purity.utils import (
    raise_exception,
)
from fa_singer_io.json_schema.factory import (
    from_json,
    from_prim_type,
    opt_prim_type,
)
from fa_singer_io.singer import (
    SingerRecord,
    SingerSchema,
)
from tap_delighted.api.core.people import (
    PersonObj,
)
from tap_delighted.streams import (
    SupportedStreams,
)

_STREAM = SupportedStreams.PEOPLE.value


def _schema() -> SingerSchema:
    properties = FrozenDict(
        {
            "id_str": JsonValue(from_prim_type(str).encode()),
            "email_str": JsonValue(from_prim_type(str).encode()),
            "name_str": JsonValue(opt_prim_type(str).encode()),
            "phone_number_str": JsonValue(opt_prim_type(str).encode()),
            "created_at_int": JsonValue(from_prim_type(int).encode()),
            "last_sent_at_int": JsonValue(opt_prim_type(int).encode()),
            "last_responded_at_int": JsonValue(opt_prim_type(int).encode()),
            "next_survey_scheduled_at_int": JsonValue(
                opt_prim_type(int).encode()
            ),
        }
    )
    schema = FrozenDict({"properties": JsonValue(properties)})
    return (
        SingerSchema.new(
            _STREAM,
            from_json(schema).alt(raise_exception).to_union(),
            frozenset(["id_str"]),
            None,
        )
        .alt(raise_exception)
        .to_union()
    )


@dataclass(frozen=True)
class _Private:
    pass


@dataclass(frozen=True)
class EncodedPersonObj:
    _private: _Private = field(repr=False, hash=False, compare=False)
    record: SingerRecord

    @staticmethod
    def schema() -> SingerSchema:
        return _schema()

    @staticmethod
    def encode(item: PersonObj) -> EncodedPersonObj:
        encoded: FrozenDict[str, UnfoldedJVal] = FrozenDict(
            {
                "id_str": item[0].value,
                "email_str": item[1].email,
                "name_str": item[1].name.value_or(None),
                "phone_number_str": item[1].phone_number.value_or(None),
                "created_at_int": item[1].created_at,
                "last_sent_at_int": item[1].last_sent_at.value_or(None),
                "last_responded_at_int": item[1].last_responded_at.value_or(
                    None
                ),
                "next_survey_scheduled_at_int": item[
                    1
                ].next_survey_scheduled_at.value_or(None),
            }
        )
        record = SingerRecord(
            SupportedStreams.PEOPLE.value, from_unfolded_dict(encoded), None
        )
        return EncodedPersonObj(_Private(), record)
