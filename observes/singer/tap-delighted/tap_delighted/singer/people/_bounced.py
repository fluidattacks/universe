from __future__ import (
    annotations,
)

from dataclasses import (
    dataclass,
    field,
)
from fa_purity import (
    FrozenDict,
    JsonValue,
    UnfoldedJVal,
)
from fa_purity.json.factory import (
    from_unfolded_dict,
)
from fa_purity.utils import (
    raise_exception,
)
from fa_singer_io.json_schema.factory import (
    from_json,
    from_prim_type,
    opt_prim_type,
)
from fa_singer_io.singer import (
    SingerRecord,
    SingerSchema,
)
from tap_delighted.api.core.people import (
    BouncedPerson,
)
from tap_delighted.streams import (
    SupportedStreams,
)

_STREAM = SupportedStreams.BOUNCED.value


def _schema() -> SingerSchema:
    properties = FrozenDict(
        {
            "person_id_str": JsonValue(from_prim_type(str).encode()),
            "email_str": JsonValue(from_prim_type(str).encode()),
            "name_str": JsonValue(opt_prim_type(str).encode()),
            "bounced_at_int": JsonValue(from_prim_type(int).encode()),
        }
    )
    schema = FrozenDict({"properties": JsonValue(properties)})
    return (
        SingerSchema.new(
            _STREAM,
            from_json(schema).alt(raise_exception).to_union(),
            frozenset(["person_id_str"]),
            None,
        )
        .alt(raise_exception)
        .to_union()
    )


@dataclass(frozen=True)
class _Private:
    pass


@dataclass(frozen=True)
class EncodedBounced:
    _private: _Private = field(repr=False, hash=False, compare=False)
    record: SingerRecord

    @staticmethod
    def schema() -> SingerSchema:
        return _schema()

    @staticmethod
    def encode(item: BouncedPerson) -> EncodedBounced:
        encoded: FrozenDict[str, UnfoldedJVal] = FrozenDict(
            {
                "person_id_str": item.person_id.value,
                "email_str": item.email,
                "name_str": item.name.value_or(None),
                "bounced_at_int": item.bounced_at,
            }
        )
        record = SingerRecord(_STREAM, from_unfolded_dict(encoded), None)
        return EncodedBounced(_Private(), record)
