{ inputs, projectPath, ... }@makes_inputs:
import (projectPath "/observes/common/check/tests") {
  inherit makes_inputs;
  inherit (inputs) nixpkgs;
  pkg_index_item = inputs.observesIndex.tap.dynamo;
}
