{ lib, makes_inputs, nixpkgs, python_pkgs, python_version, }@args:
let
  bundles = import (makes_inputs.projectPath
    "${makes_inputs.inputs.observesIndex.common.python_pkgs}/fa_purity") args;
in bundles."v1.42.0".build_bundle (default: required_deps: builder:
  builder lib (required_deps
    (python_pkgs // { inherit (default.python_pkgs) types-simplejson; })))
