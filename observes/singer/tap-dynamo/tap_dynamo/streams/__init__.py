from ._extractor_1 import (
    extract_segment,
    PageData,
    TableSegment,
)

__all__ = [
    "PageData",
    "TableSegment",
    "extract_segment",
]
