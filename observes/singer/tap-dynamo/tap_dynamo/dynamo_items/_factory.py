from ._scalar import (
    Scalar,
)
from ._scalar_set import (
    ScalarSet,
)
from ._value import (
    DynamoValue,
)
from boto3.dynamodb.types import (
    Binary,
)
from dataclasses import (
    dataclass,
)
from decimal import (
    Decimal,
)
from fa_purity import (
    Result,
    ResultE,
)
from fa_purity.frozen import (
    FrozenDict,
)
from fa_purity.pure_iter import (
    PureIterFactory,
)
from fa_purity.result.transform import (
    all_ok,
)
from fa_purity.utils import (
    cast_exception,
)
from typing import (
    FrozenSet,
    Set,
    Type,
    TypeVar,
)

_A = TypeVar("_A")
_T = TypeVar("_T")


def _from_set_any(
    raw: FrozenSet[_A], _type: Type[_T]
) -> ResultE[FrozenSet[_T]]:
    _items: Set[_T] = set()
    for item in raw:
        if not isinstance(item, _type):
            err = TypeError(f"Expected type `{_type}` but got `{type(item)}`")
            return Result.failure(cast_exception(err))
        _items.add(item)
    return Result.success(frozenset(_items))


def _assert_str(raw: _A) -> ResultE[str]:
    if isinstance(raw, str):
        return Result.success(raw)
    return Result.failure(
        TypeError(f"Expected a `str` but got `{type(raw)}`"), str
    ).alt(cast_exception)


@dataclass(frozen=True)
class ScalarFactory:
    @staticmethod
    def from_union(item: str | int | Decimal | Binary | bool | None) -> Scalar:
        if isinstance(item, str):
            return Scalar.from_str(item)
        if isinstance(item, bool):
            return Scalar.from_bool(item)
        if isinstance(item, Decimal):
            return Scalar.from_decimal(item)
        if isinstance(item, Binary):  # type: ignore[misc]
            return Scalar.from_binary(item)
        if isinstance(item, int):
            return Scalar.from_int(item)
        if item is None:
            return Scalar.empty()


@dataclass(frozen=True)
class ScalarSetFactory:
    @classmethod
    def from_set(
        cls, raw: FrozenSet[_A], empty: ScalarSet
    ) -> ResultE[ScalarSet]:
        try:
            element = next(iter(raw))
        except StopIteration:
            return Result.success(empty)
        if isinstance(element, str):
            return _from_set_any(raw, str).map(ScalarSet.from_str)
        if isinstance(element, int):
            return _from_set_any(raw, int).map(ScalarSet.from_int)
        if isinstance(element, Decimal):
            return _from_set_any(raw, Decimal).map(ScalarSet.from_decimal)
        if isinstance(element, Binary):  # type: ignore[misc]
            return _from_set_any(raw, Binary).map(ScalarSet.from_binary)  # type: ignore[misc]
        err = TypeError(
            f"First element type `{type(element)}` not expected."
            "Should be one of str, int, Decimal or Binary"
        )
        return Result.failure(cast_exception(err))


@dataclass(frozen=True)
class DynamoValueFactory:
    @classmethod
    def from_any(cls, raw: _A) -> ResultE[DynamoValue]:
        empty = ScalarSet.from_str(frozenset([]))
        if raw is None or isinstance(raw, (str, int, Decimal, Binary, bool)):  # type: ignore[misc]
            return Result.success(
                DynamoValue.from_scalar(ScalarFactory.from_union(raw))
            )
        if isinstance(raw, set):
            return ScalarSetFactory.from_set(
                frozenset(raw), empty  # type: ignore[misc]
            ).map(DynamoValue.from_set)
        if isinstance(raw, frozenset):
            return ScalarSetFactory.from_set(raw, empty).map(
                DynamoValue.from_set
            )
        if isinstance(raw, (list, tuple)):
            return all_ok(tuple(cls.from_any(r) for r in raw)).map(
                DynamoValue.from_list
            )
        if isinstance(raw, dict):
            return (
                PureIterFactory.from_list(
                    tuple((k, v) for k, v in raw.items())
                )
                .map(
                    lambda t: _assert_str(t[0]).bind(
                        lambda k: cls.from_any(t[1]).map(lambda v: (k, v))
                    )
                )
                .transform(lambda p: all_ok(p.to_list()))
                .map(lambda d: FrozenDict(dict(d)))
                .map(DynamoValue.from_dict)
            )
        return Result.failure(
            TypeError("Expected some unfolded type of `DynamoValue`"),
            DynamoValue,
        ).alt(cast_exception)
