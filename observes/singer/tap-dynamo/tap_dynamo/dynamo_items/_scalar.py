from __future__ import (
    annotations,
)

from boto3.dynamodb.types import (
    Binary,
)
from dataclasses import (
    dataclass,
    field,
)
from decimal import (
    Decimal,
)
from fa_purity._core.coproduct import (
    Coproduct,
)
from typing import (
    Callable,
    TypeVar,
)

_T = TypeVar("_T")


@dataclass(frozen=True)
class _Private:
    pass


@dataclass(frozen=True)
class Scalar:
    _private: _Private = field(repr=False, hash=False, compare=False)
    _inner_str: Coproduct[str, None]
    _inner_int: Coproduct[int, None]
    _inner_decimal: Coproduct[Decimal, None]
    _inner_binary: Coproduct[Binary, None]
    _inner_bool: Coproduct[bool, None]

    @staticmethod
    def from_str(raw: str) -> Scalar:
        return Scalar(
            _Private(),
            Coproduct.inl(raw),
            Coproduct.inr(None),
            Coproduct.inr(None),
            Coproduct.inr(None),
            Coproduct.inr(None),
        )

    @staticmethod
    def from_int(raw: int) -> Scalar:
        return Scalar(
            _Private(),
            Coproduct.inr(None),
            Coproduct.inl(raw),
            Coproduct.inr(None),
            Coproduct.inr(None),
            Coproduct.inr(None),
        )

    @staticmethod
    def from_decimal(raw: Decimal) -> Scalar:
        return Scalar(
            _Private(),
            Coproduct.inr(None),
            Coproduct.inr(None),
            Coproduct.inl(raw),
            Coproduct.inr(None),
            Coproduct.inr(None),
        )

    @staticmethod
    def from_binary(raw: Binary) -> Scalar:
        return Scalar(
            _Private(),
            Coproduct.inr(None),
            Coproduct.inr(None),
            Coproduct.inr(None),
            Coproduct.inl(raw),
            Coproduct.inr(None),
        )

    @staticmethod
    def from_bool(raw: bool) -> Scalar:
        return Scalar(
            _Private(),
            Coproduct.inr(None),
            Coproduct.inr(None),
            Coproduct.inr(None),
            Coproduct.inr(None),
            Coproduct.inl(raw),
        )

    @staticmethod
    def empty() -> Scalar:
        return Scalar(
            _Private(),
            Coproduct.inr(None),
            Coproduct.inr(None),
            Coproduct.inr(None),
            Coproduct.inr(None),
            Coproduct.inr(None),
        )

    def map(
        self,
        str_case: Callable[[str], _T],
        int_case: Callable[[int], _T],
        decimal_case: Callable[[Decimal], _T],
        binary_case: Callable[[Binary], _T],
        bool_case: Callable[[bool], _T],
        none_case: Callable[[], _T],
    ) -> _T:
        return self._inner_str.map(
            str_case,
            lambda _: self._inner_int.map(
                int_case,
                lambda _: self._inner_decimal.map(
                    decimal_case,
                    lambda _: self._inner_binary.map(
                        binary_case,
                        lambda _: self._inner_bool.map(
                            bool_case, lambda _: none_case()
                        ),
                    ),
                ),
            ),
        )
