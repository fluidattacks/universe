import base64
from boto3.dynamodb.types import (
    Binary,
)
from tap_dynamo.dynamo_items import (
    DynamoValueTransform,
)


def test_encode() -> None:
    original = bytes(b"abcdef")
    result = DynamoValueTransform._encode_binary(Binary(original))
    assert original == base64.b64decode(result)
