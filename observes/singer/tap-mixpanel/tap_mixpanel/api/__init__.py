from . import (
    _export,
)
from ._core import (
    ApiClient,
    Credentials,
    Event,
    EventName,
    ProjectId,
    TargetDate,
)


def new_api_client(creds: Credentials, project: ProjectId) -> ApiClient:
    return ApiClient(lambda e, d: _export.export(creds, project, e, d))


__all__ = [
    "ApiClient",
    "Credentials",
    "Event",
    "EventName",
    "ProjectId",
    "TargetDate",
]
