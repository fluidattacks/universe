import datetime
import logging
import sys
from datetime import (
    date,
)
from pathlib import Path
from tempfile import (
    NamedTemporaryFile,
)
from time import (
    time,
)

from etl_utils.parallel import (
    ThreadPool,
)
from etl_utils.retry import (
    retry_cmd,
)
from etl_utils.typing import (
    IO,
    Callable,
    FrozenSet,
    TypeVar,
)
from fa_purity import (
    Cmd,
    CmdUnwrapper,
    PureIterFactory,
    Result,
    ResultE,
    StreamTransform,
    Unsafe,
    cast_exception,
)
from fa_purity.date_time import (
    DatetimeFactory,
)
from fa_purity.lock import (
    ThreadLock,
)
from fa_singer_io.singer import (
    SingerMessage,
    SingerRecord,
)
from fa_singer_io.singer.emitter import (
    emit,
)
from requests.exceptions import (
    ChunkedEncodingError,
)

from .api import (
    ApiClient,
    Credentials,
    Event,
    EventName,
    ProjectId,
    TargetDate,
    new_api_client,
)

LOG = logging.getLogger(__name__)
_T = TypeVar("_T")


def _get_today() -> Cmd[date]:
    return DatetimeFactory.date_now().map(lambda d: d.date_time.date())


_last_time = [time()]


def _cmd_rate_limit(msg: Cmd[None], period: int) -> Cmd[None]:
    def _action(unwrapper: CmdUnwrapper) -> None:
        current_time = time()
        if current_time - _last_time[0] >= period:
            unwrapper.act(msg)
            _last_time[0] = current_time

    return Cmd.new_cmd(_action)


def _emit(event: Event, name: str, target: IO[str]) -> Cmd[None]:
    msg = Cmd.wrap_impure(lambda: LOG.info("Emitting events into `%s`...", name))
    record = SingerRecord("Events", event.raw, None)
    return _cmd_rate_limit(msg, 5) + emit(target, SingerMessage.from_record(record))


def _handle_chunk_error(command: Cmd[_T]) -> Cmd[ResultE[_T]]:
    def _action(unwrapper: CmdUnwrapper) -> ResultE[_T]:
        try:
            return Result.success(unwrapper.act(command))
        except ChunkedEncodingError as err:
            return Result.failure(cast_exception(err))

    return Cmd.new_cmd(_action)


def _handle_temp_files(command: Callable[[str, IO[str]], Cmd[ResultE[_T]]]) -> Cmd[ResultE[_T]]:
    def _action(unwrapper: CmdUnwrapper) -> ResultE[_T]:
        with NamedTemporaryFile(mode="r+", delete=False) as temp_file:
            LOG.info("Acting over `%s`", temp_file.name)
            result = unwrapper.act(command(temp_file.name, temp_file))
        if not result.map(lambda _: True).value_or(False):
            LOG.warning("Error detected, discarding temp data `%s`", temp_file.name)
            Path(temp_file.name).unlink()
        return result

    return Cmd.new_cmd(_action)


def _emit_events(
    client: ApiClient,
    target_date: TargetDate,
    events: FrozenSet[EventName],
) -> Cmd[str]:
    main = _handle_temp_files(
        lambda name, f: _handle_chunk_error(
            StreamTransform.consume(
                client.export(events, target_date).map(lambda e: _emit(e, name, f)),
            ).map(lambda _: name),
        ),
    )

    def _next(i: int, prev: ResultE[str]) -> Cmd[ResultE[str]]:
        retry_msg = Cmd.wrap_impure(lambda: LOG.info("Handled stream error. Retry #%s", i))
        return prev.map(lambda v: Cmd.wrap_value(Result.success(v, Exception))).value_or(
            retry_msg + main,
        )

    return retry_cmd(main, _next, 5).map(lambda r: r.alt(Unsafe.raise_exception).to_union())


def _mirror_file(file: IO[str], lock: ThreadLock) -> Cmd[None]:
    def _emit(line: str) -> Cmd[None]:
        return lock.execute_with_lock(
            Cmd.wrap_impure(lambda: sys.stdout.write(line)),  # type: ignore[misc]
        ).map(lambda _: None)  # type: ignore[misc]

    def _action(unwrapper: CmdUnwrapper) -> None:
        line = file.readline()
        while line:
            unwrapper.act(_emit(line))
            line = file.readline()

    return Cmd.new_cmd(_action)


def _emit_file(temp_file: str, lock: ThreadLock) -> Cmd[None]:
    def _action(unwrapper: CmdUnwrapper) -> None:
        with Path(temp_file).open("r", encoding="utf-8") as file:
            unwrapper.act(_mirror_file(file, lock))

    return Cmd.new_cmd(_action)


def stream_events(
    creds: Credentials,
    project: ProjectId,
    events: FrozenSet[EventName],
) -> Cmd[None]:
    target_date = _get_today().map(lambda today: TargetDate(today - datetime.timedelta(days=1)))
    grouped_events = PureIterFactory.from_list(tuple(events)).chunked(40)
    msg = Cmd.wrap_impure(lambda: LOG.info("Target events: %s", str(events)))
    pool = ThreadPool.new(3)
    lock = ThreadLock.new()
    procedure = pool.bind(
        lambda p: lock.bind(
            lambda _lock: target_date.bind(
                lambda dr: p.in_threads_none(
                    grouped_events.map(
                        lambda g: _emit_events(
                            new_api_client(creds, project),
                            dr,
                            frozenset(g),
                        ).bind(lambda f: _emit_file(f, _lock)),
                    ),
                ),
            ),
        ),
    )
    return msg + procedure
