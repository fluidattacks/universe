from __future__ import (
    annotations,
)

import logging
from dataclasses import (
    dataclass,
)
from pathlib import (
    Path,
)
from tempfile import (
    NamedTemporaryFile,
)
from types import (
    TracebackType,
)

from etl_utils.typing import (
    IO,
    AnyStr,
    Callable,
    Iterable,
    Iterator,
    TypeVar,
)
from fa_purity import (
    Cmd,
    CmdUnwrapper,
    PureIter,
    Stream,
    Unsafe,
)

LOG = logging.getLogger(__name__)
_T = TypeVar("_T")


@dataclass(frozen=True)
class _Inner:
    file_path: Path


class UnnamedIO(IO[AnyStr]):
    def __init__(self, file: IO[AnyStr]) -> None:
        """Initialize a UnnamedIO."""
        self._wrapped_io: IO[AnyStr] = file

    @property
    def name(self) -> str:
        return "[masked]"

    def __enter__(self) -> UnnamedIO[AnyStr]:
        """Enter context manager."""
        return self

    def __exit__(
        self,
        __t: type[BaseException] | None,  # noqa: PYI063
        __value: BaseException | None,
        __traceback: TracebackType | None,
    ) -> None:
        """Exit context manager."""
        return self._wrapped_io.__exit__(__t, __value, __traceback)

    def __iter__(self) -> Iterator[AnyStr]:
        """Get iter."""
        return self._wrapped_io.__iter__()

    def __next__(self) -> AnyStr:
        """Get next str."""
        return self._wrapped_io.__next__()

    def close(self) -> None:
        return self._wrapped_io.close()

    def fileno(self) -> int:
        return self._wrapped_io.fileno()

    def flush(self) -> None:
        return self._wrapped_io.flush()

    def isatty(self) -> bool:
        return self._wrapped_io.isatty()

    def read(self, __n: int = -1) -> AnyStr:  # noqa: PYI063
        return self._wrapped_io.read(__n)

    def readable(self) -> bool:
        return self._wrapped_io.readable()

    def readline(self, __limit: int = -1) -> AnyStr:  # noqa: PYI063
        return self._wrapped_io.readline(__limit)

    def readlines(self, __hint: int = -1) -> list[AnyStr]:  # noqa: PYI063
        return self._wrapped_io.readlines(__hint)

    def seek(self, __offset: int, __whence: int = 0) -> int:  # noqa: PYI063
        return self._wrapped_io.seek(__offset, __whence)

    def seekable(self) -> bool:
        return self._wrapped_io.seekable()

    def tell(self) -> int:
        return self._wrapped_io.tell()

    def truncate(self, __size: int | None = None) -> int:  # noqa: PYI063
        return self._wrapped_io.truncate(__size)

    def writable(self) -> bool:
        return self._wrapped_io.writable()

    def write(self, __s: AnyStr) -> int:  # noqa: PYI063
        return self._wrapped_io.write(__s)

    def writelines(self, __lines: Iterable[AnyStr]) -> None:  # noqa: PYI063
        return self._wrapped_io.writelines(__lines)


@dataclass(frozen=True)
class TempFile:
    _inner: _Inner

    @staticmethod
    def new() -> Cmd[TempFile]:
        def _action() -> TempFile:
            with NamedTemporaryFile("w", delete=False) as file_object:
                return TempFile(_Inner(Path(file_object.name)))

        return Cmd.wrap_impure(_action)

    @property
    def path(self) -> Path:
        """TempFile is mutable hence exposing the path is not an issue."""
        return self._inner.file_path

    def append(self, write_cmd: Callable[[UnnamedIO[str]], Cmd[_T]]) -> Cmd[_T]:
        """
        Open file in append mode and executes `write_cmd`.

        [WARNING] executing concurrent `append` commands on the same `TempFile`
        would result in unexpected behavior.
        Wrap this command using `execute_with_lock` from a common `Lock` object
        if concurrency is required.
        """

        def _action(unwrapper: CmdUnwrapper) -> _T:
            with Path(self._inner.file_path).open("a", encoding="utf-8") as file:
                return unwrapper.act(write_cmd(UnnamedIO(file)))

        return Cmd.new_cmd(_action)

    def read(self, write_cmd: Callable[[UnnamedIO[str]], Cmd[_T]]) -> Cmd[_T]:
        def _action(unwrapper: CmdUnwrapper) -> _T:
            with Path(self._inner.file_path).open("r", encoding="utf-8") as file:
                return unwrapper.act(write_cmd(UnnamedIO(file)))

        return Cmd.new_cmd(_action)

    def read_lines(self) -> Stream[str]:
        def _new_iter() -> Iterable[str]:
            with Path(self._inner.file_path).open("r", encoding="UTF-8") as file:
                line = file.readline()
                while line:
                    yield line
                    line = file.readline()

        return Unsafe.stream_from_cmd(Cmd.wrap_impure(_new_iter))

    def write(self, write_cmd: Callable[[UnnamedIO[str]], Cmd[_T]]) -> Cmd[_T]:
        def _action(unwrapper: CmdUnwrapper) -> _T:
            with Path(self._inner.file_path).open("w", encoding="utf-8") as file:
                return unwrapper.act(write_cmd(UnnamedIO(file)))

        return Cmd.new_cmd(_action)

    def write_lines(self, lines: PureIter[str]) -> Cmd[None]:
        return self.write(lambda f: Cmd.wrap_impure(lambda: f.writelines(lines)))


@dataclass(frozen=True)
class TempReadOnlyFile:
    """
    Temporal file with only read access.

    Is not possible to get the path of the file.
    i.e. for protecting it from opening with write permissions.
    """

    _inner: _Inner

    @staticmethod
    def new(content: PureIter[str]) -> Cmd[TempReadOnlyFile]:
        def _action() -> TempReadOnlyFile:
            with NamedTemporaryFile("w", delete=False) as file_object:
                file_object.writelines(content)
                return TempReadOnlyFile(_Inner(Path(file_object.name)))

        return Cmd.wrap_impure(_action)

    @staticmethod
    def from_cmd(write_cmd: Callable[[IO[str]], Cmd[None]]) -> Cmd[TempReadOnlyFile]:
        """`write_cmd` initializes the file content. It has write-only access to file."""

        def _action(unwrapper: CmdUnwrapper) -> TempReadOnlyFile:
            with NamedTemporaryFile("w", delete=False) as file_object:
                unwrapper.act(write_cmd(file_object))
                return TempReadOnlyFile(_Inner(Path(file_object.name)))

        return Cmd.new_cmd(_action)

    @staticmethod
    def save(content: Stream[str]) -> Cmd[TempReadOnlyFile]:
        def _action(unwrapper: CmdUnwrapper) -> TempReadOnlyFile:
            with NamedTemporaryFile("w", delete=False) as file_object:
                LOG.debug("Saving stream into %s", file_object.name)
                file_object.writelines(unwrapper.act(Unsafe.stream_to_iter(content)))
                return TempReadOnlyFile(_Inner(Path(file_object.name)))

        return Cmd.new_cmd(_action)

    @classmethod
    def freeze_io(cls, file: IO[str]) -> Cmd[TempReadOnlyFile]:
        def _action() -> Iterable[str]:
            file.seek(0)
            line = file.readline()
            while line:
                yield line
                line = file.readline()

        start = Cmd.wrap_impure(lambda: LOG.debug("Freezing file"))
        end = Cmd.wrap_impure(lambda: LOG.debug("Freezing completed!"))
        stream = Unsafe.stream_from_cmd(Cmd.wrap_impure(_action))
        return start + cls.save(stream).bind(lambda f: end.map(lambda _: f))

    @classmethod
    def freeze(cls, file_path: str) -> Cmd[TempReadOnlyFile]:
        def _action(unwrapper: CmdUnwrapper) -> TempReadOnlyFile:
            with Path(file_path).open("r", encoding="utf-8") as file:
                return unwrapper.act(cls.freeze_io(file))

        return Cmd.new_cmd(_action)

    def over_binary(self, cmd_fx: Callable[[UnnamedIO[bytes]], Cmd[_T]]) -> Cmd[_T]:
        def _action(unwrapper: CmdUnwrapper) -> _T:
            with Path(self._inner.file_path).open("rb") as file:
                return unwrapper.act(cmd_fx(UnnamedIO(file)))

        return Cmd.new_cmd(_action)

    def read(self) -> PureIter[str]:
        def _new_iter() -> Iterable[str]:
            with Path(self._inner.file_path).open("r", encoding="utf-8") as file:
                line = file.readline()
                while line:
                    yield line
                    line = file.readline()

        return Unsafe.pure_iter_from_cmd(Cmd.wrap_impure(_new_iter))
