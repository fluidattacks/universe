import click
from etl_utils.typing import (
    NoReturn,
)
from fa_purity import (
    Cmd,
    PureIterFactory,
)
from utils_logger import (
    start_session,
)

from . import (
    emitter,
)
from .api import (
    Credentials,
    EventName,
    ProjectId,
)


@click.command()
@click.option(
    "--user",
    envvar="MIXPANEL_USER",
    type=str,
    required=True,
    help="The service account user",
)
@click.option(
    "--secret",
    envvar="MIXPANEL_PASSWORD",
    type=str,
    required=True,
    help="The service account secret",
)
@click.option("--project", type=str, required=True, help="The project id")
@click.argument("events", type=str)
def stream_events(user: str, secret: str, project: str, events: str) -> NoReturn:
    cmd: Cmd[None] = start_session() + emitter.stream_events(
        Credentials(user, secret),
        ProjectId(project),
        frozenset(PureIterFactory.from_list(events.split(",")).map(EventName)),
    )
    cmd.compute()


@click.group()
def main() -> None:  # decorator make it to return `NoReturn`
    # cli group entrypoint
    pass


main.add_command(stream_events)
