import inspect
import sys
from dataclasses import (
    dataclass,
)
from types import (
    FrameType,
)

from etl_utils.typing import (
    Generic,
    NoReturn,
    TypeVar,
)
from fa_purity import (
    FrozenList,
    Result,
)

_T = TypeVar("_T")
_S = TypeVar("_S")
_F = TypeVar("_F")


@dataclass
class Bug(Exception, Generic[_T]):
    obj_id: str
    location: str
    parent_error: _T | None
    context: FrozenList[str]

    def explode(self) -> NoReturn:
        sys.tracebacklimit = 0
        raise self

    @classmethod
    def assume_success(
        cls,
        name: str,
        location: FrameType | None,
        context: FrozenList[str],
        result: Result[_S, _F],
    ) -> _S | NoReturn:
        return (
            result.alt(
                lambda e: Bug(
                    name,
                    str(inspect.getframeinfo(location))
                    if location is not None
                    else "?? Unknown ??",
                    e,
                    context,
                ),
            )
            .alt(lambda e: e.explode())
            .to_union()
        )

    def __str__(self) -> str:
        return (
            "\n"
            + "-" * 30
            + "\n [Id] "
            + self.obj_id
            + "\n [Location] "
            + self.location
            + "\n [Parent] "
            + str(self.parent_error)
            + "\n [Context] "
            + str(self.context)
        )
