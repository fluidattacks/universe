from arch_lint.dag import (
    DagMap,
)
from arch_lint.graph import (
    FullPathModule,
)
from etl_utils.typing import (
    Dict,
    FrozenSet,
    TypeVar,
)
from fa_purity import (
    FrozenList,
)

_dag: Dict[str, FrozenList[FrozenList[str] | str]] = {
    "tap_mixpanel": (
        "_cli",
        "emitter",
        "api",
        ("_logger", "_bug", "temp_file"),
    ),
    "tap_mixpanel.api": (
        "_export",
        "_core",
    ),
}
_T = TypeVar("_T")


def raise_or_return(item: Exception | _T) -> _T:
    if isinstance(item, Exception):
        raise item
    return item


def project_dag() -> DagMap:
    return raise_or_return(DagMap.new(_dag))


def forbidden_allowlist() -> Dict[FullPathModule, FrozenSet[FullPathModule]]:
    _raw: Dict[str, FrozenSet[str]] = {}
    return {
        raise_or_return(FullPathModule.from_raw(k)): frozenset(
            raise_or_return(FullPathModule.from_raw(i)) for i in v
        )
        for k, v in _raw.items()
    }
