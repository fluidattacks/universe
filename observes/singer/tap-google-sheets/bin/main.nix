{ inputs, makeScript, projectPath, ... }@makes_inputs:
let
  root = projectPath inputs.observesIndex.tap.google_sheets.root;
  bundle = import "${root}/entrypoint.nix" makes_inputs;
  env = bundle.env.runtime;
in makeScript {
  name = "tap-google-sheets";
  searchPaths = { bin = [ env ]; };
  entrypoint = ''
    tap-google-sheets "$@"
  '';
}
