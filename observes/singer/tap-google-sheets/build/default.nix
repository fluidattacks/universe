{ makes_inputs, nixpkgs, python_version, }:
let
  deps = import ./deps { inherit makes_inputs nixpkgs python_version; };
  self_pkgs = import ./pkg {
    inherit (deps) lib;
    inherit (deps) python_pkgs;
  };
in self_pkgs
