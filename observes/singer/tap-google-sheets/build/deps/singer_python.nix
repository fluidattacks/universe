lib: python_pkgs:
python_pkgs.buildPythonPackage rec {
  pname = "singer-python";
  version = "6.0.1";
  propagatedBuildInputs = with python_pkgs; [
    backoff
    ciso8601
    jsonschema
    pytz
    python-dateutil
    simplejson
  ];
  src = lib.fetchPypi {
    inherit pname version;
    sha256 = "U4B2gmJXeYektemvymIhuu9xoLs4A/ZTumA8GMYd7ug=";
  };
  doCheck = false;
}
