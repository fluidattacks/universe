{ makes_inputs, nixpkgs, python_version, }:
let
  lib = {
    buildEnv = nixpkgs."${python_version}".buildEnv.override;
    inherit (nixpkgs."${python_version}".pkgs) buildPythonPackage;
    inherit (nixpkgs.python3Packages) fetchPypi;
  };
  utils = makes_inputs.pythonOverrideUtils;
  layer_1 = python_pkgs:
    python_pkgs // {
      singer-python = import ./singer_python.nix lib python_pkgs;
    };

  python_pkgs = utils.compose [ layer_1 ] nixpkgs."${python_version}Packages";
in { inherit lib python_pkgs; }
