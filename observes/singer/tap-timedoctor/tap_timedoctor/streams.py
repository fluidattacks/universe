from dataclasses import (
    dataclass,
)
from fa_purity import (
    Cmd,
)
from fa_purity.date_time import (
    DatetimeUTC,
)
from typing import (
    Callable,
)


@dataclass(frozen=True)
class Options:
    start_date: DatetimeUTC
    end_date: DatetimeUTC


@dataclass(frozen=True, kw_only=True)
class DataStreams:
    work_logs: Callable[[Options], Cmd[None]]
    computer_activity: Callable[[Options], Cmd[None]]
