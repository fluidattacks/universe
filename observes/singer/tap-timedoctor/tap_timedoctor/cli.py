import click
from datetime import (
    datetime,
    timezone,
)
from dateutil.relativedelta import (
    relativedelta,
)
from fa_purity import (
    cast_exception,
    Cmd,
    Maybe,
    Result,
    ResultE,
    Unsafe,
)
from fa_purity.date_time import (
    DatetimeFactory,
    DatetimeUTC,
    RawDatetime,
)
from tap_timedoctor.streams import (
    DataStreams,
    Options,
)


def mock_streams() -> DataStreams:
    nothing = Cmd.wrap_value(None)
    return DataStreams(
        work_logs=lambda _: nothing,
        computer_activity=lambda _: nothing,
    )


def _to_date_utc(raw: str) -> ResultE[DatetimeUTC]:
    try:
        return Result.success(
            datetime.strptime(raw, "%Y-%m-%d"), Exception
        ).bind(
            lambda d: DatetimeFactory.new_utc(
                RawDatetime(
                    year=d.year,
                    month=d.month,
                    day=d.day,
                    hour=d.hour,
                    minute=d.minute,
                    second=d.second,
                    microsecond=d.microsecond,
                    time_zone=timezone.utc,
                )
            )
        )
    except ValueError as err:
        return Result.failure(cast_exception(err))


def _gen_options(
    today: DatetimeUTC,
    start_date: Maybe[DatetimeUTC],
    end_date: Maybe[DatetimeUTC],
) -> Options:
    default_start_date = (
        DatetimeUTC.assert_utc(today.date_time - relativedelta(years=1))
        .alt(Unsafe.raise_exception)
        .to_union()
    )
    default_end_date = today
    return Options(
        start_date.value_or(default_start_date),
        end_date.value_or(default_end_date),
    )


def _main(
    options: Options, work_logs: bool, computer_activity: bool
) -> Cmd[None]:
    streams = mock_streams()
    nothing = Cmd.wrap_value(None)
    work_logs_stream = streams.work_logs(options) if work_logs else nothing
    computer_activity_stream = (
        streams.computer_activity(options) if computer_activity else nothing
    )
    return work_logs_stream + computer_activity_stream


@click.command()
@click.option("--start-date", type=str, required=False)
@click.option("--end-date", type=str, required=False)
@click.option("--work-logs", type=str, is_flag=True)
@click.option("--computer-activity", type=str, is_flag=True)
def main(
    start_date: str | None,
    end_date: str | None,
    work_logs: bool,
    computer_activity: bool,
) -> None:
    _start_date = (
        Maybe.from_optional(start_date)
        .map(_to_date_utc)
        .map(lambda r: r.alt(Unsafe.raise_exception).to_union())
    )
    _end_date = (
        Maybe.from_optional(end_date)
        .map(_to_date_utc)
        .map(lambda r: r.alt(Unsafe.raise_exception).to_union())
    )
    options = DatetimeFactory.date_now().map(
        lambda t: _gen_options(t, _start_date, _end_date)
    )
    cmd: Cmd[None] = options.bind(
        lambda p: _main(p, work_logs, computer_activity)
    )
    cmd.compute()
