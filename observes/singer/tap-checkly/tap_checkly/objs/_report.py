from dataclasses import (
    dataclass,
)

from ._group import (
    CheckId,
)


@dataclass(frozen=True)
class CheckReport:  # pylint: disable=too-many-instance-attributes
    # reason for `too-many-instance-attributes`
    # even if splitted it would not reduce the number of attrs that are needed
    check_id: CheckId
    check_type: str
    deactivated: bool
    name: str
    avg: float
    p95: float
    p99: float
    success_ratio: float
