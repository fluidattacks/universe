from dataclasses import (
    dataclass,
)
from datetime import (
    datetime,
)

from fa_purity import (
    FrozenList,
    Maybe,
)

from ._subscriptions import (
    ChannelSubscription,
)


@dataclass(frozen=True)
class CheckId:
    id_str: str


@dataclass(frozen=True)
class CheckGroup:  # pylint: disable=too-many-instance-attributes
    # reason for `too-many-instance-attributes`
    # even if splitted it would not reduce the number of attrs that are needed
    activated: bool
    concurrency: int
    name: str
    alert_channels: FrozenList[ChannelSubscription]
    created_at: datetime
    updated_at: Maybe[datetime]
    double_check: bool
    locations: FrozenList[str]
    muted: bool
    runtime_id: Maybe[str]
    use_global_alert_settings: bool
    # ~apiCheckDefaults~
    # ~browserCheckDefaults~
    # ~alertSettings~
    # ~environmentVariables~
    # ~localSetupScript~
    # ~localTearDownScript~
    # ~privateLocations~
    # ~setupSnippetId~
    # ~tags~
    # ~tearDownSnippetId~
