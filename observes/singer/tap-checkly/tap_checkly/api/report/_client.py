from __future__ import (
    annotations,
)

from dataclasses import (
    dataclass,
)

from etl_utils.typing import (
    Dict,
)
from fa_purity import (
    Cmd,
    FrozenDict,
    FrozenList,
    PureIterFactory,
    Unsafe,
)
from fa_purity.date_time import (
    DatetimeUTC,
)
from fa_purity.json import (
    Primitive,
    UnfoldedFactory,
)

from tap_checkly.api._raw import (
    Credentials,
    RawClient,
)
from tap_checkly.objs import (
    CheckReport,
    DateRange,
    IndexedObj,
    ReportObj,
)

from ._decode import (
    CheckReportDecoder,
)


@dataclass(frozen=True)
class CheckReportClient:
    _client: RawClient

    def get_reports(
        self,
        from_date: DatetimeUTC,
        to_date: DatetimeUTC,
    ) -> Cmd[FrozenList[CheckReport]]:
        args: Dict[str, Primitive] = {
            "from": from_date.date_time.timestamp(),
            "to": to_date.date_time.timestamp(),
        }
        return self._client.new_get_list(
            "/v1/reporting",
            UnfoldedFactory.from_dict(FrozenDict(args)),
        ).map(
            lambda items: PureIterFactory.pure_map(
                lambda i: CheckReportDecoder(i)
                .decode_report()
                .alt(Unsafe.raise_exception)
                .to_union(),
                items,
            ).to_list(),
        )

    def get_reports_obj(
        self,
        from_date: DatetimeUTC,
        to_date: DatetimeUTC,
    ) -> Cmd[FrozenList[ReportObj]]:
        _id = DateRange(from_date, to_date)
        return self.get_reports(from_date, to_date).map(
            lambda items: tuple(IndexedObj(_id, i) for i in items),
        )

    @staticmethod
    def new(
        auth: Credentials,
    ) -> CheckReportClient:
        return CheckReportClient(RawClient.new(auth, 10))
