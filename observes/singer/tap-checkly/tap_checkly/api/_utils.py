from datetime import datetime

from etl_utils.typing import (
    Callable,
    TypeVar,
)
from fa_purity import (
    Cmd,
    FrozenList,
    Maybe,
    PureIterFactory,
    Result,
    ResultE,
    Stream,
    StreamFactory,
    StreamTransform,
)
from fa_purity.json import (
    JsonPrimitiveUnfolder,
    JsonValue,
    Unfolder,
)

from tap_checkly._utils import (
    isoparse,
    switch_maybe,
)

_T = TypeVar("_T")


def paginate_all(
    list_items: Callable[[int], Cmd[FrozenList[_T]]],
) -> Stream[_T]:
    return (
        PureIterFactory.infinite_range(1, 1)
        .map(list_items)
        .transform(lambda x: StreamFactory.from_commands(x))
        .map(lambda i: i if bool(i) else None)
        .transform(lambda x: StreamTransform.until_none(x))
        .map(lambda x: PureIterFactory.from_list(x))
        .transform(lambda x: StreamTransform.chain(x))
    )


def to_int(raw: JsonValue) -> ResultE[int]:
    return Unfolder.to_primitive(raw).bind(JsonPrimitiveUnfolder.to_int)


def to_str(raw: JsonValue) -> ResultE[str]:
    return Unfolder.to_primitive(raw).bind(JsonPrimitiveUnfolder.to_str)


def to_opt_str(raw: JsonValue) -> ResultE[Maybe[str]]:
    return (
        Unfolder.to_primitive(raw).bind(JsonPrimitiveUnfolder.to_opt_str).map(Maybe.from_optional)
    )


def to_bool(raw: JsonValue) -> ResultE[bool]:
    return Unfolder.to_primitive(raw).bind(JsonPrimitiveUnfolder.to_bool)


def to_float(raw: JsonValue) -> ResultE[float]:
    return Unfolder.to_primitive(raw).bind(
        lambda j: JsonPrimitiveUnfolder.to_float(j).lash(
            lambda _: JsonPrimitiveUnfolder.to_int(j).map(float),
        ),
    )


def to_datetime(raw: JsonValue) -> ResultE[datetime]:
    return Unfolder.to_primitive(raw).bind(JsonPrimitiveUnfolder.to_str).bind(isoparse)


def to_opt_datetime(raw: JsonValue) -> ResultE[Maybe[datetime]]:
    return (
        Unfolder.to_primitive(raw)
        .bind(JsonPrimitiveUnfolder.to_opt_str)
        .bind(
            lambda m: Maybe.from_optional(m)
            .map(
                lambda i: isoparse(i).map(Maybe.some),
            )
            .value_or(Result.success(Maybe.empty())),
        )
    )


__all__ = [
    "isoparse",
    "switch_maybe",
]
