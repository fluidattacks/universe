from etl_utils import smash
from fa_purity import (
    ResultE,
)
from fa_purity.json import JsonObj, JsonPrimitiveUnfolder, JsonUnfolder, Unfolder

from tap_checkly.api._utils import to_bool, to_datetime, to_int, to_opt_datetime, to_str
from tap_checkly.objs import (
    AlertChannel,
    AlertChannelId,
    AlertChannelObj,
    IndexedObj,
)


def decode_alert_channel(raw: JsonObj) -> ResultE[AlertChannel]:
    group_1 = smash.smash_result_5(
        JsonUnfolder.require(raw, "type", to_str),
        JsonUnfolder.require(raw, "sendRecovery", to_bool),
        JsonUnfolder.require(raw, "sendFailure", to_bool),
        JsonUnfolder.require(raw, "sendDegraded", to_bool),
        JsonUnfolder.require(raw, "sslExpiry", to_bool),
    )
    group_2 = smash.smash_result_3(
        JsonUnfolder.require(raw, "sslExpiryThreshold", to_int),
        JsonUnfolder.require(raw, "created_at", to_datetime),
        JsonUnfolder.require(raw, "updated_at", to_opt_datetime),
    )
    return group_1.bind(
        lambda g1: group_2.map(
            lambda g2: AlertChannel(*g1, *g2),
        ),
    )


def decode_alert_channel_obj(raw: JsonObj) -> ResultE[AlertChannelObj]:
    _id = JsonUnfolder.require(
        raw,
        "id",
        lambda v: Unfolder.to_primitive(v)
        .bind(
            JsonPrimitiveUnfolder.to_int,
        )
        .map(AlertChannelId),
    )
    return _id.bind(
        lambda i: decode_alert_channel(raw).map(
            lambda obj: IndexedObj(i, obj),
        ),
    )
