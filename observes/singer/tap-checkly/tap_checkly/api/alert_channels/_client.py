from __future__ import (
    annotations,
)

import inspect
from dataclasses import (
    dataclass,
)

from etl_utils.bug import Bug
from fa_purity import (
    Cmd,
    FrozenList,
    PureIterFactory,
    ResultTransform,
    Stream,
)
from fa_purity.json import UnfoldedFactory

from tap_checkly.api import (
    _utils,
)
from tap_checkly.api._raw import (
    Credentials,
    RawClient,
)
from tap_checkly.objs import (
    AlertChannelObj,
)

from . import (
    _decode,
)


@dataclass(frozen=True)
class AlertChannelsClient:
    _client: RawClient
    _per_page: int

    def _get_page(self, page: int) -> Cmd[FrozenList[AlertChannelObj]]:
        return (
            self._client.new_get_list(
                "/v1/alert-channels",
                UnfoldedFactory.from_dict(
                    {
                        "limit": self._per_page,
                        "page": page,
                    },
                ),
            )
            .map(
                lambda i: ResultTransform.all_ok(
                    PureIterFactory.from_list(i).map(_decode.decode_alert_channel_obj).to_list(),
                ),
            )
            .map(
                lambda r: Bug.assume_success(
                    "alert_channels_get_page",
                    inspect.currentframe(),
                    (),
                    r,
                ),
            )
        )

    def list_all(self) -> Stream[AlertChannelObj]:
        return _utils.paginate_all(self._get_page)

    @staticmethod
    def new(
        auth: Credentials,
        per_page: int,
    ) -> AlertChannelsClient:
        return AlertChannelsClient(RawClient.new(auth, 10), per_page)
