from dataclasses import (
    dataclass,
)

from etl_utils import smash
from fa_purity import (
    FrozenList,
    ResultE,
)
from fa_purity.json import (
    JsonObj,
    JsonUnfolder,
    Unfolder,
)

from tap_checkly.api._utils import (
    to_bool,
    to_datetime,
    to_int,
    to_opt_datetime,
    to_opt_str,
    to_str,
)
from tap_checkly.objs import (
    AlertChannelId,
    ChannelSubscription,
    CheckGroup,
    CheckGroupId,
    CheckGroupObj,
    IndexedObj,
)


def _decode_ch_sub(raw: JsonObj) -> ResultE[ChannelSubscription]:
    return JsonUnfolder.require(raw, "activated", to_bool).bind(
        lambda activated: JsonUnfolder.require(raw, "alertChannelId", to_int)
        .map(AlertChannelId)
        .map(lambda channel: ChannelSubscription(activated, channel)),
    )


def _decode_subs(raw: JsonObj) -> ResultE[FrozenList[ChannelSubscription]]:
    return JsonUnfolder.optional(
        raw,
        "alertChannelSubscriptions",
        lambda v: Unfolder.to_list_of(
            v,
            lambda j: Unfolder.to_json(j).bind(_decode_ch_sub),
        ),
    ).map(
        lambda m: m.value_or(()),
    )


def _decode_locations(raw: JsonObj) -> ResultE[FrozenList[str]]:
    return JsonUnfolder.optional(
        raw,
        "locations",
        lambda v: Unfolder.to_list_of(v, to_str),
    ).map(
        lambda m: m.value_or(()),
    )


@dataclass(frozen=True)
class CheckGroupDecoder:
    raw: JsonObj

    def decode_check(self) -> ResultE[CheckGroup]:
        raw = self.raw
        group_1 = smash.smash_result_5(
            JsonUnfolder.require(raw, "activated", to_bool),
            JsonUnfolder.require(raw, "concurrency", to_int),
            JsonUnfolder.require(raw, "name", to_str),
            _decode_subs(raw),
            JsonUnfolder.require(raw, "created_at", to_datetime),
        )
        group_2 = smash.smash_result_5(
            JsonUnfolder.optional(raw, "updated_at", to_opt_datetime).map(
                lambda m: m.bind(lambda x: x),
            ),
            JsonUnfolder.require(raw, "doubleCheck", to_bool),
            _decode_locations(raw),
            JsonUnfolder.require(raw, "muted", to_bool),
            JsonUnfolder.optional(raw, "runtimeId", to_opt_str).map(lambda m: m.bind(lambda x: x)),
        )
        super_group = smash.smash_result_3(
            group_1,
            group_2,
            JsonUnfolder.require(raw, "useGlobalAlertSettings", to_bool),
        )
        return super_group.map(
            lambda g: CheckGroup(*g[0], *g[1], g[2]),
        )

    def decode_id(self) -> ResultE[CheckGroupId]:
        return JsonUnfolder.require(self.raw, "id", to_int).map(CheckGroupId)

    def decode_obj(self) -> ResultE[CheckGroupObj]:
        return self.decode_id().bind(
            lambda cid: self.decode_check().map(lambda c: IndexedObj(cid, c)),
        )
