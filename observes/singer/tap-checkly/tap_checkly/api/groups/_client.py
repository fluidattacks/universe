from __future__ import (
    annotations,
)

from dataclasses import (
    dataclass,
)

from fa_purity import (
    Cmd,
    FrozenList,
    PureIterFactory,
    Stream,
    Unsafe,
)
from fa_purity.json import UnfoldedFactory

from tap_checkly.api import (
    _utils,
)
from tap_checkly.api._raw import (
    Credentials,
    RawClient,
)
from tap_checkly.objs import (
    CheckGroupObj,
)

from ._decode import (
    CheckGroupDecoder,
)


@dataclass(frozen=True)
class CheckGroupClient:
    _client: RawClient
    _per_page: int

    def _get_page(self, page: int) -> Cmd[FrozenList[CheckGroupObj]]:
        return self._client.new_get_list(
            "/v1/check-groups",
            UnfoldedFactory.from_dict(
                {
                    "limit": self._per_page,
                    "page": page,
                },
            ),
        ).map(
            lambda items: PureIterFactory.pure_map(
                lambda i: CheckGroupDecoder(i).decode_obj().alt(Unsafe.raise_exception).to_union(),
                items,
            ).to_list(),
        )

    def list_all(self) -> Stream[CheckGroupObj]:
        return _utils.paginate_all(self._get_page)

    @staticmethod
    def new(
        auth: Credentials,
        per_page: int,
    ) -> CheckGroupClient:
        return CheckGroupClient(RawClient.new(auth, 10), per_page)
