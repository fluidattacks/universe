from etl_utils import smash
from fa_purity import (
    FrozenDict,
    Maybe,
    Result,
    ResultE,
)
from fa_purity.json import (
    JsonObj,
    JsonUnfolder,
    Unfolder,
)

from tap_checkly._utils import switch_maybe
from tap_checkly.api._utils import (
    to_float,
    to_int,
    to_opt_str,
    to_str,
)
from tap_checkly.objs.result import (
    ApiCheckResult,
    CheckResponse,
    TimingPhases,
    Timings,
)


def _decode_timings(raw: JsonObj) -> ResultE[Timings]:
    group_1 = smash.smash_result_5(
        JsonUnfolder.require(raw, "socket", to_float),
        JsonUnfolder.require(raw, "lookup", to_float),
        JsonUnfolder.require(raw, "connect", to_float),
        JsonUnfolder.require(raw, "response", to_float),
        JsonUnfolder.require(raw, "end", to_float),
    )
    return group_1.map(lambda g1: Timings(*g1))


def _decode_timing_phases(raw: JsonObj) -> ResultE[TimingPhases]:
    group_1 = smash.smash_result_5(
        JsonUnfolder.require(raw, "wait", to_float),
        JsonUnfolder.require(raw, "dns", to_float),
        JsonUnfolder.require(raw, "tcp", to_float),
        JsonUnfolder.require(raw, "firstByte", to_float),
        JsonUnfolder.require(raw, "download", to_float),
    )
    return group_1.bind(
        lambda g1: JsonUnfolder.require(raw, "total", to_float).map(
            lambda total: TimingPhases(*g1, total),
        ),
    )


def _decode_response(raw: JsonObj) -> ResultE[CheckResponse]:
    status = JsonUnfolder.require(raw, "status", to_int)
    status_txt = JsonUnfolder.require(raw, "statusText", to_str)
    timings = JsonUnfolder.optional(
        raw,
        "timings",
        lambda j: Unfolder.to_json(j).bind(_decode_timings),
    )
    timing_phases = JsonUnfolder.optional(
        raw,
        "timingPhases",
        lambda j: Unfolder.to_json(j).bind(_decode_timing_phases),
    )
    return status.bind(
        lambda s: status_txt.bind(
            lambda st: timings.bind(
                lambda t: timing_phases.map(
                    lambda tp: CheckResponse(s, st, t, tp),
                ),
            ),
        ),
    )


def _decode_maybe_response(raw: JsonObj) -> ResultE[Maybe[CheckResponse]]:
    if raw == FrozenDict({}):
        return Result.success(Maybe.empty())
    return _decode_response(raw).map(lambda x: Maybe.some(x))


def decode_result_api(raw: JsonObj) -> ResultE[ApiCheckResult]:
    error = JsonUnfolder.optional(
        raw,
        "requestError",
        to_opt_str,
    ).map(lambda m: m.bind(lambda x: x))
    response = (
        JsonUnfolder.optional(
            raw,
            "response",
            lambda v: Unfolder.to_json(v).map(_decode_maybe_response),
        )
        .bind(switch_maybe)
        .map(lambda m: m.bind(lambda x: x))
    )
    return error.bind(lambda e: response.map(lambda r: ApiCheckResult(e, r)))
