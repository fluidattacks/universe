import inspect
import logging

from etl_utils import smash
from etl_utils.bug import Bug
from fa_purity import (
    ResultE,
)
from fa_purity.json import (
    JsonObj,
    JsonUnfolder,
    Unfolder,
)

from tap_checkly.api._utils import (
    to_bool,
    to_datetime,
    to_int,
    to_str,
)
from tap_checkly.objs import (
    CheckId,
    CheckResult,
    CheckResultId,
    CheckResultObj,
    CheckRunId,
    IndexedObj,
)

from . import (
    _api_result,
    _browser,
)

LOG = logging.getLogger(__name__)


def from_raw_result(raw: JsonObj) -> ResultE[CheckResult]:
    api_result = JsonUnfolder.optional(
        raw,
        "apiCheckResult",
        lambda v: Unfolder.to_json(v).bind(_api_result.decode_result_api),
    )
    browser_result = JsonUnfolder.optional(
        raw,
        "browserCheckResult",
        lambda v: Unfolder.to_json(v).bind(_browser.decode_browser_result),
    )
    group_1 = smash.smash_result_5(
        api_result,
        browser_result,
        JsonUnfolder.require(raw, "attempts", to_int),
        JsonUnfolder.require(raw, "checkRunId", to_int).map(CheckRunId),
        JsonUnfolder.require(raw, "created_at", to_datetime),
    )
    group_2 = smash.smash_result_5(
        JsonUnfolder.require(raw, "hasErrors", to_bool),
        JsonUnfolder.require(raw, "hasFailures", to_bool),
        JsonUnfolder.require(raw, "isDegraded", to_bool),
        JsonUnfolder.require(raw, "overMaxResponseTime", to_bool),
        JsonUnfolder.require(raw, "responseTime", to_int),
    )
    group_3 = smash.smash_result_3(
        JsonUnfolder.require(raw, "runLocation", to_str),
        JsonUnfolder.require(raw, "startedAt", to_datetime),
        JsonUnfolder.require(raw, "stoppedAt", to_datetime),
    )
    super_group = smash.smash_result_3(
        group_1,
        group_2,
        group_3,
    )
    return super_group.map(
        lambda g: CheckResult(*g[0], *g[1], *g[2]),
    )


def from_raw_obj(check_id: CheckId, raw: JsonObj) -> ResultE[CheckResultObj]:
    _id = JsonUnfolder.require(raw, "id", to_str).map(CheckResultId)
    _obj = from_raw_result(raw)
    return _id.bind(
        lambda i: _obj.map(lambda obj: IndexedObj((check_id, i), obj)),
    ).alt(
        lambda e: Bug.new(
            "check_result_obj_decode",
            inspect.currentframe(),
            e,
            (str(raw),),
        ),
    )
