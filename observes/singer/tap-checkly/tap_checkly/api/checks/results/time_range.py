from __future__ import (
    annotations,
)

from dataclasses import (
    dataclass,
)
from datetime import (
    datetime,
    timedelta,
)

from fa_purity import (
    PureIter,
    PureIterFactory,
    PureIterTransform,
    Result,
    ResultE,
    Unsafe,
)


class OutOfRangeError(Exception):
    pass


@dataclass(frozen=True)
class _DateRange:
    from_date: datetime
    to_date: datetime


@dataclass(frozen=True)
class DateRange(_DateRange):
    def __init__(self, obj: _DateRange) -> None:
        super().__init__(**obj.__dict__)  # type: ignore [misc]

    @staticmethod
    def new(from_date: datetime, to_date: datetime) -> ResultE[DateRange]:
        delta = to_date - from_date
        if delta <= timedelta(hours=6):
            item = DateRange(_DateRange(from_date, to_date))
            return Result.success(item)
        return Result.failure(
            OutOfRangeError("time delta of DateRange must be <= 6h"),
            DateRange,
        ).alt(Exception)


def _lower_limit(date: datetime, limit: datetime) -> datetime:
    return max(date, limit)


def date_ranges_dsc(
    from_date: datetime,
    to_date: datetime,
) -> PureIter[DateRange]:
    return (
        PureIterFactory.infinite_range(0, 6)
        .map(
            lambda h: DateRange.new(
                _lower_limit(to_date - timedelta(hours=h + 6), from_date),
                to_date - timedelta(hours=h),
            )
            .alt(Unsafe.raise_exception)
            .to_union(),
        )
        .map(lambda dr: None if dr.to_date <= from_date else dr)
        .transform(lambda x: PureIterTransform.until_none(x))
    )
