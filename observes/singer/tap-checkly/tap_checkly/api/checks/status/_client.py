from __future__ import (
    annotations,
)

import inspect
import logging
from dataclasses import (
    dataclass,
)

from etl_utils.bug import (
    Bug,
)
from fa_purity import (
    Cmd,
    FrozenDict,
    FrozenList,
    Maybe,
    PureIterFactory,
    PureIterTransform,
    Stream,
    StreamFactory,
    StreamTransform,
)
from fa_purity.json import (
    JsonObj,
    JsonUnfolder,
)

from tap_checkly.api._raw import (
    Credentials,
    RawClient,
)
from tap_checkly.objs import (
    CheckStatusObj,
)

from ._decode import (
    CheckStatusDecoder,
)

LOG = logging.getLogger(__name__)


@dataclass(frozen=True)
class CheckStatusClient:
    _client: RawClient
    _per_page: int

    def get_page(self) -> Cmd[FrozenList[CheckStatusObj]]:
        def _warn_ignored(
            raw: JsonObj,
            decoded: Maybe[CheckStatusObj],
        ) -> Maybe[CheckStatusObj]:
            if not decoded.map(lambda _: True).value_or(False):
                LOG.warning(
                    "Ignored check status: %s",
                    JsonUnfolder.dumps(raw),
                )
            return decoded

        return self._client.new_get_list(
            "/v1/check-statuses",
            FrozenDict({}),
        ).map(
            lambda j: PureIterFactory.from_list(j)
            .map(
                lambda i: Bug.assume_success(
                    "check_status_decode",
                    inspect.currentframe(),
                    (JsonUnfolder.dumps(i),),
                    CheckStatusDecoder(i).decode_obj().map(lambda m: _warn_ignored(i, m)),
                ),
            )
            .transform(lambda p: PureIterTransform.filter_maybe(p))
            .to_list(),
        )

    def list_all(self) -> Stream[CheckStatusObj]:
        return (
            PureIterFactory.from_list((self.get_page(),))
            .transform(lambda x: StreamFactory.from_commands(x))
            .transform(
                lambda x: StreamTransform.chain(
                    x.map(PureIterFactory.from_list),
                ),
            )
        )

    @staticmethod
    def new(
        auth: Credentials,
        per_page: int,
    ) -> CheckStatusClient:
        return CheckStatusClient(RawClient.new(auth, 10), per_page)
