import sys

from etl_utils.typing import (
    TypeVar,
)
from fa_purity import (
    Cmd,
    PureIter,
    PureIterTransform,
    Stream,
    StreamTransform,
)
from fa_singer_io.singer import (
    SingerMessage,
    SingerRecord,
    emitter,
)

from tap_checkly.singer import (
    ObjEncoder,
)
from tap_checkly.state import (
    EtlState,
)

from ._state import (
    encode_state,
)

_T = TypeVar("_T")


def emit_items(
    records: PureIter[SingerRecord],
) -> Cmd[None]:
    emissions = records.map(lambda s: emitter.emit(sys.stdout, SingerMessage.from_record(s)))
    return PureIterTransform.consume(emissions)


def emit_singer_records(
    records: Stream[SingerRecord] | PureIter[SingerRecord],
) -> Cmd[None]:
    if isinstance(records, Stream):
        emissions = records.map(lambda s: emitter.emit(sys.stdout, SingerMessage.from_record(s)))
        return StreamTransform.consume(emissions)
    emissions_2 = records.map(lambda s: emitter.emit(sys.stdout, SingerMessage.from_record(s)))
    return PureIterTransform.consume(emissions_2)


def from_encoder(
    encoder: ObjEncoder[_T],
    items: Stream[_T] | PureIter[_T],
) -> Cmd[None]:
    schemas = encoder.schemas.map(
        lambda s: emitter.emit(sys.stdout, SingerMessage.from_schema(s)),
    ).transform(PureIterTransform.consume)
    if isinstance(items, Stream):
        _data = items.map(encoder.record).transform(lambda x: StreamTransform.chain(x))
        return schemas + emit_singer_records(_data)
    _data_2 = items.map(encoder.record).transform(
        lambda x: PureIterTransform.chain(x),
    )
    return schemas + emit_singer_records(_data_2)


def emit_state(state: EtlState) -> Cmd[None]:
    return emitter.emit(sys.stdout, SingerMessage.from_state(encode_state(state)))
