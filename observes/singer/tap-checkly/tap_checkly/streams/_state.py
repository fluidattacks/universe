from fa_purity import (
    FrozenDict,
    Maybe,
    ResultE,
)
from fa_purity.json import (
    JsonObj,
    JsonPrimitiveUnfolder,
    JsonUnfolder,
    JsonValueFactory,
    UnfoldedFactory,
    Unfolder,
)
from fa_singer_io.singer import (
    SingerState,
)

from tap_checkly._utils import (
    DateInterval,
    isoparse,
)
from tap_checkly.state import (
    EtlState,
)


def _encode_interval(interval: DateInterval) -> JsonObj:
    return UnfoldedFactory.from_dict(
        {
            "newest": interval.newest.isoformat(),
            "oldest": interval.oldest.isoformat(),
        },
    )


def encode(state: EtlState) -> JsonObj:
    return FrozenDict(
        {
            "results": JsonValueFactory.from_unfolded(
                state.results.map(_encode_interval).value_or(None),
            ),
        },
    )


def encode_state(state: EtlState) -> SingerState:
    return SingerState(encode(state))


def _decode_interval(raw: JsonObj) -> ResultE[DateInterval]:
    return JsonUnfolder.require(
        raw,
        "newest",
        lambda v: Unfolder.to_primitive(v).bind(JsonPrimitiveUnfolder.to_str).bind(isoparse),
    ).bind(
        lambda newest: JsonUnfolder.require(
            raw,
            "oldest",
            lambda v: Unfolder.to_primitive(v).bind(JsonPrimitiveUnfolder.to_str).bind(isoparse),
        ).bind(
            lambda oldest: DateInterval.new(oldest, newest),
        ),
    )


def decode(raw: JsonObj) -> ResultE[EtlState]:
    return JsonUnfolder.require(
        raw,
        "results",
        lambda v: Unfolder.to_optional(
            v,
            lambda v2: Unfolder.to_json(v2).bind(_decode_interval),
        ),
    ).map(lambda m: EtlState(Maybe.from_optional(m)))
