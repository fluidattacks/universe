import math
from datetime import (
    timedelta,
)

from fa_purity import (
    PureIter,
    PureIterFactory,
    Stream,
    StreamFactory,
    StreamTransform,
)
from fa_purity.date_time import (
    DatetimeUTC,
)

from tap_checkly.api.report import (
    CheckReportClient,
)
from tap_checkly.objs import (
    ReportObj,
)


def _date_range_per_day(
    start: DatetimeUTC,
    end: DatetimeUTC,
) -> PureIter[DatetimeUTC]:
    days = math.ceil((end.date_time - start.date_time) / timedelta(days=1))
    return PureIterFactory.from_range(range(days)).map(lambda d: start + timedelta(days=d))


def daily_reports(
    client: CheckReportClient,
    from_date: DatetimeUTC,
    to_date: DatetimeUTC,
) -> Stream[ReportObj]:
    items = (
        _date_range_per_day(from_date, to_date)
        .map(lambda d: client.get_reports_obj(d, d + timedelta(days=1)))
        .transform(lambda x: StreamFactory.from_commands(x))
    )
    return items.map(lambda x: PureIterFactory.from_list(x)).transform(
        lambda s: StreamTransform.chain(s),
    )
