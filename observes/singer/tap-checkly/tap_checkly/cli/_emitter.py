import logging
from dataclasses import (
    dataclass,
)

from fa_purity import (
    Cmd,
    FrozenList,
    Maybe,
    PureIterFactory,
    PureIterTransform,
    Unsafe,
)
from fa_purity.date_time import (
    DatetimeFactory,
    DatetimeUTC,
)

from tap_checkly.api import (
    Credentials,
)
from tap_checkly.state import (
    EtlState,
)
from tap_checkly.streams import (
    Streams,
    SupportedStreams,
)

LOG = logging.getLogger(__name__)
NOW = Unsafe.compute(DatetimeFactory.date_now())


@dataclass(frozen=True)
class Emitter:
    state: EtlState
    creds: Credentials
    reports_start: Maybe[DatetimeUTC]

    def emit_stream(self, selection: SupportedStreams) -> Cmd[None]:
        _streams = Streams(self.creds, DatetimeFactory.EPOCH_START, NOW)

        def stream_mapper(  # pylint: disable=inconsistent-return-statements
            # reason for `inconsistent-return-statements`
            # it is a bug; function always returns the correct type
            item: SupportedStreams,
        ) -> Cmd[None]:  # return type should be Cmd[None]
            if item is SupportedStreams.CHECKS:
                return _streams.all_checks()
            if item is SupportedStreams.CHECK_GROUPS:
                return _streams.check_groups()
            if item is SupportedStreams.CHECK_RESULTS:
                return _streams.check_results(self.state)
            if item is SupportedStreams.ALERT_CHS:
                return _streams.alert_chs()
            if item is SupportedStreams.CHECK_STATUS:
                return _streams.check_status()
            if item is SupportedStreams.REPORTS:  # noqa: RET503 false positive
                warn = Cmd.wrap_impure(
                    lambda: LOG.warning("`REPORTS` stream skipped"),
                )
                return self.reports_start.map(
                    lambda d: _streams.check_reports(d, NOW),
                ).value_or(warn)

        def _execute(item: SupportedStreams) -> Cmd[None]:
            info = Cmd.wrap_impure(lambda: LOG.info("Executing stream: %s", item))
            return info + stream_mapper(item)

        return _execute(selection)

    def emit_streams(self, targets: FrozenList[SupportedStreams]) -> Cmd[None]:
        emissions = PureIterFactory.pure_map(self.emit_stream, targets)
        return PureIterTransform.consume(emissions)
