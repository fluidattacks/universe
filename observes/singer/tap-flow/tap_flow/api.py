from collections.abc import (
    Callable,
    Generator,
)
from datetime import (
    datetime,
    timezone,
)
import functools
import json
import sys
from tap_flow import (
    logs,
)
import time
from typing import (
    Any,
    NamedTuple,
    ParamSpec,
    TypedDict,
)
import urllib.error
import urllib.request
from uuid import (
    uuid4,
)

FlowAPIResult = dict[str, Any]  # type: ignore[misc]
P = ParamSpec("P")


class FlowAPIResponse(TypedDict):
    next: str | None
    previous: str | None
    results: list[FlowAPIResult]


class APIResponse(NamedTuple):
    response: FlowAPIResponse | None
    status_code: int


class Commit(NamedTuple):
    author: str
    author_date: datetime
    churn: float
    committer_date: datetime
    deletions: float
    files: float
    haloc: float
    help_others: float
    hexsha: str
    hunks: float
    id: str
    impact: float
    ins_del_ratio: float
    insertions: float
    is_outlier: bool
    is_trivial: bool
    legacy_refactor: float
    levenshtein: float
    message: str
    new_work: float
    risk: float


class FlowRepository(NamedTuple):
    created_at: datetime
    id: int
    name: str
    last_failure_or_success_at: datetime | None
    last_fetch_failure_reason: str | None
    last_fully_updated_at: datetime | None
    last_processing_success_at: datetime | None
    updated_at: datetime | None
    url: str
    vendor_type: str


class User(NamedTuple):
    email: str
    id: int
    name: str


class APIError(Exception):
    def __init__(self) -> None:
        msg = "There was an error in the call to the API"
        super().__init__(msg)


class RepositoryNotFound(Exception):
    def __init__(self) -> None:
        msg = "Repository not found in Flow"
        super().__init__(msg)


def _format_flow_date(date: str) -> datetime:
    if "." in date:
        date = date.split(".")[0]
    formatted_date = datetime.strptime(date, "%Y-%m-%dT%H:%M:%S").replace(
        tzinfo=timezone.utc
    )

    return formatted_date


def retry_on_errors(
    func: Callable[P, APIResponse]
) -> Callable[P, APIResponse]:
    """Decorate function to retry if an error is raised."""

    @functools.wraps(func)
    def decorated(*args: P.args, **kwargs: P.kwargs) -> APIResponse:
        """Retry the function if status code is not 200."""
        for _ in range(5):
            response = func(*args, **kwargs)
            if response.status_code != 200:
                logs.error("Retrying due to API error...")
                time.sleep(5.0)
            else:
                break
        return response

    return decorated


class FlowAPI:
    api_token: str
    base_url: str

    def __init__(self, api_token: str) -> None:
        self.api_token = api_token
        self.base_url = "https://flow.pluralsight.com"
        self.last_request_timestamp = datetime.now().timestamp()
        self.min_request_interval = 0.5

    @retry_on_errors
    def _request(self, path: str) -> APIResponse:
        response: FlowAPIResponse | None = None
        status_code: int | None = None

        self.wait()
        try:
            headers = {
                "Authorization": f"Bearer {self.api_token}",
                "User-Agent": (
                    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) "
                    "AppleWebKit/605.1.15 (KHTML, like Gecko) Version/17.2.1 "
                    "Safari/605.1.15"
                ),
            }

            request = urllib.request.Request(path, headers=headers)
            with urllib.request.urlopen(request, timeout=120) as result:  # type: ignore[misc]
                response = json.loads(result.read().decode("utf-8"))  # type: ignore[misc]
                status_code = 200
                self.last_request_timestamp = datetime.now().timestamp()
        except urllib.error.HTTPError as error:
            status_code = error.code
            if status_code == 401:
                logs.error("Invalid token or credentials")
                sys.exit(1)
            elif status_code == 403:
                logs.error("Unauthorized/Forbidden")
                sys.exit(1)

        return APIResponse(status_code=status_code, response=response)

    def get_repository_id(self, repo_name: str) -> int:
        flow_repos = self.list_repos()
        repo = [
            flow_repo
            for flow_repo in flow_repos
            if flow_repo.name.lower() == repo_name.lower()
        ]
        if len(repo) == 0:
            raise RepositoryNotFound()

        return repo[0].id

    def get_repository_commits(
        self, repo_id: int, since: datetime | None
    ) -> Generator[list[Commit], None, None]:
        api_path = (
            "/v3/customer/core/commits/"
            f"?repo_id={repo_id}&ordering=author_date&limit=1000"
        )
        if since is not None:
            since_str = datetime.strftime(since, "%Y-%m-%d %H:%M:%S")
            api_path += f"&committer_date__gte={since_str}".replace(" ", "%20")
        users = self.get_users()
        users_by_id = {user.id: user.email for user in users}
        for raw_commits in self.request(self.base_url + api_path):
            commits = [
                Commit(
                    author=users_by_id[int(commit["user_alias_id"])],
                    author_date=_format_flow_date(str(commit["author_date"])),
                    churn=float(commit["churn"]),
                    committer_date=_format_flow_date(
                        str(commit["committer_date"])
                    ),
                    deletions=float(commit["deletions"]),
                    files=float(commit["files"]),
                    haloc=float(commit["haloc"]),
                    help_others=float(commit["help_others"]),
                    hexsha=str(commit["hexsha"]),
                    hunks=float(commit["hunks"]),
                    id=str(uuid4()),
                    impact=float(commit["impact"]),
                    ins_del_ratio=float(commit["ins_del_ratio"]),
                    insertions=float(commit["insertions"]),
                    is_outlier=bool(commit["is_outlier"]),
                    is_trivial=bool(commit["is_trivial"]),
                    legacy_refactor=float(commit["legacy_refactor"]),
                    levenshtein=float(commit["levenshtein"]),
                    message=str(commit["message"]),
                    new_work=float(commit["new_work"]),
                    risk=float(commit["risk"]),
                )
                for commit in raw_commits
            ]
            yield commits

    def get_users(self) -> list[User]:
        api_path = "/v3/customer/core/user_alias/?limit=1000"
        users = []
        for user_aliases in self.request(self.base_url + api_path):
            users.extend(
                [
                    User(
                        email=str(user["email"]),
                        id=int(user["id"]),
                        name=str(user["name"]),
                    )
                    for user in user_aliases
                ]
            )

        return users

    def list_repos(self) -> list[FlowRepository]:
        api_path = "/v3/customer/core/repos/"
        repos = []
        for repositories in self.request(self.base_url + api_path):
            repos.extend(
                [
                    FlowRepository(
                        created_at=_format_flow_date(str(repo["created_at"])),
                        id=int(repo["id"]),
                        name=str(repo["name"]),
                        last_failure_or_success_at=(
                            _format_flow_date(
                                str(repo["last_failure_or_success_at"])
                            )
                            if repo["last_failure_or_success_at"] is not None
                            else None
                        ),
                        last_fetch_failure_reason=(
                            str(repo["last_fetch_failure_reason"])
                            if repo["last_fetch_failure_reason"] is not None
                            else None
                        ),
                        last_fully_updated_at=(
                            _format_flow_date(
                                str(repo["last_fully_updated_at"])
                            )
                            if repo["last_fully_updated_at"] is not None
                            else None
                        ),
                        last_processing_success_at=(
                            _format_flow_date(
                                str(repo["last_processing_success_at"])
                            )
                            if repo["last_processing_success_at"] is not None
                            else None
                        ),
                        updated_at=(
                            _format_flow_date(str(repo["updated_at"]))
                            if repo["updated_at"] is not None
                            else None
                        ),
                        url=str(repo["url"]),
                        vendor_type=str(repo["vendor_type"]),
                    )
                    for repo in repositories
                ]
            )

        return repos

    def request(self, path: str) -> Generator[list[FlowAPIResult], None, None]:
        request_path: str | None = path
        while request_path is not None:
            response = self._request(request_path)
            if response.status_code == 200 and response.response is not None:
                results = response.response["results"]
                request_path = response.response["next"]
                yield results
            else:
                raise APIError()

    def time_since_last_request(self) -> float:
        return datetime.now().timestamp() - self.last_request_timestamp

    def wait(self) -> None:
        time.sleep(
            max(
                self.min_request_interval - self.time_since_last_request(), 0.0
            )
        )
