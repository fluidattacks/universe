import click
from datetime import (
    datetime,
    timedelta,
    timezone,
)
import json
import os
from tap_flow import (
    logs,
)
from tap_flow.api import (
    Commit,
    FlowAPI,
)
from tap_flow.utils import (
    get_universe_commits,
)
from typing import (
    Callable,
    TypedDict,
)

_TYPE_BOOLEAN = {"type": "boolean"}
_TYPE_DATE = {"type": "string", "format": "date-time"}
_TYPE_NUMBER = {"type": "number"}
_TYPE_STRING = {"type": "string"}


class Record(TypedDict):
    record: dict[str, float | str]
    stream: str
    type: str


class SchemaProperties(TypedDict):
    properties: dict[str, dict[str, str]]


class Schema(TypedDict):
    key_properties: list[str]
    schema: SchemaProperties
    stream: str
    type: str


def get_record(commit: Commit) -> str:
    record: Record = {
        "type": "RECORD",
        "stream": "commits",
        "record": {
            k: (
                v
                if not isinstance(v, datetime)
                else datetime.strftime(v, "%Y-%m-%dT%H:%M:%SZ")
            )
            for k, v in commit._asdict().items()
        },
    }

    return json.dumps(record)


def get_schema() -> str:
    schema: Schema = {
        "type": "SCHEMA",
        "stream": "commits",
        "key_properties": ["id"],
        "schema": {
            "properties": {
                "id": _TYPE_STRING,
                "author": _TYPE_STRING,
                "author_date": _TYPE_DATE,
                "churn": _TYPE_NUMBER,
                "committer_date": _TYPE_DATE,
                "deletions": _TYPE_NUMBER,
                "files": _TYPE_NUMBER,
                "haloc": _TYPE_NUMBER,
                "help_others": _TYPE_NUMBER,
                "hexsha": _TYPE_STRING,
                "hunks": _TYPE_NUMBER,
                "impact": _TYPE_NUMBER,
                "ins_del_ratio": _TYPE_NUMBER,
                "insertions": _TYPE_NUMBER,
                "is_outlier": _TYPE_BOOLEAN,
                "is_trivial": _TYPE_BOOLEAN,
                "legacy_refactor": _TYPE_NUMBER,
                "levenshtein": _TYPE_NUMBER,
                "message": _TYPE_STRING,
                "new_work": _TYPE_NUMBER,
                "risk": _TYPE_NUMBER,
            }
        },
    }

    return json.dumps(schema)


@click.command()
@click.option(
    "--all",
    is_flag=True,
    help=(
        "Fetch all commit data available in Flow. "
        "Takes precedence over --last-week, --since and --yesterday"
    ),
)
@click.option(
    "--last_week",
    is_flag=True,
    help=(
        "Fetch commit data since 7 days ago UTC time. "
        "Takes precedence over --since and --yesterday"
    ),
)
@click.option(
    "--repository",
    type=str,
    required=True,
    help="Name of the repository for which to extract commit information",
)
@click.option(
    "--since",
    type=str,
    required=False,
    help="Fetch commit data since the specified date in ISO format",
)
@click.option(
    "--yesterday",
    is_flag=True,
    help=(
        "Fetch commit data since yesterday UTC time. "
        "Takes precedence over --since"
    ),
)
def main(
    repository: str,
    all: bool = False,  # pylint: disable=redefined-builtin
    last_week: bool = False,
    yesterday: bool = False,
    since: str | None = None,
) -> None:
    if all:
        date = None
    elif last_week:
        date = datetime.now(timezone.utc).replace(
            hour=0, minute=0, second=0, microsecond=0
        ) + timedelta(days=-7)
    elif yesterday:
        date = datetime.now(timezone.utc).replace(
            hour=0, minute=0, second=0, microsecond=0
        ) + timedelta(days=-1)
    elif since:
        date = datetime.strptime(since, "%Y-%m-%dT%H:%M:%S%z")
    else:
        return

    flow_api = FlowAPI(api_token=os.environ["FLOW_API_TOKEN"])
    repo_id = flow_api.get_repository_id(repository)

    trunk_commits = get_universe_commits()
    unique_commits = []
    flow_commits: list[Commit] = []

    logs.info(get_schema())
    for commits in flow_api.get_repository_commits(repo_id, date):
        for commit in commits:
            # Filter duplicated commits fetched from the API
            if commit.hexsha in unique_commits:
                continue
            unique_commits.append(commit.hexsha)

            # Filter commits that were made to development branches
            # but never made it to trunk
            if commit.hexsha in trunk_commits:
                flow_commits.append(commit)

    sort_fn: Callable[[Commit], datetime] = lambda x: x.committer_date
    for commit in sorted(flow_commits, key=sort_fn):
        logs.info(get_record(commit))
