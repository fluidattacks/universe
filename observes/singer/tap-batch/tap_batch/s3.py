from __future__ import (
    annotations,
)

import boto3
from dataclasses import (
    dataclass,
)
from fa_purity import (
    FrozenDict,
)
from json import (
    loads,
)
from mypy_boto3_s3.client import (
    S3Client,
)


@dataclass(frozen=True)
class S3URI:
    bucket: str
    obj_key: str

    @staticmethod
    def new(uri: str) -> S3URI:
        s3_prefix = "s3://"
        if uri.startswith(s3_prefix):
            uri_elements = uri.removeprefix(s3_prefix).split("/", 1)
            if len(uri_elements) != 2:
                raise ValueError(f"invalid s3 URI i.e. {uri}")
            return S3URI(uri_elements[0], uri_elements[1])
        raise ValueError(f"invalid s3 URI i.e. {uri}")


@dataclass(frozen=True)
class S3Extractor:
    uri: S3URI
    client: S3Client

    @staticmethod
    def new(uri: str) -> S3Extractor:
        s3_uri = S3URI.new(uri)
        s3_client = boto3.client("s3", region_name="us-east-1")
        return S3Extractor(s3_uri, s3_client)

    def obj_exist(self) -> bool:
        try:
            self.client.get_object(
                Bucket=self.uri.bucket, Key=self.uri.obj_key
            )
            return True
        except (
            self.client.exceptions.NoSuchBucket,
            self.client.exceptions.NoSuchKey,
        ):
            return False

    def obj_is_empty(self) -> bool:
        try:
            # pylint false positive
            s3_response = self.client.head_object(  # pylint: disable=assignment-from-no-return
                Bucket=self.uri.bucket, Key=self.uri.obj_key
            )
            bytes_size = s3_response["ContentLength"]
            if bytes_size > 0:
                return False
            return True
        except Exception as e:
            raise ValueError("Error validating file in S3") from e

    def get_state_content(self) -> FrozenDict[str, str]:
        try:
            # pylint false positive
            s3_response = self.client.get_object(  # pylint: disable=assignment-from-no-return
                Bucket=self.uri.bucket, Key=self.uri.obj_key
            )
            parsed_data: FrozenDict[str, str] = loads(
                s3_response["Body"].read().decode("utf-8")
            )
            return parsed_data
        except Exception as e:
            raise ValueError("Error parsing state content from S3") from e
