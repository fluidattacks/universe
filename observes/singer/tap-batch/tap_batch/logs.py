from logging import (
    ERROR,
    Formatter,
    getLogger,
    INFO,
    Logger,
    StreamHandler,
)
import sys

_FORMATTER = Formatter("[%(levelname)s] %(message)s")
_ERROR_HANDLER = StreamHandler(sys.stderr)
_ERROR_HANDLER.setFormatter(_FORMATTER)
_ERROR_HANDLER.setLevel(ERROR)

_INFO_HANDLER = StreamHandler(sys.stdout)
_INFO_HANDLER.setLevel(INFO)
_INFO_HANDLER.addFilter(lambda x: x.levelno == INFO)

LOGGER: Logger = getLogger(__name__)
LOGGER.setLevel(INFO)
LOGGER.addHandler(_INFO_HANDLER)
LOGGER.addHandler(_ERROR_HANDLER)
LOGGER.propagate = True


def error(msg: str) -> None:
    LOGGER.error(msg)


def info(msg: str) -> None:
    LOGGER.info(msg)
