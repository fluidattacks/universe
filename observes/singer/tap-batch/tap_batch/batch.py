from __future__ import (
    annotations,
)

import boto3
from dataclasses import (
    dataclass,
)
from datetime import (
    datetime,
)
from fa_purity import (
    FrozenList,
)
from mypy_boto3_batch.client import (
    BatchClient,
)
from tap_batch.state import (
    SingerState,
)
from typing import (
    Literal,
    NamedTuple,
)


class JobRecord(NamedTuple):
    id: str
    name: str
    created_at: datetime
    started_at: datetime
    stopped_at: datetime
    status: str
    status_reason: str


@dataclass(frozen=True)
class BatchExtractor:
    queue: str
    state: str | None
    client: BatchClient

    @staticmethod
    def new(queue: str, state: str | None) -> BatchExtractor:
        batch_client = boto3.client("batch", region_name="us-east-1")
        return BatchExtractor(queue, state, batch_client)

    def get_unprocessed_jobs(self) -> list[JobRecord]:
        jobs: list[JobRecord] = []
        statuses: list[Literal["SUCCEEDED", "FAILED"]] = [
            "SUCCEEDED",
            "FAILED",
        ]

        def save_batch_jobs(
            job_status: Literal["SUCCEEDED", "FAILED"],
            next_token: str | None,
        ) -> str | None:
            batch_response = (
                self.client.list_jobs(
                    jobQueue=self.queue,
                    jobStatus=job_status,
                    nextToken=next_token,
                )
                if next_token
                else self.client.list_jobs(
                    jobQueue=self.queue,
                    jobStatus=job_status,
                )
            )
            for job in batch_response["jobSummaryList"]:
                jobs.append(
                    JobRecord(
                        id=job["jobId"],
                        name=job["jobName"],
                        created_at=datetime.fromtimestamp(
                            job["createdAt"] / 1000
                        ),
                        started_at=datetime.fromtimestamp(
                            job["startedAt"] / 1000
                        )
                        if job.get("startedAt")
                        else datetime.fromtimestamp(job["stoppedAt"] / 1000),
                        stopped_at=datetime.fromtimestamp(
                            job["stoppedAt"] / 1000
                        ),
                        status=job["status"],
                        status_reason=job["statusReason"],
                    )
                )
            return batch_response.get("nextToken")

        for job_status in statuses:
            if next_token := save_batch_jobs(job_status, None):
                while next_token is not None:
                    next_token = save_batch_jobs(job_status, next_token)

        return jobs

    def get_sorted_jobs(
        self, jobs_list: list[JobRecord]
    ) -> FrozenList[JobRecord]:
        sorted_list: FrozenList[JobRecord] = tuple(
            sorted(
                jobs_list,
                key=lambda job: (  # type: ignore[misc]
                    job.created_at,
                    job.started_at,
                    job.stopped_at,
                    job.name,
                ),
            )
        )
        return sorted_list

    def get_filtered_jobs(
        self, jobs_list: FrozenList[JobRecord], state_datetime: datetime
    ) -> FrozenList[JobRecord]:
        filtered_jobs: FrozenList[JobRecord] = tuple(
            job for job in jobs_list if job.created_at > state_datetime
        )
        return filtered_jobs

    def get_jobs(self) -> FrozenList[JobRecord]:
        state_datetime = (
            singer_state.get_state_datetime()
            if self.state
            and (singer_state := SingerState.new(self.state))
            and singer_state.is_valid_state()
            else None
        )

        unprocessed_jobs: list[JobRecord] = self.get_unprocessed_jobs()
        sorted_jobs: FrozenList[JobRecord] = self.get_sorted_jobs(
            unprocessed_jobs
        )

        filtered_jobs = (
            self.get_filtered_jobs(sorted_jobs, state_datetime)
            if state_datetime
            else sorted_jobs
        )

        return filtered_jobs
