{ lib, python_pkgs, }:
lib.buildPythonPackage rec {
  pname = "mypy-boto3-batch";
  version = "1.34.59";
  src = lib.fetchPypi {
    inherit pname version;
    sha256 = "rsXdh8f3KRAROftePejdLxChRqtiaDFsJyhctX7jRUQ=";
  };
  nativeBuildInputs = with python_pkgs; [ boto3 ];
  propagatedBuildInputs = with python_pkgs; [ botocore typing-extensions ];
}
