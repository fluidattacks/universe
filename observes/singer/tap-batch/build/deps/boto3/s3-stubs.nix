{ lib, python_pkgs, }:
lib.buildPythonPackage rec {
  pname = "mypy-boto3-s3";
  version = "1.34.65";
  src = lib.fetchPypi {
    inherit pname version;
    sha256 = "L830Es4pJLLws021mr8GqcC75M0zYfFPDSweIRwPfd0=";
  };
  nativeBuildInputs = with python_pkgs; [ boto3 ];
  propagatedBuildInputs = with python_pkgs; [ botocore typing-extensions ];
}
