from __future__ import (
    annotations,
)

from dataclasses import (
    dataclass,
    field,
)
from datetime import (
    datetime,
)
from dateutil.parser import (
    isoparse,
)
from fa_purity import (
    FrozenDict,
    FrozenList,
    Maybe,
    PureIter,
    Result,
    ResultE,
)
from fa_purity.date_time import (
    DatetimeFactory,
)
from fa_purity.json.primitive.core import (
    Primitive,
)
from fa_purity.json.primitive.factory import (
    to_opt_primitive,
)
from fa_purity.json_2 import (
    JsonObj,
    JsonPrimitiveUnfolder,
    JsonUnfolder,
    JsonValue,
    LegacyAdapter,
    Unfolder,
)
from fa_purity.pure_iter.factory import (
    pure_map,
    PureIterFactory,
)
from fa_purity.result.transform import (
    all_ok,
)
from fa_purity.utils import (
    cast_exception,
    raise_exception,
)
import logging
from target_s3.core import (
    CompletePlainRecord,
    PlainRecord,
    RecordGroup,
)
from typing import (
    FrozenSet,
    Tuple,
)

LOG = logging.getLogger(__name__)


def _truncate_row(
    row: FrozenList[Primitive], _limit: int
) -> FrozenList[Primitive]:
    limit = _limit if _limit >= -1 else -1

    def _truncate(prim: Primitive) -> Primitive:
        if isinstance(prim, str):
            return prim[0:limit] if limit >= 0 else prim
        return prim

    return tuple(_truncate(r) for r in row)


def _ordered_data(record: CompletePlainRecord) -> FrozenList[Primitive]:
    def _key(item: Tuple[str, Primitive]) -> str:
        return item[0]

    items = tuple(record.record.record.items())
    ordered = sorted(items, key=_key)
    return tuple(i[1] for i in ordered)


def _to_str(raw: JsonValue) -> ResultE[str]:
    return Unfolder.to_primitive(raw).bind(JsonPrimitiveUnfolder.to_str)


def _ensure_one_type(raw: FrozenList[str]) -> ResultE[str]:
    types = (
        PureIterFactory.from_list(raw).filter(lambda r: r != "null").to_list()
    )
    if len(types) == 1:
        return Result.success(types[0])
    if len(types) == 0:
        return Result.success("null")
    return Result.failure(
        ValueError(f"Multiple types not supported i.e. {types}"), str
    ).alt(cast_exception)


def _handle_multi_type(raw: JsonValue) -> ResultE[str]:
    return (
        Unfolder.to_list(raw)
        .bind(
            lambda i: all_ok(
                PureIterFactory.from_list(i).map(_to_str).to_list()
            )
        )
        .bind(_ensure_one_type)
    )


def _decode_type(raw: JsonValue) -> ResultE[str]:
    return _to_str(raw).lash(lambda _: _handle_multi_type(raw))


def _is_datetime(schema: JsonObj) -> ResultE[bool]:
    "schema represents a datetime type?"
    _type = JsonUnfolder.require(schema, "type", _decode_type)
    _format: ResultE[Maybe[str]] = JsonUnfolder.optional(
        schema,
        "format",
        lambda v: Unfolder.to_primitive(v).bind(JsonPrimitiveUnfolder.to_str),
    )
    return _type.bind(
        lambda t: _format.map(
            lambda f: t == "string" and f.value_or(None) == "date-time"
        )
    ).alt(
        lambda e: ValueError(
            f"Failed `_is_datetime` with input {schema} i.e. {e}"
        )
    )


def _to_datetime(raw: str) -> ResultE[datetime]:
    try:
        return Result.success(isoparse(raw))
    except ValueError as err:
        return Result.failure(
            ValueError(
                f"Failed _to_datetime transform with raw `{raw}` i.e. {err}"
            ),
            datetime,
        ).alt(cast_exception)


def _format_datetime_on_record(
    datetime_props: FrozenSet[str], record: CompletePlainRecord
) -> ResultE[CompletePlainRecord]:
    "Format datetime properties"
    default_date: datetime = DatetimeFactory.EPOCH_START.date_time

    def _to_prim(item: str) -> Primitive:
        return item

    def _adjust(key: str, value: Primitive) -> ResultE[Primitive]:
        if key in datetime_props:
            return (
                to_opt_primitive(value, str)
                .map(
                    lambda x: _to_prim(
                        _to_datetime(x)
                        .value_or(default_date)
                        .strftime("%Y-%m-%d %H:%M:%S")
                    )
                    if x
                    else None
                )
                .alt(
                    lambda e: Exception(
                        f"Failed `_adjust` with inputs ({key}, {value}) i.e. {e}"
                    )
                )
            )
        return Result.success(value)

    def _log(item: ResultE[Tuple[str, Primitive]]) -> bool:
        LOG.warning(item)
        return False

    _records = all_ok(
        pure_map(
            lambda p: _adjust(p[0], p[1]).map(lambda a: (p[0], a)),
            tuple(record.record.record.items()),
        )
        .filter(lambda r: r.map(lambda _: True).or_else_call(lambda: _log(r)))
        .transform(lambda x: tuple(x))
    ).map(lambda l: FrozenDict(dict(l)))
    return _records.bind(
        lambda r: CompletePlainRecord.new(
            record.schema, PlainRecord.new(record.schema.stream, r)
        )
    )


def _format_datetime_on_group(group: RecordGroup) -> RecordGroup:
    properties = JsonUnfolder.require(
        LegacyAdapter.json(group.schema.schema.encode()),
        "properties",
        Unfolder.to_json,
    )
    datetime_properties = (
        properties.map(lambda d: PureIterFactory.from_list(tuple(d.items())))
        .map(
            lambda p: p.map(
                lambda i: Unfolder.to_json(i[1])
                .bind(_is_datetime)
                .map(lambda b: (i[0], b))
                .alt(lambda e: Exception(f"In key `{i[0]}` i.e. {e}")),
            )
        )
        .bind(lambda x: all_ok(x.to_list()))
        .map(
            lambda p: PureIterFactory.from_list(p)
            .filter(lambda t: t[1])
            .map(lambda x: x[0])
        )
        .map(lambda x: frozenset(x))
    )
    records = datetime_properties.map(
        lambda p: group.records.map(
            lambda c: _format_datetime_on_record(p, c)
            .alt(raise_exception)
            .unwrap()
        )
    ).unwrap()
    return RecordGroup.new(group.schema, records)


@dataclass(frozen=True)
class _Private:
    pass


@dataclass(frozen=True)
class RawFormatedRecord:
    _private: _Private = field(repr=False, hash=False, compare=False)
    record: FrozenList[Primitive]

    @staticmethod
    def format_group_records(
        group: RecordGroup, str_limit: int
    ) -> PureIter[RawFormatedRecord]:
        return (
            _format_datetime_on_group(group)
            .records.map(_ordered_data)
            .map(lambda r: _truncate_row(r, str_limit))
            .map(lambda r: RawFormatedRecord(_Private(), r))
        )
