{ projectPath, ... }@makes_inputs: {
  imports = [
    ./batch/makes.nix
    ./common/deploy-image/makes.nix
    ./common/etl-utils/makes.nix
    ./dev/makes.nix
    ./infra/makes.nix
    ./pipeline/makes.nix
    ./singer/tap-bugsnag/makes.nix
    ./etl/integrates-usage/makes.nix
    ./etl/gitlab/makes.nix
    ./etl/zoho-crm-leads/makes.nix
    ./sdk/timedoctor/makes.nix
    ./sdk/gitlab/makes.nix
    ./singer/tap-ce/makes.nix
    ./service/success-indicators/makes.nix
  ];
  inputs = let
    getBundle = relative_root:
      let root = projectPath relative_root;
      in import "${root}/entrypoint.nix" makes_inputs;
    getRuntime = relative_root: (getBundle relative_root).env.runtime;
  in {
    inherit getBundle getRuntime;
    nixpkgs-observes = let
      owner = "NixOS";
      repo = "nixpkgs";
      rev = "f74d88a6f19ec8c29bff5cc9e42ee5ec4e0e70f6";
      src = builtins.fetchTarball {
        sha256 = "0mwhisr0gzxj3qjakiimvmzmjnkhayb3y51alddl4d1ar8b4rhb5";
        url = "https://github.com/${owner}/${repo}/archive/${rev}.tar.gz";
      };
    in import src { };
    nixpkgs-observes-2 = let
      owner = "NixOS";
      repo = "nixpkgs";
      rev = "c8bb7b26f2c6ecc39be2c4ddde5f5d152e4abc65";
      src = builtins.fetchTarball {
        sha256 = "0rsfnh1ialqr09a4fg47dy1abah4rh7jxj4b0d26jqdrnjkf1spk";
        url = "https://github.com/${owner}/${repo}/archive/${rev}.tar.gz";
      };
    in import src { };
    observesIndex = import (projectPath "/observes/architecture/index.nix");
    buildPynix = let
      commit = "1d069308dae7e10862d77fb270eaad0d47688015";
      url =
        "https://gitlab.com/dmurciaatfluid/python_nix_builder/-/archive/${commit}/python_nix_builder-${commit}.tar";
      src = builtins.fetchTarball {
        inherit url;
        sha256 = "1kjwb3fd6s8dxnrplc36m6db3xmv0n097clsy4nmypqsvillx2p3";
      };
    in import "${src}/pynix";
    uploadCoverage = import (projectPath "/observes/job/coverage/default.nix");
  };
  secretsForAwsFromGitlab = {
    prodObserves = {
      roleArn = "arn:aws:iam::205810638802:role/prod_observes";
      duration = 3600;
    };
  };
}
