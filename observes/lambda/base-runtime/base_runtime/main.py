from .listener import (
    LambdaListener,
)
from base_runtime.api.core import (
    LambdaError,
)
from base_runtime.init import (
    get_api_client,
    HandlerMap,
    initialize,
)
from collections.abc import (
    Callable,
)
from fa_purity import (
    Cmd,
    Unsafe,
)
import sys
from typing import (
    NoReturn,
    TypeVar,
)

_T = TypeVar("_T")


def _init_error_exit(_: _T) -> NoReturn:
    sys.exit(1)


def _report_init_error(error: Exception) -> Cmd[None | NoReturn]:
    return (
        get_api_client()
        .map(lambda r: r.alt(Unsafe.raise_exception).to_union())
        .bind(
            lambda c: c.init_error(
                LambdaError.new(str(type(error)), str(error), tuple())
            ).map(lambda r: r.alt(_init_error_exit).to_union())
        )
    )


def start_lambda_listener(
    handler_map: Callable[[], HandlerMap]
) -> Cmd[None | NoReturn]:
    """
    How to use?
        Wrap all your function call (even the module import)
        into the argument callable. Then, when a lambda is
        triggered, the selected function will be fetch
        from the provided map.
    """
    return initialize(handler_map).bind(
        lambda r: r.map(lambda c: LambdaListener(c).event_loop())
        .alt(
            lambda e: _report_init_error(e)
            .map(_init_error_exit)
            .map(lambda _: None)
        )
        .to_union()
    )
