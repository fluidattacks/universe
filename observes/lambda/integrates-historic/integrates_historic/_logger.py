import logging
import sys


def setup_log(log: logging.Logger) -> None:
    # impure
    stderr_handler = logging.StreamHandler(sys.stderr)
    log.setLevel(logging.INFO)
    log.addHandler(stderr_handler)
