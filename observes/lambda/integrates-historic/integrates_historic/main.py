from base_runtime.api.core import (
    HandlerId,
)
from base_runtime.init import (
    HandlerMap,
)
from base_runtime.main import (
    start_lambda_listener,
)
from fa_purity import (
    Cmd,
    FrozenDict,
)
from typing import (
    NoReturn,
)


def _handlers() -> HandlerMap:
    # main program import should be called inside the handle
    # for enabling lambda runtime (base_runtime) to catch exceptions
    # at import-time/module-level
    from .core import (  # pylint: disable=C0415
        historics_event_handler,
    )

    return FrozenDict(
        {
            HandlerId("Records"): historics_event_handler,
        },
    )


def entrypoint() -> NoReturn:
    cmd: Cmd[None] = start_lambda_listener(_handlers)
    cmd.compute()
