from ._client import (
    HistoricClient,
    HistoricData,
    HistoricDataId,
)
from base_runtime.api.core import (
    Context,
    Event,
    LambdaError,
)
from connection_manager import (
    ConnectionConf,
    ConnectionManagerFactory,
    Warehouses,
    Roles,
    Databases,
)
from etl_utils.bug import (
    Bug,
)
from fa_purity import (
    Cmd,
    FrozenList,
    PureIterFactory,
    PureIterTransform,
    Result,
    ResultE,
    ResultTransform,
)
from fa_purity.json import (
    JsonObj,
    JsonUnfolder,
    JsonValue,
    Unfolder,
)
import inspect
from integrates_historic.utils import (
    decode_record,
    Record,
    StreamEvent,
)
import logging
from snowflake_client import (
    SnowflakeCursor,
)
from typing import (
    Callable,
)

LOG = logging.getLogger(__name__)


def _to_json_obj_list(raw: JsonValue) -> ResultE[FrozenList[JsonObj]]:
    return Unfolder.to_list_of(raw, Unfolder.to_json)


def _get_keys_from_data(row: HistoricData) -> HistoricDataId:
    return HistoricDataId(pk=row.pk, sk=row.sk)


def _process_historic(
    sql_cursor: SnowflakeCursor,
    record: Record,
    process_fn: Callable[[HistoricClient, HistoricData], Cmd[None]],
) -> Cmd[None]:
    client = HistoricClient(sql_cursor)
    row_data = HistoricData(
        pk=record.pk, sk=record.sk, item=record.new_image.value_or(JsonObj({}))
    )

    return client.init_table() + process_fn(client, row_data)


def _upsert_historic(
    client: HistoricClient, row_data: HistoricData
) -> Cmd[None]:
    if not row_data.item:
        return Cmd.wrap_impure(lambda: LOG.info("No rows to process"))

    key_result = _get_keys_from_data(row_data)
    return (
        client.select(key_result)
        .map(
            lambda r: Bug.assume_success(
                "_upsert_historic",
                inspect.currentframe(),
                tuple([]),
                r,
            )
        )
        .bind(
            lambda row: row.map(lambda _: client.update(row_data)).value_or(
                client.insert(row_data)
            )
        )
    )


def _remove_historic(
    client: HistoricClient, row_data: HistoricData
) -> Cmd[None]:
    key_result = _get_keys_from_data(row_data)
    nothing = Cmd.wrap_value(None)
    return (
        client.select(key_result)
        .map(
            lambda r: Bug.assume_success(
                "_remove_historic",
                inspect.currentframe(),
                tuple([]),
                r,
            )
        )
        .bind(
            lambda row: row.map(lambda _: client.delete(key_result)).value_or(
                nothing
            )
        )
    )


def upload_historic_event(
    sql_cursor: SnowflakeCursor, record: Record
) -> Cmd[None]:
    if record.event_name is StreamEvent.INSERT:
        return _process_historic(sql_cursor, record, _upsert_historic)
    if record.event_name is StreamEvent.MODIFY:
        return Cmd.wrap_impure(
            lambda: LOG.warning("Modify operation is disabled")
        )
    if record.event_name is StreamEvent.REMOVE:
        return Cmd.wrap_impure(
            lambda: LOG.warning("Remove operation is disabled")
        )


def process_records(
    sql_cursor: SnowflakeCursor, event: Event
) -> Cmd[Result[JsonObj, LambdaError]]:
    records_result = (
        JsonUnfolder.require(event.raw, "Records", _to_json_obj_list)
        .alt(
            lambda e: LambdaError.new(
                "decode_error",
                str(e),
                (JsonUnfolder.dumps(event.raw),),
            )
        )
        .bind(
            lambda records: ResultTransform.all_ok(
                PureIterFactory.from_list(records)
                .map(
                    lambda record: decode_record(record).alt(
                        lambda e: LambdaError.new(
                            "record_decode_error",
                            str(e),
                            (JsonUnfolder.dumps(record),),
                        )
                    )
                )
                .to_list()
            )
        )
    )
    return (
        records_result.map(
            lambda records: PureIterFactory.from_list(records).map(
                lambda record: upload_historic_event(sql_cursor, record)
            )
        )
        .to_coproduct()
        .map(
            lambda p: PureIterTransform.consume(p).map(
                lambda _: Result.success(JsonObj({}))
            ),
            lambda e: Cmd.wrap_value(Result.failure(e)),
        )
    )


def historics_event_handler(
    event: Event, _: Context
) -> Cmd[Result[JsonObj, LambdaError]]:
    manager = ConnectionManagerFactory.observes_manager().map(
        lambda r: Bug.assume_success(
            "connection manager", inspect.currentframe(), tuple(), r
        )
    )
    conf = ConnectionConf(
        Warehouses.GENERIC_COMPUTE,
        Roles.ETL_UPLOADER,
        Databases.OBSERVES,
    )
    return manager.bind(
        lambda m: m.execute_with_snowflake(
            lambda c: c.connection.cursor(LOG).bind(
                lambda sql_cursor: process_records(sql_cursor, event)
            ),
            conf,
        ).map(
            lambda r: r.alt(
                lambda e: e.map(
                    lambda x: x,
                    lambda se: LambdaError.new(
                        "setup_error",
                        str(se),
                        tuple(),
                    ),
                )
            )
        )
    )
