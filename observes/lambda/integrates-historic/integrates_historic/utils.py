from base_runtime.api.core import (
    LambdaError,
)
from boto3.dynamodb.types import (
    TypeDeserializer,
)
from dataclasses import (
    dataclass,
)
from enum import (
    Enum,
)
from etl_utils.bug import (
    Bug,
)
from fa_purity import (
    cast_exception,
    Cmd,
    CmdUnwrapper,
    FrozenList,
    Maybe,
    Result,
    ResultE,
)
from fa_purity.json import (
    JsonObj,
    JsonPrimitiveUnfolder,
    JsonUnfolder,
    JsonValue,
    JsonValueFactory,
    Unfolder,
)
import inspect
import logging
import simplejson
import threading
from typing import (
    TypeVar,
)

_T = TypeVar("_T")
_E = TypeVar("_E")

LOG = logging.getLogger(__name__)


class StreamEvent(str, Enum):
    INSERT = "INSERT"
    MODIFY = "MODIFY"
    REMOVE = "REMOVE"


@dataclass(frozen=True)
class Record:
    event_name: StreamEvent
    new_image: Maybe[JsonObj]
    pk: str
    sk: str


def _to_str(raw: JsonValue) -> ResultE[str]:
    return Unfolder.to_primitive(raw).bind(JsonPrimitiveUnfolder.to_str)


def _deserialize_dynamodb_json(
    item: Maybe[JsonObj],
) -> Maybe[JsonObj]:
    def _serialize(object_: object) -> object:
        if isinstance(object_, set):
            return list(object_)  # type: ignore

        return object_

    deserializer = TypeDeserializer()
    return Maybe.from_result(
        item.to_result().map(
            lambda i: Bug.assume_success(
                "deserialize_dynamodb_json",
                inspect.currentframe(),
                tuple([]),
                JsonValueFactory.from_any(
                    simplejson.loads(
                        simplejson.dumps(
                            {  # type: ignore[misc]
                                k: deserializer.deserialize(Unfolder.to_raw(v))  # type: ignore
                                for k, v in i.items()
                            },
                            default=_serialize,
                        )
                    )
                ).bind(Unfolder.to_json),
            )
        )
    )


def get_index(items: FrozenList[_T], index: int) -> Maybe[_T]:
    try:
        return Maybe.some(items[index])
    except KeyError:
        return Maybe.empty()


def decode_record(record: JsonObj) -> ResultE[Record]:
    event_name_result = JsonUnfolder.require(record, "eventName", _to_str).map(
        lambda e: StreamEvent[e]
    )
    pk_result = (
        JsonUnfolder.require(record, "dynamodb", Unfolder.to_json)
        .bind(lambda i: JsonUnfolder.require(i, "Keys", Unfolder.to_json))
        .bind(lambda j: JsonUnfolder.require(j, "pk", Unfolder.to_json))
        .bind(lambda k: JsonUnfolder.require(k, "S", _to_str))
    )
    sk_result = (
        JsonUnfolder.require(record, "dynamodb", Unfolder.to_json)
        .bind(lambda i: JsonUnfolder.require(i, "Keys", Unfolder.to_json))
        .bind(lambda j: JsonUnfolder.require(j, "sk", Unfolder.to_json))
        .bind(lambda k: JsonUnfolder.require(k, "S", _to_str))
    )
    new_image_result = (
        JsonUnfolder.require(record, "dynamodb", Unfolder.to_json)
        .bind(lambda i: JsonUnfolder.optional(i, "NewImage", Unfolder.to_json))
        .map(_deserialize_dynamodb_json)
    )
    return event_name_result.bind(
        lambda event_name: pk_result.bind(
            lambda pk: sk_result.bind(
                lambda sk: new_image_result.bind(
                    lambda new_image: Result.success(
                        Record(
                            event_name,
                            new_image,
                            pk,
                            sk,
                        ),
                        Exception,
                    )
                )
            )
        )
    )


def require_index(items: FrozenList[_T], index: int) -> ResultE[_T]:
    try:
        return Result.success(items[index])
    except IndexError as err:
        return Result.failure(cast_exception(err))


def swap_cmd_result(
    r: Result[Cmd[_T], _E],
) -> Cmd[Result[_T, _E]]:
    return r.to_coproduct().map(
        lambda c: c.map(lambda j: Result.success(j)),
        lambda e: Cmd.wrap_impure(lambda: Result.failure(e)),
    )


def swap_maybe_result(
    r: Maybe[Result[_T, _E]],
) -> Result[Maybe[_T], _E]:
    return (
        r.to_result()
        .to_coproduct()
        .map(
            lambda c: c.map(lambda j: Maybe.some(j)),
            lambda _: Result.success(Maybe.empty()),
        )
    )


def adapt(
    items: _T,
) -> Maybe[_T]:
    if not items:
        return Maybe.empty()
    return Maybe.some(items)


def cast_lambda_exception(exception: Exception) -> LambdaError:
    return LambdaError.new(
        str(type(exception)),
        str(exception),
        tuple(),
    )


def timeout(command: Cmd[_T], timeout_duration: int) -> Cmd[_T | None]:
    def _action(unwrapper: CmdUnwrapper) -> _T | None:
        result: list[_T | None] = [None]

        def run_command() -> None:
            result[0] = unwrapper.act(command)

        thread = threading.Thread(target=run_command)
        thread.start()

        thread.join(timeout_duration)

        if thread.is_alive():
            return None

        return result[0]

    return Cmd.new_cmd(_action)
