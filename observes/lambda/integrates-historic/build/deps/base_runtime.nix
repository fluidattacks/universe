{ makes_inputs, nixpkgs, python_version, }:
let
  src = makes_inputs.projectPath
    makes_inputs.inputs.observesIndex.lambda.base_runtime.root;
  out = import "${src}/build" {
    inherit makes_inputs nixpkgs python_version;
    src = nixpkgs.nix-filter {
      root = src;
      include = [ "base_runtime" "tests" "pyproject.toml" "mypy.ini" ];
    };
  };
in out.pkg
