{ makes_inputs, nixpkgs, python_version, src, }:
let
  deps = import ./deps { inherit makes_inputs nixpkgs python_version; };
  pkgDeps = {
    runtime_deps = with deps.python_pkgs; [
      etl-utils
      base-runtime
      connection-manager
      fa-purity
    ];
    build_deps = with deps.python_pkgs; [ flit-core ];
    test_deps = with deps.python_pkgs; [
      arch-lint
      mypy
      pytest
      pytest-cov
      pylint
    ];
  };
  base_bundle = makes_inputs.makePythonPyprojectPackage {
    inherit (deps.lib) buildEnv buildPythonPackage;
    inherit pkgDeps src;
  };
  bundle =
    import (makes_inputs.projectPath "/observes/common/bundle_patch.nix") {
      inherit (deps.lib) buildEnv;
      bundle = base_bundle;
    };
  secretsFile = nixpkgs.writeTextFile {
    name = "prod.yaml";
    text = builtins.readFile
      (makes_inputs.projectPath "/observes/secrets/prod.yaml");
    destination = "/secrets/prod.yaml";
  };
  caCertsFile = nixpkgs.writeTextFile {
    name = "ca-certificates.crt";
    text = builtins.readFile "${nixpkgs.cacert}/etc/ssl/certs/ca-bundle.crt";
    destination = "/etc/ssl/certs/ca-certificates.crt";
  };
  image = nixpkgs.dockerTools.buildImage {
    name = "integrates_historic_listener";
    copyToRoot = [ bundle.env.runtime secretsFile caCertsFile ];
    config = {
      Cmd = [ "integrates-historic-listener" ];
      Env = [
        "PATH=/bin"
        "SECRETS_FILE=/secrets/prod.yaml"

        # Certificate authorities
        "SSL_CERT_FILE=/etc/ssl/certs/ca-certificates.crt"
        "SYSTEM_CERTIFICATE_PATH=/etc/ssl/certs/ca-certificates.crt"
      ];
    };
    architecture = "aarch64-linux";
  };
in bundle // { inherit image; }
