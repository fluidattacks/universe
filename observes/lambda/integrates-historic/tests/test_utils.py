from base_runtime.api.core import (
    LambdaError,
)
from collections import (
    defaultdict,
)
from fa_purity import (
    Cmd,
    CmdUnwrapper,
    FrozenTools,
    Maybe,
    Result,
    Unsafe,
)
from fa_purity.json import (
    JsonPrimitive,
    JsonUnfolder,
    JsonValueFactory,
    Unfolder,
)
from integrates_historic.utils import (
    adapt,
    decode_record,
    StreamEvent,
    swap_maybe_result,
    timeout,
)
import pytest
import time

DEFAULT_JSON = (
    Unfolder.to_json(JsonValueFactory.from_dict(defaultdict(str)))
    .alt(Unsafe.raise_exception)
    .to_union()
)


def test_fomat_record() -> None:
    json_obj = (
        JsonValueFactory.from_any(
            {
                "eventID": "35216650-65f3-4c65-8d13-5f9491c4913d",
                "eventName": "MODIFY",
                "eventVersion": "1.1",
                "eventSource": "aws:dynamodb",
                "awsRegion": "ddblocal",
                "dynamodb": {
                    "ApproximateCreationDateTime": 1691164260000,
                    "Keys": {
                        "sk": {
                            "S": "STATE#state#DATE#2021-11-19T13:37:10+00:00"
                        },
                        "pk": {
                            "S": "ROOT#3a69ee71-1183-4cbe-99d9-27b2617df7d5#"
                            "GROUP#asgard"
                        },
                    },
                    "NewImage": {
                        "Message": {"S": "This item has changed"},
                        "Id": {"N": "101"},
                        "groups": {"SS": ["oneshottest", "unittesting"]},
                        "unreliable_indicators": {
                            "M": {
                                "mean_remediate": {"N": "174"},
                                "groups": {
                                    "SS": ["oneshottest", "unittesting"]
                                },
                            }
                        },
                        "payment_methods": {
                            "L": [
                                {
                                    "M": {
                                        "id": {"N": "123.2"},
                                        "documents": {"M": {}},
                                    }
                                },
                            ]
                        },
                    },
                    "SizeBytes": 1087,
                    "StreamViewType": "NEW_IMAGE",
                },
            },
        )
        .bind(Unfolder.to_json)
        .alt(Unsafe.raise_exception)
        .to_union()
    )
    record = decode_record(json_obj).alt(Unsafe.raise_exception).to_union()
    assert record.event_name is StreamEvent.MODIFY
    assert (
        record.pk == "ROOT#3a69ee71-1183-4cbe-99d9-27b2617df7d5#GROUP#asgard"
    )
    assert record.sk == "STATE#state#DATE#2021-11-19T13:37:10+00:00"
    assert Unfolder.to_primitive(
        record.new_image.value_or(DEFAULT_JSON)["Message"]
    ).alt(Unsafe.raise_exception).to_union() == JsonPrimitive.from_str(
        "This item has changed"
    )
    assert Unfolder.to_primitive(
        record.new_image.value_or(DEFAULT_JSON)["Id"]
    ).alt(Unsafe.raise_exception).to_union() == JsonPrimitive.from_int(101)
    assert set(
        Unfolder.to_list_of(
            record.new_image.value_or(DEFAULT_JSON)["groups"],
            Unfolder.to_primitive,
        )
        .alt(Unsafe.raise_exception)
        .to_union()
    ) == {
        JsonPrimitive.from_str("oneshottest"),
        JsonPrimitive.from_str("unittesting"),
    }
    indicators = (
        Unfolder.to_json(
            record.new_image.value_or(DEFAULT_JSON)["unreliable_indicators"]
        )
        .alt(Unsafe.raise_exception)
        .to_union()
    )
    assert set(
        JsonUnfolder.require(
            indicators,
            "groups",
            lambda v: Unfolder.to_list_of(v, Unfolder.to_primitive),
        )
        .alt(Unsafe.raise_exception)
        .to_union()
    ) == {
        JsonPrimitive.from_str("oneshottest"),
        JsonPrimitive.from_str("unittesting"),
    }
    assert (
        JsonUnfolder.require(
            indicators,
            "mean_remediate",
            Unfolder.to_primitive,
        )
        .alt(Unsafe.raise_exception)
        .to_union()
    ) == JsonPrimitive.from_int(174)
    assert (
        JsonUnfolder.require(
            indicators,
            "mean_remediate",
            Unfolder.to_primitive,
        )
        .alt(Unsafe.raise_exception)
        .to_union()
    ) == JsonPrimitive.from_int(174)
    list_obj = (
        Unfolder.to_list_of(
            record.new_image.value_or(DEFAULT_JSON)["payment_methods"],
            Unfolder.to_json,
        )
        .alt(Unsafe.raise_exception)
        .to_union()
    )
    assert (
        JsonUnfolder.require(
            list_obj[0],
            "id",
            Unfolder.to_primitive,
        )
        .alt(Unsafe.raise_exception)
        .to_union()
    ) == JsonPrimitive.from_float(123.2)
    assert (
        JsonUnfolder.require(
            list_obj[0],
            "documents",
            lambda v: Unfolder.to_dict_of(v, Unfolder.to_primitive),
        )
        .alt(Unsafe.raise_exception)
        .to_union()
    ) == FrozenTools.freeze({})


def test_swap_maybe_result() -> None:
    input_value: Maybe[Result[int, LambdaError]] = Maybe.some(
        Result.success(42)
    )
    expected_output: Result[Maybe[int], LambdaError] = Result.success(
        Maybe.some(42)
    )

    result = swap_maybe_result(input_value)

    assert result == expected_output


def test_adapt() -> None:
    items = [1, 2, 3]
    result = adapt(items)
    assert result == Maybe.some(items)


def test_timeout() -> None:
    def quick_action(_unwrapper: CmdUnwrapper) -> str:
        return "Quick result"

    def _quick_assert(quick_result: str | None) -> None:
        assert quick_result == "Quick result"

    quick_cmd = Cmd.new_cmd(quick_action)
    with pytest.raises(SystemExit):
        timeout(quick_cmd, 5).map(_quick_assert).compute()

    def slow_action(_unwrapper: CmdUnwrapper) -> None:
        time.sleep(10)

    def _slow_assert(slow_result: None) -> None:
        assert slow_result is None

    slow_cmd = Cmd.new_cmd(slow_action)
    with pytest.raises(SystemExit):
        timeout(slow_cmd, 1).map(_slow_assert).compute()

    def just_in_time_action(_unwrapper: CmdUnwrapper) -> str:
        time.sleep(1)
        return "Just in time result"

    def _jit_assert(jit_result: str | None) -> None:
        assert jit_result == "Just in time result"

    just_in_time_cmd = Cmd.new_cmd(just_in_time_action)
    with pytest.raises(SystemExit):
        timeout(just_in_time_cmd, 2).map(_jit_assert).compute()
