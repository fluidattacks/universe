from fa_purity import (
    Cmd,
    Maybe,
    PureIterFactory,
    Result,
    ResultE,
    StreamFactory,
    Unsafe,
)
from fa_purity.json import (
    JsonValueFactory,
    Unfolder,
)
from snowflake_client import (
    SnowflakeCursor,
)


def mock_cursor() -> SnowflakeCursor:
    nothing: Cmd[ResultE[None]] = Cmd.wrap_value(Result.success(None))
    return SnowflakeCursor(
        lambda _, __: nothing,
        lambda _, __,: nothing,
        lambda _, __, ___: nothing,
        lambda _, __, ___: nothing,
        Cmd.wrap_value(Result.success(Maybe.empty())),
        Cmd.wrap_value(Result.success(tuple())),
        lambda _: Cmd.wrap_value(Result.success(tuple())),
        lambda _: StreamFactory.from_commands(PureIterFactory.from_list([])),
    )


MOCK_RECORDS = (
    JsonValueFactory.from_any(
        {
            "Records": [
                {
                    "eventID": "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
                    "eventName": "MODIFY",
                    "eventVersion": "1.1",
                    "eventSource": "aws:dynamodb",
                    "awsRegion": "ddblocal",
                    "dynamodb": {
                        "ApproximateCreationDateTime": 1691164260000,
                        "Keys": {
                            "pk": {"S": "pk-test-1"},
                            "sk": {"S": "sk-test-1"},
                        },
                        "NewImage": {
                            "pk": {"S": "pk-test-1"},
                            "sk": {"S": "sk-test-1"},
                            "Message": {"S": "New message"},
                            "Id": {"N": "101"},
                            "unreliable_indicators": {
                                "M": {"mean_remediate": {"N": "174"}}
                            },
                            "payment_methods": {
                                "L": [
                                    {
                                        "M": {
                                            "id": {"N": "123.2"},
                                            "documents": {"M": {}},
                                        }
                                    }
                                ]
                            },
                        },
                        "SizeBytes": 1087,
                        "StreamViewType": "NEW_IMAGE",
                    },
                }
            ]
        },
    )
    .bind(Unfolder.to_json)
    .alt(Unsafe.raise_exception)
    .to_union()
)
