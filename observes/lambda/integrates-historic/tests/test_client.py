from .mock import (
    mock_cursor,
)
from decimal import (
    Decimal,
)
from fa_purity import (
    Unsafe,
)
from fa_purity.json import (
    UnfoldedFactory,
)
from integrates_historic._client import (
    HistoricClient,
    HistoricData,
    HistoricDataId,
)
from redshift_client.core.id_objs import (
    Identifier,
    SchemaId,
    TableId,
)
from tests import (
    _utils,
)


def test_delete_query() -> None:
    client = HistoricClient(
        mock_cursor(),
        SchemaId(Identifier.new("schema_test")),
        TableId(Identifier.new("table_test")),
    )
    query, values = client._delete_query(HistoricDataId("pk-test", "sk-test"))
    _utils.check_query_replacement(query, values)


def test_insert() -> None:
    client = HistoricClient(
        mock_cursor(),
        SchemaId(Identifier.new("schema_test")),
        TableId(Identifier.new("table_test")),
    )
    item = {
        "pk": "pk-test",
        "sk": "sk-test",
        "message": "test",
        "id": 10,
        "list": [{"a": "a", "b": "b"}],
        "decimal": Decimal("1.2"),
        "map": {
            "a": "a",
            "b": "b",
        },
    }
    row_data = HistoricData(
        "pk-test",
        "sk-test",
        UnfoldedFactory.from_raw_dict(item)
        .alt(Unsafe.raise_exception)
        .to_union(),
    )
    query, values = client._insert_query(row_data)
    _utils.check_query_replacement(query, values)


def test_select() -> None:
    client = HistoricClient(
        mock_cursor(),
        SchemaId(Identifier.new("schema_test")),
        TableId(Identifier.new("table_test")),
    )
    row_keys = HistoricDataId("pk-test-1", "sk-test-1")
    query, values = client._select_query(row_keys)
    _utils.check_query_replacement(query, values)


def test_update() -> None:
    client = HistoricClient(
        mock_cursor(),
        SchemaId(Identifier.new("schema_test")),
        TableId(Identifier.new("table_test")),
    )
    item = {
        "pk": "pk-test",
        "sk": "sk-test",
        "message": "test",
        "id": 10,
        "list": [{"a": "a", "b": "b"}],
        "decimal": Decimal("1.2"),
        "map": {
            "a": "a",
            "b": "b",
        },
    }
    row_data = HistoricData(
        "pk-test",
        "sk-test",
        UnfoldedFactory.from_raw_dict(item)
        .alt(Unsafe.raise_exception)
        .to_union(),
    )
    query, values = client._update_query(row_data)
    _utils.check_query_replacement(query, values)
