{ inputs, projectPath, ... }@makes_inputs:
let
  root = projectPath inputs.observesIndex.lambda.integrates_historic.root;
  bundle = import "${root}/entrypoint.nix" makes_inputs;
in bundle.image
