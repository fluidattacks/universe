from datetime import UTC, datetime
from pathlib import Path

from etl_utils.natural import NaturalOperations
from fa_purity import Maybe, Unsafe
from fa_purity.date_time import DatetimeUTC
from fa_purity.json import JsonValueFactory, Unfolder

from gitlab_sdk.ids import MrGlobalId, ProjectId, UserId
from gitlab_sdk.merge_requests._decode import decode_mr_and_id
from gitlab_sdk.merge_requests.core import (
    MergeRequest,
    MergeRequestDates,
    MergeRequestFullState,
    MergeRequestOrigins,
    MergeRequestPeople,
    MergeRequestProperties,
    MergeRequestSha,
    MergeRequestState,
    TaskCompletion,
)


def _assert_utc(date_time: datetime) -> DatetimeUTC:
    return DatetimeUTC.assert_utc(date_time).alt(Unsafe.raise_exception).to_union()


def test_decode() -> None:
    author = UserId(NaturalOperations.absolute(987654321))
    merger = UserId(NaturalOperations.absolute(1234567))
    expected = (
        MrGlobalId(NaturalOperations.absolute(99887766)),
        MergeRequest(
            MergeRequestSha(
                "a" * 40,
                Maybe.some("b" * 40),
                Maybe.empty(),
            ),
            MergeRequestDates(
                _assert_utc(datetime(2025, 2, 18, 1, 22, 33, tzinfo=UTC)),
                Maybe.some(_assert_utc(datetime(2025, 2, 19, 1, 22, 32, tzinfo=UTC))),
                Maybe.some(_assert_utc(datetime(2025, 2, 19, 1, 22, 33, tzinfo=UTC))),
                Maybe.some(_assert_utc(datetime(2025, 2, 20, 1, 22, 33, tzinfo=UTC))),
                Maybe.empty(),
            ),
            MergeRequestPeople(
                author,
                Maybe.some(merger),
                Maybe.empty(),
                (author,),
                (merger,),
            ),
            MergeRequestFullState(
                MergeRequestState.MERGED,
                "not_open",
                False,
                0,
                Maybe.empty(),
            ),
            MergeRequestProperties(
                "the_commit_title",
                Maybe.some("- Do something\\n- Update something links\\n- Other item"),
                True,
                False,
                True,
                "none",
                False,
                (),
                Maybe.empty(),
            ),
            MergeRequestOrigins(
                ProjectId(NaturalOperations.absolute(123456789)),
                "source_branch",
                ProjectId(NaturalOperations.absolute(777)),
                "master",
            ),
            Maybe.empty(),
            TaskCompletion(1, 0),
        ),
    )
    raw_data_path = Path(__file__).parent / "data.json"
    raw_data = (
        JsonValueFactory.load(raw_data_path.open(encoding="utf-8"))
        .bind(Unfolder.to_json)
        .alt(Unsafe.raise_exception)
        .to_union()
    )
    decoded_mr = decode_mr_and_id(raw_data).alt(Unsafe.raise_exception).to_union()
    assert decoded_mr == expected
