import os

from etl_utils.natural import Natural
from fa_purity import Cmd, Maybe, Result, ResultE

from gitlab_sdk._http_client._core import Credentials
from gitlab_sdk.ids import ProjectId


def get_env_var(var: str) -> Cmd[Maybe[str]]:
    return Cmd.wrap_impure(lambda: Maybe.from_optional(os.environ.get(var)))


def require_env_var(var: str) -> Cmd[ResultE[str]]:
    return get_env_var(var).map(lambda m: m.to_result().alt(lambda _: KeyError(var)))


def get_creds_from_env() -> Cmd[ResultE[Credentials]]:
    return require_env_var("GITLAB_TOKEN").map(lambda r: r.map(Credentials))


def _str_to_int(value: str) -> ResultE[int]:
    try:
        return Result.success(int(value))
    except ValueError as error:
        return Result.failure(error)


def get_project_from_env() -> Cmd[ResultE[ProjectId]]:
    return require_env_var("GITLAB_PROJECT").map(
        lambda r: r.bind(_str_to_int).bind(Natural.from_int).map(ProjectId),
    )
