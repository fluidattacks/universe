from __future__ import annotations

from dataclasses import dataclass
from enum import Enum

from fa_purity import FrozenList, Maybe, Result, ResultE, cast_exception
from fa_purity.date_time import DatetimeUTC

from gitlab_sdk.ids import MilestoneInternalId, ProjectId, UserId


class MergeRequestState(Enum):
    # locked: transitional state while a merge is happening
    OPENED = "opened"
    CLOSED = "closed"
    LOCKED = "locked"
    MERGED = "merged"

    @staticmethod
    def from_raw(raw: str) -> ResultE[MergeRequestState]:
        try:
            return Result.success(MergeRequestState(raw))
        except ValueError as err:
            return Result.failure(cast_exception(err))


@dataclass(frozen=True)
class MergeRequestSha:
    sha: str
    merge_commit_sha: Maybe[str]
    squash_commit_sha: Maybe[str]


@dataclass(frozen=True)
class MergeRequestDates:
    created_at: DatetimeUTC
    prepared_at: Maybe[DatetimeUTC]
    updated_at: Maybe[DatetimeUTC]
    merged_at: Maybe[DatetimeUTC]
    closed_at: Maybe[DatetimeUTC]


@dataclass(frozen=True)
class MergeRequestPeople:
    author: UserId
    merge_user: Maybe[UserId]
    closed_by: Maybe[UserId]
    assignees: FrozenList[UserId]
    reviewers: FrozenList[UserId]


@dataclass(frozen=True)
class MergeRequestFullState:
    state: MergeRequestState
    detailed_merge_status: str
    has_conflicts: bool
    user_notes_count: int
    merge_error: Maybe[str]


@dataclass(frozen=True)
class MergeRequestProperties:
    title: str
    description: Maybe[str]
    draft: bool
    squash: bool
    imported: bool
    imported_from: str
    first_contribution: bool
    labels: FrozenList[str]
    merge_after: Maybe[DatetimeUTC]


@dataclass(frozen=True)
class MergeRequestOrigins:
    source_project_id: ProjectId
    source_branch: str
    target_project_id: ProjectId
    target_branch: str


@dataclass(frozen=True)
class TaskCompletion:
    count: int
    completed_count: int


@dataclass(frozen=True)
class MergeRequest:
    shas: MergeRequestSha
    dates: MergeRequestDates
    people: MergeRequestPeople
    full_state: MergeRequestFullState
    properties: MergeRequestProperties
    origins: MergeRequestOrigins
    milestone: Maybe[MilestoneInternalId]
    task_completion: TaskCompletion
