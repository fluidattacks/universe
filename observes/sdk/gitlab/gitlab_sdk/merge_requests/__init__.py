"""Main documentation at https://docs.gitlab.com/ee/api/merge_requests.html ."""

from dataclasses import dataclass

from gitlab_sdk._http_client import ClientFactory, Credentials, HttpJsonClient
from gitlab_sdk.merge_requests._client import MrsClient

from . import _client


def _from_client(client: HttpJsonClient) -> MrsClient:
    return MrsClient(
        lambda p, i: _client.get_mr(client, p, i),
        lambda p: _client.most_recent_mr(client, p),
    )


@dataclass(frozen=True)
class MrClientFactory:
    @staticmethod
    def new(creds: Credentials) -> MrsClient:
        return _from_client(ClientFactory.new(creds))


__all__ = [
    "MrsClient",
]
