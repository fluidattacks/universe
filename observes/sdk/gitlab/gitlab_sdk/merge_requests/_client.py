from __future__ import (
    annotations,
)

import inspect
from collections.abc import Callable
from dataclasses import (
    dataclass,
)

from etl_utils.bug import Bug
from etl_utils.decode import int_to_str
from fa_purity import (
    Cmd,
    Coproduct,
    FrozenDict,
    FrozenList,
    Result,
    ResultE,
    cast_exception,
)
from fa_purity.json import JsonObj, Primitive, UnfoldedFactory

from gitlab_sdk._http_client import HttpJsonClient, RelativeEndpoint
from gitlab_sdk.ids import MrGlobalId, MrInternalId, ProjectId

from ._decode import decode_iid, decode_mr_and_id, decode_single
from .core import MergeRequest


@dataclass(frozen=True)
class MrsClient:
    get_mr: Callable[
        [ProjectId, MrInternalId],
        Cmd[ResultE[tuple[MrGlobalId, MergeRequest]]],
    ]
    most_recent_mr: Callable[
        [ProjectId],
        Cmd[ResultE[tuple[MrGlobalId, MrInternalId, MergeRequest]]],
    ]


def _assert_single(item: Coproduct[JsonObj, FrozenList[JsonObj]]) -> ResultE[JsonObj]:
    return item.map(
        Result.success,
        lambda _: Result.failure(ValueError("Expected a json not a list")),
    )


def _assert_multiple(item: Coproduct[JsonObj, FrozenList[JsonObj]]) -> ResultE[FrozenList[JsonObj]]:
    return item.map(
        lambda _: Result.failure(ValueError("Expected a json list not a single json")),
        Result.success,
    )


def get_mr(
    client: HttpJsonClient,
    project: ProjectId,
    mr_id: MrInternalId,
) -> Cmd[ResultE[tuple[MrGlobalId, MergeRequest]]]:
    endpoint = RelativeEndpoint.new(
        "projects",
        int_to_str(project.project_id.value),
        "merge_requests",
        int_to_str(mr_id.internal.value),
    )
    return client.get(
        endpoint,
        FrozenDict({}),
    ).map(
        lambda r: r.alt(
            lambda e: cast_exception(
                Bug.new(
                    "_get_mr",
                    inspect.currentframe(),
                    e,
                    (),
                ),
            ),
        )
        .bind(_assert_single)
        .bind(decode_mr_and_id),
    )


def most_recent_mr(
    client: HttpJsonClient,
    project: ProjectId,
) -> Cmd[ResultE[tuple[MrGlobalId, MrInternalId, MergeRequest]]]:
    endpoint = RelativeEndpoint.new(
        "projects",
        int_to_str(project.project_id.value),
        "merge_requests",
    )
    params: dict[str, Primitive] = {
        "order_by": "created_at",
        "sort": "desc",
        "per_page": 1,
    }
    return client.get(
        endpoint,
        UnfoldedFactory.from_dict(params),
    ).map(
        lambda r: r.alt(
            lambda e: cast_exception(
                Bug.new(
                    "most_recent_mr",
                    inspect.currentframe(),
                    e,
                    (),
                ),
            ),
        )
        .bind(_assert_multiple)
        .bind(decode_single)
        .bind(
            lambda r: decode_iid(r).bind(
                lambda iid: decode_mr_and_id(r).map(lambda t: (t[0], iid, t[1])),
            ),
        ),
    )
