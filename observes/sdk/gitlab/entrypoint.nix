{ inputs, ... }@makes_inputs:
let
  python_version = "python311";
  nixpkgs = inputs.nixpkgs-observes-2 // { inherit (inputs) nix-filter; };
  pynix = inputs.buildPynix {
    inherit nixpkgs;
    pythonVersion = python_version;
  };
  out = import ./build {
    inherit makes_inputs nixpkgs pynix python_version;
    src = import ./build/filter.nix nixpkgs.nix-filter ./.;
  };
in out
