{ nixpkgs, pynix, python_pkgs }:
let
  commit = "c2306821288b424a2c25c795904b2bdfc3be2b6d"; # v4.0.2
  sha256 = "083sjd5zslfxyg8zb9f0rzcg15wsj9k1wb9b2pgs0y9f41l4kfiy";
  raw = let
    raw_src = builtins.fetchTarball {
      inherit sha256;
      url =
        "https://gitlab.com/dmurciaatfluid/arch_lint/-/archive/${commit}/arch_lint-${commit}.tar";
    };
  in {
    build = import "${raw_src}/build";
    src = import "${raw_src}/build/filter.nix" nixpkgs.nix-filter raw_src;
  };
  bundle = raw.build {
    inherit (raw) src;
    inherit nixpkgs pynix;
    scripts = { run-lint = [ ]; };
  };
  extended_python_pkgs = python_pkgs // {
    inherit (bundle.deps.python_pkgs) grimp;
  };
in bundle.buildBundle { pkgDeps = bundle.requirements extended_python_pkgs; }
