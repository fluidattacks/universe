{ fetchNixpkgs, inputs, makeScript, makeTemplate, outputs, projectPath, ...
}@makes_inputs:
let
  std_jobs = import (projectPath "/observes/common/std_jobs.nix");
  pkg_index = inputs.observesIndex.sdk.timedoctor;
  root = makes_inputs.projectPath pkg_index.root;
  bundle = import "${root}/entrypoint.nix" makes_inputs;
in { jobs = std_jobs { inherit pkg_index makes_inputs; }; }
