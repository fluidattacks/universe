path_filter: src:
path_filter {
  root = src;
  include =
    [ "timedoctor_sdk" "tests" "pyproject.toml" "mypy.ini" "ruff.toml" ];
}
