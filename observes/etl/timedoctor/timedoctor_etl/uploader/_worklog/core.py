import inspect

from etl_utils.bug import (
    Bug,
)
from fa_purity import (
    Cmd,
    FrozenDict,
)
from redshift_client.core.column import (
    Column,
)
from redshift_client.core.data_type.core import (
    DataType,
    PrecisionType,
    PrecisionTypes,
    StaticTypes,
)
from redshift_client.core.id_objs import (
    ColumnId,
    DbTableId,
    Identifier,
)
from redshift_client.core.table import (
    Table,
)
from redshift_client.sql_client import (
    DbPrimitiveFactory,
)
from snowflake_client import (
    TableClient,
)

TASK_ID = ColumnId(Identifier.new("task_id"))
TASK_NAME = ColumnId(Identifier.new("task_name"))
PROJECT_ID = ColumnId(Identifier.new("project_id"))
PROJECT_NAME = ColumnId(Identifier.new("project_name"))
DEVICE_ID = ColumnId(Identifier.new("device_id"))
USER_ID = ColumnId(Identifier.new("user_id"))
USER_NAME = ColumnId(Identifier.new("user_name"))
START_TIME = ColumnId(Identifier.new("start_time"))
DURATION = ColumnId(Identifier.new("duration"))
WORK_MODE = ColumnId(Identifier.new("work_mode"))
END_TIME = ColumnId(Identifier.new("end_time"))
EDITED = ColumnId(Identifier.new("edited"))


def _var_char(limit: int) -> DataType:
    return DataType(PrecisionType(PrecisionTypes.VARCHAR, limit))


def table_definition() -> Table:
    none = DbPrimitiveFactory.from_raw(None)
    columns: FrozenDict[ColumnId, Column] = FrozenDict(
        {
            TASK_ID: Column(_var_char(256), False, none),
            TASK_NAME: Column(_var_char(256), False, none),
            PROJECT_ID: Column(_var_char(256), False, none),
            PROJECT_NAME: Column(_var_char(256), False, none),
            DEVICE_ID: Column(_var_char(256), False, none),
            USER_ID: Column(_var_char(256), False, none),
            USER_NAME: Column(_var_char(256), False, none),
            START_TIME: Column(DataType(StaticTypes.TIMESTAMPTZ), False, none),
            DURATION: Column(DataType(StaticTypes.REAL), False, none),
            WORK_MODE: Column(_var_char(256), False, none),
            END_TIME: Column(DataType(StaticTypes.TIMESTAMPTZ), False, none),
            EDITED: Column(DataType(StaticTypes.BOOLEAN), False, none),
        },
    )
    _order = (
        TASK_ID,
        TASK_NAME,
        PROJECT_ID,
        PROJECT_NAME,
        DEVICE_ID,
        USER_ID,
        USER_NAME,
        START_TIME,
        DURATION,
        WORK_MODE,
        END_TIME,
        EDITED,
    )
    table = Table.new(
        _order,
        columns,
        frozenset([TASK_ID, PROJECT_ID, DEVICE_ID, USER_ID, START_TIME]),
    )
    return Bug.assume_success("worklog_table_def", inspect.currentframe(), (), table)


def init_table(client: TableClient, table_id: DbTableId) -> Cmd[None]:
    table = table_definition()
    result = client.new_if_not_exist(
        table_id,
        table,
    )
    return result.map(
        lambda r: Bug.assume_success(
            "init_worklog_table_execution",
            inspect.currentframe(),
            (),
            r,
        ),
    )
