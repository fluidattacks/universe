import click
from etl_utils.typing import (
    NoReturn,
)
from fa_purity import (
    Cmd,
)

from timedoctor_etl.etls import (
    main_etl,
)


@click.command()
def main() -> NoReturn:
    cmd: Cmd[None] = main_etl()
    cmd.compute()
