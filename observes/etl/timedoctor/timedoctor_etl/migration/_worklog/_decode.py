from etl_utils import (
    decode,
    smash,
)
from etl_utils.typing import (
    Dict,
    Tuple,
)
from fa_purity import (
    Result,
    ResultE,
)
from fa_purity.json import (
    JsonObj,
    JsonPrimitiveUnfolder,
    JsonUnfolder,
    JsonValue,
    Unfolder,
)
from timedoctor_sdk.core import (
    DeviceId,
    ProjectId,
    ProjectName,
    TaskId,
    TaskName,
    UserId,
    UserName,
    Worklog,
    WorklogId,
    WorklogMode,
)


def _to_str(value: JsonValue) -> ResultE[str]:
    return Unfolder.to_primitive(value).bind(JsonPrimitiveUnfolder.to_str)


def decode_worklog_id(data: JsonObj) -> ResultE[WorklogId]:
    group_1 = smash.smash_result_4(
        JsonUnfolder.require(
            data,
            "task_id",
            lambda v: _to_str(v).map(TaskId),
        ),
        JsonUnfolder.optional(
            data,
            "task_name",
            lambda v: _to_str(v).map(TaskName),
        ),
        JsonUnfolder.require(
            data,
            "project_id",
            lambda v: _to_str(v).map(ProjectId),
        ),
        JsonUnfolder.optional(
            data,
            "project_name",
            lambda v: _to_str(v).map(ProjectName),
        ),
    )
    _user = JsonUnfolder.require(
        data,
        "user_id",
        lambda v: _to_str(v).map(UserId),
    )
    return group_1.bind(
        lambda g1: _user.map(
            lambda u: WorklogId(
                g1[0],
                g1[1].value_or(TaskName("")),
                g1[2],
                g1[3].value_or(ProjectName("")),
                DeviceId("unknown"),
                u,
            ),
        ),
    )


def _legacy_adapter(work_mode: str) -> WorklogMode:
    work_mode_map: Dict[str, WorklogMode] = {
        "offline or afk": WorklogMode.OFFLINE,
        "online": WorklogMode.COMPUTER,
        "mobile app": WorklogMode.MOBILE,
        "manually added": WorklogMode.MANUAL,
        "on break": WorklogMode.BREAK,
    }
    return work_mode_map.get(work_mode, WorklogMode.PAID_LEAVE)


def decode_work_log_mode(raw: str) -> ResultE[WorklogMode]:
    try:
        return Result.success(_legacy_adapter(raw.lower()))
    except ValueError as e:
        return Result.failure(e)


def decode_worklog(data: JsonObj) -> ResultE[Tuple[UserName, Worklog]]:
    group_1 = smash.smash_result_3(
        JsonUnfolder.require(
            data,
            "work_mode",
            lambda v: _to_str(v).bind(decode_work_log_mode),
        ),
        JsonUnfolder.require(
            data,
            "start_time",
            lambda v: _to_str(v).bind(decode.to_datetime_utc),
        ),
        JsonUnfolder.require(
            data,
            "length",
            lambda v: Unfolder.to_primitive(v).bind(
                lambda p: JsonPrimitiveUnfolder.to_float(p).lash(
                    lambda _: JsonPrimitiveUnfolder.to_int(p).map(float),
                ),
            ),
        ),
    )
    _id = decode_worklog_id(data)
    _user_name = JsonUnfolder.require(
        data,
        "user_name",
        lambda v: _to_str(v).map(UserName),
    )
    return group_1.bind(
        lambda g1: _user_name.bind(
            lambda u: _id.map(
                lambda i: (
                    u,
                    Worklog(
                        i,
                        g1[0],
                        g1[1],
                        g1[2],
                    ),
                ),
            ),
        ),
    )
