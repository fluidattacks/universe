from dataclasses import (
    dataclass,
)

from fa_purity import (
    Maybe,
)
from fa_purity.date_time import (
    DatetimeUTC,
)


@dataclass(frozen=True)
class LegacyComputerActivity:
    uuid: str
    date: DatetimeUTC
    user_id: str
    user_name: str
    task_id: str
    project_id: str
    process: str
    window: str
    key_strokes: Maybe[int]
    mouse_movements: Maybe[int]
    deleted_by: str
    deleted_seconds: int
