import inspect
from dataclasses import (
    dataclass,
)
from datetime import (
    datetime,
)

from dateutil.relativedelta import (
    relativedelta,
)
from etl_utils import (
    smash,
)
from etl_utils.bug import (
    Bug,
)
from etl_utils.date_range import (
    DateRange,
)
from fa_purity import (
    Cmd,
    Result,
    ResultE,
)
from fa_purity.date_time import (
    DatetimeUTC,
)
from fa_purity.json import (
    JsonObj,
    JsonPrimitiveUnfolder,
    JsonUnfolder,
    UnfoldedFactory,
    Unfolder,
)

from timedoctor_etl._s3 import (
    S3URI,
    S3Factory,
)


@dataclass(frozen=True)
class State:
    computer_activity_saved_until: DatetimeUTC
    worklogs_saved_until: DatetimeUTC


@dataclass(frozen=True)
class EtlDateRanges:
    computer_activity: DateRange
    worklogs: DateRange


def plus_relative_delta(date: DatetimeUTC, delta: relativedelta) -> DatetimeUTC:
    return Bug.assume_success(
        "plus_relative_delta",
        inspect.currentframe(),
        (),
        DatetimeUTC.assert_utc(date.date_time + delta),
    )


def minus_relative_delta(date: DatetimeUTC, delta: relativedelta) -> DatetimeUTC:
    return Bug.assume_success(
        "minus_relative_delta",
        inspect.currentframe(),
        (),
        DatetimeUTC.assert_utc(date.date_time - delta),
    )


def plus_relative_delta_with_limit(
    date: DatetimeUTC,
    delta: relativedelta,
    upper_limit: DatetimeUTC,
) -> DatetimeUTC:
    candidate = plus_relative_delta(date, delta)
    return candidate if candidate.date_time < upper_limit.date_time else upper_limit


def calc_next_date_ranges(
    today: DatetimeUTC,
    state: State,
    delta: relativedelta,
) -> ResultE[EtlDateRanges]:
    def _date_range(start: DatetimeUTC) -> ResultE[DateRange]:
        return DateRange.new(
            start,
            plus_relative_delta_with_limit(
                start,
                delta,
                minus_relative_delta(today, relativedelta(days=1)),
            ),
        )

    return smash.smash_result_2(
        _date_range(state.computer_activity_saved_until),
        _date_range(state.worklogs_saved_until),
    ).map(lambda t: EtlDateRanges(*t))


def encode_state(state: State) -> JsonObj:
    return UnfoldedFactory.from_unfolded_dict(
        {
            "computer_activity_saved_until": (
                state.computer_activity_saved_until.date_time.isoformat()
            ),
            "worklogs_saved_until": state.worklogs_saved_until.date_time.isoformat(),
        },
    )


def decode_date_time(raw: str) -> ResultE[datetime]:
    try:
        return Result.success(datetime.fromisoformat(raw))
    except Exception as e:  # pylint: disable=broad-exception-caught # noqa: BLE001
        # False positive: this is only bad if the error is not returned
        return Result.failure(e)


def decode_state(state: JsonObj) -> ResultE[State]:
    computer_activity = JsonUnfolder.require(
        state,
        "computer_activity_saved_until",
        lambda j: Unfolder.to_primitive(j)
        .bind(JsonPrimitiveUnfolder.to_str)
        .bind(decode_date_time)
        .bind(DatetimeUTC.assert_utc),
    )
    worklogs = JsonUnfolder.require(
        state,
        "worklogs_saved_until",
        lambda j: Unfolder.to_primitive(j)
        .bind(JsonPrimitiveUnfolder.to_str)
        .bind(decode_date_time)
        .bind(DatetimeUTC.assert_utc),
    )
    return smash.smash_result_2(computer_activity, worklogs).map(lambda t: State(*t))


def save_state(new_state: State) -> Cmd[ResultE[None]]:
    return S3Factory.new_s3_client().bind(
        lambda c: c.save(
            S3URI("observes.state", "timedoctor_etl/state.json"),
            encode_state(new_state),
        ),
    )


def load_state() -> Cmd[ResultE[State]]:
    return S3Factory.new_s3_client().bind(
        lambda c: c.load_json(
            S3URI("observes.state", "timedoctor_etl/state.json"),
        ).map(lambda r: r.bind(decode_state)),
    )
