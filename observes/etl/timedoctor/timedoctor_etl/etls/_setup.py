import inspect
import logging
from collections.abc import (
    Callable,
)
from dataclasses import (
    dataclass,
)

from connection_manager import (
    ConnectionConf,
    ConnectionManagerFactory,
    Databases,
    DbClients,
    Roles,
    Warehouses,
)
from etl_utils.bug import (
    Bug,
)
from etl_utils.typing import (
    Tuple,
    TypeVar,
)
from fa_purity import (
    Cmd,
    FrozenDict,
    FrozenList,
    PureIterFactory,
    Result,
    ResultE,
    StreamTransform,
    cast_exception,
)
from timedoctor_sdk.auth import (
    AuthClientFactory,
    AuthToken,
    Credentials,
)
from timedoctor_sdk.client import (
    new_api_client,
)
from timedoctor_sdk.core import (
    ApiClient,
    CompanyId,
    CompanyName,
    ProjectId,
    ProjectName,
    UserId,
    UserName,
)

from timedoctor_etl.secrets import (
    creds_from_env,
)

_T = TypeVar("_T")
LOG = logging.getLogger(__name__)
new_manager = ConnectionManagerFactory.observes_manager().map(
    lambda r: Bug.assume_success("new_manager", inspect.currentframe(), (), r),
)
dummy_creds = Credentials("", "")
get_creds = creds_from_env().map(lambda r: r.value_or(dummy_creds))
get_token: Cmd[AuthToken] = get_creds.bind(
    lambda c: AuthClientFactory.new()
    .new_token(c)
    .map(lambda r: Bug.assume_success("get_token", inspect.currentframe(), (), r)),
)


def with_common_setup(
    procedure: Callable[[DbClients, ApiClient], Cmd[ResultE[_T]]],
) -> Cmd[ResultE[_T]]:
    conf = ConnectionConf(
        Warehouses.GENERIC_COMPUTE,
        Roles.ETL_UPLOADER,
        Databases.OBSERVES,
    )
    return new_manager.bind(
        lambda manager: get_token.map(lambda t: new_api_client(LOG, t)).bind(
            lambda api: manager.execute_with_snowflake(
                lambda d: procedure(d, api),
                conf,
            ).map(
                lambda r: r.alt(
                    lambda e: e.map(
                        lambda x: x,
                        cast_exception,
                    ),
                ),
            ),
        ),
    )


@dataclass(frozen=True)
class CoreData:
    company: CompanyId
    project_map: FrozenDict[ProjectId, ProjectName]
    users_map: FrozenDict[UserId, UserName]


def _check_company(
    company: Tuple[CompanyId, CompanyName],
) -> ResultE[Tuple[CompanyId, CompanyName]]:
    if company[1] == CompanyName("Fluid Attacks"):
        return Result.success(company)
    return Result.failure(ValueError(f"Unexpected company: {company[1]}")).alt(cast_exception)


def _one_element(items: FrozenList[_T]) -> ResultE[_T]:
    if len(items) == 1:
        return Result.success(items[0])
    return Result.failure(ValueError(f"Expected only one element but got: {len(items)}")).alt(
        cast_exception,
    )


def get_core_data(
    client: ApiClient,
) -> Cmd[CoreData]:
    start_msg = Cmd.wrap_impure(lambda: LOG.info("Getting core data..."))
    return start_msg + (
        client.get_token_companies_id.map(
            lambda c: Bug.assume_success(
                "check_company",
                inspect.currentframe(),
                (str(c),),
                _one_element(c).bind(_check_company),
            ),
        )
        .map(lambda c: c[0])
        .bind(
            lambda c: client.get_projects_id(c)
            .transform(lambda s: StreamTransform.chain(s.map(PureIterFactory.from_list)))
            .to_list()
            .map(lambda i: FrozenDict(dict(i)))
            .bind(
                lambda project_map: client.get_users_id(c)
                .transform(lambda s: StreamTransform.chain(s.map(PureIterFactory.from_list)))
                .to_list()
                .map(lambda u: CoreData(c, project_map, FrozenDict(dict(u)))),
            ),
        )
    ).bind(
        lambda d: Cmd.wrap_impure(
            lambda: LOG.info("Core data loaded with %s users", len(d.users_map.items())),
        )
        + Cmd.wrap_value(d),
    )
