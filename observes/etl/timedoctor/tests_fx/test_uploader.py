import datetime
import logging

import pytest
from fa_purity import (
    Cmd,
    FrozenDict,
    Result,
    Unsafe,
)
from fa_purity.date_time import (
    DatetimeFactory,
    RawDatetime,
)
from redshift_client.core.id_objs import (
    Identifier,
    SchemaId,
)
from timedoctor_sdk.core import (
    DeviceId,
    ProjectId,
    ProjectName,
    TaskId,
    TaskName,
    UserId,
    Worklog,
    WorklogId,
    WorklogMode,
)

from timedoctor_etl.etls import (
    _setup,
)
from timedoctor_etl.uploader import (
    new_uploader,
)

LOG = logging.getLogger(__name__)
TEST_SCHEMA = SchemaId(Identifier.new("timedoctor_test"))


def test_upload_worklog() -> None:
    mock_worklog_id = WorklogId(
        TaskId("3"),
        TaskName("Task"),
        ProjectId("2"),
        ProjectName("Project"),
        DeviceId("foo"),
        UserId("user_1"),
    )
    mock_worklog = Worklog(
        mock_worklog_id,
        WorklogMode.BREAK,
        DatetimeFactory.new_utc(
            RawDatetime(
                year=2023,
                month=1,
                day=1,
                hour=0,
                minute=0,
                second=0,
                microsecond=0,
                time_zone=datetime.UTC,
            ),
        )
        .alt(Unsafe.raise_exception)
        .to_union(),
        3600,
    )
    cmd: Cmd[None] = _setup.with_common_setup(
        lambda d, _: d.connection.cursor(LOG)
        .map(d.new_table_client)
        .map(lambda c: new_uploader(c, TEST_SCHEMA, FrozenDict({})))
        .bind(lambda u: u.upload_worklogs((mock_worklog,)).map(Result.success)),
    ).map(lambda r: r.alt(Unsafe.raise_exception).to_union())
    with pytest.raises(SystemExit):
        cmd.compute()
