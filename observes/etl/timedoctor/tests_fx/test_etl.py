import pytest
from dateutil.relativedelta import (
    relativedelta,
)
from fa_purity import (
    Cmd,
)
from fa_purity.date_time import (
    DatetimeFactory,
)
from redshift_client.core.id_objs import (
    Identifier,
    SchemaId,
)

from timedoctor_etl.etls import (
    start_etls,
)
from timedoctor_etl.etls.state import (
    State,
    minus_relative_delta,
)

TEST_SCHEMA = SchemaId(Identifier.new("timedoctor_test"))


def test_main() -> None:
    cmd: Cmd[None] = DatetimeFactory.date_now().bind(
        lambda today: start_etls(
            TEST_SCHEMA,
            State(
                minus_relative_delta(today, relativedelta(days=7)),
                minus_relative_delta(today, relativedelta(days=7)),
            ),
            1,
        ),
    )
    with pytest.raises(SystemExit):
        cmd.compute()
