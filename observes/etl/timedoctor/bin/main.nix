{ inputs, makeScript, projectPath, ... }@makes_inputs:
let
  root = projectPath inputs.observesIndex.etl.timedoctor.root;
  bundle = import "${root}/entrypoint.nix" makes_inputs;
  env = bundle.env.runtime;
  common_vars = import (projectPath "/observes/common/common_vars.nix") {
    inherit projectPath;
  };
in makeScript {
  name = "timedoctor-etl";
  searchPaths = {
    bin = [ env ];
    export = common_vars;
  };
  entrypoint = ''timedoctor-etl "''${@}"'';
}
