{ inputs, makePythonVscodeSettings, projectPath, ... }@makes_inputs:
let
  root = projectPath inputs.observesIndex.etl.timedoctor.root;
  bundle = import "${root}/entrypoint.nix" makes_inputs;
in makePythonVscodeSettings {
  env = bundle.env.dev;
  bins = [ ];
  name = "observes-timedoctor-etl-env-dev";
}
