from etl_utils.natural import NaturalOperations
from fa_purity import NewFrozenList, Unsafe
from gitlab_sdk.ids import MrInternalId

from gitlab_etl.state._core import (
    Completeness,
    ContinuousMrRanges,
    MrCompleteness,
    MrIdRange,
)
from gitlab_etl.state.utils import squash_mr_completeness


def _mr_range(start: int, end: int) -> MrIdRange:
    return (
        MrIdRange.from_endpoints(
            MrInternalId(NaturalOperations.absolute(start)),
            MrInternalId(NaturalOperations.absolute(end)),
        )
        .alt(Unsafe.raise_exception)
        .to_union()
    )


def mock_completeness() -> MrCompleteness:
    return (
        MrCompleteness.new(
            ContinuousMrRanges.new(
                NewFrozenList.new(
                    _mr_range(100, 123),
                    _mr_range(123, 299),
                    _mr_range(299, 322),
                    _mr_range(322, 356),
                    _mr_range(356, 400),
                    _mr_range(400, 404),
                    _mr_range(404, 414),
                    _mr_range(414, 477),
                ),
            )
            .alt(Unsafe.raise_exception)
            .to_union(),
            NewFrozenList.new(
                Completeness.MISSING,
                Completeness.MISSING,
                Completeness.COMPLETED,
                Completeness.COMPLETED,
                Completeness.ERROR,
                Completeness.COMPLETED,
                Completeness.ERROR,
                Completeness.ERROR,
            ),
        )
        .alt(Unsafe.raise_exception)
        .to_union()
    )


def test_squash() -> None:
    expected = (
        MrCompleteness.new(
            ContinuousMrRanges.new(
                NewFrozenList.new(
                    _mr_range(100, 299),
                    _mr_range(299, 356),
                    _mr_range(356, 400),
                    _mr_range(400, 404),
                    _mr_range(404, 477),
                ),
            )
            .alt(Unsafe.raise_exception)
            .to_union(),
            NewFrozenList.new(
                Completeness.MISSING,
                Completeness.COMPLETED,
                Completeness.ERROR,
                Completeness.COMPLETED,
                Completeness.ERROR,
            ),
        )
        .alt(Unsafe.raise_exception)
        .to_union()
    )
    assert squash_mr_completeness(mock_completeness()) == expected
