from collections.abc import Callable
from typing import (
    TypeVar,
)

from fa_purity import (
    Cmd,
    CmdUnwrapper,
    Coproduct,
    FrozenList,
    Maybe,
    Result,
    ResultE,
    Stream,
    UnitType,
    Unsafe,
    cast_exception,
    unit,
)
from fa_purity.json import (
    JsonPrimitive,
)
from redshift_client.sql_client import (
    DbPrimitive,
)

_A = TypeVar("_A")
_T = TypeVar("_T")
_S = TypeVar("_S")
_F = TypeVar("_F")


def none_to_unit(_: None) -> UnitType:
    return unit


def none_result_to_unit(result: Result[None, _F]) -> Result[UnitType, _F]:
    return result.map(none_to_unit)


def int_to_str(number: int) -> str:
    return str(number)


def require_index(items: FrozenList[_T], index: int) -> ResultE[_T]:
    try:
        return Result.success(items[index])
    except IndexError as err:
        return Result.failure(cast_exception(err))


def get_index(items: FrozenList[_T], index: int) -> Maybe[_T]:
    return require_index(items, index).map(lambda v: Maybe.some(v)).value_or(Maybe.empty())


def to_json_primitive(value: DbPrimitive) -> ResultE[JsonPrimitive]:
    return value.map(
        lambda p: Result.success(p),
        lambda _: Result.failure(TypeError("Datetime is not a json primitive")).alt(cast_exception),
    )


def return_first_error(
    stream: Stream[Result[_S, _F]],
    default_success: Result[_S, _F],
) -> Cmd[Result[_S, _F]]:
    def _action(unwrapper: CmdUnwrapper) -> Result[_S, _F]:
        items = unwrapper.act(Unsafe.stream_to_iter(stream))
        for i in items:
            success = i.map(lambda _: True).value_or(False)
            if not success:
                return i
        return default_success

    return Cmd.new_cmd(_action)


def error_handler(procedure: Callable[[], _T]) -> ResultE[_T]:
    try:
        return Result.success(procedure())
    except Exception as e:  # noqa: BLE001
        # because the error is returned not ignored
        return Result.failure(e)


def merge_failures(item: Result[_A, Coproduct[_T, _T]]) -> Result[_A, _T]:
    return item.alt(
        lambda c: c.map(
            lambda x: x,
            lambda x: x,
        ),
    )
