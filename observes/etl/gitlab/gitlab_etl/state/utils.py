import inspect
from dataclasses import dataclass

from etl_utils.bug import Bug
from fa_purity import Bool, Maybe, NewFrozenList, Result, ResultE

from gitlab_etl.state._core import Completeness, ContinuousMrRanges, MrCompleteness, MrIdRange


@dataclass(frozen=True)
class _State:
    acc_completeness: NewFrozenList[tuple[MrIdRange, Completeness]]
    prev: Maybe[tuple[MrIdRange, Completeness]]


def _merge_ranges(range_1: MrIdRange, range_2: MrIdRange) -> ResultE[MrIdRange]:
    return Bool.from_primitive(range_1.end == range_2.start).map(
        lambda _: MrIdRange.from_endpoints(range_1.start, range_2.end),
        lambda _: Result.failure(ValueError("Cannot merge ranges")),
    )


def _merge(state: _State, current: tuple[MrIdRange, Completeness]) -> _State:
    return state.prev.map(
        lambda p: Bool.from_primitive(p[1] == current[1]).map(
            lambda _: _State(
                state.acc_completeness,
                Maybe.some(
                    (
                        Bug.assume_success(
                            "_merge_ranges",
                            inspect.currentframe(),
                            (str(p[0]), str(current[0])),
                            _merge_ranges(p[0], current[0]),
                        ),
                        current[1],
                    ),
                ),
            ),
            lambda _: _State(
                NewFrozenList((*state.acc_completeness, p)),
                Maybe.some(current),
            ),
        ),
    ).or_else_call(lambda: _State(state.acc_completeness, Maybe.some(current)))


def squash_mr_completeness(completeness: MrCompleteness) -> MrCompleteness:
    init = _State(NewFrozenList.new(), Maybe.empty())
    final_state = completeness.tagged_ranges().reduce(_merge, init)
    new_items: NewFrozenList[tuple[MrIdRange, Completeness]] = NewFrozenList(
        (*final_state.acc_completeness, *final_state.prev.map(lambda t: (t,)).value_or(())),
    )
    result = ContinuousMrRanges.new(new_items.map(lambda t: t[0])).bind(
        lambda c: MrCompleteness.new(c, new_items.map(lambda t: t[1])),
    )
    return Bug.assume_success(
        "squash_mr_completeness",
        inspect.currentframe(),
        (str(completeness),),
        result,
    )
