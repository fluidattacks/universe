from collections.abc import Callable
from typing import TypeVar

from etl_utils.natural import Natural
from fa_purity import FrozenDict, NewFrozenList, Result, ResultE, ResultSmash, ResultTransform
from fa_purity.json import (
    JsonObj,
    JsonPrimitiveUnfolder,
    JsonUnfolder,
    JsonValue,
    Unfolder,
)
from gitlab_sdk.ids import MrInternalId, ProjectId

from gitlab_etl.state._core import (
    Completeness,
    ContinuousMrRanges,
    MrCompleteness,
    MrIdRange,
)

_T = TypeVar("_T")


def _handle_value_error(value: Callable[[], _T]) -> ResultE[_T]:
    try:
        return Result.success(value())
    except ValueError as error:
        return Result.failure(error)


def _str_to_int(value: str) -> ResultE[int]:
    return _handle_value_error(lambda: int(value))


def _require_int(value: JsonValue) -> ResultE[int]:
    return Unfolder.to_primitive(value).bind(JsonPrimitiveUnfolder.to_int)


def _require_str(value: JsonValue) -> ResultE[str]:
    return Unfolder.to_primitive(value).bind(JsonPrimitiveUnfolder.to_str)


def _decode_obj(expected_type: str, raw: JsonObj) -> ResultE[JsonValue]:
    _type = JsonUnfolder.require(
        raw,
        "type",
        lambda v: Unfolder.to_primitive(v).bind(JsonPrimitiveUnfolder.to_str),
    ).bind(
        lambda t: Result.success(None)
        if t == expected_type
        else Result.failure(
            ValueError(f"Invalid object. Expected `{expected_type}` but got `{t}`"),
            None,
        ),
    )
    return _type.bind(lambda _: ResultTransform.get_key(raw, "value"))


def _decode_range(raw: JsonObj) -> ResultE[MrIdRange]:
    def _decode(value: JsonObj) -> ResultE[MrIdRange]:
        start = (
            JsonUnfolder.require(value, "start", _require_int)
            .bind(Natural.from_int)
            .map(MrInternalId)
        )
        end = (
            JsonUnfolder.require(value, "end", _require_int)
            .bind(Natural.from_int)
            .map(MrInternalId)
        )
        return start.bind(lambda s: end.bind(lambda e: MrIdRange.from_endpoints(s, e)))

    return _decode_obj("MrIdRange", raw).bind(Unfolder.to_json).bind(_decode)


def _decode_completeness(raw: str) -> ResultE[Completeness]:
    return _handle_value_error(lambda: Completeness(raw.lower()))


def _decode_pair(raw: JsonObj) -> ResultE[tuple[MrIdRange, Completeness]]:
    range_item = JsonUnfolder.require(raw, "range", Unfolder.to_json).bind(_decode_range)
    completeness = JsonUnfolder.require(raw, "completeness", _require_str).bind(
        _decode_completeness,
    )
    return ResultSmash.smash_result_2(range_item, completeness)


def decode_mr_completeness(raw: JsonObj) -> ResultE[MrCompleteness]:
    return (
        _decode_obj("MrCompleteness", raw)
        .bind(lambda v: Unfolder.to_list_of(v, lambda v: Unfolder.to_json(v).bind(_decode_pair)))
        .bind(
            lambda i: ContinuousMrRanges.new(NewFrozenList(i).map(lambda t: t[0])).bind(
                lambda c: MrCompleteness.new(c, NewFrozenList(i).map(lambda t: t[1])),
            ),
        )
    )


def _decode_mrs_map(raw: JsonObj) -> ResultE[FrozenDict[ProjectId, MrCompleteness]]:
    items = NewFrozenList(tuple(raw.items())).map(
        lambda t: ResultSmash.smash_result_2(
            _str_to_int(t[0]).bind(Natural.from_int).map(ProjectId),
            Unfolder.to_json(t[1]).bind(decode_mr_completeness),
        ),
    )
    return ResultTransform.all_ok_2(items).map(lambda i: FrozenDict(dict(i)))
