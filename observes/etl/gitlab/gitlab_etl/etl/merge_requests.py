import inspect
import logging
from dataclasses import dataclass
from typing import TypeVar

from etl_utils.bug import Bug
from etl_utils.natural import Natural, NaturalOperations
from etl_utils.parallel import ThreadPool
from fa_purity import (
    Bool,
    Cmd,
    CmdTransform,
    FrozenDict,
    Maybe,
    NewFrozenList,
    PureIter,
    PureIterFactory,
    PureIterTransform,
    ResultE,
    ResultTransform,
    UnitType,
    unit,
)
from gitlab_sdk.ids import MrGlobalId, MrInternalId, ProjectId
from gitlab_sdk.merge_requests import MrsClient
from gitlab_sdk.merge_requests.core import MergeRequest, MergeRequestState

from gitlab_etl._utils import none_to_unit
from gitlab_etl.db.merge_request import MrDbClient, MrObj
from gitlab_etl.state import Completeness, ContinuousMrRanges, MrCompleteness, MrIdRange
from gitlab_etl.state.utils import squash_mr_completeness

LOG = logging.getLogger(__name__)
_T = TypeVar("_T")


@dataclass(frozen=True)
class MrEtlConf:
    max_items: Natural


@dataclass(frozen=True)
class _InProjectMrObj:
    internal_id: MrInternalId
    global_id: MrGlobalId
    mr: MergeRequest


@dataclass(frozen=True)
class _TaggedMrObj:
    iid: MrInternalId
    obj: Maybe[_InProjectMrObj]
    tag: Completeness


def _cache_iter(items: PureIter[_T]) -> PureIter[_T]:
    """
    Consumes a `PureIter` to generate a in-memory list that is used by the returned `PureIter`.

    This is a sort of cache that saves computation time of the `PureIter` elements.
    """
    return PureIterFactory.from_list(items.to_list())


def _to_list(items: PureIter[_T]) -> NewFrozenList[_T]:
    return NewFrozenList(items.to_list())


def _calc_final_endpoint(state: MrCompleteness) -> MrInternalId:
    return (
        ResultTransform.get_index(state.items.ranges, -1)
        .map(lambda r: r.end)
        .value_or(MrInternalId(NaturalOperations.absolute(1)))
    )


def _classify_mr_obj(obj: _InProjectMrObj) -> _TaggedMrObj:
    """Tag a `_InProjectMrObj` with the corresponding completeness."""
    is_stable_state = Bool.from_primitive(
        obj.mr.full_state.state in (MergeRequestState.CLOSED, MergeRequestState.MERGED),
    )
    return is_stable_state.map(
        lambda _: _TaggedMrObj(obj.internal_id, Maybe.some(obj), Completeness.COMPLETED),
        lambda _: _TaggedMrObj(obj.internal_id, Maybe.empty(), Completeness.MISSING),
    )


def _to_mr_completeness(
    tagged_data: PureIter[_TaggedMrObj],
) -> ResultE[MrCompleteness]:
    """
    Transform various `_TaggedMrObj` into a `MrCompleteness`.

    The objects must have a sequential and continuos internal mr ids
    """
    full_range = ContinuousMrRanges.new(
        NewFrozenList(tagged_data.map(lambda t: MrIdRange.single(t.iid)).to_list()),
    )
    completeness = NewFrozenList(tagged_data.map(lambda t: t.tag).to_list())
    return full_range.bind(lambda r: MrCompleteness.new(r, completeness))


def get_missing_ranges(state: MrCompleteness) -> PureIter[MrIdRange]:
    return (
        PureIterFactory.from_list(state.items.ranges.items)
        .enumerate(0)
        .map(lambda t: (t[1], state.completeness.items[t[0]]))
        .filter(lambda t: t[1] is Completeness.MISSING)
        .map(lambda t: t[0])
    )


def _replace_state(
    old: MrCompleteness,
    new: PureIter[tuple[MrIdRange, ResultE[MrCompleteness]]],
) -> ResultE[MrCompleteness]:
    new_state = FrozenDict(dict(new.to_list()))
    items = _to_list(old.tagged_ranges()).bind(
        lambda t: new_state[t[0]]
        .map(lambda c: NewFrozenList(c.tagged_ranges().to_list()))
        .value_or(NewFrozenList.new(t))
        if t[0] in new_state
        else NewFrozenList.new(t),
    )

    return ContinuousMrRanges.new(items.map(lambda i: i[0])).bind(
        lambda c: MrCompleteness.new(c, items.map(lambda i: i[1])),
    )


def _append_state(
    old: MrCompleteness,
    new: tuple[MrIdRange, ResultE[MrCompleteness]],
) -> ResultE[MrCompleteness]:
    _new_completeness = (
        new[1]
        .map(
            lambda c: NewFrozenList(c.tagged_ranges().to_list()),
        )
        .value_or(NewFrozenList.new((new[0], Completeness.ERROR)))
    )
    items = NewFrozenList(_to_list(old.tagged_ranges()).items + _new_completeness.items)
    return ContinuousMrRanges.new(items.map(lambda i: i[0])).bind(
        lambda c: MrCompleteness.new(c, items.map(lambda i: i[1])),
    )


@dataclass(frozen=True)
class MrEtl:
    client: MrsClient
    db: MrDbClient
    project: ProjectId
    pool: ThreadPool

    def _save_data(
        self,
        tagged_data: PureIter[_TaggedMrObj],
    ) -> Cmd[ResultE[UnitType]]:
        data = tagged_data.map(lambda t: t.obj).transform(PureIterTransform.filter_maybe)
        return self.db.save_mrs(
            NewFrozenList(data.to_list()).map(
                lambda j: MrObj(self.project, j.internal_id, j.global_id, j.mr),
            ),
        )

    def _mr_data_per_range(
        self,
        id_range: MrIdRange,
    ) -> Cmd[PureIter[_TaggedMrObj]]:
        items = (
            self.pool.in_threads(
                id_range.to_iter().map(
                    lambda i: self.client.get_mr(self.project, i).map(lambda r: (i, r)),
                ),
            )
            .to_list()
            .map(NewFrozenList)
        )
        return items.map(
            lambda i: PureIterFactory.from_list(i.items).map(
                lambda t: t[1]
                .to_coproduct()
                .map(
                    lambda i: _classify_mr_obj(_InProjectMrObj(t[0], *i)),
                    lambda _: _TaggedMrObj(t[0], Maybe.empty(), Completeness.ERROR),
                ),
            ),
        )

    def _mr_range_etl(
        self,
        id_range: MrIdRange,
    ) -> Cmd[ResultE[MrCompleteness]]:
        msg = Cmd.wrap_impure(lambda: LOG.info("Executing MR range etl over %s", id_range))

        def _warning(item: ResultE[MrCompleteness]) -> Cmd[UnitType]:
            return item.to_coproduct().map(
                lambda _: Cmd.wrap_value(unit),
                lambda e: Cmd.wrap_impure(
                    lambda: LOG.warning("Error at MR range etl over %s. i.e. %s", id_range, e),
                ).map(none_to_unit),
            )

        return msg + (
            self._mr_data_per_range(id_range)
            .map(_cache_iter)
            .bind(
                lambda d: self._save_data(d).map(
                    lambda r: r.map(
                        lambda _: Bug.assume_success(
                            # _mr_data_per_range should return a `x: PureIter[_TaggedMrObj]`
                            # such that `_to_mr_completeness(x)` is not a Result failure
                            "_to_mr_completeness",
                            inspect.currentframe(),
                            (),
                            _to_mr_completeness(d),
                        ),
                    ),
                ),
            )
            .bind(lambda r: _warning(r).map(lambda _: r))
        )

    def missing_data_etl(self, state: MrCompleteness) -> Cmd[MrCompleteness]:
        """Execute ETL over missing mr data and return the resulting state."""
        cmds = (
            get_missing_ranges(state)
            .map(lambda i: self._mr_range_etl(i).map(lambda r: (i, r)))
            .to_list()
        )
        return CmdTransform.serial_merge(cmds).map(
            lambda r: Bug.assume_success(
                "missing_data_etl",
                inspect.currentframe(),
                (),
                _replace_state(state, PureIterFactory.from_list(r)),
            ),
        )

    def new_data_etl(self, state: MrCompleteness, limit: Natural) -> Cmd[MrCompleteness]:
        """
        Execute ETL over new mr ranges.

        [Notice] Limit is incremented by 1, therefore minimum limit is 1.
        """
        start = _calc_final_endpoint(state)
        new_range = Bug.assume_success(
            "new_range",
            inspect.currentframe(),
            (),
            MrIdRange.from_endpoints(
                start,
                MrInternalId(NaturalOperations.add(start.internal, Natural.succ(limit))),
            ),
        )

        cmd = self._mr_range_etl(new_range).map(lambda r: (new_range, r))
        return cmd.map(
            lambda r: Bug.assume_success(
                "new_data_etl",
                inspect.currentframe(),
                (),
                _append_state(state, r),
            ),
        )

    def full_etl(self, state: MrCompleteness, limit: Natural) -> Cmd[MrCompleteness]:
        return (
            self.missing_data_etl(state)
            .bind(lambda d: self.new_data_etl(d, limit))
            .map(squash_mr_completeness)
        )
