from dataclasses import dataclass

from connection_manager import CommonTableClient
from fa_purity import CmdTransform, NewFrozenList

from ._core import MrDbClient, MrObj
from ._save import save_mrs
from ._secondary import init_mr_assignees_table, init_mr_labels_table, init_mr_reviewers_table
from ._table import init_mr_table


@dataclass(frozen=True)
class MrDbClientFactory:
    @staticmethod
    def new(
        client: CommonTableClient,
    ) -> MrDbClient:
        init = CmdTransform.all_cmds_ok(
            init_mr_table(client),
            NewFrozenList.new(
                init_mr_assignees_table(client),
                init_mr_reviewers_table(client),
                init_mr_labels_table(client),
            ),
        )
        return MrDbClient(
            init,
            lambda d: save_mrs(client, d),
        )


__all__ = [
    "MrDbClient",
    "MrObj",
]
