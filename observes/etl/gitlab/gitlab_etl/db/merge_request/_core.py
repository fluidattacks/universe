from collections.abc import Callable
from dataclasses import dataclass

from fa_purity import Cmd, NewFrozenList, ResultE, UnitType
from gitlab_sdk.ids import MrGlobalId, MrInternalId, ProjectId
from gitlab_sdk.merge_requests.core import MergeRequest


@dataclass(frozen=True)
class MrObj:
    project: ProjectId
    internal_id: MrInternalId
    global_id: MrGlobalId
    mr: MergeRequest


@dataclass(frozen=True)
class MrDbClient:
    init_tables: Cmd[ResultE[UnitType]]
    save_mrs: Callable[
        [NewFrozenList[MrObj]],
        Cmd[ResultE[UnitType]],
    ]
