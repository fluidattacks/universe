from __future__ import annotations

import inspect
from dataclasses import dataclass, field
from datetime import datetime

from etl_utils import smash
from etl_utils.bug import (
    Bug,
)
from fa_purity import (
    FrozenDict,
    FrozenList,
    NewFrozenList,
    PureIterFactory,
    ResultE,
)
from fa_purity.json import Primitive
from gitlab_sdk.ids import MrInternalId, ProjectId, UserId
from redshift_client.client import (
    GroupedRows,
    TableRow,
)
from redshift_client.core.column import ColumnId
from redshift_client.core.table import Table
from redshift_client.sql_client import (
    DbPrimitive,
    DbPrimitiveFactory,
)

from gitlab_etl.db.merge_request._core import MrObj

from . import _secondary, _table


def _assert_table_row(
    table: Table,
    rows: FrozenDict[ColumnId, DbPrimitive],
    name: str,
    context: FrozenList[str],
) -> TableRow:
    return (
        TableRow.new(table, rows)
        .alt(
            lambda e: Bug.new(
                name,
                inspect.currentframe(),
                e,
                context,
            ).explode(),
        )
        .to_union()
    )


def _assert_grouped_row(
    table: Table,
    rows: FrozenList[TableRow],
    name: str,
    context: FrozenList[str],
) -> GroupedRows:
    return (
        GroupedRows.new(
            table,
            rows,
        )
        .alt(
            lambda e: Bug.new(
                name,
                inspect.currentframe(),
                e,
                context,
            ).explode(),
        )
        .to_union()
    )


def _truncate(raw: str, limit: int) -> str:
    return raw[:limit]


def encode_mr(item: MrObj) -> TableRow:
    mr = item.mr
    _encoded: dict[ColumnId, Primitive | datetime] = {
        _table.PROJECT_ID_COLUMN: item.project.project_id.value,
        _table.GLOBAL_ID_COLUMN: item.global_id.global_id.value,
        _table.INTERNAL_ID_COLUMN: item.internal_id.internal.value,
        _table.SHA_COLUMN: mr.shas.sha,
        _table.MERGE_COMMIT_SHA_COLUMN: mr.shas.merge_commit_sha.value_or(None),
        _table.SQUASH_COMMIT_SHA_COLUMN: mr.shas.squash_commit_sha.value_or(None),
        _table.CREATED_AT_COLUMN: mr.dates.created_at.date_time,
        _table.PREPARED_AT_COLUMN: mr.dates.prepared_at.map(lambda d: d.date_time).value_or(None),
        _table.UPDATED_AT_COLUMN: mr.dates.updated_at.map(lambda d: d.date_time).value_or(None),
        _table.MERGED_AT_COLUMN: mr.dates.merged_at.map(lambda d: d.date_time).value_or(None),
        _table.CLOSED_AT_COLUMN: mr.dates.closed_at.map(lambda d: d.date_time).value_or(None),
        _table.AUTHOR_COLUMN: mr.people.author.user_id.value,
        _table.MERGE_USER_COLUMN: mr.people.merge_user.map(lambda u: u.user_id.value).value_or(
            None,
        ),
        _table.CLOSED_BY_COLUMN: mr.people.closed_by.map(lambda u: u.user_id.value).value_or(None),
        _table.STATE_COLUMN: mr.full_state.state.value,
        _table.DETAILED_MERGE_STATUS_COLUMN: mr.full_state.detailed_merge_status,
        _table.HAS_CONFLICTS_COLUMN: mr.full_state.has_conflicts,
        _table.USER_NOTES_COUNT_COLUMN: mr.full_state.user_notes_count,
        _table.MERGE_ERROR_COLUMN: mr.full_state.merge_error.value_or(None),
        _table.TITLE_COLUMN: mr.properties.title,
        _table.DESCRIPTION_COLUMN: mr.properties.description.map(
            lambda s: _truncate(s, 256),
        ).value_or(None),
        _table.DRAFT_COLUMN: mr.properties.draft,
        _table.SQUASH_COLUMN: mr.properties.squash,
        _table.IMPORTED_COLUMN: mr.properties.imported,
        _table.IMPORTED_FROM_COLUMN: mr.properties.imported_from,
        _table.FIRST_CONTRIBUTION_COLUMN: mr.properties.first_contribution,
        _table.MERGE_AFTER_COLUMN: mr.properties.merge_after.map(lambda d: d.date_time).value_or(
            None,
        ),
        _table.SOURCE_PROJECT_ID_COLUMN: mr.origins.source_project_id.project_id.value,
        _table.SOURCE_BRANCH_COLUMN: mr.origins.source_branch,
        _table.TARGET_PROJECT_ID_COLUMN: mr.origins.target_project_id.project_id.value,
        _table.TARGET_BRANCH_COLUMN: mr.origins.target_branch,
        _table.TASK_COMPLETION_COUNT_COLUMN: mr.task_completion.count,
        _table.TASK_COMPLETION_COMPLETED_COUNT_COLUMN: mr.task_completion.completed_count,
    }
    rows = FrozenDict(_encoded).map(lambda c: c, DbPrimitiveFactory.from_raw)
    return _assert_table_row(
        _table.mr_table(),
        rows,
        "encode_mr",
        (str(item),),
    )


def encode_mrs(items: FrozenList[MrObj]) -> GroupedRows:
    return _assert_grouped_row(
        _table.mr_table(),
        PureIterFactory.from_list(items).map(encode_mr).to_list(),
        "encode_mrs",
        (),
    )


def encode_assignee(item: tuple[ProjectId, MrInternalId, UserId]) -> TableRow:
    _encoded: dict[ColumnId, Primitive | datetime] = {
        _secondary.PROJECT_ID_COLUMN: item[0].project_id.value,
        _secondary.MR_INTERNAL_ID_COLUMN: item[1].internal.value,
        _secondary.ASSIGNEE_COLUMN: item[2].user_id.value,
    }
    rows = FrozenDict(_encoded).map(lambda c: c, DbPrimitiveFactory.from_raw)
    return _assert_table_row(
        _secondary.mr_assignees_table(),
        rows,
        "encode_assignee",
        PureIterFactory.from_list(item).map(str).to_list(),
    )


def encode_assignees(item: MrObj) -> GroupedRows:
    return _assert_grouped_row(
        _secondary.mr_assignees_table(),
        PureIterFactory.from_list(item.mr.people.assignees)
        .map(lambda a: encode_assignee((item.project, item.internal_id, a)))
        .to_list(),
        "encode_assignees",
        (),
    )


def encode_reviewer(item: tuple[ProjectId, MrInternalId, UserId]) -> TableRow:
    _encoded: dict[ColumnId, Primitive | datetime] = {
        _secondary.PROJECT_ID_COLUMN: item[0].project_id.value,
        _secondary.MR_INTERNAL_ID_COLUMN: item[1].internal.value,
        _secondary.REVIEWER_COLUMN: item[2].user_id.value,
    }
    rows = FrozenDict(_encoded).map(lambda c: c, DbPrimitiveFactory.from_raw)
    return _assert_table_row(
        _secondary.mr_reviewers_table(),
        rows,
        "encode_reviewer",
        PureIterFactory.from_list(item).map(str).to_list(),
    )


def encode_reviewers(item: MrObj) -> GroupedRows:
    return _assert_grouped_row(
        _secondary.mr_reviewers_table(),
        PureIterFactory.from_list(item.mr.people.reviewers)
        .map(lambda a: encode_reviewer((item.project, item.internal_id, a)))
        .to_list(),
        "encode_reviewers",
        (),
    )


def encode_label(item: tuple[ProjectId, MrInternalId, str]) -> TableRow:
    _encoded: dict[ColumnId, Primitive | datetime] = {
        _secondary.PROJECT_ID_COLUMN: item[0].project_id.value,
        _secondary.MR_INTERNAL_ID_COLUMN: item[1].internal.value,
        _secondary.LABEL_COLUMN: item[2],
    }
    rows = FrozenDict(_encoded).map(lambda c: c, DbPrimitiveFactory.from_raw)
    return _assert_table_row(
        _secondary.mr_labels_table(),
        rows,
        "encode_label",
        PureIterFactory.from_list(item).map(str).to_list(),
    )


def encode_labels(item: MrObj) -> GroupedRows:
    return _assert_grouped_row(
        _secondary.mr_labels_table(),
        PureIterFactory.from_list(item.mr.properties.labels)
        .map(lambda a: encode_label((item.project, item.internal_id, a)))
        .to_list(),
        "encode_labels",
        (),
    )


@dataclass(frozen=True)
class EncodedMrs:
    @dataclass(frozen=True)
    class _Private:
        pass

    _private: EncodedMrs._Private = field(repr=False, hash=False, compare=False)
    main_rows: GroupedRows
    assignees_rows: GroupedRows
    reviewers_rows: GroupedRows
    labels_rows: GroupedRows

    @staticmethod
    def encode(mr: MrObj) -> EncodedMrs:
        return EncodedMrs(
            EncodedMrs._Private(),
            encode_mrs((mr,)),
            encode_assignees(mr),
            encode_reviewers(mr),
            encode_labels(mr),
        )

    @staticmethod
    def from_raw(
        main_rows: FrozenList[TableRow],
        assignees_rows: FrozenList[TableRow],
        reviewers_rows: FrozenList[TableRow],
        labels_rows: FrozenList[TableRow],
    ) -> ResultE[EncodedMrs]:
        return smash.smash_result_4(
            GroupedRows.new(_table.mr_table(), main_rows),
            GroupedRows.new(_secondary.mr_assignees_table(), assignees_rows),
            GroupedRows.new(_secondary.mr_reviewers_table(), reviewers_rows),
            GroupedRows.new(_secondary.mr_labels_table(), labels_rows),
        ).map(lambda t: EncodedMrs(EncodedMrs._Private(), *t))

    def append(self, other: EncodedMrs) -> EncodedMrs:
        result = EncodedMrs.from_raw(
            self.main_rows.rows + other.main_rows.rows,
            self.assignees_rows.rows + other.assignees_rows.rows,
            self.reviewers_rows.rows + other.reviewers_rows.rows,
            self.labels_rows.rows + other.labels_rows.rows,
        )
        return Bug.assume_success("append_encoded_mr", inspect.currentframe(), (), result)

    @classmethod
    def encode_objs(cls, mrs: NewFrozenList[MrObj]) -> EncodedMrs:
        empty = Bug.assume_success(
            "empty_encoded_mr",
            inspect.currentframe(),
            (),
            EncodedMrs.from_raw((), (), (), ()),
        )
        return (
            PureIterFactory.from_list(mrs.items)
            .map(cls.encode)
            .reduce(lambda p, c: p.append(c), empty)
        )
