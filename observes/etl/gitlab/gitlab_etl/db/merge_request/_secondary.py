import inspect

from connection_manager import CommonTableClient
from etl_utils.bug import Bug
from fa_purity import Cmd, FrozenDict, ResultE, UnitType
from redshift_client.core.column import (
    Column,
    ColumnId,
)
from redshift_client.core.data_type.core import (
    DataType,
    PrecisionType,
    PrecisionTypes,
    StaticTypes,
)
from redshift_client.core.id_objs import (
    DbTableId,
    Identifier,
    SchemaId,
    TableId,
)
from redshift_client.core.table import Table
from redshift_client.sql_client import DbPrimitiveFactory

from gitlab_etl._utils import none_to_unit

MR_ASSIGNEES_TABLE_ID = DbTableId(
    SchemaId(Identifier.new("gitlab-ci")),
    TableId(Identifier.new("mr_assignees")),
)
MR_REVIEWERS_TABLE_ID = DbTableId(
    SchemaId(Identifier.new("gitlab-ci")),
    TableId(Identifier.new("mr_reviewers")),
)
MR_LABELS_TABLE_ID = DbTableId(
    SchemaId(Identifier.new("gitlab-ci")),
    TableId(Identifier.new("mr_labels")),
)
# ---
PROJECT_ID_COLUMN = ColumnId(Identifier.new("project_id"))
MR_INTERNAL_ID_COLUMN = ColumnId(Identifier.new("mr_internal_id"))
# ---
ASSIGNEE_COLUMN = ColumnId(Identifier.new("assignee"))
REVIEWER_COLUMN = ColumnId(Identifier.new("reviewer"))
LABEL_COLUMN = ColumnId(Identifier.new("label"))

_empty = DbPrimitiveFactory.from_raw(None)
_id_column = Column(DataType(StaticTypes.BIGINT), False, _empty)


def mr_assignees_table() -> Table:
    _columns: FrozenDict[ColumnId, Column] = FrozenDict(
        {
            PROJECT_ID_COLUMN: _id_column,
            MR_INTERNAL_ID_COLUMN: _id_column,
            ASSIGNEE_COLUMN: _id_column,
        },
    )
    order = (
        PROJECT_ID_COLUMN,
        MR_INTERNAL_ID_COLUMN,
        ASSIGNEE_COLUMN,
    )
    result = Table.new(order, _columns, frozenset(order))
    return Bug.assume_success("mr_assignees_table", inspect.currentframe(), (), result)


def init_mr_assignees_table(client: CommonTableClient) -> Cmd[ResultE[UnitType]]:
    return client.new_if_not_exist(
        MR_ASSIGNEES_TABLE_ID,
        mr_assignees_table(),
    ).map(
        lambda r: r.map(none_to_unit).alt(
            lambda e: Bug.new(
                "init_mr_assignees_table",
                inspect.currentframe(),
                e,
                (),
            ),
        ),
    )


def mr_reviewers_table() -> Table:
    _columns: FrozenDict[ColumnId, Column] = FrozenDict(
        {
            PROJECT_ID_COLUMN: _id_column,
            MR_INTERNAL_ID_COLUMN: _id_column,
            REVIEWER_COLUMN: _id_column,
        },
    )
    order = (
        PROJECT_ID_COLUMN,
        MR_INTERNAL_ID_COLUMN,
        REVIEWER_COLUMN,
    )
    result = Table.new(order, _columns, frozenset(order))
    return Bug.assume_success("mr_reviewers_table", inspect.currentframe(), (), result)


def init_mr_reviewers_table(client: CommonTableClient) -> Cmd[ResultE[UnitType]]:
    return client.new_if_not_exist(
        MR_REVIEWERS_TABLE_ID,
        mr_reviewers_table(),
    ).map(
        lambda r: r.map(none_to_unit).alt(
            lambda e: Bug.new(
                "init_mr_reviewers_table",
                inspect.currentframe(),
                e,
                (),
            ),
        ),
    )


def mr_labels_table() -> Table:
    str_column = Column(
        DataType(PrecisionType(PrecisionTypes.VARCHAR, 256)),
        False,
        _empty,
    )
    _columns: FrozenDict[ColumnId, Column] = FrozenDict(
        {
            PROJECT_ID_COLUMN: _id_column,
            MR_INTERNAL_ID_COLUMN: _id_column,
            LABEL_COLUMN: str_column,
        },
    )
    order = (
        PROJECT_ID_COLUMN,
        MR_INTERNAL_ID_COLUMN,
        LABEL_COLUMN,
    )
    result = Table.new(order, _columns, frozenset(order))
    return Bug.assume_success("mr_labels_table", inspect.currentframe(), (), result)


def init_mr_labels_table(client: CommonTableClient) -> Cmd[ResultE[UnitType]]:
    return client.new_if_not_exist(
        MR_LABELS_TABLE_ID,
        mr_labels_table(),
    ).map(
        lambda r: r.map(none_to_unit).alt(
            lambda e: Bug.new(
                "init_mr_labels_table",
                inspect.currentframe(),
                e,
                (),
            ),
        ),
    )
