import inspect

from connection_manager import (
    CommonTableClient,
)
from etl_utils.bug import (
    Bug,
)
from fa_purity import (
    Cmd,
    FrozenDict,
)
from redshift_client.core.column import (
    Column,
    ColumnId,
)
from redshift_client.core.data_type.core import (
    DataType,
    PrecisionType,
    PrecisionTypes,
    StaticTypes,
)
from redshift_client.core.id_objs import (
    DbTableId,
    Identifier,
    SchemaId,
    TableId,
)
from redshift_client.core.table import (
    Table,
)
from redshift_client.sql_client import (
    DbPrimitiveFactory,
)

FAIL_REASON_TABLE_ID = DbTableId(
    SchemaId(Identifier.new("gitlab-ci")),
    TableId(Identifier.new("fail_reason")),
)
JOB_ID_COLUMN = ColumnId(Identifier.new("job_id"))
REASON_COLUMN = ColumnId(Identifier.new("reason"))


def fail_reason_table() -> Table:
    empty = DbPrimitiveFactory.from_raw(None)
    _columns: FrozenDict[ColumnId, Column] = FrozenDict(
        {
            JOB_ID_COLUMN: Column(DataType(StaticTypes.BIGINT), False, empty),
            REASON_COLUMN: Column(
                DataType(PrecisionType(PrecisionTypes.VARCHAR, 256)),
                False,
                empty,
            ),
        },
    )
    result = Table.new((JOB_ID_COLUMN, REASON_COLUMN), _columns, frozenset([JOB_ID_COLUMN]))
    return Bug.assume_success("fail_reason_table", inspect.currentframe(), (), result)


def init_table(client: CommonTableClient) -> Cmd[None]:
    return client.new_if_not_exist(
        FAIL_REASON_TABLE_ID,
        fail_reason_table(),
    ).map(lambda r: Bug.assume_success("init_table", inspect.currentframe(), (), r))
