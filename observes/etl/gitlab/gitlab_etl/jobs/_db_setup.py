import inspect
from collections.abc import (
    Callable,
)
from datetime import (
    UTC,
    timedelta,
)
from typing import (
    TypeVar,
)

from connection_manager import (
    ConnectionConf,
    ConnectionManagerFactory,
    Databases,
    DbClients,
    Roles,
    Warehouses,
)
from etl_utils.bug import (
    Bug,
)
from fa_purity import (
    Cmd,
    ResultE,
    cast_exception,
)
from fa_purity.date_time import (
    DatetimeFactory,
    RawDatetime,
)

from gitlab_etl.db.core import (
    DateRange,
)

_T = TypeVar("_T")


def get_target_date() -> Cmd[DateRange]:
    return (
        DatetimeFactory.date_now()
        .map(lambda d: d.date_time - timedelta(days=1))
        .map(
            lambda d: DatetimeFactory.new_utc(
                RawDatetime(
                    year=d.year,
                    month=d.month,
                    day=d.day,
                    hour=0,
                    minute=0,
                    second=0,
                    microsecond=0,
                    time_zone=UTC,
                ),
            ),
        )
        .map(lambda r: Bug.assume_success("get_target_date", inspect.currentframe(), (), r))
        .map(lambda d: DateRange.only_start(d))
    )


def with_connection(
    process: Callable[[DbClients], Cmd[ResultE[_T]]],
) -> Cmd[ResultE[_T]]:
    conf = ConnectionConf(
        Warehouses.GENERIC_COMPUTE,
        Roles.ETL_UPLOADER,
        Databases.OBSERVES,
    )
    return (
        ConnectionManagerFactory.observes_manager()
        .map(lambda r: Bug.assume_success("connection_manager", inspect.currentframe(), (), r))
        .bind(
            lambda manager: manager.execute_with_snowflake(process, conf).map(
                lambda r: r.alt(
                    lambda c: c.map(
                        lambda e: e,
                        cast_exception,
                    ),
                ),
            ),
        )
    )
