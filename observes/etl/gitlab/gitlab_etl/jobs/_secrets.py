import inspect
from enum import Enum

from etl_utils.bug import (
    Bug,
)
from etl_utils.secrets import (
    GenericSecret,
    ObservesSecrets,
    ObservesSecretsFactory,
)
from fa_purity import (
    Cmd,
    CmdTransform,
    ResultE,
)
from gitlab_sdk import Credentials


class SupportedRepos(Enum):
    UNIVERSE = "universe"
    DATAHUB = "datahub"


def _gitlab_token(secrets: ObservesSecrets, repo: SupportedRepos) -> Cmd[ResultE[GenericSecret]]:
    if repo is SupportedRepos.UNIVERSE:
        return secrets.get_secret("OBSERVES_UNIVERSE_API_TOKEN").map(
            lambda r: r.alt(lambda e: Bug.new("get_universe_token", inspect.currentframe(), e, ())),
        )
    if repo is SupportedRepos.DATAHUB:
        return secrets.get_secret("DATAHUB_API_TOKEN").map(
            lambda r: r.alt(lambda e: Bug.new("get_datahub_token", inspect.currentframe(), e, ())),
        )


def get_gitlab_token(repo: SupportedRepos) -> Cmd[ResultE[Credentials]]:
    return CmdTransform.chain_cmd_result(
        ObservesSecretsFactory.from_env(),
        lambda s: _gitlab_token(s, repo).map(lambda r: r.map(lambda g: Credentials(g.value))),
    ).map(lambda r: r.alt(lambda c: c.map(lambda e: e, lambda e: e)))
