from typing import (
    NoReturn,
)

import click
from fa_purity import (
    Cmd,
    UnitType,
    Unsafe,
)

from gitlab_etl.jobs.universe import (
    mrs as universe_mrs,
)


@click.command()
def init() -> NoReturn:
    cmd: Cmd[UnitType] = universe_mrs.init_mrs_job().map(
        lambda r: r.alt(Unsafe.raise_exception).to_union(),
    )
    cmd.compute()


@click.command()
def init_state() -> NoReturn:
    cmd: Cmd[None] = universe_mrs.init_state_job().map(
        lambda r: r.alt(Unsafe.raise_exception).to_union(),
    )
    cmd.compute()


@click.command()
def start() -> NoReturn:
    cmd: Cmd[None] = universe_mrs.mrs_job().map(lambda r: r.alt(Unsafe.raise_exception).to_union())
    cmd.compute()


@click.group()
def merge_requests() -> None:
    # main cli entrypoint
    pass


merge_requests.add_command(init)
merge_requests.add_command(init_state)
merge_requests.add_command(start)
