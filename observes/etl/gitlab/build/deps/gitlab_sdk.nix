{ lib, makes_inputs, nixpkgs, python_pkgs, python_version, }:
let
  raw_src =
    makes_inputs.projectPath makes_inputs.inputs.observesIndex.sdk.gitlab.root;
  src = import "${raw_src}/build/filter.nix" nixpkgs.nix-filter raw_src;
  pynix = makes_inputs.inputs.buildPynix {
    nixpkgs = nixpkgs // { "${python_version}Packages" = python_pkgs; };
    pythonVersion = python_version;
  };
  bundle = import "${raw_src}/build" {
    inherit makes_inputs nixpkgs pynix python_version src;
  };
in bundle
