{ inputs, makeScript, projectPath, outputs, ... }@makes_inputs:
let
  deps = with inputs.observesIndex; [ tap.gitlab.root target.warehouse.root ];
  env = builtins.map inputs.getRuntime deps;
  common_vars = import (projectPath "/observes/common/common_vars.nix") {
    inherit projectPath;
  };
in makeScript {
  searchPaths = {
    bin = env;
    export = common_vars;
  };
  name = "observes-etl-gitlab-ephemeral";
  entrypoint = ./entrypoint.sh;
}
