# shellcheck shell=bash

function start_etl {
  local schema="${1}"
  local project="${2}"
  local state="${3}"
  local token="${4}"
  local inference_conf="${5}"
  export AWS_DEFAULT_REGION="us-east-1"

  echo "[INFO] Gitlab ETL for ${project}" \
    && echo "[INFO] MRs ETL started" \
    && echo '[INFO] Running tap' \
    && tap-gitlab stream \
      "mrs_closed" \
      "mrs_merged" \
      --project "${project}" \
      --api-key "${token}" \
      --max-pages 250 \
      --state "${state}" \
    | tap-json infer-and-adjust \
      --inference-conf "${inference_conf}" \
      > gitlab_data.singer \
    && echo '[INFO] Saving into snowflake' \
    && target-warehouse only-append \
      --schema-name "${schema}" \
      --s3-state "${state}" \
      --truncate \
      --use-snowflake \
      < gitlab_data.singer \
    && echo "[INFO] Jobs ETL started" \
    && tap-gitlab stream \
      "pipe_jobs_success" \
      "pipe_jobs_failed" \
      "pipe_jobs_canceled" \
      "pipe_jobs_skipped" \
      "pipe_jobs_manual" \
      --project "${project}" \
      --api-key "${token}" \
      --max-pages 1 \
      --state "${state}" \
      > gitlab_jobs_data.singer \
    && echo '[INFO] Saving into snowflake' \
    && target-warehouse only-append \
      --schema-name "${schema}" \
      --s3-state "${state}" \
      --truncate \
      --use-snowflake \
      < gitlab_jobs_data.singer
}

start_etl "${@}"
