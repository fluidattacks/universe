# shellcheck shell=bash

alias gitlab-etl="observes-etl-gitlab"

function start_etl {
  export_notifier_key \
    && sops_export_vars 'observes/secrets/prod.yaml' \
      datahub_repo_api_token \
    && gitlab-etl \
      'gitlab-ci' \
      '40890176' \
      's3://observes.state/gitlab_etl/datahub_state.json' \
      's3://observes.state/gitlab_etl_snowflake/datahub_state.json' \
      "${datahub_repo_api_token}" \
      "__argInferenceConf__"
}

start_etl
