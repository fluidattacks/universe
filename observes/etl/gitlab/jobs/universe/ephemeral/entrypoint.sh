# shellcheck shell=bash

alias gitlab-etl="observes-etl-gitlab-ephemeral"

function start_etl {
  sops_export_vars 'observes/secrets/prod.yaml' \
    OBSERVES_UNIVERSE_API_TOKEN \
    && export_notifier_key \
    && gitlab-etl \
      'gitlab_ci_issues' \
      '20741933' \
      "${OBSERVES_UNIVERSE_API_TOKEN}" \
      "__argInferenceConf__"
}

start_etl
