{ inputs, makeScript, outputs, projectPath, ... }:
let
  deps = with inputs.observesIndex; [ service.success_indicators.root ];
  env = builtins.map inputs.getRuntime deps;
  common_vars = import (projectPath "/observes/common/common_vars.nix") {
    inherit projectPath;
  };
in makeScript {
  replace = { __argInferenceConf__ = ./inference_conf.json; };
  searchPaths = {
    bin = env ++ [ outputs."/observes/etl/gitlab/jobs" ];
    export = common_vars;
    source = [
      outputs."/observes/common/db-creds"
      outputs."/common/utils/aws"
      outputs."/common/utils/sops"
    ];
  };
  name = "observes-etl-gitlab-universe";
  entrypoint = ./entrypoint.sh;
}
