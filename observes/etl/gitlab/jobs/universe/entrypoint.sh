# shellcheck shell=bash

alias gitlab-etl="observes-etl-gitlab"

function start_etl {
  sops_export_vars 'observes/secrets/prod.yaml' \
    OBSERVES_UNIVERSE_API_TOKEN \
    && export_notifier_key \
    && gitlab-etl \
      'gitlab-ci' \
      '20741933' \
      's3://observes.state/gitlab_etl_snowflake/product_state.json' \
      "${OBSERVES_UNIVERSE_API_TOKEN}" \
      "__argInferenceConf__" \
    && success-indicators \
      single-job \
      --job "gitlab_universe"
}

start_etl
