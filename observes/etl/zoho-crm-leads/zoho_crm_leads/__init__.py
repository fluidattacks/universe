from fa_purity import (
    Unsafe,
)

from zoho_crm_leads._logger import (
    set_logger,
)

__version__ = "0.1.0"

Unsafe.compute(set_logger(__name__, __version__))
