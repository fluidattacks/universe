import csv
import inspect
import logging
import os
import re
import sys
from typing import (
    NoReturn,
)

import boto3
from botocore.exceptions import (
    ClientError,
)
from etl_utils.bug import (
    Bug,
)
from etl_utils.secrets import (
    ObservesSecretsFactory,
)
from fa_purity import (
    Cmd,
    Unsafe,
)

from zoho_crm_leads._core import get_access_token, http_request

NOTIFIER_TIMEOUT_SEC = 10
SECRETS_FILE = os.environ.get("SECRETS_FILE", "")


def _download_raw_csv() -> dict[str, list[dict[str, str]]]:
    try:
        prefix = "lead-scoring/inferences/"
        raw_csv_content: dict[str, list[dict[str, str]]] = {}
        s3 = boto3.client("s3")  # type: ignore[misc]

        file_list = s3.list_objects_v2(  # type: ignore[misc]
            Bucket="business-models",
            Prefix=prefix,
        )

        for file in file_list.get("Contents", []):  # type: ignore[misc]
            file_key: str = file["Key"]  # type: ignore[misc]
            pattern = rf"^{re.escape(prefix)}(lead|contact)_scoring_inference\.csv$"

            if re.match(pattern, file_key):
                obj = s3.get_object(Bucket="business-models", Key=file_key)  # type: ignore[misc]
                data = obj["Body"]  # type: ignore[misc]
                reader = csv.DictReader(  # type: ignore[misc]
                    line.decode("utf-8")  # type: ignore[misc]
                    for line in data.iter_lines()  # type: ignore[misc]
                )

                records: list[dict[str, str]] = list(reader)
                if records:
                    entity: str = file_key.split("/")[-1].split("_")[0]
                    raw_csv_content[entity] = records

    except ClientError:  # type: ignore[misc]
        logging.exception("Error downloading CSV from S3")
        return {}
    else:
        return raw_csv_content


def _process_data(grouped_data: dict[str, list[dict[str, str | float]]], token: str) -> None:
    if not grouped_data:
        logging.error("Not valid csv files or valid records obtained from csv files.")
        return

    for entity, records in grouped_data.items():
        if not records:
            logging.warning("No records to process for %s", entity)
            continue

        batch_size = 10
        for i in range(0, len(records), batch_size):
            body = {"data": records[i : i + batch_size]}
            try:
                Unsafe.compute(http_request(token, body, entity))
            except (OSError, RuntimeError, ValueError):
                logging.exception(
                    "Error at sending data to Zoho CRM - Module: %s",
                    entity,
                )

        logging.info("All data was successfully sent to Zoho CRM")


def _validate_data(
    raw_data: dict[str, list[dict[str, str]]],
) -> dict[str, list[dict[str, str | float]]]:
    def _row_non_empty(row: dict[str, str | float]) -> bool:
        return bool(row)

    validated_data: dict[str, list[dict[str, str | float]]] = {}
    for _entity, data in raw_data.items():
        entity = f"{_entity}s".capitalize()
        if not data:
            logging.error("No data found for %s", entity)
            continue

        headers = set(data[0].keys())
        if "id" not in headers or not any(header in headers for header in ["mql", "sql"]):
            logging.error("Missing required columns in the CSV for %s.", entity)
            continue

        validated_data[entity] = list(filter(_row_non_empty, map(_validate_row, data)))

    return validated_data


def _validate_row(row: dict[str, str]) -> dict[str, str | float]:
    def _is_valid_id(id_value: str) -> bool:
        return id_value.isdigit() and len(id_value) == 19  # noqa: PLR2004

    def _is_valid_float(value: str) -> bool:
        try:
            float_value = float(value)
        except ValueError:
            return False
        else:
            max_value = 100.0
            return 0.0 <= float_value <= max_value

    filtered_row: dict[str, str | float] = {}

    id_value = row.get("id", "")
    if _is_valid_id(id_value):
        filtered_row["id"] = id_value
        for key in ["mql", "sql"]:
            value = row.get(key, "")
            if value and _is_valid_float(value):
                filtered_row[key] = float(value)

    return filtered_row if len(filtered_row) > 1 else {}


def main() -> NoReturn:
    if "--help" in sys.argv[1:]:
        sys.exit()

    access_token = (
        ObservesSecretsFactory.from_env()
        .map(lambda r: Bug.assume_success("observes_secrets", inspect.currentframe(), (), r))
        .bind(lambda s: s.zoho_creds)
        .bind(get_access_token)
    )

    def _action(token: str | None) -> None:
        if token is None:
            error = "Failed to retrieve access token"
            raise ValueError(error)

        raw_data = _download_raw_csv()
        grouped_data = _validate_data(raw_data)
        _process_data(grouped_data, token)

    cmd: Cmd[None] = access_token.bind(lambda t: Cmd.wrap_impure(lambda: _action(t)))
    cmd.compute()
