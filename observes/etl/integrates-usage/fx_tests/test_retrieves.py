import pytest
from dateutil.relativedelta import relativedelta
from fa_purity import Cmd, FrozenList, Unsafe
from fa_purity.date_time import DatetimeFactory

from integrates_usage_etl._cloudwatch._client_1 import CloudwatchLogClientFactory
from integrates_usage_etl._date_range import DateRange, NonZeroRelativeDelta
from integrates_usage_etl.usage._core import RetrievesUsage
from integrates_usage_etl.usage.retrieves import get_retrieves_usage


def test_get_retrieves() -> None:
    delta = NonZeroRelativeDelta.new(relativedelta(days=-1)).alt(Unsafe.raise_exception).to_union()
    date_range = DatetimeFactory.date_now().map(
        lambda d: DateRange.new(d, delta),
    )
    get_usage = date_range.bind(
        lambda dr: CloudwatchLogClientFactory.new()
        .map(
            lambda r: r.alt(Unsafe.raise_exception).to_union(),
        )
        .bind(
            lambda c: get_retrieves_usage(c, dr).map(
                lambda r: r.alt(
                    lambda c: c.map(
                        lambda u: ValueError(u),
                        lambda e: e,
                    ),
                )
                .alt(
                    Unsafe.raise_exception,
                )
                .to_union(),
            ),
        ),
    )

    def _check(items: FrozenList[RetrievesUsage]) -> None:
        assert items

    cmd: Cmd[None] = get_usage.map(_check)
    with pytest.raises(SystemExit):
        cmd.compute()
