{ makes_inputs, nixpkgs, pynix, python_version, src, }:
let
  deps = import ./deps { inherit makes_inputs nixpkgs pynix python_version; };
  pkgDeps = {
    runtime_deps = with deps.python_pkgs; [
      click
      connection-manager
      etl-utils
      fa-purity
      mypy-boto3-s3
      mypy-boto3-logs
      pathos
      pure-requests
      utils-logger
    ];
    build_deps = with deps.python_pkgs; [ flit-core ];
    test_deps = with deps.python_pkgs; [
      arch-lint
      mypy
      pytest
      pytest-cov
      ruff
    ];
  };
  bundle = pynix.stdBundle { inherit pkgDeps src; };
  vs_settings = pynix.vscodeSettingsShell { pythonEnv = bundle.env.dev; };
  coverage = pynix.coverageReport {
    inherit src;
    parentModulePath =
      makes_inputs.inputs.observesIndex.etl.integrates_usage.root;
    pythonDevEnv = bundle.env.dev;
    module = makes_inputs.inputs.observesIndex.etl.integrates_usage.pkg_name;
    testsFolder = "tests";
  };
in bundle // {
  inherit coverage;
  dev_hook = vs_settings.hook.text;
}
