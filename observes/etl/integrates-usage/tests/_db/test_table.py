import pytest
from fa_purity import Cmd, Result, ResultE, Unsafe
from redshift_client.core.id_objs import DbTableId, Identifier, SchemaId, TableId
from snowflake_client import TableClient

from integrates_usage_etl._db._table import init_table, table_definition


def test_table_def() -> None:
    assert table_definition()


def test_init() -> None:
    nothing: Cmd[ResultE[None]] = Cmd.wrap_value(Result.success(None))
    mock_client = TableClient(
        get=lambda _: Unsafe.raise_exception(Exception("Should not call")),
        exist=lambda _: Unsafe.raise_exception(Exception("Should not call")),
        insert=lambda _, _a, _b, _c: Unsafe.raise_exception(Exception("Should not call")),
        named_insert=lambda _, _a: Unsafe.raise_exception(Exception("Should not call")),
        rename=lambda _, _b: Unsafe.raise_exception(Exception("Should not call")),
        delete=lambda _: Unsafe.raise_exception(Exception("Should not call")),
        delete_cascade=lambda _: Unsafe.raise_exception(Exception("Should not call")),
        add_column=lambda _, _a: Unsafe.raise_exception(Exception("Should not call")),
        add_columns=lambda _, _a: Unsafe.raise_exception(Exception("Should not call")),
        new=lambda _, _a: Unsafe.raise_exception(Exception("Should not call")),
        new_if_not_exist=lambda _, _a: nothing,
        create_like=lambda _, _a: Unsafe.raise_exception(Exception("Should not call")),
        move_data=lambda _, _a: Unsafe.raise_exception(Exception("Should not call")),
        move=lambda _, _a: Unsafe.raise_exception(Exception("Should not call")),
        migrate=lambda _, _a: Unsafe.raise_exception(Exception("Should not call")),
    )
    table_id = DbTableId(SchemaId(Identifier.new("schema_1")), TableId(Identifier.new("table_1")))
    cmd: Cmd[None] = init_table(mock_client, table_id).map(
        lambda r: r.alt(Unsafe.raise_exception).to_union(),
    )
    with pytest.raises(SystemExit):
        cmd.compute()
