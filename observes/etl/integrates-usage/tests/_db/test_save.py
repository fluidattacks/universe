import pytest
from fa_purity import Cmd, PureIterFactory, Result, ResultE, Unsafe
from fa_purity._core.utils import raise_exception
from redshift_client.core.id_objs import DbTableId, Identifier, SchemaId, TableId
from snowflake_client import TableClient

from integrates_usage_etl._db._save import _encode_usage, save_usage
from integrates_usage_etl.usage._core import Usage
from tests import _utils


def test_encode() -> None:
    usage = Usage(_utils.mock_range(), "group_1", 456)
    assert _encode_usage(usage)


def test_save_usage() -> None:
    nothing: Cmd[ResultE[None]] = Cmd.wrap_value(Result.success(None))
    mock_client = TableClient(
        get=lambda _: Unsafe.raise_exception(Exception("Should not call")),
        exist=lambda _: Unsafe.raise_exception(Exception("Should not call")),
        insert=lambda _, _a, _b, _c: Unsafe.raise_exception(Exception("Should not call")),
        named_insert=lambda _, _a: nothing,
        rename=lambda _, _b: Unsafe.raise_exception(Exception("Should not call")),
        delete=lambda _: Unsafe.raise_exception(Exception("Should not call")),
        delete_cascade=lambda _: Unsafe.raise_exception(Exception("Should not call")),
        add_column=lambda _, _a: Unsafe.raise_exception(Exception("Should not call")),
        add_columns=lambda _, _a: Unsafe.raise_exception(Exception("Should not call")),
        new=lambda _, _a: Unsafe.raise_exception(Exception("Should not call")),
        new_if_not_exist=lambda _, _a: Unsafe.raise_exception(Exception("Should not call")),
        create_like=lambda _, _a: Unsafe.raise_exception(Exception("Should not call")),
        move_data=lambda _, _a: Unsafe.raise_exception(Exception("Should not call")),
        move=lambda _, _a: Unsafe.raise_exception(Exception("Should not call")),
        migrate=lambda _, _a: Unsafe.raise_exception(Exception("Should not call")),
    )
    items = PureIterFactory.from_range(range(5)).map(
        lambda i: Usage(_utils.mock_range(), "group_1", i),
    )
    table_id = DbTableId(SchemaId(Identifier.new("schema_1")), TableId(Identifier.new("table_1")))
    cmd: Cmd[None] = save_usage(mock_client, table_id, items.to_list()).map(
        lambda r: r.alt(raise_exception).to_union(),
    )
    with pytest.raises(SystemExit):
        cmd.compute()
