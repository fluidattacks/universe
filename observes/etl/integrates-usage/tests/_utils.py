from dateutil.relativedelta import relativedelta
from fa_purity import Unsafe
from fa_purity.date_time import DatetimeFactory

from integrates_usage_etl._date_range import DateRange, NonZeroRelativeDelta


def mock_range() -> DateRange:
    return DateRange.new(
        DatetimeFactory.EPOCH_START,
        NonZeroRelativeDelta.new(relativedelta(days=1)).alt(Unsafe.raise_exception).to_union(),
    )
