from fa_purity import FrozenDict, Unsafe

from integrates_usage_etl._utils import build_dict_unique_keys, str_to_int


def test_build_dict_unique_keys_1() -> None:
    raw = (("foo", "val"), ("foo", "val2"))
    assert (
        build_dict_unique_keys(raw)
        .to_coproduct()
        .map(lambda _: Unsafe.raise_exception(Exception("Should fail")), lambda _: None)
        is None
    )


def test_build_dict_unique_keys_2() -> None:
    raw = (("foo", "val"), ("foo2", "val2"))
    expected = FrozenDict({"foo": "val", "foo2": "val2"})
    assert build_dict_unique_keys(raw).alt(Unsafe.raise_exception).to_union() == expected


def test_str_to_int_1() -> None:
    assert (
        str_to_int("1.1")
        .to_coproduct()
        .map(lambda _: Unsafe.raise_exception(Exception("Should fail")), lambda _: None)
        is None
    )


def test_str_to_int_2() -> None:
    expected = 456
    assert str_to_int("456").alt(Unsafe.raise_exception).to_union() == expected
