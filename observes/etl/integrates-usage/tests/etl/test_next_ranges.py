import datetime

from dateutil.relativedelta import relativedelta
from fa_purity import Maybe, Unsafe
from fa_purity.date_time import DatetimeFactory, DatetimeUTC, RawDatetime

from integrates_usage_etl._date_range import ContinuousDateRanges, DateRange, NonZeroRelativeDelta
from integrates_usage_etl.etl._next_ranges import NextRanges, next_ranges_from_state


def mock_range() -> DatetimeUTC:
    return (
        DatetimeFactory.new_utc(
            RawDatetime(
                year=2025,
                month=1,
                day=1,
                hour=23,
                minute=22,
                second=21,
                microsecond=0,
                time_zone=datetime.UTC,
            ),
        )
        .alt(Unsafe.raise_exception)
        .to_union()
    )


def test_next_ranges_from_state() -> None:
    # - Expect no recent range when state is empty
    delta = NonZeroRelativeDelta.new(relativedelta(weeks=1)).alt(Unsafe.raise_exception).to_union()
    first_historic = DateRange.new(mock_range(), -delta)
    expected = NextRanges(
        ContinuousDateRanges.new((DateRange.new(first_historic.start, -delta), first_historic))
        .alt(Unsafe.raise_exception)
        .to_union(),
        ContinuousDateRanges.new(()).alt(Unsafe.raise_exception).to_union(),
    )
    result = next_ranges_from_state(mock_range(), Maybe.empty(), 2, 1)
    assert result == expected


def test_next_ranges_from_state_2() -> None:
    # - Expect a limited recent range so that no date is > the now date
    delta = NonZeroRelativeDelta.new(relativedelta(weeks=1)).alt(Unsafe.raise_exception).to_union()
    state = DateRange.new(mock_range(), -delta)
    expected = NextRanges(
        ContinuousDateRanges.new(
            (
                DateRange.new(DateRange.new(state.start, -delta).start, -delta),
                DateRange.new(state.start, -delta),
            ),
        )
        .alt(Unsafe.raise_exception)
        .to_union(),
        ContinuousDateRanges.new(()).alt(Unsafe.raise_exception).to_union(),
    )
    result = next_ranges_from_state(mock_range(), Maybe.some(state), 2, 1)
    assert result == expected


def test_next_ranges_from_state_3() -> None:
    # - Expect a recent range since delta cannot surpass the now date
    delta = NonZeroRelativeDelta.new(relativedelta(weeks=1)).alt(Unsafe.raise_exception).to_union()
    state = DateRange.new(mock_range(), -delta)
    expected = NextRanges(
        ContinuousDateRanges.new(
            (
                DateRange.new(DateRange.new(state.start, -delta).start, -delta),
                DateRange.new(state.start, -delta),
            ),
        )
        .alt(Unsafe.raise_exception)
        .to_union(),
        ContinuousDateRanges.new((DateRange.new(state.end, delta),))
        .alt(Unsafe.raise_exception)
        .to_union(),
    )
    now = DateRange.new(
        mock_range(),
        NonZeroRelativeDelta.new(relativedelta(weeks=5)).alt(Unsafe.raise_exception).to_union(),
    )
    result = next_ranges_from_state(now.end, Maybe.some(state), 2, 1)
    assert result == expected
