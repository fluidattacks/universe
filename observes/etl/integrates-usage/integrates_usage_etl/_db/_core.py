from collections.abc import Callable
from dataclasses import dataclass

from fa_purity import Cmd, FrozenList, ResultE
from redshift_client.core.id_objs import DbTableId

from integrates_usage_etl.usage import Usage


@dataclass(frozen=True)
class DbClient:
    init_table: Callable[[DbTableId], Cmd[ResultE[None]]]
    save_usage: Callable[[DbTableId, FrozenList[Usage]], Cmd[ResultE[None]]]
