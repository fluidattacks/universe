import inspect

from etl_utils.bug import Bug
from fa_purity import Cmd, FrozenDict, ResultE
from redshift_client.core.column import Column, ColumnId
from redshift_client.core.data_type.core import DataType, PrecisionType, PrecisionTypes, StaticTypes
from redshift_client.core.id_objs import DbTableId, Identifier
from redshift_client.core.table import Table
from redshift_client.sql_client import DbPrimitiveFactory
from snowflake_client import TableClient

START_DATE_COLUMN = ColumnId(Identifier.new("start_date"))
END_DATE_COLUMN = ColumnId(Identifier.new("end_date"))
GROUP_COLUMN = ColumnId(Identifier.new("group"))
COUNT_COLUMN = ColumnId(Identifier.new("count"))


def _var_char(limit: int) -> DataType:
    return DataType(PrecisionType(PrecisionTypes.VARCHAR, limit))


def table_definition() -> Table:
    none = DbPrimitiveFactory.from_raw(None)
    columns: FrozenDict[ColumnId, Column] = FrozenDict(
        {
            START_DATE_COLUMN: Column(DataType(StaticTypes.TIMESTAMPTZ), False, none),
            END_DATE_COLUMN: Column(DataType(StaticTypes.TIMESTAMPTZ), False, none),
            GROUP_COLUMN: Column(_var_char(4096), False, none),
            COUNT_COLUMN: Column(DataType(StaticTypes.BIGINT), False, none),
        },
    )
    table = Table.new(
        (START_DATE_COLUMN, END_DATE_COLUMN, GROUP_COLUMN, COUNT_COLUMN),
        columns,
        frozenset({START_DATE_COLUMN, END_DATE_COLUMN, GROUP_COLUMN}),
    )
    return Bug.assume_success("table_definition", inspect.currentframe(), (), table)


def init_table(client: TableClient, table_id: DbTableId) -> Cmd[ResultE[None]]:
    result = client.new_if_not_exist(
        table_id,
        table_definition(),
    )
    return result.map(
        lambda r: r.alt(
            lambda e: Bug.new(
                "init_registration_table_execution",
                inspect.currentframe(),
                e,
                (),
            ),
        ),
    )
