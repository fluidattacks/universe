import inspect

from etl_utils.bug import Bug
from fa_purity import Cmd, FrozenDict, FrozenList, PureIterFactory, ResultE
from redshift_client.client import GroupedRows, TableRow
from redshift_client.core.column import ColumnId
from redshift_client.core.id_objs import DbTableId
from redshift_client.sql_client import DbPrimitive, DbPrimitiveFactory
from snowflake_client import TableClient

from integrates_usage_etl.usage import Usage

from ._table import COUNT_COLUMN, END_DATE_COLUMN, GROUP_COLUMN, START_DATE_COLUMN, table_definition


def _encode_usage(usage: Usage) -> TableRow:
    table = table_definition()
    encoded: dict[ColumnId, DbPrimitive] = {
        START_DATE_COLUMN: DbPrimitiveFactory.from_raw(usage.date_range.start.date_time),
        END_DATE_COLUMN: DbPrimitiveFactory.from_raw(usage.date_range.end.date_time),
        GROUP_COLUMN: DbPrimitiveFactory.from_raw(usage.group),
        COUNT_COLUMN: DbPrimitiveFactory.from_raw(usage.count),
    }
    return Bug.assume_success(
        "encode_usage",
        inspect.currentframe(),
        (str(usage),),
        TableRow.new(table, FrozenDict(encoded)),
    )


def save_usage(
    client: TableClient,
    table_id: DbTableId,
    usage: FrozenList[Usage],
) -> Cmd[ResultE[None]]:
    table = table_definition()
    _rows = GroupedRows.new(
        table,
        PureIterFactory.from_list(usage).map(_encode_usage).to_list(),
    )
    rows = Bug.assume_success(
        "grouped_rows",
        inspect.currentframe(),
        (str(usage),),
        _rows,
    )
    return client.named_insert(table_id, rows)
