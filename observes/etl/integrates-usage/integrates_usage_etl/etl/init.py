import inspect
import logging

from connection_manager import (
    ConnectionManagerFactory,
    DbClients,
)
from etl_utils.bug import Bug
from fa_purity import Cmd, Maybe, Result, ResultE, Unsafe, cast_exception
from fa_purity.date_time import DatetimeFactory

from integrates_usage_etl import _utils
from integrates_usage_etl._db import DbClientFactory
from integrates_usage_etl._s3 import S3URI, PureS3Client, S3Factory
from integrates_usage_etl.etl._retrieves import RETRIEVES_TABLE
from integrates_usage_etl.state import EtlState
from integrates_usage_etl.state.decode import decode_state
from integrates_usage_etl.state.encode import encode_state

from ._conf import CONNECTION_CONF, STATE_URI, EtlConf, EtlSetup

LOG = logging.getLogger(__name__)


def _get_state(state_uri: S3URI, s3_client: PureS3Client) -> Cmd[ResultE[EtlState]]:
    return s3_client.load_json(state_uri).map(
        lambda r: r.alt(
            lambda e: Bug.new(
                "Unable to get ETL state",
                inspect.currentframe(),
                e,
                (str(state_uri),),
            ),
        )
        .alt(cast_exception)
        .bind(decode_state),
    )


def setup() -> Cmd[ResultE[tuple[EtlSetup, EtlState]]]:
    state_uri = (
        S3URI.from_raw("s3://observes.state/integrates_usage/product_state.json")
        .alt(Unsafe.raise_exception)
        .to_union()
    )
    config = EtlConf(state_uri, 6, 1)
    state = S3Factory.new_s3_client().bind(lambda c: _get_state(state_uri, c))
    return _utils.std_chain_cmd_result(
        state,
        lambda s: DatetimeFactory.date_now()
        .map(lambda now: (EtlSetup(now, config), s))
        .map(Result.success),
    )


def _save_state(state_uri: S3URI, state: EtlState) -> Cmd[ResultE[None]]:
    return S3Factory.new_s3_client().bind(lambda c: c.save(state_uri, encode_state(state)))


def set_init_state() -> Cmd[ResultE[None]]:
    return _save_state(STATE_URI, EtlState(Maybe.empty()))


def _init_etl(clients: DbClients) -> Cmd[ResultE[None]]:
    return (
        clients.connection.cursor(LOG)
        .map(lambda x: DbClientFactory.new(x))
        .bind(lambda v: v.init_table(RETRIEVES_TABLE))
    )


def init_etl() -> Cmd[ResultE[None]]:
    create_table = _utils.std_chain_cmd_result(
        ConnectionManagerFactory.observes_manager(),
        lambda m: m.execute_with_snowflake(_init_etl, CONNECTION_CONF).map(
            lambda r: r.alt(
                lambda c: c.map(
                    cast_exception,
                    cast_exception,
                ),
            ),
        ),
    )
    return _utils.std_chain_cmd_result(
        set_init_state(),
        lambda _: create_table,
    )
