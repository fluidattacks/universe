import inspect
import logging
from collections.abc import Callable
from logging import Logger

from etl_utils.bug import Bug
from etl_utils.parallel import ThreadPool
from fa_purity import (
    Cmd,
    FrozenList,
    Maybe,
    PureIterFactory,
    Result,
    ResultE,
    cast_exception,
)
from redshift_client.core.id_objs import DbTableId, Identifier, SchemaId, TableId
from snowflake_client import SnowflakeCursor

from integrates_usage_etl import _utils
from integrates_usage_etl._cloudwatch import CloudwatchLogClientFactory
from integrates_usage_etl._date_range import ContinuousDateRanges, DateRange
from integrates_usage_etl._db import DbClient, DbClientFactory
from integrates_usage_etl.state import EtlState
from integrates_usage_etl.usage import RetrievesUsage
from integrates_usage_etl.usage.retrieves import get_retrieves_usage

from ._conf import EtlSetup
from ._next_ranges import NextRanges, next_ranges_from_state

LOG = logging.getLogger(__name__)
RETRIEVES_TABLE = DbTableId(
    SchemaId(Identifier.new("integrates_usage")),
    TableId(Identifier.new("retrieves")),
)


def _get_data(date_range: DateRange) -> Cmd[ResultE[FrozenList[RetrievesUsage]]]:
    return _utils.std_chain_cmd_result(
        CloudwatchLogClientFactory.new(),
        lambda cw: get_retrieves_usage(cw, date_range).map(
            lambda r: r.alt(
                lambda c: c.map(
                    lambda u: cast_exception(ValueError(u)),
                    cast_exception,
                ),
            ),
        ),
    )


def _save_usage(
    table: DbTableId,
    new_client: Cmd[ResultE[DbClient]],
    usage: FrozenList[RetrievesUsage],
) -> Cmd[ResultE[None]]:
    data = PureIterFactory.from_list(usage).map(lambda u: u.usage)
    return _utils.std_chain_cmd_result(new_client, lambda c: c.save_usage(table, data.to_list()))


def _get_and_save(
    table: DbTableId,
    new_client: Cmd[ResultE[DbClient]],
    date_range: DateRange,
) -> Cmd[ResultE[None]]:
    return _utils.std_chain_cmd_result(
        _get_data(date_range),
        lambda u: _save_usage(table, new_client, u),
    )


def _get_endpoints(ranges: ContinuousDateRanges) -> Maybe[DateRange]:
    start = _utils.get_index(ranges.ranges, 0).map(lambda dr: dr.start)
    end = _utils.get_index(ranges.ranges, -1).map(lambda dr: dr.end)
    return start.bind(
        lambda s: end.map(
            lambda e: DateRange.from_endpoints(s, e)
            .alt(
                lambda e: Bug.new(
                    "impossible",
                    inspect.currentframe(),
                    e,
                    (str(ranges),),
                ).explode(),
            )
            .to_union(),
        ),
    )


def _calc_next_state(ranges: NextRanges, state: EtlState) -> ResultE[EtlState]:
    new_ranges = state.retrieves_usage.map(
        lambda dr: ContinuousDateRanges.new(
            (*ranges.historic_range.ranges, dr, *ranges.recent_range.ranges),
        ),
    ).or_else_call(
        lambda: ContinuousDateRanges.new(ranges.historic_range.ranges + ranges.recent_range.ranges),
    )
    return new_ranges.map(lambda c: EtlState(_get_endpoints(c))).alt(
        lambda e: Bug.new(
            "_calc_next_state",
            inspect.currentframe(),
            e,
            (str(ranges), str(state)),
        ),
    )


def retrieves_etl(
    setup: EtlSetup,
    state: EtlState,
    pool: ThreadPool,
    new_cursor: Callable[[Logger], Cmd[SnowflakeCursor]],
) -> Cmd[ResultE[EtlState]]:
    ranges = next_ranges_from_state(
        setup.current_date,
        state.retrieves_usage,
        setup.conf.max_history_periods,
        setup.conf.max_update_periods,
    )
    new_client: Cmd[ResultE[DbClient]] = (
        new_cursor(LOG).map(DbClientFactory.new).map(Result.success)
    )
    historics = (
        PureIterFactory.from_list(ranges.historic_range.ranges)
        .map(lambda dr: _get_and_save(RETRIEVES_TABLE, new_client, dr))
        .transform(pool.in_threads)
    )
    recent = (
        PureIterFactory.from_list(ranges.recent_range.ranges)
        .map(lambda dr: _get_and_save(RETRIEVES_TABLE, new_client, dr))
        .transform(pool.in_threads)
    )
    historics_until_fail = _utils.consume_stream_until_fail(historics)
    recent_until_fail = _utils.consume_stream_until_fail(recent)
    result = historics_until_fail.bind(
        lambda m: m.to_result()
        .to_coproduct()
        .map(lambda e: Cmd.wrap_value(Maybe.some(e)), lambda _: recent_until_fail),
    )
    return result.map(
        lambda m: m.to_result()
        .to_coproduct()
        .map(
            lambda e: Result.failure(e),
            lambda _: _calc_next_state(ranges, state),
        ),
    )
