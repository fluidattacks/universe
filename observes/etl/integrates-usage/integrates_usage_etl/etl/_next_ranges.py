import inspect
from dataclasses import dataclass

from dateutil.relativedelta import relativedelta
from etl_utils.bug import Bug
from fa_purity import Maybe, PureIter, PureIterFactory, Unsafe
from fa_purity.date_time import DatetimeUTC

from integrates_usage_etl._date_range import ContinuousDateRanges, DateRange, NonZeroRelativeDelta


def _calc_ranges(
    start: DatetimeUTC,
    period: NonZeroRelativeDelta,
    repetitions: int,
    reverse: bool,
) -> PureIter[DateRange]:
    def _gen(previous: DatetimeUTC) -> tuple[DatetimeUTC, DateRange]:
        _next = DateRange.new(previous, period)
        _next_date = _next.end if not reverse else _next.start
        return (_next_date, _next)

    return PureIterFactory.from_range(range(repetitions)).generate(lambda p, _: _gen(p), start)


@dataclass(frozen=True)
class NextRanges:
    historic_range: ContinuousDateRanges
    recent_range: ContinuousDateRanges


def next_ranges_from_state(
    now: DatetimeUTC,
    state: Maybe[DateRange],
    max_history_periods: int,
    max_update_periods: int,
) -> NextRanges:
    period = NonZeroRelativeDelta.new(relativedelta(weeks=1)).alt(Unsafe.raise_exception).to_union()
    history_range = (
        state.map(lambda s: _calc_ranges(s.start, -period, max_history_periods, True))
        .value_or(_calc_ranges(now, -period, max_history_periods, True))
        .transform(
            lambda p: Bug.assume_success(
                "history_range",
                inspect.currentframe(),
                (str(now), str(state), str(max_history_periods), str(max_update_periods)),
                ContinuousDateRanges.new(p.to_list()[::-1]),
            ),
        )
    )
    recent_range = (
        state.map(
            lambda s: _calc_ranges(s.end, period, max_update_periods, False).filter(
                lambda dr: dr.end.date_time <= now.date_time,
            ),
        )
        .value_or(PureIterFactory.from_list([]))
        .transform(
            lambda p: Bug.assume_success(
                "recent_range",
                inspect.currentframe(),
                (str(now), str(state), str(max_history_periods), str(max_update_periods)),
                ContinuousDateRanges.new(p.to_list()),
            ),
        )
    )
    return NextRanges(history_range, recent_range)
