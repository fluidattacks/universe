import inspect

import click
from etl_utils.bug import Bug
from fa_purity import Cmd

from integrates_usage_etl.etl.init import init_etl

from .etl import execute_etls


@click.command()
def run() -> None:
    cmd: Cmd[None] = execute_etls().map(
        lambda r: Bug.assume_success("integrates_usage_etl", inspect.currentframe(), (), r),
    )
    cmd.compute()


@click.command()
def init() -> None:
    cmd: Cmd[None] = init_etl().map(
        lambda r: Bug.assume_success("init_etl", inspect.currentframe(), (), r),
    )
    cmd.compute()


@click.group()
def main() -> None:
    pass


main.add_command(run)
main.add_command(init)
