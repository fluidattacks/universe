import inspect
from datetime import datetime

from etl_utils.bug import Bug
from fa_purity import Result, ResultE, ResultTransform
from fa_purity.date_time import DatetimeUTC
from fa_purity.json import JsonObj, JsonPrimitiveUnfolder, JsonUnfolder, JsonValue, Unfolder

from integrates_usage_etl._date_range import DateRange

from ._core import EtlState


def _decode_obj(expected_type: str, raw: JsonObj) -> ResultE[JsonValue]:
    _type = JsonUnfolder.require(
        raw,
        "type",
        lambda v: Unfolder.to_primitive(v).bind(JsonPrimitiveUnfolder.to_str),
    ).bind(
        lambda t: Result.success(None)
        if t == expected_type
        else Result.failure(
            ValueError(f"Invalid object. Expected `{expected_type}` but got `{t}`"),
            None,
        ),
    )
    return _type.bind(lambda _: ResultTransform.try_get(raw, "value"))


def _decode_raw_date_time(raw: str) -> ResultE[datetime]:
    try:
        return Result.success(datetime.fromisoformat(raw))
    except Exception as e:  # noqa: BLE001
        # False positive: this is only bad if the error is not returned
        return Result.failure(e)


def _decode_date_time_utc(raw: str) -> ResultE[DatetimeUTC]:
    return _decode_raw_date_time(raw).bind(DatetimeUTC.assert_utc)


def _decode_date_time(raw: JsonValue) -> ResultE[DatetimeUTC]:
    return Unfolder.to_primitive(raw).bind(JsonPrimitiveUnfolder.to_str).bind(_decode_date_time_utc)


def _decode_date_range(raw: JsonObj) -> ResultE[DateRange]:
    def _decode(value: JsonObj) -> ResultE[DateRange]:
        start = JsonUnfolder.require(value, "start", _decode_date_time)
        end = JsonUnfolder.require(value, "end", _decode_date_time)
        return start.bind(lambda s: end.bind(lambda e: DateRange.from_endpoints(s, e)))

    return _decode_obj("DateRange", raw).bind(Unfolder.to_json).bind(_decode)


def decode_state(raw: JsonObj) -> ResultE[EtlState]:
    def _decode(value: JsonObj) -> ResultE[EtlState]:
        return JsonUnfolder.optional(
            value,
            "retrieves_usage",
            lambda v: Unfolder.to_json(v).bind(_decode_date_range),
        ).map(lambda retrieves_usage: EtlState(retrieves_usage))

    return (
        _decode_obj("EtlState", raw)
        .bind(Unfolder.to_json)
        .bind(_decode)
        .alt(
            lambda e: Bug.new(
                "Unable to decode state",
                inspect.currentframe(),
                e,
                (JsonUnfolder.dumps(raw),),
            ),
        )
    )
