from dataclasses import dataclass

from fa_purity import Maybe

from integrates_usage_etl._date_range import DateRange


@dataclass(frozen=True)
class EtlState:
    retrieves_usage: Maybe[DateRange]
