from etl_utils.smash import smash_result_2
from fa_purity import (
    Cmd,
    Coproduct,
    FrozenDict,
    FrozenList,
    PureIterFactory,
    Result,
    ResultE,
    ResultFactory,
    ResultTransform,
)
from fa_purity.json import JsonObj, JsonPrimitiveUnfolder, JsonUnfolder, Unfolder

from integrates_usage_etl._cloudwatch import CloudwatchLogClient, LogQuery, LogQueryLanguage
from integrates_usage_etl._date_range import DateRange
from integrates_usage_etl._utils import build_dict_unique_keys, str_to_int

from ._core import INTEGRATES_LOG_GROUP, RetrievesUsage, Usage
from ._utils import FinishedQueryResult, UnfinishedQueryResult, wait_completion


def _decode_field_value(raw: JsonObj) -> ResultE[tuple[str, str]]:
    field = JsonUnfolder.require(
        raw,
        "field",
        lambda v: Unfolder.to_primitive(v).bind(JsonPrimitiveUnfolder.to_str),
    )
    value = JsonUnfolder.require(
        raw,
        "value",
        lambda v: Unfolder.to_primitive(v).bind(JsonPrimitiveUnfolder.to_str),
    )
    return smash_result_2(field, value)


def _decode_fields(raw: FrozenList[JsonObj]) -> ResultE[FrozenDict[str, str]]:
    return (
        PureIterFactory.from_list(raw)
        .map(_decode_field_value)
        .transform(lambda p: ResultTransform.all_ok(p.to_list()))
        .bind(build_dict_unique_keys)
    )


def _decode_usage_from_fields(date_range: DateRange, raw: FrozenDict[str, str]) -> ResultE[Usage]:
    group = ResultTransform.try_get(raw, "group_name").value_or("")
    return (
        ResultTransform.try_get(raw, "transact_count")
        .bind(str_to_int)
        .map(lambda c: Usage(date_range, group, c))
    )


def _decode_usage(
    date_range: DateRange,
    result: FinishedQueryResult,
) -> ResultE[FrozenList[RetrievesUsage]]:
    return (
        PureIterFactory.from_list(result.result.data)
        .map(
            lambda i: _decode_fields(i).bind(
                lambda d: _decode_usage_from_fields(date_range, d).map(RetrievesUsage),
            ),
        )
        .transform(lambda p: ResultTransform.all_ok(p.to_list()))
    )


def get_retrieves_usage(
    client: CloudwatchLogClient,
    date_range: DateRange,
) -> Cmd[Result[FrozenList[RetrievesUsage], Coproduct[UnfinishedQueryResult, Exception]]]:
    query = """
        fields operation_name, user_metadata.group_name as group_name
        | filter operation_name like "Retrieves"
        | stats count(*) as transact_count by group_name
        | sort transact_count desc
    """
    factory: ResultFactory[FinishedQueryResult, Coproduct[UnfinishedQueryResult, Exception]] = (
        ResultFactory()
    )
    return (
        client.start_query(
            (INTEGRATES_LOG_GROUP,),
            LogQuery(LogQueryLanguage.CWLI, query),
            date_range.start,
            date_range.end,
        )
        .bind(
            lambda r: r.to_coproduct().map(
                lambda q: wait_completion(client, q),
                lambda e: Cmd.wrap_value(factory.failure(Coproduct.inr(e))),
            ),
        )
        .map(lambda r: r.bind(lambda f: _decode_usage(date_range, f).alt(Coproduct.inr)))
    )
