# shellcheck shell=bash

job_ce() {
  local state

  echo '[INFO] Executing setup' \
    && state="s3://observes.state/ce_etl/services.json" \
    && export_notifier_key \
    && common_aws_region \
    && echo '[INFO] Running tap' \
    && tap-ce --services --tags --state "${state}" > ce.singer \
    && echo '[INFO] Saving into the snowflake' \
    && target-warehouse only-append \
      --schema-name 'aws-costs' \
      --use-snowflake \
      < ce.singer \
    && success-indicators \
      single-job \
      --job "ce_etl" \
    && rm ce.singer
}

job_ce
