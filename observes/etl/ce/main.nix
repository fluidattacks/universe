{ inputs, makeScript, outputs, projectPath, ... }:
let
  deps = with inputs.observesIndex; [
    service.success_indicators.root
    tap.ce.root
    target.warehouse.root
  ];
  env = builtins.map inputs.getRuntime deps;
  common_vars = import (projectPath "/observes/common/common_vars.nix") {
    inherit projectPath;
  };
in makeScript {
  searchPaths = {
    bin = env;
    export = common_vars;
    source = [ outputs."/observes/common/db-creds" ];
  };
  name = "observes-etl-ce";
  entrypoint = ./entrypoint.sh;
}
