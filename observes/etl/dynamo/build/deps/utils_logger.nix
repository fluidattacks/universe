{ lib, makes_inputs, nixpkgs, python_pkgs, python_version, }:
let
  raw_src = makes_inputs.projectPath
    makes_inputs.inputs.observesIndex.common.utils_logger_2.root;
  src = import "${raw_src}/build/filter.nix" nixpkgs.nix-filter raw_src;
  bundle = import "${raw_src}/build" {
    inherit makes_inputs nixpkgs python_version src;
  };
in bundle.build_bundle (default: required_deps: builder:
  builder lib (required_deps (python_pkgs // {
    inherit (default.python_pkgs) bugsnag coralogix_logger;
  })))
