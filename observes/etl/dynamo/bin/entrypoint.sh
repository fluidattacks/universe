# shellcheck shell=bash

export AWS_DEFAULT_REGION="us-east-1"
export DYNAMO_LOCAL_PHASE_1=__argLocalPhase1__
export DYNAMO_LOCAL_DETERMINE_SCHEMA=__argLocalDynamoSchema__
export DYNAMO_REMOTE_DETERMINE_SCHEMA=__argRemoteDynamoSchema__
export DYNAMO_REMOTE_PHASE_1=__argRemotePhase1__
export DYNAMO_REMOTE_PHASE_2=__argRemotePhase2__
export DYNAMO_REMOTE_PHASE_3=__argRemotePhase3__
export DYNAMO_REMOTE_PHASE_4=__argRemotePhase4__
export DYNAMO_REMOTE_FULL_PIPELINE=__argRemoteFullPipeline__

: \
  && export_notifier_key \
  && common_aws_region \
  && dynamo-etl "${@}"
