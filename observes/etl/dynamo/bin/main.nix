{ outputs, inputs, makeScript, projectPath, ... }@makes_inputs:
let
  root = projectPath inputs.observesIndex.etl.dynamo.root;
  bundle = import "${root}/entrypoint.nix" makes_inputs;
  common_vars = import (projectPath "/observes/common/common_vars.nix") {
    inherit projectPath;
  };
  output_to_bin = output: "${output}/bin/${output.name}";
  job_to_bin = path: output_to_bin outputs."${path}";
in makeScript {
  searchPaths = {
    bin = [ bundle.env.runtime ];
    export = common_vars;
    source = [ outputs."/observes/common/db-creds" ];
  };
  replace = let
    args = {
      __argLocalPhase1__ = "/observes/etl/dynamo/jobs/parallel-phase-1";
      __argLocalDynamoSchema__ =
        "/observes/etl/dynamo/jobs/determine-schema-test-data";
      __argRemotePhase1__ = "/computeOnAwsBatch/observesDynamoPhase1";
      __argRemotePhase2__ = "/computeOnAwsBatch/observesDynamoPhase2";
      __argRemotePhase3__ = "/computeOnAwsBatch/observesDynamoPhase3";
      __argRemotePhase4__ = "/computeOnAwsBatch/observesDynamoPhase4";
      __argRemoteDynamoSchema__ = "/computeOnAwsBatch/observesDynamoSchema";
      __argRemoteFullPipeline__ =
        "/computeOnAwsBatch/observesDynamoPipelineSnowflake";
    };
  in builtins.mapAttrs (_: job_to_bin) args;

  name = "dynamo-etl";
  entrypoint = ./entrypoint.sh;
}
