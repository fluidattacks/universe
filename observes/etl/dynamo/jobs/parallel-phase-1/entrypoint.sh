# shellcheck shell=bash

function dynamodb_etl {
  local total_segments="${1}"
  local segment="${2}"

  local schemas
  local cache_bucket_folder
  export AWS_DEFAULT_REGION="us-east-1"

  if test "${segment}" == "auto"; then
    segment="${AWS_BATCH_JOB_ARRAY_INDEX}"
  fi \
    && echo "[INFO] worker at segment: ${segment}" \
    && schemas=$(mktemp -d) \
    && cache_bucket_folder="s3://observes.cache/dynamoEtl/new_vms_schema" \
    && echo '[INFO] Generating secret files' \
    && export_notifier_key \
    && echo "[INFO] Getting schemas cache at ${cache_bucket_folder}" \
    && aws_s3_sync "${cache_bucket_folder}" "${schemas}" \
    && echo '[INFO] Running ETL' \
    && tap-dynamo stream-segment \
      --table "integrates_vms" \
      --current "${segment}" \
      --total "${total_segments}" \
    | tap-json infer-and-adjust \
      --schemas "${schemas}/integrates_vms.schema.json" \
      | target-s3 \
        --bucket 'observes.etl-data' \
        --prefix "new_dynamodb/part_${segment}/" \
        --multifile-streams-conf __argMultifileConf__ \
        --str-limit 1024
}

dynamodb_etl "${@}"
