{ inputs, projectPath, makeScript, outputs, ... }@makes_inputs:
let
  deps = with inputs.observesIndex; [
    tap.dynamo.root
    tap.json.root
    target.s3.root
  ];
  env = builtins.map inputs.getRuntime deps;
in makeScript {
  searchPaths = {
    bin = env;
    source =
      [ outputs."/common/utils/aws" outputs."/observes/common/db-creds" ];
  };
  replace = { __argMultifileConf__ = ./multifile_conf.json; };
  name = "observes-etl-dynamo-parallel-phase-1";
  entrypoint = ./entrypoint.sh;
}
