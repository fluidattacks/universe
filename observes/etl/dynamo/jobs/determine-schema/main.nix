{ inputs, makeScript, outputs, projectPath, ... }@makes_inputs:
let
  deps = with inputs.observesIndex; [
    service.success_indicators.root
    tap.dynamo.root
    tap.json.root
  ];
  env = builtins.map inputs.getRuntime deps;
  common_vars = import (projectPath "/observes/common/common_vars.nix") {
    inherit projectPath;
  };
in makeScript {
  searchPaths = {
    bin = env;
    export = common_vars;
    source =
      [ outputs."/common/utils/aws" outputs."/observes/common/db-creds" ];
  };
  name = "observes-etl-dynamo-determine-schema";
  entrypoint = ./entrypoint.sh;
}
