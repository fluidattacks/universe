# shellcheck shell=bash

function determine_schemas {
  local table="${1}"
  local segments="${2}"
  local max_concurrency="${3}"
  local cache_bucket="${4}"

  local schemas
  export AWS_DEFAULT_REGION="us-east-1"

  schemas=$(mktemp -d) \
    && echo '[INFO] Generating secret files' \
    && export_notifier_key \
    && echo '[INFO] Determining data schemas from data...' \
    && tap-dynamo stream \
      --tables "${table}" \
      --segments "${segments}" \
      --max-concurrency "${max_concurrency}" \
    | tap-json only-emit-schema \
      > "${schemas}/${table}.schema.json" \
    && echo '[INFO] Saving schemas...' \
    && aws_s3_sync "${schemas}" "${cache_bucket}" \
    && echo '[INFO] Schemas saved!' \
    && success-indicators \
      single-job \
      --job "determine_dynamo_schema"
}

determine_schemas "${@}"
