{ inputs, makeScript, outputs, ... }@makes_inputs:
let
  deps = with inputs.observesIndex; [
    tap.dynamo.root
    tap.json.root
    service.success_indicators.root
  ];
  env = builtins.map inputs.getRuntime deps;
in makeScript {
  searchPaths = {
    bin = env ++ [ outputs."/integrates/db/dynamodb" ];
    source =
      [ outputs."/common/utils/aws" outputs."/observes/common/db-creds" ];
  };
  replace = { __argInferenceConf__ = ./inference_conf.json; };
  name = "observes-etl-dynamo-determine-schema-test-data";
  entrypoint = ./entrypoint.sh;
}
