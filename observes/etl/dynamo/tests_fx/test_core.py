from dynamo_etl_conf import (
    __version__,
)
from dynamo_etl_conf.executor import (
    new_local_executor,
)
from dynamo_etl_conf.executor._prepare import (
    _download_raw_schema_cache,
)
from fa_purity import (
    Cmd,
    FrozenList,
    Unsafe,
)
import pytest


def test_phase_2() -> None:
    executor = new_local_executor()
    cmd: Cmd[None] = executor.phase_2.map(
        lambda r: r.alt(Unsafe.raise_exception).to_union()
    )
    with pytest.raises(SystemExit):
        cmd.compute()


def test_phase_3() -> None:
    executor = new_local_executor()
    cmd: Cmd[None] = executor.full_phase_3.map(
        lambda r: r.alt(Unsafe.raise_exception).to_union()
    )
    with pytest.raises(SystemExit):
        cmd.compute()


def test_phase_4() -> None:
    executor = new_local_executor()
    cmd: Cmd[None] = executor.phase_4.map(
        lambda r: r.alt(Unsafe.raise_exception).to_union()
    )
    with pytest.raises(SystemExit):
        cmd.compute()


def test_download() -> None:
    def _assert(data: FrozenList[str]) -> None:
        assert data

    cmd: Cmd[None] = _download_raw_schema_cache().map(_assert)
    with pytest.raises(SystemExit):
        cmd.compute()
