from ._local import (
    local,
)
from ._remote import (
    remote,
)
import click
import logging

LOG = logging.getLogger(__name__)


@click.group()
def main() -> None:
    # cli entrypoint
    pass


main.add_command(local)
main.add_command(remote)
