import click
from dynamo_etl_conf._core import (
    Segment,
)
from dynamo_etl_conf.executor import (
    new_local_executor,
)
from fa_purity import (
    Cmd,
    Unsafe,
)
import logging
from typing import (
    NoReturn,
)

LOG = logging.getLogger(__name__)


@click.command()
@click.argument("total", type=int)
@click.argument("segment", type=str)
def phase_1(total: int, segment: str) -> NoReturn:
    seg = Segment.from_raw(segment).alt(Unsafe.raise_exception).to_union()
    executor = new_local_executor()
    cmd: Cmd[None] = executor.phase_1(total, seg).map(
        lambda r: r.alt(Unsafe.raise_exception).to_union()
    )
    cmd.compute()


@click.command()
def phase_2() -> NoReturn:
    executor = new_local_executor()
    cmd: Cmd[None] = executor.phase_2.map(
        lambda r: r.alt(Unsafe.raise_exception).to_union()
    )
    cmd.compute()


@click.command()
def phase_3() -> NoReturn:
    executor = new_local_executor()
    cmd: Cmd[None] = executor.full_phase_3.map(
        lambda r: r.alt(Unsafe.raise_exception).to_union()
    )
    cmd.compute()


@click.command()
def phase_4() -> NoReturn:
    executor = new_local_executor()
    cmd: Cmd[None] = executor.phase_4.map(
        lambda r: r.alt(Unsafe.raise_exception).to_union()
    )
    cmd.compute()


@click.command()
def determine_schema() -> NoReturn:
    executor = new_local_executor()
    cmd: Cmd[None] = executor.determine_schema.map(
        lambda r: r.alt(Unsafe.raise_exception).to_union()
    )
    cmd.compute()


@click.group()
def local() -> None:
    # `local` cli group
    pass


local.add_command(phase_1)
local.add_command(phase_2)
local.add_command(phase_3)
local.add_command(phase_4)
local.add_command(determine_schema)
