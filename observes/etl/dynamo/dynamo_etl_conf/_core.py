from __future__ import (
    annotations,
)

from collections.abc import (
    Callable,
)
from dataclasses import (
    dataclass,
    field,
)
from enum import (
    Enum,
)
from fa_purity import (
    Maybe,
    Result,
    ResultE,
)
from typing import (
    TypeVar,
)

_T = TypeVar("_T")


@dataclass(frozen=True)
class _Private:
    pass


class TargetTables(Enum):
    CORE = "integrates_vms"


@dataclass(frozen=True)
class Segment:
    _private: _Private = field(repr=False, hash=False, compare=False)
    _raw: Maybe[int]

    @staticmethod
    def from_raw(raw: str) -> ResultE[Segment]:
        try:
            item = Segment(_Private(), Maybe.some(int(raw)))
            return Result.success(item, Exception)
        except ValueError as err:
            if raw == "auto":
                item = Segment(_Private(), Maybe.empty())
                return Result.success(item, Exception)
            return Result.failure(Exception(err))

    def map(
        self,
        case_num: Callable[[int], _T],
        case_auto: Callable[[], _T],
    ) -> _T:
        return self._raw.map(case_num).or_else_call(case_auto)
