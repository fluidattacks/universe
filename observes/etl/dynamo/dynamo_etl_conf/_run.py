from etl_utils.bug import (
    Bug,
)
from fa_purity import (
    cast_exception,
    Cmd,
    FrozenList,
    Maybe,
    Result,
    ResultE,
    Unsafe,
)
import inspect
import logging
import subprocess
from typing import (
    TypeVar,
)

LOG = logging.getLogger(__name__)
_T = TypeVar("_T")


class CmdFailed(Exception):
    pass


def _to_int(item: _T) -> ResultE[int]:
    if isinstance(item, int):
        return Result.success(item)
    return Result.failure(
        TypeError(f"Expected int type got {type(item)}")
    ).alt(cast_exception)


def _action(cmd: FrozenList[str], enable_print: bool) -> ResultE[int]:
    LOG.info("Executing: %s", " ".join(cmd))
    proc = subprocess.Popen(  # pylint: disable=consider-using-with
        cmd,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        shell=False,
        universal_newlines=True,
    )
    stdout = Maybe.from_optional(proc.stdout)
    if stdout.map(lambda _: False).value_or(True):
        return Result.failure(ValueError("stdout not found!")).alt(
            cast_exception
        )
    _stdout = (
        stdout.to_result()
        .alt(
            lambda _: ValueError(
                "Impossible, maybe existence was ensured before"
            )
        )
        .alt(Unsafe.raise_exception)
        .to_union()
    )
    for line in iter(_stdout.readline, b""):
        if proc.poll() is not None:
            break
        if enable_print:
            print(line, end="")
    return _to_int(proc.returncode)  # type: ignore[misc]


def external_run(cmd: FrozenList[str]) -> Cmd[Result[None, CmdFailed]]:
    return (
        Cmd.wrap_impure(lambda: _action(cmd, True))
        .map(
            lambda r: Bug.assume_success(
                "return_code", inspect.currentframe(), tuple(), r
            )
        )
        .map(
            lambda i: Result.failure(CmdFailed(cmd), None)
            if i != 0
            else Result.success(None, CmdFailed)
        )
    )
