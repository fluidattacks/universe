from ._prepare import (
    get_cached_schemas,
)
from collections.abc import (
    Callable,
)
from dynamo_etl_conf import (
    _utils,
)
from etl_utils.bug import (
    Bug,
)
from etl_utils.parallel import (
    ThreadPool,
)
from fa_purity import (
    Cmd,
    FrozenTools,
    PureIterFactory,
    Result,
    ResultE,
    Unsafe,
)
from fa_purity.json import (
    JsonUnfolder,
    Primitive,
    Unfolder,
)
from fa_singer_io.singer import (
    SingerSchema,
)
import inspect
import logging
from logging import (
    Logger,
)
from redshift_client.core.id_objs import (
    SchemaId,
)
from redshift_client.sql_client import (
    DbPrimitiveFactory,
    QueryValues,
)
from snowflake_client import (
    SnowflakeCursor,
    SnowflakeQuery,
)
from typing import (
    Dict,
)

LOG = logging.getLogger(__name__)


def _copy_schema_data(
    cursor: SnowflakeCursor,
    schema: SchemaId,
    data_schema: SingerSchema,
) -> Cmd[ResultE[None]]:
    fields = Bug.assume_success(
        "singer_schema_properties",
        inspect.currentframe(),
        tuple(),
        JsonUnfolder.require(
            data_schema.schema.encode(), "properties", Unfolder.to_json
        ).map(lambda d: frozenset(d)),
    )
    order = tuple(sorted(fields))
    columns: Dict[str, str] = {f"column_{i}": v for i, v in enumerate(order)}
    columns_ids = ",".join(f"{{column_{i}}}" for i, _ in enumerate(order))
    statement = (
        "COPY INTO {schema}.{table} "
        f"({columns_ids}) "
        "FROM @dynamodb.integrates_stage "
        "PATTERN=%(pattern)s ENFORCE_LENGTH = FALSE FORCE = TRUE"
    )
    identifiers: Dict[str, str] = {
        "schema": schema.name.to_str(),
        "table": data_schema.stream,
    } | columns
    pattern = r"part_.*" + r"\/" + data_schema.stream + r"\..*csv"
    args: Dict[str, Primitive] = {"pattern": pattern}
    msg = Cmd.wrap_impure(
        lambda: LOG.info(
            "Copying %s into %s.%s",
            pattern,
            schema.name.to_str(),
            data_schema.stream,
        )
    )
    return msg + cursor.execute(
        SnowflakeQuery.dynamic_query(
            statement, FrozenTools.freeze(identifiers)
        )
        .alt(Unsafe.raise_exception)
        .to_union(),
        QueryValues(
            DbPrimitiveFactory.from_raw_prim_dict(FrozenTools.freeze(args))
        ),
    )


def copy_schemas_data(
    new_cursor: Callable[[Logger], Cmd[SnowflakeCursor]],
    pool: ThreadPool,
    schema_id: SchemaId,
) -> Cmd[ResultE[None]]:
    return _utils.chain_cmd_result(
        get_cached_schemas(),
        lambda data: PureIterFactory.from_list(data)
        .map(
            lambda s: new_cursor(LOG).bind(
                lambda c: _copy_schema_data(c, schema_id, s)
            )
        )
        .transform(
            lambda c: _utils.consume_result_stream(
                pool.in_threads(c), Result.success(None)
            )
        ),
    )
