from . import (
    _centralize,
    _prepare,
)
from ._phase_3 import (
    copy_schemas_data,
)
from connection_manager import (
    ClientAdapter,
    ConnectionConf,
    ConnectionManager,
    ConnectionManagerFactory,
    Databases,
    DbClients,
    Roles,
    Warehouses,
)
from dataclasses import (
    dataclass,
)
from dynamo_etl_conf._core import (
    Segment,
    TargetTables,
)
from dynamo_etl_conf.jobs_sdk import (
    BashJobSdk,
)
from etl_utils.parallel import (
    ThreadPool,
)
from fa_purity import (
    cast_exception,
    Cmd,
    Coproduct,
    Result,
    ResultE,
)
import logging
from redshift_client.core.id_objs import (
    Identifier,
    SchemaId,
)
from typing import (
    Callable,
    TypeVar,
)

LOADING_SCHEMA = SchemaId(
    Identifier.new("dynamodb_integrates_vms_merged_parts_loading")
)
TARGET_SCHEMA = SchemaId(Identifier.new("dynamodb"))
CACHE_URI = "s3://observes.cache/dynamoEtl/new_vms_schema"
LOG = logging.getLogger(__name__)
_T = TypeVar("_T")


@dataclass(frozen=True)
class _LocalExecutor:
    jobs_sdk: BashJobSdk

    def _with_connection(
        self,
        manager: ConnectionManager,
        action: Callable[
            [DbClients], Cmd[ResultE[_T]]
        ],
    ) -> Cmd[ResultE[_T]]:
        conf = ConnectionConf(
            Warehouses.GENERIC_COMPUTE,
            Roles.ETL_UPLOADER,
            Databases.OBSERVES,
        )
        return manager.execute_with_snowflake(
            action, conf
        ).map(
            lambda r: r.alt(
                lambda e: e.map(
                    lambda x: x,
                    cast_exception,
                )
            )
        )

    def _execute(
        self,
        action: Callable[
            [DbClients], Cmd[ResultE[_T]]
        ],
    ) -> Cmd[ResultE[_T]]:
        return ConnectionManagerFactory.observes_manager().bind(
            lambda manager: manager.to_coproduct().map(
                lambda m: self._with_connection(m, action),
                lambda b: Cmd.wrap_value(
                    Result.failure(b).alt(cast_exception)
                ),
            )
        )

    def phase_1(self, total: int, segment: Segment) -> Cmd[ResultE[None]]:
        "dynamo to s3 ETL"
        return self.jobs_sdk.local_phase_1(total, segment).map(
            lambda r: r.alt(cast_exception)
        )

    def phase_2(self) -> Cmd[ResultE[None]]:
        "prepare loading schema"
        msg = Cmd.wrap_impure(lambda: LOG.info("Preparing loading schema..."))
        return msg + self._execute(
            lambda db_clients: ThreadPool.new(100).bind(
                lambda pool: _prepare.prepare_procedure(
                    db_clients, pool, LOADING_SCHEMA
                )
            ),
        )

    def full_phase_3(self) -> Cmd[ResultE[None]]:
        return self._execute(
            lambda c: ThreadPool.new(100).bind(
                lambda pool: copy_schemas_data(
                    c.connection.cursor, pool, LOADING_SCHEMA
                )
            )
        )

    def phase_4(self) -> Cmd[ResultE[None]]:
        "migrate new data (at loading schema) into final schema"
        return self._execute(
            lambda s: s.connection.cursor(LOG).map(
                s.new_schema_client
            ).map(ClientAdapter.snowflake_schema_client_adapter).bind(
                lambda s: _centralize.centralize(
                    s, LOADING_SCHEMA, TARGET_SCHEMA
                )
            )
        )

    def determine_schema(self) -> Cmd[ResultE[None]]:
        return self.jobs_sdk.local_determine_schema(
            TargetTables.CORE.value,
            1000,
            100,
            CACHE_URI,
        ).map(lambda r: r.alt(cast_exception))


@dataclass(frozen=True)
class _RemoteExecutor:
    jobs_sdk: BashJobSdk

    def phase_1(self, total: int, segment: Segment) -> Cmd[ResultE[None]]:
        "Execute local phase 1 on batch"
        return self.jobs_sdk.remote_phase_1(total, segment).map(
            lambda r: r.alt(cast_exception)
        )

    def phase_2(self) -> Cmd[ResultE[None]]:
        "Execute local phase 2 on batch"
        msg = Cmd.wrap_impure(lambda: LOG.info("Preparing loading schema..."))
        return msg + self.jobs_sdk.remote_snowflake_phase_2().map(
            lambda r: r.alt(cast_exception)
        )

    def full_phase_3(self) -> Cmd[ResultE[None]]:
        "Execute local phase 3 on batch"
        return self.jobs_sdk.remote_snowflake_phase_3().map(
            lambda r: r.alt(cast_exception)
        )

    def phase_4(self) -> Cmd[ResultE[None]]:
        "Execute local phase 4 on batch"
        return self.jobs_sdk.remote_snowflake_phase_4().map(
            lambda r: r.alt(cast_exception)
        )

    def determine_schema(self) -> Cmd[ResultE[None]]:
        return self.jobs_sdk.remote_determine_schema(
            TargetTables.CORE.value,
            1000,
            100,
            CACHE_URI,
        ).map(lambda r: r.alt(cast_exception))

    def full_pipeline(self) -> Cmd[ResultE[None]]:
        "Execute full pipeline on aws batch"
        return self.jobs_sdk.snowflake_full_pipeline().map(
            lambda r: r.alt(cast_exception)
        )


@dataclass(frozen=True)
class Executor:
    phase_1: Callable[[int, Segment], Cmd[ResultE[None]]]
    phase_2: Cmd[ResultE[None]]
    full_phase_3: Cmd[ResultE[None]]
    phase_4: Cmd[ResultE[None]]
    determine_schema: Cmd[ResultE[None]]
    full_pipeline: Cmd[ResultE[None]]


def new_local_executor() -> Executor:
    executor = _LocalExecutor(BashJobSdk())
    return Executor(
        executor.phase_1,
        executor.phase_2(),
        executor.full_phase_3(),
        executor.phase_4(),
        executor.determine_schema(),
        Cmd.wrap_impure(
            lambda: LOG.warning(
                "Local executor does not support full pipeline execution!"
            )
        ).map(lambda n: Result.success(n)),
    )


def new_remote_executor() -> Executor:
    executor = _RemoteExecutor(BashJobSdk())
    return Executor(
        executor.phase_1,
        executor.phase_2(),
        executor.full_phase_3(),
        executor.phase_4(),
        executor.determine_schema(),
        executor.full_pipeline(),
    )
