import boto3
from connection_manager import (
    ClientAdapter,
    CommonSchemaClient,
    CommonTableClient,
    DbClients,
)
from dynamo_etl_conf import (
    _utils,
)
from etl_utils.parallel import (
    ThreadPool,
)
from fa_purity import (
    cast_exception,
    Cmd,
    Coproduct,
    FrozenDict,
    FrozenList,
    PureIterFactory,
    Result,
    ResultE,
    ResultTransform,
)
from fa_purity.json import (
    JsonValueFactory,
    Unfolder,
)
from fa_singer_io.singer import (
    SingerSchema,
)
from fa_singer_io.singer.deserializer import (
    deserialize,
)
import logging
from redshift_client.core.id_objs import (
    SchemaId,
)
from target_warehouse.data_schema.duplicates import (
    SingerToColumnMap,
)
from target_warehouse.loader import (
    CommonSingerHandler,
)

LOG = logging.getLogger(__name__)


def _download_raw_schema_cache() -> Cmd[FrozenList[str]]:
    def _action() -> FrozenList[str]:
        s3 = boto3.client("s3")
        obj = s3.get_object(
            Bucket="observes.cache",
            Key="dynamoEtl/new_vms_schema/integrates_vms.schema.json",
        )
        data = obj["Body"]
        return tuple(line.decode("utf-8") for line in data.iter_lines())

    return Cmd.wrap_impure(_action)


def _decode_schema(raw: str) -> ResultE[SingerSchema]:
    return (
        JsonValueFactory.loads(raw)
        .bind(Unfolder.to_json)
        .bind(deserialize)
        .bind(
            lambda s: s.map(
                lambda _: Result.failure(
                    TypeError("Expected a `SingerSchema` but got a record"),
                    SingerSchema,
                ).alt(cast_exception),
                lambda s: Result.success(s),
                lambda _: Result.failure(
                    TypeError("Expected a `SingerSchema` but got a state"),
                    SingerSchema,
                ).alt(cast_exception),
            )
        )
    )


def _decode_schemas(raw: FrozenList[str]) -> ResultE[FrozenList[SingerSchema]]:
    return (
        PureIterFactory.from_list(raw)
        .map(_decode_schema)
        .transform(lambda p: ResultTransform.all_ok(p.to_list()))
    )


def get_cached_schemas() -> Cmd[ResultE[FrozenList[SingerSchema]]]:
    return _download_raw_schema_cache().map(_decode_schemas)


def _handle_schemas_creation(
    pool: ThreadPool,
    new_client: Cmd[CommonTableClient],
    schemas: FrozenList[SingerSchema],
    schema_id: SchemaId,
) -> Cmd[ResultE[None]]:
    create = PureIterFactory.from_list(schemas).map(
        lambda s: new_client.bind(
            lambda client: CommonSingerHandler.schema_handler(
                schema_id, client, s, SingerToColumnMap(FrozenDict({}))
            ).map(lambda n: Result.success(n, Exception))
        )
    )
    return _utils.consume_result_stream(
        pool.in_threads(create), Result.success(None)
    )


def process_cached_schemas(
    pool: ThreadPool,
    schema_client: CommonSchemaClient,
    new_client: Cmd[CommonTableClient],
    schema_id: SchemaId,
) -> Cmd[ResultE[None]]:
    recreate_schema = schema_client.recreate_cascade(schema_id)
    return _utils.chain_cmd_result(
        get_cached_schemas(),
        lambda data: _utils.chain_cmd_result(
            recreate_schema,
            lambda _: _handle_schemas_creation(
                pool, new_client, data, schema_id
            ),
        ),
    )


def prepare_procedure(
    clients: DbClients,
    pool: ThreadPool,
    schema_id: SchemaId,
) -> Cmd[ResultE[None]]:
    schema_client = clients.connection.cursor(LOG).map(clients.new_schema_client).map(ClientAdapter.snowflake_schema_client_adapter)
    table_client = clients.connection.cursor(LOG).map(clients.new_table_client).map(ClientAdapter.snowflake_table_client_adapter)
    return schema_client.bind(
        lambda client: process_cached_schemas(
            pool,
            client,
            table_client,
            schema_id,
        )
    )
