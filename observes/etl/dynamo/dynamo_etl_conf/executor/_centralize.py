from connection_manager import (
    CommonSchemaClient,
)
from fa_purity import (
    Cmd,
    ResultE,
)
import logging
from redshift_client.core.id_objs import (
    SchemaId,
)

LOG = logging.getLogger(__name__)


def centralize(
    client: CommonSchemaClient,
    loading: SchemaId,
    target: SchemaId,
) -> Cmd[ResultE[None]]:
    msg = Cmd.wrap_impure(
        lambda: LOG.info(
            "Migrating %s -> %s",
            loading.name.to_str(),
            target.name.to_str(),
        )
    )
    return msg + client.migrate(loading, target)
