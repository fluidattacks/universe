from ._bins import (
    BinPaths,
)
from dataclasses import (
    dataclass,
)
from dynamo_etl_conf._core import (
    Segment,
)
from dynamo_etl_conf._run import (
    CmdFailed,
    external_run,
)
from fa_purity import (
    Cmd,
    Result,
)


def _to_str(num: int) -> str:
    return str(num)


@dataclass(frozen=True)
class BashJobSdk:
    @staticmethod
    def local_determine_schema(
        table: str,
        segments: int,
        max_concurrency: int,
        cache_bucket: str,
    ) -> Cmd[Result[None, CmdFailed]]:
        args = (
            BinPaths.DYNAMO_LOCAL_DETERMINE_SCHEMA.value,
            table,
            _to_str(segments),
            _to_str(max_concurrency),
            cache_bucket,
        )
        return external_run(args)

    @staticmethod
    def remote_determine_schema(
        table: str,
        segments: int,
        max_concurrency: int,
        cache_bucket: str,
    ) -> Cmd[Result[None, CmdFailed]]:
        args = (
            BinPaths.DYNAMO_REMOTE_DETERMINE_SCHEMA.value,
            table,
            _to_str(segments),
            _to_str(max_concurrency),
            cache_bucket,
        )
        return external_run(args)

    @staticmethod
    def local_phase_1(
        total_segments: int, segment: Segment
    ) -> Cmd[Result[None, CmdFailed]]:
        args = (
            BinPaths.DYNAMO_LOCAL_PHASE_1.value,
            _to_str(total_segments),
            segment.map(_to_str, lambda: "auto"),
        )
        return external_run(args)

    @staticmethod
    def remote_phase_1(
        total_segments: int, segment: Segment
    ) -> Cmd[Result[None, CmdFailed]]:
        args = (
            BinPaths.DYNAMO_REMOTE_PHASE_1.value,
            _to_str(total_segments),
            segment.map(_to_str, lambda: "auto"),
        )
        return external_run(args)

    @staticmethod
    def remote_snowflake_phase_2() -> Cmd[Result[None, CmdFailed]]:
        args = (BinPaths.DYNAMO_REMOTE_PHASE_2.value,)
        return external_run(args)

    @staticmethod
    def remote_snowflake_phase_3() -> Cmd[Result[None, CmdFailed]]:
        args = (BinPaths.DYNAMO_REMOTE_PHASE_3.value,)
        return external_run(args)

    @staticmethod
    def remote_snowflake_phase_4() -> Cmd[Result[None, CmdFailed]]:
        args = (BinPaths.DYNAMO_REMOTE_PHASE_4.value,)
        return external_run(args)

    @staticmethod
    def snowflake_full_pipeline() -> Cmd[Result[None, CmdFailed]]:
        args = (BinPaths.DYNAMO_REMOTE_FULL_PIPELINE.value,)
        return external_run(args)
