from enum import (
    Enum,
)
from os import (
    environ,
)


class BinPaths(Enum):
    DYNAMO_LOCAL_PHASE_1 = environ.get("DYNAMO_LOCAL_PHASE_1", "")
    DYNAMO_LOCAL_DETERMINE_SCHEMA = environ.get(
        "DYNAMO_LOCAL_DETERMINE_SCHEMA", ""
    )
    DYNAMO_REMOTE_PHASE_1 = environ.get("DYNAMO_REMOTE_PHASE_1", "")
    DYNAMO_REMOTE_PHASE_2 = environ.get("DYNAMO_REMOTE_PHASE_2", "")
    DYNAMO_REMOTE_PHASE_3 = environ.get("DYNAMO_REMOTE_PHASE_3", "")
    DYNAMO_REMOTE_PHASE_4 = environ.get("DYNAMO_REMOTE_PHASE_4", "")
    DYNAMO_REMOTE_DETERMINE_SCHEMA = environ.get(
        "DYNAMO_REMOTE_DETERMINE_SCHEMA", ""
    )
    DYNAMO_REMOTE_FULL_PIPELINE = environ.get(
        "DYNAMO_REMOTE_FULL_PIPELINE", ""
    )
