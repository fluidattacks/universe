# shellcheck shell=bash

function start_etl {

  echo '[INFO] Exporting secrets' \
    && common_aws_region \
    && sops_export_vars 'observes/secrets/prod.yaml' \
      bugsnag_api_key \
      bugsnag_notifier_key \
    && echo '[INFO] Running tap' \
    && tap-bugsnag stream \
      --api-key "${bugsnag_api_key}" \
      --all-streams \
    | tap-json infer-and-adjust \
      > bugsnag.singer \
    && echo '[INFO] Running target' \
    && target-warehouse destroy-and-upload \
      --schema-name 'bugsnag' \
      --truncate \
      --use-snowflake \
      < bugsnag.singer \
    && success-indicators \
      single-job \
      --job "bugsnag" \
    && rm bugsnag.singer
}

start_etl
