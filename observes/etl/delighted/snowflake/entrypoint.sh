# shellcheck shell=bash

function start_etl {

  echo '[INFO] Exporting secrets' \
    && common_aws_region \
    && sops_export_vars 'observes/secrets/prod.yaml' \
      delighted_api_key \
      bugsnag_notifier_key \
    && echo '[INFO] Running tap' \
    && tap-delighted stream \
      --api-key "${delighted_api_key}" \
      --all-streams \
      > delighted_data.singer \
    && target-warehouse destroy-and-upload \
      --schema-name 'delighted' \
      --truncate \
      --use-snowflake \
      < delighted_data.singer \
    && success-indicators \
      single-job \
      --job "delighted" \
    && rm delighted_data.singer
}

start_etl
