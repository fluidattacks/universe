# shellcheck shell=bash

function job_batch {

  local state
  local state_snowflake

  echo '[INFO] Executing setup' \
    && state="s3://observes.state/batch_etl/execution.json" \
    && state_snowflake="s3://observes.state/batch_etl_snowflake/execution.json" \
    && export_notifier_key \
    && common_aws_region \
    && echo '[INFO] Running tap' \
    && tap-batch --queue skims --state "${state}" > batch.singer \
    && echo '[INFO] Saving into the warehouse' \
    && target-warehouse only-append \
      --schema-name 'machine-executions' \
      --s3-state "${state}" \
      < batch.singer \
    && echo '[INFO] Running tap' \
    && tap-batch --queue skims --state "${state_snowflake}" > batch_2.singer \
    && echo '[INFO] Saving into snowflake' \
    && target-warehouse only-append \
      --schema-name 'machine-executions' \
      --s3-state "${state_snowflake}" \
      --use-snowflake \
      < batch_2.singer \
    && success-indicators \
      single-job \
      --job "batch_etl" \
    && rm batch.singer \
    && rm batch_2.singer
}

job_batch
