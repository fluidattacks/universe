# shellcheck shell=bash

function main {
  local zoho_creds

  zoho_creds=$(mktemp) \
    && echo '[INFO] Executing setup' \
    && common_aws_region \
    && sops_export_vars 'observes/secrets/prod.yaml' \
      bugsnag_notifier_zoho_crm_etl \
      ZOHO_CRM_ETL \
    && export bugsnag_notifier_key="${bugsnag_notifier_zoho_crm_etl}" \
    && echo "${ZOHO_CRM_ETL}" > "${zoho_creds}" \
    && tap-zoho-crm stream-modules "${zoho_creds}" \
    | tap-csv \
      | tap-json infer-and-adjust \
        > zoho_crm.singer \
    && target-warehouse destroy-and-upload \
      --schema-name "zoho_crm" \
      --persistent-tables "bulk_jobs" \
      --truncate \
      --use-snowflake \
      < zoho_crm.singer \
    && success-indicators \
      single-job \
      --job "zoho_crm_etl" \
    && rm zoho_crm.singer
}
main "${@}"
