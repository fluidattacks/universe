# shellcheck shell=bash

function job_flow {

  echo '[INFO] Preparing secrets' \
    && export_notifier_key \
    && common_aws_region \
    && sops_export_vars 'observes/secrets/prod.yaml' \
      FLOW_API_TOKEN \
    && echo '[INFO] Running tap' \
    && tap-flow --repository product --all > flow.singer \
    && echo '[INFO] Running target' \
    && target-warehouse destroy-and-upload \
      --schema-name "flow" \
      --truncate \
      --use-snowflake \
      < flow.singer \
    && success-indicators \
      single-job \
      --job 'flow_etl' \
    && rm flow.singer

}

job_flow
