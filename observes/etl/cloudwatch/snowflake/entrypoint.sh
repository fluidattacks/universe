# shellcheck shell=bash

function job_cloudwatch {

  echo '[INFO] Executing setup' \
    && export_notifier_key \
    && common_aws_region \
    && echo '[INFO] Running tap' \
    && tap-cloudwatch --namespace SkimsTechniqueTimes --average > cloudwatch.singer \
    && echo '[INFO] Running target' \
    && target-warehouse destroy-and-upload \
      --schema-name 'cloudwatch-skims-metrics' \
      --truncate \
      --use-snowflake \
      < cloudwatch.singer \
    && success-indicators \
      single-job \
      --job "cloudwatch_etl" \
    && rm cloudwatch.singer
}

job_cloudwatch
