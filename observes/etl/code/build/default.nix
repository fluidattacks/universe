{ makes_inputs, nixpkgs, python_version, src, }:
let
  deps = import ./deps { inherit makes_inputs nixpkgs python_version; };
  pkgDeps = {
    runtime_deps = with deps.python_pkgs; [
      click
      connection-manager
      etl-utils
      fa-purity
      GitPython
      gql
      integrates-dal
      mypy-boto3-batch
      mypy-boto3-dynamodb
      pathos
      requests
      types-requests
      utils-logger
    ];
    build_deps = with deps.python_pkgs; [ flit-core ];
    test_deps = with deps.python_pkgs; [
      arch-lint
      mypy
      pytest
      pytest-cov
      moto
      ruff
    ];
  };
  bundle = makes_inputs.makePythonPyprojectPackage {
    inherit (deps.lib) buildEnv buildPythonPackage;
    inherit pkgDeps src;
  };
in import (makes_inputs.projectPath "/observes/common/bundle_patch.nix") {
  inherit (deps.lib) buildEnv;
  inherit bundle;
  with_ruff = true;
}
