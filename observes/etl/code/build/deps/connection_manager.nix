{ lib, makes_inputs, nixpkgs, python_pkgs, python_version, }:
let
  raw_src = makes_inputs.projectPath
    makes_inputs.inputs.observesIndex.common.connection_manager.root;
  src = import "${raw_src}/build/filter.nix" nixpkgs.nix-filter raw_src;
  bundle = import "${raw_src}/build" {
    inherit makes_inputs nixpkgs python_version src;
  };
  utils =
    import (makes_inputs.projectPath "/observes/common/override_utils.nix") {
      inherit (makes_inputs) pythonOverrideUtils;
    };
  pkg = utils.deep_pkg_override python_pkgs.boto3-stubs bundle.pkg;
in pkg
