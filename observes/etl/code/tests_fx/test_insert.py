import logging
from os import (
    environ,
)

from fa_purity import (
    Cmd,
    FrozenList,
    Maybe,
    PureIterFactory,
    Unsafe,
)
from redshift_client.core.id_objs import (
    DbTableId,
    Identifier,
    SchemaId,
    TableId,
)
from snowflake_client import (
    ConnectionFactory,
    SnowflakeConnection,
    SnowflakeCredentials,
    SnowflakeDatabase,
    SnowflakeQuery,
    SnowflakeWarehouse,
)

from code_etl.database.clients._stamps.queries.insert_stamps import (
    insert_stamps,
)
from code_etl.objs import (
    Commit,
    CommitData,
    CommitDataId,
    CommitId,
    CommitStamp,
    DbCommitStamp,
    Deltas,
    GroupId,
    RepoId,
    RepoName,
    User,
)
from code_etl.str_utils import (
    TruncatedStr,
)

from . import (
    _utils,
)

LOG = logging.getLogger(__name__)
TEST_TABLE = DbTableId(
    SchemaId(Identifier.new("public")),
    TableId(Identifier.new("commits")),
)
MOCK_DATE_OLD = _utils.assert_new_utc(
    year=2020,
    month=5,
    day=15,
    hour=10,
    minute=20,
    second=30,
    microsecond=0,
)


def _mock_stamp(repo_id: RepoId, commit_id: CommitId) -> CommitStamp:
    return CommitStamp.normal_stamp(
        Commit(
            CommitDataId(repo_id, commit_id),
            CommitData(
                User("Billy", "billy@grim.com"),
                MOCK_DATE_OLD,
                User("Mandy", "mandy@grim.com"),
                MOCK_DATE_OLD,
                TruncatedStr.truncate("something", 4096),
                TruncatedStr.truncate("some", 256),
                Deltas(1, 1, 1, 1),
                Maybe.empty(),
            ),
        ),
        MOCK_DATE_OLD,
        MOCK_DATE_OLD,
    )


def _mock_db_stamp(repo: str, id_hash: str) -> DbCommitStamp:
    return DbCommitStamp(
        _mock_stamp(RepoId(GroupId("test"), RepoName(repo)), CommitId(id_hash)),
        False,
    )


def mock_heavy_stamps() -> FrozenList[DbCommitStamp]:
    return (
        PureIterFactory.from_range(range(2000))
        .map(
            lambda i: _mock_db_stamp("endsville_" + str(i), "1" * 40),
        )
        .to_list()
    )


def mock_stamps() -> FrozenList[DbCommitStamp]:
    return (
        _mock_db_stamp("endsville", "6" * 40),
        _mock_db_stamp("endsville_2", "9" * 40),
        _mock_db_stamp("endsville_3", "7" * 40),
    )


def _test_insertion(connection: SnowflakeConnection) -> Cmd[None]:
    return connection.cursor(LOG).bind(
        lambda c: c.execute(
            SnowflakeQuery.new_query("USE ROLE " + environ["SNOWFLAKE_ROLE"]),
            None,
        ).map(lambda r: r.alt(Unsafe.raise_exception).to_union())
        + insert_stamps(c, TEST_TABLE, mock_heavy_stamps()),
    )


def test_insertion() -> None:
    new_connection = ConnectionFactory.snowflake_connection(
        SnowflakeDatabase("observes"),
        SnowflakeWarehouse("GENERIC_COMPUTE"),
        SnowflakeCredentials(
            environ["SNOWFLAKE_USER"],
            environ["SNOWFLAKE_PRIVATE_KEY"],
            environ["SNOWFLAKE_ACCOUNT"],
        ),
    )
    action = SnowflakeConnection.connect_and_execute(new_connection, _test_insertion)
    Unsafe.compute(action)


test_insertion()
