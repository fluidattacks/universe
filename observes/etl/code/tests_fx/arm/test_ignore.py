import pytest
from etl_utils.parallel import (
    parallel_cmds,
)
from etl_utils.typing import (
    FrozenSet,
)
from fa_purity import (
    FrozenList,
    Unsafe,
)

from code_etl.integrates_api import (
    IgnoredPath,
    IntegratesClientFactory,
)
from code_etl.objs import (
    RepoId,
    RepoName,
)

from ._common import (
    get_group,
    get_token,
)


def test_single() -> None:
    client = IntegratesClientFactory.new_client(get_token())
    items = client.bind(
        lambda c: c.get_ignored_paths(RepoId(get_group(), RepoName("nickname"))),
    ).map(lambda r: r.alt(Exception).alt(Unsafe.raise_exception).to_union())

    def _test(items: FrozenSet[IgnoredPath]) -> None:
        assert items

    with pytest.raises(SystemExit):
        items.map(_test).compute()


def test_stress() -> None:
    client = IntegratesClientFactory.new_client(get_token())
    items = client.map(
        lambda c: tuple(
            c.get_ignored_paths(RepoId(get_group(), RepoName("nickname"))).map(
                lambda r: r.alt(Exception).alt(Unsafe.raise_exception).to_union(),
            )
            for _ in range(100)
        ),
    ).bind(lambda x: parallel_cmds(x, 50))

    def _test(items: FrozenList[FrozenSet[IgnoredPath]]) -> None:
        assert items
        for i in items:
            assert i

    with pytest.raises(SystemExit):
        items.map(_test).compute()
