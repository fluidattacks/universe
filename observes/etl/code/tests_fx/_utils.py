from datetime import (
    UTC,
)

from etl_utils.typing import (
    NoReturn,
)
from fa_purity import (
    Unsafe,
)
from fa_purity.date_time import (
    DatetimeFactory,
    DatetimeUTC,
    RawDatetime,
)


def assert_new_utc(
    year: int,
    month: int,
    day: int,
    hour: int,
    minute: int,
    second: int,
    microsecond: int,
) -> DatetimeUTC | NoReturn:
    return (
        DatetimeFactory.new_utc(
            RawDatetime(
                year=year,
                month=month,
                day=day,
                hour=hour,
                minute=minute,
                second=second,
                microsecond=microsecond,
                time_zone=UTC,
            ),
        )
        .alt(Unsafe.raise_exception)
        .to_union()
    )
