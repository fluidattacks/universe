from collections.abc import (
    Callable,
)

import pytest
from fa_purity import (
    Cmd,
    FrozenDict,
    Maybe,
    PureIter,
    PureIterFactory,
    PureIterTransform,
)

import tests.mock.checkpoint as mock_checkpoint
import tests.mock.client as mock_client
import tests.mock.integrates as mock_integrates
import tests.mock.recorder as mock_recorder
from code_etl.objs import (
    Checkpoint,
    Commit,
    CommitDataId,
    CommitId,
    CommitStamp,
    DbCommitStamp,
    GroupId,
    RepoContext,
    RepoId,
    RepoName,
    RepoRegistration,
)
from code_etl.upload_repo._extractor import (
    Extractor,
    new_extractor,
)
from code_etl.upload_repo._uploader import (
    Uploader,
)
from tests import (
    _utils,
)
from tests.mock.db import (
    MockDB,
)
from tests.mock.repo import (
    mock_repo,
)

MOCK_DATE_NOW = _utils.assert_new_utc(
    year=2024,
    month=5,
    day=15,
    hour=10,
    minute=20,
    second=30,
    microsecond=0,
)
REPO_ID = RepoId(GroupId("test_group"), RepoName("test_repo"))


def test_repo() -> None:
    assert mock_repo()


def _extractor(db: MockDB) -> Extractor:
    context = RepoContext(REPO_ID, "1" * 40, True)
    return new_extractor(
        context,
        Maybe.empty(),
        MOCK_DATE_NOW,
        mock_client.mock_client_stamps(db),
        mock_repo(),
    )


def _assert_gaps_inserted(items: PureIter[DbCommitStamp]) -> None:
    _expected = (
        (
            CommitId(hash="db98922d63b6fe5c3e443ed29fe85804f6a634f6"),
            _utils.assert_new_utc(2018, 5, 15, 10, 20, 30, 0),
            True,
        ),
        (
            CommitId(hash="00240fa1383895859bfade3d24efb9afb1eecf8b"),  # already in db
            _utils.assert_new_utc(2018, 5, 15, 10, 20, 30, 0),
            False,
        ),
        (
            CommitId(hash="6745988f8e924f7e181dbe8fa655f86dd32d6f27"),  # already in db
            _utils.assert_new_utc(2020, 5, 15, 10, 20, 30, 0),
            False,
        ),
        (
            CommitId(hash="159c3cfa98291d3e9f1e38096760994d481b2772"),  # already in db
            _utils.assert_new_utc(2019, 5, 15, 10, 20, 30, 0),
            False,
        ),
        (
            CommitId(hash="0e17f6e9966b0cf0804c6892232c35d86c02be30"),
            _utils.assert_new_utc(2018, 5, 15, 10, 20, 30, 0),
            True,
        ),
        (
            CommitId(hash="07b8f7d2ff61e3abf3714cae56c06cd1460ef582"),  # already in db
            _utils.assert_new_utc(2021, 5, 15, 10, 20, 30, 0),
            False,
        ),
        (
            CommitId("ce34f18fcbacd496cc536a3304baa020d7f65975"),
            _utils.assert_new_utc(2020, 5, 15, 10, 20, 30, 0),
            True,
        ),
        (
            CommitId(hash="285eb29c2403e415d51fc5ec4a59b91b4df26fda"),
            _utils.assert_new_utc(2020, 4, 15, 10, 20, 30, 0),
            True,
        ),
        (
            CommitId(hash="9237bd6e4bc20ad40189a6ff7be217ec787bbda8"),  # already in db
            _utils.assert_new_utc(2020, 4, 15, 10, 20, 30, 0),
            False,
        ),
        (
            CommitId("0d38170b0322135ca88caf0473ba42f8b0784cc0"),
            MOCK_DATE_NOW,
            False,
        ),
        (
            CommitId("bea34d1e06036684d9b07061571a861da48d0aa5"),
            MOCK_DATE_NOW,
            False,
        ),
        (
            CommitId("9f1d958f408ad38235109cccc6761fe4e8696265"),
            MOCK_DATE_NOW,
            False,
        ),
    )

    expected = frozenset(PureIterFactory.from_list(_expected))

    emmited = frozenset(
        items.map(
            lambda s: (
                s.stamp.commit.commit_id.hash,
                s.stamp.inserted_at,
                s.was_in_a_gap,
            ),
        ),
    )

    assert emmited == expected


def _check_stamps_uploader_2(
    db: MockDB,
    assert_stamps: Callable[[PureIter[DbCommitStamp]], None],
) -> Cmd[None]:
    return (
        db.stamps.stream()
        .transform(lambda s: s.to_list())
        .map(lambda s: assert_stamps(PureIterFactory.from_list(s).map(lambda t: t[1])))
    )


def _assert_checkpoint(item: FrozenDict[RepoId, Checkpoint]) -> None:
    expected = {
        REPO_ID: Checkpoint(
            CommitDataId(REPO_ID, CommitId("9f1d958f408ad38235109cccc6761fe4e8696265")),
            False,
            MOCK_DATE_NOW,
        ),
    }
    assert item == expected


def _test_uploader_2(db: MockDB) -> Cmd[None]:
    uploader = Uploader.new(
        mock_client.mock_client_stamps(db),
        mock_recorder.mock_client(db),
        mock_integrates.mock_client(),
        mock_checkpoint.mock_client(db),
        Maybe.empty(),
        MOCK_DATE_NOW,
        MOCK_DATE_NOW,
        REPO_ID,
        mock_repo(),
    )
    return uploader.bind(
        lambda u: u.full_upload.bind(lambda _: _check_stamps_uploader_2(db, _assert_gaps_inserted)),
    ).bind(
        lambda _: db.checkpoints.freeze().bind(
            lambda d: Cmd.wrap_impure(lambda: _assert_checkpoint(d)),
        ),
    )


def _init_db(extractor: Extractor) -> Cmd[MockDB]:
    def _filter(commit: Commit) -> Maybe[CommitStamp]:
        inserted_at_map = {
            CommitId("07b8f7d2ff61e3abf3714cae56c06cd1460ef582"): _utils.assert_new_utc(
                2021,
                5,
                15,
                10,
                20,
                30,
                0,
            ),
            CommitId("6745988f8e924f7e181dbe8fa655f86dd32d6f27"): _utils.assert_new_utc(
                2020,
                5,
                15,
                10,
                20,
                30,
                0,
            ),
            CommitId("9237bd6e4bc20ad40189a6ff7be217ec787bbda8"): _utils.assert_new_utc(
                2020,
                4,
                15,
                10,
                20,
                30,
                0,
            ),
            CommitId("159c3cfa98291d3e9f1e38096760994d481b2772"): _utils.assert_new_utc(
                2019,
                5,
                15,
                10,
                20,
                30,
                0,
            ),
            CommitId("00240fa1383895859bfade3d24efb9afb1eecf8b"): _utils.assert_new_utc(
                2018,
                5,
                15,
                10,
                20,
                30,
                0,
            ),
        }
        if commit.commit_id.hash in inserted_at_map:
            return Maybe.some(
                CommitStamp.normal_stamp(
                    commit,
                    inserted_at_map[commit.commit_id.hash],
                    inserted_at_map[commit.commit_id.hash],
                ),
            )
        return Maybe.empty()

    _db_stamps = (
        extractor.from_init.map(_filter)
        .transform(lambda s: PureIterTransform.filter_maybe(s))
        .map(lambda s: DbCommitStamp(s, False))
    )
    _db_registrations = FrozenDict(
        {REPO_ID: RepoRegistration(REPO_ID, _utils.assert_new_utc(2000, 5, 15, 10, 20, 30, 0))},
    )
    return MockDB.initialize(
        _db_registrations,
        _db_stamps.map(lambda s: (s.stamp.commit.commit_id, s)).transform(
            lambda i: FrozenDict(dict(i)),
        ),
        FrozenDict({}),
        FrozenDict({}),
        FrozenDict({}),
    )


def test_uploader_2() -> None:
    cmd = MockDB.empty().map(_extractor).bind(_init_db).bind(lambda db: _test_uploader_2(db))
    with pytest.raises(SystemExit):
        cmd.compute()
