from collections.abc import (
    Callable,
)

import pytest
from etl_utils.typing import (
    FrozenSet,
)
from fa_purity import (
    Cmd,
    FrozenDict,
    Maybe,
    PureIter,
    PureIterFactory,
    PureIterTransform,
)

import tests.mock.checkpoint as mock_checkpoint
import tests.mock.client as mock_client
import tests.mock.integrates as mock_integrates
import tests.mock.recorder as mock_recorder
from code_etl.objs import (
    Commit,
    CommitDataId,
    CommitId,
    CommitStamp,
    DbCommitStamp,
    RepoContext,
)
from code_etl.upload_repo._extractor import (
    Extractor,
    new_extractor,
)
from code_etl.upload_repo._uploader import (
    Uploader,
)
from tests.mock import (
    MOCK_DATE_NOW,
)
from tests.mock.db import (
    MockDB,
)
from tests.mock.repo import (
    REPO_ID,
    mock_repo,
)


def _extractor(db: MockDB) -> Extractor:
    context = RepoContext(REPO_ID, "1" * 40, True)
    return new_extractor(
        context,
        Maybe.empty(),
        MOCK_DATE_NOW,
        mock_client.mock_client_stamps(db),
        mock_repo(),
    )


def _init_db(extractor: Extractor) -> Cmd[MockDB]:
    def _filter(commit: Commit) -> Maybe[CommitStamp]:
        exclude = {
            "9f1d958f408ad38235109cccc6761fe4e8696265",
            "0d38170b0322135ca88caf0473ba42f8b0784cc0",
            # gap
            "9237bd6e4bc20ad40189a6ff7be217ec787bbda8",
            "285eb29c2403e415d51fc5ec4a59b91b4df26fda",
            # gap
            "db98922d63b6fe5c3e443ed29fe85804f6a634f6",
        }
        exclusions = PureIterFactory.from_list(tuple(exclude)).map(CommitId).transform(frozenset)
        if commit.commit_id.hash in exclusions:
            return Maybe.empty()
        return Maybe.some(CommitStamp.normal_stamp(commit, MOCK_DATE_NOW, MOCK_DATE_NOW))

    _db_stamps = (
        extractor.from_init.map(_filter)
        .transform(lambda s: PureIterTransform.filter_maybe(s))
        .map(lambda s: DbCommitStamp(s, False))
    )
    return MockDB.initialize(
        FrozenDict({}),
        _db_stamps.map(lambda s: (s.stamp.commit.commit_id, s)).transform(
            lambda i: FrozenDict(dict(i)),
        ),
        FrozenDict({}),
        FrozenDict({}),
        FrozenDict({}),
    )


def _assert_over_hashes(
    items: PureIter[DbCommitStamp],
    hashes: FrozenSet[str],
    assert_check: Callable[[DbCommitStamp], None],
) -> None:
    expected = (
        PureIterFactory.from_list(tuple(hashes))
        .map(lambda h: CommitDataId(REPO_ID, CommitId(h)))
        .transform(frozenset)
    )
    items.filter(lambda s: s.stamp.commit.commit_id in expected).map(assert_check).to_list()


def _assert_present_hash(commit_hash: CommitDataId, items: FrozenSet[CommitDataId]) -> None:
    assert commit_hash in items


def _assert_present_hashes(
    items: PureIter[DbCommitStamp],
    hashes: FrozenSet[str],
) -> None:
    expected = PureIterFactory.from_list(tuple(hashes)).map(
        lambda h: CommitDataId(REPO_ID, CommitId(h)),
    )
    stamps = items.map(lambda s: s.stamp.commit.commit_id).transform(frozenset)
    expected.map(lambda h: _assert_present_hash(h, stamps)).to_list()


def _assert_was_in_a_gap(items: PureIter[DbCommitStamp], hashes: FrozenSet[str]) -> None:
    def _assert_true_flag(item: DbCommitStamp) -> None:
        assert item.was_in_a_gap

    _assert_over_hashes(items, hashes, _assert_true_flag)


def _assert_not_was_in_a_gap(items: PureIter[DbCommitStamp], hashes: FrozenSet[str]) -> None:
    def _assert_false_flag(item: DbCommitStamp) -> None:
        assert not item.was_in_a_gap

    _assert_over_hashes(items, hashes, _assert_false_flag)


def _assert_1(items: PureIter[DbCommitStamp]) -> None:
    in_gap = {
        # gap
        "db98922d63b6fe5c3e443ed29fe85804f6a634f6",
    }
    _assert_present_hashes(items, frozenset(in_gap))
    _assert_was_in_a_gap(items, frozenset(in_gap))


def _assert_2(items: PureIter[DbCommitStamp]) -> None:
    in_gap = {
        # gap
        "9237bd6e4bc20ad40189a6ff7be217ec787bbda8",
        "285eb29c2403e415d51fc5ec4a59b91b4df26fda",
        # gap
        "db98922d63b6fe5c3e443ed29fe85804f6a634f6",
    }
    _assert_present_hashes(items, frozenset(in_gap))
    _assert_was_in_a_gap(items, frozenset(in_gap))


def _assert_3(items: PureIter[DbCommitStamp]) -> None:
    not_in_gap = {
        "9f1d958f408ad38235109cccc6761fe4e8696265",
        "0d38170b0322135ca88caf0473ba42f8b0784cc0",
    }
    _assert_present_hashes(items, frozenset(not_in_gap))
    _assert_not_was_in_a_gap(items, frozenset(not_in_gap))


def _check_stamps(
    db: MockDB,
    assert_stamps: Callable[[PureIter[DbCommitStamp]], None],
) -> Cmd[None]:
    return (
        db.stamps.stream()
        .transform(lambda s: s.to_list())
        .map(lambda s: assert_stamps(PureIterFactory.from_list(s).map(lambda t: t[1])))
    )


def _upload_data(db: MockDB) -> Cmd[None]:
    uploader = Uploader.new(
        mock_client.mock_client_stamps(db),
        mock_recorder.mock_client(db),
        mock_integrates.mock_client(),
        mock_checkpoint.mock_client(db),
        Maybe.empty(),
        MOCK_DATE_NOW,
        MOCK_DATE_NOW,
        REPO_ID,
        mock_repo(),
    )
    return uploader.bind(lambda u: u.full_upload.map(lambda _: None))


def test_was_in_a_gap() -> None:
    cmd: Cmd[None] = (
        MockDB.empty()
        .map(_extractor)
        .bind(_init_db)
        .bind(
            lambda db: _upload_data(db)
            + _check_stamps(db, _assert_1)
            + _upload_data(db)
            + _check_stamps(db, _assert_2)
            + _upload_data(db)
            + _check_stamps(db, _assert_3),
        )
    )
    with pytest.raises(SystemExit):
        cmd.compute()
