from fa_purity import (
    Cmd,
    PureIterFactory,
    PureIterTransform,
    Unsafe,
)

from code_etl.database import (
    All,
    RegistrationClient,
)

from .db import (
    MockDB,
)


def mock_client(db: MockDB) -> RegistrationClient:
    return RegistrationClient(
        Cmd.wrap_impure(
            lambda: Unsafe.raise_exception(NotImplementedError("mock registration init_table")),
        ),
        lambda _: Cmd.wrap_impure(
            lambda: Unsafe.raise_exception(NotImplementedError("mock registration count")),
        ),
        lambda i: PureIterFactory.from_list(i)
        .map(lambda r: db.registrations.set(r.repo, r))
        .transform(PureIterTransform.consume),
        lambda g: Cmd.wrap_value(
            db.registrations.stream()
            .filter(lambda t: t[0].group == g if g != All() else True)
            .map(lambda t: t[1]),
        ),
        lambda r: db.registrations.get(r),
        lambda r: db.registrations.get(r).map(lambda m: m.map(lambda _: True).value_or(False)),
    )
