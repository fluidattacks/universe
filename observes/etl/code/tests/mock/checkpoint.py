from fa_purity import (
    Cmd,
    Unsafe,
)

from code_etl.database import (
    DbCheckpointClient,
)

from .db import (
    MockDB,
)


def mock_client(db: MockDB) -> DbCheckpointClient:
    return DbCheckpointClient(
        init_table=Cmd.wrap_impure(
            lambda: Unsafe.raise_exception(NotImplementedError("mock checkpoint init_table")),
        ),
        set_checkpoint=lambda c: db.checkpoints.set(c.commit_id.repo, c),
        get_checkpoint=lambda r: db.checkpoints.get(r),
        set_start_point_search_progress=lambda c: db.start_progress.set(c.commit_id.repo, c),
        get_start_point_search_progress=lambda r: db.start_progress.get(r),
        delete_start_point_search_progress=lambda r: db.start_progress.delete_if_exist(r),
        set_end_point_search_progress=lambda c: db.end_progress.set(c.commit_id.repo, c),
        get_end_point_search_progress=lambda r: db.end_progress.get(r),
        delete_end_point_search_progress=lambda r: db.end_progress.delete_if_exist(r),
    )
