from datetime import (
    datetime,
)

from fa_purity import (
    Coproduct,
    FrozenList,
    Maybe,
    PureIterFactory,
    Unsafe,
)
from fa_purity.json import (
    Primitive,
)
from redshift_client.core.id_objs import (
    DbTableId,
    Identifier,
    SchemaId,
    TableId,
)
from redshift_client.sql_client import (
    DbPrimitiveFactory,
    RowData,
)

from code_etl.database.clients._stamps.queries.exist import (
    _query as exist_query,
)
from code_etl.database.clients._stamps.queries.get import (
    _decoder,
)
from code_etl.database.clients._stamps.queries.get import (
    _query as get_query,
)
from code_etl.database.clients._stamps.queries.init_table import (
    table_definition,
)
from code_etl.database.clients._stamps.queries.insert_stamps import (
    _encode_stamp,
)
from code_etl.database.clients._stamps.queries.insert_stamps import (
    _snowflake_query as insert_query,
)
from code_etl.objs import (
    Commit,
    CommitData,
    CommitDataId,
    CommitId,
    CommitStamp,
    DbCommitStamp,
    Deltas,
    GroupId,
    RepoId,
    RepoName,
    User,
)
from code_etl.str_utils import (
    TruncatedStr,
)
from tests import (
    _utils,
)

mock_stamp = CommitStamp.initial_stamp(
    Commit(
        CommitDataId(
            RepoId(GroupId("test_group"), RepoName("the_repo")),
            CommitId("1" * 40),
        ),
        CommitData(
            User("the_author", "the_author@foo.com"),
            _utils.assert_new_utc(2000, 1, 2, 3, 4, 5, 0),
            User("the_committer", "the_committer@foo.com"),
            _utils.assert_new_utc(2001, 1, 2, 3, 4, 5, 0),
            TruncatedStr.truncate("a message", 4096),
            TruncatedStr.truncate("a summary", 256),
            Deltas(4, 3, 2, 1),
            Maybe.empty(),
        ),
    ),
    _utils.assert_new_utc(2000, 1, 2, 3, 4, 5, 0),
)
mock_db_stamp = DbCommitStamp(mock_stamp, False)


def test_table() -> None:
    assert table_definition()


def test_encode_stamp() -> None:
    table = table_definition()
    columns = (
        PureIterFactory.from_list(tuple(table.columns.keys()))
        .map(lambda c: c.name.to_str())
        .transform(lambda p: frozenset(p))
    )
    encoded_keys = _encode_stamp(mock_db_stamp, "").keys()
    # All table columns present?
    for k in columns:
        assert k in encoded_keys
    # All encoded columns belongs to the table columns?
    for k in encoded_keys:
        assert k in columns


def test_insert_query() -> None:
    _table = DbTableId(SchemaId(Identifier.new("test")), TableId(Identifier.new("test_table")))
    _query, _values = insert_query(_table, (mock_db_stamp,))
    _utils.check_query_replacements(_query, _values)


def test_get_decode() -> None:
    _raw: FrozenList[Primitive | datetime] = (
        mock_stamp.commit.data.author.email,
        mock_stamp.commit.data.author.name,
        mock_stamp.commit.data.authored_at.date_time,
        mock_stamp.commit.data.committer.email,
        mock_stamp.commit.data.committer.name,
        mock_stamp.commit.data.committed_at.date_time,
        mock_stamp.commit.commit_id.hash.hash,
        mock_stamp.commit.data.message.msg,
        mock_stamp.commit.data.summary.msg,
        mock_stamp.commit.data.deltas.total_insertions,
        mock_stamp.commit.data.deltas.total_deletions,
        mock_stamp.commit.data.deltas.total_lines,
        mock_stamp.commit.data.deltas.total_files,
        mock_stamp.commit.commit_id.repo.group.name,
        mock_stamp.commit.commit_id.repo.repository.name,
        mock_stamp.seen_at.date_time,
        mock_stamp.inserted_at.date_time,
    )
    raw = RowData(PureIterFactory.from_list(_raw).map(DbPrimitiveFactory.from_raw).to_list())
    assert _decoder().decode_stamp(raw).alt(Unsafe.raise_exception).to_union() == mock_stamp


def test_get_query() -> None:
    _table = DbTableId(SchemaId(Identifier.new("test")), TableId(Identifier.new("test_table")))
    _group = GroupId("test")
    _utils.check_query_replacements(*get_query(_table, Coproduct.inl(_group)))


def test_exist_query() -> None:
    _table = DbTableId(SchemaId(Identifier.new("test")), TableId(Identifier.new("test_table")))
    _utils.check_query_replacements(*exist_query(_table, mock_stamp.commit.commit_id))
