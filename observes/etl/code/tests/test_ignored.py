from code_etl.upload_repo._ignored import (
    _ignored_path,
)


def test_ignored() -> None:
    ignored = frozenset(
        [
            "**/pipeline_scripts/**",
        ],
    )
    assert _ignored_path("fsdf/pipeline_scripts/lol.txt", ignored)
    assert _ignored_path("foo/foo2/pipeline_scripts/lol.txt", ignored)
    assert _ignored_path("/pipeline_scripts/lol.txt", ignored)
    assert _ignored_path("pipeline_scripts/lol.txt", ignored)


def test_ignored_2() -> None:
    ignored = frozenset(
        [
            "pipeline_scripts/**",
        ],
    )
    assert _ignored_path("/pipeline_scripts/lol.txt", ignored)
    assert _ignored_path("pipeline_scripts/lol.txt", ignored)


def test_ignored_3() -> None:
    ignored = frozenset(
        [
            "foo/foo2",
        ],
    )
    assert _ignored_path("foo/foo2/foo3", ignored)
    assert not _ignored_path("a/foo/foo2/foo3", ignored)


def test_ignored_4() -> None:
    ignored = frozenset(
        [
            "",
        ],
    )
    assert not _ignored_path("foo/foo2/foo3", ignored)
    assert not _ignored_path("a/foo/foo2/foo3", ignored)


def test_ignored_5() -> None:
    ignored = frozenset(["/", "*", "/*"])
    assert not _ignored_path("lol.txt", ignored)
    assert not _ignored_path("foo/foo2/foo3", ignored)
    assert not _ignored_path("a/foo/foo2/foo3", ignored)
    assert not _ignored_path("pipeline_scripts/lol.txt", ignored)
