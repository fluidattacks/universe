# shellcheck shell=bash

function job_code_amend_on_aws {

  sops_export_vars 'observes/secrets/prod.yaml' \
    'bugsnag_notifier_code_etl' \
    && sops_export_vars 'common/secrets/dev.yaml' \
      INTEGRATES_API_TOKEN \
    && export bugsnag_notifier_key="${bugsnag_notifier_code_etl}" \
    && code-amend-groups-on-aws
}

job_code_amend_on_aws
