{ inputs, makeScript, makeSearchPaths, projectPath, outputs, ... }@makes_inputs:
let
  searchPaths =
    import (projectPath "/observes/etl/code/bin/search_paths.nix") makes_inputs;
  env_vars = makeSearchPaths {
    append = false;
    inherit (searchPaths) export;
  };
in makeScript {
  searchPaths = {
    inherit (searchPaths) bin;
    source = [ env_vars outputs."/common/utils/sops" ];
  };
  name = "observes-etl-code-upload-group-on-aws-snowflake";
  entrypoint = ./entrypoint.sh;
}
