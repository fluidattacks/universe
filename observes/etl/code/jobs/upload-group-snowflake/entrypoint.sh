# shellcheck shell=bash

function job_code_upload_snowflake {
  local group="${1}"
  local seen_at="${2:- $(date -u +'%Y-%m-%dT%H:%M:%SZ')}"

  sops_export_vars 'observes/secrets/prod.yaml' \
    'bugsnag_notifier_code_etl' \
    && sops_export_vars 'common/secrets/dev.yaml' \
      INTEGRATES_API_TOKEN \
    && export bugsnag_notifier_key="${bugsnag_notifier_code_etl}" \
    && echo "[INFO] Working on ${group}" \
    && rm -rf -- "groups" \
    && echo "[INFO] Cloning ${group}" \
    && CI=true \
      CI_COMMIT_REF_NAME='trunk' \
      melts --init pull-repos --group "${group}" \
    && chown -R "${USER}" ./groups \
    && echo "[INFO] Uploading ${group}" \
    && shopt -s nullglob \
    && observes-etl-code upload-code-snowflake \
      --integrates-token "${INTEGRATES_API_TOKEN}" \
      --seen-at "${seen_at}" \
      --namespace "${group}" \
      "groups/${group}/"* \
    && echo "[INFO] Amend authors of ${group}" \
    && observes-etl-code amend-authors-snowflake \
      --integrates-token "${INTEGRATES_API_TOKEN}" \
      --namespace "${group}" \
    && success-indicators \
      compound-job \
      --job 'code_upload_snowflake' \
      --child "${group}" \
    && shopt -u nullglob \
    && rm -rf "groups/${group}/"
}

job_code_upload_snowflake "${@}"
