makes_inputs:
let
  uploadGroup =
    makes_inputs.outputs."/computeOnAwsBatch/observesCodeEtlUploadGroupSnowflake";
  uploadPerRoot =
    makes_inputs.outputs."/computeOnAwsBatch/observesCodeEtlUploadPerRootSnowflake";
  _to_path = b: "${b}/bin/${b.name}";
  root =
    makes_inputs.projectPath makes_inputs.inputs.observesIndex.etl.code.root;
  bundle = import "${root}/entrypoint.nix" makes_inputs;
  env = bundle.env.runtime;
  common_vars =
    import (makes_inputs.projectPath "/observes/common/common_vars.nix") {
      inherit (makes_inputs) projectPath;
    };
in {
  export = common_vars ++ [
    [ "UPLOAD_GROUP_ON_AWS" (_to_path uploadGroup) "" ]
    [ "UPLOAD_ROOT_ON_AWS" (_to_path uploadPerRoot) "" ]
  ];
  bin = [ env makes_inputs.inputs.nixpkgs.git ];
}
