from __future__ import (
    annotations,
)

import logging
from dataclasses import (
    dataclass,
)
from datetime import (
    datetime,
)

from etl_utils.typing import (
    Callable,
    FrozenSet,
)
from fa_purity import (
    Cmd,
    FrozenList,
    Maybe,
    Stream,
)

from code_etl._utils import (
    Date,
)
from code_etl.objs import (
    Checkpoint,
    CommitDataId,
    CommitStamp,
    Contribution,
    DbCommitStamp,
    GroupId,
    RepoId,
    RepoRegistration,
    User,
)

LOG = logging.getLogger(__name__)


@dataclass(frozen=True)
class All:
    pass


@dataclass(frozen=True)
class UsersPatch:
    id_obj: CommitDataId
    author: User
    committer: User


@dataclass(frozen=True, kw_only=True)
class StampsClient:
    init_commits_table: Cmd[None]
    init_files_table: Cmd[None]
    count_items: Callable[[GroupId | All], Cmd[int]]
    most_recent_hash: Callable[[RepoId], Cmd[Maybe[str]]]
    insert_stamps: Callable[[FrozenList[DbCommitStamp]], Cmd[None]]
    group_data: Callable[[GroupId | All], Cmd[Stream[CommitStamp]]]
    amend_users: Callable[[FrozenList[UsersPatch]], Cmd[None]]
    get_stamp: Callable[[CommitDataId], Cmd[Maybe[CommitStamp]]]
    exist: Callable[[CommitDataId], Cmd[bool]]


@dataclass(frozen=True)
class RegistrationClient:
    init_table: Cmd[None]
    count_registrations: Callable[[GroupId | All], Cmd[int]]
    register: Callable[[FrozenList[RepoRegistration]], Cmd[None]]
    stream_data: Callable[[GroupId | All], Cmd[Stream[RepoRegistration]]]
    get: Callable[[RepoId], Cmd[Maybe[RepoRegistration]]]
    exist: Callable[[RepoId], Cmd[bool]]


@dataclass(frozen=True)
class FirstStamp:
    date: datetime
    is_initial: bool


@dataclass(frozen=True)
class BillingClient:
    get_commit_first_inserted_at: Callable[[Contribution], Cmd[Maybe[FirstStamp]]]
    get_month_repos: Callable[[Date], Cmd[FrozenSet[GroupId]]]
    get_month_contributions: Callable[[GroupId, Date], Cmd[Stream[Contribution]]]


@dataclass(frozen=True, kw_only=True)
class DbCheckpointClient:
    init_table: Cmd[None]
    set_checkpoint: Callable[[Checkpoint], Cmd[None]]
    get_checkpoint: Callable[[RepoId], Cmd[Maybe[Checkpoint]]]
    set_start_point_search_progress: Callable[[Checkpoint], Cmd[None]]
    get_start_point_search_progress: Callable[[RepoId], Cmd[Maybe[Checkpoint]]]
    delete_start_point_search_progress: Callable[[RepoId], Cmd[None]]
    set_end_point_search_progress: Callable[[Checkpoint], Cmd[None]]
    get_end_point_search_progress: Callable[[RepoId], Cmd[Maybe[Checkpoint]]]
    delete_end_point_search_progress: Callable[[RepoId], Cmd[None]]
