from fa_purity.json import (
    Primitive,
)

from code_etl.objs import (
    Checkpoint,
)


def encode_checkpoint_to_update(
    checkpoint: Checkpoint,
) -> dict[str, Primitive]:
    raw: dict[str, Primitive] = {
        ":commit_hash": checkpoint.commit_id.hash.hash,
        ":is_initial": checkpoint.is_initial,
        ":inserted_at": checkpoint.inserted_at.date_time.isoformat(),
    }

    return raw


def encode_key(checkpoint: Checkpoint) -> dict[str, str]:
    return {
        "group": checkpoint.commit_id.repo.group.name,
        "repository": checkpoint.commit_id.repo.repository.name,
    }
