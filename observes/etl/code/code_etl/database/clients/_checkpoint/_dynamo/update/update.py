from fa_purity import (
    Cmd,
)
from mypy_boto3_dynamodb.service_resource import (
    Table,
)

from code_etl.database.clients._checkpoint._dynamo.encode import (
    encode_checkpoint_to_update,
    encode_key,
)
from code_etl.objs import (
    Checkpoint,
)


def _update_item(table: Table, checkpoint: Checkpoint) -> Cmd[None]:
    def _action() -> None:
        table.update_item(
            Key=encode_key(checkpoint),
            UpdateExpression=(
                "SET is_initial = :is_initial,"
                "inserted_at = :inserted_at, commit_hash = :commit_hash"
            ),
            ExpressionAttributeValues=encode_checkpoint_to_update(  # type: ignore[arg-type]
                checkpoint,
            ),
            ReturnValues="NONE",
        )

    return Cmd.wrap_impure(_action)


def update_checkpoint(table: Table, checkpoint: Checkpoint) -> Cmd[None]:
    return _update_item(table, checkpoint)
