import inspect

from etl_utils.bug import (
    Bug,
)
from fa_purity import (
    Cmd,
    Maybe,
    Result,
    ResultE,
)
from fa_purity.json import (
    JsonObj,
    JsonValueFactory,
    Unfolder,
)
from mypy_boto3_dynamodb.service_resource import (
    Table,
)
from mypy_boto3_dynamodb.type_defs import (
    GetItemOutputTableTypeDef,
)

from code_etl.database.clients._checkpoint._dynamo._decode import (
    decode_checkpoint,
)
from code_etl.objs import (
    Checkpoint,
)


def assert_item(
    result: GetItemOutputTableTypeDef,
) -> ResultE[Maybe[JsonObj]]:
    if item := result.get("Item"):
        return JsonValueFactory.from_any(item).bind(Unfolder.to_json).map(lambda x: Maybe.some(x))

    return Result.success(Maybe.empty())


def _get_item(
    table: Table,
    key: dict[str, str],
    attributes: tuple[str, ...],
) -> Cmd[Maybe[JsonObj]]:
    def _action() -> Maybe[JsonObj]:
        return Bug.assume_success(
            "dynamodb_get_item",
            inspect.currentframe(),
            (str(key), str(attributes)),
            assert_item(
                table.get_item(
                    Key=key,
                    AttributesToGet=attributes,
                ),
            ),
        )

    return Cmd.wrap_impure(_action)


def get_checkpoint(
    table: Table,
    group_name: str,
    repository_nickname: str,
) -> Cmd[Maybe[Checkpoint]]:
    key = {
        "group": group_name,
        "repository": repository_nickname,
    }

    attributes = (
        "inserted_at",
        "commit_hash",
        "is_initial",
    )

    return _get_item(table, key, attributes).map(
        lambda x: decode_checkpoint(x, group_name, repository_nickname),
    )
