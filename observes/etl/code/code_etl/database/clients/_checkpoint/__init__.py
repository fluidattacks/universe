from fa_purity import (
    Cmd,
)

from code_etl.database._core import (
    DbCheckpointClient,
)
from code_etl.database.clients._checkpoint._dynamo.table import (
    DynamoTableClient,
)


def new_dynamo_client(
    checkpoint_table_name: str,
    start_point_table_name: str,
    end_point_table_name: str,
) -> Cmd[DbCheckpointClient]:
    return DynamoTableClient.new(checkpoint_table_name).bind(
        lambda checkpoint_table: DynamoTableClient.new(start_point_table_name).bind(
            lambda start_point_table: DynamoTableClient.new(end_point_table_name).map(
                lambda end_point_table: DbCheckpointClient(
                    init_table=Cmd.wrap_value(None),
                    set_checkpoint=checkpoint_table.update_or_put,
                    get_checkpoint=checkpoint_table.get,
                    set_start_point_search_progress=start_point_table.update_or_put,
                    get_start_point_search_progress=start_point_table.get,
                    delete_start_point_search_progress=start_point_table.delete,
                    set_end_point_search_progress=end_point_table.update_or_put,
                    get_end_point_search_progress=end_point_table.get,
                    delete_end_point_search_progress=end_point_table.delete,
                ),
            ),
        ),
    )
