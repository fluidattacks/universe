import inspect
from dataclasses import (
    dataclass,
)
from datetime import (
    UTC,
    datetime,
)

from etl_utils.bug import (
    Bug,
)
from etl_utils.typing import (
    Dict,
    FrozenSet,
    TypeVar,
)
from fa_purity import (
    Cmd,
    FrozenList,
    FrozenTools,
    Maybe,
    PureIterFactory,
    Result,
    ResultE,
    ResultFactory,
    Stream,
    StreamTransform,
    Unsafe,
    cast_exception,
)
from fa_purity.date_time import (
    DatetimeFactory,
    RawDatetime,
)
from fa_purity.json import (
    JsonPrimitiveUnfolder,
    Primitive,
)
from redshift_client.core.id_objs import (
    SchemaId,
    TableId,
)
from redshift_client.sql_client import (
    DbPrimitive,
    DbPrimitiveFactory,
    QueryValues,
    RowData,
)
from snowflake_client import (
    SnowflakeCursor,
    SnowflakeQuery,
)

from code_etl._utils import (
    Date,
)
from code_etl.database import (
    BillingClient,
    FirstStamp,
)
from code_etl.database.clients import (
    _utils,
)
from code_etl.objs import (
    CommitDataId,
    CommitId,
    Contribution,
    GroupId,
    RepoId,
    RepoName,
    User,
)
from code_etl.str_utils import (
    TruncatedStr,
)

_T = TypeVar("_T")


def _assert_str(val: DbPrimitive) -> str:
    return val.map(
        lambda p: JsonPrimitiveUnfolder.to_str(p).alt(Unsafe.raise_exception).to_union(),
        lambda _: Unsafe.raise_exception(Exception("Expected str but got datetime")),
    )


def _get_index(items: FrozenList[_T], index: int) -> ResultE[_T]:
    factory: ResultFactory[_T, Exception] = ResultFactory()
    try:
        return factory.success(items[index])
    except IndexError as err:
        return factory.failure(err)


@dataclass(frozen=True)
class _BillingClient1:
    _sql: SnowflakeCursor
    _schema: SchemaId
    _table: TableId

    @staticmethod
    def _decode_first(raw: RowData) -> ResultE[FirstStamp]:
        inserted_at: ResultE[datetime] = _get_index(raw.data, 0).bind(
            lambda p: p.map(
                lambda _: Result.failure(ValueError("Expected date"), datetime).alt(cast_exception),
                lambda d: Result.success(d, Exception),
            ),
        )
        seen_at: ResultE[datetime] = _get_index(raw.data, 1).bind(
            lambda p: p.map(
                lambda _: Result.failure(ValueError("Expected date"), datetime).alt(cast_exception),
                lambda d: Result.success(d, Exception),
            ),
        )
        return inserted_at.bind(
            lambda i: seen_at.map(
                lambda s: FirstStamp(
                    i,
                    Bug.assume_success(
                        "first_stamp_is_initial",
                        inspect.currentframe(),
                        (),
                        DatetimeFactory.new_utc(
                            RawDatetime(
                                year=s.year,
                                month=s.month,
                                day=s.day,
                                hour=s.hour,
                                minute=s.minute,
                                second=s.second,
                                microsecond=s.microsecond,
                                time_zone=UTC,
                            ),
                        ),
                    )
                    == DatetimeFactory.EPOCH_START,
                ),
            ),
        )

    def get_commit_first_inserted_at(self, contrib: Contribution) -> Cmd[Maybe[FirstStamp]]:
        stm = """
            SELECT inserted_at, seen_at FROM {schema}.{table}
            WHERE author_name = %(author)s
            AND author_email = %(email)s
            AND authored_at = %(authored_at)s
            AND message = %(msg)s
            ORDER BY inserted_at ASC LIMIT 1
        """
        identifiers: Dict[str, str] = {
            "schema": self._schema.name.to_str(),
            "table": self._table.name.to_str(),
        }
        query = Bug.assume_success(
            "get_commit_first_inserted_at_query",
            inspect.currentframe(),
            (str(stm), str(identifiers)),
            SnowflakeQuery.dynamic_query(stm, FrozenTools.freeze(identifiers)),
        )
        values: Dict[str, Primitive] = {
            "author": contrib.author.name,
            "email": contrib.author.email,
            "authored_at": contrib.authored_at.date_time.isoformat(),
            "msg": contrib.message.msg,
        }
        return self._sql.execute(
            query,
            QueryValues(DbPrimitiveFactory.from_raw_prim_dict(FrozenTools.freeze(values))),
        ).map(
            lambda r: Bug.assume_success(
                "get_commit_first_inserted_at",
                inspect.currentframe(),
                (),
                r,
            ),
        ) + self._sql.fetch_one.map(
            lambda r: Bug.assume_success(
                "get_commit_first_inserted_at_fetch",
                inspect.currentframe(),
                (),
                r,
            ),
        ).map(
            lambda i: i.map(
                lambda r: Bug.assume_success(
                    "_decode_first",
                    inspect.currentframe(),
                    (),
                    self._decode_first(r),
                ),
            ),
        )

    def get_month_repos(self, date: Date) -> Cmd[FrozenSet[GroupId]]:
        stm = """
            SELECT DISTINCT
                namespace
            FROM {schema}.{table}
            WHERE
                TO_CHAR(inserted_at, 'YYYY-MM') = %(inserted_at)s
            AND seen_at != %(epoch)s
        """
        identifiers: Dict[str, str] = {
            "schema": self._schema.name.to_str(),
            "table": self._table.name.to_str(),
        }
        args: Dict[str, Primitive | datetime] = {
            "inserted_at": date.strftime("%Y-%m"),
            "epoch": DatetimeFactory.EPOCH_START.date_time,
        }

        def _to_group_id(row: RowData) -> GroupId:
            return GroupId(_assert_str(row.data[0]))

        query = Bug.assume_success(
            "get_month_repos",
            inspect.currentframe(),
            (str(stm), str(identifiers)),
            SnowflakeQuery.dynamic_query(stm, FrozenTools.freeze(identifiers)),
        )
        return self._sql.execute(
            query,
            QueryValues(DbPrimitiveFactory.from_raw_dict(FrozenTools.freeze(args))),
        ).map(
            lambda r: Bug.assume_success("get_month_repos", inspect.currentframe(), (), r),
        ) + self._sql.fetch_all.map(
            lambda r: Bug.assume_success(
                "get_month_repos_fetch",
                inspect.currentframe(),
                (),
                r,
            ),
        ).map(lambda d: frozenset(map(_to_group_id, d)))

    def get_month_contributions(self, group: GroupId, date: Date) -> Cmd[Stream[Contribution]]:
        stm = """
            SELECT
                author_name,
                author_email,
                authored_at,
                message,
                repository,
                hash
            FROM {schema}.{table}
            WHERE
                namespace = %(namespace)s
            AND TO_CHAR(inserted_at, 'YYYY-MM') = %(inserted_at)s
            AND seen_at != %(epoch)s
            AND  was_in_a_gap = false
        """
        identifiers: Dict[str, str] = {
            "schema": self._schema.name.to_str(),
            "table": self._table.name.to_str(),
        }
        args: Dict[str, Primitive | datetime] = {
            "namespace": group.name,
            "inserted_at": date.strftime("%Y-%m"),
            "epoch": DatetimeFactory.EPOCH_START.date_time,
        }

        def to_contrib(raw: RowData) -> Contribution:
            return Contribution(
                User(_assert_str(raw.data[0]), _assert_str(raw.data[1])),
                Bug.assume_success(
                    "get_month_contributions_to_contrib",
                    inspect.currentframe(),
                    (),
                    _utils.to_datetime_utc(raw.data[2]),
                ),
                TruncatedStr.truncate(_assert_str(raw.data[3]), 4096),
                CommitDataId(
                    RepoId(group, RepoName(_assert_str(raw.data[4]))),
                    CommitId(
                        _assert_str(raw.data[5]),
                    ),
                ),
            )

        query = Bug.assume_success(
            "get_month_contributions_query",
            inspect.currentframe(),
            (str(stm), str(identifiers)),
            SnowflakeQuery.dynamic_query(stm, FrozenTools.freeze(identifiers)),
        )
        return (
            self._sql.execute(
                query,
                QueryValues(DbPrimitiveFactory.from_raw_dict(FrozenTools.freeze(args))),
            )
            .map(
                lambda r: Bug.assume_success(
                    "get_month_contributions",
                    inspect.currentframe(),
                    (),
                    r,
                ),
            )
            .map(
                lambda _: self._sql.fetch_chunks_stream(1000)
                .map(
                    lambda r: Bug.assume_success(
                        "get_month_contributions_fetch",
                        inspect.currentframe(),
                        (),
                        r,
                    ),
                )
                .transform(
                    lambda s: StreamTransform.chain(s.map(lambda i: PureIterFactory.from_list(i))),
                )
                .map(to_contrib),
            )
        )


def new_client(sql: SnowflakeCursor, schema: SchemaId, table: TableId) -> BillingClient:
    billing = _BillingClient1(sql, schema, table)
    return BillingClient(
        billing.get_commit_first_inserted_at,
        billing.get_month_repos,
        billing.get_month_contributions,
    )
