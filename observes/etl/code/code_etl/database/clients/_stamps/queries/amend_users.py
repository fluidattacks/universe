import inspect
import logging
from datetime import (
    datetime,
)

from etl_utils.bug import (
    Bug,
)
from etl_utils.typing import (
    Dict,
    Tuple,
)
from fa_purity import (
    Cmd,
    FrozenList,
    FrozenTools,
    PureIterFactory,
)
from fa_purity.json import (
    Primitive,
)
from redshift_client.core.id_objs import (
    SchemaId,
    TableId,
)
from redshift_client.sql_client import (
    DbPrimitiveFactory,
    QueryValues,
)
from snowflake_client import (
    SnowflakeCursor,
    SnowflakeQuery,
)

from code_etl.database import (
    UsersPatch,
)
from code_etl.database.clients._stamps.queries.init_table import (
    AUTHOR_EMAIL_COLUMN,
    AUTHOR_NAME_COLUMN,
    COMMITTER_EMAIL_COLUMN,
    COMMITTER_NAME_COLUMN,
)

LOG = logging.getLogger(__name__)


def _generate_when(column_index: int, id_index: int) -> str:
    return (
        f"WHEN hash = %(hash_{id_index})s"
        f" AND namespace = %(namespace_{id_index})s"
        f" AND repository = %(repository_{id_index})s"
        f" THEN %(value_{column_index}_{id_index})s"
    )


def _generate_assignment(column_index: int, total_items: int) -> str:
    when_statements = " ".join(
        PureIterFactory.from_range(range(1, total_items + 1)).map(
            lambda i: _generate_when(column_index, i),
        ),
    )
    return f"{{column_{column_index}}} = CASE {when_statements} ELSE {{column_{column_index}}} END"


def _query(
    schema: SchemaId,
    table: TableId,
    data: FrozenList[UsersPatch],
) -> Tuple[SnowflakeQuery, QueryValues]:
    total_items = len(data)
    assignments = PureIterFactory.from_range(range(1, 5)).map(
        lambda c: _generate_assignment(c, total_items),
    )
    statement = "UPDATE {schema}.{table} SET " + ",".join(assignments)  # noqa: S608

    def _args_for_data(index: int, item: UsersPatch) -> Dict[str, Primitive | datetime]:
        return {
            f"value_1_{index}": item.author.name,
            f"value_2_{index}": item.author.email,
            f"value_3_{index}": item.committer.name,
            f"value_4_{index}": item.committer.email,
            f"hash_{index}": item.id_obj.hash.hash,
            f"namespace_{index}": item.id_obj.repo.group.name,
            f"repository_{index}": item.id_obj.repo.repository.name,
        }

    identifiers: Dict[str, str] = {
        "column_1": AUTHOR_NAME_COLUMN.name.to_str(),
        "column_2": AUTHOR_EMAIL_COLUMN.name.to_str(),
        "column_3": COMMITTER_NAME_COLUMN.name.to_str(),
        "column_4": COMMITTER_EMAIL_COLUMN.name.to_str(),
        "schema": schema.name.to_str(),
        "table": table.name.to_str(),
    }

    empty: Dict[str, Primitive | datetime] = {}
    args: Dict[str, Primitive | datetime] = (
        PureIterFactory.from_list(data)
        .enumerate(1)
        .map(lambda t: _args_for_data(t[0], t[1]))
        .reduce(lambda r, d: r | d, empty)
    )
    query = Bug.assume_success(
        "amend_users_query",
        inspect.currentframe(),
        (str(statement), str(identifiers)),
        SnowflakeQuery.dynamic_query(statement, FrozenTools.freeze(identifiers)),
    )
    return (
        query,
        QueryValues(DbPrimitiveFactory.from_raw_dict(FrozenTools.freeze(args))),
    )


def amend_users(
    cursor: SnowflakeCursor,
    schema: SchemaId,
    table: TableId,
    data: FrozenList[UsersPatch],
) -> Cmd[None]:
    msg = Cmd.wrap_impure(lambda: LOG.info("Amending %s users", len(data)))
    return msg + cursor.execute(*_query(schema, table, data)).map(
        lambda r: Bug.assume_success(
            "amend_users",
            inspect.currentframe(),
            (str(schema), str(table), str(data)),
            r,
        ),
    )
