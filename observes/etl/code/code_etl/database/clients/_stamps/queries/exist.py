import inspect

from etl_utils.bug import (
    Bug,
)
from etl_utils.typing import (
    Dict,
    Tuple,
)
from fa_purity import (
    Cmd,
    FrozenTools,
)
from fa_purity.json import (
    Primitive,
)
from redshift_client.core.id_objs import (
    DbTableId,
)
from redshift_client.sql_client import (
    DbPrimitiveFactory,
    QueryValues,
)
from snowflake_client import (
    SnowflakeCursor,
    SnowflakeQuery,
)

from code_etl.objs import (
    CommitDataId,
)

from .init_table import (
    GROUP_COLUMN,
    HASH_COLUMN,
    REPOSITORY_COLUMN,
)


def _query(
    table_id: DbTableId,
    commit_id: CommitDataId,
) -> Tuple[SnowflakeQuery, QueryValues]:
    statement = """
        SELECT 1
        FROM {schema}.{table}
        WHERE {group_column} = %(group)s
        AND {repo_column} = %(repo)s
        AND {hash_column} = %(hash)s
    """
    identifiers: Dict[str, str] = {
        "group_column": GROUP_COLUMN.name.to_str(),
        "repo_column": REPOSITORY_COLUMN.name.to_str(),
        "hash_column": HASH_COLUMN.name.to_str(),
        "schema": table_id.schema.name.to_str(),
        "table": table_id.table.name.to_str(),
    }
    args: Dict[str, Primitive] = {
        "group": commit_id.repo.group.name,
        "repo": commit_id.repo.repository.name,
        "hash": commit_id.hash.hash,
    }
    query = Bug.assume_success(
        "stamp_exist_query",
        inspect.currentframe(),
        (str(statement), str(identifiers)),
        SnowflakeQuery.dynamic_query(statement, FrozenTools.freeze(identifiers)),
    )
    return (
        query,
        QueryValues(DbPrimitiveFactory.from_raw_prim_dict(FrozenTools.freeze(args))),
    )


def stamp_exist(
    cursor: SnowflakeCursor,
    table: DbTableId,
    commit_id: CommitDataId,
) -> Cmd[bool]:
    request = cursor.execute(*_query(table, commit_id)).map(
        lambda r: Bug.assume_success(
            "stamp_exist_request",
            inspect.currentframe(),
            (str(table), str(commit_id)),
            r,
        ),
    )
    return request + cursor.fetch_one.map(
        lambda r: Bug.assume_success(
            "stamp_exist_fetch_result",
            inspect.currentframe(),
            (str(table), str(commit_id)),
            r,
        ),
    ).map(lambda b: b.map(lambda _: True).value_or(False))
