import inspect
import logging

from etl_utils.bug import (
    Bug,
)
from etl_utils.retry import (
    cmd_if_fail,
    retry_cmd,
    sleep_cmd,
)
from etl_utils.typing import (
    Dict,
    Tuple,
)
from fa_purity import (
    Cmd,
    FrozenDict,
    FrozenList,
    FrozenTools,
    PureIterFactory,
)
from fa_purity.date_time import (
    DatetimeUTC,
)
from fa_purity.json import (
    Primitive,
)
from redshift_client.core.id_objs import (
    DbTableId,
)
from redshift_client.sql_client import (
    DbPrimitive,
    DbPrimitiveFactory,
    QueryValues,
)
from snowflake_client import (
    SnowflakeCursor,
    SnowflakeQuery,
)

from code_etl.objs import (
    DbCommitStamp,
)

from .init_table import (
    GROUP_COLUMN,
    HASH_COLUMN,
    REPOSITORY_COLUMN,
    table_definition,
)

LOG = logging.getLogger(__name__)


def _encode_datetime(date: DatetimeUTC) -> str:
    return date.date_time.isoformat()


def _int_to_str(value: int) -> str:
    return str(value)


def _encode_stamp(db_stamp: DbCommitStamp, postfix: str) -> FrozenDict[str, DbPrimitive]:
    stamp = db_stamp.stamp
    data = stamp.commit.data
    commit_id = stamp.commit.commit_id
    encoded: Dict[str, Primitive] = {
        "author_name": data.author.name,
        "author_email": data.author.email,
        "authored_at": _encode_datetime(data.authored_at),
        "committer_name": data.committer.name,
        "committer_email": data.committer.email,
        "committed_at": _encode_datetime(data.committed_at),
        "message": data.message.msg,
        "summary": data.summary.msg,
        "total_insertions": data.deltas.total_insertions,
        "total_deletions": data.deltas.total_deletions,
        "total_lines": data.deltas.total_lines,
        "total_files": data.deltas.total_files,
        "namespace": commit_id.repo.group.name,
        "repository": commit_id.repo.repository.name,
        "hash": commit_id.hash.hash,
        "seen_at": _encode_datetime(stamp.seen_at),
        "inserted_at": _encode_datetime(stamp.inserted_at),
        "was_in_a_gap": db_stamp.was_in_a_gap,
    }
    return DbPrimitiveFactory.from_raw_prim_dict(
        FrozenTools.freeze(encoded).map(lambda k: k + postfix, lambda v: v),
    )


def _snowflake_query(
    table: DbTableId,
    stamps: FrozenList[DbCommitStamp],
) -> Tuple[SnowflakeQuery, QueryValues]:
    _fields = PureIterFactory.from_list(table_definition().order).map(lambda c: c.name.to_str())
    encoded_fields = ",".join(_fields)
    encoded_fields_source = ",".join(_fields.map(lambda f: "source." + f))
    values_template = ",".join(
        PureIterFactory.from_range(range(len(stamps)))
        .map(lambda i: ",".join(_fields.map(lambda f: f"%({f + '_' + _int_to_str(i)})s")))
        .map(lambda s: "(" + s + ")"),
    )
    values = FrozenDict(
        dict(
            PureIterFactory.from_list(stamps)
            .enumerate(0)
            .bind(
                lambda t: PureIterFactory.from_list(
                    tuple(_encode_stamp(t[1], "_" + _int_to_str(t[0])).items()),
                ),
            )
            .to_list(),
        ),
    )
    statement = f"""
    MERGE INTO {{schema}}.{{table}}
        USING (
            SELECT * FROM (
                VALUES {values_template}
            ) virtual_table(
                {encoded_fields}
            )
        ) source
    ON {{table}}.{{hash_column}} = source.{{hash_column}}
        AND {{table}}.{{group_column}} = source.{{group_column}}
        AND {{table}}.{{repo_column}} = source.{{repo_column}}
        WHEN NOT MATCHED THEN INSERT ({encoded_fields})
        VALUES ({encoded_fields_source});
    """  # noqa: S608
    identifiers: Dict[str, str] = {
        "hash_column": HASH_COLUMN.name.to_str(),
        "group_column": GROUP_COLUMN.name.to_str(),
        "repo_column": REPOSITORY_COLUMN.name.to_str(),
        "schema": table.schema.name.to_str(),
        "table": table.table.name.to_str(),
    }
    query = Bug.assume_success(
        "insert_stamps_query",
        inspect.currentframe(),
        (),
        SnowflakeQuery.dynamic_query(statement, FrozenTools.freeze(identifiers)),
    )
    return (
        query,
        QueryValues(values),
    )


def insert_stamps(
    cursor: SnowflakeCursor,
    table: DbTableId,
    stamps: FrozenList[DbCommitStamp],
) -> Cmd[None]:
    query, values = _snowflake_query(table, stamps)
    action = retry_cmd(
        cursor.execute(query, values).bind(
            lambda r: r.to_coproduct().map(
                lambda _: Cmd.wrap_value(r),
                lambda e: Cmd.wrap_impure(lambda: LOG.error(str(e))).map(lambda _: r),
            ),
        ),
        lambda i, r: cmd_if_fail(r, sleep_cmd(10 + i * 60)),
        10,
    )
    return action.map(
        lambda r: Bug.assume_success(
            "insert_stamps",
            inspect.currentframe(),
            (str(table), str(stamps)),
            r,
        ),
    )
