import inspect

from etl_utils.bug import (
    Bug,
)
from etl_utils.typing import (
    Dict,
    Tuple,
)
from fa_purity import (
    Cmd,
    Coproduct,
    FrozenDict,
    FrozenList,
    FrozenTools,
    Maybe,
    PureIterFactory,
    ResultE,
    ResultTransform,
    Stream,
    StreamFactory,
    StreamTransform,
)
from fa_purity.json import (
    Primitive,
)
from redshift_client.core.id_objs import (
    DbTableId,
)
from redshift_client.sql_client import (
    DbPrimitiveFactory,
    QueryValues,
)
from snowflake_client import (
    SnowflakeCursor,
    SnowflakeQuery,
)

from code_etl.database import (
    All,
)
from code_etl.database.clients._stamps.queries.init_table import (
    GROUP_COLUMN,
    table_definition,
)
from code_etl.objs import (
    CommitStamp,
    GroupId,
)

from ._decode import (
    StampDecoder,
)


def _query(table: DbTableId, group: Coproduct[GroupId, All]) -> Tuple[SnowflakeQuery, QueryValues]:
    _fields = PureIterFactory.from_list(table_definition().order).map(lambda c: c.name.to_str())
    _attrs = ",".join(_fields)
    statement = "".join(
        (
            f"SELECT {_attrs} ",
            "FROM {schema}.{table} ",
            group.map(
                lambda _: "WHERE {group_column} = %(namespace)s",
                lambda _: "",
            ),
        ),
    )
    id_args: Dict[str, str] = {
        "schema": table.schema.name.to_str(),
        "table": table.table.name.to_str(),
        "group_column": GROUP_COLUMN.name.to_str(),
    }
    args: Dict[str, Primitive] = group.map(lambda g: {"namespace": g.name}, lambda _: {})
    query = Bug.assume_success(
        "group_data_query",
        inspect.currentframe(),
        (str(statement), str(id_args)),
        SnowflakeQuery.dynamic_query(statement, FrozenTools.freeze(id_args)),
    )
    return (
        query,
        QueryValues(DbPrimitiveFactory.from_raw_prim_dict(FrozenTools.freeze(args))),
    )


def _decoder() -> StampDecoder:
    index_map = (
        PureIterFactory.from_list(table_definition().order)
        .enumerate(0)
        .map(lambda t: (t[1], t[0]))
        .transform(lambda p: FrozenDict(dict(p)))
    )
    return StampDecoder(index_map)


def _fetch(
    cursor: SnowflakeCursor,
    chunk: int,
    decoder: StampDecoder,
) -> Cmd[ResultE[Maybe[FrozenList[ResultE[CommitStamp]]]]]:
    return cursor.fetch_chunk(chunk).map(
        lambda r: r.map(
            lambda rows: Maybe.from_optional(
                tuple(decoder.decode_stamp(r) for r in rows) if rows else None,
            ),
        ),
    )


def group_data(
    cursor: SnowflakeCursor,
    table: DbTableId,
    group: Coproduct[GroupId, All],
    items_per_block: int,
) -> Cmd[Stream[CommitStamp]]:
    request = cursor.execute(*_query(table, group)).map(
        lambda r: Bug.assume_success(
            "group_data_request",
            inspect.currentframe(),
            (str(table), str(group)),
            r,
        ),
    )
    decoder = _decoder()
    items = PureIterFactory.infinite_range(0, 1).map(
        lambda _: _fetch(cursor, items_per_block, decoder),
    )
    fetch = (
        StreamFactory.from_commands(items)
        .map(
            lambda r: Bug.assume_success(
                "group_data_fetch_result",
                inspect.currentframe(),
                (str(table), str(group)),
                r,
            ),
        )
        .transform(lambda s: StreamTransform.until_empty(s))
        .map(
            lambda b: Bug.assume_success(
                "group_data_decode_block",
                inspect.currentframe(),
                (str(table), str(group)),
                ResultTransform.all_ok(b),
            ),
        )
        .map(lambda p: PureIterFactory.from_list(p))
        .transform(lambda s: StreamTransform.chain(s))
    )
    return request.map(lambda _: fetch)
