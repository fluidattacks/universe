import inspect

from connection_manager import (
    CommonTableClient,
)
from etl_utils.bug import (
    Bug,
)
from fa_purity import (
    Cmd,
    FrozenDict,
)
from redshift_client.core.column import (
    Column,
)
from redshift_client.core.data_type.core import (
    DataType,
    PrecisionType,
    PrecisionTypes,
    StaticTypes,
)
from redshift_client.core.id_objs import (
    ColumnId,
    DbTableId,
    Identifier,
)
from redshift_client.core.table import (
    Table,
)
from redshift_client.sql_client import (
    DbPrimitiveFactory,
)

# [WARNING] changing columns here will affect queries
AUTHOR_EMAIL_COLUMN = ColumnId(Identifier.new("author_email"))
AUTHOR_NAME_COLUMN = ColumnId(Identifier.new("author_name"))
AUTHORED_AT_COLUMN = ColumnId(Identifier.new("authored_at"))
COMMITTER_EMAIL_COLUMN = ColumnId(Identifier.new("committer_email"))
COMMITTER_NAME_COLUMN = ColumnId(Identifier.new("committer_name"))
COMMITTED_AT_COLUMN = ColumnId(Identifier.new("committed_at"))
HASH_COLUMN = ColumnId(Identifier.new("hash"))
MESSAGE_COLUMN = ColumnId(Identifier.new("message"))
SUMMARY_COLUMN = ColumnId(Identifier.new("summary"))
TOTAL_INSERTIONS_COLUMN = ColumnId(Identifier.new("total_insertions"))
TOTAL_DELETIONS_COLUMN = ColumnId(Identifier.new("total_deletions"))
TOTAL_LINES_COLUMN = ColumnId(Identifier.new("total_lines"))
TOTAL_FILES_COLUMN = ColumnId(Identifier.new("total_files"))
GROUP_COLUMN = ColumnId(Identifier.new("namespace"))
REPOSITORY_COLUMN = ColumnId(Identifier.new("repository"))
SEEN_AT_COLUMN = ColumnId(Identifier.new("seen_at"))
INSERTED_AT_COLUMN = ColumnId(Identifier.new("inserted_at"))
WAS_IN_A_GAP = ColumnId(Identifier.new("was_in_a_gap"))


def _var_char(limit: int) -> DataType:
    return DataType(PrecisionType(PrecisionTypes.VARCHAR, limit))


def _char(limit: int) -> DataType:
    return DataType(PrecisionType(PrecisionTypes.CHAR, limit))


def table_definition() -> Table:
    none = DbPrimitiveFactory.from_raw(None)
    columns: FrozenDict[ColumnId, Column] = FrozenDict(
        {
            AUTHOR_EMAIL_COLUMN: Column(_var_char(256), False, none),
            AUTHOR_NAME_COLUMN: Column(_var_char(256), False, none),
            AUTHORED_AT_COLUMN: Column(DataType(StaticTypes.TIMESTAMPTZ), False, none),
            COMMITTER_EMAIL_COLUMN: Column(_var_char(256), False, none),
            COMMITTER_NAME_COLUMN: Column(_var_char(256), False, none),
            COMMITTED_AT_COLUMN: Column(DataType(StaticTypes.TIMESTAMPTZ), False, none),
            HASH_COLUMN: Column(_char(40), False, none),
            MESSAGE_COLUMN: Column(_var_char(4096), False, none),
            SUMMARY_COLUMN: Column(_var_char(256), False, none),
            TOTAL_INSERTIONS_COLUMN: Column(DataType(StaticTypes.INTEGER), False, none),
            TOTAL_DELETIONS_COLUMN: Column(DataType(StaticTypes.INTEGER), False, none),
            TOTAL_LINES_COLUMN: Column(DataType(StaticTypes.INTEGER), False, none),
            TOTAL_FILES_COLUMN: Column(DataType(StaticTypes.INTEGER), False, none),
            GROUP_COLUMN: Column(_var_char(64), False, none),
            REPOSITORY_COLUMN: Column(_var_char(4096), False, none),
            SEEN_AT_COLUMN: Column(DataType(StaticTypes.TIMESTAMPTZ), False, none),
            INSERTED_AT_COLUMN: Column(DataType(StaticTypes.TIMESTAMPTZ), False, none),
            WAS_IN_A_GAP: Column(
                DataType(StaticTypes.BOOLEAN),
                False,
                none,
            ),
        },
    )
    _order = (
        AUTHOR_EMAIL_COLUMN,
        AUTHOR_NAME_COLUMN,
        AUTHORED_AT_COLUMN,
        COMMITTER_EMAIL_COLUMN,
        COMMITTER_NAME_COLUMN,
        COMMITTED_AT_COLUMN,
        HASH_COLUMN,
        MESSAGE_COLUMN,
        SUMMARY_COLUMN,
        TOTAL_INSERTIONS_COLUMN,
        TOTAL_DELETIONS_COLUMN,
        TOTAL_LINES_COLUMN,
        TOTAL_FILES_COLUMN,
        GROUP_COLUMN,
        REPOSITORY_COLUMN,
        SEEN_AT_COLUMN,
        INSERTED_AT_COLUMN,
        WAS_IN_A_GAP,
    )
    table = Table.new(
        _order,
        columns,
        frozenset([GROUP_COLUMN, REPOSITORY_COLUMN, HASH_COLUMN]),
    )
    return Bug.assume_success("table_definition", inspect.currentframe(), (), table)


def init_table(client: CommonTableClient, table_id: DbTableId) -> Cmd[None]:
    table = table_definition()
    result = client.new_if_not_exist(
        table_id,
        table,
    )
    return result.map(
        lambda r: Bug.assume_success(
            "init_commits_table_execution",
            inspect.currentframe(),
            (),
            r,
        ),
    )
