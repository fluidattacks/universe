import inspect

from connection_manager import (
    CommonTableClient,
)
from etl_utils.bug import (
    Bug,
)
from fa_purity import (
    Cmd,
    FrozenDict,
)
from redshift_client.core.column import (
    Column,
)
from redshift_client.core.data_type.core import (
    DataType,
    PrecisionType,
    PrecisionTypes,
)
from redshift_client.core.id_objs import (
    ColumnId,
    DbTableId,
    Identifier,
)
from redshift_client.core.table import (
    Table,
)
from redshift_client.sql_client import (
    DbPrimitiveFactory,
)

# [WARNING] changing columns here will affect queries
GROUP_COLUMN = ColumnId(Identifier.new("namespace"))
REPOSITORY_COLUMN = ColumnId(Identifier.new("repository"))
HASH_COLUMN = ColumnId(Identifier.new("hash"))
FILE_PATH_COLUMN = ColumnId(Identifier.new("file_path"))


def _var_char(limit: int) -> DataType:
    return DataType(PrecisionType(PrecisionTypes.VARCHAR, limit))


def _char(limit: int) -> DataType:
    return DataType(PrecisionType(PrecisionTypes.CHAR, limit))


def table_definition() -> Table:
    none = DbPrimitiveFactory.from_raw(None)
    columns: FrozenDict[ColumnId, Column] = FrozenDict(
        {
            GROUP_COLUMN: Column(_var_char(64), False, none),
            REPOSITORY_COLUMN: Column(_var_char(4096), False, none),
            HASH_COLUMN: Column(_char(40), False, none),
            FILE_PATH_COLUMN: Column(_var_char(1024), False, none),
        },
    )
    _order = (
        GROUP_COLUMN,
        REPOSITORY_COLUMN,
        HASH_COLUMN,
        FILE_PATH_COLUMN,
    )
    table = Table.new(
        _order,
        columns,
        frozenset([GROUP_COLUMN, REPOSITORY_COLUMN, HASH_COLUMN, FILE_PATH_COLUMN]),
    )
    return Bug.assume_success("table_definition", inspect.currentframe(), (), table)


def init_table(client: CommonTableClient, table_id: DbTableId) -> Cmd[None]:
    table = table_definition()
    result = client.new_if_not_exist(
        table_id,
        table,
    )
    return result.map(
        lambda r: Bug.assume_success(
            "init_commits_table_execution",
            inspect.currentframe(),
            (),
            r,
        ),
    )
