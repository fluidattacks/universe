from fa_purity import (
    PureIter,
    PureIterFactory,
)

from code_etl.objs import (
    CommitStamp,
    FileStampRelation,
)


def extract_relations(
    stamp: CommitStamp,
) -> PureIter[FileStampRelation]:
    def _build(file: str) -> FileStampRelation:
        return FileStampRelation(
            stamp.commit.commit_id,
            file,
        )

    empty: PureIter[FileStampRelation] = PureIterFactory.from_list(())
    return stamp.commit.data.files.map(
        lambda f: PureIterFactory.from_list(tuple(f)).map(_build),
    ).value_or(empty)
