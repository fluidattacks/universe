import inspect

from etl_utils.bug import (
    Bug,
)
from etl_utils.decode import (
    require_index,
)
from etl_utils.typing import (
    Dict,
    Tuple,
)
from fa_purity import (
    Cmd,
    Coproduct,
    FrozenTools,
    Result,
    cast_exception,
)
from fa_purity.json import (
    JsonPrimitiveUnfolder,
    Primitive,
)
from redshift_client.core.id_objs import (
    SchemaId,
    TableId,
)
from redshift_client.sql_client import (
    DbPrimitiveFactory,
    QueryValues,
)
from snowflake_client import (
    SnowflakeCursor,
    SnowflakeQuery,
)

from code_etl.database import (
    All,
)
from code_etl.objs import (
    GroupId,
)

from .init_table import (
    GROUP_COLUMN,
)


def _query(
    schema: SchemaId,
    table: TableId,
    group: Coproduct[GroupId, All],
) -> Tuple[SnowflakeQuery, QueryValues]:
    statement = """
        SELECT COUNT(*)
        FROM {schema}.{table}
    """ + group.map(lambda _: "WHERE {group} = %(group)s", lambda _: "")  # noqa: S608
    identifiers: Dict[str, str] = {
        "group": GROUP_COLUMN.name.to_str(),
        "schema": schema.name.to_str(),
        "table": table.name.to_str(),
    }
    args: Dict[str, Primitive] = group.map(lambda g: {"group": g.name}, lambda _: {})
    query = Bug.assume_success(
        "count_registrations_query",
        inspect.currentframe(),
        (str(statement), str(identifiers)),
        SnowflakeQuery.dynamic_query(statement, FrozenTools.freeze(identifiers)),
    )
    return (
        query,
        QueryValues(DbPrimitiveFactory.from_raw_prim_dict(FrozenTools.freeze(args))),
    )


def count_registrations(
    cursor: SnowflakeCursor,
    schema: SchemaId,
    table: TableId,
    group: Coproduct[GroupId, All],
) -> Cmd[int]:
    request = cursor.execute(*_query(schema, table, group)).map(
        lambda r: Bug.assume_success(
            "count_registrations_request",
            inspect.currentframe(),
            (str(schema), str(table), str(group)),
            r,
        ),
    )
    fetch = (
        cursor.fetch_one.map(
            lambda r: Bug.assume_success(
                "count_registrations_fetch_result",
                inspect.currentframe(),
                (str(schema), str(table), str(group)),
                r,
            ),
        )
        .map(
            lambda m: Bug.assume_success(
                "count_registrations_result",
                inspect.currentframe(),
                (str(schema), str(table), str(group)),
                m.to_result(),
            ),
        )
        .map(
            lambda i: require_index(i.data, 0).bind(
                lambda j: j.map(
                    lambda v: JsonPrimitiveUnfolder.to_int(v),
                    lambda _: Result.failure(
                        TypeError("Expected `str` but got `datetime`"),
                        int,
                    ).alt(cast_exception),
                ),
            ),
        )
        .map(
            lambda r: Bug.assume_success(
                "count_registrations_decode_result",
                inspect.currentframe(),
                (str(schema), str(table), str(group)),
                r,
            ),
        )
    )
    return request + fetch
