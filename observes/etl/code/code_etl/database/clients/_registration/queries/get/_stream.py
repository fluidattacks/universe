import inspect

from etl_utils.bug import (
    Bug,
)
from etl_utils.typing import (
    Dict,
    Tuple,
)
from fa_purity import (
    Cmd,
    Coproduct,
    FrozenTools,
    PureIterFactory,
    Stream,
    StreamTransform,
)
from fa_purity.json import (
    Primitive,
)
from redshift_client.core.id_objs import (
    SchemaId,
    TableId,
)
from redshift_client.sql_client import (
    DbPrimitiveFactory,
    QueryValues,
)
from snowflake_client import (
    SnowflakeCursor,
    SnowflakeQuery,
)

from code_etl.database import (
    All,
)
from code_etl.database.clients._registration.queries.init_table import (
    GROUP_COLUMN,
    REPO_COLUMN,
    SEEN_AT_COLUMN,
)
from code_etl.objs import (
    GroupId,
    RepoRegistration,
)

from . import (
    _decode,
)


def _query(
    schema: SchemaId,
    table: TableId,
    group: Coproduct[GroupId, All],
) -> Tuple[SnowflakeQuery, QueryValues]:
    statement = """
        SELECT {group_column},{repo_column},{seen_at_column}
        FROM {schema}.{table}
    """ + group.map(  # noqa: S608
        lambda _: " WHERE {group_column} = %(group)s",
        lambda _: "",
    )
    identifiers: Dict[str, str] = {
        "group_column": GROUP_COLUMN.name.to_str(),
        "repo_column": REPO_COLUMN.name.to_str(),
        "seen_at_column": SEEN_AT_COLUMN.name.to_str(),
        "schema": schema.name.to_str(),
        "table": table.name.to_str(),
    }
    args: Dict[str, Primitive] = group.map(
        lambda g: {"group": g.name},
        lambda _: {},
    )
    query = Bug.assume_success(
        "stream_registrations_query",
        inspect.currentframe(),
        (str(statement), str(identifiers)),
        SnowflakeQuery.dynamic_query(statement, FrozenTools.freeze(identifiers)),
    )
    return (
        query,
        QueryValues(DbPrimitiveFactory.from_raw_prim_dict(FrozenTools.freeze(args))),
    )


def stream_registrations(
    cursor: SnowflakeCursor,
    schema: SchemaId,
    table: TableId,
    group: Coproduct[GroupId, All],
) -> Cmd[Stream[RepoRegistration]]:
    request = cursor.execute(*_query(schema, table, group)).map(
        lambda r: Bug.assume_success(
            "stream_registrations_request",
            inspect.currentframe(),
            (str(schema), str(table), str(group)),
            r,
        ),
    )
    return request.map(
        lambda _: cursor.fetch_chunks_stream(1000)
        .map(
            lambda r: Bug.assume_success(
                "stream_registrations_stream",
                inspect.currentframe(),
                (str(schema), str(table), str(group)),
                r,
            ),
        )
        .map(lambda i: PureIterFactory.from_list(i))
        .transform(lambda s: StreamTransform.chain(s))
        .map(
            lambda rd: Bug.assume_success(
                "decode_registration",
                inspect.currentframe(),
                (str(schema), str(table), str(group)),
                _decode.decode_registration(rd),
            ),
        ),
    )
