from datetime import (
    datetime,
)

from etl_utils.decode import (
    require_index,
)
from fa_purity import (
    Result,
    ResultE,
    cast_exception,
)
from fa_purity.date_time import (
    DatetimeFactory,
    DatetimeTZ,
)
from fa_purity.json import (
    JsonPrimitiveUnfolder,
)
from redshift_client.sql_client import (
    RowData,
)

from code_etl.objs import (
    GroupId,
    RepoId,
    RepoName,
    RepoRegistration,
)


def decode_registration(raw: RowData) -> ResultE[RepoRegistration]:
    group = (
        require_index(raw.data, 0)
        .bind(
            lambda d: d.map(
                lambda v: JsonPrimitiveUnfolder.to_str(v),
                lambda _: Result.failure(TypeError("Expected `str` but got `datetime`"), str).alt(
                    cast_exception,
                ),
            ),
        )
        .map(GroupId)
    )
    repo = (
        require_index(raw.data, 1)
        .bind(
            lambda d: d.map(
                lambda v: JsonPrimitiveUnfolder.to_str(v),
                lambda _: Result.failure(TypeError("Expected `str` but got `datetime`"), str).alt(
                    cast_exception,
                ),
            ),
        )
        .map(RepoName)
    )
    seen_at = (
        require_index(raw.data, 2)
        .bind(
            lambda d: d.map(
                lambda _: Result.failure(
                    TypeError("Expected `datetime` but got `JsonPrimitive`"),
                    datetime,
                ).alt(cast_exception),
                lambda v: Result.success(v, Exception),
            ),
        )
        .bind(DatetimeTZ.assert_tz)
        .map(DatetimeFactory.to_utc)
    )
    return group.bind(
        lambda g: repo.bind(lambda r: seen_at.map(lambda s: RepoRegistration(RepoId(g, r), s))),
    )
