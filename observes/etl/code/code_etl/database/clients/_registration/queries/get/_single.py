import inspect

from etl_utils.bug import (
    Bug,
)
from etl_utils.typing import (
    Dict,
    Tuple,
)
from fa_purity import (
    Cmd,
    FrozenTools,
    Maybe,
)
from fa_purity.json import (
    Primitive,
)
from redshift_client.core.id_objs import (
    DbTableId,
)
from redshift_client.sql_client import (
    DbPrimitiveFactory,
    QueryValues,
)
from snowflake_client import (
    SnowflakeCursor,
    SnowflakeQuery,
)

from code_etl.database.clients._registration.queries.init_table import (
    GROUP_COLUMN,
    REPO_COLUMN,
    SEEN_AT_COLUMN,
)
from code_etl.objs import (
    RepoId,
    RepoRegistration,
)

from . import (
    _decode,
)


def _query(
    table: DbTableId,
    repo: RepoId,
) -> Tuple[SnowflakeQuery, QueryValues]:
    statement = """
        SELECT {group_column},{repo_column},{seen_at_column}
        FROM {schema}.{table}
        WHERE {group_column} = %(group)s
        AND {repo_column} = %(repository)s
        LIMIT 1
    """
    identifiers: Dict[str, str] = {
        "group_column": GROUP_COLUMN.name.to_str(),
        "repo_column": REPO_COLUMN.name.to_str(),
        "seen_at_column": SEEN_AT_COLUMN.name.to_str(),
        "schema": table.schema.name.to_str(),
        "table": table.table.name.to_str(),
    }
    args: Dict[str, Primitive] = {
        "group": repo.group.name,
        "repository": repo.repository.name,
    }
    query = Bug.assume_success(
        "get_registration_query",
        inspect.currentframe(),
        (str(statement), str(identifiers)),
        SnowflakeQuery.dynamic_query(statement, FrozenTools.freeze(identifiers)),
    )
    return (
        query,
        QueryValues(DbPrimitiveFactory.from_raw_prim_dict(FrozenTools.freeze(args))),
    )


def get_registration(
    cursor: SnowflakeCursor,
    table: DbTableId,
    repo: RepoId,
) -> Cmd[Maybe[RepoRegistration]]:
    request = cursor.execute(*_query(table, repo)).map(
        lambda r: Bug.assume_success(
            "get_registration_request",
            inspect.currentframe(),
            (str(table), str(repo)),
            r,
        ),
    )
    fetch = cursor.fetch_one.map(
        lambda r: Bug.assume_success(
            "get_registration_fetch_result",
            inspect.currentframe(),
            (str(table), str(repo)),
            r,
        ),
    ).map(
        lambda m: m.map(
            lambda r: Bug.assume_success(
                "get_registration_decode",
                inspect.currentframe(),
                (str(table), str(repo)),
                _decode.decode_registration(r),
            ),
        ),
    )
    return request + fetch
