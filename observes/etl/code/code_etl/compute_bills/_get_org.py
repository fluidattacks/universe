from __future__ import (
    annotations,
)

import logging

from etl_utils.typing import (
    Optional,
)
from fa_purity import (
    Result,
    Unsafe,
)

from code_etl._utils.cache import (
    CacheMap,
)
from code_etl.integrates_api import (
    ApiError,
    IntegratesClient,
)
from code_etl.objs import (
    GroupId,
    OrgName,
)

LOG = logging.getLogger(__name__)
group_cache: CacheMap[GroupId, Result[OrgName, ApiError]] = Unsafe.compute(CacheMap.new())


def get_org(client: IntegratesClient, grp: GroupId) -> Optional[OrgName]:
    org = (
        group_cache.get_or_set(grp, client.get_org(grp))
        .alt(lambda e: LOG.error("Api call fail get_org(%s) i.e. %s", grp.name, e))
        .value_or(None)
    )
    LOG.debug("Org of %s is %s", grp, org)
    return org
