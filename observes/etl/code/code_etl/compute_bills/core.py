from dataclasses import (
    dataclass,
)

from etl_utils.typing import (
    FrozenSet,
    Tuple,
)
from fa_purity import (
    FrozenDict,
)

from code_etl.objs import (
    Contribution,
    GroupId,
    User,
)


@dataclass(frozen=True)
class ActiveUsersReport:
    data: FrozenDict[User, Contribution]


@dataclass(frozen=True)
class FinalActiveUsersReport:
    data: FrozenDict[User, Tuple[Contribution, FrozenSet[GroupId]]]


__all__ = ["Contribution"]
