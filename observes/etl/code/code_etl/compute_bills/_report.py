from functools import (
    reduce,
)

from etl_utils.typing import (
    Callable,
    Dict,
    FrozenSet,
    Set,
    Tuple,
    TypeVar,
)
from fa_purity import (
    Cmd,
    FrozenDict,
    FrozenTools,
    Stream,
    StreamTransform,
    UnionFactory,
)

from code_etl._utils import (
    Date,
)
from code_etl.compute_bills.core import (
    ActiveUsersReport,
    Contribution,
    FinalActiveUsersReport,
)
from code_etl.database import (
    BillingClient,
)
from code_etl.objs import (
    GroupId,
    User,
)

_T = TypeVar("_T")


def _bool_reduction(value: bool, case_true: Callable[[], _T], case_false: Callable[[], _T]) -> _T:
    if value:
        return case_true()
    return case_false()


def filter_by_first_seen(
    client: BillingClient,
    data: Stream[Contribution],
    target: Date,
) -> Stream[Contribution]:
    def contrib_filter(item: Contribution) -> Cmd[Contribution | None]:
        _factory: UnionFactory[Contribution, None] = UnionFactory()
        return client.get_commit_first_inserted_at(item).map(
            lambda m: _bool_reduction(
                m.map(
                    lambda f: not f.is_initial
                    and f.date.month == target.month
                    and f.date.year == target.year,
                ).value_or(True),
                lambda: _factory.inl(item),
                lambda: _factory.inr(None),
            ),
        )

    return data.map(contrib_filter).transform(
        lambda s: StreamTransform.filter_opt(StreamTransform.squash(s)),
    )


def _extract_active_users(
    catalog: Dict[User, Contribution],
    item: Contribution,
) -> Dict[User, Contribution]:
    if catalog.get(item.author) is None:
        catalog[item.author] = item
    return catalog


def extract_active_users(data: Stream[Contribution]) -> Cmd[ActiveUsersReport]:
    empty: Dict[User, Contribution] = {}
    return (
        data.reduce(_extract_active_users, empty)
        .map(lambda i: FrozenTools.freeze(i))
        .map(ActiveUsersReport)
    )


def _calc_user_groups(
    catalog: Dict[User, Set[GroupId]],
    item: Tuple[GroupId, ActiveUsersReport],
) -> Dict[User, Set[GroupId]]:
    for user in item[1].data:
        if catalog.get(user) is None:
            catalog[user] = {item[0]}
        else:
            catalog[user].add(item[0])
    return catalog


def calc_user_groups(
    reports: FrozenDict[GroupId, ActiveUsersReport],
) -> FrozenDict[User, FrozenSet[GroupId]]:
    empty: Dict[User, Set[GroupId]] = {}
    user_groups = reduce(_calc_user_groups, reports.items(), empty)
    return FrozenTools.freeze({k: frozenset(v) for k, v in user_groups.items()})


def calc_final_report(
    report: ActiveUsersReport,
    user_groups: FrozenDict[User, FrozenSet[GroupId]],
) -> FinalActiveUsersReport:
    draft = FrozenTools.freeze({u: (c, user_groups[u]) for u, c in report.data.items()})
    return FinalActiveUsersReport(draft)


def final_reports(
    reports: FrozenDict[GroupId, ActiveUsersReport],
) -> FrozenDict[GroupId, FinalActiveUsersReport]:
    user_groups = calc_user_groups(reports)
    return FrozenTools.freeze({k: calc_final_report(v, user_groups) for k, v in reports.items()})
