import logging
from pathlib import (
    Path,
)

from etl_utils.parallel import (
    ThreadPool,
)
from etl_utils.typing import (
    FrozenSet,
    Tuple,
)
from fa_purity import (
    Cmd,
    CmdTransform,
    FrozenDict,
    FrozenList,
    FrozenTools,
    PureIterFactory,
    Unsafe,
)
from snowflake_client import (
    SnowflakeConnection,
)

from code_etl._utils import (
    Date,
    log_info,
)
from code_etl.compute_bills.core import (
    ActiveUsersReport,
    FinalActiveUsersReport,
)
from code_etl.database import (
    BillingClient,
)
from code_etl.database.clients import (
    ClientFactory,
)
from code_etl.integrates_api import (
    IntegratesClient,
)
from code_etl.objs import (
    GroupId,
)

from ._keeper import (
    ReportKeeper,
)
from ._report import (
    extract_active_users,
    filter_by_first_seen,
    final_reports,
)

LOG = logging.getLogger(__name__)


def gen_final_reports(
    _new_client: Cmd[BillingClient],
    date: Date,
    groups: FrozenSet[GroupId],
    pool: ThreadPool,
) -> Cmd[FrozenDict[GroupId, FinalActiveUsersReport]]:
    def process_group(
        group: GroupId,
    ) -> Cmd[Tuple[GroupId, ActiveUsersReport]]:
        start = log_info(LOG, "Generating report for group: %s", group.name)
        extract = log_info(LOG, "Calculating active users of: %s", group.name)
        end = log_info(LOG, "Report for %s done!", group.name)
        return start + (
            _new_client.bind(lambda c: c.get_month_contributions(group, date))
            .bind(lambda x: _new_client.map(lambda c: filter_by_first_seen(c, x, date)))
            .bind(lambda x: extract + Cmd.wrap_value(x))
            .bind(extract_active_users)
            .map(lambda u: (group, u))
            .bind(lambda x: end + Cmd.wrap_value(x))
        )

    reports = PureIterFactory.pure_map(process_group, tuple(groups))
    start = log_info(LOG, "Generating final report...")
    return (
        pool.in_threads(reports)
        .transform(lambda s: s.to_list().map(lambda x: FrozenTools.freeze(dict(x))))
        .bind(lambda x: start + Cmd.wrap_value(x))
        .map(final_reports)
    )


def save_all(
    client: IntegratesClient,
    folder: Path,
    data: FrozenDict[GroupId, FinalActiveUsersReport],
) -> Cmd[None]:
    def _action(grp: GroupId, report: FinalActiveUsersReport) -> None:
        file_path = folder / (grp.name + ".csv")
        with file_path.open("w", encoding="UTF-8") as file:
            Unsafe.compute(ReportKeeper.new(file, client).save(grp, report))

    def _save(items: Tuple[GroupId, FinalActiveUsersReport]) -> Cmd[None]:
        return Cmd.wrap_impure(lambda: _action(items[0], items[1]))

    start = log_info(LOG, "Saving reports...")
    end = log_info(LOG, "Saved!")
    save_items: Cmd[FrozenList[None]] = CmdTransform.serial_merge(
        PureIterFactory.pure_map(_save, tuple(data.items())).to_list(),
    )
    return start + save_items + end


def main(
    connection: SnowflakeConnection,
    client: IntegratesClient,
    folder: Path,
    date: Date,
) -> Cmd[None]:
    factory = ClientFactory.production()
    create_client = connection.cursor(LOG.getChild("sub_client")).map(
        lambda s: factory.new_billing_client(s),
    )
    pool = ThreadPool.new(100)
    return create_client.bind(
        lambda c1: c1.get_month_repos(date)
        .bind(
            lambda i: log_info(LOG, "Contributing groups this month: %s", str(i))
            + Cmd.wrap_value(i),
        )
        .bind(
            lambda groups: pool.bind(
                lambda p: gen_final_reports(create_client, date, groups, p).bind(
                    lambda d: save_all(client, folder, d),
                ),
            ),
        ),
    )
