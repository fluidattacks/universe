from datetime import (
    timedelta,
)

from fa_purity import (
    Cmd,
)
from fa_purity.date_time import (
    DatetimeUTC,
)

from code_etl.database import (
    RegistrationClient,
)
from code_etl.objs import (
    RepoContext,
    RepoId,
)


def get_context(
    recorder: RegistrationClient,
    repo: RepoId,
    date_now: DatetimeUTC,
) -> Cmd[RepoContext]:
    is_new = recorder.get(repo).map(
        lambda m: m.map(
            lambda r: date_now.date_time - timedelta(days=2) < r.seen_at.date_time,
        ).value_or(True),
    )
    return is_new.map(lambda n: RepoContext(repo, None, n))
