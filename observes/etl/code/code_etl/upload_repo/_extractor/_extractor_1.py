import logging
from collections.abc import (
    Callable,
)
from dataclasses import (
    dataclass,
)

from etl_utils.typing import (
    Iterable,
    Tuple,
    TypeVar,
)
from fa_purity import (
    Cmd,
    Maybe,
    PureIter,
    PureIterTransform,
    Unsafe,
)
from fa_purity.date_time import (
    DatetimeUTC,
)
from git.objects.commit import (
    Commit as GitCommit,
)

from code_etl.amend.core import (
    AmendUsers,
)
from code_etl.database import (
    StampsClient,
)
from code_etl.mailmap import (
    Mailmap,
)
from code_etl.objs import (
    Commit,
    CommitDataId,
    CommitId,
    CommitStamp,
    RepoContext,
)

from ._core import (
    Extractor,
    ProtectedAccess,
    RepoObj,
)
from ._factory import (
    CommitDataFactory,
)

LOG = logging.getLogger(__name__)
_T = TypeVar("_T")

# [WARNING] seems like `GitCommit` is bad typed and allows
# incorrect assignations like: `x: Maybe[GitCommit] = some_git_commit`


def _items_after(
    items: PureIter[_T],
    criteria: Callable[[_T], bool],
    inclusive: bool,
) -> PureIter[_T]:
    def _gen(found: bool, current: _T) -> Tuple[bool, Maybe[_T]]:
        if criteria(current):
            if inclusive:
                return (True, Maybe.from_optional(current))
            return (True, Maybe.empty())
        if not found:
            return (found, Maybe.empty())
        return (found, Maybe.some(current))

    return items.generate(_gen, False).transform(PureIterTransform.filter_maybe)


def _items_before(
    items: PureIter[_T],
    criteria: Callable[[_T], bool],
    inclusive: bool,
) -> PureIter[_T]:
    def _gen(found: bool, current: _T) -> Tuple[bool, Maybe[_T]]:
        if criteria(current):
            if inclusive:
                return (True, Maybe.from_optional(current))
            return (True, Maybe.empty())
        if not found:
            return (found, Maybe.from_optional(current))
        return (found, Maybe.empty())

    return items.generate(_gen, False).transform(PureIterTransform.until_empty)


@dataclass(frozen=True)
class _Extractor1:
    _context: RepoContext  # deprecated
    _mailmap: Maybe[Mailmap]
    _date_now: DatetimeUTC
    _stamps: StampsClient
    _repo: RepoObj

    def _to_commit(self, commit: GitCommit) -> Commit:
        _obj = CommitDataFactory.from_commit(commit)
        obj = Commit(CommitDataId(self._context.repo, _obj.commit_id), _obj.data)
        return self._mailmap.map(AmendUsers).map(lambda a: a.amend_commit_users(obj)).value_or(obj)

    def _to_stamp(self, commit: GitCommit) -> CommitStamp:
        LOG.debug("_to_stamp commit %s", commit.hexsha)
        if self._context.is_new:
            return CommitStamp.initial_stamp(
                self._to_commit(commit),
                self._date_now,
            )
        return CommitStamp.normal_stamp(
            self._to_commit(commit),
            self._date_now,
            self._date_now,
        )

    @property
    def extract_all(self) -> PureIter[CommitStamp]:
        # using `unsafe_from_cmd` assumes the repository
        # is read-only/unmodified
        def _action() -> Iterable[GitCommit]:
            return self._repo.get_repo(ProtectedAccess()).iter_commits(
                no_merges=True,
                topo_order=True,
            )

        _commits: Cmd[Iterable[GitCommit]] = Cmd.wrap_impure(_action)
        return Unsafe.pure_iter_from_cmd(_commits).map(self._to_stamp)

    @property
    def extract_from_init(self) -> PureIter[Commit]:
        # using `unsafe_from_cmd` assumes the repository
        # is read-only/unmodified
        def _action() -> Iterable[GitCommit]:
            return self._repo.get_repo(ProtectedAccess()).iter_commits(
                no_merges=True,
                topo_order=True,
                reverse=True,
            )

        _commits: Cmd[Iterable[GitCommit]] = Cmd.wrap_impure(_action)
        return Unsafe.pure_iter_from_cmd(_commits).map(self._to_commit)

    def commits_between(self, start: CommitId, end: CommitId) -> PureIter[Commit]:
        _after = _items_after(self.extract_from_init, lambda c: c.commit_id.hash == start, False)
        return _items_before(_after, lambda c: c.commit_id.hash == end, False)

    def commits_after(
        self,
        start: CommitId,
    ) -> PureIter[Commit]:
        return _items_after(self.extract_from_init, lambda c: c.commit_id.hash == start, False)

    def commits_before(
        self,
        end: CommitId,
    ) -> PureIter[Commit]:
        return _items_before(self.extract_from_init, lambda c: c.commit_id.hash == end, False)

    def exist(self, commit: CommitId) -> bool:
        try:
            # not a cmd since the repo is read-only
            self._repo.get_repo(ProtectedAccess()).commit(commit.hash)
        except ValueError:
            return False
        else:
            return True

    def _last_commit_filter(self, commit: CommitStamp) -> Maybe[CommitStamp]:
        if commit.commit.commit_id.hash.hash == self._context.last_commit:
            LOG.debug("last commit reached")
            return Maybe.empty()
        return Maybe.some(commit)

    def ids_after(self, checkpoint: CommitId) -> PureIter[CommitDataId]:
        return _items_after(
            self.extract_from_init,
            lambda c: c.commit_id.hash == checkpoint,
            False,
        ).map(lambda c: c.commit_id)

    @property
    def extract_until_last_commit(self) -> PureIter[CommitStamp]:
        return self.extract_all.map(self._last_commit_filter).transform(
            PureIterTransform.until_empty,
        )

    @property
    def head_commit_id(self) -> CommitId:
        for commit in self._repo.get_repo(ProtectedAccess()).iter_commits(
            no_merges=True,
            topo_order=True,
        ):
            return CommitId(commit.hexsha)
        msg = f"There is no final commit! i.e. {self._context}"
        raise ValueError(msg)

    @property
    def init_commit_id(self) -> CommitId:
        for commit in self._repo.get_repo(ProtectedAccess()).iter_commits(
            no_merges=True,
            topo_order=True,
            reverse=True,
        ):
            return CommitId(commit.hexsha)
        msg = f"There is no initial commit! i.e. {self._context}"
        raise ValueError(msg)


def new_extractor(
    context: RepoContext,
    mailmap: Maybe[Mailmap],
    date_now: DatetimeUTC,
    stamps: StampsClient,
    repo: RepoObj,
) -> Extractor:
    obj = _Extractor1(
        context,
        mailmap,
        date_now,
        stamps,
        repo,
    )
    return Extractor(
        obj.extract_from_init,
        obj.commits_between,
        obj.commits_after,
        obj.commits_before,
        obj.init_commit_id,
        obj.head_commit_id,
        obj.exist,
        obj.ids_after,
    )
