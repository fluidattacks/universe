from __future__ import (
    annotations,
)

from dataclasses import (
    dataclass,
    field,
)
from pathlib import (
    Path,
)

from etl_utils.typing import (
    Callable,
)
from fa_purity import (
    PureIter,
    ResultE,
    ResultFactory,
)
from git.exc import (
    InvalidGitRepositoryError,
)
from git.repo.base import (
    Repo,
)

from code_etl.objs import Commit, CommitDataId, CommitId, RepoName

StartCommitId = CommitId
EndCommitId = CommitId


@dataclass(frozen=True)
class _Private:
    pass


@dataclass(frozen=True)
class ProtectedAccess:
    pass


@dataclass(frozen=True)
class RepoObj:
    _private: _Private = field(repr=False, hash=False, compare=False)
    _repo: Repo
    path: Path
    name: RepoName

    def get_repo(self, _: ProtectedAccess) -> Repo:
        return self._repo

    @staticmethod
    def to_repo(raw: Path) -> ResultE[RepoObj]:
        factory: ResultFactory[RepoObj, Exception] = ResultFactory()
        try:
            return factory.success(RepoObj(_Private(), Repo(raw), raw, RepoName(raw.name)))
        except InvalidGitRepositoryError as err:
            return factory.failure(err)


@dataclass(frozen=True)
class Extractor:
    from_init: PureIter[Commit]
    commits_between: Callable[[StartCommitId, EndCommitId], PureIter[Commit]]
    commits_after: Callable[[CommitId], PureIter[Commit]]
    commits_before: Callable[[CommitId], PureIter[Commit]]
    init_commit_id: CommitId
    head_commit_id: CommitId
    exist: Callable[[CommitId], bool]
    ids_after: Callable[[CommitId], PureIter[CommitDataId]]
