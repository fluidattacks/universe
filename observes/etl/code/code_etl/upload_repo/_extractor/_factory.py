import inspect
from dataclasses import (
    dataclass,
)

from etl_utils.bug import (
    Bug,
)
from fa_purity import (
    Maybe,
    ResultE,
)
from fa_purity.date_time import (
    DatetimeFactory,
    DatetimeTZ,
)
from fa_purity.json import (
    JsonPrimitiveFactory,
    JsonPrimitiveUnfolder,
)
from git.objects.commit import (
    Commit,
)

from code_etl.objs import (
    CommitData,
    CommitDataObj,
    CommitId,
    Deltas,
    User,
)
from code_etl.str_utils import (
    truncate,
)


def _assert_str(raw: str | None) -> ResultE[str]:
    return JsonPrimitiveUnfolder.to_str(JsonPrimitiveFactory.from_raw(raw))


@dataclass(frozen=True)
class CommitDataFactory:
    @classmethod
    def from_commit(cls, commit: Commit) -> CommitDataObj:
        author = User(
            Bug.assume_success(
                "author_name",
                inspect.currentframe(),
                (str(commit.author.name),),
                _assert_str(commit.author.name),
            ),
            Bug.assume_success(
                "author_email",
                inspect.currentframe(),
                (str(commit.author.email),),
                _assert_str(commit.author.email),
            ),
        )
        committer = User(
            Bug.assume_success(
                "committer_name",
                inspect.currentframe(),
                (str(commit.committer.name),),
                _assert_str(commit.committer.name),
            ),
            Bug.assume_success(
                "committer_email",
                inspect.currentframe(),
                (str(commit.committer.email),),
                _assert_str(commit.committer.email),
            ),
        )
        deltas = Deltas(
            commit.stats.total["insertions"],
            commit.stats.total["deletions"],
            commit.stats.total["lines"],
            commit.stats.total["files"],
        )
        files = frozenset(str(f) for f in commit.stats.files)
        data = CommitData(
            author,
            Bug.assume_success(
                "decode_authored_at",
                inspect.currentframe(),
                (str(commit.authored_datetime),),
                DatetimeTZ.assert_tz(commit.authored_datetime).map(DatetimeFactory.to_utc),
            ),
            committer,
            Bug.assume_success(
                "decode_committed_at",
                inspect.currentframe(),
                (str(commit.committed_datetime),),
                DatetimeTZ.assert_tz(commit.committed_datetime).map(DatetimeFactory.to_utc),
            ),
            truncate(str(commit.message), 4096),
            truncate(str(commit.summary), 256),
            deltas,
            Maybe.some(files),
        )
        _id = CommitId(commit.hexsha)
        return CommitDataObj(_id, data)
