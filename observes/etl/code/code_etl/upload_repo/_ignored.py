import fnmatch
import logging
from dataclasses import (
    dataclass,
)

from etl_utils.typing import (
    FrozenSet,
)
from fa_purity import (
    Maybe,
    PureIterFactory,
)

from code_etl.integrates_api import (
    IgnoredPath,
)
from code_etl.objs import (
    CommitStamp,
)

LOG = logging.getLogger(__name__)


def _path_match(path: str, ignored: str) -> bool:
    if ignored in ["/", "*", "/*"]:
        return False
    return ignored != "" and (
        path.lstrip("/*").startswith(ignored.lstrip("/*"))
        or fnmatch.fnmatch(path.lstrip("/"), ignored)
        or fnmatch.fnmatch("/" + path.lstrip("/"), ignored)
    )


def _ignored_path(path: str, ignored: FrozenSet[str]) -> bool:
    return any(_path_match(path, i) for i in ignored)


@dataclass(frozen=True)
class IgnoredFilter:
    ignored: FrozenSet[IgnoredPath]

    def _ignored_paths(self, stamp: CommitStamp) -> FrozenSet[IgnoredPath]:
        ignored = PureIterFactory.from_list(tuple(self.ignored)).filter(
            lambda i: i.nickname == stamp.commit.commit_id.repo.repository.name
            and i.group == stamp.commit.commit_id.repo.group,
        )

        return frozenset(ignored)

    def filter_stamp(self, stamp: CommitStamp) -> Maybe[CommitStamp]:
        ignored = frozenset(i.file_path for i in self._ignored_paths(stamp))
        ignored_stamp = stamp.commit.data.files.map(
            lambda files: all(_ignored_path(f, ignored) for f in files),
        ).value_or(False)
        if ignored_stamp:
            LOG.debug(
                "Stamp ignored because it only modifies ignored paths i.e. %s",
                stamp.commit.commit_id.hash,
            )
        return Maybe.from_optional(None if ignored_stamp else stamp)
