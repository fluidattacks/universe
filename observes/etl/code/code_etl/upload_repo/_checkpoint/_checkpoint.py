from __future__ import (
    annotations,
)

import inspect
import logging
from collections.abc import (
    Callable,
)
from dataclasses import (
    dataclass,
)

from etl_utils.bug import (
    Bug,
)
from etl_utils.typing import (
    Literal,
)
from fa_purity import (
    Cmd,
    Maybe,
)

from code_etl.database import (
    DbCheckpointClient,
    StampsClient,
)
from code_etl.objs import (
    Checkpoint,
    RepoId,
)

from ._emitted import (
    EmittedCommitId,
)

_TUpsertOperation = Literal["upsert_start", "upsert_end", "upsert_checkpoint"]
_TDeleteOperation = Literal["remove_start", "remove_end"]


LOG = logging.getLogger(__name__)


def get_checkpoint(
    client: StampsClient,
    item: Maybe[EmittedCommitId],
) -> Cmd[Maybe[Checkpoint]]:
    return item.map(
        lambda c: client.get_stamp(c.commit_id)
        .map(
            lambda m: Bug.assume_success(
                "get_candidate",
                inspect.currentframe(),
                (str(item),),
                m.to_result(),
                # this should not fail since the existence of the
                # `CommitDataId` was proved before;
                # if this fails is due to a race condition + a delete/action over the data
            ),
        )
        .map(lambda s: Maybe.some(Checkpoint(s.commit.commit_id, s.is_initial, s.inserted_at))),
    ).value_or(Cmd.wrap_value(Maybe.empty(Checkpoint)))


@dataclass(frozen=True)
class CheckpointClient:
    _db_checkpoint: DbCheckpointClient

    @property
    def set_main_checkpoint(self) -> Callable[[Checkpoint], Cmd[None]]:
        return self._db_checkpoint.set_checkpoint

    @property
    def get_main_checkpoint(
        self,
    ) -> Callable[[RepoId], Cmd[Maybe[Checkpoint]]]:
        return self._db_checkpoint.get_checkpoint

    @property
    def get_start_progress(
        self,
    ) -> Callable[[RepoId], Cmd[Maybe[Checkpoint]]]:
        return self._db_checkpoint.get_start_point_search_progress

    @staticmethod
    def new(db_checkpoint: DbCheckpointClient) -> CheckpointClient:
        return CheckpointClient(db_checkpoint)

    def upsert_checkpoint(
        self,
        checkpoint: Checkpoint,
        operation: _TUpsertOperation,
    ) -> Cmd[Checkpoint]:
        operations: dict[_TUpsertOperation, Callable[[Checkpoint], Cmd[None]]] = {
            "upsert_start": self._db_checkpoint.set_start_point_search_progress,
            "upsert_end": self._db_checkpoint.set_end_point_search_progress,
            "upsert_checkpoint": self._db_checkpoint.set_checkpoint,
        }

        _operation = operations[operation]

        return (
            _operation(checkpoint)
            .map(lambda _: LOG.info("%s  `%s`", operation, checkpoint))
            .map(
                lambda _: checkpoint,
            )
        )

    def remove_checkpoint(
        self,
        checkpoint: Checkpoint,
        operation: _TDeleteOperation,
    ) -> Cmd[None]:
        operations: dict[_TDeleteOperation, Callable[[RepoId], Cmd[None]]] = {
            "remove_start": self._db_checkpoint.delete_start_point_search_progress,
            "remove_end": self._db_checkpoint.delete_end_point_search_progress,
        }

        _operation = operations[operation]

        return _operation(checkpoint.commit_id.repo).map(
            lambda _: LOG.info("%s `%s`", operation, checkpoint),
        )
