from __future__ import (
    annotations,
)

import logging
from collections.abc import (
    Iterable,
)
from dataclasses import (
    dataclass,
    field,
)
from datetime import (
    date,
)

from etl_utils.typing import (
    Callable,
    TypeVar,
)
from fa_purity import (
    Cmd,
    CmdUnwrapper,
    PureIter,
    Result,
    ResultE,
    ResultFactory,
    Stream,
    Unsafe,
)

_T = TypeVar("_T")
_S = TypeVar("_S")
_A = TypeVar("_A")
_F = TypeVar("_F")
LOG = logging.getLogger(__name__)


def log_info(log: logging.Logger, msg: str, *args: str) -> Cmd[None]:
    return Cmd.wrap_impure(lambda: log.info(msg, *args))


@dataclass(frozen=True)
class _Private:
    pass


@dataclass(frozen=True)
class Date:
    """Purified `date` wrapper."""

    _private: _Private = field(repr=False, hash=False, compare=False)
    _inner: date
    year: int
    month: int
    day: int

    @staticmethod
    def new(year: int, month: int, day: int) -> ResultE[Date]:
        try:
            obj = date(year, month, day)
            return Result.success(Date(_Private(), obj, obj.year, obj.month, obj.day))
        except ValueError as err:
            return Result.failure(Exception(err))

    def strftime(self, _format: str) -> str:
        return self._inner.strftime(_format)


def append_to_stream(stream: Stream[_T], item: _T) -> Stream[_T]:
    def _iter(unwrapper: CmdUnwrapper) -> Iterable[_T]:
        yield from unwrapper.act(Unsafe.stream_to_iter(stream))
        yield item

    new_iter = Cmd.new_cmd(_iter)
    return Unsafe.stream_from_cmd(new_iter)


def join_stream(stream_1: Stream[_T], stream_2: Stream[_T]) -> Stream[_T]:
    def _iter(unwrapper: CmdUnwrapper) -> Iterable[_T]:
        yield from unwrapper.act(Unsafe.stream_to_iter(stream_1))
        yield from unwrapper.act(Unsafe.stream_to_iter(stream_2))

    new_iter = Cmd.new_cmd(_iter)
    return Unsafe.stream_from_cmd(new_iter)


def chain_cmd_result(
    cmd_1: Cmd[Result[_S, _F]],
    cmd_2: Callable[[_S], Cmd[Result[_A, _F]]],
) -> Cmd[Result[_A, _F]]:
    factory: ResultFactory[_A, _F] = ResultFactory()
    return cmd_1.bind(
        lambda r: r.map(cmd_2).alt(lambda e: Cmd.wrap_value(factory.failure(e))).to_union(),
    )


def consume_results(commands: PureIter[Cmd[Result[None, _F]]]) -> Cmd[Result[None, _F]]:
    def _action(unwrapper: CmdUnwrapper) -> Result[None, _F]:
        for cmd in commands:
            result = unwrapper.act(cmd)
            success = result.map(lambda _: True).value_or(False)
            if not success:
                return result
        return Result.success(None)

    return Cmd.new_cmd(_action)


def return_first_error(
    stream: Stream[Result[_S, _F]],
    default_success: Result[_S, _F],
) -> Cmd[Result[_S, _F]]:
    def _action(unwrapper: CmdUnwrapper) -> Result[_S, _F]:
        items = unwrapper.act(Unsafe.stream_to_iter(stream))
        for i in items:
            success = i.map(lambda _: True).value_or(False)
            if not success:
                return i
        return default_success

    return Cmd.new_cmd(_action)
