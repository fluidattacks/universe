from __future__ import (
    annotations,
)

from collections.abc import (
    Callable,
)
from dataclasses import (
    dataclass,
)

from etl_utils.typing import (
    Dict,
    TypeVar,
)
from fa_purity import (
    Cmd,
    Coproduct,
    Result,
    ResultE,
    cast_exception,
)

_T = TypeVar("_T")


@dataclass(frozen=True)
class RateLimitPeriod:
    @dataclass(frozen=True)
    class _Private:
        pass

    _private: RateLimitPeriod._Private
    value: int

    @staticmethod
    def from_raw(period: int) -> ResultE[RateLimitPeriod]:
        if period >= 0:
            return Result.success(RateLimitPeriod(RateLimitPeriod._Private(), period))
        return Result.failure(ValueError(f"Period must be >= 0 but got {period}")).alt(
            cast_exception,
        )


@dataclass(frozen=True)
class RateLimitState:
    _state: Coproduct[None, int]

    @staticmethod
    def init() -> RateLimitState:
        return RateLimitState(Coproduct.inl(None))

    @staticmethod
    def reset() -> RateLimitState:
        return RateLimitState(Coproduct.inr(0))

    def map(self, init_case: Callable[[], _T], count_case: Callable[[int], _T]) -> _T:
        return self._state.map(lambda _: init_case(), count_case)


@dataclass(frozen=True)
class RateLimitMutableState:
    @dataclass(frozen=True)
    class _Private:
        pass

    _private: RateLimitMutableState._Private  # pylint: disable=unused-private-member
    _inner: Dict[None, RateLimitState]

    @staticmethod
    def new() -> Cmd[RateLimitMutableState]:
        return Cmd.wrap_impure(
            lambda: RateLimitMutableState(
                RateLimitMutableState._Private(),
                {None: RateLimitState(Coproduct.inl(None))},
            ),
        )

    @property
    def get(self) -> Cmd[RateLimitState]:
        return Cmd.wrap_impure(lambda: self._inner[None])

    def set(self, state: RateLimitState) -> Cmd[None]:
        def _action() -> None:
            self._inner[None] = state

        return Cmd.wrap_impure(_action)


def limit_and_ignore_cmd(
    state: RateLimitMutableState,
    command: Cmd[None],
    period: RateLimitPeriod,
) -> Cmd[None]:
    return state.get.bind(
        lambda s: s.map(
            lambda: command + state.set(RateLimitState.reset()),
            lambda p: state.set(RateLimitState(Coproduct.inr(p + 1)))
            if p < period.value
            else command + state.set(RateLimitState.reset()),
        ),
    )
