from __future__ import (
    annotations,
)

from dataclasses import (
    dataclass,
    field,
)

from etl_utils.typing import (
    Dict,
    Generic,
    TypeVar,
)
from fa_purity import (
    Cmd,
    Unsafe,
)

_T = TypeVar("_T")
_R = TypeVar("_R")


@dataclass(frozen=True)
class _Private:
    pass


@dataclass(frozen=True)
class CacheMap(Generic[_T, _R]):
    _private: _Private = field(repr=False, hash=False, compare=False)
    _cache: Dict[_T, _R]

    @staticmethod
    def new() -> Cmd[CacheMap[_T, _R]]:
        return Cmd.wrap_impure(lambda: CacheMap(_Private(), {}))

    def get_or_set(self, key: _T, cmd: Cmd[_R]) -> _R:
        if key not in self._cache:
            self._cache[key] = Unsafe.compute(cmd)
        return self._cache[key]
