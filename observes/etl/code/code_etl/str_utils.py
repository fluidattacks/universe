from __future__ import (
    annotations,
)

from dataclasses import (
    dataclass,
    field,
)

from etl_utils.typing import (
    Generic,
    Literal,
    TypeVar,
)
from fa_purity import (
    Result,
    ResultE,
    Unsafe,
    cast_exception,
)


def _utf8_lead_byte(byte: int) -> bool:
    """
    Check if the byte is an intermediate byte.

    A UTF-8 intermediate byte starts with the bits 10xxxxxx.
    """
    start_of_lead_byte = 0b10000000
    return (byte & 0b11000000) == start_of_lead_byte


class NotUTF8(Exception):
    pass


class TruncationError(Exception):
    pass


def _lead_bytes_of(byte: int) -> ResultE[int]:
    """Get UTF-8 lead bytes of char given the first byte."""
    start_of_2_byte_char = 0b11000000
    start_of_3_byte_char = 0b11100000
    start_of_4_byte_char = 0b11110000
    if (byte & 0b10000000) == 0:
        return Result.success(0)
    if (byte & 0b11100000) == start_of_2_byte_char:
        return Result.success(1)
    if (byte & 0b11110000) == start_of_3_byte_char:
        return Result.success(2)
    if (byte & 0b11111000) == start_of_4_byte_char:
        return Result.success(3)
    return Result.failure(NotUTF8(f"byte: {byte}")).alt(cast_exception)


def _search_last_not_lead_byte(raw: bytes) -> int:
    last = len(raw) - 1
    for i in range(len(raw)):
        if not _utf8_lead_byte(raw[last - i]):
            return last - i
    msg = "Unexpected: not lead byte not found"
    raise ValueError(msg)


def utf8_byte_truncate(text: str, max_bytes: int) -> str:
    if max_bytes < 0:
        msg = "max_bytes must be >= 0"
        raise ValueError(msg)
    if max_bytes == 0:
        return ""
    utf8 = text.encode("utf8")
    if len(utf8) <= max_bytes:
        return utf8.decode("utf8")
    pre_truncated = utf8[:max_bytes]
    last_not_lead_index = _search_last_not_lead_byte(pre_truncated)
    last_lead_bytes = max_bytes - 1 - last_not_lead_index
    last_not_lead_byte = pre_truncated[last_not_lead_index]
    try:
        if (
            _lead_bytes_of(last_not_lead_byte).alt(Unsafe.raise_exception).to_union()
            == last_lead_bytes
        ):
            # char bytes fits in the truncation
            return pre_truncated.decode("utf8")
        # discard incomplete char
        return pre_truncated[:last_not_lead_index].decode("utf8")
    except NotUTF8 as err:
        msg = (
            "utf8_byte_truncate(text, max_bytes)"
            f"\ntext={text}\nmax_bytes={max_bytes}"
            f"\npre_truncated={pre_truncated!r}"
            f"\nlast_not_lead_byte={last_not_lead_byte}"
        )
        raise TruncationError(msg) from err


_L = TypeVar("_L", Literal[64], Literal[256], Literal[4096])


@dataclass(frozen=True)
class _Private:
    pass


@dataclass(frozen=True)
class TruncatedStr(Generic[_L]):
    _private: _Private = field(repr=False, hash=False, compare=False)
    _length: _L
    msg: str

    @staticmethod
    def truncate(raw: str, limit: _L) -> TruncatedStr[_L]:
        return TruncatedStr(_Private(), limit, utf8_byte_truncate(raw, limit))


def truncate(raw: str, limit: _L) -> TruncatedStr[_L]:
    return TruncatedStr.truncate(raw, limit)
