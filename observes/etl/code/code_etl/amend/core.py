from dataclasses import (
    dataclass,
)

from fa_purity import (
    Maybe,
)

from code_etl.database import (
    UsersPatch,
)
from code_etl.mailmap import (
    Mailmap,
)
from code_etl.objs import (
    Commit,
    CommitData,
    CommitStamp,
    User,
)


@dataclass(frozen=True)
class AmendUsers:
    mailmap: Mailmap

    def _amend_user(self, user: User) -> Maybe[User]:
        return Maybe.from_optional(self.mailmap.alias_map.get(user))

    def _get_patch(self, raw: Commit) -> Maybe[UsersPatch]:
        fixed_author = self._amend_user(raw.data.author).value_or(raw.data.author)
        fixed_committer = self._amend_user(raw.data.committer).value_or(raw.data.committer)
        if fixed_author == raw.data.author and fixed_committer == raw.data.committer:
            return Maybe.empty()
        patch = UsersPatch(raw.commit_id, fixed_author, fixed_committer)
        return Maybe.some(patch)

    def get_users_patch(self, raw: CommitStamp) -> Maybe[UsersPatch]:
        return self._get_patch(raw.commit)

    def amend_commit_users(self, raw: Commit) -> Commit:
        patch = self._get_patch(raw)
        data = CommitData(
            patch.map(lambda p: p.author).value_or(raw.data.author),
            raw.data.authored_at,
            patch.map(lambda p: p.committer).value_or(raw.data.committer),
            raw.data.committed_at,
            raw.data.message,
            raw.data.summary,
            raw.data.deltas,
            raw.data.files,
        )
        return Commit(raw.commit_id, data)

    def amend_commit_stamp_users(self, raw: CommitStamp) -> CommitStamp:
        data = self.amend_commit_users(raw.commit)
        if raw.is_initial:
            return CommitStamp.initial_stamp(
                Commit(raw.commit.commit_id, data.data),
                raw.inserted_at,
            )
        return CommitStamp.normal_stamp(
            Commit(raw.commit.commit_id, data.data),
            raw.seen_at,
            raw.inserted_at,
        )
