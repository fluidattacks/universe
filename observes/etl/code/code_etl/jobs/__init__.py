import os

from etl_utils.typing import (
    NoReturn,
)
from fa_purity import (
    Cmd,
)
from fa_purity.date_time import (
    DatetimeFactory,
)

from code_etl.integrates_api import (
    IntegratesToken,
)
from code_etl.objs import (
    GroupId,
)

from ._utils import (
    amend,
    upload,
)
from ._utils.upload import (
    send_uploads_per_root,
    send_uploads_per_root_snowflake,
)


def amend_groups_on_aws() -> NoReturn:
    cmd: Cmd[None] = amend.send_jobs()
    cmd.compute()


def upload_groups_on_aws() -> NoReturn:
    scheduled_at = DatetimeFactory.date_now()
    cmd = scheduled_at.bind(upload.send_jobs)
    cmd.compute()


def upload_roots_on_aws() -> NoReturn:
    scheduled_at = DatetimeFactory.date_now()
    token = IntegratesToken.new(os.environ["INTEGRATES_API_TOKEN"])
    cmd = scheduled_at.bind(lambda date: send_uploads_per_root(token, GroupId("grupoi"), date))
    cmd.compute()


def upload_roots_on_aws_snowflake() -> NoReturn:
    scheduled_at = DatetimeFactory.date_now()
    token = IntegratesToken.new(os.environ["INTEGRATES_API_TOKEN"])
    cmd = scheduled_at.bind(lambda date: send_uploads_per_root_snowflake(token, date, False))
    cmd.compute()
