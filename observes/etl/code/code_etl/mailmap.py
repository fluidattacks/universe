import inspect
import re
from dataclasses import (
    dataclass,
)
from pathlib import Path

from etl_utils.bug import (
    Bug,
)
from etl_utils.typing import (
    Pattern,
)
from fa_purity import (
    FrozenDict,
    FrozenList,
    Maybe,
    ResultE,
    ResultTransform,
    cast_exception,
)

from code_etl.objs import (
    User,
)

Alias = User


@dataclass(frozen=True)
class MailmapItem:
    canon: User
    alias: User

    def encode(self) -> str:
        return f"{self.canon.name} <{self.canon.email}> {self.alias.name} <{self.alias.email}>"


@dataclass(frozen=True)
class Mailmap:
    alias_map: FrozenDict[Alias, User]


def _decode_user(item: FrozenDict[str, str | None], name_key: str, email_key: str) -> ResultE[User]:
    _name = ResultTransform.try_get(item, name_key).bind(
        lambda m: Maybe.from_optional(m)
        .to_result()
        .alt(lambda _: ValueError("Expected not None value for " + name_key)),
    )
    _email = ResultTransform.try_get(item, email_key).bind(
        lambda m: Maybe.from_optional(m)
        .to_result()
        .alt(lambda _: ValueError("Expected not None value for " + email_key)),
    )
    return _name.bind(lambda name: _email.map(lambda email: User(name, email)))


def _decode_mailmap_item(item: FrozenDict[str, str | None]) -> ResultE[MailmapItem]:
    _cannon = _decode_user(item, "canon_name", "canon_email")
    _alias = _decode_user(item, "alias_name", "alias_email")
    return _cannon.bind(lambda cannon: _alias.map(lambda alias: MailmapItem(cannon, alias)))


class MailmapFactory:
    @staticmethod
    def from_line(line: str) -> ResultE[MailmapItem]:
        mailmap_line: Pattern[str] = re.compile(
            r"^(?P<canon_name>[A-Z][a-z]+ [A-Z][a-z]+) "
            r"<(?P<canon_email>.*)> "
            r"(?P<alias_name>.*?) "
            r"<(?P<alias_email>.*?)>$",
        )
        match = (
            Maybe.from_optional(mailmap_line.match(line))
            .to_result()
            .alt(lambda _: ValueError("Match obj is None"))
            .alt(cast_exception)
        )
        return match.map(lambda m: m.groupdict(None)).bind(
            lambda d: _decode_mailmap_item(FrozenDict(d)),
        )

    @classmethod
    def from_lines(cls, lines: FrozenList[str]) -> Mailmap:
        mailmap_dict = {}
        for line in lines:
            item = Bug.assume_success(
                "from_lines_decode_mailmap_item",
                inspect.currentframe(),
                (line,),
                cls.from_line(line),
            )
            if item and item.canon != item.alias:
                mailmap_dict[item.alias] = item.canon
        return Mailmap(FrozenDict(mailmap_dict))

    @classmethod
    def from_file_path(cls, mailmap_path: str) -> Mailmap:
        with Path(mailmap_path).open(encoding="UTF-8") as file:
            return cls.from_lines(tuple(file.read().splitlines()))
