from etl_utils.secrets import (
    ObservesSecrets,
)
from etl_utils.typing import (
    Callable,
    Tuple,
)
from fa_purity import (
    Cmd,
    FrozenDict,
    Result,
)

from code_etl import (
    _utils,
)
from code_etl.mailmap import (
    Mailmap,
)
from code_etl.objs import (
    GroupId,
    OrgId,
    OrgName,
)

from ._error import (
    ApiResult,
)


def get_group_mailmap(
    get_org: Callable[[GroupId], Cmd[ApiResult[OrgName]]],
    get_mailmap: Callable[[OrgId | OrgName], Cmd[ApiResult[Mailmap]]],
    secrets: ObservesSecrets,
    group: GroupId,
) -> Cmd[ApiResult[Mailmap]]:
    def concatenate_mailmaps(mailmaps: Tuple[Mailmap, Mailmap]) -> Mailmap:
        return Mailmap(FrozenDict({**dict(mailmaps[0].alias_map), **dict(mailmaps[1].alias_map)}))

    fluid_mailmap = secrets.integrates_fluid_org_id.map(lambda s: OrgId(s.value)).bind(
        lambda org: get_mailmap(org),
    )
    org_mailmap = _utils.chain_cmd_result(get_org(group), get_mailmap)
    return _utils.chain_cmd_result(
        fluid_mailmap,
        lambda fluid_map: _utils.chain_cmd_result(
            org_mailmap,
            lambda org_map: Cmd.wrap_value(
                concatenate_mailmaps(
                    (
                        fluid_map,
                        org_map,
                    ),
                ),
            ).map(lambda x: Result.success(x)),
        ),
    )
