from etl_utils.typing import (
    Callable,
    TypeVar,
)
from fa_purity import (
    Cmd,
    PureIterFactory,
    Result,
    StreamFactory,
    Unsafe,
)

_S = TypeVar("_S")
_F = TypeVar("_F")


class MaxRetriesReached(Exception):
    pass


def retry_cmd(
    cmd: Cmd[Result[_S, _F]],
    next_cmd: Callable[[int, Result[_S, _F]], Cmd[Result[_S, _F]]],
    max_retries: int,
) -> Cmd[Result[_S, MaxRetriesReached]]:
    cmds = PureIterFactory.from_range(range(max_retries + 1)).map(
        lambda i: cmd.bind(lambda r: next_cmd(i + 1, r)),
    )
    return (
        StreamFactory.from_commands(cmds)
        .find_first(lambda x: x.map(lambda _: True).alt(lambda _: False).to_union())
        .map(
            lambda x: x.map(
                lambda r: r.alt(
                    lambda _: Unsafe.raise_exception(
                        ValueError("Impossible. Result was ensured to have a value"),
                    ),
                ).to_union(),
            )
            .to_result()
            .alt(lambda _: MaxRetriesReached(max_retries)),
        )
    )
