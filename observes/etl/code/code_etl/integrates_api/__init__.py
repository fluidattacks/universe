from __future__ import (
    annotations,
)

import inspect
from collections.abc import (
    Callable,
)
from dataclasses import (
    dataclass,
)

from etl_utils.bug import (
    Bug,
)
from etl_utils.secrets import (
    ObservesSecrets,
    ObservesSecretsFactory,
)
from etl_utils.typing import (
    FrozenSet,
)
from fa_purity import (
    Cmd,
)

from code_etl.mailmap import (
    Mailmap,
)
from code_etl.objs import (
    GroupId,
    OrgId,
    OrgName,
    RepoId,
)

from . import (
    _group_mailmap,
    _group_org,
    _ignored_paths,
    _mailmap,
    _roots,
)
from ._core import (
    IgnoredPath,
    RootNickname,
)
from ._error import (
    ApiError,
    ApiResult,
)
from ._raw_client import (
    GraphQlAsmClient,
)


@dataclass(frozen=True)
class _IntegratesToken:
    token: str


@dataclass(frozen=True)
class _Private:
    pass


@dataclass(frozen=True)
class IntegratesToken:
    _inner: _IntegratesToken

    @staticmethod
    def new(token: str) -> IntegratesToken:
        return IntegratesToken(_IntegratesToken(token))

    def get(self, _key: _Private) -> str:
        # key is to disable that token can be getted anywhere
        return self._inner.token

    def __repr__(self) -> str:
        return "[masked]"


@dataclass(frozen=True)
class IntegratesClient:
    get_ignored_paths: Callable[[RepoId], Cmd[ApiResult[FrozenSet[IgnoredPath]]]]
    get_org: Callable[[GroupId], Cmd[ApiResult[OrgName]]]
    get_roots: Callable[[GroupId], Cmd[ApiResult[FrozenSet[RootNickname]]]]
    get_mailmap: Callable[[OrgId | OrgName], Cmd[ApiResult[Mailmap]]]
    get_group_mailmap: Callable[[GroupId], Cmd[ApiResult[Mailmap]]]


@dataclass(frozen=True)
class IntegratesClientFactory:
    @staticmethod
    def _new_client(client: GraphQlAsmClient, secrets: ObservesSecrets) -> IntegratesClient:
        def get_org(group: GroupId) -> Cmd[ApiResult[OrgName]]:
            return _group_org.get_org(client, group)

        def get_mailmap(org: OrgId | OrgName) -> Cmd[ApiResult[Mailmap]]:
            return _mailmap.get_mailmap(client, org)

        return IntegratesClient(
            get_ignored_paths=lambda repo_id: _ignored_paths.get_ignored_paths(client, repo_id),
            get_org=get_org,
            get_roots=lambda g: _roots.get_roots(client, g),
            get_mailmap=get_mailmap,
            get_group_mailmap=lambda g: _group_mailmap.get_group_mailmap(
                get_org,
                get_mailmap,
                secrets,
                g,
            ),
        )

    @classmethod
    def new_client(cls, token: IntegratesToken) -> Cmd[IntegratesClient]:
        _secrets = ObservesSecretsFactory.from_env().map(
            lambda r: Bug.assume_success("observes_secrets", inspect.currentframe(), (), r),
        )
        return _secrets.bind(
            lambda secrets: GraphQlAsmClient.new(token.get(_Private())).map(
                lambda client: cls._new_client(client, secrets),
            ),
        )


__all__ = [
    "ApiError",
    "ApiResult",
    "IgnoredPath",
    "Mailmap",
    "RootNickname",
]
