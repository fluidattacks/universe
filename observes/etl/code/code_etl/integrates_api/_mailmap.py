import inspect

from etl_utils.bug import (
    Bug,
)
from fa_purity import (
    Cmd,
    FrozenDict,
    FrozenList,
    FrozenTools,
    PureIterFactory,
    Result,
    ResultE,
    ResultTransform,
)
from fa_purity.json import (
    JsonObj,
    JsonPrimitiveUnfolder,
    JsonUnfolder,
    Unfolder,
)

from code_etl.mailmap import (
    Mailmap,
    MailmapItem,
)
from code_etl.objs import (
    OrgId,
    OrgName,
    User,
)

from ._error import (
    ApiError,
)
from ._raw_client import (
    GraphQlAsmClient,
)


def _decode_entry_to_canon(raw: JsonObj) -> ResultE[User]:
    entry_name = JsonUnfolder.require(
        raw,
        "mailmapEntryName",
        lambda v: Unfolder.to_primitive(v).bind(
            JsonPrimitiveUnfolder.to_str,
        ),
    )
    entry_email = JsonUnfolder.require(
        raw,
        "mailmapEntryEmail",
        lambda v: Unfolder.to_primitive(v).bind(
            JsonPrimitiveUnfolder.to_str,
        ),
    )
    return entry_name.bind(
        lambda name: entry_email.map(
            lambda email: User(
                name=name,
                email=email,
            ),
        ),
    )


def _decode_canon(raw: JsonObj) -> ResultE[User]:
    return JsonUnfolder.require(
        raw,
        "entry",
        lambda v: Unfolder.to_json(v).bind(_decode_entry_to_canon),
    )


def _decode_subentry_to_alias(raw: JsonObj) -> ResultE[User]:
    subentry_name = JsonUnfolder.require(
        raw,
        "mailmapSubentryName",
        lambda v: Unfolder.to_primitive(v).bind(
            JsonPrimitiveUnfolder.to_str,
        ),
    )
    subentry_email = JsonUnfolder.require(
        raw,
        "mailmapSubentryEmail",
        lambda v: Unfolder.to_primitive(v).bind(
            JsonPrimitiveUnfolder.to_str,
        ),
    )
    return subentry_name.bind(lambda name: subentry_email.map(lambda email: User(name, email)))


def _decode_aliases(raw: JsonObj) -> ResultE[FrozenList[User]]:
    return JsonUnfolder.require(
        raw,
        "subentries",
        lambda v: Unfolder.to_list_of(
            v,
            lambda v: Unfolder.to_json(v).bind(_decode_subentry_to_alias),
        ),
    )


def _decode_partial_mailmap(raw: JsonObj) -> ResultE[Mailmap]:
    canon = _decode_canon(raw)
    aliases = _decode_aliases(raw)

    return canon.bind(
        lambda _canon: aliases.map(
            lambda _aliases: PureIterFactory.from_list(_aliases)
            .map(
                lambda _alias: MailmapItem(
                    canon=_canon,
                    alias=_alias,
                ),
            )
            .map(
                lambda item: (item.alias, item.canon),
            )
            .transform(lambda t: Mailmap(FrozenDict(dict(t)))),
        ),
    )


def _merge_partial_mailmaps(partial_mailmaps: FrozenList[Mailmap]) -> Mailmap:
    merged_dict: dict[User, User] = PureIterFactory.from_list(partial_mailmaps).reduce(
        lambda acc, pm: acc | dict(pm.alias_map),
        {},
    )
    return Mailmap(FrozenDict(merged_dict))


def _decode_mailmap(raw: JsonObj) -> ResultE[Mailmap]:
    return (
        ResultTransform.try_get(raw, "mailmapEntriesWithSubentries")
        .bind(
            lambda v: Unfolder.to_list_of(
                v,
                lambda j: Unfolder.to_json(j).bind(_decode_partial_mailmap),
            ),
        )
        .map(_merge_partial_mailmaps)
    )


def get_mailmap(
    client: GraphQlAsmClient,
    organization_id: OrgId | OrgName,
) -> Cmd[Result[Mailmap, ApiError]]:
    query = """
        query ObservesGetMailmap(
            $organizationId: String!,
        ) {
            mailmapEntriesWithSubentries(
                organizationId: $organizationId,
            ) {
                entry {
                    mailmapEntryName
                    mailmapEntryEmail
                }
                subentries {
                    mailmapSubentryName
                    mailmapSubentryEmail
                }
            }
        }
    """
    variables = {"organizationId": organization_id.name}

    return client.get(query, FrozenTools.freeze(variables)).map(
        lambda r: r.map(
            lambda j: Bug.assume_success(
                "decode_mailmap",
                inspect.currentframe(),
                (str(variables),),
                _decode_mailmap(raw=j),
            ),
        ),
    )
