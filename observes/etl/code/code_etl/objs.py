from __future__ import (
    annotations,
)

from dataclasses import (
    dataclass,
    field,
)

from etl_utils.typing import (
    FrozenSet,
    Literal,
    Optional,
)
from fa_purity import (
    Maybe,
)
from fa_purity.date_time import (
    DatetimeFactory,
    DatetimeUTC,
)

from code_etl.str_utils import (
    TruncatedStr,
)


@dataclass(frozen=True)
class GroupId:
    """a.k.a. namespace."""

    name: str


@dataclass(frozen=True)
class OrgId:
    name: str


@dataclass(frozen=True)
class OrgName:
    name: str


@dataclass(frozen=True)
class RepoName:
    name: str


@dataclass(frozen=True)
class RepoId:
    group: GroupId
    repository: RepoName


@dataclass(frozen=True)
class CommitId:
    # relative to repo
    hash: str


@dataclass(frozen=True)
class User:
    name: str
    email: str


@dataclass(frozen=True)
class Deltas:
    total_insertions: int
    total_deletions: int
    total_lines: int
    total_files: int


@dataclass(frozen=True)
class CommitData:
    author: User
    authored_at: DatetimeUTC
    committer: User
    committed_at: DatetimeUTC
    message: TruncatedStr[Literal[4096]]
    summary: TruncatedStr[Literal[256]]
    deltas: Deltas
    files: Maybe[FrozenSet[str]]


@dataclass(frozen=True)
class CommitDataObj:
    commit_id: CommitId
    data: CommitData


@dataclass(frozen=True)
class CommitDataId:
    repo: RepoId
    hash: CommitId


@dataclass(frozen=True)
class Commit:
    commit_id: CommitDataId
    data: CommitData


@dataclass(frozen=True)
class CommitStamp:
    @dataclass(frozen=True)
    class _Private:
        pass

    _private: CommitStamp._Private = field(repr=False, hash=False, compare=False)
    commit: Commit
    is_initial: bool
    seen_at: DatetimeUTC
    inserted_at: DatetimeUTC

    @staticmethod
    def initial_stamp(commit: Commit, inserted_at: DatetimeUTC) -> CommitStamp:
        return CommitStamp(
            CommitStamp._Private(),
            commit,
            True,
            DatetimeFactory.EPOCH_START,
            inserted_at,
        )

    @staticmethod
    def normal_stamp(commit: Commit, seen_at: DatetimeUTC, inserted_at: DatetimeUTC) -> CommitStamp:
        return CommitStamp(CommitStamp._Private(), commit, False, seen_at, inserted_at)


@dataclass(frozen=True)
class DbCommitStamp:
    stamp: CommitStamp
    was_in_a_gap: bool


@dataclass(frozen=True)
class FileStampRelation:
    commit_id: CommitDataId
    file_path: str


@dataclass(frozen=True)
class RepoRegistration:
    repo: RepoId
    seen_at: DatetimeUTC


@dataclass(frozen=True)
class RepoContext:
    repo: RepoId
    last_commit: Optional[str]
    is_new: bool


@dataclass(frozen=True)
class Contribution:
    author: User
    authored_at: DatetimeUTC
    message: TruncatedStr[Literal[4096]]
    commit_id: CommitDataId


@dataclass(frozen=True)
class Checkpoint:
    commit_id: CommitDataId
    is_initial: bool
    inserted_at: DatetimeUTC
