from __future__ import (
    annotations,
)

from ._core import (
    SheetId,
)
from .bin_sdk.tap import (
    TapConfig,
    TapGoogleSheets,
)
from .utils import (
    PkgPath,
)
from .utils.merge import (
    StdoutToQueue,
)
from .utils.process import (
    RunningSubprocess,
    StdValues,
)
from enum import (
    Enum,
)
from fa_purity import (
    Cmd,
    FrozenDict,
    Maybe,
    PureIter,
    Result,
    ResultE,
)
from fa_purity.pure_iter import (
    PureIterFactory,
)
from fa_purity.utils import (
    raise_exception,
)
from google_sheets_etl.bin_sdk.target import (
    TargetWarehouse,
    TargetWarehouseConf,
)
from google_sheets_etl.utils.pkg_path import (
    PkgPath,
)
from google_sheets_etl.utils.threads import (
    ThreadPool,
)
import logging
from pathlib import (
    Path,
)
import sys

LOG = logging.getLogger(__name__)


class TargetSheet(Enum):
    SHEET_1 = "SHEET_1"
    SHEET_2 = "SHEET_2"

    @staticmethod
    def from_raw(raw: str) -> ResultE[TargetSheet]:
        try:
            return Result.success(TargetSheet(raw.lower()))
        except ValueError as err:
            return Result.failure(Exception(err))


def catalog_map(sheet: TargetSheet) -> PkgPath:
    if sheet is TargetSheet.SHEET_1:
        return PkgPath.new(Path("catalogs/sheet_1.json"))
    if sheet is TargetSheet.SHEET_2:
        return PkgPath.new(Path("catalogs/sheet_2.json"))


def discover(conf: TapConfig, sheet: SheetId) -> Cmd[None]:
    return (
        TapGoogleSheets.new(conf, sheet)
        .discover(sys.stdout, sys.stderr)
        .map(lambda r: r.unwrap())
    )


def _start_tap(
    conf: TapConfig,
    sheet_ids_map: FrozenDict[str, SheetId],
    sheet: TargetSheet,
) -> Cmd[RunningSubprocess[str]]:
    raw: str = sheet.value
    sheet_id = Maybe.from_optional(sheet_ids_map.get(raw.lower())).unwrap()
    tap = TapGoogleSheets.new(conf, sheet_id).emit(
        StdValues.PIPE, sys.stderr, catalog_map(sheet).path
    )
    return RunningSubprocess.run_universal_newlines(tap)


def emit(
    conf: TapConfig,
    conf_2: TargetWarehouseConf,
    sheet_ids_map: FrozenDict[str, SheetId],
    sheets: PureIter[TargetSheet],
) -> Cmd[None]:
    def _pools_context(
        main_pool: ThreadPool, second_pool: ThreadPool, stq: StdoutToQueue
    ) -> Cmd[None]:
        output_from_queue = RunningSubprocess.run_universal_newlines(
            TargetWarehouse(conf_2).destroy_and_upload(
                StdValues.PIPE, sys.stderr, "google_sheets"
            )
        ).bind(
            lambda p: stq.consume_queue(Maybe.from_optional(p.stdin).unwrap())
            + p.wait_result(None).map(
                lambda r: r.alt(raise_exception).to_union()
            )
        )
        return main_pool.in_threads(
            PureIterFactory.from_list(
                (output_from_queue, stq.emit_to_queue(second_pool))
            )
        )

    taps = sheets.map(lambda s: _start_tap(conf, sheet_ids_map, s))
    return ThreadPool.new(2).bind(
        lambda pool_1: ThreadPool.new(30).bind(
            lambda pool_2: StdoutToQueue.new(taps).bind(
                lambda stq: _pools_context(pool_1, pool_2, stq)
            )
        )
    )
