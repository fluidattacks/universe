from fa_purity import (
    ResultE,
)
from fa_purity.json_2 import (
    JsonObj,
    JsonPrimitiveUnfolder,
    JsonUnfolder,
    Unfolder,
)
from google_sheets_etl.bin_sdk.tap import (
    TapConfig,
)


def decode_conf(raw: JsonObj) -> ResultE[TapConfig]:
    def get_str(key: str) -> ResultE[str]:
        return JsonUnfolder.require(raw, key, Unfolder.to_primitive).bind(
            JsonPrimitiveUnfolder.to_str
        )

    def get_int(key: str) -> ResultE[int]:
        return JsonUnfolder.require(raw, key, Unfolder.to_primitive).bind(
            JsonPrimitiveUnfolder.to_int
        )

    return get_str("client_id").bind(
        lambda client_id: get_str("client_secret").bind(
            lambda client_secret: get_str("refresh_token").bind(
                lambda refresh_token: get_str("start_date").bind(
                    lambda start_date: get_str("user_agent").bind(
                        lambda user_agent: get_int("request_timeout").map(
                            lambda request_timeout: TapConfig(
                                client_id,
                                client_secret,
                                refresh_token,
                                start_date,
                                user_agent,
                                request_timeout,
                            )
                        )
                    )
                )
            )
        )
    )
