from fa_purity import (
    Cmd,
    FrozenDict,
)
from fa_purity.json_2 import (
    JsonObj,
    UnfoldedFactory,
)
from fa_purity.utils import (
    raise_exception,
)
from google_sheets_etl.utils.process import (
    RunningSubprocess,
    StdValues,
    Subprocess,
)
import os
from pathlib import (
    Path,
)
from typing import (
    IO,
    TypeVar,
)

_T = TypeVar("_T")


def _decrypt(file_path: Path) -> Cmd[IO[str]]:
    def _assert_not_none(item: _T | None) -> _T:
        if item is not None:
            return item
        raise TypeError("Unexpected None")

    return RunningSubprocess.run_universal_newlines(
        Subprocess(
            (
                "sops",
                "--aws-profile",
                "default",
                "--decrypt",
                "--output-type",
                "json",
                file_path.as_posix(),
            ),
            None,
            StdValues.PIPE,
            None,
            FrozenDict(dict(os.environ)),
        )
    ).map(lambda p: _assert_not_none(p.stdout))


def get_secret(file_path: Path) -> Cmd[JsonObj]:
    return (
        _decrypt(file_path)
        .map(UnfoldedFactory.load)
        .map(lambda r: r.alt(raise_exception).unwrap())
    )
