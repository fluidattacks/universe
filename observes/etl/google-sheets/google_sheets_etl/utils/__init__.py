from .pkg_path import (
    PkgPath,
)
from fa_purity import (
    FrozenList,
    Maybe,
)
from typing import (
    TypeVar,
)

_T = TypeVar("_T")


def get_index(items: FrozenList[_T], index: int) -> Maybe[_T]:
    try:
        return Maybe.from_value(items[index])
    except IndexError:
        return Maybe.empty()


__all__ = [
    "PkgPath",
]
