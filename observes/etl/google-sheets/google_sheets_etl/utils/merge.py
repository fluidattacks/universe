from __future__ import (
    annotations,
)

from .process import (
    RunningSubprocess,
)
from dataclasses import (
    dataclass,
    field,
)
from fa_purity import (
    Cmd,
    FrozenList,
    PureIter,
)
from fa_purity.cmd import (
    CmdUnwrapper,
)
from fa_purity.utils import (
    raise_exception,
)
from google_sheets_etl.utils.threads import (
    ThreadLock,
    ThreadPool,
)
import logging
from queue import (
    Queue,
)
from typing import (
    IO,
)

LOG = logging.getLogger(__name__)


@dataclass(frozen=True)
class _Private:
    pass


@dataclass
class _Status:
    _end: bool

    def ended(self) -> Cmd[bool]:
        return Cmd.from_cmd(lambda: self._end)

    def set_status(self, status: bool) -> Cmd[None]:
        def _action() -> None:
            self._end = status

        return Cmd.from_cmd(_action)


@dataclass(frozen=True)
class StdoutToQueue:
    """
    Util for:
    - merge subprocess outputs to a common lines queue
    - centralize (consume) the queue into an IO[str] object
    """

    _private: _Private = field(repr=False, hash=False, compare=False)
    lock: ThreadLock
    queue: Queue[str]
    _emitters_status: FrozenList[_Status]
    _emitters: PureIter[Cmd[RunningSubprocess[str]]]

    @staticmethod
    def new(
        emitters: PureIter[Cmd[RunningSubprocess[str]]],
    ) -> Cmd[StdoutToQueue]:
        "[WARNING] The supplied emitters should not wait the process termination, only trigger it"
        emitters_status = emitters.map(lambda _: _Status(False)).to_list()
        return ThreadLock.new().map(
            lambda lock: StdoutToQueue(
                _Private(), lock, Queue(), emitters_status, emitters
            )
        )

    def _emit_to_queue(self, line: str) -> Cmd[None]:
        return self.lock.execute_with_lock(
            Cmd.from_cmd(lambda: self.queue.put(line))
        )

    def _emit_stdout_to_queue(
        self, process: Cmd[RunningSubprocess[str]], ended: _Status
    ) -> Cmd[None]:
        def _action(
            unwrap: CmdUnwrapper, process: RunningSubprocess[str]
        ) -> None:
            if process.stdout:
                line = process.stdout.readline()
                while line:
                    unwrap.act(self._emit_to_queue(line))
                    line = process.stdout.readline()

        return process.bind(
            lambda p: Cmd.new_cmd(lambda u: _action(u, p))
            + p.wait_result(None)
            .bind(
                lambda r: ended.set_status(True).map(lambda _: r)
                # [WARNING] set status to ended before raising exception
                # if status is not updated, queue consumer may get stuck in
                # an infinite loop
            )
            .map(lambda r: r.alt(raise_exception).to_union())
        )

    def all_ended(self) -> Cmd[bool]:
        "Termination status of all the processes emissions, only true when all have finish"

        def _action(unwrapper: CmdUnwrapper) -> bool:
            return all(unwrapper.act(e.ended()) for e in self._emitters_status)

        return Cmd.new_cmd(_action)

    def emit_to_queue(self, pool: ThreadPool) -> Cmd[None]:
        """
        [WARNING] an indirect output is the input queue
        This command will:
        - run each `RunningSubprocess` in a separate thread
        - for each process/thread: it will emit each line of the process stdout to the queue
        - mutate the corresponding `_Status` of each process
          to `ended = True` when emission is completed
        """
        emissions = self._emitters.enumerate(0).map(
            lambda p: self._emit_stdout_to_queue(
                p[1], self._emitters_status[p[0]]
            )
        )
        return pool.in_threads(emissions)

    def consume_queue(self, target: IO[str]) -> Cmd[None]:
        """
        This will consume the queue and emit its items to the supplied target IO[str]
        """

        def _action(unwrapper: CmdUnwrapper) -> None:
            LOG.debug("Consume queue started")
            while (
                not unwrapper.act(self.all_ended()) or not self.queue.empty()
            ):
                self.lock.acquire()
                if not self.queue.empty():
                    item = self.queue.get()
                    LOG.debug("From queue: %s", item)
                    target.write(item)
                self.lock.release()
            LOG.debug("Queue end!")
            target.close()

        return Cmd.new_cmd(_action)
