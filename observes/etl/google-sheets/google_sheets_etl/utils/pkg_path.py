from __future__ import (
    annotations,
)

from dataclasses import (
    dataclass,
    field,
)
from pathlib import (
    Path,
)


@dataclass(frozen=True)
class _Private:
    pass


@dataclass(frozen=True)
class PkgPath:
    _private: _Private = field(repr=False, hash=False, compare=False)
    path: Path

    @staticmethod
    def new(relative: Path) -> PkgPath:
        return PkgPath(_Private(), Path(__file__).parent.parent / relative)
