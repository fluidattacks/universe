from __future__ import (
    annotations,
)

from dataclasses import (
    dataclass,
)
from fa_purity import (
    FrozenDict,
)
from google_sheets_etl.utils.process import (
    StdValues,
    Subprocess,
)
import os
from typing import (
    Dict,
    IO,
)
@dataclass(frozen=True)
class TargetWarehouseConf:
    pass
@dataclass(frozen=True)
class TargetWarehouse:
    conf: TargetWarehouseConf
    def destroy_and_upload(
        self,
        _input: StdValues | IO[str] | None,
        errors: StdValues | IO[str] | None,
        db_schema: str,
    ) -> Subprocess[str]:
        args = (
            "target-warehouse",
            "destroy-and-upload",
            "--schema-name",
            db_schema,
            "--ignore-failed",
            "--truncate",
        )
        db_conf: Dict[str, str] = { }
        external: Dict[str, str] = dict(os.environ)
        return Subprocess(
            args, _input, None, errors, FrozenDict(external | db_conf)
        )
