from __future__ import (
    annotations,
)

from dataclasses import (
    dataclass,
    field,
)
from fa_purity import (
    Cmd,
    FrozenDict,
    Result,
    ResultE,
)
from fa_purity.json_2 import (
    JsonObj,
    Primitive,
    UnfoldedFactory,
    Unfolder,
)
from fa_purity.pure_iter.factory import (
    from_flist,
)
from google_sheets_etl._core import (
    SheetId,
)
from google_sheets_etl.utils.cache import (
    Cache,
)
from google_sheets_etl.utils.process import (
    RunningSubprocess,
    StdValues,
    Subprocess,
)
from google_sheets_etl.utils.temp_file import (
    TempFile,
)
import os
from pathlib import (
    Path,
)
from typing import (
    IO,
)


@dataclass(frozen=True)
class _Private:
    pass


@dataclass(frozen=True)
class TapConfig:
    client_id: str
    client_secret: str
    refresh_token: str
    start_date: str
    user_agent: str
    request_timeout: int

    def to_json(self, sheet: SheetId) -> JsonObj:
        data: FrozenDict[str, Primitive] = FrozenDict(
            {
                "client_id": self.client_id,
                "client_secret": self.client_secret,
                "refresh_token": self.refresh_token,
                "spreadsheet_id": sheet.raw,
                "start_date": self.start_date,
                "user_agent": self.user_agent,
                "request_timeout": self.request_timeout,
            }
        )
        return UnfoldedFactory.from_dict(data)

    def to_file(self, sheet: SheetId) -> Cmd[TempFile]:
        return TempFile.new().bind(
            lambda f: f.write_lines(
                from_flist((Unfolder.dumps(self.to_json(sheet)),))
            ).map(lambda _: f)
        )

    def __repr__(self) -> str:
        return "TapConfig: [masked]"


@dataclass(frozen=True)
class TapGoogleSheets:
    _private: _Private = field(repr=False, hash=False, compare=False)
    cache: Cache[TempFile] = field(repr=False, hash=False, compare=False)
    config: TapConfig
    sheet: SheetId

    @staticmethod
    def new(config: TapConfig, sheet: SheetId) -> TapGoogleSheets:
        return TapGoogleSheets(_Private(), Cache(None), config, sheet)

    @property
    def config_file_path(self) -> Path:
        return self.cache.get_or_set(self.config.to_file(self.sheet)).path

    def discover(
        self,
        out: IO[str] | None,
        errors: IO[str] | None,
    ) -> Cmd[ResultE[None]]:
        cmd = (
            "tap-google-sheets",
            "--config",
            self.config_file_path.resolve().as_posix(),
            "--discover",
        )
        process = RunningSubprocess.run_universal_newlines(
            Subprocess(cmd, None, out, errors, FrozenDict(dict(os.environ))),
        )
        return_code = process.bind(lambda p: p.wait(None))
        return return_code.map(
            lambda c: Result.success(None)
            if c == 0
            else Result.failure(
                Exception(f"Process ended with return code: {c}")
            )
        )

    def emit(
        self,
        out: StdValues | IO[str] | None,
        errors: StdValues | IO[str] | None,
        catalog: Path,
    ) -> Subprocess[str]:
        args = (
            "tap-google-sheets",
            "--config",
            self.config_file_path.resolve().as_posix(),
            "--catalog",
            catalog.resolve().as_posix(),
        )
        return Subprocess(
            args, None, out, errors, FrozenDict(dict(os.environ))
        )
