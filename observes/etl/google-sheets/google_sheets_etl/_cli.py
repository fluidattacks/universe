from __future__ import (
    annotations,
)

from . import (
    _actions,
    utils,
)
from ._actions import (
    TargetSheet,
)
from .bin_sdk.tap import (
    TapConfig,
)
from .secrets import (
    SecretManager,
)
from enum import (
    Enum,
)
from fa_purity import (
    Cmd,
    FrozenDict,
    Result,
    ResultE,
)
from fa_purity.pure_iter import (
    PureIterFactory,
)
from fa_purity.utils import (
    raise_exception,
)
from google_sheets_etl._core import (
    SheetId,
)
from google_sheets_etl.bin_sdk.target import (
    TargetWarehouseConf,
)
import logging
import sys
from typing import (
    NoReturn,
)

LOG = logging.getLogger(__name__)


class EtlCommand(Enum):
    discover = "discover"
    emit = "emit"
    help = "--help"

    @staticmethod
    def from_raw(raw: str) -> ResultE[EtlCommand]:
        try:
            return Result.success(EtlCommand(raw.lower()))
        except ValueError as err:
            return Result.failure(Exception(err))


def execute(
    extract_conf: Cmd[TapConfig],
    extract_conf_2: Cmd[TargetWarehouseConf],
    get_sheet_ids: Cmd[FrozenDict[str, SheetId]],
    command: EtlCommand,
) -> Cmd[None]:
    if command is EtlCommand.discover:
        sheet = utils.get_index(tuple(sys.argv), 2).map(SheetId).unwrap()
        return extract_conf.bind(lambda c: _actions.discover(c, sheet))
    if command is EtlCommand.emit:
        return extract_conf.bind(
            lambda c: extract_conf_2.bind(
                lambda c2: get_sheet_ids.bind(
                    lambda d: _actions.emit(
                        c, c2, d, PureIterFactory.from_list(tuple(TargetSheet))
                    )
                )
            )
        )
    if command is EtlCommand.help:
        items: str = ",".join(i.value for i in EtlCommand)
        msg = f"Available commands: {items}"
        return Cmd.from_cmd(lambda: LOG.info(msg))


def main() -> NoReturn:
    selection = (
        EtlCommand.from_raw(
            utils.get_index(tuple(sys.argv), 1).value_or("--help")
        )
        .alt(raise_exception)
        .unwrap()
    )
    cmd: Cmd[None] = execute(
        SecretManager.tap_conf(),
        SecretManager.target_conf(),
        SecretManager.get_sheet_ids(),
        selection,
    )
    cmd.compute()
