{ inputs, makeScript, projectPath, ... }@makes_inputs:
let
  root = projectPath inputs.observesIndex.etl.google_sheets.root;
  bundle = import "${root}/entrypoint.nix" makes_inputs;
  env = bundle.env.runtime;
in makeScript {
  name = "google-sheets-etl";
  searchPaths = { bin = [ env ]; };
  entrypoint = ''google-sheets-etl "''${@}"'';
}
