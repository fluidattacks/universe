from fa_purity import (
    Cmd,
)
from fa_purity.cmd import (
    unsafe_unwrap,
)
from fx_tests import (
    get_conf,
)
from google_sheets_etl._core import (
    SheetId,
)
from google_sheets_etl.bin_sdk.tap import (
    TapGoogleSheets,
)
from google_sheets_etl.utils.io_file import (
    IOAdapter,
)
from google_sheets_etl.utils.temp_file import (
    TempFile,
)
import os


def test_conf() -> None:
    assert get_conf.get_conf()
    assert get_conf.get_db_conf()


def test_discovery() -> None:
    tmp = TempFile.new()
    sheet = SheetId(os.environ["FX_TEST_SHEET_ID"])
    cmd: Cmd[None] = tmp.bind(
        lambda f: f.write(
            lambda h: IOAdapter.unsafe_io(
                h,
                lambda io: TapGoogleSheets.new(get_conf.get_conf(), sheet)
                .discover(io, None)
                .map(lambda r: r.unwrap())
                + Cmd.from_cmd(lambda: print(f.path)),
            )
        )
    )
    unsafe_unwrap(cmd)
