{ makes_inputs, nixpkgs, python_version, src, }:
let
  deps = import ./deps { inherit makes_inputs nixpkgs python_version; };
  bin_deps = with deps.nixpkgs; [ tap-google-sheets target-warehouse sops ];
  pkgDeps = {
    runtime_deps = bin_deps
      ++ (with deps.python_pkgs; [ fa-purity pathos utils-logger ]);
    build_deps = with deps.python_pkgs; [ flit-core ];
    test_deps = with deps.python_pkgs; [
      arch-lint
      mypy
      pylint
      pytest
      pytest-cov
    ];
  };
  packages = makes_inputs.makePythonPyprojectPackage {
    inherit (deps.lib) buildEnv buildPythonPackage;
    inherit pkgDeps src;
  };
in packages // { inherit bin_deps; }
