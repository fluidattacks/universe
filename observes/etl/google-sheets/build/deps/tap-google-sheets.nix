{ lib, makes_inputs, nixpkgs, }:
let
  root = makes_inputs.projectPath
    makes_inputs.inputs.observesIndex.tap.google_sheets.root;
  bundle = import "${root}/entrypoint.nix" makes_inputs;
  utils = import ./bin_utils.nix;
in utils.wrap_binary {
  name = "wrapped-tap-google-sheets";
  binary = "${bundle.pkg}/bin/tap-google-sheets";
  inherit nixpkgs;
}
