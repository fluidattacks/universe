# shellcheck shell=bash

function gitlab_dora_etl {
  export AWS_DEFAULT_REGION="us-east-1"
  local bucket="fluidattacks.public.storage"
  local local_folder=".gitlab_dora_temp_dir"
  local destination_folder="dora_metrics"

  : \
    && aws_login "prod_observes" "3600" \
    && info "Running tap" \
    && mkdir -p "${local_folder}" \
    && sops_export_vars "${SECRETS_FILE}" \
      OBSERVES_UNIVERSE_API_TOKEN \
    && tap-gitlab-dora --json > "${local_folder}/dora_metrics.json" \
    && aws s3 sync "${local_folder}/" "s3://${bucket}/${destination_folder}/" --acl public-read \
    && rm -rf "${local_folder}" \
    && success-indicators \
      single-job \
      --job "gitlab_dora" \
    || return 1
}

gitlab_dora_etl
