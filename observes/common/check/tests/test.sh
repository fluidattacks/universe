# shellcheck shell=bash

function main {
  export COVERAGE_FILE=.coverage.observes.__argName__
  echo "$ Executing __argPkgRoot__ tests"
  local pytest_args=(
    ./tests
    --cov '__argPkgName__'
    --cov-branch
    --cov-report 'term'
    --cov-report 'html:build/coverage/html'
    --cov-report 'xml:coverage.xml'
    --cov-report 'annotate:build/coverage/annotate'
    --disable-warnings
    --no-cov-on-fail
    -vvl
  )

  : && pushd __argPkgRoot__ \
    && pytest "${pytest_args[@]}" \
    && popd \
    || return 1
}

main "${@}"
