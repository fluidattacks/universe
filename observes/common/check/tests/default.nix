{ nixpkgs, makes_inputs, pkg_index_item
, # compatible with pkg items at standard 4
}:
let
  root = makes_inputs.projectPath pkg_index_item.root;
  bundle = import "${root}/entrypoint.nix" makes_inputs;
  inherit (bundle.check) tests;
  env = bundle.env.dev;
  coverage_script = makes_inputs.makeTemplate {
    name = "coverage-script-${pkg_index_item.name}";
    replace = {
      __argPkgRoot__ = nixpkgs.lib.removePrefix "/" pkg_index_item.root;
      __argName__ = pkg_index_item.name;
      __argPkgName__ = pkg_index_item.pkg_name;
    };
    template = ./test.sh;
  };
in makes_inputs.makeScript {
  searchPaths = { bin = [ tests env ]; };
  name = "observes-${pkg_index_item.name}-check-tests";
  entrypoint = "${coverage_script}/template";
}
