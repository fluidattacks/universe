{ pythonOverrideUtils }:
let
  # Python override utils.
  # Useful when overriding pkgs from an environment to ensure no collisions
  # PythonOverride = PythonPkgDerivation -> PythonPkgDerivation
  # DerivationOverride = Derivation -> Derivation
  _is_python_pkg = d: d ? overridePythonAttrs && d ? pname;
  superficial_override = override: pkg:
    let
      # If `pkg` is a python package: apply the override over each
      # of its dependencies. Else return the same pkg
      #
      # override: PythonOverride| DerivationOverride
      # pkg: PythonPkgDerivation | Derivation
      # return: PythonPkgDerivation | Derivation
      override_inputs = value:
        if builtins.isList value then map override value else override value;
    in if _is_python_pkg pkg then
      pkg.overridePythonAttrs (builtins.mapAttrs (name: value:
        if builtins.elem name [
          "nativeCheckInputs"
          "propagatedBuildInputs"
        ] then
          override_inputs value
        else
          value))
    else
      pkg;
  deep_override = is_pkg: override:
    let
      # Apply the supplied override over the pkgs that evaluates `is_pkg` to True
      # This is applied over all the python dependency tree (all sub-dependencies).
      # is_pkg: Derivation -> Bool
      # override: PythonOverride
      # return: PythonOverride
      self = deep_override is_pkg override;
      result = pkg:
        if is_pkg pkg then override pkg else superficial_override self pkg;
    in result;
  superficial_pkg_override = new_pkg:
    let
      # new_pkg: PythonPkgDerivation
      # return: PythonOverride
      is_pkg = pkg: _is_python_pkg pkg && pkg.pname == new_pkg.pname;
      override = pkg: if is_pkg pkg then new_pkg else pkg;
    in superficial_override override;
  deep_pkg_override = new_pkg:
    let
      # new_pkg: PythonPkgDerivation
      # return: PythonOverride
      is_pkg = pkg: _is_python_pkg pkg && pkg.pname == new_pkg.pname;
    in deep_override is_pkg (_: new_pkg);
  apply_deep_pkg_overrides = pkgs:
    let
      # pkgs: List[PythonPkgDerivation]
      # return: PythonOverride
      overrides = map deep_pkg_override pkgs; # : List[PythonOverride]
    in pythonOverrideUtils.compose overrides;
  # no_check_override: PythonOverride
  no_check_override = pkg:
    pkg.overridePythonAttrs (old: old // { doCheck = false; });

in {
  inherit superficial_override deep_override;
  inherit superficial_pkg_override deep_pkg_override apply_deep_pkg_overrides;
  inherit no_check_override;
}
