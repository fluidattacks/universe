# shellcheck shell=bash

function common_aws_region {
  export AWS_DEFAULT_REGION="us-east-1"
}

function export_notifier_key {
  sops_export_vars 'observes/secrets/prod.yaml' \
    bugsnag_notifier_key
}
