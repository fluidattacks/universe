{ inputs, makePythonVscodeSettings, projectPath, ... }@makes_inputs:
let
  root = projectPath inputs.observesIndex.common.utils_logger_2.root;
  bundle = import "${root}/entrypoint.nix" makes_inputs;
in makePythonVscodeSettings {
  env = bundle.env.runtime;
  bins = [ ];
  name = "observes-common-utils-logger-env-2-runtime";
}
