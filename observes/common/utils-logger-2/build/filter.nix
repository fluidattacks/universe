path_filter: src:
path_filter {
  root = src;
  include = [ "utils_logger" "tests" "pyproject.toml" "mypy.ini" ".pylintrc" ];
}
