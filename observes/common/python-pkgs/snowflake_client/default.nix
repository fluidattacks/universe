{ lib, makes_inputs, nixpkgs, python_pkgs, python_version, }:
let
  make_bundle = commit: sha256:
    let
      raw_src = builtins.fetchTarball {
        inherit sha256;
        url =
          "https://gitlab.com/dmurciaatfluid/snowflake_client/-/archive/${commit}/snowflake_client-${commit}.tar";
      };
      src = import "${raw_src}/build/filter.nix" nixpkgs.nix-filter raw_src;
    in import "${raw_src}/build" {
      inherit makes_inputs nixpkgs python_version src;
    };
in {
  "v1.1.7" = make_bundle "f0b9cb608ff7283dbfc0eb732d4f2e31ab2511d0"
    "1rjh21094s6ckp433fql82z9kgnd45ybrdgdrjmqsaz8ywybjd32";
  "v3.0.2" = make_bundle "47318b97890d907f95c7c7513595fb3173a39fca"
    "1580b8nh3yh5w5y1dympxgg6j7h775iss19h0zcvsdrh80d1fmhz";
}
