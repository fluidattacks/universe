{ lib, makes_inputs, nixpkgs, python_pkgs, python_version, }:
let
  make_bundle = commit: sha256:
    let
      raw_src = builtins.fetchTarball {
        inherit sha256;
        url =
          "https://gitlab.com/dmurciaatfluid/singer_io/-/archive/${commit}/singer_io-${commit}.tar";
      };
      src = import "${raw_src}/build/filter.nix" nixpkgs.nix-filter raw_src;
    in import "${raw_src}/build" {
      makes_lib = makes_inputs;
      inherit nixpkgs python_version src;
    };
in {
  "v2.0.0" = make_bundle "5f3f90a0db218a831a00cf2acd64b87d10d817b4"
    "0plh6fdg5chfz0zfadx0vvxq6v9h0wfkdmqidbrbdmr7h9kglz1n";
  "v2.0.1" = make_bundle "b91cb14fe7fc10e0961680920c558f799ccb7f6a"
    "0hh5h40qd8nf4zpsz4yswxq6rd50174irpkw1wpxz63mfbid2s2p";
}
