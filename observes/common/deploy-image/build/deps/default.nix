{ makes_inputs, nixpkgs, pynix, python_version, }:
let
  inherit (pynix) lib;
  layer_1 = python_pkgs:
    python_pkgs // {
      arch-lint = let
        result = import ./arch_lint.nix {
          inherit lib makes_inputs nixpkgs python_pkgs python_version;
        };
      in result.pkg;
      types-boto3 = import ./boto3/stubs.nix lib python_pkgs;
    };
  layer_2 = python_pkgs:
    python_pkgs // {
      fa-purity = let
        result = import ./fa_purity.nix {
          inherit lib makes_inputs nixpkgs python_pkgs python_version;
        };
      in result.pkg;
    };
  python_pkgs =
    pynix.utils.compose [ layer_2 layer_1 ] pynix.lib.pythonPackages;
in { inherit lib python_pkgs; }
