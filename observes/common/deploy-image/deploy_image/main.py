import logging
import sys

from fa_purity import (
    Cmd,
    Result,
    ResultE,
)

from deploy_image.deploy import DeployClient, EcrRepo, Image

from .deploy import DeployClientFactory

LOG = logging.getLogger(__name__)
stderr_handler = logging.StreamHandler(sys.stderr)
LOG.setLevel(logging.DEBUG)
LOG.addHandler(stderr_handler)


def _deploy_if_not_exist(client: DeployClient, repo: EcrRepo, image: Image) -> Cmd[ResultE[None]]:
    already_exist = Cmd.wrap_impure(
        lambda: LOG.warning("Image tag `%s` already exist on the repo `%s`", image.tag, repo),
    )
    return client.exist_tag(repo, image.tag).bind(
        lambda r: r.to_coproduct().map(
            lambda exist: already_exist.map(Result.success)
            if exist
            else client.deploy_image(repo, image),
            lambda e: Cmd.wrap_value(Result.failure(e)),
        ),
    )


def login_and_deploy(aws_region: str, repo: EcrRepo, image: Image) -> Cmd[ResultE[None]]:
    msg = Cmd.wrap_impure(
        lambda: LOG.info("Repository URI: %s; Image: %s", repo.uri, image.tar_file),
    )
    return DeployClientFactory.new(aws_region).bind(
        lambda r: r.to_coproduct().map(
            lambda c: msg + _deploy_if_not_exist(c, repo, image),
            lambda e: Cmd.wrap_value(Result.failure(e)),
        ),
    )
