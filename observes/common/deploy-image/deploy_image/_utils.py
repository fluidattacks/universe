from typing import (
    TypeVar,
)

from fa_purity import (
    FrozenList,
    Result,
    ResultE,
    cast_exception,
)

_T = TypeVar("_T")


def get_index(items: FrozenList[_T], index: int) -> ResultE[_T]:
    try:
        return Result.success(items[index])
    except IndexError as err:
        return Result.failure(cast_exception(err))
