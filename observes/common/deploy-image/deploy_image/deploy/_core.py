from collections.abc import Callable
from dataclasses import dataclass
from pathlib import Path

from fa_purity import Cmd, ResultE


@dataclass(frozen=True)
class EcrRepo:
    uri: str


@dataclass(frozen=True)
class Image:
    tar_file: Path
    tag: str


@dataclass(frozen=True)
class Credentials:
    user: str
    password: str

    def __repr__(self) -> str:
        return "[masked]"


@dataclass(frozen=True)
class DeployClient:
    deploy_image: Callable[[EcrRepo, Image], Cmd[ResultE[None]]]
    delete_tag: Callable[[EcrRepo, str], Cmd[ResultE[None]]]
    exist_tag: Callable[[EcrRepo, str], Cmd[ResultE[bool]]]
