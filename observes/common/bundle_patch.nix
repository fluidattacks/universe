{ buildEnv, bundle, with_ruff ? false }:
let
  homeless_patch = pkg:
    pkg.overridePythonAttrs (_: {
      postPhases = [ "rmHomelessShelter" ];
      rmHomelessShelter = ''
        rm -rf /homeless-shelter
      '';
    });
  ruff_check = builtins.toFile "builder.sh" ''
    echo "Executing ruff lint" \
    && ruff --version \
    && ruff check .\
    && echo "Finished ruff lint phase" \
    && echo "Executing ruff format" \
    && ruff format \
    && echo "Finished ruff format phase"
  '';

  add_ruff_check = pkg:
    pkg.overridePythonAttrs (old: {
      checkPhase = [
        (builtins.concatStringsSep "" [
          (builtins.elemAt old.checkPhase 0)
          "&& source ${ruff_check}"
        ])
      ];
    });
  build_env = extraLibs:
    buildEnv {
      inherit extraLibs;
      ignoreCollisions = false;
    };
  patch =
    if with_ruff then x: homeless_patch (add_ruff_check x) else homeless_patch;
  patched_pkg = patch bundle.pkg;
in {
  pkg = patched_pkg;
  env = {
    inherit (bundle.env) dev;
    runtime = build_env [ patched_pkg ];
  };
  check = {
    tests = homeless_patch bundle.check.tests;
    types = homeless_patch bundle.check.types;
  };
}
