{ inputs, makeTemplate, ... }:
let
  deps = with inputs.observesIndex; [ common.integrates_dal.root ];
  env = builtins.map inputs.getRuntime deps;
in makeTemplate {
  searchPaths = { bin = env; };
  name = "observes-list-groups";
  template = ./template.sh;
}
