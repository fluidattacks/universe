{ src, deps, build_required_deps, makes_inputs, }:
let
  bundle_builder = lib: pkgDeps:
    makes_inputs.makePythonPyprojectPackage {
      inherit (lib) buildEnv buildPythonPackage;
      inherit pkgDeps src;
    };
  build_bundle = builder:
    # builder: Deps -> (PythonPkgs -> PkgDeps) -> (Deps -> PkgDeps -> Bundle) -> Bundle
    # Deps: are the default project dependencies
    # PythonPkgs -> PkgDeps: is the required dependencies builder
    # Deps -> PkgDeps -> Bundle: is the bundle builder
    builder deps build_required_deps bundle_builder;
  bundle = build_bundle (default: required_deps: builder:
    builder default.lib (required_deps default.python_pkgs));
in bundle // { inherit build_bundle; }
