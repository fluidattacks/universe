{ makes_inputs, nixpkgs, pynix, }:
let
  inherit (pynix) lib;
  python_version = "python311";
  layer_1 = python_pkgs:
    python_pkgs // {
      arch-lint = let
        result = import ./arch_lint.nix {
          inherit lib nixpkgs makes_inputs python_pkgs python_version;
        };
      in result.pkg;
    };
  layer_2 = python_pkgs:
    python_pkgs // {
      fa-purity = let
        result = import ./fa_purity.nix {
          inherit lib nixpkgs makes_inputs python_pkgs python_version;
        };
      in result.pkg;
    };
  python_pkgs =
    pynix.utils.compose [ layer_2 layer_1 ] pynix.lib.pythonPackages;
in { inherit lib python_pkgs nixpkgs; }
