{ makes_inputs, nixpkgs, pynix, src, }:
let
  deps = import ./deps { inherit makes_inputs nixpkgs pynix; };
  requirements = deps: {
    runtime_deps = with deps.python_pkgs;
      [ fa-purity pathos python-dateutil types-python-dateutil ]
      ++ [ deps.nixpkgs.sops ];
    build_deps = with deps.python_pkgs; [ flit-core ];
    test_deps = with deps.python_pkgs; [
      arch-lint
      mypy
      pytest
      pytest-cov
      pytest-timeout
      ruff
    ];
  };
  buildBundle = { pkgDeps }: pynix.stdBundle { inherit pkgDeps src; };
  bundle = buildBundle { pkgDeps = requirements deps; };
  vs_settings = pynix.vscodeSettingsShell { pythonEnv = bundle.env.dev; };
  coverage = pynix.coverageReport {
    inherit src;
    parentModulePath = makes_inputs.inputs.observesIndex.common.etl_utils.root;
    pythonDevEnv = bundle.env.dev;
    module = makes_inputs.inputs.observesIndex.common.etl_utils.pkg_name;
    testsFolder = "tests";
  };
in bundle // {
  inherit deps buildBundle requirements coverage;
  dev_hook = vs_settings.hook.text;
}
