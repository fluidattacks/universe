data "aws_iam_policy_document" "dynamo_streams_kms_policy" {
  statement {
    sid    = "AllowKinesisKmsEncrypt"
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = ["kinesis.amazonaws.com"]
    }
    actions = [
      "kms:Encrypt"
    ]
    resources = ["arn:aws:kms:${var.region}:${data.aws_caller_identity.current.account_id}:key/*"]
  }
  statement {
    sid    = "KeyAdministrators"
    effect = "Allow"
    principals {
      type = "AWS"
      identifiers = [
        "arn:aws:iam::${data.aws_caller_identity.current.account_id}:root",
        "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/prod_common",
      ]
    }
    actions = [
      "kms:*"
    ]
    resources = ["arn:aws:kms:${var.region}:${data.aws_caller_identity.current.account_id}:key/*"]
  }
}
resource "aws_kms_key" "dynamo_streams_key" {
  policy                  = data.aws_iam_policy_document.dynamo_streams_kms_policy.json
  deletion_window_in_days = 30
  is_enabled              = true
  enable_key_rotation     = true

  tags = {
    "Name"              = "observes-mirror-stream"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "etl"
  }
}
resource "aws_kinesis_stream" "observes_mirror" {
  name             = "observes-mirror"
  retention_period = 24
  encryption_type  = "KMS"
  kms_key_id       = aws_kms_key.dynamo_streams_key.key_id

  stream_mode_details {
    stream_mode = "ON_DEMAND"
  }

  lifecycle {
    prevent_destroy = true
  }
  tags = {
    "Name"              = "observes-mirror-stream"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "etl"
  }
}
resource "aws_kinesis_stream" "observes_mirror_historic" {
  name             = "observes-mirror-historic"
  retention_period = 24
  encryption_type  = "KMS"
  kms_key_id       = aws_kms_key.dynamo_streams_key.key_id

  stream_mode_details {
    stream_mode = "ON_DEMAND"
  }

  lifecycle {
    prevent_destroy = true
  }
  tags = {
    "Name"              = "observes-mirror-historic-stream"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "etl"
  }
}
