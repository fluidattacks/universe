data "aws_ecr_repository" "observes" {
  name = "observes"
}

resource "aws_security_group" "observes_integrates_historic_lambda" {
  name    = "observes_integrates_historic_lambda"
  vpc_id  = data.aws_vpc.main.id
  ingress = []
  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    "Name"              = "observes_integrates_historic_lambda"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "etl"
    "NOFLUID"           = "f024_allow_egress_to_reach_anywhere"
  }
}

data "aws_iam_policy_document" "observes_lambda_assume" {
  statement {
    actions = ["sts:AssumeRole"]
    effect  = "Allow"
    sid     = "AssumeRolePolicy"

    principals {
      identifiers = ["lambda.amazonaws.com"]
      type        = "Service"
    }
  }
}

resource "aws_iam_role" "observes_integrates_historic_lambda" {
  assume_role_policy = data.aws_iam_policy_document.observes_lambda_assume.json
  name               = "observes_integrates_historic_lambda"

  tags = {
    "Name"              = "observes_integrates_historic_lambda"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "etl"
  }
}

data "aws_iam_policy_document" "observes_lambda_policy" {
  statement {
    actions = [
      "dynamodb:BatchWriteItem",
      "dynamodb:GetItem",
      "dynamodb:PutItem",
      "dynamodb:Query",
      "dynamodb:UpdateItem",
    ]
    effect = "Allow"
    resources = [
      "arn:aws:dynamodb:${var.region}:${var.accountId}:table/*",
      "arn:aws:dynamodb:${var.region}:${var.accountId}:table/*/index/*"
    ]
    sid = "dbPolicy"
  }

  statement {
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]
    effect = "Allow"
    resources = [
      "arn:aws:logs:${var.region}:${var.accountId}:log-group:*"
    ]
    sid = "logPolicy"
  }

  statement {
    actions = [
      "dynamodb:DescribeStream",
      "dynamodb:GetRecords",
      "dynamodb:GetShardIterator",
      "dynamodb:ListStreams"
    ]
    effect = "Allow"
    resources = [
      "arn:aws:dynamodb:${var.region}:${var.accountId}:table/*"
    ]
    sid = "streamsPolicy"
  }

  statement {
    actions = [
      "kms:Decrypt",
      "kms:DescribeKey",
      "kms:Encrypt",
      "kms:GenerateDataKey"
    ]
    effect = "Allow"
    resources = [
      "arn:aws:kms:${var.region}:${var.accountId}:alias/${var.profile}",
      "arn:aws:kms:${var.region}:${var.accountId}:key/*",
    ]
    sid = "kmsPolicy"
  }
}

resource "aws_iam_policy" "observes_lambda_policy" {
  name   = "observes_lambda_policy"
  policy = data.aws_iam_policy_document.observes_lambda_policy.json
  tags = {
    "Name"              = "observes_lambda_policy"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "etl"
  }
}

resource "aws_iam_role_policy_attachment" "main" {
  role       = aws_iam_role.observes_integrates_historic_lambda.name
  policy_arn = aws_iam_policy.observes_lambda_policy.arn
}

resource "aws_lambda_function" "observes_integrates_historic_lambda" {
  architectures = ["arm64"]
  function_name = "observes-integrates-historic-lambda"
  image_uri     = "${data.aws_ecr_repository.observes.repository_url}:0.36.1"
  package_type  = "Image"
  memory_size   = 512
  role          = aws_iam_role.observes_integrates_historic_lambda.arn
  timeout       = 900

  environment {
    variables = {
      HANDLER_ID = var.handlerId
    }
  }

  tags = {
    "Name"              = "observes_integrates_historic_lambda"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "etl"
  }

  lifecycle {
    create_before_destroy = true
  }
}
