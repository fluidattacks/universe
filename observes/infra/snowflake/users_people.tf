resource "snowflake_user" "cosorio" {
  name                           = "COSORIO"
  login_name                     = "COSORIO@FLUIDATTACKS.COM"
  first_name                     = "Deison"
  last_name                      = "Osorio"
  comment                        = "Analytics member"
  disabled                       = false
  display_name                   = "Deison Osorio"
  email                          = "cosorio@fluidattacks.com"
  rsa_public_key                 = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1avBZ+YkC6b9Ats4w7eO4J2saT2i7VNeINQ1Y39i5sxReO84faNuD7XxULrfNOI5mCeiyw4aDnzeCKFVJdCeTR6PUrHop/bNZBTHJYr0XI6XtJ4UqIcZmgEWwRaVt+vwrXZFo2W7WtjIRGW+mhH7En118r8q+OnrQQR78L2GFfkJ3CzjoOy+vThSERs8yhHNII3n9/g5jm+2KmXttsvZOCyxYj70aOOuFGAgoko/cTIW3ML/XTSBxLkg2G5u/IQYWTaTJpD4kUrSTES2uYRkZNJonUxa6qh33EKfYsJM4JoLG3B3DDkqg0FtL+hKm5cz7TaXZVJ3be3+rwQgEexFVQIDAQAB"
  default_warehouse              = snowflake_warehouse.generic_compute.name
  default_role                   = snowflake_account_role.dbt_uploader.name
  default_secondary_roles_option = "NONE"
  disable_mfa                    = false
}
resource "snowflake_grant_account_role" "cosorio_grant_full_reader" {
  role_name = snowflake_account_role.full_reader.name
  user_name = snowflake_user.cosorio.name
}
resource "snowflake_grant_account_role" "cosorio_grant_monitor" {
  role_name = snowflake_account_role.monitor.name
  user_name = snowflake_user.cosorio.name
}
resource "snowflake_grant_account_role" "cosorio_grant_full_writer" {
  role_name = snowflake_account_role.full_writer.name
  user_name = snowflake_user.cosorio.name
}
resource "snowflake_grant_account_role" "cosorio_grant_role_dbt_uploader" {
  role_name = snowflake_account_role.dbt_uploader.name
  user_name = snowflake_user.cosorio.name
}
# ---
resource "snowflake_user" "dhernandez" {
  name                           = "DHERNANDEZ"
  login_name                     = "DHERNANDEZ@FLUIDATTACKS.COM"
  first_name                     = "Daniel"
  last_name                      = "Hernandez"
  comment                        = "Analytics member"
  disabled                       = false
  display_name                   = "Daniel Hernandez"
  email                          = "dhernandez@fluidattacks.com"
  rsa_public_key                 = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvaam8MzyOB3frkKK7BrojIBya9DWaDGpz6dxK2L03vMLp3EcnCZ74Z5xS8qFC+4hXC+BrWtlSMThYdnYIta9mGmaV8lFFvonYe8M2t0rDuChkS3RZFXtuvLVfb9D39x7xD2N1Y6Prb5VnAS1LRhhADugJxxtkCGsmIsmQtCni1f4BWpJjoVD225WI+eRYuQvBvXtGanPtIVPPkd35jW5Mo1emNvPepyxJw25q/fOYhy4hWILeKXQN+og82c6Ac7dlaxV+7nxfoyX+LaPKNR+8Yt0TfVyVr+vwX8taTDaFra8nhZl/daVp3iVqMpfnvAoestK8oJIDGrTCcJzvsm06QIDAQAB"
  default_warehouse              = snowflake_warehouse.generic_compute.name
  default_role                   = snowflake_account_role.dbt_uploader.name
  default_secondary_roles_option = "NONE"
  disable_mfa                    = false
}
resource "snowflake_grant_account_role" "dhernandez_grant_full_reader" {
  role_name = snowflake_account_role.full_reader.name
  user_name = snowflake_user.dhernandez.name
}
resource "snowflake_grant_account_role" "dhernandez_grant_role_dbt_uploader" {
  role_name = snowflake_account_role.dbt_uploader.name
  user_name = snowflake_user.dhernandez.name
}
# ---
resource "snowflake_user" "lruiz" {
  name                           = "LRUIZ"
  login_name                     = "LRUIZ@FLUIDATTACKS.COM"
  first_name                     = "Luisa"
  last_name                      = "Ruiz"
  comment                        = "Analytics member"
  disabled                       = false
  display_name                   = "Luisa Ruiz"
  email                          = "lruiz@fluidattacks.com"
  rsa_public_key                 = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqkW+YGk1JJxxVKZQbUBlzbxZ9uadT6eqIJ8OVmhjuvtbRklOqEd9HVXAR138by7CnLyWQSEI1THlUToqiQZsi8k2a02Tn/Q0aFri/jq26qycmy9dqzfQ9LAsMLSzwXWtUPuzFo3upusWb1z09HospAFoxOZbYV0C3hRLTNQxW1TUuCCpqtLMH4JGclsdS32DBVa9DvdkZZjeoMb+sSeLM5v7sFPHghHdVPxooSDgihRGIeY4wWybOl8aO0RrkgRk7xBGEpeip6HBh1l5snSZLIK0bEBQU1r646gZw8/ssD1URcJl/YupeOECBCpVsxna57XQ6jM4bQUkUiErZyqrVQIDAQAB"
  default_warehouse              = snowflake_warehouse.generic_compute.name
  default_role                   = snowflake_account_role.dbt_uploader.name
  default_secondary_roles_option = "NONE"
  disable_mfa                    = false
}
resource "snowflake_grant_account_role" "lruiz_grant_full_reader" {
  role_name = snowflake_account_role.full_reader.name
  user_name = snowflake_user.lruiz.name
}
resource "snowflake_grant_account_role" "lruiz_orio_grant_role_dbt_uploader" {
  role_name = snowflake_account_role.dbt_uploader.name
  user_name = snowflake_user.lruiz.name
}
# ---
resource "snowflake_user" "dmurcia" {
  name                           = "DMURCIA"
  login_name                     = "DMURCIA@FLUIDATTACKS.COM"
  first_name                     = "Daniel"
  last_name                      = "Murcia"
  comment                        = "ETLs maintainer"
  disabled                       = false
  display_name                   = "Daniel Murcia"
  email                          = "dmurcia@fluidattacks.com"
  rsa_public_key                 = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4rHk31+t0nxNDZiDRUwt9dqGu1FqmPpjYe3rwRA7jzaO/5J7P0wBohyjGGlLusWP8oI5+rEXAEWBdZcy48qMNc+UZbSCdR6wF9HeamExGgpbKCCcZMJj7+qvydN0rIAYrQz1GTAzMg1vY/QEyyDBM1Wej+M0vqCN5WXNUQcQyqJJ9Ifiip4smiU9haQegaCn7lNe8kUnn52z1tC4PVm7mIy0xYF8yU2R3LV+7h8p9770TObuxZcBknHo2OcHFdoEYNo8ubQdOWb/0Pufqym2/J9rACc0odqH+0GIufBq8GMxNUgUu4VN7RY3wlQ2nrNI1joC0kEvfCaPJYB9QuWVzwIDAQAB"
  default_warehouse              = snowflake_warehouse.generic_compute.name
  default_role                   = snowflake_account_role.full_reader.name
  default_secondary_roles_option = "NONE"
  disable_mfa                    = false
}
resource "snowflake_grant_account_role" "dmurcia_grant_full_reader" {
  role_name = snowflake_account_role.full_reader.name
  user_name = snowflake_user.dmurcia.name
}
resource "snowflake_grant_account_role" "dmurcia_grant_monitor" {
  role_name = snowflake_account_role.monitor.name
  user_name = snowflake_user.dmurcia.name
}
resource "snowflake_grant_account_role" "dmurcia_grant_admin" {
  role_name = "ACCOUNTADMIN"
  user_name = snowflake_user.dmurcia.name
}
# ---
resource "snowflake_user" "mrivera" {
  name                           = "MRIVERA"
  login_name                     = "MRIVERA@FLUIDATTACKS.COM"
  first_name                     = "Michael"
  last_name                      = "Rivera"
  comment                        = "Head of analytics"
  disabled                       = false
  display_name                   = "Michael Rivera"
  email                          = "mrivera@fluidattacks.com"
  rsa_public_key                 = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArQey5+rW6TUX4oe21nkRrkNkfFk7grqjjCdCuuJzyn8Wwd+INzit8dMCRE2EHmV6kP2w4md90s/UDk+UmGo5ZswdVJTQPviQ84D95p9uhnpUbFVu6vlCGwGcMtJzJM/ZqQchJG5QgQ3LBu7Jq+4BT2qbD9QXSIC1n87dxMcl01a0vQxFMtgL6NON1XEkJM1/ork8L7Ns4yPK5SbZ3k6QePZvQNzFhPKjTR71lJkQ6+QCKbozKXXEolPm92LqKt4iY+Nzltus8SDe/k8QBijZMzvPjGUGCGKS+cCcxP9TU/AVnyQ8bQjbEEQ0pvlzducEJm3XIwDPizUEMHKXpSnFqwIDAQAB"
  default_warehouse              = snowflake_warehouse.generic_compute.name
  default_role                   = snowflake_account_role.dbt_uploader.name
  default_secondary_roles_option = "NONE"
  disable_mfa                    = false
}
resource "snowflake_grant_account_role" "mrivera_grant_admin" {
  role_name = "ACCOUNTADMIN"
  user_name = snowflake_user.mrivera.name
}
resource "snowflake_grant_account_role" "mrivera_grant_role_dbt_uploader" {
  role_name = snowflake_account_role.dbt_uploader.name
  user_name = snowflake_user.mrivera.name
}
# ---
resource "snowflake_user" "yliscano" {
  name                           = "YLISCANO"
  login_name                     = "YLISCANO@FLUIDATTACKS.COM"
  first_name                     = "Yeison"
  last_name                      = "Liscano"
  comment                        = "code ETL maintainer"
  disabled                       = false
  display_name                   = "Yeison Liscano"
  email                          = "yliscano@fluidattacks.com"
  default_warehouse              = snowflake_warehouse.generic_compute.name
  default_role                   = snowflake_account_role.code_reader.name
  default_secondary_roles_option = "NONE"
  disable_mfa                    = false
}
resource "snowflake_grant_account_role" "yliscano_grant_code_reader" {
  role_name = snowflake_account_role.code_reader.name
  user_name = snowflake_user.yliscano.name
}
resource "snowflake_grant_account_role" "yliscano_grant_support" {
  role_name = snowflake_account_role.support.name
  user_name = snowflake_user.yliscano.name
}
resource "snowflake_grant_account_role" "yliscano_grant_monitor" {
  role_name = snowflake_account_role.monitor.name
  user_name = snowflake_user.yliscano.name
}
# ---
resource "snowflake_user" "jmesa" {
  name                           = "JMESA"
  login_name                     = "JMESA@FLUIDATTACKS.COM"
  first_name                     = "Jonathan"
  last_name                      = "Mesa"
  comment                        = "Integrates archive support"
  disabled                       = false
  display_name                   = "Jonathan Mesa"
  email                          = "jmesa@fluidattacks.com"
  default_warehouse              = snowflake_warehouse.generic_compute.name
  default_role                   = snowflake_account_role.integrates.name
  default_secondary_roles_option = "NONE"
  disable_mfa                    = false
}
resource "snowflake_grant_account_role" "jmesa_grant_integrates" {
  role_name = snowflake_account_role.integrates.name
  user_name = snowflake_user.jmesa.name
}
resource "snowflake_grant_account_role" "jmesa_grant_monitor" {
  role_name = snowflake_account_role.monitor.name
  user_name = snowflake_user.jmesa.name
}
# ---
resource "snowflake_user" "mgarzon" {
  name                           = "MGARZON"
  login_name                     = "MGARZON@FLUIDATTACKS.COM"
  first_name                     = "Marcela"
  last_name                      = "Garzon"
  comment                        = "Data Scientist"
  disabled                       = false
  display_name                   = "Marcela Garzon"
  email                          = "mgarzon@fluidattacks.com"
  rsa_public_key                 = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA7H0BgrzvfisvbRP6WJxHbe3RaNMmkwOE1Zv9J3YphqW9sf1Ee/ESI2IOMgqDagZPjgKZ1HczYx+ElI/yGOpqQnk5jSCZkNWvdPyY7Z0OaUL25mYmWcDTTBvIzNj8LtQ4ijfxUplhCzobdYjQkvO7q/8qAwxjDuhhZlmaJlehyitwfrDbXvJKvq905VE7r+rsHzngTPGbj+QBf9Y42eGLoU5g6iFgFE7d6P3+/qp4Y1gGJSfUScbMGJNadZMkDBOCZCmvQ67/8mVf+eKaw2DkPqg21ehKaz6PzJOguMV9VeisYczsA1Gu+24RdbDLkbYCpnQ/5gEWu/DEH4yoKGD50QIDAQAB"
  default_warehouse              = snowflake_warehouse.generic_compute.name
  default_role                   = snowflake_account_role.dbt_uploader.name
  default_secondary_roles_option = "NONE"
  disable_mfa                    = false
}
resource "snowflake_grant_account_role" "mgarzon_grant_full_reader" {
  role_name = snowflake_account_role.full_reader.name
  user_name = snowflake_user.mgarzon.name
}
resource "snowflake_grant_account_role" "mgarzon_grant_role_dbt_uploader" {
  role_name = snowflake_account_role.dbt_uploader.name
  user_name = snowflake_user.mgarzon.name
}
# ---
resource "snowflake_user" "gtorres" {
  name                           = "GTORRES"
  login_name                     = "GTORRES@FLUIDATTACKS.COM"
  first_name                     = "German"
  last_name                      = "Torres"
  comment                        = "Analytics member"
  disabled                       = false
  display_name                   = "German Torres"
  email                          = "gtorres@fluidattacks.com"
  rsa_public_key                 = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAowVvDKKLcrJ+z+Fv9wXHH+K80G1JOjW042oFSRFAJxJBuDIDqOyzTC/2QY3QNOpyv7tGos0w4NbLMxvLzSMlZS45syRzjAlfa6O4mdkCw5f7qruaDPVMlwXdzcbuAGWKddu2u5C3KbslxDUnc4SZnVLlpHOHsblsrsB0or3e58qx80r+njFjeUU1fuvcM7dDISRsUv7aPanTJGVxtpHEXyTnhWgxKxNGchBYh/oOxYu8L+hdujgadfwbu94nMtwtbG3ejkg+2lek9PGgae/MGoF21AK9/e91UNle9FkwCg5jtEDlE9DKV0eI+Jz1kby4YolSXOP2NJMxZlx/GBI0JwIDAQAB"
  default_warehouse              = snowflake_warehouse.generic_compute.name
  default_role                   = snowflake_account_role.dbt_uploader.name
  default_secondary_roles_option = "NONE"
  disable_mfa                    = false
}
resource "snowflake_grant_account_role" "gtorres_grant_full_reader" {
  role_name = snowflake_account_role.full_reader.name
  user_name = snowflake_user.gtorres.name
}
resource "snowflake_grant_account_role" "gtorres_grant_role_dbt_uploader" {
  role_name = snowflake_account_role.dbt_uploader.name
  user_name = snowflake_user.gtorres.name
}
# ---
resource "snowflake_user" "ralvarez" {
  name                           = "RALVAREZ"
  login_name                     = "RALVAREZ@FLUIDATTACKS.COM"
  first_name                     = "Rafael"
  last_name                      = "Álvarez"
  comment                        = "Head of Staff"
  disabled                       = false
  display_name                   = "Rafael Alvarez"
  email                          = "ralvarez@fluidattacks.com"
  default_warehouse              = snowflake_warehouse.generic_compute.name
  default_role                   = snowflake_account_role.monitor.name
  default_secondary_roles_option = "NONE"
  disable_mfa                    = false
}
resource "snowflake_grant_account_role" "ralvarez_grant_full_reader" {
  role_name = snowflake_account_role.full_reader.name
  user_name = snowflake_user.ralvarez.name
}
resource "snowflake_grant_account_role" "ralvarez_grant_monitor" {
  role_name = snowflake_account_role.monitor.name
  user_name = snowflake_user.ralvarez.name
}
# ---
resource "snowflake_user" "dbetancur" {
  name                           = "DBETANCUR"
  login_name                     = "DBETANCUR@FLUIDATTACKS.COM"
  first_name                     = "Daniel"
  last_name                      = "Betancur"
  comment                        = "Head of Engineering"
  disabled                       = false
  display_name                   = "Daniel Betancur"
  email                          = "dbetancur@fluidattacks.com"
  default_warehouse              = snowflake_warehouse.generic_compute.name
  default_role                   = snowflake_account_role.monitor.name
  default_secondary_roles_option = "NONE"
  disable_mfa                    = false
}
resource "snowflake_grant_account_role" "dbetancur_grant_monitor" {
  role_name = snowflake_account_role.monitor.name
  user_name = snowflake_user.dbetancur.name
}
# ---
resource "snowflake_user" "jrestrepo" {
  name                           = "JRESTREPO"
  login_name                     = "JRESTREPO@FLUIDATTACKS.COM"
  first_name                     = "Juan"
  last_name                      = "Restrepo"
  comment                        = "Head of Engineering"
  disabled                       = false
  display_name                   = "Juan Restrepo"
  email                          = "jrestrepo@fluidattacks.com"
  default_warehouse              = snowflake_warehouse.generic_compute.name
  default_role                   = snowflake_account_role.monitor.name
  default_secondary_roles_option = "NONE"
  disable_mfa                    = false
}

resource "snowflake_grant_account_role" "jrestrepo_grant_monitor" {
  role_name = snowflake_account_role.monitor.name
  user_name = snowflake_user.jrestrepo.name
}
# ---
resource "snowflake_user" "rrodriguez" {
  name                           = "RRODRIGUEZ"
  login_name                     = "RRODRIGUEZ@FLUIDATTACKS.COM"
  first_name                     = "Rafael"
  last_name                      = "Rodriguez"
  comment                        = "Analytics member"
  disabled                       = false
  display_name                   = "Rafael Rodriguez"
  email                          = "rrodriguez@fluidattacks.com"
  default_warehouse              = snowflake_warehouse.generic_compute.name
  default_role                   = snowflake_account_role.full_reader.name
  default_secondary_roles_option = "NONE"
  disable_mfa                    = false
}
resource "snowflake_grant_account_role" "rrodriguez_grant_full_reader" {
  role_name = snowflake_account_role.full_reader.name
  user_name = snowflake_user.rrodriguez.name
}
