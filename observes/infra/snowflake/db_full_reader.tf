resource "snowflake_database_role" "full_reader" {
  database = snowflake_database.observes.name
  name     = "FULL_READER"
  comment  = "Role reading all ETLs data"
}

# Grants
resource "snowflake_grant_privileges_to_database_role" "grant_full_reader_usage" {
  privileges         = ["USAGE"]
  database_role_name = snowflake_database_role.full_reader.fully_qualified_name
  on_database        = snowflake_database_role.full_reader.database
}
resource "snowflake_grant_privileges_to_database_role" "grant_full_reader_schemas_usage" {
  for_each           = setunion(local.persistent_schemas, local.archived_schemas, local.analytics_schemas)
  privileges         = ["USAGE"]
  database_role_name = snowflake_database_role.full_reader.fully_qualified_name
  on_schema {
    schema_name = each.key
  }
}
resource "snowflake_grant_privileges_to_database_role" "grant_full_reader_read" {
  for_each           = setunion(local.persistent_schemas, local.archived_schemas, local.analytics_schemas)
  privileges         = ["SELECT"]
  database_role_name = snowflake_database_role.full_reader.fully_qualified_name
  on_schema_object {
    all {
      object_type_plural = "TABLES"
      in_schema          = each.key
    }
  }
}

# Future schemas & tables
resource "snowflake_grant_privileges_to_database_role" "full_reader_future_schemas" {
  privileges         = ["USAGE"]
  database_role_name = snowflake_database_role.full_reader.fully_qualified_name
  on_schema {
    future_schemas_in_database = snowflake_database_role.full_reader.database
  }
}
resource "snowflake_grant_privileges_to_database_role" "full_reader_future_tables" {
  privileges         = ["SELECT"]
  database_role_name = snowflake_database_role.full_reader.fully_qualified_name
  on_schema_object {
    future {
      object_type_plural = "TABLES"
      in_database        = snowflake_database_role.full_reader.database
    }
  }
}
resource "snowflake_grant_privileges_to_database_role" "grant_full_reader_read_future_dynamo" {
  privileges         = ["SELECT"]
  database_role_name = snowflake_database_role.full_reader.fully_qualified_name
  on_schema_object {
    future {
      object_type_plural = "TABLES"
      in_schema          = snowflake_schema.dynamodb.fully_qualified_name
    }
  }
}
resource "snowflake_grant_privileges_to_database_role" "grant_full_reader_read_future_analytics" {
  privileges         = ["SELECT"]
  database_role_name = snowflake_database_role.full_reader.fully_qualified_name
  on_schema_object {
    future {
      object_type_plural = "TABLES"
      in_schema          = snowflake_schema.analytics_production.fully_qualified_name
    }
  }
}
