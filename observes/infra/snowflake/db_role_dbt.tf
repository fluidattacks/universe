resource "snowflake_database_role" "dbt_uploader" {
  database = snowflake_database.observes.name
  name     = "DBT_UPLOADER"
  comment  = "Role for an ETL upload process"
}

# Grants
resource "snowflake_grant_privileges_to_database_role" "dbt_uploader_schemas" {
  privileges         = ["CREATE SCHEMA"]
  database_role_name = snowflake_database_role.dbt_uploader.fully_qualified_name
  on_database        = snowflake_database_role.dbt_uploader.database
}
resource "snowflake_grant_privileges_to_database_role" "dbt_uploader_create" {
  for_each           = local.analytics_schemas
  privileges         = ["CREATE TABLE", "CREATE VIEW", "CREATE MATERIALIZED VIEW", "USAGE"]
  database_role_name = snowflake_database_role.dbt_uploader.fully_qualified_name
  on_schema {
    schema_name = each.key
  }
}

# Assign roles
resource "snowflake_grant_database_role" "dbt_uploader_full_reader" {
  database_role_name        = snowflake_database_role.full_reader.fully_qualified_name
  parent_database_role_name = snowflake_database_role.dbt_uploader.fully_qualified_name
}
