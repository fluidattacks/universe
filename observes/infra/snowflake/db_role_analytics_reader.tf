resource "snowflake_database_role" "analytics_reader" {
  database = snowflake_database.observes.name
  name     = "ANALYTICS_READER"
  comment  = "Role for reading analytics data"
}

# Grants
resource "snowflake_grant_privileges_to_database_role" "analytics_reader_db" {
  privileges         = ["USAGE"]
  database_role_name = snowflake_database_role.analytics_reader.fully_qualified_name
  on_database        = snowflake_database_role.analytics_reader.database
}
resource "snowflake_grant_privileges_to_database_role" "grant_analytics_reader_usage_analytics" {
  privileges         = ["USAGE"]
  database_role_name = snowflake_database_role.analytics_reader.fully_qualified_name
  on_schema {
    schema_name = snowflake_schema.analytics_production.fully_qualified_name
  }
}
resource "snowflake_grant_privileges_to_database_role" "grant_analytics_reader_read_analytics" {
  privileges         = ["SELECT"]
  database_role_name = snowflake_database_role.analytics_reader.fully_qualified_name
  on_schema_object {
    all {
      object_type_plural = "TABLES"
      in_schema          = snowflake_schema.analytics_production.fully_qualified_name
    }
  }
}
resource "snowflake_grant_privileges_to_database_role" "grant_analytics_reader_read_future_analytics" {
  privileges         = ["SELECT"]
  database_role_name = snowflake_database_role.analytics_reader.fully_qualified_name
  on_schema_object {
    future {
      object_type_plural = "TABLES"
      in_schema          = snowflake_schema.analytics_production.fully_qualified_name
    }
  }
}
