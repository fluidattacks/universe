resource "snowflake_database_role" "etl_uploader" {
  database = snowflake_database.observes.name
  name     = "ETL_UPLOADER"
  comment  = "Role for an ETL upload process"
}

# Grants
resource "snowflake_grant_privileges_to_database_role" "etl_uploader_schemas" {
  privileges         = ["CREATE SCHEMA", "USAGE"]
  database_role_name = snowflake_database_role.etl_uploader.fully_qualified_name
  on_database        = snowflake_database_role.etl_uploader.database
}

resource "snowflake_grant_privileges_to_database_role" "etl_uploader_create" {
  for_each           = local.persistent_schemas
  privileges         = ["CREATE TABLE", "USAGE"]
  database_role_name = snowflake_database_role.etl_uploader.fully_qualified_name
  on_schema {
    schema_name = each.key
  }
}
resource "snowflake_grant_privileges_to_database_role" "etl_uploader_integrates_stage" {
  privileges         = ["USAGE"]
  database_role_name = snowflake_database_role.etl_uploader.fully_qualified_name
  on_schema_object {
    object_type = "STAGE"
    object_name = snowflake_stage.integrates_stage.fully_qualified_name
  }
}

resource "snowflake_grant_privileges_to_database_role" "etl_uploader_qs_stage_gm" {
  privileges         = ["USAGE"]
  database_role_name = snowflake_database_role.etl_uploader.fully_qualified_name
  on_schema_object {
    object_type = "STAGE"
    object_name = snowflake_stage.quicksight_etl_stage_gm.fully_qualified_name
  }
}

resource "snowflake_grant_privileges_to_database_role" "etl_uploader_qs_stage_oa" {
  privileges         = ["USAGE"]
  database_role_name = snowflake_database_role.etl_uploader.fully_qualified_name
  on_schema_object {
    object_type = "STAGE"
    object_name = snowflake_stage.quicksight_etl_stage_oa.fully_qualified_name
  }
}

resource "snowflake_grant_privileges_to_database_role" "etl_uploader_qs_stage_dd" {
  privileges         = ["USAGE"]
  database_role_name = snowflake_database_role.etl_uploader.fully_qualified_name
  on_schema_object {
    object_type = "STAGE"
    object_name = snowflake_stage.quicksight_etl_stage_dd.fully_qualified_name
  }
}

resource "snowflake_grant_privileges_to_database_role" "etl_uploader_qs_stage_di" {
  privileges         = ["USAGE"]
  database_role_name = snowflake_database_role.etl_uploader.fully_qualified_name
  on_schema_object {
    object_type = "STAGE"
    object_name = snowflake_stage.quicksight_etl_stage_di.fully_qualified_name
  }
}

resource "snowflake_grant_privileges_to_database_role" "etl_uploader_cloudtrail_stage" {
  privileges         = ["USAGE"]
  database_role_name = snowflake_database_role.etl_uploader.fully_qualified_name
  on_schema_object {
    object_type = "STAGE"
    object_name = snowflake_stage.quicksight_etl_stage_cloudtrail.fully_qualified_name
  }
}

resource "snowflake_grant_privileges_to_database_role" "etl_uploader_integrates_data_format" {
  privileges         = ["USAGE"]
  database_role_name = snowflake_database_role.etl_uploader.fully_qualified_name
  on_schema_object {
    object_type = "FILE FORMAT"
    object_name = snowflake_file_format.integrates_data.fully_qualified_name
  }
}

resource "snowflake_grant_privileges_to_database_role" "etl_uploader_dd_format" {
  privileges         = ["USAGE"]
  database_role_name = snowflake_database_role.etl_uploader.fully_qualified_name
  on_schema_object {
    object_type = "FILE FORMAT"
    object_name = snowflake_file_format.datasets_data_format.fully_qualified_name
  }
}

resource "snowflake_grant_privileges_to_database_role" "etl_uploader_cloudtrail_data_format" {
  privileges         = ["USAGE"]
  database_role_name = snowflake_database_role.etl_uploader.fully_qualified_name
  on_schema_object {
    object_type = "FILE FORMAT"
    object_name = snowflake_file_format.cloudtrail_data_format.fully_qualified_name
  }
}
