resource "snowflake_account_role" "support" {
  name = "SUPPORT"
}

# Grants
resource "snowflake_grant_privileges_to_account_role" "support_cases_2" {
  account_role_name = snowflake_account_role.support.name
  privileges        = ["MANAGE ACCOUNT SUPPORT CASES"]
  on_account        = true
}
resource "snowflake_grant_privileges_to_account_role" "support_cases_3" {
  account_role_name = snowflake_account_role.support.name
  privileges        = ["MANAGE USER SUPPORT CASES"]
  on_account        = true
}

# Assign roles
resource "snowflake_grant_account_role" "admin_grant_support_role" {
  role_name        = snowflake_account_role.support.name
  parent_role_name = "SYSADMIN"
}
