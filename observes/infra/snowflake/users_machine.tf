# ---- ETL ----
resource "snowflake_service_user" "etl_user" {
  name                           = "ETL_USER"
  login_name                     = "ETL_USER"
  default_warehouse              = snowflake_warehouse.generic_compute.name
  default_role                   = snowflake_account_role.etl_uploader.name
  default_secondary_roles_option = "NONE"
  rsa_public_key                 = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA9ogZ0+TFGHKcwWhxjSxc17lQUxBnlnH3aRYKPiVZWbg3BBo1C7TicIyOMNBN0xlqS+hUnH5nbLTiP2gOAHLTclkjDaQITAQUNBGz/Gw7cOYNLKfw6PXYx/QZ19eFvZMf8sx0CMulSed2A9DQU0cHH5Q/vKukue1HfCXpm9cQb5zZK9MVRRB5IGkxwVSqVntW+J269XYVqyUrE902thaDIL/qepi48hsP6wsrmWIHN18XAc+uhewbhUoFEwfgP69giIFl+Ed2Sr/Q4VikTtOTTtT6WDphgPlVQ3gx6rWWmqLfEcznok515JO222z9lSwUaCSvfNaenwscUVZLaPcRnQIDAQAB"
}
resource "snowflake_grant_account_role" "etl_user_grant_etl_uploader_role" {
  role_name = snowflake_account_role.etl_uploader.name
  user_name = snowflake_service_user.etl_user.name
}
resource "snowflake_grant_account_role" "etl_user_grant_migration_uploader_role" {
  role_name = snowflake_account_role.migration_uploader.name
  user_name = snowflake_service_user.etl_user.name
}
# ---- DBT ----
resource "snowflake_service_user" "dbt_user" {
  name                           = "DBT_USER"
  login_name                     = "DBT_USER"
  default_warehouse              = snowflake_warehouse.generic_compute.name
  default_role                   = snowflake_account_role.dbt_uploader.name
  default_secondary_roles_option = "NONE"
  rsa_public_key                 = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwO/tPUtfl5gOgbFxgo4zL5vxz8VFSsuD2IdX3NLrpRuTNHBWdKSmsT9b+9JUslBFjH9ZYdknEmJ7Y31Ii475LRLDabe9kvg3EArrPAcqiysSlrMLSXDO5HLD2XGA8NKuljFIt6EG4/H1UoeN/dg9y9s+C0TZexFu2TVqSGnypkLMo9XPT5gP9A0/ghK4dA+4Vk7Sdnaycz/reiu6fwVQmnH9jn1o2U6Esm18jty3tBW3Ek+/wD4tWWnV1Rr86zW4Vs1QAVTzpNiAXnQsuvRtba5eWTwAckUzmVAdBBJJBMJPJbdCmov1brOX1YrNpcShO57hWRDXX1KTEI8rDYEVzQIDAQAB"
}

resource "snowflake_grant_account_role" "dbt_user_grant_role" {
  role_name = snowflake_account_role.dbt_uploader.name
  user_name = snowflake_service_user.dbt_user.name
}
# ---- QUICKSIGHT ----
resource "snowflake_legacy_service_user" "quicksight" {
  name                           = "QUICKSIGHT"
  login_name                     = "QUICKSIGHT"
  password                       = var.quicksightPassword
  default_warehouse              = snowflake_warehouse.generic_compute.name
  default_role                   = snowflake_account_role.analytics_reader.name
  default_secondary_roles_option = "NONE"
  must_change_password           = false
}
resource "snowflake_grant_account_role" "quicksight_user_grant_analytics_reader" {
  role_name = snowflake_account_role.analytics_reader.name
  user_name = snowflake_legacy_service_user.quicksight.name
}
# ---- FIVETRAN ----
resource "snowflake_service_user" "fivetran" {
  name                           = "FIVETRAN"
  login_name                     = "FIVETRAN"
  default_warehouse              = snowflake_warehouse.generic_compute.name
  default_role                   = snowflake_account_role.full_writer.name
  default_secondary_roles_option = "NONE"
  rsa_public_key                 = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA5oavsu27SJieTeC7mXGw4AwO+o6SOe391cFhlj3RHbPNAactIOYeagKQsup/wCIWGKJJYXZzlFs6vrVAqCIZuvfaoyThYtRWojvm8sLDcSJzv5n79OuVWoSprAXiG4FNXLKehs+k48mGzQgIQm+yM2K8cKMSROI5SgLc3bE56Js7wIlmLijd5McUONUaFTUSACZbqqSTRVjgayfa3gaDghrvVJpzVVxq5ce/MoBBCye/HZeKxVGCgaIYReU79w+cqlNEEXg0XXD6qi0pZoaTIP9vQq+q06ieG6SlrIls+93X6y9d6xjm5kQkqd8OK5nzwsHpIFZvJBavG0bPLPJokwIDAQAB"
}
resource "snowflake_grant_account_role" "fivetran_user_grant_full_writer" {
  role_name = snowflake_account_role.full_writer.name
  user_name = snowflake_service_user.fivetran.name
}
# ---- ZOHO_DATAPREP ----
resource "snowflake_legacy_service_user" "zoho_dataprep" {
  name                           = "ZOHO_DATAPREP"
  login_name                     = "ZOHO_DATAPREP"
  password                       = var.zohoDataPrepPassword
  default_warehouse              = snowflake_warehouse.generic_compute.name
  default_role                   = snowflake_account_role.full_writer.name
  default_secondary_roles_option = "NONE"
  must_change_password           = false
}
resource "snowflake_grant_account_role" "zoho_dataprep_user_grant_full_writer" {
  role_name = snowflake_account_role.full_writer.name
  user_name = snowflake_legacy_service_user.zoho_dataprep.name
}
# ---- SORTS ----
resource "snowflake_service_user" "sorts" {
  name                           = "SORTS"
  login_name                     = "SORTS"
  default_warehouse              = snowflake_warehouse.generic_compute.name
  default_role                   = snowflake_account_role.sorts.name
  default_secondary_roles_option = "NONE"
  rsa_public_key                 = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwBLYVlW09kgWQjcyd9GYtPKUjGGz3t3MBEp7P5kBsjIdmU9Z5ftcNnBrBbDSqND1rW5FeIZbxj93DxZAhb4iX+7yK3+HS/e0V3piy8xqNDYfx8XWlr+mxEan2oQhAnAKoCWAmrDNR1SKPn/EJfb44selm70S8Q6mE5iZaPGzSzOqNOBUZYei2L9wdpm0MsIp7z7Kz5alMCetq3l3I6tPNqUh62ngQSocAFYajdCmFPHWmE128SC4XgW4pbPSB6zboiTHi0bWBUzoxSdiqaE+8nzB0UNRkPod6mSBAZrRAxoal9os0Sbq1TJ18gcFvUs4xSusZVnKZv1qtK1NXi6KMwIDAQAB"
}
resource "snowflake_grant_account_role" "sorts_grant_sorts_role" {
  role_name = snowflake_account_role.sorts.name
  user_name = snowflake_service_user.sorts.name
}
resource "snowflake_grant_account_role" "sorts_grant_analytics_reader_role" {
  role_name = snowflake_account_role.analytics_reader.name
  user_name = snowflake_service_user.sorts.name
}
# ---- INTEGRATES ----
resource "snowflake_service_user" "integrates" {
  name                           = "INTEGRATES"
  login_name                     = "INTEGRATES"
  default_warehouse              = snowflake_warehouse.generic_compute.name
  default_role                   = snowflake_account_role.integrates.name
  default_secondary_roles_option = "NONE"
  rsa_public_key                 = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwWysh2Vrn6nmjFn8Y4ln4bUHAcHAHVYPbQIBW0mscu65TEwvJHGnVkj8LoFf2jGdRGWkK/jrfhVC42QuE/CpEqVz3ylctFHOND6nAsM2JcHJIqTQcGFT7CnK+gKCLqC41Pdj541bc3NLv0lWeBbHRfcQB/LKczNXz2c260GP6h08DwFFagrLN1BSzZvtZBOWs/otMPPGl/LycAWuLe0nJTr4sqzSY4UcTnM3g2Vnaa2hkl81npE225YxbTTJSPCcUTI6ISlU4L/74rxFnCGQUfyXQ/3W2LLHGJjhVsmTFeVg6Fh89JIAocMxeu/eYvSHSojhdQ8BUSFYOx13MNs5DQIDAQAB"
}
resource "snowflake_grant_account_role" "etl_user_grant_integrates_role" {
  role_name = snowflake_account_role.integrates.name
  user_name = snowflake_service_user.integrates.name
}
# ---- AIRBYTE ----

resource "snowflake_service_user" "airbyte" {
  name                           = "AIRBYTE"
  login_name                     = "AIRBYTE"
  default_warehouse              = snowflake_warehouse.generic_compute.name
  default_role                   = snowflake_account_role.full_writer.name
  default_secondary_roles_option = "NONE"
  rsa_public_key                 = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAm3h7idJuzUvBO1mvFmwW83c42bcNWeFSWM6I4GfbF3s15Okj6n64ed5jH1f9cWnykTC+IzvZbVDw/3dw2MdrH58MJac8mklAT5rERlchSSUFqFzpzIaE58nCtzm0BXQHF73AHDJE7hnRkws4KEO6+qrSWALGj0X18miDqKI27bairh1wLL16oOkXT6qquur9uXEogZg19dg6iwc0fPGqKXaA/io9QUmtLLamEn7bUZ62gxv5rXtHJEjQabexWLvAxz8GpdZKukH420gDD13IhE2e7BdHUam/Vl9Gai+QJTaq4/MSSIqvBclS+B+8HmS+tx0+XZ+qDg2Ga+dTe9XioQIDAQAB"
}

resource "snowflake_grant_account_role" "airbyte_user_grant_full_writer" {
  role_name = snowflake_account_role.full_writer.name
  user_name = snowflake_service_user.airbyte.name
}
