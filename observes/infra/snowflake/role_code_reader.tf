resource "snowflake_account_role" "code_reader" {
  name = "CODE_READER"
}

# Grants
resource "snowflake_grant_privileges_to_account_role" "code_reader_engineering_warehouse_usage" {
  account_role_name = snowflake_account_role.code_reader.name
  privileges        = ["USAGE", "MONITOR"]
  on_account_object {
    object_type = "WAREHOUSE"
    object_name = snowflake_warehouse.engineering_compute.name
  }
}
resource "snowflake_grant_privileges_to_account_role" "code_reader_generic_warehouse_usage" {
  account_role_name = snowflake_account_role.code_reader.name
  privileges        = ["USAGE"]
  on_account_object {
    object_type = "WAREHOUSE"
    object_name = snowflake_warehouse.generic_compute.name
  }
}

# Assign roles
resource "snowflake_grant_database_role" "code_reader_role_setup" {
  database_role_name = snowflake_database_role.code_reader.fully_qualified_name
  parent_role_name   = snowflake_account_role.code_reader.name
}
resource "snowflake_grant_account_role" "admin_grant_code_reader_role" {
  role_name        = snowflake_account_role.code_reader.name
  parent_role_name = "SYSADMIN"
}
