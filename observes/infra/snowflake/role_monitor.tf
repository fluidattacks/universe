resource "snowflake_account_role" "monitor" {
  name = "MONITOR"
}

# Grants
resource "snowflake_grant_privileges_to_account_role" "warehouses_monitor" {
  for_each = toset([
    snowflake_warehouse.engineering_compute.name,
    snowflake_warehouse.dbt_compute.name,
    snowflake_warehouse.etl_compute.name,
    snowflake_warehouse.generic_compute.name,
  ])
  account_role_name = snowflake_account_role.monitor.name
  privileges        = ["MONITOR"]
  on_account_object {
    object_type = "WAREHOUSE"
    object_name = each.key
  }
}
resource "snowflake_grant_privileges_to_account_role" "monitor_generic_warehouse_usage" {
  account_role_name = snowflake_account_role.monitor.name
  privileges        = ["USAGE"]
  on_account_object {
    object_type = "WAREHOUSE"
    object_name = snowflake_warehouse.generic_compute.name
  }
}

# Assign roles
resource "snowflake_grant_database_role" "monitor_grant_usage_viewer" {
  database_role_name = "SNOWFLAKE.USAGE_VIEWER"
  parent_role_name   = snowflake_account_role.monitor.name
}
resource "snowflake_grant_database_role" "monitor_grant_usage_gov_viewer" {
  database_role_name = "SNOWFLAKE.GOVERNANCE_VIEWER"
  parent_role_name   = snowflake_account_role.monitor.name
}
resource "snowflake_grant_database_role" "monitor_grant_usage_billing_viewer" {
  database_role_name = "SNOWFLAKE.ORGANIZATION_BILLING_VIEWER"
  parent_role_name   = snowflake_account_role.monitor.name
}
resource "snowflake_grant_account_role" "admin_grant_monitor_role" {
  role_name        = snowflake_account_role.monitor.name
  parent_role_name = "SYSADMIN"
}
