resource "snowflake_storage_integration" "etl_data" {
  name                 = "ETL_DATA_INTEGRATION"
  comment              = "S3 integrates data"
  type                 = "EXTERNAL_STAGE"
  storage_provider     = "S3"
  storage_aws_role_arn = "arn:aws:iam::205810638802:role/snowflake_role"
  storage_allowed_locations = ["s3://observes.etl-data/",
    "s3://admin-console205810638802/",
  "s3://common-master-cloudtrail-bucket/"]
  enabled = true
}
resource "snowflake_file_format" "integrates_data" {
  name                         = "INTEGRATES_DATA_FORMAT"
  database                     = snowflake_database.observes.name
  schema                       = snowflake_schema.dynamodb.name
  format_type                  = "CSV"
  parse_header                 = false
  field_optionally_enclosed_by = "\""
  null_if                      = ["\\N", ""]
}

resource "snowflake_file_format" "datasets_data_format" {
  name                         = "DATASETS_DATA_FORMAT"
  database                     = snowflake_database.observes.name
  schema                       = snowflake_schema.qs_obs.name
  format_type                  = "CSV"
  parse_header                 = false
  field_delimiter              = "|"
  field_optionally_enclosed_by = "\""
  null_if                      = ["\\N", ""]
}

resource "snowflake_file_format" "cloudtrail_data_format" {
  name        = "CLOUDTRAIL_DATA_FORMAT"
  database    = snowflake_database.observes.name
  schema      = snowflake_schema.qs_obs.name
  format_type = "JSON"
  compression = "GZIP"
}
resource "snowflake_file_format" "migration_data" {
  name        = "MIGRATION_DATA_FORMAT"
  database    = snowflake_database.observes.name
  schema      = snowflake_schema.dynamodb.name
  format_type = "JSON"
}

resource "snowflake_stage" "integrates_stage" {
  name                = "INTEGRATES_STAGE"
  database            = snowflake_database.observes.name
  schema              = snowflake_schema.dynamodb.name
  url                 = "s3://observes.etl-data/new_dynamodb/"
  file_format         = "FORMAT_NAME = ${snowflake_file_format.integrates_data.fully_qualified_name}"
  storage_integration = snowflake_storage_integration.etl_data.name
}

resource "snowflake_stage" "migration_stage" {
  name                = "MIGRATION_STAGE"
  database            = snowflake_database.observes.name
  schema              = snowflake_schema.migration.name
  url                 = "s3://observes.etl-data/migration/"
  file_format         = "FORMAT_NAME = ${snowflake_file_format.migration_data.fully_qualified_name}"
  storage_integration = snowflake_storage_integration.etl_data.name
}

resource "snowflake_stage" "quicksight_etl_stage_gm" {
  name                = "QUICKSIGHT_ETL_STAGE_GM"
  database            = snowflake_database.observes.name
  schema              = snowflake_schema.qs_obs.name
  url                 = "s3://admin-console205810638802/monitoring/quicksight/group_membership/"
  file_format         = "FORMAT_NAME = ${snowflake_file_format.integrates_data.fully_qualified_name}"
  storage_integration = snowflake_storage_integration.etl_data.name
}

resource "snowflake_stage" "quicksight_etl_stage_oa" {
  name                = "QUICKSIGHT_ETL_STAGE_OA"
  database            = snowflake_database.observes.name
  schema              = snowflake_schema.qs_obs.name
  url                 = "s3://admin-console205810638802/monitoring/quicksight/object_access/"
  file_format         = "FORMAT_NAME = ${snowflake_file_format.integrates_data.fully_qualified_name}"
  storage_integration = snowflake_storage_integration.etl_data.name
}

resource "snowflake_stage" "quicksight_etl_stage_dd" {
  name                = "QUICKSIGHT_ETL_STAGE_DD"
  database            = snowflake_database.observes.name
  schema              = snowflake_schema.qs_obs.name
  url                 = "s3://admin-console205810638802/monitoring/quicksight/data_dictionary/"
  file_format         = "FORMAT_NAME = ${snowflake_file_format.integrates_data.fully_qualified_name}"
  storage_integration = snowflake_storage_integration.etl_data.name
}

resource "snowflake_stage" "quicksight_etl_stage_di" {
  name                = "QUICKSIGHT_ETL_STAGE_DI"
  database            = snowflake_database.observes.name
  schema              = snowflake_schema.qs_obs.name
  url                 = "s3://admin-console205810638802/monitoring/quicksight/datsets_info/"
  file_format         = "FORMAT_NAME = ${snowflake_file_format.datasets_data_format.fully_qualified_name}"
  storage_integration = snowflake_storage_integration.etl_data.name
}

resource "snowflake_stage" "quicksight_etl_stage_cloudtrail" {
  name                = "QUICKSIGHT_ETL_STAGE_CLOUDTRAIL"
  database            = snowflake_database.observes.name
  schema              = snowflake_schema.qs_obs.name
  url                 = "s3://common-master-cloudtrail-bucket/cloudtrail/AWSLogs/205810638802/CloudTrail/us-east-1/"
  file_format         = "FORMAT_NAME = ${snowflake_file_format.cloudtrail_data_format.fully_qualified_name}"
  storage_integration = snowflake_storage_integration.etl_data.name
}
