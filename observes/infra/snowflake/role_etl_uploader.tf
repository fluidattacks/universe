resource "snowflake_account_role" "etl_uploader" {
  name = "ETL_UPLOADER"
}

# Grants
resource "snowflake_grant_privileges_to_account_role" "etl_uploader_warehouse_usage" {
  account_role_name = snowflake_account_role.etl_uploader.name
  privileges        = ["USAGE"]
  on_account_object {
    object_type = "WAREHOUSE"
    object_name = snowflake_warehouse.etl_compute.name
  }
}
resource "snowflake_grant_privileges_to_account_role" "etl_uploader_engineering_warehouse_usage" {
  account_role_name = snowflake_account_role.etl_uploader.name
  privileges        = ["USAGE"]
  on_account_object {
    object_type = "WAREHOUSE"
    object_name = snowflake_warehouse.engineering_compute.name
  }
}
resource "snowflake_grant_privileges_to_account_role" "etl_uploader_generic_warehouse_usage" {
  account_role_name = snowflake_account_role.etl_uploader.name
  privileges        = ["USAGE"]
  on_account_object {
    object_type = "WAREHOUSE"
    object_name = snowflake_warehouse.generic_compute.name
  }
}

# Assign roles
resource "snowflake_grant_database_role" "etl_uploader_role_setup" {
  database_role_name = snowflake_database_role.etl_uploader.fully_qualified_name
  parent_role_name   = snowflake_account_role.etl_uploader.name
}
resource "snowflake_grant_account_role" "admin_grant_etl_uploader_role" {
  role_name        = snowflake_account_role.etl_uploader.name
  parent_role_name = "SYSADMIN"
}
