resource "snowflake_database_role" "indicators_writer" {
  database = snowflake_database.observes.name
  name     = "INDICATORS_WRITER"
  comment  = "Role for updating success indicators"
}

# Grants
resource "snowflake_grant_privileges_to_database_role" "indicators_writer_db" {
  privileges         = ["USAGE"]
  database_role_name = snowflake_database_role.indicators_writer.fully_qualified_name
  on_database        = snowflake_database_role.indicators_writer.database
}
resource "snowflake_grant_privileges_to_database_role" "grant_indicators_writer_usage_last_update" {
  privileges         = ["USAGE"]
  database_role_name = snowflake_database_role.indicators_writer.fully_qualified_name
  on_schema {
    schema_name = snowflake_schema.last_update.fully_qualified_name
  }
}
resource "snowflake_grant_privileges_to_database_role" "grant_indicators_writer_write_last_update" {
  for_each           = toset(["CODE_UPLOAD", "JOBS", "REPOS"])
  privileges         = ["SELECT", "INSERT", "UPDATE", "DELETE"]
  database_role_name = snowflake_database_role.indicators_writer.fully_qualified_name
  on_schema_object {
    object_type = "TABLE"
    object_name = "${snowflake_schema.last_update.fully_qualified_name}.${each.key}"
  }
}
