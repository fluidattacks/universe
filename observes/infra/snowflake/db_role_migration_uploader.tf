resource "snowflake_database_role" "migration_uploader" {
  database = snowflake_database.observes.name
  name     = "MIGRATION_UPLOADER"
  comment  = "Role for the migration process"
}

# Grants
resource "snowflake_grant_privileges_to_database_role" "migration_uploader_schemas" {
  privileges         = ["USAGE"]
  database_role_name = snowflake_database_role.migration_uploader.fully_qualified_name
  on_database        = snowflake_database_role.migration_uploader.database
}
resource "snowflake_grant_privileges_to_database_role" "migration_uploader_usage" {
  privileges         = ["CREATE TABLE", "USAGE"]
  database_role_name = snowflake_database_role.migration_uploader.fully_qualified_name
  on_schema {
    schema_name = snowflake_schema.migration.fully_qualified_name
  }
}
resource "snowflake_grant_privileges_to_database_role" "migration_uploader_create_1" {
  for_each           = local.archived_schemas
  privileges         = ["CREATE TABLE", "USAGE"]
  database_role_name = snowflake_database_role.migration_uploader.fully_qualified_name
  on_schema {
    schema_name = each.key
  }
}
resource "snowflake_grant_privileges_to_database_role" "migration_uploader_create_2" {
  for_each           = local.persistent_schemas
  privileges         = ["CREATE TABLE", "USAGE"]
  database_role_name = snowflake_database_role.migration_uploader.fully_qualified_name
  on_schema {
    schema_name = each.key
  }
}

resource "snowflake_grant_privileges_to_database_role" "migration_uploader_stage" {
  privileges         = ["USAGE"]
  database_role_name = snowflake_database_role.migration_uploader.fully_qualified_name
  on_schema_object {
    object_type = "STAGE"
    object_name = snowflake_stage.migration_stage.fully_qualified_name
  }
}
resource "snowflake_grant_privileges_to_database_role" "migration_uploader_integrates_data_format" {
  privileges         = ["USAGE"]
  database_role_name = snowflake_database_role.migration_uploader.fully_qualified_name
  on_schema_object {
    object_type = "FILE FORMAT"
    object_name = snowflake_file_format.migration_data.fully_qualified_name
  }
}
