from snowflake_client import SchemaClient
from . import (
    _utils,
)
from connection_manager import (
    ConnectionManager,
    ConnectionManagerFactory,
)
from db_migration.migration import (
    Migration,
    MigrationClients,
    MigrationTablesId,
    run_migration,
)
from etl_utils.bug import (
    Bug,
)
from fa_purity import (
    Cmd,
    PureIterFactory,
    ResultE,
)
import inspect
import logging
from redshift_client.client import (
    AwsRole,
)
from redshift_client.core.id_objs import (
    DbTableId,
    SchemaId,
)
from typing import (
    Callable,
    FrozenSet,
)

LOG = logging.getLogger(__name__)


def _migrate_full_schema(
    client: SchemaClient, schema: SchemaId, migration: Migration
) -> Cmd[ResultE[None]]:
    return _utils.chain_cmd_result(
        client.table_ids(schema),
        lambda tables: _utils.consume_results(
            PureIterFactory.from_list(tuple(tables)).map(
                lambda t: migration.migrate(MigrationTablesId(t, t))
            )
        ),
    )


def _migrate(
    clients: MigrationClients,
    migration: Migration,
    schemas: FrozenSet[SchemaId],
) -> Cmd[ResultE[None]]:
    client = clients.source.connection.cursor(LOG).map(
        clients.source.new_schema_client
    )
    migration_procedures = PureIterFactory.from_list(tuple(schemas)).map(
        lambda schema: client.bind(
            lambda c: _migrate_full_schema(c, schema, migration)
        )
    )
    return _utils.consume_results(migration_procedures)


def _with_migration_bucket(
    bucket_uri: str,
    migration_procedure: Callable[
        [MigrationClients, Migration], Cmd[ResultE[None]]
    ],
) -> Cmd[None]:
    manager = ConnectionManagerFactory.observes_manager().map(
        lambda r: Bug.assume_success(
            "connection manager", inspect.currentframe(), tuple(), r
        )
    )
    return manager.bind(
        lambda m: run_migration(
            m,
            bucket_uri,
            AwsRole(
                "arn:aws:iam::205810638802:role/observes_redshift_cluster"
            ),
            lambda c, m: migration_procedure(c, m),
        ).map(
            lambda r: Bug.assume_success(
                "run_migration", inspect.currentframe(), tuple(), r
            )
        )
    )


def migrate(schemas: FrozenSet[SchemaId]) -> Cmd[None]:
    return _with_migration_bucket(
        "s3://observes.etl-data/migration",
        lambda c, m: _migrate(c, m, schemas),
    )


def migrate_table(source: DbTableId, target: DbTableId) -> Cmd[None]:
    return _with_migration_bucket(
        "s3://observes.etl-data/migration/tables",
        lambda _, m: m.migrate(MigrationTablesId(source, target)),
    )
