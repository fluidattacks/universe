from .core import (
    migrate,
)
from fa_purity import (
    Cmd,
    PureIterFactory,
)
from redshift_client.core.id_objs import (
    Identifier,
    SchemaId,
)

ARCHIVE_SCHEMAS = (
    "checkly_old",
    "checkly_rolled_results_backup",
    "customer_success",  # fivetran
    "decripted_tables",  # fivetran
    "dynamodb_fi_forces_backup",
    "dynamodb_fi_project_access_backup",
    "formstack",
    "gsheets",  # fivetran
    "logrocket",
    "looker_scratch",
    "mailchimp_def_mailchimp",  # fivetran
    "mailchimp_def_stg_mailchimp",  # fivetran
    "moodle",
    "sendgrid",  # fivetran
    "vault",
    "zendesk",  # fivetran
)
MIGRATED_ARCHIVE = (
    "activecampaign",
    "adwords_fluid_attacks",
    "checkly_old_2",
    "checkly_rolled_results",
    "help",
    "historic_state",
    "mailchimp",
    "mailchimp_def",  # fivetran
    "mandrill",
    "matomo",
    "new_checkly_backup",
    "timedoctor_old",
)


def migrate_archive() -> Cmd[None]:
    schemas = PureIterFactory.from_list(ARCHIVE_SCHEMAS).map(
        lambda s: SchemaId(Identifier.new(s))
    )
    return migrate(frozenset(schemas))
