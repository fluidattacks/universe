from __future__ import (
    annotations,
)

from . import (
    _unload,
    _utils,
)
from ._unload import (
    TableObj,
)
from connection_manager import (
    ConnectionConf,
    ConnectionManager,
    DbClients,
    Warehouses,
    Roles,
    Databases,
)
from dataclasses import (
    dataclass,
)
from etl_utils.bug import (
    Bug,
)
from fa_purity import (
    cast_exception,
    Cmd,
    Result,
    ResultE,
)
import inspect
import logging
from redshift_client.client import (
    AwsRole,
    S3Prefix,
)
from redshift_client.core.id_objs import (
    DbTableId,
)
from redshift_client.sql_client import (
    SqlCursor,
)
from snowflake_client import (
    SnowflakeConnection,
    SnowflakeCursor,
    SnowflakeQuery,
    TableClient,
)
from typing import (
    Callable,
)

LOG = logging.getLogger(__name__)


@dataclass(frozen=True)
class _Migration:
    _source: DbClients
    _target: DbClients
    _bucket: str
    _role: AwsRole

    def export_table(
        self, _source: SnowflakeCursor, _table_obj: TableObj
    ) -> Cmd[ResultE[S3Prefix]]:
        raise NotImplementedError()

    def import_table(
        self,
        target_cursor: SnowflakeCursor,
        target: TableClient,
        table_obj: TableObj,
        target_id: DbTableId,
        prefix: S3Prefix,
    ) -> Cmd[ResultE[None]]:
        msg = Cmd.wrap_impure(
            lambda: LOG.info("Importing %s -> %s", prefix, table_obj.table_id)
        )
        success = Cmd.wrap_impure(
            lambda: LOG.info("%s completed import!", table_obj.table_id)
        )

        create_table = target.new(target_id, table_obj.table)
        _import = _unload.snowflake_load_prefix(
            target_cursor, target_id, prefix
        ).map(
            lambda r: r.alt(
                lambda e: Bug.new(
                    "import_table",
                    inspect.currentframe(),
                    e,
                    (
                        str(target_id),
                        str(table_obj),
                    ),
                )
            ).alt(cast_exception)
        )
        return msg + _utils.chain_cmd_result(
            create_table,
            lambda _: _utils.chain_cmd_result(
                _import, lambda s: success + Cmd.wrap_value(Result.success(s))
            ),
        )

    def _migrate_table(
        self, table: DbTableId, target_id: DbTableId
    ) -> Cmd[ResultE[None]]:
        new_source_table_client = self._source.connection.cursor(LOG).map(
            self._source.new_table_client
        )
        new_target_table_client = self._target.connection.cursor(LOG).map(
            self._target.new_table_client
        )
        table_obj = new_source_table_client.bind(
            lambda c: TableObj.get(c, table)
        )
        exported = _utils.chain_cmd_result(
            table_obj,
            lambda t: self._source.connection.cursor(LOG).bind(
                lambda c: self.export_table(c, t).map(
                    lambda r: r.map(lambda p: (t, p))
                )
            ),
        )
        return _utils.chain_cmd_result(
            exported,
            lambda t: self._target.connection.cursor(LOG).bind(
                lambda c: new_target_table_client.bind(
                    lambda tc: self.import_table(c, tc, t[0], target_id, t[1])
                )
            ),
        )

    def migrate(self, tables: MigrationTablesId) -> Cmd[ResultE[None]]:
        return self._migrate_table(tables.source, tables.target)


@dataclass(frozen=True)
class MigrationTablesId:
    source: DbTableId
    target: DbTableId


@dataclass(frozen=True)
class Migration:
    migrate: Callable[[MigrationTablesId], Cmd[ResultE[None]]]


@dataclass(frozen=True)
class MigrationClients:
    source: DbClients
    target: DbClients


def _setup_role(connection: SnowflakeConnection) -> Cmd[ResultE[None]]:
    return connection.cursor(LOG).bind(
        lambda c: c.execute(
            SnowflakeQuery.new_query("USE ROLE migration_uploader"), None
        )
    )


def run_migration(
    manager: ConnectionManager,
    bucket: str,
    role: AwsRole,
    procedure: Callable[[MigrationClients, Migration], Cmd[ResultE[None]]],
) -> Cmd[ResultE[None]]:
    conf = ConnectionConf(
        Warehouses.GENERIC_COMPUTE,
        Roles.ETL_UPLOADER,
        Databases.OBSERVES,
    )
    return manager.execute_with_snowflake(
        lambda c1: manager.execute_with_snowflake(
            lambda c2: _utils.chain_cmd_result(
                _setup_role(c2.connection),
                lambda _: procedure(
                    MigrationClients(c1, c2),
                    Migration(_Migration(c1, c2, bucket, role).migrate),
                ),
            ),
            conf,
        ).map(
            lambda r: r.alt(
                lambda e: e.map(
                    lambda x: x,
                    cast_exception,
                )
            )
        ),
        conf,
    ).map(
        lambda r: r.alt(
            lambda e: e.map(
                lambda x: x,
                cast_exception,
            )
        )
    )
