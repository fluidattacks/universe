from ._logger import (
    set_logger,
)
from fa_purity import (
    Unsafe,
)

__version__ = "2.1.0"

Unsafe.compute(set_logger(__name__, __version__))
