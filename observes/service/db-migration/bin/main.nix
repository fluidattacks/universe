{ inputs, makeScript, projectPath, ... }@makes_inputs:
let
  root = projectPath inputs.observesIndex.service.db_migration.root;
  bundle = import "${root}/entrypoint.nix" makes_inputs;
  env = bundle.env.runtime;
  common_vars = import (projectPath "/observes/common/common_vars.nix") {
    inherit projectPath;
  };
in makeScript {
  searchPaths = {
    bin = [ env ];
    export = common_vars;
  };
  name = "db-migration";
  entrypoint = ''db-migration "''${@}"'';
}
