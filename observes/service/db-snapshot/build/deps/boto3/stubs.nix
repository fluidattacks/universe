lib: python_pkgs:
lib.buildPythonPackage rec {
  pname = "types-boto3";
  version = "1.0.2";
  src = lib.fetchPypi {
    inherit pname version;
    sha256 = "FfP/rQMU5AoHCP7CX5SJFBT5MmAgJCK/ixm2kThTyYM=";
  };
  propagatedBuildInputs = with python_pkgs; [ boto3-stubs ];
}
