# shellcheck shell=bash

function cancel_jobs {
  local project="${1}"
  local token="${OBSERVES_UNIVERSE_API_TOKEN}"
  local threshold="${2}"

  echo "[INFO] Cancelling jobs older than ${threshold}h for ${project}" \
    && sops_export_vars 'observes/secrets/prod.yaml' \
      OBSERVES_UNIVERSE_API_TOKEN \
    && tap-gitlab clean-stuck-jobs \
      --project "${project}" \
      --api-key "${token}" \
      --threshold "${threshold}"
}

cancel_jobs '20741933' "8"
