{ inputs, makeScript, outputs, ... }:
makeScript {
  searchPaths = { bin = [ inputs.nixpkgs.python311 ]; };
  name = "observes-job-cancel-ci-jobs";
  replace = { __argCheckScript__ = ./check.py; };
  entrypoint = ./entrypoint.sh;
}
