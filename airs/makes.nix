# https://github.com/fluidattacks/makes
{ inputs, makeScript, outputs, ... }: {
  dev.airs = { source = [ outputs."/common/dev/global_deps" ]; };
  imports = [
    ./build/makes.nix
    ./config/development/makes.nix
    ./coverage/makes.nix
    ./front/i18n-unused-keys/makes.nix
    ./infra/makes.nix
    ./lint/code/makes.nix
    ./lint/content/makes.nix
    ./lint/markdownlint/makes.nix
    ./lint/md/makes.nix
    ./lint/styles/makes.nix
    ./pipeline/makes.nix
    ./test/unit/makes.nix
  ];
  jobs."/airs" = makeScript {
    replace = {
      __argAirsBuild__ = outputs."/airs/build";
      __argAirsDevelopment__ = outputs."/airs/config/development";
    };
    name = "airs";
    searchPaths = {
      bin = [
        inputs.nixpkgs.awscli
        inputs.nixpkgs.findutils
        inputs.nixpkgs.gnused
        inputs.nixpkgs.gzip
        inputs.nixpkgs.python311
        inputs.nixpkgs.utillinux
        outputs."/common/utils/bugsnag/announce"
        outputs."/common/utils/bugsnag/source-map-uploader"
      ];
      source = [
        outputs."/common/utils/aws"
        outputs."/common/utils/sops"
        outputs."/common/utils/coralogix/source-map-uploader"
      ];
    };
    entrypoint = ./entrypoint.sh;
  };
  secretsForAwsFromGitlab.prodAirs = {
    roleArn = "arn:aws:iam::205810638802:role/prod_airs";
    duration = 3600;
  };
}
