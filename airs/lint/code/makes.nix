{ inputs, makeScript, outputs, projectPath, ... }: {
  jobs."/airs/lint/code" = makeScript {
    replace = {
      __argAirsFront__ = projectPath "/airs/front";
      __argAirsSecrets__ = projectPath "/airs/secrets";
    };
    entrypoint = ./entrypoint.sh;
    name = "airs-lint-code";
    searchPaths = {
      bin = [
        inputs.nixpkgs.findutils
        inputs.nixpkgs.gnugrep
        inputs.nixpkgs.gnused
        inputs.nixpkgs.nodejs_20
        inputs.nixpkgs.utillinux
        inputs.nixpkgs.autoconf
        inputs.nixpkgs.bash
        inputs.nixpkgs.binutils.bintools
        inputs.nixpkgs.gcc
        inputs.nixpkgs.gnumake
        inputs.nixpkgs.python311
      ];
      source = [ outputs."/common/utils/aws" outputs."/common/utils/sops" ];
    };
  };
}
