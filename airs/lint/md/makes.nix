{ inputs, makeTemplate, ... }: {
  jobs."/airs/lint/md" = makeTemplate {
    replace.__argAcceptedKeywordsFile__ = ./accepted_keywords.lst;
    name = "airs-lint-md";
    searchPaths.bin =
      [ inputs.nixpkgs.diction inputs.nixpkgs.gnugrep inputs.nixpkgs.pcre ];
    template = ./template.sh;
  };
}
