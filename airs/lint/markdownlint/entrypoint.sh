# shellcheck shell=bash

function main {
  if running_in_ci_cd_provider; then
    markdownlint --config airs/lint/markdownlint/.markdownlint.jsonc 'airs/front/content'
  else
    markdownlint --fix --config airs/lint/markdownlint/.markdownlint.jsonc 'airs/front/content'
  fi
}

main "${@}"
