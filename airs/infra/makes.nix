{ outputs, ... }: {
  deployTerraform = {
    modules = {
      airsInfra = {
        setup = [
          outputs."/secretsForAwsFromGitlab/prodAirs"
          outputs."/secretsForEnvFromSops/airsInfra"
          outputs."/secretsForEnvFromSops/airsInfraProd"
          outputs."/secretsForTerraformFromEnv/airsInfraProd"
        ];
        src = "/airs/infra/src";
        version = "1.0";
      };
    };
  };
  lintTerraform = {
    modules = {
      airsInfra = {
        setup = [
          outputs."/secretsForAwsFromGitlab/dev"
          outputs."/secretsForEnvFromSops/airsInfra"
          outputs."/secretsForEnvFromSops/airsInfraDev"
          outputs."/secretsForTerraformFromEnv/airsInfraDev"
        ];
        src = "/airs/infra/src";
        version = "1.0";
      };
    };
  };
  secretsForEnvFromSops = {
    airsInfra = {
      vars = [ "CLOUDFLARE_ACCOUNT_ID" ];
      manifest = "/airs/secrets/dev.yaml";
    };
    airsInfraDev = {
      vars = [ "CLOUDFLARE_API_TOKEN" ];
      manifest = "/airs/secrets/dev.yaml";
    };
    airsInfraProd = {
      vars = [ "CLOUDFLARE_API_TOKEN_AIRS" ];
      manifest = "/airs/secrets/prod.yaml";
    };
  };
  secretsForTerraformFromEnv = {
    airsInfraDev = {
      cloudflareAccountId = "CLOUDFLARE_ACCOUNT_ID";
      cloudflareApiToken = "CLOUDFLARE_API_TOKEN";
    };
    airsInfraProd = {
      cloudflareAccountId = "CLOUDFLARE_ACCOUNT_ID";
      cloudflareApiToken = "CLOUDFLARE_API_TOKEN_AIRS";
    };
  };
  testTerraform = {
    modules = {
      airsInfra = {
        setup = [
          outputs."/secretsForAwsFromGitlab/dev"
          outputs."/secretsForEnvFromSops/airsInfra"
          outputs."/secretsForEnvFromSops/airsInfraDev"
          outputs."/secretsForTerraformFromEnv/airsInfraDev"
        ];
        src = "/airs/infra/src";
        version = "1.0";
      };
    };
  };
}
