variable "cloudflareAccountId" {
  type = string
}
variable "cloudflareApiToken" {
  type = string
}

data "cloudflare_ip_ranges" "cloudflare" {
  lifecycle {
    postcondition {
      condition     = length(self.cidr_blocks) > 0
      error_message = "CloudFlare's API returned empty list of IPs"
    }
  }
}
data "cloudflare_zones" "fluidattacks_com" {
  filter {
    name = "fluidattacks.com"
  }
}
