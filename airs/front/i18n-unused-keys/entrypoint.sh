# shellcheck shell=bash

function main {
  check_unused_translations airs/front
}

main "${@}"
