/* eslint react/forbid-component-props: 0 */
import { BsChevronRight } from "@react-icons/all-files/bs/BsChevronRight";
import { RiCloseFill } from "@react-icons/all-files/ri/RiCloseFill";
import React from "react";

import { AirsLink } from "../../AirsLink";
import { Container } from "../../Container";
import {
  BannerButton,
  BannerItem,
  BannerList,
  CloseContainer,
  InformativeBannerTitle,
} from "../styles";

interface IProps {
  buttonText: string;
  close: () => void;
  title: string;
  url: string;
}

const InformativeBannerItems: React.FC<IProps> = ({
  buttonText,
  close,
  title,
  url,
}: IProps): JSX.Element => (
  <BannerList>
    <Container
      align={"center"}
      display={"flex"}
      justify={"center"}
      minWidth={"100%"}
      width={"auto"}
    >
      <BannerItem>
        <InformativeBannerTitle>{`${title} `}</InformativeBannerTitle>
      </BannerItem>
      <BannerItem>
        <AirsLink hovercolor={"#dddde3"} href={url}>
          <BannerButton>
            {buttonText} <BsChevronRight size={15} />
          </BannerButton>
        </AirsLink>
      </BannerItem>
      <CloseContainer id={"closeInformativeBanner"} onClick={close}>
        <RiCloseFill className={"pointer white"} size={25} />
      </CloseContainer>
    </Container>
  </BannerList>
);

export { InformativeBannerItems };
