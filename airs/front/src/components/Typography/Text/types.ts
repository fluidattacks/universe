import type { ITypographyProps } from "../types";

interface ITextProps extends ITypographyProps {
  children: React.ReactNode;
}

export type { ITextProps };
