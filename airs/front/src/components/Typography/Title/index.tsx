import React from "react";
import ReactMarkdown from "react-markdown";

import { StyledTitle } from "./styles";
import type { ITitleProps } from "./types";

const Title = ({
  children,
  color,
  display,
  fontStyle,
  hColor,
  id,
  level,
  mb,
  ml,
  mr,
  mt,
  size,
  sizeMd,
  sizeSm,
  textAlign,
}: Readonly<ITitleProps>): JSX.Element => {
  if (typeof children === "string") {
    return (
      <StyledTitle
        $color={color}
        $display={display}
        $fontStyle={fontStyle}
        $hColor={hColor}
        $mb={mb}
        $ml={ml}
        $mr={mr}
        $mt={mt}
        $size={size}
        $sizeMd={sizeMd}
        $sizeSm={sizeSm}
        $textAlign={textAlign}
        as={`h${level}`}
        id={id}
      >
        <ReactMarkdown>{children}</ReactMarkdown>
      </StyledTitle>
    );
  }

  return (
    <StyledTitle
      $color={color}
      $display={display}
      $fontStyle={fontStyle}
      $hColor={hColor}
      $mb={mb}
      $ml={ml}
      $mr={mr}
      $mt={mt}
      $size={size}
      $sizeMd={sizeMd}
      $sizeSm={sizeSm}
      $textAlign={textAlign}
      as={`h${level}`}
      id={id}
    >
      {children}
    </StyledTitle>
  );
};

export { Title };
