import React from "react";

import type { IBlogCta } from "./types";

import { Container } from "../Container";
import { CtaBanner } from "../CtaBanner";

const BlogCta = ({
  buttontxt,
  link,
  title,
  paragraph,
}: Readonly<IBlogCta>): JSX.Element => (
  <Container pv={3}>
    <CtaBanner
      button1Link={link}
      button1Text={buttontxt}
      buttonClassName={"btn-rompe-trafico"}
      paragraph={paragraph}
      pv={4}
      size={"xs"}
      textSize={"medium"}
      title={title}
      variant={"dark"}
    />
  </Container>
);

export { BlogCta };
