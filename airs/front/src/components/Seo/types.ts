import type { IMetaItem } from "../GatsbyHead/types";

type TType = "blog" | "default" | "post" | "service";

interface ISeoProps {
  title: string;
  description: string;
  path?: string;
  author?: string;
  keywords?: string;
  meta?: IMetaItem[];
  image?: string;
  type?: TType;
  date?: string;
  dateModified?: string;
}

export type { ISeoProps, IMetaItem, TType };
