import { render, screen } from "@testing-library/react";
import i18next from "i18next";
import React from "react";

import { AirsLink } from ".";

const TestComponent = (): JSX.Element => {
  return (
    <div>
      <AirsLink hovercolor={"#b0b0bf"} href={"https://docs.fluidattacks.com/"}>
        {"External Link Allowed"}
      </AirsLink>
      <AirsLink href={"https://google.com"}>
        {"External Link Unallowed"}
      </AirsLink>
      <AirsLink hovercolor={"#b0b0bf"} href={"/"}>
        {"Link"}
      </AirsLink>
      <AirsLink href={"/careers/"}>{"No Spanish Reference Link"}</AirsLink>
    </div>
  );
};

describe(`AirsLink Component non translate`, (): void => {
  test("Componet should render correctly", (): void => {
    expect.hasAssertions();

    render(<TestComponent />);

    expect(screen.queryByText("Link")).toBeInTheDocument();
  });
  test("Componet should have english href", (): void => {
    expect.hasAssertions();

    render(<TestComponent />);

    const linkElement = screen.getByText("Link");

    expect(linkElement).toHaveAttribute("href", "/");
  });
});

describe("AirsLink component translate", (): void => {
  beforeAll((): void => {
    jest.replaceProperty(i18next, "language", "es");
  });

  afterAll((): void => {
    jest.restoreAllMocks();
  });

  test("Component should have spanish href", (): void => {
    expect.hasAssertions();

    render(<TestComponent />);

    const linkElement = screen.getByText("Link");

    expect(linkElement).toHaveAttribute("href", "/es/");
  });

  test("Component should not change href if it doesnt exists", (): void => {
    expect.hasAssertions();

    render(<TestComponent />);

    const linkElement = screen.getByText("No Spanish Reference Link");

    expect(linkElement).toHaveAttribute("href", "/careers/");
  });
});

describe("Airslink external href", (): void => {
  test("External allowed link", (): void => {
    expect.hasAssertions();

    render(<TestComponent />);

    const linkElement = screen.getByText("External Link Allowed");
    expect(linkElement).toHaveAttribute(
      "href",
      "https://docs.fluidattacks.com/",
    );
    expect(linkElement).toHaveAttribute("rel", "noopener");
  });

  test("External link unallowed", (): void => {
    expect.hasAssertions();

    render(<TestComponent />);

    const linkElement = screen.getByText("External Link Unallowed");
    expect(linkElement).toHaveAttribute("href", "https://google.com");
    expect(linkElement).toHaveAttribute("rel", "nofollow noopener noreferrer");
  });
});
