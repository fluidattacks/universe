import type { MouseEventHandler } from "react";

type TDecorations = "none" | "underline";

interface ITransientProps {
  decoration?: TDecorations;
  hovercolor?: string;
}
interface ILinkProps extends ITransientProps {
  onClick?: MouseEventHandler<HTMLAnchorElement> &
    MouseEventHandler<HTMLDivElement>;
}

type TStyledLinkProps = TPrefixWithDollar<ILinkProps>;

export type { ILinkProps, TDecorations, TStyledLinkProps };
