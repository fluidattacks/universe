/* eslint react/forbid-component-props: 0 */
import React from "react";

import { ClientsMenuButtons } from "./ClientsMenuButtons";

import {
  CardContainer,
  CardHeader,
  CardsContainer,
  MenuList,
} from "../../styles/styles";
import clientsData from "../../utils/clients.json";
import { CloudImage } from "../CloudImage";

const ClientsPage: React.FC = (): JSX.Element => {
  return (
    <React.Fragment>
      <div className={"flex flex-nowrap"}>
        <MenuList>
          <ClientsMenuButtons />
        </MenuList>
      </div>
      <CardsContainer>
        {clientsData.map((client): JSX.Element => {
          const { alt, clientlogo, filter, title } = client;

          return (
            <CardContainer
              className={`all-clients-cards ${filter}-cards`}
              key={clientlogo}
            >
              <CardHeader>
                <CloudImage alt={alt} src={`/airs/clients/${clientlogo}`} />
                <br />
                <React.Fragment>
                  <br />
                  <div className={"mb5"}>
                    <h4>{title}</h4>
                  </div>
                </React.Fragment>
              </CardHeader>
            </CardContainer>
          );
        })}
      </CardsContainer>
    </React.Fragment>
  );
};

export { ClientsPage };
