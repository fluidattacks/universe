import "@testing-library/jest-dom";
import { fireEvent, render, screen } from "@testing-library/react";
import React from "react";

import { ClientsPage } from ".";

jest.mock(
  "../../utils/clients.json",
  (): [
    {
      clientlogo: string;
      filter: string;
      alt: string;
      title: string;
    },
  ] => [
    {
      alt: "Alt content",
      clientlogo: "Client logo content",
      filter: "construction",
      title: "Title content",
    },
  ],
);

describe(`ClientsPage`, (): void => {
  afterEach((): void => {
    jest.restoreAllMocks();
  });

  it("ClientsPage should render mocked data", (): void => {
    expect.hasAssertions();

    render(<ClientsPage />);

    expect(screen.queryByText("Image not found")).toBeInTheDocument();
    expect(screen.queryByText("Title content")).toBeInTheDocument();
  });

  it("ClientsPage should allow filter existing cards", (): void => {
    expect.hasAssertions();

    render(<ClientsPage />);

    const constructionRadioInput = screen.getByLabelText(
      "clients.buttons.construction",
    );
    expect(constructionRadioInput).not.toBeChecked();
    fireEvent.click(constructionRadioInput);
    expect(constructionRadioInput).toBeChecked();
  });

  it("ClientsPage should allow filter not existing cards", (): void => {
    expect.hasAssertions();

    render(<ClientsPage />);

    const retailRadioInput = screen.getByLabelText("clients.buttons.retail");
    expect(retailRadioInput).not.toBeChecked();
    fireEvent.click(retailRadioInput);
    expect(retailRadioInput).toBeChecked();
  });
});
