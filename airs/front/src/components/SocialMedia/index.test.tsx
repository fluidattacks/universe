import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import React from "react";

import { SocialMedia } from ".";

describe("SocialMedia Component", (): void => {
  test("renders correct Facebook link", (): void => {
    expect.hasAssertions();
    const socialMediaUrls = [
      "https://www.facebook.com/Fluid-Attacks-267692397253577/",
      "https://www.linkedin.com/company/fluidattacks/",
      "https://twitter.com/fluidattacks/",
      "https://www.youtube.com/c/fluidattacks/",
      "https://www.instagram.com/fluidattacks/",
    ];

    render(<SocialMedia />);

    socialMediaUrls.forEach((url: string, index: number): void => {
      const linkElement = screen.getAllByRole("link")[index];
      expect(linkElement).toHaveAttribute("href", url);

      const socialMediaIcon = linkElement.querySelector(".f3.c-fluid-gray");
      expect(socialMediaIcon).toBeInTheDocument();
    });
  });
});
