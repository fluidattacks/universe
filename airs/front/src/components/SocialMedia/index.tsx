/* eslint react/forbid-component-props: 0 */
import { FaFacebookF } from "@react-icons/all-files/fa/FaFacebookF";
import { FaInstagram } from "@react-icons/all-files/fa/FaInstagram";
import { FaLinkedinIn } from "@react-icons/all-files/fa/FaLinkedinIn";
import { FaTwitter } from "@react-icons/all-files/fa/FaTwitter";
import { FaYoutube } from "@react-icons/all-files/fa/FaYoutube";
import React from "react";

import { SocialMediaLink } from "../../styles/styles";
import { AirsLink } from "../AirsLink";

const SocialMedia: React.FC = (): JSX.Element => (
  <React.Fragment>
    <AirsLink href={"https://www.facebook.com/Fluid-Attacks-267692397253577/"}>
      <SocialMediaLink>
        <FaFacebookF className={"f3 c-fluid-gray mh1"} />
      </SocialMediaLink>
    </AirsLink>
    <AirsLink href={"https://www.linkedin.com/company/fluidattacks/"}>
      <SocialMediaLink>
        <FaLinkedinIn className={"f3 c-fluid-gray"} />
      </SocialMediaLink>
    </AirsLink>
    <AirsLink href={"https://twitter.com/fluidattacks/"}>
      <SocialMediaLink>
        <FaTwitter className={"f3 c-fluid-gray"} />
      </SocialMediaLink>
    </AirsLink>
    <AirsLink href={"https://www.youtube.com/c/fluidattacks/"}>
      <SocialMediaLink>
        <FaYoutube className={"f3 c-fluid-gray"} />
      </SocialMediaLink>
    </AirsLink>
    <AirsLink href={"https://www.instagram.com/fluidattacks/"}>
      <SocialMediaLink>
        <FaInstagram className={"f3 c-fluid-gray"} />
      </SocialMediaLink>
    </AirsLink>
  </React.Fragment>
);

export { SocialMedia };
