import React from "react";

import { Container } from "./styles";

import { IframeContainer } from "../../styles/styles";
import { translate } from "../../utils/translations/translate";
import { Text, Title } from "../Typography";

const InternalForm: React.FC = (): JSX.Element => {
  return (
    <Container $bgColor={"#2e2e38"}>
      <Title color={"#f4f4f6"} level={1} mb={2} size={"big"}>
        {translate.t("internalForm.title")}
      </Title>
      <Text color={"#f4f4f6"} mb={4} size={"big"}>
        {translate.t("internalForm.subTitle")}
      </Text>
      <IframeContainer>
        <iframe
          sandbox={
            "allow-forms allow-top-navigation allow-same-origin allow-scripts"
          }
          src={
            "https://forms.zohopublic.com/fluidattacks1/form/Internalcontactus/formperma/A8qDPgOuvnpp21GF3c56RaPMQOZcJWMxUTYf6RTKntM"
          }
          style={{
            border: "0",
            height: "850px",
            marginBottom: "-7px",
            width: "100%",
          }}
          title={"Contact Us Form"}
        />
      </IframeContainer>
    </Container>
  );
};

export { InternalForm };
