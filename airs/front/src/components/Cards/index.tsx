/* eslint react/forbid-component-props: 0 */
import parse from "html-react-parser";
import React from "react";
import sanitizeHtml from "sanitize-html";

import type { ICardProps } from "./types";

import { WebsiteWrapper } from "../../scenes/WebsiteWrapper";
import {
  BannerContainer,
  BannerTitle,
  CardsContainer1200,
  FullWidthContainer,
  PageArticle,
} from "../../styles/styles";

const Card = ({
  children,
  title,
  html,
  banner,
}: Readonly<ICardProps>): JSX.Element => {
  const sanitizedHTML = sanitizeHtml(html, {
    allowedClasses: {
      "*": false,
    },
  });

  return (
    <WebsiteWrapper>
      <PageArticle $bgColor={"#f9f9f9"}>
        <BannerContainer className={banner}>
          <FullWidthContainer>
            <BannerTitle>{title}</BannerTitle>
          </FullWidthContainer>
        </BannerContainer>
        <CardsContainer1200>
          <div>{parse(sanitizedHTML)}</div>
          {children}
        </CardsContainer1200>
      </PageArticle>
    </WebsiteWrapper>
  );
};

export { Card };
