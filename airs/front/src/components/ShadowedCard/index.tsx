/* eslint-disable react/forbid-component-props */
import React from "react";

import {
  RedParagraph,
  SmallBlackText,
  SmallGrayText,
  WhiteCardContainer,
  WhiteParagraph,
} from "./styles";
import type { IShadowedCardProps } from "./types";

import { SquaredCardContainer } from "../../styles/styles";
import { CloudImage } from "../CloudImage";

const ShadowedCard = ({
  color,
  image,
  number,
  text,
}: IShadowedCardProps): JSX.Element => {
  if (color === "bg-white") {
    return (
      <WhiteCardContainer>
        <SquaredCardContainer className={color}>
          <CloudImage
            alt={"card-image"}
            src={image}
            styles={"solution-card-icon mt4"}
          />
          <React.Fragment>
            <RedParagraph>{number}</RedParagraph>
            <SmallBlackText>{text}</SmallBlackText>
          </React.Fragment>
        </SquaredCardContainer>
      </WhiteCardContainer>
    );
  }

  return (
    <WhiteCardContainer>
      <SquaredCardContainer className={color}>
        <CloudImage
          alt={"card-image"}
          src={image}
          styles={"solution-card-icon mt4"}
        />
        {color === "bg-black-18" ? (
          <React.Fragment>
            <WhiteParagraph>{number}</WhiteParagraph>
            <SmallGrayText>{text}</SmallGrayText>
          </React.Fragment>
        ) : undefined}
      </SquaredCardContainer>
    </WhiteCardContainer>
  );
};

export { ShadowedCard };
