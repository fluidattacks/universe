import "@testing-library/jest-dom";
import { render } from "@testing-library/react";
import { useStaticQuery } from "gatsby";
import React from "react";

import { ShadowedCard } from ".";
import { allCloudinaryMediaData } from "test-utils/mocks";
import type { ICloudinaryMedia } from "utils/types";

interface IDataImageMock {
  allCloudinaryMedia: ICloudinaryMedia;
}

const mockUseStaticQuery = jest.spyOn({ useStaticQuery }, `useStaticQuery`);
const mockDataUseStaticQuery: IDataImageMock = {
  allCloudinaryMedia: allCloudinaryMediaData,
};

describe("ShadowedCard", (): void => {
  beforeEach((): void => {
    mockUseStaticQuery.mockImplementation(
      (): IDataImageMock => mockDataUseStaticQuery,
    );
  });

  afterEach((): void => {
    jest.restoreAllMocks();
  });
  it("should render when color = bg-white", (): void => {
    expect.hasAssertions();

    const { getByText } = render(
      <ShadowedCard
        color={"bg-white"}
        image={"image.png"}
        number={"1"}
        text={"This is a text"}
      />,
    );

    expect(getByText("1")).toHaveClass("c-dkred");
    expect(getByText("This is a text")).toHaveClass("c-fluid-bk");
  });
  it("should render when color is not bg-white", (): void => {
    expect.hasAssertions();

    const { getByText } = render(
      <ShadowedCard
        color={"bg-black-18"}
        image={"image.png"}
        number={"1"}
        text={"This is a text"}
      />,
    );

    expect(getByText("1")).toHaveClass("white");
    expect(getByText("This is a text")).toHaveClass("c-fluid-gray");
  });
  it("should render default props", (): void => {
    expect.hasAssertions();

    const { container } = render(
      <ShadowedCard color={""} image={"image.png"} number={""} text={""} />,
    );

    expect(container).toBeInTheDocument();
  });
});
