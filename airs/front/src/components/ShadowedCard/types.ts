interface IShadowedCardProps {
  color: string;
  image: string;
  number?: string;
  text?: string;
}

export type { IShadowedCardProps };
