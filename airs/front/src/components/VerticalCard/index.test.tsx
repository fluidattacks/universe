import "@testing-library/jest-dom";
import { render } from "@testing-library/react";
import { useStaticQuery } from "gatsby";
import React from "react";

import { VerticalCard } from ".";
import { allCloudinaryMediaData } from "test-utils/mocks";
import type { ICloudinaryMedia } from "utils/types";

interface IDataImageMock {
  allCloudinaryMedia: ICloudinaryMedia;
}

const mockUseStaticQuery = jest.spyOn({ useStaticQuery }, `useStaticQuery`);
const mockDataUseStaticQuery: IDataImageMock = {
  allCloudinaryMedia: allCloudinaryMediaData,
};

describe("VerticalCard", (): void => {
  beforeEach((): void => {
    mockUseStaticQuery.mockImplementation(
      (): IDataImageMock => mockDataUseStaticQuery,
    );
  });

  afterEach((): void => {
    jest.restoreAllMocks();
  });
  it("should render VerticalCard container", (): void => {
    expect.hasAssertions();

    const { container } = render(
      <VerticalCard
        alt={"alt"}
        author={"test@fluidattacks.com"}
        btnText={"btnText"}
        date={"2023-01-02"}
        description={"description"}
        image={"image"}
        imagePadding={true}
        link={"link"}
        minWidth={"344px"}
        minWidthSm={"270px"}
        subtitle={"subtitle"}
        title={"title"}
        titleMinHeight={"80px"}
      />,
    );

    expect(container).toBeInTheDocument();
  });

  it("should render VerticalCard container with image that stats with https", (): void => {
    expect.hasAssertions();

    const { container } = render(
      <VerticalCard
        alt={"alt"}
        author={"test@fluidattacks.com"}
        btnText={"btnText"}
        date={"2023-01-02"}
        description={"description"}
        image={"https://example.com"}
        imagePadding={true}
        link={"link"}
        minWidth={"344px"}
        minWidthSm={"270px"}
        subtitle={"subtitle"}
        title={"title"}
        titleMinHeight={"80px"}
      />,
    );

    expect(container).toBeInTheDocument();
  });

  it("should render VerticalCard container without author, date and subtitle", (): void => {
    expect.hasAssertions();

    const { container } = render(
      <VerticalCard
        alt={"alt"}
        btnText={"btnText"}
        description={"description"}
        image={"image"}
        imagePadding={true}
        link={"link"}
        minWidth={"344px"}
        minWidthSm={"270px"}
        title={"title"}
        titleMinHeight={"80px"}
      />,
    );

    expect(container).toBeInTheDocument();
  });

  it("should render VerticalCard container without author, date and subtitle with image that stats with https", (): void => {
    expect.hasAssertions();

    const { container } = render(
      <VerticalCard
        alt={"alt"}
        btnText={"btnText"}
        description={"description"}
        image={"https://example.com"}
        imagePadding={true}
        link={"link"}
        minWidth={"344px"}
        minWidthSm={"270px"}
        title={"title"}
        titleMinHeight={"80px"}
      />,
    );

    expect(container).toBeInTheDocument();
  });
});
