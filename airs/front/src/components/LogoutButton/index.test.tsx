import { useAuth0 } from "@auth0/auth0-react";
import { fireEvent, render, screen } from "@testing-library/react";
import React from "react";
import { act } from "react-dom/test-utils";

import { LogoutButton } from ".";

describe("LogoutButton", (): void => {
  it('renders "Log Out" button', (): void => {
    (useAuth0 as jest.Mock).mockReturnValue({
      isAuthenticated: true,
      user: { name: "User@fluidattacks.com" },
    });

    render(<LogoutButton />);

    const logoutButton = screen.getByText("Log Out");
    expect(logoutButton).toBeInTheDocument();
  });

  it('calls logout when "Log Out" button is clicked', (): void => {
    const mockLogout = jest.fn();
    (useAuth0 as jest.Mock).mockReturnValue({
      isAuthenticated: true,
      logout: mockLogout,
      user: { name: "User@fluidattacks.com" },
    });

    render(<LogoutButton />);

    const logoutButton = screen.getByText("Log Out");

    act((): void => {
      fireEvent.click(logoutButton);
    });

    expect(mockLogout).toHaveBeenCalledWith({
      logoutParams: { returnTo: window.location.origin },
    });
  });
});
