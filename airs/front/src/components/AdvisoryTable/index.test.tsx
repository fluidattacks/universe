import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import React from "react";

import { SummaryTable } from "./SummaryTable";
import { VulnerabilityTable } from "./VulnerabilityTable";

describe("SummaryTable", (): void => {
  it("SummaryTable should show mocked data", (): void => {
    expect.hasAssertions();

    render(
      <SummaryTable
        affected-versions={"Affected versions content"}
        code={"Code content"}
        fixed-versions={"Fixed versions content"}
        name={"Name content"}
        product={"Product content"}
        release={"Release content"}
        state={"State content"}
      />,
    );

    expect(screen.queryByText("Affected versions content")).toBeInTheDocument();
    expect(screen.queryByText("Code content")).toBeInTheDocument();
    expect(screen.queryByText("Fixed versions content")).toBeInTheDocument();
    expect(screen.queryByText("Name content")).toBeInTheDocument();
    expect(screen.queryByText("Product content")).toBeInTheDocument();
    expect(screen.queryByText("Release content")).toBeInTheDocument();
    expect(screen.queryByText("State content")).toBeInTheDocument();
  });

  it("SummaryTable should show empty data", (): void => {
    expect.hasAssertions();

    render(
      <SummaryTable
        affected-versions={""}
        code={"Code content"}
        fixed-versions={""}
        name={"Name content"}
        product={"Product content"}
        release={""}
        state={""}
      />,
    );

    expect(
      screen.getByTestId("empty-affected-versions-row"),
    ).toBeInTheDocument();
    expect(screen.getByTestId("empty-fixed-versions-row")).toBeInTheDocument();
    expect(screen.getByTestId("empty-state-row")).toBeInTheDocument();
    expect(screen.getByTestId("empty-release-row")).toBeInTheDocument();
  });
});

describe("VulnerabilityTable", (): void => {
  it("VulnerabilityTable should show mocked data", (): void => {
    expect.hasAssertions();

    render(
      <VulnerabilityTable
        available={"Available content"}
        id={"Id content"}
        kind={"Kind content"}
        remote={"Remote content"}
        rule={"Rule content"}
        score={"Score content"}
        vector={"Vector content"}
      />,
    );

    expect(screen.queryByText("Available content")).toBeInTheDocument();
    expect(screen.queryByText("Id content")).toBeInTheDocument();
    expect(screen.queryByText("Kind content")).toBeInTheDocument();
    expect(screen.queryByText("Remote content")).toBeInTheDocument();
    expect(screen.queryByText("Rule content")).toBeInTheDocument();
    expect(screen.queryByText("Score content")).toBeInTheDocument();
    expect(screen.queryByText("Vector content")).toBeInTheDocument();
  });
});
