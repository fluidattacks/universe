import { render } from "@testing-library/react";
import { useStaticQuery } from "gatsby";
import React from "react";

import { CloudImage } from ".";
import { allCloudinaryMediaData } from "test-utils/mocks";
import type { ICloudinaryMedia } from "utils/types";

interface IDataImageMock {
  allCloudinaryMedia: ICloudinaryMedia;
}

const mockUseStaticQuery = jest.spyOn({ useStaticQuery }, `useStaticQuery`);
const mockDataUseStaticQuery: IDataImageMock = {
  allCloudinaryMedia: allCloudinaryMediaData,
};

describe("CloudImage", (): void => {
  beforeEach((): void => {
    mockUseStaticQuery.mockImplementation(
      (): IDataImageMock => mockDataUseStaticQuery,
    );
  });

  afterEach((): void => {
    jest.restoreAllMocks();
  });
  it("renders image elements", (): void => {
    expect.hasAssertions();

    const { getByAltText } = render(
      <CloudImage
        alt={"Test Alt"}
        height={100}
        src={"url"}
        styles={"test-styles"}
        width={100}
      />,
    );

    const imageElement = getByAltText("Test Alt") as HTMLImageElement;

    expect(imageElement).toBeInTheDocument();
    expect(imageElement.src).toContain("http://localhost/url.webp");
    expect(imageElement.alt).toBe("Test Alt");
    expect(imageElement.className).toBe("test-styles");
    expect(imageElement.width).toBe(100);
    expect(imageElement.height).toBe(100);
  });
  it("renders image elements without width and height", (): void => {
    expect.hasAssertions();

    const { getByAltText } = render(
      <CloudImage alt={"Test Alt"} src={"url"} styles={"test-styles"} />,
    );

    const imageElement = getByAltText("Test Alt") as HTMLImageElement;

    expect(imageElement).toBeInTheDocument();
    expect(imageElement.src).toContain("http://localhost/url.webp");
    expect(imageElement.alt).toBe("Test Alt");
    expect(imageElement.className).toBe("test-styles");
  });
});
