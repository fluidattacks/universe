import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import { useStaticQuery } from "gatsby";
import React from "react";

import { ServicesPage } from ".";
import { allCloudinaryMediaData } from "test-utils/mocks";
import type { ICloudinaryMedia } from "utils/types";

interface IDataServicesPageMock {
  allCloudinaryMedia: ICloudinaryMedia;
  allMarkdownRemark: {
    edges: [
      {
        node: {
          fields: {
            slug: string;
          };
          frontmatter: {
            definition: string;
            image: string;
            slug: string;
            template: string;
            title: string;
          };
        };
      },
    ];
  };
}

const mockUseStaticQuery = jest.spyOn({ useStaticQuery }, `useStaticQuery`);
const mockDataUseStaticQuery: IDataServicesPageMock = {
  allCloudinaryMedia: allCloudinaryMediaData,
  allMarkdownRemark: {
    edges: [
      {
        node: {
          fields: {
            slug: "es",
          },
          frontmatter: {
            definition: "Definition content",
            image: "url",
            slug: "Slug content",
            template: "Template content",
            title: "Title content",
          },
        },
      },
    ],
  },
};

describe("ServicesPage", (): void => {
  beforeEach((): void => {
    mockUseStaticQuery.mockImplementation(
      (): IDataServicesPageMock => mockDataUseStaticQuery,
    );
  });
  afterEach((): void => {
    jest.restoreAllMocks();
  });
  it("ServicesPage should render mocked data", (): void => {
    expect.hasAssertions();

    render(<ServicesPage />);

    expect(screen.queryByText("Image not found")).toBeInTheDocument();
    expect(screen.queryByText("Title content")).toBeInTheDocument();
    expect(screen.queryByText("Go to service")).toBeInTheDocument();
  });
});
