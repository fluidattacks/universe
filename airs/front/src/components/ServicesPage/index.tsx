/* eslint react/forbid-component-props: 0 */
import { graphql, useStaticQuery } from "gatsby";
import React from "react";

import { CardContainer, Container } from "./styles";

import { PhantomRegularRedButton } from "../../styles/styles";
import { AirsLink } from "../AirsLink";
import { CloudImage } from "../CloudImage";
import { Text, Title } from "../Typography";

const ServicesPage: React.FC = (): JSX.Element => {
  const data: IData = useStaticQuery(graphql`
    query ServicesQuery {
      allMarkdownRemark(
        filter: { frontmatter: { template: { eq: "service" } } }
      ) {
        edges {
          node {
            fields {
              slug
            }
            frontmatter {
              definition
              slug
              title
              image
              template
            }
          }
        }
      }
    }
  `);

  const servicesCards = data.allMarkdownRemark.edges.filter(
    (serviceCard): boolean => !serviceCard.node.fields.slug.includes("/es/"),
  );

  return (
    <Container>
      {servicesCards.map((serviceCard): JSX.Element => {
        const { definition, image, slug, title } = serviceCard.node.frontmatter;

        return (
          <CardContainer key={title}>
            <CloudImage alt={title} src={`airs/${image}`} styles={"w-100"} />
            <Title color={"#2e2e38"} level={2} mb={1} mt={1} size={"small"}>
              {title}
            </Title>
            <Text color={"#5c5c70"} mb={1} size={"medium"}>
              {definition}
            </Text>
            <AirsLink href={`/${slug}`}>
              <PhantomRegularRedButton>
                {"Go to service"}
              </PhantomRegularRedButton>
            </AirsLink>
          </CardContainer>
        );
      })}
    </Container>
  );
};

export { ServicesPage };
