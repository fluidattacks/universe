import "@testing-library/jest-dom";
import { fireEvent, render } from "@testing-library/react";
import React from "react";

import { TableOfContents } from ".";

describe("ShadowedCard", (): void => {
  it("should render", (): void => {
    expect.hasAssertions();

    const { getByText } = render(
      <TableOfContents headings={[{ depth: 2, value: "head1" }]} />,
    );

    fireEvent.click(getByText("head1"));

    expect(getByText("head1")).toBeInTheDocument();
  });
});
