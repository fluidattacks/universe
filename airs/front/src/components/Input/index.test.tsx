import { fireEvent, render, screen } from "@testing-library/react";
import React from "react";

import { DateInput } from "./Date";
import { Select } from "./Select";
import { TextArea } from "./TextArea";

describe("Input", (): void => {
  describe("DateInput", (): void => {
    it("renders with label", (): void => {
      expect.hasAssertions();

      render(<DateInput label={"Test Label"} onChange={jest.fn()} />);
      expect(screen.getByText("Test Label")).toBeInTheDocument();
    });

    it("renders without label", (): void => {
      expect.hasAssertions();

      render(<DateInput onChange={jest.fn()} />);
      expect(screen.queryByText(/label/u)).not.toBeInTheDocument();
    });

    describe("TextArea", (): void => {
      expect.hasAssertions();

      it("should render label text when provided", (): void => {
        render(<TextArea label={"Test Label"} onChange={jest.fn()} />);
        expect(screen.getByText("Test Label")).toBeInTheDocument();
      });

      it("should call onChange when input value changes", (): void => {
        expect.hasAssertions();

        const onChange = jest.fn();
        render(<TextArea onChange={onChange} />);

        const textarea = screen.getByRole("textbox");
        fireEvent.change(textarea, { target: { value: "new value" } });

        expect(onChange).toHaveBeenCalledWith("new value");
      });

      it("should render placeholder text", (): void => {
        expect.hasAssertions();

        render(
          <TextArea onChange={jest.fn()} placeHolder={"Test placeholder"} />,
        );
        expect(
          screen.getByPlaceholderText("Test placeholder"),
        ).toBeInTheDocument();
      });
    });

    describe("Select", (): void => {
      expect.hasAssertions();

      const options = ["Option 1", "option 2"];
      const onChange = jest.fn();

      it("renders label", (): void => {
        render(
          <Select
            label={"Test Label"}
            onChange={onChange}
            options={options}
            value={"All"}
          />,
        );

        expect(screen.getByText("Test Label")).toBeInTheDocument();
      });

      it("capitalizes option values", (): void => {
        expect.hasAssertions();

        render(
          <Select
            label={"Test Label"}
            onChange={onChange}
            options={options}
            value={"none"}
          />,
        );

        expect(screen.getByText("Option 1")).toBeInTheDocument();
        expect(screen.getByText("Option 2")).toBeInTheDocument();
      });

      it("renders without label and value", (): void => {
        expect.hasAssertions();

        render(<Select onChange={onChange} options={options} />);

        expect(screen.queryByText(/label/u)).not.toBeInTheDocument();
      });

      it("should call onChange when input value changes", (): void => {
        expect.hasAssertions();

        render(<Select onChange={onChange} options={options} />);

        const select = screen.getByRole("combobox");
        fireEvent.change(select, { target: { value: "" } });

        expect(onChange).toHaveBeenCalledWith("");
      });
    });
  });
});
