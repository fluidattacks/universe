import React, { useCallback } from "react";

import { StyledSelect } from "./styles";
import type { ISelectProps } from "./types";

import { capitalizePlainString } from "../../../utils/utilities";
import { Container } from "../../Container";
import { Text } from "../../Typography";

const Select = ({
  label = "",
  onChange,
  options,
  value = "All",
}: Readonly<ISelectProps>): JSX.Element => {
  const onChangeEvent = useCallback(
    (event: React.ChangeEvent<HTMLSelectElement>): void => {
      onChange(event.target.value);
    },
    [onChange],
  );

  return (
    <Container>
      {label ? (
        <Text color={"#2e2e38"} size={"small"}>
          {label}
        </Text>
      ) : undefined}
      <StyledSelect onChange={onChangeEvent}>
        <option value={value}>{value}</option>
        {options.map((opt): JSX.Element => {
          return (
            <option key={opt} value={opt}>
              {capitalizePlainString(opt)}
            </option>
          );
        })}
      </StyledSelect>
    </Container>
  );
};

export { Select };
