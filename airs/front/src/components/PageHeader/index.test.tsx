import { render } from "@testing-library/react";
import React from "react";

import { PageHeader } from ".";

describe("PageHeader", (): void => {
  it("renders title when no banner", (): void => {
    const { getByText } = render(
      <PageHeader
        banner={"banner"}
        pageWithBanner={false}
        slug={"slug"}
        subtext={"subtexts"}
        subtitle={"Subtitle"}
        title={"Test Title"}
      />,
    );
    expect(getByText("Test Title")).toBeInTheDocument();
  });

  it("renders title whitout subtitle and banner", (): void => {
    const { getByText } = render(
      <PageHeader
        banner={"banner"}
        pageWithBanner={false}
        slug={"slug"}
        subtext={"subtexts"}
        subtitle={""}
        title={"Test Title"}
      />,
    );
    expect(getByText("Test Title")).toBeInTheDocument();
  });
  it("renders banner when pageWithBanner=true", (): void => {
    const { getByText } = render(
      <PageHeader
        banner={"banner"}
        pageWithBanner={true}
        slug={"slug"}
        subtext={"subtexts"}
        subtitle={"Subtitle"}
        title={"Test Title"}
      />,
    );
    expect(getByText("Test Title")).toBeInTheDocument();
    expect(getByText("Subtitle")).toBeInTheDocument();
  });

  it("renders correct banner for careers page", (): void => {
    const { container } = render(
      <PageHeader
        banner={"banner"}
        pageWithBanner={true}
        slug={"careers/"}
        subtext={"subtexts"}
        subtitle={"Careers"}
        title={"Test Title"}
      />,
    );
    expect(container).toBeInTheDocument();
  });
  it("renders without subtittle", (): void => {
    const { container } = render(
      <PageHeader
        banner={"banner"}
        pageWithBanner={true}
        slug={"careers/"}
        subtext={"subtexts"}
        subtitle={""}
        title={"Test Title"}
      />,
    );
    expect(container).toBeInTheDocument();
  });
});
