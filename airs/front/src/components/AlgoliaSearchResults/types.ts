import type { Hit as IHit } from "instantsearch.js";

interface IResultProps {
  hit: IHit<{
    description?: string;
    keywords?: string;
    slug: string;
    title: string;
  }>;
}

export type { IResultProps };
