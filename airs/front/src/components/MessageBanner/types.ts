interface IMessageBannerDataProps {
  additionalDescription?: string;
  buttonText: string;
  linkButton: string;
  message: JSX.Element | string;
  mobileDescription?: string;
}

interface IMessageBannerFunctionalProps {
  onClose?: () => void;
}

type IMessageBannerProps = IMessageBannerDataProps &
  IMessageBannerFunctionalProps;

export type { IMessageBannerProps };
