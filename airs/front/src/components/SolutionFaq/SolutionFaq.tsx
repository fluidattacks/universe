import { FaMinus } from "@react-icons/all-files/fa/FaMinus";
import { FaPlus } from "@react-icons/all-files/fa/FaPlus";
import React, { useCallback, useState } from "react";

import type { IFaqProps } from "./types";

import { Container } from "../Container";
import { Title } from "../Typography";

const SolutionFaq: React.FC<IFaqProps> = ({ children, title }): JSX.Element => {
  const [description, setDescription] = useState("none");
  const [icon, setIcon] = useState("plus");

  const showDescription = useCallback((): void => {
    if (description === "none") {
      setDescription("block");
    } else {
      setDescription("none");
    }
    if (icon === "plus") {
      setIcon("minus");
    } else {
      setIcon("plus");
    }
  }, [description, icon]);

  return (
    <Container borderBottomColor={"#dddde3"} onClick={showDescription}>
      <Container
        align={"center"}
        display={"flex"}
        justify={"between"}
        maxWidth={"1200px"}
        pv={3}
        width={"100%"}
        wrap={"wrap"}
      >
        <Container width={"80%"}>
          <Title color={"#2e2e38"} level={3} size={"xs"}>
            {title}
          </Title>
        </Container>
        <Container display={"flex"} justify={"end"} mr={0} width={"20%"}>
          {icon === "plus" ? <FaPlus /> : <FaMinus />}
        </Container>
      </Container>
      <Container
        display={description === "none" ? "none" : "block"}
        justify={"start"}
      >
        {children}
      </Container>
    </Container>
  );
};

export { SolutionFaq };
