import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import React from "react";

import { Hero } from ".";

describe("Hero", (): void => {
  it("Hero should render mocked data", (): void => {
    expect.hasAssertions();

    render(
      <Hero
        button1Link={"Button 1 Link content"}
        button1Text={"Button 1 Text content"}
        button2Link={"Button 2 Link content"}
        button2Text={"Button 2 Text content"}
        image={"Image content"}
        paragraph={"Paragraph content"}
        subtitle={"Subtile content"}
        title={"Title content"}
        tone={"darkTransparent"}
        variant={"right"}
      />,
    );

    expect(screen.queryByText("Title content")).toBeInTheDocument();
    expect(screen.queryByText("Paragraph content")).toBeInTheDocument();
    expect(screen.queryByText("Button 1 Text content")).toBeInTheDocument();
    expect(screen.queryByText("Button 2 Text content")).toBeInTheDocument();
    expect(screen.queryByText("Image not found")).toBeInTheDocument();
  });
  it("Hero should render mocked data with image that stats with https", (): void => {
    expect.hasAssertions();

    jest.replaceProperty(window, "innerWidth", 400);

    render(
      <Hero
        button1Link={"Button 1 Link content"}
        button1Text={"Button 1 Text content"}
        button2Link={"Button 2 Link content"}
        button2Text={"Button 2 Text content"}
        image={"https://example.com"}
        paragraph={"Paragraph content"}
        subtitle={"Subtile content"}
        title={"Title content"}
        tone={"darkTransparent"}
        variant={"right"}
      />,
    );

    expect(screen.queryByText("Title content")).toBeInTheDocument();
    expect(screen.queryByText("Paragraph content")).toBeInTheDocument();
    expect(screen.queryByText("Button 1 Text content")).toBeInTheDocument();
    expect(screen.queryByText("Button 2 Text content")).toBeInTheDocument();
    jest.restoreAllMocks();
  });
  it("Hero should render mocked data with missing params", (): void => {
    expect.hasAssertions();

    render(
      <Hero
        button1Link={"Button 1 Link content"}
        button1Text={"Button 1 Text content"}
        button2Link={"Button 2 Link content"}
        button2Text={"Button 2 Text content"}
        image={"Image content"}
        paragraph={"Paragraph content"}
        title={"Title content"}
      />,
    );

    expect(screen.queryByText("Title content")).toBeInTheDocument();
    expect(screen.queryByText("Paragraph content")).toBeInTheDocument();
    expect(screen.queryByText("Button 1 Text content")).toBeInTheDocument();
    expect(screen.queryByText("Button 2 Text content")).toBeInTheDocument();
    expect(screen.queryByText("Image not found")).toBeInTheDocument();
  });
  it("Hero should render mocked data with some resizing", (): void => {
    expect.hasAssertions();

    jest.replaceProperty(window, "innerWidth", 400);

    render(
      <Hero
        button1Link={"Button 1 Link content"}
        button1Text={"Button 1 Text content"}
        button2Link={"Button 2 Link content"}
        button2Text={"Button 2 Text content"}
        image={"https://example.com"}
        paragraph={"Paragraph content"}
        title={"Title content"}
      />,
    );

    expect(screen.queryByText("Title content")).toBeInTheDocument();
    expect(screen.queryByText("Paragraph content")).toBeInTheDocument();
    expect(screen.queryByText("Button 1 Text content")).toBeInTheDocument();
    expect(screen.queryByText("Button 2 Text content")).toBeInTheDocument();
  });
});
