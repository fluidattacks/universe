import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import { useStaticQuery } from "gatsby";
import React from "react";

import { SimpleCard } from ".";
import { allCloudinaryMediaData } from "test-utils/mocks";
import type { ICloudinaryMedia } from "utils/types";

interface IDataImageMock {
  allCloudinaryMedia: ICloudinaryMedia;
}

const mockUseStaticQuery = jest.spyOn({ useStaticQuery }, `useStaticQuery`);
const mockDataUseStaticQuery: IDataImageMock = {
  allCloudinaryMedia: allCloudinaryMediaData,
};

describe("SimpleCard", (): void => {
  beforeEach((): void => {
    mockUseStaticQuery.mockImplementation(
      (): IDataImageMock => mockDataUseStaticQuery,
    );
  });

  afterEach((): void => {
    jest.restoreAllMocks();
  });
  it("should render SimpleCard with tittle", (): void => {
    expect.hasAssertions();

    const { container } = render(
      <SimpleCard
        description={"description"}
        descriptionColor={"#D2691E"}
        image={"image"}
        title={
          "Online Examination System v1.0 - Multiple Authenticated SQL Injections (SQLi)"
        }
        titleColor={"unset"}
        titleSize={"small"}
      />,
    );

    expect(container).toBeInTheDocument();
    const titleElement = screen.getByRole("heading", { level: 3 });
    expect(titleElement).toHaveStyle("font-size:  24px");
    expect(titleElement).toHaveStyle("color: unset");
  });

  it("should render SimpleCard an empty title", (): void => {
    expect.hasAssertions();

    const { container } = render(
      <SimpleCard
        description={"description"}
        descriptionColor={"#003423"}
        image={"image"}
      />,
    );

    expect(container).toBeInTheDocument();
  });
  it("should render SimpleCard with title and br = 2", (): void => {
    expect.hasAssertions();

    const { container } = render(
      <SimpleCard
        br={2}
        description={"description"}
        descriptionColor={"#003423"}
        image={"url"}
        title={
          "Online Examination System v1.0 - Multiple Authenticated SQL Injections (SQLi)"
        }
      />,
    );

    expect(container).toBeInTheDocument();
  });

  it("should render SimpleCard with br = 2 and without title", (): void => {
    expect.hasAssertions();

    const { container } = render(
      <SimpleCard
        br={2}
        description={"description"}
        descriptionColor={"#003423"}
        image={"url"}
      />,
    );

    expect(container).toBeInTheDocument();
  });
});
