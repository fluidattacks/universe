import React from "react";

import type { ISimpleCardProps } from "./types";

import { CloudImage } from "../CloudImage";
import { Container } from "../Container";
import { Text, Title } from "../Typography";

const SimpleCard: React.FC<ISimpleCardProps> = ({
  bgColor,
  bgGradient,
  borderColor,
  br,
  className,
  description,
  descriptionColor,
  hovercolor,
  hoverShadow,
  image,
  title = "",
  titleColor = "unset",
  titleMinHeight,
  titleSize = "small",
  maxWidth,
  widthMd,
  widthSm,
}): JSX.Element => {
  if (title) {
    return (
      <Container
        bgColor={bgColor}
        bgGradient={bgGradient}
        borderColor={borderColor}
        br={br ?? 2}
        classname={className}
        direction={"column"}
        display={"flex"}
        hoverShadow={hoverShadow}
        hovercolor={hovercolor}
        maxWidth={maxWidth}
        mh={2}
        mv={2}
        ph={3}
        pv={3}
        widthMd={widthMd}
        widthSm={widthSm}
      >
        <Container height={"42px"} mb={3} mt={2} width={"42px"}>
          {/** The image with https are ONLY for storybook purpose */}
          {image.startsWith("https") ? (
            <img alt={title} className={"w-100 h-100"} src={image} />
          ) : (
            <CloudImage alt={title} src={image} styles={"w-100 h-100"} />
          )}
        </Container>
        <Container minHeight={titleMinHeight}>
          <Title color={titleColor} level={3} size={titleSize}>
            {title}
          </Title>
        </Container>
        <Container mv={3}>
          <Text color={descriptionColor} size={"small"}>
            {description}
          </Text>
        </Container>
      </Container>
    );
  }

  return (
    <Container
      bgColor={bgColor}
      bgGradient={bgGradient}
      borderColor={borderColor}
      br={br ?? 2}
      classname={className}
      direction={"column"}
      display={"flex"}
      hoverShadow={hoverShadow}
      hovercolor={hovercolor}
      maxWidth={maxWidth}
      mh={2}
      mv={2}
      ph={3}
      pv={3}
      widthMd={widthMd}
      widthSm={widthSm}
    >
      <Container center={true} height={"64px"} mv={3} width={"64px"}>
        {/** The image with https are ONLY for storybook purpose */}
        {image.startsWith("https") ? (
          <img alt={title} className={"w-100 h-100"} src={image} />
        ) : (
          <CloudImage alt={title} src={image} styles={"w-100 h-100"} />
        )}
      </Container>
      <Container mb={3}>
        <Text color={descriptionColor} size={"small"} textAlign={"center"}>
          {description}
        </Text>
      </Container>
    </Container>
  );
};

export { SimpleCard };
