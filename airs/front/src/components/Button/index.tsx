/* eslint react/forbid-component-props: 0 */
import React, { useMemo } from "react";
import { IconContext } from "react-icons";

import { StyledButton } from "./styles";
import type { IButtonProps } from "./types";

import { Container } from "../Container";
import "tachyons";

const Button = ({
  customSize,
  children,
  className,
  disabled,
  display,
  icon,
  iconSide = "left",
  name,
  onClick,
  selected,
  size,
  type,
  variant,
}: Readonly<IButtonProps>): JSX.Element => {
  const valueIconL = useMemo((): { className: string } => {
    return { className: children === undefined ? "" : "mr1" };
  }, [children]);

  const valueIconR = useMemo((): { className: string } => {
    return { className: children === undefined ? "" : "ml1" };
  }, [children]);

  return (
    <StyledButton
      $customSize={customSize}
      $display={display}
      $selected={selected}
      $size={size}
      $variant={variant}
      className={className}
      disabled={disabled === true}
      name={name}
      onClick={onClick}
      type={type}
    >
      <Container
        align={"center"}
        display={"flex"}
        justify={display === "block" ? "center" : "unset"}
      >
        {iconSide === "left" ? (
          <IconContext.Provider value={valueIconL}>
            {icon ?? undefined}
            {children}
          </IconContext.Provider>
        ) : (
          <IconContext.Provider value={valueIconR}>
            {children}
            {icon ?? undefined}
          </IconContext.Provider>
        )}
      </Container>
    </StyledButton>
  );
};

export type { IButtonProps };
export { Button };
