import { graphql, useStaticQuery } from "gatsby";
import React from "react";

import { AdviseCardFooter } from "./styles";

import { Badge } from "../../styles/styles";
import { usePagination } from "../../utils/hooks";
import { AirsLink } from "../AirsLink";
import { Button } from "../Button";
import { Container } from "../Container";
import { Grid } from "../Grid";
import { Pagination } from "../Pagination";
import { Text } from "../Typography";

const AdviseCards: React.FC = (): JSX.Element => {
  const data: IData = useStaticQuery(graphql`
    query AdviseQuery {
      allMarkdownRemark(
        filter: {
          frontmatter: { advise: { eq: "yes" }, encrypted: { ne: "yes" } }
        }
        sort: { frontmatter: { date: DESC } }
      ) {
        edges {
          node {
            frontmatter {
              authors
              codename
              cveid
              date
              slug
              title
              severity
            }
          }
        }
      }
    }
  `);

  const listOfCards = data.allMarkdownRemark.edges.map(
    (advisePage): JSX.Element => {
      const { authors, codename, cveid, date, severity, slug, title } =
        advisePage.node.frontmatter;

      return (
        <Container
          bgColor={"#fff"}
          br={3}
          direction={"column"}
          display={"flex"}
          key={codename}
          mh={2}
          mv={2}
          ph={3}
          pv={3}
        >
          <Container pv={2}>
            <Badge $bgColor={"#dddde3"} $color={"#2e2e38"}>
              {`Severity ${severity ?? "pending"}`}
            </Badge>
          </Container>
          <Container minHeight={"56px"} mt={3}>
            <Text color={"#2e2e38"} size={"big"} weight={"bold"}>
              {title}
            </Text>
          </Container>
          <Container mv={3}>
            <Text color={"#787891"} size={"big"} weight={"bold"}>
              {cveid}
            </Text>
          </Container>
          <AdviseCardFooter>
            <Text color={"#787891"} size={"medium"}>
              {`Published: ${date}`}
            </Text>
            <Text color={"#787891"} mb={3} size={"medium"}>
              {`Discovered by ${authors}`}
            </Text>
            <AirsLink href={`/${slug}`}>
              <Button display={"block"} variant={"tertiary"}>
                {"Read More"}
              </Button>
            </AirsLink>
          </AdviseCardFooter>
        </Container>
      );
    },
  );

  const itemsPerPage = 9;

  const { currentPage, endOffset, handlePageClick, newOffset, pageCount } =
    usePagination(itemsPerPage, listOfCards);

  return (
    <React.Fragment>
      <Container center={true} maxWidth={"1300px"} ph={4} pv={5}>
        <Grid columns={3} columnsMd={2} columnsSm={1} gap={"1rem"}>
          {listOfCards.slice(newOffset, endOffset)}
        </Grid>
      </Container>
      <Pagination
        forcePage={currentPage}
        onChange={handlePageClick}
        pageCount={pageCount}
      />
    </React.Fragment>
  );
};

export { AdviseCards };
