import type { ReactNode } from "react";

type Nums0To4 = 0 | 1 | 2 | 3 | 4;
type Nums0To7 = Nums0To4 | 5 | 6 | 7;
type TAlign = "center" | "end" | "start" | "stretch" | "unset";
type TDirection = "column" | "reverse" | "row" | "unset";
type TDisplay = "block" | "flex" | "ib" | "inline" | "none";
type TJustify = "around" | "between" | "center" | "end" | "start" | "unset";
type TWrap = "nowrap" | "unset" | "wrap";
type TScroll = "none" | "x" | "xy" | "y";

interface ITransientProps {
  align?: TAlign;
  bgColor?: string;
  bgGradient?: string;
  borderBottomColor?: string;
  borderColor?: string;
  borderHoverColor?: string;
  borderTopColor?: string;
  br?: Nums0To4 | 100;
  center?: boolean;
  classname?: string;
  direction?: TDirection;
  display?: TDisplay;
  displayMd?: TDisplay;
  displaySm?: TDisplay;
  gap?: string;
  height?: string;
  hovercolor?: string;
  hoverShadow?: boolean;
  id?: string;
  justify?: TJustify;
  justifyMd?: TJustify;
  justifySm?: TJustify;
  leftBar?: string;
  mv?: Nums0To7;
  mh?: Nums0To7;
  pv?: Nums0To7;
  ph?: Nums0To7;
  pvMd?: Nums0To7;
  pvSm?: Nums0To7;
  phMd?: Nums0To7;
  phSm?: Nums0To7;
  position?: string;
  mb?: Nums0To7;
  ml?: Nums0To7;
  mr?: Nums0To7;
  mt?: Nums0To7;
  mtMd?: Nums0To7;
  mtSm?: Nums0To7;
  pb?: Nums0To7;
  pl?: Nums0To7;
  pr?: Nums0To7;
  pt?: Nums0To7;
  maxWidth?: string;
  minHeight?: string;
  minWidth?: string;
  minWidthMd?: string;
  minWidthSm?: string;
  scroll?: TScroll;
  shadow?: boolean;
  shadowBottom?: boolean;
  textHoverColor?: string;
  topBar?: string;
  width?: string;
  widthMd?: string;
  widthSm?: string;
  wrap?: TWrap;
}

interface IStyledContainerProps extends TPrefixWithDollar<ITransientProps> {
  $pointer: boolean;
}

interface IContainerProps extends ITransientProps {
  onClick?: React.MouseEventHandler<HTMLDivElement>;
  children?: ReactNode;
}

export type {
  IContainerProps,
  IStyledContainerProps,
  TAlign,
  TDirection,
  TDisplay,
  TJustify,
  TWrap,
};
