/* eslint react/forbid-component-props: 0 */
import React from "react";

import { CardContainer, Container } from "./styles";

import { translate } from "../../../utils/translations/translate";
import { AirsLink } from "../../AirsLink";
import { Button } from "../../Button";
import { Text, Title } from "../../Typography";

const Banner: React.FC = (): JSX.Element => {
  return (
    <Container>
      <CardContainer>
        <Title
          color={"#2e2e38"}
          level={2}
          size={"medium"}
          sizeMd={"medium"}
          textAlign={"center"}
        >
          {translate.t("plansPage.portrait.title")}
        </Title>
        <Text color={"#5c5c70"} mb={2} mt={1} size={"medium"}>
          {translate.t("plansPage.portrait.paragraph")}
        </Text>
        <AirsLink href={"https://app.fluidattacks.com/SignUp"}>
          <Button className={"mh2 mv3"} variant={"primary"}>
            {translate.t("productOverview.mainButton1")}
          </Button>
        </AirsLink>
        <AirsLink href={"/contact-us-demo/"}>
          <Button className={"mh2"} variant={"secondary"}>
            {translate.t("productOverview.mainButton2")}
          </Button>
        </AirsLink>
      </CardContainer>
    </Container>
  );
};

export { Banner };
