/* eslint react/forbid-component-props: 0 */
import React, { useCallback, useLayoutEffect, useState } from "react";

import { DemoBanner } from "./DemoBanner";
import {
  Container,
  HorizontalProgressBar,
  HorizontalProgressContainer,
  ProgressBar,
  ProgressCol,
  ProgressContainer,
  SectionContainer,
} from "./styles";

import { translate } from "../../../utils/translations/translate";

const ProductSection: React.FC = (): JSX.Element => {
  const data = [
    {
      description: translate.t(
        "productOverview.productSection.vulnDescription",
      ),
      image1: "vulnerabilities",
      imageRight: true,
      subtitle: translate.t("productOverview.productSection.vulnSubtitle"),
      title: translate.t("productOverview.productSection.vulnTitle"),
    },
    {
      description: translate.t(
        "productOverview.productSection.remediationDescription",
      ),
      image1: "treatments",
      imageRight: false,
      subtitle: translate.t(
        "productOverview.productSection.remediationSubtitle",
      ),
      title: translate.t("productOverview.productSection.remediationTitle"),
    },
    {
      description: translate.t(
        "productOverview.productSection.strictDescription",
      ),
      image1: "devsecops",
      imageRight: true,
      subtitle: translate.t("productOverview.productSection.strictSubtitle"),
      title: translate.t("productOverview.productSection.strictTitle"),
    },
    {
      description: translate.t(
        "productOverview.productSection.analyticsDescription",
      ),
      image1: "analytics",
      imageRight: false,
      subtitle: translate.t("productOverview.productSection.analyticsSubtitle"),
      title: translate.t("productOverview.productSection.analyticsTitle"),
    },
    {
      description: translate.t(
        "productOverview.productSection.supportDescription",
      ),
      image1: "help",
      imageRight: true,
      subtitle: translate.t("productOverview.productSection.supportSubtitle"),
      title: translate.t("productOverview.productSection.supportTitle"),
    },
  ];

  const isBrowser = typeof window !== "undefined";
  const [scrollTop, setScrollTop] = useState(5);
  const [scrollHorizontal, setScrollHorizontal] = useState(5);

  const onScroll = useCallback((): void => {
    if (isBrowser) {
      const scrollDistance = -document
        .getElementsByClassName("product-section")[0]
        .getBoundingClientRect().top;
      const progressPercentage =
        document.getElementsByClassName("product-section")[0].scrollHeight -
        document.documentElement.clientHeight;
      const scrolled = (scrollDistance / progressPercentage) * 100;

      if (scrolled <= 5) {
        setScrollTop(5);
      } else if (scrolled >= 100) {
        setScrollTop(100);
      } else {
        setScrollTop(scrolled);
      }
    }
  }, [isBrowser]);

  const onHorizontalScroll = useCallback((): void => {
    if (isBrowser) {
      const scrollDistance =
        document.getElementsByClassName("product-section")[0].scrollLeft;
      const progressPercentage =
        document.getElementsByClassName("product-section")[0].scrollWidth -
        document.getElementsByClassName("product-section")[0].clientWidth;
      const scrolled = (scrollDistance / progressPercentage) * 100;

      if (scrolled <= 5) {
        setScrollHorizontal(5);
      } else if (scrolled >= 100) {
        setScrollHorizontal(100);
      } else {
        setScrollHorizontal(scrolled);
      }
    }
  }, [isBrowser]);

  useLayoutEffect((): VoidFunction => {
    const productSection = document.getElementsByClassName("product-section");

    window.addEventListener("scroll", onScroll, { passive: true });
    productSection[0].addEventListener("scroll", onHorizontalScroll, {
      passive: true,
    });

    return (): void => {
      window.removeEventListener("scroll", onScroll);
      productSection[0].removeEventListener("scroll", onHorizontalScroll);
    };
  }, [onHorizontalScroll, onScroll]);

  return (
    <Container>
      <ProgressCol>
        <ProgressContainer>
          <ProgressBar style={{ height: `${scrollTop}%` }} />
        </ProgressContainer>
      </ProgressCol>
      <SectionContainer>
        {data.map((banner): JSX.Element => {
          return (
            <DemoBanner
              description={banner.description}
              image1={banner.image1}
              imageRight={banner.imageRight}
              key={banner.title}
              subtitle={banner.subtitle}
              title={banner.title}
            />
          );
        })}
      </SectionContainer>
      <HorizontalProgressContainer>
        <HorizontalProgressBar style={{ width: `${scrollHorizontal}%` }} />
      </HorizontalProgressContainer>
    </Container>
  );
};

export { ProductSection };
