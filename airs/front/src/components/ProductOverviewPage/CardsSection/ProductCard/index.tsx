import React from "react";

import { CloudImage } from "../../../CloudImage";
import { Text } from "../../../Typography";
import { CardContainer } from "../styles";
import type { IProductCard } from "../types";

const ProductCard: React.FC<IProductCard> = ({
  image,
  text,
}: IProductCard): JSX.Element => {
  return (
    <CardContainer>
      <CloudImage
        alt={`card-image-${image}`}
        src={`airs/product-overview/cards-section/${image}`}
        styles={"mt4"}
      />
      <Text color={"#5c5c70"} mb={2} mt={2} size={"medium"}>
        {text}
      </Text>
    </CardContainer>
  );
};

export { ProductCard };
