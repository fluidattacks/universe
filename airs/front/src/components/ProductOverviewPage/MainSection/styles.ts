import { styled } from "styled-components";

const Container = styled.div.attrs({
  className: `
    center
    flex
    flex-wrap
    ph-body
  `,
})`
  background-color: #2e2e38;
`;

const MainTextContainer = styled.div.attrs({
  className: `
    tc
    w-60
    center
    mv5
  `,
})``;

export { Container, MainTextContainer };
