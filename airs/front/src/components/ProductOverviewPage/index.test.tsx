import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import React from "react";

import { CardsSection } from "./CardsSection";

describe("ProductOverviewPage", (): void => {
  describe("CardsSection", (): void => {
    it("renders main text correctly", (): void => {
      expect.hasAssertions();

      render(<CardsSection />);
      expect(
        screen.getByText("productOverview.cardsSection.title"),
      ).toBeInTheDocument();
      expect(
        screen.getByText("productOverview.cardsSection.paragraph"),
      ).toBeInTheDocument();
      expect(
        screen.getByText("productOverview.cardsSection.card1Description"),
      ).toBeInTheDocument();
      expect(
        screen.getByText("productOverview.cardsSection.card2Description"),
      ).toBeInTheDocument();
      expect(
        screen.getByText("productOverview.cardsSection.card3Description"),
      ).toBeInTheDocument();
    });
  });
});
