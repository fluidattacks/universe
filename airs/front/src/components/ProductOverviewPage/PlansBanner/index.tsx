/* eslint react/forbid-component-props: 0 */
import { FaArrowRight } from "@react-icons/all-files/fa/FaArrowRight";
import React from "react";

import { CardButton, CardContainer, Container } from "./styles";

import { translate } from "../../../utils/translations/translate";
import { AirsLink } from "../../AirsLink";
import { Text, Title } from "../../Typography";

const PlansBanner: React.FC = (): JSX.Element => (
  <Container>
    <CardContainer>
      <Title color={"#2e2e38"} level={1} size={"medium"}>
        {translate.t("productOverview.plansBanner.title")}
      </Title>
      <Text color={"#5c5c70"} mb={1} mt={1} size={"medium"}>
        {translate.t("productOverview.plansBanner.subtitle")}
      </Text>
      <AirsLink decoration={"none"} href={"/plans"}>
        <CardButton>
          <Title color={"#2e2e38"} hColor={"#bf0b1a"} level={3} size={"xs"}>
            {translate.t("productOverview.plansBanner.link")}
          </Title>
          <FaArrowRight color={"#bf0b1a"} size={25} />
        </CardButton>
      </AirsLink>
    </CardContainer>
  </Container>
);

export { PlansBanner };
