import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import React from "react";

import { CtaBanner } from ".";

describe(`CtaBanner`, (): void => {
  it("CtaBanner should return mocked data", (): void => {
    expect.hasAssertions();

    render(
      <CtaBanner
        button1Link={"https://fluidattacks.com/"}
        button1Text={"Test"}
        button2Link={"/"}
        button2Text={"Test 2"}
        image={"image.jpg"}
        paragraph={"Description"}
        sizeSm={"medium"}
        title={"Title content"}
      />,
    );

    expect(screen.queryByText("Title content")).toBeInTheDocument();
    expect(screen.queryByText("Test")).toBeInTheDocument();
    expect(screen.queryByText("Test 2")).toBeInTheDocument();
    expect(screen.queryByText("Image not found")).toBeInTheDocument();
  });

  it("CtaBanner should return mocked data with image starts with https", (): void => {
    expect.hasAssertions();

    render(
      <CtaBanner
        button1Link={"https://fluidattacks.com/"}
        button1Text={"Test"}
        button2Link={"/"}
        button2Text={"Test 2"}
        image={"https://fluidattacks.com"}
        paragraph={"Description"}
        sizeSm={"medium"}
        title={"Title content"}
        variant={"light"}
      />,
    );

    expect(screen.queryByText("Title content")).toBeInTheDocument();
    expect(screen.queryByText("Test")).toBeInTheDocument();
    expect(screen.queryByText("Test 2")).toBeInTheDocument();
  });

  it("CtaBanner should return mocked data with undefined image", (): void => {
    expect.hasAssertions();

    render(
      <CtaBanner
        button1Link={"https://fluidattacks.com/"}
        button1Text={"Test"}
        button2Link={"/"}
        button2Text={"Test 2"}
        image={undefined}
        paragraph={"Description"}
        sizeSm={"medium"}
        title={"Title content"}
      />,
    );

    expect(screen.queryByText("Title content")).toBeInTheDocument();
    expect(screen.queryByText("Test")).toBeInTheDocument();
    expect(screen.queryByText("Test 2")).toBeInTheDocument();
    expect(screen.queryByText("Image not found")).not.toBeInTheDocument();
  });

  it("CtaBanner should return mocked data with undefined image, defined maxWidth and variant", (): void => {
    expect.hasAssertions();

    const { container } = render(
      <CtaBanner
        button1Link={"https://fluidattacks.com/"}
        button1Text={"Test"}
        button2Link={"/"}
        button2Text={"Test 2"}
        image={undefined}
        maxWidth={"100%"}
        paragraph={"Description"}
        sizeSm={"medium"}
        title={"Title content"}
      />,
    );

    expect(container).toBeInTheDocument();
  });

  it("CtaBanner should return mocked data with undefined button2Link", (): void => {
    expect.hasAssertions();

    render(
      <CtaBanner
        button1Link={"https://fluidattacks.com/"}
        button1Text={"Test"}
        button2Link={undefined}
        button2Text={"Test 2"}
        image={"image.jpg"}
        maxWidth={"1440px"}
        paragraph={"Description"}
        sizeSm={"medium"}
        title={"Title content"}
        variant={"light"}
      />,
    );

    expect(screen.queryByText("Title content")).toBeInTheDocument();
    expect(screen.queryByText("Test")).toBeInTheDocument();
    expect(screen.queryByText("Test 2")).not.toBeInTheDocument();
    expect(screen.queryByText("Image not found")).not.toBeInTheDocument();
  });

  it("CtaBanner should return mocked data with some undefined params", (): void => {
    expect.hasAssertions();

    render(
      <CtaBanner
        button1Link={"https://fluidattacks.com/"}
        button1Text={"Test"}
        button2Link={undefined}
        button2Text={"Test 2"}
        image={undefined}
        paragraph={undefined}
        sizeSm={"medium"}
        title={"Title content"}
      />,
    );

    expect(screen.queryByText("Title content")).toBeInTheDocument();
    expect(screen.queryByText("Test")).toBeInTheDocument();
    expect(screen.queryByText("Test 2")).not.toBeInTheDocument();
    expect(screen.queryByText("Image not found")).not.toBeInTheDocument();
  });

  it("CtaBanner should return mocked data with some resizing", (): void => {
    expect.hasAssertions();

    jest.replaceProperty(window, "innerWidth", 400);

    render(
      <CtaBanner
        button1Link={"https://fluidattacks.com/"}
        button1Text={"Test"}
        button2Link={undefined}
        button2Text={"Test 2"}
        image={undefined}
        maxWidth={"1440px"}
        paragraph={undefined}
        sizeSm={"medium"}
        title={"Title content"}
        variant={"dark"}
      />,
    );

    expect(screen.queryByText("Title content")).toBeInTheDocument();
    expect(screen.queryByText("Test")).toBeInTheDocument();
    expect(screen.queryByText("Test 2")).not.toBeInTheDocument();
    expect(screen.queryByText("Image not found")).not.toBeInTheDocument();
    jest.restoreAllMocks();
  });
});
