import i18next from "i18next";
import React from "react";

import type { IHeadProps } from "./types";

const GatsbyHead = ({
  children,
  linkItems,
  metaItems,
  title,
}: IHeadProps): JSX.Element => {
  const shouldAddCookieScript =
    // @ts-expect-error: the react types are missing.
    typeof window !== "undefined" && window.Cookiebot === undefined;

  return (
    <React.Fragment>
      <html lang={i18next.language} translate={"no"} />
      <title>{title}</title>
      <meta content={"no-cache"} httpEquiv={"Pragma"} key={"set-no-cache"} />
      <meta
        content={"no-cache, no-store, must-revalidate"}
        httpEquiv={"cache-control"}
        key={"cache-control"}
      />
      <script
        crossOrigin={"anonymous"}
        id={"zsiqchat"}
        key={"zoho-config-script"}
        src={"/zohoLiveChat.js"}
        type={"text/javascript"}
      />
      {shouldAddCookieScript ? (
        <script
          data-blockingmode={"auto"}
          data-cbid={"9c4480b4-b8ae-44d8-9c6f-6300b86e9094"}
          id={"Cookiebot"}
          key={"cookie-bot-script"}
          src={"https://consent.cookiebot.com/uc.js"}
          type={"text/javascript"}
        />
      ) : undefined}
      <script
        id={"pagesense-config"}
        key={"pagesense-config"}
        src={
          "https://cdn.pagesense.io/js/fluidattacks7371/8cb6abb2b68d4871a6ffa989061cb6fc.js"
        }
      />
      <link
        href={
          "https://res.cloudinary.com/fluid-attacks/image/upload/v1669232201/airs/favicon-2022.webp"
        }
        id={"favicon-page"}
        key={"favicon-page"}
        rel={"icon"}
        type={"image/x-icon"}
      />
      {metaItems.map(
        ({ content, name, property }): JSX.Element => (
          <meta content={content} key={name} name={name} property={property} />
        ),
      )}
      {linkItems.map(
        ({ href, hreflang, rel }): JSX.Element => (
          <link href={href} hrefLang={hreflang} key={hreflang} rel={rel} />
        ),
      )}
      {children}
    </React.Fragment>
  );
};

export { GatsbyHead };
