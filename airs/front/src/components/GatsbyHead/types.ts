interface IMetaItem {
  content: string;
  name: string;
  property?: string;
}

interface ILinkItem {
  href: string;
  hreflang?: string;
  rel: string;
}

interface IHeadProps {
  children?: React.ReactNode;
  linkItems: ILinkItem[];
  metaItems: IMetaItem[];
  title: string;
}

export type { IHeadProps, ILinkItem, IMetaItem };
