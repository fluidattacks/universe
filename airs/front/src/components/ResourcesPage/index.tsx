/* eslint react/forbid-component-props: 0 */
import React, { useCallback, useMemo, useState } from "react";

import { ResourcesCard } from "./ResourceCard";
import type { IResourceCardProps } from "./ResourceCard/types";
import { ResourcesMenuElements } from "./ResourcesMenuButtons";
import {
  CardsContainer,
  MainCardContainer,
  MenuList,
  ResourcesContainer,
} from "./styles";

import { FlexCenterItemsContainer, PageArticle } from "../../styles/styles";
import { translate } from "../../utils/translations/translate";
import { AirsLink } from "../AirsLink";
import { Button } from "../Button";
import { Text, Title } from "../Typography";

interface IProps {
  bannerTitle: string;
}

const ResourcesPage: React.FC<IProps> = ({
  bannerTitle,
}: IProps): JSX.Element => {
  const data = useMemo(
    (): (IResourceCardProps & { key: string })[] => [
      {
        buttonText: translate.t("resources.cardsText.buttons.downloadButton"),
        cardType: "ebook-card",
        description: translate.t(
          "resources.cardsText.eBooks.ebook1Description",
        ),
        image: "/resources/resource-card5n",
        key: "card-9",
        language: "ENGLISH",
        title: translate.t("resources.cardsText.eBooks.ebook1Title"),
        urlCard: "https://fluidattacks.docsend.com/view/aephu8hbw43wmgb3",
      },
      {
        buttonText: translate.t("resources.cardsText.buttons.downloadButton"),
        cardType: "successstory-card",
        description: translate.t(
          "resources.cardsText.successStory.successStory5Description",
        ),
        image: "/resources/resource-card23n",
        key: "card-23",
        language: "ENGLISH",
        title: translate.t(
          "resources.cardsText.successStory.successStory5Title",
        ),
        urlCard: "https://fluidattacks.docsend.com/view/tgj23t5jjuwx3zkb",
      },
      {
        buttonText: translate.t("resources.cardsText.buttons.downloadButton"),
        cardType: "successstory-card",
        description: translate.t(
          "resources.cardsText.successStory.successStory4Description",
        ),
        image: "/resources/resource-card22n",
        key: "card-22",
        language: "ENGLISH",
        title: translate.t(
          "resources.cardsText.successStory.successStory4Title",
        ),
        urlCard: "https://fluidattacks.docsend.com/view/j8rvapujmtbqiac6",
      },
      {
        buttonText: translate.t("resources.cardsText.buttons.downloadButton"),
        cardType: "successstory-card",
        description: translate.t(
          "resources.cardsText.successStory.successStory3Description",
        ),
        image: "/resources/resource-card21n",
        key: "card-21",
        language: "ENGLISH",
        title: translate.t(
          "resources.cardsText.successStory.successStory3Title",
        ),
        urlCard: "https://fluidattacks.docsend.com/view/9y9stn4y74xjrf27",
      },
      {
        buttonText: translate.t("resources.cardsText.buttons.downloadButton"),
        cardType: "successstory-card",
        description: translate.t(
          "resources.cardsText.successStory.successStory2Description",
        ),
        image: "/resources/resource-card19n",
        key: "card-19",
        language: "ENGLISH",
        title: translate.t(
          "resources.cardsText.successStory.successStory2Title",
        ),
        urlCard: "https://fluidattacks.docsend.com/view/v3aj7p3sixmh6ict",
      },
      {
        buttonText: translate.t("resources.cardsText.buttons.downloadButton"),
        cardType: "successstory-card",
        description: translate.t(
          "resources.cardsText.successStory.successStory1Description",
        ),
        image: "/resources/resource-card18n",
        key: "card-18",
        language: "ENGLISH",
        title: translate.t(
          "resources.cardsText.successStory.successStory1Title",
        ),
        urlCard: "https://fluidattacks.docsend.com/view/gfgsj2xdjfh6kwkt",
      },
      {
        buttonText: translate.t("resources.cardsText.buttons.webinarButton"),
        cardType: "webinar-card",
        description: translate.t(
          "resources.cardsText.webinars.webinar1Description",
        ),
        image: "/resources/resource-card1n",
        key: "card-1",
        language: "SPANISH",
        title: translate.t("resources.cardsText.webinars.webinar1Title"),
        urlCard: "https://www.youtube.com/watch?v=tGNbQelMrFA",
      },
      {
        buttonText: translate.t("resources.cardsText.buttons.downloadButton"),
        cardType: "whitepaper-card",
        description: translate.t(
          "resources.cardsText.whitePaper.whitePaper1Description",
        ),
        image: "/resources/resource-card17n",
        key: "card-17",
        language: "ENGLISH",
        title: translate.t("resources.cardsText.whitePaper.whitePaper1Title"),
        urlCard: "https://fluidattacks.docsend.com/view/4r7qehi7a35ndbqx",
      },
      {
        buttonText: translate.t("resources.cardsText.buttons.downloadButton"),
        cardType: "report-card",
        description: translate.t(
          "resources.cardsText.reports.report8Description",
        ),
        image: "/resources/resource-card24n",
        key: "card-24",
        language: "ENGLISH",
        title: translate.t("resources.cardsText.reports.report8Title"),
        urlCard: "https://fluidattacks.docsend.com/view/556tpmq4furacqck",
      },
      {
        buttonText: translate.t("resources.cardsText.buttons.downloadButton"),
        cardType: "report-card",
        description: translate.t(
          "resources.cardsText.reports.report7Description",
        ),
        image: "/resources/resource-card20n",
        key: "card-20",
        language: "ENGLISH",
        title: translate.t("resources.cardsText.reports.report7Title"),
        urlCard: "https://fluidattacks.docsend.com/view/e988pvaiuew3nikq",
      },
      {
        buttonText: translate.t("resources.cardsText.buttons.downloadButton"),
        cardType: "report-card",
        description: translate.t(
          "resources.cardsText.reports.report6Description",
        ),
        image: "/resources/resource-card16n",
        key: "card-16",
        language: "ENGLISH",
        title: translate.t("resources.cardsText.reports.report6Title"),
        urlCard: "https://fluidattacks.docsend.com/view/behmfvipxcha2t7v",
      },
      {
        buttonText: translate.t("resources.cardsText.buttons.downloadButton"),
        cardType: "report-card",
        description: translate.t(
          "resources.cardsText.reports.report1Description",
        ),
        image: "/resources/resource-card12n",
        key: "card-2",
        language: "ENGLISH",
        title: translate.t("resources.cardsText.reports.report1Title"),
        urlCard: "https://fluidattacks.docsend.com/view/scicyhpa7ckna2be",
      },
      {
        buttonText: translate.t("resources.cardsText.buttons.downloadButton"),
        cardType: "report-card",
        description: translate.t(
          "resources.cardsText.reports.report2Description",
        ),
        image: "/resources/resource-card2n",
        key: "card-3",
        language: "ENGLISH",
        title: translate.t("resources.cardsText.reports.report2Title"),
        urlCard: "https://fluidattacks.docsend.com/view/ua5mk5u",
      },
      {
        buttonText: translate.t("resources.cardsText.buttons.downloadButton"),
        cardType: "report-card",
        description: translate.t(
          "resources.cardsText.reports.report3Description",
        ),
        image: "/resources/resource-card13n",
        key: "card-4",
        language: "ENGLISH",
        title: translate.t("resources.cardsText.reports.report3Title"),
        urlCard: "https://fluidattacks.docsend.com/view/qkdsfs75j37k8atz",
      },
      {
        buttonText: translate.t("resources.cardsText.buttons.downloadButton"),
        cardType: "report-card",
        description: translate.t(
          "resources.cardsText.reports.report4Description",
        ),
        image: "/resources/resource-card14n",
        key: "card-5",
        language: "ENGLISH",
        title: translate.t("resources.cardsText.reports.report4Title"),
        urlCard: "https://fluidattacks.docsend.com/view/sqdniyqx445bd7ne",
      },
      {
        buttonText: translate.t("resources.cardsText.buttons.downloadButton"),
        cardType: "report-card",
        description: translate.t(
          "resources.cardsText.reports.report5Description",
        ),
        image: "/resources/resource-card15n",
        key: "card-6",
        language: "ENGLISH",
        title: translate.t("resources.cardsText.reports.report5Title"),
        urlCard: "https://fluidattacks.docsend.com/view/qh2kwhmd787mfzen",
      },
      {
        buttonText: translate.t("resources.cardsText.buttons.webinarButton"),
        cardType: "webinar-card",
        description: translate.t(
          "resources.cardsText.webinars.webinar2Description",
        ),
        image: "/resources/resource-card1n",
        key: "card-7",
        language: "SPANISH",
        title: translate.t("resources.cardsText.webinars.webinar2Title"),
        urlCard: "https://www.youtube.com/watch?v=N6nVIOsnaOA",
      },
      {
        buttonText: translate.t("resources.cardsText.buttons.webinarButton"),
        cardType: "webinar-card",
        description: translate.t(
          "resources.cardsText.webinars.webinar3Description",
        ),
        image: "/resources/resource-card3n",
        key: "card-8",
        language: "SPANISH",
        title: translate.t("resources.cardsText.webinars.webinar3Title"),
        urlCard: "https://www.youtube.com/watch?v=zUJ_kU79j7E",
      },
      {
        buttonText: translate.t("resources.cardsText.buttons.webinarButton"),
        cardType: "webinar-card",
        description: translate.t(
          "resources.cardsText.webinars.webinar4Description",
        ),
        image: "/resources/resource-card8n",
        key: "card-10",
        language: "ENGLISH",
        title: translate.t("resources.cardsText.webinars.webinar4Title"),
        urlCard: "https://www.youtube.com/watch?v=VWwqhA2LFHg",
      },
      {
        buttonText: translate.t("resources.cardsText.buttons.webinarButton"),
        cardType: "webinar-card",
        description: translate.t(
          "resources.cardsText.webinars.webinar4Description",
        ),
        image: "/resources/resource-card8n",
        key: "card-11",
        language: "SPANISH",
        title: translate.t("resources.cardsText.webinars.webinar4Title"),
        urlCard: "https://www.youtube.com/watch?v=uGnccQg8Zfc",
      },
      {
        buttonText: translate.t("resources.cardsText.buttons.webinarButton"),
        cardType: "webinar-card",
        description: translate.t(
          "resources.cardsText.webinars.webinar5Description",
        ),
        image: "/resources/resource-card3n",
        key: "card-12",
        language: "SPANISH",
        title: translate.t("resources.cardsText.webinars.webinar5Title"),
        urlCard: "https://www.youtube.com/watch?v=4MS3Glq4dv8",
      },
      {
        buttonText: translate.t("resources.cardsText.buttons.webinarButton"),
        cardType: "webinar-card",
        description: translate.t(
          "resources.cardsText.webinars.webinar6Description",
        ),
        image: "/resources/resource-card9n",
        key: "card-13",
        language: "SPANISH",
        title: translate.t("resources.cardsText.webinars.webinar6Title"),
        urlCard: "https://www.youtube.com/watch?v=8DXafdNIZ-4",
      },
      {
        buttonText: translate.t("resources.cardsText.buttons.webinarButton"),
        cardType: "webinar-card",
        description: translate.t(
          "resources.cardsText.webinars.webinar7Description",
        ),
        image: "/resources/resource-card8n",
        key: "card-14",
        language: "ENGLISH",
        title: translate.t("resources.cardsText.webinars.webinar7Title"),
        urlCard: "https://www.youtube.com/watch?v=NoI3PWnTUak",
      },
      {
        buttonText: translate.t("resources.cardsText.buttons.webinarButton"),
        cardType: "webinar-card",
        description: translate.t(
          "resources.cardsText.webinars.webinar8Description",
        ),
        image: "/resources/resource-card11n",
        key: "card-15",
        language: "SPANISH",
        title: translate.t("resources.cardsText.webinars.webinar8Title"),
        urlCard: "https://www.youtube.com/watch?reload=9&v=-KvvMD7EJAs",
      },
    ],
    [],
  );

  const [filteredData, setFilteredData] = useState(data);

  const filterData = useCallback(
    (type: string): void => {
      if (type === "all-card") {
        setFilteredData(data);
      } else {
        setFilteredData(
          data.filter(
            (resourcesCards): boolean => resourcesCards.cardType === type,
          ),
        );
      }
    },
    [data],
  );

  return (
    <PageArticle $bgColor={"#dddde3"}>
      <ResourcesContainer>
        <FlexCenterItemsContainer>
          <Title color={"#2e2e38"} level={1} mt={4} size={"big"}>
            {bannerTitle}
          </Title>
        </FlexCenterItemsContainer>
        <FlexCenterItemsContainer>
          <Text color={"#5c5c70"} mt={1} size={"medium"}>
            {translate.t("resources.elementsText.banner.subTitle")}
          </Text>
        </FlexCenterItemsContainer>
        <MainCardContainer>
          <Title color={"#5c5c70"} level={1} size={"medium"}>
            {translate.t("resources.elementsText.rules.rulesDescription1")}
          </Title>
          <Title color={"#2e2e38"} level={1} mt={1} size={"small"}>
            {translate.t("resources.elementsText.rules.rulesTitle")}
          </Title>
          <Text color={"#787891"} mb={4} mt={1} size={"big"}>
            {translate.t("resources.elementsText.rules.rulesDescription2")}
          </Text>
          <AirsLink
            decoration={"none"}
            href={"https://help.fluidattacks.com/portal/en/kb/criteria"}
          >
            <Button display={"block"} variant={"primary"}>
              {translate.t("resources.elementsText.rules.rulesButton")}
            </Button>
          </AirsLink>
        </MainCardContainer>
        <MenuList>
          <ResourcesMenuElements filterData={filterData} />
        </MenuList>
        <CardsContainer>
          {filteredData.map((resourceCard): JSX.Element => {
            return (
              <ResourcesCard
                buttonText={resourceCard.buttonText}
                cardType={resourceCard.cardType}
                description={resourceCard.description}
                image={resourceCard.image}
                key={resourceCard.key}
                language={resourceCard.language}
                title={resourceCard.title}
                urlCard={resourceCard.urlCard}
              />
            );
          })}
        </CardsContainer>
      </ResourcesContainer>
    </PageArticle>
  );
};

export { ResourcesPage };
