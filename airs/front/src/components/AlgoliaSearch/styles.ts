import { Pagination, SearchBox } from "react-instantsearch";
import { styled } from "styled-components";

interface IThemeProps {
  theme: {
    background?: string;
    faded?: string;
    foreground?: string;
  };
}

const StyledSearchRoot = styled.div.attrs({
  className: `
    mh4
    mv1
    nt1
  `,
})``;

const StyledSearchBox = styled(SearchBox)`
  width: 100%;
  position: relative;

  form {
    display: flex;
    width: 100%;
    background: #f4f4f6;
    border-radius: 6px;
  }

  input {
    border: none;
    border-color: #b0b0b0;
    border-top-left-radius: 6px;
    border-bottom-left-radius: 6px;
    border-width: 1px;
    color: ${({ theme }: Readonly<IThemeProps>): string =>
      theme.foreground as string};
    font-size: 1em;
    outline: none;
    padding: 0.5rem;
    transition: 100ms;
    height: 49px;

    ::placeholder {
      color: ${({ theme }: Readonly<IThemeProps>): string =>
        theme.faded as string};
      margin-left: 2rem;
    }
    background: #f4f4f6;
    cursor: text;
    padding-left: 1.6em;
    width: 100%;
  }

  button[type="submit"],
  button[type="reset"] {
    background: none;
    border: none;
    cursor: pointer;
    outline: none;
    background: #f4f4f6;
    border-radius: 6px;
  }

  button[type="submit"]:hover,
  button[type="reset"]:hover {
    background: #b0b0b0;

    svg {
      fill: white;
    }
  }

  button[type="submit"] svg,
  button[type="reset"] svg {
    fill: #b0b0b0;
    width: 32px;
    height: 18px;
  }

  input[type="search"]::-ms-clear {
    display: none;
    width: 0;
    height: 0;
  }
  input[type="search"]::-ms-reveal {
    display: none;
    width: 0;
    height: 0;
  }
  input[type="search"]::-webkit-search-decoration,
  input[type="search"]::-webkit-search-cancel-button,
  input[type="search"]::-webkit-search-results-button,
  input[type="search"]::-webkit-search-results-decoration {
    display: none;
  }
`;

const display = ({ $show }: Readonly<{ $show: boolean }>): string =>
  $show ? `block` : `none`;

const StyledHitItems = styled.div.attrs({})<Readonly<{ $show: boolean }>>`
  display: ${display};
  background: #f9f9f9;
  margin-top: 0.5em;
  max-height: 80vh;
  max-width: 50em;
  overflow-y: auto;
  padding: 8px;
  position: absolute;
  scrollbar-color: #b0b0b0 #f9f9f9;
  scrollbar-width: thin;
  top: auto;
  width: 90%;
  z-index: 2;

  ::-webkit-scrollbar {
    width: 7px;
  }

  ::-webkit-scrollbar-track {
    background: #f9f9f9;
  }

  ::-webkit-scrollbar-thumb {
    background-color: #b0b0b0;
    border-radius: 15px;
  }

  .ais-Hits {
    .ais-Hits-list {
      list-style: none;
      margin-left: 0;
      padding-left: 0;
    }

    li.ais-Hits-item {
      margin-bottom: 1em;

      a {
        color: #272727 !important;
        text-decoration: none !important;
      }

      mark {
        background-color: #bf0b1a;
        color: #fff;
      }
    }
  }

  .ais-PoweredBy {
    display: flex;
    justify-content: flex-end;

    svg {
      width: 150px;
    }
  }

  @media (min-width: 768px) {
    width: 80vw;
  }
`;

const StyledPagination = styled(Pagination)`
  display: flex;
  justify-content: center;
  margin-top: 1em;
  width: 100%;

  ul {
    display: flex;
    justify-content: center;
    list-style: none;

    li.ais-Pagination-item--selected {
      background: #bf0b1a;
      background-clip: padding-box;
      border-color: #bf0b1a;
      a {
        color: white;
      }
      a:hover {
        color: white;
      }
    }

    li {
      border: 1px solid #b0b0b0;
      border-radius: 4px;
      font-weight: bold;
      margin: 0 0.5em;
      padding: 0.5em;

      &:hover {
        border-color: #bf0b1a;
      }

      a {
        color: #272727;
        text-decoration: none;

        &:hover {
          color: #bf0b1a;
        }
      }
    }
  }
`;

export { StyledSearchBox, StyledSearchRoot, StyledHitItems, StyledPagination };
