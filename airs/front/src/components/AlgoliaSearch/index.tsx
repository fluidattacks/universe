import { IoMdClose } from "@react-icons/all-files/io/IoMdClose";
import algoliasearch from "algoliasearch/lite";
import type { SearchClient } from "algoliasearch/lite";
import React, { createRef, useCallback, useMemo, useState } from "react";
import { Hits, InstantSearch, PoweredBy } from "react-instantsearch";
import { ThemeProvider } from "styled-components";

import { ResetIcon } from "./ResetIcon";
import {
  StyledHitItems,
  StyledPagination,
  StyledSearchBox,
  StyledSearchRoot,
} from "./styles";
import type { IProps } from "./types";

import { useClickOutside } from "../../utils/hooks/useClickOutside";
import { useWindowSize } from "../../utils/hooks/useWindowSize";
import { SearchResults } from "../AlgoliaSearchResults";
import { Button } from "../Button";
import { Container } from "../Container";

const theme = {
  background: "white",
  faded: "#888",
  foreground: "#050505",
};

const Search = ({ closeSearchBar, indices }: Readonly<IProps>): JSX.Element => {
  const rootRef = createRef<HTMLDivElement>();
  const [showHits, setShowHits] = useState(false);
  const { width } = useWindowSize();
  const searchClient = useMemo(
    (): SearchClient =>
      algoliasearch(
        process.env.GATSBY_ALGOLIA_APP_ID as string,
        process.env.GATSBY_ALGOLIA_SEARCH_KEY as string,
      ),
    [],
  );

  useClickOutside(rootRef, (): void => {
    setShowHits(false);
  });

  const queryHook = useCallback(
    (query: string, hook: (value: string) => void): void => {
      setShowHits(query.length > 0);
      hook(query);
    },
    [],
  );

  return (
    <ThemeProvider theme={theme}>
      <StyledSearchRoot ref={rootRef}>
        <InstantSearch
          future={{
            preserveSharedStateOnUnmount: true,
          }}
          indexName={indices[0].name}
          searchClient={searchClient}
        >
          <Container
            center={true}
            minWidth={width > 1199 ? "800px" : "0"}
            pv={width > 1199 ? 3 : 0}
            width={width > 1199 ? "45%" : "100%"}
          >
            <div
              className={width > 1199 ? "inline-flex flex-nowrap w-100" : ""}
              style={{ display: "flex", gap: "8px" }}
            >
              <StyledSearchBox
                placeholder={"Search"}
                queryHook={queryHook}
                resetIconComponent={ResetIcon}
              />
              {width > 1199 ? (
                <Button onClick={closeSearchBar} variant={"tertiary"}>
                  <IoMdClose size={15} />
                </Button>
              ) : undefined}
            </div>
            <StyledHitItems $show={showHits}>
              <PoweredBy />
              <Hits hitComponent={SearchResults} />
              <StyledPagination />
            </StyledHitItems>
          </Container>
        </InstantSearch>
      </StyledSearchRoot>
    </ThemeProvider>
  );
};

export { Search };
