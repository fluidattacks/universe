import type { MouseEventHandler } from "react";

interface IProps {
  indices: { name: string; title: string }[];
  closeSearchBar?: MouseEventHandler<HTMLButtonElement>;
}

interface IQueryProps {
  query: string | undefined;
}

export type { IProps, IQueryProps };
