import { graphql, useStaticQuery } from "gatsby";
import React from "react";

import { CardsContainer } from "../../styles/styles";
import { AirsLink } from "../AirsLink";
import { DropDownCard } from "../DropDownCard";

const PartnerPage: React.FC = (): JSX.Element => {
  const data: IData = useStaticQuery(graphql`
    query PartnerQuery {
      allMarkdownRemark(
        filter: { frontmatter: { partner: { eq: "yes" } } }
        sort: { frontmatter: { alt: ASC } }
      ) {
        edges {
          node {
            html
            frontmatter {
              alt
              partnerlogo
              slug
              title
            }
          }
        }
      }
    }
  `);

  const partnerInfo = data.allMarkdownRemark.edges;

  return (
    <React.Fragment>
      <CardsContainer>
        {partnerInfo.map(({ node }): JSX.Element => {
          const { alt, partnerlogo, slug, title } = node.frontmatter;

          return (
            <DropDownCard
              alt={alt}
              cardType={"partners-cards"}
              haveTitle={false}
              htmlData={node.html}
              key={slug}
              logo={partnerlogo}
              logoPaths={"/airs/partners"}
              slug={slug}
              title={title}
            />
          );
        })}
      </CardsContainer>
      <div>
        <p>
          {"If you are interested in partnering with"}
          <code>{"Fluid Attacks"}</code>
          {", please submit the following "}
          <AirsLink href={"/contact-us/"}>{"Contact Form."}</AirsLink>
        </p>
      </div>
    </React.Fragment>
  );
};

export { PartnerPage };
