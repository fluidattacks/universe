import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import { useStaticQuery } from "gatsby";
import React from "react";

import { PartnerPage } from ".";
import { allCloudinaryMediaData } from "test-utils/mocks";
import { translate } from "utils/translations/translate";
import type { ICloudinaryMedia } from "utils/types";

interface IDataPartnerPageMock {
  allCloudinaryMedia: ICloudinaryMedia;
  allMarkdownRemark: {
    edges: [
      {
        node: {
          fields: {
            slug: string;
          };
          frontmatter: {
            alt: string;
            partnerlogo: string;
            slug: string;
            title: string;
          };
          html: string;
        };
      },
    ];
  };
}

const mockUseStaticQuery = jest.spyOn({ useStaticQuery }, `useStaticQuery`);
const mockDataUseStaticQuery: IDataPartnerPageMock = {
  allCloudinaryMedia: allCloudinaryMediaData,
  allMarkdownRemark: {
    edges: [
      {
        node: {
          fields: {
            slug: "es",
          },
          frontmatter: {
            alt: "Alt content",
            partnerlogo: "Partner logo content",
            slug: "Slug content",
            title: "Title content",
          },
          html: "",
        },
      },
    ],
  },
};

describe("ServicesPage", (): void => {
  beforeEach((): void => {
    mockUseStaticQuery.mockImplementation(
      (): IDataPartnerPageMock => mockDataUseStaticQuery,
    );
  });
  afterEach((): void => {
    jest.restoreAllMocks();
  });
  it("PartnerPage should render mocked data", (): void => {
    expect.hasAssertions();

    render(<PartnerPage />);

    expect(screen.queryByText("Image not found")).toBeInTheDocument();
    expect(screen.queryByText(translate.t("clients.card"))).toBeInTheDocument();
  });
});
