import React from "react";

import type { IComplianceCta } from "./types";

import { Container } from "../Container";
import { CtaBanner } from "../CtaBanner";

const ComplianceCta = ({
  buttontxt,
  link,
  title,
  paragraph,
}: Readonly<IComplianceCta>): JSX.Element => (
  <Container ph={7} pv={3}>
    <CtaBanner
      button1Link={link}
      button1Text={buttontxt}
      buttonClassName={"btn-rompe-trafico"}
      paragraph={paragraph}
      pv={4}
      size={"xs"}
      textSize={"medium"}
      title={title}
      variant={"dark"}
    />
  </Container>
);

export { ComplianceCta };
