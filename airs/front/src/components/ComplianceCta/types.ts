interface IComplianceCta {
  buttontxt: string;
  link: string;
  title: string;
  paragraph?: string;
}

export type { IComplianceCta };
