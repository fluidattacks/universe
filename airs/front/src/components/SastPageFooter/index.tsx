/* eslint react/forbid-component-props: 0 */
import React from "react";

import {
  LanguagesListContainer,
  ListColumn,
  ListContainer,
  SastParagraph,
} from "./styles";

import { translate } from "../../utils/translations/translate";
import { CloudImage } from "../CloudImage";
import { Title } from "../Typography";

export const SastPageFooter: React.FC = (): JSX.Element => (
  <LanguagesListContainer>
    <SastParagraph>
      <CloudImage
        alt={"OWASP-logo"}
        src={"/airs/product/icon-logo-owasp-rojo"}
        styles={"pr3 w3 h3"}
      />
      <p className={"ma0"}>{translate.t("sastCategoryParagraph.phrase1")}</p>
      <b className={"ma0 poppins"}>
        {translate.t("sastCategoryParagraph.bold1")}
      </b>
      <div className={"poppins"}>
        <Title color={"#2e2e38"} level={3} mt={4} size={"small"}>
          {translate.t("sastCategoryParagraph.supportedLanguages")}
        </Title>
      </div>
    </SastParagraph>
    <ListContainer>
      <ListColumn>
        <li>{"C#"}</li>
        <li>{"Dart"}</li>
        <li>{"Go"}</li>
        <li>{"Java"}</li>
        <li>{"JavaScript/TypeScript"}</li>
        <li>{"Kotlin"}</li>
        <li>{"PHP"}</li>
        <li>{"Python"}</li>
        <li>{"Ruby"}</li>
        <li>{"Scala"}</li>
        <li>{"Swift"}</li>
      </ListColumn>
      <ListColumn>
        <li>{"Android (XML)"}</li>
        <li>{"Ansible (YAML)"}</li>
        <li>{"ARM (JSON)"}</li>
        <li>{"Azure Blueprints (YAML)"}</li>
        <li>{"Cloudformation (YAML)"}</li>
        <li>{"Docker (Docker)"}</li>
        <li>{"Docker Compose (YAML)"}</li>
        <li>{"Helm (YAML)"}</li>
        <li>{"Kubernetes (YAML)"}</li>
        <li>{"Terraform (HCL)"}</li>
      </ListColumn>
      <ListColumn>
        <li>{"Angular"}</li>
        <li>{"AngularJS"}</li>
        <li>{"ASP.NET"}</li>
        <li>{"ASP.NET Core"}</li>
        <li>{"Django"}</li>
        <li>{"Express"}</li>
        <li>{"FastAPI"}</li>
        <li>{"Flask"}</li>
        <li>{"Flutter"}</li>
        <li>{"Gin"}</li>
        <li>{"Koa"}</li>
      </ListColumn>
      <ListColumn className={"v-top"}>
        <li>{"Laravel"}</li>
        <li>{"Microsoft .NET"}</li>
        <li>{"NestJS"}</li>
        <li>{"Next.js"}</li>
        <li>{"Node.js"}</li>
        <li>{"React Native"}</li>
        <li>{"Ruby on Rails"}</li>
        <li>{"Spring Boot"}</li>
        <li>{"Starlette"}</li>
        <li>{"Struts"}</li>
        <li>{"Vue.js"}</li>
      </ListColumn>
    </ListContainer>
  </LanguagesListContainer>
);
