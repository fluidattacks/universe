import React from "react";

import { LineItem } from "./LineItem";
import { ContainerTimeLapse } from "./styles";
import { TimeItem } from "./TimeItem";

interface IProps {
  confirmed: string;
  contacted: string;
  disclosure: string;
  discovered: string;
  // eslint-disable-next-line react/require-default-props
  image?: boolean;
  patched: string;
  replied: string;
}

const TimeLapse = ({
  confirmed,
  contacted,
  disclosure,
  discovered,
  image = false,
  patched,
  replied,
}: IProps): JSX.Element => (
  <ContainerTimeLapse>
    <TimeItem
      date={discovered}
      image={image}
      text={"Vulnerability discovered."}
    />
    <LineItem />
    <TimeItem date={contacted} image={image} text={"Vendor contacted."} />
    {replied ? (
      <React.Fragment>
        <LineItem />
        <TimeItem
          date={replied}
          image={image}
          text={"Vendor replied acknowledging the report."}
        />
      </React.Fragment>
    ) : undefined}
    {confirmed ? (
      <React.Fragment>
        <LineItem />
        <TimeItem
          date={confirmed}
          image={image}
          text={"Vendor Confirmed the vulnerability."}
        />
      </React.Fragment>
    ) : undefined}
    {patched ? (
      <React.Fragment>
        <LineItem />
        <TimeItem
          date={patched}
          image={image}
          text={"Vulnerability patched."}
        />
      </React.Fragment>
    ) : undefined}
    {disclosure ? (
      <React.Fragment>
        <LineItem />
        <TimeItem date={disclosure} image={image} text={"Public Disclosure."} />
      </React.Fragment>
    ) : undefined}
  </ContainerTimeLapse>
);

export type { IProps };
export { TimeLapse };
