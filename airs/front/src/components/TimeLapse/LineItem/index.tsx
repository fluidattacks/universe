import React from "react";

import { Line, LineCol } from "../styles";

const LineItem: React.FC = (): JSX.Element => (
  <LineCol>
    <Line />
  </LineCol>
);

export { LineItem };
