import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import { useStaticQuery } from "gatsby";
import React from "react";

import { TimeItem } from "./TimeItem";

import { TimeLapse } from ".";
import { allCloudinaryMediaData } from "test-utils/mocks";
import type { ICloudinaryMedia } from "utils/types";

interface IDataImageMock {
  allCloudinaryMedia: ICloudinaryMedia;
}

const mockUseStaticQuery = jest.spyOn({ useStaticQuery }, `useStaticQuery`);
const mockDataUseStaticQuery: IDataImageMock = {
  allCloudinaryMedia: allCloudinaryMediaData,
};

describe("TimeLapse", (): void => {
  beforeEach((): void => {
    mockUseStaticQuery.mockImplementation(
      (): IDataImageMock => mockDataUseStaticQuery,
    );
  });

  afterEach((): void => {
    jest.restoreAllMocks();
  });
  it("should render TimeLapse container", (): void => {
    expect.hasAssertions();

    const { container } = render(
      <TimeLapse
        confirmed={"2023-01-01"}
        contacted={"2023-01-02"}
        disclosure={"2023-01-03"}
        discovered={"2023-01-04"}
        patched={"2023-01-05"}
        replied={"2023-01-06"}
      />,
    );

    expect(container).toBeInTheDocument();
  });

  it("should render TimeLapse container without some variables", (): void => {
    expect.hasAssertions();

    const { container } = render(
      <TimeLapse
        confirmed={""}
        contacted={"2023-01-02"}
        disclosure={""}
        discovered={"2023-01-04"}
        patched={""}
        replied={""}
      />,
    );

    expect(container).toBeInTheDocument();
  });

  describe("TimeItem", (): void => {
    it("renders image from props when image prop is true", (): void => {
      expect.hasAssertions();

      render(<TimeItem date={"2022-01-01"} image={true} text={"Test"} />);

      expect(screen.getByAltText("Time-lapse-logo")).toBeInTheDocument();
      expect(screen.getByText("2022-01-01")).toBeInTheDocument();
      expect(screen.getByText("Test")).toBeInTheDocument();
    });

    it("renders CloudImage when image prop is false", (): void => {
      expect.hasAssertions();

      render(<TimeItem date={"2022-01-01"} image={false} text={"Test"} />);

      expect(screen.getByText("Image not found")).toBeInTheDocument();
      expect(screen.getByText("2022-01-01")).toBeInTheDocument();
      expect(screen.getByText("Test")).toBeInTheDocument();
    });
  });
});
