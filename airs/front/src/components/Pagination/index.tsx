import { FiChevronLeft } from "@react-icons/all-files/fi/FiChevronLeft";
import { FiChevronRight } from "@react-icons/all-files/fi/FiChevronRight";
import React from "react";
import ReactPaginate from "react-paginate";

interface IPaginatorHandlers {
  onChange: (prop: { selected: number }) => void;
}
interface IPaginator extends IPaginatorHandlers {
  forcePage: number;
  pageCount: number;
}

export const Pagination = ({
  forcePage,
  onChange,
  pageCount,
}: Readonly<IPaginator>): JSX.Element => (
  <ReactPaginate
    activeLinkClassName={"active"}
    breakClassName={"break-item"}
    containerClassName={"pagination-container"}
    forcePage={forcePage}
    marginPagesDisplayed={1}
    nextClassName={"page-item"}
    nextLabel={<FiChevronRight />}
    nextLinkClassName={"page-link"}
    onPageChange={onChange}
    pageClassName={"page-item"}
    pageCount={pageCount}
    pageLinkClassName={"page-link"}
    pageRangeDisplayed={3}
    previousClassName={"page-item"}
    previousLabel={<FiChevronLeft />}
    previousLinkClassName={"page-link"}
  />
);
