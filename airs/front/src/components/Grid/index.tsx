import React from "react";

import { StyledGrid } from "./styles";
import type { IGridProps } from "./types";

const Grid = ({
  children,
  columns,
  columnsMd,
  columnsSm,
  gap,
  pv,
  ph,
}: Readonly<IGridProps>): JSX.Element => {
  return (
    <StyledGrid
      $columns={columns}
      $columnsMd={columnsMd}
      $columnsSm={columnsSm}
      $gap={gap}
      $ph={ph}
      $pv={pv}
      role={"grid"}
    >
      {children}
    </StyledGrid>
  );
};

export { Grid };
