import "@testing-library/jest-dom";
import { fireEvent, render } from "@testing-library/react";
import React from "react";

import { FloatingContactButton } from ".";

describe("FloatingContactButton", (): void => {
  it("should render", (): void => {
    expect.hasAssertions();

    const { getByText } = render(
      <FloatingContactButton
        background={"#bf0b1a"}
        backgroundHover={"#ffffff"}
        color={"#111111"}
        language={"es"}
        text={"Text"}
        yPosition={"50%"}
      />,
    );

    fireEvent.click(getByText("Text"));

    expect(getByText("Text")).toBeInTheDocument();
  });
});
