/* eslint react/forbid-component-props: 0 */
import React, { useCallback, useState } from "react";
import { createPortal } from "react-dom";

import {
  CloseButtonContainer,
  Container,
  Dialog,
  FloatButton,
} from "./styledComponents";

import { Button } from "../Button";

interface IFloatingButton {
  background: string;
  backgroundHover: string;
  color: string;
  language: string;
  text: string;
  yPosition: string;
}

const FloatingContactButton: React.FC<IFloatingButton> = ({
  background,
  backgroundHover,
  color,
  language,
  text,
  yPosition,
}: IFloatingButton): JSX.Element => {
  const [open, setOpen] = useState(false);
  const src =
    language === "es"
      ? "https://forms.zohopublic.com/fluid4ttacks/form/ContactForm/formperma/g4Je_6zumBrCA3iMMJMHU6b4JrVrUzItp_cfyb3ad74?zf_lang=es"
      : "https://forms.zohopublic.com/fluid4ttacks/form/ContactForm/formperma/g4Je_6zumBrCA3iMMJMHU6b4JrVrUzItp_cfyb3ad74";

  const handleCLick = useCallback((): void => {
    document.getElementById("closeInformativeBanner")?.click();
    setOpen((previousState): boolean => !previousState);
  }, []);

  return (
    <FloatButton
      $background={background}
      $backgroundHover={backgroundHover}
      $color={color}
      $yPosition={yPosition}
      onClick={handleCLick}
    >
      {text}
      {open
        ? createPortal(
            <Container>
              <Dialog>
                <CloseButtonContainer>
                  <Button customSize={{ fontSize: 20, ph: 0, pv: 0 }}>
                    {"X"}
                  </Button>
                </CloseButtonContainer>
                <iframe
                  sandbox={
                    "allow-forms allow-top-navigation allow-same-origin allow-scripts allow-popups"
                  }
                  src={src}
                  style={{
                    border: "0",
                    height: "95%",
                    width: "100%",
                  }}
                  title={"Contact Us Form"}
                />
              </Dialog>
            </Container>,
            document.body,
          )
        : null}
    </FloatButton>
  );
};

export { FloatingContactButton };
