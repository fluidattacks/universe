import dayjs from "dayjs";

import { dateFilter, filterPostsByLanguage } from "./filter-posts";
import { translate } from "./translations/translate";

describe("filters", (): void => {
  const posts: INodes[] = [
    {
      node: {
        fields: {
          slug: "/blog/testing/",
        },
        frontmatter: {
          alt: "Photo by Doe on Fluid",
          author: "Fluid testing",
          category: "development",
          certification: "string",
          date: dayjs().subtract(3, "months").toISOString(),
          description: "post description",
          identifier: "testting",
          image: "string",
          slug: "testing/",
          spanish: "string",
          subtitle: "string",
          tags: "development, testing, compliance",
          title: "testing",
        },
      },
    },
    {
      node: {
        fields: {
          slug: "/es/blog/unitestting/",
        },
        frontmatter: {
          alt: "alt",
          author: "fluid testing",
          category: "development",
          certification: "string",
          date: dayjs().subtract(1, "weeks").toISOString(),
          description: "post description",
          identifier: "blog/unitesting",
          image: "string",
          slug: "unitestting/",
          spanish: "string",
          subtitle: "string",
          tags: "development, testing, compliance",
          title: "testing",
        },
      },
    },
    {
      node: {
        fields: {
          slug: "/es/blog/unitestting/",
        },
        frontmatter: {
          alt: "alt",
          author: "fluid testing",
          category: "development",
          certification: "string",
          date: dayjs().subtract(10, "months").toISOString(),
          description: "post description",
          identifier: "blog/unitesting",
          image: "string",
          slug: "unitestting/",
          spanish: "string",
          subtitle: "string",
          tags: "development, testing, compliance",
          title: "testing",
        },
      },
    },
  ];
  it("should return an array of posts in the current language (en)", (): void => {
    expect.hasAssertions();
    expect(filterPostsByLanguage(posts)).toEqual([posts[0]]);
    jest.clearAllMocks();
  });

  it("should return an array of posts in the last month", (): void => {
    expect.hasAssertions();
    const result = posts.filter((post): boolean =>
      dateFilter(post, translate.t("blogListDatePeriods.lastMonth")),
    );
    expect(result).toEqual([posts[1]]);
  });

  it("should return an array of posts in the last six months", (): void => {
    expect.hasAssertions();
    const result = posts.filter((post): boolean =>
      dateFilter(post, translate.t("blogListDatePeriods.lastSixMonths")),
    );
    expect(result).toEqual([posts[0], posts[1]]);
  });

  it("should return an array of posts in the last year", (): void => {
    expect.hasAssertions();
    const result = posts.filter((post): boolean =>
      dateFilter(post, translate.t("blogListDatePeriods.lastYear")),
    );
    expect(result).toEqual([posts[0], posts[1], posts[2]]);
  });
});
