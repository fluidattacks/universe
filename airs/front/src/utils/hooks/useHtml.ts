/*  eslint-disable @typescript-eslint/no-explicit-any */
import type { ReactNode } from "react";
import prod from "react/jsx-runtime";
import rehypeHighlight from "rehype-highlight";
import rehypeParse from "rehype-parse";
import rehypeReact from "rehype-react";
import { unified } from "unified";

// @ts-expect-error: the react types are missing.
const production = { Fragment: prod.Fragment, jsx: prod.jsx, jsxs: prod.jsxs };

const useHtml = (
  components: Record<string, React.FC<any>>,
  html: string,
): ReactNode => {
  const htmlNoSpaces = html.replace(
    /(?<temp2><code>[\s\S]*?<\/code>)|(?<temp1>[^\S\r\n]+)(?![^<>]*<\/code>)/gu,
    (match, codeTag: string, space: string): string => {
      if (codeTag) {
        return codeTag;
      } else if (space) {
        return " ";
      }

      return match;
    },
  );

  const file = unified()
    .use(rehypeParse, { fragment: true })
    .use(rehypeHighlight)
    .use(rehypeReact, {
      ...production,
      components,
    })
    .processSync(htmlNoSpaces);

  return file.result;
};

export { useHtml };
