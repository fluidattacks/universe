import { describe, test } from "@jest/globals";
import { render, screen } from "@testing-library/react";
import React from "react";
import "@testing-library/jest-dom";

import { useBlogsDate, useDateYear } from "./useSafeDate";
import { useWindowLocation } from "./useWindowLocation";

describe("Utils", (): void => {
  const TestComponent = (): JSX.Element => {
    const year = useDateYear();
    const blogDate = useBlogsDate("2023-09-22");
    const spanishBlogDate = useBlogsDate("2023-09-22", "es");
    const location = useWindowLocation();

    return (
      <React.Fragment>
        <div>{blogDate}</div>
        <div>{spanishBlogDate}</div>
        <div>{year}</div>
        <div>{location}</div>
      </React.Fragment>
    );
  };
  test("useDateYear should return actual year", (): void => {
    expect.hasAssertions();

    const { getByText } = render(<TestComponent />);
    const yearElement = getByText(new Date().getFullYear().toString());

    expect(yearElement).toBeInTheDocument();
  });

  test("useBlogDate should format english date", (): void => {
    expect.hasAssertions();

    render(<TestComponent />);

    expect(screen.queryByText("September 22, 2023")).toBeInTheDocument();
  });

  test("useBlogDate should format spanish date", (): void => {
    expect.hasAssertions();

    render(<TestComponent />);

    expect(screen.queryByText("septiembre 22, 2023")).toBeInTheDocument();
  });
});

describe("Window mock to production", (): void => {
  const { location } = window;
  jest.spyOn(window, "location", "get").mockRestore();

  it("Window location should be blog/", (): void => {
    expect.hasAssertions();

    const mockedLocation = {
      ...location,
      href: "https://www.fluidattacks.com/blog/",
    };
    jest.spyOn(window, "location", "get").mockReturnValue(mockedLocation);
    const expectedLocation = "blog/";
    const result = useWindowLocation();
    jest.spyOn(window, "location", "get").mockRestore();

    expect(result).toBe(expectedLocation);
  });
});

describe("Window mock to dev", (): void => {
  const { location } = window;
  jest.spyOn(window, "location", "get").mockRestore();

  it("Window location should be learn/", (): void => {
    expect.hasAssertions();

    const mockedLocation = {
      ...location,
      href: "https://www.localhost.com/learn/",
    };
    jest.spyOn(window, "location", "get").mockReturnValue(mockedLocation);
    const expectedLocation = "learn/";
    const result = useWindowLocation();
    jest.spyOn(window, "location", "get").mockRestore();

    expect(result).toBe(expectedLocation);
  });
});
