import React from "react";

import { Seo } from "../components/Seo";
import { LearnPage } from "../scenes/LearnPage";
import { WebsiteWrapper } from "../scenes/WebsiteWrapper";
import { PageArticle } from "../styles/styles";
import { i18next, translate } from "../utils/translations/translate";
import { updateLanguage } from "../utils/utilities";

const LearnIndex: React.FC<IQueryData> = ({
  pageContext,
}: IQueryData): JSX.Element => {
  const { language } = pageContext;
  void updateLanguage(language);

  return (
    <WebsiteWrapper>
      <PageArticle $bgColor={"#dddde3"}>
        <LearnPage />
      </PageArticle>
    </WebsiteWrapper>
  );
};

export const Head = (): JSX.Element => {
  const learnImage =
    "https://res.cloudinary.com/fluid-attacks/image/upload/v1619632208/airs/bg-blog_bj0szx";
  const languagePrefix = i18next.language === "es" ? "es/" : "";

  return (
    <Seo
      description={translate.t("learn.description")}
      image={learnImage}
      keywords={translate.t("learn.keywords")}
      path={`https://fluidattacks.com/${languagePrefix}learn/`}
      title={`Learn ${translate.t("learnPageTitle.title1")}`}
    />
  );
};

export default LearnIndex;
