import React from "react";

import { Seo } from "../components/Seo";
import { BlogsPage } from "../scenes/BlogsPage";
import { WebsiteWrapper } from "../scenes/WebsiteWrapper";
import { PageArticle } from "../styles/styles";
import { i18next, translate } from "../utils/translations/translate";
import { updateLanguage } from "../utils/utilities";

const BlogIndex: React.FC<IQueryData> = ({
  pageContext,
}: IQueryData): JSX.Element => {
  const { language } = pageContext;
  void updateLanguage(language);

  return (
    <WebsiteWrapper>
      <PageArticle $bgColor={"#dddde3"}>
        <BlogsPage />
      </PageArticle>
    </WebsiteWrapper>
  );
};

export const Head = (): JSX.Element => {
  const blogImage: string =
    "https://res.cloudinary.com/fluid-attacks/image/upload/v1619632208/airs/bg-blog_bj0szx";
  const languagePrefix = i18next.language === "es" ? "es/" : "";

  return (
    <Seo
      author={"Fluid Attacks"}
      description={translate.t("blog.description")}
      image={blogImage}
      keywords={translate.t("blog.keywords")}
      path={`https://fluidattacks.com/${languagePrefix}blog/`}
      title={`Blog ${translate.t("blogPageTitle.title2")}`}
    />
  );
};

export default BlogIndex;
