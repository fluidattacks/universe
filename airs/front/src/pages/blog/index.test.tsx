import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import React from "react";

import AuthorsIndex from "./authors";
import CategoriesIndex from "./categories";
import TagsIndex from "./tags";

import { exampleQueryData } from "test-utils/mocks";

describe("Pages", (): void => {
  describe("blog", (): void => {
    it("renders TagsIndex with correctly mocked data", (): void => {
      expect.hasAssertions();

      render(
        <TagsIndex
          data={exampleQueryData.data}
          pageContext={exampleQueryData.pageContext}
        />,
      );

      expect(screen.getByText("blog.tags.title")).toBeInTheDocument();
    });
  });

  describe("categories", (): void => {
    it("renders CategoriesIndex with correctly mocked data", (): void => {
      expect.hasAssertions();

      const { container } = render(
        <CategoriesIndex
          data={exampleQueryData.data}
          pageContext={exampleQueryData.pageContext}
        />,
      );

      expect(container).toBeInTheDocument();
    });
  });

  describe("authors", (): void => {
    it("renders AuthorsIndex with correctly mocked data", (): void => {
      expect.hasAssertions();

      const { container } = render(
        <AuthorsIndex
          data={exampleQueryData.data}
          pageContext={exampleQueryData.pageContext}
        />,
      );

      expect(container).toBeInTheDocument();
    });
  });
});
