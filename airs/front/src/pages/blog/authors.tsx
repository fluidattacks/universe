import React from "react";

import { Seo } from "../../components/Seo";
import { AuthorsPage } from "../../scenes/AuthorsPage";
import { WebsiteWrapper } from "../../scenes/WebsiteWrapper";
import { PageArticle } from "../../styles/styles";
import { translate } from "../../utils/translations/translate";
import { updateLanguage } from "../../utils/utilities";

const AuthorsIndex: React.FC<IQueryData> = ({
  pageContext,
}: IQueryData): JSX.Element => {
  const { language } = pageContext;
  void updateLanguage(language);

  return (
    <WebsiteWrapper>
      <PageArticle $bgColor={"#f9f9f9"}>
        <AuthorsPage />
      </PageArticle>
    </WebsiteWrapper>
  );
};

export const Head = (): JSX.Element => {
  return (
    <Seo
      description={translate.t("blog.listDescriptions.authors.description")}
      keywords={translate.t("blog.keywords")}
      path={"https://fluidattacks.com/blog/authors/"}
      title={`${translate.t("blog.authors.title")} | Blog | Fluid Attacks`}
    />
  );
};

export default AuthorsIndex;
