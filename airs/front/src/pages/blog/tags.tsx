import React from "react";

import { Seo } from "../../components/Seo";
import { TagsPage } from "../../scenes/TagsPage";
import { WebsiteWrapper } from "../../scenes/WebsiteWrapper";
import { PageArticle } from "../../styles/styles";
import { translate } from "../../utils/translations/translate";
import { updateLanguage } from "../../utils/utilities";

const TagsIndex: React.FC<IQueryData> = ({
  pageContext,
}: IQueryData): JSX.Element => {
  const { language } = pageContext;
  void updateLanguage(language);

  return (
    <WebsiteWrapper>
      <PageArticle $bgColor={"#f9f9f9"}>
        <TagsPage />
      </PageArticle>
    </WebsiteWrapper>
  );
};

export const Head = ({ pageContext }: IQueryData): JSX.Element => {
  const { language } = pageContext;
  const languagePrefix = language === "es" ? "es/" : "";

  return (
    <Seo
      description={translate.t("blog.listDescriptions.tags.description")}
      keywords={translate.t("blog.keywords")}
      path={`https://fluidattacks.com/${languagePrefix}blog/tags/`}
      title={`${translate.t("blog.tags.title")} | Blog | Fluid Attacks`}
    />
  );
};

export default TagsIndex;
