import React from "react";

import { Seo } from "../../components/Seo";
import { CategoriesPage } from "../../scenes/CategoriesPage";
import { WebsiteWrapper } from "../../scenes/WebsiteWrapper";
import { PageArticle } from "../../styles/styles";
import { translate } from "../../utils/translations/translate";
import { updateLanguage } from "../../utils/utilities";

const CategoriesIndex: React.FC<IQueryData> = ({
  pageContext,
}: IQueryData): JSX.Element => {
  const { language } = pageContext;
  void updateLanguage(language);

  return (
    <WebsiteWrapper>
      <PageArticle $bgColor={"#f9f9f9"}>
        <CategoriesPage />
      </PageArticle>
    </WebsiteWrapper>
  );
};

export const Head = ({ pageContext }: IQueryData): JSX.Element => {
  const { language } = pageContext;
  const sitePath =
    language === "en" ? "blog/categories/" : "es/blog/categorias/";

  return (
    <Seo
      description={translate.t("blog.listDescriptions.categories.description")}
      keywords={translate.t("blog.keywords")}
      path={`https://fluidattacks.com/${sitePath}`}
      title={`${translate.t("blog.categories.title")} | Blog | Fluid Attacks`}
    />
  );
};

export default CategoriesIndex;
