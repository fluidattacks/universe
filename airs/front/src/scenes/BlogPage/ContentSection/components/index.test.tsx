import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import React from "react";

import { Header3 } from "./Header3";
import { Header4 } from "./Header4";

describe("Components", (): void => {
  it("Header 3 should render string child", (): void => {
    expect.hasAssertions();

    render(
      <Header3 color={"#2e2e38"} level={3}>
        {"String child"}
      </Header3>,
    );
    expect(screen.queryByText("String child")).toBeInTheDocument();
  });
  it("Header 3 should render number child", (): void => {
    expect.hasAssertions();

    render(
      <Header3 color={"#2e2e38"} level={3}>
        {2}
      </Header3>,
    );
    expect(screen.queryByText(2)).toBeInTheDocument();
  });

  it("Header 4 should render string child", (): void => {
    expect.hasAssertions();

    render(
      <Header4 color={"#2e2e38"} level={4}>
        {"String child"}
      </Header4>,
    );
    expect(screen.queryByText("String child")).toBeInTheDocument();
  });
  it("Header 4 should render number child", (): void => {
    expect.hasAssertions();

    render(
      <Header4 color={"#2e2e38"} level={4}>
        {2}
      </Header4>,
    );
    expect(screen.queryByText(2)).toBeInTheDocument();
  });
});
