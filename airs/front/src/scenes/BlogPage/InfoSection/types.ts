interface IInfoProps {
  author: string;
  date: string;
  timeToRead?: number;
  writer: string;
}

export type { IInfoProps };
