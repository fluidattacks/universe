import React from "react";

import { ContentSection } from "./ContentSection";
import { CtaSection } from "./CtaSection";
import { HeaderSection } from "./HeaderSection";
import { InfoSection } from "./InfoSection";
import { SlideSection } from "./SlideSection";
import { SubscribeSection } from "./SubscribeSection";
import { TagsSection } from "./TagsSection";

import { Container } from "../../components/Container";
import { TableOfContents } from "../../components/TableOfContents";
import { useWindowSize } from "../../utils/hooks/useWindowSize";

interface IBlogPageProps {
  author: string;
  category: string;
  date: string;
  description: string;
  headings: [
    {
      depth: number;
      value: string;
    },
  ];
  html: string;
  image: string;
  language: string;
  slug: string;
  subtitle: string;
  tags: string;
  timeToRead: number;
  title: string;
  writer: string;
}

const BlogPage: React.FC<IBlogPageProps> = ({
  author,
  category,
  description,
  date,
  headings,
  html,
  image,
  language,
  slug,
  subtitle,
  tags,
  timeToRead,
  title,
  writer,
}): JSX.Element => {
  const { width } = useWindowSize();

  return (
    <React.Fragment>
      <HeaderSection
        category={category}
        description={description}
        image={image}
        subtitle={subtitle}
        title={title}
      />
      <InfoSection
        author={author}
        date={date}
        timeToRead={timeToRead}
        writer={writer}
      />
      <Container center={true} display={width < 1200 ? "flex" : "none"}>
        <TableOfContents headings={headings} />
      </Container>
      <ContentSection
        headings={headings}
        html={html}
        language={language}
        slug={slug}
      />
      <TagsSection tags={tags} />
      <SubscribeSection />
      <SlideSection slug={slug} tags={tags} />
      <CtaSection />
    </React.Fragment>
  );
};

export { BlogPage };
