/* eslint react/forbid-component-props: 0 */
import React from "react";

import {
  Cell,
  ComparativeTable,
  DescriptionCell,
  DescriptionContainer,
  HeadColumn,
  LeftColumn,
  LevelDescLeftColumn,
  LevelDescRightColumn,
  RightColumn,
  Row,
  TableContainer,
} from "./styles";

import { AirsLink } from "../../../components/AirsLink";
import { CloudImage } from "../../../components/CloudImage";
import { Container } from "../../../components/Container";
import { Grid } from "../../../components/Grid";
import { Text, Title } from "../../../components/Typography";
import { translate } from "../../../utils/translations/translate";

interface ICells {
  scores: number[];
  text: string;
}

const ComparativeWithOthers: React.FC = (): JSX.Element => {
  const heads: string[] = [
    translate.t(
      "plansPage.comparativeWithOthers.characteristics.characteristic1",
    ),

    translate.t(
      "plansPage.comparativeWithOthers.characteristics.characteristic2",
    ),
    translate.t(
      "plansPage.comparativeWithOthers.characteristics.characteristic3",
    ),
    translate.t(
      "plansPage.comparativeWithOthers.characteristics.characteristic4",
    ),
    translate.t(
      "plansPage.comparativeWithOthers.characteristics.characteristic5",
    ),
    translate.t(
      "plansPage.comparativeWithOthers.characteristics.characteristic6",
    ),
    translate.t(
      "plansPage.comparativeWithOthers.characteristics.characteristic7",
    ),
    translate.t(
      "plansPage.comparativeWithOthers.characteristics.characteristic8",
    ),
  ];

  const cells: ICells[] = [
    {
      scores: [4, 4, 4, 4, 4, 4, 4, 4],
      text: translate.t("plansPage.comparativeWithOthers.company.company1"),
    },
    {
      scores: [3, 1, 2, 2, 0, 4, 2, 0],
      text: translate.t("plansPage.comparativeWithOthers.company.company2"),
    },
    {
      scores: [2, 0, 0, 1, 0, 0, 0, 0],
      text: translate.t("plansPage.comparativeWithOthers.company.company3"),
    },
    {
      scores: [3, 1, 2, 2, 0, 4, 2, 0],
      text: translate.t("plansPage.comparativeWithOthers.company.company4"),
    },
    {
      scores: [2, 1, 1, 3, 0, 4, 1, 0],
      text: translate.t("plansPage.comparativeWithOthers.company.company5"),
    },
    {
      scores: [2, 1, 2, 2, 0, 4, 2, 2],
      text: translate.t("plansPage.comparativeWithOthers.company.company6"),
    },
  ];

  const descriptions: string[] = [
    translate.t("plansPage.comparativeWithOthers.descriptions.description1"),
    translate.t("plansPage.comparativeWithOthers.descriptions.description2"),
    translate.t("plansPage.comparativeWithOthers.descriptions.description3"),
    translate.t("plansPage.comparativeWithOthers.descriptions.description4"),
    translate.t("plansPage.comparativeWithOthers.descriptions.description5"),
    translate.t("plansPage.comparativeWithOthers.descriptions.description6"),
    translate.t("plansPage.comparativeWithOthers.descriptions.description7"),
    translate.t("plansPage.comparativeWithOthers.descriptions.description8"),
  ];

  const levels: string[] = [
    translate.t("plansPage.comparativeWithOthers.levels.level1"),
    translate.t("plansPage.comparativeWithOthers.levels.level2"),
    translate.t("plansPage.comparativeWithOthers.levels.level3"),
    translate.t("plansPage.comparativeWithOthers.levels.level4"),
    translate.t("plansPage.comparativeWithOthers.levels.level5"),
  ];

  const url =
    "https://help.fluidattacks.com/portal/en/kb/find-security-vulnerabilities/compare-fluid-attacks-with-others";

  return (
    <Container
      align={"center"}
      bgColor={"#ffffff"}
      display={"flex"}
      id={"comparative-plans-table"}
      justify={"center"}
      ph={4}
      pv={5}
      wrap={"wrap"}
    >
      <Title
        color={"#2e2e38"}
        level={1}
        mb={3}
        size={"medium"}
        textAlign={"center"}
      >
        {translate.t("plansPage.comparativeWithOthers.title")}
      </Title>
      <TableContainer>
        <ComparativeTable>
          <thead>
            <tr>
              <th />
              {[...Array(8).keys()].map((el: number): JSX.Element => {
                const pos = el === 0 ? "first" : "intermediate";
                const lstPos = el === 7 ? "last" : pos;

                return (
                  <HeadColumn $position={lstPos} key={`head-${el}`}>
                    <Title
                      color={"#ffffff"}
                      level={3}
                      size={"xxs"}
                      textAlign={"center"}
                    >
                      {heads[el]}
                    </Title>
                  </HeadColumn>
                );
              })}
            </tr>
          </thead>
          <tbody>
            {[...Array(6).keys()].map((rowEl: number): JSX.Element => {
              const pos = rowEl === 0 ? "first" : "intermediate";
              const lstPos = rowEl === 5 ? "last" : pos;

              return (
                <Row key={`row-${rowEl}`}>
                  <DescriptionCell
                    $position={lstPos}
                    key={`companyCell-${rowEl}`}
                  >
                    <Text color={"#2e2e38"} weight={"bold"}>
                      {cells[rowEl].text}
                    </Text>
                  </DescriptionCell>
                  {[...Array(8).keys()].map(
                    (scoreEl: number): JSX.Element => (
                      <Cell
                        $isBottom={lstPos === "last"}
                        $isLast={scoreEl === 7}
                        key={`cell-${scoreEl}`}
                      >
                        <CloudImage
                          alt={`comp-lvl-${cells[rowEl].scores[scoreEl]}`}
                          src={`airs/plans/comp-lvl-${cells[rowEl].scores[scoreEl]}`}
                          width={90}
                        />
                      </Cell>
                    ),
                  )}
                </Row>
              );
            })}
          </tbody>
        </ComparativeTable>
      </TableContainer>
      <DescriptionContainer>
        <LeftColumn>
          {[...Array(8).keys()].map(
            (el: number): JSX.Element => (
              <React.Fragment key={`desc-${el}`}>
                {descriptions[el]}
                <br />
              </React.Fragment>
            ),
          )}
          <Text color={"#2e2e38"} mt={4}>
            {translate.t(
              "plansPage.comparativeWithOthers.descriptions.description9",
            )}
            <AirsLink href={url}>{url}</AirsLink>
          </Text>
        </LeftColumn>
        <RightColumn>
          <Title
            color={"#2e2e38"}
            level={4}
            ml={3}
            size={"xs"}
            textAlign={"start"}
          >
            {translate.t("plansPage.comparativeWithOthers.levels.title")}
          </Title>
          <Grid columns={2} gap={"0rem"}>
            {[...Array(5).keys()].map((el: number): JSX.Element => {
              return (
                <React.Fragment key={`cap-${el}`}>
                  <LevelDescLeftColumn>
                    <CloudImage
                      alt={"comp-lvl-0"}
                      src={`airs/plans/comp-lvl-${el}`}
                      width={90}
                    />
                  </LevelDescLeftColumn>
                  <LevelDescRightColumn>
                    <Text color={"#000000"} textAlign={"start"}>
                      {levels[el]}
                    </Text>
                  </LevelDescRightColumn>
                </React.Fragment>
              );
            })}
          </Grid>
        </RightColumn>
      </DescriptionContainer>
    </Container>
  );
};

export { ComparativeWithOthers };
