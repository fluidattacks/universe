import { render } from "@testing-library/react";
import { useStaticQuery } from "gatsby";
import "@testing-library/jest-dom";
import React from "react";

import { ComparativeWithOthers } from ".";
import { allCloudinaryMediaData } from "test-utils/mocks";
import type { ICloudinaryMedia } from "utils/types";

interface IDataImageMock {
  allCloudinaryMedia: ICloudinaryMedia;
}

const mockUseStaticQuery = jest.spyOn({ useStaticQuery }, `useStaticQuery`);
const mockDataUseStaticQuery: IDataImageMock = {
  allCloudinaryMedia: allCloudinaryMediaData,
};

describe("ComparativeWithOthers", (): void => {
  beforeEach((): void => {
    mockUseStaticQuery.mockImplementation(
      (): IDataImageMock => mockDataUseStaticQuery,
    );
  });

  afterEach((): void => {
    jest.restoreAllMocks();
  });
  it("should render container", (): void => {
    expect.hasAssertions();

    const { container } = render(<ComparativeWithOthers />);

    expect(container).toBeInTheDocument();
  });
});
