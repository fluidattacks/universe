import { fireEvent, render, screen } from "@testing-library/react";
import React from "react";

import { ComparativePlans } from "./Comparative";
import { PlansQuestion } from "./FaqSection/Question";
import { Header } from "./Header";
import { PlansCta } from "./PlansCta";

import { PlansPage } from ".";

describe("PlansPage", (): void => {
  it("renders PlansPage", (): void => {
    expect.hasAssertions();

    render(<PlansPage />);

    expect(
      screen.getByText(
        "plansPage.comparativePlans.characteristics.characteristic1",
      ),
    ).toBeInTheDocument();
    const table = screen.getAllByRole("table", { name: "" });
    expect(table).toHaveLength(2);
    expect(
      screen.getByRole("cell", {
        name: "plansPage.comparativePlans.characteristics.characteristic2",
      }),
    ).toBeInTheDocument();
    expect(
      screen.getByRole("row", {
        name: "plansPage.comparativePlans.essentialTitle plansPage.header.button.essential plansPage.comparativePlans.advancedTitle plansPage.header.button.advanced",
      }),
    ).toBeInTheDocument();
  });

  it("renders the CTA section", (): void => {
    expect.hasAssertions();

    render(<PlansCta />);

    expect(
      screen.getByRole("heading", { name: "blog.ctaTitle" }),
    ).toBeInTheDocument();
  });

  describe("Header component", (): void => {
    it("renders essential plan title", (): void => {
      expect.hasAssertions();

      render(<Header />);
      expect(screen.getByText("Essential plan")).toBeInTheDocument();
    });

    it("renders advanced plan title", (): void => {
      expect.hasAssertions();

      render(<Header />);
      expect(screen.getByText("Advanced plan")).toBeInTheDocument();
    });

    it("switches plans essential when Switch component is clicked", (): void => {
      expect.hasAssertions();

      // eslint-disable-next-line functional/immutable-data
      Object.defineProperties(window, {
        location: {
          value: { hash: "#essential" },
          writable: true,
        },
      });

      render(<Header />);

      expect(window.location.hash).toBe("#essential");
    });
    it("switches plans advanced when Switch component is clicked", (): void => {
      expect.hasAssertions();

      // eslint-disable-next-line functional/immutable-data
      Object.defineProperties(window, {
        location: {
          value: { hash: "#advanced" },
          writable: true,
        },
      });

      render(<Header />);

      expect(window.location.hash).toBe("#advanced");
    });
  });

  describe("handleSwitch", (): void => {
    it("updates window location hash when handleSwitch is called", (): void => {
      expect.hasAssertions();

      const replaceMock = jest.fn();
      // eslint-disable-next-line functional/immutable-data
      Object.defineProperty(window, "location", {
        value: { replace: replaceMock },
        writable: true,
      });

      render(<Header />);

      fireEvent.click(screen.getByText("Advanced plan"));

      const expectedHash = "#advanced";
      expect(replaceMock).toHaveBeenCalledWith(expectedHash);

      fireEvent.click(screen.getByText("Essential plan"));

      const expectedHash2 = "#essential";
      expect(replaceMock).toHaveBeenCalledWith(expectedHash2);
    });
  });
  describe("PlansQuestion", (): void => {
    it("renders PlansQuestion", (): void => {
      expect.hasAssertions();

      render(
        <PlansQuestion answer={"some answer"} question={"some question"} />,
      );

      expect(screen.getByText("some question")).toBeInTheDocument();
      expect(screen.getByText("some answer")).toBeInTheDocument();
    });

    it("should render with expected style", (): void => {
      expect.hasAssertions();

      const { getByText } = render(
        <PlansQuestion answer={"some answer"} question={"some question"} />,
      );

      expect(getByText("some question")).toBeInTheDocument();
      expect(getByText("some question")).toHaveStyle({ display: "block" });

      fireEvent.click(getByText("some answer"));
      expect(getByText("some answer")).toHaveStyle({ display: "block" });
    });
  });

  it("ComparativePlans", (): void => {
    expect.hasAssertions();

    render(<ComparativePlans />);

    expect(
      screen.getByText("plansPage.comparativePlans.subtitle"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("plansPage.comparativePlans.paragraph"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("plansPage.comparativePlans.title"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("plansPage.comparativePlans.essentialTitle"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("plansPage.comparativePlans.advancedTitle"),
    ).toBeInTheDocument();
  });
});
