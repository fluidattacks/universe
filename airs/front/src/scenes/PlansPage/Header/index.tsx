import { IoMdArrowDown } from "@react-icons/all-files/io/IoMdArrowDown";
import React, { useCallback, useEffect, useState } from "react";

import { IconBlock, KnowLink, MainCoverPlans, Span, Tag } from "./styles";
import { Switch } from "./Switch";

import { AirsLink } from "../../../components/AirsLink";
import { Button } from "../../../components/Button";
import { CloudImage } from "../../../components/CloudImage";
import { Container } from "../../../components/Container";
import { Text, Title } from "../../../components/Typography";
import { useWindowSize } from "../../../utils/hooks/useWindowSize";
import { translate } from "../../../utils/translations/translate";

type TPlan = "advanced" | "essential";

const Header: React.FC = (): JSX.Element => {
  const { width } = useWindowSize();

  const getPlanFromWindowLocation = (): TPlan => {
    if (typeof window !== "undefined") {
      const { hash } = window.location;
      if (hash === "#essential") {
        return "essential";
      }
    }

    return "advanced";
  };

  const [plan, setPlan] = useState<TPlan>(getPlanFromWindowLocation());

  useEffect((): (() => void) => {
    const handleHashChange = (): void => {
      setPlan(getPlanFromWindowLocation());
    };
    window.addEventListener("hashchange", handleHashChange);
    window.addEventListener("scroll", handleHashChange);

    return (): void => {
      window.removeEventListener("hashchange", handleHashChange);
      window.removeEventListener("scroll", handleHashChange);
    };
  }, []);

  const title: Record<TPlan, JSX.Element> = {
    advanced: (
      <Text color={"#25252d"} key={1} size={"medium"} weight={"semibold"}>
        {translate.t("plansPage.header.advanced.keyword1")}
        <Span $fColor={"#bf0b1a"}>
          {translate.t("plansPage.header.advanced.separator")}
        </Span>
        {translate.t("plansPage.header.advanced.keyword2")}
        <Span $fColor={"#bf0b1a"}>
          {translate.t("plansPage.header.advanced.separator")}
        </Span>
        {translate.t("plansPage.header.advanced.keyword3")}
      </Text>
    ),
    essential: (
      <Text color={"#121216"} key={0} size={"medium"} weight={"semibold"}>
        {translate.t("plansPage.header.essential.text")}
        &nbsp; &nbsp;
        <Tag>{translate.t("plansPage.header.essential.tag")}</Tag>
      </Text>
    ),
  };

  const plans = Object.keys(title) as TPlan[];

  const handleSwitch = useCallback(
    (selectedTab: string): void => {
      const selectedPlan = plans.filter((_plan): boolean =>
        selectedTab.toLocaleLowerCase().includes(_plan),
      );

      if (typeof window !== "undefined") {
        window.location.replace(`#${selectedPlan[0]}`);
      }
    },
    [plans],
  );

  const tabs: string[] = ["Essential plan", "Advanced plan"];

  return (
    <Container display={"flex"} height={"fit-content"} phSm={4} width={"100%"}>
      <MainCoverPlans>
        <Container display={"flex"} justify={"center"} mb={5}>
          <Container
            display={"flex"}
            height={"fit-content"}
            justify={"center"}
            maxWidth={"1200px"}
            wrap={"wrap"}
          >
            <Title
              color={"#2e2e38"}
              level={3}
              mt={5}
              size={"big"}
              textAlign={"center"}
            >
              {translate.t("plansPage.portrait.title")}
            </Title>
            <Container display={"flex"} justify={"center"} mt={4} wrap={"wrap"}>
              <Switch
                currentSelection={plan}
                handleSwitch={handleSwitch}
                options={tabs}
              />
            </Container>
          </Container>
        </Container>
        <Container
          display={"flex"}
          justify={"end"}
          width={"100%"}
          wrap={"wrap"}
        >
          <Container display={"flex"} maxWidth={"1600px"} wrap={"wrap"}>
            <Container
              display={"flex"}
              justify={"end"}
              pl={4}
              width={"50%"}
              widthMd={"100%"}
            >
              <Container>
                {title[plan]}
                <Container maxWidth={"812px"} mt={3}>
                  <Title color={"#2e2e38"} level={3} mb={4} size={"medium"}>
                    {translate.t(`plansPage.header.${plan}.title`)}
                  </Title>
                </Container>
                {[...Array(4).keys()].map(
                  (el: number): JSX.Element =>
                    el === 0 ? (
                      <div key={el} />
                    ) : (
                      <Container
                        display={"flex"}
                        key={el}
                        mb={4}
                        wrap={width < 960 ? "wrap" : "nowrap"}
                      >
                        <Container width={"fit-content"} widthSm={"100%"}>
                          <IconBlock key={`icon${el}`}>
                            <CloudImage
                              alt={"plans-icon"}
                              key={`icon-image${el}`}
                              src={`airs/plans/icon-${plan}-${el}`}
                            />
                          </IconBlock>
                        </Container>
                        <Container key={`container-${el}`}>
                          <Title
                            color={"#2e2e38"}
                            display={"block"}
                            key={`title${el}`}
                            level={3}
                            size={"xs"}
                          >
                            {translate.t(
                              `plansPage.header.${plan}.titleBenefit${el}`,
                            )}
                          </Title>
                          <Text
                            color={"#535365"}
                            display={"block"}
                            key={`text-${el}`}
                          >
                            {translate.t(
                              `plansPage.header.${plan}.textBenefit${el}`,
                            )}
                          </Text>
                        </Container>
                      </Container>
                    ),
                )}
                <Container
                  align={"center"}
                  display={"flex"}
                  justify={"start"}
                  justifySm={"center"}
                  mb={4}
                  wrap={"wrap"}
                >
                  <AirsLink
                    href={
                      plan === "essential"
                        ? "https://app.fluidattacks.com/SignUp"
                        : "/contact-us/"
                    }
                  >
                    <Button size={"md"} variant={"primary"}>
                      {plan === "essential"
                        ? translate.t("plansPage.header.button.essential")
                        : translate.t("plansPage.header.button.advanced")}
                    </Button>
                  </AirsLink>
                  <KnowLink href={"#comparative-plans-table"}>
                    {translate.t("plansPage.header.link")}
                    <IoMdArrowDown />
                  </KnowLink>
                </Container>
              </Container>
            </Container>
            <Container
              display={"flex"}
              justify={"end"}
              justifyMd={"center"}
              width={"50%"}
              widthMd={"100%"}
            >
              <CloudImage
                alt={"plans-header-image"}
                src={`airs/plans/${plan}-header-1`}
                styles={"plans-header-img"}
              />
            </Container>
          </Container>
        </Container>
      </MainCoverPlans>
    </Container>
  );
};

export { Header };
