interface ISwitchHandlers {
  handleSwitch: (selectedState: string) => void;
}
interface ISwitchProps extends ISwitchHandlers {
  currentSelection: string;
  options: readonly string[];
}

export type { ISwitchProps };
