import React from "react";

import { ClientsSection } from "./ClientsSection";
import { ContinuousCycle } from "./ContinuousCycle";
import { DiscoverContinuous } from "./DiscoverContinuous";
import { HeaderHero } from "./HeaderHero";
import { HomeCta } from "./HomeCta";
import { Reviews } from "./ReviewsSection";
import { SolutionsSection } from "./SolutionsSection";

const HomePage: React.FC = (): JSX.Element => (
  <React.Fragment>
    <HeaderHero />
    <ClientsSection />
    <DiscoverContinuous />
    <ContinuousCycle />
    <SolutionsSection />
    <Reviews />
    <HomeCta />
  </React.Fragment>
);

export { HomePage };
