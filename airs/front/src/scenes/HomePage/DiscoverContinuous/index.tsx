import React, { useCallback, useState } from "react";
import LiteYouTubeEmbed from "react-lite-youtube-embed";

import { HomeVideoContainer } from "./styles";

import { AirsLink } from "../../../components/AirsLink";
import { Button } from "../../../components/Button";
import { Container } from "../../../components/Container";
import { Grid } from "../../../components/Grid";
import { SimpleCard } from "../../../components/SimpleCard";
import { Title } from "../../../components/Typography";
import { translate } from "../../../utils/translations/translate";

const DiscoverContinuous: React.FC = (): JSX.Element => {
  const [video, setVideo] = useState(true);

  const videoId = "k46Rq45T144";

  const playVideo = useCallback((): void => {
    setVideo((previousState): boolean => !previousState);
  }, []);

  return (
    <Container bgColor={"#ffffff"}>
      <Container
        align={"center"}
        bgColor={"#ffffff"}
        center={true}
        display={"flex"}
        justify={"center"}
        maxWidth={"1440px"}
        pv={5}
        wrap={"wrap"}
      >
        <Title
          color={"#bf0b1a"}
          level={3}
          mb={4}
          size={"small"}
          textAlign={"center"}
        >
          {translate.t("home.discoverContinuous.subtitle")}
        </Title>
        <Container maxWidth={"951px"} ph={4}>
          <Title
            color={"#2e2e38"}
            level={1}
            mb={4}
            size={"medium"}
            textAlign={"center"}
          >
            {translate.t("home.discoverContinuous.title")}
          </Title>
        </Container>
        <HomeVideoContainer
          $button={video}
          $isVisible={true}
          onClick={playVideo}
        >
          <LiteYouTubeEmbed
            aspectHeight={9}
            aspectWidth={16}
            id={videoId}
            playlist={false}
            thumbnail={
              "https://res.cloudinary.com/fluid-attacks/image/upload/v1682024721/airs/home/DiscoverContinuous/video-image.png"
            }
            title={"home-video"}
          />
        </HomeVideoContainer>
        <Container center={true} maxWidth={"1250px"} ph={4}>
          <Grid columns={3} columnsMd={3} columnsSm={1} gap={"1rem"}>
            <SimpleCard
              bgColor={"#f4f4f6"}
              bgGradient={"#ffffff, #f4f4f6"}
              description={translate.t(
                "home.discoverContinuous.card1.subtitle",
              )}
              descriptionColor={"#535365"}
              image={"airs/home/DiscoverContinuous/card1"}
              title={translate.t("home.discoverContinuous.card1.title")}
              titleColor={"#2e2e38"}
            />
            <SimpleCard
              bgColor={"#f4f4f6"}
              bgGradient={"#ffffff, #f4f4f6"}
              description={translate.t(
                "home.discoverContinuous.card2.subtitle",
              )}
              descriptionColor={"#535365"}
              image={"airs/home/DiscoverContinuous/card2"}
              title={translate.t("home.discoverContinuous.card2.title")}
              titleColor={"#2e2e38"}
            />
            <SimpleCard
              bgColor={"#f4f4f6"}
              bgGradient={"#ffffff, #f4f4f6"}
              description={translate.t(
                "home.discoverContinuous.card3.subtitle",
              )}
              descriptionColor={"#535365"}
              image={"airs/home/DiscoverContinuous/card3"}
              title={translate.t("home.discoverContinuous.card3.title")}
              titleColor={"#2e2e38"}
            />
          </Grid>
        </Container>
        <Container display={"flex"} justify={"center"} maxWidth={"900px"}>
          <AirsLink href={"/services/continuous-hacking/"}>
            <Button size={"md"} variant={"primary"}>
              {translate.t("home.discoverContinuous.button")}
            </Button>
          </AirsLink>
        </Container>
      </Container>
    </Container>
  );
};

export { DiscoverContinuous };
