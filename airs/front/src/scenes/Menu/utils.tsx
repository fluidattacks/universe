import React from "react";

import { CompanyMenu } from "./Categories/Company";
import { PlatformMenu } from "./Categories/Platform";
import { ResourcesMenu } from "./Categories/Resources";
import { ServiceMenu } from "./Categories/Service";

import { Search } from "../../components/AlgoliaSearch";
import type { TDirection } from "../../components/Chevron";
import type { ISize } from "components/Button/types";

const searchIndices = [
  {
    description: `fluidattacks_airs`,
    keywords: `fluidattacks_airs`,
    name: `fluidattacks_airs`,
    title: `fluidattacks_airs`,
  },
];

const customButtonSize = (fontSize: number, ph: number, pv: number): ISize => {
  return {
    fontSize,
    ph,
    pv,
  };
};

const initialChevronStates: Record<string, TDirection> = {
  company: "down",
  platform: "down",
  resources: "down",
  services: "down",
};

const contents = (closeSearchBar?: () => void): JSX.Element[] => {
  const search = closeSearchBar ? (
    <Search
      closeSearchBar={closeSearchBar}
      indices={searchIndices}
      key={"search"}
    />
  ) : (
    <div />
  );

  return [
    <div key={"close"} />,
    <ServiceMenu display={"block"} key={"services"} />,
    <PlatformMenu display={"block"} key={"platform"} />,
    <ResourcesMenu display={"block"} key={"resources"} />,
    <CompanyMenu display={"block"} key={"company"} />,
    search,
  ];
};

export { initialChevronStates, searchIndices, contents, customButtonSize };
