import { render, screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import "@testing-library/jest-dom";
import React from "react";

import { CompanyMenu } from "./Categories/Company";

import { NavbarComponent } from ".";

describe("NavbarComponent", (): void => {
  it("displays language buttons with correct text", async (): Promise<void> => {
    expect.hasAssertions();

    render(<NavbarComponent />);

    const languageButtonsTexts = ["English", "Español"];

    await waitFor((): void => {
      languageButtonsTexts.forEach((text): void => {
        const languageButtons = screen.queryAllByText(text);

        expect(languageButtons[0]).toBeInTheDocument();
      });
    });
  });

  it("displays section information after click", async (): Promise<void> => {
    expect.hasAssertions();

    render(<NavbarComponent />);

    await userEvent.click(screen.getAllByText("menu.buttons.service")[0]);

    expect(
      screen.getByText("menu.services.allInOne.title"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("menu.services.allInOne.continuous.title"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("menu.services.allInOne.continuous.subtitle"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("menu.services.solutions.title"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("menu.services.solutions.applicationSec.title"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("menu.services.solutions.applicationSec.subtitle"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("menu.services.solutions.compliance.title"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("menu.services.solutions.compliance.subtitle"),
    ).toBeInTheDocument();
  });

  describe("CompanyMenu", (): void => {
    it("renders correctly data", (): void => {
      expect.hasAssertions();

      render(<CompanyMenu display={"block"} />);

      expect(
        screen.getByText("menu.company.fluid.about.title"),
      ).toBeInTheDocument();
      expect(screen.getByText("menu.company.fluid.title")).toBeInTheDocument();
      expect(
        screen.getByText("menu.company.fluid.about.subtitle"),
      ).toBeInTheDocument();
      expect(
        screen.getByText("menu.company.fluid.certifications.subtitle"),
      ).toBeInTheDocument();
      expect(
        screen.getByText("menu.company.fluid.certifications.title"),
      ).toBeInTheDocument();
    });
    it("renders links", (): void => {
      expect.hasAssertions();

      render(<CompanyMenu display={"block"} />);

      expect(
        screen.getByRole("link", { name: "menu.company.fluid.about.title" }),
      ).toHaveAttribute("href", "/about-us/");
      expect(
        screen.getByRole("link", {
          name: "menu.company.fluid.certifications.title",
        }),
      ).toHaveAttribute("href", "/certifications/");
      expect(
        screen.getByRole("link", { name: "menu.company.fluid.partners.title" }),
      ).toHaveAttribute("href", "/partners/");
      expect(
        screen.getByRole("link", { name: "menu.company.fluid.careers.title" }),
      ).toHaveAttribute("href", "/careers/");
    });
  });
});
