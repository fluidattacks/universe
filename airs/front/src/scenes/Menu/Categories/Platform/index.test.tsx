import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import React from "react";

import { PlatformMenu } from ".";

describe("PlatformMenu", (): void => {
  it("renders correctly data", (): void => {
    expect.hasAssertions();

    render(<PlatformMenu display={"block"} />);

    expect(
      screen.getByText("menu.platform.aSinglePane.title"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("menu.platform.aSinglePane.platformOverview.subtitle"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("menu.platform.products.links.aspm"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("menu.platform.products.links.re"),
    ).toBeInTheDocument();
  });
});
it("renders links", (): void => {
  expect.hasAssertions();

  render(<PlatformMenu display={"block"} />);

  expect(
    screen.getByRole("link", {
      name: "menu.platform.aSinglePane.platformOverview.title",
    }),
  ).toHaveAttribute("href", "/platform/");
  expect(
    screen.getByRole("link", { name: "menu.platform.products.links.re" }),
  ).toHaveAttribute("href", "/product/re/");
  expect(
    screen.getByRole("link", { name: "menu.platform.products.links.scr" }),
  ).toHaveAttribute("href", "/solutions/secure-code-review/");
});
