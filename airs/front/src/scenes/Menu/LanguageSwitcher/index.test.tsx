import { render, screen } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import "@testing-library/jest-dom";
import { navigate } from "gatsby";
import i18next from "i18next";
import React from "react";

import { LanguageSwitcher } from ".";

describe("LanguageSwitcher", (): void => {
  it("renders correctly data", (): void => {
    expect.hasAssertions();

    const { container, getByText } = render(<LanguageSwitcher />);

    expect(container).toBeInTheDocument();
    expect(getByText("Español")).toBeInTheDocument();
  });
  it("LanguageSwitcher allow switch language", async (): Promise<void> => {
    expect.hasAssertions();

    render(<LanguageSwitcher />);

    await userEvent.click(screen.getByText("Español"));
    expect(navigate).toHaveBeenCalledTimes(1);
    expect(navigate).toHaveBeenCalledWith("/es/", { replace: true });
  });
  it("LanguageSwitcher renders in spanish", (): void => {
    expect.hasAssertions();

    jest.replaceProperty(i18next, "language", "es");

    render(<LanguageSwitcher />);

    expect(screen.getByText("English")).toBeInTheDocument();
  });
});
