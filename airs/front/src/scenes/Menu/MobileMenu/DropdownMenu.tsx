/* eslint react/forbid-component-props: 0 */
import { BsChevronLeft } from "@react-icons/all-files/bs/BsChevronLeft";
import { BsChevronRight } from "@react-icons/all-files/bs/BsChevronRight";
import { IoMdClose } from "@react-icons/all-files/io/IoMdClose";
import { Link } from "gatsby";
import React, { useCallback, useEffect, useState } from "react";

import { AirsLink } from "../../../components/AirsLink";
import { Search } from "../../../components/AlgoliaSearch";
import { Button } from "../../../components/Button";
import { CloudImage } from "../../../components/CloudImage";
import { Container } from "../../../components/Container";
import type { TDisplay } from "../../../components/Container/types";
import { Text } from "../../../components/Typography";
import { NavbarInnerContainer, NavbarList } from "../../../styles/styles";
import { useWindowSize } from "../../../utils/hooks/useWindowSize";
import { translate } from "../../../utils/translations/translate";
import {
  ContainerWithSlide,
  MenuFootContainer,
  MenuMobileInnerContainer,
  SlideMenu,
} from "../styles";
import { contents, customButtonSize, searchIndices } from "../utils";
import type { ISize } from "components/Button/types";

interface IServiceProps {
  display: TDisplay;
  handleClick: () => void;
  status: number;
  setStatus: React.Dispatch<React.SetStateAction<number>>;
}

const DropdownMenu: React.FC<IServiceProps> = ({
  display,
  handleClick,
  status,
  setStatus,
}): JSX.Element => {
  const { width } = useWindowSize();
  const [categoryShowed, setCategoryShowed] = useState(0);
  const listContents: JSX.Element[] = contents();

  const categories: string[] = [
    "none",
    "Service",
    "Platform",
    "Resources",
    "Company",
  ];
  const customOptionsButtonSize: ISize = customButtonSize(16, 32, 10);

  const resetState = useCallback((): void => {
    setCategoryShowed(0);
  }, []);

  const handleClickButton = useCallback(
    (el: number): (() => void) =>
      (): void => {
        resetState();
        setStatus(2);
        setCategoryShowed(el);
      },
    [resetState, setStatus],
  );

  const handleGoBack = useCallback((): void => {
    resetState();
    setStatus(1);
  }, [resetState, setStatus]);

  useEffect((): void => {
    if (status === 0) {
      resetState();
    }
  }, [resetState, status]);

  return (
    <MenuMobileInnerContainer
      className={width < 960 ? "sidenav fixed w-100 overflow-y-auto" : ""}
      style={{ display: width < 1200 ? display : "none" }}
    >
      <NavbarInnerContainer
        id={"inner_navbar"}
        style={{ display: width < 960 ? display : "none" }}
      >
        <NavbarList className={"roboto flex w-100"} id={"navbar_list"}>
          <div className={"w-auto flex flex-nowrap"}>
            <li>
              <Link className={"db tc pa1 no-underline"} to={"/"}>
                <Container ph={3} pv={2} width={"150px"}>
                  <CloudImage
                    alt={"Fluid Attacks logo navbar"}
                    src={"airs/menu/Logo"}
                  />
                </Container>
              </Link>
            </li>
          </div>
          <div className={"w-75"} />
          <Container
            display={width < 960 ? "flex" : "none"}
            justifyMd={"end"}
            justifySm={"end"}
            maxWidth={width > 768 ? "50px" : "90%"}
          >
            <Button onClick={handleClick} variant={"ghost"}>
              <IoMdClose size={25} />
            </Button>
          </Container>
        </NavbarList>
      </NavbarInnerContainer>

      <Container
        bgColor={"#ffffff"}
        display={width < 1200 ? display : "none"}
        position={"absolute"}
        width={"100%"}
      >
        <SlideMenu
          $isShown={categoryShowed === 0}
          $mobile={width < 960}
          $status={status}
        >
          <Container
            display={width < 960 ? display : "none"}
            width={"auto"}
            widthSm={"100%"}
          >
            <Search indices={searchIndices} />
          </Container>
          <Container width={"auto"} widthSm={"100%"}>
            <Button
              customSize={customOptionsButtonSize}
              display={"block"}
              onClick={handleClickButton(1)}
              variant={"ghost"}
            >
              <Container display={"flex"} width={"100%"}>
                <Container width={"98%"}>
                  <Text
                    color={"#2e2e38"}
                    display={"inline"}
                    textAlign={"start"}
                  >
                    {translate.t("menu.buttons.service")}
                  </Text>
                </Container>
                <Container
                  align={"center"}
                  display={"flex"}
                  minWidth={"25px"}
                  width={"2%"}
                >
                  <BsChevronRight color={"#2e2e38"} size={16} />
                </Container>
              </Container>
            </Button>
          </Container>
          <Container width={"auto"} widthSm={"100%"}>
            <Button
              customSize={customOptionsButtonSize}
              display={"block"}
              onClick={handleClickButton(2)}
              variant={"ghost"}
            >
              <Container display={"flex"} width={"100%"}>
                <Container width={"98%"}>
                  <Text
                    color={"#2e2e38"}
                    display={"inline"}
                    textAlign={"start"}
                  >
                    {translate.t("menu.buttons.platform")}
                  </Text>
                </Container>
                <Container
                  align={"center"}
                  display={"flex"}
                  minWidth={"25px"}
                  width={"2%"}
                >
                  <BsChevronRight color={"#2e2e38"} size={16} />
                </Container>
              </Container>
            </Button>
          </Container>
          <AirsLink href={"/plans/"}>
            <Container width={"auto"} widthSm={"100%"}>
              <Button
                customSize={customOptionsButtonSize}
                display={"block"}
                onClick={handleClick}
                variant={"ghost"}
              >
                <Container display={"flex"} width={"100%"}>
                  <Container width={"98%"}>
                    <Text
                      color={"#2e2e38"}
                      display={"inline"}
                      textAlign={"start"}
                    >
                      {translate.t("menu.buttons.plans")}
                    </Text>
                  </Container>
                </Container>
              </Button>
            </Container>
          </AirsLink>

          <Container width={"auto"} widthSm={"100%"}>
            <Button
              customSize={customOptionsButtonSize}
              display={"block"}
              onClick={handleClickButton(3)}
              variant={"ghost"}
            >
              <Container display={"flex"} width={"100%"}>
                <Container width={"98%"}>
                  <Text
                    color={"#2e2e38"}
                    display={"inline"}
                    textAlign={"start"}
                  >
                    {translate.t("menu.buttons.resources")}
                  </Text>
                </Container>
                <Container
                  align={"center"}
                  display={"flex"}
                  minWidth={"25px"}
                  width={"2%"}
                >
                  <BsChevronRight color={"#2e2e38"} size={16} />
                </Container>
              </Container>
            </Button>
          </Container>
          <AirsLink href={"/advisories/"}>
            <Container width={"auto"} widthSm={"100%"}>
              <Button
                customSize={customOptionsButtonSize}
                display={"block"}
                onClick={handleClick}
                variant={"ghost"}
              >
                <Container display={"flex"} width={"100%"}>
                  <Container width={"98%"}>
                    <Text
                      color={"#2e2e38"}
                      display={"inline"}
                      textAlign={"start"}
                    >
                      {translate.t("menu.buttons.advisories")}
                    </Text>
                  </Container>
                </Container>
              </Button>
            </Container>
          </AirsLink>

          <Container width={"auto"} widthSm={"100%"}>
            <Button
              customSize={customOptionsButtonSize}
              display={"block"}
              onClick={handleClickButton(4)}
              variant={"ghost"}
            >
              <Container display={"flex"} width={"100%"}>
                <Container width={"98%"}>
                  <Text
                    color={"#2e2e38"}
                    display={"inline"}
                    textAlign={"start"}
                  >
                    {translate.t("menu.buttons.company")}
                  </Text>
                </Container>
                <Container
                  align={"center"}
                  display={"flex"}
                  minWidth={"25px"}
                  width={"2%"}
                >
                  <BsChevronRight color={"#2e2e38"} size={16} />
                </Container>
              </Container>
            </Button>
          </Container>
        </SlideMenu>
        <MenuFootContainer $isShown={width < 960} id={"menufoot"}>
          <Container
            align={"center"}
            bgColor={"#aaaaa"}
            display={"flex"}
            justify={"center"}
            width={"100%"}
          >
            <Container align={"center"} display={"flex"} justify={"center"}>
              <Container
                align={"center"}
                display={"block"}
                justify={"center"}
                width={"90%"}
                wrap={"nowrap"}
              >
                <AirsLink href={"/contact-us/"}>
                  <Button display={"block"} variant={"tertiary"}>
                    {translate.t("menu.buttons.contact")}
                  </Button>
                </AirsLink>
                <br />
                <AirsLink
                  decoration={"none"}
                  href={"https://app.fluidattacks.com/SignUp"}
                >
                  <Button display={"block"} variant={"primary"}>
                    {translate.t("menu.buttons.trial")}
                  </Button>
                </AirsLink>
              </Container>
            </Container>
          </Container>
        </MenuFootContainer>
        <ContainerWithSlide
          $isShown={categoryShowed !== 0}
          $mobile={width < 960}
        >
          <Container borderBottomColor={"#2e2e38"} mb={3}>
            <Button
              customSize={customOptionsButtonSize}
              icon={
                <BsChevronLeft
                  color={"#2e2e38"}
                  size={16}
                  style={{ paddingRight: "10px" }}
                />
              }
              iconSide={"left"}
              onClick={handleGoBack}
              variant={"ghost"}
            >
              {categories[categoryShowed]}
            </Button>
          </Container>
          {listContents[categoryShowed]}
        </ContainerWithSlide>
      </Container>
    </MenuMobileInnerContainer>
  );
};

export { DropdownMenu };
