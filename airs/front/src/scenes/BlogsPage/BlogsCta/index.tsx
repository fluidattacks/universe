import React from "react";

import { Container } from "../../../components/Container";
import { CtaBanner } from "../../../components/CtaBanner";
import { translate } from "../../../utils/translations/translate";

const BlogsCta: React.FC = (): JSX.Element => {
  return (
    <Container bgColor={"#dddde3"} ph={4} pv={5}>
      <CtaBanner
        button1Link={"https://app.fluidattacks.com/SignUp"}
        button1Text={"Try for free"}
        button2Link={"/services/continuous-hacking/"}
        button2Text={"Learn more"}
        image={"/airs/blogs/cta-1"}
        paragraph={translate.t("blog.ctaDescription")}
        ph={0}
        pv={3}
        size={"small"}
        sizeSm={"medium"}
        textSize={"medium"}
        title={translate.t("blog.ctaTitle")}
      />
    </Container>
  );
};

export { BlogsCta };
