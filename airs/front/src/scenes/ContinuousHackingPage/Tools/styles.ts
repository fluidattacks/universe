import { styled } from "styled-components";

import type { IIconProps } from "./types";

const IconBlock = styled.div<IIconProps>`
  width: ${({ $width }): string => ($width === undefined ? "45px" : $width)};
  height: ${({ $height }): string =>
    $height === undefined ? "33px" : $height};
  display: flex;
  justify-content: center;
  align-items: center;
  margin-right: 15px;
  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);
  border: solid 1px #f4f4f6;
  border-radius: 7px;
`;

export { IconBlock };
