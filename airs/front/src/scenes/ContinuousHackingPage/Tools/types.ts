interface IIconProps {
  $width?: string;
  $height?: string;
}

export type { IIconProps };
