import React from "react";

import { ContinuousHackingClientsDesktop } from "./Desktop";
import { ContinuousHackingClientsMobile } from "./Mobile";

import { useWindowSize } from "../../../utils/hooks/useWindowSize";

const ContinuousHackingClients: React.FC = (): JSX.Element => {
  const { width } = useWindowSize();

  if (width >= 1100) {
    return <ContinuousHackingClientsDesktop />;
  }

  return <ContinuousHackingClientsMobile />;
};

export { ContinuousHackingClients };
