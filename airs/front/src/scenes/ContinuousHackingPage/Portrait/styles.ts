import { styled } from "styled-components";

import { SimpleCard } from "../../../components/SimpleCard";

const Card = styled(SimpleCard)`
  height: 270px;
  padding-left: 16px !important;
  padding-right: 16px !important;
  padding-bottom: 24px !important ;

  > img {
    width: 40px;
    height: 40px;
  }
`;

export { Card };
