import React, { useCallback } from "react";

import { useWindowSize } from "../../../utils/hooks/useWindowSize";
import { DesktopFooter } from "../DesktopFooter";
import { MediumFooter } from "../MediumFooter";
import { MobileFooter } from "../MobileFooter";

const Layout = ({ children }: { children: JSX.Element }): JSX.Element => {
  const { width } = useWindowSize();

  const getFooterSize = useCallback((): JSX.Element => {
    if (width < 480) {
      return <MobileFooter />;
    } else if (width < 961) {
      return <MediumFooter />;
    }

    return <DesktopFooter />;
  }, [width]);

  return (
    <React.StrictMode>
      <div className={"bg-lightgray lh-copy ma0"} style={{ height: "auto" }}>
        <main>{children}</main>
        {getFooterSize()}
      </div>
    </React.StrictMode>
  );
};

export { Layout };
