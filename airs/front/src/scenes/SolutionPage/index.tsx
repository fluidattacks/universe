import React from "react";

import { HeaderSection } from "./HeaderSection";
import { MainSection } from "./MainSection";

interface ISolutionPageProps {
  description: string;
  html: string;
  image: string;
  title: string;
}

const SolutionPage: React.FC<ISolutionPageProps> = ({
  description,
  html,
  image,
  title,
}): JSX.Element => {
  return (
    <React.Fragment>
      <HeaderSection description={description} image={image} title={title} />
      <MainSection html={html} />
    </React.Fragment>
  );
};

export { SolutionPage };
