import { graphql, useStaticQuery } from "gatsby";
import i18next from "i18next";
import React, { useCallback } from "react";

import { CardSlideShow } from "../../../../components/CardSlideShow";
import { translatedPages } from "../../../../utils/translations/spanishPages";
import { translate } from "../../../../utils/translations/translate";

interface ISolutionSlideShowProps {
  description: string;
  solution: string;
  title: string;
}

const SolutionSlideShow = ({
  description,
  solution,
  title,
}: Readonly<ISolutionSlideShowProps>): JSX.Element => {
  const data: IData = useStaticQuery(graphql`
    query SolutionBlogList {
      allMarkdownRemark(
        filter: {
          fields: { slug: { regex: "/blog/" } }
          frontmatter: { image: { regex: "" } }
        }
        sort: { frontmatter: { date: DESC } }
      ) {
        edges {
          node {
            fields {
              slug
            }
            frontmatter {
              alt
              slug
              tags
              description
              image
              title
              subtitle
            }
          }
        }
      }
    }
  `);

  const englishSortList: {
    [key: string]: string[];
    devsecops: string[];
    ethicalHacking: string[];
  } = {
    appSecurityPostureManagement: [
      "/blog/cvssf-risk-exposure-metric/",
      "/blog/what-is-vulnerability-management/",
      "/blog/choose-vulnerability-management/",
      "/blog/vulnerability-assessment/",
    ],
    devsecops: [
      "/blog/why-is-cloud-devsecops-important/",
      "/blog/what-does-a-devsecops-engineer-do/",
      "/blog/azure-devsecops-with-fluid-attacks/",
      "/blog/aws-devsecops-with-fluid-attacks/",
      "/blog/devsecops-best-practices/",
      "/blog/devsecops-tools/",
      "/blog/how-to-implement-devsecops/",
      "/blog/devsecops-concept/",
    ],
    ethicalHacking: [
      "/blog/tribe-of-hackers-5/",
      "/blog/tribe-of-hackers-4/",
      "/blog/tribe-of-hackers-3/",
      "/blog/tribe-of-hackers-2/",
      "/blog/tribe-of-hackers-1/",
      "/blog/delimit-ethical-hacking/",
      "/blog/hacking-ethics/",
      "/blog/thinking-like-hacker/",
      "/blog/what-is-ethical-hacking/",
    ],
    penetrationTestingAsAService: [
      "/blog/continuous-penetration-testing/",
      "/blog/what-is-ptaas/",
      "/blog/penetration-testing-compliance/",
      "/blog/bas-vs-pentesting-vs-red-teaming/",
      "/blog/importance-pentesting/",
      "/blog/choosing-pentesting-team/",
      "/blog/types-of-penetration-testing/",
      "/blog/what-is-manual-penetration-testing/",
      "/blog/penetration-testing/",
    ],
    redTeaming: [
      "/blog/bas-vs-pentesting-vs-red-teaming/",
      "/blog/tiber-eu-providers/",
      "/blog/tiber-eu-framework/",
      "/blog/attacking-without-announcing/",
      "/blog/red-team-exercise/",
      "/blog/why-apply-red-teaming/",
    ],
    secureCodeReview: [
      "/blog/differences-between-sast-sca-dast/",
      "/blog/sastisfying-app-security/",
      "/blog/secure-coding-five-steps/",
      "/blog/secure-coding-practices/",
      "/blog/code-quality-and-security/",
      "/blog/manual-code-review/",
      "/blog/secure-code-review/",
    ],
    securityTesting: [
      "/blog/what-is-mast/",
      "/blog/what-is-ptaas/",
      "/blog/reverse-engineering/",
      "/blog/stand-shoulders-giants/",
      "/blog/sca-scans/",
      "/blog/sastisfying-app-security/",
      "/blog/casa-approved-static-scanning/",
      "/blog/differences-between-sast-sca-dast/",
      "/blog/security-testing-fundamentals/",
    ],
    vulnerabilityManagement: [
      "/blog/cvssf-risk-exposure-metric/",
      "/blog/attack-resistance-management-psirts/",
      "/blog/web-app-vulnerability-scanning/",
      "/blog/vulnerability-scan/",
      "/blog/vulnerability-assessment/",
      "/blog/from-asm-to-arm/",
      "/blog/what-is-vulnerability-management/",
      "/blog/choose-vulnerability-management/",
    ],
  };

  const translateSortList = useCallback(
    (
      sortList: {
        [key: string]: string[];
        devsecops: string[];
        ethicalHacking: string[];
      },
      section: string,
    ): string[] => {
      return sortList[section].map((url): string => {
        const spanishTranslation = translatedPages.find(
          (page): boolean => page.en === url,
        )?.es;

        return spanishTranslation === undefined ? url : spanishTranslation;
      });
    },
    [],
  );

  const hybridSortList: {
    [key: string]: string[];
    devsecops: string[];
    ethicalHacking: string[];
  } = {
    appSecurityPostureManagement: translateSortList(
      englishSortList,
      "appSecurityPostureManagement",
    ),
    devsecops: translateSortList(englishSortList, "devsecops"),
    ethicalHacking: translateSortList(englishSortList, "ethicalHacking"),
    penetrationTestingAsAService: translateSortList(
      englishSortList,
      "penetrationTestingAsAService",
    ),
    redTeaming: translateSortList(englishSortList, "redTeaming"),
    secureCodeReview: translateSortList(englishSortList, "secureCodeReview"),
    securityTesting: translateSortList(englishSortList, "securityTesting"),
    vulnerabilityManagement: translateSortList(
      englishSortList,
      "vulnerabilityManagement",
    ),
  };

  const currentLanguage = i18next.language;

  const sortList: {
    [key: string]: string[];
    devsecops: string[];
    ethicalHacking: string[];
  } = currentLanguage === "en" ? englishSortList : hybridSortList;

  const blogs: INodes[] = data.allMarkdownRemark.edges;

  const toFilter: string[] = sortList[solution];

  const filterBlogs: INodes[] = blogs.filter((post): boolean => {
    return toFilter.some((slug): boolean => {
      return slug === post.node.fields.slug;
    });
  });

  const sortedBlogs = [...filterBlogs].sort((first, second): number => {
    const firstSlug: string = first.node.fields.slug;
    const secondSlug: string = second.node.fields.slug;
    const firstSlugIndex: number = toFilter.indexOf(first.node.fields.slug);
    const secondSlugIndex: number = toFilter.indexOf(second.node.fields.slug);
    const isFirstSlugInSpanish: boolean = firstSlug.includes("/es/blog/");
    const isSecondSlugInSpanish: boolean = secondSlug.includes("/es/blog/");

    if (
      (isFirstSlugInSpanish && isSecondSlugInSpanish) ||
      (!isFirstSlugInSpanish && !isSecondSlugInSpanish)
    )
      return secondSlugIndex - firstSlugIndex;

    if (isFirstSlugInSpanish) return -1;

    return 1;
  });

  return (
    <CardSlideShow
      btnText={translate.t("solution.readPost")}
      containerDescription={description}
      containerTitle={title}
      data={sortedBlogs}
      variant={"dark"}
    />
  );
};

export { SolutionSlideShow };
