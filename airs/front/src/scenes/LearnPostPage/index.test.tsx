import "@testing-library/jest-dom";
import { render, screen, waitFor } from "@testing-library/react";
import React from "react";

import { BlogLink } from "./ContentSection/components/BlogLink";
import { Caution } from "./ContentSection/components/Caution";

import { PostPage } from ".";
import { BlogCta } from "../../components/BlogCta";

describe("LearnPostPage", (): void => {
  it("should render correctly", async (): Promise<void> => {
    expect.hasAssertions();

    const { getByText } = render(
      <PostPage
        author={"Fluid"}
        category={"category"}
        date={"2023-01-01"}
        description={"description"}
        html={"<p>html content</p>"}
        image={"image.jpg"}
        slug={"test-post"}
        subtitle={"subtitle"}
        title={"title"}
        writer={"Jane Doe"}
      />,
    );
    await waitFor((): void => {
      expect(getByText("html content")).toBeInTheDocument();
    });
    expect(getByText("Category")).toBeInTheDocument();
    expect(getByText("Fluid")).toBeInTheDocument();
    expect(getByText("description")).toBeInTheDocument();
    expect(getByText("2023-01-01")).toBeInTheDocument();
    expect(getByText("subtitle")).toBeInTheDocument();
    expect(getByText("title")).toBeInTheDocument();
  });

  it("should render correctly without description", async (): Promise<void> => {
    expect.hasAssertions();
    const { getByText } = render(
      <PostPage
        author={"Fluid"}
        category={"category"}
        date={"2023-01-01"}
        description={""}
        html={"<p>html content</p>"}
        image={"image.jpg"}
        slug={"test-post"}
        subtitle={"subtitle"}
        title={"title"}
        writer={"Jane Doe"}
      />,
    );
    await waitFor((): void => {
      expect(getByText("html content")).toBeInTheDocument();
    });
    expect(getByText("blog.share.title")).toBeInTheDocument();
    expect(getByText("Category")).toBeInTheDocument();
    expect(getByText("2023-01-01")).toBeInTheDocument();
    expect(getByText("subtitle")).toBeInTheDocument();
    expect(getByText("title")).toBeInTheDocument();
  });

  describe("BlogCta", (): void => {
    it("renders without crashing", (): void => {
      expect.hasAssertions();

      render(
        <BlogCta
          buttontxt={"Button Text"}
          link={"https://example.com"}
          paragraph={"Paragraph"}
          title={"título"}
        />,
      );

      expect(screen.getByText("Button Text")).toBeInTheDocument();
      expect(screen.getByText("título")).toBeInTheDocument();
      expect(screen.getByText("Paragraph")).toBeInTheDocument();
      expect(screen.getByRole("link")).toHaveAttribute(
        "href",
        "https://example.com",
      );
    });
  });

  describe("BlogLink", (): void => {
    it("renders without crashing", (): void => {
      expect.hasAssertions();

      render(
        <BlogLink href={"https://example.com"}>
          <p>{"Text"}</p>{" "}
        </BlogLink>,
      );

      expect(screen.getByText("Text")).toBeInTheDocument();
      expect(screen.getByRole("link")).toHaveAttribute(
        "href",
        "https://example.com",
      );
    });
  });

  describe("Caution", (): void => {
    it("renders without crashing", (): void => {
      expect.hasAssertions();

      render(
        <Caution>
          <p>{"Text Caution"}</p>{" "}
        </Caution>,
      );

      expect(screen.getByText("Text Caution")).toBeInTheDocument();
    });
  });
});
