import React from "react";

import { ContentSection } from "./ContentSection";
import { CtaSection } from "./CtaSection";
import { HeaderSection } from "./HeaderSection";
import { InfoSection } from "./InfoSection";

interface ISolutionPageProps {
  author: string;
  category: string;
  date: string;
  description: string;
  html: string;
  image: string;
  slug: string;
  subtitle: string;
  title: string;
  writer: string;
}

const PostPage: React.FC<ISolutionPageProps> = ({
  author,
  category,
  description,
  date,
  html,
  image,
  slug,
  subtitle,
  title,
  writer,
}): JSX.Element => {
  return (
    <React.Fragment>
      <HeaderSection
        category={category}
        description={description}
        image={image}
        subtitle={subtitle}
        title={title}
      />
      <InfoSection author={author} date={date} writer={writer} />
      <ContentSection html={html} slug={slug} />
      <CtaSection />
    </React.Fragment>
  );
};

export { PostPage };
