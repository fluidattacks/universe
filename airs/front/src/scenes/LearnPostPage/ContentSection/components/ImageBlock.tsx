import React from "react";

import { ImageBlockContainer } from "./styles";

interface IImageProps {
  children: React.ReactNode;
}

const ImageBlock: React.FC<IImageProps> = ({ children }): JSX.Element => (
  <ImageBlockContainer>{children}</ImageBlockContainer>
);

export { ImageBlock };
