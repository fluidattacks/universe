import React from "react";

import { BlogLink } from "./components/BlogLink";
import { Caution } from "./components/Caution";
import { Header2 } from "./components/Header2";
import { Header3 } from "./components/Header3";
import { Header4 } from "./components/Header4";
import { ImageBlock } from "./components/ImageBlock";
import { Paragraph } from "./components/Paragraph";
import { Quote } from "./components/Quote";
import { TableBlock } from "./components/TableBlock";
import { VideoBlock } from "./components/VideoBlock";

import { BlogCta } from "../../../components/BlogCta";
import { Container } from "../../../components/Container";
import { ShareSection } from "../../../components/ShareSection";
import { useHtml } from "../../../utils/hooks/useHtml";

interface IContentProps {
  html: string;
  slug: string;
}

const components = {
  a: BlogLink,
  "caution-box": Caution,
  "cta-banner": BlogCta,
  h2: Header2,
  h3: Header3,
  h4: Header4,
  "image-block": ImageBlock,
  p: Paragraph,
  "quote-box": Quote,
  "table-block": TableBlock,
  "video-block": VideoBlock,
};

const ContentSection: React.FC<IContentProps> = ({
  html,
  slug,
}): JSX.Element => {
  const content = useHtml(components, html);

  return (
    <Container ph={4} pv={5}>
      <Container
        center={true}
        direction={"reverse"}
        display={"flex"}
        maxWidth={"1440px"}
        wrap={"wrap"}
      >
        <Container width={"95%"} widthMd={"85%"} widthSm={"100%"}>
          <Container center={true} maxWidth={"800px"}>
            <div className={"new-internal"}>{content}</div>
          </Container>
        </Container>
        <Container width={"5%"} widthMd={"15%"} widthSm={"100%"}>
          <ShareSection slug={slug} />
        </Container>
      </Container>
    </Container>
  );
};

export { ContentSection };
