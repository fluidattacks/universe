import { render, screen } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { useStaticQuery } from "gatsby";
import React from "react";

import { PostsList } from "./PostsList";

import { LearnPage } from ".";
import { allCloudinaryMediaData } from "test-utils/mocks";
import type { ICloudinaryMedia } from "utils/types";

interface IDataPostsListMock {
  allCloudinaryMedia: ICloudinaryMedia;
  allMarkdownRemark: {
    edges: {
      node: {
        fields: {
          slug: string;
        };
        html: string;
        frontmatter: {
          alt: string;
          author: string;
          category: string;
          date: string;
          description: string;
          image: string;
          slug: string;
          spanish: string;
          subtitle: string;
          tags: string;
          title: string;
        };
      };
    }[];
  };
}

const mockUseStaticQuery = jest.spyOn({ useStaticQuery }, `useStaticQuery`);
const mockDataUseStaticQuery: IDataPostsListMock = {
  allCloudinaryMedia: allCloudinaryMediaData,
  allMarkdownRemark: {
    edges: [
      {
        node: {
          fields: {
            slug: "",
          },
          frontmatter: {
            alt: "",
            author: "Andres Roldan",
            category: "Argerich",
            date: "2021-11-02",
            description: "description",
            image: "test.jgp",
            slug: "/es/1",
            spanish: "Yes",
            subtitle: "subtitle",
            tags: "éxitos,tag2,tag3",
            title: "First post",
          },
          html: "",
        },
      },
      {
        node: {
          fields: {
            slug: "",
          },
          frontmatter: {
            alt: "",
            author: "John Doe",
            category: "Argerich2",
            date: "2021-11-03",
            description: "description 2",
            image: "test.jgp",
            slug: "/es/2",
            spanish: "Yes",
            subtitle: "subtitle 2",
            tags: "tag4",
            title: "Second post",
          },
          html: "",
        },
      },
    ],
  },
};

describe(`LearnPage`, (): void => {
  beforeEach((): void => {
    mockUseStaticQuery.mockImplementation(
      (): IDataPostsListMock => mockDataUseStaticQuery,
    );
  });

  afterEach((): void => {
    jest.restoreAllMocks();
  });
  it("renders LearnPage", (): void => {
    expect.hasAssertions();

    const { container } = render(<LearnPage />);

    expect(container).toBeInTheDocument();
  });
});

describe("PostsList", (): void => {
  beforeEach((): void => {
    mockUseStaticQuery.mockImplementation(
      (): IDataPostsListMock => mockDataUseStaticQuery,
    );
  });

  afterEach((): void => {
    jest.restoreAllMocks();
  });
  it("renders PostsList", (): void => {
    expect.hasAssertions();

    render(<PostsList />);

    expect(screen.queryByText("November 2, 2021")).toBeInTheDocument();
    expect(screen.queryByText("description")).toBeInTheDocument();
    expect(screen.queryByText("Argerich")).toBeInTheDocument();
    expect(screen.queryByText("Éxitos,tag2,tag3")).toBeInTheDocument();
    expect(screen.queryByText("First post")).toBeInTheDocument();

    expect(screen.queryByText("November 3, 2021")).toBeInTheDocument();
    expect(screen.queryByText("description 2")).toBeInTheDocument();
    expect(screen.queryByText("Argerich2")).toBeInTheDocument();
    expect(screen.queryByText("Tag4")).toBeInTheDocument();
    expect(screen.queryByText("Second post")).toBeInTheDocument();
  });

  it("BlogsPage should allow using filters", async (): Promise<void> => {
    expect.hasAssertions();

    render(<PostsList />);

    expect(screen.queryByText("First post")).toBeInTheDocument();
    expect(screen.queryByText("Second post")).toBeInTheDocument();

    const dropdown = screen.getAllByRole("combobox");

    expect(dropdown).toHaveLength(3);

    await userEvent.selectOptions(dropdown[0], "Andres Roldan");

    expect(screen.queryByText("First post")).toBeInTheDocument();
    expect(screen.queryByText("Second post")).not.toBeInTheDocument();

    await userEvent.selectOptions(dropdown[0], "John Doe");

    expect(screen.queryByText("First post")).not.toBeInTheDocument();
    expect(screen.queryByText("Second post")).toBeInTheDocument();

    await userEvent.selectOptions(dropdown[0], "blogListAuthors.all");

    await userEvent.selectOptions(dropdown[1], "Éxitos,tag2,tag3");

    expect(screen.queryByText("First post")).toBeInTheDocument();
    expect(screen.queryByText("Second post")).not.toBeInTheDocument();

    await userEvent.selectOptions(dropdown[1], "Tag4");

    expect(screen.queryByText("First post")).not.toBeInTheDocument();
    expect(screen.queryByText("Second post")).toBeInTheDocument();

    await userEvent.selectOptions(dropdown[1], "blogListTags.all");

    await userEvent.selectOptions(dropdown[2], "BlogListDatePeriods.lastMonth");

    expect(
      screen.queryByText("There are no posts to display"),
    ).toBeInTheDocument();

    await userEvent.selectOptions(dropdown[2], "blogListDatePeriods.all");

    const searchInput = screen.getAllByRole("textbox");

    expect(searchInput).toHaveLength(1);

    await userEvent.type(searchInput[0], "First");

    expect(screen.queryByText("First post")).toBeInTheDocument();
    expect(screen.queryByText("Second post")).not.toBeInTheDocument();

    await userEvent.clear(searchInput[0]);

    await userEvent.click(
      screen.getByRole("button", {
        name: "Argerich",
      }),
    );

    expect(screen.queryByText("First post")).toBeInTheDocument();
    expect(screen.queryByText("Second post")).not.toBeInTheDocument();

    await userEvent.click(
      screen.getByRole("button", {
        name: "Argerich2",
      }),
    );

    expect(screen.queryByText("First post")).not.toBeInTheDocument();
    expect(screen.queryByText("Second post")).toBeInTheDocument();
  });
});
