import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import { useStaticQuery } from "gatsby";
import React from "react";

import { CategoriesPage } from ".";
import { allCloudinaryMediaData } from "test-utils/mocks";
import { translate } from "utils/translations/translate";
import type { ICloudinaryMedia } from "utils/types";

interface IDataCategoriesPageMock {
  allCloudinaryMedia: ICloudinaryMedia;
  allMarkdownRemark: {
    edges: [
      {
        node: {
          fields: {
            slug: string;
          };
          frontmatter: {
            category: string;
          };
        };
      },
    ];
  };
}

const mockUseStaticQuery = jest.spyOn({ useStaticQuery }, `useStaticQuery`);
const mockDataUseStaticQuery: IDataCategoriesPageMock = {
  allCloudinaryMedia: allCloudinaryMediaData,
  allMarkdownRemark: {
    edges: [
      {
        node: {
          fields: {
            slug: "",
          },
          frontmatter: {
            category: "Category content",
          },
        },
      },
    ],
  },
};

describe("CategoriesPage", (): void => {
  beforeEach((): void => {
    mockUseStaticQuery.mockImplementation(
      (): IDataCategoriesPageMock => mockDataUseStaticQuery,
    );
  });
  afterEach((): void => {
    jest.restoreAllMocks();
  });
  it("CategoriesPage should render mocked data", (): void => {
    expect.hasAssertions();

    render(<CategoriesPage />);

    expect(
      screen.queryByText(translate.t("blog.categories.title")),
    ).toBeInTheDocument();
    expect(
      screen.queryByText(translate.t("blog.subscribeCta.title")),
    ).toBeInTheDocument();
    expect(
      screen.queryByText(translate.t("blog.subscribeCta.title")),
    ).toBeInTheDocument();
    expect(screen.queryByText("Category content")).toBeInTheDocument();
    expect(
      screen.queryByText(translate.t("blog.ctaTitle")),
    ).toBeInTheDocument();
    expect(
      screen.queryByText(translate.t("blog.ctaDescription")),
    ).toBeInTheDocument();
    expect(screen.queryByText("Try for free")).toBeInTheDocument();
    expect(screen.queryByText("Learn more")).toBeInTheDocument();
    expect(screen.queryByText("Image not found")).toBeInTheDocument();
  });
});
