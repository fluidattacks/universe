import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import { useStaticQuery } from "gatsby";
import React from "react";

import { BlogsToFilterPage } from ".";
import { allCloudinaryMediaData } from "test-utils/mocks";
import { translate } from "utils/translations/translate";
import type { ICloudinaryMedia } from "utils/types";

interface IDataBlogsToFilterPageMock {
  allCloudinaryMedia: ICloudinaryMedia;
  allMarkdownRemark: {
    edges: [
      {
        node: {
          fields: {
            slug: string;
          };
          frontmatter: {
            alt: string;
            author: string;
            category: string;
            date: string;
            slug: string;
            description: string;
            image: string;
            spanish: string;
            subtitle: string;
            tags: string;
            title: string;
          };
        };
      },
    ];
  };
}

const mockUseStaticQuery = jest.spyOn({ useStaticQuery }, `useStaticQuery`);
const mockDataUseStaticQuery: IDataBlogsToFilterPageMock = {
  allCloudinaryMedia: allCloudinaryMediaData,
  allMarkdownRemark: {
    edges: [
      {
        node: {
          fields: {
            slug: "",
          },
          frontmatter: {
            alt: "Alt content",
            author: "Fluid",
            category: "Category content",
            date: "Date content",
            description: "Description content",
            image: "Image content",
            slug: "Slug content",
            spanish: "Spanish content",
            subtitle: "Subtitle content",
            tags: "Tags content",
            title: "Title content",
          },
        },
      },
    ],
  },
};

describe("BlogsToFilterPage", (): void => {
  beforeEach((): void => {
    mockUseStaticQuery.mockImplementation(
      (): IDataBlogsToFilterPageMock => mockDataUseStaticQuery,
    );
  });
  afterEach((): void => {
    jest.restoreAllMocks();
  });
  it("BlogsToFilterPage should render mocked data with some missing params", (): void => {
    expect.hasAssertions();

    render(<BlogsToFilterPage filterBy={"author"} value={"Fluid"} />);

    expect(
      screen.queryByText(translate.t("blog.subscribeCta.title")),
    ).toBeInTheDocument();
    expect(
      screen.queryByText(translate.t("blog.ctaTitle")),
    ).toBeInTheDocument();
    expect(
      screen.queryByText(translate.t("blog.ctaDescription")),
    ).toBeInTheDocument();
    expect(screen.queryByText("Try for free")).toBeInTheDocument();
    expect(screen.queryByText("Learn more")).toBeInTheDocument();
  });
});
