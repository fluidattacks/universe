import React from "react";

import { Layout } from "../Footer/Layout";
import { NavbarComponent } from "../Menu";

const WebsiteWrapper = ({
  children,
}: Readonly<{
  children: React.ReactNode;
}>): JSX.Element => {
  return (
    <React.StrictMode>
      <Layout>
        <div>
          <NavbarComponent />
          {children}
        </div>
      </Layout>
    </React.StrictMode>
  );
};

export { WebsiteWrapper };
