import { graphql } from "gatsby";
import type { StaticQueryDocument } from "gatsby";
import React from "react";

import { PageHeader } from "../components/PageHeader";
import { Seo } from "../components/Seo";
import { WebsiteWrapper } from "../scenes/WebsiteWrapper";
import { IframeContainer, PageArticle } from "../styles/styles";
import { updateLanguage } from "../utils/utilities";

const ContactUsIndex: React.FC<IQueryData> = ({
  data,
  pageContext,
}: IQueryData): JSX.Element => {
  const { language } = pageContext;
  void updateLanguage(language);

  const { slug, subtext, subtitle, title } = data.markdownRemark.frontmatter;
  const src =
    language === "es"
      ? "https://forms.zohopublic.com/fluid4ttacks/form/ContactForm/formperma/g4Je_6zumBrCA3iMMJMHU6b4JrVrUzItp_cfyb3ad74?zf_lang=es"
      : "https://forms.zohopublic.com/fluid4ttacks/form/ContactForm/formperma/g4Je_6zumBrCA3iMMJMHU6b4JrVrUzItp_cfyb3ad74";
  const isDemo = slug.endsWith("demo/");
  const paramsForm = "srcpg=Demonstration";
  const separator = src.includes("?") ? "&" : "?";
  const prefilledSrc = isDemo ? `${src}${separator}${paramsForm}` : src;

  return (
    <WebsiteWrapper>
      <PageArticle $bgColor={"#f9f9f9"}>
        <PageHeader
          banner={"contact-bg"}
          pageWithBanner={true}
          slug={slug}
          subtext={subtext}
          subtitle={subtitle}
          title={title}
        />
        <IframeContainer>
          <iframe
            height={"100%"}
            sandbox={
              "allow-scripts allow-forms allow-top-navigation allow-same-origin allow-popups"
            }
            src={prefilledSrc}
            style={{ border: 0 }}
            title={"Contact Form"}
            width={"85%"}
          />
        </IframeContainer>
      </PageArticle>
    </WebsiteWrapper>
  );
};

export const Head = ({ data }: IQueryData): JSX.Element => {
  const { description, keywords, slug, title } =
    data.markdownRemark.frontmatter;

  return (
    <Seo
      description={description}
      image={
        "https://res.cloudinary.com/fluid-attacks/image/upload/v1619631770/airs/contact-us/bg-contact-us_cpcyoj"
      }
      keywords={keywords}
      path={slug}
      title={`${title} | Fluid Attacks`}
    />
  );
};

export const query: StaticQueryDocument = graphql`
  query ContactUsIndex($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      fields {
        slug
      }
      frontmatter {
        description
        keywords
        slug
        subtext
        subtitle
        title
      }
    }
  }
`;

export default ContactUsIndex;
