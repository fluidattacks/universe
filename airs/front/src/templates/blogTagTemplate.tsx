/* eslint react/forbid-component-props: 0 */
import Bugsnag from "@bugsnag/js";
import React from "react";

import { Seo } from "../components/Seo";
import { BlogsToFilterPage } from "../scenes/BlogsToFilterPage";
import { WebsiteWrapper } from "../scenes/WebsiteWrapper";
import { PageArticle } from "../styles/styles";
import { translate } from "../utils/translations/translate";
import {
  capitalizePlainString,
  kebabCaseToCamelCase,
  updateLanguage,
} from "../utils/utilities";

const BlogTagTemplate: React.FC<IQueryData> = ({
  pageContext,
}: IQueryData): JSX.Element => {
  const { language } = pageContext;
  void updateLanguage(language);

  const { tagName, tagUri } = pageContext;

  // Uri -> translation key
  const homologousCategories: Record<string, string> = {
    "aprendizaje-automatico": "machineLearning",
    "blue-team": "blueTeam",
    ciberseguridad: "cybersecurity",
    codigo: "code",
    credencial: "credential",
    criptografia: "cryptography",
    cumplimiento: "compliance",
    devsecops: "devsecops",
    empresa: "company",
    estandar: "standard",
    exploit: "exploit",
    formacion: "training",
    hacking: "hacking",
    "ingenieria-social": "socialEngineering",
    malware: "malware",
    nube: "cloud",
    pentesting: "pentesting",
    "pruebas-de-seguridad": "securityTesting",
    "red-team": "redTeam",
    riesgo: "risk",
    software: "software",
    tendencia: "trend",
    vulnerabilidad: "vulnerability",
    vulnserver: "vulnserver",
    web: "web",
    windows: "windows",
  };

  const tagKey =
    language === "en"
      ? kebabCaseToCamelCase(tagUri)
      : homologousCategories[tagUri];

  const tagTitle = capitalizePlainString(tagName);
  const description = translate.t(`blogListTags.${tagKey}.description`);

  const translationNotFound = new RegExp(
    `blogListTags.${tagKey}.description`,
    "ui",
  ).test(description);

  if (translationNotFound) {
    Bugsnag.notify(`Description not found for tag ${tagName}`);
  }

  const tagDescription = translationNotFound
    ? translate.t("blogListTags.alternativeDescription", { tag: tagName })
    : description;

  return (
    <WebsiteWrapper>
      <PageArticle $bgColor={"transparent"}>
        <BlogsToFilterPage
          description={tagDescription}
          filterBy={"tag"}
          title={tagTitle}
          value={tagName}
        />
      </PageArticle>
    </WebsiteWrapper>
  );
};

export const Head = ({ pageContext }: IQueryData): JSX.Element => {
  const { tagName, tagUri, language } = pageContext;
  const tagTitle = capitalizePlainString(tagName);
  const metaDescription = translate.t("blogListTags.metaDescription", {
    tag: tagTitle,
  });
  const languagePrefix = language === "es" ? "es/" : "";

  return (
    <Seo
      description={metaDescription}
      image={
        "https://res.cloudinary.com/fluid-attacks/image/upload/v1619632208/airs/bg-blog_bj0szx"
      }
      keywords={translate.t("blog.keywords")}
      path={`https://fluidattacks.com/${languagePrefix}blog/tags/${tagUri}`}
      title={`${translate.t("blogPageTitle.title1")} ${tagTitle} ${translate.t(
        "blogPageTitle.title2",
      )}`}
      type={"blog"}
    />
  );
};

export default BlogTagTemplate;
