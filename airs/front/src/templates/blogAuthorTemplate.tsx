import Bugsnag from "@bugsnag/js";
import React from "react";

import { Seo } from "../components/Seo";
import { BlogsToFilterPage } from "../scenes/BlogsToFilterPage";
import { WebsiteWrapper } from "../scenes/WebsiteWrapper";
import { PageArticle } from "../styles/styles";
import { translate } from "../utils/translations/translate";
import { kebabCaseToCamelCase, updateLanguage } from "../utils/utilities";

const BlogAuthorTemplate: React.FC<IQueryData> = ({
  pageContext,
}: IQueryData): JSX.Element => {
  const { authorName, authorUri, language } = pageContext;
  void updateLanguage(language);
  const kebabCaseName = kebabCaseToCamelCase(authorUri);

  const description = translate.t(
    `blogListAuthors.${kebabCaseName}.description`,
  );
  const descriptionNotFound =
    description === `blogListAuthors.${kebabCaseName}.description`;
  if (descriptionNotFound) {
    Bugsnag.notify(`Author ${authorName} has no description`);
  }

  const headerDescription = descriptionNotFound
    ? translate.t("blogListAuthors.descriptionNotAvailable")
    : description;

  return (
    <WebsiteWrapper>
      <PageArticle $bgColor={"transparent"}>
        <BlogsToFilterPage
          description={headerDescription}
          filterBy={"author"}
          title={`Posts by ${authorName}`}
          value={authorName}
        />
      </PageArticle>
    </WebsiteWrapper>
  );
};

export const Head = ({ pageContext }: IQueryData): JSX.Element => {
  const { authorName, authorUri, language } = pageContext;
  const metaDescription = translate.t("blogListAuthors.metaDescription", {
    author: authorName,
  });
  const languagePrefix = language === "es" ? "es/" : "";

  return (
    <Seo
      description={metaDescription}
      image={
        "https://res.cloudinary.com/fluid-attacks/image/upload/v1619632208/airs/bg-blog_bj0szx"
      }
      keywords={translate.t("blog.keywords")}
      path={`https://fluidattacks.com/${languagePrefix}blog/authors/${authorUri}`}
      title={`${translate.t("blogPageTitle.title3")} ${authorName}
      ${translate.t("blogPageTitle.title2")}`}
      type={"blog"}
    />
  );
};

export default BlogAuthorTemplate;
