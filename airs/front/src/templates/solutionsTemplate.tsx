import { graphql } from "gatsby";
import type { StaticQueryDocument } from "gatsby";
import React from "react";

import { Container } from "../components/Container";
import { Grid } from "../components/Grid";
import { Hero } from "../components/Hero";
import { Seo } from "../components/Seo";
import { VerticalCard } from "../components/VerticalCard";
import { WebsiteWrapper } from "../scenes/WebsiteWrapper";
import { PageArticle } from "../styles/styles";
import { translate } from "../utils/translations/translate";
import { updateLanguage } from "../utils/utilities";

const SolutionsIndex: React.FC<IQueryData> = ({
  pageContext,
}: IQueryData): JSX.Element => {
  const { language } = pageContext;
  void updateLanguage(language);

  const solutionData = [
    {
      image: "solution-devsecops_jgeyje",
      link: "/solutions/devsecops/",
      paragraph: translate.t("solutions.devSecOps.paragraph"),
      title: translate.t("solutions.devSecOps.subtitle"),
    },
    {
      image: "solution-security-testing_mmthfa",
      link: "/solutions/security-testing/",
      paragraph: translate.t("solutions.securityTesting.paragraph"),
      title: translate.t("solutions.securityTesting.subtitle"),
    },
    {
      image: "solution-penetration-testing-as-a-service",
      link: "/solutions/penetration-testing-as-a-service/",
      paragraph: translate.t(
        "solutions.penetrationTestingAsAService.paragraph",
      ),
      title: translate.t("solutions.penetrationTestingAsAService.subtitle"),
    },
    {
      image: "solution-ethical-hacking_zuhkms",
      link: "/solutions/ethical-hacking/",
      paragraph: translate.t("solutions.ethicalHacking.paragraph"),
      title: translate.t("solutions.ethicalHacking.subtitle"),
    },
    {
      image: "solution-red-teaming_trx6rr",
      link: "/solutions/red-teaming/",
      paragraph: translate.t("solutions.redTeaming.paragraph"),
      title: translate.t("solutions.redTeaming.subtitle"),
    },
    {
      image: "solution-secure-code-review_dyaluj",
      link: "/solutions/secure-code-review/",
      paragraph: translate.t("solutions.secureCode.paragraph"),
      title: translate.t("solutions.secureCode.subtitle"),
    },
    {
      image: "solution-vulnerability-management_a5xmkt",
      link: "/solutions/vulnerability-management/",
      paragraph: translate.t("solutions.vulnerabilityManagement.paragraph"),
      title: translate.t("solutions.vulnerabilityManagement.subtitle"),
    },
    {
      image: "solution-app-security-posture-management",
      link: "/solutions/app-security-posture-management/",
      paragraph: translate.t(
        "solutions.appSecurityPostureManagement.paragraph",
      ),
      title: translate.t("solutions.appSecurityPostureManagement.subtitle"),
    },
  ];

  return (
    <WebsiteWrapper>
      <PageArticle $bgColor={"#ffffff"}>
        <Hero
          button1Link={"https://app.fluidattacks.com/SignUp"}
          button1Text={translate.t("home.hero.button1")}
          button2Link={"/contact-us/"}
          button2Text={translate.t("home.hero.button2")}
          image={"airs/solutions/Index/application-security-solutions"}
          paragraph={translate.t("solutions.information.paragraph")}
          size={"big"}
          sizeSm={"medium"}
          title={translate.t("solutions.information.subtitle")}
        />
        <Container center={true} maxWidth={"1440px"} ph={4} pv={5}>
          <Grid columns={3} columnsMd={2} columnsSm={1} gap={"1rem"}>
            {solutionData.map((solution): JSX.Element => {
              return (
                <VerticalCard
                  alt={solution.title}
                  bgColor={"transparent"}
                  btnDisplay={"inline-block"}
                  btnText={translate.t("solutions.homeCards.button")}
                  btnVariant={"primary"}
                  description={solution.paragraph}
                  image={`airs/solutions/${solution.image}`}
                  imagePadding={true}
                  key={solution.title}
                  link={solution.link}
                  title={solution.title}
                />
              );
            })}
          </Grid>
        </Container>
      </PageArticle>
    </WebsiteWrapper>
  );
};

export const Head = ({ data }: IQueryData): JSX.Element => {
  const { description, keywords, slug, title } =
    data.markdownRemark.frontmatter;

  return (
    <Seo
      description={description}
      image={
        "https://res.cloudinary.com/fluid-attacks/image/upload/v1619630822/airs/solutions/bg-solutions_ylz99o"
      }
      keywords={keywords}
      path={slug}
      title={`${title} | Fluid Attacks`}
    />
  );
};

export const query: StaticQueryDocument = graphql`
  query SolutionsIndex($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      fields {
        slug
      }
      frontmatter {
        description
        keywords
        slug
        title
      }
    }
  }
`;

export default SolutionsIndex;
