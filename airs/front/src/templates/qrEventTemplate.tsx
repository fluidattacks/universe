import { graphql } from "gatsby";
import type { StaticQueryDocument } from "gatsby";
import React from "react";

import { PageHeader } from "../components/PageHeader";
import { Seo } from "../components/Seo";
import { WebsiteWrapper } from "../scenes/WebsiteWrapper";
import { IframeContainer, PageArticle } from "../styles/styles";
import { updateLanguage } from "../utils/utilities";

const QrEventIndex: React.FC<IQueryData> = ({
  data,
  pageContext,
}: IQueryData): JSX.Element => {
  const { language } = pageContext;
  void updateLanguage(language);

  const { slug, subtext, subtitle, title } = data.markdownRemark.frontmatter;

  return (
    <WebsiteWrapper>
      <PageArticle $bgColor={"#f9f9f9"}>
        <PageHeader
          banner={"contact-bg"}
          pageWithBanner={true}
          slug={slug}
          subtext={subtext}
          subtitle={subtitle}
          title={title}
        />
        <IframeContainer>
          <iframe
            sandbox={
              "allow-forms allow-top-navigation allow-same-origin allow-scripts allow-popups"
            }
            src={
              "https://forms.zohopublic.com/fluid4ttacks/form/QREvents/formperma/DrJym0UgnGezDSs8wr46hykRnWlDKHYuZZfz5Ig8YiE"
            }
            style={{
              border: "0",
              height: "900px",
              marginBottom: "-7px",
              width: "100%",
            }}
            title={"QR Event Form"}
          />
        </IframeContainer>
      </PageArticle>
    </WebsiteWrapper>
  );
};

export const Head = ({ data }: IQueryData): JSX.Element => {
  const { description, keywords, slug, title } =
    data.markdownRemark.frontmatter;

  return (
    <Seo
      description={description}
      image={
        "https://res.cloudinary.com/fluid-attacks/image/upload/v1619631770/airs/contact-us/bg-contact-us_cpcyoj"
      }
      keywords={keywords}
      path={slug}
      title={`${title} | Fluid Attacks`}
    />
  );
};

export const query: StaticQueryDocument = graphql`
  query QrEventIndex($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      fields {
        slug
      }
      frontmatter {
        description
        keywords
        title
        subtitle
        subtext
        slug
      }
    }
  }
`;

export default QrEventIndex;
