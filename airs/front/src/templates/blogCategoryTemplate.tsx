import Bugsnag from "@bugsnag/js";
import React from "react";

import { Seo } from "../components/Seo";
import { BlogsToFilterPage } from "../scenes/BlogsToFilterPage";
import { WebsiteWrapper } from "../scenes/WebsiteWrapper";
import { PageArticle } from "../styles/styles";
import { translate } from "../utils/translations/translate";
import {
  capitalizePlainString,
  kebabCaseToCamelCase,
  updateLanguage,
} from "../utils/utilities";

const BlogCategoryTemplate: React.FC<IQueryData> = ({
  pageContext,
}: IQueryData): JSX.Element => {
  const { categoryName, categoryUri, language } = pageContext;
  void updateLanguage(language);

  // Uri -> translation key
  const categoriesHomologous: Record<string, string> = {
    ataques: "attacks",
    desarrollo: "development",
    entrevista: "interview",
    filosofia: "philosophy",
    opiniones: "opinions",
    politica: "politics",
  };

  const categoryKey =
    language === "en"
      ? kebabCaseToCamelCase(categoryUri)
      : categoriesHomologous[categoryUri];

  const description = translate.t(
    `blogListCategories.${categoryKey}.description`,
  );

  if (description === `blogListCategories.${categoryKey}.description`) {
    Bugsnag.notify(`Category ${categoryName} has no description`);
  }

  return (
    <WebsiteWrapper>
      <PageArticle $bgColor={"transparent"}>
        <BlogsToFilterPage
          description={description}
          filterBy={"category"}
          title={categoryName}
          value={categoryName}
        />
      </PageArticle>
    </WebsiteWrapper>
  );
};

export const Head = ({ pageContext }: IQueryData): JSX.Element => {
  const { categoryName, categoryUri, language } = pageContext;
  const metaDescription = translate.t("blogListCategories.metaDescription", {
    category: categoryName,
  });
  const languagePrefix = language === "es" ? "es/" : "";

  return (
    <Seo
      description={metaDescription}
      image={
        "https://res.cloudinary.com/fluid-attacks/image/upload/v1619632208/airs/bg-blog_bj0szx"
      }
      keywords={translate.t("blog.keywords")}
      path={`https://fluidattacks.com/${languagePrefix}blog/categories/${categoryUri}`}
      title={`${translate.t("blogPageTitle.title1")} ${capitalizePlainString(
        categoryName,
      )} ${translate.t("blogPageTitle.title2")}`}
      type={"blog"}
    />
  );
};

export default BlogCategoryTemplate;
