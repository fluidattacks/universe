import { graphql } from "gatsby";
import type { StaticQueryDocument } from "gatsby";
import React from "react";

import { PageHeader } from "../components/PageHeader";
import { Seo } from "../components/Seo";
import { WebsiteWrapper } from "../scenes/WebsiteWrapper";
import { IframeContainer, PageArticle } from "../styles/styles";
import { updateLanguage } from "../utils/utilities";

const SubscribeIndex: React.FC<IQueryData> = ({
  data,
  pageContext,
}: IQueryData): JSX.Element => {
  const { language } = pageContext;
  void updateLanguage(language);

  const { slug, subtext, subtitle, title } = data.markdownRemark.frontmatter;

  return (
    <WebsiteWrapper>
      <PageArticle $bgColor={"#f9f9f9"}>
        <PageHeader
          banner={"contact-bg"}
          pageWithBanner={true}
          slug={slug}
          subtext={subtext}
          subtitle={subtitle}
          title={title}
        />
        <IframeContainer>
          <iframe
            sandbox={
              "allow-forms allow-top-navigation allow-same-origin allow-scripts allow-popups"
            }
            src={
              "https://forms.zohopublic.com/fluid4ttacks/form/Newsletter/formperma/Oj1qU25E49Iy6up1YnEtxsXuOxqbDEl1V5QtvLIAM7c"
            }
            style={{
              border: "0",
              height: "700px",
              width: "100%",
            }}
            title={"Subscription form"}
          />
        </IframeContainer>
      </PageArticle>
    </WebsiteWrapper>
  );
};

export const Head = ({ data }: IQueryData): JSX.Element => {
  const { description, keywords, slug, title } =
    data.markdownRemark.frontmatter;

  return (
    <Seo
      description={description}
      image={
        "https://res.cloudinary.com/fluid-attacks/image/upload/v1619631770/airs/contact-us/bg-contact-us_cpcyoj"
      }
      keywords={keywords}
      path={slug}
      title={`${title} | Fluid Attacks`}
    />
  );
};

export const query: StaticQueryDocument = graphql`
  query SubscribeIndex($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      fields {
        slug
      }
      frontmatter {
        description
        keywords
        slug
        subtext
        subtitle
        title
      }
    }
  }
`;

export default SubscribeIndex;
