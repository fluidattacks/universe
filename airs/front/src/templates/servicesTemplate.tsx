import { graphql } from "gatsby";
import type { StaticQueryDocument } from "gatsby";
import _ from "lodash";
import React from "react";

import { Seo } from "../components/Seo";
import { ServicesPage } from "../components/ServicesPage";
import { Title } from "../components/Typography";
import { Layout } from "../scenes/Footer/Layout";
import { NavbarComponent } from "../scenes/Menu";
import { PageArticle, SectionContainer } from "../styles/styles";
import { updateLanguage } from "../utils/utilities";

const ServicesIndex: React.FC<IQueryData> = ({
  data,
  pageContext,
}: IQueryData): JSX.Element => {
  const { language } = pageContext;
  void updateLanguage(language);

  if (_.isNull(data.markdownRemark)) return <div />;

  const { title } = data.markdownRemark.frontmatter;

  return (
    <Layout>
      <div>
        <NavbarComponent />
        <PageArticle $bgColor={"#f4f4f6"}>
          <SectionContainer>
            <Title color={"#2e2e38"} level={1} size={"big"}>
              {title}
            </Title>
            <ServicesPage />
          </SectionContainer>
        </PageArticle>
      </div>
    </Layout>
  );
};

export const Head = ({ data }: IQueryData): JSX.Element => {
  if (_.isNull(data.markdownRemark))
    return <title>{"Services | Fluid Attacks"}</title>;

  const { description, image, keywords, slug, title } =
    data.markdownRemark.frontmatter;

  return (
    <Seo
      description={description}
      image={image}
      keywords={keywords}
      path={slug}
      title={`${title} | Fluid Attacks`}
    />
  );
};

export const query: StaticQueryDocument = graphql`
  query ServicesIndex($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      fields {
        slug
      }
      frontmatter {
        keywords
        description
        slug
        title
        image
      }
    }
  }
`;

export default ServicesIndex;
