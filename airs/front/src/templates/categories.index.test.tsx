import "@testing-library/jest-dom";
import { render } from "@testing-library/react";
import React from "react";

import CategoriesIndex from "./categoriesTemplate";
import CategoryIndex from "./categoryTemplate";

import { exampleQueryData } from "test-utils/mocks";

describe("categories", (): void => {
  describe("categoryTemplate", (): void => {
    it("CategoryIndex should render mocked data", (): void => {
      expect.hasAssertions();

      const { container } = render(
        <CategoryIndex
          data={exampleQueryData.data}
          pageContext={exampleQueryData.pageContext}
        />,
      );

      expect(container).toBeInTheDocument();
    });
    it("CategoryIndex should render mocked data without headtitle", (): void => {
      expect.hasAssertions();

      const modifiedData = {
        ...exampleQueryData,
        data: {
          ...exampleQueryData.data,
          markdownRemark: {
            ...exampleQueryData.data.markdownRemark,
            frontmatter: {
              ...exampleQueryData.data.markdownRemark.frontmatter,
              headtitle: "",
            },
          },
        },
      };

      const { container } = render(
        <CategoryIndex
          data={modifiedData.data}
          pageContext={exampleQueryData.pageContext}
        />,
      );

      expect(container).toBeInTheDocument();
    });
  });

  describe("categoriesTemplate", (): void => {
    it("CategoriesIndex should render mocked data", (): void => {
      expect.hasAssertions();

      const { container } = render(
        <CategoriesIndex
          data={exampleQueryData.data}
          pageContext={exampleQueryData.pageContext}
        />,
      );

      expect(container).toBeInTheDocument();
    });
  });
});
