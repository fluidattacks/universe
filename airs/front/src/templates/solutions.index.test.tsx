import "@testing-library/jest-dom";
import { render, waitFor } from "@testing-library/react";
import React from "react";

import SolutionsIndex from "./solutionsTemplate";
import SolutionIndex from "./solutionTemplate";

import { exampleQueryData } from "test-utils/mocks";

describe("solutions", (): void => {
  describe("solutionTemplate", (): void => {
    it("SolutionsIndex should render mocked data", async (): Promise<void> => {
      expect.hasAssertions();

      const { container } = render(
        <SolutionIndex
          data={exampleQueryData.data}
          pageContext={exampleQueryData.pageContext}
        />,
      );

      await waitFor((): void => {
        expect(container).toBeInTheDocument();
      });
    });
    it("SolutionIndex should render mocked data without headtitle", async (): Promise<void> => {
      expect.hasAssertions();

      const modifiedData = {
        ...exampleQueryData,
        data: {
          ...exampleQueryData.data,
          markdownRemark: {
            ...exampleQueryData.data.markdownRemark,
            frontmatter: {
              ...exampleQueryData.data.markdownRemark.frontmatter,
              headtitle: "",
            },
          },
        },
      };
      const { container } = render(
        <SolutionIndex
          data={modifiedData.data}
          pageContext={exampleQueryData.pageContext}
        />,
      );
      await waitFor((): void => {
        expect(container).toBeInTheDocument();
      });
    });
  });

  describe("solutionsTemplate", (): void => {
    it("SolutionsIndex should render mocked data", (): void => {
      expect.hasAssertions();

      const { container } = render(
        <SolutionsIndex
          data={exampleQueryData.data}
          pageContext={exampleQueryData.pageContext}
        />,
      );

      expect(container).toBeInTheDocument();
    });
  });
});
