/* eslint react/forbid-component-props: 0 */
import { Link, graphql } from "gatsby";
import type { StaticQueryDocument } from "gatsby";
import React from "react";

import { Button } from "../components/Button";
import { CloudImage } from "../components/CloudImage";
import { Seo } from "../components/Seo";
import { Text, Title } from "../components/Typography";
import { WebsiteWrapper } from "../scenes/WebsiteWrapper";
import {
  FlexCenterItemsContainer,
  PageArticle,
  PageContainer,
  SystemsCardContainer,
} from "../styles/styles";
import { translate } from "../utils/translations/translate";
import { updateLanguage } from "../utils/utilities";

const CompliancesIndex: React.FC<IQueryData> = ({
  data,
  pageContext,
}: IQueryData): JSX.Element => {
  const { language } = pageContext;
  void updateLanguage(language);

  const { title } = data.markdownRemark.frontmatter;

  const complianceData = [
    {
      image: "owasp",
      link: "/compliance/owasp/",
      paragraph: translate.t("compliances.owasp.paragraph"),
      title: translate.t("compliances.owasp.subtitle"),
    },
    {
      image: "pci",
      link: "/compliance/pci/",
      paragraph: translate.t("compliances.pci.paragraph"),
      title: translate.t("compliances.pci.subtitle"),
    },
    {
      image: "hipaa",
      link: "/compliance/hipaa/",
      paragraph: translate.t("compliances.hipaa.paragraph"),
      title: translate.t("compliances.hipaa.subtitle"),
    },
    {
      image: "nist",
      link: "/compliance/nist/",
      paragraph: translate.t("compliances.nist.paragraph"),
      title: translate.t("compliances.nist.subtitle"),
    },
    {
      image: "gdpr",
      link: "/compliance/gdpr/",
      paragraph: translate.t("compliances.gdpr.paragraph"),
      title: translate.t("compliances.gdpr.subtitle"),
    },
    {
      image: "cve",
      link: "/compliance/cve/",
      paragraph: translate.t("compliances.cve.paragraph"),
      title: translate.t("compliances.cve.subtitle"),
    },
    {
      image: "cwe",
      link: "/compliance/cwe/",
      paragraph: translate.t("compliances.cwe.paragraph"),
      title: translate.t("compliances.cwe.subtitle"),
    },
  ];

  return (
    <WebsiteWrapper>
      <PageArticle $bgColor={"#f4f4f6"}>
        <FlexCenterItemsContainer>
          <Title
            color={"#2e2e38"}
            level={1}
            mt={4}
            size={"big"}
            textAlign={"center"}
          >
            {title}
          </Title>
        </FlexCenterItemsContainer>
        <PageContainer className={"flex flex-wrap"}>
          {complianceData.map((complianceCard): JSX.Element => {
            return (
              <SystemsCardContainer key={complianceCard.title}>
                <CloudImage
                  alt={title}
                  src={`airs/compliance/${complianceCard.image}`}
                  styles={"w-100"}
                />
                <Title color={"#2e2e38"} level={2} mb={1} mt={1} size={"small"}>
                  {complianceCard.title}
                </Title>
                <Text color={"#5c5c70"} mb={1} size={"medium"}>
                  {complianceCard.paragraph}
                </Text>
                <Link to={complianceCard.link}>
                  <Button variant={"tertiary"}>{"Read more"}</Button>
                </Link>
              </SystemsCardContainer>
            );
          })}
        </PageContainer>
      </PageArticle>
    </WebsiteWrapper>
  );
};

export const Head = ({ data }: IQueryData): JSX.Element => {
  const { description, keywords, slug, title } =
    data.markdownRemark.frontmatter;

  return (
    <Seo
      description={description}
      image={
        "https://res.cloudinary.com/fluid-attacks/image/upload/v1619637251/airs/compliance/cover-compliance_vnojb7"
      }
      keywords={keywords}
      path={slug}
      title={`${title} | Fluid Attacks`}
    />
  );
};

export const query: StaticQueryDocument = graphql`
  query CompliancesIndex($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      fields {
        slug
      }
      frontmatter {
        description
        keywords
        slug
        title
      }
    }
  }
`;

export default CompliancesIndex;
