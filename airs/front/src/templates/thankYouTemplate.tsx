import { graphql } from "gatsby";
import type { StaticQueryDocument } from "gatsby";
import React from "react";

import { Container } from "../components/Container";
import { PageHeader } from "../components/PageHeader";
import { Seo } from "../components/Seo";
import { Text } from "../components/Typography";
import { WebsiteWrapper } from "../scenes/WebsiteWrapper";
import { IframeContainer, PageArticle } from "../styles/styles";
import { updateLanguage } from "../utils/utilities";

const ThankYouIndex: React.FC<IQueryData> = ({
  data,
  pageContext,
}: IQueryData): JSX.Element => {
  const { language } = pageContext;
  void updateLanguage(language);

  const { slug, subtext, subtitle, title } = data.markdownRemark.frontmatter;
  const src =
    "https://calendar.google.com/calendar/appointments/schedules/AcZssZ0ZSgVbAJh3yuV0KUzBLjrZbKxVHhflbXO6a-vPRXtgmbhKphQHUCGPK4lLeKMb48zJl0HSwu7K?gv=true";
  const subText =
    language === "es"
      ? "**Elige una fecha y hora** para reunirte con nuestro equipo y explorar cómo Hacking Continuo puede beneficiar a tu empresa."
      : "**Pick a date and time** to meet our team and explore how Continuous Hacking can benefit your company";

  return (
    <WebsiteWrapper>
      <PageArticle $bgColor={"#f9f9f9"}>
        <PageHeader
          banner={"contact-bg thankyou-page"}
          pageWithBanner={true}
          slug={slug}
          subtext={subtext}
          subtitle={subtitle}
          title={title}
        />
        <Container center={true} display={"flex"} mv={5} wrap={"wrap"}>
          <Text color={"#475467"} textAlign={"center"} weight={"regular"}>
            {subText}
          </Text>
        </Container>
        <IframeContainer>
          <iframe
            height={"100%"}
            sandbox={
              "allow-scripts allow-forms allow-top-navigation allow-same-origin allow-popups"
            }
            src={src}
            style={{ border: 0 }}
            title={"Contact Form"}
            width={"85%"}
          />
        </IframeContainer>
      </PageArticle>
    </WebsiteWrapper>
  );
};

export const Head = ({ data }: IQueryData): JSX.Element => {
  const { description, keywords, slug, title } =
    data.markdownRemark.frontmatter;

  return (
    <Seo
      description={description}
      image={
        "https://res.cloudinary.com/fluid-attacks/image/upload/v1619631770/airs/contact-us/bg-contact-us_cpcyoj"
      }
      keywords={keywords}
      path={slug}
      title={`${title} | Fluid Attacks`}
    />
  );
};

export const query: StaticQueryDocument = graphql`
  query ThankYouIndex($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      fields {
        slug
      }
      frontmatter {
        description
        keywords
        slug
        subtext
        subtitle
        title
      }
    }
  }
`;

export default ThankYouIndex;
