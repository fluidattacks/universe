import { graphql } from "gatsby";
import type { StaticQueryDocument } from "gatsby";
import { decode } from "he";
import _ from "lodash";
import React, { useEffect } from "react";

import { Seo } from "../components/Seo";
import { BlogPage } from "../scenes/BlogPage";
import { WebsiteWrapper } from "../scenes/WebsiteWrapper";
import { PageArticle } from "../styles/styles";
import { useBlogsDate } from "../utils/hooks/useSafeDate";
import { updateLanguage } from "../utils/utilities";

const BlogsIndex: React.FC<IQueryData> = ({
  data,
  pageContext,
}: IQueryData): JSX.Element => {
  const { language } = pageContext;
  void updateLanguage(language);

  const { html, timeToRead, headings } = data.markdownRemark;

  const {
    author,
    category,
    date,
    definition,
    image,
    slug,
    subtitle,
    tags,
    title,
    writer,
  } = data.markdownRemark.frontmatter;
  const publishedDate = useBlogsDate(date, language);
  useEffect((): void => {
    const startComment = document.createComment("email_off");
    const endComment = document.createComment("/email_off");
    const bodyElement = document.body;
    bodyElement.insertBefore(startComment, bodyElement.firstChild);
    document.body.appendChild(endComment);
  }, []);

  return (
    <WebsiteWrapper>
      <PageArticle $bgColor={"#fff"}>
        <BlogPage
          author={author}
          category={category}
          date={publishedDate}
          description={definition}
          headings={headings}
          html={html}
          image={image}
          language={language}
          slug={slug}
          subtitle={subtitle}
          tags={tags}
          timeToRead={timeToRead}
          title={title}
          writer={writer}
        />
      </PageArticle>
    </WebsiteWrapper>
  );
};

export const Head = ({ data, pageContext }: IQueryData): JSX.Element => {
  const {
    author,
    date,
    description,
    headtitle,
    image,
    keywords,
    modified,
    slug,
    title,
  } = data.markdownRemark.frontmatter;
  const { language } = pageContext;
  const languagePrefix = language === "es" ? "es/" : "";

  const publishedDate = useBlogsDate(date, language);
  const modifiedDate = useBlogsDate(modified ? modified : date, language);
  const formatedSlug = slug.startsWith("blog/")
    ? slug.split("/").slice(1).join("/")
    : slug;

  return (
    <Seo
      author={author}
      date={publishedDate}
      dateModified={modifiedDate}
      description={description}
      image={image.replace(".webp", "")}
      keywords={keywords}
      path={`https://fluidattacks.com/${languagePrefix}blog/${formatedSlug}`}
      title={
        _.isNil(headtitle)
          ? `${decode(title)} | Blog | Fluid Attacks`
          : `${decode(headtitle)} | Blog | Fluid Attacks`
      }
      type={"blog"}
    />
  );
};

export const query: StaticQueryDocument = graphql`
  query BlogsPages($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      tableOfContents
      headings {
        depth
        value
      }
      timeToRead
      fields {
        slug
      }
      frontmatter {
        author
        category
        date
        definition
        modified
        description
        image
        keywords
        slug
        subtitle
        tags
        writer
        title
        headtitle
      }
    }
  }
`;

export default BlogsIndex;
