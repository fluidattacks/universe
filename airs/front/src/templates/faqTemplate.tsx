/* eslint react/forbid-component-props:0 */
import { graphql } from "gatsby";
import type { StaticQueryDocument } from "gatsby";
import { decode } from "he";
import parse from "html-react-parser";
import React, { useCallback, useEffect, useMemo, useState } from "react";
import sanitizeHtml from "sanitize-html";

import { PageHeader } from "../components/PageHeader";
import { Seo } from "../components/Seo";
import { WebsiteWrapper } from "../scenes/WebsiteWrapper";
import {
  CenteredSpacedContainer,
  FaqContainer,
  PageArticle,
  PhantomRegularRedButton,
} from "../styles/styles";
import { updateLanguage } from "../utils/utilities";

const FaqIndex: React.FC<IQueryData> = ({
  data,
  pageContext,
}: IQueryData): JSX.Element => {
  const { language } = pageContext;
  void updateLanguage(language);

  const { banner, slug, subtext, subtitle, title } =
    data.markdownRemark.frontmatter;

  const hasBanner = typeof banner === "string";
  const sanitizedHTML = useMemo((): string => {
    const { html } = data.markdownRemark;

    return sanitizeHtml(html, {
      allowedClasses: {
        "*": false,
      },
    });
  }, [data.markdownRemark]);
  const questions = useMemo(
    (): string[] => sanitizedHTML.split("</div>"),
    [sanitizedHTML],
  );
  const questionsPerPage = 10;

  const [questionsToShow, setQuestionsToShow] = useState<string[]>([]);
  const [next, setNext] = useState(questionsPerPage);

  const loopWithSlice = useCallback(
    (start: number, end: number): void => {
      const slicedPosts = questions.slice(start, end);
      setQuestionsToShow(slicedPosts);
    },
    [questions],
  );

  useEffect((): void => {
    loopWithSlice(0, questionsPerPage);
  }, [loopWithSlice]);

  const handleShowMorePosts = useCallback((): void => {
    loopWithSlice(0, next + questionsPerPage);
    setNext((previousState): number => previousState + questionsPerPage);
  }, [next, questionsPerPage, loopWithSlice]);

  return (
    <WebsiteWrapper>
      <PageArticle $bgColor={"#f4f4f6"}>
        <PageHeader
          banner={banner}
          pageWithBanner={hasBanner}
          slug={slug}
          subtext={subtext}
          subtitle={subtitle}
          title={decode(title)}
        />
        <FaqContainer>
          {questionsToShow.map((question): JSX.Element => {
            return <div key={question}>{parse(question)}</div>;
          })}
          <CenteredSpacedContainer>
            <PhantomRegularRedButton
              className={"w-50-ns w-100"}
              onClick={handleShowMorePosts}
            >
              {"Show more"}
            </PhantomRegularRedButton>
          </CenteredSpacedContainer>
        </FaqContainer>
      </PageArticle>
    </WebsiteWrapper>
  );
};

export const Head = ({ data }: IQueryData): JSX.Element => {
  const { description, keywords, slug } = data.markdownRemark.frontmatter;

  return (
    <Seo
      description={description}
      image={
        "https://res.cloudinary.com/fluid-attacks/image/upload/v1669230787/airs/logo-fluid-2022"
      }
      keywords={keywords}
      path={slug}
      title={"FAQ | Fluid Attacks"}
    />
  );
};

export const query: StaticQueryDocument = graphql`
  query FaqIndex($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      fields {
        slug
      }
      frontmatter {
        banner
        description
        keywords
        slug
        subtext
        subtitle
        title
      }
    }
  }
`;

export default FaqIndex;
