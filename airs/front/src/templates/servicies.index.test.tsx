import "@testing-library/jest-dom";
import { render, waitFor } from "@testing-library/react";
import React from "react";

import ServicesIndex from "./servicesTemplate";
import ContinuousHackingIndex from "./serviceTemplate";

import { exampleQueryData } from "test-utils/mocks";

describe("services", (): void => {
  describe("servicesTemplate", (): void => {
    it("ServicesIndex should render mocked data", async (): Promise<void> => {
      expect.hasAssertions();

      const { container } = render(
        <ServicesIndex
          data={exampleQueryData.data}
          pageContext={exampleQueryData.pageContext}
        />,
      );
      await waitFor((): void => {
        expect(container).toBeInTheDocument();
      });
    });
    it("ServicesIndex should render mocked data with null slug", async (): Promise<void> => {
      expect.hasAssertions();

      const modifiedData = {
        ...exampleQueryData,
        data: {
          ...exampleQueryData.data,
          markdownRemark: {
            ...exampleQueryData.data.markdownRemark,
            frontmatter: {
              ...exampleQueryData.data.markdownRemark.frontmatter,
            },
            slug: "",
          },
        },
      };
      const { container } = render(
        <ServicesIndex
          data={modifiedData.data}
          pageContext={exampleQueryData.pageContext}
        />,
      );
      await waitFor((): void => {
        expect(container).toBeInTheDocument();
      });
    });
  });

  describe("serviceTemplate", (): void => {
    it("ContinuousHackingIndex should render mocked data", (): void => {
      expect.hasAssertions();

      const { container } = render(
        <ContinuousHackingIndex
          data={exampleQueryData.data}
          pageContext={exampleQueryData.pageContext}
        />,
      );

      expect(container).toBeInTheDocument();
    });
  });
  it("ContinuousHackingIndex should render mocked data without headtitle", (): void => {
    expect.hasAssertions();

    const modifiedData = {
      ...exampleQueryData,
      data: {
        ...exampleQueryData.data,
        markdownRemark: {
          ...exampleQueryData.data.markdownRemark,
          frontmatter: {
            ...exampleQueryData.data.markdownRemark.frontmatter,
            headtitle: "",
          },
        },
      },
    };
    const { container } = render(
      <ContinuousHackingIndex
        data={modifiedData.data}
        pageContext={exampleQueryData.pageContext}
      />,
    );

    expect(container).toBeInTheDocument();
  });
});
