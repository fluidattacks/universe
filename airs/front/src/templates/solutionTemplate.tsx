import { graphql } from "gatsby";
import type { StaticQueryDocument } from "gatsby";
import React from "react";

import { Seo } from "../components/Seo";
import { SolutionPage } from "../scenes/SolutionPage";
import { WebsiteWrapper } from "../scenes/WebsiteWrapper";
import { PageArticle } from "../styles/styles";
import { updateLanguage } from "../utils/utilities";

const SolutionIndex: React.FC<IQueryData> = ({
  data,
  pageContext,
}: IQueryData): JSX.Element => {
  const { language } = pageContext;
  void updateLanguage(language);

  const { description, image, title } = data.markdownRemark.frontmatter;
  const { html } = data.markdownRemark;

  return (
    <WebsiteWrapper>
      <PageArticle $bgColor={"#f4f4f6"}>
        <SolutionPage
          description={description}
          html={html}
          image={image}
          title={title}
        />
      </PageArticle>
    </WebsiteWrapper>
  );
};

export const Head = ({ data }: IQueryData): JSX.Element => {
  const { description, headtitle, identifier, image, keywords, slug } =
    data.markdownRemark.frontmatter;

  return (
    <Seo
      description={description}
      image={image.replace(".webp", "")}
      keywords={keywords}
      path={slug}
      title={
        headtitle
          ? `${headtitle} | Solutions | Fluid Attacks`
          : `${identifier} | Solutions | Fluid Attacks`
      }
      type={"service"}
    />
  );
};

export const query: StaticQueryDocument = graphql`
  query SolutionIndex($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      fields {
        slug
      }
      frontmatter {
        description
        keywords
        slug
        identifier
        image
        title
        headtitle
      }
    }
  }
`;

export default SolutionIndex;
