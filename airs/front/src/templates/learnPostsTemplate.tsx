import { graphql } from "gatsby";
import type { StaticQueryDocument } from "gatsby";
import { decode } from "he";
import React from "react";

import { Seo } from "../components/Seo";
import { PostPage } from "../scenes/LearnPostPage";
import { WebsiteWrapper } from "../scenes/WebsiteWrapper";
import { PageArticle } from "../styles/styles";
import { useBlogsDate } from "../utils/hooks/useSafeDate";
import { updateLanguage } from "../utils/utilities";

const LearnPostsIndex: React.FC<IQueryData> = ({
  data,
  pageContext,
}: IQueryData): JSX.Element => {
  const { language } = pageContext;
  void updateLanguage(language);

  const { html } = data.markdownRemark;

  const {
    author,
    category,
    date,
    definition,
    image,
    slug,
    subtitle,
    title,
    writer,
  } = data.markdownRemark.frontmatter;
  const publishedDate = useBlogsDate(date, language);

  return (
    <WebsiteWrapper>
      <PageArticle $bgColor={"#fff"}>
        <PostPage
          author={author}
          category={category}
          date={publishedDate}
          description={definition}
          html={html}
          image={image}
          slug={slug}
          subtitle={subtitle}
          title={title}
          writer={writer}
        />
      </PageArticle>
    </WebsiteWrapper>
  );
};

export const Head = ({ pageContext, data }: IQueryData): JSX.Element => {
  const { language } = pageContext;
  const {
    author,
    date,
    description,
    headtitle,
    image,
    keywords,
    modified,
    slug,
    title,
  } = data.markdownRemark.frontmatter;
  const publishedDate = useBlogsDate(date, language);
  const modifiedDate = useBlogsDate(modified ? modified : date, language);
  const languagePrefix = language === "es" ? "es/" : "";
  const formatedSlug = slug.startsWith("learn/")
    ? slug.split("/").slice(1).join("/")
    : slug;

  return (
    <Seo
      author={author}
      date={publishedDate}
      dateModified={modifiedDate}
      description={description}
      image={image.replace(".webp", ".png")}
      keywords={keywords}
      path={`https://fluidattacks.com/${languagePrefix}learn/${formatedSlug}`}
      title={
        headtitle
          ? `${decode(headtitle)} | Learn | Fluid Attacks`
          : `${decode(title)} | Learn | Fluid Attacks`
      }
      type={"post"}
    />
  );
};

export const query: StaticQueryDocument = graphql`
  query LearnPostsPages($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      fields {
        slug
      }
      frontmatter {
        author
        category
        date
        definition
        modified
        description
        image
        keywords
        slug
        subtitle
        tags
        writer
        title
        headtitle
      }
    }
  }
`;

export default LearnPostsIndex;
