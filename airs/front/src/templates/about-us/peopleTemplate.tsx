/* eslint react/forbid-component-props: 0 */
import { graphql } from "gatsby";
import type { StaticQueryDocument } from "gatsby";
import React from "react";

import { PeopleSectionList } from "../../components/PeopleSectionsList";
import { Seo } from "../../components/Seo";
import { WebsiteWrapper } from "../../scenes/WebsiteWrapper";
import {
  ArticleContainer,
  BannerContainer,
  BannerTitle,
  FullWidthContainer,
  PageArticle,
} from "../../styles/styles";
import { updateLanguage } from "../../utils/utilities";

const PeopleIndex: React.FC<IQueryData> = ({
  data,
  pageContext,
}: IQueryData): JSX.Element => {
  const { language } = pageContext;
  void updateLanguage(language);

  const { banner, title } = data.markdownRemark.frontmatter;

  return (
    <WebsiteWrapper>
      <PageArticle $bgColor={"#f9f9f9"}>
        <BannerContainer className={banner}>
          <FullWidthContainer>
            <BannerTitle>{title}</BannerTitle>
          </FullWidthContainer>
        </BannerContainer>
        <ArticleContainer>
          <PeopleSectionList />
        </ArticleContainer>
      </PageArticle>
    </WebsiteWrapper>
  );
};

export const Head = ({ data }: IQueryData): JSX.Element => {
  const { description, keywords, slug, title } =
    data.markdownRemark.frontmatter;

  return (
    <Seo
      description={description}
      image={
        "https://res.cloudinary.com/fluid-attacks/image/upload/v1619632545/airs/about-us/people/cover-people_lxsx5t"
      }
      keywords={keywords}
      path={slug}
      title={`${title} | Fluid Attacks`}
    />
  );
};

export const query: StaticQueryDocument = graphql`
  query PeoplePage($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      fields {
        slug
      }
      frontmatter {
        banner
        description
        keywords
        slug
        title
      }
    }
  }
`;

export default PeopleIndex;
