/* eslint react/forbid-component-props: 0 */
import { graphql } from "gatsby";
import type { StaticQueryDocument } from "gatsby";
import React from "react";

import { AirsLink } from "../components/AirsLink";
import { CloudImage } from "../components/CloudImage";
import { Seo } from "../components/Seo";
import { Text, Title } from "../components/Typography";
import { WebsiteWrapper } from "../scenes/WebsiteWrapper";
import {
  FlexCenterItemsContainer,
  PageArticle,
  PageContainer,
  PhantomRegularRedButton,
  SystemsCardContainer,
} from "../styles/styles";
import { translate } from "../utils/translations/translate";
import { updateLanguage } from "../utils/utilities";

const CategoriesIndex: React.FC<IQueryData> = ({
  data,
  pageContext,
}: IQueryData): JSX.Element => {
  const { language } = pageContext;
  void updateLanguage(language);

  const { title } = data.markdownRemark.frontmatter;

  const categoryData = [
    {
      image: "aspm",
      link: "/product/aspm/",
      paragraph: translate.t("categories.aspm.paragraph"),
      title: translate.t("categories.aspm.subtitle"),
    },
    {
      image: "sast",
      link: "/product/sast/",
      paragraph: translate.t("categories.sast.paragraph"),
      title: translate.t("categories.sast.subtitle"),
    },
    {
      image: "dast",
      link: "/product/dast/",
      paragraph: translate.t("categories.dast.paragraph"),
      title: translate.t("categories.dast.subtitle"),
    },
    {
      image: "mpt",
      link: "/product/mpt/",
      paragraph: translate.t("categories.mpt.paragraph"),
      title: translate.t("categories.mpt.subtitle"),
    },
    {
      image: "sca",
      link: "/product/sca/",
      paragraph: translate.t("categories.sca.paragraph"),
      title: translate.t("categories.sca.subtitle"),
    },
    {
      image: "cspm",
      link: "/product/cspm/",
      paragraph: translate.t("categories.cspm.paragraph"),
      title: translate.t("categories.cspm.subtitle"),
    },
    {
      image: "re",
      link: "/product/re/",
      paragraph: translate.t("categories.re.paragraph"),
      title: translate.t("categories.re.subtitle"),
    },
    {
      image: "ptaas",
      link: "/product/ptaas/",
      paragraph: translate.t("categories.ptaas.paragraph"),
      title: translate.t("categories.ptaas.subtitle"),
    },
    {
      image: "mast",
      link: "/product/mast/",
      paragraph: translate.t("categories.mast.paragraph"),
      title: translate.t("categories.mast.subtitle"),
    },
  ];

  return (
    <WebsiteWrapper>
      <PageArticle $bgColor={"#f4f4f6"}>
        <FlexCenterItemsContainer>
          <Title color={"#2e2e38"} level={1} mt={4} size={"big"}>
            {title}
          </Title>
        </FlexCenterItemsContainer>
        <PageContainer className={"flex flex-wrap"}>
          {categoryData.map(
            (categoryCard): JSX.Element => (
              <SystemsCardContainer key={categoryCard.title}>
                <CloudImage
                  alt={title}
                  src={`airs/product/${categoryCard.image}-card`}
                  styles={"w-100"}
                />
                <div className={"poppins"}>
                  <Title
                    color={"#2e2e38"}
                    level={3}
                    mb={1}
                    mt={1}
                    size={"small"}
                  >
                    {categoryCard.title}
                  </Title>
                </div>
                <Text color={"#5c5c70"} mb={3} size={"medium"}>
                  {categoryCard.paragraph}
                </Text>
                <AirsLink href={categoryCard.link}>
                  <PhantomRegularRedButton>
                    {language === "en" ? "Go to product" : "Ir al producto"}
                  </PhantomRegularRedButton>
                </AirsLink>
              </SystemsCardContainer>
            ),
          )}
        </PageContainer>
      </PageArticle>
    </WebsiteWrapper>
  );
};

export const Head = ({ data }: IQueryData): JSX.Element => {
  const { description, keywords, slug, title } =
    data.markdownRemark.frontmatter;

  return (
    <Seo
      description={description}
      image={
        "https://res.cloudinary.com/fluid-attacks/image/upload/v1619630822/airs/solutions/bg-solutions_ylz99o"
      }
      keywords={keywords}
      path={slug}
      title={`${title} | Fluid Attacks`}
    />
  );
};

export const query: StaticQueryDocument = graphql`
  query CategoriesIndex($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      frontmatter {
        description
        keywords
        slug
        title
      }
    }
  }
`;

export default CategoriesIndex;
