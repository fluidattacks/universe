import { graphql } from "gatsby";
import type { StaticQueryDocument } from "gatsby";
import React from "react";

import { ResourcesPage } from "../components/ResourcesPage";
import { Seo } from "../components/Seo";
import { WebsiteWrapper } from "../scenes/WebsiteWrapper";
import { updateLanguage } from "../utils/utilities";

const ResourcesIndex: React.FC<IQueryData> = ({
  data,
  pageContext,
}: IQueryData): JSX.Element => {
  const { language } = pageContext;
  void updateLanguage(language);

  const { title } = data.markdownRemark.frontmatter;

  return (
    <WebsiteWrapper>
      <ResourcesPage bannerTitle={title} />
    </WebsiteWrapper>
  );
};

export const Head = ({ data }: IQueryData): JSX.Element => {
  const { description, keywords, slug, title } =
    data.markdownRemark.frontmatter;

  return (
    <Seo
      description={description}
      image={
        "https://res.cloudinary.com/fluid-attacks/image/upload/v1619632049/airs/resources/resources-main_u1gggc"
      }
      keywords={keywords}
      path={slug}
      title={`${title} | Fluid Attacks`}
    />
  );
};

export const query: StaticQueryDocument = graphql`
  query ResourcesIndex($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      fields {
        slug
      }
      frontmatter {
        description
        keywords
        phrase
        slug
        title
      }
    }
  }
`;

export default ResourcesIndex;
