import "@testing-library/jest-dom";
import { render } from "@testing-library/react";
import React from "react";

import SystemsIndex from "./systemsTemplate";
import SystemIndex from "./systemTemplate";

import { exampleQueryData } from "test-utils/mocks";

describe("system", (): void => {
  describe("systemTemplate", (): void => {
    it("SystemIndex should render mocked data", (): void => {
      expect.hasAssertions();

      const { container } = render(
        <SystemIndex
          data={exampleQueryData.data}
          pageContext={exampleQueryData.pageContext}
        />,
      );

      expect(container).toBeInTheDocument();
    });
  });

  describe("systemsTemplate", (): void => {
    it("SystemsIndex should render mocked data", (): void => {
      expect.hasAssertions();

      const { container } = render(
        <SystemsIndex
          data={exampleQueryData.data}
          pageContext={exampleQueryData.pageContext}
        />,
      );

      expect(container).toBeInTheDocument();
    });
    it("SystemsIndex should render mocked data without headtitle", (): void => {
      expect.hasAssertions();

      const modifiedData = {
        ...exampleQueryData,
        data: {
          ...exampleQueryData.data,
          markdownRemark: {
            ...exampleQueryData.data.markdownRemark,
            frontmatter: {
              ...exampleQueryData.data.markdownRemark.frontmatter,
              headtitle: "",
            },
          },
        },
      };
      const { container } = render(
        <SystemIndex
          data={modifiedData.data}
          pageContext={modifiedData.pageContext}
        />,
      );

      expect(container).toBeInTheDocument();
    });
  });
});
