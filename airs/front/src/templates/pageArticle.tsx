import { graphql } from "gatsby";
import type { StaticQueryDocument } from "gatsby";
import { decode } from "he";
import parse from "html-react-parser";
import React from "react";
import sanitizeHtml from "sanitize-html";

import { PageHeader } from "../components/PageHeader";
import { Seo } from "../components/Seo";
import { WebsiteWrapper } from "../scenes/WebsiteWrapper";
import { ArticleContainer, ArticleTitle, PageArticle } from "../styles/styles";
import { updateLanguage } from "../utils/utilities";

const MdDefaultPage: React.FC<IQueryData> = ({
  data,
  pageContext,
}: IQueryData): JSX.Element => {
  const { language } = pageContext;
  void updateLanguage(language);

  const { banner, slug, subtext, subtitle, title } =
    data.markdownRemark.frontmatter;
  const { html } = data.markdownRemark;
  const sanitizedHTML = sanitizeHtml(html, {
    allowedClasses: {
      "*": false,
    },
  });

  const hasBanner: boolean = typeof banner === "string";
  const isCareers: boolean = slug === "careers/";

  return (
    <WebsiteWrapper>
      <PageArticle $bgColor={"#f9f9f9"}>
        <PageHeader
          banner={banner}
          pageWithBanner={hasBanner}
          slug={slug}
          subtext={subtext}
          subtitle={subtitle}
          title={decode(title)}
        />
        {isCareers ? <ArticleTitle>{decode(title)}</ArticleTitle> : undefined}
        {/* eslint-disable-next-line react/forbid-component-props */}
        <ArticleContainer className={"internal"}>
          {parse(sanitizedHTML)}
        </ArticleContainer>
      </PageArticle>
    </WebsiteWrapper>
  );
};

export const Head = ({ data }: IQueryData): JSX.Element => {
  const { description, keywords, slug, title } =
    data.markdownRemark.frontmatter;
  const seoTitle: string = slug === "cookie/" ? "Política de cookies" : title;

  return (
    <Seo
      description={description}
      image={
        "https://res.cloudinary.com/fluid-attacks/image/upload/v1669230787/airs/logo-fluid-2022"
      }
      keywords={keywords}
      path={slug}
      title={`${seoTitle} | Fluid Attacks`}
    />
  );
};

export const query: StaticQueryDocument = graphql`
  query PageArticleBySlug($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      fields {
        slug
      }
      frontmatter {
        banner
        description
        keywords
        slug
        subtext
        title
        subtitle
      }
    }
  }
`;

export default MdDefaultPage;
