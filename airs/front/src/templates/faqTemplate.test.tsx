import { render, screen } from "@testing-library/react";
import React from "react";

import FaqIndex from "./faqTemplate";

import { exampleQueryData } from "test-utils/mocks";

describe("FaqIndex", (): void => {
  it("should render", (): void => {
    expect.hasAssertions();
    render(
      <FaqIndex
        data={exampleQueryData.data}
        pageContext={exampleQueryData.pageContext}
      />,
    );
    expect(screen.getByRole("article")).toBeInTheDocument();
    const questions = document.querySelectorAll(".faq-container")[0].children;
    expect(questions).toHaveLength(3);
    expect(questions[0].children).toHaveLength(1);
    expect(
      questions[0].children[0].getElementsByTagName("h3")[0].textContent,
    ).toBe("Hello, World!");
    expect(
      questions[0].children[0].getElementsByTagName("p")[0].textContent,
    ).toBe("This is a paragraph.");
  });
});
