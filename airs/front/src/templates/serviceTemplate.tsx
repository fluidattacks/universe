import { graphql } from "gatsby";
import type { StaticQueryDocument } from "gatsby";
import React from "react";

import { Seo } from "../components/Seo";
import { ContinuousHackingPage } from "../scenes/ContinuousHackingPage";
import { WebsiteWrapper } from "../scenes/WebsiteWrapper";
import { PageArticle } from "../styles/styles";
import { updateLanguage } from "../utils/utilities";

const ContinuousHackingIndex: React.FC<IQueryData> = ({
  pageContext,
}: IQueryData): JSX.Element => {
  const { language } = pageContext;
  void updateLanguage(language);

  return (
    <WebsiteWrapper>
      <PageArticle $bgColor={"#f9f9f9"}>
        <ContinuousHackingPage language={language} />
      </PageArticle>
    </WebsiteWrapper>
  );
};

export const Head = ({ data, pageContext }: IQueryData): JSX.Element => {
  const { description, headtitle, image, keywords, slug, title } =
    data.markdownRemark.frontmatter;
  const { language } = pageContext;
  const translatedTitle = language === "en" ? "Services" : "Servicios";

  return (
    <Seo
      description={description}
      image={image.replace(".webp", "")}
      keywords={keywords}
      path={slug}
      title={
        headtitle
          ? `${headtitle} | ${translatedTitle} | Fluid Attacks`
          : `${title} | ${translatedTitle} | Fluid Attacks`
      }
      type={"service"}
    />
  );
};

export const query: StaticQueryDocument = graphql`
  query ServiceIndex($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      fields {
        slug
      }
      frontmatter {
        description
        keywords
        slug
        image
        title
        headtitle
      }
    }
  }
`;

export default ContinuousHackingIndex;
