const esModules = [
  "react-markdown",
  "unified",
  "micromark",
  "remark-.+",
  "space-separated-tokens",
  "decode-named-character-reference",
  "trim-lines",
  "rehype-highlight",
  "hast-util-to-text",
  "unist-util-find-after",
  "lowlight",
  "devlop",
  "rehype-parse",
  "rehype-react",
  "hast-util-from-html",
  "hast-util-to-jsx-runtime",
  "estree-util-is-identifier-name",
  "html-url-attributes",
  "mdast-util-to-hast",
].join("|");

module.exports = {
  bail: 1,
  collectCoverage: true,
  maxWorkers: 1,
  coverageDirectory: ".coverage",
  coverageReporters: ["text", "lcov"],
  coverageThreshold: {
    global: {
      statements: 90.85,
      lines: 90.85,
    },
  },
  collectCoverageFrom: ["src/**/*.{ts,tsx}", "!src/**/*.stories.{ts,tsx}"],
  globals: {
    __PATH_PREFIX__: ``,
  },
  moduleFileExtensions: ["ts", "tsx", "js"],
  moduleDirectories: ["node_modules", "src"],
  moduleNameMapper: {
    "\\.(css)$": "identity-obj-proxy",
  },
  setupFiles: [
    "<rootDir>/__mocks__/loadershim.js",
    "<rootDir>/__mocks__/gatsby.js",
    "<rootDir>/__mocks__/react-instantsearch.js",
  ],
  setupFilesAfterEnv: [
    "<rootDir>/__mocks__/setup.ts",
    "<rootDir>/jest-setup.ts",
  ],
  testEnvironment: "jest-environment-jsdom",
  testRegex: "(/__tests__/.*|(\\.|/)(test|spec))\\.tsx?$",
  transform: {
    "^.+\\.tsx?$": "ts-jest",
    "\\.(css)$": "<rootDir>/jest-css-transform.js",
    [`(${esModules}).*\\.js$`]: "<rootDir>/jest-preprocess.js",
  },
  transformIgnorePatterns: [`node_modules/(?!(${esModules}))/`],
};
