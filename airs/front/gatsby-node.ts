import path from "path";

import type { GatsbyNode } from "gatsby";
import { createFilePath } from "gatsby-source-filesystem";

interface IQueryResult {
  allMarkdownRemark: {
    edges: [
      {
        node: {
          id: string;
          fields: {
            slug: string;
          };
          frontmatter: {
            author: string;
            category: string;
            slug: string;
            tags: string;
            template: string | null;
          };
        };
      },
    ];
  };
}

const defaultTemplate = path.resolve("./src/templates/pageArticle.tsx");
const blogsTemplate = path.resolve("./src/templates/blogsTemplate.tsx");
const learnPostsTemplate = path.resolve(
  "./src/templates/learnPostsTemplate.tsx",
);

const setTemplate = (template: string): string =>
  path.resolve(`./src/templates/${template}Template.tsx`);

const stringToUri = (word: string): string => {
  return word
    .toLowerCase()
    .replaceAll(/\s+/gu, "-")
    .normalize("NFD")
    .replace(/[\u0300-\u036f]/gu, "");
};

const createPages: GatsbyNode["createPages"] = async ({
  graphql,
  actions,
}): Promise<void> => {
  const { createPage } = actions;

  await graphql<IQueryResult>(`
    {
      allMarkdownRemark(limit: 2000) {
        edges {
          node {
            id
            fields {
              slug
            }
            frontmatter {
              slug
              tags
              category
              author
              template
            }
          }
        }
      }
    }
  `).then((value): void => {
    const posts = value.data?.allMarkdownRemark.edges;

    posts?.forEach((post): void => {
      if (post.node.fields.slug.startsWith("/pages/")) {
        if (post.node.frontmatter.template === null) {
          createPage({
            component: defaultTemplate,
            context: {
              id: post.node.id,
              language: "en",
              slug: `/pages/${stringToUri(post.node.frontmatter.slug)}`,
            },
            path: stringToUri(post.node.frontmatter.slug),
          });
        } else {
          createPage({
            component: setTemplate(post.node.frontmatter.template),
            context: {
              id: post.node.id,
              language: "en",
              slug: stringToUri(post.node.fields.slug),
            },
            path: stringToUri(post.node.frontmatter.slug),
          });
        }
      } else if (post.node.fields.slug.startsWith("/blog/")) {
        createPage({
          component: blogsTemplate,
          context: {
            id: post.node.id,
            language: "en",
            slug: `/blog/${stringToUri(post.node.frontmatter.slug)}`,
          },
          path: `/blog/${stringToUri(post.node.frontmatter.slug)}`,
        });
      } else if (post.node.fields.slug.startsWith("/learn/")) {
        createPage({
          component: learnPostsTemplate,
          context: {
            id: post.node.id,
            language: "en",
            slug: `/learn/${stringToUri(post.node.frontmatter.slug)}`,
          },
          path: `/learn/${stringToUri(post.node.frontmatter.slug)}`,
        });
      } else if (post.node.fields.slug.startsWith("/es/pages/")) {
        if (post.node.frontmatter.template === null) {
          createPage({
            component: defaultTemplate,
            context: {
              id: post.node.id,
              language: "es",
              slug: `/es/pages/${stringToUri(post.node.frontmatter.slug)}`,
            },
            path: `es/${stringToUri(post.node.frontmatter.slug)}`,
          });
        } else {
          createPage({
            component: setTemplate(post.node.frontmatter.template),
            context: {
              id: post.node.id,
              language: "es",
              slug: `/es/pages/${stringToUri(post.node.frontmatter.slug)}`,
            },
            path: `es/${stringToUri(post.node.frontmatter.slug)}`,
          });
        }
      } else if (post.node.fields.slug.startsWith("/es/blog/")) {
        createPage({
          component: blogsTemplate,
          context: {
            id: post.node.id,
            language: "es",
            slug: `/es/${stringToUri(post.node.frontmatter.slug)}`,
          },
          path: `es/${stringToUri(post.node.frontmatter.slug)}`,
        });
      } else if (post.node.fields.slug.startsWith("/es/learn/")) {
        createPage({
          component: learnPostsTemplate,
          context: {
            id: post.node.id,
            language: "es",
            slug: `/es/${stringToUri(post.node.frontmatter.slug)}`,
          },
          path: `es/${stringToUri(post.node.frontmatter.slug)}`,
        });
      }
    });

    /**
     * Create Tag Pages
     */
    const tagTemplate = path.resolve("./src/templates/blogTagTemplate.tsx");
    const tags = (posts || [])
      .filter(
        (post): boolean =>
          post.node.fields.slug.startsWith("/blog/") &&
          post.node.frontmatter.tags !== "",
      )
      .map((post): string[] => post.node.frontmatter.tags.split(", "));
    const spanishTags = (posts || [])
      .filter(
        (post): boolean =>
          post.node.fields.slug.startsWith("/es/blog/") &&
          post.node.frontmatter.tags !== "",
      )
      .map((post): string[] => post.node.frontmatter.tags.split(", "));

    const tagsList = tags.flat();
    const spanishTagsList = new Set(spanishTags.flat());

    spanishTagsList.forEach((tagName): void => {
      const tagUri = stringToUri(tagName);
      createPage({
        component: tagTemplate,
        context: {
          language: "es",
          tagName,
          tagUri,
        },
        path: `es/blog/tags/${tagUri}`,
      });
    });

    tagsList.forEach((tagName): void => {
      const tagUri = stringToUri(tagName);
      createPage({
        component: tagTemplate,
        context: {
          language: "en",
          tagName,
          tagUri,
        },
        path: `blog/tags/${tagUri}`,
      });
    });

    /**
     * Create Author Pages
     */

    const authorTemplate = path.resolve(
      "./src/templates/blogAuthorTemplate.tsx",
    );
    const authors = (posts || [])
      .filter(
        (post): boolean =>
          post.node.fields.slug.startsWith("/blog/") &&
          post.node.frontmatter.author !== "",
      )
      .map((post): string => post.node.frontmatter.author);

    const authorsList = new Set(authors.flat());

    authorsList.forEach((authorName): void => {
      const authorUri = stringToUri(authorName);
      createPage({
        component: authorTemplate,
        context: {
          authorName,
          authorUri,
          language: "en",
        },
        path: `blog/authors/${authorUri}`,
      });
    });

    /**
     * Create Categories Pages
     */
    const categoryTemplate = path.resolve(
      `./src/templates/blogCategoryTemplate.tsx`,
    );
    const categories = (posts || [])
      .filter(
        (post): boolean =>
          post.node.fields.slug.startsWith("/blog/") &&
          post.node.frontmatter.category !== "",
      )
      .map((post): string => post.node.frontmatter.category);
    const spanishCategories = (posts || [])
      .filter(
        (post): boolean =>
          post.node.fields.slug.startsWith("/es/blog/") &&
          post.node.frontmatter.category !== "",
      )
      .map((post): string => post.node.frontmatter.category);

    const categoriesList = new Set(categories.flat());
    const spanishCategoriesList = new Set(spanishCategories.flat());

    categoriesList.forEach((categoryName): void => {
      const categoryUri = stringToUri(categoryName);
      createPage({
        component: categoryTemplate,
        context: {
          categoryName,
          categoryUri,
          language: "en",
        },
        path: `blog/categories/${categoryUri}`,
      });
    });
    spanishCategoriesList.forEach((categoryName): void => {
      const categoryUri = stringToUri(categoryName);
      createPage({
        component: categoryTemplate,
        context: {
          categoryName,
          categoryUri,
          language: "es",
        },
        path: `es/blog/categorias/${categoryUri}`,
      });
    });
  });
};

const onCreateNode: GatsbyNode["onCreateNode"] = ({
  node,
  actions,
  getNode,
}): void => {
  const { createNodeField } = actions;

  if (node.internal.type === `MarkdownRemark`) {
    const value = createFilePath({ getNode, node });
    createNodeField({
      name: `slug`,
      node,
      value,
    });
  }
};

const onCreatePage: GatsbyNode["onCreatePage"] = ({ page, actions }): void => {
  const { createPage, deletePage } = actions;
  const newPage = { ...page, context: { language: "en" } };
  const spanishHome = { ...page, context: { language: "es" }, path: `/es/` };
  const spanishBlog = {
    ...page,
    context: { language: "es" },
    path: `/es/blog/`,
  };
  const spanishLearn = {
    ...page,
    context: { language: "es" },
    path: `/es/learn/`,
  };
  const spanishBlogTags = {
    ...page,
    context: { language: "es" },
    path: `/es/blog/tags/`,
  };
  const spanishBlogCategories = {
    ...page,
    context: { language: "es" },
    path: `/es/blog/categorias/`,
  };

  if (page.path === "/") {
    createPage(spanishHome);
  }

  if (page.path === "/blog/") {
    createPage(spanishBlog);
  }
  if (page.path === "/learn/") {
    createPage(spanishLearn);
  }
  if (page.path === "/blog/tags/") {
    createPage(spanishBlogTags);
  }
  if (page.path === "/blog/categories/") {
    createPage(spanishBlogCategories);
  }

  if (
    page.path === "/" ||
    page.path === "/learn/" ||
    page.path === "/blog/" ||
    page.path === "/blog/authors/" ||
    page.path === "/blog/categories/" ||
    page.path === "/blog/tags/"
  ) {
    deletePage(page);
    createPage(newPage);
  }
};

const onCreateWebpackConfig: GatsbyNode["onCreateWebpackConfig"] = ({
  actions,
}): void => {
  actions.setWebpackConfig({
    devtool: "source-map",
  });
};

export { createPages, onCreateNode, onCreatePage, onCreateWebpackConfig };
