const css = require('identity-obj-proxy');

module.exports = {
  process(src, filename, config, options) {
    return {
      code: `module.exports = ${JSON.stringify(css)};`,
    }
  },
};
