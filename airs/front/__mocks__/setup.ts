/* eslint-disable @typescript-eslint/no-unsafe-return */
import "@testing-library/jest-dom";
import { configure } from "@testing-library/react";

const newMaxTime = 30000;
jest.setTimeout(newMaxTime);
configure({ asyncUtilTimeout: 30000 });

jest.mock("i18next", (): void => {
  const i18next = jest.requireActual("i18next");

  return {
    ...i18next,
    language: "en",
    t: (str: string): string => str,
  };
});

jest.mock("@auth0/auth0-react", (): void => {
  const auth = jest.requireActual("@auth0/auth0-react");

  return {
    ...auth,
    useAuth0: jest.fn(),
  };
});

jest.mock("@bugsnag/js", (): void => {
  const bugsnag = jest.requireActual("@bugsnag/js");

  return {
    ...bugsnag,
    notify: jest.fn(),
  };
});
