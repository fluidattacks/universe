---
slug: advisories/mccartney/
title: Online Movie Ticket Booking System v1.0 - Stored Cross-Site Scripting (XSS)
authors: Andres Roldan
writer: aroldan
codename: mccartney
product: Online Movie Ticket Booking System v1.0
date: 2023-09-28 12:00 COT
cveid: CVE-2023-44174
severity: 6.4
description: Online Movie Ticket Booking System v1.0 - Stored Cross-Site Scripting (XSS)
keywords: Fluid Attacks, Security, Vulnerabilities, XSS, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

<summary-table
    name="Online Movie Ticket Booking System v1.0 - Stored Cross-Site Scripting (XSS)"
    code="[McCartney](https://en.wikipedia.org/wiki/Paul_McCartney)"
    product="Online Movie Ticket Booking System"
    vendor="Projectworlds Pvt. Limited"
    affected-versions="Version 1.0"
    fixed-versions=""
    state="Public"
    release="2023-09-28">
</summary-table>

## Vulnerabilities

<vulnerability-table
    kind="Stored Cross-Site Scripting (XSS)"
    rule="[010. Stored cross-site scripting](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-010/)"
    remote="Yes"
    vector="CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:C/C:L/I:L/A:N"
    score="6.4"
    available="Yes"
    id="[CVE-2023-44174](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-44174)">
</vulnerability-table>

## Description

Online Movie Ticket Booking System v1.0 is vulnerable to
an authenticated Stored Cross-Site Scripting vulnerability.

## Vulnerability

The 'amount' parameter of the process_booking.php resource
is copied into the bank.php document as plain text
between tags. Any input is echoed unmodified in the
bank.php response. The vulnerable
code is:

process_booking.php:

```php
session_start();
extract($_POST);
include('config.php');
$_SESSION['screen']=$screen;
$_SESSION['seats']=$seats;
$_SESSION['amount']=$amount;
$_SESSION['date']=$date;
header('location:bank.php');
```

bank.php:

```html
  <dl class="mercDetails">
      <dt>Merchant</dt> <dd>Shop Street</dd>
    <dt>Transaction Amount</dt> <dd>INR <?php echo  $_SESSION['amount'];?></dd>
    <dt>Debit Card</dt> <dd><?php echo  $number;?></%></dd>
  </dl>
```

## Our security policy

We have reserved the ID CVE-2023-44174 to refer
to this issues from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Online Movie Ticket Booking System v1.0
* Operating System: Any

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://projectworlds.in/>

## Timeline

<time-lapse
discovered="2023-09-26"
contacted="2023-09-26"
disclosure="2023-09-28">
</time-lapse>
