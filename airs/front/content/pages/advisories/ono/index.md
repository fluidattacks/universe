---
slug: advisories/ono/
title: Online Art Gallery v1.0 - Multiple Unauthenticated SQL Injections (SQLi)
authors: Andres Roldan
writer: aroldan
codename: Ono
product: Online Art Gallery v1.0
date: 2023-10-26 12:00 COT
cveid: CVE-2023-44267
severity: 9.8
description: Online Art Gallery v1.0 - Multiple Unauthenticated SQL Injections (SQLi)
keywords: Fluid Attacks, Security, Vulnerabilities, SQLi, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | Online Art Gallery v1.0 - Multiple Unauthenticated SQL Injections (SQLi) |
| **Code name** | [Ono](https://es.wikipedia.org/wiki/Yoko_Ono) |
| **Product** | Online Art Gallery |
| **Vendor** | Projectworlds Pvt. Limited |
| **Affected versions** | Version 1.0 |
| **State** | Public |
| **Release date** | 2023-10-26 |

## Vulnerabilities

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Unauthenticated SQL Injections (SQLi) |
| **Rule** | [146. SQL Injection](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-146) |
| **Remote** | Yes |
| **CVSSv3 Vector** | CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H |
| **CVSSv3 Base Score** | 9.8 |
| **Exploit available** | Yes |
| **CVE ID(s)** | [CVE-2023-44267](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-44267) |

## Description

Online Art Gallery v1.0 is vulnerable to
an Unauthenticated SQL Injection vulnerability.
The 'lnm' parameter of the header.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$fnm=trim($_POST['fnm']);
$lnm=trim($_POST['lnm']);
$gender=trim($_POST['gender']);
$contact=trim($_POST['contact']);
$email=trim($_POST['email']);
$pwd=trim($_POST['pwd']);
$dbpass=password_hash($pwd, PASSWORD_DEFAULT);
$confirm=trim($_POST['confirmpwd']);
$add1=trim($_POST['add1']);
$add2=trim($_POST['add2']);
$add3=trim($_POST['add3']);
...
$query="insert into user_reg (fname,lname,gender,contact,email,password,add1,add2,add3) value('$fnm','$lnm','$gender','$contact','$email','$dbpass','$add1','$add2','$add3')";
mysqli_query($link,$query) or die("Error inserting data.".mysqli_error($link));
```

## Our security policy

We have reserved the ID CVE-2023-44267 to refer
to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Online Art Gallery v1.0
* Operating System: Any

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://projectworlds.in/>

## Timeline

<time-lapse
discovered="2023-09-28"
contacted="2023-09-28"
disclosure="2023-10-26">
</time-lapse>
