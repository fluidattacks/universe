---
slug: advisories/skims-62/
title: Easy Code Snippets - Reflected cross-site scripting (XSS)
authors: Andres Roldan
writer: aroldan
codename: skims-62
product: Easy Code Snippets 1.0.
date: 2024-12-06 12:00 COT
cveid: CVE-2025-22626
description: Easy Code Snippets 1.0. - Reflected cross-site scripting (XSS) Vulnerability
keywords: Fluid Attacks, Security, Vulnerabilities, CVE, SAST
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | Easy Code Snippets 1.0. - Reflected cross-site scripting (XSS) |
| **Code name** | skims-62 |
| **Product** | Easy Code Snippets |
| **Affected versions** | Version 1.0. |
| **State** | Private |
| **Release date** | 2024-12-06 |

## Vulnerability

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Reflected cross-site scripting (XSS) |
| **Rule** | [Reflected cross-site scripting (XSS)](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-008) |
| **Remote** | No |
| **CVSSv4 Vector** | CVSS:4.0/AV:N/AC:L/AT:N/PR:N/UI:A/VC:H/VI:L/VA:L/SC:L/SI:L/SA:L/E:U |
| **Exploit available** | No |
| **CVE ID(s)** | [CVE-2025-22626](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2025-22626) |

## Description

Easy Code Snippets
1.0.
was found to be vulnerable.
The web application dynamically
generates web content without
validating the source of the
potentially untrusted data in
myapp/includes/admin/forms/class-wpcs-sn
ippet-list.php.

## Vulnerability

Skims by Fluid Attacks discovered a
Reflected cross-site scripting (XSS)
in
Easy Code Snippets
1.0..
The following is the output of the tool:

### Skims output

```powershell
  362 | 'page']) && $_GET['page'] == 'ecsnippets-snippets' && $total_snippet >= '5' ) {
  363 | = '<div class=""error settings-error"" id=""setting-error-""><p><strong>'.__( 'You can add maximun five snippets, to add more
  364 | sage;
  365 |
  366 | l = add_query_arg( array(
  367 | nippets-snippets',
  368 | dd-new-snippet'
  369 |
  370 | echo esc_url($new_snippet_Url); ?>"" class=""page-title-action"">
  371 |  New Snippet', 'ecsnippets' ); ?>
  372 |
  373 |
  374 |
  375 |
> 376 | en"" name=""page"" value=""<?php echo $_REQUEST['page'] ?>"" />
  377 | ets -->
  378 | stTable->search_box( __( 'Search Snippets', 'ecsnippets' ), 'ecsnippets_ltable_search' ); ?>
  379 | ender the completed list table -->
  380 | stTable->display(); ?>
  381 |
  382 |
      ^ Col 19
```

## Our security policy

We have reserved the ID CVE-2025-22626 to refer
to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Easy Code Snippets
1.0.

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team using
[Skims](../../plans/#essential)

## Timeline

<time-lapse
  discovered="2024-12-06"
  contacted="2025-01-07"
  replied=""
  confirmed=""
  patched=""
  disclosure="">
</time-lapse>
