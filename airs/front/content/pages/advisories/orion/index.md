---
slug: advisories/orion/
title: Gym Management System Project v1.0 - Insecure File Upload
authors: Andres Roldan
writer: aroldan
codename: Orion
product: Gym Management System Project v1.0
date: 2023-09-28 12:00 COT
cveid: CVE-2023-5185
severity: 9.1
description: Gym Management System Project v1.0 - Insecure File Upload
keywords: Fluid Attacks, Security, Vulnerabilities, CVE, Advisory
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

<summary-table
    name="Gym Management System Project v1.0 - Insecure File Upload"
    code="[Orion](https://en.wikipedia.org/wiki/Tomasz_Wr%C3%B3blewski)"
    product="Gym Management System Project"
    vendor="Projectworlds Pvt. Limited"
    affected-versions="Version 1.0"
    fixed-versions=""
    state="Public"
    release="2023-09-28">
</summary-table>

## Vulnerability

<vulnerability-table
    kind="Insecure File Upload"
    rule="[027. Insecure File Upload](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-027/)"
    remote="Yes"
    vector="CVSS:3.1/AV:N/AC:L/PR:H/UI:N/S:C/C:H/I:H/A:H"
    score="9.1"
    available="Yes"
    id="[CVE-2023-5185](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-5185)">
</vulnerability-table>

## Description

Gym Management System Project v1.0 is vulnerable to
an Insecure File Upload vulnerability on the 'file'
parameter of profile/i.php page, allowing an
authenticated attacker to obtain Remote Code Execution
on the server hosting the application.

## Vulnerability

The 'file' parameter of the profile/i.php page resource
does not validate the contents, extension and type of
the file uploaded as a book image, leading to an
arbitrary file upload which can be abused to obtain
Remote Code Execution. The vulnerable code is located
at profile/i.php page:

```html
  <div class="col-md-4" style="width:100px;">
     <?php if($row['pic'] == 0) { ?>
    <img src="../img/thumb.png" width="100px" height="100px"><br><br>
    <form method="POST" action="../upload.php?id=<?php echo $row['id']; ?>" enctype="multipart/form-data">
    <input type="file" name="file" style="color: transparent;" accept="image/x-png, image/gif, image/jpeg"><br>
    <input type="submit" value="upload" name="pupload">
    </form>
    <?php } else if($row['pic'] == 1){ ?>
    <img src="../upload/<?php echo $row['picName']; ?>" width="100px" height="100px">
    <?php } ?>
  </div>
```

## Evidence of exploitation

![evidence1](https://res.cloudinary.com/fluid-attacks/image/upload/v1695658075/airs/advisories/orion/evidence1.webp)

![evidence1](https://res.cloudinary.com/fluid-attacks/image/upload/v1695658078/airs/advisories/orion/evidence2.webp)

## Our security policy

We have reserved the ID CVE-2023-5185
to refer to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Gym Management System Project v1.0
* Operating System: Any

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://projectworlds.in/>

## Timeline

<time-lapse
discovered="2023-09-25"
contacted="2023-09-25"
disclosure="2023-09-28">
</time-lapse>
