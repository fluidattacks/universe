---
slug: advisories/harrison/
title: Online Movie Ticket Booking System v1.0 - Reflected Cross-Site Scripting (XSS)
authors: Andres Roldan
writer: aroldan
codename: Harrison
product: Online Movie Ticket Booking System v1.0
date: 2023-09-28 12:00 COT
cveid: CVE-2023-44173
severity: 5.4
description: Online Movie Ticket Booking System v1.0 - Reflected Cross-Site Scripting (XSS)
keywords: Fluid Attacks, Security, Vulnerabilities, XSS, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

<summary-table
    name="Online Movie Ticket Booking System v1.0 -
    Reflected Cross-Site Scripting (XSS)"
    code="[Harrison](https://en.wikipedia.org/wiki/George_Harrison)"
    product="Online Movie Ticket Booking System"
    vendor="Projectworlds Pvt. Limited"
    affected-versions="Version 1.0"
    fixed-versions=""
    state="Public"
    release="2023-09-28">
</summary-table>

## Vulnerabilities

<vulnerability-table
    kind="Reflected Cross-Site Scripting (XSS)"
    rule="[008. Reflected cross-site scripting](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-008/)"
    remote="Yes"
    vector="CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:L/A:L"
    score="5.4"
    available="Yes"
    id="[CVE-2023-44173](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-44173)">
</vulnerability-table>

## Description

Online Movie Ticket Booking System v1.0 is vulnerable to
an authenticated Reflected Cross-Site Scripting vulnerability.

## Vulnerability

The 'number' parameter of the bank.php resource
is copied into the HTML document as plain text
between tags. Any input is echoed unmodified in the
application's response. The vulnerable
code is:

```php
<?php
session_start();
if(!isset($_SESSION['user']))
{
    header('location:login.php');
}
extract($_POST);
?>
...
    <dl class="mercDetails">
        <dt>Merchant</dt> <dd>Shop Street</dd>
    <dt>Transaction Amount</dt> <dd>INR <?php echo  $_SESSION['amount'];?></dd>
    <dt>Debit Card</dt> <dd><?php echo  $number;?></%></dd>
  </dl>
```

## Our security policy

We have reserved the ID CVE-2023-44173 to refer
to this issues from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Online Movie Ticket Booking System v1.0
* Operating System: Any

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://projectworlds.in/>

## Timeline

<time-lapse
discovered="2023-09-26"
contacted="2023-09-26"
disclosure="2023-09-28">
</time-lapse>
