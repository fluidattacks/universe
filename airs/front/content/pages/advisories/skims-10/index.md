---
slug: advisories/skims-10/
title: FooGallery - Reflected cross-site scripting (XSS)
authors: Andres Roldan
writer: aroldan
codename: skims-10
product: FooGallery – Responsive Photo Gallery, Image Viewer, Justified, Masonry and Carousel 2.4.29
date: 2024-12-06 12:00 COT
cveid: CVE-2025-22624
description: FooGallery – Responsive Photo Gallery, Image Viewer, Justified, Masonry and Carousel 2.4.29 - Reflected cross-site scripting (XSS) Vulnerability
keywords: Fluid Attacks, Security, Vulnerabilities, CVE, SAST
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | FooGallery – Responsive Photo Gallery, Image Viewer, Justified, Masonry and Carousel 2.4.29 - Reflected cross-site scripting (XSS) |
| **Code name** | skims-10 |
| **Product** | FooGallery – Responsive Photo Gallery, Image Viewer, Justified, Masonry and Carousel |
| **Affected versions** | Version 2.4.29 |
| **State** | Public |
| **Release date** | 2024-03-05 |

## Vulnerability

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Reflected cross-site scripting (XSS) |
| **Rule** | [Reflected cross-site scripting (XSS)](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-008) |
| **Remote** | No |
| **CVSSv4 Vector** | CVSS:4.0/AV:N/AC:L/AT:N/PR:N/UI:A/VC:H/VI:L/VA:L/SC:L/SI:L/SA:L/E:U |
| **Exploit available** | No |
| **CVE ID(s)** | [CVE-2025-22624](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2025-22624) |

## Description

FooGallery – Responsive Photo Gallery,
Image Viewer, Justified, Masonry and
Carousel
2.4.29
was found to be vulnerable.
The web application dynamically
generates web content without
validating the source of the
potentially untrusted data in
myapp/extensions/albums/admin/class-meta
boxes.php.

## Vulnerability

Skims by Fluid Attacks discovered a
Reflected cross-site scripting (XSS)
in
FooGallery – Responsive Photo Gallery,
Image Viewer, Justified, Masonry and
Carousel
2.4.29.
The following is the output of the tool:

### Skims output

```powershell
  400 |
  401 |
  402 |
  403 | lery_details' ) ) {
  404 |
  405 | id );
  406 |
  407 |
  408 | gallery ); ?>
  409 |
> 410 | ""foogallery_id"" value=""<?php echo $foogallery_id; ?>"" />
  411 |
  412 |
  413 | s ) {
  414 | ld, true );
  415 | lds-' . $field;
  416 |
  417 |
  418 | =""' . $input_id . '"" name=""' . $field . '"" value=""' . esc_attr( foogallery_sanitize_javascript( $value ) ) . '"" />';
  419 |
  420 |
      ^ Col 50
```

## Our security policy

We have reserved the ID CVE-2025-22624 to refer
to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: FooGallery – Responsive Photo Gallery,
Image Viewer, Justified, Masonry and
Carousel
2.4.29

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team using
[Skims](../../plans/#essential)

## Timeline

<time-lapse
  discovered="2024-12-06"
  contacted="2025-02-21"
  replied=""
  confirmed=""
  patched="2025-03-04"
  disclosure="2025-03-05">
</time-lapse>
