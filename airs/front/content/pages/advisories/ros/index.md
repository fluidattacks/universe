---
slug: advisories/ros/
title: Online Matrimonial Project v1.0 - Multiple Unauthenticated SQL Injections (SQLi)
authors: Andres Roldan
writer: aroldan
codename: ros
product: Online Matrimonial Project v1.0
date: 2023-11-02 12:00 COT
cveid: CVE-2023-46785,CVE-2023-46787,CVE-2023-46788,CVE-2023-46789,CVE-2023-46793,CVE-2023-46800
severity: 9.8
description: Online Matrimonial Project v1.0 - Multiple Unauthenticated SQL Injections (SQLi)
keywords: Fluid Attacks, Security, Vulnerabilities, SQLi, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | Online Matrimonial Project v1.0 - Multiple Unauthenticated SQL Injections (SQLi) |
| **Code name** | [Ros](https://en.wikipedia.org/wiki/Sigur_R%C3%B3s) |
| **Product** | Online Matrimonial Project |
| **Vendor** | Projectworlds Pvt. Limited |
| **Affected versions** | Version 1.0 |
| **State** | Public |
| **Release date** | 2023-11-02 |

## Vulnerabilities

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Unauthenticated SQL Injections (SQLi) |
| **Rule** | [146. SQL Injection](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-146) |
| **Remote** | Yes |
| **CVSSv3 Vector** | CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H |
| **CVSSv3 Base Score** | 9.8 |
| **Exploit available** | Yes |
| **CVE ID(s)** | [CVE-2023-46785](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-46785), [CVE-2023-46787](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-46787), [CVE-2023-46788](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-46788), [CVE-2023-46789](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-46789), [CVE-2023-46793](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-46793), [CVE-2023-46800](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-46800) |

## Description

Online Matrimonial Project v1.0 is vulnerable to
multiple Unauthenticated SQL Injection vulnerabilities.

## Vulnerabilities

### CVE-2023-46785

The 'id' parameter of the partner_preference.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$id=$_GET['id'];
writepartnerprefs($id);

///reading partnerprefs from db

$sql="SELECT * FROM partnerprefs WHERE custId = $id";
$result=mysqlexec($sql);
```

### CVE-2023-46787

The 'username' parameter of the auth/auth.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$myusername=$_POST['username'];
$mypassword=$_POST['password'];

// To protect MySQL injection (more detail about MySQL injection)
$myusername = stripslashes($myusername);
$mypassword = stripslashes($mypassword);

$sql="SELECT * FROM users WHERE username='$myusername' AND password='$mypassword'";
$result=mysqli_query($conn,$sql);
```

### CVE-2023-46788

The 'id' parameter in the 'uploadphoto()'
function of the functions.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

photouploader.php:

```php
$id=$_GET['id'];
if(isloggedin()){
 //do nothing stay here
} else{
   header("location:login.php");
}

//calling photo uploader function
if ($_SERVER['REQUEST_METHOD'] == 'POST'){ uploadphoto($id); }
```

functions.php:

```php
function uploadphoto($id){
    $target = "profile/". $id ."/";
if (!file_exists($target)) {
    mkdir($target, 0777, true);
}
...
$sql="SELECT id FROM photos WHERE cust_id = '$id'";
$result = mysqlexec($sql);
```

### CVE-2023-46789

The 'filename' attribute of the 'pic1' multipart
parameter of the functions.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
// This gets all the other information from the form
$pic1=($_FILES['pic1']['name']);
$pic2=($_FILES['pic2']['name']);
$pic3=($_FILES['pic3']['name']);
$pic4=($_FILES['pic4']['name']);
...
//code part to check weather a photo already exists
if(mysqli_num_rows($result) == 0) {
    // no photo for curret user, do stuff...
    $sql="INSERT INTO photos (id, cust_id, pic1, pic2, pic3, pic4) VALUES ('', '$id', '$pic1' ,'$pic2', '$pic3','$pic4')";
    // Writes the information to the database
    mysqlexec($sql);


} else {
// There is a photo for customer so up
    $sql="UPDATE photos SET pic1 = '$pic1', pic2 = '$pic2', pic3 = '$pic3', pic4 = '$pic4' WHERE cust_id=$id";
    // Writes the information to the database
mysqlexec($sql);
}
```

### CVE-2023-46793

The 'day' parameter in the 'register()'
function of the functions.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
function register(){
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $uname=$_POST['name'];
    $pass=$_POST['pass'];
    $email=$_POST['email'];
    $day=$_POST['day'];
    $month=$_POST['month'];
    $year=$_POST['year'];
        $day=$_POST['day'];
        $month=$_POST['month'];
        $year=$_POST['year'];
    $dob=$year ."-" . $month . "-" .$day ;
    $gender=$_POST['gender'];
    require_once("includes/dbconn.php");

    $sql = "INSERT
            INTO
               users
               ( profilestat, username, password, email, dateofbirth, gender, userlevel)
            VALUES
               (0, '$uname', '$pass', '$email', '$dob', '$gender', 0)";

    if (mysqli_query($conn,$sql))
...
```

### CVE-2023-46800

The 'id' parameter of the view_profile.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$id=$_GET['id'];
//safty purpose copy the get id
$profileid=$id;

//getting profile details from db
$sql="SELECT * FROM customer WHERE cust_id = $id";
$result = mysqlexec($sql);
```

## Our security policy

We have reserved the IDs CVE-2023-46785,
CVE-2023-46787, CVE-2023-46788,
CVE-2023-46789,
CVE-2023-46793,
and CVE-2023-46800
to refer to these issues from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Online Matrimonial Project v1.0
* Operating System: Any

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://projectworlds.in/>

## Timeline

<time-lapse
discovered="2023-10-26"
contacted="2023-10-26"
disclosure="2023-11-02">
</time-lapse>
