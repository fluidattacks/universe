---
slug: advisories/cole/
title: Watchdog Antivirus v1.6.415 - DoS
authors: Andres Roldan
writer: aroldan
codename: cole
product: Watchdog Antivirus v1.6.415
date: 2024-04-22 12:00 COT
cveid: CVE-2024-1241
severity: 5.5
description: Watchdog Antivirus v1.6.415 - Denial of Service Vulnerability
keywords: Fluid Attacks, Security, Vulnerabilities, DoS, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | Watchdog Antivirus v1.6.415 - Denial of Service |
| **Code name** | [Cole](https://en.wikipedia.org/wiki/Nat_King_Cole) |
| **Product** | Watchdog Antivirus |
| **Vendor** | Watchdog |
| **Affected versions** | Version 1.6.415 |
| **State** | Public |
| **Release date** | 2024-04-22 |

## Vulnerability

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Denial of Service (DoS) |
| **Rule** | [002. Asymmetric Denial of Service](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-002/) |
| **Remote** | No |
| **CVSSv3 Vector** | CVSS:3.1/AV:L/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:H |
| **CVSSv3 Base Score** | 5.5 |
| **Exploit available** | Yes |
| **CVE ID(s)** | [CVE-2024-1241](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-1241) |

## Description

Watchdog Antivirus v1.6.415 is vulnerable to
a Denial of Service vulnerability by triggering the
`0x80002014` IOCTL code of the
`wsdk-driver.sys` driver.

## Vulnerability

The `0x80002014` IOCTL code of the
`wsdk-driver.sys` driver allows to perform a Denial of Service,
leading to a BSOD of the affected computer caused by a
NULL pointer dereference.

The disassembled vulnerable function is `sub_140008D5C`:

```c
__int64 __fastcall sub_140008D5C(HANDLE ProcessId)  // [3]
{
  __int64 v1; // rbx
  const char *ProcessImageFileName; // rdi
  NTSTATUS v3; // eax
  int v4; // edx
  int v5; // r8d
  PEPROCESS Process; // [rsp+48h] [rbp+10h] BYREF

  Process = 0i64;
  v1 = (unsigned int)ProcessId;
  ProcessImageFileName = 0i64;
  v3 = PsLookupProcessByProcessId((HANDLE)(unsigned int)ProcessId, &Process);
  if ( v3 < 0 )
  {
    if ( off_140005250 != &off_140005250 )
    {
      _mm_lfence();
      LOBYTE(v4) = 3;
      sub_140001084((int)off_140005248->DeviceExtension, v4, v5, 0x14, (__int64)&unk_140003960, v3);
    }
  }
  else
  {
    ProcessImageFileName = (const char *)PsGetProcessImageFileName(Process);  // [2]
    ObfDereferenceObject(Process);
  }
  if ( stricmp(ProcessImageFileName, "WAV.exe") )  // [3]
    return 0xC0000001i64;
  qword_140005298 = v1;
  return 0i64;
}
```

The `sub_140008D5C` function tries to find the process
instance of the `WAV.exe` executable. It receives a
parameter called `ProcessId` at `[1]`, tries to find the
image filename with `PsGetProcessImageFileName` at `[2]` and compares
if the file name matches with `WAV.exe` using `stricmp()` at `[3]`.
However, the parameter `ProcessId` can be controlled by the
attacker, making the result of `PsGetProcessImageFileName`
to be `NULL`, performing an invalid comparison with `stricmp()`,
resulting in a NULL pointer dereference:

```c
CONTEXT:  ffffca0f2802ed20 -- (.cxr 0xffffca0f2802ed20)
rax=0000000000000000 rbx=0000000041414141 rcx=0000000000000000
rdx=fffff80172caac80 rsi=0000000080002014 rdi=0000000000000000
rip=fffff80177dcc6b2 rsp=ffffca0f2802f728 rbp=ffffcf0c89df45c0
 r8=0000000000000000  r9=ffffcf0c8efe40a8 r10=fffff80172caac80
r11=0000000000000000 r12=0000000000000000 r13=0000000000000000
r14=0000000000000000 r15=ffffcf0c8e61d8f0
iopl=0         nv up ei ng nz na po nc
cs=0010  ss=0018  ds=002b  es=002b  fs=0053  gs=002b             efl=00050286
nt!_ascii_stricmp+0x6:
fffff801`77dcc6b2 450fb603        movzx   r8d,byte ptr [r11] ds:002b:00000000`00000000=??
Resetting default scope

PROCESS_NAME:  IOCTLBruteForce.exe

STACK_TEXT:
ffffca0f`2802f728 fffff801`77dcc709     : ffffca0f`28020014 ffffca0f`2802f788 00000000`00000004 00000000`00000000 : nt!_ascii_stricmp+0x6
ffffca0f`2802f730 fffff801`72ca8de7     : 00000000`41414141 00000000`80002014 00000000`41414141 00000000`00000000 : nt!stricmp+0x9
ffffca0f`2802f760 fffff801`72ca806e     : ffffcf0c`89df45c0 00000000`00000000 00000000`000000c8 fffff801`780d4b41 : wsdkd+0x8de7
ffffca0f`2802f7a0 fffff801`77cd1f35     : ffffcf0c`89df45c0 00000000`00000004 00000000`00000002 00000000`00000001 : wsdkd+0x806e
```

## Our security policy

We have reserved the ID CVE-2024-1241 to refer
to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Watchdog Antivirus v1.6.415
* Operating System: Windows

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://watchdog.dev/>

**Product page** <https://watchdog.dev/solutions/anti-virus/>

## Timeline

<time-lapse
discovered="2024-02-05"
contacted="2024-02-05"
replied="2024-03-03"
disclosure="2024-04-22">
</time-lapse>
