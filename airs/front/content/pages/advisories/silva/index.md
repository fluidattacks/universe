---
slug: advisories/silva/
title: Suite CRM v7.14.2 - RCE via LFI
authors: Carlos Bello
writer: cbello
codename: silva
product: Suite CRM v7.14.2 - RCE via LFI
date: 2024-02-19 12:00 COT
cveid: CVE-2024-1644
severity: 9.9
description: Suite CRM v7.14.2  -  RCE via Local File Inclusion
keywords: Fluid Attacks, Security, Vulnerabilities, RCE, LFI, Suite CRM, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

<summary-table
    name="Suite CRM v7.14.2 - RCE via LFI"
    code="[Silva](https://en.wikipedia.org/wiki/Lil_Silva)"
    product="Suite CRM"
    affected-versions="Version 7.14.2"
    fixed-versions=""
    state="Public"
    release="2024-02-19">
</summary-table>

## Vulnerability

<vulnerability-table
    kind="Local file inclusion (LFI)"
    rule="[123. Local file inclusion (LFI)](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-123)"
    remote="Yes"
    vector="CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:C/C:H/I:H/A:H"
    score="9.9"
    available="No"
    id="[CVE-2024-1644](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-1644)">
</vulnerability-table>

## Description

Suite CRM version 7.14.2 allows including local php files. This is possible
because the application is vulnerable to LFI.

## Vulnerability

A local file inclusion (LFI) vulnerability has been identified in Suite CRM
that, under certain conditions, could allow an attacker to obtain remote
command execution. The attacker must have minimum privileges.

## Vulnerable path

Below I will show you the vulnerable path in code, from the source to the sink.

```php
// Index.php
use App\Kernel;
use Symfony\Component\ErrorHandler\Debug;
use Symfony\Component\HttpFoundation\Request;

[...]

$legacyRoute = $kernel->getLegacyRoute($request);

if (!empty($legacyRoute)) {

    $path = './legacy';
    if (!empty($legacyRoute['dir'])) {
        $path .= '/' . $legacyRoute['dir'];
    }

    chdir($path);

    $access = $legacyRoute['access'] ?? false;
    if ($access === false) {
        http_response_code(404);
        exit;
    }

    if (file_exists($legacyRoute['file'])) {

        /* @noinspection PhpIncludeInspection */
        require $legacyRoute['file']; // Vulnerability Here
    } else {

        http_response_code(404);
        exit;
    }

}
[...]
```

```php
// Kernel.php
public function getLegacyRoute(Request $request): array {
    $this->initializeBundles();
    $this->initializeContainer();

    if ($this->container->has('legacy.route.handler')) {
        return $this->container->get('legacy.route.handler')->getLegacyRoute($request);
    }

    return [];
}
```

```php
// LegacyRouteHandler.php
public function getLegacyRoute(Request $request): array {
    [...]

    if ($this->isLegacyNonViewActionRoute($request)) {
        return $this->legacyNonViewActionRedirectHandler->getIncludeFile($request);
    }

    [...]
}
```

```php
// LegacyRedirectHandler.php
public function getIncludeFile(Request $request): array {
    $baseUrl = $request->getPathInfo(); // Anything after the index.php file

    $baseUrl = substr($baseUrl, 1);

    if (strpos($baseUrl, '.php') === false) {
        $baseUrl .= 'index.php';
    }

    return [
        'dir' => '',
        'file' => $baseUrl, // Arbitrary path (with custom wrapper)
        'access' => true
    ];
}
```

## Evidence of exploitation

1. Create a product and upload a malicious image

    <iframe src="https://tinyurl.com/ay3ftxpw"
    width="835" height="504" frameborder="0" title="File-Upload-SuiteCRM-7.14.2"
    webkitallowfullscreen mozallowfullscreen allowfullscreen controls
    loading="lazy"></iframe>

2. Include the image from the previous step with the LFI to achieve RCE

    <iframe src="https://tinyurl.com/yc4kx3kv"
    width="835" height="504" frameborder="0" title="RCE-via-LFI-SuiteCRM-7.14.2"
    webkitallowfullscreen mozallowfullscreen allowfullscreen controls
    loading="lazy"></iframe>

## Our security policy

We have reserved the ID CVE-2024-1644 to refer to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Suite CRM 7.14.2

* Operating System: MacOS

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by [Carlos
Bello](https://www.linkedin.com/in/carlos-andres-bello) from Fluid Attacks'
Offensive Team.

## References

**Vendor page** <https://github.com/salesagility/SuiteCRM/>

## Timeline

<time-lapse
  discovered="2024-01-05"
  contacted="2024-01-05"
  replied="2024-01-10"
  confirmed="2024-01-10"
  patched=""
  disclosure="2024-02-19">
</time-lapse>
