---
slug: advisories/netrebko/
title: Online Job Portal v1.0 - Multiple Unauthenticated SQL Injections (SQLi)
authors: Andres Roldan
writer: aroldan
codename: netrebko
product: Online Job Portal v1.0
date: 2023-11-02 12:00 COT
cveid: CVE-2023-46677,CVE-2023-46679
severity: 9.8
description: Online Job Portal v1.0 - Multiple Unauthenticated SQL Injections (SQLi)
keywords: Fluid Attacks, Security, Vulnerabilities, SQLi, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | Online Job Portal v1.0 - Multiple Unauthenticated SQL Injections (SQLi) |
| **Code name** | [Netrebko](https://en.wikipedia.org/wiki/Anna_Netrebko) |
| **Product** | Online Job Portal |
| **Vendor** | Projectworlds Pvt. Limited |
| **Affected versions** | Version 1.0 |
| **State** | Public |
| **Release date** | 2023-11-02 |

## Vulnerabilities

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Unauthenticated SQL Injections (SQLi) |
| **Rule** | [146. SQL Injection](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-146) |
| **Remote** | Yes |
| **CVSSv3 Vector** | CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H |
| **CVSSv3 Base Score** | 9.8 |
| **Exploit available** | Yes |
| **CVE ID(s)** | [CVE-2023-46677](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-46677), [CVE-2023-46679](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-46679) |

## Description

Online Job Portal v1.0 is vulnerable to
multiple Unauthenticated SQL Injection vulnerabilities.

## Vulnerabilities

### CVE-2023-46677

The 'txt_uname' parameter of the sign-up.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$uname = strip_tags($_POST['txt_uname']);
$upass = strip_tags($_POST['txt_upass']);
$pic=$_FILES["img"]["name"];
...
$sql= mysqli_query($con,"insert into registration(name,email,image,password) values('$uname','$umail','$pic','$upass')");
```

### CVE-2023-46679

The 'txt_uname_email' parameter of the index.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$email=$_POST["txt_uname_email"];
$pass=$_POST["txt_password"];

$sql=mysqli_query($con,"select * from registration where email='$email' and password='$pass'");
```

## Our security policy

We have reserved the IDs CVE-2023-46677
and CVE-2023-46679 to refer
to these issues from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Online Job Portal v1.0
* Operating System: Any

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://projectworlds.in/>

## Timeline

<time-lapse
discovered="2023-10-24"
contacted="2023-10-24"
disclosure="2023-11-02">
</time-lapse>
