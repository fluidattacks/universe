---
slug: advisories/arrau/
title: Online Notice Board System v1.0 - File UL
authors: Andres Roldan
writer: aroldan
codename: arrau
product: Online Notice Board System v1.0
date: 2024-01-03 12:00 COT
cveid: CVE-2023-50760
severity: 8.8
description: Online Notice Board System v1.0 - Insecure File Upload
keywords: Fluid Attacks, Security, Vulnerabilities, CVE, Advisory
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | Online Notice Board System v1.0 - Insecure File Upload |
| **Code name** | [Arrau](https://es.wikipedia.org/wiki/Claudio_Arrau) |
| **Product** | Online Notice Board System |
| **Vendor** | Kashipara Group |
| **Affected versions** | Version 1.0 |
| **State** | Public |
| **Release date** | 2024-01-03 |

## Vulnerability

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Insecure File Upload |
| **Rule** | [027. Insecure File Upload](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-027/) |
| **Remote** | Yes |
| **CVSSv3 Vector** | CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:H/A:H |
| **CVSSv3 Base Score** | 8.8 |
| **Exploit available** | Yes |
| **CVE ID(s)** | [CVE-2023-50760](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-50760) |

## Description

Online Notice Board System v1.0 is vulnerable to
an Insecure File Upload vulnerability on the 'f'
parameter of user/update_profile_pic.php page, allowing an
authenticated attacker to obtain Remote Code Execution
on the server hosting the application.

## Vulnerability

The 'f' parameter of the user/update_profile_pic.php page
resource does not validate the contents, extension and type of
the file uploaded as a book image, leading to an
arbitrary file upload which can be abused to obtain
Remote Code Execution. The vulnerable code is located
at profile/i.php page:

```php
extract($_POST);
if(isset($update))
{
$img=$_FILES['f']['name'];
...
move_uploaded_file($_FILES['f']['tmp_name'],"../images/$user/".$_FILES['f']['name']);
```

## Our security policy

We have reserved the ID CVE-2023-50760
to refer to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Online Notice Board System v1.0
* Operating System: Any

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://www.kashipara.com/>

## Timeline

<time-lapse
discovered="2024-01-03"
contacted="2024-01-03"
disclosure="2024-01-03">
</time-lapse>
