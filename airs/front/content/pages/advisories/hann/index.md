---
slug: advisories/hann/
title: Online Food Ordering System v1.0 - Multiple Unauthenticated SQL Injections (SQLi)
authors: Andres Roldan
writer: aroldan
codename: Hann
product: Online Food Ordering System v1.0
date: 2023-11-01 12:00 COT
cveid: CVE-2023-45323,CVE-2023-45325,CVE-2023-45334,CVE-2023-45336,CVE-2023-45338,CVE-2023-45340,CVE-2023-45341,CVE-2023-45342,CVE-2023-45343,CVE-2023-45344,CVE-2023-45345,CVE-2023-45346,CVE-2023-45347
severity: 9.8
description: Online Food Ordering System v1.0 - Multiple Unauthenticated SQL Injections (SQLi)
keywords: Fluid Attacks, Security, Vulnerabilities, SQLi, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | Online Food Ordering System v1.0 - Multiple Unauthenticated SQL Injections (SQLi) |
| **Code name** | [Hann](https://en.wikipedia.org/wiki/Hilary_Hahn) |
| **Product** | Online Food Ordering System |
| **Vendor** | Projectworlds Pvt. Limited |
| **Affected versions** | Version 1.0 |
| **State** | Public |
| **Release date** | 2023-11-01 |

## Vulnerabilities

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Unauthenticated SQL Injections (SQLi) |
| **Rule** | [146. SQL Injection](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-146) |
| **Remote** | Yes |
| **CVSSv3 Vector** | CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H |
| **CVSSv3 Base Score** | 9.8 |
| **Exploit available** | Yes |
| **CVE ID(s)** | [CVE-2023-45323](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-45323), [CVE-2023-45325](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-45325), [CVE-2023-45334](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-45334), [CVE-2023-45336](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-45336), [CVE-2023-45338](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-45338), [CVE-2023-45340](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-45340), [CVE-2023-45341](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-45341), [CVE-2023-45342](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-45342), [CVE-2023-45343](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-45343), [CVE-2023-45344](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-45344), [CVE-2023-45345](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-45345), [CVE-2023-45346](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-45346), [CVE-2023-45347](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-45347) |

## Description

Online Food Ordering System v1.0 is vulnerable to
multiple Unauthenticated SQL Injection vulnerabilities.

## Vulnerabilities

### CVE-2023-45323

The 'name' parameter of the routers/add-item.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$name = $_POST['name'];
$price = $_POST['price'];
$sql = "INSERT INTO items (name, price) VALUES ('$name', $price)";
$con->query($sql);
```

### CVE-2023-45325

The 'address' parameter of the routers/add-users.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$username = $_POST['username'];
$password = $_POST['password'];
$name = $_POST['name'];
$email = $_POST['email'];
$contact = $_POST['contact'];
$address = $_POST['address'];
$role = $_POST['role'];
$verified = $_POST['verified'];
$deleted = $_POST['deleted'];
$sql = "INSERT INTO users (username, password, name, email, contact, address, role, verified, deleted) VALUES ('$username', '$password', '$name', '$email', $contact, '$address', '$role', $verified, $deleted)";
if($con->query($sql)==true)
```

### CVE-2023-45334

The 'status' parameter of the routers/edit-orders.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$status = $_POST['status'];
$id = $_POST['id'];

$sql = "UPDATE orders SET status='$status' WHERE id=$id;";
$con->query($sql);
```

### CVE-2023-45336

The 'password' parameter of the routers/router.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$username = $_POST['username'];
$password = $_POST['password'];

$result = mysqli_query($con, "SELECT * FROM users WHERE username='$username' AND password='$password' AND role='Administrator' AND not deleted;");
```

### CVE-2023-45338

The 'id' parameter of the routers/add-ticket.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$type = $_POST['type'];
$user_id = $_POST['id'];
if($type != ''){
    $sql = "INSERT INTO tickets (poster_id, subject, description, type) VALUES ($user_id, '$subject', '$description', '$type')";
```

### CVE-2023-45340

The 'phone' parameter of the routers/details-router.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$phone = $_POST['phone'];
$email = htmlspecialchars($_POST['email']);
$address = htmlspecialchars($_POST['address']);
$sql = "UPDATE users SET name = '$name', username = '$username', password='$password', contact=$phone, email='$email', address='$address' WHERE id = $user_id;";
```

### CVE-2023-45341

The '*_price' parameter of the routers/menu-router.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
if(preg_match("/[0-9]+_price/",$key)){
    $key = strtok($key, '_');
    $sql = "UPDATE items SET price = $value WHERE id = $key;";
    $con->query($sql);
}
```

### CVE-2023-45342

The 'phone' parameter of the routers/register-router.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$phone = $_POST['phone'];

$sql = "INSERT INTO users (name, username, password, contact) VALUES ('$name', '$username', '$password', $phone);";
if($con->query($sql)==true)
```

### CVE-2023-45343

The 'ticket_id' parameter of the routers/ticket-message.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$ticket_id = $_POST['ticket_id'];
$role = $_POST['role'];
if($role == 'Administrator'){
    $sql = "UPDATE tickets SET status = 'Answered' WHERE id=$ticket_id;";
    $con->query($sql);
}
```

### CVE-2023-45344

The '*_balance' parameter of the routers/user-router.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
if(preg_match("/[0-9]+_balance/",$key)){
    $key = strtok($key, '_');
    $result = mysqli_query($con,"SELECT * from wallet WHERE customer_id = $key;");
    if($row = mysqli_fetch_array($result)){
        $wallet_id = $row['id'];
        $sql = "UPDATE wallet_details SET balance = '$value' WHERE wallet_id = $wallet_id;";
        $con->query($sql);
    }
}
```

### CVE-2023-45345

The '*_deleted' parameter of the routers/user-router.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
if(preg_match("/[0-9]+_deleted/",$key)){
    $key = strtok($key, '_');
    $sql = "UPDATE users SET deleted = '$value' WHERE id = $key;";
    $con->query($sql);
}
```

### CVE-2023-45346

The '*_role' parameter of the routers/user-router.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
if(preg_match("/[0-9]+_role/",$key)){
    $key = strtok($key, '_');
    $sql = "UPDATE users SET role = '$value' WHERE id = $key;";
    $con->query($sql);
}
```

### CVE-2023-45347

The '*_verified' parameter of the routers/user-router.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
if(preg_match("/[0-9]+_verified/",$key)){
    $key = strtok($key, '_');
    $sql = "UPDATE users SET verified = '$value' WHERE id = $key;";
    $con->query($sql);
}
```

## Our security policy

We have reserved the IDs CVE-2023-45323,
CVE-2023-45325,
CVE-2023-45334,
CVE-2023-45336, CVE-2023-45338,
CVE-2023-45340, CVE-2023-45341,
CVE-2023-45342, CVE-2023-45343, CVE-2023-45344,
CVE-2023-45345, CVE-2023-45346 and CVE-2023-45347
to refer to these issues from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Online Food Ordering System v1.0
* Operating System: Any

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://projectworlds.in/>

## Timeline

<time-lapse
discovered="2023-10-06"
contacted="2023-10-06"
disclosure="2023-11-01">
</time-lapse>
