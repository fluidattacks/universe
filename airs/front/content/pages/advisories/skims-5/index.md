---
slug: advisories/skims-5/
title: Age Verification - Reflected cross-site scripting (XSS)
authors: Andres Roldan
writer: aroldan
codename: skims-5
product: Age Verification for your checkout page. Verify your customer's identity 1.20.0
date: 2024-12-06 12:00 COT
cveid: CVE-2025-22622
description: Age Verification for your checkout page. Verify your customer's identity 1.20.0 - Reflected cross-site scripting (XSS) Vulnerability
keywords: Fluid Attacks, Security, Vulnerabilities, CVE, SAST
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | Age Verification for your checkout page. Verify your customer's identity 1.20.0 - Reflected cross-site scripting (XSS) |
| **Code name** | skims-5 |
| **Product** | Age Verification for your checkout page. Verify your customer's identity |
| **Affected versions** | Version 1.20.0 |
| **State** | Public |
| **Release date** | 2025-02-18 |

## Vulnerability

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Reflected cross-site scripting (XSS) |
| **Rule** | [Reflected cross-site scripting (XSS)](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-008) |
| **Remote** | No |
| **CVSSv4 Vector** | CVSS:4.0/AV:N/AC:L/AT:N/PR:N/UI:A/VC:H/VI:L/VA:L/SC:L/SI:L/SA:L/E:U |
| **Exploit available** | No |
| **CVE ID(s)** | [CVE-2025-22622](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2025-22622) |

## Description

Age Verification for your checkout
page. Verify your customer's identity
1.20.0
was found to be vulnerable.
The web application dynamically
generates web content without
validating the source of the
potentially untrusted data in
myapp/class-wc-integration-agechecker-in
tegration.php.

## Vulnerability

Skims by Fluid Attacks discovered a
Reflected cross-site scripting (XSS)
in
Age Verification for your checkout
page. Verify your customer's identity
1.20.0.
The following is the output of the tool:

### Skims output

```powershell
  1414 |                     <p><strong>Minimum Total invalid!</strong> Total must be a valid numeric value.</p>
  1415 |                 </div>
  1416 |     <?php
  1417 |     return $this->get_option($key);
  1418 |    }
  1419 |
  1420 |    return $value;
  1421 |   }
  1422 |
  1423 |   public function validate_titled_text_field($key) {
> 1424 |    echo $_POST[ $this->plugin_id . $this->id . '_' . $key ];
  1425 |    return $_POST[ $this->plugin_id . $this->id . '_' . $key ];
  1426 |   }
  1427 |
  1428 |   public function validate_category_apikeys_field($key) {
  1429 |    $value = $_POST[""categoryApiKeysListData""];
  1430 |    if (!isset($value)) {
  1431 |     ?>
  1432 |                 <div class=""error notice"">
  1433 |                     <p><strong>Invalid category api keys.</strong></p>
  1434 |                 </div>
       ^ Col 0
```

## Our security policy

We have reserved the ID CVE-2025-22622 to refer
to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Age Verification for your checkout
  page. Verify your customer's identity
  1.20.0

## Mitigation

The version 1.20.1 contains the patched
version of the plugin.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team using
[Skims](../../plans/#essential)

## Timeline

<time-lapse
  discovered="2024-12-06"
  contacted="2025-02-10"
  replied=""
  confirmed=""
  patched="2025-02-11"
  disclosure="2025-02-18">
</time-lapse>
