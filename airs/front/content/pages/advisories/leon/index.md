---
slug: advisories/leon/
title: Suite CRM v7.14.2 - SSRF
authors: Carlos Bello
writer: cbello
codename: leon
product: Suite CRM v7.14.2 - SSRF
date: 2024-02-06 12:00 COT
cveid: CVE-2023-6388
severity: 5.0
description: Suite CRM v7.14.2                                 -                         SSRF
keywords: Fluid Attacks, Security, Vulnerabilities, SSRF, Suite CRM, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

<summary-table
    name="Suite CRM v7.14.2 - SSRF"
    code="[Leon](https://es.wikipedia.org/wiki/Car%C3%ADn_Le%C3%B3n)"
    product="Suite CRM"
    affected-versions="Version 7.14.2"
    fixed-versions=""
    state="Public"
    release="">
</summary-table>

## Vulnerability

<vulnerability-table
    kind="Server-site request forgery"
    rule="[100. Server-site request forgery](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-100)"
    remote="Yes"
    vector="CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:C/C:N/I:L/A:N"
    score="5.0"
    available="Yes"
    id="[CVE-2023-6388](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-6388)">
</vulnerability-table>

## Description

Suite CRM version 7.14.2 allows making arbitrary HTTP requests through
the vulnerable server. This is possible because the application is vulnerable
to SSRF.

## Vulnerability

A server request forgery (SSRF) vulnerability has been identified in Suite CRM
that, under certain conditions, could allow a user to make arbitrary HTTP requests
through the vulnerable server.

The vulnerability exists because a user-entered URL is passed to the getimagesize
function.

```php
public function handleInput($feed, $link_type, $link_url) {
    parent::handleInput($feed, $link_type, $link_url);

    // The FeedLinkHandlerLink class will help sort this url out for us
    $link_url = $feed->link_url;

    // Vulnerability Here
    $imageData = getimagesize($link_url);

    if (! isset($imageData)) {
        // The image didn't pull down properly, could be a link and allow_url_fopen could be disabled
        $imageData[0] = 0;
        $imageData[1] = 0;
    } else {
        if (max($imageData[0], $imageData[1]) > 425) {
            // This is a large image, we need to set some specific width/height properties so that the browser can scale it.
            $scale = 425 / max($imageData[0], $imageData[1]);
            $imageData[0] = floor($imageData[0]*$scale);
            $imageData[1] = floor($imageData[1]*$scale);
        }
    }

    $feed->link_url = base64_encode(serialize(array('url'=>$link_url,'width'=>$imageData[0],'height'=>$imageData[1])));
}
```

## Evidence of exploitation

<iframe src="https://tinyurl.com/4sxam5bb"
width="835" height="504" frameborder="0" title="SSRF-SuiteCRM-7.14.2"
webkitallowfullscreen mozallowfullscreen allowfullscreen controls
loading="lazy"></iframe>

## Our security policy

We have reserved the ID CVE-2023-6388 to refer to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Suite CRM v7.14.2

* Operating System: MacOS

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by [Carlos
Bello](https://www.linkedin.com/in/carlos-andres-bello) from Fluid Attacks'
Offensive Team.

## References

**Vendor page** <https://github.com/salesagility/SuiteCRM/>

## Timeline

<time-lapse
  discovered="2023-12-05"
  contacted="2023-12-05"
  replied="2023-12-07"
  confirmed="2023-12-07"
  patched="2023-12-06"
  disclosure="2024-02-06">
</time-lapse>
