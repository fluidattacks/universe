---
slug: advisories/davis/
title: IObit Malware Fighter v11.0.0.1274 - DoS
authors: Andres Roldan
writer: aroldan
codename: davis
product: IObit Malware Fighter v11.0.0.1274
date: 2024-01-16 12:00 COT
cveid: CVE-2024-0430
severity: 5.5
description: IObit Malware Fighter v11.0.0.1274 - Denial of Service (DoS)
keywords: Fluid Attacks, Security, Vulnerabilities, DoS, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | IObit Malware Fighter v11.0.0.1274 - Denial of Service (DoS) |
| **Code name** | [Davis](https://en.wikipedia.org/wiki/Miles_Davis) |
| **Product** | IObit Malware Fighter |
| **Vendor** | IObit |
| **Affected versions** | Version 11.0.0.1274 |
| **State** | Public |
| **Release date** | 2024-01-16 |

## Vulnerability

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Denial of Service (DoS) |
| **Rule** | [002. Asymmetric Denial of Service](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-002) |
| **Remote** | No |
| **CVSSv3 Vector** | CVSS:3.1/AV:L/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:H |
| **CVSSv3 Base Score** | 5.5 |
| **Exploit available** | Yes |
| **CVE ID(s)** | [CVE-2024-0430](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-0430) |

## Description

IObit Malware Fighter v11.0.0.1274 is vulnerable to
a Denial of Service vulnerability by triggering the
0x8001E00C IOCTL code of the ImfHpRegFilter.sys
driver.

## Vulnerability

The 0x8001E00C IOCTL code of the ImfHpRegFilter.sys
driver is vulnerable to DoS, leading to a BSOD of
the affected computer caused by a NULL pointer
dereference.

```bash
rax=0000000000000000 rbx=0000000000000000 rcx=fffff8012ff33404
rdx=0000000000002e20 rsi=0000000000000000 rdi=0000000000000000
rip=fffff8012ff3279e rsp=fffff68d04cca720 rbp=0000000000000000
 r8=fffff68d04cca7a8  r9=0000000000000000 r10=4141414141414141
r11=ffffca879f3d5010 r12=0000000000000000 r13=0000000000000000
r14=0000000000000000 r15=0000000000000000
iopl=0         nv up ei ng nz ac po nc
ImfHpRegFilter+0x279e:
fffff801`2ff3279e 66392b          cmp     word ptr [rbx],bp ds:00000000`00000000=????
```

## Our security policy

We have reserved the ID CVE-2024-0430 to refer
to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: IObit Malware Fighter v11.0.0.1274
* Operating System: Windows

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://www.iobit.com/en/index.php>

**Product page** <https://www.iobit.com/en/malware-fighter.php>

## Timeline

<time-lapse
discovered="2024-01-11"
contacted="2024-01-11"
disclosure="2024-01-16">
</time-lapse>
