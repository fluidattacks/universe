---
slug: advisories/fitzgerald/
title: Twister Antivirus v8.17 - Out-of-bounds Read
authors: Andres Roldan
writer: aroldan
codename: fitzgerald
product: Twister Antivirus v8.17
date: 2024-02-06 12:00 COT
cveid: CVE-2024-1140
severity: 5.8
description: Twister Antivirus v8.17 - Out-of-bounds Read and DoS
keywords: Fluid Attacks, Security, Vulnerabilities, OOB, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | Twister Antivirus v8.17 - Out-of-bounds Read |
| **Code name** | [Fitzgerald](https://en.wikipedia.org/wiki/Ella_Fitzgerald) |
| **Product** | Twister Antivirus |
| **Vendor** | Filseclab |
| **Affected versions** | Version 8.17 |
| **State** | Public |
| **Release date** | 2024-02-06 |

## Vulnerability

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Out-of-bounds Read |
| **Rule** | [111. Out-of-bounds Read](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-111/) |
| **Remote** | No |
| **CVSSv3 Vector** | CVSS:3.1/AV:L/AC:H/PR:L/UI:N/S:U/C:L/I:L/A:H |
| **CVSSv3 Base Score** | 5.8 |
| **Exploit available** | Yes |
| **CVE ID(s)** | [CVE-2024-1140](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-1140) |

## Description

Twister Antivirus v8.17 is vulnerable to
an Out-of-bounds Read vulnerability by triggering the
`0x801120B8` IOCTL code of the `filmfd.sys` driver.

## Vulnerability

The `0x801120B8` IOCTL code of the `filmfd.sys` driver
driver allows to perform a Out-of-bounds read of a page
which is allocated next to the vulnerable buffer.
When issuing a `0x801120B8` IOCTL request with
NULL `lpInBuffer` and a short `lpOutBuffer`,
the out-of-bounds read occur at `filmfd+0xf3f8`
when trying to dereference `0x420` bytes from the
`lpOutBuffer` buffer which is controlled by the user.
This leads to a Denial of Service if the dereferenced
address contains invalid memory. If the attacker can
control the allocation of objects adjacent to the
vulnerable buffer, this may be upgraded to a
more powerful primitive.

The resulting debugging session is the following:

```c
0: kd> bp filmfd+0xf3f8 ".printf \"rax = 0x%p\", @rax; .echo; dps rax+420 L1; .echo; gc"
0: kd> g
rax = 0xffffa28408942640
ffffa284`08942a60  00000000`00000000

rax = 0xffffa2840be6ed00
ffffa284`0be6f120  00000000`00000000

rax = 0xffffa28407da4a40
ffffa284`07da4e60  00000000`00000000

rax = 0xffffa28407ea0d00
ffffa284`07ea1120  ffffa284`0b948158

rax = 0xffffa2840b941d00
ffffa284`0b942120  ????????`????????

KDTARGET: Refreshing KD connection

*** Fatal System Error: 0x00000050
                       (0xFFFFA2840B942120,0x0000000000000000,0xFFFFF800696BF3F8,0x0000000000000002)

Driver at fault:
***    filmfd.sys - Address FFFFF800696BF3F8 base at FFFFF800696B0000, DateStamp 5166646c
.
Break instruction exception - code 80000003 (first chance)

A fatal system error has occurred.

PAGE_FAULT_IN_NONPAGED_AREA (50)
Invalid system memory was referenced.  This cannot be protected by try-except.
Typically the address is just plain bad or it is pointing at freed memory.
Arguments:
Arg1: ffffd40cdd345220, memory referenced.
Arg2: 0000000000000000, X64: bit 0 set if the fault was due to a not-present PTE.
    bit 1 is set if the fault was due to a write, clear if a read.
    bit 3 is set if the processor decided the fault was due to a corrupted PTE.
    bit 4 is set if the fault was due to attempted execute of a no-execute PTE.
    - ARM64: bit 1 is set if the fault was due to a write, clear if a read.
    bit 3 is set if the fault was due to attempted execute of a no-execute PTE.
Arg3: ffffd40cdd344e10, If non-zero, the instruction address which referenced the bad memory
    address.
Arg4: 0000000000000002, (reserved)

rax=ffffd40cdd344e00 rbx=0000000000000000 rcx=fffff800712d0000
rdx=ffffd40cdd344e00 rsi=0000000000000000 rdi=0000000000000000
rip=fffff800712df3f8 rsp=ffffee096cc226c0 rbp=0000000000000002
 r8=ffffd40cdd344e10  r9=ffffd40cd9b0ba70 r10=fffff800712dfdd0
r11=0000000000000000 r12=0000000000000000 r13=0000000000000000
r14=0000000000000000 r15=0000000000000000
iopl=0         nv up ei ng nz na pe nc
filmfd+0xf3f8:
fffff800`712df3f8 8b8020040000    mov     eax,dword ptr [rax+420h] ds:ffffd40c`dd345220=????????
Resetting default scope
```

## Our security policy

We have reserved the ID CVE-2024-1140 to refer
to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Twister Antivirus v8.17
* Operating System: Windows

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <http://www.filseclab.com/en-us/index.htm>

**Product page** <http://www.filseclab.com/en-us/products/twister.htm>

## Timeline

<time-lapse
discovered="2024-01-30"
contacted="2024-01-30"
disclosure="2024-02-06">
</time-lapse>
