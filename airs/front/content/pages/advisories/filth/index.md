---
slug: advisories/filth/
title: Online Book Store Project v1.0 - Unauthenticated SQL Injection (SQLi)
authors: Andres Roldan
writer: aroldan
codename: Filth
product: Online Book Store Project v1.0
date: 2023-09-28 12:00 COT
cveid: CVE-2023-43739
severity: 9.8
description: Online Book Store Project v1.0 - Unauthenticated SQL Injection (SQLi)
keywords: Fluid Attacks, Security, Vulnerabilities, SQLi, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

<summary-table
    name="Online Book Store Project v1.0 - Unauthenticated SQL Injection (SQLi)"
    code="[Filth](https://en.wikipedia.org/wiki/Dani_Filth)"
    product="Online Book Store Project"
    vendor="Projectworlds Pvt. Limited"
    affected-versions="Version 1.0"
    fixed-versions=""
    state="Public"
    release="2023-09-28">
</summary-table>

## Vulnerabilities

<vulnerability-table
    kind="Unauthenticated SQL Injection (SQLi)"
    rule="[146. SQL injection](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-146)"
    remote="Yes"
    vector="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H"
    score="9.8"
    available="Yes"
    id="[CVE-2023-43739](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-43739)">
</vulnerability-table>

## Description

The 'bookisbn' parameter of the cart.php resource
does not validate the characters received and they
are sent unfiltered to the database.
The relevant vulnerable code is:

cart.php:

```php
if(isset($_SESSION['cart']) && (array_count_values($_SESSION['cart']))){
    $_SESSION['total_price'] = total_price($_SESSION['cart']);
    $_SESSION['total_items'] = total_items($_SESSION['cart']);
```

cart_functions.php:

```php
function total_price($cart){
    $price = 0.0;
    if(is_array($cart)){
        foreach($cart as $isbn => $qty){
            $bookprice = getbookprice($isbn);
            if($bookprice){
                $price += $bookprice * $qty;
            }
        }
    }
    return $price;
}
```

database_functions.php:

```php
function getbookprice($isbn){
    $conn = db_connect();
    $query = "SELECT book_price FROM books WHERE book_isbn = '$isbn'";
    $result = mysqli_query($conn, $query);
    if(!$result){
        echo "get book price failed! " . mysqli_error($conn);
        exit;
    }
    $row = mysqli_fetch_assoc($result);
    return $row['book_price'];
}
```

## Our security policy

We have reserved the ID CVE-2023-43739
to refer
to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Online Book Store Project v1.0
* Operating System: Any

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://projectworlds.in/>

## Timeline

<time-lapse
discovered="2023-09-21"
contacted="2023-09-21"
disclosure="2023-09-28">
</time-lapse>
