---
slug: advisories/freebird/
title: Teedy v1.11 - Stored cross-site scripting (XSS)
authors: Renzo Machado
writer: rmachado
codename: Free_Bird
product: Teedy v1.11 -  Stored cross-site scripting (XSS)
date: 2023-09-25 12:00 COT
cveid: CVE-2023-4892
severity: 5.7
description: Teedy v1.11   -   Stored cross-site scripting (XSS)
keywords: Fluid Attacks, Security, Vulnerabilities, XSS, Teedy, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

<summary-table
    name="Teedy v1.11 - Stored cross-site scripting (XSS)"
    code="[Free Bird](https://en.wikipedia.org/wiki/Free_Bird)"
    product="Teedy"
    affected-versions="Version 1.11"
    state="Public"
    release="2023-09-25">
</summary-table>

## Vulnerability

<vulnerability-table
    kind="Stored cross-site scripting (XSS)"
    rule="[010. Stored cross-site scripting](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-010)"
    remote="Yes"
    vector="CVSS:3.0/AV:N/AC:L/PR:L/UI:R/S:U/C:H/I:N/A:N"
    score="5.7"
    available="Yes"
    id="[CVE-2023-4892](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-4892)">
</vulnerability-table>

## Description

Teedy v1.11 has a vulnerability in its text editor that allows events
to be executed in HTML tags that an attacker could manipulate. Thanks
to this, it is possible to execute malicious JavaScript in the webapp.

## Vulnerability

A Stored cross-site scripting (XSS) vulnerability was identified in
Teedy v1.11, which can allow an attacker to steal data such as session
cookies or even steal private documents from other users. This vulnerability
can be exploited in case a user wants to modify the malicious document created
by the attacker.

## Exploitation

To exploit this vulnerability go to Home -> Add a document -> Edit -> Save -> Edit
the created document (this is where the XSS is executed).

![teedy-010](https://user-images.githubusercontent.com/81485801/263359061-5f9d37ad-6c9e-48f7-a6ad-bd742a77503b.png)

It should be clarified that the attacker needs access to the platform in order to
exploit this vulnerability. This would be an example of a functional payload:

```html
<img src=x onerror=alert(document.cookie)>
```

This payload only retrieves the user's cookie and displays it on the
screen, but an attacker could send it to a remote server to steal
another user's session.

## Our security policy

We have reserved the ID CVE-2023-4892 to refer to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Teedy v1.11 (2023-03-12)

* Operating System: MacOS

## Mitigation

An updated version of Teedy is available at the vendor page.

## Credits

The vulnerability was discovered by
[Renzo Machado](https://www.linkedin.com/in/renzo-neomar-machado-50881a235/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://teedy.io>

## Timeline

<time-lapse
discovered="2023-09-11"
contacted="2023-09-12"
replied=""
confirmed=""
patched=""
disclosure="2023-09-25">
</time-lapse>
