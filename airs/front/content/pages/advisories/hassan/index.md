---
slug: advisories/hassan/
title: Zemana AntiLogger v2.74.204.664 - DoS
authors: Andres Roldan
writer: aroldan
codename: hassan
product: Zemana AntiLogger v2.74.204.664
date: 2024-03-14 12:00 COT
cveid: CVE-2024-2204
severity: 5.5
description: Zemana AntiLogger v2.74.204.664 - Denial of Service Vulnerability
keywords: Fluid Attacks, Security, Vulnerabilities, Leak, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | Zemana AntiLogger v2.74.204.664 - Denial of Service (DoS) |
| **Code name** | [Hassan](https://www.last.fm/pt/music/Georgina+Hassan/+wiki) |
| **Product** | Zemana AntiLogger |
| **Vendor** | Zemana Ltd. |
| **Affected versions** | Version 2.74.204.664 |
| **State** | Public |
| **Release date** | 2024-03-14 |

## Vulnerability

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Denial of Service (DoS) |
| **Rule** | [002. Asymmetric Denial of Service](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-002/) |
| **Remote** | No |
| **CVSSv3 Vector** | CVSS:3.1/AV:L/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:H |
| **CVSSv3 Base Score** | 5.5 |
| **Exploit available** | Yes |
| **CVE ID(s)** | [CVE-2024-2204](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-2204) |

## Description

Zemana AntiLogger v2.74.204.664 is vulnerable to
a Denial of Service (DoS) vulnerability by
triggering the `0x80002004` and `0x80002010` IOCTL
codes of the `zam64.sys` and `zamguard64.sys` drivers.

## Vulnerability

The `0x80002004` and `0x80002010` IOCTL codes of the
`zam64.sys` and `zamguard64.sys` drivers are
vulnerable to Denial of Service (DoS) leading to a BSOD of
the affected computer caused by a NULL pointer
dereference.

In order to perform calls to any IOCTL of the
`zam64.sys` and `zamguard64.sys` driver, a call to the
IOCTL `0x80002010` must be performed
with the current process ID as an authorized IOCTL process
caller:

```c
if ( IoctlCode != 0x80002010 )
{
    if ( IoctlCode + 0x7FFFDFAC > 0x10
    || (CurrentStackLocation = 0x11001i64, !_bittest((const int *)&CurrentStackLocation, IoctlCode + 0x7FFFDFAC)) )
    {
    if ( (unsigned int)sub_140009BE4(CurrentStackLocation, "Main.c") && !(unsigned int)sub_140009BEC(v6, 1i64) )
    {
        v3 = 0xC0000022;
        DnsPrint_RpcZoneInfo(
        7,
        (unsigned int)"Main.c",
        0x1E2,
        (unsigned int)"DeviceIoControlHandler",
        0xC0000022,
        "ProcessID %d is not authorized to send IOCTLs ",
        v6);
        goto LABEL_79;
    }
    }
}
```

The handling code of the `0x80002010` IOCTL calls
`sub_14000F4B4`:

```c
__int64 __fastcall sub_14000F4B4(unsigned int *SystemBuffer, __int64 a2, _DWORD *a3, _DWORD *a4)
{
  struct _UNICODE_STRING *v4; // rdi
  unsigned int v8; // eax
  __int64 result; // rax
  __int64 v10; // [rsp+50h] [rbp+8h] BYREF

  v10 = 0i64;
  v4 = (struct _UNICODE_STRING *)(SystemBuffer + 0x1D);  // [1]
  DnsPrint_RpcZoneInfo(
    1,
    (unsigned int)"IoctlHandler.c",
    0x16,
    (unsigned int)"IoctlCreateFileBypassFilters",
    0,
    "Request %ws",
    SystemBuffer + 0x1D);  // [2]
  v8 = SystemBuffer[2];
  if ( (v8 & 2) != 0 || (v8 & 0x100) != 0 )
    sub_14000C33C(v4, *SystemBuffer);
  result = sub_14000AC2C(v4, *SystemBuffer, SystemBuffer[1], 1, SystemBuffer[2], 7u, SystemBuffer[3], (void **)&v10);  //[3]
```

That function receives the `SystemBuffer`
variable as a first parameter. When the
`nInBufferSize` value of the IOCTL request call is 0
and the `lpInBuffer` is NULL, the value of `SystemBuffer`
is also 0. However, there are not checks (`[1], [2], [3]`)
for such case before trying to dereference the variable.
The result is a NULL pointer dereference:

```powershell
CONTEXT:  ffffa5848e18dd60 -- (.cxr 0xffffa5848e18dd60)
rax=0000000000000001 rbx=0000000000000000 rcx=0000000000000000
rdx=0000000000000000 rsi=0000000000000000 rdi=00000000c0000001
rip=fffff803392e0284 rsp=ffffa5848e18e760 rbp=0000000000001910
 r8=0000000000000208  r9=fffff803392f06b0 r10=0000000000000001
r11=0000000000000000 r12=0000000000000000 r13=fffff803392f06b0
r14=0000000000000000 r15=ffffa7053c4fd5b0
iopl=0         nv up ei pl nz na pe nc
cs=0010  ss=0018  ds=002b  es=002b  fs=0053  gs=002b             efl=00050202
zam64+0x10284:
fffff803`392e0284 8b0b            mov     ecx,dword ptr [rbx] ds:002b:00000000`00000000=????????
Resetting default scope

PROCESS_NAME:  IOCTLBruteForce.exe

STACK_TEXT:
ffffa584`8e18e760 fffff803`392e0701     : ffffd186`282fb928 00000000`00000000 00000000`00000000 00000000`00000000 : zam64+0x10284
ffffa584`8e18e790 fffff803`334d1f35     : ffffa705`00000000 ffffa705`3c4fd5b0 00000000`00000002 00000000`00000001 : zam64+0x10701
```

## Our security policy

We have reserved the ID CVE-2024-2204 to refer
to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Zemana AntiLogger v2.74.204.664
* Operating System: Windows

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://zemana.com/>

**Product page** <https://zemana.com/us/antilogger.html>

## Timeline

<time-lapse
discovered="2024-02-23"
contacted="2024-03-05"
disclosure="2024-03-14">
</time-lapse>
