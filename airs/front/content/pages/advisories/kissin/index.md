---
slug: advisories/kissin/
title: Student Information System v1.0 - 3 SQLi
authors: Andres Roldan
writer: aroldan
codename: kissin
product: Student Information System v1.0
date: 2023-12-06 12:00 COT
cveid: CVE-2023-5010,CVE-2023-5011,CVE-2023-5007
severity: 8.8
description: Student Information System v1.0 - Multiple Authenticated SQL Injections (SQLi)
keywords: Fluid Attacks, Security, Vulnerabilities, SQLi, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | Student Information System v1.0 - Multiple Authenticated SQL Injections (SQLi) |
| **Code name** | [Kissin](https://en.wikipedia.org/wiki/Evgeny_Kissin) |
| **Product** | Student Information System |
| **Vendor** | Kashipara Group |
| **Affected versions** | Version 1.0 |
| **State** | Public |
| **Release date** | 2023-12-06 |

## Vulnerabilities

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Authenticated SQL Injections (SQLi) |
| **Rule** | [146. SQL Injection](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-146) |
| **Remote** | Yes |
| **CVSSv3 Vector** | CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:H/A:H |
| **CVSSv3 Base Score** | 8.8 |
| **Exploit available** | Yes |
| **CVE ID(s)** | [CVE-2023-5010](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-5010), [CVE-2023-5011](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-5011), [CVE-2023-5007](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-5007) |

## Description

Student Information System v1.0 is vulnerable to
multiple Authenticated SQL Injection vulnerabilities.

## Vulnerabilities

### CVE-2023-5010

The 'coursecode' parameter of the marks.php
resource does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$coursecode=$_POST['coursecode'];
$coursename=$_POST['coursename'];

$seatlimit=$_POST['seatlimit'];
if($_GET['del']=='edit')
      {
              mysqli_query($con,"UPDATE `marks` SET `courseCode`='$coursecode',`courseName`='$coursename',`noofSeats`=$seatlimit WHERE id = ".$_GET['id']);
                  $_SESSION['delmsg']=" Edited !!";
      }
```

### CVE-2023-5011

The 'coursename' parameter of the marks.php
resource does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$coursecode=$_POST['coursecode'];
$coursename=$_POST['coursename'];

$seatlimit=$_POST['seatlimit'];
if($_GET['del']=='edit')
      {
              mysqli_query($con,"UPDATE `marks` SET `courseCode`='$coursecode',`courseName`='$coursename',`noofSeats`=$seatlimit WHERE id = ".$_GET['id']);
                  $_SESSION['delmsg']=" Edited !!";
      }
```

### CVE-2023-5007

The 'id' parameter of the marks.php
resource does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
if($_GET['del']=='delete')
    {
              mysqli_query($con,"delete from marks where id = '".$_GET['id']."'");
                  $_SESSION['delmsg']=" deleted !!";
        }
```

## Our security policy

We have reserved the IDs CVE-2023-5010,
CVE-2023-5011 and CVE-2023-5007 to refer
to these issues from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Student Information System v1.0
* Operating System: Any

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://www.kashipara.com/>

## Timeline

<time-lapse
discovered="2023-11-22"
contacted="2023-11-22"
disclosure="2023-12-06">
</time-lapse>
