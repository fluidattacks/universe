---
slug: advisories/gilels/
title: Student Result Management System - SQLi
authors: Andres Roldan
writer: aroldan
codename: gilels
product: Student Result Management System v1.0
date: 2023-12-06 12:00 COT
cveid: CVE-2023-48716,CVE-2023-48718,CVE-2023-48720,CVE-2023-48722
severity: 9.8
description: Student Result Management System v1.0 - Multiple Unauthenticated SQL Injections (SQLi)
keywords: Fluid Attacks, Security, Vulnerabilities, SQLi, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | Student Result Management System v1.0 - Multiple Unauthenticated SQL Injections (SQLi) |
| **Code name** | [Gilels](https://en.wikipedia.org/wiki/Emil_Gilels) |
| **Product** | Student Result Management System |
| **Vendor** | Projectworlds Pvt. Limited |
| **Affected versions** | Version 1.0 |
| **State** | Public |
| **Release date** | 2023-12-06 |

## Vulnerabilities

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Unauthenticated SQL Injections (SQLi) |
| **Rule** | [146. SQL Injection](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-146) |
| **Remote** | Yes |
| **CVSSv3 Vector** | CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H |
| **CVSSv3 Base Score** | 9.8 |
| **Exploit available** | Yes |
| **CVE ID(s)** | [CVE-2023-48716](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-48716), [CVE-2023-48718](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-48718), [CVE-2023-48720](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-48720), [CVE-2023-48722](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-48722) |

## Description

Student Result Management System v1.0 is vulnerable to
multiple Unauthenticated SQL Injection vulnerabilities.

## Vulnerabilities

### CVE-2023-48716

The 'class_id' parameter of the add_classes.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$name=$_POST["class_name"];
$id=$_POST["class_id"];

// validation
if (empty($name) or empty($id) or preg_match("/[a-z]/i",$id)) {
    if(empty($name))
        echo '<p class="error">Please enter class</p>';
    if(empty($id))
        echo '<p class="error">Please enter class id</p>';
    if(preg_match("/[a-z]/i",$id))
        echo '<p class="error">Please enter valid class id</p>';
    exit();
}

$sql = "INSERT INTO `class` (`name`, `id`) VALUES ('$name', '$id')";
$result=mysqli_query($conn,$sql);
```

### CVE-2023-48718

The 'class_name' parameter of the add_students.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$name=$_POST['student_name'];
$rno=$_POST['roll_no'];
if(!isset($_POST['class_name']))
    $class_name=null;
else
    $class_name=$_POST['class_name'];

// validation
if (empty($name) or empty($rno) or empty($class_name) or preg_match("/[a-z]/i",$rno) or !preg_match("/^[a-zA-Z ]*$/",$name)) {
    if(empty($name))
        echo '<p class="error">Please enter name</p>';
    if(empty($class_name))
        echo '<p class="error">Please select your class</p>';
    if(empty($rno))
        echo '<p class="error">Please enter your roll number</p>';
    if(preg_match("/[a-z]/i",$rno))
        echo '<p class="error">Please enter valid roll number</p>';
    if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
            echo '<p class="error">No numbers or symbols allowed in name</p>';
            }
    exit();
}

$sql = "INSERT INTO `students` (`name`, `rno`, `class_name`) VALUES ('$name', '$rno', '$class_name')";
$result=mysqli_query($conn,$sql);
```

### CVE-2023-48720

The 'password' parameter of the login.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$username=$_POST["userid"];
$password=$_POST["password"];
$sql = "SELECT userid FROM admin_login WHERE userid='$username' and password = '$password'";
$result=mysqli_query($conn,$sql);
```

### CVE-2023-48722

The 'class_name' parameter of the add_results.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
if(!isset($_POST['class_name']))
    $class_name=null;
else
    $class_name=$_POST['class_name'];
...
// validation
if (empty($class_name) or empty($rno) or $p1>100 or  $p2>100 or $p3>100 or $p4>100 or $p5>100 or $p1<0 or  $p2<0 or $p3<0 or $p4<0 or $p5<0 ) {
    if(empty($class_name))
        echo '<p class="error">Please select class</p>';
    if(empty($rno))
        echo '<p class="error">Please enter roll number</p>';
    if(preg_match("/[a-z]/i",$rno))
        echo '<p class="error">Please enter valid roll number</p>';
    if(preg_match("/[a-z]/i",$marks))
        echo '<p class="error">Please enter valid marks</p>';
    if($p1>100 or  $p2>100 or $p3>100 or $p4>100 or $p5>100 or $p1<0 or  $p2<0 or $p3<0 or $p4<0 or $p5<0)
        echo '<p class="error">Please enter valid marks</p>';
    exit();
}

$name=mysqli_query($conn,"SELECT `name` FROM `students` WHERE `rno`='$rno' and `class_name`='$class_name'");
```

## Our security policy

We have reserved the IDs CVE-2023-48716, CVE-2023-48718,
CVE-2023-48720 and CVE-2023-48722
to refer to these issues from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Student Result Management System v1.0
* Operating System: Any

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://projectworlds.in/>

## Timeline

<time-lapse
discovered="2023-11-17"
contacted="2023-11-17"
disclosure="2023-12-06">
</time-lapse>
