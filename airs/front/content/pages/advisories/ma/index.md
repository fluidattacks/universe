---
slug: advisories/ma/
title: Online Voting System Project v1.0 - SQLi
authors: Andres Roldan
writer: aroldan
codename: ma
product: Online Voting System Project v1.0
date: 2023-12-07 12:00 COT
cveid: CVE-2023-48433,CVE-2023-48434
severity: 9.8
description: Online Voting System Project v1.0 - Multiple Unauthenticated SQL Injections (SQLi)
keywords: Fluid Attacks, Security, Vulnerabilities, SQLi, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | Online Voting System Project v1.0 - Multiple Unauthenticated SQL Injections (SQLi) |
| **Code name** | [Yo-Yo Ma](https://en.wikipedia.org/wiki/Yo-Yo_Ma) |
| **Product** | Online Voting System Project |
| **Vendor** | Projectworlds Pvt. Limited |
| **Affected versions** | Version 1.0 |
| **State** | Public |
| **Release date** | 2023-12-07 |

## Vulnerabilities

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Unauthenticated SQL Injections (SQLi) |
| **Rule** | [146. SQL Injection](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-146) |
| **Remote** | Yes |
| **CVSSv3 Vector** | CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H |
| **CVSSv3 Base Score** | 9.8 |
| **Exploit available** | Yes |
| **CVE ID(s)** | [CVE-2023-48433](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-48433), [CVE-2023-48434](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-48434) |

## Description

Online Voting System Project v1.0 is vulnerable to
multiple Unauthenticated SQL Injection vulnerabilities.

## Vulnerabilities

### CVE-2023-48433

The 'username' parameter of the login_action.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$sql = mysqli_query($con, 'SELECT * FROM loginusers WHERE username="'.$_POST['username'].'"  AND password="'.md5($_POST['password']).'" AND status="ACTIVE" ' );
```

### CVE-2023-48434

The 'username' parameter of the reg_action.php resource
does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$sq = mysqli_query($con, 'SELECT username FROM loginusers WHERE username="'.$_POST['username'].'"');
```

## Our security policy

We have reserved the IDs CVE-2023-48433 and
CVE-2023-48434 to refer to these issues from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Online Voting System Project v1.0
* Operating System: Any

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://projectworlds.in/>

## Timeline

<time-lapse
discovered="2023-11-16"
contacted="2023-11-16"
disclosure="2023-12-07">
</time-lapse>
