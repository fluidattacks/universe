---
slug: advisories/martin/
title: Leave Management System Project v1.0 - Multiple Authenticated SQL Injections (SQLi)
authors: Andres Roldan
writer: aroldan
codename: Martin
product: Leave Management System Project v1.0
date: 2023-10-27 12:00 COT
cveid: CVE-2023-44480,CVE-2023-44481,CVE-2023-44482
severity: 9.8
description: Leave Management System Project v1.0 - Multiple Authenticated SQL Injections (SQLi)
keywords: Fluid Attacks, Security, Vulnerabilities, SQLi, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | Leave Management System Project v1.0 - Multiple Authenticated SQL Injections (SQLi) |
| **Code name** | [Martin](https://en.wikipedia.org/wiki/George_Martin) |
| **Product** | Leave Management System Project |
| **Vendor** | Projectworlds Pvt. Limited |
| **Affected versions** | Version 1.0 |
| **State** | Public |
| **Release date** | 2023-10-27 |

## Vulnerabilities

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Authenticated SQL Injections (SQLi) |
| **Rule** | [146. SQL Injection](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-146) |
| **Remote** | Yes |
| **CVSSv3 Vector** | CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H |
| **CVSSv3 Base Score** | 9.8 |
| **Exploit available** | Yes |
| **CVE ID(s)** | [CVE-2023-44480](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-44480), [CVE-2023-44481](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-44481), [CVE-2023-44482](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-44482) |

## Description

Leave Management System Project v1.0 is vulnerable to
multiple Authenticated SQL Injection vulnerabilities.

## Vulnerabilities

### CVE-2023-44480

The 'setcasualleave' parameter of the admin/setleaves.php
resource does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$setsickleave = strip_tags(trim($_POST['setsickleave']));
$setearnleave = strip_tags(trim($_POST['setearnleave']));
$setcasualleave = strip_tags(trim($_POST['setcasualleave']));
...
$update = "UPDATE employees SET CasualLeave = '".$setcasualleave."' WHERE Dept = '".$row2["Dept"]."'";
$conn->query($update);
```

### CVE-2023-44481

The 'setearnleave' parameter of the admin/setleaves.php
resource does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$setsickleave = strip_tags(trim($_POST['setsickleave']));
$setearnleave = strip_tags(trim($_POST['setearnleave']));
$setcasualleave = strip_tags(trim($_POST['setcasualleave']));
...
$update = "UPDATE employees SET EarnLeave = '".$setearnleave."' WHERE Dept = '".$row2["Dept"]."'";
$conn->query($update);
```

### CVE-2023-44482

The 'setsickleave' parameter of the admin/setleaves.php
resource does not validate the characters received and they
are sent unfiltered to the database. The vulnerable
code is:

```php
$setsickleave = strip_tags(trim($_POST['setsickleave']));
$setearnleave = strip_tags(trim($_POST['setearnleave']));
$setcasualleave = strip_tags(trim($_POST['setcasualleave']));
...
$update = "UPDATE employees SET SickLeave = '".$setsickleave."'WHERE Dept = '".$row2["Dept"]."'";
$conn->query($update);
```

## Our security policy

We have reserved the IDs CVE-2023-44480,
CVE-2023-44481 and CVE-2023-44482 to refer
to these issues from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Leave Management System Project v1.0
* Operating System: Any

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://projectworlds.in/>

## Timeline

<time-lapse
discovered="2023-09-29"
contacted="2023-09-29"
disclosure="2023-10-27">
</time-lapse>
