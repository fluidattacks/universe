---
slug: advisories/skims-48/
title: Action Network - Reflected cross-site scripting (XSS)
authors: Andres Roldan
writer: aroldan
codename: skims-48
product: Action Network 1.4.
date: 2025-01-03 12:00 COT
cveid: CVE-2025-0779
description: Action Network 1.4. - Reflected cross-site scripting (XSS) Vulnerability
keywords: Fluid Attacks, Security, Vulnerabilities, CVE, SAST
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | Action Network 1.4. - Reflected cross-site scripting (XSS) |
| **Code name** | skims-48 |
| **Product** | Action Network |
| **Affected versions** | Version 1.4. |
| **State** | Private |
| **Release date** | 2025-01-03 |

## Vulnerability

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Reflected cross-site scripting (XSS) |
| **Rule** | [Reflected cross-site scripting (XSS)](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-008) |
| **Remote** | No |
| **CVSSv4 Vector** | CVSS:4.0/AV:N/AC:L/AT:N/PR:N/UI:A/VC:H/VI:L/VA:L/SC:L/SI:L/SA:L/E:U |
| **Exploit available** | No |
| **CVE ID(s)** | [CVE-2025-0779](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2025-0779) |

## Description

Action Network
1.4.
was found to be vulnerable.
The web application dynamically
generates web content without
validating the source of the
potentially untrusted data in
myapp/actionnetwork.php.

## Vulnerability

Skims by Fluid Attacks discovered a
Reflected cross-site scripting (XSS)
in
Action Network
1.4..
The following is the output of the tool:

### Skims output

```powershell
  1459 |
  1460 | et($_REQUEST['type']) && isset($action_list->action_type_plurals[$_REQUEST['type']]) ? $action_list->action_type_plurals[
  1461 | intf(
  1462 | : ""actions"", or plural of action type, which will be searched) */
  1463 | 'actionnetwork'),
  1464 |
  1465 |
  1466 |
  1467 |
  1468 | twork-actions-filter"" method=""get"">
> 1469 | en"" name=""page"" value=""<?php echo $_REQUEST['page'] ?>"" />
  1470 | box"">
  1471 | reen-reader-text"" for=""action-search-input""><?php echo $searchtext; ?>:</label>
  1472 | rch"" id=""action-search-input"" name=""search"" value=""<?php echo isset($_REQUEST['search']) ? stripslashes(esc_attr($_REQUES
  1473 | mit"" id=""action-search-submit"" class=""button"" value=""<?php echo $searchtext; ?>"">
  1474 |
  1475 | t->display(); ?>
  1476 |
  1477 | -options"">
  1478 | onnetwork shortcodes for actions synced via the API can take two additional attributes besides the required <strong>id</s
  1479 | 'The <strong>size</strong> attribute can be set to <strong>full</strong> or <strong>standard</strong> (standard is the de
       ^ Col 22
```

## Our security policy

We have reserved the ID CVE-2025-0779 to refer
to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Action Network
1.4.

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team using
[Skims](../../plans/#essential)

## Timeline

<time-lapse
  discovered="2025-01-03"
  contacted="2025-01-03"
  replied=""
  confirmed=""
  patched=""
  disclosure="">
</time-lapse>
