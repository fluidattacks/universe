---
slug: advisories/starr/
title: Online Movie Ticket Booking System v1.0 - Multiple Unauthenticated SQL Injections (SQLi)
authors: Andres Roldan
writer: aroldan
codename: Starr
product: Online Movie Ticket Booking System v1.0
date: 2023-09-28 12:00 COT
cveid: CVE-2023-44163,CVE-2023-44164,CVE-2023-44166
severity: 9.8
description: Online Movie Ticket Booking System v1.0 - Multiple Unauthenticated SQL Injections (SQLi)
keywords: Fluid Attacks, Security, Vulnerabilities, SQLi, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

<summary-table
    name="Online Movie Ticket Booking System v1.0 -
    Multiple Unauthenticated SQL Injections (SQLi)"
    code="[Starr](https://en.wikipedia.org/wiki/Ringo_Starr)"
    product="Online Movie Ticket Booking System"
    vendor="Projectworlds Pvt. Limited"
    affected-versions="Version 1.0"
    fixed-versions=""
    state="Public"
    release="2023-09-28">
</summary-table>

## Vulnerabilities

<vulnerability-table
    kind="Unauthenticated SQL Injections (SQLi)"
    rule="[146. SQL injection](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-146)"
    remote="Yes"
    vector="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H"
    score="9.8"
    available="Yes"
    id="[CVE-2023-44163](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-44163),
    [CVE-2023-44164](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-44164),
    [CVE-2023-44166](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-44166)">
</vulnerability-table>

## Description

Online Movie Ticket Booking System v1.0 is vulnerable to
multiple Unauthenticated SQL Injection vulnerabilities.

## Vulnerabilities

### CVE-2023-44163

The 'search' parameter of the process_search.php resource
does not validate the characters received and they
are sent unfiltered to the database.
The vulnerable code is:

```html
<?php include('header.php');
extract($_POST);
?>
</div>
<div class="content">
    <?php print_r($rs);?>
    <div class="wrap">
        <div class="content-top">
            <h3>Movies</h3>

            <?php
               $today=date("Y-m-d");
              $qry2=mysqli_query($con,"select DISTINCT movie_name,movie_id,image,cast from tbl_movie where movie_name='".$search."'");
```

### CVE-2023-44164

The 'Email' parameter of the process_login.php resource
does not validate the characters received and they
are sent unfiltered to the database.
The relevant vulnerable code is:

```php
include('config.php');
session_start();
$email = $_POST["Email"];
$pass = $_POST["Password"];
$qry=mysqli_query($con,"select * from tbl_login where username='$email' and password='$pass'");
```

### CVE-2023-44166

The 'age' parameter of the process_registration.php resource
does not validate the characters received and they
are sent unfiltered to the database.
The relevant vulnerable code is:

```php
session_start();
include('config.php');
extract($_POST);
mysqli_query($con,"insert into  tbl_registration values(NULL,'$name','$email','$phone','$age','gender')");
```

## Our security policy

We have reserved the IDs CVE-2023-44163,
CVE-2023-44164 and CVE-2023-44166
to refer to these issues from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Online Movie Ticket Booking System v1.0
* Operating System: Any

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://projectworlds.in/>

## Timeline

<time-lapse
discovered="2023-09-26"
contacted="2023-09-26"
disclosure="2023-09-28">
</time-lapse>
