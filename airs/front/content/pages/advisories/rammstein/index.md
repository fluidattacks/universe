---
slug: advisories/rammstein/
title: Supabase v0.23.06 - SQL Injection
authors: Renzo Machado
writer: rmachado
codename: rammstein
product: Supabase v0.23.06 - SQL Injection
date: 2023-08-1 12:00 COT
cveid: CVE-2023-4122
severity: 8.6
description: Supabase v0.23.06          -         SQL Injection
keywords: Fluid Attacks, Security, Vulnerabilities, SQLI, Supabase, CVE
banner: advisories-bg
advise: yes
template: maskedAdvisory
encrypted: yes
---

## Summary

<summary-table
    name="Supabase v0.23.06 - SQL Injection"
    code="[Rammstein](https://en.wikipedia.org/wiki/Rammstein)"
    product="Supabase"
    affected-versions="Version 0.23.06"
    fixed-versions=""
    state="Public"
    release="2023-07-27">
</summary-table>

## Vulnerability

<vulnerability-table
    kind="SQL Injection"
    rule="[146. SQL injection](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-146)"
    remote="Yes"
    vector="CVSS:3.0/AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:L/A:H"
    score="8.6"
    available="No"
    id="[CVE-2023-4122](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-4122)">
</vulnerability-table>

## Description

Supabase v0.23.06 has a default configuration that allows an attacker to
manipulate the database without being authenticated. This is possible because
the application is vulnerable to SQL injection (SQLi).

## Vulnerability

A SQL injection (SQLi) vulnerability was identified in Supabase v0.23.06 which,
if the default security settings are maintained, would allow any user to execute
SQL statements in the database.

## Exploitation

It is important to make it clear that this vulnerability only occurs if the default
security configuration is left in place. In case it is modified (for example, if
the login functionality is added) the vulnerability would be inaccessible.

These are the steps to exploit the vuln:

1. You must compile the tool in any of the ways I mentioned before.

2. You must open the localhost in the port configured for the tool (in the Brew version
   it is http://localhost:54323/, but in the others it is http://localhost:3000/).

3. Then, go to the projects folder (by default the login is disabled).

4. Using BurpSuite, capture the requests that are generated when
   entering the Projects tab.

5. One of the requests will be a POST to this url
   http://localhost:54323/api/pg-meta/default/query?key=project-read-only,
   this must be sent to the repeater.

6. The body of the json that is sent must be modified. All the text part of the
   query is vulnerable to SQLi, without counting that this endpoint can be reached
   without credentials or any bearer.

## Evidence of exploitation

<iframe
src="https://www.veed.io/embed/a2f98554-1641-4752-91e1-474c07aa95b5"
width="744" height="504" frameborder="0"
title="SQL-Exploitation" webkitallowfullscreen
mozallowfullscreen allowfullscreen loading="lazy"></iframe>

## Our security policy

We have reserved the ID CVE-2023-4122 to refer to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Supabase v0.23.06 (2023-08-01)

* Operating System: MacOS

## Mitigation

An updated version of Supabase is available at the vendor page.

## Credits

The vulnerability was discovered by
[Renzo Machado](https://www.linkedin.com/in/renzo-neomar-machado-50881a235/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://supabase.com/>

**Supabase Security Bulletin (August 2023) | CVEs addressed** <https://supabase.com/blog>

## Timeline

<time-lapse
discovered="2023-07-27"
contacted="2023-05-04"
replied="2023-05-04"
confirmed="2023-05-08"
patched="2023-06-09"
disclosure="2023-06-13">
</time-lapse>
