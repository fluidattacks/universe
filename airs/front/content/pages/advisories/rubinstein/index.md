---
slug: advisories/rubinstein/
title: Student Information System - File Upload
authors: Andres Roldan
writer: aroldan
codename: rubinstein
product: Student Information System v1.0
date: 2023-12-06 12:00 COT
cveid: CVE-2023-4122
severity: 9.9
description: Student Information System v1.0 - Insecure File Upload
keywords: Fluid Attacks, Security, Vulnerabilities, CVE, Advisory
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | Student Information System v1.0 - Insecure File Upload |
| **Code name** | [Rubinstein](https://en.wikipedia.org/wiki/Arthur_Rubinstein) |
| **Product** | Student Information System |
| **Vendor** | Kashipara Group |
| **Affected versions** | Version 1.0 |
| **State** | Public |
| **Release date** | 2023-12-06 |

## Vulnerability

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Insecure File Upload |
| **Rule** | [027. Insecure File Upload](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-027/) |
| **Remote** | Yes |
| **CVSSv3 Vector** | CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:C/C:H/I:H/A:H |
| **CVSSv3 Base Score** | 9.9 |
| **Exploit available** | Yes |
| **CVE ID(s)** | [CVE-2023-4122](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-4122) |

## Description

Student Information System v1.0 is vulnerable to
an Insecure File Upload vulnerability on the 'photo'
parameter of my-profile page, allowing an
authenticated attacker to obtain Remote Code Execution
on the server hosting the application.

## Vulnerability

The 'photo' parameter of the my-profile.php resource
does not validate the contents, extension and type of
the file uploaded as a profile image, leading to an
arbitrary file upload which can be abused to obtain
Remote Code Execution. The vulnerable code is:

```php
if(isset($_POST['submit']))
{
  var_dump($_POST);
$staffname=$_POST['studentname'];
if($_FILES["photo"]["name"]!=""){//it will upload the pic on database
  $photo=$_FILES["photo"]["name"];
  move_uploaded_file($_FILES["photo"]["tmp_name"],"staffphoto/".$_FILES["photo"]["name"]);//photo will move to temp folder
  $ret1=mysqli_query($con,"update staff set staffPhoto='$photo'  where StaffRegno='".$_SESSION['login']."'");
}
```

## Our security policy

We have reserved the ID CVE-2023-4122
to refer to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Student Information System v1.0
* Operating System: Any

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://www.kashipara.com/>

## Timeline

<time-lapse
discovered="2023-11-22"
contacted="2023-11-22"
disclosure="2023-12-06">
</time-lapse>
