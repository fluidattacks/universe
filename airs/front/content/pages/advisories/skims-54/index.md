---
slug: advisories/skims-54/
title: WPC Order Notes for WooCommerce - Reflected cross-site scripting (XSS)
authors: Andres Roldan
writer: aroldan
codename: skims-54
product: WPC Order Notes for WooCommerce 1.5.
date: 2025-01-03 12:00 COT
cveid: CVE-2025-0780
description: WPC Order Notes for WooCommerce 1.5. - Reflected cross-site scripting (XSS) Vulnerability
keywords: Fluid Attacks, Security, Vulnerabilities, CVE, SAST
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | WPC Order Notes for WooCommerce 1.5. - Reflected cross-site scripting (XSS) |
| **Code name** | skims-54 |
| **Product** | WPC Order Notes for WooCommerce |
| **Affected versions** | Version 1.5. |
| **State** | Private |
| **Release date** | 2025-01-03 |

## Vulnerability

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Reflected cross-site scripting (XSS) |
| **Rule** | [Reflected cross-site scripting (XSS)](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-008) |
| **Remote** | No |
| **CVSSv4 Vector** | CVSS:4.0/AV:N/AC:L/AT:N/PR:N/UI:A/VC:H/VI:L/VA:L/SC:L/SI:L/SA:L/E:U |
| **Exploit available** | No |
| **CVE ID(s)** | [CVE-2025-0780](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2025-0780) |

## Description

WPC Order Notes for WooCommerce
1.5.
was found to be vulnerable.
The web application dynamically
generates web content without
validating the source of the
potentially untrusted data in
myapp/wpc-order-notes.php.

## Vulnerability

Skims by Fluid Attacks discovered a
Reflected cross-site scripting (XSS)
in
WPC Order Notes for WooCommerce
1.5..
The following is the output of the tool:

### Skims output

```powershell
  410 |    wp_die();
  411 |   }
  412 |
  413 |   function ajax_update_order_note() {
  414 |    $comment = [
  415 |     'comment_ID'      => sanitize_text_field( $_POST['note_id'] ),
  416 |     'comment_content' => sanitize_text_field( $_POST['note_content'] )
  417 |    ];
  418 |    wp_update_comment( $comment );
  419 |
> 420 |    echo $_POST['note_content'];
  421 |    wp_die();
  422 |   }
  423 |
  424 |   function ajax_add_order_note() {
  425 |    $ids              = explode( '.', $_POST['ids'] );
  426 |    $note_type        = wc_clean( wp_unslash( $_POST['note_type'] ) );
  427 |    $is_customer_note = ( 'customer' === $note_type ) ? 1 : 0;
  428 |
  429 |    foreach ( $ids as $id ) {
  430 |     $order = wc_get_order( $id );
      ^ Col 0
```

## Our security policy

We have reserved the ID CVE-2025-0780 to refer
to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: WPC Order Notes for WooCommerce
1.5.

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team using
[Skims](../../plans/#essential)

## Timeline

<time-lapse
  discovered="2025-01-03"
  contacted="2025-01-03"
  replied=""
  confirmed=""
  patched=""
  disclosure="">
</time-lapse>
