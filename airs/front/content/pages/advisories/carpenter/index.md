---
slug: advisories/carpenter/
title: Online Blood Donation Management System v1.0 - Stored Cross-Site Scripting (XSS)
authors: Andres Roldan
writer: aroldan
codename: Carpenter
product: Online Blood Donation Management System v1.0
date: 2023-10-27 12:00 COT
cveid: CVE-2023-44484
severity: 6.1
description: Online Blood Donation Management System v1.0 - Stored Cross-Site Scripting (XSS)
keywords: Fluid Attacks, Security, Vulnerabilities, XSS, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | Online Blood Donation Management System v1.0 - Stored Cross-Site Scripting (XSS) |
| **Code name** | [Carpenter](https://en.wikipedia.org/wiki/Karen_Carpenter) |
| **Product** | Online Blood Donation Management System |
| **Vendor** | Projectworlds Pvt. Limited |
| **Affected versions** | Version 1.0 |
| **State** | Public |
| **Release date** | 2023-10-27 |

## Vulnerabilities

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Stored Cross-Site Scripting (XSS) |
| **Rule** | [010. Stored cross-site scripting](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-010/) |
| **Remote** | Yes |
| **CVSSv3 Vector** | CVSS:3.1/AV:N/AC:L/PR:N/UI:R/S:C/C:L/I:L/A:N |
| **CVSSv3 Base Score** | 6.1 |
| **Exploit available** | Yes |
| **CVE ID(s)** | [CVE-2023-44484](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-44484) |

## Description

Online Blood Donation Management System v1.0 is vulnerable to
a Stored Cross-Site Scripting vulnerability.
The 'firstName' parameter of the users/register.php resource
is copied into the users/member.php document as plain text
between tags. Any input is echoed unmodified in the
users/member.php response. The vulnerable
code is:

users/register.php:

```php
$firstName = $_POST['firstName'];
$lastName = $_POST['lastName'];
$email = $_POST['email'];
$dob = $_POST['dob'];
$gender = $_POST['gender'];
$address = $_POST['address'];
$city = $_POST['city'];
$mobile = $_POST['mobile'];
$bType = $_POST['blood_group'];

require_once 'php/DBConnect.php';
$db = new DBConnect();
$flag = $db->registerUser($firstName, $lastName, $email, $dob, $gender, $bType, $address, $city, $mobile);
```

users/DBConnect.php:

```php
public function registerUser($firstName,$lastName,$email,$dob,$gender,$b_type,$address,$city,$mobile){
    $stmt = $this->db->prepare("INSERT INTO users (first_name,last_name,email,dob,gender,b_type,address,city,mobile) VALUES (?,?,?,?,?,?,?,?,?)");
    $stmt->execute([$firstName,$lastName,$email,$dob,$gender,$b_type,$address,$city,$mobile]);
    return true;
}
```

users/member.php:

```php
require_once 'php/DBConnect.php';
$db = new DBConnect();
$users = $db->getUsers();
...
<?php include 'layout/_member_layout.php'; ?>
```

layout/_member_layout.php:

```php
<?php foreach($users as $u): $i++;?>
<tr class="<?php if($i%2==0){echo $bg_background;}else{echo 'bg-danger';} ?>">
    <td><?= $u['first_name']." ".$u['last_name']; ?></td>
    <td><?= $u['email']; ?></td>
    <td><?= $u['dob']; ?></td>
    <td><?= $u['gender']; ?></td>
    <td><?= $u['b_type']; ?></td>
    <td><?= wordwrap($u['address'], 26, '<br>'); ?></td>
    <td><?= $u['city']; ?></td>
    <td><?= $u['mobile']; ?></td>
</tr>
```

## Our security policy

We have reserved the ID CVE-2023-44484 to refer
to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Online Blood Donation Management System v1.0
* Operating System: Any

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://projectworlds.in/>

## Timeline

<time-lapse
discovered="2023-09-29"
contacted="2023-09-29"
disclosure="2023-10-27">
</time-lapse>
