---
slug: advisories/ingrosso/
title: Bhima 1.27.0 - Sensitive Information Disclosure via IDOR
authors: Carlos Bello
writer: cbello
codename: ingrosso
product: Bhima 1.27.0 - Sensitive Information Disclosure via IDOR
date: 2023-04-10 12:00 COT
cveid: CVE-2023-0967
severity: 6.5
description: Bhima 1.27.0      -      Sensitive Information Disclosure via IDOR
keywords: Fluid Attacks, Security, Vulnerabilities, IDOR, Bhima, Sensitive Information Disclosure
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

<summary-table
    name="Bhima 1.27.0 - Sensitive Information Disclosure via IDOR"
    code="[Ingrosso](https://en.wikipedia.org/wiki/Sebastian_Ingrosso)"
    product="Bhima"
    affected-versions="Version 1.27.0"
    fixed-versions=""
    state="Public"
    release="2023-04-10">
</summary-table>

## Vulnerability

<vulnerability-table
    kind="Insecure object reference"
    rule="[013. Insecure object reference](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-013)"
    remote="Yes"
    vector="CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:N/A:N"
    score="6.5"
    available="Yes"
    id="[CVE-2023-0967](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-0967)">
</vulnerability-table>

## Description

Bhima version 1.27.0 allows an attacker authenticated with normal user
permissions to view sensitive data of other application users and data
that should only be viewed by the administrator. This is possible because
the application is vulnerable to IDOR, it does not properly validate user
permissions with respect to certain actions the user can perform.

## Vulnerability

This vulnerability occurs because the application is vulnerable to IDOR, it
does not correctly validate user permissions with respect to certain actions
that can be performed by the user.

## Exploitation

### Evidence of exploitation

![idor-leak-data](https://user-images.githubusercontent.com/51862990/220761163-3f561935-f22e-4c40-bc99-ce02afb3c637.gif)

## Our security policy

We have reserved the ID CVE-2023-0967 to refer to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Bhima 1.27.0

* Operating System: GNU/Linux

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by [Carlos
Bello](https://www.linkedin.com/in/carlos-andres-bello) from Fluid Attacks'
Offensive Team.

## References

**Vendor page** <https://github.com/IMA-WorldHealth/bhima/>

## Timeline

<time-lapse
  discovered="2023-02-22"
  contacted="2023-02-22"
  replied="2023-02-22"
  confirmed=""
  patched=""
  disclosure="2023-04-10">
</time-lapse>
