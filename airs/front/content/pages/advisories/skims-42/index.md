---
slug: advisories/skims-42/
title: ﻿WooCommerce Modal Fly Cart + Ajax add to cart - Reflected cross-site scripting (XSS)
authors: Andres Roldan
writer: aroldan
codename: skims-42
product: ﻿WooCommerce Modal Fly Cart + Ajax add to cart 1.1.1
date: 2025-01-03 12:00 COT
cveid: CVE-2025-0778
description: ﻿WooCommerce Modal Fly Cart + Ajax add to cart 1.1.1 - Reflected cross-site scripting (XSS) Vulnerability
keywords: Fluid Attacks, Security, Vulnerabilities, CVE, SAST
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | ﻿WooCommerce Modal Fly Cart + Ajax add to cart 1.1.1 - Reflected cross-site scripting (XSS) |
| **Code name** | skims-42 |
| **Product** | ﻿WooCommerce Modal Fly Cart + Ajax add to cart |
| **Affected versions** | Version 1.1.1 |
| **State** | Private |
| **Release date** | 2025-01-03 |

## Vulnerability

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Reflected cross-site scripting (XSS) |
| **Rule** | [Reflected cross-site scripting (XSS)](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-008) |
| **Remote** | No |
| **CVSSv4 Vector** | CVSS:4.0/AV:N/AC:L/AT:N/PR:N/UI:A/VC:H/VI:L/VA:L/SC:L/SI:L/SA:L/E:U |
| **Exploit available** | No |
| **CVE ID(s)** | [CVE-2025-0778](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2025-0778) |

## Description

﻿WooCommerce Modal Fly Cart + Ajax add
to cart
1.1.1
was found to be vulnerable.
The web application dynamically
generates web content without
validating the source of the
potentially untrusted data in
myapp/inc/class-frontend.php.

## Vulnerability

Skims by Fluid Attacks discovered a
Reflected cross-site scripting (XSS)
in
﻿WooCommerce Modal Fly Cart + Ajax add
to cart
1.1.1.
The following is the output of the tool:

### Skims output

```powershell
  129 |    )
  130 |   );
  131 |
  132 |
  133 |  }
  134 |
  135 |
  136 |
  137 |  public function so_27270880_add_variation_to_cart() {
  138 |
> 139 |    echo $_POST['product_id'];
  140 |    exit ;
  141 |    ob_start();
  142 |
  143 |    $product_id        = apply_filters( 'woocommerce_add_to_cart_product_id', absint( $_POST['product_id'] ) );
  144 |    $quantity          = empty( $_POST['quantity'] ) ? 1 : wc_stock_amount( $_POST['quantity'] );
  145 |
  146 |    $variation_id      = isset( $_POST['variation_id'] ) ? absint( $_POST['variation_id'] ) : '';
  147 |    $variations         = ! empty( $_POST['variation'] ) ? (array) $_POST['variation'] : '';
  148 |
  149 |    $passed_validation = apply_filters( 'woocommerce_add_to_cart_validation', true, $product_id, $quantity, $variation_id,
      ^ Col 0
```

## Our security policy

We have reserved the ID CVE-2025-0778 to refer
to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: ﻿WooCommerce Modal Fly Cart + Ajax add
to cart
1.1.1

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team using
[Skims](../../plans/#essential)

## Timeline

<time-lapse
  discovered="2025-01-03"
  contacted="2025-01-03"
  replied=""
  confirmed=""
  patched=""
  disclosure="">
</time-lapse>
