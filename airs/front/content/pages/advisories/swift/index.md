---
slug: advisories/swift/
title: Zod 3.22.2 - Regular expression Denial of Service
authors: Diana Osorio
writer: dosorio
codename: swift
product: Zod 3.22.2 - Regular expression Denial of Service
date: 2023-09-28 12:00 COT
cveid: CVE-2023-4316
severity: 7.5
description: Zod 3.22.2  -  Regular expression Denial of Service
keywords: Fluid Attacks, Security, Vulnerabilities, DOS, ZOD, Re Dos, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

<summary-table
    name="Zod 3.22.2 - Regular expression Denial of Service"
    code="[Swift](https://es.wikipedia.org/wiki/Taylor_Swift)"
    product="Zod"
    affected-versions="Version 3.22.2"
    fixed-versions=""
    state="Public"
    release="2023-09-28">
</summary-table>

## Vulnerability

<vulnerability-table
    kind="Asymmetric denial of service - ReDoS"
    rule="[211. Asymmetric denial of service - ReDoS](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-211/)"
    remote="Yes"
    vector="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:H"
    score="7.5"
    available="Yes"
    id="[CVE-2023-2533](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-4316)">
</vulnerability-table>

## Description

Zod in version 3.22.2 allows an attacker to perform a denial
of service while validating emails

## Vulnerability

Zod performs validations to determine if a string belongs to
a valid email, the validations use of a vulnerable regular
expression that allows an attacker to send a malicious string
of data to generate excessive processing overhead and ultimately
crash the server. It was identified that by sending a string of
data of a predetermined length and increasing the number of
characters, the time it takes for the application to process
the request grows exponentially.

## Exploitation

The application uses the following vulnerable regular expression:

```txt
^([A-Z0-9_+-]+\.?)*[A-Z0-9_+-]@([A-Z0-9][A-Z0-9\-]*\.)+[A-Z]{2,}$
```

First we create a scheme to validate emails:

```js
export const validateSchema = (schema) => (req, res, next) =>{
    try {
        if(req.query.email){
            schema.parse(req.query)
        }else{
            schema.parse(req.body)
        }
        next()
    } catch (error) {
        return res
        .status(400)
        .json({error: error.errors.map((error) => error.message)})
    }
}


export const pdfGeneratorSchema = z.object({
    email: z.string({
        required_error: "Email is required"
    }).email({message: "Invalid email"})
})
```

And perform validation:

```js
router.get('/api/customer/export',  validateSchema(pdfGeneratorSchema), authorized, exportData);
```

## Evidence of exploitation

First we send invalid information and we can see that the
validation done by Zod works correctly, but when we increase
the number of characters in the request for the invalid data,
we can see that the processing time increases until the server
stops responding.

<iframe src="https://www.veed.io/embed/ab824de0-cab1-4632-88f4-13eb0290ec88"
width="835" height="504" frameborder="0" title="Zod-ReDoS"
webkitallowfullscreen mozallowfullscreen allowfullscreen controls></iframe>

## Our security policy

We have reserved the ID CVE-2023-4316 to refer to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Zod 3.22.2

* Operating System: GNU/Linux

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by [Diana
Osorio](https://co.linkedin.com/in/diana-osorio-quiroga) from Fluid Attacks'
Offensive Team.

## References

**Vendor page** <https://zod.dev/>

## Timeline

<time-lapse
  discovered="2023-09-18"
  contacted="2023-09-19"
  replied=""
  confirmed=""
  patched=""
  disclosure="2023-09-28">
</time-lapse>
