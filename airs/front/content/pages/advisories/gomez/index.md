---
slug: advisories/gomez/
title: Zemana AntiLogger - Kernel Memory Leak
authors: Andres Roldan
writer: aroldan
codename: gomez
product: Zemana AntiLogger v2.74.204.664
date: 2024-03-14 12:00 COT
cveid: CVE-2024-2180
severity: 5.5
description: Zemana AntiLogger v2.74.204.664 - Kernel Memory Leak Vulnerability
keywords: Fluid Attacks, Security, Vulnerabilities, Leak, CVE
banner: advisories-bg
advise: yes
template: advisory
---

## Summary

| | |
| --------------------- | ---------------------------------------------------------------------------------|
| **Name** | Zemana AntiLogger v2.74.204.664 - Kernel Memory Leak |
| **Code name** | [Gomez](https://es.wikipedia.org/wiki/Marta_G%C3%B3mez) |
| **Product** | Zemana AntiLogger |
| **Vendor** | Zemana Ltd. |
| **Affected versions** | Version 2.74.204.664 |
| **State** | Public |
| **Release date** | 2024-03-14 |

## Vulnerability

| | |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------|
| **Kind** | Kernel Memory Leak |
| **Rule** | [037. Technical Information Leak](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-037/) |
| **Remote** | No |
| **CVSSv3 Vector** | CVSS:3.1/AV:L/AC:L/PR:L/UI:N/S:U/C:H/I:N/A:N |
| **CVSSv3 Base Score** | 5.5 |
| **Exploit available** | Yes |
| **CVE ID(s)** | [CVE-2024-2180](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-2180) |

## Description

Zemana AntiLogger v2.74.204.664 is vulnerable to
a Memory Information Leak vulnerability by
triggering the `0x80002020` IOCTL code of the
`zam64.sys` and `zamguard64.sys` drivers.

## Vulnerability

The `0x80002020` IOCTL code of the
`zam64.sys` and `zamguard64.sys` drivers allow to leak
the kernel base address, making the `kASLR` protection
useless.

In order to perform calls to any IOCTL of the
`zam64.sys` and `zamguard64.sys` driver, a call to the
IOCTL `0x80002010` must be performed
with the current process ID as an authorized IOCTL process
caller:

```c
if ( IoctlCode != 0x80002010 )
{
    if ( IoctlCode + 0x7FFFDFAC > 0x10
    || (CurrentStackLocation = 0x11001i64, !_bittest((const int *)&CurrentStackLocation, IoctlCode + 0x7FFFDFAC)) )
    {
    if ( (unsigned int)sub_140009BE4(CurrentStackLocation, "Main.c") && !(unsigned int)sub_140009BEC(v6, 1i64) )
    {
        v3 = 0xC0000022;
        DnsPrint_RpcZoneInfo(
        7,
        (unsigned int)"Main.c",
        0x1E2,
        (unsigned int)"DeviceIoControlHandler",
        0xC0000022,
        "ProcessID %d is not authorized to send IOCTLs ",
        v6);
        goto LABEL_79;
    }
    }
}
```

The handling code of the `0x80002020` IOCTL calls
`sub_14000B828` which performs a call to
`ZwQuerySystemInformation`, using
`SystemModuleInformation (0xB)` as first parameter.

```c
if ( ZwQuerySystemInformation(
        SystemModuleInformation,
        SystemInformationClassOutput,
        NumberOfBytes,
        (PULONG)&NumberOfBytes) < 0 )
```

The output buffer is populated with the information
returned by `ZwQuerySystemInformation`. Notice the
first returned `QWORD`:

```powershell
PS C:\Users\admin\Desktop> .\PoC.exe
[+] Bytes returned: 29 (0x1d)
[+] Output (0): FFFFF80252600000
[+] Output (8): 526D65747379535C
[+] Output (16): 747379735C746F6F
[+] Output (24): 0000005C32336D65
```

It matches with the kernel base address as fetched
from the debugger:

```powershell
0: kd> lm m nt
Browse full module list
start             end                 module name
fffff802`52600000 fffff802`53646000   nt
```

## Our security policy

We have reserved the ID CVE-2024-2180 to refer
to this issue from now on.

* https://fluidattacks.com/advisories/policy/

## System Information

* Version: Zemana AntiLogger v2.74.204.664
* Operating System: Windows

## Mitigation

There is currently no patch available for this vulnerability.

## Credits

The vulnerability was discovered by
[Andres Roldan](https://www.linkedin.com/in/andres-roldan/)
from Fluid Attacks' Offensive Team.

## References

**Vendor page** <https://zemana.com/>

**Product page** <https://zemana.com/us/antilogger.html>

## Timeline

<time-lapse
discovered="2024-02-23"
contacted="2024-03-04"
disclosure="2024-03-14">
</time-lapse>
