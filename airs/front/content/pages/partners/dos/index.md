---
slug: partners/dos/
title: DOS
description: Partners like DOS allow us to complete our portfolio and offer better security testing services. Get to know them and become one of them.
keywords: Fluid Attacks, Partners, Dos, Services, Security Testing, Software Development, Red Team, Pentesting, Ethical Hacking
partnerlogo: logo-dos
alt: Logo Dos
partner: yes
---

[DOS](https://www.dos.com.ec/) is a firm with thirty years of experience
in the technology field.
They plan, manage and execute IT integration service projects,
along with consulting services, cloud computing,
and preventive or corrective support,
making it one of the leading IT vendors in Ecuador.
