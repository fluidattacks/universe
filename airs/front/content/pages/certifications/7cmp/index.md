---
slug: certifications/7cmp/
title: 7ASecurity Certified Mobile Professional
description: Our team of ethical hackers proudly holds the 7CMP (7ASecurity Certified Mobile Professional) certification, among many others.
keywords: Fluid Attacks, Ethical Hackers, Red Team, Certifications, Cybersecurity, Pentesters, Whitehat Hackers, Certified Mobile Professional
certificationlogo: logo-7cmp
alt: Logo 7CMP
certification: yes
certificationid: 47
---

[7CMP](https://store.7asecurity.com/products/certified-mobile-professional)
is a mobile penetration testing certification created by 7ASecurity.
The exam's orientation is practical,
so applicants must perform pentesting on real Android and iOS apps.
These must be completed within a predetermined time frame
and culminate in written reports.
7ASecurity offers preliminary courses associated with this certification
that cover and go beyond the OWASP Mobile Top 10.
