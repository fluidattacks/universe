---
slug: certifications/pmpa/
title: Practical Mobile Pentest Associate
description: Our team of ethical hackers proudly holds the PMPA (Practical Mobile Pentest Associate) certification, among many others.
keywords: Fluid Attacks, Ethical Hackers, Red Team, Certifications, Cybersecurity, Pentesters, Whitehat Hackers, PMPA
certificationlogo: logo-pmpa
alt: Logo PMPA
certification: yes
certificationid: 32
---

[PMPA](https://certifications.tcm-sec.com/pmpa/)
is a certification created by TCM Security.
Candidates must prove their ability to perform a mobile app penetration test
at a beginner level.
They have two days to exploit an app on Android
employing static/dynamic analysis skills and web app pentesting skills
and two days to provide a detailed written report.
