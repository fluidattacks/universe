---
slug: certifications/mcrta/
title: Multi-Cloud Red Team Analyst
description: Our team of ethical hackers proudly holds the MCRTA (Multi-Cloud Red Team Analyst) certification, among many others.
keywords: Fluid Attacks, Ethical Hackers, Red Team, Certifications, Cybersecurity, Pentesters, Whitehat Hackers, Multi Cloud Red Team Analyst
certificationlogo: logo-mcrta
alt: Logo MCRTA
certification: yes
certificationid: 57
---

[MCRTA](https://cyberwarfare.live/product/multi-cloud-red-team-analyst-mcrta/)
is a certification created by CyberWarFare Labs.
Individuals awarded MCRTA have completed a course
and passed challenges.
Acing the latter mainly requires skills to perform red teaming operations
on AWS and Azure cloud computing services and Google Cloud Platform
and abuse misconfigurations in those clouds.
