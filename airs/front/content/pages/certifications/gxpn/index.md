---
slug: certifications/gxpn/
title: GIAC Exploit Researcher and Advanced Penetration Tester
description: Our team of ethical hackers proudly holds the GXPN (GIAC Exploit Researcher and Advanced Penetration Tester) certification, among many others.
keywords: Fluid Attacks, Ethical Hackers, Red Team, Certifications, Cybersecurity, Pentesters, Whitehat Hackers, GXPN
certificationlogo: logo-gxpn
alt: Logo GXPN
certification: yes
certificationid: 10
---

[GXPN](https://www.giac.org/certifications/exploit-researcher-advanced-penetration-tester-gxpn/)
is a certification
issued by GIAC Certifications.
Candidates must pass an exam
proving their advanced [penetration testing](../../blog/penetration-testing/)
skills and knowledge about exploitation.
These include bypassing network access control systems
and using protocol fuzzing
to discover weaknesses in a target system,
as well as attacking and exploiting common flaws
in cryptographic implementations.
Completing the exam questions requires hands-on skills
and the performance of tasks mimicking reality.
