---
slug: certifications/ecxd/
title: Certified eXploit Developer
description: Our team of ethical hackers proudly holds the eCXD (Certified eXploit Developer) certification, among many others.
keywords: Fluid Attacks, Ethical Hackers, Red Team, Certifications, Cybersecurity, Pentesters, Whitehat Hackers, ECXD
certificationlogo: logo-ecxd
alt: Logo eCXD
certification: yes
certificationid: 16
---

eCXD is a now retired certification created by INE Security.
It tested the individual's ability
to detect software vulnerabilities.
In addition,
it evaluated their skill
to develop exploits on Linux and Windows.
eCXD tests were based on real-world scenarios.
Subjects under evaluation had to show knowledge
in advanced exploit methodologies.
Moreover,
they had to go further
by devising alternative exploitation paths.
