---
slug: certifications/pipa/
title: Practical IoT Pentest Associate
description: Our team of ethical hackers proudly holds the PIPA (Practical IoT Pentest Associate) certification, among many others.
keywords: Fluid Attacks, Ethical Hackers, Red Team, Certifications, Cybersecurity, Pentesters, Whitehat Hackers, PIPA
certificationlogo: logo-pipa
alt: Logo PIPA
certification: yes
certificationid: 31
---

[PIPA](https://certifications.tcm-sec.com/pjit/)
is a certification created by TCM Security.
Candidates must prove their ability
to perform a firmware review of an IoT device
and write a professional report.
Acing the entry-level practical test requires basic pentesting skills,
but it should be noted
that IoT security and hardware hacking are particularly complex areas.
