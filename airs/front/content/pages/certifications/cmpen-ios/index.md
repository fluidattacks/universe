---
slug: certifications/cmpen-ios/
title: Certified Mobile Pentester (CMPen) – iOS
description: Our team of ethical hackers proudly holds the Certified Mobile Pentester (CMPen) - iOS certification, among many others.
keywords: Fluid Attacks, Ethical Hackers, Red Team, Certifications, Cybersecurity, Pentesters, Whitehat Hackers, Cmpen Ios
certificationlogo: logo-cmpen-ios
alt: Logo CMPen iOS
certification: yes
certificationid: 52
---

[CMPen - iOS](https://secops.group/product/certified-mobile-pentester-cmpen-ios/)
is a certification created by The SecOps Group.
Candidates must pass an intermediate-level exam
demonstrating their knowledge of iOS app security
and performing static and dynamic tests on this kind of application.
They have to solve different challenges,
detect and exploit several vulnerabilities
and obtain some flags.
This certification has no expiration date.
