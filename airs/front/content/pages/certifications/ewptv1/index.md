---
slug: certifications/ewptv1/
title: Web Application Penetration Tester
description: Our team of ethical hackers proudly holds the eWPTv1 (Web Application Penetration Tester) certification, among many others.
keywords: Fluid Attacks, Ethical Hackers, Red Team, Certifications, Cybersecurity, Pentesters, Whitehat Hackers, EWPT
certificationlogo: logo-ewptv1
alt: Logo eWPTv1
certification: yes
certificationid: 19
---

[eWPTv1](https://security.ine.com/certifications/ewpt-certification/)
is a certification created by INE Security.
It is the only certification for Web Application Penetration testers
that evaluates the ability to attack a target.
It assesses
a cybersecurity professional's web application penetration testing skills.
The eWPTv1 certification assesses the expertise of a person
in two main aspects:

- [Penetration testing](../../blog/penetration-testing/)
  processes and methodologies

- Web application analysis and inspection
