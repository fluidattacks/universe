---
slug: certifications/ecre/
title: Certified Reverse Engineer
description: Our team of ethical hackers proudly holds the eCRE (Certified Reverse Engineer) certification, among many others.
keywords: Fluid Attacks, Ethical Hackers, Red Team, Certifications, Cybersecurity, Pentesters, Whitehat Hackers, ECRE
certificationlogo: logo-ecre
alt: Logo eCRE
certification: yes
certificationid: 15
---

eCRE is a now retired certification created by INE Security.
It certifies that
the individual is capable of performing reverse engineering
on Windows-based applications.
Candidates have to pass a challenging theoretical exam
and successfully complete a practical test
where they prove their ability
to analyze complex algorithms and code,
and to bypass different code obfuscation methods.
