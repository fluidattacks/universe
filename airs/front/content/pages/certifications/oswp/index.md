---
slug: certifications/oswp/
title: OffSec Wireless Professional
description: Our team of ethical hackers proudly holds the OSWP (OffSec Wireless Professional) certification, among many others.
keywords: Fluid Attacks, Ethical Hackers, Red Team, Certifications, Cybersecurity, Pentesters, Whitehat Hackers, OSWP
certificationlogo: logo-oswp
alt: Logo OSWP
certification: yes
certificationid: 8
---

[OSWP](https://www.offsec.com/courses/pen-210/)
is the only professional certification
in practical wireless attacks
in the security field today.
In a hands-on exam,
an OSWP must prove they have the skills
to do 802.11 wireless audits
using open source tools.
