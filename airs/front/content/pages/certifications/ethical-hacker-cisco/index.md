---
slug: certifications/ethical-hacker-cisco/
title: Ethical Hacker
description: Our team of ethical hackers proudly holds the Ethical Hacker certification, among many others.
keywords: Fluid Attacks, Ethical Hackers, Red Team, Certifications, Cybersecurity, Pentesters, Whitehat Hackers, Ethical Hacker Cisco Networking Academy
certificationlogo: logo-ethical-hacker-cisco
alt: Logo Ethical Hacker Cisco
certification: yes
certificationid: 60
---

[Ethical Hacker](https://skillsforall.com/course/ethical-hacker?courseLang=en-US)
is a certification created by Cisco Networking Academy.
To earn it,
candidates must pass a written exam consisting of multiple-choice questions.
Succeeding requires a solid understanding of offensive security
as well as how to create a useful report of the findings.
Optionally,
the candidates may first complete the 70-hour, self-paced course,
which offers information using a gamified narrative
and many labs to gain hands-on experience with realistic scenarios.
Both the course and exam are free of cost.
