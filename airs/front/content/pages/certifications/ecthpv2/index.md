---
slug: certifications/ecthpv2/
title: Certified Threat Hunting Professional
description: Our team of ethical hackers proudly holds the eCTHPv2 (Certified Threat Hunting Professional) certification, among many others.
keywords: Fluid Attacks, Ethical Hackers, Red Team, Certifications, Cybersecurity, Pentesters, Whitehat Hackers, ECTHP
certificationlogo: logo-ecthpv2
alt: Logo eCTHPv2
certification: yes
certificationid: 20
---

[eCTHPv2](https://security.ine.com/certifications/ecthp-certification/)
is a certification created by INE Security.
Candidates have to prove their threat hunting
and threat identification capabilities
in a practical test
modeled after real-world corporate network vulnerabilities.
Up-to-date knowledge of advanced attack techniques,
as well as proficiency in event analysis
and network traffic inspection
are required to complete the test successfully.
In addition,
candidates must prove that
they can propose suitable defense strategies.
