---
slug: certifications/capen/
title: Certified AppSec Pentester
description: Our team of ethical hackers proudly holds the Certified AppSec Pentester certification, among many others.
keywords: Fluid Attacks, Ethical Hackers, Red Team, Certifications, Cybersecurity, Pentesters, Whitehat Hackers, Capen
certificationlogo: logo-capen-1
alt: Logo Certified AppSec Pentester
certification: yes
certificationid: 50
---

[CAPen](https://secops.group/product/certified-appsec-pentester/)
is a certification created by The SecOps Group.
Candidates have to prove their knowledge on application pentesting
in a four-hour practical exam.
The exam poses challenges
that involve identifying and exploiting vulnerabilities
and obtaining flags
in an environment that mimics those in real organizations.
