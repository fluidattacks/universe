---
slug: compliance/cwe/
title: CWE
category: compliance
description: At Fluid Attacks, we work and keep up to date with the CWE list for our findings and reports, as well as contribute to community efforts.
keywords: Fluid Attacks, CWE, MITRE, Continuous Hacking, Security, Standards, Ethical Hacking, Pentesting
banner: bg-compliance-internal
template: compliance
---

## Common Weakness Enumeration (CWE)

Today’s complex security landscape demands constant vigilance,
and staying ahead of evolving threats can feel like a juggling act.
Enter CWE,
a community-built resource that empowers organizations
to combat vulnerabilities by standardizing
their identification and mitigation.

## What is CWE?

[CWE](https://cwe.mitre.org/) is a community-developed,
free-to-use list or dictionary
of common hardware and software weaknesses.
CWE functions as a standard language for security tools
and operations that aim to identify, eliminate and prevent weaknesses.
The [U.S. Department of Homeland Security’s](https://www.dhs.gov/) (DHS)
[Cybersecurity and Infrastructure Security Agency](https://www.dhs.gov/cisa/cybersecurity-division)
(CISA) sponsors this project,
which is managed by
the [Homeland Security Systems Engineering and Development Institute](https://www.dhs.gov/science-and-technology/hssedi)
(HSSEDI), which is, in turn, operated by
[The MITRE Corporation](https://www.mitre.org/) (MITRE).

MITRE started its software weakness categorizing efforts in 1999 and,
at that time, created the
CVE (Common Vulnerabilities and Exposures) List.
In 2005,
they evaluated CVE for use in the code assessment industry
and produced the Preliminary List of Vulnerability Examples for Researchers
(PLOVER) document.
In PLOVER, the researchers succeeded in collecting known flaws in the code,
abstracting them and organizing them into common classes.
Later, they established definitions
and descriptions for the groups of weaknesses,
and it was then, in 2006, that the CWE List
and the associated classification taxonomy emerged.

## What is a weakness in CWE?

A weakness in CWE refers to a specific type
of coding flaw or design error that exists
in software or hardware systems.
If it has the potential to be exploited by attackers,
it can eventually become a security vulnerability.
The weaknesses are broken down by CWE IDs,
a unique identifier assigned to the category
(e.g., CWE-356: Product UI does not Warn User of Unsafe Actions),
along with a detailed explanation of the weakness,
its potential consequences,
specific instances of the weakness found
in different software or hardware, likelihood of exploit,
and recommended approaches to mitigate or address the weakness.

CWE has evolved over the years in line with advances in technology,
as well as enterprise concerns and needs.
For this reason,
CWE has been including content for mobile applications
since 2014 and support for hardware weaknesses since 2020.
The most dangerous weaknesses are found
in the [CWE Top 25](https://cwe.mitre.org/top25/),
which are the easiest vulnerabilities
to find and exploit and lead to severe consequences.

## Scoring weaknesses with CWE

CWE scoring refers to methods for assessing
the severity and potential impact of software and hardware weaknesses,
as defined by the CWE list.
It helps prioritize mitigation efforts by identifying
the most critical vulnerabilities
and weaknesses that need to be addressed.
The two primary scoring systems associated with CWE
are CWSS (Common Weakness Scoring System)
and CWRAF (Common Weakness Risk Analysis Framework).

[CWSS](https://cwe.mitre.org/cwss/), developed by MITRE,
is a flexible and open framework for scoring weaknesses based
on three primary metric groups:

- **Base findings:** Measures the inherent risk of the weaknesses,
  accuracy of its discovery, and solidity of controls.
- **Attack surface:** Measures the barriers an attacker must defeat
  to exploit the weakness.
- **Environmental:** Incorporates factors specific to the system
  or context where the weakness exists.

CWSS allows organizations to adjust values
and factors to align with their specific risk tolerance
and security priorities.

[CWRAF](https://cwe.mitre.org/cwraf/), also developed by MITRE,
focuses on scoring weaknesses based on their potential impact
on an organization’s mission or business objectives.
It can be tailored to consider the specific context and consequences
of a weakness for a particular organization.

CWE scoring is not an exact science,
and scores should be used as guidance
rather than absolute measures of risk.
Combining scoring systems with the judgment of experts
who have a thorough understanding of specific vulnerabilities
is crucial for effective risk management.

## What is the difference between CWE, CVE, and OWASP?

While all three are valuable resources for improving software security,
CWE, [CVE](https://cve.mitre.org/index.html)
and [OWASP](https://owasp.org/)
differ in their focus and purpose.
When CWE classifies and describes weaknesses
in software and hardware,
CVE identifies and tracks specific instances of vulnerabilities,
and OWASP provides different resources
like documentation, articles, and tools, among others,
in its effort to improve application security.

They all seek to help the cybercommunity achieve
the best possible software security;
the way they do it is what differs.
CVE and CWE are closely related,
as they are lists that provide a common language
or standard naming for the classification
and categorization of cybersecurity risks,
and OWASP works to improve the security of mobile
and web applications through its sources of reference,
guidelines and recommendations.
They all play a role in increasing awareness about cybersecurity issues
and promote best practices in secure software development.

<div>
<cta-banner
buttontxt="Read more"
link="/solutions/ethical-hacking/"
title="Get started with Fluid Attacks' Ethical Hacking solution right now"
></cta-banner>
</div>

## Fluid Attacks and CWE

Fluid Attacks uses the Common Weakness Enumeration (CWE)
database to categorize and share security findings,
making them compatible with other sources
and boosting your company’s overall security.
Think of CWE as a shared map of cybersecurity vulnerabilities.
By using the same language as other sources,
this map allows you to seamlessly navigate your security landscape
and build a robust defense.
Fluid Attacks contributes to this map by pinpointing,
labeling and classifying weaknesses with the help of
our [ethical hackers](../../solutions/ethical-hacking/)
and our specialized tools.

You can choose the [plan](../../plans/)
that fits your organization’s needs best,
and if you’d like to try out our tools first,
[start a free trial](https://app.fluidattacks.com/) here.
[Contact us](../../contact-us/) at any time.
