---
slug: compliance/cve/
title: CVE
category: compliance
description: At Fluid Attacks, we work and keep up to date with the CVE list for our findings and reports, as well as contribute to community efforts.
keywords: Fluid Attacks, CVE, MITRE, Continuous Hacking, Security, Standards, Ethical Hacking, Pentesting
banner: bg-compliance-internal
template: compliance
---

<div class="paragraph fw3 f3 lh-2">

## Common Vulnerabilities and Exposures (CVE)

The cyberworld is constantly bombarded with malicious attacks
and many companies have paid a hefty price
for not dealing with them properly,
possibly because they were unaware of the issue
or discovered it too late.
That’s why developers and companies
must use all the tools at their disposal;
one such tool is the CVE list.
This is a comprehensive, commonly-recognized and collaborative database
that champions cybersecurity
and helps understand threats and their potential impact.

</div>

<div class="sect2 fw3 f3 lh-2">

### What is CVE?

[CVE](https://cve.mitre.org/index.html) (Common Vulnerabilities and
Exposures) is a free-to-use list of publicly known cybersecurity
vulnerabilities. It was established in 1999 as an international
dictionary with standardized identifiers for vulnerabilities — each
record has an identification number, a description and public
references. This project’s main objectives were to reduce the
inconsistency between cybersecurity databases and tools (there was not
much agreement on identifying security issues) and facilitate data
sharing.

Nowadays, CVE is incorporated into many products and services
worldwide and ensures certainty among stakeholders when communicating
about vulnerabilities. It also provides a useful baseline for evaluating
and comparing tools and services, especially with regards to their
coverage.

## Who sponsors CVE?

The [U.S. Department of Homeland Security’s](https://www.dhs.gov/) (DHS)
[Cybersecurity and Infrastructure Security
Agency](https://www.cisa.gov/cybersecurity-division) (CISA) sponsors
CVE. On the other hand, the American not-for-profit organization
[MITRE](https://www.mitre.org/) copyrighted the CVE List to keep it a
free and open standard and legally protect its use. CVE is not a
vulnerability database but feeds the [NVD
database](https://nvd.nist.gov/) (also sponsored by CISA) and others
with data on all CVE Records. These records' IDs are like this one:
[CVE-2023-4122](/../../advisories/rubinstein/) (a vulnerability discovered by
the Fluid Attacks team in 2023).

## How are CVEs determined?

A vulnerability isn’t automatically assigned a CVE ID after its discovery.
The process can be described like this:
A person (usually a researcher, white hacker, or vendor)
discovers a vulnerability and reports it to a CNA (CVE Numbering Authority).
Then, the CNA analyzes the flaw against the CVE criteria;
if eligible, the CNA assigns a CVE ID and documents the details.
Lastly, the CVE is added to the public MITRE CVE list.

Organizations from all over the world can [partner](https://www.cve.org/PartnerInformation/Partner#CNA)
with the CVE program as CNAs to assign CVE IDs
and publish CVE records for vulnerabilities
within their specific and stipulated scope.
The types of organizations include vendors,
universities, open source, CERTs,
hosted services, bug bounty providers, and associations,
but anyone who wants to report a vulnerability is also welcome.

## CVE IDs’ usage

MITRE is transitioning all its information
to their [new website](https://www.cve.org/),
where they keep everyone informed through newsletters and events.
Two paths can be used to find vulnerabilities listed there:
(a) downloading the latest version of the CVE list,
which can be found [here](https://www.cve.org/Downloads);
(b) through their [search bar](https://cve.mitre.org/cve/search_cve_list.html),
which accepts information that can include CVE IDs,
keywords or product versions.
They regularly update the list to include new vulnerabilities
and their relevant information.

## Goals and benefits of CVEs

CVEs are valuable for improving cybersecurity
by facilitating communication, collaboration,
and effective vulnerability identification and management.
Ultimately, CVE enables users to make informed decisions
about risk mitigation;
its public disclosure fosters transparency
and trust within the cybersecurity community.

The benefits include standardization,
which allows for simple information sharing;
prioritization, which helps focus resources
on the most critical vulnerabilities;
tracking, which helps measure and improve security posture;
and accountability,
which holds vendors responsible for addressing vulnerabilities
and encourages timely patching.
The benefits of CVE IDs can be divided into three categories:

- Unification: CVE IDs provide a common language
  for talking about vulnerabilities,
  making it easier for security professionals to share information
  and coordinate responses.

- Monitoring: CVE IDs can be used to know the type of vulnerability,
  the product, version and vendor involved,
  and its severity and impact,
  as well as to track its status and the availability of remediation patches.

- Tool integration: Many security tools integrate CVE IDs
  into their databases for vulnerability scanning,
  identification and reporting.

## What are the limitations of CVE?

While CVEs offer numerous benefits,
their effectiveness depends on timely disclosure and vendor responsiveness.
The system is not perfect,
as some vulnerabilities may go unreported,
not fulfill CVE criteria (but are still introducing risks to organizations),
or be misclassified.
CVEs alone are not a complete solution but are an important foundation
for effective vulnerability management.

<div>
<cta-banner
buttontxt="Read more"
link="/solutions/vulnerability-management/"
title="Get started with Fluid Attacks' Vulnerability Management
solution right now"
></cta-banner>
</div>

## Fluid Attacks and CVE

CVE is a valuable resource for the management
of vulnerabilities and the minimization
of potential cyberattacks and data breaches.
CVE allows the connection of diverse elements oriented towards cybersecurity.
So, when your company, for example,
hires a [security testing](../../../blog/security-testing-fundamentals/) service
and receives vulnerability reports with CVE records,
you can then access information in other tools,
services or databases (compatible with CVE)
to better understand and remediate the security issues.
Fluid Attacks provides you with such reports.
But we go further given that,
apart from being a CVE Numbering Authority
and a top 10 [CVE lab](https://github.com/fluidattacks/awesome-cvelabs)
worldwide, we have pentesters even to identify
and report your zero-day vulnerabilities.

Fluid Attacks is among the cybersecurity companies that,
for findings and reports, work and stay up to date based on CVE identifiers.
Thus, you can conveniently set a link between the information
we provide you with and many other CVE-compatible sources.
Additionally, Fluid Attacks' red team,
with its skillful ethical hackers dedicated
to discovering zero-day vulnerabilities,
is currently part of the CVE community that feeds the CVE List.

You can [contact us](../../contact-us/)
to get started with a 21-day [free trial](https://app.fluidattacks.com/)
of our Machine plan and later upgrade to
our Advanced [plan](../../plans/).

</div>
