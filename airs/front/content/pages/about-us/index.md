---
slug: about-us/
title: About us
description: Fluid Attacks is a cybersecurity company that offers a comprehensive AppSec solution that integrates automated tools, AI, and pentesters.
keywords: Fluid Attacks, About Us, Company, Ethical Hacking, Pentesting, Cybersecurity
banner: about-us-bg
---

Since **2001**,
Fluid Attacks has been committed to growing as a team
and developing its own technology
to **contribute to global cybersecurity**.

## Our mission

### Help organizations develop and deploy secure software without delays

Our **[Continuous Hacking](../services/continuous-hacking/)**
is an **all-in-one solution**
that combines our own tools,
AI and certified pentesters
throughout the entire software development lifecycle
to identify security vulnerabilities accurately,
keeping false positives and false negatives to a minimum.

In addition to reporting risk exposure promptly,
we support our clients through our **[platform](https://app.fluidattacks.com/)**
to easily understand, manage and fix security issues in their software
and thereby achieve **high remediation rates**
and guarantee high-quality and safe products to their end users.

## Principles

### Fluid Attacks is fully committed to ethical business practices

**Honesty:**
This virtue is paramount to all our business interactions;
it is something we offer to third parties and consequently expect in return.

**Teamwork:**
We achieve our outcomes through collaborative efforts.
Therefore,
we uphold dignified and respectful treatment for all team members.

**Discipline:**
We fulfill all our commitments with unwavering dedication,
perseverance and total focus on excellence on behalf of all our customers.

### How we apply our principles regarding our clients

The following are some examples:

- We only perform ethical hacking with the prior authorization of the client
  who owns the evaluation target.

- We report to the client all security issues we identify in their software.

- We do not disclose information about detected vulnerabilities
  to unauthorized parties inside or outside Fluid Attacks.

- If we make a mistake,
  we inform the client,
  do everything necessary to fix it,
  and take full responsibility for any negative impacts.

- After completing a project,
  we seek to eliminate all types of infections from the client's systems
  that were part of our tests.

- We do not keep records of vulnerabilities
  identified in the client's software
  once a project has been closed.

## Fluid Attacks is a CNA

We are a **CVE Numbering Authority** (CNA)
and a CVE lab [among the top 10 worldwide](https://github.com/fluidattacks/awesome-cvelabs).
Through our research team,
we identify and [disclose security issues](../advisories/)
in open-source software.

## Free trial

Search for vulnerabilities in your apps for free
with Fluid Attacks' automated security testing!
Start your [21-day free trial](https://app.fluidattacks.com/SignUp)
and discover the benefits of the [Continuous Hacking Essential plan](../plans/).
If you prefer the Advanced plan,
which includes the expertise of Fluid Attacks' pentesters,
fill out [this contact form](../contact-us/).
