---
slug: cybersecurity-labeling-for-iot/
title: Cybersecurity Labeling for IoT
date: 2023-12-07
subtitle: Smart devices are to be more secure for consumers
category: politics
tags: cybersecurity, trend, compliance
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1701966723/blog/cybersecurity-labeling-for-iot/cover_cyberlabeling.webp
alt: Photo by Jochen van Wylick on Unsplash
description: Following the steps of some in Europe, the U.S. advances in its cybersecurity labeling program to help consumers choose IoT devices more resistant to attacks.
keywords: Cybersecurity Labeling, Smart Devices, Internet Of Things, Iot Devices, Software Development, Labeling Program, Cyber Trust Mark, Ethical Hacking, Pentesting
author: Jason Chavarría
writer: jchavarria
name: Jason Chavarría
about1: Content Writer and Editor
source: https://unsplash.com/photos/yellow-and-black-robot-toy-Y40Ga3oahAs
---

## IoT devices cybersecurity needs to be better

The number of Internet of things (IoT) devices in the world is big
and on the rise.
By July 2023,
it was [15.14 billion](https://www.statista.com/statistics/1183457/iot-connected-devices-worldwide/).
And it is estimated that,
by the start of the new decade,
they will be about 29.42 billion.
Basically,
their amount seems to almost double every five years.
Right now,
large corporate networks [may have millions](https://technologymagazine.com/articles/cybersecurity-no-longer-one-size-fits-all-in-an-iot-world)
of such smart devices connected to them.
That could be up to 30% of all the connected devices.

Organizations [use IoT devices](https://www.techtarget.com/iotagenda/tip/Top-8-IoT-applications-and-examples-in-business)
for several reasons.
Some,
such as those in the healthcare sector,
find them useful for tracking equipment and tools remotely via sensors
or offering remote support to practitioners in augmented reality.
Others,
like those in the retail sector,
may use IoT devices to boost the efficiency of their logistics
(e.g., monitoring that inventory is
where the inventory system claims it should be).

IoT devices are, however, known to be insecure,
as they generally come with [default](https://medium.com/@OSableProject/dont-just-leave-them-change-them-iot-default-passwords-an-osable-guide-b42a1d1ccfda),
guessable usernames and/or passwords,
use outdated software components,
do not encrypt data,
and do not get regular software updates,
or [these are not always successful](https://onomondo.com/blog/iot-device-update-tips/).
Possibly a part of the fault lies in manufacturers' eagerness
to deploy their devices massively and quickly,
which outweighs any importance given to security tests during development.
Another contributing factor may be the lack of strict government regulations
on the cybersecurity of such devices.

The fact is
that the lack of compliance with security requirements in IoT devices
represents an important risk.
By the start of 2023,
organizations globally received a mean of [59.7 attack attempts weekly](https://blog.checkpoint.com/security/the-tipping-point-exploring-the-surge-in-iot-cyberattacks-plaguing-the-education-sector/)
targeting those devices.
Insecure gadgets,
making up a big chunk of the organizations' attack surfaces,
are then the focus of a lot of activity by malicious hackers.
Indeed,
e.g., highly skilled groups of cybercriminals
who have enough resources to attack their targets repeatedly,
aka advanced persistent threats (APTs),
[are leveraging zero-day security vulnerabilities](https://securelist.com/kaspersky-security-bulletin-apt-predictions-2024/111048/)
in smart home cameras,
connected car systems,
wearable devices, etc.,
to do surveillance and carry out espionage campaigns.

## Cybersecurity labeling program in the United States

The above has motivated nations to include in their cybersecurity strategies
an item regarding actions to increase cybersecurity on smart devices
to protect consumers.
Accordingly,
in the U.S.,
the White House and the Federal Communications Commission (FCC)
created a program,
named [U.S. Cyber Trust Mark](https://www.whitehouse.gov/briefing-room/statements-releases/2023/07/18/biden-harris-administration-announces-cybersecurity-labeling-program-for-smart-devices-to-protect-american-consumers/),
that aims to certify and label Internet-enabled devices as secure
and will possibly be ready to start in late 2024.

The program is part of
the [United States National Cybersecurity Strategy Implementation](https://www.whitehouse.gov/wp-content/uploads/2023/07/National-Cybersecurity-Strategy-Implementation-Plan-WH.gov_.pdf).
It is in the process of establishing guidelines for manufacturers
to follow voluntarily in the products they develop and deploy.
These products include smart televisions,
microwaves,
refrigerators,
fitness trackers,
climate control systems, and more.
The guidelines shall be based on [recommended cybersecurity criteria](https://doi.org/10.6028/NIST.CSWP.02042022-2)
published by National Institute of Standards and Technology (NIST).
Further,
the program proposes
that complying products get a cybersecurity label
"in the form of a distinct shield logo" ([see here the proposed logo](https://www.fcc.gov/cybersecurity-certification-mark)).
This label would help consumers easily choose smart devices
that may be less vulnerable to cyberattacks than those without the label.
And to **help choice** even more,
and also **promote transparency and competition**,
the FCC plans to use a QR code
that consumers can scan
to access a national registry listing the products
with their security information,
which can then be used by the consumer to establish comparisons.

Although  NIST is still working
to define the requirements for consumer-grade routers,
and other agencies are undertaking research
to develop requirements for other smart devices,
we can still summarize,
in our own words,
the recommended criteria that NIST has already made public.

The source we refer to is the white paper from February 4, 2022,
titled "[Recommended Criteria for Cybersecurity Labeling
for Consumer Internet of Things (IoT) Products](https://doi.org/10.6028/NIST.CSWP.02042022-2)."
It is a response to the Executive Order 14028,
"Improving the Nation's Cybersecurity,"
which tasks NIST with,
among other things,
formulating cybersecurity criteria and labeling approaches for IoT.
Most of this agency's criteria can be satisfied
by the IoT product's software and/or hardware,
and some apply to the developer.
We present them briefly as follows:

- **Asset identification:** The product has a unique identifier
  and inventories all of its connected components.

- **Product configuration:** The product's default setting is secure,
  and authorized users, services or components can change settings
  and also revert them to default.

- **Data protection:** The product and its components protect the data
  they store and transmit
  from unauthorized access, disclosure and modification.

- **Interface access control:**
  The product and its components restrict logical access
  to local and network interfaces
  to only the authorized users, services or components.

- **Software update:** There is a secure and configurable mechanism
  to update the software of the product's components
  (even non-executable software data).

- **Cybersecurity state awareness:**
  The product detects cybersecurity incidents
  affecting or effected by its components and the data they store and transmit.

- **Documentation:**
  The developer creates exhaustive documentation on the product
  that consumers can read before purchase
  and mentions information such as the product's intended use,
  compliance and noncompliance with requirements,
  components,
  security tests passed,
  as well as the vendor's methods of receiving reports of vulnerabilities,
  processes for recording reported vulnerabilities,
  policy for responding to such reports,
  policy for disclosing verified vulnerabilities,
  and processes for receiving news from component suppliers
  about changes in the latter's products.

- **Information and query reception:** The developer receives information
  relevant to the cybersecurity of the product
  and responds to queries about it.

- **Information dissemination:** The developer discloses through a channel
  information and events throughout the product's support lifecycle,
  including updates to terms of support,
  needed maintenance,
  new vulnerabilities,
  data breaches,
  as well as other cybersecurity relevant information,
  like steps for vulnerability remediation
  and the developer's security practices and certifications
  related to such practices.

- **Product education and awareness:** The developer educates users
  and creates awareness on the product's cybersecurity related information,
  such as that regarding configuration and patch management to mitigate risks.

<div>
<cta-banner
buttontxt="Read more"
link="/solutions/security-testing/"
title="Get started with Fluid Attacks' Security Testing solution right now"
/>
</div>

This sounds like it's going to be a gigantic step
for IoT devices cybersecurity.
Remarkably,
the [White House's statement](https://www.whitehouse.gov/briefing-room/statements-releases/2023/07/18/biden-harris-administration-announces-cybersecurity-labeling-program-for-smart-devices-to-protect-american-consumers/)
about the program
mentions the manufacturers and retailers
that have announced their support and commitment to the program,
and these include big names.
These are the participants mentioned:
Amazon, Best Buy, Carnegie Mellon University, Cisco Systems,
Connectivity Standards Alliance, Consumer Reports,
Consumer Technology Association, CyLab, Google, Infineon,
the Information Technology Industry Council, IoXT, KeySight,
LG Electronics U.S.A., Logitech, OpenPolicy, Qorvo, Qualcomm,
Samsung Electronics, UL Solutions, Yale and August U.S.

## Cybersecurity labeling programs already running in Europe

By the way,
other nations have already introduced their own labeling programs.
[Finland was the pioneer](https://www.traficom.fi/en/news/finland-becomes-first-european-country-certify-safe-smart-devices-new-cybersecurity-label)
and then Germany followed.
And this is crucial,
since Europe has been reported
as the region whose IoT devices have been [targeted the most in early 2023](https://blog.checkpoint.com/security/the-tipping-point-exploring-the-surge-in-iot-cyberattacks-plaguing-the-education-sector/)
(about 70 attacks per organization every week).
Let's take Germany's case of cybersecurity labeling as an example.
The country's Federal Office for Information Security (BSI in German)
started the program [by the end of 2021](https://www.bsi.bund.de/DE/Service-Navi/Presse/Pressemitteilungen/Presse2021/211208_IT-SiK_Antragsverfahren-startet.html?nn=1069038)
with routers and email services
and now also validates and labels IoT devices
(e.g., Xiaomi's [smart cameras and vacuums](https://www.bsi.bund.de/SiteGlobals/Forms/IT-Sicherheitskennzeichen/IT-Sicherheitskennzeichen_Formular.html)).
The developers who want to participate voluntarily
need to comply with the BSI's criteria.
These are based on the European standard
"[Cyber Security for Consumer Internet of Things: Baseline Requirements](https://www.etsi.org/deliver/etsi_en/303600_303699/303645/02.01.01_60/en_303645v020101p.pdf)"
(the same source for Finland's program),
which is a 2020 document
whose provisions have lots in common with those we described above,
published by NIST years later.
If the product earns the label with the BSI,
[valid for at least two years](https://www.bsi.bund.de/DE/Themen/Unternehmen-und-Organisationen/IT-Sicherheitskennzeichen/fuer-Hersteller/FAQ-IT-SiK-fuer-hersteller_node.html),
it will be visible for consumers,
who will access the security information
by entering the URL in the label or scanning the added QR code.
Just in November this year the BSI launched a nationwide advertising campaign
for consumers to be aware of the label.

This effort in Europe is significant
and can prepare developers for what's looming over them:
The European Union's [Cyber Resilience Act](../cyber-resilience-act/) (CRA)
is expected to take effect next year.
All digital products destined for the European market will need to comply
with cybersecurity requirements throughout their lifecycles.
Fines for infringement,
which includes acts like deception and noncompliance,
will go from about €5M to €15M
(we expand on this regulation in our dedicated blog post).

## IoT is just the beginning

While in the U.S. the labeling program will start with IoT,
we see that in Germany their own has been addressing other software products
as well.
The U.S. may follow these steps soon, though,
since [NIST has also the task](https://www.nist.gov/itl/executive-order-14028-improving-nations-cybersecurity/cybersecurity-labeling-consumers-0)
to identify "secure software development practices or criteria
for a consumer software labeling program."
[Their public 2022 white paper](https://doi.org/10.6028/NIST.CSWP.02042022-1)
already mentions technical criteria
as claims that developers can make about their product.
In a nutshell,
a group of the criteria reads
that the vendor adheres to accepted secure software development practices
throughout the software development lifecycle (SDLC).

How about being ahead of the curve by securing your software now?
In our [Continuous Hacking](../../services/continuous-hacking/) solution,
we use several security testing techniques by default
to verify your application's compliance
with several secure development guidelines and cybersecurity standards.
You can see all results when you log in on our platform
and swiftly manage all vulnerabilities throughout the SDLC.
As we provide you with fix recommendations through several options
(including AI-generated, step-by-step guidance,
and, in our flagship plan, advice from our pentesters),
you can take action fast to fulfill the security requirements needed.
[Start your free trial now](https://app.fluidattacks.com/SignUp).
