---
slug: nebraska-joke-infrastructure-dependency/
title: The Joke on Someone in Nebraska
date: 2024-03-22
subtitle: A digital infrastructure issue that many still ignore
category: opinions
tags: software, code, risk, vulnerability, web, cybersecurity
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1711073708/blog/nebraska-joke-infrastructure-dependency/cover_nebraska_joke_infrastructure_dependency.webp
alt: Photo by Valery Fedotov on Unsplash
description: The joke mentioned here is just a small sample of a big dependency issue in the global digital infrastructure, about which we need to raise awareness.
keywords: Nebraska Joke, Dependency, Digital Infrastructure, Open Source Software, Software Components, Xkcd, Sbom, Pentesting, Ethical Hacking
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Content Writer and Editor
source: https://unsplash.com/photos/brown-wooden-blocks-on-black-table-CqX6IhVj2TI
---

I once read or heard somewhere something to the effect that,
in the digital world,
there are many fundamental problems with structure and security
to which we don't pay enough attention.
It was precisely this statement
one of those that ignited in the recesses of my memory
when I first saw the joke about someone in Nebraska,
around which this blog post revolves:

<div class="imgblock">

![Dependency](https://res.cloudinary.com/fluid-attacks/image/upload/v1711074047/blog/nebraska-joke-infrastructure-dependency/dependency-xkcd.webp)

<div class="title">

"Dependency." [Comic #2347](https://xkcd.com/2347/) on xkcd.

</div>

</div>

## Is that a joke or an alert? —Both

This joke is part of xkcd,
a webcomic created by the American cartoonist,
engineer, and writer Randall Munroe.
What the author is trying to show here is the **dependency between elements**
in an overall digital system or architecture
represented as a tower of blocks
—like a Jenga—
that symbolize software components.
Thus,
as we move up from the tower's bottom,
the higher-level components depend on more and more lower-level components.

Curiously,
at the third level of the tower,
from bottom to top,
we find a thin component that,
we could easily suppose,
if broken, or removed,
would mean the destabilization and the fall of the whole system,
or at least what is up there
(i.e., "all modern digital infrastructure").
What is most peculiar and funny,
but at the same time worrying,
is that,
besides being a thin component,
Randall refers to it as a project maintained by a random person
in a particular place since a specific date.
But what does this mean?

Actually,
it's not that
our systems depend on what a specific individual in Nebraska is doing.
What this joke —at the same time alarm— seems to be aiming at is
to give an example of **a general reality**
that we have been experiencing for many years:
various programs or software components,
some of them small,
maintained mainly due to personal reasons
by one or a few non-famous or unsung people,
anywhere in the world,
support a lot of our current digital infrastructure.

While Randall's cartoon depicts the aforementioned reality,
when we review its caption,
we can read the following:
"Someday ImageMagick will finally break for good
and we'll have a long period of scrambling
as we try to reassemble civilization from the rubble."
This is a specific example,
perhaps exaggerated but illustrative.
**ImageMagick** is a free and open-source software
for processing, creating, displaying, editing and converting raster images,
[created back in 1987](https://en.wikipedia.org/wiki/ImageMagick)
by John Cristy,
which has been and is widely used in various applications,
[especially](https://www.jstage.jst.go.jp/article/abas/21/5/21_0220914a/_pdf)
those intended for web services,
and to which communities of engineers and developers have contributed
over time.

Although many other libraries and APIs can perform ImageMagick's tasks,
this one seems to have become the default package.
The problem with this library is that,
[back in 2016](https://ine.com/blog/imagetragick-a-tragick-image-conversion-tale),
five vulnerabilities were discovered in it
(which ended up carrying the name "[ImageTragick](https://imagetragick.com/)"),
[one of which](https://blog.sucuri.net/2016/05/imagemagick-remote-command-execution-vulnerability.html),
when exploited,
would allow remote code execution (RCE).
**This finding set off alarm bells** among engineers and developers,
signifying a high potential risk
since if ImageMagick were to break down,
lots of programs depending on it would fail,
at least temporarily,
showing an imbalance and collapse in "the tower."

<div>
<cta-banner
buttontxt="Read more"
link="/solutions/vulnerability-management/"
title="Get started with Fluid Attacks' Vulnerability Management solution
right now"
/>
</div>

## Delving deeper into the problem

Nowadays,
many of us know,
although many more are probably unaware of it,
that the smallest and largest companies and organizations around the world
are using open-source software components from third parties.
**Almost all existing software depends on such products**
developed and maintained by individuals and communities of developers.
But why?

One of the main reasons is the now typical proverb,
"**Don't reinvent the wheel**."
That is,
if that human work was already functional enough,
don't waste your time and go on to build something new.
In the software universe,
many people create and share code publicly and for free,
which fulfills specific functions,
some of which are very basic.
So,
others don't have to build their applications from scratch;
they don't need to develop all the components of their products.
They simply resort to what has already been created
and shared by the community,
which allows for cheaper,
simpler and more efficient technological development.

<div class="imgblock">

![Reinvent the Wheel](https://res.cloudinary.com/fluid-attacks/image/upload/v1711074103/blog/nebraska-joke-infrastructure-dependency/reinvent-the-wheel-xkcd.webp)

<div class="title">

"Reinvent the Wheel." [Comic #2140](https://xkcd.com/2140/) on xkcd.

</div>

</div>

In other words,
here we are talking about **reuse** and **modularity**,
principles that have been used for a long time
but are fundamental today in software engineering.
So,
when a group of developers aims to create an application,
they end up building a chain or dependency tree
with multiple open-source software components
previously developed separately by other engineers,
some of them only a few lines of code
that fulfill specific functions.

The problem,
as Randall illustrated,
is that many of these open-source software component projects
that are fundamental to large-scale projects
are often maintained by very few people.
Unlike large enterprises like Linux,
for example,
many other **projects don't get enough attention**.
Those who preserve them are usually experts in them
who often work too much and for little or no money,
mainly as a hobby,
and on whom all the responsibility falls.
However,
for reasons such as weariness or finding better opportunities,
some of these projects may be abandoned.
This drawback,
together with the security vulnerabilities that may be present,
often means a considerable risk
to the integrity and security of "the whole system."

### Heartbleed and left-pad

In addition to the ImageMagick case,
other illustrative cases of this worrying problem have been,
for example,
those of Heartbleed and left-pad.
**[Heartbleed](../my-heart-bleeds/)** is a bug discovered in 2014 in OpenSSL,
a library that enables encrypted communications over the Internet,
for instance,
for e-commerce.
This one,
unlike others,
is a relatively large software component since,
by that time,
it had around 500 thousand lines of code.
As reported [on a simple website](https://heartbleed.com/)
dedicated to this vulnerability,
it allowed "stealing the information protected, under normal conditions,
by the SSL/TLS encryption used to secure the Internet."

Apparently,
this case qualified as irresponsibility
on the part of the OpenSSL team
because they had previously known a latent problem
that they didn't pay enough attention to.
It took about two years to detect Heartbleed.
The worst thing is that,
seemingly,
the maintenance of the affected component depended only on two guys,
both volunteers and overworked.
[It is said that](https://www.jstage.jst.go.jp/article/abas/21/5/21_0220914a/_pdf),
despite what happened,
donations for this project grew very little,
from around $2,000 to $9,000 a year.
(This underfunding is something prevalent
in open-source software development history
and something we wish to emphasize in a future blog post).

<div class="imgblock">

![Heartbleed](https://res.cloudinary.com/fluid-attacks/image/upload/v1711074309/blog/nebraska-joke-infrastructure-dependency/heartbleed-xkcd.webp)

<div class="title">

"Heartbleed." [Comic #1353](https://xkcd.com/1353/) on xkcd.

</div>

</div>

For a similar,
more recent case,
[see Log4j](../log4shell/),
an Apache's open-source library in which a vulnerability was discovered
that allowed remote attackers to execute arbitrary code on exposed systems.

Regarding **[left-pad](https://qz.com/646467/how-one-programmer-broke-the-internet-by-deleting-a-tiny-piece-of-code)**,
it turns out that a young man named Azer Koçulu had been developing code
for npm,
a well-known and widely used web development service
for finding and installing open-source software packages
written in JavaScript.
When npm,
in an imbroglio not worth mentioning here,
apparently acted against the basic principles of open-source culture
regarding a package Koçulu had been working on,
he chose to abandon it,
specifically delete it,
back in 2016,
which led to a meltdown among many projects and websites dependent on it.

So,
engineers and other individuals started receiving error messages
when trying to run their applications and services.
These required a package called left-pad
that npm no longer had in its portfolio
and that some people had never even heard of before.
It was too strange that a package had disappeared.
The most curious thing was that it had only 11 lines of simple code
to fulfill a single function.
The absence of this small library affected many other packages
that depended on it,
directly or indirectly;
among the latter,
React,
a large component widely used in different countries around the world,
even on the Facebook website.
Faced with this havoc,
npm was forced to promptly restore those 11 lines of code.

## How should we deal with this problem?

Everyone working on all components of their own software products?
That's not viable.
What seems more feasible at the moment
is to keep the open-source culture intact
but **contribute to its maintenance and security to a greater extent**.
As many have already said,
why don't the big companies of the capitalist system
that depend on these public and free projects
contribute to their financing or in some other way?
Where is the institutional and governmental support?
All this is still insufficient.

Apart from raising awareness of this situation,
seeking to minimize ignorance about it,
and looking for funding for projects on which we all depend,
many companies from different industrial sectors can also contribute
to the security of our digital infrastructure.
Attention!
**The fact that we don't have to reinvent the wheel doesn't mean
we should overlook it**.
Can we not learn about it?
While we don't have to develop the third-party software components
we use in our products,
we should know about them, review them, update them, improve them.

Here's where the much-talked-about [software bill of materials](../../learn/what-is-software-bill-of-materials/)
(SBOM) comes into play.
Employ it as an organized inventory of all third-party components
used by your application.
From there,
discard those packages that are unnecessary,
request a security vulnerability assessment on the rest,
and, if possible,
**contribute with review**, **code or funds**,
especially to those software components
that perform a critical role in keeping your company alive.

<quote-box>

The current state of our digital infrastructure is one of the most
poorly understood issues of our time.
It is critical that we understand it.
—[Nadia Eghbal](https://www.fordfoundation.org/wp-content/uploads/2016/07/roads-and-bridges-the-unseen-labor-behind-our-digital-infrastructure.pdf)

</quote-box>

If you want to learn how Fluid Attacks can help you with SBOM,
[software composition analysis](../sca-scans/)
([SCA](../../product/sca/)),
and more security testing,
feel free to [contact us](../../contact-us/).
