---
slug: point-of-sale-security/
title: Security for the Point of Sale
date: 2024-10-29
subtitle: Protecting your PoS systems from cyber threats
category: philosophy
tags: cybersecurity, company, risk, trend
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1730243221/blog/point-of-sale-security/cover_point_of_sale_security.webp
alt: Photo by Clay Banks on Unsplash
description: PoS devices are now part and parcel of the retail industry. In this post, we explain the risks these systems face and the best practices for protecting them.
keywords: Retail Sector, Pos System, Point Of Sale Security, Point Of Sale Hacking, Pos Malware, Security Best Practices, Pci Dss, Ethical Hacking, Pentesting
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Content Writer and Editor
source: https://unsplash.com/photos/man-paying-using-credit-card-oNm9NkTFLfA
---

Forever 21, TJX, Home Depot, and Target were those major retailers that,
as we reported in [our previous post](../retail-sector-data-breaches/),
explicitly suffered devastating data breaches
due to inadequate security measures in their PoS systems.
These point-of-sale (PoS) devices or terminals,
increasingly replacing cash transactions,
are indispensable not only for retail stores
but for almost every modern business in tourism, hospitality, and food service,
among other sectors, daily.
It is well established that if companies do not pay sufficient attention
to these systems' security,
chances are they will suffer financial and data losses,
legal repercussions, and enormous reputational damage from cyberattacks.

In this blog post,
we explore diverse security threats PoS systems face,
review some typical hacking methods against them,
and provide actionable steps that companies can follow
to protect these devices
and their customers' confidential data and financial assets.

## What is a PoS system?

A PoS system is a computerized device businesses use in their establishments
that allows customers,
usually in person,
to complete financial asset transactions and make purchases efficiently.
It typically consists of a monitor, keyboard, barcode scanner,
receipt printer, and card reader.
PoS devices can connect to private networks to enable card transactions
as well as to the Internet.
They record every sale,
providing real-time data for accounting and inventory management.
They can also be helpful for reviewing customer behavior
and managing customer data and business relationships.

## Understanding PoS security

As PoS systems become more integrated into daily operations,
they become increasingly attractive targets for cybercriminals.
PoS security refers to the measures taken to protect PoS systems
from unauthorized access, malware infections, and data and money theft.
This includes securing the hardware and software components,
implementing strong network security measures,
and training employees on security best practices.

The importance of PoS security cannot be overstated.
A compromised PoS device can lead to the theft of sensitive customer data,
such as credit card numbers, social security numbers, and addresses.
This information can be used for fraud,
causing significant financial losses to businesses and customers.
Data breaches can also damage a business's reputation
and erode customer trust.
In today's competitive landscape,
companies prioritizing security and data protection are more likely to gain
and retain customer loyalty.

<div>
<cta-banner
buttontxt="Read more"
link="/solutions/penetration-testing-as-a-service/"
title="Get started with Fluid Attacks' Penetration Testing as a Service
right now"
/>
</div>

## How PoS systems get hacked

Understanding how attackers compromise PoS terminals is crucial for businesses
to implement adequate security strategies
and protect sensitive customer data.

### Methods of attack

Cybercriminals employ various techniques
to gain unauthorized access to PoS systems:

- **Vulnerability exploitation:**
  PoS systems often come with default operating systems,
  like Windows or Unix,
  that may have minimal security measures and,
  consequently,
  inherent vulnerabilities that can be exploited by cybercriminals.
  Moreover,
  security weaknesses can be associated with insecure networks
  and outdated or unpatched third-party components or dependencies
  in the PoS system's software.
  These components may not always undergo rigorous security testing,
  leaving potential loopholes for attackers to leverage.
  Threat actors often seek unsecured IP addresses or hack Wi-Fi connections
  to access PoS devices remotely.
  Once inside the network,
  they can exploit operating system or software vulnerabilities
  to install malware and steal data
  or carry out even more complex attacks,
  such as disrupting operations.

- **Brute-force attacks:**
  These attacks involve using automated tools
  that systematically try different combinations of usernames and passwords,
  essentially "guessing" their way in.
  (These tools can test thousands, even millions, of credential combinations
  per second.)
  The danger lies in the fact that many PoS systems come with default,
  easily guessable passwords,
  or employees may set weak passwords for convenience.

- **Compromised credentials:**
  Cybercriminals can leverage previously [compromised credentials](../credential-stuffing/),
  including those of third-party vendors
  associated with the company's PoS systems.
  Employees who use easily guessable passwords or repeat the same credentials
  across multiple accounts
  inadvertently create vulnerabilities.
  Previous data breaches or leaks are often the source of these credentials
  for attackers to access, operate undetected
  and appear as authorized users on victims' networks.

- **Social engineering:**
  [Social engineering](../social-engineering/) exploits human psychology
  rather than technical vulnerabilities to compromise PoS systems.
  [Phishing attacks](../phishing/) are a prime example,
  where attackers craft deceptive emails
  (apparently coming from trusted sources)
  to trick employees into taking actions that affect their company’s security.
  These messages may create a sense of urgency
  or use enticing subject lines
  to lure the recipient into clicking a malicious link
  or opening an infected attachment.

- **Insider threats:**
  These threats,
  involving employees or individuals with authorized access,
  can compromise PoS systems and lead to significant data breaches.
  One scenario involves bribing employees
  to temporarily hand over PoS terminals to malicious hackers
  outside business hours.
  These attackers then exploit the systems within a short timeframe,
  returning the terminals before the business reopens.
  This allows them to initiate undetected data theft
  or other malicious activities,
  with the compromised system appearing to function normally.
  Insider threats can also stem from disgruntled employees
  seeking to cause harm or steal data for personal gain.
  They may exploit their knowledge of system vulnerabilities
  or use their access privileges to install malware or manipulate data.

- **Direct installation of malware:**
  In some cases,
  attackers may physically connect infected USB devices to PoS systems
  to install malware.
  This method requires physical access to the system
  but can be highly effective in compromising the entire network.

- **Credit card skimmers:**
  Although not a method of intrusion into PoS software,
  credit card skimmers pose a significant threat
  to the security of these systems.
  These physical devices are attached to the PoS terminal
  to collect information from cards swiped through the reader.
  This data is then used for fraudulent purposes.

Once attackers gain access to a PoS system,
they typically install PoS malware.
This [malicious software](../../learn/malicious-code/)
can spread across the network,
stealing data from various systems at different points.
The malware captures sensitive information,
such as credit card numbers,
and packages it for transfer to an external site or remote server
controlled by the attackers.
This data exfiltration process is often sufficiently well designed
to avoid detection for extended periods.

### Common PoS malware

Some common types of PoS malware include the following:

- **Memory scrapers:**
  These scrape data from the memory of a PoS terminal,
  often targeting RAM,
  where payment card information is stored.
  This is frequently achieved just before the encryption of that information
  when payment cards are swiped through the PoS system.

- **Keyloggers:**
  These [record every keystroke](../keylogging-keyloggers/)
  typed on a PoS device,
  including passwords and credit card numbers.

- **Network sniffers:**
  These monitor network traffic
  and capture data transmitted between the PoS system and other devices.

- **Backdoor:**
  These create a backdoor that allows hackers to have a hidden entry point
  during extended time
  and access the system remotely to steal data or install additional malware.

Here are some notorious examples of PoS malware:

- **Dexter:**
  First detected in [December 2012](https://en.wikipedia.org/wiki/Dexter_(malware)),
  it was an early example of sophisticated PoS malware.
  Dexter was designed to steal information from PoS systems' RAM,
  parse it to extract credit and debit card data,
  and upload it to a command-and-control server in Seychelles.

- **vSkimmer:**
  Discovered in 2013,
  vSkimmer was considered an [updated version of Dexter](https://www.scworld.com/news/vskimmer-trojan-steals-card-data-on-point-of-sale-systems).
  It works by scanning the memory of infected running PoS systems
  for sensitive card information
  to send to an external server.
  What makes vSkimmer stand out is its ability to store stolen data
  on a specific USB drive
  if the infected system is offline,
  allowing attackers to retrieve it later.

- **Backoff:**
  Backoff is a [notorious family of PoS malware](https://www.cisa.gov/news-events/alerts/2014/07/31/backoff-point-sale-malware)
  that publicly emerged around 2013.
  This malware stood out due to its ability to log keystrokes,
  scrape memory for card data,
  and communicate with remote servers for data exfiltration and updates.
  It was also capable of self-deleting to hinder forensic investigations.

- **PoSeidon:**
  Identified around 2015,
  PoSeidon is a PoS malware family
  that combined keylogging capabilities with RAM scraping
  to steal credit card data.
  It would search the PoS device's memory for number sequences
  that matched credit card patterns
  and then upload that data to an exfiltration server.
  PoSeidon also has [self-update capabilities](https://www.bankinfosecurity.com/pos-malware-still-works-a-8044)
  and self-protection mechanisms
  to preserve permanence on the infected machine
  and guard against reverse engineering.

- **UDPoS:**
  [Unlike other PoS malware](https://www.forcepoint.com/blog/x-labs/udpos-exfiltrating-credit-card-data-dns)
  that typically rely on HTTP for communication,
  UDPoS, discovered in 2017, uses DNS tunneling
  to exfiltrate stolen credit card data.
  This makes it harder to detect,
  as DNS traffic is generally considered normal.
  UDPoS also disguises itself as a LogMeIn service pack
  to blend in with legitimate software
  and evade detection by security tools.
  Once installed,
  UDPoS scrapes the memory of infected PoS systems
  to capture track data from credit cards
  and transmits this stolen information to the attackers through DNS queries.

## Protecting your PoS systems: best practices

Here are some essential steps businesses can take
to protect their PoS systems from cyberattacks:

- Change default passwords and use strong, unique passphrases
  for all accounts (modify them frequently).

- Implement multi-factor authentication for added security.

- Configure firewalls to restrict unauthorized access to the network.

- Segment the network to isolate PoS systems from other devices.

- Use end-to-end encryption to protect cardholder data in transit and at rest.

- Tokenize sensitive data to avoid storing it in plain text.

- Regularly update PoS software and firmware to patch vulnerabilities.

- Continuously assess the security of your systems with [automated tools](../vulnerability-scan/)
  and ethical hackers or [pentesters](../penetration-testing/)
  to detect and remediate their vulnerabilities.

- Use reputable antivirus and anti-malware software with real-time protection.

- Implement whitelisting to restrict the execution of unauthorized applications
  (e.g., web browsers and emails) on the PoS devices.

- Employ code signing to prevent tampering with software.

- Secure PoS terminals physically to prevent tampering and theft.

- Use surveillance cameras to monitor activity around PoS devices.

- Train employees on security best practices,
  such as password hygiene and phishing awareness.

- Implement role-based access control to limit employee access
  to sensitive data and functions.

- Ensure that third-party vendors who access PoS systems
  have adequate security measures.

- Adhere to industry standards and regulations,
  such as PCI DSS.

- Implement a data backup and recovery plan in case of a data breach.

PoS systems play a vital role in modern businesses
but also present a security challenge.
By implementing the security measures outlined in this blog post,
companies can significantly reduce the risk of successful cyberattacks
and protect their valuable data and financial assets.

Remember,
cybersecurity is an endless responsibility.
It's essential to stay informed about the latest cyber threats
and vulnerabilities
and regularly review and update your security posture.
Don't wait for a data breach.
Proactively secure your software with Fluid Attacks.
Our expert team and advanced technology work together
to uncover vulnerabilities and help fortify your applications.
[Let's talk](../../contact-us/).
