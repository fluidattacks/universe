---
slug: retail-sector-data-breaches/
title: Retail Sector Data Breaches
date: 2024-10-21
subtitle: Top seven successful cyberattacks against this industry
category: attacks
tags: cybersecurity, company, risk, trend
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1729549213/blog/retail-sector-data-breaches/cover_retail_sector_data_breaches.webp
alt: Photo by Charles Etoroma on Unsplash
description: Cyberattacks that negatively impact some companies can serve as a warning and a lesson for others, at least for those still behind the curve in cybersecurity.
keywords: Retail Data Breaches, Retail Sector Data Breaches, Cyber Security In Retail Industry, Retail Cyber Security Attacks, Cyberattacks Against Retailers, Point Of Sale Systems, Target, Ethical Hacking, Pentesting
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Content Writer and Editor
source: https://unsplash.com/photos/black-shopping-cart-on-white-floor-u0F1bva4Qh0
---

Over the years,
many well-known brands in the retail industry
have made the headlines worldwide.
While this can be the product of achievements,
it also happens when retailers are victims of cyberattacks
that significantly affect their customers' data and,
consequently, the company's finances and reputation.

A few days ago,
[we published a blog post](../retail-sector-cybersecurity/)
outlining statistics,
threats, challenges, and best practices in cybersecurity for this industry.
On this occasion,
we would like to share some of the most outstanding cases of cyber attacks
and data breaches experienced by well-known multinational retailers.
These cases can serve as a warning to other companies
inside and outside this industry
and as a learning resource to avoid making similar mistakes in cybersecurity.

## 7. Under Armour (2018)

Under Armour, Inc. is an [American performance](https://en.wikipedia.org/wiki/Under_Armour)
apparel and footwear company
that manufactures and sells sports clothing, footwear, and accessories
designed to enhance athletic performance.
[In March 2018](https://www.securityweek.com/under-armour-says-150-million-affected-data-breach/),
this retailer publicly reported a data breach
after its MyFitnessPal application was hacked,
affecting approximately **150 million user accounts**.
Allegedly,
since February of the same year,
the attackers obtained email addresses, usernames, and hashed passwords
but not social security or payment card information.
Nonetheless,
the company [asked its users](https://www.zdnet.com/article/under-armour-reports-150-million-myfitnesspal-accounts-hacked/)
to change their passwords
through various communication channels.
Among the sources reviewed for this post,
there is no report of any disclosure of the type of attack
or the financial cost of this data breach to Under Armour.

## 6. Forever 21 (2017-2023)

Forever 21 is a [multinational fast-fashion retailer](https://en.wikipedia.org/wiki/Forever_21)
known for its trendy clothing and accessories at affordable prices,
primarily for young women and teens.
In November 2017,
[the company was investigating](https://www.zdnet.com/article/forever-21-reveals-potential-data-breach/)
a potential data breach
that compromised customers' personal and payment card information.
The investigation focused on transactions
between March and October of that year.
[Shortly thereafter](https://www.zdnet.com/article/forever-21-investigation-reveals-malware-present-at-some-stores/),
Forever 21 confirmed the data breach.

Apparently,
this retailer had point-of-sale (PoS) systems in some of its stores
that had not yet received or never received encryption
and authentication upgrades
that it was supposed to have started implementing in 2015.
From there,
the attackers accessed Forever 21's network
and installed malware to steal information.
Although there is talk of compromised credit card data,
the number of customers affected was not reported.

[It was in 2023](https://techcrunch.com/2023/08/31/forever-21-data-breach-half-million/)
that the media spoke of **over half a million affected people**.
But no, not in the aforementioned attack, but in a new one,
which reportedly started in January of that year.
However,
[it seems that](https://therecord.media/forever-21-data-breach)
among those affected, in this case,
there were only current and former employees of the retailer
whose personal information was compromised.
Although the type of attack was not disclosed,
the media inferred that it may have been a [ransomware attack](../ransomware/).
Forever 21's financial costs have also not been revealed so far.

## 5. eBay (2014)

eBay Inc. is a [U.S. e-commerce firm](https://en.wikipedia.org/wiki/EBay)
where people and businesses sell and buy a wide variety of goods
and services worldwide.
In May 2014,
[eBay asked its users](https://www.zdnet.com/article/ebay-change-your-passwords-due-to-cyberattack/)
to change their passwords
because of an attack that compromised its personal information database.
This included names, dates of birth, email addresses, phone numbers,
and "encrypted" passwords.
What was seemingly not stolen was financial data,
which was stored separately.
As in the Under Armour case described above,
it is striking that eBay asked its customers to change their passwords
as if [they did not have sufficient confidence](https://www.troyhunt.com/the-ebay-breach-answers-to-questions/)
in its encryption methods.

The attackers had gained access to the company's network
by compromising the login credentials of some employees a few months ago.
It is assumed that
the total number of **affected users** could be **145 million**.
But did malicious hackers manage to get all the data
from just a few employee accounts?
That's odd.
Supposedly,
no financial fraud was reported in the end,
but all that stolen information we know can be useful for cybercriminals,
for example, in their [social engineering campaigns](../social-engineering/).
The costs for eBay were apparently not disclosed.

<div>
<cta-banner
buttontxt="Read more"
link="/solutions/security-testing/"
title="Get started with Fluid Attacks' Security Testing solution right now"
/>
</div>

## 4. Neiman Marcus (2013-2020)

Neiman Marcus is an [American department store chain](https://en.wikipedia.org/wiki/Neiman_Marcus)
offering high-end designer brands in fashion, accessories, and home goods.
[In 2014](https://www.zdnet.com/article/neiman-marcus-1-1-million-cards-compromised/),
this retailer reported being the victim of a data breach
in which information from **1.1 million customer payment cards**
was compromised.
Malware had been installed on their systems
and had acted from mid-July to the end of October 2013.
By then,
they said there was no connection to the Target case
(reported as number one on the list in this post),
but many single-payer cards had already been used fraudulently.

[In early 2019](https://www.zdnet.com/article/neiman-marcus-agrees-to-1-5-million-data-breach-settlement/),
the company reached an agreement with different states in the nation
to provide **$1.5 million** in response to this security incident.
By then, it was stated that, in reality,
the number of compromised cards was around **370 thousand**,
of which more than **9 thousand** had been fraudulently used.

Despite Neiman Marcus' response,
which was supposed to also involve improvements in cybersecurity,
in 2021, it conveyed a [new data breach](https://www.zdnet.com/article/neiman-marcus-breach-includes-payment-card-numbers-and-expiration-dates/).
It had reportedly occurred more than a year ago,
back in May 2020,
but was discovered in September of the following year.
Some **4.6 million customer accounts**,
including their payment card numbers and personal information,
were seemingly compromised.
The retailer said,
"Approximately 3.1 million payment and virtual gift cards were affected
for these customers."

## 3. TJX Companies (2007)

The TJX Companies, Inc. is an American [multinational off-price retailer](https://en.wikipedia.org/wiki/TJX_Companies)
that operates a chain of department stores
offering discounted brand-name apparel and home fashions.
This company disclosed in early 2007 that
customer records had been compromised [for nearly two years](https://www.zdnet.com/article/tjx-says-45-7-million-customer-records-were-compromised/).
Since July 2005,
cybercriminals had accessed TJX's network and installed malware
to steal the personal and financial information
of at least **45.7 million customers**.
([Apparently](https://www.zdnet.com/article/the-worst-it-security-incidents-of-2007/),
this reported number was much lower than the actual number;
it was later reported to be **more than 95 million**).
Credit and debit card transactions were affected in several of TJX's stores
in countries such as the U.S., Canada, Puerto Rico, and the U.K.

Such access seems to have been gained by the attackers
through some of the PoS systems of TJ Maxx,
one of TJX's subsidiaries.
[It is said that](https://www.zdnet.com/article/tjxs-failure-to-secure-wi-fi-could-cost-1b/)
their security was quite deficient.
They had flaws in basic encryption and access control security.
Moreover, TJX's wireless network [was apparently protected](https://www.zdnet.com/article/wi-fi-hack-caused-tk-maxx-security-breach/)
by Wired Equivalent Privacy (WEP),
one of the weakest forms of security for such networks.
The hackers obtained employee login credentials, created their own accounts,
and, throughout the reported time,
collected data related to customer transactions.
From there,
they could sell this information on the black market or use it for asset theft.
By the following year,
[some hackers had already](https://www.zdnet.com/article/alleged-tjx-hackers-charged/)
been implicated and charged with this and similar crimes.

TJX's initial costs for dealing with the data breach,
user reporting, and security enhancements amounted to **$5 million**,
which is nothing compared to what came next.
[Months later](https://www.zdnet.com/article/tjx-data-breach-costs-17-million-and-growing/),
another **$12 million** in charges were added,
and some [media estimated that](https://www.zdnet.com/article/tjxs-failure-to-secure-wi-fi-could-cost-1b/)
the sum would reach **billions of dollars**.
As time went by,
the company was hit with lawsuits filed by users
and investigations and fines by government agencies
for non-compliance with customer protection laws.

## 2. Home Depot (2014)

The Home Depot, Inc. is [a major U.S. retailer](https://en.wikipedia.org/wiki/Home_Depot)
that offers a wide range of home improvement products and services.
[In September 2014](https://www.zdnet.com/article/home-depot-confirms-payment-system-attack/),
this company confirmed that its payment systems had been subject
to a malware attack similar to the one received by Target Corporation
(see case below),
which had begun in April.
Allegedly, the attackers used the credentials of a third-party vendor
to access Home Depot's network and installed malware
to compromise PoS systems and steal data of customers
using payment cards in the U.S. and Canada.
[According to the company](https://www.zdnet.com/article/home-depot-53-million-email-addresses-swiped-too/),
that malware had not been used in previous attacks
and was designed to evade antivirus software detection.

[In this case](https://www.zdnet.com/article/home-depot-agrees-to-17-5m-settlement-over-2014-data-breach/),
**more than 40 million customers** were affected.
Initially,
**56 million credit and debit card numbers** were reported to be compromised,
but in November of the same year,
[it was declared that](https://www.zdnet.com/article/home-depot-53-million-email-addresses-swiped-too/)
**53 million email addresses** were also affected.
Such payment card information could have been used by criminals
to make fraudulent online purchases or create cloned cards.

Once the investigation was completed,
[Home Depot had to add](https://www.zdnet.com/article/home-depot-56-million-payment-cards-affected-by-cyberattack/)
encryption enhancements to its PoS terminals.
It appears that they also began to accelerate
the implementation of chip-and-pin technology.
Additionally,
they had to hire a chief information security officer (CISO),
train their staff in security awareness,
and implement two-factor authentication (2FA), firewalls,
and [penetration testing](../what-is-manual-penetration-testing/),
[among other security measures](https://www.secureworld.io/industry-news/5-home-depot-changes-following-data-breach).
Years later,
the company ended up paying **$17.5 million** in settlements
with different states,
which was only a fraction of the total costs,
to which were added litigation by clients and various institutions.

## 1. Target (2013)

Target is [among the largest](https://en.wikipedia.org/wiki/Target_Corporation)
American retail corporations.
It offers a wide assortment of products,
including apparel, home goods, and groceries, focusing on value and design.
Target [suffered a cyberattack](https://www.zdnet.com/article/anatomy-of-the-target-data-breach-missed-opportunities-and-lessons-learned/)
in late November 2013,
apparently in the middle of Black Friday.
Around two weeks later,
its staff discovered the breach and reported it to the U.S. Justice Department.
The attack was mitigated after two days.

Apparently,
it was enough to compromise only one third-party vendor
out of the many that could have been attacked
for the impact to be successful.
Specifically, it was Fazio Mechanical,
a refrigeration contractor
whose cybersecurity weaknesses allowed the attackers to break
into Target's corporate network.
The attack vector was a [phishing email](../phishing/),
which allowed Citadel (a variant of the Zeus banking trojan)
to be installed on Fazio's machines,
a company that did not suitably use anti-malware software.
Once inside Target's network,
which was seemingly poorly segmented,
the hackers could find and exploit vulnerabilities
to move laterally and then gain privileges and take control of the servers.
Finally,
they infiltrated and infected Target's PoS systems with malware
to extract credit and debit card information and sell it on the black market.

Attackers stole data from roughly **40 million credit and debit cards**,
along with personal information from up to **70 million customers**.
Target's costs exceeded **$200 million**,
including legal fees, settlements, and investments in security improvements.
The actual cost may have been much higher,
considering lost sales, customer churn, and damage to their stock price.
This was one of the most significant retail data breaches in history
at the time.
[It's even said](https://www.zdnet.com/article/the-target-breach-two-years-later/)
to have been the first case
in which "the CEO of a major corporation got fired because of a data breach."
The breach significantly eroded public trust in Target's security practices.
It led to lawsuits, regulatory fines, and negative media coverage,
impacting their brand image and customer loyalty for years to come.

Target's data breach and the other cyberattacks described here
can serve as a wake-up call for all or at least many retail industry members.
These cases highlighted, among several things,
vulnerabilities in their point-of-sale (PoS) systems and networks,
the presence of low-skilled-in-cybersecurity workers,
and the need for more stringent and up-to-date security measures.
Whether you are a retailer or not,
today, cybersecurity is not only a necessity but also the law of the land.
Integrating automated and manual security testing,
Fluid Attacks is here to help you avoid being the next victim
to make headlines.
[Contact us](../../contact-us/).
