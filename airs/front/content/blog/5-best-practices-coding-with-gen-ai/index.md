---
slug: 5-best-practices-coding-with-gen-ai/
title: Tips on Developing Software With AI
date: 2023-12-29
subtitle: Five best practices for coding with the help of gen AI
category: development
tags: cybersecurity, code, security-testing, trend
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1703875585/blog/5-best-practices-coding-with-gen-ai/cover_coding_with_gen_ai.webp
alt: Photo by Takahiro Sakamoto on Unsplash
description: Generative AI tools are an ally for developers to write code efficiently. We share five best practices for developing software securely while using those tools.
keywords: Generative Ai, Gen Ai Tools, Ai Generated Code, Best Practices, Security Testing, Cheat Sheet, Copilot, Ethical Hacking, Pentesting
author: Jason Chavarría
writer: jchavarria
name: Jason Chavarría
about1: Content Writer and Editor
source: https://unsplash.com/photos/five-flying-jet-planes-on-sky-at-daytime-IMued0tpO1s
---

When using generative AI to code,
the best practices to follow include
always reviewing the output,
conducting security tests on it,
avoiding to feed the tool with client or sensitive corporate data,
making sure you are using a tool approved by your company
and taking care not to breach open-source licenses.
Since gen AI is here to stay
as a mighty ally to development projects' efficiency,
it is important to check
that it is implemented securely.

<image-block>

!["Cheat sheet Fluid Attacks - Best practices for developing with gen AI"](https://res.cloudinary.com/fluid-attacks/image/upload/v1703881967/blog/5-best-practices-coding-with-gen-ai/cheat-sheet-five-best-practices-for-developing-securely-with-gen-ai-fluid-attacks.webp)

Cheat sheet: Five best practices for developing securely with gen AI

</image-block>

## Review the output yourself

As it happens with open-source, third-party software components,
AI is offering a way for software development projects to gain agility.
For example,
you can use GitHub Copilot to write code fast
thanks to it autocompleting the intended software functions
based on your prompts.
GitHub research suggests that AI may give you an [efficiency boost of 55%](https://github.blog/2022-09-07-research-quantifying-github-copilots-impact-on-developer-productivity-and-happiness/#:~:text=developers%20who%20used%20GitHub%20Copilot).

Gen AI also has the capability
to enable people with low technical knowledge
to develop software.
What's more,
[it's been suggested](https://www.techtarget.com/searchitoperations/news/366563975/DevSecOps-pros-prep-for-GenAI-upheavals-in-2024)
that AI could do the tasks of junior developers
and, nonetheless, serve as a learning tool for them.
Moreover,
it's expected
that a significant number of enterprises will likely use gen AI
to unburden devs of some tasks
and assign other more important projects to them.

Rest assured,
gen AI is a tool to support your work.
In its current state,
it is not about to replace you.
In fact,
you should always expect that there will be flaws in AI-generated code.
Reviews **must** be performed always.
Take a good look at the code,
thinking about [secure coding practices](../secure-coding-practices/).
When in doubt,
ask knowledgeable peers to review and provide input on the AI-generated code
before you commit it.

## Conduct security testing on AI-generated code

Security is a big issue of AI-generated code.
Fortunately,
there are tools
that can help you identify its vulnerabilities.
We have listed some that are free and open source (FOSS)
in our [main post about security testing](../security-testing-fundamentals/),
including [our own tool](https://help.fluidattacks.com/portal/en/kb/articles/configure-the-tests-by-the-standalone-scanner).
If the application you're coding is ready to run,
you can assess it
with [dynamic application security testing](../../product/dast/)
(DAST)
in addition to [static application security testing](../../product/sast/)
(SAST).
The former will attack your application
by interacting with it "from the outside."

It also helps a lot to have [ethical hackers](../what-is-ethical-hacking/)
review the code and running application,
since [vulnerability scanning](../vulnerability-scan/),
a completely automated process,
is known to produce reports with high rates of [false positives](../impacts-of-false-positives/)
and miss actual security issues.
These hackers, aka security analysts, can find true vulnerabilities
that, if exploited, would have a terrible impact
on information availability, confidentiality and integrity.

Once you learn of the vulnerabilities,
you need to [remediate them](../vulnerability-remediation-process/).
Again, you may use gen AI to fix the code.
In fact,
we recently rolled out the [Autofix](https://help.fluidattacks.com/portal/en/kb/articles/fix-code-automatically-with-gen-ai)
function
of our extension on VS Code.
You can get a **suggested** code fix with the click of a button.
But even then you will have to be careful with the output,
as this function leverages gen AI and can't escape its limitations.
So, you should review the fix suggestion
and subject it to security testing.

If you're interested in a free trial of [security testing](../../solutions/security-testing/)
with our automated tool,
click [here](https://app.fluidattacks.com/SignUp) to start.

## Use gen AI tools approved by your company

It's important that orgs identify and monitor the use of gen AI
should they allow it.
They will need to write up policies and company guidelines
regarding the use of AI,
helped by their intellectual property and legal teams.
As AI can err in the same ways humans have done writing code,
existing governance can serve as a foundation.
We give advice on what to include in a [gen AI policy](../6-main-items-gen-ai-usage-policy)
in a dedicated blog post.
These are some things orgs will need to do:

- Establish the AI tools that they identify as secure,
  while being aware of the limitations of this technology.

- Educate their employees on the use of gen AI technologies
  and policies related to it.

- Establish the security testing solutions
  to be used to secure AI-generated code.

- Monitor for any intellectual property infringement.

You will need to be aware of requirements
by your supervisor or other persons in your company
(e.g., leader, Head of Product, CTO, management)
that they be informed that you are using gen AI tools.
If you are unsure whether you are allowed to use them at all
or which are the right ones,
ask around.

<div>
<cta-banner
buttontxt="Read more"
link="/solutions/security-testing/"
title="Get started with Fluid Attacks' Security Testing solution right now"
/>
</div>

## Don't feed client or intellectual property information to public GPT engines

Don't send confidential data of your company or its clients
that could be fed into a cloud that is much beyond your control.
Basically,
the GPT engine would train on this data
and eventually suggest it to users outside your company.
Moreover,
devs around the world are discouraged
from entering sensitive data into GPT engines,
as data breaches have occurred.
(Counting some related to ChatGPT:
Samsung thrice [leaked its own secrets](https://www.theregister.com/2023/04/06/samsung_reportedly_leaked_its_own/);
exploiting a flawed open-source library
gave attackers [access to the chat history](https://securityintelligence.com/articles/chatgpt-confirms-data-breach/)
of other users;
over [100,000 accounts were stolen](https://www.bleepingcomputer.com/news/security/over-100-000-chatgpt-accounts-stolen-via-info-stealing-malware/)
once.)
So,
be aware of any policy in your organization
detailing how to use gen AI tools for development.

## Be mindful about copyright of open-source projects the AI tool trains on

Is there the possibility
that you're copying someone else's work
when using the gen AI output?
The issues of copyright infringement
(when the output copies copyrightable elements of existing software verbatim)
and breaches of open-source licenses
(e.g., failing to provide required notices and attribution statements)
are new
and to be analyzed case by case
(already there is an intricate [lawsuit against Copilot](https://www.theregister.com/2023/05/12/github_microsoft_openai_copilot/)).

Granted,
Copilot,
for example,
has trained on so many GitHub repositories
that the code it suggests may not look exactly like any particular code
protected by copyright or requiring credits as per its open-source license.
Still,
there is a risk of infringement,
and it's not mitigated easily,
as Copilot removes in its suggestions copyright,
management
and license information
required by some open-source licenses.

What we recommend is to think
whether you could get the functions you need
by using open-source libraries knowingly.
You can follow our advice on [choosing open-source software](../choosing-open-source/)
wisely.
By the way,
there are tools that will help you find out if there's any trouble
with the third-party code you introduce into your project.
By conducting [software composition analysis](../../product/sca/) (SCA),
these tools identify components
that have known vulnerabilities and pose possible license conflicts.
By [trying our tool for free](https://app.fluidattacks.com/SignUp),
you will get these scans as well as SAST and DAST.
Start it now
and don't forget to [download](https://help.fluidattacks.com/portal/en/kb/articles/install-the-vs-code-extension)
our VS Code plugin
to easily locate the vulnerable code
and get code fix suggestions through gen AI.
