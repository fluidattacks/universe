---
slug: cryptocurrency-and-crypto-crime/
title: Cryptocurrency and Crypto Crime
date: 2023-09-29
subtitle: Introduction to a trend of just over a decade ago
category: philosophy
tags: software, cryptography, risk, trend, hacking, social-engineering
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1696000648/blog/cryptocurrency-and-crypto-crime/cover_cryptocurrency_and_crypto_crime.webp
alt: Photo by Bastian Riccardi on Unsplash
description: We share basic knowledge about cryptocurrency and related concepts, including crypto crime, a worrying issue that has gone hand in hand with that paradigm.
keywords: Cryptocurrency, Blockchain, Crypto Crime, Decentralized Finance, Defi Protocols, Defi Hacks, Smart Contracts, Pentesting, Ethical Hacking
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Content Writer and Editor
source: https://unsplash.com/photos/Q08CNRiq3k0
---

Ab initio,
our intention was to write a blog post
about prominent crypto crime incidents around the world.
However,
we had little gone into the topic of cryptocurrencies or cryptoassets
and related criminality
in this blog before.
For this reason,
we thought it best to have an introduction first,
especially for those who know little about this emerging financial paradigm,
before moving on to talk about specific cases,
which will come as a top 10
in [the blog post that follows this one](../top-10-crypto-crimes).

## What is cryptocurrency?

**Cryptocurrency is a digital asset or money
that works on blockchain technology**
and is used to buy and sell products and services
or make transactions online
without needing an intermediary institution
such as a central bank.
Unlike "fiat currency" (e.g., the U.S. dollar),
for cryptocurrencies,
there is no central authority to produce them
and manage or maintain their value.
Instead,
they tend to work under a distributed consensus approach.
Although there are currently [more than 9,000 cryptocurrencies](https://www.forbes.com/advisor/investing/cryptocurrency/what-is-cryptocurrency/)
in circulation,
among the best known are,
for example,
Bitcoin (BTC), Ethereum (ETH), Tether (USDT),
Binance Coin (BNB), and Solana (SOL).

The name "cryptocurrency" comes from the blending of the words
"cryptography" and "currency."
Each cryptocurrency and its transactions are protected
by complex cryptographic algorithms or encryption techniques
(e.g., public-private key pairs and hashing functions).
These processes make it almost impossible for cryptocurrencies
to be counterfeited or double-spent.
The values of cryptocurrencies and their coins
(["publicly agreed-on](https://www.geeksforgeeks.org/what-is-a-cryptocurrency/)
records of ownership")
vary according to supply and demand,
just as with any other asset or thing to which we humans attach value.

## What is blockchain?

Usually,
when cryptocurrencies are transferred from one individual to another,
these transactions are recorded in a public ledger or **blockchain**.
Therefore,
everyone who is part of a specific crypto project's network
can see the transactions occurring.
The name "blockchain" arises
because different groups of transactions are arranged and recorded in "blocks"
that are then linked to a "chain" of other blocks of previous transactions.
In this technology,
the blocks generated must be verified
by all or some individual nodes of the network
(devices running the cryptocurrency software)
before being confirmed and joined to the chain.
Each new block has information from the previous one
so that a dependency and a history are created,
which makes it almost impossible to alter a block.

## How are cryptocurrencies generated and obtained?

The organizers of crypto projects
usually control the minting and releasing of coins.
For instance,
in the case of Bitcoin,
considered the first cryptocurrency,
which was launched back in 2009,
[it's said that](https://www.newyorker.com/magazine/2011/10/10/the-crypto-currency)
a few coins would be distributed every 10 minutes or so
until a particular ceiling was reached
over the next 20 years.
These coins are then sought by so-called "miners,"
who work with programs on highly powerful computing devices
to solve proof-of-work puzzles.

Let's get this straight.
Bitcoin works based on the **proof of work** consensus mechanism.
Each participating node in the network can use computational power
to solve specific problems or puzzles associated with new transaction blocks.
That is,
to verify and join each block to the blockchain,
the solution to the puzzle or complex math problem must be achieved.
For example,
the cryptocurrency encryption algorithm generates a target hash
or string of characters for the block.
Then,
miners with their computers,
as a competition,
generate their own hashes,
hoping to guess the target hash.
The first miner to succeed is the winner of the coins.
In addition,
they can be paid with the network's transaction fees
by the exchanging parties.

This mining work usually takes minutes,
which is far too long compared to the milliseconds it takes to generate a hash.
But what is really problematic is that
increasingly more computational power and,
therefore,
more energy is required in these mining competitions.
Hence,
other cryptos use alternative consensus and verification mechanisms,
such as **proof of stake**.
In this mechanism,
what every individual has to do
is to put other crypto funds as collateral.
The number of transactions a node can verify
depends on the amount of cryptocurrency it is willing to stake
or temporarily store in a common safe.
Moreover,
validators are chosen randomly based on these amounts.

## What are some disadvantages of cryptocurrency?

One of the promises of this paradigm
after the Global Financial Crisis of 2007-8
was to prevent other crises in the future.
This was coupled with making financial architecture and operations
faster and cheaper.
Indeed,
[it is often more affordable](https://www.fool.com/investing/stock-market/market-sectors/financials/cryptocurrency-stocks/guide-to-cryptocurrencies/)
to use cryptocurrencies than traditional financial institutions.
For example,
with the former,
there are no storage fees,
and sending money to other countries is much cheaper.
However,
cryptocurrencies are highly volatile in their prices.
Besides,
there is usually no insurance for the funds held with them,
and,
in general,
their use is still controversial,
and many governments and organizations worldwide have not authorized it.

The disadvantage we are most interested in highlighting here
is the one related to cybersecurity.
In essence,
**cryptocurrency blockchains are highly secure**.
(Even though it is said that transactions are anonymous,
they are more likely to be pseudonymous
because authorities such as the FBI can track them
for cases in which misconduct needs to be investigated.)
The issue is so strict that,
in them,
you handle a private key or password that if you lose it,
you have no way to recover access to your funds.
In other words,
there is no password reset function or support service.

**But things take a different turn
when off-chain crypto-related key storage repositories
such as exchanges and online wallets appear**.
Apart from the fact that,
with them,
there is again dependence on a third party
(for example, to retrieve the keys),
which, by the way, is out of the initially intended decentralization approach,
these systems can be hacked
when there's not enough investment on their security.
Breaches, heists, and frauds or scams then crop up.

## Crypto crime

Cryptocurrency crime,
or crypto crime,
is growing in parallel with the cryptocurrency market.
As reported by [Crystal](https://crystalblockchain.com/security-breaches-and-fraud-involving-crypto/),
**$16.7 billion** has been stolen in cryptocurrencies
from January 2011 to February 2023
in 461 incidents.
About 27% of that money was obtained with security breaches,
28% with DeFi hacks,
and 45% with scams.
According to Crystal,
the most popular crypto-theft method until 2021
was the infiltration of **crypto-exchange** security systems,
but since then, and currently, it is **DeFi** hacks.

**Cryptocurrency exchanges** are systems
that serve to purchase coins
using conventional mechanisms
and for trading different kinds of cryptocurrencies.
They also provide user wallets
that free people from using cryptographic keys.
However,
they have full custody of the cryptocurrencies,
and these can be irretrievably lost in case of theft.
In fact,
[it is said that](http://www0.cs.ucl.ac.uk/staff/P.McCorry/preventing-cryptocurrency-exchange.pdf)
between 2009 and 2015,
more than a third of these exchange platforms were compromised,
and almost half of them disappeared.

**DeFi (decentralized finance)**
is a recently emerging [financial technology](https://www.investopedia.com/decentralized-finance-defi-5113835)
based on secure distributed ledgers
such as those used by cryptocurrencies.
DeFi is a broad concept encompassing different applications
for traditional but decentralized
(oriented to peer-to-peer exchange)
financial services
(e.g., sales, loans, investments).
These decentralized applications (dApps) are built
on top of blockchain technologies
such as smart contracts or digital agreements
(automated programs to verify specific rules before funds are transferred).
Among the different types of digital assets
that dApps can handle
are cryptocurrencies.
These applications can be created by any developer,
as DeFi is usually a free, open-source marketplace
based primarily on the Ethereum network.

According to [Chainalysis](https://www.chainalysis.com/blog/2022-biggest-year-ever-for-crypto-hacking/),
2022 has been the biggest year for crypto hacking,
totaling around **$3.8 billion** in thefts.
Of this sum,
roughly 82% corresponded to attacks on DeFi protocols.
And of what was stolen in these DeFi hacks,
more than 60% was obtained by attacking **cross-chain bridge** protocols.

As the name suggests,
these protocols allow users to transfer funds or cryptocurrencies
from one blockchain to another.
Typically,
this is done by blocking the assets in a smart contract
on the first blockchain
and minting equivalent assets on the second blockchain.
These smart contracts act as large, centralized repositories of funds
backing the assets that pass to the other blockchain.
As a result,
they become attractive targets for hackers,
who look for bugs in the code or other vulnerabilities to exploit
at the right time.
Likewise,
these protocols allow the growth of ["'chain-hopping' typologies](https://hub.elliptic.co/analysis/the-top-ten-latest-crypto-crime-typologies/)
of money laundering,
whereby criminals attempt to break the funds trail on the blockchain
by swapping their ill-gotten funds into other assets or coins."

[Reportedly](https://www.chainalysis.com/blog/2022-biggest-year-ever-for-crypto-hacking/),
of the $3.8 billion stolen in 2022,
roughly **$1.7 billion** was stolen
by the notorious North Korea-linked hacking team **[Lazarus Group](../lazarus-malware-cyberattack/)**.
During 2021 and 2022,
this group heavily relied on the **crypto mixer** Tornado Cash,
which,
thanks to its technical attributes,
made it difficult to trace specific funds
and thus allowed them to launder the stolen cryptocurrencies.

**[Crypto mixers](https://cointelegraph.com/explained/what-is-a-cryptocurrency-mixer-and-how-does-it-work)**
are programs that aim to increase the privacy of transactions
by breaking end-to-end traceability.
They allow mashing up various types of funds,
including cryptocurrencies,
in private pools before being transferred at random intervals
to different designated recipients.
While they are not always for illicit uses,
it is true that they largely exist
to overshadow the normal transparency of the crypto ecosystem
and assist cybercriminals.
[That is why](https://hub.elliptic.co/analysis/the-top-ten-latest-crypto-crime-typologies/)
several mixers have already been sanctioned
by the U.S. Treasury's Office of Foreign Assets Control (OFAC)
in recent years.

Protocols,
services and programs such as those we have presented here
are of great help to malicious hackers and criminals and,
in fact,
have contributed to an increase in the number of **[ransomware attacks](../ransomware/)**
over the years.

Another type of crypto crime is **flash loan attacks**.
[These are carried out](https://www.bitdegree.org/crypto/learn/crypto-terms/what-is-flash-loan-attack)
by exploiting smart contracts that enforce flash loans.
On these loans,
users can borrow assets with no upfront collateral
or third-party overseers
as long as they pay back before the transaction concludes.
([In the case of Bitcoin](https://www.wallstreetmojo.com/flash-loan-attack/),
for example,
the transaction can take 10 to 30 minutes,
while for Ethereum,
it takes around 13 seconds).
Attackers in that period extract funds from the blockchain
and take advantage of issues
such as a cryptocurrency being priced differently in two different markets:
They use those funds to buy coins on the platform
where they are cheaper
and then sell them where they are more expensive,
repay the loan and make a profit.

Apart from the hacking acts,
as mentioned above,
the biggest thefts have been **scams**.
Normally,
there is something like the [Ponzi scheme](https://www.investopedia.com/terms/p/ponzischeme.asp)
(in this case for cryptoassets),
a fraudulent investing scam
in which investors are promised high rates of return
but end up being scammed after receiving some deceptive first funds.
In this context,
some people speak often of the "**[rug pull](https://cointelegraph.com/explained/crypto-rug-pulls-what-is-a-rug-pull-in-crypto-and-6-ways-to-spot-it)**"
method.
Fraudsters develop new cryptos,
lure investors with them,
and then disappear with the invested funds,
especially when prices reach a certain ceiling.

[Another similar](https://hub.elliptic.co/analysis/the-top-ten-latest-crypto-crime-typologies/)
investment scam
that has come to the fore lately
is "**pig butchering**."
Here,
criminals seek to establish supposed romantic or friendship links online
with victims (often the commonly very vulnerable elderly),
to whom they portray themselves as successful,
having profited enormously from investing in cryptoassets.
From there,
they then go on to persuade people to invest as well.
In some cases,
scammers create fake websites that simulate cryptocurrency exchange platforms,
and that is where victims transfer their funds.
In other cases,
victims actually acquire legitimate cryptoassets
but unknowingly end up transferring them
to crypto wallets controlled by criminals.

## A couple of recommendations

If you develop DeFi protocols and dApps with good intentions,
remember that you are in an area that is generally opaque
on regulatory and security issues.
Therefore,
it is always advisable to have third-party cybersecurity experts
performing continuous tests on your software,
including simulated attack scenarios.
Keep in mind that DeFi protocols should have good security controls in place,
such as automated processes that interrupt transactions
if suspicious activity is detected.

<div>
<cta-banner
buttontxt="Read more"
link="/solutions/secure-code-review/"
title="Get started with Fluid Attacks' Secure Code Review solution right now"
/>
</div>

Now,
if you are going to use cryptocurrencies or other cryptoassets,
you should thoroughly examine how they work
and through which protocols you will use them.
You must really understand what this is all about.
Check the sites of the projects well and even the comments of their users,
seeking to be sure of their authenticity.
If you can,
contact other people who have invested or,
better yet,
experts or advisors on the subject.
Be wary of anyone who invites you to invest,
especially when that person has only recently entered your life.

Finally,
in the event that you are already in the crypto market,
enable two-factor or multi-factor authentication in your crypto wallet
or other platforms,
and take good care of and do not share your private keys.
