---
slug: security-testing-fundamentals/
title: Security Testing Fundamentals
date: 2023-11-01
subtitle: Learn the types, tools, techniques, principles and more
category: philosophy
tags: cybersecurity, security-testing, software, code
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1698867448/blog/security-testing-fundamentals/cover_security_testing.webp
alt: Photo by Erzsébet Vehofsics on Unsplash
description: We define security testing and tell you all the basics. These include how to perform it to find vulnerabilities in software applications and other systems.
keywords: Security Testing, Software Development, Application Security, Vulnerabilities, Security Testing Tools, Source Code, Security Scanning, Ethical Hacking, Pentesting
author: Jason Chavarría
writer: jchavarria
name: Jason Chavarría
about1: Content Writer and Editor
source: https://unsplash.com/photos/an-owl-sitting-on-top-of-a-wooden-post-Ql0D1hQeKB8
---

In our daily lives,
we entrust the safeguarding of our personal information
and other sensitive data to IT systems
(and, of course, their owners).
These data are constantly at risk,
with cybercriminals (black-hat/malicious hackers) wanting to gain access
to those systems
and find a way to profit from that.
It is therefore of the utmost importance
to subject every system to security testing.
Basically,
this means evaluating each system
to check that its design and configuration allow the availability,
confidentiality
and integrity of the data.
Whether you're a business owner,
a software developer
or simply an individual who values their online privacy,
understanding security testing is essential.
This blog post will present the fundamentals of security testing:
what it is, what types, tools and techniques there are,
its principles and more.

## What is security testing?

A system can be evaluated
during its entire software development lifecycle (SDLC),
that is,
all through the phases
that go from it being designed and worked on
to being made available to end users and maintained.
What a human or machine (henceforth, “tool”) evaluates changes
according to what is in focus.

Take,
for instance,
an automated test that runs as the system’s source code undergoes modifications
and checks that single lines of code do not exceed a certain character length.
Fixing the lines that the test flags as noncompliant may result in a code
that is more comfortable to read.
The focus of that test is formatting.

Security testing is software testing focused on verifying
that evaluated systems have no flaws,
either of design or configuration,
that could allow situations
in which the service or information are not available to users,
unauthorized individuals can access data of any degree of sensitivity,
or can tamper with information.
Moreover,
security testing involves the classification and report of the findings
and delivery of recommendations to eliminate the vulnerabilities.
Bear in mind
that both humans and machines can do security testing.

## Main goals of security testing

In line with the definition offered above,
it’s easy to see
that security testing intends to achieve the following:

- Identification of digital assets

- Identification and classification of security vulnerabilities

- Discovery of the potential impacts of vulnerabilities if exploited

- Reports of the findings for their remediation

- Recommendations for eliminating the vulnerabilities

All in all,
if we were to condense the above to answer
what is the one goal of security testing,
we would say:
to find out details about the security status of an information system.

## Why is security testing important?

When asking ourselves about the importance of something,
the question can be posed like this:
“Does having that something make any difference?”
Conducting security tests may have visible consequences that signal success,
such as compliance with security standards and guidelines that require it,
higher quality software,
as security plays an important role there,
and the trust of users.

It may be the case
that some companies take these accomplishments as permanent,
or at least quite durable,
so they might downplay the importance of further conducting security tests.
The threat landscape is continuously evolving, though.
Malicious hackers come up with perfected methods
in their relentless attacks to systems worldwide.
[Performing security testing continuously](../../services/continuous-hacking/)
does make a difference.
It takes just one cybersecurity incident to realize this.

The costs of cyberattacks are today more devastating than ever.
The fact
that among the biggest cost amplifiers
is [noncompliance with regulations](https://www.ibm.com/reports/data-breach)
underlines why security testing is important.
In 2023,
the global average cost of a data breach broke 2022’s record.
It is now at USD 4.45M.
And attacks are not restricted to the big fish.
Organizations worldwide with less than 500 employees reported impacts
costing an average of USD 3.31M.
For small and medium-sized enterprises,
suffering the consequences of an attack can easily mean the end
of their operations.

## Case study: A costly lesson in the power of security testing

Marriott International, a leading hospitality chain,
unfortunately became the target of two major data breaches in 2014 and 2020,
exposing the personal information of millions of guests.
These incidents serve as a stark reminder
of the critical role security testing plays
in safeguarding sensitive data
and preventing costly security breaches.

<image-block>

!["Case study: Marriott Int"](https://res.cloudinary.com/fluid-attacks/image/upload/v1707840844/blog/security-testing-fundamentals/case-study-marriott-int-security-testing-fluid-attacks.png.webp)

Case study: Marriott International data breaches

</image-block>

## Key principles of security testing

Since security testing is such an important part of your preventive strategy,
it is recommended that you try to uphold
what some have referred to as security testing principles.
They are the following:

- **Comprehensiveness:** Security testing should use multiple techniques
  (like assessment of the source code together with inspection from the
  outside of the system running), evaluate several security
  requirements and span the entire SDLC.

- **Realistic tests:** Security testing should include realistic attack
  simulations.

- **Continuity:** Security testing should be done continuously even as the
  technology evaluated is not undergoing changes frequently.

- **Collaboration:** Security testing should be a collaborative activity
  between the development, operations and security teams.

## Types of security testing

It’s a curious thing that,
if you browse the Internet looking for the types of security testing,
you find a potpourri of items that are really in different category levels.
To avoid making the same mistake,
we’ll follow what [we have done before in this blog](../types-of-penetration-testing/),
which is to present types according to the systems under evaluation.
We agree then,
only in part with the sources we have seen.
Moreover,
we will present only a selection
of the most commonly known security testing types.

### Web application security testing

When testing web applications,
[web scanners](../web-app-vulnerability-scanning/) and humans focus especially
on finding the vulnerabilities linked
to the Open Worldwide Application Security Project [(OWASP) Top 10](../owasp-top-10-2021/)
list of risks to web applications.
That is why they check compliance with the following:

- The app uses stable,
  tested and up-to-date versions of third-party components.

- The app discards unsafe inputs.

- The app includes HTTP headers.

- The app uses pre-existing and up-to-date mechanisms
  to implement cryptographic functions.

- The app defines users with privileges.

- The app requires lengthy passwords and passphrases.

- The app controls redirects.

We check these and more to help you [secure your web applications](../../systems/web-apps/).

### Mobile application security testing

Similarly to testing web apps,
[mobile apps testing](../what-is-mast/) often follows OWASP
as a primary authority.
In this case, its the OWASP Mobile Top 10.
Accordingly,
[vulnerability scanning](../vulnerability-scan/)
and [manual review](../what-is-manual-penetration-testing/)
check whether the app complies with the following:

- It has insecure functionalities disabled or otherwise controlled.

- It does not expose session IDs in URLs and messages presented to the user.

- It keeps communication protocols (e.g., Bluetooth) hidden,
  protected with credentials or turned off.

- It requests multi-factor authentication.

- It encrypts sensitive information.

- It sets roles with different levels of privilege to access resources.

- It does not have repeated functions, methods or classes.

- It does not allow parameter inclusion in directory names or file paths.

- It obfuscates the data if it is not in focus.

- It does not have unverifiable files (i.e., not included in audits)
  in its source code.

We check these and more to help you [secure your mobile apps](../../systems/mobile-apps/).

### Network security testing

The goal of this type of security testing is to find weaknesses
in website,
database,
web application
and File Transfer Protocol security controls.
The following are some of the requirements assessed:

- Firewalls correctly filter traffic, blocking unauthorized access.

- Intrusion detection and prevention systems effectively identify,
  stop, log and report potential incidents.

- User access controls are in place and secure.

- Network segments are isolated and securely configured.

- Data encryption mechanisms are in place and secure.

- Load balancers and proxies are securely configured.

- The network is resilient to distributed denial of service (DDoS) attacks.

- Network devices and systems are up to date.

### API testing

Application programming interfaces (APIs) serve as gateways for data exchange
between software systems and applications.
Software products can be exposed to cyberattacks through their APIs.
Therefore, the latter should also be tested.
The following are some security requirements the assessments focus on:

- Authentication mechanisms (e.g., API tokens, OAuth)
  are implemented correctly.

- Authorization rules are in place and secure.

- Data privacy controls protect information during transmission and storage.

- Input is validated to prevent injection of malicious code.

- The API controls the domains that can access it.

- Error messages do not expose sensitive information.

- File upload endpoints are secure.

We check these and more to help you [secure your APIs](../../systems/apis/).

### Cloud infrastructure security testing

Cloud infrastructure allows software development projects
to build their products on customized modern architectures.
As it is highly scalable,
those teams can increase their application's use of cloud servers
in order to satisfy users’ need for speed and effectiveness.
These are some requirements verified in the tests:

- Cloud resource configurations abide by security best practices.

- Identity and access management policies
  allow for proper user authentication and authorization.

- Key management practices effectively protect encryption keys and credentials.

- Measures are in place to prevent unauthorized access
  to cloud management consoles.

- APIs used within the cloud environment are secure.

We check these and more to help you [secure your cloud infrastructure](../../systems/cloud-infrastructure/).

## Other terms related to security testing

We would like to define here some of the terms
that have been associated with security testing,
even sometimes as “types of security testing,”
but which, in our opinion, could be seen
as either a larger process that includes security testing,
a fancy name for the same activity
or an activity within security testing.

### Cybersecurity risk assessment

Cybersecurity risk assessments focus on quantifying the impact and likelihood
of potential risk scenarios.
The desired outcome of a risk assessment is a structured risk management plan
that can guide the organization in making decisions
about security improvements, resource allocation
and the improvement of its digital assets overall.
The plan should include security testing as a regular practice.

### Security auditing

Security audits refer to systematic examinations
of an organization's information systems,
networks
and physical infrastructure.
The audit focuses on the security policies and procedures in place
to see how they adhere to industry standards and best practices
as well as legal requirements.
It may not necessarily be performed by independent auditors or consultants,
but by internal auditors as well.

### Security posture assessment

Security posture assessments check an organization’s current state
of technical security controls.
They are conducted on a regular basis
(quarterly or annually, but ideally it should be continuous)
to provide insights into the company’s readiness to withstand cyberattacks.
A desired outcome is an up-to-date and detailed report
about the security vulnerabilities found,
along with recommendations on prioritization.
Sounds like security testing, right?

### Vulnerability scanning

[Scanning for vulnerabilities](../vulnerability-scan/) is an activity
that focuses on finding security flaws in software
using automated tools.
Ideally,
it’s done continuously,
testing every change.
Although really it should be done
regardless of whether a software project is making changes to its product
regularly,
given the constant evolution of cyber threats
and discovery of vulnerabilities existing for a long time.
Basically, it’s security testing done by tools.

### Ethical hacking

[Ethical hacking](../../solutions/ethical-hacking/)
denotes the work of the [ethical hacker](../hacking-ethics/).
And what the ethical hacker does is use all their skills
to find the security weaknesses in systems
whose owners have given permission to attack.
The term has been used interchangeably with “[pentesting](../what-is-manual-penetration-testing/)”
and “[red teaming](../red-team-exercise/).”
These are merely methodologies in the line of work that is ethical hacking.
The first one,
often associated with a more general target than that of red teaming.
What’s more,
red teaming has been largely linked to exercises
in which only some people in the targeted organization know
of the authorization of the attacks.
By the way,
the attacks in both of these ethical hacking methodologies may include
[social engineering](../social-engineering/).
This last technique refers to using psychological manipulation
to push people to disclose sensitive information
or take risky actions
(like visiting fishy websites or downloading dangerous files).

<div>
<cta-banner
buttontxt="Read more"
link="/solutions/security-testing/"
title="Get started with Fluid Attacks' Security Testing solution right now"
/>
</div>

## Security testing techniques

There are different ways to approach security testing of a software product.
Tests are focused on a certain level of analysis of a system depending on,
for example,
whether initial access to the system’s source code or documentation is granted
(white-box testing) or not (black-box testing).
What can give further depth to security tests is using the manual method
along the automated one.
That is,
allowing ethical hackers to review code
or try and hack into the system themselves.
The following are the testing techniques
you will probably hear about the most in the cybersecurity industry.

### Static application security testing (SAST)

[SAST](../../product/sast/) is a kind of white-box testing
and is also referred to as static analysis or static source code analysis.
When done by a [SAST tool](../devsecops-tools/),
the program scans the code to find anything that coincides with known errors
it has stored in a database.
It does this using sophisticated functions
(e.g., pattern recognition, data flow analysis, control flow analysis).
When done manually,
the security analyst,
who knows the assessed system’s context,
can find security issues that the tool could not flag.

### Dynamic application security testing (DAST)

If you’re wondering about the use of the words “static” and “dynamic”
in this and the previous technique,
here’s what they mean:
“static” denotes that the system is assessed without being executed,
whereas “dynamic” means that the system is assessed as it is running.
[DAST](../../product/dast/) is a kind of black-box testing
and is also known as dynamic analysis.
It assesses working applications that are already on a virtual machine,
web server
or a container
by basically attacking them.
Be it done by a DAST tool or a hacker,
the technique works by sending attack vectors
such as strings of code to application endpoints
to try and trigger unexpected and undesirable behaviors.
As with SAST,
performing DAST manually alongside the tool’s scans
can help reduce the false negative rate significantly.

### Software composition analysis (SCA)

Software doesn’t have to be completely original.
Actually,
it would be a nuisance
(and even a dangerous decision)
to go and reinvent the wheel
while there are secure,
broadly-used,
community-supported,
open-source software components
to get the desired functionality in your software product.
This is generally followed,
and to such an extent that software development projects may lose track
of the open-source components in use
and, therefore, not know their security status
and which need updating or replacing.
[SCA](../../product/sca/) addresses these problems.

Software composition analysis is another kind of white-box testing.
[SCA tools](../devsecops-tools/) identify the third-party software components
used in the assessed software product
and reports them,
informing of their security status
and whether there are license conflicts
(e.g., a library requires software projects using them
to make their source code freely available
and the organization doesn’t want that).
An inventory of the components found
is what is currently known as a software bill of materials (SBOM).

### Cloud security posture management (CSPM)

Since misconfigurations of cloud services are common
and [dangerous mistakes](../microsoft-38tb-data-leak/),
it’s important to conduct [vulnerability assessment](../vulnerability-assessment/)
in cloud-based systems and infrastructures
to identify, prioritize and remediate issues like that.
[CSPM](../what-is-cspm/) scans are then the way to go.
They involve both white-box and black-box testing
directed at infrastructure as code (IaC) scripts,
container images
and runtime environments
to verify compliance with security requirements.
The latter include custom organization policies
about their use of cloud services.

### Manual penetration testing (MPT)

We’re moving on now to the techniques that tools cannot perform.
They pertain to the realm of [ethical hacking](../what-is-ethical-hacking/),
which refers to the work of highly capable security analysts
to essentially identify and exploit security issues in systems
to gain access to them
with the permission of the systems’ owners.
[MPT](../what-is-manual-penetration-testing/) is security testing
in which ethical hackers create custom exploits to bypass defenses,
simulating the tactics, techniques and procedures
that characterize the attacks by cybercriminals.
It can be white-box and black-box testing
and goes by many names,
like “pen testing,”
“pentesting,”
“manual pentesting”
or just “penetration testing.”
Anyway,
all these terms point to a powerful approach,
as it finds more [risk exposure](https://fluidattacks.docsend.com/view/e988pvaiuew3nikq)
than scanning does,
and this bigger share corresponds often to the most complex vulnerabilities.
This is in part because tools are limited
in chaining one detected vulnerability
with some other to achieve a discovery of a greater impact
compared to that accomplished
by only exploiting the initial vulnerability.
Automation does not yet surpass humans
when it comes to simulating cyberattacks.

### Secure code review (SCR)

[SCR](../secure-code-review/)
is when the source code of an application or any other system
is subject to the scrutiny of ethical hackers or pen testers
to identify security vulnerabilities.
This white-box testing technique is manual work
that,
in comparison to automated SAST,
can detect more complex and business-critical vulnerabilities.
This,
due to the knowledge of business logic that ethical hackers work with.

### Reverse engineering (RE)

The last black-box testing technique,
and indeed the last one in this list,
that we think you will see mentioned elsewhere most commonly
is [software reverse engineering](../reverse-engineering/).
As the name suggests,
it involves looking at software from the outside
to work out its source code
(i.e., reversing the steps of development).
And its focus can be in specific,
very important security functions,
like encryption algorithms,
whose resistance to be hacked is of great interest
for software development projects.

## Security testing tools

As we already mentioned,
security testing can be done by tools, automatically, and humans, manually.
Extant software products for automated security testing
(aka [vulnerability scanning](../vulnerability-scan/))
are too many to list in this blog post.
However,
we can offer a list of tools that are free and open source software (FOSS)
and following many best practices
collected by the Open Source Security Foundation (OpenSSF).

- [**Fluid Attacks’ Machine:**](https://help.fluidattacks.com/portal/en/kb/articles/configure-the-tests-by-the-standalone-scanner)
  This is a command-line interface application
  [recommended](../casa-approved-static-scanning/)
  by Google’s [Cloud Application Security Assessment](https://appdefensealliance.dev/casa/tier-2/ast-guide/static-scan)
  (CASA) framework
  that has achieved [100% in true positives and 0% in false positives](../owasp-benchmark-fluid-attacks/)
  on the OWASP Benchmark version 1.2.
  and a [gold badge in OpenSSF best practices](../openssf-gold-badge-for-universe/).
  It performs automated SAST, DAST, CSPM and SCA
  in source code written in any of multiple programming languages.

- [**OWASP® Zed Attack Proxy (ZAP):**](https://www.zaproxy.org/)
  This is a [web application vulnerability scanner](../web-app-vulnerability-scanning/)
  [recommended by CASA](https://appdefensealliance.dev/casa/tier-2/ast-guide/dynamic-scan)
  that performs DAST.
  It has a [passing badge in OpenSSF best practices](https://www.bestpractices.dev/en/projects?q=zed%20attack).

- [**Horusec:**](https://horusec.io/site/)
  This is a tool to perform SAST
  that supports multiple programming languages
  and has earned a [passing badge in OpenSSF best practices](https://www.bestpractices.dev/en/projects/5146).

- [**SpotBugs:**](https://spotbugs.github.io/)
  This is a tool to perform static analysis in Java web applications
  and Android apps.
  [A 2021 study](https://arxiv.org/pdf/2112.04037.pdf) found its detection ratio
  to be 53.8%.
  It has implemented many OpenSSF best practices
  but [still hasn’t earned a passing badge](https://www.bestpractices.dev/en/projects/3224).

- [**Flawfinder:**](https://dwheeler.com/flawfinder/)
  This is a tool to scan source code written in C and C++.
  It has a [passing badge in OpenSSF best practices](https://www.bestpractices.dev/en/projects/323).

- [**Railroader:**](https://railroader.org/)
  This is a tool to analyze the code of applications
  developed in the Rails framework,
  which is used to program web applications in the Ruby language.
  It has a [passing badge in OpenSSF best practices](https://www.bestpractices.dev/en/projects/2514).

There are also many tools to scan Android and iOS apps.
OWASP has compiled them [here](https://github.com/OWASP/owasp-mastg/tree/master/tools).
Its criteria have been that the tools be FOSS,
capable of analyzing recent apps,
regularly updated
and have strong community support.

## The impact of false positives and false negatives in security testing

Albeit in different ways,
both false positives and false negatives can undermine
the effectiveness of security testing efforts.

False positives can trigger build interruptions,
creating unnecessary delays and setbacks.
Moreover, repeated false positives can erode developers’ trust
in the security tools or methods,
potentially leading to them
[ignoring legitimate warnings in the future](../impacts-of-false-positives/).
Investigating false alarms also consumes valuable time and resources,
stagnating the creation of new value.

In turn, false negatives can lead to a false sense of security,
and further failing to identify real vulnerabilities
can leave the software vulnerable to exploitation by attackers.
Finding a solution that can guarantee both minimizing false positives
and false negatives
is crucial for maintaining a robust security posture.

## How to do security testing

Security testing should be a planned activity
in every software development project.
In line with the [DevSecOps methodology](../devsecops-concept/),
developers should be aware of software security
since the beginning of the project
and always act proactively in its favor.
This includes knowing that they might have to rework promptly
on the code they have written when it is found it has vulnerabilities.
For this reason,
they will be thankful if the products used for security testing
prove to be accurate (show low false positives and false negatives rates).

The project needs to first identify the tools and humans
that will test the software product in each phase of the SDLC.
We have written extensively in another blog post
about [which techniques to use](../devsecops-tools/)
from each early phase of the SDLC.
Basically,
white-box testing techniques can be leveraged
when the developers have just started writing code.
Later on,
when a build artifact is ready
that can be deployed into staging or testing environments,
black-box testing techniques can be used.
A combination of automated and manual testing is advised.

The project should also need to set criteria and policies
on how much risk exposure it should be able to tolerate
and for what amount of time.
This way,
they can prioritize the remediation of a given group of vulnerabilities,
based on security testing results on the risk exposure associated to them.
Automated testing should be in place in continuous integration
and continuous delivery pipelines
to stop builds from being delivered to end users
when such builds do not comply with the project’s policies.

Remediation should take place shortly after the security issues are found.
This will allow a short time period
during which an attack could take place.
Then,
when the security issue is thought to be eliminated
or a workaround is worked out,
the project should conduct security testing again
to verify that they have effectively mitigated the targeted risk exposure.

## How we perform security testing

We offer Continuous Hacking for software development projects
to secure their products continuously
from the early stages of the SDLC.
We conduct SAST, DAST, SCA, CSPM, MPT (or PTaaS), SCR and RE
to provide accurate and detailed information
about the software product’s risk exposure.
[We offer two plans](../../plans/).
In Essential plan,
security testing is done by our tool only.
The tool reports very low rates of false positives and false negatives,
is recommended by the CASA framework
and earned an OpenSSF best practices gold badge.
In Advanced plan,
security testing is done by our tools and pentesters.
The latter have several [certifications in offensive security](../../certifications/).

From the start,
the project can visualize the mapping of the attack surface
and the results of security testing on our platform.
It can also go on to manage the security issues found.
Through our IDE extension on VS Code,
developers can conveniently see the flawed lines of code.
With the click of a button,
they can get generative AI-created guides
to successfully remediate the vulnerabilities.
If subscribed to our flagship plan,
Advanced plan,
developers can talk to our pentesters to receive their guidance.
In both plans,
we offer reattacks to verify that the issues are indeed remediated.

## Advantages of security testing with Fluid Attacks

When software development projects choose Fluid Attacks
to secure their products,
they benefit from the following:

- Continuous security testing with several techniques

- Exhaustive and continuous vulnerability reports

- Minimal rates of false positives

- Vulnerability management from one single platform

- Detection of complex vulnerabilities
  and an accurate, updated status of risk exposure

- Remediation guidance through generative AI and expert help

We invite you to start a [free trial](https://app.fluidattacks.com/SignUp)
of our security scanning with our certified automated tool.
You can upgrade at any moment to a plan that includes manual techniques.
