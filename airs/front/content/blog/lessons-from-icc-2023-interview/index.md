---
slug: lessons-from-icc-2023-interview/
title: Lessons From Competing in the ICC
date: 2023-08-18
subtitle: An interview with members of our hacking team
category: interview
tags: cybersecurity, training, hacking
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1692386944/blog/lessons-from-icc-2023-interview/cover_icc.webp
alt: Photo by Dave Photoz on Unsplash
description: Members of our hacking team who took part in the International Cybersecurity Challenge tell us what the hardest part was, their recommendations, and more.
keywords: International Cybersecurity Challenge, Icc, Team Latin America, Cyber Team Latin America And Caribbean, Ctfs, Jeopardy Ctf, Attack Defense Ctf, Ethical Hacking, Pentesting
author: Jason Chavarría
writer: jchavarria
name: Jason Chavarría
about1: Content Writer and Editor
source: https://unsplash.com/photos/BDIFIT1ILDs
---

<div class="blog-questions">

The [ICC](https://www.ic3.games/) (International Cybersecurity Challenge)
is an esports competition organized by the ENISA
(European Union Agency for Cybersecurity).
Its first edition was in the summer of 2022 in Athens.
This year it was held earlier this month in San Diego.
The latter edition gathered competitors from 86 nations,
aged 18 to 25,
each in either of 7 teams,
each, in turn, representing one world region
(Africa, Asia, Canada, Europe, USA, Latin America and Oceania).

On the first day, there was a Jeopardy-style CTF
with categories such as cryptography,
pwning (binary exploitation),
web
and forensic.
The second day began with an Attack-Defense-style CTF
in which competitors had to defend the servers and machines assigned to them
and attack those of the other teams.
(Read the descriptions of these kinds of CTFs
in our post about our selection of the [top 10 CTFs](../top-10-ctf-contests/).)
The challenge closed with a [King of the Hill](https://github.com/Kkevsterrr/koth)
(aka KotH) competition,
in which each team must work "to attack,
control,
and defend as many computers on a target network as they can."
The winner of the ICC this and last year was Team Europe.

We interviewed Óscar Uribe, Alejandro Pérez and Said Cortes.
They passed the application challenges
to earn their spot on the Cyber Team Latin America & Caribbean
and competed in the ICC.
Said participated in 2022,
and Óscar and Alejandro participated in 2023.
We asked them about the toughest part of the challenge,
what preparation anyone interested in participating should consider,
and their recommendations to the Latin American team, among other things.

**What was the preparation you had before the ICC?**

1. **Óscar:** We received workshops and lectures
    before flying to meet our teammates.
    But the preparation also consisted of a great deal of independent work.
    Each one of us had to seek
    to learn more about different exploitation techniques.
    Then,
    when we got to San Diego,
    we organized the responsibilities of each player and laid out strategies.

<!-- end list -->

1. **Alejandro:** Mainly we made a table with the different categories
    that we expected to face in the ICC
    where each participant would put their skill level.
    Based on that,
    we defined the specific areas where each of the team members would work.

<!-- end list -->

1. **Said:** The preparation we had last year were various CTFs
    and participation in events by invite on Hack The Box.
    But I consider that there is room for improvement
    for the amount of preparation provided by the staff.

**What do you think was the toughest part of the challenge?**

1. **Óscar:** All the sections had their own level of difficulty,
    but I felt we needed more practice for the reversing and pwning categories.
    We also fell a bit short with our strategies
    and analysis in the Attack-Defense-style CTF.

<!-- end list -->

1. **Alejandro:** Personally,
    I believe that making our strategy work out was the toughest part,
    since we did not know each other personally
    until a few days before the challenge
    and so had a short time to extend comments for improvement
    and indications to apply in the competition.

<!-- end list -->

1. **Said:** I think
    that the reversing and pwning categories are somewhat complicated areas
    to understand,
    and the competition is based a lot on them.

**Do you find that the vulnerabilities encountered during the competition
resemble those you find in the systems you evaluate on a day-to-day basis?**

1. **Óscar:** No. The CTFs seek to add a bit of complexity to the challenges
    so that you have to use more ingenious solutions
    or spend more time figuring things out.

<!-- end list -->

1. **Alejandro:** I think yes.
    Each company that creates the machines or challenges for these games
    tries to do it in their own way,
    but you always find similarities to what you find in traditional pentests.

<!-- end list -->

1. **Said:** Not at all.
    At least the Jeopardy CTFs,
    they are designed to be a puzzle
    and the vulnerabilities will be more elaborate
    than those found by someone working daily in the area.
    In the Attack-Defense section, though,
    it is possible to find more similarities
    to what you see in a day's work as a red team and blue team.

<div>
<cta-banner
buttontxt="Read more"
link="/solutions/red-teaming/"
title="Get started with Fluid Attacks' Red Teaming solution right now"
/>
</div>

**What do you think are some non-technical skills required by the ICC?**

1. **Óscar:** Communication and teamwork.

<!-- end list -->

1. **Alejandro:** Teamwork and communication.
    Just like in professional sports teams,
    it is necessary to express ourselves
    and help each other in quick and effective ways.

<!-- end list -->

1. **Said:** I think that soft skills contribute a lot to successful networking
    outside the challenge.
    During the challenge,
    I think it's the technical skills that play the biggest part.

**Which skills and knowledge are you motivated
to strengthen after this competition?**

1. **Óscar:** I would like to improve my knowledge in reversing and pwning.
    They are very useful to succeed in Attack-Defense,
    along with the knowledge in the web and cryptography categories.

<!-- end list -->

1. **Alejandro:** I am motivated to learn more about binary exploitation.
    I feel like it is a very interesting area
    and a very useful one when exploring careers in cybersecurity.

<!-- end list -->

1. **Said:** After the competition
    I have strengthened my knowledge in reversing.
    I've also worked on my skills for Attack-Defense.
    Since I had been dedicated 100% to offensive security,
    I had to start practicing defensive security.
    So,
    what I have studied are some techniques and tools
    that help identify attacks on systems.
    Also,
    I've practiced on platforms like HTB Academy and HTB Battlegrounds
    to learn new things.

**What do you think are the skills and knowledge a person must possess
to be a part of the Latin American team?**

1. **Óscar:** First,
    I would say they should have a passion for cybersecurity and challenges.
    They should be persistent.
    Now technically,
    they should be skilled in some of the categories of the CTFs.
    It's not necessary to be good at all of them.
    In fact,
    the idea of there being several people on the team is
    that they are good at different things.
    Ideally,
    each person should be a specialist.
    To practice,
    I recommend heading to CTFtime,
    where there are CTFs almost always and of all skill levels,
    from beginner to advanced.

<!-- end list -->

1. **Alejandro:** They should be someone
    who specializes in the categories of the CTFs,
    or someone who has extensive knowledge of penetration testing.
    Now,
    you won't always know the solution to a problem,
    but having a strong base knowledge is important to learn along the way.
    Everyone interested could benefit from training on Hack The Box,
    TryHackMe
    and PwnTillDawn.

<!-- end list -->

1. **Said:** I think people looking to compete nationally and internationally
    should practice in CTF events that one can find on the CTFtime platform,
    identify which area they like the most
    and focus on that area.
    It also helps a lot to read other people's writeups
    to know how they solved a particular challenge
    and make community with people who know more than you.

**This and last year,
the Latin American team got sixth place.
How could the team improve its performance for the next competition?**

1. **Óscar:** I think it may improve by modifying the selection process
    so that it seeks people who are specialists in each topic of the ICC.
    Once the team is formed, the members can start training much earlier
    and working many more times together than in the previous challenges.

<!-- end list -->

1. **Alejandro:** The team may benefit from doing as Team Europe,
    starting the selection process one year before the event
    and practicing as a team from that time on.

<!-- end list -->

1. **Said:** Based on what I experienced last year
    and what I have seen this year,
    I think that the team's performance may be improved
    by a better selection process
    and more effective actions by the coaching staff.
    The former should focus on recruiting a number of skilled persons
    per category;
    for example,
    people whose strength is reversing,
    others who are better at pwning,
    and so on.
    And the coaching staff should hold a bootcamp
    to strengthen the team members' skills
    and motivate training from several months before the challenge.
    Of course,
    every selected participant should be self-motivated
    and need little incentive to work on strengthening their skills.
    Finally,
    I think that as the ICC starts to gain more coverage,
    more people will know about it,
    and so there are more chances of reuniting a more diverse set of talents.

**Alejandro, because of your age,
you will still be eligible for next year's ICC.
Would you like to participate in it?**

1. **Alejandro:** Yes, I'll be waiting for the new call
    and apply to earn my place again in the challenge.

We are grateful to our hacking team members
for sharing their experience with us.
We hope that you find their recommendations useful
and are motivated to take part in next year's ICC.
Find the contact information
of the Cyber Team Latin America & Caribbean on [their website](https://icclatinoamerica.org/).
Or find the team that interests you
on the International Cybersecurity Championship and Conference (IC3) [website](https://www.ic3.games/2023-teams).

</div>
