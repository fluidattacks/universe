---
slug: cvssf-risk-exposure-metric/
title: What Your Risk Management's Missing
date: 2024-11-29
subtitle: Why measure cybersecurity risk with our CVSSF metric?
category: philosophy
tags: cybersecurity, risk, company
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1668626573/blog/cvssf-risk-exposure-metric/cover_cvssf.webp
alt: Photo by Maxim Hopman on Unsplash
description: We present some of the flaws of the traditional measure of cybersecurity risk and introduce CVSSF, the risk-exposure-based metric with which we overcome them.
keywords: Cvss, Cvssf, Risk Exposure, Risk Management, Cybersecurity Risk Management, Cybersecurity Success, Security Status, Ethical Hacking, Pentesting
author: Jason Chavarría
writer: jchavarria
name: Jason Chavarría
about1: Content Writer and Editor
source: https://unsplash.com/photos/HBkuFoJuwDI
---

Cybersecurity incidents are on the rise,
partly evidencing the fault in the traditional approach
that guides how to [prioritize risk management](../what-is-vulnerability-management/).
Thus,
organizations worldwide are in urgent need of alternatives
that yield useful information
to help them make better decisions in cybersecurity.
To contribute to solving this problem,
we propose our **CVSSF metric**.
This is a risk-exposure-based value
that enables organizations
to improve the prioritization of vulnerabilities in remediation.
And mainly,
it gives them a more accurate measure of their security status.
In this blog post,
we will talk about the motivations for this metric
and evidence of its worthiness.

## Flaws of the traditional measure of cybersecurity risk

The cyber threats to organizations worldwide continue to grow.
The [number of devices connected to the Internet](../trend-forecast-for-2024/)
is getting higher.
Thus,
organizations' attack surfaces
(i.e., points of exposure of IT systems to untrusted sources)
are bigger,
along with the chances of getting breached.
The [exploitation of single critical vulnerabilities](../what-trends-to-expect-for-2023/)
in third-party software
threatens to severely disrupt supply chains.
Attackers keep compromising [a great number of industries](https://blog.checkpoint.com/research/check-point-research-reports-highest-increase-of-global-cyber-attacks-seen-in-last-two-years-a-30-increase-in-q2-2024-global-cyber-attacks/).
And all this comes
as the average cost of a data breach keeps breaking records.

The threats above are things
that every organization has to deal with
in their way to create value for their clients.
In short,
**aiming for success is a risky business**.
Indeed,
managing risk is arguably the most important issue to resolve in cybersecurity.
Accordingly,
standards and frameworks by renowned sources
(e.g., [NIST](../nist-supply-chain-risk/),
[ISO](../iso-iec-27002-2022/))
give firms a pretty good idea of controls to mitigate risk.
However,
the sheer amount of successful cyberattacks suggests
that the way companies are prioritizing risk mitigation is not working.

What is the faulty approach to cybersecurity risk management?
Well,
most organizations use scores and risk maps from known standards.
This traditional approach,
of course,
takes into account that vulnerabilities differ
in the degrees to which they would affect a system if exploited.

One measure is the **Common Vulnerability Scoring System**
([CVSS](https://www.first.org/cvss/specification-document))
score,
widely used for communicating the characteristics
and severity of vulnerabilities.
Basically,
it quantifies the vulnerability's exploitability, scope and impact.
That is the base score,
which is further adjusted by temporal and environmental variables.
The result is a **severity value between 0.0 and 10.0.**
Most importantly,
this range is broken down into a qualitative severity rating scale,
where 0.0 = none;
0.1 - 3.9 = low;
4.0 - 6.9 = medium;
7.0 - 8.9 = high,
and 9.0 - 10.0 = critical.

Vulnerability scanning products generally provide the CVSS scores
of detected vulnerabilities.
This information is supposed to help organizations
reduce uncertainty in regard to what their security status is
and what to remediate first.

Usually,
to get an idea of the state of security,
what comes to mind is the aggregation of the number of vulnerabilities
and their CVSS scores.
This poses a difficulty.
Suppose we have ten vulnerabilities with a score of 1.0 each
in our application X
and one vulnerability with a score of 10.0
in our application Y.
The former would not be at a higher,
or even the same,
risk for having more security issues than,
or equaling in added-up score to,
the latter.
It is the app Y that should be prioritized for vulnerability remediation.
Moreover,
success in remediating the ten vulnerabilities in app X
should not be considered as valuable
as fixing that one critical flaw in app Y.

<div class="imgblock">

![CVSSF risk management - Fluid Attacks](https://res.cloudinary.com/fluid-attacks/image/upload/v1668627114/blog/cvssf-risk-exposure-metric/cvssf-risk-management-fluid-attacks.webp)

<div class="title">

Ten CVSS 1.0 vulnerabilities do not equal one CVSS 10.0 vulnerability.

</div>

</div>

Here is where the grouping of vulnerabilities into their severity ranges
seems useful.
Assuming our two applications have many more vulnerabilities
with different CVSS scores,
we could keep a tally of,
let's say,
low, medium, high and critical severity vulnerabilities
which we would try to address separately.
This approach would yield
a complicated view of the security status of each system,
as it would not be unified.
This could be tolerated.
But a basic flaw is
that the segmentation of CVSS scores into qualitative levels is arbitrary.
For example,
a vulnerability is considered critical
only if it has been given a score of at least 9.0,
so one that is given a score of 8.9 is labeled as of high severity.
It is that mere difference of 0.1
that would mean the prioritization of the former,
while both could be threatening the system
with a fairly similar degree of risk.
Even worse,
the latter may be seen as having the same priority
as another vulnerability with a score of 7.0
since both are part of the high severity range.

<div>
<cta-banner
buttontxt="Read more"
link="/solutions/vulnerability-management/"
title="Get started with Fluid Attacks' Vulnerability Management solution
right now"
/>
</div>

## Fluid Attacks' risk-exposure-based metric

At Fluid Attacks,
we recognize the limitations of the traditional approach.
This is why we propose a new metric to contribute to measure
— mostly to report — risk more accurately,
allowing organizations to improve their perception
of their systems' risk exposure
(i.e., the extent to which a system is vulnerable to successful cyberattacks)
and the prioritization of vulnerabilities for remediation.
We call it the CVSSF metric,
as it takes the CVSS base score
and adjusts it to show the exponential nature of severity.
"F" stands for "à la Fluid Attacks."
The formula is the following:

<div class="imgblock">

![CVSSF risk formula - Fluid Attacks](https://res.cloudinary.com/fluid-attacks/image/upload/v1668629343/blog/cvssf-risk-exposure-metric/cvssf-risk-formula-fluid-attacks.webp)

</div>

In this formula,
the constant P means "proportionality."
It determines
how many vulnerabilities of a given CVSS base score equal one vulnerability
of one more CVSS base score unit.
This can be determined by each organization.
But what have our security experts observed?
They have reached a consensus to give P a value of 4.
You can understand this as follows:
"Four vulnerabilities of a CVSS base score of 9.0 equal one of 10.0."
As for the constant R, it means "range"
and restricts the values of the CVSSF
to ones that the organization feels comfortable using.
Again,
we recommend using 4.

<div class="imgblock">

![CVSSF risk formula with values - Fluid Attacks](https://res.cloudinary.com/fluid-attacks/image/upload/v1668629362/blog/cvssf-risk-exposure-metric/cvssf-risk-formula-with-values-fluid-attacks.webp)

</div>

Our adjustments to CVSS values with our CVSSF formula enable us
to provide more or better visibility to the riskiest vulnerabilities
in a system.
Using the scale illustrated above as an example,
those ten vulnerabilities at the left side together equal a CVSSF of 0.2.
Meanwhile,
the one vulnerability at the right has a CVSSF of 4,096.0.
A striking difference!

## Benefits of the CVSSF

The riskiest vulnerabilities may not appear as frequently
as low or medium severity vulnerabilities,
so when they all are aggregated as per the traditional approach,
the importance of the riskiest could be overshadowed.
When organizations use the CVSSF,
they can see their aggregated risk exposure rise more dramatically
than it otherwise would
if one high or critical severity vulnerability is reported in their systems.

Case in point:
Our [State of Attacks 2024](https://fluidattacks.docsend.com/view/556tpmq4furacqck)
documented that 94.1% of the vulnerabilities we detected
in the systems of our clients
were of a low or medium CVSS severity.
Yet it was the remaining 5.9%
that generated 94.9% of the reported risk exposure.
They were exclusively high or critical CVSS severity vulnerabilities.
It is, therefore, clear that
what should matter most to organizations is not the number of vulnerabilities
but the risk exposure they represent.

To illustrate this last point even better,
let's look at the case of a report in our platform's Analytics section
for a demo organization.
Chart 1 shows us vulnerability management over time,
while Chart 2 shows us risk exposure management (CVSSF) over time:

<div class="imgblock">

![Chart of vulnerabilities](https://res.cloudinary.com/fluid-attacks/image/upload/v1732919915/blog/cvssf-risk-exposure-metric/vulnerabilities_over_time_cvssf.webp)

<div class="title">

Chart 1. Vulnerability management over time.

</div>

</div>

<div class="imgblock">

![Chart of risk exposure](https://res.cloudinary.com/fluid-attacks/image/upload/v1732919915/blog/cvssf-risk-exposure-metric/exposure_over_time_cvssf.webp)

<div class="title">

Chart 2. Risk exposure management over time.

</div>

</div>

Although most of the bars have a similar behavior to their counterparts
in the other chart,
something to highlight occurred in the fifth report (**2021-12**).
The difference between open vulnerabilities and open risk exposure percentages
is the most pronounced.
We can immediately deduce that
although the total number of open vulnerabilities
for that organization
is relatively low (**21.5%**),
they represent a high total risk exposure at that time (**61.6%**).
Hence,
focusing only on the number of vulnerabilities
to account for the security status of that organization
would be a misconception.

So,
organizations using the CVSSF can **promptly prioritize remediation**
of vulnerabilities that represent the higher risk exposure.
If successful,
this would cause the aggregated risk exposure to go down significantly.
Also,
their uncertainty about their security status can be greatly reduced,
as they have a **sounder aggregated value** at their disposal
on which they can **base their goals for cybersecurity success**,
and which they can **use for predictions of potential losses**.

## Fluid Attacks detects the risk exposure in your system

At Fluid Attacks,
we offer [Continuous Hacking](../../services/continuous-hacking/)
to help our clients know and improve the security status of their applications
throughout the software development lifecycle.
We do this by leveraging security testing early and continuously
using a combination of automation and the manual work of our pentesters,
an approach that serves not only to detect vulnerabilities
but also to remediate them.

We've found
that implementing our manual security testing
(e.g., [penetration testing as a service, PTaaS](../../solutions/penetration-testing-as-a-service/))
is invaluable.
During 2023,
our pentesters reported **more than 97%** of the critical vulnerabilities
and **71.4%** of the total risk exposure
attributed to vulnerabilities in clients' systems.
If clients depended only on automated tools,
their risk-based vulnerability management would be jeopardized.

Want us to assess your applications?
On our [platform](../../platform/),
you can see the aggregated risk exposure,
as well as its management, over time,
and benchmark it against that of best-performing organizations.
From there,
you can also assign remediation to developers in your team,
get remediation suggestions from generative AI,
ask our pentesters for guidance for more complex issues,
and request reattacks to verify the effectiveness of your remediation efforts.

Start your [21-day free trial](https://app.fluidattacks.com/SignUp)
of our Continuous Hacking Essential plan today.

> **Note:**
> For a summarized presentation of our CVSSF metric,
> download the [white paper](https://fluidattacks.docsend.com/view/4r7qehi7a35ndbqx).
