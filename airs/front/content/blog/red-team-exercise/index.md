---
slug: red-team-exercise/
title: Red Teaming
date: 2019-09-18
subtitle: What is it, how does it work and what are its benefits?
category: philosophy
tags: cybersecurity, red-team, security-testing, pentesting, social-engineering, credential
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1665159428/blog/red-team-exercise/cover_red_team_exercise.webp
alt: Photo by Daniel Cooke on Unsplash
description: After reading this post, you will understand what a red team in cybersecurity is, how red teaming works and what benefits it can bring to your company.
keywords: Redteam Security, Red Team Cyber Security Definition, Security Red Team, Red Team Kill Chain, Red Team Hacking, Red Team Exercises, Red Team Blue Team Exercise, Ethical Hacking, Pentesting
author: Anderson Taguada
writer: anders2d
name: Anderson Taguada
about1: Software Engineering undergrad student
source: https://unsplash.com/photos/gi8FUu-XXjU
---

How can we be satisfied with cybersecurity defense capabilities
that are only theoretical?
We no longer have to wait to be bombarded by cybercriminals
to realize whether our threat detectors,
firewalls and other defensive strategies and security controls
are efficient.
We no longer have to wait for malicious hackers
to detect vulnerabilities in our systems
to be able to act on them.
Although it may seem counterintuitive,
we can request the service of being attacked in the cybersecurity field.
These attacks are done,
though,
in an ethical and regulated manner
to find out how strong we are.
We can hire professionals to show us vividly
what an attacker could do against us
and how well we and our systems would respond
to those real threats.
In this post,
we will talk about **red teams** and **red teaming**
so that you can better understand this solution.

## What is a red team?

We are not referring to Liverpool,
Bayern Munich, or some similar team here.
We are focused on a [red team in cybersecurity](../../solutions/red-teaming/).
This "cyber red team" is a group of security experts and professional hackers,
inside or outside the target company,
with the required skills and,
of course,
the permission to simulate "real-world" cyberattacks.
In these attacks or red team activities,
playing specific roles,
red team members must emulate tactics,
techniques and procedures used by today's threat actors
to compromise an organization's systems and cyber defenses.
Red team testing is a kind of [security testing](../security-testing-fundamentals/)
that evaluates the effectiveness of the company's threat prevention,
detection and response strategies.
It delivers reports of detected vulnerabilities and exploitation impacts
as feedback for vulnerability remediation
and improvement of the organization's cyber defenses.

Typically,
when we talk about **red teams**,
**blue teams**
(and sometimes **purple teams**)
come up in the conversation.

## Red team vs. blue team vs. purple team

A **red team** is made up of specialists in offensive security
([ethical hackers](../what-is-ethical-hacking/))
whose mission,
as mentioned above,
is to impact and compromise an information system and its defenses.
The size of this team is variable
and sometimes depends on the complexity of the tasks.

### Red team skill set

The members of this team are expected
to have extensive technical knowledge,
creativity and cunning
to gain access to systems and move through them undetected.
Ideally,
their skills and backgrounds are miscellaneous.
Some may be more adept,
for example,
in software development,
[penetration testing](../penetration-testing/) (aka pentesting),
social engineering, business knowledge,
IT administration, threat intelligence
or security controls and tools.

<div class="imgblock">

![Red team skills](https://res.cloudinary.com/fluid-attacks/image/upload/v1693526970/blog/red-team-exercise/skills_red_team_exercise.webp)

<div class="title">

Red team skills.
([Image](https://miro.medium.com/v2/resize:fit:1400/0*h1bxXqkRpVBuM9TF)
taken from [medium.com](https://medium.com/@redteamwrangler/how-do-i-prepare-to-join-a-red-team-d74ffb5fdbe6).)

</div>

</div>

### How to become a red teamer

Becoming a red team member is complex and requires
education, experience, skill development, and a particular mindset.
It’s necessary to first build a strong foundation in cybersecurity,
either by formal education or intense self-thought training.
Certifications related to red teaming solidify expertise.
Gain practical experience through labs, bug bounty programs,
or by being in a blue team.
Networking with industry professionals
provides invaluable connections and knowledge.
Soft skills like communication and critical thinking
are crucial when it comes to teamwork and reporting findings.
There is an inherent ethical responsibility in red teaming,
which must always be upheld and remembered.
Embrace continuous learning and explore new areas,
evolving with the ever-changing cybersecurity landscape.
If interested,
we recommend our [Tribe of Hackers posts series](../tribe-of-hackers-1/)
based on the book by the best-selling author
(and hacker) Marcus J. Carey.

### What is a blue team?

A **blue team** is made up of specialists in defensive security,
including incident response consultants,
for example.
They must guide the organization to assess its environment
and organize, implement and improve security controls
to identify and stop or deal with red team intrusions
or real threat actors.
Their defensive work seeks to prevent damage
to the structure and operations of the organization's systems.
In some cases,
the blue team may intervene in the planning
or implementation of recovery measures.
The members of this team must fully understand security strategies,
both at the technological and human levels.
They must be highly skilled in the detection of threats,
the appropriate response to them
and the correct use of tools that support these purposes.

### What is a purple team?

Finally, [the purple team](../purple-team/).
From childhood,
you know that mixing red and blue makes purple.
So,
in cybersecurity,
a **purple team** is made up of offensive and defensive security experts.
Sometimes,
this team is referred to as an intermediary,
responsible for assessing,
coordinating and facilitating interactions
between the red and blue teams.
Other times,
it is referred to as members of both teams
collaboratively working in unison.
The intention is to maximize the contributions of both groups of experts
in favor of an organization's cybersecurity posture.

Despite all this,
it is generally enough to speak only of red and blue teams.
Both of them can have as a knowledge source of tactics,
techniques and procedures of threat actors the [MITRE ATT&CK Framework](https://attack.mitre.org/),
which is a product of real-world experiences.
Usually,
the red and blue teams have a leading role in the **red teaming** practice.
However,
the presence of the latter may not be indispensable.

<div>
<cta-banner
buttontxt="Read more"
link="/solutions/red-teaming/"
title="Get started with Fluid Attacks' Red Teaming solution right now"
/>
</div>

## What is red teaming?

In line with the above,
to define red teaming,
we refer to the offensive art
with which we can help a company know how secure its systems are.
Apparently,
this was a practice that originated in the military context.
In an adversarial approach,
the idea was to confront two teams simulating reality
to evaluate the quality and strength of their strategies.
(Something like this happens in Attack-Defense style [CTF contests](../top-10-ctf-contests/)
for hacker groups.)
This gave rise to the red team and blue team.
In a controlled environment,
a red team attack tests an organization's threat prevention,
detection and response capabilities and strategies
—factors in which a blue team may be involved with plans,
systems and standards.

Many times,
the blue team
or the members of the security team of the company under evaluation
may not be aware that red teaming or simulated attacks are taking place.
(In fact,
this is ideal;
see "[Attacking Without Announcing](../attacking-without-announcing/).")
As we discussed in another post
regarding the [TIBER-EU tests](../tiber-eu-framework/),
a white team,
close to the blue team,
may also come into play.
A white team is a small group in the organization
that may be the only one aware of the red teaming.
This team is responsible for approving and requesting
the initiation and interruption of attacks.
It also acts as a liaison between the other two groups.
It is always expected that,
after red teaming exercises,
the target organization will mature its security
by refining its controls and remediating its vulnerabilities.
Incidentally,
it is the company's mission to ensure that
what it receives is red teaming and not just **pentesting**.

## Penetration testing vs. red teaming

Both red teaming and pentesting are offensive security approaches.
In both cases,
"real-world" cyberattacks are simulated
under controlled conditions
to evaluate system security.
Among the differences often mentioned is that
penetration testing acts on a narrower scope.
For instance,
applications under development
or individual components belonging to a network
are evaluated.
On the other hand,
red teaming is more oriented towards testing a complete enterprise environment
in production
with all its systems involved simultaneously.
It may not focus on finding and reporting all vulnerabilities
as penetration testing does.
The red team may concentrate on meeting specific infiltration
and impact objectives.
Further,
some say that pentesting can be conducted by a single individual,
while red teaming must involve several.

Both approaches can be integrated.
A vendor can initially apply several penetration testing cycles
within a company's [DevSecOps model](../devsecops-concept/).
Based on what's shown in the reports,
the company can perform vulnerability remediation on its systems.
After these cycles,
an organization that is more mature in its security can open the way
to red teaming.
Then,
with specific objectives such as accessing a particular network element
or sensitive information,
for example,
it is possible to test
how effective the remediations have been
and what security gaps there are in the systems
in the face of certain types of attacks.

Red teaming can be seen as a deeper,
holistic and more complex penetration testing.
Because of this,
red teaming can take longer than pentesting.
A red team has to employ stealth and evasion.
It is often said that in red teaming,
as compared to penetration testing,
the surprise factor prevails
(i.e., the blue team is unaware of the attack simulation).
Nevertheless,
this will depend on what is defined between the provider and the client.
Also,
red teaming expands the range of attack types to be used,
adding,
for example,
physical penetration and [social engineering](../social-engineering/)
(e.g., [phishing](../phishing/)).
This means that
not only the systems are put to the test,
but also the personnel who manage them.
That is why it can provide more and better ideas than pentesting
for improving plans for prevention,
detection and response to threats.

## What are the usual red team goals?

Red teams in their work can establish different goals from the beginning.
Here, we highlight the following red team objectives:

- To attack systems in production environments.
- To escalate privileges
  and take administrative control of critical systems.
- To access system files and sensitive information,
  even extract and modify it.
- To not affect the availability of any service
  to customers or users.
  However, they can demonstrate it can be done.
- To not be detected at any stage of the attack.
- To inject custom Trojans.
- To install "backdoors" for future access
  and determine how easy it is to notice their existence.
- To maximize the level of penetration and impact.

## How does red teaming work?

As we pointed out earlier,
and as [Rafael Alvarez](https://co.linkedin.com/in/jralvarezc),
CTO of Fluid Attacks,
[once said](../attacking-without-announcing/),
ideally,
the blue team should not be aware of the execution of red teaming.
In particular,
for the blue team to know the times and places
where the red team intends to attack
may be seen as inappropriate.
"In order to know with certainty the security level of your company,
these exercises must be as close to reality as possible,"
says Alvarez.
The few members of the organization,
i.e., its highest authorities,
who will be aware of the red teaming,
are the ones who will give the red team express authorization
and legal protection
for its **offensive security exercise**.

### What are some common red team tactics?

The classification of red teaming tactics can be made
largely according to the targets of evaluation:

- **Application pentesting:**
  The red team evaluates both web and mobile applications
  to identify and exploit vulnerabilities
  resulting from design, development and configuration errors,
  such as those related to access subversion,
  unexpected injection, and functionality abuse.
- **Network pentesting:**
  Experts look for misconfigurations and other weaknesses in network security
  that serve as access points to the network and the systems connected.
  Sometimes,
  they even install so-called "backdoors" for further access to the network,
  install malware on some of its units,
  and perform "network sniffing" to monitor its data traffic,
  intercept communications and map the environment.
- **Social engineering:**
  The target for ethical hackers,
  in this case,
  is the members of an organization.
  The red team aims to exploit human weaknesses
  to persuade and manipulate these people
  through tactics such as phishing, [smishing](../smishing/),
  and even bribery to gain access to the organization's systems
  and confidential information.
- **Physical security testing:**
  Beyond attacks on virtual environments,
  the red team can test an organization's physical security controls.
  Here,
  the weaknesses could be in controls such as security guards,
  cameras, locks, alarms, etc.,
  which could allow access to offices or restricted areas
  with critical systems and sensitive information.

### What's involved in a red team exercise?

Red teaming must begin with the establishment of clear objectives,
such as those outlined before.
Once these are defined,
the red team starts a passive and active target reconnaissance.
In this step,
the team collects all the information it can
and believes necessary about context,
infrastructure, equipment, operations, people involved and more.
This process leads the red team
to acquire and develop a variety of red teaming tools,
including assessment, password cracking, social engineering
and exploitation tools,
that suit the objectives and the target company.
The team also carries out weaponization,
where,
for example,
the creation of malicious payloads or pieces of code takes place.

The red team then assesses the attack surface
to identify vulnerabilities and entry points,
including human weaknesses.
Then,
it defines vulnerabilities to exploit.
Through social engineering,
exploitation of a software vulnerability,
or any other attack vector,
the red team gains access to the target
and executes the payload on it.
This allows the team to compromise system components
and bypass security controls.
Attackers always take measures to prevent the blue team
from becoming aware of the intrusion.
If the latter succeeds,
the former is compelled to struggle against any containment effort.

Subsequently,
the red team tries to move within the target,
keeping in mind the objectives that were initially set.
If necessary,
it looks for new vulnerabilities to exploit
in order to escalate privileges
and continue moving through the systems.
Once the purposes of the exercise have been achieved,
the team proceeds to a reporting and analysis stage.
The experts report on what they achieved,
the key identified vulnerabilities
and the performance of the security controls and the blue team
(if there was blue team engagement).
The owners of the assessed systems can then receive support
for vulnerability remediation and improvement of their procedures
to prevent, detect and respond to the types of attacks involved
in the red teaming.

## Red team exercise scenarios

When we asked [Andres Roldan](https://co.linkedin.com/in/andres-roldan),
VP of Hacking at Fluid Attacks,
about the red teaming methodology they use
in the hacking exercises they conduct from the company,
he told us that they follow the "kill chain strategy."

### Kill chain

"Kill chain" is a military term
to describe the steps of an attack.
One of its models is the "F2T2EA,"
which includes the following stages:

1. **Find:** Identify a target through surveillance
   or intelligence gathering.
2. **Fix:** Obtain the specific location of the target.
3. **Track:** Keep an eye on the target
   until it is decided whether to engage or not.
4. **Target:** Choose the appropriate weapons to use against the target
   and generate the intended impacts.
5. **Engage:** Employ weapons against the target.
6. **Assess:** Evaluate the effects of the attack,
   including any intelligence collected at the location.

<div class="imgblock">

![F2T2EA](https://res.cloudinary.com/fluid-attacks/image/upload/v1693529493/blog/red-team-exercise/F2T2EA_red_team_exercise.webp)

<div class="title">

F2T2EA kill chain.
([Image](http://1.bp.blogspot.com/--IWKt1g_8qg/UvTu_2JK6gI/AAAAAAAAAAo/jRp6euZzmGk/s1600/F2T2EA+1.jpg)
taken from [myarick.blogspot.com](http://myarick.blogspot.com/2014/02/f2t2ea.html).)

</div>

</div>

### Cyber kill chain

The American security and aerospace company Lockheed Martin
and its incident team
developed this framework to prevent cyberattacks.
The following are the phases
they identify that adversaries follow
to achieve their attack objectives:

1. **Reconnaissance:** Learn about the target
   employing multiple techniques.
2. **Weaponization:** Combine the attack vector
   with a malicious payload.
3. **Delivery:** Transmit the payload
   through a communications vector.
4. **Exploitation:** Leverage software or human vulnerabilities
   to run the payload.
5. **Installation:** The payload establishes the persistence
   of an individual host.
6. **Command & control (C2):** The malware communicates
   and provides control to the attackers.
7. **Actions on objectives:** Attackers steal data
   or do whatever they have among their ultimate goals.

<div class="imgblock">

![Cyber kill chain](https://res.cloudinary.com/fluid-attacks/image/upload/v1693529982/blog/red-team-exercise/cyber_kill_chain_red_team_exercise.webp)

<div class="title">

Cyber kill chain.
([Image](https://www.lockheedmartin.com/content/dam/lockheed-martin/rms/photo/cyber/THE-CYBER-KILL-CHAIN-body.png.pc-adaptive.1280.medium.png)
taken from [lockheedmartin.com](https://www.lockheedmartin.com/en-us/capabilities/cyber/cyber-kill-chain.html).)

</div>

</div>

### Cyber kill chain 3.0

The cyber kill chain 3.0 is an update
(with minor changes)
of the previous model
for a better defense
that was [suggested by Corey Nachreiner](https://www.helpnetsecurity.com/2015/02/10/kill-chain-30-update-the-cyber-kill-chain-for-better-defense/),
WatchGuard's Chief Technology Officer.
This red team kill chain has the following stages:

1. **Recon**
2. **Delivery**
3. **Exploitation**
4. **Infection**
5. **Command & control - Lateral movement & pivoting**
6. **Objective/Exfiltration**

The above red teaming scenarios are not the only possible strategies
for red teaming.
Keep in mind that you and your colleagues can also develop,
test, modify and implement your own red team strategy.

### Red team exercise example

Take a look at this video from FOX 9 about a red team exercise:

<div class="tc">
<iframe
  width="560"
  height="315"
  src="https://www.youtube.com/embed/YIV0xvatX0M"
  frameborder="0"
  allowfullscreen
  loading="lazy"></iframe>
</div>

## What are red teaming tools?

Linked to this question,
you could also ask yourself,
"What to look for in a red team tool?"
The tools for these security assessment exercises should be helpful
for applying tactics such as those mentioned above.
Red team tools can be classified according to **the phase of red teaming**
in which they serve specific functions.
The following are just a few examples:

### Passive reconnaissance

- **Wireshark:**
  [Wireshark](https://www.wireshark.org/)
  is an open-source tool for analyzing network traffic in real time,
  intercepting it, and detecting security issues in networks.
- **OSINT Framework:**
  [OSINT Framework](https://osintframework.com/)
  is a compilation of OSINT tools filtered by categories
  to facilitate intelligence gathering.
- **Maltego:**
  Maltego is a tool for real-time data
  (e.g., names, phone numbers, accounts, email addresses)
  mining, gathering and correlation.

### Active reconnaissance

- **Nmap:**
  [Nmap](https://nmap.org/)
  ("Network Mapper") is an open-source tool
  for network discovery and inventory
  (e.g., identifying open ports)
  and security auditing.
- **sqlmap:**
  [sqlmap](https://sqlmap.org/)
  is an open-source tool
  for detecting and testing SQL injection vulnerabilities
  and taking control of database servers.
- **OpenVAS:**
  Open Vulnerability Assessment Scanner ([OpenVAS](https://www.openvas.org/))
  is an open-source tool for vulnerability scanning and management.

### Weaponization - Delivery - Exploitation

- **SET:**
  The Social-Engineer Toolkit ([SET](https://github.com/trustedsec/social-engineer-toolkit))
  is an open-source pentesting framework
  for developing and delivering social engineering attacks.
- **Gophish:**
  [Gophish](https://getgophish.com/)
  is an open-source phishing framework for designing,
  scheduling, and launching phishing campaigns
  and tracking their results.
- **Metasploit:**
  [Metasploit](https://www.metasploit.com/)
  is an open-source pentesting framework
  for identifying vulnerabilities
  and developing and executing exploits.
- **BeEF:**
  The Browser Exploitation Framework ([BeEF](https://beefproject.com/))
  is a penetration testing tool
  for using client-side attack vectors
  and exploiting web browser vulnerabilities.
- **Veil:**
  [Veil Framework](https://www.veil-framework.com/)
  is a collection of tools,
  among which the most widely used is Veil-Evasion,
  which was designed to bypass common antivirus products.
- **hashcat:**
  [hashcat](https://hashcat.net/hashcat/)
  is an open-source [password hash cracker](../pass-cracking/)
  for applying methods such as brute force and dictionary attacks.

### Privilege escalation

- **BloodHound:**
  [BloodHound](https://github.com/BloodHoundAD/BloodHound)
  is a tool for mapping and visualizing the infrastructure
  and hidden relationships within Active Directory or Azure environments
  and identifying complex attack paths.
- **PowerUp:**
  [PowerUp](https://github.com/PowerShellMafia/PowerSploit/tree/master/Privesc)
  is a PowerShell tool for checking for common Windows misconfigurations
  and executing Windows privilege attacks
  to escalate privileges on systems running that OS.
- **BeRoot:**
  [BeRoot Project](https://github.com/AlessandroZ/BeRoot)
  is a post-exploitation tool for checking for common misconfigurations
  and providing information on potential ways to escalate privileges.

### Lateral movement

- **CrackMapExec:**
  [CrackMapExec](https://github.com/Porchetta-Industries/CrackMapExec)
  (CME) is a pentesting tool for evaluating and exploiting vulnerabilities
  in Active Directory environments
  and making lateral movements in Windows networks.
- **mimikatz:**
  [mimikatz](https://github.com/gentilkiwi/mimikatz)
  is an open-source "tool to play with Windows security,"
  extracting credentials,
  performing pass-the-hash and pass-the-ticket attacks,
  and building golden tickets.
- **LaZagne:**
  [LaZagne](https://github.com/AlessandroZ/LaZagne)
  is an open-source tool for retrieving passwords
  from different applications stored on local computers
  and aiding lateral movement.

### Command & control

- **Cobalt Strike:**
  [Cobalt Strike](https://www.cobaltstrike.com/)
  is a post-exploitation tool for emulating long-term,
  quiet actors embedded in networks.
  Its signature payload,
  Beacon,
  allows hackers to gain and maintain control of compromised systems.
- **Empire Project:**
  [Empire](https://github.com/EmpireProject/Empire)
  is a PowerShell post-exploitation framework
  for managing and maintaining access to compromised systems,
  evading security solutions.

### Objective - Exfiltration

- **DNSExfiltrator:**
  [DNSExfiltrator](https://github.com/Arno0x/DNSExfiltrator)
  is a tool for copying and transferring
  (i.e., "exfiltrating")
  data over a DNS request covert channel.
- **CloakifyFactory:**
  [Cloakify](https://github.com/TryCatchHCF/Cloakify)
  is a set of tools,
  including those for exfiltrating data in plain sight
  and evading detection by network alerts.
- **Powershell-RAT:**
  [This RAT](https://github.com/Viralmaniar/Powershell-RAT)
  is a tool that acts as a backdoor in Windows machines
  for exfiltrating data as email attachments via Gmail,
  avoiding detection by standard antivirus software.

## The importance of red teaming

You may still be wondering,
"Why do I need red team cyber security?"
Although we already have [a blog post](../why-apply-red-teaming/)
where we extensively justify the value or importance of red teaming
for organizations' cybersecurity,
we can summarize it as follows:
**A closer approximation to reality**.
A cyberattack simulation by a red team puts to the test
the cybersecurity program of an organization,
attending to its prevention, detection, defense and response strategies.
Strategies that the organization has ready to deal with cybercriminals
on a daily basis
and that,
with the red team's assessment,
may show weaknesses
that should be addressed as soon as possible.

Let's take a look at some specific benefits of red teaming.

### Benefits of red teaming

In general terms,
red teaming provides a broad view of the security status of your organization
for a consequent maturation.
Some of the specific benefits of red teaming
we chose to highlight
are the following:

- Testing prevention controls
  (e.g., firewalls, access controls)
  and detection controls
  (e.g., SOC, AntiX, XDR).
- Identifying misconfigurations and security vulnerabilities in the technology
  that's developed and used
  as well as weaknesses in the behavior of people within the organization.
- In relation to the previous benefit,
  increasing team awareness
  about how the human factor can jeopardize the security
  of the organization's assets and operations.
- Disclosing attack vectors
  and how malicious attackers can move through the target
  if weaknesses are not resolved.
- Boosting vulnerability remediation rates.
- Recognizing that implementing defensive measures
  without testing them
  is not reliable enough.
- Strengthening strategies and procedures
  for detection and response to cyberattacks.

## When should you use a red team?

When your organization does not reject or neglect
basic and advanced cybersecurity prevention measures.
When your organization has matured enough in security
to have gone beyond vulnerability assessment through automated tools
or spasmodic pentesting.
Ultimately,
when all of you know sufficiently about your organization's threat landscape
and the preventive and defensive measures you can implement
to enhance its security
and that of your customers or users.

## Checklist for a successful red team exercise

The following checklist,
which can be modified to the needs of the organization,
is an example of what's important to cover when attempting
to execute a successful red team exercise.
It should only be taken into account if the organization matches
the last paragraph's description.

<image-block>

!["Checklist for a successful red teaming exercise"](https://res.cloudinary.com/fluid-attacks/image/upload/v1708460416/blog/red-team-exercise/Check_list_-_red_teaming1.webp)

Checklist for a successful red teaming exercise

</image-block>

## Red teaming by Fluid Attacks

At Fluid Attacks,
we are experts in [security testing](../../solutions/security-testing/).
Our red team can perform [penetration testing](../../solutions/penetration-testing-as-a-service/),
[red teaming](../../solutions/red-teaming/)
and [ethical hacking](../../solutions/ethical-hacking/) services
for your organization
as both parties agree to carry them out.
In line with what's been described above,
we recommend starting with pentesting
to continuing with red teaming
as the security of your organization and its systems matures.
A red team external to your work environment,
unaffected by conflicts of interest,
would perform evaluations without absurd inhibitions
and deliver transparent reports on your prevention,
detection and response measures.
Our ethical hackers have [certifications](../certificates-comparison-i/)
such as OSCP, CEH, CRTP, CRTE, CRTO, eWPTv1, and eCPTXv2,
[among many others](../../certifications/).
They are also very experienced,
possess diverse backgrounds and skills,
and work in different red team roles.
On average,
we offer more than 15 ethical hackers per project.

[Contact us](../../contact-us/)
and let our red team hacking tell you for sure,
with substantial evidence,
what havoc cybercriminals could wreak on your organization's systems.

<caution-box>

The original blog post was expanded and updated by [Felipe Ruiz](../authors/felipe-ruiz/)
in **September 2023**.

</caution-box>
