---
slug: citrix-bleed-and-ransomware-attacks/
title: Ransomware Is Loving Citrix Bleed
date: 2023-12-13
subtitle: Boeing, 60 credit unions, and more, have been impacted
category: attacks
tags: cybersecurity, software, vulnerability, trend
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1702491421/blog/citrix-bleed-and-ransomware-attacks/cover_citrixbleed.webp
alt: Photo by ANIRUDH on Unsplash
description: The U.S. has been a major focus of ransomware attacks leveraging the Citrix Bleed vulnerability. We tell you about recent exploits involving this security flaw.
keywords: Citrix Bleed, Citrix Bleed Vulnerability, Ransomware, Ransomware Gang, Netscaler, Credit Unions, Supply Chain Attack, Ethical Hacking, Pentesting
author: Jason Chavarría
writer: jchavarria
name: Jason Chavarría
about1: Content Writer and Editor
source: https://unsplash.com/photos/a-bunch-of-red-bubbles-floating-in-the-air-UiwUtEqROEs
---

It's been a very productive end of year for cybercriminals
targeting the Citrix Bleed vulnerability.
It's quite a convenient flaw:
By stealing the session cookie,
gangs can gain access to systems
with no need to find and steal login credentials.
The latest news says that over 60 credit unions were forced offline
as their one common provider was infected with ransomware.
It's believed criminals leveraged Citrix Bleed to achieve this.
This impactful incident reminds organizations all around the world
how important it is to watch out for vulnerabilities
in the technology supply chain.
We tell you here about what's been happening recently with this security flaw,
including that latest news.

## About Citrix Bleed

Citrix Bleed (i.e., [CVE-2023-4966](https://nvd.nist.gov/vuln/detail/CVE-2023-4966))
is one of the recent vulnerabilities
that affect Citrix's NetScaler Application Delivery Controllers (ADC)
and Gateway.
NetScaler is a network device that provides load balancing,
firewall
and VPN services
to optimize the delivery of applications over the Internet.
ADC refers to features for load balancing and traffic management,
and Gateway, to the VPN and authentication components.
[Citrix made its advisory public on October 10](https://support.citrix.com/article/CTX579459/netscaler-adc-and-netscaler-gateway-security-bulletin-for-cve20234966-and-cve20234967).
But gangs have been exploiting the flaw since August.
In a vulnerable device,
they can retrieve session cookies
and bypass password requirements and MFA to take over a user session
and gain access to sensitive information.

Citrix developed patches and informed users.
And so people should have the updated versions now.
The implementation is ongoing,
resulting in less devices at risk in the world,
as the following chart from The ShadowServer Foundation shows.
But even this does not stop massive hacks from happening.

<image-block>

!["Number of devices with Citrix Bleed worldwide over time"](https://res.cloudinary.com/fluid-attacks/image/upload/v1702491431/blog/citrix-bleed-and-ransomware-attacks/Figure_number_devices_citrix_bleed_world_over_time.webp)

Number of devices with Citrix Bleed worldwide over time
(source: [dashboard.shadowserver.org](https://dashboard.shadowserver.org/statistics/combined/time-series/?date_range=other&d1=2023-10-11&d2=2023-12-12&source=http_vulnerable&source=http_vulnerable6&tag=cve-2023-4966%2B&group_by=geo&style=stacked))

</image-block>

## Ransomware attacks involving Citrix Bleed

We had mentioned a notable breach related to the Citrix Bleed vulnerability
last month [in our blog](../trend-forecast-for-2024/).
It was the attack to The Boeing Distribution Inc.'s systems
by the Russia-linked ransomware gang LockBit.
The result was a leak of 45 GB of the firm's data.
The gang's use of Citrix Bleed earned a joint [cybersecurity advisory](https://www.cisa.gov/news-events/cybersecurity-advisories/aa23-325a)
by several U.S. agencies
(among which are the CISA and the FBI)
and an Australian security authority.
Their advice was,
of course,
to update and isolate the NetScaler ADC and Gateway appliances.

Yet another giant hit by ransomware in November,
possibly enabled by Citrix Bleed,
was [Toyota](https://therecord.media/toyota-cyberattack-financial-services-divison).
This time,
it was the Medusa ransomware gang who claimed responsibility for the breach.
Moreover,
by the end of the month
the U.S. Health Sector Cybersecurity Coordination Center
issued [a warning to hospitals and healthcare facilities](https://therecord.media/hhs-warns-of-citrix-bleed-bug),
as the NetScaler flaw is at the center of attacks
causing outages in this industry.

The headlines by the start of December were marked
by the **supply chain attack with ransomware**
that disrupted the operation of more than 60 credit unions in the U.S.
The root of this incident was
that criminals gained access to unpatched NetScaler servers
of an information technology services provider common to those credit unions.

<div>
<cta-banner
buttontxt="Read more"
link="/solutions/security-testing/"
title="Get started with Fluid Attacks' Security Testing solution right now"
/>
</div>

The National Credit Union Administration (NCUA),
which insures and regulates the affected financial firms,
[confirmed the intrusion](https://www.theregister.com/2023/12/02/ransomware_infection_credit_unions/),
number of affected orgs
and cause.
The services provider is the business analytics
and talent services organization Trellance Cooperative Holdings Inc,
which owns two further providers,
whose names are Ongoing Operations LLC and Fedcomp.
And it is believed that the ransomware attack,
which went down on November 26,
was likely possible thanks to the Citrix Bleed flaw.
The impact was an outage of credit unions across the country,
depending on Trellance to resolve the issue
and get them back online.

What was the response of the IT provider after learning about the attack?
Ongoing Operations LLC say they immediately began to investigate the incident
and even brought in third-party specialists
who would help them determine its scope and nature.
Plus,
they notified federal law enforcement.
They informed all this to their customers
along with the assurance that they had not found evidence
that the attackers had misused the accessed information.

The NCUA learned fast about this incident.
This kind of works as a reminder
that the [NCUA requires since September 1](https://ncua.gov/files/agenda-items/cyber-incident-notification-requirements-final-rule-20230216.pdf)
that federally insured credit unions report such events
no later than 72 hours after learning of them.
This is while CISA publishes their final rule
with the Cyber Incident Reporting Act’s requirements,
for which the agency has a deadline in 2025.
The Act also states the limit for notifying CISA would be 72 hours.
Accordingly, the NCUA, [reportedly](https://www.theregister.com/2023/12/02/ransomware_infection_credit_unions/),
informed the Department of the Treasury, CISA and the FBI about the event.

The stunts of ransomware gangs leveraging Citrix Bleed don't end here.
Cybersecurity researcher [Kevin Beaumont has posted](https://doublepulsar.com/what-it-means-citrixbleed-ransom-group-woes-grow-as-over-60-credit-unions-hospitals-47766a091d4f)
that HTC Global Services had not updated their NetScaler
by the end of November
and are now being held to extortion by the AlphV ransomware group.
On their ransomware portal,
they display stolen documents that are branded Caretech,
a division of the managed service provider for the U.S. healthcare sector.
And yet another victim of AlphV was Fidelity National Financial,
which had patched NetScaler late.
Without a doubt,
Citrix Bleed needs to be dealt with now.
That is: Organizations need to patch now!

## Secure your supply chain

Spotting the risk of supply chain attacks is hard for organizations,
as they are often unaware of the software they use.
Taking preventive measures is the way to go.
Finding out whether there's vulnerable software in use
and how to solve that must be among everyone's top concerns right now.
Furthermore,
we advise, as always,
that impacted companies **never pay the ransom**.
One,
they can't know for sure that the gang will keep their promises.
And two,
they would perpetuate the attacks.
Instead,
victims need to be thorough in their investigations
and transparent and open when talking about the incidents.

Product developers:
You have a big responsibility in your hands.
The reason supply chain attacks happen in the first place is security holes
in software.
Secure it now.
Scan it for vulnerabilities,
test it in attack simulations
and fix security problems as soon as possible,
all of this **constantly**.
Do not hesitate to ask us about our [Continuous Hacking](../../services/continuous-hacking/)
solution.
You can just [try it for free for 21 days](https://app.fluidattacks.com/SignUp)
and see for yourself
how the overwhelming task of managing vulnerabilities
turns into something you can have under control.
