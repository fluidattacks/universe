---
slug: top-10-crypto-crimes/
title: Top 10 Crypto Crimes
date: 2023-10-02
subtitle: Outstanding incidents of this type of crime since 2011
category: philosophy
tags: software, cryptography, risk, trend, hacking, social-engineering
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1696007796/blog/top-10-crypto-crimes/cover_top_10_crypto_crimes.webp
alt: Photo by engin akyurt on Unsplash
description: Discover some of the most notorious crypto crimes to date, among which we present not only software cyberattacks but also tremendous scams.
keywords: Cryptocurrency, Blockchain, Top Crypto Crimes, Decentralized Finance, Defi Breach, Defi Hacks, Scam, Pentesting, Ethical Hacking
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Content Writer and Editor
source: https://unsplash.com/photos/a-dirty-round-object-sitting-on-a-white-surface-6IuIuDYf9yA
---

After an introduction to the cryptocurrency paradigm and related criminality
in [the previous blog post](../cryptocurrency-and-crypto-crime/),
we move on here to briefly describe some crypto crime incidents
that have stood out since 2011.
(If you are not acquainted with concepts such as blockchain and DeFi protocols,
we invite you to read that post first.)
We took as a primary basis the data reported by [Crystal](https://crystalblockchain.com/security-breaches-and-fraud-involving-crypto/),
a company that,
from January 2011 to February 2023,
has recorded **461 incidents**
in which around **$16.7 billion** has been stolen.

Four hundred sixty-one are so many cases to choose from
to define a top 10.
What we did was base it largely on the amount of money stolen,
but we also considered variety in the type of crime
and the frequency of reference to the cases in other sources.
For this reason,
some incidents with higher amounts of stolen funds are not shown here.
Additionally,
it is worth mentioning that,
in a couple of cases,
we grouped several incidents aimed at the same target
but placed more emphasis on the most important of them.

## 10. PancakeBunny (DeFi breach / 2021 / $49.1m)

PancakeBunny is a DeFi (decentralized finance) protocol
mainly for Binance Coin (BNB).
Associated with PancakeBunny are three incidents,
all in the same year,
but the first of them accounted for the most stolen funds:
**$45 million**.
In this case,
which occurred in May 2021,
the hackers involved carried out a flash loan attack.
[They initially borrowed](https://cointelegraph.com/explained/the-biggest-crypto-heists-of-all-time)
a large sum of BNBs from the pools of the PancakeSwap exchange.
[They then used a bug](https://coinmarketcap.com/academy/article/the-tragicomedy-of-pancakebunny)
to manipulate the exchange rates USDT/BNB and BUNNY/BNB
(USDT is short for crypto Tether,
and BUNNY is the PancakeBunny token)
and acquired a large amount of BUNNY
and dumped them to the market,
which caused their price to plummet.
In a series of complex procedures,
the hackers managed to acquire tokens at one price,
sell them at another,
make a huge profit and repay the initial loan.

## 9. KuCoin (breach / 2020 / $281m)

KuCoin is an Asian cryptocurrency exchange founded in 2017.
In this case,
hackers managed to obtain some private keys
and gain access to several hot wallets of this exchange.
(Unlike cold wallets,
**hot wallets are connected to the Internet**,
so they are considered less secure).
There were then massive thefts of [ERC-20 tokens](https://www.investopedia.com/news/what-erc20-and-what-does-it-mean-ethereum/)
and various cryptocurrencies,
most notably Bitcoin (BTC) and Ethereum (ETH).
[KuCoin reacted quickly](https://www.coindesk.com/markets/2020/09/26/over-280m-drained-in-kucoin-crypto-exchange-hack/),
transferring the remaining funds to new hot wallets,
getting rid of the affected ones,
and freezing deposits and withdrawals.
[It is said that](https://cointelegraph.com/explained/the-biggest-crypto-heists-of-all-time)
the exchange managed to recover around $240 million.
However,
[another source of information](https://www.forbes.com/sites/thomasbrewster/2021/02/09/north-korean-hackers-accused-of-biggest-cryptocurrency-theft-of-2020-their-heists-are-now-worth-175-billion/?sh=80c7b395b0bb)
suggests that
the recovery was complete thanks to the cooperation with partners,
security institutions, and other organizations.
Later,
some researchers attributed this theft to the North Korean attacker crew
called Lazarus Group,
mainly based on their observations
about how the criminals laundered the stolen funds.

## 8. Wormhole (DeFi breach / 2022 / $326m)

[Wormhole](https://wormholecrypto.medium.com/wormhole-incident-report-02-02-22-ad9b8f21eec6)
is a cross-chain message-passing protocol.
On its network,
Portal acts as a token bridge
between the blockchains of the cryptocurrencies Ethereum and Solana (SOL).
In this case,
hackers exploited a **vulnerability in the signature verification code**
of the Wormhole network to,
without depositing funds in a contract on the source chain,
successfully mint 120 thousand Wormhole-wrapped ETH on the target chain,
i.e., Solana.
These ETH tokens,
therefore,
were not collateralized or backed by deposits
on the Ethereum side of the bridge.
From there,
the criminals sent about 93 thousand tokens back to Ethereum,
where they could exchange them for native ETH.
On the other hand,
the remaining tokens were exchanged for SOLs on Solana.
Shortly thereafter,
Jump Crypto,
a team involved in the development of Wormhole,
replenished the missing funds.
A year later,
[Jump Crypto allegedly](https://blockworks.co/news/jump-crypto-wormhole-hack-recovery)
counterattacked the Wormhole exploiters
to recover ETHs stolen from them successfully.

## 7. Coincheck (breach / 2018 / $535m)

Coincheck is a Japanese cryptocurrency exchange,
which in early 2018 had around 500 million NEM tokens
(i.e., [XEM](https://www.investopedia.com/tech/meet-nem-xem-harvested-cryptocurrency/),
the cryptocurrency running on the New Economy Movement blockchain)
[stolen](https://www.coindesk.com/markets/2018/01/26/coincheck-confirms-crypto-hack-loss-larger-than-mt-gox/)
from its digital hot wallets
(apparently they did not use cold wallets),
which were transferred to more or less [20 accounts](https://web.archive.org/web/20180201060121/http://the-japan-news.com/news/article/0004215668).
As noted in the subtitle,
this sum corresponded to **more than $500 million**.
From there,
Coincheck had to freeze several deposit,
trading and withdrawal services.
[According to this exchange](https://coinmarketcap.com/academy/article/coincheck-hack-one-of-the-biggest-crypto-hacks-in-history),
the hackers were able to access their systems
due to a shortage of employees.
Still,
the incident was more associated with limited security controls
and vulnerability exploitation.
The perpetrators of the theft were apparently never identified,
[but in 2021](https://www.coindesk.com/policy/2021/01/22/30-charged-in-japan-with-trading-96m-of-crypto-stolen-in-coincheck-hack/),
there were reports of 30 people
accused of exchanging a significant portion of the XEM stolen in that hack.
Coincheck eventually managed to use its capital
to compensate its 260,000 customers for the losses
and, obviously, was forced to improve its security protocols,
including, [for example](https://en.wikipedia.org/wiki/Coincheck),
enhanced "know your customer" procedures.

## 6. FTX (breach / 2022 / $600m)

[Here's the case](https://www.nytimes.com/2022/11/12/business/ftx-cryptocurrency-hack.html)
of what was once one of the largest cryptocurrency exchanges in the world:
FTX, which, based in the Bahamas,
had its native cryptocurrency called FTT.
This company,
just one day after having filed for bankruptcy
([apparently](https://www.halborn.com/blog/post/explained-the-ftx-hack-november-2022)
due to lack of liquidity and the failure of a deal with Binance),
reported it was investigating unauthorized access and transactions
from its accounts.
This incident came to make things worse
at a time
when the collapse of FTX had already meant **losses of billions of dollars**
in cryptos for the customers.
It seems the perpetrator was either a hacker
or an insider with special access
who intended to abscond with hundreds of millions of dollars,
and they took it upon themselves
[to move and convert](https://www.washingtonpost.com/technology/2022/11/12/ftx-crypto-hack/)
the looted tokens to ETH quickly.
From administration,
this company recommended customers delete FTX applications
and not log in to its website to prevent further theft,
allegedly because there was already malware associated with them.
In addition,
[it said](https://www.coindesk.com/business/2022/11/12/ftx-crypto-wallets-see-mysterious-late-night-outflows-totalling-more-than-380m/)
it had accelerated the transfer of remaining funds to cold wallets.
[Almost a year](https://cryptopotato.com/ftx-hacker-moves-8-mllion-in-stolen-funds-for-the-first-time-in-10-months/)
after the incident,
according to data related to the chain,
a few million dollars were reported
to have been moved from the alleged perpetrator's wallet.

<div>
<cta-banner
buttontxt="Read more"
link="/solutions/secure-code-review/"
title="Get started with Fluid Attacks' Secure Code Review solution right now"
/>
</div>

## 5. Poly Network (DeFi breach / 2021 / $614m)

Poly Network is an international cross-chain protocol that,
as suggested by its name,
serves for the interoperability of diverse blockchains.
This DeFi platform was confronted
with one of the biggest cryptocurrency heists in history
at the hands of a hacker who,
[it seems](https://www.cnbc.com/2021/08/12/poly-network-hacker-behind-600-million-crypto-heist-did-it-for-fun.html),
**did it just for fun**.
He managed to exploit a vulnerability in Poly Network's code
to steal hundreds of millions of dollars in ETH,
Binance Smart Chains (BSC) and Polygon cryptos.
However,
according to his intentions,
the hacker,
[nicknamed Mr. White Hat](https://cointelegraph.com/explained/the-biggest-crypto-heists-of-all-time),
was gradually returning the stolen funds since,
according to him,
"I am _not_ very interested in money!"
He just wanted this to serve as a lesson for them.
Mr. White Hat was even in frequent and public dialogue with Poly Network
about it.
[Indeed](https://www.cnbc.com/2021/08/23/poly-network-hacker-returns-remaining-cryptocurrency.html),
the company ended up offering him a $500k reward
for helping it identify weaknesses in its systems
and, bizarrely enough, a job as "chief security advisor."
[Despite all this](https://decrypt.co/147059/poly-network-attack-conjures-billions-of-dollars-in-tokens-that-did-not-exist),
in the middle of this year,
it was reported that Poly Network had been hacked again.
This time,
the perpetrator had taken advantage of a vulnerability
that allowed them to create new tokens.

## 4. Mt. Gox (breach-fraud / 2011-4 / $615m)

The first of the crypto crime incidents with Mt. Gox is often referred to
as the oldest.
In fact,
it is called the "**first crypto exchange hack**."
Mt. Gox was a Japanese BTC exchange launched in 2010
that once handled 70% of transactions in this cryptocurrency.
In 2011,
cybercriminals compromised the private keys
and, consequently,
the account of one of the platform's administrators
and flooded Mt. Gox with vast amounts of fake BTCs.
As a result,
the price of this cryptocurrency dropped sharply,
[apparently](https://medium.com/hackernoon/a-huge-list-of-cryptocurrency-thefts-16d6bf246389)
to 1 cent!
Then,
the culprits created sell orders on the accounts of Mt. Gox users
and, from there,
were able to buy and withdraw more than two thousand bitcoins
at a very low cost.
That was the first case.
However,
months later,
2,609 BTCs were destroyed due to a programming error.
Finally,
in 2014,
the platform's operations ceased because,
[throughout those years](https://cointelegraph.com/explained/the-biggest-crypto-heists-of-all-time),
the thefts had reached the enormous magnitude of around 850 thousand BTCs,
which corresponded to hundreds of millions of dollars.
In mid-2023,
the U.S. Department of Justice [issued a press release](https://www.justice.gov/opa/pr/russian-nationals-charged-hacking-one-cryptocurrency-exchange-and-illicitly-operating-another)
charging two Russian citizens for hacking Mt. Gox.

## 3. Ronin (DeFi breach / 2022 / $650m)

According to the [Crystal data](https://crystalblockchain.com/security-breaches-and-fraud-involving-crypto/)
we used for reference,
this is **the largest DeFi hack ever**.
Ronin,
another cross-chain bridge,
was reportedly hacked by the aforementioned Lazarus Group.
[It took six days](https://www.elliptic.co/blog/540-million-stolen-from-the-ronin-defi-bridge)
for Ronin to identify and report
what was the result of exploiting human weakness through social engineering.
Explicitly,
the hackers obtained the private cryptographic keys
of some of the bridge's validator nodes
(the mere five needed to approve transactions).
It was then that the U.S. Treasury's Office of Foreign Assets Control (OFAC)
sanctioned their Ethereum address
so that no transactions could be made from it.
However,
it was confirmed that part of the stolen funds had already been transferred
to Tornado Cash,
one of the most popular smart contracts for laundering cryptoassets
on the Ethereum blockchain.
[Ronin](https://cointelegraph.com/news/the-aftermath-of-axie-infinity-s-650m-ronin-bridge-hack)
then had to freeze deposit and withdrawal services temporarily,
and its developer team,
Sky Mavis,
promised to reimburse victims and,
if possible,
recover the stolen funds.

## 2. Thodex  (fraud / 2021 / $2b)

Now,
to close this list,
we have a couple of scams
that lead us to talk about abysmal figures of over one billion dollars.
[Thodex](https://en.wikipedia.org/wiki/Thodex)
was a cryptocurrency exchange platform
founded by Faruk Fatih Özer in 2017
that became one of the most successful in Turkey.
[In 2021](https://www.dw.com/en/turkish-cryptocurrency-platform-founder-vanishes-fraud-suspected/a-57302955),
when this exchange already had around 390 thousand active users,
Özer suddenly shut down the platform
(stating on its website that it would only be for a few days)
and fled Turkey with a sum of around $2 billion in investors' assets.
Before long,
many users were already filing criminal complaints against Thodex
for being unable to access their accounts and funds.
[In mid-2022](https://cointelegraph.com/news/turkish-crypto-exchange-thodex-ceo-11-000-year-prison-for-2-b-scam),
Özer was arrested in Albania and,
in 2023,
deported to his home country,
where he was sentenced to **11,196 years in prison**
for organized crime, fraud and money laundering.
[Özer's brother and sister](https://www.bbc.com/news/world-europe-66752785)
were also convicted on the same charges.

## 1. PlusToken (fraud / 2019 / $2.9b)

[PlusToken](https://www.chainalysis.com/blog/plustoken-scam-bitcoin-price/)
was a cryptocurrency wallet founded in 2018 by Chen Bo
that, as a Ponzi scheme,
promised its users high rates of return
for the purchase of its tokens associated with Bitcoin or Ethereum.
Although operating mainly in China and South Korea,
[it expanded](https://coingeek.com/plustoken-scam-top-operators-jailed-for-up-to-11-years/)
to other Asian countries
thanks to Bo's recruitment of members.
[In 2019](https://www.coinbureau.com/analysis/plustoken-scam/),
users started to report problems when extracting their funds from the platform.
[Apparently](https://www.okta.com/identity-101/plus-token/),
the scammers then withdrew the collected funds in different cryptocurrencies
and posted the ruthless message,
"**Sorry, we have run**."
Although the authorities at the time managed to arrest six people involved,
the transactions made it clear that
many were still fugitives.
The criminals were eventually chasing out several million
in different exchanges until,
in mid-2020,
the Chinese government put an end to the fraud
by arresting 109 traders,
of whom 27 were the alleged masterminds.
Chen Bo and other perpetrators [were sentenced](https://www.coindesk.com/policy/2020/12/01/ringleaders-of-plustoken-scam-jailed-for-up-to-11-years/)
to between two and 11 years in prison
and received fines ranging from $18,000 to $900,000.
