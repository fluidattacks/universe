---
slug: undersupported-digital-infrastructure/
title: Undersupported Infrastructure
date: 2024-04-17
subtitle: We need you, but we can't give you any money
category: opinions
tags: software, code, risk, vulnerability, web, cybersecurity
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1713388624/blog/undersupported-digital-infrastructure/cover_undersupported_digital_infrastructure.webp
alt: Photo by Brian Kelly on Unsplash
description: We want to persist in raising awareness about the scarce support that many open-source software projects, on which nearly everyone depends, are receiving.
keywords: Digital Infrastructure, Open Source Software, Open Source Projects, Roads And Bridges, Software Components, Undersupporting, Underfunding, Pentesting, Ethical Hacking
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Content Writer and Editor
source: https://unsplash.com/photos/a-bridge-over-a-forest-IYz4lLZ1su0
---

The phrase that makes up this post's subtitle was [once uttered](https://www.informationweek.com/it-leadership/ntp-s-fate-hinges-on-father-time-)
by software developer Harlan Stenn,
president of the Network Time Foundation.
With it,
Harlan was letting us know people's usual,
generally unspoken stance
on **the plight of the public digital infrastructure**
and many of its open-source software components
on which we all depend in one way or another.

A few days ago,
in [the post about the joke of someone in Nebraska](../nebraska-joke-infrastructure-dependency/),
we pointed out the enormous dependence that business giants,
as well as small and medium-sized companies,
national governments and,
in general,
Internet users have on those components or projects
that are often maintained by a few subjects,
with poor or without any remuneration.
As we mentioned there,
we intended to write a post about this problem of underfunding
—which is part of a broader problem of "undersupporting"—
and here it is.
This text is mainly based on the report *[Roads and Bridges](https://www.fordfoundation.org/work/learning/research-reports/roads-and-bridges-the-unseen-labor-behind-our-digital-infrastructure/):
The Unseen Labor Behind Our Digital Infrastructure* (2016),
written by Nadia Eghbal,
and arises because it addresses an issue
that has not received enough attention even today.

## Introduction

We know that the open-source software community
typically works on a decentralized,
non-hierarchical basis
—the irony is that the current digital capitalist economy is forged
and maintained on so many of its free projects.
So,
no central organization or authority grants permissions
or determines what is to be built,
maintained, and used in the infrastructure.
(Outliers are, for example, the IETF and W3C
that establish specific standards and requirements
in the most fundamental pieces or basic levels of the web).

From this community,
individuals and groups create programming languages,
frameworks, and libraries,
some more complex than others,
and make them available to anyone who wants or needs to use them.
Usually,
a new project is expected to improve an existing one
—bringing new features or solving some of its problems—
to be considered useful or even "the best available option"
and worth adopting.
Some projects turn out to be more preferred than others,
and some simply end up being ignored.

Successful projects become increasingly known,
implemented and used by teams of developers and technology companies.
Apart from their good functionality,
these components are widespread
thanks to factors such as the developers' reputation,
the attractiveness of the products' names,
and the advertising campaigns carried out.
While some open-source projects originate within a company
and/or as part of a business,
here we focus mainly on those
that derive from independent individuals or communities.

Consumers,
in turn,
take advantage of these public goods
to solve their specific problems.
Companies, for example, turn to open-source software components
to build and support their own products and services
and make money from them.
As more and more people demand software components,
often maintained by a few developers,
imbalances begin to appear.
Consumers constantly send inordinate and outrageous requests
to these individuals
while providing them with no retribution.

## Why do they generate open-source software?

Usually,
it is volunteer subjects
who work on developing and maintaining open-source projects.
They do it as a hobby,
an art they love and find satisfying,
and as a way to solve their problems.
These purposes can be linked
to the desire to affect the lives of others positively,
but, in general, we're not talking about "altruism."
Many developers and maintainers of open-source projects need to think about
their future economic prosperity,
even when money may remain a taboo subject in the open-source community.

Money may still be seen by many as a perversion of the original idea
of the open-source community,
but let's face it:
Blessed are those few who work full-time as volunteers on these projects
and have no need to receive a penny.
The truth is that one of the most common intentions of contributing developers
is to gain a reputation.
They usually intend to be recognized in the communities,
prove their worth,
and build their portfolio of work
(sometimes with several small contributions),
and then have the opportunity to get compensated
to sustain their projects
or merely be hired in large companies
where they are well remunerated.

When developers are tied to these projects for one reason or another,
they feel the heavy burden,
which sometimes,
when a project has been widespread,
is as if it becomes an "ethical obligation" to the world.
This burden reflects injustice,
especially in the inequality of economic retribution.
Developers who maintain open-source projects,
often full-time,
on which companies, government entities, and others rely
earn nothing or very little compared to those
who are part of these organizations.

## Concerns for developers and maintainers

As Nadia comments,
there are more and more posts and public conversations on the web
about the fatigue and stress experienced by these volunteers.
Many times,
only one or two individuals are in charge of a project's survival.
Support from many other people is often casual or infrequent.
Based on the above,
some contributors arrive, lend a hand for a while,
are recognized,
and then fortunately hired by a company,
so they abandon their support for the project without a second thought.

Sometimes,
old open-source projects have trouble finding even contributors
within the community.
Most developers prefer to get to work on new and popular projects
that catch their attention
and may further serve their purpose of gaining reputation,
and not on those that are solid, fundamental, "well-established enough,"
and have worked well so far for users or consumers.

Some developers and maintainers of open-source projects
indeed earn income by consulting and supporting consumers
in using their software,
especially when the projects are widely used
and people are willing to pay for them.
However,
the resulting income is often not enough
to cover decent salaries and,
at the same time,
investments in security and servers,
for example.
In addition,
when few people are involved,
providing a service can represent an obstacle to the project's progress
since investing a lot of time
in activities other than developing and maintaining the software
is necessary.

What starts out as a fun software development project
may become an ordeal for developers
when they have to fix bugs or vulnerabilities reported to them
(some resulting from low-quality contributions),
perform quality checks on contributions,
and deal with criticism or complaint comments.
Sometimes,
criticism comes from the contributors themselves
or individuals who only point out the projects' shortcomings
but do not pitch in with anything.
And claims for new features or consulting come from users
of that free software,
sometimes even high-caliber,
well-established companies with huge revenues,
who shamelessly have certain expectations of those projects
on which they depend.

It's here that institutional support becomes necessary.
As in the case of OpenSSL,
some sympathetic people create foundations and new roles
that are not technically oriented
but rather focused on these projects' management, design, marketing,
communication, and sustainability.
Some organizations provide this assistance
but don't get involved with financial support.
Other institutions,
such as the Linux Foundation and the Mozilla Foundation,
have gotten support for specific projects
from various corporations and organizations.
Software companies,
for instance,
sometimes contribute by reporting security issues,
suggesting changes, and providing feedback.
Nonetheless,
the strength and coverage of this support remain like gold dust.

## Concerns for all of us

Tired,
exhausted from managing these projects
(perhaps already with other priorities in mind)
and getting no rewards other than the pleasure of programming,
these developers may at any time abandon their projects,
sometimes in the hope that someone else will keep them going.
This can represent a loss of skilled labor
and a slowed growth of innovation.
Without volunteers to maintain them,
each consumer would have to look for similar open-source software components
or take on the projects and preserve them independently,
among other possible solutions.

Moreover,
either because of the burden placed on developers
or because of the intervention of amateur developers
—without adequate education in cybersecurity
or not paying much attention to it—
the infrastructure is left vulnerable,
more prone to abuse and attacks
that would mean incidents such as service interruptions.
If a project is abandoned or suffers security breaches,
it must surely get some fame and,
as a consequence,
perhaps for a while,
some more support.
But what about those similar projects
that remain unpopular and undersupported
but are still pillars of our infrastructure?

<div>
<cta-banner
buttontxt="Read more"
link="/solutions/vulnerability-management/"
title="Get started with Fluid Attacks' Vulnerability Management solution
right now"
/>
</div>

## Lack of awareness and support

Often,
developers or maintainers have no idea who is using their software,
and many consumers do not have clear visibility
into what software components they are employing
or do not know who those libraries depend on
or what their status or situation is.
Additionally,
there are often misconceptions
that this work by open-source software developers is well-funded
and has extensive and constantly active teams.
In fact,
what are usually exceptional cases of sufficient funding
can be seen as if they are the rule.

Certainly,
there is sponsorship from software companies and development groups
and donations from foundations,
academic institutions and corporations,
but these are not enough.
Platforms have emerged to make donations to maintainers
to support their work,
but, as mentioned in Nadia's report,
it ends up being,
figuratively speaking,
money for a couple of beers but not to pay the rent.
Relying on the goodwill of the people in non-recurring donations
is not promising.

Likewise,
it is not easy to find cooperation between companies
that are competing within a market.
A company may not want to sponsor a project
that also benefits its competitors
when they are not contributing to it at all.
There's also a problem when a private benefactor wants special privileges
(especially if it is shown to be the sole or major financial contributor)
or when there is support from a government with political interests,
thus jeopardizing the neutrality of the project.
The intention to preserve such neutrality,
along with decentralization,
may then limit many potential financial supports.

## More support is needed

The maintenance of some projects is much more expensive than others.
When they are tiny,
the maintenance required is not significantly high.
In other cases (usually large projects),
big companies that rely on specific packages assign their own personnel
to be on call to contribute to their maintenance,
relieving the burden on those individuals originally responsible
(even monetary support arrives),
or they also hire some of the contributors to work on the project full time.
The biggest concern is for projects large enough
to require significant maintenance
but not large enough to receive corporate attention and support.

Today,
we need more collaborative work to maintain our digital infrastructure.
We should see that support as a shared responsibility.
But to do that,
we need a broader and more precise understanding of the situation first.
From there,
we could expect the emergence of more advocates,
without political or commercial constraints,
of this approach
that we should not afford to lose.
The innovation and global progress this approach has brought us is unmatched,
so it would not be suitable
to end up in a "closed-source software society."

Part of the solution may be to seek to make a full recognition,
from each company or organization,
of all those libraries, frameworks, and other open-source software components
that we are using and depending on.
From there,
we could try to catalog those projects
and find out which of them really need support.
Depending on what we define and what our capabilities are,
we could start providing different types of resources.
We could even get involved in foundations oriented to support those projects
and cooperate with our partners who also require those projects.

As these words come out,
they sound so idealistic, so utopian.
Nadia rightly said that this problem is not easy to solve.
But collecting and sharing data and metrics on the current status
of many essential projects
and the impact they have on our economy
can nudge many of us who are willing to help.
However,
not everyone wants to contribute,
and perhaps it would mess things up even more
to seek to force them to do so.

As we have experienced firsthand,
or at least seen in social psychology experiments,
there are cases in which
there may be many people around a person
who is suffering and needs help,
and we all stand by and do nothing,
usually waiting for someone else to take the first step.
In this case,
we know that some of us have already intervened in one way or another.
Still,
**we need to make the problem and our contribution to its solution visible
to everyone**
so that we can help prevent tragedies in our digital infrastructure.
