---
slug: sastisfying-app-security/
title: Sastisfying App Security
date: 2019-09-29
category: development
subtitle: An introduction to SAST
tags: cybersecurity, security-testing
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1620331073/blog/sastisfying-app-security/cover_pbcyaf.webp
alt: Photo by Desola Lanre-Ologun on Unsplash
description: This blog post is an introduction to SAST. We share the concept, how SAST works, its types, history, and some of the benefits of implementing it in projects.
keywords: SAST, SDLC, Code, Automated Test, Manual Test, Vulnerabilities, Ethical Hacking, Pentesting
author: Kevin Cardona
writer: kzccardona
name: Kevin Cardona
about1: Systems Engineering undergrad student
about2: Enjoy life
source: https://unsplash.com/photos/kwzWjTnDPLk
---

SAST ([Static Application Security Testing](../../product/sast/))
is a type of white box test
in which a set of technologies is used
to analyze the source code,
byte code or the application binaries
in order to identify and reveal known security vulnerabilities
that can be exploited by malicious users.

## A bit of history

In his 1976 paper,
"Design and Code Inspections to Reduce Errors in Program Development",
Michael E. Fagan explained how to do a code review
and, thus, created the world's first [code review process](../secure-code-review/).
[Fagan inspection](https://en.wikipedia.org/wiki/Fagan_inspection)
is a formal execution process that involves several phases
and participants and detects defects in software development
by validating predefined entry and exit criteria.

<div class="imgblock">

![Fagan Flow via secjuice.com](https://res.cloudinary.com/fluid-attacks/image/upload/v1620331068/blog/sastisfying-app-security/fagan_jbxgtk.webp)

<div class="title">

Fagan Flow (image taken from[here](https://www.secjuice.com/sast-isnt-code-review-fight-me/))

</div>

</div>

In 1992, in his article “Experience with Fagan’s Inspection Method,”
E.P. Doolan proposed using software that would keep a database
of previously detected errors and automatically scan the code for them.
This is what gave rise to the use of automated code review tools.

## Software development Lifecycle (SDLC)

SDLC is a series of stages that must be followed for the development
of a specific software product. These stages ensure that the quality,
functionality, and objectives of the application meet customer
expectations and development standards.

<div class="imgblock">

![Software development Life Cycle phases via Synotive.com](https://res.cloudinary.com/fluid-attacks/image/upload/v1620331069/blog/sastisfying-app-security/sdlc_tgxhhl.webp)

<div class="title">

Software development lifecycle (image taken from)
[here](https://www.synotive.com/blog/wp-content/uploads/2017/02/software-development-life-cycle.jpg)

</div>

</div>

From the early stages of the SDLC,
it is important to use testing methodologies that quickly identify security
vulnerabilities in order to remediate them before the application’s release.
Multiple known vulnerabilities can be found on the following websites:

- [OWASP Top Ten
   Project](https://www.owasp.org/index.php/Category:OWASP_Top_Ten_Project)

- [CWE/SANS TOP 25 Most Dangerous Software
   Errors](https://www.sans.org/top25-software-errors/)

- [Common Weakness Enumeration](https://cwe.mitre.org/data/index.html)

<div class="imgblock">

![OWASP Top Ten Project via owasp.org](https://res.cloudinary.com/fluid-attacks/image/upload/v1620331070/blog/sastisfying-app-security/owasp_nks30b.webp)

<div class="title">

OWASP Top 10 from 2013 to 2017 (image taken from) [here](https://www.owasp.org/images/5/5e/OWASP-Top-10-2017-es.pdf)

</div>

</div>

By applying SAST,
we can detect and avoid most security vulnerabilities
listed on the above websites.

## How does SAST work?

SAST can be applied manually or through the use of automated tools.

**Manual testing** is done by a team of testers responsible for
reviewing the code for known security vulnerabilities. Once
vulnerabilities are found, they are reported to the development team to
be solved. Manual testing includes several stages:

1. **Synchronization:** This stage includes receiving the application
   from the developers and a complete explanation
   of what it does and how it does it.

2. **Review:** In this stage, the testing team takes the source code
   and analyzes each line, method, class, and file for security
   vulnerabilities.

3. **Reporting:** At this stage, false positives and irrelevant
   information are eliminated, and reports of findings are created and
   delivered to project leaders responsible for communicating with
   developers, who then mitigate or patch the vulnerabilities.

<div class="imgblock">

![Report of finding in manually test via Mitre.org](https://res.cloudinary.com/fluid-attacks/image/upload/v1620331068/blog/sastisfying-app-security/report_vsuvtz.webp)

<div class="title">

Example of a manual test finding report (image taken from)
[here](https://www.mitre.org/sites/default/files/publications/secure-code-review-report-sample.pdf)

</div>

</div>

Many [tools](https://www.owasp.org/index.php/Source_Code_Analysis_Tools)
allow us to perform **automated code analysis** and provide us with
reports of the vulnerabilities discovered during scanning.
These flexible tools can be integrated with different development environments,
including Waterfall, continuous integration/continuous deployment (CI/CD),
Agile/DevOps and repositories, and even with other testing tools.

<div>
<cta-banner
buttontxt="Read more"
link="/solutions/secure-code-review/"
title="Get started with Fluid Attacks' Secure Code Review solution right now"
/>
</div>

These types of tools use sophisticated functions such as data flow
analysis, control flow analysis, and pattern recognition to identify
potential security vulnerabilities. The result is that vulnerabilities
are reported sooner, especially in complex projects or projects with too
many lines of code.

<div class="imgblock">

![Report of findings in automated tests via Oreilly.com](https://res.cloudinary.com/fluid-attacks/image/upload/v1620331068/blog/sastisfying-app-security/toolreport_jjexvn.webp)

<div class="title">

Example of automated test finding report (image taken from)
[here](https://www.oreilly.com/library/view/industrial-internet-application/9781788298599/521ecdf9-f298-4e26-9b68-5baf6602094d.xhtml)

</div>

</div>

Reports should always be checked by experts because automated tools
tend to notify large numbers of false positives should be discarded
to know the actual risks of an application.

As
[Synopsys](https://www.synopsys.com/software-integrity/resources/knowledge-database/static-application-security-testing.html)
says "There are six simple steps needed to perform SAST efficiently in
organizations that have a very large number of applications built on
different languages, frameworks, and platforms."

</quote-box>

1. Choose an automated tool capable of performing source code reviews
   of apps written in the programming languages you employ.

2. Manage licensing requirements, set up access controls,
   and organize the infrastructure needed to deploy the tool.

3. Tailor the tool's scope, add or remove verification requirements
   according to your organization's needs,
   integrate the tool into your development environment
   and link it to a platform that allows you to track its results and reports.

4. Prioritize your applications according to their value
   and risks and perform scans on them with the tool.
   Continue these assessments throughout the evolution of your software.

5. Analyze the results obtained by the tool and discard false positives.
   Prioritize vulnerabilities according to the risk they represent
   and carry out their remediation.

6. Keep your teams trained on the proper use of the tool within your SDLC.

## Benefits

- SAST can be applied in the early stages of the SDLC,
  as it looks for vulnerabilities in the code before it is compiled.
  As long as there is remediation,
  this can help ensure that many security vulnerabilities
  do not accumulate in the application just before it is released.

- Remediating vulnerabilities identified with SAST
  from the early stages of the SDLC means lower costs in terms of time
  and money compared to late detection and remediation.

- SAST is flexible and can be adapted to any type of project.

- SAST can be fully integrated with CI/CD,
  Agile and DevOps environments ([DevSecOps](../../solutions/devsecops/)).

## Conclusions

- It is important to know the security vulnerabilities
  to which our applications are exposed.
  In order to do so,
  we must continuously read and inform ourselves
  via resources such as [OWASP](../../compliance/owasp/)
  or [CWE](../../compliance/cwe/).

- [Security testing](../../solutions/security-testing/)
  should always be performed on applications
  to ensure that
  they are able to maintain the confidentiality,
  integrity and availability of information.

- Perform continuous reviews of applications.
  Security tests should never be performed only once.

- Using SAST helps programmers learn or reinforce
  secure coding standards and practices.
