---
slug: protecting-data-financial-services/
title: Financial Industry Data Protection
date: 2024-05-28
subtitle: Data protection in the financial sector, tips and more
category: philosophy
tags: cybersecurity, company, compliance, trend, software
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1716503686/blog/protecting-data-financial-services/cover_data_protection.webp
alt: Photo by Towfiqu barbhuiya on Unsplash
description: Challenges today in data protection in the financial industry and measures to take, be it in software development or otherwise, to safeguard data.
keywords: Data Protection, Financial Institutions, Developing Software, Data Breaches, Compliance Data Protection, Sensitive Data, Financial Services Software Development, Ethical Hacking, Pentesting
author: Wendy Rodriguez
writer: wrodriguez
name: Wendy Rodriguez
about1: Content Writer and Editor
source: https://unsplash.com/photos/a-golden-padlock-sitting-on-top-of-a-keyboard-FnA5pAzqhMM
---

In the financial services industry, trust is essential.
Customers entrust your company with their private information
while hoping it will be handled carefully and securely.
And they’re right to expect that.
Most financial services have not only updated their systems
to handle modifications for data protection,
but also alter their procedures to guarantee ongoing security.

Data protection refers to the practices,
technologies and regulations that ensure the availability,
security and privacy of information.
Software developers who work in this field
are aware of the additional pressure they face.
They know they must handle the difficult tasks
of adhering to regulations and security best practices.
Not only should developers in your team consider the software
they build (that it’s useful and operable).
They should also consider the access it offers,
the data and resources it opens up to,
and the cyber criminals who might use it to further their criminal activities.

This blog tackles the data that needs to be protected,
challenges and measures to be considered
when creating financial service software.
But let's first understand the data you're trying to protect.

## Understanding data protection

Data protection refers
to the overall practices to safeguard personal information from damages,
corruption or loss.
It guarantees that users may easily access data through backup,
recovery and appropriate governance.
Financial institutions handle a wide range of sensitive data,
which must be considered when developing a software application.
Among the data types that your product must protect are the following:

- **Personal data:** This includes names, addresses, phone numbers,
  email addresses and  national identification numbers.

- **Financial data:** This includes account numbers, transaction history,
  balances, credit and debit card information.

- **Authentication data:** This includes usernames,
  passwords and other credentials used to access financial accounts
  like biometric information.

- **Internal data:** This includes employee data, internal emails and memos,
  intellectual property and budget spreadsheets and revenue projections.

- **System data:** This includes data about the software used
  by the platforms or the entity’s IT infrastructure.

### Why is data protection important?

Ensuring the security of financial data while developing software
should be one of the biggest priorities for developers.

Prioritizing data security is important because of different reasons.
Some have to do with **compliance**
(adhering to legal and regulatory requirements
is necessary in order to avoid penalties and legal action)
and **reducing risks**
(strong data protection minimizes the risks of cyberattacks like data breaches).

Other reasons involve **customer trust** (incidents erode it),
**operational continuity**
(prevention of loss of services and assurance that systems remain functional)
and **reduction of financial loss**
(direct loss from ransomware attacks
or indirect loss from downtime or recovering data).

## Challenges when developing software for financial services

When developing software for the financial sector,
a particular set of challenges arise,
and could go beyond the common software development issues.

One of those challenges
is **striking the right balance between security and usability**.
Overly-strict security protocols may frustrate users,
obstruct adoption and render the application useless.
The key is to implement strong security features in a way that is seamless,
making the application user-friendly and logical,
while adhering to the regulations.

Another challenge for developers
is the **integration with legacy systems**.
Financial institutions often rely on legacy systems
built with outdated technologies.
Integrating new software with these older systems can be complex and expensive.
Careful planning, data mitigation strategies,
using modern APIs, are all ways to bridge the gap
between legacy systems and new software components.

There's another challenge in
**staying up-to-date with the changing regulatory landscape**.
This could include new laws,
standards and industry guidelines that don’t interact with each other.
In turn,
not staying up-to-date may lead to vulnerabilities and perhaps hefty fines.
Developers should find an integrated system
with a single and comprehensive view of all regulated requirements,
to save them future headaches.

Financial institutions often
need to **integrate their software with third-party systems**,
like payment gateways or credit bureaus.
Developers should take their time and ensure the seamless
and secure integration with these external systems.

Of course, one of the biggest challenges
is **protecting user data** in itself.

### Challenges with data protection

Organizations face an intricate maze of issues related to data privacy,
particularly in the financial industry.
The following are key areas of difficulty:

- **Vast amount of data:** The increasing volume of data
  makes it difficult to track, manage and secure effectively.
  Financial institutions collect a vast amount of sensitive data,
  and organizing this data spread out across different systems
  increases the risk of exposure.

- **Managing the data lifecycle:** Data goes through various stages
  throughout its lifecycle –from creation and storage, to use and deletion.
  Specific data like financial or proprietary information
  requires specific compliance and other mandates,
  adding complexity to data lifecycle management.

- **Human factor:** Human error is a significant contributor
  to data breaches and accidental data leaks.
  Negligence by employees or lack of awareness can be just
  as damaging as malicious attacks.

- **Cloud security concerns:** As more financial institutions
  move towards cloud-based solutions, data security concerns arise.

- **Access controls:** Providing access and authorization
  for a hybrid workforce and third-party applications makes
  managing controls difficult since either could be hit by an attack.
  Stolen credentials or weak passwords also make breaches possible.

Financial software must be built with strong data security measures
in place to prevent unauthorized access, data breaches or identity theft.
These features will be covered later.

<div>
<cta-banner
buttontxt="Read more"
link="/solutions/vulnerability-management/"
title="Get started with Fluid Attacks' Vulnerability Management
solution right now"
/>
</div>

## Compliance with data security regulations and standards

For starters, **understanding the regulations and laws that apply**
to the specific organization is part
of creating strong software security.
The laws and regulations related to data protection that mostly affect
the financial sector are presented below.

- **General Data Protection Regulation (GDPR):** This
  comprehensive EU regulation focuses on online privacy
  and data management within EU Member States.
  We, at Fluid Attacks,
  have associated [specific security requirements](https://help.fluidattacks.com/portal/en/kb/articles/criteria-compliance-gdpr)
  with these regulations.

- **New York Department of Financial Services Cybersecurity Regulation:**
  This regulation requires aligning cybersecurity strategies with the
  [NIST](https://doi.org/10.6028/NIST.CSWP.04162018) framework.
  This includes deploying robust security infrastructure,
  having an updated system to detect security attacks,
  appointing a CISO (chief information security officer),
  and providing detailed reports on their cybersecurity risks,
  policies and procedures' effectiveness.
  From our end, [we set these requirements](https://help.fluidattacks.com/portal/en/kb/articles/criteria-compliance-nydfs)
  using this regulation.

- **Gramm-Leach-Bliley Act (GLBA):** This law mandates that
  financial institutions inform customers about data-sharing practices
  and offer them the choice to opt-out of sharing their data with third parties.
  GLBA aims to protect “nonpublic personal information” (NPA) of customers,
  which includes social security numbers and transaction histories.
  We established [these requirements](https://help.fluidattacks.com/portal/en/kb/articles/criteria-compliance-glba)
  based on this law.

- **Payment Card Industry Data Security Standard (PCI DSS):** These sets
  of security standards seek to safeguard cardholder data.
  It also sets rules on how merchants, service providers,
  financial institutions, developers and payment solutions process,
  store and transmit data from consumers.
  Its latest version, [PCI DSS v4.0](../pci-dss-v4-new-13-requirements/),
  updated the names of its 12 principal categories.
  These are the [software security requirements we have established](https://help.fluidattacks.com/portal/en/kb/articles/criteria-compliance-pci)
  using this standard.

Other regulatory compliance and data protection laws are:

- [**Sarbanes-Oxley Act (SOX)**](https://sarbanes-oxley-act.com/)

- [**Payment Services Directive (PSD2)**](https://eur-lex.europa.eu/eli/dir/2015/2366/oj)

- [**Basel III**](https://www.bis.org/publ/bcbs189.pdf)

- [**California Consumer Privacy Act (CCPA)**](https://help.fluidattacks.com/portal/en/kb/articles/criteria-compliance-ccpa/)

All of them focus on protecting sensitive information,
but need to be taken into account respectively.
Their compliance contributes to a reliable
and sustainable financial services sector
and helps them to successfully navigate
the complicated regulatory environment.

Knowing which regulation applies and understanding all their particulars
that apply to your company is of the utmost importance.
This also **helps you maintain a proactive position in case of audits**.
Speaking of which, **conducting regular internal and external audits**
can help to identify and address gaps in compliance
or data protection practices.

The development and consulting teams
should **establish a clear and unified set of guidelines for handling data**.
This ensures everyone involved knows exactly how data is
collected, stored, used, and if it complies with regulations.

**Using tools to automate compliance tasks and reporting** could also
streamline the process.

**Data security attorneys or consultants could be hired** to
analyze applicable laws, suggest actions and help stay compliant.

For the development team,
there are very specific requirements that are needed in the software.
We discuss some of them next.

## Data protection measures

The basis of the relationship between financial service institutions
and their clients is **trust**.
This confidence is built on entities'
capacity to keep private information out of the wrong hands.
A company's reputation and economic operations
may suffer significantly and permanently from inadequate data security.

Developers must adhere to strict regulations,
which control all aspects of their software development process,
including the tools they utilize.
They must prioritize robust security measures to protect sensitive data,
and could consider the following security measures.

### Technological controls for system data security

Several basic measures can be taken by developers
and other teams alike from the technological standpoint.
One of those is **disk encryption**,
which entails converting data on a storage device into code
rendering it unreadable to anyone that doesn’t have the decryption key.
Data is encrypted using algorithms.
An encryption key is used to encrypt and decrypt the data,
and the key can be protected with passwords, PINs or biometric signatures.
Disk encryption ensures sensitive data remains confidential
and protected from unauthorized access.
If it's a part of an incident response plan,
it can be included in the recovery strategy.

To take it a step further, endpoint security solutions
like [**Zero Trust Network Access**](../zero-trust-network-access/)
can be used on all devices
that access sensitive data.
ZTNA can significantly strengthen security postures
and protect data the way it's required by regulatory standards.

Yet another measure is to use a **DLP** (data loss prevention) solution
to detect and prevent customer data leakage
or inappropriate sharing, transfer, or use of sensitive data.

Moreover, **cloud services and cloud-based applications** can be used
for backup and recovery,
benefiting from their availability and scalability.

And, of course, a **secure infrastructure** is
always suggested across all critical resources.
For example,
PCI DSS focuses on different security aspects, like:

- Properly configured **firewalls**, which need to be maintained
  at least every six months and have a recorded set of usage rules.

- **Network segmentation**, which needs to be done logically.
  By dividing the network into zones with varying levels of security,
  seeking to isolate the cardholder data environment from unauthorized users.

- **Protecting all systems and applications** is an essential procedure
  that calls for processes and mechanisms
  (like deploying anti-malware solutions or performing periodic scans)
  to catch vulnerabilities, which must be timely patched.

### Data protection processes and practices

A very important aspect of data security
is **data backup and recovery**.
Scheduled backups should be done regularly to ensure data
can be restored in case of loss or corruption.
These backups should be stored in multiple locations,
including offsite or in cloud-based solutions.
Tests should be conducted in order to ensure recovery processes.

Another very important aspect is access control.
An access model can be more or less lax in authentication
and authorization requirements. A strict model is ZTNA.
Part of the [zero trust philosophy](../../learn/zero-trust-security/),
ZTNA is based on is granular access.
Granting users access to only the data they need to perform their jobs,
or **least privilege access control**,
minimizing the potential damages caused by compromised credentials.

Another way to protect data is by only collecting
and storing the data absolutely necessary for functionality,
which is called **data minimization**.

One more measure is to regularly identify
and patch vulnerabilities in developed code.
Through [**vulnerability management**](../../solutions/vulnerability-management/)
within the [DevSecOps](../../solutions/devsecops/) methodology,
developers can get to vulnerabilities early,
reducing risk exposure.
This process involves **regular security assessments**,
through [penetration testing](../penetration-testing/) and
[vulnerability scanning](../vulnerability-scan/),
and swift remediation efforts.

Other steps like **classifying data** based on sensitivity
to prioritize protection efforts or creating
a **comprehensive inventory** that maps all data collected
and used for transparency,
can be taken to address the technological challenges.

At a more general level, an extremely important practice is
to **create awareness among employees**.
Training them on data security best practices,
[phishing](../phishing/) scams,
[social engineering](../social-engineering/) tactics
and other dangers that could be triggered by human error
should be discussed with them.

Also another important process to share widely
within the organization is establishing
a documented **incident response** plan that outlines procedures for detecting,
responding to and recovering from data breaches or incidents.

Lastly,
PCI DSS has a [guide](https://listings.pcisecuritystandards.org/documents/Responding_to_a_Cardholder_Data_Breach.pdf)
on how to respond to a cardholder data breach
and stipulates how organizations should implement an incident response plan.
This **breach detection and reporting** guide provides information like:
limiting data exposure,
understanding who needs to be immediately informed in case of a breach,
and making sure third-party contracts have incident response plans.
It also covers in detail when assistance from
a Payment Card Industry Forensic Investigator (PFI) should be sought.

### Software requirements compliance management

As we’ve seen before,
there are requirements that financial institutions need to comply with.
Most laws and regulations require things like:
strong encryption, password policies, session management, access controls,
compliance requirements for privacy, keeping logs and auditing.
Let's discuss some of the key requirements
and understand how and what the developing team should
adhere to if they want to achieve compliance.
To keep it brief and consistent,
we will stick to PCI DSS requirements and advice.

- **Data encryption:** PCI DSS mandates that
  all cardholder data transmitted over public networks be encrypted,
  which protects the data in case of interception,
  and that cardholder data stored on databases or servers also be encrypted.
  The use of encryption algorithms with a minimum
  of 128 bits of effective key strength
  is absolutely necessary to comply with this mandate.
  Some examples of compliant algorithms include
  the popular AES and the public encryption key RSA.
  Some other closely related requirements to comply
  with PCI are: to document key management processes and procedures,
  test constantly and update protocols,
  keep cardholder data storage to a minimum,
  and to strictly monitor network activity.

- **Password requirements:** [PCI DSS 4.0](../pci-dss-best-practices-2024/)
  mandates a minimum password length of 12 characters.
  Eight characters if the system does not support 12.
  Passwords should be complex, incorporating a combination
  of lowercase and uppercase letters,
  numbers and special characters.
  This makes them more resistant to brute force attacks
  and password-cracking tools.
  PCI DSS also requires password resets every 90 days.
  The establishment of a certain number of failed login attempts and
  the use of multi-factor authentication for access are other requirements.
  Also, all default passwords for systems and applications
  need to be changed immediately upon first use.

- **Session management:** There are some requirements
  PCI DSS mandates about this topic.
  Session timeouts for inactive user sessions
  (typically after 15 minutes of inactivity),
  which then require the user to re-authenticate to re-activate
  the terminal or session.
  Others include unique session identifiers
  and monitoring for unauthorized session activity.
  Reviewing logs for suspicious behavior should also be done.

- **Access controls:** Access to data and systems should be granted
  on the principle of least privilege.
  This means that users should only have minimum access permissions
  based on their job classification and functions.
  These privileges need to be approved by authorized personnel
  and be reviewed at least once every six months.

## Fluid Attacks' contribution to securing financial services software

As demonstrated in the last section,
financial institutions’ defense against threats
doesn't encompass just one solution.
Your organization can benefit greatly from a multi-layered approach
in order to protect your financial assets,
which includes user data.
Speaking from experience,
we at Fluid Attacks know that vulnerabilities can be gateways
into networks and that cyber criminals have the means
to gain unauthorized access and reach important assets.
However,
we also know how to prevent cyber attacks that can lead
to [breaches](../top-10-data-breaches/),
[ransomware](../10-biggest-ransomware-attacks/) or any other dreadful incident.

We have worked with important financial services companies.
One of them is a well-known money transferring entity
that makes sending money around the world easy.
One of its main objectives is to keep its customers’
personal information protected as
it collects all kinds of sensitive information.
The firm has established rules on how that data is gathered,
stored, transmitted and used.
It also stays compliant
with different laws and regulations worldwide.
It was used to point-in-time security testing,
which are periodic assessments of the entity’s security stand.
However, this approach mostly leads to outdated reports
and it affected this firm by reporting vulnerabilities
only after the software had been made available to users.
The firm sought out a company that finds vulnerabilities throughout
the software development lifecycle (SDLC),
uses technologies and tools as well as ethical hackers
to identify potential vulnerabilities,
and believes in the DevSecOps methodology as indispensable
for software development.

Since 2021,
our Continuous Hacking solution has strengthened
the above company’s security posture
by identifying different types of vulnerabilities,
reporting them to its developing team,
providing insights and suggesting remediation strategies actionable
before deploying their software.
By the first quarter of 2023,
this firm had reduced its risk exposure by 75%.
Not only that,
but their teams were collaborating more thanks to our processes and platform,
both intended to foster teamwork in security matters.
They emphasized our proficiency, the usefulness of our evidence
of exploitability of detected vulnerabilities
and the vast assistance our pentesters offered.
You can find more about its success story [here](https://fluidattacks.docsend.com/view/j8rvapujmtbqiac6).

This financial entity is one of the many, many companies
our solutions have helped.
By keeping them up to date with their security status
with our [security testing](../../solutions/security-testing/) solution,
whose accuracy is made largely possible
by the [manual penetration testing](../../solutions/penetration-testing-as-a-service/)
done by our expert [pentesters](../../solutions/ethical-hacking/),
[these companies](../../clients/) know they can rely
on us for their vulnerability identification
without affecting their time-to-market.
We’re always to help, [contact us](../../contact-us/).
