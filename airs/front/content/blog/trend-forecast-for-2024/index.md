---
slug: trend-forecast-for-2024/
title: Trend Forecast for 2024
date: 2023-11-24
subtitle: Threat landscape and cybersecurity trends we anticipate
category: opinions
tags: cybersecurity, trend, company
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1700853956/blog/trend-forecast-for-2024/cover_forecast_2024.webp
alt: Photo by Jez Timms on Unsplash
description: Get an idea of the top threats to cybersecurity and preventive trends that we predict for 2024.
keywords: Gen Ai, Advanced Persistent Threats, Supply Chain Attacks, Software Supply Chain, Smart Devices, Cybersecurity Trend, Cyberattacks, Ransomware, Ethical Hacking, Pentesting
author: Jason Chavarría
writer: jchavarria
name: Jason Chavarría
about1: Content Writer and Editor
source: https://unsplash.com/photos/a-bunch-of-fireworks-that-are-in-the-dark-r4lM2v9M84Q
---

As is our custom,
we would like to open by referring to the global cost of cybercrime
expected for next year.
Cybersecurity Ventures' estimate for 2024 is [$9.5 trillion](https://cybersecurityventures.com/cybercrime-to-cost-the-world-9-trillion-annually-in-2024/).
Reasons for the rise in cost include the expansion of the attack surface,
the constant evolution of ransomware,
the increase of cryptocurrency-related scams
and the emergence of further geopolitical tensions.
Considering the breakdown of this record figure,
we will spend this New Year's knowing that,
each second,
organizations worldwide are losing $302,000 to cybercriminals.
Accordingly,
the cybersecurity market will continue to grow.
[Gartner](https://www.gartner.com/en/newsroom/press-releases/2023-09-28-gartner-forecasts-global-security-and-risk-management-spending-to-grow-14-percent-in-2024)
forecasts companies worldwide will spend a total of $215 billion in security
and risk management in 2024.
Of course,
it helps to be aware of the [current trends](../cybersecurity-trends-2023/)
and prepare for the trends to come.
In this blog post,
we present our brief forecast for 2024.

## Threat landscape forecast for 2024

<br />

### Cyberattacks enhanced by AI

We had mentioned as a [2023 trend](../cybersecurity-trends-2023/)
the way that generative artificial intelligence (gen AI) is helping criminals
create more convincing phishing messages.
Well,
this trend is expected to grow even stronger in 2024.
It is very likely
that the material used by cybercriminals,
including voice and video,
will appear more legit.
What's more,
gen AI "as a service" will probably rise.
That is,
there will be a market for gen AI tools in underground forums,
so that criminals can buy their use to carry out their campaigns.

### Advanced persistent threats targeting the supply chain and more

We're anticipating advanced persistent threats (APTs)
to be at the center of cybercriminal activities next year.
(If you're unfamiliar with the term,
it denotes adversaries with lots of expertise and resources
and who can therefore repeatedly and adaptively create opportunities
to achieve their objectives over an extended period of time.)
[Kaspersky](https://securelist.com/kaspersky-security-bulletin-apt-predictions-2024/111048/),
who have documented APTs' interest in smart devices,
predict
that the criminals will leverage systems of home cameras and cars
with vulnerabilities or misconfigured or outdated software
to expand their malicious surveillance.
As the firm has seen APTs' successful use
of very silent exploit delivery methods
it expects these adversaries to keep that practice:
sending exploits through messaging apps
which are activated without user interaction,
sending links that trigger attacks upon being opened
and hacking Wi-Fi networks.
Apart from smart devices,
threat actors may turn their attention
to the exploitation of managed file transfer systems,
since,
as was evidenced with the consequences of the [exploitation of MOVEit](https://www.emsisoft.com/en/blog/44123/unpacking-the-moveit-breach-statistics-and-analysis/),
the impacts would be compromising thousands of organizations.

We have referred to cyberwar as one [2023 trend](../cybersecurity-trends-2023/).
As conflicts around the world keep emerging,
so will the activity of state-sponsored threats grow in cyberspace.
This possibility should urge nations
to strengthen their protection of critical infrastructure systems,
as well as those of government and defense sectors.
Moreover,
the activity of hacktivists is expected to persist,
which involves mostly distributed denial-of-service (DDoS) attacks,
website defacement
and unauthorized access to data.

Yet another trend mentioned by Kaspersky
that we also anticipate
is APTs leveraging vulnerabilities in open-source software.
One notable modus operandi of theirs
that may become trendy
is purchasing supply chain attacks as a service.
Meaning,
they may get in the market access packages
that target various software vendors and IT service suppliers,
this way they can rapidly launch large-scale attacks.
A quite different clientele
(e.g., ill-intentioned and not too technically skilled people)
may probably find in the market next year
many more hacker-for-hire groups than today.

### Ransomware

Ransomware activities this end of year have declined a little,
given the dismantling of the [Ragnar Locker](https://www.secureworld.io/industry-news/authorities-take-down-ragnar-locker)
gang
and the [Qakbot botnet](https://therecord.media/qakbot-cybercrime-botnet-takedown-fbi)
prior to that.
Anyway,
what's normal is
that the individuals who had worked on dismantled gangs and botnets
find their way back to the stage.

[LockBit](https://therecord.media/ransomware-tracker-the-latest-figures),
who have been the most active actor for quite a while,
and other gangs keep the threat of ransomware very much alive.
A recent stunt by LockBit was to leverage a [widely exploited vulnerability](https://www.cisa.gov/news-events/cybersecurity-advisories/aa23-325a)
to bypass password requirements and multifactor authentication
in The Boeing Distribution Inc.'s systems.
After perceiving no ransom payment,
the gang published [45 GB of Boeing’s data](https://www.itbrew.com/stories/2023/11/17/after-boeing-declines-to-pay-up-ransomware-group-leaks-45-gb-of-data).
It won't be a surprise,
then,
to see a continuation of the exploitation of zero-day vulnerabilities
by ransomware gangs and affiliates
to deliver their malware.

As gangs keep demanding costly ransom sums,
it's predicted
that the victims of their attacks will have lost around [$265 billion annually](https://cybersecurityventures.com/global-ransomware-damage-costs-predicted-to-reach-250-billion-usd-by-2031/)
by 2031,
and every two seconds there will be a ransomware attack.

<div>
<cta-banner
buttontxt="Read more"
link="/solutions/ethical-hacking/"
title="Get started with Fluid Attacks' Ethical Hacking solution right now"
/>
</div>

## Trend forecast for the cybersecurity industry

<br />

### AI for cybersecurity

Regarding the use of artificial intelligence in the cybersecurity industry,
we expect to see next year the [continuation of the trend](../cybersecurity-trends-2023/)
of using AI for the remediation of vulnerabilities in software products.
That is,
to help devs achieve faster fix times,
security testing solutions will continue to offer AI-generated fixes.

As for promoting a more secure cyberspace
that can still embrace AI,
some regulations on its use are going to be in place in the following years.
An example is the use of the [Blueprint for an AI Bill of Rights](https://bidenwhitehouse.archives.gov/ostp/ai-bill-of-rights/)
framework
to guide organizations' policies, practices and design
to protect the American public.
Basically,
they should follow five principles,
which we could summarize as follows:

- Design and test automated systems thoughtfully,
  so that they are safe to use and effective.

- Take measures to prevent algorithmic discrimination.

- Include data privacy protections by default.

- Give notice to users that automated systems are in use
  and explain the role of automation in determining system outcomes.

- Where appropriate, provide human alternatives
  that users can turn to if the automated system fails
  or if the users want to contest its impacts on them.

### SEC rules and greater liability of CISOs

[We had talked last year](../sec-new-regulations/)
about the coming of new rules
by the U.S. Securities and Exchange Commission (SEC).
On December 18, 2023, the SEC rules will start to take effect.
In our words,
they require publicly traded companies do the following:

- Disclose cybersecurity incidents within four days
  with details on their nature, scope and timing
  and material, or likely material, impact on the company.

- Describe their strategies for cybersecurity risk detection and management
  and the material, or likely material, effects of risks
  from threats and previous incidents.

- Describe management's cybersecurity expertise
  in assessing and managing material risks from cybersecurity threats,
  as well as the oversight role of their board of directors.

We expect these new rules to shape how companies see cybersecurity
and react to cyber incidents.
As a matter of fact,
some recent events already have shown what awaits those who fail to comply.
We are talking about the SEC filing charges against
[SolarWinds and its chief information security officer](https://www.securityweek.com/sec-charges-solarwinds-and-its-ciso-with-fraud-and-cybersecurity-failures/),
and the prior sentencing of [Uber's chief security officer](https://www.justice.gov/usao-ndca/pr/former-chief-security-officer-uber-sentenced-three-years-probation-covering-data)
to three year's probation.
Both cases underline the aversive effects
of lying about the companies' state of cybersecurity.

Naturally,
this state of affairs,
capturing security leaders' legal liability
in the aftermath of cybersecurity incidents,
is and will keep creating a sense of urgency to disclose events
and a higher level of compromise in cybersecurity.
It has been advised that the C-suite and cybersecurity team work harder
to [speak the same language](https://www.csoonline.com/article/653983/companies-are-already-feeling-the-pressure-from-upcoming-us-sec-cyber-rules.html),
i.e., be actively involved in complying with the SEC rules.
Further,
the cybersecurity team should have a deeper relationship [with the legal team](https://www.securityweek.com/industry-reactions-to-sec-charging-solarwinds-and-its-ciso-feedback-friday/)
to work together in managing corporate risk,
compliance and regulatory functions.

### Software supply chain security

We had mentioned [software supply chain security](../software-supply-chain-security/)
(SSCS) as a [2023 trend](../cybersecurity-trends-2023/).
This one is not going away any time soon,
as supply chain attacks keep being successful
and more costly than attacks of other kinds.
No organization should turn a deaf ear to this threat.
[Gartner's prediction](https://www.gartner.com/en/newsroom/press-releases/2022-03-07-gartner-identifies-top-security-and-risk-management-trends-for-2022)
has been
that by 2025 almost half of organizations worldwide will have suffered
the consequences of such attacks.

Again,
we mention
that the SSCS approach means to go beyond generating SBOMs
and performing SCA scans.
Thoroughly securing the supply chains will need to involve
verifying the provenance of software components
and assessing the security policies of suppliers
and how well they comply with industry standards,
among other secure development practices.

### Cybersecurity labeling for consumers

2024 is the launch year of the [U.S. Cyber Trust Mark program](https://www.whitehouse.gov/briefing-room/statements-releases/2023/07/18/biden-harris-administration-announces-cybersecurity-labeling-program-for-smart-devices-to-protect-american-consumers/).
It is the creation of the White House
and the U.S. Federal Communications Commission (FCC)
that will certify and label Internet-enabled devices as secure.
This is an important step
to help reduce the cybersecurity risks
caused by Internet of things (IoT) devices,
which are the ones targeted by this program
and are estimated to be around [25 billion](https://docs.fcc.gov/public/attachments/DOC-395909A1.pdf)
by the start of the new decade.

The program's guidelines include providing regular software updates,
implementing strong and unique default passwords,
protecting data
and having incident detection capabilities.
Seeing the earned digital label in a device would help consumer choice,
ideally with them favoring those products that are safer
and less vulnerable to attacks.
It's noteworthy
that big manufacturers and retailers will be supporting
and committing to the program.
They include,
according to the White House,
"Amazon,
Best Buy,
Google,
LG Electronics U.S.A.,
Logitech,
and Samsung Electronics."
This trend will hopefully protect consumers
from suffering the consequences of the campaigns by APTs on smart devices,
which we referred to above.
We provide more information about this trend in a [dedicated blog post](../cybersecurity-labeling-for-iot/).

## Prepare for trends securing your software

You can see
that it's going to be a risky business
to go into 2024 without securing your software.
At Fluid Attacks,
we are aware of the trends to come and prepare for them,
developing our own security testing software
in accordance with them
and growing our team of pentesters.
We assess our clients' software continuously
and contribute to their remediation of security issues
both through AI-generated guides and our pentesters' advice.
Don't put it off any longer:
Start a [free trial](https://app.fluidattacks.com/SignUp)
of our automated security testing now.
Upgrade at any time of the trial to include assessments
by our [pentesters](../../solutions/ethical-hacking/).
