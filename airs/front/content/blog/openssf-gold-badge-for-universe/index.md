---
slug: openssf-gold-badge-for-universe/
title: Our New OpenSSF Gold Badge
date: 2023-10-12
subtitle: Now we follow all best practices required by OpenSSF
category: politics
tags: cybersecurity, software, code, compliance
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1697120855/blog/openssf-gold-badge-for-universe/cover_openssf_gold_badge.webp
alt: Photo by Jean-Daniel Calame on Unsplash
description: After evidencing statement and branch coverages above 90% and other high-level best practices, our open-source project Universe got the rare OpenSSF gold badge.
keywords: Openssf, Best Practices, Open Source Security, Openssf Best Practices Badge, Software, Vulnerabilities, Criteria, Ethical Hacking, Pentesting
author: Jason Chavarría
writer: jchavarria
name: Jason Chavarría
about1: Content Writer and Editor
source: https://unsplash.com/photos/vK8a67HU7To
---

At Fluid Attacks,
we work diligently to offer high-quality, open-source software products.
We are therefore permanently on the lookout
for development best practices we can implement
to keep on with our continuous improvement.
It's been a year since we satisfied the "passing" level criteria
of the set of best practices established by Open Source Security Foundation
(OpenSSF).
Since then,
we have implemented further enhancements
and, this month, we achieved the gold badge
as a reward for our compliance with all the required criteria of the project.
This is a rare achievement,
attained only by [27 out of more than 6,200](https://www.bestpractices.dev/en/projects?gteq=300)
software development projects
that evaluated their implementation of OpenSSF best practices
at the time of this writing.
[Two projects by Fluid Attacks](https://www.bestpractices.dev/en/users/21932)
—[Universe](https://gitlab.com/fluidattacks/universe)
and [Makes](https://github.com/fluidattacks/makes)—
are among those few.
This achievement for Universe comes
after we reached statement and branch coverages above 90%
in the source code of our [vulnerability scanner](../casa-approved-static-scanning/)
and [platform](../../platform/).

## What is Open Source Security Foundation?

[Open Source Security Foundation](https://openssf.org/) (OpenSSF)
is an organization
that aims to advance open-source security
by providing tools, services, infrastructure, training and resources
that enable participants in the open-source ecosystem’s knowledge
of secure development practices,
creation and distribution of security policies,
identification of vulnerabilities for prompt remediation,
and notification about quality, supportability, defects and mitigations.

This organization is a project of [Linux Foundation](https://www.linuxfoundation.org/),
which helps developers and companies
identify and contribute to important open-source projects.
OpenSSF, in turn,
draws together the most important open-source security initiatives
and the people and firms that support them.

## OpenSSF best practices

OpenSSF's [Best Practices for OSS Developers working group](https://github.com/ossf/wg-best-practices-os-developers)
raises awareness and fosters education on best practices for secure coding.
The group currently develops the OpenSSF Best Practices Badge project,
to which a web application ([BadgeApp](https://www.bestpractices.dev/en))
is associated.
On this application,
free/libre and open-source software (FLOSS) projects
can self-certify their level of compliance
according to the percentage of criteria they meet.
Some criteria are absolutely required,
whereas other,
although normally required,
admit valid reasons for being ignored.

In the next six lists,
we summarize the best practices that,
[according to the working group](https://www.bestpractices.dev/en/criteria_discussion),
"well-run FLOSS projects typically already follow,"
that is,
we think they mean,
before trying to implement practices motivated by OpenSSF's project.
Still,
they say,
"only about 10% of projects pursuing a badge achieve [this] passing level."

**Best practices categorized as "Basics":**

- Showing in the project's website a succinct description
  of what the software does,
  what it should be done to obtain or provide feedback on the project
  and contribute to it,
  and what is required for such contributions to be acceptable.

- Releasing the software product as FLOSS,
  ideally approved by the Open Source Initiative,
  and publishing the FLOSS license in the source repository.

- Providing a basic documentation of the software product
  and reference documentation of its external interface.
  Normally, this documentation would be available in English.

- Supporting HTTPS using TLS in the project's website(s).

- Having at least one searchable mechanism of discussion
  where messages and topics can be referenced with URLs.

- Maintaining the project
  by responding to significant problem and vulnerability reports.
  Normally, the project would accept reports in English.

**Best practices related to change control:**

- Having a version-controlled source repository publicly readable
  that has its own URL,
  allows traceability
  and includes versions between releases subject to peer-review.
  It is advised
  that a common distributed version control software such as Git be used.

- Having a unique version identifier for each release.

- Providing human-readable release notes
  that include all the major changes and CVE IDs of eliminated vulnerabilities.

**Best practices related to error and vulnerability reporting:**

- Providing users a process to submit bug reports,
  responding to a majority of those received in the last two to twelve months,
  and keeping an archive for reports and responses.
  Normally, projects would use issue trackers for individual issues
  and respond to more than half the requests for enhancements.

- Publishing the vulnerability reporting process on the project website
  and informing how it works for private reports (if possible).

- Taking a maximum of fourteen days to respond to vulnerability reports
  received in the last six months.

<div>
<cta-banner
buttontxt="Read more"
link="/solutions/security-testing/"
title="Get started with Fluid Attacks' Security Testing solution right now"
/>
</div>

**Best practices related to software quality:**

- Providing a working build system
  that can rebuild the software from source code,
  should it be necessary to build the software product for use.
  Normally, the tools for this would be FLOSS.

- Using at least one automated test suite that is FLOSS.
  It is advised that the tests cover most functionalities,
  branches
  and input fields
  (i.e., that most of them have a corresponding test
  verifying they do what is expected of them).
  Further,
  it is advised to implement continuous integration,
  running automated tests to check the new or changed code.

- Creating tests for major new functionalities
  and having a general policy enforcing it.

- Enabling a tool or feature to look for common mistakes in code
  and addressing any warnings generated.

**Best practices related to security:**

- Having at least one primary developer
  who is knowledgeable in secure software design,
  meaning he understands design principles
  such as [open design](https://en.wikipedia.org/wiki/Open-design_movement),
  [least privilege](https://help.fluidattacks.com/portal/en/kb/articles/criteria-requirements-186/),
  [input validation](https://help.fluidattacks.com/portal/en/kb/articles/criteria-requirements-342/)
  with allowlists,
  [fail-safe default](https://help.fluidattacks.com/portal/en/kb/articles/criteria-requirements-341/),
  [economy of mechanism](https://en.wikipedia.org/wiki/Saltzer_and_Schroeder%27s_design_principles),
  and other.

- Having at least one primary developer
  who is knowledgeable in common errors that lead to vulnerabilities
  like those exposing to [SQL injection](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-146/)
  and [cross-site scripting](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-010/)
  attacks.

- Using cryptographic functions based on [pre-existent](https://help.fluidattacks.com/portal/en/kb/articles/criteria-requirements-147/),
  tested,
  secure
  and approved mechanisms.
- [Transmitting data using secure protocols](https://help.fluidattacks.com/portal/en/kb/articles/criteria-requirements-181/)
  (e.g., HTTPS)
  to counter man-in-the-middle attacks.

- Having no vulnerabilities of medium or higher severity unpatched
  and known publicly for over 60 days.
  Projects are advised to eliminate critical vulnerabilities
  found in their product ASAP.

- Leaking no valid private credentials intended to limit user access.

**Best practices related to [static application security testing](../../product/sast/)
(SAST)
and [dynamic application security testing](../../product/dast/) (DAST):**

- Testing any proposed major production release with a SAST tool
  before it's available to end users
  and remediating all found vulnerabilities
  of medium or higher severity quickly (as required above).
  It is advised
  that SAST be conducted "**[on every commit or at least daily](https://www.bestpractices.dev/en/criteria/0)**."

- Remediating quickly (see above) all vulnerabilities
  of the aforementioned severity levels found through a DAST tool.
  It is suggested
  that DAST be conducted before releasing major changes.
  Special emphasis is made in [detecting memory safety problems](../switch-memory-safe-language-rust/)
  with DAST
  if the project includes code in memory-unsafe languages.

## Fluid Attacks' recent OpenSSF gold badge

In August, 2022,
our Universe repository [got a “passing” OpenSSF badge](https://www.bestpractices.dev/en/projects/6313)
for meeting all the criteria summarized above.
([We had certified Makes](https://www.bestpractices.dev/en/projects/5703)
with this kind of badge earlier that year,
in March.)
We worked further to implement many more best practices
ranked in a higher level by OpenSSF's project.
For some time,
Universe held a "silver" badge.

We want to highlight some best practices at the silver level
for their extreme relevance related to project oversight and software quality.
The ones in the former category include

- defining and documenting a clear [project governance model](https://dev.fluidattacks.com/getting-started/governance/),
  where the roles and responsibilities
  and the structure for decision-making are spelled out,
  and

- adopting a publicly viewable [code of conduct](https://gitlab.com/fluidattacks/universe/-/blob/trunk/CODE_OF_CONDUCT.md).

Regarding software quality,
we highlight [best practices like](https://www.bestpractices.dev/en/projects/6313?criteria_level=1#quality)
listing external dependencies
and monitoring third-party components and vulnerabilities found in them.

The last achievements we documented
to gain the OpenSSF gold badge for Universe
corresponded to the statement and branch coverages in two of our [components](https://dev.fluidattacks.com/components/).
Statement coverage refers to the completeness of a test suite
evaluating units that state the actions to be taken with the data
on which a program is to operate.
Branch coverage is the completeness of a test suite
evaluating decision points' (`if` and `case` statements') true and false cases.
Evidencing both types of coverage being above 90%
(in the case of branch coverage, 10% higher than required),
we finally satisfied the pending criteria to get our gold badge this month.
We are very proud of this achievement and will keep working to uphold it.

You might be interested
in using our high-quality and accurate [vulnerability scanner](../../product/)
to conduct SAST, DAST, software composition analysis,
and cloud security posture management
in search of security issues present in your software.
Then,
you can manage those vulnerabilities
with the help of our high-quality [platform](../../platform/).
[Try it all for free for 21 days](https://app.fluidattacks.com/SignUp)!
