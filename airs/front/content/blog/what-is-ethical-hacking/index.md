---
slug: what-is-ethical-hacking/
title: What Is Ethical Hacking?
date: 2022-04-11
subtitle: Key concepts, how it works and why it is important
category: philosophy
tags: cybersecurity, hacking, pentesting, red-team, security-testing, vulnerability
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1649710011/blog/what-is-ethical-hacking/cover_ethical_hacking.webp
alt: Photo by Bruno Kelzer on Unsplash
description: Learn what ethical hacking is and some related key concepts, how ethical hackers work, and what are the benefits of this assessment methodology.
keywords: Definition Ethical Hacking, Ethical Hacking Phases, Ethical Hacking Meaning, White Hat Evaluation, White Hacker Meaning, Black Hat Hacker, Ethical Hacker Definition, Target Of Evaluation In Ethical Hacking, Pentesting
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Content Writer and Editor
source: https://unsplash.com/photos/J1sd2zy87rc
---

As you may know,
Fluid Attacks is a company
that specializes in [ethical hacking](../../solutions/ethical-hacking/).
We are a big [red team](../../solutions/red-teaming/),
an offensive security team
with the mission of detecting security vulnerabilities in [IT systems](../../systems/).
As we recently realized that
we didn't have an informative, introductory blog post
about what ethical hacking is,
we decided to create it.
This text is aimed mainly at those who are new to the subject
and want to get an introduction.
It is partially based on a recent workshop given by our VP of Hacking,
[Andres Roldan](https://co.linkedin.com/in/andres-roldan),
to a group of journalists.

## Key concepts of ethical hacking

### What is cybersecurity?

[It is said that](https://datareportal.com/global-digital-overview)
almost five billion people currently use the Internet,
which corresponds to nearly 63% of the world's population.
Moreover,
around 92% of these users,
at some point,
virtually from anywhere,
have access to the network through mobile devices.
We are undoubtedly in a highly interconnected digital world
where,
as in the "tangible" reality,
menaces exist [from the outset](../first-cyberattack/).
In the face of constant threats,
**cybersecurity** has become necessary.
[Gartner](https://www.gartner.com/en/information-technology/glossary/cybersecurity),
partially right,
defines this term
as "the combination of people,
policies, processes and technologies
employed by an enterprise
to protect its cyber assets."
(I said "partially"
because it is also true that as an individual user,
you can access cybersecurity).
But what should cyber assets be protected against?
—Cyberattacks.

### What are cyberattacks?

[These are assaults](https://www.checkpoint.com/cyber-hub/cyber-security/what-is-cyber-attack/)
carried out by **cybercriminals**
who attack one or more IT systems
from one or more computers.
Cyberattacks can disable victims' systems,
steal their data
or use them as launching points for other assaults.
According to an [IBM security report](https://www.ibm.com/security/data-breach/threat-intelligence/),
the top cyberattack types (tactics) last year
included the following:
[ransomware](../ransomware/),
unauthorized server access,
[business email compromise](../cost-cybercrime-ii/),
data theft and credential harvesting.
And among the most frequently used techniques
to achieve these objectives
were the following:
[phishing](../phishing/),
vulnerability exploitation,
stolen credentials,
[brute force](../pass-cracking/) and remote desktop.

### What is hacking?

Cyberattacks can be seen as a part of hacking,
which is mainly the process of identifying and exploiting security issues
in systems
to gain unauthorized access to them.
This activity is carried out by hackers,
who are usually divided into two types or groups.

### What are the types of hackers?

Many cybercriminals
who execute the assaults are so-called **malicious hackers**,
threat actors or **black hat hackers**.
Among their primary motivations is the idea of obtaining some financial reward.
They may also attack just to express their disagreement
with the decisions of governments or companies.
There are also attacks resulting from the mere desire of hackers to take risks
and achieve recognition in certain groups of people.
Sometimes,
cybercriminals are even hired by dishonest firms to spoil projects
and affect the reputation of their rivals.
Something similar happens among governments
(e.g., [the Russia-Ukraine cyberwar](../timeline-new-cyberwar/)).
(If you want to know more about how hackers think,
[visit this blog post](../thinking-like-hacker/).)
Regardless,
in a universe where we can experience lots of counter-stimuli,
it is to be expected that
there are **white hats** if there are black hats.
Namely,
if there are malicious hackers,
there are also **ethical hackers**.

### What is an ethical hacker?

An ethical hacker or white hat hacker is an offensive cybersecurity expert
who executes cyberattacks mainly in favor of the security of others.
Should you still have the question
"How are ethical hackers different from malicious hackers?"
on your mind,
we invite you to read our post ["Hacking Ethics"](../hacking-ethics/).
There,
we explain how both groups may share qualities such as curiosity,
cleverness, and patience,
but they are vastly different
when it comes to their purposes
and the effects or consequences of their actions.

### How to become an ethical hacker?

[We already have a blog post](../how-to-become-an-ethical-hacker/)
in which we answer this question.
There,
we tell you about the technical and non-technical skills
you should possess to be an ethical hacker
and about the [certifications](../../certifications/)
you should get to improve your knowledge and recognition.

## Definition of ethical hacking

**Ethical hacking is perhaps the best way
to respond to malicious hacking**.
Ethical hackers examine and attack systems to find out,
and sometimes exploit,
their vulnerabilities
by copying threat actors' tactics,
techniques and procedures.
The big difference is that
the attack is carried out with the system owner's consent,
who will be responsible for remediating the reported security vulnerabilities.
Ethical hacking is thus a methodology for assessing IT systems
in which the tests are intended to simulate
as closely as possible
the attack methods of cybercriminals,
but not to do harm,
but instead to enable organizations to overcome their weaknesses
and optimize their protection of operations and assets.

<div>
<cta-banner
buttontxt="Read more"
link="/solutions/ethical-hacking/"
title="Get started with Fluid Attacks' Ethical Hacking solution right now"
/>
</div>

In ethical hacking,
experts must keep up to date
on the existence and use of hacking tools,
as well as on the attack trends used by adversaries.
In their reports,
ethical hackers provide information about identified vulnerabilities,
including how critical they are.
They do this by following public frameworks
such as [CVE](../../compliance/cve/)
and [CVSS](https://www.first.org/cvss/).
They also provide evidence of the exploitation of vulnerabilities
and which information assets can be compromised in an attack.
Beyond finding known vulnerabilities,
ethical hackers can also conduct research
to discover and record zero-day vulnerabilities,
i.e., previously unknown threats.

## Ethical hacking vs. penetration testing

Ethical hacking is a rather broad concept that,
while often used interchangeably with terms
such as "[pentesting](../penetration-testing/)"
and "red teaming,"
it might be more accurate to say
it encompasses both methodologies.
Let's clear this up.

We have said time and again that
penetration testing is a type of [security testing](../security-testing-fundamentals/)
in which human intervention is essential.
(Even though many providers claim
they offer such "automated pentesting.")
People trained to do penetration testing,
i.e., to understand the infrastructure and functionality
of different IT systems,
recognize and detect their security vulnerabilities,
and, if necessary, take advantage of them
to provide tangible signs of potential impacts,
can be identified as hackers.
Therefore,
an ethical hacker doing pentesting is doing ethical hacking.

What is sometimes suggested by some people
as a difference between ethical hacking and penetration testing
is the breadth of their scope of testing.
It's said that pentesting is more oriented
to evaluate with predefined methods specific systems
to verify they comply with security standards requirements
for certain assets,
while ethical hacking aims at a broader coverage,
and its methods may not be predefined.
(This is similar
to [the comparison we once made](../red-team-exercise/)
between red teaming and pentesting).
However,
both procedures are tasks performed by ethical hackers.
So,
going back to the initial statement,
both can be seen as ethical hacking.
On balance,
pentesting is a form of ethical hacking.

## Types of ethical hacking

Then,
regarding the previous section,
it is curious to find on the Internet that
some companies or media tell us that
the types of ethical hacking vary according to the systems
they intend to evaluate
(e.g., web apps, networks, IoT).
Isn't this what should be done instead
to separate the types of pentesting
(procedure targeting specific systems)?
In fact,
that was something Chavarría already did
[in another of our blog posts](../types-of-penetration-testing/).
(If you want to understand the difference between white,
gray and black pentesting,
[read this post](../what-is-manual-penetration-testing/).)
Here,
we can say that
among types of ethical hacking,
we could include penetration testing and red teaming,
but the intention is not to muddle the waters.

## 5 phases of ethical hacking

For the ethical hacking process to happen,
the systems' owner must previously define and approve an attack surface
and a target of evaluation
(i.e., part or all of the attack surface).
The targets can be [web](../../systems/web-apps/)
or [mobile apps](../../systems/mobile-apps/),
[APIs and microservices](../../systems/apis/),
[thick clients](../../systems/thick-clients/),
[cloud infrastructure](../../systems/cloud-infrastructure/),
[networks and hosts](../../systems/networks-and-hosts/),
[IoT devices](../../systems/iot/)
and operational technology.
The commonly used ethical hacking methodology
can be divided into reconnaissance,
enumeration, analysis, exploitation and reporting phases.

1. **Reconnaissance phase:**
   In the **passive reconnaissance phase**,
   ethical hackers collect information from external sources
   without interacting directly with the target.
   They employ,
   for example,
   [Open Source Intelligence](../social-engineering/)
   (i.e., publicly available information)
   collection techniques.
   They can resort to common web search engines
   such as Google and Bing
   to discover relevant details about the target.
   Due to the characteristics of this phase,
   there is little chance of hackers being detected.

   In the **active reconnaissance phase**,
   the ethical hackers already have direct contact with the target.
   They identify sources of information
   and technology belonging to the organization
   that owns the system under evaluation.
   They interact with the organization's services,
   systems and even personnel
   to collect data and define attack vectors.
   The chances of hackers being discovered increase considerably
   if we compare this phase with the previous one.

2. **Enumeration phase:**
   In this phase,
   ethical hackers set out to sketch the target's security state
   and prepare for the attack.
   They identify its strengths and weaknesses
   and begin envisioning the possible impacts
   that may result from the assault.
   According to the particular characteristics of the target,
   hackers prepare a special arsenal for it.

3. **Analysis phase:**
   In this phase,
   ethical hackers are responsible
   for determining the exact impact
   of attacking each of the vulnerabilities they have identified.
   They evaluate each scenario and attack vector,
   as well as the difficulties of exploitation.
   They take into account the damage to the integrity,
   confidentiality and availability of the target in each case.
   In addition,
   the hackers examine the potential impact
   on systems close to the target.

4. **Exploitation phase:**
   According to Roldan,
   it's this phase
   where ethical hacking differs
   from the operation of automated security testing tools.
   The tool is limited to identifying vulnerabilities,
   while the ethical hacker means to exploit them
   to reach high-value objectives
   within their target of evaluation.
   In this way,
   they can identify the real effects
   that a cybercriminal could achieve
   by exploiting these vulnerabilities.

5. **Reporting phase:**
   After the exploitation is completed,
   ethical hackers have to present the findings to all stakeholders.
   One of the hackers' deliverables is an executive summary,
   thanks to which the managers of the organization
   that owns the target
   can easily understand the identified risks.
   From this report,
   they can manage processes for risk mitigation.
   Another deliverable is a technical summary
   so that developers
   or other professionals
   can understand each vulnerability in detail
   and proceed with remediation.

## Ethical hacking tools

In [our main blog post about red teaming](../red-team-exercise/),
we had already provided a list of 23 tools used by hackers
for the different phases of that form of evaluation,
which,
as we said,
can be seen as a type of ethical hacking.
Since you can access that list,
in this case,
we want to complement it with some other tools
used by our ethical hackers
in our [Continuous Hacking](../../services/continuous-hacking/)
[Advanced plan](../../plans/).

- **Amass:**
  [Amass](https://github.com/owasp-amass/amass)
  is an [OWASP project](https://owasp.org/www-project-amass/)
  for performing network mapping of attack surfaces
  and finding external assets.
- **Apktool:**
  [Apktool](https://apktool.org/)
  is an [open-source tool](https://github.com/ibotpeaches/apktool/)
  for performing [reverse engineering](../reverse-engineering/)
  on APK (Android Package Kit) files.
- **bettercap:**
  [bettercap](https://www.bettercap.org/)
  is an [open-source tool](https://github.com/bettercap/bettercap)
  for performing reconnaissance and attacking WiFi and Ethernet networks,
  wireless HIDs (human interface devices)
  and BLE (Bluetooth Low Energy) devices.
- **Ciphey:**
  [Ciphey](https://github.com/Ciphey/Ciphey)
  is an automated open-source tool for decrypting encryptions
  (without knowing the keys),
  decoding encodings, and cracking hashes.
- **Covenant:**
  [Covenant](https://github.com/cobbr/Covenant)
  is a collaborative .NET command and control framework
  for highlighting attack surfaces
  and facilitating the use of offensive tradecraft.
- **DNSRecon:**
  [DNSRecon](https://github.com/darkoperator/dnsrecon)
  is an open-source tool for performing reconnaissance on domains.
- **Frida:**
  [Frida](https://frida.re/)
  is an open-source dynamic instrumentation toolkit
  often used to understand mobile applications' internal behavior
  and network communications.
- **Fuff:**
  [Fuff](https://github.com/ffuf/ffuf)
  ("fuzz faster u fool") is an open-source tool written in Go for fuzzing
  (i.e., automatically sending random data to an app
  to find errors or unexpected behavior).
- **Ghidra:**
  [Ghidra](https://ghidra-sre.org/)
  is an [open-source](https://github.com/NationalSecurityAgency/ghidra)
  software reverse engineering framework
  developed by the National Security Agency Research Directorate.
- **Gitleaks:**
  [Gitleaks](https://gitleaks.io/)
  is an [open-source SAST tool](https://github.com/gitleaks/gitleaks)
  for scanning Git repositories, files, and directories
  and detecting hardcoded secrets.
- **Hopper:**
  [Hopper](https://www.hopperapp.com/)
  is a reverse engineering tool for Mac and Linux
  for disassembling, decompiling and debugging applications.
- **HTTP Toolkit:**
  [HTTP Toolkit](https://httptoolkit.com/)
  is an [open-source tool](https://github.com/httptoolkit/httptoolkit)
  for intercepting, inspecting, and rewriting HTTP(S) traffic.
- **Hydra:**
  [Hydra](https://github.com/vanhauser-thc/thc-hydra)
  is an open-source parallelized login cracker
  ([password brute-forcing tool](https://www.techtarget.com/searchsecurity/tutorial/How-to-use-the-Hydra-password-cracking-tool))
  that supports multiple attack protocols.
- **Interactsh:**
  [Interactsh](https://app.interactsh.com/#/)
  is an [open-source tool](https://github.com/projectdiscovery/interactsh)
  for detecting out-of-band interactions
  or vulnerabilities that cause external interactions.
- **JMeter:**
  [Apache JMeter](https://jmeter.apache.org/)
  is an [open-source tool](https://github.com/apache/jmeter)
  for performing load testing
  and analyzing and measuring the performance of applications and services.
- **John the Ripper:**
  [John the Ripper](https://www.openwall.com/john/)
  is an [open-source tool](https://github.com/openwall/john)
  for [password cracking](../pass-cracking/)
  that supports multiple hash and cipher types.
- **MobSF:**
  [MobSF](https://mobsf.github.io/Mobile-Security-Framework-MobSF/)
  (Mobile Security Framework)
  is an automated [open-source tool](https://github.com/MobSF/Mobile-Security-Framework-MobSF)
  for performing static and dynamic analysis of mobile apps.
- **Pacu:**
  [Pacu](https://rhinosecuritylabs.com/aws/pacu-open-source-aws-exploitation-framework/)
  is an [open-source exploitation framework](https://github.com/RhinoSecurityLabs/pacu)
  for testing the security of Amazon Web Services (AWS) environments.
- **ZAP:**
  [ZAP](https://www.zaproxy.org/)
  (Zed Attack Proxy) is an [open-source tool](https://github.com/zaproxy/zaproxy)
  for automatically detecting security vulnerabilities in web applications.

## The importance of ethical hacking

Although the importance of ethical hacking may already be evident to you
from what we have discussed so far,
we can make it crystal clear
by showing some specific benefits
this security testing methodology can bring to your organization.

### Benefits of ethical hacking

- The effort of ethical hackers allows a significant approach to reality.
  They accurately simulate what black hat hackers or threat actors can do
  against your systems
  (even using techniques such as social engineering).
  This involves the detailed discovery of different weaknesses,
  vulnerabilities or entry points
  that cybercriminals could exploit,
  and that must be remediated as soon as possible.

- Ethical hackers not only reveal the location of the security issue
  but can also provide you with precise evidence
  of how it could be exploited
  and what impact it would have on your operations,
  assets or sensitive information.

- Ethical hacking often has a broader vulnerability detection scope
  than [vulnerability scanning](../vulnerability-scan/).
  Hackers can analyze business logic and correlate vulnerabilities
  to identify complex and critical security issues.

- Ethical hackers can also contribute with their cybersecurity expertise
  to review and verify reports from automated tools,
  which can often deliver false positives.

- When ethical hacking is a continuous activity
  that parallels your organization's software development,
  your teams can receive timely reports
  that can help improve vulnerability remediation rates,
  reduce costs,
  and deploy secure products without delays to end users.

### What are some limitations of ethical hacking?

The most notable limitation of ethical hacking is speed.
Hackers,
no matter how experienced,
do not work at the pace of an automated machine or scanner.
While security testing
supported only by automated tools
is not ethical hacking,
a comprehensive approach to ethical hacking,
such as the one we offer at Fluid Attacks,
can involve the joint intervention of humans and scanners.
This allows you to take advantage of the quickness of technology
in detecting known vulnerabilities
and the cleverness and accuracy of hackers
in identifying more complex, risky and even previously unknown security issues.

### For whom is ethical hacking recommended?

Financial institutions are the ones
that hire the services of ethical hacking companies the most,
mainly due to regulations that require it.
However,
it's recommended that any organization with a presence on the Internet
or developing digital products
test the security of their systems with ethical hacking,
meaning to prevent successful cyberattacks against them from happening.

Follow [this link](../../solutions/ethical-hacking/)
if you and your company are interested in knowing details
about Fluid Attacks' Ethical Hacking solution.
But if what you'd like is to wear a white hat
and be an ethical hacker on our [red team](../red-team-exercise/),
follow [this one](../../careers/).
For more details on each case,
don't hesitate to [contact us](../../contact-us/).
