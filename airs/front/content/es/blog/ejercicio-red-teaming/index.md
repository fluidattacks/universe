---
slug: blog/ejercicio-red-teaming/
title: Red Teaming
date: 2019-09-18
subtitle: ¿Qué es, cómo funciona y cuáles son sus beneficios?
category: filosofía
tags: ciberseguridad, red-team, pruebas de seguridad, pentesting, ingeniería social, credenciales
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1665159428/blog/red-team-exercise/cover_red_team_exercise.webp
alt: Foto por Daniel Cooke en Unsplash
description: Tras leer este artículo, comprenderás qué es un red team en ciberseguridad, cómo funciona y qué beneficios puede aportar a tu empresa.
keywords: Seguridad Redteam, Definicion De Ciberseguridad Red Team, Seguridad Red Team, Red Team Kill Chain, Hacking Red Team, Ejercicios Red Team, Ejercicio Red Team Blue Team, Hacking Etico, Pentesting
author: Anderson Taguada
writer: anders2d
name: Anderson Taguada
about1: Software Engineering undergrad student
source: https://unsplash.com/photos/gi8FUu-XXjU
---

¿Cómo podemos conformarnos con capacidades defensivas de ciberseguridad
que son solo teóricas? Ya no tenemos que esperar
a ser bombardeados por ciberdelincuentes para darnos cuenta
de la eficacia de nuestros detectores de amenazas,
*firewalls* y demás estrategias defensivas y controles de seguridad.
Ya no tenemos que esperar a que los *hackers* maliciosos
detecten vulnerabilidades en nuestros sistemas para poder actuar sobre ellas.
Aunque parezca contraintuitivo,
en el campo de la ciberseguridad podemos solicitar
el servicio de ser atacados.
Eso sí, estos ataques se realizan de forma ética
y regulada para averiguar qué tan fuertes somos.
Podemos contratar a profesionales que nos muestren vívidamente
lo que un atacante podría hacer contra nosotros
y lo bien que nosotros y nuestros sistemas
responderíamos a esas amenazas reales.
En este artículo hablaremos de ***red teams*** y ***red teaming***
para que puedas entender mejor esta solución.

## ¿Qué es un *red team*?

No nos referimos aquí al Liverpool,
al Bayern de Múnich o a algún equipo similar.
Nos enfocamos en un equipo rojo
o [*red team* en ciberseguridad](../../soluciones/red-team/).
Este *cyber red team* es un grupo de expertos en seguridad
y *hackers* profesionales, dentro o fuera de la empresa objetivo,
con las habilidades requeridas y, por supuesto,
el permiso para simular ciberataques del “mundo real”.
En estos ataques o actividades de *red team*,
desempeñando roles específicos,
sus miembros deben emular tácticas,
técnicas y procedimientos utilizados por los atacantes maliciosos
de hoy en día que ponen en peligro los sistemas
y las ciber-defensas de una organización.
Las pruebas de los *red teams* evalúan
la eficacia de las estrategias de prevención,
detección y respuesta a las amenazas de la empresa.
Proporciona informes sobre las vulnerabilidades detectadas
y los impactos de explotación como retroalimentación
para la remediación de vulnerabilidades
y la mejora de las ciber-defensas de la organización.

Normalmente,
cuando hablamos de ***red teams***,
los ***blue teams*** (y a veces los ***purple teams***)
aparecen en la conversación.

## *Red team vs. blue team vs. purple team*

Un ***red team*** está formado por especialistas en seguridad ofensiva
([*hackers* éticos](../que-es-hacking-etico/))
cuya misión, como ya se ha mencionado,
es impactar y comprometer un sistema de información y sus defensas.
El tamaño de este equipo es variable
y a veces depende de la complejidad de las tareas.

### Conjunto de habilidades del *red team*

Se espera que los miembros de este equipo
tengan amplios conocimientos técnicos,
creatividad y astucia para acceder a los sistemas
y moverse entre ellos sin ser detectados.
Lo ideal es que sus habilidades y experiencia sean variadas.
Algunos pueden ser más competentes, por ejemplo,
en desarrollo de *software*, pruebas de penetración
(también conocidas como *pentesting*), ingeniería social,
conocimientos empresariales, administración de TI,
inteligencia sobre amenazas, o controles
y herramientas de seguridad.

<div class="imgblock">

![Red team skills](https://res.cloudinary.com/fluid-attacks/image/upload/v1693526970/blog/red-team-exercise/skills_red_team_exercise.webp)

<div class="title">

Habilidades del *red team*.
([Imagen](https://miro.medium.com/v2/resize:fit:1400/0*h1bxXqkRpVBuM9TF)
tomada de [medium.com](https://medium.com/@redteamwrangler/how-do-i-prepare-to-join-a-red-team-d74ffb5fdbe6))

</div>

</div>

### Cómo convertirse en miembro de un *red team*

Convertirse en miembro de un *red team* es algo complejo
que requiere educación, experiencia,
desarrollo de habilidades y una forma de pensar particular.
Primero es necesario construir una base sólida en ciberseguridad,
ya sea mediante la educación formal o una formación autodidacta intensa.
Las certificaciones relacionadas
con el *red teaming* afianzan los conocimientos.
Se puede adquirir experiencia práctica a través de laboratorios,
programas de *bug bounty* o formando parte de un *blue team*.
Relacionarse con profesionales del sector proporciona conexiones
y conocimientos de un valor incalculable.
Las aptitudes interpersonales,
como la comunicación y el pensamiento crítico,
son fundamentales a la hora de trabajar en equipo
y comunicar los hallazgos.
Hay una responsabilidad ética inherente al trabajo en *red teaming*,
la cual debe mantenerse y ser recordada siempre.
Acogerse al aprendizaje continuo y explorar nuevas áreas,
evolucionando con el panorama cambiante de la ciberseguridad.
Si estás interesado, te recomendamos
nuestra [serie de artículos de la Tribu de hackers](../tribu-de-hackers-1/),
basada en el libro del exitoso autor (y hacker) Marcus J. Carey.

### ¿Qué es un *blue team*?

Un equipo azul o *blue team* está formado por especialistas en seguridad defensiva,
incluidos, por ejemplo, consultores de respuesta a incidentes.
Estos deben guiar a la organización para evaluar su entorno y organizar,
implantar y mejorar los controles de seguridad para identificar
y detener o afrontar intrusiones del *red team* o de atacantes reales.
Su trabajo defensivo busca prevenir daños a la estructura
y las operaciones de los sistemas de la organización.
En algunos casos, el *blue team* puede intervenir en la planificación
o implementación de medidas de recuperación.
Los miembros de este equipo deben comprender
perfectamente las estrategias de seguridad,
tanto a nivel tecnológico como humano.
Deben estar altamente cualificados en la detección de amenazas,
en la respuesta adecuada a las mismas
y en el uso correcto de las herramientas que apoyan estos propósitos.

### ¿Qué es un *purple team*?

Por último, [el equipo púrpura o *purple team*](../../../blog/purple-team/).
Desde la infancia sabes que mezclando rojo y azul se obtiene el púrpura.
Pues bien, en ciberseguridad, un ***purple team***
está formado por expertos en seguridad ofensiva y defensiva.
A veces,
este equipo es referido como un intermediario, responsable de evaluar,
coordinar y facilitar las interacciones entre los *red* y *blue teams*.
Otras veces, se hace referencia a él como miembros de ambos equipos
que colaboran trabajando al unísono.
La intención es maximizar las contribuciones de ambos grupos
de expertos en favor de la postura de ciberseguridad
de una organización.

A pesar de todo, por lo general basta con hablar de *red* y *blue teams*.
Ambos pueden tener como fuente de conocimiento de las tácticas,
técnicas y procedimientos de los actores maliciosos
el [ATT&CK Framework de MITRE](https://attack.mitre.org/),
el cual es producto de experiencias del mundo real.
Normalmente,
los *red* y *blue teams* tienen un rol protagónico
en la práctica del ***red teaming***. Sin embargo,
la presencia de los equipos azules puede no ser indispensable.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/red-team/"
title="Inicia ahora con la solución de red teaming de Fluid Attacks"
/>
</div>

## ¿Qué es *red teaming*?

Conforme a lo anterior,
para definir *red teaming* nos referimos al arte ofensivo
con el que podemos ayudar a una compañía a conocer
cuán seguros son sus sistemas.
Al parecer,
se trata de una práctica que se originó en el contexto militar.
Bajo un enfoque de confrontación, la idea
consistía en enfrentar a dos equipos simulando
la realidad para evaluar la calidad y la fuerza de sus estrategias.
(Algo así ocurre en los [concursos CTF](../../../blog/top-10-ctf-contests/)
estilo *Attack-Defense* para grupos de *hackers*).
Así surgieron el *red team* y el *blue team*.
En un ambiente controlado,
un ataque del *red team* pone a prueba las capacidades
y estrategias de prevención, detección
y respuesta a amenazas de una organización
—factores en los que un *blue team* puede estar involucrado con planes,
sistemas y estándares.

Muchas veces,
el *blue team* o los miembros del equipo de seguridad
de la empresa en evaluación pueden no ser conscientes
de que se están produciendo ataques simulados o *red teaming*.
(De hecho, esto es lo ideal; véase "[Attacking Without Announcing](../../../blog/attacking-without-announcing/).")
Como comentamos en otro artículo
sobre las [pruebas TIBER-EU](../../../blog/tiber-eu-framework/),
también puede intervenir un equipo blanco o *white team*,
cercano al *blue team*.
Un *white team* es un pequeño grupo de la organización
que puede ser el único consciente del *red teaming*.
Este equipo se encarga de aprobar y solicitar el inicio
y la interrupción de los ataques.
También actúa como enlace entre los otros dos grupos.
Siempre se espera que, tras los ejercicios de *red teaming*,
la organización objetivo madure su seguridad perfeccionando
sus controles y remediando sus vulnerabilidades.
Por cierto,
es misión de la empresa asegurarse de que lo que reciba
sea *red teaming* y no solo ***pentesting***.

## *Pentesting vs. red teaming*

Tanto *red teaming* como *pentesting* son enfoques de seguridad ofensivos.
En ambos casos, se simulan ciberataques del "mundo real"
en condiciones controladas para evaluar la seguridad del sistema.
Entre las diferencias que suelen mencionarse está
que las pruebas de penetración o *pentesting* actúan
con un alcance más reducido.
Por ejemplo, se evalúan aplicaciones en desarrollo
o componentes individuales pertenecientes a una red.
Por otro lado, el *red teaming* está más orientado
a probar un entorno empresarial completo en producción
con todos sus sistemas implicados simultáneamente.
Asimismo, puede que no se centre en encontrar
e informar de todas las vulnerabilidades
como hacen las pruebas de penetración.
El *red team* puede concentrarse
en alcanzar objetivos específicos de infiltración e impacto.
Además, algunos afirman que el *pentesting*
puede ser realizado por una sola persona,
mientras que en el *red teaming* deben participar varias.

Ambos enfoques pueden integrarse.
Un proveedor puede aplicar inicialmente varios ciclos
de pruebas de penetración dentro del [modelo DevSecOps](../concepto-devsecops/)
de una empresa.
Basándose en lo que muestran los informes,
la empresa puede remediar las vulnerabilidades en sus sistemas.
Después de estos ciclos,
una organización más madura en su seguridad puede abrir paso al *red teaming*.
Entonces, con objetivos específicos como, por ejemplo,
acceder a un elemento concreto de la red o a información sensible,
se puede comprobar hasta qué punto han sido eficaces las remediaciones
y qué brechas de seguridad existen
en los sistemas ante determinados tipos de ataques.

El *red teaming* puede considerarse
una prueba de penetración más profunda, holística y compleja.
Por ello,
el *red teaming* puede llevar más tiempo que el *pentesting*.
Un *red team* tiene que ser sigiloso y evasivo.
A menudo se dice que en *red teaming*,
en comparación con las pruebas de penetración,
prevalece el factor sorpresa
(es decir, el *blue team* desconoce la simulación del ataque).
Sin embargo,
esto dependerá de lo que se defina entre el proveedor y el cliente.
Además,
el *red teaming* amplía el abanico de tipos de ataque a utilizar,
añadiendo, por ejemplo, la penetración física y
la [ingeniería social](../../../blog/social-engineering/)
(p. ej., [*phishing*](../../../blog/phishing/)).
Esto significa que no solo se ponen a prueba los sistemas,
sino también el personal que los gestiona.
Por eso esta técnica puede aportar más y mejores ideas que el *pentesting*
para mejorar los planes de prevención,
detección y respuesta a las amenazas.

## ¿Cuáles son las metas más comunes de un *red team*?

En su trabajo,
los *red teams* pueden establecer diferentes metas desde el principio.
Aquí resaltamos las siguientes:

- Atacar sistemas en ambientes de producción.
- Escalar privilegios y tomar control administrativo de sistemas críticos.
- Acceder a archivos del sistema e información sensible,
  e incluso extraerla y modificarla.
- No afectar la disponibilidad de ningún servicio
  para clientes o usuarios.
  Sin embargo, pueden demostrar que se puede hacer.
- No ser detectados en ninguna fase del ataque.
- Inyectar troyanos personalizados.
- Instalar "puertas traseras" para accesos futuros
  y determinar lo fácil que es darse cuenta de su existencia.
- Maximizar el nivel de penetración e impacto.

## ¿Cómo funciona el *red teaming*?

Como hemos señalado antes,
y [como dijo](../../../blog/attacking-without-announcing/) alguna vez
[Rafael Alvarez](https://co.linkedin.com/in/jralvarezc),
CTO de Fluid Attacks,
lo ideal es que el *blue team* no sepa de la ejecución del *red teaming*.
En concreto,
que el *blue team* conozca la hora
y el lugar en el que el *red team*
pretende atacar puede considerarse inapropiado.
Según sugiere Álvarez,
para conocer con certeza el nivel de seguridad de tu empresa,
estos ejercicios deben ser lo más parecidos posible a la realidad.
Los pocos miembros de la organización,
es decir, sus máximas autoridades, que estarán al tanto del *red teaming*,
son los que darán al *red team* autorización expresa
y amparo legal para su **ejercicio ofensivo de seguridad**.

### ¿Cuáles son las tácticas habituales del *red team*?

La clasificación de las tácticas de *red teaming* por lo general
se puede realizar en función de los objetivos de la evaluación:

- ***Pentesting* de aplicaciones**: El *red team* evalúa tanto aplicaciones web
  como móviles para identificar y explotar vulnerabilidades
  derivadas de errores de diseño, desarrollo y configuración,
  tales como las relacionadas con subversión de accesos,
  inyección inesperada y abuso de funcionalidades.
- ***Pentesting* de redes**: Los expertos buscan errores de configuración
  y otras debilidades en la seguridad de la red que sirvan
  como puntos de acceso a la misma y a los sistemas conectados.
  A veces, incluso instalan las llamadas "puertas traseras"
  para seguir accediendo a la red, instalar *malware* en algunas
  de sus unidades y realizar "*sniffing* de red" para vigilar
  su tráfico de datos, interceptar comunicaciones y mapear el entorno.
- **Ingeniería social**: En este caso, el objetivo de los *hackers* éticos
  son los miembros de una organización.
  El *red team* pretende explotar las debilidades humanas para persuadir
  y manipular a estas personas a través de tácticas como *phishing*,
  [*smishing*](../../../blog/smishing/)
  e incluso soborno para obtener acceso a los sistemas
  y a la información confidencial de la organización.
- **Pruebas de seguridad física**: Más allá
  de los ataques a entornos virtuales,
  el *red team* puede poner a prueba los controles
  de seguridad físicos de una organización.
  En este caso, los puntos débiles podrían estar en controles
  como guardias de seguridad, cámaras, cerraduras, alarmas, etc.,
  los cuales podrían permitir el acceso a oficinas
  o zonas restringidas con sistemas críticos e información confidencial.

### ¿Qué implica un ejercicio de *red team*?

Un *red teaming* o ejercicio de *red team* debe comenzar
con el planteamiento de metas claras, como las descritas anteriormente.
Una vez definidas,
el *red team* inicia un reconocimiento pasivo y activo del objetivo.
En este paso, el equipo recopila toda la información que puede
y cree necesaria sobre el contexto, la infraestructura, los equipos,
las operaciones, las personas implicadas y mucho más.
Este proceso lleva al *red team* a adquirir y desarrollar
una serie de herramientas de *red teaming*,
incluyendo herramientas de evaluación, descifrado de contraseñas,
ingeniería social y explotación, que se adaptan a las metas
y a la empresa objetivo de ataque.
El equipo también lleva a cabo la *weaponization* o preparación del armamento,
en la que, por ejemplo, tiene lugar la creación de *payloads*
o trozos de código maliciosos.

A continuación,
el *red team* evalúa la superficie de ataque
para identificar vulnerabilidades y puntos de entrada,
incluidas las debilidades humanas.
Luego, define las vulnerabilidades a explotar.
Mediante ingeniería social, explotación de una vulnerabilidad
de *software* o cualquier otro vector de ataque,
el *red team* obtiene acceso al objetivo y ejecuta el *payload*
o la carga maliciosa en él.
Esto permite al equipo comprometer los componentes del sistema
y eludir los controles de seguridad.
Los atacantes siempre toman medidas para evitar que el *blue team*
se percate de la intrusión. Si este último lo consigue,
los primeros se ven obligados a luchar contra cualquier esfuerzo de contención.

Posteriormente,
el *red team* intenta moverse dentro del objetivo,
teniendo en cuenta las metas fijadas inicialmente.
Si es necesario, el equipo busca nuevas vulnerabilidades que explotar
para escalar privilegios y seguir moviéndose a través de los sistemas.
Una vez alcanzados los propósitos del ejercicio,
el equipo pasa a la fase de elaboración de informes y análisis.
Los expertos informan sobre lo que han conseguido,
las principales vulnerabilidades identificadas
y el rendimiento de los controles de seguridad
y del *blue team* (si hubo participación de este).
Los propietarios de los sistemas evaluados
pueden entonces recibir apoyo para la remediación de vulnerabilidades
y la mejora de sus procedimientos para prevenir,
detectar y responder a los tipos de ataques implicados en el *red teaming*.

## Escenarios de ejercicios de *red team*

Cuando le preguntamos a [Andres Roldan](https://co.linkedin.com/in/andres-roldan),
vicepresidente de *hacking* de Fluid Attacks,
por la metodología de *red teaming* que utilizan
en los ejercicios de *hacking* que llevan a cabo desde la empresa,
nos dijo que siguen la "estrategia *kill chain*".

### *Kill chain*

"*Kill chain*" (o cadena de exterminio)
es un término militar para describir los pasos de un ataque.
Uno de sus modelos es el "F2T2EA",
el cual incluye las siguientes etapas:

1. **Encontrar:** Identificar un objetivo mediante vigilancia
   o recopilación de información secreta.
2. **Asegurar:** Obtener la ubicación específica del objetivo.
3. **Rastrear:** Vigilar al objetivo hasta que se decida
   si se va a atacar o no.
4. **Apuntar:** Escoger las armas adecuadas
   para utilizar contra el objetivo y generar los impactos previstos.
5. **Atacar:** Utilizar las armas contra el objetivo.
6. **Evaluar:** Examinar los efectos del ataque,
   incluyendo la información secreta obtenida en el lugar.

<div class="imgblock">

![F2T2EA](https://res.cloudinary.com/fluid-attacks/image/upload/v1693529493/blog/red-team-exercise/F2T2EA_red_team_exercise.webp)

<div class="title">

F2T2EA *kill chain*.
([Imagen](http://1.bp.blogspot.com/--IWKt1g_8qg/UvTu_2JK6gI/AAAAAAAAAAo/jRp6euZzmGk/s1600/F2T2EA+1.jpg)
tomada de [myarick.blogspot.com](http://myarick.blogspot.com/2014/02/f2t2ea.html))

</div>

</div>

### *Cyber kill chain*

La empresa estadounidense aeroespacial y de seguridad Lockheed Martin
y su equipo de incidentes desarrollaron
este marco para prevenir ciberataques.
A continuación se detallan las fases que ellos identificaron
siguen los adversarios para lograr sus objetivos de ataque:

1. **Reconocimiento:** Informarse sobre el objetivo
   empleando múltiples técnicas.
2. **Preparación del armamento:** Combinar el vector de ataque
   con un *payload* o carga maliciosa.
3. **Entrega:** Transmitir la carga a través
   de un vector de comunicaciones.
4. **Explotación:** Aprovechar vulnerabilidades de *software*
   o humanas para ejecutar la carga.
5. **Instalación:** La carga establece la persistencia
   de un *host* individual.
6. **Comando y control (C2):** El *malware* comunica y proporciona control
   a los atacantes.
7. **Acciones sobre objetivos:** Los atacantes roban datos
   o hacen cualquier cosa que tengan entre sus metas finales.

<div class="imgblock">

![Cyber kill chain](https://res.cloudinary.com/fluid-attacks/image/upload/v1693529982/blog/red-team-exercise/cyber_kill_chain_red_team_exercise.webp)

<div class="title">

*Cyber kill chain*.
([Imagen](https://www.lockheedmartin.com/content/dam/lockheed-martin/rms/photo/cyber/THE-CYBER-KILL-CHAIN-body.png.pc-adaptive.1280.medium.png)
tomada de [lockheedmartin.com](https://www.lockheedmartin.com/en-us/capabilities/cyber/cyber-kill-chain.html))

</div>

</div>

### *Cyber kill chain* 3.0

El *cyber kill chain* 3.0
es una actualización (con pequeños cambios)
del modelo anterior para una mejor defensa que
fue [sugerido por Corey Nachreiner](https://www.helpnetsecurity.com/2015/02/10/kill-chain-30-update-the-cyber-kill-chain-for-better-defense/),
CTO de WatchGuard.
Este *kill chain* de *red team* tiene las siguientes etapas:

1. **Reconocimiento**
2. **Entrega**
3. **Explotación**
4. **Infección**
5. **Comando y control - Movimiento lateral y *pivoting***
6. **Objetivo/Exfiltración**

Los anteriores escenarios de *red teaming*
no son las únicas estrategias posibles.
Ten en cuenta que tú y tus compañeros también pueden desarrollar,
poner a prueba, modificar y aplicar su propia estrategia de *red team*.

### Ejemplo de ejercicio de *red team*

Mira este video de FOX 9 sobre un ejercicio de *red team*:

<div style="text-align: center;">
<iframe
  width="560"
  height="315"
  src="https://www.youtube.com/embed/YIV0xvatX0M"
  frameborder="0"
  allowfullscreen></iframe>
</div>

## ¿Cuáles son herramientas de *red teaming*?

En relación con esta pregunta, también podrías cuestionarte:
“¿Qué buscar en una herramienta de *red team*?”
Las herramientas para estos ejercicios de evaluación de seguridad
deberían ser útiles para aplicar tácticas como las mencionadas anteriormente.
Las herramientas de los *red teams* pueden clasificarse
según **la fase del *red teaming*** en la que cumplen funciones específicas.
Las siguientes son solo algunos ejemplos:

### Reconocimiento pasivo

- **Wireshark:**
  [Wireshark](https://www.wireshark.org/) es una herramienta
  de código abierto para analizar el tráfico de red en tiempo real,
  interceptarlo y detectar problemas de seguridad en las redes.
- **OSINT Framework:**
  [OSINT Framework](https://osintframework.com/) es una compilación
  de herramientas OSINT filtradas por categorías para facilitar
  la recopilación de información secreta.
- **Maltego:**
  Maltego es una herramienta para la extracción,
  recopilación y correlación de datos en tiempo real
  (p. ej., nombres, números de teléfono, cuentas,
  direcciones de correo electrónico).

### Reconocimiento activo

- **Nmap:**
  [Nmap](https://nmap.org/)
  ("Network Mapper") es una herramienta de código abierto
  para el descubrimiento e inventario de redes
  (p. ej., identificación de puertos abiertos) y auditoría de seguridad.
- **sqlmap:**
  [sqlmap](https://sqlmap.org/)
  es una herramienta de código abierto para detectar
  y probar vulnerabilidades de inyección SQL
  y tomar el control de servidores de bases de datos.
- **OpenVAS:**
  Open Vulnerability Assessment Scanner ([OpenVAS](https://www.openvas.org/))
  es una herramienta de código abierto
  para el escaneo y gestión de vulnerabilidades.

### Preparación de armamento - Lanzamiento - Explotación

- **SET:**
  El Social-Engineer Toolkit ([SET](https://github.com/trustedsec/social-engineer-toolkit))
  es un marco de *pentesting* de código abierto para desarrollar
  y lanzar ataques de ingeniería social.
- **Gophish:**
  [Gophish](https://getgophish.com/)
  es un marco de *phishing* de código abierto para diseñar,
  programar y lanzar campañas de *phishing*
  y hacer seguimiento de sus resultados.
- **Metasploit:**
  [Metasploit](https://www.metasploit.com/)
  es un marco de *pentesting* de código abierto
  para identificar vulnerabilidades
  y desarrollar y ejecutar *exploits*.
- **BeEF:**
  El Browser Exploitation Framework ([BeEF](https://beefproject.com/))
  es una herramienta de pruebas de penetración
  para utilizar vectores de ataque del lado del cliente
  y explotar vulnerabilidades de navegadores *web*.
- **Veil:**
  [Veil Framework](https://www.veil-framework.com/)
  es una colección de herramientas,
  entre las cuales la más utilizada es Veil-Evasion,
  diseñada para eludir los productos antivirus más comunes.
- **hashcat:**
  [hashcat](https://hashcat.net/hashcat/)
  es un [descifrador de contraseñas](../../../blog/pass-cracking/)
  de código abierto para aplicar métodos como la fuerza bruta
  y los ataques de diccionario.

### Escalar privilegios

- **BloodHound:**
  [BloodHound](https://github.com/BloodHoundAD/BloodHound)
  es una herramienta para mapear y visualizar la infraestructura
  y las relaciones ocultas dentro de los entornos Active Directory
  o Azure e identificar rutas de ataque complejas.
- **PowerUp:**
  [PowerUp](https://github.com/PowerShellMafia/PowerSploit/tree/master/Privesc)
  es una herramienta de PowerShell para
  comprobar las configuraciones erróneas comunes de Windows
  y ejecutar ataques de privilegio de Windows para escalar privilegios
  en sistemas que ejecutan ese sistema operativo.
- **BeRoot:**
  [BeRoot Project](https://github.com/AlessandroZ/BeRoot)
  es una herramienta post-explotación para
  comprobar errores de configuración comunes y proporcionar información
  sobre posibles formas de escalar privilegios.

### Movimiento lateral

- **CrackMapExec:**
  [CrackMapExec](https://github.com/Porchetta-Industries/CrackMapExec)
  (CME) es una herramienta de *pentesting* para evaluar
  y explotar vulnerabilidades en entornos Active Directory
  y realizar movimientos laterales en redes de Windows.
- **mimikatz:**
  [mimikatz](https://github.com/gentilkiwi/mimikatz)
  es una herramienta de código abierto para jugar con la seguridad de Windows,
  extrayendo credenciales, realizando ataques *pass-the-hash*
  y *pass-the-ticket*, y construyendo *golden tickets*.
- **LaZagne:**
  [LaZagne](https://github.com/AlessandroZ/LaZagne)
  es una herramienta de código abierto
  para recuperar contraseñas de diferentes aplicaciones almacenadas
  en computadores locales y ayudar al movimiento lateral.

### Comando y control

- **Cobalt Strike:**
  [Cobalt Strike](https://www.cobaltstrike.com/)
  es una herramienta post-explotación para emular actores silenciosos
  a largo plazo infiltrados en redes.
  Su *payload* distintivo, Beacon, permite a los *hackers* obtener
  y mantener el control de los sistemas comprometidos.
- **Empire Project:**
  [Empire](https://github.com/EmpireProject/Empire)
  es un marco post-explotación de PowerShell para gestionar
  y mantener el acceso a sistemas comprometidos,
  evadiendo las soluciones de seguridad.

### Objetivo - Exfiltración

- **DNSExfiltrator:**
  [DNSExfiltrator](https://github.com/Arno0x/DNSExfiltrator)
  es una herramienta para copiar y transferir
  (es decir, “exfiltrar”)
  datos a través de un canal encubierto de petición DNS.
- **CloakifyFactory:**
  [Cloakify](https://github.com/TryCatchHCF/Cloakify)
  es un conjunto de herramientas,
  incluyendo aquellas para exfiltrar datos a plena vista
  y eludir la detección por parte de alertas de red.
- **Powershell-RAT:**
  [RAT](https://github.com/Viralmaniar/Powershell-RAT)
  es una herramienta que actúa como puerta trasera en máquinas
  de Windows para exfiltrar datos como archivos adjuntos
  de correo electrónico a través de Gmail,
  evitando la detección por parte de *software* antivirus estándar.

## La importancia del *red teaming*

Puede que todavía te preguntes:
¿Por qué necesito ciberseguridad de *red teams*?
Aunque ya tenemos un [artículo en el blog](../../../blog/why-apply-red-teaming/)
donde justificamos ampliamente el valor
o la importancia del *red teaming* para la ciberseguridad
de las organizaciones, podemos resumirlo de la siguiente manera:
**Una mayor aproximación a la realidad**.
Una simulación de ciberataques por parte de un *red team*
pone a prueba el programa de ciberseguridad de una organización,
atendiendo a sus estrategias de prevención, detección, defensa y respuesta.
Estrategias que la organización tiene preparadas
para hacer frente a los ciberdelincuentes día a día y que,
con la evaluación del *red team*,
pueden mostrar debilidades que deberían ser tratadas cuanto antes.

Veamos algunos beneficios concretos del *red teaming*.

### Beneficios del *red teaming*

En términos generales,
el *red teaming* proporciona un panorama amplio
del estado de seguridad de tu organización
para una consecuente maduración.
Algunos de los beneficios específicos del *red teaming*
que hemos decidido destacar son los siguientes:

- Poner a prueba los controles de prevención
  (p. ej., *firewalls*, controles de acceso)
  y de detección (p. ej., SOC, AntiX, XDR).
- Identificar errores de configuración y vulnerabilidades de seguridad
  en la tecnología que se desarrolla y utiliza,
  así como debilidades en el comportamiento
  de las personas dentro de la organización.
- En relación con el beneficio anterior,
  aumentar la concientización del equipo sobre cómo el factor humano
  puede poner en riesgo la seguridad de los activos
  y las operaciones de la organización.
- Dar a conocer vectores de ataque y cómo los atacantes maliciosos
  pueden moverse a través del objetivo
  si no se resuelven las debilidades.
- Aumentar las tasas de remediación de vulnerabilidades.
- Reconocer que aplicar medidas defensivas
  sin probarlas no es suficientemente fiable.
- Reforzar las estrategias y procedimientos de detección
  y respuesta a los ciberataques.

## ¿Cuándo deberías utilizar un *red team*?

Cuando tu organización no descarta o descuida las medidas básicas
y avanzadas de prevención en ciberseguridad.
Cuando tu organización haya madurado lo suficiente
en seguridad como para haber ido más allá de la evaluación
de vulnerabilidades mediante herramientas
automatizadas o *pentesting* esporádico.
Y finalmente, cuando todos ustedes conozcan suficientemente
acerca del panorama de amenazas de su organización
y de las medidas preventivas y defensivas que pueden implementar
para mejorar su seguridad y la de sus clientes o usuarios

## Lista de control para un ejercicio de *red team* exitoso

La siguiente lista de control,
que puede modificarse según las necesidades de la organización,
es un ejemplo de lo que es importante contemplar cuando
se intenta ejecutar con éxito un ejercicio de *red team*.
Se debería tener en cuenta únicamente si la organización
se ajusta a la descripción del último párrafo.

<image-block>

!["Lista de control para un ejercicio de red team exitoso"](https://res.cloudinary.com/fluid-attacks/image/upload/v1709080158/airs/es/blogs/im%C3%A1genes%20traducidas/ejercicio-red-teaming/Lista_de_control_-_red_teaming4.webp)

Lista de control para un ejercicio de *red team* exitoso

</image-block>

## Red teaming de Fluid Attacks

En Fluid Attacks somos expertos
en [pruebas de seguridad](../../soluciones/pruebas-seguridad/).
Nuestro *red team* puede realizar servicios
de [*pentesting*](../../soluciones/pruebas-penetracion-servicio/),
[*red teaming*](../../soluciones/red-team/)
y [*hacking* ético](../../soluciones/hacking-etico/) para tu organización
siempre que ambas partes estén de acuerdo en realizarlos.
De acuerdo con lo descrito anteriormente,
recomendamos comenzar con *pentesting* para continuar
con *red teaming* a medida que madura la seguridad
de tu organización y sus sistemas.
Un *red team* ajeno a tu entorno de trabajo,
no afectado por conflictos de intereses,
realizaría evaluaciones sin inhibiciones absurdas
y entregaría informes transparentes
sobre tus medidas de prevención, detección y respuesta.
Nuestros *pentesters* cuentan
con [certificaciones](../../../blog/certificates-comparison-i/)
como OSCP, CEH, CRTP, CRTE, CRTO, eWPTv1, y eCPTXv2,
[entre muchas otras](../../../certifications/).
También tienen mucha experiencia,
cuentan con diversos perfiles y habilidades,
y trabajan en diferentes roles de *red team*.
En promedio, ofrecemos más de 15 *pentesters* o *hackers* éticos por proyecto.

[Contáctanos](../../contactanos/)
y permite que nuestro *hacking* de equipo rojo te diga con certeza,
con pruebas sustanciales,
qué estragos podrían causar los ciberdelincuentes
en los sistemas de tu organización.

<caution-box>

El artículo original del blog fue ampliado y actualizado
por [Felipe Ruiz](../../../blog/authors/felipe-ruiz/)
en **septiembre del 2023**.

</caution-box>
