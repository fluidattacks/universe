---
slug: blog/6-items-principales-politica-ia-gen/
title: Bases para tu política de uso de IA generativa
date: 2024-01-05
subtitle: 6 aspectos principales en una política de IA para desarrollar software
category: opiniones
tags: ciberseguridad, cumplimiento, tendencia
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1704478373/blog/6-main-items-gen-ai-usage-policy/cover_gen_ai_policy.webp
alt: Foto por Wynand Uys en Unsplash
description: Tu compañía debería tener una política sobre el uso de IA generativa para el desarrollo de software. Lee nuestras sugerencias sobre los puntos más importantes que debes incluir.
keywords: Ia Generativa, Herramientas Ia Generativa, Politica Ia Generativa, Codigo Generado Por Ia Generativa, Politica De Uso De Ia Generativa, Copilot, Chatgpt, Hacking Etico, Pentesting
author: Jason Chavarría
writer: jchavarria
name: Jason Chavarría
about1: Escritor y editor
source: https://unsplash.com/photos/six-dolphins-at-calm-water-0_32AIxy1Hk
---

Los ítems principales en una política empresarial
sobre el uso de IA generativa para el desarrollo de *software*
incluyen la identificación de los casos de uso de IA generativa en tu empresa,
el establecimiento de las herramientas de IA generativa
que los desarrolladores están autorizados a usar
y la solución de pruebas de seguridad
para mantener seguro el código generado con IA,
la descripción de los datos que no deben usarse en las herramientas de IA,
el requisito de que los proveedores notifiquen
y describan su uso de IA generativa,
y la descripción de las estrategias de comunicación
y aprendizaje de la política de IA.
Como mencionamos en nuestro artículo sobre las buenas prácticas
para el [desarrollo seguro con IA](../5-buenas-practicas-codigo-ia-gen/),
las compañías pueden basar sus políticas de IA generativa
en sus políticas ya existentes.

<image-block>

!["Cheat sheet Fluid Attacks - Política IA generativa"](https://res.cloudinary.com/fluid-attacks/image/upload/v1705959187/airs/es/blogs/im%C3%A1genes%20traducidas/6-items-principales-politica-ia-generativa/6-politica-IA-generativa-para-un-desarrollo-2.webp)

*Cheat sheet*: Seis aspectos principales en una política de desarrollo
de *software* con IA generativa

</image-block>

## Identificar los casos de uso de la IA generativa en tu empresa

Es importante saber cómo se utilizaría la IA generativa en tu empresa.
Los casos de uso que puedas identificar te servirán de guía
para establecer los requisitos de seguridad pertinentes.
Los siguientes son algunos ejemplos de casos de uso:

- Uso de ChatGPT para encontrar errores en el código fuente
  y soluciones a los mismos

- Uso de Copilot para escribir funciones de *software* de forma rápida

- Uso de herramientas de seguridad de aplicaciones
  para corregir automáticamente el código en el que se hayan
  detectado vulnerabilidades.

El alcance de la política se definiría entonces
por los casos de uso que cubre.
Además,
la mención de los riesgos de seguridad implicados
en dichos casos de uso ayudaría a establecer
claramente el propósito de la política.
Incluso otra información importante relativa a los casos
de uso que debe transmitirse en la política
son las consecuencias de violarla.

## Establecer las herramientas de IA generativa que el desarrollador puede usar

Los desarrolladores de tu compañía probablemente
ya estén familiarizados con herramientas de IA como ChatGPT y Copilot.
La política debería mencionar estas tecnologías de ayuda
para establecer si se permite su uso y cualquier limitación al mismo.
También puede exigir a los desarrolladores
que notifiquen si han utilizado herramientas
de IA generativa en sus tareas y en qué medida.

Además,
debería establecerse un proceso para que los desarrolladores
soliciten el uso de la herramienta de IA aprobada
para un fin distinto del especificado en la política.
La política debería también dejar claro qué podrían hacer
los desarrolladores para solicitar el uso de una herramienta no aprobada.

## Establecer la solución que evaluará la seguridad del código generado por IA

El código generado por IA puede contener vulnerabilidades de seguridad,
algunas de las cuales pueden deberse a que la herramienta desconoce
la lógica de negocio de la compañía.
Es importante establecer en tu política de IA que los desarrolladores
siempre deben revisar el resultado de la herramienta.
Los [escáneres de vulnerabilidades](../escaneo-de-vulnerabilidades/)
son sus aliados en este proceso para identificar las amenazas más simples.
La política debe informar, después de una consulta cuidadosa, qué herramientas
de [pruebas de seguridad de aplicaciones](../fundamentos-pruebas-de-seguridad/)
están permitidas.
Por consiguiente,
debe reconocer las tasas significativas de falsos positivos
y falsos negativos de los reportes de las herramientas.

Para encontrar vulnerabilidades más críticas para la empresa,
se aconseja un enfoque que implique
la [revisión manual del código fuente](../revision-codigo-fuente/)
por parte de analistas de seguridad
([*pentesters* o *hackers* éticos](../que-es-hacking-etico/)).
La política puede entonces establecer que estas pruebas manuales
se realicen junto con las automatizadas.
En otro artículo describimos lo que deberías buscar en una solución
de [gestión de vulnerabilidades](../elegir-gestion-de-vulnerabilidades/).
Presentamos a continuación una *checklist* simplificada de diez elementos
para darte una idea:

- Permite identificar y hacer inventario de los activos digitales de tu empresa.

- Produce reportes precisos rápidamente.

- Utiliza múltiples técnicas (p. ej., [SAST](../../producto/sast/),
  [DAST](../../producto/dast/) y [*pentesting*](../../producto/mpt/)),
  que se mejoran constantemente.

- Ofrece una única plataforma.

- Evalúa el cumplimiento de múltiples estándares de seguridad.

- Busca vulnerabilidades continuamente.

- Permite priorizar las vulnerabilidades basándose en los riesgos.

- Ofrece ayuda continua para comprender y remediar las vulnerabilidades.

- Valida la remediación satisfactoria de las vulnerabilidades,
  ofreciendo incluso mecanismos automatizados
  para detener los despliegues riesgosos.

- Permite la personalización de sus reportes y el seguimiento
  y la comparación del progreso en la remediación de vulnerabilidades.

Otra información clave que debe estar presente
en tu política es si a los desarrolladores se les permite utilizar
herramientas de inteligencia artificial
para obtener **sugerencias** de corrección del código.
Y, de ser así, qué herramientas están permitidas.
Eso sí,
hay que aclarar que incluso estos resultados tienen
que ser revisados de nuevo en busca de vulnerabilidades.
¿Qué sentido tiene entonces?
Bueno, la IA generativa ayuda a acelerar el proceso.

Si todavía estás buscando la solución adecuada,
te invitamos a iniciar una [prueba gratuita](https://app.fluidattacks.com/SignUp)
de nuestra herramienta y disfrutar de muchas
de las características enumeradas anteriormente,
incluyendo nuestra función [Autofix](https://help.fluidattacks.com/portal/en/kb/articles/fix-code-automatically-with-gen-ai)
potenciada por IA generativa.
Ofrecemos todas las características enumeradas
en la lista a través de nuestro [plan estrella](../../planes/) (pago).

## Describir los datos que no deben introducirse en las herramientas de IA

Tu compañía debería haber identificado qué tipo de información maneja
y dónde se encuentran los datos sensibles.
Si tu compañía adopta la IA generativa,
lo mejor es definir antes de su uso qué información no debe introducirse
en los *prompts* de las herramientas de IA.
Esto se hace después de conocer a fondo las medidas de protección
de datos de las herramientas.

Tu compañía tendría que identificar el modo
en que se puede poner en peligro la confidencialidad
de la información en casos de uso como los de arriba,
puesto que la información introducida en algunas herramientas
de IA generativa puede utilizarse para seguir entrenando
la herramienta e incluso ser objeto de filtraciones de datos.
Algunos incidentes muy sonados con ChatGPT son la
[exposición al público por parte de Samsung de sus propios secretos](https://www.theregister.com/2023/04/06/samsung_reportedly_leaked_its_own/)
tres veces, el
[acceso de atacantes al historial de chat](https://securityintelligence.com/articles/chatgpt-confirms-data-breach/)
de otros usuarios tras explotar una falla en una librería de código abierto,
y el [robo de más de 100.000](https://www.bleepingcomputer.com/news/security/over-100-000-chatgpt-accounts-stolen-via-info-stealing-malware/)
cuentas de usuario.

Por consiguiente,
y esto no debería ser totalmente nuevo en las políticas de tu empresa
(excepto para la consideración del uso de IA generativa),
deberían establecerse claramente una obligación
y una directriz para informar de incidentes relacionados con la IA
(p. ej., filtración de datos, violación de la propiedad intelectual).

<div>
<cta-banner
buttontxt="Leer más"
link="/soluciones/gestion-vulnerabilidades/"
title="Inicia ahora con la solución de gestión de vulnerabilidades
de Fluid Attacks"
/>
</div>

## Exigir a los proveedores que notifiquen y describan su uso de IA generativa

Tu compañía debe tomar medidas preventivas contra los ataques
a la cadena de suministro.
Hemos mencionado algunos de los aspectos más importantes
en los que hay que fijarse siguiendo el enfoque
de [seguridad de la cadena de suministro de *software*](../../../blog/software-supply-chain-security/).
Uno de ellos es verificar las políticas y procedimientos
de los proveedores para ver si se ajustan
a las buenas prácticas y estándares.

La política de IA generativa de tu empresa debería establecer
que los proveedores deben detallar el uso que hacen
de la IA generativa de terceros de la forma más exhaustiva posible.
Algunos datos clave son el tipo de datos que introducen
y cómo se aseguran de que no se utilicen en el entrenamiento de la herramienta.
En cuanto a los estándares,
la política puede exigir que el proveedor conozca
y cumpla la norma [ISO/IEC 38507:2022](https://www.iso.org/standard/56641.html).
Además,
tu empresa también puede ser un proveedor y, en ese caso,
su política debe exigir la divulgación detallada
de su propio uso de IA generativa a los proyectos
que utilicen el *software* de tu empresa en su desarrollo.

## Describir las estrategias de comunicación y aprendizaje de la política de IA

Tu compañía debe hacer accesible su política de IA generativa
a todos los empleados y comunicarles cualquier cambio importante que reciba.
En cuanto a la estrategia de comunicación de la política,
podría incluir lo siguiente:

- La estructura de los mensajes que se dirigirán al personal
  (p. ej., un caso con un problema, héroe y moraleja)

- Los canales de comunicación
  (p. ej., correo electrónico, redes sociales) y las herramientas
  (p. ej., videos, infografías) que se utilizarán

- Los métodos y tiempos para supervisar y evaluar el impacto de la comunicación,
  como el conocimiento de la política y la actitud hacia ella

- Los canales de retroalimentación a través
  de los cuales se obtiene información para mejorar la política
  y/o su estrategia de comunicación

Además,
tu organización necesita asegurarse de que su personal lea
y reconozca la política de IA generativa
y complete la capacitación sobre el uso responsable
de las herramientas de IA generativa.
Respecto a esto último, la política puede incluir lo siguiente:

- Los formatos y materiales de la capacitación

- El personal encargado de impartir la capacitación

- Los métodos y momentos para supervisar y evaluar los conocimientos
  sobre el uso responsable de las herramientas de la IA generativa

- Los canales de retroalimentación a través de los cuales
  se obtiene información para mejorar la estrategia
  de aprendizaje de la política

Para disponer de un recurso útil en las estrategias de comunicación
y formación sobre políticas de tu compañía,
consulta nuestro resumen con cinco buenas prácticas
para [desarrollar de forma segura con IA generativa](../5-buenas-practicas-codigo-ia-gen/).
Y si tu compañía permite el uso
de nuestra [extensión para el IDE](https://help.fluidattacks.com/portal/en/kb/articles/install-the-vs-code-extension)
(entorno de desarrollo integrado),
nuestra documentación es un recurso útil,
no solo para aprender cómo usarla sino, además,
[cómo maneja los datos de manera segura](https://help.fluidattacks.com/portal/en/kb/articles/integrations-faq).
