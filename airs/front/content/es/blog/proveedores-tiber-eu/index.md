---
slug: blog/proveedores-tiber-eu/
title: Consejos de TIBER para elegir proveedores
date: 2022-05-20
subtitle: Requisitos para la inteligencia de amenazas y el red teaming
category: filosofía
tags: ciberseguridad, red-team, blue-team, pruebas de seguridad, empresa
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1653319371/blog/tiber-eu-providers/cover_tiber_eu_providers.webp
alt: Foto por Terry Vlisidis en Unsplash
description: En este artículo conocerás las pautas que TIBER-EU proporciona a las entidades para elegir proveedores de inteligencia de amenazas y red teaming.
keywords: TIBER EU, Pautas, Red Team, Pruebas Red Team, Escenarios Red Team, Inteligencia De Amenazas, Ciberresiliencia, Hacking Etico, Pentesting
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Escritor y editor
source: https://unsplash.com/photos/WsEbnsnKbUE
---

[¿Has oído hablar del TIBER-EU?](../marco-tiber-eu/)
Esta pregunta es el título que dimos a un artículo del blog la semana pasada,
presentando esa iniciativa liderada
por el Banco Central Europeo principalmente para evaluar
y proteger los sistemas financieros de la eurozona.
En este nuevo artículo,
nos centramos explícitamente en los requisitos del TIBER-EU
para los proveedores de inteligencia de amenazas
(TI por sus iniciales en inglés)
y [*red teaming*](../ejercicio-red-teaming/) RT.
Estas empresas son las encargadas
de analizar las amenazas potenciales y realizar
[*hacking* ético](../que-es-hacking-etico/)
contra las entidades financieras europeas para poner a prueba
su ciberseguridad y ciberresiliencia.
Las partes interesadas no deben hacer
una elección improvisada de los proveedores.
Para ello existen las
[pautas de contratación de servicios (Services Procurement Guidelines)](https://www.ecb.europa.eu/pub/pdf/other/ecb.1808tiber_eu_framework.en.pdf)
del TIBER-EU, que tomamos como referencia
para este segundo artículo de la serie.
Te invitamos a revisar el documento completo
para más detalles sobre lo que presentamos aquí.

## ¿Qué encontramos en las pautas de contratación de servicios?

Como se indica en las pautas de contratación de servicios,
debido a la naturaleza sensible de las pruebas TIBER-EU,
las entidades deben seleccionar cuidadosamente a proveedores
de TI y RT que puedan proporcionar un nivel adecuado
de experiencia profesional y apoyo para la realización de la prueba.
Estas pautas indican los requisitos y estándares
que deben cumplir los proveedores de TI y RT
para realizar las pruebas TIBER-EU.
Ofrecen orientación y criterios de selección para las entidades
que deseen contratar proveedores.
Y también ofrecen preguntas y listas de verificación
de acuerdos que ayudan a formalizar el proceso de contratación.
El TIBER-EU Knowledge Centre se encarga
de hacer un seguimiento del mercado de TI y RT
y de introducir cambios en los requisitos
de las pautas siempre que sea necesario.

### Pautas relacionadas a los proveedores de inteligencia de amenazas

El proveedor de TI tiene la misión de proporcionar
una imagen clara de la superficie de ataque de la entidad evaluada
y generar escenarios de amenazas que imiten la realidad.
Estos sirven de base para los escenarios
de ataque que utilizará el proveedor de RT.
El proveedor debe conocer a los actores de amenaza,
sus capacidades, motivos y metodologías,
especialmente en lo que respecta al tipo de entidad del que se trate.
En cuanto a este objetivo de pruebas,
el proveedor de TI debe conocer sus operaciones,
funciones críticas y personal, así como sus puntos débiles.

Entre los requisitos
para el proveedor de TI está tener al menos
tres referencias de asignaciones previas relacionadas
con pruebas de *red team* dirigidas por inteligencia de amenazas.
Además,
TIBER-EU menciona un seguro de indemnización
adecuado con el que el proveedor
pueda responder a situaciones comprometedoras,
como las que podrían derivarse de la negligencia.
Debe haber un responsable de TI que dirija y supervise las actividades.
Este debería tener al menos cinco años de experiencia en el área,
de los cuales al menos tres
deberían ser en proyectos del sector financiero.
Adicionalmente,
debería poseer certificaciones como el
CREST Certified Threat Intelligence Manager (CCTIM)
y el Offensive Security Certified Expert (OSCE).

Por parte de los miembros del equipo de TI,
cada uno debería poseer al menos dos años de experiencia
en inteligencia de amenazas.
Se requiere un equipo multidisciplinar
con un amplio espectro de conocimientos,
incluyendo OSINT, HUMINT y conocimientos geopolíticos.
Entre la amplia gama de certificaciones que podrían
haber obtenido se encuentran la
CREST Certified Simulated Attack Specialist (CCSAS),
la Cybersecurity Nexus (CSX),
la Certified Information Systems Security Professional (CISSP)
y la Systems Security Certified Practitioner (SSCP).
Se espera que el equipo haya proporcionado inteligencia de amenazas
para pruebas de *red team* en el pasado.

La entidad receptora de la prueba TIBER-EU
es la encargada de verificar que el proveedor de TI cumpla estas
y otras normas establecidas en las pautas.
No obstante, puede encomendar esta responsabilidad
a organismos de acreditación y certificación de la Unión Europea.
La entidad debería buscar un proveedor de TI
con expertos técnicos que puedan explicar su metodología
y con personal de apoyo.
Debería ser una compañía con un dominio maduro de los estándares éticos
y adscrita a un código de conducta reconocido.
Debería ser un proveedor que garantice
que gestionará adecuadamente los sistemas
y los riesgos de información de la entidad.
Además, la entidad debería solicitar evidencia de las políticas
de seguridad de la información del posible proveedor.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/red-team/"
title="Inicia ahora con la solución de red teaming de Fluid Attacks"
/>
</div>

### Pautas relacionadas con los proveedores de *red teaming*

De acuerdo con el Marco TIBER-EU,
la entidad debe asegurarse de que el proveedor de
RT llevará a cabo una prueba de *red team*
dirigida por inteligencia y no solo
una [prueba de penetración](../../soluciones/pruebas-penetracion-servicio/).
La principal distinción entre
estos dos métodos de prueba es la siguiente:
el primero incluye un escenario completo con personas,
procesos y tecnologías en la evaluación.
El segundo suele centrarse en los sistemas
y sus vulnerabilidades técnicas y de configuración.
Siguiendo los esfuerzos del proveedor de TI,
este otro proveedor toma escenarios de amenazas
y los convierte en ataques.
El proveedor de RT debería tener como objetivo evaluar
la postura de ciberresiliencia de la entidad en función
de la amenaza a la que se enfrenta.
Siempre debería haber una estrecha relación entre los proveedores
para estructurar y actualizar los planes de prueba
y generar y entregar el informe final.

Como en el caso del proveedor de TI,
el proveedor de RT debe contar con varios años de experiencia,
un seguro de indemnización y un gestor competente y cualificado.
Aparte de la mencionada OSCE,
el director de pruebas de *red team* también debería poseer
un certificado como el CREST Certified Simulated Attack Manager (CCSAM).
Los miembros del equipo deberían poseer al menos dos años
de experiencia en pruebas de *red team*.
Entre los conocimientos y aptitudes que debe tener el *red team*,
TIBER-EU sugiere los siguientes:
conocimientos empresariales, pruebas de *red team*, pruebas de penetración,
reconocimiento, inteligencia de amenazas, gestión de riesgos,
desarrollo de *exploits*, penetración física,
ingeniería social y análisis de vulnerabilidades.

Las certificaciones que puede obtener
un miembro de un *red team* son múltiples.
TIBER-EU sugiere algunas de ellas que podrían estar entre
las que certifiquen al equipo del proveedor de RT.
Por supuesto, cuantas más tengan, mejor.
Además de destacar varias certificaciones de [GIAC](https://www.giac.org/)
y [Offensive Security](https://www.offensive-security.com/courses-and-certifications/),
mencionan la eLearnSecurity Certified Professional Penetration Tester (eCPPT)
y la Certified Ethical Hacker (CEH), entre otras.
En Fluid Attacks, tenemos algunas de estas y más.
Recientemente hemos incluido
en [nuestra lista de certificaciones](../../../certifications/)
varias de [Mile2](https://www.mile2.com/).
Estrechamente asociadas al *red teaming*, tenemos, por ejemplo:
la Certified Red Team Operator,
la Certified Red Teaming Expert y la Certified Red Team Professional.

Una vez más,
el cumplimiento de los requisitos por parte
del proveedor es algo que la entidad o los organismos
de acreditación y certificación deben verificar antes
de iniciar la prueba TIBER-EU.
Tres de los criterios más importantes para un comprador
de servicios de pruebas de *red team* son la reputación
y el historial del proveedor de RT
y la conducta ética que adopta y hace cumplir.
La entidad necesita encontrar en el proveedor un plan adecuado
de gestión de riesgos y confidencialidad.
Este último debe ofrecer metodologías avanzadas,
innovadoras y de alta calidad.
Todo ello,
con la expectativa de una simulación adecuada de ataques reales
contra la entidad como objetivo global.

El *red teaming* evalúa a las organizaciones
y sus estrategias de mitigación de riesgos,
detección y respuesta ante amenazas y resiliencia.
También identifica sus debilidades y vulnerabilidades
para que las corrijan y mejoren sus medidas preventivas.
Aunque TIBER-EU es una iniciativa para proyectos con entidades europeas,
puede servir de referencia para muchos en todo el mundo.
Tanto para quienes ofrecemos servicios como los mencionados,
como para quienes los requieren.
Fluid Attacks, por ejemplo,
es un *red team* altamente experimentado
y cualificado que puede actuar en favor
de la ciberseguridad de tu organización.
Te invitamos a descubrirlo. [¡Contáctanos!](../../contactanos/)
