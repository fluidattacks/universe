---
slug: blog/pruebas-de-penetracion-continuas/
title: Pruebas de penetración continuas
date: 2023-01-23
subtitle: Beneficios del pentesting continuo frente al puntual
category: filosofía
tags: pentesting, pruebas de seguridad, ciberseguridad, empresa
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1674517469/blog/continuous-penetration-testing/cover_continuous.webp
alt: Foto por Anchor Lee en Unsplash
description: El pentesting continuo supera al pentesting realizado de forma puntual y periódica. Presentamos sus ventajas y cómo superamos los retos que plantea su implementación.
keywords: Pruebas De Penetracion Continuas, Pruebas De Penetracion Puntuales, Postura De Seguridad, Evaluaciones Continuas, Alcance De Las Pruebas De Penetracion, Con Que Frecuencia Se Deben Realizar Las Pruebas De Penetracion, Cuanto Cuestan Las Pruebas De Penetracion, Hacking Etico, Pentesting
author: Jason Chavarría
writer: jchavarria
name: Jason Chavarría
about1: Escritor y editor
source: https://unsplash.com/photos/3cdnsf7-wLw
---

Las organizaciones ya no pueden ignorar
la necesidad de proteger su _software_ de forma continua.
Así como las ciberamenazas no descansan,
tampoco deberían hacerlo los desarrolladores
en sus esfuerzos por remediar fallas.
Explicamos por qué es importante
realizar continuamente [_pentesting_](../que-es-prueba-de-penetracion-manual/).
Argumentamos que este ayuda a mantener una postura de seguridad sensata
y a ahorrar gastos
(nota: especialmente tiempo).
Además,
comparamos este enfoque con las pruebas de penetración puntuales.
Más aún,
mencionamos las formas en que nuestra solución
de [_pentesting_](../../soluciones/pruebas-penetracion-servicio/)
supera los desafíos comúnmente asociados con las evaluaciones continuas.

## ¿Con qué frecuencia debe realizarse el _pentesting_?

¿Has visto los temas de los boletines
que reciben los expertos en ciberseguridad?
Siempre es algo así como
"los atacantes están explotando una falla en
[inserte el nombre del *software*]",
o
"[inserte el nombre de la técnica de ataque maliciosa]
alcanza un nuevo nivel".
Por no hablar de la inclusión de los ataques _ransomware_,
que aparecen constantemente en los titulares.
¿Tiene sentido realizar evaluaciones de seguridad
únicamente una vez al año?
La respuesta es un rotundo "¡no!".

Es cierto,
las empresas que evalúan la seguridad
de sus sistemas anualmente,
o que al menos la sometieron a una evaluación una sola vez,
están abordando el problema,
aunque sea mínimamente.
Las pruebas de penetración puntuales
les proporcionan un pantallazo de su postura de seguridad.
Puede ser en términos de superficie de ataque conocido,
vulnerabilidades en los sistemas,
riesgos cuantificados o tasa de remediación en ese momento.
Esa es su referencia,
que es útil para las comparaciones con la siguiente evaluación.
Pero sigue existiendo la incertidumbre
de si podrán resistir los ataques
que sufran entre esos periodos,
ya que los desarrolladores tienen
que generar valor constantemente.
Lo que las empresas realmente necesitan
es ver cómo está su superficie de ataque,
su ambiente de amenazas y el rendimiento
de la gestión de vulnerabilidades en tiempo real.
La conclusión es que
las pruebas de penetración deben realizarse de forma continua.

## Beneficios del _pentesting_ continuo

### Identificación de la postura de seguridad en tiempo real

Cuando las evaluaciones se realizan de forma continua,
las empresas pueden descubrir a tiempo
los activos que componen sus superficies de ataque.
Entonces tienen la oportunidad de evaluar
las vulnerabilidades de esos activos.
Cualquier nueva amenaza
(p. ej., ataque hipotético)
que aparezca en su ambiente es imitada inmediatamente
por los _hackers_ éticos que realizan las pruebas.
Aunque lo ideal es que estos profesionales altamente certificados
encuentren las debilidades
o vulnerabilidades antes de que se tenga noticia
de cualquier explotación en libre circulación.
El riesgo identificado y mitigado también
es fiel a la postura de seguridad actual,
no a la de hace meses.
Y como a los desarrolladores
se les notifica al instante cualquier falla encontrada en cambios recientes,
pueden empezar a resolverla mientras ese código está reciente en sus mentes.

### Reducción de costos

Puede que te sorprenda,
pero los costos pueden ser más bajos
para las pruebas de _pentesting_ continuas que para las puntuales.
Y no se trata solo de costos en dólares.

Llevar a cabo evaluaciones anuales
(o no de forma continua)
significa que las empresas tienen que dedicar
una gran cantidad de tiempo a establecer el alcance del proyecto,
conseguir los profesionales adecuados,
fijar las expectativas y entregables,
acordar metodologías y plazos, etc.
Esto hay que hacerlo una y otra vez.
El _pentesting_ continuo puede requerir ajustes ocasionales
en la configuración inicial de vez en cuando,
pero eso sería todo.

El tiempo de remediación también se reduce
en gran medida al realizar evaluaciones continuas,
ya que los desarrolladores son notificados
y pueden solucionar los problemas sobre la marcha.
Por el contrario,
las evaluaciones puntuales acumulan lo que podría ser la cantidad
de problemas de seguridad de un año,
medio año o un trimestre.
Gestionar todas estas debilidades
y vulnerabilidades se convierte en una tarea tediosa
y aparentemente interminable.
En última instancia,
los desarrolladores se ven obligados a rehacer el código
de hace meses en lugar de generar valor.

En cuanto al dinero,
el costo de los _pentests_ continuos,
en el mejor de los casos,
no superará al de las brechas de datos.
Es una pócima difícil de tragar,
pero tu empresa gasta en ciberseguridad
para evitar perder cantidades mucho mayores
por los efectos de los ciberataques.
Evaluar continuamente la resistencia de tus sistemas
frente a las últimas tendencias en ataques
es una mejor estrategia en comparación
con las pruebas de seguridad puntuales cuando se trata
de evitar la irrupción de _hackers_ malintencionados.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/pruebas-penetracion-servicio/"
title="Inicia ahora con las pruebas de penetración como servicio
de Fluid Attacks"
/>
</div>

### Alcance modificable

Mientras que el alcance del _pentesting_
es generalmente estático en el caso del enfoque puntual,
es modificable en el caso del enfoque continuo.
El primero está en desventaja,
porque en caso de que se descubran activos
que eran desconocidos antes del contrato,
el alcance de las pruebas de penetración los excluiría.
Un contrato de _pentesting_ continuo permitiría
a la empresa ajustar el alcance
para que las pruebas posteriores se centren oportunamente
en esas áreas recién descubiertas de la superficie de ataque.

### Asistencia continua por expertos

Cuando las empresas han implantado el enfoque puntual,
las limitaciones de tiempo pueden no permitir
a los desarrolladores ponerse en contacto
con los analistas de seguridad
y resolver muchas de sus dudas.
Como decíamos en un punto anterior,
los reportes pueden contener problemas de varios meses.
Tu equipo puede aplicar una estrategia
de priorización razonable para arreglar
lo que podría ser más problemático
y obtener para esto el apoyo de expertos,
pero podría quedar mucho por resolver.
Los analistas de seguridad pueden validar
que las correcciones son eficaces y,
si es el caso,
tu empresa puede lograr suficiente cumplimiento de un estándar.
Entonces,
habrá que esperar a la siguiente evaluación
para que los desarrolladores resuelvan sus dudas con los expertos,
e incluso entonces pueden surgir problemas más urgentes.
Las evaluaciones continuas pueden eliminar este problema
al permitir el contacto con los expertos de forma permanente.

## Retos del _pentesting_ continuo

A pesar de saber que están sometidas a ciberamenazas constantes,
hay razones por las que las empresas que contratan pruebas de penetración
de terceros podrían optar por evaluaciones puntuales.

A menudo,
se trata de que las empresas se preguntan:
"¿Cuánto nos cuestan las pruebas de penetración
en relación con nuestro presupuesto para ciberseguridad?".
De hecho,
la empresa puede permitirse solo la evaluación puntual regular
y puede que la haga al menos anualmente,
en el caso de que necesite
seguir [ciertos estándares](../cumplimiento-pruebas-de-penetracion/)
en los que el cumplimiento de las pruebas de penetración
es obligatorio.

Además,
está el reto de que el _pentesting_ manual
se tarda tiempo en obtener resultados.
Esto alimenta la idea de que la seguridad
es un obstáculo para la generación de valor.

Y también está el desafío de los recursos
de la empresa para tratar todos los datos
que arrojan las pruebas de penetración continuas.
Surgen dudas no solo sobre dónde almacenar los datos,
sino también sobre cómo hacer un seguimiento
de los hallazgos a medida que se acumulan.

## Así aborda Fluid Attacks estos retos

En Fluid Attacks,
ofrecemos nuestra [solución de _pentesting_](../../soluciones/pruebas-penetracion-servicio/)
bajo el modelo de [pruebas de penetración como servicio](../que-es-ptaas/)
(PTaaS),
lo que significa que reconocemos la necesidad de seguridad
para cubrir todo el ciclo de vida de desarrollo de _software_ (SDLC).
Ofrecemos a las empresas que buscan tercerizar los servicios
de _pentesting_ una solución que aborda los desafíos
de las pruebas de penetración continuas:

- Reconociendo las limitaciones presupuestarias de la mayoría de las empresas,
  nuestras evaluaciones continuas son rentables.

- Dejamos que los desarrolladores desplieguen primero,
  ya que la generación de valor es una necesidad,
  y luego siguen nuestros _hackers_ éticos,
  quienes prueban los microcambios
  y reportan los problemas de ciberseguridad que encuentran.

- Incluimos con nuestro servicio el acceso a nuestra
  [plataforma](../../plataforma/),
  donde los usuarios configuran el alcance de las evaluaciones,
  ven los hallazgos a través de gráficas útiles,
  asignan la remediación,
  leen las recomendaciones,
  hablan con nuestros _hackers_,
  hacen un seguimiento de las fallas
  y la exposición al riesgo que representan, y mucho más.

[Contáctanos](../../contactanos/)
para recibir nuestra propuesta de _pentesting_ continuo.

Nuestra solución de _pentesting_ forma parte de
nuestro [plan Advanced](../../planes/)
de [Hacking Continuo](../../servicios/hacking-continuo/).
**Puedes**
[**probar ahora gratis nuestro plan Essential de Hacking Continuo**](https://app.fluidattacks.com/SignUp),
que implica solo pruebas de seguridad automatizadas,
para encontrar vulnerabilidades determinísticas en tus sistemas
y familiarizarte con nuestra plataforma.
También puedes cambiarte al plan Advanced
desde la versión demo para disfrutar de _pentesting_ continuo.
