---
slug: blog/tribu-de-hackers-2/
title: Tribu de hackers red team 2.0
date: 2020-11-27
subtitle: Aprendiendo del experto en red teams Benjamin Donnelly
category: opiniones
tags: ciberseguridad, red-team, hacking, pentesting, blue-team
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1620331132/blog/tribe-of-hackers-2/cover_tdklcz.webp
alt: Foto por Manyu Varma en Unsplash
description: Este artículo se basa en el libro "Tribe of Hackers Red Team" de Carey y Jin. Aquí compartimos contenido de la entrevista con Benjamin Donnelly.
keywords: Red Team, Seguridad Red Team, Hacking Red Team, Pentesting, Hacking Etico, Blue Team, Conocimiento, Tribu De Hackers
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Escritor y editor
source: https://unsplash.com/photos/OFzTf6iZb2Y
---

En julio de este año, escribí un post basado en el libro
[*Tribe of Hackers Red Team*](https://www.amazon.com/Tribe-Hackers-Red-Team-Cybersecurity/dp/1119643325)
de Marcus J. Carey y Jennifer Jin (2019).
En esa ocasión, presenté una breve descripción del libro,
el cual está dirigido principalmente a todos aquellos interesados
en los [*red teams*](../ejercicio-red-teaming/)
(Fluid Attacks, por ejemplo, es un [*red team*](../../soluciones/red-team/)).
Además, me referí a las respuestas que Marcus dio
a las preguntas que él mismo y Jin formularon a los más
de 40 expertos que aparecen en su libro.
Para este artículo,
me centraré en lo que compartió uno de esos expertos en *red teams*.
Espero que sea de tu interés —ten en cuenta el valor significativo
que siempre tendrán las experiencias ajenas para nuestro aprendizaje.

En este caso, tenemos al ingeniero
[estadounidense-canadiense](https://en.everybodywiki.com/Benjamin_Donnelly_\(polymath\))
Benjamin Donnelly,
quien ha trabajado como parte de equipos que hackean cosas como prisiones,
plantas de energía, multinacionales e incluso estados enteros.
Ha participado en proyectos de investigación como el
Active Defense Harbinger Distribution
([ADHD](https://www.activecountermeasures.com/free-tools/adhd/)),
financiado por DARPA, y fue el creador del
[criptosistema Ball and Chain](https://www.irongeek.com/i.php?page=videos/derbycon4/t108-ball-and-chain-a-new-paradigm-in-stored-password-security-benjamin-donnelly-and-tim-tomes).

Ben se acercó al mundo de los *red teams* cuando,
cursando el bachillerato,
participó en una competición del programa
de educación cibernética estadounidense
[CyberPatriot](https://en.wikipedia.org/wiki/CyberPatriot).
Aunque su formación allí estaba más orientada al trabajo de
[*blue team*](../../../blog/purple-team/),
consiguió introducirse en las "artes rojas"
y empezó a participar en torneos de
[NetWars](https://www.sans.org/cyber-ranges/netwars-tournaments/core/)
Allí,
compitiendo contra profesionales,
Ben empezó a ganar reconocimiento por sus habilidades
e incluso consiguió un trabajo con un instructor de
[SANS](https://www.sans.org/).
Después de eso, y sin duda a base de trabajo duro,
obtuvo el título oficial de *pentester*/investigador de seguridad.

## Para los que buscan ser diligentes en los *red teams*

Benjamin ofrece algunas recomendaciones
para conseguir trabajo en un *red team*.
Lo que sugiere inicialmente es reconocer las sutiles diferencias
entre las empresas que realizan este tipo de trabajo.
Para ello las separa en dos grupos,
y para cada caso ofrece algunos consejos.

Ben se refiere al primer grupo como un equipo
el tipo operador de redes informáticas,
el cual se centra en explotar redes
o sistemas a través de diversos marcos y herramientas de *hacking*.
Las formas en que los atacantes pueden obtener
y aprovechar accesos son comunicadas al cliente.
De acuerdo a esto,
Benjamin comenta que si quieres unirte a uno de estos equipos,
tienes que enfocarte en la formación sobre simulación de brechas
porque de eso se trata su mundo.
Efectivamente,
en Fluid Attacks la
[simulación de brechas y ataques](../../../blog/what-is-breach-attack-simulation/)
forma parte de nuestro trabajo.
Además, Ben afirma que no suele ser necesario que
poseas muchos certificados para un trabajo de este tipo,
ni siquiera títulos universitarios,
si cuentas con las habilidades suficientes y te das a conocer.
Tal es el caso para trabajar con nosotros
como [*hacker* ético o *pentester*](../../../careers/openings/), por ejemplo.

Ben se refiere al segundo grupo como un equipo del tipo ingeniero de seguridad.
Según él, este se centra en crear
y auditar soluciones complejas para mejorar la sofisticación técnica
y la seguridad de un determinado sistema de *software* o *hardware*.
Este equipo prioriza el análisis de sistemas desde varias perspectivas
y responde con los controles necesarios a los posibles vectores
de ataque que los *hackers* podrían utilizar en sistemas concretos.
Y aunque veo este tipo de trabajo más relacionado con los *blue teams*,
también podría asociarlo con el puesto
de [arquitectos de seguridad informática](../../../careers/openings/)
en Fluid Attacks.
Por cierto, en nuestra empresa tampoco se exige
un título universitario para presentarse a este puesto.
Pero como sugiere Ben, para ambos grupos,
querrás contar con alguna combinación de conocimientos
de ciencias informáticas y tecnología de la información.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/red-team/"
title="Inicia ahora con la solución de red teaming de Fluid Attacks"
/>
</div>

Según Ben, aprender las habilidades básicas necesarias
(por ejemplo, la manipulación de la infraestructura)
trabajando en puestos de defensa de redes informáticas
facilitará tu transición a un trabajo dentro de un *red team*.
Aprenderás qué es lo que hacen los atacantes mientras
te preparas para anticiparte a ellos.
Por otro lado, además del aspecto técnico,
este joven experto recomienda trabajar algunas actitudes.
Sugiere mantener activas las disposiciones para hacer preguntas,
reconocer la ignorancia en algún tema sin problemas y autocorregirse.

## Para los que ya sudan sangre en los *red teams*

Desde la perspectiva de Ben,
el trabajo del *red team* es curiosamente más eficaz
cuando se hace individualmente.
Él se refiere a la comunicación interpersonal
como un gran problema porque esta profesión
implica enormes cantidades de información.
Además,
las operaciones que se realizan suelen ser muy complejas y específicas.
Por su experiencia,
Ben dice que los miembros del equipo tienen que averiguar
lo que está pasando dentro del sistema rastreando
enormes cantidades de variables indicadas por respuestas externas,
como los mensajes de error.
Comunicar exactamente qué está pasando, y cuándo,
a los demás miembros del equipo, puede ser un reto enorme.

Sin embargo,
Ben muestra un gran interés por la colaboración
cuando trabaja con *blue teams*,
y prefiere el [método de pruebas de caja gris](https://es.wikipedia.org/wiki/Pruebas_con_caja_gris)
al de caja negra.
(No obstante, el enfoque que a utilizar
habitualmente depende de lo que elija la organización para la cual trabajas).
Para él,
lo ideal sería que los analistas probaran secciones discretas
de la aplicación o red para comprender las amenazas
que afronta exactamente esa parte del sistema,
independientemente de cualquier otra capa de protección.

<div class="imgblock">

![Benjamin Donnelly](https://res.cloudinary.com/fluid-attacks/image/upload/v1620331130/blog/tribe-of-hackers-2/benjamin_somzhx.webp)

<div class="title">

Fotografía original de Benjamin tomada de pbs.twimg.com.
[pbs.twimg.com](https://web.archive.org/web/20190929024550/https://pbs.twimg.com/profile_images/1092476712741302272/Ss5tKSjh_400x400.jpg).

</div>

</div>

Para Ben,
los miembros de los *red teams* no deberían orientar
su trabajo hacia la postura "estoy viviendo la vida de un *hacker*",
deseando únicamente impresionar a los demás.
Esta profesión debería tener entre sus objetivos centrales ayudar
a los equipos de seguridad a optimizar la seguridad de sus sistemas
o programas informáticos.
Por eso Ben considera que su trabajo es informar al equipo
de producto literalmente todo lo que sabe.
Si ellos pueden retener todo ese conocimiento, entonces él,
como profesional de *red cell*, tiene que seguir adelante y descubrir más.
Por cierto,
y como aspecto fundamental a tener en cuenta
en los reportes que tengas que hacer, Ben sugiere lo siguiente:
Ser detallado, correcto y honesto.

## Para empresas que aspiran estar a la vanguardia en seguridad

Cuando se le pregunta a Ben
por el control de seguridad menos eficaz actualmente en uso,
parece referirse sin dudarlo a la tecnología *firewall*.
Él plantea que todos esos caros dispositivos de “seguridad”
que parecen seguir vendiéndose como pan caliente
son efectivamente capaces de detener
al 0% de los adversarios técnicamente sofisticados.
El mayor problema es que muchas personas que deciden acceder
a esa tecnología desconocen sus capacidades reales.

Una de las recomendaciones más importantes de Ben
para prevenir ataques a sistemas o redes es aislar al 100% al cliente (host).
Según él,
pocas empresas requieren actualmente en sus redes
que los sistemas se comuniquen directamente entre sí.
Los servicios en la nube han adquirido una relevancia especial
en los últimos tiempos,
y las aplicaciones empresariales no tienen por qué ser
[*on-premises*](https://en.wikipedia.org/wiki/On-premises_software).
La implementación del aislamiento ayuda enormemente a prevenir ataques
en los que los *hackers* podrían acceder a los recursos
de red del cliente y explotarlos.
Ben concluye esta parte mencionando que sin acceso entre dispositivos,
¿cómo se supone que alguien va a encontrar
y explotar servidores o estaciones de trabajo no parcheados en una red?
¿Cómo se supone que alguien va a desplazarse lateralmente?
¿Cómo se supone que va a transmitir credenciales
o acceder a un directorio compartido
[SMB](https://es.wikipedia.org/wiki/Server_Message_Block)
fraudulento?

## ¡Eso es todo, amigos!

Todos estos fueron consejos e ideas bastante simples pero valiosos,
extraídos de lo que Benjamin Donnelly compartió
en *Tribe of Hackers Red Team*.
Espero que te hayan gustado.
Si quieres saber más sobre nuestro *red team* en Fluid Attacks,
no dudes en [contactarnos](../../contactanos/).
