---
slug: blog/metrica-exposicion-al-riesgo-cvssf/
title: Lo que le falta a tu gestión de riesgos
date: 2024-11-29
subtitle: ¿Por qué calcular riesgos de ciberseguridad con nuestra métrica CVSSF?
category: filosofía
tags: ciberseguridad, riesgo, empresa
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1668626573/blog/cvssf-risk-exposure-metric/cover_cvssf.webp
alt: Foto por Maxim Hopman en Unsplash
description: Exponemos algunos de los defectos de la medida tradicional del riesgo en ciberseguridad y presentamos CVSSF, la métrica basada en la exposición al riesgo que nos permite superarlos.
keywords: Cvss, Cvssf, Exposicion Al Riesgo, Gestion Del Riesgo, Gestion Del Riesgo En Ciberseguridad, Exito En Ciberseguridad, Estatus De Seguridad, Hacking Etico, Pentesting
author: Jason Chavarría
writer: jchavarria
name: Jason Chavarría
about1: Escritor y editor
source: https://unsplash.com/photos/HBkuFoJuwDI
---

Los incidentes de ciberseguridad van en aumento,
lo que en parte hace evidente la falla en el enfoque tradicional
que guía la forma de [priorizar la gestión de riesgos](../que-es-gestion-de-vulnerabilidades/).
Por ello,
las organizaciones de todo el mundo necesitan urgentemente
alternativas que brinden información útil que les ayude
a tomar mejores decisiones en cuanto a la ciberseguridad.
Con el fin de contribuir a resolver este problema,
proponemos nuestra **métrica CVSSF**.
Se trata de un valor basado en la exposición al riesgo
que permite a las organizaciones mejorar la priorización
de vulnerabilidades para su remediación.
Y principalmente,
les proporciona una medida más precisa de su estatus de seguridad.
En este artículo hablaremos de las motivaciones detrás de esta métrica
y de la evidencia de su valor.

## Fallas de la medición tradicional del riesgo en ciberseguridad

Las ciberamenazas para las organizaciones de todo el mundo siguen creciendo.
El [número de dispositivos conectados a la Internet](../../../blog/trend-forecast-for-2024/)
es cada vez mayor.
Por lo tanto,
las superficies de ataque de las organizaciones
(es decir, los puntos de exposición
de los sistemas informáticos a fuentes no fiables)
son mayores,
junto con las posibilidades de ser vulneradas.
La [explotación de vulnerabilidades críticas](../../../blog/what-trends-to-expect-for-2023/)
individuales en _software_ de terceros amenaza
con perturbar seriamente las cadenas de suministro.
Los atacantes siguen poniendo en peligro
un [gran número de industrias](https://blog.checkpoint.com/research/check-point-research-reports-highest-increase-of-global-cyber-attacks-seen-in-last-two-years-a-30-increase-in-q2-2024-global-cyber-attacks/).
Y todo ello mientras el costo promedio
de una brecha de datos sigue batiendo récords.

Las amenazas anteriores son cosas con las que toda organización
tiene que lidiar en su camino hacia la creación de valor para sus clientes.
En resumen, **aspirar al éxito es una empresa arriesgada**.
De hecho,
la gestión del riesgo es posiblemente la cuestión más importante
a resolver en ciberseguridad.
En consecuencia,
los estándares y marcos de fuentes reconocidas (p. ej., [NIST](../../../blog/nist-supply-chain-risk/),
[ISO](../../../blog/iso-iec-27002-2022/))
dan a las empresas una buena idea de los controles para mitigar el riesgo.
Sin embargo,
la gran cantidad de ciberataques con éxito sugiere
que la forma en que las compañías están priorizando la mitigación
del riesgo no está funcionando.

¿Cuál es el enfoque erróneo de la gestión de riesgos en ciberseguridad?
Bueno,
la mayoría de las organizaciones utiliza puntuaciones
y mapas de riesgo de estándares conocidos.
Este enfoque tradicional, por supuesto,
tiene en cuenta que las vulnerabilidades difieren según el grado
en que afectarían a un sistema si fueran explotadas.

Un indicador es la puntuación en **Common Vulnerability Scoring System**
([CVSS](https://www.first.org/cvss/specification-document))
utilizada extensamente para comunicar las características
y la severidad de las vulnerabilidades.
Básicamente,
cuantifica la explotabilidad,
el alcance y el impacto de la vulnerabilidad.
Esa es la puntuación de base,
que se ajusta posteriormente mediante variables temporales y del entorno.
El resultado es un **valor de severidad entre 0,0 y 10,0**.
Y lo que es más importante,
este rango se desglosa en una escala cualitativa
de calificación de la gravedad,
en la que 0,0 = ninguna;
0,1 - 3,9 = baja;
4,0 - 6,9 = media;
7,0 - 8,9 = alta,
y 9,0 - 10,0 = crítica.

Los productos de escaneo de vulnerabilidades suelen proporcionar
las puntuaciones CVSS de las vulnerabilidades detectadas.
Se supone que esta información ayuda a organizaciones a reducir
su incerteza frente al estado de su seguridad y qué remediar primero.

Normalmente,
para hacerse una idea del estado de seguridad,
lo que viene a la mente es la agregación del número de vulnerabilidades
y sus puntuaciones CVSS.
Esto plantea una dificultad.
Supongamos que tenemos diez vulnerabilidades
con una puntuación de 1,0 cada una
en nuestra aplicación X
y una vulnerabilidad con una puntuación de 10,0 en nuestra aplicación Y.
La primera no tendría un riesgo mayor,
ni siquiera el mismo,
por tener más problemas de seguridad que,
o igualar en puntuación sumada a,
la segunda.
Es la aplicación Y la que debe ser priorizada
para la remediación de vulnerabilidades.
Además,
el éxito en la remediación de las diez vulnerabilidades de la aplicación X
no debería considerarse tan valioso
como la remediación de ese único problema crítico en la aplicación Y.

<div class="imgblock">

![Gestión de riesgo CVSSF - Fluid Attacks](https://res.cloudinary.com/fluid-attacks/image/upload/v1668627114/blog/cvssf-risk-exposure-metric/cvssf-risk-management-fluid-attacks.webp)

<div class="title">

Diez vulnerabilidades de CVSS 1,0 no equivalen
a una vulnerabilidad de CVSS 10,0.

</div>

</div>

Aquí es donde la agrupación de vulnerabilidades en sus rangos de severidad
parece útil.
Suponiendo que nuestras dos aplicaciones tengan muchas más vulnerabilidades
con diferentes puntuaciones CVSS,
podríamos llevar un recuento de,
digamos, vulnerabilidades de severidad baja, media, alta y crítica
que trataríamos de abordar por separado.
Este enfoque daría una visión complicada
del estado de seguridad de cada sistema,
ya que no estaría unificado.
Esto se podría tolerar.
Pero una falla básica es que la segmentación
de las puntuaciones CVSS en niveles cualitativos es arbitraria.
Por ejemplo,
una vulnerabilidad se considera crítica
solo si se le ha dado una puntuación de al menos 9,0,
por lo que una que recibe una puntuación de 8,9
se etiqueta como de severidad alta.
Esa mera diferencia de 0,1 es la que supondría la priorización de la primera,
mientras que ambas podrían estar amenazando al sistema
con un grado de riesgo bastante similar.
Peor aún,
se podría considerar que esta última tiene la misma prioridad
que otra vulnerabilidad con una puntuación de 7,0,
ya que ambas forman parte del rango de severidad alta.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/gestion-vulnerabilidades/"
title="Inicia ahora con la solución de gestión de vulnerabilidades
de Fluid Attacks"
/>
</div>

## La métrica de Fluid Attacks basada en la exposición al riesgo

En Fluid Attacks reconocemos las limitaciones del enfoque tradicional.
Por eso proponemos una nueva métrica
para contribuir a medir —mayormente a reportar—
el riesgo de forma más precisa,
permitiendo a las organizaciones mejorar su percepción
de la exposición al riesgo de sus sistemas
(es decir, el grado en que un sistema es vulnerable a ciberataques exitosos)
y la priorización de vulnerabilidades para su remediación.
La llamamos métrica CVSSF,
ya que toma la puntuación base CVSS
y la ajusta para mostrar la naturaleza exponencial de la severidad.
"F" significa "a la Fluid Attacks".
La fórmula es la siguiente:

<div class="imgblock">

![Fórmula de riesgo CVSSF - Fluid Attacks](https://res.cloudinary.com/fluid-attacks/image/upload/v1668629343/blog/cvssf-risk-exposure-metric/cvssf-risk-formula-fluid-attacks.webp)

</div>

En esta fórmula,
la constante P significa "proporcionalidad".
Determina cuántas vulnerabilidades con una puntuación base
en CVSS equivalen a una vulnerabilidad con una unidad
más de puntuación base en CVSS.
Esto puede determinarlo cada organización.
Pero ¿qué han observado nuestros expertos en seguridad?
Han llegado a un consenso para dar a P un valor de 4.
Se puede entender de la siguiente manera:
"Cuatro vulnerabilidades de una puntuación base CVSS
de 9,0 equivalen a una de 10,0".
En cuanto a la constante R, que significa "rango",
restringe los valores del CVSSF a los que la organización
se sienta cómoda utilizando.
De nuevo, recomendamos utilizar el 4.

<div class="imgblock">

![Fórmula de riesgo CVSSF con valores - Fluid Attacks](https://res.cloudinary.com/fluid-attacks/image/upload/v1668629362/blog/cvssf-risk-exposure-metric/cvssf-risk-formula-with-values-fluid-attacks.webp)

</div>

Nuestros ajustes de los valores CVSS con nuestra fórmula CVSSF
nos permiten proporcionar más o mejor visibilidad a las vulnerabilidades
más riesgosas de un sistema.
Utilizando la balanza ilustrada anteriormente como ejemplo,
esas diez vulnerabilidades de la izquierda en conjunto
equivalen a un CVSSF de 0,2.
Mientras tanto,
la única vulnerabilidad de la derecha tiene un CVSSF de 4.096,0.
¡Una diferencia asombrosa!

## Beneficios del CVSSF

Es posible que las vulnerabilidades de mayor riesgo
no aparezcan con tanta frecuencia como las de severidad baja o media,
por lo que cuando se agregan todas siguiendo el enfoque tradicional,
la importancia de las más riesgosas podría quedar eclipsada.
Cuando las organizaciones utilizan el CVSSF,
pueden ver cómo se incrementa su exposición al riesgo agregado
de forma más drástica de lo que lo haría en caso
de que se notificara una vulnerabilidad
de severidad alta o crítica en sus sistemas.

Un ejemplo:
Nuestro [Reporte de ataques 2024](https://fluidattacks.docsend.com/view/x6dm4rv2ciu2taae)
documentó que el 94,1% de las vulnerabilidades que detectamos
en los sistemas de nuestros clientes
eran de una severidad baja o media en CVSS.
**Sin embargo,
fue el 5,9% restante
el que generó el 94,9% de la exposición al riesgo reportada**.
Se trató exclusivamente de vulnerabilidades de severidad alta o crítica
en CVSS.
Por tanto,
está claro que lo que más debería importar a las organizaciones
no es el número de vulnerabilidades,
sino la exposición al riesgo que estas representan.

Para ilustrar aún mejor este último punto,
veamos el caso de un informe en la sección Analytics de nuestra plataforma
para una organización de demostración.
El Gráfico 1 nos muestra la gestión de vulnerabilidades a lo largo del tiempo,
mientras que el Gráfico 2 nos muestra la gestión de la exposición al riesgo
(CVSSF) a lo largo del tiempo:

<div class="imgblock">

![Gráfico de vulnerabilidades](https://res.cloudinary.com/fluid-attacks/image/upload/v1732919915/blog/cvssf-risk-exposure-metric/vulnerabilities_over_time_cvssf.webp)

<div class="title">

Gráfico 1. Gestión de vulnerabilidades en el tiempo.

</div>

</div>

<div class="imgblock">

![Gráfico de exposición al riesgo](https://res.cloudinary.com/fluid-attacks/image/upload/v1732919915/blog/cvssf-risk-exposure-metric/exposure_over_time_cvssf.webp)

<div class="title">

Gráfico 2. Gestión de exposición al riesgo en el tiempo.

</div>

</div>

Aunque la mayoría de las barras tienen un comportamiento similar
a sus homólogas en el otro gráfico,
en el quinto informe (**2021-12**) se produce algo a destacar.
La diferencia entre los porcentajes de vulnerabilidades abiertas
y de exposición al riesgo abierta
es la más pronunciada.
Podemos deducir inmediatamente que aunque el número total de vulnerabilidades
abiertas para esa organización
es relativamente bajo (**21,5%**),
estas representan una alta exposición total al riesgo
en ese momento (**61,6%**).
Por lo tanto,
centrarse únicamente en la cantidad de vulnerabilidades
para dar cuenta del estado de seguridad de esa organización
sería un error garrafal.

Así pues,
las organizaciones que utilizan el CVSSF pueden
**priorizar rápidamente la remediación** de las vulnerabilidades
que representan una mayor exposición al riesgo.
Si tienen éxito,
esto haría que la exposición al riesgo agregada disminuya significativamente.
Además,
su incerteza sobre el estado de su seguridad puede reducirse enormemente,
ya que tienen a su disposición un **valor agregado más fiable**
en el que pueden **basar sus objetivos de éxito en ciberseguridad**,
y que pueden **utilizar para predecir posibles pérdidas**.

## Fluid Attacks detecta la exposición al riesgo en tu sistema

En Fluid Attacks,
ofrecemos [Hacking Continuo](../../servicios/hacking-continuo/)
para ayudar a nuestros clientes a conocer y mejorar el estado de seguridad
de sus aplicaciones a lo largo del ciclo de vida de desarrollo de *software*.
Para ello,
aprovechamos las pruebas de seguridad de forma temprana y continua
mediante una combinación de automatización y trabajo manual
de nuestros *pentesters*,
un enfoque que no solo sirve para detectar vulnerabilidades,
sino también para remediarlas.

Hemos comprobado que la implementación de nuestras pruebas
de seguridad manuales
(p. ej., [*pentesting* como servicio, PTaaS](../../soluciones/pruebas-penetracion-servicio/))
tiene un valor incalculable.
Durante 2023,
nuestros *pentesters* reportaron **más del 97%** de las vulnerabilidades críticas
y el **71,4%** de la exposición al riesgo total
atribuida a vulnerabilidades en los sistemas de nuestros clientes.
Si los clientes dependieran únicamente de herramientas automatizadas,
su gestión de vulnerabilidades basada en riesgos se vería comprometida.

¿Quieres que evaluemos tus aplicaciones?
En nuestra [plataforma](../../plataforma/)
puedes ver la exposición al riesgo agregada,
así como su gestión, a lo largo del tiempo,
y compararla con la de las organizaciones con mejores resultados.
Desde allí,
también puedes asignar tareas de remediación
a los desarrolladores de tu equipo,
obtener sugerencias de remediación a partir de IA generativa,
pedir asesoramiento a nuestros *pentesters*
para problemas más complejos
y solicitar reataques para verificar la efectividad
de tus esfuerzos de remediación.

Inicia hoy mismo tu [prueba gratuita de 21 días](https://app.fluidattacks.com/SignUp)
del plan Essential de nuestro Hacking Continuo.

> **Nota:**
> Para una presentación resumida de nuestra métrica CVSSF,
> descarga el [_white paper_](https://fluidattacks.docsend.com/view/4r7qehi7a35ndbqx).
