---
slug: blog/10-mayores-ataques-ransomware/
title: Los 10 mayores ataques de ransomware
date: 2024-03-21
subtitle: Hechos históricos que te incitarán a actuar
category: ataques
tags: ciberseguridad, empresa, tendencia, riesgo, software
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1711043923/blog/10-biggest-ransomware-attacks/cover_biggest_ransomware_attacks_1.webp
alt: Foto por Sebastian Pociecha en Unsplash
description: En este artículo hablamos de los mayores y más impactantes ataques de ransomware de la historia que enfatizan la necesidad de tomar medidas de ciberseguridad.
keywords: Ransomware, Servicio, Raas, Rescate, Vulnerabilidad, Software, Malware, Codigo Malicioso, Criptodivisa, Ciberseguridad, Hacking Etico, Pentesting
author: Wendy Rodriguez
writer: wrodriguez
name: Wendy Rodriguez
about1: Escritor y editor
source: https://unsplash.com/photos/silhouette-of-people-at-night-8_kfXcUieg4
---

"Troyano AIDS" suena muy amenazador, ¿no?
Ese es el nombre del *malware* utilizado en el primer ataque de
[*ransomware*](../../../blog/ransomware/) hace más de 30 años.
En 1989, ese troyano, creado por el biólogo Joseph Popp,
se distribuyó a través de disquetes.
Este encriptaba los nombres de los archivos del computador y,
después de que el usuario reiniciara su máquina 90 veces,
aparecía una ventana con instrucciones sobre qué hacer
para conseguir un instrumento descodificador.
El mensaje incluía el monto del rescate
y la dirección a la que enviar un cheque o giro postal.
Este intento de extorsión a través de *software* malicioso
ya existía antes de la llegada del Internet.
Ahora que Internet forma parte de nuestra rutina diaria,
el *ransomware* se ha convertido en una amenaza constante.

La empresa de seguimiento de criptomonedas
[Chainalysis](https://www.chainalysis.com/blog/ransomware-2024/)
ha informado que en 2023 se alcanzó un récord en pagos por *ransomware*.
Las extorsiones por ataques de *ransomware* superaron
los 1.000 millones de dólares el año pasado,
a pesar de que agencias de seguridad como
el FBI (Federal Bureau of Investigation)
y entidades gubernamentales como CISA
(Cybersecurity and Infrastructure Security Agency)
recomiendan no realizar el pago.
Pagar rescates no es el único aspecto a tener en cuenta cuando
se ejecutan ataques de *ransomware*.
Las empresas tienen que lidiar con clientes afectados,
pérdida de datos, disminución de ingresos por tiempo fuera de línea,
reputación perjudicada, demandas judiciales y,
como veremos en este *post*, algunas empresas nunca se recuperan.
Teniendo en cuenta el método de ejecución del *ransomware*,
las consecuencias de la explotación
y las reacciones de las organizaciones afectadas,
hemos recopilado los diez mayores ataques de *ransomware*
que se han registrado hasta la fecha.

## 10 - Universidad de California

La Universidad de California en San Francisco,
o UCSF, fue objeto de un ataque de *ransomware* en el 2020.
Este ataque afectó a varios servidores del sistema informático
de la facultad de medicina.
[Según BBC News](https://www.bbc.com/news/technology-53214783),
el *ransomware* utilizado para el ataque fue **NetWalker**,
que encriptó y extrajo todos los datos que incautó
para utilizarlos posteriormente en la fase de negociación.
Aunque pudieron contener el ataque dentro
de los sistemas informáticos de la facultad de medicina
y no afectó a la investigación COVID-19 ni a la atención a los pacientes,
los directivos de la UCSF declararon
que aún así se vieron obligados a pagar el rescate debido
a la importancia de los datos cifrados
para continuar la investigación académica en la facultad de medicina.
Se pagaron aproximadamente **1,14 millones de dólares**
en criptomoneda para recuperar el acceso a los datos cifrados.

A lo largo de su actividad,
los ciberdelincuentes de NetWalker atacaron diferentes instituciones,
incluidas otras universidades,
y utilizaron siempre el mismo *modus operandi*.
A través de correos electrónicos de spam
o de [*phishing*](../../../blog/phishing/),
obtenían acceso no autorizado a los sistemas
y cifraban todos los datos posibles que encontraban.
En este caso, los atacantes aprovecharon los datos robados,
publicándolos en su blog como prueba de sus acciones,
y forzando así a la universidad a pagar el rescate.

## 9 - Ciudad de Dallas

[En mayo 2023](https://www.techtarget.com/searchsecurity/news/366553259/Dallas-doles-out-85M-to-remediate-May-ransomware-attack),
esta ciudad reveló una destinación de **8,5 millones de dólares**
para esfuerzos de restauración y remediación
después de que un grupo identificado como **Royal** se infiltrara
y afectara a menos de 200 de los sistemas informáticos de la ciudad.
Una cuenta de dominio de servicio básico conectada
a los servidores de la ciudad fue intervenida
por los atacantes de Royal,
que luego utilizaron tecnologías de pruebas de penetración
y herramientas autorizadas para gestión remota de terceros
con el fin de ejecutar un movimiento lateral.
Se informó que mediante los focos de mando
y control previamente desplegados,
el grupo pudo moverse por la red
de la ciudad semanas antes de lanzar el ataque,
el cual encriptó los datos de los servidores de la ciudad.

El ataque provocó interrupciones
o retrasos en el sitio web del Departamento de Policía de Dallas,
en los pagos en línea de los servicios municipales,
en los servicios de alerta de los bomberos de Dallas
y en los sistemas judiciales de la ciudad.
También se robaron datos confidenciales
(números de seguro social e información médica privada),
a lo que la ciudad se limitó a enviar cartas a los afectados.
No hace falta decir que los afectados siguen buscando algún tipo
de compensación o solución por parte de la ciudad.

## 8 - Kaseya

El creador de *software* Kaseya fue víctima
de un ataque de *ransomware* complejo
[en julio de 2021](https://www.csoonline.com/article/571081/the-kaseya-ransomware-attack-a-timeline.html).
La compañía es conocida por proveer MSPs
(proveedores de servicios gestionados)
y desarrollar administradores de sistemas/servidores virtuales (VSAs).
En el ataque,
los ciberdelincuentes explotaron vulnerabilidades de día cero en
el *software* [*on-premises*](../seguridad-nube-on-premises/) de Kaseya VSA,
lo que les permitió eludir la autenticación y distribuir *ransomware*
a los clientes de Kaseya con el fin de encriptar
los archivos de los sistemas afectados.
El ataque provocó una interrupción generalizada de los servicios,
y se estima que unas 1.500 organizaciones de diferentes sectores
se vieron afectadas por el *ransomware*.

La organización criminal **REvil**
(también conocida como Sodinokibi)
perpetró este ataque y pidió inicialmente **70 millones de dólares**
para obtener un descifrador universal.
Kaseya se negó a pagar y rápidamente reaccionó,
desactivando sus servidores VSA e indicando a todos sus clientes
que apagaran sus propios servidores VSA
hasta que los parches estuvieran disponibles,
lo que ocurrió un par de días después del ataque.
El incidente evidenció la creciente tendencia del *ransomware*
que tiene como objetivo a los proveedores de servicios gestionados
y, por efecto, a sus clientes.

## 7 - JBS Foods

**REvil** también llevó a cabo
otro importante ataque de *ransomware* en 2021,
esta vez contra una de las empresas de procesamiento
de carne más grandes del mundo,
[JBS Foods](../../../blog/jbs-revil-cyberattack/).
En este ataque se infiltraron en la red
de la empresa con credenciales filtradas
de un ataque anterior
(se extrajeron [5 TB of data](https://securityscorecard.com/blog/jbs-ransomware-attack-started-in-march/)
de datos durante tres meses).
Posteriormente, REvil desplegó un *ransomware*
que encriptó los datos e interrumpió la producción
en varias de las instalaciones de procesamiento
de carne de JBS alrededor del mundo.

Como resultado,
la empresa tuvo que cerrar sus operaciones
y finalmente cedió al rescate,
pagando **11 millones de dólares** para obtener una clave de descifrado.

## 6 - Kronos

El ataque de *ransomware* de diciembre de 2021
a la empresa de gestión de recursos humanos Kronos
( anteriormente conocida como Kronos, ahora Ultimate Kronos Group o UKG)
tuvo como objetivo su servicio Kronos Private Cloud.
Este servicio basado en la nube era utilizado
por muchas empresas para gestionar asuntos como pagos,
datos de asistencia y horas extra.
En el momento de publicar este artículo,
**la identidad de los atacantes aún no ha sido confirmada**,
pero se sabe que robaron los datos de los clientes
y solicitaron un pago a la empresa.
Después de que UKG accediera a las demandas de los atacantes,
se supo que la filtración de datos afectó a más de 8.000 organizaciones,
incluidos hospitales, fábricas y pequeñas empresas
que confiaban en UKG para las nóminas
y la asignación de horarios de los empleados.
Aparentemente, este ataque de *ransomware* se relaciona
con el troyano bancario Kronos de 2014,
en el que el [código malicioso](../../learn/codigo-malicioso/)
se dirigía a las sesiones del navegador
para obtener credenciales de inicio de sesión de forma ilegal
mediante una combinación de inyecciones web
y *keylogging* (registro de pulsaciones).
Los detalles técnicos de este ataque nunca
se hicieron públicos; sin embargo,
se informó de que UKG pagó una **cantidad desconocida** a los atacantes.

Las repercusiones legales
de este ataque se han dejado sentir tiempo después.
El ataque dio lugar a demandas de las empresas afectadas
en busca de compensación por daños y perjuicios,
y en julio de 2023,
UKG llegó a un acuerdo de 6 millones de dólares
con los empleados afectados de esas empresas.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/gestion-vulnerabilidades/"
title="Inicia ahora con la solución de gestión de vulnerabilidades
de Fluid Attacks"
/>
</div>

## 5 - Colonial Pipeline

Considerado una "amenaza para la seguridad nacional"
por el gobierno de Joe Biden,
[este](../../../blog/pipeline-ransomware-darkside/) ataque
de *ransomware* de 2021 fue un incidente disruptivo
que afectó al suministro de combustible a lo largo
de la costa este de Estados Unidos.
Colonial Pipeline,
uno de los mayores y más importantes proveedores de combustible del país,
transporta gasolina, diesel,
combustible para aviones y combustible para calefacción doméstica
desde Texas hasta la región noreste.

El ataque fue ejecutado por el grupo malicioso
conocido como **DarkSide**,
que obtuvo acceso no autorizado a través
de una contraseña expuesta de una cuenta VPN
(reutilización de [contraseñas](../../../blog/pass-cracking/)).
Los atacantes desplegaron un *ransomware*
que cifró los datos de Colonial Pipeline
y exigió el pago de un rescate en criptomoneda
a cambio de una clave de descifrado.
La empresa mitigó el impacto apagando sus sistemas,
lo que causó interrupciones en el suministro de combustible,
provocando compras desesperadas y escasez de combustible,
así como una subida de precios.
La empresa acabó pagando el rescate.
Aproximadamente **4,4 millones de dólares**,
para luego restablecer el sistema;
con la ayuda del Departamento de Justicia,
se recuperó más de la mitad del pago.

## 4 - Travelex

Como se vio anteriormente en este artículo,
**REvil** estuvo involucrado en algunos de los ataques
más lucrativos de los últimos años.
En diciembre de 2019,
la entonces empresa líder mundial en cambio de divisas, Travelex,
sufrió un gran ataque que aprovechaba una vulnerabilidad
en los servidores VPN Pulse Secure de la compañía.
El *ransomware* Sodinokibi provocó la paralización
del sistema informático de la empresa y cifró los datos,
dejando a Travelex sin poder acceder a sus archivos.
No se puede decir que la culpa sea únicamente del proveedor Pulse Secure.
Ellos habían identificado
y aplicado un parche a la vulnerabilidad en abril de 2019,
pero Travelex no había implementado el parche en sus servidores,
lo que dejó una ventana abierta
para los cazadores de vulnerabilidades como REvil.

El ataque dañó gravemente y para siempre a Travelex.
Aunque los atacantes pidieron un rescate de 6 millones de dólares,
la compañía acabó pagando **2,3 millones**.
También consiguió recuperar el acceso a sus datos.
Sin embargo,
Travelex tuvo problemas con el sistema
y estuvo fuera de línea durante casi dos semanas.
Después de fallarle a socios como bancos
y cadenas de supermercados,
y debido a la amenaza de una investigación del
Reglamento General de Protección de Datos
([GDPR](../../../compliance/gdpr/))entre otras luchas financieras,
Travelex se vio obligada a liquidar en 2020.

## 3 - Gobierno de Costa Rica

[En abril de 2022](../../../blog/conti-gang-attacked-costa-rica/),
un ciberataque contra el gobierno de Costa Rica
fue ejecutado tan ferozmente que fue declarado "emergencia nacional".
Los atacantes lograron penetrar primero el Ministerio de Hacienda,
encriptando archivos y paralizando dos sistemas importantes:
el servicio digital de impuestos y el sistema informático de control aduanero.
El grupo de *ransomware* Conti se atribuyó la autoría
de este ataque y exigió un rescate de **10 millones de dólares**
a cambio de devolver los datos de los contribuyentes
y no atacar a otras entidades gubernamentales.
Sin embargo, el gobierno costarricense se negó a pagar el rescate,
lo que provocó que otras instituciones se vieran afectadas.
El Ministerio de Ciencia, Innovación, Tecnología y Telecomunicaciones
y el Ministerio de Trabajo y Seguridad Social,
así como la Caja Costarricense de Seguro Social.
Posteriormente,
se subieron al sitio web de Conti 672 GB de archivos robados.

Se cree que el ataque de Conti está relacionado
con el segundo ataque perpetrado
por el grupo de operaciones
de [*ransomware* como servicio](../../../blog/ransomware-as-a-service/)
**Hive**.
Este tuvo como objetivo los sistemas de salud de Costa Rica,
a los que afectó obligando a cerrar la Historia Clínica Digital Única
y el Sistema Centralizado de Recaudación.
Al igual que en el primer ataque,
el gobierno se negó a pagar el rescate de **5 millones de dólares**.
Recuperarse de los ataques llevó tiempo y recursos,
recibiendo ayuda de Microsoft y de los gobiernos
de Estados Unidos, Israel y España
para restablecer los servicios de Costa Rica.

## 2 - Gobierno de Ucrania

Los ataques **NotPetya**,
que se produjeron en 2017,
afectaron a varios países de todo el mundo,
pero uno de los ataques se dirigió
a Ucrania supuestamente por motivaciones políticas.
NotPetya tenía un par de similitudes con el virus Petya de 2016,
y empleaba las mismas tácticas que el notorio ataque WannaCry,
explotando dispositivos sin parches,
propagándose por las redes y cifrando datos.
Este ataque sobrescribia el registro de arranque maestro (MBR)
con cargas maliciosas e inutilizaba las máquinas infectadas.
Sin embargo, no se trataba de un *ransomware* en sí.
Los mensajes de NotPetya en los que se pedía
un rescate eran falsos ya que no había posibilidad real
de obtener una clave de descifrado ni siquiera después
de efectuar los pagos.
Sin una clave de descifrado,
los datos quedaron irreversiblemente cifrados,
los archivos fueron irrecuperables
y se produjeron daños permanentes.

Organismos gubernamentales,
empresas e infraestructuras críticas ucranianas quedaron interrumpidos.
Las investigaciones posteriores de organismos gubernamentales de Estados Unidos,
Reino Unido y otros países
[atribuyeron](https://www.washingtonpost.com/world/national-security/russian-military-was-behind-notpetya-cyberattack-in-ukraine-cia-concludes/2018/01/12/048d8506-f7ca-11e7-b34a-b85626af34ef_story.html)
el ataque al ejército ruso, concretamente a la GRU
(inteligencia militar rusa).
Las investigaciones apuntaron a las tensiones geopolíticas
entre Rusia y Ucrania,
las cuales se siguen viendo hoy en día.

## 1 - WannaCry

En mayo de 2017,
el ataque de *ransomware* WannaCry fue noticia
por ser uno de los ciberataques más extensos
y notorios de la historia, ya que afectó a organizaciones
e individuos de todo el mundo.
Aparentemente,
la banda de *hackers* maliciosos **Lazarus Group** perpetró el ataque,
que tuvo como objetivo computadores con el sistema operativo Microsoft Windows
y explotó una vulnerabilidad con el *hack* denominado EternalBlue,
el cual había sido robado y filtrado por el grupo The Shadow Brokers.
Meses antes del ataque,
Microsoft había publicado correcciones para solucionar esa vulnerabilidad,
pero muchas organizaciones y particulares no actualizaron
ni aplicaron los parches, lo que les dejó expuestos al riesgo.

Alrededor de 230.000 equipos de más de 150 países
se vieron afectados pocos días después de la publicación de WannaCry.
El impacto se sintió en gran medida en organizaciones
como la empresa española de telecomunicaciones Telefónica,
donde los equipos infectados mostraban una ventana emergente
que exigía el pago a través de divisas digitales.
El servicio nacional de salud del Reino Unido también
fue víctima de WannaCry,
los hospitales y centros de salud se vieron obligados a cancelar citas
y desviar pacientes debido a que los sistemas informáticos
estaban bloqueados por el *ransomware*.
Los *hackers* también explotaron objetivos más pequeños,
cifrando archivos de computadores individuales
para pedir entre 300 y 600 dólares en criptomoneda
para liberar los archivos.
La empresa de ciberriesgos Cyence calculó en su momento
que la pérdida estimada por el ataque era de unos
[4.000 millones de dólares](https://www.cbsnews.com/news/wannacry-ransomware-attacks-wannacry-virus-losses/).

Después de leer este*post*,
nadie te culparía por correr hacia tu computador,
apagarlo y no volver a usarlo nunca más.
Pero esperamos que estas historias de terror,
por escalofriantes que sean,
sirvan como ejemplo para hacer conciencia y motivar a tomar medidas
y estar mejor preparados para los ataques.
Para las empresas, recomendamos nuestra solución
[Hacking Continuo](../../servicios/hacking-continuo/),
que combina herramientas automatizadas,
IA y expertos en *hacking* para ampliar tus capacidades
frente al campo minado de la ciberseguridad.
[Contáctanos ya](../../contactanos/).
