---
slug: blog/cumplimiento-pruebas-de-penetracion/
title: Cumplimiento que pide pentesting
date: 2023-01-19
subtitle: Qué estándares de seguridad requieren pentesting
category: política
tags: pentesting, cumplimiento, ciberseguridad, empresa
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1674158363/blog/penetration-testing-compliance/cover_compliance.webp
alt: Foto por Nik Shuliahin en Unsplash
description: "Exponemos si las pruebas de penetración son necesarias para el cumplimiento de estas normas de seguridad: GDPR, GLBA, HIPAA, ISO 27001, PCI DSS, SOC 2 y SWIFT CSCF."
keywords: Soc 2 Pruebas De Penetracion, Iso Pruebas De Penetracion, Glba Pruebas De Penetracion, Gdpr Pruebas De Penetracion, Hipaa Pruebas De Penetracion, Pci Dss Pruebas De Penetracion, Cumplimiento, Hacking Etico, Pentesting
author: Jason Chavarría
writer: jchavarria
name: Jason Chavarría
about1: Escritor y editor
source: https://unsplash.com/photos/JOzv_pAkcMk
---

La necesidad de proteger la información sensible ha dado lugar
a regulaciones en todas las industrias.
Algunas de ellas exigen
realizar [pruebas de penetración](../que-es-prueba-de-penetracion-manual/).
Este enfoque ofensivo,
que imita el comportamiento de atacantes malintencionados,
puede arrojar resultados muy precisos en las pruebas de seguridad.
En este artículo, exponemos si las pruebas de penetración
son exigidas por los estándares internacionales
sobre los cuales la mayoría de personas se plantea esta pregunta.
Sin embargo,
destacamos la importancia de no limitarse al cumplimiento básico.

## ¿Qué son las pruebas de penetración?

Las pruebas de penetración (o pruebas de penetración "manuales")
son un enfoque de evaluación de la seguridad
que consiste en simular escenarios de ataque actuales
para comprobar la capacidad de defensa de los sistemas informáticos.
Este enfoque solo es posible con intervención humana,
ya que son los profesionales de la seguridad,
los *pentesters*, quienes conocen el comportamiento de los atacantes.
Así,
el *pen testing* va más allá de las evaluaciones de vulnerabilidades
(también conocidas como escaneo de vulnerabilidades
o análisis de vulnerabilidades) realizadas únicamente mediante herramientas.
Utilizar únicamente la automatización conduce
a altas tasas de falsos positivos y falsos negativos,
lo que añade carga administrativa a los equipos técnicos de las organizaciones.
Por lo tanto,
cuando se trata de precisión y ahorro de gastos,
el *pen testing* es la opción más ventajosa.

## Pruebas de penetración para el cumplimiento con regulaciones

### ¿Deben hacerse pruebas de penetración para cumplir con GDPR?

El [reglamento general de protección de datos](https://gdpr-info.eu/)
(GDPR, por sus siglas en inglés)
establece normas relacionadas con la protección de datos
y privacidad de las personas naturales dentro de la Unión Europea (UE)
y el Espacio Económico Europeo (EEE).
Esto se aplica incluso a las empresas que,
operando fuera de esos territorios, almacenan,
procesan o transfieren información personal de ciudadanos de la UE y del EEE.

Este estándar exige a las empresas que apliquen medidas técnicas
y organizativas para garantizar un nivel de seguridad
de datos acorde con sus riesgos.
Además, obliga a comprobar periódicamente la eficacia de tales medidas.
Aunque las pruebas de penetración no son obligatorias según el GDPR,
se trata de un enfoque que, simulando ataques del mundo real,
puede evaluar con precisión la confidencialidad, integridad,
disponibilidad y resistencia de los sistemas.
Por ejemplo, algunos requisitos básicos de los sistemas,
como especificar la finalidad de la recolección de datos
y respetar las preferencias de rastreo,
no son validados por herramientas automatizadas.

En Fluid Attacks
comprobamos que tus sistemas cumplan los requisitos de seguridad vinculados
al [GDPR](https://help.fluidattacks.com/portal/en/kb/articles/criteria-compliance-gdpr).
Entre ellos se incluyen la gestión eficaz de los datos
y los controles de privacidad.

### ¿Deben hacerse pruebas de penetración para cumplir con GLBA?

[Desde diciembre del 2022](https://www.federalregister.gov/documents/2021/12/09/2021-25736/standards-for-safeguarding-customer-information),
**las instituciones financieras están obligadas
a realizar pruebas de penetración** anualmente.
Esto es de acuerdo con la regulación final
de la Comisión Federal de Comercio de EE. UU.
sobre las normas para proteger la información de los clientes.
La Comisión estableció estas normas en virtud
de la Ley Gramm Leach Bliley (GLBA),
que busca regular las prácticas de privacidad
y seguridad de los datos de las instituciones del sector financiero.

Como sugiere el nombre de la normativa,
se exige especialmente a las entidades que demuestren
la eficacia de sus controles de protección
de la información de los clientes.
Una cosa que cabe mencionar es que la definición
de sistemas de información en la normativa incluye los sistemas físicos
y los empleados. Por lo tanto,
los especialistas en pruebas de penetración deben utilizar técnicas
como la [ingeniería social](../../../blog/social-engineering/)
y [*phishing*](../../../blog/phishing/)
para probar la ciberseguridad de las organizaciones.

Te ayudamos con el [cumplimiento de la GLBA](https://help.fluidattacks.com/portal/en/kb/articles/criteria-compliance-glba)
comprobando que tus sistemas especifiquen
la finalidad de la recopilación de datos,
soliciten el consentimiento del usuario y le permitan darse de baja,
entre otros requisitos.

### ¿Deben hacerse pruebas de penetración para cumplir con HIPAA?

[HIPAA](https://www.hhs.gov/hipaa/for-professionals/index.html)
es una ley federal según la cual
deben crearse normas que regulen y mejoren la forma
en que las entidades manejan la información de salud protegida
(PHI, por sus siglas en inglés).
Este tipo de información se refiere a aquella que puede utilizarse
para identificar a un paciente.
PHI puede tratarse de resultados de laboratorio,
visitas al hospital, prescripciones y registros de vacunación.
Las instituciones de salud,
y claramente toda empresa que tenga datos de pacientes,
deben garantizar la confidencialidad
y seguridad de la información utilizando
mecanismos de protección administrativos, físicos y técnicos adecuados.

El uso de pruebas de penetración es aconsejable pero no obligatorio.
Sin embargo,
vale la pena señalar que toda organización se beneficia
de someter sus sistemas a pruebas contra simulaciones
del comportamiento de atacantes malintencionados.
En este caso,
estamos hablando de una industria que tiene atributos atractivos
a los ojos de los adversarios.
Una de las principales motivaciones que ellos tienen
para atentar contra las instituciones de salud
es el alto valor de los datos de los pacientes.
Un factor que puede contribuir es el hecho de que estas entidades
suelen utilizar sistemas legados.
Sabemos que los sistemas sin actualizar son objetivos fáciles,
ya que es probable que sus vulnerabilidades sean conocidas
y, por tanto, explotadas constantemente por atacantes oportunistas.

Evaluamos si tus sistemas cumplen
los requisitos de seguridad relacionados
con [HIPAA](https://help.fluidattacks.com/portal/en/kb/articles/criteria-compliance-hipaa/).
Entre ellos,
los que tienen que ver con autenticación, autorización y criptografía.

### ¿Deben hacerse pruebas de penetración para cumplir con ISO?

[ISO/IEC 27001](https://www.iso.org/obp/ui/#iso:std:iso-iec:27001:ed-3:v1:en)
(o solo "ISO 27001") ha sido desarrollado por la ISO
(Organización Internacional de Normalización)
y la IEC (Comisión Electrotécnica Internacional).
Esta norma proporciona requisitos para
establecer, implementar, mantener y mejorar continuamente
un sistema de gestión de la seguridad de la información
(ISMS, por sus siglas en inglés).
Este documento ISO es oficialmente un estándar que puede utilizarse
en todo el mundo como base para la evaluación formal del cumplimiento
por parte de auditores de certificación acreditados.

Los controles de seguridad que pueden considerarse
más pertinentes varían notablemente según los sectores
que adoptan la norma ISO 27001.
Por lo tanto, no existe un conjunto único de controles para validar,
aunque hay elementos que se exigen explícitamente para la certificación.
En este sentido,
hay cláusulas que pueden justificar el uso
de *pentesting* como el enfoque preferido.
Una se refiere a que la organización determine su proceso de evaluación
del riesgo de la información.
La otra, se refiere a proporcionar pruebas del monitoreo
y medición de la seguridad de la información.
Recomendamos las pruebas de penetración
como un enfoque extremadamente adecuado
para probar las defensas de las organizaciones
contra ataques que en realidad podrían suceder
dado su panorama de amenazas.

Fluid Attacks verifica el cumplimiento de varios requisitos
de seguridad relacionados con
la norma [ISO/IEC 27001](https://help.fluidattacks.com/portal/en/kb/articles/criteria-compliance-iso27001).
Estos incluyen requisitos legales, de privacidad,
de manejo de datos, de código fuente
y de seguridad de la red, entre otros.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/pruebas-penetracion-servicio/"
title="Inicia ahora con las pruebas de penetración como servicio
de Fluid Attacks"
/>
</div>

### ¿Deben hacerse pruebas de penetración para cumplir con PCI?

El
[Estándar de Seguridad de Datos de la Industria de Tarjetas de Pago](https://docs-prv.pcisecuritystandards.org/PCI%20DSS/Standard/PCI-DSS-v4_0.pdf#lang=es)
(PCI DSS, por sus siglas en inglés) es conocido mundialmente.
Su objetivo es evitar el robo de datos y el fraude protegiendo
las transacciones realizadas con tarjetas de débito y crédito.
Básicamente, toda empresa que acepte, procese,
almacene o transmita información de tarjetas de débito
y crédito debería cumplir con PCI DSS.
Además, certificar y comunicar que una empresa cumple esta norma
garantiza a los clientes que pueden confiar sus datos
a la aplicación de la empresa.

**Cumplir las pruebas de penetración de PCI DSS consiste en realizar
pruebas de penetración externas e internas con regularidad**.
Además,
el requisito específico pide a las empresas
que corrijan las vulnerabilidades explotables
y los puntos débiles de seguridad encontrados.
¿Y qué se entiende por "regularmente"?
Es realizar pruebas de penetración al menos una vez al año.
Sin embargo,
se exige a los proveedores de servicios que demuestren
que sus sistemas se evalúan
al menos una vez cada seis meses y después de cualquier cambio
en los controles/métodos de segmentación.
Esto se debe a que generalmente tienen acceso a mayores volúmenes
de información de titulares de tarjetas
o pueden ser un puerto de entrada para comprometer otras múltiples entidades.

Fluid Attacks evalúa si los sistemas cumplen
los requisitos de seguridad vinculados
a [PCI DSS](https://help.fluidattacks.com/portal/en/kb/articles/criteria-compliance-pci).
Estos están relacionados con el manejo de datos,
la criptografía y el código fuente seguro,
entre otros.

### ¿Deben hacerse pruebas de penetración para cumplir con SOC 2?

El Instituto Americano de Contadores Públicos Certificados
(AICPA, por sus siglas en inglés)
creó los [controles de sistemas y organizaciones](https://www.aicpa.org/resources/landing/system-and-organization-controls-soc-suite-of-services)
(SOC, también conocidos como controles de organizaciones de servicios)
para referirse a los informes elaborados durante una auditoría.
SOC 2 muestra el resultado de la evaluación de las organizaciones proveedoras
de servicios en función de cinco criterios de confianza en el servicio:
seguridad, disponibilidad, integridad del procesamiento,
confidencialidad y privacidad.
Este estándar es relevante para las empresas tecnológicas
(p. ej., empresas SaaS)
que proporcionan sistemas de información
como servicio a otras organizaciones.

Los auditores que verifican el cumplimiento
de los criterios del servicio de confianza tienen instrucciones
de este estándar para validar si la organización proveedora
de servicios evalúa los controles de sus sistemas constantemente
o en ocasiones separadas.
Además, la normativa menciona las pruebas de penetración
como un tipo aceptable de dichas evaluaciones.
Aunque los requisitos de las pruebas de penetración
SOC 2 no son claros en cuanto a la frecuencia adecuada
con la que deben realizarse las evaluaciones,
recomendamos que se hagan de forma continua,
imitando el flujo constante de las ciberamenazas.

Verificamos los requisitos de seguridad relacionados
con los criterios [SOC 2](https://help.fluidattacks.com/portal/en/kb/articles/criteria-compliance-soc2/)
en tus sistemas. Estos requisitos incluyen el manejo de datos,
la seguridad de los dispositivos y
los controles de autenticación y autorización,
entre otros.

### ¿Deben hacerse pruebas de penetración para cumplir con CSCF de SWIFT?

El
[marco de controles de seguridad del cliente creado por SWIFT](https://www.swift.com/node/16726)
(CSCF, por sus siglas en inglés)
establece controles de seguridad obligatorios
y de asesoramiento para promover la seguridad
del sistema de comunicaciones interbancarias SWIFT.
SWIFT significa "Society for Worldwide Interbank Financial Telecommunication"
(Sociedad para las Telecomunicaciones Interbancarias y Financieras Mundiales),
que permite las transacciones financieras y los pagos entre bancos.

**Esta normativa exige explícitamente a las instituciones
financieras realizar pruebas de penetración**.
Este enfoque de seguridad se solicita para cumplir
un objetivo de control específico para validar
la configuración de seguridad operativa
e identificar brechas de seguridad.
(Puedes descargar el formulario de autocertificación de esta normativa,
en el que se menciona la necesidad de realizar *pentesting*,
siguiendo este [enlace](https://www.swift.com/swift-resource/239601/download).)

Evaluamos si tus sistemas cumplen los requisitos relacionados con
[CSCF de SWIFT](https://help.fluidattacks.com/portal/en/kb/articles/criteria-compliance-swiftcsc/).
Estos incluyen los de autorización,
autenticación y componentes de terceros,
entre otros.

## ¿Necesito cumplir con las pruebas de penetración?

La verdad es que si estás en una industria
que maneja datos sensibles de los clientes,
estás obligado a realizar pruebas de seguridad.
Independientemente de si en tu sector es obligatorio
o no realizar específicamente pruebas de penetración,
debes saber que seleccionar este enfoque es más prudente
que ejecutar *únicamente* escaneos de vulnerabilidades.
Además,
te recomendamos que sometas tus sistemas
a pruebas de seguridad de forma continua, no solo periódica.
Recuerda que las amenazas evolucionan constantemente,
por lo que durante el tiempo que no realizas pruebas
eres más susceptible de ser vulnerado.

Si estabas buscando una normativa que no está en este artículo,
probablemente esté en
nuestra [Documentación](https://help.fluidattacks.com/portal/en/kb/criteria/compliance).
No dudes en [contactarnos](../../contactanos/)
si tienes alguna pregunta relacionada con el cumplimiento de estándares.

## Cómo ayudamos a cumplir estándares que requieren las pruebas de penetración

Fluid Attacks realiza pruebas de penetración continuas
desde el principio y durante todo el ciclo de vida de desarrollo de *software*.
Nuestra solución de [*pentesting*](../../soluciones/pruebas-penetracion-servicio/)
forma parte de nuestro servicio todo en uno,
[Hacking Continuo](../../servicios/hacking-continuo/).
En nuestras evaluaciones,
comprobamos que los sistemas en cuestión cumplan
con diversos estándares internacionales de renombre.
Ayudamos a nuestros clientes a ir más allá del cumplimiento
realizando pruebas de penetración de forma continua,
comprobando cada versión de sus sistemas.
Nuestros clientes saben que no basta con realizar
una sola prueba de penetración al año o trimestralmente,
porque realizan cambios todos los días
y el panorama de las amenazas también está en perpetua evolución.

Hacking Continuo incluye el acceso a
nuestra [plataforma](../../plataforma/).
En ella, los usuarios pueden ver hasta qué punto cumplen varios estándares.
Comienza tu [**prueba gratuita de 21 días**](https://app.fluidattacks.com/SignUp)
para aprovechar nuestras pruebas de seguridad automatizadas
y explorar la plataforma.
Puedes cambiar a un plan que incluya *pentesting*.
