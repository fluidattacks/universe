---
slug: blog/tribu-de-hackers-1/
title: Tribu de hackers red team 1.0
date: 2020-07-17
subtitle: Aprendiendo del experto en red team Marcus J. Carey
category: opiniones
tags: ciberseguridad, red-team, hacking, pentesting, blue-team
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1620331129/blog/tribe-of-hackers-1/cover_yosoni.webp
alt: Foto por Lucas Benjamin en Unsplash
description: Este artículo se basa en el libro "Tribe of Hackers Red Team" de Carey y Jin. Compartimos aquí una introducción y algunos de los aspectos más destacados de la primera entrevista.
keywords: Red Team, Hacking Red Team, Ciberseguridad Blue Team, Pentesting, Hacking Etico, Blue Team Red Team, Conocimientos, Tribu De Hackers
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Escritor y editor
source: https://unsplash.com/photos/R79qkPYvrcM
---

[_Tribe of Hackers Red
Team:_](https://www.amazon.com/gp/product/B07VWHCQMR/ref=dbs_a_def_rwt_bibl_vppi_i2)
*Tribal Knowledge from the Best in Offensive Cybersecurity*
de Marcus J. Carey y Jennifer Jin (2019)
pertenece a la serie de libros de ciberseguridad
*Tribe of Hackers* (Tribu de Hackers).
Hace poco me lo compartieron aquí en Fluid Attacks,
y ahora me gustaría compartir contigo algunos
de los aspectos más destacados que he seleccionado para este artículo.
Quizá haya algo que despierte tu curiosidad.

[Marcus J. Carey](https://www.linkedin.com/in/marcuscarey/)
es un *hacker* estadounidense con más de **25** años
de experiencia en ciberseguridad.
Es alguien que se describe a sí mismo como una persona inquieta
y curiosa desde que era niño.
Nunca se privó de la posibilidad de generar
y plantear preguntas para ampliar sus conocimientos sobre diversos temas.
Las mentes hambrientas son algo con lo que solemos contar,
o mejor dicho, algo que nos caracteriza
como humanos sin importar la información que busquemos.
Esa mente hambrienta de Marcus hizo posible el nacimiento
de este libro que actualmente tengo en mis manos.

Echemos un vistazo a lo que podemos aprender
de estas casi **300** páginas.
Al principio, Marcus dice que probablemente el lector eligió
ese libro para aprender de los mejores en *red teams*.
Y así es, efectivamente nos interesan los
[*red teams*](../ejercicio-red-teaming/).
También es cierto que, como en muchas disciplinas,
la información que comparten "los mejores" suele ser valiosa.
En este caso, las preguntas, formuladas con el apoyo de una comunidad
de ciberseguridad en Twitter,
van dirigidas a *hackers* especializados en seguridad ofensiva,
también conocida como [*red teaming*](../../soluciones/red-team/).
¡Suena genial!

- \- - - - - - - - - - - - - - - - - - - - -

### Paréntesis

En Fluid Attacks, trabajamos puramente como un *red team*,
lo que significa que estamos continuamente
evaluando o atacando infraestructuras,
aplicaciones y código fuente para encontrar vulnerabilidades
que puedan suponer riesgos tanto para los propietarios
como para los usuarios de esos sistemas.
Por otro lado,
un *blue team* se encarga de defender los sistemas para,
tal como sugiere Marcus, garantizar que la confidencialidad,
integridad y disponibilidad de todos los activos no se vean afectadas.
En otras palabras, el *red team*, con su cañón,
verifica hasta qué punto ha sido eficaz el trabajo del *blue team*
para construir el muro.
Luego, cuando nos referimos a la combinación de ambos bandos,
algo implementado en algunas organizaciones,
hablamos de un [purple team](../../../blog/purple-team/).

- \- - - - - - - - - - - - - - - - - - - - -

En concreto, Marcus y Jennifer (su colaboradora)
formularon las mismas 21 preguntas a 47 expertos
en *red teaming* para elaborar este libro.
Por supuesto, el primero en responderlas es el propio Marcus.
Vamos a centrarnos en algunas de sus opiniones en este artículo;
posiblemente el primero de una serie centrada en *Tribe of Hackers Red Team*.

<div class="imgblock">

![Marcus J. Carey](https://res.cloudinary.com/fluid-attacks/image/upload/v1620331128/blog/tribe-of-hackers-1/marcus_wvbmtg.webp)

<div class="title">

Imagen 1. Foto de Marcus tomada de
[LinkedIn](https://www.linkedin.com/pulse/im-unemployed-i-am-hiring-marcus-carey?articleId=6202227092038365184#comments-6202227092038365184&trk=public_profile_article_view)

</div>

</div>

Según Marcus,
no es común que la gente empiece directamente en trabajos de *red team*.
(Bueno, cabe destacar que la mayoría de los *hackers*
de Fluid Attacks empezaron a trabajar como miembros del *red team*
y nunca formaron parte de *blue teams*, según me informaron).
Él dice que la mejor forma de conseguir un trabajo
en un *red team* es empezar a trabajar para un *blue team*
y adquirir conocimientos de ciberseguridad como ingeniero de *software*,
administrador de sistemas o roles relacionados.
Una vez allí, puedes involucrarte más en
[eventos](../../../about-us/events/)
y redes de ciberseguridad, y también prepararte y tratar de obtener alguna
[certificación](../../../certifications/) relacionada con *red teaming*.
Además, puedes descargar máquinas virtuales
y aplicaciones web con vulnerabilidades
si quieres empezar a practicar por tu cuenta.
Por supuesto, para evitar problemas con la ley,
ten en cuenta que es prudente explotar solo sistemas que sean propios
o que tengan permiso explícito para este tipo de actividad.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/red-team/"
title="Inicia ahora con la solución de red teaming de Fluid Attacks"
/>
</div>

Desde el punto de vista de una organización,
Marcus comparte su creencia de que todas las personas en TI
e ingeniería de *software* deberían saber cómo construir,
proteger y *hackear* cualquier cosa de la que estén a cargo.
Aunque ciertamente pueda sonar excesivo,
este comentario refleja una sugerencia de integración de competencias.
Y esto es en parte lo que muchas empresas han conseguido
al permitir la formación de *purple teams*.
Sin embargo, por otro lado están las empresas que han abierto
sus puertas al *pentesting* solo para cumplir la normativa
y lo han mantenido bastante limitado.
¡Eso debería preocuparles!

Respecto a una de las preguntas sobre cuál es el control
más importante para evitar que un sistema se vea comprometido,
Marcus eligió como respuesta restringir los privilegios administrativos
de los usuarios finales.
Menciona esto como algo fácil de implementar
y escalar en cualquier organización.
Es algo sencillo pero que da resultados significativos.
Así mismo ocurre para la autenticación de dos factores/dos pasos,
la longitud de las contraseñas y las actualizaciones automáticas.
Todo esto está relacionado con la otra sugerencia de Marcus
sobre la defensa general de los sistemas contra ataques:
establecer políticas y seguirlas.
Por cierto,
¿has oído hablar de nuestra sección de documentación
[“Criteria”](https://help.fluidattacks.com/portal/en/kb/criteria/requirements)
(recopilación de requisitos de seguridad de Fluid Attacks)?
Échale un vistazo para hacerte una idea de cómo lo hacemos.

Como sugerencia de Marcus respecto al trabajo en equipo,
debe destacarse la comunicación como el elemento más significativo.
Además, esta debe ir acompañada de un alto grado de confianza
entre los miembros del equipo.
Estos no deben tener miedo de pedir ayuda
a sus compañeros cuando sea necesario.
Adicionalmente, Marcus sugiere transparencia, es decir,
la posibilidad de que todos los otros miembros puedan ver
todo lo que hace y documenta cada miembro del equipo.
Eso es algo que permiten las herramientas de colaboración.
Mientras leo estas recomendaciones,
me alegra reconocer que en Fluid Attacks las seguimos fielmente.

Ahora bien,
si trabajas como miembro de un *red team* y tienes que informar
y apoyar a un equipo azul externo o interno tras completar una operación,
debes actuar como un profesional.
¿Qué significa esto?
Siempre hazles entender
que estás en el mismo equipo en cuanto a la misión principal, aconseja Marcus.
Al mismo tiempo,
no te enfades y ayúdales si su plan para corregir
los problemas que descubriste no funciona o no se aplica correctamente.
Además, como recomendación relevante para la entrega de informes útiles,
no descartes la idea de utilizar [CVSS](https://www.first.org/cvss/),
[MITRE ATT\&CK](https://attack.mitre.org/), o
[NIST](https://nvd.nist.gov/general) como referencias.
Utiliza el conocimiento compartido públicamente,
no intentes reinventar lo que ya está establecido y,
por consiguiente, gana credibilidad
y facilidad de transmisión de la información.

Marcus destaca lo crucial que puede ser la empatía
para un miembro de un *red team*.
Sugiere que te pongas en el lugar de la otra persona
y que no seas un patán.
Por lo tanto,
propone esta característica humana como una a tener
en cuenta a la hora de contratar a miembros de un *red team*.
No obstante,
creo necesario aclarar que la empatía es común en las personas
(dejando de lado los casos atípicos),
aunque su nivel de expresión es variable.
Es cierto que algunos *mostramos más empatía* que otros,
pero también es cierto que es algo que podemos mejorar.

Por último,
y quizás volviendo a lo que mencionaba sobre él mismo inicialmente,
Marcus se refiere al hambre de conocimiento
como un factor esencial de un excelente *red teamer*.
Esta persona sería alguien motivado para estudiar,
para aprender cómo funcionan las cosas.
Estaría dispuesta a mejorar algunas habilidades
y siempre lista para ayudar a los demás.
Precisamente, Marcus y los demás miembros de *red teams*
entrevistados en su libro nos están ayudando.
Están compartiendo sus conocimientos con nosotros.
Y si al final ya sabíamos todo esto, bueno, no está de más recordarlo.
Quizá hasta puedan decir palabras que nos motiven,
y eso ya es algo positivo de por sí.
