---
slug: blog/pci-dss-buenas-practicas-2024/
title: Buenas prácticas de PCI DSS en 2024
date: 2024-01-26
subtitle: Comprende fácilmente los 51 requisitos nuevos de PCI DSS
category: opiniones
tags: ciberseguridad, cumplimiento, empresa
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1706281168/blog/pci-dss-best-practices-2024/cover_pci_dss_best_practices.webp
alt: Foto por Eduardo Balderas en Unsplash
description: PCI DSS v4.0 trae 51 requisitos nuevos a modo de buenas prácticas hasta marzo de 2025. Compartimos una clasificación que puede ayudar a asimilarlos.
keywords: Pci Dss, Buenas Practicas, Controles De Seguridad, Nuevos Requisitos, Cumplimiento Pci Dss, Escaneos De Vulnerabilidad, Proveedores De Servicios, Hacking Etico, Pentesting
author: Jason Chavarría
writer: jchavarria
name: Jason Chavarría
about1: Escritor y editor
source: https://unsplash.com/photos/selective-focus-photography-of-person-holding-baseball-bat-UVxd5b-_tw8
---

[PCI DSS v4.0](https://www.pcisecuritystandards.org/lang/es-es/)
establece requisitos nuevos para las compañías que manejan datos de cuentas.
Los evaluadores tendrán en cuenta una minoría (13) de esos
requisitos en las evaluaciones de PCI DSS
a partir de este año
(los resumimos [aquí](../pci-dss-v4-13-nuevos-requisitos/)).
A lo largo del año,
los **51** requisitos restantes se considerarán buenas prácticas
y no afectarán a los resultados de las evaluaciones.
Así será hasta el **31 de marzo de 2025**.

Dado que familiarizarse con los 51 requisitos
puede resultar abrumador,
nos gustaría compartirlos organizados en algunas categorías
que creemos que ayudarán a revisarlos con mayor facilidad:

- Desplegar pruebas automatizadas para protegerse contra las amenazas actuales.

- Gestionar la superficie de ataque
  y los riesgos de seguridad de forma exhaustiva y preventiva.

- Definir la frecuencia de las evaluaciones.

- Proteger los datos sensibles de autenticación
  (SAD, por sus siglas en inglés) y el número de cuenta principal
  (PAN, por sus siglas en inglés).

- Implementar mecanismos criptográficos adecuados.

- Implementar mecanismos de autenticación más robustos.

- Revisar y mejorar el control de acceso en las cuentas de usuario
  y en las cuentas de aplicaciones y sistemas.

- Disponer de programas de respuesta a incidentes más estrictos.

- Disponer de programas de concientización sobre seguridad actualizados
  y más completos.

A su vez,
consideramos que estas categorías forman parte de tres áreas generales:
pruebas de seguridad y gestión de riesgos,
desarrollo seguro, y programas de respuesta a incidentes
y de concientización sobre seguridad.

<caution-box>

**Descargo de responsabilidad:** Enumeramos los requisitos dichos
de forma diferente a como lo hace
el [documento](https://www.pcisecuritystandards.org/lang/es-es/)
de PCI DSS v4.0.
Esto nos ha permitido resumir los requisitos que constan de varios puntos.
Recomendamos al lector que base su cumplimiento
en el documento PCI DSS v4.0
y que considere este artículo del blog únicamente
como una clasificación de los nuevos requisitos realizada por terceros.

</caution-box>

## Las 51 buenas prácticas nuevas

<br />

### Novedades sobre las pruebas de seguridad y la gestión de riesgos

Empecemos con las nuevas directrices sobre detección de amenazas.
Las novedades tienen en cuenta la evolución de las tendencias.
Entre ellas se encuentran la seguridad de la cadena
de suministro de *software*,
el acceso de los cibercriminales a los sistemas
mediante *malware* distribuido por dispositivos extraíbles
y la necesidad de automatización.

A continuación presentamos nuestras versiones simplificadas
de los puntos nuevos que, en resumen,
piden **desplegar pruebas automatizadas
para protegerse contra las amenazas actuales**:

- Mantener un inventario de *software* personalizado
  y componentes de *software* de terceros para facilitar
  la gestión de vulnerabilidades y parches. (6.3.2)

- Realizar análisis automatizados contra *malware* cuando se inserten,
  conecten o monten lógicamente dispositivos electrónicos extraíbles. (5.3.3)

- Implementar procesos y mecanismos automatizados
  para detectar ataques de *phishing* y defenderse de ellos. (5.4.1)

- Desplegar una solución técnica automatizada que detecte
  y prevenga continuamente los ataques basados en la web. (6.4.2)

- Utilizar mecanismos automatizados
  para revisar los registros de auditoría. (10.4.1.1)

- Desplegar un mecanismo para detectar cambios
  no autorizados en los encabezados HTTP
  y en el contenido de las páginas de pago al menos una vez cada siete días
  o cuando sea necesario según el análisis específico. (11.6.1)

- Utilizar técnicas de detección y/o prevención de intrusiones para detectar,
  alertar/prevenir y abordar comunicaciones encubiertas
  con sistemas de mando y control. (11.5.1.1)
  (Dirigido únicamente a los proveedores de servicios).

De todas ellas,
destaca la inclusión para abordar el [*phishing*](../../../blog/phishing/).
El estándar advierte del peligro de confundir este control
de seguridad con el mero entrenamiento para la concientización.
Su consejo es desplegar controles contra la falsificación de identidad,
así como depuradores de enlaces y *antimalware* del lado del servidor
para bloquear los correos electrónicos de *phishing* y *malware*.

Respecto al requisito 6.3.2, no debería sorprendernos,
ya que en los últimos dos años se ha tomado cada vez
más conciencia de la importancia de generar una
lista de materiales de *software* ([SBOM](../../learn/que-es-sbom/)).
Vinculamos este requisito al despliegue de pruebas automatizadas
ya que se aconseja utilizar
[herramientas de análisis de composición de *software*](../../producto/sca/),
entre otras,
para hacer inventario de las dependencias de terceros que componen
el producto que se pretende proteger,
junto con detalles como el estado de seguridad.

Nos gustaría describir otros requisitos que se refieren
a **gestionar la superficie de ataque y los riesgos de seguridad
de forma exhaustiva y preventiva**:

- Realizar escaneos de vulnerabilidades habilitados por credenciales
  y privilegios suficientes, y documentar los sistemas donde
  el escaneo autenticado no es posible. (11.3.1.2)

- Gestionar las vulnerabilidades que no se consideren de alto riesgo
  o críticas según el análisis de riesgo específico de la empresa. (11.3.1.1)

- Apoyar las solicitudes de los clientes para realizar
  pruebas de penetración o para obtener los resultados de las mismas.
  (11.4.7) (Dirigido a proveedores de servicios).

- Revisar la efectividad de la separación lógica mediante pruebas
  de penetración al menos una vez cada seis meses.
  (A1.1.4) (Dirigido a proveedores de servicios).

- Apoyar las decisiones sobre la frecuencia con que deben realizarse
  los requisitos con análisis de riesgos específicos revisados
  al menos una vez cada 12 meses. (12.3.1)

- Revisar las tecnologías en uso de *hardware* y *software*
  al menos una vez cada 12 meses para definir si cumplen
  con las necesidades de seguridad y si aún son respaldadas por el proveedor,
  y documentar el plan de acción
  para remediar las tecnologías obsoletas. (12.3.4)

- Documentar y confirmar el alcance de la evaluación
  PCI DSS al menos una vez cada seis meses
  y cuando se produzcan cambios significativos. (12.5.2.1)

- Documentar y comunicar una revisión a los directivos
  sobre el impacto en el alcance de PCI DSS
  y la aplicabilidad de los controles en caso de cambios significativos
  en la estructura organizativa
  (p. ej., fusiones o adquisiciones de empresas). (12.5.3)

Cabe destacar que PCI DSS v4.0 refuerza aún más sus requisitos
para los proveedores de servicios en lo que respecta
a las [pruebas de penetración](../pentesting/).
Además,
esta versión puede promover una mejor cultura
de [remediación de vulnerabilidades](../../../blog/vulnerability-remediation-process/)
ya que promueve el tratamiento de todas las vulnerabilidades.
También es bueno ver en el estándar el nuevo requisito
de [escaneo](../escaneo-de-vulnerabilidades/) autenticado,
que puede hacer que las evaluaciones de seguridad sean más exhaustivas.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/pruebas-penetracion-servicio/"
title="Inicia ahora con las pruebas de penetración como servicio
de Fluid Attacks"
/>
</div>

Y observamos que
otro conjunto de nuevos requisitos gira en torno
a **definir la frecuencia de las evaluaciones**:

- Definir la frecuencia de los escaneos periódicos de *malware*. (5.3.2.1)

- Definir la frecuencia de las evaluaciones de los componentes
  del sistema identificados como no expuestos
  al riesgo de *malware*. (5.2.3.1)

- Realizar un análisis de riesgos específico
  para definir la frecuencia de las revisiones de registros
  de los componentes del sistema para los que no es obligatoria
  una revisión diaria de registros. (10.4.2.1)

- Realizar un análisis de riesgos específico para definir
  la frecuencia de las revisiones periódicas de los dispositivos
  de punto de interacción y el tipo de revisiones. (9.5.1.2.1)

### Novedades sobre desarrollo seguro

También hemos identificado requisitos
que podrían clasificarse como buenas prácticas de desarrollo
para **proteger los datos sensibles de autenticación (SAD)
y el número de cuenta principal (PAN)**:

- Mantener al mínimo el almacenamiento de los SAD
  antes de completar la autorización. (3.2.1)

- Evitar que el PAN sea copiado o reubicado por personal
  no autorizado en tecnologías de acceso remoto
  (p. ej., escritorios virtuales). (3.4.2)

- Confirmar que los certificados utilizados para proteger
  el PAN durante la transmisión a través de redes abiertas
  y públicas son válidos y no están caducados o revocados. (4.2.1)

- Mantener un inventario de claves y certificados de confianza
  utilizados para proteger el PAN durante la transmisión. (4.2.1.1)

- Documentar todos los *scripts* de páginas de pago que se cargan
  y ejecutan en el navegador del consumidor con su justificación
  e implementar métodos para confirmar que cada *script* está autorizado
  y asegurar su integridad. (6.4.3)

V4.0 hace un llamado a las entidades para que mejoren
su cifrado con el fin de proteger los datos de las cuentas.
Se aconseja utilizar algoritmos de *hashing* como HMAC, CMAC y GMAC.
Los requisitos que hablan
de **implementar mecanismos criptográficos apropiados** son los siguientes:

- Documentar que se evita el uso de las mismas claves criptográficas
  en ambientes de producción y de prueba. (3.6.1.1)
  (Dirigido a proveedores de servicios).
- Utilizar criptografía fuerte para cifrar el SAD
  que se almacena antes de completar la autorización. (3.3.2)

- Utilizar criptografía fuerte para cifrar el SAD. (3.3.3)
  (Dirigido a los emisores).
- Utilizar *hashes* con clave criptográfica de todos
  los datos PAN para hacerlos ilegibles. (3.5.1.1)

- Utilizar cifrado a nivel de disco y a nivel de partición
  en dispositivos electrónicos extraíbles
  (p. ej., copias de seguridad en cinta)
  para que hacer ilegible el PAN. (3.5.1.2)

- Realizar un inventario de *suites* de cifrado criptográfico
  y protocolos en uso al menos una vez cada 12 meses,
  revisar su viabilidad continua y diseñar un plan de acción
  para cuando queden obsoletos. (12.3.3)

Otro grupo podría resumirse
como **implementar mecanismos de autenticación más robustos**,
lo cual es bastante claro en la indicación de hacer
que las contraseñas sean más largas
y exigir el éxito de todos los factores de la MFA
(autenticación de múltiples factores).
Los requisitos son los siguientes:

- Si la autenticación de las cuentas de usuario
  se realiza mediante contraseñas o frases de contraseña,
  estas deben tener al menos 12 caracteres
  (si el sistema no lo admite, el mínimo es de 8 caracteres)
  y tener tanto caracteres numéricos como alfabéticos. (8.3.6)

- Rotar periódicamente las contraseñas/frases de contraseña
  de las cuentas de aplicaciones y del sistema
  y hacer que sean complejas. (8.6.3)

- Exigir que las contraseñas/frases de contraseña
  se cambien al menos una vez cada 90 días
  o analizar en tiempo real la postura de seguridad
  de las cuentas de usuario. (8.3.10.1)
  (Dirigido a proveedores de servicios).

- Implementar MFA para todos los accesos
  al entorno de datos del tarjetahabiente. (8.4.2)
  (No aplica a las cuentas de usuario en terminales de puntos de venta).

- Exigir el cumplimiento de todos los factores de autenticación
  para conceder el acceso, hacer que sean al menos dos factores
  y utilizar controles para que no puedan eludirse
  ni sean susceptibles de ataques de repetición. (8.5.1)

Otro grupo de nuevos requisitos puede estar relacionado con
**revisar y mejorar el control de acceso en las cuentas de usuario
y en las cuentas de aplicaciones y sistemas**.
Un par de requisitos que sobresalen se centran en impedir
y restringir el inicio de sesión interactivo
(es decir, iniciar sesión en cuentas de sistema o de aplicación
como si se tratara de cuentas de usuario).
También vemos una gran mejoría con los requisitos
para revisar los privilegios de acceso.

- Limitar el acceso de las cuentas de aplicaciones
  y sistemas solo a los sistemas, aplicaciones o procesos necesarios
  y aplicar el principio de mínimo privilegio. (7.2.5)

- Revisar periódicamente las cuentas de aplicaciones y sistemas
  y los privilegios de acceso relacionados. (7.2.5.1)

- Impedir el inicio de sesión interactivo y documentar cuándo está permitido,
  exigiendo la aprobación explícita de la dirección ejecutiva,
  el uso únicamente durante el tiempo necesario,
  la confirmación de la identidad del usuario antes del acceso
  y posibilidad de atribución a este usuario individual. (8.6.1)

- Evitar que las contraseñas o frases de contraseña
  para cualquier aplicación y cuenta de sistema que pueda utilizarse
  para iniciar sesión interactiva estén codificadas en *scripts*,
  archivos de configuración/propiedades o código fuente. (8.6.2)

- Revisar al menos una vez cada seis meses todas las cuentas de usuario
  y los privilegios de acceso relacionados. (7.2.4)

- Implementar la separación lógica para que, a menos que esté autorizado,
  el proveedor no pueda acceder a los ambientes de sus clientes
  y viceversa. (A1.1.1) (Dirigido a los proveedores de servicios).

### Novedades para programas de respuesta a incidentes y de concientización

Relacionamos un conjunto de puntos nuevos
con **disponer de programas de respuesta a incidentes más estrictos:**

- Detectar, alertar y abordar con prontitud las fallas de los sistemas
  de control de la seguridad críticos. (10.7.2)

- Responder con rapidez, restablecer las funciones de seguridad
  e implementar controles para evitar que se repitan las fallas. (10.7.3)

- Detectar, alertar y abordar con prontitud las fallas
  de los mecanismos de revisión automatizados de registros de auditoría
  y de las herramientas de revisión de código automatizadas.
  (A3.3.1) (Dirigido a entidades a las que las franquicias de pago
  o adquirentes exijan someterse a evaluaciones adicionales debido,
  por ejemplo, a un historial de repetidas filtraciones de datos de cuentas).

- Disponer de procedimientos de respuesta a incidentes
  para cuando el número de cuenta principal se almacena donde no se espera,
  incluida la determinación de su procedencia
  y de cómo ha llegado a donde está. (12.10.7)

- Incluir que las alertas de un mecanismo de detección de cambios
  y manipulaciones en las páginas de pago sean monitoreadas
  y reciban respuesta. (12.10.5)

- Implementar métodos seguros para que los clientes informen
  sobre incidentes de seguridad y vulnerabilidades,
  y responder a cada reporte. (A1.2.3)
  (Dirigido a los proveedores de servicios).

- Realizar un análisis de riesgos específico para definir
  la frecuencia de la capacitación del personal de respuesta a incidentes.
  (12.10.4.1)

Promover la mejora de los [procesos de manejo de vulnerabilidades](../../../blog/iso-iec-30111/)
es una adición notable en esta versión.
Además, constituye un buen avance que ahora sean todas las entidades,
y no solo los proveedores de servicios,
quienes deban gestionar las fallas de sus sistemas
de control de seguridad críticos
(p. ej., controles de seguridad de red, IDS/IPS, controles de acceso lógico).

Por último,
queremos referirnos a los nuevos requisitos que vinculamos
a **disponer de programas de concientización
sobre seguridad actualizados y más completos**.
Entre ellos, vemos de nuevo el énfasis en la prevención de ataques
de *phishing* exitosos.

- Incluir contenidos sobre el uso aceptable de las tecnologías
  de usuario final. (12.6.3.2)

- Incluir el *phishing* y otros ataques relacionados
  con la ingeniería social. (12.6.3.1)

- Revisar el programa al menos una vez cada 12 meses
  para mantenerlo actualizado frente a nuevas amenazas
  o vulnerabilidades. (12.6.2)

## Contribuimos a que tu *software* cumpla con PCI DSS

Fluid Attacks prueba si tu *software* cumple
los [requisitos de PCI DSS](https://help.fluidattacks.com/portal/en/kb/articles/criteria-compliance-pci).
Comienza a utilizar la [solución todo en uno](../../servicios/hacking-continuo/)
que realiza un [escaneo de vulnerabilidades](../escaneo-de-vulnerabilidades/)
continuo y [*pentesting*](../pentesting/)
y ofrece consejos de remediación de un [equipo de *hacking* experimentado](../../../certifications/)
y [a través de IA generativa](https://help.fluidattacks.com/portal/en/kb/articles/fix-code-automatically-with-gen-ai).

Para una prueba gratuita de escaneo de vulnerabilidades
con la herramienta automatizada de Fluid Attacks,
pulsa [aquí](https://app.fluidattacks.com/SignUp).
