---
slug: blog/codificacion-segura-cinco-pasos/
title: ¿Codificación segura en cinco pasos?
date: 2022-12-05
subtitle: Un enfoque sencillo para probar en la formación sobre ciberseguridad
category: desarrollo
tags: ciberseguridad, pruebas de seguridad, software, empresa, código
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1670296878/blog/secure-coding-five-steps/cover_secure_coding_five_steps.webp
alt: Foto por Ralston Smith en Unsplash
description: Presentamos una breve reseña de un estudio en el que los autores sugieren un enfoque para implantar y promover entre los desarrolladores de software prácticas de codificación seguras.
keywords: Codificacion Segura, Codigo Seguro, Practicas De Codificacion Segura, Desarrollo De Software, Revision De Codigo Seguro, Cinco Pasos, Vulnerabilidades De Seguridad, Hacking Etico, Pentesting
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Escritor y editor
source: https://unsplash.com/photos/zc9pWsPZd4Y
---

La educación en ciberseguridad sigue escaseando.
Todavía falta generación de mano de obra en este campo.
Según el[(ISC)² 2022 Cybersecurity Workforce Study](https://www.isc2.org//-/media/ISC2/Research/2022-WorkForce-Study/ISC2-Cybersecurity-Workforce-Study.ashx),
actualmente existe un déficit mundial
de 3,4 millones de trabajadores en ciberseguridad.
Por el lado del desarrollo de *software*,
muchos profesionales no están familiarizados con el tema
y entregan productos que a veces ni siquiera se someten a una revisión
(al menos adecuada).
La codificación segura y la [revisión de código fuente](../revision-codigo-fuente/),
o mejor aún, todo un ciclo de desarrollo seguro,
debería enseñarse a los desarrolladores desde el principio de sus carreras.
Aunque su trabajo suele contar con el apoyo de otros equipos
(como uno especializado en seguridad),
los desarrolladores expertos en seguridad pueden contribuir
desde el principio a que sus productos sean de alta calidad y,
en consecuencia, seguros.
Debemos tratar de aprovechar los recursos existentes
y crear métodos que contribuyan a su formación.
En este artículo, nos centramos precisamente
en un enfoque sencillo sugerido recientemente
en un artículo de investigación.

## "Codificación segura en cinco pasos"

Mientras vagaba por la Internet buscando un tema
para completar una breve serie de cinco artículos sobre el concepto
de "[revisión de código seguro (en inglés *secure code review*)](../../soluciones/revision-codigo-fuente/),"
me llamó la atención un artículo titulado
"Secure Coding in Five Steps” (Codificación segura en cinco pasos).
(Por cierto, los artículos de esa serie son
"[Revisión de código seguro](../revision-codigo-fuente/),"
"[Repasa y practica la codificación segura](../practicas-codificacion-segura/),"
"[La indispensable revisión manual de código](../revision-manual-de-codigo/),"
"[Seguridad como parte de la calidad del código](../calidad-y-seguridad-del-codigo/)"
y este que estás leyendo ahora).
Publicado a mediados de 2021 por Zeng y Zhu
en la *Journal of Cybersecurity Education, Research and Practice*,
se trata de un artículo de acceso libre y gratuito que puedes encontrar
y revisar en su totalidad
en [digitalcommons.kennesaw.edu](https://digitalcommons.kennesaw.edu/jcerp/vol2021/iss1/5/).

Más concretamente,
llegué a este artículo porque estaba pensando
y buscando información sobre estándares,
cursos y formación para la codificación segura.
Como empiezan diciendo los autores en el resumen del artículo,
existen numerosos recursos de buenas prácticas en el sector,
pero sigue siendo difícil enseñar
con eficacia prácticas de codificación segura.
Material es lo que hay. Y en abundancia abrumadora.
Con mis ya casi tres años en el campo de la ciberseguridad,
he sido testigo de ello. Siguiendo a los autores del artículo en cuestión,
el problema con estos recursos, como las guías de pruebas,
las bases de datos de vulnerabilidades
y los estándares de codificación segura,
es que no están pensados ni desarrollados directamente
para el mundo académico y sus estudiantes.

Como sin duda es ya sabido por muchos de nosotros,
la educación en temas de ciberseguridad dentro
de las universidades sigue siendo deficiente.
A lo largo de los años,
se han hecho esfuerzos por incluir cursos sobre desarrollo de *software*
seguro o codificación segura en los programas de estudios
de informática de varias instituciones
e incluso por hacer público material en línea para el aprendizaje.
Todo ello para hacer frente a las crecientes amenazas
y riesgos de un universo cibernético cada vez más abarrotado
de sistemas de información interconectados
y de ciberdelincuentes que buscan aprovecharse de ellos.

Lo que pretenden los investigadores del artículo aquí referenciado
es mejorar esta situación.
Parten del hecho de que es desde el principio del desarrollo
de productos de *software* que los aprendices
y profesionales deben pensar y actuar en términos de seguridad.
Presentan un **método de aprendizaje de cinco pasos** que resulta
más adecuado para los estudiantes,
menos complejo y que les motiva a practicar la codificación segura.
Según estos autores, el objetivo a largo plazo es educar
a los estudiantes en la mentalidad correcta,
los conocimientos necesarios y las habilidades
para desarrollar *software* seguro.
En su investigación, no se limitaron a compartir la teoría,
sino que la pusieron en práctica con sus propios alumnos de pregrado
y posgrado en su curso de seguridad informática y de *software*.
Veamos cada paso bien contextualizado
y cómo fue la metodología de estos investigadores en acción.

### Antes del primer paso

Aunque podríamos hablar de un "paso cero",
los investigadores no dieron nombre a este primer procedimiento.
Quizá porque no se centra en la codificación segura *per se*.
Lo que hicieron inicialmente, antes del primer paso,
fue ofrecer a los alumnos un amplio panorama en desarrollo seguro,
utilizando como referencia
el [Ciclo de vida de desarrollo de seguridad de Microsoft
(SDL, por su nombre en inglés)](https://www.microsoft.com/en-us/securityengineering/sdl/practices).
Se trata de un conjunto de 12 prácticas
que promueven el desarrollo de *software* seguro
(prácticas estrechamente relacionadas con lo que hacemos en Fluid Attacks):

1. [Proporcionar entrenamiento](../../../blog/training-basic/)
2. Definir [requisitos de seguridad](https://help.fluidattacks.com/portal/en/kb/criteria/requirements)
3. Definir métricas y [reportes de cumplimiento](https://help.fluidattacks.com/portal/en/kb/criteria/compliance)
4. Realizar modelos de amenazas
5. Establecer requisitos de diseño
6. Definir y utilizar estándares de criptografía
7. Gestionar el riesgo de seguridad derivado del [uso de componentes de terceros](../escaneos-sca/)
8. Utilizar herramientas aprobadas
9. Realizar pruebas de seguridad de análisis estático ([SAST](../../producto/sast/))
10. Realizar pruebas de seguridad de análisis dinámico([DAST](../../producto/dast/))
11. Realizar [pruebas de penetración](../../soluciones/pruebas-penetracion-servicio/)
12. Establecer un [proceso estándar de respuesta a incidentes](../../../blog/incident-response-plan/)

Para satisfacer la necesidad de aprendizaje activo
(practicando los cinco pasos de la codificación segura),
los investigadores decidieron utilizar
una sencilla aplicación web llamada ShareAlbum.
Desarrollada por otros estudiantes utilizando PHP, HTML y MySQL,
esta aplicación permite compartir fotos
y vídeos entre sus usuarios, así como mensajes.
Los archivos de ShareAlbum pueden clasificarse como públicos
o privados y ser revisados, comentados
y etiquetados por los usuarios dependiendo de los privilegios preestablecidos.
Posteriormente,
los autores presentaron en detalle esta aplicación a
los estudiantes participantes en el estudio.

### Primer paso

Este paso tiene como objetivo la
**adquisición conocimientos sobre las vulnerabilidades de seguridad más comunes**.
Entran en juego las
[25 debilidades de *software* más peligrosas del top de CWE](https://help.fluidattacks.com/portal/en/kb/articles/criteria-compliance-cwe25/)
y el top 10 de [OWASP de los 10](https://help.fluidattacks.com/portal/en/kb/articles/criteria-compliance-owasprisks/)
riesgos de seguridad de aplicaciones web más críticos.
Los autores presentaron estas listas
(además de las Common Vulnerabilities and Exposures, [CVE](../../../compliance/cve/))
a sus estudiantes.
A continuación, seleccionaron tres tipos de vulnerabilidades
([Cross-site scripting](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-008/),
[inyección SQL](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-146/)
y [carga no restringida de archivos clasificados como peligrosos](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-027/))
y explicaron en profundidad sus características, mecanismos de explotación,
posibles repercusiones y métodos de detección y remediación.
Después, para ilustrar vívidamente todo esto,
los investigadores utilizaron ShareAlbum como ejemplo de *software*
con estas vulnerabilidades.
Como parte de su participación activa,
los estudiantes tuvieron que revisar las demás vulnerabilidades de las listas,
elegir dos y explicarlas brevemente a sus compañeros.
Además,
los estudiantes recibieron pequeños fragmentos de código de ShareAlbum
para detectar y remediar vulnerabilidades
de los tres tipos comentados inicialmente.

### Segundo paso

Este paso está orientado a
**adquirir competencias en materia de pruebas de seguridad
o identificación de vulnerabilidades**.
Los autores reconocen que la revisión manual de código es indispensable
y los beneficios de mezclar este método con herramientas automatizadas
(por su escalabilidad y rapidez).
Por este motivo,
asignaron a pequeños grupos de estudiantes la tarea
de detectar errores en el código de la aplicación siguiendo manualmente
un listado de control
(adaptado de la guía de revisión de código de OWASP)
y utilizando una herramienta gratuita de tipo SAST,
a la que fueron debidamente introducidos.
La lista de problemas de seguridad
que los estudiantes pudieron reunir incluía errores
como la validación incorrecta de *inputs*,
la autorización indebida y la exposición de información.
En su ejercicio con la herramienta automatizada,
a los estudiantes se les permitió ser conscientes de las deficiencias de esta,
como la existencia de falsos positivos y falsos negativos,
y recibieron instrucción para su identificación y solución.
Después, pudieron practicar el reconocimiento
y el reporte de los falsos positivos proporcionados
por la herramienta en su lista de errores identificados.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/revision-codigo-fuente/"
title="Inicia ahora con la solución de revisión de código seguro
de Fluid Attacks"
/>
</div>

### Tercer paso

Este paso está orientado a la **priorización de vulnerabilidades**.
Los investigadores compartieron con sus estudiantes
que la remediación de todas las vulnerabilidades puede ser costosa,
por lo que deberían priorizarse según diferentes atributos.
Siempre será más beneficioso en términos de gestión de riesgos
y recursos esforzarse por cerrar algunas vulnerabilidades antes que otras.
Por ello,
se presentó a los alumnos el sistema Common Vulnerability Scoring System
(CVSS; en Fluid Attacks, debido a un par de defectos en este sistema,
lo modificamos ligeramente para convertirlo en
"[CVSSF](../../../blog/cvssf-risk-exposure-metric/)").
Los autores enseñaron a los estudiantes el uso de métricas
(p. ej., explotabilidad e impacto)
para calcular las puntuaciones CVSS y clasificar
y etiquetar las vulnerabilidades.
Seguidamente,
los estudiantes utilizaron la calculadora CVSS para
las vulnerabilidades que habían detectado previamente en la aplicación.
De ahí, priorizaron las vulnerabilidades según las puntuaciones obtenidas
y proporcionaron los tres errores principales
que debían remediarse en el siguiente paso.

### Cuarto paso

Este paso tiene como objetivo
**remediar las vulnerabilidades y mitigar los riesgos**.
Se tuvieron en cuenta las sugerencias de remediación proporcionadas
por los sitios web de CWE y OWASP y por la herramienta SAST.
Los autores tomaron como ejemplo las tres vulnerabilidades
que utilizaron y presentaron a los estudiantes
su remediación paso a paso en ShareAlbum.
Después, los estudiantes tomaron sus tres principales problemas
o errores de seguridad del paso anterior,
discutieron las estrategias de remediación
y luego las aplicaron como cambios de código.
Por último, realizaron otra revisión de código para comprobar
si esas modificaciones habían sido eficaces
y si habían dado lugar a nuevas vulnerabilidades,
las cuales habría que remediar.

### Quinto paso

Este último paso está orientado
a la **elaboración de reportes o documentación de los resultados**.
Reconociendo el uso frecuente de plantillas
de reportes estándar en las industrias,
los investigadores elaboraron una plantilla basada en
los elementos del informe de codificación segura de OWASP
y la muestra de revisión de código seguro de MITRE.
Los estudiantes tenían que presentar un informe siguiendo la plantilla,
la cual constaba de nueve puntos, entre ellos fechas de revisión,
módulos de código revisados, lista de comprobación y herramienta utilizada,
vulnerabilidades identificadas y las tres principales
(cada una con detalles como nombre, descripción, ubicación,
severidad y estrategia de remediación), entre otros.
(Por ejemplo,
todo ese tipo de datos los encuentras al recibir reportes en la
[plataforma](../../../blog/from-asm-to-arm/) de Fluid Attacks).

## Conclusiones y opiniones

Las contribuciones reales de este enfoque
de cinco pasos no están muy claras.
Los autores solo hablan de haber realizado encuestas
a los estudiantes antes y después del entrenamiento.
(Puedes consultar el documento para conocer las características
de los participantes y los resultados en cifras).
Aunque no da la impresión de haber sido un trabajo muy riguroso,
sobre todo en la recopilación y análisis de datos,
no soy quién para evaluarlo o criticarlo.
Lo que nos deja este estudio es que, aparentemente,
los estudiantes después de la formación, según sus comentarios,
estaban más motivados para revisar y aplicar pautas,
estándares y prácticas de codificación segura
(incluidas la notificación y la remediación de errores)
en su futuro trabajo de desarrollo de *software*.
Además, la mayoría de ellos se sintieron cómodos
con el enfoque del entrenamiento y los estudios de casos.

Mientras leía este artículo,
me pareció que estaba muy en línea con lo que hemos ofrecido
en anteriores entradas sobre revisión de código seguro
y, en parte, con nuestra forma de trabajar
en Fluid Attacks en la evaluación de *software*.
No sé si este enfoque de cinco pasos
es la mejor manera de introducir a los desarrolladores
de *software* en las prácticas y habilidades de codificación segura.
Aun así, es un enfoque que vale la pena
(los investigadores dicen que lo están mejorando aún más,
por ejemplo, ampliando el espectro de lenguajes de programación utilizados).
Me pareció una buena idea eso de empezar dando a los estudiantes
una visión general de la codificación segura,
presentándoles los muchos recursos disponibles
(otros ejemplos: [OWASP SAMM](https://fluidattacks.docsend.com/view/qh2kwhmd787mfzen),
SAFEcode y estándares CERT),
así como dándoles muchos ejemplos y,
sobre todo, oportunidades prácticas.
Desde las universidades y las empresas,
deberíamos fomentar el aprendizaje en el desarrollo
de *software* seguro con propuestas como esta,
que sirvan como parte de una estrategia preventiva frente
a las crecientes ciberamenazas actuales.
Te invitamos a revisar a fondo el artículo de Zeng y Zhu
para conocer más detalles sobre el enfoque que proponen.

Ahora,
si estás buscando apoyo para los equipos de desarrollo
de tu organización por parte de expertos en seguridad,
no dudes en [contactarnos](../../contactanos/).
En Fluid Attacks, ofrecemos
[revisión de código seguro](../../soluciones/revision-codigo-fuente/)
dentro de nuestro [servicio Hacking Continuo](../../servicios/hacking-continuo/)
que mezcla herramientas automatizadas con inteligencia experta
para detectar vulnerabilidades en tu *software*.
Si quieres una muestra de nuestras capacidades en ciberseguridad,
te invitamos a disfrutar de las técnicas [SAST](../../producto/sast/),
[DAST](../../producto/dast/)
y [SCA](../../producto/sca/) con nuestras herramientas automatizadas
en nuestra [prueba gratuita de 21 días](https://app.fluidattacks.com/SignUp).
