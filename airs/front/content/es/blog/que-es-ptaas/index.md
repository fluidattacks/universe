---
slug: blog/que-es-ptaas/
title: Pentesting como servicio
date: 2022-09-21
subtitle: ¿Qué es PTaaS y qué beneficios te aporta?
category: filosofía
tags: ciberseguridad, pentesting, pruebas de seguridad, hacking, empresa, red-team
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1663789228/blog/what-is-ptaas/cover_ptaas.webp
alt: Foto por Dulcey Lima en Unsplash
description: Las pruebas de penetración como servicio (PTaaS) son pentesting continuos para encontrar vulnerabilidades complejas. Conoce aquí los beneficios que PTaaS puede ofrecer a tu organización.
keywords: Pruebas De Penetracion Como Servicio, Ptaas, Pen Testing, Pen Test Manual, Herramientas Automatizadas, Escaneo De Vulnerabilidades, Devsecops, Pentesting, Hacking Etico
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Escritor y editor
source: https://unsplash.com/photos/ovHJ_FajmLo
---

Hace más de un año,
hablamos [en un artículo de este blog](../../../blog/choosing-pentesting-team/)
sobre la creciente expansión del mercado del _pentesting_
y lo complejo que se está volviendo
para las organizaciones elegir correctamente
un proveedor ante tantas ofertas disponibles.
El problema reside en que muchas de ellas pueden ser engañosas
y no garantizan una calidad suficiente
que otros proveedores pueden alcanzar en este método
de pruebas de seguridad.
En aquel entonces destacamos algunos atributos clave
que podías tener en cuenta
para elegir un proveedor de pruebas de penetración competente.
Ahora,
te informaremos sobre un modelo más reciente,
las pruebas de penetración como servicio ([PTaaS](../../producto/ptaas/),
por su nombre en inglés),
en el que el _pen testing_ tradicional se ajusta
para tener más valor dentro de la ágil
y ahora popular metodología [DevSecOps](../../soluciones/devsecops/).
Nuestra intención es que tengas claro en qué consiste
y qué ventajas ofrece antes de tomar una decisión.

## Introducción: ¿Qué son las pruebas de penetración?

Como viste en el párrafo anterior,
utilizamos las palabras "pruebas de penetración",
"_pentesting_" y "_pen testing_".
Esto es algo común en este contexto,
pero todas ellas se refieren al mismo concepto:
pruebas de seguridad en sistemas de información
mediante la simulación de ataques reales
con la autorización de sus propietarios
para detectar vulnerabilidades.
Las pruebas de penetración forman parte
de una postura de seguridad ofensiva
en la que la idea predominante es que la mejor manera
de hacer frente a los atacantes maliciosos
es [pensar y actuar como ellos](../../../blog/thinking-like-hacker/).
Para ello,
los expertos en seguridad,
conocidos como evaluadores de sombrero blanco,
_hackers_ éticos o, precisamente, _pentesters_,
utilizan diversas tácticas,
técnicas y procedimientos.
En sus resultados de penetración y explotación,
estos expertos revelan a los propietarios y partes interesadas
dónde y cómo hacer ajustes para proteger sus sistemas.

Los continuos avances de la ciberdelincuencia
y la acelerada evolución de la tecnología
hacen necesario evaluar una y otra vez
la seguridad de los sistemas.
Erróneamente,
muchas organizaciones creen que implementar herramientas automatizadas
es la solución perfecta. Y que cuantas más herramientas tengan, mejor.
La automatización cumple con el llamado escaneo de vulnerabilidades.
Esto, sin embargo,
actúa solo como un primer nivel dentro de una estrategia
de pruebas de seguridad integral.
Los sistemas se revisan a través de este método
para detectar en ellos rápidamente problemas
de seguridad previamente conocidos.
No incluir un nivel activo de intervención humana
en un proyecto de pruebas de seguridad,
precisamente con el _pen testing_ manual,
es un error garrafal.

Te pedimos que seas consciente del énfasis
que hemos puesto más arriba en "manual".
Hablamos de pruebas de penetración manuales porque,
en el contexto de la ciberseguridad,
también se atribuye a las herramientas automáticas
la capacidad de realizar _pentesting_.
No negamos que las herramientas puedan infiltrarse
o abrirse camino en diversos rincones de un sistema.
Pero una prueba de penetración adecuada
no debe limitarse a la automatización.
El _pentesting_ sin intervención humana acaba siendo
un simple escaneo de vulnerabilidades.
A diferencia de lo que pueden conseguir
los _hackers_ éticos con una inspección en profundidad,
este método de escaneo no logra reportar vulnerabilidades complejas
de la lógica empresarial
ni de las vulnerabilidades de día cero.
Además,
arroja falsos positivos y falsos negativos,
que los profesionales deben validar.

## ¿Cómo suele realizarse el _pentesting_?

Un servicio de pruebas de penetración
puede incluir entre sus objetivos
aplicaciones web y móviles,
redes, dispositivos IoT
y muchos otros sistemas de información.
Su objetivo es detectar problemas en los controles de autenticación
y autorización de usuarios,
exposición de datos sensibles,
errores en la codificación segura
y debilidades en los mecanismos de defensa,
entre otros muchos problemas de seguridad.
Para comenzar con la penetración,
los _pentesters_ deben obtener la aprobación
del propietario del sistema,
que puede establecer ciertos límites de alcance.
Una vez acordado todo,
los _pentesters_ inician una fase de reconocimiento.

En primer lugar está el reconocimiento pasivo,
en el que los _hackers_ recopilan información sobre la organización
y el objetivo sin interactuar directamente con ellos.
Recurren entonces a fuentes externas y abiertas.
Luego, está el reconocimiento activo mediante
la interacción directa con el objetivo.
Los _pen testers_ buscan un perfilado profundo
con una recopilación de información más intrusiva.
Identifican la tecnología utilizada y su funcionamiento.
Además,
determinan posibles vectores de entrada y ataque.

Posteriormente,
los _pentesters_ utilizan herramientas de escaneo
y métodos manuales que contribuyen
a la identificación de vulnerabilidades.
Analizan a través de diversos factores el nivel de riesgo
y el impacto que puede generar
la explotación de cada problema de seguridad.
Después de toda la planificación,
los _hackers_ intentan explotar
las vulnerabilidades de forma creativa
(algo que una herramienta automática no puede hacer),
preferiblemente dentro de un [ambiente de prueba](https://www.techtarget.com/searchsoftwarequality/definition/staging-environment).
Consiguen acceso al objetivo con diferentes métodos
(p. ej., escalada de privilegios y movimiento lateral),
a distintos niveles de profundidad,
con el fin de determinar los impactos reales.

Una vez finalizada la tarea,
los _pen testers_ recopilan sus resultados
en reportes técnicos y ejecutivos.
Estos presentan a las partes interesadas detalles
sobre las vulnerabilidades detectadas y explotadas,
las respuestas del sistema a la penetración,
los datos a los que accedieron
y toda el resto de información sobre el incidente simulado.
Adicionalmente,
aportan evidencias de los problemas de seguridad
y recomendaciones para solucionarlos.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/pruebas-penetracion-servicio/"
title="Inicia ahora con las pruebas de penetración como servicio
de Fluid Attacks"
/>
</div>

## ¿Qué es PTaaS?

Antes de la computación en la nube,
el _pentesting_ se solía contratar para llevarse a cabo
como una evaluación puntual entre intervalos de tiempo amplios,
por ejemplo, de forma anual o semestral.
(Sin embargo,
si es que lo aplican,
muchas organizaciones siguen solicitándolo de esta manera).
En este modelo,
solo se entregan los resultados al cliente
en un reporte final estático que podría tener incluso datos desactualizados.
Las pruebas de penetración como servicio (PTaaS)
surgieron como un nuevo modelo de entrega
de _pentesting_ para eliminar las limitaciones anteriores.
PTaaS se adapta a la velocidad de desarrollo actual
y se realiza de forma continua mientras
el _software_ evoluciona a un determinado ritmo
en el SDLC (ciclo de vida de desarrollo de _software_).
Los resultados son entregados de forma gradual
basándose en los nuevos hallazgos.

PTaaS utiliza una plataforma centralizada
basada en la nube en la que los resultados se pueden ver,
supervisar y analizar continuamente.
El cliente puede lograr una gestión de vulnerabilidades exitosa,
ya que este nuevo modelo constante
ayuda a resolver el problema de priorización
y remediación causado por el modelo anterior,
en el cual todas las vulnerabilidades,
viejas y nuevas,
se dejan para ser reportadas en un solo punto en el tiempo.
Otra dificultad resuelta con PTaaS es la limitada
o inexistente colaboración entre desarrolladores
y _pentesters_.
Estos últimos pueden ahora apoyar con frecuencia a los primeros,
resolviendo sus dudas y proporcionándoles recomendaciones
o instrucciones de remediación.

En PTaaS,
debe haber _pentesting_ automatizado y manual.
Este modelo reconoce que la creatividad humana
sigue siendo indispensable en la evaluación de sistemas.
Si solo fuera lo primero,
acabaríamos hablando simplemente de _software_ como servicio (SaaS).
Las pruebas de penetración manuales continuas
se combinan con el escaneo de vulnerabilidades
para disfrutar de los beneficios de ambas soluciones.
Los expertos y las herramientas pueden garantizar
el uso de una amplia variedad
de metodologías de pruebas de seguridad.
Mientras que las herramientas automatizadas
se concentran en la detección rápida de vulnerabilidades conocidas,
los _pentesters_ se dedican a descubrir vulnerabilidades
más complejas e incluso desconocidas hasta el momento.
Los _pen testers_ también correlacionan sus resultados
y validan los proporcionados por las herramientas,
asegurándose de que el reporte final es correcto
y que nada se ha omitido.

## Beneficios de PTaaS

Puedes esperar lo siguiente de un proveedor de PTaaS experto:

- Una integración de automatización y _hackers_ éticos o _pentesters_
  que mejora la eficiencia y precisión de las pruebas de seguridad.

- Un único panel de control con todos los datos relevantes
  durante las pruebas de penetración que te ofrece un control amplio
  y cómodo para la gestión de vulnerabilidades.

- Los datos están siempre disponibles y se actualizan continuamente
  a medida que avanza la evaluación de tu sistema;
  un procedimiento que se mantiene alerta a los cambios recientes.

- La remediación de vulnerabilidades puede realizarse
  poco después de la identificación,
  siguiendo un orden de prioridades.
  Se evita llegar a producción con un alto riesgo de verse perjudicado
  por ciberataques.

- Su modelo permite una colaboración constante entre el grupo de _pen testers_
  y tu equipo de desarrolladores.

- Una vez que hayas remediado una vulnerabilidad,
  puedes solicitar la verificación de la efectividad
  de la solución implementada.

## PTaaS por parte de Fluid Attacks

De acuerdo con lo anterior,
tanto si intentas únicamente cumplir con estándares
como [PCI DSS](../../../compliance/pci/),
[NIST](../../../compliance/nist/), [GDPR](../../../compliance/gdpr/),
[HIPAA](../../../compliance/hipaa/), etc.,
como si aspiras a un compromiso más amplio
con la seguridad de tu empresa y clientes o usuarios,
en Fluid Attacks ofrecemos un óptimo PTaaS.
Evaluamos en modo seguro
(es decir, sin afectar la disponibilidad de tus servicios)
la seguridad de tus aplicaciones
web y móviles, redes, dispositivos,
infraestructura en la nube y otros sistemas informáticos.
Combinamos nuestras herramientas automáticas
con [pruebas de penetración manuales](../../soluciones/pruebas-penetracion-servicio/)
realizadas por nuestros expertos en ciberseguridad,
quienes cuentan con certificaciones de alta reputación
y diversos conjuntos de habilidades.
De este modo,
obtenemos tasas mínimas de falsos positivos y falsos negativos.

Incorporamos PTaaS a tu SDLC desde el principio
y probamos tu _software_ al ritmo de tu equipo de desarrollo
y sus microcambios.
En nuestra [plataforma](https://app.fluidattacks.com/),
recibes continuamente informes detallados,
a medida que avanza el _pentesting_ continuo,
los cuales te facilitan la comprensión de tu exposición al riesgo
y la priorización de los problemas de seguridad para su remediación.
Tus desarrolladores pueden mantener comunicación
y colaboración con nuestros _hackers_,
de quienes reciben evidencias claras y tangibles
y recomendaciones de reparación.
Además,
nuestro equipo te ofrece reataques ilimitados para verificar
que tus vulnerabilidades han sido efectivamente cerradas.
Por otra parte,
nuestro agente DevSecOps rompe el _build_
para evitar que las vulnerabilidades pasen a producción
si permanecen abiertas,
de acuerdo con las políticas de tu organización.

Esta solución forma parte
de nuestro [servicio Hacking Continuo](../../servicios/hacking-continuo/).
Te invitamos a contactarnos si estás interesado en conocer
los beneficios de nuestro _pentesting_
como servicio ([PTaaS](../../producto/ptaas/)).
Si quieres empezar con nuestros servicios de pruebas de seguridad
mediante herramientas automáticas,
tenemos a tu disposición [una prueba gratuita de 21 días](https://app.fluidattacks.com/SignUp)
de nuestro plan Essential.
