---
slug: blog/transparencia-seguridad-cadena-de-suministro/
title: El compromiso debe mostrarse
date: 2024-07-17
subtitle: Transparencia por menos ataques a la cadena de suministro
category: filosofía
tags: ciberseguridad, riesgo
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1721239722/blog/transparency-for-supply-chain-security/cover_transparency.webp
alt: Foto por Wilhelm Gunkel en Unsplash
description: Debería ser fácil saber qué tan comprometidas están las librerías de OSS más utilizadas con la seguridad. Y esa transparencia que pedimos deberíamos practicarla nosotros mismos.
keywords: Prevencion De Ataques Cadena De Suministro, Transparencia En Ciberseguridad, Seguridad De La Cadena De Suministro De Software, Polyfill, Librerias De Codigo Abierto Mas Usadas, Practicas De Seguridad De Software, Hacking Etico, Pentesting
author: Jason Chavarría
writer: jchavarria
name: Jason Chavarría
about1: Escritor y editor
source: https://unsplash.com/photos/white-and-black-diamond-shape-illustration-3VQ4AfOKCVc
---

## ¿Escuchaste sobre el ataque a la cadena de suministro más reciente?

Ha vuelto a ocurrir.
Recientemente se descubrió que una librería de *software* abrió una puerta
a ataques
afectando una enorme cantidad de productos que la tienen en sus cimientos.
Tenemos un nombre para eso:
ataque a la cadena de suministro.
Nos referimos esta vez a la explotación de polyfill.js.
Para los que se acaban de enterar de esta noticia,
el dominio que los productos de *software* utilizan para embeber esta librería,
con el propósito de hacer disponibles funciones modernas
en navegadores antiguos,
ha estado enviando código JavaScript malicioso a dichos productos,
lo que ha provocado que los usuarios sean redirigidos
a sitios web no solicitados.
Unas tres semanas después de conocerse la noticia,
más de [80 mil páginas web](https://publicwww.com/websites/%22cdn.polyfill.io%22/)
siguen utilizando la biblioteca.
La utilizan incluso luego de que el autor original
[urgió a los proyectos a eliminar polyfill.js](https://www.theregister.com/2024/06/25/polyfillio_china_crisis/).
Es más,
se sigue utilizando incluso cuando esta misma persona [dice](https://community.fastly.com/t/new-options-for-polyfill-io-users/2540)
que los navegadores modernos pueden ocuparse
de aquello para lo que se creó la biblioteca.

## ¿Qué tan comprometidas están las librerías más utilizadas con la seguridad?

Queremos hablar de un evento anterior a los ataques mencionados,
ya que creemos que es clave para entender una preocupación importante
en torno a los ataques a la cadena de suministro.
Y es que polyfill.js había cambiado de propietario en febrero de 2024.
Y al [darse cuenta de la disparidad](https://web.archive.org/web/20240229113710/https://github.com/polyfillpolyfill/polyfill-service/issues/2834)
entre el comentario del creador
("[...] eliminen [la librería] INMEDIATAMENTE")
y el de otra persona importante involucrada previamente en el proyecto
("He estado en conversaciones con Funnull durante muchos meses
y ellos serán los nuevos mantenedores
y operadores del repositorio [de la librería] en GitHub [...]"),
los usuarios empezaron a hacerse preguntas.
¿Qué se debe hacer?
¿Y quiénes son Funnull?
De hecho,
esta última pregunta ha dado lugar a descubrimientos preocupantes.
[Se ha reportado](https://www.theregister.com/2024/06/25/polyfillio_china_crisis/)
que, aunque la empresa dice ser estadounidense,
su sitio web está en mandarín,
las direcciones donde afirma tener oficinas son falsas
y su ubicación real podría ser Filipinas.
Al revisar su sitio web,
[hay quienes se han dado cuenta](https://dev.to/schalkneethling/to-polyfill-or-not-to-polyfillio-5ggd)
de que la empresa menciona de paso una política de privacidad,
que nadie puede localizar,
y afirma
que los propios usuarios deben leer periódicamente los términos del servicio
para enterarse de cualquier cambio en ellos.
El cambio de propietario se hizo de forma descuidada,
sin una comunicación transparente sobre quiénes son los nuevos dueños
y cuáles serán las implicaciones para los usuarios.

Lo anterior nos lleva a preguntarnos:
¿Quién se encarga del mantenimiento
de los componentes de *software* más usados?
Ya conoces el chiste de que es una persona no reconocida
la que mantiene un bloque
que, si se rompiera, haría que todo el edificio se derrumbara.
[Un *post* anterior](../chiste-nebraska-dependencia-infraestructura/)
sostiene que, aunque se trata de un chiste gracioso,
es algo cercano a la realidad.
The Linux Foundation
y The Laboratory for Innovation Science de Harvard
[descubrieron](https://www.linuxfoundation.org/press/press-release/the-linux-foundation-and-harvards-lab-for-innovation-science-release-census-of-most-widely-used-open-source-application-libraries)
que,
en aproximadamente una cuarta parte de las 49 librerías gratuitas
y de código abierto más usadas,
**solo un desarrollador** de cada librería evaluada había contribuido
a más del 80% de las líneas de código.
¿Y hasta qué punto están comprometidos con la seguridad estos
y los demás proyectos más usados?
La investigación mencionada muestra
que muchos de ellos se mantienen desde cuentas individuales
en lugar de organizacionales.
Las primeras tienen menos granularidad que las segundas
en cuanto al control de permisos y publicación.
Además,
otra observación que se puede hacer es
que la gran mayoría de las librerías más utilizadas no participan
en el programa de [Insignia de Mejores Prácticas](https://www.bestpractices.dev/es)
de Open Source Security Foundation.
Este programa es de The Linux Foundation
y es una oportunidad
para que los proyectos de desarrollo muestren abiertamente
su grado de cumplimiento de muchos requisitos de seguridad.
Aunque el hecho de que no participen en él no significa necesariamente
que no sigan las buenas prácticas esperadas,
cabe la duda.
En serio,
dado que nuestro *software*, como es bien sabido,
**se compone en gran medida de dependencias indirectas de código abierto**
(es decir, paquetes utilizados por los paquetes
que utilizamos en nuestro proyecto),
es lógico que queramos saber hasta qué punto son seguras estas dependencias.
Necesitamos transparencia,
es decir,
que los proyectos sean abiertos y honestos
sobre sus prácticas, riesgos e incidentes de seguridad,
si queremos ser capaces de contrarrestar los ataques a la cadena de suministro.
Incluso,
nosotros mismos,
dado que nuestro proyecto puede formar parte de otro(s) proyecto(s),
tenemos que empezar a ser transparentes,
o mantener o perfeccionar nuestra transparencia.

<div>
<cta-banner
buttontxt="Leer más"
link="/soluciones/gestion-vulnerabilidades/"
title="Inicia ahora con la solución de gestión de vulnerabilidades
de Fluid Attacks"
/>
</div>

## ¿Estamos aportando nuestro granito de arena?

Los siguientes son algunos datos básicos
que deberíamos mostrar públicamente
y pedir a todos los demás proyectos de desarrollo de *software*
que sigan nuestro ejemplo:

- **Disponibilidad:** Hacer pública la tasa de peticiones procesadas con éxito
  en un intervalo de tiempo relevante.

- **Tiempo de respuesta:** Hacer público cuánto tiempo tarda responder
  a una cantidad relevante de peticiones
  (por ejemplo, el 99% de las peticiones).

- **Código fuente:** Hacer público el código fuente,
  pues hemos argumentado una y otra vez
  que [publicar nuestro código fuente no nos resta seguridad](../../../blog/oss-security/),
  al contrario,
  fomenta una mayor seguridad
  debido a las mayores expectativas sobre los desarrolladores
  y el mayor escrutinio de la comunidad.

- **Incidentes:** Revelar la cronología,
  causa,
  impacto,
  solución,
  de cada problema;
  y en cuanto a haber sufrido un ciberataque exitoso,
  reportarlo con veracidad y exhaustividad.

- **Vulnerabilidades de seguridad:** Hacer públicos los fallos
  que se hayan detectado en el proyecto,
  especialmente aquellos que están sin gestionar.

<caution-box>

Estos son los enlaces a esa información en el caso de Fluid Attacks:
[Disponibilidad, tiempo de respuesta](https://availability.fluidattacks.com/),
[código fuente](https://gitlab.com/fluidattacks/universe/),
[incidentes y vulnerabilidades de seguridad](https://status.fluidattacks.com/history).

</caution-box>

En Fluid Attacks estamos convencidos de que si exhibimos lo que hacemos,
si todo el mundo puede ver la información de arriba sobre nuestro proyecto,
seguro que nos irá mejor.
Y este cuidado redundará sin duda
en pro de la calidad de las cadenas de suministro de *software*.

Por suerte, no habrá forma de escapar a la necesidad de ser transparentes,
ya que algunas iniciativas y normativas actuales de gran importancia
están presionando para ello.
Si no han llegado a nuestra región,
siguen ejerciendo presión para que el resto del mundo adopte medidas similares,
aunque para esto falte tiempo.
Estos son algunos ejemplos:

- Las [reglas de la Comisión de Bolsa y Valores](../../../blog/sec-new-regulations/)
  (SEC, por su nombre en inglés) de Estados Unidos
  ya exigen a las empresas que cotizan en bolsa
  que revelen los incidentes con prontitud,
  describan su gestión de riesgos
  e informen de la experiencia en ciberseguridad de sus directivos.
  Los CISOs pueden estar seguros de que son legalmente responsables
  de la postura de ciberseguridad de su empresa.
  [Ya hablamos en otra ocasión](../../../blog/trend-forecast-for-2024/#sec-rules-and-greater-liability-of-cisos)
  sobre cómo,
  incluso antes de estas normas,
  un par de CISOs se metieron en un buen lío
  por mentir sobre el estado de la ciberseguridad de sus empresas.

- Se espera que [Cyber Resilience Act](../../../blog/cyber-resilience-act/)
  de la Unión Europea [entre en vigor](https://www.europarl.europa.eu/legislative-train/theme-a-europe-fit-for-the-digital-age/file-european-cyber-resilience-act?sid=8101)
  pronto.
  Este marco jurídico exigirá
  que todos los productos con elementos digitales en la Unión Europea
  estén protegidos durante todo el ciclo de vida de desarrollo de *software*
  (SDLC, por su nombre en inglés).
  Ya están definidas las sanciones monetarias por incumplimiento.

- Se espera que el [etiquetado de ciberseguridad para IoT](../../../blog/cybersecurity-labeling-for-iot/)
  (es decir, la emisión de etiquetas de aprobación
  a los dispositivos de Internet de las cosas
  que cumplan los requisitos clave de ciberseguridad)
  entre pronto en vigor en Estados Unidos,
  así como se ha implementado en Europa.
  Es más,
  puede esperarse que el etiquetado se extienda a otros productos
  además de los dispositivos IoT,
  como está pasando ahora en Alemania.
  Estos programas son estupendos,
  ya que ayudan al consumidor a saber rápidamente
  si se han seguido prácticas de desarrollo de *software* seguro, y cuáles,
  y a elegir el producto más seguro.

- Desde hace algún tiempo,
  la generación de listas de materiales de *software*
  ([SBOMs](../../learn/que-es-sbom/), por su nombre en inglés)
  en formatos estándar específicos
  es una medida clave para detectar problemas
  que exponen los proyectos a ataques a la cadena de suministro.

- Ha surgido el enfoque
  de la seguridad de la cadena de suministro de *software*
  ([SSCS](../../../blog/software-supply-chain-security/),
  por su nombre en inglés),
  que básicamente va más allá de los SBOMs
  y los análisis de composición de *software*
  ([SCA](../escaneos-sca/), por su nombre en inglés)
  para dar seguridad incluso a la elección del proveedor,
  al código fuente propio a lo largo del SDLC,
  a las prácticas de seguridad
  y mucho más.

- El proyecto Secure Supply Chain Consumption Framework
  ([S2C2F](https://github.com/ossf/s2c2f)),
  procedente también de The Linux Foundation,
  da una idea de cómo consumir dependencias de *software* de código abierto
  en los flujos de trabajo de los desarrolladores.

Todo esto puede parecer abrumador,
pero está claro que es justo que cada uno de nosotros actúe
en favor de la certeza de nuestros colegas desarrolladores
y, en última instancia, de los usuarios finales,
de que nuestras prácticas de seguridad evitan ataques proactivamente.
Podemos ayudarte a proteger tu *software*
ofreciéndote las mejores técnicas y herramientas de AppSec
para [gestionar las vulnerabilidades](../../soluciones/gestion-vulnerabilidades/)
en todo el SDLC,
no solo las relacionadas con tu código,
sino también con el estado del código de terceros que utilizas.
[Contáctanos](../../contactanos/).
