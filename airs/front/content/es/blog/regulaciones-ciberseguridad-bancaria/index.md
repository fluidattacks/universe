---
slug: blog/regulaciones-ciberseguridad-bancaria/
title: La dura labor de la banca de cumplir estándares
date: 2024-06-26
subtitle: Garantizar cumplimiento y seguridad en el sector bancario
category: política
tags: ciberseguridad, empresa, tendencia, riesgo, software
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1719435480/blog/banking-cybersecurity-regulations/cover_banking_cybersecurity_regulations.webp
alt: Foto por Towfiqu barbhuiya en Unsplash
description: Las normas afectan a los bancos de forma desafiante pero ventajosa. Conócelas y lee nuestras recomendaciones para garantizar una postura de seguridad sólida.
keywords: Reglamentos De Ciberseguridad Bancaria, Reglamentos De Servicios Financieros, Cumplimiento De Ciberseguridad, Pruebas De Seguridad, Gestion De Vulnerabilidades, Desarrollo De Software Seguro, Marco De Ciberseguridad Bancaria, Hacking Etico, Pentesting
author: Wendy Rodriguez
writer: wrodriguez
name: Wendy Rodriguez
about1: Escritor y editor
source: https://unsplash.com/photos/a-glass-jar-filled-with-coins-and-a-plant-joqWSI9u_XM
---

Millones de personas confiamos en los bancos
para proteger el dinero que tanto nos ha costado ganar
y nuestra información personal.
Debido a la evolución continua del panorama de amenazas,
garantizar la seguridad requiere esfuerzos constantes y adicionales
por parte de tu banco.
Tu empresa se enfrenta a la presión necesaria y significativa
de cumplir numerosos reglamentos y leyes
y corre el riesgo de sufrir daños
tanto financieros como de reputación si no lo hace.

[Se ha reportado](https://www.financierworldwide.com/financial-institutions-avoiding-sanctions-breaches)
que, en los últimos años,
Estados Unidos ha sido el país con mayor número de sanciones
impuestas por diversos organismos reguladores.
De 2008 a 2022,
los bancos estadounidenses pagaron 37.000 millones de dólares por infracciones,
lo que convierte a Estados Unidos en el país más multado del mundo.
Algunos de estos bancos son JPMorgan Chase, BNP Paribas, HSBC y Goldman Sachs.

El cumplimiento de estándares es importante
no solo por las razones obvias de proteger los datos de tus clientes
y evitar multas.
También porque los marcos normativos proporcionan
un plan de trabajo estructurado para adoptar mejores prácticas
que promueven la transformación digital y mejoran la seguridad en general.
Muchos de estos marcos requieren un enfoque proactivo
para identificar y abordar las debilidades del sistema
antes de que los ciberdelincuentes puedan explotarlas.
Como resultado,
las pruebas de seguridad se convierten en un método preventivo de primer orden
para instituciones financieras como la tuya.
Este artículo explicará la importancia de anticiparse a los ciberataques,
los reglamentos clave que influyen en los bancos
y cómo la gestión de las vulnerabilidades garantiza su cumplimiento.

## Los retos a los que se enfrentan los bancos

Los bancos se enfrentan a numerosos retos
a la hora de cumplir los distintos reglamentos y estándares de ciberseguridad.
Un reto importante es el complejo entorno normativo.
Los bancos operan bajo una gran variedad de reglamentos locales y nacionales,
cada una con su propio conjunto de requisitos de ciberseguridad.
Mantenerse al día con estos requisitos cambiantes puede ser una tarea ardua.

Otro reto es la continua evolución de las ciberamenazas.
Constantemente se descubren nuevas vulnerabilidades
y los ciberdelincuentes desarrollan métodos de ataque creativos.
Estos ataques pueden dirigirse no solo a los sistemas de los bancos,
sino también a sus proveedores.
Esto hace que la superficie de ataque sea más amplia
y obliga a que los bancos sean proactivos e innovadores
en sus estrategias de seguridad.

La aplicación y el mantenimiento de medidas de ciberseguridad sólidas
es otro ámbito en el que los bancos suelen tener dificultades.
Esto requiere recursos considerables,
incluyendo personal informático cualificado,
tecnología sofisticada,
formación continua
y abogados expertos.
Los bancos pequeños pueden tener dificultades
para conseguir los recursos necesarios para lograr y mantener su cumplimiento.

Otros retos son el entorno de trabajo híbrido
y la complejidad que conlleva el trabajo a distancia.
Se suma a esto la necesidad de unos requisitos de responsabilidad estructurados
y más estrictos para los CEOs y CFOs.
A esto se le añade la complejidad de la jerga normativa,
que da lugar a interpretaciones abiertas.
Y, además de todo lo anterior,
el ritmo rápido de los avances tecnológicos obliga a los bancos
a actualizar periódicamente sus sistemas y medidas de seguridad.

## Las ventajas del cumplimiento con estándares

Los beneficios de cumplir con estándares van más allá
de evitar multas elevadas.
Las siguientes son algunas de las principales motivaciones
para cumplir los estándares:

- **Mejor protección de datos:**
  La principal ventaja es la protección de la privacidad.
  Al adherirse a las normas,
  tu banco puede mitigar significativamente el riesgo de filtración de datos.
  Además, proteger la información personal y financiera es crucial
  para mantener la confianza de los clientes.

- **Gestión de riesgos:** Cumplir estándares implica identificar,
  evaluar
  y gestionar los riesgos.
  Esto permite a tu banco contrarrestar proactivamente posibles ataques.

- **Responsabilidad y transparencia:** Cumplir estándares fomenta
  tanto la responsabilidad ante los organismos reguladores
  como la transparencia en las operaciones bancarias.

- **Eficacia operativa:** El cumplimiento adecuado de estándares puede conducir
  a operaciones más fluidas,
  ya que a menudo implica la simplificar procesos,
  lo cual mejora la eficiencia.

- **Ventaja competitiva:** Los bancos pueden diferenciarse en el mercado
  como instituciones seguras y confiables.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/pruebas-seguridad/"
title="Inicia ahora con la solución de pruebas de seguridad
de Fluid Attacks"
/>
</div>

## Reglamentos clave para la banca

Como ya se ha dicho,
los bancos tienen que navegar por un mar de leyes y reglamentos.
Cada uno de ellos pretende proteger a los clientes
de los riesgos de ciberseguridad manteniendo la confidencialidad,
integridad
y disponibilidad de los datos y sistemas financieros.
Las infracciones de estos reglamentos pueden acarrear cuantiosas multas,
pérdidas financieras,
daños a la reputación
y una mayor supervisión reglamentaria.
A continuación se desglosan algunos reglamentos y normas clave
que afectan a la ciberseguridad bancaria:

- **Estándar de Seguridad de Datos de Payment Card Industry (PCI DSS):**
  PCI DSS (por su nombre en inglés) aplica para cualquier organización
  que maneje información de tarjetas de crédito.
  Este estándar establece controles específicos
  para proteger la información de titulares de tarjetas,
  como el cifrado,
  los controles de acceso
  y las pruebas de seguridad periódicas.

  En cuanto a esto último, el requisito 11 de la PCI DSS,
  "Ponga a prueba regularmente la seguridad de los sistemas y de las redes",
  hace énfasis en la necesidad de realizar pruebas de seguridad continuas.
  El estándar [PCI DSS v4.0.1](https://docs-prv.pcisecuritystandards.org/PCI%20DSS/Standard/PCI-DSS-v4_0_1.pdf#lang=es)
  exige [escaneos de vulnerabilidades](../escaneo-de-vulnerabilidades/)
  internos y externos trimestralmente
  y después de cambios significativos,
  y [pruebas de penetración](../pentesting/) al menos una vez al año
  y después de cambios significativos.
  Las infracciones pueden acarrear un aumento de los costes
  de procesamiento de tarjetas de crédito,
  la pérdida de privilegios de cuenta
  e incluso procesos penales,
  con multas que oscilan entre miles y millones de dólares
  en función de la gravedad de la infracción
  y la presencia de filtraciones de datos.

  Nosotros, en Fluid Attacks, hemos establecido para nuestras pruebas
  [estos requisitos de *software*](https://help.fluidattacks.com/portal/en/kb/articles/criteria-compliance-pci)
  utilizando este estándar.

- **Gramm-Leach-Bliley Act (GLBA):** GLBA exige a las instituciones financieras
  que protejan la información personal no pública de los clientes.
  Por eso, obliga a aplicar medidas de seguridad
  para proteger los datos de los clientes
  frente a filtraciones de datos y accesos no autorizados.
  Es una ley aplicada por organismos como la Comisión Federal de Comercio
  (FTC, por su nombre en inglés),
  la Corporación Federal de Seguros de Depósitos
  (FDIC, por su nombre en inglés)
  y otros.

  A partir de 2023,
  la GLBA impone medidas de seguridad como pruebas de penetración
  y análisis de vulnerabilidades.
  Su [Safeguards Rule](https://www.ftc.gov/business-guidance/privacy-security/gramm-leach-bliley-act)
  exige a las instituciones financieras que desarrollen,
  apliquen
  y mantengan un programa integral de seguridad de la información.
  Las pruebas de penetración una vez al año son obligatorias,
  aunque las mejores prácticas sugieren realizarlas con mayor frecuencia,
  y el escaneo de vulnerabilidades debe realizarse cada seis meses.
  El incumplimiento de esta ley puede dar lugar a multas
  de hasta 51.744 dólares por infracción, cobradas por la FTC,
  demandas judiciales,
  daños a la reputación
  y un mayor escrutinio reglamentario.

  Aquí, en Fluid Attacks, hemos creado para nuestras pruebas
  [estos requisitos de *software*](https://help.fluidattacks.com/portal/en/kb/articles/criteria-compliance-glba)
  basados en esta ley.

- **Regulaciones de ciberseguridad
  del Departamento de Servicios Financieros de Nueva York (NYDFS):**
  Las regulaciones de ciberseguridad del NYDFS
  imponen controles específicos de ciberseguridad
  a las instituciones financieras que operan en Nueva York.
  Incluyen directrices para planes de respuesta a incidentes,
  requisitos de notificación en caso de filtraciones,
  autenticación multifactor
  y políticas de seguridad de proveedores.

  [23 NYCRR 500](https://www.dfs.ny.gov/system/files/documents/2023/03/23NYCRR500_0.pdf)
  exige pruebas de penetración anuales
  y evaluaciones de vulnerabilidades cada tres meses
  para identificar y mitigar los riesgos.
  También exige supervisión continua y prácticas de desarrollo seguro
  para las aplicaciones de uso interno.
  El incumplimiento puede acarrear multas elevadas,
  un aumento de las inspecciones y auditorías,
  demandas judiciales y daños a la reputación.

  Establecimos para nuestras pruebas [estos requisitos de *software*](https://help.fluidattacks.com/portal/en/kb/articles/criteria-compliance-nydfs)
  utilizando estas regulaciones.

- **Bank Secrecy Act (BSA):**
  BSA (traducido frecuentemente como "Ley de secreto bancario")
  se centra en la lucha contra el blanqueo de capitales
  (AML, por su nombre en inglés)
  y la lucha contra el terrorismo financiero (CFT, por su nombre en inglés).
  Exige que las instituciones financieras apliquen programas
  para detectar y notificar actividades sospechosas.
  Aunque BSA no obliga explícitamente a realizar pruebas de seguridad,
  exige medidas de seguridad sólidas para garantizar la integridad
  y confidencialidad de los datos utilizados en el cumplimiento de la AML.
  Los reguladores que administran BSA,
  como Financial Crimes Enforcement Network ([FinCEN](https://www.fincen.gov/)),
  sí imponen requisitos que exigen pruebas de seguridad.

  Las multas por infracciones intencionadas pueden alcanzar los 100.000 dólares
  por infracción,
  con consecuencias adicionales que incluyen procesos penales,
  daños a la reputación,
  aumento de la supervisión
  y posible revocación de licencias bancarias.

- **SWIFT Customer Security Controls Framework:**
  SWIFT (traducido frecuentemente como
  "Sociedad para las Comunicaciones Interbancarias y Financieras Mundiales")
  proporciona un sistema de mensajería financiera global
  para transacciones internacionales.
  Su CSP (traducido como "Programa de seguridad del cliente")
  y su [CSCF](https://www2.swift.com/knowledgecentre/rest/v1/publications/cscf_dd/53.0/CSCF_v2024_20231017.pdf)
  (traducido como "Marco de controles de seguridad del cliente")
  establecen controles obligatorios
  y de asesoramiento para proteger los entornos SWIFT locales.

  El principio 2 exige el escaneo anual de vulnerabilidades
  con su respectiva documentación de la remediación,
  mientras que el principio 7 recomienda pruebas de penetración proactivas.
  El incumplimiento puede aumentar el riesgo de ciberataques
  y dar lugar a la exclusión de la red SWIFT,
  interrumpiendo las transacciones internacionales.
  Un [ejemplo notable](https://globaleurope.eu/europes-future/swift-exclusion-is-fine-sanctioning-the-russian-central-bank-is-better/)
  es la exclusión de Rusia de SWIFT debido a las sanciones de la Unión Europea
  relacionadas con la guerra contra Ucrania.

  Por nuestra parte,
  establecimos para nuestras pruebas [estos requisitos de *software*](https://help.fluidattacks.com/portal/en/kb/articles/criteria-compliance-swiftcsc/)
  utilizando este marco.

- **Federal Information Security Management Act (FISMA):**
  FISMA (traducido frecuentemente como
  "Ley federal de gestión de la seguridad de la información")
  exige que los organismos federales y sus contratistas,
  incluidos los bancos que trabajan con sistemas federales,
  desarrollen, documenten y apliquen
  un programa de seguridad de la información.
  Las evaluaciones periódicas de seguridad,
  la supervisión continua
  y los planes de respuesta a incidentes son esenciales para el cumplimiento.
  El incumplimiento puede conllevar la cancelación del contrato federal,
  sanciones económicas
  y daños a la reputación.

  Establecimos para nuestras pruebas [estos requisitos de *software*](https://help.fluidattacks.com/portal/en/kb/articles/criteria-compliance-f/)
  utilizando esta ley.

- **Ley de Privacidad del Consumidor de California (CCPA):**
  CCPA (por su nombre en inglés) es principalmente
  una ley de privacidad de datos que afecta a las prácticas de ciberseguridad
  de los bancos que operan en California.
  La ley concede a los residentes de California
  derechos sobre sus datos personales
  e impone requisitos a las empresas para proteger esos datos.
  Las medidas de protección de datos,
  las notificaciones de infracciones
  y la gestión de los derechos de los consumidores
  son esenciales para cumplir con CCPA.
  Las multas por infracción pueden alcanzar hasta 7.500 dólares
  por cada infracción intencionada
  y 2.500 dólares por cada infracción no intencionada,
  además de posibles demandas judiciales.

  [Estos](https://help.fluidattacks.com/portal/en/kb/articles/criteria-compliance-ccpa/)
  son los requisitos de *software* que hemos establecido con esta ley
  para nuestras pruebas.

### Algunos marcos útiles para el cumplimiento de la normativa bancaria

Los bancos pueden aprovechar varios marcos de ciberseguridad
para mejorar su postura de seguridad
y garantizar una protección sólida contra las ciberamenazas.
Algunos marcos clave cuya aplicación puede resultar beneficiosa son:

- **Recursos de Federal Financial Institutions Examination Council (FFIEC):**
  [FFIEC](https://www.ffiec.gov/) (traducido frecuentemente como
  "Consejo Federal de Examen de Instituciones Financieras")
  es un organismo interinstitucional
  que proporciona una amplia orientación sobre ciberseguridad
  y pruebas de seguridad para las instituciones financieras.
  Aunque FFIEC no impone reglamentos específicos,
  sus directrices son utilizadas por los reguladores bancarios federales
  (como la FDIC, OCC y la Reserva Federal)
  para evaluar las prácticas de ciberseguridad
  de las instituciones financieras.

- **Marco de Ciberseguridad
  del Instituto Nacional de Normas y Tecnología (NIST):**
  Este marco proporciona orientación
  para gestionar los riesgos de ciberseguridad.
  Aunque es voluntario,
  el marco de [NIST](https://help.fluidattacks.com/portal/en/kb/articles/criteria-compliance-nist/)
  puede ser una herramienta valiosa
  para que las instituciones bancarias construyan un programa integral
  de ciberseguridad.
  El marco hace énfasis en la identificación de vulnerabilidades,
  la implementación de controles de seguridad,
  el monitoreo continuo
  y las pruebas periódicas.

- **ISO 27001:**
  Este estándar internacional proporciona métodos estructurados
  para proteger los datos sensibles,
  que incluyen la información financiera,
  la propiedad intelectual,
  la información de los empleados
  y la información confiada a terceros.
  El sistema de gestión de la seguridad de la información
  (SGSI, por su nombre en inglés)
  de un banco debe establecerse,
  implementarse,
  mantenerse
  y mejorarse continuamente
  de acuerdo con los criterios descritos en [ISO 27001](https://help.fluidattacks.com/portal/en/kb/articles/criteria-compliance-iso27001).

- **Reglamento General de Protección de Datos (RGPD):**
  Este reglamento europeo (conocido en inglés como GDPR)
  se aplica a los bancos de la UE y de fuera de la UE
  que tienen clientes en Europa.
  Se centra en proteger los datos personales
  y garantizar la privacidad de los datos.
  Este reglamento proporciona ejemplos excelentes
  de las medidas que los bancos deben aplicar
  para salvaguardar la información de sus clientes.
  [RGPD](https://help.fluidattacks.com/portal/en/kb/articles/criteria-compliance-gdpr)
  exige medidas técnicas y organizacionales adecuadas
  para las instituciones financieras,
  ayudándoles así a reforzar su postura de seguridad
  y fomentando la excelencia en las operaciones.

## Recomendaciones para el cumplimiento en la ciberseguridad bancaria

Dar prioridad a una postura sólida de ciberseguridad puede ser un reto,
pero con un enfoque proactivo,
los bancos pueden lograr el cumplimiento de estándares
y de esta manera crear medidas de seguridad fuertes
en el desarrollo de *software*.
Ya habíamos hablado anteriormente sobre cómo los [bancos](../ciberseguridad-bancaria/#ciber-higiene-bancaria),
y en general las [instituciones financieras](../ciberseguridad-servicios-financieros/),
deberían seguir las mejores prácticas para crear servicios seguros.
Pero aquí ofrecemos algunas recomendaciones clave
para desarrollar aplicaciones de software seguras para los bancos:

- **Establecer requisitos de seguridad al principio del SDLC:**
  El uso de modelos de amenazas al principio del proceso de desarrollo
  puede ayudar a identificar posibles riesgos de seguridad
  y crear medidas para enfrentarlos.

- **Integrar prácticas de codificación segura
  en todo el proceso de desarrollo:**
  La [revisión de código seguro](../../soluciones/revision-codigo-fuente/),
  tanto manual como automatizada, puede reducir los riesgos de seguridad
  antes de desplegar a producción.

- **Identificar las vulnerabilidades de las aplicaciones:**
  Realizar pruebas de seguridad de aplicaciones estáticas ([SAST](../../producto/sast/))
  y pruebas de seguridad de aplicaciones dinámicas ([DAST](../../producto/dast/)).

- **Ejecutar evaluaciones de vulnerabilidades:**
  Realice [pruebas de penetración](../../soluciones/pruebas-penetracion-servicio/)
  y análisis de vulnerabilidades
  para identificar los puntos débiles explotables
  en el sistema de *software* del banco.

- **Crear una estrategia de gestión de vulnerabilidades:**
  [Este proceso](../../soluciones/gestion-vulnerabilidades/)
  implica la identificación,
  evaluación,
  priorización
  y notificación de vulnerabilidades
  a lo largo de varias etapas del ciclo de desarrollo
  para minimizar la exposición al riesgo.

- **Implementar controles de seguridad para terceros:**
  Utilizar controles para garantizar la seguridad de los componentes
  y bibliotecas de *software*, como mantener un SBOM actualizado.
  También es imprescindible evaluar las prácticas de seguridad
  de [terceros proveedores](../cadena-suministro-sector-financiero/).

- **Implementar un proceso de gestión de parches:**
  Asegúrese de que todo el *software* y los sistemas están actualizados
  con los últimos parches de seguridad.

- **Desarrollar un mapa de cumplimiento:**
  Identifique las normas de ciberseguridad que aplican a su banco,
  describa los pasos necesarios para lograr el cumplimiento
  y revise continuamente las políticas de ciberseguridad.

- **Documente claramente las políticas
  y procedimientos de ciberseguridad de su banco:**
  Estas políticas deben describir las funciones y responsabilidades,
  la tecnología utilizada
  y los procedimientos de notificación de incidentes.

Fluid Attacks ofrece una solución completa
que aborda la mayoría de las recomendaciones mencionadas.
Nuestra solución todo en uno tiene como objetivo
identificar todas las vulnerabilidades a lo largo del SDLC
y, lo que es más importante, permitir su remediación antes de los despliegues.
Ideal para los bancos que buscan mantener el cumplimiento de estándares,
nuestra solución [Hacking Continuo](../../servicios/hacking-continuo/)
ayuda a identificar los problemas
y proporciona informes detallados necesarios para una gestión precisa.
Nuestra [plataforma](../../plataforma/)
dispone de una sección para mostrar estos detalles.
Al integrarse con otros [sistemas](https://help.fluidattacks.com/portal/en/kb/integrate-with-fluid-attacks),
ofrece opciones de remediación que mejoran la gestión de vulnerabilidades
en comparación a cuando la plataforma se usa por sí sola.

Queremos ayudar a tu banco a garantizar el cumplimiento.
[Contáctanos](../../contactanos/) ahora para empezar.
