---
slug: blog/filtraciones-datos-sector-minorista/
title: Filtraciones de datos en el sector minorista
date: 2024-10-21
subtitle: Los siete ciberataques más exitosos contra esta industria
category: ataques
tags: ciberseguridad, empresa, riesgo, tendencia
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1729549213/blog/retail-sector-data-breaches/cover_retail_sector_data_breaches.webp
alt: Foto por Charles Etoroma en Unsplash
description: Los ataques que afectan negativamente a algunas empresas pueden servir de advertencia y lección para otras, al menos para las aún rezagadas en ciberseguridad.
keywords: Filtraciones De Datos En Minoristas, Violaciones De Datos Sector Minorista, Ciberseguridad En La Industria Minorista, Ciberataques Sector Minorista, Ataques Contra Comercios, Terminales De Punto De Venta, Target, Hacking Etico, Pentesting
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Escritor y editor
source: https://unsplash.com/photos/black-shopping-cart-on-white-floor-u0F1bva4Qh0
---

A lo largo de los años,
muchas marcas conocidas del sector minorista
han ocupado los titulares de noticias en todo el mundo.
Aunque esto puede ser producto de logros,
también ocurre cuando los minoristas son víctimas de ciberataques
que afectan significativamente a los datos de sus clientes
y, en consecuencia, a las finanzas y reputación de la empresa.

Hace unos días,
[publicamos un blog *post*](../ciberseguridad-sector-minorista/)
en el que exponemos estadísticas, amenazas, retos
y mejores prácticas en ciberseguridad para este sector.
En esta ocasión,
nos gustaría compartir algunos de los casos más destacados de ciberataques
y filtraciones o violaciones de datos
sufridos por conocidos minoristas multinacionales.
Estos casos pueden servir como advertencia para otras compañías
dentro y fuera de esta industria
y como recurso de aprendizaje para evitar cometer errores similares
en materia de ciberseguridad.

## 7. Under Armour (2018)

Under Armour, Inc. es una [compañía estadounidense](https://en.wikipedia.org/wiki/Under_Armour)
de prendas de vestir
que fabrica y vende ropa deportiva, calzado y accesorios
diseñados para mejorar el rendimiento atlético.
[En marzo de 2018](https://www.securityweek.com/under-armour-says-150-million-affected-data-breach/),
este minorista informó públicamente de una filtración de datos
después de que su aplicación MyFitnessPal fuera hackeada,
afectando aproximadamente a **150 millones de cuentas de usuario**.
Supuestamente, desde febrero de ese mismo año,
los atacantes obtuvieron direcciones de correo electrónico,
nombres de usuario y contraseñas cifradas,
pero no información de seguridad social ni de tarjetas de pago.
No obstante,
la empresa [pidió a sus usuarios](https://www.zdnet.com/article/under-armour-reports-150-million-myfitnesspal-accounts-hacked/)
a través de diversos canales de comunicación
que cambiaran sus contraseñas.
Entre las fuentes revisadas para este *post*,
no hay ningún informe que revele el tipo de ataque
o el coste financiero de esta violación de datos para Under Armour.

## 6. Forever 21 (2017-2023)

Forever 21 es una [multinacional de moda](https://en.wikipedia.org/wiki/Forever_21)
conocida por su ropa y accesorios de tendencia a precios asequibles,
principalmente para mujeres jóvenes y adolescentes.
En noviembre de 2017,
[la empresa estaba investigando](https://www.zdnet.com/article/forever-21-reveals-potential-data-breach/)
una posible filtración de datos
que comprometía la información personal y de tarjetas de pago de sus clientes.
La investigación se centró en las transacciones
realizadas entre marzo y octubre de ese año.
[Poco después](https://www.zdnet.com/article/forever-21-investigation-reveals-malware-present-at-some-stores/),
Forever 21 confirmó la filtración de datos.

Al parecer,
este minorista tenía sistemas o terminales de punto de venta (TPV)
en algunas de sus tiendas
que aún no habían recibido o nunca recibieron las actualizaciones
de cifrado y autenticación
que se suponía que había empezado a implementar en 2015.
A partir de ahí,
los atacantes accedieron a la red de Forever 21
e instalaron *malware* para robar información.
Aunque se habla de datos de tarjetas de crédito comprometidos,
no se informó del número de clientes afectados.

[Fue en 2023](https://techcrunch.com/2023/08/31/forever-21-data-breach-half-million/)
cuando los medios de comunicación hablaron
de **más de medio millón de afectados**.
Pero no, no en el ataque previamente mencionado,
sino en uno nuevo,
que al parecer comenzó en enero de ese año.
Sin embargo,
[parece que](https://therecord.media/forever-21-data-breach)
entre los afectados, en este caso,
solo había empleados actuales y antiguos del minorista
cuya información personal se vio comprometida.
Aunque no se reveló el tipo de ataque,
los medios de comunicación dedujeron
que podría haberse tratado de un [ataque de *ransomware*](../../../blog/ransomware/).
Hasta ahora,
tampoco se han revelado los costes financieros de Forever 21.

## 5. eBay (2014)

eBay Inc. es una [compañía estadounidense](https://en.wikipedia.org/wiki/EBay)
de comercio electrónico
en la que personas y empresas venden y compran
una amplia variedad de bienes y servicios en todo el mundo.
En mayo de 2014,
[eBay pidió a sus usuarios](https://www.zdnet.com/article/ebay-change-your-passwords-due-to-cyberattack/)
que cambiaran sus contraseñas
debido a un ataque que puso en peligro su base de datos
de información personal.
Esta incluía nombres, fechas de nacimiento,
direcciones de correo electrónico,
números de teléfono y contraseñas "cifradas".
Lo que aparentemente no fue robado fueron los datos financieros,
que se almacenaban por separado.
Como en el caso de Under Armour, descrito anteriormente,
llama la atención que eBay pidiera a sus clientes
que cambiaran sus contraseñas,
como si [no confiara lo suficiente](https://www.troyhunt.com/the-ebay-breach-answers-to-questions/)
en sus métodos de cifrado.

Los atacantes habían conseguido acceder a la red de la empresa
comprometiendo las credenciales de acceso de algunos empleados
unos meses atrás.
Se supone que el número total de **usuarios afectados**
podría ascender a **145 millones**.
Pero, ¿lograron los *hackers* malintencionados obtener todos los datos
de unas pocas cuentas de empleados?
Eso resulta extraño.
Supuestamente,
al final no se registró ningún fraude financiero,
pero toda esa información robada sabemos que puede ser útil
para los ciberdelincuentes, por ejemplo,
en sus [campañas de ingeniería social](../../../blog/social-engineering/).
Al parecer, no se han revelado los costos para eBay.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/pruebas-seguridad/"
title="Inicia ahora las pruebas de seguridad de Fluid Attacks"
/>
</div>

## 4. Neiman Marcus (2013-2020)

Neiman Marcus es una [cadena estadounidense de almacenes](https://en.wikipedia.org/wiki/Neiman_Marcus)
que ofrece marcas de diseño de alta gama en moda,
accesorios y artículos para el hogar.
[En 2014](https://www.zdnet.com/article/neiman-marcus-1-1-million-cards-compromised/),
este minorista denunció haber sido víctima de una filtración de datos
en la que se vio comprometida información
de **1,1 millones de tarjetas de pago de clientes**.
El *malware* había sido instalado en sus sistemas
y había actuado desde mediados de julio hasta finales de octubre de 2013.
Para entonces,
dijeron que no había conexión con el caso de Target
(reportado como número uno de la lista en este *post*),
pero muchas tarjetas de pago único ya habían sido utilizadas
de forma fraudulenta.

[A principios de 2019](https://www.zdnet.com/article/neiman-marcus-agrees-to-1-5-million-data-breach-settlement/),
la compañía llegó a un acuerdo con diferentes estados de la nación
para entregar **1,5 millones de dólares**
en respuesta a este incidente de seguridad.
Por aquel entonces, se afirmaba que,
en realidad, el número de tarjetas comprometidas rondaba las **370 mil**,
de las cuales más de **9 mil** habían sido utilizadas fraudulentamente.

A pesar de la respuesta de Neiman Marcus,
que supuestamente también implicaba mejoras en la ciberseguridad,
en 2021 la compañía comunicó una [nueva violación de datos](https://www.zdnet.com/article/neiman-marcus-breach-includes-payment-card-numbers-and-expiration-dates/).
Al parecer,
se había producido hacía más de un año,
en mayo de 2020,
pero se descubrió en septiembre del año siguiente.
Parece que unos **4,6 millones de cuentas de clientes**,
incluidos los números de sus tarjetas de pago e información personal,
se vieron comprometidos.
El minorista luego dijo que
aproximadamente 3,1 millones de tarjetas de pago y de regalo virtuales
se vieron afectadas para estos clientes.

## 3. TJX Companies (2007)

The TJX Companies, Inc. es una [multinacional estadounidense](https://en.wikipedia.org/wiki/TJX_Companies)
de venta al por menor
que gestiona una cadena de grandes almacenes
que ofrece ropa de marca y moda para el hogar con descuento.
Esta compañía reveló a principios de 2007
que los registros de sus clientes habían estado en peligro
[durante casi dos años](https://www.zdnet.com/article/tjx-says-45-7-million-customer-records-were-compromised/).
Desde julio de 2005,
los ciberdelincuentes habían accedido a la red de TJX
e instalado programas maliciosos para robar la información personal
y financiera de al menos **45,7 millones de clientes**.
([Al parecer](https://www.zdnet.com/article/the-worst-it-security-incidents-of-2007/),
la cifra comunicada era muy inferior a la real;
posteriormente se informó de que **superaba los 95 millones**).
Las transacciones con tarjetas de crédito y débito
se vieron afectadas en varias tiendas de TJX
en países como Estados Unidos, Canadá, Puerto Rico y el Reino Unido.

Supuestamente,
los atacantes obtuvieron dicho acceso
a través de algunas de las TDV de TJ Maxx,
una de las filiales de TJX.
[Se dice que](https://www.zdnet.com/article/tjxs-failure-to-secure-wi-fi-could-cost-1b/)
su seguridad era bastante deficiente.
Tenían fallos en la encriptación básica
y en la seguridad del control de acceso.
Además,
la red inalámbrica de TJX estaba [aparentemente protegida](https://www.zdnet.com/article/wi-fi-hack-caused-tk-maxx-security-breach/)
por Wired Equivalent Privacy (WEP),
una de las formas más débiles de seguridad para este tipo de redes.
Los *hackers* obtuvieron las credenciales
de inicio de sesión de los empleados,
crearon sus propias cuentas y, durante todo el tiempo señalado,
recopilaron datos relacionados con las transacciones de los clientes.
A partir de ahí,
podían vender esta información en el mercado negro
o utilizarla para el robo de activos.
Al año siguiente,
[algunos *hackers*](https://www.zdnet.com/article/alleged-tjx-hackers-charged/)
ya habían sido implicados y acusados
de este y otros delitos similares.

Los costos iniciales de TJX para hacer frente a la filtración de datos,
los informes de los usuarios y las mejoras de seguridad
ascendieron a **5 millones de dólares**,
que no es nada comparado con lo que vino luego.
[Meses después](https://www.zdnet.com/article/tjx-data-breach-costs-17-million-and-growing/),
se añadieron otros **12 millones de dólares** en cargos,
y [algunos medios estimaron](https://www.zdnet.com/article/tjxs-failure-to-secure-wi-fi-could-cost-1b/)
que la suma alcanzaría los **miles de millones de dólares**.
Con el paso del tiempo,
la compañía se vio afectada por demandas interpuestas por usuarios
e investigaciones y multas de organismos gubernamentales
por incumplimiento de las leyes de protección al cliente.

## 2. Home Depot (2014)

The Home Depot, Inc. es un importante [minorista estadounidense](https://en.wikipedia.org/wiki/Home_Depot)
que ofrece una amplia gama de productos y servicios de mejora del hogar.
[En septiembre de 2014](https://www.zdnet.com/article/home-depot-confirms-payment-system-attack/),
esta empresa confirmó
que sus sistemas de pago habían sido objeto de un ataque de *malware*
similar al recibido por Target Corporation (véase el caso a continuación),
que había comenzado en abril.
Presuntamente,
los atacantes utilizaron las credenciales de un proveedor externo
para acceder a la red de Home Depot
e instalaron malware para comprometer los sistemas PoS
y robar datos de los clientes que utilizaban tarjetas de pago
en Estados Unidos y Canadá.
[Según la empresa](https://www.zdnet.com/article/home-depot-53-million-email-addresses-swiped-too/),
ese *malware* no se había utilizado en ataques anteriores
y estaba diseñado para eludir la detección por parte de *software* antivirus.

[En este caso](https://www.zdnet.com/article/home-depot-agrees-to-17-5m-settlement-over-2014-data-breach/),
**más de 40 millones de clientes** se vieron afectados.
Inicialmente,
se informó de que estaban comprometidos
**56 millones de números de tarjetas de crédito y débito**,
pero en noviembre del mismo año [se declaró que](https://www.zdnet.com/article/home-depot-53-million-email-addresses-swiped-too/)
también estaban afectadas
**53 millones de direcciones de correo electrónico**.
Estos datos de tarjetas de pago podrían haber sido utilizados por delincuentes
para realizar compras fraudulentas en línea o crear tarjetas clonadas.

Una vez concluida la investigación,
[Home Depot tuvo que añadir](https://www.zdnet.com/article/home-depot-56-million-payment-cards-affected-by-cyberattack/)
mejoras de encriptación a sus TPV.
Al parecer,
también empezaron a acelerar la implantación de la tecnología *chip-and-pin*.
Además,
tuvieron que contratar a un jefe de seguridad de la información (CISO),
formar a su personal en concienciación sobre seguridad
e implantar la autenticación de dos factores (2FA), *firewalls*
y [pruebas de penetración](../que-es-prueba-de-penetracion-manual/),
[entre otras medidas de seguridad](https://www.secureworld.io/industry-news/5-home-depot-changes-following-data-breach).
Años más tarde,
la compañía acabó pagando **17,5 millones de dólares**
en acuerdos con distintos estados,
lo que suponía solo una parte de los costes totales,
a los que se sumaron los litigios de clientes y diversas instituciones.

## 1. Target (2013)

Target es una de las mayores empresas [minoristas estadounidenses](https://en.wikipedia.org/wiki/Target_Corporation).
Ofrece un amplio surtido de productos,
como ropa, artículos para el hogar y comestibles,
centrándose en el valor y el diseño.
Target [sufrió un ciberataque](https://www.zdnet.com/article/anatomy-of-the-target-data-breach-missed-opportunities-and-lessons-learned/)
a finales de noviembre de 2013,
al parecer en pleno Black Friday.
Unas dos semanas después,
su personal descubrió la brecha
y la denunció al Departamento de Justicia de Estados Unidos.
El ataque fue mitigado al cabo de dos días.

Al parecer,
fue suficiente con comprometer a un solo proveedor de terceros
de los muchos que podrían haber sido atacados
para que el impacto fuera exitoso.
En concreto, se trató de Fazio Mechanical,
un contratista de refrigeración
cuyas debilidades de ciberseguridad permitieron a los atacantes
entrar en la red corporativa de Target.
El vector de ataque fue un correo electrónico tipo [*phishing*](../../../blog/phishing/),
que permitió instalar Citadel
(una variante del troyano bancario Zeus)
en las máquinas de Fazio,
una compañía que no utilizaba adecuadamente *software antimalware*.
Una vez dentro de la red de Target,
que aparentemente estaba mal segmentada,
los *hackers* pudieron encontrar y explotar vulnerabilidades
para moverse lateralmente y luego obtener privilegios
y tomar el control de los servidores.
Por último,
estos se infiltraron e infectaron las TPV de Target con *malware*
para extraer información de tarjetas de crédito y débito
y venderla en el mercado negro.

Los atacantes robaron datos
de unas 40 millones de tarjetas de crédito y débito,
junto con información personal de hasta 70 millones de clientes.
Los costos de Target superaron los 200 millones de dólares,
incluidos honorarios de abogados, indemnizaciones
e inversiones en mejoras de seguridad.
El costo real puede haber sido mucho mayor,
teniendo en cuenta la pérdida de ventas,
la pérdida de clientes y el daño a la cotización de sus acciones.
En su momento fue una de las violaciones de datos más importantes
de la historia.
[Incluso se dice](https://www.zdnet.com/article/the-target-breach-two-years-later/)
que fue el primer caso
en el que el CEO de una gran corporación
fue despedido a causa de una filtración de datos.
El incidente erosionó significativamente la confianza pública
en las prácticas de seguridad de Target.
Conllevó demandas judiciales, multas reglamentarias
y una cobertura negativa en los medios de comunicación,
lo que afectó a su imagen de marca
y a la fidelidad de sus clientes durante años.

La filtración de datos de Target y los demás ciberataques aquí descritos
pueden servir de llamado de atención para todos
o al menos muchos miembros del sector minorista.
Estos casos pusieron de manifiesto, entre otras cosas,
las vulnerabilidades de sus redes y terminales de puntos de venta (TPV),
la presencia de trabajadores poco cualificados en ciberseguridad
y la necesidad de medidas de seguridad más estrictas y actualizadas.
Tanto si eres minorista como si no,
hoy en día la ciberseguridad no es solo una necesidad,
sino también una obligación.
Mediante la integración de pruebas de seguridad automatizadas y manuales,
Fluid Attacks está aquí para ayudarte a evitar ser la próxima víctima
que ocupe los titulares.
[Contáctanos](../../contactanos/).
