---
slug: blog/escaneos-sca/
title: Escaneos SCA
date: 2022-10-26
subtitle: ¿Qué es el SCA y qué podemos ganar con él?
category: filosofía
tags: ciberseguridad, pruebas de seguridad, software, empresa
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1666821717/blog/sca-scans/cover_sca_scans.webp
alt: Foto por Dan Freeman en Unsplash
description: Después de leer este artículo, entenderás qué es el análisis de composición de software (SCA) y qué podemos obtener de los escaneos SCA.
keywords: Analisis De Composicion De Software, Sca, Componentes De Codigo Abierto, Software De Codigo Abierto, Analisis Sca, Dependencias, Pruebas De Seguridad, Pentesting, Hacking Etico
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Escritor y editor
source: https://unsplash.com/photos/VAWqURK_Th0
---

La velocidad es fundamental en el sector del desarrollo de *software*,
el cual está en constante evolución.
Los equipos de desarrolladores responden a la presión
de entregar productos de alta calidad en el menor tiempo posible.
Con ello, se pretende que sus empresas sigan siendo dinámicas
y competitivas en sus mercados. Hoy en día,
como apoyo a este desarrollo acelerado de,
por ejemplo, aplicaciones,
los desarrolladores tienen muchas creaciones ajenas a su disposición.
Las aplicaciones en demanda pueden ser cada vez más complejas,
por lo que empezar de cero y recrear todo el trabajo de otros
(una práctica ya "anticuada")
podría considerarse un sueño inalcanzable.

A lo largo de los años,
comunidades de desarrolladores han estado colaborando
para crear y perfeccionar paquetes
o bloques de código con distintas funcionalidades
(p. ej., construcción de interfaces de usuario,
análisis de datos, operaciones criptográficas).
Estos componentes de código abierto
(p. ej., bibliotecas, imágenes de contenedores, módulos)
pueden utilizarse de forma continua
y gratuita para construir nuevos proyectos.
Lo que hacen es ayudar a reducir no solo el tiempo
sino también los costos
(como los derivados de la compra de *software* propietario)
y ofrecer oportunidades para la innovación
y el apoyo de la comunidad.

Recientemente [se ha afirmado que](https://www.darkreading.com/application-security/dependency-problems-increase-for-open-source-components)
una aplicación de *software* promedio depende de más
de 500 bibliotecas y componentes de código abierto.
Y es que muchas veces,
las bibliotecas utilizadas en las aplicaciones
traen consigo otras dependencias.
Existen dependencias directas y transitivas.
Estas últimas son los componentes necesarios
para los componentes directamente vinculados a la aplicación.
También se ha dicho que, hoy en día,
casi todas las aplicaciones existentes hacen uso de estos recursos,
e incluso se ha estimado que el código abierto
constituye más del 75%
u 80% del código de la aplicación promedio.
[Como compartimos](../../../blog/vulns-triage-synopsys/)
alguna vez en este blog,
el código propietario acaba sirviendo más como ensamblador
e invocador de funciones.
(Para más detalles sobre el *software* de código abierto,
[lee este artículo](../../../blog/look-inside-oss/).)

Sin embargo,
surgen varios problemas con el crecimiento del ecosistema
de código abierto y el amplio uso
que las aplicaciones hacen de estos recursos.
Muchos desarrolladores y empresas no prestan suficiente atención
a los componentes que utilizan.
Pueden perder fácilmente la pista del material
que tienen en sus productos y de las dependencias asociadas.
La gestión de los árboles de dependencias es cada vez más compleja,
y la exposición al riesgo aumenta a medida
que se introducen componentes en las aplicaciones.
Esto último se debe a la existencia de defectos
y vulnerabilidades en estos recursos.
Muchas organizaciones no tienen en cuenta las fallas de calidad
y seguridad que pueden presentar los componentes de código abierto.
Aquí es donde entra en juego el proceso
en el que nos centramos en este artículo:
el [análisis de composición de *software*](../../producto/sca/)
(SCA, por su nombre en inglés).
(Por cierto,
para saber qué hay que tener en cuenta a la hora de elegir
el código abierto que se va a utilizar,
[lee este *post*](../../../blog/choosing-open-source/)).

## Definición de análisis de composición de *software*

SCA es una metodología de análisis
para la evaluación de aplicaciones de *software*.
En ella,
las herramientas automatizadas se centran en revisar
los componentes de código abierto y dependencias de una aplicación.
Pueden reportar no solo qué componentes se han implementado,
sino también su estado de seguridad
y posibles conflictos de licencia.
De este modo, cuando integras razonablemente el escaneo
con herramientas SCA en tu ciclo de vida de desarrollo de *software*
(SDLC, por su nombre en inglés), pretendes garantizar que el *software*
proveniente de otros grupos de desarrollo se utilice correctamente
y represente seguridad tanto para tu aplicación
como para la cadena de suministro de *software*.

## ¿Qué podemos obtener de los escaneos SCA?

Como dijimos,
los escaneos SCA pueden reportar
los componentes de código abierto de la aplicación.
En concreto,
las herramientas pueden generar un ***software bill of materials***
(SBOM, por su nombre en inglés).
Se trata de un inventario,
una lista detallada de recursos y dependencias.
Normalmente,
se espera que un SBOM proporcione el nombre de cada componente,
la versión, la ruta del archivo o la ubicación en la aplicación escaneada,
la fecha de publicación, la fuente,
la suma de verificación o *checksum* y la información de licencia,
entre otros datos.
Los SBOM pueden generarse escaneando archivos de manifiesto
(archivos con metadatos para un conjunto de archivos)
o artefactos de compilación
(para aplicar huellas digitales binarias).
Incluso puede utilizarse una mezcla de ambos enfoques
para obtener reportes más precisos.
Conseguirlo dependerá de las capacidades de las herramientas de SCA.
Los SBOM suelen entregarse como archivos de texto en formatos
JSON, XML o similares.

Después del SBOM,
uno de los objetivos de los escaneos SCA
puede ser ayudar a detectar **conflictos de licencia**.
La idea es que el propietario de la aplicación pueda resolverlos
y evitar futuras disputas legales,
como las relacionadas con la propiedad intelectual comprometida.
El SCA,
en este caso,
identifica las licencias de *software* de código abierto asociadas
a los componentes que se utilizan en la aplicación para compararlas
con las políticas de la organización implicada.
Aunque el acceso al código fuente de esos recursos es libre,
su uso puede estar limitado por algunas restricciones.
Las licencias para el código abierto pueden variar
en sus requisitos para derechos como los de copia,
modificación y redistribución.

Sabemos que hay cientos de licencias de código abierto
que tienen requisitos únicos para las personas
que planean utilizar los recursos con fines personales o comerciales.
También hay licencias con una gran variedad de requisitos en su interior.
Pueden ser tantas que llega a considerarse
un [aspecto negativo](https://en.wikipedia.org/wiki/Open-source_software)
del movimiento de código abierto
porque a menudo es difícil comprender
las implicaciones legales de las diferencias entre licencias.
Los tipos de licencias más comunes
en el código abierto son las categorizadas como permisivas.
Estas tienden a gestionar menos restricciones en el uso de componentes.
A menudo no exigen compensación monetaria o intelectual y,
por tanto, representan un riesgo bajo.
Por otro lado,
las mayor riesgo, especialmente para uso comercial,
son las llamadas licencias recíprocas o *copyleft*.
Con ellas se exige alguna compensación por el uso del código.

Por ejemplo,
a una empresa de "código cerrado",
la licencia de un componente de código abierto
podría estar pidiéndole que, al utilizar ese recurso,
su aplicación también sea de código público.
Otra petición podría ser que los cambios de la empresa
en el componente también estuvieran disponibles
para su inclusión en el proyecto original comunitario.
La gestión de un recurso de este tipo
podría resultar problemática para la empresa.
De ahí la utilidad de comprobar la compatibilidad
entre las políticas de la empresa
y los requisitos de los componentes de código abierto que utilizan.
La empresa puede reconocer qué requisitos debe cumplir, cómo,
y, si es necesario,
qué recursos debe descartar por ser totalmente incompatibles.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/pruebas-seguridad/"
title="Empieza ya con la solución de pruebas de seguridad de Fluid Attacks"
/>
</div>

Uno de los objetivos fundamentales en los escaneos SCA,
que, de hecho,
es al que más atención prestamos en Fluid Attacks,
es la **detección de vulnerabilidades**.
Específicamente,
lo que tenemos aquí es una correlación de componentes
con bases de datos de vulnerabilidades de seguridad conocidas,
como la National Vulnerability Database ([NVD](https://nvd.nist.gov/))
y la Common Vulnerabilities and Exposures ([CVE](../../compliance/cve/)).
La herramienta identifica las versiones específicas
de los componentes utilizados en la aplicación y,
como producto de la comparación con las bases de datos,
señala en ellas las vulnerabilidades asociadas o correspondientes.
Dependiendo de las configuraciones,
algunas herramientas pueden reportar
si dichas vulnerabilidades son explotables.
Hay herramientas que incluso ofrecen recomendaciones
para remediar las vulnerabilidades.

Algo que resulta problemático es que una herramienta no reporte
todas las vulnerabilidades conocidas.
Esto puede ocurrir cuando ella no tiene en cuenta las vulnerabilidades
en las dependencias directas y en las transitivas.
Aquí hablamos de falsos negativos.
En consecuencia, por ejemplo,
la corrección podría concentrarse en las vulnerabilidades
de las primeras capas del árbol de dependencias.
Otro inconveniente puede ser la precisión limitada de las herramientas.
Algunas de ellas pueden fallar a la hora de señalar vulnerabilidades,
a veces reportando componentes inofensivos como si estuvieran afectados.
Aquí hablamos de falsos positivos.

Dado que nadie debería confiar ciegamente
en los resultados de las herramientas,
la intervención humana aquí es siempre aconsejable,
así como en otros procesos de pruebas de seguridad.
Para los falsos positivos, hay ejercicios de verificación.
Para los falsos negativos, existe la detección manual.
Sin embargo, es siempre prudente ampliar el alcance
de la herramienta más allá de unas cuantas bases de datos.
Hay avisos de vulnerabilidades (así como pruebas de explotaciones)
que pueden encontrarse en muchos otros sitios y bases de datos.
(Ten en cuenta que esto es algo que los atacantes también saben.)
Cuantos más de estos tenga la herramienta SCA
en su repertorio como fuentes, mejor.
(Un equipo de investigación es el apoyo ideal para estas
y otras herramientas).

Cuando se reporta públicamente una vulnerabilidad
en código abierto,
como ocurrió recientemente
con "[Text4Shell](https://www.linkedin.com/feed/update/urn:li:activity:6990675889344630784)"
(similar al notorio "[Log4Shell](../../../blog/log4shell/)")
en la biblioteca Apache Commons Text,
no pasa mucho tiempo antes de que los medios de explotación
también se publiquen y se hagan efectivos.
El equipo a cargo de una herramienta SCA
debe asegurarse de que puede identificar
esta vulnerabilidad lo antes posible.
Lo ideal sería que una herramienta SCA se mantuviera actualizada
ante la aparición de nuevas vulnerabilidades.
Tras un escaneo SCA preciso con la herramienta actualizada,
una empresa podrá saber si su aplicación
utiliza un componente afectado (si aún no lo sabía).
Verá si el recurso tiene esa vulnerabilidad
y qué debe hacer para remediarla.
(En el caso de Text4Shell,
esto se resuelve con la actualización entregada
por la Apache Software Foundation).
Entre más pronto la empresa descubra todo esto,
mejor será la tarea de remediación
y la reducción de su exposición al riesgo.

## Escaneos SCA con Fluid Attacks

En Fluid Attacks,
reconocemos la importancia de la implementación temprana
y continua de los escaneos SCA.
**Nos centramos principalmente en ayudarte a identificar
vulnerabilidades de seguridad en tus componentes de código abierto**.
Disponemos de varias fuentes públicas
para la detección de vulnerabilidades.
Además,
siempre tratamos de estar actualizados
con los nuevos problemas de seguridad a medida que surgen.
Para ello,
contamos con el apoyo de nuestro equipo de investigación de seguridad.

En Fluid Attacks,
formamos parte de una cultura moderna y expansiva de DevSecOps,
y te ayudamos en su [implementación](../como-implementar-devsecops/).
Así, podemos poner en marcha nuestra herramienta SCA desde el principio
y durante las fases de tu SDLC,
siguiendo el ritmo de tus desarrolladores.
Intentamos reducir al máximo los obstáculos
que impiden un desarrollo de *software* rápido y seguro.
Cada vez que tu equipo añada nuevos componentes,
puedes solicitar una evaluación
y recibir informes de SCA con evidencias y recomendaciones.
Todos estos reportes los obtienes
en nuestra [plataforma](https://app.fluidattacks.com/),
donde puedes entender,
gestionar y priorizar las vulnerabilidades a remediar.

Para complementar el trabajo de los escaneos SCA,
contamos con [SAST](../../producto/sast/)
y [DAST](../../producto/dast/) automatizados
y [*pentesting*](../../soluciones/pruebas-penetracion-servicio/) manual.
Hace algún tiempo,
publicamos aquí un artículo en el que puedes conocer
las diferencias entre [SAST, DAST and SCA](../diferencias-entre-sast-sca-dast/).
En términos generales,
mientras que SCA se centra en las vulnerabilidades
de los componentes de *software* de código abierto que utilizas,
SAST presta atención especial a las vulnerabilidades del código fuente
que tus desarrolladores escriben para tu aplicación.
DAST, por su parte,
examina la aplicación en ejecución para detectar vulnerabilidades
en sus funcionalidades y operaciones.
Por otro lado,
el [*pentesting* manual](../../../blog/what-is-manual-penetration-testing/)
se centra en problemas más complejos
que las herramientas pasan por alto,
incluyendo, por ejemplo,
fallas en la lógica de negocio.
De hecho,
el trabajo manual de nuestro equipo de *pentesters*
también permite verificar los resultados de las herramientas
y detectar vulnerabilidades de día cero,
buscando así evitar falsos positivos
y falsos negativos en los reportes finales.

Si te interesa detectar vulnerabilidades
con nuestras herramientas
SAST, DAST y SCA,
te invitamos a suscribirte
a nuestra [prueba gratuita de 21 días](https://app.fluidattacks.com/SignUp).
Si tienes alguna pregunta,
no dudes en [contactarnos](../../contactanos/).
