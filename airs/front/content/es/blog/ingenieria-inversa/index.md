---
slug: blog/ingenieria-inversa/
title: Destapemos el programa
date: 2020-03-11
subtitle: Ideas generales sobre ingeniería inversa de *software*
category: ataques
tags: software, ciberseguridad, vulnerabilidad, hacking, pruebas de seguridad
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1620331058/blog/reverse-engineering/cover_nsktcf.webp
alt: Foto por Erda Estremera en Unsplash
description: Aquí revisamos algunos conceptos básicos de la ingeniería inversa dentro de la informática y a través de qué herramientas se puede utilizar tanto para hacking ético como para hacking malicioso.
keywords: Revertir, Software, Ingeniera Inversa, Seguridad, Vulnerabilidad, Hacking, Hacking Etico, Pentesting
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Escritor y editor
source: https://unsplash.com/photos/sxNt9g77PE0
---

Por pura curiosidad, un niño o niña de hoy en día
puede tomar cualquier aparato y desarmarlo,
posiblemente preguntándose qué elementos hay dentro
y cómo se integran para funcionar.
Algo parecido puede hacer un adulto en un taller,
pero, por ejemplo,
con la intención de reparar un motor
que por alguna razón desconocida ha dejado de funcionar.
También podemos mencionar a la mujer que,
en su lugar de trabajo,
tiene la misión de deconstruir el programa
en el que otros habían trabajado antes
solo con el fin de renovar y mejorar algunas de sus características.

<div class="imgblock">

![child1](https://res.cloudinary.com/fluid-attacks/image/upload/v1620331056/blog/reverse-engineering/child1_pwhnxl.webp)

<div class="title">

Foto por [Kelly Sikkema](https://unsplash.com/@kellysikkema)
en Unsplash.

</div>

</div>

Todos ellos han aplicado lo que se conoce como **ingeniería inversa**,
que se constituye como un proceso de deconstrucción.
Se trata de invertir los pasos del proceso de desarrollo,
con el fin de analizar y obtener conocimientos sobre cualquier
cosa [diseñada y elaborada por el ser humano](https://www.mitre.org/sites/default/files/publications/pr-15-2630-reverse-engineering-cognition.pdf).
Esta puede ser una sustancia química,
una máquina, un código de *software* u otro tipo de objeto.
[Este proceso trata de revelar y determinar en él sus detalles más intrínsecos](https://www.foo.be/cours/dess-20122013/b/Eldad_Eilam-Reversing__Secrets_of_Reverse_Engineering-Wiley\(2005\).pdf),
sus componentes y relaciones entre sí.
Se pretende descubrir cómo se diseñó
y produjo el objeto en cuestión.

A continuación exponemos algunas de las razones
por las que se emplea la ingeniería inversa:

- Información sobre un producto. La documentación puede haberse perdido,
  ser inaccesible o simplemente no haber existido nunca,
  y no hay contacto con el productor.

- Análisis de un producto.
  Saber cómo funciona, qué componentes tiene,
  definir los costos e identificar posibles violaciones de derechos de autor.

- Actualización o corrección del funcionamiento del producto.

- Auditoría o evaluación de la seguridad del producto.

- Creación de duplicados del producto sin licencia.

- Cuestión de competencia. Comprender qué hacen los competidores
  y qué distingue a sus productos.

- Simple curiosidad y aprendizaje sobre la estructura del producto.

Podemos aplicar la ingeniería inversa dentro de la tecnología
de la información (TI),
[ya sea para *software* o *hardware*](https://www.youtube.com/watch?v=7v7UaMsgg_c).
En este caso nos centramos en la **ingeniería inversa de *software***
(SRE, por su nombre en inglés),
que, tal y como se entiende, se aplica al análisis de *software*,
al [descubrimiento de sus propiedades generales](https://www.mitre.org/sites/default/files/publications/pr-15-2630-reverse-engineering-cognition.pdf),
y a la identificación de sus componentes,
funciones y relaciones.
Esto se hace a menudo en ausencia de su código fuente
o de la documentación pertinente,
[con el fin de repararlo o mejorarlo](https://link.springer.com/chapter/10.1007/978-3-642-04117-4_31).
Este proceso surgió del mantenimiento y soporte de *software*
[en gran medida derivado del análisis de *malware*](https://link.springer.com/chapter/10.1007/978-3-319-74950-1_6).

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/hacking-etico/"
title="Empieza ya con la solución Hacking ético de Fluid Attacks"
/>
</div>

Podemos dividir la SRE [en dos fases](https://www.foo.be/cours/dess-20122013/b/Eldad_Eilam-Reversing__Secrets_of_Reverse_Engineering-Wiley\(2005\).pdf):
La primera puede considerarse una observación
a gran escala para determinar la estructura general,
y a veces áreas de interés especial,
del *software* analizado.
Esta fase implica el uso de diversas herramientas
y varios servicios del sistema operativo.
Estas herramientas y servicios permiten obtener información,
rastrear *inputs* y *outputs*,
inspeccionar ejecutables, entre otras cosas.
La segunda fase es más profunda,
y está más orientada a los fragmentos de código
para comprenderlos en su estructura y funcionalidad.

## Herramientas

<div class="imgblock">

![child2](https://res.cloudinary.com/fluid-attacks/image/upload/v1620331057/blog/reverse-engineering/child2_csnxly.webp)

<div class="title">

Fotografía tomada de
[Exploratorium](https://www.exploratorium.edu/sites/default/files/tinkering/files/open_make_april_18.jpg).

</div>

</div>

De acuerdo con la información anterior,
la SRE depende considerablemente del uso de herramientas,
muchas de las cuales
[no fueron diseñadas directamente para este fin](https://www.foo.be/cours/dess-20122013/b/Eldad_Eilam-Reversing__Secrets_of_Reverse_Engineering-Wiley\(2005\).pdf).

Antes de continuar, es prudente hacer un paréntesis en este punto.
Y, para los que no estamos habituados al tema,
explicar brevemente los lenguajes de *software*
en dos niveles generales.

El nivel inferior se compone generalmente de código binario
(unos y ceros) o hexadecimal.
Esta representación ejecutable del *software*,
lo que lee la CPU, se conoce como **lenguaje máquina**
o también como código de *bytes*.

En el mismo nivel de lenguajes,
siendo una forma diferente de representación de lo mismo,
están los **lenguajes ensambladores**.
Estos son más fáciles de entender para los humanos,
porque trazan o representan patrones de bits específicos,
instrucciones de máquina, utilizando mnemónicos útiles
(secuencias de caracteres cortos pero fáciles de recordar).

El nivel superior abarca lenguajes aún más comprensibles,
que poseen palabras clave y constructores que los desarrolladores
utilizan como bloques de construcción en sus programas.
En este nivel se sitúan, por ejemplo, COBOL, Java y C.

Ahora, respecto a las herramientas,
una de las principales en la SRE es el **desensamblador**,
el cual desarrolla un proceso contrario a un ensamblador,
y que será
diferente [dependiendo de la plataforma en la que se utilice](https://link.springer.com/chapter/10.1007/978-3-319-74950-1_6).
El **desensamblador** traduce el lenguaje máquina
(*input*) a lenguaje ensamblador (*output*)
[para todo el programa o partes de él](https://www.foo.be/cours/dess-20122013/b/Eldad_Eilam-Reversing__Secrets_of_Reverse_Engineering-Wiley\(2005\).pdf).

Como expansión del trabajo del **desensamblador**, y
[en algunas tareas de reversión
como única herramienta necesaria](https://www.foo.be/cours/dess-20122013/b/Eldad_Eilam-Reversing__Secrets_of_Reverse_Engineering-Wiley\(2005\).pdf),
aparece el **depurador**.
Con él, sobre el código desensamblado,
podemos establecer puntos de interrupción
—y verificar el estado actual del programa dentro de estos—
en lugares de interés,
recorrer el código línea a línea en su análisis,
e incluso realizar ediciones en fase de ejecución.
Es decir, a diferencia del **desensamblador**,
el **depurador** no trabaja en código de programa estático,
sino que nos permite observar el comportamiento
del programa a medida que se ejecuta.
Además, a un flujo adecuado para la percepción humana, con
[pausas en la ejecución según sea necesario](https://link.springer.com/chapter/10.1007/978-3-319-74950-1_6).

Así, tras la intervención del **desensamblador**,
se espera que se traduzca el lenguaje ensamblador
a un lenguaje de alto nivel, más fácil de leer y comprender.
Esto sería más apropiado para la posterior modificación
del programa en cuestión.
Sin embargo, [esta tarea es compleja](https://link.springer.com/chapter/10.1007/978-3-642-04117-4_31).

Por último, hablemos del **decompilador**,
que es lo contrario de un compilador.
Esta herramienta intenta recrear el código fuente original,
en lenguaje de alto nivel, mediante el análisis del código binario
o, a veces, del lenguaje ensamblador.
Sin embargo,
la información obtenida es compleja de entender.
Conceptos de alto nivel como clases, matrices,
conjuntos y listas pueden no ser fácilmente recreados.
Y los comentarios y nombres de variables
pueden haberse perdido por completo
(omitidos durante la compilación),
incluso el nombre del lenguaje de alto nivel utilizado.

Aún así, el **decompilador** es valioso
y útil porque revela toda
la información básica [sobre el funcionamiento del programa](https://link.springer.com/chapter/10.1007/978-3-319-74950-1_6).
El **decompilador** recupera el lenguaje de alto nivel
del programa a partir del código máquina,
mientras que el **desensamblador** se queda en la reconstrucción
de las instrucciones en lenguaje ensamblador.

Para más información sobre las herramientas
y la muestra de las más utilizadas, visita
[Medium](https://medium.com/@vignesh4303/reverse-engineering-resources-beginners-to-intermediate-guide-links-f64c207505ed)
y
[Apriorit](https://www.apriorit.com/dev-blog/366-software-reverse-engineering-tools).

## SRE para la seguridad

La SRE puede ser útil para modificar estructuras de *software*,
alterar código, añadir o eliminar comandos y cambiar funciones,
afectando así su flujo lógico.
Desde el ámbito de la seguridad,
la SRE proporciona técnicas para el *hacking*,
ya sea malicioso o ético.
En otras palabras,
sirve para hacer daño o para generar protección y prevenirlo.

Lo positivo es que la SRE ha permitido encontrar
fallas y vulnerabilidades en, por ejemplo, algoritmos de encriptación.
También, analizar el comportamiento y las propiedades del *malware*
en sistemas de prueba o en sistemas ajenos ya infectados
(de ahí el desarrollo de *software* antivirus).
Además, ha permitido prevenir la piratería de programas
y de la información contenida en ellos,
protegiendo así los derechos digitales.

Lo negativo es que a través de la SRE los delincuentes
encuentran vulnerabilidades en los sistemas,
y bueno... se aprovechan de ellas.

El [hacking ético](../../soluciones/hacking-etico/)
es lo que hacemos en Fluid Attacks,
con nuestros expertos en seguridad,
y con el permiso de las empresas clientes.
Así, en sus aplicaciones y otros sistemas,
descubrimos debilidades de seguridad y fomentamos su remediación.
Por lo tanto,
protegemos a nuestros clientes de futuros ataques
por parte de los ciberdelincuentes.
Nuestro equipo de *pentesters*,
dentro de su postura ética,
y haciendo uso de la ingeniería inversa
y las pruebas de penetración manuales,
debe simular el comportamiento de los *hackers* maliciosos,
y así entender sus intenciones y planes para los ataques.
Todo esto con el fin de sugerir la implementación de medidas
de prevención adecuadas.

¡[Contáctanos](../../contactanos/),
y conoce más sobre nuestro servicio\!
