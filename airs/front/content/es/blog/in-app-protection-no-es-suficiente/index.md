---
slug: blog/in-app-protection-no-es-suficiente/
title: In-app protection no es suficiente
date: 2024-05-21
subtitle: Si tu capa esencial de seguridad es vulnerable, estás frito
category: filosofía
tags: ciberseguridad, tendencia
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1716330108/blog/in-app-protection-is-not-enough/cover_in_app_protection.webp
alt: Foto por Jasmin Egger en Unsplash
description: No debes dejar de remediar vulnerabilidades en apps móviles solo porque confías plenamente en tecnologías como soluciones de RASP o anti-ingeniería inversa.
keywords: In App Protection, Seguridad Esencial, Seguridad Avanzada, Seguridad De Moviles, Proteccion De Aplicaciones Android, Seguridad En Apps, Pruebas De Seguridad, Hacking Etico, Pentesting
author: Jason Chavarría
writer: jchavarria
name: Jason Chavarría
about1: Escritor y editor
source: https://unsplash.com/photos/white-egg-on-brown-wooden-tray-vWtfT-o-UOA
---

Déjanos empezar este artículo del blog con una petición.
Imagínate que has traído tus compras a tu casa de campo,
incluyendo una caja con donas.
Guardas la mayoría de víveres con prisa,
pues debes salir urgentemente a recoger a algunos de tus invitados.
Como no tienes más espacio en el refrigerador
y tampoco tienes tiempo para buscar una bolsa hermética en tu auto,
decides guardar las donas en su caja dentro de una bolsa de plástico
y las dejas en la mesa de la cocina.
"Debería ser suficiente", piensas.
Horas más tarde, vuelves con los comensales y,
mientras buscas en la cocina algo a qué convidarlos,
percibes una señal.
Una hormiga, varias, muchas.
Todas cerca a la bolsa que contiene las donas.
De haberla revisado,
habrías visto en ella un pequeño agujero.

"¡Yo habría reforzado la bolsa con otra bolsa!" algún lector podría alegar.
Posiblemente.
Pero,
como puede comprobar alguien
que haya tenido un problema de hormigas en su cocina,
ellas siempre encuentran alguna manera de entrar donde no se les quiere
si se les da tiempo.
Aclaremos algo, además,
y es que no estamos suponiendo que la bolsa hermética sea infalible.
En tu casa de campo puede haber otros antagonistas,
con más fuerza para romper las diversas capas que salvaguardan las donas.
Nuestro punto es
que combinar estas soluciones habría resultado en una prevención más exitosa
que la que vimos en esta situación hipotética.

¿Acaso lo anterior no nos recuerda a la postura
que algunos han tomado respecto a la ciberseguridad?
Añadir capas sobre capas –todas falibles– como defensa,
sin recurrir a la solución más acertada,
confiados de que esto es suficiente.
¿Este es el caso tuyo y de tu equipo?

## Seguridad por oscuridad

Las inmensas dimensiones del mercado de ciberseguridad contienen,
entre muchas otras,
ofertas de soluciones que implementan controles
para proteger las aplicaciones mientras se ejecutan.
Es el caso de una categoría de soluciones
conocida por Gartner como "*in-app protection*".

*In-app protection* es el tipo de solución
que consiste en integrar funciones en el proyecto de *software* del cliente
para hacerlo más resistente a ciberataques.
Algunas de las defensas que se ofrecen, directamente o subcontratadas,
son las siguientes:

- ***Runtime application self-protection* (RASP):**
  Una función que monitorea la app (p. ej., sus *inputs* y *outputs*)
  y notifica al usuario o cierra la app si identifica un patrón inseguro.

- **Prevención de ataques [*man-in-the-middle*](../../learn/que-es-ataque-man-in-the-middle/)
  (MITM):** Un conjunto de funciones como detección de *proxies* maliciosos,
  fijación de certificados (ver abajo),
  *allowlisting*/*whitelisting* de URLs, etc.,
  que prevendrían que *hackers* intercepten comunicaciones entre dos partes.

- **Detección de dispositivos *jailbroken* o *rooted*:**
  Una función que notifica o previene la ejecución de la app
  en dispositivos en que se permite el acceso al usuario *root*.

- **Fijación segura de certificados:**
  Una función que obliga el uso de certificados SSL idénticos
  para la comunicación entre la aplicación y un servidor,
  con el motivo de prevenir que los atacantes desvíen esa comunicación
  hacia un servidor malicioso con certificados fraudulentos.

- **Ofuscamiento de código:**
  Una función que hace que el código sea difícil de interpretar por un *hacker*
  o de escanear con una herramienta de [análisis de código estático](../seguridad-app-sastisfactoria/)
  (SAST)
  para encontrar vulnerabilidades.

- **Técnicas anti-[ingeniería inversa](../ingenieria-inversa/):**
  Una función que bloquea herramientas que usan los *hackers*
  para la identificación de los componentes,
  funciones
  y relaciones del producto de *software*,
  y más profundamente, su lógica de negocio,
  sin acceso inicial al código fuente.

Las anteriores son medidas de seguridad **adicionales** para las aplicaciones,
recomendadas por algunos,
por ejemplo, por el proyecto [OWASP Mobile App Security](https://mas.owasp.org/)
(MAS),
para apps de organizaciones con una necesidad imperante
de salvaguardar sus activos y su lógica de negocio.
Pero algunas soluciones de este mercado prometen aliviar la carga
del equipo de desarrolladores,
permitir la velocidad de las salidas a producción
y, algo muy sugestivo, proteger las apps sin necesidad de escribir código.
Estas propuestas pueden interpretarse por los clientes
como medidas suficientes para mantener seguras sus aplicaciones,
como si así se remediaran sus vulnerabilidades.

Necesitamos que tú y tu equipo aprendan y entiendan este mantra:
"Usar controles de seguridad no es remediar".
Efectivamente,
las características de seguridad que mencionamos líneas atrás
se tratan de defensas
que ayudan a aumentar la complejidad de explotar una vulnerabilidad
o entender el código.
La [remediación](../../../blog/vulnerability-remediation-process/), en cambio,
es la eliminación de la característica en la postura de seguridad,
configuración
o diseño del producto de *software* que lo hace abierto a la explotación.

Quizá algunos equipos que delegan la seguridad a *in-app protection*
no necesitan la aclaración de arriba.
Están satisfechos con el blindaje que han adquirido para sus apps
y ven como innecesaria la remediación,
un procedimiento que perciben demasiado costoso.
Pero esconder las vulnerabilidades de *software* tras capas de seguridad
sin intención de remediarlas es una decisión desafortunada.

## Solo estás comprando tiempo

El equipo de *hacking* de Fluid Attacks tiene experiencia evadiendo controles
como los mencionados en este *post*.
No necesariamente se debe
a que el RASP con que se encuentran está mal configurado,
o que el ofuscamiento es insuficiente,
sino a que los *hackers* **con el tiempo suficiente**
siempre logran evadir estos controles.
Y detrás de estos pueden encontrar *back-ends*
con vulnerabilidades esperando ser explotadas.
O bien los *hackers* pueden procurar versiones anteriores de las apps
sin *in-app protection*
y lanzar sus ataques contra estas.

Un exceso de confianza en los controles de seguridad
puede eventualmente significar una brecha de seguridad
que puede causar pérdidas de información, dinero y reputación.

<div>
<cta-banner
buttontxt="Leer más"
link="/es/soluciones/pruebas-seguridad/"
title="Inicia ahora las pruebas de seguridad de Fluid Attacks"
/>
</div>

## No te confíes solo de la seguridad resiliente

Repetimos que las defensas en la modalidad de *in-app protection*
son medidas adicionales.
Estas no reemplazan a los requisitos de seguridad
con los que deben cumplir el código fuente y las configuraciones de tu app.
Esto mismo es lo que propone OWASP MAS.
Este proyecto reconoce la importancia de los controles de seguridad
como [**seguridad resiliente**](https://mas.owasp.org/MASVS/Intro/03-Using_the_MASVS/#mas-testing-profiles)
y los considera como una capa extra que refuerza otras dos:
una de **seguridad esencial** y otra de **seguridad avanzada**.

La seguridad esencial tiene que ver
con que tu app móvil use mecanismos de criptografía preexistentes,
probados
y seguros,
proteja el tráfico de la red,
use mecanismos seguros de comunicación entre procesos,
no use dependencias de terceros vulnerables,
entre otros requisitos.
Todo esto debe probarse continuamente
y corregirse cuando no se cumple.
No importa qué tan buena crees que sea tu seguridad resiliente.
En un *post* anterior enumeramos los [riesgos de las aplicaciones móviles](../que-es-mast/)
y el rol que tienen las pruebas de seguridad para identificarlos.

Por otra parte,
es altamente recomendado que implementes seguridad avanzada
si tu app maneja datos sensibles de alto riesgo.
Algunos requisitos de esta capa son
la seguridad de sus mecanismos de autenticación,
el uso de fijación de identidad,
el uso de mecanismos de actualización seguros, etc.

## Remediar es más seguro y cada vez más rápido

Ya hemos definido lo que deberías entender como adicional y lo que es esencial.
Sabemos que en última instancia es la seguridad del código
y configuraciones de tu app
lo que está entre un actor malicioso
y la integridad, confidencialidad y disponibilidad de la información
de tu negocio y de los usuarios finales.
De modo que indiscutiblemente parte importante del esfuerzo de tu equipo
debe estar en identificar y corregir problemas de seguridad
mientras las medidas de seguridad resiliente te compran un poco de tiempo.

No solo es más seguro remediar,
sino que es una tarea que tiende a ganar velocidad.
Cuando tu equipo desarrolladores se responsabiliza de la seguridad de la app,
puede adquirir conocimiento de las vulnerabilidades
y, con un *feedback* oportuno, puede [aprender a escribir código seguro](../codificacion-segura-cinco-pasos/).
Esto último se traduce
en menos instancias de al menos un tipo de vulnerabilidad
y menos reproceso y tiempo de remediación.
De hecho,
en nuestro reporte [State of Attacks 2023](https://fluidattacks.docsend.com/view/h5vcqn8eh8pdrguj),
encontramos que,
si se rompe el *build*, evitando salir a producción con código vulnerable,
los equipos de desarrollo toman menos tiempo
para remediar las vulnerabilidades detectadas que si no se rompe el *build*.

## Fluid Attacks evalúa tu seguridad exhaustivamente y ayuda en la remediación

Como mencionamos arriba,
Fluid Attacks cuenta con un equipo de *hacking*.
Se trata de un equipo [altamente calificado](../../../certifications/)
que evalúa continuamente la seguridad de productos de *software*
al tiempo que también lo hacen las [herramientas AppSec](../../soluciones/pruebas-seguridad/)
desarrolladas por Fluid Attacks.
Tal combinación resulta en rapidez y precisión.
Con Fluid Attacks,
puedes reconocer las vulnerabilidades de tu seguridad esencial,
avanzada
y resiliente,
y obtener recomendaciones de remediación por un equipo de expertos.

Encuentra vulnerabilidades en tu app
antes de que un cibercriminal lo haga.
[Contáctanos](../../contactanos/).
