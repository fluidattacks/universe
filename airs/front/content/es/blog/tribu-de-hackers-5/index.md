---
slug: blog/tribu-de-hackers-5/
title: Tribu de hackers red team 5.0
date: 2021-03-31
subtitle: Aprendiendo del experto en red teams Carlos Pérez
category: opiniones
tags: ciberseguridad, red-team, hacking, pentesting, blue-team
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1620331133/blog/tribe-of-hackers-5/cover_lgtcjo.webp
alt: Foto por Jonathan Petersson en Unsplash
description: Este artículo se basa en el libro "Tribe of Hackers Red Team" de Carey y Jin. Aquí compartimos el contenido de la entrevista con Carlos Pérez.
keywords: Red Team, Ciberseguridad Red Team, Hacking Red Team, Pentesting, Hacking Etico, Blue Team, Conocimiento, Tribu De Hackers
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Escritor y editor
source: https://unsplash.com/photos/gQhWMkYh3Yc
---

Este artículo es el quinto de una serie basada en el libro
[*Tribe of Hackers Red Team*](https://www.amazon.com/Tribe-Hackers-Red-Team-Cybersecurity/dp/1119643325)
de Carey y Jin (2019).
Como ya comenté [en el primer artículo](../tribu-de-hackers-1/),
en este libro encontramos las respuestas de 47 expertos
en [*red teaming*](../ejercicio-red-teaming/) a las mismas 21 preguntas.
En las entradas anteriores,
hice referencia a las opiniones de [(1.0) Carey](../tribu-de-hackers-1/),
[(2.0) Donnelly](../tribu-de-hackers-2/),
[(3.0) Weidman](../tribu-de-hackers-3/),
y [(4.0) Secor](../tribu-de-hackers-4/).
Para esta ocasión,
decidí centrarme en las respuestas de Carlos Pérez
([Darkoperator](https://twitter.com/carlos_perez?lang=en)),
el primer latinoamericano incluido en la serie,
quien lleva más de veinte años en el mundo de la ciberseguridad.

Carlos trabajó para el gobierno de Puerto Rico,
realizando *pentesting* y ayudando a proteger sus redes.
Más tarde,
se unió a Compaq/HP como arquitecto sénior de soluciones
para las prácticas de consultoría de seguridad
y redes para clientes en Sudamérica, Centroamérica y el Caribe.
También trabajó en Tenable como director de ingeniería inversa y,
en el momento de la entrevista del libro,
era el líder de práctica para la investigación en TrustedSec.
Actualmente,
Carlos es conocido por sus contribuciones a herramientas
de seguridad de código abierto como
[DNSRecon](https://github.com/darkoperator/dnsrecon) y
[Metasploit](https://github.com/darkoperator/Metasploit-Plugins).

## Para los que buscan ser diligentes en los *red teams*

Carlos comienza recomendando conocimientos específicos,
divididos en técnicos y no técnicos,
que él considera necesarios para quienes quieran formar parte de un *red team*.
**En el aspecto técnico**,
inicia refiriéndose a una base sólida en lógica de programación,
un conocimiento esencial para la correcta adaptación
a diversos lenguajes de programación,
así como para la producción y alteración de herramientas.
A continuación,
Carlos sugiere una buena comprensión de las redes porque,
según dice, la mayoría de las acciones atravesarán este tipo de ambiente.
Además, según Carlos,
tendrás que entender cómo se configuran,
se mantienen y se aseguran los sistemas.
Y deberías mantener un método de práctica y aprendizaje constantes,
siempre con el objetivo de evitar cualquier sesgo técnico.

**En el aspecto no técnico**,
Carlos comienza destacando la importancia de conocer acerca de las estructuras,
la comunicación y el trabajo en equipo de una organización.
Precisamente, en cuanto al acto de expresar ideas,
reconoce que muchos en este campo son introvertidos.
Sin embargo, sin pelos en la lengua,
Carlos advierte que si no eres capaz de transmitir información sobre riesgos,
mitigación y apoyo de una manera que los responsables
de la toma de decisiones puedan utilizar y comprender,
entonces habrás fracasado.
Por último,
él añade la importancia de aprender sobre las nuevas tendencias
y buenas prácticas en la industria de TI
(que a veces son ignoradas por los profesionales),
por ejemplo, Cloud y [DevOps](../../soluciones/devsecops/).

Al igual que otros expertos cuyas opiniones se han presentado en esta serie,
Carlos nos recuerda que no es necesario participar
en actividades ilegales para adquirir habilidades de *red team*.
La información,
el entrenamiento y el material de referencia para aprender
todos los aspectos están disponibles públicamente,
y todo puede simularse en un ámbito de prueba para ensayar y validar conceptos.
No cometas el estúpido error de jugar
al chico/chica malo/a cuando probablemente puedas aprender
las mismas habilidades en el proceso para convertirte en
un [*hacker* ético](../../soluciones/hacking-etico/),
siendo un *hacker* ético.

## Para los que ya sudan sangre en los *red teams*

Empecemos por el trabajo en equipo.
De acuerdo con Carlos, cada miembro del *red team*
debería tener un conocimiento claro del cliente
y de los sistemas que se van a evaluar.
La planificación debe realizarse precisamente en grupo.
Todos los miembros pueden compartir sus opiniones desde el principio,
y el equipo puede discutirlas con la intención de llegar a acuerdos.
A medida que avance el proyecto,
deben realizarse reuniones periódicas para revisar las acciones.
Al final de un contrato,
debe hacerse un intercambio de opiniones
en el que los egos queden a un lado
y se diga sinceramente lo que hay que mejorar.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/red-team/"
title="Inicia ahora con la solución de red teaming de Fluid Attacks"
/>
</div>

Para Carlos, es falso decir que las nuevas técnicas
y *exploits* deben mantenerse en secreto, incluso de los clientes,
para evitar perder ventajas en otros contratos.
*Red teaming* no consiste simplemente en emular,
sino que también implica cultivar una relación con el cliente,
en la que el pensamiento crítico puede ayudar
a gestionar los riesgos potenciales y mejorar la ciberseguridad.

Cuando, por ejemplo,
en un ejercicio de [*pentesting*](../../soluciones/pruebas-penetracion-servicio/)
o de [simulación de ataque](../../../blog/what-is-breach-attack-simulation/)
los equipos de seguridad del cliente consiguen descubrirte,
ten en cuenta algo que Carlos comparte desde su experiencia:
No tiene por qué ser algo negativo con tu trabajo y tus capacidades;
puede ser que en el lado del cliente
ya hayan aprendido de proyectos anteriores
y hayan aplicado las medidas necesarias.
De acuerdo con sus palabras,
puedes recordarte que tu tarea es ayudarles a poner a prueba su seguridad
y hacer que sus sistemas sean más seguros.

<div class="imgblock">

![cpq](https://res.cloudinary.com/fluid-attacks/image/upload/v1620331133/blog/tribe-of-hackers-5/cpq_nti68o.webp)

<div class="title">

La imagen original de Carlos fue tomada del libro de referencia.

</div>

</div>

## Para empresas que aspiran estar a la vanguardia en seguridad

A la pregunta de cuándo introducir un *red team*
en el programa de seguridad de una organización,
Carlos responde (en términos de condiciones):
En esa organización tiene que haber una cultura de involucrar
a la seguridad desde el principio del proceso,
cuando tenga sentido hacerlo,
y una disposición a escuchar ideas críticas alternadas
de los planes cuando estos se expongan.
Debe ser una empresa que reconozca la necesidad
y esté dispuesta a someter a evaluación sus proyectos
y sistemas para identificar debilidades y vulnerabilidades en ellos.
Pero no solo eso, según Carlos,
la organización debe estar dispuesta a asumir esfuerzos para eliminar
y mitigar los riesgos señalados por el *red team*.

El criterio de Carlos es bastante valioso
cuando sugiere que es mejor no implementar los servicios
de un *red team* dentro de una empresa, al menos no en ese momento,
en el que su equipo de seguridad está algo aislado
de los procesos generales de la toma de decisiones.
Además, para él, no es buena idea convocar a los *red teams* cuando,
más que una colaboración,
lo que hay entre los grupos de esa empresa es solo competencia y conflicto.

Por otro lado,
Carlos advierte a las compañías interesadas
en su seguridad que tengan cuidado con, desde su punto de vista,
el "control de seguridad de menor costo" que en muchos lugares
se puede ver implementado.
Se refiere a herramientas sin métricas,
objetivos y entrenamiento ajustados
a las particularidades de la empresa cliente,
las cuales acaban solo proporcionando un efecto placebo
a los que firmaron el cheque.

Adicionalmente,
Carlos menciona un control de seguridad fácil
y sencillo que una empresa puede implementar ahora que
el [phishing](../../../blog/phishing/) y el *malware*
son tan empleados para comprometer redes o sistemas.
Se trata de abordar primero las vías de entrada más comunes.
Según Carlos,
la mayoría de las compañías no bloquean ni controlan la ejecución de HTA,
Windows Scripting Host o macros de Office.
Después de bloquear las rutas de entrada,
el equipo de seguridad puede empezar a perfilar
el comportamiento típico dentro del entorno
para construir un sistema de detección automática
de comportamientos anormales.

## ¡Eso es todo, amigos!

No olvides que puedes acceder a la entrevista completa con Carlos Pérez
en el
[libro de Carey y Jin](https://www.amazon.com/Tribe-Hackers-Red-Team-Cybersecurity/dp/1119643325).
Por cierto,
recuerda que si quieres formar parte del *red team* de Fluid Attacks,
puedes consultar nuestra página de [Trabaja con nosotros](../../../careers/).
Y si necesitas información sobre nuestros
[servicios](../../servicios/hacking-continuo/) y
[soluciones](../../soluciones/) para tu organización,
puedes hacer [click aquí para contactarnos](../../contactanos/).
