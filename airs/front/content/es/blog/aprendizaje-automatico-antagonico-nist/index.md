---
slug: blog/aprendizaje-automatico-antagonico-nist/
title: Aprendizaje automático antagónico
date: 2024-01-25
subtitle: NIST aclara la clasificación de ataques contra la IA
category: filosofía
tags: ciberseguridad, aprendizaje automático, software, riesgo, tendencia
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1706217218/blog/adversarial-machine-learning-nist/cover_adversarial_machine_learning_nist.webp
alt: Foto por Fervent Jant en Unsplash
description: Presentamos un resumen de un informe reciente de NIST sobre el aprendizaje automático antagónico que podría ayudarnos a entender mejor los ataques contra y desde los sistemas de IA.
keywords: Aprendizaje Automatico Adversarial, Aml, Ml, Inteligencia Artificial Predictiva, Inteligencia Artificial Generativa, Predai, Genai, Nist, Pentesting, Hacking Etico
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Escritor y editor
source: https://unsplash.com/photos/bruce-lee-action-figure-with-black-background-kKLEgX-Z_O4
---

Los recientes avances en inteligencia artificial (IA)
con modelos mejorados de aprendizaje automático
(*machine learning*, ML)
han aportado enormes beneficios a una gran variedad de sectores.
Eso ya lo sabemos.
(Véase, por ejemplo, [nuestro uso práctico de la IA generativa](https://help.fluidattacks.com/portal/en/kb/articles/integrations-faq)
para ayudar a remediar vulnerabilidades en nuestro Hacking Continuo).
Sin embargo, como cualquier otra tecnología de la información,
esos avances plantean riesgos. Los sistemas de IA son *software*,
por lo que pueden ser vulnerables y, en consecuencia,
recibir ciberataques de, y actuar como vectores de ataque para,
personas con malas intenciones.

Así lo reconoce el Instituto Nacional de Estándares
y Tecnología de Estados Unidos
(NIST, por su nombre en inglés) en su informe
*[Adversarial Machine Learning](https://nvlpubs.nist.gov/nistpubs/ai/NIST.AI.100-2e2023.pdf):
A Taxonomy and Terminology of Attacks and Mitigations*,
publicado este año y descrito en este artículo del blog.
Aquí **nos centramos** más en la información compartida
sobre los **ataques** que en las mitigaciones.
Para conocer a fondo esta iniciativa,
te invitamos a leer el reporte completo.

Los seres humanos estamos acostumbrados a dar nombre a las cosas,
por más abstractas que sean.
Los términos y conceptos a menudo nos permiten abordar
de forma sistemática los problemas a los que nos enfrentamos
en distintos periodos de nuestra existencia como humanidad.
De ahí viene —podemos suponer— el propósito del NIST
de empezar a elaborar una taxonomía
y una terminología (como conocimiento compartido)
en el **aprendizaje automático antagónico** (AML, por su nombre en inglés).

El AML puede verse concretamente como ataques orientados
a aplicaciones de IA y sus modelos de ML.
Pero también puede entenderse de forma más amplia
como un enfoque de investigación destinado a aclarar los objetivos,
capacidades y métodos de los adversarios
o atacantes de la IA e intentar contrarrestarlos
con medidas preventivas y de respuesta.

Los adversarios de la IA buscan explotar
las vulnerabilidades de estos sistemas,
especialmente en las distintas fases de los ciclos de vida del ML
(es decir, desarrollo, entrenamiento, pruebas y despliegue).
Sin embargo,
la literatura actual en AML se centra principalmente
en los ataques dentro de las fases de entrenamiento y despliegue.
Es importante señalar que los atacantes
pueden explotar vulnerabilidades no solo en los modelos de ML,
sino también en la infraestructura en la que se despliega la IA.
Específicamente,
los modelos de aprendizaje automático utilizados
en la IA moderna son susceptibles a ataques a través
de las API públicas que los presentan
y contra las plataformas en las que se despliegan.
El reporte de NIST se enfoca en el primer caso,
considerando el segundo como parte
de las taxonomías tradicionales de ciberseguridad.

NIST define la **taxonomía** para
AML basándose en **cinco dimensiones**:
(a) tipo de IA,
(b) método de aprendizaje y fase del ciclo de vida ML
en la que comienza el ataque,
(c) objetivos del atacante,
(d) capacidades del atacante, y
(e) conocimiento del atacante del modelo ML específico y más allá.
Los autores del informe admiten
que este será un proyecto en constante evolución y, por el momento,
ofrecen una taxonomía de los ataques más estudiados.
Para la breve segmentación de los tipos de ataques
que ofrecemos en nuestro resumen,
tomamos el tipo de IA (IA predictiva e IA generativa)
y la fase del ciclo de vida del ML
(fase de entrenamiento y fase de despliegue).

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/hacking-etico/"
title="Inicia ahora con la solución de *hacking* ético de Fluid Attacks"
/>
</div>

## Ataques contra IA predictiva

### En la fase de entrenamiento

En esta etapa del ciclo de vida,
encontramos ataques de envenenamiento (*poisoning attacks*)
contra los datos del modelo de ML
y contra el propio modelo.
Estos ataques, los cuales pueden tener muchas variantes,
suelen acabar vulnerando la integridad
o la disponibilidad del sistema de IA.

En los **ataques de envenenamiento de datos**,
el adversario controla los conjuntos de datos de entrenamiento
del modelo y puede insertar o modificar muestras a su voluntad.
Entonces,
cuando el atacante inserta muestras envenenadas
con una etiqueta específica,
el modelo aprende esa etiqueta errónea.
Así, por ejemplo, si el modelo es un clasificador de imágenes,
después de pasar por la fase de entrenamiento,
clasifica erróneamente algunas imágenes en su periodo de prueba.
Si su tarea es, digamos, identificar señales de tráfico,
es posible que pase por alto algunas de ellas
o que tome otras imágenes como si fueran tales señales.
Algo parecido se ha demostrado en la alteración
de detectores de *spam*.

Por otro lado, en los **ataques de envenenamiento de modelos**,
el adversario controla el modelo y sus parámetros.
En este caso, el envenenamiento del modelo se produce
con la inserción de funcionalidades maliciosas.
Por ejemplo, el atacante puede inyectar
un “accionador troyano” (*Trojan trigger*)
en el modelo después de haber infectado solamente algunos
de sus componentes que fueron traídos como actualizaciones
para ser añadidos al servidor.
Tal infección, como en el caso de los datos envenenados,
puede generar problemas en la precisión del modelo.

En algunos ataques de envenenamiento,
el atacante puede centrarse en objetivos específicos,
ya sean muestras más pequeñas o componentes concretos del modelo,
e insertar patrones de *backdoor* generados previamente
para, de nuevo, inducir clasificaciones erróneas.
Por último, en cuanto a la disponibilidad,
los ataques de envenenamiento provocan
una degradación indiscriminada del modelo en todas las muestras
y acaban consiguiendo algo así como una denegación
de servicio para los usuarios del sistema de IA.

### En la fase de despliegue

En los **ataques de evasión**,
el adversario modifica las muestras de prueba
para crear los denominados ejemplos adversarios.
Se trata de muestras que,
habiendo recibido una perturbación mínima,
la cual puede ser incluso imperceptible para el ojo humano,
reciben del sistema una clasificación alterada
que apunta a una clase arbitraria elegida por el atacante.
Al tratarse de muestras muy similares a las originales
e incorrectamente clasificadas por el modelo,
afectan a las predicciones del sistema.
Entonces, como en el caso de los ataques de envenenamiento,
un clasificador de imágenes, por ejemplo,
es engañado con tales ejemplos adversarios,
y el atacante, para sus fines maliciosos,
impide que el sistema reconozca imágenes específicas
como se supone que debe hacerlo.

En los **ataques a la privacidad**,
el atacante tiene acceso de consulta al modelo de ML.
Por lo tanto, puede enviar un gran número de preguntas (*queries*)
al modelo para recibir información que le permita lograr
la inferencia de pertenencia o la reconstrucción de los datos.
En el primer caso,
el adversario puede inferir la presencia o el uso de registros
o muestras específicas en el conjunto de datos
de entrenamiento empleado por el modelo.
En el segundo caso, puede reconstruir contenido particular
o características de los datos de entrenamiento del modelo.
Se trata entonces de ingeniería inversa
para obtener información privada sobre usuarios individuales,
infraestructuras o parámetros del sistema de IA.

La reconstrucción de muestras en,
por ejemplo, modelos de redes neuronales puede ocurrir
porque estas tienden a memorizar los datos de entrenamiento.
Sin embargo, aparentemente,
los atacantes no suelen lograr extracciones de datos precisas,
sino reconstrucciones de modelos equivalentes
que alcanzan rendimientos de predicción similares
a los obtenidos por los modelos originales.
Esto es algo que incluso permite a los adversarios realizar ataques
más potentes contra los sistemas que son su objetivo.

## Ataques contra IA generativa

La mayoría de los tipos de ataque presentados
por NIST para la IA predictiva también aplican a la IA generativa.
Sin embargo,
se han documentado otras formas de violación
de la seguridad para esta segunda rama de la inteligencia artificial.
La más destacada es la de las **violaciones por abuso**.
En este caso, el adversario consigue redirigir
o modificar el propósito del modelo
(p. ej., un *large language model*, LLM)
para lograr sus propios objetivos mediante
la inyección de instrucciones ([*prompt injection*](../../../blog/indirect-prompt-injection-llms/)).
Es decir, el atacante, directa o indirectamente,
inyecta texto en el modelo con la intención
de alterar su comportamiento.

Así, por ejemplo,
recurriendo a las capacidades de un LLM,
el adversario puede ordenarle que genere textos
o imágenes ofensivas o desinformativas,
así como contenidos de *malware* o *phishing*,
para su posterior propagación en la Internet.
El hecho de que los LLM puedan integrarse fácilmente
con distintos tipos de aplicaciones facilita
la difusión de información maliciosa
y la generación de amplios ciberataques, algo que,
entre sus usuarios, puede afectar a su fiabilidad.

En cuanto a los problemas de privacidad,
también existe la posibilidad de que el atacante simplemente
pida a un modelo de IA generativa inseguro
que reproduzca información privada o confidencial
con la que haya trabajado previamente.
También se han demostrado casos de inyección en los que,
de algún modo, el adversario ordena al LLM
que persuada a los usuarios finales para
que revelen alguna información sensible.
(Véase nuestro artículo [“Indirect Prompt Injection to LLMs."](../../../blog/indirect-prompt-injection-llms/))

Por último,
por el lado de la violación de disponibilidad,
en este tipo de inteligencia artificial se han documentado
casos en los que el atacante entrega *inputs* maliciosos
o un gran número de *inputs* al modelo ML
para provocar un ejercicio computacional abrumador
sobre el mismo y llevar a la denegación de servicio a otros usuarios.

## ¿Qué pasa con la mitigación de riesgos?

Como decíamos al principio,
no pretendemos hacer énfasis en los métodos de mitigación
de riesgos y gestión de consecuencias
para los ataques descritos anteriormente.
Esto se debe en parte a que, si bien estrategias
como la elaboración de perfiles de datos,
la formación multimodal, la monitorización del comportamiento
y la autenticación de la cadena de suministro han demostrado su utilidad,
muchas técnicas también han resultado ineficaces.
Han surgido desequilibrios entre determinados tipos de ataques
y las técnicas de mitigación fiables existentes.

Para enfrentarse con éxito a muchos
de los tipos de ataques mencionados,
esperamos que se siga avanzando
en nuevas estrategias preventivas y defensivas.
Para ello,
la contribución de los adversarios es enormemente indispensable.
Sí, tal y como lo lees.
En Fluid Attacks sabemos por experiencia propia que los atacantes,
cuando no tienen malas intenciones,
como es el caso de los *hackers* éticos,
son un apoyo muy valioso a la hora de identificar vulnerabilidades
y proponer soluciones a las mismas.
Ellos, junto con otros especialistas en ciberseguridad,
teniendo a su disposición reportes como el proporcionado por NIST,
podrán advertirnos sobre cómo hacer frente a estas amenazas.

De momento,
te recomendamos hacer un uso cuidadoso de esta tecnología y,
como hacemos en Fluid Attacks,
confiar únicamente en aquellos proveedores
de IA cuyos servicios muestren **políticas de seguridad y privacidad estrictas**
para el almacenamiento y tratamiento de datos sensibles.
