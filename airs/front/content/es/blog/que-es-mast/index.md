---
slug: blog/que-es-mast/
title: ¿Qué es MAST?
date: 2022-08-18
subtitle: Protege tus apps de los cibercriminales al acecho
category: filosofía
tags: ciberseguridad, pruebas de seguridad, hacking, empresa, riesgo
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1660857426/blog/what-is-mast/cover_mast.webp
alt: Foto por Agê Barros en Unsplash
description: Aquí hablamos de las aplicaciones móviles y algunos de sus riesgos de seguridad, también sobre qué es MAST y cómo puede contribuir a la seguridad de las aplicaciones.
keywords: Apps Moviles, Mast, Aplicaciones, Pruebas De Seguridad, Seguridad De Aplicaciones, Sast, Dast, Hacking Etico, Pentesting
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Escritor y editor
source: https://unsplash.com/photos/fKAjOxgZNPg
---

Cada mañana, al levantarme,
lo primero que hago es mirar en mi teléfono
qué mensajes nuevos he recibido.
Es un hábito muy arraigado.
Para muchos de nosotros,
el uso de las capacidades de estos dispositivos
forma parte de nuestra rutina diaria. Como seguramente has observado,
estamos saturados de [aplicaciones móviles](../../../systems/mobile-apps/).
Las utilizamos para administrar nuestro dinero,
pedir servicios de transporte y comida, jugar,
fijar objetivos y hacerle seguimiento a nuestra actividad física,
y para montones y montones de cosas más.
Incluso hay aplicaciones tan estrambóticas
y divertidas [como la que te dice](https://runpee.com/)
cuáles son los momentos adecuados en una película
que estás viendo en el cine para salir al baño
y no perderte las escenas más cruciales.
Al parecer, a principios de esta década,
el número de aplicaciones móviles rondaba
ya los [8.9 millones](http://www.forbes.com/sites/johnkoetsier/2020/02/28/there-are-now-89-million-mobile-apps-and-china-is-40-of-mobile-app-spending/).

Más allá del uso personal que hacemos de los teléfonos móviles,
está el uso que ahora hacemos de ellos en nuestros lugares de trabajo
para cumplir con las operaciones empresariales.
Cada vez más empleados trabajan desde estos dispositivos.
En ello puede haber influido el crecimiento
del trabajo a distancia debido a la pandemia.
Cada vez más,
los datos de las organizaciones se gestionan en los teléfonos móviles,
que en muchos sentidos han sustituido a los computadores.
Del mismo modo,
las aplicaciones móviles son ahora el pilar de múltiples
empresas que dependen de ellas para llevar a cabo su actividad diaria.
Gracias a ellas, las compañías pueden hacerse un lugar
y participar en el mercado en línea y conectar con sus clientes
o usuarios en distintos lugares del mundo.

La competencia en el ámbito
de las aplicaciones móviles es increíblemente intensa.
Los equipos de DevOps tienen la tarea de crear
y lanzar aplicaciones a un ritmo acelerado
para ganar terreno entre los grupos de consumidores
y actualizarlas con frecuencia para estar a la altura.
Sin embargo,
esa prisa no debería traducirse en lanzar aplicaciones inseguras.
Muchas aplicaciones tienen acceso
a grandes cantidades de información de los usuarios,
parte de la cual son datos sensibles que deben protegerse.
Los teléfonos móviles
y sus aplicaciones han despertado un hambre voraz en los ciberdelincuentes,
quienes saben que ahí,
como en cualquier otro sistema informático,
pueden encontrar vulnerabilidades para explotar.

## Riesgos de las aplicaciones móviles

Las aplicaciones y dispositivos móviles
no suelen alcanzar el mismo nivel de seguridad
que las aplicaciones de escritorio y los PC.
Hoy en día,
los sistemas operativos más comunes
para aplicaciones móviles son Android e iOS.
Estos sistemas disponen de controles de seguridad
que ayudan de alguna manera a los desarrolladores
de _software_ a crear aplicaciones seguras.
No obstante,
a menudo corresponde a los desarrolladores elegir
entre múltiples opciones de seguridad,
y muchas veces sus decisiones no son las mejores.

Las aplicaciones móviles que carecen
de controles adecuados pueden acabar
revelando datos sensibles a otras aplicaciones
presentes en los dispositivos.
Los usuarios maliciosos y el _malware_
pueden saltarse fácilmente los controles de autenticación,
autorización e introducción de datos.
Por otra parte,
la falta de un cifrado potente para el almacenamiento
y la transmisión de datos es bastante peligrosa.
Todas estas debilidades se deben a ignorancia
y errores en el proceso de desarrollo.
En este sentido,
los desarrolladores novatos
(aunque los expertos también pueden cometer estos errores)
también pueden insertar inocentemente nombres de usuario
y contraseñas directamente en sus códigos fuente.
Además,
pueden copiar y pegar fragmentos de código o descargar librerías
y marcos de trabajo sin pensarlo dos veces y,
de este modo,
acabar creando y distribuyendo aplicaciones vulnerables.

Riesgos como los anteriores se exponen en
el [OWASP Mobile Top 10](https://help.fluidattacks.com/portal/en/kb/articles/criteria-compliance-owaspm10/),
que forma parte del [OWASP Mobile Security Project](https://owasp.org/www-project-mobile-security/).
Este proyecto pretende proporcionar
a los desarrolladores de _software_ y a los equipos de seguridad
recursos de orientación para crear
y mantener aplicaciones móviles seguras.
El estándar ofrecido en este proyecto es
el [OWASP MASVS](https://help.fluidattacks.com/portal/en/kb/articles/criteria-compliance-owaspmasvs)
(Mobile Application Security Verification Standard).
Vinculada a este estándar hay una guía,
la [OWASP MSTG](https://owasp.org/www-project-mobile-security-testing-guide/)
(Mobile Security Testing Guide),
la cual describe los procesos para probar los controles incluidos en MASVS.

El último OWASP Mobile Top 10,
[la lista de 2016](https://owasp.org/www-project-mobile-top-10/2016-risks/),
presenta los siguientes riesgos:

1. **Uso inadecuado de la plataforma:**
   Las características de la plataforma no se utilizan correctamente
   o no se implementan sus respectivos controles de seguridad.

2. **Almacenamiento de datos inseguro:**
   Los datos no están adecuadamente protegidos
   y pueden ser de fácil acceso para usuarios maliciosos o _malware_.

3. **Comunicación insegura:**
   No hay una protección adecuada para el tráfico de red
   y los datos están expuestos a ser interceptados en las transmisiones.

4. **Autenticación insegura:**
   No se utilizan esquemas de autenticación o estos son débiles,
   lo que permite a los atacantes acceder a funciones y datos privados.

5. **Criptografía insuficiente:**
   Se utilizan algoritmos de cifrado débil o procesos de cifrado defectuosos,
   lo que permite a los atacantes revertir con éxito datos
   o códigos sensibles a su forma original.

6. **Autorización insegura:**
   Se utilizan esquemas de autenticación deficientes,
   lo que permite a los atacantes ejecutar funcionalidades
   que deben estar destinadas a usuarios con altos privilegios.

7. **Calidad (deficiente) del código del cliente:**
   Se llevaron a cabo prácticas de codificación deficientes
   por lo que usuarios externos podrían introducir código
   en la app para su ejecución.

8. **Alteración del código:**
   No se detectan alteraciones en el código,
   en las APIs del sistema, o en los datos y recursos de la aplicación,
   cuyo comportamiento puede ser alterado por los atacantes.

9. **Ingeniería inversa:**
   No existe una ofuscación de código efectiva que ayude a prevenir
   la conversión de regreso a código fuente legible
   y la revelación de información interna de la aplicación.

10. **Funcionalidad extraña:**
    El desarrollo deja funcionalidades ocultas o código de prueba
    dentro de la aplicación que pueden ser explotadas por el atacante.

Como en otros contextos,
los ciberdelincuentes que atacan aplicaciones móviles
pretenden pasar desapercibidos y robar información sensible
—incluida la propiedad intelectual—,
alterar la funcionalidad de la aplicación,
redistribuirla ilegalmente,
infiltrarse en los dispositivos de los usuarios
y afectar a la reputación de los desarrolladores
y propietarios de las aplicaciones.
Las empresas responsables que desarrollan
sus aplicaciones móviles o las desarrollan para terceros
y que forman parte de la cultura DevSecOps
optan por mantener la seguridad de sus productos
bajo evaluación continua para prevenir ciberataques
y pérdidas consecuentes.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/devsecops/"
title="Empieza ya con la solución DevSecOps de Fluid Attacks"
/>
</div>

## Aquí es donde entra MAST

Las pruebas de seguridad de aplicaciones móviles
(MAST, por su nombre en inglés)
analizan estas aplicaciones durante
o después de su desarrollo para identificar problemas de seguridad
en ellas según la plataforma móvil en la que se ejecutan
y los marcos en los que están desarrolladas.
MAST empieza por la comprensión del propósito empresarial de cada aplicación,
los usuarios a los que va dirigida
y los tipos de datos que almacena, maneja y transmite.
La identificación de vulnerabilidades
durante MAST debe ir rápidamente seguida
de su remediación para reducir la superficie de ataque
y proteger así a las organizaciones
y a sus usuarios o clientes de los agresores.

Las técnicas de evaluación más utilizadas en MAST
para la detección de vulnerabilidades
son las pruebas de seguridad de aplicaciones
estáticas ([SAST](../../producto/sast/))
y las pruebas de seguridad de aplicaciones
dinámicas ([DAST](../../producto/dast/)).
[La primera es](../seguridad-app-sastisfactoria/) la que accede
a la estructura interna de la aplicación mientras
no se está ejecutando para evaluar su
código fuente, _byte_ o binario.
No detecta problemas relacionados con datos en transición o en reposo.
La segunda es la que, sin acceso a la estructura,
analiza la funcionalidad de la aplicación mientras se ejecuta.
Simula ataques contra la aplicación y verifica sus reacciones
(la efectividad de sus controles de seguridad).

Aplicar una de estas técnicas, o incluso ambas,
basándose únicamente en la automatización,
es insuficiente para una evaluación holística de una aplicación móvil.
Las herramientas automáticas pueden actualizarse constantemente
en función de las bases de datos
y las nuevas versiones de las plataformas móviles,
pero se centran en la detección de algunas vulnerabilidades conocidas.
Para una mejor cobertura en MAST,
deben incluirse [pruebas de penetración manuales](../../soluciones/pruebas-penetracion-servicio/)
continuas.
Este procedimiento realizado por expertos
en seguridad aumenta la precisión,
reduciendo las tasas de falsos positivos
y no pasando por alto vulnerabilidades más complejas y graves,
incluso de día cero.

Cuando una organización solicita este servicio,
busca prevenir la explotación de vulnerabilidades
en las aplicaciones que emplea u ofrece a sus usuarios.
Busca asegurar que los controles de seguridad
de la aplicación funcionan correctamente y así proteger la integridad
y confidencialidad de su información y la de sus usuarios.
Al utilizar servicios de distribución digital,
como Google Play Store o Apple App Store,
las organizaciones familiarizadas con MAST
saben que estos proveedores no revisan completamente sus aplicaciones.
(Ni Google ni Apple realizan evaluaciones dinámicas, por ejemplo.)
Ellas saben que es su propio deber
obtener análisis integrales de su _software_.

## MAST con Fluid Attacks

No importa si tu empresa es pequeña o grande.
Aunque las segundas suelen ser más buscadas
por los ciberdelincuentes,
las primeras también pueden ser víctimas de ciberataques
a través de sus propias aplicaciones.
En Fluid Attacks ofrecemos MAST,
integrando SAST, DAST y _pentesting_
para identificar vulnerabilidades en
tus [aplicaciones móviles](../../../systems/mobile-apps/).
(Nuestro equipo de *pentesters* cuenta con certificaciones
como [eMAPT](../../../certifications/emapt/)
para evaluar la seguridad de aplicaciones móviles
y otras muchas credenciales que [puedes consultar aquí](../../../certifications/)).
Introducimos fácilmente nuestras pruebas
en tu ciclo de vida de desarrollo de _software_
para analizar continuamente tu código
y configuraciones para identificar problemas de seguridad
y que así puedas remediarlos antes de mover
tus aplicaciones a producción.
Además, es posible que utilices nuestras pruebas
no solo en tus aplicaciones,
sino que también las solicites
para comprobar muchos otros sistemas.
Te recordamos que, dentro de nuestras técnicas de análisis,
también disponemos de [SCA](../../producto/sca/)
para detectar problemas de seguridad
en tus componentes de código abierto o de terceros.

Puedes [contactarnos](../../pages/contactanos/)
para obtener más información sobre MAST
en Fluid Attacks y sus capacidades.
Y si quieres empezar a disfrutar de nuestras pruebas automáticas,
puedes solicitar la prueba gratuita de 21 días
de nuestro [plan Essential de Hacking Continuo aquí](https://app.fluidattacks.com/SignUp).
