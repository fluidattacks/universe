---
slug: blog/incidente-de-crowdstrike-julio-2024/
title: Sobre el incidente de CrowdStrike
date: 2024-07-26
subtitle: Una lección de este colapso mundial es moverse hacia la izquierda
category: opiniones
tags: ciberseguridad, empresa, software, windows, tendencia
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1722027176/blog/crowdstrike-incident-july-2024/cover_crowdstrike_july_2024.webp
alt: Foto por James Lee en Unsplash
description: Un defecto en una actualización de CrowdStrike colapsó 8,5 millones de máquinas. Creemos que este suceso muestra que hay que probar la seguridad desde las primeras fases del desarrollo.
keywords: Caida De Crowdstrike, Caida De Crowdstrike Falcon, Defecto De Crowdstrike Falcon, Colapso De Microsoft, Colapso De Windows, Colapso Informatico Mas Grande De La Historia, Shift To The Left, Hacking Etico, Pentesting
author: Jason Chavarría
writer: jchavarria
name: Jason Chavarría
about1: Escritor y editor
source: https://unsplash.com/photos/white-and-black-bird-in-close-up-photography-e50B_uRoPow
---

El 19 de julio,
los sistemas de Windows sufrieron un ciclo recurrente
de pantalla azul de la muerte (BSOD, por su nombre en inglés) y reinicio
por un problema del sensor de la empresa de ciberseguridad CrowdStrike
usado para recopilar datos de telemetría
sobre posibles nuevas técnicas de amenaza.
Las imágenes de personas amontonadas en aeropuertos de todo el mundo,
mientras sus vuelos se quedaban en tierra,
fueron quizá la representación más visible del caos
que ha supuesto este último apagón informático.
¡Se cancelaron más de 3.300 vuelos!
Pero otros sectores también se vieron afectados.
Con unos **8,5 millones** de dispositivos averiados,
este suceso ha supuesto una grave pérdida económica mundial.
Las respuestas de CrowdStrike y de Microsoft han sido rápidas,
proporcionando los pasos para la recuperación de las máquinas.
Desde entonces,
la empresa de ciberseguridad también ha dado explicaciones
sobre lo que falló en su sensor.
De su explicación,
percibimos algo crucial que consideramos una lección
sobre el desplazamiento de las pruebas de seguridad hacia la izquierda.

## En qué consistió la interrupción informática

El incidente [afectó](https://www.crowdstrike.com/blog/falcon-content-update-preliminary-post-incident-report/)
al sensor de seguridad de puntos finales de CrowdStrike
para sistemas Microsoft Windows.
Un tipo de configuración de contenido de seguridad para el sensor
indica a este qué comportamientos específicos debe observar,
detectar
o impedir.
El personal de detección de amenazas identifica así probables acciones
de adversarios.
La plataforma de CrowdStrike recibe a menudo actualizaciones
de la configuración del contenido de seguridad.
Una actualización específica enviada a *hosts* de Windows
con la versión 7.11 del sensor
hizo que los sistemas se bloquearan,
esto es,
si los sistemas estaban en línea durante una hora específica
en la que la actualización estaba disponible.
El colapso se debió a la incapacidad del sistema de CrowdStrike
para manejar una excepción
provocada por una lectura de memoria fuera de los límites
(que consiste en la lectura de datos fuera del búfer previsto),
a su vez provocada por el contenido de seguridad problemático
que la empresa desplegó.
Los sistemas Windows quedaron atrapados en un bucle interminable
de BSOD y reinicio.

Lo que permitió entregar la actualización fue la confianza de la empresa
en los resultados exitosos de sus pruebas de estrés
realizadas en la fase de preparación,
en no haber tenido problemas con actualizaciones anteriores,
y en la aprobación por su sistema validador de contenidos.
Este último evalúa que todo esté bien en la actualización para su publicación,
y debido a un error en este caso no vio problema alguno.

Al no tener acceso a sus sistemas temporalmente,
muchas empresas no pudieron operar con normalidad.
[Según una estimación](https://www.parametrixinsurance.com/in-the-news/crowdstrike-to-cost-fortune-500-5-4-billion),
unas 125 de las empresas Fortune 500 de Estados Unidos fueron afectadas.
Además,
dice que se enfrentan a una pérdida directa colectiva
de **5.400 millones de dólares**.
Un 57% de las pérdidas causadas por el suceso
la sufrirían los sectores de la salud y bancario.
En cuanto a las compañías aéreas,
que en EE. UU. incluyeron a Delta, United Airlines y American Airlines,
sus pérdidas se estiman en 143 millones de dólares cada una.
Otros servicios afectados para los que se prevén pérdidas son los informáticos,
de comercio minorista y mayorista,
financieros,
y de manufactura.

La recuperación de máquinas ha sido un suplicio,
ya que en la mayoría de los casos ha requerido trabajo manual
por parte del personal de informática.
[Microsoft aconsejó](https://arstechnica.com/information-technology/2024/07/crowdstrike-fixes-start-at-reboot-up-to-15-times-and-get-more-complex-from-there/)
el reinicio como solución eficaz,
indicando que pueden ser necesarios varios intentos (incluso 15 reinicios)
para que la estrategia funcione.
Se sugirieron otras soluciones si la anterior no funciona,
entre ellas,
restaurar el sistema a una versión sin la actualización de CrowdStrike
o arrancar la máquina en modo seguro
para eliminar manualmente el archivo problemático.
Por cierto,
no es una sorpresa que actores maliciosos [han aprovechado el evento](https://www.scmagazine.com/news/5-ways-threat-actors-are-taking-advantage-of-the-crowdstrike-outage)
para poner trampas haciéndolas pasar como soluciones para los usuarios.

Las circunstancias han llegado a una medida dura:
Lo que [se dice que algunos denominan](https://homeland.house.gov/2024/07/22/chairmen-green-garbarino-request-public-testimony-from-crowdstrike-ceo-following-global-it-outage/)
el colapso informático más grande de la historia
es la causa de que se haya requerido el testimonio público
del CEO de CrowdStrike
ante el House Committee on Homeland Security de EE. UU.
Esto,
a pesar de que la empresa de ciberseguridad ha actuado con diligencia
para evitar que los daños fueran a más.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/devsecops/"
title="Empieza ya con la solución DevSecOps de Fluid Attacks"
/>
</div>

## Otra llamada a mover la seguridad hacia la izquierda

A las empresas de ciberseguridad,
este incidente nos sacude.
En realidad somos facilitadores importantes del éxito en las operaciones
de empresas de todo el mundo.
Por ello,
este suceso nos recuerda que debemos estar mucho más atentos
de que nuestros productos se prueben a fondo antes de cada lanzamiento
y que las propias pruebas sean correctas y exhaustivas.
Por supuesto,
durante este incidente,
CrowdStrike [ha publicado](https://www.crowdstrike.com/blog/falcon-content-update-preliminary-post-incident-report/)
una serie de medidas a tomar para prevenir futuros incidentes como este
con su sensor.
Mencionan, por ejemplo,
pruebas locales para desarrolladores y mejoras en su validador de contenidos,
como más pruebas de validación.
Sin embargo,
lo que nos resulta más interesante
es su mención de las revisiones manuales de código
y la evaluación de los procesos de calidad
desde el desarrollo hasta el despliegue,
ambas por parte de terceros.

Sentimos la necesidad de resaltar la cuestión
de que la confianza en la prevención de defectos
de la actualización de contenidos
se situó en las fases de prueba y preparación,
es decir, justo antes del despliegue en producción.
Y es que en la línea que va de izquierda a derecha
que representa el ciclo de vida del desarrollo,
desde los requisitos hasta el mantenimiento, respectivamente,
la seguridad debe moverse a la izquierda.
Este desplazamiento a la izquierda en el desarrollo de *software*
significa **probar la seguridad antes
en el ciclo de vida de desarrollo de *software* (SDLC)**,
es decir,
antes que en la fase de pruebas tradicional.
En lugar de esperar a las fases de preparación o posteriores al despliegue,
en esta postura,
las pruebas comienzan en las fases iniciales del desarrollo,
incluidas las de recopilación de requisitos, diseño, y escritura de código.
Al incorporar las pruebas de seguridad y funcionalidad en una fase temprana,
los desarrolladores pueden identificar y corregir las vulnerabilidades
antes de seguir adelante.
Esto evita la acumulación de errores,
reduce la probabilidad de que surjan problemas críticos en fases posteriores
y hace que la remediación sea menos costosa
que si se hace después de que el producto se haya entregado
a los usuarios finales.
En el incidente analizado aquí vemos claramente lo costosos que pueden resultar
los problemas descubiertos después de la publicación.

Los desarrolladores deben tener presente la seguridad mientras escriben código
y ser capaces de revisar manualmente la seguridad del código
escrito por sus compañeros.
De hecho, una revisión por terceros es un cuidado extra,
aun con ella los propios desarrolladores deben ser
desarrolladores de código seguro y de pruebas exhaustivas
para las funcionalidades que añaden.

En la misma línea,
sentimos la necesidad de subrayar la importancia de *dogfooding*
(es decir, probar los productos desarrollados por la propia empresa
como si uno fuera un usuario final).
Esto debe hacerse antes de hacer públicas
las actualizaciones de los productos.
Entendemos, por el [análisis preliminar posterior al incidente](https://www.crowdstrike.com/blog/falcon-content-update-preliminary-post-incident-report/)
por CrowdStrike,
que la empresa hace *dogfooding* para un tipo de configuración
de contenido de seguridad de su sensor,
pero no lo hace para el tipo que tuvo la culpa en este incidente.
Vale la pena señalar,
entonces,
que esta estrategia debería considerarse
para cada parte de los productos que uno ofrece a los usuarios finales.

Así que,
lector,
piensa en tus prácticas de desarrollo
y reconoce si estás entretejiendo continuamente la seguridad
en todo el SDLC.
Piensa en la importancia de tu producto para la comunidad
y las operaciones de sus empresas clientes.
Y si quieres que te ayudemos a desarrollar y desplegar *software* seguro,
no dudes en [preguntarnos](../../contactanos/)
por nuestra solución [Hacking Continuo](../../servicios/hacking-continuo/).
