---
slug: blog/cadena-suministro-sector-financiero/
title: Cadena de suministro segura en empresas financieras
date: 2024-06-14
subtitle: Gestionando la cadena de suministro de software en el sector financiero
category: filosofía
tags: ciberseguridad, empresa, tendencia, riesgo, software
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1718314726/blog/supply_chain_finance_sector/cover_finance_supply_chain.webp
alt: Foto por FlyD en Unsplash
description: La cadena de suministro de software en el sector financiero es una labor difícil de gestionar. Pero es una tarea posible. Hablaremos de riesgos, tendencias y ofrecemos ciertas recomendaciones.
keywords: Cadena De Suministro, Instituciones Financieras, Ataques A La Cadena De Suministro Instituciones Financieras, Proveedor, Seguridad De La Cadena De Suministro, Amenazas Y Tendencias, Recomendaciones Para Gestion De Riesgos En Cadena De Suministro, Hacking Etico, Pentesting
author: Wendy Rodriguez
writer: wrodriguez
name: Wendy Rodriguez
about1: Escritor y editor
source: https://unsplash.com/photos/pink-padlock-on-silver-chain-IMbquw-IQhg
---

Como hemos visto en nuestra anterior [*post*](../mayores-violaciones-datos-sector-financiero/),
las instituciones financieras llevan mucho tiempo siendo
un objetivo codiciado para los ciberataques.
Y ahora, una amenaza va en ascenso: los ataques a la cadena de suministro.
Estos ataques no se dirigen directamente a la institución financiera,
sino que se infiltran en sus proveedores de *software*
u otros terceros dentro de la compleja red de proveedores
que conforman el ecosistema financiero moderno.
Una vez dentro,
los atacantes causan un efecto dominó
al que se enfrentan tanto el proveedor afectado
como la organización que utiliza ese servicio.
Ambas empresas pueden sufrir violaciones de datos,
instalación de *malware*, dificultades operativas
y posiblemente incluso futuras multas reglamentarias.
Un ataque exitoso a la cadena de suministro también puede dañar
la confianza de clientes y socios por igual.

El año pasado,
el panorama de amenazas en la cadena de suministro de *software* reveló
cómo está cambiando el nivel de complejidad de estos ataques,
haciéndolos más peligrosos.
En 2023,
se descubrieron sofisticados ataques dirigidos al sector bancario.
En ambos ataques
los actores de amenaza subieron paquetes maliciosos a NPM,
el mayor registro de *software* del mundo.
En el primer ataque,
el paquete incluía una carga dañina
para interceptar secretamente la información de inicio de sesión
y enviarla a una dirección remota.
En el segundo ataque, los paquetes
contenían *scripts* que, tras su instalación,
identificaban el sistema operativo de la víctima
y descifraban los archivos encriptados importantes de los paquetes.
Posteriormente,
los archivos se utilizaron para descargar un binario malicioso
en el sistema de la víctima.
El atacante utilizó Havoc Framework,
que es un marco avanzado de mando y control posterior a la explotación.
Este marco permite a los atacantes manejar
y evadir ataques eludiendo las medidas de seguridad como Windows Defender.
Es importante mencionar que el atacante se hizo pasar
por un empleado del banco con una página falsa
de LinkedIn asociada a la entidad,
lo que llevó a los expertos en seguridad que investigaban
el incidente a considerar que toda la situación
podría ser una prueba de penetración.
Esto demuestra una estrategia inteligente que los atacantes
pueden utilizar para engañar a los investigadores.

Estos ataques evidencian la complejidad
con la que los actores maliciosos están llevando a cabo sus ataques.
Los métodos están evolucionando con el fin
de explotar las vulnerabilidades de la cadena de suministro,
lo que hace imperativo que las medidas de seguridad
evolucionen también para convertirse en un frente más fuerte.
Abordaremos algunas buenas prácticas
y recomendaciones para quienes se ocupan de proteger
a las instituciones financieras.
Pero antes, recordemos qué es un ataque a la cadena de suministro.

## ¿Qué es un ataque a la cadena de suministro?

Un ataque a la cadena de suministro es un tipo de ciberataque
que se dirige a los eslabones débiles de la red de una organización,
explotando vulnerabilidades en distribuidores y proveedores externos.
Los actores de amenaza obtienen acceso a estos sistemas y los utilizan
para lanzar ataques contra el objetivo final,
en este caso, la entidad financiera.
Los ataques a la cadena de suministro son peligrosos
porque pueden ser muy difíciles de detectar.
Las organizaciones pueden incluso no ser conscientes de que sus proveedores
han sido vulnerados o puede que ni siquiera tengan una visibilidad clara
de los componentes de terceros en su *software*.

Estos ataques se dirigen a proveedores de confianza
que proporcionan *software* o servicios que son vitales
para la cadena de suministro de la organización.
Puede tratarse de un proveedor, un vendedor
un cliente o una biblioteca de *software* de terceros
de la que depende la empresa.
Los atacantes pueden manipular el *software* durante
el proceso de fabricación o distribución.
Podrían atacar a un proveedor de confianza
y obtener acceso a través de una debilidad que encuentren.
Pueden robar credenciales del proveedor externo,
incluso podrían rastrear vulnerabilidades
y averiguar qué organizaciones utilizan al proveedor vulnerable.

### ¿Por qué el sector financiero es vulnerable a estos ataques?

Además de ser un objetivo de mucho valor
debido a la [información sensible](../proteccion-datos-servicios-financieros/)
a la que acceden y manejan,
las instituciones financieras son especialmente vulnerables
ya que utilizan ampliamente servicios de terceros
para sus plataformas en línea
(como la banca digital,
las plataformas de negociación o incluso sus sistemas centrales).
A menudo utilizan servicios de terceros para el almacenamiento en la nube,
procesamiento de datos u otras funciones importantes.
Estos complejos ecosistemas de *software* suponen
un mayor número de puntos de entrada,
una superficie de ataque amplificada, lo que hace a
[**la seguridad de la cadena de suministro**](../../../blog/software-supply-chain-security/)
una prioridad crítica.

## Tendencias y riesgos

Como ya se ha dicho,
los ataques a la cadena de suministro son cada vez más elaborados
y ahora están dirigiéndose a nuevas áreas con tácticas combinadas.
El año pasado, las tendencias clave incluyeron:

- **Ataques multipaquete:** Los atacantes dividieron las acciones maliciosas
  en varios paquetes para eludir la detección.

- **Fraudes en cometidos:** Se observó que los ciberdelincuentes actuaban
  como fuentes legítimas y conocidas mientras introducían su código malicioso.

- **Activos digitales abandonados:** Los atacantes explotaron activos antiguos,
  como *buckets* de AWS abandonados, para introducir código malicioso
  y hacer que parecieran un mecanismo de entrega fiable.

- **La clásica ingeniería social:** Se utilizaron las redes sociales
  para establecer confianza;
  caso (1) perfiles de desarrolladores falsos que engañaban
  a los usuarios para que usaran paquetes maliciosos de código abierto,
  y caso (2) reutilización Pruebas de Concepto (PoC por sus siglas en inglés)
  para crear *scripts* engañosos de vulnerabilidades recientemente reveladas.

Los ataques a la cadena de suministro de *software* no van a disminuir en 2024;
al contrario, se espera que evolucionen.
Esto conlleva mayores retos y riesgos para las organizaciones,
especialmente el codiciado sector financiero.
Las amenazas destacadas
por [SecurityWeek's Cyber Insights](https://www.securityweek.com/cyber-insights-2024-supply-chain/)
son las siguientes

- Ataques patrocinados por gobiernos que buscan desestabilizar las economías

- Riesgos de consolidación en las cadenas de suministro ampliadas,
  incluida la dependencia de un único proveedor o de cuartos colaboradores

- Ataques sofisticados a la cadena de suministro que aprovechan la IA
  o el aprendizaje automático

- Alianzas entre grupos delictivos técnicamente especializados
  para atacar las cadenas de suministro

- Corrupción de *software* de código abierto (OSS) con paquetes maliciosos
  en repositorios públicos

### ¿Cuáles son los ciberriesgos de la cadena de suministro?

Debido a sus complejos ecosistemas
las instituciones financieras se enfrentan a una amplia gama de riesgos
desde diferentes puntos de entrada.
Según un estudio sobre tendencias en la gestión de ciberriesgos
en la cadena de suministro del 2022 al 2023,
estos son los posibles ciberriesgos y sus probables puntos de entrada:

<image-block>

!["Ejemplos de ciberriesgos previstos"](https://res.cloudinary.com/fluid-attacks/image/upload/v1718639588/airs/es/blogs/im%C3%A1genes%20traducidas/cadena_suministro_sector_financiero/ejemplos_ciberriesgos_previsto.webp)

Tomado del estudio realizado por [PwC](https://www.pwc.com/jp/en/knowledge/thoughtleadership/assets/pdf/financial-supply-chain-cyber-risk.pdf).

</image-block>

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/gestion-vulnerabilidades/"
title="Inicia ahora con la solución de gestión de vulnerabilidades
de Fluid Attacks"
/>
</div>

## Consejos para gestionar los ciberriesgos en la cadena de suministro

Gracias a las autoridades reguladoras,
las empresas se ven obligadas a aplicar las pautas que establezcan
condiciones específicas que se deben cumplir
al utilizar un servicio de terceros.
Por ejemplo,
PCI DSS publicó un documento llamado
"Complemento de información: Garantía de seguridad por parte de terceros".
Este documento describe los requisitos específicos
de PCI DSS aplicables a los proveedores terceros.
Proporciona orientación tanto al proveedor
como a la empresa que adquiere el servicio,
y detalla muchos controles que ambas empresas deben tener,
como el control de acceso seguro y la seguridad de la red.
El uso responsable de proveedores externos también
es incentivado por otras leyes y regulaciones,
como la ley de privacidad del consumidor de California
(CCPA, por sus siglas en inglés)
y el reglamento general de protección de datos
(GDPR, por sus siglas en inglés).
Estas, entre otras leyes,
obligan a las empresas a cerciorarse que los proveedores
con los que trabajan se ciñen a la reglamentación aplicable.

Las siguientes recomendaciones
para la seguridad de la cadena de suministro de *software*
proceden de expertos de instituciones financieras de todo el mundo.
En ellas se analizan las etapas y las consideraciones que una persona
responsable de la seguridad de la cadena de suministro debería tener en cuenta
a la hora de elegir y tratar con un proveedor externo.

- Etapa de preselección: estudio previo de los riesgos de seguridad

    - Recopilar y evaluar datos que estén públicamente disponibles
      para analizar los riesgos de seguridad del proveedor y sus productos.
    - El uso de servicios externos de evaluación de riesgos
      podría agilizar este procedimiento.

- Etapa de contratación: evaluación de los riesgos de seguridad

    - Asignar miembros del personal técnico/de seguridad al equipo
      de evaluación, y basar la evaluación en escenarios de amenazas actuales.
    - Prepárese para la posibilidad de incidentes y documentar el alcance
      de las obligaciones y los plazos para informar de tales incidentes
      en un acuerdo o contrato de servicio.

- Gestión de riesgos de seguridad con subcontratistas

    - Examinar los procedimientos de gestión de riesgos de los subcontratistas
      que manejan sistemas de alto riesgo en una visita presencial.
    - Si el trabajo se subcontrata a una cuarta parte,
      insistir en que la gestión de seguridad la realice
      el subcontratista original.
    - Exigir a todas las partes de la cadena que apliquen medidas de seguridad
      en el mismo grado que el que exige tu propia empresa.

- Gestión del *software*

    - Cuando se instalen nuevos productos,
      la [gestión de la configuración](../../learn/que-es-gestion-de-configuracion/)
      del *software* debe realizarse con
      y vincularla a la [gestión de vulnerabilidades](../que-es-gestion-de-vulnerabilidades/).
    - Utilizar listas de materiales de *software* ([SBOMs](../../learn/que-es-sbom/))
      para supervisar los recursos y
      mejorar la eficacia y la seguridad.
    - Utilizar herramientas de gestión de *software* de código abierto
      para simplificar el proceso de elección de este
      e identificar dependencias.

- Gestión del *hardware*

    - Gestionar meticulosamente los activos y crear un mecanismo
      que permita actualizar rápidamente el *firmware*.
    - Realizar pruebas de seguridad basadas en escenarios de amenazas.

- Informes para directivos

    - Redactar reportes en un lenguaje "empresarial"
      y basarlos en cómo afectarán los gastos al ingreso,
      la satisfacción del cliente, la lealtad a la marca y la reputación.
    - En cuanto al informe sobre riesgos de ciberseguridad,
      considerar los enfoques basados en riesgos, en costos,
      en TI y otros para determinar el camino más adecuado para tu organización.

### Buenas prácticas de seguridad para la cadena de suministro

Proteger la cadena de suministro de *software* es fundamental
para que las instituciones financieras estén mejor preparadas
y protegidas contra las ciberamenazas.

1. **Gestiona los riesgos de los proveedores de *software*:** Antes de asociarte
   con un proveedor, asegúrate de que cumple tus requisitos de seguridad.
   Realiza una evaluación exhaustiva de su postura de seguridad
   (p. ej., prácticas de desarrollo seguras, respuesta a incidentes,
   controles de acceso, etc.)
   antes de instalar cualquier cosa, y monitorízala continuamente.
   Dentro de las cláusulas contractuales, incluye cláusulas de seguridad estrictas
   que obliguen al proveedor a cumplir tus requisitos específicos
   y te notifique de cualquier incidente de seguridad.

2. **Utiliza una herramienta de análisis de composición de *software* (SCA):**
   Gestiona los componentes de terceros y de código abierto de tu *software*.
   SCA puede identificar las vulnerabilidades del *software* de código abierto,
   lo que te ayuda a priorizar la aplicación de parches
   o actualizar los componentes vulnerables
   antes de que los atacantes consigan explotarlos.
   También puede ayudar a identificar conflictos de licencia y generar SBOMs.
   Esto es lo que ofrece Hacking Continuo de Fluid Attacks
   con su [técnica SCA](../../producto/sca/).

3. **Crea tu propia lista de materiales de *software* (SBOM):** Crea y mantén
   una lista de materiales detallada
   que enumere todos los componentes de *software*
   utilizados dentro de tu organización,
   incluidas las bibliotecas de código abierto y las dependencias de terceros.
   Esto te ayudará a identificar posibles vulnerabilidades
   en tu cadena de suministro.
   Al desarrollar *software*,
   un SBOM bien elaborado a lo largo del SDLC ayuda
   a minimizar los problemas en el futuro.
   Existen formatos estándar de SBOM, como CycloneDX y SPDX,
   que ayudan a contar con SBOMs de alta calidad.
   Nuestra plataforma genera SBOMs utilizando estos dos formatos estándar,
   que muestran información, dependencias y vulnerabilidades de forma coherente.
   Obtén más información del proceso de creación
   de SBOM de Fluid Attacks [aquí](https://help.fluidattacks.com/portal/en/kb/articles/analyze-your-supply-chain-security).

4. **Protege el SDLC:** Integra prácticas de seguridad, como los SBOM,
   a lo largo de todo el SDLC, desde el desarrollo del código
   hasta su despliegue.
   [Revisión del código](../revision-codigo-fuente/),
   [escaneo de vulnerabilidades](../escaneo-de-vulnerabilidades/),
   [pruebas de penetración](../pentesting/),
   y, muy importante,
   [reparación de vulnerabilidades](../../../blog/vulnerability-remediation-process/)
   se incluyen como prácticas de seguridad.

5. **Gestiona las vulnerabilidades:** Identificar y abordar proactivamente las
   vulnerabilidades puede ayudar a reducir significativamente
   el riesgo de ataques exitosos dirigidos a debilidades
   dentro de tu entorno de desarrollo,
   incluyendo los componentes de *software* de terceros.
   La detección temprana te permite parchear
   o actualizar rápidamente los componentes vulnerables.
   Los análisis continuos y las revisiones manuales de los activos digitales
   pueden ayudar a encontrar posibles vulnerabilidades
   antes de que un atacante pueda explotarlas.
   La gestión de vulnerabilidades también ayuda
   a mejorar la gestión con proveedores
   y a cumplir con las regulaciones,
   además de reducir los vacíos en tu superficie de ataque.
   Descubre cómo puedes beneficiarte de la
   [solución de gestión de vulnerabilidades](../../soluciones/gestion-vulnerabilidades/)
   de Fluid Attacks.

## Evaluar continuamente para gestionar la cadena de suministro

La gestión de los riesgos de suministro de *software*
puede mejorarse con todo lo anterior,
y sugeriríamos ir un paso adelante evaluando con *pentesters* continuamente.
La seguridad de la cadena de suministro va más allá
de la gestión de los riesgos de terceros.
De hecho,
el enfoque de seguridad de la cadena de suministro de *software*
incluye proteger el *software* propio,
la infraestructura y mucho más.

Nuestra solución AppSec incluye [*pentesters* o *hackers* éticos](../../soluciones/hacking-etico/),
quienes piensan como actores maliciosos.
Conocen sus estrategias y utilizan ese conocimiento
para guiarte y defender tus sistemas.
Nuestro equipo de *pentesters* realiza pruebas manuales,
trabajando junto con herramientas de escaneo de vulnerabilidades
([SAST](../../producto/sast/),
[DAST](../../producto/dast/),
[SCA](../../producto/sca/)
y [CSPM](../../producto/cspm/))
e IA para lograr evaluaciones exhaustivas que buscan debilidades
y posibles problemas de forma precisa.
A partir de ahí, el equipo de *hackers*
y las herramientas informan a tu equipo desde nuestra plataforma,
la cual puede integrarse en VS Code IDE con [nuestra extensión](https://help.fluidattacks.com/portal/en/kb/articles/install-the-vs-code-extension)
para agilizar aún más el proceso de gestión de vulnerabilidades
para tus desarrolladores.

Este enfoque proactivo permite a las entidades financieras,
y a cualquier otra industria,
construir una postura de seguridad robusta contra los ataques
a la cadena de suministro.
Las [pruebas de seguridad](../../soluciones/pruebas-seguridad/) continuas
a través de nuestra solución se realizan desde las primeras etapas del SDLC,
lo que proporciona información inmediata sobre las vulnerabilidades,
y así puedes decidir proactivamente cómo proceder.
Déjanos ayudar a tu organización, [contáctanos ya](../../contactanos/).
