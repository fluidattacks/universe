---
slug: blog/tribu-de-hackers-3/
title: Tribu de hackers red team 3.0
date: 2020-12-09
subtitle: Aprendiendo de la experta en red teams Georgia Weidman
category: opiniones
tags: ciberseguridad, red-team, hacking, pentesting, blue-team
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1620331131/blog/tribe-of-hackers-3/cover_rids16.webp
alt: Foto por Dorinel Panaite en Unsplash
description: Este artículo se basa en el libro "Tribe of Hackers Red Team" de Carey y Jin. Compartimos aquí contenido de la entrevista con Georgia Weidman.
keywords: Red Team, Ciberseguridad Red Team, Hacking Red Team, Pentesting, Hacking Etico, Blue Team, Conocimiento, Tribu De Hackers
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Escritor y editor
source: https://unsplash.com/photos/oTrebzk1v54
---

Aquí vamos con un tercer artículo con
[*Tribe of Hackers Red Team*](https://www.amazon.com/Tribe-Hackers-Red-Team-Cybersecurity/dp/1119643325)
de Marcus J. Carey y Jennifer Jin (2019) como referencia.
En ocasiones anteriores,
habíamos apreciado opiniones y recibido consejos de dos hombres,
expertos en
[*red teaming*](../../soluciones/red-team/):
[(1.0) Marcus Carey](../tribu-de-hackers-1/) y
[(2.0) Benjamin Donnelly](../tribu-de-hackers-2/).
Ahora es el momento de abrir espacio al género femenino dentro
de este apasionante contexto.
Pocas mujeres aparecen en el libro citado,
del mismo modo que pocas mujeres trabajan en *red teams* como Fluid Attacks.
De hecho,
todavía no tienen mucha presencia en el ámbito
del [hacking ético](../../soluciones/hacking-etico/),
hecho que nos gustaría ayudar a cambiar.
Por eso en este artículo, como un pequeño incentivo,
queremos centrarnos en el punto de vista
y algunas recomendaciones
de [Georgia Weidman](https://twitter.com/georgiaweidman)
—emprendedora en serie, evaluadora de penetración,
investigadora de seguridad, oradora, formadora y autora—
para quienes estamos interesados en el [*red teaming*](../ejercicio-red-teaming/).

<div class="imgblock">

![Varvara Grabova](https://res.cloudinary.com/fluid-attacks/image/upload/v1620331132/blog/tribe-of-hackers-3/varvara_v5ioff.webp)

<div class="title">

Foto por [V. Grabova](https://unsplash.com/@santabarbara77)
en [Unsplash](https://unsplash.com/photos/6Jm_LSrf4Zc).

</div>

</div>

## Para los que buscan ser diligentes en los *red teams*

Como le ocurrió a Benjamin Donnelly
(véase [el artículo anterior](../tribu-de-hackers-2/)),
Georgia empezó su carrera en *red teaming*
tras participar en la universidad en una competición de ciberdefensa.
Y aunque se declara una persona con pocas habilidades sociales,
al menos para hablar con alguien de tú a tú,
eso no le ha impedido establecer redes
y conseguir oportunidades para trabajar en *red teams*.
Esto se ha visto reforzado por sus investigaciones sobre seguridad,
sus presentaciones en conferencias
y sus clases de entrenamiento como voluntaria.

En 2014,
Georgia publicó un libro
titulado [*Penetration Testing: A Hands-On Introduction to Hacking*](https://www.amazon.com/Penetration-Testing-Hands-Introduction-Hacking/dp/1593275641).
Ella lo recomienda para que la gente nueva
aprenda sobre *hacking* en un ambiente controlado.
Además, para aquellos interesados en adquirir habilidades de *red team*
(sin actividad ilegal), Georgia sugiere participar en competiciones
como [capturar la bandera](https://medium.com/@thehackersmeetup/beginners-guide-to-capture-the-flag-ctf-71a1cbd9d27c)
(CTF, por su nombre en inglés).
En ellas,
uno tiene el consentimiento para atacar
a los objetivos en diferentes marcos temporales.
Georgia plantea que, en general,
mientras estés practicando en sistemas, aplicaciones, etc.,
que te pertenecen o tienes permiso expreso para atacar,
estás aprendiendo éticamente.

Es llamativo algo que menciona Georgia en relación
con lo que ella espera de las personas que se presentan
a entrevistas como miembros potenciales de *red teams*.
Además de buenas dotes
para la comunicación con públicos técnicos y no técnicos,
ella solicita profesionales apasionados por el oficio.
Por tanto,
plantea que **no busca gente que trabaje de 9 a. m. a 5 p. m.
y se vaya a la casa a jugar videojuegos toda la noche**.
(¿Te has dado cuenta de cuántas horas al día dedicas a los videojuegos?
¿Crees que realmente sabes gestionar tu tiempo?)
Para ella es esencial contar con personas
que investiguen sobre seguridad y presenten sus resultados
al mundo a través de cualquier medio de comunicación disponible.
Georgia busca individuos que,
cuando reconocen que carecen de alguna habilidad en un área concreta,
hacen todo lo posible por obtener los conocimientos correspondientes.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/gestion-vulnerabilidades/"
title="Inicia ahora con la solución de red teaming de Fluid Attacks"
/>
</div>

## Para los que ya sudan sangre en los *red teams*

Cuando Carey y Jin preguntan a Georgia sobre las reglas de juego
(acuerdo para trabajar con un cliente),
ella plantea algo que podemos encontrar a menudo en este campo:
Teniendo en cuenta cómo
gran parte de nuestra sociedad ve a los *hackers*
(genios malignos decididos a destruir el mundo solo
para demostrar a sus rivales que pueden hacerlo),
muchas organizaciones tienen una comprensible resistencia
a permitir que los evaluadores de seguridad las ataquen.
Por esta razón,
las reglas de juego definidas antes del inicio
de las pruebas de seguridad dentro de cada proyecto variarán
en su rigor en función de la comodidad del cliente.
Y esto es algo que siempre debemos respetar.

Georgia expresa más adelante con precisión que romper las reglas del juego,
incluso cuando crees que eso hace que las pruebas
sean más auténticas respecto al mundo real,
solo contribuye a la noción de que los *hackers* éticos
son solo atacantes maliciosos con un trabajo de fachada.
Y no solo eso.
Violar las reglas de un contrato puede llevar incluso a alguna penalización
o simplemente a la anulación de los honorarios acordados,
así como a la pérdida de un cliente para el futuro y,
por qué no,
a dañar la reputación del equipo.

Georgia afirma que la parte más valiosa de
las pruebas de seguridad no es conseguir la administración del dominio,
sino dejar al cliente con una comprensión clara
de sus deficiencias de seguridad y un plan de acción para solucionarlas.
Según ella,
las recomendaciones de remediación de las herramientas automatizadas
a menudo no se ajustan a los planes de negocio
y, por tanto, no son aplicables.
Por este motivo, las ideas detalladas
y contextualizadas de los profesionales de la seguridad
pueden ser mucho más valiosas para
la [gestión de vulnerabilidades](../../soluciones/gestion-vulnerabilidades/).

Inmediatamente después,
lo que comenta Georgia conecta perfectamente
con lo que hacemos en Fluid Attacks a través de nuestra plataforma.
Para Georgia es importante no solo explicar claramente sus resultados,
sino también mantener un diálogo abierto con el *blue team* del cliente,
mientras trabajan en la remediación de los problemas,
por si tienen alguna duda.
Además, como complemento esencial a lo anterior,
ella menciona la validación de la remediación
—la cual, por ejemplo, en nuestra empresa realizamos con reataques—
para asegurar siempre que las vulnerabilidades identificadas
y reportadas han sido cerradas exitosamente.

<div class="imgblock">

![Georgia Weidman](https://res.cloudinary.com/fluid-attacks/image/upload/v1620331131/blog/tribe-of-hackers-3/weidman_svdpmi.webp)

<div class="title">

Imagen original de Georgia,
tomada de [pbs.twimg.com](https://pbs.twimg.com/media/CrYuOfaWcAAXM3u.jpg).

</div>

</div>

## Para empresas que aspiran estar a la vanguardia en seguridad

Muchos clientes acuden a Georgia
en búsqueda de *red teaming* o pruebas de penetración,
cuando en realidad lo que necesitan es empezar
por un análisis de vulnerabilidades o ayuda para desarrollar
un programa básico de seguridad.
Este es un punto crucial que Georgia remarcó para las empresas.
En su ignorancia,
algunas compañías creen que el *red teaming* es **inmediatamente**
necesario cuando su postura de seguridad aún no es robusta,
y lo que se detectará primero son parches faltantes,
contraseñas por defecto y otras cosas similares de fácil detección.
En estos casos, según Georgia,
deberían utilizarse más a menudo escáneres o procesos automatizados.
Eso es lo que hacemos en Fluid Attacks
con nuestra herramienta al principio de los proyectos,
buscando vulnerabilidades conocidas tanto superficiales
como determinísticas para remediarlas lo antes posible.

Asegúrate de resolver primero los problemas de seguridad más sencillos,
luego refuerza tu sistema con todas
las medidas sugeridas por los expertos en seguridad y, por último,
somételo a la destreza de los *hackers* éticos
para que lo evalúen a fondo.

## ¡Eso es todo, amigos!

Si quieres conocer nuestra solución de [pruebas de seguridad](../../soluciones/pruebas-seguridad/)
para tu empresa, puedes [contactarnos aquí](../../contactanos/).
Espero que hayas disfrutado leyendo este artículo,
el tercero de la serie Tribu de *hackers red team*.
¡Hasta pronto!
