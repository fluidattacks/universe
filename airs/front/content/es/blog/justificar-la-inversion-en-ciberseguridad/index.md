---
slug: blog/justificar-la-inversion-en-ciberseguridad/
title: Están esperando un golpe de realidad
date: 2024-03-06
subtitle: ¿Cómo podemos justificar la inversión en ciberseguridad?
category: opiniones
tags: ciberseguridad, empresa, riesgo, software, pruebas de seguridad
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1709774678/blog/justify-investment-in-cybersecurity/cover_justify_investment_in_cybersecurity.webp
alt: Foto por A S en Unsplash
description: Aunque puede ser una tarea difícil porque la ciberseguridad no suele generar beneficios tangibles, aquí presentamos algunas ideas que ayudan a justificar la inversión en ella.
keywords: Inversion En Ciberseguridad, Invertir En Ciberseguridad, Costos Y Beneficios, Roi, Retorno De La Inversion, Exposicion Al Riesgo, Pentesting, Hacking Etico
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Escritor y editor
source: https://unsplash.com/photos/shallow-focus-photo-of-white-and-gray-cat-J7rRzjba-kY
---

"Hasta ahora no nos ha pasado nada malo,
y seguramente nunca nos pasará.
No hemos tenido que gastar millones
en ciberseguridad como esas otras empresas.
Pobres tontos,
invirtiendo una y otra vez; malgastan el dinero en algo
que no les da ni la más mínima prueba tangible
de que haya servido para algo".

Lleno del llamado **sesgo de optimismo** está quien quiera
que salga con algo como lo anterior.
"Suerte", podríamos decir,
es en lo que esta persona —supongamos un CEO—
y su empresa han confiado hasta ahora.
Sin embargo, si mantienen esa postura indefinidamente,
tarde o temprano, es probable que tengan un choque con la realidad,
un gran golpe que podría resultar muy costoso.
**Invertir en ciberseguridad vale la pena**,
pero, como se comenta en este artículo, es algo
que **puede resultar complicado de justificar**,
tanto antes como después del hecho.
(Excluimos aquí los casos en los que la inversión
ya es obligatoria para que una compañía cumpla
los requisitos de su sector industrial).

## Abordando la inversión en ciberseguridad

Por lo general,
los directores de seguridad de la información (**CISOs**)
son los encargados
de **persuadir a los directores o gerentes de sus organizaciones**
para que inviertan en seguridad.
Los CISOs pueden ser bombardeados desde antes de la inversión
por parte de los directivos con preguntas
sobre la pertinencia de dicha inversión y los retornos estimados
y/o, después de la inversión,
con solicitudes de pruebas de que ha sido un éxito.
La cuestión es que responder de forma convincente
a todo esto no suele ser tarea fácil.

Aquí entran en juego los típicos análisis de costos
y beneficios previos y posteriores a la inversión.
Estos últimos incluyen, por ejemplo,
el ROI (return on investment o, en español, retorno de la inversión).
El ROI es una medida de rendimiento para determinar lo eficiente
o rentable que resultó ser una inversión.
Concretamente, es un cociente entre el monto de retorno
y el costo de la inversión.
Así, si ese valor es positivo,
es porque los ingresos fueron mayores que la inversión.
El **problema de utilizar el ROI** es que
de la inversión en ciberseguridad
**no obtenemos retornos tangibles o ingresos monetarios**,
como podría ser el caso de las inversiones en desarrollo
de nuevos productos y campañas de mercadeo.

En este campo,
es más apropiado hablar de **retornos en términos de beneficios**,
los cuales están principalmente relacionados con
la **prevención de pérdidas** y el **ahorro de recursos**.
La seguridad, de hecho,
tiene más que ver con la protección del ROI
que se puede obtener de otras áreas de una organización.
Considerar y analizar estos beneficios
y compararlos con los costos de inversión es la forma en que
a menudo se sugiere justificar la inversión en ciberseguridad
para convencer a los ejecutivos,
pero esto puede ser una tarea compleja.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/pruebas-seguridad/"
title="Inicia ahora las pruebas de seguridad de Fluid Attacks"
/>
</div>

## Antes de decidir invertir

La ciberseguridad engloba todas aquellas actividades encaminadas
a proteger los sistemas informáticos
—incluyendo infraestructuras, operaciones y datos—
pertenecientes a una persona u organización frente a posibles riesgos,
amenazas y ataques.
**Contribuir a esa protección es el beneficio principal** que
una empresa puede obtener invirtiendo en ciberseguridad.
Basta con visitar la página web de un conocido periódico
o revista que cubra temas tecnológicos
para descubrir titulares sobre ciberataques perpetrados
con éxito contra organizaciones de todo tipo
y tamaño en todo el mundo.
No todas, pero muchas de estas víctimas suelen ser aquellas
que no invierten "lo suficiente" en su seguridad,
y algunas pueden haber invertido casi nada.

Invertir casi nada o no "lo suficiente"
podría significar para esas organizaciones
la interrupción de sus operaciones,
el robo de datos confidenciales, pérdidas monetarias,
daños a su reputación y fiabilidad, e incluso la quiebra.
Seamos realistas:
Formar parte del mundo de la tecnología,
en el que abundan las amenazas,
y no pagar lo suficiente por soluciones
y productos de ciberseguridad es una verdadera imprudencia.
Punto. **Invertir garantiza la prevención de molestias** como las mencionadas.
(Si eso no lo reconocen los líderes de la compañía
de la que formas parte, te recomiendo que busques otro trabajo).
Pero ahora la pregunta es,
**¿cómo sabremos que la inversión será "suficiente"?**

Una respuesta simple y obvia a esta pregunta sería
que **la solución adoptada erradicaría todo lo que representa
un riesgo de ciberseguridad**.
Pero esto no tendría en cuenta nuestros recursos limitados
y las dificultades para saber algo con absoluta certeza.
Una opción recomendable es buscar un equilibrio:
La inversión no impide ni dificulta el crecimiento de la empresa y,
al mismo tiempo,
no la deja vulnerable a daños significativos
que la lleven al colapso.
Esto sería especialmente cierto en el caso
de las pequeñas y medianas empresas.
Ya en el caso de las grandes empresas,
principalmente, el equilibrio consistiría en que
no se produzcan retrasos en las operaciones,
no se derroche dinero y la exposición al riesgo
se mantenga lo más baja posible.
Pero, al menos en gran medida,
**¿cómo podemos conocer nuestra exposición al riesgo?**

Los riesgos de ciberseguridad
dependen de factores como el tamaño
y los ingresos de la organización,
el número de dispositivos informáticos utilizados,
el sector al que pertenece,
la información que maneja y almacena,
el número de empleados y su formación en seguridad,
y el número y la complejidad de los productos
de *software* o aplicaciones que desarrolla y utiliza,
entre otros.
Sin embargo,
**la exposición al riesgo viene determinada fundamentalmente
por las vulnerabilidades**
o debilidades presentes en el *software*
y en las personas.
Si pudiéramos conocer *todas* las vulnerabilidades
de seguridad de una organización,
conoceríamos *toda* su exposición al riesgo.
Lo que generalmente conseguimos es acercarnos a ello.

La **explotación de vulnerabilidades** por parte
de ciberdelincuentes suele generar **pérdidas para una compañía**.
Las vulnerabilidades varían en su severidad
o en la exposición al riesgo que representan y,
por lo tanto, en el daño que su explotación podría generar.
**Pensar en términos de posibles pérdidas** y
cuantificarlas puede ser útil para hacer
comparaciones con los costos de inversión.
Así, si lo que podríamos perder es más significativo
que lo que invertiríamos,
sería prudente invertir para evitar esas pérdidas.
Nuestra inversión sería "suficiente".
Pero aquí el asunto se ensombrece porque determinar
o cuantificar esas pérdidas puede ser bastante complejo.

De hecho,
estos cálculos se facilitan cuando se definen áreas específicas
de impacto por la explotación de vulnerabilidades concretas
durante periodos determinados, como, por ejemplo,
las relacionadas con la denegación de servicios.
No obstante, **un análisis juicioso de pre-inversión**
implicaría hacerlo para absolutamente todas las áreas
que pudieran verse afectadas,
considerando todas las vulnerabilidades
(o al menos las de mayor severidad) que pudieran
estar presentes en el *software* (según su naturaleza)
y todos los posibles efectos o impactos de su explotación.
Además,
deberían considerarse las probabilidades
de que se produzcan estas situaciones.
No es que esto sea imposible,
pero sería una tarea gigantesca para expertos en análisis de datos
y probabilidades con una cantidad de datos indudablemente monumental,
por lo que, curiosamente, también podríamos preguntarnos,
**¿vale la pena invertir en ello?**

Algunas organizaciones,
que desconocen o no están dispuestas a invertir
en métodos de predicción tan complejos,
recurren a **enfoques más imprecisos o débiles**,
como por ejemplo,
revisar las pérdidas sufridas por compañías similares
en incidentes de seguridad conocidos públicamente.
Sin embargo,
las situaciones e impactos para una compañía
y otra pueden depender de numerosas variables diferentes.
El hecho de que alguien haya tenido que pagar X millones de dólares
por un ataque de *ransomware* no es un buen punto de referencia
para decir que eso es a lo que uno se enfrentaría
y luego determinar si conviene
o no invertir cierta cantidad de dinero en seguridad.

Dejando a un lado estas comparaciones ingenuas
e incluso la opción de estimar las probabilidades
de que se produzcan determinadas pérdidas en tu compañía,
reconocer que el problema se deriva
de las vulnerabilidades existentes en tus sistemas
te permite de todos modos deducir que remediarlas ayudará
a reducir esas probabilidades inciertas.
Pero para remediarlas, primero hay que identificarlas.
Y para identificarlas, **hay que invertir** en ciberseguridad,
concretamente en **pruebas de seguridad**.
Esta primera inversión debe hacerse sin pensar demasiado,
más bien como un compromiso inmediato.
Aquí hay que empezar invirtiendo para acercarse
a tener una idea de la inversión "suficiente" o necesaria.
Se trata de una cuestión de prueba y error.
Hay que detectar las vulnerabilidades lo antes posible;
**entonces se podrán evaluar las inversiones según los resultados**.

## Después de decidir invertir

Una vez que hayas decidido no esperar por un golpe de realidad,
tienes que pasar a elegir qué productos
y soluciones de pruebas de seguridad vas a adquirir
e implementar en tu empresa.
De acuerdo con lo que hemos mencionado,
la inversión no debería afectar a tu productividad
y crecimiento económico y, al mismo tiempo,
debería mantener tu exposición al riesgo bastante baja.
Aquí es donde **se evalúan las ofertas
y resultados de los proveedores de ciberseguridad**.

La inversión en pruebas de seguridad
debe justificarse principalmente por la cobertura,
precisión, rapidez y profundidad de los métodos
y técnicas de **detección de vulnerabilidades** que te ofrecen,
así como por los medios que te proporcionan
para la **priorización y gestión de vulnerabilidades**,
y lo que te permiten conseguir en **tiempos y tasas de remediación**.
Pregúntate lo siguiente:
¿Esa solución nos ofrece una amplia visibilidad
de nuestra superficie de ataque
y un entendimiento de su exposición al riesgo?
¿Nos permite reconocer nuestros componentes de *software* y
descartar aquellos que son inútiles o innecesarios?
¿Ahorra a nuestros desarrolladores tiempo y
esfuerzo en remediar vulnerabilidades con diversos medios de apoyo?
¿No obstaculiza el despliegue de nuestro *software* a producción?

**Los métodos de evaluación proporcionados
para tus sistemas deberían ser variados**
y orientarse, por ejemplo,
al código fuente estático ([SAST](../../producto/sast/)),
al *software* en ejecución ([DAST](../../producto/dast/))
y a los componentes y dependencias
de *software*de terceros ([SCA](../../producto/sca/)).
Además,
debería haber evaluaciones realizadas tanto
por **herramientas automatizadas como por expertos**
(p. ej., *pentesters* o [*hackers* éticos](../../soluciones/hacking-etico/)),
ya que las primeras a menudo informan de "problemas de seguridad"
donde no los hay (falsos positivos)
o no consiguen detectar algunos reales (falsos negativos).

Vale la pena recalcar que invertir en pruebas de seguridad
es totalmente inútil si solo vas a sentarte
a ver cómo te notifican todas las vulnerabilidades
sin mover un dedo para remediarlas.
La remediación debe ser un compromiso.
Al tener la opción de priorizar las vulnerabilidades
de acuerdo con la exposición al riesgo que representan
y teniendo en cuenta tu tolerancia al riesgo actual,
puedes entonces determinar qué inversiones de tiempo,
esfuerzo y dinero hacer con tu equipo para la remediación.
Recuerda que **identificar y remediar los problemas
en fases tempranas del ciclo de vida de desarrollo
de *software* significa reducir enormemente los costos**.
Disfrutar de este beneficio depende en gran parte
de la integración de controles de seguridad automáticos
y del apoyo que recibas del proveedor guías
y alternativas de remediación de vulnerabilidades.

Deberías tener en cuenta que,
al menos durante un tiempo,
la inversión en ciberseguridad puede estar parcialmente justificada
por el número de vulnerabilidades de las que te informan
y la exposición al riesgo que suponen.
Sin embargo, después de algún tiempo de detecciones y,
por supuesto, de remediaciones,
así como de prevenciones por parte de tu equipo de desarrollo basadas
en la retroalimentación recibida,
los reportes pueden volverse cada vez menos numerosos,
aunque puede que nunca dejen de llegar.
Por lo tanto,
**el éxito de tu inversión** puede ser definido más adelante
por el hecho de obtener menos reportes
y poder ver **una disminución significativa
y constante en la exposición al riesgo de tu compañía**,
incluso bajo evaluaciones integrales y exhaustivas de vulnerabilidades,
lo cual, en otras palabras,
representaría una **alta madurez en tu postura de ciberseguridad**.

¿Ya conoces la prueba gratuita de 21 días de escaneo de vulnerabilidades
con la herramienta de pruebas para AppSec de Fluid Attacks?
[Te invitamos a probarla](https://app.fluidattacks.com/SignUp).
¿No quieres ser escéptico sobre el valor
de una inversión en pruebas de seguridad?
Te invitamos a acceder a nuestra solución todo en uno,
[Hacking Continuo](../../servicios/hacking-continuo/),
y disfrutar de todos sus beneficios favoreciendo la madurez
en ciberseguridad de tu compañía.
[Contáctanos](../../contactanos/).
