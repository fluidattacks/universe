---
slug: blog/elegir-gestion-de-vulnerabilidades/
title: ¿Es esa la opción más adecuada?
date: 2023-03-27
subtitle: Consejos para elegir una solución de gestión de vulnerabilidades
category: filosofía
tags: ciberseguridad, pruebas de seguridad, pentesting, hacking, empresa
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1679966015/blog/choose-vulnerability-management/cover_choose_vulnerability_management.webp
alt: Foto por Luis Villasmil en Unsplash
description: Si todavía te preguntas a qué deberías prestar más atención al elegir una solución de gestión de vulnerabilidades, te recomendamos que leas este artículo.
keywords: Gestion De Vulnerabilidades De Ciberseguridad, Manejo De Vulnerabilidades De Seguridad, Evaluacion De Vulnerabilidades De Ciberseguridad, Software De Evaluacion De Vulnerabilidades, Evaluacion De Riesgos Y Vulnerabilidades, Escaneo De Seguridad, Hacking Etico, Pentesting
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Escritor y editor
source: https://unsplash.com/photos/jPpHpgWNCKs
---

Sin duda,
estás siendo testigo de que los ambientes tecnológicos están en pleno auge.
Por si fuera poco, cada vez hay más *hackers* maliciosos al acecho,
esperando encontrar brechas en los sistemas de las empresas
u organizaciones para generar daños y obtener beneficios.
Por este motivo, las brechas,
fallas o vulnerabilidades de tu compañía deberían detectarse
y solucionarse lo antes posible.
Una solución para la [evaluación de vulnerabilidades](../evaluacion-de-vulnerabilidades/)
puede ayudarte a detectar estos problemas de seguridad.
Pero es cuando un proceso de evaluación de vulnerabilidades
forma parte de un programa o solución más amplio
denominado [gestión de vulnerabilidades](../que-es-gestion-de-vulnerabilidades/)
que se puede cumplir el propósito de solucionarlos.

En Fluid Attacks,
donde te ofrecemos nuestra [solución de gestión de vulnerabilidades](../../soluciones/gestion-vulnerabilidades/),
dentro de nuestro
[servicio integral Hacking Continuo](../../servicios/hacking-continuo/),
creemos que toda organización fielmente comprometida
con su ciberseguridad, su reputación y el bienestar de sus clientes
debería implementar una solución como esta para
erradicar sus vulnerabilidades o problemas de seguridad.
En este artículo,
te damos algunos consejos que puedes tener en cuenta a la hora
de elegir una solución de gestión de vulnerabilidades,
ya que existen muchas disponibles en el mercado:

## Descubrimiento e inventario de activos

Busca una solución que aplique rápidamente
una metodología de inventario que te permita tener un amplio conocimiento
de los activos del ecosistema digital de tu empresa
y la capacidad de monitorearlos.
Dispositivos, servidores, sistemas operativos, aplicaciones,
contenedores, etc., todos ellos podrían ser puntos de acceso inesperados
para los actores de amenazas dentro de una superficie de ataque.
No basta con saber qué activos están presentes en un momento dado.
El descubrimiento y el inventario de activos es algo
que una solución debería hacer continuamente.
La solución debería permitirte listar y gestionar todos los activos
o infraestructuras nuevos que lleguen al entorno.
De esta forma,
tú y tus equipos obtienen total visibilidad de dónde pueden existir
vulnerabilidades antes de empezar a evaluar.

## Velocidad combinada con precisión

La solución que elijas debería ofrecerte rapidez
y precisión en la detección de vulnerabilidades.
Abstente de asumir que la evaluación de vulnerabilidades
es algo que solo deberían hacer las herramientas automatizadas.
La evaluación de vulnerabilidades puede referirse tanto
al [escaneo de vulnerabilidades](../escaneo-de-vulnerabilidades/)
como a las [pruebas de penetración](../que-es-prueba-de-penetracion-manual/).
En otras palabras,
la evaluación de vulnerabilidades puede ser realizada
tanto por escáneres automatizados como por expertos en ciberseguridad.
(Como hacemos en Fluid Attacks con nuestro *software* de
escaneo de código abierto y nuestros *pentesters*).
Para un enfoque integral de tus sistemas informáticos,
la solución debería contar con ambos métodos de evaluación
— ¡no te quedes con una solución que únicamente incluya escáneres!
Ten en cuenta que los humanos, es decir,
los *pentesters* o analistas de vulnerabilidades,
son esenciales para mantener al mínimo las tasas de falsos positivos
y falsos negativos,
que siguen siendo elevadas para las herramientas automatizadas.
Además,
siempre convendrá comprobar que tanto los escáneres como los *pentesters*
de la solución cuentan con [reconocimientos](../escaneo-estatico-aprobado-por-casa/),
[logros](../../../blog/owasp-benchmark-fluid-attacks/)
y [certificaciones](../../../certifications/).

## Técnicas múltiples y de amplio espectro

La evaluación de vulnerabilidades de ciberseguridad
dentro de una solución adecuada de gestión de vulnerabilidades
debería aplicar varias técnicas
(p. ej., [SAST](../../producto/sast/),
[DAST](../../producto/dast/), [SCA](../../producto/sca/),
CSPM, [MPT](../../producto/ptaas/),
[RE](../../producto/re/)).
La herramienta o escáner debería disponer de una base de datos
de vulnerabilidades bien surtida.
Además, debería haber *pentesters* de apoyo y equipos de investigación
para detectar vulnerabilidades de día cero
y alimentar los escáneres con frecuencia.
Adicionalmente,
la intervención humana permitiría realizar
pruebas de explotación en ataques simulados, internos o externos,
para evaluar impactos potenciales que no suelen conseguir
las máquinas automatizadas.

## Un único tablero de gestión

Tu solución debería permitirte definir
y controlar diferentes alcances de evaluación en función
de las necesidades de tu empresa.
Tanto la evaluación (con los alcances, métodos de prueba y resultados)
como el resto de la gestión
(revisión, priorización, remediación y validación)
deberían gestionarse desde un único tablero de control
(p. ej., la [plataforma](https://www.youtube.com/watch?v=g8H_c0b7fwo)
de Fluid Attacks).
Dicha plataforma debería ser informativa
y fácil de usar tanto para el personal técnico como para el no técnico.
También debería permitir el análisis continuo de riesgos
y la supervisión de vulnerabilidades,
con la ayuda de múltiples funcionalidades,
detalles sobre los hallazgos y formas de presentar y filtrar la información.
Adicionalmente,
la plataforma debería tener bien definidos los privilegios de acceso
y de uso según los roles de las partes interesadas.

## Cumplimiento

Con la solución de gestión de vulnerabilidades que elijas,
deberías poder cumplir con estándares y pautas de seguridad internacionales
(p. ej., [PCI DSS](../../../compliance/pci/),
[HIPAA](../../../compliance/hipaa/), [GDPR](../../../compliance/gdpr/),
[OWASP](../../../compliance/owasp/), [NIST](../../../compliance/nist/))
relacionados, por ejemplo, con las configuraciones,
controles y pruebas de seguridad adecuados.
La solución debería tener como base un gran número de referencias al respecto.
Debería permitirte elegir los requisitos más adecuados
para tu sector e incluso establecer tus propias políticas.
Al igual que ocurre con las bases de datos de vulnerabilidades,
los estándares y pautas internacionales de seguridad deberían revisarse
y actualizarse constantemente a medida que se modifican o surgen nuevos.
Lo ideal sería que,
desde el tablero de gestión mencionado en el consejo anterior,
tu compañía pudiera hacer un seguimiento de este cumplimiento.

## Continuidad

Las pruebas de seguridad no son para hacerse mensualmente,
y mucho menos trimestralmente.
Con una solución apropiada,
deberías poder evaluar tus sistemas de forma continua y segura,
sin generar interrupciones en los servicios ni en las operaciones.
Los equipos de desarrollo de tu empresa no dejan de realizar modificaciones,
mejoras y actualizaciones de los productos.
Además,
nuevos sistemas y aplicaciones se integran en tus redes
a medida que pasa el tiempo y a veces de forma inesperada
y no autorizada por empleados e incluso actores de amenazas.
Debido a todos estos cambios surgen nuevas vulnerabilidades,
y otras nuevas vulnerabilidades en componentes
que pueden estarse usando en tu empresa son reportadas públicamente,
alterando así el panorama de amenazas.
De ahí la necesidad de realizar pruebas de seguridad continuas.
Estas garantizan informes actualizados que facilitan
los procesos de remediación y reducen los costos.
Recuerda que no se trata solo de listar vulnerabilidades,
sino también de abordarlas de inmediato.

## Priorización

Consigue una solución que te permita priorizar
las vulnerabilidades para su remediación
de acuerdo a su exposición al riesgo,
no solo a sus puntuaciones o etiquetas CVSS.
Estas métricas vienen cargadas de dificultades,
como las de segmentación y agregación
(véase por qué Fluid Attacks utiliza
la [métrica modificada CVSSF](../../../blog/cvssf-risk-exposure-metric/)).
Para priorizar correctamente los problemas de seguridad,
deberían entrar en juego los datos históricos,
la explotabilidad y los *exploits*
y amenazas actuales en el ambiente cibernético.
Además, debería haber una contextualización clara,
teniendo en cuenta los datos,
las operaciones y los recursos tecnológicos en riesgo y
las posibles repercusiones en la empresa en cuestión.

## Remediación y soporte

La solución que elijas debería proporcionar recomendaciones
y pautas de remediación de vulnerabilidades a tus equipos,
así como soporte constante para resolver dudas a través de diversos canales
de comunicación rápidos y eficaces.
La información de soporte debería entregarse en una terminología comprensible
para las personas encargadas de solucionar los problemas notificados
e ir acompañada de referencias e incluso estimaciones,
como el posible tiempo de remediación.
La solución también debería permitirte asignar
la remediación de cada vulnerabilidad a la persona correspondiente
dentro de tu organización desde la misma plataforma en la que examinas,
por ejemplo, gráficos y cifras relacionados con los hallazgos.
Además,
debería ofrecerte la posibilidad de aceptar temporal
e indefinidamente vulnerabilidades que,
según los criterios de tu empresa, no se consideren riesgosas.

## Validación de la remedición

Una solución adecuada de gestión
de vulnerabilidades debería permitirte validar que la remediación
que tu equipo da a una vulnerabilidad es realmente práctica o efectiva.
(Esto es algo que, por ejemplo, en Fluid Attacks,
permitimos a nuestros clientes con un suministro ilimitado
de reataques sobre vulnerabilidades que reportan como cerradas o remediadas).
Además, una solución de este tipo debería integrar en tus *pipelines*
de CI/CD un mecanismo de evaluación que señale la presencia
de vulnerabilidades no aceptadas e incluso interrumpa automáticamente su flujo
(es decir, rompa el *build*)
para evitar que dichos problemas de seguridad pasen a producción.

## Reportes y progreso

Busca una solución que te permita ver,
personalizar (según las necesidades de tu empresa)
y descargar fácilmente informes en varios formatos
para compartirlos con el personal, incluidos los equipos de desarrollo,
especialistas en seguridad y directivos.
Adicionalmente, una solución de este tipo debería permitirte evaluar
y realizar seguimiento del progreso de tu compañía,
incluyendo cómo se compara con otras empresas
en la mitigación de la exposición al riesgo,
los tiempos de remediación de vulnerabilidades
y otras métricas relevantes para tu ciberseguridad.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/gestion-vulnerabilidades/"
title="Empieza ya con la solución de Gestión de vulnerabilidades
de Fluid Attacks"
/>
</div>

En Fluid Attacks siempre tenemos en cuenta todos los puntos anteriores,
no solo para implementarlos en nuestro servicio
sino también para trabajar en mejorarlos,
pensando en el bienestar de nuestros clientes.
¿Quieres descubrir parte de nuestra solución de gestión de vulnerabilidades
(incluyendo nuestro escáner de código abierto y nuestra plataforma)
en una prueba gratuita de 21 días?
[Sigue este enlace](https://app.fluidattacks.com/SignUp).
¿Quieres ser uno de nuestros clientes?
[Simplemente contáctanos](../../contactanos/).
