---
slug: blog/mayores-violaciones-datos-sector-financiero/
title: Top 8 de brechas de datos del sector financiero
date: 2024-06-06
subtitle: Las violaciones de datos más graves cometidas en el sector financiero
category: ataques
tags: ciberseguridad, empresa, tendencia, exploit, software, ingeniería social
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1717634219/blog/top-financial-data-breaches/cover_data_breaches_finance.webp
alt: Foto por Robs en Unsplash
description: Conoce las duras lecciones que el sector con más violaciones de datos tiene para enseñar a otras compañías.
keywords: Violaciones De Datos En Sector Financiero, Violaciones De Datos Financieros, Mayores Violaciones De Datos, Clientes Afectados, Riesgos De Ciberseguridad, Hacking Etico, Pentesting
author: Wendy Rodriguez
writer: wrodriguez
name: Wendy Rodriguez
about1: Escritor y editor
source: https://unsplash.com/photos/a-group-of-people-standing-next-to-each-other-HOrhCnQsxnQ
---

Las violaciones de datos son incidentes de seguridad
en los que personas no autorizadas acceden a datos protegidos.
Pueden ocurrir por varias razones,
incluidos los ciberataques maliciosos,
procedimientos de seguridad inadecuados o fallas técnicas.
Al decidir su objetivo,
los atacantes maliciosos buscan el máximo impacto y el mayor beneficio.
En el sector financiero consiguen ambas cosas, y lo saben.
El año pasado
[el sector financiero fue el más vulnerado](https://www.marketsmedia.com/finance-most-breached-industry-in-2023/),
superando a los sectores de la salud y los servicios profesionales.

Por muy vergonzoso o negativo que sea para su marca,
es útil que sus colegas sepan
cómo se han visto afectadas estas empresas financieras
por las violaciones de datos
y exponer las lecciones que pueden enseñar a otras organizaciones,
independientemente del sector.
Por eso hemos elaborado esta lista,
las 8 mayores violaciones de datos en el sector financiero,
que incluye bancos,
*fintechs* y agencias de reportes credenciales y de diferentes países.
Están ordenadas por el número de datos comprometidos.
Hemos incluido la causa,
el impacto y las lecciones aprendidas porque
queremos que obtengas la mayor información posible de estas empresas,
para que sus errores pasados no sean tus errores futuros.

Dado que las violaciones de datos registradas por los servicios financieros
alcanzaron un máximo histórico en el año 2023,
nos gustaría resaltar uno de esos incidentes
antes de abordar el top 8 de todos los tiempos.
Este afectó a un impactante número de clientes
de bancos conocidos y consolidados.
Conozcamos más detalles.

## La mayor violación de datos de 2023

En 2023
[la mayor violación de datos](https://cybernews.com/news/ncb-management-services-data-breach/)
experimentada por una institución financiera
la sufrió la agencia de cobro de deudas NCB Management Services.
Sufrieron un ataque de violación de datos que,
al parecer, concluyó con el pago de un rescate.
Sin embargo,
alrededor de **1,1 millones de personas** vieron comprometida su información.
Clientes de Bank of America y Capital One,
entre otras compañías,
vieron su información comprometida, la cual incluía su dirección,
número de identificación personal,
número de cuenta y estado de la cuenta, durante el ataque a NCB.
La empresa estadounidense de cobro de deudas tuvo conocimiento
de la filtración tres días después de que se produjera.
Más tarde tuvieron que enviar un comunicado a las posibles víctimas,
informándoles de que atacantes habían penetrado en sus sistemas.
En el comunicado, NCB aseguraba que los atacantes
ya habían sido bloqueados y
que no había conocimiento de que la información a la que se había accedido
fuese distribuida o utilizada maliciosamente.

NCB omitió más detalles sobre la naturaleza del ataque sufrido.
Pero una [demanda colectiva](https://www.classaction.org/news/ncb-management-services-failed-to-protect-consumers-info-from-hackers-class-action-says)
se presentó poco después de que se anunciara el ataque.
En ella se especificaba que la red de NCB no estaba cifrada
y era accesible sin contraseña ni autenticación multifactor.
La demanda también especifica que
"la empresa podría haber evitado la brecha
aplicando medidas razonables de ciberseguridad
o si simplemente hubiese eliminado los datos".
Una fuente interna desconocida afirma que la empresa de cobros sabía
que tenía vulnerabilidades pero no hizo nada al respecto.
Esta demanda sigue abierta.

## Las mayores violaciones de datos en el sector financiero

### 8. Robinhood - Ingeniería social

- **¿Cuándo se produjo la violación?** Noviembre de 2021.
- **¿Qué causó la violación?** Mediante ingeniería social,
  actores maliciosos engañaron a un empleado de atención al cliente
  para que diera acceso a ciertos sistemas de atención al cliente.
- **¿Cuántos clientes se vieron afectados?** Alrededor de 7 millones
  de usuarios.
- **¿Qué impacto tuvo?** La violación que sufrió [esta aplicación de *trading*](https://newsroom.aboutrobinhood.com/robinhood-announces-data-security-incident-update/)
  expuso las direcciones de correo electrónico de
  5 millones de usuarios y los nombres completos de otros 2 millones.
  Para aproximadamente 310 usuarios,
  se filtraron datos adicionales como su fecha de nacimiento y código postal.
  Para aproximadamente 10 usuarios, se expuso información más detallada.
- **¿Qué se puede aprender?** Este ataque con esta táctica específica
  destaca la necesidad de un entrenamiento exhaustivo para empleados,
  y que aborde las estrategias cambiantes utilizadas por los ciberdelincuentes.
  Robinhood comunicó rápida y
  honestamente a sus usuarios la brecha de seguridad,
  informándoles de la cantidad de datos que fueron comprometidos
  y las medidas que se tomaron para mitigarlo.
  Es una lección importante a tener en cuenta:
  transparencia y una respuesta rápida.
  Esta app *fintech* también dio a los usuarios afectados consejos
  sobre cómo proteger sus cuentas y datos privados.

### 7. Korea Credit Bureau - Amenaza interna

- **¿Cuándo se produjo la violación?** Entre diciembre de 2012
  y diciembre de 2013, fue descubierta a principios de 2014.
- **¿Qué causó la violación?** Se trató de una amenaza interna.
  El culpable fue un empleado de la empresa Korea Credit Bureau (KCB).
  El empleado fue acusado, y posteriormente arrestado,
  por robar los datos de los clientes de
  tres compañías de tarjetas de crédito mientras trabajaba para ellas
  como consultor temporal.
  Este empleado supuestamente copió los datos de los clientes
  en un disco externo durante un largo periodo de tiempo.
  La información robada se vendió a empresas de mercadeo,
  y al descubrirse, los directivos de estas últimas fueron arrestados.
- **¿Cuántos clientes se vieron afectados?** Al menos 20 millones de usuarios
  de bancos y tarjetas de crédito, el 40% de la población surcoreana.
- **¿Qué impacto tuvo?** La violación que sufrió
  [esta empresa de calificación crediticia y gestión de riesgos](https://www.securityweek.com/20-million-people-fall-victim-south-korea-data-leak/)
  expuso los números de identidad de los clientes,
  nombres, números de teléfono, direcciones,
  y números de tarjetas de crédito y fechas de caducidad.
  El Tribunal Supremo de Corea ordenó a KCB
  pagar a las víctimas 62,300 millones de wons
  (unos 48 millones de dólares),
  así como una indemnización por daños y perjuicios diferidos.
  Tras una demanda en 2015,
  las víctimas fueron indemnizadas por las tres empresas de tarjetas de crédito
  cuando se determinó que los datos robados no podían recuperarse.
- **¿Qué se puede aprender?** El hecho de que los datos
  no estuvieran encriptados contribuyó a la facilidad con la que fueron robados,
  por lo que el cifrado de datos es clave para proteger los datos sensibles.
  La violación de KCB es un duro recordatorio
  de la importancia de los controles de seguridad internos.
  Las amenazas internas son extremadamente peligrosas,
  y una medida contundente como el principio del mínimo privilegio del
  [modelo de seguridad de *zero trust*](../../learn/seguridad-zero-trust/)
  podría haber evitado este tipo de situaciones.

### 6. Experian - Ingeniería social avanzada

- **¿Cuándo se produjo la violación?** Entre mayo y agosto de 2020.
- **¿Qué causó la violación?** Un actor malicioso,
  haciéndose pasar por representante de un
  cliente legítimo, engañó a un empleado de Experian
  para que entregara información sensible.
  Se trataba de una forma sofisticada de ingeniería social,
  ya que el actor malicioso proporcionó la información de verificación
  que Experian solicita a sus clientes.
- **¿Cuántos clientes se vieron afectados?** Alrededor de 24 millones
  de clientes y casi 800.000 empresas.
- **¿Qué impacto tuvo?** La violación, la cual fue específica a Sudáfrica,
  que [esta agencia de informes crediticios](https://www.experian.com/data-breach/knowledge-center/reports-guides/2020-2021-data-breach-response-guide)
  experimentó la exposición de datos como nombres,
  números de identidad, datos de contacto,
  direcciones físicas e información laboral
  (títulos, lugares de trabajo y fechas de inicio).
  Experian afirmó haber recuperado los datos robados,
  sin embargo, se descubrió un gran archivo de estos datos
  en el popular sitio web de transferencia de datos WeSendIt.
- **¿Qué se puede aprender?** La importancia de procedimientos minuciosos
  de verificación de clientes queda demostrada con este incidente.
  La necesidad de capacitar a los empleados sobre las amenazas cibernéticas
  y las técnicas de ingeniería social también se pone en evidencia
  en este incidente,
  ya que los empleados son un blanco principal de los delincuentes.

### 5. CardSystems Solutions, Inc. - Vulnerabilidad explotada

- **¿Cuándo se produjo la violación?** Junio de 2005.
- **¿Qué causó la violación?** Los *hackers* se infiltraron
  en la red de CardSystems,
  explotando una vulnerabilidad para acceder a los archivos de la empresa.
  Los atacantes fueron capaces de instalar *software* malicioso,
  el cual capturó información de tarjetas de crédito
  durante la ejecución de las transacciones.
  La empresa tenía como clientes al menos 100,000 empresas pequeñas
  y procesaba al menos 15,000 millones de dólares anuales
  en transacciones con tarjetas de crédito en el momento del incidente.
- **¿Cuántos clientes se vieron afectados?** Más de 40 millones de
  de usuarios de tarjetas de crédito.
- **¿Qué impacto tuvo?** La violación
  que sufrió [este procesador de pagos de terceros](https://www.nbcnews.com/id/wbna8260050)
  expuso nombres, números de cuentas bancarias, números de tarjetas de crédito,
  fechas de caducidad y códigos de seguridad de millones de usuarios.
  Se trataba principalmente de usuarios de MasterCard
  pero la brecha también incluyó a usuarios de Visa y American Express.
  Parte de las víctimas quedaron expuestas o sufrieron cargos fraudulentos
  debido a los números de tarjeta de crédito robados.
  CardSystems se enfrentó a demandas y multas, así como a pérdidas comerciales,
  puesto que MasterCard y Visa les retiraron como procesadores de pagos.
  En su momento, se calificó como el mayor fallo de seguridad en EE.UU.
  y fue un gran llamado de atención para la industria
  de las tarjetas de crédito.
- **¿Qué se puede aprender?** Después de una investigación,
  se informó que la empresa no cumplía
  con los estándares de seguridad de la industria.
  Si hubieran seguido las normas y requisitos,
  no se habrían visto comprometidos.
  Este incidente afectó significativamente al sector financiero,
  dando lugar a regulaciones más estrictas para la seguridad de los datos.
  Ya hablamos de la importancia de la protección de datos
  y la necesidad de cumplir la regulación en un [*post* anterior](../proteccion-datos-servicios-financieros/).

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/pruebas-seguridad/"
title="Inicia ahora las pruebas de seguridad de Fluid Attacks"
/>
</div>

### 4. JPMorgan Chase - Vulnerabilidad explotada

- **¿Cuándo se produjo la violación?** Descubierta en julio de 2014,
  comenzó un mes antes. FUe revelado en octubre de 2014.
- **¿Qué causó la violación?** Actores maliciosos explotaron vulnerabilidades
  en las aplicaciones web de JP Morgan Chase,
  en particular una vulnerabilidad de día cero,
  para acceder a servidores que contenían datos de clientes.
  La principal causa de la violación fue un servidor único
  que no estaba actualizado con autenticación multi-factor.
- **¿Cuántos clientes se vieron afectados?** Aproximadamente 83 millones
  de cuentas (76 millones de particulares + 7 millones de pequeñas empresas).
- **¿Qué impacto tuvo?** La violación que sufrió
  [este banco](https://archive.nytimes.com/dealbook.nytimes.com/2014/10/02/jpmorgan-discovers-further-cyber-security-issues/)
  expuso nombres, direcciones de correo electrónico,
  números de teléfono y direcciones postales.
  Según una investigación interna de la empresa,
  no se accedió a datos como números de la seguridad social
  y credenciales para sitios web,
  Sin embargo, la brecha expuso a los clientes
  a ataques de *phishing* y robo de identidad.
- **¿Qué se puede aprender?** Esta violación refuerza la necesidad
  de que las empresas cuenten con prácticas sólidas
  de [gestión de vulnerabilidades](../../soluciones/gestion-vulnerabilidades/)
  para identificar las debilidades de sus sistemas.
  Aunque JP Morgan gastaba al parecer 250 millones de dólares
  al año en seguridad de la información,
  no actualizaron uno de sus servidores con MFA,
  por lo que es absolutamente necesario
  un sistema de actualización exhaustivo y meticuloso.
  Destacamos la necesidad de soluciones que den prioridad a la seguridad
  en [este *post*](../ciberseguridad-servicios-financieros/).
  Hay que mencionar una buena práctica:
  Su red estaba segmentada,
  lo que restringió el acceso y ayudó a minimizar el impacto de la violación.

### 3. Capital One - Controles de acceso laxos

- **¿Cuándo se produjo la violación?** Marzo de 2019.
- **¿Qué causó la violación?** Un ex empleado de Amazon Web Services (AWS)
  obtuvo acceso a la infraestructura de almacenamiento en la nube de Capital One
  utilizando credenciales de acceso no autorizadas.
  El ex empleado entendía la plataforma de AWS,
  por lo que fue capaz de explotar un *firewall* mal configurado en esa red.
- **¿Cuántos clientes se vieron afectados?** Alrededor de 100 millones
  de personas en EE.UU. y unos 6 millones en Canadá.
- **¿Qué impacto tuvo?** La violación que sufrió
  [este banco](https://www.capitalone.com/digital/facts2019/)
  expuso números de seguridad social
  y números de cuentas bancarias de usuarios estadounidenses,
  así como fechas de nacimiento, direcciones, números de teléfono,
  saldos de créditos,
  transacciones y puntuaciones de crédito.
  El *hacker* fue condenado por fraude electrónico y acceso no autorizado.
  Capital One fue multada con 80 millones de dólares y acordó
  pagar 190 millones de dólares a los clientes afectados.
- **¿Qué se puede aprender?** Este ataque recuerda
  a las empresas la necesidad de tener controles de acceso más estrictos
  para evitar el acceso no autorizado a datos sensibles.
  La importancia de una configuración adecuada
  para la seguridad en la nube es absoluta,
  la cual puede reforzarse con evaluaciones continuas
  y siguiendo las recomendaciones de
  una herramienta [CSPM](../../producto/cspm/).
  Otro problema fue el *firewall* mal configurado del banco,
  que creó una vulnerabilidad la cual fue explotada por el *hacker*
  Las [pruebas de seguridad](../../soluciones/pruebas-seguridad/) continuas
  y el [escaneo de vulnerabilidades](../escaneo-de-vulnerabilidades/)
  son esenciales para identificar y abordar posibles vulnerabilidades
  en los sistemas de protección.

### 2. Heartland Payment Systems - Inyección SQL

- **¿Cuándo se produjo la violación?** A finales de 2008
  y descubierta/anunciada en enero de 2009.
- **¿Qué causó la violación?** Los *hackers* utilizaron
  una combinación de técnicas:
  una vulnerabilidad de inyección SQL explotada
  y *malware* inyectado a través de un formulario en el sitio web de Heartland.
  Una vez dentro,
  instalaron *malware* para interceptar los datos de las tarjetas
  de crédito en tránsito.
- **¿Cuántos clientes se vieron afectados?** Aproximadamente 130 millones
  de individuos.
- **¿Qué impacto tuvo?** La violación que sufrió
  [este proveedor de tecnología y procesador de pagos](https://abcnews.go.com/Business/PersonalFinance/story?id=6695611&page=1)
  expuso números de tarjetas de crédito, nombres de titulares de tarjetas,
  fechas de caducidad y códigos de seguridad.
- **¿Qué se puede aprender?** Este incidente ilustra la importancia
  de fuertes medidas de seguridad,
  como revisar y parchear vulnerabilidades continuamente,
  la implementación de medidas de seguridad internas robustas
  y el cifrado de datos personales.
  Heartland tomó varias medidas y arregló posibles puntos débiles
  en la seguridad que podrían haber permitido más ataques.
  También eliminaron el *malware*
  y las *backdoors* que los atacantes utilizaron.

### 1. Equifax - Ataque a la cadena de suministro

- **¿Cuándo se produjo la violación?** Anunciada en septiembre de 2017;
  el ataque se produjo en marzo de 2017 y no fue detectado hasta julio de 2017.
- **¿Qué causó la violación?** La empresa fue atacada a través
  de una vulnerabilidad explotada en una aplicación web utilizada
  para las reclamaciones de los consumidores.
  La vulnerabilidad ([CVE-2017-5638](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-5638))
  se encontraba en Apache Struts, un marco de código abierto
  que sirve para crear aplicaciones web escritas en Java.
  Equifax no parcheó esta vulnerabilidad a tiempo;
  el parche se había publicado seis meses antes del ataque.
  La aplicación expuesta permitía acceder a una base de datos que contenía
  nombres de usuario y contraseñas almacenadas en forma de texto sin cifrar,
  lo que permitió a los atacantes adentrarse más en el sistema.
- **¿Cuántos clientes se vieron afectados?** Alrededor de 147 millones
  de clientes.
- **¿Qué impacto tuvo?** La violación que sufrió
  [esta agencia de informes crediticios](https://www.forbes.com/sites/thomasbrewster/2017/09/14/equifax-hack-the-result-of-patched-vulnerability/?sh=7d1e319f5cda)
  expuso nombres, fechas de nacimiento, números de seguridad social,
  permisos de conducir y números de tarjetas de crédito.
  Debido a una segmentación inadecuada,
  los atacantes pudieron moverse lateralmente donde
  encontraron credenciales de empleados sin cifrar,
  lo que les permitió seguir accediendo a más sistemas.
  Y mientras tanto, nadie sospechaba nada.
  La Comisión Federal de Comercio multó a Equifax
  con casi 700 millones de dólares
  por no proteger los datos de sus consumidores y no adoptar medidas básicas
  que pudieron haber evitado la violación.
- **¿Qué se puede aprender?** La Comisión Federal de Comercio tenía razón,
  varias medidas básicas podrían haber evitado esta situación.
  Una de esas medidas era la actualización de sus sistemas
  y buscar parches en aplicaciones de terceros.
  La seguridad de la cadena de suministro
  se convierte en algo innegociable cuando
  una empresa utiliza componentes de terceros.
  Explicamos este tema en
  [este *post*](../../../blog/software-supply-chain-security/).
  Otra medida básica era el cifrado de datos,
  especialmente las credenciales para acceder a partes del sistema,
  así como los datos sensibles.
  Esta parte destaca la importancia de las prácticas de autenticación sólidas,
  las cuales pueden ayudar a prevenir el acceso no autorizado.
  La segmentación adecuada y lógica de la red también forma parte de las medidas
  que podrían haberse tomado para prevenir esta brecha.

Con la comprensión de estos ejemplos
las organizaciones podrían aplicar mejores medidas de seguridad
para mitigar el riesgo de violación de datos.
Entre ellas figuran
**utilizar contraseñas seguras,
parchear las vulnerabilidades del *software* con la mayor brevedad,
aplicar mejores controles de acceso,
capacitar a empleados y clientes en las buenas prácticas de ciberseguridad,
y disponer de un plan de respuesta ante cualquier incidente**.

## Fluid Attacks contra la violación de datos

Estas violaciones de datos, todas ellas perpetradas por actores maliciosos,
demuestran que hay varias formas de penetrar un sistema
y aprovecharse de las malas prácticas.
Cabe mencionar un aspecto importante,
las 5 mayores violaciones de esta lista
tuvieron que ver con vulnerabilidades de *software*.
Por ello, desde Fluid Attacks,
recomendamos
[pruebas de seguridad](../../soluciones/pruebas-seguridad/) continuas,
tanto con herramientas automatizadas
como con métodos manuales que impliquen
un [equipo de hacking ético](../../soluciones/hacking-etico/).

[Contáctanos](../../contactanos/) para que te contemos cómo
nuestra [solución de Hacking Continuo](../../servicios/hacking-continuo/)
puede ayudar a tu organización.
