---
slug: blog/revision-codigo-fuente/
title: Revisión de código seguro
date: 2022-09-02
subtitle: Definición, métodos y beneficios
category: filosofía
tags: ciberseguridad, pruebas de seguridad, software, empresa, código
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1662160860/blog/secure-code-review/cover_secure_code_review.webp
alt: Foto por Edi Libedinsky en Unsplash
description: Infórmate sobre la revisión de código seguro y los beneficios de su uso temprano y sistemático en el ciclo de vida de desarrollo de software.
keywords: Revision De Codigo Fuente, Revision De Codigo De Seguridad De Aplicacion, Revision De Codigo De Seguridad Se Debe Hacer Durante Todo El Sdlc, Revision De Codigo Sast, Revision De Codigo Como Servicio, Revision De Codigo Para Seguridad, Practicas De Codificacion Segura, Hacking Etico, Pentesting
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Escritor y editor
source: https://unsplash.com/photos/1bhp9zBPHVE
---

Si no se ha revisado la seguridad del código,
la probabilidad de que la aplicación tenga problemas
es prácticamente del 100%.
Este es un mensaje contundente que aparece en las primeras páginas de
la [guía de revisión de código de OWASP](https://owasp.org/www-pdf-archive/OWASP_Code_Review_Guide_v2.pdf).
Una organización que no pone el código que utiliza
y desarrolla bajo revisión es irresponsable con sus activos
y los de sus clientes o usuarios.
Los problemas de seguridad de sus productos
pueden ser explotados por los ciberdelincuentes,
provocando fugas de datos o interrupciones en las operaciones
y consecuentes multas y pérdida de clientes y reputación.
Para ayudar a evitar todo esto,
es prudente acompañar el desarrollo de _software_ desde el principio con
una [revisión del código fuente](../../soluciones/revision-codigo-fuente/).

## ¿Qué es la revisión de código seguro?

La revisión de código seguro
(SCR, por su nombre en inglés)
es el análisis del código fuente de una aplicación
para identificar fallas de seguridad o vulnerabilidades.
Estas aparecen en el ciclo de vida de desarrollo de _software_
(SDLC, por su nombre en inglés)
y deben cerrarse o corregirse para reforzar la seguridad del código.
La revisión del código fuente puede tener lugar en cualquier punto del SDLC,
pero dentro de [la cultura DevSecOps](../concepto-devsecops/)
es más valioso utilizarlo desde las primeras fases.
Se trata de un procedimiento que puede realizarse de forma manual o automática.

Ahora tu pregunta puede ser:
¿Cómo funciona el proceso de revisión de código?

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/revision-codigo-fuente/"
title="Empieza ya con la solución revisión de código seguro de Fluid Attacks"
/>
</div>

## El proceso de revisión de código seguro

La revisión manual de código se lleva a cabo con gran atención al detalle.
Uno o varios analistas de seguridad examinan cada línea de código,
entendiendo lo que están evaluando, teniendo en cuenta su uso y contexto,
las intenciones del desarrollador y la lógica empresarial.
Por otro lado, la revisión automatizada
del código fuente es un proceso en el que se examina
más código en menos tiempo pero en el que no se tienen en cuenta
los factores anteriores. Las herramientas trabajan con
un conjunto predefinido de reglas,
están restringidas a ciertos tipos de vulnerabilidades
y padecen, algunas más que otras, el defecto de reportar falsos positivos
(decir que algo es una vulnerabilidad cuando no lo es).
Entre los métodos más utilizados en la revisión de código
se encuentran las [pruebas de seguridad de aplicaciones estáticas](../../producto/sast/)
(SAST, por su nombre en inglés)
y el [análisis de composición de _software_](../../producto/sca/)
(SCA, por su nombre en inglés).
(En [este artículo anterior del blog](../diferencias-entre-sast-sca-dast/)
se explica en qué se diferencia uno del otro).

### ¿Cómo realizar una revisión de código seguro?

La mejor opción para lograr una sólida revisión del código
es tomar las revisiones manuales y automatizadas
y fusionarlas para aprovechar sus capacidades particulares.
Las herramientas automatizadas de revisión de código seguro,
con su evaluación rápida y "superficial" de la superficie de ataque,
facilitan que los analistas de seguridad se centren
en identificar vulnerabilidades más complejas y críticas para la empresa.
Los expertos, especialmente
los [_hackers_ éticos](../../soluciones/hacking-etico/),
quienes aplican buenas prácticas de revisión de código,
teniendo también en cuenta la perspectiva de los atacantes,
pueden evaluar el código para reconocer los problemas de seguridad
que más contribuyen a la exposición al riesgo del objetivo de evaluación.
(Por ejemplo, en nuestro último reporte [State of Attacks](https://fluidattacks.docsend.com/view/behmfvipxcha2t7v),
compartimos que el **67,4%** de la exposición total al riesgo
en los sistemas evaluados fue reportado mediante el método manual).
Esto hace posible que la remediación de vulnerabilidades,
una acción que siempre debería estar conectada a la revisión de código,
pueda seguir una priorización.
En definitiva, la idea es reducir al máximo el número
de vulnerabilidades que pasan a producción,
pero siempre reparando primero las más peligrosas.

## Herramientas de revisión de código seguro

Para la implementación automatizada de los métodos anteriormente mencionados
que se utilizan habitualmente en la revisión de código,
es decir, [SAST](../seguridad-app-sastisfactoria/)
y [SCA](../escaneos-sca/),
hay herramientas disponibles que identifican rápidamente
las vulnerabilidades de seguridad conocidas.

### Herramientas de pruebas de seguridad de aplicaciones estáticas (SAST)

Las herramientas SAST son programas que escanean automáticamente
el código fuente o el código objeto de las aplicaciones
—mientras no se están ejecutando—
para detectar vulnerabilidades que coincidan con las que tienen
almacenadas en sus bases de datos.

### Herramientas de análisis de composición de _software_ (SCA)

Las herramientas SCA son programas que escanean automáticamente
las aplicaciones para inventariar sus componentes de _software_
de terceros y sus dependencias,
e identificar en ellos vulnerabilidades que coincidan con las registradas
en las bases de datos.

## Revisión de código seguro vs. pruebas de seguridad de aplicaciones

Pruebas de seguridad de aplicaciones (AST, por su nombre en inglés)
ses un concepto más amplio que revisión de código seguro.
De hecho, este último forma parte del primero.
AST, aparte de SAST y SCA, incluye métodos de evaluación
como las [pruebas de seguridad de aplicaciones dinámicas](../../producto/dast/)
(DAST, por su nombre en inglés),
las [pruebas de penetración manual](../../producto/mpt/)
(MPT, por su nombre en inglés)
y la [ingeniería inversa](../../producto/re/)
(RE, por su nombre en inglés).
Mientras que SCR puede aplicarse
en cualquier fase del desarrollo de _software_, DAST y MPT,
por ejemplo, se emplean generalmente cuando la aplicación se puede
ejecutar para evaluar su comportamiento a través de vectores de ataque.

## ¿Por qué necesitas una revisión de código seguro?

Un equipo de desarrollo exitoso,
comprometido con la seguridad de sus productos,
siempre tiene como pilar la revisión de código.
Cualquier organización que desarrolle _software_
debería tenerla entre sus prácticas constantes,
desde las primeras etapas del SDLC,
prestando atención a los pequeños cambios
que los miembros de su equipo van realizando en el código.
La seguridad en general y las debilidades comunes en el _software_
y su explotación no suelen enseñarse
a los desarrolladores en sus lugares de estudio y trabajo.
E incluso los desarrolladores más experimentados,
debido a factores como el agotamiento o el descuido,
pueden cometer errores de codificación
y acabar generando vulnerabilidades como las enumeradas
en el [Top 10 de OWASP](https://help.fluidattacks.com/portal/en/kb/articles/criteria-compliance-owasprisks/)
y el [Top 25 de CWE](https://help.fluidattacks.com/portal/en/kb/articles/criteria-compliance-cwe25/).
Por razones como estas,
el código fuente debería mantenerse bajo revisión
de expertos en seguridad.

### Los mayores problemas de seguridad en la codificación

¿Por qué es importante la revisión de código seguro?
SCR identifica la ausencia de prácticas de codificación seguras,
la falta de controles de seguridad adecuados
y la infracción de estándares de cumplimiento
como [PCI DSS](https://help.fluidattacks.com/portal/en/kb/articles/criteria-compliance-pci)
e [HIPAA](https://help.fluidattacks.com/portal/en/kb/articles/criteria-compliance-hipaa/).
Los proveedores de revisión de código pueden encontrar, por ejemplo,
una validación inexistente o errónea de entradas o _inputs_
(verificar que estos cumplen con unas características concretas)
procedentes de distintas fuentes que interactúan con la aplicación
(p. ej., usuarios, archivos, fuentes de datos).
Pueden descubrir que un desarrollador cometió el error
de dejar información confidencial
(p. ej., _tokens_, credenciales) dentro del código,
habiendo olvidado eliminarla después de haberla puesto
ahí sin justificación razonable.
Pueden ver que la información que necesita ser almacenada
y transferida no pasa por los algoritmos de cifrado adecuados.
Del mismo modo,
pueden descubrir que los procesos de autenticación
de usuarios son bastante débiles, requiriendo,
por ejemplo, contraseñas cortas con poca variedad en sus caracteres,
y que los controles de autorización son deficientes
y acaban dando acceso innecesario a cualquier usuario sin solicitar permiso.

Un problema importante que se descubre a menudo en la revisión de código
(con ayuda, por ejemplo, del método SCA)
son las vulnerabilidades de los componentes de _software_
de terceros y de código abierto.
Hoy en día,
el desarrollo de aplicaciones depende en gran medida de dichos componentes,
los cuales se importan de diversas fuentes
y sirven de soporte para lo que se pretende construir,
que a menudo resulta tener poca originalidad.
La dependencia también existe entre algunos componentes y otros.
Así, al utilizar uno de ellos,
el desarrollador puede no ser consciente
de la relación de este con los demás.
Los ciberdelincuentes tienen entre sus objetivos deseados
estas dependencias y componentes para buscar vulnerabilidades que explotar.
Este es un problema tan frecuente que, de hecho, tal y como informamos
en el [State of Attacks del 2022](https://fluidattacks.docsend.com/view/behmfvipxcha2t7v),
el problema de seguridad más común entre los sistemas evaluados fue
"[Uso de _software_ con vulnerabilidades conocidas](https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-011/),"
y el requisito cuya violación supuso la mayor exposición total fue
"[Verificar componentes de terceros](https://help.fluidattacks.com/portal/en/kb/articles/criteria-requirements-262/)."

### Prácticas de codificación segura durante la revisión de código

Los expertos y las herramientas responsables
de una revisión de código seguro se encargarán de verificar
si los desarrolladores del _software_ en evaluación
han empleado o no prácticas de codificación segura.
Aunque tenemos dos artículos en el blog que tratan más a fondo
las prácticas de codificación segura
(estos son, "[Revisar y practicar la codificación segura](../../../blog/secure-coding-practices/)"
y "[¿Codificación segura en cinco pasos?](../../../blog/secure-coding-five-steps/)"),
los cuales te invitamos a leer,
aquí tienes algunas de esas prácticas que deberían ser siempre
un punto de referencia para el desarrollo y la revisión de código.

Específicamente,
asegúrate de que tu _software_ cumpla con objetivos como los siguientes:

- Validar las entradas o _inputs_ procedentes de fuentes no fiables
  y aceptar solo las que cumplan características específicas.
- Verificar la identidad de los usuarios o entidades
  que desean acceder a recursos privados y, en operaciones críticas,
  solicitar autenticación multifactor.
- Exigir a los usuarios la creación de contraseñas suficientemente complejas.
- Restringir el acceso a recursos de alto valor
  solo a unos pocos usuarios autorizados.
- Otorgar a los usuarios acceso por defecto únicamente a los recursos necesarios
  para realizar determinadas tareas.
- Establecer límites de tiempo de inactividad de sesión relativamente cortos.
- Utilizar algoritmos de cifrado reconocidos, comprobados
  y actualizados para la información sensible en tránsito y en reposo.
- No poseer datos sensibles como comentarios dentro de su código.
- No revelar información valiosa a los atacantes en mensajes de error
  resultantes de actividades inválidas.
- Todos los componentes elaborados por terceros
  deben estar actualizados en sus últimas versiones.

Para más información sobre prácticas de codificación segura,
también puedes consultar las recomendaciones de OWASP
para desarrolladores en su [Code Review Guide](https://owasp.org/www-pdf-archive/OWASP_Code_Review_Guide_v2.pdf).

## ¿Cuándo deberías implementar una revisión de código seguro?

La revisión de código forma parte de un enfoque preventivo,
el cual debería abordarse en primer lugar, en vez de un enfoque reactivo.
Aplicar este método desde que se escriben
las primeras líneas de código permite identificar
y remediar las vulnerabilidades antes de pasar a producción,
para no tener que estar parcheando la aplicación continuamente.
Mantenerse un paso adelante de los _hackers_ maliciosos
y bloquear en el código cualquier posible entrada para usos indebidos,
incluso travesuras simples,
es sin duda una estrategia muy eficaz para reducir la probabilidad
de catástrofes causadas por ciberataques.

## Otros beneficios de las revisiones de código fuente

La revisión de código seguro permite que el número de errores
o vulnerabilidades encontrados en las etapas finales del SDLC,
a través de procedimientos como MPT, sea menor.
Por lo tanto,
también se puede reducir el tiempo que los desarrolladores
tienen que dedicar a los procesos de remediación en estas etapas.
Solucionar un gran número de vulnerabilidades
poco antes de pasar a producción se convierte
en una piedra en el zapato para los desarrolladores.
Ten siempre en cuenta que es más fácil
y menos costoso hacer correcciones de código
en el entorno de desarrollo que en producción.
Con una revisión continua del código fuente,
estás más cerca de la causa del problema
y puedes solucionarlo inmediatamente,
evitando cualquier acumulación.

Gracias a una revisión temprana del código,
los desarrolladores pueden empezar a asumir el compromiso
no solo de subsanar los problemas de seguridad detectados en sus productos,
sino también de hacer que sus resultados sean mejores cada día.
Esto puede ser un proceso en cadena.
Ciertos grupos de desarrolladores,
con la ayuda de los equipos de seguridad y sus pruebas o revisiones,
pueden transmitir conocimientos,
inspirar a otros a mejorar sus prácticas y productividad
y realizar la transición hacia una mentalidad
en la que todos en la organización son responsables de la seguridad.
Esos errores de seguridad que tan a menudo dieron lugar
a vulnerabilidades pueden volverse menos frecuentes con el tiempo.

Las organizaciones que deciden implementar la revisión de código
en sus procesos de desarrollo de _software_
reconocen la responsabilidad de cumplir
con los estándares establecidos en sus industrias.
Buscan ofrecer productos y servicios que garanticen
la seguridad de sus operaciones, datos y otros recursos,
principalmente los de sus clientes o usuarios.
De esta forma, consiguen generar confianza
y reflejar compromiso y calidad.
Esto afecta su competitividad favorablemente
y les ayuda a mantener una reputación sólida.

## ¿Cómo elegir tu equipo de revisión de código seguro?

Aunque un equipo de desarrolladores
puede hacer sus propias revisiones de código,
por ejemplo cuando un desarrollador pide a un compañero
que revise su _build_ para evitar errores lógicos o de estilo,
se recomienda que, en temas de seguridad,
participen expertos en la materia.
La revisión por parte de un agente externo puede garantizar
que se informará todas las fallas manteniendo una perspectiva imparcial.

Los principales elementos de una revisión de código
dentro de un buen equipo de ciberseguridad deberían
ser un grupo de expertos en seguridad ampliamente certificados
y herramientas automatizadas que muestren
bajas tasas de falsos positivos.
Sus evaluaciones deberían poder realizarse en una amplia gama
de lenguajes de programación,
basarse en múltiples estándares internacionales de seguridad
e informar de los hallazgos en una única plataforma que priorice,
fomente y facilite la remediación.

## Solución de revisión de código seguro de Fluid Attacks

En Fluid Attacks,
ofrecemos nuestra [solución de revisión de código seguro](../../soluciones/revision-codigo-fuente/)
como una evaluación exhaustiva y precisa del código fuente de tu _software_,
combinando procedimientos manuales
(por parte de nuestros [_hackers_ éticos certificados](../../../certifications/))
y automáticos basados en métodos como [SAST](../../producto/sast/)
y [SCA](../../producto/sca/).
([En 2021](../../../blog/owasp-benchmark-fluid-attacks/),
nuestra herramienta SAST automatizada obtuvo una puntuación perfecta
en el OWASP Benchmark v.1.2. y, [en 2022](../escaneo-estatico-aprobado-por-casa/),
fue aprobada por la App Defense Alliance
para validar los requisitos de nivel 2 del marco de evaluación
de seguridad de aplicaciones en la nube de la Alliance.
También cabe destacar que se trata de una herramienta
de revisión de código abierto).
Con nosotros,
puedes aplicar la revisión de código desde
las primeras etapas de tu SDLC de forma continua.
Puedes resolver tus problemas de seguridad rápidamente
(priorizando aquellos que representan la mayor exposición al riesgo)
en favor de la productividad de tu equipo de desarrollo
y de la seguridad de tus productos.

Nuestro SCR soporta alrededor de **40** lenguajes de programación,
incluyendo C, C#, C++, HTML, Java, JavaScript, PHP, Python, Ruby y Swift.
Tenemos entre nuestros requisitos de revisión los que figuran
en más de **60** [estándares de seguridad](https://help.fluidattacks.com/portal/en/kb/criteria/compliance),
internacionales,
incluidos CERT, CVE, CWE, HIPAA, NIST, OWASP y PCI DSS.
Y nos ajustamos a los requisitos específicos de tu aplicación
y lógica de negocio, todos ellos en constante revisión y actualización.
Integramos nuestro [agente CI](https://help.fluidattacks.com/portal/en/kb/articles/view-details-of-the-security-of-your-builds)
en tus _pipelines_ CI/CD para romper el _build_
cuando hay incumplimientos de políticas y vulnerabilidades abiertas.
Y te informamos de todo en nuestra [plataforma](https://app.fluidattacks.com/),
donde puedes comprender y analizar a fondo tus problemas de seguridad,
así como recibir recomendaciones y gestionar los procesos de remediación.
Además,
tus desarrolladores pueden tener a la mano nuestra [extensión IDE](https://help.fluidattacks.com/portal/en/kb/integrate-with-fluid-attacks/use-ide-extensions/vs-code-extension)
para VS Code para un reconocimiento
más rápido de las líneas de código afectadas.
Todo esto forma parte de nuestro servicio
de [Hacking Continuo](../../servicios/hacking-continuo/)
que también integra métodos de pruebas de seguridad
como [DAST](../../producto/dast/),
[pruebas de penetración manual](../../soluciones/pruebas-penetracion-servicio/)
e [ingeniería inversa](../../producto/re/).

No dudes en [contactarnos](../../contactanos/)
si deseas más información sobre nuestra solución de revisión de código seguro
y otras soluciones de nuestro [Hacking Continuo](../../servicios/hacking-continuo/).
[Haz clic aquí](https://app.fluidattacks.com/SignUp)
para probar nuestro plan Essential de Hacking Continuo
de forma gratuita durante **21 días**.
