---
slug: blog/piensa-como-hacker/
title: ¡Piensa como un hacker!
date: 2021-09-10
subtitle: Y afronta con éxito a los atacantes maliciosos
category: filosofía
tags: hacking, ciberseguridad, exploit, red-team, empresa
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1631323179/blog/thinking-like-hacker/cover_hacker.webp
alt: Foto por Giorgio Trovato en Unsplash
description: Este artículo del blog se basa principalmente en un estudio sobre las actitudes y comportamientos de los hackers. Estas observaciones pueden ayudar un poco a prevenir los ataques de los actores amenazantes.
keywords: Hacker, Pensar, Acceso, Sistema, Vulnerabilidad, Ataque, Pentesting, Hacking Etico
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Escritor y editor
source: https://unsplash.com/photos/HAFZE_xZR4o
---

Hace ya una década, [el columnista Roger A. Grimes comentó que](https://www.csoonline.com/article/2622041/to-beat-hackers—​you-have-to-think-like-them.html)
los asesores profesionales le preguntaban
a menudo qué rasgo ayudaría más a destacar
a un profesional de seguridad informática.
Para lo cual su respuesta era siempre la misma:
pensar como un *hacker*. Sin embargo,
inmediatamente aclaró que no se refería a *hackers* maliciosos
(de sombrero negro) sino a personas capaces de idear
formas de penetrar en cualquier sistema informático
sin incurrir en actividades ilegales (*hackers* de sombrero blanco).
No obstante,
podríamos suponer que solo las motivaciones
y los objetivos finales pueden diferenciar a ambos grupos.
De acuerdo con Grimes,
mirando los sistemas a través de los ojos de un *hacker*,
puedes identificar mejor las debilidades y crear las defensas;
los mejores *antihackers* son los mismos *hackers*.
(De hecho, este es el núcleo de Fluid Attacks,
el empleo de *hackers* éticos en favor
de la ciberseguridad de las empresas clientes).

En relación a dichas observaciones, esta vez,
tomé como referencia principal para este artículo del
blog [la investigación realizada por Esteves, Ramalho y de Haro](https://sloanreview.mit.edu/article/to-improve-cybersecurity-think-like-a-hacker/),
publicada en 2017 en
[la MIT Sloan Management Review](https://sloanreview.mit.edu/).
Básicamente, los investigadores encuestaron a 23 *hackers* experimentados y,
a partir de los datos recogidos,
crearon un marco de trabajo para ayudar a las organizaciones a responder
a las amenazas de ciberataques.
Veamos qué podríamos extraer de su artículo y de otras fuentes.

Hoy en día, debemos saber que una buena lucha contra
los riesgos de ciberseguridad requiere que,
dentro de las empresas interesadas,
todos los implicados comprendan los rasgos,
las estrategias y la mentalidad de quienes pueden convertirse en intrusos.
Todos estos responsables deben mantener una postura abierta
y flexible para ver a los atacantes
y los problemas desde diversos ángulos.

Entre las primeras características de los *hackers*
(tanto de sombrero negro como de sombrero blanco)
mencionadas por los autores referenciados,
tenemos su alta capacidad intelectual,
sus conocimientos de apoyo en informática y su tendencia
a disfrutar de correr riesgos.
En lo que respecta específicamente a los *hackers* maliciosos,
estos a menudo se sienten atraídos por la idea de ganar miles
o millones de dólares con sus ciberataques.
Estos ataques pueden dirigirse fácilmente a personas
u organizaciones considerablemente alejadas de sus lugares de acción.
Hoy en día, a diferencia del pasado,
estos delincuentes trabajan en grupos,
lo que sin duda los hace más fuertes.
Cada individuo puede contribuir al equipo con sus conocimientos
y especialidades particulares.

## Mentalidad y acciones de los *hackers*

Tratar de pensar como el atacante es actuar de forma preventiva.
Es querer anticiparse a lo que el delincuente puede hacer
con tus sistemas y activos de información para reducir los riesgos.
Para este fin,
es bastante útil conocer las mentalidades o conjuntos
de actitudes que se atribuyen a los *hackers*.
(Pero, de nuevo, estas tendencias básicas, lejos de las malas intenciones,
también pueden atribuirse a los *hackers* éticos,
quienes mejor pueden entender a los actores amenazantes).
Precisamente,
Esteves y sus colegas nos ofrecen dos mentalidades asociadas
a los *hackers* en un ataque: **exploratoria** y **explotadora**.
Cuando un ataque está empezando,
los *hackers* suelen utilizar una mentalidad de exploración
que combina el pensamiento deliberado y el intuitivo
y se basa en la experimentación intensiva.
Una vez conseguido el acceso a un sistema,
los *hackers* recurren a una mentalidad de explotación
para alcanzar sus objetivos (p. ej., el robo de información).

Vinculados a estos dos conjuntos de actitudes anteriores,
los investigadores se remiten a cuatro pasos
que los *hackers* suelen seguir en sus ataques.
Dos pasos de exploración: (1) identificación de vulnerabilidades
y (2) escaneo y pruebas.
Dos pasos de explotación: (3) obtener acceso y (4) mantener el acceso.

En el primer paso (identificación de vulnerabilidades),
los *hackers* suelen demostrar paciencia y determinación,
además de astucia y curiosidad.
Una vez elegida, digamos, una empresa como objetivo,
examinan minuciosamente los sistemas en búsqueda de vulnerabilidades.
Recopilan tantos datos como les es posible
(técnica de ***footprinting***).
Tal como nuestro jefe de equipo ofensivo
[Andres Roldan](../../../blog/authors/andres-roldan) mencionó
una vez en una presentación, los *hackers* (incluidos los éticos) obtienen,
por ejemplo, detalles técnicos del objetivo,
limitaciones técnicas y enumeración de posibles controles y datos.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/hacking-etico/"
title="Empieza ya con la solución Hacking Ético de Fluid Attacks"
/>
</div>

Al detectar muchas de las fallas de seguridad disponibles
para explotación, el error humano a menudo también aparece
en la mira de los *hackers*.
El acceso a los sistemas puede facilitarse mediante una comunicación
engañosa con personas internas de la empresa,
quienes pueden entregar credenciales a los atacantes sin saberlo.
A partir de esto,
características como las habilidades sociales
y persuasivas también destacan en algunos *hackers*.

En el segundo paso (escaneo y pruebas),
los *hackers* se empeñan en progresar.
Su avance con accesos no autorizados puede verse facilitado
por las vulnerabilidades que detectan con herramientas
de escaneo en las aplicaciones de los sistemas.
Estas fallas, aunque sean pequeñas,
pueden contribuir a que se abra una brecha incluso mayor.

En el tercer paso (obtener acceso),
los *hackers* entran en el sistema tras haber definido
las posibles rutas de vulnerabilidad a seguir o explotar.
Como dijo Roldán en su presentación,
los *hackers* pueden utilizar *exploits* públicos o crear los propios.
Pueden abusar de las fallas de autorización,
descifrar credenciales y obtener acceso de tipo administrativo.
A partir de ahí, extraen toda la información posible,
abusan de privilegios y acceden a otros dominios con movimiento lateral.

Después, según los autores, en el último paso (mantener el acceso),
los *hackers* intentan conservar la posesión del sistema
y el acceso para futuros ataques mientras pasan desapercibidos.
En esta fase,
es habitual que los *hackers* borren rastros
y evidencias para evitar ser detectados.

## ¿Cómo nos puede ayudar todo esto?

Todo esto es solo un resumen.
Comprender a grandes rasgos cómo piensa y actúa un hacker,
descubrir sus patrones de comportamiento,
por ejemplo, [estudiando ataques que ya se han producido](https://www.darkreading.com/vulnerabilities-threats/how-to-think-like-a-hacker)
o viéndoles atacar (a *hackers* éticos, por supuesto),
puede ayudar a educar a tu empresa increíblemente
en materia de ciberseguridad.
Esto puede ayudar especialmente a tus ingenieros
y desarrolladores a generar planes,
prepararse para posibles eventos futuros y reducir riesgos.

A partir del trabajo de Esteves y sus colegas,
puedes seguir varias recomendaciones.
En primer lugar, intenta conseguir personas especializadas,
con suerte ya cualificadas como *hackers*,
para que se unan a tu plantilla o trabajen como externos que,
además de evaluar tus sistemas, ayuden a entrenar a otros
de tus empleados desde un punto de vista ofensivo.
Por ejemplo,
algunas organizaciones han invitado a *hackers* a infiltrarse
en su *software* y descubrir vulnerabilidades a cambio de recompensas
y reconocimiento ([*bug bounties*](https://www.hackerone.com/resources/hackerone/what-are-bug-bounties-how-do-they-work-with-examples)).
Además, [desde hace tiempo](https://pctechmag.com/2011/09/how-7-black-hat-hackers-landed-legit-jobs/),
algunos *hackers* de sombrero negro han empezado a cambiar de bando.
Han sido buscados y contratados para trabajar de forma ética,
incluso en las empresas a las que llegaron a dirigir ataques.

Busca formas de hacer *footprinting* en tu empresa con regularidad,
y revisa tus sistemas y sus puntos débiles con lupa.
Procura educar a tus empleados sobre las políticas de tratamiento
de datos y las técnicas que los *hackers* malintencionados
podrían utilizar para engañarlos.
Realiza pruebas de penetración en tus aplicaciones con la ayuda
de tus expertos y de empresas como Fluid Attacks para averiguar
hasta dónde podrían llegar los ciberdelincuentes en tus sistemas y por qué.
Vigila todas las rutas posibles para cerrarlas o bloquearlas cuanto antes.
Por último, permanece atento a cualquier evento sospechoso
y asegúrate de que tus sistemas de control y monitoreo estén actualizados.

Si quieres empezar a sumergirte en las ideas y formas de pensar
de los *hackers* o ampliar tu espectro en este sentido,
te invito a que eches un vistazo a nuestros artículos
([1.0](../../../blog/tribe-of-hackers-1/),
[2.0](../../../blog/tribe-of-hackers-2/), [3.0](../../../blog/tribe-of-hackers-3/),
[4.0](../../../blog/tribe-of-hackers-4/), [5.0](../../../blog/tribe-of-hackers-5/))
basados en el libro [*Tribe of Hackers Red Team*](https://www.amazon.com/gp/product/B07VWHCQMR/ref=dbs_a_def_rwt_bibl_vppi_i2)
de Carey y Jin (2019).

<quote-box>

Uno de los pasos más importantes para prevenir
una violación de la ciberseguridad es comprender a tu adversario;
las técnicas que utiliza, el *malware* con el que está armado,
sus objetivos y las vulnerabilidades que te ponen en mayor riesgo.
—[Threatpost](https://threatpost.com/webinars/how-to-think-like-a-threat-actor/?utm_source=TT&utm_medium=TT&utm_campaign=August_Uptycs_Webinar)

</quote-box>
