---
slug: blog/seguridad-punto-de-venta/
title: Seguridad para el punto de venta
date: 2024-10-29
subtitle: Protegiendo tus TPV de las ciberamenazas
category: filosofía
tags: ciberseguridad, empresa, riesgo, tendencia
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1730243221/blog/point-of-sale-security/cover_point_of_sale_security.webp
alt: Foto por Clay Banks en Unsplash
description: Los TPV forman parte integral del sector minorista. En este post explicamos los riesgos a los que se enfrentan estos sistemas y las mejores prácticas para protegerlos.
keywords: Sector Minorista, Sistema Tpv, Seguridad De Terminales De Punto De Venta, Punto De Venta Hacking, Tpv Malware, Buenas Practicas De Seguridad, Pci Dss, Hacking Etico, Pentesting
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Escritor y editor
source: https://unsplash.com/photos/man-paying-using-credit-card-oNm9NkTFLfA
---

​​Forever 21, TJX, Home Depot y Target fueron esos grandes minoristas que,
como informamos en [nuestro *post* anterior](../filtraciones-datos-sector-minorista/),
explícitamente sufrieron devastadoras filtraciones de datos
debido a medidas de seguridad inadecuadas en sus TPV.
Estos dispositivos o terminales de punto de venta
(TPV; PoS en inglés por *point of sale*),
que sustituyen cada vez más a las transacciones en efectivo,
son indispensables no solo para las tiendas minoristas,
sino para casi todas las empresas modernas de turismo, hostelería y comida,
entre otros sectores, diariamente.
Está comprobado que si las compañías no prestan suficiente atención
a la seguridad de estos sistemas,
lo más probable es que sufran pérdidas financieras y de datos,
repercusiones legales y un enorme daño a su reputación
a causa de los ciberataques.

En este blog *post*,
exploramos diversas amenazas a la seguridad
a las que se enfrentan los TPV,
repasamos algunos métodos típicos de *hacking* contra ellos
y ofrecemos medidas prácticas que las empresas pueden emplear
para proteger estos dispositivos y los datos confidenciales
y activos financieros de sus clientes.

## ¿Qué es un terminal de punto de venta (TPV)?

Un TPV es un dispositivo informático
que las compañías utilizan en sus establecimientos
y que permite a los clientes, normalmente en persona,
realizar transacciones de activos financieros
y efectuar compras de forma eficiente.
Suele constar de un monitor, un teclado, un lector de códigos de barras,
una impresora de recibos y un lector de tarjetas.
Los TPV pueden conectarse a redes privadas
para permitir las transacciones con tarjeta,
así como a la Internet.
Registran cada venta,
proporcionando datos en tiempo real para la contabilidad
y la gestión de inventarios.
También pueden ser útiles para revisar el comportamiento de los clientes
y gestionar sus datos y relaciones comerciales.

## Comprender la seguridad de los TPV

A medida que los sistemas de TPV se integran más en las operaciones diarias,
se convierten en objetivos cada vez más atractivos para los ciberdelincuentes.
La "seguridad de los TPV" se refiere a las medidas tomadas
para protegerlos de accesos no autorizados,
infecciones de *malware* y robos de datos y dinero.
Esto incluye asegurar los componentes de *hardware* y *software*,
implementar fuertes medidas de seguridad en la red
y formar a los empleados en las mejores prácticas de seguridad.

Nunca se insistirá lo suficiente en la importancia de la seguridad de los TPV.
Un dispositivo comprometido
puede conducir al robo de datos sensibles de los clientes,
como números de tarjetas de crédito, números de la seguridad social
y direcciones.
Esta información puede utilizarse para cometer fraudes,
causando importantes pérdidas económicas a empresas y clientes.
Las violaciones de datos también pueden dañar
la reputación de una organización
y erosionar la confianza de los clientes.
En el panorama competitivo actual,
las compañías que dan prioridad a la seguridad y la protección de datos
tienen más probabilidades de ganar y conservar la lealtad de sus clientes.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/pruebas-penetracion-servicio/"
title="Inicia ahora con las pruebas de penetración como servicio
de Fluid Attacks"
/>
</div>

## Cómo se hackean los TPV

Para que las empresas apliquen estrategias de seguridad adecuadas
y protejan los datos confidenciales de sus clientes,
es fundamental saber cómo los atacantes comprometen
a los terminales de punto de venta.

### Métodos de ataque

Los ciberdelincuentes emplean diversas técnicas
para obtener acceso no autorizado a los TPV:

- **Explotación de vulnerabilidades:**
  Los TPV suelen venir con sistemas operativos por defecto,
  como Windows o Unix,
  que pueden tener medidas de seguridad mínimas y,
  en consecuencia,
  vulnerabilidades inherentes que pueden ser explotadas
  por los *hackers* maliciosos.
  Además,
  los puntos débiles de seguridad pueden estar asociados a redes inseguras
  y a componentes o dependencias de terceros obsoletos
  o sin parches en el *software* del sistema de TPV.
  Estos componentes no siempre se someten a rigurosas pruebas de seguridad,
  lo que deja posibles lagunas que los atacantes pueden aprovechar.
  Los criminales suelen buscar direcciones IP no seguras
  o hackear conexiones Wi-Fi para acceder a los terminales de forma remota.
  Una vez dentro de la red,
  pueden aprovechar las vulnerabilidades del sistema operativo o del *software*
  para instalar *malware* y robar datos
  o llevar a cabo ataques aún más complejos,
  como los de interrupción de las operaciones.

- **Ataques de fuerza bruta:**
  Estos ataques implican el uso de herramientas automatizadas
  que prueban sistemáticamente diferentes combinaciones
  de nombres de usuario y contraseñas,
  esencialmente "adivinando" su modo de entrada.
  (Estas herramientas pueden probar miles, incluso millones,
  de combinaciones de credenciales por segundo).
  El peligro reside en el hecho
  de que muchos sistemas TPV vienen con contraseñas por defecto,
  fáciles de adivinar,
  o los empleados pueden establecer contraseñas débiles por comodidad.

- **Credenciales comprometidas:**
  Los ciberdelincuentes pueden aprovechar
  [credenciales previamente comprometidas](../../../blog/credential-stuffing/),
  incluidas las de terceros proveedores asociados a los TPV de la empresa.
  Los empleados que utilizan contraseñas fáciles de adivinar
  o repiten las mismas credenciales en varias cuentas
  crean vulnerabilidades sin darse cuenta.
  Las filtraciones de datos suelen ser la fuente de obtención
  de estas credenciales
  para que los atacantes accedan, operen sin ser detectados
  y aparezcan como usuarios autorizados en las redes de las víctimas.

- **Ingeniería social:**
  [La ingeniería social](../../../blog/social-engineering/)
  explota la psicología humana en lugar de las vulnerabilidades técnicas
  para comprometer los sistemas de punto de venta.
  Un buen ejemplo son los [ataques de *phishing*](../../../blog/phishing/),
  en los que los delincuentes elaboran mensajes de correo electrónico
  engañosos (aparentemente procedentes de fuentes de confianza)
  para inducir a los empleados a realizar acciones
  que afectan a la seguridad de su compañía.
  Estos mensajes pueden crear una sensación de urgencia
  o utilizar líneas de asunto atractivas
  para inducir al destinatario a hacer clic en un enlace malicioso
  o abrir un archivo adjunto infectado.

- **Amenazas internas:**
  Estas amenazas,
  que implican a empleados o personas con acceso autorizado,
  pueden poner en peligro los sistemas de TPV
  y provocar importantes filtraciones de datos.
  Un ejemplo consiste en sobornar a los empleados
  para que entreguen temporalmente los terminales de punto de venta
  a *hackers* malintencionados
  fuera del horario comercial.
  A continuación,
  estos atacantes explotan los sistemas en un breve plazo de tiempo
  y devuelven los terminales antes de que vuelva a abrir el negocio.
  Esto les permite iniciar el robo de datos
  u otras actividades maliciosas sin ser detectados,
  aparentando que el sistema comprometido funciona con normalidad.
  Las amenazas internas también pueden provenir de empleados descontentos
  que buscan causar daño o robar datos en beneficio propio.
  Pueden explotar su conocimiento de las vulnerabilidades del sistema
  o utilizar sus privilegios de acceso
  para instalar *malware* o manipular datos.

- **Instalación directa de malware:**
  En algunos casos,
  los atacantes pueden conectar físicamente dispositivos USB infectados
  a los TPV para instalar *malware*.
  Este método requiere acceso físico al sistema,
  pero puede ser muy eficaz para comprometer toda la red.

- **Skimmers de tarjetas de crédito:**
  Aunque no es un método de intrusión al *software* del TPV,
  los *skimmers* de tarjetas de crédito suponen una importante amenaza
  para la seguridad de estos sistemas.
  Estos dispositivos físicos se conectan al terminal de punto de venta
  para recopilar información de las tarjetas pasadas por el lector.
  Estos datos se utilizan después con fines fraudulentos.

Una vez que los atacantes consiguen acceder a un TPV,
suelen instalar *malware*.
Este *software* malicioso puede propagarse por la red,
robando datos de varios sistemas en diferentes puntos.
El *malware* captura información sensible,
como números de tarjetas de crédito,
y la empaqueta para transferirla a un sitio externo o servidor remoto
controlado por los atacantes.
Este proceso de exfiltración de datos
suele estar lo suficientemente bien diseñado
como para evitar su detección durante largos periodos.

### Malware de TPV comunes

Algunos tipos comunes de *malware* de TPV son los siguientes:

- **Raspadores de memoria (memory scrapers):**
  Extraen datos de la memoria de un terminal de punto de venta,
  a menudo de la memoria RAM,
  donde se almacena la información de las tarjetas de pago.
  Esto suele ocurrir justo antes del cifrado de dicha información
  cuando se pasan las tarjetas de pago por el TPV.

- **Registradores de teclas (keyloggers):**
  [Registran todas las pulsaciones](../../../blog/keylogging-keyloggers/)
  que se realizan en un TPV,
  incluidas las contraseñas y los números de tarjetas de crédito.

- **Sniffers (olfateadores) de red:**
  Monitorizan el tráfico de red y capturan los datos transmitidos
  entre el terminal de punto de venta y otros dispositivos.

- **Puerta trasera (backdoor):**
  Crean una puerta trasera que permite a los *hackers*
  tener un punto de entrada oculto durante un tiempo prolongado
  y acceder al sistema de forma remota
  para robar datos o instalar *malware* adicional.

He aquí algunos ejemplos conocidos de *malware* de TPV:

- **Dexter:**
  Detectado por primera vez en [diciembre de 2012](https://en.wikipedia.org/wiki/Dexter_(malware)),
  fue uno de los primeros ejemplos de *malware* sofisticado para TPV.
  Dexter estaba diseñado para robar información de la memoria RAM de los TPV,
  analizarla para extraer datos de tarjetas de crédito y débito
  y cargarlos en un servidor de mando y control en Seychelles.

- **vSkimmer:**
  Descubierto en 2013,
  vSkimmer fue considerado una [versión actualizada de Dexter](https://www.scworld.com/news/vskimmer-trojan-steals-card-data-on-point-of-sale-systems).
  Funciona escaneando la memoria de los sistemas TPV infectados
  en busca de información sensible de tarjetas
  para enviarla a un servidor externo.
  Lo que hace destacar a vSkimmer
  es su capacidad para almacenar los datos robados
  en una unidad USB específica
  si el sistema infectado no está *online*,
  lo que permite a los atacantes recuperarlos más tarde.

- **Backoff:**
  Backoff es una [conocida familia de *malware* de TPV](https://www.cisa.gov/news-events/alerts/2014/07/31/backoff-point-sale-malware)
  que surgió públicamente alrededor de 2013.
  Este *malware* destacaba por su capacidad
  para registrar pulsaciones de teclas,
  rastrear la memoria en busca de datos de tarjetas
  y comunicarse con servidores remotos
  para la filtración de datos y actualizaciones.
  También era capaz de auto-eliminarse
  para dificultar las investigaciones forenses.

- **PoSeidon:**
  Identificado en torno a 2015,
  PoSeidon es una familia de *malware* de TPV
  que combinaba funciones de *keylogging* con RAM *scraping*
  para robar datos de tarjetas de crédito.
  Buscaba en la memoria del dispositivo TPV secuencias de números
  que coincidieran con patrones de tarjetas de crédito
  y luego subía esos datos a un servidor de "exfiltración".
  [PoSeidon también tiene](https://www.bankinfosecurity.com/pos-malware-still-works-a-8044)
  capacidades de autoactualización
  y mecanismos de autoprotección
  para conservar la permanencia en la máquina infectada
  y protegerse de la ingeniería inversa.

- **UDPoS:**
  [A diferencia de otros *malware* de TPV](https://www.forcepoint.com/blog/x-labs/udpos-exfiltrating-credit-card-data-dns)
  que normalmente se basan en HTTP para la comunicación,
  UDPoS, descubierto en 2017, utiliza túneles DNS
  para filtrar datos robados de tarjetas de crédito.
  Esto hace que sea más difícil de detectar,
  ya que el tráfico DNS generalmente se considera normal.
  UDPoS también se disfraza como un paquete de servicios de LogMeIn
  para mezclarse con *software* legítimo
  y evadir la detección de las herramientas de seguridad.
  Una vez instalado,
  UDPoS "raspa" la memoria de los TPV infectados
  para capturar los datos de seguimiento de las tarjetas de crédito
  y transmite esta información robada a los atacantes
  a través de consultas DNS.

## Protección de los TPV: buenas prácticas

He aquí algunas medidas esenciales
que las empresas pueden adoptar
para proteger sus TPV de los ciberataques:

- Cambia las contraseñas predeterminadas
  y utiliza *passphrases* fuertes y únicas
  para todas las cuentas (modifícalas con frecuencia).

- Aplica la autenticación multifactor para mayor seguridad.

- Configura *firewalls* para restringir el acceso no autorizado a la red.

- Segmenta la red para aislar los TPV de otros dispositivos.

- Utiliza el cifrado de extremo a extremo
  para proteger los datos en tránsito y en reposo
  de los titulares de tarjetas.

- Pon *tokens* a los datos sensibles
  para evitar almacenarlos en texto plano.

- Actualiza periódicamente el *software* y el *firmware* de los TPV
  para parchear las vulnerabilidades.

- Evalúa continuamente la seguridad de tus sistemas
  con [herramientas automatizadas](../escaneo-de-vulnerabilidades/)
  y *hackers* éticos o [*pentesters*](../pentesting/)
  para detectar y remediar sus vulnerabilidades.

- Utiliza *software* antivirus y anti-malware de confianza
  con protección en tiempo real.

- Implementa "listas blancas" (*whitelisting*)
  para restringir la ejecución de aplicaciones no autorizadas
  (p. ej., navegadores web y correos electrónicos)
  en los dispositivos TPV.

- Emplea la firma de código para prevenir la manipulación del *software*.

- Protege físicamente los terminales de punto de venta
  para evitar manipulaciones no autorizadas y robos.

- Usa cámaras de vigilancia para monitorear la actividad en torno a los TPV.

- Instruye a los empleados en las mejores prácticas de seguridad,
  como la higiene de las contraseñas
  y el reconocimiento de los ataques de *phishing*.

- Implementa un control de acceso basado en roles
  para limitar el acceso de los empleados a datos y funciones sensibles.

- Asegúrate de que los proveedores externos que acceden a los TPV
  cuenten con las medidas de seguridad adecuadas.

- Cumple las normas y reglamentos del sector, como PCI DSS.

- Implementa un plan de copia de seguridad y recuperación de datos
  en caso de filtración de datos.

Los TPV desempeñan un papel vital en las empresas modernas,
pero también suponen un reto para la seguridad.
Mediante la implementación de las medidas de seguridad descritas
en este blog *post*,
las empresas pueden reducir significativamente el riesgo
de sufrir ciberataques exitosos
y proteger sus valiosos datos y activos financieros.

Recuerda que la ciberseguridad es una responsabilidad permanente.
Es esencial que te mantengas informado sobre las últimas ciberamenazas
y vulnerabilidades
y revises y actualices periódicamente tu postura de seguridad.
No esperes a que se produzca una filtración de datos.
Protege tu *software* de forma proactiva con Fluid Attacks.
Nuestro equipo de expertos y nuestra avanzada tecnología
trabajan juntos para descubrir vulnerabilidades
y ayudar a fortificar tus aplicaciones.
[Pongámonos en contacto](../../contactanos/).
