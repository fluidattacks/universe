---
slug: blog/seguridad-app-sastisfactoria/
title: Seguridad de apps sastisfactoria
date: 2019-09-29
category: desarrollo
subtitle: Introducción a SAST
tags: ciberseguridad, pruebas de seguridad
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1620331073/blog/sastisfying-app-security/cover_pbcyaf.webp
alt: Foto por Desola Lanre-Ologun en Unsplash
description: Este artículo es una introducción a SAST. Compartimos el concepto, cómo funciona SAST, sus tipos, historia y algunos de los beneficios de implementarlo en los proyectos.
keywords: SAST, SDLC, Codigo, Pruebas Automatizadas, Pruebas Manuales, Vulnerabilidades, Hacking Etico, Pentesting
author: Kevin Cardona
writer: kzccardona
name: Kevin Cardona
about1: Systems Engineering undergrad student
about2: Enjoy life
source: https://unsplash.com/photos/kwzWjTnDPLk
---

SAST ([pruebas de seguridad de aplicaciones estáticas](../../producto/sast/))
es un tipo de prueba caja blanca
en la que se utiliza un conjunto de tecnologías
para analizar el código fuente,
el código de *bytes* o los binarios de la aplicación
con el fin de identificar y revelar vulnerabilidades de seguridad conocidas
que puedan ser explotadas por usuarios malintencionados.

## Un poco de historia

En su artículo de 1976,
"Design and Code Inspections to Reduce Errors in Program Development",
Michael E. Fagan explicó cómo realizar una revisión de código y,
de este modo,
creó el primer [proceso de revisión de código](../secure-code-review/)
del mundo.
La [inspección de Fagan](https://es.wikipedia.org/wiki/Inspecci%C3%B3n_de_Fagan)
es un proceso de ejecución formal que implica varias fases
y participantes y detecta defectos en el desarrollo de *software*
mediante la validación de criterios de entrada y salida predefinidos.

<div class="imgblock">

![Fagan Flow via secjuice.com](https://res.cloudinary.com/fluid-attacks/image/upload/v1620331068/blog/sastisfying-app-security/fagan_jbxgtk.webp)

<div class="title">

Flujo de Fagan (imagen tomada de) [aquí](https://www.secjuice.com/sast-isnt-code-review-fight-me/)

</div>

</div>

En 1992, en su artículo “Experience with Fagan's Inspection Method”,
E.P. Doolan propuso utilizar un *software*
que mantuviera una base de datos de errores previamente detectados
y escaneara automáticamente el código en busca de ellos.
Esto fue lo que dio inicio al uso de herramientas automatizadas
de revisión de código.

## Ciclo de vida de desarrollo de *software* (SDLC)

El SDLC es una serie de etapas que deben seguirse
para el desarrollo de un producto de *software* específico.
Estas etapas garantizan que la calidad,
la funcionalidad y los objetivos de la aplicación cumplan
las expectativas del cliente y los estándares de desarrollo.

<div class="imgblock">

![Software development Life Cycle phases via Synotive.com](https://res.cloudinary.com/fluid-attacks/image/upload/v1620331069/blog/sastisfying-app-security/sdlc_tgxhhl.webp)

<div class="title">

Ciclo de vida de desarrollo de *software* (imagen tomada de)
[aquí](https://www.synotive.com/blog/wp-content/uploads/2017/02/software-development-life-cycle.jpg)

</div>

</div>

Desde las primeras fases del SDLC es importante utilizar metodologías
de prueba que identifiquen rápidamente las vulnerabilidades
de seguridad para remediarlas antes del lanzamiento de la aplicación.
En los siguientes sitios web se pueden encontrar
múltiples vulnerabilidades conocidas:

- [Proyecto OWASP Top Ten](https://www.owasp.org/index.php/Category:OWASP_Top_Ten_Project).

- [CWE/SANS TOP 25 Errores de software más peligrosos](https://www.sans.org/top25-software-errors/).

- [Enumeración de debilidades comunes CWE](https://cwe.mitre.org/data/index.html).

<div class="imgblock">

![OWASP Top Ten Project via owasp.org](https://res.cloudinary.com/fluid-attacks/image/upload/v1620331070/blog/sastisfying-app-security/owasp_nks30b.webp)

<div class="title">

Imagen 3. Proyecto OWASP Top Ten vía [owasp.org](https://www.owasp.org/images/5/5e/OWASP-Top-10-2017-es.pdf)

</div>

</div>

Aplicando SAST podemos detectar y evitar
la mayoría de las vulnerabilidades de seguridad enumeradas
en los sitios web anteriores.

## ¿Cómo funciona SAST?

SAST puede aplicarse manualmente
o mediante el uso de herramientas automatizadas.

**Las pruebas manuales** son realizadas por un equipo de evaluadores
encargados de revisar el código en búsqueda
de vulnerabilidades de seguridad conocidas.
Una vez encontradas las vulnerabilidades,
se reportan al equipo de desarrollo para su solución.
Las pruebas manuales incluyen varias etapas:

1. **Sincronización:** Esta etapa incluye recibir
   de los desarrolladores la aplicación y
   una explicación completa de lo que esta hace y cómo lo hace.

2. **Revisión:** En esta etapa, el equipo de pruebas toma
   el código fuente y analiza cada línea, método,
   clase y archivo en búsqueda de vulnerabilidades de seguridad.

3. **Reporte:** En esta etapa, se eliminan los falsos positivos
   y la información irrelevante, y se crean y entregan reportes
   de hallazgos a los líderes de proyecto responsables
   de comunicarse con los desarrolladores,
   quienes luego mitigan o remedian las vulnerabilidades.

<div class="imgblock">

![Reporte de hallazgos en pruebas manuales vía Mitre.org](https://res.cloudinary.com/fluid-attacks/image/upload/v1620331068/blog/sastisfying-app-security/report_vsuvtz.webp)

<div class="title">

Ejemplo de un reporte de hallazgo de una prueba manual (imagen tomada de)
[aquí](https://www.mitre.org/sites/default/files/publications/secure-code-review-report-sample.pdf)

</div>

</div>

Muchas [herramientas](https://www.owasp.org/index.php/Source_Code_Analysis_Tools)
nos permiten realizar análisis de código automatizados
y nos proporcionar reportes de las vulnerabilidades descubiertas
durante el escaneo.
Estas herramientas flexibles pueden integrarse
con diferentes entornos de desarrollo, incluidos Waterfall,
integración continua/despliegue continuo (CI/CD),
Agile/DevOps y repositorios,
e incluso con otras herramientas de pruebas.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/revision-codigo-fuente/"
title="Empieza ya con la solución revisión de código de fuente de Fluid Attacks"
/>
</div>

Las herramientas automatizadas utilizan funciones sofisticadas
como el análisis de flujo de datos, el análisis de flujo de control
y el reconocimiento de patrones
para identificar posibles vulnerabilidades de seguridad.
El resultado es que las vulnerabilidades conocidas se reportan rápidamente,
algo realmente útil en proyectos complejos o con demasiadas líneas de código.

<div class="imgblock">

![Reporte de hallazgos en pruebas automatizadas vía Oreilly.com](https://res.cloudinary.com/fluid-attacks/image/upload/v1620331068/blog/sastisfying-app-security/toolreport_jjexvn.webp)

<div class="title">

Ejemplo de un reporte de hallazgo de una prueba automatizada (imagen tomada de)
[aquí](https://www.oreilly.com/library/view/industrial-internet-application/9781788298599/521ecdf9-f298-4e26-9b68-5baf6602094d.xhtml)

</div>

</div>

Los reportes siempre deberían ser revisados por expertos,
ya que las herramientas automatizadas tienden a notificar
un gran número de falsos positivos que deben ser descartados
para conocer los riesgos reales de una aplicación.

Según
[Synopsys](https://www.synopsys.com/software-integrity/resources/knowledge-database/static-application-security-testing.html),
hay seis sencillos pasos necesarios para realizar SAST de forma eficiente
en empresas que tienen un gran número
de aplicaciones construidas en diferentes
lenguajes, marcos y plataformas.

</quote-box>

1. Elige una herramienta automatizada capaz de realizar
   revisiones de código fuente de aplicaciones escritas
   en los lenguajes de programación que utilices.

2. Gestiona los requisitos de licencia,
   establece controles de acceso y organiza la infraestructura necesaria
   para desplegar la herramienta.

3. Personaliza el alcance de la herramienta,
   añade o elimina requisitos de verificación
   según las necesidades de tu organización,
   integra la herramienta en tu entorno de desarrollo
   y vincula a una plataforma que te permita hacer
   un seguimiento de sus resultados e informes.

4. Prioriza tus aplicaciones en función de su valor
   y sus riesgos y realiza escaneos en ellas con la herramienta.
   Continúa con estas evaluaciones a lo largo de la evolución de tu software.

5. Analiza los resultados obtenidos por la herramienta
   y descarta los falsos positivos. Prioriza las vulnerabilidades
   en función del riesgo que representan y lleva a cabo su remediación.

6. Mantén a tus equipos capacitados en el uso adecuado
   de la herramienta dentro de tu SDLC.

## Beneficios

- SAST puede aplicarse en las primeras fases del SDLC,
  ya que busca vulnerabilidades en el código antes de que se compile.
  Siempre que haya remediación,
  esto puede ayudar a garantizar que muchas vulnerabilidades de seguridad
  no se acumulen en la aplicación justo antes de su lanzamiento.

- Remediar vulnerabilidades identificadas con SAST
  desde las fases tempranas del SDLC significa menores costos
  en términos de tiempo y dinero en comparación
  con la detección y remediación tardías.

- SAST es flexible y puede adaptarse a cualquier tipo de proyecto.

- SAST puede integrarse completamente con los ambientes CI/CD,
  Agile y DevOps ([DevSecOps](../../soluciones/devsecops/)).

## Conclusiones

- Es importante conocer las vulnerabilidades de seguridad
  a las que están expuestas nuestras aplicaciones.
  Para ello,
  debemos leer e informarnos continuamente a través de recursos
  como [OWASP](../../../compliance/owasp/)
  o [CWE](../../../compliance/cwe/).

- Siempre se deben realizar
  [pruebas de seguridad](../../soluciones/pruebas-seguridad/)
  a las aplicaciones para garantizar
  que son capaces de mantener la confidencialidad,
  integridad y disponibilidad de la información.

- Realiza revisiones continuas de las aplicaciones.
  Las pruebas de seguridad nunca deberían realizarse solo una vez.

- El uso de SAST ayuda a los programadores a aprender
  o reforzar los estándares y prácticas de codificación segura.
