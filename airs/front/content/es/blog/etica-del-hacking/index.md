---
slug: blog/etica-del-hacking/
title: Ética del hacking
date: 2023-05-03
subtitle: O lo que hace al hacker ético
category: filosofía
tags: ciberseguridad, hacking, red-team, empresa
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1683132263/blog/hacking-ethics/cover_ethics.webp
alt: Foto por David Ramírez en Unsplash
description: Si los hackers éticos pueden hacer todo lo que hacen los ciberdelincuentes, ¿qué les impide obrar mal? Veamos en qué se diferencian y cómo eso se manifiesta en los códigos de ética.
keywords: Etica Del Hacker, Etica Del Hacking, Definicion Hacking Etico, Hackeo Etico, Hacking De Ciberseguridad, Hacking Etico Y Ciberseguridad, Ciberseguridad Y Hacking Etico, Hacking Etico, Pentesting
author: Jason Chavarría
writer: jchavarria
name: Jason Chavarría
about1: Escritor y editor
source: https://unsplash.com/photos/0uL3-Tkofe4
---

En la introducción del libro [*Secrets of a super hacker*](https://archive.org/details/Secrets_of_a_Super_Hacker),
el escritor Gareth Branwyn habla de las diferentes imágenes
que los *hackers* han tenido a lo largo de los treinta
y tantos años anteriores a la publicación del libro.
Él menciona cómo en los años 60 y 70 los *hackers*
tenían el perfil de "científicos independientes".
Su ética se centraba en la creencia de que todo *hacker*
debía tener acceso a la información y las herramientas
que le ayudarían a mejorar la sociedad.
Este objetivo benévolo se refleja en la definición inicial de *hacking*,
utilizada por los estudiantes de ingeniería,
que consistía en [averiguar](https://staysafeonline.org/cybersecurity-for-business/history-ethical-hacking/)
la forma de optimizar la tecnología bajo estudio.

Los *hackers* bienintencionados no solo trabajaban
pensando en su derecho a la información,
sino también en el de la humanidad.
En el libro, cuyo autor es el *hacker* conocido como The Knightmare,
se mencionan ideales de derechos humanos relativos
al libre flujo de información.
Entre otros, que todo el mundo sepa de la información que existe,
que tenga libre acceso a ella y que sus ideas y preguntas sean escuchadas.
Además, que cada individuo pueda controlar cómo se utiliza
su propia información personal.
A continuación,
el autor define al *hacking* como la persecución de estos
y otros ideales mediante el uso de computadores.
Ahora nos es fácil identificar esa actitud
de los primeros *hackers* en los manifiestos
de [Cyberpunk](http://www.ecn.org/settorecyb/txt/cybermanifest.html)
y [Anonymous](https://www.dazeddigital.com/artsandculture/article/16308/1/we-are-anonymous-we-do-not-forgive-we-do-not-forget).

Branwyn explora los diferentes mitos que han alimentado
las fantasías de los *hackers* de ser nómadas
tecnológicos en un mundo despiadado,
por el estilo del *hacker* como vaquero, pirata o *cyborg*.
Supongo que a ti también te atrae la imagen del *hacker* héroe.
En una sociedad en la que inteligentes son objeto de burla y maltratados
por los que tienen fuerza física,
el "nerdo" informático encuentra en el ciberespacio un lugar
en el que se le permite ser el tipo rudo que derrota a estos
últimos para ayudar a la gente a recuperar su libertad.

Sin embargo,
las acciones de los "hacktivistas" y otros similares
despiertan sentimientos encontrados en la gente.
Sentimientos alimentados por los medios de comunicación
y quizá coincidentes con las propias inclinaciones
políticas de la gente.
Pero lo que puede causar menos división de opiniones
son los delitos cometidos por *hackers* maliciosos.
En los años 80 y 90 fue cuando comenzaron en serio los enjuiciamientos
y las oleadas de detenciones de expertos en informática
cuyas intenciones eran poco honorables.

En el libro,
se anticipaba que en el futuro el terrorismo informático
se presentaría de forma significativa. Y así ha sido.
Hoy en día ya conocemos muchos nombres de grupos dedicados
al *ransomware* y sabemos que en este mismo momento
se están produciendo ciberataques de muchos tipos en todo el mundo,
lo que representa un costo considerable para las víctimas.
Mientras tanto,
la ciberseguridad intenta contrarrestar continuamente la fuerza
de la ciberdelincuencia.

Para luchar contra los cibercriminales,
la mejor apuesta ha sido la de evaluar preventivamente
la seguridad de los sistemas a través de los ojos del atacante.
Por suerte para la ciberseguridad,
hoy en día se puede hackear legalmente.
Los lectores habituales de este blog recordarán
nuestro artículo ¡[Piensa como un *hacker*](../piensa-como-hacker/)!
En él,
urgimos a las organizaciones a entender
cómo trabajan los *hackers* maliciosos,
así como a contratar profesionales que intenten penetrar
en las defensas de las organizaciones
e informen de las debilidades detectadas.
La estrategia de contratar a *hackers* bienintencionados
para hacer algo bueno no es nada nuevo.
Y me llamó la atención, como puede que a ti también,
saber que al principio estos *hackers* eran a menudo ciberdelincuentes
que se habían reformado. Los *hackers* contratados formaban
"tiger teams" y ayudaban a gobiernos
y organismos a mejorar su ciberseguridad.
También desde el principio ha habido *hackers* que trabajan
como evaluadores de seguridad autoproclamados
y avisan a las empresas de problemas de seguridad en sus sistemas.

Con tanta información y satisfacción que se puede obtener hackeando sistemas,
es una gran hazaña de los *hackers* de sombrero blanco
no dejarse llevar por la curiosidad y atenerse a un código de ética.
Sin embargo,
cabe preguntarse si ese código debe figurar expresamente,
por ejemplo, en documentos oficiales.
En realidad,
como escribió el periodista Stephen Levy en un capítulo
de libro titulado "The hacker ethic",
ni los manifiestos ni los misioneros tuvieron que inculcar
principios a la comunidad *hacker* primitiva,
sino que el computador hizo la labor de conversión.
Es posible relacionar eso con lo que algunos autores argumentan,
a saber,
que [a medida que se desarrollan los conocimientos informáticos](https://journal.acs.org.au/index.php/ajis/article/view/204/178)
crece también el respeto por los computadores y la información,
y que la [falta de habilidad y respeto](http://dx.doi.org/10.2139/ssrn.1286030)
hacia la integridad de los sistemas es mal vista
por los *hackers* de sombrero blanco.

Pero un problema que puede justificar la formulación
de una ética de *hacking* es que tanto el trabajo
de los *hackers* maliciosos como el de
los [*hackers* éticos](../que-es-hacking-etico/)
exigen las mismas aptitudes.
[Hemos descrito en otro artículo](../piensa-como-hacker/)
los comportamientos que demuestran ambos grupos:
paciencia, determinación, astucia y curiosidad
durante los procesos de exploración y explotación.
Un estímulo reforzador de estos comportamientos puede ser
el placer que se manifiesta en el complejo sentimiento
de orgullo de sí mismo y de reconocimiento
(ambos evidentes en las narraciones de The Knightmare).
¿Dónde trazamos el límite?
Bueno, un desencadenante comúnmente mencionado del comportamiento
delictivo informático parece ser
la [codicia](https://cs.slu.edu/~chambers/spring13/443/assignments/Ethics-distributed.pdf).
Y esto coincide con la motivación principal de los ciberataques,
que suele ser un beneficio económico.
Aparte de eso,
considerar motivaciones como la insatisfacción política,
la asunción de riesgos, el ganarse una reputación,
la guerra, parecen devolvernos al punto de partida.
La gran diferencia se encuentra en los efectos de las prácticas de cada grupo.
Entonces aparece en escena la ética para regular
a los *hackers* en este sentido.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/hacking-etico/"
title="Empieza ya con la solución Hacking Ético de Fluid Attacks"
/>
</div>

Afortunadamente,
no nos faltan códigos de ética de donde escoger.
El más conveniente aquí es el de The Knightmare,
que considera los efectos de ejercer como *hacker*.
Se establecen en él los principios que pongo aquí
parafraseando al autor:

- Un *hacker* nunca debería dañar,
  alterar o perjudicar voluntariamente a alguna tecnología o persona.

- En caso de haber hecho daño,
  el *hacker* debería corregirlo y luego evitar volver a hacer el mismo daño.

- Un *hacker* no debería lucrarse injustamente de un *hackeo*
  y no debe permitir que otros lo hagan.

- Un *hacker* debería informar a los propietarios del sistema
  sobre las vulnerabilidades y debilidades de seguridad encontradas.

- Un *hacker* debería impartir conocimientos cuando se le pida
  y compartirlos cuando disponga de ellos.
  (El autor añade que esto no es necesario, pero es cortesía).

- Un *hacker* debería ser consciente de su posible vulnerabilidad
  en todos los ámbitos informáticos, incluso en el papel de *hacker*.
  El autor aconseja actuar con discreción.

- Un *hacker* debería perseverar, pero no ser estúpido
  ni correr riesgos por mera codicia.

Además, The Knightmare ofrece un par de consejos.
Uno es rodearse de personas que sigan el mismo código o uno similar.
Otro es mostrar honestidad y compasión en los actos propios,
lo que llevará a los demás a actuar de la misma manera
y ahorrará al *hacker* problemas que puedan surgir por falta de amabilidad.

Ha pasado algún tiempo desde que se
publicó [*Secrets of a super hacker*](https://archive.org/details/Secrets_of_a_Super_Hacker).
El contexto ha evolucionado y entre los cambios está la vinculación
(y certificación) de los *hackers* éticos.
Como ya dije, hay muchos más códigos de ética,
y puede que ofrezcan algunos elementos
que podrían añadirse a la lista anterior.
Por ejemplo, Electronic Commerce Council (EC-Council),
que expide las certificaciones de Certified Ethical Hacker (CEH),
ofrece [su propio código de ética](https://www.eccouncil.org/code-of-ethics/).
Entre los 18 ítems de su código,
esta institución pide a los *hackers* que respeten la propiedad intelectual,
eviten utilizar *software* o procesos ilegales,
obtengan el consentimiento previo de los clientes para recopilar
y manejar información durante el *hacking*,
comprueben que sus habilidades (las del *hacker*)
están a la altura de las tareas, lleven una buena gestión de los proyectos,
no se asocien con *hackers* de sombrero negro
y no sean condenados por ningún delito grave
o por violar la ley del país.
Además, algunas instituciones,
como [GIAC](https://www.giac.org/policies/ethics/),
que expide varias certificaciones de seguridad de la información,
afirman oficialmente que investigarán la violación
de su código de ética y someterán al transgresor a acciones disciplinarias.

Para concluir, aunque el *hacking* nació como una empresa benevolente
-y aunque pueda parecer que los códigos de ética solo recalcan
cómo ser una persona decente-
ahora forma parte de una trayectoria profesional legítima y,
como ocurre con las actividades de cualquier otra profesión
-la cual también podría cruzar la línea de la corrupción-,
ayuda mucho a los intereses de los *hackers*
y de sus clientes el intentar garantizar que se haga pensando
en el bien de los sistemas, de sus usuarios y de sus propietarios.

Los [*hackers* éticos](../../soluciones/hacking-etico/) o *pentesters*
[certificados](../../../certifications/)
y el escáner de vulnerabilidades de Fluid Attacks
buscan vulnerabilidades en tu sistema continuamente
y durante tu ciclo de vida de desarrollo de *software* (SDLC).
[Contáctanos](../../contactanos/) para preguntarnos sobre nuestro servicio.
