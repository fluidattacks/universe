---
slug: blog/la-mayor-de-todas-las-brechas/
title: ¿La mayor de todas las brechas?
date: 2024-01-26
subtitle: Más bien, un montón de brechas en un solo lugar
category: opiniones
tags: ciberseguridad, credenciales, riesgo, vulnerabilidad, tendencia
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1706319423/blog/the-mother-of-all-breaches/cover_the_mother_of_all_breaches.webp
alt: Foto por Ray Hennessy en Unsplash
description: Dijeron que habían descubierto la filtración de datos más grande de la historia. Pero todo fue una falsa alarma. Entendamos por qué.
keywords: La Mayor De Todas Las Brechas, La Madre De Todas Las Brechas De Datos, Moab, Diachenko, Cybernews, Fuga, Leak Lookup, Pentesting, Hacking Etico
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Escritor y editor
source: https://unsplash.com/photos/flock-of-white-and-black-birds-1cVQMljvsnw
---

Sí,
eso es lo que dicen algunos:
la "**mayor de todas las brechas**" (“mother of all breaches, MOAB”).
¿Qué significa eso? ¿Qué ha ocurrido?
El investigador de seguridad [Volodymyr "Bob" Diachenko](https://www.linkedin.com/feed/update/urn:li:activity:7155298570126921730/),
en colaboración con el equipo de [Cybernews](https://cybernews.com/security/billions-passwords-credentials-leaked-mother-of-all-breaches/),
al parecer,
descubrió recientemente una filtración masiva de datos
con más de **26.000 millones de registros**.
Esto es más de tres veces el número de seres humanos que hay hoy en la tierra.
Pero,
¿se ha denominado adecuadamente este hallazgo?

Empecemos por destacar lo que se ha descubierto
en esta gigantesca cantidad de datos.
Según los investigadores,
se trata principalmente de contraseñas
y datos de usuarios de aplicaciones como
LinkedIn, Twitter/X, Wattpad, Evite, Adobe y Weibo, entre otras.
Pero el primer puesto entre todas ellas es para Tencent QQ,
una aplicación china de mensajería instantánea,
que representa alrededor del 5,8% del total de la "MOAB".
Esta filtración de datos también contiene registros de agencias gubernamentales
de Estados Unidos, Alemania, Brasil, Turquía y otros países.

Lo que el equipo de investigación encontró concretamente
en una "instancia abierta" fue una base de datos organizada minuciosamente
con casi **4.000 carpetas** que ocupaban unos **12 terabytes**.
La cuestión es que cada carpeta contiene registros
de una filtración o brecha de datos distinta,
**muchas de las cuales ya habían sido reportadas anteriormente**.
Así que,
aunque fueron los investigadores quienes aparentemente
lo bautizaron como la "MOAB",
este hallazgo parece más bien una base de datos de múltiples brechas de datos.
El equipo incluso expresó que es muy probable que haya duplicados
en esa base de datos,
pero que de todos modos parece haber nuevos datos de usuarios incluidos.
No obstante,
en lugar de decir la "mayor de todas las brechas de datos",
creo que es más apropiado llamarla
"la mayor compilación de múltiples filtraciones",
tal y como,
curiosamente,
Cybernews se refirió a ella más tarde en su propia publicación.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/pruebas-penetracion-servicio/"
title="Inicia ahora con las pruebas de penetración como servicio
de Fluid Attacks"
/>
</div>

Bien,
pero ¿de dónde salieron todos esos datos?
Cuando leí por primera vez sobre este descubrimiento,
no había ninguna declaración de un grupo criminal asumiendo la responsabilidad.
Pero hoy, 26 de enero,
Malwarebytes dijo en un [*post* actualizado](https://www.malwarebytes.com/blog/news/2024/01/the-mother-of-all-breaches-26-billion-records-found-online)
que la fuente de la base de datos
era [Leak-Lookup](https://leak-lookup.com/).
Lo creas o no,
se trata de un buscador de filtraciones de datos
que te permite buscar entre miles de brechas de datos
para estar al tanto de las credenciales que pueden haber sido expuestas.
Y precisamente,
Leak-Lookup muestra en su página web un total
de unos 26.000 millones de registros,
correspondientes a 4.176 filtraciones.

Entonces,
¿la brecha se produjo en una empresa que ha recopilado
y recopila brechas de datos?
(Había olvidado por un momento la exposición al riesgo
que representan estos servicios de recopilación de datos).
Afirmativo.
De hecho, Leak-Lookup [publicó en su cuenta X](https://twitter.com/leaklookup/status/1749951429693919400)
el 23 de enero
(enlazando al *post* original de Cybernews del 22 de enero)
que todo ese asunto era producto de un error de configuración
del *firewall* de sus sistemas que ya habían solucionado.
Según Malwarebytes,
la compañía afectada dijo que el acceso inicial a su base de datos
se produjo en diciembre del año pasado.

Seamos sinceros.
Todo suena extraño.
No solo el hecho de que Cybernews hablara de ello
como la mayor de todas las brechas cuando sabían
que no lo era (¿estrategia de *clickbait*?),
sino también que no informaran inmediatamente que ya conocían
la fuente de todos esos datos.
(El 26 de enero,
[Cybernews](https://web.archive.org/web/20240126094954/https://cybernews.com/security/billions-passwords-credentials-leaked-mother-of-all-breaches/)
seguía sin informar de ello.
**Actualización**: llegaron a hacerlo el 29 de enero).
Además,
tanto el descubridor como la fuente del descubrimiento
son equipos que recopilan registros de brechas de datos en la Internet.
(Sí, Cybernews tiene su [verificador de fugas de datos](https://cybernews.com/personal-data-leak-check/).)
El *post* de Leak-Lookup incluso empezaba diciendo algo como:
No esperábamos esa publicidad,
de ninguna manera.
¿Qué significa eso?
Aquí parece haber gato encerrado.
Pero bueno,
prefiero no andar especulando,
al menos ya no más.

De todos modos,
sabiendo que hay muchos datos sensibles circulando en la Internet
—incluidos posiblemente los tuyos—
que los actores maliciosos pueden aprovechar para ataques como
el [*phishing*](../../../blog/phishing/) selectivo,
el [robo de credenciales](../../../blog/relleno-credenciales/),
y la usurpación de identidad,
te invitamos a tener en cuenta las siguientes recomendaciones,
para algunos quizá ya trilladas:

- Si quieres,
  puedes empezar visitando sitios como [Have I Been Pwned](https://haveibeenpwned.com/)
  para verificar si alguno de tus datos de acceso
  u otros detalles personales son públicos debido a las brechas.
  Sin embargo,
  es posible que esta base de datos aún no esté actualizada
  con lo que se supone que son las nuevas brechas dentro de la "MOAB".

- Independientemente de que hayas visto o no
  tus cuentas registradas dentro de las afectadas en sitios como ese,
  cambia tus contraseñas lo antes posible.
  Esto es algo que, de hecho, deberías hacer con frecuencia, digamos, cada mes.

- Si sigues utilizando contraseñas sencillas como "123456"
  o "contraseña1" para cualquier aplicación o servicio,
  cámbialas también.
  Pero no se trata solo de cambiarlas de "contraseña1" a "contraseña2"
  o algo similar.
  No nos cansaremos de repetirlo:
  crea contraseñas o frases de contraseña con más de 12 caracteres,
  utilizando al menos una letra mayúscula,
  una letra minúscula, un símbolo y un número.

- No utilices la misma contraseña en distintas aplicaciones.
  Si tu problema es que tienes muchas contraseñas que recordar,
  es mejor que utilices un administrador de contraseñas como 1Password.

- Activa la autenticación de dos factores o multifactor.
  Gracias a esto,
  en caso de que los actores maliciosos tengan una de tus credenciales,
  también deberían tener acceso a otras contraseñas
  o incluso a cualquiera de tus dispositivos personales
  para poder causar daños.
  Esta estrategia es como añadir otra capa
  de seguridad a tu uso diario de la TI.

¿Eres cliente de Fluid Attacks
y aún no has descargado nuestra extensión de VS Code?
[Te invitamos a hacerlo](https://help.fluidattacks.com/portal/en/kb/articles/install-the-vs-code-extension)
para que aproveches la remediación automática
de vulnerabilidades mediante IA generativa.
¿No conoces nuestras soluciones?
[Comienza una prueba gratuita de 21 días ahora mismo](https://app.fluidattacks.com/SignUp).
