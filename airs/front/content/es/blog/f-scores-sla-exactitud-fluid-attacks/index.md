---
slug: blog/f-scores-sla-exactitud-fluid-attacks/
title: Oferta pública de alta exactitud
date: 2025-02-25
subtitle: F-scores y SLA de exactitud en Fluid Attacks
category: política
tags: ciberseguridad, empresa, pruebas de seguridad, tendencia, cumplimiento
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1740524751/blog/f-scores-accuracy-sla-fluid-attacks/cover_f_scores_accuracy_sla_fluid_attacks.webp
alt: Foto por Steve Smith en Unsplash
description: Fluid Attacks en su solución ASPM ofrece a sus clientes estrictas medidas de exactitud basadas en F-scores. Descubre de qué se trata.
keywords: Sla De Exactitud, Acuerdo De Nivel De Servicio, F Scores, Precision, Recuperacion, Falsos Positivos, Falsos Negativos, Hacking Etico, Pentesting
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Escritor y editor
source: https://unsplash.com/photos/a-bald-eagle-flying-through-the-air-with-its-wings-spread-2S-XTkpXrdM
---

Todas las empresas de seguridad de aplicaciones (AppSec)
dicen que te ofrecen resultados de pruebas de seguridad
con tasas muy bajas de falsos positivos.
Algunas, si acaso, mencionan las tasas de falsos negativos.
Pero, **¿algún proveedor de pruebas de seguridad
te ofrece acuerdos de nivel de servicio (SLA) de exactitud?**

Muchos hemos cometido, o seguimos cometiendo, el error
de creer que unas tasas bajas de falsos positivos o de falsos negativos,
como medidas independientes,
son motivo suficiente para determinar
que una solución de pruebas de seguridad es "exacta"
y vale la pena seguir utilizándola
para evaluar nuestros productos de *software*.

Para entender por qué esto es un error,
repasemos qué son los falsos positivos y los falsos negativos
y veamos cómo sus tasas pueden ser engañosas
a través de un ejemplo ilustrativo
(en aras de la simplicidad, en esta ilustración manejamos números diminutos).

## Sobre tasas de falsos positivos y falsos negativos

Supongamos que tu compañía desarrolla
y pone a disposición de sus clientes una aplicación web que,
en un momento dado,
alguien te recuerda que, al menos para cumplir con estándares,
debes someter a pruebas de seguridad.
Por limitaciones de tiempo y costes,
decides utilizar una herramienta automatizada que llamaremos "A"
(un nombre ingenioso, ¡lo sé!).
Una vez que la **herramienta A** completa las pruebas,
informa de **12 vulnerabilidades de seguridad** en tu aplicación.

Poco después,
miembros de tu equipo de desarrollo,
encargados de remediar dichas vulnerabilidades,
comienzan a informarte que aquel reporte de "SQL injection"
por parte de la herramienta
parece erróneo,
que aquel supuesto "problema de cifrado de datos"
en realidad no parece un problema,
y así sucesivamente.

Más tarde,
un par de miembros de tu empresa con amplio conocimiento en ciberseguridad
te confirman estas sospechas:
la herramienta A les ha dado varios informes erróneos.
Incluso te preguntan:
¿qué tal si además no nos está reportando todo?
Es entonces cuando deciden que su aplicación
debe someterse a un examen más riguroso.
Buscando consejo dentro y fuera de la compañía,
uno de tus aliados acaba recomendándote que sometas la aplicación
a **pruebas exhaustivas que combinen herramientas automatizadas
y expertos en *pentesting*** (también conocidos como *pentesters*).

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/pruebas-penetracion-servicio/"
title="Inicia ahora con las pruebas de penetración como servicio
de Fluid Attacks"
/>
</div>

Una vez completadas estas nuevas pruebas,
resulta que en realidad tu aplicación,
la cual no había sido modificada luego del primer análisis,
tiene **18 vulnerabilidades**.
Pero ojo,
la herramienta A solo te había reportado **5** de ellas.
¿Cómo puede ser?
Mira la siguiente figura:

<div class="imgblock">

![Gráfica de medición](https://res.cloudinary.com/fluid-attacks/image/upload/v1740583348/blog/f-scores-accuracy-sla-fluid-attacks/grafica-medicion.webp)

</div>

Estos cinco aciertos de la herramienta A son lo que conocemos
como **verdaderos positivos** (VP).
Las otras 7 supuestas vulnerabilidades reportadas por esa herramienta
son mentiras o falsas alarmas,
lo que técnicamente se llama **falsos positivos** (FP).
Asimismo,
esas **13** vulnerabilidades que la herramienta no pudo detectar y reportar
son los **falsos negativos** (FN).
Los **verdaderos negativos** (VN),
por su parte,
serían esas porciones de código o elementos de la aplicación
que podrían ser vulnerables pero que,
en este caso **14**, no lo son.
(Los VN son valores que, en casos de aplicaciones comunes,
es decir, no diseñadas específicamente para evaluar comparativamente
soluciones de pruebas de seguridad,
podrían resultar "imposibles" de determinar,
sobre todo por las "incontables" formas en que ciertos fragmentos de código,
por ejemplo, podrían ser vulnerables.)

A partir de estos valores,
se pueden calcular las bien conocidas **tasa de falsos positivos** (FPR)
y **tasa de falsos negativos** (FNR),
las cuales siguen las siguientes fórmulas:

- FPR = FP/(FP+VN)

- FNR = FN/(FN+VP)

Así,
suponiendo inocuamente que el conocimiento obtenido sobre la aplicación web
de este ejemplo
con las pruebas de seguridad integrales fue "absoluto",
la **herramienta A** tuvo, en este caso, un **FPR** de 7/(7+14) = **0,33**
y un **FNR** de 13/(13+5) = **0,72**.

## ¿Por qué depender solo de estos valores puede ser visto como un error?

Digamos que,
sin conocer los resultados globales
de las pruebas de seguridad integrales anteriores,
hubieses puesto en evaluación tu aplicación web
por parte de la **herramienta B**.
En su reporte,
esta herramienta te informa de tan solo **3 vulnerabilidades**,
**dos** de las cuales resultan ser falsos positivos.
Asumiendo erróneamente que todo lo demás que podría ser vulnerable no lo es,
esta herramienta tendría un mejor **FPR** que la herramienta A:
2/(2+35) = **0,05**.

Ahora,
supón que,
en lugar de las previamente mencionadas,
utilizaste la **herramienta C**
—nuevamente sin conocer los resultados anteriores—
la cual te reporta **¡39 vulnerabilidades!**
Es como si considerara vulnerables todos los elementos
(todos los círculos de la figura) evaluados.
Por tanto,
su **FNR** es **cero**, ya que no ha pasado nada por alto.
Es más, esta tasa seguiría siendo la misma
incluso conociendo todos los verdaderos positivos: 0/(0+18) = **0**.

Ambas herramientas,
la B y la C,
te dejan en un aprieto.
La **herramienta B**, aún con su **FPR superreducido**,
representa un problema enorme para la seguridad de tu compañía,
generando una falsa sensación de seguridad,
al haber reportado sólo una de las 18 vulnerabilidades de tu aplicación,
es decir, habiendo tenido **17 falsos negativos**.
La **herramienta C**, con su **asombroso FNR nulo**,
representa un problema importante para el empleo de recursos
como el tiempo y el esfuerzo de tus equipos de desarrollo y seguridad,
ya que estos tienen que lidiar con **21 falsos positivos**
que descubrirán al intentar remediarlos.

En resumen,
depender de FPR y FNR como valores separados no es una opción viable,
recomendable, para definir la exactitud de,
y en consecuencia el beneficio en términos de certeza que puede otorgar,
una solución de pruebas de seguridad.
**Los valores de FP y FN deben siempre tenerse en cuenta juntos**
y en relación a un universo de vulnerabilidades
para este tipo de determinaciones.
Es entonces cuando aparecen los **F-scores**.

## ¿Qué son los F-scores?

En los sistemas de búsqueda, detección y reporte de información,
la precisión y la recuperación o exhaustividad son métricas de rendimiento.
La **precisión** es la fracción de casos relevantes
entre todo lo reportado por el sistema.
La **recuperación** es la fracción de casos relevantes reportados por el sistema
entre todo aquello relevante en el objetivo de evaluación.
Ambas métricas dependen de valores como VP, FP y FN
(podemos ver que, tal vez por esa complejidad que supone su definición,
tal como señalamos más arriba, los VN no se tienen en cuenta).
Estas son sus fórmulas:

<div class="imgblock">

![Ecuaciones de medición](https://res.cloudinary.com/fluid-attacks/image/upload/v1740526069/blog/f-scores-accuracy-sla-fluid-attacks/ecuaciones-medicion.webp)

</div>

Como podemos apreciar,
**la precisión resalta la importancia de los falsos positivos**,
y **la recuperación destaca la relevancia de los falsos negativos**,
ambos reportados por el sistema.
Estas dos métricas pueden incluirse en una sola ecuación
que permite una más amplia definición de desempeño del sistema
y que podemos entender como **exactitud**.
Hablamos de la **ecuación de F-scores**:

Fᵦ = (1 + β²) · {[precisión · recuperación] /
[(β² · precisión) + recuperación]}

En esta ecuación,
los valores habituales de beta (β) son 1, 0,5 y 2.
**F1** es la media armónica de ambas variables,
mientras que **F0,5** da más importancia a la **precisión**
que a la recuperación
(es decir, su valor se aproxima más al de la primera métrica),
y **F2** da más importancia a la **recuperación** que a la precisión.

- F0,5 = 1,25 · {[precisión · recuperación] /
  [(0,25 · precisión) + recuperación]}

- F2 = 5 · {[precisión · recuperación] / [(4 · precisión) + recuperación]}

## Retomando el ejemplo de las herramientas

Como mostramos anteriormente,
la herramienta B destacaba por su bajo FPR,
y la herramienta C por su nulo FNR.
La **herramienta B**, con tan pocos FP y VP,
había obtenido un **FPR de 0,05 o 5%**.
Por lo tanto,
teniendo en cuenta únicamente los informes positivos *conocidos*,
su precisión no es tan mala,
considerando que se trata de una herramienta automatizada:

Precisión: 1/(1+2) = 0,33 = 33%

No obstante, como vimos,
el mayor problema para ti y tu empresa con esta herramienta B
se basaba en sus falsos negativos,
es decir, todas aquellas vulnerabilidades no reportadas
que podían ser detectadas y tal vez explotadas por los ciberdelincuentes.
De ahí que consideremos
que **su capacidad de recuperación es bastante pobre**:

Recuperación: 1/18 = 0,06 = 6%

Acorde con lo previamente expuesto,
el **F0,5** para esta herramienta sería de **0,17** o **17,4%**,
valor más cercano al de precisión
porque sabemos que el F0,5 da más relevancia a esta métrica.
Asimismo,
el **F2** sería de **0,07** o **7,2%**,
valor más cercano al de recuperación
porque sabemos que el F2 da más relevancia a esta métrica.

Todas estas cifras,
sobre todo recuperación y F2,
serían indicios sustanciales para decir que la **herramienta B**,
por muy "buen" FPR que tenga,
**no sería una buena elección** para evaluar la seguridad de tu aplicación.
(Algo similar puedes descubrir si examinas los datos de la herramienta C.)

## Fluid Attacks te ofrece un SLA de exactitud

En Fluid Attacks,
hemos reconocido durante mucho tiempo el problema de centrarse
sólo en las tasas de falsos positivos (FPR)
o en las de falsos negativos (FNR).
Así que, aunque, como casi todas las compañías de AppSec,
hemos defendido que mantenemos unos FPR bajos y,
como pocas, unos FNR bajos,
sabíamos que,
en aras de optimizar la precisión de nuestras pruebas
en beneficio propio y de nuestros clientes,
**no podíamos limitarnos a esos datos**.
Así que recurrimos a las métricas de precisión y recuperación y,
a partir de ahí, a la puntuación F1.

Más allá de esto,
como algo aún más provechoso para todas las partes involucradas y,
por lo que hemos visto, que nadie más hace,
decidimos empezar a ofrecer a nuestros clientes una puntuación mínima de F1.
Este valor debía cumplirse mediante nuestras pruebas de seguridad integrales
al evaluar los productos de *software* de nuestros clientes
como garantía de rendimiento o **acuerdo de nivel de servicio** (SLA).
Así,
este asunto empezó a tener un toque legal.
Es más,
no ofrecimos una cifra insignificante.
Apuntamos a lo grande desde el principio:
**¡la puntuación F1 mínima era de 0,9 o 90%!**

Pero, si nadie más lo hace,
¿por qué ponernos la soga al cuello?
Bueno, más que nada, se trata de **un reto** y,
al mismo tiempo, **un compromiso por garantizar el rendimiento óptimo**
de nuestras herramientas automatizadas y *pentesters* y,
en consecuencia,
los informes más precisos y exhaustivos posibles para nuestros clientes
sobre el estado de seguridad de sus aplicaciones.

Incluso, recientemente,
decidimos dar un paso más osado.
Nos pusimos en los zapatos de los ejecutivos
y los equipos de seguridad y desarrollo de nuestros clientes
y elegimos dar más peso a los falsos negativos (FN)
y a los falsos positivos (FP).
A partir de ahí,
**definimos que debíamos pasar a F2 y F0,5** y no seguir con F1,
y, en ambos casos, ofrecer lo mismo que antes:
el 90% como mínimo.
Por lo tanto,
nuestro renovado **SLA de exactitud**,
el cual comenzó a aplicar desde enero de 2025,
dice que se alcanzan puntuaciones F2 y F0,5 de al menos el **90%**
en los informes de [exposición al riesgo](https://help.fluidattacks.com/portal/en/kb/articles/cvssf-metric)
y vulnerabilidades del *software* de un cliente,
respectivamente.

Aquí hay que subrayar un par de cosas.
En primer lugar,
hablamos de "F2 *y* F0,5",
una conjunción que implica que **para ambas puntuaciones**,
no solo para una de ellas,
**debe alcanzarse al menos el 90%**.
En segundo lugar,
para **F2 nos centramos en la exposición al riesgo**
y para el **F0,5 en las vulnerabilidades**.
¿Por qué?

En Fluid Attacks,
hablamos de "exposición al riesgo" en función de las **unidades CVSSF**.
Esta métrica modifica los valores de severidad CVSS mediante la fórmula
**CVSSF = 4^(CVSS-4)**,
principalmente para establecer diferencias de valor más evidentes
entre vulnerabilidades según su rango de severidad.
Así, por ejemplo,
si la diferencia entre una vulnerabilidad de puntuación CVSS de 8,0
y una de 10,0
es de dos unidades,
en CVSSF sería de 3.840 unidades.
Asimismo,
podría suponerse erróneamente
que una vulnerabilidad de puntuación CVSS de 10,0
tiene el mismo valor que 10 vulnerabilidades de severidad 1,0.
Sin embargo,
en términos de CVSSF la diferencia se hace notoria:
La primera tendría un valor de 4.096,
mientras que las otras diez acumularían un valor de apenas 0,2.
(Para más detalles,
te invitamos a leer nuestro post "[Lo que le falta a tu gestión de riesgos](../metrica-exposicion-al-riesgo-cvssf/)".)

De acuerdo con lo anterior,
será más valioso para un cliente conocer las métricas relativas
a los falsos negativos
en función de la exposición al riesgo
que en función del número de vulnerabilidades.
Y es que, por ejemplo, el hecho de que 4.096 unidades de exposición al riesgo
no se notificaran o aparecieran como FN
(producto de una única vulnerabilidad crítica)
llamará más la atención que saber que sólo había un (1) FN en los informes.
En la misma línea de razonamiento,
este FN representado con unidades CVSSF implicaría una mayor reducción
en el F2 que ofrecemos al cliente
frente a lo que ocurriría representándolo con la cantidad de vulnerabilidades.
Esta reducción más significativa sería para nosotros un llamado de atención
más relevante sobre la exactitud de nuestras pruebas.

En cuanto a los falsos positivos,
a los desarrolladores de los clientes les importará más
estar informados sobre la cantidad de vulnerabilidades.
Por ejemplo,
estarán más interesados en el hecho de que 20 vulnerabilidades
supuestamente de severidad media fueran en realidad FP
—principalmente por el tiempo y el esfuerzo invertidos en descubrirlas—
que en que una única vulnerabilidad con la mayor exposición al riesgo
fuera un FP.
De ahí que hayamos decidido seguir determinando el F0,5
a partir del número de vulnerabilidades reportadas.

En Fluid Attacks,
**calculamos las puntuaciones F2 y F0,5** cada trimestre
como valores acumulados para cada cliente,
específicamente para sus grupos en el plan Advanced.
Tenemos en cuenta todo su historial de vulnerabilidades,
incluso considerando los grupos de *software* en evaluación
que fueron eliminados.
Para conocer los demás criterios de este **SLA de exactitud**
en nuestras pruebas de seguridad integrales,
te invitamos a visitar nuestra [Knowledge Base](https://help.fluidattacks.com/portal/en/kb/articles/accuracy-sla#Criteria).

Esperamos sinceramente que nuestro compromiso
con tu seguridad y nuestro rendimiento
te proporcione tanta gratificación como a nosotros
y te inspire para seguir **mejorando y madurando
en tu postura de ciberseguridad**.
