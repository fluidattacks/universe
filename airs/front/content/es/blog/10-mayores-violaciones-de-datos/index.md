---
slug: blog/10-mayores-violaciones-de-datos/
title: Las 10 mayores violaciones de datos de la historia
date: 2024-04-11
subtitle: Los robos de datos que dejaron su huella para siempre
category: ataques
tags: ciberseguridad, empresa, tendencia, riesgo, software, exploit
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1712856640/blog/top-ten-data-breaches/top-ten-data-breaches.webp
alt: Foto por Sean Pollock en Unsplash
description: Una mirada retrospectiva a estos ataques malintencionados que nos hicieron agradecer que no estuvieran dirigidos a nuestras organizaciones.
keywords: Violacion De Datos, Ataques, Robo, Usuarios Afectados, Ciberseguridad, Actores Maliciosos, Compañia, Hacking Etico, Pentesting
author: Wendy Rodriguez
writer: wrodriguez
name: Wendy Rodriguez
about1: Escritor y editor
source: https://unsplash.com/photos/low-angle-photo-of-city-high-rise-buildings-during-daytime-PhYq704ffdA
---

¿Has tenido que enfrentarte a una violación de datos?
Si no es así, considérate afortunado.
[Las estadísticas muestran](https://www.itgovernance.co.uk/blog/list-of-data-breaches-and-cyber-attacks-in-2023)
que el número de violaciones de datos
en 2023 alcanzó la cifra récord de 2.814.
Las noticias sobre estos incidentes no cesan:
la semana pasada [se informó](https://www.forbesindia.com/article/news/hit-with-massive-data-breach-boat-loses-data-of-75-million-customers/92483/1)
que los datos de **7,5 millones** de clientes
de la empresa electrónica india boAt Lifestyle fueron robados
y expuestos en la [*dark web*](../../../blog/dark-web/).
Información personal identificable como nombres,
direcciones de correo electrónico
y números de teléfono están ahora a la venta en diversos foros
y podrían llegar a utilizarse con fines maliciosos,
como estafas telefónicas, correos *phishing* o chantaje.
Este tipo de ataques
puede acarrear graves problemas no solo para los clientes,
sino también para las empresas que los padecen.
Las consecuencias para las empresas pueden incluir pérdidas financieras,
daños a la reputación, pérdida de clientes,
implicaciones legales y paralización de la producción;
todas estas cosas pueden ocurrir simultáneamente.

Como se ha demostrado una y otra vez,
aprendemos de las adversidades.
Y estas organizaciones han tenido que aprender
algunas duras lecciones como resultado de actores maliciosos
que aprovecharon sus prácticas de ciberseguridad poco rigurosas.
En este *post*,
queremos revisar las diez mayores violaciones de datos de la historia,
ordenadas por el número de usuarios/cuentas afectadas.
Antes de seguir adelante, tengamos en cuenta dos cosas:
(1) con el fin de proteger su imagen, las entidades afectadas
por robo de datos no siempre proporcionan detalles
sobre cómo fueron atacados o qué fue atacado,
y (2) la diferencia entre la violación de datos
y la fuga de datos
(la primera supone un acceso intencionado y no autorizado a datos,
mientras que la segunda suele implicar la exposición accidental
de datos sensibles).

## 10 - LinkedIn

A mediados de 2012,
unos ciberdelincuentes se infiltraron en el sistema
de la [red social](https://www.cbsnews.com/news/linkedin-2012-data-breach-hack-much-worse-than-we-thought-passwords-emails/)
y robaron **117 millones** de cuentas de correo electrónico
y contraseñas de usuarios, tanto premium como gratuitos.
Al principio se creía que 6,5 millones de usuarios
se habían visto afectados,
ante lo cual LinkedIn hizo poco o nada para advertirles del incidente.
Pero entonces llegó 2016,
y las ventas de todos los datos robados fueron vistas en la *dark web*.
En ese momento, la empresa reconoció el ataque
y emitió un comunicado en el que aconsejaba
a sus usuarios cambiar sus contraseñas,
evitar la reutilización de estas
y aprovechar las funciones de seguridad avanzadas,
como la autenticación de dos factores.
Los usuarios premium estadounidenses de la plataforma
de empleo presentaron una demanda colectiva,
a la que la compañía accedió y compensó con un total
de 1,25 millones de dólares a las víctimas que pagaron
por una suscripción entre 2006 y 2012.

## 9 - Dubsmash

La [antiguamente popular app de video-mensajería](https://monitor.mozilla.org/breach-details/Dubsmash)
se vio afectada por un robo de datos en 2018.
La compañía reveló la situación en 2019 después
de ver cómo los atacantes vendían los datos robados
en la *dark web*.
Los actores maliciosos se infiltraron en el sistema de la app
y accedieron a los datos de los usuarios
que incluían los nombres de los titulares de las cuentas,
nombres de usuario, direcciones de correo electrónico,
ubicaciones geográficas y contraseñas cifradas,
lo que sigue siendo un riesgo de seguridad ya que
los atacantes con suficientes recursos
y tiempo podrían descifrar estas contraseñas.
La información de **162 millones** de usuarios
de Dubsmash se vio comprometida,
a lo que la empresa se limitó a responder con consejos
para actuar en caso de una filtración.

## 8 - Wattpad

Este [popular sitio web](https://www.cbc.ca/news/business/wattpad-data-breach-1.5657724)
para contar historias y publicarlas fue objeto
de un ataque informático en 2020.
La violación de datos provocó la exposición de datos
de cuentas de más de 270 millones de usuarios.
La base de datos, que incluía nombres de usuario,
contraseñas cifradas, ubicaciones geográficas y correos electrónicos,
se vendió en secreto en la *dark web* por 10 bitcoins
(casi 100.000 dólares de la época).
No se reveló mucho,
pero se sabe que afectó a todos los que se unieron
al sitio web antes de 2017.
Un comunicado de la [compañía decía](https://support.wattpad.com/hc/en-us/articles/360046141392-FAQs-on-the-Wattpad-Security-Incident-July-2020)
que "por un exceso de precaución" Wattpad había restablecido
las contraseñas de todos los usuarios.

## 7 - Marriott International

Esta importante [cadena hotelera](https://news.marriott.com/news/2018/11/30/marriott-announces-starwood-guest-reservation-database-security-incident)
se convirtió en el objetivo de un importante robo de datos
que se anunció en 2018 y que expuso la información personal
de millones de huéspedes.
Ese año,
se hizo público que unos actores maliciosos
habían obtenido acceso no autorizado a los sistemas
de Starwood Hotels en 2014,
el cual Marriott compró en 2016,
y que habían copiado los datos de los huéspedes
sin que la empresa se diera cuenta.
Aunque algunos duplicados pueden haber inflado la cifra,
podrían haberse visto comprometidos los datos
de hasta **327 millones** de huéspedes.
La información expuesta incluía diversos datos personales,
como nombres, direcciones postales, números de teléfono,
direcciones de correo electrónico, fechas de nacimiento y,
en algunos casos, números de pasaporte y datos de tarjetas bancarias.
El ataque significó un duro golpe para la reputación de Marriott
y un gran impacto financiero que se tradujo en repercusiones legales
por parte de los clientes,
una multa por la violación de los derechos de privacidad
de los ciudadanos británicos conforme
al [GDPR](../../../compliance/gdpr/) y
gastos para la recuperación del sistema.

## 6 - MySpace

Los mismos vendedores que ofrecieron los datos robados de LinkedIn
en 2016 también afirmaron tener credenciales
de un incidente no reportado.
En el 2013,
el una vez [popular sitio web](https://www.mcafee.com/blogs/internet-security/myspace-accounts-hacked/)
MySpace sufrió un ataque que comprometió los datos
de **360 millones** de usuarios.
Pero, o no lo sabían o no hicieron ninguna declaración
porque no fue hasta 2016 que la violación de datos
fue expuesta por actores maliciosos en busca de ganancias.
Para entonces, MySpace había sido comprada por Time Inc,
que informó a los usuarios de esta red social de la violación
y les explicó que sus credenciales podrían haber sido utilizadas
para acceder a otros sitios web.
Para MySpace,
que ya estaba luchando por competir con otras
plataformas sociales más nuevas,
la revelación del ataque fue un duro golpe a su reputación y credibilidad.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/pruebas-penetracion-servicio/"
title="Inicia ahora con las pruebas de penetración como servicio
de Fluid Attacks"
/>
</div>

## 5 - FriendFinder Networks

Este robo de datos de 2016 afectó a **412 millones** de cuentas
de usuarios en 6 sitios web diferentes pertenecientes
a esta [red social para citas y entretenimiento para adultos](https://wolfesystems.com.au/insights-from-the-2016-adult-friend-finder-breach/).
FriendFinder Networks tenía prácticas de seguridad inadecuadas,
como almacenar contraseñas como texto sin cifrar,
lo que contribuyó al alcance de la violación.
Las 6 bases de datos robadas incluían información
como nombres de clientes,
direcciones de correo electrónico y contraseñas.
Fue una pesadilla para los usuarios,
que se vieron expuestos a robos de identidad,
intentos de extorsión y ataques de *phishing*.
A la empresa tampoco le fue bien,
ya que perdió un número crítico de clientes
junto con su reputación, por no mencionar las investigaciones
y multas de los organismos de protección de datos
con las que tuvo que lidiar.

## 4 - Yahoo (2014)

El robo de datos de 2014 de
este [proveedor de servicios web](https://www.cnet.com/news/privacy/yahoo-500-million-accounts-hacked-data-breach/#google_vignette)
se hizo público en 2016 y afectó a **500 millones** de cuentas.
La información robada incluía una cantidad significativa
de datos de usuario como nombres,
direcciones de correo electrónico, números de teléfono,
fechas de nacimiento, contraseñas cifradas y preguntas
y respuestas de seguridad cifradas y sin cifrar.
Aunque las contraseñas estaban cifradas,
los *hackers* lograron descifrar los *hash* con el tiempo,
y las preguntas de seguridad sin cifrar se utilizaron
para acceder a determinadas cuentas.
Fue muy preocupante que Yahoo esperara más
de dos años para revelar públicamente el ataque,
dando ese tiempo a los atacantes para
que pudieran explotar la información violada.
En ese momento, Verizon Inc. estaba en medio de negociaciones
para comprar la actividad principal de Internet de Yahoo.
Debido a este ataque y a las medidas legales tomadas
por los usuarios y las autoridades,
Verizon compró a Yahoo por una cantidad significativamente inferior
a la prevista en un principio.
No será la última vez que oigamos hablar de esta compañía
a medida que avancemos en esta lista.

## 3 - Aadhaar

La [base de datos de identificación del gobierno indio](https://economictimes.indiatimes.com/tech/technology/aadhar-data-leak-personal-data-of-81-5-crore-indians-on-sale-on-dark-web-report/articleshow/104856898.cms)
sufrió un ataque en 2018 por parte de actores maliciosos
que explotaron vulnerabilidades en el mecanismo de cifrado
y aprovecharon protocolos de seguridad obsoletos
para acceder a datos confidenciales.
El alcance del ataque fue gigantesco,
ya que se robaron nombres completos, direcciones, datos biométricos
y números Aadhaar de **815 millones** de ciudadanos,
los cuales posteriormente se pusieron a la venta en la *dark web*.
Los afectados fueron expuestos al fraude financiero,
a la violación de su intimidad
y a la pérdida de confianza en las iniciativas gubernamentales
y los sistemas digitales.
Como buen ejemplo de respuesta positiva
por parte de la entidad comprometida,
el gobierno indio aprendió la lección
e implementó las últimas tecnologías de cifrado, controles
de acceso más estrictos y protocolos de autenticación
avanzados, fortaleciendo así la infraestructura
de ciberseguridad de la nación.

## 2 - Indonesia SIM card

En 2022,
un robo masivo de datos afectó a los [registros de tarjetas SIM](https://restofworld.org/2022/indonesia-hacked-sim-bjorka/)
de **1.300 millones** de usuarios de Indonesia.
Un *hacker* llamado "Bjorka" apareció en un popular foro
de la *dark web* vendiendo estos perfiles de registro
de tarjetas SIM que revelaban números de identidad nacionales,
números de teléfono y nombres de proveedores de telecomunicaciones,
entre otra información.
Es importante señalar que la población de Indonesia
es inferior al número de tarjetas registradas
(275,5 millones en 2022),
lo que sugiere que los datos podrían incluir duplicados
o registros de personas con varias tarjetas SIM.
En un principio,
el gobierno indonesio restó importancia a la situación,
negando el alcance del ataque.
Por desgracia,
los indonesios están acostumbrados a que su información quede expuesta,
ya que lo hacen con tanta frecuencia que incluso llaman
jocosamente a Indonesia un "país de código abierto".
El atacante publicó un comunicado
en el que afirmaba que solo había realizado el ataque
para mostrar la ["terrible política de protección de datos"](https://thediplomat.com/2022/09/bjorka-the-online-hacker-trying-to-take-down-the-indonesian-government/)
del país, especialmente si lo gestiona el gobierno.
Este incidente, entre otros muchos de ciberseguridad,
ha provocado reclamaciones para que se regule
y aplique de forma más estricta la protección
de datos en el país asiático.

## 1 - Yahoo (2013)

Esta violación de datos fue etiquetada por
el [New York Times](https://www.nytimes.com/2017/10/03/technology/yahoo-hack-3-billion-users.html)
como el mayor robo de la red informática
conocido de una empresa en la historia.
Después de que la compañía fuera comprada por Verizon,
salió a la luz que Yahoo había sido objeto de un ataque en 2013
y que los datos de **3.000 millones** de usuarios
se habían visto comprometidos.
Esto no debe confundirse con el incidente de 2014
que se reconoció en 2016.
La compañía confirmó este ataque en 2017,
después de que se obtuvieran nuevos análisis
que indicaban que todas las cuentas de usuarios
de Yahoo se habían visto afectadas por el ataque de 2013.
El ataque, que comprometió los mismos datos
de usuario que el de 2014,
tuvo inmensas repercusiones financieras para Yahoo.
Los costos de investigación del ataque,
la mejora de técnicas de seguridad,
gastos legales, acuerdos millonarios y multas por
incumplimiento de normativas ascendieron a más
de 150 millones de dólares.
También se produjeron daños reputacionales,
ya que el incidente generó preocupación sobre la capacidad
de la compañía para proteger los datos de los usuarios
y cumplir con las normas de ciberseguridad;
todo ello contribuyó a un descenso de la base de usuarios de Yahoo.

Cabe destacar que la mayoría de estas
violaciones de datos fueron el resultado de ataques
en los que se detectaron vulnerabilidades
y se explotaron para obtener acceso no autorizado.
A la hora de desarrollar *software*,
es preferible [detectar las vulnerabilidades](../../soluciones/gestion-vulnerabilidades/)
en una fase temprana del SDLC que después de un robo.
Por eso recomendamos nuestra solución
de [Hacking Continuo](../../servicios/hacking-continuo/),
que identifica, explota y comunica las vulnerabilidades
de tu *software* para que puedas abordarlas de inmediato.
[Contáctanos](../../contactanos/) para más información.
