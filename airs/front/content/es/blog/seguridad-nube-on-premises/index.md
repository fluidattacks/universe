---
slug: blog/seguridad-nube-on-premises/
title: ¿Nos quedamos o nos vamos?
date: 2024-02-19
subtitle: On-premises o en la nube, la seguridad es tu responsabilidad
category: filosofía
tags: ciberseguridad, software, nube, tendencia, empresa
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1708380065/blog/on-premises-cloud-security/cover_on_premises_cloud_security.webp
alt: Foto por Kenrick Mills en Unsplash
description: Te traemos una breve comparación entre entornos on-premises y en la nube, revelando sus ventajas y desventajas, con un énfasis especial en la seguridad.
keywords: On Premises, Nube, Entornos, Infraestructura, Proveedores De Nube, Seguridad En La Nube, Datos Sensibles, Pentesting, Hacking Etico
author: Felipe Ruiz
writer: fruiz
name: Felipe Ruiz
about1: Escritor y editor
source: https://unsplash.com/photos/white-clouds-on-blue-sky-during-daytime-ppY9ZjLjB0o
---

"¡Míralos! No dejan de migrar a la nube
mientras nosotros permanecemos aquí en nuestros nidos.
¿Deberíamos seguirlos? ¿O deberíamos quedarnos aquí? ¿Qué sería lo mejor?"
Esto es algo que ciertos pájaros, perdón,
ciertas empresas que siguen *on-premises* o en las instalaciones
no dejan de preguntarse en esta era de transformación digital gigantesca.

Amazon Web Services, Google Cloud Platform y Microsoft Azure
son las plataformas de computación en la nube que,
en los últimos años, han liderado esta creciente mutación tecnológica,
alojando en sus infraestructuras los sistemas
y datos de empresas de todo tipo y tamaño alrededor del mundo.
Estos Goliats de la informática han ido persuadiendo
a las organizaciones para que migren de los entornos *on-premises*
a la nube al ofrecerles ventajas como mayores potencia computacional,
velocidad, almacenamiento de datos, escalabilidad y reducción de costos.

Sin embargo,
muchas compañías siguen reticentes a migrar total
o parcialmente a la nube,
lo que a veces puede estar plenamente justificado
y otras veces no.
En determinadas ocasiones,
para algunas organizaciones,
aunque podríamos suponer que cada vez son menos,
migrar a la nube puede no ser lo mejor o puede resultar innecesario.
Aquí entran en juego factores como el tamaño de la compañía,
el sector al que pertenece, sus operaciones de TI,
su capacidad económica, sus objetivos de negocio
e incluso su capacidad para planificar y su disciplina.
Sin embargo, quedarse *on-premises* porque ya están acostumbrados
a trabajar así o porque creen que podrían enfrentarse
a más riesgos de seguridad en la nube puede
que no sean justificaciones contundentes.
Comparemos brevemente los dos entornos.

## Entornos *on-premises* vs. en la nube

Cuando,
en calidad de compañía,
dispones de una **infraestructura *on-premises***,
debes ocuparte de adquirir,
instalar y realizar el mantenimiento de tu *hardware* y *software*,
así como de almacenar y proteger tus datos.
Una de las ventajas de este enfoque es poder controlar
todos los componentes y operaciones de tus sistemas,
incluida la seguridad de tus activos y datos sensibles.
La desventaja puede ser que debes disponer
de las herramientas necesarias
y de personal debidamente capacitado para un mantenimiento correcto.

Tanto el monitoreo como el mantenimiento del tiempo operativo
y la seguridad en entornos *on-premises* deben realizarse
las veinticuatro horas del día.
Tienes que estar siempre al tanto de las actualizaciones
y parches de *software*
y del estado y protección de la infraestructura física,
lo que a menudo puede convertirse en una gran carga,
sobre todo si no cuentas con los recursos suficientes.
(Esta sobrecarga es muy atractiva para los ciberdelincuentes.)
Mantener el control total en los sistemas locales
a veces puede significar incluso que tu compañía
se centre más en mantener todo en funcionamiento
y seguro que en ofrecer innovación en el mercado.

Esta carga puede reducirse significativamente cuando migras a la nube.
Al no tener una infraestructura corporativa que mantener,
puedes reducir considerablemente los costos.
En el entorno de la nube,
no hay necesidad de una inversión tan sustancial en herramientas y personal.
Principalmente porque los grandes proveedores
de la nube ya han invertido grandes cantidades
de dinero en métodos de seguridad de última generación
para monitorear el *software* y el *hardware* 24/7.
Ellos saben que con un servicio tan generalizado,
no pueden permitirse ser víctimas de ciberataques exitosos.

Sin embargo,
uno de los inconvenientes más serios en relación
con la seguridad en la nube es el llamado
"modelo de responsabilidad compartida",
el cual a veces es malinterpretado.
Como explicamos
en [un *post* enfocado en este modelo](../../../blog/shared-responsibility-model/),
los proveedores de servicios
en la nube son principalmente responsables de cumplir
con los requisitos de seguridad a nivel de infraestructura,
para el cual son evaluados y certificados.
En cambio, como cliente, te corresponde a ti cumplir
los requisitos de seguridad de tus datos,
aplicaciones u otros sistemas
(esto variará dependiendo del servicio que hayas contratado,
ya sea infraestructura como servicio,
plataforma como servicio o *software* como servicio).

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/pruebas-seguridad/"
title="Inicia ahora las pruebas de seguridad de Fluid Attacks"
/>
</div>

El caso es que,
dentro de la nube,
**debes seguir velando por la seguridad de tu empresa**
(aunque en menor medida)
y garantizar el cumplimiento de la normativa según tu territorio y sector.
No reconocer esta responsabilidad de forma adecuada
y no haber cambiado la mentalidad para comprender
la lógica de este entorno relativamente
nuevo representa un riesgo considerable.
De hecho,
muchas brechas o filtraciones pueden producirse simplemente
por configuraciones incorrectas del almacenamiento en la nube.
Sin embargo,
una migración y configuración adecuadas probablemente garantizarán
una reducción de la superficie de ataque de tu compañía.

Si tu empresa forma parte de la nube,
tus datos residen en los servidores del proveedor.
Aunque hayan invertido mucho en seguridad,
no es prudente descartar el riesgo de que
en algún momento alguien se infiltre
en uno de sus entornos de servidores y, desde allí,
pueda extraer y comprometer información,
no solo de una, sino de muchas organizaciones.
Sin embargo, dado que muchos ataques,
como el [*phishing*](../../../blog/phishing/),
suelen producirse a través de los puestos de trabajo de los usuarios
y estos se encuentran segmentados en la nube,
esto ayuda a evitar muchos riesgos de acceso.
Para contribuir a mitigar tu exposición a riesgos en la nube,
tendrás que gestionar procesos robustos
y avanzados de cifrado de la información
para los datos sensibles de tu compañía.
Además,
es recomendable que utilices autenticación multifactor,
límites el acceso por dirección IP y, por supuesto,
elijas un proveedor de nube acreditado y fiable.

Aparte de lo anterior,
otra desventaja de la infraestructura *on-premises*
es su escalabilidad limitada.
Si en un momento dado, por ejemplo,
tienes un número fijo de servidores pero
necesitas temporalmente un poco más de recursos computacionales
o de almacenamiento,
adquirir un nuevo servidor solo para ese
fin seguramente no será rentable.
En cambio,
si estás en la nube, pagarás por los recursos
y servicios que necesites bajo demanda.
Es decir,
puedes añadir o quitar recursos según necesites,
teniendo en cuenta evitar consumir más de lo previsto
y las consiguientes pérdidas.
En este caso,
los problemas de algunas compañías surgen cuando
no tienen una planificación adecuada
ni disciplina en la gestión de estos recursos.

Por último,
otra ventaja de almacenar tus datos
y aplicaciones en la nube es que esta ofrece la posibilidad
de realizar copias de seguridad de los datos y de la red.
Allí,
tus datos se almacenan en múltiples centros de datos,
por lo que la falla de uno de ellos
no te impedirá acceder a tu información;
es decir, no afectará a su disponibilidad.
Algo similar ocurre en el caso de los servidores.
Algunos proveedores de servicios
en la nube ofrecen servidores virtualizados,
los cuales permiten sencillas migraciones
entre ellos cuando se producen fallas en su funcionamiento.
En este sentido,
el problema de los entornos *on-premises* es que
el trabajo suele depender de unos pocos servidores físicos.
Así que, si estos se dañan, esto puede suponer pérdidas exorbitantes.

## Entornos híbridos

Cuando deseas aprovechar las ventajas de la nube
pero no abandonar la infraestructura *on-premises*
o cuando planeas migrar gradualmente,
existe otra opción: los entornos híbridos.
El primer caso es muy común entre aquellas organizaciones
que todavía dependen en gran medida
del control total sobre sus datos sensibles
y sistemas críticos.
Estos se almacenan y mantienen en el entorno de las instalaciones.
Por otro lado,
aquellos datos y sistemas que no cumplen
con estas características de privacidad se alojan en la nube,
de la cual la empresa interesada busca aprovechar su flexibilidad,
agilidad y reducción de costos al máximo.

## Donde sea que estés, Fluid Attacks es para ti

Aunque en Fluid Attacks abogamos por migrar a la nube,
algo que ya hicimos hace años con nuestros datos y productos,
también reconocemos que esto puede seguir siendo innecesario
o demasiado complicado para algunas organizaciones.
Tanto si migras como si no,
queremos recalcar que
el **monitoreo continuo de la seguridad sigue siendo tu responsabilidad**.
Ya sea que trabajes en una infraestructura *on-premises* o en la nube,
híbrida o incluso multi-nube,
necesitas ser plenamente consciente de los componentes integrados
e interconectados de tu entorno
y de sus vulnerabilidades de seguridad para remediarlas.

Si deseas dejar tus datos
y sistemas más importantes en tus instalaciones
por la tranquilidad que te da mantener
un control directo sobre estos,
sigue siendo prudente recordar que este material
debe ser revisado a fondo para garantizar su seguridad.
**En ciberseguridad, la ocultación no es sinónimo de privacidad**.
Si estás en la nube,
no olvides tus responsabilidades de seguridad en la misma:
Conocer a fondo el servicio ofrecido por el proveedor,
evaluar plenamente tus configuraciones
y verificar constantemente el estado de seguridad
de tus datos y aplicaciones.

Desde Fluid Attacks,
con nuestras herramientas automatizadas,
inteligencia artificial y equipo de *hacking*,
ya estés aquí o allá,
podemos ayudarte a cumplir con tus responsabilidades de ciberseguridad.
No dudes en [contactarnos](../../contactanos/).
[Haz clic aquí](https://app.fluidattacks.com/SignUp)
si quieres empezar con una **prueba gratuita**.
