---
slug: blog/ciberseguridad-bancaria/
title: Ciberseguridad en la banca
date: 2024-06-20
subtitle: Una gran comodidad conlleva un mayor riesgo
category: filosofía
tags: ciberseguridad, empresa, tendencia, riesgo, software
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1718836225/blog/bank-cybersecurity/cover_bank_cybersecurity.webp
alt: Foto por Andre Taissin en Unsplash
description: Descubre la creciente oleada de riesgos cibernéticos que los bancos deben afrontar y conoce los consejos de ciber higiene para este sector.
keywords: Ciberseguridad Bancaria, Seguridad En El Sector Bancario, Postura De Ciberseguridad, Evaluacion De Riesgos, Regulaciones En La Banca, Retos De La Ciberseguridad Bancaria, Ciber Higiene Bancaria, Hacking Etico, Pentesting
author: Wendy Rodriguez
writer: wrodriguez
name: Wendy Rodriguez
about1: Escritor y editor
source: https://unsplash.com/photos/pink-and-black-ceramic-piggy-bank-Dc2SRspMak4
---

Como muchas otras industrias,
el sector bancario ha emprendido gradualmente
una transformación digital en los últimos años.
Esta transición ha traído consigo oportunidades nuevas e interesantes,
tanto para las empresas como para los particulares.
La comodidad de la banca en línea es indiscutible.
Con unos pocos clics, se pueden consultar saldos,
transferir fondos o pagar facturas.

Sin embargo,
una gran comodidad conlleva una gran responsabilidad,
especialmente en lo que se refiere a la ciberseguridad.
Los bancos están constantemente asediados por atacantes
que buscan explotar vulnerabilidades
y robar a sus clientes el dinero que tanto
les ha costado ganar así como información que podría aprovecharse
en un rescate o venderse en la *dark web*.

### Estadísticas y consecuencias de ciberataques en la banca

Las estadísticas y los incidentes recientes dibujan
el panorama actual al que se enfrentan los bancos.
Estos,
junto con otros tipos de instituciones del [sector financiero](../ciberseguridad-servicios-financieros/),
son objetivos predilectos para los ciberdelincuentes.
Esto es lo que informan las estadísticas:

- Un [informe sobre DDoS de 2023](https://www.fsisac.com/hubfs/Knowledge/DDoS/FSISAC_DDoS-HereToStay.pdf)
  informa que un tercio de todos los ataques
  de denegación de servicio distribuidos (DDoS)
  se dirigieron al sector financiero,
  lo que lo convierte en la industria más atacada.
- Un reporte de [Sophos sobre el estado de los ataques de *ransomware*](https://news.sophos.com/en-us/2023/07/13/the-state-of-ransomware-in-financial-services-2023/)
  indica que los servicios financieros, incluidos los bancos,
  siguen siendo un mercado muy atacado,
  pasando del 55% en el informe de 2022 al 64% en el de 2023.
- [El reporte del costo de violación de datos 2023 de IBM](https://www.ibm.com/reports/data-breach)
  estima que el costo promedio de un ciberataque a una institución financiera
  es de aproximadamente 5,9 millones de dólares.
- [Entre 2019 y 2020](https://www.complianceweek.com/surveys-and-benchmarking/report-fines-against-financial-institutions-hit-104b-in-2020/29869.article),
  los servicios financieros de todo el mundo recibieron multas
  que ascendieron a 10.400 millones de dólares por parte
  de las entidades reguladoras a causa de incumplimientos.
- [Fortunly](https://fortunly.com/statistics/data-breach-statistics/)
  informó que el 92% de los cajeros automáticos son vulnerables a los ataques.
- Los reguladores estadounidenses multaron al [banco Capital One](../mayores-violaciones-datos-sector-financiero/)
  con 80 millones de dólares después de una violación de datos en 2019.
  Esta violación expuso la información de alrededor de 100 millones de usuarios
  en Estados Unidos y alrededor de 6 millones en Canadá.

Las repercusiones de los ciberataques exitosos
en los bancos pueden ser profusas.
Siempre existe la posibilidad de pérdidas financieras asociadas
con fondos robados, pagos de rescates por *ransomware*,
honorarios de abogados, gastos de recuperación, entre otros.
Los bancos también pueden enfrentarse
a cuantiosas multas por infracciones de la regulación.
Otra repercusión es el tiempo de inactividad derivado
de una interrupción operativa.
Los daños a la reputación,
la pérdida de confianza y de clientes son las consecuencias menos deseables,
pero más probables, a las que puede enfrentarse un banco tras un incidente.

## Regulaciones de ciberseguridad en la banca

Los bancos operan bajo rigurosos marcos reguladores destinados
a garantizar la seguridad de los sistemas financieros,
incluida la protección de los datos de los clientes.
Las entidades reguladoras cambian de un país a otro,
pero todas buscan formas de proteger al consumidor.

Por ejemplo, en Estados Unidos,
hay varias regulaciones obligatorias que cumplir, como:

- Lla de la autoridad interinstitucional [FFIEC](https://www.ffiec.gov/)
  (Federal Financial Institutions Examination Council)
- Las políticas y normas de protección de los titulares de tarjetas
  [PCI DSS](https://www.pcisecuritystandards.org/)
  (Payment Card Industry Data Security Standards).
- La Unión Europea tiene el [GDPR](https://gdpr-info.eu/)
  (General Data Protection Regulation)
  que determina cómo se usan y protegen los datos de los ciudadanos de la UE.
- El Reino Unido tiene su equivalente, el [Data Protection Act](https://www.gov.uk/data-protection#:~:text=The%20Data%20Protection%20Act%202018%20is%20the%20UK's%20implementation%20of,used%20fairly%2C%20lawfully%20and%20transparently).
- Singapur cuenta con la agencia reguladora [MAS](https://www.mas.gov.sg/)
  (Monetary Authority of Singapore).
- Canadá cuenta con [OSFI](https://www.osfi-bsif.gc.ca/en)
  (Office of the Superintendent of Financial Institutions).

Y así sucesivamente.

El cumplimiento con estos y otros organismos reguladores
requiere actualizaciones constantes de los protocolos
que deberían incluir [pruebas de seguridad](../../soluciones/pruebas-seguridad/)
preventivas,
informes exhaustivos de incidentes y auditorías periódicas,
garantizando así las medidas de ciberseguridad robustas
que los bancos deben seguir.

## Los retos de la ciberseguridad bancaria

Tradicionalmente,
los bancos operan con departamentos separados
que utilizan sistemas diferentes y buscan alcanzar sus propios objetivos.
Esta falta de integración ha obstaculizado el crecimiento,
restringido la escalabilidad, disminuido la satisfacción del cliente
y facilitado la propagación de vulnerabilidades de seguridad.

El panorama bancario actual implica una vasta red
de tecnologías interconectadas,
que incluyen desde plataformas móviles hasta servicios en la nube.
Esta interconexión amplía la superficie de ataque
(es decir, crea muchos posibles puntos de entrada para los ciberdelincuentes).
Otras circunstancias, como el aumento de la dependencia
de los canales digitales, los entornos personalizables
en la nube y el uso de *software* de terceros también han
creado una mayor superficie de ataque.

Otros retos están alimentados por varios factores.
Hay que tener en cuenta los sistemas antiguos desactualizados
y un personal poco preparado que carece de los conocimientos necesarios.
La evolución de las tácticas de los ciberdelincuentes,
como la ingeniería social y los ataques de *spear-phishing*,
las herramientas avanzadas como los kits de explotaciones
e incluso el aprendizaje automático manipulado
para aprovechar las vulnerabilidades son también factores
que contribuyen a la proliferación de las ciberamenazas.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/gestion-vulnerabilidades/"
title="Inicia ahora con la solución de gestión de vulnerabilidades
de Fluid Attacks"
/>
</div>

## Importancia de la ciber higiene bancaria

Una cultura de ciber higiene en los bancos fomenta
un entorno bancario digital más seguro.
Su propósito es proteger los activos valiosos,
mantener la confianza de los clientes,
cumplir los requisitos regulatorios
y garantizar la estabilidad operativa.
Es algo más que implementar soluciones técnicas;
se trata de crear una responsabilidad compartida
en la que tanto los empleados como los clientes
comprendan la importancia de las buenas prácticas de ciberseguridad.
Este planteamiento colectivo es vital
para evitar violaciones de datos, instalación de *malware*
y otros incidentes que pueden interrumpir los servicios bancarios.

### Ciber higiene bancaria

A continuación exponemos las prácticas clave para cultivar
una ciber higiene sólida dentro de una entidad bancaria:

- **Marco de seguridad robusto:** Implementar un marco de seguridad integral
  como el [Marco de Ciberseguridad de NIST](https://www.nist.gov/cyberframework)
  o el [ISO 27001](https://www.iso.org/standard/27001).
  Estos marcos proporcionan un enfoque estructurado para
  identificar, proteger, responder y recuperarse de un ciberataque.
  Otro marco que recomendamos es el modelo de seguridad [Zero Trust](../../learn/seguridad-zero-trust/),
  y su solución [ZTNA](../ztna/).
  *Zero trust* se basa en principios como el privilegio mínimo,
  la autenticación y supervisión continua,
  la microsegmentación y la presunción de violación.
  Todos estos principios contribuyen a mejorar la ciberseguridad de un banco.

- **Evaluación habitual de los riesgos:** Realizar evaluaciones
  de riesgos periódicas para identificar posibles amenazas
  a la infraestructura de TI, aplicaciones y procesos,
  así como su impacto y propensión.
  Esto ayudará a crear una estrategia de gestión de riesgos que contribuya,
  entre otras cosas, a priorizar
  y gestionar las vulnerabilidades de forma rápida y eficaz.

- **La privacidad de los datos como prioridad:** Una de las
  principales preocupaciones de las regulaciones y leyes es este punto.
  La protección de datos incluye varios procesos
  y prácticas que analizamos
  en otro [*post* del blog](../proteccion-datos-servicios-financieros/).

- **Seguridad multinivel:** Implementar una defensa por capas con *firewalls*,
  sistemas de detección de intrusos, cifrado de datos y la muy recomendable MFA.
  La autenticación multifactor añade una capa adicional de seguridad,
  ya que va más allá de las contraseñas.
  Requiere múltiples formas de autenticación,
  y puede exigirse a empleados, clientes e incluso proveedores.

- **Supervisión y pruebas continuas:** Vigilar continuamente la actividad
  de la red y los sistemas de seguridad buscando vulnerabilidades
  para poder detectarlas y solucionarlas rápidamente.
  Esto incluye [pruebas de penetración](../pentesting/) frecuentes
  ([obligatorias](../../../blog/penetration-testing-compliance/)
  en algunos estándares, p.ej. PCI DSS, SWIFT CSCF)
  y [escaneo de vulnerabilidades](../escaneo-de-vulnerabilidades/).
  Ambas son soluciones proporcionadas por Fluid Attacks,
  en las que la experiencia de
  su [equipo de hacking](../../soluciones/hacking-etico/)
  se suma a las capacidades
  de escaneo de su herramienta automatizada.

- **Plan de respuesta a incidentes:** Todos los bancos deben tener
  un [plan de respuesta a incidentes](../../../blog/incident-response-plan/)
  establecido y claro que describa los procedimientos para
  detectar, contener y recuperarse de los ciberataques.

- **Gestión de proveedores externos:** Antes de permitir
  el acceso a cualquier sistema, evaluar
  la postura de ciberseguridad de los proveedores
  con los que el banco está considerando trabajar.
  [Las entidades financieras deben asegurarse](../cadena-suministro-sector-financiero/)
  de que la seguridad del proveedor está alineada con la suya.

- **Actualización constante:** Mantener una cultura de actualización
  continua de los sistemas con los últimos parches y configuraciones.

- **Capacitación de los empleados:** Capacitar regularmente
  al personal sobre las buenas prácticas de ciberseguridad,
  la concientización sobre el [*phishing*](../../../blog/phishing/)
  y tácticas de [ingeniería social](../../../blog/social-engineering/)
  para minimizar los errores humanos.

- **Educación del cliente:** Informar a los clientes sobre
  las amenazas a la seguridad en línea y motivarlos para que se protejan,
  esto por medio de correos electrónicos informativos.
  Promover prácticas seguras, como el uso de contraseñas robustas
  y la activación de MFA, y proporcionarles información
  sobre correos electrónicos o llamadas telefónicas de *phishing*.

### Lista para evaluar la seguridad

Los responsables de seguridad pueden plantearse a sí mismos
y a sus equipos preguntas como las siguientes
para evaluar la postura de ciberseguridad de su banco:

- ¿Se realizan con regularidad evaluaciones de riesgos,
  análisis de vulnerabilidades y pruebas de penetración?
- ¿Se aplican los controles de acceso correctos para restringir el acceso
  a la información sensible?
- ¿Se garantiza el cifrado de extremo a extremo para todos los datos,
  tanto en tránsito como en reposo?
- ¿Se aplica el MFA en todos los sistemas críticos?
- ¿Se actualizan y parchean continuamente todos los programas y sistemas?
- ¿Dispone el banco de un plan fiable de respuesta a incidentes que incluya
  esquemas de comunicación y procedimientos post-ataque
  para hacer frente al ciberataque?
- ¿Están capacitados los empleados para reconocer e informar sobre cualquier
  amenaza cibernética?
- ¿Son conscientes los distintos departamentos del banco
  de su responsabilidad compartida en materia de ciberseguridad?
- ¿Incluye el consejo de directivos a personas
  que posean conocimientos sobre ciberseguridad?

### Mitigación de riesgos en el desarrollo de aplicaciones bancarias

Integrar la seguridad en la estructura de las aplicaciones bancarias
desde el principio es fundamental para mitigar los ciberriesgos.
Los riesgos pueden identificarse en una fase temprana
del ciclo de vida de desarrollo del *software* (SDLC)
mediante la integración de factores de seguridad.
Esto puede incluir un análisis preliminar para comprender
las necesidades del banco y ejercicios con modelos
de amenazas para identificar posibles vulnerabilidades.

Otra forma de mitigar los riesgos es aplicar estándares
de codificación que den prioridad a la seguridad,
así como realizar revisiones constantes del código
para identificar y corregir las vulnerabilidades en una fase temprana.
La solución de [revisión de código seguro](../../soluciones/revision-codigo-fuente/)
de Fluid Attacks
combina la ventaja de las herramientas de revisión de código fuente
con la de la revisión manual.
Esto permite una identificación temprana y precisa de los puntos débiles
y una remediación pronta de los mismos.

Otras estrategias de mitigación en desarrollo podrían incluir
la creación de planes para mitigar los riesgos,
adoptando un modelo de desarrollo seguro como
el [marco de evaluación de riesgos de OWASP](https://owasp.org/www-project-risk-assessment-framework/)
para proporcionar un enfoque estructurado.
El uso de *software* y bibliotecas de código abierto
bien verificadas ayudan a prevenir riesgos adicionales.

Por último, la integración de las pruebas de seguridad
en los *pipelines* CI/CD
(y la desactivación de dichos *pipelines* si se detectan fallos en el código)
puede ayudar a detectar vulnerabilidades
y remediarlas antes de que se desplieguen.
Esto puede ahorrar a todos tiempo y dinero que,
de otro modo, se destinaría a gastos de reparación.

## La solución de Fluid Attacks para el sector bancario

Los retos y amenazas que acechan a los bancos
no van a terminar.
Cada vez son más complejos y menos fáciles de detectar.
En lugar de esperar a que los atacantes exploten las vulnerabilidades,
los bancos pueden adoptar una solución como la que ofrecemos.
Nuestro enfoque proactivo de la gestión de vulnerabilidades
ha ayudado a los bancos a mejorar continuamente su postura de seguridad
y a adelantarse a la evolución constante de las ciberamenazas.

Nuestra solución [Hacking continuo](../../servicios/hacking-continuo/)
es la opción AppSec ideal para los bancos.
Nuestra solución integral no solo utiliza
herramientas automatizadas de escaneo de vulnerabilidades como
[SAST](../../producto/sast/) o [SCA](../../producto/sca/),
sino que también aprovecha la IA, así como a nuestros *pentesters*
para identificar y explotar vulnerabilidades a lo largo del SDLC.
Y como se ha mencionado anteriormente
cuanto antes identifiquemos y notifiquemos una vulnerabilidad,
más protegida estará tu aplicación.
Hablando de reportes, la visibilidad que nuestra plataforma provee es extensa.
Gestionar las vulnerabilidades es más fácil
ya que puedes informarte sobre el problema de seguridad,
su gravedad y ubicación, priorizarlo,
asignarlo a tu equipo e incluso recibir sugerencias
de solución de nuestros *pentesters*.
Conoce nuestra plataforma [aquí](../../plataforma/).

Dado que buscamos agilizar el proceso de trabajo de tus desarrolladores,
nuestra plataforma se integra fácilmente con sistemas existentes,
lo que mejora la eficiencia y escalabilidad
del proceso de gestión de vulnerabilidades.
Con nuestras funciones de integración, puedes crear *issues* desde GitLab,
Azure DevOps o Jira automáticamente,
encontrar problemas de seguridad en tus ambientes de nube AWS o GCP,
y utilizar la extensión VS Code para ir a la línea de código específica
en la que se descubrió la vulnerabilidad,
aprovechar la IA generativa para obtener sugerencias de correcciones,
y mucho más.
Consulta qué extensiones pueden serte útiles [aquí](https://help.fluidattacks.com/portal/en/kb/integrate-with-fluid-attacks).

El camino hacia una postura de ciberseguridad robusta es continuo,
repetitivo, requiere vigilancia, adaptabilidad y compromiso.
Nosotros, en Fluid Attacks,
queremos formar parte de ese camino.
[Contáctanos](../../contactanos/)
y permítenos mostrarte lo que podemos hacer por ti.
