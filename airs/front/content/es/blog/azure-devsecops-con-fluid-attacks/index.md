---
slug: blog/azure-devsecops-con-fluid-attacks/
title: Cómo apoyamos DevSecOps en Azure
date: 2022-11-03
subtitle: Evaluaciones manuales continuas para superar el MCSB
category: filosofía
tags: ciberseguridad, devsecops, nube, empresa
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1667483115/blog/azure-devsecops-with-fluid-attacks/cover_azure.webp
alt: Foto por Alvan Nee en Unsplash
description: El Microsoft Cloud Security Benchmark recomienda red teaming y pentesting regulares. Te contamos cómo te ayudamos a ir más allá y lograr DevSecOps en Azure.
keywords: Devsecops, Devsecops Azure, Herramientas Devsecops Azure, Devsecops En La Nube, Devsecops En Azure, Microsoft Cloud Security Benchmark, Red Teaming, Hacking Etico, Pentesting
author: Jason Chavarría
writer: jchavarria
name: Jason Chavarría
about1: Escritor y editor
source: https://unsplash.com/photos/ZCHj_2lJP00
---

[Azure](https://azure.microsoft.com/es-mx/)
es la plataforma de computación en la nube pública de Microsoft.
Siendo uno de los principales proveedores,
[superó](https://www.zdnet.com/article/cloud-computing-microsoft-azure-ups-the-pressure-on-aws/)
a Amazon Web Services (AWS)
y Google Cloud en crecimiento de adopción durante el 2021.
Como tal,
Azure ofrece herramientas y funcionalidades,
así como infraestructura,
almacenamiento y capacidad de procesamiento.
Esto permite a empresas de todos
los tamaños desarrollar aplicaciones escalables
y ahorrar costos.
Puesto que estas aplicaciones deben ser seguras,
Azure cuenta con servicios que ayudan a los usuarios
a seguir [las buenas prácticas de DevSecOps](../buenas-practicas-devsecops/).

A pesar de que algunas actividades de DevSecOps
consisten en automatizar las pruebas
y actualizaciones de seguridad,
la tecnología por sí sola no es suficiente.
El mismo Azure
aconseja en el [Microsoft cloud security benchmark](https://learn.microsoft.com/en-us/security/benchmark/azure/)
(MCSB)
que técnicas manuales como el
[***red teaming***](../../../blog/red-team-exercise/)
y [***pentesting***](../que-es-prueba-de-penetracion-manual/)
deben complementar el escaneo de vulnerabilidades para descubrir riesgos.
Debería prestarse más atención a esto,
ya que nuestros *pentesters* suelen encontrar configuraciones
inseguras en los servicios en la nube.
En este artículo del blog,
hablamos sobre cómo Fluid Attacks apoya tu implementación
de DevSecOps en Azure con técnicas manuales en combinación
con pruebas de seguridad automatizadas.

## El modelo de responsabilidad compartida de seguridad en la nube (SRM)

Antes de pasar al tema principal de este *post*,
abordemos primero un tema relevante:
el [modelo de responsabilidad compartida](../../../blog/shared-responsibility-model/)
de seguridad en la nube (SRM, por sus siglas en inglés).
No te distraeremos durante mucho tiempo.
Todo lo que necesitas recordar es que Azure,
como proveedor de servicios en la nube,
será responsable de acciones como mantener
la disponibilidad de sus servicios,
la seguridad de la infraestructura física que los soporta
y la incorporación de características
de seguridad en sus soluciones.

Pero los usuarios también tenemos que ser conscientes
de nuestras responsabilidades.
Tenemos que proteger nuestro ambiente DevOps
y desplegar aplicaciones seguras en la nube.
Por lo tanto,
tendremos que responsabilizarnos si no protegemos
la cadena de suministro de *software*,
no realizamos pruebas de seguridad continuas,
no configuramos los servicios correctamente, etc.
Para cumplir con esta responsabilidad,
los usuarios debemos recurrir
a [DevSecOps en la nube](../importancia-de-la-nube-devsecops/).

## DevSecOps en Azure con Fluid Attacks

La cultura [DevSecOps](../concepto-devsecops/)
busca unir a los equipos de desarrollo,
seguridad y operaciones desde el inicio del desarrollo de *software*
para construir apps más seguras.
(Explicamos en otro artículo [cómo implementar DevSecOps](../como-implementar-devsecops/)).
En la nube,
esta cultura requiere que los equipos presten
atención lo antes posible a la configuración adecuada
de los servicios en la nube
y realicen pruebas de seguridad continuas de los archivos
de infraestructura como código (IaC, por sus siglas en inglés)
y las imágenes de contenedores, entre otras cosas.
Este enfoque de mover la seguridad hacia la izquierda facilita
y hace menos costoso resolver los problemas.

Para ayudar a sus usuarios a lograr DevSecOps,
Azure ha proporcionado recomendaciones
para automatizar la seguridad con sus productos,
lo que además está en el centro
del [MCSB](https://learn.microsoft.com/en-us/security/benchmark/azure/)
publicado en octubre de 2022,
el cual es un cambio de marca del Azure Security Benchmark (ASB).
Este recurso proporciona controles prescriptivos
que ayudan a los usuarios a mejorar su ambiente multinube
y la seguridad de las cargas de trabajo,
datos y servicios en Azure.

Los controles incluidos en el MCSB
comprenden actividades para proteger los datos,
las redes y el acceso a los recursos,
obtener visibilidad de los activos y los riesgos para estos,
y garantizar estrategias, políticas
y procedimientos de seguridad organizacionales claros.

Existen herramientas de DevSecOps en Azure
para ayudar a implementar estos controles.
De modo que,
por ejemplo, los equipos de desarrollo pueden utilizar
el servicio Azure Pipelines para CI/CD.
Esta herramienta les permite compilar y agrupar código en contenedores,
protegiendo así el ambiente del desarrollador,
y tener control de versiones.
Para abordar la cuestión de la protección de datos,
redes y accesos,
Azure ofrece el servicio Azure Key Vault para almacenar claves de API,
certificados, *tokens*, contraseñas y otros secretos,
y Azure Active Directory (Azure AD)
para gestionar la identidad y el control de acceso.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/devsecops/"
title="Empieza ya con la solución DevSecOps de Fluid Attacks"
/>
</div>

A la hora de evaluar los riesgos,
dos aspectos del MCSB merecen atención especial:
la **gestión de posturas y vulnerabilidades** y la **seguridad en DevOps**.
Estos son algunos controles que,
en nuestra opinión,
resumen las actividades de evaluación en estos dominios:

- **Infraestructura DevOps segura:**
  acciones como identificar cualquier error de configuración
  en áreas centrales de Azure DevOps,
  mecanismos de autenticación débiles
  y secretos en el código fuente.

- **Garantizar la seguridad de la cadena de suministro de *software*:**
  acciones como garantizar que los componentes
  de código abierto están actualizados
  y libres de vulnerabilidades conocidas.

- **Integrar las pruebas de seguridad de aplicaciones estáticas (SAST)
  en el proceso DevOps:**
  acciones como evaluar el código fuente y evitar
  que el código vulnerable se despliegue en producción.

- **Integrar las pruebas de seguridad de aplicaciones dinámicas (DAST)
  en el proceso DevOps:**
  acciones como evaluar la aplicación en tiempo de ejecución.

- **Reforzar la seguridad de la carga de trabajo
  a lo largo del ciclo de vida de DevOps:**
  acciones como la gestión de vulnerabilidades en imágenes de contenedores y
  dependencias a lo largo de todo el ciclo de vida de desarrollo
  de *software* (SDLC).

- **Llevar a cabo operaciones regulares de *red team*:**
  acciones como complementar el enfoque tradicional de escaneo
  de vulnerabilidades con *pentesting* y *red teaming*.

Azure ofrece orientación que puede abordar
algunos controles en estas áreas.
Así pues,
aconseja escanear las imágenes de los contenedores
en busca de vulnerabilidades
con Microsoft Defender for Cloud y también utilizar
las soluciones de pruebas de seguridad automatizadas
de GitHub para seguir buscando vulnerabilidades
en el código propietario
(p. ej., credenciales en el código fuente)
y en los componentes de terceros
(p. ej., bibliotecas de código abierto desactualizadas).

Si se tiene en cuenta el último control de la lista anterior,
se puede ver que se queda corto un enfoque centrado únicamente
en el uso de herramientas de seguridad Azure DevOps
y otro *software* DevSecOps.
Una razón importante es que las herramientas automatizadas
arrojan resultados con tasas significativas de falsos positivos
y falsos negativos.
Se necesita trabajo manual para revisar los resultados
e identificar problemas que una herramienta,
por ejemplo, para SAST o DAST, no haría.

Sin ir más lejos:
en nuestro [State of Attacks de 2022](https://fluidattacks.docsend.com/view/behmfvipxcha2t7v),
en el que mostramos los análisis de los resultados
de las pruebas de seguridad de nuestros clientes
a lo largo de un año,
descubrimos que fueron solo nuestros *pentesters*,
y no las herramientas,
quienes detectaron **todas las vulnerabilidades de severidad crítica**
reportadas en los sistemas.

Como expertos en *pentesting* y *red teaming*,
ahora te mostramos cómo podemos apoyar tu implementación
de DevSecOps con estos enfoques.

Las **pruebas de penetración**
se realizan mediante simulaciones de ataques reales,
por ejemplo,
utilizando ataques personalizados para eludir las defensas.
Este es un excelente ejemplo de pruebas funcionales que exigen creatividad
y un conocimiento de las técnicas de los autores de amenazas actuales
que no puede lograrse mediante herramientas.
(De hecho,
[sostenemos](../que-es-prueba-de-penetracion-manual/)
que no existen las pruebas de penetración "automatizadas").
Pero evaluar la aplicación desde el exterior
no es la única forma de realizar *pentesting*.
También existe la [revisión manual del código fuente](../../soluciones/revision-codigo-fuente/)
para encontrar problemas recurrentes como credenciales
en el código fuente y errores de configuración de los servicios.
Por lo tanto,
te recomendamos que vayas más allá del consejo
de las pruebas de penetración "regulares"
y las **conviertas en una actividad continua**
a lo largo del SDLC.

Utilizando nuestra [solución de *pentesting*](../../soluciones/pruebas-penetracion-servicio/),
puedes disfrutar de los siguientes beneficios:

- Una comprensión más completa de las vulnerabilidades
  gracias al apoyo experto de nuestro equipo de *pentesters*.

- Una combinación de pruebas de seguridad automatizadas
  y manuales que garantiza la detección
  de vulnerabilidades de gravedad crítica,
  arrojando tasas muy bajas de falsos positivos y falsos negativos.

- Conocimiento actualizado sobre el estado de seguridad de tu aplicación,
  solicitándonos que realicemos *pentesting* continuamente
  a medida que evoluciona el *software* nativo de la nube,
  lo que se conoce como
  [pruebas de penetración como servicio](../que-es-ptaas/) (PTaaS).

- La opción de solicitar que rompamos el *build*
  para evitar que los *builds* vulnerables que violen
  las políticas de tu organización pasen a producción.

- Reataques ilimitados por parte de nuestros *pentesters*
  sin costo adicional
  (esto es,
  para verificar si las vulnerabilidades han sido efectivamente remediadas).

***Red teaming*** también es una simulación de ataques reales,
pero se acerca aún más a la realidad.
Por ejemplo,
no se limita a las evaluaciones a nivel tecnológico,
sino que también incluye pruebas a nivel propiamente humano.
Esto forma parte de un enfoque holístico
para averiguar la eficacia de las estrategias de prevención,
detección y respuesta a los ataques de una organización.
Por consiguiente,
en *red teaming*, los ataques se realizan
con el consentimiento de los directivos de la organización,
pero la mayoría de las personas del equipo de respuesta a incidentes
y los empleados no son conscientes de ello.
Y otra característica que lo hace más realista
es que el [*red team*](../../../blog/red-team-exercise/)
no se centra en encontrar todas las vulnerabilidades,
sino que se fija objetivos específicos.

Utilizando nuestra [solución de *red teaming*](../../soluciones/red-team/),
además de los de nuestra solución de *pentesting*,
puedes disfrutar de los siguientes beneficios:

- Simulaciones de ataques que son altamente realistas,
  ya que imitan las tácticas,
  técnicas y procedimientos de los atacantes.

- Una visión panorámica de la seguridad de tu organización
  a través de las evaluaciones de *pentesters* con
  [certificaciones](../../../certifications/) avanzadas (OSEE,
  CRTO, CRTE y CARTP).

Obtienes una visualización constantemente actualizada
de todos los hallazgos en nuestra [plataforma](../../plataforma/).
En ella,
trazamos un mapa de todos los activos digitales
de tu organización y puedes realizar un seguimiento
de su exposición al riesgo a lo largo del tiempo.
También puedes ver las pruebas de explotación de vulnerabilidades técnicas,
asignar su remediación a miembros de tu equipo,
hablar con nuestros *pentesters* para que te orienten
y muchas otras cosas.

Ahora es el momento de comenzar Azure [DevSecOps](../../soluciones/devsecops/)
con Fluid Attacks.
¡[Contáctanos](../../contactanos-demo/)\!

¿No estás seguro?
Disfruta de nuestras pruebas de seguridad automatizadas
[gratuitamente durante 21 días](https://app.fluidattacks.com/SignUp).
Puedes cambiar de plan en cualquier momento
para añadir pruebas de seguridad manuales.
