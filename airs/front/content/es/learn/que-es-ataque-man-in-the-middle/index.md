---
slug: learn/que-es-ataque-man-in-the-middle/
title: Ataque Man-In-The-Middle
date: 2023-11-03
subtitle: ¿Qué es un ataque MITM?
category: attacks
tags: vulnerabilidad, riesgo, empresa, web, ciberseguridad
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1699021846/learn/man%20in%20the%20middle/cover_mitm_1.webp
alt: Foto por Angel Luciano en Unsplash
description: Aprende qué es un ataque man-in-the-middle (MITM), cómo funciona, qué tipos hay y cómo prevenirlos.
keywords: Fluid Attacks, Man In The Middle, Ataques, Mitm, Web, Pentesting, Ethical Hacking
author: Wendy Rodriguez
writer: wrodriguez
name: Wendy Rodriguez
about1: Escritor y editor
source: https://unsplash.com/photos/black-and-white-eye-illustration--hWwL0n3_As
---
## Man in the middle

Imagínate esto:
estás teniendo una conversación con un amigo por teléfono,
alguien en quien confías y le cuentas tus secretos.
De repente recibes una respuesta de tu amigo
que no tiene nada que ver con lo que le dijiste; no tiene sentido.
Acabas descubriendo que hay una tercera persona espiando su conversación
y modificando los mensajes sin que ninguno de los dos lo sepa,
enviando el mensaje que ese tercero quiere transmitir.
Este intermediario atenta contra tu intimidad,
dificultando el proceso
de conversación con tu amigo.

Este es un simple ejemplo de alguien vigilando
y alterando algo privado,
pero en el mundo cibernético,
existe un ataque de intermediario llamado man-in-the-middle,
el cual se infiltra en los sistemas para cambiar el contenido,
redirigir a los usuarios a sitios peligrosos, o acceder a información valiosa,
causando graves daños a su paso.

## ¿Qué es un ataque man-in-the-middle?

Un ataque man-in-the-middle (MITM)
es un tipo de ciberataque en el que el actor malicioso
se infiltra en secreto, retransmite o altera la comunicación entre dos partes
que creen estar comunicándose entre sí directamente.
Como resultado, el atacante es capaz de interceptar y/o modificar
cualquier dato intercambiado entre las dos partes
sin que estas se den cuenta.
Los ataques MITM, por sus iniciales en inglés, pueden ser muy peligrosos,
ya que permiten robar datos sensibles,
como contraseñas, números de tarjetas de crédito y detalles de cuentas,
o incluso podrían redirigir a los usuarios
a sitios web maliciosos donde el atacante
espera para apoderarse de sus sistemas.

Los ataques MITM pueden realizarse de diversas formas,
pero uno de los métodos más comunes
es crear un falso *hotspot* de conexión Wi-Fi
que tenga el mismo nombre que un *hotspot* legítimo.
Cuando un usuario se conecta al falso *hotspot*,
el atacante es capaz de interceptar todo el tráfico
que se envía entre el dispositivo del usuario y la Internet.
Otro método habitual utilizado en ataques MITM es el uso de un servidor proxy,
que actúa como intermediario entre un cliente y un servidor.

Este tipo de ataques no son tan comunes como los conocidos
[*phishing*](../../../blog/phishing/) o [*ransomware*](../../../blog/ransomware/),
pero hasta un [35% de los ataques en el 2019](https://www.thesslstore.com/blog/80-eye-opening-cyber-security-statistics-for-2019/#man-in-the-middle-attack-statistics)
estaban relacionados con intentos de explotación a través de ataques MITM.

## ¿Cómo funcionan los ataques man-in-the-middle?

Los ataques MITM pueden producirse de varias formas
y tienen como objetivo medios de comunicación diferentes,
como la Internet, el correo electrónico o los *hotspots*.
Los atacantes pueden hacer lo siguiente:

- **Interceptación**: Los atacantes se interponen
  entre las dos partes que intentan comunicarse,
  interceptando los datos que fluyen entre ellos.
  Esto puede hacerse utilizando varias técnicas, como espiar
  en una red Wi-Fi insegura, manipular el *hardware* de una red,
  o aprovechar las vulnerabilidades de un *software* o un dispositivo.
  Los atacantes también pueden hacerse pasar por una
  o ambas partes implicadas en la comunicación.

- **Descifrado**: Los atacantes capturan los datos intercambiados
  entre las dos partes, que luego son descifrados
  y manipulados sin el conocimiento de los usuarios.
  Los datos pueden incluir información sensible
  que es susceptible de ser robada o alterada,
  lo que podría conducir, por ejemplo,
  al robo de activos financieros o a la inyección
  de código malicioso en un sitio web.

Los ataques MITM pueden ser sofisticados
y su éxito depende a menudo de las habilidades de los atacantes
y de las vulnerabilidades de los sistemas atacados.

### Tipos de ataques man-in-the-middle

Los ataques MITM pueden ejecutarse mediante diversas técnicas, tales como:

- **Suplantación de ARP**: (ARP spoofing en inglés)
  es la manipulación por parte de los atacantes del protocolo
  de resolución de direcciones (ARP)
  para asociar sus direcciones MAC con las direcciones IP
  de dispositivos legítimos en la red,
  provocando que el tráfico se enrute a través de sus máquinas.

- **Suplantación de DNS**: (DNS spoofing en inglés)
  es la manipulación del sistema de nombres de dominio (DNS)
  para redirigir a los usuarios a sitios web falsos
  que controlan los atacantes.

- **Eliminación de SSL**: (SSL stripping en inglés)
  es cuando los atacantes fuerzan el paso de una conexión segura (HTTPS)
  a una conexión no cifrada (HTTP),
  lo que facilita interceptar y alterar la data.

- **Suplantación de HTTP**: (HTTP spoofing en inglés)
  es el intento de los atacantes de engañar a los usuarios
  haciéndoles creer que se están conectando a un sitio web seguro
  que utiliza HTTPS, cuando en realidad,
  la conexión no es segura ni está cifrada.

- **Hombre en el navegador**: (Man-in-the-browser en inglés)
  es cuando los atacantes se aprovechan de las vulnerabilidades
  de los navegadores para alterar contenido,
  afectar comportamientos y apropiarse de información.

- **Suplantación de IP**: (IP spoofing en inglés)
  es cuando los atacantes crean paquetes de protocolo de Internet (IP)
  con direcciones fuente falsas para hacerse pasar por *hosts* legítimos,
  recibiendo cada uno de los paquetes de red de las víctimas
  y obteniendo así acceso a información privada.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/red-team/"
title="Inicia ahora con la solución de red teaming de Fluid Attacks"
/>
</div>

## ¿Cómo detectar un ataque man-in-the-middle?

Puede ser difícil,
pero reconocer un ataque MITM suele ser una combinación
de conocimiento técnico y medidas de seguridad, así como la debida cautela.
En el caso de que un usuario vea algo
fuera de lo común que llama su atención
(podría ser un anuncio, una ventana emergente,
un error ortográfico o un enlace no solicitado),
lo mejor que puede hacer es cerrar todas las pestañas
y desconectarse de la Internet.
Desconectarse de una red si el navegador web no comienza con HTTPS.
Realizar un escaneo exhaustivo con antivirus
y antimalware en el *software* sospechoso.
Si el ataque se produce en una red local conocida
es necesario cambiar inmediatamente la contraseña Wi-Fi
y activar la encriptación WPA3.

En las organizaciones deben tener un plan proactivo
que permita a sus equipos de ciberseguridad actuar con rapidez
ya que los datos de sus clientes, empleados
o socios podrían verse afectados.
Los [*red teams*](../../blog/ejercicio-red-teaming/) de las organizaciones
pueden hacer un gran trabajo cuando se fijan objetivos específicos,
y si la organización se toma en serio su seguridad,
no se quedarán de brazos cruzados esperando el ataque,
implementarán controles de seguridad
que puedan detectar ataques como el MITM,
y buscan continuamente puntos débiles en los sistemas informáticos.

## Cómo mitigar y prevenir un ataque man-in-the-middle

Reducir los riesgos de un ataque MITM requiere
unir técnicas de seguridad y buenas prácticas
para proteger la comunicación y la privacidad de datos.
A continuación se ofrecen algunos consejos para prevenir
o mitigar los ataques man-in-the-middle:

- **Utilizar siempre HTTPS**: Solo utilizar conexiones HTTPS seguras
  y cifradas para la navegación y comunicación en línea.

- **Verificar los certificados SSL**: Comprobar siempre la validez
  de los certificados SSL al acceder a sitios web.
  Si se encuentra un error o advertencia SSL,
  abstenerse de continuar con la conexión.

- **Implementar la fijación de certificados**: Se trata de una función
  de seguridad que impone el uso de certificados SSL específicos
  para un sitio web concreto,
  dificultando a los atacantes el uso de certificados fraudulentos.

- **Usar una VPN**: Una red privada virtual (VPN) puede cifrar
  el tráfico de la Internet,
  haciendo más difícil para los atacantes interceptar datos en redes inseguras,
  como las redes Wi-Fi públicas, que deben evitarse a toda costa.

- **Actualizar regularmente el *software***: Mantener los sistemas operativos,
  *software* y dispositivos actualizados con los últimos parches de seguridad.
  Es muy fácil para los atacantes explotar *software* obsoleto.

- **La educación es clave**: Capacitar a todos
  los implicados para identificar y comprender
  los posibles peligros de la Internet.
  Enseñar a adoptar un comportamiento seguro en Internet
  puede prevenir ataques.

- **Comunicación segura por correo electrónico**: Utilizar herramientas
  de cifrado para correo electrónico,
  como PGP o S/MIME, para proteger la privacidad
  en la comunicación puede ser de gran ayuda.

- **Seguimiento e informes continuos**: Mediante pruebas proactivas,
  reforzando las defensas y proporcionando informes actualizados,
  las organizaciones pueden prevenir,
  identificar y mitigar los ataques MITM más fácilmente.

Se recomiendan otras buenas prácticas
como el uso de contraseñas seguras
y activar la autenticación de dos factores (2FA) en todas las cuentas,
no compartir información personal en línea,
como la dirección personal o el número de teléfono,
y mantenerse informado sobre las últimas ciberamenazas.

## Fluid Attacks vs. ataques man-in-the-middle

Nuestro [Hacking Continuo](../../servicios/hacking-continuo/)
puede incluir la simulación de ataques en escenarios reales,
incluidos los ataques MITM.
Si se recurre a *hackers* éticos para imitar posibles actos maliciosos,
puedes identificar debilidades y vulnerabilidades en tu red,
sistemas y aplicaciones que podrían ser explotados por atacantes MITM.
[Ejercicios del *red team*](../../soluciones/red-team/)
ponen a prueba tus capacidades de respuesta ante incidentes,
permitiéndole a tu organización practicar
cómo respondería y mitigaría los ataques MITM,
garantizando que los equipos de seguridad estén bien preparados.
Además,
las pruebas de seguridad continuas pueden fomentar
una cultura consciente de seguridad
y sensibilizar a todos los implicados en el proceso de desarrollo.
Fluid Attacks es una solución ideal para las organizaciones
que se toman la seguridad tan en serio como debe ser.
No te arriesgues a sufrir daños financieros o de reputación,
y [contáctanos](../../contactanos/) para empezar con
el [plan](../../planes/) que prefieras.
