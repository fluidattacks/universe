---
slug: learn/codigo-malicioso/
title: Código malicioso
date: 2023-09-01
subtitle: Conoce más sobre este ciber riesgo
category: attacks
tags: vulnerabilidad, riesgo, empresa, malware, ciberseguridad
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1693598533/learn/malicious%20code/cover_possible_effects_malicious_code.webp
alt: Foto por Isaac Quesada en Unsplash
description: Descubre qué es, ejemplos, posibles efectos, y cómo evitar su propagación.
keywords: Fluid Attacks, Codigo Malicioso, Ataques, Posibles Efectos, Pentesting, Hacking Etico
author: Wendy Rodriguez
writer: wrodriguez
name: Wendy Rodriguez
about1: Escritora y editora
source: https://unsplash.com/photos/U0apbBgkOeQ
---

¿Alguna vez has tenido un resfriado tan fuerte
que no puedes desempeñar tus funciones correctamente?
Esta incómoda situación también puede darse en un computador
u otro sistema informático, hasta dejarlo fuera de servicio.
La causa puede ser lo que se conoce como código malicioso o _malware_,
el cual puede adoptar muchas formas.
Al igual que un virus se infiltra y daña las células de un cuerpo,
un código malicioso puede infiltrarse e infectar sistemas y archivos.
Pero, ¿cuáles son los posibles efectos de un código malicioso?
Hay varias consecuencias inquietantes,
que pueden provocar daños permanentes y costosos.
Pero primero lo primero:

## ¿Qué es un código malicioso?

El código malicioso, también conocido como _malware_ o _programa maligno_,
es un tipo de _software_ desarrollado
con el fin de causar daños en dispositivos, redes, aplicaciones o sistemas,
y que puede tomar la forma de un virus, troyano, gusano,
[_ransomware_](../../../blog/ransomware/),
_adware_ o _spyware_, entre otros.
Los programas maliciosos suelen crearse
para obtener acceso no autorizado a información confidencial,
arrebatar datos, dañar archivos, alterar sistemas,
tomar el control de un dispositivo de forma remota o vulnerar una aplicación,
y los efectos indeseables que pueden acarrear.

## ¿Cuáles son los indicios de un ataque de código malicioso?

Antes de entrar en detalles sobre los posibles efectos desastrosos
de los ataques de código malicioso,
sería conveniente que reconocieras algunos de los primeros indicios
de que un _malware_ se está ejecutando en tus dispositivos.
Estos son algunos de esos indicadores:

- Lentitud a la hora de ejecutar _hardware_ o _software_
  o bloqueo de aplicaciones

- Dificultades para iniciar sesiones en cuentas

- Pérdida de acceso a archivos

- Recibir una nota de _ransomware_

- Documentos o fondos desaparecidos

- Inusuales ventanas emergentes o anuncios de _spam_

- Aumento excesivo en el uso de internet o consumo de los recursos del sistema

No te confíes si todo parece ir sobre ruedas con tu sistema;
la falta de noticias no siempre es una buena noticia.
La mejor forma de prevenir brechas de ciberseguridad
([como esta épica filtración](../../../blog/epik-hack/))
es estar respaldado por constantes pruebas de seguridad, que,
además de ser una valiosa estrategia preventiva,
es una oportunidad de aprendizaje para todos los implicados.

<div>
<cta-banner
buttontxt="Más información"
link="/es/soluciones/revision-codigo-fuente/"
title="Inicia ahora con la solución de revisión de código seguro
de Fluid Attacks"
/>
</div>

## ¿Qué daños puede causar un código malicioso?

El malware puede causar estragos una vez que se le ha dejado correr libremente,
sobre todo si la víctima no es consciente o no está preparada,
pero dependiendo del tipo de código malicioso y del método utilizado
([ingeniería social](../../../blog/social-engineering/),
explotación de vulnerabilidades, scripts maliciosos, etc.),
los daños pueden empezar a percibirse.
Por ejemplo, obtener acceso a un dispositivo y,
a partir de ahí, acceder a información privada como nombres de usuario,
contraseñas o números de tarjetas de crédito,
y robar esos datos, interrumpir las operaciones,
inutilizar las aplicaciones al ralentizarlas y,
si no se está preparado,
propagarse a otros dispositivos conectados a la red.

En el peor de los casos,
se sabe que un código malicioso puede tomar
el control de un dispositivo a distancia
y bloquear a la víctima o borrar archivos importantes,
así como reformatear discos duros o pedir un rescate bloqueando
el acceso a los sistemas hasta que se paguen sumas millonarias.
El daño causado dependerá siempre de la variedad de _malware_
que elija utilizar el atacante y de la intención del ataque.

## Ejemplos de ataques de código malicioso

El código malicioso se presenta en distintas variedades;
a continuación, algunos de los tipos más comunes:

- **Troyano**: Este _malware_ pretende camuflarse
  como un programa legítimo cuando, en realidad,
  es un virus que podría destruir datos,
  robarlos o dar acceso al ciberdelincuente a los sistemas comprometidos.

- **Spyware**: Creado para vigilar,
  registrar y recopilar información de una empresa o una persona,
  a menudo sin que la víctima lo sepa.
  Este _malware_ envía datos al atacante malicioso
  sin consentimiento del usuario.

- **Gusano**: Es un código malicioso que busca difundirse a través de una red.
  La propagación de este _malware_ puede provocar
  la caída de los sistemas de toda una empresa.
  Este es un reflejo de las fallas en seguridad
  que tienen los dispositivos afectados.

- **Adware**: También conocidos como _spam_,
  estos anuncios maliciosos pueden hacer que el rendimiento
  de un dispositivo se reduzca o que se muestren molestas ventanas emergentes,
  que pueden traer consigo virus más peligrosos ocultos en sus enlaces.

- **Ransomware**: Es un tipo de virus específico
  que toma como rehenes a archivos o sistemas enteros para pedir
  un rescate que la víctima debe pagar, usualmente en criptomoneda,
  antes de una fecha límite.

Todos estos tipos de _malware_ pueden tener diferentes consecuencias,
así que ¿cuáles son los posibles efectos de un código malicioso?

## Efectos que puede tener un código malicioso

En cuanto a las consecuencias de los ataques de código malicioso,
las posibilidades pueden ser muy perjudiciales
e incluso tener repercusiones importantes.
Su alcance estará sujeto al tipo de daño que quiera causar el atacante,
pero he aquí algunos efectos que podría tener sobre un usuario o empresa:

- **Robo de datos**: Los datos de la víctima podrían ser vendidos
  o utilizados para compras no deseadas o ilegales.
  Si lo que se tomaron fueron credenciales o identificaciones personales,
  pueden ser alteradas o reutilizadas para fines desconocidos,
  fraude económico o incluso espionaje empresarial.

- **Espionaje**: Escuchar a escondidas, vigilar o espiar,
  son situaciones en las que intervienen programas espía
  diseñados para vigilar en secreto,
  [grabar pulsaciones en el teclado](../../../blog/keylogging-keyloggers/),
  hacer capturas de pantalla o acceder a los micrófonos
  o cámaras de los computadores.

- **Interrupción de sistemas**: Un código malicioso
  puede saturar los recursos del sistema,
  provocando caídas o dificultades en el rendimiento,
  interrumpiendo el funcionamiento y la fluidez de una aplicación o de una red.

- **Secuestro de sistemas**: En los peores casos,
  cuando el ransomware se ejecuta y toma control,
  pueden producirse desde alteraciones irreversibles
  en los archivos de un sistema hasta interrupciones
  en la prestación de un servicio nacional
  (p. ej., [Colonial Pipelines](../../../blog/pipeline-ransomware-darkside/)).

- **Datos alterados o eliminados**: La pérdida de documentación,
  las vulnerabilidades en el sistema o las fallas en las aplicaciones
  pueden ser el resultado de diferentes tipos de códigos maliciosos
  que alteran, corrompen o borran datos del sistema contagiado.

Además de lo mencionado anteriormente sobre el posible efecto
de los ataques de código malicioso,
cabe destacar que las secuelas afectarán tanto a la seguridad como al negocio.
La reputación de tu empresa puede quedar dañada definitivamente,
algo que también afectará a su competitividad en el mercado
y tal vez abra la puerta a repercusiones legales.

## ¿Cómo consigue un código malicioso acceder a un sistema?

El internet es una puerta de entrada para que
los ciberdelincuentes intenten acceder a información privada,
aplicaciones o dispositivos.
Eres vulnerable a cualquier ataque cada vez que entras en internet,
por ejemplo cuando accedes a tu cuenta de correo electrónico,
podrías abrir un _malspam_,
o descargar aplicaciones de fuentes desconocidas, visitar
(sin que tú lo sepas) sitios _web_ pirateados
o hacer clic en anuncios o enlaces maliciosos.
El _malware_ también podría propagarse a través de dispositivos extraíbles,
como una unidad de memoria USB.

## ¿Cómo evitar la propagación de códigos maliciosos?

La mejor forma de prevenir los ataques de código malicioso
es adoptar una actitud proactiva.
Existen formas sencillas de protegerse contra el _malware_:

<image-block>

!["Evitar la propagación de códigos maliciosos"](https://res.cloudinary.com/fluid-attacks/image/upload/v1709824766/airs/es/learn/im%C3%A1genes%20traducidas/Evitar_propagacion_codigos_maliciosos1.webp)

Evitar la propagación de códigos maliciosos.

</image-block>

Las herramientas automatizadas y la experiencia humana
pueden ayudar a prevenir los ataques de los actores maliciosos,
que en este momento no muestran signos de desaceleración
y van por todo lo que pueden conseguir.
El informe
[Verizon’s 2023 Data Breach Investigations Report](https://www.verizon.com/business/en-gb/resources/reports/dbir/)
señala que el 83% de las brechas son ejecutadas por actores externos
que no sólo buscan una retribución monetaria,
sino también causar daños y ganar popularidad.
Abordar la ciberseguridad con la combinación correcta de personas
([_hackers_ éticos](../../soluciones/hacking-etico/)
o [_pentesters_](../../soluciones/pruebas-penetracion-servicio/))
y tecnología (escáneres o herramientas)
garantizará una protección integral contra
los ataques de código malicioso.

La revisión del código fuente podría ayudarte en esta situación,
así que [contáctanos](../../contactanos/) si quieres más información
sobre este u otros servicios,
como [Hacking Continuo](../../servicios/hacking-continuo/).
[Haz clic aquí](https://app.fluidattacks.com/SignUp)
para probar
nuestro plan Essential gratis por **21 días**.
