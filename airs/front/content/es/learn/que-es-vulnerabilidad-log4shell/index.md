---
slug: learn/que-es-vulnerabilidad-log4shell/
title: Log4Shell
date: 2023-09-22
subtitle: Conoce esta vulnerabilidad de Log4j y cómo remediarla
category: ataques
tags: vulnerabilidad, ciberseguridad, exploit, software, pentesting
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1695342910/learn/log4j-log4shell/cover_log4j_log4shell_1.webp
alt: Foto por Egor Myznik en Unsplash
description: Fluid Attacks te ayuda a aprender sobre la vulnerabilidad Log4Shell, su historia, cómo detectarla y su remediación.
keywords: Fluid Attacks, Remediación, Remediar, Log4j, Vulnerabilidades, Log4shell, Gestion De Vulnerabilidades, Pentesting, Hacking Etico
author: Wendy Rodriguez
writer: wrodriguez
name: Wendy Rodriguez
about1: Escritora y editora
source: https://unsplash.com/photos/JXQLlgS94tI
---

## ¿Cuál es la vulnerabilidad de Log4j?

¿Alguna vez has oído hablar o has encontrado una "grieta" en el sistema?
¿Un vacío legal que permite eludir una ley?
Estos se conocen como brechas.
Se puede encontrar una brecha en casi todos los campos
en los que interviene el ser humano,
aguardando a ser encontrada y explotada o arreglada.
[CVE-2021-44228](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-44228),
o vulnerabilidad Log4Shell, es una de ellas,
una brecha peligrosa en las medidas de seguridad de un sistema,
a la cual es necesario hacer frente para mantener su integridad
y protegerlo de posibles ciberataques.
La vulnerabilidad Log4Shell se encuentra en la herramienta de registro Log4j,
la cual es un componente de _software_ extensamente utilizado
en todo el mundo por millones de aplicaciones,
lo que convierte a Log4Shell en una de las vulnerabilidades
de mayor gravedad de todos los tiempos.

La vulnerabilidad Log4j conocida como "Log4Shell"
permite una crítica ejecución remota de código
([RCE](../../../blog/close-invisible-doors/))
en la biblioteca de registro Apache Log4j versiones 2.0-beta9 a 2.14.1.
Fue revelada por primera vez en diciembre de 2021,
y se considera una de las
[vulnerabilidades de _software_ más severas de los últimos años](../../../blog/log4shell/).
Los atacantes son capaces de ejecutar código Java arbitrario en sistemas
que utilicen versiones vulnerables de la biblioteca Apache Log4j
gracias a la falla de ejecución remota de código Log4Shell,
lo que, a su vez, permite a ellos tomar el control de servidores o sistemas,
robar datos o instalar _malware_.

## ¿Qué es Log4j?

Log4j fue creado para responder a la necesidad de una API
flexible y configurable para aplicaciones Java.
Se desarrolló como un marco de registro de código abierto y,
tras varias mejoras,
evolucionó hasta convertirse en el Log4j que conocemos hoy.
Su distribuidor, [Apache Software Foundation](https://logging.apache.org/log4j/2.x/),
publicó esta utilidad en 2001.
Es muy utilizada debido a su flexibilidad,
ya que se adapta a una amplia gama de aplicaciones
con diferentes requisitos de registro;
también por su rendimiento,
ya que busca minimizar el impacto que generan un gran número de mensajes
de registro en las aplicaciones.
Otras razones incluyen su facilidad de uso para los desarrolladores
ya que ofrece una gran variedad de opciones de configuración,
también es un proyecto de código abierto,
lo que permite que cualquiera pueda utilizarlo y contribuir a él.

## ¿Qué es Log4j2?

Log4j2 es un marco de registro para aplicaciones Java
y fue lanzado como una actualización de Log4j con varias mejoras,
entre ellas:

- **Rendimiento**: Log4j2 es significativamente más rápido que Log4j,
  especialmente en aplicaciones multiproceso.

- **Flexibilidad**: Log4j2 proporciona una gama
  más amplia de opciones de configuración,
  lo que le permite a los usuarios adaptar el sistema
  de registro a sus necesidades.

- **Seguridad**: Log4j2 incluye una serie de características de seguridad,
  como una configuración para utilizar un listado
  de niveles de registro permitidos.

- **Apoyo de la comunidad**: Log4j2 es un marco de registro popular,
  hay una gran comunidad de usuarios y desarrolladores que proporcionan apoyo,
  responden preguntas, añaden características y comparten correcciones.

## ¿Cuándo se descubrió Log4Shell?

La vulnerabilidad se explotó por primera vez
en el popular servicio de juegos Minecraft
al enviar una sola línea de texto malicioso al chat del juego,
permitiendo que los servidores del juego se vieran comprometidos,
pero no fue hasta noviembre de 2021 que Chen Zhaojun,
un investigador de seguridad en Alibaba,
informó de la vulnerabilidad al Apache Software Foundation.
Ya se estaba discutiendo en las comunidades de seguridad informática,
y, además,
se hizo pública una demostración de cómo explotar esta vulnerabilidad.
El **9 de diciembre de 2021**,
Apache informó de una vulnerabilidad de día cero,
que inicialmente se difundió de forma privada.
El 10 de diciembre
Log4Shell se divulgó públicamente y se lanzó un parche inicial.
Ese mismo día
CVE publicó su primer informe al respecto,
llamándolo CVE-2021-44228 y proporcionando actualizaciones casi diarias
sobre hallazgos y correcciones.

<div>
<cta-banner
buttontxt="Mas información"
link="/es/soluciones/gestion-vulnerabilidades/"
title="Inicia ahora con la solución de gestión de vulnerabilidades
de Fluid Attacks"
/>
</div>

## ¿Por qué Log4Shell fue tan crítico?

Java es uno de los principales lenguajes de programación
que se utiliza para muchos sistemas _backend_,
desde servidores de pequeñas empresas
sistemas de automatización y dispositivos IoT.
La mayoría de estos sistemas implementan el registro por diferentes medios.
La forma más sencilla de hacerlo ha sido utilizar la biblioteca Log4j.
Después de que se informara de que la biblioteca contenía una vulnerabilidad,
muchos lo declararon una brecha crítica ya que:

- Log4Shell, al ser una vulnerabilidad de día cero,
  fue algo que los cibercriminales aprovecharon
  antes de que nadie lo supiera.
  Las pruebas indican que los atacantes la explotaron inicialmente
  unos días antes de su anuncio público.

- Alrededor del 40% de todas las redes del mundo
  pudieron ser vulnerables a las fallas de Log4j.
  En ese momento, los servicios comerciales más afectados fueron
  Amazon Web Services, iCloud, Cloudflare, Minecraft:
  Java Edition, Tencent QQ, Steam y muchos otros.
  El [Washington Post](https://www.washingtonpost.com/technology/2021/12/20/log4j-hack-vulnerability-java/)
  lo llamó "la brecha de seguridad más grave de la historia"
  y se dice que las consecuencias se prolongarán durante años.

- Java también se utiliza en equipos para fábricas, gobiernos
  servicios, hospitales y otros dispositivos personalizados.
  Se sabe que la biblioteca Log4j es utilizada por estos dispositivos así como
  como computadores y servidores convencionales.

- La conocida plataforma de escritorio y aplicaciones VMware Horizon
  fue muy explotada, recibiendo una gran cantidad de ataques
  que lograron vulnerarla.

- Esto es una hipótesis, pero Log4j es una librería de código abierto
  que puede ser utilizada por muchos desarrolladores
  que podrían no seguir las reglas de licenciamiento y no informar de su uso.
  Es decir, Log4j podría haber sido implementado en aplicaciones
  que no saben que lo están usando,
  haciéndolas vulnerables sin que los usuarios o propietarios lo sepan.

## ¿Cómo funciona Log4shell?

La vulnerabilidad explota una característica de Log4j llamada JNDI lookup,
que es un mecanismo que permite a las aplicaciones Java
buscar recursos en servidores remotos.
Log4j utiliza la búsqueda JNDI para resolver patrones de mensajes de registro.
Log4Shell se produce cuando Log4j está configurado
para utilizar JNDI lookup para resolver patrones de mensajes
de registro que contienen URL LDAP maliciosas.
El atacante tendría que enviar un mensaje de registro
especialmente diseñado a un sistema vulnerable,
y cuando Log4j intente resolver estas URL,
se conectará al servidor LDAP del atacante y ejecutará la carga maliciosa.
Dicho mensaje puede ser cualquier cosa que el atacante desee,
desde un simple comando "shutdown -r" hasta código más complejo
que permita al atacante obtener el control del sistema objetivo.

## ¿Cómo detectar la vulnerabilidad de Log4Shell?

Log4Shell merece toda la atención de organizaciones y desarrolladores,
y ahora que ha pasado algún tiempo y se ha comprendido mejor,
existen formas eficaces de detectar las vulnerabilidades de Log4j.
Las útiles herramientas de escaneo pueden descubrirlas
al recorrer un sistema y buscar vulnerabilidades conocidas de Log4j.
Incluso hay herramientas, algunas gratuitas,
diseñadas para buscar vulnerabilidades de Log4j,
como [Palantir's Log4j-sniffer](https://github.com/palantir/log4j-sniffer)
y el escáner [CERT Coordination Center](https://github.com/CERTCC/CVE-2021-44228_scanner),
entre otras opciones.
Fluid Attacks ofrece su técnica de análisis de composición de _software_
([SCA](../../producto/sca/))
que escanea y descubre riesgos de ciberseguridad relacionados
con componentes de código abierto y de terceros, como Log4j
(pruebalo gratuitamente durante [21 días](https://app.fluidattacks.com/SignUp)
y descubre esta técnica de evaluación).
Las herramientas y técnicas facilitan la tarea,
pero los equipos de seguridad, como los
[_hackers_ éticos](../../blog/que-es-hacking-etico/) o _pentesters_,
podrían ir más allá de las herramientas de escaneo automatizado,
ya que Log4Shell se esconde profundamente en las cadenas de dependencia,
lo cual se puede atacar con un método de mayor eficacia
como lo es [_pentesting_](../../soluciones/pruebas-penetracion-servicio/).

Las pruebas manuales son ejecutadas por _hackers_ éticos o _pentesters_
que son más que capaces de buscar Log4Shell profundamente oculto,
y pueden ayudar su causa con herramientas como [Apache Maven]
(<https://maven.apache.org/>)
para generar árboles de dependencias
y mapear todas las dependencias de una aplicación.
Otras pautas útiles son las de la
Agencia de Ciberseguridad y Seguridad de Infraestructuras (CISA)
llamadas "Apache Log4j vulnerability
[guidance](https://www.cisa.gov/news-events/news/apache-log4j-vulnerability-guidance)"
y también [la lista](https://github.com/cisagov/log4j-affected-db)
de _software_ afectado por Log4Shell.

## ¿Cómo remediar esta vulnerabilidad de Log4j?

Remediar o mitigar la vulnerabilidad Log4Shell
es el siguiente paso lógico tras detectarla,
y dependiendo de las circunstancias, pueden aplicarse soluciones.
Para remediarla,
se debe actualizar la biblioteca Log4j a una versión parcheada
que solucione la vulnerabilidad específica.
La actualización a la versión Log4j
2.17.10 (Java 8), 2.12.4 (Java 7) y 2.3.2 (Java 6)
corregirá CVE-2021-44228 y otras vulnerabilidades.
Si se consulta
el [sitio web oficial de Log4j](https://logging.apache.org/log4j/2.x/manual/index.html)
o los avisos de seguridad pueden ayudar a encontrar
si hay una vulnerabilidad existente en una versión específica.
Después de actualizar la biblioteca Log4j,
la configuración de Log4j de la aplicación debe ser revisada
para asegurarse que sigue buenas prácticas de seguridad.
Eliminar registros innecesarios o excesivos también puede ayudar,
así como auditar el contenido de los mensajes de registro
para evitar la inclusión de datos no fiables o,
lo que podría llegar a ser, datos maliciosos.

En los casos en que la actualización
a la nueva versión de la biblioteca Log4j
no sea una posibilidad, aprender a mitigar
la vulnerabilidad es la mejor opción:

- Deshabilitar las búsquedas de mensajes JNDI en aplicaciones vulnerables.
  Al deshabilitar manualmente las sustituciones de búsqueda de mensajes
  en la configuración de Log4j,
  se evita exitosamente una ejecución remota de código por parte del atacante.

- Utilizar un _firewall_ de aplicaciones web (WAF)
  para bloquear el tráfico malicioso,
  lo que ayuda a bloquear protocolos de uso habitual como RMI o LDAP.
  Las direcciones IP vinculadas a ataques también deben
  bloquearse en cuanto se identifiquen.

- Implementar mecanismos de supervisión y detección
  para identificar mensajes de registro sospechosos o maliciosos.
  También se debería incluir la supervisión de actividad inusual,
  como la creación de nuevos procesos o archivos,
  o cambios en la configuración del sistema.

Un panel único en el que se puedan gestionar las vulnerabilidades ayuda mucho.
Una plataforma estratégicamente organizada que evalúe la exposición al riesgo,
que promueve y controle el procedimiento de remediación,
compruebe el progreso y el estado de la seguridad de un sistema,
proporcione informes actualizados
y ofrezca asistencia a través de chat en directo.
No es mucho pedir porque aquí
en Fluid Attacks es exactamente lo que ofrecemos.
Nuestra [plataforma](../../plataforma/),
en la que nuestra solución de [gestión de vulnerabilidades](../../soluciones/gestion-vulnerabilidades/)
cobra vida, puede ayudar eficazmente en situaciones
en las que Log4Shell está presente, así como en muchas más.

Es importante tener en cuenta que las vulnerabilidades de Log4j
pueden ser descubiertas a lo largo del tiempo,
por lo que es esencial mantenerse informado
sobre los últimos avisos de seguridad
y las buenas prácticas para proteger aplicaciones.

Recomendamos a nuestros clientes que consulten
[esta](https://help.fluidattacks.com/portal/en/kb/articles/criteria-fixes-java-091#Compliant_code)
guía de remediación para comprender cómo prevenir inyecciones de logs.

## Gestionar vulnerabilidades con Fluid Attacks

Necesitas mantener protegidas tus dependencias de código abierto.
Pero saber qué aplicaciones utilizan Log4j y, por tanto,
son vulnerables es una tarea que lleva mucho tiempo.
Nosotros te ayudamos a resolver esta cuestión con rapidez y precisión.
La combinación de software de escaneo avanzado
y nuestro equipo de *pentesters*
optimiza la identificación y notificación de problemas como Log4shell
u otras vulnerabilidades de día cero.
Esto forma parte de la solución
de [gestión de vulnerabilidades](../../soluciones/gestion-vulnerabilidades/)
de Fluid Attacks que, tras la detección de vulnerabilidades,
facilita a los clientes la revisión,
categorización y priorización de los datos,
lo que permite a todos los implicados remediar
las vulnerabilidades con mayor rapidez.
Uno de los beneficios que ofrecemos con nuestro
[Hacking Continuo](../../servicios/hacking-continuo/)
consiste en que nuestros clientes
puedan evaluar sus aplicaciones constantemente
y por lo tanto son conscientes de si están utilizando
una biblioteca de terceros que pueda tener
una vulnerabilidad conocida recientemente.

[Contáctanos](../../contactanos/) ahora para comenzar
con nuestras soluciones y aprovecha
nuestro [plan Advanced](../../planes/),
el cual te garantiza una mayor protección contra los ciberataques.
