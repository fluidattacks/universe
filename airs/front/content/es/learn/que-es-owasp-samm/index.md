---
slug: learn/que-es-owasp-samm/
title: OWASP SAMM
date: 2023-11-17
subtitle: El marco OWASP para lograr madurez en seguridad
category: filosofía
tags: riesgo, web, software, tendencia, ciberseguridad, cumplimiento
image: https://res.cloudinary.com/fluid-attacks/image/upload/v1700256635/learn/owasp%20samm/cover_owasp_samm_1.webp
alt: Foto por Sajad Nori en Unsplash
description: Conoce el marco OWASP SAMM para formular y aplicar una estrategia proactiva para alcanzar la madurez en seguridad de software.
keywords: Fluid Attacks, Owasp, Samm, Proyecto Open Worldwide Application Security, Modelo De Madurez De Aseguramiento De Software, Cumplimiento, Web, App, Seguridad, Hacking Etico, Pentesting
author: Wendy Rodriguez
writer: wrodriguez
name: Wendy Rodriguez
about1: Escritor y editor
source: https://unsplash.com/photos/person-holding-black-and-white-round-ornament-21mJd5NUGZU
---

## OWASP SAMM

En el mundo digital actual
el *software* es una parte esencial de casi todos los negocios.
Como resultado, es cada vez más importante para las organizaciones
asegurarse de que su *software* sea seguro.
OWASP es una comunidad renombrada en seguridad de *software* que,
con proyectos como el marco OWASP SAMM,
proporciona a las organizaciones una ayuda cuyo objetivo es
alcanzar el objetivo final: un *software* seguro.

## ¿Qué es OWASP?

OWASP (Open Worldwide Application Security Project)
es una organización sin ánimo de lucro que se centra
en la optimización de la seguridad de los sistemas de *software*.
OWASP proporciona varios recursos
para ayudar a los desarrolladores de *software*
y a los profesionales de la seguridad a entender
y abordar las vulnerabilidades de seguridad más comunes en el *software*.
Estos recursos incluyen
el [OWASP ASVC](https://help.fluidattacks.com/portal/en/kb/articles/criteria-compliance-owaspmasvs),
un marco de requisitos de seguridad,
y
el [OWASP Top Ten](https://help.fluidattacks.com/portal/en/kb/articles/criteria-compliance-owasprisks/),
una lista de los riesgos más críticos para la seguridad de las aplicaciones web,
junto con una serie de herramientas
y directrices para el desarrollo de *software* de seguridad.
OWASP también organiza conferencias y eventos de capacitación en todo el mundo
para promover la importancia de la seguridad de *software*.

## ¿Qué es OWASP SAMM?

Otro recurso importante de OWASP
es el marco [SAMM](https://help.fluidattacks.com/portal/en/kb/articles/criteria-compliance-osamm/)
(Software assurance maturity model
o modelo de madurez de aseguramiento de *software*),
que ayuda a las organizaciones a formular e implementar estrategias de seguridad
adaptadas a sus riesgos, limitaciones y objetivos específicos.
Proporciona a las organizaciones una forma eficaz y cuantificable de
evaluar sus programas de seguridad de *software*
y mejorar sus prácticas de seguridad.

## ¿Por qué es importante OWASP SAMM?

OWASP SAMM se centra en definir y optimizar la postura de una organización
enfatizando un enfoque equilibrado y orientado al riesgo.
SAMM es importante para las organizaciones que desarrollan *software*
porque les ayuda a establecer y priorizar sus esfuerzos de seguridad
y garantizar que su *software* es seguro
durante todo el ciclo de desarrollo.
Al ser un marco de código abierto
puede personalizarse para adaptarse
a las necesidades específicas de las distintas organizaciones.
Este marco puede ofrecer orientación a las organizaciones
para crear estrategias y alcanzar sus objetivos de madurez a través de:

- Identificar y evaluar los riesgos de seguridad del *software*

- Desarrollar e implementar controles eficaces de seguridad del *software*

- Medir y hacer un seguimiento del progreso en la mejora
  de la seguridad del *software*

Mediante el uso de SAMM,
las organizaciones pueden adoptar un enfoque más proactivo de la seguridad,
y, por lo tanto, reducir el riesgo de violaciones de seguridad
y otras vulnerabilidades del *software*.

## OWASP SAMM v2

Publicado en 2020,
SAMM v2 es la última versión de este marco
e incluye una serie de nuevas características y mejoras:

- Una nueva función de negocio llamada **Implementación**.
  Esta función representa una serie de actividades básicas
  en los dominios de creación y despliegue de una organización.

- Un [modelo de madurez](https://owaspsamm.org/model/) revisado,
  que está más alineado con los últimos estándares de seguridad
  y las buenas prácticas.

- Un [SAMM Toolbox](https://owaspsamm.org/assessment/) actualizado
  que incluye una serie de nuevas herramientas y recursos.

- Una nueva iniciativa [SAMM Benchmark](https://owaspsamm.org/benchmark/)
  que proporciona información sobre la madurez
  y avances en comparación con los de organizaciones similares.

SAMM v2 puede ser utilizado por organizaciones de todos los tamaños y sectores.
El modelo puede utilizarse para evaluar,
formular y aplicar una estrategia de seguridad de *software*.
Sus beneficios incluyen la mejora de la postura de seguridad del software
y el cumplimiento de los estándares de seguridad,
mayor confianza de los clientes y menor riesgo de violaciones.

## Modelo de madurez OWASP SAMM

Las 15 prácticas de seguridad que se encuentran en SAMM v2
se agrupan dentro del marco
como cinco áreas esenciales de funciones empresariales.
Son **gobernanza**, **construcción**, **implementación**,
**verificación** y **operaciones**,
y representan las actividades básicas que las organizaciones deberían realizar
para garantizar la seguridad de su *software*.

- **Gobernanza:** La función empresarial de gobernanza es responsable
  de establecer y mantener la política
  general de seguridad del software de la organización.
  Esto incluye definir los riesgos de la organización
  y el establecimiento de estándares de seguridad,
  asignando roles y responsabilidades,
  y supervisar la implementación de la estrategia
  de seguridad de la organización
  para alinearla con el objetivo general de la empresa.
  Sus prácticas esenciales en materia de seguridad
  son **estrategia y métricas**, **política y cumplimiento**,
  y **educación y orientación**.

- **Construcción**: La función empresarial de la construcción es responsable
  de garantizar que la seguridad del *software*
  a lo largo del ciclo de vida de desarrollo de *software* (SDLC).
  Esto incluye la identificación y evaluación de los requisitos de seguridad,
  el diseño de arquitecturas seguras y la elaboración de modelos de amenazas.
  Sus prácticas esenciales en materia de seguridad
  son la **evaluación de amenazas**, **requisitos de seguridad**,
  y **arquitectura segura**.

- **Implementación**: La función empresarial de implementación es responsable
  de desplegar y mantener *software* seguro.
  Esto incluye la creación y el despliegue de *software* seguro,
  gestionar y supervisar las configuraciones de seguridad,
  y recibir retroalimentación.
  Sus prácticas de seguridad clave son **construcción segura**,
  **despliegue seguro** y **gestión de defectos**.

- **Verificación**: La función empresarial de verificación es responsable
  de garantizar que se cumplen los requisitos de seguridad del *software*.
  Esto incluye la realización de pruebas y auditorías de seguridad,
  realizar revisiones de código y verificar que los controles de seguridad
  están implementados y en funcionamiento.
  Sus prácticas clave en materia de seguridad son
  la **evaluación de la arquitectura**,
  las **pruebas basadas en requisitos** y las **pruebas de seguridad**.

- **Operaciones**: La función de operaciones es responsable de
  responsable de supervisar y gestionar los riesgos de seguridad
  a lo largo del SDLC.
  Esto incluye la identificación y evaluación de los riesgos operativos,
  implementar controles de seguridad y gestionar los incidentes detectados.
  Sus prácticas clave de seguridad son la **gestión de incidentes**,
  **gestión del entorno** y **gestión operativa**.

Toda práctica clave de seguridad tiene dos actividades asociadas,
cada una de las cuales forma parte de un flujo diferente.
Ambas corrientes tienen objetivos alcanzables que pueden lograrse
a niveles de madurez superiores.
El modelo SAMM es el siguiente:

<div class="imgblock">

![SAMM Overview](https://res.cloudinary.com/fluid-attacks/image/upload/v1700259462/learn/owasp%20samm/samm_overview.webp)

<div class="title">

Resumen de OWASP SAMM(https://owaspsamm.org/release-notes-v2/)
(tomado de [aquí](https://owaspsamm.org//img/pages/SAMM_v2_overview.svg)).

</div>

</div>

### Niveles de madurez OWASP SAMM

OWASP SAMM establece cuatro niveles de madurez (0 a 3)
para cada práctica de seguridad y función empresarial.
Los niveles representan la evolución de
programa de seguridad de *software* de una organización,
que van desde un estado cero con prácticas de seguridad mínimas
(nivel 0) a un estado optimizado y bien definido (nivel 3).

- Nivel 0 - Inactivo: prácticas de seguridad nulas o mínimas

- Nivel 1 - Inicial: prácticas de seguridad puntuales

- Nivel 2 - Definido: aumento notable;
  prácticas de seguridad definidas y documentadas

- Nivel 3 - Dominio: prácticas de seguridad medidas cuantitativamente
  y mejoradas de forma continua.

La parte de evaluación de este proceso de verificación
es una forma segura de saber en qué punto se encuentra cada organización.
La fundación OWASP proporciona herramientas de evaluación que
son gratuitas y fáciles de entender,
con un [cuestionario] (https://docs.google.com/spreadsheets/d/1jmLVltRhuG19AX5cLUcWH1Qox2Uic17rD29gMVG5zDE/view#gid=1716553355)
completo que revisa cada una de las funciones empresariales y sus prácticas,
proporcionando una puntuación exhaustiva
que se puede utilizar como punto de partida.
La evaluación periódica
y los esfuerzos de mejora continua ayudan a las organizaciones a
progresar a través de los niveles de madurez
y mejorar su postura general de seguridad de *software*.

<div>
<cta-banner
buttontxt="Leer más"
link="/es/soluciones/pruebas-seguridad/"
title="Inicia ahora las pruebas de seguridad de Fluid Attacks"
/>
</div>

## Cómo alcanzar un alto nivel de madurez

Alcanzar el nivel de madurez más alto en OWASP SAMM
requiere un enfoque integral y proactivo.
Implica establecer una cultura de mejoramiento continuo,
integrar la seguridad en todos los aspectos del SDLC,
y aprovechar la automatización y las prácticas de seguridad avanzadas.
He aquí una guía detallada para alcanzar el máximo nivel de madurez:

1. Establecer una cultura de mejora continua: Fomentar una
   mentalidad consciente de la seguridad en toda la organización,
   haciendo hincapié en la importancia de la seguridad en todos los aspectos
   del desarrollo y el despliegue del *software*.
   Fomentar y apoyar la formación y el aprendizaje continuos
   a la vez que se promueve la comunicación abierta y la colaboración entre
   los equipos de seguridad y todos los departamentos implicados en el SDLC.

2. Integrar la seguridad en el SDLC: Incorporar las actividades
   de seguridad en todas las fases del SDLC,
   implementar un proceso de modelado de amenazas para identificar, analizar
   y priorizar las amenazas potenciales al *software*,
   y utilizar herramientas como [SAST](../../producto/sast/)
   o [DAST](../../producto/dast/)
   para identificar y remediar vulnerabilidades antes de su despliegue.
   [Pentesting](../../blog/importancia-pentesting/)
   puede evaluar la postura de seguridad
   identificando puntos débiles adicionales.
   Cuando esta tarea es ejecutada por *hackers* éticos certificados,
   el resultado será una menor tasa de falsos positivos.

3. Aprovechar la automatización y las prácticas de seguridad avanzadas:
   Implementar *pipelines*
   de integración continua y entrega continua (CI/CD)
   con controles de seguridad integrados
   y [escaneo de vulnerabilidades](../../blog/escaneo-de-vulnerabilidades/)
   automatizados.
   Utilizar la infraestructura como código (IaC) para automatizar
   y asegurar el aprovisionamiento y las configuraciones de la infraestructura,
   así como soluciones de seguridad basadas en la nube.

4. Supervisión y mejora continuas: Establecer una supervisión continua
   de los registros de seguridad,
   el tráfico de red y el comportamiento del sistema
   para detectar anomalías y posibles vulnerabilidades.
   Implementar reportes que comuniquen frecuentemente el estado del *software*,
   incidentes y evaluaciones.

## Cómo encaja Fluid Attacks en el plan OWASP SAMM

Para nosotros está claro que alcanzar el máximo nivel de madurez
en OWASP SAMM es un trayecto continuo, no un destino.
Fluid Attacks puede proporcionar diferentes herramientas
y soluciones para llevar a tu organización
hacia la madurez en su postura de seguridad.

OWASP SAMM define
[pruebas de seguridad](../../soluciones/pruebas-seguridad/)
como una práctica fundamental
para verificar la efectividad de los controles de seguridad
e identificar vulnerabilidades en diferentes sistemas.
Con Fluid Attacks, las pruebas de seguridad están garantizadas
en todas las fases del SDLC.

Además de las pruebas de seguridad
contamos con un equipo
de [hacking ético](../../soluciones/hacking-etico/)
[altamente certificado](../../../certifications/)
que realiza [pruebas de penetración](../../soluciones/pruebas-penetracion-servicio/)
manuales para el descubrimiento
y análisis detallado de vulnerabilidades ocultas.

También puedes contar con una única [plataforma](../../plataforma/)
que proporciona detalles sobre los hallazgos,
su naturaleza interactiva permite a los usuarios estar al día
y planificar una rápida remediación.

Comienza con una [prueba gratuita de 21 días](https://app.fluidattacks.com/SignUp)
para la evaluación de vulnerabilidades
a través de nuestra herramienta automatizada.
Si deseas más información
no dudes en [contactarnos](../../contactanos/).
