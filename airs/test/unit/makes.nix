{ inputs, makeScript, ... }: {
  jobs."/airs/test/unit" = makeScript {
    entrypoint = ./entrypoint.sh;
    name = "airs-unit-test";
    searchPaths.bin = [
      inputs.nixpkgs.nodejs_20
      inputs.nixpkgs.gnugrep
      inputs.nixpkgs.gnused
      inputs.nixpkgs.utillinux
      inputs.nixpkgs.autoconf
      inputs.nixpkgs.bash
      inputs.nixpkgs.binutils.bintools
      inputs.nixpkgs.gcc
      inputs.nixpkgs.gnumake
      inputs.nixpkgs.python311
    ];
  };
}
