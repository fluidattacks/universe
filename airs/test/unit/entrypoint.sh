# shellcheck shell=bash

function main {
  : \
    && pushd airs/front \
    && rm -rf node_modules \
    && npm clean-install --ignore-scripts \
    && npm run test \
      -- \
      "${@}" \
    && popd \
    || return 1
}

main "${@}"
