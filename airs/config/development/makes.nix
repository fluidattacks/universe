{ inputs, isDarwin, makeScript, outputs, projectPath, ... }:
let
  libcPackage = if isDarwin then inputs.nixpkgs.clang else inputs.nixpkgs.musl;
in {
  jobs."/airs/config/development" = makeScript {
    replace.__argAirsSecrets__ = projectPath "/airs/secrets";
    entrypoint = ./entrypoint.sh;
    name = "airs-config-development";
    searchPaths = {
      rpath = [ libcPackage ];
      bin = [
        inputs.nixpkgs.findutils
        inputs.nixpkgs.gnugrep
        inputs.nixpkgs.gnused
        inputs.nixpkgs.nodejs_20
        inputs.nixpkgs.utillinux
        inputs.nixpkgs.autoconf
        inputs.nixpkgs.bash
        inputs.nixpkgs.binutils.bintools
        inputs.nixpkgs.gcc
        inputs.nixpkgs.gnumake
        inputs.nixpkgs.python311
      ] ++ inputs.nixpkgs.lib.optionals inputs.nixpkgs.stdenv.isDarwin [
        (inputs.makeImpureCmd {
          cmd = "open";
          path = "/usr/bin/open";
        })
      ];
      source = [ outputs."/common/utils/aws" outputs."/common/utils/sops" ];
    };
  };
}
