{ inputs, isDarwin, makeScript, outputs, projectPath, __nixpkgs__, makeTemplate
, ... }:
let
  libcPackage = if isDarwin then inputs.nixpkgs.clang else inputs.nixpkgs.musl;
in {
  jobs."/airs/build" = makeScript {
    replace.__argAirsSecrets__ = projectPath "/airs/secrets";
    entrypoint = ./entrypoint.sh;
    name = "airs-build";
    searchPaths = {
      rpath = [ libcPackage ];
      bin = [
        inputs.nixpkgs.findutils
        inputs.nixpkgs.gnugrep
        inputs.nixpkgs.gnused
        inputs.nixpkgs.nodejs_20
        inputs.nixpkgs.utillinux
        inputs.nixpkgs.autoconf
        inputs.nixpkgs.bash
        inputs.nixpkgs.binutils.bintools
        inputs.nixpkgs.gcc
        inputs.nixpkgs.gnumake
        inputs.nixpkgs.python311
      ];
      source = [
        outputs."/common/utils/aws"
        outputs."/common/utils/sops"
        (makeTemplate {
          name = "vips";
          searchPaths = {
            export = [
              [ "CPATH" inputs.nixpkgs.glib.dev "/include/glib-2.0" ]
              [ "CPATH" inputs.nixpkgs.glib.out "/lib/glib-2.0/include" ]
              [ "CPATH" __nixpkgs__.vips.dev "/include" ]
            ];
          };
        })
      ];
    };
  };
}
