{ projectPath, ... }:
let
  arch = let
    commit = "3c8778c732597a2fdee42d7c20d1ab5d948e13d4";
    sha256 = "sha256:0ii4qam4ykq9gbi2lmkk9p9gixmrypmz54jgcji9hpypk4azxy3w";
    url =
      "https://gitlab.com/fluidattacks/universe/-/raw/${commit}/common/ci/arch.nix";
    src = builtins.fetchurl { inherit sha256 url; };
  in import src;

  rules = {
    default = arch.core.rules.titleRule {
      products = [ "all" "airs" ];
      types = [ ];
    };
    noRotate = arch.core.rules.titleRule {
      products = [ "all" "airs" ];
      types = [ "feat" "fix" "refac" ];
    };
  };
in {
  pipelines = {
    airs = {
      gitlabPath = "/airs/pipeline/default.yaml";
      jobs = [
        {
          output = "/airs eph";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.default ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.airs ];
          };
        }
        {
          output = "/airs prod";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.prod ++ [ rules.default ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.airs ];
          };
        }
        {
          output = "/airs/lint/code";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.airs ];
          };
        }
        {
          output = "/airs/front/i18n-unused-keys";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.airs ];
          };
        }
        {
          output = "/airs/lint/content";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.airs ];
          };
        }
        {
          output = "/airs/test/unit";
          gitlabExtra = arch.extras.default // {
            artifacts = {
              paths = [ "airs/front/.coverage/lcov.info" ];
              expire_in = "1 day";
            };
            rules = arch.rules.dev ++ [ rules.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.airs ];
          };
        }
        {
          output = "/airs/coverage";
          gitlabExtra = arch.extras.default // {
            needs = [ "/airs/test/unit" ];
            rules = arch.rules.dev ++ [ rules.noRotate ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.airs ];
            variables.GIT_DEPTH = 1000;
          };
        }
        {
          output = "/airs/lint/styles";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.airs ];
          };
        }
        {
          output = "/airs/lint/markdownlint";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.airs ];
          };
        }
        {
          output = "/deployTerraform/airsInfra";
          gitlabExtra = arch.extras.default // {
            resource_group = "$CI_JOB_NAME";
            rules = arch.rules.prod ++ [ rules.default ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.airs ];
          };
        }
        {
          output = "/lintTerraform/airsInfra";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.airs ];
          };
        }
        {
          output = "/pipelineOnGitlab/airs";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.default ];
            stage = arch.stages.test;
            tags = [ arch.tags.airs ];
          };
        }
        {
          output = "/airs/machine/sca";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.default ];
            script = [ "m . /skims scan $PWD/airs/sca_config.yaml" ];
            stage = arch.stages.test;
            tags = [ arch.tags.airs ];
          };
        }
        {
          output = "/airs/machine/dast";
          gitlabExtra = arch.extras.default // {
            needs = [ "/airs eph" ];
            rules = arch.rules.dev ++ [ rules.default ];
            script = [
              "m . /skims --strict scan https://web.eph.fluidattacks.com/$CI_COMMIT_REF_SLUG"
            ];
            stage = arch.stages.post-deploy;
            tags = [ arch.tags.airs ];
          };
        }
        {
          output = "/testTerraform/airsInfra";
          gitlabExtra = arch.extras.default // {
            resource_group = "$CI_JOB_NAME";
            rules = arch.rules.dev ++ [ rules.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.airs ];
          };
        }
      ];
    };
  };
}
