import { fixupPluginRules } from "@eslint/compat";
import eslint from "@eslint/js";
import typescriptEslint from "@typescript-eslint/eslint-plugin";
import tsParser from "@typescript-eslint/parser";
import functional from "eslint-plugin-functional";
import importPlugin from "eslint-plugin-import";
import jest from "eslint-plugin-jest";
import jsxA11Y from "eslint-plugin-jsx-a11y";
import prettier from "eslint-plugin-prettier";
import eslintPluginPrettierRecommended from "eslint-plugin-prettier/recommended";
import reactPlugin from "eslint-plugin-react";
import hooksPlugin from "eslint-plugin-react-hooks";
import globals from "globals";
import tseslint from "typescript-eslint";

export default [
  eslint.configs.all,
  ...tseslint.configs.all,
  reactPlugin.configs.flat.all,
  reactPlugin.configs.flat["jsx-runtime"],
  jest.configs["flat/all"],
  functional.configs.recommended,
  functional.configs.externalTypeScriptRecommended,
  functional.configs.stylistic,
  eslintPluginPrettierRecommended,
  {
    // Keep this object separate to ignore these files for all rules
    ignores: [
      "**/.coverage",
      "*.mjs",
      "test",
      "**/*.js",
      "esbuild/*",
      "**/*.js.map",
      "**/vite.config.ts",
    ],
  },
  {
    plugins: {
      "@typescript-eslint": typescriptEslint,
      functional: functional,
      import: importPlugin,
      jest: jest,
      "jsx-a11y": jsxA11Y,
      prettier: prettier,
      react: reactPlugin,
      "react-hooks": fixupPluginRules(hooksPlugin),
    },
    linterOptions: {
      reportUnusedDisableDirectives: true,
    },
    languageOptions: {
      globals: {
        ...globals.browser,
        ...globals.node,
      },
      parser: tsParser,
      ecmaVersion: 5,
      sourceType: "module",
      parserOptions: {
        ecmaFeatures: {
          jsx: true,
        },
        projectService: true,
        tsconfigRootDir: import.meta.dirname,
        project: ["./tsconfig.json"],
      },
    },
    settings: {
      "import/parsers": {
        "@typescript-eslint/parser": [".ts", ".tsx"],
      },
      "import/resolver": {
        // You will also need to install and configure the TypeScript resolver
        // See also https://github.com/import-js/eslint-import-resolver-typescript#configuration
        typescript: true,
        node: true,
      },
      react: {
        version: "detect",
      },
    },
    files: ["**/*.ts", "**/*.tsx"],
    rules: {
      ...hooksPlugin.configs.recommended.rules,
      "capitalized-comments": [
        "error",
        "always",
        {
          ignoreConsecutiveComments: true,
        },
      ],
      eqeqeq: ["error", "smart"],
      "func-style": [
        "error",
        "declaration",
        {
          allowArrowFunctions: true,
        },
      ],
      "functional/functional-parameters": [
        "error",
        {
          enforceParameterCount: false,
        },
      ],
      /* Temporarily disabled. We need inheritance for some classes that use
       * the VSCode API, but it is worth seeing if we can accomplish that
       * with composition instead of inheritance.
       */
      "functional/no-class-inheritance": "off",
      "functional/no-expression-statements": "off",
      "functional/no-return-void": "off",
      "functional/prefer-immutable-types": "off",
      "functional/no-conditional-statements": "off",
      "functional/no-classes": "off",
      "id-length": [
        "error",
        {
          exceptions: ["_", "$", "t"],
          properties: "never",
        },
      ],
      "line-comment-position": ["error", "above"],
      "max-lines": ["error", 750],
      "max-lines-per-function": "off",
      "max-statements": "off",
      /*
       * Replaced by @typescript-eslint/no-duplicate-imports to avoid
       * conflicts with type imports
       * https://github.com/typescript-eslint/typescript-eslint/issues/2315
       */
      "no-duplicate-imports": "off",
      "no-ternary": "off",
      /*
       * This rule is deactivated as the codebase uses extensively "undefined"
       * It was established by a previous TSLint rule:
       * https://palantir.github.io/tslint/rules/no-null-keyword/
       * For consistency, we will prefer "undefined" over "null"
       */
      "no-undefined": "off",
      /*
       * Exception to the next rule: The variable "__typename" is required
       * by Apollo Library
       */
      "no-underscore-dangle": [
        "error",
        {
          allow: ["__typename"],
        },
      ],
      "no-void": [
        "error",
        {
          allowAsStatement: true,
        },
      ],
      "one-var": ["error", "never"],
      "padding-line-between-statements": [
        "error",
        {
          blankLine: "always",
          prev: "*",
          next: "return",
        },
      ],
      "sort-imports": [
        "error",
        {
          ignoreDeclarationSort: true,
        },
      ],
      "prettier/prettier": [
        "error",
        {},
        {
          usePrettierrc: true,
        },
      ],
      /*
       * Useful when migrating from TSLint to ESLint
       */
      "@typescript-eslint/ban-ts-comment": [
        "error",
        {
          "ts-expect-error": "allow-with-description",
        },
      ],
      "@typescript-eslint/ban-tslint-comment": "error",
      "@typescript-eslint/explicit-function-return-type": [
        "error",
        {
          allowConciseArrowFunctionExpressionsStartingWithVoid: false,
          allowTypedFunctionExpressions: false,
          allowHigherOrderFunctions: false,
        },
      ],
      "@typescript-eslint/max-params": [
        "error",
        {
          max: 9,
        },
      ],
      "@typescript-eslint/naming-convention": [
        "error",
        {
          selector: "variable",
          format: ["camelCase", "UPPER_CASE", "PascalCase"],
          leadingUnderscore: "allow",
        },
        {
          selector: "interface",
          format: ["PascalCase"],
          prefix: ["I"],
        },
      ],
      "import/no-duplicates": ["error"],
      "@typescript-eslint/no-magic-numbers": [
        "off",
        {
          ignore: [-1, 0, 1],
        },
      ],
      "@typescript-eslint/no-misused-promises": [
        "error",
        {
          checksVoidReturn: false,
        },
      ],
      /*
       * Using non-null assertions cancels the benefits of the strict
       * null-checking mode
       */
      "@typescript-eslint/no-non-null-assertion": "error",
      /*
       * This rule is incompatible with
       * "@typescript-eslint/no-non-null-assertion" and it
       * is not a problem to have verbose type casts
       */
      "@typescript-eslint/non-nullable-type-assertion-style": "off",
      "@typescript-eslint/no-unsafe-assignment": "off",
      "@typescript-eslint/no-unused-vars": [
        "error",
        {
          argsIgnorePattern: "^_",
        },
      ],
      /*
       * This is a good rule in theory, but in practice we found:
       * 1. We tried to use a DeepReadonly utility type to solve types nesting
       * problems; but it conflicts with wrapper components, since you cannot
       * modify the types of the component being wrapped, if it is from a third
       * party library.
       * 2. We are using the functional eslint plugin that is already in charge of
       * avoiding side effects as much as possible
       */
      "@typescript-eslint/prefer-readonly-parameter-types": "off",
      "@typescript-eslint/sort-type-constituents": "error",
      /*
       * Turned off to allow use of toBeCalled() in tests. Superseded by
       * jest/unbound-method
       */
      "@typescript-eslint/unbound-method": "off",
      /*
       * Since we need side effects to make our program meaningful, for instance:
       * callbacks, DOM mutations, Node processes, etc. We need to disable the
       * next TWO rules, the result is: from now on it should be inferred that
       * any method call without an assignment, produce some kind of side effect
       */
      "import/default": "error",
      "import/export": "error",
      "import/exports-last": "error",
      "import/first": "error",
      "import/group-exports": "error",
      "import/newline-after-import": "error",
      "import/no-absolute-path": [
        "error",
        {
          commonjs: false,
        },
      ],
      "import/no-cycle": [
        "error",
        {
          ignoreExternal: true,
        },
      ],
      "import/no-default-export": "error",
      "import/no-deprecated": "error",
      "import/no-extraneous-dependencies": [
        "error",
        {
          optionalDependencies: false,
        },
      ],
      "import/no-named-as-default": "error",
      "import/no-named-as-default-member": "error",
      "import/no-named-default": "error",
      "import/no-namespace": "error",
      "import/no-self-import": "error",
      /*
       * The vscode API is not a dependency, it comes from the editor itself
       */
      "import/no-unresolved": [
        "error",
        {
          ignore: ["vscode"],
        },
      ],
      "import/no-useless-path-segments": [
        "error",
        {
          noUselessIndex: true,
        },
      ],
      "import/no-webpack-loader-syntax": "error",
      "import/order": [
        "error",
        {
          alphabetize: {
            order: "asc",
            caseInsensitive: true,
          },

          groups: ["builtin", "external", "sibling"],
          "newlines-between": "always",
        },
      ],
      "jest/prefer-importing-jest-globals": ["off"],
      "react/jsx-boolean-value": ["error", "always"],
      "react/jsx-curly-brace-presence": ["error", "always"],
      "react/jsx-filename-extension": [
        "error",
        {
          extensions: [".tsx"],
        },
      ],
      "react/jsx-fragments": ["error", "element"],
      "react/jsx-max-depth": "off",
      "react/jsx-no-bind": [
        "error",
        {
          allowFunctions: true,
        },
      ],
      /*
       * "allowedStrings" is not working once the @typescript-eslint@4.15.2
       * plugin is updated. Affected string changed by its Unicode equivalent
       */
      "react/jsx-no-literals": ["error"],
      "react/function-component-definition": [
        "error",
        {
          namedComponents: "arrow-function",
        },
      ],
      "react-hooks/exhaustive-deps": "error",
      "react-hooks/rules-of-hooks": "error",
      "@typescript-eslint/parameter-properties": [
        1,
        {
          allow: ["public readonly", "private readonly"],
        },
      ],
    },
  },
];
