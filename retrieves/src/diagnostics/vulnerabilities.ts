import { existsSync } from "fs";
import { basename, join, sep } from "path";

import _ from "lodash";
import { DateTime } from "luxon";
import { groupBy } from "ramda";
import type { SimpleGit } from "simple-git";
import { simpleGit } from "simple-git";
import type {
  DiagnosticCollection,
  ExtensionContext,
  TextDocument,
  TextLine,
} from "vscode";
import { DiagnosticSeverity, Uri, env, window, workspace } from "vscode";

import {
  getGitRootVulnerabilities,
  getGroupGitRoots,
} from "@retrieves/api/root";
import type { IVulnerability } from "@retrieves/types";
import { VulnerabilityDiagnostic } from "@retrieves/types";
import { getRootInfoFromPath } from "@retrieves/utils/file";
import { getGitRootPath } from "@retrieves/utils/git";
import { rebase } from "@retrieves/utils/vulnerabilities";

const SEVERITY_MAP = {
  ACCEPTED: DiagnosticSeverity.Warning,
  ACCEPTED_UNDEFINED: DiagnosticSeverity.Warning,
  IN_PROGRESS: DiagnosticSeverity.Error,
  ON_HOLD: DiagnosticSeverity.Information,
  REJECTED: DiagnosticSeverity.Information,
  REQUESTED: DiagnosticSeverity.Information,
  SAFE: DiagnosticSeverity.Hint,
  SUBMITTED: DiagnosticSeverity.Warning,
  UNTREATED: DiagnosticSeverity.Error,
  VERIFIED: DiagnosticSeverity.Information,
  VULNERABLE: DiagnosticSeverity.Error,
};

const resolveSeverity = (vulnerability: IVulnerability): DiagnosticSeverity => {
  if (
    !_.isNil(vulnerability.verification) &&
    vulnerability.verification !== "Verified"
  ) {
    return DiagnosticSeverity.Information;
  } else if (
    !_.isNil(vulnerability.treatmentStatus) &&
    vulnerability.treatmentStatus.includes("ACCEPTED")
  ) {
    return SEVERITY_MAP[vulnerability.treatmentStatus];
  }

  return SEVERITY_MAP[vulnerability.state];
};

const createDiagnostic = (
  _doc: TextDocument | undefined,
  lineOfText: TextLine,
  vulnerability: IVulnerability,
): VulnerabilityDiagnostic => {
  const datePrefix = env.language.startsWith("es") ? "Hallado" : "Found";
  const reportDate: string | null = _.isNil(vulnerability.reportDate)
    ? null
    : DateTime.fromSQL(vulnerability.reportDate, {
        zone: "America/Bogota",
      }).toRelativeCalendar({ locale: env.language });
  const dateString: string = _.isNil(reportDate)
    ? ""
    : `${datePrefix} ${reportDate} |`;
  const diagnostic = new VulnerabilityDiagnostic(
    vulnerability,
    lineOfText.range,
    `${dateString} VULNERABILITY: ${vulnerability.finding.description}`,
    resolveSeverity(vulnerability),
  );

  return diagnostic;
};

const getLineOfText = (
  document: TextDocument,
  lineIndex: number,
  newLine: number | null,
): TextLine => {
  if (newLine === null) {
    return document.lineAt(lineIndex > 0 ? lineIndex - 1 : lineIndex);
  }

  return document.lineAt(newLine > 0 ? newLine - 1 : newLine);
};

const mapVulnToDiagnostic = async (
  repo: SimpleGit,
  currentWorkingPath: string,
  document: TextDocument,
  vuln: IVulnerability,
  fileRelativePath?: string,
): Promise<VulnerabilityDiagnostic> => {
  const lineIndex = parseInt(vuln.specific, 10);

  const newLine = await rebase(
    repo,
    vuln.commitHash,
    fileRelativePath ??
      document.fileName.replace(`${currentWorkingPath}${sep}`, ""),
    lineIndex,
  );

  const lineOfText = getLineOfText(document, lineIndex, newLine);

  return createDiagnostic(document, lineOfText, vuln);
};

const setDiagnostics = async (
  retrievesDiagnostics: DiagnosticCollection,
  document: TextDocument,
  rootId: string,
): Promise<void> => {
  const pathInfo = getRootInfoFromPath(document.fileName);
  if (!pathInfo || !workspace.workspaceFolders) {
    return;
  }

  const currentWorkingUri = workspace.workspaceFolders[0].uri;
  const rootDir = await getGitRootPath(currentWorkingUri.fsPath);
  const currentRepo = simpleGit({ baseDir: rootDir });
  const { groupName, fileRelativePath } = pathInfo;
  const vulnerabilities = await getGitRootVulnerabilities(groupName, rootId);
  const fileDiagnostics = vulnerabilities
    .filter(
      (vuln): boolean =>
        // Check path existence
        vuln.where
          // Windows compatibility
          .replaceAll("/", sep)
          .replace(vuln.rootNickname, basename(rootDir)) ===
          join(basename(rootDir), fileRelativePath) &&
        ["VULNERABLE", "SUBMITTED"].includes(vuln.state) &&
        // Check valid line number
        !Number.isNaN(parseInt(vuln.specific, 10)) &&
        parseInt(vuln.specific, 10) <= document.lineCount,
    )
    .map(
      async (vuln): Promise<VulnerabilityDiagnostic> =>
        mapVulnToDiagnostic(
          currentRepo,
          rootDir,
          document,
          vuln,
          fileRelativePath,
        ),
    );

  retrievesDiagnostics.set(document.uri, await Promise.all(fileDiagnostics));
};

const setDiagnosticsFromRoot = async (
  retrievesDiagnostics: DiagnosticCollection,
  document: TextDocument,
  rootNickname: string,
  vulnerabilities: IVulnerability[],
): Promise<void> => {
  if (!workspace.workspaceFolders) {
    return;
  }
  const currentWorkingUri = workspace.workspaceFolders[0].uri;
  const rootPath = await getGitRootPath(currentWorkingUri.fsPath);
  const currentRepo = simpleGit({ baseDir: rootPath });

  const fileDiagnostics = vulnerabilities
    .filter(
      (vuln): boolean =>
        // Check path existence
        vuln.where
          // Windows compatibility
          .replaceAll("/", sep)
          .replace(rootNickname, basename(rootPath))
          .includes(document.fileName.split(basename(rootPath))[1]) &&
        // Check valid line number
        !Number.isNaN(parseInt(vuln.specific, 10)) &&
        parseInt(vuln.specific, 10) <= document.lineCount,
    )
    .map(
      async (vuln): Promise<VulnerabilityDiagnostic> =>
        mapVulnToDiagnostic(currentRepo, rootPath, document, vuln),
    );
  retrievesDiagnostics.set(document.uri, await Promise.all(fileDiagnostics));
};

const handleDiagnostics = async (
  retrievesDiagnostics: DiagnosticCollection,
  document: TextDocument,
): Promise<void> => {
  const pathInfo = getRootInfoFromPath(document.fileName);
  if (!pathInfo) {
    return;
  }
  const { groupName, nickname } = pathInfo;
  const gitRoots = await getGroupGitRoots(groupName);
  // Try an exact match first
  const exactRoot = gitRoots.find(
    (item): boolean => item.nickname === nickname,
  );
  // If there's no strict match, try a fuzzy match instead
  const gitRoot =
    exactRoot ??
    gitRoots.find(
      (item): boolean =>
        item.nickname.includes(nickname) || nickname.includes(item.nickname),
    );

  if (!gitRoot) {
    return;
  }
  void setDiagnostics(retrievesDiagnostics, document, gitRoot.id);
};

const subscribeToDocumentChanges = (
  context: ExtensionContext,
  retrievesDiagnostics: DiagnosticCollection,
): void => {
  if (window.activeTextEditor) {
    void handleDiagnostics(
      retrievesDiagnostics,
      window.activeTextEditor.document,
    );
  }
  // eslint-disable-next-line functional/immutable-data
  context.subscriptions.push(
    window.onDidChangeActiveTextEditor((editor): void => {
      if (editor) {
        void handleDiagnostics(retrievesDiagnostics, editor.document);
      }
    }),
  );

  // eslint-disable-next-line functional/immutable-data
  context.subscriptions.push(
    workspace.onDidChangeTextDocument((event): void => {
      void handleDiagnostics(retrievesDiagnostics, event.document);
    }),
  );
};

const setDiagnosticsToAllFiles = (
  retrievesDiagnostics: DiagnosticCollection,
  rootNickname: string,
  workspacePath: string,
  vulnerabilities: IVulnerability[],
): void => {
  const vulnsGroupByPath = groupBy((vuln): string => {
    return vuln.where.trim().replace(`${rootNickname}/`, "");
  }, vulnerabilities);
  Object.keys(vulnsGroupByPath).forEach(async (filePath): Promise<void> => {
    const uri = Uri.file(join(workspacePath, filePath));
    // Windows compatibility
    if (!existsSync(uri.fsPath)) {
      return;
    }
    const document = await workspace.openTextDocument(uri);
    void setDiagnosticsFromRoot(
      retrievesDiagnostics,
      document,
      rootNickname,
      vulnsGroupByPath[filePath] ?? [],
    );
  });

  workspace.onDidSaveTextDocument((document: TextDocument): void => {
    const [, relativePath] = document.fileName.split(`${rootNickname}${sep}`);
    if (vulnsGroupByPath[relativePath]) {
      void setDiagnosticsFromRoot(
        retrievesDiagnostics,
        document,
        rootNickname,
        vulnsGroupByPath[relativePath] ?? [],
      );
    }
  });
};

export {
  createDiagnostic,
  subscribeToDocumentChanges,
  setDiagnosticsToAllFiles,
};
