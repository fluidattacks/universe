import type { ExtensionContext } from "vscode";
import { ExtensionMode } from "vscode";

export const getEnvironment = (
  context: ExtensionContext,
): "development" | "production" => {
  if (context.extensionMode === ExtensionMode.Production) {
    return "production";
  }

  return "development";
};
