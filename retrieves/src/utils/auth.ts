import type { SecretStorage } from "vscode";

export class RetrievesAuthentication {
  static #instance: RetrievesAuthentication;
  private constructor(private readonly secrets: SecretStorage) {}

  /** Gets the singleton instance */
  public static get instance(): RetrievesAuthentication {
    return RetrievesAuthentication.#instance;
  }

  /**
   * Initializes the singleton, should be run in `extension.ts` or the Jest
   * setup only
   * @param secrets The `secrets` attribute of the `ExtensionContext` instance
   */
  public static init(secrets: SecretStorage): void {
    // eslint-disable-next-line functional/immutable-data
    RetrievesAuthentication.#instance = new RetrievesAuthentication(secrets);
  }

  /** Gets the token from the `SecretStorage` */
  public async getToken(): Promise<string> {
    return this.secrets.get("fluidToken") as Thenable<string>;
  }
}
