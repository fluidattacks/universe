/* eslint-disable functional/immutable-data */
/*
 * The module 'vscode' contains the VS Code extensibility API
 * Import the module and reference it with the alias vscode in your code below
 */

import { existsSync } from "fs";
import { basename } from "path";

import _ from "lodash";
import { flatten, partial } from "ramda";
import { simpleGit } from "simple-git";
import type { ExtensionContext } from "vscode";
import { commands, languages, window, workspace } from "vscode";

import { checkGitInstallation, getGitRootPath } from "./utils/git";

import { getGroups } from "@retrieves/api/groups";
import { getUserInfo } from "@retrieves/api/role";
import {
  acceptVulnerabilityTemporarily,
  acceptVulnerabilityTemporarilyTreeItem,
} from "@retrieves/commands/acceptVulnerabilityTemporarily";
import { addLineToYaml } from "@retrieves/commands/addLineToYaml";
import { applySuggestedFixForVulnerability } from "@retrieves/commands/applySuggestedFixForVulnerability";
import { clone } from "@retrieves/commands/clone";
import { environmentUrls } from "@retrieves/commands/environmentUrls";
import { customFixForVulnerability } from "@retrieves/commands/getCustomFixForVulnerability";
import {
  getFindingDescription,
  getFindingDescriptionTreeItem,
} from "@retrieves/commands/getFindingDescription";
import {
  goToCriteria,
  goToCriteriaTreeItem,
} from "@retrieves/commands/goToCriteria";
import { goToFindingTreeItem } from "@retrieves/commands/goToFinding";
import {
  requestReattack,
  requestReattackTreeItem,
} from "@retrieves/commands/requestReattack";
import { setToken } from "@retrieves/commands/setToken";
import { showExtensionLog } from "@retrieves/commands/showExtensionLog";
import { toeLines } from "@retrieves/commands/toeLines";
import { updateToeLinesAttackedLines } from "@retrieves/commands/updateToeLinesAttackedLines";
import { subscribeToDocumentChanges } from "@retrieves/diagnostics/vulnerabilities";
import { FindingsProvider } from "@retrieves/providers/findings";
import { GroupsProvider } from "@retrieves/providers/groups";
import type { IGitRoot, IGroup } from "@retrieves/types";
import { RetrievesAuthentication } from "@retrieves/utils/auth";
import { shouldEnableHackerMode } from "@retrieves/utils/hackerMode";
import { Logger } from "@retrieves/utils/logging";
import { disambiguateRoot, identifyRoots } from "@retrieves/utils/root";
import { startBugsnag } from "@retrieves/utils/telemetry";
import { resolveToken } from "@retrieves/utils/token";
import { showWelcomeTab } from "@retrieves/utils/welcome";

const activate = async (context: ExtensionContext): Promise<void> => {
  Logger.info("Initializing extension");
  startBugsnag(context);
  commands.registerCommand("fluidattacks.showOutput", showExtensionLog);

  // Setup initial conditions
  await Promise.all(
    [
      "fluidattacks.hasSetToken",
      "fluidattacks.hackerMode",
      "fluidattacks.groupsAvailable",
      "fluidattacks.identifiedRepository",
    ].map(
      async (condition): Promise<void> =>
        commands.executeCommand("setContext", condition, false),
    ),
  );

  commands.registerCommand(
    "fluidattacks.setToken",
    partial(setToken, [context]),
  );
  if (!workspace.workspaceFolders) {
    Logger.warn("No open workspace");

    return;
  }

  // Check the token's existence
  const apiToken = await resolveToken(context.secrets);
  if (_.isNil(apiToken) || apiToken === "") {
    Logger.warn("Empty API token");

    // Show the welcome tab to input the token and reload the editor
    showWelcomeTab();

    return;
  }
  await context.secrets.store("fluidToken", apiToken);
  RetrievesAuthentication.init(context.secrets);

  // Check the token's validity
  const userInfo = await getUserInfo();
  if (userInfo.error?.message === "Login required") {
    // Show the welcome tab to input a new valid token and reload the editor
    showWelcomeTab();

    return;
  }
  // Token is valid from this point onwards
  await commands.executeCommand("setContext", "fluidattacks.hasSetToken", true);
  await context.globalState.update("userEmail", userInfo.userEmail);

  commands.registerCommand(
    "fluidattacks.goToCriteriaTreeItem",
    goToCriteriaTreeItem,
  );
  commands.registerCommand(
    "fluidattacks.goToFindingTreeItem",
    goToFindingTreeItem,
  );
  commands.registerCommand(
    "fluidattacks.getFindingDescriptionTreeItem",
    partial(getFindingDescriptionTreeItem, [context]),
  );

  // Setup the diagnostics (underlined lines) object and related commands
  const retrievesDiagnostics =
    languages.createDiagnosticCollection("fluidattacks");
  context.subscriptions.push(retrievesDiagnostics);
  commands.registerCommand(
    "fluidattacks.goToCriteria",
    partial(goToCriteria, [retrievesDiagnostics]),
  );
  commands.registerCommand(
    "fluidattacks.getFindingDescription",
    partial(getFindingDescription, [context, retrievesDiagnostics]),
  );
  commands.registerCommand(
    "fluidattacks.requestReattack",
    partial(requestReattack, [retrievesDiagnostics]),
  );
  commands.registerCommand(
    "fluidattacks.acceptVulnerabilityTemporarily",
    partial(acceptVulnerabilityTemporarily, [context, retrievesDiagnostics]),
  );

  // Find out where we are and setup local git root-related business logic
  const currentWorkingUri = workspace.workspaceFolders[0].uri;
  Logger.info(`Current working path is ${currentWorkingUri.path}`);
  const localNickname = basename(currentWorkingUri.path);
  if (!existsSync(currentWorkingUri.path)) {
    // Windows compatibility
    Logger.info(
      `FsPath: ${currentWorkingUri.fsPath}`,
      existsSync(currentWorkingUri.fsPath),
    );
  }
  const repo = simpleGit(currentWorkingUri.fsPath);
  const gitRemote = await Promise.resolve(
    repo
      .listRemote(["--get-url"])
      .then((url): string => url.toString().trim())
      .catch((): string => ""),
  );

  // Fetch available roots from the platform and identify the local root
  const groups = await getGroups();
  const roots = await identifyRoots(
    groups,
    gitRemote,
    userInfo.role,
    localNickname,
  );
  const gitRoot = await disambiguateRoot(roots);

  if (gitRoot) {
    Logger.info("Root found", {
      groupName: gitRoot.groupName,
      nickname: gitRoot.nickname,
      state: gitRoot.state,
      url: gitRoot.url,
      userRole: gitRoot.userRole,
    });
  } else {
    Logger.info("Root not found");
  }
  await context.globalState.update("groupName", gitRoot?.groupName);

  // Hacker mode business logic
  if (shouldEnableHackerMode(currentWorkingUri, gitRoot, userInfo.role)) {
    Logger.info("Hacker mode enabled");
    await commands.executeCommand(
      "setContext",
      "fluidattacks.hackerMode",
      true,
    );

    void commands.executeCommand(
      "setContext",
      "fluidattacks.groupsAvailable",
      true,
    );
    context.subscriptions.push(
      commands.registerCommand(
        "fluidattacks.lines",
        partial(toeLines, [context]),
      ),
    );

    context.subscriptions.push(
      commands.registerCommand(
        "fluidattacks.environmentUrls",
        partial(environmentUrls, [context]),
      ),
    );

    void window.createTreeView("user_groups", {
      showCollapseAll: true,
      treeDataProvider: new GroupsProvider(
        groups
          .filter((group: IGroup): boolean => group.userRole === "hacker")
          .map((group: IGroup): string => group.name),
      ),
    });

    commands.registerCommand("fluidattacks.clone", clone);
    commands.registerCommand(
      "fluidattacks.updateToeLinesAttackedLines",
      updateToeLinesAttackedLines,
    );
    commands.registerCommand("fluidattacks.addSelectedText", addLineToYaml);

    subscribeToDocumentChanges(context, retrievesDiagnostics);
  } else {
    // User mode business logic
    Logger.info("User mode enabled");

    if (gitRoot === undefined) {
      Logger.info("Local nickname: ", localNickname);
      Logger.info("Local remote URL: ", gitRemote);
      Logger.info(
        "Available active roots array:",
        flatten(
          groups.map(
            (
              group: IGroup,
            ): Pick<IGitRoot, "groupName" | "nickname" | "state" | "url">[] => {
              if (group.roots === null) {
                return [];
              }

              return group.roots
                .filter((root): boolean => root.state === "ACTIVE")
                .map(
                  ({
                    nickname,
                    state,
                    url,
                  }): Pick<
                    IGitRoot,
                    "groupName" | "nickname" | "state" | "url"
                  > => ({
                    groupName: group.name,
                    nickname,
                    state,
                    url,
                  }),
                );
            },
          ),
        ),
      );

      await window.showWarningMessage(
        `Could not identify the repository. Check that either the folder's name
        matches the root nickname or the local remote URL path is within
        the root's remote URL set in the Fluid Attacks platform. Example:
        my-company/my-repo in https://github.com/my-company/my-repo.git`,
      );

      return;
    }

    await commands.executeCommand(
      "setContext",
      "fluidattacks.identifiedRepository",
      true,
    );

    await checkGitInstallation(currentWorkingUri.fsPath);
    const rootDir = await getGitRootPath(currentWorkingUri.fsPath);
    const findingsProvider = new FindingsProvider(
      retrievesDiagnostics,
      gitRoot,
      rootDir,
    );
    window.createTreeView("vulnerabilities", {
      showCollapseAll: true,
      treeDataProvider: findingsProvider,
    });
    commands.registerCommand(
      "fluidattacks.requestReattackTreeItem",
      partial(requestReattackTreeItem, [findingsProvider]),
    );
    context.subscriptions.push(
      commands.registerCommand(
        "fluidattacks.applySuggestedFixForVulnerability",
        applySuggestedFixForVulnerability,
      ),
    );
    context.subscriptions.push(
      commands.registerCommand(
        "fluidattacks.getCustomFixForVulnerability",
        partial(customFixForVulnerability, [context]),
      ),
    );
    commands.registerCommand(
      "fluidattacks.acceptVulnerabilityTemporarilyTreeItem",
      partial(acceptVulnerabilityTemporarilyTreeItem, [
        context,
        findingsProvider,
      ]),
    );
    commands.registerCommand(
      "fluidattacks.refreshVulnerabilities",
      async (): Promise<void> => {
        await findingsProvider.refresh();
      },
    );

    // Populate TreeView and Diagnostics
    await findingsProvider.getChildren();
  }
};

const deactivate = async (context: ExtensionContext): Promise<void> => {
  await context.secrets.delete("fluidToken");
};

export { activate, deactivate };
