import { resolve } from "path";

import { run } from "jest";
import { window } from "vscode";

export async function integrationTestRunner(): Promise<void> {
  await window.showInformationMessage("Start all tests.");
  const testsRoot = resolve(__dirname, "..");

  await run([], testsRoot);
}
