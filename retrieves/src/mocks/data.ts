import type {
  IFinding,
  IGitRoot,
  IOrganization,
  IStakeholder,
  IVulnerability,
} from "@retrieves/types";

const getMockFinding = (groupName: string): IFinding => {
  return {
    description: "test description",
    groupName,
    id: "test-id",
    maxOpenSeverityScoreV4: 7.4,
    title: "011. Use of software with known vulnerabilities",
  };
};

const getMockRoots = (
  groupName: string,
): (IGitRoot & { __typename: string })[] => {
  return [
    {
      __typename: "GitRoot",
      downloadUrl: "pre-signed-url-here",
      gitEnvironmentUrls: [{ id: "env-1", url: "https://env1.com/api" }],
      gitignore: [],
      groupName,
      id: "git-root-1",
      nickname: "root1",
      state: "ACTIVE",
      url: "http://gitrepos.com/org/repo.git",
    },
    {
      __typename: "GitRoot",
      downloadUrl: "another-pre-signed-url-here",
      gitEnvironmentUrls: [],
      gitignore: [],
      groupName,
      id: "git-root-2",
      nickname: "root2",
      state: "ACTIVE",
      url: "ssh://git@gitrepos.com:org/dev-utils.git",
    },
    {
      __typename: "GitRoot",
      downloadUrl: "yet-another-pre-signed-url-here",
      gitEnvironmentUrls: [],
      gitignore: [],
      groupName,
      id: "git-root-3",
      nickname: "root3",
      state: "INACTIVE",
      url: "git@gitrepos.com:org/deprecated-repo.git",
    },
  ];
};

const getMockOrganizations: (IOrganization & { __typename: string })[] = [
  {
    __typename: "Organization",
    groups: [
      {
        name: "group1",
        roots: [],
        subscription: "continuous",
        userRole: "user",
      },
      {
        name: "group2",
        roots: getMockRoots("group2"),
        subscription: "continuous",
        userRole: "user",
      },
      {
        name: "group3",
        roots: [],
        subscription: "continuous",
        userRole: "user",
      },
    ],
    name: "org1",
  },
];

const getMockRootVulnerabilities = (finding: IFinding): IVulnerability[] => {
  return [
    {
      commitHash: "test-commit-hash",
      finding,
      id: "test-vuln-id-1",
      isAutoFixable: false,
      reportDate: "2023-04-16 05:48:40",
      rootNickname: "my-root",
      specific: "0",
      state: "VULNERABLE",
      treatmentStatus: "UNTREATED",
      verification: null,
      where: "my-root/scanners/other-scanner/requirements.txt",
    },
    {
      commitHash: "test-commit-hash",
      finding,
      id: "test-vuln-id-2",
      isAutoFixable: false,
      reportDate: "2023-05-16 05:48:40",
      rootNickname: "my-root",
      specific: "0",
      state: "VULNERABLE",
      treatmentStatus: "UNTREATED",
      verification: null,
      where: "my-root/scanners/w3af/requirements.txt",
    },
    {
      commitHash: "test-commit-hash",
      finding,
      id: "test-vuln-id-3",
      isAutoFixable: true,
      reportDate: "2022-11-02 05:30:04",
      rootNickname: "my-root",
      specific: "0",
      state: "VULNERABLE",
      treatmentStatus: "UNTREATED",
      verification: "Verified",
      where: "my-root/toes/bwapp/Dockerfile",
    },
    {
      commitHash: "test-commit-hash",
      finding,
      id: "test-vuln-id-4",
      isAutoFixable: false,
      reportDate: "2021-11-21 00:30:27",
      rootNickname: "my-root",
      specific: "52",
      state: "VULNERABLE",
      treatmentStatus: "UNTREATED",
      verification: null,
      where: "my-root/scanners/w3af/requirements.txt",
    },
    {
      commitHash: "test-commit-hash",
      finding,
      id: "test-vuln-id-5",
      isAutoFixable: false,
      reportDate: "2023-04-16 05:48:40",
      rootNickname: "my-root",
      specific: "0",
      state: "VULNERABLE",
      treatmentStatus: "UNTREATED",
      verification: null,
      where: "my-root/scanners/w3af/requirements.txt",
    },
    {
      commitHash: "test-commit-hash",
      finding,
      id: "test-vuln-id-6",
      isAutoFixable: false,
      reportDate: "2023-08-18 02:52:51",
      rootNickname: "my-root",
      specific: "68",
      state: "SAFE",
      treatmentStatus: "UNTREATED",
      verification: null,
      where: "my-root/scanners/w3af/requirements.txt",
    },
  ];
};

const mockStakeholders: IStakeholder[] = [
  {
    email: "john@company.com",
    invitationState: "REGISTERED",
    role: "user",
  },
  {
    email: "jane@company.com",
    invitationState: "REGISTERED",
    role: "group_manager",
  },
  {
    email: "bill@company.com",
    invitationState: "REGISTERED",
    role: "vulnerability_manager",
  },
  {
    email: "bob@company.com",
    invitationState: "PENDING",
    role: "vulnerability_manager",
  },
  {
    email: "dev@fluidattacks.com",
    invitationState: "REGISTERED",
    role: "user",
  },
  {
    email: "hacker@fluidattacks.com",
    invitationState: "REGISTERED",
    role: "hacker",
  },
];

export {
  getMockFinding,
  getMockRootVulnerabilities,
  getMockOrganizations,
  getMockRoots,
  mockStakeholders,
};
