export interface IBugsnagConfig {
  isTelemetryEnabled: boolean;
  stage: string;
  retrievesVersion: string;
}
