/* eslint-disable functional/immutable-data */
import type { Event } from "@bugsnag/core";
import type { Error } from "@bugsnag/core/types/event";
import Bugsnag from "@bugsnag/js";
import BugsnagPluginReact from "@bugsnag/plugin-react";
import type {
  BugsnagErrorBoundary,
  BugsnagPluginReactResult,
} from "@bugsnag/plugin-react";
import React, { Fragment } from "react";

import type { IBugsnagConfig } from "@retrieves/webview/types";

const isCausedByRetrieves = (event: Event): boolean => {
  return event.errors.some((error): boolean => {
    return error.stacktrace.some(
      (stackframe): boolean =>
        stackframe.file.includes("retrieves") ||
        stackframe.file.includes("fluidattacks"),
    );
  });
};

const startBugsnag = (config: IBugsnagConfig): void => {
  Bugsnag.start({
    apiKey: "ca660653ea33c94979c1bb51f3fb99e8",
    appType: "webview",
    appVersion: config.retrievesVersion,
    onError: (event: Event): boolean => {
      // Strip out unneeded device info
      const { hostname, osName, osVersion, runtimeVersions, time } =
        event.device;
      event.device = { hostname, osName, osVersion, runtimeVersions, time };
      event.errors.forEach((error: Error): void => {
        const message: string | undefined = event.context;
        event.context = error.errorMessage;
        error.errorMessage = typeof message === "string" ? message : "";
        event.groupingHash = event.context;
      });

      // Sending metadata will take the isTelemetryEnabled option into account
      return config.isTelemetryEnabled && isCausedByRetrieves(event);
    },
    plugins: [new BugsnagPluginReact(React)],
    releaseStage: config.stage,
  }).startSession();
};

const reactPlugin: BugsnagPluginReactResult | undefined =
  Bugsnag.getPlugin("react");

const bugsnagErrorBoundary: BugsnagErrorBoundary =
  reactPlugin === undefined ? Fragment : reactPlugin.createErrorBoundary(React);

export { startBugsnag, bugsnagErrorBoundary as BugsnagErrorBoundary };
