/* eslint-disable jest/max-expects */
import { ApolloError } from "@apollo/client";
import { GraphQLError } from "graphql";
import { window } from "vscode";

import { getGroups } from "./groups";
import { getUserInfo } from "./role";
import {
  getGitRoot,
  getGitRootVulnerabilities,
  getGroupGitRoots,
  getGroupGitRootsSimple,
  markFileAsAttacked,
} from "./root";
import { refetchVulnerability } from "./vulnerabilities";

import { getMockFinding } from "@retrieves/mocks/data";
import type { IFinding, IVulnerability } from "@retrieves/types";
import { Logger } from "@retrieves/utils/logging";

describe("test api", (): void => {
  it("should get groups and handle extraGroups", async (): Promise<void> => {
    expect.hasAssertions();

    const groups = await getGroups();

    // Three from the mock handlers and one from extraGroups
    expect(groups).toHaveLength(4);
  });

  it("should make sure only active roots are brought in", async (): Promise<void> => {
    expect.hasAssertions();

    const groupName = "test-group";
    const roots = await getGroupGitRootsSimple(groupName);

    roots.forEach((root): void => {
      expect(root.state).toBe("ACTIVE");
    });

    const hackerRoots = await getGroupGitRoots(groupName);

    hackerRoots.forEach((root): void => {
      expect(root.state).toBe("ACTIVE");
    });
  });

  it("should handle git root errors", async (): Promise<void> => {
    expect.hasAssertions();

    const groupName = "failGroup";
    const rootId = "test-rood-id";
    const groupRootsSimple = await getGroupGitRootsSimple(groupName);

    expect(groupRootsSimple).toStrictEqual([]);
    expect(window.showErrorMessage).toHaveBeenNthCalledWith(
      1,
      "Our platform's API endpoint is currently unavailable. Please refresh in a moment or reload the extension",
      ...["Reload", "Later"],
    );

    const groupRoots = await getGroupGitRoots(groupName);

    expect(groupRoots).toStrictEqual([]);
    expect(window.showErrorMessage).toHaveBeenNthCalledWith(
      2,
      "This token has expired. Please retry with a valid API token",
    );

    const gitRootVulnerabilities = await getGitRootVulnerabilities(
      groupName,
      rootId,
    );

    expect(gitRootVulnerabilities).toStrictEqual([]);
    expect(window.showErrorMessage).toHaveBeenNthCalledWith(
      3,
      "Token format unrecognized. Please retry with a valid API token",
    );

    const gitRoot = await getGitRoot(groupName, rootId);

    expect(gitRoot).toStrictEqual({});
    expect(window.showErrorMessage).toHaveBeenNthCalledWith(
      4,
      "This token has expired. Please retry with a valid API token",
    );
  });

  it("should get a singular git root", async (): Promise<void> => {
    expect.hasAssertions();

    const groupName = "testGroup";
    const rootId = "git-root-1";
    const root = await getGitRoot(groupName, rootId);

    expect(root).toStrictEqual({
      __typename: "GitRoot",
      downloadUrl: "pre-signed-url-here",
      gitEnvironmentUrls: [{ id: "env-1", url: "https://env1.com/api" }],
      gitignore: [],
      id: "git-root-1",
      nickname: "root1",
      state: "ACTIVE",
      url: "http://gitrepos.com/org/repo.git",
    });

    const anotherRootId = "fake-git-root";
    const errorSpy = jest.spyOn(Logger, "error");
    const anotherRoot = await getGitRoot(groupName, anotherRootId);

    expect(anotherRoot).toStrictEqual({});
    expect(errorSpy).toHaveBeenCalledWith(
      "Failed to get a GitRoot from testGroup:",
      new ApolloError({
        graphQLErrors: [
          new GraphQLError("Exception - Access denied or root not found"),
        ],
      }),
    );
  });

  it("should warn the user if a DevSecOps token is detected", async (): Promise<void> => {
    expect.hasAssertions();

    const warnSpy = jest.spyOn(Logger, "warn");
    const userInfo = await getUserInfo();

    expect(userInfo).toStrictEqual({
      role: "user",
      userEmail: "forces.group@fluidattacks.com",
    });

    expect(warnSpy).toHaveBeenCalledWith(
      "Using DevSecOps token instead of the user's API token",
    );
    expect(window.showWarningMessage).toHaveBeenCalledWith(
      `You seem to be using a DevSecOps
          token. This token is not meant to be used for this extension, you
          cannot request reattacks nor accept vulnerabilities with it. We
          encourage you to use your own API token instead`,
    );
  });

  it("should refetch vulnerabilities", async (): Promise<void> => {
    expect.hasAssertions();

    const finding: IFinding = getMockFinding("testGroup");
    const vulnerability: IVulnerability = {
      commitHash: "",
      finding,
      id: "testVulnId",
      isAutoFixable: true,
      reportDate: "",
      rootNickname: "",
      specific: "10",
      state: "VULNERABLE",
      where: "file.ts",
    };

    expect(vulnerability.verification).toBeUndefined();

    const refetchedVulnerabilty = await refetchVulnerability(vulnerability);

    expect(refetchedVulnerabilty.verification).toBe("Requested");

    const anotherVulnerability: IVulnerability = {
      ...vulnerability,
      id: "failVulnId",
    };
    const errorSpy = jest.spyOn(Logger, "error");
    const anotherRefetchedVulnerabilty =
      await refetchVulnerability(anotherVulnerability);

    expect(anotherRefetchedVulnerabilty).toStrictEqual(anotherVulnerability);
    expect(errorSpy).toHaveBeenCalledWith(
      "Failed to refetch vulnerability:",
      new ApolloError({
        graphQLErrors: [
          new GraphQLError("Response not successful: Received status code 504"),
        ],
      }),
    );
  });

  it("should mark attacked files", async (): Promise<void> => {
    expect.hasAssertions();

    const groupName = "testGroup";
    const attackedLines = 10;
    const rootId = "test-rood-id";
    const fileName = "test2/test.sh";
    const comments = "comment 1";

    const attackedFileResult = await markFileAsAttacked(
      groupName,
      attackedLines,
      rootId,
      fileName,
      comments,
    );

    expect(attackedFileResult.success).toBe(true);
    expect(attackedFileResult.message).toBeUndefined();

    const anotherGroupName = "failGroup";
    const errorSpy = jest.spyOn(Logger, "error");

    const attackedFileResultFail = await markFileAsAttacked(
      anotherGroupName,
      attackedLines,
      rootId,
      fileName,
      comments,
    );

    expect(errorSpy).toHaveBeenCalledWith(
      "Failed to mark file test2/test.sh:",
      new ApolloError({
        graphQLErrors: [new GraphQLError("Error verifying group toe lines")],
      }),
    );
    expect(attackedFileResultFail.success).toBe(false);
    expect(attackedFileResultFail.message).toBe(
      "Error verifying group toe lines",
    );
  });
});
