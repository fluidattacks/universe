import type {
  IFindingDescription,
  IGitRoot,
  IOrganization,
  IStakeholder,
  IToeLines,
  IVulnerability,
} from "@retrieves/types";

interface IGetFindingDescription {
  data: { finding: IFindingDescription };
}

interface IGetGitRoot {
  data: { root: IGitRoot };
}

interface IGetGitRoots {
  data: { group: { roots: IGitRoot[] } };
}

interface IGetUserInfo {
  data: {
    me: {
      role: string;
      userEmail: string;
    };
  };
}

interface IGetGroups {
  data: {
    me: {
      organizations: IOrganization[];
    };
  };
}

interface IGetGroupStakeholders {
  data: {
    group: {
      stakeholders: IStakeholder[];
    };
  };
}

interface IVulnerabilitiesEdge {
  node: IVulnerability;
}

interface IVulnerabilitiesPaginator {
  edges: IVulnerabilitiesEdge[];
  pageInfo: {
    hasNextPage: boolean;
    endCursor: string;
  };
}

interface IGetVulnerabilities {
  group: { vulnerabilities: IVulnerabilitiesPaginator };
}

interface IToeLinesEdge {
  node: IToeLines;
}

interface IToeLinesPaginator {
  edges: IToeLinesEdge[];
  pageInfo: {
    hasNextPage: boolean;
    endCursor: string;
  };
}

interface IToeLinesQuery {
  group: { toeLines: IToeLinesPaginator };
}

interface IUpdateAttackedFile {
  updateToeLinesAttackedLines: { success: boolean; message?: string };
}

interface IUpdateAttackedFileMutation {
  data: IUpdateAttackedFile;
}

export type {
  IGetFindingDescription,
  IGetGroups,
  IGetGitRoot,
  IGetGitRoots,
  IGetGroupStakeholders,
  IGetUserInfo,
  IGetVulnerabilities,
  IToeLinesQuery,
  IToeLinesEdge,
  IUpdateAttackedFile,
  IUpdateAttackedFileMutation,
  IToeLinesPaginator,
  IVulnerabilitiesEdge,
  IVulnerabilitiesPaginator,
};
