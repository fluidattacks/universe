import type { ApolloError } from "@apollo/client";

import type { IGetFindingDescription } from "./types";

import { GET_FINDING_DESCRIPTION } from "@retrieves/queries";
import type { IFindingDescription } from "@retrieves/types";
import { API_CLIENT, handleGraphQlError } from "@retrieves/utils/apollo";
import { Logger } from "@retrieves/utils/logging";

export const fetchFindingDescription = async (
  identifier: string,
): Promise<IFindingDescription> => {
  const result: IGetFindingDescription = await API_CLIENT.query({
    query: GET_FINDING_DESCRIPTION,
    variables: { findingId: identifier },
  }).catch(async (error: unknown): Promise<IGetFindingDescription> => {
    await handleGraphQlError(error as ApolloError);
    Logger.error("Failed to fetch a finding's description:", error);

    return {
      data: {
        finding: {
          attackVectorDescription: "",
          description: "",
          groupName: "",
          id: identifier,
          maxOpenSeverityScoreV4: 0,
          recommendation: "",
          severityVectorV4: "",
          threat: "",
          title: "",
          vulnerabilitiesSummaryV4: {
            open: 0,
          },
        },
      },
    };
  });

  return result.data.finding;
};
