/* eslint-disable jest/max-expects */
/* eslint-disable max-lines */
import type {
  DiagnosticCollection,
  ExtensionContext,
  InputBoxValidationMessage,
  QuickPickItem,
} from "vscode";
import {
  DiagnosticSeverity,
  InputBoxValidationSeverity,
  Position,
  Range,
  TreeItemCollapsibleState,
  Uri,
  commands,
  env,
  window,
} from "vscode";

import {
  acceptVulnerabilityTemporarily,
  acceptVulnerabilityTemporarilyTreeItem,
} from "./acceptVulnerabilityTemporarily";

import {
  goToCriteria,
  goToCriteriaTreeItem,
} from "@retrieves/commands/goToCriteria";
import { goToFindingTreeItem } from "@retrieves/commands/goToFinding";
import {
  requestReattack,
  requestReattackTreeItem,
} from "@retrieves/commands/requestReattack";
import { setToken } from "@retrieves/commands/setToken";
import { showExtensionLog } from "@retrieves/commands/showExtensionLog";
import { getMockFinding } from "@retrieves/mocks/data";
import type { FindingsProvider } from "@retrieves/providers/findings";
import { FindingTreeItem } from "@retrieves/treeItems/finding";
import { VulnerabilityTreeItem } from "@retrieves/treeItems/vulnerability";
import type { IFinding, IVulnerability } from "@retrieves/types";
import { VulnerabilityDiagnostic } from "@retrieves/types";
import { Logger } from "@retrieves/utils/logging";
import { getAppUrl } from "@retrieves/utils/url";
import { validTextField } from "@retrieves/utils/validations";

jest.mock(
  "@retrieves/utils/prompts",
  (): Record<string, unknown> => ({
    getAcceptanceEndDate: jest
      .fn()
      .mockResolvedValueOnce("2023-10-03")
      .mockResolvedValueOnce("1970-01-01")
      .mockResolvedValueOnce("2023-10-03")
      .mockResolvedValueOnce("2023-10-03")
      .mockResolvedValueOnce("2023-11-03")
      .mockResolvedValueOnce("2023-10-03"),
    getJustificationForReattack: jest
      .fn()
      .mockResolvedValueOnce("test justification")
      .mockResolvedValueOnce("test justification")
      .mockResolvedValueOnce("test justification")
      .mockResolvedValueOnce("test justification"),
    getJustificationForTreatment: jest
      .fn()
      .mockResolvedValue("test justification"),
  }),
);

jest.mock(
  "@retrieves/utils/validations",
  (): Record<string, unknown> => ({
    validTextField: jest.fn(),
  }),
);

describe("test commands", (): void => {
  const userOptions = [
    {
      description: "Vulnerability manager",
      label: "bill@company.com",
    },
    {
      description: "Group manager",
      label: "jane@company.com",
    },
    {
      description: "User",
      label: "john@company.com",
    },
  ];
  const mockFinding: IFinding = getMockFinding("testGroup");
  const mockVulnerability: IVulnerability = {
    commitHash: "testCommitHash",
    finding: mockFinding,
    id: "testVulnId",
    isAutoFixable: true,
    reportDate: "2023-02-01 15:48:40",
    rootNickname: "",
    specific: "10",
    state: "VULNERABLE",
    where: "file.ts",
  };

  it("should accept a vulnerability temporarily", async (): Promise<void> => {
    expect.hasAssertions();

    const anotherFinding: IFinding = {
      ...mockFinding,
      title: "060. Insecure service configuration - Host verification",
    };
    const anotherVuln: IVulnerability = {
      ...mockVulnerability,
      finding: anotherFinding,
    };
    const vulnerabilityItem = new VulnerabilityTreeItem(
      "file.ts",
      TreeItemCollapsibleState.Collapsed,
      "testVulnId",
      anotherFinding,
      anotherVuln,
      "/home/path/to/root",
    );

    const mockFindingsProvider = {
      refresh: jest.fn(),
    } as unknown as FindingsProvider;

    const { contextValue } = vulnerabilityItem;

    await acceptVulnerabilityTemporarilyTreeItem(
      {
        globalState: { get: jest.fn((_key): string => "user@company.com") },
      } as unknown as ExtensionContext,
      mockFindingsProvider,
      vulnerabilityItem,
    );

    expect(window.showQuickPick).toHaveBeenCalledWith(
      userOptions as QuickPickItem[],
      { canPickMany: false, title: "Select the user to assign" },
    );
    expect(window.showInformationMessage).toHaveBeenCalledWith(
      "Treatment updated successfully",
    );
    expect(mockFindingsProvider.refresh).toHaveBeenCalledWith(
      vulnerabilityItem,
    );

    expect(contextValue).toBe("vulnerability-fixable");

    // Lets simulate an Invalid date (1970-01-01) error
    await acceptVulnerabilityTemporarilyTreeItem(
      {
        globalState: { get: jest.fn((_key): string => "dev@fluidattacks.com") },
      } as unknown as ExtensionContext,
      mockFindingsProvider,
      vulnerabilityItem,
    );

    expect(window.showQuickPick).toHaveBeenNthCalledWith(
      2,
      [
        {
          description: "Vulnerability manager",
          label: "bill@company.com",
        },
        {
          description: "User",
          label: "dev@fluidattacks.com",
        },
        {
          description: "Group manager",
          label: "jane@company.com",
        },
        {
          description: "User",
          label: "john@company.com",
        },
      ] as QuickPickItem[],

      { canPickMany: false, title: "Select the user to assign" },
    );
    expect(window.showErrorMessage).toHaveBeenCalledWith(
      "Exception - Chosen date is either in the past or exceeds the " +
        "maximum number of days allowed by the defined policy",
    );

    // Unfixable vuln

    const anotherOmittedFinding: IFinding = {
      ...mockFinding,
      title: "439. Sensitive information in source code - IP",
    };
    const anotherVulnWithoutAutofix: IVulnerability = {
      ...mockVulnerability,
      finding: anotherOmittedFinding,
      isAutoFixable: false,
      specific: "0",
    };
    const vulnItem = new VulnerabilityTreeItem(
      "file.ts",
      TreeItemCollapsibleState.Collapsed,
      "testVulnId",
      anotherOmittedFinding,
      anotherVulnWithoutAutofix,
      "/home/path/to/root",
    );

    const contextUnfixableValue = vulnItem.contextValue;

    await acceptVulnerabilityTemporarilyTreeItem(
      {
        globalState: { get: jest.fn((_key): string => "user@company.com") },
      } as unknown as ExtensionContext,
      mockFindingsProvider,
      vulnItem,
    );

    expect(window.showQuickPick).toHaveBeenCalledWith(
      userOptions as QuickPickItem[],
      { canPickMany: false, title: "Select the user to assign" },
    );
    expect(window.showInformationMessage).toHaveBeenNthCalledWith(
      2,
      "Treatment updated successfully",
    );
    expect(mockFindingsProvider.refresh).toHaveBeenCalledWith(
      vulnerabilityItem,
    );
    expect(contextUnfixableValue).toBe("vulnerability-unfixable");
  });

  it("should fail gracefully if the stakeholder query fails", async (): Promise<void> => {
    expect.hasAssertions();

    const finding: IFinding = getMockFinding("failGroup");
    const vulnerability: IVulnerability = {
      commitHash: "",
      finding,
      id: "testVulnId",
      isAutoFixable: true,
      reportDate: "",
      rootNickname: "",
      specific: "10",
      state: "VULNERABLE",
      where: "file.ts",
    };

    const vulnerabilityItem = new VulnerabilityTreeItem(
      "file.ts",
      TreeItemCollapsibleState.Collapsed,
      "testVulnId",
      finding,
      vulnerability,
      "/home/path/to/root",
    );

    const mockFindingsProvider = {
      refresh: jest.fn(),
    } as unknown as FindingsProvider;

    await acceptVulnerabilityTemporarilyTreeItem(
      {
        globalState: { get: jest.fn((_key): string => "failUser@company.com") },
      } as unknown as ExtensionContext,
      mockFindingsProvider,
      vulnerabilityItem,
    );

    expect(window.showQuickPick).toHaveBeenCalledWith(
      [
        {
          label: "failUser@company.com",
        },
      ] as QuickPickItem[],
      { canPickMany: false, title: "Select the user to assign" },
    );
    // Lets assume the user cannot assign themselves
    expect(window.showWarningMessage).toHaveBeenNthCalledWith(
      2,
      "Assigned not valid",
    );

    // Test acceptVulnerabilityTemporarily fail

    const mockDiagnostic: VulnerabilityDiagnostic[] = [
      new VulnerabilityDiagnostic(
        vulnerability,
        new Range(new Position(9, 1), new Position(9, 1)),
        "Found 7 months ago | VULNERABILITY: test description",
        DiagnosticSeverity.Error,
      ),
    ];
    // eslint-disable-next-line functional/immutable-data
    Object.defineProperty(window, "activeTextEditor", {
      configurable: true,
      value: window.activeTextEditor,
    });

    await acceptVulnerabilityTemporarily(
      {
        globalState: {
          get: jest.fn((_key): string => {
            // eslint-disable-next-line jest/no-conditional-in-test
            return _key === "userEmail" ? "failUser@company.com" : "failGroup";
          }),
        },
      } as unknown as ExtensionContext,
      {
        get: jest.fn(
          (_uri: Uri): VulnerabilityDiagnostic[] | undefined => mockDiagnostic,
        ),
      } as unknown as DiagnosticCollection,
    );

    expect(window.showQuickPick).toHaveBeenCalledWith(
      [
        {
          label: "failUser@company.com",
        },
      ] as QuickPickItem[],
      { canPickMany: false, title: "Select the user to assign" },
    );
    // Lets assume the user cannot assign themselves
    expect(window.showWarningMessage).toHaveBeenNthCalledWith(
      3,
      "Assigned not valid",
    );
  });

  it("should accept vulnerability temporarily based on a diagnostic", async (): Promise<void> => {
    expect.hasAssertions();

    const mockDiagnostic: VulnerabilityDiagnostic[] = [
      new VulnerabilityDiagnostic(
        mockVulnerability,
        new Range(new Position(9, 1), new Position(9, 1)),
        "Found 7 months ago | VULNERABILITY: test description",
        DiagnosticSeverity.Error,
      ),
    ];

    await acceptVulnerabilityTemporarily(
      {
        globalState: {
          get: jest.fn((_key): string => {
            // eslint-disable-next-line jest/no-conditional-in-test
            return _key === "userEmail" ? "user@company.com" : "testGroup";
          }),
        },
      } as unknown as ExtensionContext,
      {
        get: jest.fn(
          (_uri: Uri): VulnerabilityDiagnostic[] | undefined => mockDiagnostic,
        ),
      } as unknown as DiagnosticCollection,
    );

    expect(window.showQuickPick).toHaveBeenCalledWith(
      userOptions as QuickPickItem[],
      { canPickMany: false, title: "Select the user to assign" },
    );
    expect(window.showInformationMessage).toHaveBeenNthCalledWith(
      3,
      "Treatment updated successfully",
    );

    jest.clearAllMocks();
  });

  it("shouldn't accept vulnerability temporarily based on a diagnostic", async (): Promise<void> => {
    expect.hasAssertions();

    const mockDiagnostic: VulnerabilityDiagnostic[] = [
      new VulnerabilityDiagnostic(
        mockVulnerability,
        new Range(new Position(9, 1), new Position(9, 1)),
        "Found 7 months ago | VULNERABILITY: test description",
        DiagnosticSeverity.Error,
      ),
    ];

    await acceptVulnerabilityTemporarily(
      {
        globalState: {
          get: jest.fn((_key): string => {
            // eslint-disable-next-line jest/no-conditional-in-test
            return _key === "userEmail" ? "user@company.com" : "testGroup";
          }),
        },
      } as unknown as ExtensionContext,
      {
        get: jest.fn(
          (_uri: Uri): VulnerabilityDiagnostic[] | undefined => mockDiagnostic,
        ),
      } as unknown as DiagnosticCollection,
    );

    expect(window.showQuickPick).toHaveBeenCalledWith(
      userOptions as QuickPickItem[],
      { canPickMany: false, title: "Select the user to assign" },
    );
    expect(window.showInformationMessage).not.toHaveBeenCalledWith(
      "Treatment updated successfully",
    );

    jest.clearAllMocks();
  });

  it("should show information message if there isn't diagnostics for treatment", async (): Promise<void> => {
    expect.hasAssertions();

    await acceptVulnerabilityTemporarily(
      {
        globalState: {
          get: jest.fn(),
        },
      } as unknown as ExtensionContext,
      {
        get: jest.fn(
          (_uri: Uri): VulnerabilityDiagnostic[] | undefined => undefined,
        ),
      } as unknown as DiagnosticCollection,
    );

    expect(window.showInformationMessage).toHaveBeenCalledWith(
      "This line does not contain vulnerabilities",
    );

    jest.clearAllMocks();
  });

  it("should show a text field to set the token", async (): Promise<void> => {
    expect.hasAssertions();

    const dummyContext = { secrets: { store: jest.fn() } };
    await setToken(dummyContext as unknown as ExtensionContext);

    expect(window.showInputBox).toHaveBeenCalledWith({
      password: true,
      placeHolder: "Paste your token here",
      title: "Fluid Attacks API Token",
      validateInput: expect.any(Function),
    });
  });

  it("should store the token and show a reload message if the token is valid", async (): Promise<void> => {
    expect.hasAssertions();

    const mockToken = "validToken";
    const infoSpy = jest.spyOn(Logger, "info");

    jest.mocked(window.showInputBox).mockResolvedValue(mockToken);

    const dummyContext = {
      secrets: { store: jest.fn() },
    } as unknown as ExtensionContext;

    await setToken(dummyContext);

    expect(dummyContext.secrets.store).toHaveBeenCalledWith(
      "fluidToken",
      mockToken,
    );
    expect(infoSpy).toHaveBeenCalledWith("Fluid Attacks token updated");
    expect(window.showInformationMessage).toHaveBeenCalledWith(
      "Reload to apply changes",
      "Reload",
      "Later",
    );
  });

  it('should reload the window if the "Reload" action is selected', async (): Promise<void> => {
    expect.hasAssertions();

    jest.mocked(window.showInputBox).mockResolvedValue("validToken");

    const dummyContext = {
      secrets: { store: jest.fn() },
    } as unknown as ExtensionContext;
    await setToken(dummyContext);

    expect(Logger.info).toHaveBeenCalledWith("Reloading extension host");
    expect(commands.executeCommand).toHaveBeenCalledWith(
      "workbench.action.restartExtensionHost",
    );
  });

  it("should return a validation message for invalid token", async (): Promise<void> => {
    expect.hasAssertions();

    jest.mocked(window.showInputBox).mockResolvedValue("=invalid-token");
    jest
      .mocked(validTextField)
      .mockReturnValue("Invalid character at the beginning: '='");

    const dummyContext = {
      secrets: { store: jest.fn() },
    } as unknown as ExtensionContext;

    await setToken(dummyContext);

    expect(window.showInputBox).toHaveBeenCalledWith({
      password: true,
      placeHolder: "Paste your token here",
      title: "Fluid Attacks API Token",
      validateInput: expect.any(Function),
    });

    const inputBoxOptions = jest.mocked(window.showInputBox).mock
      .calls[0][0] as {
      validateInput: (token: string) => InputBoxValidationMessage | undefined;
    };
    const validationMessage = inputBoxOptions.validateInput("=invalid-token");

    expect(validationMessage).toStrictEqual({
      message:
        "Invalid JSON Web Token: Invalid character at the beginning: '='",
      severity: InputBoxValidationSeverity.Error,
    });

    // Undefined
    jest.mocked(window.showInputBox).mockResolvedValue("valid-token");
    jest.mocked(validTextField).mockReturnValue(undefined);

    await setToken(dummyContext);

    expect(window.showInputBox).toHaveBeenCalledWith({
      password: true,
      placeHolder: "Paste your token here",
      title: "Fluid Attacks API Token",
      validateInput: expect.any(Function),
    });

    const otherInputBoxOptions = jest.mocked(window.showInputBox).mock
      .calls[0][0] as {
      validateInput: (token: string) => InputBoxValidationMessage | undefined;
    };
    const otherValidationMessage =
      otherInputBoxOptions.validateInput("valid-token");

    expect(otherValidationMessage).toBeUndefined();
  });

  it("should show the extension log", (): void => {
    expect.hasAssertions();

    const showSpy = jest.spyOn(Logger, "show");
    showExtensionLog();

    expect(showSpy).toHaveBeenCalledWith();
  });

  it("should link the user to the platform", (): void => {
    expect.hasAssertions();

    const finding: IFinding = {
      description: "test description",
      groupName: "testGroup",
      id: "testId",
      maxOpenSeverityScoreV4: 3.3,
      title: "011. Use of software with known vulnerabilities",
    };

    const findingItem = new FindingTreeItem(
      "011. Use of software with known vulnerabilities",
      TreeItemCollapsibleState.Collapsed,
      "test-id",
      finding,
    );
    const baseUrl = getAppUrl();
    const uri = Uri.parse(`${baseUrl}/groups/testGroup/vulns/testId/locations`);
    goToFindingTreeItem(findingItem);

    expect(env.openExternal).toHaveBeenCalledWith(uri);
  });

  it("should link the user to criteria", (): void => {
    expect.hasAssertions();

    const finding: IFinding = {
      description: "test description",
      groupName: "testGroup",
      id: "testId",
      maxOpenSeverityScoreV4: 3.3,
      title: "011. Use of software with known vulnerabilities",
    };

    const findingItem = new FindingTreeItem(
      "011. Use of software with known vulnerabilities",
      TreeItemCollapsibleState.Collapsed,
      "test-id",
      finding,
    );
    const uri = Uri.parse(
      `https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-011`,
    );
    goToCriteriaTreeItem(findingItem);

    expect(env.openExternal).toHaveBeenCalledWith(uri);
  });

  it("should link the user to criteria based on a diagnostic", (): void => {
    expect.hasAssertions();

    const finding: IFinding = {
      description: "test description",
      groupName: "testGroup",
      id: "testId",
      maxOpenSeverityScoreV4: 3.1,
      title: "011. Use of software with known vulnerabilities",
    };

    const vulnerability: IVulnerability = {
      commitHash: "testCommitHash",
      finding,
      id: "testVulnId",
      isAutoFixable: true,
      reportDate: "2023-02-01 15:48:40",
      rootNickname: "",
      specific: "10",
      state: "VULNERABLE",
      where: "folder/subfolder/file.ts",
    };
    const mockDiagnostic: VulnerabilityDiagnostic[] = [
      new VulnerabilityDiagnostic(
        vulnerability,
        new Range(new Position(9, 1), new Position(9, 1)),
        "Found 7 months ago | VULNERABILITY: test description",
        DiagnosticSeverity.Error,
      ),
    ];
    // eslint-disable-next-line functional/immutable-data
    Object.defineProperty(window, "activeTextEditor", {
      configurable: true,
      value: window.activeTextEditor,
    });

    goToCriteria({
      get: jest.fn(
        (_uri: Uri): VulnerabilityDiagnostic[] | undefined => mockDiagnostic,
      ),
    } as unknown as DiagnosticCollection);

    const uri = Uri.parse(
      `https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-011`,
    );

    expect(env.openExternal).toHaveBeenCalledWith(uri);
  });

  it("should show information message if diagnostic(s) is undefined", (): void => {
    expect.hasAssertions();

    goToCriteria({
      get: jest.fn(
        (_uri: Uri): VulnerabilityDiagnostic[] | undefined => undefined,
      ),
    } as unknown as DiagnosticCollection);

    expect(window.showInformationMessage).toHaveBeenCalledWith(
      "This line does not contain vulnerabilities",
    );

    // Different fileDiagnostics start line and activeTextEditor active line

    const finding: IFinding = {
      description: "test description",
      groupName: "testGroup",
      id: "testId",
      maxOpenSeverityScoreV4: 3.2,
      title: "011. Use of software with known vulnerabilities",
    };

    const vulnerability: IVulnerability = {
      commitHash: "testCommitHash",
      finding,
      id: "testVulnId",
      isAutoFixable: true,
      reportDate: "2023-02-01 15:48:40",
      rootNickname: "",
      specific: "10",
      state: "VULNERABLE",
      where: "folder/subfolder/file.ts",
    };
    const mockDiagnostic: VulnerabilityDiagnostic[] = [
      new VulnerabilityDiagnostic(
        vulnerability,
        new Range(new Position(9, 1), new Position(9, 1)),
        "Found 7 months ago | VULNERABILITY: test description",
        DiagnosticSeverity.Error,
      ),
    ];

    // eslint-disable-next-line functional/immutable-data
    Object.defineProperty(window, "activeTextEditor", {
      configurable: true,
      value: {
        document: {
          fileName: "testFile.txt",
        },
        selection: {
          active: { line: 8 },
          end: { line: 9 },
          start: { line: 9 },
        },
      },
    });

    goToCriteria({
      get: jest.fn(
        (_uri: Uri): VulnerabilityDiagnostic[] | undefined => mockDiagnostic,
      ),
    } as unknown as DiagnosticCollection);

    expect(window.showInformationMessage).toHaveBeenNthCalledWith(
      5,
      "This line does not contain vulnerabilities",
    );
  });

  it("should return undefined if no activeTextEditor", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    // eslint-disable-next-line functional/immutable-data
    Object.defineProperty(window, "activeTextEditor", {
      configurable: true,
      value: undefined,
    });
    const mockGet = jest.fn();

    goToCriteria({
      get: mockGet,
    } as unknown as DiagnosticCollection);

    await acceptVulnerabilityTemporarily(
      {
        globalState: {
          get: mockGet,
        },
      } as unknown as ExtensionContext,
      {
        get: mockGet,
      } as unknown as DiagnosticCollection,
    );

    expect(mockGet).not.toHaveBeenCalled();
    expect(env.openExternal).not.toHaveBeenCalled();
    expect(window.showQuickPick).not.toHaveBeenCalled();
  });

  it("should request a reattack", async (): Promise<void> => {
    expect.hasAssertions();

    const vulnerabilityItem = new VulnerabilityTreeItem(
      "file.ts",
      TreeItemCollapsibleState.Collapsed,
      "testVulnId",
      mockFinding,
      mockVulnerability,
      "/home/path/to/root",
    );
    const mockFindingsProvider = {
      refresh: jest.fn(),
    } as unknown as FindingsProvider;

    await requestReattackTreeItem(mockFindingsProvider, vulnerabilityItem);

    expect(window.showInformationMessage).toHaveBeenCalledWith(
      "Reattack requested successfully",
    );
    expect(mockFindingsProvider.refresh).toHaveBeenCalledWith(
      vulnerabilityItem,
    );

    // Now lets simulate an error like Login Required
    const anotherFinding: IFinding = {
      ...getMockFinding("testGroup"),
      id: "should-fail-test-id",
    };
    const anotherVulnerability: IVulnerability = {
      commitHash: "",
      finding: anotherFinding,
      id: "testVulnId",
      isAutoFixable: true,
      reportDate: "",
      rootNickname: "",
      specific: "10",
      state: "VULNERABLE",
      where: "file.ts",
    };

    const anotherVulnerabilityItem = new VulnerabilityTreeItem(
      "file.ts",
      TreeItemCollapsibleState.Collapsed,
      "testVulnId",
      anotherFinding,
      anotherVulnerability,
      "/home/path/to/root",
    );

    await requestReattackTreeItem(
      mockFindingsProvider,
      anotherVulnerabilityItem,
    );

    expect(window.showWarningMessage).toHaveBeenCalledWith("Login required");
    // Called in the first part of the test, not this one
    expect(mockFindingsProvider.refresh).not.toHaveBeenCalledTimes(2);
  });

  it("shouldn't request a reattack if no activeTextEditor", async (): Promise<void> => {
    expect.hasAssertions();

    const mockretrievesDiagnostics = {
      get: jest.fn(),
    } as unknown as DiagnosticCollection;
    await requestReattack(mockretrievesDiagnostics);

    expect(mockretrievesDiagnostics.get).not.toHaveBeenCalled();
    expect(window.showInformationMessage).not.toHaveBeenNthCalledWith(
      2,
      "Reattack requested successfully",
    );
  });

  it("should request a reattack based on a diagnostic", async (): Promise<void> => {
    expect.hasAssertions();

    const mockDiagnostic: VulnerabilityDiagnostic[] = [
      new VulnerabilityDiagnostic(
        mockVulnerability,
        new Range(new Position(9, 1), new Position(9, 1)),
        "Found 7 months ago | VULNERABILITY: test description",
        DiagnosticSeverity.Error,
      ),
    ];
    const mockretrievesDiagnostics = {
      get: jest.fn(
        (_uri: Uri): VulnerabilityDiagnostic[] | undefined => mockDiagnostic,
      ),
    } as unknown as DiagnosticCollection;

    // eslint-disable-next-line functional/immutable-data
    Object.defineProperty(window, "activeTextEditor", {
      configurable: true,
      value: {
        document: {
          fileName: "testFile.txt",
          getText: jest.fn((): string => "Content test"),
        },
        selection: {
          active: { line: 8 },
          end: { line: 9 },
          start: { line: 9 },
        },
      },
    });
    await requestReattack(mockretrievesDiagnostics);

    expect(window.showInformationMessage).toHaveBeenNthCalledWith(
      2,
      "Reattack requested successfully",
    );
  });

  it("should fail request a reattack by login required", async (): Promise<void> => {
    expect.hasAssertions();

    // Now lets simulate an error like Login Required
    const anotherFinding: IFinding = {
      ...getMockFinding("testGroup"),
      id: "should-fail-test-id",
    };
    const anotherVulnerability: IVulnerability = {
      commitHash: "",
      finding: anotherFinding,
      id: "testVulnId",
      isAutoFixable: true,
      reportDate: "",
      rootNickname: "",
      specific: "10",
      state: "VULNERABLE",
      where: "file.ts",
    };

    const mockDiagnostic: VulnerabilityDiagnostic[] = [
      new VulnerabilityDiagnostic(
        anotherVulnerability,
        new Range(new Position(9, 1), new Position(9, 1)),
        "Found 7 months ago | VULNERABILITY: test description",
        DiagnosticSeverity.Error,
      ),
    ];
    const mockretrievesDiagnostics = {
      get: jest.fn(
        (_uri: Uri): VulnerabilityDiagnostic[] | undefined => mockDiagnostic,
      ),
    } as unknown as DiagnosticCollection;

    // eslint-disable-next-line functional/immutable-data
    Object.defineProperty(window, "activeTextEditor", {
      configurable: true,
      value: {
        document: {
          fileName: "testFile.txt",
          getText: jest.fn((): string => "Content test"),
        },
        selection: {
          active: { line: 8 },
          end: { line: 9 },
          start: { line: 9 },
        },
      },
    });
    await requestReattack(mockretrievesDiagnostics);

    expect(window.showWarningMessage).toHaveBeenNthCalledWith(
      2,
      "Login required",
    );

    jest.clearAllMocks();
  });

  it("should show information message if there isn't diagnostics for reattack", async (): Promise<void> => {
    expect.hasAssertions();

    const mockretrievesDiagnostics = {
      get: jest.fn(
        (_uri: Uri): VulnerabilityDiagnostic[] | undefined => undefined,
      ),
    } as unknown as DiagnosticCollection;

    await requestReattack(mockretrievesDiagnostics);

    expect(window.showInformationMessage).toHaveBeenCalledWith(
      "This line does not contain vulnerabilities",
    );

    jest.clearAllMocks();
  });

  it("shouldn't request a reattack without justification", async (): Promise<void> => {
    expect.hasAssertions();

    const mockDiagnostic: VulnerabilityDiagnostic[] = [
      new VulnerabilityDiagnostic(
        mockVulnerability,
        new Range(new Position(9, 1), new Position(9, 1)),
        "Found 7 months ago | VULNERABILITY: test description",
        DiagnosticSeverity.Error,
      ),
    ];
    const mockretrievesDiagnostics = {
      get: jest.fn(
        (_uri: Uri): VulnerabilityDiagnostic[] | undefined => mockDiagnostic,
      ),
    } as unknown as DiagnosticCollection;

    await requestReattack(mockretrievesDiagnostics);

    expect(window.showInformationMessage).not.toHaveBeenNthCalledWith(
      3,
      "Reattack requested successfully",
    );

    const vulnerabilityItem = new VulnerabilityTreeItem(
      "file.ts",
      TreeItemCollapsibleState.Collapsed,
      "testVulnId",
      mockFinding,
      mockVulnerability,
      "/home/path/to/root",
    );
    const mockFindingsProvider = {
      refresh: jest.fn(),
    } as unknown as FindingsProvider;

    await requestReattackTreeItem(mockFindingsProvider, vulnerabilityItem);

    expect(window.showInformationMessage).not.toHaveBeenNthCalledWith(
      3,
      "Reattack requested successfully",
    );
  });

  it("shouldn't accept a vulnerability temporarily no assigned user selected", async (): Promise<void> => {
    expect.hasAssertions();

    const vulnerabilityItem = new VulnerabilityTreeItem(
      "file.ts",
      TreeItemCollapsibleState.Collapsed,
      "testVulnId",
      mockFinding,
      mockVulnerability,
      "/home/path/to/root",
    );

    const mockFindingsProvider = {
      refresh: jest.fn(),
    } as unknown as FindingsProvider;

    jest.mocked(window.showQuickPick).mockResolvedValue(undefined);

    await acceptVulnerabilityTemporarilyTreeItem(
      {
        globalState: { get: jest.fn((_key): string => "user@company.com") },
      } as unknown as ExtensionContext,
      mockFindingsProvider,
      vulnerabilityItem,
    );

    expect(window.showQuickPick).toHaveBeenCalledWith(
      userOptions as QuickPickItem[],
      { canPickMany: false, title: "Select the user to assign" },
    );
    expect(window.showInformationMessage).not.toHaveBeenCalledWith(
      "Treatment updated successfully",
    );
  });
});
