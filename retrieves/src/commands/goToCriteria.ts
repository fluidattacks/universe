import type { DiagnosticCollection } from "vscode";
import { Uri, env, window } from "vscode";

import type { FindingTreeItem } from "@retrieves/treeItems/finding";
import type { VulnerabilityDiagnostic } from "@retrieves/types";

const goToCriteria = (retrievesDiagnostics: DiagnosticCollection): void => {
  const { activeTextEditor } = window;
  if (!activeTextEditor) {
    return;
  }
  const fileDiagnostics: readonly VulnerabilityDiagnostic[] | undefined =
    retrievesDiagnostics.get(activeTextEditor.document.uri);

  if (!fileDiagnostics) {
    void window.showInformationMessage(
      "This line does not contain vulnerabilities",
    );

    return;
  }
  const diagnostic = fileDiagnostics.find(
    (item): boolean =>
      item.range.start.line === activeTextEditor.selection.active.line &&
      item.source === "fluidattacks",
  );

  if (
    diagnostic?.code === undefined ||
    typeof diagnostic.code === "string" ||
    typeof diagnostic.code === "number"
  ) {
    void window.showInformationMessage(
      "This line does not contain vulnerabilities",
    );

    return;
  }

  const findingTitle: string = diagnostic.code.value.toString();
  const url = Uri.parse(
    `https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-${
      findingTitle.split(".")[0]
    }`,
  );
  void env.openExternal(url);
};

const goToCriteriaTreeItem = (item: FindingTreeItem): void => {
  const url = Uri.parse(
    `https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-${
      item.finding.title.split(".")[0]
    }`,
  );
  void env.openExternal(url);
};

export { goToCriteria, goToCriteriaTreeItem };
