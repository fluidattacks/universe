{ inputs, makeScript, fetchArchive, outputs, ... }:
makeScript {
  entrypoint = "start_code_server $@";
  name = "retrieves-code-server-start";
  searchPaths = { source = [ outputs."/retrieves/code-server" ]; };
}
