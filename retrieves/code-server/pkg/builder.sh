# shellcheck shell=bash
function main {
  : \
    && mkdir -p "${out}" \
    && pushd "${envSrc}" \
    && info "Building code-server..." \
    && cp \
      --recursive \
      --no-preserve=mode \
      bin lib node_modules npm-shrinkwrap.json out package.json src typings "${out}" \
    && sed -i 's/exec "$ROOT\/lib\/node" "$ROOT" "$@"/exec node "$ROOT" "$@"/' "${out}"/bin/code-server \
    && chmod a+x "${out}"/bin/code-server \
    && chmod a+x "${out}"/lib/node \
    && popd \
    || return 1
}

main "${@}"
