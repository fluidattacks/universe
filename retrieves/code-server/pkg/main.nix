{ inputs, makeDerivation, ... }:
let
  platformMappings = {
    x86_64-linux = {
      platform = "linux-amd64";
      checksum = "sha256:1c4dyn7f9x6jxil9b4sgzws99j2bcq7ifhs4qiy3vmkag4zacl0h";
    };
    aarch64-linux = {
      platform = "linux-arm64";
      checksum = "sha256:1vn1arff937x2lgvispnd8k6gi5118c5wsprxk4c1yxspba1av1x";
    };
    aarch64-darwin = {
      platform = "macos-arm64";
      checksum = "sha256:1n4fa0rb5cnzkbhxv6d1izirwgg4svc06shfcz6q6fykj27gpzs4";
    };
  };
  currentPlatform = builtins.currentSystem;
  platformInfo = builtins.getAttr currentPlatform platformMappings;
  version = "4.96.4";
in makeDerivation {
  builder = ./builder.sh;
  env = {
    envSrc = builtins.fetchTarball {
      url =
        "https://github.com/coder/code-server/releases/download/v${version}/code-server-${version}-${platformInfo.platform}.tar.gz";
      sha256 = "${platformInfo.checksum}";
    };
  };
  name = "retrieves-code-server";
  searchPaths = { bin = [ inputs.nixpkgs.gnused ]; };
}
