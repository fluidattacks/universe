# shellcheck shell=bash
export PORT=9090
function start_code_server() {
  standalone=false
  path_arg=""
  while [[ $# -gt 0 ]]; do
    case $1 in
      --standalone)
        standalone=true
        shift
        ;;
      --path=*)
        path_arg="${1#*=}"
        shift
        ;;
      *)
        shift
        ;;
    esac
  done
  : \
    && aws_login "dev" "3600" \
    && sops_export_vars integrates/secrets/dev.yaml \
      TEST_RETRIEVES_TOKEN \
    && export FLUID_API_TOKEN=$TEST_RETRIEVES_TOKEN \
    && install_extension \
    && pushd retrieves \
    && if [ "$standalone" = true ]; then
      info "Starting standalone code-server"
      __argCodeServer__/bin/code-server --auth none --disable-telemetry --disable-workspace-trust "${path_arg}" &
    else
      info "Starting code-server..."
      __argCodeServer__/bin/code-server --auth none --disable-telemetry --disable-workspace-trust "${path_arg}"
    fi \
    && popd \
    || return 1
}

function stop_code_server() {
  : \
    && info "Terminating code-server..." \
    && if fuser -k $PORT/tcp; then
      info "Successfully terminated code-server"
    else
      info "Code-server instance not found. Skipping..."
    fi
}

function install_extension() {
  : \
    && pushd retrieves \
    && npm ci \
    && rm -rf dist \
    && ./node_modules/.bin/webpack-cli --mode development --config ./webpack/extension.config.js \
    && ./node_modules/.bin/webpack-cli --mode development --config ./webpack/webview.config.js \
    && ./node_modules/.bin/vsce package \
    && info "Packaged successfully" \
    && __argCodeServer__/bin/code-server --auth none --disable-telemetry --install-extension fluidattacks-*.vsix \
    && popd \
    || return 1
}

function code_server_cli() {
  : \
    && pushd __argCodeServer__ \
    && bin/code-server "${@}" \
    && popd \
    || return 1
}
