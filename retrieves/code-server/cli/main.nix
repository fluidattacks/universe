{ inputs, makeScript, fetchArchive, outputs, ... }:
makeScript {
  entrypoint = "code_server_cli $@";
  name = "retrieves-code-server-cli";
  searchPaths = { source = [ outputs."/retrieves/code-server" ]; };
}
