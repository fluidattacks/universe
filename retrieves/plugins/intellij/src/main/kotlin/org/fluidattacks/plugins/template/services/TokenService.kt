package org.fluidattacks.plugins.template.services

import com.intellij.ide.util.PropertiesComponent
import com.intellij.openapi.components.Service
import com.intellij.openapi.project.Project

@Service(Service.Level.PROJECT)
class TokenService(
    private val project: Project,
) {
    private val tokenKey = "org.fluidattacks.plugins.template.token"

    fun getToken(): String? = PropertiesComponent.getInstance().getValue(tokenKey)

    fun setToken(token: String) {
        PropertiesComponent.getInstance().setValue(tokenKey, token)
    }
}
