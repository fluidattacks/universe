{ inputs, makeScript, ... }:
makeScript {
  entrypoint = ./entrypoint.sh;
  name = "integrates-intellij";
  searchPaths = { bin = [ inputs.nixpkgs.ktlint ]; };
}
