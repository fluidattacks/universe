# shellcheck shell=bash

function _lint {
  info "Running ktlint for ${dir}"
  ktlint --color -F "src/**/*.kt" "!generated/**"
}

function main {
  local dir="retrieves/plugins/intellij"

  pushd "${dir}" || error "${dir} directory not found"

  case "${1:-}" in
    lint) _lint "${@:2}" ;;
    *) error "Must provide 'lint' as the first argument" ;;
  esac
}

main "${@}"
