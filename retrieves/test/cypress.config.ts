import { defineConfig } from "cypress";

const { APP_URL, FLUID_API_TOKEN } = process.env;
export default defineConfig({
  e2e: {
    baseUrl: "http://localhost:9090",
    retries: {
      openMode: 0,
      runMode: 1,
    },
    setupNodeEvents(on, config) {
      on("task", {
        log(args) {
          console.log(...args);
          return null;
        },
      });
      return config;
    },
    specPattern: "./specs/**",
    supportFile: "support/setup.ts",
    testIsolation: true,
    defaultCommandTimeout: 10000,
  },
  env: {
    APP_URL,
    FLUID_API_TOKEN,
  },
});
