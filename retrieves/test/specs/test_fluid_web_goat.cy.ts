describe("Test web goat", () => {
  it("Test fluid attacks IDE on web goat app", () => {
    const vulnerableColor = "rgb(176, 16, 17)";

    const initWait = 5500;
    const folderWait = 1500;

    cy.log(`Target app url: ${Cypress.env("APP_URL")}`);
    cy.visit(`/?folder=${Cypress.env("REPO_PATH")}`);
    cy.wait(initWait);
    cy.findExactText("a", "Dockerfile")
      .children()
      .should("have.css", "color", vulnerableColor);
    cy.findExactText("a", "src")
      .children()
      .should("have.css", "color", vulnerableColor);
    cy.findExactText("a", "src").parent().click();
    cy.wait(folderWait);
    cy.findExactText("a", "it")
      .children()
      .should("have.css", "color", vulnerableColor);
    cy.findExactText("a", "it").parent().click();
    cy.wait(folderWait);
    cy.findExactText("a", "JWTLessonIntegrationTest.java")
      .children()
      .should("have.css", "color", vulnerableColor);
    cy.findExactText("a", "JWTLessonIntegrationTest.java").click();
    cy.get(`[aria-label*="Close ("]`).click();
    cy.get(`[aria-label="Collapse Folders in Explorer"]`).click();
  });
});
