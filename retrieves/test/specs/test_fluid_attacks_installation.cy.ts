describe("Test fluid attacks installation", () => {
  it("Test if the extension was successfully installed", () => {
    cy.log(`Target app url: ${Cypress.env("APP_URL")}`);
    cy.visit("/");
    cy.get('[aria-label*="Extensions ("]').filter("a").click();
    cy.contains("Fluid Attacks", { timeout: 24000 }).click();
  });
});
