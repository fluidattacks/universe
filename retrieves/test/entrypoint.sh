# shellcheck shell=bash

function main {
  export APP_URL="https://localhost:8001"
  export NODE_TLS_REJECT_UNAUTHORIZED=0
  export FLUID_API_TOKEN
  export CYPRESS_REPO_PATH

  aws_login dev 3600
  sops_export_vars_cypress integrates/secrets/dev.yaml TEST_RETRIEVES_TOKEN
  FLUID_API_TOKEN="${TEST_RETRIEVES_TOKEN}"
  DAEMON=true integrates-db
  DAEMON=true integrates-back dev
  sleep 40
  start_code_server --standalone

  pushd retrieves/test || error "retrieves/test not found"
  npm ci
  CYPRESS_REPO_PATH="${PWD}/WebGoat"
  if [ ! -d "${CYPRESS_REPO_PATH}" ]; then
    git clone https://github.com/WebGoat/WebGoat
    git -C "${CYPRESS_REPO_PATH}" checkout 9d5ab5fb21921cf1a70de9bfb1d4e0f367bbbd4d
  fi
  run_cypress_fa "firefox" "${@}"
  stop_code_server
}

main "${@}"
