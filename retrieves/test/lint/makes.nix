{ inputs, makeScript, ... }: {
  jobs."/retrieves/test/lint" = makeScript {
    entrypoint = ./entrypoint.sh;
    name = "retrieves-test-lint";
    searchPaths.bin = [ inputs.nixpkgs.bash inputs.nixpkgs.nodejs_20 ];
  };
}
