{ inputs, isLinux, isDarwin, makeScript, outputs, ... }: {
  imports = [ ./lint/makes.nix ];
  jobs."/retrieves/test" = makeScript {
    entrypoint = ./entrypoint.sh;
    name = "retrieves-test";
    searchPaths = {
      bin = [
        inputs.nixpkgs.git
        inputs.nixpkgs.kubectl
        inputs.nixpkgs.nodejs_20
        outputs."/integrates/back"
        outputs."/integrates/db"
      ] ++ inputs.nixpkgs.lib.optionals isLinux [ inputs.nixpkgs.chromium ]
        ++ inputs.nixpkgs.lib.optionals isDarwin [ inputs.nixpkgs.electron ];
      source = [
        outputs."/common/utils/aws"
        outputs."/common/utils/sops"
        outputs."/retrieves/code-server"
        outputs."/common/utils/cypress-fa"
      ];
    };
  };
}
