{ inputs, makeScript, outputs, projectPath, ... }:
let version = "v0.23.2";
in makeScript {
  entrypoint = ./entrypoint.sh;
  name = "retrieves";
  replace = {
    __argSecretsProd__ = projectPath "/integrates/secrets/prod.yaml";
    __argTreeSitter__ = (inputs.nixpkgs.fetchurl {
      sha256 = "sha256-d+tqL8Wm+CbPgfVGfc13z7Un0gGZblNKAmr2pb5j6uE=";
      url =
        "https://github.com/tree-sitter/tree-sitter/releases/download/${version}/tree-sitter.wasm";
    }).outPath;
    __argCSharpWasm__ = outputs."/common/utils/tree-sitter/wasm/c_sharp";
    __argDartWasm__ = outputs."/common/utils/tree-sitter/wasm/dart";
    __argGoWasm__ = outputs."/common/utils/tree-sitter/wasm/go";
    __argHclWasm__ = outputs."/common/utils/tree-sitter/wasm/hcl";
    __argJavaWasm__ = outputs."/common/utils/tree-sitter/wasm/java";
    __argJavascriptWasm__ = outputs."/common/utils/tree-sitter/wasm/javascript";
    __argJsonWasm__ = outputs."/common/utils/tree-sitter/wasm/json";
    __argKotlinWasm__ = outputs."/common/utils/tree-sitter/wasm/kotlin";
    __argPhpWasm__ = outputs."/common/utils/tree-sitter/wasm/php";
    __argPythonWasm__ = outputs."/common/utils/tree-sitter/wasm/python";
    __argRubyWasm__ = outputs."/common/utils/tree-sitter/wasm/ruby";
    __argScalaWasm__ = outputs."/common/utils/tree-sitter/wasm/scala";
    __argSwiftWasm__ = outputs."/common/utils/tree-sitter/wasm/swift";
    __argTypescriptWasm__ = outputs."/common/utils/tree-sitter/wasm/typescript";
    __argYamlWasm__ = outputs."/common/utils/tree-sitter/wasm/yaml";
  };
  searchPaths = {
    bin = [ inputs.nixpkgs.bash inputs.nixpkgs.nodejs_20 ];
    source = [ outputs."/common/utils/aws" outputs."/common/utils/sops" ];
  };
}
