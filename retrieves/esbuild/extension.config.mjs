import { build, context } from "esbuild";

const isDev = process.env.GITLAB_CI === undefined;
const buildOptions = {
  bundle: true,
  entryPoints: ["./src/extension.ts"],
  external: ["bufferutil", "utf-8-validate", "vscode"],
  format: "cjs",
  minify: !isDev,
  outdir: "dist",
  platform: "node",
  sourcemap: true,
};

if (isDev) {
  const ctx = await context(buildOptions);

  // eslint-disable-next-line fp/no-mutating-methods
  await ctx.watch();
} else {
  await build(buildOptions);
}
