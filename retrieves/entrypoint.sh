# shellcheck shell=bash

function _deploy {
  local wasm_parsers=(
    __argCSharpWasm__
    __argDartWasm__
    __argGoWasm__
    __argHclWasm__
    __argJavaWasm__
    __argJavascriptWasm__
    __argJsonWasm__
    __argKotlinWasm__
    __argPhpWasm__
    __argPythonWasm__
    __argRubyWasm__
    __argScalaWasm__
    __argSwiftWasm__
    __argTypescriptWasm__
    __argYamlWasm__
  )

  rm -rf assets dist
  mkdir -p assets
  cp __argTreeSitter__ assets/tree-sitter.wasm
  for parser in "${wasm_parsers[@]}"; do
    if [ -d "$parser" ]; then
      cp "$parser"/tree-sitter-*.wasm assets
    else
      cp "$parser" assets
    fi
  done
  npm run bundle
  aws_login "prod_integrates" "3600"
  sops_export_vars __argSecretsProd__ "AZURE_ACCESS_TOKEN"
  ./node_modules/.bin/vsce publish \
    -p "${AZURE_ACCESS_TOKEN}" \
    --allow-missing-repository \
    --skip-duplicate
  ./node_modules/@bugsnag/source-maps/bin/cli upload-node \
    --api-key ca660653ea33c94979c1bb51f3fb99e8 \
    --detect-app-version \
    --overwrite \
    --directory dist
}

function main {
  local dir="retrieves"

  pushd "${dir}" || error "${dir} directory not found"
  npm ci

  case "${1:-}" in
    deploy) _deploy "${@:2}" ;;
    lint | test) npm run "$@" ;;
    *) error "Must provide either 'deploy', 'lint', or 'test' as the first argument" ;;
  esac
}

main "${@}"
