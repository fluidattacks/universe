{ utils }:
utils.ssl-cert {
  name = "integrates";
  options = [
    "-subj"
    (builtins.concatStringsSep "" [
      "/C=CO"
      "/CN=fluidattacks.com"
      "/emailAddress=development@fluidattacks.com"
      "/L=Medellin"
      "/O=Fluid Attacks"
      "/ST=Antioquia"
    ])
  ];
}
