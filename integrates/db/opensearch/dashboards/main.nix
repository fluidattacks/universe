{ inputs, isLinux, makeScript, managePorts, ... }:
let
  version = "2.15.0";
  # https://github.com/NixOS/nixpkgs/issues/287096
  opensearch-dashboards = inputs.nixpkgs.stdenv.mkDerivation {
    inherit version;
    installPhase = ''
      mkdir -p $out
      mv * $out
      rm -r $out/node

      wrapProgram $out/bin/opensearch-dashboards \
        --set NODE_HOME "${inputs.nixpkgs.nodejs_18}"
    '';
    name = "opensearch-dashboards";
    nativeBuildInputs = [ inputs.nixpkgs.makeBinaryWrapper ];
    src = inputs.nixpkgs.fetchurl {
      url =
        "https://artifacts.opensearch.org/releases/core/opensearch-dashboards/${version}/opensearch-dashboards-min-${version}-linux-arm64.tar.gz";
      sha256 = "sha256-qIKEueKFjM0YicmgZw4Py0KN9SHy1EX7JJwXHeYETqU=";
    };
  };
in makeScript {
  entrypoint = ./entrypoint.sh;
  name = "opensearch-dashboards";
  replace = { __argOpensearchDashboards__ = opensearch-dashboards; };
  searchPaths = {
    bin = [ opensearch-dashboards ];
    source = [ managePorts ];
  };
}
