# shellcheck shell=bash

function main {
  local env="${1-}"

  : && if test "${env}" = 'prod'; then
    : aws_login "dev" "3600" \
      && sops_export_vars common/secrets/dev.yaml \
        INTEGRATES_API_TOKEN \
      && source __argIntegratesBackEnv__/template "${env}"
  else
    trap "integrates-db stop" EXIT \
      && DAEMON=true integrates-db \
      && export AWS_S3_PATH_PREFIX="${CI_COMMIT_REF_NAME}-charts-snapshots/" \
      && populate_storage "/${CI_COMMIT_REF_NAME}-charts-snapshots" \
      && source __argIntegratesBackEnv__/template "${env}"
  fi \
    && pushd integrates \
    && ENV_GECKO_DRIVER='__argGeckoDriver__' \
      ENV_FIREFOX='__argFirefox__' \
      RESULTS_DIR='back/integrates/charts/collector/reports' \
      python3 'back/integrates/cli/invoker.py' "integrates.charts.collector.generate_reports.main" \
    && aws_s3_sync \
      'back/integrates/charts/collector' \
      "s3://integrates/analytics/${CI_COMMIT_REF_NAME}/snapshots" \
      --exclude "*" --include "*.png" \
    && popd \
    || return 1
}

main "${@}"
