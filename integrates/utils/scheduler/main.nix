{ inputs, makeScript, outputs, ... }:
makeScript {
  entrypoint = ./entrypoint.sh;
  name = "integrates-scheduler";
  replace.__argIntegratesBackEnv__ = outputs."/integrates/back/env";
  searchPaths = {
    bin = [ inputs.nixpkgs.gnugrep inputs.nixpkgs.tokei ];
    source = [ outputs."/common/utils/aws" outputs."/common/utils/sops" ];
  };
}
