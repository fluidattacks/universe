# shellcheck shell=bash

function main {
  local env="${1-}"
  local module="${2-}"

  echo "[INFO] Waking up: ${module}" \
    && if [ "${env}" = "prod" ]; then
      aws_login "prod_integrates" "3600"
    fi \
    && source __argIntegratesBackEnv__/template "${env}" \
    && if test -z "${module-}"; then
      echo '[ERROR] Second argument must be the module to execute' \
        && return 1
    fi \
    && pushd integrates \
    && python3 'back/integrates/cli/invoker.py' "${module}" \
    && popd \
    || return 1
}

main "${@}"
