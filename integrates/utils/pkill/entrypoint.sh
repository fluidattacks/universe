# shellcheck shell=bash

function main {
  pkill -TERM -f "${1}"
}

main "${@}"
