package toe_port_state

import (
	"fluidattacks.com/types/toe_ports"
)

#toePortStatePk: =~"^GROUP#[a-z]+#PORTS#ROOT#[a-f0-9-]{36}#ADDRESS#(?:\\d{1,3}\\.){3}\\d{1,3}#PORT#\\d{1,5}$"
#toePortStateSk: =~"^STATE#state#DATE#(\\d{4})-(\\d{2})-(\\d{2})(T(\\d{2}):(\\d{2}):(\\d{2})(\\.\\d+)?(Z|([+-])(\\d{2}):(\\d{2})))?$"

#ToePortStateKeys: {
	pk:   string & #toePortStatePk
	sk:   string & #toePortStateSk
	pk_2: string
	pk_3: string
	sk_2: string
	sk_3: string
}

#ToePortStateItem: {
	#ToePortStateKeys
	toe_ports.#ToePortState
}

ToePortState:
{
	FacetName: "toe_port_state"
	KeyAttributeAlias: {
		PartitionKeyAlias: "GROUP#group_name#PORTS#ROOT#root_id#ADDRESS#address#PORT#port"
		SortKeyAlias:      "STATE#state#DATE#iso8601utc"
	}
	NonKeyAttributes: [
		"pk_2",
		"pk_3",
		"sk_2",
		"sk_3",
		"attacked_at",
		"attacked_by",
		"be_present",
		"be_present_until",
		"first_attack_at",
		"has_vulnerabilities",
		"modified_by",
		"modified_date",
		"seen_at",
		"seen_first_time_by",
	]
	DataAccess: {
		MySql: {}
	}
}

ToePortState: TableData: [
	{
		seen_first_time_by:  "test2@test.com"
		has_vulnerabilities: false
		sk:                  "STATE#state#DATE#2022-12-15T23:17:05.638952+00:00"
		modified_by:         "test2@test.com"
		sk_3:                "STATE#state#DATE#2022-12-15T23:17:05.638952+00:00"
		pk_2:                "GROUP#PORTS#ROOT#ADDRESS#PORT#oneshottest"
		pk:                  "GROUP#oneshottest#PORTS#ROOT#d312f0b9-da49-4d2b-a881-bed438875e99#ADDRESS#127.0.0.1#PORT#8080"
		pk_3:                "GROUP#oneshottest"
		be_present:          true
		modified_date:       "2022-12-15T23:17:05.638952+00:00"
		sk_2:                "STATE#state#DATE#2022-12-15T23:17:05.638952+00:00"
		seen_at:             "2022-12-15T23:17:05.638936+00:00"
	},
	{
		seen_first_time_by:  "test2@test.com"
		has_vulnerabilities: false
		sk:                  "STATE#state#DATE#2022-12-15T23:17:28.940918+00:00"
		modified_by:         "test2@test.com"
		sk_3:                "STATE#state#DATE#2022-12-15T23:17:28.940918+00:00"
		pk_2:                "GROUP#PORTS#ROOT#ADDRESS#PORT#oneshottest"
		pk:                  "GROUP#oneshottest#PORTS#ROOT#d312f0b9-da49-4d2b-a881-bed438875e99#ADDRESS#127.0.0.1#PORT#443"
		pk_3:                "GROUP#oneshottest"
		be_present:          true
		modified_date:       "2022-12-15T23:17:28.940918+00:00"
		sk_2:                "STATE#state#DATE#2022-12-15T23:17:28.940918+00:00"
		seen_at:             "2022-12-15T23:17:28.940909+00:00"
	},
	{
		seen_first_time_by:  "test2@test.com"
		has_vulnerabilities: false
		attacked_at:         "2022-12-15T23:26:15.369616+00:00"
		be_present:          true
		modified_date:       "2022-12-15T23:26:15.369655+00:00"
		attacked_by:         "test2@test.com"
		sk:                  "STATE#state#DATE#2022-12-15T23:26:15.369655+00:00"
		modified_by:         "test2@test.com"
		sk_3:                "STATE#state#DATE#2022-12-15T23:26:15.369655+00:00"
		pk_2:                "GROUP#PORTS#ROOT#ADDRESS#PORT#oneshottest"
		pk:                  "GROUP#oneshottest#PORTS#ROOT#d312f0b9-da49-4d2b-a881-bed438875e99#ADDRESS#127.0.0.1#PORT#443"
		pk_3:                "GROUP#oneshottest"
		first_attack_at:     "2022-12-15T23:26:15.369616+00:00"
		sk_2:                "STATE#state#DATE#2022-12-15T23:26:15.369655+00:00"
		seen_at:             "2022-12-15T23:17:28.940909+00:00"
	},
	{
		seen_first_time_by:  "test2@test.com"
		has_vulnerabilities: false
		attacked_at:         "2022-12-15T23:26:15.369616+00:00"
		be_present:          false
		modified_date:       "2022-12-15T23:26:22.269823+00:00"
		attacked_by:         "test2@test.com"
		be_present_until:    "2022-12-15T23:26:22.269810+00:00"
		sk:                  "STATE#state#DATE#2022-12-15T23:26:22.269823+00:00"
		modified_by:         "test2@test.com"
		sk_3:                "STATE#state#DATE#2022-12-15T23:26:22.269823+00:00"
		pk_2:                "GROUP#PORTS#ROOT#ADDRESS#PORT#oneshottest"
		pk:                  "GROUP#oneshottest#PORTS#ROOT#d312f0b9-da49-4d2b-a881-bed438875e99#ADDRESS#127.0.0.1#PORT#443"
		pk_3:                "GROUP#oneshottest"
		first_attack_at:     "2022-12-15T23:26:15.369616+00:00"
		sk_2:                "STATE#state#DATE#2022-12-15T23:26:22.269823+00:00"
		seen_at:             "2022-12-15T23:17:28.940909+00:00"
	},
	{
		seen_first_time_by:  "test2@test.com"
		has_vulnerabilities: false
		sk:                  "STATE#state#DATE#2022-12-15T23:17:34.129688+00:00"
		modified_by:         "test2@test.com"
		sk_3:                "STATE#state#DATE#2022-12-15T23:17:34.129688+00:00"
		pk_2:                "GROUP#PORTS#ROOT#ADDRESS#PORT#oneshottest"
		pk:                  "GROUP#oneshottest#PORTS#ROOT#d312f0b9-da49-4d2b-a881-bed438875e99#ADDRESS#127.0.0.1#PORT#21"
		pk_3:                "GROUP#oneshottest"
		be_present:          true
		modified_date:       "2022-12-15T23:17:34.129688+00:00"
		sk_2:                "STATE#state#DATE#2022-12-15T23:17:34.129688+00:00"
		seen_at:             "2022-12-15T23:17:34.129679+00:00"
	},
	{
		seen_first_time_by:  "test2@test.com"
		has_vulnerabilities: false
		attacked_at:         "2022-12-15T23:26:15.371462+00:00"
		be_present:          true
		modified_date:       "2022-12-15T23:26:15.371483+00:00"
		attacked_by:         "test2@test.com"
		sk:                  "STATE#state#DATE#2022-12-15T23:26:15.371483+00:00"
		modified_by:         "test2@test.com"
		sk_3:                "STATE#state#DATE#2022-12-15T23:26:15.371483+00:00"
		pk_2:                "GROUP#PORTS#ROOT#ADDRESS#PORT#oneshottest"
		pk:                  "GROUP#oneshottest#PORTS#ROOT#d312f0b9-da49-4d2b-a881-bed438875e99#ADDRESS#127.0.0.1#PORT#21"
		pk_3:                "GROUP#oneshottest"
		first_attack_at:     "2022-12-15T23:26:15.371462+00:00"
		sk_2:                "STATE#state#DATE#2022-12-15T23:26:15.371483+00:00"
		seen_at:             "2022-12-15T23:17:34.129679+00:00"
	},
	{
		seen_first_time_by:  "test2@test.com"
		has_vulnerabilities: false
		sk:                  "STATE#state#DATE#2022-12-15T23:17:55.869143+00:00"
		modified_by:         "test2@test.com"
		sk_3:                "STATE#state#DATE#2022-12-15T23:17:55.869143+00:00"
		pk_2:                "GROUP#PORTS#ROOT#ADDRESS#PORT#oneshottest"
		pk:                  "GROUP#oneshottest#PORTS#ROOT#d312f0b9-da49-4d2b-a881-bed438875e99#ADDRESS#127.0.0.1#PORT#995"
		pk_3:                "GROUP#oneshottest"
		be_present:          true
		modified_date:       "2022-12-15T23:17:55.869143+00:00"
		sk_2:                "STATE#state#DATE#2022-12-15T23:17:55.869143+00:00"
		seen_at:             "2022-12-15T23:17:55.869123+00:00"
	},
	{
		seen_first_time_by:  "test2@test.com"
		has_vulnerabilities: false
		be_present:          false
		modified_date:       "2022-12-15T23:26:23.286765+00:00"
		be_present_until:    "2022-12-15T23:26:23.286754+00:00"
		sk:                  "STATE#state#DATE#2022-12-15T23:26:23.286765+00:00"
		modified_by:         "test2@test.com"
		sk_3:                "STATE#state#DATE#2022-12-15T23:26:23.286765+00:00"
		pk_2:                "GROUP#PORTS#ROOT#ADDRESS#PORT#oneshottest"
		pk:                  "GROUP#oneshottest#PORTS#ROOT#d312f0b9-da49-4d2b-a881-bed438875e99#ADDRESS#127.0.0.1#PORT#995"
		pk_3:                "GROUP#oneshottest"
		sk_2:                "STATE#state#DATE#2022-12-15T23:26:23.286765+00:00"
		seen_at:             "2022-12-15T23:17:55.869123+00:00"
	},
	{
		seen_first_time_by:  "test2@test.com"
		has_vulnerabilities: false
		sk:                  "STATE#state#DATE#2022-12-15T23:17:46.186185+00:00"
		modified_by:         "test2@test.com"
		sk_3:                "STATE#state#DATE#2022-12-15T23:17:46.186185+00:00"
		pk_2:                "GROUP#PORTS#ROOT#ADDRESS#PORT#oneshottest"
		pk:                  "GROUP#oneshottest#PORTS#ROOT#d312f0b9-da49-4d2b-a881-bed438875e99#ADDRESS#127.0.0.1#PORT#22"
		pk_3:                "GROUP#oneshottest"
		be_present:          true
		modified_date:       "2022-12-15T23:17:46.186185+00:00"
		sk_2:                "STATE#state#DATE#2022-12-15T23:17:46.186185+00:00"
		seen_at:             "2022-12-15T23:17:46.186174+00:00"
	},
	{
		seen_first_time_by:  "test2@test.com"
		has_vulnerabilities: false
		sk:                  "STATE#state#DATE#2022-12-15T23:17:51.060925+00:00"
		modified_by:         "test2@test.com"
		sk_3:                "STATE#state#DATE#2022-12-15T23:17:51.060925+00:00"
		pk_2:                "GROUP#PORTS#ROOT#ADDRESS#PORT#oneshottest"
		pk:                  "GROUP#oneshottest#PORTS#ROOT#d312f0b9-da49-4d2b-a881-bed438875e99#ADDRESS#127.0.0.1#PORT#110"
		pk_3:                "GROUP#oneshottest"
		be_present:          true
		modified_date:       "2022-12-15T23:17:51.060925+00:00"
		sk_2:                "STATE#state#DATE#2022-12-15T23:17:51.060925+00:00"
		seen_at:             "2022-12-15T23:17:51.060916+00:00"
	},
	{
		seen_first_time_by:  "test2@test.com"
		has_vulnerabilities: false
		attacked_at:         "2022-12-15T23:26:15.366560+00:00"
		be_present:          true
		modified_date:       "2022-12-15T23:26:15.366618+00:00"
		attacked_by:         "test2@test.com"
		sk:                  "STATE#state#DATE#2022-12-15T23:26:15.366618+00:00"
		modified_by:         "test2@test.com"
		sk_3:                "STATE#state#DATE#2022-12-15T23:26:15.366618+00:00"
		pk_2:                "GROUP#PORTS#ROOT#ADDRESS#PORT#oneshottest"
		pk:                  "GROUP#oneshottest#PORTS#ROOT#d312f0b9-da49-4d2b-a881-bed438875e99#ADDRESS#127.0.0.1#PORT#110"
		pk_3:                "GROUP#oneshottest"
		first_attack_at:     "2022-12-15T23:26:15.366560+00:00"
		sk_2:                "STATE#state#DATE#2022-12-15T23:26:15.366618+00:00"
		seen_at:             "2022-12-15T23:17:51.060916+00:00"
	},
] & [...#ToePortStateItem]
