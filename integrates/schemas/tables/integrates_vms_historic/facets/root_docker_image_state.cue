package root_docker_image_state

import (
	"fluidattacks.com/types/roots"
)

#rootDockerImageStatePk: =~"^GROUP#[a-z]+#ROOT#[a-z0-9-]+#URI#[a-z0-9-]+"
#rootDockerImageStateSk: =~"^STATE#state#DATE#(\\d{4}-[01]\\d-[0-3]\\d([Tt][0-2]\\d:[0-5]\\d(:[0-5]\\d(\\.\\d{1,9})?)?([Zz]|([+-][0-2]\\d:[0-5]\\d))?)?)$"

#RootDockerImageStateKeys: {
	pk: string & #rootDockerImageStatePk
	sk: string & #rootDockerImageStateSk
}

#RootDockerImageStateItem: {
	#RootDockerImageStateKeys
	roots.#RootDockerImageState
}

RootDockerImageState:
{
	FacetName: "root_docker_image_state"
	KeyAttributeAlias: {
		PartitionKeyAlias: "GROUP#group_name#ROOT#uuid#URI#uri"
		SortKeyAlias:      "STATE#state#DATE#iso8601utc"
	}
	NonKeyAttributes: [
		"credential_id",
		"modified_by",
		"modified_date",
		"include",
		"status",
		"digest",
		"size",
		"history",
		"layers",
	]
	DataAccess: {
		MySql: {}
	}
}

RootDockerImageState: TableData: [
	{
		credential_id: "dd2f08da-be59-4f97-8117-00f1cbbf6aec"
		include:       true
		sk:            "STATE#state#DATE#2024-03-13T17:33:52.935263+00:00"
		modified_by:   "integrates@fluidattacks.com"
		pk:            "GROUP#unittesting#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19#URI#docker.io/library/ubuntu:latest"
		modified_date: "2024-03-13T17:33:52.935263+00:00"
		status:        "CREATED"
		digest:        "sha256:2e863c44b718727c860746568e1d54afd13b2fa71b160f5cd9058fc436217b30"
		size:          10091
		history:
		[
			{
				created:     "2024-04-10T18:52:02.793393865Z"
				created_by:  "ARG RELEASE"
				empty_layer: true
			},
			{
				created:     "2024-04-10T18:52:02.828022213Z"
				created_by:  "ARG LAUNCHPAD_BUILD_ARCH"
				empty_layer: true
			},
			{
				created:     "2024-04-10T18:52:02.855792154Z"
				created_by:  "LABEL org.opencontainers.image.ref.name=ubuntu"
				empty_layer: true
			},
			{
				created:     "2024-04-10T18:52:02.883498404Z"
				created_by:  "LABEL org.opencontainers.image.version=22.04"
				empty_layer: true
			},
			{
				created:    "2024-04-10T18:52:04.750464835Z"
				created_by: "ADD file:3bd10da0673e2e72cb06a1f64a9df49a36341df39b0f762e3d1b38ee4de296fa in / "
			},
			{
				created:     "2024-04-10T18:52:04.937319396Z"
				created_by:  "CMD [\"/bin/bash\"]"
				empty_layer: true
			},
		]
		layers: {SS: [
			"sha256:e0a9f5911802534ba097660206feabeb0247a81e409029167b30e2e1f2803b57",
		]}
	},
	{
		credential_id: "dd2f08da-be59-4f97-8117-00f1cbbf6aec"
		include:       true
		sk:            "STATE#state#DATE#2024-03-13T17:33:52.935263+00:00"
		modified_by:   "integrates@fluidattacks.com"
		pk:            "GROUP#unittesting#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19#URI#docker.io/nginx:latest"
		modified_date: "2024-03-13T17:33:52.935263+00:00"
		status:        "CREATED"
		digest:        "sha256:67682bda769fae1ccf5183192b8daf37b64cae99c6c3302650f6f8bf5f0f95df"
		size:          10091
		history:
		[
			{
				created:    "2024-06-21T02:12:35Z"
				created_by: "/bin/sh -c #(nop) ADD file:b24689567a7c604de93e4ef1dc87c372514f692556744da43925c575b4f80df6 in /"
			},
			{
				created:     "2024-06-21T02:12:35Z"
				created_by:  "/bin/sh -c #(nop)  CMD [\"bash\"]"
				empty_layer: true
			},
			{
				created:     "2024-06-21T02:12:35Z"
				created_by:  "LABEL maintainer=NGINX Docker Maintainers \u003cdocker-maint@nginx.com\u003e"
				comment:     "buildkit.dockerfile.v0"
				empty_layer: true
			},
		]
		layers: {SS: [
			"sha256:32148f9f6c5aadfa167ee7b146b9703c59307049d68b090c19db019fd15c5406",
			"sha256:32cfaf91376fefc4934558561815920002a94dffaf52bc67b7382ea7869553b6",
			"sha256:933a3ce2c78a89e4646f6314e7e5b10ffd5d7f4d72ecf7a320d5a3e110f1e146",
		]}
	},
	{
		credential_id: "dd2f08da-be59-4f97-8117-00f1cbbf6aec"
		include:       true
		sk:            "STATE#state#DATE#2024-03-13T17:33:52.935263+00:00"
		modified_by:   "integrates@fluidattacks.com"
		pk:            "GROUP#unittesting#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19#URI#docker.io/mongo:latest"
		modified_date: "2024-03-13T17:33:52.935263+00:00"
		status:        "CREATED"
		digest:        "sha256:54996a559c724c726a31fb8131e1c9088a05f7e531760e2897212389bbf20fed"
		size:          10091
		history:
		[
			{
				created:     "2024-06-27T20:10:10.528110614Z"
				created_by:  "/bin/sh -c #(nop)  ARG RELEASE"
				empty_layer: true
			},
			{
				created:     "2024-06-27T20:10:10.566300625Z"
				created_by:  "/bin/sh -c #(nop)  ARG LAUNCHPAD_BUILD_ARCH"
				empty_layer: true
			},
			{
				created:     "2024-06-27T20:10:10.606737029Z"
				created_by:  "/bin/sh -c #(nop)  LABEL org.opencontainers.image.ref.name=ubuntu"
				empty_layer: true
			},
		]
		layers: {SS: [
			"sha256:931b7ff0cb6f494b27d31a4cbec3efe62ac54676add9c7469560302f1541ecaf",
			"sha256:5aa8dfb74c5eea943287c0f8406eec5393bb48c44b288c8f073cbe7037cc2387",
			"sha256:5ee4f093a153f722212a0fd67ed29562a336cea9236ccd53a722c72fa0698af7",
		]}
	},
] & [...#RootDockerImageStateItem]
