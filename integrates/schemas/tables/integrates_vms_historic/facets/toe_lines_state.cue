package toe_lines_state

import (
	"fluidattacks.com/types/toe_lines"
)

#toeLinesStatePk: =~"^GROUP#[a-z]+#LINES#ROOT#[a-f0-9-]{36}#FILENAME#[a-z0-9./_#-]+$"
#toeLinesStateSk: =~"^STATE#state#DATE#(\\d{4})-(\\d{2})-(\\d{2})(T(\\d{2}):(\\d{2}):(\\d{2})(\\.\\d+)?(Z|([+-])(\\d{2}):(\\d{2})))?$"

#ToeLinesStateKeys: {
	pk:   string & #toeLinesStatePk
	sk:   string & #toeLinesStateSk
	pk_2: string
	pk_3: string
	sk_2: string
	sk_3: string
}

#ToeLinesStateItem: {
	#ToeLinesStateKeys
	toe_lines.#ToeLinesState
}

ToeLinesState:
{
	FacetName: "toe_lines_state"
	KeyAttributeAlias: {
		PartitionKeyAlias: "GROUP#group_name#LINES#ROOT#root_id#FILENAME#filename"
		SortKeyAlias:      "STATE#state#DATE#iso8601utc"
	}
	NonKeyAttributes: [
		"pk_2",
		"pk_3",
		"sk_2",
		"sk_3",
		"attacked_at",
		"attacked_by",
		"attacked_lines",
		"be_present",
		"be_present_until",
		"comments",
		"first_attack_at",
		"has_vulnerabilities",
		"last_author",
		"last_commit",
		"last_commit_date",
		"loc",
		"modified_by",
		"modified_date",
		"seen_at",
		"sorts_risk_level",
		"sorts_priority_factor",
		"sorts_risk_level_date",
		"sorts_suggestions",
	]
	DataAccess: {
		MySql: {}
	}
}

ToeLinesState: TableData: [
	{
		loc:                   53
		last_commit:           "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c1"
		sorts_risk_level_date: "2021-04-10T05:00:00+00:00"
		be_present:            true
		attacked_by:           "test@test.com"
		sk:                    "STATE#state#DATE#2020-11-16T15:41:04+00:00"
		pk_2:                  "GROUP#LINES#ROOT#FILENAME#barranquilla"
		pk_3:                  "GROUP#barranquilla"
		first_attack_at:       "2020-01-19T15:41:04+00:00"
		sorts_priority_factor: 82
		comments:              "comment 15"
		sorts_risk_level:      92
		sorts_suggestions:
		[
			{
				finding_title: "083. XML injection (XXE)"
				probability:   90
			},
			{
				finding_title: "033. Password change without identity check"
				probability:   50
			},
		]
		last_author:         "user@gmail.com"
		has_vulnerabilities: false
		attacked_at:         "2021-01-20T05:00:00+00:00"
		modified_date:       "2020-11-16T15:41:04+00:00"
		be_present_until:    "2021-01-01T15:41:04+00:00"
		attacked_lines:      53
		modified_by:         "test2@test.com"
		sk_3:                "STATE#state#DATE#2020-11-16T15:41:04+00:00"
		pk:                  "GROUP#barranquilla#LINES#ROOT#2e349e7f-390e-40e9-a8b6-2983eaee92ff#FILENAME#path/to/test13.py"
		last_commit_date:    "2020-11-16T15:41:04+00:00"
		sk_2:                "STATE#state#DATE#2020-11-16T15:41:04+00:00"
		seen_at:             "2020-01-01T15:41:04+00:00"
	},
	{
		loc:                   75
		last_commit:           "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c1"
		sorts_risk_level_date: "2021-04-10T05:00:00+00:00"
		be_present:            true
		attacked_by:           "test@test.com"
		sk:                    "STATE#state#DATE#2020-11-16T15:41:04+00:00"
		pk_2:                  "GROUP#LINES#ROOT#FILENAME#barranquilla"
		pk_3:                  "GROUP#barranquilla"
		first_attack_at:       "2020-01-19T15:41:04+00:00"
		sorts_priority_factor: 20
		comments:              "comment 16"
		sorts_risk_level:      30
		sorts_suggestions:
		[
			{
				finding_title: "027. Insecure file upload"
				probability:   100
			},
		]
		last_author:         "user@gmail.com"
		has_vulnerabilities: false
		attacked_at:         "2021-01-20T05:00:00+00:00"
		modified_date:       "2020-11-16T15:41:04+00:00"
		be_present_until:    "2021-01-01T15:41:04+00:00"
		attacked_lines:      70
		modified_by:         "test2@test.com"
		sk_3:                "STATE#state#DATE#2020-11-16T15:41:04+00:00"
		pk:                  "GROUP#barranquilla#LINES#ROOT#2e349e7f-390e-40e9-a8b6-2983eaee92ff#FILENAME#path/to/test14.py"
		last_commit_date:    "2020-11-16T15:41:04+00:00"
		sk_2:                "STATE#state#DATE#2020-11-16T15:41:04+00:00"
		seen_at:             "2020-01-01T15:41:04+00:00"
	},
	{
		loc:                   540
		last_commit:           "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c1"
		sorts_risk_level_date: "2021-04-10T05:00:00+00:00"
		be_present:            false
		attacked_by:           "test@test.com"
		sk:                    "STATE#state#DATE#2020-11-16T15:41:04+00:00"
		pk_2:                  "GROUP#LINES#ROOT#FILENAME#barranquilla"
		pk_3:                  "GROUP#barranquilla"
		first_attack_at:       "2020-01-19T15:41:04+00:00"
		sorts_priority_factor: 18
		comments:              "comment 10"
		sorts_risk_level:      28
		sorts_suggestions:
		[
			{
				finding_title: "083. XML injection (XXE)"
				probability:   90
			},
			{
				finding_title: "033. Password change without identity check"
				probability:   50
			},
		]
		last_author:         "user@gmail.com"
		has_vulnerabilities: false
		attacked_at:         "2021-01-20T05:00:00+00:00"
		modified_date:       "2020-11-16T15:41:04+00:00"
		be_present_until:    "2021-01-01T15:41:04+00:00"
		attacked_lines:      0
		modified_by:         "test2@test.com"
		sk_3:                "STATE#state#DATE#2020-11-16T15:41:04+00:00"
		pk:                  "GROUP#barranquilla#LINES#ROOT#2e349e7f-390e-40e9-a8b6-2983eaee92ff#FILENAME#path/to/test8.py"
		last_commit_date:    "2020-11-16T15:41:04+00:00"
		sk_2:                "STATE#state#DATE#2020-11-16T15:41:04+00:00"
		seen_at:             "2020-01-01T15:41:04+00:00"
	},
	{
		loc:                   8
		last_commit:           "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c1"
		sorts_risk_level_date: "2021-04-10T05:00:00+00:00"
		be_present:            true
		attacked_by:           "test@test.com"
		sk:                    "STATE#state#DATE#2020-11-16T15:41:04+00:00"
		pk_2:                  "GROUP#LINES#ROOT#FILENAME#barranquilla"
		pk_3:                  "GROUP#barranquilla"
		first_attack_at:       "2020-01-19T15:41:04+00:00"
		sorts_priority_factor: 45
		comments:              "comment 12"
		sorts_risk_level:      55
		sorts_suggestions:
		[
			{
				finding_title: "033. Password change without identity check"
				probability:   50
			},
		]
		last_author:         "user@gmail.com"
		has_vulnerabilities: false
		attacked_at:         "2021-01-20T05:00:00+00:00"
		modified_date:       "2020-11-16T15:41:04+00:00"
		be_present_until:    "2021-01-01T15:41:04+00:00"
		attacked_lines:      0
		modified_by:         "test2@test.com"
		sk_3:                "STATE#state#DATE#2020-11-16T15:41:04+00:00"
		pk:                  "GROUP#barranquilla#LINES#ROOT#2e349e7f-390e-40e9-a8b6-2983eaee92ff#FILENAME#path/to/test10.py"
		last_commit_date:    "2020-11-16T15:41:04+00:00"
		sk_2:                "STATE#state#DATE#2020-11-16T15:41:04+00:00"
		seen_at:             "2020-01-01T15:41:04+00:00"
	},
	{
		loc:              350
		comments:         "comment 5"
		sorts_risk_level: 100
		last_commit:      "e17059d1e17059d1e17059d1e17059d1e17059d1"
		sorts_suggestions:
		[
			{
				finding_title: "027. Insecure file upload"
				probability:   100
			},
		]
		last_author:           "user@gmail.com"
		has_vulnerabilities:   false
		sorts_risk_level_date: "2021-03-30T05:00:00+00:00"
		attacked_at:           "2021-02-20T05:00:00+00:00"
		be_present:            true
		modified_date:         "2020-11-15T15:41:04+00:00"
		attacked_by:           "test2@test.com"
		attacked_lines:        50
		sk:                    "STATE#state#DATE#2020-11-15T15:41:04+00:00"
		modified_by:           "test2@test.com"
		sk_3:                  "STATE#state#DATE#2020-11-15T15:41:04+00:00"
		pk_2:                  "GROUP#LINES#ROOT#FILENAME#unittesting"
		pk:                    "GROUP#unittesting#LINES#ROOT#765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a#FILENAME#path/to/test3.py"
		pk_3:                  "GROUP#unittesting"
		first_attack_at:       "2020-02-19T15:41:04+00:00"
		last_commit_date:      "2020-11-15T15:41:04+00:00"
		sk_2:                  "STATE#state#DATE#2020-11-15T15:41:04+00:00"
		sorts_priority_factor: 90
		seen_at:               "2020-02-01T15:41:04+00:00"
	},
	{
		loc:              350
		comments:         "comment 5"
		sorts_risk_level: 100
		last_commit:      "e17059d1e17059d1e17059d1e17059d1e17059d1"
		sorts_suggestions:
		[
			{
				finding_title: "027. Insecure file upload"
				probability:   100
			},
		]
		last_author:           "user@gmail.com"
		has_vulnerabilities:   false
		sorts_risk_level_date: "2021-03-30T05:00:00+00:00"
		attacked_at:           "2021-02-20T05:00:00+00:00"
		be_present:            true
		modified_date:         "2021-02-20T05:00:00+00:00"
		attacked_by:           "test2@test.com"
		attacked_lines:        50
		sk:                    "STATE#state#DATE#2021-02-20T05:00:00+00:00"
		modified_by:           "test2@test.com"
		sk_3:                  "STATE#state#DATE#2021-02-20T05:00:00+00:00"
		pk_2:                  "GROUP#LINES#ROOT#FILENAME#unittesting"
		pk:                    "GROUP#unittesting#LINES#ROOT#765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a#FILENAME#path/to/test3.py"
		pk_3:                  "GROUP#unittesting"
		first_attack_at:       "2020-02-19T15:41:04+00:00"
		sk_2:                  "STATE#state#DATE#2021-02-20T05:00:00+00:00"
		sorts_priority_factor: 90
		seen_at:               "2020-02-01T15:41:04+00:00"
	},
	{
		loc:              180
		comments:         "comment 1"
		sorts_risk_level: 80
		last_commit:      "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c2"
		sorts_suggestions:
		[
			{
				finding_title: "083. XML injection (XXE)"
				probability:   90
			},
			{
				finding_title: "033. Password change without identity check"
				probability:   50
			},
		]
		last_author:           "user@gmail.com"
		has_vulnerabilities:   false
		sorts_risk_level_date: "2021-02-20T05:00:00+00:00"
		attacked_at:           "2021-02-20T05:00:00+00:00"
		be_present:            true
		modified_date:         "2020-11-15T15:41:04+00:00"
		attacked_by:           "test2@test.com"
		attacked_lines:        4
		sk:                    "STATE#state#DATE#2020-11-15T15:41:04+00:00"
		modified_by:           "test2@test.com"
		sk_3:                  "STATE#state#DATE#2020-11-15T15:41:04+00:00"
		pk_2:                  "GROUP#LINES#ROOT#FILENAME#unittesting"
		pk:                    "GROUP#unittesting#LINES#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19#FILENAME#test/test#.config"
		pk_3:                  "GROUP#unittesting"
		first_attack_at:       "2020-02-19T15:41:04+00:00"
		last_commit_date:      "2020-11-15T15:41:04+00:00"
		sk_2:                  "STATE#state#DATE#2020-11-15T15:41:04+00:00"
		sorts_priority_factor: 70
		seen_at:               "2020-02-01T15:41:04+00:00"
	},
	{
		loc:              180
		comments:         "comment 1"
		sorts_risk_level: 80
		last_commit:      "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c2"
		sorts_suggestions:
		[
			{
				finding_title: "083. XML injection (XXE)"
				probability:   90
			},
			{
				finding_title: "033. Password change without identity check"
				probability:   50
			},
		]
		last_author:           "user@gmail.com"
		has_vulnerabilities:   false
		sorts_risk_level_date: "2021-02-20T05:00:00+00:00"
		attacked_at:           "2021-02-20T05:00:00+00:00"
		be_present:            true
		modified_date:         "2021-02-20T05:00:00+00:00"
		attacked_by:           "test2@test.com"
		attacked_lines:        4
		sk:                    "STATE#state#DATE#2021-02-20T05:00:00+00:00"
		modified_by:           "test2@test.com"
		sk_3:                  "STATE#state#DATE#2021-02-20T05:00:00+00:00"
		pk_2:                  "GROUP#LINES#ROOT#FILENAME#unittesting"
		pk:                    "GROUP#unittesting#LINES#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19#FILENAME#test/test#.config"
		pk_3:                  "GROUP#unittesting"
		first_attack_at:       "2020-02-19T15:41:04+00:00"
		sk_2:                  "STATE#state#DATE#2021-02-20T05:00:00+00:00"
		sorts_priority_factor: 70
		seen_at:               "2020-02-01T15:41:04+00:00"
	},
	{
		loc:              180
		comments:         "comment 1"
		sorts_risk_level: 80
		last_commit:      "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c2"
		sorts_suggestions:
		[
			{
				finding_title: "083. XML injection (XXE)"
				probability:   90
			},
			{
				finding_title: "033. Password change without identity check"
				probability:   50
			},
		]
		last_author:           "user@gmail.com"
		has_vulnerabilities:   false
		attacked_at:           "2021-02-20T05:00:00+00:00"
		be_present:            true
		sorts_risk_level_date: "2021-02-20T05:00:00+00:00"
		modified_date:         "2022-02-11T05:00:00+00:00"
		attacked_by:           "test2@test.com"
		attacked_lines:        4
		sk:                    "STATE#state#DATE#2022-02-11T05:00:00+00:00"
		modified_by:           "test2@test.com"
		sk_3:                  "STATE#state#DATE#2022-02-11T05:00:00+00:00"
		pk_2:                  "GROUP#LINES#ROOT#FILENAME#unittesting"
		pk:                    "GROUP#unittesting#LINES#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19#FILENAME#test/test#.config"
		pk_3:                  "GROUP#unittesting"
		first_attack_at:       "2020-02-19T15:41:04+00:00"
		sk_2:                  "STATE#state#DATE#2022-02-11T05:00:00+00:00"
		sorts_priority_factor: 70
		seen_at:               "2020-02-01T15:41:04+00:00"
	},
	{
		loc:              100
		comments:         "comment 4"
		sorts_risk_level: 80
		last_commit:      "e17059d1e17059d1e17059d1e17059d1e17059d1"
		sorts_suggestions:
		[
			{
				finding_title: "033. Password change without identity check"
				probability:   50
			},
		]
		last_author:           "user@gmail.com"
		has_vulnerabilities:   false
		sorts_risk_level_date: "2021-03-30T05:00:00+00:00"
		attacked_at:           "2021-02-20T05:00:00+00:00"
		be_present:            true
		modified_date:         "2020-11-15T15:41:04+00:00"
		attacked_by:           "test2@test.com"
		attacked_lines:        0
		sk:                    "STATE#state#DATE#2020-11-15T15:41:04+00:00"
		modified_by:           "test2@test.com"
		sk_3:                  "STATE#state#DATE#2020-11-15T15:41:04+00:00"
		pk_2:                  "GROUP#LINES#ROOT#FILENAME#unittesting"
		pk:                    "GROUP#unittesting#LINES#ROOT#765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a#FILENAME#path/to/test2.py"
		pk_3:                  "GROUP#unittesting"
		first_attack_at:       "2020-02-19T15:41:04+00:00"
		last_commit_date:      "2020-11-15T15:41:04+00:00"
		sk_2:                  "STATE#state#DATE#2020-11-15T15:41:04+00:00"
		sorts_priority_factor: 70
		seen_at:               "2020-02-01T15:41:04+00:00"
	},
	{
		loc:              100
		comments:         "comment 4"
		sorts_risk_level: 80
		last_commit:      "e17059d1e17059d1e17059d1e17059d1e17059d1"
		sorts_suggestions:
		[
			{
				finding_title: "033. Password change without identity check"
				probability:   50
			},
		]
		last_author:           "user@gmail.com"
		has_vulnerabilities:   false
		sorts_risk_level_date: "2021-03-30T05:00:00+00:00"
		attacked_at:           "2021-02-20T05:00:00+00:00"
		be_present:            true
		modified_date:         "2021-02-20T05:00:00+00:00"
		attacked_by:           "test2@test.com"
		attacked_lines:        0
		sk:                    "STATE#state#DATE#2021-02-20T05:00:00+00:00"
		modified_by:           "test2@test.com"
		sk_3:                  "STATE#state#DATE#2021-02-20T05:00:00+00:00"
		pk_2:                  "GROUP#LINES#ROOT#FILENAME#unittesting"
		pk:                    "GROUP#unittesting#LINES#ROOT#765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a#FILENAME#path/to/test2.py"
		pk_3:                  "GROUP#unittesting"
		first_attack_at:       "2020-02-19T15:41:04+00:00"
		sk_2:                  "STATE#state#DATE#2021-02-20T05:00:00+00:00"
		sorts_priority_factor: 70
		seen_at:               "2020-02-01T15:41:04+00:00"
	},
	{
		loc:                   172
		last_commit:           "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c1"
		sorts_risk_level_date: "2021-04-10T05:00:00+00:00"
		be_present:            false
		attacked_by:           "test@test.com"
		sk:                    "STATE#state#DATE#2020-11-16T15:41:04+00:00"
		pk_2:                  "GROUP#LINES#ROOT#FILENAME#unittesting"
		pk_3:                  "GROUP#unittesting"
		first_attack_at:       "2020-01-19T15:41:04+00:00"
		sorts_priority_factor: 10
		comments:              "comment 2"
		sorts_risk_level:      0
		sorts_suggestions:
		[
			{
				finding_title: "027. Insecure file upload"
				probability:   100
			},
		]
		last_author:         "user@gmail.com"
		has_vulnerabilities: false
		attacked_at:         "2021-01-20T05:00:00+00:00"
		modified_date:       "2020-11-16T15:41:04+00:00"
		be_present_until:    "2021-01-01T15:41:04+00:00"
		attacked_lines:      120
		modified_by:         "test2@test.com"
		sk_3:                "STATE#state#DATE#2020-11-16T15:41:04+00:00"
		pk:                  "GROUP#unittesting#LINES#ROOT#765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a#FILENAME#test2/test.sh"
		last_commit_date:    "2020-11-16T15:41:04+00:00"
		sk_2:                "STATE#state#DATE#2020-11-16T15:41:04+00:00"
		seen_at:             "2020-01-01T15:41:04+00:00"
	},
	{
		loc:              172
		comments:         "comment 2"
		sorts_risk_level: 0
		last_commit:      "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c1"
		sorts_suggestions:
		[
			{
				finding_title: "027. Insecure file upload"
				probability:   100
			},
		]
		last_author:           "user@gmail.com"
		has_vulnerabilities:   false
		sorts_risk_level_date: "2021-04-10T05:00:00+00:00"
		attacked_at:           "2021-01-20T05:00:00+00:00"
		be_present:            false
		modified_date:         "2021-02-20T05:00:00+00:00"
		attacked_by:           "test@test.com"
		be_present_until:      "2021-01-01T15:41:04+00:00"
		attacked_lines:        120
		sk:                    "STATE#state#DATE#2021-02-20T05:00:00+00:00"
		modified_by:           "test2@test.com"
		sk_3:                  "STATE#state#DATE#2021-02-20T05:00:00+00:00"
		pk_2:                  "GROUP#LINES#ROOT#FILENAME#unittesting"
		pk:                    "GROUP#unittesting#LINES#ROOT#765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a#FILENAME#test2/test.sh"
		pk_3:                  "GROUP#unittesting"
		first_attack_at:       "2020-01-19T15:41:04+00:00"
		sk_2:                  "STATE#state#DATE#2021-02-20T05:00:00+00:00"
		sorts_priority_factor: 15
		seen_at:               "2020-01-01T15:41:04+00:00"
	},
	{
		loc:                   738
		last_commit:           "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c1"
		sorts_risk_level_date: "2021-04-10T05:00:00+00:00"
		be_present:            true
		attacked_by:           "test@test.com"
		sk:                    "STATE#state#DATE#2020-11-16T15:41:04+00:00"
		pk_2:                  "GROUP#LINES#ROOT#FILENAME#barranquilla"
		pk_3:                  "GROUP#barranquilla"
		first_attack_at:       "2020-01-19T15:41:04+00:00"
		sorts_priority_factor: 13
		comments:              "comment 17"
		sorts_risk_level:      23
		sorts_suggestions:
		[
			{
				finding_title: "027. Insecure file upload"
				probability:   100
			},
		]
		last_author:         "user@gmail.com"
		has_vulnerabilities: false
		attacked_at:         "2021-01-20T05:00:00+00:00"
		modified_date:       "2020-11-16T15:41:04+00:00"
		be_present_until:    "2021-01-01T15:41:04+00:00"
		attacked_lines:      34
		modified_by:         "test2@test.com"
		sk_3:                "STATE#state#DATE#2020-11-16T15:41:04+00:00"
		pk:                  "GROUP#barranquilla#LINES#ROOT#2e349e7f-390e-40e9-a8b6-2983eaee92ff#FILENAME#path/to/test15.py"
		last_commit_date:    "2020-11-16T15:41:04+00:00"
		sk_2:                "STATE#state#DATE#2020-11-16T15:41:04+00:00"
		seen_at:             "2020-01-01T15:41:04+00:00"
	},
	{
		loc:              34
		comments:         "comment 6"
		sorts_risk_level: 80
		last_commit:      "e17059d1e17059d1e17059d1e17059d1e17059d1"
		sorts_suggestions:
		[
			{
				finding_title: "083. XML injection (XXE)"
				probability:   90
			},
			{
				finding_title: "033. Password change without identity check"
				probability:   50
			},
		]
		last_author:           "user@gmail.com"
		has_vulnerabilities:   false
		sorts_risk_level_date: "2021-03-30T05:00:00+00:00"
		attacked_at:           "2021-02-20T05:00:00+00:00"
		be_present:            true
		modified_date:         "2020-11-15T15:41:04+00:00"
		attacked_by:           "test2@test.com"
		attacked_lines:        10
		sk:                    "STATE#state#DATE#2020-11-15T15:41:04+00:00"
		modified_by:           "test2@test.com"
		sk_3:                  "STATE#state#DATE#2020-11-15T15:41:04+00:00"
		pk_2:                  "GROUP#LINES#ROOT#FILENAME#unittesting"
		pk:                    "GROUP#unittesting#LINES#ROOT#765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a#FILENAME#path/to/test4.py"
		pk_3:                  "GROUP#unittesting"
		first_attack_at:       "2020-02-19T15:41:04+00:00"
		last_commit_date:      "2020-11-15T15:41:04+00:00"
		sk_2:                  "STATE#state#DATE#2020-11-15T15:41:04+00:00"
		sorts_priority_factor: 70
		seen_at:               "2020-02-01T15:41:04+00:00"
	},
	{
		loc:              34
		comments:         "comment 6"
		sorts_risk_level: 80
		last_commit:      "e17059d1e17059d1e17059d1e17059d1e17059d1"
		sorts_suggestions:
		[
			{
				finding_title: "083. XML injection (XXE)"
				probability:   90
			},
			{
				finding_title: "033. Password change without identity check"
				probability:   50
			},
		]
		last_author:           "user@gmail.com"
		has_vulnerabilities:   false
		sorts_risk_level_date: "2021-03-30T05:00:00+00:00"
		attacked_at:           "2021-02-20T05:00:00+00:00"
		be_present:            true
		modified_date:         "2021-02-20T05:00:00+00:00"
		attacked_by:           "test2@test.com"
		attacked_lines:        10
		sk:                    "STATE#state#DATE#2021-02-20T05:00:00+00:00"
		modified_by:           "test2@test.com"
		sk_3:                  "STATE#state#DATE#2021-02-20T05:00:00+00:00"
		pk_2:                  "GROUP#LINES#ROOT#FILENAME#unittesting"
		pk:                    "GROUP#unittesting#LINES#ROOT#765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a#FILENAME#path/to/test4.py"
		pk_3:                  "GROUP#unittesting"
		first_attack_at:       "2020-02-19T15:41:04+00:00"
		sk_2:                  "STATE#state#DATE#2021-02-20T05:00:00+00:00"
		sorts_priority_factor: 70
		seen_at:               "2020-02-01T15:41:04+00:00"
	},
	{
		loc:                   789
		last_commit:           "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c1"
		sorts_risk_level_date: "2021-04-10T05:00:00+00:00"
		be_present:            true
		attacked_by:           "test@test.com"
		sk:                    "STATE#state#DATE#2020-11-16T15:41:04+00:00"
		pk_2:                  "GROUP#LINES#ROOT#FILENAME#barranquilla"
		pk_3:                  "GROUP#barranquilla"
		first_attack_at:       "2020-01-19T15:41:04+00:00"
		sorts_priority_factor: 20
		comments:              "comment 19"
		sorts_risk_level:      30
		sorts_suggestions:
		[
			{
				finding_title: "083. XML injection (XXE)"
				probability:   90
			},
		]
		last_author:         "user@gmail.com"
		has_vulnerabilities: false
		attacked_at:         "2021-01-20T05:00:00+00:00"
		modified_date:       "2020-11-16T15:41:04+00:00"
		be_present_until:    "2021-01-01T15:41:04+00:00"
		attacked_lines:      0
		modified_by:         "test2@test.com"
		sk_3:                "STATE#state#DATE#2020-11-16T15:41:04+00:00"
		pk:                  "GROUP#barranquilla#LINES#ROOT#2e349e7f-390e-40e9-a8b6-2983eaee92ff#FILENAME#path/to/test17.py"
		last_commit_date:    "2020-11-16T15:41:04+00:00"
		sk_2:                "STATE#state#DATE#2020-11-16T15:41:04+00:00"
		seen_at:             "2020-01-01T15:41:04+00:00"
	},
	{
		loc:              500
		comments:         "comment 3"
		sorts_risk_level: 50
		last_commit:      "e17059d1e17059d1e17059d1e17059d1e17059d1"
		sorts_suggestions:
		[
			{
				finding_title: "083. XML injection (XXE)"
				probability:   90
			},
		]
		last_author:           "user@gmail.com"
		has_vulnerabilities:   false
		sorts_risk_level_date: "2021-03-30T05:00:00+00:00"
		attacked_at:           "2021-02-20T05:00:00+00:00"
		be_present:            false
		modified_date:         "2020-11-15T15:41:04+00:00"
		attacked_by:           "test2@test.com"
		attacked_lines:        500
		sk:                    "STATE#state#DATE#2020-11-15T15:41:04+00:00"
		modified_by:           "test2@test.com"
		sk_3:                  "STATE#state#DATE#2020-11-15T15:41:04+00:00"
		pk_2:                  "GROUP#LINES#ROOT#FILENAME#unittesting"
		pk:                    "GROUP#unittesting#LINES#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19#FILENAME#path/to/test1.py"
		pk_3:                  "GROUP#unittesting"
		first_attack_at:       "2020-02-19T15:41:04+00:00"
		last_commit_date:      "2020-11-15T15:41:04+00:00"
		sk_2:                  "STATE#state#DATE#2020-11-15T15:41:04+00:00"
		sorts_priority_factor: 40
		seen_at:               "2020-02-01T15:41:04+00:00"
	},
	{
		loc:              500
		comments:         "comment 3"
		sorts_risk_level: 50
		last_commit:      "e17059d1e17059d1e17059d1e17059d1e17059d1"
		sorts_suggestions:
		[
			{
				finding_title: "083. XML injection (XXE)"
				probability:   90
			},
		]
		last_author:           "user@gmail.com"
		has_vulnerabilities:   false
		sorts_risk_level_date: "2021-03-30T05:00:00+00:00"
		attacked_at:           "2021-02-20T05:00:00+00:00"
		be_present:            false
		modified_date:         "2021-02-20T05:00:00+00:00"
		attacked_by:           "test2@test.com"
		attacked_lines:        500
		sk:                    "STATE#state#DATE#2021-02-20T05:00:00+00:00"
		modified_by:           "test2@test.com"
		sk_3:                  "STATE#state#DATE#2021-02-20T05:00:00+00:00"
		pk_2:                  "GROUP#LINES#ROOT#FILENAME#unittesting"
		pk:                    "GROUP#unittesting#LINES#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19#FILENAME#path/to/test1.py"
		pk_3:                  "GROUP#unittesting"
		first_attack_at:       "2020-02-19T15:41:04+00:00"
		sk_2:                  "STATE#state#DATE#2021-02-20T05:00:00+00:00"
		sorts_priority_factor: 40
		seen_at:               "2020-02-01T15:41:04+00:00"
	},
	{
		loc:                   345
		last_commit:           "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c1"
		sorts_risk_level_date: "2021-04-10T05:00:00+00:00"
		be_present:            true
		attacked_by:           "test@test.com"
		sk:                    "STATE#state#DATE#2020-11-16T15:41:04+00:00"
		pk_2:                  "GROUP#LINES#ROOT#FILENAME#barranquilla"
		pk_3:                  "GROUP#barranquilla"
		first_attack_at:       "2020-01-19T15:41:04+00:00"
		sorts_priority_factor: 65
		comments:              "comment 13"
		sorts_risk_level:      75
		sorts_suggestions:
		[
			{
				finding_title: "083. XML injection (XXE)"
				probability:   90
			},
		]
		last_author:         "user@gmail.com"
		has_vulnerabilities: false
		attacked_at:         "2021-01-20T05:00:00+00:00"
		modified_date:       "2020-11-16T15:41:04+00:00"
		be_present_until:    "2021-01-01T15:41:04+00:00"
		attacked_lines:      345
		modified_by:         "test2@test.com"
		sk_3:                "STATE#state#DATE#2020-11-16T15:41:04+00:00"
		pk:                  "GROUP#barranquilla#LINES#ROOT#2e349e7f-390e-40e9-a8b6-2983eaee92ff#FILENAME#path/to/test11.py"
		last_commit_date:    "2020-11-16T15:41:04+00:00"
		sk_2:                "STATE#state#DATE#2020-11-16T15:41:04+00:00"
		seen_at:             "2020-01-01T15:41:04+00:00"
	},
	{
		loc:                   382
		last_commit:           "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c1"
		sorts_risk_level_date: "2021-04-10T05:00:00+00:00"
		be_present:            true
		attacked_by:           "test@test.com"
		sk:                    "STATE#state#DATE#2020-11-16T15:41:04+00:00"
		pk_2:                  "GROUP#LINES#ROOT#FILENAME#barranquilla"
		pk_3:                  "GROUP#barranquilla"
		first_attack_at:       "2020-01-19T15:41:04+00:00"
		sorts_priority_factor: 2
		comments:              "comment 18"
		sorts_risk_level:      12
		sorts_suggestions:
		[
			{
				finding_title: "027. Insecure file upload"
				probability:   100
			},
		]
		last_author:         "user@gmail.com"
		has_vulnerabilities: false
		attacked_at:         "2021-01-20T05:00:00+00:00"
		modified_date:       "2020-11-16T15:41:04+00:00"
		be_present_until:    "2021-01-01T15:41:04+00:00"
		attacked_lines:      19
		modified_by:         "test2@test.com"
		sk_3:                "STATE#state#DATE#2020-11-16T15:41:04+00:00"
		pk:                  "GROUP#barranquilla#LINES#ROOT#2e349e7f-390e-40e9-a8b6-2983eaee92ff#FILENAME#path/to/test16.py"
		last_commit_date:    "2020-11-16T15:41:04+00:00"
		sk_2:                "STATE#state#DATE#2020-11-16T15:41:04+00:00"
		seen_at:             "2020-01-01T15:41:04+00:00"
	},
	{
		loc:                   982
		last_commit:           "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c1"
		sorts_risk_level_date: "2021-04-10T05:00:00+00:00"
		be_present:            true
		attacked_by:           "test@test.com"
		sk:                    "STATE#state#DATE#2020-11-16T15:41:04+00:00"
		pk_2:                  "GROUP#LINES#ROOT#FILENAME#barranquilla"
		pk_3:                  "GROUP#barranquilla"
		first_attack_at:       "2020-01-19T15:41:04+00:00"
		sorts_priority_factor: 90
		comments:              "comment 14"
		sorts_risk_level:      100
		sorts_suggestions:
		[
			{
				finding_title: "083. XML injection (XXE)"
				probability:   90
			},
			{
				finding_title: "033. Password change without identity check"
				probability:   50
			},
		]
		last_author:         "user@gmail.com"
		has_vulnerabilities: false
		attacked_at:         "2021-01-20T05:00:00+00:00"
		modified_date:       "2020-11-16T15:41:04+00:00"
		be_present_until:    "2021-01-01T15:41:04+00:00"
		attacked_lines:      844
		modified_by:         "test2@test.com"
		sk_3:                "STATE#state#DATE#2020-11-16T15:41:04+00:00"
		pk:                  "GROUP#barranquilla#LINES#ROOT#2e349e7f-390e-40e9-a8b6-2983eaee92ff#FILENAME#path/to/test12.py"
		last_commit_date:    "2020-11-16T15:41:04+00:00"
		sk_2:                "STATE#state#DATE#2020-11-16T15:41:04+00:00"
		seen_at:             "2020-01-01T15:41:04+00:00"
	},
	{
		loc:              350
		comments:         "comment 1"
		sorts_risk_level: 80
		last_commit:      "e17059d1e17059d1e17059d1e17059d1e17059d1"
		sorts_suggestions:
		[
			{
				finding_title: "083. XML injection (XXE)"
				probability:   90
			},
			{
				finding_title: "033. Password change without identity check"
				probability:   50
			},
		]
		last_author:           "user@gmail.com"
		has_vulnerabilities:   false
		sorts_risk_level_date: "2021-03-30T05:00:00+00:00"
		attacked_at:           "2021-02-20T05:00:00+00:00"
		be_present:            true
		modified_date:         "2020-11-15T15:41:04+00:00"
		attacked_by:           "test2@test.com"
		attacked_lines:        4
		sk:                    "STATE#state#DATE#2020-11-15T15:41:04+00:00"
		modified_by:           "test2@test.com"
		sk_3:                  "STATE#state#DATE#2020-11-15T15:41:04+00:00"
		pk_2:                  "GROUP#LINES#ROOT#FILENAME#unittesting"
		pk:                    "GROUP#unittesting#LINES#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19#FILENAME#universe/path/to/file3.ext"
		pk_3:                  "GROUP#unittesting"
		first_attack_at:       "2020-02-19T15:41:04+00:00"
		last_commit_date:      "2020-11-15T15:41:04+00:00"
		sk_2:                  "STATE#state#DATE#2020-11-15T15:41:04+00:00"
		sorts_priority_factor: 70
		seen_at:               "2020-02-01T15:41:04+00:00"
	},
	{
		loc:                   37
		last_commit:           "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c1"
		sorts_risk_level_date: "2021-04-10T05:00:00+00:00"
		be_present:            true
		attacked_by:           "test@test.com"
		sk:                    "STATE#state#DATE#2020-11-16T15:41:04+00:00"
		pk_2:                  "GROUP#LINES#ROOT#FILENAME#barranquilla"
		pk_3:                  "GROUP#barranquilla"
		first_attack_at:       "2020-01-19T15:41:04+00:00"
		sorts_priority_factor: 68
		comments:              "comment 11"
		sorts_risk_level:      78
		sorts_suggestions:
		[
			{
				finding_title: "027. Insecure file upload"
				probability:   100
			},
		]
		last_author:         "user@gmail.com"
		has_vulnerabilities: false
		attacked_at:         "2021-01-20T05:00:00+00:00"
		modified_date:       "2020-11-16T15:41:04+00:00"
		be_present_until:    "2021-01-01T15:41:04+00:00"
		attacked_lines:      12
		modified_by:         "test2@test.com"
		sk_3:                "STATE#state#DATE#2020-11-16T15:41:04+00:00"
		pk:                  "GROUP#barranquilla#LINES#ROOT#2e349e7f-390e-40e9-a8b6-2983eaee92ff#FILENAME#path/to/test9.py"
		last_commit_date:    "2020-11-16T15:41:04+00:00"
		sk_2:                "STATE#state#DATE#2020-11-16T15:41:04+00:00"
		seen_at:             "2020-01-01T15:41:04+00:00"
	},
] & [...#ToeLinesStateItem]
