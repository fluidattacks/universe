package group_access_state

import (
	"fluidattacks.com/types/group_access"
)

#groupAccessStatePk: =~"^USER#[a-zA-Z0-9._%+-@]+#GROUP#[a-z]+$"
#groupAccessStateSk: =~"^STATE#state#DATE#(\\d{4}-[01]\\d-[0-3]\\d([Tt][0-2]\\d:[0-5]\\d(:[0-5]\\d(\\.\\d{1,9})?)?([Zz]|([+-][0-2]\\d:[0-5]\\d))?)?)$"

#GroupAccessStateKeys: {
	pk:   string & #groupAccessStatePk
	sk:   string & #groupAccessStateSk
	pk_2: string & =~"^USER#GROUP#[a-z]+$"
	pk_3: string & =~"^GROUP#[a-zA-Z0-9]+$"
	sk_2: string & #groupAccessStateSk
	sk_3: string & #groupAccessStateSk
}

#GroupAccessStateItem: {
	group_access.#GroupAccessState
	#GroupAccessStateKeys
}

GroupAccessState:
{
	FacetName: "group_access_state"
	KeyAttributeAlias: {
		PartitionKeyAlias: "USER#email#GROUP#name"
		SortKeyAlias:      "STATE#state#DATE#iso8601utc"
	}
	NonKeyAttributes: [
		"pk_2",
		"pk_3",
		"sk_2",
		"sk_3",
		"modified_by",
		"modified_date",
		"confirm_deletion",
		"has_access",
		"invitation",
		"responsibility",
		"role",
	]
	DataAccess: {
		MySql: {}
	}
}

GroupAccessState: TableData: [
	{
		role:           "admin"
		responsibility: "Test"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#asgard"
		has_access:     true
		pk:             "USER#aaguirre@fluidattacks.com#GROUP#asgard"
		pk_3:           "GROUP#asgard"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "admin"
		responsibility: "Test"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#barranquilla"
		has_access:     true
		pk:             "USER#integratesmanager@gmail.com#GROUP#barranquilla"
		pk_3:           "GROUP#barranquilla"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "admin"
		responsibility: "Test"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#metropolis"
		has_access:     true
		pk:             "USER#integratesmanager@gmail.com#GROUP#metropolis"
		pk_3:           "GROUP#metropolis"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "admin"
		responsibility: "Test"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#monteria"
		has_access:     true
		pk:             "USER#integratesmanager@gmail.com#GROUP#monteria"
		pk_3:           "GROUP#monteria"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "customer_manager"
		responsibility: "Test Owner"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#unittesting"
		has_access:     true
		pk:             "USER#customer_manager@fluidattacks.com#GROUP#unittesting"
		pk_3:           "GROUP#unittesting"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "reattacker"
		responsibility: "Test"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#oneshottest"
		has_access:     true
		pk:             "USER#integrateshacker@fluidattacks.com#GROUP#oneshottest"
		pk_3:           "GROUP#oneshottest"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "admin"
		responsibility: "Test"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#monteria"
		has_access:     true
		pk:             "USER#aaguirre@fluidattacks.com#GROUP#monteria"
		pk_3:           "GROUP#monteria"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "customer_manager"
		responsibility: "Tester"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#unittesting"
		has_access:     true
		pk:             "USER#unittest2@fluidattacks.com#GROUP#unittesting"
		pk_3:           "GROUP#unittesting"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "admin"
		responsibility: "Test"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#monteria"
		has_access:     true
		pk:             "USER#integratesmanager@fluidattacks.com#GROUP#monteria"
		pk_3:           "GROUP#monteria"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "admin"
		responsibility: "Test"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#monteria"
		has_access:     true
		pk:             "USER#continuoushacking@gmail.com#GROUP#monteria"
		pk_3:           "GROUP#monteria"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "user"
		responsibility: "Test"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#unittesting"
		has_access:     true
		pk:             "USER#integratesuser2@fluidattacks.com#GROUP#unittesting"
		pk_3:           "GROUP#unittesting"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "service_forces"
		responsibility: "Test"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#unittesting"
		has_access:     true
		pk:             "USER#integratesserviceforces@fluidattacks.com#GROUP#unittesting"
		pk_3:           "GROUP#unittesting"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "admin"
		responsibility: "Test"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#asgard"
		has_access:     true
		pk:             "USER#continuoushacking@gmail.com#GROUP#asgard"
		pk_3:           "GROUP#asgard"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "user"
		responsibility: ""
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#sheele"
		has_access:     true
		pk:             "USER#org_testuser3@gmail.com#GROUP#sheele"
		pk_3:           "GROUP#sheele"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "resourcer"
		responsibility: "Test"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#unittesting"
		has_access:     true
		pk:             "USER#integratesresourcer@fluidattacks.com#GROUP#unittesting"
		pk_3:           "GROUP#unittesting"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "hacker"
		responsibility: "to be deleted by inactivity, first attempt"
		invitation: {
			url_token:      "unknown"
			role:           "hacker"
			responsibility: "to be deleted by inactivity, first attempt"
			is_used:        true
		}
		sk:            "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:          "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:          "USER#GROUP#deleteimamura"
		has_access:    true
		pk:            "USER#inactive_imamura1@fluidattacks.com#GROUP#deleteimamura"
		pk_3:          "GROUP#deleteimamura"
		modified_date: "2020-01-01T20:07:57+00:00"
		sk_2:          "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		responsibility: "Test"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#oneshottest"
		has_access:     true
		pk:             "USER#integratesmanager@fluidattacks.com#GROUP#oneshottest"
		pk_3:           "GROUP#oneshottest"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "admin"
		responsibility: "Test"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#metropolis"
		has_access:     true
		pk:             "USER#continuoushacking@gmail.com#GROUP#metropolis"
		pk_3:           "GROUP#metropolis"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "admin"
		responsibility: "Test"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#asgard"
		has_access:     true
		pk:             "USER#integratesmanager@gmail.com#GROUP#asgard"
		pk_3:           "GROUP#asgard"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "user"
		responsibility: "Test"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#oneshottest"
		has_access:     true
		pk:             "USER#integratesuser@gmail.com#GROUP#oneshottest"
		pk_3:           "GROUP#oneshottest"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "group_manager"
		responsibility: "Test"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#unittesting"
		has_access:     true
		pk:             "USER#continuoushacking@gmail.com#GROUP#unittesting"
		pk_3:           "GROUP#unittesting"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "group_manager"
		responsibility: "Test"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#barranquilla"
		has_access:     true
		pk:             "USER#integratesuser@gmail.com#GROUP#barranquilla"
		pk_3:           "GROUP#barranquilla"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "hacker"
		responsibility: "to be deleted by inactivity, second attempt"
		invitation: {
			url_token:      "unknown"
			role:           "hacker"
			responsibility: "to be deleted by inactivity, second attempt"
			is_used:        true
		}
		sk:            "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:          "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:          "USER#GROUP#deleteimamura"
		has_access:    true
		pk:            "USER#inactive_imamura2@fluidattacks.com#GROUP#deleteimamura"
		pk_3:          "GROUP#deleteimamura"
		modified_date: "2020-01-01T20:07:57+00:00"
		sk_2:          "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "admin"
		responsibility: "Test"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#barranquilla"
		has_access:     true
		pk:             "USER#aaguirre@fluidattacks.com#GROUP#barranquilla"
		pk_3:           "GROUP#barranquilla"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "admin"
		responsibility: "Test"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#metropolis"
		has_access:     true
		pk:             "USER#aaguirre@fluidattacks.com#GROUP#metropolis"
		pk_3:           "GROUP#metropolis"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "admin"
		responsibility: "Test"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#gotham"
		has_access:     true
		pk:             "USER#integratesmanager@gmail.com#GROUP#gotham"
		pk_3:           "GROUP#gotham"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "user"
		responsibility: "Not to be deleted by inactivity"
		invitation: {
			url_token:      "unknown"
			role:           "user"
			responsibility: "Not to be deleted by inactivity"
			is_used:        true
		}
		sk:            "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:          "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:          "USER#GROUP#deleteimamura"
		has_access:    true
		pk:            "USER#active_imamura3@fluidattacks.com#GROUP#deleteimamura"
		pk_3:          "GROUP#deleteimamura"
		modified_date: "2020-01-01T20:07:57+00:00"
		sk_2:          "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "group_manager"
		responsibility: "Test"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#unittesting"
		has_access:     true
		pk:             "USER#integratesuser@gmail.com#GROUP#unittesting"
		pk_3:           "GROUP#unittesting"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "admin"
		responsibility: "Test"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#gotham"
		has_access:     true
		pk:             "USER#integratesmanager@fluidattacks.com#GROUP#gotham"
		pk_3:           "GROUP#gotham"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "group_manager"
		responsibility: "Test"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#metropolis"
		has_access:     true
		pk:             "USER#integratesuser@gmail.com#GROUP#metropolis"
		pk_3:           "GROUP#metropolis"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "group_manager"
		responsibility: "Test"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#gotham"
		has_access:     true
		pk:             "USER#integratesuser@gmail.com#GROUP#gotham"
		pk_3:           "GROUP#gotham"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "group_manager"
		responsibility: "Test"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#monteria"
		has_access:     true
		pk:             "USER#integratesuser@gmail.com#GROUP#monteria"
		pk_3:           "GROUP#monteria"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "admin"
		responsibility: "Test"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#unittesting"
		has_access:     true
		pk:             "USER#aaguirre@fluidattacks.com#GROUP#unittesting"
		pk_3:           "GROUP#unittesting"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		responsibility: "Tester"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#unittesting"
		has_access:     true
		pk:             "USER#unittest@fluidattacks.com#GROUP#unittesting"
		pk_3:           "GROUP#unittesting"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "admin"
		responsibility: "Test"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#metropolis"
		has_access:     true
		pk:             "USER#integratesmanager@fluidattacks.com#GROUP#metropolis"
		pk_3:           "GROUP#metropolis"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "group_manager"
		responsibility: "Test"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#asgard"
		has_access:     true
		pk:             "USER#integratesuser@gmail.com#GROUP#asgard"
		pk_3:           "GROUP#asgard"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		responsibility: "Test"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#unittesting"
		has_access:     true
		pk:             "USER#integratesmanager@gmail.com#GROUP#unittesting"
		pk_3:           "GROUP#unittesting"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		responsibility: "Test"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#oneshottest"
		has_access:     true
		pk:             "USER#integratesmanager@gmail.com#GROUP#oneshottest"
		pk_3:           "GROUP#oneshottest"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "reattacker"
		responsibility: "Test reattacker"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#unittesting"
		has_access:     true
		pk:             "USER#integratesreattacker@fluidattacks.com#GROUP#unittesting"
		pk_3:           "GROUP#unittesting"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "group_manager"
		responsibility: "Test"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#unittesting"
		has_access:     true
		pk:             "USER#continuoushack2@gmail.com#GROUP#unittesting"
		pk_3:           "GROUP#unittesting"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "admin"
		responsibility: "Test"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#asgard"
		has_access:     true
		pk:             "USER#integratesmanager@fluidattacks.com#GROUP#asgard"
		pk_3:           "GROUP#asgard"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "admin"
		responsibility: "Test"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#gotham"
		has_access:     true
		pk:             "USER#continuoushacking@gmail.com#GROUP#gotham"
		pk_3:           "GROUP#gotham"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "reviewer"
		responsibility: "Test"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#unittesting"
		has_access:     true
		pk:             "USER#integratesreviewer@fluidattacks.com#GROUP#unittesting"
		pk_3:           "GROUP#unittesting"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "group_manager"
		responsibility: "Test"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#oneshottest"
		has_access:     true
		pk:             "USER#continuoushacking@gmail.com#GROUP#oneshottest"
		pk_3:           "GROUP#oneshottest"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "vulnerability_manager"
		responsibility: "Test vulnerability manager"
		invitation: {
			url_token:      "unknown"
			role:           "vulnerability_manager"
			responsibility: "Test vulnerability manager"
			is_used:        true
		}
		sk:            "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:          "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:          "USER#GROUP#unittesting"
		has_access:    true
		pk:            "USER#vulnmanager@gmail.com#GROUP#unittesting"
		pk_3:          "GROUP#unittesting"
		modified_date: "2020-01-01T20:07:57+00:00"
		sk_2:          "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		responsibility: "Test"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#unittesting"
		has_access:     true
		pk:             "USER#integratesmanager@fluidattacks.com#GROUP#unittesting"
		pk_3:           "GROUP#unittesting"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "admin"
		responsibility: "Test"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#barranquilla"
		has_access:     true
		pk:             "USER#integratesmanager@fluidattacks.com#GROUP#barranquilla"
		pk_3:           "GROUP#barranquilla"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "admin"
		responsibility: "Test"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#gotham"
		has_access:     true
		pk:             "USER#aaguirre@fluidattacks.com#GROUP#gotham"
		pk_3:           "GROUP#gotham"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "admin"
		responsibility: "Test"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#barranquilla"
		has_access:     true
		pk:             "USER#continuoushacking@gmail.com#GROUP#barranquilla"
		pk_3:           "GROUP#barranquilla"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "hacker"
		responsibility: "Test"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#unittesting"
		has_access:     true
		pk:             "USER#integrateshacker@fluidattacks.com#GROUP#unittesting"
		pk_3:           "GROUP#unittesting"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		responsibility: "Continuous Testing user"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#continuoustesting"
		has_access:     true
		pk:             "USER#integrateshacker@fluidattacks.com#GROUP#continuoustesting"
		pk_3:           "GROUP#continuoustesting"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "service_forces"
		responsibility: "Forces service user"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#unittesting"
		has_access:     true
		pk:             "USER#forces.unittesting@fluidattacks.com#GROUP#unittesting"
		pk_3:           "GROUP#unittesting"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "admin"
		responsibility: "Test"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#oneshottest"
		has_access:     true
		pk:             "USER#aaguirre@fluidattacks.com#GROUP#oneshottest"
		pk_3:           "GROUP#oneshottest"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
	{
		role:           "user"
		responsibility: "Test"
		sk:             "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		sk_3:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
		pk_2:           "USER#GROUP#unittesting"
		has_access:     true
		pk:             "USER#integratesuser2@gmail.com#GROUP#unittesting"
		pk_3:           "GROUP#unittesting"
		modified_date:  "2020-01-01T20:07:57+00:00"
		sk_2:           "STATE#state#DATE#2020-01-01T20:07:57+00:00"
	},
] & [...#GroupAccessStateItem]
