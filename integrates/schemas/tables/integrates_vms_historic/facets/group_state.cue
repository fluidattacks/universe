package group_state

import (
	"fluidattacks.com/types/groups"
)

#groupStatePk: =~"^GROUP#[a-z]+#ORG#[a-f0-9-]+$"
#groupStateSk: =~"^STATE#state#DATE#(\\d{4}-[01]\\d-[0-3]\\d([Tt][0-2]\\d:[0-5]\\d(:[0-5]\\d(\\.\\d{1,9})?)?([Zz]|([+-][0-2]\\d:[0-5]\\d))?)?)$"

#GroupStateKeys: {
	pk:   string & #groupStatePk
	sk:   string & #groupStateSk
	pk_2: string
	sk_2: string
	pk_3: string
	sk_3: string
}

#GroupStateItem: {
	groups.#GroupState
	#GroupStateKeys
}

GroupState:
{
	FacetName: "group_state"
	KeyAttributeAlias: {
		PartitionKeyAlias: "GROUP#name#ORG#organization_id"
		SortKeyAlias:      "STATE#state#DATE#iso8601utc"
	}
	NonKeyAttributes: [
		"pk_2",
		"pk_3",
		"sk_2",
		"sk_3",
		"modified_by",
		"modified_date",
		"has_essential",
		"has_advanced",
		"managed",
		"status",
		"tier",
		"type",
		"tags",
		"comments",
		"justification",
		"payment_id",
		"pending_deletion_date",
		"service",
	]
	DataAccess: {
		MySql: {}
	}
}

GroupState: TableData: [
	{
		has_advanced:  false
		modified_date: "2020-05-20T22:00:00+00:00"
		type:          "CONTINUOUS"
		tier:          "ONESHOT"
		managed:       "NOT_MANAGED"
		service:       "WHITE"
		modified_by:   "unknown"
		sk:            "STATE#state#DATE#2020-05-20T22:00:00+00:00"
		has_essential: false
		sk_3:          "STATE#state#DATE#2020-05-20T22:00:00+00:00"
		pk_2:          "GROUP#ORG#setpendingdeletion"
		pk:            "GROUP#setpendingdeletion#ORG#c6cecc0e-bb92-4079-8b6d-c4e815c10bb1"
		pk_3:          "GROUP#setpendingdeletion"
		sk_2:          "STATE#state#DATE#2020-05-20T22:00:00+00:00"
		status:        "ACTIVE"
	},
	{
		has_advanced:  false
		modified_date: "2020-05-20T22:00:00+00:00"
		type:          "CONTINUOUS"
		tags: [
			"test-newgroups",
		]
		tier:          "FREE"
		managed:       "NOT_MANAGED"
		service:       "WHITE"
		modified_by:   "unknown"
		sk:            "STATE#state#DATE#2020-05-20T22:00:00+00:00"
		has_essential: false
		sk_3:          "STATE#state#DATE#2020-05-20T22:00:00+00:00"
		pk_2:          "GROUP#ORG#monteria"
		pk:            "GROUP#monteria#ORG#956e9107-fd8d-49bc-b550-5609a7a1f6ac"
		pk_3:          "GROUP#monteria"
		sk_2:          "STATE#state#DATE#2020-05-20T22:00:00+00:00"
		status:        "ACTIVE"
	},
	{
		has_advanced:  true
		modified_date: "2020-05-20T22:00:00+00:00"
		type:          "CONTINUOUS"
		tags: [
			"test-nogroups",
		]
		tier:          "ESSENTIAL"
		managed:       "NOT_MANAGED"
		service:       "WHITE"
		modified_by:   "unknown"
		sk:            "STATE#state#DATE#2020-05-20T22:00:00+00:00"
		has_essential: false
		sk_3:          "STATE#state#DATE#2020-05-20T22:00:00+00:00"
		pk_2:          "GROUP#ORG#metropolis"
		pk:            "GROUP#metropolis#ORG#c6cecc0e-bb92-4079-8b6d-c4e815c10bb1"
		pk_3:          "GROUP#metropolis"
		sk_2:          "STATE#state#DATE#2020-05-20T22:00:00+00:00"
		status:        "ACTIVE"
	},
	{
		has_advanced:  true
		modified_date: "2020-05-20T22:00:00+00:00"
		type:          "CONTINUOUS"
		tier:          "ESSENTIAL"
		managed:       "NOT_MANAGED"
		service:       "WHITE"
		modified_by:   "unknown"
		sk:            "STATE#state#DATE#2020-05-20T22:00:00+00:00"
		has_essential: true
		sk_3:          "STATE#state#DATE#2020-05-20T22:00:00+00:00"
		pk_2:          "GROUP#ORG#deleteimamura"
		pk:            "GROUP#deleteimamura#ORG#7376c5fe-4634-4053-9718-e14ecbda1e6b"
		pk_3:          "GROUP#deleteimamura"
		sk_2:          "STATE#state#DATE#2020-05-20T22:00:00+00:00"
		status:        "ACTIVE"
	},
	{
		has_advanced:  false
		modified_date: "2020-05-20T22:00:00+00:00"
		type:          "CONTINUOUS"
		tags: [
			"test-nogroups",
		]
		tier:          "ONESHOT"
		managed:       "NOT_MANAGED"
		service:       "WHITE"
		modified_by:   "unknown"
		sk:            "STATE#state#DATE#2020-05-20T22:00:00+00:00"
		has_essential: false
		sk_3:          "STATE#state#DATE#2020-05-20T22:00:00+00:00"
		pk_2:          "GROUP#ORG#gotham"
		pk:            "GROUP#gotham#ORG#c6cecc0e-bb92-4079-8b6d-c4e815c10bb1"
		pk_3:          "GROUP#gotham"
		sk_2:          "STATE#state#DATE#2020-05-20T22:00:00+00:00"
		status:        "ACTIVE"
	},
	{
		has_advanced:  false
		modified_date: "2020-05-20T22:00:00+00:00"
		type:          "CONTINUOUS"
		tier:          "OTHER"
		managed:       "NOT_MANAGED"
		service:       "WHITE"
		modified_by:   "unknown"
		sk:            "STATE#state#DATE#2020-05-20T22:00:00+00:00"
		has_essential: false
		sk_3:          "STATE#state#DATE#2020-05-20T22:00:00+00:00"
		pk_2:          "GROUP#ORG#kurome"
		pk:            "GROUP#kurome#ORG#f2e2777d-a168-4bea-93cd-d79142b294d2"
		pk_3:          "GROUP#kurome"
		sk_2:          "STATE#state#DATE#2020-05-20T22:00:00+00:00"
		status:        "ACTIVE"
	},
	{
		has_advanced:  false
		modified_date: "2019-01-20T22:00:00+00:00"
		type:          "ONESHOT"
		tags: [
			"test-groups",
			"another-tag",
			"test-updates",
			"test-tag",
		]
		tier:          "ONESHOT"
		managed:       "NOT_MANAGED"
		service:       "BLACK"
		modified_by:   "unknown"
		sk:            "STATE#state#DATE#2019-01-20T22:00:00+00:00"
		has_essential: true
		sk_3:          "STATE#state#DATE#2019-01-20T22:00:00+00:00"
		pk_2:          "GROUP#ORG#oneshottest"
		pk:            "GROUP#oneshottest#ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
		pk_3:          "GROUP#oneshottest"
		sk_2:          "STATE#state#DATE#2019-01-20T22:00:00+00:00"
		status:        "ACTIVE"
	},
	{
		has_advanced:          false
		pending_deletion_date: "2020-12-22T19:36:29+00:00"
		modified_date:         "2020-05-20T22:00:00+00:00"
		type:                  "CONTINUOUS"
		tier:                  "OTHER"
		managed:               "NOT_MANAGED"
		service:               "WHITE"
		modified_by:           "unknown"
		sk:                    "STATE#state#DATE#2020-05-20T22:00:00+00:00"
		has_essential:         false
		sk_3:                  "STATE#state#DATE#2020-05-20T22:00:00+00:00"
		pk_2:                  "GROUP#ORG#deletegroup"
		pk:                    "GROUP#deletegroup#ORG#c6cecc0e-bb92-4079-8b6d-c4e815c10bb1"
		pk_3:                  "GROUP#deletegroup"
		sk_2:                  "STATE#state#DATE#2020-05-20T22:00:00+00:00"
		status:                "ACTIVE"
	},
	{
		has_advanced:  false
		modified_date: "2020-05-20T22:00:00+00:00"
		type:          "CONTINUOUS"
		tags: [
			"test-nogroups",
		]
		tier:          "OTHER"
		managed:       "NOT_MANAGED"
		service:       "WHITE"
		modified_by:   "unknown"
		sk:            "STATE#state#DATE#2020-05-20T22:00:00+00:00"
		has_essential: false
		sk_3:          "STATE#state#DATE#2020-05-20T22:00:00+00:00"
		pk_2:          "GROUP#ORG#asgard"
		pk:            "GROUP#asgard#ORG#c6cecc0e-bb92-4079-8b6d-c4e815c10bb1"
		pk_3:          "GROUP#asgard"
		sk_2:          "STATE#state#DATE#2020-05-20T22:00:00+00:00"
		status:        "ACTIVE"
	},
	{
		has_advanced:  false
		modified_date: "2020-05-20T22:00:00+00:00"
		type:          "CONTINUOUS"
		tags: ["test-newgroups"]
		tier:          "ADVANCED"
		managed:       "NOT_MANAGED"
		service:       "WHITE"
		modified_by:   "unknown"
		sk:            "STATE#state#DATE#2020-05-20T22:00:00+00:00"
		has_essential: false
		sk_3:          "STATE#state#DATE#2020-05-20T22:00:00+00:00"
		pk_2:          "GROUP#ORG#barranquilla"
		pk:            "GROUP#barranquilla#ORG#956e9107-fd8d-49bc-b550-5609a7a1f6ac"
		pk_3:          "GROUP#barranquilla"
		sk_2:          "STATE#state#DATE#2020-05-20T22:00:00+00:00"
		status:        "ACTIVE"
	},
	{
		has_advanced:  false
		modified_date: "2020-05-20T22:00:00+00:00"
		type:          "CONTINUOUS"
		tier:          "OTHER"
		managed:       "NOT_MANAGED"
		service:       "WHITE"
		modified_by:   "unknown"
		sk:            "STATE#state#DATE#2020-05-20T22:00:00+00:00"
		has_essential: false
		sk_3:          "STATE#state#DATE#2020-05-20T22:00:00+00:00"
		pk_2:          "GROUP#ORG#sheele"
		pk:            "GROUP#sheele#ORG#f2e2777d-a168-4bea-93cd-d79142b294d2"
		pk_3:          "GROUP#sheele"
		sk_2:          "STATE#state#DATE#2020-05-20T22:00:00+00:00"
		status:        "ACTIVE"
	},
	{
		has_advanced:  true
		modified_date: "2020-05-20T22:00:00+00:00"
		type:          "CONTINUOUS"
		tags: ["another-tag"]
		tier:          "FREE"
		managed:       "NOT_MANAGED"
		service:       "WHITE"
		modified_by:   "unknown"
		sk:            "STATE#state#DATE#2020-05-20T22:00:00+00:00"
		has_essential: true
		sk_3:          "STATE#state#DATE#2020-05-20T22:00:00+00:00"
		pk_2:          "GROUP#ORG#lubbock"
		pk:            "GROUP#lubbock#ORG#fe80d2d4-ccb7-46d1-8489-67c6360581de"
		pk_3:          "GROUP#lubbock"
		sk_2:          "STATE#state#DATE#2020-05-20T22:00:00+00:00"
		status:        "ACTIVE"
	},
	{
		has_advanced:  true
		modified_date: "2018-03-08T00:43:18+00:00"
		type:          "CONTINUOUS"
		tags: ["test-groups", "test-updates", "test-tag"]
		tier:          "ESSENTIAL"
		managed:       "NOT_MANAGED"
		service:       "WHITE"
		modified_by:   "unknown"
		sk:            "STATE#state#DATE#2018-03-08T00:43:18+00:00"
		has_essential: true
		sk_3:          "STATE#state#DATE#2018-03-08T00:43:18+00:00"
		pk_2:          "GROUP#ORG#unittesting"
		pk:            "GROUP#unittesting#ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
		pk_3:          "GROUP#unittesting"
		sk_2:          "STATE#state#DATE#2018-03-08T00:43:18+00:00"
		status:        "ACTIVE"
	},
	{
		has_advanced:  true
		modified_date: "2020-05-20T22:00:00+00:00"
		type:          "CONTINUOUS"
		tags: ["another-tag", "test-tag"]
		tier:          "ADVANCED"
		managed:       "NOT_MANAGED"
		service:       "WHITE"
		modified_by:   "unknown"
		sk:            "STATE#state#DATE#2020-05-20T22:00:00+00:00"
		has_essential: true
		sk_3:          "STATE#state#DATE#2020-05-20T22:00:00+00:00"
		pk_2:          "GROUP#ORG#continuoustesting"
		pk:            "GROUP#continuoustesting#ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
		pk_3:          "GROUP#continuoustesting"
		sk_2:          "STATE#state#DATE#2020-05-20T22:00:00+00:00"
		status:        "ACTIVE"
	},
] & [...#GroupStateItem]
