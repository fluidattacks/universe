package git_root_state

import (
	"fluidattacks.com/types/roots"
)

#gitRootStatePk: =~"^ROOT#[a-f0-9-]+#GROUP#[a-zA-Z0-9]+$"
#gitRootStateSk: =~"^STATE#state#DATE#(\\d{4}-[01]\\d-[0-3]\\d([Tt][0-2]\\d:[0-5]\\d(:[0-5]\\d(\\.\\d{1,9})?)?([Zz]|([+-][0-2]\\d:[0-5]\\d))?)?)$"

#GitRootStateKeys: {
	pk:   string & #gitRootStatePk
	sk:   string & #gitRootStateSk
	pk_2: string & =~"^ROOT#GROUP#[a-z]+$"
	pk_3: string & =~"^GROUP#[a-zA-Z0-9]+$"
	sk_2: string & #gitRootStateSk
	sk_3: string & #gitRootStateSk
}

#GitRootStateItem: {
	roots.#GitRootState
	#GitRootStateKeys
}

GitRootState:
{
	FacetName: "git_root_state"
	KeyAttributeAlias: {
		PartitionKeyAlias: "ROOT#uuid#GROUP#group_name"
		SortKeyAlias:      "STATE#state#DATE#iso8601utc"
	}
	NonKeyAttributes: [
		"pk_2",
		"pk_3",
		"sk_2",
		"sk_3",
		"modified_by",
		"modified_date",
		"branch",
		"environment",
		"includes_health_check",
		"nickname",
		"status",
		"url",
		"credential_id",
		"criticality",
		"gitignore",
		"other",
		"reason",
		"use_egress",
		"use_vpn",
		"use_ztna",
	]
	DataAccess: {
		MySql: {}
	}
}

GitRootState: TableData: [
	{
		gitignore: []
		modified_date:         "2020-11-19T13:39:56+00:00"
		branch:                "main"
		url:                   "https://gitlab.com/fluidattacks/demo.git"
		environment:           "QA"
		includes_health_check: false
		modified_by:           "jdoe@fluidattacks.com"
		nickname:              "integrates_1"
		sk:                    "STATE#state#DATE#2020-11-19T13:39:56+00:00"
		sk_3:                  "STATE#state#DATE#2020-11-19T13:39:56+00:00"
		pk_2:                  "ROOT#GROUP#unittesting"
		pk:                    "ROOT#765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a#GROUP#unittesting"
		pk_3:                  "GROUP#unittesting"
		sk_2:                  "STATE#state#DATE#2020-11-19T13:39:56+00:00"
		status:                "ACTIVE"
	},
	{
		gitignore: []
		modified_date:         "2024-05-09T13:39:56+00:00"
		branch:                "develop"
		url:                   "https://gitlab.com/fluidattacks/machine"
		environment:           "QA"
		includes_health_check: false
		modified_by:           "integratesmanager@gmail.com"
		nickname:              "integrates_test_root"
		sk:                    "STATE#state#DATE#2024-05-09T13:39:56+00:00"
		sk_3:                  "STATE#state#DATE#2024-05-09T13:39:56+00:00"
		pk_2:                  "ROOT#GROUP#unittesting"
		pk:                    "ROOT#765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a#GROUP#unittesting"
		pk_3:                  "GROUP#unittesting"
		sk_2:                  "STATE#state#DATE#2024-05-09T13:39:56+00:00"
		status:                "ACTIVE"
	},
	{
		gitignore: []
		modified_date:         "2020-11-19T13:39:56+00:00"
		branch:                "develop"
		url:                   "https://gitlab.com/fluidattacks/integrates"
		environment:           "QA"
		includes_health_check: false
		sk:                    "STATE#state#DATE#2020-11-19T13:39:56+00:00"
		modified_by:           "integratesmanager@fluidattacks.com"
		nickname:              "barranquilla_1"
		sk_3:                  "STATE#state#DATE#2020-11-19T13:39:56+00:00"
		pk_2:                  "ROOT#GROUP#barranquilla"
		pk:                    "ROOT#2e349e7f-390e-40e9-a8b6-2983eaee92ff#GROUP#barranquilla"
		pk_3:                  "GROUP#barranquilla"
		sk_2:                  "STATE#state#DATE#2020-11-19T13:39:56+00:00"
		credential_id:         "c624dc13-ac87-4cff-b710-767d3c3fdfd4"
		criticality:           "HIGH"
		status:                "ACTIVE"
	},
	{
		gitignore:
		[
			"bower_components/*",
			"node_modules/*",
		]
		modified_date:         "2020-11-19T13:41:56+00:00"
		branch:                "develop"
		url:                   "https://gitlab.com/fluidattacks/integrates"
		environment:           "QA"
		includes_health_check: false
		modified_by:           "jdoe@fluidattacks.com"
		nickname:              "barranquilla_1"
		sk:                    "STATE#state#DATE#2020-11-19T13:41:56+00:00"
		sk_3:                  "STATE#state#DATE#2020-11-19T13:41:56+00:00"
		pk_2:                  "ROOT#GROUP#barranquilla"
		pk:                    "ROOT#2e349e7f-390e-40e9-a8b6-2983eaee92ff#GROUP#barranquilla"
		pk_3:                  "GROUP#barranquilla"
		sk_2:                  "STATE#state#DATE#2020-11-19T13:41:56+00:00"
		status:                "ACTIVE"
	},
	{
		gitignore: []
		modified_date:         "2020-11-19T13:37:10+00:00"
		branch:                "master"
		url:                   "https://github.com/WebGoat/WebGoat.git"
		environment:           "production"
		includes_health_check: true
		modified_by:           "jdoe@fluidattacks.com"
		nickname:              "WebGoat"
		sk:                    "STATE#state#DATE#2020-11-19T13:37:10+00:00"
		sk_3:                  "STATE#state#DATE#2020-11-19T13:37:10+00:00"
		pk_2:                  "ROOT#GROUP#asgard"
		pk:                    "ROOT#7d4765c7-9236-4fc8-9724-000f5339422f#GROUP#asgard"
		pk_3:                  "GROUP#asgard"
		sk_2:                  "STATE#state#DATE#2020-11-19T13:37:10+00:00"
		credential_id:         "0df32c4a-526d-4879-8dc8-9ae191d2721b"
		criticality:           "MEDIUM"
		status:                "ACTIVE"
	},
	{
		gitignore: []
		modified_date:         "2020-11-19T13:37:10+00:00"
		branch:                "master"
		url:                   "https://gitlab.com/fluidattacks/bwapp.git"
		environment:           "production"
		includes_health_check: true
		modified_by:           "jdoe@fluidattacks.com"
		nickname:              "bwapp"
		sk:                    "STATE#state#DATE#2020-11-19T13:37:10+00:00"
		sk_3:                  "STATE#state#DATE#2020-11-19T13:37:10+00:00"
		pk_2:                  "ROOT#GROUP#asgard"
		pk:                    "ROOT#3a69ee71-1183-4cbe-99d9-27b2617df7d5#GROUP#asgard"
		pk_3:                  "GROUP#asgard"
		sk_2:                  "STATE#state#DATE#2020-11-19T13:37:10+00:00"
		credential_id:         "0df32c4a-526d-4879-8dc8-9ae191d2721b"
		criticality:           "LOW"
		status:                "ACTIVE"
	},
	{
		gitignore:
		[
			"bower_components/*",
			"node_modules/*",
		]
		modified_date:         "2020-11-19T13:37:10+00:00"
		branch:                "master"
		url:                   "https://gitlab.com/fluidattacks/universe.git"
		environment:           "production"
		includes_health_check: true
		modified_by:           "jdoe@fluidattacks.com"
		nickname:              "universe"
		sk:                    "STATE#state#DATE#2020-11-19T13:37:10+00:00"
		sk_3:                  "STATE#state#DATE#2020-11-19T13:37:10+00:00"
		pk_2:                  "ROOT#GROUP#unittesting"
		pk:                    "ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19#GROUP#unittesting"
		pk_3:                  "GROUP#unittesting"
		sk_2:                  "STATE#state#DATE#2020-11-19T13:37:10+00:00"
		credential_id:         "0df32c4a-526d-4879-8dc8-9ae191d2721b"
		criticality:           "LOW"
		status:                "ACTIVE"
	},
	{
		gitignore: []
		modified_date:         "2020-11-19T13:39:56+00:00"
		branch:                "main"
		url:                   "https://github.com/fluidattacks/integrates"
		environment:           "QA"
		includes_health_check: false
		modified_by:           "integratesreattacker@fluidattacks.com"
		nickname:              "integrates_2"
		sk:                    "STATE#state#DATE#2020-11-19T13:39:56+00:00"
		sk_3:                  "STATE#state#DATE#2020-11-19T13:39:56+00:00"
		pk_2:                  "ROOT#GROUP#unittesting"
		pk:                    "ROOT#f8771794-0428-4018-90c3-ce20525cee02#GROUP#unittesting"
		pk_3:                  "GROUP#unittesting"
		sk_2:                  "STATE#state#DATE#2020-11-19T13:39:56+00:00"
		credential_id:         "0df32c4a-526d-4879-8dc8-9ae191d2721b"
		criticality:           "MEDIUM"
		status:                "ACTIVE"
	},
	{
		gitignore: []
		modified_date:         "2020-11-19T13:39:56+00:00"
		branch:                "main"
		url:                   "https://github.com/fluidattacks/universe2.git"
		environment:           "QA"
		includes_health_check: false
		modified_by:           "integratesreattacker@fluidattacks.com"
		nickname:              "monteria_1"
		sk:                    "STATE#state#DATE#2020-11-19T13:39:56+00:00"
		sk_3:                  "STATE#state#DATE#2020-11-19T13:39:56+00:00"
		pk_2:                  "ROOT#GROUP#monteria"
		pk:                    "ROOT#f48afafd-a8f2-4925-84b1-6a6dc031b775#GROUP#monteria"
		pk_3:                  "GROUP#monteria"
		sk_2:                  "STATE#state#DATE#2020-11-19T13:39:56+00:00"
		credential_id:         "c624dc13-ac87-4cff-b710-767d3c3fdfd4"
		criticality:           "HIGH"
		status:                "ACTIVE"
	},
] & [...#GitRootStateItem]
