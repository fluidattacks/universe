package group_policies

import (
	"fluidattacks.com/types/groups"
)

#groupPoliciesPk: =~"^GROUP#[a-z]+#ORG#"
#groupPoliciesSk: =~"^STATE#policies#DATE#(\\d{4}-[01]\\d-[0-3]\\d([Tt][0-2]\\d:[0-5]\\d(:[0-5]\\d(\\.\\d{1,9})?)?([Zz]|([+-][0-2]\\d:[0-5]\\d))?)?)$"

#GroupPoliciesKeys: {
	pk:   string & #groupPoliciesPk
	sk:   string & #groupPoliciesSk
	pk_2: string & =~"^GROUP#ORG#[a-z]+"
	pk_3: string & =~"^GROUP#[a-z]+"
	sk_2: #groupPoliciesSk
	sk_3: #groupPoliciesSk
}

#GroupPoliciesItem: {
	#GroupPoliciesKeys
	groups.#GroupPolicies
}

GroupPolicies:
{
	FacetName: "group_policies"
	KeyAttributeAlias: {
		PartitionKeyAlias: "GROUP#name#ORG#organization_id"
		SortKeyAlias:      "STATE#policies#DATE#iso8601utc"
	}
	NonKeyAttributes: [
		"pk_2",
		"pk_3",
		"sk_2",
		"sk_3",
		"modified_by",
		"modified_date",
		"inactivity_period",
		"max_acceptance_days",
		"max_acceptance_severity",
		"max_number_acceptances",
		"min_acceptance_severity",
		"min_breaking_severity",
		"vulnerability_grace_period",
	]
	DataAccess: {
		MySql: {}
	}
}

GroupPolicies: TableData:
	[
		{
			max_number_acceptances:     3
			min_acceptance_severity:    0.0
			min_breaking_severity:      3.9
			max_acceptance_days:        90
			max_acceptance_severity:    3.9
			vulnerability_grace_period: 10
			modified_by:                "integratesmanager@gmail.com"
			sk:                         "STATE#policies#DATE#2021-11-22T20:07:57+00:00"
			sk_3:                       "STATE#policies#DATE#2021-11-22T20:07:57+00:00"
			pk_2:                       "GROUP#ORG#oneshottest"
			pk:                         "GROUP#oneshottest#ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
			pk_3:                       "GROUP#oneshottest"
			modified_date:              "2021-11-22T20:07:57+00:00"
			sk_2:                       "STATE#policies#DATE#2021-11-22T20:07:57+00:00"
		},
		{
			modified_by:   "integratesmanager@gmail.com"
			sk:            "STATE#policies#DATE#2023-02-15T20:07:57+00:00"
			sk_3:          "STATE#policies#DATE#2023-02-15T20:07:57+00:00"
			pk_2:          "GROUP#ORG#unittesting"
			pk:            "GROUP#unittesting#ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
			pk_3:          "GROUP#unittesting"
			modified_date: "2023-02-15T20:07:57+00:00"
			sk_2:          "STATE#policies#DATE#2023-02-15T20:07:57+00:00"
		},
	] & [...#GroupPoliciesItem]
