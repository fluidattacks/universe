package organization_policies

import (
	"fluidattacks.com/types"
)

#organizationPoliciesPk: =~"^ORG#[a-z0-9-]+#ORG#[a-z]+"
#organizationPoliciesSk: =~"^STATE#policies#DATE#(\\d{4}-[01]\\d-[0-3]\\d([Tt][0-2]\\d:[0-5]\\d(:[0-5]\\d(\\.\\d{1,9})?)?([Zz]|([+-][0-2]\\d:[0-5]\\d))?)?)$"

#OrganizationPoliciesKeys: {
	pk: string & #organizationPoliciesPk
	sk: string & #organizationPoliciesSk
}

#OrganizationPoliciesItem: {
	#OrganizationPoliciesKeys
	types.#Policies
}

OrganizationPolicies:
{
	FacetName: "organization_policies"
	KeyAttributeAlias: {
		PartitionKeyAlias: "ORG#id#ORG#name"
		SortKeyAlias:      "STATE#policies#DATE#iso8601utc"
	}
	NonKeyAttributes: [
		"modified_by",
		"modified_date",
		"max_acceptance_days",
		"max_acceptance_severity",
		"max_number_acceptances",
		"min_acceptance_severity",
		"min_breaking_severity",
		"inactivity_period",
		"vulnerability_grace_period",
	]
	DataAccess: {
		MySql: {}
	}
}

OrganizationPolicies: TableData: [
	{
		min_acceptance_severity: 0.0
		inactivity_period:       90
		modified_by:             "unknown"
		min_breaking_severity:   0.0
		sk:                      "STATE#policies#DATE#2019-11-22T20:07:57+00:00"
		pk:                      "ORG#33c08ebd-2068-47e7-9673-e1aa03dc9448#ORG#kiba"
		modified_date:           "2019-11-22T20:07:57+00:00"
		max_acceptance_severity: 10.0
	},
	{
		max_number_acceptances:     3
		min_acceptance_severity:    1.0
		inactivity_period:          90
		vulnerability_grace_period: 0
		modified_by:                "integratesmanager@gmail.com"
		min_breaking_severity:      0.0
		sk:                         "STATE#policies#DATE#2019-11-22T20:07:57+00:00"
		max_acceptance_days:        30
		pk:                         "ORG#956e9107-fd8d-49bc-b550-5609a7a1f6ac#ORG#kamiya"
		modified_date:              "2019-11-22T20:07:57+00:00"
		max_acceptance_severity:    8.5
	},
	{
		min_acceptance_severity: 0.0
		inactivity_period:       90
		modified_by:             "unknown"
		min_breaking_severity:   0.0
		sk:                      "STATE#policies#DATE#2019-11-22T20:07:57+00:00"
		pk:                      "ORG#d32674a9-9838-4337-b222-68c88bf54647#ORG#makoto"
		modified_date:           "2019-11-22T20:07:57+00:00"
		max_acceptance_severity: 10.0
	},
	{
		min_acceptance_severity: 0.0
		inactivity_period:       90
		modified_by:             "unknown"
		min_breaking_severity:   0.0
		sk:                      "STATE#policies#DATE#2019-11-22T20:07:57+00:00"
		pk:                      "ORG#fe80d2d4-ccb7-46d1-8489-67c6360581de#ORG#tatsumi"
		modified_date:           "2019-11-22T20:07:57+00:00"
		max_acceptance_severity: 10.0
	},
	{
		max_number_acceptances:     2
		min_acceptance_severity:    0.0
		inactivity_period:          90
		vulnerability_grace_period: 0
		modified_by:                "integratesmanager@gmail.com"
		min_breaking_severity:      0.0
		sk:                         "STATE#policies#DATE#2019-11-22T20:07:57+00:00"
		max_acceptance_days:        60
		pk:                         "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#ORG#okada"
		modified_date:              "2019-11-22T20:07:57+00:00"
		max_acceptance_severity:    10.0
	},
	{
		min_acceptance_severity: 0.0
		inactivity_period:       90
		modified_by:             "unknown"
		min_breaking_severity:   0.0
		sk:                      "STATE#policies#DATE#2019-11-22T20:07:57+00:00"
		pk:                      "ORG#ed6f051c-2572-420f-bc11-476c4e71b4ee#ORG#ikari"
		modified_date:           "2019-11-22T20:07:57+00:00"
		max_acceptance_severity: 10.0
	},
	{
		max_number_acceptances:     1
		min_acceptance_severity:    0.0
		inactivity_period:          90
		vulnerability_grace_period: 0
		modified_by:                "integratesmanager@gmail.com"
		min_breaking_severity:      0.0
		sk:                         "STATE#policies#DATE#2019-11-22T20:07:57+00:00"
		max_acceptance_days:        60
		pk:                         "ORG#f2e2777d-a168-4bea-93cd-d79142b294d2#ORG#hajime"
		modified_date:              "2019-11-22T20:07:57+00:00"
		max_acceptance_severity:    7.0
	},
	{
		min_acceptance_severity: 3.4
		inactivity_period:       90
		modified_by:             "unknown"
		min_breaking_severity:   0.0
		sk:                      "STATE#policies#DATE#2019-11-22T20:07:57+00:00"
		pk:                      "ORG#c2ee2d15-04ab-4f39-9795-fbe30cdeee86#ORG#bulat"
		modified_date:           "2019-11-22T20:07:57+00:00"
		max_acceptance_severity: 6.9
	},
	{
		max_number_acceptances:     4
		min_acceptance_severity:    3.0
		inactivity_period:          90
		vulnerability_grace_period: 0
		modified_by:                "integratesmanager@gmail.com"
		min_breaking_severity:      0.0
		sk:                         "STATE#policies#DATE#2019-11-22T20:07:57+00:00"
		max_acceptance_days:        90
		pk:                         "ORG#c6cecc0e-bb92-4079-8b6d-c4e815c10bb1#ORG#makimachi"
		modified_date:              "2019-11-22T20:07:57+00:00"
		max_acceptance_severity:    7.0
	},
	{
		min_acceptance_severity: 0.0
		inactivity_period:       90
		modified_by:             "unknown"
		min_breaking_severity:   0.0
		sk:                      "STATE#policies#DATE#2019-11-22T20:07:57+00:00"
		pk:                      "ORG#ffddc7a3-7f05-4fc7-b65d-7defffa883c2#ORG#himura"
		modified_date:           "2019-11-22T20:07:57+00:00"
		max_acceptance_severity: 10.0
	},
] & [...#OrganizationPoliciesItem]
