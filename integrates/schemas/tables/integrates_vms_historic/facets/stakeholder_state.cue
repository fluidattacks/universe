package stakeholder_state

import (
	"fluidattacks.com/types/stakeholders"
)

#stakeholderStatePk: =~"^USER#[\\w.-]+@[a-zA-Z\\d.-]+.[a-zA-Z]{2,}#USER#[\\w.-]+@[a-zA-Z\\d.-]+.[a-zA-Z]{2,}$"
#stakeholderStateSk: =~"^STATE#state#DATE#(\\d{4}-[01]\\d-[0-3]\\d([Tt][0-2]\\d:[0-5]\\d(:[0-5]\\d(\\.\\d{1,9})?)?([Zz]|([+-][0-2]\\d:[0-5]\\d))?)?)$"

#StakeholderStateKeys: {
	pk: string & #stakeholderStatePk
	sk: string & #stakeholderStateSk
}

#StakeholderStateItem: {
	#StakeholderStateKeys
	stakeholders.#StakeholderState
}

StakeholderState:
{
	FacetName: "stakeholder_state"
	KeyAttributeAlias: {
		PartitionKeyAlias: "USER#email#USER#email"
		SortKeyAlias:      "STATE#state#DATE#iso8601utc"
	}
	NonKeyAttributes: [
		"modified_by",
		"modified_date",
		"notifications_preferences",
		"trusted_devices",
	]
	DataAccess: {
		MySql: {}
	}
}

StakeholderState: TableData: [
	{
		modified_by:   "integratesresourcer@fluidattacks.com"
		sk:            "STATE#state#DATE#2018-02-28T16:54:12+00:00"
		pk:            "USER#integratesresourcer@fluidattacks.com#USER#integratesresourcer@fluidattacks.com"
		modified_date: "2018-02-28T16:54:12+00:00"
		notifications_preferences: {
			sms: []
			email:
			[
				"ACCESS_GRANTED",
				"EVENT_REPORT",
				"GROUP_REPORT",
				"NEW_COMMENT",
				"NEW_DRAFT",
				"REMEDIATE_FINDING",
				"REMINDER_NOTIFICATION",
				"UPDATED_TREATMENT",
				"VULNERABILITY_ASSIGNED",
				"VULNERABILITY_REPORT",
			]
		}
	},
	{
		modified_by:   "integratesuser@gmail.com"
		sk:            "STATE#state#DATE#2018-02-28T16:54:12+00:00"
		pk:            "USER#integratesuser@gmail.com#USER#integratesuser@gmail.com"
		modified_date: "2018-02-28T16:54:12+00:00"
		notifications_preferences: {
			sms: []
			email:
			[
				"ACCESS_GRANTED",
				"AGENT_TOKEN",
				"EVENT_REPORT",
				"FILE_UPDATE",
				"GROUP_INFORMATION",
				"GROUP_REPORT",
				"NEW_COMMENT",
				"NEW_DRAFT",
				"PORTFOLIO_UPDATE",
				"REMEDIATE_FINDING",
				"REMINDER_NOTIFICATION",
				"ROOT_UPDATE",
				"SERVICE_UPDATE",
				"UNSUBSCRIPTION_ALERT",
				"UPDATED_TREATMENT",
				"VULNERABILITY_ASSIGNED",
				"VULNERABILITY_REPORT",
			]
		}
	},
	{
		modified_by:   "cypress@cypress.com"
		sk:            "STATE#state#DATE#2023-10-05T00:00:00+00:00"
		pk:            "USER#cypress@cypress.com#USER#cypress@cypress.com"
		modified_date: "2023-10-05T00:00:00+00:00"
		notifications_preferences: {
			sms: []
			email:
			[
				"ACCESS_GRANTED",
				"AGENT_TOKEN",
				"EVENT_REPORT",
				"FILE_UPDATE",
				"GROUP_INFORMATION",
				"GROUP_REPORT",
				"NEW_COMMENT",
				"NEW_DRAFT",
				"PORTFOLIO_UPDATE",
				"REMEDIATE_FINDING",
				"REMINDER_NOTIFICATION",
				"ROOT_UPDATE",
				"SERVICE_UPDATE",
				"UNSUBSCRIPTION_ALERT",
				"UPDATED_TREATMENT",
				"VULNERABILITY_ASSIGNED",
				"VULNERABILITY_REPORT",
			]
		}
	},
	{
		modified_by:   "continuoushacking@gmail.com"
		sk:            "STATE#state#DATE#2018-02-28T16:54:12+00:00"
		pk:            "USER#continuoushacking@gmail.com#USER#continuoushacking@gmail.com"
		modified_date: "2018-02-28T16:54:12+00:00"
		notifications_preferences: {
			sms: []
			email:
			[
				"ACCESS_GRANTED",
				"AGENT_TOKEN",
				"EVENT_REPORT",
				"FILE_UPDATE",
				"GROUP_INFORMATION",
				"GROUP_REPORT",
				"NEW_COMMENT",
				"NEW_DRAFT",
				"PORTFOLIO_UPDATE",
				"REMEDIATE_FINDING",
				"REMINDER_NOTIFICATION",
				"ROOT_UPDATE",
				"SERVICE_UPDATE",
				"UNSUBSCRIPTION_ALERT",
				"UPDATED_TREATMENT",
				"VULNERABILITY_ASSIGNED",
				"VULNERABILITY_REPORT",
			]
		}
	},
	{
		modified_by:   "integratesmanager@fluidattacks.com"
		sk:            "STATE#state#DATE#2018-02-28T16:54:12+00:00"
		pk:            "USER#integratesmanager@fluidattacks.com#USER#integratesmanager@fluidattacks.com"
		modified_date: "2018-02-28T16:54:12+00:00"
		notifications_preferences: {
			sms: []
			email:
			[
				"ACCESS_GRANTED",
				"AGENT_TOKEN",
				"EVENT_REPORT",
				"FILE_UPDATE",
				"GROUP_INFORMATION",
				"GROUP_REPORT",
				"NEW_COMMENT",
				"NEW_DRAFT",
				"PORTFOLIO_UPDATE",
				"REMEDIATE_FINDING",
				"REMINDER_NOTIFICATION",
				"ROOT_UPDATE",
				"SERVICE_UPDATE",
				"UNSUBSCRIPTION_ALERT",
				"UPDATED_TREATMENT",
				"VULNERABILITY_ASSIGNED",
				"VULNERABILITY_REPORT",
			]
		}
	},
	{
		modified_by:   "customer_manager@fluidattacks.com"
		sk:            "STATE#state#DATE#2018-08-28T16:54:12+00:00"
		pk:            "USER#customer_manager@fluidattacks.com#USER#customer_manager@fluidattacks.com"
		modified_date: "2018-08-28T16:54:12+00:00"
		notifications_preferences: {
			sms: []
			email:
			[
				"ACCESS_GRANTED",
				"AGENT_TOKEN",
				"EVENT_REPORT",
				"FILE_UPDATE",
				"GROUP_INFORMATION",
				"GROUP_REPORT",
				"NEW_COMMENT",
				"NEW_DRAFT",
				"PORTFOLIO_UPDATE",
				"REMEDIATE_FINDING",
				"REMINDER_NOTIFICATION",
				"ROOT_UPDATE",
				"SERVICE_UPDATE",
				"UNSUBSCRIPTION_ALERT",
				"UPDATED_TREATMENT",
				"VULNERABILITY_ASSIGNED",
				"VULNERABILITY_REPORT",
			]
		}
	},
	{
		modified_by:   "jdoe@testcompany.com"
		sk:            "STATE#state#DATE#2022-02-09T16:54:12+00:00"
		pk:            "USER#jdoe@testcompany.com#USER#jdoe@testcompany.com"
		modified_date: "2022-02-09T16:54:12+00:00"
		notifications_preferences: {
			sms: []
			email:
			[
				"ACCESS_GRANTED",
				"AGENT_TOKEN",
				"EVENT_REPORT",
				"FILE_UPDATE",
				"GROUP_INFORMATION",
				"GROUP_REPORT",
				"NEW_COMMENT",
				"NEW_DRAFT",
				"PORTFOLIO_UPDATE",
				"REMEDIATE_FINDING",
				"REMINDER_NOTIFICATION",
				"ROOT_UPDATE",
				"SERVICE_UPDATE",
				"UNSUBSCRIPTION_ALERT",
				"UPDATED_TREATMENT",
				"VULNERABILITY_ASSIGNED",
				"VULNERABILITY_REPORT",
			]
		}
	},
	{
		modified_by:   "integratesuser2@gmail.com"
		sk:            "STATE#state#DATE#2018-02-28T16:54:12+00:00"
		pk:            "USER#integratesuser2@gmail.com#USER#integratesuser2@gmail.com"
		modified_date: "2018-02-28T16:54:12+00:00"
		notifications_preferences: {
			sms: []
			email:
			[
				"ACCESS_GRANTED",
				"EVENT_REPORT",
				"GROUP_REPORT",
				"NEW_COMMENT",
				"NEW_DRAFT",
				"REMEDIATE_FINDING",
				"REMINDER_NOTIFICATION",
				"UPDATED_TREATMENT",
				"VULNERABILITY_ASSIGNED",
				"VULNERABILITY_REPORT",
			]
		}
	},
	{
		modified_by:   "continuoushack2@gmail.com"
		sk:            "STATE#state#DATE#2018-02-28T16:54:12+00:00"
		pk:            "USER#continuoushack2@gmail.com#USER#continuoushack2@gmail.com"
		modified_date: "2018-02-28T16:54:12+00:00"
		notifications_preferences: {
			sms: []
			email:
			[
				"ACCESS_GRANTED",
				"AGENT_TOKEN",
				"EVENT_REPORT",
				"FILE_UPDATE",
				"GROUP_INFORMATION",
				"GROUP_REPORT",
				"NEW_COMMENT",
				"NEW_DRAFT",
				"PORTFOLIO_UPDATE",
				"REMEDIATE_FINDING",
				"REMINDER_NOTIFICATION",
				"ROOT_UPDATE",
				"SERVICE_UPDATE",
				"UNSUBSCRIPTION_ALERT",
				"UPDATED_TREATMENT",
				"VULNERABILITY_ASSIGNED",
				"VULNERABILITY_REPORT",
			]
		}
		trusted_devices:
		[
			{
				browser:          "firefox"
				device:           "ubuntu"
				first_login_date: "2024-02-14T10:55:12+00:00"
				ip_address:       "192.168.1.1"
				last_attempt:     "2024-02-14T10:55:12+00:00"
				last_login_date:  "2024-02-14T10:55:12+00:00"
				location:         "Toronto"
				otp_token_jti:    "abc961"
			},
		]
	},
	{
		modified_by:   "unittest2@fluidattacks.com"
		sk:            "STATE#state#DATE#2018-02-28T16:54:12+00:00"
		pk:            "USER#unittest2@fluidattacks.com#USER#unittest2@fluidattacks.com"
		modified_date: "2018-02-28T16:54:12+00:00"
		notifications_preferences: {
			sms: []
			email:
			[
				"ACCESS_GRANTED",
				"AGENT_TOKEN",
				"EVENT_REPORT",
				"FILE_UPDATE",
				"GROUP_INFORMATION",
				"GROUP_REPORT",
				"NEW_COMMENT",
				"NEW_DRAFT",
				"PORTFOLIO_UPDATE",
				"REMEDIATE_FINDING",
				"REMINDER_NOTIFICATION",
				"ROOT_UPDATE",
				"SERVICE_UPDATE",
				"UNSUBSCRIPTION_ALERT",
				"UPDATED_TREATMENT",
				"VULNERABILITY_ASSIGNED",
				"VULNERABILITY_REPORT",
			]
		}
	},
	{
		modified_by:   "integratesmanager@gmail.com"
		sk:            "STATE#state#DATE#2018-02-28T16:54:12+00:00"
		pk:            "USER#integratesmanager@gmail.com#USER#integratesmanager@gmail.com"
		modified_date: "2018-02-28T16:54:12+00:00"
		notifications_preferences: {
			sms: []
			email:
			[
				"ACCESS_GRANTED",
				"AGENT_TOKEN",
				"EVENT_REPORT",
				"FILE_UPDATE",
				"GROUP_INFORMATION",
				"GROUP_REPORT",
				"NEW_COMMENT",
				"NEW_DRAFT",
				"PORTFOLIO_UPDATE",
				"REMEDIATE_FINDING",
				"REMINDER_NOTIFICATION",
				"ROOT_UPDATE",
				"SERVICE_UPDATE",
				"UNSUBSCRIPTION_ALERT",
				"UPDATED_TREATMENT",
				"VULNERABILITY_ASSIGNED",
				"VULNERABILITY_REPORT",
			]
		}
	},
] & [...#StakeholderStateItem]
