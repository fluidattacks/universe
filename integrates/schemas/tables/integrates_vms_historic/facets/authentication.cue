package authentication

import (
	"fluidattacks.com/bounds"
)

#authenticationPk: =~"^USER#[a-zA-Z0-9]+$"
#authenticationSk: =~"^AUTH#DATE#(\\d{4}-[01]\\d-[0-3]\\d([Tt][0-2]\\d:[0-5]\\d(:[0-5]\\d(\\.\\d{1,9})?)?([Zz]|([+-][0-2]\\d:[0-5]\\d))?)?)$"

#AuthenticationItem: {
	pk:         string & #authenticationPk
	sk:         string & #authenticationSk
	action:     string
	date:       string & bounds.#isoDate
	email:      string
	ip:         string
	user_agent: string
}

Authentication:
{
	FacetName: "authentication"
	KeyAttributeAlias: {
		PartitionKeyAlias: "USER#email"
		SortKeyAlias:      "AUTH#DATE#iso8601utc"
	}
	NonKeyAttributes: [
		"action",
		"date",
		"email",
		"ip",
		"user_agent",
	]
	DataAccess: {
		MySql: {}
	}
}

AuthenticationItem: TableData: [] & [...#AuthenticationItem]
