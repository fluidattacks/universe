package integrates_vms

import (
	"fluidattacks.com/database_schema"
	"fluidattacks.com/tables/integrates_vms/facets:aws_marketplace_subscription_metadata"
	"fluidattacks.com/tables/integrates_vms/facets:compliance_unreliable_indicators"
	"fluidattacks.com/tables/integrates_vms/facets:credentials_metadata"
	"fluidattacks.com/tables/integrates_vms/facets:event_comment"
	"fluidattacks.com/tables/integrates_vms/facets:event_historic_state"
	"fluidattacks.com/tables/integrates_vms/facets:event_metadata"
	"fluidattacks.com/tables/integrates_vms/facets:finding_comment"
	"fluidattacks.com/tables/integrates_vms/facets:finding_historic_state"
	"fluidattacks.com/tables/integrates_vms/facets:finding_historic_verification"
	"fluidattacks.com/tables/integrates_vms/facets:finding_id"
	"fluidattacks.com/tables/integrates_vms/facets:finding_metadata"
	"fluidattacks.com/tables/integrates_vms/facets:forces_execution"
	"fluidattacks.com/tables/integrates_vms/facets:git_root_historic_state"
	"fluidattacks.com/tables/integrates_vms/facets:git_root_metadata"
	"fluidattacks.com/tables/integrates_vms/facets:group_access"
	"fluidattacks.com/tables/integrates_vms/facets:group_historic_access"
	"fluidattacks.com/tables/integrates_vms/facets:group_historic_policies"
	"fluidattacks.com/tables/integrates_vms/facets:group_historic_state"
	"fluidattacks.com/tables/integrates_vms/facets:group_id"
	"fluidattacks.com/tables/integrates_vms/facets:group_metadata"
	"fluidattacks.com/tables/integrates_vms/facets:group_unreliable_indicators"
	"fluidattacks.com/tables/integrates_vms/facets:hook_metadata"
	"fluidattacks.com/tables/integrates_vms/facets:ip_root_historic_state"
	"fluidattacks.com/tables/integrates_vms/facets:ip_root_metadata"
	"fluidattacks.com/tables/integrates_vms/facets:jira_install"
	"fluidattacks.com/tables/integrates_vms/facets:machine_finding_code"
	"fluidattacks.com/tables/integrates_vms/facets:mailmap_entry"
	"fluidattacks.com/tables/integrates_vms/facets:mailmap_subentry"
	"fluidattacks.com/tables/integrates_vms/facets:notification_metadata"
	"fluidattacks.com/tables/integrates_vms/facets:org_finding_policy_historic_state"
	"fluidattacks.com/tables/integrates_vms/facets:org_finding_policy_metadata"
	"fluidattacks.com/tables/integrates_vms/facets:organization_access"
	"fluidattacks.com/tables/integrates_vms/facets:organization_historic_policies"
	"fluidattacks.com/tables/integrates_vms/facets:organization_historic_state"
	"fluidattacks.com/tables/integrates_vms/facets:organization_metadata"
	"fluidattacks.com/tables/integrates_vms/facets:organization_unreliable_indicators"
	"fluidattacks.com/tables/integrates_vms/facets:organization_unreliable_integration_repository"
	"fluidattacks.com/tables/integrates_vms/facets:portfolio_metadata"
	"fluidattacks.com/tables/integrates_vms/facets:root_docker_image"
	"fluidattacks.com/tables/integrates_vms/facets:root_docker_image_historic_state"
	"fluidattacks.com/tables/integrates_vms/facets:root_docker_image_layer"
	"fluidattacks.com/tables/integrates_vms/facets:root_environment_secret"
	"fluidattacks.com/tables/integrates_vms/facets:root_environment_url"
	"fluidattacks.com/tables/integrates_vms/facets:root_secret"
	"fluidattacks.com/tables/integrates_vms/facets:stakeholder_historic_state"
	"fluidattacks.com/tables/integrates_vms/facets:stakeholder_metadata"
	"fluidattacks.com/tables/integrates_vms/facets:toe_input_metadata"
	"fluidattacks.com/tables/integrates_vms/facets:toe_lines_metadata"
	"fluidattacks.com/tables/integrates_vms/facets:toe_packages_image"
	"fluidattacks.com/tables/integrates_vms/facets:toe_packages_metadata"
	"fluidattacks.com/tables/integrates_vms/facets:toe_packages_vulnerability"
	"fluidattacks.com/tables/integrates_vms/facets:toe_port_metadata"
	"fluidattacks.com/tables/integrates_vms/facets:trial_metadata"
	"fluidattacks.com/tables/integrates_vms/facets:url_root_historic_state"
	"fluidattacks.com/tables/integrates_vms/facets:url_root_metadata"
	"fluidattacks.com/tables/integrates_vms/facets:vulnerability_historic_state"
	"fluidattacks.com/tables/integrates_vms/facets:vulnerability_historic_treatment"
	"fluidattacks.com/tables/integrates_vms/facets:vulnerability_historic_verification"
	"fluidattacks.com/tables/integrates_vms/facets:vulnerability_historic_zero_risk"
	"fluidattacks.com/tables/integrates_vms/facets:vulnerability_metadata"
	"fluidattacks.com/tables/integrates_vms/facets:vulnerability_snippet"
	"fluidattacks.com/tables/integrates_vms/keys"
)

#IntegratesVmsTable: {
	TableName: "integrates_vms"
	KeyAttributes: {
		PartitionKey: {
			AttributeName: "pk"
			AttributeType: "S"
		}
		SortKey: {
			AttributeName: "sk"
			AttributeType: "S"
		}
	}
	NonKeyAttributes: [for entry in keys.NonKeyAttributesData {
		AttributeName: entry.n
		AttributeType: entry.t
	}]
	BillingMode: "PAY_PER_REQUEST"
	DataAccess: {"MySql": {}}
	SampleDataFormats: {}
} & database_schema.#Table

#IntegratesVmsTable: TableFacets: [
	stakeholder_metadata.StakeholderMetadata,
	organization_access.OrganizationAccess,
	group_access.GroupAccess,
	group_historic_access.GroupHistoricAccess,
	portfolio_metadata.PortfolioMetadata,
	organization_metadata.OrganizationMetadata,
	organization_unreliable_indicators.OrganizationUnreliableIndicators,
	organization_historic_policies.OrganizationHistoricPolicies,
	organization_historic_state.OrganizationHistoricState,
	group_id.GroupId,
	group_metadata.GroupMetadata,
	group_historic_state.GroupHistoricState,
	group_historic_policies.GroupHistoricPolicies,
	group_unreliable_indicators.GroupUnreliableIndicators,
	forces_execution.ForcesExecution,
	event_metadata.EventMetadata,
	event_historic_state.EventHistoricState,
	event_comment.EventComment,
	finding_comment.FindingComment,
	root_secret.RootSecret,
	root_environment_secret.RootEnvironmentSecret,
	toe_packages_image.ToePackagesImage,
	root_docker_image.RootDockerImage,
	root_docker_image_historic_state.RootDockerImageHistoricState,
	root_docker_image_layer.RootDockerImageLayer,
	root_environment_url.RootEnvironmentUrl,
	git_root_metadata.GitRootMetadata,
	git_root_historic_state.GitRootHistoricState,
	ip_root_metadata.IpRootMetadata,
	ip_root_historic_state.IpRootHistoricState,
	jira_install.JiraInstall,
	url_root_metadata.UrlRootMetadata,
	url_root_historic_state.UrlRootHistoricState,
	compliance_unreliable_indicators.ComplianceUnreliableIndicators,
	credentials_metadata.CredentialsMetadata,
	toe_packages_metadata.ToePackagesMetadata,
	toe_packages_vulnerability.ToePackagesVulnerability,
	toe_lines_metadata.ToeLinesMetadata,
	toe_input_metadata.ToeInputMetadata,
	toe_port_metadata.ToePortMetadata,
	org_finding_policy_metadata.OrgFindingPolicyMetadata,
	org_finding_policy_historic_state.OrgFindingPolicyHistoricState,
	finding_id.FindingId,
	finding_metadata.FindingMetadata,
	finding_historic_state.FindingHistoricState,
	finding_historic_verification.FindingHistoricVerification,
	vulnerability_metadata.VulnerabilityMetadata,
	vulnerability_snippet.VulnerabilitySnippet,
	vulnerability_historic_state.VulnerabilityHistoricState,
	vulnerability_historic_treatment.VulnerabilityHistoricTreatment,
	vulnerability_historic_verification.VulnerabilityHistoricVerification,
	vulnerability_historic_zero_risk.VulnerabilityHistoricZeroRisk,
	stakeholder_historic_state.StakeholderHistoricState,
	organization_unreliable_integration_repository.OrganizationUnreliableIntegrationRepository,
	trial_metadata.TrialMetadata,
	hook_metadata.HookMetadata,
	machine_finding_code.MachineFindingCode,
	mailmap_entry.MailmapEntry,
	mailmap_subentry.MailmapSubentry,
	notification_metadata.NotificationMetadata,
	aws_marketplace_subscription_metadata.AwsMarketplaceSubscriptionMetadata,
] & [...database_schema.#Facet]

#IntegratesVmsTable: GlobalSecondaryIndexes: [
	{
		IndexName: "inverted_index"
		KeyAttributes: {
			PartitionKey: {
				AttributeName: "sk"
				AttributeType: "S"
			}
			SortKey: {
				AttributeName: "pk"
				AttributeType: "S"
			}
		}
		Projection: {
			ProjectionType: "ALL"
		}
	},
	{
		IndexName: "gsi_hash"
		KeyAttributes: {
			PartitionKey: {
				AttributeName: "pk_hash"
				AttributeType: "S"
			}
			SortKey: {
				AttributeName: "sk_hash"
				AttributeType: "S"
			}
		}
		Projection: {
			ProjectionType: "ALL"
		}
	},
	{
		IndexName: "gsi_2"
		KeyAttributes: {
			PartitionKey: {
				AttributeName: "pk_2"
				AttributeType: "S"
			}
			SortKey: {
				AttributeName: "sk_2"
				AttributeType: "S"
			}
		}
		Projection: {
			ProjectionType: "ALL"
		}
	},
	{
		IndexName: "gsi_3"
		KeyAttributes: {
			PartitionKey: {
				AttributeName: "pk_3"
				AttributeType: "S"
			}
			SortKey: {
				AttributeName: "sk_3"
				AttributeType: "S"
			}
		}
		Projection: {
			ProjectionType: "ALL"
		}
	},
	{
		IndexName: "gsi_4"
		KeyAttributes: {
			PartitionKey: {
				AttributeName: "pk_4"
				AttributeType: "S"
			}
			SortKey: {
				AttributeName: "sk_4"
				AttributeType: "S"
			}
		}
		Projection: {
			ProjectionType: "ALL"
		}
	},
	{
		IndexName: "gsi_5"
		KeyAttributes: {
			PartitionKey: {
				AttributeName: "pk_5"
				AttributeType: "S"
			}
			SortKey: {
				AttributeName: "sk_5"
				AttributeType: "S"
			}
		}
		Projection: {
			ProjectionType: "ALL"
		}
	},
	{
		IndexName: "gsi_6"
		KeyAttributes: {
			PartitionKey: {
				AttributeName: "pk_6"
				AttributeType: "S"
			}
			SortKey: {
				AttributeName: "sk_6"
				AttributeType: "S"
			}
		}
		Projection: {
			ProjectionType: "ALL"
		}
	},
]
