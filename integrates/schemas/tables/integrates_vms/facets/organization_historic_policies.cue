package organization_historic_policies

import (
	"fluidattacks.com/types"
)

#organizationHistoricPoliciesPk: =~"^ORG#[a-f0-9-]{36}$"
#organizationHistoricPoliciesSk: =~"^POLICIES#(\\d{4})-(\\d{2})-(\\d{2})(T(\\d{2}):(\\d{2}):(\\d{2})(\\.\\d+)?(Z|([+-])(\\d{2}):(\\d{2})))?$"

#OrganizationHistoricPoliciesKeys: {
	pk: string & #organizationHistoricPoliciesPk
	sk: string & #organizationHistoricPoliciesSk
}

#OrganizationHistoricPoliciesItem: {
	#OrganizationHistoricPoliciesKeys
	types.#Policies
}

OrganizationHistoricPolicies:
{
	FacetName: "organization_historic_policies"
	KeyAttributeAlias: {
		PartitionKeyAlias: "ORG#id"
		SortKeyAlias:      "POLICIES#iso8601utc"
	}
	NonKeyAttributes: [
		"modified_date",
		"modified_by",
		"max_acceptance_days",
		"max_acceptance_severity",
		"max_number_acceptances",
		"min_acceptance_severity",
		"min_breaking_severity",
		"inactivity_period",
		"vulnerability_grace_period",
	]
	DataAccess: {
		MySql: {}
	}
}

OrganizationHistoricPolicies: TableData: [
	{
		inactivity_period:       90
		min_acceptance_severity: 0.0
		sk:                      "POLICIES#2019-11-22T20:07:57+00:00"
		modified_by:             "unknown"
		min_breaking_severity:   0.0
		pk:                      "ORG#33c08ebd-2068-47e7-9673-e1aa03dc9448"
		modified_date:           "2019-11-22T20:07:57+00:00"
		max_acceptance_severity: 10.0
	},
	{
		inactivity_period:          90
		min_acceptance_severity:    1.0
		vulnerability_grace_period: 0
		sk:                         "POLICIES#2019-11-22T20:07:57+00:00"
		modified_by:                "unknown"
		min_breaking_severity:      0.0
		max_acceptance_days:        30
		pk:                         "ORG#7376c5fe-4634-4053-9718-e14ecbda1e6b"
		modified_date:              "2019-11-22T20:07:57+00:00"
		max_acceptance_severity:    8.5
	},
	{
		inactivity_period:          90
		max_number_acceptances:     4
		min_acceptance_severity:    3.0
		vulnerability_grace_period: 0
		sk:                         "POLICIES#2019-11-22T20:07:57+00:00"
		modified_by:                "integratesmanager@gmail.com"
		min_breaking_severity:      0.0
		max_acceptance_days:        90
		pk:                         "ORG#c6cecc0e-bb92-4079-8b6d-c4e815c10bb1"
		modified_date:              "2019-11-22T20:07:57+00:00"
		max_acceptance_severity:    7.0
	},
	{
		inactivity_period:       90
		min_acceptance_severity: 0.0
		sk:                      "POLICIES#2019-11-22T20:07:57+00:00"
		modified_by:             "unknown"
		min_breaking_severity:   0.0
		pk:                      "ORG#ffddc7a3-7f05-4fc7-b65d-7defffa883c2"
		modified_date:           "2019-11-22T20:07:57+00:00"
		max_acceptance_severity: 10.0
	},
	{
		inactivity_period:          90
		max_number_acceptances:     1
		min_acceptance_severity:    0.0
		vulnerability_grace_period: 0
		sk:                         "POLICIES#2019-11-22T20:07:57+00:00"
		modified_by:                "integratesmanager@gmail.com"
		min_breaking_severity:      0.0
		max_acceptance_days:        60
		pk:                         "ORG#f2e2777d-a168-4bea-93cd-d79142b294d2"
		modified_date:              "2019-11-22T20:07:57+00:00"
		max_acceptance_severity:    7.0
	},
	{
		inactivity_period:       90
		min_acceptance_severity: 3.4
		sk:                      "POLICIES#2019-11-22T20:07:57+00:00"
		modified_by:             "unknown"
		min_breaking_severity:   0.0
		pk:                      "ORG#c2ee2d15-04ab-4f39-9795-fbe30cdeee86"
		modified_date:           "2019-11-22T20:07:57+00:00"
		max_acceptance_severity: 6.9
	},
	{
		inactivity_period:       90
		min_acceptance_severity: 0.0
		sk:                      "POLICIES#2019-11-22T20:07:57+00:00"
		modified_by:             "unknown"
		min_breaking_severity:   0.0
		pk:                      "ORG#ed6f051c-2572-420f-bc11-476c4e71b4ee"
		modified_date:           "2019-11-22T20:07:57+00:00"
		max_acceptance_severity: 10.0
	},
	{
		inactivity_period:          90
		max_number_acceptances:     3
		min_acceptance_severity:    1.0
		vulnerability_grace_period: 0
		sk:                         "POLICIES#2019-11-22T20:07:57+00:00"
		modified_by:                "integratesmanager@gmail.com"
		min_breaking_severity:      0.0
		max_acceptance_days:        30
		pk:                         "ORG#956e9107-fd8d-49bc-b550-5609a7a1f6ac"
		modified_date:              "2019-11-22T20:07:57+00:00"
		max_acceptance_severity:    8.5
	},
	{
		inactivity_period:       90
		min_acceptance_severity: 0.0
		sk:                      "POLICIES#2019-11-22T20:07:57+00:00"
		modified_by:             "unknown"
		min_breaking_severity:   0.0
		pk:                      "ORG#fe80d2d4-ccb7-46d1-8489-67c6360581de"
		modified_date:           "2019-11-22T20:07:57+00:00"
		max_acceptance_severity: 10.0
	},
	{
		inactivity_period:          90
		max_number_acceptances:     2
		min_acceptance_severity:    0.0
		vulnerability_grace_period: 0
		sk:                         "POLICIES#2019-11-22T20:07:57+00:00"
		modified_by:                "integratesmanager@gmail.com"
		min_breaking_severity:      0.0
		max_acceptance_days:        60
		pk:                         "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
		modified_date:              "2019-11-22T20:07:57+00:00"
		max_acceptance_severity:    10.0
	},
	{
		inactivity_period:       90
		min_acceptance_severity: 0.0
		sk:                      "POLICIES#2019-11-22T20:07:57+00:00"
		modified_by:             "unknown"
		min_breaking_severity:   0.0
		pk:                      "ORG#d32674a9-9838-4337-b222-68c88bf54647"
		modified_date:           "2019-11-22T20:07:57+00:00"
		max_acceptance_severity: 10.0
	},
	{
		min_acceptance_severity: 0.0
		inactivity_period:       90
		sk:                      "POLICIES#2024-07-16T20:26:54.966549+00:00"
		modified_by:             "integratesmanager@gmail.com"
		pk:                      "ORG#5f727061-f608-4ff3-97c0-df9056011e2c"
		modified_date:           "2024-07-16T20:26:54.966549+00:00"
		max_acceptance_severity: 10.0
	},
] & [...#OrganizationHistoricPoliciesItem]
