package root_docker_image_layer

import (
	"fluidattacks.com/types/roots"
)

#rootDockerImageLayerPk: =~"^GROUP#[a-z]+#ROOT#[a-z0-9-]{36}#URI#[a-z0-9./:_-]+$"
#rootDockerImageLayerSk: =~"^DIGEST#sha256:[a-f0-9]+$"

#RootDockerImageLayerKeys: {
	pk: string & #rootDockerImageLayerPk
	sk: string & #rootDockerImageLayerSk
}

#RootDockerImageLayerItem: {
	#RootDockerImageLayerKeys
	roots.#RootDockerImageLayer
}

RootDockerImageLayer:
{
	FacetName: "root_docker_image_layer"
	KeyAttributeAlias: {
		PartitionKeyAlias: "GROUP#group_name#ROOT#uuid#URI#uri"
		SortKeyAlias:      "DIGEST#digest"
	}
	NonKeyAttributes: [
		"digest",
		"media_type",
		"size",
	]
	DataAccess: {
		MySql: {}
	}
}

RootDockerImageLayer: TableData: [
	{
		pk:         "GROUP#unittesting#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19#URI#docker.io/library/ubuntu:latest"
		sk:         "DIGEST#sha256:e0a9f5911802534ba097660206feabeb0247a81e409029167b30e2e1f2803b57"
		media_type: "application/vnd.oci.image.layer.v1.tar+gzip"
		digest:     "sha256:e0a9f5911802534ba097660206feabeb0247a81e409029167b30e2e1f2803b57"
		size:       29179503
	},
	{
		pk:         "GROUP#unittesting#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19#URI#docker.io/nginx:latest"
		sk:         "DIGEST#sha256:32cfaf91376fefc4934558561815920002a94dffaf52bc67b7382ea7869553b6"
		media_type: "application/vnd.oci.image.layer.v1.tar+gzip"
		digest:     "sha256:32cfaf91376fefc4934558561815920002a94dffaf52bc67b7382ea7869553b6"
		size:       1164
	},
	{
		pk:         "GROUP#unittesting#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19#URI#docker.io/nginx:latest"
		sk:         "DIGEST#sha256:2bb0b7dbd861281e084c4d276c901f746cb1753a1ccbf094a394731bf39e2031"
		media_type: "application/vnd.oci.image.layer.v1.tar+gzip"
		digest:     "sha256:2bb0b7dbd861281e084c4d276c901f746cb1753a1ccbf094a394731bf39e2031"
		size:       1164
	},
	{
		pk:         "GROUP#unittesting#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19#URI#docker.io/nginx:latest"
		sk:         "DIGEST#sha256:933a3ce2c78a89e4646f6314e7e5b10ffd5d7f4d72ecf7a320d5a3e110f1e146"
		media_type: "application/vnd.oci.image.layer.v1.tar+gzip"
		digest:     "sha256:933a3ce2c78a89e4646f6314e7e5b10ffd5d7f4d72ecf7a320d5a3e110f1e146"
		size:       1164
	},
	{
		pk:         "GROUP#unittesting#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19#URI#docker.io/mongo:latest"
		sk:         "DIGEST#sha256:931b7ff0cb6f494b27d31a4cbec3efe62ac54676add9c7469560302f1541ecaf"
		media_type: "application/vnd.oci.image.layer.v1.tar+gzip"
		digest:     "sha256:931b7ff0cb6f494b27d31a4cbec3efe62ac54676add9c7469560302f1541ecaf"
		size:       4499070
	},
	{
		pk:         "GROUP#unittesting#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19#URI#docker.io/mongo:latest"
		sk:         "DIGEST#sha256:5aa8dfb74c5eea943287c0f8406eec5393bb48c44b288c8f073cbe7037cc2387"
		media_type: "application/vnd.oci.image.layer.v1.tar+gzip"
		digest:     "sha256:5aa8dfb74c5eea943287c0f8406eec5393bb48c44b288c8f073cbe7037cc2387"
		size:       4499070
	},
	{
		pk:         "GROUP#unittesting#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19#URI#docker.io/mongo:latest"
		sk:         "DIGEST#sha256:5ee4f093a153f722212a0fd67ed29562a336cea9236ccd53a722c72fa0698af7"
		media_type: "application/vnd.oci.image.layer.v1.tar+gzip"
		digest:     "sha256:5ee4f093a153f722212a0fd67ed29562a336cea9236ccd53a722c72fa0698af7"
		size:       4499070
	},
] & [...#RootDockerImageLayerItem]
