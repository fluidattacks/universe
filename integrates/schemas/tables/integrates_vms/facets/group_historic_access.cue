package group_historic_access

import (
	"fluidattacks.com/types/group_access"
)

#groupHistoricAccessPk: =~"^USER#[a-zA-Z0-9._%+-@]+#GROUP#[a-z]+$"
#groupHistoricAccessSk: =~"^STATE#[0-9TZ:+.-]+$"

#GroupHistoricAccessKeys: {
	pk: string & #groupHistoricAccessPk
	sk: string & #groupHistoricAccessSk
}

#GroupHistoricAccessItem: {
	#GroupHistoricAccessKeys
	group_access.#GroupAccess
}

GroupHistoricAccess:
{
	FacetName: "group_historic_access"
	KeyAttributeAlias: {
		PartitionKeyAlias: "USER#email#GROUP#name"
		SortKeyAlias:      "STATE#iso8601utc"
	}
	NonKeyAttributes: [
		"domain",
		"email",
		"group_name",
		"state",
		"expiration_time",
	]
	DataAccess: {
		MySql: {}
	}
}

GroupHistoricAccess: TableData: [
	{
		sk: "STATE#2020-01-01T20:07:57+00:00"
		pk: "USER#org_testuser3@gmail.com#GROUP#sheele"
		state: {
			role:           "user"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: ""
		}
		group_name: "sheele"
		email:      "org_testuser3@gmail.com"
	},
	{
		sk: "STATE#2020-01-01T20:07:57+00:00"
		pk: "USER#unittest@fluidattacks.com#GROUP#unittesting"
		state: {
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Tester"
			has_access:     true
		}
		group_name: "unittesting"
		email:      "unittest@fluidattacks.com"
	},
	{
		sk: "STATE#2020-01-01T20:07:57+00:00"
		pk: "USER#integratesuser@gmail.com#GROUP#asgard"
		state: {
			role:           "group_manager"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test"
		}
		group_name: "asgard"
		email:      "integratesuser@gmail.com"
	},
	{
		sk: "STATE#2024-07-16T20:27:47.640672+00:00"
		pk: "USER#integratesmanager@gmail.com#GROUP#miquella"
		state: {
			modified_by:   "integratesmanager@gmail.com"
			has_access:    true
			role:          "customer_manager"
			modified_date: "2024-07-16T20:27:47.640672+00:00"
		}
		group_name: "miquella"
		email:      "integratesmanager@gmail.com"
	},
	{
		sk: "STATE#2024-07-16T20:27:24.535499+00:00"
		pk: "USER#integratesmanager@gmail.com#GROUP#malenia"
		state: {
			modified_by:   "integratesmanager@gmail.com"
			has_access:    true
			role:          "customer_manager"
			modified_date: "2024-07-16T20:27:24.535499+00:00"
		}
		group_name: "malenia"
		email:      "integratesmanager@gmail.com"
	},
] & [...#GroupHistoricAccessItem]
