package organization_metadata

import (
	"fluidattacks.com/types/organizations"
)

#organizationMetadataPk: =~"^ORG#[a-f0-9-]{36}$"
#organizationMetadataSk: =~"^ORG#[a-z]+$"

#OrganizationMetadataKeys: {
	pk:   string & #organizationMetadataPk
	sk:   string & #organizationMetadataSk
	pk_2: string
	sk_2: string
}

#OrganizationItem: {
	#OrganizationMetadataKeys
	organizations.#OrganizationMetadata
}

OrganizationMetadata:
{
	FacetName: "organization_metadata"
	KeyAttributeAlias: {
		PartitionKeyAlias: "ORG#id"
		SortKeyAlias:      "ORG#name"
	}
	NonKeyAttributes: [
		"billing_information",
		"country",
		"created_by",
		"created_date",
		"id",
		"jira_associated_id",
		"name",
		"pk_2",
		"payment_methods",
		"policies",
		"priority_policies",
		"sk_2",
		"state",
		"value",
		"vulnerabilities_pathfile",
	]
	DataAccess: {
		MySql: {}
	}
}

OrganizationMetadata: TableData: [
	{
		pk:           "ORG#33c08ebd-2068-47e7-9673-e1aa03dc9448"
		sk:           "ORG#kiba"
		name:         "kiba"
		created_date: "2019-11-22T20:07:57+00:00"
		id:           "33c08ebd-2068-47e7-9673-e1aa03dc9448"
		pk_2:         "ORG#all"
		sk_2:         "ORG#33c08ebd-2068-47e7-9673-e1aa03dc9448"
		state: {
			aws_external_id: "ce5a4871-850f-48ee-bb6e-4bde918fe589"
			modified_by:     "unknown"
			modified_date:   "2019-11-22T20:07:57+00:00"
			status:          "ACTIVE"
		}
		policies: {
			inactivity_period:                   90
			modified_by:                         "unknown"
			min_breaking_severity:               0.0
			modified_date:                       "2019-11-22T20:07:57+00:00"
			min_acceptance_severity:             0.0
			max_acceptance_severity:             10.0
			max_number_of_expected_contributors: 10
		}
		country:    "Colombia"
		created_by: "unknown"
	},
	{
		pk:           "ORG#7376c5fe-4634-4053-9718-e14ecbda1e6b"
		sk:           "ORG#imamura"
		name:         "imamura"
		created_date: "2019-11-22T20:07:57+00:00"
		id:           "7376c5fe-4634-4053-9718-e14ecbda1e6b"
		pk_2:         "ORG#all"
		sk_2:         "ORG#7376c5fe-4634-4053-9718-e14ecbda1e6b"
		state: {
			aws_external_id:       "016a56db-a4aa-4fba-954a-2608fd439699"
			modified_by:           "unknown"
			modified_date:         "2019-11-22T20:07:57+00:00"
			status:                "ACTIVE"
			pending_deletion_date: "2019-11-22T20:07:57+00:00"
		}
		policies: {
			min_acceptance_severity:    1.0
			vulnerability_grace_period: 0
			modified_by:                "unknown"
			min_breaking_severity:      0.0
			max_acceptance_days:        30
			modified_date:              "2019-11-22T20:07:57+00:00"
			max_acceptance_severity:    8.5
		}
		country:    "Colombia"
		created_by: "unknown@unknown.com"
	},
	{
		pk:           "ORG#c6cecc0e-bb92-4079-8b6d-c4e815c10bb1"
		sk:           "ORG#makimachi"
		name:         "makimachi"
		created_date: "2019-11-22T20:07:57+00:00"
		id:           "c6cecc0e-bb92-4079-8b6d-c4e815c10bb1"
		pk_2:         "ORG#all"
		sk_2:         "ORG#c6cecc0e-bb92-4079-8b6d-c4e815c10bb1"
		state: {
			aws_external_id: "105c358f-6557-4c36-9b00-d38da19f2458"
			modified_by:     "unknown"
			modified_date:   "2019-11-22T20:07:57+00:00"
			status:          "ACTIVE"
		}
		priority_policies: [
			{policy: "Network (AV:N)", value: 50},
		]
		policies: {
			inactivity_period:                   90
			max_number_acceptances:              4
			min_acceptance_severity:             3.0
			vulnerability_grace_period:          0
			modified_by:                         "integratesmanager@gmail.com"
			min_breaking_severity:               0.0
			max_acceptance_days:                 90
			modified_date:                       "2019-11-22T20:07:57+00:00"
			max_acceptance_severity:             7.0
			max_number_of_expected_contributors: 5
		}
		country:    "Colombia"
		created_by: "unknown@unknown.com"
	},
	{
		pk:           "ORG#ffddc7a3-7f05-4fc7-b65d-7defffa883c2"
		sk:           "ORG#himura"
		name:         "himura"
		created_date: "2019-11-22T20:07:57+00:00"
		id:           "ffddc7a3-7f05-4fc7-b65d-7defffa883c2"
		pk_2:         "ORG#all"
		sk_2:         "ORG#ffddc7a3-7f05-4fc7-b65d-7defffa883c2"
		state: {
			aws_external_id: "97cf786e-a47e-4f15-b3f0-84eb920bc774"
			modified_by:     "unknown"
			modified_date:   "2019-11-22T20:07:57+00:00"
			status:          "ACTIVE"
		}
		policies: {
			inactivity_period:       90
			modified_by:             "unknown"
			min_breaking_severity:   0.0
			modified_date:           "2019-11-22T20:07:57+00:00"
			min_acceptance_severity: 0.0
			max_acceptance_severity: 10.0
		}
		country:    "Colombia"
		created_by: "unknown@unknown.com"
	},
	{
		pk:           "ORG#f2e2777d-a168-4bea-93cd-d79142b294d2"
		sk:           "ORG#hajime"
		name:         "hajime"
		created_date: "2019-11-22T20:07:57+00:00"
		id:           "f2e2777d-a168-4bea-93cd-d79142b294d2"
		pk_2:         "ORG#all"
		sk_2:         "ORG#f2e2777d-a168-4bea-93cd-d79142b294d2"
		state: {
			aws_external_id:       "72bbf517-8611-4ec6-9a5e-00438a103653"
			modified_by:           "unknown"
			modified_date:         "2019-11-22T20:07:57+00:00"
			status:                "ACTIVE"
			pending_deletion_date: "2019-11-22T20:07:57+00:00"
		}
		policies: {
			inactivity_period:          90
			max_number_acceptances:     1
			min_acceptance_severity:    0.0
			vulnerability_grace_period: 0
			modified_by:                "integratesmanager@gmail.com"
			min_breaking_severity:      0.0
			max_acceptance_days:        60
			modified_date:              "2019-11-22T20:07:57+00:00"
			max_acceptance_severity:    7.0
		}
		country:    "Colombia"
		created_by: "unknown@unknown.com"
	},
	{
		pk:           "ORG#c2ee2d15-04ab-4f39-9795-fbe30cdeee86"
		sk:           "ORG#bulat"
		name:         "bulat"
		created_date: "2019-11-22T20:07:57+00:00"
		id:           "c2ee2d15-04ab-4f39-9795-fbe30cdeee86"
		pk_2:         "ORG#all"
		sk_2:         "ORG#c2ee2d15-04ab-4f39-9795-fbe30cdeee86"
		state: {
			aws_external_id: "0a3fd5c4-9e85-4f39-9215-3993f0ed5c14"
			modified_by:     "unknown"
			modified_date:   "2019-11-22T20:07:57+00:00"
			status:          "ACTIVE"
		}
		policies: {
			inactivity_period:       90
			modified_by:             "unknown"
			min_breaking_severity:   0.0
			modified_date:           "2019-11-22T20:07:57+00:00"
			min_acceptance_severity: 3.4
			max_acceptance_severity: 6.9
		}
		country:    "Colombia"
		created_by: "unknown@unknown.com"
	},
	{
		pk:           "ORG#ed6f051c-2572-420f-bc11-476c4e71b4ee"
		sk:           "ORG#ikari"
		name:         "ikari"
		created_date: "2019-11-30T15:00:00+00:00"
		id:           "ed6f051c-2572-420f-bc11-476c4e71b4ee"
		pk_2:         "ORG#all"
		sk_2:         "ORG#ed6f051c-2572-420f-bc11-476c4e71b4ee"
		state: {
			aws_external_id: "5f91c7ac-0da9-4875-a23f-2854a66d7ee8"
			modified_by:     "integratesmanager@gmail.com"
			modified_date:   "2019-11-30T15:00:00+00:00"
			status:          "DELETED"
		}
		policies: {
			inactivity_period:                   90
			modified_by:                         "unknown"
			min_breaking_severity:               0.0
			modified_date:                       "2019-11-22T20:07:57+00:00"
			min_acceptance_severity:             0.0
			max_acceptance_severity:             10.0
			max_number_of_expected_contributors: 5
		}
		country:    "Colombia"
		created_by: "integratesmanager@gmail.com"
	},
	{
		pk:           "ORG#956e9107-fd8d-49bc-b550-5609a7a1f6ac"
		sk:           "ORG#kamiya"
		name:         "kamiya"
		created_date: "2019-11-22T20:07:57+00:00"
		id:           "956e9107-fd8d-49bc-b550-5609a7a1f6ac"
		pk_2:         "ORG#all"
		sk_2:         "ORG#956e9107-fd8d-49bc-b550-5609a7a1f6ac"
		state: {
			aws_external_id: "bd46edab-20ff-4045-b656-aa4d4752dc85"
			modified_by:     "unknown"
			modified_date:   "2019-11-22T20:07:57+00:00"
			status:          "ACTIVE"
		}
		policies: {
			inactivity_period:          90
			max_number_acceptances:     3
			min_acceptance_severity:    1.0
			vulnerability_grace_period: 0
			modified_by:                "integratesmanager@gmail.com"
			min_breaking_severity:      0.0
			max_acceptance_days:        30
			modified_date:              "2019-11-22T20:07:57+00:00"
			max_acceptance_severity:    8.5
		}
		country:    "Colombia"
		created_by: "unknown@unknown.com"
	},
	{
		pk:           "ORG#fe80d2d4-ccb7-46d1-8489-67c6360581de"
		sk:           "ORG#tatsumi"
		name:         "tatsumi"
		created_date: "2019-11-22T20:07:57+00:00"
		id:           "fe80d2d4-ccb7-46d1-8489-67c6360581de"
		pk_2:         "ORG#all"
		sk_2:         "ORG#fe80d2d4-ccb7-46d1-8489-67c6360581de"
		state: {
			aws_external_id: "449886ae-64be-472f-b950-22e04d2c747a"
			modified_by:     "unknown"
			modified_date:   "2019-11-22T20:07:57+00:00"
			status:          "ACTIVE"
		}
		policies: {
			inactivity_period:       90
			modified_by:             "unknown"
			min_breaking_severity:   0.0
			modified_date:           "2019-11-22T20:07:57+00:00"
			min_acceptance_severity: 0.0
			max_acceptance_severity: 10.0
		}
		country:    "Colombia"
		created_by: "unknown@unknown.com"
	},
	{
		pk:           "ORG#6f28de07-9fee-43f8-9675-a647422ff0f9"
		sk:           "ORG#kanto"
		name:         "kanto"
		created_date: "2019-11-22T20:07:57+00:00"
		id:           "6f28de07-9fee-43f8-9675-a647422ff0f9"
		pk_2:         "ORG#all"
		sk_2:         "ORG#6f28de07-9fee-43f8-9675-a647422ff0f9"
		state: {
			aws_external_id: "dacd1b32-e095-4f47-b03b-682ecce72f05"
			modified_by:     "__adminEmail__"
			modified_date:   "2024-09-19T10:00:00.000000+00:00"
			status:          "ACTIVE"
		}
		policies: {
			inactivity_period:       90
			modified_by:             "__adminEmail__"
			min_breaking_severity:   0.0
			modified_date:           "2024-09-19T10:00:00.000000+00:00"
			min_acceptance_severity: 0.0
			max_acceptance_severity: 10.0
		}
		country:    "Colombia"
		created_by: "__adminEmail__"
	},
	{
		pk:           "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
		sk:           "ORG#okada"
		name:         "okada"
		created_date: "2018-02-08T00:43:18+00:00"
		id:           "38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
		pk_2:         "ORG#all"
		sk_2:         "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
		state: {
			aws_external_id:       "44b02eb8-376f-4644-adf1-93d3aee726d7"
			modified_by:           "unknown"
			modified_date:         "2018-02-08T00:43:18+00:00"
			status:                "ACTIVE"
			pending_deletion_date: "2019-11-22T20:07:57+00:00"
		}
		priority_policies: [
			{policy: "PTAAS", value: 100},
			{policy: "CSPM", value: 40},
			{policy: "Attacked (E:A)", value: 100},
			{policy: "Network (AV:N)", value: 100},
		]
		policies: {
			inactivity_period:          90
			max_number_acceptances:     2
			min_acceptance_severity:    0.0
			vulnerability_grace_period: 0
			modified_by:                "integratesmanager@gmail.com"
			min_breaking_severity:      0.0
			max_acceptance_days:        60
			modified_date:              "2019-11-22T20:07:57+00:00"
			max_acceptance_severity:    10.0
		}
		payment_methods:
		[
			{
				id:            "38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
				business_name: "Fluid"
				email:         "test@fluidattacks.com"
				country:       "Colombia"
				state:         "Antioquia"
				city:          "Medellín"
				documents: {}
			},
			{
				id:            "4722b0b7-cfeb-4898-8308-185dfc2523bc"
				business_name: "Testing Company and Sons"
				email:         "test@fluidattacks.com"
				country:       "Colombia"
				state:         "Antioquia"
				city:          "Medellín"
				documents: {}
			},
		]

		country:    "Colombia"
		created_by: "unknown@unknown.com"
	},
	{
		pk:           "ORG#d32674a9-9838-4337-b222-68c88bf54647"
		sk:           "ORG#makoto"
		name:         "makoto"
		created_date: "2019-11-22T20:07:57+00:00"
		id:           "d32674a9-9838-4337-b222-68c88bf54647"
		pk_2:         "ORG#all"
		sk_2:         "ORG#d32674a9-9838-4337-b222-68c88bf54647"
		state: {
			aws_external_id:       "cda07b37-bf28-4c0c-88a4-ca8f4b6a4251"
			modified_by:           "unknown"
			modified_date:         "2019-11-22T20:07:57+00:00"
			status:                "ACTIVE"
			pending_deletion_date: "2019-11-22T20:07:57+00:00"
		}
		policies: {
			inactivity_period:       90
			modified_by:             "unknown"
			min_breaking_severity:   0.0
			modified_date:           "2019-11-22T20:07:57+00:00"
			min_acceptance_severity: 0.0
			max_acceptance_severity: 10.0
		}
		country:    "Colombia"
		created_by: "unknown@unknown.com"
	},
	{
		country: "Colombia"
		sk:      "ORG#haligtree"
		name:    "haligtree"
		policies: {
			modified_by:             "integratesmanager@gmail.com"
			modified_date:           "2024-07-16T20:26:54.966549+00:00"
			min_acceptance_severity: 0.0
			max_acceptance_severity: 10.0
			inactivity_period:       90
		}
		pk_2:         "ORG#all"
		pk:           "ORG#5f727061-f608-4ff3-97c0-df9056011e2c"
		created_date: "2024-07-16T20:26:54.966549+00:00"
		id:           "5f727061-f608-4ff3-97c0-df9056011e2c"
		state: {
			modified_by:     "integratesmanager@gmail.com"
			aws_external_id: "392c3cb7-4f43-41fb-ba68-bdfc58ba432f"
			modified_date:   "2024-07-16T20:26:54.966549+00:00"
			status:          "ACTIVE"
		}
		created_by: "integratesmanager@gmail.com"
		sk_2:       "ORG#5f727061-f608-4ff3-97c0-df9056011e2c"
	},
] & [...#OrganizationItem]
