package stakeholder_historic_state

import (
	"fluidattacks.com/types/stakeholders"
)

#stakeholderHistoricStatePk: =~"^USER#[a-zA-Z0-9._%+-@]+$"
#stakeholderHistoricStateSk: =~"^STATE#(\\d{4})-(\\d{2})-(\\d{2})(T(\\d{2}):(\\d{2}):(\\d{2})(\\.\\d+)?(Z|([+-])(\\d{2}):(\\d{2})))?$"

#StakeholderHistoricStateKeys: {
	pk: string & #stakeholderHistoricStatePk
	sk: string & #stakeholderHistoricStateSk
}

#StakeholderHistoricStateItem: {
	#StakeholderHistoricStateKeys
	stakeholders.#StakeholderState
}

StakeholderHistoricState:
{
	FacetName: "stakeholder_historic_state"
	KeyAttributeAlias: {
		PartitionKeyAlias: "USER#email"
		SortKeyAlias:      "STATE#iso8601utc"
	}
	NonKeyAttributes: [
		"modified_date",
		"modified_by",
		"notifications_preferences",
		"trusted_devices",
	]
	DataAccess: {
		MySql: {}
	}
}

StakeholderHistoricState: TableData: [
	{
		pk:            "USER#integratesuser@gmail.com"
		sk:            "STATE#2018-02-28T16:54:12+00:00"
		modified_date: "2018-02-28T16:54:12+00:00"
		modified_by:   "integratesuser@gmail.com"
		notifications_preferences: {
			sms: []
			email:
			[
				"ACCESS_GRANTED",
				"AGENT_TOKEN",
				"EVENT_REPORT",
				"FILE_UPDATE",
				"GROUP_INFORMATION",
				"GROUP_REPORT",
				"NEW_COMMENT",
				"NEW_DRAFT",
				"PORTFOLIO_UPDATE",
				"REMEDIATE_FINDING",
				"REMINDER_NOTIFICATION",
				"ROOT_UPDATE",
				"SERVICE_UPDATE",
				"UNSUBSCRIPTION_ALERT",
				"UPDATED_TREATMENT",
				"VULNERABILITY_ASSIGNED",
				"VULNERABILITY_REPORT",
			]

		}
	},
	{
		pk:            "USER#integratesuser2@gmail.com"
		sk:            "STATE#2018-02-28T16:54:12+00:00"
		modified_date: "2018-02-28T16:54:12+00:00"
		modified_by:   "integratesuser2@gmail.com"
		notifications_preferences: {
			sms: []
			email:
			[
				"ACCESS_GRANTED",
				"EVENT_REPORT",
				"GROUP_REPORT",
				"NEW_COMMENT",
				"NEW_DRAFT",
				"REMEDIATE_FINDING",
				"REMINDER_NOTIFICATION",
				"UPDATED_TREATMENT",
				"VULNERABILITY_ASSIGNED",
				"VULNERABILITY_REPORT",
			]

		}
	},
	{
		pk:            "USER#integratesmanager@fluidattacks.com"
		sk:            "STATE#2018-02-28T16:54:12+00:00"
		modified_date: "2018-02-28T16:54:12+00:00"
		modified_by:   "integratesmanager@fluidattacks.com"
		notifications_preferences: {
			sms: []
			email:
			[
				"ACCESS_GRANTED",
				"AGENT_TOKEN",
				"EVENT_REPORT",
				"FILE_UPDATE",
				"GROUP_INFORMATION",
				"GROUP_REPORT",
				"NEW_COMMENT",
				"NEW_DRAFT",
				"PORTFOLIO_UPDATE",
				"REMEDIATE_FINDING",
				"REMINDER_NOTIFICATION",
				"ROOT_UPDATE",
				"SERVICE_UPDATE",
				"UNSUBSCRIPTION_ALERT",
				"UPDATED_TREATMENT",
				"VULNERABILITY_ASSIGNED",
				"VULNERABILITY_REPORT",
			]

		}
	},
	{
		pk:            "USER#integratesmanager@gmail.com"
		sk:            "STATE#2018-02-28T16:54:12+00:00"
		modified_date: "2018-02-28T16:54:12+00:00"
		modified_by:   "integratesmanager@gmail.com"
		notifications_preferences: {
			sms: []
			email:
			[
				"ACCESS_GRANTED",
				"AGENT_TOKEN",
				"EVENT_REPORT",
				"FILE_UPDATE",
				"GROUP_INFORMATION",
				"GROUP_REPORT",
				"NEW_COMMENT",
				"NEW_DRAFT",
				"PORTFOLIO_UPDATE",
				"REMEDIATE_FINDING",
				"REMINDER_NOTIFICATION",
				"ROOT_UPDATE",
				"SERVICE_UPDATE",
				"UNSUBSCRIPTION_ALERT",
				"UPDATED_TREATMENT",
				"VULNERABILITY_ASSIGNED",
				"VULNERABILITY_REPORT",
			]

		}
	},
	{
		pk:            "USER#customer_manager@fluidattacks.com"
		sk:            "STATE#2018-08-28T16:54:12+00:00"
		modified_date: "2018-08-28T16:54:12+00:00"
		modified_by:   "customer_manager@fluidattacks.com"
		notifications_preferences: {
			sms: []
			email:
			[
				"ACCESS_GRANTED",
				"AGENT_TOKEN",
				"EVENT_REPORT",
				"FILE_UPDATE",
				"GROUP_INFORMATION",
				"GROUP_REPORT",
				"NEW_COMMENT",
				"NEW_DRAFT",
				"PORTFOLIO_UPDATE",
				"REMEDIATE_FINDING",
				"REMINDER_NOTIFICATION",
				"ROOT_UPDATE",
				"SERVICE_UPDATE",
				"UNSUBSCRIPTION_ALERT",
				"UPDATED_TREATMENT",
				"VULNERABILITY_ASSIGNED",
				"VULNERABILITY_REPORT",
			]

		}
	},
	{
		pk:            "USER#continuoushack2@gmail.com"
		sk:            "STATE#2018-02-28T16:54:12+00:00"
		modified_date: "2018-02-28T16:54:12+00:00"
		modified_by:   "continuoushack2@gmail.com"
		notifications_preferences: {
			sms: []
			email:
			[
				"ACCESS_GRANTED",
				"AGENT_TOKEN",
				"EVENT_REPORT",
				"FILE_UPDATE",
				"GROUP_INFORMATION",
				"GROUP_REPORT",
				"NEW_COMMENT",
				"NEW_DRAFT",
				"PORTFOLIO_UPDATE",
				"REMEDIATE_FINDING",
				"REMINDER_NOTIFICATION",
				"ROOT_UPDATE",
				"SERVICE_UPDATE",
				"UNSUBSCRIPTION_ALERT",
				"UPDATED_TREATMENT",
				"VULNERABILITY_ASSIGNED",
				"VULNERABILITY_REPORT",
			]

		}
		trusted_devices:
		[
			{
				browser:          "firefox"
				device:           "ubuntu"
				first_login_date: "2024-02-14T10:55:12+00:00"
				ip_address:       "192.168.1.1"
				last_attempt:     "2024-02-14T10:55:12+00:00"
				last_login_date:  "2024-02-14T10:55:12+00:00"
				location:         "Toronto"
				otp_token_jti:    "abc961"
			},
		]

	},
	{
		pk:            "USER#continuoushacking@gmail.com"
		sk:            "STATE#2018-02-28T16:54:12+00:00"
		modified_date: "2018-02-28T16:54:12+00:00"
		modified_by:   "continuoushacking@gmail.com"
		notifications_preferences: {
			sms: []
			email:
			[
				"ACCESS_GRANTED",
				"AGENT_TOKEN",
				"EVENT_REPORT",
				"FILE_UPDATE",
				"GROUP_INFORMATION",
				"GROUP_REPORT",
				"NEW_COMMENT",
				"NEW_DRAFT",
				"PORTFOLIO_UPDATE",
				"REMEDIATE_FINDING",
				"REMINDER_NOTIFICATION",
				"ROOT_UPDATE",
				"SERVICE_UPDATE",
				"UNSUBSCRIPTION_ALERT",
				"UPDATED_TREATMENT",
				"VULNERABILITY_ASSIGNED",
				"VULNERABILITY_REPORT",
			]

		}
	},
	{
		pk:            "USER#integratesresourcer@fluidattacks.com"
		sk:            "STATE#2018-02-28T16:54:12+00:00"
		modified_date: "2018-02-28T16:54:12+00:00"
		modified_by:   "integratesresourcer@fluidattacks.com"
		notifications_preferences: {
			sms: []
			email:
			[
				"ACCESS_GRANTED",
				"EVENT_REPORT",
				"GROUP_REPORT",
				"NEW_COMMENT",
				"NEW_DRAFT",
				"REMEDIATE_FINDING",
				"REMINDER_NOTIFICATION",
				"UPDATED_TREATMENT",
				"VULNERABILITY_ASSIGNED",
				"VULNERABILITY_REPORT",
			]

		}
	},
	{
		pk:            "USER#unittest2@fluidattacks.com"
		sk:            "STATE#2018-02-28T16:54:12+00:00"
		modified_date: "2018-02-28T16:54:12+00:00"
		modified_by:   "unittest2@fluidattacks.com"
		notifications_preferences: {
			sms: []
			email:
			[
				"ACCESS_GRANTED",
				"AGENT_TOKEN",
				"EVENT_REPORT",
				"FILE_UPDATE",
				"GROUP_INFORMATION",
				"GROUP_REPORT",
				"NEW_COMMENT",
				"NEW_DRAFT",
				"PORTFOLIO_UPDATE",
				"REMEDIATE_FINDING",
				"REMINDER_NOTIFICATION",
				"ROOT_UPDATE",
				"SERVICE_UPDATE",
				"UNSUBSCRIPTION_ALERT",
				"UPDATED_TREATMENT",
				"VULNERABILITY_ASSIGNED",
				"VULNERABILITY_REPORT",
			]

		}
	},
] & [...#StakeholderHistoricStateItem]
