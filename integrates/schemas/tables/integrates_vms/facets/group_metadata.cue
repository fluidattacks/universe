package group_metadata

import (
	"fluidattacks.com/types/groups"
)

#GroupMetadataKeys: {
	pk: string & =~"^GROUP#[a-z]+$"
	sk: string & =~"^ORG#[a-z0-9-]+$"
}

#GroupItem: {
	#GroupMetadataKeys
	groups.#GroupMetadata
}

GroupMetadata:
{
	FacetName: "group_metadata"
	KeyAttributeAlias: {
		PartitionKeyAlias: "GROUP#name"
		SortKeyAlias:      "ORG#organization_id"
	}
	NonKeyAttributes: [
		"name",
		"description",
		"language",
		"files",
		"created_date",
		"agent_token",
		"state",
		"organization_id",
		"business_id",
		"business_name",
		"context",
		"disambiguation",
		"sprint_duration",
		"policies",
		"sprint_start_date",
		"created_by",
		"azure_issues",
		"gitlab_issues",
	]
	DataAccess: {
		MySql: {}
	}
}

GroupMetadata: TableData: [
	{
		pk:           "GROUP#oneshottest"
		sk:           "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
		name:         "oneshottest"
		description:  "oneshot testing"
		language:     "EN"
		created_date: "2019-01-20T22:00:00+00:00"
		state: {
			has_advanced:  false
			tier:          "ONESHOT"
			managed:       "NOT_MANAGED"
			service:       "BLACK"
			modified_by:   "unknown"
			has_essential: true
			modified_date: "2019-01-20T22:00:00+00:00"
			tags:
			[
				"test-groups",
				"another-tag",
				"test-updates",
				"test-tag",
			]

			type:   "ONESHOT"
			status: "ACTIVE"
		}
		organization_id: "38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
		business_id:     "14441323"
		business_name:   "Testing Company and Sons"
		sprint_duration: 2
		policies: {
			max_number_acceptances:              3
			min_acceptance_severity:             0.0
			vulnerability_grace_period:          10
			modified_by:                         "integratesmanager@gmail.com"
			min_breaking_severity:               3.9
			max_acceptance_days:                 90
			modified_date:                       "2021-11-22T20:07:57+00:00"
			max_acceptance_severity:             3.9
			max_number_of_expected_contributors: 5
		}
		created_by: "unknown"
	},
	{
		pk:          "GROUP#unittesting"
		sk:          "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
		name:        "unittesting"
		description: "Integrates unit test group"
		language:    "EN"
		files:
		[
			{
				modified_by:   "unittest@fluidattacks.com"
				description:   "Test"
				modified_date: "2019-03-01T20:21:00+00:00"
				file_name:     "test.zip"
			},
			{
				modified_by:   "unittest@fluidattacks.com"
				description:   "shell"
				modified_date: "2019-04-24T19:56:00+00:00"
				file_name:     "shell.exe"
			},
			{
				modified_by:   "unittest@fluidattacks.com"
				description:   "shell2"
				modified_date: "2019-04-24T19:59:00+00:00"
				file_name:     "shell2.exe"
			},
			{
				modified_by:   "unittest@fluidattacks.com"
				description:   "eerweterterter"
				modified_date: "2019-08-06T19:28:00+00:00"
				file_name:     "asdasd.py"
			},
		]

		created_date: "2018-03-08T00:43:18+00:00"
		state: {
			has_advanced:  true
			tier:          "ESSENTIAL"
			managed:       "NOT_MANAGED"
			service:       "WHITE"
			modified_by:   "unknown"
			has_essential: true
			modified_date: "2018-03-08T00:43:18+00:00"
			type:          "CONTINUOUS"
			tags:
			[
				"test-groups",
				"test-updates",
				"test-tag",
			]

			status: "ACTIVE"
		}
		organization_id:   "38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
		business_id:       "14441323"
		business_name:     "Testing Company and Sons"
		context:           "Group context test"
		disambiguation:    "Disambiguation test"
		sprint_duration:   2
		sprint_start_date: "2022-05-31T00:00:00+00:00"
		created_by:        "unknown"
		policies: {
			modified_by:   "integratesmanager@gmail.com"
			modified_date: "2023-02-15T20:07:57+00:00"
		}
	},
	{
		pk:           "GROUP#setpendingdeletion"
		sk:           "ORG#c6cecc0e-bb92-4079-8b6d-c4e815c10bb1"
		name:         "setpendingdeletion"
		description:  "Integrates group"
		language:     "EN"
		created_date: "2020-05-20T22:00:00+00:00"
		state: {
			has_advanced:  false
			tier:          "ONESHOT"
			managed:       "NOT_MANAGED"
			service:       "WHITE"
			modified_by:   "unknown"
			has_essential: false
			modified_date: "2020-05-20T22:00:00+00:00"
			type:          "CONTINUOUS"
			status:        "ACTIVE"
		}
		organization_id:   "c6cecc0e-bb92-4079-8b6d-c4e815c10bb1"
		business_id:       "14441323"
		business_name:     "Testing Company and Sons"
		sprint_duration:   2
		sprint_start_date: "2022-06-06T00:00:00+00:00"
		created_by:        "unknown"
	},
	{
		pk:           "GROUP#kurome"
		sk:           "ORG#f2e2777d-a168-4bea-93cd-d79142b294d2"
		name:         "kurome"
		description:  "Integrates group"
		language:     "EN"
		created_date: "2020-05-20T22:00:00+00:00"
		state: {
			has_advanced:  false
			tier:          "OTHER"
			managed:       "NOT_MANAGED"
			service:       "WHITE"
			modified_by:   "unknown"
			has_essential: false
			modified_date: "2020-05-20T22:00:00+00:00"
			type:          "CONTINUOUS"
			status:        "ACTIVE"
		}
		organization_id:   "f2e2777d-a168-4bea-93cd-d79142b294d2"
		business_id:       "14441323"
		business_name:     "Testing Company and Sons"
		sprint_duration:   2
		sprint_start_date: "2022-06-06T00:00:00+00:00"
		created_by:        "unknown"
	},
	{
		pk:          "GROUP#continuoustesting"
		sk:          "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
		name:        "continuoustesting"
		description: "Integrates continuous group"
		language:    "EN"
		files:
		[
			{
				modified_by:   "unittest@fluidattacks.com"
				description:   "shell2"
				modified_date: "2019-04-24T19:59:00+00:00"
				file_name:     "shell2.exe"
			},
			{
				modified_by:   "unittest@fluidattacks.com"
				description:   "asdasd"
				modified_date: "2019-08-06T19:28:00+00:00"
				file_name:     "asdasd.py"
			},
		]

		created_date: "2020-05-20T22:00:00+00:00"
		state: {
			has_advanced:  true
			tier:          "ADVANCED"
			managed:       "NOT_MANAGED"
			service:       "WHITE"
			modified_by:   "unknown"
			has_essential: true
			modified_date: "2020-05-20T22:00:00+00:00"
			tags:
			[
				"another-tag",
				"test-tag",
			]

			type:   "CONTINUOUS"
			status: "ACTIVE"
		}
		organization_id:   "38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
		business_id:       "14441323"
		business_name:     "Testing Company and Sons"
		sprint_duration:   2
		sprint_start_date: "2022-06-06T00:00:00+00:00"
		created_by:        "unknown"
	},
	{
		pk:           "GROUP#gotham"
		sk:           "ORG#c6cecc0e-bb92-4079-8b6d-c4e815c10bb1"
		name:         "gotham"
		description:  "Integrates group"
		language:     "EN"
		created_date: "2020-05-20T22:00:00+00:00"
		state: {
			has_advanced:  false
			tier:          "ONESHOT"
			managed:       "NOT_MANAGED"
			service:       "WHITE"
			modified_by:   "unknown"
			has_essential: false
			modified_date: "2020-05-20T22:00:00+00:00"
			tags:
			[
				"test-nogroups",
			]

			type:   "CONTINUOUS"
			status: "ACTIVE"
		}
		organization_id:   "c6cecc0e-bb92-4079-8b6d-c4e815c10bb1"
		business_id:       "14441323"
		business_name:     "Testing Company and Sons"
		sprint_duration:   2
		sprint_start_date: "2022-06-06T00:00:00+00:00"
		created_by:        "unknown"
	},
	{
		pk:           "GROUP#barranquilla"
		sk:           "ORG#956e9107-fd8d-49bc-b550-5609a7a1f6ac"
		name:         "barranquilla"
		description:  "Integrates group"
		language:     "EN"
		created_date: "2020-05-20T22:00:00+00:00"
		state: {
			has_advanced:  false
			tier:          "ADVANCED"
			managed:       "NOT_MANAGED"
			service:       "WHITE"
			modified_by:   "unknown"
			has_essential: false
			modified_date: "2020-05-20T22:00:00+00:00"
			tags:
			[
				"test-newgroups",
			]

			type:   "CONTINUOUS"
			status: "ACTIVE"
		}
		organization_id:   "956e9107-fd8d-49bc-b550-5609a7a1f6ac"
		business_id:       "14441323"
		business_name:     "Testing Company and Sons"
		sprint_duration:   2
		sprint_start_date: "2022-06-06T00:00:00+00:00"
		created_by:        "unknown"
	},
	{
		pk:           "GROUP#lubbock"
		sk:           "ORG#fe80d2d4-ccb7-46d1-8489-67c6360581de"
		name:         "lubbock"
		description:  "Integrates group"
		language:     "EN"
		created_date: "2020-05-20T22:00:00+00:00"
		state: {
			has_advanced:  true
			tier:          "FREE"
			managed:       "NOT_MANAGED"
			service:       "WHITE"
			modified_by:   "unknown"
			has_essential: true
			modified_date: "2020-05-20T22:00:00+00:00"
			tags:
			[
				"another-tag",
			]

			type:   "CONTINUOUS"
			status: "ACTIVE"
		}
		organization_id:   "fe80d2d4-ccb7-46d1-8489-67c6360581de"
		business_id:       "14441323"
		business_name:     "Testing Company and Sons"
		sprint_duration:   2
		sprint_start_date: "2022-06-06T00:00:00+00:00"
		created_by:        "unknown"
	},
	{
		pk:           "GROUP#monteria"
		sk:           "ORG#956e9107-fd8d-49bc-b550-5609a7a1f6ac"
		name:         "monteria"
		description:  "Integrates group"
		language:     "EN"
		created_date: "2020-05-20T22:00:00+00:00"
		state: {
			has_advanced:  false
			tier:          "FREE"
			managed:       "NOT_MANAGED"
			service:       "WHITE"
			modified_by:   "unknown"
			has_essential: false
			modified_date: "2020-05-20T22:00:00+00:00"
			type:          "CONTINUOUS"
			tags:
			[
				"test-newgroups",
			]

			status: "ACTIVE"
		}
		organization_id:   "956e9107-fd8d-49bc-b550-5609a7a1f6ac"
		business_id:       "14441323"
		business_name:     "Testing Company and Sons"
		sprint_duration:   2
		sprint_start_date: "2022-06-06T00:00:00+00:00"
		created_by:        "unknown"
	},
	{
		pk:           "GROUP#deleteimamura"
		sk:           "ORG#7376c5fe-4634-4053-9718-e14ecbda1e6b"
		name:         "deleteimamura"
		description:  "Integrates group"
		language:     "EN"
		created_date: "2020-05-20T22:00:00+00:00"
		state: {
			has_advanced:  true
			tier:          "ESSENTIAL"
			managed:       "NOT_MANAGED"
			service:       "WHITE"
			modified_by:   "unknown"
			has_essential: true
			modified_date: "2020-05-20T22:00:00+00:00"
			type:          "CONTINUOUS"
			status:        "ACTIVE"
		}
		organization_id:   "7376c5fe-4634-4053-9718-e14ecbda1e6b"
		business_id:       "14441323"
		business_name:     "Testing Company and Sons"
		sprint_duration:   2
		sprint_start_date: "2022-06-06T00:00:00+00:00"
		created_by:        "unknown"
	},
	{
		pk:           "GROUP#asgard"
		sk:           "ORG#c6cecc0e-bb92-4079-8b6d-c4e815c10bb1"
		name:         "asgard"
		description:  "Integrates group"
		language:     "EN"
		created_date: "2020-05-20T22:00:00+00:00"
		state: {
			has_advanced:  false
			tier:          "OTHER"
			managed:       "NOT_MANAGED"
			service:       "WHITE"
			modified_by:   "unknown"
			has_essential: false
			modified_date: "2020-05-20T22:00:00+00:00"
			type:          "CONTINUOUS"
			tags:
			[
				"test-nogroups",
			]

			status: "ACTIVE"
		}
		organization_id:   "c6cecc0e-bb92-4079-8b6d-c4e815c10bb1"
		business_id:       "14441323"
		business_name:     "Testing Company and Sons"
		sprint_duration:   2
		sprint_start_date: "2022-06-06T00:00:00+00:00"
		created_by:        "unknown"
	},
	{
		pk:           "GROUP#deletegroup"
		sk:           "ORG#c6cecc0e-bb92-4079-8b6d-c4e815c10bb1"
		name:         "deletegroup"
		description:  "Integrates group"
		language:     "EN"
		created_date: "2020-05-20T22:00:00+00:00"
		state: {
			has_advanced:          false
			tier:                  "OTHER"
			managed:               "NOT_MANAGED"
			service:               "WHITE"
			pending_deletion_date: "2020-12-22T19:36:29+00:00"
			modified_by:           "unknown"
			has_essential:         false
			modified_date:         "2020-05-20T22:00:00+00:00"
			type:                  "CONTINUOUS"
			status:                "ACTIVE"
		}
		organization_id:   "c6cecc0e-bb92-4079-8b6d-c4e815c10bb1"
		business_id:       "14441323"
		business_name:     "Testing Company and Sons"
		sprint_duration:   2
		sprint_start_date: "2022-06-06T00:00:00+00:00"
		created_by:        "unknown"
	},
	{
		pk:           "GROUP#metropolis"
		sk:           "ORG#c6cecc0e-bb92-4079-8b6d-c4e815c10bb1"
		name:         "metropolis"
		description:  "Integrates group"
		language:     "EN"
		created_date: "2020-05-20T22:00:00+00:00"
		state: {
			has_advanced:  true
			tier:          "ESSENTIAL"
			managed:       "NOT_MANAGED"
			service:       "WHITE"
			modified_by:   "unknown"
			has_essential: false
			modified_date: "2020-05-20T22:00:00+00:00"
			tags:
			[
				"test-nogroups",
			]

			type:   "CONTINUOUS"
			status: "ACTIVE"
		}
		organization_id:   "c6cecc0e-bb92-4079-8b6d-c4e815c10bb1"
		business_id:       "14441323"
		business_name:     "Testing Company and Sons"
		sprint_duration:   2
		sprint_start_date: "2022-06-06T00:00:00+00:00"
		created_by:        "unknown"
	},
	{
		pk:           "GROUP#sheele"
		sk:           "ORG#f2e2777d-a168-4bea-93cd-d79142b294d2"
		name:         "sheele"
		description:  "Integrates group"
		language:     "EN"
		created_date: "2020-05-20T22:00:00+00:00"
		state: {
			has_advanced:  false
			tier:          "OTHER"
			managed:       "NOT_MANAGED"
			service:       "WHITE"
			modified_by:   "unknown"
			has_essential: false
			modified_date: "2020-05-20T22:00:00+00:00"
			type:          "CONTINUOUS"
			status:        "ACTIVE"
		}
		organization_id:   "f2e2777d-a168-4bea-93cd-d79142b294d2"
		business_id:       "14441323"
		business_name:     "Testing Company and Sons"
		sprint_duration:   2
		sprint_start_date: "2022-06-06T00:00:00+00:00"
		created_by:        "unknown"
	},
	{
		organization_id: "5f727061-f608-4ff3-97c0-df9056011e2c"
		sk:              "ORG#5f727061-f608-4ff3-97c0-df9056011e2c"
		name:            "malenia"
		description:     "AWS Marketplace Group 1"
		language:        "EN"
		pk:              "GROUP#malenia"
		created_date:    "2024-07-16T20:27:24.500402+00:00"
		state: {
			has_essential: true
			tier:          "FREE"
			managed:       "MANAGED"
			service:       "WHITE"
			modified_by:   "integratesmanager@gmail.com"
			has_advanced:  false
			modified_date: "2024-07-16T20:27:24.500409+00:00"
			type:          "CONTINUOUS"
			status:        "ACTIVE"
		}
		sprint_start_date: "2024-07-16T00:00:00+00:00"
		created_by:        "integratesmanager@gmail.com"
		sprint_duration:   1
	},
	{
		organization_id: "5f727061-f608-4ff3-97c0-df9056011e2c"
		sk:              "ORG#5f727061-f608-4ff3-97c0-df9056011e2c"
		name:            "miquella"
		description:     "AWS Marketplace Group 2"
		language:        "EN"
		pk:              "GROUP#miquella"
		created_date:    "2024-07-16T20:27:47.608603+00:00"
		state: {
			has_essential: true
			tier:          "FREE"
			managed:       "MANAGED"
			service:       "WHITE"
			modified_by:   "integratesmanager@gmail.com"
			has_advanced:  false
			modified_date: "2024-07-16T20:27:47.608618+00:00"
			type:          "CONTINUOUS"
			status:        "ACTIVE"
		}
		sprint_start_date: "2024-07-16T00:00:00+00:00"
		created_by:        "integratesmanager@gmail.com"
		sprint_duration:   1
	},
	{
		organization_id: "6f28de07-9fee-43f8-9675-a647422ff0f9"
		sk:              "ORG#6f28de07-9fee-43f8-9675-a647422ff0f9"
		name:            "groudon"
		description:     "Testing app 1"
		language:        "ES"
		pk:              "GROUP#groudon"
		created_date:    "2024-09-19T09:10:00+00:00"
		state: {
			has_essential: true
			has_advanced:  true
			tier:          "ADVANCED"
			managed:       "MANAGED"
			service:       "WHITE"
			modified_by:   "__adminEmail__"
			modified_date: "2024-09-19T09:10:00+00:00"
			type:          "CONTINUOUS"
			status:        "ACTIVE"
		}
		sprint_start_date: "2024-09-19T00:00:00+00:00"
		created_by:        "__adminEmail__"
		sprint_duration:   1
	},
	{
		organization_id: "6f28de07-9fee-43f8-9675-a647422ff0f9"
		sk:              "ORG#6f28de07-9fee-43f8-9675-a647422ff0f9"
		name:            "kyogre"
		description:     "Testing app 2"
		language:        "ES"
		pk:              "GROUP#kyogre"
		created_date:    "2024-09-19T09:10:00+00:00"
		state: {
			has_essential: true
			has_advanced:  true
			tier:          "ADVANCED"
			managed:       "MANAGED"
			service:       "WHITE"
			modified_by:   "__adminEmail__"
			modified_date: "2024-09-19T09:10:00+00:00"
			type:          "CONTINUOUS"
			status:        "ACTIVE"
		}
		sprint_start_date: "2024-09-19T00:00:00+00:00"
		created_by:        "__adminEmail__"
		sprint_duration:   1
	},
] & [...#GroupItem]
