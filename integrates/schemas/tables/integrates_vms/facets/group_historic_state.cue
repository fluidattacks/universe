package group_historic_state

import (
	"fluidattacks.com/types/groups"
)

#groupHistoricStatePk: =~"^GROUP#[a-z]+$"
#groupHistoricStateSk: =~"^STATE#(\\d{4})-(\\d{2})-(\\d{2})(T(\\d{2}):(\\d{2}):(\\d{2})(\\.\\d+)?(Z|([+-])(\\d{2}):(\\d{2})))?$"

#GroupHistoricStateKeys: {
	pk: string & #groupHistoricStatePk
	sk: string & #groupHistoricStateSk
}
#GroupHistoricStateItem: {
	#GroupHistoricStateKeys
	groups.#GroupState
}

GroupHistoricState:
{
	FacetName: "group_historic_state"
	KeyAttributeAlias: {
		PartitionKeyAlias: "GROUP#name"
		SortKeyAlias:      "STATE#iso8601utc"
	}
	NonKeyAttributes: [
		"type",
		"status",
		"tags",
		"modified_date",
		"modified_by",
		"comments",
		"justification",
		"has_essential",
		"has_advanced",
		"pending_deletion_date",
		"service",
		"tier",
		"managed",
		"payment_id",
	]
	DataAccess: {
		MySql: {}
	}
}

GroupHistoricState: TableData: [
	{
		has_advanced:  false
		tier:          "ONESHOT"
		managed:       "NOT_MANAGED"
		service:       "BLACK"
		sk:            "STATE#2019-01-20T22:00:00+00:00"
		modified_by:   "unknown"
		has_essential: true
		pk:            "GROUP#oneshottest"
		modified_date: "2019-01-20T22:00:00+00:00"
		type:          "ONESHOT"
		status:        "ACTIVE"
		tags:
		[
			"test-groups",
			"another-tag",
			"test-updates",
			"test-tag",
		]

	},
	{
		has_advanced:  true
		tier:          "ESSENTIAL"
		managed:       "NOT_MANAGED"
		service:       "WHITE"
		sk:            "STATE#2018-03-08T00:43:18+00:00"
		modified_by:   "unknown"
		has_essential: true
		pk:            "GROUP#unittesting"
		modified_date: "2018-03-08T00:43:18+00:00"
		tags:
		[
			"test-groups",
			"test-updates",
			"test-tag",
		]

		type:   "CONTINUOUS"
		status: "ACTIVE"
	},
	{
		has_advanced:  false
		tier:          "ONESHOT"
		managed:       "NOT_MANAGED"
		service:       "WHITE"
		sk:            "STATE#2020-05-20T22:00:00+00:00"
		modified_by:   "unknown"
		has_essential: false
		pk:            "GROUP#setpendingdeletion"
		modified_date: "2020-05-20T22:00:00+00:00"
		type:          "CONTINUOUS"
		status:        "ACTIVE"
	},
	{
		has_advanced:  false
		tier:          "OTHER"
		managed:       "NOT_MANAGED"
		service:       "WHITE"
		sk:            "STATE#2020-05-20T22:00:00+00:00"
		modified_by:   "unknown"
		has_essential: false
		pk:            "GROUP#kurome"
		modified_date: "2020-05-20T22:00:00+00:00"
		type:          "CONTINUOUS"
		status:        "ACTIVE"
	},
	{
		has_advanced:  true
		tier:          "ADVANCED"
		managed:       "NOT_MANAGED"
		service:       "WHITE"
		sk:            "STATE#2020-05-20T22:00:00+00:00"
		modified_by:   "unknown"
		has_essential: true
		pk:            "GROUP#continuoustesting"
		modified_date: "2020-05-20T22:00:00+00:00"
		type:          "CONTINUOUS"
		tags:
		[
			"another-tag",
			"test-tag",
		]

		status: "ACTIVE"
	},
	{
		has_advanced:  false
		tier:          "ONESHOT"
		managed:       "NOT_MANAGED"
		service:       "WHITE"
		sk:            "STATE#2020-05-20T22:00:00+00:00"
		modified_by:   "unknown"
		has_essential: false
		pk:            "GROUP#gotham"
		modified_date: "2020-05-20T22:00:00+00:00"
		type:          "CONTINUOUS"
		tags:
		[
			"test-nogroups",
		]

		status: "ACTIVE"
	},
	{
		has_advanced:  false
		tier:          "ADVANCED"
		managed:       "NOT_MANAGED"
		service:       "WHITE"
		sk:            "STATE#2020-05-20T22:00:00+00:00"
		modified_by:   "unknown"
		has_essential: false
		pk:            "GROUP#barranquilla"
		modified_date: "2020-05-20T22:00:00+00:00"
		tags:
		[
			"test-newgroups",
		]

		type:   "CONTINUOUS"
		status: "ACTIVE"
	},
	{
		has_advanced:  true
		tier:          "FREE"
		managed:       "NOT_MANAGED"
		service:       "WHITE"
		sk:            "STATE#2020-05-20T22:00:00+00:00"
		modified_by:   "unknown"
		has_essential: true
		pk:            "GROUP#lubbock"
		modified_date: "2020-05-20T22:00:00+00:00"
		tags:
		[
			"another-tag",
		]

		type:   "CONTINUOUS"
		status: "ACTIVE"
	},
	{
		has_advanced:  false
		tier:          "FREE"
		managed:       "NOT_MANAGED"
		service:       "WHITE"
		sk:            "STATE#2020-05-20T22:00:00+00:00"
		modified_by:   "unknown"
		has_essential: false
		pk:            "GROUP#monteria"
		modified_date: "2020-05-20T22:00:00+00:00"
		type:          "CONTINUOUS"
		status:        "ACTIVE"
		tags:
		[
			"test-newgroups",
		]

	},
	{
		has_advanced:  true
		tier:          "ESSENTIAL"
		managed:       "NOT_MANAGED"
		service:       "WHITE"
		sk:            "STATE#2020-05-20T22:00:00+00:00"
		modified_by:   "unknown"
		has_essential: true
		pk:            "GROUP#deleteimamura"
		modified_date: "2020-05-20T22:00:00+00:00"
		type:          "CONTINUOUS"
		status:        "ACTIVE"
	},
	{
		has_advanced: false
		tier:         "OTHER"
		managed:      "NOT_MANAGED"
		service:      "WHITE"
		sk:           "STATE#2020-05-20T22:00:00+00:00"
		modified_by:  "unknown"
		tags:
		[
			"test-nogroups",
		]

		has_essential: false
		pk:            "GROUP#asgard"
		modified_date: "2020-05-20T22:00:00+00:00"
		type:          "CONTINUOUS"
		status:        "ACTIVE"
	},
	{
		has_advanced:          false
		tier:                  "OTHER"
		managed:               "NOT_MANAGED"
		service:               "WHITE"
		pending_deletion_date: "2020-12-22T19:36:29+00:00"
		sk:                    "STATE#2020-05-20T22:00:00+00:00"
		modified_by:           "unknown"
		has_essential:         false
		pk:                    "GROUP#deletegroup"
		modified_date:         "2020-05-20T22:00:00+00:00"
		type:                  "CONTINUOUS"
		status:                "ACTIVE"
	},
	{
		has_advanced:  false
		tier:          "ESSENTIAL"
		managed:       "NOT_MANAGED"
		service:       "WHITE"
		sk:            "STATE#2020-05-20T22:00:00+00:00"
		modified_by:   "unknown"
		has_essential: false
		pk:            "GROUP#metropolis"
		modified_date: "2020-05-20T22:00:00+00:00"
		tags:
		[
			"test-nogroups",
		]

		type:   "CONTINUOUS"
		status: "ACTIVE"
	},
	{
		has_advanced:  false
		tier:          "OTHER"
		managed:       "NOT_MANAGED"
		service:       "WHITE"
		sk:            "STATE#2020-05-20T22:00:00+00:00"
		modified_by:   "unknown"
		has_essential: false
		pk:            "GROUP#sheele"
		modified_date: "2020-05-20T22:00:00+00:00"
		type:          "CONTINUOUS"
		status:        "ACTIVE"
	},
] & [...#GroupHistoricStateItem]
