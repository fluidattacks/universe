package aws_marketplace_subscription_metadata

import (
	"fluidattacks.com/types/marketplace"
)

#AwsMarketplaceSubscriptionMetadataPk: =~"^AWS_PRODUCT#[a-zA-Z0-9]+$"
#AwsMarketplaceSubscriptionMetadataSk: =~"^AWS_CUSTOMER#[a-zA-Z0-9]+$"

#AwsMarketplaceSubscriptionMetadataKeys: {
	pk: string & #AwsMarketplaceSubscriptionMetadataPk
	sk: string & #AwsMarketplaceSubscriptionMetadataSk
}

#AwsMarketplaceSubscriptionItem: {
	marketplace.#AwsMarketplaceSubscriptionMetadata
	#AwsMarketplaceSubscriptionMetadataKeys
}

AwsMarketplaceSubscriptionMetadata:
{
	FacetName: "aws_marketplace_subscription_metadata"
	KeyAttributeAlias: {
		PartitionKeyAlias: "AWS_PRODUCT#product_code"
		SortKeyAlias:      "AWS_CUSTOMER#aws_customer_id"
	}
	NonKeyAttributes: [
		"aws_account_id",
		"created_at",
		"state",
		"user",
	]
	DataAccess: {
		MySql: {}
	}
}

AwsMarketplaceSubscriptionMetadata: TableData: [
	{
		aws_account_id: "900670174129"
		created_at:     "2024-04-21T18:16:00+00:00"
		pk:             "AWS_PRODUCT#be517c023bbd4f8b8f9b8f3bd"
		sk:             "AWS_CUSTOMER#D1Hv8MM4v6Q"
		state: {
			entitlements: {
				[
					{
						dimension:       "groups"
						expiration_date: "2025-04-21T18:20:00.711842+00:00"
						value:           5
					},
				]
			}
			has_free_trial: false
			modified_date:  "2024-04-21T18:20:00.711842+00:00"
			status:         "ACTIVE"
		}
	},
	{
		created_at: "2024-04-21T18:16:00+00:00"
		pk:         "AWS_PRODUCT#be517c023bbd4f8b8f9b8f3bd"
		sk:         "AWS_CUSTOMER#beekT0YP3P0"
		state: {
			entitlements: [
				{
					dimension:       "groups"
					expiration_date: "2025-04-21T18:20:00.711842+00:00"
					value:           7
				},
			]
			has_free_trial: false
			modified_date:  "2024-04-21T18:20:00.711842+00:00"
			status:         "ACTIVE"
		}
	},
	{
		created_at: "2024-04-21T18:16:00+00:00"
		pk:         "AWS_PRODUCT#be517c023bbd4f8b8f9b8f3bd"
		sk:         "AWS_CUSTOMER#4oq3nuzyj98"
		state: {
			entitlements: [
				{
					dimension:       "groups"
					expiration_date: "2025-04-21T18:20:00.711842+00:00"
					value:           1
				},
			]
			has_free_trial: false
			modified_date:  "2024-04-21T18:20:00.711842+00:00"
			status:         "ACTIVE"
		}
		user: "integratesmanager@gmail.com"
	},
] & [...#AwsMarketplaceSubscriptionItem]
