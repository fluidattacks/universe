package finding_id

#findingIdSk: =~"^FIN#[0-9]+$"

#FindingIdItem: {
	pk: "FIN_ID"
	sk: string & #findingIdSk
}

rawTableData: [
	{
		sk: "FIN#422286126"
	},
	{
		sk: "FIN#436712859"
	},
	{
		sk: "FIN#436992569"
	},
	{
		sk: "FIN#457497316"
	},
	{
		sk: "FIN#457497318"
	},
	{
		sk: "FIN#463461507"
	},
	{
		sk: "FIN#463558592"
	},
	{
		sk: "FIN#151213607"
	},
	{
		sk: "FIN#475041513"
	},
	{
		sk: "FIN#475041524"
	},
	{
		sk: "FIN#475041535"
	},
	{
		sk: "FIN#011011011"
	},
	{
		sk: "FIN#011011012"
	},
	{
		sk: "FIN#560175507"
	},
	{
		sk: "FIN#563827909"
	},
	{
		sk: "FIN#683100860"
	},
	{
		sk: "FIN#818828206"
	},
	{
		sk: "FIN#836530833"
	},
	{
		sk: "FIN#988493279"
	},
	{
		sk: "FIN#991607942"
	},
	{
		sk: "FIN#997868246"
	},
]

FindingId:
{
	FacetName: "finding_id"
	KeyAttributeAlias: {
		PartitionKeyAlias: "FIN_ID"
		SortKeyAlias:      "FIN#id"
	}
	DataAccess: {
		MySql: {}
	}
}

FindingId: TableData: [for data in rawTableData {
	{
		sk: data.sk
		pk: "FIN_ID"
	}
}] & [...#FindingIdItem]
