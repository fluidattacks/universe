package finding_metadata

import (
	"fluidattacks.com/types/findings"
)

#findingMetadataPk: =~"^FIN#[0-9a-z-]+$"
#findingMetadataSk: =~"^GROUP#[a-z]+$"

#FindingMetadataKeys: {
	pk: string & #findingMetadataPk
	sk: string & #findingMetadataSk
}

#FindingItem: {
	#FindingMetadataKeys
	findings.#FindingMetadata
}

FindingMetadata:
{
	FacetName: "finding_metadata"
	KeyAttributeAlias: {
		PartitionKeyAlias: "FIN#id"
		SortKeyAlias:      "GROUP#group_name"
	}
	NonKeyAttributes: [
		"description",
		"evidences",
		"recommendation",
		"requirements",
		"threat",
		"cvss_version",
		"title",
		"id",
		"sorts",
		"group_name",
		"severity_score",
		"attack_vector_description",
		"min_time_to_remediate",
		"state",
		"verification",
		"unreliable_indicators",
		"creation",
		"unfulfilled_requirements",
	]
	DataAccess: {
		MySql: {}
	}
}

FindingMetadata: TableData: [
	{
		requirements: "REQ.173. System must discard all potentially harmful information obtained via data inputs."
		group_name:   "unittesting"
		unfulfilled_requirements:
		[
			"173",
		]
		description:    "The application allows raw XML injection, which is parsed by the server allowing the disclosure of sensitive data via XML external entities."
		recommendation: "Use whitelists to validate and filter all inputs sent to the application, and disable External Entities so it is not possible to execute unwanted actions in the server."
		unreliable_indicators: {
			unreliable_status:                                "VULNERABLE"
			unreliable_total_open_cvssf:                      1351.176
			unreliable_where:                                 "http://webgoatlocalhost/WebGoat/xxe/simple, http://webgoatlocalhost:8080/WebGoat/xxe/content-type"
			unreliable_newest_vulnerability_report_date:      "2020-06-11T02:22:56+00:00"
			unreliable_oldest_vulnerability_report_date:      "2020-06-11T02:22:56+00:00"
			unreliable_oldest_open_vulnerability_report_date: "2020-06-11T02:22:56+00:00"
			unreliable_zero_risk_summary: {
				confirmed: 0
				rejected:  0
				requested: 0
			}
			vulnerabilities_summary: {
				open:             0
				closed:           0
				submitted:        1
				rejected:         0
				open_critical:    0
				open_high:        0
				open_low:         0
				open_medium:      0
				open_critical_v3: 0
				open_high_v3:     0
				open_low_v3:      0
				open_medium_v3:   0
			}
		}
		title:        "083. XML injection (XXE)"
		cvss_version: "3.1"
		sk:           "GROUP#unittesting"
		id:           "3c534404-f5e0-46f8-aa54-09742dd03009"
		pk:           "FIN#3c534404-f5e0-46f8-aa54-09742dd03009"
		state: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2020-06-11T05:00:00+00:00"
			status:        "CREATED"
		}
		threat: "Amenaza."
		evidences: {
			evidence3: {
				author_email:  "hacker@fluidattacks.com"
				modified_date: "2018-11-29T05:00:00+00:00"
				description:   "1"
				url:           "unittesting-3c534404-f5e0-46f8-aa54-09742dd03009-evidence_route_3"
			}
			evidence1: {
				author_email:  ""
				modified_date: "2018-11-29T05:00:00+00:00"
				description:   "Change of content type that open a window to an XMLinjection"
				url:           "unittesting-3c534404-f5e0-46f8-aa54-09742dd03009-evidence_route_1"
			}
			evidence2: {
				author_email:  ""
				modified_date: "2018-11-29T05:00:00+00:00"
				description:   "Comments with sensitive data of server files"
				url:           "unittesting-3c534404-f5e0-46f8-aa54-09742dd03009-evidence_route_2"
			}
		}
		min_time_to_remediate:     60
		sorts:                     "NO"
		attack_vector_description: "- Access information without authorization - Manipulate application behavior - Read server files and disclose their contents - Execute DOS through entity amplifications"
		creation: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2020-06-08T23:41:59+00:00"
			status:        "CREATED"
		}
		severity_score: {
			cvssf:          675.588
			base_score:     8.7
			temporal_score: 3.9
			cvss_v3:        "CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:C/C:H/I:N/A:H/E:H/RL:U/RC:C/CR:H/IR:H/AR:H/MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:H/MI:L/MA:H"
			threat_score:   8.8
			cvssf_v4:       776.047
			cvss_v4:        "CVSS:4.0/AV:N/AC:H/AT:N/PR:N/UI:N/VC:H/VI:N/VA:H/SC:L/SI:L/SA:L/MAV:N/MAC:L/MPR:N/MUI:N/MVC:H/MVI:L/MVA:H/MSC:N/MSI:N/MSA:N/CR:H/IR:H/AR:H/E:A"
		}
	},
	{
		requirements: "REQ.0077. La aplicación no debe revelar detalles del sistema interno como stack traces, fragmentos de sentencias SQL y nombres de base de datos o tablas.\r\nREQ.0176. El sistema debe restringir el acceso a objetos del sistema que tengan contenido sensible. Sólo permitirá su acceso a usuarios autorizados."
		group_name:   "oneshottest"
		unfulfilled_requirements:
		[
			"077",
			"176",
		]

		description:    "Descripción de fuga de información técnica"
		recommendation: "Eliminar el banner de los servicios con fuga de información, Verificar que los encabezados HTTP no expongan ningún nombre o versión."
		unreliable_indicators: {
			unreliable_status:                                "VULNERABLE"
			unreliable_total_open_cvssf:                      0.871
			unreliable_where:                                 "192.168.1.9"
			unreliable_newest_vulnerability_report_date:      "2020-09-12T13:45:48+00:00"
			unreliable_oldest_vulnerability_report_date:      "2020-09-12T13:45:48+00:00"
			unreliable_oldest_open_vulnerability_report_date: "2020-09-12T13:45:48+00:00"
			unreliable_zero_risk_summary: {
				confirmed: 0
				rejected:  0
				requested: 0
			}
			vulnerabilities_summary: {
				open:             0
				closed:           1
				submitted:        1
				rejected:         0
				open_critical:    0
				open_high:        0
				open_low:         0
				open_medium:      0
				open_critical_v3: 0
				open_high_v3:     0
				open_low_v3:      0
				open_medium_v3:   0
			}
		}
		title:        "037. Technical information leak"
		cvss_version: "3.1"
		sk:           "GROUP#oneshottest"
		id:           "457497318"
		pk:           "FIN#457497318"
		state: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2018-11-29T05:00:00+00:00"
			status:        "CREATED"
		}
		threat: "Amenaza."
		evidences: {
			evidence3: {
				author_email:  ""
				modified_date: "2018-11-29T05:00:00+00:00"
				description:   "1"
				url:           "oneshottest-457497318-evidence_route_3"
			}
			evidence1: {
				author_email:  ""
				modified_date: "2018-11-29T05:00:00+00:00"
				description:   "Comentario"
				url:           "oneshottest-457497318-evidence_route_1"
			}
			evidence2: {
				author_email:  ""
				modified_date: "2018-11-29T05:00:00+00:00"
				description:   "test"
				url:           "oneshottest-457497318-evidence_route_2"
			}
		}
		min_time_to_remediate:     18
		sorts:                     "NO"
		attack_vector_description: "Descripción"
		creation: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2018-04-08T00:43:18+00:00"
			status:        "CREATED"
		}
		severity_score: {
			cvssf:          0.871
			base_score:     4.6
			temporal_score: 3.9
			cvss_v3:        "CVSS:3.1/AV:A/AC:H/PR:L/UI:N/S:U/C:L/I:L/A:L/E:P/RL:T/RC:U/MAV:A/MAC:H/MPR:L/MUI:N/MS:U/MC:L/MI:L/MA:L"
			threat_score:   1.2
			cvssf_v4:       0.021
			cvss_v4:        "CVSS:4.0/AV:A/AC:H/AT:N/PR:L/UI:N/VC:L/VI:L/VA:L/SC:N/SI:N/SA:N/E:P/MAV:A/MAC:H/MPR:L/MUI:N/MVC:L/MVI:L/MVA:L"
		}
	},
	{
		requirements: "Protect system cryptographic keys\nSource code without sensitive information\nDisable insecure functionalities"
		group_name:   "unittesting"
		unfulfilled_requirements:
		[
			"145",
			"156",
			"266",
		]
		description:    "The source code repository contains sensitive information such as usernames, passwords, email addresses and IP addresses, among others. Alternatively, while values may be password=123 o aws.secret_key=test, they reveal the bad practice of storing sensitive information in the repository with no encryption, and sooner or later they can be replaced for real sensitive values.\n"
		recommendation: "- Delete all hardcoded sensitive information.\n- Change all affected access credentials.\n- Remove sensitive information from git logs.\n- Load sensitive data from safe sources such as, key vault services, configuration files properly encrypted or administrative environment variables.\n"
		unreliable_indicators: {
			unreliable_status:                                "VULNERABLE"
			total_open_cvssf:                                 0.25
			unreliable_where:                                 ".github/workflows/docker-image.yml"
			unreliable_newest_vulnerability_report_date:      "2023-01-16T03:18:48.093087+00:00"
			unreliable_oldest_vulnerability_report_date:      "2023-01-16T03:18:48.093087+00:00"
			unreliable_oldest_open_vulnerability_report_date: "2023-01-16T03:18:48.093087+00:00"
			newest_vulnerability_report_date:                 "2023-01-16T03:18:48.093087+00:00"
			oldest_vulnerability_report_date:                 "2023-01-16T03:18:48.093087+00:00"
			unreliable_zero_risk_summary: {
				confirmed: 0
				rejected:  0
				requested: 0
			}
			vulnerabilities_summary: {
				open:             0
				closed:           0
				submitted:        1
				rejected:         0
				open_critical:    0
				open_high:        0
				open_low:         0
				open_medium:      0
				open_critical_v3: 0
				open_high_v3:     0
				open_low_v3:      0
				open_medium_v3:   0
			}
		}
		title:        "009. Sensitive information in source code"
		cvss_version: "3.1"
		sk:           "GROUP#unittesting"
		id:           "8feae4c0-b3b2-4138-8baf-f3a6c60f351d"
		pk:           "FIN#8feae4c0-b3b2-4138-8baf-f3a6c60f351d"
		state: {
			justification: "NO_JUSTIFICATION"
			modified_by:   "machine@fluidattacks.com"
			modified_date: "2023-01-16T03:18:49.093087+00:00"
			source:        "MACHINE"
			status:        "CREATED"
		}
		threat: "Attacker with access to the source-code from the Internet."
		evidences: {
			evidence5: {
				author_email:  "machine@fluidattacks.com"
				description:   "JWT token in project/.github/workflows/docker-image.yml"
				is_draft:      false
				modified_date: "2023-01-16T03:18:46.019734+00:00"
				url:           "unittesting-8feae4c0-b3b2-4138-8baf-f3a6c60f351d-evidence_route_5.png"
			}
		}
		min_time_to_remediate:     30
		sorts:                     "NO"
		attack_vector_description: "Get sensitive information or private secrets.\n"
		creation: {
			"justification": "NO_JUSTIFICATION"
			"modified_by":   "machine@fluidattacks.com"
			"modified_date": "2023-01-16T03:18:45.210204+00:00"
			"source":        "MACHINE"
			"status":        "CREATED"
		}
		severity_score: {
			base_score:     3.1
			cvssf:          0.25
			cvssf_v4:       0.024
			cvss_v3:        "CVSS:3.1/AV:N/AC:H/PR:L/UI:N/S:U/C:L/I:N/A:N/E:P/RL:U/RC:C"
			cvss_v4:        "CVSS:4.0/AV:N/AC:H/AT:N/PR:L/UI:N/VC:N/VI:N/VA:N/SC:L/SI:N/SA:N/E:P"
			temporal_score: 3
			threat_score:   1.3
		}
	},
	{
		requirements: "REQ.0169. Debe usarse construcciones parametrizadas o procedimientos almacenados parametrizados para la creación dinámica de sentencias (ej: java.sql.PreparedStatement)"
		group_name:   "lubbock"
		unfulfilled_requirements:
		[
			"169",
			"173",
		]

		description:    "Se generan sentencias SQL dinámicas sin la validación requerida de datos y sin utilizar sentencias parametrizadas o procedimientos almacenados."
		recommendation: "Realizar las consultas a la base de datos por medio de sentencias o procedimientos parametrizados."
		unreliable_indicators: {
			unreliable_status:                                "VULNERABLE"
			unreliable_total_open_cvssf:                      0.0
			unreliable_where:                                 ""
			unreliable_newest_vulnerability_report_date:      "2019-12-12T13:45:48+00:00"
			unreliable_oldest_vulnerability_report_date:      "2019-12-12T13:45:48+00:00"
			unreliable_oldest_open_vulnerability_report_date: "2019-12-12T13:45:48+00:00"
			unreliable_zero_risk_summary: {
				confirmed: 0
				rejected:  0
				requested: 0
			}
			vulnerabilities_summary: {
				open:             1
				closed:           0
				submitted:        1
				rejected:         0
				open_critical:    0
				open_high:        0
				open_low:         0
				open_medium:      0
				open_critical_v3: 0
				open_high_v3:     0
				open_low_v3:      0
				open_medium_v3:   0
			}
		}
		title:        "001. SQL injection - C Sharp SQL API"
		cvss_version: "3.1"
		sk:           "GROUP#lubbock"
		id:           "818828206"
		pk:           "FIN#818828206"
		state: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2019-11-24T05:00:00+00:00"
			status:        "CREATED"
		}
		threat: ""
		evidences: {}
		min_time_to_remediate:     18
		sorts:                     "NO"
		attack_vector_description: ""
		creation: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2019-11-22T20:07:57+00:00"
			status:        "CREATED"
		}
		severity_score: {
			cvssf:          0.0
			base_score:     0.0
			temporal_score: 0.0
			cvss_v3:        "CVSS:3.1/AV:P/AC:H/PR:H/UI:R/S:U/C:N/I:N/A:N"
			cvssf_v4:       0.0
			cvss_v4:        "CVSS:4.0/AV:N/AC:L/AT:N/PR:N/UI:N/VC:N/VI:N/VA:N/SC:N/SI:N/SA:N"
			threat_score:   0.0
		}
	},
	{
		requirements: "158. System source code must be implemented in a stable, updated, tested and free of known vulnerabilities version of the chosen programming language."
		group_name:   "unittesting"
		unfulfilled_requirements:
		[
			"158",
		]

		description:    "The application imports modules that are not used. This is a bad practice because it loads modules that will not be used, and doing so unnecessarily increases the load."
		recommendation: "Import only the modules necessary for the correct functionality of the application."
		unreliable_indicators: {
			total_open_priority:                              0
			unreliable_status:                                "DRAFT"
			unreliable_total_open_cvssf:                      0.0
			unreliable_where:                                 ""
			unreliable_newest_vulnerability_report_date:      "2019-09-13T14:58:38+00:00"
			unreliable_oldest_vulnerability_report_date:      "2020-09-12T13:45:48+00:00"
			unreliable_oldest_open_vulnerability_report_date: "2020-09-12T13:45:48+00:00"
			unreliable_zero_risk_summary: {
				confirmed: 0
				rejected:  0
				requested: 0
			}
			vulnerabilities_summary: {
				open:             0
				submitted:        1
				closed:           0
				rejected:         0
				open_critical:    0
				open_high:        0
				open_low:         0
				open_medium:      0
				open_critical_v3: 0
				open_high_v3:     0
				open_low_v3:      0
				open_medium_v3:   0
			}
		}
		title:        "379. Inappropriate coding practices - Unnecessary imports"
		cvss_version: "3.1"
		sk:           "GROUP#unittesting"
		id:           "563827909"
		pk:           "FIN#563827909"
		state: {
			modified_by:   "unittest@fluidattacks.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2022-08-22T17:46:10+00:00"
			status:        "CREATED"
		}
		threat: "Authorized attacker from the Internet with access to the application."
		evidences: {}
		min_time_to_remediate:     15
		sorts:                     "NO"
		attack_vector_description: "Test attack vector."
		creation: {
			modified_by:   "unittest@fluidattacks.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2022-08-22T17:46:10+00:00"
			status:        "CREATED"
		}
		severity_score: {
			cvssf:          0.144
			base_score:     3.1
			temporal_score: 2.6
			cvss_v3:        "CVSS:3.1/AV:N/AC:H/PR:L/UI:N/S:U/C:N/I:N/A:L/E:U/RL:O/RC:R/CR:H/MAV:A/MAC:H/MPR:N/MUI:R/MS:U/MC:L/MA:L"
			threat_score:   0.6
			cvssf_v4:       0.009
			cvss_v4:        "CVSS:4.0/AV:N/AC:H/AT:N/PR:L/UI:N/VC:N/VI:N/VA:L/SC:N/SI:N/SA:N/E:U/CR:H/MAV:A/MAC:H/MPR:N/MUI:P/MVC:L/MVA:L"
		}
	},
	{
		requirements: "REQ.0077. La aplicación no debe revelar detalles del sistema interno como stack traces, fragmentos de sentencias SQL y nombres de base de datos o tablas.\r\nREQ.0176. El sistema debe restringir el acceso a objetos del sistema que tengan contenido sensible. Sólo permitirá su acceso a usuarios autorizados."
		group_name:   "unittesting"
		unfulfilled_requirements:
		[
			"077",
			"176",
		]

		description:    "Descripción de fuga de información técnica"
		recommendation: "Eliminar el banner de los servicios con fuga de información, Verificar que los encabezados HTTP no expongan ningún nombre o versión."
		unreliable_indicators: {
			total_open_priority:                              0
			unreliable_status:                                "SAFE"
			unreliable_total_open_cvssf:                      0.0
			unreliable_where:                                 ""
			unreliable_newest_vulnerability_report_date:      "2018-11-27T19:54:08+00:00"
			unreliable_oldest_vulnerability_report_date:      "2018-11-27T19:54:08+00:00"
			unreliable_oldest_open_vulnerability_report_date: "2018-11-27T19:54:08+00:00"
			unreliable_zero_risk_summary: {
				confirmed: 0
				rejected:  0
				requested: 0
			}
			vulnerabilities_summary: {
				open:             0
				closed:           1
				submitted:        1
				rejected:         0
				open_critical:    0
				open_high:        0
				open_low:         0
				open_medium:      0
				open_critical_v3: 0
				open_high_v3:     0
				open_low_v3:      0
				open_medium_v3:   0
			}
		}
		title:        "037. Technical information leak"
		cvss_version: "3.1"
		sk:           "GROUP#unittesting"
		id:           "457497316"
		pk:           "FIN#457497316"
		state: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2018-11-27T05:00:00+00:00"
			status:        "CREATED"
		}
		threat: "Amenaza."
		evidences: {
			evidence3: {
				author_email:  "integratesmanager@gmail.com"
				modified_date: "2018-11-27T05:00:00+00:00"
				description:   "Comentario"
				url:           "unittesting-457497316-evidence_route_3.png"
			}
			evidence2: {
				author_email:  "integratesmanager@gmail.com"
				modified_date: "2018-11-27T05:00:00+00:00"
				description:   "test"
				url:           "unittesting-457497316-evidence_route_2.jpg"
			}
		}
		min_time_to_remediate:     18
		sorts:                     "NO"
		attack_vector_description: "Descripción"
		creation: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2018-04-08T00:43:18+00:00"
			status:        "CREATED"
		}
		severity_score: {
			cvssf:          0.871
			base_score:     4.6
			temporal_score: 3.9
			cvss_v3:        "CVSS:3.1/AV:A/AC:H/PR:L/UI:N/S:U/C:L/I:L/A:L/E:P/RL:T/RC:U/MAV:A/MAC:H/MPR:L/MUI:N/MS:U/MC:L/MI:L/MA:L"
			threat_score:   3.9
			cvss_v4:        "CVSS:4.0/AV:A/AC:H/AT:N/PR:L/UI:N/VC:L/VI:L/VA:L/SC:N/SI:N/SA:N/MAV:A/MAC:H/MPR:L/MUI:N/MVC:L/MVI:L/MVA:L/MSC:N/MSI:N/MSA:N/E:P"
			cvssf_v4:       0.021
		}
	},
	{
		requirements: "REQ.0176. El sistema debe restringir el acceso a objetos del sistema que tengan contenido sensible. Sólo permitirá su acceso a usuarios autorizados."
		group_name:   "unittesting"
		unfulfilled_requirements:
		[
			"176",
			"177",
			"261",
			"300",
		]

		description:    "Se obtiene información de negocio, como:\r\n- Lista de usuarios\r\n- Información estratégica\r\n- Información de empleados\r\n- Información de clientes\r\n- Información de proveedores"
		recommendation: "De acuerdo a la clasificación de la información encontrada, establecer los controles necesarios para que la información sea accesible sólo a las personas indicadas."
		unreliable_indicators: {
			total_open_priority:                              6250
			unreliable_status:                                "VULNERABLE"
			unreliable_total_open_cvssf:                      3.960
			unreliable_where:                                 "192.168.1.10, 192.168.1.12, 192.168.1.13, 192.168.1.14, 192.168.1.15, 192.168.1.16, 192.168.1.17, 192.168.1.2, 192.168.1.3, 192.168.1.4, 192.168.1.5, 192.168.1.6, 192.168.1.7, 192.168.1.8, 192.168.1.9, 192.168.100.101, 192.168.100.104, 192.168.100.105, 192.168.100.108, 192.168.100.111"
			unreliable_newest_vulnerability_report_date:      "2019-09-16T21:01:24+00:00"
			unreliable_oldest_vulnerability_report_date:      "2019-08-30T14:30:13+00:00"
			unreliable_oldest_open_vulnerability_report_date: "2019-08-30T14:30:13+00:00"
			unreliable_zero_risk_summary: {
				confirmed: 0
				rejected:  0
				requested: 0
			}
			vulnerabilities_summary: {
				open:             27
				closed:           4
				submitted:        1
				rejected:         0
				open_critical:    0
				open_high:        0
				open_low:         0
				open_medium:      0
				open_critical_v3: 0
				open_high_v3:     0
				open_low_v3:      0
				open_medium_v3:   0
			}
		}
		title:        "038. Business information leak"
		cvss_version: "3.1"
		sk:           "GROUP#unittesting"
		id:           "436992569"
		pk:           "FIN#436992569"
		state: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2019-04-08T05:00:00+00:00"
			status:        "CREATED"
		}
		threat: "Risk."
		evidences: {
			exploitation: {
				author_email:  "integratesmanager@gmail.com"
				modified_date: "2019-04-08T05:00:00+00:00"
				description:   ""
				url:           "unittesting-436992569-exploitation.png"
			}
			evidence1: {
				author_email:  "integratesmanager@gmail.com"
				modified_date: "2019-04-08T05:00:00+00:00"
				description:   "Comm1"
				url:           "unittesting-436992569-evidence_route_1.png"
			}
			evidence2: {
				author_email:  "integratesmanager@gmail.com"
				modified_date: "2019-04-08T05:00:00+00:00"
				description:   "Comm2"
				url:           "unittesting-436992569-evidence_route_2.jpg"
			}
			evidence3: {
				author_email:  "integratesmanager@gmail.com"
				modified_date: "2019-04-08T05:00:00+00:00"
				description:   "Comm3"
				url:           "unittesting-436992569-evidence_route_3.png"
			}
			evidence4: {
				author_email:  "integratesmanager@gmail.com"
				modified_date: "2019-04-08T05:00:00+00:00"
				description:   "Comm4"
				url:           "unittesting-436992569-evidence_route_4.png"
			}
			evidence5: {
				author_email:  "integratesmanager@gmail.com"
				modified_date: "2019-04-08T05:00:00+00:00"
				description:   "Comm5"
				url:           "unittesting-436992569-evidence_route_5.png"
			}
			animation: {
				author_email:  "integratesmanager@gmail.com"
				modified_date: "2019-04-08T05:00:00+00:00"
				description:   ""
				url:           "unittesting-436992569-animation.webm"
			}
		}
		min_time_to_remediate:     18
		sorts:                     "NO"
		attack_vector_description: "- Atack vector"
		creation: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2019-04-08T00:43:18+00:00"
			status:        "CREATED"
		}
		verification: {
			modified_by: "integrateshacker@fluidattacks.com"
			vulnerability_ids: {
				SS: [
					"15375781-31f2-4953-ac77-f31134225747",
				]
			}
			comment_id:    "1558048727111"
			modified_date: "2020-02-21T15:41:04+00:00"
			status:        "VERIFIED"
		}
		severity_score: {
			cvssf:          0.165
			base_score:     2.9
			temporal_score: 2.7
			cvss_v3:        "CVSS:3.1/AV:A/AC:H/PR:N/UI:R/S:C/C:N/I:N/A:L/E:F/RL:W/RC:R/CR:H/IR:L/AR:H/MAV:N/MAC:L/MPR:N/MUI:N/MS:U"
			threat_score:   6.9
			cvssf_v4:       55.715
			cvss_v4:        "CVSS:4.0/AV:A/AC:H/AT:N/PR:N/UI:A/VC:N/VI:N/VA:L/SC:L/SI:L/SA:L/MAV:N/MAC:L/MPR:N/MUI:N/MSC:N/MSI:N/MSA:N/CR:H/IR:L/AR:H/E:A"
		}
	},
	{
		requirements: "REQ.0173. El sistema debe descartar toda la informacion potencialmente insegura que sea recibida por entradas de datos."
		group_name:   "unittesting"
		unfulfilled_requirements:
		[
			"176",
			"177",
			"261",
			"300",
		]

		description:    "La aplicación infectada permite la inyección de código javascript ejecutado de forma remota y permite la exfiltración de archivos o ejecución remota de comandos en el servidor."
		recommendation: "Filtrar la informacioue recibe y envia la aplicacioor medio de listas blancas"
		unreliable_indicators: {
			total_open_priority:                              250
			unreliable_status:                                "VULNERABLE"
			unreliable_total_open_cvssf:                      3.960
			unreliable_where:                                 "192.168.100.111"
			unreliable_newest_vulnerability_report_date:      "2019-09-16T21:01:24+00:00"
			unreliable_oldest_vulnerability_report_date:      "2019-08-30T14:30:13+00:00"
			unreliable_oldest_open_vulnerability_report_date: "2019-08-30T14:30:13+00:00"
			unreliable_zero_risk_summary: {
				confirmed: 0
				rejected:  0
				requested: 0
			}
		}
		title:        "010. Stored cross-site scripting (XSS)"
		cvss_version: "3.1"
		sk:           "GROUP#unittesting"
		id:           "151213607"
		pk:           "FIN#151213607"
		state: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2019-04-08T05:00:00+00:00"
			status:        "CREATED"
		}
		threat: "Risk."
		evidences: {
			exploitation: {
				author_email:  "integratesmanager@gmail.com"
				modified_date: "2019-04-08T05:00:00+00:00"
				description:   ""
				url:           "unittesting-151213607-exploitation.png"
			}
			evidence1: {
				author_email:  "integratesmanager@gmail.com"
				modified_date: "2019-04-08T05:00:00+00:00"
				description:   "Comm1"
				url:           "unittesting-151213607-evidence_route_1.png"
			}
			evidence2: {
				author_email:  "integratesmanager@gmail.com"
				modified_date: "2019-04-08T05:00:00+00:00"
				description:   "Comm2"
				url:           "unittesting-151213607-evidence_route_2.jpg"
			}
			evidence3: {
				author_email:  "integratesmanager@gmail.com"
				modified_date: "2019-04-08T05:00:00+00:00"
				description:   "Comm3"
				url:           "unittesting-151213607-evidence_route_3.png"
			}
			evidence4: {
				author_email:  "integratesmanager@gmail.com"
				modified_date: "2019-04-08T05:00:00+00:00"
				description:   "Comm4"
				url:           "unittesting-151213607-evidence_route_4.png"
			}
			evidence5: {
				author_email:  "integratesmanager@gmail.com"
				modified_date: "2019-04-08T05:00:00+00:00"
				description:   "Comm5"
				url:           "unittesting-151213607-evidence_route_5.png"
			}
			animation: {
				author_email:  "integratesmanager@gmail.com"
				modified_date: "2019-04-08T05:00:00+00:00"
				description:   ""
				url:           "unittesting-151213607-animation.webm"
			}
		}
		min_time_to_remediate:     18
		sorts:                     "NO"
		attack_vector_description: "- Atack vector"
		creation: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2019-04-08T00:43:18+00:00"
			status:        "CREATED"
		}
		verification: {
			modified_by: "integrateshacker@fluidattacks.com"
			vulnerability_ids: {
				SS: [
					"7c9e8d7a-7f35-6f04-5cce-8acb98b31cf8",
				]
			}
			comment_id:    "8617231051651"
			modified_date: "2020-02-21T15:41:04+00:00"
			status:        "VERIFIED"
		}
		severity_score: {
			cvssf:          0.165
			base_score:     2.9
			temporal_score: 2.7
			cvss_v3:        "CVSS:3.1/AV:A/AC:H/PR:N/UI:R/S:C/C:N/I:N/A:L/E:F/RL:W/RC:R/CR:H/IR:L/AR:H/MAV:N/MAC:L/MPR:N/MUI:N/MS:U"
			threat_score:   6.9
			cvssf_v4:       55.715
			cvss_v4:        "CVSS:4.0/AV:A/AC:H/AT:N/PR:N/UI:A/VC:N/VI:N/VA:L/SC:L/SI:L/SA:L/MAV:N/MAC:L/MPR:N/MUI:N/MSC:N/MSI:N/MSA:N/CR:H/IR:L/AR:H/E:A"
		}
	},
	{
		requirements: "REQ.0174. La aplicación debe garantizar que las peticiones que ejecuten transacciones no sigan un patrón discernible."
		group_name:   "unittesting"
		unfulfilled_requirements:
		[
			"029",
			"174",
		]

		description:    "La aplicación permite engañar a un usuario autenticado por medio de links manipulados para ejecutar acciones sobre la aplicación sin su consentimiento.."
		recommendation: "Hacer uso de tokens en los formularios para la verificación de las peticiones realizadas por usuarios legítimos.\r\n"
		unreliable_indicators: {
			total_open_priority:                              150
			unreliable_status:                                "VULNERABLE"
			unreliable_total_open_cvssf:                      1.516
			unreliable_where:                                 "path/to/file2.ext"
			unreliable_newest_vulnerability_report_date:      "2019-01-15T16:04:14+00:00"
			unreliable_oldest_vulnerability_report_date:      "2019-01-15T15:43:39+00:00"
			unreliable_oldest_open_vulnerability_report_date: "2019-01-15T15:43:39+00:00"
			unreliable_zero_risk_summary: {
				confirmed: 0
				rejected:  0
				requested: 3
			}
		}
		title:        "007. Cross-site request forgery"
		cvss_version: "3.1"
		sk:           "GROUP#unittesting"
		id:           "463558592"
		pk:           "FIN#463558592"
		state: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2018-12-17T05:00:00+00:00"
			status:        "CREATED"
		}
		threat: "Test."
		evidences: {
			evidence1: {
				author_email:  "integratesmanager@gmail.com"
				modified_date: "2018-12-17T05:00:00+00:00"
				description:   "test"
				url:           "unittesting-463558592-evidence_route_1.png"
			}
			evidence2: {
				author_email:  "integratesmanager@gmail.com"
				modified_date: "2018-12-17T05:00:00+00:00"
				description:   "Test2"
				url:           "unittesting-463558592-evidence_route_2.jpg"
			}
			evidence3: {
				author_email:  "integratesmanager@gmail.com"
				modified_date: "2018-12-17T05:00:00+00:00"
				description:   "Test3"
				url:           "unittesting-463558592-evidence_route_3.png"
			}
			evidence4: {
				author_email:  "integratesmanager@gmail.com"
				modified_date: "2018-12-17T05:00:00+00:00"
				description:   "An error"
				url:           "unittesting-463558592-evidence_route_4.png"
			}
			evidence5: {
				author_email:  "integratesmanager@gmail.com"
				modified_date: "2018-12-17T05:00:00+00:00"
				description:   "4"
				url:           "unittesting-463558592-evidence_route_5.png"
			}
		}
		min_time_to_remediate:     18
		sorts:                     "NO"
		attack_vector_description: "test"
		creation: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2018-04-08T00:43:18+00:00"
			status:        "CREATED"
		}
		verification: {
			modified_by: "integratesuser@gmail.com"
			vulnerability_ids: {
				SS: [
					"3bcdb384-5547-4170-a0b6-3b397a245465",
					"74632c0c-db08-47c2-b013-c70e5b67c49f",
				]
			}
			comment_id:    "1558048727999"
			modified_date: "2020-01-19T15:41:04+00:00"
			status:        "REQUESTED"
		}
		severity_score: {
			cvssf:          1.516
			base_score:     5.1
			temporal_score: 4.3
			cvss_v3:        "CVSS:3.1/AV:A/AC:H/PR:L/UI:R/S:U/C:H/I:L/A:N/E:U/RL:O/RC:R/IR:H/MAV:A/MAC:H/MPR:L/MUI:R/MS:U/MC:H/MI:L"
			threat_score:   0.7
			cvssf_v4:       0.010
			cvss_v4:        "CVSS:4.0/AV:A/AC:H/AT:N/PR:L/UI:P/VC:H/VI:L/VA:N/SC:N/SI:N/SA:N/E:U/IR:H/MAV:A/MAC:H/MPR:L/MUI:P/MVC:H/MVI:L"
		}
	},
	{
		requirements: "REQ.0265. El sistema debe restringir el acceso a funciones del sistema que ejecutan procesos críticos de negocio. sólo permitirá acceso a usuarios autorizados.\nREQ.0266. La organización debe deshabilitar las funciones inseguras de un sistema. (hardening de sistemas)"
		group_name:   "continuoustesting"
		unfulfilled_requirements:
		[
			"173",
			"265",
			"266",
		]

		description:    "Se ejecutan comandos en la consola del servidor gracias a peticiones mal configuradas, software desactualizado, funciones inseguras."
		recommendation: "- Asegurarse de que ninguna petición utilice funciones inseguras que ejecuten comando en el sistema operativo.\n- Asegurarse de que los componentes de la aplicación estén actualizados ya que estos pueden permitir ejecutar comandos gracias a múltiples vulnerabilidades."
		unreliable_indicators: {
			unreliable_status:                                "DRAFT"
			unreliable_total_open_cvssf:                      0.0
			unreliable_where:                                 ""
			unreliable_newest_vulnerability_report_date:      "2020-09-12T13:45:48+00:00"
			unreliable_oldest_vulnerability_report_date:      "2020-09-12T13:45:48+00:00"
			unreliable_oldest_open_vulnerability_report_date: "2020-09-12T13:45:48+00:00"
			unreliable_zero_risk_summary: {
				confirmed: 0
				rejected:  0
				requested: 0
			}
		}
		title:        "004. Remote command execution"
		cvss_version: "3.1"
		sk:           "GROUP#continuoustesting"
		id:           "836530833"
		pk:           "FIN#836530833"
		state: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2019-11-22T20:10:04+00:00"
			status:        "CREATED"
		}
		threat: ""
		evidences: {
			animation: {
				author_email:  ""
				modified_date: "2019-11-22T20:08:11+00:00"
				description:   ""
				url:           "continuoustesting-836530833-exploitation.png"
			}
		}
		min_time_to_remediate:     18
		sorts:                     "NO"
		attack_vector_description: ""
		creation: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2019-11-22T20:08:11+00:00"
			status:        "CREATED"
		}
		severity_score: {
			cvssf:          97.006
			base_score:     7.3
			temporal_score: 7.3
			cvss_v3:        "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:L/A:L/MAV:N/MAC:L/MUI:N"
			threat_score:   6.9
			cvssf_v4:       55.715
			cvss_v4:        "CVSS:4.0/AV:N/AC:L/AT:N/PR:N/UI:N/VC:L/VI:L/VA:L/SC:N/SI:N/SA:N/MAC:L/MUI:N"
		}
	},
	{
		requirements: "REQ.0072. El tiempo de respuesta con la concurrencia máxima esperada debe ser de no más de 5 segundos."
		group_name:   "continuoustesting"
		unfulfilled_requirements:
		[
			"072",
			"327",
		]

		description:    "Se genera la caída del servidor como resultado de ataques de amplificación, en donde una petición genera múltiples respuestas."
		recommendation: "Definir un time-out cuando una consulta o una búsqueda se está demorando mucho en procesar la información."
		unreliable_indicators: {
			unreliable_status:                                "DRAFT"
			unreliable_total_open_cvssf:                      0.0
			unreliable_where:                                 ""
			unreliable_newest_vulnerability_report_date:      "2018-11-27T19:54:08+00:00"
			unreliable_oldest_vulnerability_report_date:      "2020-09-12T13:45:48+00:00"
			unreliable_oldest_open_vulnerability_report_date: "2020-09-12T13:45:48+00:00"
			unreliable_zero_risk_summary: {
				confirmed: 0
				rejected:  0
				requested: 0
			}
		}
		title:        "002. Asymmetric denial of service"
		cvss_version: "3.1"
		sk:           "GROUP#continuoustesting"
		id:           "991607942"
		pk:           "FIN#991607942"
		state: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "DUPLICATED"
			source:        "ASM"
			modified_date: "2019-11-22T20:10:56+00:00"
			status:        "DELETED"
		}
		threat: ""
		evidences: {}
		min_time_to_remediate:     18
		sorts:                     "NO"
		attack_vector_description: ""
		creation: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2019-11-22T20:08:03+00:00"
			status:        "CREATED"
		}
		severity_score: {
			cvssf:          0.0
			base_score:     0.0
			temporal_score: 0.0
			cvss_v3:        "CVSS:3.1/AV:P/AC:H/PR:H/UI:R/S:U/C:N/I:N/A:N"
			cvss_v4:        "CVSS:4.0/AV:N/AC:L/AT:N/PR:N/UI:N/VC:N/VI:N/VA:N/SC:N/SI:N/SA:N"
			cvssf_v4:       0.0
			threat_score:   0.0
		}
	},
	{
		requirements: "REQ.0173. El sistema debe descartar toda la informacion potencialmente insegura que sea recibida por entradas de datos."
		group_name:   "unittesting"
		unfulfilled_requirements:
		[
			"173",
		]

		description:    "La aplicaciofectada permite la inyeccio de coo XML que es ejecutado de forma remota y permite la exfiltracio de archivos o ejecucioemota de comandos en el servidor."
		recommendation: "Filtrar la informacioue recibe y envia la aplicacioor medio de listas blancas"
		unreliable_indicators: {
			total_open_priority:                              400
			unreliable_status:                                "VULNERABLE"
			unreliable_total_open_cvssf:                      0.392
			unreliable_where:                                 "192.168.1.18, 192.168.100.105"
			unreliable_newest_vulnerability_report_date:      "2019-09-13T14:58:38+00:00"
			unreliable_oldest_vulnerability_report_date:      "2019-09-12T13:45:48+00:00"
			unreliable_oldest_open_vulnerability_report_date: "2019-09-12T13:45:48+00:00"
			unreliable_zero_risk_summary: {
				confirmed: 0
				rejected:  0
				requested: 0
			}
			vulnerabilities_summary: {
				open:             2
				closed:           0
				submitted:        1
				rejected:         0
				open_critical:    0
				open_high:        0
				open_low:         0
				open_medium:      0
				open_critical_v3: 0
				open_high_v3:     0
				open_low_v3:      0
				open_medium_v3:   0
			}
		}
		title:        "083. XML injection (XXE)"
		cvss_version: "3.1"
		sk:           "GROUP#unittesting"
		id:           "463461507"
		pk:           "FIN#463461507"
		state: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2019-01-10T05:00:00+00:00"
			status:        "CREATED"
		}
		threat: "<i>test</i>"
		evidences: {
			evidence2: {
				author_email:  "integratesmanager@gmail.com"
				modified_date: "2019-01-10T05:00:00+00:00"
				description:   "A2 test"
				url:           "unittesting-463461507-evidence_route_2.jpg"
			}
			evidence3: {
				author_email:  "integratesmanager@gmail.com"
				modified_date: "2019-01-10T05:00:00+00:00"
				description:   "123A"
				url:           "unittesting-463461507-evidence_route_3.png"
			}
			evidence4: {
				author_email:  "integratesmanager@gmail.com"
				modified_date: "2019-01-10T05:00:00+00:00"
				description:   "AAA1"
				url:           "unittesting-463461507-evidence_route_4.png"
			}
			evidence5: {
				author_email:  "integratesmanager@gmail.com"
				modified_date: "2019-01-10T05:00:00+00:00"
				description:   "sdasdasda"
				url:           "unittesting-463461507-evidence_route_5.png"
			}
			animation: {
				author_email:  "integratesmanager@gmail.com"
				modified_date: "2019-01-10T05:00:00+00:00"
				description:   "Animation test"
				url:           "unittesting-463461507-animation.webm"
			}
		}
		min_time_to_remediate:     18
		sorts:                     "NO"
		attack_vector_description: "Test attack vector."
		creation: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2018-04-08T00:43:18+00:00"
			status:        "CREATED"
		}
		severity_score: {
			cvssf:          0.330
			base_score:     3.7
			temporal_score: 3.2
			cvss_v3:        "CVSS:3.1/AV:A/AC:H/PR:N/UI:R/S:U/C:L/I:N/A:L/E:P/RL:O/RC:R/CR:H/MAV:A/MAC:H/MPR:N/MUI:R/MS:U/MC:L/MA:L"
			threat_score:   1.2
			cvssf_v4:       0.021
			cvss_v4:        "CVSS:4.0/AV:A/AC:H/AT:N/PR:N/UI:P/VC:L/VI:N/VA:L/SC:N/SI:N/SA:N/E:P/CR:H/MAV:A/MAC:H/MPR:N/MUI:P/MVC:L/MVA:L"
		}
	},
	{
		requirements: "R262. Los componentes provistos por terceros deben corresponder a versiones estables, probadas y actualizadas."
		group_name:   "asgard"
		unfulfilled_requirements:
		[
			"262",
		]

		description:    "Se encuentra software desactualizado con vulnerabilidades."
		recommendation: "Actualizar el software afectado a sus versiones recomendadas por el fabricante."
		unreliable_indicators: {
			total_open_priority:                              375
			unreliable_status:                                "DRAFT"
			unreliable_total_open_cvssf:                      0.0
			unreliable_where:                                 ""
			unreliable_newest_vulnerability_report_date:      "2019-09-13T14:58:38+00:00"
			unreliable_oldest_vulnerability_report_date:      "2020-09-12T13:45:48+00:00"
			unreliable_oldest_open_vulnerability_report_date: "2020-09-12T13:45:48+00:00"
			unreliable_zero_risk_summary: {
				confirmed: 0
				rejected:  0
				requested: 0
			}
		}
		title:        "011. Use of software with known vulnerabilities"
		cvss_version: "3.1"
		sk:           "GROUP#asgard"
		id:           "683100860"
		pk:           "FIN#683100860"
		state: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2018-05-02T00:45:11+00:00"
			status:        "CREATED"
		}
		threat: "This is a threat"
		evidences: {}
		min_time_to_remediate:     15
		sorts:                     "NO"
		attack_vector_description: "This is an attack vector"
		creation: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2018-04-08T00:43:18+00:00"
			status:        "CREATED"
		}
		severity_score: {
			cvssf:          0.500
			base_score:     4.3
			temporal_score: 3.5
			cvss_v3:        "CVSS:3.1/AV:A/AC:L/PR:N/UI:N/S:U/C:N/I:L/A:N/E:U/RL:W/RC:U/IR:H/MAV:A/MAC:L/MPR:L/MUI:N/MS:U/MI:L"
			threat_score:   1.2
			cvssf_v4:       0.021
			cvss_v4:        "CVSS:4.0/AV:A/AC:L/AT:N/PR:N/UI:N/VC:N/VI:L/VA:N/SC:N/SI:N/SA:N/E:U/IR:H/MAV:A/MAC:L/MPR:L/MUI:N/MVI:L"
		}
	},
	{
		requirements: "R095. The users that will access the system with administrator or root privileges must be defined.\n R096. The privileges required by the users who will access the system must be defined.\n R186. The principle of least privilege must be applied when creating new objects and roles, setting access permissions, and accessing other systems.\n"
		group_name:   "asgard"
		unfulfilled_requirements:
		[
			"095",
			"096",
			"186",
		]

		description:    "Los contenedores ejecutados en la aplicación no limitan los permisos de los usuarios ejecutando tareas como un usuario root en lugar de un usuario personalizado.\n"
		recommendation: "- Limitar los permisos del usuario con el que se ejecuta las instrucciones del contenedor.\n- Evitar utilizar el usuario root como usuario por defecto.\n"
		unreliable_indicators: {
			total_open_priority:                              300
			unreliable_status:                                "DRAFT"
			unreliable_total_open_cvssf:                      0.0
			unreliable_where:                                 ""
			unreliable_newest_vulnerability_report_date:      "2019-09-13T14:58:38+00:00"
			unreliable_oldest_vulnerability_report_date:      "2020-09-12T13:45:48+00:00"
			unreliable_oldest_open_vulnerability_report_date: "2020-09-12T13:45:48+00:00"
			unreliable_zero_risk_summary: {
				confirmed: 0
				rejected:  0
				requested: 0
			}
		}
		title:        "266. Excessive Privileges - Docker"
		cvss_version: "3.1"
		sk:           "GROUP#asgard"
		id:           "aeb1a8c9-e0fc-44f6-b6a6-e6ccfc1997c1"
		pk:           "FIN#aeb1a8c9-e0fc-44f6-b6a6-e6ccfc1997c1"
		state: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2018-05-02T00:45:11+00:00"
			status:        "CREATED"
		}
		threat: "Atacante autenticado con acceso local al contenedor.\n"
		evidences: {}
		min_time_to_remediate:     15
		sorts:                     "NO"
		attack_vector_description: "Obtener control total del contenedor con el usuario por defecto.\n"
		creation: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2018-04-08T00:43:18+00:00"
			status:        "CREATED"
		}
		severity_score: {
			cvssf:          0.500
			base_score:     4.6
			temporal_score: 3.5
			cvss_v3:        "CVSS:3.1/AV:L/AC:L/PR:L/UI:N/S:U/C:L/I:L/A:L/E:U/RL:O/RC:C"
			cvssf_v4:       0.018
			threat_score:   1.1
			cvss_v4:        "CVSS:4.0/AV:L/AC:L/AT:N/PR:L/UI:N/VC:L/VI:L/VA:L/SC:N/SI:N/SA:N/E:U"
		}
	},
	{
		requirements: "REQ.0266. La organización debe deshabilitar las funciones inseguras de un sistema. (hardening de sistemas)"
		group_name:   "asgard"
		unfulfilled_requirements:
		[
			"266",
		]

		description:    "Las dependencias de Docker no están pineadas a un digest específico, haciendo que el proceso de construcción sea incapaz de verificar la integridad de la imagen y permitiendo a un actor malicioso sobreescribir los componentes utilizados con componentes maliciosos sin dejar un rastro.\n"
		recommendation: "Utilizar mecanismos como git-commits, o utilizar artefactos y hashes para verificar la integridad de los datos.\n"
		unreliable_indicators: {
			total_open_priority:                              575
			unreliable_status:                                "DRAFT"
			unreliable_total_open_cvssf:                      0.0
			unreliable_where:                                 ""
			unreliable_newest_vulnerability_report_date:      "2019-09-13T14:58:38+00:00"
			unreliable_oldest_vulnerability_report_date:      "2020-09-12T13:45:48+00:00"
			unreliable_oldest_open_vulnerability_report_date: "2020-09-12T13:45:48+00:00"
			unreliable_zero_risk_summary: {
				confirmed: 0
				rejected:  0
				requested: 0
			}
		}
		title:        "380. Supply Chain Attack - Docker"
		cvss_version: "3.1"
		sk:           "GROUP#asgard"
		id:           "25eafe4e-e287-4915-9a19-73bc476b325c"
		pk:           "FIN#25eafe4e-e287-4915-9a19-73bc476b325c"
		state: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2018-05-02T00:45:11+00:00"
			status:        "CREATED"
		}
		threat: "Atacante anónimo desde Internet con acceso de escritura a los releasers del proveedor.\n"
		evidences: {}
		min_time_to_remediate:     15
		sorts:                     "NO"
		attack_vector_description: "Sobreescribir dependencias o componentes con contenido malicioso.\n"
		creation: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2018-04-08T00:43:18+00:00"
			status:        "CREATED"
		}
		severity_score: {
			cvssf:          0.500
			base_score:     2.6
			temporal_score: 3.5
			cvss_v3:        "CVSS:3.1/AV:N/AC:H/PR:N/UI:R/S:U/C:N/I:L/A:N/E:U/RL:O/RC:R"
			cvssf_v4:       0.008
			threat_score:   0.5
			cvss_v4:        "CVSS:4.0/AV:N/AC:H/AT:N/PR:N/UI:A/VC:N/VI:L/VA:N/SC:N/SI:N/SA:N/E:U"
		}
	},
	{
		requirements: "REQ.0173. The system must discard all potentially harmful information received via data inputs.\n"
		group_name:   "asgard"
		unfulfilled_requirements:
		[
			"173",
			"320",
			"342",
		]

		description:    "The system mixes trusted and untrusted data in the same data structure or structured message.\n"
		recommendation: "- Prevent the use of untrusted data in critical data structures or structured messages.\n"
		unreliable_indicators: {
			total_open_priority:                              100
			unreliable_status:                                "DRAFT"
			unreliable_total_open_cvssf:                      0.0
			unreliable_where:                                 ""
			unreliable_newest_vulnerability_report_date:      "2019-09-13T14:58:38+00:00"
			unreliable_oldest_vulnerability_report_date:      "2020-09-12T13:45:48+00:00"
			unreliable_oldest_open_vulnerability_report_date: "2020-09-12T13:45:48+00:00"
			unreliable_zero_risk_summary: {
				confirmed: 0
				rejected:  0
				requested: 0
			}
		}
		title:        "089. Lack of data validation - Trust boundary violation"
		cvss_version: "3.1"
		sk:           "GROUP#asgard"
		id:           "a97ea2d5-a205-43c9-8200-cef8bf60b3de"
		pk:           "FIN#a97ea2d5-a205-43c9-8200-cef8bf60b3de"
		state: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2018-05-02T00:45:11+00:00"
			status:        "CREATED"
		}
		threat: "Authenticated attacker from the Internet.\n"
		evidences: {}
		min_time_to_remediate:     45
		sorts:                     "NO"
		attack_vector_description: "Introduce data into critical data structures, which could lead to some types of injections.\n"
		creation: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2018-04-08T00:43:18+00:00"
			status:        "CREATED"
		}
		severity_score: {
			cvssf:          0.500
			base_score:     4.3
			temporal_score: 3.5
			cvss_v3:        "CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:L/A:N"
			threat_score:   5.3
			cvssf_v4:       6.063
			cvss_v4:        "CVSS:4.0/AV:N/AC:L/AT:N/PR:L/UI:N/VC:N/VI:L/VA:N/SC:N/SI:N/SA:N"
		}
	},
	{
		requirements: "REQ.0181. The transmission of sensitive information and the execution of sensitive functions must be performed through secure protocols.\n"
		group_name:   "asgard"
		unfulfilled_requirements:
		[
			"181",
		]

		description:    "Customer information is transmitted over a channel that does not use encryption.\n"
		recommendation: "Deploy the application over an encrypted communication channel, e.g. HTTPS using TLS.\n"
		unreliable_indicators: {
			total_open_priority:                              50
			unreliable_status:                                "DRAFT"
			unreliable_total_open_cvssf:                      0.0
			unreliable_where:                                 ""
			unreliable_newest_vulnerability_report_date:      "2019-09-13T14:58:38+00:00"
			unreliable_oldest_vulnerability_report_date:      "2020-09-12T13:45:48+00:00"
			unreliable_oldest_open_vulnerability_report_date: "2020-09-12T13:45:48+00:00"
			unreliable_zero_risk_summary: {
				confirmed: 0
				rejected:  0
				requested: 0
			}
		}
		title:        "332. Use of insecure channel - Source code"
		cvss_version: "3.1"
		sk:           "GROUP#asgard"
		id:           "3c9c260f-a287-4ea4-8888-7fb01c4382fd"
		pk:           "FIN#3c9c260f-a287-4ea4-8888-7fb01c4382fd"
		state: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2018-05-02T00:45:11+00:00"
			status:        "CREATED"
		}
		threat: "Anonymous attacker in adjacent network executing a man-in-the-middle attack.\n"
		evidences: {}
		min_time_to_remediate:     60
		sorts:                     "NO"
		attack_vector_description: "- Capture sensitive information and credentials in plain text.\n- Intercept communication and steal or forge requests and responses.\n"
		creation: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2018-04-08T00:43:18+00:00"
			status:        "CREATED"
		}
		severity_score: {
			cvssf:          0.500
			base_score:     3.4
			temporal_score: 3.5
			cvss_v3:        "CVSS:3.1/AV:A/AC:H/PR:N/UI:R/S:U/C:L/I:L/A:N/E:P/RL:O"
			cvssf_v4:       0.018
			threat_score:   1.1
			cvss_v4:        "CVSS:3.1/AV:A/AC:H/PR:N/UI:R/S:U/C:L/I:L/A:N/E:P/RL:O"
		}
	},
	{
		requirements: "REQ.0177. The system must not store sensitive information in temporary files or cache memory.\n"
		group_name:   "asgard"
		unfulfilled_requirements:
		[
			"177",
		]

		description:    "The application does not disable caching of input fields, thus the information will be stored in the browser's cache.\n"
		recommendation: "The data cache to browsers level must be disabled using some of these methods:\n- Use HTTP control headers that prevent cache.\n- Use meta-tags in HTML pages.\n- Disable the autocomplete field to prevent cache to fields level.\n"
		unreliable_indicators: {
			total_open_priority:                              100
			unreliable_status:                                "DRAFT"
			unreliable_total_open_cvssf:                      0.0
			unreliable_where:                                 ""
			unreliable_newest_vulnerability_report_date:      "2019-09-13T14:58:38+00:00"
			unreliable_oldest_vulnerability_report_date:      "2020-09-12T13:45:48+00:00"
			unreliable_oldest_open_vulnerability_report_date: "2020-09-12T13:45:48+00:00"
			unreliable_zero_risk_summary: {
				confirmed: 0
				rejected:  0
				requested: 0
			}
		}
		title:        "065. Cached form fields"
		cvss_version: "3.1"
		sk:           "GROUP#asgard"
		id:           "a97ea2d5-a205-43c9-8200-cef8bf67b6de"
		pk:           "FIN#a97ea2d5-a205-43c9-8200-cef8bf67b6de"
		state: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2018-05-02T00:45:11+00:00"
			status:        "CREATED"
		}
		threat: "Anonymous attacker from local network.\n"
		evidences: {}
		min_time_to_remediate:     15
		sorts:                     "NO"
		attack_vector_description: "Obtain valid users of the application."
		creation: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2018-04-08T00:43:18+00:00"
			status:        "CREATED"
		}
		severity_score: {
			cvssf:          0.500
			base_score:     3.3
			temporal_score: 3.5
			cvss_v3:        "CVSS:3.1/AV:L/AC:L/PR:N/UI:R/S:U/C:L/I:N/A:N"
			cvssf_v4:       2.297
			threat_score:   4.6
			cvss_v4:        "CVSS:4.0/AV:L/AC:L/AT:N/PR:N/UI:A/VC:L/VI:N/VA:N/SC:N/SI:N/SA:"
		}
	},
	{
		requirements: "REQ.0302. All dependencies (third-party software/libraries) must be explicitly declared (name and specific version) in a file inside the source code repository. Their source code must not be directly included in the repository.\n"
		group_name:   "asgard"
		unfulfilled_requirements:
		[
			"302",
		]

		description:    "Dependencies are not explicitly declared (name and version) within the source code. They are copied directly into the repositories.\n"
		recommendation: "All dependencies must be declared and must referenced with a dependency manager (npm, pip, maven). This allows to standardize projects construction and packaging.\n"
		unreliable_indicators: {
			total_open_priority:                              100
			unreliable_status:                                "DRAFT"
			unreliable_total_open_cvssf:                      0.0
			unreliable_where:                                 ""
			unreliable_newest_vulnerability_report_date:      "2019-09-13T14:58:38+00:00"
			unreliable_oldest_vulnerability_report_date:      "2020-09-12T13:45:48+00:00"
			unreliable_oldest_open_vulnerability_report_date: "2020-09-12T13:45:48+00:00"
			unreliable_zero_risk_summary: {
				confirmed: 0
				rejected:  0
				requested: 0
			}
		}
		title:        "079. Non-upgradable dependencies"
		cvss_version: "3.1"
		sk:           "GROUP#asgard"
		id:           "57bd4e62-c12b-4c18-b829-f2b39a7483c2"
		pk:           "FIN#57bd4e62-c12b-4c18-b829-f2b39a7483c2"
		state: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2018-05-02T00:45:11+00:00"
			status:        "CREATED"
		}
		threat: "Authenticated attacker from the Internet.\n"
		evidences: {}
		min_time_to_remediate:     120
		sorts:                     "NO"
		attack_vector_description: "- Loss of maintainability because dependencies are not maintained.\n- Late update of units in case a vulnerability is reported for one of the reported vulnerabilities.\n"
		creation: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2018-04-08T00:43:18+00:00"
			status:        "CREATED"
		}
		severity_score: {
			cvssf:          0.500
			base_score:     4.3
			temporal_score: 3.5
			cvss_v3:        "CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:L/A:N"
			threat_score:   5.3
			cvssf_v4:       6.063
			cvss_v4:        "CVSS:4.0/AV:N/AC:L/AT:N/PR:L/UI:N/VC:N/VI:L/VA:N/SC:N/SI:N/SA:N"
		}
	},
	{
		requirements: "REQ.0148. The asymmetric encryption mechanism must use a minimum key size of 2048 bits.\n"
		group_name:   "asgard"
		unfulfilled_requirements:
		[
			"148",
			"149",
			"150",
			"181",
			"336",
		]

		description:    "The application uses insecure encryption algorithms.\n"
		recommendation: "Use algorithms considered cryptographically secure.\n"
		unreliable_indicators: {
			total_open_priority:                              100
			unreliable_status:                                "DRAFT"
			unreliable_total_open_cvssf:                      0.0
			unreliable_where:                                 ""
			unreliable_newest_vulnerability_report_date:      "2019-09-13T14:58:38+00:00"
			unreliable_oldest_vulnerability_report_date:      "2020-09-12T13:45:48+00:00"
			unreliable_oldest_open_vulnerability_report_date: "2020-09-12T13:45:48+00:00"
			unreliable_zero_risk_summary: {
				confirmed: 0
				rejected:  0
				requested: 0
			}
		}
		title:        "052. Insecure encryption algorithm"
		cvss_version: "3.1"
		sk:           "GROUP#asgard"
		id:           "601395739"
		pk:           "FIN#601395739"
		state: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2018-05-02T00:45:11+00:00"
			status:        "CREATED"
		}
		threat: "Use algorithms considered cryptographically secure.\n"
		evidences: {}
		min_time_to_remediate:     30
		sorts:                     "NO"
		attack_vector_description: "- Reverse the ciphertext and collect sensible information.\n- Tamper protected data by exploiting algorithm collisions.\n"
		creation: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2018-04-08T00:43:18+00:00"
			status:        "CREATED"
		}
		severity_score: {
			cvssf:          0.500
			base_score:     2.9
			temporal_score: 3.5
			cvss_v3:        "CVSS:3.1/AV:A/AC:H/PR:N/UI:N/S:U/C:L/I:N/A:N/E:F/RL:O"
			cvssf_v4:       0.095
			threat_score:   2.3
			cvss_v4:        "CVSS:4.0/AV:A/AC:H/AT:N/PR:N/UI:N/VC:L/VI:N/VA:N/SC:N/SI:N/SA:N/E:A"
		}
	},
	{
		requirements: "REQ.0323. Binary and other types of files, which are often not audited for security purposes, should not be stored in the source code repository.\n"
		group_name:   "asgard"
		unfulfilled_requirements:
		[
			"323",
		]

		description:    "Files are stored in the repository that cannot be verified because their content is not compatible with their extension.\n"
		recommendation: "- Remove the files that should not be versioned from the repository.\n- Include the affected extensions in the .gitignore file.\n"
		unreliable_indicators: {
			total_open_priority:                              200
			unreliable_status:                                "DRAFT"
			unreliable_total_open_cvssf:                      0.0
			unreliable_where:                                 ""
			unreliable_newest_vulnerability_report_date:      "2019-09-13T14:58:38+00:00"
			unreliable_oldest_vulnerability_report_date:      "2020-09-12T13:45:48+00:00"
			unreliable_oldest_open_vulnerability_report_date: "2020-09-12T13:45:48+00:00"
			unreliable_zero_risk_summary: {
				confirmed: 0
				rejected:  0
				requested: 0
			}
		}
		title:        "117. Unverifiable files"
		cvss_version: "3.1"
		sk:           "GROUP#asgard"
		id:           "686716780"
		pk:           "FIN#686716780"
		state: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2018-05-02T00:45:11+00:00"
			status:        "CREATED"
		}
		threat: "Authenticated attacker from the Internet.\n"
		evidences: {}
		min_time_to_remediate:     15
		sorts:                     "NO"
		attack_vector_description: "- Difficult the versioning and security auditing process.\n- Introduce vulnerabilities of previous versions in the repository.\n"
		creation: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2018-04-08T00:43:18+00:00"
			status:        "CREATED"
		}
		severity_score: {
			cvssf:          0.500
			base_score:     2.9
			temporal_score: 3.5
			cvss_v3:        "CVSS:3.1/AV:N/AC:H/PR:L/UI:N/S:U/C:N/I:L/A:N/E:U"
			threat_score:   0.6
			cvssf_v4:       0.009
			cvss_v4:        "CVSS:4.0/AV:N/AC:H/AT:N/PR:L/UI:N/VC:N/VI:L/VA:N/SC:N/SI:N/SA:N/E:U"
		}
	},
	{
		requirements: "REQ.0029. The session cookies of web applications must have security attributes (HttpOnly, Secure, SameSite) and prefixes (e.g., __Host-).\n"
		group_name:   "asgard"
		unfulfilled_requirements:
		[
			"029",
		]

		description:    "The applications cookies are generated without properly setting the HttpOnly attribute.\n"
		recommendation: "The application must set the HttpOnly attribute in the cookies with sensitive information.\n"
		unreliable_indicators: {
			total_open_priority:                              100
			unreliable_status:                                "DRAFT"
			unreliable_total_open_cvssf:                      0.0
			unreliable_where:                                 ""
			unreliable_newest_vulnerability_report_date:      "2019-09-13T14:58:38+00:00"
			unreliable_oldest_vulnerability_report_date:      "2020-09-12T13:45:48+00:00"
			unreliable_oldest_open_vulnerability_report_date: "2020-09-12T13:45:48+00:00"
			unreliable_zero_risk_summary: {
				confirmed: 0
				rejected:  0
				requested: 0
			}
		}
		title:        "128. Insecurely generated cookies - HttpOnly"
		cvss_version: "3.1"
		sk:           "GROUP#asgard"
		id:           "11f9f9dc-3c53-4568-976c-9e509acb8c4f"
		pk:           "FIN#11f9f9dc-3c53-4568-976c-9e509acb8c4f"
		state: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2018-05-02T00:45:11+00:00"
			status:        "CREATED"
		}
		threat: "Authorized attacker from internet network performing a XSS attack.\n"
		evidences: {}
		min_time_to_remediate:     30
		sorts:                     "NO"
		attack_vector_description: "Obtain sensitive information by performing a XSS attack.\n"
		creation: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2018-04-08T00:43:18+00:00"
			status:        "CREATED"
		}
		severity_score: {
			cvssf:          0.500
			base_score:     2.4
			temporal_score: 3.5
			cvss_v3:        "CVSS:3.1/AV:N/AC:H/PR:L/UI:R/S:U/C:L/I:N/A:N/E:P/RL:O/RC:C"
			threat_score:   0.018
			cvssf_v4:       1.1
			cvss_v4:        "CVSS:4.0/AV:N/AC:H/AT:N/PR:L/UI:A/VC:L/VI:N/VA:N/SC:N/SI:N/SA:N/E:P"
		}
	},
	{
		requirements: "REQ.0174. Requests that execute transactions must not follow any distinguishable pattern.\n"
		group_name:   "asgard"
		unfulfilled_requirements:
		[
			"029",
			"174",
		]

		description:    "The applications configuration allows an attacker to trick authenticated users into executing actions without their consent.\n"
		recommendation: "Use of tokens in forms to verify requests done by legitimate users.\n"
		unreliable_indicators: {
			total_open_priority:                              200
			unreliable_status:                                "DRAFT"
			unreliable_total_open_cvssf:                      0.0
			unreliable_where:                                 ""
			unreliable_newest_vulnerability_report_date:      "2019-09-13T14:58:38+00:00"
			unreliable_oldest_vulnerability_report_date:      "2020-09-12T13:45:48+00:00"
			unreliable_oldest_open_vulnerability_report_date: "2020-09-12T13:45:48+00:00"
			unreliable_zero_risk_summary: {
				confirmed: 0
				rejected:  0
				requested: 0
			}
		}
		title:        "007. Cross-site request forgery"
		cvss_version: "3.1"
		sk:           "GROUP#asgard"
		id:           "9333202e-8e36-4cdf-8bc3-6a180accd8b8"
		pk:           "FIN#9333202e-8e36-4cdf-8bc3-6a180accd8b8"
		state: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2018-05-02T00:45:11+00:00"
			status:        "CREATED"
		}
		threat: "Anonymous attacker from the Internet.\n"
		evidences: {}
		min_time_to_remediate:     30
		sorts:                     "NO"
		attack_vector_description: "Impersonate a user request to execute malicious actions in the application.\n"
		creation: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2018-04-08T00:43:18+00:00"
			status:        "CREATED"
		}
		severity_score: {
			cvssf:          0.500
			base_score:     4.3
			temporal_score: 3.5
			cvss_v3:        "CVSS:3.1/AV:N/AC:L/PR:N/UI:R/S:U/C:N/I:L/A:N"
			cvssf_v4:       4.595
			threat_score:   5.1
			cvss_v4:        "CVSS:4.0/AV:N/AC:L/AT:N/PR:N/UI:A/VC:N/VI:L/VA:N/SC:N/SI:N/SA:N"
		}
	},
	{
		requirements: "REQ.0145. The systems private asymmetric or symmetric keys must be protected and should not be exposed.\n"
		group_name:   "asgard"
		unfulfilled_requirements:
		[
			"145",
			"156",
			"266",
		]

		description:    "The source code repository contains sensitive information such as usernames, passwords, email addresses and IP addresses, among others. Alternatively, while values may be password=123 o aws.secret_key=test, they reveal the bad practice of storing sensitive information in the repository with no encryption, and sooner or later they can be replaced for real sensitive values.\n"
		recommendation: "- Delete all hardcoded sensitive information.\n- Change all affected access credentials.\n- Remove sensitive information from git logs.\n- Load sensitive data from safe sources such as, key vault services, configuration files properly encrypted or administrative environment variables.\n"
		unreliable_indicators: {
			total_open_priority:                              100
			unreliable_status:                                "DRAFT"
			unreliable_total_open_cvssf:                      0.0
			unreliable_where:                                 ""
			unreliable_newest_vulnerability_report_date:      "2019-09-13T14:58:38+00:00"
			unreliable_oldest_vulnerability_report_date:      "2020-09-12T13:45:48+00:00"
			unreliable_oldest_open_vulnerability_report_date: "2020-09-12T13:45:48+00:00"
			unreliable_zero_risk_summary: {
				confirmed: 0
				rejected:  0
				requested: 0
			}
		}
		title:        "009. Sensitive information in source code"
		cvss_version: "3.1"
		sk:           "GROUP#asgard"
		id:           "566548395"
		pk:           "FIN#566548395"
		state: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2018-05-02T00:45:11+00:00"
			status:        "CREATED"
		}
		threat: "Anonymous attacker with access to the source-code from the Internet."
		evidences: {}
		min_time_to_remediate:     30
		sorts:                     "NO"
		attack_vector_description: "Get sensitive information or private secrets.\n"
		creation: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2018-04-08T00:43:18+00:00"
			status:        "CREATED"
		}
		severity_score: {
			cvssf:          0.500
			base_score:     3.7
			temporal_score: 3.5
			cvss_v3:        "CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:L/I:N/A:N/E:H/RL:U/RC:C"
			cvssf_v4:       24.251
			threat_score:   6.3
			cvss_v4:        "CVSS:4.0/AV:N/AC:H/AT:N/PR:N/UI:N/VC:L/VI:N/VA:N/SC:N/SI:N/SA:N/E:A"
		}
	},
	{
		requirements: "R040. El sistema debe validar que el formato (estructura) de los archivos corresponda con su extensión.\nR041. El sistema debe validar que el contenido de los archivos transferidos hacia el mismo sistema esté libre de código malicioso."
		group_name:   "asgard"
		unfulfilled_requirements:
		[
			"040",
			"041",
		]

		description:    "El sistema de carga de archivos permite cargar archivos con doble extensión y extensiones inseguras como (.html, .php, .exe), tampoco se valida si el archivo está libre de código malicioso o no."
		recommendation: "- Validar con expresiones regulares que el archivo corresponda con una sola extensión\n- Validar con un antivirus que el archivo no contenga ningún tipo de código malicioso.\n- Validar que el Content Type corresponda a la extension del archivo antes de subirlo al servidor"
		unreliable_indicators: {
			unreliable_status:                                "DRAFT"
			unreliable_total_open_cvssf:                      0.0
			unreliable_where:                                 ""
			unreliable_newest_vulnerability_report_date:      "2019-09-13T14:58:38+00:00"
			unreliable_oldest_vulnerability_report_date:      "2020-09-12T13:45:48+00:00"
			unreliable_oldest_open_vulnerability_report_date: "2020-09-12T13:45:48+00:00"
			unreliable_zero_risk_summary: {
				confirmed: 0
				rejected:  0
				requested: 0
			}
		}
		title:        "027. Insecure file upload"
		cvss_version: "3.1"
		sk:           "GROUP#asgard"
		id:           "997868246"
		pk:           "FIN#997868246"
		state: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2018-05-02T01:45:11+00:00"
			status:        "CREATED"
		}
		threat: "This is a threat"
		evidences: {}
		min_time_to_remediate:     16
		sorts:                     "NO"
		attack_vector_description: "This is an attack vector"
		creation: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2018-04-08T00:43:18+00:00"
			status:        "CREATED"
		}
		severity_score: {
			cvssf:          6.964
			base_score:     6.5
			temporal_score: 5.4
			cvss_v3:        "CVSS:3.1/AV:A/AC:L/PR:N/UI:N/S:U/C:N/I:H/A:N/E:P/RL:O/RC:U/IR:H/MAV:A/MAC:L/MPR:L/MUI:N/MS:U/MI:L"
			threat_score:   2.0
			cvssf_v4:       0.062
			cvss_v4:        "CVSS:4.0/AV:A/AC:L/AT:N/PR:N/UI:N/VC:N/VI:H/VA:N/SC:N/SI:N/SA:N/MAV:A/MAC:L/MPR:L/MUI:N/MVI:L/MSC:N/MSI:N/MSA:N/IR:H/E:P"
		}
	},
	{
		requirements: "REQ.0229. El sistema debe solicitar a cualquier actor que intenta autenticarse, como mínimo un nombre de usuario y una contraseña."
		group_name:   "continuoustesting"
		unfulfilled_requirements:
		[
			"229",
			"231",
			"264",
			"319",
			"328",
		]

		description:    "Los servicios críticos sobre la red interna como acceso a bases de datos, recursos compartidos con información sensible y/o web services, no cuentan con un doble factor de autenticación lo cual hace más fácil para un atacante con un usuario comprometido acceder a estos."
		recommendation: "Implementar un doble factor de autenticación ya sea por software o hardware para incrementar el nivel de protección de la autenticación de los recursos críticos."
		unreliable_indicators: {
			unreliable_status:                                "DRAFT"
			unreliable_total_open_cvssf:                      0.0
			unreliable_where:                                 ""
			unreliable_newest_vulnerability_report_date:      "2019-09-13T14:58:38+00:00"
			unreliable_oldest_vulnerability_report_date:      "2020-09-12T13:45:48+00:00"
			unreliable_oldest_open_vulnerability_report_date: "2020-09-12T13:45:48+00:00"
			unreliable_zero_risk_summary: {
				confirmed: 0
				rejected:  0
				requested: 0
			}
		}
		title:        "081. Lack of multi-factor authentication"
		cvss_version: "3.1"
		sk:           "GROUP#continuoustesting"
		id:           "475041524"
		pk:           "FIN#475041524"
		state: {
			modified_by:   "unittest@fluidattacks.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2019-02-04T17:46:10+00:00"
			status:        "CREATED"
		}
		threat: "Los servicios críticos sobre la red interna como acceso a bases de datos, recursos compartidos con información sensible y/o web services, no cuentan con un doble factor de autenticación lo cual hace más fácil para un atacante con un usuario comprometido acceder a estos."
		evidences: {
			evidence1: {
				author_email:  "unittest@fluidattacks.com"
				modified_date: "2019-02-04T17:46:10+00:00"
				description:   "Evidencia"
				url:           "continuoustesting-475041524-evidence_route_1.png"
			}
			evidence2: {
				author_email:  "unittest@fluidattacks.com"
				modified_date: "2019-02-04T17:46:10+00:00"
				description:   "AAAAAAAAAAAA"
				url:           "continuoustesting-475041524-evidence_route_2.png"
			}
		}
		min_time_to_remediate:     25
		sorts:                     "NO"
		attack_vector_description: "Los servicios críticos sobre la red interna como acceso a bases de datos, recursos compartidos con información sensible y/o web services, no cuentan con un doble factor de autenticación lo cual hace más fácil para un atacante con un usuario comprometido acceder a estos"
		creation: {
			modified_by:   "unittest@fluidattacks.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2019-02-04T17:46:10+00:00"
			status:        "CREATED"
		}
		severity_score: {
			cvssf:          0.435
			base_score:     3.8
			temporal_score: 3.4
			cvss_v3:        "CVSS:3.1/AV:P/AC:H/PR:L/UI:N/S:U/C:L/I:L/A:L/E:P/RL:O/CR:L/AR:L/MAV:N/MAC:H/MPR:H/MUI:R/MS:U/MC:L/MA:L"
			threat_score:   1.1
			cvssf_v4:       0.018
			cvss_v4:        "CVSS:4.0/AV:P/AC:H/AT:N/PR:L/UI:N/VC:L/VI:L/VA:L/SC:N/SI:N/SA:N/E:P/AR:L/MAV:N/MAC:H/MPR:H/MUI:P/MVC:L/MVA:L"
		}
	},
	{
		requirements: "REQ.0229. El sistema debe solicitar a cualquier actor que intenta autenticarse, como mínimo un nombre de usuario y una contraseña."
		group_name:   "oneshottest"
		unfulfilled_requirements:
		[
			"229",
			"231",
			"264",
			"319",
			"328",
		]

		description:    "Los servicios críticos sobre la red interna como acceso a bases de datos, recursos compartidos con información sensible y/o web services, no cuentan con un doble factor de autenticación lo cual hace más fácil para un atacante con un usuario comprometido acceder a estos."
		recommendation: "Implementar un doble factor de autenticación ya sea por software o hardware para incrementar el nivel de protección de la autenticación de los recursos críticos."
		unreliable_indicators: {
			unreliable_status:                                "DRAFT"
			unreliable_total_open_cvssf:                      0.0
			unreliable_where:                                 ""
			unreliable_newest_vulnerability_report_date:      "2019-09-13T14:58:38+00:00"
			unreliable_oldest_vulnerability_report_date:      "2020-09-12T13:45:48+00:00"
			unreliable_oldest_open_vulnerability_report_date: "2020-09-12T13:45:48+00:00"
			unreliable_zero_risk_summary: {
				confirmed: 0
				rejected:  0
				requested: 0
			}
		}
		title:        "081. Lack of multi-factor authentication"
		cvss_version: "3.1"
		sk:           "GROUP#oneshottest"
		id:           "475041535"
		pk:           "FIN#475041535"
		state: {
			modified_by:   "unittest@fluidattacks.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2019-02-04T17:46:10+00:00"
			status:        "CREATED"
		}
		threat: "Los servicios críticos sobre la red interna como acceso a bases de datos, recursos compartidos con información sensible y/o web services, no cuentan con un doble factor de autenticación lo cual hace más fácil para un atacante con un usuario comprometido acceder a estos."
		evidences: {
			evidence1: {
				author_email:  ""
				modified_date: "2019-02-04T17:46:10+00:00"
				description:   "Evidencia"
				url:           "oneshottest-475041535-evidence_route_1"
			}
			evidence2: {
				author_email:  ""
				modified_date: "2019-02-04T17:46:10+00:00"
				description:   "AAAAAAAAA"
				url:           "oneshottest-475041535-evidence_route_2"
			}
		}
		min_time_to_remediate:     18
		sorts:                     "NO"
		attack_vector_description: "Los servicios críticos sobre la red interna como acceso a bases de datos, recursos compartidos con información sensible y/o web services, no cuentan con un doble factor de autenticación lo cual hace más fácil para un atacante con un usuario comprometido acceder a estos"
		creation: {
			modified_by:   "unittest@fluidattacks.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2019-02-04T17:46:10+00:00"
			status:        "CREATED"
		}
		severity_score: {
			cvssf:          0.435
			base_score:     3.8
			temporal_score: 3.4
			cvss_v3:        "CVSS:3.1/AV:P/AC:H/PR:L/UI:N/S:U/C:L/I:L/A:L/E:P/RL:O/CR:L/AR:L/MAV:N/MAC:H/MPR:H/MUI:R/MS:U/MC:L/MA:L"
			threat_score:   1.1
			cvssf_v4:       0.018
			cvss_v4:        "CVSS:4.0/AV:P/AC:H/AT:N/PR:L/UI:N/VC:L/VI:L/VA:L/SC:N/SI:N/SA:N/E:P/AR:L/MAV:N/MAC:H/MPR:H/MUI:P/MVC:L/MVA:L"
		}
	},
	{
		requirements: "REQ.0173. El sistema debe descartar toda la informacion potencialmente insegura que sea recibida por entradas de datos."
		group_name:   "unittesting"
		unfulfilled_requirements:
		[
			"227",
			"228",
			"229",
			"231",
			"235",
			"264",
			"319",
		]

		description:    "La aplicación afectada permite la inyeccio de coo XML que es ejecutado de forma remota y permite la exfiltracio de archivos o ejecucioemota de comandos en el servidor."
		recommendation: "Filtrar la informacioue recibe y envia la aplicacion medio de listas blancas"
		unreliable_indicators: {
			unreliable_status:                                "DRAFT"
			unreliable_total_open_cvssf:                      0.0
			unreliable_where:                                 ""
			unreliable_newest_vulnerability_report_date:      "2019-09-13T14:58:38+00:00"
			unreliable_oldest_vulnerability_report_date:      "2020-09-12T13:45:48+00:00"
			unreliable_oldest_open_vulnerability_report_date: "2020-09-12T13:45:48+00:00"
			unreliable_zero_risk_summary: {
				confirmed: 0
				rejected:  0
				requested: 0
			}
		}
		title:        "006. Authentication mechanism absence or evasion"
		cvss_version: "3.1"
		sk:           "GROUP#unittesting"
		id:           "560175507"
		pk:           "FIN#560175507"
		state: {
			modified_by:   "unittest@fluidattacks.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2019-02-07T17:46:10+00:00"
			rejection: {
				rejection_date: "2019-02-07T17:46:10+00:00"
				submitted_by:   "unittest@fluidattacks.com"
				other:          ""
				reasons: {
					SS: [
						"OMISSION",
					]
				}
				rejected_by: "customer_manager@fluidattacks.com"
			}
			status: "DELETED"
		}
		threat: "<i>test</i>"
		evidences: {}
		min_time_to_remediate:     18
		sorts:                     "NO"
		attack_vector_description: "Test attack vector."
		creation: {
			modified_by:   "unittest@fluidattacks.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2019-02-04T17:46:10+00:00"
			status:        "CREATED"
		}
		severity_score: {
			cvssf:          0.330
			base_score:     3.7
			temporal_score: 3.2
			cvss_v3:        "CVSS:3.1/AV:A/AC:H/PR:N/UI:R/S:U/C:L/I:N/A:L/E:P/RL:O/RC:R/CR:H/MAV:A/MAC:H/MPR:N/MUI:R/MS:U/MC:L/MA:L"
			threat_score:   1.2
			cvssf_v4:       0.021
			cvss_v4:        "CVSS:4.0/AV:A/AC:H/AT:N/PR:N/UI:P/VC:L/VI:N/VA:L/SC:N/SI:N/SA:N/E:P/CR:H/MAV:A/MAC:H/MPR:N/MUI:P/MVC:L/MVA:L"
		}
	},
	{
		requirements: "262. The system must use stable, tested and up-to-date versions of third-party components."
		group_name:   "unittesting"
		unfulfilled_requirements:
		[
			"227",
			"228",
			"229",
			"231",
			"235",
			"264",
			"319",
		]

		description:    "The system uses the version of a software or dependency with known vulnerabilities.'"
		recommendation: "Update the affected software to the versions recommended by the vendor."
		unreliable_indicators: {
			unreliable_status:                                "DRAFT"
			unreliable_total_open_cvssf:                      0.0
			unreliable_where:                                 ""
			unreliable_newest_vulnerability_report_date:      "2019-09-13T14:58:38+00:00"
			unreliable_oldest_vulnerability_report_date:      "2020-09-12T13:45:48+00:00"
			unreliable_oldest_open_vulnerability_report_date: "2020-09-12T13:45:48+00:00"
			unreliable_zero_risk_summary: {
				confirmed: 0
				rejected:  0
				requested: 0
			}
		}
		title:        "011. Use of software with known vulnerabilities"
		cvss_version: "3.1"
		sk:           "GROUP#unittesting"
		id:           "011011011"
		pk:           "FIN#011011011"
		state: {
			modified_by:   "unittest@fluidattacks.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2019-02-07T17:46:10+00:00"
			rejection: {
				rejection_date: "2019-02-07T17:46:10+00:00"
				submitted_by:   "unittest@fluidattacks.com"
				other:          ""
				reasons: {
					SS: [
						"OMISSION",
					]
				}
				rejected_by: "customer_manager@fluidattacks.com"
			}
			status: "DELETED"
		}
		threat: "<i>test</i>"
		evidences: {}
		min_time_to_remediate:     18
		sorts:                     "NO"
		attack_vector_description: "Test attack vector."
		creation: {
			modified_by:   "unittest@fluidattacks.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2019-02-04T17:46:10+00:00"
			status:        "CREATED"
		}
		severity_score: {
			cvssf:          0.330
			base_score:     3.7
			temporal_score: 3.2
			cvss_v3:        "CVSS:3.1/AV:A/AC:H/PR:N/UI:R/S:U/C:L/I:N/A:L/E:P/RL:O/RC:R/CR:H/MAV:A/MAC:H/MPR:N/MUI:R/MS:U/MC:L/MA:L"
			threat_score:   1.2
			cvssf_v4:       0.021
			cvss_v4:        "CVSS:4.0/AV:A/AC:H/AT:N/PR:N/UI:P/VC:L/VI:N/VA:L/SC:N/SI:N/SA:N/E:P/CR:H/MAV:A/MAC:H/MPR:N/MUI:P/MVC:L/MVA:L"
		}
	},
	{
		requirements: "173. The system must discard all potentially harmful information received via data inputs."
		group_name:   "unittesting"
		unfulfilled_requirements:
		[
			"173",
		]

		description:    "The system builds LDAP queries using untrusted data that could modify the query."
		recommendation: "- Avoid using untrusted data to generate dynamic LDAP queries."
		unreliable_indicators: {
			unreliable_status:                                "DRAFT"
			unreliable_total_open_cvssf:                      0.0
			unreliable_where:                                 ""
			unreliable_newest_vulnerability_report_date:      "2019-09-13T14:58:38+00:00"
			unreliable_oldest_vulnerability_report_date:      "2020-09-12T13:45:48+00:00"
			unreliable_oldest_open_vulnerability_report_date: "2020-09-12T13:45:48+00:00"
			unreliable_zero_risk_summary: {
				confirmed: 0
				rejected:  0
				requested: 0
			}
			vulnerabilities_summary: {
				open:             0
				closed:           5
				submitted:        0
				rejected:         0
				open_critical:    0
				open_high:        0
				open_low:         0
				open_medium:      0
				open_critical_v3: 0
				open_high_v3:     0
				open_low_v3:      0
				open_medium_v3:   0
			}
		}
		title:        "107. LDAP Injection"
		cvss_version: "3.1"
		sk:           "GROUP#unittesting"
		id:           "1d901788-4de5-4879-a3c1-d4b015ac4e54"
		pk:           "FIN#1d901788-4de5-4879-a3c1-d4b015ac4e54"
		state: {
			modified_by:   "unittest@fluidattacks.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2024-11-14T10:00:00+00:00"
			status:        "CREATED"
		}
		threat: "<i>test</i>"
		evidences: {
			evidence1: {
				author_email:  ""
				modified_date: "2024-11-14T10:00:00+00:00"
				description:   "Evidencia"
				url:           "okada-unittesting-abcdef1234"
			}
		}
		min_time_to_remediate:     18
		sorts:                     "NO"
		attack_vector_description: "Test attack vector."
		creation: {
			modified_by:   "unittest@fluidattacks.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2024-11-14T10:00:00+00:00"
			status:        "CREATED"
		}
		severity_score: {
			cvssf:          0.330
			base_score:     3.7
			temporal_score: 3.2
			cvss_v3:        "CVSS:3.1/AV:A/AC:H/PR:N/UI:R/S:U/C:L/I:N/A:L/E:P/RL:O/RC:R/CR:H/MAV:A/MAC:H/MPR:N/MUI:R/MS:U/MC:L/MA:L"
			threat_score:   1.2
			cvssf_v4:       0.021
			cvss_v4:        "CVSS:4.0/AV:A/AC:H/AT:N/PR:N/UI:P/VC:L/VI:N/VA:L/SC:N/SI:N/SA:N/E:P/CR:H/MAV:A/MAC:H/MPR:N/MUI:P/MVC:L/MVA:L"
		}
	},
	{
		requirements: "262. The system must use stable, tested and up-to-date versions of third-party components."
		group_name:   "unittesting"
		unfulfilled_requirements:
		[
			"262",
		]

		description:    "The system uses the version of a software or dependency with known vulnerabilities.'"
		recommendation: "Update the affected software to the versions recommended by the vendor."
		unreliable_indicators: {
			total_open_priority:                              350
			unreliable_status:                                "VULNERABLE"
			unreliable_total_open_cvssf:                      0.0
			unreliable_where:                                 ""
			unreliable_newest_vulnerability_report_date:      "2019-09-13T14:58:38+00:00"
			unreliable_oldest_vulnerability_report_date:      "2020-09-12T13:45:48+00:00"
			unreliable_oldest_open_vulnerability_report_date: "2020-09-12T13:45:48+00:00"
			unreliable_zero_risk_summary: {
				confirmed: 0
				rejected:  0
				requested: 0
			}
			vulnerabilities_summary: {
				open:             3
				closed:           0
				submitted:        0
				rejected:         0
				open_critical:    0
				open_high:        0
				open_low:         0
				open_medium:      0
				open_critical_v3: 0
				open_high_v3:     0
				open_low_v3:      0
				open_medium_v3:   0
			}
		}
		title:        "011. Use of software with known vulnerabilities"
		cvss_version: "3.1"
		sk:           "GROUP#unittesting"
		id:           "011011012"
		pk:           "FIN#011011012"
		state: {
			modified_by:   "unittest@fluidattacks.com"
			justification: "NO_JUSTIFICATION"
			source:        "MACHINE"
			modified_date: "2019-02-07T17:46:10+00:00"
			rejection: {
				rejection_date: "2019-02-07T17:46:10+00:00"
				submitted_by:   "unittest@fluidattacks.com"
				other:          ""
				reasons: {
					SS: [
						"OMISSION",
					]
				}
				rejected_by: "customer_manager@fluidattacks.com"
			}
			status: "CREATED"
		}
		threat: "<i>test</i>"
		evidences: {}
		min_time_to_remediate:     18
		sorts:                     "NO"
		attack_vector_description: "Test attack vector."
		creation: {
			modified_by:   "unittest@fluidattacks.com"
			justification: "NO_JUSTIFICATION"
			source:        "MACHINE"
			modified_date: "2019-02-04T17:46:10+00:00"
			status:        "CREATED"
		}
		severity_score: {
			cvssf:          0.330
			base_score:     4.5
			temporal_score: 0.0
			cvss_v3:        "CVSS:3.1/AV:N/AC:H/PR:L/UI:N/S:U/C:L/I:L/A:L/E:P/RL:O/RC:C"
			cvssf_v4:       0.024
			threat_score:   1.3
			cvss_v4:        "CVSS:4.0/AV:N/AC:H/AT:N/PR:L/UI:N/VC:L/VI:L/VA:L/SC:N/SI:N/SA:N/E:P"
		}
	},
	{
		requirements: "REQ.0266. La organización debe deshabilitar las funciones inseguras de un sistema. (hardening de sistemas)"
		group_name:   "unittesting"
		unfulfilled_requirements:
		[
			"266",
		]

		description:    "Funcionalidad insegura description"
		recommendation: "-"
		unreliable_indicators: {
			total_open_priority:                              250
			unreliable_status:                                "VULNERABLE"
			unreliable_total_open_cvssf:                      1.320
			unreliable_where:                                 "192.168.1.19"
			unreliable_newest_vulnerability_report_date:      "2019-04-08T00:45:15+00:00"
			unreliable_oldest_vulnerability_report_date:      "2019-04-08T00:45:15+00:00"
			unreliable_oldest_open_vulnerability_report_date: "2019-04-08T00:45:15+00:00"
			unreliable_zero_risk_summary: {
				confirmed: 0
				rejected:  0
				requested: 0
			}
		}
		title:        "014. Insecure functionality"
		cvss_version: "3.1"
		sk:           "GROUP#unittesting"
		id:           "988493279"
		pk:           "FIN#988493279"
		state: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2019-04-08T00:45:15+00:00"
			status:        "CREATED"
		}
		threat: ""
		evidences: {
			exploitation: {
				author_email:  "integratesmanager@gmail.com"
				modified_date: "2019-04-08T00:45:15+00:00"
				description:   ""
				url:           "unittesting-988493279-exploitation.png"
			}
		}
		min_time_to_remediate:     20
		sorts:                     "NO"
		attack_vector_description: ""
		creation: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2019-04-08T00:43:18+00:00"
			status:        "CREATED"
		}
		severity_score: {
			cvssf:          24.251
			base_score:     7.3
			temporal_score: 6.3
			cvss_v3:        "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:L/A:L/E:P/RL:O/RC:R/CR:H/AR:L/MAV:L/MAC:L/MPR:L/MUI:N/MS:U"
			threat_score:   1.9
			cvssf_v4:       0.054
			cvss_v4:        "CVSS:4.0/AV:N/AC:L/AT:N/PR:N/UI:N/VC:L/VI:L/VA:L/SC:N/SI:N/SA:N/E:P/CR:H/AR:L/MAV:L/MAC:L/MPR:L/MUI:N"
		}
	},
	{
		requirements: "REQ.273. Todas las estaciones de trabajo en producción deben contar con una suite de seguridad inalterable (Anti-virus, Antispyware, Host Firewall, Host-IDS, Host-IPS)."
		group_name:   "asgard"
		unfulfilled_requirements:
		[
			"273",
		]

		description:    "Es posible conectar dispositivos USB al equipo que permiten infectarlo con software malicioso."
		recommendation: "- Establecer controles de acceso físico que impidan la conexión de dispositivos periféricos no autorizados.\n- Instalar una suite de seguridad apropiada en las estaciones de trabajo que mitigue el riesgo de infección con software malicioso."
		unreliable_indicators: {
			unreliable_status:                                "DRAFT"
			unreliable_total_open_cvssf:                      0.0
			unreliable_where:                                 ""
			unreliable_newest_vulnerability_report_date:      "2019-09-13T14:58:38+00:00"
			unreliable_oldest_vulnerability_report_date:      "2020-09-12T13:45:48+00:00"
			unreliable_oldest_open_vulnerability_report_date: "2020-09-12T13:45:48+00:00"
			unreliable_zero_risk_summary: {
				confirmed: 0
				rejected:  0
				requested: 0
			}
		}
		title:        "104. USB flash drive attacks"
		cvss_version: "3.1"
		sk:           "GROUP#asgard"
		id:           "436712859"
		pk:           "FIN#436712859"
		state: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2020-08-02T01:45:11+00:00"
			status:        "CREATED"
		}
		threat: "This is a threat"
		evidences: {}
		min_time_to_remediate:     18
		sorts:                     "NO"
		attack_vector_description: "This is an attack vector"
		creation: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2020-04-03T17:43:18+00:00"
			status:        "CREATED"
		}
		severity_score: {
			cvssf:          0.758
			base_score:     4.6
			temporal_score: 3.8
			cvss_v3:        "CVSS:3.1/AV:P/AC:L/PR:N/UI:N/S:U/C:N/I:H/A:N/E:P/RL:O/RC:U/IR:H/MAV:A/MAC:L/MPR:L/MUI:R/MS:U/MI:H"
			cvssf_v4:       0.330
			threat_score:   3.2
			cvss_v4:        "CVSS:4.0/AV:P/AC:L/AT:N/PR:N/UI:N/VC:N/VI:H/VA:N/SC:N/SI:N/SA:N/MAV:A/MAC:L/MPR:L/MUI:A/MVI:H/MSC:N/MSI:N/MSA:N/IR:H/E:P"
		}
	},
	{
		requirements: "REQ.0229. El sistema debe solicitar a cualquier actor que intenta autenticarse, como mínimo un nombre de usuario y una contraseña."
		group_name:   "oneshottest"
		unfulfilled_requirements:
		[
			"229",
			"231",
			"264",
			"319",
			"328",
		]

		description:    "Los servicios críticos sobre la red interna como acceso a bases de datos, recursos compartidos con información sensible y/o web services, no cuentan con un doble factor de autenticación lo cual hace más fácil para un atacante con un usuario comprometido acceder a estos."
		recommendation: "Implementar un doble factor de autenticación ya sea por software o hardware para incrementar el nivel de protección de la autenticación de los recursos críticos."
		unreliable_indicators: {
			unreliable_status:                                "VULNERABLE"
			unreliable_total_open_cvssf:                      0.435
			unreliable_where:                                 "path/to/file4.ext"
			unreliable_newest_vulnerability_report_date:      "2019-04-12T13:45:48+00:00"
			unreliable_oldest_vulnerability_report_date:      "2019-04-12T13:45:48+00:00"
			unreliable_oldest_open_vulnerability_report_date: "2019-04-12T13:45:48+00:00"
			unreliable_zero_risk_summary: {
				confirmed: 1
				rejected:  0
				requested: 0
			}
		}
		title:        "081. Lack of multi-factor authentication"
		cvss_version: "3.1"
		sk:           "GROUP#oneshottest"
		id:           "475041513"
		pk:           "FIN#475041513"
		state: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2018-04-08T00:45:11+00:00"
			status:        "CREATED"
		}
		threat: "Los servicios críticos sobre la red interna como acceso a bases de datos, recursos compartidos con información sensible y/o web services, no cuentan con un doble factor de autenticación lo cual hace más fácil para un atacante con un usuario comprometido acceder a estos."
		evidences: {
			evidence1: {
				author_email:  ""
				modified_date: "2018-04-08T00:43:18+00:00"
				description:   "Evidencia"
				url:           "continuoustesting-475041513-evidence_route_1"
			}
			evidence2: {
				author_email:  ""
				modified_date: "2018-04-08T00:43:18+00:00"
				description:   "AAAAAAAAAAAA"
				url:           "continuoustesting-475041513-evidence_route_2"
			}
		}
		min_time_to_remediate:     18
		sorts:                     "NO"
		attack_vector_description: "Los servicios críticos sobre la red interna como acceso a bases de datos, recursos compartidos con información sensible y/o web services, no cuentan con un doble factor de autenticación lo cual hace más fácil para un atacante con un usuario comprometido acceder a estos"
		creation: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2018-04-08T00:43:18+00:00"
			status:        "CREATED"
		}
		severity_score: {
			cvssf:          0.435
			base_score:     3.8
			temporal_score: 3.4
			cvss_v3:        "CVSS:3.1/AV:P/AC:H/PR:L/UI:N/S:U/C:L/I:L/A:L/E:P/RL:O/CR:L/AR:L/MAV:N/MAC:H/MPR:H/MUI:R/MS:U/MC:L/MA:L"
			threat_score:   1.1
			cvssf_v4:       0.018
			cvss_v4:        "CVSS:4.0/AV:P/AC:H/AT:N/PR:L/UI:N/VC:L/VI:L/VA:L/SC:N/SI:N/SA:N/E:P/AR:L/MAV:N/MAC:H/MPR:H/MUI:P/MVC:L/MVA:L"
		}
	},
	{
		requirements: "R359. Avoid using generic exceptions."
		group_name:   "unittesting"
		unfulfilled_requirements:
		[
			"266",
		]

		description:    "The source code uses generic exceptions to handle unexpected errors. Catching generic exceptions obscures the problem that caused the error and promotes a generic way to handle different categories or sources of error. This may cause security vulnerabilities to materialize, as some special flows go unnoticed."
		recommendation: "Implement password politicies with the best practices for strong passwords."
		unreliable_indicators: {
			total_open_priority:                              250
			unreliable_status:                                "VULNERABLE"
			unreliable_total_open_cvssf:                      3.482
			unreliable_where:                                 "test/data/lib_path/f060/csharp.cs"
			unreliable_newest_vulnerability_report_date:      "2020-01-03T17:46:10+00:00"
			unreliable_oldest_vulnerability_report_date:      "2020-01-03T17:46:10+00:00"
			unreliable_oldest_open_vulnerability_report_date: "2020-01-03T17:46:10+00:00"
			unreliable_zero_risk_summary: {
				confirmed: 1
				rejected:  0
				requested: 0
			}
			vulnerabilities_summary: {
				open:             0
				closed:           1
				submitted:        1
				rejected:         1
				open_critical:    0
				open_high:        0
				open_low:         0
				open_medium:      0
				open_critical_v3: 0
				open_high_v3:     0
				open_low_v3:      0
				open_medium_v3:   0
			}
		}
		title:        "060. Insecure service configuration - Host verification"
		cvss_version: "3.1"
		sk:           "GROUP#unittesting"
		id:           "422286126"
		pk:           "FIN#422286126"
		state: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2018-07-09T05:00:00+00:00"
			status:        "CREATED"
		}
		threat: "A attack can get passwords  of users and He can impersonate them or used the credentials for practices maliciosus."
		evidences: {
			exploitation: {
				author_email:  "integratesmanager@gmail.com"
				modified_date: "2018-07-09T05:00:00+00:00"
				description:   ""
				url:           "unittesting-422286126-exploitation.png"
			}
			records: {
				author_email:  "integratesmanager@gmail.com"
				modified_date: "2018-07-09T05:00:00+00:00"
				description:   ""
				url:           "unittesting-422286126-evidence_file.csv"
			}
			evidence1: {
				author_email:  "integratesmanager@gmail.com"
				modified_date: "2018-07-09T05:00:00+00:00"
				description:   "this is a test description"
				url:           "unittesting-422286126-evidence_route_1.png"
			}
			evidence2: {
				author_email:  "integratesmanager@gmail.com"
				modified_date: "2018-07-09T05:00:00+00:00"
				description:   "exception"
				url:           "unittesting-422286126-evidence_route_2.jpg"
			}
			evidence3: {
				author_email:  "integratesmanager@gmail.com"
				modified_date: "2018-07-09T05:00:00+00:00"
				description:   "update testing"
				url:           "unittesting-422286126-evidence_route_3.png"
			}
			evidence4: {
				author_email:  "integratesmanager@gmail.com"
				modified_date: "2018-07-09T05:00:00+00:00"
				description:   "changed for testing purposesese"
				url:           "unittesting-422286126-evidence_route_4.png"
			}
			evidence5: {
				author_email:  "integratesmanager@gmail.com"
				modified_date: "2018-07-09T05:00:00+00:00"
				description:   "yes, it does."
				url:           "unittesting-422286126-evidence_route_5.png"
			}
			animation: {
				author_email:  "integratesmanager@gmail.com"
				modified_date: "2018-07-09T05:00:00+00:00"
				description:   ""
				url:           "unittesting-422286126-animation.webm"
			}
		}
		min_time_to_remediate:     18
		sorts:                     "NO"
		attack_vector_description: "This is an attack vector"
		creation: {
			modified_by:   "integratesmanager@gmail.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2018-04-08T00:43:18+00:00"
			status:        "CREATED"
		}
		severity_score: {
			cvssf:          0.218
			base_score:     3.5
			temporal_score: 2.9
			cvss_v3:        "CVSS:3.1/AV:A/AC:L/PR:L/UI:N/S:U/C:N/I:L/A:N/E:U/RL:W/RC:U/MAV:A/MAC:L/MPR:L/MUI:N/MS:U/MI:L"
			threat_score:   1.2
			cvssf_v4:       0.021
			cvss_v4:        "CVSS:4.0/AV:A/AC:L/AT:N/PR:L/UI:N/VC:N/VI:L/VA:N/SC:N/SI:N/SA:N/E:U/MAV:A/MAC:L/MPR:L/MUI:N/MVI:L"
		}
	},
	{
		sk:           "GROUP#groudon"
		pk:           "FIN#196c7083-1a6f-4120-99e9-3ab0b1696c7d"
		id:           "196c7083-1a6f-4120-99e9-3ab0b1696c7d"
		title:        "037. Technical information leak"
		group_name:   "groudon"
		requirements: "REQ.0077. La aplicación no debe revelar detalles del sistema interno como stack traces, fragmentos de sentencias SQL y nombres de base de datos o tablas.\r\nREQ.0176. El sistema debe restringir el acceso a objetos del sistema que tengan contenido sensible. Sólo permitirá su acceso a usuarios autorizados."
		unfulfilled_requirements:
		[
			"077",
			"176",
		]
		description:    "Descripción de fuga de información técnica"
		recommendation: "Eliminar el banner de los servicios con fuga de información, Verificar que los encabezados HTTP no expongan ningún nombre o versión."
		cvss_version:   "3.1"
		state: {
			modified_by:   "hacker@fluidattacks.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2024-09-19T11:00:00+00:00"
			status:        "CREATED"
		}
		unreliable_indicators: {
			unreliable_status: "VULNERABLE"
			unreliable_where:  "test/data/lib_path/f060/csharp.cs"
			unreliable_zero_risk_summary: {
				confirmed: 0
				rejected:  0
				requested: 0
			}
		}
		threat: "Amenaza."
		evidences: {
			evidence1: {
				author_email:  ""
				modified_date: "2024-09-19T11:00:00+00:00"
				description:   "Evidencia 1"
				url:           "oneshottest-457497318-evidence_route_1"
			}
			evidence2: {
				author_email:  ""
				modified_date: "2024-09-19T11:00:00+00:00"
				description:   "Evidencia 2"
				url:           "oneshottest-457497318-evidence_route_2"
			}
		}
		min_time_to_remediate:     18
		sorts:                     "NO"
		attack_vector_description: "Descripción"
		creation: {
			modified_by:   "hacker@fluidattacks.com"
			justification: "NO_JUSTIFICATION"
			source:        "ASM"
			modified_date: "2024-09-19T11:00:00+00:00"
			status:        "CREATED"
		}
		severity_score: {
			cvssf:          0.871
			base_score:     4.6
			temporal_score: 3.9
			cvss_v3:        "CVSS:3.1/AV:A/AC:H/PR:L/UI:N/S:U/C:L/I:L/A:L/E:P/RL:T/RC:U/MAV:A/MAC:H/MPR:L/MUI:N/MS:U/MC:L/MI:L/MA:L"
			threat_score:   1.2
			cvssf_v4:       0.021
			cvss_v4:        "CVSS:4.0/AV:A/AC:H/AT:N/PR:L/UI:N/VC:L/VI:L/VA:L/SC:N/SI:N/SA:N/E:P/MAV:A/MAC:H/MPR:L/MUI:N/MVC:L/MVI:L/MVA:L"
		}
	},
] & [...#FindingItem]
