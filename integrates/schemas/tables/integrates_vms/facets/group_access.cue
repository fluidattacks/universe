package group_access

import (
	"fluidattacks.com/types/group_access"
)

#groupAccessPk:   =~"^USER#[a-zA-Z0-9._+-@]+$"
#groupAccessSk:   =~"^GROUP#[a-z]+$"
#groupAccessPk_2: =~"^DOMAIN#([a-zA-Z0-9.-]+.[a-zA-Z]{2,}|__adminDomain__)$"
#groupAccessSk_2: =~"^USER#[a-zA-Z0-9._+-@]+#GROUP#[a-z]+$"

#GroupAccessKeys: {
	pk:   string & #groupAccessPk
	sk:   string & #groupAccessSk
	pk_2: string & #groupAccessPk_2
	sk_2: string & #groupAccessSk_2
}

#GroupAccessItem: {
	#GroupAccessKeys
	group_access.#GroupAccess
}

GroupAccess:
{
	FacetName: "group_access"
	KeyAttributeAlias: {
		PartitionKeyAlias: "USER#email"
		SortKeyAlias:      "GROUP#name"
	}
	NonKeyAttributes: [
		"domain",
		"email",
		"group_name",
		"pk_2",
		"sk_2",
		"state",
		"expiration_time",
	]
	DataAccess: {
		MySql: {}
	}
}

GroupAccess: TableData: [
	{
		sk:   "GROUP#asgard"
		pk:   "USER#integratesuser@gmail.com"
		sk_2: "USER#integratesuser@gmail.com#GROUP#asgard"
		pk_2: "DOMAIN#gmail.com"
		state: {
			role:           "group_manager"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test"
		}
		group_name: "asgard"
		email:      "integratesuser@gmail.com"
		domain:     "gmail.com"
	},
	{
		sk:   "GROUP#barranquilla"
		pk:   "USER#integratesuser@gmail.com"
		sk_2: "USER#integratesuser@gmail.com#GROUP#barranquilla"
		pk_2: "DOMAIN#gmail.com"
		state: {
			role:           "group_manager"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test"
		}
		group_name: "barranquilla"
		email:      "integratesuser@gmail.com"
		domain:     "gmail.com"
	},
	{
		sk:   "GROUP#gotham"
		pk:   "USER#integratesuser@gmail.com"
		sk_2: "USER#integratesuser@gmail.com#GROUP#gotham"
		pk_2: "DOMAIN#gmail.com"
		state: {
			role:           "group_manager"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test"
		}
		group_name: "gotham"
		email:      "integratesuser@gmail.com"
		domain:     "gmail.com"
	},
	{
		sk:   "GROUP#metropolis"
		pk:   "USER#integratesuser@gmail.com"
		sk_2: "USER#integratesuser@gmail.com#GROUP#metropolis"
		pk_2: "DOMAIN#gmail.com"
		state: {
			role:           "group_manager"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test"
		}
		group_name: "metropolis"
		email:      "integratesuser@gmail.com"
		domain:     "gmail.com"
	},
	{
		sk:   "GROUP#monteria"
		pk:   "USER#integratesuser@gmail.com"
		sk_2: "USER#integratesuser@gmail.com#GROUP#monteria"
		pk_2: "DOMAIN#gmail.com"
		state: {
			role:           "group_manager"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test"
		}
		group_name: "monteria"
		email:      "integratesuser@gmail.com"
		domain:     "gmail.com"
	},
	{
		sk:   "GROUP#oneshottest"
		pk:   "USER#integratesuser@gmail.com"
		sk_2: "USER#integratesuser@gmail.com#GROUP#oneshottest"
		pk_2: "DOMAIN#gmail.com"
		state: {
			role:           "user"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test"
		}
		group_name: "oneshottest"
		email:      "integratesuser@gmail.com"
		domain:     "gmail.com"
	},
	{
		sk:   "GROUP#unittesting"
		pk:   "USER#integratesuser@gmail.com"
		sk_2: "USER#integratesuser@gmail.com#GROUP#unittesting"
		pk_2: "DOMAIN#gmail.com"
		state: {
			role:           "group_manager"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test"
		}
		group_name: "unittesting"
		email:      "integratesuser@gmail.com"
		domain:     "gmail.com"
	},
	{
		sk:   "GROUP#unittesting"
		pk:   "USER#forces.unittesting@fluidattacks.com"
		sk_2: "USER#forces.unittesting@fluidattacks.com#GROUP#unittesting"
		pk_2: "DOMAIN#fluidattacks.com"
		state: {
			role:           "service_forces"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Forces service user"
		}
		group_name: "unittesting"
		email:      "forces.unittesting@fluidattacks.com"
		domain:     "fluidattacks.com"
	},
	{
		sk:   "GROUP#unittesting"
		pk:   "USER#integratesuser2@gmail.com"
		sk_2: "USER#integratesuser2@gmail.com#GROUP#unittesting"
		pk_2: "DOMAIN#gmail.com"
		state: {
			role:           "user"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test"
		}
		group_name: "unittesting"
		email:      "integratesuser2@gmail.com"
		domain:     "gmail.com"
	},
	{
		sk:   "GROUP#asgard"
		pk:   "USER#integratesmanager@fluidattacks.com"
		sk_2: "USER#integratesmanager@fluidattacks.com#GROUP#asgard"
		pk_2: "DOMAIN#fluidattacks.com"
		state: {
			role:           "admin"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test"
		}
		group_name: "asgard"
		email:      "integratesmanager@fluidattacks.com"
		domain:     "fluidattacks.com"
	},
	{
		sk:   "GROUP#barranquilla"
		pk:   "USER#integratesmanager@fluidattacks.com"
		sk_2: "USER#integratesmanager@fluidattacks.com#GROUP#barranquilla"
		pk_2: "DOMAIN#fluidattacks.com"
		state: {
			role:           "admin"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test"
		}
		group_name: "barranquilla"
		email:      "integratesmanager@fluidattacks.com"
		domain:     "fluidattacks.com"
	},
	{
		sk:   "GROUP#gotham"
		pk:   "USER#integratesmanager@fluidattacks.com"
		sk_2: "USER#integratesmanager@fluidattacks.com#GROUP#gotham"
		pk_2: "DOMAIN#fluidattacks.com"
		state: {
			role:           "admin"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test"
		}
		group_name: "gotham"
		email:      "integratesmanager@fluidattacks.com"
		domain:     "fluidattacks.com"
	},
	{
		sk:   "GROUP#metropolis"
		pk:   "USER#integratesmanager@fluidattacks.com"
		sk_2: "USER#integratesmanager@fluidattacks.com#GROUP#metropolis"
		pk_2: "DOMAIN#fluidattacks.com"
		state: {
			role:           "admin"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test"
		}
		group_name: "metropolis"
		email:      "integratesmanager@fluidattacks.com"
		domain:     "fluidattacks.com"
	},
	{
		sk:   "GROUP#monteria"
		pk:   "USER#integratesmanager@fluidattacks.com"
		sk_2: "USER#integratesmanager@fluidattacks.com#GROUP#monteria"
		pk_2: "DOMAIN#fluidattacks.com"
		state: {
			role:           "admin"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test"
		}
		group_name: "monteria"
		email:      "integratesmanager@fluidattacks.com"
		domain:     "fluidattacks.com"
	},
	{
		sk:   "GROUP#oneshottest"
		pk:   "USER#integratesmanager@fluidattacks.com"
		sk_2: "USER#integratesmanager@fluidattacks.com#GROUP#oneshottest"
		pk_2: "DOMAIN#fluidattacks.com"
		state: {
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test"
			has_access:     true
		}
		group_name: "oneshottest"
		email:      "integratesmanager@fluidattacks.com"
		domain:     "fluidattacks.com"
	},
	{
		sk:   "GROUP#unittesting"
		pk:   "USER#integratesmanager@fluidattacks.com"
		sk_2: "USER#integratesmanager@fluidattacks.com#GROUP#unittesting"
		pk_2: "DOMAIN#fluidattacks.com"
		state: {
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test"
			has_access:     true
		}
		group_name: "unittesting"
		email:      "integratesmanager@fluidattacks.com"
		domain:     "fluidattacks.com"
	},
	{
		sk:   "GROUP#unittesting"
		pk:   "USER#integratesreattacker@fluidattacks.com"
		sk_2: "USER#integratesreattacker@fluidattacks.com#GROUP#unittesting"
		pk_2: "DOMAIN#fluidattacks.com"
		state: {
			role:           "reattacker"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test reattacker"
		}
		group_name: "unittesting"
		email:      "integratesreattacker@fluidattacks.com"
		domain:     "fluidattacks.com"
	},
	{
		sk:   "GROUP#unittesting"
		pk:   "USER#vulnmanager@gmail.com"
		sk_2: "USER#vulnmanager@gmail.com#GROUP#unittesting"
		pk_2: "DOMAIN#gmail.com"
		state: {
			role:           "vulnerability_manager"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test vulnerability manager"
			invitation: {
				url_token:      "unknown"
				role:           "vulnerability_manager"
				responsibility: "Test vulnerability manager"
				is_used:        true
			}
		}
		group_name: "unittesting"
		email:      "vulnmanager@gmail.com"
		domain:     "gmail.com"
	},
	{
		sk:   "GROUP#asgard"
		pk:   "USER#integratesmanager@gmail.com"
		sk_2: "USER#integratesmanager@gmail.com#GROUP#asgard"
		pk_2: "DOMAIN#gmail.com"
		state: {
			role:           "admin"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test"
		}
		group_name: "asgard"
		email:      "integratesmanager@gmail.com"
		domain:     "gmail.com"
	},
	{
		sk:   "GROUP#barranquilla"
		pk:   "USER#integratesmanager@gmail.com"
		sk_2: "USER#integratesmanager@gmail.com#GROUP#barranquilla"
		pk_2: "DOMAIN#gmail.com"
		state: {
			role:           "admin"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test"
		}
		group_name: "barranquilla"
		email:      "integratesmanager@gmail.com"
		domain:     "gmail.com"
	},
	{
		sk:   "GROUP#gotham"
		pk:   "USER#integratesmanager@gmail.com"
		sk_2: "USER#integratesmanager@gmail.com#GROUP#gotham"
		pk_2: "DOMAIN#gmail.com"
		state: {
			role:           "admin"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test"
		}
		group_name: "gotham"
		email:      "integratesmanager@gmail.com"
		domain:     "gmail.com"
	},
	{
		sk:   "GROUP#metropolis"
		pk:   "USER#integratesmanager@gmail.com"
		sk_2: "USER#integratesmanager@gmail.com#GROUP#metropolis"
		pk_2: "DOMAIN#gmail.com"
		state: {
			role:           "admin"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test"
		}
		group_name: "metropolis"
		email:      "integratesmanager@gmail.com"
		domain:     "gmail.com"
	},
	{
		sk:   "GROUP#monteria"
		pk:   "USER#integratesmanager@gmail.com"
		sk_2: "USER#integratesmanager@gmail.com#GROUP#monteria"
		pk_2: "DOMAIN#gmail.com"
		state: {
			role:           "admin"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test"
		}
		group_name: "monteria"
		email:      "integratesmanager@gmail.com"
		domain:     "gmail.com"
	},
	{
		sk:   "GROUP#oneshottest"
		pk:   "USER#integratesmanager@gmail.com"
		sk_2: "USER#integratesmanager@gmail.com#GROUP#oneshottest"
		pk_2: "DOMAIN#gmail.com"
		state: {
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test"
			has_access:     true
		}
		group_name: "oneshottest"
		email:      "integratesmanager@gmail.com"
		domain:     "gmail.com"
	},
	{
		sk:   "GROUP#unittesting"
		pk:   "USER#integratesmanager@gmail.com"
		sk_2: "USER#integratesmanager@gmail.com#GROUP#unittesting"
		pk_2: "DOMAIN#gmail.com"
		state: {
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test"
			has_access:     true
		}
		group_name: "unittesting"
		email:      "integratesmanager@gmail.com"
		domain:     "gmail.com"
	},
	{
		sk:   "GROUP#unittesting"
		pk:   "USER#integratesreviewer@fluidattacks.com"
		sk_2: "USER#integratesreviewer@fluidattacks.com#GROUP#unittesting"
		pk_2: "DOMAIN#fluidattacks.com"
		state: {
			role:           "reviewer"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test"
		}
		group_name: "unittesting"
		email:      "integratesreviewer@fluidattacks.com"
		domain:     "fluidattacks.com"
	},
	{
		sk:   "GROUP#asgard"
		pk:   "USER#continuoushacking@gmail.com"
		sk_2: "USER#continuoushacking@gmail.com#GROUP#asgard"
		pk_2: "DOMAIN#gmail.com"
		state: {
			role:           "admin"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test"
		}
		group_name: "asgard"
		email:      "continuoushacking@gmail.com"
		domain:     "gmail.com"
	},
	{
		sk:   "GROUP#barranquilla"
		pk:   "USER#continuoushacking@gmail.com"
		sk_2: "USER#continuoushacking@gmail.com#GROUP#barranquilla"
		pk_2: "DOMAIN#gmail.com"
		state: {
			role:           "admin"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test"
		}
		group_name: "barranquilla"
		email:      "continuoushacking@gmail.com"
	},
	{
		sk:   "GROUP#gotham"
		pk:   "USER#continuoushacking@gmail.com"
		sk_2: "USER#forces.unittesting@fluidattacks.com#GROUP#unittesting"
		pk_2: "DOMAIN#fluidattacks.com"
		state: {
			role:           "admin"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test"
		}
		group_name: "gotham"
		email:      "continuoushacking@gmail.com"
		domain:     "gmail.com"
	},
	{
		sk:   "GROUP#metropolis"
		pk:   "USER#continuoushacking@gmail.com"
		sk_2: "USER#continuoushacking@gmail.com#GROUP#metropolis"
		pk_2: "DOMAIN#gmail.com"
		state: {
			role:           "admin"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test"
		}
		group_name: "metropolis"
		email:      "continuoushacking@gmail.com"
		domain:     "gmail.com"
	},
	{
		sk:   "GROUP#monteria"
		pk:   "USER#continuoushacking@gmail.com"
		sk_2: "USER#continuoushacking@gmail.com#GROUP#monteria"
		pk_2: "DOMAIN#gmail.com"
		state: {
			role:           "admin"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test"
		}
		group_name: "monteria"
		email:      "continuoushacking@gmail.com"
		domain:     "gmail.com"
	},
	{
		sk:   "GROUP#oneshottest"
		pk:   "USER#continuoushacking@gmail.com"
		sk_2: "USER#continuoushacking@gmail.com#GROUP#oneshottest"
		pk_2: "DOMAIN#gmail.com"
		state: {
			role:           "group_manager"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test"
		}
		group_name: "oneshottest"
		email:      "continuoushacking@gmail.com"
		domain:     "gmail.com"
	},
	{
		sk:   "GROUP#unittesting"
		pk:   "USER#continuoushacking@gmail.com"
		sk_2: "USER#continuoushacking@gmail.com#GROUP#unittesting"
		pk_2: "DOMAIN#gmail.com"
		state: {
			role:           "group_manager"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test"
		}
		group_name: "unittesting"
		email:      "continuoushacking@gmail.com"
		domain:     "gmail.com"
	},
	{
		sk:   "GROUP#unittesting"
		pk:   "USER#customer_manager@fluidattacks.com"
		sk_2: "USER#customer_manager@fluidattacks.com#GROUP#unittesting"
		pk_2: "DOMAIN#fluidattacks.com"
		state: {
			role:           "customer_manager"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test Owner"
		}
		group_name: "unittesting"
		email:      "customer_manager@fluidattacks.com"
		domain:     "fluidattacks.com"
	},
	{
		sk:   "GROUP#unittesting"
		pk:   "USER#integratesserviceforces@fluidattacks.com"
		sk_2: "USER#integratesserviceforces@fluidattacks.com#GROUP#unittesting"
		pk_2: "DOMAIN#fluidattacks.com"
		state: {
			role:           "service_forces"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test"
		}
		group_name: "unittesting"
		email:      "integratesserviceforces@fluidattacks.com"
		domain:     "fluidattacks.com"
	},
	{
		sk:   "GROUP#unittesting"
		pk:   "USER#integratesresourcer@fluidattacks.com"
		sk_2: "USER#integratesresourcer@fluidattacks.com#GROUP#unittesting"
		pk_2: "DOMAIN#fluidattacks.com"
		state: {
			role:           "resourcer"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test"
		}
		group_name: "unittesting"
		email:      "integratesresourcer@fluidattacks.com"
		domain:     "fluidattacks.com"
	},
	{
		sk:   "GROUP#unittesting"
		pk:   "USER#unittest2@fluidattacks.com"
		sk_2: "USER#unittest2@fluidattacks.com#GROUP#unittesting"
		pk_2: "DOMAIN#fluidattacks.com"
		state: {
			role:           "customer_manager"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Tester"
		}
		group_name: "unittesting"
		email:      "unittest2@fluidattacks.com"
		domain:     "fluidattacks.com"
	},
	{
		sk:   "GROUP#deleteimamura"
		pk:   "USER#active_imamura3@fluidattacks.com"
		sk_2: "USER#active_imamura3@fluidattacks.com#GROUP#deleteimamura"
		pk_2: "DOMAIN#fluidattacks.com"
		state: {
			role:           "user"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Not to be deleted by inactivity"
			invitation: {
				url_token:      "unknown"
				role:           "user"
				responsibility: "Not to be deleted by inactivity"
				is_used:        true
			}
		}
		group_name: "deleteimamura"
		email:      "active_imamura3@fluidattacks.com"
		domain:     "fluidattacks.com"
	},
	{
		sk:   "GROUP#deleteimamura"
		pk:   "USER#inactive_imamura1@fluidattacks.com"
		sk_2: "USER#inactive_imamura1@fluidattacks.com#GROUP#deleteimamura"
		pk_2: "DOMAIN#fluidattacks.com"
		state: {
			role:           "hacker"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "to be deleted by inactivity, first attempt"
			invitation: {
				url_token:      "unknown"
				role:           "hacker"
				responsibility: "to be deleted by inactivity, first attempt"
				is_used:        true
			}
		}
		group_name: "deleteimamura"
		email:      "inactive_imamura1@fluidattacks.com"
		domain:     "fluidattacks.com"
	},
	{
		sk:   "GROUP#unittesting"
		pk:   "USER#integratesuser2@fluidattacks.com"
		sk_2: "USER#integratesuser2@fluidattacks.com#GROUP#unittesting"
		pk_2: "DOMAIN#fluidattacks.com"
		state: {
			role:           "user"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test"
		}
		group_name: "unittesting"
		email:      "integratesuser2@fluidattacks.com"
		domain:     "fluidattacks.com"
	},
	{
		sk:   "GROUP#continuoustesting"
		pk:   "USER#integrateshacker@fluidattacks.com"
		sk_2: "USER#integrateshacker@fluidattacks.com#GROUP#continuoustesting"
		pk_2: "DOMAIN#fluidattacks.com"
		state: {
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Continuous Testing user"
			has_access:     true
		}
		group_name: "continuoustesting"
		email:      "integrateshacker@fluidattacks.com"
		domain:     "fluidattacks.com"
	},
	{
		sk:   "GROUP#oneshottest"
		pk:   "USER#integrateshacker@fluidattacks.com"
		sk_2: "USER#integrateshacker@fluidattacks.com#GROUP#oneshottest"
		pk_2: "DOMAIN#fluidattacks.com"
		state: {
			role:           "reattacker"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test"
		}
		group_name: "oneshottest"
		email:      "integrateshacker@fluidattacks.com"
		domain:     "fluidattacks.com"
	},
	{
		sk:   "GROUP#unittesting"
		pk:   "USER#integrateshacker@fluidattacks.com"
		sk_2: "USER#integrateshacker@fluidattacks.com#GROUP#unittesting"
		pk_2: "DOMAIN#fluidattacks.com"
		state: {
			role:           "hacker"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test"
		}
		group_name: "unittesting"
		email:      "integrateshacker@fluidattacks.com"
		domain:     "fluidattacks.com"
	},
	{
		sk:   "GROUP#unittesting"
		pk:   "USER#continuoushack2@gmail.com"
		sk_2: "USER#continuoushack2@gmail.com#GROUP#unittesting"
		pk_2: "DOMAIN#gmail.com"
		state: {
			role:           "group_manager"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test"
		}
		group_name: "unittesting"
		email:      "continuoushack2@gmail.com"
		domain:     "gmail.com"
	},
	{
		sk:   "GROUP#deleteimamura"
		pk:   "USER#inactive_imamura2@fluidattacks.com"
		sk_2: "USER#inactive_imamura2@fluidattacks.com#GROUP#deleteimamura"
		pk_2: "DOMAIN#fluidattacks.com"
		state: {
			role:           "hacker"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "to be deleted by inactivity, second attempt"
			invitation: {
				url_token:      "unknown"
				role:           "hacker"
				responsibility: "to be deleted by inactivity, second attempt"
				is_used:        true
			}
		}
		group_name: "deleteimamura"
		email:      "inactive_imamura2@fluidattacks.com"
		domain:     "fluidattacks.com"
	},
	{
		sk:   "GROUP#asgard"
		pk:   "USER#__adminEmail__"
		sk_2: "USER#__adminEmail__#GROUP#asgard"
		pk_2: "DOMAIN#__adminDomain__"
		state: {
			role:           "admin"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test"
		}
		group_name: "asgard"
		email:      "__adminEmail__"
		domain:     "__adminDomain__"
	},
	{
		sk:   "GROUP#barranquilla"
		pk:   "USER#__adminEmail__"
		sk_2: "USER#__adminEmail__#GROUP#barranquilla"
		pk_2: "DOMAIN#__adminDomain__"
		state: {
			role:           "admin"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test"
		}
		group_name: "barranquilla"
		email:      "__adminEmail__"
		domain:     "__adminDomain__"
	},
	{
		sk:   "GROUP#gotham"
		pk:   "USER#__adminEmail__"
		sk_2: "USER#__adminEmail__#GROUP#gotham"
		pk_2: "DOMAIN#__adminDomain__"
		state: {
			role:           "admin"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test"
		}
		group_name: "gotham"
		email:      "__adminEmail__"
		domain:     "__adminDomain__"
	},
	{
		sk:   "GROUP#metropolis"
		pk:   "USER#__adminEmail__"
		sk_2: "USER#__adminEmail__#GROUP#metropolis"
		pk_2: "DOMAIN#__adminDomain__"
		state: {
			role:           "admin"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test"
		}
		group_name: "metropolis"
		email:      "__adminEmail__"
		domain:     "__adminDomain__"
	},
	{
		sk:   "GROUP#monteria"
		pk:   "USER#__adminEmail__"
		sk_2: "USER#__adminEmail__#GROUP#monteria"
		pk_2: "DOMAIN#__adminDomain__"
		state: {
			role:           "admin"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test"
		}
		group_name: "monteria"
		email:      "__adminEmail__"
		domain:     "__adminDomain__"
	},
	{
		sk:   "GROUP#oneshottest"
		pk:   "USER#__adminEmail__"
		sk_2: "USER#__adminEmail__#GROUP#oneshottest"
		pk_2: "DOMAIN#__adminDomain__"
		state: {
			role:           "admin"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test"
		}
		group_name: "oneshottest"
		email:      "__adminEmail__"
		domain:     "__adminDomain__"
	},
	{
		sk:   "GROUP#unittesting"
		pk:   "USER#__adminEmail__"
		sk_2: "USER#__adminEmail__#GROUP#unittesting"
		pk_2: "DOMAIN#__adminDomain__"
		state: {
			role:           "admin"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Test"
		}
		group_name: "unittesting"
		email:      "__adminEmail__"
	},
	{
		sk:   "GROUP#sheele"
		pk:   "USER#org_testuser3@gmail.com"
		sk_2: "USER#org_testuser3@gmail.com#GROUP#sheele"
		pk_2: "DOMAIN#gmail.com"
		state: {
			role:           "user"
			has_access:     true
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: ""
		}
		group_name: "sheele"
		email:      "org_testuser3@gmail.com"
		domain:     "gmail.com"
	},
	{
		sk:   "GROUP#unittesting"
		pk:   "USER#unittest@fluidattacks.com"
		sk_2: "USER#unittest@fluidattacks.com#GROUP#unittesting"
		pk_2: "DOMAIN#fluidattacks.com"
		state: {
			modified_date:  "2020-01-01T20:07:57+00:00"
			responsibility: "Tester"
			has_access:     true
		}
		group_name: "unittesting"
		email:      "unittest@fluidattacks.com"
		domain:     "fluidattacks.com"
	},
	{
		sk:   "GROUP#malenia"
		pk:   "USER#integratesmanager@gmail.com"
		sk_2: "USER#integratesmanager@gmail.com#GROUP#malenia"
		pk_2: "DOMAIN#gmail.com"
		state: {
			modified_by:   "integratesmanager@gmail.com"
			has_access:    true
			role:          "customer_manager"
			modified_date: "2024-07-16T20:27:24.745728+00:00"
		}
		group_name: "malenia"
		email:      "integratesmanager@gmail.com"
		domain:     "gmail.com"
	},
	{
		sk:   "GROUP#miquella"
		pk:   "USER#integratesmanager@gmail.com"
		sk_2: "USER#integratesmanager@gmail.com#GROUP#miquella"
		pk_2: "DOMAIN#fluidattacks.com"
		state: {
			modified_by:   "integratesmanager@gmail.com"
			has_access:    true
			role:          "customer_manager"
			modified_date: "2024-07-16T20:27:47.851993+00:00"
		}
		group_name: "miquella"
		email:      "integratesmanager@gmail.com"
		domain:     "gmail.com"
	},
	{
		sk:   "GROUP#groudon"
		pk:   "USER#__adminEmail__"
		sk_2: "USER#__adminEmail__#GROUP#groudon"
		pk_2: "DOMAIN#__adminDomain__"
		state: {
			role:           "group_manager"
			has_access:     true
			modified_date:  "2024-09-19T09:10:00+00:00"
			responsibility: "Platform Manager"
		}
		group_name: "groudon"
		email:      "__adminEmail__"
	},
	{
		sk:   "GROUP#kyogre"
		pk:   "USER#__adminEmail__"
		sk_2: "USER#__adminEmail__#GROUP#kyogre"
		pk_2: "DOMAIN#__adminDomain__"
		state: {
			role:           "group_manager"
			has_access:     true
			modified_date:  "2024-09-19T09:10:00+00:00"
			responsibility: "Platform Manager"
		}
		group_name: "kyogre"
		email:      "__adminEmail__"
	},
] & [...#GroupAccessItem]
