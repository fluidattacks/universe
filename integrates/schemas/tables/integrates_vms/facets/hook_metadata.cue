package hook_metadata

import (
	"fluidattacks.com/types/hook"
)

#hookMetadataPk: =~"^HOOK#[a-z0-9-]+"
#hookMetadataSk: =~"^GROUP#[a-z]+"

#HookMetadataKeys: {
	pk: string & #hookMetadataPk
	sk: string & #hookMetadataSk
}

#HookItem: {
	#HookMetadataKeys
	hook.#HookMetadata
}

HookMetadata:
{
	FacetName: "hook_metadata"
	KeyAttributeAlias: {
		PartitionKeyAlias: "HOOK#id"
		SortKeyAlias:      "GROUP#group_name"
	}
	NonKeyAttributes: [
		"name",
		"entry_point",
		"id",
		"group_name",
		"state",
		"token",
		"hook_events",
		"token_header",
	]
	DataAccess: {
		MySql: {}
	}
}

HookMetadata: TableData: [
	{
		group_name:   "unittesting"
		id:           "4039d098-ffc5-4984-8ed3-eb17bca98e19"
		entry_point:  "https://webhook.site/048c940b-b89e-467c-9cee-b6633b482e0f"
		name:         "webhook site"
		token:        "token"
		token_header: "token_header"
		sk:           "GROUP#unittesting"
		pk:           "HOOK#4039d098-ffc5-4984-8ed3-eb17bca98e19"
		hook_events: {
			SS: [
				"VULNERABILITY_CREATED",
			]
		}
		state: {
			modified_by:   "unknown"
			modified_date: "2019-11-22T20:07:57+00:00"
			status:        "INACTIVE"
		}
	},
	{
		group_name:   "unittesting"
		id:           "4039d098-ffc5-4984-8ed3-eb17bca98e20"
		entry_point:  "https://webhook.site/048c940b-b89e-467c-9cee-b6633b482e0g"
		token:        "token"
		token_header: "token_header"
		sk:           "GROUP#unittesting"
		pk:           "HOOK#4039d098-ffc5-4984-8ed3-eb17bca98e20"
		hook_events: {
			SS: [
				"VULNERABILITY_DELETED",
			]
		}
		state: {
			modified_by:   "unknown"
			modified_date: "2019-11-25T20:07:57+00:00"
			status:        "ACTIVE"
		}
	},
] & [...#HookItem]
