package organization_access

import (
	"fluidattacks.com/types/organization_access"
)

#organizationAccessPk: =~"^USER#[\\w.-@]+|__adminEmail__"
#organizationAccessSk: =~"^ORG#[a-f0-9-]{36}$"

#OrganizationAccessKeys: {
	pk: string & #organizationAccessPk
	sk: string & #organizationAccessSk
}

#OrganizationAccessItem: {
	#OrganizationAccessKeys
	organization_access.#OrganizationAccess
}

OrganizationAccess:
{
	FacetName: "organization_access"
	KeyAttributeAlias: {
		PartitionKeyAlias: "USER#email"
		SortKeyAlias:      "ORG#id"
	}
	NonKeyAttributes: [
		"email",
		"state",
		"organization_id",
		"expiration_time",
	]
	DataAccess: {
		MySql: {}
	}
}

OrganizationAccess: TableData: [
	{
		sk: "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
		pk: "USER#integratesuser@gmail.com"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			role:          "organization_manager"
			has_access:    true
			modified_date: "2023-12-28T15:26:46.102255+00:00"
		}
		email:           "integratesuser@gmail.com"
		organization_id: "38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
	},
	{
		sk: "ORG#956e9107-fd8d-49bc-b550-5609a7a1f6ac"
		pk: "USER#integratesuser@gmail.com"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			role:          "organization_manager"
			has_access:    true
			modified_date: "2023-12-28T15:26:46.166517+00:00"
		}
		email:           "integratesuser@gmail.com"
		organization_id: "956e9107-fd8d-49bc-b550-5609a7a1f6ac"
	},
	{
		sk: "ORG#c2ee2d15-04ab-4f39-9795-fbe30cdeee86"
		pk: "USER#integratesuser@gmail.com"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			modified_date: "2023-12-28T15:26:46.195148+00:00"
			has_access:    true
		}
		email:           "integratesuser@gmail.com"
		organization_id: "c2ee2d15-04ab-4f39-9795-fbe30cdeee86"
	},
	{
		sk: "ORG#c6cecc0e-bb92-4079-8b6d-c4e815c10bb1"
		pk: "USER#integratesuser@gmail.com"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			role:          "organization_manager"
			has_access:    true
			modified_date: "2023-12-28T15:26:46.213294+00:00"
		}
		email:           "integratesuser@gmail.com"
		organization_id: "c6cecc0e-bb92-4079-8b6d-c4e815c10bb1"
	},
	{
		sk: "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
		pk: "USER#forces.unittesting@fluidattacks.com"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			role:          "user"
			has_access:    true
			modified_date: "2023-12-28T15:26:46.087556+00:00"
		}
		email:           "forces.unittesting@fluidattacks.com"
		organization_id: "38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
	},
	{
		sk: "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
		pk: "USER#integratesuser2@gmail.com"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			role:          "user"
			has_access:    true
			modified_date: "2023-12-28T15:26:46.101561+00:00"
		}
		email:           "integratesuser2@gmail.com"
		organization_id: "38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
	},
	{
		sk: "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
		pk: "USER#integratesmanager@fluidattacks.com"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			role:          "organization_manager"
			has_access:    true
			modified_date: "2023-12-28T15:26:46.091225+00:00"
		}
		email:           "integratesmanager@fluidattacks.com"
		organization_id: "38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
	},
	{
		sk: "ORG#956e9107-fd8d-49bc-b550-5609a7a1f6ac"
		pk: "USER#integratesmanager@fluidattacks.com"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			role:          "admin"
			has_access:    true
			modified_date: "2023-12-28T15:26:46.158115+00:00"
		}
		email:           "integratesmanager@fluidattacks.com"
		organization_id: "956e9107-fd8d-49bc-b550-5609a7a1f6ac"
	},
	{
		sk: "ORG#c2ee2d15-04ab-4f39-9795-fbe30cdeee86"
		pk: "USER#integratesmanager@fluidattacks.com"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			role:          "user"
			has_access:    true
			modified_date: "2023-12-28T15:26:46.192941+00:00"
		}
		email:           "integratesmanager@fluidattacks.com"
		organization_id: "c2ee2d15-04ab-4f39-9795-fbe30cdeee86"
	},
	{
		sk: "ORG#c6cecc0e-bb92-4079-8b6d-c4e815c10bb1"
		pk: "USER#integratesmanager@fluidattacks.com"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			role:          "admin"
			has_access:    true
			modified_date: "2023-12-28T15:26:46.211696+00:00"
		}
		email:           "integratesmanager@fluidattacks.com"
		organization_id: "c6cecc0e-bb92-4079-8b6d-c4e815c10bb1"
	},
	{
		sk: "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
		pk: "USER#integratesreattacker@fluidattacks.com"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			role:          "user"
			has_access:    true
			modified_date: "2023-12-28T15:26:46.093509+00:00"
		}
		email:           "integratesreattacker@fluidattacks.com"
		organization_id: "38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
	},
	{
		sk: "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
		pk: "USER#vulnmanager@gmail.com"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			role:          "user"
			has_access:    true
			modified_date: "2023-12-28T15:26:46.106877+00:00"
		}
		email:           "vulnmanager@gmail.com"
		organization_id: "38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
	},
	{
		sk: "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
		pk: "USER#integratesmanager@gmail.com"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			role:          "organization_manager"
			has_access:    true
			modified_date: "2023-12-28T15:26:46.092084+00:00"
		}
		email:           "integratesmanager@gmail.com"
		organization_id: "38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
	},
	{
		sk: "ORG#956e9107-fd8d-49bc-b550-5609a7a1f6ac"
		pk: "USER#integratesmanager@gmail.com"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			role:          "admin"
			has_access:    true
			modified_date: "2023-12-28T15:26:46.161321+00:00"
		}
		email:           "integratesmanager@gmail.com"
		organization_id: "956e9107-fd8d-49bc-b550-5609a7a1f6ac"
	},
	{
		sk: "ORG#c2ee2d15-04ab-4f39-9795-fbe30cdeee86"
		pk: "USER#integratesmanager@gmail.com"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			role:          "user"
			has_access:    true
			modified_date: "2023-12-28T15:26:46.194126+00:00"
		}
		email:           "integratesmanager@gmail.com"
		organization_id: "c2ee2d15-04ab-4f39-9795-fbe30cdeee86"
	},
	{
		sk: "ORG#c6cecc0e-bb92-4079-8b6d-c4e815c10bb1"
		pk: "USER#integratesmanager@gmail.com"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			role:          "admin"
			has_access:    true
			modified_date: "2023-12-28T15:26:46.212423+00:00"
		}
		email:           "integratesmanager@gmail.com"
		organization_id: "c6cecc0e-bb92-4079-8b6d-c4e815c10bb1"
	},
	{
		sk: "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
		pk: "USER#integratesreviewer@fluidattacks.com"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			role:          "user"
			has_access:    true
			modified_date: "2023-12-28T15:26:46.096659+00:00"
		}
		email:           "integratesreviewer@fluidattacks.com"
		organization_id: "38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
	},
	{
		sk: "ORG#33c08ebd-2068-47e7-9673-e1aa03dc9448"
		pk: "USER#continuoushacking@gmail.com"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			role:          "admin"
			has_access:    true
			modified_date: "2023-12-28T15:26:46.070377+00:00"
		}
		email:           "continuoushacking@gmail.com"
		organization_id: "33c08ebd-2068-47e7-9673-e1aa03dc9448"
	},
	{
		sk: "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
		pk: "USER#continuoushacking@gmail.com"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			modified_date: "2023-12-28T15:26:46.085747+00:00"
			has_access:    true
		}
		email:           "continuoushacking@gmail.com"
		organization_id: "38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
	},
	{
		sk: "ORG#956e9107-fd8d-49bc-b550-5609a7a1f6ac"
		pk: "USER#continuoushacking@gmail.com"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			role:          "admin"
			has_access:    true
			modified_date: "2023-12-28T15:26:46.155538+00:00"
		}
		email:           "continuoushacking@gmail.com"
		organization_id: "956e9107-fd8d-49bc-b550-5609a7a1f6ac"
	},
	{
		sk: "ORG#c2ee2d15-04ab-4f39-9795-fbe30cdeee86"
		pk: "USER#continuoushacking@gmail.com"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			modified_date: "2023-12-28T15:26:46.191678+00:00"
			has_access:    true
		}
		email:           "continuoushacking@gmail.com"
		organization_id: "c2ee2d15-04ab-4f39-9795-fbe30cdeee86"
	},
	{
		sk: "ORG#c6cecc0e-bb92-4079-8b6d-c4e815c10bb1"
		pk: "USER#continuoushacking@gmail.com"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			role:          "admin"
			has_access:    true
			modified_date: "2023-12-28T15:26:46.210942+00:00"
		}
		email:           "continuoushacking@gmail.com"
		organization_id: "c6cecc0e-bb92-4079-8b6d-c4e815c10bb1"
	},
	{
		sk: "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
		pk: "USER#customer_manager@fluidattacks.com"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			role:          "customer_manager"
			has_access:    true
			modified_date: "2023-12-28T15:26:46.086556+00:00"
		}
		email:           "customer_manager@fluidattacks.com"
		organization_id: "38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
	},
	{
		sk: "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
		pk: "USER#integratesserviceforces@fluidattacks.com"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			role:          "user"
			has_access:    true
			modified_date: "2023-12-28T15:26:46.098220+00:00"
		}
		email:           "integratesserviceforces@fluidattacks.com"
		organization_id: "38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
	},
	{
		sk: "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
		pk: "USER#integratesresourcer@fluidattacks.com"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			role:          "user"
			has_access:    true
			modified_date: "2023-12-28T15:26:46.094985+00:00"
		}
		email:           "integratesresourcer@fluidattacks.com"
		organization_id: "38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
	},
	{
		sk: "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
		pk: "USER#unittest2@fluidattacks.com"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			role:          "customer_manager"
			has_access:    true
			modified_date: "2023-12-28T15:26:46.104167+00:00"
		}
		email:           "unittest2@fluidattacks.com"
		organization_id: "38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
	},
	{
		sk: "ORG#f2e2777d-a168-4bea-93cd-d79142b294d2"
		pk: "USER#org_testuser5@gmail.com"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			role:          "user"
			has_access:    true
			modified_date: "2023-12-28T15:26:46.236905+00:00"
		}
		email:           "org_testuser5@gmail.com"
		organization_id: "f2e2777d-a168-4bea-93cd-d79142b294d2"
	},
	{
		sk: "ORG#f2e2777d-a168-4bea-93cd-d79142b294d2"
		pk: "USER#org_testgroupmanager1@gmail.com"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			role:          "customer_manager"
			has_access:    true
			modified_date: "2023-12-28T15:26:46.233358+00:00"
		}
		email:           "org_testgroupmanager1@gmail.com"
		organization_id: "f2e2777d-a168-4bea-93cd-d79142b294d2"
	},
	{
		sk: "ORG#f2e2777d-a168-4bea-93cd-d79142b294d2"
		pk: "USER#org_testuser2@gmail.com"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			modified_date: "2023-12-28T15:26:46.234347+00:00"
			has_access:    true
		}
		email:           "org_testuser2@gmail.com"
		organization_id: "f2e2777d-a168-4bea-93cd-d79142b294d2"
	},
	{
		sk: "ORG#7376c5fe-4634-4053-9718-e14ecbda1e6b"
		pk: "USER#active_imamura3@fluidattacks.com"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			role:          "user"
			has_access:    true
			modified_date: "2023-12-28T15:26:46.140342+00:00"
		}
		email:           "active_imamura3@fluidattacks.com"
		organization_id: "7376c5fe-4634-4053-9718-e14ecbda1e6b"
	},
	{
		sk: "ORG#7376c5fe-4634-4053-9718-e14ecbda1e6b"
		pk: "USER#inactive_imamura1@fluidattacks.com"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			role:          "admin"
			has_access:    true
			modified_date: "2023-12-28T15:26:46.141165+00:00"
		}
		email:           "inactive_imamura1@fluidattacks.com"
		organization_id: "7376c5fe-4634-4053-9718-e14ecbda1e6b"
	},
	{
		sk: "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
		pk: "USER#integratesuser2@fluidattacks.com"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			role:          "user"
			has_access:    true
			modified_date: "2023-12-28T15:26:46.099248+00:00"
		}
		email:           "integratesuser2@fluidattacks.com"
		organization_id: "38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
	},
	{
		sk: "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
		pk: "USER#integrateshacker@fluidattacks.com"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			role:          "user"
			has_access:    true
			modified_date: "2023-12-28T15:26:46.089645+00:00"
		}
		email:           "integrateshacker@fluidattacks.com"
		organization_id: "38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
	},
	{
		sk: "ORG#956e9107-fd8d-49bc-b550-5609a7a1f6ac"
		pk: "USER#integrateshacker@fluidattacks.com"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			modified_date: "2023-12-28T15:26:46.156684+00:00"
			has_access:    true
		}
		email:           "integrateshacker@fluidattacks.com"
		organization_id: "956e9107-fd8d-49bc-b550-5609a7a1f6ac"
	},
	{
		sk: "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
		pk: "USER#continuoushack2@gmail.com"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			modified_date: "2023-12-28T15:26:46.085022+00:00"
			has_access:    true
		}
		email:           "continuoushack2@gmail.com"
		organization_id: "38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
	},
	{
		sk: "ORG#956e9107-fd8d-49bc-b550-5609a7a1f6ac"
		pk: "USER#continuoushack2@gmail.com"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			modified_date: "2023-12-28T15:26:46.154637+00:00"
			has_access:    true
		}
		email:           "continuoushack2@gmail.com"
		organization_id: "956e9107-fd8d-49bc-b550-5609a7a1f6ac"
	},
	{
		sk: "ORG#33c08ebd-2068-47e7-9673-e1aa03dc9448"
		pk: "USER#org_testuser6@gmail.com"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			modified_date: "2023-12-28T15:26:46.072043+00:00"
			has_access:    true
		}
		email:           "org_testuser6@gmail.com"
		organization_id: "33c08ebd-2068-47e7-9673-e1aa03dc9448"
	},
	{
		sk: "ORG#7376c5fe-4634-4053-9718-e14ecbda1e6b"
		pk: "USER#inactive_imamura2@fluidattacks.com"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			role:          "user"
			has_access:    true
			modified_date: "2023-12-28T15:26:46.141894+00:00"
		}
		email:           "inactive_imamura2@fluidattacks.com"
		organization_id: "7376c5fe-4634-4053-9718-e14ecbda1e6b"
	},
	{
		sk: "ORG#f2e2777d-a168-4bea-93cd-d79142b294d2"
		pk: "USER#org_testuser4@gmail.com"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			modified_date: "2023-12-28T15:26:46.235930+00:00"
			has_access:    true
		}
		email:           "org_testuser4@gmail.com"
		organization_id: "f2e2777d-a168-4bea-93cd-d79142b294d2"
	},
	{
		sk: "ORG#c2ee2d15-04ab-4f39-9795-fbe30cdeee86"
		pk: "USER#unittest"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			modified_date: "2023-12-28T15:26:46.197152+00:00"
			has_access:    true
		}
		email:           "unittest"
		organization_id: "c2ee2d15-04ab-4f39-9795-fbe30cdeee86"
	},
	{
		sk: "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
		pk: "USER#__adminEmail__"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			role:          "organization_manager"
			has_access:    true
			modified_date: "2023-12-28T15:26:46.084044+00:00"
		}
		email:           "__adminEmail__"
		organization_id: "38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
	},
	{
		sk: "ORG#956e9107-fd8d-49bc-b550-5609a7a1f6ac"
		pk: "USER#__adminEmail__"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			role:          "admin"
			has_access:    true
			modified_date: "2023-12-28T15:26:46.153595+00:00"
		}
		email:           "__adminEmail__"
		organization_id: "956e9107-fd8d-49bc-b550-5609a7a1f6ac"
	},
	{
		sk: "ORG#c2ee2d15-04ab-4f39-9795-fbe30cdeee86"
		pk: "USER#__adminEmail__"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			role:          "user"
			has_access:    true
			modified_date: "2023-12-28T15:26:46.190316+00:00"
		}
		email:           "__adminEmail__"
		organization_id: "c2ee2d15-04ab-4f39-9795-fbe30cdeee86"
	},
	{
		sk: "ORG#c6cecc0e-bb92-4079-8b6d-c4e815c10bb1"
		pk: "USER#__adminEmail__"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			role:          "admin"
			has_access:    true
			modified_date: "2023-12-28T15:26:46.210111+00:00"
		}
		email:           "__adminEmail__"
		organization_id: "c6cecc0e-bb92-4079-8b6d-c4e815c10bb1"
	},
	{
		sk: "ORG#fe80d2d4-ccb7-46d1-8489-67c6360581de"
		pk: "USER#org_testuser1@gmail.com"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			modified_date: "2023-12-28T15:26:46.250720+00:00"
			has_access:    true
		}
		email:           "org_testuser1@gmail.com"
		organization_id: "fe80d2d4-ccb7-46d1-8489-67c6360581de"
	},
	{
		sk: "ORG#f2e2777d-a168-4bea-93cd-d79142b294d2"
		pk: "USER#org_testuser3@gmail.com"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			role:          "user"
			has_access:    true
			modified_date: "2023-12-28T15:26:46.235063+00:00"
		}
		email:           "org_testuser3@gmail.com"
		organization_id: "f2e2777d-a168-4bea-93cd-d79142b294d2"
	},
	{
		sk: "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
		pk: "USER#org-resourcer@integrates.com"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			role:          "resourcer"
			has_access:    true
			modified_date: "2023-12-28T15:26:46.102255+00:00"
		}
		email:           "org-resourcer@integrates.com"
		organization_id: "38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
	},
	{
		sk: "ORG#5f727061-f608-4ff3-97c0-df9056011e2c"
		pk: "USER#integratesmanager@gmail.com"
		state: {
			modified_by:   "integratesmanager@gmail.com"
			has_access:    true
			role:          "customer_manager"
			modified_date: "2024-07-16T20:26:54.996205+00:00"
		}
		email:           "integratesmanager@gmail.com"
		organization_id: "5f727061-f608-4ff3-97c0-df9056011e2c"
	},
	{
		sk: "ORG#6f28de07-9fee-43f8-9675-a647422ff0f9"
		pk: "USER#__adminEmail__"
		state: {
			modified_by:   "__adminEmail__"
			has_access:    true
			role:          "customer_manager"
			modified_date: "2024-09-19T09:00:00.000000+00:00"
		}
		email:           "__adminEmail__"
		organization_id: "6f28de07-9fee-43f8-9675-a647422ff0f9"
	},
] & [...#OrganizationAccessItem]
