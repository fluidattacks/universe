package credentials_metadata

import (
	"fluidattacks.com/types/credentials"
)

#credentialsMetadataPk: =~"^CRED#[0-9a-z-]{36}$"
#credentialsMetadataSk: =~"^ORG#ORG#[0-9a-z-]{36}$"

#CredentialsMetadataKeys: {
	pk:   string & #credentialsMetadataPk
	sk:   string & #credentialsMetadataSk
	pk_2: string
	sk_2: string
}

#CredentialsItem: {
	credentials.#CredentialsMetadata
	#CredentialsMetadataKeys
}

CredentialsMetadata:
{
	FacetName: "credentials_metadata"
	KeyAttributeAlias: {
		PartitionKeyAlias: "CRED#id"
		SortKeyAlias:      "ORG#organization_id"
	}
	NonKeyAttributes: [
		"id",
		"pk_2",
		"sk_2",
		"state",
		"organization_id",
		"secret",
	]
	DataAccess: {
		MySql: {}
	}
}

CredentialsMetadata: TableData: [
	{
		organization_id: "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
		sk:              "ORG#ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
		pk_2:            "OWNER#integratesmanager@fluidattacks.com"
		id:              "dd2f08da-be59-4f97-8117-00f1cbbf6aec"
		pk:              "CRED#dd2f08da-be59-4f97-8117-00f1cbbf6aec"
		secret: {
			user:     ""
			password: ""
		}
		state: {
			owner:         "integratesmanager@fluidattacks.com"
			modified_by:   "integratesmanager@fluidattacks.com"
			name:          "Test credential"
			modified_date: "2022-12-22T03:45:00+00:00"
			is_pat:        false
			type:          "HTTPS"
		}
		sk_2: "CRED#dd2f08da-be59-4f97-8117-00f1cbbf6aec"
	},
	{
		organization_id: "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
		sk:              "ORG#ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
		pk_2:            "OWNER#integratesmanager@fluidattacks.com"
		id:              "0105c02b-34cd-43e8-b5b1-ed500f0c0878"
		pk:              "CRED#0105c02b-34cd-43e8-b5b1-ed500f0c0878"
		secret: {
			key: "__sshKey__"
		}
		state: {
			owner:         "integratesmanager@fluidattacks.com"
			modified_by:   "integratesmanager@fluidattacks.com"
			name:          "SSH Key 2"
			modified_date: "2022-12-22T03:45:00+00:00"
			type:          "SSH"
		}
		sk_2: "CRED#0105c02b-34cd-43e8-b5b1-ed500f0c0878"
	},
	{
		organization_id: "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
		sk:              "ORG#ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
		pk_2:            "OWNER#integratesmanager@gmail.com"
		id:              "abfe078c-19c3-49e3-a226-0410200f3d6f"
		pk:              "CRED#abfe078c-19c3-49e3-a226-0410200f3d6f"
		secret: {
			key: "LS0tLS1CRUdJTiBPUEVOU1NIIFBSSVZBVEUgS0VZLS0tLS0KTUlJRW93SUJBQUtDQVFFQWk3K2RQWmhVMlVLYUxwM3pDMG5hV25paEd0YWs0TGFFYjh4azA4YVdOampEZ0EwdQpVWlFTUDNFMWJ1MEFXZGdxS1ArVWtFVFNZS1hCaFZQRnJ3UU0zdDJVZDFuckJtMDdlUVBXd2t6N1hONjZ3b0JoCnJVa1FOdDIvZEVWUTZOaitxekZoRkd3cXNya0tLU0x4M2F4RW5jbXJwb3BzNXo1dDJ3b3pCYnFPelRCMVNhUUEKd0Q4VUp1VWhMRHZUd1ZwU3lvSEVDbjB1cXhVL3dmZ05Jcno4VkQ0ZlFOenFSZXdhRmxXOW1JTmhycnZNQmFvRApZTUt3bElmcGlHbmEyVEJIOUFEYW8xNWxSQ2R5OEZxRE9aMzlMREtjMGZqVWZPUFJ3d3RzdkYvRXc4YkRJVWNmCnJmdUgyNHJHYUQ5aDlpVFVQc1BpYTFRZDd1WExlUExMTXZkVzB3SURBUUFCQW9JQkFHamhVTUFtY0I5eDFjY0EKc2FRYU5DN0w4NVR4M0dIdGVPMXl4NFVNdURWUFdxSGl3bzFsQkdEcXRNQmdUSEFoUkZycW5YTzh3dXJYMjZvVgpXZnBrbnNnd0libUZHVmNFYWFzRHo0eVpPOWpTSy9YSFpnY29HMndoOWdycjdySWlFTmNvUXdTUnZXWE9hNnE5CjJDbVduUlNTZ3ltRklBQllvUmZQeXNVVTR4Rm9tQ09MMnBPR0lMODFxYmNBZnVJOEVDR0FwbXlhWWdIaWpjSlgKVlJXNDd1NnpNRWhlY3BwSHZmZG9yTS8wU20xbGdwSWE1WGI3SEFIQ2Q4aDVIKzJzazBheFBRcnQwakZIazNYRgpWYjUzeWdjVS92SXRKZkUvM0kyelRIYS94VExJVW1ieEg5eDJEQ2RDRmk2VkprSHFsY04xM2oxUVk1REF0TmVhCnQ3TGRyNGtDZ1lFQTUwUWpSSWR4VHpjcDNQWmttRHZPVzFoOUREU2lWd1Q3bVc2bzdKV2pOOVp4Ui9mdWpvRGYKLzJFcFQ2bThCSnl6UzJjQ3hZZENxRk1pRTh4QVJ2b2plcTYya1FRSkNqYThIbEY3YU5GL1JNL09oTXNFNFo3WgpqMmlFWnhZZHVsU2tmMWJkMi9XczJQby9lNTZoS0VuTXBPWE9RNzVET2N4T3FNQW8rV3NFU1c4Q2dZRUFtckhOCnpDV1FJeXNWMmhNeVBWMlc1NzZoTFFxMHJyb2NJUXBiaXk2elRUckpoK0NBcXFrdkdiSjZ0Qzh5MGhlcm5mWWYKbGIwNWRid1d4KzZkQjM0Y3lwYTZSV1VyaUw3RDB4ZFdxOVlOeC9IaVhrbFFhdERreDdtRmp3UHE5czNGVGpCRgp1TUlUeXRscHFQc3o3WDFaemltYVFCMFRkOXBZaGdYNUkra09MdDBDZ1lCSTU1NEdtSHdMOE9QdG1HOFYzcVNxCi96Wk1ocnMybTRlRlV2RkZ3OVowWDlIT1VrYU4vZXFmWTAzV296ZDJiTC9JNFd4cVFMMnV1cVpmZjJBei85NzMKYWVSN3lyM2V6bXAzVkZDSE5xUGNXWFpOUG9NSGJVQkFpVDVVRVJER0VMRGF3OU9rNThuWmpPbytXaitMcDlndgprNUoyb0tBb3RGT0h0eXd6bEowQldRS0JnR0hTQVpNUVJiOFhaYkFLNTRBS2lIeHUvVGlnUm5VZTF0N0Z6L0dKClRIU1BzaFNzRUhEYW55QWV6cHpXSThyYWtQbElROXpLVldWdjFQQUVvV08xaWJ0SjZPd3M5NFIzTFJsMDdxZW0KcWZ3L3BxZDhzTTk2M05DM0xnK3JxSTc4dThHZm9OY2VVcW01SmFsbmdBU3plWEIydWtYRW5TVHkrM3QwbDREdgp4TG81QW9HQkFMeWhQb25IcVV4U20wTUUrK1FKUU0ySTZiaTJPTHkyODZPbjRHZ2dKbEVDaS9zak0zbzZSdlg2Cmo5WmdyKytSa3ZsUS9STjBEdTh6M2NsUnZ3dWhUa2VUNDNUTDFiTFBFRzBjcWRsT3Foa01JcW90RjNJMXNlTFkKdGJRMmtIRnloNCtmRFZaRUYxYll3cTIrbFc2Yk1VczZFbGsrby8vcnc3VlpHWWllMzAxeAotLS0tLUVORCBPUEVOU1NIIFBSSVZBVEUgS0VZLS0tLS0K"
		}
		state: {
			owner:         "integratesmanager@gmail.com"
			modified_by:   "integratesmanager@gmail.com"
			name:          "Unused SSH Key 3"
			modified_date: "2022-12-22T03:45:00+00:00"
			type:          "SSH"
		}
		sk_2: "CRED#abfe078c-19c3-49e3-a226-0410200f3d6f"
	},
	{
		organization_id: "ORG#956e9107-fd8d-49bc-b550-5609a7a1f6ac"
		sk:              "ORG#ORG#956e9107-fd8d-49bc-b550-5609a7a1f6ac"
		pk_2:            "OWNER#integratesmanager@fluidattacks.com"
		id:              "c624dc13-ac87-4cff-b710-767d3c3fdfd4"
		pk:              "CRED#c624dc13-ac87-4cff-b710-767d3c3fdfd4"
		secret: {
			key: "LS0tLS1CRUdJTiBPUEVOU1NIIFBSSVZBVEUgS0VZLS0tLS0KTUlJRW93SUJBQUtDQVFFQWk3K2RQWmhVMlVLYUxwM3pDMG5hV25paEd0YWs0TGFFYjh4azA4YVdOampEZ0EwdQpVWlFTUDNFMWJ1MEFXZGdxS1ArVWtFVFNZS1hCaFZQRnJ3UU0zdDJVZDFuckJtMDdlUVBXd2t6N1hONjZ3b0JoCnJVa1FOdDIvZEVWUTZOaitxekZoRkd3cXNya0tLU0x4M2F4RW5jbXJwb3BzNXo1dDJ3b3pCYnFPelRCMVNhUUEKd0Q4VUp1VWhMRHZUd1ZwU3lvSEVDbjB1cXhVL3dmZ05Jcno4VkQ0ZlFOenFSZXdhRmxXOW1JTmhycnZNQmFvRApZTUt3bElmcGlHbmEyVEJIOUFEYW8xNWxSQ2R5OEZxRE9aMzlMREtjMGZqVWZPUFJ3d3RzdkYvRXc4YkRJVWNmCnJmdUgyNHJHYUQ5aDlpVFVQc1BpYTFRZDd1WExlUExMTXZkVzB3SURBUUFCQW9JQkFHamhVTUFtY0I5eDFjY0EKc2FRYU5DN0w4NVR4M0dIdGVPMXl4NFVNdURWUFdxSGl3bzFsQkdEcXRNQmdUSEFoUkZycW5YTzh3dXJYMjZvVgpXZnBrbnNnd0libUZHVmNFYWFzRHo0eVpPOWpTSy9YSFpnY29HMndoOWdycjdySWlFTmNvUXdTUnZXWE9hNnE5CjJDbVduUlNTZ3ltRklBQllvUmZQeXNVVTR4Rm9tQ09MMnBPR0lMODFxYmNBZnVJOEVDR0FwbXlhWWdIaWpjSlgKVlJXNDd1NnpNRWhlY3BwSHZmZG9yTS8wU20xbGdwSWE1WGI3SEFIQ2Q4aDVIKzJzazBheFBRcnQwakZIazNYRgpWYjUzeWdjVS92SXRKZkUvM0kyelRIYS94VExJVW1ieEg5eDJEQ2RDRmk2VkprSHFsY04xM2oxUVk1REF0TmVhCnQ3TGRyNGtDZ1lFQTUwUWpSSWR4VHpjcDNQWmttRHZPVzFoOUREU2lWd1Q3bVc2bzdKV2pOOVp4Ui9mdWpvRGYKLzJFcFQ2bThCSnl6UzJjQ3hZZENxRk1pRTh4QVJ2b2plcTYya1FRSkNqYThIbEY3YU5GL1JNL09oTXNFNFo3WgpqMmlFWnhZZHVsU2tmMWJkMi9XczJQby9lNTZoS0VuTXBPWE9RNzVET2N4T3FNQW8rV3NFU1c4Q2dZRUFtckhOCnpDV1FJeXNWMmhNeVBWMlc1NzZoTFFxMHJyb2NJUXBiaXk2elRUckpoK0NBcXFrdkdiSjZ0Qzh5MGhlcm5mWWYKbGIwNWRid1d4KzZkQjM0Y3lwYTZSV1VyaUw3RDB4ZFdxOVlOeC9IaVhrbFFhdERreDdtRmp3UHE5czNGVGpCRgp1TUlUeXRscHFQc3o3WDFaemltYVFCMFRkOXBZaGdYNUkra09MdDBDZ1lCSTU1NEdtSHdMOE9QdG1HOFYzcVNxCi96Wk1ocnMybTRlRlV2RkZ3OVowWDlIT1VrYU4vZXFmWTAzV296ZDJiTC9JNFd4cVFMMnV1cVpmZjJBei85NzMKYWVSN3lyM2V6bXAzVkZDSE5xUGNXWFpOUG9NSGJVQkFpVDVVRVJER0VMRGF3OU9rNThuWmpPbytXaitMcDlndgprNUoyb0tBb3RGT0h0eXd6bEowQldRS0JnR0hTQVpNUVJiOFhaYkFLNTRBS2lIeHUvVGlnUm5VZTF0N0Z6L0dKClRIU1BzaFNzRUhEYW55QWV6cHpXSThyYWtQbElROXpLVldWdjFQQUVvV08xaWJ0SjZPd3M5NFIzTFJsMDdxZW0KcWZ3L3BxZDhzTTk2M05DM0xnK3JxSTc4dThHZm9OY2VVcW01SmFsbmdBU3plWEIydWtYRW5TVHkrM3QwbDREdgp4TG81QW9HQkFMeWhQb25IcVV4U20wTUUrK1FKUU0ySTZiaTJPTHkyODZPbjRHZ2dKbEVDaS9zak0zbzZSdlg2Cmo5WmdyKytSa3ZsUS9STjBEdTh6M2NsUnZ3dWhUa2VUNDNUTDFiTFBFRzBjcWRsT3Foa01JcW90RjNJMXNlTFkKdGJRMmtIRnloNCtmRFZaRUYxYll3cTIrbFc2Yk1VczZFbGsrby8vcnc3VlpHWWllMzAxeAotLS0tLUVORCBPUEVOU1NIIFBSSVZBVEUgS0VZLS0tLS0K"
		}
		state: {
			owner:         "integratesmanager@fluidattacks.com"
			modified_by:   "integratesmanager@fluidattacks.com"
			name:          "Kamiya SSH"
			modified_date: "2022-12-22T03:45:00+00:00"
			type:          "SSH"
		}
		sk_2: "CRED#c624dc13-ac87-4cff-b710-767d3c3fdfd4"
	},
	{
		organization_id: "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
		sk:              "ORG#ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
		pk_2:            "OWNER#integratesreattacker@fluidattacks.com"
		id:              "0df32c4a-526d-4879-8dc8-9ae191d2721b"
		pk:              "CRED#0df32c4a-526d-4879-8dc8-9ae191d2721b"
		secret: {
			access_token:  "e43cf1ca88612f0ab62866aaeebb23cd498f8b33813f81a9b95ebf67d45b86bc"
			refresh_token: "1f281de97a9be2737034126089a8dbcfeadd5eb8fbb3380a66ad4c4ec2acaf7c"
			redirect_uri:  "https://localhost:8001/oauth_gitlab?subject=ORG%2338eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
			valid_until:   "2023-12-22T04:45:00+00:00"
		}
		state: {
			is_pat:        false
			owner:         "integratesreattacker@fluidattacks.com"
			modified_by:   "integratesreattacker@fluidattacks.com"
			name:          "reattacker (GitLab OAuth)"
			modified_date: "2023-12-22T03:45:00+00:00"
			type:          "OAUTH"
		}
		sk_2: "CRED#0df32c4a-526d-4879-8dc8-9ae191d2721b"
	},
	{
		organization_id: "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
		sk:              "ORG#ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
		pk_2:            "OWNER#integratesmanager@fluidattacks.com"
		id:              "5f8bd377-fb89-4094-be6b-c9092019b01c"
		pk:              "CRED#5f8bd377-fb89-4094-be6b-c9092019b01c"
		secret: {
			access_token:  "e43cf1ca88612f0ab62866aaeebb23cd498f8b33813f81a9b95ebf67d45b86bc"
			refresh_token: "1f281de97a9be2737034126089a8dbcfeadd5eb8fbb3380a66ad4c4ec2acaf7c"
			redirect_uri:  "https://localhost:8001/oauth_gitlab?subject=ORG%2338eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
			valid_until:   "2023-12-22T04:45:00+00:00"
		}
		state: {
			is_pat:        false
			owner:         "integratesmanager@fluidattacks.com"
			modified_by:   "integratesmanager@fluidattacks.com"
			name:          "manager (GitLab OAuth)"
			modified_date: "2023-12-22T03:45:00+00:00"
			type:          "OAUTH"
		}
		sk_2: "CRED#5f8bd377-fb89-4094-be6b-c9092019b01c"
	},
	{
		organization_id: "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
		sk:              "ORG#ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
		pk_2:            "OWNER#integratesmanager@fluidattacks.com"
		id:              "0b8bf4cb-8735-4232-8199-46cd9802ad2a"
		pk:              "CRED#0b8bf4cb-8735-4232-8199-46cd9802ad2a"
		secret: {
			key: "LS0tLS1CRUdJTiBPUEVOU1NIIFBSSVZBVEUgS0VZLS0tLS0KTUlJRW93SUJBQUtDQVFFQWk3K2RQWmhVMlVLYUxwM3pDMG5hV25paEd0YWs0TGFFYjh4azA4YVdOampEZ0EwdQpVWlFTUDNFMWJ1MEFXZGdxS1ArVWtFVFNZS1hCaFZQRnJ3UU0zdDJVZDFuckJtMDdlUVBXd2t6N1hONjZ3b0JoCnJVa1FOdDIvZEVWUTZOaitxekZoRkd3cXNya0tLU0x4M2F4RW5jbXJwb3BzNXo1dDJ3b3pCYnFPelRCMVNhUUEKd0Q4VUp1VWhMRHZUd1ZwU3lvSEVDbjB1cXhVL3dmZ05Jcno4VkQ0ZlFOenFSZXdhRmxXOW1JTmhycnZNQmFvRApZTUt3bElmcGlHbmEyVEJIOUFEYW8xNWxSQ2R5OEZxRE9aMzlMREtjMGZqVWZPUFJ3d3RzdkYvRXc4YkRJVWNmCnJmdUgyNHJHYUQ5aDlpVFVQc1BpYTFRZDd1WExlUExMTXZkVzB3SURBUUFCQW9JQkFHamhVTUFtY0I5eDFjY0EKc2FRYU5DN0w4NVR4M0dIdGVPMXl4NFVNdURWUFdxSGl3bzFsQkdEcXRNQmdUSEFoUkZycW5YTzh3dXJYMjZvVgpXZnBrbnNnd0libUZHVmNFYWFzRHo0eVpPOWpTSy9YSFpnY29HMndoOWdycjdySWlFTmNvUXdTUnZXWE9hNnE5CjJDbVduUlNTZ3ltRklBQllvUmZQeXNVVTR4Rm9tQ09MMnBPR0lMODFxYmNBZnVJOEVDR0FwbXlhWWdIaWpjSlgKVlJXNDd1NnpNRWhlY3BwSHZmZG9yTS8wU20xbGdwSWE1WGI3SEFIQ2Q4aDVIKzJzazBheFBRcnQwakZIazNYRgpWYjUzeWdjVS92SXRKZkUvM0kyelRIYS94VExJVW1ieEg5eDJEQ2RDRmk2VkprSHFsY04xM2oxUVk1REF0TmVhCnQ3TGRyNGtDZ1lFQTUwUWpSSWR4VHpjcDNQWmttRHZPVzFoOUREU2lWd1Q3bVc2bzdKV2pOOVp4Ui9mdWpvRGYKLzJFcFQ2bThCSnl6UzJjQ3hZZENxRk1pRTh4QVJ2b2plcTYya1FRSkNqYThIbEY3YU5GL1JNL09oTXNFNFo3WgpqMmlFWnhZZHVsU2tmMWJkMi9XczJQby9lNTZoS0VuTXBPWE9RNzVET2N4T3FNQW8rV3NFU1c4Q2dZRUFtckhOCnpDV1FJeXNWMmhNeVBWMlc1NzZoTFFxMHJyb2NJUXBiaXk2elRUckpoK0NBcXFrdkdiSjZ0Qzh5MGhlcm5mWWYKbGIwNWRid1d4KzZkQjM0Y3lwYTZSV1VyaUw3RDB4ZFdxOVlOeC9IaVhrbFFhdERreDdtRmp3UHE5czNGVGpCRgp1TUlUeXRscHFQc3o3WDFaemltYVFCMFRkOXBZaGdYNUkra09MdDBDZ1lCSTU1NEdtSHdMOE9QdG1HOFYzcVNxCi96Wk1ocnMybTRlRlV2RkZ3OVowWDlIT1VrYU4vZXFmWTAzV296ZDJiTC9JNFd4cVFMMnV1cVpmZjJBei85NzMKYWVSN3lyM2V6bXAzVkZDSE5xUGNXWFpOUG9NSGJVQkFpVDVVRVJER0VMRGF3OU9rNThuWmpPbytXaitMcDlndgprNUoyb0tBb3RGT0h0eXd6bEowQldRS0JnR0hTQVpNUVJiOFhaYkFLNTRBS2lIeHUvVGlnUm5VZTF0N0Z6L0dKClRIU1BzaFNzRUhEYW55QWV6cHpXSThyYWtQbElROXpLVldWdjFQQUVvV08xaWJ0SjZPd3M5NFIzTFJsMDdxZW0KcWZ3L3BxZDhzTTk2M05DM0xnK3JxSTc4dThHZm9OY2VVcW01SmFsbmdBU3plWEIydWtYRW5TVHkrM3QwbDREdgp4TG81QW9HQkFMeWhQb25IcVV4U20wTUUrK1FKUU0ySTZiaTJPTHkyODZPbjRHZ2dKbEVDaS9zak0zbzZSdlg2Cmo5WmdyKytSa3ZsUS9STjBEdTh6M2NsUnZ3dWhUa2VUNDNUTDFiTFBFRzBjcWRsT3Foa01JcW90RjNJMXNlTFkKdGJRMmtIRnloNCtmRFZaRUYxYll3cTIrbFc2Yk1VczZFbGsrby8vcnc3VlpHWWllMzAxeAotLS0tLUVORCBPUEVOU1NIIFBSSVZBVEUgS0VZLS0tLS0K"
		}
		state: {
			owner:         "integratesmanager@fluidattacks.com"
			modified_by:   "integratesmanager@fluidattacks.com"
			name:          "Product New SSH Key"
			modified_date: "2021-12-22T03:45:00+00:00"
			type:          "SSH"
		}
		sk_2: "CRED#0b8bf4cb-8735-4232-8199-46cd9802ad2a"
	},
] & [...#CredentialsItem]
