package org_finding_policy_historic_state

import (
	"fluidattacks.com/types/organization_finding_policies"
)

#orgFindingPolicyHistoricStatePk: =~"^POLICY#[a-f0-9-]{36}$"
#orgFindingPolicyHistoricStateSk: =~"^STATE#(\\d{4})-(\\d{2})-(\\d{2})(T(\\d{2}):(\\d{2}):(\\d{2})(\\.\\d+)?(Z|([+-])(\\d{2}):(\\d{2})))?$"

#OrgFindingPolicyHistoricStateKeys: {
	pk: string & #orgFindingPolicyHistoricStatePk
	sk: string & #orgFindingPolicyHistoricStateSk
}

#OrgFindingPolicyHistoricStateItem: {
	#OrgFindingPolicyHistoricStateKeys
	organization_finding_policies.#OrgFindingPolicyState
}

OrgFindingPolicyHistoricState:
{
	FacetName: "org_finding_policy_historic_state"
	KeyAttributeAlias: {
		PartitionKeyAlias: "POLICY#uuid"
		SortKeyAlias:      "STATE#iso8601utc"
	}
	NonKeyAttributes: [
		"status",
		"modified_date",
		"modified_by",
	]
	DataAccess: {
		MySql: {}
	}
}

OrgFindingPolicyHistoricState: TableData: [
	{
		pk:            "POLICY#8b35ae2a-56a1-4f64-9da7-6a552683bf46"
		sk:            "STATE#2021-04-25T13:37:10+00:00"
		status:        "SUBMITTED"
		modified_date: "2021-04-25T13:37:10+00:00"
		modified_by:   "test@test.com"
	},
	{
		pk:            "POLICY#8b35ae2a-56a1-4f64-9da7-6a552683bf46"
		sk:            "STATE#2021-04-26T13:37:10+00:00"
		status:        "APPROVED"
		modified_date: "2021-04-26T13:37:10+00:00"
		modified_by:   "test2@test.com"
	},
	{
		pk:            "POLICY#5f1de801-5c4b-428a-ba33-6a1b8d63e45c"
		sk:            "STATE#2024-04-16T13:37:10+00:00"
		status:        "APPROVED"
		modified_date: "2024-04-16T13:37:10+00:00"
		modified_by:   "test2@test.com"
	},
] & [...#OrgFindingPolicyHistoricStateItem]
