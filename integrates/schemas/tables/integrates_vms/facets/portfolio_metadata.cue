package portfolio_metadata

import (
	"fluidattacks.com/types/portfolios"
)

#portfolioMetadataPk: =~"^PORTFOLIO#[a-z-]+$"
#portfolioMetadataSk: =~"^ORG#[a-z]+$"

#PortfolioMetadataKeys: {
	pk: string & #portfolioMetadataPk
	sk: string & #portfolioMetadataSk
}

#PortfolioItem: {
	#PortfolioMetadataKeys
	portfolios.#PortfolioMetadata
}

PortfolioMetadata:
{
	FacetName: "portfolio_metadata"
	KeyAttributeAlias: {
		PartitionKeyAlias: "PORTFOLIO#id"
		SortKeyAlias:      "ORG#name"
	}
	NonKeyAttributes: [
		"id",
		"unreliable_indicators",
		"organization_name",
		"groups",
	]
	DataAccess: {
		MySql: {}
	}
}

PortfolioMetadata: TableData: [
	{
		sk: "ORG#okada"
		groups: {
			SS: [
				"oneshottest",
				"unittesting",
			]
		}
		id:                "test-updates"
		organization_name: "okada"
		pk:                "PORTFOLIO#test-updates"
		unreliable_indicators: {
			mean_remediate:                   132.0
			max_severity:                     6.0
			max_open_severity:                4.7
			mean_remediate_critical_severity: 0.0
			last_closing_date:                20
			mean_remediate_high_severity:     0.0
			mean_remediate_low_severity:      126.0
			mean_remediate_medium_severity:   113.5
		}
	},
	{
		sk: "ORG#okada"
		groups: {
			SS: [
				"continuoustesting",
				"oneshottest",
			]
		}
		id:                "another-tag"
		organization_name: "okada"
		pk:                "PORTFOLIO#another-tag"
		unreliable_indicators: {
			mean_remediate:                   103.0
			max_severity:                     3.9
			max_open_severity:                3.9
			mean_remediate_critical_severity: 0.0
			last_closing_date:                45
			mean_remediate_high_severity:     0.0
			mean_remediate_low_severity:      0.0
			mean_remediate_medium_severity:   0.0
		}
	},
	{
		sk: "ORG#okada"
		groups: {
			SS: [
				"continuoustesting",
				"oneshottest",
				"unittesting",
			]
		}
		id:                "test-tag"
		organization_name: "okada"
		pk:                "PORTFOLIO#test-tag"
		unreliable_indicators: {
			mean_remediate:                   73.0
			max_severity:                     6.3
			max_open_severity:                6.3
			mean_remediate_critical_severity: 83.0
			last_closing_date:                45
			mean_remediate_high_severity:     93.0
			mean_remediate_low_severity:      103.0
			mean_remediate_medium_severity:   113.0
		}
	},
	{
		sk: "ORG#kamiya"
		groups: {
			SS: [
				"barranquilla",
				"monteria",
			]
		}
		id:                "test-newgroups"
		organization_name: "kamiya"
		pk:                "PORTFOLIO#test-newgroups"
		unreliable_indicators: {
			mean_remediate:                   112.0
			max_severity:                     6.3
			max_open_severity:                2.3
			mean_remediate_critical_severity: 0.0
			last_closing_date:                40
			mean_remediate_high_severity:     0.0
			mean_remediate_low_severity:      134.0
			mean_remediate_medium_severity:   122.3
		}
	},
	{
		sk: "ORG#makimachi"
		groups: {
			SS: [
				"asgard",
				"gotham",
				"metropolis",
			]
		}
		id:                "test-nogroups"
		organization_name: "makimachi"
		pk:                "PORTFOLIO#test-nogroups"
		unreliable_indicators: {
			mean_remediate:                   123.0
			max_severity:                     4.3
			max_open_severity:                3.3
			mean_remediate_critical_severity: 0.0
			last_closing_date:                50
			mean_remediate_high_severity:     0.0
			mean_remediate_low_severity:      116.0
			mean_remediate_medium_severity:   135.9
		}
	},
	{
		sk: "ORG#okada"
		groups: {
			SS: [
				"oneshottest",
				"unittesting",
			]
		}
		id:                "test-groups"
		organization_name: "okada"
		pk:                "PORTFOLIO#test-groups"
		unreliable_indicators: {
			mean_remediate:                   174.0
			max_severity:                     6.3
			max_open_severity:                6.3
			mean_remediate_critical_severity: 0.0
			last_closing_date:                23
			mean_remediate_high_severity:     0.0
			mean_remediate_low_severity:      116.0
			mean_remediate_medium_severity:   143.5
		}
	},
] & [...#PortfolioItem]
