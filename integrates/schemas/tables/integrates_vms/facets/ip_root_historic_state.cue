package ip_root_historic_state

import (
	"fluidattacks.com/types/roots"
)

#ipRootHistoricStatePk: =~"^ROOT#[a-z0-9-]+"
#ipRootHistoricStateSk: =~"^STATE#(\\d{4})-(\\d{2})-(\\d{2})(T(\\d{2}):(\\d{2}):(\\d{2})(\\.\\d+)?(Z|([+-])(\\d{2}):(\\d{2})))?$"

#IpRootHistoricStateKeys: {
	pk: string & #ipRootHistoricStatePk
	sk: string & #ipRootHistoricStateSk
}

#IpRootHistoricStateItem: {
	#IpRootHistoricStateKeys
	roots.#IpRootState
}

IpRootHistoricState:
{
	FacetName: "ip_root_historic_state"
	KeyAttributeAlias: {
		PartitionKeyAlias: "ROOT#uuid"
		SortKeyAlias:      "STATE#iso8601utc"
	}
	NonKeyAttributes: [
		"status",
		"reason",
		"modified_date",
		"modified_by",
		"nickname",
		"address",
		"other",
	]
	DataAccess: {
		MySql: {}
	}
}

IpRootHistoricState: TableData: [
	{
		pk:            "ROOT#d312f0b9-da49-4d2b-a881-bed438875e99"
		sk:            "STATE#2020-11-19T13:44:37+00:00"
		status:        "ACTIVE"
		modified_date: "2020-11-19T13:44:37+00:00"
		modified_by:   "jdoe@fluidattacks.com"
		nickname:      "ip_root_1"
		address:       "127.0.0.1"
	},
	{
		pk:            "ROOT#814addf0-316c-4415-850d-21bd3783b011"
		sk:            "STATE#2020-12-19T13:44:37+00:00"
		status:        "INACTIVE"
		modified_date: "2020-12-19T13:44:37+00:00"
		modified_by:   "jdoe@fluidattacks.com"
		nickname:      "ip_root_2"
		address:       "127.0.0.1"
	},
] & [...#IpRootHistoricStateItem]
