package event_historic_state

import (
	"fluidattacks.com/types/events"
)

#eventKey: =~"^EVENT#[0-9]+$"
#stateKey: =~"^STATE#[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}[+-][0-9]{2}:[0-9]{2}$"

#EventHistoricStateKeys: {
	pk: string & #eventKey
	sk: string & #stateKey
}

#EventHistoricStateItem: {
	events.#EventState
	#EventHistoricStateKeys
}

EventHistoricState:
{
	FacetName: "event_historic_state"
	KeyAttributeAlias: {
		PartitionKeyAlias: "EVENT#id"
		SortKeyAlias:      "STATE#iso8601utc"
	}
	NonKeyAttributes: [
		for k, _ in events.#EventState {k}, "comment_id",
	]
	DataAccess: {
		MySql: {}
	}
}

EventHistoricState: TableData: [
	{
		sk:            "STATE#2018-12-17T21:20:00+00:00"
		modified_by:   "unittest@fluidattacks.com"
		pk:            "EVENT#463578352"
		modified_date: "2018-12-17T21:20:00+00:00"
		status:        "OPEN"
	},
	{
		sk:            "STATE#2018-12-17T21:21:03+00:00"
		modified_by:   "unittest@fluidattacks.com"
		pk:            "EVENT#463578352"
		modified_date: "2018-12-17T21:21:03+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2018-12-26T18:37:00+00:00"
		modified_by:   "unittest@fluidattacks.com"
		pk:            "EVENT#463578352"
		modified_date: "2018-12-26T18:37:00+00:00"
		status:        "SOLVED"
	},
	{
		sk:            "STATE#2019-04-02T08:02:00+00:00"
		modified_by:   "unittest@fluidattacks.com"
		pk:            "EVENT#540462628"
		modified_date: "2019-04-02T08:02:00+00:00"
		status:        "OPEN"
	},
	{
		sk:            "STATE#2019-09-25T14:36:27+00:00"
		modified_by:   "unittest@fluidattacks.com"
		pk:            "EVENT#540462628"
		modified_date: "2019-09-25T14:36:27+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2018-06-27T12:00:00+00:00"
		modified_by:   "unittest@fluidattacks.com"
		pk:            "EVENT#418900971"
		modified_date: "2018-06-27T12:00:00+00:00"
		status:        "OPEN"
	},
	{
		sk:            "STATE#2018-06-27T19:40:05+00:00"
		modified_by:   "unittest@fluidattacks.com"
		pk:            "EVENT#418900971"
		modified_date: "2018-06-27T19:40:05+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2019-04-02T08:02:00+00:00"
		modified_by:   "unittest@fluidattacks.com"
		pk:            "EVENT#540462638"
		modified_date: "2019-04-02T08:02:00+00:00"
		status:        "OPEN"
	},
	{
		sk:            "STATE#2021-05-25T14:36:27+00:00"
		modified_by:   "unittest@fluidattacks.com"
		pk:            "EVENT#540462638"
		modified_date: "2021-05-25T14:36:27+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2019-09-19T13:09:00+00:00"
		modified_by:   "unittest@fluidattacks.com"
		pk:            "EVENT#538745942"
		modified_date: "2019-09-19T13:09:00+00:00"
		status:        "OPEN"
	},
	{
		sk:            "STATE#2019-09-19T15:43:43+00:00"
		modified_by:   "unittest@fluidattacks.com"
		pk:            "EVENT#538745942"
		modified_date: "2019-09-19T15:43:43+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2019-01-01T20:24:25+00:00"
		modified_by:   "unittest@fluidattacks.com"
		pk:            "EVENT#418900979"
		modified_date: "2020-01-01T20:24:25+00:00"
		status:        "OPEN"
	},
	{
		sk:            "STATE#2019-01-02T20:24:25+00:00"
		modified_by:   "unittest@fluidattacks.com"
		pk:            "EVENT#418900979"
		modified_date: "2020-01-02T20:24:25+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2019-01-03T20:24:25+00:00"
		modified_by:   "unittest@fluidattacks.com"
		pk:            "EVENT#418900979"
		modified_date: "2020-01-03T20:24:25+00:00"
		status:        "SOLVED"
	},
	{
		sk:            "STATE#2020-01-02T12:00:00+00:00"
		modified_by:   "unittest@fluidattacks.com"
		pk:            "EVENT#418900978"
		modified_date: "2020-01-02T12:00:00+00:00"
		status:        "OPEN"
	},
	{
		sk:            "STATE#2020-01-02T19:40:05+00:00"
		modified_by:   "unittest@fluidattacks.com"
		pk:            "EVENT#418900978"
		modified_date: "2020-01-02T19:40:05+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2019-03-11T14:00:00+00:00"
		modified_by:   "unittest@fluidattacks.com"
		pk:            "EVENT#484763304"
		modified_date: "2019-03-11T14:00:00+00:00"
		status:        "OPEN"
	},
	{
		sk:            "STATE#2019-03-11T15:57:45+00:00"
		modified_by:   "unittest@fluidattacks.com"
		pk:            "EVENT#484763304"
		modified_date: "2019-03-11T15:57:45+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2019-04-11T18:37:00+00:00"
		modified_by:   "unittest@fluidattacks.com"
		pk:            "EVENT#484763304"
		modified_date: "2019-04-11T18:37:00+00:00"
		status:        "SOLVED"
	},
	{
		sk:            "STATE#2018-01-01T20:24:25+00:00"
		modified_by:   "unittest@fluidattacks.com"
		pk:            "EVENT#418900923"
		modified_date: "2018-01-01T20:24:25+00:00"
		status:        "OPEN"
	},
	{
		sk:            "STATE#2018-01-02T20:24:25+00:00"
		modified_by:   "unittest@fluidattacks.com"
		pk:            "EVENT#418900923"
		modified_date: "2018-01-02T20:24:25+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2018-01-03T20:24:25+00:00"
		modified_by:   "unittest@fluidattacks.com"
		pk:            "EVENT#418900923"
		modified_date: "2018-01-03T20:24:25+00:00"
		status:        "SOLVED"
	},
	{
		sk:            "STATE#2022-10-28T00:00:00+00:00"
		modified_by:   "unittest@fluidattacks.com"
		pk:            "EVENT#48192579"
		modified_date: "2022-10-28T00:00:00+00:00"
		status:        "OPEN"
	},
	{
		sk:            "STATE#2022-10-28T00:00:00+00:01"
		modified_by:   "unittest@fluidattacks.com"
		pk:            "EVENT#48192579"
		modified_date: "2022-10-28T00:00:00+00:01"
		status:        "CREATED"
	},
] & [...#EventHistoricStateItem]
