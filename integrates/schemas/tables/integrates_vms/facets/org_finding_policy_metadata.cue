package org_finding_policy_metadata

import (
	"fluidattacks.com/types/organization_finding_policies"
)

#orgFindingPolicyMetadataPk: =~"^POLICY#[a-f0-9-]{36}$"
#orgFindingPolicyMetadataSk: =~"^ORG#[a-z0-9]+$"

#OrgFindingPolicyMetadataKeys: {
	pk: string & #orgFindingPolicyMetadataPk
	sk: string & #orgFindingPolicyMetadataSk
}

#OrgFindingPolicyItem: {
	#OrgFindingPolicyMetadataKeys
	organization_finding_policies.#OrgFindingPolicyMetadata
}

OrgFindingPolicyMetadata:
{
	FacetName: "org_finding_policy_metadata"
	KeyAttributeAlias: {
		PartitionKeyAlias: "POLICY#uuid"
		SortKeyAlias:      "ORG#name"
	}
	NonKeyAttributes: [
		"name",
		"tags",
		"treatment_acceptance",
		"state",
	]
	DataAccess: {
		MySql: {}
	}
}

OrgFindingPolicyMetadata: TableData: [
	{
		pk:   "POLICY#8b35ae2a-56a1-4f64-9da7-6a552683bf46"
		sk:   "ORG#okada"
		name: "007. Cross-site request forgery"
		state: {
			status:        "APPROVED"
			modified_date: "2021-04-26T13:37:10+00:00"
			modified_by:   "test2@test.com"
		}
		treatment_acceptance: "ACCEPTED_UNDEFINED"
	},
	{
		pk:   "POLICY#5f1de801-5c4b-428a-ba33-6a1b8d63e45c"
		sk:   "ORG#okada"
		name: "010. Stored cross-site scripting (XSS)"
		state: {
			status:        "APPROVED"
			modified_date: "2024-04-16T13:37:10+00:00"
			modified_by:   "test2@test.com"
		}
		treatment_acceptance: "ACCEPTED"
	},
] & [...#OrgFindingPolicyItem]
