package finding_comment

import (
	"fluidattacks.com/types/finding_comments"
)

#findingCommentPk: =~"^CMNT#[0-9]+$"
#findingCommentSk: =~"^FIN#[0-9]+$"

#FindingCommentKeys: {
	pk: string & #findingCommentPk
	sk: string & #findingCommentSk
}

#FindingCommentItem: {
	finding_comments.#FindingComment
	#FindingCommentKeys
}

FindingComment:
{
	FacetName: "finding_comment"
	KeyAttributeAlias: {
		PartitionKeyAlias: "CMNT#id"
		SortKeyAlias:      "FIN#finding_id"
	}
	NonKeyAttributes: [
		"finding_id",
		"full_name",
		"comment_type",
		"parent_id",
		"id",
		"creation_date",
		"content",
		"email",
	]
	DataAccess: {
		MySql: {}
	}
}

FindingComment: TableData: [
	{
		finding_id:    "457497318"
		full_name:     "user Integrates"
		comment_type:  "COMMENT"
		parent_id:     "0"
		id:            "1558048728999"
		creation_date: "2021-01-19T15:41:04+00:00"
		content:       "This is a comment."
		email:         "integratesuser@gmail.com"
		sk:            "FIN#" + finding_id
		pk:            "CMNT#" + id
	},
	{
		finding_id:    "457497318"
		full_name:     "hacker Integrates"
		comment_type:  "OBSERVATION"
		parent_id:     "0"
		id:            "1558048729999"
		creation_date: "2022-01-19T15:41:04+00:00"
		content:       "This is an observation."
		email:         "integrateshacker@fluidattacks.com"
		sk:            "FIN#" + finding_id
		pk:            "CMNT#" + id
	},
	{
		finding_id:    "463558592"
		full_name:     "user Integrates"
		comment_type:  "VERIFICATION"
		parent_id:     "0"
		id:            "1558048727999"
		creation_date: "2020-01-19T15:41:04+00:00"
		content:       "This is a commenting test of a request verification."
		email:         "integratesuser@gmail.com"
		sk:            "FIN#" + finding_id
		pk:            "CMNT#" + id
	},
	{
		finding_id:    "151213607"
		full_name:     "user Integrates"
		comment_type:  "VERIFICATION"
		parent_id:     "0"
		id:            "8617231051651"
		creation_date: "2020-01-19T15:41:04+00:00"
		content:       "This is a commenting test of a request verification."
		email:         "integratesuser@gmail.com"
		sk:            "FIN#" + finding_id
		pk:            "CMNT#" + id
	},
	{
		finding_id:    "422286126"
		full_name:     "unit test"
		comment_type:  "COMMENT"
		parent_id:     "0"
		id:            "1566336916294"
		creation_date: "2019-08-20T21:35:16+00:00"
		content:       "This is a commenting test"
		email:         "unittest@fluidattacks.com"
		sk:            "FIN#" + finding_id
		pk:            "CMNT#" + id
	},
	{
		finding_id:    "436992569"
		full_name:     "hacker Integrates"
		comment_type:  "VERIFICATION"
		parent_id:     "0"
		id:            "1558048727111"
		creation_date: "2020-02-21T15:41:04+00:00"
		content:       "this is a commenting test of a verifying request verification in vulns"
		email:         "integrateshacker@fluidattacks.com"
		sk:            "FIN#" + finding_id
		pk:            "CMNT#" + id
	},
	{
		finding_id:    "500592001"
		full_name:     "unit test"
		comment_type:  "COMMENT"
		parent_id:     "1558036062503"
		id:            "1558048727932"
		creation_date: "2019-05-16T23:18:47+00:00"
		content:       "Se cierra la vulnerabilidad."
		email:         "unittest@fluidattacks.com"
		sk:            "FIN#" + finding_id
		pk:            "CMNT#" + id
	},
	{
		finding_id:    "436992569"
		full_name:     "user Integrates"
		comment_type:  "VERIFICATION"
		parent_id:     "0"
		id:            "1558048727000"
		creation_date: "2020-02-19T15:41:04+00:00"
		content:       "This is a commenting test of a request verification in vulns"
		email:         "integratesuser@gmail.com"
		sk:            "FIN#" + finding_id
		pk:            "CMNT#" + id
	},
] & [...#FindingCommentItem]
