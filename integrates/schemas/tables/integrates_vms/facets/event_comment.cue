package event_comment

import (
	"fluidattacks.com/types/events"
)

#eventCommentPk: =~"^CMNT#[0-9]+$"
#eventCommentSk: =~"^EVENT#[0-9]+#GROUP#[a-zA-Z0-9]+$"

#EventCommentKeys: {
	pk: string & #eventCommentPk
	sk: string & #eventCommentSk
}

#EventCommentItem: {
	events.#EventComment
	#EventCommentKeys
}

EventComment:
{
	FacetName: "event_comment"
	KeyAttributeAlias: {
		PartitionKeyAlias: "CMNT#id"
		SortKeyAlias:      "EVENT#event_id#GROUP#group_name"
	}
	NonKeyAttributes: [
		for k, _ in events.#EventComment {k},
	]
	DataAccess: {
		MySql: {}
	}
}

EventComment: TableData: [
	{
		full_name:     "user Integrates"
		event_id:      "418900971"
		group_name:    "unittesting"
		parent_id:     "0"
		sk:            "EVENT#418900971#GROUP#unittesting"
		pk:            "CMNT#1677548565934"
		id:            "1677548565934"
		creation_date: "2023-01-23T15:00:00+00:00"
		email:         "integratesuser@gmail.com"
		content:       "Testing event comment."
	},
] & [...#EventCommentItem]
