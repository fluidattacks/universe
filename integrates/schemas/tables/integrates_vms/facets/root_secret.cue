package root_secret

import (
	"fluidattacks.com/types/roots"
)

#rootSecretPk: =~"^ROOT#[a-f0-9-]{36}$"
#rootSecretSk: =~"^SECRET#[A-z0-9_]+$"

#RootSecretKeys: {
	pk: string & #rootSecretPk
	sk: string & #rootSecretSk
}

#RootSecretItem: {
	#RootSecretKeys
	roots.#RootSecret
}

RootSecret:
{
	FacetName: "root_secret"
	KeyAttributeAlias: {
		PartitionKeyAlias: "ROOT#uuid"
		SortKeyAlias:      "SECRET#key"
	}
	NonKeyAttributes: [
		"created_at",
		"key",
		"value",
		"state",
	]
	DataAccess: {
		MySql: {}
	}
}

RootSecret: TableData: [
	{
		pk:         "ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19"
		sk:         "SECRET#username"
		key:        "username"
		value:      "jane_doe"
		created_at: "2023-06-22T17:51:53.312099+00:00"
		state: {
			owner:         "jdoe@testcompany.com"
			description:   "User name for user management"
			modified_by:   "jdoe@testcompany.com"
			modified_date: "2023-07-22T17:51:53.312099+00:00"
		}
	},
] & [...#RootSecretItem]
