package group_historic_policies

import (
	"fluidattacks.com/types/groups"
)

#groupHistoricPoliciesPk: =~"^GROUP#[a-z]+$"
#groupHistoricPoliciesSk: =~"^POLICIES#[0-9TZ:+.-]+$"

#GroupHistoricPoliciesKeys: {
	pk: string & #groupHistoricPoliciesPk
	sk: string & #groupHistoricPoliciesSk
}

#GroupHistoricPoliciesItem: {
	#GroupHistoricPoliciesKeys
	groups.#GroupPolicies
}

GroupHistoricPolicies:
{
	FacetName: "group_historic_policies"
	KeyAttributeAlias: {
		PartitionKeyAlias: "GROUP#name"
		SortKeyAlias:      "POLICIES#iso8601utc"
	}
	NonKeyAttributes: [
		"modified_date",
		"modified_by",
		"max_acceptance_days",
		"max_acceptance_severity",
		"max_number_acceptances",
		"min_acceptance_severity",
		"min_breaking_severity",
		"inactivity_period",
		"vulnerability_grace_period",
	]
	DataAccess: {
		MySql: {}
	}
}

GroupHistoricPolicies: TableData: [
	{
		max_number_acceptances:     3
		min_acceptance_severity:    0.0
		vulnerability_grace_period: 0
		sk:                         "POLICIES#2021-11-22T20:07:57+00:00"
		modified_by:                "integratesmanager@gmail.com"
		min_breaking_severity:      0.0
		max_acceptance_days:        60
		pk:                         "GROUP#oneshottest"
		modified_date:              "2021-11-22T20:07:57+00:00"
		max_acceptance_severity:    3.9
	},
] & [...#GroupHistoricPoliciesItem]
