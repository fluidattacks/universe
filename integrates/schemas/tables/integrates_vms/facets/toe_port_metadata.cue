package toe_port_metadata

import (
	"fluidattacks.com/types/toe_ports"
)

#toePortMetadataPk: =~"^GROUP#[a-z]+$"
#toePortMetadataSk: =~"^PORTS#ROOT#[a-f0-9-]{36}#ADDRESS#[0-9.]+#PORT#[0-9]{1,5}$"

#ToePortMetadataKeys: {
	pk:   string & #toePortMetadataPk
	sk:   string & #toePortMetadataSk
	pk_2: string
	sk_2: string
}

#ToePortItem: {
	#ToePortMetadataKeys
	toe_ports.#ToePortMetadata
}

ToePortMetadata:
{
	FacetName: "toe_port_metadata"
	KeyAttributeAlias: {
		PartitionKeyAlias: "GROUP#group_name"
		SortKeyAlias:      "PORTS#ROOT#root_id#ADDRESS#address#PORT#port"
	}
	NonKeyAttributes: [
		"address",
		"port",
		"group_name",
		"root_id",
		"state",
		"pk_2",
		"sk_2",
	]
	DataAccess: {
		MySql: {}
	}
}

ToePortMetadata: TableData: [
	{
		address:    "127.0.0.1"
		group_name: "oneshottest"
		port:       "110"
		sk:         "PORTS#ROOT#d312f0b9-da49-4d2b-a881-bed438875e99#ADDRESS#127.0.0.1#PORT#110"
		pk_2:       "GROUP#oneshottest"
		root_id:    "d312f0b9-da49-4d2b-a881-bed438875e99"
		pk:         "GROUP#oneshottest"
		state: {
			has_vulnerabilities: false
			modified_by:         "test2@test.com"
			attacked_at:         "2022-12-15T23:26:15.366560+00:00"
			be_present:          true
			first_attack_at:     "2022-12-15T23:26:15.366560+00:00"
			modified_date:       "2022-12-15T23:26:15.366618+00:00"
			attacked_by:         "test2@test.com"
			seen_first_time_by:  "test2@test.com"
			seen_at:             "2022-12-15T23:17:51.060916+00:00"
		}
		sk_2: "PORTS#PRESENT#true#ROOT#d312f0b9-da49-4d2b-a881-bed438875e99#ADDRESS#127.0.0.1#PORT#110"
	},
	{
		address:    "127.0.0.1"
		group_name: "oneshottest"
		port:       "21"
		sk:         "PORTS#ROOT#d312f0b9-da49-4d2b-a881-bed438875e99#ADDRESS#127.0.0.1#PORT#21"
		pk_2:       "GROUP#oneshottest"
		root_id:    "d312f0b9-da49-4d2b-a881-bed438875e99"
		pk:         "GROUP#oneshottest"
		state: {
			has_vulnerabilities: false
			modified_by:         "test2@test.com"
			attacked_at:         "2022-12-15T23:26:15.371462+00:00"
			be_present:          true
			first_attack_at:     "2022-12-15T23:26:15.371462+00:00"
			modified_date:       "2022-12-15T23:26:15.371483+00:00"
			attacked_by:         "test2@test.com"
			seen_first_time_by:  "test2@test.com"
			seen_at:             "2022-12-15T23:17:34.129679+00:00"
		}
		sk_2: "PORTS#PRESENT#true#ROOT#d312f0b9-da49-4d2b-a881-bed438875e99#ADDRESS#127.0.0.1#PORT#21"
	},
	{
		address:    "127.0.0.1"
		port:       "22"
		group_name: "oneshottest"
		sk:         "PORTS#ROOT#d312f0b9-da49-4d2b-a881-bed438875e99#ADDRESS#127.0.0.1#PORT#22"
		root_id:    "d312f0b9-da49-4d2b-a881-bed438875e99"
		pk_2:       "GROUP#oneshottest"
		pk:         "GROUP#oneshottest"
		state: {
			modified_by:         "test2@test.com"
			be_present:          true
			modified_date:       "2022-12-15T23:17:46.186185+00:00"
			has_vulnerabilities: false
			seen_first_time_by:  "test2@test.com"
			seen_at:             "2022-12-15T23:17:46.186174+00:00"
		}
		sk_2: "PORTS#PRESENT#true#ROOT#d312f0b9-da49-4d2b-a881-bed438875e99#ADDRESS#127.0.0.1#PORT#22"
	},
	{
		address:    "127.0.0.1"
		group_name: "oneshottest"
		port:       "443"
		sk:         "PORTS#ROOT#d312f0b9-da49-4d2b-a881-bed438875e99#ADDRESS#127.0.0.1#PORT#443"
		pk_2:       "GROUP#oneshottest"
		root_id:    "d312f0b9-da49-4d2b-a881-bed438875e99"
		pk:         "GROUP#oneshottest"
		state: {
			has_vulnerabilities: false
			modified_by:         "test2@test.com"
			attacked_at:         "2022-12-15T23:26:15.369616+00:00"
			be_present:          false
			first_attack_at:     "2022-12-15T23:26:15.369616+00:00"
			modified_date:       "2022-12-15T23:26:22.269823+00:00"
			attacked_by:         "test2@test.com"
			be_present_until:    "2022-12-15T23:26:22.269810+00:00"
			seen_first_time_by:  "test2@test.com"
			seen_at:             "2022-12-15T23:17:28.940909+00:00"
		}
		sk_2: "PORTS#PRESENT#false#ROOT#d312f0b9-da49-4d2b-a881-bed438875e99#ADDRESS#127.0.0.1#PORT#443"
	},
	{
		address:    "127.0.0.1"
		port:       "8080"
		group_name: "oneshottest"
		sk:         "PORTS#ROOT#d312f0b9-da49-4d2b-a881-bed438875e99#ADDRESS#127.0.0.1#PORT#8080"
		root_id:    "d312f0b9-da49-4d2b-a881-bed438875e99"
		pk_2:       "GROUP#oneshottest"
		pk:         "GROUP#oneshottest"
		state: {
			seen_first_time_by:  "test2@test.com"
			modified_by:         "test2@test.com"
			be_present:          true
			modified_date:       "2022-12-15T23:17:05.638952+00:00"
			has_vulnerabilities: false
			seen_at:             "2022-12-15T23:17:05.638936+00:00"
		}
		sk_2: "PORTS#PRESENT#true#ROOT#d312f0b9-da49-4d2b-a881-bed438875e99#ADDRESS#127.0.0.1#PORT#8080"
	},
	{
		address:    "127.0.0.1"
		group_name: "oneshottest"
		port:       "995"
		sk:         "PORTS#ROOT#d312f0b9-da49-4d2b-a881-bed438875e99#ADDRESS#127.0.0.1#PORT#995"
		pk_2:       "GROUP#oneshottest"
		root_id:    "d312f0b9-da49-4d2b-a881-bed438875e99"
		pk:         "GROUP#oneshottest"
		state: {
			modified_by:         "test2@test.com"
			be_present:          false
			modified_date:       "2022-12-15T23:26:23.286765+00:00"
			be_present_until:    "2022-12-15T23:26:23.286754+00:00"
			has_vulnerabilities: false
			seen_first_time_by:  "test2@test.com"
			seen_at:             "2022-12-15T23:17:55.869123+00:00"
		}
		sk_2: "PORTS#PRESENT#false#ROOT#d312f0b9-da49-4d2b-a881-bed438875e99#ADDRESS#127.0.0.1#PORT#995"
	},
] & [...#ToePortItem]
