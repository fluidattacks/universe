package toe_lines_metadata

import (
	"fluidattacks.com/types/toe_lines"
)

#toeLinesMetadataPk: =~"^GROUP#[a-z]+$"
#toeLinesMetadataSk: =~"^LINES#ROOT#[a-f0-9-]{36}#FILENAME#[^\\:\\*\\?\"<>\\|]+$"

#ToeLinesMetadataKeys: {
	pk:   string & #toeLinesMetadataPk
	sk:   string & #toeLinesMetadataSk
	pk_2: string
	sk_2: string
}

#ToeLinesItem: {
	#ToeLinesMetadataKeys
	toe_lines.#ToeLinesMetadata
}

ToeLinesMetadata:
{
	FacetName: "toe_lines_metadata"
	KeyAttributeAlias: {
		PartitionKeyAlias: "GROUP#group_name"
		SortKeyAlias:      "LINES#ROOT#root_id#FILENAME#filename"
	}
	NonKeyAttributes: [
		"seen_first_time_by",
		"group_name",
		"filename",
		"root_id",
		"pk_2",
		"sk_2",
		"state",
		"attacked_at",
		"attacked_by",
		"attacked_lines",
		"be_present",
		"be_present_until",
		"comments",
		"first_attack_at",
		"has_vulnerabilities",
		"last_author",
		"last_commit",
		"last_commit_date",
		"loc",
		"modified_by",
		"modified_date",
		"seen_at",
		"sorts_risk_level",
		"sorts_priority_factor",
		"sorts_risk_level_date",
		"sorts_suggestions",
	]
	DataAccess: {
		MySql: {}
	}
}

ToeLinesMetadata: TableData: [
	{
		pk:         "GROUP#unittesting"
		sk:         "LINES#ROOT#9eccb9ed-e835-4dbc-b28e-8cecd0296e27#FILENAME#foo_file.py"
		pk_2:       "GROUP#unittesting"
		sk_2:       "LINES#PRESENT#false#ROOT#9eccb9ed-e835-4dbc-b28e-8cecd0296e27#FILENAME#foo_file.py"
		group_name: "unittesting"
		filename:   "foo_file.py"
		root_id:    "9eccb9ed-e835-4dbc-b28e-8cecd0296e27"
		state: {
			loc:                   500
			attacked_lines:        500
			comments:              "no comments"
			sorts_risk_level:      50
			sorts_priority_factor: 40
			last_commit:           "293abe78145b8f097f1709635527faf9b4ea69b2"
			has_vulnerabilities:   true
			be_present:            true
			last_author:           "user@gmail.com"
			attacked_by:           "test2@test.com"
			modified_by:           "test2@test.com"
			first_attack_at:       "2020-02-19T15:41:04+00:00"
			sorts_risk_level_date: "2024-03-30T05:00:00+00:00"
			last_commit_date:      "2024-11-14T18:00:00+00:00"
			seen_at:               "2024-11-15T18:00:00+00:00"
			attacked_at:           "2024-11-15T18:00:00+00:00"
			modified_date:         "2024-11-16T18:00:00+00:00"
			sorts_suggestions:
			[
				{
					finding_title: "083. XML injection (XXE)"
					probability:   90
				},
			]
		}
	},
	{
		pk:         "GROUP#unittesting"
		sk:         "LINES#ROOT#9eccb9ed-e835-4dbc-b28e-8cecd0296e27#FILENAME#foo_file2.py"
		pk_2:       "GROUP#unittesting"
		sk_2:       "LINES#PRESENT#false#ROOT#9eccb9ed-e835-4dbc-b28e-8cecd0296e27#FILENAME#foo_file2.py"
		group_name: "unittesting"
		filename:   "foo_file2.py"
		root_id:    "9eccb9ed-e835-4dbc-b28e-8cecd0296e27"
		state: {
			loc:                   500
			attacked_lines:        500
			comments:              "no comments"
			sorts_risk_level:      50
			sorts_priority_factor: 40
			last_commit:           "293abe78145b8f097f1709635527faf9b4ea69b2"
			has_vulnerabilities:   true
			be_present:            true
			last_author:           "user@gmail.com"
			attacked_by:           "test2@test.com"
			modified_by:           "test2@test.com"
			first_attack_at:       "2020-02-19T15:41:04+00:00"
			sorts_risk_level_date: "2024-03-30T05:00:00+00:00"
			last_commit_date:      "2024-11-14T18:00:00+00:00"
			seen_at:               "2024-11-15T18:00:00+00:00"
			attacked_at:           "2024-11-15T18:00:00+00:00"
			modified_date:         "2024-11-16T18:00:00+00:00"
			sorts_suggestions:
			[
				{
					finding_title: "083. XML injection (XXE)"
					probability:   90
				},
			]
		}
	},
	{
		pk:         "GROUP#unittesting"
		sk:         "LINES#ROOT#9eccb9ed-e835-4dbc-b28e-8cecd0296e27#FILENAME#main.py"
		pk_2:       "GROUP#unittesting"
		sk_2:       "LINES#PRESENT#false#ROOT#9eccb9ed-e835-4dbc-b28e-8cecd0296e27#FILENAME#main.py"
		group_name: "unittesting"
		filename:   "main.py"
		root_id:    "9eccb9ed-e835-4dbc-b28e-8cecd0296e27"
		state: {
			loc:                   500
			attacked_lines:        500
			comments:              "no comments"
			sorts_risk_level:      50
			sorts_priority_factor: 40
			last_commit:           "293abe78145b8f097f1709635527faf9b4ea69b2"
			has_vulnerabilities:   true
			be_present:            true
			last_author:           "user@gmail.com"
			attacked_by:           "test2@test.com"
			modified_by:           "test2@test.com"
			first_attack_at:       "2020-02-19T15:41:04+00:00"
			sorts_risk_level_date: "2024-03-30T05:00:00+00:00"
			last_commit_date:      "2024-11-14T18:00:00+00:00"
			seen_at:               "2024-11-15T18:00:00+00:00"
			attacked_at:           "2024-11-15T18:00:00+00:00"
			modified_date:         "2024-11-16T18:00:00+00:00"
			sorts_suggestions:
			[
				{
					finding_title: "083. XML injection (XXE)"
					probability:   90
				},
			]
		}
	},
	{
		pk:         "GROUP#unittesting"
		sk:         "LINES#ROOT#9eccb9ed-e835-4dbc-b28e-8cecd0296e27#FILENAME#lib/foo_file.py"
		pk_2:       "GROUP#unittesting"
		sk_2:       "LINES#PRESENT#false#ROOT#9eccb9ed-e835-4dbc-b28e-8cecd0296e27#FILENAME#lib/foo_file.py"
		group_name: "unittesting"
		filename:   "lib/foo_file.py"
		root_id:    "9eccb9ed-e835-4dbc-b28e-8cecd0296e27"
		state: {
			loc:                   500
			attacked_lines:        500
			comments:              "no comments"
			sorts_risk_level:      50
			sorts_priority_factor: 40
			last_commit:           "293abe78145b8f097f1709635527faf9b4ea69b2"
			has_vulnerabilities:   true
			be_present:            true
			last_author:           "user@gmail.com"
			attacked_by:           "test2@test.com"
			modified_by:           "test2@test.com"
			first_attack_at:       "2020-02-19T15:41:04+00:00"
			sorts_risk_level_date: "2024-03-30T05:00:00+00:00"
			last_commit_date:      "2024-11-14T18:00:00+00:00"
			seen_at:               "2024-11-15T18:00:00+00:00"
			attacked_at:           "2024-11-15T18:00:00+00:00"
			modified_date:         "2024-11-16T18:00:00+00:00"
			sorts_suggestions:
			[
				{
					finding_title: "083. XML injection (XXE)"
					probability:   90
				},
			]
		}
	},
	{
		group_name: "unittesting"
		filename:   "test/test#.config"
		sk:         "LINES#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19#FILENAME#test/test#.config"
		pk_2:       "GROUP#unittesting"
		root_id:    "4039d098-ffc5-4984-8ed3-eb17bca98e19"
		pk:         "GROUP#unittesting"
		sk_2:       "LINES#PRESENT#true#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19#FILENAME#test/test#.config"
		state: {
			loc:                   180
			comments:              "comment 1"
			sorts_risk_level:      80
			sorts_priority_factor: 70
			sorts_risk_level_date: "2021-02-20T05:00:00+00:00"
			last_commit:           "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c2"
			last_commit_date:      "2020-11-15T15:41:04+00:00"
			last_author:           "user@gmail.com"
			has_vulnerabilities:   true
			attacked_at:           "2021-02-20T05:00:00+00:00"
			be_present:            true
			attacked_by:           "test2@test.com"
			attacked_lines:        4
			first_attack_at:       "2020-02-19T15:41:04+00:00"
			modified_by:           "test2@test.com"
			modified_date:         "2020-11-15T15:41:04+00:00"
			seen_at:               "2020-02-01T15:41:04+00:00"
			sorts_suggestions:
			[
				{
					finding_title: "083. XML injection (XXE)"
					probability:   90
				},
				{
					finding_title: "033. Password change without identity check"
					probability:   50
				},
			]

		}
	},
	{
		group_name: "unittesting"
		filename:   "universe/path/to/file3.ext"
		sk:         "LINES#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19#FILENAME#universe/path/to/file3.ext"
		pk_2:       "GROUP#unittesting"
		root_id:    "4039d098-ffc5-4984-8ed3-eb17bca98e19"
		pk:         "GROUP#unittesting"
		sk_2:       "LINES#PRESENT#true#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19#FILENAME#universe/path/to/file3.ext"
		state: {
			loc:                   350
			comments:              "comment 1"
			sorts_risk_level:      80
			sorts_priority_factor: 70
			sorts_risk_level_date: "2021-03-30T05:00:00+00:00"
			last_commit:           "e17059d1e17059d1e17059d1e17059d1e17059d1"
			last_commit_date:      "2020-11-15T15:41:04+00:00"
			last_author:           "user@gmail.com"
			has_vulnerabilities:   false
			attacked_at:           "2021-02-20T05:00:00+00:00"
			be_present:            true
			attacked_by:           "test2@test.com"
			attacked_lines:        4
			first_attack_at:       "2020-02-19T15:41:04+00:00"
			modified_by:           "test2@test.com"
			modified_date:         "2020-11-15T15:41:04+00:00"
			seen_at:               "2020-02-01T15:41:04+00:00"
			sorts_suggestions:
			[
				{
					finding_title: "083. XML injection (XXE)"
					probability:   90
				},
				{
					finding_title: "033. Password change without identity check"
					probability:   50
				},
			]

		}
	},
	{
		group_name: "unittesting"
		filename:   "path/to/test1.py"
		sk:         "LINES#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19#FILENAME#path/to/test1.py"
		pk_2:       "GROUP#unittesting"
		root_id:    "4039d098-ffc5-4984-8ed3-eb17bca98e19"
		pk:         "GROUP#unittesting"
		sk_2:       "LINES#PRESENT#false#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19#FILENAME#path/to/test1.py"
		state: {
			loc:                   500
			comments:              "comment 3"
			sorts_risk_level:      50
			sorts_priority_factor: 40
			sorts_risk_level_date: "2021-03-30T05:00:00+00:00"
			last_commit:           "e17059d1e17059d1e17059d1e17059d1e17059d1"
			last_commit_date:      "2020-11-15T15:41:04+00:00"
			last_author:           "user@gmail.com"
			has_vulnerabilities:   false
			attacked_at:           "2021-02-20T05:00:00+00:00"
			be_present:            false
			attacked_by:           "test2@test.com"
			attacked_lines:        500
			first_attack_at:       "2020-02-19T15:41:04+00:00"
			modified_by:           "test2@test.com"
			modified_date:         "2020-11-15T15:41:04+00:00"
			seen_at:               "2020-02-01T15:41:04+00:00"
			sorts_suggestions:
			[
				{
					finding_title: "083. XML injection (XXE)"
					probability:   90
				},
			]

		}
	},
	{
		group_name: "unittesting"
		filename:   "path/to/test2.py"
		sk:         "LINES#ROOT#765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a#FILENAME#path/to/test2.py"
		pk_2:       "GROUP#unittesting"
		root_id:    "765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a"
		pk:         "GROUP#unittesting"
		sk_2:       "LINES#PRESENT#true#ROOT#765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a#FILENAME#path/to/test2.py"
		state: {
			loc:                   100
			comments:              "comment 4"
			sorts_risk_level:      80
			sorts_priority_factor: 70
			sorts_risk_level_date: "2021-03-30T05:00:00+00:00"
			last_commit:           "e17059d1e17059d1e17059d1e17059d1e17059d1"
			last_commit_date:      "2020-11-15T15:41:04+00:00"
			last_author:           "user@gmail.com"
			has_vulnerabilities:   false
			attacked_at:           "2021-02-20T05:00:00+00:00"
			be_present:            true
			attacked_by:           "test2@test.com"
			attacked_lines:        0
			first_attack_at:       "2020-02-19T15:41:04+00:00"
			modified_by:           "test2@test.com"
			modified_date:         "2020-11-15T15:41:04+00:00"
			seen_at:               "2020-02-01T15:41:04+00:00"
			sorts_suggestions:
			[
				{
					finding_title: "033. Password change without identity check"
					probability:   50
				},
			]

		}
	},
	{
		group_name: "unittesting"
		filename:   "path/to/test3.py"
		sk:         "LINES#ROOT#765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a#FILENAME#path/to/test3.py"
		pk_2:       "GROUP#unittesting"
		root_id:    "765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a"
		pk:         "GROUP#unittesting"
		sk_2:       "LINES#PRESENT#true#ROOT#765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a#FILENAME#path/to/test3.py"
		state: {
			loc:                   350
			comments:              "comment 5"
			sorts_risk_level:      100
			sorts_priority_factor: 90
			sorts_risk_level_date: "2021-03-30T05:00:00+00:00"
			last_commit:           "e17059d1e17059d1e17059d1e17059d1e17059d1"
			last_commit_date:      "2020-11-15T15:41:04+00:00"
			last_author:           "user@gmail.com"
			has_vulnerabilities:   false
			attacked_at:           "2021-02-20T05:00:00+00:00"
			be_present:            true
			attacked_by:           "test2@test.com"
			attacked_lines:        50
			first_attack_at:       "2020-02-19T15:41:04+00:00"
			modified_by:           "test2@test.com"
			modified_date:         "2020-11-15T15:41:04+00:00"
			seen_at:               "2020-02-01T15:41:04+00:00"
			sorts_suggestions:
			[
				{
					finding_title: "027. Insecure file upload"
					probability:   100
				},
			]

		}
	},
	{
		group_name: "unittesting"
		filename:   "path/to/test4.py"
		sk:         "LINES#ROOT#765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a#FILENAME#path/to/test4.py"
		pk_2:       "GROUP#unittesting"
		root_id:    "765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a"
		pk:         "GROUP#unittesting"
		sk_2:       "LINES#PRESENT#true#ROOT#765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a#FILENAME#path/to/test4.py"
		state: {
			loc:                   34
			comments:              "comment 6"
			sorts_risk_level:      80
			sorts_priority_factor: 70
			sorts_risk_level_date: "2021-03-30T05:00:00+00:00"
			last_commit:           "e17059d1e17059d1e17059d1e17059d1e17059d1"
			last_commit_date:      "2020-11-15T15:41:04+00:00"
			last_author:           "user@gmail.com"
			has_vulnerabilities:   false
			attacked_at:           "2021-02-20T05:00:00+00:00"
			be_present:            true
			attacked_by:           "test2@test.com"
			attacked_lines:        10
			first_attack_at:       "2020-02-19T15:41:04+00:00"
			modified_by:           "test2@test.com"
			modified_date:         "2020-11-15T15:41:04+00:00"
			seen_at:               "2020-02-01T15:41:04+00:00"
			sorts_suggestions:
			[
				{
					finding_title: "083. XML injection (XXE)"
					probability:   90
				},
				{
					finding_title: "033. Password change without identity check"
					probability:   50
				},
			]

		}
	},
	{
		group_name: "unittesting"
		filename:   "test2/test.sh"
		sk:         "LINES#ROOT#765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a#FILENAME#test2/test.sh"
		pk_2:       "GROUP#unittesting"
		root_id:    "765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a"
		pk:         "GROUP#unittesting"
		sk_2:       "LINES#PRESENT#false#ROOT#765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a#FILENAME#test2/test.sh"
		state: {
			loc:                   172
			comments:              "comment 2"
			sorts_risk_level:      0
			sorts_priority_factor: 10
			sorts_risk_level_date: "2021-04-10T05:00:00+00:00"
			last_commit:           "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c1"
			last_commit_date:      "2020-11-16T15:41:04+00:00"
			last_author:           "user@gmail.com"
			has_vulnerabilities:   false
			attacked_at:           "2021-01-20T05:00:00+00:00"
			be_present:            false
			attacked_by:           "test@test.com"
			be_present_until:      "2021-01-01T15:41:04+00:00"
			attacked_lines:        120
			first_attack_at:       "2020-01-19T15:41:04+00:00"
			modified_by:           "test2@test.com"
			modified_date:         "2020-11-16T15:41:04+00:00"
			seen_at:               "2020-01-01T15:41:04+00:00"
			sorts_suggestions:
			[
				{
					finding_title: "027. Insecure file upload"
					probability:   100
				},
			]

		}
	},
	{
		group_name: "barranquilla"
		filename:   "path/to/test8.py"
		sk:         "LINES#ROOT#2e349e7f-390e-40e9-a8b6-2983eaee92ff#FILENAME#path/to/test8.py"
		pk_2:       "GROUP#barranquilla"
		root_id:    "2e349e7f-390e-40e9-a8b6-2983eaee92ff"
		pk:         "GROUP#barranquilla"
		sk_2:       "LINES#PRESENT#false#ROOT#2e349e7f-390e-40e9-a8b6-2983eaee92ff#FILENAME#path/to/test8.py"
		state: {
			loc:                   540
			comments:              "comment 10"
			sorts_risk_level:      28
			sorts_priority_factor: 18
			sorts_risk_level_date: "2021-04-10T05:00:00+00:00"
			last_commit:           "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c1"
			last_commit_date:      "2020-11-16T15:41:04+00:00"
			last_author:           "user@gmail.com"
			has_vulnerabilities:   false
			attacked_at:           "2021-01-20T05:00:00+00:00"
			be_present:            false
			attacked_by:           "test@test.com"
			be_present_until:      "2021-01-01T15:41:04+00:00"
			attacked_lines:        0
			first_attack_at:       "2020-01-19T15:41:04+00:00"
			modified_by:           "test2@test.com"
			modified_date:         "2020-11-16T15:41:04+00:00"
			seen_at:               "2020-01-01T15:41:04+00:00"
			sorts_suggestions:
			[
				{
					finding_title: "083. XML injection (XXE)"
					probability:   90
				},
				{
					finding_title: "033. Password change without identity check"
					probability:   50
				},
			]

		}
	},
	{
		group_name: "barranquilla"
		filename:   "path/to/test9.py"
		sk:         "LINES#ROOT#2e349e7f-390e-40e9-a8b6-2983eaee92ff#FILENAME#path/to/test9.py"
		pk_2:       "GROUP#barranquilla"
		root_id:    "2e349e7f-390e-40e9-a8b6-2983eaee92ff"
		pk:         "GROUP#barranquilla"
		sk_2:       "LINES#PRESENT#true#ROOT#2e349e7f-390e-40e9-a8b6-2983eaee92ff#FILENAME#path/to/test9.py"
		state: {
			loc:                   37
			comments:              "comment 11"
			sorts_risk_level:      78
			sorts_priority_factor: 68
			sorts_risk_level_date: "2021-04-10T05:00:00+00:00"
			last_commit:           "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c1"
			last_commit_date:      "2020-11-16T15:41:04+00:00"
			last_author:           "user@gmail.com"
			has_vulnerabilities:   false
			attacked_at:           "2021-01-20T05:00:00+00:00"
			be_present:            true
			attacked_by:           "test@test.com"
			be_present_until:      "2021-01-01T15:41:04+00:00"
			attacked_lines:        12
			first_attack_at:       "2020-01-19T15:41:04+00:00"
			modified_by:           "test2@test.com"
			modified_date:         "2020-11-16T15:41:04+00:00"
			seen_at:               "2020-01-01T15:41:04+00:00"
			sorts_suggestions:
			[
				{
					finding_title: "027. Insecure file upload"
					probability:   100
				},
			]

		}
	},
	{
		group_name: "barranquilla"
		filename:   "path/to/test10.py"
		sk:         "LINES#ROOT#2e349e7f-390e-40e9-a8b6-2983eaee92ff#FILENAME#path/to/test10.py"
		pk_2:       "GROUP#barranquilla"
		root_id:    "2e349e7f-390e-40e9-a8b6-2983eaee92ff"
		pk:         "GROUP#barranquilla"
		sk_2:       "LINES#PRESENT#true#ROOT#2e349e7f-390e-40e9-a8b6-2983eaee92ff#FILENAME#path/to/test10.py"
		state: {
			loc:                   8
			comments:              "comment 12"
			sorts_risk_level:      55
			sorts_priority_factor: 45
			sorts_risk_level_date: "2021-04-10T05:00:00+00:00"
			last_commit:           "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c1"
			last_commit_date:      "2020-11-16T15:41:04+00:00"
			last_author:           "user@gmail.com"
			has_vulnerabilities:   false
			attacked_at:           "2021-01-20T05:00:00+00:00"
			be_present:            true
			attacked_by:           "test@test.com"
			be_present_until:      "2021-01-01T15:41:04+00:00"
			attacked_lines:        0
			first_attack_at:       "2020-01-19T15:41:04+00:00"
			modified_by:           "test2@test.com"
			modified_date:         "2020-11-16T15:41:04+00:00"
			seen_at:               "2020-01-01T15:41:04+00:00"
			sorts_suggestions:
			[
				{
					finding_title: "033. Password change without identity check"
					probability:   50
				},
			]

		}
	},
	{
		group_name: "barranquilla"
		filename:   "path/to/test11.py"
		sk:         "LINES#ROOT#2e349e7f-390e-40e9-a8b6-2983eaee92ff#FILENAME#path/to/test11.py"
		pk_2:       "GROUP#barranquilla"
		root_id:    "2e349e7f-390e-40e9-a8b6-2983eaee92ff"
		pk:         "GROUP#barranquilla"
		sk_2:       "LINES#PRESENT#true#ROOT#2e349e7f-390e-40e9-a8b6-2983eaee92ff#FILENAME#path/to/test11.py"
		state: {
			loc:                   345
			comments:              "comment 13"
			sorts_risk_level:      75
			sorts_priority_factor: 65
			sorts_risk_level_date: "2021-04-10T05:00:00+00:00"
			last_commit:           "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c1"
			last_commit_date:      "2020-11-16T15:41:04+00:00"
			last_author:           "user@gmail.com"
			has_vulnerabilities:   false
			attacked_at:           "2021-01-20T05:00:00+00:00"
			be_present:            true
			attacked_by:           "test@test.com"
			be_present_until:      "2021-01-01T15:41:04+00:00"
			attacked_lines:        345
			first_attack_at:       "2020-01-19T15:41:04+00:00"
			modified_by:           "test2@test.com"
			modified_date:         "2020-11-16T15:41:04+00:00"
			seen_at:               "2020-01-01T15:41:04+00:00"
			sorts_suggestions:
			[
				{
					finding_title: "083. XML injection (XXE)"
					probability:   90
				},
			]

		}
	},
	{
		group_name: "barranquilla"
		filename:   "path/to/test12.py"
		sk:         "LINES#ROOT#2e349e7f-390e-40e9-a8b6-2983eaee92ff#FILENAME#path/to/test12.py"
		pk_2:       "GROUP#barranquilla"
		root_id:    "2e349e7f-390e-40e9-a8b6-2983eaee92ff"
		pk:         "GROUP#barranquilla"
		sk_2:       "LINES#PRESENT#true#ROOT#2e349e7f-390e-40e9-a8b6-2983eaee92ff#FILENAME#path/to/test12.py"
		state: {
			loc:                   982
			comments:              "comment 14"
			sorts_risk_level:      100
			sorts_priority_factor: 90
			sorts_risk_level_date: "2021-04-10T05:00:00+00:00"
			last_commit:           "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c1"
			last_commit_date:      "2020-11-16T15:41:04+00:00"
			last_author:           "user@gmail.com"
			has_vulnerabilities:   false
			attacked_at:           "2021-01-20T05:00:00+00:00"
			be_present:            true
			attacked_by:           "test@test.com"
			be_present_until:      "2021-01-01T15:41:04+00:00"
			attacked_lines:        844
			first_attack_at:       "2020-01-19T15:41:04+00:00"
			modified_by:           "test2@test.com"
			modified_date:         "2020-11-16T15:41:04+00:00"
			seen_at:               "2020-01-01T15:41:04+00:00"
			sorts_suggestions:
			[
				{
					finding_title: "083. XML injection (XXE)"
					probability:   90
				},
				{
					finding_title: "033. Password change without identity check"
					probability:   50
				},
			]

		}
	},
	{
		group_name: "barranquilla"
		filename:   "path/to/test13.py"
		sk:         "LINES#ROOT#2e349e7f-390e-40e9-a8b6-2983eaee92ff#FILENAME#path/to/test13.py"
		pk_2:       "GROUP#barranquilla"
		root_id:    "2e349e7f-390e-40e9-a8b6-2983eaee92ff"
		pk:         "GROUP#barranquilla"
		sk_2:       "LINES#PRESENT#true#ROOT#2e349e7f-390e-40e9-a8b6-2983eaee92ff#FILENAME#path/to/test13.py"
		state: {
			loc:                   53
			comments:              "comment 15"
			sorts_risk_level:      92
			sorts_priority_factor: 82
			sorts_risk_level_date: "2021-04-10T05:00:00+00:00"
			last_commit:           "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c1"
			last_commit_date:      "2020-11-16T15:41:04+00:00"
			last_author:           "user@gmail.com"
			has_vulnerabilities:   false
			attacked_at:           "2021-01-20T05:00:00+00:00"
			be_present:            true
			attacked_by:           "test@test.com"
			be_present_until:      "2021-01-01T15:41:04+00:00"
			attacked_lines:        53
			first_attack_at:       "2020-01-19T15:41:04+00:00"
			modified_by:           "test2@test.com"
			modified_date:         "2020-11-16T15:41:04+00:00"
			seen_at:               "2020-01-01T15:41:04+00:00"
			sorts_suggestions:
			[
				{
					finding_title: "083. XML injection (XXE)"
					probability:   90
				},
				{
					finding_title: "033. Password change without identity check"
					probability:   50
				},
			]

		}
	},
	{
		group_name: "barranquilla"
		filename:   "path/to/test14.py"
		sk:         "LINES#ROOT#2e349e7f-390e-40e9-a8b6-2983eaee92ff#FILENAME#path/to/test14.py"
		pk_2:       "GROUP#barranquilla"
		root_id:    "2e349e7f-390e-40e9-a8b6-2983eaee92ff"
		pk:         "GROUP#barranquilla"
		sk_2:       "LINES#PRESENT#true#ROOT#2e349e7f-390e-40e9-a8b6-2983eaee92ff#FILENAME#path/to/test14.py"
		state: {
			loc:                   75
			comments:              "comment 16"
			sorts_risk_level:      30
			sorts_priority_factor: 20
			sorts_risk_level_date: "2021-04-10T05:00:00+00:00"
			last_commit:           "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c1"
			last_commit_date:      "2020-11-16T15:41:04+00:00"
			last_author:           "user@gmail.com"
			has_vulnerabilities:   false
			attacked_at:           "2021-01-20T05:00:00+00:00"
			be_present:            true
			attacked_by:           "test@test.com"
			be_present_until:      "2021-01-01T15:41:04+00:00"
			attacked_lines:        70
			first_attack_at:       "2020-01-19T15:41:04+00:00"
			modified_by:           "test2@test.com"
			modified_date:         "2020-11-16T15:41:04+00:00"
			seen_at:               "2020-01-01T15:41:04+00:00"
			sorts_suggestions:
			[
				{
					finding_title: "027. Insecure file upload"
					probability:   100
				},
			]

		}
	},
	{
		group_name: "barranquilla"
		filename:   "path/to/test15.py"
		sk:         "LINES#ROOT#2e349e7f-390e-40e9-a8b6-2983eaee92ff#FILENAME#path/to/test15.py"
		pk_2:       "GROUP#barranquilla"
		root_id:    "2e349e7f-390e-40e9-a8b6-2983eaee92ff"
		pk:         "GROUP#barranquilla"
		sk_2:       "LINES#PRESENT#true#ROOT#2e349e7f-390e-40e9-a8b6-2983eaee92ff#FILENAME#path/to/test15.py"
		state: {
			loc:                   738
			comments:              "comment 17"
			sorts_risk_level:      23
			sorts_priority_factor: 13
			sorts_risk_level_date: "2021-04-10T05:00:00+00:00"
			last_commit:           "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c1"
			last_commit_date:      "2020-11-16T15:41:04+00:00"
			last_author:           "user@gmail.com"
			has_vulnerabilities:   false
			attacked_at:           "2021-01-20T05:00:00+00:00"
			be_present:            true
			attacked_by:           "test@test.com"
			be_present_until:      "2021-01-01T15:41:04+00:00"
			attacked_lines:        34
			first_attack_at:       "2020-01-19T15:41:04+00:00"
			modified_by:           "test2@test.com"
			modified_date:         "2020-11-16T15:41:04+00:00"
			seen_at:               "2020-01-01T15:41:04+00:00"
			sorts_suggestions:
			[
				{
					finding_title: "027. Insecure file upload"
					probability:   100
				},
			]

		}
	},
	{
		group_name: "barranquilla"
		filename:   "path/to/test16.py"
		sk:         "LINES#ROOT#2e349e7f-390e-40e9-a8b6-2983eaee92ff#FILENAME#path/to/test16.py"
		pk_2:       "GROUP#barranquilla"
		root_id:    "2e349e7f-390e-40e9-a8b6-2983eaee92ff"
		pk:         "GROUP#barranquilla"
		sk_2:       "LINES#PRESENT#true#ROOT#2e349e7f-390e-40e9-a8b6-2983eaee92ff#FILENAME#path/to/test16.py"
		state: {
			loc:                   382
			comments:              "comment 18"
			sorts_risk_level:      12
			sorts_priority_factor: 2
			sorts_risk_level_date: "2021-04-10T05:00:00+00:00"
			last_commit:           "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c1"
			last_commit_date:      "2020-11-16T15:41:04+00:00"
			last_author:           "user@gmail.com"
			has_vulnerabilities:   false
			attacked_at:           "2021-01-20T05:00:00+00:00"
			be_present:            true
			attacked_by:           "test@test.com"
			be_present_until:      "2021-01-01T15:41:04+00:00"
			attacked_lines:        19
			first_attack_at:       "2020-01-19T15:41:04+00:00"
			modified_by:           "test2@test.com"
			modified_date:         "2020-11-16T15:41:04+00:00"
			seen_at:               "2020-01-01T15:41:04+00:00"
			sorts_suggestions:
			[
				{
					finding_title: "027. Insecure file upload"
					probability:   100
				},
			]

		}
	},
	{
		group_name: "barranquilla"
		filename:   "path/to/test17.py"
		sk:         "LINES#ROOT#2e349e7f-390e-40e9-a8b6-2983eaee92ff#FILENAME#path/to/test17.py"
		pk_2:       "GROUP#barranquilla"
		root_id:    "2e349e7f-390e-40e9-a8b6-2983eaee92ff"
		pk:         "GROUP#barranquilla"
		sk_2:       "LINES#PRESENT#true#ROOT#2e349e7f-390e-40e9-a8b6-2983eaee92ff#FILENAME#path/to/test17.py"
		state: {
			loc:                   789
			comments:              "comment 19"
			sorts_risk_level:      30
			sorts_priority_factor: 20
			sorts_risk_level_date: "2021-04-10T05:00:00+00:00"
			last_commit:           "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c1"
			last_commit_date:      "2020-11-16T15:41:04+00:00"
			last_author:           "user@gmail.com"
			has_vulnerabilities:   false
			attacked_at:           "2021-01-20T05:00:00+00:00"
			be_present:            true
			attacked_by:           "test@test.com"
			be_present_until:      "2021-01-01T15:41:04+00:00"
			attacked_lines:        0
			first_attack_at:       "2020-01-19T15:41:04+00:00"
			modified_by:           "test2@test.com"
			modified_date:         "2020-11-16T15:41:04+00:00"
			seen_at:               "2020-01-01T15:41:04+00:00"
			sorts_suggestions:
			[
				{
					finding_title: "083. XML injection (XXE)"
					probability:   90
				},
			]

		}
	},
] & [...#ToeLinesItem]
