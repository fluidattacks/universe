package hook

import (
	"fluidattacks.com/bounds"
)

#HookState: {
	modified_by:   string
	modified_date: string & bounds.#isoDate
	status:        string
}

#HookMetadata: {
	name?:        string
	entry_point:  string
	id:           string
	group_name:   string
	token:        string
	token_header: string
	hook_events: {
		SS: [string]
	}
	state: #HookState
}
