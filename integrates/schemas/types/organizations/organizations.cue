package organizations

import (
	"fluidattacks.com/bounds"
	"fluidattacks.com/types"
)

#OrganizationState: {
	aws_external_id:        string
	status:                 string
	modified_by:            string
	modified_date:          string & bounds.#isoDate
	pending_deletion_date?: string & bounds.#isoDate
}

#OrganizationState: {
	aws_external_id: string
	modified_by:     string
	modified_date:   string & bounds.#isoDate
	status:          string
}

#OrganizationPriorityPolicy: {
	policy: string
	value:  int
}

#OrganizationBilling: {
	billing_email:    string
	last_modified_by: string
	customer_email:   string
	customer_id:      string
	modified_at:      string & bounds.#isoDate
	billed_groups?: [...string]
	subscription_id?: int
}

#DocumentFile: {
	file_name:     string
	modified_date: string & bounds.#isoDate
}

#OrganizationDocuments: {
	rut?:    #DocumentFile
	tax_id?: #DocumentFile
}

#OrganizationPaymentMethods: {
	id:            string
	business_name: string
	email:         string
	country:       string
	state:         string
	city:          string
	documents:     #OrganizationDocuments
}

#OrganizationMetadata: {
	billing_customer?: string
	billing_information?: [...#OrganizationBilling]
	country:             string
	created_by:          string
	created_date?:       string & bounds.#isoDate
	id:                  string
	jira_associated_id?: string
	name:                string
	payment_methods?: [...#OrganizationPaymentMethods]
	policies: types.#Policies
	priority_policies?: [...#OrganizationPriorityPolicy]
	state:                     #OrganizationState
	vulnerabilities_pathfile?: string
}

#StandardCompliance: {
	standard_name:    string
	compliance_level: number
}

#OrganizationUnreliableIndicators: {
	compliance_level?:                  number
	compliance_weekly_trend?:           number
	estimated_days_to_full_compliance?: number
	missed_repositories?:               int
	covered_repositories?:              int
	missed_authors?:                    int
	covered_authors?:                   int
	has_ztna_roots?:                    bool
	standard_compliances?: [...#StandardCompliance]
}
