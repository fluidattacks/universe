package stakeholders

import (
	"fluidattacks.com/bounds"
)

#NotificationsParameters: {
	min_severity: number
}

#NotificationsPreferences: {
	email: [...string]
	sms: [...string]

	available?: [...string]
	parameters?: #NotificationsParameters
}

#TrustedDevice: {
	ip_address:        string
	device:            string
	location:          string
	browser:           string
	otp_token_jti?:    string
	last_attempt?:     string
	first_login_date?: string & bounds.#isoDate
	last_login_date?:  string & bounds.#isoDate
}

#StakeholderState: {
	modified_date?:             string & bounds.#isoDate
	modified_by?:               string
	notifications_preferences?: #NotificationsPreferences
	trusted_devices?: [...#TrustedDevice]
}

#AccessTokens: {
	id:         string
	issued_at:  int
	jti_hashed: string
	salt:       string
	name?:      string
	last_use?:  string & bounds.#isoDate
}

#StakeholderLogin: {
	modified_by:     string
	modified_date:   string & bounds.#isoDate
	expiration_time: int
	browser:         string
	country_code:    string
	device:          string
	ip_address:      string
	provider:        string
	subject:         string
}

#StakeholderPhone: {
	country_code:         string
	calling_country_code: string
	national_number:      string
}

#StakeholderSessionToken: {
	jti:   string
	state: string
}

#StakeholderTours: {
	new_group:         bool
	new_root:          bool
	new_risk_exposure: bool
	welcome:           bool
}

#StakeholderMetadata: {
	access_tokens?: [...#AccessTokens]
	aws_customer_id?:       string
	email?:                 string
	enrolled:               bool
	first_name?:            string
	is_concurrent_session?: bool
	is_registered:          bool
	jira_client_key?:       string
	last_login_date?:       string & bounds.#isoDate
	last_name?:             string
	legal_remember?:        bool
	login?:                 #StakeholderLogin
	phone?:                 #StakeholderPhone
	registration_date?:     string & bounds.#isoDate
	role?:                  string
	session_token?:         #StakeholderSessionToken
	session_key?:           string
	state?:                 #StakeholderState
	subject?:               string
	tours?:                 #StakeholderTours
}
