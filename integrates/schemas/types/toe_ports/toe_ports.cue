package toe_ports

import (
	"fluidattacks.com/bounds"
)

#ToePortState: {
	attacked_at?:        string & bounds.#isoDate
	attacked_by?:        string
	be_present:          bool
	be_present_until?:   string & bounds.#isoDate
	first_attack_at?:    string & bounds.#isoDate
	has_vulnerabilities: bool
	modified_by?:        string
	modified_date?:      string & bounds.#isoDate
	seen_at?:            string & bounds.#isoDate
	seen_first_time_by?: string
}

#ToePortMetadata: {
	address:    string
	port:       string
	group_name: string
	root_id:    string
	state:      #ToePortState
}
