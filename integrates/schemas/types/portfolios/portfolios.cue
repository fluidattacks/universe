package portfolios

#PortfolioUnreliableIndicators: {
	mean_remediate?:                   number
	max_severity?:                     number
	max_open_severity?:                number
	mean_remediate_critical_severity?: number
	last_closing_date?:                int
	mean_remediate_high_severity?:     number
	mean_remediate_low_severity?:      number
	mean_remediate_medium_severity?:   number
}

#PortfolioMetadata: {
	id:                    string
	unreliable_indicators: #PortfolioUnreliableIndicators
	organization_name:     string
	groups: {SS: [...string]}
}
