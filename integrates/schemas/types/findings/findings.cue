package findings

import (
	"fluidattacks.com/bounds"
)

#SeverityScore: {
	base_score:     number
	temporal_score: number
	cvss_v3:        string
	cvssf:          number
	cvssf_v4:       number
	threat_score:   number
	cvss_v4:        string
}

#DraftRejection:
{
	other: string
	reasons: {SS: [...string]}
	rejected_by:    string
	rejection_date: string & bounds.#isoDate
	submitted_by:   string
}

#FindingState: {
	modified_by:   string
	modified_date: string & bounds.#isoDate
	source:        string
	status:        string
	rejection?:    #DraftRejection
	justification: string
}

#FindingEvidence: {
	author_email:  string
	description:   string
	modified_date: string & bounds.#isoDate
	url:           string
	is_draft?:     bool
}

#FindingEvidences: {
	animation?:    #FindingEvidence
	evidence1?:    #FindingEvidence
	evidence2?:    #FindingEvidence
	evidence3?:    #FindingEvidence
	evidence4?:    #FindingEvidence
	evidence5?:    #FindingEvidence
	exploitation?: #FindingEvidence
	records?:      #FindingEvidence
}

#FindingTreatmentSummary: {
	accepted:           int
	accepted_undefined: int
	in_progress:        int
	untreated:          int
}

#FindingVerificationSummary: {
	requested: int
	on_hold:   int
	verified:  int
}

#FindingZeroRiskSummary: {
	confirmed: int
	rejected:  int
	requested: int
}

#FindingVulnerabilitiesSummary: {
	closed?:           int
	open?:             int
	submitted?:        int
	rejected?:         int
	open_critical?:    int
	open_high?:        int
	open_low?:         int
	open_medium?:      int
	open_critical_v3?: int
	open_high_v3?:     int
	open_low_v3?:      int
	open_medium_v3?:   int
}

#FindingUnreliableIndicators: {
	unreliable_status:            string
	unreliable_where:             string
	unreliable_zero_risk_summary: #FindingZeroRiskSummary

	max_open_epss?:                                    int
	max_open_root_criticality?:                        string
	max_open_severity_score?:                          number
	max_open_severity_score_v4?:                       number
	newest_vulnerability_report_date?:                 string & bounds.#isoDate
	oldest_vulnerability_report_date?:                 string & bounds.#isoDate
	rejected_vulnerabilities?:                         int
	submitted_vulnerabilities?:                        int
	total_open_cvssf?:                                 number
	total_open_cvssf_v4?:                              number
	total_open_priority?:                              int
	treatment_summary?:                                #FindingTreatmentSummary
	unreliable_newest_vulnerability_report_date?:      string & bounds.#isoDate
	unreliable_oldest_open_vulnerability_report_date?: string & bounds.#isoDate
	unreliable_oldest_vulnerability_report_date?:      string & bounds.#isoDate
	unreliable_total_open_cvssf?:                      number
	verification_summary?:                             #FindingVerificationSummary
	vulnerabilities_summary?:                          #FindingVulnerabilitiesSummary
}

#FindingVerification: {
	comment_id:    string
	modified_by:   string
	modified_date: string & bounds.#isoDate
	status:        string
	vulnerability_ids?: SS: [...string]
}

#FindingMetadata: {
	group_name:                string
	id:                        string
	severity_score:            #SeverityScore
	state:                     #FindingState
	title:                     string
	attack_vector_description: string
	description:               string
	evidences:                 #FindingEvidences
	recommendation:            string
	requirements:              string
	sorts:                     string
	threat:                    string
	cvss_version:              string
	unfulfilled_requirements: [...string]
	unreliable_indicators: #FindingUnreliableIndicators

	min_time_to_remediate?:     int
	creation?:                  #FindingState
	rejected_vulnerabilities?:  int
	submitted_vulnerabilities?: int
	verification?:              #FindingVerification
}
