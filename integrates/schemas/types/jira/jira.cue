package jira

import (
	"fluidattacks.com/bounds"
)

#JiraInstallState: {
	modified_by:    string
	modified_date:  string & bounds.#isoDate
	used_api_token: string
	used_jira_jwt:  string
}

#JiraSecurityInstall: {
	associated_email: string
	base_url:         string
	client_key:       string
	cloud_id:         string
	description:      string
	display_url:      string
	event_type:       string
	installation_id:  string
	key:              string
	plugins_version:  string
	product_type:     string
	public_key:       string
	server_version:   string
	shared_secret:    string
	state:            #JiraInstallState
}
