package toe_lines

import (
	"fluidattacks.com/bounds"
)

#SortsSuggestion: {
	finding_title: string
	probability:   int
}

#ToeLinesState: {
	attacked_by:            string
	attacked_lines:         int
	be_present:             bool
	comments:               string
	has_vulnerabilities:    bool
	last_author:            string
	last_commit:            string
	loc:                    int
	modified_by:            string
	modified_date:          string & bounds.#isoDate
	seen_at:                string & bounds.#isoDate
	sorts_risk_level:       int
	last_commit_date?:      string & bounds.#isoDate
	attacked_at?:           string & bounds.#isoDate
	be_present_until?:      string & bounds.#isoDate
	first_attack_at?:       string & bounds.#isoDate
	sorts_priority_factor?: int
	sorts_risk_level_date?: string & bounds.#isoDate
	sorts_suggestions?: [...#SortsSuggestion]
}

#ToeLinesMetadata: {
	group_name: string
	filename:   string
	root_id:    string
	state:      #ToeLinesState

	seen_first_time_by?: string
}
