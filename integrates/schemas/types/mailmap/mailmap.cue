package mailmap

import (
	"fluidattacks.com/bounds"
)

#MailmapEntry: {
	mailmap_entry_name:        string
	mailmap_entry_email:       string
	mailmap_entry_created_at?: string & bounds.#isoDate
	mailmap_entry_updated_at?: string & bounds.#isoDate
}

#MailmapSubentry: {
	mailmap_subentry_name:        string
	mailmap_subentry_email:       string
	mailmap_subentry_created_at?: string & bounds.#isoDate
	mailmap_subentry_updated_at?: string & bounds.#isoDate
}
