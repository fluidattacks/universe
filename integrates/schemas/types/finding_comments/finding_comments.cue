package finding_comments

import (
	"fluidattacks.com/bounds"
)

#FindingComment: {
	finding_id:    string
	comment_type:  string
	parent_id:     string
	id:            string
	creation_date: string & bounds.#isoDate
	content:       string
	email:         string

	full_name?: string
}
