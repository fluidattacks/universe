package forces

import (
	"fluidattacks.com/bounds"
)

#ExploitResult: {
	exploitability: string
	kind:           string
	state:          string
	where:          string
	who:            string
}

#ExecutionVulnerabilities: {
	num_of_accepted_vulnerabilities:       int
	num_of_open_vulnerabilities:           int
	num_of_closed_vulnerabilities:         int
	num_of_open_managed_vulnerabilities:   int
	num_of_open_unmanaged_vulnerabilities: int

	accepted?: [...#ExploitResult]
	closed?: [...#ExploitResult]
	num_of_vulnerabilities_in_accepted_exploits?:   int
	num_of_vulnerabilities_in_exploits?:            int
	num_of_vulnerabilities_in_integrates_exploits?: int
	open?: [...#ExploitResult]
}

#ForcesExecution: {
	branch:          string
	commit:          string
	execution_date:  string & bounds.#isoDate
	exit_code:       string
	group_name:      string
	id:              string
	kind:            string
	origin:          string
	repo:            string
	strictness:      string
	vulnerabilities: #ExecutionVulnerabilities

	days_until_it_breaks?: int
	grace_period?:         int
	severity_threshold?:   number
}
