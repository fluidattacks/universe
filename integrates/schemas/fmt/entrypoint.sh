# shellcheck shell=bash

function main {
  local base_args=(
    --strict
    --files
  )
  : \
    && if test -n "${CI-}"; then
      info "Running CUE fmt in check mode"
      base_args+=(
        --check
      )
    else
      info "Running CUE fmt in fix mode"
    fi \
    && pushd integrates/schemas \
    && cue fmt . "${base_args[@]}" \
    && info "Successfully linted the CUE schemas" \
    && popd \
    || return 1
}

main "${@}"
