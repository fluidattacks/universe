{ makeScript, inputs, outputs, projectPath, ... }:
makeScript {
  entrypoint = ./entrypoint.sh;
  name = "integrates-schema-val";
  replace = {
    __argIntegratesBackEnv__ = outputs."/integrates/back/env";
    __argCueSchemasBase__ = projectPath "/integrates/schemas";
  };

  searchPaths = {
    bin = [
      outputs."/common/utils/cue"
      inputs.nixpkgs.awscli
      inputs.nixpkgs.gawk
      inputs.nixpkgs.python3
    ];
    source = [ outputs."/common/utils/aws" ];
  };
}
