import json


class SupportedTypesNotFoundError(Exception):
    pass


def _transform_value(
    value: dict,
) -> (
    str | int | float | bool | dict | list
):  # pylint: disable=too-many-return-statements
    if "S" in value:
        return value["S"]
    if "N" in value:
        return int(value["N"]) if value["N"].isdigit() else float(value["N"])
    if "BOOL" in value:
        return value["BOOL"]
    if "M" in value:
        return {k: _transform_value(v) for k, v in value["M"].items()}
    if "L" in value:
        return [_transform_value(i) for i in value["L"]]
    if "SS" in value:
        return value
    if "NULL" in value:
        return "null"
    raise SupportedTypesNotFoundError(
        f"Supported types not found in: {value.keys()}"
    )


def handle_dynamodb_to_json(items: dict) -> None:
    simple_json = [
        {k: _transform_value(v) for k, v in item.items()}
        for item in items["Items"]
    ]
    print(json.dumps(simple_json, indent=2, ensure_ascii=False))
