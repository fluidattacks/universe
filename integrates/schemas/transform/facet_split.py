import argparse
from dynamodb.types import (
    Facet,
    Table,
)
from integrates.db_model import (
    HISTORIC_TABLE,
    TABLE,
)
import json
import logging
import re
import sys
from typing import (
    Optional,
)

_TARGET_TABLES = {
    "integrates_vms": TABLE,
    "integrates_vms_historic": HISTORIC_TABLE,
}

LOGGER = logging.getLogger(__name__)


def _is_key_alias(key: str) -> bool:
    return bool(re.match(r"^[A-Z]+$", key))


def _find_facet(
    facets: dict[str, Facet],
    item_pk: str,
    item_sk: str,
) -> Optional[str]:
    queried_pk_aliases = set(filter(_is_key_alias, item_pk.split("#")))
    queried_sk_aliases = set(filter(_is_key_alias, item_sk.split("#")))
    for facet_name, facet in facets.items():
        design_pk_aliases = set(
            filter(_is_key_alias, facet.pk_alias.split("#"))
        )
        design_sk_aliases = set(
            filter(_is_key_alias, facet.sk_alias.split("#"))
        )
        if (
            queried_pk_aliases == design_pk_aliases
            and queried_sk_aliases == design_sk_aliases
        ):
            return facet_name
    return None


def split_facets(table: Table, content: list[dict]) -> dict[str, list[dict]]:
    split_data = {}
    non_matching_items: list[dict] = []

    for item in content:
        facet = _find_facet(table.facets, item["pk"], item["sk"])
        if facet is None:
            non_matching_items.append(item)
            continue
        if facet not in split_data:
            split_data[facet] = [item]
        else:
            split_data[facet].append(item)
    if len(non_matching_items) > 0:
        LOGGER.info(
            "Found %d items which matched none facets.",
            len(non_matching_items),
        )
        non_matching_filename = "non_matching_items.json"
        with open(
            non_matching_filename, mode="w", encoding="utf-8"
        ) as json_file:
            json.dump(non_matching_items, json_file, indent=2)
        LOGGER.info("Saved non matching items: %s", non_matching_filename)
    else:
        LOGGER.info("All items matched facets!")
    return split_data


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--table", type=str, required=True)
    args = parser.parse_args()
    table_name = args.table
    LOGGER.info("Processing table: %s", table_name)
    input_obj = json.load(sys.stdin)
    if table_name not in _TARGET_TABLES:
        raise ValueError(f"Unknown table: {table_name}")
    data = split_facets(_TARGET_TABLES[table_name], input_obj)
    print(json.dumps(data, indent=2, ensure_ascii=False))
