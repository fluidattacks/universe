import argparse
from dynamo_to_json import (
    handle_dynamodb_to_json,
)
import json
from json_to_dynamo import (
    handle_json_to_dynamodb,
)
import sys

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--to-json",
        action="store_true",
        help="Transform from DynamoDB JSON to JSON",
    )
    parser.add_argument(
        "--to-dynamodb",
        action="store_true",
        help="Transform from JSON to DynamoDB JSON",
    )
    args = parser.parse_args()

    input_obj = json.load(sys.stdin)
    if args.to_dynamodb:
        handle_json_to_dynamodb(input_obj)
    elif args.to_json:
        handle_dynamodb_to_json(input_obj)
    else:
        parser.print_help()
        sys.exit(1)
