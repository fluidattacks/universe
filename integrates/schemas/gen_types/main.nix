{ inputs, makeScript, projectPath, ... }:
makeScript {
  name = "integrates-schemas-gen-types";
  replace.__argDbModel__ = projectPath "/integrates/back/integrates/db_model";
  entrypoint = ./entrypoint.sh;
  searchPaths.bin = [ inputs.nixpkgs.go inputs.nixpkgs.diffutils ];
}
