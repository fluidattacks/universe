package main

import (
	"fmt"

	"cuelang.org/go/cue"
)

func wrapItem(cueDef string) string {
	return fmt.Sprintf("%sItem", cueDef)
}

func wrapOptional(cueDef string, optional bool) string {
	if optional {
		return fmt.Sprintf("NotRequired[%s]", cueDef)
	}
	return cueDef
}

func wrapList(baseType string) string {
	return fmt.Sprintf("list[%s]", baseType)
}

func cueNativeToPy(kind cue.Kind) string {
	switch kind {
	case cue.IntKind:
		return "int"
	case cue.StringKind:
		return "str"
	case cue.BoolKind:
		return "bool"
	case cue.NumberKind, cue.FloatKind:
		return "Decimal"
	default:
		return "Any"
	}
}
