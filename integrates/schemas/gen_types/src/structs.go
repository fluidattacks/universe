package main

import "strings"

type GenerationPolicy struct {
	OutFileName  string
	RuffDisables []string
	ImportMaps   map[string][]string
}

type TableFacet struct {
	Table string
	Facet string
}

type ClassDetails struct {
	Content      *strings.Builder
	Dependencies []string
}

type ClassMap map[string]ClassDetails
