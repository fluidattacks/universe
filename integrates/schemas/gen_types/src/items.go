package main

import (
	"fmt"
	"os"
	"path"
	"strings"

	"cuelang.org/go/cue"
	"cuelang.org/go/cue/load"
)

const indent = "    "

func isCueStringSet(v cue.Value) bool {
	subfields, err := v.Fields()
	if err != nil {
		return false
	}
	for subfields.Next() {
		if subfields.Label() == "SS" {
			return true
		}
	}
	return false
}

func getExprVal(val cue.Value, targetKind cue.Kind) (cue.Value, bool) {
	_, values := val.Expr()
	for _, v := range values {
		if v.Kind() == targetKind {
			return v, true
		}
	}
	return val, false
}

func extractCueDef(v cue.Value) (string, error) {
	expVal, found := getExprVal(v, cue.StringKind)

	if !found {
		return "", fmt.Errorf("unsupported expression kind: %v", v)
	}
	typeName, err := expVal.String()
	if err != nil {
		return "", fmt.Errorf("error converting %v value: %v", expVal, err)
	}
	typeName = strings.ReplaceAll(typeName, "#", "")
	return wrapItem(typeName), nil
}

func processCueList(classes ClassMap, v cue.Value) string {

	if v.IncompleteKind() != cue.ListKind {
		return "Any"
	}

	listElements := v.LookupPath(cue.MakePath(cue.AnyIndex))

	if !listElements.Exists() {
		return "Any"
	}

	switch kind := listElements.IncompleteKind(); kind {
	case cue.StructKind:
		_, refPath := listElements.ReferencePath()
		typeStr := strings.ReplaceAll(refPath.String(), "#", "")
		if typeStr == "" {
			return "Item"
		}
		typeStr = wrapItem(typeStr)
		content, deps, err := processCueStruct(classes, typeStr, listElements,
			TableFacet{"", ""})
		classes[typeStr] = ClassDetails{content, deps}
		if err != nil {
			fmt.Printf("Error processing struct: %v\n", err)
			return "Any"
		}
		return typeStr
	case cue.ListKind:
		nestedSubtype := processCueList(classes, listElements)
		return fmt.Sprintf("list[%s]", nestedSubtype)
	default:
		return cueNativeToPy(kind)
	}
}

func processFieldIterator(classes ClassMap, fields *cue.Iterator,
	parentName string,
	sb *strings.Builder) (bool, string, error) {
	var typeName string
	var fieldType string
	var err error

	fieldName := fields.Label()
	fieldValue := fields.Value()
	optional := fields.IsOptional()
	isStruct := false
	structName := ""

	switch kind := fieldValue.IncompleteKind(); kind {

	case cue.StructKind:
		if isCueStringSet(fieldValue) {
			typeName := wrapOptional("set[str]", optional)
			fmt.Fprintf(sb, "%s%s: %s\n", indent, fieldName, typeName)
			return false, "", nil
		}

		typeName, err = extractCueDef(fieldValue)
		if err != nil {
			return false, "", err
		}
		content, deps, err := processCueStruct(classes, // indirect recursion
			typeName, fieldValue, TableFacet{"", ""})
		if err != nil {
			return false, "", err
		}
		updateClassMap(classes, typeName, content, deps)

		fieldType = wrapOptional(typeName, optional)
		isStruct = true
		structName = typeName

	case cue.ListKind:
		listSubType := processCueList(classes, fieldValue)
		appendToClassMapDeps(classes, parentName, []string{listSubType})
		fieldType = wrapOptional(wrapList(listSubType), optional)

	default:
		fieldType = wrapOptional(cueNativeToPy(kind), optional)
	}
	fmt.Fprintf(sb, "%s%s: %s\n", indent, fieldName, fieldType)
	return isStruct, structName, nil
}

func processCueStruct(classes ClassMap, name string,
	v cue.Value, tableFacet TableFacet) (*strings.Builder, []string, error) {

	var sb strings.Builder

	fmt.Fprintf(&sb, "\n\nclass %s(TypedDict):\n", name)
	if tableFacet.Table != "" && tableFacet.Facet != "" {
		fmt.Fprintf(&sb, "%s\"\"\"\n%sTable: %s\n%sFacet: %s\n%s\"\"\"\n\n",
			indent, indent, tableFacet.Table, indent, tableFacet.Facet, indent)
	}

	fields, _ := v.Fields(cue.All())

	fieldsLen := 0

	dependencies := make([]string, 0)

	for fields.Next() {
		fieldsLen += 1
		isStruct, structName, err := processFieldIterator(classes, fields,
			name, &sb)
		if err != nil {
			return nil, nil, fmt.Errorf("error processing field: %v", err)
		}
		if isStruct {
			dependencies = append(dependencies, structName)
		}
	}

	if fieldsLen == 0 {
		sb.Reset()
		sb.WriteString("\n\n# This type definition is too complex to be defined from cuelang-go api\n")
		sb.WriteString("# More info at: https://gitlab.com/fluidattacks/universe/-/issues/13678\n")
		fmt.Fprintf(&sb, "%s = dict[str, Any]\n", name)
	}

	return &sb, dependencies, nil
}

func processCuePath(
	classes ClassMap,
	path string, ctx *cue.Context,
	tf TableFacet) error {

	instances := load.Instances([]string{path}, nil)
	instance := ctx.BuildInstance(instances[0])

	fields, err := instance.Fields(cue.Definitions(true))
	if err != nil {
		return fmt.Errorf("error processing %v path: %v", path, err)
	}

	for fields.Next() {
		sel := fields.Selector()
		val := fields.Value()
		if val.Kind() == cue.StructKind && sel.LabelType() == cue.DefinitionLabel {
			structName := strings.Split(sel.String(), "#")[1]
			if strings.HasSuffix(structName, "Keys") {
				continue
			}
			content, deps, err := processCueStruct(classes, structName, val, tf)
			updateClassMap(classes, structName, content, deps)
			if err != nil {
				return fmt.Errorf("error processing struct %v at: %v, cause:%v ",
					sel, path, err)
			}
		}
	}
	return nil
}

func processTable(classes ClassMap, table string, ctx *cue.Context,
	tablesBasePath string) error {
	facetsBasePath := path.Join(tablesBasePath, table, "facets")

	files, err := os.ReadDir(facetsBasePath)
	if err != nil {
		return fmt.Errorf("error reading %v path: %v", facetsBasePath, err)
	}

	fmt.Printf("Processing: %v. ", table)

	for _, facet := range files {
		if strings.HasSuffix(facet.Name(), ".cue") {
			tableFacet := TableFacet{table, strings.Split(facet.Name(), ".")[0]}
			facetPath := path.Join(facetsBasePath, facet.Name())
			err := processCuePath(classes, facetPath, ctx, tableFacet)
			if err != nil {
				return fmt.Errorf("error processing %v path: %v", facetPath, err)
			}

			fmt.Print(".")
		}
	}

	fmt.Print(" Done!\n")
	return nil
}

func GenItemsPy(ctx *cue.Context, tablesBasePath string,
	itemsBasePath string) error {

	policies := map[string]GenerationPolicy{
		"integrates_vms": {"items.py",
			[]string{},
			map[string][]string{
				"decimal":                      {"Decimal"},
				"typing":                       {"Any", "NotRequired", "TypedDict"},
				"integrates.class_types.types": {"Item"},
			}},
		"integrates_vms_historic": {"historic_items.py",
			[]string{},
			map[string][]string{
				"decimal": {"Decimal"},
				"typing":  {"NotRequired", "TypedDict"},
			}},
	}

	const ack = `This file was generated by a script. Do not edit manually.
If you need to make changes, please run
m . /integrates/schemas/gen_types

More information at
https://dev.fluidattacks.com/components/integrates/database/database-design`

	for table, policy := range policies {
		outFileName := fmt.Sprintf("%v/%v", itemsBasePath, policy.OutFileName)

		classes := make(ClassMap)

		err := processTable(classes, table, ctx, tablesBasePath)
		if err != nil {
			return err
		}

		file, err := initFile(outFileName)
		if err != nil {
			return err
		}

		defer file.Close()

		err = addHeaders(file, policy.RuffDisables, ack)
		if err != nil {
			return err
		}

		err = addImports(file, policy.ImportMaps)
		if err != nil {
			return err
		}

		classesOrder, err := correctOrder(classes)
		if err != nil {
			return err
		}

		err = writeToFile(file, classes, classesOrder)
		if err != nil {
			return err
		}

		fmt.Printf("File %v generated successfully!\n", outFileName)
	}

	return nil
}
