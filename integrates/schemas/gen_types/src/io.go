package main

import (
	"fmt"
	"log"
	"os"
	"sort"
	"strings"
)

func initFile(filename string) (*os.File, error) {
	file, err := os.OpenFile(filename, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0644)
	if err != nil {
		return nil, err
	}
	err = file.Truncate(0)

	if err != nil {
		return nil, err
	}
	return file, nil

}

func addHeaders(file *os.File, lintDisables []string, ack string) error {
	var err error

	var sb strings.Builder

	if len(ack) > 0 {
		fmt.Fprintf(&sb, "\"\"\"\n%s\n\"\"\"\n\n", ack)
	}

	if len(lintDisables) > 0 {
		for _, policy := range lintDisables {
			fmt.Fprintf(&sb, "# noqa: %s", policy)
		}
		fmt.Fprint(&sb, "\n\n")
	}

	_, err = file.WriteString(sb.String())

	return err
}

func addImports(file *os.File, importsMap map[string][]string) error {
	var err error

	var sb strings.Builder

	keys := make([]string, 0, len(importsMap))
	for k := range importsMap {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	for _, k := range keys {
		fmt.Fprintf(&sb, "from %s import (\n%s%s,\n)\n", k, indent,
			strings.Join(importsMap[k], ",\n"+indent))
	}

	_, err = file.WriteString(sb.String())

	if err != nil {
		return err
	}
	return nil
}

func handleErr(err error, failMsg string, succMsg string) {
	if err != nil {
		log.Fatal(fmt.Errorf("%v: %v", failMsg, err))
	}
	fmt.Println(succMsg)
}

func writeToFile(file *os.File, classes ClassMap, order []string) error {
	for _, className := range order {
		classDetails := classes[className]
		_, err := file.WriteString(classDetails.Content.String())
		if err != nil {
			return fmt.Errorf("error writing to file: %v", err)
		}
	}
	return nil
}
