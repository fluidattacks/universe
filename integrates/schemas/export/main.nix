{ makeScript, inputs, outputs, projectPath, ... }:
makeScript {
  replace = {
    "__argDbDesign__" = projectPath "/integrates/schemas/database-design.json";
  };
  entrypoint = ./entrypoint.sh;
  name = "integrates-schemas-export";
  searchPaths.bin = [
    inputs.nixpkgs.diffutils
    inputs.nixpkgs.jd-diff-patch
    inputs.nixpkgs.python3
    outputs."/common/utils/cue"
  ];
}
