{ inputs, pkgs, ... }:
let
  utils = import inputs.utils { inherit pkgs; };
  cert = import inputs.cert { inherit utils; };
in {
  cachix = utils.cachix.config.default;
  packages = [ utils.shell.aws utils.shell.sops ];

  languages.javascript = {
    enable = true;
    package = pkgs.nodejs_20;
    npm = {
      enable = true;
      install.enable = true;
    };
  };

  scripts = {
    integrates-front.exec = utils.shell.strict ''
      npm run "$@"
    '';
  };

  enterShell = ''
    export FI_WEBPACK_TLS_CERT="${cert}/cert.crt"
    export FI_WEBPACK_TLS_KEY="${cert}/cert.key"
  '';
}
