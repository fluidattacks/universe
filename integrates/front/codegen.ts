import type { CodegenConfig } from "@graphql-codegen/cli";

const config: CodegenConfig = {
  documents: "src/**/queries.ts",
  generates: {
    "src/gql/": {
      config: {
        avoidOptionals: {
          defaultValue: true,
          field: true,
          inputValue: false,
        },
        scalars: {
          DateTime: "string",
          GenericScalar: "unknown",
          JSONString: "string",
        },
      },
      plugins: [],
      preset: "client",
      presetConfig: {
        fragmentMasking: { unmaskFunctionName: "getFragmentData" },
      },
    },
  },
  overwrite: true,
  schema: "./../back/integrates/api",
};

/* eslint-disable import/no-default-export */
export default config;
