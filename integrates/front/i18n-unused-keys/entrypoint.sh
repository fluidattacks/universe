# shellcheck shell=bash

function main {
  check_unused_translations "integrates/front"
}

main "${@}"
