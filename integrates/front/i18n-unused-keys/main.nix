{ makeScript, outputs, ... }:
makeScript {
  entrypoint = ./entrypoint.sh;
  name = "integrates-front-i18n-unused-keys";
  searchPaths = { source = [ outputs."/common/utils/i18n-unused" ]; };
}
