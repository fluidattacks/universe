# shellcheck shell=bash

function main {
  local dir="integrates/front"
  export NODE_OPTIONS="--max-old-space-size=8192"

  pushd "${dir}" || error "${dir} not found"
  if [ ! -d node_modules ]; then
    npm set cache .npm
    npm ci
  fi
  npm run test -- "${@}" --shard "${CI_NODE_INDEX:-1}/${CI_NODE_TOTAL:-1}"
}

main "${@}"
