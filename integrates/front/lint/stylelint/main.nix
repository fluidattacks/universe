{ inputs, makeScript, ... }:
makeScript {
  entrypoint = ./entrypoint.sh;
  name = "integrates-front-lint-stylelint";
  searchPaths.bin = [ inputs.nixpkgs.nodejs_20 inputs.nixpkgs.bash ];
}
