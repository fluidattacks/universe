# shellcheck shell=bash

function main {
  local stylelint_args=(
    --cache
    '**/*.{ts, tsx}'
  )
  local return_value=0

  : && pushd integrates/front \
    && if [ ! -d node_modules ]; then
      info "Installing node modules"
      npm ci
    fi \
    && export PATH="${PWD}/node_modules/.bin:${PATH}" \
    && export NODE_PATH="${PWD}/node_modules:${NODE_PATH}" \
    && if stylelint "${stylelint_args[@]}"; then
      info 'All styles are ok!'
    else
      info 'Some files do not follow the suggested style.' \
        && info 'we will fix some of the issues automatically,' \
        && info 'but the job will fail.' \
        && { stylelint "${stylelint_args[@]}" --fix || true; } \
        && return_value=1
    fi \
    && popd \
    && return "${return_value}"
}

main "$@"
