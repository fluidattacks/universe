// @ts-check

import { createFolderStructure } from "eslint-plugin-project-structure";

export const folderStructureConfig = createFolderStructure({
  rules: {
    component_directory: {
      children: [
        {
          ruleId: "component_directory",
        },
        {
          name: "index(.(template|test))?.tsx",
        },
        {
          name: "index.d.ts",
        },
        {
          name: "(constants|context|hooks|queries|styles|types|utils|validations)?.ts",
        },
        {
          name: "hooks",
          children: [
            {
              name: "{kebab-case}.ts",
            },
          ],
        },
      ],
      name: "{kebab-case}",
    },
    features_directory: {
      children: [
        {
          ruleId: "features_directory",
        },
        {
          name: "(index|utils)(.(template|test))?.tsx",
        },
        {
          name: "(constants|context|hooks|queries|styles|types|utils|validations)?.ts",
        },
        {
          name: "hooks",
          children: [
            {
              name: "{kebab-case}.ts",
            },
          ],
        },
      ],
      name: "{kebab-case}",
    },
    generic_directory: {
      children: [
        {
          ruleId: "generic_directory",
        },
        {
          name: "{kebab-case}(.test)?.(ts|tsx)",
        },
        {
          name: "index(.test)?.tsx",
        },
        {
          name: "index?.tsx",
        },
      ],
      name: "{kebab-case}",
    },
    pages_directory: {
      children: [
        {
          ruleId: "pages_directory",
        },
        {
          name: "index(.(template|test))?.tsx",
        },
        {
          name: "{kebab-case}(.test)?.(ts|tsx)",
        },
        {
          name: "(constants|context|hooks|queries|styles|types|utils|validations)?.(ts|tsx)",
        },
      ],
      name: "{kebab-case}",
    },
    typings_directory: {
      children: [
        {
          ruleId: "typings_directory",
        },
        {
          name: "{kebab-case}.d.ts",
        },
      ],
    },
  },
  structure: [
    { name: "*" },
    {
      name: "src",
      children: [
        {
          name: "components",
          children: [
            {
              name: "@core",
              ruleId: "component_directory",
            },
            {
              ruleId: "component_directory",
            },
          ],
        },
        {
          name: "context",
          ruleId: "generic_directory",
        },
        {
          name: "features",
          ruleId: "features_directory",
        },
        {
          name: "gql",
          ruleId: "generic_directory",
        },
        {
          name: "graphics",
          ruleId: "generic_directory",
        },
        {
          name: "hooks",
          ruleId: "generic_directory",
        },
        {
          name: "mocks",
          ruleId: "generic_directory",
        },
        {
          name: "pages",
          ruleId: "pages_directory",
        },
        {
          name: "resources",
          ruleId: "generic_directory",
        },
        {
          name: "typings",
          ruleId: "typings_directory",
        },
        {
          name: "utils",
          ruleId: "generic_directory",
        },
        {
          name: "app.tsx",
        },
        {
          name: "styles.ts",
        },
        {
          name: "types.ts",
        },
      ],
    },
  ],
});
