import type { HTMLAttributes, HTMLProps } from "react";

type TVariants = "ordered" | "unordered";

type TNestedListItem = Record<string, string[]>;

interface IListProps
  extends HTMLProps<HTMLElement>,
    HTMLAttributes<HTMLElement> {
  items?: (JSX.Element | TNestedListItem | string)[];
  variant?: TVariants;
  level?: number;
}

export type { IListProps, TVariants };
