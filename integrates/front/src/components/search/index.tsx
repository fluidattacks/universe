import type { FormikHelpers } from "formik";
import { Formik } from "formik";
import { useCallback } from "react";

import { SearchBar } from "./search-bar";
import type { ISearchProps } from "./types";

const Search = ({
  enableSearchBar = true,
  handleOnSubmit,
  handleOnKeyPressed,
  placeHolder,
  smallSearch,
}: ISearchProps): JSX.Element | null => {
  const handleSubmit = useCallback(
    (
      { search }: { search: string },
      formikHelpers: FormikHelpers<{
        search: string;
      }>,
    ): void => {
      handleOnSubmit?.(search);
      handleOnKeyPressed?.(search);
      formikHelpers.resetForm();
    },
    [handleOnKeyPressed, handleOnSubmit],
  );

  if (!enableSearchBar) {
    return null;
  }

  return (
    <Formik initialValues={{ search: "" }} onSubmit={handleSubmit}>
      <SearchBar
        handleOnKeyPressed={handleOnKeyPressed}
        handleOnSubmit={handleOnSubmit}
        name={"search"}
        placeHolder={placeHolder}
        smallSearch={smallSearch}
      />
    </Formik>
  );
};

export { Search };
