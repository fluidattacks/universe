import { fireEvent, screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";

import { Search } from ".";
import { render } from "mocks/index";

describe("search", (): void => {
  it("should render", async (): Promise<void> => {
    expect.hasAssertions();

    const handleOnSearch = jest.fn();
    const handleOnKeyPressed = jest.fn();

    render(
      <Search
        handleOnKeyPressed={handleOnKeyPressed}
        handleOnSubmit={handleOnSearch}
        placeHolder={"Search"}
      />,
    );

    expect(screen.getAllByRole("searchbox")).toHaveLength(1);

    const value = "Test";
    await userEvent.type(screen.getByRole("searchbox"), value);
    await waitFor((): void => {
      expect(handleOnKeyPressed).toHaveBeenCalledTimes(value.length);
    });

    expect(handleOnSearch).toHaveBeenCalledTimes(0);

    fireEvent.keyUp(screen.getByRole("searchbox"), {
      key: "Enter",
    });

    await waitFor((): void => {
      expect(handleOnSearch).toHaveBeenCalledTimes(1);
    });
  });
});
