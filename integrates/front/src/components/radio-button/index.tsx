import { Field } from "formik";

import { CheckMark, LabelContainer } from "./styles";
import type { IRadioButtonProps } from "./types";

const RadioButton = ({
  defaultChecked = false,
  disabled = false,
  label,
  name,
  onChange,
  onClick,
  showLabel = true,
  value,
  variant = "input",
}: Readonly<IRadioButtonProps>): JSX.Element => {
  if (variant === "input") {
    return (
      <LabelContainer $disabled={disabled}>
        <input
          aria-label={label}
          defaultChecked={defaultChecked}
          disabled={disabled}
          name={name}
          onChange={onChange}
          onClick={onClick}
          type={"radio"}
          value={value}
        />
        <CheckMark />
        {showLabel ? label : null}
      </LabelContainer>
    );
  }

  return (
    <LabelContainer $disabled={disabled}>
      <Field name={name} onClick={onClick} type={"radio"} value={value} />
      <CheckMark />
      {label}
    </LabelContainer>
  );
};

export { RadioButton };
