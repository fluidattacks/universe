import type { HTMLAttributes, HTMLProps } from "react";

type TFieldVariant = "formikField" | "input" | "inputOnly" | "tableCheckbox";
interface IRadioButtonProps
  extends HTMLAttributes<HTMLInputElement>,
    HTMLProps<HTMLInputElement> {
  name: string;
  showLabel?: boolean;
  value: string;
  variant?: TFieldVariant;
}

export type { IRadioButtonProps, TFieldVariant };
