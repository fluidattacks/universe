import type { IColumnModalProps } from "@fluidattacks/design";
import { Button, ColumnsModal, useColumnsModal } from "@fluidattacks/design";
import type { Column, RowData } from "@tanstack/react-table";
import isNil from "lodash/isNil";
import { useCallback, useEffect, useMemo, useState } from "react";
import { useTranslation } from "react-i18next";

import type { IToggleProps } from "components/table/types";

export const ColumnToggle = <TData extends RowData>(
  props: IToggleProps<TData>,
): JSX.Element => {
  const { columnOrder, newTable = false, table } = props;
  const { t } = useTranslation();
  const [visibleColumns, setVisibleColumns] = useState(0);
  const [, hasSubHeaders] = table.getHeaderGroups();
  const tableState = table.getState();

  const columns = useMemo(
    (): Column<TData>[] =>
      table
        .getAllLeafColumns()
        .filter((column): boolean => column.id !== "selection"),
    [table],
  );
  const items = useMemo((): IColumnModalProps[] => {
    return columns.map(
      (column): IColumnModalProps => ({
        group: column.parent?.id,
        id: column.id,
        locked: column.getIsPinned() === "left",
        name: column.columnDef.header as string,
        toggleVisibility: column.toggleVisibility,
        visible: column.getIsVisible(),
      }),
    );
  }, [columns]);

  const columnsModalRef = useColumnsModal({
    columnOrder,
    items,
    name: "table-column-management",
    setColumnOrder: table.setColumnOrder,
  });

  const showModal = useCallback((): void => {
    columnsModalRef.toggle();
  }, [columnsModalRef]);

  useEffect((): void => {
    setVisibleColumns(
      table
        .getVisibleLeafColumns()
        .filter((column): boolean => column.id !== "selection").length,
    );
  }, [table, tableState]);

  return (
    <div id={"columns-filter"}>
      {newTable ? (
        <Button
          icon={"columns"}
          id={"ColumnsToggleBtn"}
          onClick={showModal}
          variant={"ghost"}
        >
          <p>{visibleColumns}</p>
        </Button>
      ) : (
        <Button
          icon={"columns"}
          id={"ColumnsToggleBtn"}
          onClick={showModal}
          variant={"ghost"}
        >
          {`${t("group.findings.tableSet.btn.text")} (${visibleColumns} / ${columns.length})`}
        </Button>
      )}
      <ColumnsModal
        modalRef={columnsModalRef}
        title={"Manage columns"}
        variant={isNil(hasSubHeaders) ? "ungrouped" : "grouped"}
      />
    </div>
  );
};
