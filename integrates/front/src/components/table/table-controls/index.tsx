import { Button } from "@fluidattacks/design";
import type { RowData } from "@tanstack/react-table";
import { Fragment } from "react";
import { useTranslation } from "react-i18next";

import type { ITableControlsProps } from "./types";

import { ExportCsv } from "../export-csv";

const TableControls = <TData extends RowData>({
  csvConfig,
  data,
  extraButtons,
}: ITableControlsProps<TData>): JSX.Element => {
  const { t } = useTranslation();

  return (
    <Fragment>
      <ExportCsv csvConfig={csvConfig} data={data}>
        <Button
          icon={"file-export"}
          id={"CSVExportBtn"}
          tooltip={t("buttons.exportTip", { format: "CSV" })}
          variant={"ghost"}
        >
          {t("group.findings.exportCsv.text")}
        </Button>
      </ExportCsv>
      {extraButtons}
    </Fragment>
  );
};

export { TableControls };
