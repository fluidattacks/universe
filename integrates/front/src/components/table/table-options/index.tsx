import { Button } from "@fluidattacks/design";
import type { RowData, Table, TableMeta } from "@tanstack/react-table";
import { isEmpty, isNil } from "lodash";
import { useMemo } from "react";
import * as React from "react";

import {
  ControlsContainer,
  SearchContainer,
  TableOptionsContainer,
} from "../styles";
import { ColumnToggle } from "../table-column-toggle";
import { Search } from "components/search";

interface ITableOptionsProps<TData extends RowData> {
  readonly table: Table<TData>;
  readonly actionsContent: React.ReactNode;
  readonly highlightedContent: React.ReactNode;
}

const TableOptions = <TData extends RowData>({
  table,
  actionsContent,
  highlightedContent,
}: ITableOptionsProps<TData>): React.ReactNode => {
  const {
    defaultColumnOrder,
    filters,
    filtersComponent,
    openFiltersHandler,
    onSearch,
  } = useMemo(
    (): TableMeta<TData> => table.options.meta ?? {},
    [table.options.meta],
  );
  const { columnVisibility } = table.getState();

  const totalFilters = useMemo(
    (): number =>
      Object.entries(filters ?? {}).filter(
        ([key, value]): boolean =>
          !isEmpty(value) &&
          !key.startsWith("set") &&
          !key.startsWith("reset") &&
          !key.startsWith("pagination") &&
          !key.startsWith("sort"),
      ).length,
    [filters],
  );

  return (
    <TableOptionsContainer>
      <SearchContainer>
        <Search
          enableSearchBar={true}
          handleOnSubmit={onSearch}
          placeHolder={"Search"}
        />
      </SearchContainer>
      {highlightedContent === undefined ? undefined : (
        <div className={"highlight-container"}>{highlightedContent}</div>
      )}
      <ControlsContainer>
        {filters === undefined && isNil(columnVisibility) ? null : (
          <div className={"table-controls-container"}>
            {filters && filtersComponent === undefined ? (
              <Button
                icon={"filter"}
                id={"filterBtn"}
                onClick={openFiltersHandler}
                variant={"ghost"}
              >
                <p>{totalFilters}</p>
              </Button>
            ) : undefined}
            {filtersComponent && filters ? filtersComponent : undefined}
            {isNil(columnVisibility) ? undefined : (
              <ColumnToggle
                columnOrder={defaultColumnOrder}
                newTable={true}
                table={table}
              />
            )}
          </div>
        )}
        {actionsContent === undefined ? undefined : (
          <div className={"actions-container"}>{actionsContent}</div>
        )}
      </ControlsContainer>
    </TableOptionsContainer>
  );
};

export { TableOptions };
