import { screen } from "@testing-library/react";

import { ExportOptions } from ".";
import { render } from "mocks";

describe("exportOptions", (): void => {
  it("renders export options with filtered and all data buttons", (): void => {
    expect.hasAssertions();

    const mockData = [
      { id: 1, name: "Item 1" },
      { id: 2, name: "Item 2" },
    ];
    const mockFilteredData = [{ id: 1, name: "Item 1" }];

    const { container } = render(
      <ExportOptions data={mockData} filteredData={mockFilteredData} />,
    );

    expect(screen.getByText("table.exportCsv.filtered")).toBeInTheDocument();
    expect(screen.getByText("table.exportCsv.all")).toBeInTheDocument();
    expect(container).toBeInTheDocument();
  });

  it("renders with empty data arrays", (): void => {
    expect.hasAssertions();

    const emptyData: never[] = [];

    const { container } = render(
      <ExportOptions data={emptyData} filteredData={emptyData} />,
    );

    expect(screen.getByText("table.exportCsv.filtered")).toBeInTheDocument();
    expect(screen.getByText("table.exportCsv.all")).toBeInTheDocument();
    expect(container).toBeInTheDocument();
  });
});
