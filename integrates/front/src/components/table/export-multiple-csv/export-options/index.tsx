import { Container, ListItem, ListItemsWrapper } from "@fluidattacks/design";
import type { RowData } from "@tanstack/table-core";
import { useTranslation } from "react-i18next";

import type { IExportOptionsProps } from "./types";

import { ExportCsv } from "components/table/export-csv";

const PROCESSING_ROWS_LIMIT = 5000;
const ExportOptions = <TData extends RowData>({
  data,
  filteredData,
  onBatchDownload = undefined,
  size = undefined,
}: IExportOptionsProps<TData>): JSX.Element => {
  const { t } = useTranslation();

  const batchExport =
    (size ?? 0) > PROCESSING_ROWS_LIMIT && onBatchDownload !== undefined;

  return (
    <Container>
      <ListItemsWrapper>
        <ExportCsv csvConfig={{ export: true }} data={filteredData ?? data}>
          <ListItem value={"export-filtered-data"}>
            {t("table.exportCsv.filtered")}
          </ListItem>
        </ExportCsv>
        {batchExport ? (
          <ListItem
            id={"export-batch-data"}
            onClick={onBatchDownload}
            value={"export-batch-data"}
          >
            {t("table.exportCsv.all")}
          </ListItem>
        ) : (
          <ExportCsv csvConfig={{ export: true }} data={data}>
            <ListItem value={"export-all-data"}>
              {t("table.exportCsv.all")}
            </ListItem>
          </ExportCsv>
        )}
      </ListItemsWrapper>
    </Container>
  );
};

export { ExportOptions, PROCESSING_ROWS_LIMIT };
