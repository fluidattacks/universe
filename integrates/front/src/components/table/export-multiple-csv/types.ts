import type { RowData } from "@tanstack/react-table";

interface IExportFileProps<TData extends RowData> {
  data: TData[];
  fetchReportData?: () => Promise<void>;
  filteredData?: TData[];
  onBatchDownload?: (event: React.MouseEvent<HTMLLIElement>) => void;
  size?: number;
}

export type { IExportFileProps };
