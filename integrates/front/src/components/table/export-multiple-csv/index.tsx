import { Button, Container, Icon } from "@fluidattacks/design";
import type { RowData } from "@tanstack/react-table";
import { useCallback, useRef, useState } from "react";
import { useTranslation } from "react-i18next";

import { ExportOptions } from "./export-options";
import { ExportAnimation } from "./styles";
import type { IExportFileProps } from "./types";

import { useClickOutside } from "hooks/use-click-outside";

const ExportMultipleCsv = <TData extends RowData>({
  data,
  fetchReportData = undefined,
  filteredData,
  onBatchDownload = undefined,
  size = undefined,
}: IExportFileProps<TData>): JSX.Element => {
  const [open, setOpen] = useState<boolean | undefined>(false);
  const { t } = useTranslation();
  const dropdownRef = useRef<HTMLDivElement>(null);
  const openDropdown = useCallback(async (): Promise<void> => {
    if (!(open ?? false)) {
      setOpen(undefined);
      if (fetchReportData) await fetchReportData();
    }
    setOpen((previousState): boolean | undefined => !(previousState ?? false));
  }, [fetchReportData, open]);

  useClickOutside(
    dropdownRef.current,
    (): void => {
      setOpen(false);
    },
    true,
  );

  return (
    <Container position={"relative"} ref={dropdownRef}>
      <Button
        id={"export-csv"}
        onClick={openDropdown}
        tooltip={t("buttons.exportTip", { format: "CSV" })}
        variant={"ghost"}
      >
        {open === undefined ? (
          <ExportAnimation>
            <Icon icon={"file-export"} iconSize={"xs"} iconType={"fa-light"} />
          </ExportAnimation>
        ) : (
          <Icon icon={"file-export"} iconSize={"xs"} iconType={"fa-light"} />
        )}
        {t("group.findings.exportCsv.text")}
      </Button>
      <Container
        display={(open ?? false) ? "block" : "none"}
        mt={0.25}
        position={"absolute"}
        right={"1px"}
        zIndex={2}
      >
        <ExportOptions
          data={data}
          filteredData={filteredData}
          onBatchDownload={onBatchDownload}
          size={size}
        />
      </Container>
    </Container>
  );
};

export { ExportMultipleCsv };
