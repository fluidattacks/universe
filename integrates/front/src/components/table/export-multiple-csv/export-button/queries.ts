import { graphql } from "gql";

const REQUEST_GROUP_CSV_REPORT = graphql(`
  query RequestGroupCsvReport(
    $groupName: String!
    $reportType: ReportType!
    $verificationCode: String!
  ) {
    csvReport(
      groupName: $groupName
      reportType: $reportType
      verificationCode: $verificationCode
    ) {
      success
    }
  }
`);

export { REQUEST_GROUP_CSV_REPORT };
