import { screen } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { Fragment } from "react";

import { ExportMultipleCsv } from ".";
import { render } from "mocks";

describe("exportMultipleCsv", (): void => {
  const mockData = [
    { id: 1, name: "Test 1" },
    { id: 2, name: "Test 2" },
  ];
  const mockFilteredData = [{ id: 1, name: "Test 1" }];

  it("should render export button with correct text and icon", (): void => {
    expect.hasAssertions();

    render(
      <ExportMultipleCsv data={mockData} filteredData={mockFilteredData} />,
    );

    const button = screen.getByRole("button");

    expect(button).toHaveAttribute("id", "export-csv");
    expect(
      screen.getByText("group.findings.exportCsv.text"),
    ).toBeInTheDocument();
  });

  it("should toggle dropdown on button click", async (): Promise<void> => {
    expect.hasAssertions();

    const { container } = render(
      <ExportMultipleCsv data={mockData} filteredData={mockFilteredData} />,
    );

    const dropdown = container.querySelector("ul");

    expect(dropdown).not.toBeVisible();

    await userEvent.click(screen.getByRole("button"));

    expect(dropdown).toBeVisible();

    const exportButton = screen.getByTestId("export-all-data");

    expect(exportButton).toBeVisible();

    await userEvent.click(screen.getByRole("button"));

    expect(dropdown).not.toBeVisible();
  });

  it("should close dropdown when clicking outside", async (): Promise<void> => {
    expect.hasAssertions();

    const { container } = render(
      <Fragment>
        <div data-testid={"outside"}>{"Outside"}</div>
        <ExportMultipleCsv data={mockData} filteredData={mockFilteredData} />
      </Fragment>,
    );

    await userEvent.click(screen.getByRole("button"));
    const dropdown = container.querySelector("ul");

    expect(dropdown).toBeVisible();

    await userEvent.click(screen.getByTestId("outside"));

    expect(dropdown).not.toBeVisible();
  });

  it("should render with empty data arrays", (): void => {
    expect.hasAssertions();

    render(<ExportMultipleCsv data={[]} filteredData={[]} />);

    expect(screen.getByRole("button")).toBeInTheDocument();
  });

  it("should render with batch processing option", async (): Promise<void> => {
    expect.hasAssertions();

    const generateLargeDataset = (
      count: number,
    ): Record<string, number | string>[] => {
      return Array.from(
        { length: count },
        (_, index): Record<string, number | string> => ({
          id: index + 1,
          name: `Test ${index + 1}`,
        }),
      );
    };

    const mockLargeData = generateLargeDataset(1500);
    const mockLargeDataFiltered = mockLargeData.slice(0, 100);
    const mockOnBatchDownload = jest.fn();

    render(
      <ExportMultipleCsv
        data={mockLargeData}
        filteredData={mockLargeDataFiltered}
        onBatchDownload={mockOnBatchDownload}
        size={5500}
      />,
    );
    const exportButton = screen.getByRole("button", {
      name: "group.findings.exportCsv.text",
    });

    expect(exportButton).toBeInTheDocument();

    await userEvent.click(exportButton);

    const batchExportButton = screen.getByTestId("export-batch-data");

    expect(batchExportButton).toBeInTheDocument();

    await userEvent.click(batchExportButton);

    expect(mockOnBatchDownload).toHaveBeenCalledTimes(1);
  });
});
