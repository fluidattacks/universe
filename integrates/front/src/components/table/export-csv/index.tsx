import type { RowData } from "@tanstack/react-table";
import _ from "lodash";
import { useMemo } from "react";
import { CSVLink } from "react-csv";

import type { IExportCsvProps, IHeader } from "./types";

import { flattenData } from "utils/format-helpers";

const ExportCsv = <TData extends RowData>({
  csvConfig = {},
  data,
  children,
}: Readonly<IExportCsvProps<TData>>): JSX.Element => {
  const {
    columns: csvColumns = undefined,
    export: csvExport = false,
    headers: csvHeaders = {},
    name: csvName = "Report",
  } = csvConfig;

  const flattenedData = useMemo(
    (): object[] =>
      _.isUndefined(csvColumns)
        ? flattenData(data as object[])
        : flattenData(data as object[]).map((item: object): object =>
            _.pick(item, csvColumns),
          ),
    [data, csvColumns],
  );

  const hasFormattedHeaders = useMemo(
    (): boolean => flattenedData.length > 0 && !_.isEmpty(csvHeaders),
    [flattenedData.length, csvHeaders],
  );

  const headers = useMemo(
    (): IHeader[] =>
      (hasFormattedHeaders ? Object.keys(flattenedData[0]) : []).map(
        (value: string): IHeader =>
          _.includes(Object.keys(csvHeaders), value)
            ? {
                key: value,
                label: csvHeaders[value],
              }
            : { key: value, label: value },
      ),
    [hasFormattedHeaders, flattenedData, csvHeaders],
  );

  if (!csvExport) return <div />;

  return (
    <CSVLink
      data={flattenedData}
      filename={csvName}
      headers={hasFormattedHeaders ? headers : undefined}
    >
      {children}
    </CSVLink>
  );
};

export { ExportCsv };
