import { Button } from "@fluidattacks/design";
import { screen } from "@testing-library/react";

import { ExportCsv } from ".";
import { render } from "mocks";

const TestComponent = (): JSX.Element => (
  <Button
    icon={"file-export"}
    id={"test-component"}
    tooltip={"export"}
    variant={"ghost"}
  >
    {"Export"}
  </Button>
);

describe("exportCsv", (): void => {
  it("renders with default headers when hasFormattedHeaders is false", (): void => {
    expect.hasAssertions();

    const mockData = [
      { id: 1, name: "Test" },
      { id: 2, name: "Test 2" },
    ];

    const { container } = render(
      <ExportCsv csvConfig={{ export: true, name: "test.csv" }} data={mockData}>
        <TestComponent />
      </ExportCsv>,
    );

    expect(container.querySelector("a")).toBeInTheDocument();
    expect(container.querySelector("#test-component")).toBeInTheDocument();
  });

  it("renders with custom headers when hasFormattedHeaders is true", (): void => {
    expect.hasAssertions();

    const mockData = [
      { id: 1, name: "Test" },
      { id: 2, name: "Test 2" },
    ];
    const mockHeaders = { id: "ID", name: "Name" };

    const { container } = render(
      <ExportCsv
        csvConfig={{ export: true, headers: mockHeaders, name: "test.csv" }}
        data={mockData}
      >
        <TestComponent />
      </ExportCsv>,
    );

    expect(container.querySelector("a")).toBeInTheDocument();
    expect(container.querySelector("#test-component")).toBeInTheDocument();
  });

  it("displays correct button text and tooltip", (): void => {
    expect.hasAssertions();

    const mockData = [{ id: 1 }];

    render(
      <ExportCsv csvConfig={{ export: true, name: "test.csv" }} data={mockData}>
        <TestComponent />
      </ExportCsv>,
    );

    expect(screen.getByText("Export")).toBeInTheDocument();
    expect(screen.getByRole("button")).toBeInTheDocument();
    expect(screen.getByRole("button")).toHaveAttribute(
      "data-tooltip-id",
      "test-component-tooltip",
    );
  });

  it("renders with empty data array", (): void => {
    expect.hasAssertions();

    const { container } = render(
      <ExportCsv csvConfig={{ export: true, name: "empty.csv" }} data={[]}>
        <TestComponent />
      </ExportCsv>,
    );

    expect(container.querySelector("a")).toBeInTheDocument();
    expect(container.querySelector("#test-component")).toBeInTheDocument();
  });
});
