/* eslint-disable @typescript-eslint/no-unnecessary-type-parameters */
import type {
  Cell,
  Column,
  ColumnDef,
  ColumnOrderState,
  Row,
  RowData,
  Table,
} from "@tanstack/react-table";
import type { Dispatch, FormEvent, SetStateAction } from "react";

import type { IUseTable } from "hooks/use-table";

interface ICellHelper<TData extends RowData> {
  table: Table<TData>;
  column: Column<TData>;
  row: Row<TData>;
  cell: Cell<TData, unknown>;
  getValue: <TTValue = unknown>() => TTValue;
  renderValue: <TTValue = unknown>() => TTValue | null;
}

interface IPagMenuProps<TData extends RowData> {
  table: Table<TData>;
}

interface ICsvConfig {
  columns?: string[];
  export?: boolean;
  headers?: Record<string, string>;
  name?: string;
}

interface ITableOptions<TData extends RowData> {
  columnToggle?: boolean;
  hasGlobalFilter?: boolean;
  hasNextPage?: boolean;
  enableRowSelection?: boolean | ((row: Row<TData>) => boolean);
  enableSearchBar?: boolean;
  manualPagination?: boolean;
  searchPlaceholder?: string;
  size?: number;
  smallSearch?: boolean;
  enableSorting?: boolean;
}

interface ITableProps<TData extends RowData> {
  data: TData[];
  dataPrivate?: boolean;
  columns: ColumnDef<TData>[];
  csvConfig?: ICsvConfig;
  expandedRow?: (row: Row<TData>) => JSX.Element;
  extraButtons?: JSX.Element;
  filters?: JSX.Element;
  filtersApplied?: JSX.Element;
  loadingData?: boolean;
  onNextPage?: () => Promise<void>;
  onRowClick?: (row: Row<TData>) => (event: FormEvent<HTMLElement>) => void;
  onSearch?: (search: string) => void;
  rightSideComponents?: JSX.Element;
  rowSelectionSetter?: Dispatch<SetStateAction<TData[]>>;
  rowSelectionState?: TData[];
  selectionMode?: "checkbox" | "radio";
  tableRef: IUseTable;
  options?: ITableOptions<TData>;
  extraBelowButtons?: JSX.Element;
}

interface IToggleProps<TData extends RowData> {
  columnOrder?: ColumnOrderState;
  newTable?: boolean;
  table: Table<TData>;
}

interface IPaginationProps<TData extends RowData>
  extends Pick<ITableProps<TData>, "onNextPage"> {
  hasNextPage?: boolean;
  table: Table<TData>;
}

export type {
  ICellHelper,
  IPagMenuProps,
  ITableProps,
  IToggleProps,
  ICsvConfig,
  ITableOptions,
  IPaginationProps,
};
