import { Container, Loading, Text } from "@fluidattacks/design";
import type { Row, RowData, Table } from "@tanstack/react-table";
import { flexRender } from "@tanstack/react-table";
import { useCallback, useMemo } from "react";
import { useTheme } from "styled-components";

import { CellContainer } from "../../styles";
import { translate } from "utils/translations/translate";

interface IBodyProps<TData extends RowData> {
  table: Table<TData>;
  loading?: boolean;
}

const Body = <TData extends RowData>({
  table,
  loading = false,
}: Readonly<IBodyProps<TData>>): JSX.Element => {
  const theme = useTheme();
  const clickIndex = useMemo((): number => {
    return (table.options.enableRowSelection as boolean) ? 1 : 0;
  }, [table]);

  const handleRowClick = useCallback(
    (row: Row<TData>): VoidFunction => {
      return (): void => {
        table.options.onRowClick?.(row);
      };
    },
    [table],
  );

  if (loading) {
    return (
      <tbody>
        <tr className={"row-loading"} data-testid={"row-loading"}>
          <td colSpan={table.getVisibleLeafColumns().length}>
            <Container
              alignItems={"center"}
              display={"flex"}
              justify={"center"}
              width={"100%"}
            >
              <Loading size={"md"} />
            </Container>
          </td>
        </tr>
      </tbody>
    );
  }

  if (table.getRowModel().rows.length === 0) {
    return (
      <tbody>
        <tr className={"row-no-data"} data-testid={"row-no-data"}>
          <td colSpan={table.getVisibleLeafColumns().length + 1}>
            <Text
              color={theme.palette.gray[800]}
              size={"sm"}
              textAlign={"center"}
            >
              {translate.t("table.noDataIndication")}
            </Text>
          </td>
        </tr>
      </tbody>
    );
  }

  return (
    <tbody>
      {table.getRowModel().rows.map((row): JSX.Element => {
        return (
          <tr data-testid={`row-${row.id}`} key={row.id}>
            {row.getVisibleCells().map((cell, index): JSX.Element => {
              return (
                <td
                  data-testid={`cell-${cell.id}`}
                  key={cell.id}
                  onClick={
                    index === clickIndex ? handleRowClick(row) : undefined
                  }
                >
                  <CellContainer $width={`${cell.column.getSize()}`}>
                    {flexRender(cell.column.columnDef.cell, cell.getContext())}
                  </CellContainer>
                </td>
              );
            })}
          </tr>
        );
      })}
    </tbody>
  );
};

export { Body };
