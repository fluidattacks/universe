import { Container, Icon, Loading, Text } from "@fluidattacks/design";
import type { Row, RowData, Table } from "@tanstack/react-table";
import { flexRender } from "@tanstack/react-table";
import _ from "lodash";
import type { FormEvent, FormEventHandler } from "react";
import { Fragment, useCallback } from "react";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

import { Checkbox } from "components/checkbox";
import { RadioButton } from "components/radio-button";
import type { ITableProps } from "components/table/types";

interface IBodyProps<TData extends RowData>
  extends Pick<
    ITableProps<TData>,
    | "data"
    | "expandedRow"
    | "onRowClick"
    | "rowSelectionSetter"
    | "selectionMode"
  > {
  loadingData?: boolean;
  radioSelectionHandler: (row: Row<TData>) => FormEventHandler;
  table: Table<TData>;
}

const Body = <TData extends RowData>({
  data,
  expandedRow,
  loadingData = false,
  onRowClick,
  radioSelectionHandler,
  rowSelectionSetter,
  selectionMode,
  table,
}: Readonly<IBodyProps<TData>>): JSX.Element => {
  const theme = useTheme();
  const { t } = useTranslation();

  function rowExpansionHandler(row: Row<TData>): (event: FormEvent) => void {
    return (event: FormEvent): void => {
      event.stopPropagation();
      row.toggleExpanded();
      // To prevent toggling checkboxes and radio buttons
      event.preventDefault();
    };
  }

  const handleClick = useCallback((event: FormEvent): void => {
    event.stopPropagation();
  }, []);

  function rowSelectionHandler(row: Row<TData>): (event: FormEvent) => void {
    return (event: FormEvent): void => {
      event.stopPropagation();
      row.toggleSelected();
    };
  }

  if (loadingData) {
    return (
      <tbody>
        <tr className={"no-data-row"}>
          <td colSpan={table.getVisibleLeafColumns().length}>
            <Container
              alignItems={"center"}
              display={"flex"}
              justify={"center"}
              width={"100%"}
            >
              <Loading size={"md"} />
            </Container>
          </td>
        </tr>
      </tbody>
    );
  }

  return (
    <tbody>
      {_.isEmpty(data) || table.getRowModel().rows.length === 0 ? (
        <tr className={"no-data-row"}>
          <td colSpan={table.getVisibleLeafColumns().length + 1}>
            <Text
              color={theme.palette.gray[800]}
              size={"sm"}
              textAlign={"center"}
            >
              {t("table.noDataIndication")}
            </Text>
          </td>
        </tr>
      ) : undefined}
      {table.getRowModel().rows.map((row): JSX.Element => {
        return (
          <Fragment key={row.id}>
            <tr>
              {rowSelectionSetter === undefined ? undefined : (
                <td>
                  {selectionMode === "radio" ? (
                    <RadioButton
                      defaultChecked={row.getIsSelected()}
                      disabled={!row.getCanSelect()}
                      name={"table-body-row-radio-button"}
                      onChange={radioSelectionHandler(row)}
                      onClick={handleClick}
                      type={selectionMode}
                      value={""}
                    />
                  ) : (
                    <Checkbox
                      defaultChecked={row.getIsSelected()}
                      disabled={!row.getCanSelect()}
                      name={"table-body-row-checkbox"}
                      onChange={rowSelectionHandler(row)}
                      onClick={handleClick}
                      value={""}
                      variant={"tableCheckbox"}
                    />
                  )}
                </td>
              )}
              {row.getVisibleCells().map((cell): JSX.Element => {
                const isFirstCell = cell === row.getVisibleCells()[0];
                const isClickable = isFirstCell && onRowClick !== undefined;
                const isExpandable = isFirstCell && expandedRow !== undefined;

                return (
                  <td
                    className={isClickable ? "clickable" : undefined}
                    key={cell.id}
                    onClick={isClickable ? onRowClick(row) : undefined}
                  >
                    <Container
                      alignItems={"center"}
                      display={"flex"}
                      width={"100%"}
                    >
                      {isExpandable ? (
                        <div
                          onClick={rowExpansionHandler(row)}
                          onKeyPress={rowExpansionHandler(row)}
                          role={"button"}
                          style={{ cursor: "pointer", paddingRight: "8px" }}
                          tabIndex={0}
                        >
                          <Icon
                            icon={
                              row.getIsExpanded() ? "angle-up" : "angle-down"
                            }
                            iconSize={"xxs"}
                          />
                        </div>
                      ) : undefined}
                      {flexRender(
                        cell.column.columnDef.cell,
                        cell.getContext(),
                      )}
                    </Container>
                  </td>
                );
              })}
            </tr>
            {row.getIsExpanded() && (
              <tr>
                <td colSpan={row.getVisibleCells().length + 1}>
                  {expandedRow?.(row)}
                </td>
              </tr>
            )}
          </Fragment>
        );
      })}
    </tbody>
  );
};

export { Body };
