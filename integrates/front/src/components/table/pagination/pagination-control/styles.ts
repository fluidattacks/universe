import { IconButton } from "@fluidattacks/design";
import { styled } from "styled-components";

const ArrowButton = styled(IconButton)<{ $padding?: string }>`
  ${({ theme, $padding }): string => `
  background-color: ${theme.palette.white};
  border: 1px solid ${theme.palette.gray[200]};
  border-radius: 2px;
  padding: ${$padding ?? theme.spacing[0.25]};

  path {
    fill: ${theme.palette.gray[700]};
  }

  &:disabled {
    cursor: not-allowed;
    background: ${theme.palette.gray[200]};
    border-color: ${theme.palette.gray[200]};

    path {
      fill: ${theme.palette.gray[400]};
    }
  }

  &:hover:not([disabled]) {
    color: inherit;
    background-color: ${theme.palette.gray[100]};
  }
  `}
`;

export { ArrowButton };
