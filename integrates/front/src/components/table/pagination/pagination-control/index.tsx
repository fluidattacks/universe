/* eslint-disable functional/immutable-data */
import { Container, Text } from "@fluidattacks/design";
import type { RowData } from "@tanstack/react-table";
import { useCallback, useRef } from "react";
import { useTheme } from "styled-components";

import { ArrowButton } from "./styles";

import type { IPaginationProps } from "../../types";

const PaginationControl = <TData extends RowData>({
  hasNextPage = false,
  onNextPage = undefined,
  table,
}: Readonly<IPaginationProps<TData>>): JSX.Element => {
  const theme = useTheme();
  const {
    pagination: { pageIndex },
  } = table.getState();
  const pageCount = table.getPageCount();
  const isInLast = pageCount - pageIndex <= 2;
  const needLoadMore = hasNextPage && isInLast;
  const loading = useRef<boolean>(false);

  const goToNext = useCallback((): void => {
    if (loading.current) return;

    if (isInLast && onNextPage) {
      loading.current = true;
      void onNextPage().finally((): void => {
        table.setPageIndex(pageIndex + 1);
        loading.current = false;
      });
    } else {
      table.setPageIndex(pageIndex + 1);
    }
  }, [isInLast, onNextPage, pageIndex, table]);

  const handlePreviousPage = useCallback((): void => {
    table.previousPage();
  }, [table]);

  const handleFirstPage = useCallback((): void => {
    table.setPageIndex(0);
  }, [table]);

  const handleLastPage = useCallback((): void => {
    if (loading.current) return;

    if (onNextPage) {
      loading.current = true;
      void onNextPage().finally((): void => {
        table.setPageIndex(pageCount - 1);
        loading.current = false;
      });
    } else {
      table.setPageIndex(pageCount - 1);
    }
  }, [onNextPage, pageCount, table]);

  return (
    <Container alignItems={"center"} display={"flex"} gap={1}>
      <ArrowButton
        disabled={!table.getCanPreviousPage()}
        icon={"chevrons-left"}
        iconSize={"xxs"}
        id={"go-first"}
        onClick={handleFirstPage}
        variant={"ghost"}
      />

      <ArrowButton
        $padding={`4px 6px`}
        disabled={!table.getCanPreviousPage()}
        icon={"chevron-left"}
        iconSize={"xxs"}
        id={"go-previous"}
        onClick={handlePreviousPage}
        variant={"ghost"}
      />
      <Container>
        <Text
          color={theme.palette.gray[800]}
          display={"inline-block"}
          size={"sm"}
        >
          {`${pageIndex + 1}`}
        </Text>
        <Text
          color={theme.palette.gray[400]}
          display={"inline-block"}
          size={"sm"}
        >
          {`/${pageCount}`}
        </Text>
      </Container>
      <ArrowButton
        $padding={`4px 6px`}
        disabled={!table.getCanNextPage() && !needLoadMore}
        icon={"chevron-right"}
        iconSize={"xxs"}
        id={"go-next"}
        onClick={goToNext}
        variant={"ghost"}
      />
      <ArrowButton
        disabled={!table.getCanNextPage() && !needLoadMore}
        icon={"chevrons-right"}
        iconSize={"xxs"}
        id={"go-last"}
        onClick={handleLastPage}
        variant={"ghost"}
      />
    </Container>
  );
};

export { PaginationControl };
