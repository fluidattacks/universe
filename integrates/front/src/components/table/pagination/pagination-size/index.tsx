import { Text } from "@fluidattacks/design";
import type { RowData } from "@tanstack/react-table";
import { useCallback } from "react";
import { useTheme } from "styled-components";

import type { IPaginationProps } from "../../types";

const PaginationSize = <TData extends RowData>({
  size,
  table,
}: Readonly<IPaginationProps<TData> & { size: number }>): JSX.Element => {
  const theme = useTheme();
  const {
    pagination: { pageIndex, pageSize },
  } = table.getState();

  const getCurrentRange = useCallback(
    (currentPage: number, limit: number): string => {
      const fromRange = limit * currentPage + 1;
      const toRange = Math.min(limit * (currentPage + 1), size);
      const currentRange = `${fromRange} - ${toRange} of ${size}`;

      return currentRange;
    },
    [size],
  );
  const itemRange = getCurrentRange(pageIndex, pageSize);

  return (
    <Text color={theme.palette.gray[600]} display={"inline-block"} size={"sm"}>
      {itemRange}
    </Text>
  );
};

export { PaginationSize };
