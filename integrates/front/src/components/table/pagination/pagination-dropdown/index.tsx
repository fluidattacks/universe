import { useFloating } from "@floating-ui/react-dom";
import { Container, IconButton, Text } from "@fluidattacks/design";
import { isEqual } from "lodash";
import { useCallback, useEffect, useRef, useState } from "react";
import * as React from "react";
import { useTheme } from "styled-components";

import type { IPaginationDropdown } from "./types";

import { useClickOutside } from "hooks/use-click-outside";

const PaginationDropdown = ({
  items,
  initial,
}: Readonly<IPaginationDropdown>): JSX.Element => {
  const theme = useTheme();
  const ref = useRef<HTMLDivElement>(null);
  const [collapsed, setCollapsed] = useState(false);
  const [selected, setSelected] = useState(initial);
  const { refs, floatingStyles } = useFloating({
    placement: "top",
  });

  const handleCollapse = useCallback((): void => {
    setCollapsed(!collapsed);
  }, [collapsed]);

  const handleSelection = useCallback(
    (index: number): React.MouseEventHandler<HTMLDivElement> => {
      return (_: React.MouseEvent<HTMLDivElement>): void => {
        const { value, onClick } = items[index];
        setSelected(value);
        onClick();
        handleCollapse();
      };
    },
    [items, handleCollapse],
  );

  const onScroll = useCallback(
    (event: Event): void => {
      if (
        refs.floating.current &&
        !isEqual(refs.floating.current, event.target as Node)
      ) {
        setCollapsed(false);
      }
    },
    [refs.floating],
  );

  useClickOutside(
    ref.current,
    (): void => {
      setCollapsed(false);
    },
    true,
  );

  useEffect((): VoidFunction => {
    document.addEventListener("scroll", onScroll, true);

    return (): void => {
      document.removeEventListener("scroll", onScroll, true);
    };
  }, [onScroll]);

  return (
    <Container position={"relative"} ref={refs.setReference}>
      <Container
        alignItems={"center"}
        display={"flex"}
        gap={0.25}
        minHeight={"28px"}
      >
        <Text color={theme.palette.gray[600]} size={"sm"}>
          {selected}
        </Text>
        <IconButton
          color={theme.palette.gray[400]}
          icon={collapsed ? "chevron-up" : "chevron-down"}
          iconSize={"xxs"}
          id={"paginator-dropdown"}
          onClick={handleCollapse}
          variant={"ghost"}
        />
      </Container>
      {collapsed ? (
        <div ref={ref}>
          <Container
            bgColor={theme.palette.white}
            borderRadius={theme.spacing["0.25"]}
            display={"flex"}
            flexDirection={"column"}
            gap={0.25}
            position={"absolute"}
            px={0.25}
            py={0.25}
            ref={refs.setFloating}
            shadow={"md"}
            style={floatingStyles}
            zIndex={9999}
          >
            {items.map((item, index): JSX.Element => {
              const onItemClick = handleSelection(index);

              return (
                <Container
                  bgColor={
                    selected === item.value
                      ? theme.palette.gray[800]
                      : "transparent"
                  }
                  bgColorHover={
                    selected === item.value
                      ? theme.palette.gray[800]
                      : theme.palette.gray[100]
                  }
                  borderRadius={theme.spacing["0.25"]}
                  cursor={"pointer"}
                  key={item.value}
                  minWidth={"40px"}
                  onClick={onItemClick}
                  px={0.25}
                  py={0.25}
                  role={"button"}
                >
                  <Text
                    color={
                      selected === item.value
                        ? theme.palette.white
                        : theme.palette.gray[600]
                    }
                    size={"sm"}
                    textAlign={"center"}
                  >
                    {item.value}
                  </Text>
                </Container>
              );
            })}
          </Container>
        </div>
      ) : undefined}
    </Container>
  );
};

export { PaginationDropdown };
