/* eslint-disable functional/immutable-data */
import { useFloating } from "@floating-ui/react-dom";
import { Container, IconButton, Text } from "@fluidattacks/design";
import type { RowData } from "@tanstack/react-table";
import _ from "lodash";
import { useCallback, useEffect, useMemo, useRef, useState } from "react";
import * as React from "react";
import { useTheme } from "styled-components";

import type { IPaginationProps } from "../../../types";
import { useClickOutside } from "hooks/use-click-outside";

const PaginationDropdown = <TData extends RowData>({
  table,
}: Readonly<IPaginationProps<TData>>): JSX.Element => {
  const theme = useTheme();
  const ref = useRef<HTMLDivElement>(null);
  const [collapsed, setCollapsed] = useState(false);
  const [selected, setSelected] = useState(
    table.getState().pagination.pageSize,
  );
  const size = table.getRowCount();
  const { refs, floatingStyles } = useFloating({
    placement: "top",
  });

  const handleCollapse = useCallback((): void => {
    setCollapsed(!collapsed);
  }, [collapsed]);

  const onScroll = useCallback(
    (event: Event): void => {
      if (
        refs.floating.current &&
        !_.isEqual(refs.floating.current, event.target as Node)
      ) {
        setCollapsed(false);
      }
    },
    [refs.floating],
  );
  const SIZE_SMALL = 10;
  const SIZE_MEDIUM = 20;
  const SIZE_LARGE = 50;
  const SIZE_XLARGE = 100;
  const sizes = useMemo(
    (): number[] =>
      _.transform(
        [SIZE_SMALL, SIZE_MEDIUM, SIZE_LARGE, SIZE_XLARGE],
        (acc, current): boolean => {
          acc.push(current);

          return current <= size;
        },
        [] as number[],
      ),
    [size],
  );

  const handleSelection = useCallback(
    (index: number): React.MouseEventHandler<HTMLDivElement> => {
      return (_event: React.MouseEvent<HTMLDivElement>): void => {
        setSelected(index);
        table.setPageSize(index);
        handleCollapse();
      };
    },
    [table, setSelected, handleCollapse],
  );

  useClickOutside(
    ref.current,
    (): void => {
      setCollapsed(false);
    },
    true,
  );

  useEffect((): VoidFunction => {
    document.addEventListener("scroll", onScroll, true);

    return (): void => {
      document.removeEventListener("scroll", onScroll, true);
    };
  }, [onScroll]);

  return (
    <Container
      alignItems={"center"}
      bgColor={theme.palette.white}
      border={"1px solid"}
      borderColor={theme.palette.gray[200]}
      borderRadius={"4px"}
      display={"flex"}
      minHeight={"24px"}
      position={"relative"}
      px={0.25}
      ref={refs.setReference}
    >
      <Text color={theme.palette.gray[600]} pr={0.25} size={"sm"}>
        {selected}
      </Text>
      <IconButton
        color={theme.palette.gray[400]}
        icon={collapsed ? "chevron-up" : "chevron-down"}
        iconSize={"xxs"}
        id={"paginator-dropdown"}
        onClick={handleCollapse}
        variant={"ghost"}
      />
      <div ref={ref}>
        <Container
          bgColor={theme.palette.white}
          border={`1px solid ${theme.palette.gray[200]}`}
          borderRadius={theme.spacing["0.5"]}
          display={"flex"}
          flexDirection={"column"}
          position={"absolute"}
          px={0.25}
          py={0.25}
          ref={refs.setFloating}
          shadow={"md"}
          style={{
            ...floatingStyles,
            visibility: collapsed ? "visible" : "hidden",
          }}
          zIndex={9999}
        >
          {sizes.map((item): JSX.Element => {
            const onItemClick = handleSelection(item);

            return (
              <Container
                bgColor={
                  selected === item ? theme.palette.gray[800] : "transparent"
                }
                bgColorHover={
                  selected === item
                    ? theme.palette.gray[800]
                    : theme.palette.gray[100]
                }
                borderRadius={theme.spacing["0.25"]}
                cursor={"pointer"}
                key={item}
                maxWidth={"40px"}
                minWidth={"40px"}
                onClick={onItemClick}
                px={0.25}
                py={0.25}
                role={"button"}
              >
                <Text
                  color={
                    selected === item
                      ? theme.palette.white
                      : theme.palette.gray[600]
                  }
                  size={"sm"}
                  textAlign={"center"}
                >
                  {item}
                </Text>
              </Container>
            );
          })}
        </Container>
      </div>
    </Container>
  );
};

export { PaginationDropdown };
