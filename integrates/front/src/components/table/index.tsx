import { Container } from "@fluidattacks/design";
import {
  getCoreRowModel,
  getExpandedRowModel,
  getFacetedMinMaxValues,
  getFacetedRowModel,
  getFacetedUniqueValues,
  getFilteredRowModel,
  getPaginationRowModel,
  getSortedRowModel,
  useReactTable,
} from "@tanstack/react-table";
import type { FilterFn, RowData, Row as TableRow } from "@tanstack/react-table";
import _ from "lodash";
import type { ChangeEvent, FormEventHandler } from "react";
import { isValidElement, useCallback, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";

import { Pagination } from "./pagination";
import type { ITableOptions, ITableProps } from "./types";

import { Search } from "components/search";
import { TableContainer } from "components/table/styles";
import { Body } from "components/table/table-body";
import { ColumnToggle } from "components/table/table-column-toggle";
import { TableControls } from "components/table/table-controls";
import { Head } from "components/table/table-head";

const Table = <TData extends RowData>({
  columns,
  csvConfig,
  data,
  dataPrivate = false,
  expandedRow,
  extraButtons,
  extraBelowButtons,
  filters,
  filtersApplied,
  loadingData,
  onNextPage,
  onRowClick,
  onSearch,
  rightSideComponents,
  rowSelectionSetter,
  rowSelectionState,
  selectionMode = "checkbox",
  tableRef,
  options = {} as ITableOptions<TData>,
}: Readonly<ITableProps<TData>>): JSX.Element => {
  const [globalFilter, setGlobalFilter] = useState("");
  const [expanded, setExpanded] = useState({});
  const [totalData, setTotalData] = useState(0);
  const [rowSelection, setRowSelection] = useState({});
  const { t } = useTranslation();

  const {
    columnToggle = false,
    hasGlobalFilter = true,
    hasNextPage = false,
    enableRowSelection = true,
    enableSearchBar = true,
    searchPlaceholder,
    size,
    smallSearch = false,
    manualPagination = false,
    enableSorting = true,
  } = options;

  const radioSelectionHandler = useCallback(
    (row: TableRow<TData>): FormEventHandler =>
      (event: ChangeEvent<HTMLInputElement>): void => {
        event.stopPropagation();
        setRowSelection({});
        row.toggleSelected();
      },
    [],
  );

  const filterFun: FilterFn<TData> = (
    row: TableRow<TData>,
    columnId: string,
    filterValue: string,
  ): boolean => {
    return String(row.getValue(columnId))
      .toLowerCase()
      .includes(filterValue.toLowerCase());
  };

  useEffect((): void => {
    if (totalData === 0) {
      setTotalData(data.length);

      return;
    }
    if (totalData > data.length) {
      tableRef.setPagination({
        ...tableRef.pagination,
        pageIndex: 0,
      });
    }
    setTotalData(data.length);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data, tableRef]);

  const paginateData = useCallback(
    (rawData: TData[]): TData[] => {
      return rawData.slice(
        tableRef.pagination.pageIndex * tableRef.pagination.pageSize,
        (tableRef.pagination.pageIndex + 1) * tableRef.pagination.pageSize,
      );
    },
    [tableRef],
  );

  const table = useReactTable<TData>({
    autoResetAll: false,
    columns,
    data: manualPagination ? paginateData(data) : data,
    enableRowSelection,
    enableSorting,
    getCoreRowModel: getCoreRowModel(),
    getExpandedRowModel: getExpandedRowModel(),
    getFacetedMinMaxValues: getFacetedMinMaxValues(),
    getFacetedRowModel: getFacetedRowModel(),
    getFacetedUniqueValues: getFacetedUniqueValues(),
    getFilteredRowModel: getFilteredRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
    getRowCanExpand: (): boolean => expandedRow !== undefined,
    getSortedRowModel: getSortedRowModel(),
    globalFilterFn: filterFun,
    initialState: { columnOrder: tableRef.defaultColumnOrder },
    manualPagination,
    onColumnOrderChange: tableRef.setColumnOrder,
    onColumnVisibilityChange: tableRef.setColumnVisibility,
    onExpandedChange: setExpanded,
    onGlobalFilterChange: setGlobalFilter,
    onPaginationChange: tableRef.setPagination,
    onRowSelectionChange: setRowSelection,
    onSortingChange: tableRef.setSorting,
    rowCount: manualPagination ? size : undefined,
    state: {
      columnOrder: tableRef.columnOrder,
      columnPinning: tableRef.columnPinning,
      columnVisibility: tableRef.columnVisibility,
      expanded,
      globalFilter: hasGlobalFilter ? globalFilter : null,
      pagination: tableRef.pagination,
      rowSelection,
      sorting: tableRef.sorting,
    },
  });

  const globalFilterHandler = useCallback(
    (value: string): void => {
      const {
        pagination: { pageIndex },
      } = table.getState();

      setGlobalFilter(value);
      if (pageIndex !== 0) {
        table.setPageIndex(0);
      }

      if (onSearch) {
        onSearch(value);
      }
    },
    [onSearch, table],
  );

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  function helper(val1: any, val2: any): boolean | undefined {
    if (
      (_.isFunction(val1) && _.isFunction(val2)) ||
      (_.isObject(val1) && isValidElement(val1))
    ) {
      return true;
    }

    return undefined;
  }

  /*
   * Next useEffect() takes the information of rowSelectionState
   * (row originals) and selects the equivalent rows in the rowSelection so
   * both are in sync, this is to support unselecting rows outside table
   */

  useEffect((): void => {
    if (rowSelectionState === undefined) {
      return undefined;
    }
    table.getExpandedRowModel().rows.forEach((row: TableRow<TData>): void => {
      if (
        _.some(rowSelectionState, (selected): boolean =>
          _.isEqualWith(row.original, selected, helper),
        )
      ) {
        if (row.getIsSelected()) {
          return undefined;
        }
        row.toggleSelected();
      } else {
        if (row.getIsSelected()) {
          row.toggleSelected();
        }

        return undefined;
      }

      return undefined;
    });

    return undefined;
  }, [rowSelectionState, table]);

  useEffect((): void => {
    rowSelectionSetter?.(
      table
        .getSelectedRowModel()
        .flatRows.map((row: TableRow<TData>): TData => row.original),
    );
  }, [rowSelection, rowSelectionSetter, table]);

  return (
    <div
      className={`w-100${dataPrivate ? " ssr-block" : ""}`}
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...(dataPrivate && { "data-private": true })}
      id={tableRef.id}
    >
      <Container
        alignItems={"flex-start"}
        display={"flex"}
        justify={"space-between"}
        pb={1.25}
        pt={0.5}
        scroll={"none"}
      >
        <Container alignItems={"center"} display={"flex"} gap={0.5}>
          {columnToggle ? (
            <ColumnToggle
              columnOrder={tableRef.defaultColumnOrder}
              table={table}
            />
          ) : undefined}
          {filters}
        </Container>
        <Container alignItems={"center"} display={"flex"} gap={0.5}>
          <TableControls
            csvConfig={csvConfig}
            data={data}
            extraButtons={extraButtons}
          />
          {enableSearchBar ? (
            <Container width={smallSearch ? "max-content" : "250px"}>
              <Search
                enableSearchBar={enableSearchBar}
                handleOnKeyPressed={globalFilterHandler}
                placeHolder={searchPlaceholder ?? t("table.search")}
                smallSearch={smallSearch}
              />
            </Container>
          ) : undefined}
          {rightSideComponents}
        </Container>
      </Container>
      {extraBelowButtons ? (
        <Container
          alignItems={"center"}
          display={"flex"}
          gap={0.5}
          justify={"space-between"}
          pb={1.25}
          width={"100%"}
        >
          {extraBelowButtons}
        </Container>
      ) : undefined}
      {filtersApplied ?? undefined}
      <TableContainer
        $clickable={onRowClick !== undefined}
        $rowSelection={rowSelectionSetter !== undefined}
      >
        <table>
          <Head
            expandedRow={expandedRow}
            rowSelectionSetter={rowSelectionSetter}
            selectionMode={selectionMode}
            table={table}
          />
          <Body
            data={data}
            expandedRow={expandedRow}
            loadingData={loadingData}
            onRowClick={onRowClick}
            radioSelectionHandler={radioSelectionHandler}
            rowSelectionSetter={rowSelectionSetter}
            selectionMode={selectionMode}
            table={table}
          />
        </table>
      </TableContainer>
      <Pagination
        hasNextPage={hasNextPage}
        onNextPage={onNextPage}
        size={size ?? table.getFilteredRowModel().rows.length}
        table={table}
      />
    </div>
  );
};

export { Table };
