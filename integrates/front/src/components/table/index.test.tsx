import type { ColumnDef, Row } from "@tanstack/react-table";
import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { useState } from "react";
import * as React from "react";

import type { ICsvConfig, ITableOptions } from "./types";
import { filterDate } from "./utils";

import { CustomThemeProvider } from "components/colors";
import { Table } from "components/table";
import { useTable } from "hooks";
import { render } from "mocks";

interface IRandomData {
  color: string;
  date: string;
  name: string;
  numberrange: number;
  numbertrack: number;
}

const columns: ColumnDef<IRandomData>[] = [
  {
    accessorKey: "name",
    header: "Name",
  },
  {
    accessorKey: "numberrange",
    header: "Number Range",
    meta: { filterType: "numberRange" },
  },
  {
    accessorKey: "date",
    filterFn: filterDate,
    header: "Entrance Date",
    meta: { filterType: "dateRange" },
  },
  {
    accessorKey: "color",
    header: "Shirt Color",
    meta: { filterType: "select" },
  },
  {
    accessorKey: "numbertrack",
    header: "Track Number",
    meta: { filterType: "number" },
  },
];

const data: IRandomData[] = [
  {
    color: "blue",
    date: "2022-01-20",
    name: "Daria Hays",
    numberrange: 12,
    numbertrack: 6,
  },
  {
    color: "blue",
    date: "2022-06-18",
    name: "Palmer Wilcox",
    numberrange: 12,
    numbertrack: 2,
  },
  {
    color: "white",
    date: "2023-01-22",
    name: "Merritt Sherman",
    numberrange: 13,
    numbertrack: 3,
  },
  {
    color: "white",
    date: "2022-08-28",
    name: "Forrest Ortiz",
    numberrange: 7,
    numbertrack: 1,
  },
  {
    color: "red",
    date: "2023-08-19",
    name: "April Long",
    numberrange: 6,
    numbertrack: 6,
  },
  {
    color: "red",
    date: "2022-06-20",
    name: "Desirae Bailey",
    numberrange: 9,
    numbertrack: 2,
  },
  {
    color: "brown",
    date: "2023-03-31",
    name: "Kato Soto",
    numberrange: 11,
    numbertrack: 2,
  },
  {
    color: "brown",
    date: "2023-07-08",
    name: "Emerald Brennan",
    numberrange: 11,
    numbertrack: 6,
  },
  {
    color: "black",
    date: "2022-05-12",
    name: "Donovan Woods",
    numberrange: 9,
    numbertrack: 8,
  },
  {
    color: "black",
    date: "2022-03-07",
    name: "Brandon Hernandez",
    numberrange: 6,
    numbertrack: 0,
  },
  {
    color: "blue",
    date: "2023-08-23",
    name: "Phyllis Garrett",
    numberrange: 11,
    numbertrack: 9,
  },
  {
    color: "blue",
    date: "2022-06-29",
    name: "Theodore Daniels",
    numberrange: 9,
    numbertrack: 10,
  },
  {
    color: "white",
    date: "2023-08-28",
    name: "Coby Delgado",
    numberrange: 12,
    numbertrack: 13,
  },
  {
    color: "white",
    date: "2023-06-12",
    name: "Lareina Shaffer",
    numberrange: 14,
    numbertrack: 6,
  },
  {
    color: "red",
    date: "2023-04-16",
    name: "Arthur Richardson",
    numberrange: 12,
    numbertrack: 1,
  },
  {
    color: "red",
    date: "2021-07-30",
    name: "Amber Morgan",
    numberrange: 8,
    numbertrack: 3,
  },
  {
    color: "brown",
    date: "2021-01-26",
    name: "Justin Clay",
    numberrange: 10,
    numbertrack: 3,
  },
  {
    color: "brown",
    date: "2023-04-01",
    name: "Timothy Powers",
    numberrange: 0,
    numbertrack: 2,
  },
  {
    color: "black",
    date: "2022-03-24",
    name: "Marshall Massey",
    numberrange: 7,
    numbertrack: 7,
  },
  {
    color: "black",
    date: "2023-05-29",
    name: "Brian Reeves",
    numberrange: 1,
    numbertrack: 5,
  },
  {
    color: "blue",
    date: "2022-10-19",
    name: "Lesley Howard",
    numberrange: 7,
    numbertrack: 9,
  },
  {
    color: "blue",
    date: "2022-06-24",
    name: "Ivor Delgado",
    numberrange: 1,
    numbertrack: 0,
  },
  {
    color: "white",
    date: "2022-08-17",
    name: "Leila William",
    numberrange: 7,
    numbertrack: 4,
  },
  {
    color: "white",
    date: "2023-05-12",
    name: "Steel Dominguez",
    numberrange: 5,
    numbertrack: 8,
  },
  {
    color: "red",
    date: "2023-02-09",
    name: "Beau Vaughn",
    numberrange: 14,
    numbertrack: 3,
  },
  {
    color: "red",
    date: "2022-08-04",
    name: "Mannix Bradley",
    numberrange: 15,
    numbertrack: 4,
  },
  {
    color: "brown",
    date: "2023-07-15",
    name: "Dean Zimmerman",
    numberrange: 6,
    numbertrack: 2,
  },
  ...Array.from(
    { length: 100 },
    (_, index): IRandomData => ({
      color: ["black", "blue", "white", "red", "brown"][index % 5],
      date: `202${index % 4}-0${(index % 9) + 1}-1${(index % 9) + 1}`,
      name: `Generated Name ${index + 1}`,
      numberrange: (index % 16) + 1,
      numbertrack: index % 10,
    }),
  ),
];

describe("table", (): void => {
  interface ITestComponentProps {
    testCsvConfig?: ICsvConfig;
    testDataSelection?: boolean;
    testData: IRandomData[];
    testExpandedRow?: (row: Row<IRandomData>) => JSX.Element;
    testOptions?: ITableOptions<IRandomData>;
    testSelectionMode?: "checkbox" | "radio";
    onNextPage?: jest.Mock;
  }

  const TestComponent: React.FC<ITestComponentProps> = ({
    testCsvConfig,
    testData,
    testDataSelection,
    testExpandedRow,
    testOptions,
    testSelectionMode,
    onNextPage,
  }): JSX.Element => {
    const [tableData, setTableData] = useState(testData);
    const [selectedData, setSelectedData] = useState<IRandomData[]>([]);
    const tableRef = useTable("testTable");

    const handleNextPage = React.useCallback(async (): Promise<void> => {
      if (onNextPage) {
        const newData = await onNextPage();
        if (Array.isArray(newData) && newData.length > 0) {
          setTableData((prev): IRandomData[] => [
            ...prev,
            ...(newData as IRandomData[]),
          ]);
        }
      }
    }, [onNextPage]);

    return (
      <CustomThemeProvider>
        <p>{JSON.stringify(selectedData[0])}</p>
        <Table
          columns={columns}
          csvConfig={testCsvConfig}
          data={tableData}
          expandedRow={testExpandedRow}
          onNextPage={handleNextPage}
          options={testOptions}
          rowSelectionSetter={
            testDataSelection === true ? setSelectedData : undefined
          }
          rowSelectionState={
            testDataSelection === true ? selectedData : undefined
          }
          selectionMode={testSelectionMode}
          tableRef={tableRef}
        />
      </CustomThemeProvider>
    );
  };

  it("should render csv button", (): void => {
    expect.hasAssertions();

    render(<TestComponent testCsvConfig={{ export: true }} testData={data} />);

    expect(screen.getByRole("table")).toBeInTheDocument();
    expect(
      screen.getByRole("button", { name: "group.findings.exportCsv.text" }),
    ).toBeInTheDocument();
  });

  it("should not render pagination controls", (): void => {
    expect.hasAssertions();

    render(
      <CustomThemeProvider>
        <TestComponent testData={data.slice(0, 10)} />
      </CustomThemeProvider>,
    );

    expect(screen.getByRole("table")).toBeInTheDocument();
    expect(screen.queryAllByRole("row")).toHaveLength(11);

    expect(
      screen.queryByRole("button", { name: "paginator-dropdown" }),
    ).not.toBeInTheDocument();

    expect(
      screen.queryByRole("button", { name: "go-last" }),
    ).not.toBeInTheDocument();

    expect(
      screen.queryByRole("button", { name: "go-next" }),
    ).not.toBeInTheDocument();

    expect(
      screen.queryByRole("button", { name: "go-first" }),
    ).not.toBeInTheDocument();

    expect(
      screen.queryByRole("button", { name: "go-previous" }),
    ).not.toBeInTheDocument();
  });

  it("should render pagination controls", async (): Promise<void> => {
    expect.hasAssertions();

    const onNextPage = jest.fn().mockResolvedValue(undefined);

    render(
      <CustomThemeProvider>
        <TestComponent onNextPage={onNextPage} testData={data.slice(0, 11)} />
      </CustomThemeProvider>,
    );

    expect(screen.getByRole("table")).toBeInTheDocument();
    expect(screen.queryAllByRole("row")).toHaveLength(11);

    expect(screen.getByText("1 - 10 of 11")).toBeInTheDocument();

    expect(
      screen.queryByRole("button", { name: "paginator-dropdown" }),
    ).toBeInTheDocument();

    expect(
      screen.queryByRole("button", { name: "go-last" }),
    ).toBeInTheDocument();

    expect(
      screen.queryByRole("button", { name: "go-next" }),
    ).toBeInTheDocument();

    expect(
      screen.queryByRole("button", { name: "go-first" }),
    ).toBeInTheDocument();

    expect(
      screen.queryByRole("button", {
        name: "go-previous",
      }),
    ).toBeInTheDocument();

    expect(screen.getAllByText("1")).toHaveLength(2);

    expect(screen.getByRole("button", { name: "go-last" })).not.toBeDisabled();
    expect(screen.getByRole("button", { name: "go-next" })).not.toBeDisabled();

    await userEvent.click(screen.getByRole("button", { name: "go-last" }));
    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(2);
    });

    expect(screen.queryAllByText("1")).toHaveLength(0);
    expect(screen.getByText("2")).toBeInTheDocument();

    expect(screen.getByRole("button", { name: "go-last" })).toBeDisabled();
    expect(screen.getByRole("button", { name: "go-next" })).toBeDisabled();

    expect(screen.getByRole("button", { name: "go-first" })).not.toBeDisabled();
    expect(
      screen.getByRole("button", { name: "go-previous" }),
    ).not.toBeDisabled();

    await userEvent.click(screen.getByRole("button", { name: "go-first" }));
    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(11);
    });

    expect(screen.getByRole("button", { name: "go-first" })).toBeDisabled();
    expect(screen.getByRole("button", { name: "go-previous" })).toBeDisabled();

    expect(screen.getAllByText("1")).toHaveLength(2);

    await userEvent.click(screen.getByRole("button", { name: "go-next" }));

    expect(onNextPage).toHaveBeenCalledTimes(2);

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(2);
    });

    expect(screen.queryAllByText("1")).toHaveLength(0);
    expect(screen.getByText("2")).toBeInTheDocument();

    await userEvent.click(screen.getByRole("button", { name: "go-previous" }));
    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(11);
    });

    expect(screen.getAllByText("1")).toHaveLength(2);
  });

  it("should render pagination dropdown", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <CustomThemeProvider>
        <TestComponent testData={data.slice(0, 11)} />
      </CustomThemeProvider>,
    );

    expect(screen.getByRole("table")).toBeInTheDocument();
    expect(screen.queryAllByRole("row")).toHaveLength(11);

    expect(
      screen.queryByRole("button", { name: "paginator-dropdown" }),
    ).toBeInTheDocument();

    await userEvent.click(
      screen.getByRole("button", { name: "paginator-dropdown" }),
    );
    await userEvent.click(screen.getByRole("button", { name: "11" }));
  });

  it("should change pagination", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <CustomThemeProvider>
        <TestComponent testData={data.slice(0, 27)} />
      </CustomThemeProvider>,
    );

    expect(screen.getByRole("table")).toBeInTheDocument();
    expect(screen.queryAllByRole("row")).toHaveLength(11);

    await userEvent.click(
      screen.getByRole("button", { name: "paginator-dropdown" }),
    );
    await userEvent.click(screen.getByRole("button", { name: "20" }));
    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(21);
    });

    await userEvent.click(
      screen.getByRole("button", { name: "paginator-dropdown" }),
    );
    await userEvent.click(screen.getByRole("button", { name: "27" }));
    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(28);
    });

    await userEvent.click(
      screen.getByRole("button", { name: "paginator-dropdown" }),
    );
    await userEvent.click(screen.getByRole("button", { name: "10" }));
    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(11);
    });

    await userEvent.click(screen.getByRole("button", { name: "go-last" }));
    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(8);
    });

    await userEvent.click(screen.getByRole("button", { name: "go-first" }));
    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(11);
    });

    expect(screen.queryByText("Daria Hays")).toBeInTheDocument();

    await userEvent.click(screen.getByRole("button", { name: "go-next" }));
    await waitFor((): void => {
      expect(screen.queryByText("Phyllis Garrett")).toBeInTheDocument();
    });

    expect(screen.queryByText("Daria Hays")).not.toBeInTheDocument();

    await userEvent.click(screen.getByRole("button", { name: "go-previous" }));
    await waitFor((): void => {
      expect(screen.queryByText("Daria Hays")).toBeInTheDocument();
    });

    expect(screen.queryByText("Phyllis Garrett")).not.toBeInTheDocument();

    await userEvent.click(screen.getByRole("button", { name: "go-next" }));
    await waitFor((): void => {
      expect(screen.queryByText("Theodore Daniels")).toBeInTheDocument();
    });

    await userEvent.click(screen.getByRole("button", { name: "go-next" }));
    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(8);
    });

    await userEvent.click(screen.getByRole("button", { name: "go-first" }));
    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(11);
    });

    await userEvent.click(screen.getByRole("button", { name: "go-last" }));
    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(8);
    });

    await userEvent.click(screen.getByRole("button", { name: "go-first" }));
    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(11);
    });

    expect(screen.queryByText("Daria Hays")).toBeInTheDocument();
  });

  it("should hide columns", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <CustomThemeProvider>
        <TestComponent testData={data} testOptions={{ columnToggle: true }} />
      </CustomThemeProvider>,
    );

    expect(screen.getByRole("table")).toBeInTheDocument();
    expect(
      screen.getByRole("button", {
        name: /group\.findings\.tableSet\.btn\.text .*/u,
      }),
    ).toBeInTheDocument();

    expect(
      screen.getByRole("columnheader", { name: "Shirt Color" }),
    ).toBeInTheDocument();

    await userEvent.click(
      screen.getByRole("button", {
        name: /group\.findings\.tableSet\.btn\.text .*/u,
      }),
    );
    await waitFor((): void => {
      expect(screen.getByText("Manage columns")).toBeInTheDocument();
    });

    expect(screen.getAllByRole("checkbox")).toHaveLength(5);

    const colorInput = screen.getByRole("checkbox", { name: "Shirt Color" });
    await userEvent.click(colorInput);

    expect(
      screen.queryByRole("columnheader", { name: "Shirt Color" }),
    ).not.toBeInTheDocument();

    await userEvent.click(colorInput);

    expect(
      screen.queryByRole("columnheader", { name: "Shirt Color" }),
    ).toBeInTheDocument();
  });

  it("should sort data", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <CustomThemeProvider>
        <TestComponent testData={data} testOptions={{ columnToggle: true }} />
      </CustomThemeProvider>,
    );

    expect(screen.getByRole("table")).toBeInTheDocument();
    expect(
      screen.getByRole("columnheader", { name: "Name" }),
    ).toBeInTheDocument();

    expect(screen.queryAllByRole("cell")[0].textContent).toBe("Daria Hays");

    await userEvent.click(screen.getByText("Name"));

    await waitFor((): void => {
      expect(screen.queryAllByRole("cell")[0].textContent).toBe("Amber Morgan");
    });

    await userEvent.click(screen.getByText("Name"));

    await waitFor((): void => {
      expect(screen.queryAllByRole("cell")[0].textContent).toBe(
        "Timothy Powers",
      );
    });
  });

  it("should only select one when radio selection", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <TestComponent
        testData={[data[0]]}
        testDataSelection={true}
        testSelectionMode={"radio"}
      />,
    );

    expect(screen.getByRole("table")).toBeInTheDocument();
    expect(screen.queryAllByRole("radio", { checked: true })).toHaveLength(0);

    await userEvent.click(screen.queryAllByRole("radio")[0]);
    await waitFor((): void => {
      expect(
        screen.getByText(
          '{"color":"blue","date":"2022-01-20","name":"Daria Hays","numberrange":12,"numbertrack":6}',
        ),
      ).toBeInTheDocument();
    });

    expect(screen.queryAllByRole("radio", { checked: true })).toHaveLength(1);
  });

  it("should select all checkboxes", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <TestComponent
        testData={data}
        testDataSelection={true}
        testSelectionMode={"checkbox"}
      />,
    );

    expect(screen.getByRole("table")).toBeInTheDocument();
    expect(screen.queryAllByRole("checkbox", { checked: true })).toHaveLength(
      0,
    );

    await userEvent.click(screen.queryAllByRole("checkbox")[0]);
    await waitFor((): void => {
      expect(screen.queryAllByRole("checkbox", { checked: true })).toHaveLength(
        11,
      );
    });

    await userEvent.click(screen.queryAllByRole("checkbox")[0]);
    await waitFor((): void => {
      expect(screen.queryAllByRole("checkbox", { checked: true })).toHaveLength(
        0,
      );
    });
  });

  it("should select many checkboxes", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <TestComponent
        testData={data}
        testDataSelection={true}
        testSelectionMode={"checkbox"}
      />,
    );

    expect(screen.getByRole("table")).toBeInTheDocument();
    expect(screen.queryAllByRole("checkbox", { checked: true })).toHaveLength(
      0,
    );

    await userEvent.click(screen.queryAllByRole("checkbox")[1]);
    await waitFor((): void => {
      expect(screen.queryAllByRole("checkbox", { checked: true })).toHaveLength(
        1,
      );
    });

    await userEvent.click(screen.queryAllByRole("checkbox")[2]);
    await waitFor((): void => {
      expect(screen.queryAllByRole("checkbox", { checked: true })).toHaveLength(
        2,
      );
    });

    await userEvent.click(screen.queryAllByRole("checkbox")[3]);
    await waitFor((): void => {
      expect(screen.queryAllByRole("checkbox", { checked: true })).toHaveLength(
        3,
      );
    });
  });

  it("should expand rows", async (): Promise<void> => {
    expect.hasAssertions();

    const handleRowExpandMock = jest.fn();

    render(
      <TestComponent
        testData={data.slice(0, 2)}
        testDataSelection={true}
        testExpandedRow={handleRowExpandMock}
      />,
    );

    expect(screen.getByRole("table")).toBeInTheDocument();

    await userEvent.click(screen.queryAllByRole("button", { name: "" })[1]);

    expect(handleRowExpandMock).toHaveBeenCalledTimes(1);
  });

  it("should render table without sticky header", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <CustomThemeProvider>
        <TestComponent testData={data} />
      </CustomThemeProvider>,
    );
    await waitFor((): void => {
      const tableHeader = document.querySelector("th");

      expect(tableHeader?.style.position).not.toBe("sticky");
    });
  });

  it("should render 100 rows", async (): Promise<void> => {
    expect.hasAssertions();

    const onNextPage = jest.fn().mockResolvedValue(undefined);

    render(
      <CustomThemeProvider>
        <TestComponent onNextPage={onNextPage} testData={data} />
      </CustomThemeProvider>,
    );

    expect(screen.queryAllByRole("row")).toHaveLength(11);

    expect(screen.getByText("1 - 10 of 127")).toBeInTheDocument();
    expect(
      screen.queryByRole("button", { name: "paginator-dropdown" }),
    ).toBeInTheDocument();

    await userEvent.click(
      screen.getByRole("button", { name: "paginator-dropdown" }),
    );

    await userEvent.click(screen.getByRole("button", { name: "100" }));

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(101);
    });

    expect(onNextPage).toHaveBeenCalledTimes(0);
  });

  it("should keep current page when fetching more data", async (): Promise<void> => {
    expect.hasAssertions();

    const mockFetchData: IRandomData[] = [
      {
        color: "blue",
        date: "2023-01-17",
        name: "Lucille Nolan",
        numberrange: 14,
        numbertrack: 9,
      },
      {
        color: "blue",
        date: "2021-11-30",
        name: "Tristan Burke",
        numberrange: 10,
        numbertrack: 3,
      },
    ];

    const onNextPage = jest.fn().mockResolvedValue(mockFetchData);

    render(
      <CustomThemeProvider>
        <TestComponent onNextPage={onNextPage} testData={data.slice(0, 11)} />
      </CustomThemeProvider>,
    );

    expect(screen.queryAllByRole("row")).toHaveLength(11);

    expect(screen.getByText("1 - 10 of 11")).toBeInTheDocument();

    await userEvent.click(screen.getByRole("button", { name: "go-last" }));

    expect(onNextPage).toHaveBeenCalledTimes(1);

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(4);
    });

    expect(screen.getAllByText("2")).toHaveLength(1);
    expect(screen.getAllByText("/2")).toHaveLength(1);
  });
});
