import { Container, Lottie } from "@fluidattacks/design";

import { redLoader } from "resources";

const formatInProcessHandler = (text: string | undefined): JSX.Element => {
  return (
    <Container display={"flex"} flexDirection={"row"}>
      <Lottie animationData={redLoader} size={16} />
      &nbsp;
      {text}
    </Container>
  );
};

export { formatInProcessHandler };
