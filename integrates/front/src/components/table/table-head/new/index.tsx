import { Container } from "@fluidattacks/design";
import type { Header, RowData, Table } from "@tanstack/react-table";
import { flexRender } from "@tanstack/react-table";
import { isNil } from "lodash";
import _ from "lodash";
import { useCallback } from "react";
import * as React from "react";

import { TableResizer } from "../../styles";
import { theme } from "components/colors";
import { SortIcon } from "components/table/sort-icon";

interface IHeadProps<TData extends RowData> {
  readonly table: Table<TData>;
  readonly containerRef: React.RefObject<HTMLDivElement>;
  readonly groupHeaders?: boolean;
}

const COLUMN_OFFSET = 16;
const HEADER_GROUP_OFFSET = 105;
const HEADER_OFFSET = 80;

const Head = <TData extends RowData>({
  table,
  containerRef,
  groupHeaders = false,
}: IHeadProps<TData>): JSX.Element => {
  const onSort = useCallback(
    (header: Header<TData, unknown>): VoidFunction => {
      return (): void => {
        if (header.column.getCanSort()) {
          table.setPageIndex(0);
          header.column.toggleSorting();
        }
      };
    },
    [table],
  );

  const onResize = (
    header: Header<TData, unknown>,
  ): React.MouseEventHandler<HTMLDivElement> => {
    return (event: React.MouseEvent<HTMLDivElement>): void => {
      header.getResizeHandler()(event);
    };
  };

  if (groupHeaders)
    return (
      <thead>
        {table.getHeaderGroups().map(
          (headerGroup): JSX.Element => (
            <tr data-testid={"row-headers"} key={headerGroup.id}>
              {headerGroup.headers.map((header): React.ReactNode => {
                // REFAC NEEDED: This is a workaround to full width the last column
                const { isPlaceholder } = header;
                const currentHeader = isPlaceholder
                  ? header.subHeaders[0]
                  : header;

                const tableSize = table.getTotalSize();
                const totalSize = containerRef.current?.clientWidth ?? 0;
                const isLastColumn =
                  header.index === headerGroup.headers.length - 1;
                const resizeLastColumn = isLastColumn && totalSize > 0;

                const normalSize =
                  currentHeader.getSize() +
                  COLUMN_OFFSET * currentHeader.colSpan;
                const fillSize =
                  tableSize < totalSize
                    ? totalSize - tableSize + normalSize - HEADER_GROUP_OFFSET
                    : normalSize;

                /**
                 * Header placeholder includes a rowSpan.
                 * This boolean prevents rendering the child headers.
                 */
                const dontRenderChild =
                  _.isUndefined(header.column.parent) && header.depth > 1;

                if (dontRenderChild) {
                  return null;
                }

                return (
                  <th
                    colSpan={currentHeader.colSpan}
                    id={`header-${currentHeader.id}`}
                    key={currentHeader.id}
                    rowSpan={isPlaceholder ? 0 : 1}
                  >
                    <Container
                      alignItems={"center"}
                      borderBottom={
                        isPlaceholder
                          ? undefined
                          : `1px solid${theme.palette.white}`
                      }
                      display={"flex"}
                      gap={0.25}
                      height={isPlaceholder ? "6rem" : "3rem"}
                      lineSpacing={0}
                      onClick={onSort(currentHeader)}
                      pl={0.5}
                      pt={isPlaceholder ? 3 : 0}
                      whiteSpace={"nowrap"}
                      width={
                        resizeLastColumn ? `${fillSize}px` : `${normalSize}px`
                      }
                    >
                      {flexRender(
                        currentHeader.column.columnDef.header,
                        currentHeader.getContext(),
                      )}
                      {currentHeader.column.getCanSort() ? (
                        <SortIcon
                          variant={currentHeader.column.getIsSorted()}
                        />
                      ) : undefined}
                    </Container>
                    {currentHeader.column.getCanResize() && (
                      <TableResizer onMouseDown={onResize(header)} />
                    )}
                  </th>
                );
              })}
            </tr>
          ),
        )}
      </thead>
    );

  const [headerGroup, hasSubHeaders] = table.getHeaderGroups();
  const group = isNil(hasSubHeaders) ? headerGroup : hasSubHeaders;

  return (
    <thead>
      <tr data-testid={"row-headers"}>
        {group.headers.map((header): React.ReactNode => {
          // REFAC NEEDED: This is a workaround to full width the last column
          const totalSize = containerRef.current?.clientWidth ?? 0;
          const isLastColumn = header.index === group.headers.length - 1;
          const resizeLastColumn = isLastColumn && totalSize > 0;

          const normalSize = header.getSize() + COLUMN_OFFSET;
          const fillSize = Math.max(
            header.getSize() + HEADER_OFFSET,
            totalSize - table.getTotalSize() + HEADER_OFFSET,
          );

          return (
            <th id={`header-${header.id}`} key={header.id}>
              <Container
                alignItems={"center"}
                bgColor={theme.palette.gray[200]}
                display={"flex"}
                gap={0.25}
                height={"3rem"}
                lineSpacing={0}
                onClick={onSort(header)}
                pl={0.5}
                pr={1}
                whiteSpace={"nowrap"}
                width={resizeLastColumn ? `${fillSize}px` : `${normalSize}px`}
              >
                {flexRender(
                  header.column.columnDef.header,
                  header.getContext(),
                )}
                {header.column.getCanSort() ? (
                  <SortIcon variant={header.column.getIsSorted()} />
                ) : undefined}
              </Container>
              {header.column.getCanResize() && (
                <TableResizer onMouseDown={onResize(header)} />
              )}
            </th>
          );
        })}
      </tr>
    </thead>
  );
};

export { Head };
