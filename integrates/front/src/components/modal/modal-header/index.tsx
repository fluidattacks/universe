import { IconButton, Text } from "@fluidattacks/design";
import isEmpty from "lodash/isEmpty";
import { useCallback, useEffect } from "react";
import * as React from "react";
import { useTheme } from "styled-components";

import { Header, Title } from "../styles";
import type { IModalProps } from "../types";

interface IModalHeaderProps {
  title: React.ReactNode | string;
  description: React.ReactNode | string;
  modalRef: IModalProps["modalRef"];
  otherActions?: React.ReactNode;
  onClose?: () => void;
}

const ModalHeader = ({
  title,
  description,
  modalRef,
  otherActions,
  onClose,
}: Readonly<IModalHeaderProps>): JSX.Element => {
  const theme = useTheme();
  const handleClose = useCallback((): void => {
    onClose?.();
    modalRef.close();
  }, [modalRef, onClose]);

  useEffect((): (() => void) => {
    const handleKeydown = (event: KeyboardEvent): void => {
      if (event.key === "Escape") {
        handleClose();
      }
    };
    window.addEventListener("keydown", handleKeydown);

    return (): void => {
      window.removeEventListener("keydown", handleKeydown);
    };
  }, [handleClose]);
  const Description = useCallback((): React.ReactNode | undefined => {
    if (isEmpty(description)) return undefined;
    if (typeof description === "string") {
      return (
        <Text mt={0.5} size={"sm"}>
          {description}
        </Text>
      );
    }

    return description;
  }, [description]);

  return (
    <Header>
      <Title>
        {typeof title === "string" ? (
          <Text
            color={theme.palette.gray[800]}
            fontWeight={"bold"}
            mr={1}
            size={"xl"}
          >
            {title}
          </Text>
        ) : (
          title
        )}
        <div className={"flex"}>
          {otherActions}
          <IconButton
            color={theme.palette.gray[300]}
            icon={"close"}
            iconSize={"xs"}
            iconType={"fa-light"}
            id={"modal-close"}
            onClick={handleClose}
            variant={"ghost"}
          />
        </div>
      </Title>
      <Description />
    </Header>
  );
};

export { ModalHeader };
