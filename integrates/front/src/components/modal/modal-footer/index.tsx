import { Button } from "@fluidattacks/design";
import _ from "lodash";
import { useCallback } from "react";
import * as React from "react";

import { Footer } from "../styles";
import type { IModalProps } from "../types";
import { Checkbox } from "components/checkbox";

interface IModalFooterProps {
  modalRef: IModalProps["modalRef"];
  dontShowCheckbox?: boolean;
  cancelButton: IModalProps["cancelButton"];
  confirmButton: IModalProps["confirmButton"];
}

const ModalFooter = ({
  modalRef,
  dontShowCheckbox = false,
  confirmButton = undefined,
  cancelButton = undefined,
}: Readonly<IModalFooterProps>): JSX.Element | null => {
  const { close, setDontShow } = modalRef;
  const handleCancel = useCallback((): void => {
    cancelButton?.onClick();
    close();
  }, [cancelButton, close]);

  const handleConfirm = useCallback((): void => {
    confirmButton?.onClick();
    close();
  }, [confirmButton, close]);

  const handleCheckDontShow = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>): void => {
      if (setDontShow) {
        setDontShow(event.target.checked);
      }
    },
    [setDontShow],
  );

  const isThereExtraInfo = [confirmButton, cancelButton].some(
    (item): boolean => !_.isEmpty(item),
  );

  const fullInfo = [dontShowCheckbox, confirmButton, cancelButton].every(
    Boolean,
  );

  if (isThereExtraInfo) {
    return (
      <Footer $fullInfo={fullInfo} $gap={0.75}>
        {Boolean(dontShowCheckbox) && (
          <Checkbox
            name={`dont-show-${modalRef.name}`}
            onClick={handleCheckDontShow as () => void}
            value={"Don't show again"}
          />
        )}
        <div>
          {_.isObject(cancelButton) && (
            <Button onClick={handleCancel} variant={"tertiary"} width={"100%"}>
              {cancelButton.text}
            </Button>
          )}
          {_.isObject(confirmButton) && (
            <Button onClick={handleConfirm} variant={"primary"} width={"100%"}>
              {confirmButton.text}
            </Button>
          )}
        </div>
      </Footer>
    );
  }

  return null;
};

export { ModalFooter };
