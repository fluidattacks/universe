import { Button, Container } from "@fluidattacks/design";
import { useTranslation } from "react-i18next";

import type { IModalConfirmProps } from "../types";

const ModalConfirm = ({
  disabled,
  id = "modal-confirm",
  onCancel,
  onConfirm = "submit",
  txtCancel,
  txtConfirm,
}: Readonly<IModalConfirmProps>): JSX.Element => {
  const { t } = useTranslation();
  const isSubmit = onConfirm === "submit";

  return (
    <Container
      display={"flex"}
      gap={0.75}
      maxWidth={onCancel ? "392px" : "190px"}
      mt={1.5}
    >
      {onCancel ? (
        <Button
          id={`${id}-cancel`}
          justify={"center"}
          onClick={onCancel}
          variant={"tertiary"}
          width={"100%"}
        >
          {txtCancel ?? t("components.modal.cancel")}
        </Button>
      ) : undefined}
      <Button
        disabled={disabled}
        id={id}
        justify={"center"}
        onClick={isSubmit ? undefined : onConfirm}
        type={isSubmit ? "submit" : "button"}
        variant={"primary"}
        width={"100%"}
      >
        {txtConfirm ?? t("components.modal.confirm")}
      </Button>
    </Container>
  );
};

export { ModalConfirm };
