import { styled } from "styled-components";

import type { IStyledFooterProps, TSize } from "./types";
import { getVariant } from "./utils";

const ModalBackground = styled.div.attrs({
  className: "comp-modal absolute--fill fixed overflow-auto",
})`
  align-items: center;
  background-color: rgb(52 64 84 / 70%);
  display: flex;
  justify-content: center;
  z-index: 99999;
`;

const ModalContainer = styled.div<{ $size: TSize }>`
  ${({ theme, $size }): string => `
    background-color: ${theme.palette.white};
    border: 1px solid ${theme.palette.gray[200]};
    border-radius: ${theme.spacing[0.25]};
    color: ${theme.palette.gray[800]};
    display: flex;
    flex-direction: column;
    font-family: ${theme.typography.type.primary};
    font-size: ${theme.typography.text.sm};
    max-height: 80%;
    min-height: minmax(max-content, 32%);
    overflow: hidden auto;
    padding: ${theme.spacing[1.25]};
    box-shadow: ${theme.shadows.lg};
    margin: auto;
    white-space: pre-line;

    > *:not(img) {
      margin-bottom: ${theme.spacing[1.25]};
    }

    > *:last-child {
      margin-bottom: 0;
    }

    ${getVariant(theme, $size)}
  `}
`;

const Header = styled.div`
  display: block;
  overflow: unset;
  height: max-content;
  max-height: 100%;
  max-width: 100%;
  position: static;
`;

const Title = styled.div.attrs({
  className: "flex items-center justify-between mb-1.5",
})``;

const Footer = styled.div<IStyledFooterProps>`
  display: flex;
  flex-direction: row;
  gap: ${({ theme, $gap = 0 }): string => theme.spacing[$gap]};
  justify-content: ${({ $justifyContent = "space-between" }): string =>
    $justifyContent};
  margin: 0;
  max-width: 392px;

  > label {
    margin: 0;
    flex: ${({ $fullInfo = false }): string =>
      $fullInfo ? "8 2 auto" : "1 1 auto"};
  }

  > div {
    display: flex;
    flex-direction: row;
    flex: 1 1 auto;
    gap: ${({ theme, $gap = 0 }): string => theme.spacing[$gap]};
    margin-right: ${({ theme }): string => theme.spacing[0.75]};

    > button {
      justify-content: center;
      max-width: 190px;
    }
  }

  > div:last-child {
    margin-right: 0;
  }
`;

const ImageContainer = styled.div<{ $framed?: boolean }>`
  img {
    margin: 0 calc(-${({ theme }): string => theme.spacing[1.5]} - 1px)
      ${({ theme }): string => theme.spacing[1.5]};

    min-width: calc(100% + 2px + ${({ theme }): string => theme.spacing[3]});
    height: 185px;
    object-fit: cover;

    ${({ $framed = false }): string =>
      $framed
        ? `
            border-radius: 8px;
            min-width: 100%;
            margin-left: 0;
            margin-right: 0;
          `
        : ``}
  }
`;

export {
  Header,
  ModalContainer,
  Footer,
  ImageContainer,
  ModalBackground,
  Title,
};
