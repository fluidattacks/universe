import { Container, Link, Text } from "@fluidattacks/design";
import isNil from "lodash/isNil";

import type { IEditableProps } from "./types";

import { Label } from "../../label";

const Editable = ({
  children,
  currentValue,
  externalLink,
  isEditing,
  label,
  tooltip,
}: Readonly<IEditableProps>): JSX.Element => {
  if (isEditing) {
    return <div>{children}</div>;
  }
  const value = isNil(currentValue) ? "" : currentValue;
  const href = String(externalLink ?? value);

  return (
    <Container>
      <Label tooltip={tooltip} weight={"bold"}>
        {label}
      </Label>
      {href.startsWith("https://") ? (
        <Container>
          <Link href={href}>{value}</Link>
        </Container>
      ) : (
        <Text size={"md"}>{value}</Text>
      )}
    </Container>
  );
};

export type { IEditableProps };
export { Editable };
