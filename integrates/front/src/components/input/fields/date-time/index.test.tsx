import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { Form, Formik } from "formik";

import { InputDateTime } from ".";
import { CustomThemeProvider } from "components/colors";
import { render } from "mocks";

describe("inputDateTime component", (): void => {
  const date = new Date();
  const month = date.toLocaleString("default", { month: "long" });
  const year = date.getFullYear();

  it("should render component", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Formik initialValues={{}} onSubmit={jest.fn()}>
        <Form name={"testForm"}>
          <CustomThemeProvider>
            <InputDateTime label={"Label date time"} name={"inputDateTime"} />
          </CustomThemeProvider>
        </Form>
      </Formik>,
    );

    await waitFor((): void => {
      expect(screen.getByText("Label date time")).toBeInTheDocument();
    });

    expect(
      screen.getByRole("button", { name: "Calendar" }),
    ).toBeInTheDocument();

    ["month,", "day,", "year,", "hour,", "minute,", "AM/PM,"].forEach(
      (name: string): void => {
        expect(screen.getByRole("spinbutton", { name })).toBeInTheDocument();
      },
    );
  });

  it("should render and get value from dropdown Calendar", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Formik initialValues={{}} onSubmit={jest.fn()}>
        <Form name={"testForm"}>
          <CustomThemeProvider>
            <InputDateTime label={"Label date time"} name={"inputDateTime"} />
          </CustomThemeProvider>
        </Form>
      </Formik>,
    );

    expect(screen.getByText("Label date time")).toBeInTheDocument();
    expect(
      screen.getByRole("button", { name: "Calendar" }),
    ).toBeInTheDocument();

    await userEvent.click(screen.getByRole("button", { name: "Calendar" }));

    expect(screen.getByText(`${month} ${year}`)).toBeInTheDocument();

    await userEvent.click(screen.getByText("17"));
    await userEvent.type(
      screen.getByRole("spinbutton", { name: "hour," }),
      "10",
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: "minute," }),
      "32",
    );

    expect(
      screen.getByText(`Selected Date: ${month} 17, ${year} at 10:32 AM`),
    ).toBeInTheDocument();
  });

  it("should render and change value with typing", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Formik initialValues={{}} onSubmit={jest.fn()}>
        <Form name={"testForm"}>
          <CustomThemeProvider>
            <InputDateTime label={"Label date time"} name={"inputDateTime"} />
          </CustomThemeProvider>
        </Form>
      </Formik>,
    );

    expect(screen.getByText("Label date time")).toBeInTheDocument();
    expect(
      screen.getByRole("button", { name: "Calendar" }),
    ).toBeInTheDocument();

    // Type date mm/dd/yyyy
    await userEvent.type(
      screen.getByRole("spinbutton", { name: "month," }),
      "10",
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: "day," }),
      "11",
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: "year," }),
      "2024",
    );

    // Type time hh:mm AM/PM
    await userEvent.type(
      screen.getByRole("spinbutton", { name: "hour," }),
      "8",
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: "minute," }),
      "29",
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: "AM/PM," }),
      "PM",
    );

    expect(
      screen.getByText("Selected Date: October 11, 2024 at 08:29 PM"),
    ).toBeInTheDocument();
  });
});
