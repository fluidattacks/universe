import { Button, Text } from "@fluidattacks/design";
import { createCalendar } from "@internationalized/date";
import { useCalendar } from "@react-aria/calendar";
import type { TimeValue } from "@react-aria/datepicker";
import { useLocale } from "@react-aria/i18n";
import type { CalendarStateOptions } from "@react-stately/calendar";
import { useCalendarState } from "@react-stately/calendar";
import type { DatePickerState } from "@react-stately/datepicker";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

import { OutlineContainer, TimeOutlineContainer } from "./styles";

import { TimeField } from "../../../utils/date-time-field";
import { CalendarGrid } from "../../date/calendar/grid";
import { Header } from "../../date/calendar/header";
import { FooterContainer } from "../../date/calendar/styles";

const Calendar = ({
  handleTimeChange,
  onClose,
  props,
  timeState,
}: Readonly<{
  handleTimeChange: (selectedValue: TimeValue | null) => void;
  onClose: () => void;
  props: Omit<CalendarStateOptions, "createCalendar" | "locale">;
  timeState: DatePickerState;
}>): JSX.Element => {
  const theme = useTheme();
  const { t } = useTranslation();
  const { locale } = useLocale();
  const state = useCalendarState({ ...props, createCalendar, locale });
  const { prevButtonProps, nextButtonProps, title } = useCalendar(props, state);

  return (
    <div className={"calendar"}>
      <Header
        nextButtonProps={nextButtonProps}
        prevButtonProps={prevButtonProps}
        state={state}
        title={title}
      />
      <CalendarGrid state={state} />
      <TimeOutlineContainer>
        <Text color={theme.palette.gray[800]} fontWeight={"bold"} size={"sm"}>
          {t("calendar.time")}
        </Text>
        <OutlineContainer>
          <TimeField
            hourCycle={12}
            label={"time-field"}
            onChange={handleTimeChange}
            value={timeState.timeValue}
          />
        </OutlineContainer>
      </TimeOutlineContainer>
      <FooterContainer>
        <Button onClick={onClose} variant={"ghost"}>
          {t("calendar.cancel")}
        </Button>
      </FooterContainer>
    </div>
  );
};

export { Calendar };
