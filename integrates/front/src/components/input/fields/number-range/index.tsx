import { Container } from "@fluidattacks/design";
import { useFormikContext } from "formik";

import { InputContainer } from "../../input-container";
import type { IInputNumberRangeProps } from "../../types";
import { NumberField } from "../../utils/number-field";

const InputNumberRange = ({
  disabled = false,
  id,
  label,
  propsMax,
  propsMin,
  required,
  tooltip,
}: IInputNumberRangeProps): JSX.Element => {
  const { getFieldMeta } = useFormikContext();
  const metaPropsMax = getFieldMeta(propsMax.name ?? "input-number-max");
  const metaPropsMin = getFieldMeta(propsMin.name ?? "input-number-min");

  const alert =
    (metaPropsMax.touched || metaPropsMin.touched) && !disabled
      ? (metaPropsMax.error ?? metaPropsMin.error)
      : undefined;

  return (
    <InputContainer
      alert={alert}
      disabled={disabled}
      id={id}
      label={label}
      name={propsMax.name}
      required={required}
      tooltip={tooltip}
    >
      <Container display={"flex"} gap={0.25} width={"100%"}>
        <NumberField {...propsMin} alert={alert} disabled={disabled} />
        <NumberField {...propsMax} alert={alert} disabled={disabled} />
      </Container>
    </InputContainer>
  );
};

export { InputNumberRange };
