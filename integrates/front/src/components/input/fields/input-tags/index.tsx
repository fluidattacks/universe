import { Tag } from "@fluidattacks/design";
import { useField } from "formik";
import { useCallback, useMemo, useRef, useState } from "react";
import * as React from "react";

import { InputContainer } from "../../input-container";
import { OutlineContainer, OutlineInput, TagInput } from "../../styles";
import type { IInputTagsProps } from "../../types";
import { createEvent } from "../../utils";

const InputTags: React.FC<IInputTagsProps> = (props): JSX.Element => {
  const {
    disabled = false,
    id,
    label,
    name = "input-tag",
    onRemove,
    placeholder,
    required,
    tooltip,
    updateStoredData,
    weight,
  } = props;
  const inputRef = useRef<HTMLInputElement>(null);
  const [field, { error, touched }] = useField<string>(name);
  const [inputValue, setInputValue] = useState("");
  const { onChange, onBlur, value } = field;

  const handleInputChange = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>): void => {
      setInputValue(event.target.value);
    },
    [],
  );

  const tags = useMemo((): string[] => {
    return value ? value.split(",").filter(Boolean) : [];
  }, [value]);

  const setTags = useCallback(
    (values: string[]): void => {
      const newTags = new Set(values.filter(Boolean));
      const newValue = [...newTags].join(",");
      /*
       * In the case of tags the info is store in tags and value keys, that's
       * To keep the compatibility with the old version (switching between FP and Old visual)
       */
      if (updateStoredData)
        updateStoredData({
          id: id ?? name,
          tags: [...newTags],
          value: newValue,
        });

      const changeEvent = createEvent("change", name, newValue);
      onChange(changeEvent);
      inputRef.current?.scrollIntoView(false);
    },
    [id, name, onChange, updateStoredData],
  );

  const handleOnBlur = useCallback(
    (event: React.FocusEvent<HTMLInputElement>): void => {
      const trimmedValue = inputValue.trim();
      if (trimmedValue.length && !tags.includes(trimmedValue)) {
        event.preventDefault();

        setTags([...tags, trimmedValue]);
        setInputValue("");
      }
      onBlur(event);
    },
    [inputValue, onBlur, setTags, tags],
  );

  const handleKeyDown = useCallback(
    (event: React.KeyboardEvent<HTMLInputElement>): void => {
      const trimmedValue = inputValue.trim();

      if (
        ["Enter", ","].includes(event.key) &&
        trimmedValue.length &&
        !tags.includes(trimmedValue)
      ) {
        event.preventDefault();

        setTags([...tags, trimmedValue]);
        setInputValue("");
      } else if (
        event.key === "Backspace" &&
        !trimmedValue.length &&
        tags.length
      ) {
        event.preventDefault();
        onRemove?.(tags[tags.length - 1]);
        setTags(tags.slice(0, -1));
      }
    },
    [inputValue, onRemove, setTags, tags],
  );

  const removeTag = useCallback(
    (tag: string): VoidFunction => {
      return (): void => {
        onRemove?.(tag);
        setTags(tags.filter((currentValue): boolean => currentValue !== tag));
      };
    },
    [onRemove, setTags, tags],
  );

  const alert = touched ? error : undefined;

  return (
    <InputContainer
      alert={alert}
      label={label}
      name={name}
      placeholder={placeholder}
      required={required}
      tooltip={tooltip}
      weight={weight}
    >
      <OutlineContainer $disabled={disabled} $show={alert !== undefined}>
        <OutlineInput $maxWidth={"680px"} $overflowX={"auto"}>
          {tags.map((tag): JSX.Element => {
            return (
              <Tag
                disabled={disabled}
                key={tag}
                onClose={removeTag(tag)}
                tagLabel={tag}
              />
            );
          })}
          <TagInput
            {...field}
            $maskValue={false}
            aria-label={name}
            autoComplete={"off"}
            disabled={disabled}
            name={name}
            onBlur={handleOnBlur}
            onChange={handleInputChange}
            onKeyDown={handleKeyDown}
            placeholder={tags.length === 0 ? placeholder : undefined}
            ref={inputRef}
            type={"text"}
            value={inputValue}
          />
        </OutlineInput>
      </OutlineContainer>
    </InputContainer>
  );
};

export { InputTags };
