import { Container, Icon, IconButton } from "@fluidattacks/design";
import { useField } from "formik";
import isEmpty from "lodash/isEmpty";
import { useCallback } from "react";
import * as React from "react";
import { useTheme } from "styled-components";

import { InputContainer } from "../../input-container";
import { OutlineContainer, OutlineInputText, StyledInput } from "../../styles";
import type { TInputProps } from "../../types";
import { ActionButton } from "../../utils/action-button";

const Input = (props: TInputProps): JSX.Element => {
  const {
    disabled = false,
    handleActionClick,
    hasAction = false,
    hasIcon = false,
    helpLink,
    helpLinkText,
    helpText,
    id,
    indexArray = 0,
    label,
    maskValue = false,
    name = "input-field",
    suggestions,
    onChange,
    onBlur,
    onPaste,
    placeholder,
    required,
    removeItemArray,
    tooltip,
    tooltipPlace = "top",
    type = "text",
    value,
    validateOnBlur = true,
    weight,
  } = props;
  const theme = useTheme();
  const [field, { error, touched }] = useField(name);
  const alert = touched && !disabled ? error : undefined;
  const hasError = alert !== undefined;
  const inputValue = value ?? field.value;
  const showSuccess = validateOnBlur
    ? !hasError && touched && !disabled && !isEmpty(inputValue)
    : false;

  const formatInput = useCallback(
    (event: React.KeyboardEvent<HTMLInputElement>): void => {
      if (event.key === "Enter") {
        event.preventDefault();
      }
    },
    [],
  );
  const handleBlur = useCallback(
    (event: React.FocusEvent<HTMLInputElement>): void => {
      if (onBlur) {
        onBlur(event);
      }
      field.onBlur(event);
    },
    [onBlur, field],
  );
  const handleChange = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>): void => {
      if (onChange) {
        onChange(event);
      }
      field.onChange(event);
    },
    [onChange, field],
  );

  return (
    <InputContainer
      alert={alert}
      disabled={disabled}
      helpLink={helpLink}
      helpLinkText={helpLinkText}
      helpText={helpText}
      label={label}
      name={name}
      required={required}
      tooltip={tooltip}
      tooltipPlace={tooltipPlace}
      weight={weight}
    >
      <OutlineContainer
        $disabled={disabled}
        $show={hasError}
        $showSuccess={showSuccess}
      >
        <OutlineInputText $hasAction={hasAction} $hasIcon={hasIcon}>
          {hasIcon ? (
            <Icon icon={"user"} iconSize={"xs"} iconType={"fa-light"} />
          ) : undefined}
          <StyledInput
            {...field}
            $maskValue={maskValue}
            aria-hidden={false}
            aria-label={name}
            autoComplete={"off"}
            data-testid={`${name}-input`}
            disabled={disabled}
            id={id}
            list={`list_${name}`}
            name={name}
            onBlur={handleBlur}
            onChange={handleChange}
            onKeyDown={formatInput}
            onPaste={onPaste}
            placeholder={placeholder}
            required={undefined}
            type={type}
            value={isEmpty(inputValue) ? "" : inputValue}
          />
          <Container alignItems={"center"} display={"flex"} gap={0.25}>
            {hasError ? (
              <Icon
                icon={"exclamation-circle"}
                iconClass={"error-icon"}
                iconColor={theme.palette.error[500]}
                iconSize={"xs"}
              />
            ) : undefined}
            <Icon
              icon={"check-circle"}
              iconClass={"success-icon"}
              iconColor={theme.palette.success[500]}
              iconSize={"xs"}
            />
            {id === "array" ? (
              <IconButton
                color={theme.palette.gray["400"]}
                icon={"trash"}
                iconSize={"xxs"}
                iconType={"fa-light"}
                onClick={
                  removeItemArray ? removeItemArray(indexArray) : undefined
                }
                variant={"ghost"}
              />
            ) : undefined}
          </Container>
        </OutlineInputText>
        {hasAction ? (
          <ActionButton
            borderRadius={`0 ${theme.spacing["0.5"]} ${theme.spacing["0.5"]} 0`}
            disabled={disabled}
            icon={"eye"}
            onClick={handleActionClick}
          />
        ) : undefined}
      </OutlineContainer>
      {suggestions ? (
        <datalist id={`list_${name}`}>
          {suggestions.map(
            (suggestion): JSX.Element => (
              <option key={suggestion} value={suggestion} />
            ),
          )}
        </datalist>
      ) : undefined}
    </InputContainer>
  );
};

export { Input };
