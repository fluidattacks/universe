import { FieldArray } from "formik";
import type { ReactNode } from "react";

import { ArrayField } from "./field";

import type { TInputArrayProps } from "../../types";

const DEFAULT_MAX_VALUE = 100;
const InputArray = ({
  addButtonText,
  disabled,
  initEmpty,
  initValue = "",
  required,
  label,
  max = undefined,
  name = "array-field",
  onChange,
  placeholder,
  tooltip,
}: Readonly<Partial<TInputArrayProps>>): JSX.Element => {
  return (
    <FieldArray name={name}>
      {(props): ReactNode => {
        return (
          <ArrayField
            {...props}
            addButtonText={addButtonText}
            disabled={disabled}
            initEmpty={initEmpty}
            initValue={initValue}
            label={label}
            max={max ?? DEFAULT_MAX_VALUE}
            name={name}
            onChange={onChange}
            placeholder={placeholder}
            required={required}
            tooltip={tooltip}
          />
        );
      }}
    </FieldArray>
  );
};

export { InputArray };
