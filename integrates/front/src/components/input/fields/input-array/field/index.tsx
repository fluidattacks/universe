import { Button, Col, Row } from "@fluidattacks/design";
import isEmpty from "lodash/isEmpty";
import { Fragment, useCallback, useEffect } from "react";
import { useTranslation } from "react-i18next";

import type { TInputArrayProps } from "../../../types";
import { Input } from "../../input";

export const ArrayField = ({
  addButtonText,
  disabled = false,
  initEmpty = true,
  initValue = "",
  required,
  label,
  max,
  name,
  onChange,
  placeholder,
  tooltip,
  form,
  push,
  remove,
}: TInputArrayProps): JSX.Element => {
  const { t } = useTranslation();
  const values = form.getFieldProps(name).value as string[];

  const addItem = useCallback((): void => {
    push(initValue);
  }, [initValue, push]);

  const removeItem = useCallback(
    (index: number): (() => void) =>
      (): void => {
        remove(index);
      },
    [remove],
  );

  useEffect((): void => {
    if (!initEmpty && isEmpty(values)) {
      push(initValue);
    }
  }, [initEmpty, initValue, push, values]);

  return (
    <div>
      <Row>
        {values.length > 0 ? (
          <Fragment>
            {values.map((_, index: number): JSX.Element => {
              const fieldName = `${name}.${index}`;

              return (
                <Col key={fieldName} lg={80} md={80} sm={80}>
                  <Input
                    disabled={disabled}
                    id={
                      (!initEmpty && values.length === 1) || disabled
                        ? ""
                        : "array"
                    }
                    indexArray={index}
                    label={label}
                    name={fieldName}
                    onChange={onChange}
                    placeholder={placeholder}
                    removeItemArray={removeItem}
                    required={required}
                    tooltip={tooltip}
                  />
                </Col>
              );
            })}
          </Fragment>
        ) : undefined}
        {values.length < max ? (
          <Col lg={20} md={20} sm={20}>
            <Button
              disabled={disabled}
              icon={"plus"}
              onClick={addItem}
              variant={"ghost"}
            >
              {addButtonText ?? t("buttons.add")}
            </Button>
          </Col>
        ) : undefined}
      </Row>
    </div>
  );
};
