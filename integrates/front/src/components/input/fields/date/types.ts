import type {
  AriaCalendarCellProps,
  AriaCalendarGridProps,
  CalendarAria,
} from "@react-aria/calendar";
import type { DateValue } from "@react-aria/datepicker";
import type {
  CalendarState,
  RangeCalendarState,
} from "@react-stately/calendar";
import type { DatePickerStateOptions } from "@react-stately/datepicker";
import type { RangeValue } from "@react-types/shared";

interface ICalendarState {
  state: CalendarState | RangeCalendarState;
}

interface IDateProps
  extends Omit<JSX.IntrinsicElements["input"], "defaultValue" | "form"> {
  handleOnMaxChange?: (value: DateValue) => void;
  handleOnMinChange?: (value: DateValue) => void;
  handleRangeChange?: (value: RangeValue<DateValue>) => void;
  label?: string;
  name: string;
  tooltip?: string;
  minName?: string;
  maxName?: string;
}

type TInputDateProps = IDateProps &
  Omit<DatePickerStateOptions<DateValue>, "label">;

type TGridProps = AriaCalendarGridProps & ICalendarState;
type TCellProps = AriaCalendarCellProps & ICalendarState;
type THeaderProps = ICalendarState &
  Pick<CalendarAria, "nextButtonProps" | "prevButtonProps" | "title">;

export type {
  IDateProps,
  TInputDateProps,
  TCellProps,
  TGridProps,
  THeaderProps,
};
