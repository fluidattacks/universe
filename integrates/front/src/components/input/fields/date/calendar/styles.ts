import { css, styled } from "styled-components";

const TitleContainer = styled.div`
  ${({ theme }): string => `
  align-items: center;
  color: ${theme.palette.gray[800]};
  display: flex;
  font-family: ${theme.typography.type.primary};
  font-size: ${theme.typography.text.sm};
  font-weight: ${theme.typography.weight.bold};
  gap: ${theme.spacing[0.5]};
  height: 40px;
  justify-content: center;
  line-height: ${theme.spacing[1.25]};
  text-align: center;
  padding: ${theme.spacing[0.5]} 0;
  width: 200px;
  word-spacing: ${theme.spacing[0.25]};

  &:not(:hover) .year-selection {
    display: none;
  }
  `}
`;

const YearSwitch = styled.div.attrs({ className: "year-selection" })`
  align-items: flex-start;
  display: flex;
  flex-direction: column;
  gap: 6px;
  height: 14px;
  justify-content: center;
  margin-bottom: 10px;
  padding: 0 0.5px;
  position: relative;
  width: 8px;
`;

const WeekDaysTr = styled.tr`
  align-items: flex-start;
  display: flex;
  padding: ${({ theme }): string => theme.spacing[0.25]} 0;
  width: 100%;
`;

const sharedDayStyles = css`
  ${({ theme }): string => `
  align-items: center;
  color: ${theme.palette.gray[800]};
  cursor: pointer;
  display: flex;
  flex-direction: column;
  font-family: ${theme.typography.type.primary};
  font-size: ${theme.typography.text.sm};
  font-weight: ${theme.typography.weight.regular};
  height: 40px;
  justify-content: center;
  line-height: ${theme.spacing[1.25]};
  text-align: center;
  width: 40px;
  `}
`;

const DayHeaderTh = styled.th`
  ${sharedDayStyles}
`;

const DayItemTd = styled.td`
  ${sharedDayStyles}

  ${({ theme }): string => `
  &#range {
    background: ${theme.palette.primary[25]};
  }

  div:not(.dot) {
    align-items: center;
    display: flex;
    flex-direction: column;
    height: 100%;
    justify-content: center;
    position: relative;
    text-align: center;
    width: 100%;

    &:focus-visible {
      outline: none;
    }

    &.disabled {
      color: ${theme.palette.gray[400]};
      cursor: auto;
    }

    &.selected {
      background: ${theme.palette.primary[500]};
      color: ${theme.palette.white};
    }

    &.focused {
      border: 2px solid ${theme.palette.primary[500]};
    }

    &.today:not(.selected) {
      color: ${theme.palette.primary[500]};
      font-weight: ${theme.typography.weight.bold};
    }

    &:hover:not(.disabled, .selected, .range) {
      background: ${theme.palette.gray[100]};
    }
  }
  `}
`;

const DotIconContainer = styled.div.attrs({ className: "dot" })`
  position: absolute;
  top: 48%;

  > span {
    font-size: 4px;
  }
`;

const CaretButton = styled.button`
  all: unset;
  cursor: pointer;
  display: flex;

  > span {
    height: 3.5px;
    width: 7px;
  }
`;

const FooterContainer = styled.div`
  ${({ theme }): string => `
  align-items: end;
  border-top: 1px solid ${theme.palette.gray[200]};
  display: flex;
  flex-direction: row-reverse;
  gap: ${theme.spacing[0.5]};
  height: 44px;
  margin-bottom: ${theme.spacing[0.25]};
  padding: 0 ${theme.spacing[0.25]};
  width: 100%;
  `}
`;

export {
  CaretButton,
  DayItemTd,
  DayHeaderTh,
  DotIconContainer,
  FooterContainer,
  TitleContainer,
  WeekDaysTr,
  YearSwitch,
};
