import { parseDate } from "@internationalized/date";
import type { DateValue } from "@react-aria/datepicker";
import { useDatePicker } from "@react-aria/datepicker";
import { useDatePickerState } from "@react-stately/datepicker";
import { useField } from "formik";
import _ from "lodash";
import { useCallback, useRef } from "react";

import { Calendar } from "./calendar";
import type { TInputDateProps } from "./types";

import { InputContainer } from "../../input-container";
import { handleValueChange } from "../../utils";
import { DateSelector } from "../../utils/date-selector";
import { Dialog } from "../../utils/dialog";
import { Popover } from "../../utils/popover";
import { formatDate } from "utils/date";

const InputDate = (props: TInputDateProps): JSX.Element => {
  const datePickerRef = useRef(null);
  const state = useDatePickerState(props);
  const { close } = state;
  const { disabled = false, required, label, name, tooltip } = props;
  const [{ value }, { error, touched }, { setValue }] = useField(name);
  const alert = touched ? error : undefined;
  const { groupProps, fieldProps, buttonProps, dialogProps, calendarProps } =
    useDatePicker(
      { ...props, "aria-label": label ?? name },
      state,
      datePickerRef,
    );
  const handleCalendarChange = useCallback(
    (selectedValue: DateValue): void => {
      if (calendarProps.onChange) {
        calendarProps.onChange(selectedValue);
      }
      handleValueChange({
        selectedValue: formatDate(String(selectedValue)),
        setValue,
      });
    },
    [calendarProps, setValue],
  );

  return (
    <InputContainer
      alert={alert}
      disabled={disabled}
      label={label}
      name={name}
      required={required}
      tooltip={tooltip}
    >
      <DateSelector
        alert={alert}
        buttonProps={buttonProps}
        datePickerRef={datePickerRef}
        disabled={disabled}
        fieldProps={
          _.isNil(value) || _.isEmpty(value) || value === "-"
            ? fieldProps
            : {
                ...fieldProps,
                value: parseDate((value as string).replace(/\//gu, "-")),
              }
        }
        groupProps={groupProps}
        isOpen={state.isOpen}
        name={name}
      />
      {state.isOpen && !disabled ? (
        <Popover
          offset={8}
          placement={"bottom start"}
          state={state}
          triggerRef={datePickerRef}
        >
          <Dialog {...dialogProps}>
            <Calendar
              onClose={close}
              props={{ ...calendarProps, onChange: handleCalendarChange }}
            />
          </Dialog>
        </Popover>
      ) : null}
    </InputContainer>
  );
};

export { InputDate };
