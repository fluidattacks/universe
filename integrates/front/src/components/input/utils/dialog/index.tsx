import { useDialog } from "@react-aria/dialog";
import { useRef } from "react";
import * as React from "react";

import { DialogContainer } from "../styles";
import type { TDialogProps } from "../types";

const Dialog = ({
  children,
  ...props
}: React.PropsWithChildren<TDialogProps>): JSX.Element => {
  const dialogRef = useRef(null);
  const { dialogProps } = useDialog(props, dialogRef);

  return (
    <DialogContainer {...dialogProps} ref={dialogRef}>
      {children}
    </DialogContainer>
  );
};

export { Dialog };
