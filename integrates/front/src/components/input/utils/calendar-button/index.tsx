import { Icon } from "@fluidattacks/design";
import type { IconName } from "@fortawesome/free-solid-svg-icons";
import type { AriaButtonOptions } from "@react-aria/button";
import { useButton } from "@react-aria/button";
import { useRef } from "react";
import { useTheme } from "styled-components";

import { CalendarButtonAction } from "../styles";

const GRAY_700 = 700;
const GRAY_400 = 400;
const Button = ({
  disabled = false,
  icon,
  props,
}: Readonly<{
  disabled?: boolean;
  icon: IconName;
  props: AriaButtonOptions<"button">;
}>): JSX.Element => {
  const theme = useTheme();
  const ref = useRef(null);
  const { buttonProps } = useButton(props, ref);

  return (
    <CalendarButtonAction
      {...buttonProps}
      $disabled={disabled}
      $focus={Boolean(buttonProps["aria-expanded"])}
      $header={icon === "calendar" || icon === "calendar-clock"}
      disabled={disabled}
      ref={ref}
      type={"button"}
    >
      <Icon
        icon={icon}
        iconColor={theme.palette.gray[disabled ? GRAY_400 : GRAY_700]}
        iconSize={"xs"}
        iconType={"fa-light"}
      />
    </CalendarButtonAction>
  );
};

export { Button };
