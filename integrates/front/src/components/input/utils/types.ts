import type { IconName } from "@fortawesome/free-solid-svg-icons";
import type {
  AriaDatePickerProps,
  DatePickerAria,
  DateValue,
} from "@react-aria/datepicker";
import type { AriaDialogProps } from "@react-aria/dialog";
import type { AriaPopoverProps } from "@react-aria/overlays";
import type {
  DateFieldState,
  DateFieldStateOptions,
  DateSegment,
  TimeFieldStateOptions,
} from "@react-stately/datepicker";
import type { OverlayTriggerState } from "@react-stately/overlays";
import type React from "react";

import type { IBorderModifiable } from "components/@core/types";

interface IDateSelectorProps
  extends Pick<DatePickerAria, "buttonProps" | "fieldProps" | "groupProps">,
    Pick<AriaDatePickerProps<DateValue>, "granularity"> {
  alert?: string;
  datePickerRef: React.MutableRefObject<null>;
  disabled?: boolean;
  handleOnChange?: (value: DateValue) => void;
  isOpen: boolean;
  name: string;
  variant?: "range";
}

interface IPopoverProps {
  children: React.ReactNode;
  isFilter?: boolean;
  state: OverlayTriggerState;
}

interface IDateSegmentProps {
  segment: DateSegment;
  state: DateFieldState;
}

interface IDateFieldProps {
  disabled: boolean;
  error: boolean;
}

interface IActionButtonProps extends IBorderModifiable {
  disabled: boolean;
  icon: IconName;
  onClick?: () => void;
}

type TPopOverProps = IPopoverProps & Omit<AriaPopoverProps, "popoverRef">;
type TDialogProps = AriaDialogProps;
type TDateFieldProps = IDateFieldProps & {
  props: Omit<DateFieldStateOptions, "createCalendar" | "locale">;
};
type TTimeFieldProps = Omit<TimeFieldStateOptions, "createCalendar" | "locale">;

export type {
  IActionButtonProps,
  TDateFieldProps,
  IDateSegmentProps,
  IDateSelectorProps,
  TDialogProps,
  TPopOverProps,
  TTimeFieldProps,
};
