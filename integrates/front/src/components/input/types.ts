import type { FieldArrayRenderProps, FieldHookConfig } from "formik";
import type { DefaultTheme } from "styled-components";

import type { TInputDateProps } from "./fields/date/types";
import type { TInputDateRangeProps } from "./fields/date-range";

import type { TPlace } from "components/@core/types";
import type { IDropDownOption } from "components/dropdown/types";
import type { IPermanentData } from "components/filter/types";

interface ILabelProps {
  htmlFor?: string;
  isRequired?: boolean;
  tooltip?: string;
  tooltipPlace?: TPlace;
  weight?: keyof DefaultTheme["typography"]["weight"];
}

type TSharedProps = Omit<ILabelProps, "isRequired"> &
  Omit<JSX.IntrinsicElements["input"], "ref"> & {
    helpLink?: string;
    helpLinkText?: string;
    helpText?: string;
    label?: string;
  };

type TInputProps = TSharedProps & {
  handleActionClick?: () => void;
  hasAction?: boolean;
  hasIcon?: boolean;
  indexArray?: number;
  maskValue?: boolean;
  removeItemArray?: (index: number) => () => void;
  suggestions?: string[];
  validateOnBlur?: boolean;
};

interface IInputBaseProps extends TSharedProps {
  alert?: string;
}

type TTextAreaProps = JSX.IntrinsicElements["textarea"] &
  Omit<ILabelProps, "isRequired"> & {
    helpLink?: string;
    helpText?: string;
    label?: string;
    maskValue?: boolean;
  };

interface IInputTagsProps extends TSharedProps {
  onRemove?: (tag: string) => void;
  updateStoredData?: (permanentData: IPermanentData) => void;
}

interface IInputNumberProps extends TSharedProps {
  alert?: string;
  decimalPlaces?: number;
  enableTextLimit?: boolean;
  updateStoredData?: (newValue?: number) => void;
}

interface IInputNumberRangeProps {
  disabled?: boolean;
  id?: string;
  label?: string;
  propsMin: IInputNumberProps;
  propsMax: IInputNumberProps;
  required?: boolean;
  tooltip?: string;
}

interface ISelect extends Omit<TSharedProps, "onChange"> {
  items: IDropDownOption[];
  handleOnChange?: (selection: IDropDownOption) => void;
}

interface IInputArray extends Omit<TSharedProps, "form" | "max"> {
  addButtonText?: string;
  initEmpty?: boolean;
  initValue: string;
  max: number;
}

type TSelectProps = FieldHookConfig<string> & ISelect;
type TInputArrayProps = FieldArrayRenderProps & IInputArray;

export type {
  ILabelProps,
  TSharedProps,
  TInputProps,
  IInputBaseProps,
  TTextAreaProps,
  IInputTagsProps,
  IInputNumberProps,
  IInputNumberRangeProps,
  TSelectProps,
  TInputArrayProps,
  TInputDateProps,
  TInputDateRangeProps,
};
