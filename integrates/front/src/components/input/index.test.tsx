import { fireEvent, screen, waitFor, within } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { Form, Formik } from "formik";

import { Input, InputFile, InputNumber, InputTags, Select, TextArea } from ".";
import { render } from "mocks";

describe("input", (): void => {
  it("should render Input components", (): void => {
    expect.hasAssertions();

    render(
      <Formik initialValues={{}} onSubmit={jest.fn()}>
        <Form name={"testForm"}>
          <Input label={"Last name"} name={"inputText"} />
          <InputFile
            accept={"image/*"}
            label={"Select file"}
            name={"inputFile"}
          />
          <InputNumber label={"Select number"} name={"inputNumber"} />
          <Select
            items={[
              { value: "Option 1" },
              { value: "Option 2" },
              { value: "Option 3" },
            ]}
            label={"Select one option"}
            name={"inputSelect"}
          />
          <TextArea
            label={"Comments"}
            maxLength={1000}
            name={"inputTextArea"}
          />
        </Form>
      </Formik>,
    );

    expect(
      screen.getByRole("combobox", { name: "inputText" }),
    ).toBeInTheDocument();
    expect(screen.getByTestId("inputFile")).toBeInTheDocument();
    expect(
      screen.getByRole("spinbutton", { name: "inputNumber" }),
    ).toBeInTheDocument();
    expect(
      screen.getByRole("combobox", { name: "inputSelect" }),
    ).toBeInTheDocument();
    expect(
      screen.getByRole("textbox", { name: "inputTextArea" }),
    ).toBeInTheDocument();
  });

  it("should render InputNumber and change its value", async (): Promise<void> => {
    expect.hasAssertions();

    const user = userEvent.setup();

    render(
      <Formik initialValues={{}} onSubmit={jest.fn()}>
        <Form name={"testForm"}>
          <InputNumber label={"Select number"} name={"inputNumber"} />
        </Form>
      </Formik>,
    );
    const inputNumber = screen.getByRole("spinbutton", { name: "inputNumber" });

    expect(inputNumber).toBeInTheDocument();

    // Manual typing
    await user.type(inputNumber, "5");

    expect(inputNumber).toHaveValue(5);

    // With action buttons
    await user.click(screen.getByRole("button", { name: "plus" }));

    expect(inputNumber).toHaveValue(6);

    await user.dblClick(screen.getByRole("button", { name: "minus" }));

    expect(inputNumber).toHaveValue(4);
  });

  it("should render select and choose options", async (): Promise<void> => {
    expect.hasAssertions();

    const user = userEvent.setup();

    render(
      <Formik initialValues={{ inputSelect: "" }} onSubmit={jest.fn()}>
        <Form name={"testForm"}>
          <Select
            items={[
              { header: "Select one option", value: "" },
              { value: "Option 1" },
              { value: "Option 2" },
              { value: "Option 3" },
            ]}
            name={"inputSelect"}
          />
        </Form>
      </Formik>,
    );
    const selectContainer = screen.getByTestId("inputSelect-select");

    expect(screen.getByText("Select one option")).toBeInTheDocument();

    await user.click(within(selectContainer).getByText("Select one option"));

    expect(within(selectContainer).queryAllByText("Option 1")).toHaveLength(1);
    expect(within(selectContainer).queryAllByText("Option 2")).toHaveLength(1);

    await user.click(within(selectContainer).getByText("Option 2"));

    expect(within(selectContainer).queryAllByText("Option 1")).toHaveLength(0);
  });

  it("should render Input and TextArea and update values", async (): Promise<void> => {
    expect.hasAssertions();

    const user = userEvent.setup();

    const { container } = render(
      <Formik initialValues={{}} onSubmit={jest.fn()}>
        <Form name={"testForm"}>
          <Input label={"Text input label"} name={"inputText"} />
          <TextArea
            label={"Text area input label"}
            maxLength={1000}
            name={"inputTextArea"}
          />
        </Form>
      </Formik>,
    );

    await user.type(container.querySelectorAll("input")[0], "This is a test");

    expect(container.querySelectorAll("input")[0]).toHaveValue(
      "This is a test",
    );

    await user.type(
      container.querySelectorAll("textarea")[0],
      "This is a test typed message",
    );

    expect(container.querySelectorAll("textarea")[0]).toHaveValue(
      "This is a test typed message",
    );
  });

  it("should render InputFile and update file name", async (): Promise<void> => {
    expect.hasAssertions();

    const user = userEvent.setup();
    const file = new File([""], "image.png", { type: "image/png" });

    render(
      <Formik initialValues={{}} onSubmit={jest.fn()}>
        <Form name={"testForm"}>
          <InputFile
            accept={"image/*"}
            label={"Select file"}
            multiple={false}
            name={"inputFile"}
          />
        </Form>
      </Formik>,
    );

    const inputFile = screen.getByTestId("inputFile");

    expect(inputFile).toBeInTheDocument();

    await user.click(screen.getByRole("button", { name: "inputFile.button" }));
    fireEvent.change(inputFile, { target: { files: [file] } });
    await waitFor((): void => {
      expect(screen.getByText("image.png")).toBeInTheDocument();
    });
  });

  it("should render InputTags and add and remove closeTag", async (): Promise<void> => {
    expect.hasAssertions();

    const user = userEvent.setup();

    const { container } = render(
      <Formik initialValues={{}} onSubmit={jest.fn()}>
        <Form name={"testForm"}>
          <InputTags label={"Tag input label"} name={"inputTags"} />
        </Form>
      </Formik>,
    );

    const inputTagField = container.querySelectorAll("input[name='inputTags']");

    expect(inputTagField[0]).toBeInTheDocument();

    await user.type(inputTagField[0], "Fluid Attack's");
    fireEvent.keyDown(inputTagField[0], { key: "Enter" });
    await waitFor((): void => {
      expect(screen.getByText("Fluid Attack's")).toBeInTheDocument();
    });

    await user.click(container.querySelectorAll("button")[0]);

    expect(screen.queryByText("Fluid Attack's")).not.toBeInTheDocument();
  });
});
