import { Container, Link, Tag, Text } from "@fluidattacks/design";
import { useField, useFormikContext } from "formik";
import _ from "lodash";
import { useEffect } from "react";
import { useTheme } from "styled-components";

import { Label } from "../label";
import { ErrorMessage, InputBox } from "../styles";
import type { IInputBaseProps } from "../types";

const InputContainer = ({
  alert,
  children,
  disabled,
  helpLink,
  helpLinkText = "Link here",
  helpText,
  id,
  label,
  maxLength,
  name = "input-field",
  required,
  role,
  tooltip,
  tooltipPlace = "top",
  weight,
  width = "",
  ...props
}: Readonly<IInputBaseProps>): JSX.Element => {
  const theme = useTheme();
  const { errors, isSubmitting } = useFormikContext();
  const [field] = useField(name);
  const { value } = field;
  const valueLength = _.isString(value) ? value.length : 0;

  useEffect((): void => {
    if (
      isSubmitting &&
      Object.keys(errors).findIndex(
        (errorKey): boolean => errorKey === name,
      ) === 0
    ) {
      _.first(document.getElementsByName(name))?.focus();
    }
  }, [errors, isSubmitting, name]);

  return (
    <InputBox
      $disabled={disabled}
      $type={role}
      $width={String(width)}
      role={role}
      {...props}
    >
      {label === undefined ? undefined : (
        <Label
          htmlFor={id ?? name}
          isRequired={required}
          tooltip={tooltip}
          tooltipPlace={tooltipPlace}
          weight={weight}
        >
          {label}
        </Label>
      )}
      {children ?? undefined}
      <Container
        display={"flex"}
        justify={_.isUndefined(alert) ? "end" : "space-between"}
        width={"100%"}
      >
        {_.isUndefined(alert) ? undefined : (
          <ErrorMessage $show={true}>{alert}</ErrorMessage>
        )}
        {_.isUndefined(maxLength) ? undefined : (
          <Tag
            priority={"low"}
            tagLabel={`${valueLength}/${maxLength}`}
            variant={"error"}
          />
        )}
      </Container>
      <Container
        alignItems={"start"}
        display={"flex"}
        gap={0.25}
        width={"100%"}
      >
        {_.isUndefined(helpText) ? undefined : (
          <Text
            color={theme.palette.gray[400]}
            display={"inline-block"}
            size={"sm"}
          >
            {helpText}
          </Text>
        )}
        {_.isUndefined(helpLink) ? undefined : (
          <Container alignItems={"center"} display={"inline-flex"} gap={0.25}>
            <Link href={helpLink}>{helpLinkText}</Link>
          </Container>
        )}
      </Container>
    </InputBox>
  );
};

export { InputContainer };
