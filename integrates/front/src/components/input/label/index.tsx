import { Container, Tooltip } from "@fluidattacks/design";
import isEmpty from "lodash/isEmpty";
import * as React from "react";

import { AsteriskSpan, InputLabel } from "../styles";
import type { ILabelProps } from "../types";

const Label = ({
  children,
  htmlFor,
  isRequired = false,
  tooltip,
  tooltipPlace = "top",
  weight = "regular",
}: React.PropsWithChildren<ILabelProps>): JSX.Element => {
  const tooltipId = `${htmlFor}-tooltip`;
  const hasTooltip = tooltip !== undefined && !isEmpty(tooltip);
  const hasHtmlFor = htmlFor !== undefined && !isEmpty(htmlFor);

  return (
    <InputLabel $weight={weight} htmlFor={htmlFor}>
      {children}
      {isRequired ? <AsteriskSpan>{"*"}</AsteriskSpan> : undefined}
      {hasTooltip && hasHtmlFor ? (
        <Container display={"inline"} ml={0.25}>
          <Tooltip
            icon={"circle-info"}
            id={tooltipId}
            place={tooltipPlace}
            tip={tooltip}
          />
        </Container>
      ) : null}
    </InputLabel>
  );
};

export { Label };
