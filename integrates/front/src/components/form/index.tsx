import { Container, Heading, IconButton, Text } from "@fluidattacks/design";
import type { FormikValues } from "formik";
import { Formik } from "formik";
import { useTheme } from "styled-components";

import { InnerForm } from "./inner-form";
import type { TFormProps } from "./types";

const Form = <Values extends FormikValues>({
  children,
  enableReinitialize,
  initialValues,
  innerRef,
  maxWidth,
  minWidth,
  name,
  onCloseModal,
  onSubmit,
  subtitle,
  title,
  validationSchema,
  width,
}: Readonly<TFormProps<Values>>): JSX.Element => {
  const theme = useTheme();

  return (
    <Container maxWidth={maxWidth} minWidth={minWidth} width={width}>
      {title === undefined ? null : (
        <Container display={"flex"} justify={"space-between"}>
          <Container display={"flex"} flexDirection={"column"} mb={1.5} mr={2}>
            <Heading fontWeight={"bold"} size={"md"}>
              {title}
            </Heading>
            {subtitle === undefined ? null : (
              <Text mt={0.5} size={"sm"}>
                {subtitle}
              </Text>
            )}
          </Container>
          {onCloseModal ? (
            <Container>
              <IconButton
                color={theme.palette.gray[400]}
                icon={"xmark"}
                iconSize={"xs"}
                iconType={"fa-light"}
                id={"modal-close"}
                onClick={onCloseModal}
                variant={"ghost"}
              />
            </Container>
          ) : undefined}
        </Container>
      )}
      <Formik
        enableReinitialize={enableReinitialize}
        initialValues={initialValues}
        innerRef={innerRef}
        name={name}
        onSubmit={onSubmit}
        validationSchema={validationSchema}
      >
        {children}
      </Formik>
    </Container>
  );
};

export { Form, InnerForm };
