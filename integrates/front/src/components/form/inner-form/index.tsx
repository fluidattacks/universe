import { Button, Container } from "@fluidattacks/design";
import type { FormikValues } from "formik";
import { Form } from "formik";
import type { PropsWithChildren } from "react";
import { useTranslation } from "react-i18next";

import type { TInnerFormProps } from "../types";

const InnerForm = <Values extends FormikValues>({
  cancelButtonLabel,
  children,
  id = "modal-confirm",
  onCancel,
  onConfirm = "submit",
  submitButtonLabel,
  submitDisabled = false,
}: PropsWithChildren<Partial<TInnerFormProps<Values>>>): JSX.Element => {
  const { t } = useTranslation();
  const isSubmit = onConfirm === "submit";

  return (
    <Form>
      <Container>
        <Container display={"flex"} flexDirection={"column"} gap={0.5} mb={1.5}>
          {children}
        </Container>
        <Container display={"flex"} gap={0.75} maxWidth={"392px"}>
          {onCancel ? (
            <Button
              id={`${id}-cancel`}
              justify={"center"}
              onClick={onCancel}
              type={"reset"}
              variant={"tertiary"}
              width={"100%"}
            >
              {cancelButtonLabel ?? t("buttons.cancel")}
            </Button>
          ) : undefined}
          <Button
            disabled={submitDisabled}
            id={id}
            justify={"center"}
            onClick={isSubmit ? undefined : onConfirm}
            type={isSubmit ? "submit" : "button"}
            width={"100%"}
          >
            {submitButtonLabel ?? t("buttons.confirm")}
          </Button>
        </Container>
      </Container>
    </Form>
  );
};

export { InnerForm };
