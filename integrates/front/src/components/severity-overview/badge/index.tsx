import { Container, Text, Tooltip } from "@fluidattacks/design";
import capitalize from "lodash/capitalize";
import type { DefaultTheme } from "styled-components";
import { useTheme } from "styled-components";

type TSeverityOverviewBadgeVariant = "critical" | "high" | "low" | "medium";

interface ISeverityOverviewBadgeProps {
  readonly variant: TSeverityOverviewBadgeVariant;
  readonly value: number;
}

interface IVariant {
  iconColor: string;
  iconText: string;
  iconTextColor: string;
  textColor: string;
}

const getVariant = (
  theme: DefaultTheme,
  variant: TSeverityOverviewBadgeVariant,
  value: number,
): IVariant => {
  const iconColors: Record<string, string> = {
    critical: theme.palette.primary[700],
    high: theme.palette.error[500],
    low: theme.palette.warning[200],
    medium: theme.palette.warning[500],
    none: theme.palette.gray[200],
  };
  const textColors: Record<string, string> = {
    disabled: theme.palette.gray[400],
    enabled: theme.palette.gray[800],
  };
  const iconTextColors: Record<string, string> = {
    critical: theme.palette.white,
    high: theme.palette.white,
    low: theme.palette.gray[800],
    medium: theme.palette.gray[800],
    none: theme.palette.gray[400],
  };

  return {
    iconColor: value < 1 ? iconColors.none : iconColors[variant],
    iconText: capitalize(variant.charAt(0)),
    iconTextColor: value < 1 ? iconTextColors.none : iconTextColors[variant],
    textColor: value < 1 ? textColors.disabled : textColors.enabled,
  };
};

const SeverityOverviewBadge = ({
  variant,
  value,
}: ISeverityOverviewBadgeProps): JSX.Element => {
  const theme = useTheme();
  const { iconColor, iconText, iconTextColor, textColor } = getVariant(
    theme,
    variant,
    value,
  );
  const tooltipLabel = capitalize(variant.charAt(0)) + variant.slice(1);

  return (
    <Container
      alignItems={"center"}
      display={"inline-flex"}
      gap={0.5}
      minWidth={"43px"}
    >
      <Tooltip id={tooltipLabel} place={"top"} tip={tooltipLabel}>
        <Container
          alignItems={"center"}
          bgColor={iconColor}
          borderRadius={"4px"}
          cursor={"pointer"}
          display={"flex"}
          height={"22px"}
          maxWidth={"22px"}
          minWidth={"22px"}
        >
          <Text color={iconTextColor} size={"sm"} textAlign={"center"}>
            {iconText}
          </Text>
        </Container>
      </Tooltip>
      <Text color={textColor} size={"sm"}>
        {value}
      </Text>
    </Container>
  );
};

export type { ISeverityOverviewBadgeProps, TSeverityOverviewBadgeVariant };
export { SeverityOverviewBadge };
