import { screen } from "@testing-library/react";

import { PopUpMobile } from ".";
import { CustomThemeProvider } from "components/colors";
import { render } from "mocks";

describe("popUpMobile", (): void => {
  it("should render a component", (): void => {
    expect.hasAssertions();

    const props = {
      _portal: false,
      confirmButton: {
        onClick: (): void => {
          "test";
        },
        text: "Back to homepage",
      },
      container: document.getElementById("pop-up-container-1"),
      description:
        "We recommend that you use the platform when you are on a laptop.",
      image: {
        imageAlt: "viewMobileError",
        imageHeight: "175px",
        imageSrc: "integrates/resources/laptop",
        imageWidth: "100%",
      },
      title: "Our platform works best on desktop screens.",
      width: "335px",
    };

    render(
      <CustomThemeProvider>
        <PopUpMobile {...props} />
      </CustomThemeProvider>,
    );

    expect(screen.getByText(props.title)).toBeInTheDocument();
    expect(screen.getByText(props.description)).toBeInTheDocument();
    expect(screen.getByText(props.confirmButton.text)).toBeInTheDocument();
    expect(screen.getByAltText(props.image.imageAlt)).toBeInTheDocument();
  });

  it("should render when highligthing to side", (): void => {
    expect.hasAssertions();

    const props = {
      _portal: false,
      confirmButton: {
        onClick: (): void => {
          "test";
        },
        text: "Back to homepage",
      },
      container: document.getElementById("pop-up-container-2"),
      description:
        "To have a better experience we recommend that you use the platform when you are on a laptop.",
      highlightDescription: "Our platform works best on desktop screens",
      image: {
        imageAlt: "viewMobileError",
        imageSrc: "integrates/resources/radarAnimated2",
        imageWidth: "175px",
      },
      title: "We are now scanning your application.",
      width: "335px",
    };
    render(
      <CustomThemeProvider>
        <PopUpMobile {...props} />
      </CustomThemeProvider>,
    );

    expect(screen.getByText(props.title)).toBeInTheDocument();
    expect(screen.getByText(props.description)).toBeInTheDocument();
    expect(screen.getByText(props.highlightDescription)).toBeInTheDocument();
    expect(screen.getByText(props.confirmButton.text)).toBeInTheDocument();
    expect(screen.getByAltText(props.image.imageAlt)).toBeInTheDocument();
  });

  it("should render if the description is empty", (): void => {
    expect.hasAssertions();

    const props = {
      _portal: false,
      confirmButton: {
        onClick: (): void => {
          "test";
        },
        text: "",
      },
      container: null,
      description: "",
      highlightDescription: "",
      image: {
        imageAlt: "viewMobileError",
        imageSrc: "integrates/resources/radarAnimated2",
        imageWidth: "175px",
      },
      title: "",
      width: "335px",
    };
    render(
      <CustomThemeProvider>
        <PopUpMobile {...props} />
      </CustomThemeProvider>,
    );

    expect(screen.getAllByText(props.title)).toHaveLength(11);
    expect(screen.getAllByText(props.description)).toHaveLength(11);
    expect(screen.getAllByText(props.highlightDescription)).toHaveLength(11);
    expect(screen.getAllByText(props.confirmButton.text)).toHaveLength(11);
    expect(screen.getAllByAltText(props.image.imageAlt)).toHaveLength(1);
  });
});
