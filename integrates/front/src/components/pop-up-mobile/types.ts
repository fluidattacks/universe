interface IButtonProps {
  onClick: () => void;
  text: string;
}

interface IImageProps {
  imageSrc: string;
  imageWidth?: string;
  imageHeight?: string;
  imageAlt: string;
}
interface IPopUpMobileProps {
  _portal?: boolean;
  darkBackground?: boolean;
  cancelButton?: IButtonProps;
  confirmButton?: IButtonProps;
  container?: HTMLElement | null;
  image: IImageProps;
  title: string;
  highlightDescription?: string[] | string;
  description: string;
  width?: string;
}

export type { IPopUpMobileProps };
