import type { Property } from "csstype";

import type { ICardHeader } from "../card-header/types";

type TInputType = "date" | "number" | "text";
interface ICardWithInputProps extends ICardHeader {
  id?: string;
  inputNumberProps?: {
    decimalPlaces?: number;
    max?: number;
    min?: number;
    updateStoredData?: (newValue?: number) => void;
  };
  disabled?: boolean;
  inputAlign?: Property.TextAlign;
  inputType?: TInputType;
  minHeight?: string;
  minWidth?: string;
  name: string;
  placeholder?: string;
}

interface ICardInputContainerProps {
  $inputAlign?: Property.TextAlign;
  $minHeight?: string;
  $minWidth?: string;
}

export type { ICardWithInputProps, ICardInputContainerProps, TInputType };
