import { useMemo } from "react";

import { CardInputContainer } from "./styles";
import type { ICardWithInputProps } from "./types";

import { CardHeader } from "../card-header";
import { Input, InputDate, InputNumber } from "components/input";

const CardWithInput = ({
  headerChildren,
  description,
  disabled,
  id,
  inputAlign,
  inputType = "text",
  inputNumberProps,
  minHeight,
  minWidth,
  name,
  placeholder,
  tooltip,
  title,
}: ICardWithInputProps): JSX.Element => {
  const input = useMemo((): JSX.Element => {
    if (inputType === "text")
      return (
        <Input
          disabled={disabled}
          name={name}
          placeholder={placeholder}
          validateOnBlur={false}
        />
      );

    if (inputType === "date")
      return <InputDate disabled={disabled} name={name} />;

    const { decimalPlaces, max, min, updateStoredData } =
      inputNumberProps ?? {};

    return (
      <InputNumber
        decimalPlaces={decimalPlaces}
        disabled={disabled}
        max={max}
        min={min}
        name={name}
        placeholder={placeholder}
        updateStoredData={updateStoredData}
      />
    );
  }, [disabled, inputNumberProps, inputType, name, placeholder]);

  return (
    <CardInputContainer
      $inputAlign={inputAlign}
      $minHeight={minHeight}
      $minWidth={minWidth}
    >
      <CardHeader
        description={description}
        headerChildren={headerChildren}
        id={id}
        title={title}
        tooltip={tooltip}
      />
      {input}
    </CardInputContainer>
  );
};

export { CardWithInput };
