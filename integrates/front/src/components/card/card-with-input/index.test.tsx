import { Container } from "@fluidattacks/design";
import { screen } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { Formik } from "formik";

import type { TInputType } from "./types";

import { CardWithInput } from ".";
import { CustomThemeProvider } from "components/colors";
import { render } from "mocks";

describe("card with input", (): void => {
  interface ITestComponent {
    inputType?: TInputType;
  }

  const TestComponent = ({
    inputType,
  }: Readonly<ITestComponent>): JSX.Element => {
    const onSubmit: jest.Mock = jest.fn();

    return (
      <CustomThemeProvider>
        <Formik initialValues={{ input: "" }} onSubmit={onSubmit}>
          <Container display={"flex"} gap={1} width={"400px"} wrap={"wrap"}>
            <CardWithInput
              description={"Card description"}
              inputType={inputType}
              name={"cardInput"}
            />
          </Container>
        </Formik>
      </CustomThemeProvider>
    );
  };

  it("should render a card with input", (): void => {
    expect.hasAssertions();

    render(<TestComponent />);

    expect(screen.queryByText("Card description")).toBeInTheDocument();
    expect(
      screen.getByRole("combobox", { name: "cardInput" }),
    ).toBeInTheDocument();
  });

  it("should render a card with number input", async (): Promise<void> => {
    expect.hasAssertions();

    const user = userEvent.setup();

    render(<TestComponent inputType={"number"} />);
    const inputNumber = screen.getByRole("spinbutton", { name: "cardInput" });

    expect(screen.queryByText("Card description")).toBeInTheDocument();

    expect(inputNumber).toBeInTheDocument();

    await user.click(screen.getByRole("button", { name: "plus" }));

    expect(inputNumber).toHaveValue(1);

    await user.dblClick(screen.getByRole("button", { name: "minus" }));

    expect(inputNumber).toHaveValue(0);
  });

  it("should render a card with date input", async (): Promise<void> => {
    expect.hasAssertions();

    const user = userEvent.setup();

    render(<TestComponent inputType={"date"} />);

    expect(screen.queryByText("Card description")).toBeInTheDocument();

    const date = new Date();
    const month = date.toLocaleString("en", { month: "long" });
    const year = date.getFullYear();

    expect(
      screen.getByRole("button", { name: "Calendar" }),
    ).toBeInTheDocument();

    await user.click(screen.getByRole("button", { name: "Calendar" }));

    expect(screen.getByText(`${month} ${year}`)).toBeInTheDocument();

    await user.click(screen.getByText("15"));

    expect(
      screen.getByText(`Selected Date: ${month} 15, ${year}`),
    ).toBeInTheDocument();
  });
});
