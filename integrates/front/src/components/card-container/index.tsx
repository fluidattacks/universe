import { Container } from "@fluidattacks/design";
import { useTheme } from "styled-components";

import type { ICardContainerProps } from "./types";

const SPACING_SMALL = 0.5;
const SPACING_MEDIUM = 1.25;
const CardContainer = ({
  children,
  bgColor,
  border,
  gap = SPACING_SMALL,
  px = SPACING_MEDIUM,
  py = SPACING_MEDIUM,
  ...props
}: Readonly<ICardContainerProps>): JSX.Element => {
  const theme = useTheme();

  return (
    <Container
      bgColor={bgColor ?? theme.palette.white}
      border={border ?? `1px solid ${theme.palette.gray[200]}`}
      borderRadius={"4px"}
      display={"inline-flex"}
      flexDirection={"column"}
      gap={gap}
      px={px}
      py={py}
      {...props}
    >
      {children}
    </Container>
  );
};

export { CardContainer };
