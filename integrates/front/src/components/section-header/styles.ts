import { styled } from "styled-components";

import { BaseComponent } from "components/@core/styles";

const HeaderContainer = styled(BaseComponent).attrs<{ $isVisible: boolean }>(
  ({ $isVisible }): object => ({
    className: `header ${$isVisible ? "visible" : "no-visible"}`,
  }),
)`
  transition:
    transform 0.3s ease-in-out,
    opacity 0.3s ease-in-out;

  position: sticky;
  top: 0;

  &.visible {
    opacity: 1;
    transform: translateY(0);
    pointer-events: auto;
  }

  &.no-visible {
    opacity: 0;
    transform: translateY(-15px);
    pointer-events: none;
  }
`;

export { HeaderContainer };
