import { screen } from "@testing-library/react";
import { useTranslation } from "react-i18next";

import { SectionHeader } from ".";
import { render } from "mocks";

describe("sectionHeader", (): void => {
  const { t } = useTranslation();

  it("should render with all props", (): void => {
    expect.hasAssertions();

    const items = [
      {
        id: "1",
        label: "Tab 1",
        link: "/link1",
      },
      {
        id: "2",
        label: "Tab 2",
        link: "/link2",
      },
    ];

    render(
      <SectionHeader
        header={t("components.sectionHeader.title.normal")}
        tabs={items}
      />,
    );

    expect(
      screen.getByText("components.sectionHeader.title.normal"),
    ).toBeInTheDocument();
    expect(screen.getByText("Tab 1")).toBeInTheDocument();
    expect(screen.getByText("Tab 2")).toBeInTheDocument();
  });

  it("should render without primary and secondary buttons", (): void => {
    expect.hasAssertions();

    const items = [
      {
        id: "1",
        label: "Tab 1",
        link: "/link1",
      },
      {
        id: "2",
        label: "Tab 2",
        link: "/link2",
      },
    ];

    render(
      <SectionHeader
        header={t("components.sectionHeader.title.bold")}
        tabs={items}
      />,
    );

    expect(
      screen.getByText("components.sectionHeader.title.bold"),
    ).toBeInTheDocument();
    expect(screen.getByText("Tab 1")).toBeInTheDocument();
    expect(screen.getByText("Tab 2")).toBeInTheDocument();
  });
});
