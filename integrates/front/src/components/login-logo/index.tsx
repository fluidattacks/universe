import { CloudImage } from "@fluidattacks/design";

import { LogoWrapper } from "./styles";

const LoginLogo = (): JSX.Element => {
  return (
    <LogoWrapper>
      <CloudImage
        alt={"logo"}
        publicId={"integrates/login/loginLogoExtended"}
      />
    </LogoWrapper>
  );
};

export { LoginLogo };
