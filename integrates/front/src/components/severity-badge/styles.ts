import { styled } from "styled-components";

interface ITextProps {
  $color: string;
}

const Text = styled.p<ITextProps>`
  font-family: Roboto, sans-serif;
  font-size: 12px;
  font-style: normal;
  font-weight: 400;
  line-height: 20px;
  margin: 0;

  ${({ $color }): string => `color: ${$color}`}
`;

export { Text };
