import { Container } from "@fluidattacks/design";
import { isEmpty } from "lodash";
import { useTheme } from "styled-components";
import type { DefaultTheme } from "styled-components";

import { Text } from "./styles";

type TSeverityBadgeVariant = "critical" | "disable" | "high" | "low" | "medium";

interface ISeverityBadgeProps {
  readonly textL?: string;
  readonly textR: string;
  readonly variant: TSeverityBadgeVariant;
}

interface IVariant {
  bgColor: string;
  colorL: string;
  colorR: string;
}

const variants = (
  severity: TSeverityBadgeVariant,
  theme: DefaultTheme,
): IVariant => {
  switch (severity) {
    case "critical": {
      return {
        bgColor: theme.palette.primary[700],
        colorL: theme.palette.gray[800],
        colorR: theme.palette.white,
      };
    }
    case "disable": {
      return {
        bgColor: theme.palette.gray[200],
        colorL: theme.palette.gray[400],
        colorR: theme.palette.gray[400],
      };
    }
    case "high": {
      return {
        bgColor: theme.palette.error[500],
        colorL: theme.palette.gray[800],
        colorR: theme.palette.white,
      };
    }
    case "low": {
      return {
        bgColor: theme.palette.warning[200],
        colorL: theme.palette.gray[800],
        colorR: theme.palette.gray[800],
      };
    }
    case "medium":
    default: {
      return {
        bgColor: theme.palette.warning[500],
        colorL: theme.palette.gray[800],
        colorR: theme.palette.gray[800],
      };
    }
  }
};

const SeverityBadge = ({
  textL,
  textR,
  variant,
}: ISeverityBadgeProps): JSX.Element => {
  const theme = useTheme();
  const { bgColor, colorL, colorR } = variants(variant, theme);

  const hasTextL = !isEmpty(textL);
  const SMALL_PADDING = 0.125;
  const LARGE_PADDING = 0.25;

  return (
    <Container display={"inline-flex"}>
      {hasTextL ? (
        <Container
          alignItems={"center"}
          bgColor={theme.palette.white}
          border={`1px solid ${bgColor}`}
          borderRadius={"4px 0px 0px 4px"}
          display={"flex"}
          justify={"center"}
          px={0.25}
          py={0}
        >
          <Text $color={colorL}>{textL}</Text>
        </Container>
      ) : undefined}
      <Container
        alignItems={"center"}
        bgColor={bgColor}
        border={`1px solid ${bgColor}`}
        borderRadius={hasTextL ? "0px 4px 4px 0px" : "4px"}
        display={"flex"}
        px={hasTextL ? LARGE_PADDING : SMALL_PADDING}
        py={0}
      >
        <Text $color={colorR}>{textR}</Text>
      </Container>
    </Container>
  );
};

export type { ISeverityBadgeProps, TSeverityBadgeVariant };
export { SeverityBadge };
