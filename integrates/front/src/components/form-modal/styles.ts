import { styled } from "styled-components";

import { ModalContainer as ModalContainer1 } from "components/modal/styles";

const ModalContainer = styled(ModalContainer1)`
  #header {
    font-size: 20px;
  }
`;

export { ModalContainer };
