import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import * as React from "react";

import { FormModal } from ".";
import { CustomThemeProvider } from "components/colors";
import { InnerForm } from "components/form";
import { Input } from "components/input";
import { useModal } from "hooks/use-modal";
import { render } from "mocks";

const onSubmit = jest.fn();
const onCancel = jest.fn();
const onClose = jest.fn();

const FormModalComponent = ({
  children,
  initialValues,
  isOpen = true,
}: Readonly<{
  children: React.ReactNode;
  initialValues: { location: string };
  isOpen?: boolean;
  overflow?: boolean;
}>): JSX.Element => {
  const modalProps = useModal("test-form-modal");

  return (
    <CustomThemeProvider>
      <FormModal
        initialValues={initialValues}
        modalRef={{
          ...modalProps,
          close: onClose,
          isOpen,
        }}
        onSubmit={onSubmit}
        size={"sm"}
        subtitle={"Subtitle"}
        title={"Test Form Modal"}
      >
        {(props): JSX.Element => (
          <InnerForm {...props} onCancel={onCancel}>
            {children}
          </InnerForm>
        )}
      </FormModal>
    </CustomThemeProvider>
  );
};

describe("form as Modal Component", (): void => {
  it("should render modal", (): void => {
    expect.hasAssertions();

    render(
      <FormModalComponent initialValues={{ location: "Initial Location" }}>
        <Input
          label={"Location"}
          name={"location"}
          placeholder={"Location file"}
        />
      </FormModalComponent>,
    );

    expect(screen.getByText("Test Form Modal")).toBeInTheDocument();
    expect(screen.getByText("Subtitle")).toBeInTheDocument();
    expect(screen.getByText("Location")).toBeInTheDocument();

    const locationInput = screen.getByPlaceholderText("Location file");

    expect(locationInput).toHaveValue("Initial Location");

    expect(onSubmit).not.toHaveBeenCalled();
    expect(onCancel).not.toHaveBeenCalled();
    expect(onClose).not.toHaveBeenCalled();

    jest.clearAllMocks();
  });

  it("should render modal overflow", (): void => {
    expect.hasAssertions();

    render(
      <FormModalComponent
        initialValues={{ location: "Initial Location" }}
        overflow={false}
      >
        <Input
          label={"Location"}
          name={"location"}
          placeholder={"Location file"}
        />
      </FormModalComponent>,
    );

    expect(screen.getByText("Test Form Modal")).toBeInTheDocument();

    jest.clearAllMocks();
  });

  it("should not render modal", (): void => {
    expect.hasAssertions();

    render(
      <FormModalComponent
        initialValues={{ location: "Initial Location" }}
        isOpen={false}
      >
        <Input
          label={"Location"}
          name={"location"}
          placeholder={"Location file"}
        />
      </FormModalComponent>,
    );

    expect(screen.queryByText("Test Form Modal")).not.toBeInTheDocument();
    expect(screen.queryByText("Subtitle")).not.toBeInTheDocument();
    expect(screen.queryByText("Location")).not.toBeInTheDocument();

    jest.clearAllMocks();
  });

  it("should call handlers", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <FormModalComponent initialValues={{ location: "" }}>
        <Input
          label={"Location"}
          name={"location"}
          placeholder={"Location file"}
          suggestions={["test", "suggestion"]}
          tooltip={"Location"}
        />
      </FormModalComponent>,
    );

    const user = userEvent.setup();

    const locationInput = screen.getByPlaceholderText("Location file");
    await userEvent.type(locationInput, "A Location");

    expect(locationInput).toHaveValue("A Location");

    await user.click(screen.getByText("buttons.confirm"));
    await user.click(screen.getByText("buttons.cancel"));

    expect(locationInput).toHaveValue("");

    await waitFor((): void => {
      expect(onSubmit).toHaveBeenCalledTimes(1);
    });

    expect(onCancel).toHaveBeenCalledTimes(1);
    expect(onClose).not.toHaveBeenCalled();

    jest.clearAllMocks();
  });

  it("should close modal", async (): Promise<void> => {
    expect.hasAssertions();

    const user = userEvent.setup();

    render(
      <FormModalComponent initialValues={{ location: "" }}>
        <Input
          label={"Location"}
          name={"location"}
          placeholder={"Location file"}
          suggestions={["test", "suggestion"]}
          tooltip={"Location"}
        />
      </FormModalComponent>,
    );

    await user.click(screen.getAllByRole("button")[0]);

    expect(onClose).toHaveBeenCalledTimes(1);
  });
});
