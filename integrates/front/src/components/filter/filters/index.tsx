import { useField } from "formik";
import { useCallback } from "react";
import * as React from "react";

import { DateRangeFilter } from "./fields/date-range-filter";
import { NumberRangeFilter } from "./fields/number-range-filter";
import type { IFilter } from "./types";

import type { IDropDownOption } from "components/dropdown/types";
import { Input, InputNumber, InputTags, Select } from "components/input";

const Filter = ({
  id,
  label,
  onChange: updateStoredData,
  mappedOptions,
  minMaxRangeValues,
  numberRangeValues,
  type,
  value,
}: IFilter): JSX.Element => {
  const [field] = useField(id);
  const handleChange = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>): void => {
      field.onChange(event);
      updateStoredData({ id, value: event.target.value });
    },
    [field, id, updateStoredData],
  );

  const handleOnPaste = useCallback(
    (event: React.ClipboardEvent<HTMLInputElement>): void => {
      event.preventDefault();
      const inputText = event.clipboardData.getData("text").trim();
      updateStoredData({ id, value: inputText });
    },
    [id, updateStoredData],
  );

  const updateStoredDataNumberField = useCallback(
    (newValue: number | undefined): void => {
      updateStoredData({
        id,
        value: newValue === undefined ? "" : String(newValue),
      });
    },
    [id, updateStoredData],
  );

  const handleChangeSelect = useCallback(
    (item: IDropDownOption): void => {
      updateStoredData({
        id,
        value: item.value,
      });
    },
    [id, updateStoredData],
  );

  switch (type) {
    case "text": {
      return (
        <Input
          id={id}
          label={label}
          name={id}
          onChange={handleChange}
          onPaste={handleOnPaste}
          validateOnBlur={false}
          value={value}
        />
      );
    }
    case "number": {
      return (
        <InputNumber
          id={id}
          label={label}
          max={minMaxRangeValues?.[1] ?? Infinity}
          min={minMaxRangeValues?.[0]}
          name={id}
          onChange={handleChange}
          updateStoredData={updateStoredDataNumberField}
          value={value}
        />
      );
    }
    case "numberRange": {
      return (
        <NumberRangeFilter
          id={id}
          label={label}
          propsMax={{
            max: minMaxRangeValues?.[1],
            min: minMaxRangeValues?.[0],
            name: `${String(id)}-max`,
            placeholder: "Max",
            value: numberRangeValues?.[1],
          }}
          propsMin={{
            max: minMaxRangeValues?.[1],
            min: minMaxRangeValues?.[0],
            name: `${String(id)}-min`,
            placeholder: "Min",
            value: numberRangeValues?.[0],
          }}
          setPermanentData={updateStoredData}
        />
      );
    }
    case "select": {
      return (
        <Select
          handleOnChange={handleChangeSelect}
          id={id}
          items={mappedOptions ?? []}
          label={label}
          name={id}
          value={value}
        />
      );
    }
    case "dateRange": {
      return (
        <DateRangeFilter
          id={id}
          label={label}
          name={id}
          setPermanentData={updateStoredData}
        />
      );
    }
    case "tags": {
      return (
        <InputTags
          id={id}
          label={label}
          name={id}
          updateStoredData={updateStoredData}
          value={value}
        />
      );
    }
    case undefined:
    case "checkboxes":
    case "switch":
    default: {
      return <div key={id} />;
    }
  }
};

export { Filter };
