import type {
  IPermanentData,
  ISelectedOptions,
  ISwitchOptions,
  TInputType,
} from "../types";

interface IFilter {
  id: string;
  label: string;
  onChange: (permanentData: IPermanentData) => void;
  switchValues?: ISwitchOptions[];
  checkValues?: string[];
  mappedOptions?: ISelectedOptions[];
  minMaxRangeValues?: [number, number];
  numberRangeValues?: [number | undefined, number | undefined];
  rangeValues?: [string, string];
  type?: TInputType;
  value?: string;
}

export type { IFilter };
