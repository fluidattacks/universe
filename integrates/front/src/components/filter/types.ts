import type { Dispatch, SetStateAction } from "react";

type TInputType =
  | "checkboxes"
  | "dateRange"
  | "number"
  | "numberRange"
  | "select"
  | "switch"
  | "tags"
  | "text";

type TValueType =
  | "checkValues"
  | "numberRangeValues"
  | "rangeValues"
  | "switchValues"
  | "tags"
  | "value";

interface IFilter<IData extends object> extends IPermanentData {
  filterFn?:
    | "caseInsensitive"
    | "caseSensitive"
    | "includesInArray"
    | "includesInsensitive"
    | "includesSensitive";
  isBackFilter?: boolean;
  isLegacyFilter?: boolean;
  key:
    | keyof IData
    | ((
        arg0: IData,
        value?: string,
        rangeValues?: [string, string],
        numberRangeValues?: [number | undefined, number | undefined],
      ) => boolean);
  label: string;
  minMaxRangeValues?: [number, number];
  selectOptions?:
    | ISelectedOptions[]
    | string[]
    | ((arg0: IData[]) => ISelectedOptions[]);
  tags?: string[];
  type?: TInputType;
}

interface ISelectedOptions {
  header?: string;
  value: string;
}

interface ISwitchOptions {
  label: { on: string; off: string };
  value: string;
  checked?: boolean;
}

interface IFilterComp<IData extends object> extends IFilter<IData> {
  key: keyof IData;
}

interface IPermanentData extends ILegacyData {
  switchValues?: ISwitchOptions[];
  checkValues?: string[];
  id: string;
  value?: string;
  rangeValues?: [string, string];
  numberRangeValues?: [number | undefined, number | undefined];
  tags?: string[];
  valueType?: TValueType;
}

interface ILegacyData {
  legacySwitchValues?: ISwitchOptions[];
  legacyCheckValues?: string[];
  legacyValue?: string;
  legacyRangeValues?: [string, string];
  legacyNumberRangeValues?: [number | undefined, number | undefined];
  legacyTags?: string[];
}

interface IPermanentValuesProps {
  permaValue: IPermanentData;
  permaValues: IPermanentData[];
  setPermaValues?: Dispatch<SetStateAction<IPermanentData[]>>;
}

interface IFiltersProps<IData extends object> {
  dataset?: IData[];
  permaset?: [IPermanentData[], Dispatch<SetStateAction<IPermanentData[]>>];
  filters: IFilter<IData>[];
  setFilters:
    | Dispatch<SetStateAction<IFilter<IData>[]>>
    | ((newFilters: IFilter<IData>[]) => void);
}

export type {
  IFilter,
  IFilterComp,
  IFiltersProps,
  IPermanentData,
  IPermanentValuesProps,
  ISelectedOptions,
  ISwitchOptions,
  TInputType,
  TValueType,
};
