import type { ColumnDef } from "@tanstack/react-table";
import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { useMemo, useState } from "react";
import * as React from "react";

import type { IFilter, IPermanentData } from "./types";
import { getInitialValues, resetFilters } from "./utils";

import { Filters } from ".";
import { CustomThemeProvider } from "components/colors";
import { Table } from "components/table";
import { useTable } from "hooks";
import { render } from "mocks";
import { setFiltersUtil } from "utils/set-filters";

interface IRandomData {
  category: string;
  date: string;
  severity: number;
}

interface ITestComponentProps {
  readonly data: IRandomData[];
  readonly filters: IFilter<IRandomData>[];
}

const TestComponent: React.FC<ITestComponentProps> = ({
  data,
  filters,
}): JSX.Element => {
  const [filterHelper, setFilterHelper] =
    useState<IFilter<IRandomData>[]>(filters);

  const filteredData = setFiltersUtil(data, filterHelper);
  const tableRef = useTable("testTable");

  const columns = useMemo(
    (): ColumnDef<IRandomData>[] => [
      {
        accessorKey: "severity",
        header: "Severity",
      },
      {
        accessorKey: "date",
        header: "Report Date",
      },
      {
        accessorKey: "category",
        header: "Vulnerability Category",
      },
    ],
    [],
  );

  return (
    <CustomThemeProvider>
      <Filters filters={filterHelper} setFilters={setFilterHelper} />
      <Table
        columns={columns}
        data={filteredData}
        options={{ enableSearchBar: false }}
        tableRef={tableRef}
      />
    </CustomThemeProvider>
  );
};

describe("filters", (): void => {
  const dataset: IRandomData[] = [
    {
      category: "Cross-Site Scripting",
      date: "2022-01-20",
      severity: 12,
    },
    {
      category: "SQL Injection",
      date: "2021-01-01",
      severity: 12,
    },
    {
      category: "Insecure Deserialization",
      date: "2023-01-22",
      severity: 13,
    },
    {
      category: "Insufficient Logging & Monitoring",
      date: "2022-08-28",
      severity: 7,
    },
    {
      category: "Broken Authentication",
      date: "2023-08-19",
      severity: 6,
    },
    {
      category: "Loose Object Level Authorization",
      date: "2022-06-20",
      severity: 9,
    },
    {
      category: "Loss trabeability",
      date: "2023-03-31",
      severity: 11,
    },
    {
      category: "Loss trabeability",
      date: "2022-03-31",
      severity: 11,
    },
    {
      category: "Information disclosure",
      date: "2022-05-12",
      severity: 9,
    },
    {
      category: "Information disclosure",
      date: "2022-03-07",
      severity: 6,
    },
    {
      category: "Broken Authentication",
      date: "2023-08-23",
      severity: 11,
    },
    {
      category: "Broken Authentication",
      date: "2022-06-29",
      severity: 9,
    },
    {
      category: "Insecure functionality",
      date: "2023-08-28",
      severity: 12,
    },
    {
      category: "Insecure functionality",
      date: "2023-06-12",
      severity: 14,
    },
    {
      category: "Local file inclusion",
      date: "2023-04-16",
      severity: 12,
    },
    {
      category: "Local file inclusion",
      date: "2021-07-30",
      severity: 8,
    },
    {
      category: "Loss trabeability",
      date: "2021-01-26",
      severity: 10,
    },
    {
      category: "Loss trabeability",
      date: "2023-04-01",
      severity: 0,
    },
    {
      category: "Information disclosure",
      date: "2022-03-24",
      severity: 7,
    },
    {
      category: "Information disclosure",
      date: "2023-05-29",
      severity: 1,
    },
    {
      category: "Broken Authentication",
      date: "2022-10-19",
      severity: 7,
    },
    {
      category: "Broken Authentication",
      date: "2022-06-24",
      severity: 1,
    },
    {
      category: "Insecure functionality",
      date: "2022-08-17",
      severity: 7,
    },
    {
      category: "Insecure functionality",
      date: "2023-05-12",
      severity: 5,
    },
    {
      category: "Local file inclusion",
      date: "2023-02-09",
      severity: 14,
    },
    {
      category: "Local file inclusion",
      date: "2022-08-04",
      severity: 15,
    },
    {
      category: "Loss trabeability",
      date: "-",
      severity: 6,
    },
  ];

  // The number of rows that should be displayed by default
  const totalOfRows = 11;

  function countItemsFulfillingCondition<T>(
    items: T[],
    condition: (item: T) => boolean,
  ): number {
    return items.filter(condition).length;
  }

  function countNumberOfVisibleRows(
    condition: (item: IRandomData) => boolean,
  ): number {
    const total = countItemsFulfillingCondition(dataset, condition);

    if (total === 0) return 2;

    if (total > 10) return 11;

    return total + 1;
  }

  const assertNumberOfRows = async (numberOfRows: number): Promise<void> => {
    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(numberOfRows);
    });
  };
  const clearFilters = async (): Promise<void> => {
    await userEvent.click(
      screen.getByRole("button", { name: "filter.cancel" }),
    );

    await assertNumberOfRows(totalOfRows);
  };
  const openFilters = async (): Promise<void> => {
    await userEvent.click(screen.getByRole("button", { name: "filter.title" }));
  };

  const closeFilters = async (): Promise<void> => {
    await userEvent.click(
      screen.getByRole("button", { name: "close-filters" }),
    );
  };

  it("should display and test button", async (): Promise<void> => {
    expect.hasAssertions();

    const filters: IFilter<IRandomData>[] = [];

    render(<TestComponent data={dataset} filters={filters} />);

    expect(screen.getByRole("table")).toBeInTheDocument();

    await assertNumberOfRows(totalOfRows);

    expect(
      screen.getByRole("button", { name: "filter.title" }),
    ).toBeInTheDocument();

    await userEvent.click(screen.getByRole("button", { name: "filter.title" }));

    expect(
      screen.getByRole("button", { hidden: true, name: "filter.cancel" }),
    ).toBeInTheDocument();
    expect(screen.getAllByText("filter.title")).toHaveLength(2);
  });

  it("should filter by range of severitys", async (): Promise<void> => {
    expect.hasAssertions();

    const filters: IFilter<IRandomData>[] = [
      {
        id: "severity",
        key: "severity",
        label: "Severity",
        type: "numberRange",
      },
    ];

    render(<TestComponent data={dataset} filters={filters} />);

    expect(screen.getByRole("table")).toBeInTheDocument();

    await assertNumberOfRows(totalOfRows);

    await openFilters();

    const spinMax = screen.getByPlaceholderText("Max");

    expect(spinMax).toBeInTheDocument();

    expect(spinMax).toHaveAttribute("placeholder", "Max");

    await userEvent.type(screen.getByPlaceholderText("Min"), "1");
    await userEvent.type(screen.getByPlaceholderText("Max"), "5");
    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(4);
    });

    expect(
      screen.getAllByText("Severity")[0].nextElementSibling?.textContent,
    ).toBe("1 - 5");

    const plusButtons = screen.getAllByRole("button", { name: "plus" });

    expect(plusButtons[0]).toBeInTheDocument();

    await userEvent.click(plusButtons[0]);

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(2);
    });

    expect(
      screen.getAllByText("Severity")[0].nextElementSibling?.textContent,
    ).toBe("2 - 5");
    expect(
      screen.queryByText("table.noDataIndication"),
    ).not.toBeInTheDocument();

    const minusButtons = screen.getAllByRole("button", { name: "minus" });

    expect(minusButtons[1]).toBeInTheDocument();

    await userEvent.click(minusButtons[1]);

    await assertNumberOfRows(
      countNumberOfVisibleRows(
        (item): boolean => item.severity >= 2 && item.severity <= 4,
      ),
    );

    expect(screen.getByText("table.noDataIndication")).toBeInTheDocument();

    const tag =
      screen.getAllByText("Severity")[0].nextElementSibling?.textContent;
    const xMark = screen.getByTestId("xmark-icon");

    expect(tag).toBe("2 - 4");

    // Clean the filter by clicking on the xmark tag icon
    await userEvent.click(xMark.parentElement ?? xMark);
    await assertNumberOfRows(totalOfRows);

    await closeFilters();
  });

  it("should filter date range", async (): Promise<void> => {
    expect.hasAssertions();

    const filters: IFilter<IRandomData>[] = [
      {
        id: "date",
        key: "date",
        label: "Date Range",
        type: "dateRange",
      },
    ];

    render(<TestComponent data={dataset} filters={filters} />);

    expect(screen.getByRole("table")).toBeInTheDocument();

    await openFilters();

    await waitFor((): void => {
      expect(
        screen.queryByRole("spinbutton", { name: /month, Start Date/u }),
      ).toBeInTheDocument();
    });

    // Typing date manually
    await userEvent.type(
      screen.getByRole("spinbutton", { name: /month, Start Date/u }),
      "01",
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: /day, Start Date/u }),
      "01",
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: /year, Start Date/u }),
      "2021",
    );

    await userEvent.type(
      screen.getByRole("spinbutton", { name: /month, End Date/u }),
      "03",
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: /day, End Date/u }),
      "30",
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: /year, End Date/u }),
      "2022",
    );

    await assertNumberOfRows(7);

    const storedDateRange =
      screen.getAllByText("Date Range")[0].nextElementSibling?.textContent;

    const xMark = screen.getByTestId("xmark-icon");

    expect(storedDateRange).toBe("January 1, 2021 - March 30, 2022");

    // Click on the xmark container, related to the stored data
    await userEvent.click(xMark.parentElement ?? xMark);
    await assertNumberOfRows(totalOfRows);

    await closeFilters();
  });

  it("should filter custom", async (): Promise<void> => {
    expect.hasAssertions();

    function customFilterFn(data: IRandomData): boolean {
      return data.category === "Loss trabeability";
    }

    const filters: IFilter<IRandomData>[] = [
      { id: "name", key: customFilterFn, label: "name" },
    ];

    render(<TestComponent data={dataset} filters={filters} />);

    expect(screen.getByRole("table")).toBeInTheDocument();

    await assertNumberOfRows(countNumberOfVisibleRows(customFilterFn));
  });

  it("should filter by text", async (): Promise<void> => {
    expect.hasAssertions();

    const filters: IFilter<IRandomData>[] = [
      {
        id: "category",
        key: "category",
        label: "Category",
        type: "text",
      },
    ];

    render(<TestComponent data={dataset} filters={filters} />);

    await waitFor((): void => {
      expect(screen.getByRole("table")).toBeInTheDocument();
    });

    await openFilters();

    await waitFor((): void => {
      expect(
        document.querySelector("input[name='category']"),
      ).toBeInTheDocument();
    });

    await userEvent.type(
      document.querySelectorAll("input[name='category']")[0],
      "Insecure",
    );

    await assertNumberOfRows(
      countNumberOfVisibleRows((item): boolean =>
        item.category.startsWith("Insecure"),
      ),
    );

    expect(
      screen.getAllByText("Category")[0].nextElementSibling?.textContent,
    ).toBe("Insecure");

    await clearFilters();
    await closeFilters();
  });

  it("should filter text case sensitive", async (): Promise<void> => {
    expect.hasAssertions();

    const filters: IFilter<IRandomData>[] = [
      {
        filterFn: "caseSensitive",
        id: "category",
        key: "category",
        label: "Category",
        type: "text",
      },
    ];

    render(<TestComponent data={dataset} filters={filters} />);

    await openFilters();

    await waitFor((): void => {
      expect(
        document.querySelector("input[name='category']"),
      ).toBeInTheDocument();
    });

    await userEvent.type(
      document.querySelectorAll("input[name='category']")[0],
      "insecure",
    );

    await assertNumberOfRows(
      countNumberOfVisibleRows((item): boolean =>
        item.category.includes("insecure"),
      ),
    );

    expect(screen.getByText("table.noDataIndication")).toBeInTheDocument();
    expect(
      screen.getAllByText("Category")[0].nextElementSibling?.textContent,
    ).toBe("insecure");

    await clearFilters();
    await closeFilters();
  });

  it("should filter text includes case insensitive", async (): Promise<void> => {
    expect.hasAssertions();

    const filters: IFilter<IRandomData>[] = [
      {
        filterFn: "includesInsensitive",
        id: "category",
        key: "category",
        label: "Category",
        type: "text",
      },
    ];

    render(<TestComponent data={dataset} filters={filters} />);

    await openFilters();

    await waitFor((): void => {
      expect(
        document.querySelector("input[name='category']"),
      ).toBeInTheDocument();
    });

    await userEvent.type(
      document.querySelectorAll("input[name='category']")[0],
      "level",
    );

    await assertNumberOfRows(
      countNumberOfVisibleRows((item): boolean =>
        item.category.toLowerCase().includes("level"),
      ),
    );

    expect(
      screen.queryByText("table.noDataIndication"),
    ).not.toBeInTheDocument();
    expect(
      screen.getAllByText("Category")[0].nextElementSibling?.textContent,
    ).toBe("level");

    await clearFilters();
    await closeFilters();
  });

  it("should filter text includes case sensitive", async (): Promise<void> => {
    expect.hasAssertions();

    const filters: IFilter<IRandomData>[] = [
      {
        filterFn: "includesSensitive",
        id: "category",
        key: "category",
        label: "Category",
        type: "text",
      },
    ];

    render(<TestComponent data={dataset} filters={filters} />);

    await openFilters();

    await waitFor((): void => {
      expect(
        document.querySelector("input[name='category']"),
      ).toBeInTheDocument();
    });

    await userEvent.type(
      document.querySelector("input[name='category']") as HTMLElement,
      "File",
    );

    await assertNumberOfRows(
      countNumberOfVisibleRows((item): boolean =>
        item.category.includes("File"),
      ),
    );

    expect(screen.getByText("table.noDataIndication")).toBeInTheDocument();
    expect(
      screen.getAllByText("Category")[0].nextElementSibling?.textContent,
    ).toBe("File");

    await clearFilters();

    await userEvent.type(
      document.querySelector("input[name='category']") as HTMLElement,
      "file",
    );
    await assertNumberOfRows(
      countNumberOfVisibleRows((item): boolean =>
        item.category.includes("file"),
      ),
    );

    expect(
      screen.getAllByText("Category")[0].nextElementSibling?.textContent,
    ).toBe("file");

    await clearFilters();
    await closeFilters();
  });

  it("should handle stored values with mixed value types", (): void => {
    expect.hasAssertions();

    const storedValues: IPermanentData[] = [
      { id: "1", value: "stringValue", valueType: "value" },
      { id: "2", rangeValues: ["start", "end"], valueType: "rangeValues" },
      { id: "3", numberRangeValues: [1, 10], valueType: "numberRangeValues" },
    ];
    const result = getInitialValues(storedValues);

    expect(result).toStrictEqual({
      "1": "stringValue",
      "2-max": "end",
      "2-min": "start",
      "3-max": 10,
      "3-min": 1,
    });
  });

  it("should reset text-related filters", (): void => {
    expect.hasAssertions();

    const filter1: IFilter<IRandomData> = {
      filterFn: "caseInsensitive",
      id: "category",
      key: "category",
      label: "",
      selectOptions: [{ header: "Value", value: "value" }],
      type: "select",
      value: "value",
    };
    const result = resetFilters(filter1, filter1, jest.fn());

    expect(result.filterFn).toBe("caseInsensitive");
    expect(result).toStrictEqual({
      ...filter1,
      isBackFilter: undefined,
      value: "",
      valueType: "value",
    });

    const filter2: IFilter<IRandomData> = {
      id: "category",
      key: "category",
      label: "",
      selectOptions: [{ header: "Value", value: "value" }],
      type: "select",
      value: "anotherval",
    };
    const result2 = resetFilters(filter2, filter2, jest.fn());

    expect(result2.filterFn).toBeUndefined();
    expect(result2).toStrictEqual({
      ...filter2,
      filterFn: undefined,
      isBackFilter: undefined,
      value: "",
      valueType: "value",
    });
  });

  it("should render legacy filters", async (): Promise<void> => {
    expect.hasAssertions();

    const filters: IFilter<IRandomData>[] = [
      {
        id: "date",
        isLegacyFilter: true,
        key: "date",
        label: "Date Range",
        legacyRangeValues: ["01-01-2021", "03-30-2022"],
        type: "dateRange",
      },
      {
        filterFn: "includesSensitive",
        id: "category",
        isLegacyFilter: true,
        key: "category",
        label: "Category",
        legacyValue: "SQL Injection",
        type: "text",
      },
      {
        id: "severity",
        isLegacyFilter: true,
        key: "severity",
        label: "Severity",
        legacyNumberRangeValues: [1, 5],
        type: "numberRange",
      },
    ];

    render(<TestComponent data={dataset} filters={filters} />);

    await waitFor((): void => {
      expect(
        screen.getAllByText("Category")[0].nextElementSibling?.textContent,
      ).toBe("SQL Injection");
    });
    await waitFor((): void => {
      expect(
        screen.getAllByText("Date Range")[0].nextElementSibling?.textContent,
      ).toBe("January 1, 2021 - March 30, 2022");
    });
    await waitFor((): void => {
      expect(
        screen.getAllByText("Severity")[0].nextElementSibling?.textContent,
      ).toBe("1 - 5");
    });
  });
});
