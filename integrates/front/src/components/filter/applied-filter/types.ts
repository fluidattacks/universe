import type { IFilter } from "../types";

interface IAppliedFilter {
  filter: IFilter<object>;
  filterValue?: string;
  label: string;
  onClose: (filterToReset: IFilter<object>) => () => void;
}
interface IAppliedFilters {
  filters: IFilter<object>[];
  dataset?: object[];
  onClose: (filterToReset: IFilter<object>) => () => void;
}

export type { IAppliedFilter, IAppliedFilters };
