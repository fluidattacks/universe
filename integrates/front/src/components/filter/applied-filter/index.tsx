import { Container, Tag } from "@fluidattacks/design";
import _ from "lodash";

import type { IAppliedFilter } from "./types";

const AppliedFilter = ({
  filter,
  label,
  onClose,
  filterValue,
}: IAppliedFilter): JSX.Element | undefined => {
  if (_.isNil(filterValue) || _.isEmpty(String(filterValue))) {
    return undefined;
  }

  return (
    <Container key={label} pr={0.25}>
      <Tag
        filterValues={filterValue}
        icon={"exclamation-circle"}
        id={"remove-filter"}
        onClose={onClose(filter)}
        tagLabel={`${label} `}
      />
    </Container>
  );
};

export { AppliedFilter };
