import { Button, Container, SlideOutMenu } from "@fluidattacks/design";
import { Form, FormikProvider, useFormik } from "formik";
import { useCallback, useEffect } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { AppliedFilters } from "./applied-filters";
import { Filter } from "./filters";
import {
  areLegacyFilters,
  getInitialValues,
  getMappedOptions,
  resetFilters,
  resetStoredData,
  updateFilterData,
} from "./utils";

import type {
  IFilter,
  IFiltersProps,
  IPermanentData,
} from "components/filter/types";
import { useModal } from "hooks";
import { Logger } from "utils/logger";

const Filters = <IData extends object>({
  dataset = undefined,
  permaset = undefined,
  filters,
  setFilters,
}: IFiltersProps<IData>): JSX.Element => {
  const { t } = useTranslation();
  const { close, isOpen, open } = useModal("filter-slide-menu");

  const [storedValues, setStoredValues] = permaset ?? [undefined, undefined];

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: getInitialValues(storedValues),
    onSubmit: close,
  });

  const updateStoredValue = useCallback(
    (data: IPermanentData): void => {
      setStoredValues?.((storedData): IPermanentData[] =>
        storedData.map((permadata): IPermanentData => {
          if (permadata.id === data.id) {
            return {
              ...permadata,
              ...data,
            };
          }

          return permadata;
        }),
      );
    },
    [setStoredValues],
  );
  const resetStoredValue = useCallback(
    (
      filterToReset: IFilter<IData>,
      permData: IPermanentData[],
    ): IPermanentData[] => {
      return permData.map((storedData): IPermanentData => {
        if (storedData.id === filterToReset.id) {
          return resetStoredData(storedData);
        }

        return {
          ...storedData,
        };
      });
    },
    [],
  );

  const removeFilter = useCallback(
    (filterToReset: IFilter<IData>): VoidFunction => {
      return (): void => {
        setFilters(
          filters.map((filter): IFilter<IData> => {
            if (filter.id === filterToReset.id) {
              return resetFilters(filter, filterToReset, formik.setFieldValue);
            }

            return {
              ...filter,
            };
          }),
        );
        setStoredValues?.((permData): IPermanentData[] =>
          resetStoredValue(filterToReset, permData),
        );
      };
    },
    [
      filters,
      formik.setFieldValue,
      resetStoredValue,
      setFilters,
      setStoredValues,
    ],
  );

  const resetFiltersHandler = useCallback(
    (event: Readonly<React.FormEvent>): void => {
      event.stopPropagation();
      setFilters(
        filters.map((filter): IFilter<IData> => {
          // By setting shouldResetField to false, the process is faster
          return resetFilters(filter, filter, formik.setFieldValue, false);
        }),
      );
      setStoredValues?.(storedValues.map(resetStoredData));
      formik.setValues({}).catch((): void => {
        Logger.error("An error occurred during the filtering process");
      });
    },
    [filters, formik, storedValues, setFilters, setStoredValues],
  );

  const updateStoredData = useCallback(
    (permanentData: IPermanentData): void => {
      setFilters(
        filters.map((filter): IFilter<IData> => {
          if (filter.id === permanentData.id) {
            updateStoredValue(permanentData);

            return {
              ...filter,
              ...permanentData,
            };
          }

          return filter;
        }),
      );
    },
    [filters, setFilters, updateStoredValue],
  );

  useEffect((): void => {
    if (storedValues !== undefined) {
      setFilters(
        filters.map(
          (filter): IFilter<IData> =>
            updateFilterData(filter, "storage", storedValues),
        ),
      );
    }
    if (areLegacyFilters(filters)) {
      const updatedFilters = filters.map((filter): IFilter<IData> => {
        if (filter.isLegacyFilter === true) {
          return updateFilterData(filter, "legacy");
        }

        return updateFilterData(filter, "storage", storedValues);
      });
      setFilters(updatedFilters);
      setStoredValues?.(updatedFilters);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <React.Fragment>
      <Container
        alignItems={"center"}
        display={"flex"}
        gap={0.25}
        maxWidth={"100%"}
        wrap={"wrap"}
      >
        <Container pr={0.25}>
          <Button
            icon={"filter"}
            id={"filterBtn"}
            onClick={isOpen ? close : open}
            variant={"ghost"}
          >
            {t("filter.title")}
          </Button>
        </Container>
        <AppliedFilters
          dataset={dataset}
          filters={filters as IFilter<object>[]}
          onClose={removeFilter}
        />
      </Container>
      <FormikProvider value={formik}>
        <Form>
          <SlideOutMenu
            closeIconId={"close-filters"}
            isOpen={isOpen}
            onClose={close}
            primaryButtonText={t("filter.confirm")}
            primaryOnClick={close}
            secondaryButtonText={t("filter.cancel")}
            secondaryOnClick={resetFiltersHandler}
            title={t("filter.title")}
          >
            <Container display={"flex"} flexDirection={"column"} gap={1}>
              {isOpen
                ? filters.map((filter: IFilter<IData>): JSX.Element => {
                    const mappedOptions =
                      filter.type === "select"
                        ? getMappedOptions(filter as IFilter<object>, dataset)
                        : undefined;

                    return (
                      <Filter
                        id={filter.id}
                        key={filter.id}
                        label={filter.label}
                        mappedOptions={mappedOptions}
                        minMaxRangeValues={filter.minMaxRangeValues}
                        numberRangeValues={filter.numberRangeValues}
                        onChange={updateStoredData}
                        rangeValues={filter.rangeValues}
                        type={filter.type}
                        value={filter.value}
                      />
                    );
                  })
                : undefined}
            </Container>
          </SlideOutMenu>
        </Form>
      </FormikProvider>
    </React.Fragment>
  );
};

export { Filters };
