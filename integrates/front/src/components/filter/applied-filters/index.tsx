import { Container, Text } from "@fluidattacks/design";
import _ from "lodash";
import { Fragment } from "react";
import { useTranslation } from "react-i18next";

import {
  formatCheckValues,
  formatDateRange,
  formatNumberRange,
  formatSwitchValues,
  formatTags,
  formatValue,
} from "./utils";

import { AppliedFilter } from "../applied-filter";
import type { IAppliedFilter, IAppliedFilters } from "../applied-filter/types";
import { getMappedOptions } from "components/filter/utils";

const AppliedFilters = ({
  filters,
  dataset,
  onClose,
}: Readonly<IAppliedFilters>): JSX.Element => {
  const { t } = useTranslation();
  const activeFilters = filters
    .map((filter): JSX.Element | undefined => {
      const {
        label,
        rangeValues,
        numberRangeValues,
        type,
        value,
        checkValues,
        switchValues,
        tags,
      } = filter;

      const mappedOptions = getMappedOptions(filter, dataset);

      switch (type) {
        case "numberRange": {
          const formattedValue = formatNumberRange(numberRangeValues);

          return (
            <AppliedFilter
              filter={filter}
              filterValue={formattedValue}
              key={label}
              label={label}
              onClose={onClose}
            />
          );
        }
        case "dateRange": {
          const formattedValue = formatDateRange(rangeValues);

          return (
            <AppliedFilter
              filter={filter}
              filterValue={formattedValue}
              key={label}
              label={label}
              onClose={onClose}
            />
          );
        }

        case "checkboxes": {
          const formattedCheckValues = formatCheckValues(
            checkValues,
            mappedOptions,
          );

          return (
            <AppliedFilter
              filter={filter}
              filterValue={formattedCheckValues}
              key={label}
              label={label}
              onClose={onClose}
            />
          );
        }

        case "switch": {
          const formattedSwitchValues = formatSwitchValues(switchValues);

          return (
            <AppliedFilter
              filter={filter}
              filterValue={formattedSwitchValues}
              key={label}
              label={label}
              onClose={onClose}
            />
          );
        }

        case "tags": {
          const formattedTags = formatTags(tags);

          return (
            <AppliedFilter
              filter={filter}
              filterValue={formattedTags}
              key={label}
              label={label}
              onClose={onClose}
            />
          );
        }

        case undefined:
        case "number":
        case "select":
        case "text":
        default: {
          const formattedValue = formatValue(value, mappedOptions);

          return (
            <AppliedFilter
              filter={filter}
              filterValue={formattedValue}
              key={label}
              label={label}
              onClose={onClose}
            />
          );
        }
      }
    })
    .filter((filter): boolean => {
      if (filter === undefined) return false;

      const { filterValue } = filter.props as IAppliedFilter;

      return !_.isNil(filterValue) && !_.isEmpty(String(filterValue));
    });

  return (
    <Fragment>
      {activeFilters.length > 0 ? (
        <Container pr={0.25}>
          <Text display={"inline-block"} fontWeight={"bold"} size={"sm"}>
            {t("filter.applied")}
          </Text>
        </Container>
      ) : null}
      {activeFilters}
    </Fragment>
  );
};

export { AppliedFilters };
