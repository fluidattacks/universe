import { Icon } from ".";
import { CustomThemeProvider } from "components/colors";
import { render } from "mocks";

describe("icon", (): void => {
  it("should render Icon component with default props", (): void => {
    expect.hasAssertions();

    const { container } = render(
      <CustomThemeProvider>
        <Icon
          color={"#2e90fa"}
          icon={"face-awesome"}
          iconType={"fa-solid"}
          size={"lg"}
        />
      </CustomThemeProvider>,
    );

    expect(container.firstChild).toBeInTheDocument();
  });
});
