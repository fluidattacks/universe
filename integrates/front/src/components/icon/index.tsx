/*
 * Set a key attribute to force DOM updates for ...
 * https://stackoverflow.com/questions/49899245/setstate-not-updating-font-awesome-icon
 */
import { IconWrap } from "./styles";
import type { IIconProps } from "./types";

const Icon = ({
  iconClass = "",
  addStyle,
  clickable,
  color,
  disabled,
  hoverColor,
  icon,
  mt,
  mr,
  mb,
  ml,
  onClick,
  size,
  transition,
  iconType = "fa-solid",
  rotation,
}: IIconProps): JSX.Element => {
  return (
    <IconWrap
      $clickable={clickable}
      $color={color}
      $disabled={disabled}
      $hoverColor={hoverColor}
      $mb={mb}
      $ml={ml}
      $mr={mr}
      $mt={mt}
      $rotation={rotation}
      $size={size}
      $transition={transition}
      data-testid={`${icon}-icon`}
      key={`${iconType}-${icon}`}
      onClick={onClick}
    >
      <i className={`${iconType} fa-${icon} ${iconClass}`} style={addStyle} />
    </IconWrap>
  );
};

export { Icon };
