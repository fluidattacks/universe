import { CloudImage, Container } from "@fluidattacks/design";

interface ILogoProps {
  readonly width?: string;
}
const InlineLogo = ({ width = "150px" }: ILogoProps): JSX.Element => {
  return (
    <Container width={width}>
      <CloudImage alt={"logo"} publicId={"integrates/login/platformLogo"} />
    </Container>
  );
};

export { InlineLogo };
