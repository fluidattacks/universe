import { styled } from "styled-components";

const AsteriskSpan = styled.span`
  color: ${({ theme }): string => theme.palette.error["500"]};
  margin-left: 0.25rem;
  font-size: 1rem;
  vertical-align: bottom;
`;

const InputContainer = styled.label<{
  $alert?: boolean;
  $disabled: boolean;
  $error?: boolean;
}>`
  display: flex;
  align-items: center;
  position: relative;
  cursor: pointer;
  font-family: ${({ theme }): string => theme.typography.type.primary};
  font-size: ${({ theme }): string => theme.typography.text.sm};
  font-weight: ${({ theme, $alert = false }): string =>
    $alert ? "inherit" : theme.typography.weight.regular};
  line-height: ${({ theme }): string => theme.spacing[1]};
  letter-spacing: 0;
  text-align: left;
  color: ${({ theme, $alert = false, $disabled }): string => {
    const isDisabled = $disabled ? 300 : 600;

    return $alert ? "inherit" : theme.palette.gray[isDisabled];
  }};

  input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
    height: 0;
    width: 0;
  }

  svg {
    margin-right: ${({ theme }): string => theme.spacing[0.5]};
    top: 0;
    left: 0;
    height: ${({ theme }): string => theme.spacing[0.5]};
    width: ${({ theme }): string => theme.spacing[0.5]};
    padding: ${({ theme }): string => theme.spacing[0.25]};
    background-color: ${({ theme }): string => theme.palette.white};
    border-radius: ${({ theme }): string => theme.spacing[0.25]};
    border: 1px solid ${({ theme }): string => theme.palette.gray[600]};
  }

  svg > path {
    fill: ${({ theme }): string => theme.palette.white};
  }

  input:checked ~ span > svg {
    background-color: ${({ theme }): string => theme.palette.primary[500]};
    border: 1px solid ${({ theme }): string => theme.palette.primary[500]};
  }

  input:disabled ~ span > svg {
    border: 1px solid ${({ theme }): string => theme.palette.gray[300]};
  }

  input:not(:disabled):hover ~ span > svg {
    box-shadow: 0 0 0 4px ${({ theme }): string => theme.palette.gray[100]};
  }

  input:checked:disabled ~ span > svg {
    background-color: ${({ theme }): string => theme.palette.gray[300]};
    border: 1px solid ${({ theme }): string => theme.palette.gray[300]};
  }

  input:not(:disabled):hover:checked ~ span > svg {
    box-shadow: 0 0 0 4px ${({ theme }): string => theme.palette.primary[50]};
  }

  input:checked ~ span > svg > path {
    fill: ${({ theme }): string => theme.palette.white};
  }

  input:checked:disabled ~ span > svg > path {
    fill: ${({ theme }): string => theme.palette.white};
  }
`;

export { InputContainer, AsteriskSpan };
