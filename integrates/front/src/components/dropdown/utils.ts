import type { ReferenceType } from "@floating-ui/react-dom";

import type { IDropDownMap } from "./types";

const getDropDownMap = (
  parent: React.MutableRefObject<ReferenceType | null>,
): IDropDownMap => {
  if (!parent.current) {
    const positions: IDropDownMap = {
      bottom: 0,
      height: 0,
      left: 0,
      width: 0,
    };

    return positions;
  }

  const parentRect = parent.current.getBoundingClientRect();

  const positions: IDropDownMap = {
    bottom: parentRect.bottom,
    height: parentRect.height,
    left: parentRect.left,
    width: parentRect.width,
  };

  return positions;
};

export { getDropDownMap };
