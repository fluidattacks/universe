import type { IListItemProps } from "@fluidattacks/design/dist/components/list-item/types";

type TVariant = "dropdown" | "nested-list-item";

interface IDropDownOption extends IListItemProps {
  header?: string;
  permissions?: string;
  extraCondition?: boolean;
}

interface IDropdownProps {
  error?: string;
  items: IDropDownOption[];
  id?: string;
  isSelectInput?: boolean;
  open?: boolean;
  customSelectionHandler?: (selection: IDropDownOption) => void;
  initialSelection?: IDropDownOption;
  variant?: TVariant;
  handleCollapse?: (
    setCollapsed?: React.Dispatch<React.SetStateAction<boolean>>,
  ) => void;
  onKeyDown?: IDropDownOption["onKeyDown"];
  disabled?: boolean;
  children?: React.ReactNode;
  role?: IDropDownOption["role"];
  placeholder?: string;
  width?: string;
}

interface IDropDownMap {
  bottom: number;
  height: number;
  left: number;
  width: number;
}

export type { IDropDownMap, IDropdownProps, TVariant, IDropDownOption };
