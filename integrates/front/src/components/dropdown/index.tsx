import { flip, useFloating } from "@floating-ui/react-dom";
import {
  Container as ContainerComp,
  Icon,
  ListItem,
  Text,
} from "@fluidattacks/design";
import _ from "lodash";
import { Fragment, useCallback, useEffect, useRef, useState } from "react";
import { useTheme } from "styled-components";

import { Container, DropDownSelector, DropdownContainer } from "./styles";
import type { IDropDownMap, IDropDownOption, IDropdownProps } from "./types";
import { getDropDownMap } from "./utils";

import { Authorize } from "components/@core/authorize";
import { OutlineContainer, OutlineInput } from "components/input/styles";
import { useClickOutside } from "hooks/use-click-outside";

const Dropdown = ({
  children,
  customSelectionHandler,
  error,
  initialSelection,
  isSelectInput = false,
  disabled = false,
  id,
  items,
  handleCollapse,
  onKeyDown,
  open = false,
  placeholder,
  variant = "dropdown",
  width,
}: Readonly<IDropdownProps>): JSX.Element => {
  const theme = useTheme();
  const ref = useRef<HTMLDivElement>(null);
  const [collapsed, setCollapsed] = useState(open && !disabled);
  const [currentSelection, setCurrentSelection] = useState(
    items[0] ?? { value: "" },
  );
  const [currentItems, setCurrentItems] = useState(items);
  const { refs, floatingStyles, update } = useFloating({
    middleware: [flip()],
  });
  const [dropDownMap, setDropDownMap] = useState<IDropDownMap>(
    getDropDownMap(refs.reference),
  );

  const handleSelection = useCallback(
    (event: HTMLElementEvent<HTMLLIElement>, item: IDropDownOption): void => {
      setCurrentSelection(item);
      const itemOnClick = item.onClick;
      customSelectionHandler?.(item);
      setCollapsed(!collapsed);
      if (itemOnClick) {
        itemOnClick(event);
      }
    },
    [collapsed, customSelectionHandler, setCurrentSelection],
  );

  const handleClick = useCallback((): void => {
    setCollapsed(!collapsed);
    setDropDownMap(getDropDownMap(refs.reference));
    if (handleCollapse) {
      handleCollapse(setCollapsed);
    }
  }, [collapsed, refs.reference, handleCollapse]);

  const onScroll = useCallback(
    (event: Event): void => {
      if (
        refs.floating.current &&
        !_.isEqual(refs.floating.current, event.target as Node)
      ) {
        setCollapsed(false);
      }
    },
    [refs.floating],
  );

  const generateHandleOnClick = useCallback(
    (
      item: IDropDownOption,
    ): ((event: HTMLElementEvent<HTMLLIElement>) => void) =>
      (event: HTMLElementEvent<HTMLLIElement>): void => {
        handleSelection(event, item);
      },
    [handleSelection],
  );

  useClickOutside(
    ref.current,
    (): void => {
      setCollapsed(false);
    },
    true,
  );

  const isDisabled =
    disabled || (items.length === 1 && placeholder === undefined);

  useEffect((): void => {
    if (initialSelection !== undefined) {
      setCurrentSelection(initialSelection);
    }
  }, [initialSelection]);

  useEffect((): VoidFunction => {
    const handleKeydown = (event: KeyboardEvent): void => {
      const filteredItems = items.filter((item): boolean => {
        if (_.isNil(item.header) || _.isEmpty(event.key)) {
          return false;
        }

        return item.header.toLowerCase().startsWith(event.key.toLowerCase());
      });
      if (collapsed) {
        if (filteredItems.length === 0) {
          setCurrentItems(items);
        } else {
          update();
          setCurrentItems(filteredItems);
        }
      }
    };
    document.addEventListener("scroll", onScroll, true);
    window.addEventListener("keydown", handleKeydown);

    return (): void => {
      document.removeEventListener("scroll", onScroll, true);
      window.removeEventListener("keydown", handleKeydown);
      setCurrentItems(items);
    };
  }, [onScroll, collapsed, items, update]);
  const dropdown = (
    <Fragment>
      <ul className={"parent-option"} id={id} ref={refs.setReference}>
        <DropDownSelector
          $isInput={isSelectInput}
          $width={width}
          data-testid={`${id}-selected-option`}
          disabled={isDisabled}
          icon={currentSelection.icon}
          onClick={handleClick}
          onKeyDown={onKeyDown}
          placeholder={placeholder}
          value={currentSelection.value?.replace(/\n/gu, " ")}
        >
          <Text size={"sm"}>
            {currentSelection.header?.replace(/\n/gu, " ") ??
              currentSelection.value?.replace(/\n/gu, " ")}
          </Text>
          <ContainerComp display={"flex"} gap={0.5}>
            {error === undefined || !isSelectInput ? undefined : (
              <Icon
                icon={"exclamation-circle"}
                iconClass={"error-icon"}
                iconColor={theme.palette.error[500]}
                iconSize={"xs"}
              />
            )}
            <Icon
              icon={collapsed ? "chevron-up" : "chevron-down"}
              iconColor={
                disabled ? theme.palette.gray[500] : theme.palette.gray[400]
              }
              iconSize={"xs"}
            />
          </ContainerComp>
        </DropDownSelector>
      </ul>
      {collapsed ? (
        <DropdownContainer
          $parentMap={dropDownMap}
          $variant={variant}
          className={variant}
          data-testid={`${id}-${variant}-options`}
          ref={refs.setFloating}
          style={floatingStyles}
        >
          {currentItems.map((props, index): JSX.Element => {
            const key = `option-${index}`;
            const handleOnClick = generateHandleOnClick(props);
            const isSelected = currentSelection.value === props.value;

            if (
              !_.isUndefined(props.permissions) ||
              !_.isUndefined(props.extraCondition)
            ) {
              return (
                <Authorize
                  can={props.permissions}
                  extraCondition={props.extraCondition}
                  key={key}
                >
                  <ListItem
                    disabled={props.disabled}
                    href={props.href}
                    icon={props.icon}
                    onClick={handleOnClick}
                    selected={isSelected}
                    value={props.value}
                    width={width}
                  >
                    <Text size={"sm"}>{props.header ?? props.value}</Text>
                  </ListItem>
                </Authorize>
              );
            }

            return (
              <ListItem
                disabled={props.disabled}
                href={props.href}
                icon={props.icon}
                key={key}
                onClick={handleOnClick}
                selected={isSelected}
                value={props.value}
                width={width}
              >
                <Text size={"sm"}>{props.header ?? props.value}</Text>
              </ListItem>
            );
          })}
          {children}
        </DropdownContainer>
      ) : undefined}
    </Fragment>
  );

  return isSelectInput ? (
    <OutlineContainer
      $disabled={isDisabled}
      $show={error !== undefined}
      aria-disabled={isDisabled}
      data-testid={id ?? "dropdown"}
      ref={ref}
    >
      <OutlineInput>{dropdown}</OutlineInput>
    </OutlineContainer>
  ) : (
    <Container
      $error={error !== undefined}
      $open={collapsed}
      $variant={variant}
      $width={width}
      aria-disabled={isDisabled}
      data-testid={id ?? "dropdown"}
      ref={ref}
    >
      {dropdown}
    </Container>
  );
};

export { Dropdown };
