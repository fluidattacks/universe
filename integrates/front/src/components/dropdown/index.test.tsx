import { Container } from "@fluidattacks/design";
import { fireEvent, screen, waitFor } from "@testing-library/react";

import type { IDropDownOption } from "./types";

import { Dropdown } from ".";
import { CustomThemeProvider } from "components/colors";
import { render } from "mocks";

describe("dropdown", (): void => {
  const items: IDropDownOption[] = [
    { value: "radio-button-0" },
    { value: "option 1" },
    { icon: "gear", value: "option with icon" },
    { value: "option 3" },
  ];

  it("should render", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <CustomThemeProvider>
        <Dropdown disabled={false} items={items} open={true} />
      </CustomThemeProvider>,
    );

    await waitFor((): void => {
      expect(screen.getAllByText("radio-button-0")).toHaveLength(2);
    });

    expect(screen.getByText("option 1")).toBeInTheDocument();
    expect(screen.getByText("option with icon")).toBeInTheDocument();
    expect(screen.getByText("option 3")).toBeInTheDocument();
  });

  it("should open show selections, and allow select", async (): Promise<void> => {
    expect.hasAssertions();

    const onClickCallback: jest.Mock = jest.fn();
    const customSelectionHandler: jest.Mock = jest.fn();
    const optionClick: jest.Mock = jest.fn();

    const { container } = render(
      <CustomThemeProvider>
        <Dropdown
          customSelectionHandler={customSelectionHandler}
          disabled={false}
          handleCollapse={onClickCallback}
          items={[
            ...items,
            { onClick: optionClick, value: "option with click" },
          ]}
          open={false}
        />
      </CustomThemeProvider>,
    );

    const radioButton0 = container.querySelector('li[value="radio-button-0"]');

    expect(radioButton0).toBeInTheDocument();
    expect(
      container.querySelector('li[value="option 3"]'),
    ).not.toBeInTheDocument();

    // Open dropdown menu
    if (radioButton0) {
      fireEvent.click(radioButton0);
    }
    // Select
    const element = container.querySelector('li[value="option with click"]');
    if (element) {
      fireEvent.click(element);
    }

    // Dropdown closes once selected
    expect(
      container.querySelector('li[value="radio-button-0"]'),
    ).not.toBeInTheDocument();
    expect(
      container.querySelector('li[value="option with click"]'),
    ).toBeInTheDocument();

    await waitFor((): void => {
      expect(onClickCallback).toHaveBeenCalledTimes(1);
    });
    await waitFor((): void => {
      expect(customSelectionHandler).toHaveBeenCalledTimes(1);
    });
    await waitFor((): void => {
      expect(optionClick).toHaveBeenCalledTimes(1);
    });
  });

  it("close when scroll", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <CustomThemeProvider>
        <Container maxHeight={"500px"}>
          <Container minHeight={"1000px"}>
            <Dropdown disabled={false} items={items} open={true} />
          </Container>
        </Container>
      </CustomThemeProvider>,
    );
    // eslint-disable-next-line @typescript-eslint/prefer-destructuring
    const scrollableContainer = screen.getAllByRole("generic")[0];
    await waitFor((): void => {
      expect(screen.getAllByText("radio-button-0")).toHaveLength(2);
    });
    fireEvent.scroll(scrollableContainer);
    await waitFor((): void => {
      expect(screen.getAllByText("radio-button-0")).toHaveLength(1);
    });
  });

  it("close when outside click", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <CustomThemeProvider>
        <Container>
          <Dropdown disabled={false} items={items} open={true} />
        </Container>
      </CustomThemeProvider>,
    );

    // eslint-disable-next-line @typescript-eslint/prefer-destructuring
    const outsideElement = screen.getAllByRole("generic")[0];
    await waitFor((): void => {
      expect(screen.getAllByText("radio-button-0")).toHaveLength(2);
    });
    fireEvent.click(outsideElement);
    await waitFor((): void => {
      expect(screen.getAllByText("radio-button-0")).toHaveLength(1);
    });
  });
});
