import type { IconName } from "@fortawesome/free-solid-svg-icons";
import type { DefaultTheme } from "styled-components";

import type { TIconType } from "components/icon/types";

interface ISideBarButtonProps {
  collapsed?: boolean;
  disabled?: boolean;
  canDo?: string;
  icon: IconName;
  iconColor?: string;
  iconType?: TIconType;
  id: string;
  marginBottom?: keyof DefaultTheme["spacing"];
  marginTop?: keyof DefaultTheme["spacing"];
  onClick?: () => void;
  text: string;
  to?: string;
}

export type { ISideBarButtonProps };
