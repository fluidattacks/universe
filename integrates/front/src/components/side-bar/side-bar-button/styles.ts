import type { DefaultTheme } from "styled-components";
import { styled } from "styled-components";

const getBgHoverColor = (
  theme: DefaultTheme,
  disabled: boolean,
  selected: boolean,
): string => {
  if (disabled) {
    return theme.palette.white;
  }

  if (selected) {
    return theme.palette.primary[25];
  }

  return theme.palette.gray[100];
};

const getTextHoverColor = (
  theme: DefaultTheme,
  disabled: boolean,
  selected: boolean,
): string => {
  if (disabled) {
    return theme.palette.gray[200];
  }

  if (selected) {
    return theme.palette.gray[800];
  }

  return theme.palette.gray[500];
};

const SideBarButtonWrapper = styled.div<{
  $disabled: boolean;
  $selected: boolean;
}>`
  cursor: ${({ $disabled }): string => ($disabled ? "not-allowed" : "pointer")};

  div > div {
    margin-left: 13px;
    margin-right: 13px;
  }

  &:hover > div > div {
    background-color: ${({ theme, $disabled, $selected }): string =>
      getBgHoverColor(theme, $disabled, $selected)};
  }

  &:hover {
    p {
      color: ${({ theme, $disabled, $selected }): string =>
        getTextHoverColor(theme, $disabled, $selected)};
    }
  }
`;

const LinkWrapper = styled.div`
  a {
    text-decoration: none;
  }
`;

export { LinkWrapper, SideBarButtonWrapper };
