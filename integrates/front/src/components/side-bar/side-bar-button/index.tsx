import { Container, Icon, Text } from "@fluidattacks/design";
import { isUndefined } from "lodash";
import { useEffect, useState } from "react";
import { NavLink, useLocation } from "react-router-dom";
import { useTheme } from "styled-components";

import { LinkWrapper, SideBarButtonWrapper } from "./styles";
import type { ISideBarButtonProps } from "./types";
import { getBgColor, getIconColor, getIconType, getTextWeight } from "./utils";

import { Can } from "context/authz/can";

const SideBarButton = ({
  collapsed = false,
  disabled = false,
  canDo,
  icon,
  iconColor,
  iconType,
  id,
  marginBottom,
  marginTop,
  onClick,
  text,
  to,
}: Readonly<ISideBarButtonProps>): JSX.Element => {
  const theme = useTheme();
  const [isSelected, setIsSelected] = useState(false);
  const location = useLocation();

  useEffect((): void => {
    const isActive = isUndefined(to) ? false : location.pathname.startsWith(to);
    setIsSelected(isActive);
  }, [location.pathname, to]);

  const button = (
    <SideBarButtonWrapper
      $disabled={disabled}
      $selected={isSelected}
      onClick={disabled ? undefined : onClick}
    >
      <Container
        alignItems={"center"}
        bgColor={theme.palette.white}
        borderLeft={
          isSelected
            ? `4px solid ${getBgColor(theme, "500", disabled, isSelected)}`
            : "4px solid transparent"
        }
        display={"flex"}
        height={"30px"}
        id={id}
        mb={marginBottom}
        mt={marginTop}
        width={"100%"}
      >
        <Container
          alignItems={"center"}
          bgColor={getBgColor(theme, "25", disabled, isSelected)}
          borderRadius={"4px"}
          display={"flex"}
          height={"100%"}
          justify={"center"}
          maxWidth={"30px"}
          minWidth={"30px"}
        >
          <Icon
            clickable={false}
            icon={icon}
            iconColor={iconColor ?? getIconColor(theme, disabled, isSelected)}
            iconSize={"xxs"}
            iconType={iconType ?? getIconType(disabled, isSelected)}
          />
        </Container>
        {collapsed ? undefined : (
          <Text
            color={disabled ? theme.palette.gray[200] : theme.palette.gray[800]}
            display={"inline"}
            fontWeight={getTextWeight(disabled, isSelected)}
            mr={1}
            size={"sm"}
          >
            {text}
          </Text>
        )}
      </Container>
    </SideBarButtonWrapper>
  );

  if (!isUndefined(to) && !isUndefined(canDo)) {
    return (
      <LinkWrapper>
        <Can do={canDo}>
          <NavLink to={to}>{button}</NavLink>
        </Can>
      </LinkWrapper>
    );
  }

  if (!isUndefined(to)) {
    return (
      <LinkWrapper>
        <NavLink to={to}>{button}</NavLink>
      </LinkWrapper>
    );
  }

  return button;
};

export { SideBarButton };
