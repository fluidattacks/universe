import type { ReactElement } from "react";
import { Children, cloneElement, useMemo } from "react";
import * as React from "react";

import { Step } from "./step";
import { ResponsiveContainer } from "./styles";
import type { IProgressIndicator, IStepProps } from "./types";

const MIN_WIDTH = 150;
const ProgressIndicator = ({
  children,
  orientation = "horizontal",
  minWidth = MIN_WIDTH,
}: Readonly<IProgressIndicator>): JSX.Element => {
  const childrenArray = React.Children.toArray(
    children,
  ) as ReactElement<IStepProps>[];

  const numberOfSteps = useMemo(
    (): number => Children.count(children),
    [children],
  );
  const totalWidth = useMemo(
    (): number => numberOfSteps * minWidth,
    [numberOfSteps, minWidth],
  );

  return (
    <ResponsiveContainer
      $minWidth={`${totalWidth}px`}
      $orientation={orientation}
    >
      {childrenArray.map((child): ReactElement<IStepProps> => {
        return cloneElement(child, { minWidth, orientation });
      })}
    </ResponsiveContainer>
  );
};

export { ProgressIndicator, Step };
