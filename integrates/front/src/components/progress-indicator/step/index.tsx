import { Container, ProgressBar, Text } from "@fluidattacks/design";
import type { IconName } from "@fortawesome/free-solid-svg-icons";
import { useTheme } from "styled-components";

import { InProgressIcon } from "../in-progress-icon";
import { StyledIcon, colors } from "../styles";
import type { IStepProps, TStepState } from "../types";

const MIN_WIDTH = 150;
const Step = ({
  label,
  minWidth = MIN_WIDTH,
  state = "not completed",
  stepNumber = 1,
  title,
  onClick,
  orientation = "horizontal",
}: Readonly<IStepProps>): JSX.Element => {
  const theme = useTheme();
  const iconsName: Record<TStepState, IconName> = {
    completed: "circle-check",
    disabled: "circle-dashed",
    error: "circle-exclamation",
    "in progress": "circle-half",
    "not completed": "circle-dashed",
  };

  const isDisabled = state === "disabled";
  const COMPLETED_PERCENTAGE = 100;

  return (
    <Container
      display={"inline-flex"}
      flexDirection={orientation === "horizontal" ? "column" : "row"}
      minWidth={`${minWidth}px`}
    >
      <ProgressBar
        minWidth={minWidth}
        orientation={orientation}
        percentage={
          isDisabled || state === "not completed" ? 0 : COMPLETED_PERCENTAGE
        }
        rounded={false}
        variant={
          state === "error" ? "progressIndicatorError" : "progressIndicator"
        }
      />
      <Container
        display={"flex"}
        gap={0.5}
        pb={0.5}
        pl={0.5}
        pr={0.5}
        pt={0.5}
        width={"100%"}
      >
        {state === "in progress" ? (
          <InProgressIcon onClick={onClick} />
        ) : (
          <StyledIcon
            $variant={state}
            clickable={!isDisabled}
            disabled={isDisabled}
            icon={iconsName[state]}
            iconColor={colors(state, theme)}
            iconSize={"xs"}
            iconType={"fa-light"}
            onClick={isDisabled ? undefined : onClick}
          />
        )}
        <Container>
          <Text
            color={
              isDisabled ? theme.palette.gray[300] : theme.palette.gray[800]
            }
            fontWeight={"bold"}
            size={"sm"}
          >
            {`${stepNumber}.${title}`}
          </Text>
          {label === undefined ? null : (
            <Text
              color={
                isDisabled ? theme.palette.gray[300] : theme.palette.gray[600]
              }
              size={"xs"}
            >
              {label}
            </Text>
          )}
        </Container>
      </Container>
    </Container>
  );
};

export { Step };
