import { fireEvent, screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";

import { ProgressIndicator, Step } from ".";
import { render } from "mocks";

describe("progress indicator components", (): void => {
  it("should render a steps", (): void => {
    expect.hasAssertions();

    const clickCallback: jest.Mock = jest.fn();
    const { container } = render(
      <ProgressIndicator>
        <Step
          label={"test completed state"}
          onClick={clickCallback}
          state={"completed"}
          stepNumber={1}
          title={"Test Completed state"}
        />
        <Step
          label={"test error state"}
          onClick={clickCallback}
          state={"error"}
          stepNumber={2}
          title={"Test Error state"}
        />
        <Step
          label={"test not completed state"}
          onClick={clickCallback}
          state={"not completed"}
          stepNumber={3}
          title={"Test not Completed state"}
        />
        <Step
          label={"test disabled state"}
          onClick={clickCallback}
          state={"not completed"}
          stepNumber={4}
          title={"Test disabled state"}
        />
      </ProgressIndicator>,
    );

    expect(screen.getByText("1.Test Completed state")).toBeInTheDocument();
    expect(screen.getByText("2.Test Error state")).toBeInTheDocument();
    expect(screen.getByText("3.Test not Completed state")).toBeInTheDocument();
    expect(screen.getByText("4.Test disabled state")).toBeInTheDocument();
    expect(container.querySelector(".fa-circle-check")).toBeInTheDocument();
    expect(
      container.querySelector(".fa-circle-exclamation"),
    ).toBeInTheDocument();
    expect(container.querySelectorAll(".fa-circle-dashed")).toHaveLength(2);
  });

  it("should be clickable", async (): Promise<void> => {
    expect.hasAssertions();

    const clickCallback: jest.Mock = jest.fn();
    const { container } = render(
      <ProgressIndicator>
        <Step
          label={"test completed state"}
          onClick={clickCallback}
          state={"completed"}
          stepNumber={1}
          title={"Test Completed state"}
        />
        <Step
          label={"test error state"}
          onClick={clickCallback}
          state={"error"}
          stepNumber={2}
          title={"Test Error state"}
        />
        <Step
          label={"test in progress state"}
          onClick={clickCallback}
          state={"in progress"}
          stepNumber={3}
          title={"Test in progress state"}
        />
      </ProgressIndicator>,
    );

    const completedStep = container.querySelector(".fa-circle-check");
    const errorStep = container.querySelector(".fa-circle-exclamation");
    const inProgressStep = container.querySelector(".in-progress-icon");

    if (completedStep) await userEvent.click(completedStep);
    if (errorStep) await userEvent.click(errorStep);
    if (inProgressStep) await userEvent.click(inProgressStep);
    await waitFor((): void => {
      expect(clickCallback).toHaveBeenCalledTimes(3);
    });
  });

  it("should not be clickable", (): void => {
    expect.hasAssertions();

    const clickCallback: jest.Mock = jest.fn();
    const { container } = render(
      <ProgressIndicator>
        <Step
          label={"test disabled state"}
          onClick={clickCallback}
          state={"disabled"}
          stepNumber={1}
          title={"Test Disabled state"}
        />
      </ProgressIndicator>,
    );

    expect(screen.queryByText("test disabled state")).toBeInTheDocument();

    const iconElement = container.querySelector(".fa-circle-dashed");

    expect(iconElement).toBeInTheDocument();

    if (iconElement) {
      fireEvent.click(iconElement);
    }

    expect(clickCallback).not.toHaveBeenCalled();
  });
});
