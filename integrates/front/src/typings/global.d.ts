type HTMLElementEvent<T extends HTMLElement> = Event &
  React.MouseEvent<T> & { target: T };
