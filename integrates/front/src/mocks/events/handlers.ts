import { GraphQLError } from "graphql";
import { HttpResponse, type StrictResponse, graphql } from "msw";

import { GET_ORG_EVENTS } from "features/event-bar/queries";
import type {
  DownloadEventFileMutationMutation as DownloadEventFile,
  GetEventConsultingQuery as GetEventConsulting,
  GetEventDescriptionQuery as GetEventDescription,
  GetEventEvidencesQuery as GetEventEvidences,
  GetEventHeaderQuery as GetEventHeader,
  GetOrganizationEventsQuery as GetOrganizationEvents,
  RejectEventSolutionMutationMutation as RejectEventSolution,
  UpdateEventMutationMutation as UpdateEvent,
} from "gql/graphql";
import { EventType, SolveEventReason } from "gql/graphql";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";
import { GET_EVENT_CONSULTING } from "pages/event/consulting/queries";
import {
  GET_EVENT_DESCRIPTION,
  REJECT_EVENT_SOLUTION_MUTATION,
  UPDATE_EVENT_MUTATION,
} from "pages/event/description/queries";
import {
  DOWNLOAD_FILE_MUTATION,
  GET_EVENT_EVIDENCES,
} from "pages/event/evidence/queries";
import { GET_EVENT_HEADER } from "pages/event/queries";

const graphqlMocked = graphql.link(LINK);
export const eventHandlers = [
  graphqlMocked.query(
    GET_EVENT_HEADER,
    ({
      variables,
    }): StrictResponse<IErrorMessage | { data: GetEventHeader }> => {
      const { eventId, groupName } = variables;

      if (eventId === "413372600" && groupName === "TEST") {
        return HttpResponse.json({
          data: {
            event: {
              __typename: "Event",
              eventDate: "2019-12-09 12:00",
              eventStatus: "SOLVED",
              eventType: EventType.Other,
              id: "413372600",
            },
          },
        });
      }

      return HttpResponse.json({ errors: [new GraphQLError("Event error")] });
    },
  ),
  graphqlMocked.query(
    GET_EVENT_DESCRIPTION,
    ({
      variables,
    }): StrictResponse<IErrorMessage | { data: GetEventDescription }> => {
      const { eventId, groupName } = variables;

      if (eventId === "413372600" && groupName === "TEST") {
        return HttpResponse.json({
          data: {
            event: {
              __typename: "Event",
              affectedReattacks: [],
              closingDate: "",
              detail: "Something happened",
              environment: null,
              eventStatus: "CREATED",
              eventType: EventType.Other,
              hacker: "unittest@fluidattacks.com",
              id: "413372600",
              otherSolvingReason: "",
              root: {
                nickname: "integrates_1",
              },
              solvingReason: null,
            },
          },
        });
      }
      if (eventId === "413372601" && groupName === "TEST") {
        return HttpResponse.json({
          data: {
            event: {
              __typename: "Event",
              affectedReattacks: [],
              closingDate: "2022-08-09 13:37:00",
              detail: "Something happened",
              environment: null,
              eventStatus: "SOLVED",
              eventType: EventType.AuthorizationSpecialAttack,
              hacker: "unittest@fluidattacks.com",
              id: "413372601",
              otherSolvingReason: "Reason test",
              root: null,
              solvingReason: SolveEventReason.Other,
            },
          },
        });
      }
      if (eventId === "413372602" && groupName === "TEST") {
        return HttpResponse.json({
          data: {
            event: {
              __typename: "Event",
              affectedReattacks: [],
              closingDate: "2022-08-09 13:37:00",
              detail: "Something happened",
              environment: null,
              eventStatus: "VERIFICATION_REQUESTED",
              eventType: EventType.AuthorizationSpecialAttack,
              hacker: "unittest@fluidattacks.com",
              id: "413372602",
              otherSolvingReason: null,
              root: {
                nickname: "integrates_2",
              },
              solvingReason: null,
            },
          },
        });
      }

      return HttpResponse.json({ errors: [new GraphQLError("")] });
    },
  ),
  graphqlMocked.query(
    GET_ORG_EVENTS,
    ({
      variables,
    }): StrictResponse<IErrorMessage | { data: GetOrganizationEvents }> => {
      const { organizationName } = variables;

      if (["okada", "stored", "test"].includes(organizationName)) {
        return HttpResponse.json({
          data: {
            organizationId: {
              __typename: "Organization",
              groups: [
                {
                  __typename: "Group",
                  events: [],
                  name: "TEST",
                },
              ],
              name: organizationName,
            },
          },
        });
      }

      return HttpResponse.json({ errors: [new GraphQLError("")] });
    },
  ),
  graphqlMocked.query(
    GET_EVENT_CONSULTING,
    ({
      variables,
    }): StrictResponse<IErrorMessage | { data: GetEventConsulting }> => {
      const { eventId, groupName } = variables;

      if (eventId === "413372600" && groupName === "TEST") {
        return HttpResponse.json({
          data: {
            event: {
              __typename: "Event",
              consulting: [
                {
                  content: "Hello world",
                  created: "2019/12/04 08:13:53",
                  email: "unittest@fluidattacks.com",
                  fullName: "Test User",
                  id: "1337260012345",
                  modified: "2019/12/04 08:13:53",
                  parentComment: "0",
                },
                {
                  content: "Second world",
                  created: "2019/12/05 08:13:53",
                  email: "unittest@fluidattacks.com",
                  fullName: "User Test",
                  id: "1337260012349",
                  modified: "2019/12/04 08:13:53",
                  parentComment: "0",
                },
              ],
              id: "413372600",
            },
          },
        });
      }
      if (eventId === "413372602" && groupName === "TEST") {
        return HttpResponse.json({
          data: {
            event: {
              __typename: "Event",
              consulting: [],
              id: "413372602",
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Event consult error")],
      });
    },
  ),
  graphqlMocked.mutation(
    UPDATE_EVENT_MUTATION,
    (): StrictResponse<{ data: UpdateEvent }> => {
      return HttpResponse.json({
        data: {
          updateEvent: {
            success: true,
          },
        },
      });
    },
  ),
  graphqlMocked.mutation(
    REJECT_EVENT_SOLUTION_MUTATION,
    (): StrictResponse<{ data: RejectEventSolution }> => {
      return HttpResponse.json({
        data: {
          rejectEventSolution: {
            success: true,
          },
        },
      });
    },
  ),
  graphqlMocked.query(
    GET_EVENT_EVIDENCES,
    ({
      variables,
    }): StrictResponse<IErrorMessage | { data: GetEventEvidences }> => {
      const { eventId, groupName } = variables;

      if (eventId === "413372600" && groupName === "TEST") {
        return HttpResponse.json({
          data: {
            event: {
              __typename: "Event",
              eventStatus: "CREATED",
              evidences: {
                file1: null,
                image1: null,
                image2: null,
                image3: null,
                image4: null,
                image5: null,
                image6: null,
              },
              id: "413372600",
            },
          },
        });
      }
      if (eventId === "413372601" && groupName === "TEST") {
        return HttpResponse.json({
          data: {
            event: {
              __typename: "Event",
              eventStatus: "SOLVED",
              evidences: {
                file1: null,
                image1: null,
                image2: null,
                image3: null,
                image4: null,
                image5: null,
                image6: null,
              },
              id: "413372601",
            },
          },
        });
      }
      if (eventId === "413372602" && groupName === "TEST") {
        return HttpResponse.json({
          data: {
            event: {
              __typename: "Event",
              eventStatus: "CREATED",
              evidences: {
                file1: {
                  date: "2020-10-11 00:00:00",
                  fileName: "some_file.pdf",
                },
                image1: {
                  date: "2020-10-12 00:00:00",
                  fileName: "some_image.png",
                },
                image2: null,
                image3: null,
                image4: null,
                image5: null,
                image6: null,
              },
              id: "413372602",
            },
          },
        });
      }
      if (eventId === "413372603" && groupName === "TEST") {
        return HttpResponse.json({
          data: {
            event: {
              __typename: "Event",
              eventStatus: "CLOSED",
              evidences: {
                file1: {
                  date: "020-10-17 00:00:00",
                  fileName: "some_file.pdf",
                },
                image1: {
                  date: "2020-10-12 00:00:00",
                  fileName: "some_image.png",
                },
                image2: null,
                image3: null,
                image4: null,
                image5: null,
                image6: null,
              },
              id: "413372603",
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Event evidences error")],
      });
    },
  ),
  graphqlMocked.mutation(
    DOWNLOAD_FILE_MUTATION,
    (): StrictResponse<{ data: DownloadEventFile }> => {
      return HttpResponse.json({
        data: {
          downloadEventFile: {
            success: true,
            url: "https://localhost:9000/some_file.pdf",
          },
        },
      });
    },
  ),
];
