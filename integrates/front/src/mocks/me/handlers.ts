import dayjs from "dayjs";
import { HttpResponse, type StrictResponse, graphql } from "msw";

import { GET_USER_ROLE } from "features/user-role/queries";
import {
  GET_STAKEHOLDER_PHONE,
  VERIFY_STAKEHOLDER_MUTATION,
} from "features/verify-dialog/queries";
import type {
  AddAuditEventMutation,
  GetCurrentUserAtHomeQuery as GetCurrentUser,
  GetGroupInfoQuery,
  GetMeVulnerabilitiesAssignedQuery as GetMeVulnerabilitiesAssigned,
  GetMeVulnerabilitiesAssignedIdsQuery as GetMeVulnerabilitiesAssignedIds,
  GetNotificationsQuery,
  GetStakeholderGroupsQuery,
  GetStakeholderPhoneQueryQuery as GetStakeholderPhone,
  GetStakeholderTrialQuery as GetStakeholderTrial,
  GetUserInvitationAtHomeQuery,
  GetUserOrganizationsAtTopBarQuery,
  GetUserOrganizationsGroupsAtDashboardQuery as GetUserOrganizationsGroups,
  GetUserOrganizationsQuery,
  GetUserRememberQuery as GetUserRemember,
  GetUserRoleQuery as GetUserRole,
  VerifyStakeholderMutationAtVerifyDialogMutation as VerifyStakeholder,
} from "gql/graphql";
import {
  ADD_AUDIT_EVENT,
  GET_STAKEHOLDER_TRIAL,
  GET_USER_ORGANIZATIONS,
} from "hooks/queries";
import { LINK } from "mocks/constants";
import { GET_STAKEHOLDER_GROUPS } from "pages/auto-enrollment/queries";
import { GET_USER_REMEMBER } from "pages/dashboard/legal-notice/queries";
import { GET_NOTIFICATIONS } from "pages/dashboard/navbar/downloads/queries";
import {
  GET_GROUP_INFO,
  GET_ME_VULNERABILITIES_ASSIGNED_IDS,
  GET_USER_ORGANIZATIONS as GET_USER_ORGANIZATIONS_TOP_BAR,
} from "pages/dashboard/navbar/queries";
import { GET_CURRENT_USER, GET_USER_INVITATION } from "pages/home/queries";
import { GET_ME_VULNERABILITIES_ASSIGNED } from "pages/to-do/assigned-vulnerabilities/queries";
import { GET_USER_ORGANIZATIONS_GROUPS } from "pages/to-do/queries";

const graphqlMocked = graphql.link(LINK);
export const meHandlers = [
  graphqlMocked.mutation(
    ADD_AUDIT_EVENT,
    (): StrictResponse<{ data: AddAuditEventMutation }> => {
      return HttpResponse.json({ data: { addAuditEvent: { success: true } } });
    },
  ),
  graphqlMocked.mutation(
    VERIFY_STAKEHOLDER_MUTATION,
    (): StrictResponse<{ data: VerifyStakeholder }> => {
      return HttpResponse.json({
        data: {
          verifyStakeholder: {
            success: true,
          },
        },
      });
    },
  ),
  graphqlMocked.query(
    GET_STAKEHOLDER_PHONE,
    (): StrictResponse<{ data: GetStakeholderPhone }> => {
      return HttpResponse.json({
        data: {
          me: {
            __typename: "Me",
            phone: {
              callingCountryCode: "1",
              countryCode: "US",
              nationalNumber: "1234545",
            },
            userEmail: "test@fluidattacks.com",
          },
        },
      });
    },
  ),
  graphqlMocked.query(
    GET_USER_ORGANIZATIONS_GROUPS,
    (): StrictResponse<{ data: GetUserOrganizationsGroups }> => {
      return HttpResponse.json({
        data: {
          me: {
            __typename: "Me",
            organizations: [
              {
                __typename: "Organization",
                groups: [
                  {
                    __typename: "Group",
                    name: "TEST",
                    permissions: [],
                    serviceAttributes: [],
                  },
                  {
                    __typename: "Group",
                    name: "unittesting",
                    permissions: [],
                    serviceAttributes: ["has_advanced", "is_continuous"],
                  },
                ],
                name: "okada",
              },
            ],
            userEmail: "",
          },
        },
      });
    },
  ),
  graphqlMocked.query(
    GET_USER_ORGANIZATIONS,
    (): StrictResponse<{ data: GetUserOrganizationsQuery }> => {
      return HttpResponse.json({
        data: {
          me: {
            __typename: "Me",
            organizations: [
              { __typename: "Organization", name: "okada" },
              { __typename: "Organization", name: "test" },
              { __typename: "Organization", name: "test2" },
              { __typename: "Organization", name: "anotherorg" },
              { __typename: "Organization", name: "bulat" },
              { __typename: "Organization", name: "secondorg" },
            ],
            userEmail: "test@test.test",
          },
        },
      });
    },
  ),
  graphqlMocked.query(
    GET_USER_ORGANIZATIONS_TOP_BAR,
    (): StrictResponse<{ data: GetUserOrganizationsAtTopBarQuery }> => {
      return HttpResponse.json({
        data: {
          me: {
            __typename: "Me",
            organizations: [
              { __typename: "Organization", name: "okada" },
              { __typename: "Organization", name: "test" },
              { __typename: "Organization", name: "test2" },
              { __typename: "Organization", name: "anotherorg" },
              { __typename: "Organization", name: "bulat" },
              { __typename: "Organization", name: "secondorg" },
            ],
            userEmail: "test@test.test",
            userName: "test",
          },
        },
      });
    },
  ),
  graphqlMocked.query(
    GET_ME_VULNERABILITIES_ASSIGNED_IDS,
    (): StrictResponse<{ data: GetMeVulnerabilitiesAssignedIds }> => {
      return HttpResponse.json({
        data: {
          me: {
            __typename: "Me",
            userEmail: "test@test.test",
            vulnerabilitiesAssigned: [],
          },
        },
      });
    },
  ),
  graphqlMocked.query(
    GET_GROUP_INFO,
    (): StrictResponse<{
      data: GetGroupInfoQuery;
    }> => {
      return HttpResponse.json({
        data: {
          group: {
            __typename: "Group",
            name: "unittesting",
            organization: "okada",
          },
        },
      });
    },
  ),
  graphqlMocked.query(
    GET_USER_REMEMBER,
    (): StrictResponse<{ data: GetUserRemember }> => {
      return HttpResponse.json({
        data: {
          me: {
            __typename: "Me",
            remember: true,
            userEmail: "test@test.rest",
          },
        },
      });
    },
  ),
  graphqlMocked.query(
    GET_STAKEHOLDER_TRIAL,
    (): StrictResponse<{ data: GetStakeholderTrial }> => {
      return HttpResponse.json({
        data: {
          me: {
            __typename: "Me",
            trial: null,
            userEmail: "test@test.test",
          },
        },
      });
    },
  ),
  graphqlMocked.query(
    GET_USER_ROLE,
    ({ variables }): StrictResponse<{ data: GetUserRole }> => {
      const orgs = ["okada", "test", "bulat"];
      const groups = ["unittesting", "test", "testgroup"];
      const {
        groupLevel,
        groupName,
        organizationLevel,
        organizationName,
        userLevel,
      } = variables;
      if (
        groupLevel &&
        groups.includes(groupName.toLowerCase()) &&
        !organizationLevel &&
        !userLevel
      ) {
        return HttpResponse.json({
          data: {
            group: {
              __typename: "Group",
              name: groupName,
              userRole: "user",
            },
          },
        });
      }
      if (
        !groupLevel &&
        organizationLevel &&
        orgs.includes(organizationName) &&
        !userLevel
      ) {
        return HttpResponse.json({
          data: {
            organizationId: {
              __typename: "Organization",
              name: organizationName,
              userRole: "user",
            },
          },
        });
      }

      return HttpResponse.json({
        data: {
          me: {
            __typename: "Me",
            role: "user",
            userEmail: "test@test.test",
          },
        },
      });
    },
  ),
  graphqlMocked.query(
    GET_ME_VULNERABILITIES_ASSIGNED,
    (): StrictResponse<{ data: GetMeVulnerabilitiesAssigned }> => {
      return HttpResponse.json({
        data: {
          me: {
            __typename: "Me",
            userEmail: "test@test.test",
            vulnerabilitiesAssigned: [],
          },
        },
      });
    },
  ),
  graphqlMocked.query(
    GET_USER_INVITATION,
    (): StrictResponse<{ data: GetUserInvitationAtHomeQuery }> => {
      return HttpResponse.json({
        data: {
          me: {
            __typename: "Me",
            hasAllInvitationsPending: false,
            userEmail: "test@fluidattacks.com",
            userName: "John Doe",
          },
        },
      });
    },
  ),
  graphqlMocked.query(
    GET_CURRENT_USER,
    (): StrictResponse<{ data: GetCurrentUser }> => {
      return HttpResponse.json({
        data: {
          me: {
            __typename: "Me",
            awsSubscription: null,
            enrolled: true,
            permissions: [],
            phone: null,
            sessionExpiration: dayjs().add(1, "day").toISOString(),
            tours: {
              newGroup: true,
              newRoot: true,
              welcome: true,
            },
            trial: {
              completed: true,
              groupRole: null,
            },
            userEmail: "test@fluidattacks.com",
            userName: "John Doe",
          },
        },
      });
    },
  ),
  graphqlMocked.query(
    GET_STAKEHOLDER_GROUPS,
    (): StrictResponse<{ data: GetStakeholderGroupsQuery }> => {
      return HttpResponse.json({
        data: {
          me: {
            __typename: "Me",
            isPersonalEmail: false,
            organizations: [],
            trial: null,
            userEmail: "test@fluidattacks.com",
            userName: "John Doe",
          },
        },
      });
    },
  ),
  graphqlMocked.query(
    GET_NOTIFICATIONS,
    (): StrictResponse<{ data: GetNotificationsQuery }> => {
      return HttpResponse.json({
        data: {
          me: {
            __typename: "Me",
            notifications: [],
            userEmail: "test@test.test",
          },
        },
      });
    },
  ),
];
