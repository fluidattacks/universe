/* eslint-disable max-lines */
import { GraphQLError } from "graphql";
import _ from "lodash";
import { HttpResponse, type StrictResponse, graphql } from "msw";

import { GET_ORGANIZATION_CREDENTIALS as GET_CREDENTIALS_FROM_OAUTH_FORM } from "features/add-oauth-root-form/queries";
import { GET_ORGANIZATION_BILLING as GET_ORGANIZATION_BILLING_FROM_USER } from "features/add-user-modal/confirm-invitation/queries";
import { GET_ORG_EVENTS } from "features/event-bar/queries";
import { GET_ORGANIZATION_POLICIES as GET_ORGANIZATION_POLICIES_DESCRIPTION } from "features/vulnerabilities/treatment-modal/update-treatment/queries";
import type {
  AddCredentialsMutationMutation,
  GeOrganizationGroupsQuery,
  GetFindingsPoliciesQuery,
  GetNGroupsQuery,
  GetOrgEventsQuery,
  GetOrganizationBillingAtBillingViewQuery,
  GetOrganizationBillingAtInvitationQuery,
  GetOrganizationBillingBydateQuery,
  GetOrganizationComplianceQuery as GetOrganizationCompliance,
  GetOrgCredentialsQuery as GetOrganizationCredentials,
  GetOrganizationDataQuery as GetOrganizationData,
  GetOrganizationEventsQuery as GetOrganizationEvents,
  GetOrganizationGroupNamesQuery,
  GetOrganizationGroupsQuery,
  GetOrganizationIdQuery as GetOrganizationId,
  GetOrganizationIdDataQuery as GetOrganizationIdData,
  GetOrganizationIntegrationRepositoriesQuery,
  GetOrganizationPoliciesAtOrganizationQuery as GetOrganizationPolicies,
  GetOrganizationPoliciesAtUpdateDescQuery as GetOrganizationPoliciesDescription,
  GetOrganizationRootsQuery,
  GetOrganizationStakeholdersQuery,
  GrantStakeholderOrganizationAccessMutationMutation,
  RemoveCredentialsMutationMutation as RemoveCredentials,
  RemoveStakeholderOrganizationAccessMutationMutation,
  UpdateCredentialsMutationMutation,
  UpdateOrganizationPoliciesMutation as UpdateOrganizationPolicies,
  UpdateOrganizationPriorityPoliciesMutation as UpdateOrganizationPriorityPolicies,
  UpdateOrganizationStakeholderMutationMutation,
} from "gql/graphql";
import {
  InvitationState,
  ManagedType,
  OrganizationFindingPolicyStatus,
  ServiceType,
  TierType,
  TreatmentAcceptancePolicy,
} from "gql/graphql";
import { LINK } from "mocks/constants";
import type { IErrorMessage, IMessage } from "mocks/types";
import { GET_ORGANIZATION_ID_DATA } from "pages/dashboard/sidebar/queries";
import { GET_ORGANIZATION_GROUP_NAMES } from "pages/group/group-selector/queries";
import { GET_N_GROUPS } from "pages/group/members/queries";
import {
  GET_ORGANIZATION_BILLING_BY_DATE,
  GET_ORGANIZATION_BILLING as GET_ORGANIZATION_BILLING_FROM_BILLING,
} from "pages/organization/billing/queries";
import { GET_ORGANIZATION_COMPLIANCE } from "pages/organization/compliance/queries";
import {
  ADD_CREDENTIALS,
  GET_ORGANIZATION_CREDENTIALS,
  REMOVE_CREDENTIALS,
  UPDATE_CREDENTIALS,
} from "pages/organization/credentials/queries";
import {
  GET_ORGANIZATION_EVENTS,
  GET_ORGANIZATION_GROUPS,
  GET_ORGANIZATION_ROOTS,
} from "pages/organization/groups/queries";
import {
  ADD_STAKEHOLDER_MUTATION,
  GET_ORGANIZATION_STAKEHOLDERS,
  REMOVE_STAKEHOLDER_MUTATION,
  UPDATE_STAKEHOLDER_MUTATION,
} from "pages/organization/members/queries";
import {
  GET_ORGANIZATION_GROUPS as GET_ORGANIZATION_GROUPS_FROM_OUTSIDE,
  GET_ORGANIZATION_INTEGRATION_REPOSITORIES,
} from "pages/organization/outside/queries";
import {
  GET_FINDINGS_POLICIES,
  GET_ORGANIZATION_POLICIES,
  UPDATE_ORGANIZATION_POLICIES,
  UPDATE_ORGANIZATION_PRIORITY_POLICIES,
} from "pages/organization/policies/queries";
import {
  GET_ORGANIZATION_DATA,
  GET_ORGANIZATION_ID,
} from "pages/organization/queries";

const graphqlMocked = graphql.link(LINK);
export const organizationHandlers = [
  graphqlMocked.query(
    GET_ORGANIZATION_STAKEHOLDERS,
    ({
      variables,
    }): StrictResponse<
      IErrorMessage | { data: GetOrganizationStakeholdersQuery }
    > => {
      const { organizationId } = variables;

      if (organizationId === "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3") {
        return HttpResponse.json({
          data: {
            organization: {
              __typename: "Organization",
              name: "okada",
              stakeholders: [
                {
                  __typename: "Stakeholder",
                  email: "testuser1@gmail.com",
                  firstLogin: "2020-06-01",
                  invitationState: InvitationState.Registered,
                  lastLogin: "2020-09-01",
                  role: "customer_manager",
                },
                {
                  __typename: "Stakeholder",
                  email: "testuser2@gmail.com",
                  firstLogin: "2020-08-01",
                  invitationState: InvitationState.Registered,
                  lastLogin: "-",
                  role: "organization_manager",
                },
              ],
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [
          new GraphQLError("An error occurred fetching organization users"),
        ],
      });
    },
  ),
  graphqlMocked.mutation(
    UPDATE_STAKEHOLDER_MUTATION,
    ({
      variables,
    }): StrictResponse<
      | Record<"errors", IMessage[]>
      | { data: UpdateOrganizationStakeholderMutationMutation }
    > => {
      const { email } = variables;
      if (email === "testuser1@gmail.com") {
        return HttpResponse.json({
          errors: [
            new GraphQLError("Exception - Email is not valid"),
            new GraphQLError("Exception - Invalid field in form"),
            new GraphQLError("Exception - Invalid characters"),
            new GraphQLError("Exception - Invalid email address in form"),
            new GraphQLError("Exception - Stakeholder error"),
          ],
        });
      }

      return HttpResponse.json({
        data: {
          updateOrganizationStakeholder: {
            modifiedStakeholder: {
              email,
            },
            success: true,
          },
        },
      });
    },
  ),
  graphqlMocked.mutation(
    REMOVE_STAKEHOLDER_MUTATION,
    (): StrictResponse<{
      data: RemoveStakeholderOrganizationAccessMutationMutation;
    }> => {
      return HttpResponse.json({
        data: {
          removeStakeholderOrganizationAccess: {
            success: true,
          },
        },
      });
    },
  ),
  graphqlMocked.mutation(
    ADD_STAKEHOLDER_MUTATION,
    ({
      variables,
    }): StrictResponse<{
      data: GrantStakeholderOrganizationAccessMutationMutation;
    }> => {
      const { email } = variables;

      return HttpResponse.json({
        data: {
          grantStakeholderOrganizationAccess: {
            grantedStakeholder: {
              email,
            },
            success: true,
          },
        },
      });
    },
  ),
  graphqlMocked.query(
    GET_ORGANIZATION_BILLING_FROM_USER,
    ({
      variables,
    }): StrictResponse<
      IErrorMessage | { data: GetOrganizationBillingAtInvitationQuery }
    > => {
      const { organizationId } = variables;

      if (organizationId === "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3") {
        return HttpResponse.json({
          data: {
            organization: {
              __typename: "Organization",
              billing: { authors: [{ actor: "Test <test@gmail.com>" }] },
              name: "okada",
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Organization error")],
      });
    },
  ),
  graphqlMocked.query(
    GET_ORGANIZATION_GROUPS,
    ({
      variables,
    }): StrictResponse<
      IErrorMessage | { data: GetOrganizationGroupsQuery }
    > => {
      const { organizationId } = variables;

      if (organizationId === "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3") {
        return HttpResponse.json({
          data: {
            organization: {
              coveredAuthors: 2,
              coveredRepositories: 1,
              groups: [
                {
                  closedVulnerabilities: 0,
                  description: "Continuous type test group",
                  hasAdvanced: true,
                  hasEssential: true,
                  managed: ManagedType.Managed,
                  name: "unittesting",
                  openFindings: 2,
                  openVulnerabilities: 0,
                  service: ServiceType.White,
                  subscription: "continuous",
                  userRole: "user",
                },
                {
                  closedVulnerabilities: 0,
                  description: "One-shot type test group",
                  hasAdvanced: true,
                  hasEssential: true,
                  managed: ManagedType.Managed,
                  name: "oneshottest",
                  openFindings: 1,
                  openVulnerabilities: 0,
                  service: ServiceType.White,
                  subscription: "oneshot",
                  userRole: "group_manager",
                },
                {
                  closedVulnerabilities: 0,
                  description: "Continuous group for deletion",
                  hasAdvanced: false,
                  hasEssential: true,
                  managed: ManagedType.Managed,
                  name: "pendingGroup",
                  openFindings: 2,
                  openVulnerabilities: 0,
                  service: ServiceType.White,
                  subscription: "continuous",
                  userRole: "customer_manager",
                },
              ],
              missedAuthors: 3,
              missedRepositories: 1,
              name: "okada",
              trial: null,
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [
          new GraphQLError("An error occurred fetching organization groups"),
        ],
      });
    },
  ),
  graphqlMocked.query(
    GET_ORGANIZATION_ROOTS,
    ({
      variables,
    }): StrictResponse<IErrorMessage | { data: GetOrganizationRootsQuery }> => {
      const { organizationId } = variables;

      if (organizationId === "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3") {
        return HttpResponse.json({
          data: {
            organization: {
              groups: [
                {
                  name: "unittesting",
                  roots: [],
                },
                {
                  name: "oneshottest",
                  roots: [],
                },
                {
                  name: "pendingGroup",
                  roots: [],
                },
              ],
              name: "okada",
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [
          new GraphQLError("An error occurred fetching organization roots"),
        ],
      });
    },
  ),
  graphqlMocked.query(
    GET_ORGANIZATION_EVENTS,
    ({
      variables,
    }): StrictResponse<IErrorMessage | { data: GetOrgEventsQuery }> => {
      const { organizationId } = variables;

      if (organizationId === "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3") {
        return HttpResponse.json({
          data: {
            organization: {
              groups: [
                {
                  events: [],
                  name: "unittesting",
                },
                {
                  events: [],
                  name: "oneshottest",
                },
                {
                  events: [],
                  name: "pendingGroup",
                },
              ],
              name: "okada",
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [
          new GraphQLError("An error occurred fetching organization events"),
        ],
      });
    },
  ),
  graphqlMocked.query(
    GET_N_GROUPS,
    ({
      variables,
    }): StrictResponse<IErrorMessage | { data: GetNGroupsQuery }> => {
      const { organizationId } = variables;

      // eslint-disable-next-line functional/no-let
      let organization = {
        nGroups: 2,
        name: "test",
      };

      switch (organizationId) {
        case "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3":
          organization = {
            ...organization,
            name: "okada",
          };
          break;

        case "ORG#f0c74b3e-bce4-4946-ba63-cb7e113ee817":
          organization = {
            ...organization,
            name: "unittesting",
          };
          break;

        default:
      }

      return HttpResponse.json({
        data: {
          organization: {
            ...organization,
          },
        },
      });
    },
  ),
  graphqlMocked.query(
    GET_ORGANIZATION_BILLING_BY_DATE,
    ({
      variables,
    }): StrictResponse<
      IErrorMessage | { data: GetOrganizationBillingBydateQuery }
    > => {
      const { organizationId } = variables;

      if (organizationId === "ORG#15eebe68-e9ce-4611-96f5-13d6562687e1") {
        return HttpResponse.json({
          data: {
            organization: {
              __typename: "Organization",
              billing: {
                authors: [
                  {
                    activeGroups: [
                      {
                        name: "continuoustesting",
                        tier: TierType.Advanced,
                      },
                      {
                        name: "unittesting",
                        tier: TierType.Advanced,
                      },
                    ],
                    actor: "Dev 1 <dev1@fluidattacks.com>",
                  },
                  {
                    activeGroups: [
                      {
                        name: "unittesting",
                        tier: TierType.Advanced,
                      },
                    ],
                    actor: "Dev 2 <dev2@fluidattacks.com>",
                  },
                ],
              },
              groups: [
                {
                  billing: {
                    costsAuthors: 798,
                    costsBase: 0,
                    costsTotal: 798,
                    numberAuthors: 4,
                  },
                  hasAdvanced: false,
                  hasEssential: true,
                  hasForces: true,
                  managed: ManagedType.Managed,
                  name: "unittesting",
                  paymentId: "None",
                  permissions: [],
                  service: ServiceType.White,
                  tier: TierType.Oneshot,
                },
              ],
              name: "okada",
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Organization error")],
      });
    },
  ),
  graphqlMocked.query(
    GET_ORGANIZATION_POLICIES,
    ({
      variables,
    }): StrictResponse<IErrorMessage | { data: GetOrganizationPolicies }> => {
      const { organizationId } = variables;

      if (organizationId === "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3") {
        return HttpResponse.json({
          data: {
            organization: {
              daysUntilItBreaks: null,
              inactivityPeriod: 90,
              maxAcceptanceDays: null,
              maxAcceptanceSeverity: 10,
              maxNumberAcceptances: null,
              minAcceptanceSeverity: 0,
              minBreakingSeverity: 0,
              name: "okada",
              userRole: "organization_manager",
              vulnerabilityGracePeriod: 1,
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Organization policies error")],
      });
    },
  ),
  graphqlMocked.query(
    GET_FINDINGS_POLICIES,
    ({
      variables,
    }): StrictResponse<IErrorMessage | { data: GetFindingsPoliciesQuery }> => {
      const { organizationId } = variables;

      if (organizationId === "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3") {
        return HttpResponse.json({
          data: {
            organization: {
              __typename: "Organization",
              findingPolicies: [
                {
                  __typename: "FindingPolicy",
                  id: "5f1de801-5c4b-428a-ba33-6a1b8d63e45c",
                  lastStatusUpdate: "2024-04-16T13:37:10+00:00",
                  name: "010. Stored cross-site scripting (XSS)",
                  status: OrganizationFindingPolicyStatus.Approved,
                  tags: [],
                  treatmentAcceptance: TreatmentAcceptancePolicy.Accepted,
                },
                {
                  __typename: "FindingPolicy",
                  id: "69bfacfd-fd4f-48c8-b2aa-92907b715da7",
                  lastStatusUpdate: "2024-09-23T16:37:18.901988+00:00",
                  name: "008. Reflected cross-site scripting (XSS)",
                  status: OrganizationFindingPolicyStatus.Submitted,
                  tags: [],
                  treatmentAcceptance:
                    TreatmentAcceptancePolicy.AcceptedUndefined,
                },
                {
                  __typename: "FindingPolicy",
                  id: "8b35ae2a-56a1-4f64-9da7-6a552683bf46",
                  lastStatusUpdate: "2021-04-26T13:37:10+00:00",
                  name: "007. Cross-site request forgery",
                  status: OrganizationFindingPolicyStatus.Approved,
                  tags: [],
                  treatmentAcceptance:
                    TreatmentAcceptancePolicy.AcceptedUndefined,
                },
                {
                  __typename: "FindingPolicy",
                  id: "9f1f7933-3300-4a25-a0a4-2106af4cb0b6",
                  lastStatusUpdate: "2024-09-23T16:38:19.952043+00:00",
                  name: "016. Insecure encryption algorithm - SSL/TLS",
                  status: OrganizationFindingPolicyStatus.Submitted,
                  tags: ["test"],
                  treatmentAcceptance: TreatmentAcceptancePolicy.Accepted,
                },
              ],
              name: "okada",
              userRole: "organization_manager",
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Organization policies error")],
      });
    },
  ),
  graphqlMocked.mutation(
    UPDATE_ORGANIZATION_POLICIES,
    (): StrictResponse<{ data: UpdateOrganizationPolicies }> => {
      return HttpResponse.json({
        data: {
          updateOrganizationPolicies: {
            success: true,
          },
        },
      });
    },
  ),
  graphqlMocked.mutation(
    UPDATE_ORGANIZATION_PRIORITY_POLICIES,
    (): StrictResponse<{ data: UpdateOrganizationPriorityPolicies }> => {
      return HttpResponse.json({
        data: {
          updateOrganizationPriorityPolicies: {
            success: true,
          },
        },
      });
    },
  ),
  graphqlMocked.query(
    GET_ORGANIZATION_CREDENTIALS,
    ({
      variables,
    }): StrictResponse<
      IErrorMessage | { data: GetOrganizationCredentials }
    > => {
      const { organizationId } = variables;

      if (organizationId === "ORG#15eebe68-e9ce-4611-96f5-13d6562687e1") {
        return HttpResponse.json({
          data: {
            organization: {
              __typename: "Organization",
              credentials: [
                {
                  __typename: "Credentials",
                  azureOrganization: "testorg1",
                  id: "6e52c11c-abf7-4ca3-b7d0-635e394f41c1",
                  isPat: true,
                  isToken: true,
                  name: "Credentials test",
                  oauthType: "",
                  owner: "owner@test.com",
                  type: "HTTPS",
                },
              ],
              groups: [
                {
                  gitRoots: {
                    edges: [],
                  },
                  name: "group-test",
                },
              ],
              name: "org-test",
            },
          },
        });
      }

      if (
        _.includes(["f0c74b3e-bce4-4946-ba63-cb7e113ee817", ""], organizationId)
      ) {
        return HttpResponse.json({
          data: {
            organization: {
              __typename: "Organization",
              credentials: [],
              groups: [],
              name: "okada",
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Organization credentials error")],
      });
    },
  ),
  graphqlMocked.mutation(
    ADD_CREDENTIALS,
    ({
      variables,
    }): StrictResponse<
      Record<"errors", IMessage[]> | { data: AddCredentialsMutationMutation }
    > => {
      const { organizationId } = variables;
      if (organizationId === "ORG#12c44fe4-4171-4f49-9cba-ef8b58790cbd") {
        return HttpResponse.json({
          errors: [
            new GraphQLError(
              "Exception - A credential exists with the same name",
            ),
            new GraphQLError(
              "Exception - Field cannot fill with blank characters",
            ),
            new GraphQLError("Exception - Password should start with a letter"),
            new GraphQLError(
              "Exception - Password should include at least one number",
            ),
            new GraphQLError(
              "Exception - Password should include lowercase characters",
            ),
            new GraphQLError(
              "Exception - Password should include uppercase characters",
            ),
            new GraphQLError(
              "Exception - Password should include symbols characters",
            ),

            new GraphQLError(
              "Exception - Password should not include sequentials characters",
            ),
          ],
        });
      }

      return HttpResponse.json({
        data: {
          addCredentials: {
            success: true,
          },
        },
      });
    },
  ),
  graphqlMocked.mutation(
    REMOVE_CREDENTIALS,
    (): StrictResponse<{ data: RemoveCredentials }> => {
      return HttpResponse.json({
        data: {
          removeCredentials: {
            success: true,
          },
        },
      });
    },
  ),
  graphqlMocked.mutation(
    UPDATE_CREDENTIALS,
    ({
      variables,
    }): StrictResponse<
      IErrorMessage | { data: UpdateCredentialsMutationMutation }
    > => {
      const { credentialsId, organizationId } = variables;

      if (
        credentialsId === "6e52c11c-abf7-4ca3-b7d0-635e394f41c1" &&
        organizationId === "ORG#15eebe68-e9ce-4611-96f5-13d6562687e1"
      ) {
        return HttpResponse.json({
          data: {
            updateCredentials: {
              success: true,
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Error updating credentials")],
      });
    },
  ),
  graphqlMocked.query(
    GET_ORGANIZATION_COMPLIANCE,
    ({
      variables,
    }): StrictResponse<IErrorMessage | { data: GetOrganizationCompliance }> => {
      const { organizationId } = variables;

      if (organizationId === "ORG#15eebe68-e9ce-4611-96f5-13d6562687e1") {
        return HttpResponse.json({
          data: {
            organization: {
              __typename: "Organization",
              compliance: {
                __typename: "OrganizationCompliance",
                complianceLevel: 0.3,
                complianceWeeklyTrend: -0.02,
                estimatedDaysToFullCompliance: 10,
                standards: [
                  {
                    __typename: "OrganizationComplianceStandard",
                    avgOrganizationComplianceLevel: 0.5,
                    bestOrganizationComplianceLevel: 0.99,
                    complianceLevel: 0.75,
                    standardTitle: "standardname1",
                    worstOrganizationComplianceLevel: 0.2,
                  },
                  {
                    __typename: "OrganizationComplianceStandard",
                    avgOrganizationComplianceLevel: 0.6,
                    bestOrganizationComplianceLevel: 0.8,
                    complianceLevel: 0.88,
                    standardTitle: "standardname2",
                    worstOrganizationComplianceLevel: 0.0,
                  },
                ],
              },
              name: "org-test",
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Organization compliance error")],
      });
    },
  ),
  graphqlMocked.query(
    GET_ORGANIZATION_GROUP_NAMES,
    ({
      variables,
    }): StrictResponse<
      IErrorMessage | { data: GetOrganizationGroupNamesQuery }
    > => {
      const { organizationId } = variables;

      if (organizationId === "ORG#15eebe68-e9ce-4611-96f5-13d6562687e1") {
        return HttpResponse.json({
          data: {
            organization: {
              __typename: "Organization",
              groups: [
                {
                  name: "group1",
                },
                {
                  name: "group2",
                },
              ],
              name: "org-test",
            },
          },
        });
      }
      if (organizationId === "okada") {
        return HttpResponse.json({
          data: {
            organization: {
              __typename: "Organization",
              groups: [
                {
                  name: "test",
                },
              ],
              name: "okada",
            },
          },
        });
      }
      if (organizationId === "testorg") {
        return HttpResponse.json({
          data: {
            organization: {
              __typename: "Organization",
              groups: [
                {
                  name: "testgroup",
                },
              ],
              name: "testorg",
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Organization group names error")],
      });
    },
  ),
  graphqlMocked.query(
    GET_ORGANIZATION_BILLING_FROM_BILLING,
    ({
      variables,
    }): StrictResponse<
      IErrorMessage | { data: GetOrganizationBillingAtBillingViewQuery }
    > => {
      const { organizationId } = variables;

      if (organizationId === "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3") {
        return HttpResponse.json({
          data: {
            organization: {
              __typename: "Organization",
              billing: {
                __typename: "OrganizationBilling",
                costsAuthors: 1197,
                costsBase: 2798,
                costsTotal: 3995,
                numberAuthorsAdvanced: 3,
                numberAuthorsEssential: 3,
                numberAuthorsTotal: 5,
                numberGroupsAdvanced: 1,
                numberGroupsEssential: 1,
                numberGroupsTotal: 3,
                paymentMethods: [
                  {
                    __typename: "PaymentMethod",
                    brand: "",
                    businessName: "Testing Company and Sons",
                    city: "-",
                    country: "Colombia",
                    default: false,
                    email: "test@test.test",
                    expirationMonth: "",
                    expirationYear: "",
                    id: "4722b0b7-cfeb-4898-8308-185dfc2523bc",
                    lastFourDigits: "",
                    rut: null,
                    state: "-",
                    taxId: null,
                  },
                ],
              },
              country: "Colombia",
              name: "org-test",
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Organization error")],
      });
    },
  ),
  graphqlMocked.query(
    GET_ORGANIZATION_GROUPS_FROM_OUTSIDE,
    ({
      variables,
    }): StrictResponse<IErrorMessage | { data: GeOrganizationGroupsQuery }> => {
      const { organizationId } = variables;

      if (organizationId === "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3") {
        return HttpResponse.json({
          data: {
            organization: {
              __typename: "Organization",
              groups: [
                {
                  __typename: "Group",
                  name: "group1",
                  permissions: [
                    "integrates_api_mutations_add_git_root_mutate",
                    "integrates_api_mutations_update_git_root_mutate",
                  ],
                  serviceAttributes: ["has_service_white"],
                },
                {
                  __typename: "Group",
                  name: "group2",
                  permissions: ["integrates_api_mutations_add_git_root_mutate"],
                  serviceAttributes: ["has_service_black"],
                },
              ],
              name: "org-test",
              permissions: ["integrates_api_mutations_add_credentials_mutate"],
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Organization groups error")],
      });
    },
  ),
  graphqlMocked.query(
    GET_ORGANIZATION_INTEGRATION_REPOSITORIES,
    ({
      variables,
    }): StrictResponse<
      IErrorMessage | { data: GetOrganizationIntegrationRepositoriesQuery }
    > => {
      const { organizationId } = variables;

      if (organizationId === "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3") {
        return HttpResponse.json({
          data: {
            organization: {
              __typename: "Organization",
              integrationRepositoriesConnection: {
                __typename: "IntegrationRepositoriesConnection",
                edges: [
                  {
                    __typename: "IntegrationRepositoriesEdge",
                    node: {
                      __typename: "OrganizationIntegrationRepositories",
                      defaultBranch: "main",
                      lastCommitDate: "2022-11-09 02:34:40+00:00",
                      url: "https://testrepo.com/testorg1/testproject1/_git/testrepo",
                    },
                  },
                  {
                    __typename: "IntegrationRepositoriesEdge",
                    node: {
                      __typename: "OrganizationIntegrationRepositories",
                      defaultBranch: "main",
                      lastCommitDate: "2022-10-19 02:34:40+00:00",
                      url: "https://testrepo.com/testorg1/testproject1/_git/testsecondrepo",
                    },
                  },
                ],
                pageInfo: {
                  endCursor: "bnVsbA==",
                  hasNextPage: false,
                },
              },
              name: "org-test",
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Organization error")],
      });
    },
  ),
  graphqlMocked.query(
    GET_ORGANIZATION_ID,
    ({
      variables,
    }): StrictResponse<IErrorMessage | { data: GetOrganizationId }> => {
      const orgs = ["okada", "bulat", "testorg"];
      const { organizationName } = variables;

      if (orgs.includes(organizationName)) {
        return HttpResponse.json({
          data: {
            organizationId: {
              __typename: "Organization",
              id: "ORG#f0c74b3e-bce4-4946-ba63-cb7e113ee817",
              name: organizationName,
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Organization group names error")],
      });
    },
  ),
  graphqlMocked.query(
    GET_ORG_EVENTS,
    ({
      variables,
    }): StrictResponse<IErrorMessage | { data: GetOrganizationEvents }> => {
      const { organizationName } = variables;

      if (organizationName === "okada") {
        return HttpResponse.json({
          data: {
            organizationId: {
              __typename: "Organization",
              groups: [
                {
                  __typename: "Group",
                  events: [],
                  name: "TEST",
                },
                {
                  __typename: "Group",
                  events: [],
                  name: "test",
                },
              ],
              name: "okada",
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Organization group events error")],
      });
    },
  ),
  graphqlMocked.query(
    GET_ORGANIZATION_POLICIES_DESCRIPTION,
    (): StrictResponse<{ data: GetOrganizationPoliciesDescription }> => {
      return HttpResponse.json({
        data: {
          organizationId: {
            __typename: "Organization",
            findingPolicies: [
              {
                id: "8b35ae2a-56a1-4f64-9da7-6a552683bf46",
                lastStatusUpdate: "2021-04-26T13:37:10+00:00",
                name: "007. Cross-site request forgery",
                status: OrganizationFindingPolicyStatus.Approved,
                tags: [],
                treatmentAcceptance:
                  TreatmentAcceptancePolicy.AcceptedUndefined,
              },
              {
                id: "c78c640e-7e10-4fa4-ac46-c6ea7924a036",
                lastStatusUpdate: "2023-08-28T13:50:36.517358+00:00",
                name: "038. Business information leak",
                status: OrganizationFindingPolicyStatus.Approved,
                tags: [],
                treatmentAcceptance: TreatmentAcceptancePolicy.Accepted,
              },
            ],
            name: "okada",
          },
        },
      });
    },
  ),
  graphqlMocked.query(
    GET_CREDENTIALS_FROM_OAUTH_FORM,
    (): StrictResponse<{ data: GetOrganizationCredentials }> => {
      return HttpResponse.json({
        data: {
          organization: {
            __typename: "Organization",
            credentials: [],
            groups: [],
            name: "okada",
          },
        },
      });
    },
  ),
  graphqlMocked.query(
    GET_ORGANIZATION_DATA,
    (): StrictResponse<{ data: GetOrganizationData }> => {
      return HttpResponse.json({
        data: {
          me: {
            __typename: "Me",
            tags: [],
            userEmail: "test@fluidattacks.com",
          },
          organizationId: {
            __typename: "Organization",
            hasZtnaRoots: false,
            id: "ORG#f0c74b3e-bce4-4946-ba63-cb7e113ee811",
            name: "test",
            permissions: [],
          },
        },
      });
    },
  ),
  graphqlMocked.query(
    GET_ORGANIZATION_ID_DATA,
    ({
      variables,
    }): StrictResponse<IErrorMessage | { data: GetOrganizationIdData }> => {
      const orgs = ["okada", "bulat", "testorg"];
      const { organizationName } = variables;

      if (orgs.includes(organizationName)) {
        return HttpResponse.json({
          data: {
            organizationId: {
              __typename: "Organization",
              hasZtnaRoots: false,
              id: "ORG#f0c74b3e-bce4-4946-ba63-cb7e113ee817",
              name: organizationName,
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Organization id data error")],
      });
    },
  ),
];
