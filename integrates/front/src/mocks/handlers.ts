import { eventHandlers } from "mocks/events/handlers";
import { externalHandlers } from "mocks/external/handlers";
import { findingHandlers } from "mocks/finding/handlers";
import { groupHandlers } from "mocks/group/handlers";
import { meHandlers } from "mocks/me/handlers";
import { organizationHandlers } from "mocks/organization/handlers";
import { vulnerabilitiesHandlers } from "mocks/vulnerabilities/handlers";

export const handlers = [
  ...externalHandlers,
  ...eventHandlers,
  ...findingHandlers,
  ...groupHandlers,
  ...meHandlers,
  ...organizationHandlers,
  ...vulnerabilitiesHandlers,
];
