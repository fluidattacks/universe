import type { TypedDocumentNode } from "@apollo/client";
import { CustomThemeProvider as DesignThemeProvider } from "@fluidattacks/design";
import type { RenderOptions, RenderResult } from "@testing-library/react";
import { render as renderFromRtl } from "@testing-library/react";
import type {
  GraphQLHandler,
  GraphQLQuery,
  HttpHandler,
  StrictResponse,
  graphql,
} from "msw";
import { HttpResponse } from "msw";
import * as React from "react";
import { MemoryRouter } from "react-router-dom";
import type { MemoryRouterProps } from "react-router-dom";

import { CustomThemeProvider } from "components/colors";
import { authContext } from "context/auth";
import { server } from "mocks/server";
import { ApolloProvider } from "utils/apollo";

interface IRenderOptions extends RenderOptions {
  mocks?: (GraphQLHandler | HttpHandler)[];
  memoryRouter?: MemoryRouterProps;
}

type TReturn<T> = T extends (...args: never[]) => infer R ? R : never;

function createGraphQLMocks<T extends GraphQLQuery, S>(
  graphqlMocked: TReturn<(typeof graphql)["link"]>,
  mocks: [TypedDocumentNode<T, S>, "mutation" | "query", T][],
): GraphQLHandler[] {
  return mocks.map(
    ([query, type, response]): GraphQLHandler =>
      graphqlMocked[type](
        query,
        (): StrictResponse<{ data: T }> =>
          HttpResponse.json({ data: response }),
      ),
  );
}

const authContextMock = {
  awsSubscription: null,
  tours: {
    newGroup: true,
    newRoot: true,
    welcome: true,
  },
  userEmail: "test@testing.com",
  userName: "test",
};

const Providers = ({
  children,
  memoryRouter = {},
}: Readonly<
  React.PropsWithChildren<{
    memoryRouter?: MemoryRouterProps;
  }>
>): JSX.Element => {
  return (
    <MemoryRouter {...memoryRouter}>
      <CustomThemeProvider>
        <DesignThemeProvider>
          <authContext.Provider value={authContextMock}>
            <ApolloProvider>{children}</ApolloProvider>
          </authContext.Provider>
        </DesignThemeProvider>
      </CustomThemeProvider>
    </MemoryRouter>
  );
};

const render = (
  children: React.ReactElement,
  options?: Omit<IRenderOptions, "wrapper">,
): RenderResult => {
  const { mocks = undefined, memoryRouter = {} } = options ?? {};

  if (mocks) {
    server.use(...mocks);
  }

  return renderFromRtl(children, {
    wrapper: (props): JSX.Element => (
      <Providers {...props} memoryRouter={memoryRouter} />
    ),
  });
};

export { render, createGraphQLMocks };
