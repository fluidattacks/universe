import { useQuery } from "@apollo/client";
import _ from "lodash";
import { createContext, useCallback, useMemo } from "react";
import * as React from "react";

import type { IAllGroupContext } from "./types";

import {
  authzGroupContext,
  authzPermissionsContext,
  groupAttributes,
  groupLevelPermissions,
} from "../config";
import type { IGroups, IOrganizationGroups } from "@types";
import { GET_USER_ORGANIZATIONS_GROUPS } from "pages/to-do/queries";
import type { IAction, IGroupAction } from "pages/to-do/types";
import { Logger } from "utils/logger";

const allGroupContext = createContext<IAllGroupContext>({});

const AllGroupPermissionsProvider: React.FC<
  Readonly<{
    children: JSX.Element;
  }>
> = ({ children }): JSX.Element => {
  const { data: userData } = useQuery(GET_USER_ORGANIZATIONS_GROUPS, {
    fetchPolicy: "cache-first",
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.warning("An error occurred fetching user groups", error);
      });
    },
  });

  const onGroupChange = useCallback(
    (vulnerabilitiesGroupName: string[]): void => {
      groupAttributes.update([]);
      groupLevelPermissions.update([]);
      if (userData !== undefined) {
        const currentPermissions = _.flatten(
          userData.me.organizations.map((organization): IAction[][] =>
            organization.groups.map((group): IAction[] =>
              group.permissions.map((action): IAction => ({ action })),
            ),
          ),
        );
        const groupsServicesAttributes = userData.me.organizations.reduce<
          IOrganizationGroups["groups"]
        >(
          (previousValue, currentValue): IOrganizationGroups["groups"] => [
            ...previousValue,
            ...currentValue.groups.filter((group): group is IGroups =>
              vulnerabilitiesGroupName.includes(group.name.toLowerCase()),
            ),
          ],
          [],
        );
        const currentAttributes = Array.from(
          new Set(
            groupsServicesAttributes.reduce(
              (previous: string[], current): string[] => [
                ...previous,
                ...current.serviceAttributes,
              ],
              [],
            ),
          ),
        );
        if (currentAttributes.length > 0) {
          groupAttributes.update(
            currentAttributes.map((action): IAction => ({ action })),
          );
        }
        if (currentPermissions.length > 0 && currentPermissions[0].length > 0) {
          groupLevelPermissions.update(
            Array.from(
              new Set(
                currentPermissions.reduce(
                  (
                    selectedPermission: IAction[],
                    currentPermission: IAction[],
                  ): IAction[] =>
                    currentPermission.length < selectedPermission.length
                      ? currentPermission
                      : selectedPermission,
                  currentPermissions[0],
                ),
              ),
            ),
          );
        }
      }
    },
    [userData],
  );

  const changePermissions = useCallback(
    (groupName: string): void => {
      groupLevelPermissions.update([]);
      if (userData !== undefined) {
        const recordPermissions: IGroupAction[] = _.flatten(
          userData.me.organizations.map((organization): IGroupAction[] =>
            organization.groups.map(
              (group): IGroupAction => ({
                actions: group.permissions.map(
                  (action): IAction => ({
                    action,
                  }),
                ),
                groupName: group.name,
              }),
            ),
          ),
        );
        const filteredPermissions: IGroupAction[] = recordPermissions.filter(
          (recordPermission: IGroupAction): boolean =>
            recordPermission.groupName.toLowerCase() ===
            groupName.toLowerCase(),
        );
        if (filteredPermissions.length > 0) {
          groupLevelPermissions.update(filteredPermissions[0].actions);
        }
      }
    },
    [userData],
  );

  const allGroupValue = useMemo(
    (): IAllGroupContext => ({
      changePermissions,
      onGroupChange,
      userData: {
        ...userData,
        me: {
          ...userData?.me,
          __typename: "Me",
          organizations:
            userData?.me.organizations.map(
              (org): IOrganizationGroups => ({
                ...org,
                __typename: "Organization",
                groups: org.groups.map(
                  (group): IGroups => ({
                    ...group,
                    __typename: "Group",
                    name: group.name,
                    permissions: group.permissions,
                    serviceAttributes: group.serviceAttributes,
                  }),
                ),
              }),
            ) ?? [],
          userEmail: userData?.me.userEmail ?? "",
        },
      },
    }),
    [changePermissions, onGroupChange, userData],
  );

  if (_.isUndefined(userData) || _.isEmpty(userData)) {
    return <div />;
  }

  return (
    <authzGroupContext.Provider value={groupAttributes}>
      <authzPermissionsContext.Provider value={groupLevelPermissions}>
        <allGroupContext.Provider value={allGroupValue}>
          {children}
        </allGroupContext.Provider>
      </authzPermissionsContext.Provider>
    </authzGroupContext.Provider>
  );
};

export { allGroupContext, AllGroupPermissionsProvider };
