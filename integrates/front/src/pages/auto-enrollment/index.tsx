import { Container } from "@fluidattacks/design";
import isUndefined from "lodash/isUndefined";
import { useCallback, useEffect, useMemo, useState } from "react";
import * as React from "react";
import { useNavigate } from "react-router-dom";

import { FastTrack } from "./fast-track";
import { useGetStakeholderGroupsQuery } from "./hooks/get-stakeholder-groups";
import { OauthRepoForm } from "./oauth-repo-form";
import { RepositoryForm } from "./repository-form";
import { StyledMainContent } from "./styles";
import type {
  IAutoenrollment,
  IOrgAttr,
  IRootAttr,
  TEnrollPages,
} from "./types";

import { ProgressTopBar } from "pages/auto-enrollment/progress-top-bar";

const INITIAL_PROGRESS = 50;
const Autoenrollment: React.FC<IAutoenrollment> = ({
  initialPage,
  trialGroupName,
  trialOrgId,
  trialOrgName,
}): JSX.Element => {
  const navigate = useNavigate();

  // State management
  const [page, setPage] = useState<TEnrollPages>(initialPage ?? "fastTrack");
  const [progress, setProgress] = useState(INITIAL_PROGRESS);
  const [rootMessages, setRootMessages] = useState({
    message: "",
    type: "success",
  });

  const [repository, setRepository] = useState<IRootAttr>({
    branch: "",
    credentials: {
      arn: "",
      auth: "TOKEN",
      azureOrganization: undefined,
      isPat: false,
      key: "",
      name: "",
      password: "",
      token: "",
      type: "",
      typeCredential: "",
      user: "",
    },
    exclusions: [],
    hasExclusions: "",
    url: "",
  });

  // API operations
  const { data, loading, refetch } = useGetStakeholderGroupsQuery();

  const successMutation = useMemo(
    (): { group: boolean; organization: boolean } => ({
      group: Boolean(data.me.group.name),
      organization: Boolean(data.me.organization.name),
    }),
    [data],
  );

  const onAddRoot = useCallback((): void => {
    if (!isUndefined(trialOrgName) && !isUndefined(trialGroupName)) {
      location.replace(
        `/orgs/${trialOrgName.toLowerCase()}` +
          `/groups/${trialGroupName.toLowerCase()}/vulns`,
      );
    }
  }, [trialOrgName, trialGroupName]);

  useEffect((): void => {
    if (!loading && data.me.isPersonalEmail) {
      navigate("/SignUp?error=invalidEmail");
    }
  }, [loading, data.me.isPersonalEmail, navigate]);

  if (loading) {
    return <div />;
  }

  const { organization } = data.me;

  const pages: Record<TEnrollPages, JSX.Element> = {
    fastTrack: <FastTrack refetch={refetch} setPage={setPage} />,
    oauthRepoForm: (
      <OauthRepoForm
        onAddRoot={onAddRoot}
        setProgress={setProgress}
        trialGroupName={trialGroupName}
        trialOrgId={trialOrgId}
      />
    ),
    repository: (
      <RepositoryForm
        externalId={organization.awsExternalId}
        initialValues={repository}
        mutationsState={successMutation}
        orgId={organization.id}
        orgName={organization.name}
        organizationValues={data.me.organizationValues as IOrgAttr}
        rootMessages={rootMessages}
        setPage={setPage}
        setProgress={setProgress}
        setRepositoryValues={setRepository}
        setRootMessages={setRootMessages}
      />
    ),
    standBy: <div />,
  };

  return (
    <StyledMainContent>
      <ProgressTopBar progress={progress} userName={data.me.userName} />
      <Container id={"dashboard"}>{pages[page]}</Container>
    </StyledMainContent>
  );
};

export { Autoenrollment };
