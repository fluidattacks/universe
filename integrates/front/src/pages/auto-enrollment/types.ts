interface IAutoenrollment {
  initialPage?: TEnrollPages;
  trialGroupName?: string;
  trialOrgName?: string;
  trialOrgId?: string;
}

interface IAddOrganizationResult {
  addOrganization: {
    organization: {
      awsExternalId: string;
      id: string;
      name: string;
    };
    success: boolean;
  };
}

interface IAddRootProps {
  externalId: string;
  initialValues: IRootAttr;
  mutationsState: {
    group: boolean;
    organization: boolean;
  };
  orgId: string;
  orgName: string;
  organizationValues: IOrgAttr;
  rootMessages: {
    message: string;
    type: string;
  };
  setPage: React.Dispatch<React.SetStateAction<TEnrollPages>>;
  setProgress: React.Dispatch<React.SetStateAction<number>>;
  setRepositoryValues: React.Dispatch<React.SetStateAction<IRootAttr>>;
  setRootMessages: React.Dispatch<
    React.SetStateAction<{
      message: string;
      type: string;
    }>
  >;
}

type TAlertMessages = React.Dispatch<
  React.SetStateAction<{
    message: string;
    type: string;
  }>
>;

interface IGetStakeholderGroupsResult {
  me: {
    organizations: {
      country: string;
      groups: {
        name: string;
      }[];
      name: string;
    }[];
    trial: {
      completed: boolean;
      startDate: string;
    } | null;
    userEmail: string;
    userName: string;
  };
}

interface IRootAttr {
  branch: string;
  credentials: {
    arn: string;
    auth: "AWSROLE" | "TOKEN" | "USER";
    azureOrganization: string | undefined;
    isPat: boolean;
    key: string;
    name: string;
    password: string;
    token: string;
    type: "" | "AWSROLE" | "HTTPS" | "SSH";
    typeCredential: "" | "AWSROLE" | "OAUTH" | "SSH" | "TOKEN" | "USER";
    user: string;
  };
  exclusions: string[];
  hasExclusions: string;
  url: string;
}

interface IOrgAttr {
  groupDescription: string;
  groupName: string;
  organizationCountry: string;
  organizationName: string;
  reportLanguage: string;
  terms: string[];
}

interface ICredential {
  arn: string;
  auth: "AWSROLE" | "TOKEN" | "USER";
  azureOrganization: string | undefined;
  isPat: boolean | undefined;
  key: string;
  name: string;
  password: string;
  token: string;
  type: "" | "AWSROLE" | "HTTPS" | "SSH";
  user: string;
}

type TEnrollPages = "fastTrack" | "oauthRepoForm" | "repository" | "standBy";

export type {
  IAddOrganizationResult,
  IAddRootProps,
  IAutoenrollment,
  ICredential,
  IGetStakeholderGroupsResult,
  IOrgAttr,
  IRootAttr,
  TAlertMessages,
  TEnrollPages,
};
