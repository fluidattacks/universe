import { styled } from "styled-components";

const StyledLink = styled.a.attrs({
  rel: "noopener noreferrer",
  target: "_blank",
})`
  color: ${({ theme }): string => theme.palette.gray[700]};
  cursor: pointer;
  text-decoration: underline;
`;

export { StyledLink };
