import { Button, Container, Heading, Text } from "@fluidattacks/design";
import { useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { StyledLink } from "./styles";
import type { IRepositoryFormProps } from "./types";

import { AddRoot } from "../add-root";
import { LanguageSidebar } from "../language-sidebar";
import { useModal } from "hooks/use-modal";

const RepositoryForm: React.FC<IRepositoryFormProps> = ({
  externalId,
  initialValues,
  mutationsState,
  orgId,
  orgName,
  organizationValues,
  rootMessages,
  setPage,
  setProgress,
  setRepositoryValues,
  setRootMessages,
}): JSX.Element => {
  const languageModalRef = useModal("language-modal");
  const { t } = useTranslation();

  const handleOpenModal = useCallback((): void => {
    languageModalRef.open();
  }, [languageModalRef]);

  return (
    <Container
      alignItems={"center"}
      center={true}
      display={"flex"}
      flexDirection={"column"}
      justify={"center"}
      maxWidth={"700px"}
      my={1.5}
      pb={3}
      pt={3.5}
      px={2}
      scroll={"none"}
    >
      <Button
        disabled={languageModalRef.isOpen}
        icon={"circle-info"}
        onClick={handleOpenModal}
        variant={"ghost"}
      >
        {t("autoenrollment.languages.checkLanguages")}
      </Button>
      <LanguageSidebar modalRef={languageModalRef} />
      <Heading
        fontWeight={"bold"}
        lineSpacing={2}
        mb={1.25}
        mt={2}
        size={"lg"}
        textAlign={"center"}
      >
        {t("autoenrollment.title")}
      </Heading>
      <Text mb={1.5} size={"sm"} textAlign={"center"}>
        {t("autoenrollment.subtitle.main")}{" "}
        <StyledLink
          href={
            "https://forms.zohopublic.com/fluid4ttacks/form/Connector/" +
            "formperma/545HRaMNtPUtcUWstXkbVSFVtluGQdMCIT9QXwMvZps"
          }
        >
          {t("autoenrollment.subtitle.contact")}
        </StyledLink>{" "}
        {t("autoenrollment.subtitle.end")}
      </Text>
      <Container
        bgColor={"#ffffff"}
        border={"1px solid #e9e9ed"}
        borderRadius={"8px"}
        px={1.5}
        py={1.5}
        shadow={"md"}
        width={"700px"}
      >
        <AddRoot
          externalId={externalId}
          initialValues={initialValues}
          mutationsState={mutationsState}
          orgId={orgId}
          orgName={orgName}
          organizationValues={organizationValues}
          rootMessages={rootMessages}
          setPage={setPage}
          setProgress={setProgress}
          setRepositoryValues={setRepositoryValues}
          setRootMessages={setRootMessages}
        />
      </Container>
    </Container>
  );
};

export { RepositoryForm };
