import {
  Button,
  Container,
  Heading,
  SlideOutMenu,
  Tag,
  Text,
} from "@fluidattacks/design";
import { useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import {
  advancedCICD,
  advancedInfra,
  advancedLanguages,
  essentialLanguages,
} from "./utils";

import { List } from "components/list";
import type { IUseModal } from "hooks/use-modal";

interface ILanguageSidebarProps {
  modalRef: IUseModal;
}

const LanguageSidebar: React.FC<Readonly<ILanguageSidebarProps>> = ({
  modalRef,
}): JSX.Element => {
  const { t } = useTranslation();
  const [page, setPage] = React.useState<"advanced" | "essential">("essential");

  const closeModal = useCallback((): void => {
    modalRef.close();
  }, [modalRef]);

  const goToEssential = useCallback((): void => {
    setPage("essential");
  }, []);

  const goToAdvanced = useCallback((): void => {
    setPage("advanced");
  }, []);

  const pages = {
    advanced: (
      <Container>
        <Container center={true} display={"block"} mt={0}>
          <Button onClick={goToEssential} showArrow={true} variant={"tertiary"}>
            {"Go to Essential Plan"}
          </Button>
        </Container>
        <Heading fontWeight={"bold"} mt={1.25} size={"md"}>
          {t("autoenrollment.languages.advancedLanguages.advancedPlan")}
        </Heading>
        <Container display={"flex"} wrap={"wrap"}>
          <Text mt={1} size={"sm"}>
            {t("autoenrollment.languages.advancedLanguages.description")}
          </Text>
          <Text mt={1} size={"sm"}>
            {t("autoenrollment.languages.advancedLanguages.description2")}
          </Text>
        </Container>
        <Container>
          <List cols={1} items={advancedLanguages} />
          <Text mt={1} size={"sm"}>
            {t(
              "autoenrollment.languages.advancedLanguages.advancedSupportedCICD",
            )}
          </Text>
          <List cols={1} items={advancedCICD} />
          <Text mt={1} size={"sm"}>
            {t(
              "autoenrollment.languages.advancedLanguages.advancedSupportedInfra",
            )}
          </Text>
          <List cols={1} items={advancedInfra} />
        </Container>
      </Container>
    ),
    essential: (
      <Container>
        <Container center={true} display={"block"} mt={0}>
          <Button onClick={goToAdvanced} showArrow={true} variant={"tertiary"}>
            {"Go to Advanced Plan"}
          </Button>
        </Container>
        <Heading fontWeight={"bold"} mt={1.25} size={"md"}>
          {t("autoenrollment.languages.essentialLanguages.essentialPlan")}
          &nbsp;
          <Tag
            tagLabel={t("autoenrollment.languages.essentialLanguages.tag")}
            variant={"inactive"}
          />
        </Heading>
        <Text mt={1} size={"sm"}>
          {t("autoenrollment.languages.essentialLanguages.description")}
        </Text>
        <Container pb={0.75} pl={1} pr={1} pt={0.75}>
          <List cols={2} items={essentialLanguages} />
        </Container>
      </Container>
    ),
  };

  return (
    <SlideOutMenu
      isOpen={modalRef.isOpen}
      onClose={closeModal}
      title={t("autoenrollment.languages.essentialLanguages.title")}
    >
      {pages[page]}
    </SlideOutMenu>
  );
};

export { LanguageSidebar };
