import { styled } from "styled-components";

const autoEnrollmentBg =
  "https://res.cloudinary.com/fluid-attacks/image/upload/v1707504905/integrates/resources/autoEnrollmentBg.webp";

const StyledMainContent = styled.div`
  background-image: url(${autoEnrollmentBg});
  background-size: cover;
  min-height: 100vh;
  max-height: 100vh;
  overflow: auto;
`;
export { StyledMainContent };
