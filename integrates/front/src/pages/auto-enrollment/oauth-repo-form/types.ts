import type { TEnrollPages } from "../types";

interface IFastTrack {
  setPage: React.Dispatch<React.SetStateAction<TEnrollPages>>;
}

export type { IFastTrack };
