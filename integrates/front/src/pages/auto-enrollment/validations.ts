import type { AnyObject, ArraySchema, Schema } from "yup";
import { array, lazy, object, string } from "yup";

import type { IRootAttr } from "./types";

import { translate } from "utils/translations/translate";

const { t } = translate;

const validationSchema = lazy(
  (values: IRootAttr): Schema =>
    object().shape({
      branch: string()
        .required(t("validations.required"))
        .matches(/^[a-zA-Z0-9]+$/u, t("validations.alphanumeric")),
      credentials: object({
        arn: string().when("type", ([type]: string[]): Schema => {
          return type === "AWSROLE"
            ? string().required(t("validations.required")).isValidArnFormat()
            : string();
        }),
        auth: string(),
        azureOrganization: string().when(
          ["type", "isPat"],
          ([type, isPat]: unknown[]): Schema => {
            const protocol = values.credentials.auth === "TOKEN" ? "HTTPS" : "";

            return String(type) === protocol && Boolean(isPat)
              ? string()
                  .required(t("validations.required"))
                  .isValidTextField(t("validations.invalidSpaceInField"), /\s/u)
              : string();
          },
        ),
        key: string().when("type", ([type]: string[]): Schema => {
          return type === "SSH"
            ? string()
                .required(t("validations.required"))
                .matches(
                  /^-{5}BEGIN OPENSSH PRIVATE KEY-{5}\n(?:[a-zA-Z0-9+/=]+\n)+-{5}END OPENSSH PRIVATE KEY-{5}\n?$/u,
                  t("validations.invalidSshFormat"),
                )
            : string();
        }),
        name: string().when("type", ([type]: unknown[]): Schema => {
          return type === undefined
            ? string()
            : string().required(t("validations.required"));
        }),
        password: string().when("type", (): Schema => {
          const protocol = values.credentials.auth === "USER" ? "HTTPS" : "";

          return protocol
            ? string().required(t("validations.required"))
            : string();
        }),
        token: string().when("type", (): Schema => {
          const protocol = values.credentials.auth === "TOKEN" ? "HTTPS" : "";

          return protocol
            ? string().required(t("validations.required"))
            : string();
        }),
        type: string().required(t("validations.required")),
        typeCredential: string().required(t("validations.required")),
        user: string().when("type", (): Schema => {
          const protocol = values.credentials.auth === "USER" ? "HTTPS" : "";

          return protocol
            ? string().required(t("validations.required"))
            : string();
        }),
      }),
      exclusions: array()
        .of(string().required())
        .when("hasExclusions", {
          is: "yes",
          then: (): ArraySchema<string[] | undefined, AnyObject, ""> =>
            array()
              .of(string().required().isValidExcludeFormat(values.url))
              .min(1, t("validations.required")),
        }),
      hasExclusions: string().required(t("validations.required")),
      url: string()
        .isValidUrlProtocolOrSSHFormat()
        .required(t("validations.required")),
    }),
);

export { validationSchema };
