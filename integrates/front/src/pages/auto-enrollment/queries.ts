import { graphql } from "gql";

const ADD_ENROLLMENT = graphql(`
  mutation AddEnrollment {
    addEnrollment {
      success
    }
  }
`);

const ADD_GIT_ROOT = graphql(`
  mutation AddGitRootAutoEnrollment(
    $branch: String!
    $credentials: RootCredentialsInput
    $gitignore: [String!]
    $groupName: String!
    $includesHealthCheck: Boolean!
    $nickname: String!
    $url: String!
    $useEgress: Boolean!
    $useVpn: Boolean!
    $useZtna: Boolean!
  ) {
    addGitRoot(
      branch: $branch
      credentials: $credentials
      gitignore: $gitignore
      groupName: $groupName
      includesHealthCheck: $includesHealthCheck
      nickname: $nickname
      url: $url
      useEgress: $useEgress
      useVpn: $useVpn
      useZtna: $useZtna
    ) {
      success
    }
  }
`);

const ADD_GROUP = graphql(`
  mutation AddGroup(
    $description: String!
    $groupName: String!
    $hasAdvanced: Boolean!
    $hasEssential: Boolean!
    $language: Language!
    $organizationName: String!
    $service: ServiceType!
    $subscription: SubscriptionType!
  ) {
    addGroup(
      description: $description
      groupName: $groupName
      hasAdvanced: $hasAdvanced
      hasEssential: $hasEssential
      language: $language
      organizationName: $organizationName
      service: $service
      subscription: $subscription
    ) {
      success
    }
  }
`);

const ADD_ORGANIZATION = graphql(`
  mutation AddOrganization($name: String!) {
    addOrganization(name: $name) {
      success
      organization {
        id
        awsExternalId
        name
      }
    }
  }
`);

const GET_STAKEHOLDER_GROUPS = graphql(`
  query GetStakeholderGroups {
    me {
      isPersonalEmail
      userEmail
      userName
      organizations {
        id
        awsExternalId
        country
        name
        groups {
          name
        }
      }
      trial {
        completed
        startDate
      }
    }
  }
`);

const VALIDATE_GIT_ACCESS = graphql(`
  mutation ValidateGitAccess(
    $branch: String!
    $credentials: RootCredentialsInput!
    $organizationId: String
    $url: String!
  ) {
    validateGitAccess(
      branch: $branch
      credentials: $credentials
      organizationId: $organizationId
      url: $url
    ) {
      success
    }
  }
`);

const SEND_NEW_ENROLLED_USER = graphql(`
  mutation SendNewEnrolledUser($groupName: String!) {
    sendNewEnrolledUser(groupName: $groupName) {
      success
    }
  }
`);

export {
  ADD_ENROLLMENT,
  ADD_GIT_ROOT,
  ADD_GROUP,
  ADD_ORGANIZATION,
  GET_STAKEHOLDER_GROUPS,
  SEND_NEW_ENROLLED_USER,
  VALIDATE_GIT_ACCESS,
};
