import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql/error";
import { HttpResponse, type StrictResponse, graphql } from "msw";

import {
  ADD_ENROLLMENT,
  ADD_GIT_ROOT,
  ADD_GROUP,
  ADD_ORGANIZATION,
  GET_STAKEHOLDER_GROUPS,
  SEND_NEW_ENROLLED_USER,
  VALIDATE_GIT_ACCESS,
} from "./queries";

import { Autoenrollment } from ".";
import type {
  AddEnrollmentMutation,
  AddGitRootMutation,
  AddGroupMutation,
  AddOrganizationMutation as AddOrganization,
  GetStakeholderGroupsQuery as GetStakeholderGroups,
  SendNewEnrolledUserMutation,
  ValidateGitAccessMutation,
} from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";
import { msgError, msgSuccess } from "utils/notifications";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

describe("autoenrollment", (): void => {
  it("should render flow to add organization", async (): Promise<void> => {
    expect.hasAssertions();

    const replaceLocation = jest.fn();

    // eslint-disable-next-line functional/immutable-data
    Object.defineProperty(window, "location", {
      value: { ...window.location, replace: replaceLocation },
    });
    render(<Autoenrollment />, {
      memoryRouter: {
        initialEntries: ["/"],
      },
      mocks: [
        graphql.link(LINK).query(
          GET_STAKEHOLDER_GROUPS,
          (): StrictResponse<{ data: GetStakeholderGroups }> => {
            return HttpResponse.json({
              data: {
                me: {
                  __typename: "Me",
                  isPersonalEmail: false,
                  organizations: [],
                  trial: null,
                  userEmail: "jdoe@fluidattacks.com",
                  userName: "John Doe",
                },
              },
            });
          },
          { once: true },
        ),
        graphql.link(LINK).query(
          GET_STAKEHOLDER_GROUPS,
          (): StrictResponse<{ data: GetStakeholderGroups }> => {
            return HttpResponse.json({
              data: {
                me: {
                  __typename: "Me",
                  isPersonalEmail: false,
                  organizations: [],
                  trial: null,
                  userEmail: "jdoe@fluidattacks.com",
                  userName: "John Doe",
                },
              },
            });
          },
          { once: true },
        ),
        graphql.link(LINK).mutation(
          ADD_ORGANIZATION,
          (): StrictResponse<IErrorMessage | { data: AddOrganization }> => {
            return HttpResponse.json({
              data: {
                addOrganization: {
                  organization: {
                    awsExternalId: "6e52c11c-abf7-4ca3-b7d0-635e394f41c1",
                    id: "ORG#eb50af04-4d50-4e40-bab1-a3fe9f672f9d",
                    name: "esdeath",
                  },
                  success: true,
                },
              },
            });
          },
          { once: true },
        ),
        graphql.link(LINK).query(
          GET_STAKEHOLDER_GROUPS,
          (): StrictResponse<{ data: GetStakeholderGroups }> => {
            return HttpResponse.json({
              data: {
                me: {
                  __typename: "Me",
                  isPersonalEmail: false,
                  organizations: [
                    {
                      awsExternalId: "6e52c11c-abf7-4ca3-b7d0-635e394f41c1",
                      country: "Colombia",
                      groups: [],
                      id: "ORG#eb50af04-4d50-4e40-bab1-a3fe9f672f9d",
                      name: "esdeath",
                    },
                  ],
                  trial: null,
                  userEmail: "jdoe@fluidattacks.com",
                  userName: "John Doe",
                },
              },
            });
          },
          { once: true },
        ),
        graphql.link(LINK).mutation(
          VALIDATE_GIT_ACCESS,
          (): StrictResponse<
            IErrorMessage | { data: ValidateGitAccessMutation }
          > => {
            return HttpResponse.json({
              data: {
                validateGitAccess: {
                  success: true,
                },
              },
            });
          },
          { once: true },
        ),
        graphql.link(LINK).query(
          GET_STAKEHOLDER_GROUPS,
          (): StrictResponse<{ data: GetStakeholderGroups }> => {
            return HttpResponse.json({
              data: {
                me: {
                  __typename: "Me",
                  isPersonalEmail: false,
                  organizations: [
                    {
                      awsExternalId: "6e52c11c-abf7-4ca3-b7d0-635e394f41c1",
                      country: "Colombia",
                      groups: [],
                      id: "ORG#eb50af04-4d50-4e40-bab1-a3fe9f672f9d",
                      name: "esdeath",
                    },
                  ],
                  trial: null,
                  userEmail: "jdoe@fluidattacks.com",
                  userName: "John Doe",
                },
              },
            });
          },
          { once: true },
        ),
        graphql.link(LINK).mutation(
          ADD_GROUP,
          ({
            variables,
          }): StrictResponse<IErrorMessage | { data: AddGroupMutation }> => {
            const { description, hasEssential, hasAdvanced, organizationName } =
              variables;
            if (
              description === "Trial group" &&
              hasEssential &&
              !hasAdvanced &&
              organizationName === "ESDEATH"
            ) {
              return HttpResponse.json({
                data: {
                  addGroup: {
                    success: true,
                  },
                },
              });
            }

            return HttpResponse.json({
              errors: [new GraphQLError("Error adding group")],
            });
          },
          { once: true },
        ),
        graphql.link(LINK).mutation(
          ADD_GIT_ROOT,
          ({
            variables,
          }): StrictResponse<IErrorMessage | { data: AddGitRootMutation }> => {
            const { url } = variables;
            if (url === "ssh://git@gitlab.com:fluidattacks/universe.git") {
              return HttpResponse.json({
                data: {
                  addGitRoot: {
                    success: true,
                  },
                },
              });
            }

            return HttpResponse.json({
              errors: [new GraphQLError("Error adding git root")],
            });
          },
          { once: true },
        ),
        graphql.link(LINK).mutation(
          ADD_ENROLLMENT,
          (): StrictResponse<
            IErrorMessage | { data: AddEnrollmentMutation }
          > => {
            return HttpResponse.json({
              data: {
                addEnrollment: {
                  success: true,
                },
              },
            });
          },
          { once: true },
        ),
        graphql.link(LINK).mutation(
          SEND_NEW_ENROLLED_USER,
          ({
            variables,
          }): StrictResponse<
            IErrorMessage | { data: SendNewEnrolledUserMutation }
          > => {
            const { groupName } = variables;

            if (groupName === "esdeath") {
              return HttpResponse.json({
                data: {
                  sendNewEnrolledUser: {
                    success: true,
                  },
                },
              });
            }

            return HttpResponse.json({
              errors: [
                new GraphQLError("Error sending enrolled user git root"),
              ],
            });
          },
          { once: true },
        ),
      ],
    });

    await waitFor((): void => {
      expect(
        screen.queryByText("autoenrollment.fastTrack.manual.button"),
      ).toBeInTheDocument();
    });

    await userEvent.click(
      screen.getByText("autoenrollment.fastTrack.manual.button"),
    );

    await waitFor((): void => {
      expect(screen.queryByText("autoenrollment.title")).toBeInTheDocument();
    });

    await userEvent.click(screen.getByText("autoenrollment.goBack"));

    await waitFor((): void => {
      expect(
        screen.queryByText("autoenrollment.fastTrack.manual.button"),
      ).toBeInTheDocument();
    });
    await userEvent.click(
      screen.getByText("autoenrollment.fastTrack.manual.button"),
    );
    await waitFor((): void => {
      expect(screen.queryByText("autoenrollment.title")).toBeInTheDocument();
    });

    await userEvent.type(
      screen.getByRole("combobox", { name: "url" }),
      "git@gitlab.com:fluidattacks/universe.git",
    );

    await userEvent.type(
      screen.getByRole("combobox", { name: "branch" }),
      "main",
    );

    await waitFor((): void => {
      expect(screen.getByText("SSH")).toBeInTheDocument();
    });

    await userEvent.type(
      screen.getByRole("combobox", { name: "credentials.name" }),
      "credential name",
    );

    await userEvent.type(
      screen.getByRole("textbox", { name: "credentials.key" }),
      "-----BEGIN OPENSSH PRIVATE KEY-----\ntest\n-----END OPENSSH PRIVATE KEY-----",
    );
    await userEvent.click(
      screen.getByRole("radio", {
        name: "components.oauthRootForm.steps.s3.exclusions.radio.no.label",
      }),
    );
    await waitFor((): void => {
      expect(screen.getByText("autoenrollment.checkAccess")).not.toBeDisabled();
    });
    await userEvent.click(screen.getByText("autoenrollment.checkAccess"));

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "autoenrollment.messages.accessChecked.body",
        "autoenrollment.messages.accessChecked.title",
      );
    });

    await waitFor((): void => {
      expect(screen.queryByText("autoenrollment.submit")).toBeInTheDocument();
    });

    await userEvent.click(screen.getByText("autoenrollment.submit"));

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "organization.tabs.groups.newGroup.success",
        "organization.tabs.groups.newGroup.titleSuccess",
      );
    });

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "autoenrollment.messages.success.body",
        "autoenrollment.messages.success.title",
      );
    });

    expect(replaceLocation).toHaveBeenCalledWith(
      "/orgs/esdeath/groups/esdeath/vulns",
    );
  });

  it("should render error adding data", async (): Promise<void> => {
    expect.hasAssertions();

    render(<Autoenrollment />, {
      memoryRouter: {
        initialEntries: ["/"],
      },
      mocks: [
        graphql.link(LINK).query(
          GET_STAKEHOLDER_GROUPS,
          (): StrictResponse<{ data: GetStakeholderGroups }> => {
            return HttpResponse.json({
              data: {
                me: {
                  __typename: "Me",
                  isPersonalEmail: false,
                  organizations: [],
                  trial: null,
                  userEmail: "jdoe@fluidattacks.com",
                  userName: "John Doe",
                },
              },
            });
          },
          { once: true },
        ),
        graphql.link(LINK).query(
          GET_STAKEHOLDER_GROUPS,
          (): StrictResponse<{ data: GetStakeholderGroups }> => {
            return HttpResponse.json({
              data: {
                me: {
                  __typename: "Me",
                  isPersonalEmail: false,
                  organizations: [],
                  trial: null,
                  userEmail: "jdoe@fluidattacks.com",
                  userName: "John Doe",
                },
              },
            });
          },
          { once: true },
        ),
        graphql.link(LINK).mutation(
          ADD_ORGANIZATION,
          (): StrictResponse<IErrorMessage | { data: AddOrganization }> => {
            return HttpResponse.json({
              errors: [
                new GraphQLError(
                  "Exception - Only corporate emails are allowed",
                ),
              ],
            });
          },
          { once: true },
        ),
      ],
    });

    await waitFor((): void => {
      expect(
        screen.queryByText("autoenrollment.fastTrack.manual.button"),
      ).toBeInTheDocument();
    });

    await userEvent.click(
      screen.getByText("autoenrollment.fastTrack.manual.button"),
    );

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        "Only corporate emails are allowed",
      );
    });
    await waitFor((): void => {
      expect(
        screen.queryByText("autoenrollment.title"),
      ).not.toBeInTheDocument();
    });
  });
});
