import { styled } from "styled-components";

const RepoButton = styled.div`
  position: relative;
  display: flex;
  border: 1px solid ${({ theme }): string => theme.palette.gray[200]};
  border-radius: 4px;
  align-items: center;
  background-color: #fff;
  margin: 12px;
  justify-content: center;
  width: 243px;
  height: 76px;
  padding: ${({ theme }): string => theme.spacing[1]};
  user-select: none;

  &:hover {
    box-shadow: ${({ theme }): string => theme.shadows.sm};
  }

  @media (width <= 768px) {
    width: 100%;
    margin-bottom: 18px;
  }
`;

const StyledRadio = styled.input.attrs({ type: "radio" })`
  position: absolute;
  top: ${({ theme }): string => theme.spacing[1]};
  left: ${({ theme }): string => theme.spacing[1]};
  border: 2px solid white;
  box-shadow: 0 0 0 1px #dddde3;
  appearance: none;
  border-radius: 50%;
  width: 12px;
  height: 12px;
  background-color: #fff;

  &[checked] {
    transition: all 100ms;
    background-color: #bf0b1a;
    box-shadow: 0 0 0 1px #bf0b1a;
  }
`;

const StyledRedLink = styled.a`
  color: ${({ theme }): string => theme.palette.primary[500]};
  cursor: pointer;
  text-decoration: underline;
`;

const StyledLink = styled.a.attrs({
  rel: "noopener noreferrer",
  target: "_blank",
})`
  color: ${({ theme }): string => theme.palette.gray[700]};
  cursor: pointer;
  text-decoration: underline;
`;

export { StyledRedLink, RepoButton, StyledRadio, StyledLink };
