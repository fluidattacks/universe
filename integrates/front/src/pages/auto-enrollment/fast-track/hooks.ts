import type { ApolloQueryResult } from "@apollo/client";
import { useLazyQuery, useMutation } from "@apollo/client";
import mixpanel from "mixpanel-browser";
import { useCallback, useState } from "react";

import type { GetStakeholderGroupsQuery } from "gql/graphql";
import {
  ADD_ORGANIZATION,
  GET_STAKEHOLDER_GROUPS,
} from "pages/auto-enrollment/queries";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";
import { translate } from "utils/translations/translate";
import { generateWord } from "utils/word-generator";

interface IAddOrganizationNewUser {
  addNewOrganization: () => Promise<boolean>;
  notLoading: boolean;
}
const MAX_RANDOM_MULTIPLIER = 3;
const useAddOrganizationNewUser = (
  refetch: () => Promise<ApolloQueryResult<GetStakeholderGroupsQuery>>,
): IAddOrganizationNewUser => {
  const [notLoading, setNotLoading] = useState(true);
  const [getStakeholderData] = useLazyQuery(GET_STAKEHOLDER_GROUPS, {
    onError: (error): void => {
      error.graphQLErrors.forEach(({ message }): void => {
        Logger.error("An error occurred loading stakeholder groups", message);
      });
    },
  });

  const [addOrganization] = useMutation(ADD_ORGANIZATION, {
    onError: (error): void => {
      error.graphQLErrors.forEach(({ message }): void => {
        if (message === "Access denied") {
          msgError(translate.t("sidebar.newOrganization.modal.invalidName"));
        } else if (message.includes("organization name is already taken")) {
          const auxName = generateWord(
            Math.floor(Math.random() * MAX_RANDOM_MULTIPLIER) + 4,
          );
          void addOrganization({
            variables: {
              name: auxName,
            },
          });
        } else if (
          message === "Exception - Only corporate emails are allowed"
        ) {
          msgError(
            translate.t(
              "autoenrollment.fastTrackDesktop.manual.organization.onlyCorporate",
            ),
          );
          Logger.error(
            "An error occurred creating new organization from enrollment",
            message,
          );
        } else {
          msgError(translate.t("groupAlerts.errorTextsad"));
          Logger.error(
            "An error occurred creating new organization from enrollment",
            message,
          );
        }
      });
    },
  });

  const addNewOrganization = useCallback(async (): Promise<boolean> => {
    setNotLoading(false);
    const stakeholder = await getStakeholderData();
    if ((stakeholder.data?.me.organizations[0]?.id ?? "") !== "") {
      return true;
    }
    try {
      mixpanel.track("AddOrganization");
      const response = await addOrganization({
        variables: {
          name: generateWord(
            Math.floor(Math.random() * MAX_RANDOM_MULTIPLIER) + 4,
          ).toUpperCase(),
        },
      });
      const orgResult = response.data;
      await refetch();
      setNotLoading(true);

      return orgResult ? orgResult.addOrganization.success : false;
    } catch {
      return false;
    }
  }, [addOrganization, getStakeholderData, refetch]);

  return { addNewOrganization, notLoading };
};

export { useAddOrganizationNewUser };
