import { styled } from "styled-components";

const StyledProgressBar = styled.div<{ progress: number }>`
  position: relative;
  width: 100%;
  height: 4px;

  &::before {
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    width: ${({ progress }): string => `${progress}%`};
    height: 100%;
    background-color: ${({ theme }): string => theme.palette.primary[500]};
  }
`;

export { StyledProgressBar };
