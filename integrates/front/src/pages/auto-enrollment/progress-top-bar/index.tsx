import { Avatar, Container } from "@fluidattacks/design";
import { useTheme } from "styled-components";

import { StyledProgressBar } from "./styles";
import type { IProgressTopBarProps } from "./types";

import { InlineLogo } from "components/inline-logo";

const ProgressTopBar = ({
  userName,
  progress,
}: Readonly<IProgressTopBarProps>): JSX.Element => {
  const theme = useTheme();

  return (
    <Container>
      <Container
        alignItems={"center"}
        bgColor={theme.palette.white}
        borderBottom={`1px solid ${theme.palette.gray[300]}`}
        display={"flex"}
        justify={"space-between"}
        pb={0.75}
        pl={1.5}
        pr={1.5}
        pt={0.75}
      >
        <Container alignItems={"center"} display={"flex"}>
          <InlineLogo />
          <Container
            alignItems={"center"}
            borderLeft={`1px solid ${theme.palette.gray[200]}`}
            display={"flex"}
            ml={0.5}
            pl={0.5}
          />
        </Container>
        <Container alignItems={"center"} display={"flex"}>
          <Avatar showUsername={true} userName={userName} />
        </Container>
      </Container>
      <StyledProgressBar progress={progress} />
    </Container>
  );
};

export { ProgressTopBar };
