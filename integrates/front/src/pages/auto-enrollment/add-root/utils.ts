/* eslint-disable @typescript-eslint/no-invalid-void-type */
import type { FormikErrors } from "formik";
import isEmpty from "lodash/isEmpty";

import type { IRootAttr } from "../types";
import { checkCodeCommitFormat } from "../utils";

const cleanCredentialsValues = (
  setFieldValue: (
    field: string,
    value: boolean | string,
    shouldValidate?: boolean,
  ) => Promise<FormikErrors<IRootAttr> | void>,
): void => {
  void setFieldValue("credentials.name", "", false);
  void setFieldValue("credentials.id", "", false);
  void setFieldValue("credentials.password", "", false);
  void setFieldValue("credentials.token", "", false);
  void setFieldValue("credentials.user", "", false);
  void setFieldValue("credentials.key", "", false);
  void setFieldValue("credentials.azureOrganization", "", false);
  void setFieldValue("credentials.type", "", false);
  void setFieldValue("credentials.typeCredential", "", false);
  void setFieldValue("credentials.arn", "", false);
};

const chooseCredentialType = (
  url: string,
  setFieldValue: (
    field: string,
    value: boolean | string,
    shouldValidate?: boolean,
  ) => Promise<FormikErrors<IRootAttr> | void>,
): void => {
  const httpsRegex = /^https?:\/\/(?:(?!\b.*dev\.azure\.com\b)).*/u;
  const azureRegex = /.*(?:dev\.azure\.com).*/u;
  const sshRegex = /^(?:ssh|git@).*/u;
  if (azureRegex.test(url)) {
    cleanCredentialsValues(setFieldValue);
    void setFieldValue("credentials.typeCredential", "TOKEN", false);
    void setFieldValue("credentials.type", "HTTPS", false);
    void setFieldValue("credentials.auth", "TOKEN", false);
    void setFieldValue("credentials.isPat", true, false);
  }

  if (httpsRegex.test(url)) {
    cleanCredentialsValues(setFieldValue);
    void setFieldValue("credentials.typeCredential", "USER", false);
    void setFieldValue("credentials.type", "HTTPS", false);
    void setFieldValue("credentials.auth", "USER", false);
    void setFieldValue("credentials.isPat", false, false);
  }

  if (sshRegex.test(url)) {
    cleanCredentialsValues(setFieldValue);
    void setFieldValue("credentials.typeCredential", "SSH", false);
    void setFieldValue("credentials.type", "SSH", false);
    void setFieldValue("credentials.auth", "", false);
    void setFieldValue("credentials.isPat", false, false);
  }

  if (checkCodeCommitFormat(url)) {
    cleanCredentialsValues(setFieldValue);
    void setFieldValue("credentials.typeCredential", "AWSROLE", false);
    void setFieldValue("credentials.type", "AWSROLE", false);
    void setFieldValue("credentials.auth", "", false);
    void setFieldValue("credentials.isPat", false, false);
  }

  if (
    !azureRegex.test(url) &&
    !httpsRegex.test(url) &&
    !sshRegex.test(url) &&
    !checkCodeCommitFormat(url)
  ) {
    cleanCredentialsValues(setFieldValue);
  }
};

const submittableCredentials = (values: IRootAttr): boolean => {
  if (
    values.credentials.typeCredential === "AWSROLE" &&
    (!values.credentials.name || !values.credentials.arn || !values.branch)
  ) {
    return true;
  }
  if (
    values.credentials.typeCredential === "SSH" &&
    (!values.credentials.name || !values.credentials.key || !values.branch)
  ) {
    return true;
  }
  if (
    values.credentials.typeCredential === "USER" &&
    (!values.credentials.name ||
      !values.credentials.user ||
      !values.credentials.password ||
      !values.branch)
  ) {
    return true;
  }
  if (
    values.credentials.typeCredential === "TOKEN" &&
    (!values.credentials.name ||
      !values.credentials.token ||
      isEmpty(values.credentials.azureOrganization) ||
      !values.branch)
  ) {
    return true;
  }

  return false;
};

export { chooseCredentialType, submittableCredentials };
