import type { IRootAttr } from "pages/auto-enrollment/types";

interface ICredentialsSectionProps {
  accessChecked: boolean;
  externalId: string;
  repoUrl: string;
}

interface ICredentialsTypeProps {
  accessChecked: boolean;
  externalId: string;
  values: IRootAttr;
}

export type { ICredentialsTypeProps, ICredentialsSectionProps };
