import { Heading } from "@fluidattacks/design";
import { useFormikContext } from "formik";
import isEmpty from "lodash/isEmpty";
import { useCallback } from "react";
import * as React from "react";
import type { FC } from "react";
import { useTranslation } from "react-i18next";

import { CredentialsType } from "./credentials-type";
import type { ICredentialsSectionProps } from "./types";

import type { IRootAttr } from "../../../types";
import { StyledExpandedCell } from "../../styles";
import { Input, Select } from "components/input";

const CredentialsSection: FC<ICredentialsSectionProps> = ({
  accessChecked,
  externalId,
  repoUrl,
}): JSX.Element => {
  const { t } = useTranslation();
  const { setFieldValue, values } = useFormikContext<IRootAttr>();

  const onTypeChange = useCallback(
    (event: React.ChangeEvent<HTMLSelectElement>): void => {
      event.preventDefault();
      if (event.target.value === "SSH") {
        void setFieldValue("credentials.type", "SSH");
        void setFieldValue("credentials.auth", "");
        void setFieldValue("credentials.isPat", false);
      }
      if (event.target.value === "") {
        void setFieldValue("type", "");
        void setFieldValue("auth", "");
        void setFieldValue("isPat", false);
      }
      if (event.target.value === "USER") {
        void setFieldValue("credentials.type", "HTTPS");
        void setFieldValue("credentials.auth", "USER");
        void setFieldValue("credentials.isPat", false);
      }
      if (event.target.value === "TOKEN") {
        void setFieldValue("credentials.type", "HTTPS");
        void setFieldValue("credentials.auth", "TOKEN");
        void setFieldValue("credentials.isPat", true);
      }
      if (event.target.value === "AWSROLE") {
        void setFieldValue("credentials.type", "AWSROLE");
        void setFieldValue("credentials.auth", "AWSROLE");
        void setFieldValue("credentials.isPat", false);
      }
    },
    [setFieldValue],
  );

  return (
    <React.Fragment>
      <StyledExpandedCell>
        <Heading fontWeight={"bold"} mb={2} size={"md"}>
          {t("autoenrollment.credentials.title")}
        </Heading>
      </StyledExpandedCell>
      <Select
        disabled={true}
        items={[
          {
            label: "",
            value: "",
          },
          {
            label: t("autoenrollment.credentials.ssh"),
            value: "SSH",
          },
          {
            label: t("autoenrollment.credentials.userHttps"),
            value: "USER",
          },
          {
            label: t("autoenrollment.credentials.azureToken"),
            value: "TOKEN",
          },
          {
            label: t("autoenrollment.credentials.awsRole"),
            value: "AWSROLE",
          },
        ]}
        label={t("autoenrollment.credentials.type.label")}
        name={"credentials.typeCredential"}
        onChange={onTypeChange}
        required={true}
        tooltip={t("autoenrollment.credentials.type.toolTip")}
      />
      <Input
        disabled={accessChecked}
        label={t("autoenrollment.credentials.name.label")}
        name={"credentials.name"}
        required={true}
        tooltip={t("autoenrollment.credentials.name.toolTip")}
      />
      {isEmpty(repoUrl) &&
      isEmpty(values.credentials.typeCredential) ? undefined : (
        <CredentialsType
          accessChecked={accessChecked}
          externalId={externalId}
          values={values}
        />
      )}
    </React.Fragment>
  );
};

export { CredentialsSection };
