import { Container, Heading, Icon, Text } from "@fluidattacks/design";
import { FieldArray, useFormikContext } from "formik";
import type { FC } from "react";
import { useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import {
  StyledAlert,
  StyledIconBox,
  StyledInputsWrapper,
  StyledRadioGroup,
} from "./styles";

import type { IRootAttr } from "../../../types";
import { InputArray } from "components/input";
import { RadioButton } from "components/radio-button";
import { openUrl } from "utils/resource-helpers";

interface IExclusionsProps {
  readonly setRootMessages: React.Dispatch<
    React.SetStateAction<{
      message: string;
      type: string;
    }>
  >;
}

const Exclusions: FC<Readonly<IExclusionsProps>> = ({
  setRootMessages,
}): JSX.Element => {
  const { t } = useTranslation();
  const { setFieldValue, values } = useFormikContext<IRootAttr>();

  const goToDocumentation = useCallback((): void => {
    openUrl(
      "https://mirrors.edge.kernel.org/pub/software/scm/git/docs/gitignore.html#_pattern_format",
    );
  }, []);

  const handleRender = useCallback((): JSX.Element => {
    return (
      <Container scroll={"none"}>
        <Text fontWeight={"bold"} mt={1} size={"sm"}>
          {t("components.oauthRootForm.steps.s3.exclusions.exclude")}
          <StyledIconBox>
            <Icon
              clickable={true}
              icon={"circle-info"}
              iconSize={"xs"}
              iconType={"fa-light"}
              ml={0.25}
              onClick={goToDocumentation}
            />
          </StyledIconBox>
        </Text>
        <StyledAlert>{t("autoenrollment.exclusions.warning")}</StyledAlert>
        <StyledInputsWrapper>
          <InputArray
            addButtonText={t("autoenrollment.exclusions.addExclusion")}
            name={`exclusions`}
          />
        </StyledInputsWrapper>
      </Container>
    );
  }, [goToDocumentation, t]);

  const onChangeHasExclusions = useCallback((): void => {
    void setFieldValue("exclusions", []);
    setRootMessages({ message: "", type: "" });
  }, [setFieldValue, setRootMessages]);

  return (
    <React.Fragment>
      <Heading fontWeight={"bold"} mb={2} mt={1} size={"md"}>
        {"Configurations"}
      </Heading>
      <Text mb={0} size={"sm"}>
        {t("autoenrollment.exclusions.label")}
      </Text>
      <StyledRadioGroup>
        <RadioButton
          defaultChecked={values.hasExclusions === "yes"}
          label={t(
            "components.oauthRootForm.steps.s3.exclusions.radio.yes.label",
          )}
          name={"hasExclusions"}
          onClick={onChangeHasExclusions}
          required={true}
          value={t(
            "components.oauthRootForm.steps.s3.exclusions.radio.yes.value",
          )}
          variant={"formikField"}
        />
        <RadioButton
          defaultChecked={values.hasExclusions === "no"}
          label={t(
            "components.oauthRootForm.steps.s3.exclusions.radio.no.label",
          )}
          name={"hasExclusions"}
          onClick={onChangeHasExclusions}
          required={true}
          value={t(
            "components.oauthRootForm.steps.s3.exclusions.radio.no.value",
          )}
          variant={"formikField"}
        />
      </StyledRadioGroup>
      {values.hasExclusions === "yes" ? (
        <FieldArray name={"exclusions"} render={handleRender} />
      ) : undefined}
    </React.Fragment>
  );
};

export { Exclusions };
