import { Alert } from "@fluidattacks/design";
import type { IContainerProps } from "@fluidattacks/design/dist/components/container/types";
import { styled } from "styled-components";

import { Checkbox } from "components/checkbox";

const StyledRadioGroup = styled.div`
  display: flex;

  > * {
    margin-top: 0.5rem;
    margin-right: 1rem;
  }
`;

const StyledAlert = styled(Alert).attrs<IContainerProps>({
  my: 0.625,
})``;

const StyledCheckbox = styled(Checkbox)`
  margin-bottom: 0.5rem;
`;

const StyledIconBox = styled.span`
  display: inline-block;
  vertical-align: text-bottom;
`;

const StyledRepoContainer = styled.div`
  overflow: hidden;
`;

const StyledInputsWrapper = styled.div`
  display: block;
  margin-bottom: ${({ theme }): string => theme.spacing[2]};
`;

export {
  StyledRadioGroup,
  StyledAlert,
  StyledCheckbox,
  StyledIconBox,
  StyledRepoContainer,
  StyledInputsWrapper,
};
