import { useQuery } from "@apollo/client";
import { useMemo } from "react";

import type { IGetStakeholderGroupsQueryResult } from "./types";

import { GET_STAKEHOLDER_GROUPS } from "../queries";
import { Logger } from "utils/logger";
import { generateWord } from "utils/word-generator";

const useGetStakeholderGroupsQuery = (): IGetStakeholderGroupsQueryResult => {
  const MAX_RANDOM_FACTOR = 3;
  const defaultOrgName = useMemo((): string => {
    return generateWord(Math.floor(Math.random() * MAX_RANDOM_FACTOR) + 4);
  }, []);

  const { data, error, loading, refetch } = useQuery(GET_STAKEHOLDER_GROUPS, {
    onError: (err): void => {
      err.graphQLErrors.forEach(({ message }): void => {
        Logger.error("An error occurred loading stakeholder groups", message);
      });
    },
  });

  const newData: IGetStakeholderGroupsQueryResult["data"] = {
    me: {
      group: {
        description: "",
        name: data?.me.organizations[0]?.groups?.[0]?.name ?? "",
      },
      isPersonalEmail: data?.me.isPersonalEmail ?? false,
      organization: {
        awsExternalId: data?.me.organizations[0]?.awsExternalId ?? "",
        country: data?.me.organizations[0]?.country ?? "",
        id: data?.me.organizations[0]?.id ?? "",
        name: data?.me.organizations[0]?.name ?? defaultOrgName,
      },
      organizationValues: {
        groupName: data?.me.organizations[0]?.groups?.[0]?.name ?? "",
        organizationCountry: data?.me.organizations[0]?.country ?? "",
        organizationName: data?.me.organizations[0]?.name ?? defaultOrgName,
      },
      trial: data?.me.trial ?? null,
      userEmail: data?.me.userEmail ?? "",
      userName: data?.me.userName ?? "",
    },
  };

  return {
    data: newData,
    error,
    loading,
    refetch,
  };
};

export { useGetStakeholderGroupsQuery };
