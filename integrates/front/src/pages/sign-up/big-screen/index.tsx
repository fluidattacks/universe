import {
  CloudImage,
  Container,
  Divider,
  Heading,
  Icon,
  Modal,
  Text,
  useModal,
} from "@fluidattacks/design";
import React, { useCallback, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import { useTheme } from "styled-components";

import { Slider } from "./slider";
import {
  AdvertisementContent,
  AdvertisementWrapper,
  FormWrapper,
  SignupGrid,
  StyledLink,
  StyledList,
} from "./styles";

import { ModalConfirm } from "components/modal";
import { useCloudinaryImage } from "hooks/use-cloudinary-image";
import { LoginProviders } from "pages/login/big-screen/provider-form/login-providers";

const BigScreenSignup: React.FC = (): JSX.Element => {
  const { t } = useTranslation();
  const [errorType, setErrorType] = useState("");
  const backgroundImage = useCloudinaryImage({
    publicId: "integrates/login/bannerLogin",
  });
  const theme = useTheme();
  const navigate = useNavigate();
  const modalRef = useModal("signup-error-modal");
  const listItems = [
    "Continuous security testing through AppSec tools (SAST, DAST, SCA and CSPM)",
    "A single pane of glass",
    "Help for vulnerability remediation",
  ];

  useEffect((): void => {
    const searchParams = new URLSearchParams(location.search);
    const error = searchParams.get("error") ?? false;
    if (
      error !== false &&
      ["invalidEmail", "existingDomain", "previousTrial"].includes(error)
    ) {
      setErrorType(error);
      modalRef.open();
    }
  }, [modalRef]);

  const handleClose = useCallback((): void => {
    modalRef.close();
    navigate(location.pathname, { replace: true });
  }, [modalRef, navigate]);

  return (
    <SignupGrid>
      <FormWrapper>
        <Container
          display={"flex"}
          flexDirection={"column"}
          gap={3}
          maxWidth={"388px"}
        >
          <Container
            alignItems={"center"}
            display={"flex"}
            flexDirection={"column"}
          >
            <CloudImage
              alt={"Fluid Attacks Logo"}
              publicId={"integrates/login/loginLogoExtended"}
              width={"220px"}
            />
            <Text
              color={theme.palette.gray[400]}
              mt={2.25}
              size={"md"}
              textAlign={"center"}
            >
              {"FREE TRIAL"}
            </Text>
            <Heading
              color={theme.palette.gray[800]}
              fontWeight={"bold"}
              lineSpacing={2.5}
              mt={0.5}
              size={"lg"}
              textAlign={"center"}
            >
              {"Create your account with "}
              {"your work email"}
            </Heading>
            <Text
              color={theme.palette.gray[500]}
              lineSpacing={1.75}
              mt={0.25}
              size={"xl"}
              textAlign={"center"}
            >
              {"Already have an account?  "}
              <StyledLink color={theme.palette.primary[500]} to={"/"}>
                {"Log in"}
              </StyledLink>
            </Text>
          </Container>
          <LoginProviders section={"Signup"} />

          <Text
            color={theme.palette.gray[400]}
            size={"sm"}
            textAlign={"center"}
          >
            <StyledLink to={"https://fluidattacks.com/terms-use/"}>
              {t("login.footer.term1")}
            </StyledLink>
            {t("login.footer.term2")}
            <StyledLink to={"https://fluidattacks.com/privacy/"}>
              {t("login.footer.term3")}
            </StyledLink>
            <br />
            {t("login.footer.copyright")}
          </Text>
        </Container>
      </FormWrapper>
      <AdvertisementWrapper url={backgroundImage.toURL()}>
        <AdvertisementContent>
          <Heading
            color={"white"}
            fontWeight={"bold"}
            lineSpacing={2.5}
            size={"lg"}
            wordBreak={"break-word"}
          >
            {"Develop and deploy secure software without delays."}
          </Heading>
          <Text
            color={"white"}
            fontWeight={"bold"}
            lineSpacing={1.75}
            mb={2.25}
            mt={0.5}
            size={"xl"}
            wordBreak={"break-word"}
          >
            {
              "Try Continuous Hacking free for 21 days. No credit card required."
            }
          </Text>
          <StyledList>
            {listItems.map((item, index): JSX.Element => {
              const key = `item-${index}`;

              return (
                <li key={key}>
                  <Icon
                    icon={"circle-check"}
                    iconColor={theme.palette.gray[100]}
                    iconSize={"xs"}
                  />
                  <Text
                    color={theme.palette.gray[100]}
                    lineSpacing={1.5}
                    size={"md"}
                  >
                    {item}
                  </Text>
                </li>
              );
            })}
          </StyledList>
          <Divider color={theme.palette.gray[600]} mb={1.25} mt={1.5} />
          <Slider />
        </AdvertisementContent>
      </AdvertisementWrapper>
      <Modal
        modalRef={{ ...modalRef, close: handleClose }}
        size={"md"}
        title={t(`signup.modal.${errorType}.title`)}
      >
        {t(`signup.modal.${errorType}.description`)}
        <ModalConfirm
          onConfirm={handleClose}
          txtConfirm={t(`signup.modal.${errorType}.button`)}
        />
      </Modal>
    </SignupGrid>
  );
};

export { BigScreenSignup };
