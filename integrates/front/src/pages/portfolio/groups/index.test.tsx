import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import { Route, Routes } from "react-router-dom";

import { PORTFOLIO_GROUPS_QUERY } from "./queries";

import { PortfolioGroups } from ".";
import type { GetPortfolioGroupsQuery as GetPortfolioGroups } from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";
import { msgError } from "utils/notifications";

const mockNavigatePush: jest.Mock = jest.fn();
jest.mock(
  "react-router-dom",
  (): Record<string, unknown> => ({
    ...jest.requireActual("react-router-dom"),
    useNavigate: (): jest.Mock => mockNavigatePush,
  }),
);
jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();

  return mockedNotifications;
});

describe("portfolio Groups", (): void => {
  const graphqlMocked = graphql.link(LINK);
  const mockedResult: { description: string; name: string }[] = [
    {
      description: "test1 description",
      name: "test1",
    },
    {
      description: "test2 description",
      name: "test2",
    },
  ];

  it("should render a component", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route
          element={<PortfolioGroups organizationId={""} />}
          path={"/orgs/:organizationName/portfolios/:tagName/groups"}
        />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/portfolios/test-projects/groups"],
        },
        mocks: [
          graphqlMocked.query(
            PORTFOLIO_GROUPS_QUERY,
            (): StrictResponse<{ data: GetPortfolioGroups }> => {
              return HttpResponse.json({
                data: {
                  tag: {
                    groups: mockedResult,
                    name: "test-projects",
                  },
                },
              });
            },
          ),
        ],
      },
    );

    await waitFor((): void => {
      expect(screen.queryAllByRole("table")).toHaveLength(1);
    });

    await userEvent.click(screen.getByText("Test1"));

    expect(mockNavigatePush).toHaveBeenCalledWith("/groups/test1/analytics");
  });

  it("should filter by search", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route
          element={<PortfolioGroups organizationId={""} />}
          path={"/orgs/:organizationName/portfolios/:tagName/groups"}
        />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/portfolios/test-projects/groups"],
        },
        mocks: [
          graphqlMocked.query(
            PORTFOLIO_GROUPS_QUERY,
            (): StrictResponse<{ data: GetPortfolioGroups }> => {
              return HttpResponse.json({
                data: {
                  tag: {
                    groups: mockedResult,
                    name: "test-projects",
                  },
                },
              });
            },
          ),
        ],
      },
    );

    await waitFor((): void => {
      expect(screen.queryAllByRole("table")).toHaveLength(1);
    });

    const searchField = screen.getByPlaceholderText(
      "organization.portfolios.group.searchPlaceholder",
    );

    await userEvent.type(searchField, "test2");

    await waitFor((): void => {
      expect(screen.queryByText("test2 description")).toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(screen.queryByText("test1 description")).not.toBeInTheDocument();
    });
  });

  it("should render an error in component", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route
          element={<PortfolioGroups organizationId={""} />}
          path={"/orgs/:organizationName/portfolios/:tagName/groups"}
        />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/portfolios/another-tag/groups"],
        },
        mocks: [
          graphqlMocked.query(
            PORTFOLIO_GROUPS_QUERY,
            (): StrictResponse<IErrorMessage> => {
              return HttpResponse.json({
                errors: [new GraphQLError("Error getting portfolio")],
              });
            },
          ),
        ],
      },
    );

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith("groupAlerts.errorTextsad");
    });
  });
});
