import { useQuery } from "@apollo/client";
import { Container, Row } from "@fluidattacks/design";
import type { ColumnDef, Row as TableRow } from "@tanstack/react-table";
import _, { capitalize } from "lodash";
import { useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useNavigate, useParams } from "react-router-dom";

import { PORTFOLIO_GROUPS_QUERY } from "./queries";

import { Table } from "components/table";
import { useTable } from "hooks";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";

interface IPortfolioGroupsProps {
  readonly organizationId: string;
}

const PortfolioGroups: React.FC<IPortfolioGroupsProps> = ({
  organizationId,
}): JSX.Element => {
  const { t } = useTranslation();
  const tableRef = useTable("tblGroupsTag");
  const { tagName } = useParams() as { tagName: string };

  const { data } = useQuery(PORTFOLIO_GROUPS_QUERY, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.error("An error occurred loading tag groups", error);
      });
    },
    variables: { organizationId, tag: tagName },
  });
  const navigate = useNavigate();

  const columns: ColumnDef<{
    description: string | null;
    name: string | null;
  }>[] = [
    {
      accessorKey: "name",
      header: t("organization.portfolios.group.headers.groupName"),
    },
    {
      accessorKey: "description",
      header: t("organization.portfolios.group.headers.description"),
    },
  ];

  const handleRowTagClick = useCallback(
    (
      rowInfo: TableRow<{
        __typename?: "Group";
        description: string | null;
        name: string | null;
      }>,
    ): ((event: React.FormEvent) => void) => {
      return (event: React.FormEvent): void => {
        navigate(`/groups/${rowInfo.original.name?.toLowerCase()}/analytics`);
        event.preventDefault();
      };
    },
    [navigate],
  );

  if (_.isUndefined(data) || _.isEmpty(data)) {
    return <div />;
  }

  return (
    <Container px={1.25}>
      <Row>
        <Table
          columns={columns}
          data={
            data.tag?.groups?.map(
              ({
                name,
                description,
              }): {
                description: string | null;
                name: string;
              } => ({
                description,
                name: capitalize(name),
              }),
            ) ?? []
          }
          onRowClick={handleRowTagClick}
          options={{
            searchPlaceholder: t(
              "organization.portfolios.group.searchPlaceholder",
            ),
          }}
          tableRef={tableRef}
        />
      </Row>
    </Container>
  );
};

export { PortfolioGroups };
