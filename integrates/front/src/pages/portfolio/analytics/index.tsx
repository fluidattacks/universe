import * as React from "react";
import { useLocation, useParams } from "react-router-dom";

import { Charts } from "features/charts";

interface IPortfolioAnalyticsProps {
  readonly organizationId: string;
}

const PortfolioAnalytics: React.FC<IPortfolioAnalyticsProps> = ({
  organizationId,
}): JSX.Element => {
  const { tagName } = useParams() as { tagName: string };
  const location = useLocation();
  const searchParams = new URLSearchParams(location.search);

  const subjectFromSearchParams = searchParams.get("portfolio");
  const subject =
    subjectFromSearchParams ?? `${organizationId}PORTFOLIO#${tagName}`;

  return (
    <React.StrictMode>
      <Charts
        bgChange={searchParams.get("bgChange") === "true"}
        entity={"portfolio"}
        reportMode={searchParams.get("reportMode") === "true"}
        subject={subject}
      />
    </React.StrictMode>
  );
};

export { PortfolioAnalytics };
