import { useContext } from "react";
import * as React from "react";
import { Navigate, Route, Routes } from "react-router-dom";

import { PortfolioAnalytics } from "./analytics";
import { PortfolioGroups } from "./groups";
import { PortfolioTemplate } from "./index.template";

import { organizationDataContext } from "pages/organization/context";
import { useOrgUrl } from "pages/organization/hooks";

const Portfolio: React.FC = (): JSX.Element => {
  const { url } = useOrgUrl("/portfolios/:tagName/*");
  const { organizationId } = useContext(organizationDataContext);

  return (
    <PortfolioTemplate>
      <Routes>
        <Route
          element={<PortfolioAnalytics organizationId={organizationId} />}
          path={`/analytics`}
        />
        <Route
          element={<PortfolioGroups organizationId={organizationId} />}
          path={`/groups`}
        />
        <Route
          element={<Navigate to={`${url}/analytics`.replace("//", "/")} />}
          path={"*"}
        />
      </Routes>
    </PortfolioTemplate>
  );
};

export { Portfolio };
