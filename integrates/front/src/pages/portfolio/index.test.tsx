import { screen, waitFor } from "@testing-library/react";
import { Route, Routes } from "react-router-dom";

import { Portfolio } from ".";
import { render } from "mocks";

describe("portfolio", (): void => {
  it("should render a component", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route
          element={<Portfolio />}
          path={"/orgs/:organizationName/portfolios/:tagName/analytics/*"}
        />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/portfolios/test-projects/analytics"],
        },
      },
    );

    await waitFor((): void => {
      expect(screen.getAllByRole("link")).toHaveLength(2);
    });

    expect(
      screen.getByText("organization.tabs.portfolios.tabs.indicators.text"),
    ).toBeInTheDocument();

    jest.clearAllMocks();
  });
});
