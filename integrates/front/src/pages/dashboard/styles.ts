import { styled } from "styled-components";

const DashboardContainer = styled.div.attrs({
  className: "flex flex-column h-100",
})`
  background-color: #f9fafb;
  color: #2e2e38;
  font-family: Roboto, sans-serif;
  font-size: 14px;
`;

const DashboardContent = styled.main.attrs({
  className: "flex flex-auto flex-column",
})`
  overflow: hidden auto;
  padding: 4px 24px 72px;
`;

export { DashboardContainer, DashboardContent };
