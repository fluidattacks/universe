interface IAWSSubscriptionModalProps {
  onClose: () => void;
}

export type { IAWSSubscriptionModalProps };
