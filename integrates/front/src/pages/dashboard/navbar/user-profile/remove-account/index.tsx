import { Alert } from "@fluidattacks/design";
import { useConfirmDialog } from "@fluidattacks/design";
import { Fragment, useCallback, useContext, useMemo } from "react";
import { useTranslation } from "react-i18next";

import { useGetUserCredentials } from "../hooks";
import type {
  ICredentialWithOrganization,
  IUserCredAndRootsAttr,
} from "../types";
import { Modal } from "components/modal";
import { authContext } from "context/auth";
import { CredManagementModal } from "features/cred-management-modal";
import type { ICredManagementModalDataProps } from "features/cred-management-modal/types";
import type { IUseModal } from "hooks/use-modal";

interface IRemoveAccountProps {
  modalRef: IUseModal;
  onRemove: () => void;
}

const RemoveAccount = ({
  modalRef,
  onRemove,
}: Readonly<IRemoveAccountProps>): JSX.Element => {
  const { userEmail } = useContext(authContext);
  const { t } = useTranslation();
  const { confirm, ConfirmDialog } = useConfirmDialog();
  const [userData] = useGetUserCredentials();
  const handleClick = useCallback(async (): Promise<void> => {
    const confirmResult = await confirm({
      content: (
        <Alert variant={"warning"}>
          {t("navbar.deleteAccount.modal.text")}
        </Alert>
      ),
      title: t("navbar.deleteAccount.text"),
    });
    modalRef.close();
    if (confirmResult) {
      onRemove();
    }
  }, [confirm, modalRef, onRemove, t]);

  const credentialsData = useMemo((): ICredManagementModalDataProps => {
    const filteredCredentials =
      userData?.me.organizations.reduce(
        (
          result: ICredentialWithOrganization[],
          organization: IUserCredAndRootsAttr["me"]["organizations"][0],
        ): ICredentialWithOrganization[] => {
          const organizationName = organization.name;
          const credsWithOrganizationName = organization.credentials
            .filter((credential): boolean => credential.owner === userEmail)
            .map(
              (credential): ICredentialWithOrganization => ({
                ...credential,
                organizationName,
              }),
            );

          return [...result, ...credsWithOrganizationName];
        },
        [],
      ) ?? [];

    if (
      modalRef.isOpen &&
      userData !== undefined &&
      filteredCredentials.length === 0
    ) {
      void handleClick();
    }

    return {
      credentials: filteredCredentials,
      groups:
        userData?.me.organizations.reduce(
          (
            result: ICredManagementModalDataProps["groups"],
            organization: IUserCredAndRootsAttr["me"]["organizations"][0],
          ): ICredManagementModalDataProps["groups"][0][] => [
            ...result,
            ...(organization.groups?.map(
              (group): ICredManagementModalDataProps["groups"][0] => ({
                ...group,
                organizationName: organization.name,
              }),
            ) ?? []),
          ],
          [],
        ) ?? [],
    };
  }, [modalRef.isOpen, handleClick, userData, userEmail]);

  const handleClose = useCallback((): void => {
    modalRef.close();
  }, [modalRef]);

  if (userData === undefined) {
    return (
      <Modal
        cancelButton={{
          onClick: handleClose,
          text: t("components.modal.cancel"),
        }}
        confirmButton={{
          onClick: onRemove,
          text: t("components.modal.confirm"),
        }}
        modalRef={{ ...modalRef, close: handleClose, isOpen: true }}
        size={"sm"}
        title={t("navbar.deleteAccount.text")}
      />
    );
  }

  return (
    <Fragment>
      {credentialsData.credentials.length > 0 && (
        <CredManagementModal
          data={credentialsData}
          onClose={handleClose}
          onContinue={handleClick}
          open={modalRef.isOpen}
          origin={"Me"}
        />
      )}
      <ConfirmDialog />
    </Fragment>
  );
};

export { RemoveAccount };
