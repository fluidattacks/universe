import { Modal, useModal } from "@fluidattacks/design";
import * as React from "react";
import { useTranslation } from "react-i18next";

import type { IChannelSelectorProps } from "./types";

import { Channels } from "features/verify-dialog/channels";

const ChannelSelector: React.FC<IChannelSelectorProps> = ({
  onClose,
  isOpen,
  handleChannelClick,
}): JSX.Element | null => {
  const { t } = useTranslation();
  const modalProps = useModal("channel-selector");

  if (!isOpen) return null;

  return (
    <Modal
      modalRef={{ ...modalProps, close: onClose, isOpen }}
      size={"sm"}
      title={t("verifyDialog.title")}
    >
      <Channels
        isChannelOpen={isOpen}
        onClick={handleChannelClick}
        padding={[0, 0, 0, 0]}
        phone={undefined}
      />
    </Modal>
  );
};

export { ChannelSelector };
