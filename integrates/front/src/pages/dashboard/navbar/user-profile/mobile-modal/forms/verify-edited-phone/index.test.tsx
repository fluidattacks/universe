import { screen } from "@testing-library/react";

import { VerifyEditedPhoneForm } from ".";
import type { IPhoneAttr } from "../../types";
import { render } from "mocks";

describe("edit verified phone modal", (): void => {
  it("should render a form", (): void => {
    expect.hasAssertions();

    const isOpen = true;
    const phone: IPhoneAttr = {
      callingCountryCode: "+1",
      nationalNumber: "123456789",
    };
    const phoneToEdit: IPhoneAttr = {
      callingCountryCode: "+1",
      nationalNumber: "098765432",
    };
    const onSubmit = jest.fn();
    const onCancel = jest.fn();

    jest.spyOn(console, "error").mockImplementation();
    render(
      <VerifyEditedPhoneForm
        isOpen={isOpen}
        onCancel={onCancel}
        onSubmit={onSubmit}
        phone={phone}
        phoneToEdit={phoneToEdit}
      />,
    );

    const inputNewVerificationCode = document.querySelector(
      "input[name='newVerificationCode']",
    );

    expect(
      screen.getByText("profile.mobileModal.fields.phoneNumber"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("profile.mobileModal.fields.newPhoneNumber"),
    ).toBeInTheDocument();
    expect(inputNewVerificationCode).toBeInTheDocument();
    expect(inputNewVerificationCode).toBeEnabled();
  });

  it("should not render the form when isOpen is false", (): void => {
    expect.hasAssertions();

    const phone: IPhoneAttr = {
      callingCountryCode: "+1",
      nationalNumber: "123456789",
    };
    const phoneToEdit: IPhoneAttr = {
      callingCountryCode: "+1",
      nationalNumber: "098765432",
    };

    const onSubmit = jest.fn();
    const onCancel = jest.fn();
    const isOpen = false;

    render(
      <VerifyEditedPhoneForm
        isOpen={isOpen}
        onCancel={onCancel}
        onSubmit={onSubmit}
        phone={phone}
        phoneToEdit={phoneToEdit}
      />,
    );

    expect(
      screen.queryByLabelText("profile.mobileModal.fields.phoneNumber"),
    ).not.toBeInTheDocument();
    expect(
      screen.queryByLabelText("profile.mobileModal.fields.newPhoneNumber"),
    ).not.toBeInTheDocument();
    expect(
      screen.queryByLabelText("New Verification Code"),
    ).not.toBeInTheDocument();
  });
});
