import { Form, InnerForm } from "@fluidattacks/design";
import isNil from "lodash/isNil";
import type { FC } from "react";
import { useTranslation } from "react-i18next";

import { EditPhoneContent } from "./content";
import type { IEditPhoneForm } from "./types";

import type { IEditionFormValues } from "../../types";
import { editPhoneSchema } from "../../validations";
import { Can } from "context/authz/can";

const EditPhoneForm: FC<IEditPhoneForm> = ({
  handleOpenEdit,
  isOpen,
  onSubmit,
  onCancel,
  isEditingMobileCode,
  phone,
}: IEditPhoneForm): JSX.Element | null => {
  const { t } = useTranslation();

  if (!isOpen || isNil(phone)) return null;

  return (
    <Form
      cancelButton={{ onClick: onCancel }}
      confirmButton={{ label: t("profile.mobileModal.edit") }}
      defaultDisabled={false}
      defaultValues={{
        newPhone: phone.callingCountryCode,
        phone: phone.callingCountryCode + phone.nationalNumber,
        verificationCode: "",
      }}
      id={"editPhone"}
      onSubmit={isEditingMobileCode ? onSubmit : handleOpenEdit}
      yupSchema={isEditingMobileCode ? editPhoneSchema : undefined}
    >
      <Can do={"integrates_api_mutations_update_stakeholder_phone_mutate"}>
        <InnerForm<IEditionFormValues>>
          {(methods): JSX.Element => (
            <EditPhoneContent
              isEditingMobileCode={isEditingMobileCode}
              phone={phone}
              {...methods}
            />
          )}
        </InnerForm>
      </Can>
    </Form>
  );
};

export { EditPhoneForm };
