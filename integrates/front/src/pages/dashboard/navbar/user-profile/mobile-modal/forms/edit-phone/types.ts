import type { IEditionFormValues } from "../../types";
import type { IPhoneAttr } from "../../types";
import type { IPhoneForm } from "../types";

interface IEditPhoneForm extends IPhoneForm<IEditionFormValues> {
  phone: IPhoneAttr | null;
  isEditingMobileCode: boolean;
  handleOpenEdit: () => void;
}

export type { IEditPhoneForm };
