import { Form, InnerForm, PhoneInput } from "@fluidattacks/design";
import type { FC } from "react";
import { useTranslation } from "react-i18next";

import type { IAdditionFormValues } from "../../types";
import { addPhoneSchema } from "../../validations";
import type { IPhoneForm } from "../types";
import { Can } from "context/authz/can";

const AddPhoneForm: FC<IPhoneForm<IAdditionFormValues>> = ({
  isOpen,
  onSubmit,
  onCancel,
}: IPhoneForm<IAdditionFormValues>): JSX.Element | null => {
  const { t } = useTranslation();
  if (!isOpen) return null;

  return (
    <Form
      cancelButton={{ onClick: onCancel }}
      confirmButton={{ label: t("profile.mobileModal.add") }}
      defaultValues={{ phone: "" }}
      id={"addPhone"}
      onSubmit={onSubmit}
      yupSchema={addPhoneSchema}
    >
      <Can do={"integrates_api_mutations_update_stakeholder_phone_mutate"}>
        <InnerForm>
          {({ formState: { errors }, register, watch }): JSX.Element => (
            <PhoneInput
              error={errors.phone?.message?.toString()}
              label={t("profile.mobileModal.fields.phoneNumber")}
              name={"phone"}
              register={register}
              watch={watch}
            />
          )}
        </InnerForm>
      </Can>
    </Form>
  );
};

export { AddPhoneForm };
