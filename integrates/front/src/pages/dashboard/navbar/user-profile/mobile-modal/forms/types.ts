interface IPhoneForm<T> {
  isOpen: boolean;
  onCancel: () => void;
  onSubmit: (values: T) => void;
}

export type { IPhoneForm };
