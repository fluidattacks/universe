import type { IVerifyAdditionCodeFormValues } from "../../types";
import type { IPhoneAttr } from "../../types";
import type { IPhoneForm } from "../types";

interface IVerifyPhoneForm extends IPhoneForm<IVerifyAdditionCodeFormValues> {
  phoneToAdd: IPhoneAttr | undefined;
}

export type { IVerifyPhoneForm };
