import { mixed, object, string } from "yup";

import type { IPhoneAttr } from "./types";

import { formatPhone } from "utils/format-helpers";
import { translate } from "utils/translations/translate";

const addPhoneSchema = object().shape({
  phone: mixed<IPhoneAttr>()
    .transform(
      (value): IPhoneAttr => (value === "" ? value : formatPhone(value)),
    )
    .required(translate.t("validations.required"))
    .isValidPhoneNumber(),
});

const verifyCodeSchema = object().shape({
  newVerificationCode: string().required(translate.t("validations.required")),
});

const editPhoneSchema = object().shape({
  newPhone: mixed<IPhoneAttr>()
    .transform(
      (value): IPhoneAttr => (value === "" ? value : formatPhone(value)),
    )
    .required(translate.t("validations.required"))
    .isValidPhoneNumber(),
  verificationCode: string().required(translate.t("validations.required")),
});

export { addPhoneSchema, editPhoneSchema, verifyCodeSchema };
