import { Input, PhoneInput, type TFormMethods } from "@fluidattacks/design";
import { Fragment, useEffect } from "react";
import { useTranslation } from "react-i18next";

import type { IEditionFormValues, IPhoneAttr } from "../../../types";

const EditPhoneContent = ({
  isEditingMobileCode,
  formState,
  phone,
  register,
  reset,
  watch,
}: {
  isEditingMobileCode: boolean;
  phone?: IPhoneAttr;
} & TFormMethods<IEditionFormValues>): JSX.Element => {
  const { t } = useTranslation();
  useEffect((): void => {
    if (phone)
      reset({
        newPhone: phone.callingCountryCode,
        phone: phone.callingCountryCode + phone.nationalNumber,
      });
  }, [phone, reset]);

  return (
    <Fragment>
      <PhoneInput
        disabled={true}
        label={t("profile.mobileModal.fields.phoneNumber")}
        name={"phone"}
        register={register}
        watch={watch}
      />
      {isEditingMobileCode ? (
        <Fragment>
          <Input
            error={formState.errors.verificationCode?.message}
            isTouched={"verificationCode" in formState.touchedFields}
            isValid={!("verificationCode" in formState.errors)}
            label={t("profile.mobileModal.fields.verificationCode")}
            name={"verificationCode"}
            register={register}
          />
          <PhoneInput
            error={formState.errors.newPhone?.message}
            label={t("profile.mobileModal.fields.newPhoneNumber")}
            name={"newPhone"}
            register={register}
            watch={watch}
          />
        </Fragment>
      ) : undefined}
    </Fragment>
  );
};

export { EditPhoneContent };
