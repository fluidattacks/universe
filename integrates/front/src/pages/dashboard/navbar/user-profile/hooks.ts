import { useQuery } from "@apollo/client";

import { GET_USER_CREDENTIALS_AND_ROOTS } from "./queries";
import type { IUserCredAndRootsAttr } from "./types";

import { Logger } from "utils/logger";

const useGetUserCredentials = (): readonly [
  IUserCredAndRootsAttr | undefined,
] => {
  const { data } = useQuery(GET_USER_CREDENTIALS_AND_ROOTS, {
    fetchPolicy: "cache-and-network",
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.warning("An error getting user credentials", error);
      });
    },
  });

  return [data as IUserCredAndRootsAttr | undefined] as const;
};

export { useGetUserCredentials };
