import { useMutation } from "@apollo/client";
import { useConfirmDialog } from "@fluidattacks/design";
import mixpanel from "mixpanel-browser";
import { Fragment, useCallback, useContext, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";

import { AccessTokenModal } from "./api-token-modal";
import { AWSSubscriptionModal } from "./aws-subscription-modal";
import { MenuOptions } from "./menu-options";
import { MobileModal } from "./mobile-modal";
import { REMOVE_STAKEHOLDER_MUTATION } from "./queries";
import { RemoveAccount } from "./remove-account";

import { authContext } from "context/auth";
import type { IMeetingModeContext } from "context/meeting-mode";
import { meetingModeContext } from "context/meeting-mode";
import { useAudit } from "hooks/use-audit";
import { useModal } from "hooks/use-modal";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

const UserProfile: React.FC = (): JSX.Element => {
  const [isMenuVisible, setIsMenuVisible] = useState<boolean>(false);
  const { t } = useTranslation();
  const navigate = useNavigate();
  const { awsSubscription } = useContext(authContext);
  const userCredentialModal = useModal("credManagementModal");
  const { addAuditEvent } = useAudit();

  const { meetingMode, setMeetingMode } = useContext(
    meetingModeContext as React.Context<Required<IMeetingModeContext>>,
  );
  const toggleMeetingMode = useCallback((): void => {
    setMeetingMode((currentValue): boolean => {
      mixpanel.track(`${currentValue ? "Disable" : "Enable"}MeetingMode`);
      addAuditEvent(
        `MeetingMode.${currentValue ? "Disable" : "Enable"}`,
        "unknown",
      );

      return !currentValue;
    });
  }, [addAuditEvent, setMeetingMode]);

  const [isAWSSubscriptionModalOpen, setIsAWSSubscriptionModalOpen] =
    useState(false);
  const openAWSSubscriptionModal = useCallback((): void => {
    setIsMenuVisible(false);
    setIsAWSSubscriptionModalOpen(true);
  }, []);
  const closeAWSSubscriptionModal = useCallback((): void => {
    setIsMenuVisible(false);
    setIsAWSSubscriptionModalOpen(false);
  }, []);

  const [isMobileModalOpen, setIsMobileModalOpen] = useState(false);
  const openMobileModal = useCallback((): void => {
    setIsMenuVisible(false);
    setIsMobileModalOpen(true);
  }, []);
  const closeMobileModal = useCallback((): void => {
    setIsMenuVisible(false);
    setIsMobileModalOpen(false);
  }, []);

  const [isTokenModalOpen, setIsTokenModalOpen] = useState(false);
  const openTokenModal = useCallback((): void => {
    setIsMenuVisible(false);
    setIsTokenModalOpen(true);
  }, []);
  const closeTokenModal = useCallback((): void => {
    setIsTokenModalOpen(false);
  }, []);

  const [removeStakeholder] = useMutation(REMOVE_STAKEHOLDER_MUTATION, {
    onCompleted: (mtResult): void => {
      if (mtResult.removeStakeholder.success) {
        msgSuccess(
          t("navbar.deleteAccount.success"),
          t("navbar.deleteAccount.successTitle"),
        );
      } else {
        navigate("/home");
      }
    },
    onError: (removeError): void => {
      removeError.graphQLErrors.forEach((error): void => {
        switch (error.message) {
          case "Exception - The previous invitation to this user was requested less than a minute ago":
            msgError(t("navbar.deleteAccount.requestedTooSoon"));
            break;
          case "Exception - Credential is still in use by a root":
            msgError(t("groupAlerts.accountCredentialInUse"));
            break;
          default:
            Logger.error("An error occurred while deleting account", error);
            msgError(t("groupAlerts.errorTextsad"));
        }
      });
      navigate("/home");
    },
  });

  const { confirm, ConfirmDialog } = useConfirmDialog();

  const deleteAccount = useCallback((): void => {
    setIsMenuVisible(false);
    userCredentialModal.open();
  }, [userCredentialModal]);

  const removeAccount = useCallback(async (): Promise<void> => {
    await removeStakeholder();
  }, [removeStakeholder]);

  const logout = useCallback((): void => {
    setIsMenuVisible(false);
    confirm({ title: t("navbar.logout") })
      .then((result: boolean): void => {
        if (result) {
          mixpanel.reset();
          location.assign("/logout");
        }
      })
      .catch((): void => {
        Logger.error("An error occurred in logout");
      });
  }, [confirm, t]);

  const toggleShowMenu = useCallback((): void => {
    setIsMenuVisible((previous): boolean => !previous);
  }, [setIsMenuVisible]);

  return (
    <Fragment>
      <MenuOptions
        deleteAccount={deleteAccount}
        isMenuVisible={isMenuVisible}
        logout={logout}
        meetingMode={meetingMode}
        openAWSSubscriptionModal={
          awsSubscription === null ? undefined : openAWSSubscriptionModal
        }
        openMobileModal={openMobileModal}
        openTokenModal={openTokenModal}
        setIsMenuVisible={setIsMenuVisible}
        toggleMeetingMode={toggleMeetingMode}
        toggleShowMenu={toggleShowMenu}
      />
      {isAWSSubscriptionModalOpen ? (
        <AWSSubscriptionModal onClose={closeAWSSubscriptionModal} />
      ) : undefined}
      {isMobileModalOpen ? (
        <MobileModal onClose={closeMobileModal} />
      ) : undefined}
      {isTokenModalOpen ? (
        <AccessTokenModal onClose={closeTokenModal} open={true} />
      ) : undefined}
      {userCredentialModal.isOpen ? (
        <RemoveAccount
          modalRef={userCredentialModal}
          onRemove={removeAccount}
        />
      ) : undefined}
      <ConfirmDialog />
    </Fragment>
  );
};

export { UserProfile };
