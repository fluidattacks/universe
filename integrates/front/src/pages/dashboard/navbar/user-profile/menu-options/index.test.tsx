import { screen } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";

import { MenuOptions } from ".";
import type { IAuthContext } from "context/auth";
import { authContext } from "context/auth";
import { render } from "mocks";

const context: IAuthContext = {
  awsSubscription: null,
  tours: {
    newGroup: false,
    newRoot: false,
    welcome: false,
  },
  userEmail: "jonhDoe@fluidattacks.com",
  userName: "Jhon Doe",
};

describe("userProfileMenuOptions", (): void => {
  const memoryRouter = {
    initialEntries: ["/orgs/okada"],
  };

  it("menu feature preview", async (): Promise<void> => {
    expect.hasAssertions();

    const setIsMenuVisible = jest.fn();

    render(
      <authContext.Provider value={context}>
        <div id={"root"}>{"click-outside-menu"}</div>
        <MenuOptions
          deleteAccount={jest.fn()}
          isMenuVisible={true}
          logout={jest.fn()}
          meetingMode={false}
          openAWSSubscriptionModal={undefined}
          openMobileModal={jest.fn()}
          openTokenModal={jest.fn()}
          setIsMenuVisible={setIsMenuVisible}
          toggleMeetingMode={jest.fn()}
          toggleShowMenu={jest.fn()}
        />
      </authContext.Provider>,
      { memoryRouter },
    );

    const user = userEvent.setup();
    await user.click(screen.getByText("Jhon"));

    expect(screen.getByText("navbar.mobile")).toBeInTheDocument();
    expect(screen.getByText("navbar.speakup")).toBeInTheDocument();
    expect(screen.getByText("navbar.token")).toBeInTheDocument();
    expect(screen.getByText("navbar.notification")).toBeInTheDocument();
    expect(screen.getByText("navbar.trustedDevices")).toBeInTheDocument();
    expect(screen.getByText("navbar.deleteAccount.text")).toBeInTheDocument();
    expect(screen.getByText("navbar.logout")).toBeInTheDocument();

    await user.click(screen.getByText("click-outside-menu"));

    expect(setIsMenuVisible).toHaveBeenCalledTimes(1);
    expect(setIsMenuVisible).toHaveBeenCalledWith(false);

    jest.clearAllMocks();
  });
});
