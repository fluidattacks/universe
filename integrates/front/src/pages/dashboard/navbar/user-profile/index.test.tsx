import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import * as React from "react";

import { GET_ACCESS_TOKEN } from "./api-token-modal/queries";
import { GET_STAKEHOLDER_PHONE } from "./mobile-modal/queries";
import {
  GET_USER_CREDENTIALS_AND_ROOTS,
  REMOVE_STAKEHOLDER_MUTATION,
} from "./queries";

import { UserProfile } from ".";
import type { IAuthContext } from "context/auth";
import { authContext } from "context/auth";
import type {
  GetAccessTokenQueryQuery as GetAccessToken,
  GetStakeholderPhoneQueryQuery as GetStakeholderPhone,
  GetUserCredentialsAndRootsQuery as GetUserCredentialsAndRoots,
  RemoveStakeholderMutationMutation as RemoveStakeholder,
} from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IMessage } from "mocks/types";
import { msgError, msgSuccess } from "utils/notifications";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

jest.mock("mixpanel-browser", (): Record<string, unknown> => {
  const mockedMixPanel: Record<string, () => Record<string, unknown>> =
    jest.requireActual("mixpanel-browser");
  jest.spyOn(mockedMixPanel, "reset").mockImplementation();

  return mockedMixPanel;
});

const mockNavigatePush: jest.Mock = jest.fn();
jest.mock(
  "react-router-dom",
  (): Record<string, unknown> => ({
    ...jest.requireActual("react-router-dom"),
    useNavigate: (): jest.Mock => mockNavigatePush,
  }),
);

const context: IAuthContext = {
  awsSubscription: null,
  tours: {
    newGroup: false,
    newRoot: false,
    welcome: false,
  },
  userEmail: "jonhDoe@fluidattacks.com",
  userName: "Jhon Doe",
};

const ContextComponent = ({
  children,
}: Readonly<{
  children: React.ReactNode;
}>): JSX.Element => (
  <authContext.Provider value={context}>{children}</authContext.Provider>
);

describe("user Profile", (): void => {
  const btnConfirm = "Confirm";
  const memoryRouter = {
    initialEntries: ["/orgs/okada"],
  };
  const mockQuery = graphql
    .link(LINK)
    .query(
      GET_USER_CREDENTIALS_AND_ROOTS,
      (): StrictResponse<{ data: GetUserCredentialsAndRoots }> => {
        return HttpResponse.json({
          data: {
            me: {
              __typename: "Me",
              organizations: [],
              userEmail: "jonhDoe@fluidattacks.com",
            },
          },
        });
      },
    );

  it("should render the delete account modal", async (): Promise<void> => {
    expect.hasAssertions();

    const mockQueryMutation = [
      mockQuery,
      graphql
        .link(LINK)
        .query(
          GET_USER_CREDENTIALS_AND_ROOTS,
          (): StrictResponse<{ data: GetUserCredentialsAndRoots }> => {
            return HttpResponse.json({
              data: {
                me: {
                  __typename: "Me",
                  organizations: [],
                  userEmail: "jonhDoe@fluidattacks.com",
                },
              },
            });
          },
        ),
      graphql
        .link(LINK)
        .mutation(
          REMOVE_STAKEHOLDER_MUTATION,
          (): StrictResponse<{ data: RemoveStakeholder }> => {
            return HttpResponse.json({
              data: {
                removeStakeholder: {
                  success: true,
                },
              },
            });
          },
        ),
    ];

    render(
      <ContextComponent>
        <UserProfile />
      </ContextComponent>,
      { memoryRouter, mocks: mockQueryMutation },
    );

    expect(screen.getAllByRole("button")).toHaveLength(1);

    await userEvent.click(screen.getAllByRole("button")[0]);

    expect(
      screen.queryByText("navbar.deleteAccount.modal.text"),
    ).not.toBeInTheDocument();

    await userEvent.click(screen.getByText("navbar.deleteAccount.text"));

    await expect(
      screen.findByText("navbar.deleteAccount.modal.text"),
    ).resolves.toBeInTheDocument();

    await userEvent.click(screen.getByText(btnConfirm));

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "navbar.deleteAccount.success",
        "navbar.deleteAccount.successTitle",
      );
    });

    jest.clearAllMocks();
  });

  it("should render fail an delete account modal", async (): Promise<void> => {
    expect.hasAssertions();

    const mockQueryFalse = [
      mockQuery,
      graphql
        .link(LINK)
        .mutation(
          REMOVE_STAKEHOLDER_MUTATION,
          (): StrictResponse<{ data: RemoveStakeholder }> => {
            return HttpResponse.json({
              data: {
                removeStakeholder: {
                  success: false,
                },
              },
            });
          },
        ),
    ];

    render(
      <ContextComponent>
        <UserProfile />
      </ContextComponent>,
      { memoryRouter, mocks: mockQueryFalse },
    );

    expect(screen.getAllByRole("button")).toHaveLength(1);

    await userEvent.click(screen.getAllByRole("button")[0]);

    expect(
      screen.queryByText("navbar.deleteAccount.modal.text"),
    ).not.toBeInTheDocument();

    await userEvent.click(screen.getByText("navbar.deleteAccount.text"));

    await waitFor((): void => {
      expect(
        screen.queryByText("navbar.deleteAccount.modal.text"),
      ).toBeInTheDocument();
    });
    await userEvent.click(screen.getByText(btnConfirm));

    await waitFor((): void => {
      expect(mockNavigatePush).toHaveBeenCalledWith("/home");
    });

    expect(msgSuccess).toHaveBeenCalledTimes(0);

    jest.clearAllMocks();
  });

  it("should delete account error", async (): Promise<void> => {
    expect.hasAssertions();

    const mockQueryFalse = [
      mockQuery,
      graphql
        .link(LINK)
        .mutation(
          REMOVE_STAKEHOLDER_MUTATION,
          (): StrictResponse<Record<"errors", IMessage[]>> => {
            return HttpResponse.json({
              errors: [
                new GraphQLError(
                  "Exception - Credential is still in use by a root",
                ),
                new GraphQLError("Unexpected error"),
              ],
            });
          },
        ),
    ];

    render(
      <ContextComponent>
        <UserProfile />
      </ContextComponent>,
      { memoryRouter, mocks: mockQueryFalse },
    );

    expect(screen.getAllByRole("button")).toHaveLength(1);

    await userEvent.click(screen.getAllByRole("button")[0]);

    expect(
      screen.queryByText("navbar.deleteAccount.modal.text"),
    ).not.toBeInTheDocument();

    await userEvent.click(screen.getByText("navbar.deleteAccount.text"));

    await waitFor((): void => {
      expect(
        screen.queryByText("navbar.deleteAccount.modal.text"),
      ).toBeInTheDocument();
    });

    await userEvent.click(screen.getByText(btnConfirm));
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledTimes(2);
    });

    expect(msgError).toHaveBeenLastCalledWith("groupAlerts.errorTextsad");

    expect(mockNavigatePush).toHaveBeenCalledWith("/home");

    jest.clearAllMocks();
  });

  it("should hiden menu when clicking ouside menu, but inside root node in feature preview", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <ContextComponent>
        <div id={"root"}>{"click-outside-menu"}</div>
        <UserProfile />
      </ContextComponent>,
      { memoryRouter },
    );

    const user = userEvent.setup();
    await user.click(screen.getByRole("button"));

    expect(screen.getByText("navbar.mobile")).toBeInTheDocument();
    expect(screen.getByText("navbar.token")).toBeInTheDocument();
    expect(screen.getByText("navbar.notification")).toBeInTheDocument();
    expect(screen.getByText("navbar.trustedDevices")).toBeInTheDocument();
    expect(screen.getByText("navbar.deleteAccount.text")).toBeInTheDocument();
    expect(screen.getByText("navbar.logout")).toBeInTheDocument();

    await user.click(screen.getByText("click-outside-menu"));

    expect(screen.queryByText("navbar.mobile")).not.toBeInTheDocument();
    expect(screen.queryByText("navbar.token")).not.toBeInTheDocument();
    expect(screen.queryByText("navbar.notification")).not.toBeInTheDocument();
    expect(screen.queryByText("navbar.trustedDevices")).not.toBeInTheDocument();
    expect(
      screen.queryByText("navbar.deleteAccount.text"),
    ).not.toBeInTheDocument();
    expect(screen.queryByText("navbar.logout")).not.toBeInTheDocument();

    jest.clearAllMocks();
  });

  it("should render modals", async (): Promise<void> => {
    expect.hasAssertions();

    const mockQueries = [
      mockQuery,
      graphql
        .link(LINK)
        .query(
          GET_ACCESS_TOKEN,
          (): StrictResponse<{ data: GetAccessToken }> => {
            return HttpResponse.json({
              data: {
                me: {
                  __typename: "Me",
                  accessTokens: [],
                  userEmail: "jonhDoe@fluidattacks.com",
                },
              },
            });
          },
        ),
    ];

    render(
      <ContextComponent>
        <UserProfile />
      </ContextComponent>,
      { memoryRouter, mocks: mockQueries },
    );

    await userEvent.click(screen.getAllByRole("button")[0]);
    await userEvent.click(screen.getByText("navbar.deleteAccount.text"));

    await expect(
      screen.findByText("navbar.deleteAccount.modal.text"),
    ).resolves.toBeInTheDocument();

    await userEvent.click(screen.getByText("Cancel"));

    expect(screen.queryByText("navbar.token")).not.toBeInTheDocument();

    await userEvent.click(screen.getAllByRole("button")[0]);
    await userEvent.click(screen.getByText("navbar.token"));

    expect(screen.queryByText("navbar.token")).not.toBeInTheDocument();

    expect(screen.getByText("updateAccessToken.title")).toBeInTheDocument();

    jest.clearAllMocks();
  });

  it("should render mobile modal", async (): Promise<void> => {
    expect.hasAssertions();

    const mockQueries = [
      graphql
        .link(LINK)
        .query(
          GET_STAKEHOLDER_PHONE,
          (): StrictResponse<{ data: GetStakeholderPhone }> => {
            return HttpResponse.json({
              data: {
                me: {
                  __typename: "Me",
                  phone: null,
                  userEmail: "jonhDoe@fluidattacks.com",
                },
              },
            });
          },
        ),
    ];

    render(
      <ContextComponent>
        <UserProfile />
      </ContextComponent>,
      { memoryRouter, mocks: mockQueries },
    );

    await userEvent.click(screen.getAllByRole("button")[0]);
    await userEvent.click(screen.getByText("navbar.mobile"));

    // Once clicked an option, menu closes.
    expect(screen.queryByText("navbar.mobile")).not.toBeInTheDocument();

    expect(screen.getByText("profile.mobileModal.title")).toBeInTheDocument();

    jest.clearAllMocks();
  });
});
