import { object, string } from "yup";

import { translate } from "utils/translations/translate";

const MAX_NAME_LENGTH = 20;

const validationSchema = object().shape({
  expirationTime: string()
    .required(translate.t("validations.required"))
    .isValidDate("lowerDate")
    .isValidDate("validDateToken"),
  name: string()
    .required(translate.t("validations.required"))
    .max(
      MAX_NAME_LENGTH,
      translate.t("validations.maxLength", { count: MAX_NAME_LENGTH }),
    )
    .isValidTextBeginning()
    .isValidTextField(),
});

export { validationSchema };
