import type { ApolloQueryResult } from "@apollo/client";
import {
  Button,
  Container,
  Form,
  InnerForm,
  Input,
  InputDate,
  Modal,
  Row,
  Text,
  TextArea,
  useModal,
} from "@fluidattacks/design";
import dayjs from "dayjs";
import isUndefined from "lodash/isUndefined";
import mixpanel from "mixpanel-browser";
import { useCallback, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

import { useAddAccessToken } from "../hooks";
import { validationSchema } from "../validations";
import type { GetAccessTokenQueryQuery } from "gql/graphql";
import { msgError, msgSuccess } from "utils/notifications";

interface IAddTokenModalProps {
  readonly open: boolean;
  readonly onClose: () => void;
  readonly refetch: () => Promise<ApolloQueryResult<GetAccessTokenQueryQuery>>;
}

interface IAccessTokenAttr {
  expirationTime: string;
  name: string;
}

const WEEKS_IN_SIX_MONTHS = 26;

const AddTokenModal: React.FC<IAddTokenModalProps> = ({
  open,
  onClose,
  refetch,
}): JSX.Element => {
  const theme = useTheme();
  const { t } = useTranslation();
  const sixMonthsLater = dayjs()
    .add(WEEKS_IN_SIX_MONTHS, "weeks")
    .format("YYYY-MM-DD");
  const oneDayLater = dayjs().add(1, "day").format("YYYY-MM-DD");
  const modalProps = useModal("add-api-token");

  const [addAccessToken, mtResponse] = useAddAccessToken(refetch);
  const [show, setShow] = useState(false);

  const handleReveal = useCallback((): void => {
    void refetch();
    setShow(!show);
  }, [refetch, show]);

  const handleUpdateAPIToken = useCallback(
    async (values: IAccessTokenAttr): Promise<void> => {
      mixpanel.track("GenerateAPIToken");
      if (mtResponse.data?.addAccessToken.sessionJwt === undefined)
        await addAccessToken({
          variables: {
            expirationTime: dayjs(values.expirationTime).unix(),
            name: values.name,
          },
        });
      else onClose();
    },
    [addAccessToken, mtResponse, onClose],
  );

  const handleCopy = useCallback(async (): Promise<void> => {
    const { clipboard } = navigator;

    if (isUndefined(clipboard)) {
      msgError(t("updateAccessToken.copy.failed"));
    } else {
      await clipboard.writeText(
        mtResponse.data?.addAccessToken.sessionJwt ?? "",
      );
      msgSuccess(
        t("updateAccessToken.copy.successfully"),
        t("updateAccessToken.copy.success"),
      );
    }
  }, [mtResponse.data?.addAccessToken.sessionJwt, t]);

  const jwtValue = ((): string | undefined => {
    if (mtResponse.data?.addAccessToken.sessionJwt === undefined) {
      return undefined;
    } else if (show) {
      return mtResponse.data.addAccessToken.sessionJwt;
    }
    const STAR_STRING_LENGTH = 300;

    return "*".repeat(STAR_STRING_LENGTH);
  })();

  return (
    <Modal
      id={"updateAccessToken"}
      modalRef={{ ...modalProps, close: onClose, isOpen: open }}
      size={"sm"}
      title={t("updateAccessToken.addTitle")}
    >
      <Form
        cancelButton={{ onClick: onClose }}
        defaultValues={{ expirationTime: "", name: "" }}
        onSubmit={handleUpdateAPIToken}
        yupSchema={validationSchema}
      >
        {jwtValue === undefined ? (
          <InnerForm>
            {({ formState, register, setValue, watch }): JSX.Element => {
              return (
                <React.Fragment>
                  <Row>
                    <Input
                      error={formState.errors.name?.message?.toString()}
                      id={"name"}
                      isTouched={"name" in formState.touchedFields}
                      isValid={!("name" in formState.errors)}
                      label={t("updateAccessToken.fields.name")}
                      name={"name"}
                      register={register}
                    />
                  </Row>
                  <Row>
                    <InputDate
                      error={formState.errors.expirationTime?.message?.toString()}
                      label={t("updateAccessToken.expirationTime")}
                      max={sixMonthsLater}
                      min={oneDayLater}
                      name={"expirationTime"}
                      register={register}
                      setValue={setValue}
                      watch={watch}
                    />
                  </Row>
                </React.Fragment>
              );
            }}
          </InnerForm>
        ) : (
          <React.Fragment>
            <Text color={theme.palette.gray[800]} size={"sm"}>
              {t("updateAccessToken.message")}
            </Text>
            <TextArea
              disabled={true}
              maskValue={true}
              name={"sessionJwt"}
              rows={5}
              value={jwtValue}
            />
            <Container alignItems={"start"} display={"flex"} gap={0.75}>
              <Button icon={"copy"} onClick={handleCopy} variant={"ghost"}>
                {t("updateAccessToken.copy.copy")}
              </Button>
              <Button
                icon={show ? "eye-slash" : "eye"}
                id={"reveal-token"}
                onClick={handleReveal}
                variant={"ghost"}
              >
                {show
                  ? t("updateAccessToken.hidden")
                  : t("updateAccessToken.view")}
              </Button>
            </Container>
          </React.Fragment>
        )}
      </Form>
    </Modal>
  );
};

export { AddTokenModal };
