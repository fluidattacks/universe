import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import dayjs from "dayjs";
import { GraphQLError } from "graphql";
import { HttpResponse, type StrictResponse, graphql } from "msw";

import {
  ADD_ACCESS_TOKEN,
  GET_ACCESS_TOKEN,
  INVALIDATE_ACCESS_TOKEN_MUTATION,
} from "./queries";

import { AccessTokenModal } from ".";
import type {
  AddAccessTokenMutation as AddAccessToken,
  AddAccessTokenMutation,
  GetAccessTokenQueryQuery as GetAccessToken,
  InvalidateAccessTokenMutationMutation as InvalidateAccessToken,
} from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";
import { msgError, msgSuccess } from "utils/notifications";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

describe("update access token modal", (): void => {
  const graphqlMocked = graphql.link(LINK);
  const handleOnClose: jest.Mock = jest.fn();

  const msToSec = 1000;
  const yyyymmdd = 10;

  it("should render an add access token modal", async (): Promise<void> => {
    expect.hasAssertions();

    const mockQueryFalse = [
      graphqlMocked.query(
        GET_ACCESS_TOKEN,
        (): StrictResponse<{ data: GetAccessToken }> => {
          return HttpResponse.json({
            data: {
              me: {
                __typename: "Me",
                accessTokens: [],
                userEmail: "test@fluidattacks.com",
              },
            },
          });
        },
      ),
    ];

    render(<AccessTokenModal onClose={handleOnClose} open={true} />, {
      mocks: mockQueryFalse,
    });

    expect(screen.getByText("updateAccessToken.title")).toBeInTheDocument();

    await waitFor((): void => {
      expect(
        screen.queryByText("updateAccessToken.buttons.add"),
      ).toBeInTheDocument();
    });

    expect(screen.getByText("table.noDataIndication")).toBeInTheDocument();

    await userEvent.click(screen.getByText("updateAccessToken.buttons.add"));

    await waitFor((): void => {
      expect(
        screen.queryByText("updateAccessToken.addTitle"),
      ).toBeInTheDocument();
    });

    expect(screen.getByText("Confirm")).toBeDisabled();
    expect(
      screen.getByText("updateAccessToken.expirationTime"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("updateAccessToken.fields.name"),
    ).toBeInTheDocument();

    await userEvent.click(screen.getByText("Cancel"));

    await userEvent.click(screen.getAllByRole("button")[0]);

    await waitFor((): void => {
      expect(handleOnClose).toHaveBeenCalledTimes(1);
    });

    jest.clearAllMocks();
  });

  it("should render a token creation date", async (): Promise<void> => {
    expect.hasAssertions();

    const mockQueryTrue = [
      graphqlMocked.query(
        GET_ACCESS_TOKEN,
        (): StrictResponse<{ data: GetAccessToken }> => {
          return HttpResponse.json({
            data: {
              me: {
                __typename: "Me",
                accessTokens: [
                  {
                    __typename: "AccessToken",
                    id: "a8e91ab7-29c8-4f74-b8d4-3c7d69cf187f",
                    issuedAt: 1687376518,
                    lastUse: dayjs.utc().toISOString(),
                    name: "FirstToken",
                  },
                ],
                userEmail: "test@fluidattacks.com",
              },
            },
          });
        },
        { once: true },
      ),
      graphqlMocked.mutation(
        INVALIDATE_ACCESS_TOKEN_MUTATION,
        ({
          variables,
        }): StrictResponse<IErrorMessage | { data: InvalidateAccessToken }> => {
          const { id } = variables;
          if (id === "a8e91ab7-29c8-4f74-b8d4-3c7d69cf187f") {
            return HttpResponse.json({
              data: {
                invalidateAccessToken: {
                  success: true,
                },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error invalidating token")],
          });
        },
        { once: true },
      ),
      graphqlMocked.query(
        GET_ACCESS_TOKEN,
        (): StrictResponse<{ data: GetAccessToken }> => {
          return HttpResponse.json({
            data: {
              me: {
                __typename: "Me",
                accessTokens: [],
                userEmail: "test@fluidattacks.com",
              },
            },
          });
        },
        { once: true },
      ),
    ];

    render(<AccessTokenModal onClose={handleOnClose} open={true} />, {
      mocks: mockQueryTrue,
    });

    await waitFor((): void => {
      expect(screen.queryByText("FirstToken")).toBeInTheDocument();
    });

    expect(screen.getAllByRole("row")).toHaveLength(2);
    expect(screen.getAllByRole("row")[1].textContent).toStrictEqual(
      [
        "FirstToken",
        "2023-06-21",
        "a few seconds ago",
        "updateAccessToken.buttons.revoke",
      ].join(""),
    );

    await userEvent.click(screen.getByText("updateAccessToken.buttons.revoke"));

    expect(
      screen.getByText("updateAccessToken.invalidate"),
    ).toBeInTheDocument();

    await userEvent.click(screen.getByText("Confirm"));

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "updateAccessToken.delete",
        "updateAccessToken.invalidated",
      );
    });
    await waitFor((): void => {
      expect(screen.getAllByRole("row")[1].textContent).toBe(
        "table.noDataIndication",
      );
    });

    await userEvent.click(screen.queryAllByRole("button")[0]);

    await waitFor((): void => {
      expect(handleOnClose).toHaveBeenCalledTimes(1);
    });

    jest.clearAllMocks();
  });

  it("should render a new access token", async (): Promise<void> => {
    expect.hasAssertions();

    const expirationTime: string = dayjs()
      .add(1, "month")
      .toISOString()
      .substring(0, yyyymmdd);

    const addAccessToken: AddAccessTokenMutation = {
      addAccessToken: {
        sessionJwt: "dummyJwt",
        success: true,
      },
    };
    const mockMutation = [
      graphqlMocked.query(
        GET_ACCESS_TOKEN,
        (): StrictResponse<{ data: GetAccessToken }> => {
          return HttpResponse.json({
            data: {
              me: {
                __typename: "Me",
                accessTokens: [],
                userEmail: "test@fluidattacks.com",
              },
            },
          });
        },
        { once: true },
      ),
      graphqlMocked.mutation(
        ADD_ACCESS_TOKEN,
        ({
          variables,
        }): StrictResponse<IErrorMessage | { data: AddAccessToken }> => {
          const { expirationTime: expirationTimeVar, name } = variables;
          if (
            expirationTimeVar ===
              Math.floor(new Date(expirationTime).getTime() / msToSec) &&
            name === "AnotherToken"
          ) {
            return HttpResponse.json({ data: addAccessToken });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error invalidating token")],
          });
        },
        { once: true },
      ),
      graphqlMocked.query(
        GET_ACCESS_TOKEN,
        (): StrictResponse<{ data: GetAccessToken }> => {
          return HttpResponse.json({
            data: {
              me: {
                __typename: "Me",
                accessTokens: [
                  {
                    __typename: "AccessToken",
                    id: "28565d7d-0eb8-48a3-85f3-4fd6d03e3725",
                    issuedAt: 1687376518,
                    lastUse: null,
                    name: "AnotherToken",
                  },
                ],
                userEmail: "test@fluidattacks.com",
              },
            },
          });
        },
        { once: true },
      ),
    ];

    render(<AccessTokenModal onClose={handleOnClose} open={true} />, {
      mocks: mockMutation,
    });

    expect(screen.getByText("updateAccessToken.title")).toBeInTheDocument();

    await waitFor((): void => {
      expect(
        screen.queryByText("updateAccessToken.buttons.add"),
      ).toBeInTheDocument();
    });

    expect(screen.getByText("table.noDataIndication")).toBeInTheDocument();

    await userEvent.click(screen.getByText("updateAccessToken.buttons.add"));

    await waitFor((): void => {
      expect(
        screen.queryByText("updateAccessToken.addTitle"),
      ).toBeInTheDocument();
    });

    await userEvent.type(
      screen.getByRole("spinbutton", { name: /month, /u }),
      expirationTime.slice(5, 7),
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: /day, /u }),
      expirationTime.slice(8),
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: /year, /u }),
      expirationTime.slice(0, 4),
    );
    await userEvent.type(
      screen.getByRole("textbox", { name: "name" }),
      "AnotherToken",
    );

    await userEvent.click(screen.getByText("Confirm"));

    await waitFor((): void => {
      expect(screen.getByText("updateAccessToken.message")).toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(screen.getAllByRole("row")[1].textContent).toStrictEqual(
        [
          "AnotherToken",
          "2023-06-21",
          "Never",
          "updateAccessToken.buttons.revoke",
        ].join(""),
      );
    });

    expect(msgSuccess).toHaveBeenCalledWith(
      "updateAccessToken.successfully",
      "updateAccessToken.success",
    );

    await userEvent.click(screen.getByText("updateAccessToken.copy.copy"));

    expect(msgError).toHaveBeenCalledWith("updateAccessToken.copy.failed");

    jest.clearAllMocks();
  });
});
