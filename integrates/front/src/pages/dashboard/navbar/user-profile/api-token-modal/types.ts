interface IInvalidateAccessTokenAttr {
  invalidateAccessToken: {
    success: boolean;
  };
}

interface ITokensModalProps {
  open: boolean;
  onClose: () => void;
}

interface IAccessTokens {
  id: string;
  name: string;
  issuedAt: number;
  lastUse: string | null;
}

export type { IAccessTokens, IInvalidateAccessTokenAttr, ITokensModalProps };
