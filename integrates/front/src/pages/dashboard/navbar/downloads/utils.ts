import dayjs from "dayjs";

import type { TNotification } from "./hooks";

import { ReportStatus } from "gql/graphql";

const isReportActive = (notification: TNotification): boolean => {
  return (
    (notification.status !== ReportStatus.Deleted &&
      notification.expirationTime === null) ||
    (dayjs().isBefore(notification.expirationTime) &&
      notification.status !== ReportStatus.Deleted)
  );
};

const isReportExpired = (notification: TNotification): boolean => {
  return (
    !isReportActive(notification) &&
    notification.status !== ReportStatus.Deleted
  );
};

const isReportNotification = (notification: TNotification): boolean => {
  return (notification.__typename as string) === "ReportNotification";
};

export { isReportActive, isReportExpired, isReportNotification };
