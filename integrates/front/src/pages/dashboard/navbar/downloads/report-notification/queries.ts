import { graphql } from "gql";

const GET_NOTIFICATION = graphql(`
  query GetNotification($notificationId: ID!) {
    me {
      userEmail
      notification(notificationId: $notificationId) {
        __typename
        ... on ReportNotification {
          id
          downloadURL
        }
      }
    }
  }
`);

export { GET_NOTIFICATION };
