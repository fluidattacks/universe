import { CloudImage, Container, Icon, Text } from "@fluidattacks/design";
import dayjs from "dayjs";
import { filesize } from "filesize";
import { useCallback } from "react";
import * as React from "react";
import { useTheme } from "styled-components";

import { useDownload, useUpdateNotifications } from "./hooks";
import { CloseButton, DownloadButton } from "./styles";

import type { TNotification } from "../hooks";
import { ReportStatus } from "gql/graphql";

const IMAGE_BASE_PATH = "integrates/notifications/report-type";

interface IReportNotificationProps {
  readonly expired: boolean;
  readonly notification: TNotification;
  readonly onDownload?: (notificationId: string) => void;
}
const EXPIRED_COLOR = 400;
const ACTIVE_COLOR = 800;
const ReportNotification: React.FC<IReportNotificationProps> = ({
  expired,
  notification,
  onDownload,
}): React.JSX.Element => {
  const theme = useTheme();

  const { downloading, download } = useDownload(notification.id);
  const { dismissNotification } = useUpdateNotifications(
    notification.id,
    notification.size,
    notification.s3FilePath,
  );

  const handleDownload = useCallback(async (): Promise<void> => {
    await download();
    onDownload?.(notification.id);
  }, [download, notification, onDownload]);

  const downloadAvailable =
    notification.status === ReportStatus.Ready && !expired;

  return (
    <Container
      alignItems={"center"}
      bgColorHover={theme.palette.gray[100]}
      display={"flex"}
      gap={0.5}
      key={notification.id}
      px={1.25}
      py={1}
    >
      <Container
        display={"flex"}
        flexDirection={"column"}
        flexGrow={1}
        gap={0.125}
      >
        <Text
          color={theme.palette.gray[expired ? EXPIRED_COLOR : ACTIVE_COLOR]}
          fontWeight={"bold"}
          size={"sm"}
        >
          {notification.name}
        </Text>
        {notification.status === ReportStatus.Preparing ? (
          <Container alignItems={"center"} display={"flex"} gap={0.25}>
            <Icon
              icon={"hourglass-start"}
              iconColor={theme.palette.gray[400]}
              iconSize={"xs"}
              iconType={"fa-regular"}
            />
            <Text color={theme.palette.gray[400]} size={"xs"}>
              {"Queued for processing..."}
            </Text>
          </Container>
        ) : undefined}
        {notification.status === ReportStatus.Processing ? (
          <Container alignItems={"center"} display={"flex"} gap={0.25}>
            <Icon
              icon={"refresh"}
              iconColor={theme.palette.gray[400]}
              iconSize={"xs"}
              iconType={"fa-regular"}
            />
            <Text color={theme.palette.gray[400]} size={"xs"}>
              {"Processing file..."}
            </Text>
          </Container>
        ) : undefined}
        {notification.status === ReportStatus.Ready ? (
          <Container display={"flex"} gap={0.5}>
            <CloudImage
              height={"16px"}
              publicId={
                expired
                  ? `${IMAGE_BASE_PATH}-${notification.format}-expired`
                  : `${IMAGE_BASE_PATH}-${notification.format}`
              }
              width={"16px"}
            />
            {expired ? (
              <Text color={theme.palette.gray[400]} size={"xs"}>
                {"Download expired"}
              </Text>
            ) : (
              <Text color={theme.palette.gray[400]} size={"xs"}>
                {filesize(notification.size ?? "")}&nbsp;
                {"•"}&nbsp;
                {dayjs(notification.notifiedAt).fromNow()}
              </Text>
            )}
          </Container>
        ) : undefined}
      </Container>

      <Container
        display={"flex"}
        flexDirection={"column"}
        flexGrow={1}
        gap={0.125}
      >
        <CloseButton
          height={"30px"}
          icon={"close"}
          iconSize={"xxs"}
          onClick={dismissNotification}
          variant={"ghost"}
          width={"30px"}
        />
        {downloadAvailable ? (
          <DownloadButton
            disabled={downloading}
            height={"30px"}
            icon={"arrow-down-to-bracket"}
            iconSize={"xxs"}
            iconType={"fa-regular"}
            onClick={handleDownload}
            variant={"ghost"}
            width={"30px"}
          />
        ) : undefined}
      </Container>
    </Container>
  );
};

export { ReportNotification };
