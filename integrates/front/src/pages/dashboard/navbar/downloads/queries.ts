import { graphql } from "gql";

const GET_NOTIFICATIONS = graphql(`
  query GetNotifications {
    me {
      userEmail
      notifications {
        __typename
        ... on ReportNotification {
          id
          expirationTime
          format
          name
          notifiedAt
          s3FilePath
          size
          status
        }
      }
    }
  }
`);

const UPDATE_NOTIFICATIONS = graphql(`
  mutation UpdateNotifications(
    $notificationId: String!
    $s3FilePath: String
    $size: Int
  ) {
    updateNotifications(
      notificationId: $notificationId
      s3FilePath: $s3FilePath
      size: $size
    ) {
      success
    }
  }
`);
export { GET_NOTIFICATIONS, UPDATE_NOTIFICATIONS };
