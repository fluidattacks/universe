import { screen } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { HttpResponse, type StrictResponse, graphql } from "msw";

import { GET_NOTIFICATIONS } from "./queries";

import { Downloads } from ".";
import type { GetNotificationsQuery } from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";

describe("downloads", (): void => {
  const graphqlMocked = graphql.link(LINK);

  it("should toggle menu", async (): Promise<void> => {
    expect.hasAssertions();

    render(<Downloads />, {
      mocks: [
        graphqlMocked.query(
          GET_NOTIFICATIONS,
          (): StrictResponse<{ data: GetNotificationsQuery }> => {
            return HttpResponse.json({
              data: {
                me: {
                  __typename: "Me",
                  notifications: [],
                  userEmail: "test@test.rest",
                },
              },
            });
          },
        ),
      ],
    });

    const button = await screen.findByText("Downloads");

    expect(button).toBeInTheDocument();

    await userEvent.click(button);

    await expect(
      screen.findByText("Recent download history"),
    ).resolves.toBeInTheDocument();

    await userEvent.click(button);

    expect(
      screen.queryByText("Recent download history"),
    ).not.toBeInTheDocument();
  });
});
