import {
  Button,
  Container,
  Divider,
  EmptyState,
  Icon,
  IconButton,
  Loading,
  NotificationSign,
  Text,
} from "@fluidattacks/design";
import * as React from "react";
import { useTheme } from "styled-components";

import { useMenu, useNotificationsQuery, usePendingDownloads } from "./hooks";
import { ReportNotification } from "./report-notification";
import { DownloadAnimation } from "./styles";
import { isReportActive, isReportExpired, isReportNotification } from "./utils";

import { ReportStatus } from "gql/graphql";

const Downloads = (): React.JSX.Element => {
  const theme = useTheme();

  const { closeMenu, isOpen, menuRef, toggleMenu } = useMenu();

  const { notifications, loading } = useNotificationsQuery();
  const reportNotifications = notifications.filter(isReportNotification);
  const activeReports = reportNotifications.filter(isReportActive);
  const expiredReports = reportNotifications.filter(isReportExpired);

  const inProgress = activeReports.some(
    (report): boolean =>
      report.status === ReportStatus.Preparing ||
      report.status === ReportStatus.Processing,
  );
  const { hasPendingDownloads, markAsDownloaded } =
    usePendingDownloads(activeReports);

  return (
    <div ref={menuRef}>
      <Button onClick={toggleMenu} px={0.5} py={0.5} variant={"ghost"}>
        {inProgress ? (
          <DownloadAnimation>
            <Icon
              icon={"arrow-down-to-bracket"}
              iconSize={"xxs"}
              iconType={"fa-light"}
            />
          </DownloadAnimation>
        ) : (
          <React.Fragment>
            <NotificationSign
              left={"16%"}
              show={hasPendingDownloads}
              top={"23%"}
            />
            <Icon
              icon={"arrow-down-to-bracket"}
              iconSize={"xs"}
              iconType={"fa-light"}
            />
          </React.Fragment>
        )}
        {"Downloads"}
      </Button>
      {isOpen ? (
        <Container
          bgColor={theme.palette.white}
          border={`1px solid ${theme.palette.gray[200]}`}
          borderRadius={"4px"}
          display={"flex"}
          flexDirection={"column"}
          gap={0.5}
          minWidth={"266px"}
          pb={0.25}
          position={"absolute"}
          pt={1}
          right={"1px"}
          shadow={"md"}
          top={"40px"}
          zIndex={9999}
        >
          <Container
            alignItems={"center"}
            display={"flex"}
            justify={"center"}
            px={1.25}
          >
            <Text
              color={theme.palette.gray[800]}
              fontWeight={"bold"}
              size={"sm"}
            >
              {"Recent download history"}
            </Text>
            <IconButton
              icon={"close"}
              iconSize={"xxs"}
              onClick={closeMenu}
              variant={"ghost"}
            />
          </Container>
          <Container display={"flex"} flexDirection={"column"}>
            {loading ? (
              <Container display={"flex"} justify={"center"} py={5}>
                <Loading />
              </Container>
            ) : undefined}
            {!loading &&
            activeReports.length === 0 &&
            expiredReports.length === 0 ? (
              <EmptyState
                description={
                  "Once you download files, they'll appear here for easy access anytime."
                }
                imageSrc={"integrates/empty/notifications"}
                size={"sm"}
                title={"You have no downloads yet"}
              />
            ) : undefined}
            {activeReports.map((notification): React.JSX.Element => {
              return (
                <ReportNotification
                  expired={false}
                  key={notification.id}
                  notification={notification}
                  onDownload={markAsDownloaded}
                />
              );
            })}
            {expiredReports.length > 0 ? <Divider /> : undefined}
            {expiredReports.map((notification): React.JSX.Element => {
              return (
                <ReportNotification
                  expired={true}
                  key={notification.id}
                  notification={notification}
                />
              );
            })}
          </Container>
        </Container>
      ) : undefined}
    </div>
  );
};

export { Downloads };
