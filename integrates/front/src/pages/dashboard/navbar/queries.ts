import { graphql } from "gql";

const GET_USER_ORGANIZATIONS = graphql(`
  query GetUserOrganizationsAtTopBar {
    me {
      userEmail
      userName
      organizations {
        name
      }
    }
  }
`);

const GET_ME_VULNERABILITIES_ASSIGNED_IDS = graphql(`
  query GetMeVulnerabilitiesAssignedIds {
    me {
      __typename
      userEmail
      vulnerabilitiesAssigned {
        id
      }
    }
  }
`);

const GET_GROUP_INFO = graphql(`
  query GetGroupInfo($groupName: String!) {
    group(groupName: $groupName) {
      __typename
      name
      organization
    }
  }
`);

export {
  GET_ME_VULNERABILITIES_ASSIGNED_IDS,
  GET_USER_ORGANIZATIONS,
  GET_GROUP_INFO,
};
