import { useQuery } from "@apollo/client";
import { Button, NotificationSign } from "@fluidattacks/design";
import { useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";

import { GET_ME_VULNERABILITIES_ASSIGNED_IDS } from "../queries";
import { Logger } from "utils/logger";

const ToDo: React.FC = (): React.JSX.Element => {
  const navigate = useNavigate();
  const { t } = useTranslation();

  const { data } = useQuery(GET_ME_VULNERABILITIES_ASSIGNED_IDS, {
    fetchPolicy: "cache-first",
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.error(
          "An error occurred fetching vulnerabilities assigned ids",
          error,
        );
      });
    },
  });
  const vulnerabilitiesAssigned = data?.me.vulnerabilitiesAssigned ?? [];
  const hasVulnerabilities = vulnerabilitiesAssigned.length > 0;

  const handleClick = useCallback((): void => {
    navigate("/todos");
  }, [navigate]);

  return (
    <React.Fragment>
      <NotificationSign left={"23%"} show={hasVulnerabilities} top={"22%"} />
      <Button
        icon={"square-check"}
        onClick={handleClick}
        px={0.5}
        py={0.5}
        variant={"ghost"}
      >
        {t("components.navBar.toDo")}
      </Button>
    </React.Fragment>
  );
};

export { ToDo };
