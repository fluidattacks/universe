import { styled } from "styled-components";

const AnnounceWrapper = styled.div`
  > div {
    position: initial !important;
  }
`;

export { AnnounceWrapper };
