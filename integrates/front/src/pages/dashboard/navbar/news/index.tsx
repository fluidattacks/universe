import { Button } from "@fluidattacks/design";
import AnnounceKit from "announcekit-react";
import mixpanel from "mixpanel-browser";
import { useCallback, useContext } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { AnnounceWrapper } from "./styles";

import { authContext } from "context/auth";
import { useAudit } from "hooks/use-audit";
import { getEnvironment } from "utils/environment";

const News: React.FC = (): React.JSX.Element => {
  const { userEmail } = useContext(authContext);
  const { t } = useTranslation();

  const { addAuditEvent } = useAudit();
  const handleNewsOpen = useCallback((): void => {
    mixpanel.track("OpenNewsWidget");
    addAuditEvent("NewsWidget", "unknown");
  }, [addAuditEvent]);

  return (
    <AnnounceWrapper>
      <AnnounceKit
        boosters={getEnvironment() !== "ephemeral"}
        onWidgetOpen={handleNewsOpen}
        user={{ email: userEmail, id: userEmail }}
        widget={"https://news.fluidattacks.tech/widgets/v2/ZmEGk"}
        widgetStyle={{
          left: "26%",
          position: "absolute",
          top: "45%",
          zIndex: 11,
        }}
      >
        <Button icon={"bullhorn"} px={0.5} py={0.5} variant={"ghost"}>
          {t("components.navBar.news")}
        </Button>
      </AnnounceKit>
    </AnnounceWrapper>
  );
};

export { News };
