import { Container, GroupSelector, Text, useModal } from "@fluidattacks/design";
import { useCallback, useEffect, useRef, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

import { AddOrganizationModal } from "./add-organization-modal";

import { Dropdown } from "components/dropdown";
import type { IDropDownOption } from "components/dropdown/types";

const ORG_LENGTH = 5;
interface IDropdownProps {
  handleOrganizationChange: (org: string) => void;
  initialOrg?: string;
  organizationList: { name: string }[];
}

const OrganizationDropdown = ({
  handleOrganizationChange,
  initialOrg,
  organizationList,
}: Readonly<IDropdownProps>): JSX.Element => {
  const theme = useTheme();
  const { t } = useTranslation();
  const dropDownRef = useRef<HTMLDivElement>(null);
  const modalProps = useModal("add-organization-modal");

  const [displayOrganizations, setDisplayOrganizations] = useState(false);
  const [orgSelected, setOrgSelected] = useState(
    initialOrg ?? organizationList[0].name,
  );

  const handleClick = useCallback(
    (setShowDropList?: React.Dispatch<React.SetStateAction<boolean>>): void => {
      setDisplayOrganizations(!displayOrganizations);
      if (
        setShowDropList &&
        typeof setShowDropList === "function" &&
        organizationList.length > ORG_LENGTH
      ) {
        setShowDropList(false);
      }
    },
    [displayOrganizations, organizationList.length],
  );

  const handleClickItem = useCallback(
    (organizationName: string): (() => void) =>
      (): void => {
        setOrgSelected(organizationName);
        handleOrganizationChange(organizationName);
      },
    [handleOrganizationChange, setOrgSelected],
  );

  const handleModalClickItem = useCallback(
    (organizationName: string): void => {
      setOrgSelected(organizationName);
      handleOrganizationChange(organizationName);
      handleClick();
    },
    [handleClick, handleOrganizationChange],
  );

  const openAddOrganizationModal = useCallback((): void => {
    modalProps.open();
    handleClick();
  }, [handleClick, modalProps]);

  const formatName = (orgName: string): string => {
    const formatedOrgName = `${orgName[0].toUpperCase()}${orgName.slice(1)}`;

    return formatedOrgName;
  };

  useEffect((): VoidFunction => {
    const handleOutSideClick = (event: MouseEvent): void => {
      if (
        dropDownRef.current &&
        !dropDownRef.current.contains(event.target as Node) &&
        organizationList.length <= ORG_LENGTH
      ) {
        setDisplayOrganizations(false);
      }
    };

    document.addEventListener("mousedown", handleOutSideClick);

    return (): void => {
      document.removeEventListener("mousedown", handleOutSideClick);
    };
  }, [displayOrganizations, organizationList.length]);

  useEffect((): void => {
    if (initialOrg !== undefined) {
      setOrgSelected(initialOrg);
    }
  }, [initialOrg]);

  const orgItems = organizationList.map((org): IDropDownOption => {
    return {
      header: formatName(org.name),
      onClick: handleClickItem(org.name),
      value: formatName(org.name),
    };
  });

  const dropdownItems: IDropDownOption[] = [
    ...orgItems,
    {
      header: t("sidebar.newOrganization.modal.title"),
      icon: "square-plus",
      onClick: openAddOrganizationModal,
      value: t("sidebar.newOrganization.modal.title"),
    },
  ];

  return (
    <Container
      alignItems={"center"}
      borderLeft={`1px solid ${theme.palette.gray[200]}`}
      display={"flex"}
      ml={0.5}
      pl={0.5}
    >
      <Text color={theme.palette.gray[400]} mr={0.5} size={"sm"}>
        {"Organization"}
      </Text>
      <Container>
        <Dropdown
          handleCollapse={handleClick}
          initialSelection={{ value: formatName(orgSelected) }}
          items={dropdownItems}
          width={"174px"}
        />
      </Container>
      {organizationList.length > ORG_LENGTH ? (
        <GroupSelector
          handleNewOrganization={openAddOrganizationModal}
          isOpen={displayOrganizations}
          items={organizationList}
          onClose={handleClick}
          onSelect={handleModalClickItem}
          selectedItem={orgSelected}
          title={t("navbar.organizationsModal.title")}
          variant={"organization-selector"}
        />
      ) : undefined}
      {modalProps.isOpen ? (
        <AddOrganizationModal modalProps={modalProps} />
      ) : undefined}
    </Container>
  );
};

export type { IDropdownProps };
export { OrganizationDropdown };
