import type { IUseModal } from "@fluidattacks/design";

interface IAddOrganizationModalProps {
  modalProps: IUseModal;
}

export type { IAddOrganizationModalProps };
