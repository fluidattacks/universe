import { Modal, useModal } from "@fluidattacks/design";
import { useContext } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { WelcomeForm } from "./form";

import { PopUpMobile } from "components/pop-up-mobile";
import { authContext } from "context/auth";
import { useTour, useWindowSize } from "hooks";
import { openUrl } from "utils/resource-helpers";

const MOBILE_BREAKPOINT = 768;
const WelcomeModal: React.FC = (): JSX.Element | null => {
  const { t } = useTranslation();
  const { trialAdmin, userInTrial } = useContext(authContext);
  const { tours } = useTour();
  const isOpen = !tours.welcome && Boolean(userInTrial);
  const { width } = useWindowSize();
  const modalProps = useModal("welcome-modal-fp");

  if (isOpen) {
    if (width < MOBILE_BREAKPOINT) {
      return (
        <PopUpMobile
          confirmButton={{
            onClick: (): void => {
              openUrl("https://fluidattacks.com/");
            },
            text: t("autoenrollment.welcome.mobile.button"),
          }}
          darkBackground={true}
          description={` ${t("autoenrollment.welcome.mobile.description")}`}
          highlightDescription={[
            t("autoenrollment.welcome.mobile.highlights.1"),
            t("autoenrollment.welcome.mobile.highlights.2"),
          ]}
          image={{
            imageAlt: "viewMobileError",
            imageSrc: "integrates/resources/radarMobileAnimated",
            imageWidth: "175px",
          }}
          title={t("autoenrollment.welcome.mobile.title")}
          width={"335px"}
        />
      );
    }

    if (trialAdmin === true) {
      return (
        <Modal
          id={"welcome-modal"}
          modalRef={{ ...modalProps, isOpen: true }}
          size={"md"}
        >
          <WelcomeForm />
        </Modal>
      );
    }
  }

  return null;
};

export { WelcomeModal };
