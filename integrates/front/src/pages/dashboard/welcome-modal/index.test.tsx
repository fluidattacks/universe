import { screen, waitFor } from "@testing-library/react";
import { HttpResponse, type StrictResponse, graphql } from "msw";

import { WelcomeModal } from ".";
import { authContext } from "context/auth";
import type { UpdateToursMutation } from "gql/graphql";
import { UPDATE_TOURS } from "hooks/queries";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";

describe("welcomeModalFP", (): void => {
  const graphqlMocked = graphql.link(LINK);

  const updateToursMutation = graphqlMocked.mutation(
    UPDATE_TOURS,
    (): StrictResponse<IErrorMessage | { data: UpdateToursMutation }> => {
      return HttpResponse.json({
        data: {
          updateTours: { success: true },
        },
      });
    },
  );

  it("should render a component", async (): Promise<void> => {
    expect.hasAssertions();

    const setUserMock = jest.fn();

    render(
      <authContext.Provider
        value={{
          awsSubscription: null,
          setUser: setUserMock,
          tours: {
            newGroup: false,
            newRoot: false,
            welcome: false,
          },
          trialAdmin: true,
          userEmail: "",
          userInTrial: true,
          userName: "",
        }}
      >
        <WelcomeModal />
      </authContext.Provider>,
      {
        mocks: [updateToursMutation],
      },
    );

    await waitFor((): void => {
      expect(document.getElementById("welcome-form")).toBeDisabled();
    });
  });

  it("should not render a component", (): void => {
    expect.hasAssertions();

    const setUserMock = jest.fn();

    render(
      <authContext.Provider
        value={{
          awsSubscription: null,
          setUser: setUserMock,
          tours: {
            newGroup: false,
            newRoot: false,
            welcome: true,
          },
          userEmail: "",
          userName: "",
        }}
      >
        <WelcomeModal />
      </authContext.Provider>,
      {
        mocks: [updateToursMutation],
      },
    );

    expect(
      screen.queryByText("autoenrollment.welcome.modal.button"),
    ).not.toBeInTheDocument();
  });

  it("should not render without trial", (): void => {
    expect.hasAssertions();

    const setUserMock = jest.fn();

    render(
      <authContext.Provider
        value={{
          awsSubscription: null,
          setUser: setUserMock,
          tours: {
            newGroup: false,
            newRoot: false,
            welcome: false,
          },
          userEmail: "",
          userInTrial: false,
          userName: "",
        }}
      >
        <WelcomeModal />
      </authContext.Provider>,
      {
        mocks: [updateToursMutation],
      },
    );

    expect(
      screen.queryByText("autoenrollment.welcome.modal.button"),
    ).not.toBeInTheDocument();
  });

  it("should render a mobile component", (): void => {
    expect.hasAssertions();

    // eslint-disable-next-line functional/immutable-data
    Object.defineProperty(window, "innerWidth", { value: 700 });
    const setUserMock = jest.fn();

    render(
      <authContext.Provider
        value={{
          awsSubscription: null,
          setUser: setUserMock,
          tours: {
            newGroup: false,
            newRoot: false,
            welcome: false,
          },
          userEmail: "",
          userInTrial: true,
          userName: "",
        }}
      >
        <WelcomeModal />
      </authContext.Provider>,
      {
        mocks: [updateToursMutation],
      },
    );

    const welcomeBtn = screen.getByText("autoenrollment.welcome.mobile.title");

    expect(welcomeBtn).toBeInTheDocument();
  });
});
