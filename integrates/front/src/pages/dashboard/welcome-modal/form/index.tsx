import { useMutation } from "@apollo/client";
import {
  CardWithSelector,
  Container,
  Form,
  InnerForm,
  Lottie,
  PhoneInput as PhoneField,
  Text,
} from "@fluidattacks/design";
import { useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

import type { IPhoneAttr } from "features/verify-dialog/types";
import type { PhoneInput, TrialReason } from "gql/graphql";
import { useTour } from "hooks";
import { useGetStakeholderGroupsQuery } from "pages/auto-enrollment/hooks/get-stakeholder-groups";
import { SEND_NEW_ENROLLED_USER } from "pages/auto-enrollment/queries";
import {
  UPDATE_ENROLLMENT_MUTATION,
  UPDATE_STAKEHOLDER_MUTATION,
} from "pages/group/findings/queries";
import { welcomeValidationSchema } from "pages/group/findings/validations";
import { useGroupUrl } from "pages/group/hooks";
import { radar } from "resources";
import { Logger } from "utils/logger";

const WelcomeForm: React.FC = (): JSX.Element => {
  const { t } = useTranslation();
  const { setCompleted } = useTour();
  const { params } = useGroupUrl();
  const { groupName } = params;
  const { data } = useGetStakeholderGroupsQuery();
  const theme = useTheme();

  const [updateTrialReason] = useMutation(UPDATE_ENROLLMENT_MUTATION, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.error("Couldn't update enrollment reason", error);
      });
    },
  });

  const [updateStakeholderPhone] = useMutation(UPDATE_STAKEHOLDER_MUTATION, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.error("Couldn't update stakeholder phone", error);
      });
    },
  });

  const [sendNewEnrolledUserNotification] = useMutation(
    SEND_NEW_ENROLLED_USER,
    {
      context: { skipGlobalErrorHandler: true },
      onError: (updateError): void => {
        updateError.graphQLErrors.forEach(({ message }): void => {
          Logger.warning("An error occurred sending the notification", message);
        });
      },
    },
  );

  const handleSubmit = useCallback(
    async (values: { phone: string; reason: string }): Promise<void> => {
      try {
        const phone = values.phone as unknown as IPhoneAttr;

        await Promise.all([
          updateTrialReason({
            variables: {
              reason: values.reason as TrialReason,
            },
          }),
          updateStakeholderPhone({
            variables: {
              newPhone: {
                callingCountryCode: phone.callingCountryCode,
                nationalNumber: phone.nationalNumber,
              } as PhoneInput,
            },
          }),
        ]);
        if (groupName === undefined) {
          const group = data.me.group.name;
          await sendNewEnrolledUserNotification({
            variables: { groupName: group },
          });
        } else {
          await sendNewEnrolledUserNotification({
            variables: { groupName },
          });
        }
      } catch (error) {
        Logger.error("Error in submitting form", error);
      } finally {
        setCompleted("welcome");
      }
    },
    [
      updateTrialReason,
      updateStakeholderPhone,
      groupName,
      setCompleted,
      data.me.group.name,
      sendNewEnrolledUserNotification,
    ],
  );

  return (
    <Form
      buttonAlignment={"end"}
      confirmButton={{ label: t("autoenrollment.welcome.modal.button") }}
      defaultValues={{
        phone: "1",
        reason: "",
      }}
      id={"welcome-form"}
      onSubmit={handleSubmit}
      yupSchema={welcomeValidationSchema}
    >
      <InnerForm>
        {({ formState: { errors }, register, watch }): JSX.Element => (
          <Container minHeight={"330px"} my={1}>
            <Container
              alignItems={"center"}
              borderBottom={`2px solid ${theme.palette.gray[800]}`}
              display={"flex"}
              flexDirection={"row"}
              gap={1.25}
              justify={"flex-start"}
            >
              <Lottie animationData={radar} size={98} />
              <Container>
                <Text fontWeight={"bold"} size={"md"}>
                  {t("autoenrollment.welcome.modal.title2")}
                </Text>
                <Text size={"sm"}>
                  {t("autoenrollment.welcome.modal.description")}
                </Text>
              </Container>
            </Container>
            <Text fontWeight={"bold"} mb={0.75} mt={0.75} size={"sm"}>
              {t("autoenrollment.welcome.modal.reasons.title")}
            </Text>
            <Container alignItems={"center"} gap={0.5} mb={2}>
              <Container display={"flex"} flexDirection={"row"} gap={0.75}>
                <CardWithSelector
                  imageId={"integrates/autoenrollment/infinite-icon"}
                  register={register}
                  selectorProps={{ name: "reason", value: "DEVELOP_SECURE" }}
                  title={t(
                    "autoenrollment.welcome.modal.reasons.developSecure",
                  )}
                  width={"33%"}
                />
                <CardWithSelector
                  imageId={"integrates/autoenrollment/cloud-check-icon"}
                  register={register}
                  selectorProps={{
                    name: "reason",
                    value: "VALIDATE_FRAMEWORK",
                  }}
                  title={t(
                    "autoenrollment.welcome.modal.reasons.validateFramework",
                  )}
                  width={"33%"}
                />
                <CardWithSelector
                  imageId={"integrates/autoenrollment/certificate-icon"}
                  register={register}
                  selectorProps={{ name: "reason", value: "COMPLIANCE" }}
                  title={t("autoenrollment.welcome.modal.reasons.compliance")}
                  width={"33%"}
                />
              </Container>
              {"reason" in errors ? (
                <Text color={theme.palette.error[500]} size={"sm"}>
                  {`* ${errors.reason?.message?.toString()}`}
                </Text>
              ) : undefined}
            </Container>
            <Container
              display={"flex"}
              style={{ margin: "0 auto" }}
              width={"45%"}
            >
              <PhoneField
                error={errors.phone?.message?.toString()}
                label={t("autoenrollment.welcome.modal.reasons.phone")}
                name={"phone"}
                register={register}
                required={true}
                watch={watch}
              />
            </Container>
          </Container>
        )}
      </InnerForm>
    </Form>
  );
};

export { WelcomeForm };
