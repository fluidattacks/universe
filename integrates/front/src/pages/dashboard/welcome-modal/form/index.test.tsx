import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import { HttpResponse, type StrictResponse, graphql } from "msw";

import { WelcomeForm } from ".";
import { authContext } from "context/auth";
import {
  type SendNewEnrolledUserMutation,
  TrialReason,
  type UpdateToursMutation,
} from "gql/graphql";
import { UPDATE_TOURS } from "hooks/queries";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";
import { SEND_NEW_ENROLLED_USER } from "pages/auto-enrollment/queries";
import {
  UPDATE_ENROLLMENT_MUTATION,
  UPDATE_STAKEHOLDER_MUTATION,
} from "pages/group/findings/queries";

describe("welcomeForm", (): void => {
  const graphqlMocked = graphql.link(LINK);

  const mocks = [
    graphqlMocked.mutation(
      SEND_NEW_ENROLLED_USER,
      ({
        variables,
      }): StrictResponse<
        IErrorMessage | { data: SendNewEnrolledUserMutation }
      > => {
        const { groupName } = variables;
        if (groupName === "TEST") {
          return HttpResponse.json({
            data: {
              sendNewEnrolledUser: { success: true },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error sending notification")],
        });
      },
    ),
    graphqlMocked.mutation(
      UPDATE_ENROLLMENT_MUTATION,
      ({
        variables,
      }): StrictResponse<
        | IErrorMessage
        | {
            data: {
              __typename?: "Mutation";
              updateEnrollment: {
                __typename?: "SimplePayload";
                success: boolean;
              };
            };
          }
      > => {
        const { reason } = variables;
        if (reason === TrialReason.DevelopSecure) {
          return HttpResponse.json({
            data: {
              updateEnrollment: { success: true },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error updating enrollment")],
        });
      },
    ),
    graphqlMocked.mutation(
      UPDATE_STAKEHOLDER_MUTATION,
      ({
        variables,
      }): StrictResponse<
        | IErrorMessage
        | {
            data: {
              __typename?: "Mutation";
              updateStakeholder: {
                __typename?: "SimplePayload";
                success: boolean;
              };
            };
          }
      > => {
        const { newPhone } = variables;
        if (newPhone.callingCountryCode === "+57") {
          return HttpResponse.json({
            data: {
              updateStakeholder: { success: true },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error updating phone")],
        });
      },
    ),
    graphqlMocked.mutation(
      UPDATE_TOURS,
      (): StrictResponse<IErrorMessage | { data: UpdateToursMutation }> => {
        return HttpResponse.json({
          data: {
            updateTours: { success: true },
          },
        });
      },
    ),
  ];

  it("should render a component", async (): Promise<void> => {
    expect.hasAssertions();

    render(<WelcomeForm />);

    await waitFor((): void => {
      expect(
        screen.getByText("autoenrollment.welcome.modal.title2"),
      ).toBeInTheDocument();
    });
  });

  it("should send notification", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <authContext.Provider
        value={{
          awsSubscription: null,
          setUser: jest.fn(),
          tours: {
            newGroup: false,
            newRoot: false,
            welcome: false,
          },
          userEmail: "test@test.test",
          userName: "test",
        }}
      >
        <WelcomeForm />
      </authContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/TEST/vulns"],
        },
        mocks,
      },
    );

    await waitFor((): void => {
      expect(
        screen.getByText("autoenrollment.welcome.modal.title2"),
      ).toBeInTheDocument();
    });

    await userEvent.type(
      document.querySelectorAll("input[name='phone']")[0],
      "+573202587410",
    );

    const radio = screen.getByDisplayValue("DEVELOP_SECURE");
    await userEvent.click(radio);

    const welcomeBtn = screen.getByText("autoenrollment.welcome.modal.button");

    await waitFor((): void => {
      expect(welcomeBtn).not.toBeDisabled();
    });
    await userEvent.click(welcomeBtn);
  });

  it("should return errors", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <authContext.Provider
        value={{
          awsSubscription: null,
          setUser: jest.fn(),
          tours: {
            newGroup: false,
            newRoot: false,
            welcome: false,
          },
          userEmail: "test@test.test",
          userName: "test",
        }}
      >
        <WelcomeForm />
      </authContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/GROUP/vulns"],
        },
        mocks,
      },
    );

    await waitFor((): void => {
      expect(
        screen.getByText("autoenrollment.welcome.modal.title2"),
      ).toBeInTheDocument();
    });

    await userEvent.type(
      document.querySelectorAll("input[name='phone']")[0],
      "+603202587410",
    );

    const radio = screen.getByDisplayValue("COMPLIANCE");
    await userEvent.click(radio);

    const welcomeBtn = screen.getByText("autoenrollment.welcome.modal.button");

    await waitFor((): void => {
      expect(welcomeBtn).not.toBeDisabled();
    });
    await userEvent.click(welcomeBtn);
  });
});
