import { graphql } from "gql";

const GET_ORGANIZATION_ID_DATA = graphql(`
  query GetOrganizationIdData($organizationName: String!) {
    organizationId(organizationName: $organizationName) {
      id
      hasZtnaRoots
      name
    }
  }
`);

export { GET_ORGANIZATION_ID_DATA };
