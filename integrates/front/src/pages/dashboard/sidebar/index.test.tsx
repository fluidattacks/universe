import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import { Route, Routes } from "react-router-dom";

import { Sidebar } from ".";
import type { GetGroupServicesQuery as GetGroupServices } from "gql/graphql";
import { GET_GROUP_SERVICES } from "hooks/queries";
import { render } from "mocks";
import { LINK } from "mocks/constants";

describe("sidebar", (): void => {
  const graphqlMocked = graphql.link(LINK);
  const memoryRouter = {
    initialEntries: ["/orgs/okada/groups"],
  };
  const memoryRouterGroup = {
    initialEntries: ["/orgs/okada/groups/unittesting/vulns"],
  };

  const groupMock = [
    graphqlMocked.query(
      GET_GROUP_SERVICES,
      (): StrictResponse<{ data: GetGroupServices }> => {
        return HttpResponse.json({
          data: {
            group: {
              __typename: "Group",
              name: "unittesting",
              serviceAttributes: [],
            },
          },
        });
      },
    ),
  ];

  it("should render sidebar component", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    render(
      <Routes>
        <Route
          element={<Sidebar organizations={["okada"]} />}
          path={"/orgs/:organizationName/*"}
        />
      </Routes>,
      {
        memoryRouter,
      },
    );
    const appLogo = screen.getAllByRole("link");
    const helpButton = screen.getAllByText("components.sideBar.help.title");

    await waitFor((): void => {
      expect(appLogo[0]).toHaveAttribute("href", "/orgs/okada/groups");
    });

    expect(appLogo[1]).toHaveAttribute("href", "/orgs/okada/analytics");
    expect(appLogo[2]).toHaveAttribute("href", "/orgs/okada/portfolios");
    expect(appLogo[3]).toHaveAttribute("href", "/orgs/okada/policies");
    expect(appLogo[4]).toHaveAttribute("href", "/orgs/okada/outside");
    expect(appLogo[5]).toHaveAttribute("href", "/orgs/okada/credentials");
    expect(appLogo[6]).toHaveAttribute("href", "/orgs/okada/compliance");

    expect(helpButton[0]).toBeInTheDocument();

    await userEvent.click(helpButton[0]);

    expect(
      screen.queryByText("components.sideBar.help.expert.title"),
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText("components.sideBar.help.email.title"),
    ).toBeInTheDocument();
    expect(
      screen.queryByText("components.sideBar.help.learn.title"),
    ).toBeInTheDocument();
    expect(
      screen.queryByText("components.sideBar.help.center.title"),
    ).toBeInTheDocument();
  });

  it("should render talk to hacker when in group", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    render(<Sidebar organizations={["okada"]} />, {
      memoryRouter: memoryRouterGroup,
      mocks: groupMock,
    });
    const helpButton = screen.getAllByText("components.sideBar.help.title");

    expect(helpButton[0]).toBeInTheDocument();

    await userEvent.click(helpButton[0]);

    expect(
      screen.queryByText("components.sideBar.help.expert.title"),
    ).toBeInTheDocument();
  });
});
