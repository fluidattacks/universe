import { useQuery } from "@apollo/client";
import type { IMenuItemProps } from "@fluidattacks/design";
import isNil from "lodash/isNil";
import mixpanel from "mixpanel-browser";
import { useCallback, useMemo } from "react";
import { useTranslation } from "react-i18next";
import { useMatch } from "react-router-dom";

import { GET_ORGANIZATION_ID_DATA } from "./queries";

import { SideBar } from "components/side-bar";
import type { ISideBarButton } from "components/side-bar/types";
import {
  authzPermissionsContext,
  organizationLevelPermissions,
} from "context/authz/config";
import { LearningModal } from "features/learning-modal";
import { TalkToHackerModal } from "features/talk-to-a-hacker";
import { UpgradeFreeTrialModal } from "features/upgrade-free-trial-users";
import { UpgradeGroupsModal } from "features/upgrade-groups-modal";
import { ManagedType } from "gql/graphql";
import { useAudit } from "hooks/use-audit";
import { useCalendly } from "hooks/use-calendly";
import { useHelpState } from "hooks/use-help-state";
import type { IHelpState } from "hooks/use-help-state";
import { useTrial } from "hooks/use-trial";
import { useZohoChat } from "hooks/use-zoho-chat";
import { getOrgFromLocalStorage, getOrgFromUrl } from "hooks/utils";
import { onlineStatusBadge } from "utils/format-helpers";
import { Logger } from "utils/logger";
import { openUrl } from "utils/resource-helpers";

const talkToAHackerAdvancedImg = "integrates/plans/talk-to-a-hacker";

const Sidebar = ({
  organizations,
}: Readonly<{ organizations: string[] }>): JSX.Element => {
  const trial = useTrial();
  const {
    isAdvancedActive,
    isAvailable,
    openUpgradeModal,
    isUpgradeOpen,
    closeUpgradeModal,
  } = useCalendly();

  const orgMatch = useMatch("/orgs/:organizationName/*");
  const { organizationName } = orgMatch?.params ?? {};
  const orgName =
    getOrgFromUrl(organizationName, organizations) ??
    getOrgFromLocalStorage(organizations) ??
    organizations[0];

  const { t } = useTranslation();
  const { data: orgData } = useQuery(GET_ORGANIZATION_ID_DATA, {
    fetchPolicy: "cache-first",
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.error("Couldn't load organization data", error);
      });
    },
    skip: isNil(orgName),
    variables: {
      organizationName: orgName,
    },
  });

  // Help items
  const isTalkToHackerOpen = useHelpState(
    (state): boolean => state.isTalkToHackerOpen,
  );
  const updateTalkToHacker = useHelpState(
    (state): ((isTalkToHackerOpen: IHelpState["isTalkToHackerOpen"]) => void) =>
      state.setIsTalkToHackerOpen,
  );
  const isLearningOpen = useHelpState((state): boolean => state.isLearningOpen);
  const updateLearning = useHelpState(
    (state): ((isLearningOpen: IHelpState["isLearningOpen"]) => void) =>
      state.setIsLearningOpen,
  );
  const { openChat, isChatOnline } = useZohoChat();

  const openTalkToHackerModal = useCallback((): void => {
    if (isAdvancedActive && isAvailable) {
      updateTalkToHacker(true);
    } else {
      openUpgradeModal();
    }
  }, [isAvailable, isAdvancedActive, openUpgradeModal, updateTalkToHacker]);
  const closeTalkToHackerModal = useCallback((): void => {
    updateTalkToHacker(false);
  }, [updateTalkToHacker]);
  const openLearningModal = useCallback((): void => {
    updateLearning(true);
  }, [updateLearning]);
  const closeLearningModal = useCallback((): void => {
    updateLearning(false);
  }, [updateLearning]);
  const openHelpEmail = useCallback((): void => {
    openUrl("mailto:help@fluidattacks.com", false);
  }, []);
  const openHelpCenter = useCallback((): void => {
    openUrl("https://help.fluidattacks.com/portal/en/kb");
  }, []);
  const { addAuditEvent } = useAudit();
  const openZohoChat = useCallback((): void => {
    mixpanel.track("OpenLiveChat");
    addAuditEvent("LiveChat", "unknown");
    openChat();
  }, [addAuditEvent, openChat]);

  const talkToHackerItem: IMenuItemProps[] = [
    {
      description: t("components.sideBar.help.expert.description"),
      icon: "headphones",
      onClick: openTalkToHackerModal,
      requiresUpgrade: !(isAdvancedActive && isAvailable),
      title: t("components.sideBar.help.expert.title"),
    },
  ];
  const helpItems: IMenuItemProps[] = [
    {
      customBadge: onlineStatusBadge(isChatOnline),
      description: t("components.sideBar.help.chat.description"),
      icon: "message",
      onClick: openZohoChat,
      title: t("components.sideBar.help.chat.title"),
    },
    {
      description: t("components.sideBar.help.learn.description"),
      icon: "life-ring",
      onClick: openLearningModal,
      title: t("components.sideBar.help.learn.title"),
    },
    {
      description: t("components.sideBar.help.center.description"),
      icon: "book",
      onClick: openHelpCenter,
      title: t("components.sideBar.help.center.title"),
    },
    {
      description: t("components.sideBar.help.email.description"),
      icon: "envelope",
      onClick: openHelpEmail,
      title: t("components.sideBar.help.email.title"),
    },
  ];

  // Side bar items
  const sideBarItems = useMemo(
    (): ISideBarButton[] => [
      {
        icon: "folder",
        id: "groupsBtn",
        text: t("organization.tabs.groups.text"),
        to: `/orgs/${orgName}/groups`,
      },
      {
        icon: "chart-simple",
        id: "analyticsBtn",
        text: t("organization.tabs.analytics.text"),
        to: `/orgs/${orgName}/analytics`,
      },
      {
        icon: "briefcase",
        id: "portfoliosBtn",
        text: t("organization.tabs.portfolios.text"),
        to: `/orgs/${orgName}/portfolios`,
      },
      {
        canDo: "integrates_api_resolvers_organization_stakeholders_resolve",
        icon: "user-plus",
        id: "membersBtn",
        text: t("organization.tabs.users.text"),
        to: `/orgs/${orgName}/members`,
      },
      {
        icon: "file-shield",
        id: "policiesBtn",
        text: t("organization.tabs.policies.text"),
        to: `/orgs/${orgName}/policies`,
      },
      {
        canDo: "integrates_api_resolvers_query_mailmap_entries_resolve",
        icon: "envelope",
        id: "mailmapBtn",
        text: t("Mailmap"),
        to: `/orgs/${orgName}/mailmap`,
      },
      {
        canDo: "integrates_api_resolvers_organization_billing_resolve",
        icon: "credit-card",
        id: "billingBtn",
        text: t("organization.tabs.billing.text"),
        to: `/orgs/${orgName}/billing`,
      },
      {
        icon: "hexagon-exclamation",
        id: "outsideBtn",
        text: t("organization.tabs.weakest.text"),
        to: `/orgs/${orgName}/outside`,
      },
      {
        icon: "key",
        id: "credentialsBtn",
        text: t("organization.tabs.credentials.text"),
        to: `/orgs/${orgName}/credentials`,
      },
      {
        icon: "shield-check",
        id: "complianceBtn",
        text: t("organization.tabs.compliance.text"),
        to: `/orgs/${orgName}/compliance`,
      },
      {
        canDo: "see_ztna_logs",
        hide: orgData?.organizationId.hasZtnaRoots === false,
        icon: "file-lines",
        id: "ztnaLogsBtn",
        text: t("organization.tabs.ztna.text"),
        to: `/orgs/${orgName}/logs`,
      },
    ],
    [orgName, orgData?.organizationId.hasZtnaRoots, t],
  );

  return (
    <authzPermissionsContext.Provider value={organizationLevelPermissions}>
      <SideBar
        helpItems={isAvailable ? talkToHackerItem.concat(helpItems) : helpItems}
        sideBarItems={sideBarItems}
      />
      {isTalkToHackerOpen ? (
        <TalkToHackerModal closeTalkToHackerModal={closeTalkToHackerModal} />
      ) : undefined}
      {isUpgradeOpen && trial?.trial.state === ManagedType.Trial && (
        <UpgradeFreeTrialModal
          description={t("components.sideBar.help.expert.trial.description")}
          img={talkToAHackerAdvancedImg}
          onClose={closeUpgradeModal}
          title={t("components.sideBar.help.expert.trial.title")}
        />
      )}
      {isUpgradeOpen && trial === null && (
        <UpgradeGroupsModal onClose={closeUpgradeModal} />
      )}
      {isLearningOpen ? (
        <LearningModal closeLearningModal={closeLearningModal} />
      ) : undefined}
    </authzPermissionsContext.Provider>
  );
};

export { Sidebar };
