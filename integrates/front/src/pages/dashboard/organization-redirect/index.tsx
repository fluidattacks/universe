import { useQuery } from "@apollo/client";
import * as React from "react";
import { Navigate, useLocation, useParams } from "react-router-dom";

import { GET_GROUP_ORGANIZATION, GET_PORTFOLIO_ORGANIZATION } from "./queries";

interface IOrganizationRedirectProps {
  readonly type: "group" | "portfolio";
}

/**
 * In the past, paths did not include organization names in them.
 * This component queries the organization name and redirects accordingly.
 */
const OrganizationRedirect: React.FC<IOrganizationRedirectProps> = ({
  type,
}): JSX.Element => {
  const { pathname } = useLocation();
  const { groupName, tagName } = useParams<{
    groupName: string;
    tagName: string;
  }>();

  const { data: groupData } = useQuery(GET_GROUP_ORGANIZATION, {
    skip: type !== "group",
    variables: { groupName: groupName ?? "" },
  });
  const { data: portfolioData } = useQuery(GET_PORTFOLIO_ORGANIZATION, {
    skip: type !== "portfolio",
    variables: { tagName: tagName ?? "" },
  });

  if (type === "group" && groupData?.group) {
    const { organization } = groupData.group;

    return <Navigate to={`/orgs/${organization}${pathname}`} />;
  }

  if (type === "portfolio" && portfolioData?.tag) {
    const { organization } = portfolioData.tag;

    return <Navigate to={`/orgs/${organization}${pathname}`} />;
  }

  return <div />;
};

export { OrganizationRedirect };
