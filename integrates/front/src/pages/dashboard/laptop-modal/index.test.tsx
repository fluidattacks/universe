import { screen } from "@testing-library/react";

import { LaptopModal } from ".";
import { render } from "mocks";
import { openUrl } from "utils/resource-helpers";

describe("laptopModal", (): void => {
  it("should render a component", (): void => {
    expect.hasAssertions();

    render(<LaptopModal isOpen={true} />);

    expect(
      screen.queryByText("dashboard.minimumWidth.title"),
    ).toBeInTheDocument();
  });

  it("should not render a component", (): void => {
    expect.hasAssertions();

    render(<LaptopModal isOpen={false} />);

    expect(
      screen.queryByText("dashboard.minimumWidth.title"),
    ).not.toBeInTheDocument();
  });

  it("should render if the website is open", (): void => {
    expect.hasAssertions();

    const props = {
      confirmButton: {
        onClick: (): void => {
          openUrl("https://fluidattacks.com/");
        },
      },
    };

    render(<LaptopModal isOpen={true} {...props} />);

    expect(
      screen.queryByText("dashboard.minimumWidth.button"),
    ).toBeInTheDocument();
  });
});
