import { Modal, useModal } from "@fluidattacks/design";
import { useCallback, useContext, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useIdleTimer } from "react-idle-timer";

import { authContext } from "context/auth";

/*
 * 5 min is recommended. More than that is assumed risk.
 * Check with the product owner before modifying these values
 *
 * {https://help.fluidattacks.com/portal/en/kb/articles/criteria-requirements-023}
 */
const SECONDS_IN_MINUTE = 60;
const MILLISECONDS_IN_SECOND = 1000;

const MINUTES_IN_MS = SECONDS_IN_MINUTE * MILLISECONDS_IN_SECOND;
const FIFTEEN_MINUTES = 15;
const ONE_MINUTE = 1;

const TIME_TO_IDLE_MS = FIFTEEN_MINUTES * MINUTES_IN_MS;
const TIME_TO_LOGOUT_MS = ONE_MINUTE * MINUTES_IN_MS;

const SessionIdle: React.FC = (): JSX.Element => {
  const { userEmail } = useContext(authContext);
  const [idleWarningOpen, setIdleWarningOpen] = useState(false);
  const { t } = useTranslation();
  const modalProps = useModal("session-idle");

  const { activate } = useIdleTimer({
    crossTab: true,
    onActive: (): void => {
      setIdleWarningOpen(false);
    },
    onIdle: (): void => {
      location.replace("/logout");
    },
    onPrompt: (): void => {
      setIdleWarningOpen(true);
    },
    promptBeforeIdle: TIME_TO_LOGOUT_MS,
    syncTimers: 500,
    timeout: userEmail.endsWith("@fluidattacks.com")
      ? 2 * TIME_TO_IDLE_MS
      : TIME_TO_IDLE_MS,
  });

  const dismissWarning = useCallback((): void => {
    activate();
  }, [activate]);

  return (
    <Modal
      confirmButton={{
        onClick: dismissWarning,
        text: t("validations.inactiveSessionDismiss"),
      }}
      modalRef={{
        ...modalProps,
        close: dismissWarning,
        isOpen: idleWarningOpen,
      }}
      size={"sm"}
      title={t("validations.inactiveSessionModal")}
    >
      <p>{t("validations.inactiveSession")}</p>
    </Modal>
  );
};

export { SessionIdle };
