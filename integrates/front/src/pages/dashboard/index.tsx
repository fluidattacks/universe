import { Container, MessageBanner, ScrollUpButton } from "@fluidattacks/design";
import { useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { Navigate, Outlet, Route, Routes, useMatch } from "react-router-dom";

import { useStartUrl } from "./hooks";
import { LaptopModal } from "./laptop-modal";
import { LegalNotice } from "./legal-notice";
import { useLegalNotice } from "./legal-notice/hooks";
import { Navbar } from "./navbar";
import { OrganizationRedirect } from "./organization-redirect";
import { SessionIdle } from "./session-idle";
import { Sidebar } from "./sidebar";
import { DashboardContainer, DashboardContent } from "./styles";
import { TrialTimeBanner } from "./trial-time-banner";
import { WelcomeModal } from "./welcome-modal";

import { Modal, ModalConfirm } from "components/modal";
import { AllGroupPermissionsProvider } from "context/authz/all-group-permissions-provider";
import { GroupLevelPermissionsProvider } from "context/authz/group-level-permissions-provider";
import { OrgLevelPermissionsProvider } from "context/authz/org-level-permissions-provider";
import { useInitialOrganization, useModal, useWindowSize } from "hooks";
import { Event } from "pages/event";
import { Finding } from "pages/finding";
import { Group } from "pages/group";
import { DockerImagePackages } from "pages/group/supply-chain/docker-image-packages";
import { PackageDetails } from "pages/group/supply-chain/package-details";
import { RootPackages } from "pages/group/supply-chain/root-packages";
import { Integrations } from "pages/integrations";
import { Organization } from "pages/organization";
import { OrganizationDataProvider } from "pages/organization/context";
import { Portfolio } from "pages/portfolio";
import { ToDo } from "pages/to-do";
import { User } from "pages/user";

/*
 * Main application paths
 * {@link https://v5.reactrouter.com/web/api/Route/path-string-string}
 */
const MOBILE_BREAKPOINT = 768;
const Dashboard: React.FC = (): JSX.Element => {
  const orgMatch = useMatch("/orgs/:organizationName/*");
  const { data, orgName, loading } = useInitialOrganization();
  const { acceptLegalNotice, legalNoticeOpen } = useLegalNotice();
  const { t } = useTranslation();
  const pendingModal = useModal("pending-invitations-modal");
  useStartUrl();
  const { width } = useWindowSize();

  const handleLogout = useCallback((): void => {
    location.replace("/logout");
  }, []);

  if (!loading && data.organizations.length === 0) {
    return (
      <Container>
        <Modal
          modalRef={{ ...pendingModal, isOpen: true }}
          size={"sm"}
          title={t("autoenrollment.pendingInvitation.title")}
        >
          {t("autoenrollment.pendingInvitation.message")}
          <ModalConfirm
            onConfirm={handleLogout}
            txtConfirm={t("navbar.logout")}
          />
        </Modal>
      </Container>
    );
  }

  if (loading || legalNoticeOpen === undefined) {
    return <div />;
  }

  if (legalNoticeOpen) {
    return <LegalNotice onAccept={acceptLegalNotice} />;
  }

  return (
    <DashboardContainer>
      {orgMatch === null ? null : <TrialTimeBanner />}
      <MessageBanner message={t("dashboard.banner.message1")} />
      <Navbar initialOrganization={orgName} />
      <div className={"flex flex-auto flex-row"}>
        <Sidebar organizations={data.organizations} />
        <DashboardContent id={"dashboard"} style={{ padding: "unset" }}>
          <Routes>
            <Route element={<Integrations />} path={"/integrations/*"} />
            <Route
              element={
                <OrgLevelPermissionsProvider>
                  <OrganizationDataProvider>
                    <Outlet />
                  </OrganizationDataProvider>
                </OrgLevelPermissionsProvider>
              }
              path={`/orgs/:organizationName/*`}
            >
              <Route
                element={
                  <GroupLevelPermissionsProvider>
                    <Outlet />
                  </GroupLevelPermissionsProvider>
                }
                path={"groups/:groupName/*"}
              >
                <Route element={<Event />} path={"events/:eventId/*"} />
                <Route element={<Finding />} path={"vulns/:findingId/*"} />
                <Route
                  element={<DockerImagePackages />}
                  path={"surface/packages/docker-images/:dockerImageId"}
                />
                <Route
                  element={<RootPackages />}
                  path={"surface/packages/roots/:rootId"}
                />
                <Route
                  element={<PackageDetails />}
                  path={"surface/packages/roots/:rootId/:packageId"}
                />
                <Route element={<Group />} path={"*"} />
              </Route>
              <Route element={<Portfolio />} path={"portfolios/:tagName/*"} />
              <Route element={<Organization />} path={"*"} />
            </Route>
            <Route
              element={
                <AllGroupPermissionsProvider>
                  <ToDo />
                </AllGroupPermissionsProvider>
              }
              path={"/todos/*"}
            />
            <Route element={<User />} path={"/user/*"} />
            {/* Legacy paths kept for backwards-compatibility */}
            <Route
              element={<OrganizationRedirect type={"group"} />}
              path={"/groups/:groupName/*"}
            />
            <Route
              element={<OrganizationRedirect type={"portfolio"} />}
              path={"/portfolios/:tagName/*"}
            />
            {/* Fallback if none of the paths were matched */}
            <Route element={<Navigate to={`/orgs/${orgName}`} />} path={"*"} />
          </Routes>
          <ScrollUpButton />
          <SessionIdle />
        </DashboardContent>
        <LaptopModal isOpen={width < MOBILE_BREAKPOINT} />
        <WelcomeModal />
      </div>
    </DashboardContainer>
  );
};

export { Dashboard };
