import { graphql } from "gql";

interface IUserRemember {
  me: {
    __typename: "Me";
    remember: boolean;
    userEmail: string;
  };
}

const GET_USER_REMEMBER = graphql(`
  query GetUserRemember {
    me {
      remember
      userEmail
    }
  }
`);

const ACCEPT_LEGAL_MUTATION = graphql(`
  mutation AcceptLegalMutation($remember: Boolean!) {
    acceptLegal(remember: $remember) {
      success
    }
  }
`);

export { ACCEPT_LEGAL_MUTATION, GET_USER_REMEMBER };
export type { IUserRemember };
