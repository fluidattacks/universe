import { Link } from "@fluidattacks/design";
import { Text } from "@fluidattacks/design";
import { useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { Checkbox } from "components/checkbox";
import { InnerForm } from "components/form";
import { FormModal } from "components/form-modal";
import { useModal } from "hooks/use-modal";

interface ILegalNoticeProps {
  readonly onAccept: (remember: boolean) => void;
}

const LegalNotice: React.FC<ILegalNoticeProps> = ({
  onAccept,
}): JSX.Element => {
  const { t } = useTranslation();
  const modalProps = useModal("legal-notice-modal");
  const currentYear = new Date().getFullYear();

  const handleSubmit = useCallback(
    (values: { remember: boolean }): void => {
      onAccept(values.remember);
    },
    [onAccept],
  );

  return (
    <FormModal
      initialValues={{ remember: false }}
      modalRef={{ ...modalProps, isOpen: true }}
      name={"acceptLegal"}
      onSubmit={handleSubmit}
      size={"lg"}
      title={t("legalNotice.title")}
    >
      <InnerForm submitButtonLabel={t("legalNotice.accept")}>
        <Text size={"sm"}>
          {t("legalNotice.description.legal", { currentYear })}
        </Text>
        <Text mb={0.75} size={"sm"}>
          {t("legalNotice.description.privacy")}
          <Link href={"https://fluidattacks.com/privacy/"}>
            {t("legalNotice.description.privacyLinkText")}
          </Link>
        </Text>
        <Checkbox
          label={t("legalNotice.rememberCbo.text")}
          name={"remember"}
          tooltip={t("legalNotice.rememberCbo.tooltip")}
          variant={"formikField"}
        />
      </InnerForm>
    </FormModal>
  );
};

export { LegalNotice };
