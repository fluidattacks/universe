import { useQuery } from "@apollo/client";
import { GroupSelector as GroupSelectorComp } from "@fluidattacks/design";
import sortBy from "lodash/sortBy";
import { useCallback } from "react";
import { useTranslation } from "react-i18next";
import { useNavigate, useParams } from "react-router-dom";

import { GET_ORGANIZATION_GROUP_NAMES } from "./queries";

import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";

const GroupSelector = ({
  isOpen,
  close,
}: Readonly<{
  isOpen: boolean;
  close: VoidFunction;
}>): JSX.Element => {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const { groupName, organizationName } = useParams() as {
    groupName: string;
    organizationName: string;
  };

  const { data: groupNamesData } = useQuery(GET_ORGANIZATION_GROUP_NAMES, {
    fetchPolicy: "cache-first",
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.warning("An error occurred fetching portfolios", error);
      });
    },
    variables: {
      organizationId: organizationName.toLowerCase(),
    },
  });

  const groupList = sortBy(groupNamesData?.organization.groups, ["name"]);
  const groupSelected = groupName.charAt(0).toUpperCase() + groupName.slice(1);
  const organization =
    organizationName.charAt(0).toUpperCase() + organizationName.slice(1);

  const handleGroupChange = useCallback(
    (group: string): void => {
      if (group !== groupSelected) {
        navigate(
          `/orgs/${organizationName}/groups/${group.toLowerCase()}/vulns`,
        );
      }
      close();
    },
    [close, groupSelected, navigate, organizationName],
  );

  return (
    <GroupSelectorComp
      isOpen={isOpen}
      items={groupList as { name: string }[]}
      onClose={close}
      onSelect={handleGroupChange}
      selectedItem={groupSelected}
      title={t("group.groupSelector.title", { organization })}
    />
  );
};

export { GroupSelector };
