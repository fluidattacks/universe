import { graphql } from "gql";

const GET_ORGANIZATION_GROUP_NAMES = graphql(`
  query GetOrganizationGroupNames($organizationId: String!) {
    organization(organizationId: $organizationId) {
      name
      groups {
        name
      }
    }
  }
`);

export { GET_ORGANIZATION_GROUP_NAMES };
