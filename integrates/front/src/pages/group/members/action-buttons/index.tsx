import { Button } from "@fluidattacks/design";
import { Fragment } from "react";
import { useTranslation } from "react-i18next";

import { Can } from "context/authz/can";

const ActionsButtons = ({
  areButtonsDisabled,
  handleRemoveUser,
  areThereMultipleSelections,
  openEditUserModal,
}: Readonly<{
  areButtonsDisabled: boolean;
  handleRemoveUser: () => Promise<void>;
  areThereMultipleSelections: boolean;
  openEditUserModal: VoidFunction;
}>): JSX.Element => {
  const { t } = useTranslation();

  return (
    <Fragment>
      <Can do={"integrates_api_mutations_update_group_stakeholder_mutate"}>
        <Button
          disabled={areButtonsDisabled || areThereMultipleSelections}
          icon={"edit"}
          id={"editUser"}
          onClick={openEditUserModal}
          tooltip={
            areThereMultipleSelections
              ? t("searchFindings.tabUsers.editButton.disabled")
              : t("searchFindings.tabUsers.editButton.tooltip")
          }
          variant={"ghost"}
        >
          {t("organization.tabs.users.editButton.text")}
        </Button>
      </Can>
      <Can do={"integrates_api_mutations_remove_stakeholder_access_mutate"}>
        <Button
          disabled={areButtonsDisabled}
          icon={"trash"}
          id={"removeUser"}
          onClick={handleRemoveUser}
          tooltip={t("searchFindings.tabUsers.removeUserButton.tooltip")}
          variant={"ghost"}
        >
          {t("organization.tabs.users.removeButton.text")}
        </Button>
      </Can>
    </Fragment>
  );
};

export { ActionsButtons };
