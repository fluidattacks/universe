import { Alert, Text } from "@fluidattacks/design";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

import { usePolicies } from "hooks/use-policies";

interface IMembersAlertProps {
  organizationId: string;
}

export const MembersAlert = ({
  organizationId,
}: Readonly<IMembersAlertProps>): JSX.Element => {
  const { t } = useTranslation();
  const theme = useTheme();
  const { orgPolicies, loadingPolicies } = usePolicies(organizationId);

  return (
    <Alert closable={true} show={!loadingPolicies} variant={"info"}>
      <Text color={theme.palette.info["700"]} fontWeight={"bold"} size={"xs"}>
        {t("organization.members.policiesAlert", {
          inactivityPeriod: orgPolicies?.organization.inactivityPeriod ?? 0,
        })}
      </Text>
    </Alert>
  );
};
