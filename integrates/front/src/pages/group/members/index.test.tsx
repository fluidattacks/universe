import { PureAbility } from "@casl/ability";
import { fireEvent, screen, waitFor, within } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import dayjs, { extend } from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";
import { GraphQLError } from "graphql";
import isEmpty from "lodash/isEmpty";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import { Route, Routes } from "react-router-dom";

import {
  ADD_STAKEHOLDER_MUTATION,
  REMOVE_STAKEHOLDER_MUTATION,
  UPDATE_GROUP_STAKEHOLDER_MUTATION,
} from "./queries";

import { GroupMembers } from ".";
import { authzPermissionsContext } from "context/authz/config";
import type {
  GrantStakeholderMutationMutation as AddStakeholder,
  GetStakeholdersQuery as GetStakeholders,
  RemoveStakeholderAccessMutationMutation as RemoveStakeholderAccess,
  UpdateGroupStakeholderMutationMutation as UpdateGroupStakeholder,
} from "gql/graphql";
import { InvitationState, StakeholderRole } from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage, IMessage } from "mocks/types";
import { GET_STAKEHOLDERS } from "pages/group/queries";
import { msgError, msgSuccess } from "utils/notifications";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

const timeFromNow = (value: string): string => {
  const date = new Date(value);
  if (isEmpty(value) || isNaN(date.getTime())) return "-";

  extend(relativeTime);

  return dayjs(value, "YYYY-MM-DD hh:mm:ss A").fromNow();
};

describe("group members view", (): void => {
  const graphqlMocked = graphql.link(LINK);

  const memoryRouter = {
    initialEntries: ["/groups/TEST/members"],
  };

  it("should render an error in component", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route element={<GroupMembers />} path={"/groups/:groupName/members"} />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/groups/TEST1/members"],
        },
      },
    );

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledTimes(1);
    });

    jest.clearAllMocks();
  });

  it("should display all group stakeholder columns", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route element={<GroupMembers />} path={"/groups/:groupName/members"} />
      </Routes>,
      {
        memoryRouter,
      },
    );

    await waitFor((): void => {
      expect(
        screen.queryByText("organization.members.policiesAlert"),
      ).toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(screen.queryByRole("table")).toBeInTheDocument();
    });

    expect(
      screen.getByText("searchFindings.usersTable.usermail"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("searchFindings.usersTable.userRole"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("searchFindings.usersTable.userResponsibility"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("searchFindings.usersTable.firstlogin"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("searchFindings.usersTable.lastlogin"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("searchFindings.usersTable.invitation"),
    ).toBeInTheDocument();
    await expect(
      screen.findByText("user@gmail.com"),
    ).resolves.toBeInTheDocument();
    await expect(
      screen.findByText("userModal.roles.user"),
    ).resolves.toBeInTheDocument();

    expect(
      screen.getByRole("link", { name: "userModal.roles.user" }),
    ).toBeInTheDocument();

    await expect(
      screen.findByText("Test responsibility"),
    ).resolves.toBeInTheDocument();
    await expect(screen.findByText("2017-09-05")).resolves.toBeInTheDocument();
    await expect(
      screen.findByText(timeFromNow("2017-10-29")),
    ).resolves.toBeInTheDocument();
    expect(screen.getAllByText("Registered")).toHaveLength(2);

    jest.clearAllMocks();
  });

  it("should render an add stakeholder component", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route element={<GroupMembers />} path={"/groups/:groupName/members"} />
      </Routes>,
      {
        memoryRouter,
      },
    );

    await waitFor((): void => {
      expect(screen.queryByRole("table")).toBeInTheDocument();
    });

    jest.clearAllMocks();
  });

  it("should render an edit stakeholder component", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_grant_stakeholder_access_mutate" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<GroupMembers />}
            path={"/groups/:groupName/members"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter,
      },
    );
    await waitFor((): void => {
      expect(screen.queryByRole("table")).toBeInTheDocument();
    });

    expect(
      screen.getByText("organization.members.addUserButton.text"),
    ).toBeInTheDocument();

    jest.clearAllMocks();
  });

  it("should open a modal to add stakeholder", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_grant_stakeholder_access_mutate" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<GroupMembers />}
            path={"/groups/:groupName/members"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter,
      },
    );
    await waitFor((): void => {
      expect(screen.queryByRole("table")).toBeInTheDocument();
    });

    expect(
      screen.queryByText("searchFindings.tabUsers.title"),
    ).not.toBeInTheDocument();

    await userEvent.click(
      screen.getByText("organization.members.addUserButton.text"),
    );
    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.tabUsers.title"),
      ).toBeInTheDocument();
    });

    jest.clearAllMocks();
  });

  it("should open a modal to edit stakeholder", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_update_group_stakeholder_mutate" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<GroupMembers />}
            path={"/groups/:groupName/members"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter,
      },
    );
    await waitFor((): void => {
      expect(screen.queryByRole("table")).toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(2);
    });

    expect(
      screen.queryByText("organization.tabs.users.editButton.text"),
    ).toBeDisabled();

    await waitFor(async (): Promise<void> => {
      await userEvent.click(
        await within(
          within(screen.getAllByRole("row")[1]).getAllByRole("cell")[0],
        ).findByTestId("check-icon"),
      );
    });
    await waitFor((): void => {
      expect(
        screen.queryByText("organization.tabs.users.editButton.text"),
      ).not.toBeDisabled();
    });

    expect(
      screen.queryByText("searchFindings.tabUsers.editStakeholderTitle"),
    ).not.toBeInTheDocument();

    await userEvent.click(
      screen.getByText("organization.tabs.users.editButton.text"),
    );
    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.tabUsers.editStakeholderTitle"),
      ).toBeInTheDocument();
    });

    jest.clearAllMocks();
  });

  it("should add stakeholder to the group", async (): Promise<void> => {
    expect.hasAssertions();

    const mocksMutation = [
      graphqlMocked.mutation(
        ADD_STAKEHOLDER_MUTATION,
        ({
          variables,
        }): StrictResponse<IErrorMessage | { data: AddStakeholder }> => {
          const { email, groupName, responsibility, role } = variables;
          if (
            email === "unittest@test.com" &&
            groupName === "TEST" &&
            responsibility === "Project Manager" &&
            role === StakeholderRole.Hacker
          ) {
            return HttpResponse.json({
              data: {
                grantStakeholderAccess: {
                  grantedStakeholder: {
                    email: "unittest@test.com",
                    firstLogin: "",
                    lastLogin: "",
                    responsibility: "Project Manager",
                    role: "HACKER",
                  },
                  success: true,
                },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error adding stakeholder")],
          });
        },
      ),
    ];
    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_grant_stakeholder_access_mutate" },
      { action: "grant_group_level_role:hacker" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<GroupMembers />}
            path={"/groups/:groupName/members"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      { memoryRouter, mocks: mocksMutation },
    );
    await waitFor((): void => {
      expect(screen.queryByRole("table")).toBeInTheDocument();
    });

    expect(
      screen.queryByText("searchFindings.tabUsers.title"),
    ).not.toBeInTheDocument();

    await userEvent.click(document.querySelectorAll("#addUser")[0]);
    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.tabUsers.title"),
      ).toBeInTheDocument();
    });

    fireEvent.change(screen.getByRole("combobox", { name: "email" }), {
      target: { value: "unittest@test.com" },
    });
    await userEvent.type(
      screen.getByRole("textbox", { name: "responsibility" }),
      "Project Manager",
    );
    await userEvent.click(screen.getByRole("combobox", { name: "role" }));
    await userEvent.click(screen.getByText("userModal.roles.hacker"));

    await waitFor((): void => {
      expect(screen.queryByText("Confirm")).not.toBeDisabled();
    });

    expect(
      screen.queryByText("userModal.confirm.title"),
    ).not.toBeInTheDocument();

    await userEvent.click(screen.getByText("Confirm"));
    // Outside company
    await waitFor((): void => {
      expect(screen.queryByText("userModal.confirm.title")).toBeInTheDocument();
    });
    await userEvent.click(
      screen.getAllByRole("button", { name: "Confirm" })[1],
    );
    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledTimes(1);
    });

    expect(
      screen.queryByText("searchFindings.tabUsers.title"),
    ).not.toBeInTheDocument();

    jest.clearAllMocks();
  });

  it("should remove stakeholder from the group", async (): Promise<void> => {
    expect.hasAssertions();

    const mocksMutation = [
      graphqlMocked.query(
        GET_STAKEHOLDERS,
        (): StrictResponse<{ data: GetStakeholders }> => {
          return HttpResponse.json({
            data: {
              group: {
                __typename: "Group",
                name: "TEST",
                stakeholders: [
                  {
                    __typename: "Stakeholder",
                    email: "user@gmail.com",
                    firstLogin: "2017-09-05 15:00:00",
                    invitationState: InvitationState.Registered,
                    lastLogin: "2017-10-29 13:40:37",
                    responsibility: "Test responsibility",
                    role: "user",
                  },
                  {
                    __typename: "Stakeholder",
                    email: "user2@gmail.com",
                    firstLogin: "2018-09-05 15:00:00",
                    invitationState: InvitationState.Registered,
                    lastLogin: "2018-11-29 13:40:37",
                    responsibility: "To be a great",
                    role: "user manager",
                  },
                ],
              },
            },
          });
        },
        { once: true },
      ),
      graphqlMocked.mutation(
        REMOVE_STAKEHOLDER_MUTATION,
        ({
          variables,
        }): StrictResponse<
          IErrorMessage | { data: RemoveStakeholderAccess }
        > => {
          const { userEmail, groupName } = variables;
          if (userEmail === "user@gmail.com" && groupName === "TEST") {
            return HttpResponse.json({
              data: {
                removeStakeholderAccess: {
                  removedEmail: "user@gmail.com",
                  success: true,
                },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error removing stakeholder")],
          });
        },
      ),
      graphqlMocked.query(
        GET_STAKEHOLDERS,
        (): StrictResponse<{ data: GetStakeholders }> => {
          return HttpResponse.json({
            data: {
              group: {
                __typename: "Group",
                name: "TEST",
                stakeholders: [
                  {
                    __typename: "Stakeholder",
                    email: "user2@gmail.com",
                    firstLogin: "2018-09-05 15:00:00",
                    invitationState: InvitationState.Registered,
                    lastLogin: "2018-11-29 13:40:37",
                    responsibility: "To be a great",
                    role: "user manager",
                  },
                ],
              },
            },
          });
        },
        { once: true },
      ),
    ];
    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_remove_stakeholder_access_mutate" },
      { action: "integrates_api_mutations_update_group_stakeholder_mutate" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<GroupMembers />}
            path={"/groups/:groupName/members"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      { memoryRouter, mocks: mocksMutation },
    );
    await waitFor((): void => {
      expect(screen.queryByRole("table")).toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(screen.getAllByRole("checkbox", { checked: false })).toHaveLength(
        3,
      );
    });

    expect(document.querySelectorAll("#removeUser")[0]).toBeDisabled();

    await waitFor((): void => {
      expect(screen.queryByText("user@gmail.com")).toBeInTheDocument();
    });
    const cell = screen.getByText("user@gmail.com");
    const row = cell.closest("tr");
    const checkbox = within(row as HTMLElement).getByRole("checkbox");
    await userEvent.click(checkbox);

    await waitFor((): void => {
      expect(document.querySelectorAll("#removeUser")[0]).not.toBeDisabled();
    });

    expect(screen.getAllByRole("checkbox", { checked: false })).toHaveLength(2);
    expect(screen.getAllByRole("checkbox", { checked: true })).toHaveLength(1);

    await userEvent.click(document.querySelectorAll("#removeUser")[0]);

    await waitFor((): void => {
      expect(
        screen.queryByText(
          "searchFindings.tabUsers.removeUserButton.confirmTitle",
        ),
      ).toBeInTheDocument();
    });

    await userEvent.click(screen.getByText("Confirm"));
    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "user@gmail.com searchFindings.tabUsers.successDelete",
        "searchFindings.tabUsers.titleSuccess",
      );
    });

    jest.clearAllMocks();
  });

  it("should edit stakeholder from the group", async (): Promise<void> => {
    expect.hasAssertions();

    const mocksMutation = [
      graphqlMocked.mutation(
        UPDATE_GROUP_STAKEHOLDER_MUTATION,
        ({
          variables,
        }): StrictResponse<
          IErrorMessage | { data: UpdateGroupStakeholder }
        > => {
          const { email, groupName, responsibility, role } = variables;
          if (
            email === "user@gmail.com" &&
            groupName === "TEST" &&
            responsibility === "Project Manager" &&
            role === StakeholderRole.Hacker
          ) {
            return HttpResponse.json({
              data: {
                updateGroupStakeholder: {
                  success: true,
                },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error updating stakeholder")],
          });
        },
      ),
    ];
    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_update_group_stakeholder_mutate" },
      { action: "grant_group_level_role:hacker" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<GroupMembers />}
            path={"/groups/:groupName/members"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      { memoryRouter, mocks: mocksMutation },
    );
    await waitFor((): void => {
      expect(screen.queryByRole("table")).toBeInTheDocument();
    });

    expect(document.querySelectorAll("#editUser")[0]).toBeDisabled();

    await waitFor((): void => {
      expect(screen.queryByText("user@gmail.com")).toBeInTheDocument();
    });
    const cell = screen.getByText("user@gmail.com");
    const row = cell.closest("tr");
    const checkbox = within(row as HTMLElement).getByRole("checkbox");
    await userEvent.click(checkbox);

    await waitFor((): void => {
      expect(document.querySelectorAll("#editUser")[0]).not.toBeDisabled();
    });

    expect(
      screen.queryByText("searchFindings.tabUsers.editStakeholderTitle"),
    ).not.toBeInTheDocument();

    await userEvent.click(document.querySelectorAll("#editUser")[0]);
    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.tabUsers.editStakeholderTitle"),
      ).toBeInTheDocument();
    });
    await userEvent.clear(
      screen.getByRole("textbox", { name: "responsibility" }),
    );
    await userEvent.type(
      screen.getByRole("textbox", { name: "responsibility" }),
      "Project Manager",
    );

    await userEvent.click(screen.getByRole("combobox", { name: "role" }));
    await userEvent.click(screen.getByText("userModal.roles.hacker"));

    await waitFor((): void => {
      expect(screen.queryByText("Confirm")).not.toBeDisabled();
    });
    await userEvent.click(screen.getByText("Confirm"));
    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledTimes(1);
    });

    expect(
      screen.queryByText("searchFindings.tabUsers.title"),
    ).not.toBeInTheDocument();

    jest.clearAllMocks();
  });

  it("should handle errors when adding a stakeholder to the group", async (): Promise<void> => {
    expect.hasAssertions();

    const mocksMutation = [
      graphqlMocked.mutation(
        ADD_STAKEHOLDER_MUTATION,
        ({
          variables,
        }): StrictResponse<
          Record<"errors", IMessage[]> | { data: AddStakeholder }
        > => {
          const { email, groupName, responsibility, role } = variables;
          if (
            email === "unittest@test.com" &&
            groupName === "TEST" &&
            responsibility === "Project Manager" &&
            role === StakeholderRole.Hacker
          ) {
            return HttpResponse.json({
              errors: [
                new GraphQLError("Error adding stakeholder"),
                new GraphQLError("Exception - Email is not valid"),
                new GraphQLError("Exception - Invalid field in form"),
                new GraphQLError("Exception - Invalid characters"),
                new GraphQLError("Exception - Invalid email address in form"),
                new GraphQLError(
                  "Exception - Groups without an active Fluid Attacks service " +
                    "can not have Fluid Attacks staff",
                ),
                new GraphQLError(
                  "Exception - Groups with any active Fluid Attacks service " +
                    "can only have Hackers provided by Fluid Attacks",
                ),
              ],
            });
          }

          return HttpResponse.json({
            data: {
              grantStakeholderAccess: {
                grantedStakeholder: {
                  email: "unittest@test.com",
                  firstLogin: "",
                  lastLogin: "",
                  responsibility: "Project Manager",
                  role: "HACKER",
                },
                success: true,
              },
            },
          });
        },
      ),
    ];
    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_grant_stakeholder_access_mutate" },
      { action: "grant_group_level_role:hacker" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<GroupMembers />}
            path={"/groups/:groupName/members"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      { memoryRouter, mocks: mocksMutation },
    );
    await waitFor((): void => {
      expect(screen.queryByRole("table")).toBeInTheDocument();
    });

    expect(
      screen.queryByText("searchFindings.tabUsers.title"),
    ).not.toBeInTheDocument();

    await userEvent.click(document.querySelectorAll("#addUser")[0]);
    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.tabUsers.title"),
      ).toBeInTheDocument();
    });

    fireEvent.change(screen.getByRole("combobox", { name: "email" }), {
      target: { value: "unittest@test.com" },
    });
    await userEvent.type(
      screen.getByRole("textbox", { name: "responsibility" }),
      "Project Manager",
    );

    await userEvent.click(screen.getByRole("combobox", { name: "role" }));
    await userEvent.click(screen.getByText("userModal.roles.hacker"));

    await waitFor((): void => {
      expect(screen.queryByText("Confirm")).not.toBeDisabled();
    });

    expect(
      screen.queryByText("userModal.confirm.title"),
    ).not.toBeInTheDocument();

    await userEvent.click(screen.getByText("Confirm"));
    // Outside company
    await waitFor((): void => {
      expect(screen.queryByText("userModal.confirm.title")).toBeInTheDocument();
    });
    await userEvent.click(
      screen.getAllByRole("button", { name: "Confirm" })[1],
    );

    const TEST_TIMES_CALLED = 7;
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledTimes(TEST_TIMES_CALLED);
    });

    expect(
      screen.queryByText("searchFindings.tabUsers.title"),
    ).not.toBeInTheDocument();

    jest.clearAllMocks();
  });

  it("should handle error when removing a stakeholder from the group", async (): Promise<void> => {
    expect.hasAssertions();

    const mocksMutation = [
      graphqlMocked.mutation(
        REMOVE_STAKEHOLDER_MUTATION,
        ({
          variables,
        }): StrictResponse<
          Record<"errors", IMessage[]> | { data: RemoveStakeholderAccess }
        > => {
          const { userEmail, groupName } = variables;
          if (userEmail === "user@gmail.com" && groupName === "TEST") {
            return HttpResponse.json({
              errors: [
                new GraphQLError("Error removing stakeholder"),
                new GraphQLError(
                  "Exception - Credential is still in use by a root",
                ),
              ],
            });
          }

          return HttpResponse.json({
            data: {
              removeStakeholderAccess: {
                removedEmail: "user@gmail.com",
                success: true,
              },
            },
          });
        },
      ),
    ];
    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_remove_stakeholder_access_mutate" },
      { action: "integrates_api_mutations_update_group_stakeholder_mutate" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<GroupMembers />}
            path={"/groups/:groupName/members"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      { memoryRouter, mocks: mocksMutation },
    );
    await waitFor((): void => {
      expect(screen.queryByRole("table")).toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(screen.getAllByRole("checkbox", { checked: false })).toHaveLength(
        3,
      );
    });

    expect(document.querySelectorAll("#removeUser")[0]).toBeDisabled();

    await waitFor((): void => {
      expect(screen.queryByText("user@gmail.com")).toBeInTheDocument();
    });
    const cell = screen.getByText("user@gmail.com");
    const row = cell.closest("tr");
    const checkbox = within(row as HTMLElement).getByRole("checkbox");

    await userEvent.click(checkbox);

    await waitFor((): void => {
      expect(document.querySelectorAll("#removeUser")[0]).not.toBeDisabled();
    });

    expect(screen.queryAllByRole("checkbox", { checked: false })).toHaveLength(
      2,
    );
    expect(screen.getAllByRole("checkbox", { checked: true })).toHaveLength(1);

    await userEvent.click(document.querySelectorAll("#removeUser")[0]);

    await userEvent.click(screen.getByText("Confirm"));
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledTimes(2);
    });

    expect(
      screen.queryByText("searchFindings.tabUsers.title"),
    ).not.toBeInTheDocument();

    jest.clearAllMocks();
  });

  it("should handle error when removing a forces stakeholder from the group", async (): Promise<void> => {
    expect.hasAssertions();

    const mocksQuery = graphqlMocked.query(
      GET_STAKEHOLDERS,
      (): StrictResponse<{ data: GetStakeholders }> => {
        return HttpResponse.json({
          data: {
            group: {
              __typename: "Group",
              name: "TEST",
              stakeholders: [
                {
                  __typename: "Stakeholder",
                  email: "user@gmail.com",
                  firstLogin: "2017-09-05 15:00:00",
                  invitationState: InvitationState.Registered,
                  lastLogin: "2017-10-29 13:40:37",
                  responsibility: "Test responsibility",
                  role: "user",
                },
                {
                  __typename: "Stakeholder",
                  email: "forces.TEST@fluidattacks.com",
                  firstLogin: "2018-09-05 15:00:00",
                  invitationState: InvitationState.Registered,
                  lastLogin: "2018-11-29 13:40:37",
                  responsibility: "To be a great",
                  role: "user manager",
                },
              ],
            },
          },
        });
      },
      { once: true },
    );
    const mocksMutation = [
      graphqlMocked.mutation(
        REMOVE_STAKEHOLDER_MUTATION,
        ({
          variables,
        }): StrictResponse<
          IErrorMessage | { data: RemoveStakeholderAccess }
        > => {
          const { userEmail, groupName } = variables;
          if (
            userEmail === "forces.TEST@fluidattacks.com" &&
            groupName === "TEST"
          ) {
            return HttpResponse.json({
              errors: [
                new GraphQLError("Exception - Invalid forces stakeholder"),
              ],
            });
          }

          return HttpResponse.json({
            data: {
              removeStakeholderAccess: {
                removedEmail: "forces.TEST@fluidattacks.com",
                success: true,
              },
            },
          });
        },
      ),
    ];
    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_remove_stakeholder_access_mutate" },
      { action: "integrates_api_mutations_update_group_stakeholder_mutate" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<GroupMembers />}
            path={"/groups/:groupName/members"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter,
        mocks: [...mocksMutation, mocksQuery],
      },
    );
    await waitFor((): void => {
      expect(screen.queryByRole("table")).toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(screen.getAllByRole("checkbox", { checked: false })).toHaveLength(
        3,
      );
    });

    expect(document.querySelectorAll("#removeUser")[0]).toBeDisabled();

    await waitFor(async (): Promise<void> => {
      const cell = screen.getByText("forces.TEST@fluidattacks.com");
      const row = cell.closest("tr");
      const checkbox = within(row as HTMLElement).getByRole("checkbox");

      await userEvent.click(checkbox);
    });

    await waitFor((): void => {
      expect(document.querySelectorAll("#removeUser")[0]).not.toBeDisabled();
    });

    await userEvent.click(document.querySelectorAll("#removeUser")[0]);

    await userEvent.click(screen.getByText("Confirm"));
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        "The forces user can not be removed",
      );
    });

    jest.clearAllMocks();
  });

  it("should handle error when editing a stakeholder from the group", async (): Promise<void> => {
    expect.hasAssertions();

    const mocksMutation = [
      graphqlMocked.mutation(
        UPDATE_GROUP_STAKEHOLDER_MUTATION,
        ({
          variables,
        }): StrictResponse<
          Record<"errors", IMessage[]> | { data: UpdateGroupStakeholder }
        > => {
          const { email, groupName, responsibility, role } = variables;
          if (
            email === "user@gmail.com" &&
            groupName === "TEST" &&
            responsibility === "Project Manager" &&
            role === StakeholderRole.Hacker
          ) {
            return HttpResponse.json({
              errors: [
                new GraphQLError("Error updating stakeholder"),
                new GraphQLError("Exception - Invalid field in form"),
                new GraphQLError("Exception - Invalid characters"),
                new GraphQLError(
                  "Exception - Groups without an active Fluid Attacks service " +
                    "can not have Fluid Attacks staff",
                ),
                new GraphQLError(
                  "Exception - Groups with any active Fluid Attacks service " +
                    "can only have Hackers provided by Fluid Attacks",
                ),
              ],
            });
          }

          return HttpResponse.json({
            data: {
              updateGroupStakeholder: {
                success: true,
              },
            },
          });
        },
      ),
    ];
    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_update_group_stakeholder_mutate" },
      { action: "grant_group_level_role:hacker" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<GroupMembers />}
            path={"/groups/:groupName/members"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      { memoryRouter, mocks: mocksMutation },
    );
    await waitFor((): void => {
      expect(screen.queryByRole("table")).toBeInTheDocument();
    });

    expect(document.querySelectorAll("#editUser")[0]).toBeDisabled();

    await waitFor((): void => {
      expect(screen.queryByText("user@gmail.com")).toBeInTheDocument();
    });
    const cell = screen.getByText("user@gmail.com");
    const row = cell.closest("tr");
    const checkbox = within(row as HTMLElement).getByRole("checkbox");

    await userEvent.click(checkbox);

    await waitFor((): void => {
      expect(document.querySelectorAll("#editUser")[0]).not.toBeDisabled();
    });

    expect(
      screen.queryByText("searchFindings.tabUsers.editStakeholderTitle"),
    ).not.toBeInTheDocument();

    await userEvent.click(document.querySelectorAll("#editUser")[0]);
    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.tabUsers.editStakeholderTitle"),
      ).toBeInTheDocument();
    });
    await userEvent.clear(
      screen.getByRole("textbox", { name: "responsibility" }),
    );
    await userEvent.type(
      screen.getByRole("textbox", { name: "responsibility" }),
      "Project Manager",
    );

    await userEvent.click(screen.getByRole("combobox", { name: "role" }));
    await userEvent.click(screen.getByText("userModal.roles.hacker"));

    await waitFor((): void => {
      expect(screen.queryByText("Confirm")).not.toBeDisabled();
    });
    await userEvent.click(screen.getByText("Confirm"));
    const TEST_TIMES_CALLED = 5;

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledTimes(TEST_TIMES_CALLED);
    });

    expect(
      screen.queryByText("searchFindings.tabUsers.title"),
    ).not.toBeInTheDocument();

    jest.clearAllMocks();
  });

  it("should remove many stakeholders from the group", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const mocksMutation = [
      graphqlMocked.query(
        GET_STAKEHOLDERS,
        (): StrictResponse<{ data: GetStakeholders }> => {
          return HttpResponse.json({
            data: {
              group: {
                __typename: "Group",
                name: "TEST",
                stakeholders: [
                  {
                    __typename: "Stakeholder",
                    email: "user@gmail.com",
                    firstLogin: "2017-09-05 15:00:00",
                    invitationState: InvitationState.Registered,
                    lastLogin: "2017-10-29 13:40:37",
                    responsibility: "Test responsibility",
                    role: "user",
                  },
                  {
                    __typename: "Stakeholder",
                    email: "user2@gmail.com",
                    firstLogin: "2018-09-05 15:00:00",
                    invitationState: InvitationState.Registered,
                    lastLogin: "2018-11-29 13:40:37",
                    responsibility: "To be a great",
                    role: "user manager",
                  },
                ],
              },
            },
          });
        },
        { once: true },
      ),
      graphqlMocked.mutation(
        REMOVE_STAKEHOLDER_MUTATION,
        ({
          variables,
        }): StrictResponse<
          IErrorMessage | { data: RemoveStakeholderAccess }
        > => {
          const { userEmail, groupName } = variables;
          if (
            userEmail === "user@gmail.com" ||
            (userEmail === "user2@gmail.com" && groupName === "TEST")
          ) {
            return HttpResponse.json({
              data: {
                removeStakeholderAccess: {
                  removedEmail: "user@gmail.com",
                  success: true,
                },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error removing stakeholder")],
          });
        },
      ),
      graphqlMocked.query(
        GET_STAKEHOLDERS,
        (): StrictResponse<{ data: GetStakeholders }> => {
          return HttpResponse.json({
            data: {
              group: {
                __typename: "Group",
                name: "TEST",
                stakeholders: [],
              },
            },
          });
        },
        { once: true },
      ),
    ];

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_remove_stakeholder_access_mutate" },
      { action: "integrates_api_mutations_update_group_stakeholder_mutate" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<GroupMembers />}
            path={"/groups/:groupName/members"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      { memoryRouter, mocks: mocksMutation },
    );
    await waitFor((): void => {
      expect(screen.queryByRole("table")).toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(screen.getAllByRole("checkbox", { checked: false })).toHaveLength(
        3,
      );
    });

    expect(document.querySelectorAll("#removeUser")[0]).toBeDisabled();

    await waitFor((): void => {
      expect(screen.queryByText("user@gmail.com")).toBeInTheDocument();
    });

    const cell = screen.getByText("user@gmail.com");
    const row = cell.closest("tr");
    const checkbox = within(row as HTMLElement).getByRole("checkbox");
    await userEvent.click(checkbox);

    await waitFor((): void => {
      expect(screen.queryByText("user2@gmail.com")).toBeInTheDocument();
    });
    const cellTwo = screen.getByText("user2@gmail.com");
    const rowTwo = cellTwo.closest("tr");
    const checkboxTwo = within(rowTwo as HTMLElement).getByRole("checkbox");
    await userEvent.click(checkboxTwo);

    await waitFor((): void => {
      expect(document.querySelectorAll("#removeUser")[0]).not.toBeDisabled();
    });

    await userEvent.click(document.querySelectorAll("#removeUser")[0]);

    await waitFor((): void => {
      expect(
        screen.queryByText(
          "searchFindings.tabUsers.removeUserButton.confirmTitle",
        ),
      ).toBeInTheDocument();
    });

    expect(
      screen.queryByText(
        "user@gmail.com, user2@gmail.com searchFindings.tabUsers.removeUserButton.confirmMessage",
      ),
    ).toBeInTheDocument();

    await userEvent.click(screen.getByText("Confirm"));
    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "searchFindings.tabUsers.successDeletePlural",
        "searchFindings.tabUsers.titleSuccess",
      );
    });

    await waitFor((): void => {
      expect(
        screen.queryByText("organization.tabs.users.emptyComponent.title"),
      ).toBeInTheDocument();
    });

    jest.clearAllMocks();
  });

  it("should resend invitation to stakeholder", async (): Promise<void> => {
    expect.hasAssertions();

    const mocksQuery = graphqlMocked.query(
      GET_STAKEHOLDERS,
      (): StrictResponse<{ data: GetStakeholders }> => {
        return HttpResponse.json({
          data: {
            group: {
              __typename: "Group",
              name: "TEST",
              stakeholders: [
                {
                  __typename: "Stakeholder",
                  email: "user@gmail.com",
                  firstLogin: "",
                  invitationState: InvitationState.Pending,
                  lastLogin: "",
                  responsibility: "Test responsibility",
                  role: "user",
                },
                {
                  __typename: "Stakeholder",
                  email: "user2@gmail.com",
                  firstLogin: "2018-09-05 15:00:00",
                  invitationState: InvitationState.Registered,
                  lastLogin: "2018-11-29 13:40:37",
                  responsibility: "To be a great",
                  role: "user manager",
                },
              ],
            },
          },
        });
      },
      { once: true },
    );

    const mocksMutation = graphqlMocked.mutation(
      ADD_STAKEHOLDER_MUTATION,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: AddStakeholder }> => {
        const { email, groupName, responsibility, role } = variables;
        if (
          email === "user@gmail.com" &&
          groupName === "TEST" &&
          responsibility === "Test responsibility" &&
          role === StakeholderRole.User
        ) {
          return HttpResponse.json({
            data: {
              grantStakeholderAccess: {
                grantedStakeholder: {
                  email: "user@gmail.com",
                  firstLogin: "",
                  lastLogin: "",
                  responsibility: "Test responsibility",
                  role: "USER",
                },
                success: true,
              },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error adding stakeholder")],
        });
      },
    );
    render(
      <Routes>
        <Route element={<GroupMembers />} path={"/groups/:groupName/members"} />
      </Routes>,
      {
        memoryRouter,
        mocks: [mocksQuery, mocksMutation],
      },
    );
    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(3);
    });

    const rowContent =
      /user@gmail.com userModal.roles.user Test responsibility - - Pending searchFindings.usersTable.resendEmail/iu;

    const row = screen.getByRole("row", { name: rowContent });

    await userEvent.click(within(row).getByRole("button"));

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenLastCalledWith(
        "searchFindings.tabUsers.success user@gmail.com",
        "searchFindings.tabUsers.titleSuccess",
      );
    });

    jest.clearAllMocks();
  });

  it("should filter members", async (): Promise<void> => {
    expect.hasAssertions();

    const mocks = [
      graphql
        .link(LINK)
        .query(
          GET_STAKEHOLDERS,
          (): StrictResponse<{ data: GetStakeholders }> => {
            return HttpResponse.json({
              data: {
                group: {
                  __typename: "Group",
                  name: "TEST",
                  stakeholders: [
                    {
                      __typename: "Stakeholder",
                      email: "user@gmail.com",
                      firstLogin: "2017-09-05 15:00:00",
                      invitationState: InvitationState.Unregistered,
                      lastLogin: "2017-10-29 13:40:37",
                      responsibility: "Test responsibility",
                      role: "user",
                    },
                    {
                      __typename: "Stakeholder",
                      email: "user2@gmail.com",
                      firstLogin: "2018-09-05 15:00:00",
                      invitationState: InvitationState.Registered,
                      lastLogin: "2018-11-29 13:40:37",
                      responsibility: "To be a great",
                      role: "user manager",
                    },
                    {
                      __typename: "Stakeholder",
                      email: "userintegrates@gmail.com",
                      firstLogin: "2017-09-05 15:00:00",
                      invitationState: InvitationState.Registered,
                      lastLogin: "2017-10-29 13:40:37",
                      responsibility: "Test responsibility",
                      role: "admin",
                    },
                    {
                      __typename: "Stakeholder",
                      email: "userintegrates2@gmail.com",
                      firstLogin: "",
                      invitationState: InvitationState.Pending,
                      lastLogin: "",
                      responsibility: "To be a great",
                      role: "user manager",
                    },
                    {
                      __typename: "Stakeholder",
                      email: "admin@fluidattacks.com",
                      firstLogin: "2018-09-05 15:00:00",
                      invitationState: InvitationState.Registered,
                      lastLogin: "2018-11-29 13:40:37",
                      responsibility: "To be a great",
                      role: "admin",
                    },
                  ],
                },
              },
            });
          },
        ),
    ];

    render(
      <Routes>
        <Route element={<GroupMembers />} path={"/groups/:groupName/members"} />
      </Routes>,
      {
        memoryRouter,
        mocks,
      },
    );
    await waitFor((): void => {
      expect(screen.queryByRole("table")).toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(6);
    });

    const filterBtn = document.querySelector("#filterBtn");

    expect(filterBtn).toBeInTheDocument();

    if (filterBtn instanceof HTMLElement) {
      await userEvent.click(filterBtn);
    }

    await userEvent.click(
      screen.getByTestId("li-filter-option-searchFindings.usersTable.userRole"),
    );

    const adminLabel = document.querySelector(
      'label:has(input[value="admin"])',
    );
    if (adminLabel instanceof HTMLElement) {
      await userEvent.click(adminLabel);
    }

    await userEvent.click(screen.getByText("Apply"));
    await userEvent.click(screen.getByRole("button", { name: "modal-close" }));

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(3);
    });

    await expect(
      screen.findByText("userintegrates@gmail.com"),
    ).resolves.toBeInTheDocument();

    await userEvent.click(screen.getByTestId("xmark-icon"));

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(6);
    });

    jest.clearAllMocks();
  });
});
