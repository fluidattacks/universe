interface IFrozenGroupModalProps {
  isOpen: boolean;
}

export type { IFrozenGroupModalProps };
