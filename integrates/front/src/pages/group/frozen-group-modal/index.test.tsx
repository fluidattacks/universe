import { screen } from "@testing-library/react";

import { FrozenGroupModal } from ".";
import { render } from "mocks";

describe("frozenGroupModal", (): void => {
  it("should render a component", (): void => {
    expect.hasAssertions();

    render(<FrozenGroupModal isOpen={true} />);

    expect(screen.queryByText("group.frozen.title")).toBeInTheDocument();
  });

  it("should not render a component", (): void => {
    expect.hasAssertions();

    render(<FrozenGroupModal isOpen={false} />);

    expect(screen.queryByText("group.frozen.title")).not.toBeInTheDocument();
  });
});
