import type { QueryResult } from "@apollo/client";
import { useQuery } from "@apollo/client";
import { useAbility } from "@casl/react";
import { useTranslation } from "react-i18next";
import { type Params, useMatch } from "react-router-dom";

import { GET_AUTHORS, GET_STAKEHOLDERS } from "./queries";

import {
  authzGroupContext,
  authzPermissionsContext,
} from "context/authz/config";
import type {
  Exact,
  GetAuthorsQuery,
  GetStakeholdersQuery,
  InputMaybe,
  Scalars,
} from "gql/graphql";
import { useAudit } from "hooks/use-audit";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";

const useAuthors = (
  groupName: string,
  date?: InputMaybe<string>,
  permissionSkip?: boolean,
  showErrors?: boolean,
): QueryResult<
  GetAuthorsQuery,
  Exact<{
    date?: InputMaybe<Scalars["DateTime"]["input"]>;
    groupName: Scalars["String"]["input"];
  }>
> => {
  const { t } = useTranslation();
  const { addAuditEvent } = useAudit();
  const permissions = useAbility(authzPermissionsContext);
  const groupPermissions = useAbility(authzGroupContext);

  return useQuery(GET_AUTHORS, {
    onCompleted: (): void => {
      addAuditEvent("Group.Authors", groupName);
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        if (showErrors ?? true) msgError(t("groupAlerts.errorTextsad"));
        Logger.warning(
          "An error occurred getting billing data from stakeholder",
          error,
        );
      });
    },
    skip:
      (permissionSkip ?? false) &&
      (permissions.cannot("integrates_api_resolvers_group_billing_resolve") ||
        groupPermissions.can("has_service_black")),
    variables: { date, groupName },
  });
};

const useStakeholders = (
  groupName: string,
  permissionSkip?: boolean,
): QueryResult<
  GetStakeholdersQuery,
  Exact<{
    groupName: Scalars["String"]["input"];
  }>
> => {
  const { t } = useTranslation();
  const { addAuditEvent } = useAudit();
  const permissions = useAbility(authzPermissionsContext);

  return useQuery(GET_STAKEHOLDERS, {
    onCompleted: (): void => {
      addAuditEvent("Group.Stakeholders", groupName);
    },
    onError: (error): void => {
      msgError(t("groupAlerts.errorTextsad"));
      Logger.warning("An error occurred loading group members", error);
    },
    skip:
      (permissionSkip ?? false) &&
      permissions.cannot(
        "integrates_api_resolvers_query_stakeholder__resolve_for_group",
      ),
    variables: { groupName },
  });
};

const useGroupUrl = (path = "*"): { params: Params; url: string } => {
  const match = useMatch(
    `/orgs/:organizationName/groups/:groupName/${path}`.replace("//", "/"),
  );

  return { params: match?.params ?? {}, url: match?.pathnameBase ?? "" };
};

export { useAuthors, useGroupUrl, useStakeholders };
