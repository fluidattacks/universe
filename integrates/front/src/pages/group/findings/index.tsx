/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { useAbility } from "@casl/react";
import { Alert, Container, Link, Span, Toggle } from "@fluidattacks/design";
import type { Row as FormRow } from "@tanstack/react-table";
import _ from "lodash";
import mixpanel from "mixpanel-browser";
import { useCallback, useContext, useEffect, useMemo, useState } from "react";
import * as React from "react";
import type { FormEvent } from "react";
import { useTranslation } from "react-i18next";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import { useTheme } from "styled-components";

import { renderDescription } from "./description";
import { ExtraButtons } from "./extra-buttons";
import { useFindingsQuery } from "./hooks";
import type { IFindingAttr } from "./types";
import { getTableColumns } from "./utils";

import { Table } from "components/table";
import { authzPermissionsContext } from "context/authz/config";
import { meetingModeContext } from "context/meeting-mode";
import { EmptyVulnerabilities } from "features/empty-vulnerabilities";
import { EventBar } from "features/event-bar";
import { ExpertButton } from "features/expert-button";
import { FindingsFilters } from "features/findings/filters";
import { ManagedType } from "gql/graphql";
import { useDebouncedCallback, useTable, useWindowSize } from "hooks";
import { useAudit } from "hooks/use-audit";
import { useAuthz } from "hooks/use-authz";
import { useSettingsStore } from "hooks/use-settings-store";
import { useFiltersStore } from "pages/finding/vulnerabilities/hooks/use-filters-store";
import { vulnerabilitiesContext } from "pages/group/context";
import type { IVulnerabilitiesContext } from "pages/group/types";

const BREAKPOINT_LARGE_SCREEN = 1200;
const CONTACT_US_URL =
  "https://calendar.google.com/calendar/u/0/appointments/schedules/AcZssZ0ZSgVbAJh3yuV0KUzBLjrZbKxVHhflbXO6a-vPRXtgmbhKphQHUCGPK4lLeKMb48zJl0HSwu7K";
const GroupFindings: React.FC = (): JSX.Element => {
  const { groupName, organizationName } = useParams() as {
    groupName: string;
    organizationName: string;
  };
  const { cvssVersion, setCvssVersion } = useSettingsStore();
  const permissions = useAbility(authzPermissionsContext);
  const theme = useTheme();
  const { width } = useWindowSize();
  const { meetingMode } = useContext(meetingModeContext);
  const {
    openVulnerabilities,
    setOpenVulnerabilities,
  }: IVulnerabilitiesContext = useContext(vulnerabilitiesContext);
  const navigate = useNavigate();
  const { pathname } = useLocation();
  const { t } = useTranslation();
  const { canResolve, canMutate } = useAuthz();
  const canGetHacker = canResolve("finding_hacker");
  const canRemoveFinding = canMutate("remove_finding");

  // State management
  const { search, technique } = useFiltersStore();
  const [filteredFindings, setFilteredFindings] = useState<IFindingAttr[]>([]);

  const tableRef = useTable(
    "tblFindings",
    {
      Treatment: false,
      age: false,
      closingPercentage: false,
      description: false,
      reattack: false,
      releaseDate: false,
    },
    [
      "title",
      "status",
      "priority",
      "vulnerabilitiesSummary_open",
      "severityOverview",
      "lastVulnerability",
      ...[canGetHacker ? "hacker" : ""],
    ],
    { left: ["title", "status", "priority"] },
  );
  const [activeSearch, setActiveSearch] = useState(false);
  const [selectedFindings, setSelectedFindings] = useState<IFindingAttr[]>([]);

  const goToFinding = useCallback(
    (rowInfo: FormRow<IFindingAttr>): ((event: FormEvent) => void) => {
      return (event: FormEvent): void => {
        const vulnerabilityUrl = `${pathname}/${rowInfo.original.id}/locations`;
        const location = vulnerabilityUrl;
        navigate(location);
        event.preventDefault();
      };
    },
    [navigate, pathname],
  );

  const { filledGroupInfo, findings, group, hasEssential, loading, refetch } =
    useFindingsQuery(groupName);

  const { addAuditEvent } = useAudit();
  const onChangeCvssVersion = useCallback((): void => {
    mixpanel.track("TouchCvssToggle");
    addAuditEvent("CVSSToggle", groupName);
    setCvssVersion(cvssVersion.value === "v4.0" ? "v3.1" : "v4.0");
  }, [addAuditEvent, cvssVersion, groupName, setCvssVersion]);

  const tableColumns = getTableColumns(
    meetingMode,
    canGetHacker,
    cvssVersion,
    group?.totalOpenPriority ?? 0,
    t,
  );

  const typesArray = findings.map((find: IFindingAttr): string[] => [
    find.title,
    find.title,
  ]);
  const typesOptions = Object.fromEntries(
    _.sortBy(typesArray, (arr): string => arr[0]),
  );
  const showTestingYourAppLoader = (): boolean => {
    return (
      !loading &&
      !activeSearch &&
      _.isEmpty(findings) &&
      _.isEmpty(search) &&
      _.isEmpty(technique) &&
      group?.userRole !== "hacker"
    );
  };
  const DEBOUNCE_DELAY = 500;
  const handleSearch = useDebouncedCallback((title: string): void => {
    setActiveSearch(true);
    void refetch({ title });
  }, DEBOUNCE_DELAY);
  const handleRowExpand = useCallback(
    (row: FormRow<IFindingAttr>): JSX.Element => {
      return renderDescription(row.original);
    },
    [],
  );

  const language = useMemo(
    (): "en" | "es" =>
      (group?.language.toLowerCase() as "en" | "es" | undefined) ?? "en",
    [group],
  );

  useEffect((): void => {
    if (group !== undefined && setOpenVulnerabilities !== undefined) {
      const newValue = group.findings!.reduce(
        (previousValue, find): number =>
          previousValue + find!.vulnerabilitiesSummary.open,
        0,
      );
      if (openVulnerabilities !== newValue) {
        setOpenVulnerabilities(newValue);
      }
    }
  }, [group, openVulnerabilities, setOpenVulnerabilities]);

  return (
    <Container
      bgColor={theme.palette.gray[50]}
      id={"users"}
      minHeight={"100%"}
      px={1.25}
      py={1.25}
    >
      {group?.managed === ManagedType.Trial ? (
        <Alert closable={true} mb={1.25} variant={"info"}>
          {t("organization.tabs.groups.vulnerabilities.banner")}
          <Link color={theme.palette.info[700]} href={CONTACT_US_URL}>
            <Span fontWeight={"bold"} size={"xs"}>
              {t("organization.tabs.groups.vulnerabilities.contact")}
            </Span>
          </Link>
        </Alert>
      ) : undefined}

      {showTestingYourAppLoader() ? (
        <EmptyVulnerabilities />
      ) : (
        <Container height={"100%"} id={"group-findings-container"}>
          <EventBar organizationName={organizationName} pb={1.25} />
          <Table
            columns={tableColumns}
            data={filteredFindings}
            dataPrivate={false}
            expandedRow={handleRowExpand}
            filters={
              <FindingsFilters
                cvssVersion={cvssVersion.maxOpenSeverityKey}
                findings={findings}
                groupName={groupName}
                permissions={permissions}
                refetch={refetch}
                setFilteredFindings={setFilteredFindings}
              />
            }
            loadingData={loading}
            onRowClick={goToFinding}
            onSearch={handleSearch}
            options={{
              columnToggle: true,
              hasGlobalFilter: false,
              searchPlaceholder: t("searchFindings.searchPlaceholder"),
              smallSearch: width < BREAKPOINT_LARGE_SCREEN,
            }}
            rightSideComponents={
              <React.Fragment>
                <Toggle
                  defaultChecked={cvssVersion.value === "v4.0"}
                  leftDescription={"CVSS 3.1"}
                  name={"cvssToggle"}
                  onChange={onChangeCvssVersion}
                  rightDescription={"CVSS 4.0"}
                />
                <ExtraButtons
                  enableCerts={hasEssential ? filledGroupInfo : false}
                  groupName={groupName}
                  language={language}
                  selectedFindings={selectedFindings}
                  typesOptions={Object.keys(typesOptions)}
                />
              </React.Fragment>
            }
            rowSelectionSetter={
              canRemoveFinding ? setSelectedFindings : undefined
            }
            rowSelectionState={selectedFindings}
            tableRef={tableRef}
          />
          <ExpertButton text={t("group.findings.buttons.help.label")} />
        </Container>
      )}
    </Container>
  );
};

export { GroupFindings };
