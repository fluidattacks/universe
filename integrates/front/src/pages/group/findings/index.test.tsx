import { PureAbility } from "@casl/ability";
import { screen, waitFor, within } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import dayjs from "dayjs";
import { GraphQLError } from "graphql";
import _ from "lodash";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import { Route, Routes } from "react-router-dom";

import {
  ADD_FINDING_MUTATION,
  GET_CRITERIA,
  GET_FINDINGS,
  GET_GROUP_FINDINGS_QUERIES,
} from "./queries";

import { GroupFindings } from ".";
import { authContext } from "context/auth";
import {
  authzGroupContext,
  authzPermissionsContext,
} from "context/authz/config";
import { GET_GROUP_ROOTS } from "features/empty-vulnerabilities/queries";
import type {
  AddFindingMutationMutation as AddFinding,
  GetFindingsQueryQuery as GetFindings,
  GetFindingsCriteriaQueryQuery,
  GetGroupFindingsQueriesQuery as GetGroupFindingsQueries,
  GetGroupRootsQuery as GetGroupRoots,
  GetRootNicknamesQuery as GetRoots,
  RemoveFindingMutationMutation as RemoveFinding,
} from "gql/graphql";
import {
  Language,
  ManagedType,
  RemoveFindingJustification,
  ServiceType,
} from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";
import { REMOVE_FINDING_MUTATION } from "pages/finding/queries";
import { GET_ROOTS } from "pages/group/scope/queries";
import type {
  IGitRootAttr,
  IGroupDataResult,
} from "pages/organization/groups/types";
import { noVulnerabilitiesFound } from "pages/organization/groups/utils";
import { msgError, msgSuccess } from "utils/notifications";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");

  jest.spyOn(mockedNotifications, "msgError").mockImplementation();
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

interface IWrapperProps {
  path?: string;
  element: JSX.Element;
}

const Wrapper = (props: Readonly<IWrapperProps>): JSX.Element => {
  const { path = "/", element } = props;

  return (
    <Routes>
      <Route element={element} path={path} />
    </Routes>
  );
};

const criteriaVulnerabilities = [
  {
    cursor: "001",
    node: {
      en: {
        description:
          "Dynamic SQL statements are generated without the required data validation and without using parameterized statements or stored procedures.\n",
        impact:
          "Inject SQL statements, with the possibility of obtaining information about the database, as well as extract information from it.\n",
        recommendation:
          "Perform queries to the database through sentences or parameterized procedures.\n",
        threat: "Authenticated attacker from the Internet.",
        title: "001. SQL injection - C Sharp SQL API",
      },
      es: {
        description:
          "Se generan sentencias SQL dinámicas sin la validación requerida de datos y sin utilizar sentencias parametrizadas o procedimientos almacenados.\n",
        impact:
          "Inyectar sentencias SQL, con la posibilidad de obtener información sobre la base de datos, así como extraer información de la misma.\n",
        recommendation:
          "Realizar las consultas a la base de datos por medio de sentencias o procedimientos parametrizados.\n",
        threat: "Atacante autenticado desde Internet.",
        title: "001. SQL injection - C Sharp SQL API",
      },
      findingNumber: "001",
      remediationTime: "15",
      requirements: ["161", "173"],
      score: {
        base: {
          attackComplexity: "L",
          attackVector: "N",
          availability: "N",
          confidentiality: "N",
          integrity: "L",
          privilegesRequired: "L",
          scope: "U",
          userInteraction: "N",
        },
        temporal: {
          exploitCodeMaturity: "U",
          remediationLevel: "O",
          reportConfidence: "C",
        },
      },
      scoreV4: {
        base: {
          attackComplexity: "L",
          attackRequirements: "N",
          attackVector: "N",
          availabilitySa: "N",
          availabilityVa: "N",
          confidentialitySc: "N",
          confidentialityVc: "N",
          integritySi: "N",
          integrityVi: "L",
          privilegesRequired: "L",
          userInteraction: "N",
        },
        threat: {
          exploitMaturity: "U",
        },
      },
    },
  },
];

const criteriaConnection = {
  edges: criteriaVulnerabilities,
  pageInfo: {
    endCursor: "001",
    hasNextPage: false,
  },
  total: 1,
};

const generateFindings = (
  amount: number,
  generated: GetFindings["group"]["findings"],
): GetFindings["group"]["findings"] => {
  if (amount <= 0) {
    return generated;
  }

  return generateFindings(amount - 1, [
    ...(generated ?? []),
    {
      __typename: "Finding",
      age: 125,
      description: "description",
      hacker: "machine@fluidattacks.com",
      id: amount.toString(),
      isExploitable: false,
      lastVulnerability: 125,
      maxOpenSeverityScore: 0.0,
      maxOpenSeverityScoreV4: 0.0,
      minTimeToRemediate: 15,
      openAge: 0,
      rejectedVulnerabilities: 0,
      releaseDate: "2024-10-24 17:20:02",
      status: "SAFE",
      submittedVulnerabilities: 10,
      title: `title - ${amount}`,
      totalOpenPriority: 0.0,
      treatmentSummary: {
        __typename: "TreatmentSummary",
        accepted: 0,
        acceptedUndefined: 0,
        inProgress: 0,
        untreated: 0,
      },
      verificationSummary: {
        __typename: "VerificationSummary",
        onHold: 0,
        requested: 0,
        verified: 0,
      },
      verified: true,
      vulnerabilitiesSummary: {
        __typename: "VulnerabilitiesSummary",
        closed: 8,
        open: 0,
        openCritical: 0,
        openHigh: 0,
        openLow: 0,
        openMedium: 0,
      },
      vulnerabilitiesSummaryV4: {
        __typename: "VulnerabilitiesSummary",
        closed: 8,
        open: 0,
        openCritical: 0,
        openHigh: 0,
        openLow: 0,
        openMedium: 0,
      },
      zeroRiskSummary: {
        __typename: "ZeroRiskSummary",
        confirmed: 14,
        requested: 0,
      },
    },
  ]);
};

const findings: GetFindings["group"]["findings"] = [
  {
    __typename: "Finding",
    age: 252,
    description: "This is a test description",
    hacker: "anyhacker@fluidattacks.com",
    id: "438679960",
    isExploitable: true,
    lastVulnerability: 33,
    maxOpenSeverityScore: 2.9,
    maxOpenSeverityScoreV4: 1.9,
    minTimeToRemediate: 60,
    openAge: 60,
    releaseDate: "2020-02-05 09:56:40",
    status: "VULNERABLE",
    title: "038. Business information leak",
    totalOpenPriority: 0,
    treatmentSummary: {
      accepted: 0,
      acceptedUndefined: 0,
      inProgress: 0,
      untreated: 1,
    },
    verificationSummary: {
      onHold: 1,
      requested: 2,
      verified: 3,
    },
    verified: false,
    vulnerabilitiesSummary: {
      closed: 6,
      open: 6,
      openCritical: 2,
      openHigh: 2,
      openLow: 0,
      openMedium: 2,
    },
    vulnerabilitiesSummaryV4: {
      closed: 6,
      open: 6,
      openCritical: 0,
      openHigh: 2,
      openLow: 2,
      openMedium: 2,
    },
  },
  {
    __typename: "Finding",
    age: 240,
    description: "I just have updated the description",
    hacker: "anyhacker@fluidattacks.com",
    id: "697510163",
    isExploitable: false,
    lastVulnerability: 22,
    maxOpenSeverityScore: 3.6,
    maxOpenSeverityScoreV4: 3.6,
    minTimeToRemediate: 60,
    openAge: 60,
    releaseDate: "2020-07-05 09:56:40",
    status: "VULNERABLE",
    title: "001. SQL injection - C Sharp SQL API",
    totalOpenPriority: 0,
    treatmentSummary: {
      accepted: 0,
      acceptedUndefined: 0,
      inProgress: 6,
      untreated: 0,
    },
    verificationSummary: {
      onHold: 0,
      requested: 0,
      verified: 0,
    },
    verified: false,
    vulnerabilitiesSummary: {
      closed: 6,
      open: 6,
      openCritical: 2,
      openHigh: 2,
      openLow: 0,
      openMedium: 2,
    },
    vulnerabilitiesSummaryV4: {
      closed: 6,
      open: 6,
      openCritical: 0,
      openHigh: 2,
      openLow: 2,
      openMedium: 2,
    },
  },
  {
    __typename: "Finding",
    age: 23,
    description: "I just have updated the description",
    hacker: "anyhacker@fluidattacks.com",
    id: "268510163",
    isExploitable: false,
    lastVulnerability: 22,
    maxOpenSeverityScore: 0,
    maxOpenSeverityScoreV4: 0,
    minTimeToRemediate: 0,
    openAge: 23,
    releaseDate: "2020-04-22 09:56:40",
    status: "SAFE",
    title: "071. Insecure or unset HTTP headers - Referrer-Policy",
    totalOpenPriority: 0,
    treatmentSummary: {
      accepted: 0,
      acceptedUndefined: 0,
      inProgress: 0,
      untreated: 0,
    },
    verificationSummary: {
      onHold: 0,
      requested: 0,
      verified: 0,
    },
    verified: false,
    vulnerabilitiesSummary: {
      closed: 6,
      open: 6,
      openCritical: 2,
      openHigh: 2,
      openLow: 0,
      openMedium: 2,
    },
    vulnerabilitiesSummaryV4: {
      closed: 6,
      open: 6,
      openCritical: 0,
      openHigh: 2,
      openLow: 2,
      openMedium: 2,
    },
  },
];

describe("groupFindings", (): void => {
  const graphqlMocked = graphql.link(LINK);
  const apolloDataFinding = {
    group: {
      __typename: "Group" as const,
      businessId: "id",
      businessName: "name",
      description: "description",
      findings,
      hasEssential: false,
      language: Language.En,
      managed: ManagedType.Managed,
      name: "TEST",
      totalOpenPriority: 0,
      userRole: "user-role",
    },
  };

  const filterDataFinding = {
    group: {
      __typename: "Group" as const,
      businessId: "id",
      businessName: "name",
      description: "description",
      findings: [
        {
          __typename: "Finding" as const,
          age: 252,
          description: "This is a test description",
          hacker: "anyhacker@fluidattacks.com",
          id: "413372600",
          isExploitable: true,
          lastVulnerability: 33,
          maxOpenSeverityScore: 2.9,
          maxOpenSeverityScoreV4: 2.9,
          minTimeToRemediate: 60,
          openAge: 60,
          releaseDate: "2020-02-05 09:56:40",
          status: "VULNERABLE",
          title: "004. Remote command execution",
          totalOpenPriority: 0,
          treatmentSummary: {
            accepted: 0,
            acceptedUndefined: 0,
            inProgress: 0,
            untreated: 1,
          },
          verificationSummary: {
            onHold: 1,
            requested: 2,
            verified: 3,
          },
          verified: false,
          vulnerabilitiesSummary: {
            closed: 6,
            open: 6,
            openCritical: 2,
            openHigh: 2,
            openLow: 0,
            openMedium: 2,
          },
          vulnerabilitiesSummaryV4: {
            closed: 6,
            open: 6,
            openCritical: 0,
            openHigh: 2,
            openLow: 2,
            openMedium: 2,
          },
        },
      ],
      hasEssential: false,
      language: Language.En,
      managed: ManagedType.Managed,
      name: "TEST",
      totalOpenPriority: 0,
      userRole: "user-role",
    },
  };

  const apolloDataMock = [
    graphqlMocked.query(
      GET_FINDINGS,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: GetFindings }> => {
        const {
          canGetRejectedVulnerabilities,
          canGetSubmittedVulnerabilities,
          groupName,
          root,
        } = variables;

        if (
          !canGetRejectedVulnerabilities &&
          !canGetSubmittedVulnerabilities &&
          _.isEqual(root, "back/src/model/user/index.js") &&
          groupName === "TEST"
        ) {
          return HttpResponse.json({ data: filterDataFinding });
        }

        if (
          !canGetRejectedVulnerabilities &&
          !canGetSubmittedVulnerabilities &&
          groupName === "TEST"
        ) {
          return HttpResponse.json({ data: apolloDataFinding });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error getting group findings")],
        });
      },
    ),
    graphqlMocked.query(
      GET_FINDINGS,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: GetFindings }> => {
        const {
          canGetRejectedVulnerabilities,
          canGetSubmittedVulnerabilities,
          root,
          groupName,
        } = variables;
        if (
          !canGetRejectedVulnerabilities &&
          !canGetSubmittedVulnerabilities &&
          _.isEqual(root, "") &&
          groupName === "TEST"
        ) {
          return HttpResponse.json({ data: apolloDataFinding });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error getting group findings")],
        });
      },
    ),
    graphqlMocked.query(
      GET_FINDINGS,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: GetFindings }> => {
        const {
          canGetRejectedVulnerabilities,
          canGetSubmittedVulnerabilities,
          root,
          groupName,
        } = variables;
        if (
          !canGetRejectedVulnerabilities &&
          !canGetSubmittedVulnerabilities &&
          _.isEqual(root, undefined) &&
          groupName === "TEST"
        ) {
          return HttpResponse.json({ data: apolloDataFinding });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error getting group findings")],
        });
      },
    ),
    graphqlMocked.query(
      GET_ROOTS,
      ({ variables }): StrictResponse<IErrorMessage | { data: GetRoots }> => {
        const { groupName } = variables;
        if (groupName === "TEST") {
          return HttpResponse.json({
            data: {
              group: {
                __typename: "Group",
                name: "TEST",
                roots: [],
              },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error getting group roots")],
        });
      },
    ),
  ];

  const mockRoots = graphqlMocked.query(
    GET_GROUP_ROOTS,
    ({
      variables,
    }): StrictResponse<IErrorMessage | { data: GetGroupRoots }> => {
      const { groupName } = variables;
      if (groupName === "TEST") {
        return HttpResponse.json({
          data: {
            group: {
              __typename: "Group",
              closedVulnerabilities: 0,
              description: "Test group",
              hasAdvanced: false,
              hasEssential: true,
              managed: ManagedType.Managed,
              name: "TEST",
              openFindings: 0,
              openVulnerabilities: 0,
              roots: [],
              service: ServiceType.White,
              subscription: "Machine",
              userRole: "testRole",
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Error getting group roots")],
      });
    },
  );

  const mocksGroupFindings = {
    group: {
      __typename: "Group" as const,
      businessId: "14441323",
      businessName: "Testing Company and Sons",
      description: "Integrates unit test group",
      findings: [
        {
          __typename: "Finding" as const,
          age: 252,
          description: "Test description",
          hacker: "anyhacker@fluidattacks.com",
          id: "438679960",
          isExploitable: true,
          lastVulnerability: 5,
          maxOpenSeverityScore: 2.9,
          maxOpenSeverityScoreV4: 2.9,
          minTimeToRemediate: 60,
          openAge: 60,
          releaseDate: null,
          status: "VULNERABLE",
          title: "038. Business information leak",
          totalOpenPriority: 0,
          treatmentSummary: {
            accepted: 0,
            acceptedUndefined: 0,
            inProgress: 0,
            untreated: 1,
          },
          verificationSummary: {
            onHold: 1,
            requested: 2,
            verified: 3,
          },
          verified: false,
          vulnerabilitiesSummary: {
            closed: 6,
            open: 6,
            openCritical: 2,
            openHigh: 2,
            openLow: 0,
            openMedium: 2,
          },
          vulnerabilitiesSummaryV4: {
            closed: 6,
            open: 6,
            openCritical: 0,
            openHigh: 2,
            openLow: 2,
            openMedium: 2,
          },
        },
      ],
      hasEssential: true,
      language: Language.En,
      managed: ManagedType.Managed,
      name: "TEST",
      totalOpenPriority: 0,
      userRole: "admin",
    },
  };

  const mocksFindings = [
    graphqlMocked.query(
      GET_FINDINGS,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: GetFindings }> => {
        const {
          canGetRejectedVulnerabilities,
          canGetSubmittedVulnerabilities,
          groupName,
        } = variables;
        if (
          !canGetRejectedVulnerabilities &&
          !canGetSubmittedVulnerabilities &&
          groupName === "TEST"
        ) {
          return HttpResponse.json({ data: mocksGroupFindings });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error getting group findings")],
        });
      },
    ),
    graphqlMocked.query(
      GET_FINDINGS,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: GetFindings }> => {
        const {
          canGetRejectedVulnerabilities,
          canGetSubmittedVulnerabilities,
          root,
          groupName,
        } = variables;
        if (
          !canGetRejectedVulnerabilities &&
          !canGetSubmittedVulnerabilities &&
          _.isEqual(root, "") &&
          groupName === "TEST"
        ) {
          return HttpResponse.json({ data: mocksGroupFindings });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error getting group findings")],
        });
      },
    ),
    graphqlMocked.query(
      GET_FINDINGS,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: GetFindings }> => {
        const {
          canGetRejectedVulnerabilities,
          canGetSubmittedVulnerabilities,
          root,
          groupName,
        } = variables;
        if (
          !canGetRejectedVulnerabilities &&
          !canGetSubmittedVulnerabilities &&
          _.isEqual(root, undefined) &&
          groupName === "TEST"
        ) {
          return HttpResponse.json({ data: mocksGroupFindings });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error getting group findings")],
        });
      },
    ),
  ];

  const mockCriteria = [
    graphqlMocked.query(
      GET_CRITERIA,
      (): StrictResponse<{ data: GetFindingsCriteriaQueryQuery }> => {
        return HttpResponse.json({
          data: {
            __typename: "Query",
            ...{ criteriaConnection },
          },
        });
      },
    ),
    graphqlMocked.query(
      GET_GROUP_FINDINGS_QUERIES,
      (): StrictResponse<{ data: GetGroupFindingsQueries }> => {
        return HttpResponse.json({
          data: {
            __typename: "Query",
            ...{ criteriaConnection },
          },
        });
      },
    ),
  ];

  it("should render all finding", async (): Promise<void> => {
    expect.hasAssertions();

    const allFindings: GetFindings["group"]["findings"] = generateFindings(
      100,
      [],
    );

    const mockedFindings = graphqlMocked.query(
      GET_FINDINGS,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: GetFindings }> => {
        const { groupName } = variables;

        if (groupName === "TEST") {
          return HttpResponse.json({
            data: {
              group: {
                __typename: "Group" as const,
                businessId: "id",
                businessName: "name",
                description: "description",
                findings: allFindings,
                hasEssential: false,
                language: Language.En,
                managed: ManagedType.Managed,
                name: "TEST",
                totalOpenPriority: 0,
                userRole: "user-role",
              },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error getting group findings")],
        });
      },
    );

    localStorage.clear();
    sessionStorage.clear();
    jest.clearAllMocks();

    render(
      <Wrapper
        element={<GroupFindings />}
        path={"/orgs/:organizationName/groups/:groupName/vulns"}
      />,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/TEST/vulns"],
        },
        mocks: [mockedFindings, ...mockCriteria],
      },
    );

    await waitFor((): void => {
      expect(screen.queryAllByRole("table")).toHaveLength(1);
    });
    await waitFor((): void => {
      expect(
        screen.queryByText("group.findings.buttons.help.button"),
      ).toBeInTheDocument();
    });

    await userEvent.click(
      document.getElementById("paginator-dropdown") as HTMLElement,
    );

    await userEvent.click(
      screen.getByText(allFindings?.length.toString() ?? ""),
    );

    expect(screen.queryAllByRole("row")).toHaveLength(
      (allFindings?.length ?? 0) + 1,
    );

    allFindings?.forEach((finding): void => {
      expect(screen.getByText(finding?.title ?? "#####")).toBeInTheDocument();
    });
  });

  it("should render an error in component", async (): Promise<void> => {
    expect.hasAssertions();

    localStorage.clear();
    sessionStorage.clear();
    jest.clearAllMocks();

    render(
      <Wrapper
        element={<GroupFindings />}
        path={"/orgs/:organizationName/groups/:groupName/*"}
      />,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/TEST1/vulns"],
        },
        mocks: [...apolloDataMock, mockRoots, ...mockCriteria],
      },
    );

    await waitFor((): void => {
      expect(msgError).toHaveBeenNthCalledWith(1, "groupAlerts.errorTextsad");
    });
  });

  it("should display all finding columns", async (): Promise<void> => {
    expect.hasAssertions();

    localStorage.clear();
    sessionStorage.clear();
    jest.clearAllMocks();

    render(
      <authContext.Provider
        value={{
          awsSubscription: null,
          setUser: jest.fn(),
          tours: {
            newGroup: true,
            newRoot: true,
            welcome: true,
          },
          userEmail: "",
          userName: "",
        }}
      >
        <Wrapper
          element={<GroupFindings />}
          path={"/orgs/:organizationName/groups/:groupName/vulns"}
        />
      </authContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/TEST/vulns"],
        },
        mocks: [...mocksFindings, apolloDataMock[3], ...mockCriteria],
      },
    );

    await waitFor((): void => {
      expect(
        screen.queryByText("038. Business information leak"),
      ).toBeInTheDocument();
    });
    await waitFor((): void => {
      expect(
        screen.queryByText("group.findings.buttons.help.button"),
      ).toBeInTheDocument();
    });

    expect(screen.queryByText("Where")).not.toBeInTheDocument();
    expect(screen.queryByText("Reattack")).not.toBeInTheDocument();
    expect(
      screen.queryByText("test1@fluidattacks.com"),
    ).not.toBeInTheDocument();

    await userEvent.click(
      screen.getByText(/group\.findings\.tableSet\.btn\.text .*/u),
    );

    await userEvent.click(screen.getByText("Vulnerability management"));

    await userEvent.click(screen.getByRole("checkbox", { name: /Reattack/u }));

    await userEvent.click(screen.getByRole("button", { name: "Save" }));

    await waitFor((): void => {
      expect(screen.queryByText("Manage columns")).not.toBeInTheDocument();
    });

    const fieldsToTest = [
      "New",
      "Type",
      "Status",
      "Last report",
      "Reattack",
      "Open vulns",
    ];
    fieldsToTest.forEach((field): void => {
      expect(screen.getByText(field)).toBeInTheDocument();
    });

    expect(
      within(screen.getByRole("table")).queryByText(
        "038. Business information leak",
      ),
    ).toBeInTheDocument();
    expect(
      screen.getByText("group.findings.description.value"),
    ).toBeInTheDocument();
    expect(screen.getByText("Vulnerable")).toBeInTheDocument();
    expect(
      within(screen.getByRole("table")).getByText("Pending"),
    ).toBeInTheDocument();
  });

  it("should add finding", async (): Promise<void> => {
    expect.hasAssertions();

    localStorage.clear();
    sessionStorage.clear();
    jest.clearAllMocks();
    const expectedCriteria = {
      attackVectorDescription: criteriaVulnerabilities[0].node.en.impact,
      cvss4Vector:
        "CVSS:4.0/AV:N/AC:L/AT:N/PR:L/UI:N/VC:N/VI:L/VA:N/SC:N/SI:N/SA:N/E:U/CR:X/IR:X/AR:X/MAV:X/MAC:X/MAT:X/MPR:X/MUI:X/MVC:X/MVI:X/MVA:X/MSC:X/MSI:X/MSA:X",
      cvssVector:
        "CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:L/A:N/E:U/RL:O/RC:C/CR:X/IR:X/AR:X/MAV:X/MAC:X/MPR:X/MUI:X/MS:X/MC:X/MI:X/MA:X",
      description: criteriaVulnerabilities[0].node.en.description,
      groupName: "TEST",
      minTimeToRemediate: 15,
      recommendation: criteriaVulnerabilities[0].node.en.recommendation,
      threat: criteriaVulnerabilities[0].node.en.threat,
      title: criteriaVulnerabilities[0].node.en.title,
      unfulfilledRequirements: ["161", "173"],
    };

    const mockedMutations = [
      graphqlMocked.mutation(
        ADD_FINDING_MUTATION,
        ({
          variables,
        }): StrictResponse<IErrorMessage | { data: AddFinding }> => {
          if (_.isEqual(variables, expectedCriteria)) {
            return HttpResponse.json({
              data: { addFinding: { success: true } },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error getting adding a finding")],
          });
        },
      ),
    ];
    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_add_finding_mutate" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <authzGroupContext.Provider
          value={new PureAbility([{ action: "can_report_vulnerabilities" }])}
        >
          <Wrapper
            element={<GroupFindings />}
            path={"/orgs/:organizationName/groups/:groupName/vulns"}
          />
        </authzGroupContext.Provider>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/TEST/vulns"],
        },
        mocks: [
          ...apolloDataMock,
          ...mockedMutations,
          ...apolloDataMock,
          ...mockCriteria,
        ],
      },
    );

    await userEvent.click(screen.getByRole("checkbox", { name: "cvssToggle" }));

    await waitFor((): void => {
      expect(
        screen.queryByRole("button", {
          name: "group.findings.buttons.add.text",
        }),
      ).toBeInTheDocument();
    });
    await waitFor((): void => {
      expect(
        screen.queryByText("group.findings.buttons.help.button"),
      ).toBeInTheDocument();
    });

    await userEvent.click(
      screen.getByRole("button", { name: "group.findings.buttons.add.text" }),
    );
    await waitFor((): void => {
      expect(
        screen.queryByRole("combobox", { name: "title" }),
      ).toBeInTheDocument();
    });

    await userEvent.type(
      screen.getByRole("combobox", { name: "title" }),
      "001. SQL injection - C Sharp SQL API",
    );

    await waitFor((): void => {
      expect(
        screen.queryByRole("textbox", { name: "description" }),
      ).toHaveValue(criteriaVulnerabilities[0].node.en.description);
    });

    await waitFor((): void => {
      expect(
        screen.getByRole("button", { name: "Confirm" }),
      ).not.toBeDisabled();
    });
    await userEvent.click(screen.getByRole("button", { name: "Confirm" }));

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenLastCalledWith(
        "group.findings.addModal.alerts.addedFinding",
        "groupAlerts.titleSuccess",
      );
    });

    expect(screen.getAllByRole("row")[1]).toHaveTextContent(
      "038. Business information leak",
    );
  });

  it("should delete finding", async (): Promise<void> => {
    expect.hasAssertions();

    localStorage.clear();
    sessionStorage.clear();
    jest.clearAllMocks();

    const deleteMutationMock = [
      graphqlMocked.mutation(
        REMOVE_FINDING_MUTATION,
        ({
          variables,
        }): StrictResponse<IErrorMessage | { data: RemoveFinding }> => {
          const { findingId, justification } = variables;
          if (
            findingId === "438679960" &&
            justification === RemoveFindingJustification.NotRequired
          ) {
            return HttpResponse.json({
              data: {
                removeFinding: {
                  success: true,
                },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error removing finding")],
          });
        },
      ),
    ];
    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_remove_finding_mutate" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <authzGroupContext.Provider
          value={new PureAbility([{ action: "can_report_vulnerabilities" }])}
        >
          <Wrapper
            element={<GroupFindings />}
            path={"/orgs/:organizationName/groups/:groupName/vulns"}
          />
        </authzGroupContext.Provider>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/TEST/vulns"],
        },
        mocks: [
          ...deleteMutationMock,
          graphqlMocked.query(
            GET_FINDINGS,
            ({
              variables,
            }): StrictResponse<IErrorMessage | { data: GetFindings }> => {
              const {
                canGetRejectedVulnerabilities,
                canGetSubmittedVulnerabilities,
                groupName,
              } = variables;
              if (
                !canGetRejectedVulnerabilities &&
                !canGetSubmittedVulnerabilities &&
                groupName === "TEST"
              ) {
                return HttpResponse.json({ data: mocksGroupFindings });
              }

              return HttpResponse.json({
                errors: [new GraphQLError("Error getting group findings")],
              });
            },
          ),
        ],
      },
    );

    await waitFor((): void => {
      expect(
        screen.getByText("038. Business information leak"),
      ).toBeInTheDocument();
    });

    await userEvent.click(screen.getAllByRole("checkbox", { name: "" })[1]);

    await waitFor((): void => {
      expect(
        screen.queryByRole("button", {
          name: "group.findings.buttons.delete.text",
        }),
      ).toBeInTheDocument();
    });

    await userEvent.click(
      screen.getByRole("button", {
        name: "group.findings.buttons.delete.text",
      }),
    );
    await waitFor((): void => {
      expect(
        screen.queryByRole("combobox", { name: "justification" }),
      ).toBeInTheDocument();
    });

    await userEvent.click(
      screen.getByRole("combobox", { name: "justification" }),
    );
    await userEvent.click(
      screen.getByText("group.findings.deleteModal.justification.notRequired"),
    );

    await waitFor((): void => {
      expect(
        screen.getByRole("button", { name: "Confirm" }),
      ).not.toBeDisabled();
    });
    await userEvent.click(screen.getByRole("button", { name: "Confirm" }));

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenLastCalledWith(
        "group.findings.deleteModal.alerts.vulnerabilitiesDeleted",
        "searchFindings.successTitle",
      );
    });
  });

  describe("function unit tests", (): void => {
    it("should have no vulnerabilities found (no Essential)", (): void => {
      expect.hasAssertions();

      localStorage.clear();
      sessionStorage.clear();
      jest.clearAllMocks();

      const group = {
        __typename: "Group",
        closedVulnerabilities: null,
        description: "Trial group",
        hasAdvanced: true,
        hasEssential: false,
        managed: "TRIAL",
        name: "jdoefluidattacks",
        openFindings: null,
        openVulnerabilities: null,
        roots: [
          {
            __typename: "GitRoot",
            cloningStatus: {
              __typename: "GitRootCloningStatus",
              status: "QUEUED",
            },
          },
        ],
        service: "WHITE",
        subscription: "continuous",
        userRole: "group_manager",
      } as unknown as IGroupDataResult;

      const gitRoots = group.roots.filter(
        (root): root is IGitRootAttr => root.__typename === "GitRoot",
      );

      const result = noVulnerabilitiesFound(gitRoots, group);

      expect(result).toBe(false);
    });

    it("should have vulnerabilities (with open findings)", (): void => {
      expect.hasAssertions();

      const group = {
        __typename: "Group",
        closedVulnerabilities: null,
        description: "Trial group",
        hasAdvanced: false,
        hasEssential: true,
        managed: "TRIAL",
        name: "jdoefluidattacks",
        openFindings: 1,
        openVulnerabilities: null,
        roots: [
          {
            __typename: "GitRoot",
            cloningStatus: {
              __typename: "GitRootCloningStatus",
              status: "UNKNOWN",
            },
          },
        ],
        service: "WHITE",
        subscription: "continuous",
        userRole: "group_manager",
      } as unknown as IGroupDataResult;

      const gitRoots = group.roots.filter(
        (root): root is IGitRootAttr => root.__typename === "GitRoot",
      );

      const result = noVulnerabilitiesFound(gitRoots, group);

      expect(result).toBe(false);
    });

    it("should have no vulnerabilities (group suspended aka UNDER_REVIEW)", (): void => {
      expect.hasAssertions();

      const group = {
        __typename: "Group",
        closedVulnerabilities: null,
        description: "Suspended group",
        hasAdvanced: false,
        hasEssential: true,
        managed: "UNDER_REVIEW",
        name: "jdoefluidattacks",
        openFindings: 0,
        openVulnerabilities: null,
        roots: [
          {
            __typename: "GitRoot",
            cloningStatus: {
              __typename: "GitRootCloningStatus",
              status: "UNKNOWN",
            },
          },
        ],
        service: "WHITE",
        subscription: "continuous",
        userRole: "group_manager",
      } as unknown as IGroupDataResult;

      const gitRoots = group.roots.filter(
        (root): root is IGitRootAttr => root.__typename === "GitRoot",
      );

      const result = noVulnerabilitiesFound(gitRoots, group);

      expect(result).toBe(true);
    });

    it("should have no vulnerabilities found (machine execution is ongoing)", (): void => {
      expect.hasAssertions();

      const group = {
        __typename: "Group",
        closedVulnerabilities: null,
        description: "Trial group",
        hasAdvanced: false,
        hasEssential: true,
        managed: "TRIAL",
        name: "jdoefluidattacks",
        openFindings: null,
        openVulnerabilities: null,
        roots: [
          {
            __typename: "GitRoot",
            cloningStatus: {
              __typename: "GitRootCloningStatus",
              status: "QUEUED",
            },
            machineStatus: {
              modifiedDate: dayjs().format(),
              status: "IN_PROGRESS",
            },
          },
        ],
        service: "WHITE",
        subscription: "continuous",
        userRole: "group_manager",
      } as unknown as IGroupDataResult;

      const gitRoots = group.roots.filter(
        (root): root is IGitRootAttr => root.__typename === "GitRoot",
      );

      const result = noVulnerabilitiesFound(gitRoots, group);

      expect(result).toBe(false);
    });

    it("should have no vulnerabilities found (machine execution finished)", (): void => {
      expect.hasAssertions();

      const group = {
        __typename: "Group",
        closedVulnerabilities: null,
        description: "Trial group",
        hasAdvanced: false,
        hasEssential: true,
        managed: "TRIAL",
        name: "jdoefluidattacks",
        openFindings: null,
        openVulnerabilities: null,
        roots: [
          {
            __typename: "GitRoot",
            cloningStatus: {
              __typename: "GitRootCloningStatus",
              status: "OK",
            },
            machineStatus: {
              modifiedDate: "2023-08-08T00:00:00+00:00",
              status: "SUCCESS",
            },
          },
        ],
        service: "WHITE",
        subscription: "continuous",
        userRole: "group_manager",
      } as unknown as IGroupDataResult;

      const gitRoots = group.roots.filter(
        (root): root is IGitRootAttr => root.__typename === "GitRoot",
      );

      const result = noVulnerabilitiesFound(gitRoots, group);

      expect(result).toBe(true);
    });

    it("should have no vulnerabilities found (machine execution failed)", (): void => {
      expect.hasAssertions();

      const group = {
        __typename: "Group",
        closedVulnerabilities: null,
        description: "Trial group",
        hasAdvanced: false,
        hasEssential: true,
        managed: "TRIAL",
        name: "jdoefluidattacks",
        openFindings: null,
        openVulnerabilities: null,
        roots: [
          {
            __typename: "GitRoot",
            cloningStatus: {
              __typename: "GitRootCloningStatus",
              status: "OK",
            },
            machineStatus: {
              modifiedDate: dayjs().subtract(3, "hour").format(),
              status: "FAILED",
            },
          },
        ],
        service: "WHITE",
        subscription: "continuous",
        userRole: "group_manager",
      } as unknown as IGroupDataResult;

      const gitRoots = group.roots.filter(
        (root): root is IGitRootAttr => root.__typename === "GitRoot",
      );

      const result = noVulnerabilitiesFound(gitRoots, group);

      expect(result).toBe(true);
    });
  });

  describe("filter findings", (): void => {
    const setup = async (): Promise<void> => {
      const mockedPermissions = new PureAbility<string>([
        { action: "see_draft_status" },
        { action: "see_review_filter" },
      ]);
      render(
        <authzPermissionsContext.Provider value={mockedPermissions}>
          <Wrapper
            element={<GroupFindings />}
            path={"/orgs/:organizationName/groups/:groupName/vulns"}
          />
        </authzPermissionsContext.Provider>,
        {
          memoryRouter: {
            initialEntries: ["/orgs/okada/groups/TEST/vulns"],
          },
          mocks: [...apolloDataMock, ...mockCriteria],
        },
      );

      await userEvent.click(
        screen.getByRole("checkbox", { name: "cvssToggle" }),
      );
    };

    const cleanup = async (): Promise<void> => {
      await userEvent.click(
        screen.getByRole("button", { hidden: true, name: "filter.cancel" }),
      );
    };

    it("should render add filter button", async (): Promise<void> => {
      expect.hasAssertions();

      await setup();

      await waitFor(
        async (): Promise<void> =>
          expect(screen.findAllByRole("row")).resolves.toHaveLength(4),
      );

      expect(
        screen.getByRole("button", { name: "filter.title" }),
      ).toBeInTheDocument();
    });

    it("should render clear filters button", async (): Promise<void> => {
      expect.hasAssertions();

      await setup();

      const addButton = (): HTMLElement =>
        screen.getByRole("button", { name: "filter.title" });
      const clearButton = (): HTMLElement =>
        screen.getByRole("button", { hidden: true, name: "filter.cancel" });

      await userEvent.click(addButton());

      expect(clearButton()).toBeInTheDocument();

      await userEvent.click(clearButton());

      await waitFor((): void => {
        expect(screen.getAllByRole("row")).toHaveLength(4);
      });
    });

    it("should filter findings by last report", async (): Promise<void> => {
      expect.hasAssertions();

      await setup();

      await userEvent.click(
        screen.getByRole("button", { name: "filter.title" }),
      );

      await userEvent.type(
        await screen.findByRole("spinbutton", { name: "lastVulnerability" }),
        "22",
      );

      expect(
        screen.queryAllByText("group.findings.lastReport")[0]
          ?.nextElementSibling?.textContent,
      ).toBe("22");
      expect(screen.getAllByRole("row")).toHaveLength(3);

      await cleanup();
    });

    it("should filter findings by type", async (): Promise<void> => {
      expect.hasAssertions();

      await setup();

      await userEvent.click(
        screen.getByRole("button", { name: "filter.title" }),
      );

      await userEvent.click(
        document.querySelectorAll("#title-select")[0].firstChild as Element,
      );

      await userEvent.click(
        within(screen.getByTestId("title-select")).getByText(
          "038. Business information leak",
        ),
      );

      expect(
        screen.queryAllByText("group.findings.type")[0]?.nextElementSibling
          ?.textContent,
      ).toBe("038. Business information leak");

      expect(screen.getAllByRole("row")).toHaveLength(2);

      await userEvent.click(screen.getByTestId("title-select"));
      await cleanup();

      expect(screen.getAllByRole("row")).toHaveLength(4);
      expect(
        screen.queryByText(
          "group.findings.type = 038. Business information leak",
        ),
      ).not.toBeInTheDocument();

      await cleanup();
    });

    it("should filter findings by treatment", async (): Promise<void> => {
      expect.hasAssertions();

      await setup();

      await userEvent.click(
        screen.getByRole("button", { name: "filter.title" }),
      );

      await userEvent.click(
        document.querySelectorAll("#treatment-select")[0].firstChild as Element,
      );
      await userEvent.click(
        within(screen.getByTestId("treatment-select")).getByText(
          "searchFindings.tabDescription.treatment.inProgress",
        ),
      );

      expect(
        screen.queryAllByText("group.findings.treatment")[0]?.nextElementSibling
          ?.textContent,
      ).toBe("searchFindings.tabDescription.treatment.inProgress");

      expect(screen.getAllByRole("row")).toHaveLength(2);
      expect(
        within(screen.getAllByRole("row")[1]).getByText(
          "001. SQL injection - C Sharp SQL API",
        ),
      ).toBeInTheDocument();

      await userEvent.click(
        document.querySelectorAll("#treatment-select")[0].firstChild as Element,
      );
      await userEvent.click(
        screen.getByText("searchFindings.tabDescription.treatment.new"),
      );

      expect(
        screen.queryAllByText("group.findings.treatment")[0]?.nextElementSibling
          ?.textContent,
      ).toBe("searchFindings.tabDescription.treatment.new");
      expect(screen.getAllByRole("row")).toHaveLength(2);
      expect(
        within(screen.getAllByRole("row")[1]).getByText(
          "038. Business information leak",
        ),
      ).toBeInTheDocument();

      await cleanup();
    });

    it("should filter findings by severity score", async (): Promise<void> => {
      expect.hasAssertions();

      await setup();

      await userEvent.click(
        screen.getByRole("button", { name: "filter.title" }),
      );

      const minInput = screen.findByRole("spinbutton", {
        name: "maxOpenSeverityScore-min",
      });
      const maxInput = screen.findByRole("spinbutton", {
        name: "maxOpenSeverityScore-max",
      });

      await userEvent.type(await minInput, "4");

      expect(
        screen.queryAllByText("group.findings.severity")[0]?.nextElementSibling
          ?.textContent,
      ).toBe("Min 4");

      await waitFor(async (): Promise<void> => {
        await expect(
          screen.findByText("table.noDataIndication"),
        ).resolves.toBeInTheDocument();
      });

      await userEvent.clear(await minInput);
      await userEvent.type(await minInput, "2");
      await userEvent.type(await maxInput, "4");

      expect(
        screen.queryAllByText("group.findings.severity")[0]?.nextElementSibling
          ?.textContent,
      ).toBe("2 - 4");

      expect(screen.getAllByRole("row")).toHaveLength(3);

      expect(
        within(screen.queryAllByRole("row")[1]).getAllByRole("cell")[1]
          .textContent,
      ).toBe("Vulnerable");

      await userEvent.click(screen.getAllByRole("checkbox")[0]);

      await waitFor((): void => {
        expect(screen.getAllByRole("row")).toHaveLength(2);
      });

      expect(
        within(screen.queryAllByRole("row")[1]).getAllByRole("cell")[3]
          .textContent,
      ).toBe("6");

      await userEvent.click(screen.getAllByRole("checkbox")[0]);

      await waitFor((): void => {
        expect(screen.getAllByRole("row")).toHaveLength(3);
      });

      await userEvent.click(
        screen.getByRole("button", { name: "filter.title" }),
      );
      await waitFor((): void => {
        expect(
          screen.queryByRole("button", { hidden: true, name: "filter.cancel" }),
        ).toBeInTheDocument();
      });

      await cleanup();
    });

    it("should filter findings by age", async (): Promise<void> => {
      expect.hasAssertions();

      await setup();
      await userEvent.click(
        screen.getByRole("button", { name: "filter.title" }),
      );

      const ageInput = await screen.findByRole("spinbutton", { name: "age" });

      await userEvent.type(ageInput, "240");

      expect(
        screen.queryAllByText("group.findings.age")[0]?.nextElementSibling
          ?.textContent,
      ).toBe("240");
      expect(screen.getAllByRole("row")).toHaveLength(2);

      await waitFor((): void => {
        expect(
          within(screen.queryAllByRole("row")[1]).getAllByRole("cell")[0]
            .textContent,
        ).toBe("001. SQL injection - C Sharp SQL API");
      });

      await userEvent.clear(ageInput);
      await userEvent.type(ageInput, "14");

      expect(
        screen.queryAllByText("group.findings.age")[0]?.nextElementSibling
          ?.textContent,
      ).toBe("14");

      await waitFor((): void => {
        expect(
          screen.queryByText("table.noDataIndication"),
        ).toBeInTheDocument();
      });
      await cleanup();
    });

    it("should filter findings by review", async (): Promise<void> => {
      expect.hasAssertions();

      await setup();
      await userEvent.click(
        screen.getByRole("button", { name: "filter.title" }),
      );

      await userEvent.click(
        document.querySelectorAll("#review-select")[0].firstChild as Element,
      );

      await userEvent.click(screen.getByText("group.findings.boolean.True"));

      expect(
        screen.queryAllByText("group.findings.review")[0]?.nextElementSibling
          ?.textContent,
      ).toBe("group.findings.boolean.True");

      await waitFor(async (): Promise<void> => {
        await expect(
          screen.findByText("table.noDataIndication"),
        ).resolves.toBeInTheDocument();
      });

      await userEvent.click(
        document.querySelectorAll("#review-select")[0].firstChild as Element,
      );
      await userEvent.click(
        within(screen.getByTestId("review-select")).getByText(
          "group.findings.boolean.False",
        ),
      );

      expect(
        screen.queryAllByText("group.findings.review")[0]?.nextElementSibling
          ?.textContent,
      ).toBe("group.findings.boolean.False");

      expect(screen.queryAllByRole("row")).toHaveLength(4);

      await cleanup();
    });

    it("should filter findings by release date", async (): Promise<void> => {
      expect.hasAssertions();

      await setup();
      await userEvent.click(
        screen.getByRole("button", { name: "filter.title" }),
      );

      await userEvent.type(
        await screen.findByRole("spinbutton", { name: /month, Start Date/u }),
        "02",
      );
      await userEvent.type(
        screen.getByRole("spinbutton", { name: /day, Start Date/u }),
        "05",
      );
      await userEvent.type(
        screen.getByRole("spinbutton", { name: /year, Start Date/u }),
        "2020",
      );
      await userEvent.type(
        screen.getByRole("spinbutton", { name: /month, End Date/u }),
        "07",
      );
      await userEvent.type(
        screen.getByRole("spinbutton", { name: /day, End Date/u }),
        "05",
      );
      await userEvent.type(
        screen.getByRole("spinbutton", { name: /year, End Date/u }),
        "2020",
      );

      expect(
        screen.queryAllByText("group.findings.releaseDate")[0]
          ?.nextElementSibling?.textContent,
      ).toBe("February 5, 2020 - July 5, 2020");

      await waitFor(async (): Promise<void> => {
        await expect(screen.findAllByRole("row")).resolves.toHaveLength(4);
      });
      await cleanup();
    });

    it("should filter findings by state", async (): Promise<void> => {
      expect.hasAssertions();

      await setup();
      await userEvent.click(
        screen.getByRole("button", { name: "filter.title" }),
      );

      await userEvent.click(
        document.querySelectorAll("#state-select")[0].firstChild as Element,
      );
      await userEvent.click(
        screen.getByText("searchFindings.header.status.draft.label"),
      );

      await waitFor(async (): Promise<void> => {
        await expect(
          screen.findByText("table.noDataIndication"),
        ).resolves.toBeInTheDocument();
      });

      await userEvent.click(
        document.querySelectorAll("#state-select")[0].firstChild as Element,
      );
      await userEvent.click(
        within(screen.getByTestId("state-select")).getByText(
          "searchFindings.header.status.closed.label",
        ),
      );

      await waitFor(async (): Promise<void> => {
        await expect(screen.findAllByRole("row")).resolves.toHaveLength(2);
      });

      expect(
        within(screen.getAllByRole("row")[1]).getByText(
          "071. Insecure or unset HTTP headers - Referrer-Policy",
        ),
      ).toBeInTheDocument();

      await cleanup();
    });

    it("should filter findings by reattack", async (): Promise<void> => {
      expect.hasAssertions();

      await setup();

      expect(
        screen.getByRole("button", { name: "filter.title" }),
      ).toBeInTheDocument();

      await userEvent.click(
        screen.getByRole("button", { name: "filter.title" }),
      );

      await userEvent.click(
        screen.getByRole("button", { hidden: true, name: "filter.cancel" }),
      );

      await waitFor((): void => {
        expect(screen.getAllByRole("row")).toHaveLength(4);
      });

      await userEvent.click(
        document.querySelectorAll("#reattack-select")[0].firstChild as Element,
      );

      await userEvent.click(
        within(screen.getByTestId("reattack-select")).getByText("Pending"),
      );

      expect(screen.queryByText("filter.applied")).toBeInTheDocument();

      expect(
        screen.queryAllByText("group.findings.reattack")[0]?.nextElementSibling
          ?.textContent,
      ).toBe("Pending");

      expect(screen.getAllByRole("row")).toHaveLength(2);

      await waitFor((): void => {
        expect(
          within(screen.queryAllByRole("row")[1]).getAllByRole("cell")[0]
            .textContent,
        ).toBe("038. Business information leak");
      });

      await userEvent.click(
        document.querySelectorAll("#reattack-select")[0].firstChild as Element,
      );

      await userEvent.click(screen.getByText("-"));

      expect(
        screen.queryAllByText("group.findings.reattack")[0]?.nextElementSibling
          ?.textContent,
      ).toBe("-");
      expect(screen.getAllByRole("row")).toHaveLength(3);

      await cleanup();
    });

    it("should filter findings by location", async (): Promise<void> => {
      expect.hasAssertions();

      await setup();

      await waitFor((): void => {
        expect(screen.getAllByRole("row")).toHaveLength(4);
      });

      await userEvent.click(
        screen.getByRole("button", { name: "filter.title" }),
      );

      await userEvent.click(
        await screen.findByRole("combobox", { name: "root" }),
      );
      await userEvent.paste("back/src/model/user/index.js");

      await waitFor((): void => {
        expect(screen.getAllByRole("row")).toHaveLength(2);
      });

      await waitFor((): void => {
        expect(
          within(screen.queryAllByRole("row")[1]).getAllByRole("cell")[0]
            .textContent,
        ).toBe("004. Remote command execution");
      });

      await cleanup();
    });
  });
});
