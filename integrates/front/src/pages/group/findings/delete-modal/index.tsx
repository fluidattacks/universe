import { useMutation } from "@apollo/client";
import { ComboBox, Form, InnerForm, Modal } from "@fluidattacks/design";
import { useCallback, useState } from "react";
import { useTranslation } from "react-i18next";
import { mixed, object } from "yup";

import type { IDeleteModalProps } from "./types";

import {
  getAreAllMutationValid,
  getResults,
  handleRemoveFindingsError,
} from "../utils";
import { REMOVE_FINDING_MUTATION } from "pages/finding/queries";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

const DeleteModal = ({
  modalRef,
  refetch,
  selectedFindings,
}: IDeleteModalProps): JSX.Element => {
  const { t } = useTranslation();
  const [isRunning, setIsRunning] = useState(false);
  const { close } = modalRef;
  const prefix = "group.findings.deleteModal.justification.";

  const [removeFinding, { loading: deleting }] = useMutation(
    REMOVE_FINDING_MUTATION,
    {
      onError: ({ graphQLErrors }): void => {
        graphQLErrors.forEach((error): void => {
          if (
            error.message ===
            "Invalid, you cannot delete a finding that contains released vulnerabilities"
          ) {
            msgError(t("group.findings.deleteModal.errors.invalidFinding"));
            close();
          } else {
            msgError(t("groupAlerts.errorTextsad"));
            Logger.warning("An error occurred deleting finding", error);
          }
        });
      },
    },
  );

  const validMutationsHelper = useCallback(
    (handleCloseModal: () => void, areAllMutationValid: boolean[]): void => {
      if (areAllMutationValid.every(Boolean)) {
        msgSuccess(
          t("group.findings.deleteModal.alerts.vulnerabilitiesDeleted"),
          t("searchFindings.successTitle"),
        );
        void refetch();
        handleCloseModal();
      }
    },
    [refetch, t],
  );

  const handleRemoveFinding = useCallback(
    async (justification: unknown): Promise<void> => {
      setIsRunning(true);
      if (selectedFindings.length === 0) {
        msgError(t("searchFindings.tabResources.noSelection"));
        setIsRunning(false);
      } else {
        try {
          const results = await getResults(
            removeFinding,
            selectedFindings,
            justification,
          );
          const areAllMutationValid = getAreAllMutationValid(results);
          validMutationsHelper(close, areAllMutationValid);
        } catch (updateError: unknown) {
          handleRemoveFindingsError(updateError);
        } finally {
          setIsRunning(false);
        }
      }
    },
    [close, removeFinding, selectedFindings, t, validMutationsHelper],
  );

  const handleDelete = useCallback(
    async (values: Record<string, unknown>): Promise<void> => {
      await handleRemoveFinding(values.justification);
    },
    [handleRemoveFinding],
  );

  return (
    <Modal
      id={"removeVulnerability"}
      modalRef={modalRef}
      size={"sm"}
      title={t("group.findings.deleteModal.title")}
    >
      <Form
        cancelButton={{ onClick: close }}
        confirmButton={{ disabled: isRunning || deleting }}
        defaultValues={{ justification: "" }}
        onSubmit={handleDelete}
        yupSchema={object().shape({
          justification: mixed().required(t("validations.required")),
        })}
      >
        <InnerForm>
          {({ control }): JSX.Element => (
            <ComboBox
              control={control}
              items={[
                { name: t(`${prefix}duplicated`), value: "DUPLICATED" },
                { name: t(`${prefix}falsePositive`), value: "FALSE_POSITIVE" },
                { name: t(`${prefix}notRequired`), value: "NOT_REQUIRED" },
              ]}
              label={t("group.findings.deleteModal.justification.label")}
              name={"justification"}
              required={true}
            />
          )}
        </InnerForm>
      </Form>
    </Modal>
  );
};

export { DeleteModal };
