import type { ApolloQueryResult } from "@apollo/client";
import type { IUseModal } from "@fluidattacks/design";

import type { IFindingAttr } from "../types";
import type { GetFindingsQueryQuery } from "gql/graphql";

interface IDeleteModalProps {
  modalRef: IUseModal;
  refetch: () =>
    | Promise<ApolloQueryResult<GetFindingsQueryQuery>>
    | Promise<void>;
  selectedFindings: IFindingAttr[];
}

export type { IDeleteModalProps };
