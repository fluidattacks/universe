import { createColumnHelper } from "@tanstack/react-table";
import type { CellContext, ColumnDef } from "@tanstack/react-table";
import type { ExecutionResult } from "graphql";
import type { TFunction } from "i18next";
import _ from "lodash";

import type {
  IFindingAttr,
  IFindingSuggestionData,
  ITreatmentSummaryAttr,
  IVerificationSummaryAttr,
  IVulnerabilitiesResume,
  IVulnerabilityCriteriaData,
  TStatus,
} from "./types";

import { statusFormatter } from "features/vulnerabilities/formatters/status";
import type { GetFindingsQueryQuery } from "gql/graphql";
import type { ICVSSVersionObject } from "hooks/use-settings-store";
import { validateNotEmpty } from "pages/finding/description/utils";
import type { IRemoveFindingResultAttr } from "pages/finding/types";
import { getPriorityScore } from "utils/cvss";
import { formatDate } from "utils/date";
import {
  formatPercentage,
  includeTagsFormatter,
  severityOverviewFormatter,
} from "utils/format-helpers";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";
import { translate } from "utils/translations/translate";

type TRemoveFindingResult = ExecutionResult<IRemoveFindingResultAttr>;

const formatReattack = (
  verificationSummary: IVerificationSummaryAttr,
): string =>
  translate.t(
    verificationSummary.requested > 0 || verificationSummary.onHold > 0
      ? "group.findings.reattackValues.True"
      : "group.findings.reattackValues.False",
  );

const formatState = (state: string): JSX.Element => {
  const stateParameters: Record<string, string> = {
    DRAFT: "searchFindings.header.status.draft.label",
    REJECTED: "searchFindings.header.status.rejected.label",
    SAFE: "searchFindings.header.status.closed.label",
    SUBMITTED: "searchFindings.header.status.submitted.label",
    VULNERABLE: "searchFindings.header.status.open.label",
    closed: "searchFindings.header.status.closed.label",
    open: "searchFindings.header.status.open.label",
  };

  return statusFormatter(translate.t(stateParameters[state]));
};

const formatStatus = (status: string): JSX.Element => {
  return statusFormatter(status, true);
};

const formatVisibleStatus = (status: TStatus, requested: number): TStatus => {
  return status === "DRAFT" && requested > 0 ? "VULNERABLE" : status;
};

const formatTreatmentSummary = (
  state: "DRAFT" | "SAFE" | "VULNERABLE",
  treatmentSummary: ITreatmentSummaryAttr,
): string =>
  state === "VULNERABLE"
    ? `
${translate.t("searchFindings.tabDescription.treatment.new")}: ${
        treatmentSummary.untreated
      },
${translate.t("searchFindings.tabDescription.treatment.inProgress")}: ${
        treatmentSummary.inProgress
      },
${translate.t("searchFindings.tabDescription.treatment.accepted")}: ${
        treatmentSummary.accepted
      },
${translate.t("searchFindings.tabDescription.treatment.acceptedUndefined")}: ${
        treatmentSummary.acceptedUndefined
      }
`
    : "-";

const formatClosingPercentage = (finding: IFindingAttr): number => {
  const { closed, open } = finding.vulnerabilitiesSummary;

  if (open + closed === 0) {
    return finding.status === "SAFE" ? 1.0 : 0;
  }

  return closed / (open + closed);
};
//
const formatFindings = (
  findings: IFindingAttr[],
  findingLocations: Record<string, IVulnerabilitiesResume>,
): IFindingAttr[] =>
  findings
    .filter(
      (finding): boolean =>
        !(
          finding.vulnerabilitiesSummaryV4.open === 0 &&
          finding.vulnerabilitiesSummaryV4.closed === 0 &&
          (finding.zeroRiskSummary?.confirmed ?? 0) !== 0
        ),
    )
    .map(
      (finding): IFindingAttr => ({
        ...finding,
        closingPercentage: formatClosingPercentage(finding),
        locationsInfo: {
          findingId: finding.id,
          locations: _.get(findingLocations, finding.id, undefined)?.wheres,
          treatmentAssignmentEmails:
            _.get(findingLocations, finding.id, undefined)
              ?.treatmentAssignmentEmails ?? new Set([]),
          vulnerabilitiesSummary: finding.vulnerabilitiesSummary,
        },
        reattack: formatReattack(finding.verificationSummary),
        status: formatVisibleStatus(
          finding.status,
          finding.zeroRiskSummary?.requested ?? 0,
        ),
        treatment: formatTreatmentSummary(
          finding.status,
          finding.treatmentSummary,
        ),
      }),
    );

const getAreAllMutationValid = (
  results: ExecutionResult<IRemoveFindingResultAttr>[],
): boolean[] => {
  return results.map((result): boolean => {
    if (!_.isUndefined(result.data) && !_.isNull(result.data)) {
      const removeInfoSuccess = _.isUndefined(result.data.removeFinding)
        ? true
        : result.data.removeFinding.success;

      return removeInfoSuccess;
    }

    return false;
  });
};

const getResults = async (
  removeFinding: (
    variables: Record<string, unknown>,
  ) => Promise<TRemoveFindingResult>,
  findings: IFindingAttr[],
  justification: unknown,
): Promise<TRemoveFindingResult[]> => {
  const chunkSize = 10;
  const vulnChunks = _.chunk(findings, chunkSize);
  const updateChunks = vulnChunks.map(
    (chunk): (() => Promise<TRemoveFindingResult[]>) =>
      async (): Promise<TRemoveFindingResult[]> => {
        const updates = chunk.map(
          async (finding): Promise<TRemoveFindingResult> =>
            removeFinding({
              variables: { findingId: finding.id, justification },
            }),
        );

        return Promise.all(updates);
      },
  );

  // Sequentially execute chunks
  return updateChunks.reduce(
    async (previousValue, currentValue): Promise<TRemoveFindingResult[]> => [
      ...(await previousValue),
      ...(await currentValue()),
    ],
    Promise.resolve<TRemoveFindingResult[]>([]),
  );
};

const handleRemoveFindingsError = (updateError: unknown): void => {
  if (
    _.includes(
      String(updateError),
      translate.t("searchFindings.tabVuln.exceptions.sameValues"),
    )
  ) {
    msgError(translate.t("searchFindings.tabVuln.exceptions.sameValues"));
  } else {
    msgError(translate.t("groupAlerts.errorTextsad"));
    Logger.warning("An error occurred removing findings", updateError);
  }
};

const getFindingSuggestions = (
  vulnsData: Record<string, IVulnerabilityCriteriaData>,
  language: "en" | "es" | undefined = "en",
): IFindingSuggestionData[] => {
  const findingSuggestions = Object.keys(vulnsData).map(
    (key): IFindingSuggestionData => {
      // Base score metrics
      const attackVector = validateNotEmpty(
        vulnsData[key].score.base.attackVector,
      );
      const attackComplexity = validateNotEmpty(
        vulnsData[key].score.base.attackComplexity,
      );
      const privilegesRequired = validateNotEmpty(
        vulnsData[key].score.base.privilegesRequired,
      );
      const userInteraction = validateNotEmpty(
        vulnsData[key].score.base.userInteraction,
      );
      const severityScope = validateNotEmpty(vulnsData[key].score.base.scope);
      const confidentialityImpact = validateNotEmpty(
        vulnsData[key].score.base.confidentiality,
      );
      const integrityImpact = validateNotEmpty(
        vulnsData[key].score.base.integrity,
      );
      const availabilityImpact = validateNotEmpty(
        vulnsData[key].score.base.availability,
      );
      // Temporal score metrics
      const exploitability = validateNotEmpty(
        vulnsData[key].score.temporal.exploitCodeMaturity,
      );

      const remediationLevel = validateNotEmpty(
        vulnsData[key].score.temporal.remediationLevel,
      );
      const reportConfidence = validateNotEmpty(
        vulnsData[key].score.temporal.reportConfidence,
      );

      const minTimeToRemediateRaw = validateNotEmpty(
        vulnsData[key].remediationTime,
      );
      const minTimeToRemediate = minTimeToRemediateRaw
        ? _.parseInt(minTimeToRemediateRaw)
        : null;
      const { requirements } = vulnsData[key];

      return {
        attackVectorDescription: validateNotEmpty(
          vulnsData[key][language].impact,
        ),
        code: key,
        description: validateNotEmpty(vulnsData[key][language].description),
        minTimeToRemediate,
        recommendation: validateNotEmpty(
          vulnsData[key][language].recommendation,
        ),
        score: {
          attackComplexity,
          attackVector,
          availabilityImpact,
          confidentialityImpact,
          exploitability,
          integrityImpact,
          privilegesRequired,
          remediationLevel,
          reportConfidence,
          severityScope,
          userInteraction,
        },
        scoreV4: {
          attackComplexity: validateNotEmpty(
            vulnsData[key].scoreV4.base.attackComplexity,
          ),
          attackRequirements: validateNotEmpty(
            vulnsData[key].scoreV4.base.attackRequirements,
          ),
          attackVector: validateNotEmpty(
            vulnsData[key].scoreV4.base.attackVector,
          ),
          availabilitySubsequentImpact: validateNotEmpty(
            vulnsData[key].scoreV4.base.availabilitySa,
          ),
          availabilityVulnerableImpact: validateNotEmpty(
            vulnsData[key].scoreV4.base.availabilityVa,
          ),
          confidentialitySubsequentImpact: validateNotEmpty(
            vulnsData[key].scoreV4.base.confidentialitySc,
          ),
          confidentialityVulnerableImpact: validateNotEmpty(
            vulnsData[key].scoreV4.base.confidentialityVc,
          ),
          exploitability: validateNotEmpty(
            vulnsData[key].scoreV4.threat.exploitMaturity,
          ),
          integritySubsequentImpact: validateNotEmpty(
            vulnsData[key].scoreV4.base.integritySi,
          ),
          integrityVulnerableImpact: validateNotEmpty(
            vulnsData[key].scoreV4.base.integrityVi,
          ),
          privilegesRequired: validateNotEmpty(
            vulnsData[key].scoreV4.base.privilegesRequired,
          ),
          userInteraction: validateNotEmpty(
            vulnsData[key].scoreV4.base.userInteraction,
          ),
        },
        threat: validateNotEmpty(vulnsData[key][language].threat),
        title: validateNotEmpty(vulnsData[key][language].title),
        unfulfilledRequirements: requirements,
      };
    },
  );

  return findingSuggestions;
};
const isGroupInfoFilled = (data: GetFindingsQueryQuery | undefined): boolean =>
  !_.isEmpty(data?.group.description) &&
  !_.isEmpty(data?.group.businessId) &&
  !_.isEmpty(data?.group.businessName);

const getGroupOpenVersion = (
  cvssVersion: string,
  groupOpenCVSSF: number | undefined,
  groupOpenCVSSFV4: number | undefined,
): number =>
  (cvssVersion === "maxOpenSeverityScore"
    ? groupOpenCVSSF
    : groupOpenCVSSFV4) ?? 0;

const columnHelper = createColumnHelper<IFindingAttr>();

const getTableColumns = (
  meetingMode: boolean,
  canGetHacker: boolean,
  cvssVersion: ICVSSVersionObject,
  groupTotalPriority: number | undefined,
  t: TFunction,
): ColumnDef<IFindingAttr>[] => [
  columnHelper.group({
    columns: [
      columnHelper.accessor("title", {
        cell: (
          cell: CellContext<IFindingAttr, unknown>,
        ): JSX.Element | string => {
          const finding: IFindingAttr = cell.row.original;
          const DAYS_IN_A_WEEK = 7;

          return includeTagsFormatter({
            newTag:
              finding.lastVulnerability <= DAYS_IN_A_WEEK &&
              finding.vulnerabilitiesSummary.open > 0,
            reviewTag:
              !meetingMode &&
              ((_.isNumber(finding.rejectedVulnerabilities) &&
                finding.rejectedVulnerabilities > 0) ||
                (_.isNumber(finding.submittedVulnerabilities) &&
                  finding.submittedVulnerabilities > 0)),
            text: finding.title,
          });
        },
        header: "Type",
      }),
      columnHelper.accessor("status", {
        cell: (cell: CellContext<IFindingAttr, unknown>): JSX.Element =>
          formatState(String(cell.getValue())),
        header: "Status",
      }),
      columnHelper.accessor("description", { header: "Description" }),
    ],
    footer: (props): string => props.column.id,
    header: "Vulnerability information",
  }),
  columnHelper.group({
    columns: [
      columnHelper.accessor("totalOpenPriority", {
        cell: (cell): string =>
          getPriorityScore(
            cell.row.original.totalOpenPriority,
            groupTotalPriority ?? 0,
            cell.row.original.status,
          ),
        header: t("searchFindings.tabVuln.vulnTable.priority"),
        id: "priority",
      }),
      columnHelper.accessor("vulnerabilitiesSummary.open", {
        header: "Open vulns",
      }),
      columnHelper.display({
        cell: (cell: CellContext<IFindingAttr, unknown>): JSX.Element =>
          severityOverviewFormatter(
            cell.row.original[cvssVersion.summaryKey].openCritical,
            cell.row.original[cvssVersion.summaryKey].openHigh,
            cell.row.original[cvssVersion.summaryKey].openMedium,
            cell.row.original[cvssVersion.summaryKey].openLow,
          ),
        header: "Severity overview",
        id: "severityOverview",
      }),
    ],
    footer: (props): string => props.column.id,
    header: "Risk and severity",
  }),
  columnHelper.group({
    columns: [
      columnHelper.accessor("lastVulnerability", {
        cell: (cell): string => {
          const value = Number(cell.getValue());

          return t("group.findings.description.value", {
            count: value,
          });
        },
        header: "Last report",
      }),
      columnHelper.accessor("age", { header: "Age" }),
      columnHelper.accessor("releaseDate", {
        cell: (cell): string => formatDate(String(cell.getValue() ?? "")),
        header: "Release date",
      }),
    ],
    footer: (props): string => props.column.id,
    header: "Time and tracking",
  }),
  columnHelper.group({
    columns: [
      columnHelper.accessor("closingPercentage", {
        cell: (cell: CellContext<IFindingAttr, unknown>): string =>
          formatPercentage(Number(cell.getValue())),
        header: t("group.findings.closingPercentage"),
      }),
      columnHelper.accessor("reattack", { header: "Reattack" }),
      columnHelper.display({
        cell: (cell): string => {
          const treatment = cell.row.original.treatmentSummary;

          return `Untreated: ${treatment.untreated}, In progress: ${treatment.inProgress},
            Temporarily accepted:  ${treatment.accepted}, Permanently accepted:
            ${treatment.acceptedUndefined}`;
        },
        header: "Treatment",
      }),
      ...(canGetHacker
        ? [columnHelper.accessor("hacker", { header: "Reported by" })]
        : []),
    ],
    footer: (props): string => props.column.id,
    header: "Vulnerability management",
  }),
];

export {
  formatFindings,
  formatState,
  formatStatus,
  formatTreatmentSummary,
  formatReattack,
  getAreAllMutationValid,
  getFindingSuggestions,
  getGroupOpenVersion,
  getResults,
  getTableColumns,
  handleRemoveFindingsError,
  isGroupInfoFilled,
};
