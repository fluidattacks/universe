import {
  Checkbox,
  Col,
  ComboBox,
  Container,
  Form,
  type IItem,
  InnerForm,
  Input,
  InputDate,
  InputDateRange,
  InputNumber,
  InputNumberRange,
  OutlineContainer,
  Row,
} from "@fluidattacks/design";
import { getLocalTimeZone, today } from "@internationalized/date";
import { camelCase, isEmpty, isNil } from "lodash";
import { Fragment, useCallback } from "react";
import { useTranslation } from "react-i18next";

import type {
  IFilterFormProps,
  IFormValues,
  IReportParams,
} from "../../../types";
import { validationSchema } from "../../../validations";
import {
  findingTitle,
  getAge,
  getMaxSeverity,
  getMinSeverity,
  lastReport,
  location,
} from "../../utils";
import { formatDate } from "utils/date";

export const FilterForm = ({
  onClose,
  requestGroupReport,
  setIsVerifyDialogOpen,
  setVerifyCallbacks,
  typesOptions,
}: IFilterFormProps): JSX.Element => {
  const { t } = useTranslation();

  const onRequestReport = useCallback(
    (values: IFormValues): void => {
      const reportParams: IReportParams = {
        age: getAge(values),
        closingDate:
          formatDate(values.closingDate) === "-"
            ? undefined
            : formatDate(values.closingDate),
        findingTitle: findingTitle(values),
        lastReport: lastReport(values),
        location: location(values),
        maxReleaseDate:
          formatDate(values.maxReleaseDate) === "-"
            ? undefined
            : formatDate(values.maxReleaseDate),
        maxSeverity: getMaxSeverity(values),
        minReleaseDate:
          formatDate(values.minReleaseDate) === "-"
            ? undefined
            : formatDate(values.minReleaseDate),
        minSeverity: getMinSeverity(values),
        states: values.states,
        treatments: isEmpty(values.closingDate) ? values.treatments : undefined,
        verificationCode: "",
        verifications: values.verifications,
      };

      setVerifyCallbacks(
        async (verificationCode: string): Promise<void> => {
          await requestGroupReport({
            ...reportParams,
            verificationCode,
          });
        },
        (): void => {
          setIsVerifyDialogOpen(false);
        },
      );

      setIsVerifyDialogOpen(true);
    },
    [requestGroupReport, setIsVerifyDialogOpen, setVerifyCallbacks],
  );

  const selectTypeOptions = typesOptions.map(
    (typeCode: string): IItem => ({ name: typeCode, value: typeCode }),
  );

  return (
    <Form
      cancelButton={{ onClick: onClose }}
      confirmButton={{ label: t("group.findings.report.generateXls") }}
      defaultValues={{
        age: undefined,
        closingDate: "",
        findingTitle: "",
        lastReport: undefined,
        location: "",
        maxReleaseDate: "",
        maxSeverity: undefined,
        minReleaseDate: "",
        minSeverity: undefined,
        startClosingDate: "",
        states: [],
        treatments: [],
        verifications: [],
      }}
      id={"reportTreatments"}
      onSubmit={onRequestReport}
      yupSchema={validationSchema}
    >
      <InnerForm<IFormValues>>
        {({
          control,
          formState,
          register,
          setValue,
          trigger,
          watch,
        }): JSX.Element => {
          const closingDate = watch("closingDate");
          const reValidate = (name: keyof IFormValues): VoidFunction => {
            return (): void => {
              void trigger(name);
            };
          };

          return (
            <Fragment>
              <Row align={"start"} justify={"start"}>
                <Col lg={33} md={33} sm={33}>
                  <ComboBox
                    control={control}
                    items={selectTypeOptions}
                    label={t("group.findings.report.findingTitle.text")}
                    name={"findingTitle"}
                    tooltip={t("group.findings.report.findingTitle.tooltip")}
                  />
                </Col>
                <Col lg={66} md={66} sm={66}>
                  <InputDateRange
                    error={
                      formState.errors.minReleaseDate?.message ??
                      formState.errors.maxReleaseDate?.message
                    }
                    label={t("group.findings.report.releaseDate.text")}
                    name={"releaseDate"}
                    propsMax={{ name: "maxReleaseDate" }}
                    propsMin={{ name: "minReleaseDate" }}
                    register={register}
                    setValue={setValue}
                    tooltip={t("group.findings.report.releaseDate.tooltip")}
                  />
                </Col>
              </Row>
              <Row align={"start"} justify={"start"}>
                <Col lg={33} md={33} sm={33}>
                  <Input
                    error={formState.errors.location?.message}
                    isTouched={"location" in formState.touchedFields}
                    isValid={!("location" in formState.errors)}
                    label={t("group.findings.report.location.text")}
                    name={"location"}
                    register={register}
                    tooltip={t("group.findings.report.location.tooltip")}
                  />
                </Col>
                <Col lg={33} md={33} sm={33}>
                  <InputNumber
                    label={t("group.findings.report.lastReport.text")}
                    max={10000}
                    min={0}
                    name={"lastReport"}
                    register={register}
                    setValue={setValue}
                    tooltip={t("group.findings.report.lastReport.tooltip")}
                    watch={watch}
                  />
                </Col>
                <Col lg={33} md={33} sm={33}>
                  <InputNumber
                    label={t("group.findings.report.age.text")}
                    max={10000}
                    min={0}
                    name={"age"}
                    register={register}
                    setValue={setValue}
                    tooltip={t("group.findings.report.age.tooltip")}
                    watch={watch}
                  />
                </Col>
              </Row>
              <Row align={"start"} justify={"start"}>
                <Col lg={66} md={66} sm={66}>
                  <InputNumberRange
                    error={
                      formState.errors.minSeverity?.message ??
                      formState.errors.maxSeverity?.message
                    }
                    label={t("group.findings.report.severity.text")}
                    name={"severity"}
                    propsMax={{
                      decimalPlaces: 1,
                      max: 10,
                      min: 0,
                      name: "maxSeverity",
                      onChange: reValidate("minSeverity"),
                      register,
                      setValue,
                      value: watch("maxSeverity"),
                    }}
                    propsMin={{
                      decimalPlaces: 1,
                      max: 10,
                      min: 0,
                      name: "minSeverity",
                      onChange: reValidate("maxSeverity"),
                      register,
                      setValue,
                      value: watch("minSeverity"),
                    }}
                    tooltip={t("group.findings.report.severity.tooltip")}
                  />
                </Col>
                <Col lg={33} md={33} sm={33}>
                  <InputDate
                    error={formState.errors.closingDate?.message}
                    label={t("group.findings.report.closingDate.text")}
                    maxValue={today(getLocalTimeZone())}
                    name={"closingDate"}
                    register={register}
                    setValue={setValue}
                    tooltip={t("group.findings.report.closingDate.tooltip")}
                    watch={watch}
                  />
                </Col>
              </Row>
              &nbsp;
              <Row align={"start"} justify={"start"}>
                {isEmpty(closingDate) || isNil(closingDate) ? (
                  <Col lg={40} md={40} sm={40}>
                    <OutlineContainer
                      error={formState.errors.treatments?.message}
                      htmlFor={t("group.findings.report.treatment")}
                      weight={"bold"}
                    >
                      <Container
                        display={"flex"}
                        flexDirection={"column"}
                        gap={0.75}
                      >
                        {[
                          "ACCEPTED",
                          "ACCEPTED_UNDEFINED",
                          "IN_PROGRESS",
                          "UNTREATED",
                        ].map(
                          (treatment): JSX.Element => (
                            <Checkbox
                              {...register("treatments")}
                              disabled={
                                !(isEmpty(closingDate) || isNil(closingDate))
                              }
                              key={treatment}
                              label={t(
                                `searchFindings.tabDescription.treatment.${camelCase(
                                  treatment,
                                )}`,
                              )}
                              name={"treatments"}
                              value={treatment}
                            />
                          ),
                        )}
                      </Container>
                    </OutlineContainer>
                  </Col>
                ) : undefined}
                <Col lg={30} md={30} sm={30}>
                  <OutlineContainer
                    htmlFor={t("group.findings.report.reattack.title")}
                    weight={"bold"}
                  >
                    {isEmpty(closingDate) || isNil(closingDate) ? (
                      <Container
                        display={"flex"}
                        flexDirection={"column"}
                        gap={0.75}
                      >
                        {[
                          "NOT_REQUESTED",
                          "REQUESTED",
                          "ON_HOLD",
                          "VERIFIED",
                        ].map(
                          (verification): JSX.Element => (
                            <Checkbox
                              {...register("verifications")}
                              key={verification}
                              label={t(
                                `group.findings.report.reattack.${verification.toLowerCase()}`,
                              )}
                              name={"verifications"}
                              value={verification}
                            />
                          ),
                        )}
                      </Container>
                    ) : (
                      <div>
                        {["VERIFIED"].map(
                          (verification): JSX.Element => (
                            <Checkbox
                              {...register("verifications")}
                              key={verification}
                              label={t(
                                `group.findings.report.reattack.${verification.toLowerCase()}`,
                              )}
                              name={"verifications"}
                              value={verification}
                            />
                          ),
                        )}
                      </div>
                    )}
                  </OutlineContainer>
                </Col>
                <Col lg={30} md={30} sm={30}>
                  <OutlineContainer
                    error={formState.errors.states?.message}
                    htmlFor={t("group.findings.report.state")}
                    weight={"bold"}
                  >
                    {isEmpty(closingDate) || isNil(closingDate) ? (
                      <Container
                        display={"flex"}
                        flexDirection={"column"}
                        gap={0.75}
                      >
                        {["SAFE", "VULNERABLE"].map(
                          (state): JSX.Element => (
                            <Checkbox
                              {...register("states")}
                              key={state}
                              label={t(
                                `searchFindings.tabVuln.${state.toLowerCase()}`,
                              )}
                              name={"states"}
                              value={state}
                            />
                          ),
                        )}
                      </Container>
                    ) : (
                      <div>
                        {["SAFE"].map(
                          (state): JSX.Element => (
                            <Checkbox
                              {...register("states")}
                              key={state}
                              label={t(
                                `searchFindings.tabVuln.${state.toLowerCase()}`,
                              )}
                              name={"states"}
                              value={state}
                            />
                          ),
                        )}
                      </div>
                    )}
                  </OutlineContainer>
                </Col>
              </Row>
            </Fragment>
          );
        }}
      </InnerForm>
    </Form>
  );
};
