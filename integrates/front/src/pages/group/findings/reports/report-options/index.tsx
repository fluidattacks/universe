import { useLazyQuery } from "@apollo/client";
import {
  Container,
  Icon,
  ListItem,
  ListItemsWrapper,
  Modal,
  useModal,
} from "@fluidattacks/design";
import mixpanel from "mixpanel-browser";
import { useCallback, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import { useTheme } from "styled-components";

import { REQUEST_GROUP_REPORT } from "../../queries";
import { FilterReportModal } from "../filter-modal";
import type { IReportOptionsProps } from "../types";
import { setReport } from "../utils";
import { Can } from "context/authz/can";
import type { ReportType } from "gql/graphql";
import { GET_NOTIFICATIONS } from "pages/dashboard/navbar/downloads/queries";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

export const ReportOptions = ({
  enableCerts,
  typesOptions,
  setIsVerifyDialogOpen,
  setVerifyCallbacks,
}: IReportOptionsProps): JSX.Element => {
  const { groupName } = useParams() as { groupName: string };
  const { t } = useTranslation();
  const theme = useTheme();
  const location = useLocation();
  const navigate = useNavigate();
  const modalProps = useModal("incomplete-modal");
  const [isFilterReportModalOpen, setIsFilterReportModalOpen] = useState(false);
  const [isOpenIncompleteModal, setIsOpenIncompleteModal] = useState(false);

  const handleFilterReportsModal = useCallback((): void => {
    setIsFilterReportModalOpen((prev): boolean => !prev);
  }, []);

  const [requestGroupReport, { client }] = useLazyQuery(REQUEST_GROUP_REPORT, {
    onCompleted: (): void => {
      msgSuccess(
        t("groupAlerts.reportRequested"),
        t("groupAlerts.titleSuccess"),
      );
      if (isFilterReportModalOpen) {
        handleFilterReportsModal();
      }
      setIsVerifyDialogOpen?.(false);
      void client.refetchQueries({ include: [GET_NOTIFICATIONS] });
    },
    onError: (errors): void => {
      errors.graphQLErrors.forEach((error): void => {
        switch (error.message) {
          case "Exception - The user already has a requested report for the same group":
            msgError(t("groupAlerts.reportAlreadyRequested"));
            break;
          case "Exception - Stakeholder could not be verified":
            msgError(t("group.findings.report.alerts.nonVerifiedStakeholder"));
            break;
          case "Exception - The verification code is invalid":
            msgError(t("group.findings.report.alerts.invalidVerificationCode"));
            break;
          default:
            msgError(t("groupAlerts.errorTextsad"));
            Logger.warning("An error occurred requesting group report", error);
        }
      });
    },
  });

  const handleRequestGroupReport = useCallback(
    async (reportType: string, verificationCode: string): Promise<void> => {
      mixpanel.track("GroupReportRequest", { reportType });
      await requestGroupReport({
        variables: {
          groupName,
          reportType: reportType as ReportType,
          verificationCode,
        },
      });
    },
    [groupName, requestGroupReport],
  );

  const handleRequestReport = useCallback(
    (event: React.MouseEvent<HTMLElement>): void => {
      const target = event.currentTarget as HTMLElement;
      const reportType = setReport(target);
      if (reportType !== undefined) {
        setVerifyCallbacks?.(
          async (verificationCode): Promise<void> => {
            await handleRequestGroupReport(reportType, verificationCode);
          },
          (): void => {
            setIsVerifyDialogOpen?.(false);
          },
        );
      }
      setIsVerifyDialogOpen?.(true);
    },
    [handleRequestGroupReport, setVerifyCallbacks, setIsVerifyDialogOpen],
  );

  const goToScope = useCallback((): void => {
    const url = location.pathname;
    navigate(url.replace("/vulns", "/scope"));
  }, [navigate, location]);

  const handleIncompleteModal = useCallback((): void => {
    setIsOpenIncompleteModal(!isOpenIncompleteModal);
  }, [isOpenIncompleteModal]);

  return (
    <Container>
      <ListItemsWrapper>
        <Can I={"see_group_certificate"}>
          <ListItem
            id={"report-cert"}
            onClick={enableCerts ? handleRequestReport : handleIncompleteModal}
            value={"cert"}
          >
            {t("group.findings.report.cert.text")}
          </ListItem>
        </Can>
        <ListItem id={"report-zip"} onClick={handleRequestReport} value={"zip"}>
          {t("group.findings.report.data.text")}
        </ListItem>
        <ListItem id={"report-pdf"} onClick={handleRequestReport} value={"pdf"}>
          {t("group.findings.report.pdf.text")}
        </ListItem>
        <Container
          alignItems={"center"}
          borderColor={theme.palette.gray[200]}
          borderTop={"1px solid"}
          display={"flex"}
          flexDirection={"row"}
          justify={"space-between"}
        >
          <ListItem
            id={"report-excel"}
            onClick={handleRequestReport}
            value={"xls"}
          >
            {t("group.findings.report.xls.text")}
          </ListItem>
          <Container
            alignItems={"center"}
            bgColorHover={theme.palette.gray[100]}
            borderColor={theme.palette.gray[200]}
            borderLeft={"1px solid"}
            cursor={"pointer"}
            data-testid={"customize-report"}
            display={"flex"}
            height={"40px"}
            justify={"center"}
            onClick={handleFilterReportsModal}
            px={0.5}
            py={0.5}
            width={"40px"}
          >
            <Icon icon={"sliders"} iconSize={"xs"} iconType={"fa-light"} />
          </Container>
        </Container>
      </ListItemsWrapper>
      <FilterReportModal
        isOpen={isFilterReportModalOpen}
        onClose={handleFilterReportsModal}
        typesOptions={typesOptions}
      />
      <Modal
        cancelButton={{
          onClick: handleIncompleteModal,
          text: t("group.findings.report.cert.incomplete.cancel"),
        }}
        confirmButton={{
          onClick: goToScope,
          text: t("group.findings.report.cert.incomplete.confirm"),
        }}
        description={t("group.findings.report.cert.incomplete.description")}
        modalRef={{
          ...modalProps,
          close: handleIncompleteModal,
          isOpen: isOpenIncompleteModal,
        }}
        size={"sm"}
        title={t("group.findings.report.cert.incomplete.title")}
      />
    </Container>
  );
};
