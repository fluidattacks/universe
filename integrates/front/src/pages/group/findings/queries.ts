import { graphql } from "gql";

graphql(`
  fragment GetFindingsCriteria on Query {
    criteriaConnection(after: $after, first: null) {
      total
      edges {
        cursor
        node {
          findingNumber
          remediationTime
          requirements
          en {
            description
            impact
            recommendation
            threat
            title
          }
          es {
            description
            impact
            recommendation
            threat
            title
          }

          score {
            base {
              attackComplexity
              attackVector
              availability
              confidentiality
              integrity
              privilegesRequired
              scope
              userInteraction
            }
            temporal {
              exploitCodeMaturity
              remediationLevel
              reportConfidence
            }
          }
          scoreV4 {
            base {
              attackComplexity
              attackRequirements
              attackVector
              availabilitySa
              availabilityVa
              confidentialitySc
              confidentialityVc
              integritySi
              integrityVi
              privilegesRequired
              userInteraction
            }
            threat {
              exploitMaturity
            }
          }
        }
      }
      pageInfo {
        endCursor
        hasNextPage
      }
    }
  }
`);

const ADD_FINDING_MUTATION = graphql(`
  mutation AddFindingMutation(
    $attackVectorDescription: String!
    $cvss4Vector: String!
    $cvssVector: String
    $description: String!
    $groupName: String!
    $minTimeToRemediate: Int
    $recommendation: String!
    $threat: String!
    $title: String!
    $unfulfilledRequirements: [String!]!
  ) {
    addFinding(
      attackVectorDescription: $attackVectorDescription
      cvss4Vector: $cvss4Vector
      cvssVector: $cvssVector
      description: $description
      groupName: $groupName
      minTimeToRemediate: $minTimeToRemediate
      recommendation: $recommendation
      threat: $threat
      title: $title
      unfulfilledRequirements: $unfulfilledRequirements
    ) {
      success
    }
  }
`);

const GET_FINDINGS = graphql(`
  query GetFindingsQuery(
    $canGetHacker: Boolean!
    $canGetRejectedVulnerabilities: Boolean!
    $canGetSubmittedVulnerabilities: Boolean!
    $canGetZRSummary: Boolean!
    $groupName: String!
    $root: String
    $technique: Technique
    $title: String
  ) {
    group(groupName: $groupName) {
      businessId
      businessName
      description
      hasEssential
      language
      managed
      name
      totalOpenPriority
      userRole
      findings(root: $root, technique: $technique, title: $title) {
        id
        age
        description
        hacker @include(if: $canGetHacker)
        isExploitable
        lastVulnerability
        maxOpenSeverityScore
        maxOpenSeverityScoreV4
        minTimeToRemediate
        openAge
        rejectedVulnerabilities @include(if: $canGetRejectedVulnerabilities)
        releaseDate
        status
        submittedVulnerabilities @include(if: $canGetSubmittedVulnerabilities)
        title
        totalOpenPriority
        verified
        treatmentSummary {
          accepted
          acceptedUndefined
          inProgress
          untreated
        }
        verificationSummary {
          onHold
          requested
          verified
        }
        vulnerabilitiesSummary {
          closed
          open
          openCritical
          openHigh
          openLow
          openMedium
        }
        vulnerabilitiesSummaryV4 {
          closed
          open
          openCritical
          openHigh
          openLow
          openMedium
        }
        zeroRiskSummary @include(if: $canGetZRSummary) {
          confirmed
          requested
        }
      }
    }
  }
`);

const GET_CRITERIA = graphql(`
  query GetFindingsCriteriaQuery($after: String!) {
    ...GetFindingsCriteria
  }
`);

const REQUEST_GROUP_REPORT = graphql(`
  query RequestGroupReportAtFindings(
    $age: Int
    $closingDate: DateTime
    $findingTitle: String
    $groupName: String!
    $lang: ReportLang
    $lastReport: Int
    $location: String
    $maxReleaseDate: DateTime
    $maxSeverity: Float
    $minReleaseDate: DateTime
    $minSeverity: Float
    $reportType: ReportType!
    $startClosingDate: DateTime
    $states: [VulnerabilityState!]
    $treatments: [VulnerabilityTreatment!]
    $verificationCode: String!
    $verifications: [VulnerabilityVerification!]
  ) {
    report(
      age: $age
      closingDate: $closingDate
      findingTitle: $findingTitle
      groupName: $groupName
      lang: $lang
      lastReport: $lastReport
      location: $location
      maxReleaseDate: $maxReleaseDate
      maxSeverity: $maxSeverity
      minReleaseDate: $minReleaseDate
      minSeverity: $minSeverity
      reportType: $reportType
      startClosingDate: $startClosingDate
      states: $states
      treatments: $treatments
      verificationCode: $verificationCode
      verifications: $verifications
    ) {
      success
    }
  }
`);

const GET_GROUP_FINDINGS_QUERIES = graphql(`
  query GetGroupFindingsQueries($after: String!) {
    ...GetFindingsCriteria
  }
`);

const UPDATE_ENROLLMENT_MUTATION = graphql(`
  mutation UpdateEnrollmentMutation($reason: TrialReason!) {
    updateEnrollment(reason: $reason) {
      success
    }
  }
`);

const UPDATE_STAKEHOLDER_MUTATION = graphql(`
  mutation UpdateStakeholderMutation($newPhone: PhoneInput!) {
    updateStakeholder(phone: $newPhone) {
      success
    }
  }
`);

export {
  ADD_FINDING_MUTATION,
  GET_CRITERIA,
  GET_GROUP_FINDINGS_QUERIES,
  GET_FINDINGS,
  REQUEST_GROUP_REPORT,
  UPDATE_ENROLLMENT_MUTATION,
  UPDATE_STAKEHOLDER_MUTATION,
};
