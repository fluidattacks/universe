import type { ApolloQueryResult } from "@apollo/client";
import type { IUseModal } from "@fluidattacks/design";

import type { IFindingSuggestionData } from "../types";
import type { GetFindingsQueryQuery } from "gql/graphql";

interface IAddVulnerabilityModalProps {
  groupName: string;
  modalRef: IUseModal;
  refetch: () =>
    | Promise<ApolloQueryResult<GetFindingsQueryQuery>>
    | Promise<void>;
  suggestions?: IFindingSuggestionData[];
  loading?: boolean;
}

export type { IAddVulnerabilityModalProps };
