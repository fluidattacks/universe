import { useQuery } from "@apollo/client";
import { Container, Loading, Tabs } from "@fluidattacks/design";
import { isNil } from "lodash";
import { useCallback, useState } from "react";
import * as React from "react";
import ReactAnsi from "react-ansi";
import { useTranslation } from "react-i18next";

import { columns, getStatus } from "./utils";

import { ExecutionDetails } from "../execution-details";
import { GET_FORCES_EXECUTION } from "../queries";
import type {
  IExecutionModalProps,
  IExploitResult,
  TExecutionTabs,
} from "../types";
import { Filters } from "components/filter";
import type {
  IFilter,
  IPermanentData,
  ISelectedOptions,
} from "components/filter/types";
import { Modal } from "components/modal";
import { Table } from "components/table";
import { useStoredState, useTable } from "hooks";
import { useAudit } from "hooks/use-audit";
import { setFiltersUtil } from "utils/set-filters";

const ExecutionModal = ({
  modalRef,
  execution,
}: IExecutionModalProps): JSX.Element => {
  const { t } = useTranslation();
  const { addAuditEvent } = useAudit();
  const [currentTab, setCurrentTab] = useState<TExecutionTabs>("summary");
  const tableRef = useTable("tblCompromisedToe", undefined, [
    "exploitability",
    "state",
    "kind",
    "who",
    "where",
  ]);
  const [filtersPermaset, setFiltersPermaset] = useStoredState<
    IPermanentData[]
  >(
    "tblCompromisedToeFilters",
    [
      { id: "exploitability", value: "" },
      { id: "state", value: "" },
      { id: "kind", value: "" },
      { id: "who", value: "" },
      { id: "where", value: "" },
    ],
    localStorage,
  );

  // Query operations
  const { loading, data } = useQuery(GET_FORCES_EXECUTION, {
    onCompleted: (): void => {
      addAuditEvent("ForcesExecution", execution.executionId);
    },
    variables: {
      executionId: execution.executionId,
      groupName: execution.groupName,
    },
  });

  // Data management
  const dataExecution = { ...data?.forcesExecution };
  const vulns =
    isNil(dataExecution.vulnerabilities) ||
    isNil(dataExecution.vulnerabilities.open) ||
    isNil(dataExecution.vulnerabilities.closed) ||
    isNil(dataExecution.vulnerabilities.accepted)
      ? []
      : dataExecution.vulnerabilities.open.concat(
          dataExecution.vulnerabilities.closed.concat(
            dataExecution.vulnerabilities.accepted,
          ),
        );
  const dataset = vulns.map(
    (elem): IExploitResult => ({
      ...elem,
      state: getStatus(elem.state ?? ""),
    }),
  );

  const [filters, setFilters] = useState<IFilter<IExploitResult>[]>([
    {
      id: "exploitability",
      key: "exploitability",
      label: t("group.forces.compromisedToe.exploitability"),
      type: "number",
    },
    {
      id: "state",
      key: "state",
      label: t("group.forces.compromisedToe.status"),
      selectOptions: (exploits): ISelectedOptions[] =>
        exploits.map((exploit): ISelectedOptions => ({ value: exploit.state })),
      type: "select",
    },
    {
      id: "kind",
      key: "kind",
      label: t("group.forces.compromisedToe.type"),
      selectOptions: (exploits): ISelectedOptions[] =>
        exploits.map(
          (exploit): ISelectedOptions => ({ value: exploit.kind ?? "" }),
        ),
      type: "select",
    },
    {
      id: "who",
      key: "who",
      label: t("group.forces.compromisedToe.specific"),
      selectOptions: (exploits): ISelectedOptions[] =>
        exploits.map(
          (exploit): ISelectedOptions => ({ value: exploit.who ?? "" }),
        ),
      type: "select",
    },
    {
      id: "where",
      key: "where",
      label: t("group.forces.compromisedToe.where"),
      selectOptions: (exploits): ISelectedOptions[] =>
        exploits.map(
          (exploit): ISelectedOptions => ({ value: exploit.where ?? "" }),
        ),
      type: "select",
    },
  ]);
  const filteredDataset = setFiltersUtil(dataset, filters);

  const toTab = useCallback((event: React.MouseEvent): void => {
    event.preventDefault();

    const target = event.target as HTMLButtonElement;

    setCurrentTab(target.id as TExecutionTabs);
  }, []);

  const tabContent: Record<TExecutionTabs, JSX.Element> = {
    log: <ReactAnsi autoScroll={true} log={dataExecution.log as string} />,
    summary: (
      <Table
        columns={columns}
        csvConfig={{ export: false }}
        data={filteredDataset}
        filters={
          <Filters
            dataset={dataset}
            filters={filters}
            permaset={[filtersPermaset, setFiltersPermaset]}
            setFilters={setFilters}
          />
        }
        options={{ columnToggle: true, enableSearchBar: true }}
        tableRef={tableRef}
      />
    ),
  };

  if (loading) {
    return (
      <Container
        alignItems={"center"}
        display={"flex"}
        height={"100%"}
        justify={"center"}
      >
        <Loading color={"red"} label={"Loading..."} size={"lg"} />
      </Container>
    );
  }

  return (
    <Modal
      modalRef={modalRef}
      size={"md"}
      title={t("group.forces.executionDetailsModal.title")}
    >
      {isNil(dataExecution) ? undefined : (
        <Container>
          <ExecutionDetails execution={execution} />
          <Container my={2}>
            <Tabs
              borders={false}
              box={true}
              items={[
                {
                  id: "summary",
                  isActive: currentTab === "summary",
                  label: t("group.forces.tabs.summary.text"),
                  link: "/summary",
                  onClick: toTab,
                  tooltip: t("group.forces.tabs.summary.tooltip"),
                },
                {
                  id: "log",
                  isActive: currentTab === "log",
                  label: t("group.forces.tabs.log.text"),
                  link: "/log",
                  onClick: toTab,
                  tooltip: t("group.forces.tabs.log.tooltip"),
                },
              ]}
            />
          </Container>
          <Container>{tabContent[currentTab]}</Container>
        </Container>
      )}
    </Modal>
  );
};

export { ExecutionModal };
