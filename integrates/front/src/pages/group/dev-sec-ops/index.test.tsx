import { fireEvent, screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import { Route, Routes } from "react-router-dom";

import {
  GET_FORCES_EXECUTION,
  GET_FORCES_EXECUTIONS,
  SEND_FORCES_EXECUTIONS_FILE,
} from "./queries";
import { formatExecutions, getVulnerabilitySummaries } from "./utils";

import { GroupDevSecOps } from ".";
import type {
  GetForcesExecutionQuery as GetForcesExecution,
  GetForcesExecutionsQuery as GetForcesExecutions,
  SendForcesExecutionsFileQuery as SendForcesExecutionsFile,
} from "gql/graphql";
import { VulnerabilityExploitState } from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage, IMessage } from "mocks/types";
import { msgError, msgSuccess } from "utils/notifications";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

describe("groupDevSecOps", (): void => {
  const memoryRouter = {
    initialEntries: ["/unittesting/devsecops"],
  };

  const errorMemoryRouter = {
    initialEntries: ["/unittesting1/devsecops"],
  };

  const mocks = [
    graphql
      .link(LINK)
      .query(
        GET_FORCES_EXECUTIONS,
        ({
          variables,
        }): StrictResponse<IErrorMessage | { data: GetForcesExecutions }> => {
          const { first, groupName, status, strictness } = variables;
          if (first === 150 && groupName === "unittesting") {
            return HttpResponse.json({
              data: {
                group: {
                  __typename: "Group",
                  forcesExecutionsConnection: {
                    edges: [
                      {
                        __typename: "ExecutionEdge" as const,
                        node: {
                          __typename: "ForcesExecution" as const,
                          date: "2020-02-19T19:31:18+00:00",
                          daysUntilItBreaks: null,
                          executionId: "33e5d863252940edbfb144ede56d56cf",
                          exitCode: "1",
                          gitRepo: "Repository",
                          gracePeriod: 0,
                          groupName: "unittesting",
                          kind: "dynamic",
                          severityThreshold: 0,
                          strictness: "strict",
                          vulnerabilities: {
                            numOfAcceptedVulnerabilities: 1,
                            numOfClosedVulnerabilities: 1,
                            numOfManagedVulnerabilities: 2,
                            numOfOpenVulnerabilities: 1,
                            numOfUnManagedVulnerabilities: 1,
                          },
                        },
                      },
                      {
                        __typename: "ExecutionEdge" as const,
                        node: {
                          __typename: "ForcesExecution" as const,
                          date: "2020-02-20T19:31:18+00:00",
                          daysUntilItBreaks: 120,
                          executionId: "33e5d863252940edbfb144ede56d56cg",
                          exitCode: "1",
                          gitRepo: "Repository",
                          gracePeriod: 0,
                          groupName: "unittesting",
                          kind: "dynamic",
                          severityThreshold: 0,
                          strictness: "strict",
                          vulnerabilities: {
                            numOfAcceptedVulnerabilities: 2,
                            numOfClosedVulnerabilities: 2,
                            numOfManagedVulnerabilities: 2,
                            numOfOpenVulnerabilities: 2,
                            numOfUnManagedVulnerabilities: 1,
                          },
                        },
                      },
                      {
                        __typename: "ExecutionEdge" as const,
                        node: {
                          __typename: "ForcesExecution" as const,
                          date: "2020-09-05T18:54:36+00:00",
                          daysUntilItBreaks: null,
                          executionId: "92968b3ba84645bf976c12b15f9464bb",
                          exitCode: "0",
                          gitRepo: "Repository",
                          gracePeriod: 0,
                          groupName: "unittesting",
                          kind: "other",
                          severityThreshold: 0,
                          strictness: "lax",
                          vulnerabilities: {
                            numOfAcceptedVulnerabilities: 0,
                            numOfClosedVulnerabilities: 0,
                            numOfManagedVulnerabilities: 0,
                            numOfOpenVulnerabilities: 0,
                            numOfUnManagedVulnerabilities: 0,
                          },
                        },
                      },
                      {
                        __typename: "ExecutionEdge" as const,
                        node: {
                          __typename: "ForcesExecution" as const,
                          date: "2020-09-04T18:54:36+00:00",
                          daysUntilItBreaks: null,
                          executionId: "08c1e735a73243f2ab1ee0757041f80e",
                          exitCode: "0",
                          gitRepo: "unable to retrieve",
                          gracePeriod: 0,
                          groupName: "unittesting",
                          kind: "other",
                          severityThreshold: 0,
                          strictness: "lax",
                          vulnerabilities: {
                            numOfAcceptedVulnerabilities: 0,
                            numOfClosedVulnerabilities: 8,
                            numOfManagedVulnerabilities: 8,
                            numOfOpenVulnerabilities: 32,
                            numOfUnManagedVulnerabilities: 32,
                          },
                        },
                      },
                    ].filter((edge): boolean => {
                      if (status !== undefined) {
                        return edge.node.vulnerabilities
                          .numOfOpenVulnerabilities === 0
                          ? status === "Secure"
                          : status === "Vulnerable";
                      }
                      if (strictness !== undefined) {
                        return edge.node.strictness === strictness;
                      }

                      return true;
                    }),
                    pageInfo: {
                      endCursor:
                        '["1586044800000", "EXEC#2020-02-19 07:31 PM"]',
                      hasNextPage: true,
                    },
                    total: 1,
                  },
                  name: "unittesting",
                },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("An error occurred")],
          });
        },
      ),
    graphql
      .link(LINK)
      .query(
        GET_FORCES_EXECUTION,
        ({
          variables,
        }): StrictResponse<IErrorMessage | { data: GetForcesExecution }> => {
          const { executionId, groupName } = variables;
          if (
            (executionId === "33e5d863252940edbfb144ede56d56cf" ||
              executionId === "33e5d863252940edbfb144ede56d56cg") &&
            groupName === "unittesting"
          ) {
            return HttpResponse.json({
              data: {
                forcesExecution: {
                  groupName: "unittesting",
                  log: "",
                  vulnerabilities: {
                    accepted: [
                      {
                        exploitability: "0.91",
                        kind: "DYNAMIC",
                        state: VulnerabilityExploitState.Open,
                        where: "HTTP/Implementation",
                        who: "https://test.com/test",
                      },
                    ],
                    closed: [
                      {
                        exploitability: "0.94",
                        kind: "DYNAMIC",
                        state: VulnerabilityExploitState.Accepted,
                        where: "HTTP/Implementation",
                        who: "https://test.com/test",
                      },
                      {
                        exploitability: "0.97",
                        kind: "DYNAMIC",
                        state: VulnerabilityExploitState.Closed,
                        where: "192.168.100.101",
                        who: "4444",
                      },
                    ],
                    numOfAcceptedVulnerabilities: 1,
                    numOfClosedVulnerabilities: 1,
                    numOfManagedVulnerabilities: 2,
                    numOfOpenVulnerabilities: 1,
                    numOfUnManagedVulnerabilities: 1,
                    open: [
                      {
                        exploitability: "0.91",
                        kind: "DYNAMIC",
                        state: VulnerabilityExploitState.Unknown,
                        where: "HTTP/Implementation",
                        who: "https://test.com/test",
                      },
                    ],
                  },
                },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("An error occurred")],
          });
        },
      ),
  ];

  it("getVulnerabilitySummaries", (): void => {
    expect.hasAssertions();

    expect(getVulnerabilitySummaries(null)).toBe(
      "0 Accepted, 0 Unmanaged, 0 Total",
    );
    expect(
      getVulnerabilitySummaries({
        numOfAcceptedVulnerabilities: 2,
        numOfClosedVulnerabilities: 0,
        numOfManagedVulnerabilities: 2,
        numOfOpenVulnerabilities: 2,
        numOfUnManagedVulnerabilities: 2,
      }),
    ).toBe("2 Accepted, 2 Unmanaged, 4 Total");
    expect(
      getVulnerabilitySummaries({
        numOfAcceptedVulnerabilities: 2,
        numOfClosedVulnerabilities: 0,
        numOfManagedVulnerabilities: null,
        numOfOpenVulnerabilities: 2,
        numOfUnManagedVulnerabilities: 2,
      }),
    ).toBe("0 Accepted, 2 Unmanaged, 2 Total");
  });

  it("formatExecutions", (): void => {
    expect.hasAssertions();
    expect(
      formatExecutions([
        {
          node: {
            date: "2020-02-19T19:31:18+00:00",
            daysUntilItBreaks: null,
            executionId: "33e5d863252940edbfb144ede56d56cf",
            exitCode: "0",
            gitRepo: "Repository",
            gracePeriod: 0,
            groupName: "unittesting",
            kind: "dynamic",
            severityThreshold: 0,
            strictness: "strict",
            vulnerabilities: {
              numOfAcceptedVulnerabilities: 1,
              numOfClosedVulnerabilities: 1,
              numOfManagedVulnerabilities: null,
              numOfOpenVulnerabilities: 1,
              numOfUnManagedVulnerabilities: null,
            },
          },
        },
      ]),
    ).toStrictEqual([
      {
        breakBuild: "No",
        date: "2020-02-19 07:31 PM",
        daysUntilItBreaks: null,
        executionId: "33e5d863252940edbfb144ede56d56cf",
        exitCode: "0",
        foundVulnerabilities: {
          accepted: 1,
          closed: 1,
          open: 1,
          total: 3,
        },
        gitRepo: "Repository",
        gracePeriod: 0,
        groupName: "unittesting",
        kind: "DYNAMIC",
        severityThreshold: 0,
        status: "Vulnerable",
        strictness: "Strict",
        vulnerabilities: {
          numOfAcceptedVulnerabilities: 1,
          numOfClosedVulnerabilities: 1,
          numOfManagedVulnerabilities: null,
          numOfOpenVulnerabilities: 1,
          numOfUnManagedVulnerabilities: null,
        },
      },
    ]);
  });

  it("should render an error in component", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route element={<GroupDevSecOps />} path={"/:groupName/devsecops"} />
      </Routes>,
      {
        memoryRouter: errorMemoryRouter,
        mocks,
      },
    );
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith("groupAlerts.errorTextsad");
    });
  });

  it("should render a component", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route element={<GroupDevSecOps />} path={"/:groupName/devsecops"} />
      </Routes>,
      {
        memoryRouter,
        mocks,
      },
    );
    await waitFor((): void => {
      expect(
        screen.queryByText("group.forces.tableAdvice"),
      ).toBeInTheDocument();
    });
    await waitFor((): void => {
      expect(
        screen.queryByText(
          "group.forces.policiesAlert.temporaryAcceptance.title",
        ),
      ).toBeInTheDocument();
    });
    await waitFor((): void => {
      expect(
        screen.queryByText("group.forces.policiesAlert.devSecOps.title"),
      ).toBeInTheDocument();
    });
  });

  it("should render forces table", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route element={<GroupDevSecOps />} path={"/:groupName/devsecops"} />
      </Routes>,
      {
        memoryRouter,
        mocks,
      },
    );
    await waitFor((): void => {
      expect(screen.queryByRole("table")).toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(
        screen.getByRole("cell", { name: "2020-02-19 07:31 PM" }),
      ).toBeInTheDocument();
    });

    expect(
      screen.getAllByRole("row")[0].textContent?.replace(/[^a-zA-Z ]/gu, ""),
    ).toStrictEqual(
      [
        "group.forces.date",
        "group.forces.status.title",
        "group.forces.exitCode.title",
        "group.forces.status.managedVulnerabilities",
        "group.forces.status.unManagedVulnerabilities",
        "group.forces.strictness.title",
        "group.forces.kind.title",
        "group.forces.gitRepo",
        "group.forces.identifier",
      ]
        .join("")
        .replace(/[^a-zA-Z ]/gu, ""),
    );
    expect(
      screen.getAllByRole("row")[1].textContent?.replace(/[^a-zA-Z0-9 ]/gu, ""),
    ).toStrictEqual(
      [
        "20200219 0731 PM",
        "Vulnerable",
        "Error",
        "2",
        "1",
        "Strict",
        "DYNAMIC",
        "Repository",
        "33e5d863",
      ]
        .join("")
        .replace(/[^a-zA-Z0-9 ]/gu, ""),
    );
  });

  it("should render forces modal", async (): Promise<void> => {
    expect.hasAssertions();

    const { container } = render(
      <Routes>
        <Route element={<GroupDevSecOps />} path={"/:groupName/devsecops"} />
      </Routes>,
      {
        memoryRouter,
        mocks,
      },
    );
    await waitFor((): void => {
      expect(screen.queryByRole("table")).toBeInTheDocument();
    });

    expect(
      screen.queryByText("group.forces.executionDetailsModal.title"),
    ).not.toBeInTheDocument();

    await waitFor((): void => {
      expect(
        screen.getByRole("cell", { name: "2020-02-19 07:31 PM" }),
      ).toBeInTheDocument();
    });

    await userEvent.click(
      screen.getByRole("cell", { name: "2020-02-19 07:31 PM" }),
    );
    await waitFor((): void => {
      expect(
        screen.queryByText("group.forces.executionDetailsModal.title"),
      ).toBeInTheDocument();
    });

    expect(
      screen.getByText("group.forces.severityThreshold.title"),
    ).toBeInTheDocument();
    expect(screen.getAllByText("0")[0]).toBeInTheDocument();
    expect(
      screen.getByText("group.forces.daysUntilItBreaks.title"),
    ).toBeInTheDocument();
    expect(screen.queryByText("-")).toBeInTheDocument();

    fireEvent.keyDown(container, { key: "Escape" });

    await waitFor((): void => {
      expect(
        screen.queryByText("group.forces.executionDetailsModal.title"),
      ).not.toBeInTheDocument();
    });

    expect(
      screen.getByRole("cell", { name: "2020-02-20 07:31 PM" }),
    ).toBeInTheDocument();

    await userEvent.click(
      screen.getByRole("cell", { name: "2020-02-20 07:31 PM" }),
    );
    await waitFor((): void => {
      expect(
        screen.queryByText("group.forces.executionDetailsModal.title"),
      ).toBeInTheDocument();
    });

    expect(
      screen.getByText("group.forces.daysUntilItBreaks.title"),
    ).toBeInTheDocument();
    expect(screen.queryByText("120")).toBeInTheDocument();
    expect(
      screen.getByText("group.forces.foundVulnerabilities.title"),
    ).toBeInTheDocument();
    expect(
      screen.queryByText("2 Accepted, 1 Unmanaged, 3 Total"),
    ).toBeInTheDocument();
  });

  it("should render export async button", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route element={<GroupDevSecOps />} path={"/:groupName/devsecops"} />
      </Routes>,
      {
        memoryRouter,
        mocks,
      },
    );
    await waitFor((): void => {
      expect(screen.queryByRole("table")).toBeInTheDocument();
    });

    expect(
      screen.getByRole("button", {
        name: "group.findings.buttons.report.text",
      }),
    ).toBeInTheDocument();
    expect(
      screen.queryByText("group.findings.exportCsv.text"),
    ).not.toBeInTheDocument();
  });

  it("should render filter button", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route element={<GroupDevSecOps />} path={"/:groupName/devsecops"} />
      </Routes>,
      {
        memoryRouter,
        mocks,
      },
    );
    await waitFor((): void => {
      expect(screen.queryByRole("table")).toBeInTheDocument();
    });

    expect(document.querySelector("#filterBtn")).toBeInTheDocument();
  });

  it("should render forces modal table and log", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route element={<GroupDevSecOps />} path={"/:groupName/devsecops"} />
      </Routes>,
      { memoryRouter, mocks },
    );
    await waitFor((): void => {
      expect(screen.queryByRole("table")).toBeInTheDocument();
    });

    expect(
      screen.queryByText("group.forces.executionDetailsModal.title"),
    ).not.toBeInTheDocument();

    await waitFor((): void => {
      expect(
        screen.getByRole("cell", { name: "2020-02-19 07:31 PM" }),
      ).toBeInTheDocument();
    });
    await userEvent.click(
      screen.getByRole("cell", { name: "2020-02-19 07:31 PM" }),
    );
    await waitFor((): void => {
      expect(
        screen.queryByText("group.forces.executionDetailsModal.title"),
      ).toBeInTheDocument();
    });

    expect(screen.getByText("Exploitability")).toBeInTheDocument();
    expect(screen.getByText("Status")).toBeInTheDocument();
    expect(screen.getByText("Type")).toBeInTheDocument();
    expect(screen.getByText("Specific")).toBeInTheDocument();
    expect(screen.getByText("Where")).toBeInTheDocument();
    expect(
      document.querySelector(".log-module_logBody__1lkPy"),
    ).not.toBeInTheDocument();

    await userEvent.click(
      screen.getByRole("link", { name: "group.forces.tabs.log.text" }),
    );

    expect(
      document.querySelector(".log-module_logBody__1lkPy"),
    ).toBeInTheDocument();
  });

  it("should render execution details", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route element={<GroupDevSecOps />} path={"/:groupName/devsecops"} />
      </Routes>,
      { memoryRouter, mocks },
    );
    await waitFor((): void => {
      expect(screen.queryByRole("table")).toBeInTheDocument();
    });

    expect(
      screen.queryByText("group.forces.executionDetailsModal.title"),
    ).not.toBeInTheDocument();

    await waitFor((): void => {
      expect(
        screen.getByRole("cell", { name: "2020-02-19 07:31 PM" }),
      ).toBeInTheDocument();
    });

    await userEvent.click(
      screen.getByRole("cell", { name: "2020-02-19 07:31 PM" }),
    );
    await waitFor((): void => {
      expect(
        screen.queryByText("group.forces.executionDetailsModal.title"),
      ).toBeInTheDocument();
    });

    expect(screen.getAllByText("group.forces.date")[1]).toBeInTheDocument();
    expect(
      screen.getAllByText("group.forces.status.title")[1],
    ).toBeInTheDocument();
    expect(
      screen.getAllByText("group.forces.strictness.title")[1],
    ).toBeInTheDocument();
    expect(
      screen.getByText("group.forces.severityThreshold.title"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("group.forces.gracePeriod.title"),
    ).toBeInTheDocument();
    expect(
      screen.getAllByText("group.forces.kind.title")[1],
    ).toBeInTheDocument();
    expect(screen.getAllByText("group.forces.gitRepo")[1]).toBeInTheDocument();
    expect(
      screen.getByText("group.forces.foundVulnerabilities.title"),
    ).toBeInTheDocument();
  });

  it("should render execution details values", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route element={<GroupDevSecOps />} path={"/:groupName/devsecops"} />
      </Routes>,
      { memoryRouter, mocks },
    );
    await waitFor((): void => {
      expect(screen.queryByRole("table")).toBeInTheDocument();
    });

    expect(
      screen.queryByText("group.forces.executionDetailsModal.title"),
    ).not.toBeInTheDocument();

    await waitFor((): void => {
      expect(
        screen.getByRole("cell", { name: "2020-02-19 07:31 PM" }),
      ).toBeInTheDocument();
    });

    await userEvent.click(
      screen.getByRole("cell", { name: "2020-02-19 07:31 PM" }),
    );
    await waitFor((): void => {
      expect(
        screen.queryByText("group.forces.executionDetailsModal.title"),
      ).toBeInTheDocument();
    });

    expect(screen.getAllByText("Vulnerable")[1]).toBeInTheDocument();
    expect(screen.getAllByText("Strict")[1]).toBeInTheDocument();
    expect(screen.getAllByText("0")[0]).toBeInTheDocument();
    expect(screen.getAllByText("0")[1]).toBeInTheDocument();
    expect(screen.getAllByText("DYNAMIC")[1]).toBeInTheDocument();
    expect(screen.getAllByText("Repository")[1]).toBeInTheDocument();
    expect(screen.getAllByText("2020-02-19 07:31 PM")[1]).toBeInTheDocument();
  });

  it("should handle request group report", async (): Promise<void> => {
    expect.hasAssertions();

    jest.spyOn(console, "warn").mockImplementation();

    const mockExport = [
      graphql
        .link(LINK)
        .query(
          GET_FORCES_EXECUTIONS,
          ({
            variables,
          }): StrictResponse<IErrorMessage | { data: GetForcesExecutions }> => {
            const { first, groupName } = variables;
            if (first === 150 && groupName === "unittesting") {
              return HttpResponse.json({
                data: {
                  group: {
                    __typename: "Group",
                    forcesExecutionsConnection: {
                      edges: [
                        {
                          __typename: "ExecutionEdge",
                          node: {
                            __typename: "ForcesExecution",
                            date: "2020-02-19T19:31:18+00:00",
                            daysUntilItBreaks: null,
                            executionId: "33e5d863252940edbfb144ede56d56cf",
                            exitCode: "1",
                            gitRepo: "Repository",
                            gracePeriod: 0,
                            groupName: "unittesting",
                            kind: "dynamic",
                            severityThreshold: 0,
                            strictness: "strict",
                            vulnerabilities: {
                              numOfAcceptedVulnerabilities: 1,
                              numOfClosedVulnerabilities: 1,
                              numOfManagedVulnerabilities: 2,
                              numOfOpenVulnerabilities: 1,
                              numOfUnManagedVulnerabilities: 1,
                            },
                          },
                        },
                        {
                          __typename: "ExecutionEdge",
                          node: {
                            __typename: "ForcesExecution",
                            date: "2020-09-05T18:54:36+00:00",
                            daysUntilItBreaks: null,
                            executionId: "92968b3ba84645bf976c12b15f9464bb",
                            exitCode: "0",
                            gitRepo: "Repository",
                            gracePeriod: 0,
                            groupName: "unittesting",
                            kind: "other",
                            severityThreshold: 0,
                            strictness: "lax",
                            vulnerabilities: {
                              numOfAcceptedVulnerabilities: 0,
                              numOfClosedVulnerabilities: 0,
                              numOfManagedVulnerabilities: 0,
                              numOfOpenVulnerabilities: 0,
                              numOfUnManagedVulnerabilities: 0,
                            },
                          },
                        },
                      ],
                      pageInfo: {
                        endCursor:
                          '["1586044800000", "EXEC#2020-02-19 07:31 PM"]',
                        hasNextPage: true,
                      },
                      total: 160,
                    },
                    name: "unittesting",
                  },
                },
              });
            }

            return HttpResponse.json({
              errors: [new GraphQLError("An error occurred")],
            });
          },
        ),
      graphql
        .link(LINK)
        .query(
          GET_FORCES_EXECUTION,
          ({
            variables,
          }): StrictResponse<IErrorMessage | { data: GetForcesExecution }> => {
            const { executionId, groupName } = variables;
            if (
              (executionId === "33e5d863252940edbfb144ede56d56cf" ||
                executionId === "33e5d863252940edbfb144ede56d56cg") &&
              groupName === "unittesting"
            ) {
              return HttpResponse.json({
                data: {
                  forcesExecution: {
                    groupName: "unittesting",
                    log: "execution_logs",
                    vulnerabilities: {
                      accepted: [],
                      closed: [
                        {
                          exploitability: "Functional",
                          kind: "DYNAMIC",
                          state: VulnerabilityExploitState.Accepted,
                          where: "HTTP/Implementation",
                          who: "https://test.com/test",
                        },
                      ],
                      numOfAcceptedVulnerabilities: 1,
                      numOfClosedVulnerabilities: 1,
                      numOfManagedVulnerabilities: 2,
                      numOfOpenVulnerabilities: 1,
                      numOfUnManagedVulnerabilities: 1,
                      open: [
                        {
                          exploitability: "Unproven",
                          kind: "DYNAMIC",
                          state: VulnerabilityExploitState.Unknown,
                          where: "HTTP/Implementation",
                          who: "https://test.com/test",
                        },
                      ],
                    },
                  },
                },
              });
            }

            return HttpResponse.json({
              errors: [new GraphQLError("An error occurred")],
            });
          },
        ),
      graphql
        .link(LINK)
        .query(
          SEND_FORCES_EXECUTIONS_FILE,
          ({
            variables,
          }): StrictResponse<
            IErrorMessage | { data: SendForcesExecutionsFile }
          > => {
            const { groupName } = variables;

            if (groupName === "unittesting") {
              return HttpResponse.json({
                data: {
                  sendExportedFile: {
                    success: true,
                  },
                },
              });
            }

            return HttpResponse.json({
              errors: [new GraphQLError("An error occurred sending file")],
            });
          },
        ),
    ];

    render(
      <Routes>
        <Route element={<GroupDevSecOps />} path={"/:groupName/devsecops"} />
      </Routes>,

      {
        memoryRouter,
        mocks: mockExport,
      },
    );
    await waitFor((): void => {
      expect(screen.queryByRole("table")).toBeInTheDocument();
    });

    expect(
      screen.getByRole("button", {
        name: "group.findings.buttons.report.text",
      }),
    ).toBeInTheDocument();

    await userEvent.click(
      screen.getByRole("button", {
        name: "group.findings.buttons.report.text",
      }),
    );

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "groupAlerts.reportRequested",
        "groupAlerts.titleSuccess",
      );
    });
  });

  it("should render error when requesting group report", async (): Promise<void> => {
    expect.hasAssertions();

    const mockExport = [
      graphql
        .link(LINK)
        .query(
          GET_FORCES_EXECUTIONS,
          ({
            variables,
          }): StrictResponse<IErrorMessage | { data: GetForcesExecutions }> => {
            const { first, groupName } = variables;
            if (first === 150 && groupName === "unittesting") {
              return HttpResponse.json({
                data: {
                  group: {
                    __typename: "Group",
                    forcesExecutionsConnection: {
                      edges: [
                        {
                          __typename: "ExecutionEdge",
                          node: {
                            __typename: "ForcesExecution",
                            date: "2020-02-19T19:31:18+00:00",
                            daysUntilItBreaks: null,
                            executionId: "33e5d863252940edbfb144ede56d56cf",
                            exitCode: "1",
                            gitRepo: "Repository",
                            gracePeriod: 0,
                            groupName: "unittesting",
                            kind: "dynamic",
                            severityThreshold: 0,
                            strictness: "strict",
                            vulnerabilities: {
                              numOfAcceptedVulnerabilities: 1,
                              numOfClosedVulnerabilities: 1,
                              numOfManagedVulnerabilities: 2,
                              numOfOpenVulnerabilities: 1,
                              numOfUnManagedVulnerabilities: 1,
                            },
                          },
                        },
                        {
                          __typename: "ExecutionEdge",
                          node: {
                            __typename: "ForcesExecution",
                            date: "2020-09-05T18:54:36+00:00",
                            daysUntilItBreaks: null,
                            executionId: "92968b3ba84645bf976c12b15f9464bb",
                            exitCode: "0",
                            gitRepo: "Repository",
                            gracePeriod: 0,
                            groupName: "unittesting",
                            kind: "other",
                            severityThreshold: 0,
                            strictness: "lax",
                            vulnerabilities: {
                              numOfAcceptedVulnerabilities: 0,
                              numOfClosedVulnerabilities: 0,
                              numOfManagedVulnerabilities: 0,
                              numOfOpenVulnerabilities: 0,
                              numOfUnManagedVulnerabilities: 0,
                            },
                          },
                        },
                      ],
                      pageInfo: {
                        endCursor:
                          '["1586044800000", "EXEC#2020-02-19 07:31 PM"]',
                        hasNextPage: true,
                      },
                      total: 160,
                    },
                    name: "unittesting",
                  },
                },
              });
            }

            return HttpResponse.json({
              errors: [new GraphQLError("An error occurred")],
            });
          },
        ),
      graphql
        .link(LINK)
        .query(
          GET_FORCES_EXECUTION,
          ({
            variables,
          }): StrictResponse<IErrorMessage | { data: GetForcesExecution }> => {
            const { executionId, groupName } = variables;
            if (
              (executionId === "33e5d863252940edbfb144ede56d56cf" ||
                executionId === "33e5d863252940edbfb144ede56d56cg") &&
              groupName === "unittesting"
            ) {
              return HttpResponse.json({
                data: {
                  forcesExecution: {
                    groupName: "unittesting",
                    log: "",
                    vulnerabilities: {
                      accepted: [],
                      closed: [],
                      numOfAcceptedVulnerabilities: 1,
                      numOfClosedVulnerabilities: 1,
                      numOfManagedVulnerabilities: 2,
                      numOfOpenVulnerabilities: 1,
                      numOfUnManagedVulnerabilities: 1,
                      open: [
                        {
                          exploitability: "Unproven",
                          kind: "DYNAMIC",
                          state: VulnerabilityExploitState.Unknown,
                          where: "HTTP/Implementation",
                          who: "https://test.com/test",
                        },
                      ],
                    },
                  },
                },
              });
            }

            return HttpResponse.json({
              errors: [new GraphQLError("An error occurred")],
            });
          },
        ),
      graphql
        .link(LINK)
        .query(
          SEND_FORCES_EXECUTIONS_FILE,
          ({
            variables,
          }): StrictResponse<
            Record<"errors", IMessage[]> | { data: SendForcesExecutionsFile }
          > => {
            const { groupName } = variables;

            if (groupName === "unittesting") {
              return HttpResponse.json({
                errors: [
                  new GraphQLError(
                    "Exception - The user already has a requested report for the same group",
                  ),
                  new GraphQLError(
                    "Exception - Stakeholder could not be verified",
                  ),
                  new GraphQLError("groupAlerts.errorTextsad"),
                ],
              });
            }

            return HttpResponse.json({
              data: {
                sendExportedFile: {
                  success: true,
                },
              },
            });
          },
        ),
    ];

    render(
      <Routes>
        <Route element={<GroupDevSecOps />} path={"/:groupName/devsecops"} />
      </Routes>,
      {
        memoryRouter,
        mocks: mockExport,
      },
    );
    await waitFor((): void => {
      expect(screen.queryByRole("table")).toBeInTheDocument();
    });

    expect(
      screen.getByRole("button", {
        name: "group.findings.buttons.report.text",
      }),
    ).toBeInTheDocument();

    await userEvent.click(
      screen.getByRole("button", {
        name: "group.findings.buttons.report.text",
      }),
    );

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledTimes(3);
    });
  });

  it("should render empty state", async (): Promise<void> => {
    expect.hasAssertions();

    const mockEmpty = [
      graphql
        .link(LINK)
        .query(
          GET_FORCES_EXECUTIONS,
          ({
            variables,
          }): StrictResponse<IErrorMessage | { data: GetForcesExecutions }> => {
            const { first, groupName } = variables;
            if (first === 150 && groupName === "unittesting") {
              return HttpResponse.json({
                data: {
                  group: {
                    __typename: "Group",
                    forcesExecutionsConnection: {
                      edges: [],
                      pageInfo: {
                        endCursor: "[]",
                        hasNextPage: false,
                      },
                      total: 0,
                    },
                    name: "unittesting",
                  },
                },
              });
            }

            return HttpResponse.json({
              errors: [new GraphQLError("An error occurred")],
            });
          },
        ),
    ];

    jest.spyOn(console, "warn").mockImplementation();
    render(
      <Routes>
        <Route element={<GroupDevSecOps />} path={"/:groupName/devsecops"} />
      </Routes>,
      {
        memoryRouter,
        mocks: mockEmpty,
      },
    );
    await waitFor((): void => {
      expect(screen.queryByRole("table")).not.toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(
        screen.queryByText("group.forces.emptyState.title"),
      ).toBeInTheDocument();
    });

    expect(
      screen.queryByText("group.forces.emptyState.description"),
    ).toBeInTheDocument();
    expect(
      screen.queryByText("group.forces.emptyState.helpButtonText"),
    ).toBeInTheDocument();
    expect(
      screen.queryByText("group.forces.emptyState.installButtonText"),
    ).toBeInTheDocument();

    const title1 = await screen.findByText(
      "group.forces.emptyState.accordion1.title",
    );

    expect(title1).toBeInTheDocument();

    await userEvent.click(title1);
    const descriptionText1 = await screen.findByText(
      "group.forces.emptyState.accordion1.description",
    );

    expect(descriptionText1).toBeInTheDocument();

    const title2 = await screen.findByText(
      "group.forces.emptyState.accordion2.title",
    );

    expect(title2).toBeInTheDocument();

    await userEvent.click(title2);

    const descriptionText2 = await screen.findByText(
      "group.forces.emptyState.accordion2.description",
    );

    expect(descriptionText2).toBeInTheDocument();

    const title3 = await screen.findByText(
      "group.forces.emptyState.accordion3.title",
    );

    expect(title3).toBeInTheDocument();

    await userEvent.click(title3);

    const descriptionText3 = await screen.findByText(
      "group.forces.emptyState.accordion3.description",
    );

    expect(descriptionText3).toBeInTheDocument();
  });
});
