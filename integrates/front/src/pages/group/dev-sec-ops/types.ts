import type {
  GetForcesExecutionQuery as IForcesExecution,
  GetForcesExecutionsQuery as IForcesExecutions,
} from "gql/graphql";
import type { IUseModal } from "hooks/use-modal";

type TForcesExecutionVulnerabilities = Exclude<
  Exclude<IForcesExecution, null>["forcesExecution"],
  null
>["vulnerabilities"];
type TForcesExploit = Exclude<
  Exclude<TForcesExecutionVulnerabilities, null>["open"],
  null
>[0];
interface IExploitResult extends Omit<TForcesExploit, "state"> {
  state: string;
}

interface IFoundVulnerabilities {
  accepted: number;
  closed: number;
  open: number;
  total: number;
}
type TForcesExecutionsEdges =
  IForcesExecutions["group"]["forcesExecutionsConnection"]["edges"];
type TForcesExecutions = Exclude<TForcesExecutionsEdges[0], null>["node"];

interface IExecution extends TForcesExecutions {
  foundVulnerabilities: IFoundVulnerabilities;
  breakBuild: string;
  status: string;
}

interface IExecutionDetailsProps {
  execution: IExecution;
}

interface IExecutionModalProps extends IExecutionDetailsProps {
  modalRef: IUseModal;
}

type TExecutionTabs = "log" | "summary";

export type {
  IFoundVulnerabilities,
  IExecution,
  IExploitResult,
  TForcesExecutionsEdges,
  TExecutionTabs,
  IExecutionModalProps,
  IExecutionDetailsProps,
};
