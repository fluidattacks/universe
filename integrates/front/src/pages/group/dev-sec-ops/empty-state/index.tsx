import {
  Accordion,
  Container,
  EmptyState as Empty,
} from "@fluidattacks/design";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { openUrl } from "utils/resource-helpers";

const EmptyState: React.FC = (): JSX.Element => {
  const { t } = useTranslation();

  const helpLink = "mailto:help@fluidattacks.com";
  const installLink =
    "https://help.fluidattacks.com/portal/en/kb/articles/install-the-ci-agent-to-break-the-build";

  return (
    <React.Fragment>
      <Empty
        cancelButton={{
          onClick: (): void => {
            openUrl(helpLink);
          },
          text: t("group.forces.emptyState.helpButtonText"),
        }}
        confirmButton={{
          onClick: (): void => {
            openUrl(installLink);
          },
          text: t("group.forces.emptyState.installButtonText"),
        }}
        description={t("group.forces.emptyState.description")}
        imageSrc={"integrates/empty/devSecOpsEmptyState"}
        title={t("group.forces.emptyState.title")}
      />
      <Container center={true} maxWidth={"900px"}>
        <Container pb={4} pl={4} pr={4} pt={0}>
          <Accordion
            items={[
              {
                description: t(
                  "group.forces.emptyState.accordion1.description",
                ),
                title: t("group.forces.emptyState.accordion1.title"),
              },
              {
                description: t(
                  "group.forces.emptyState.accordion2.description",
                ),
                title: t("group.forces.emptyState.accordion2.title"),
              },
              {
                description: t(
                  "group.forces.emptyState.accordion3.description",
                ),
                title: t("group.forces.emptyState.accordion3.title"),
              },
            ]}
          />
        </Container>
      </Container>
    </React.Fragment>
  );
};

export { EmptyState };
