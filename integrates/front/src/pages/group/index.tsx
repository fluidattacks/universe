import { useQuery } from "@apollo/client";
import { useAbility } from "@casl/react";
import { Container } from "@fluidattacks/design";
import { Fragment } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { Navigate, Outlet, Route, Routes, useParams } from "react-router-dom";

import { GroupAnalytics } from "./analytics";
import { GroupAuthors } from "./authors";
import { GroupDevSecOps } from "./dev-sec-ops";
import { GroupEvents } from "./events";
import { GroupFindings } from "./findings";
import { useGroupUrl } from "./hooks";
import { GroupTemplate } from "./index.template";
import { GroupMembers } from "./members";
import { GET_GROUP_DATA } from "./queries";
import { GroupScope } from "./scope";
import { AllPackagesTab } from "./supply-chain/all-packages-tab";
import { DockerImagesTab } from "./supply-chain/docker-images-tab";
import { RootsTab } from "./supply-chain/roots-tab";
import { GroupSurface } from "./surface";
import { GroupToeInputs } from "./surface/inputs";
import { GroupToeLanguages } from "./surface/languages";
import { GroupToeLines } from "./surface/lines";
import { GroupToePorts } from "./surface/ports";

import { authzPermissionsContext } from "context/authz/config";
import { FreeTrialEnd } from "features/free-trial-end";
import { ManagedType } from "gql/graphql";
import { useTrial } from "hooks";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";

const Group: React.FC = (): JSX.Element => {
  const { url: groupUrl } = useGroupUrl();
  const { groupName, organizationName } = useParams() as {
    groupName: string;
    organizationName: string;
  };
  const trial = useTrial();
  const { t } = useTranslation();
  const managed = ManagedType;

  const permissions = useAbility(authzPermissionsContext);

  const canGetToeLines = permissions.can(
    "integrates_api_resolvers_group_toe_lines_connection_resolve",
  );
  const canGetToeInputs = permissions.can(
    "integrates_api_resolvers_group_toe_inputs_resolve",
  );
  const canGetToePorts = permissions.can(
    "integrates_api_resolvers_group_toe_ports_resolve",
  );

  const { data: groupData } = useQuery(GET_GROUP_DATA, {
    fetchPolicy: "no-cache",
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.warning("An error occurred loading group managed", error);
      });
    },
    variables: { groupName, organizationName },
  });

  return (
    <GroupTemplate>
      <Fragment>
        <Routes>
          <Route element={<GroupAnalytics />} path={"/analytics"} />
          <Route element={<GroupAuthors />} path={"/authors"} />
          <Route element={<GroupDevSecOps />} path={"/devsecops"} />
          <Route element={<GroupEvents />} path={"/events"} />
          <Route element={<GroupFindings />} path={"/vulns"} />
          <Route element={<GroupMembers />} path={"/members"} />
          <Route element={<GroupScope />} path={"/scope"} />
          <Route element={<GroupSurface />} path={"/surface/*"}>
            <Route
              element={<GroupToeLines isInternal={false} />}
              path={`lines`}
            />
            <Route
              element={<GroupToeInputs isInternal={false} />}
              path={`inputs`}
            />
            <Route
              element={
                <Container px={1.25} py={1.25}>
                  <Outlet />
                </Container>
              }
              path={"packages"}
            >
              <Route
                element={<DockerImagesTab groupName={groupName} />}
                path={"docker-images"}
              />
              <Route
                element={<RootsTab groupName={groupName} />}
                path={"roots"}
              />
              <Route
                element={<AllPackagesTab groupName={groupName} />}
                path={"all-packages"}
              />
              <Route
                element={
                  <Navigate to={`${groupUrl}/surface/packages/all-packages`} />
                }
                path={"*"}
              />
            </Route>
            <Route
              element={<GroupToePorts isInternal={false} />}
              path={`ports`}
            />
            <Route element={<GroupToeLanguages />} path={`languages`} />
            <Route
              element={<Navigate to={`${groupUrl}/surface/lines`} />}
              path={"*"}
            />
          </Route>
          <Route element={<GroupSurface />} path={"/internal/surface/*"}>
            {canGetToeLines ? (
              <Route
                element={<GroupToeLines isInternal={true} />}
                path={"lines"}
              />
            ) : null}
            {canGetToeInputs ? (
              <Route
                element={<GroupToeInputs isInternal={true} />}
                path={"inputs"}
              />
            ) : null}
            <Route
              element={
                <Container px={1.25} py={1.25}>
                  <Outlet />
                </Container>
              }
              path={"packages"}
            >
              <Route
                element={<DockerImagesTab groupName={groupName} />}
                path={"docker-images"}
              />
              <Route
                element={<RootsTab groupName={groupName} />}
                path={"roots"}
              />
              <Route
                element={<AllPackagesTab groupName={groupName} />}
                path={"all-packages"}
              />
              <Route
                element={
                  <Navigate
                    to={`${groupUrl}/internal/surface/packages/all-packages`}
                  />
                }
                path={"*"}
              />
            </Route>
            {canGetToePorts ? (
              <Route
                element={<GroupToePorts isInternal={true} />}
                path={"ports"}
              />
            ) : null}
            {permissions.rules.length > 0 ? (
              <Route element={<GroupToeLanguages />} path={"languages"} />
            ) : undefined}
            <Route element={<Navigate to={`${groupUrl}/vulns`} />} path={"*"} />
          </Route>
          <Route
            element={
              <Navigate
                replace={true}
                to={`${groupUrl}/members`.replace("//", "/")}
              />
            }
            path={"/stakeholders"}
          />
          <Route
            element={
              <Navigate
                replace={true}
                to={`${groupUrl}/vulns`.replace("//", "/")}
              />
            }
            path={"*"}
          />
        </Routes>
        <FreeTrialEnd
          isOpen={
            (trial?.trial.completed ?? false)
              ? groupData?.group.managed === managed.UnderReview
              : false
          }
        />
      </Fragment>
    </GroupTemplate>
  );
};

export { Group };
