import { StrictMode } from "react";

import { AddFileModal } from "./modal";
import type { IAddFilesModalProps } from "./types";

const AddFilesModal = ({
  isAddingFileToDB,
  isOpen,
  isUploading,
  progressPercentage,
  onClose,
  onSubmit,
  values,
}: Readonly<IAddFilesModalProps>): JSX.Element => (
  <StrictMode>
    <AddFileModal
      isAddingFileToDB={isAddingFileToDB}
      isOpen={isOpen}
      isUploading={isUploading}
      onClose={onClose}
      onSubmit={onSubmit}
      progressPercentage={progressPercentage}
      values={values}
    />
  </StrictMode>
);

export type { IAddFilesModalProps };
export { AddFilesModal };
