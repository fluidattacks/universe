import {
  Alert,
  Container,
  Form,
  InnerForm,
  InputFile,
  Modal,
  ProgressBar,
  TextArea,
  useModal,
} from "@fluidattacks/design";
import { type FC, Fragment } from "react";
import { useTranslation } from "react-i18next";

import type { IAddFilesModalProps } from "../types";
import { validationSchema } from "../validations";

const AddFileModal: FC<IAddFilesModalProps> = ({
  isAddingFileToDB,
  isOpen,
  isUploading,
  onClose,
  onSubmit,
  progressPercentage,
  values,
}): JSX.Element => {
  const { t } = useTranslation();
  const modalProps = useModal("add-file");

  return (
    <Modal
      modalRef={{ ...modalProps, close: onClose, isOpen }}
      size={"sm"}
      title={t("searchFindings.tabResources.modalFileTitle")}
    >
      <Form
        cancelButton={{ onClick: onClose }}
        confirmButton={{ disabled: isUploading }}
        defaultValues={values}
        onSubmit={onSubmit}
        yupSchema={validationSchema}
      >
        <InnerForm>
          {({
            formState: { errors },
            getFieldState,
            register,
            setValue,
            watch,
          }): JSX.Element => {
            const { invalid, isTouched } = getFieldState("description");

            return (
              <Fragment>
                <InputFile
                  error={errors.file?.message?.toString()}
                  id={"file"}
                  name={"file"}
                  register={register}
                  required={true}
                  setValue={setValue}
                  watch={watch}
                />
                <TextArea
                  error={errors.description?.message?.toString()}
                  isTouched={isTouched}
                  isValid={!invalid}
                  label={t("searchFindings.tabResources.description")}
                  maxLength={200}
                  name={"description"}
                  register={register}
                  required={true}
                  watch={watch}
                />
                {isUploading ? (
                  <Container display={"flex"} flexDirection={"column"} gap={1}>
                    <Alert variant={"info"}>
                      {t("searchFindings.tabResources.uploadingProgress")}
                    </Alert>
                    <ProgressBar
                      minWidth={246}
                      percentage={progressPercentage}
                      showPercentage={true}
                    />
                    {isAddingFileToDB ? (
                      <Alert variant={"info"}>
                        {t("groupAlerts.addingFileToDb")}
                      </Alert>
                    ) : undefined}
                  </Container>
                ) : undefined}
              </Fragment>
            );
          }}
        </InnerForm>
      </Form>
    </Modal>
  );
};

export { AddFileModal };
