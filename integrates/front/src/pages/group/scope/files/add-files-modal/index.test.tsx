import { fireEvent, screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";

import type { IAddFileValues } from "./types";

import { render } from "mocks";
import { AddFilesModal } from "pages/group/scope/files/add-files-modal";

const Wrapper = ({
  isUploading = false,
  handleClose = jest.fn(),
  handleSubmit = jest.fn(),
}: Readonly<{
  isUploading?: boolean;
  handleClose?: VoidFunction;
  handleSubmit?: (values: IAddFileValues) => void;
}>): JSX.Element => {
  const values: IAddFileValues = {
    description: "",
    file: undefined as unknown as FileList,
  };

  return (
    <AddFilesModal
      isAddingFileToDB={isUploading}
      isOpen={true}
      isUploading={isUploading}
      onClose={handleClose}
      onSubmit={handleSubmit}
      progressPercentage={100}
      values={values}
    />
  );
};

describe("add Files modal", (): void => {
  it("should render", (): void => {
    expect.hasAssertions();

    render(<Wrapper />);

    expect(
      screen.queryByText("searchFindings.tabResources.modalFileTitle"),
    ).toBeInTheDocument();
    expect(
      screen.queryByText("searchFindings.tabResources.uploadingProgress"),
    ).not.toBeInTheDocument();
  });

  it("should render uploadbar", (): void => {
    expect.hasAssertions();

    render(<Wrapper isUploading={true} />);

    expect(
      screen.queryByText("searchFindings.tabResources.modalFileTitle"),
    ).toBeInTheDocument();
    expect(
      screen.queryByText("searchFindings.tabResources.uploadingProgress"),
    ).toBeInTheDocument();
    expect(screen.queryByText("100%")).toBeInTheDocument();
    expect(
      screen.queryByText("groupAlerts.addingFileToDb"),
    ).toBeInTheDocument();
  });

  it("should close on cancel", async (): Promise<void> => {
    expect.hasAssertions();

    const handleClose: jest.Mock = jest.fn();
    render(<Wrapper handleClose={handleClose} />);
    await userEvent.click(screen.getByText("Cancel"));
    await waitFor((): void => {
      expect(handleClose).toHaveBeenCalledTimes(1);
    });
  });

  it("should require fields", async (): Promise<void> => {
    expect.hasAssertions();

    const handleClose: jest.Mock = jest.fn();
    const file = new File(["okada-test.txt"], "okada-test.txt", {
      type: "text/plain",
    });

    render(<Wrapper handleClose={handleClose} />);
    const confirmBtn = "Confirm";

    await userEvent.type(screen.getByRole("textbox"), "test description");

    expect(screen.getByText(confirmBtn)).not.toBeDisabled();

    await userEvent.click(screen.getByText(confirmBtn));

    expect(screen.getByText("Required field")).toBeInTheDocument();

    await userEvent.clear(screen.getByRole("textbox"));

    await userEvent.upload(screen.getByTestId("file"), file);

    expect(screen.getByText(confirmBtn)).not.toBeDisabled();

    await userEvent.click(screen.getByText(confirmBtn));

    expect(screen.getByText("Required field")).toBeInTheDocument();
  });

  it("should submit a file", async (): Promise<void> => {
    expect.hasAssertions();

    const handleSubmit: jest.Mock = jest.fn();

    const file = new File(["okada-test.zip"], "okada-test.zip", {
      type: "application/zip",
    });

    render(<Wrapper handleSubmit={handleSubmit} />);
    const confirmBtn = "Confirm";

    fireEvent.change(screen.getByTestId("file"), { target: { files: [file] } });
    await userEvent.type(
      screen.getByRole("textbox", { name: "description" }),
      "test description",
    );

    await userEvent.click(screen.getByText(confirmBtn));

    await waitFor((): void => {
      expect(handleSubmit).toHaveBeenCalledTimes(1);
    });

    expect(screen.getByRole("textbox", { name: "description" })).toHaveValue(
      "test description",
    );
    expect(screen.getByText("okada-test.zip")).toBeInTheDocument();
  });
});
