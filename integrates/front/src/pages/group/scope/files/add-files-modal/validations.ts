import { mixed, object, string } from "yup";

import { translate } from "utils/translations/translate";

const MAX_FILE_SIZE = 5000;

const validationSchema = object().shape({
  description: string()
    .required(translate.t("validations.required"))
    .matches(/^(?!=).+/u, translate.t("validations.invalidValueInField"))
    .isValidTextBeginning()
    .isValidTextField(),
  file: mixed()
    .required(translate.t("validations.required"))
    .isValidFileName()
    .isValidFileSize(undefined, MAX_FILE_SIZE),
});

export { validationSchema };
