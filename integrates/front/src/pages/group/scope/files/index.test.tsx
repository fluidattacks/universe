import { PureAbility } from "@casl/ability";
import { fireEvent, screen, waitFor, within } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import isEqual from "lodash/isEqual";
import { newServer } from "mock-xmlhttprequest";
import { HttpResponse, type StrictResponse, graphql } from "msw";

import {
  ADD_FILES_TO_DB_MUTATION,
  DOWNLOAD_FILE_MUTATION,
  GET_FILES,
  GET_GIT_ROOTS_QUERIES,
  REMOVE_FILE_MUTATION,
  SIGN_POST_URL_MUTATION,
} from "../queries";
import { authzPermissionsContext } from "context/authz/config";
import type {
  AddFilesToDbMutationMutation as AddFilesToDb,
  DownloadFileMutationMutation as DownloadFile,
  GetFilesQueryQuery as GetFiles,
  GetGitRootsViewQueriesQuery,
  RemoveFileMutationMutation as RemoveFile,
  SignPostUrlMutationMutation as SignPostUrl,
} from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage, IMessage } from "mocks/types";
import type { IFilesProps } from "pages/group/scope/files";
import { Files } from "pages/group/scope/files";
import { msgError, msgSuccess } from "utils/notifications";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

describe("files", (): void => {
  const graphqlMocked = graphql.link(LINK);
  const btnConfirm = "Confirm";

  const NUMBER_OF_ROWS = 3;
  const mockProps: IFilesProps = {
    groupName: "TEST",
    organizationName: "testorg",
  };
  const initialGitRootsQueriesMockGlobal = graphqlMocked.query(
    GET_GIT_ROOTS_QUERIES,
    (): StrictResponse<{ data: GetGitRootsViewQueriesQuery }> => {
      return HttpResponse.json({
        data: {
          __typename: "Query",
          ...{
            group: {
              __typename: "Group",
              gitEnvironmentUrls: [],
              name: "unittesting",
            },
          },
          ...{
            organizationId: {
              __typename: "Organization",
              awsExternalId: "6e52c11c-abf7-4ca3-b7d0-635e394f41c1",
              name: "testorg",
            },
          },
        },
      });
    },
  );
  const mocksFiles = [
    graphqlMocked.query(GET_FILES, (): StrictResponse<{ data: GetFiles }> => {
      return HttpResponse.json({
        data: {
          resources: {
            files: [
              {
                description: "Test",
                fileName: "test.zip",
                uploadDate: "2019-03-01 15:21",
                uploader: "unittest@fluidattacks.com",
              },
              {
                description: "shell",
                fileName: "shell.exe",
                uploadDate: "2019-04-24 14:56",
                uploader: "unittest@fluidattacks.com",
              },
            ],
          },
        },
      });
    }),
    initialGitRootsQueriesMockGlobal,
  ];

  it("should add a file", async (): Promise<void> => {
    expect.hasAssertions();

    const file = new File(["image.png"], "image.png", { type: "image/png" });
    const mocksMutation = [
      graphqlMocked.mutation(
        ADD_FILES_TO_DB_MUTATION,
        ({
          variables,
        }): StrictResponse<IErrorMessage | { data: AddFilesToDb }> => {
          const { filesDataInput, groupName } = variables;
          if (
            isEqual(filesDataInput, [
              {
                description: "Test description",
                fileName: "image.png",
              },
            ]) &&
            groupName === "TEST"
          ) {
            return HttpResponse.json({
              data: {
                addFilesToDb: { success: true },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error updating url roots data")],
          });
        },
      ),
      graphqlMocked.mutation(
        SIGN_POST_URL_MUTATION,
        ({
          variables,
        }): StrictResponse<IErrorMessage | { data: SignPostUrl }> => {
          const { filesDataInput, groupName } = variables;
          if (
            isEqual(filesDataInput, [
              {
                description: "Test description",
                fileName: "image.png",
              },
            ]) &&
            groupName === "TEST"
          ) {
            return HttpResponse.json({
              data: {
                signPostUrl: {
                  success: true,
                  url: {
                    fields: {
                      algorithm: "",
                      credential: "",
                      date: "",
                      key: "",
                      policy: "",
                      securitytoken: "",
                      signature: "",
                    },
                    url: "https://mocked.test",
                  },
                },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error signing urls")],
          });
        },
      ),
      graphqlMocked.query(
        GET_FILES,
        (): StrictResponse<{ data: GetFiles }> => {
          return HttpResponse.json({
            data: {
              resources: {
                files: [
                  {
                    description: "Test",
                    fileName: "test.zip",
                    uploadDate: "2019-03-01 15:21",
                    uploader: "unittest@fluidattacks.com",
                  },
                  {
                    description: "shell",
                    fileName: "shell.exe",
                    uploadDate: "2019-04-24 14:56",
                    uploader: "unittest@fluidattacks.com",
                  },
                  {
                    description: "Test description",
                    fileName: "image.png",
                    uploadDate: "2019-03-01 15:21",
                    uploader: "unittest@fluidattacks.com",
                  },
                ],
              },
            },
          });
        },
        { once: true },
      ),
    ];
    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_sign_post_url_mutate" },
    ]);
    const server = newServer({
      post: [
        "https://mocked.test",
        {
          status: 200,
        },
      ],
    });

    server.install();

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Files
          groupName={mockProps.groupName}
          organizationName={mockProps.organizationName}
        />
      </authzPermissionsContext.Provider>,
      { mocks: [...mocksFiles, ...mocksMutation] },
    );
    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(NUMBER_OF_ROWS);
    });

    expect(
      screen.queryByText("searchFindings.tabResources.addRepository"),
    ).toBeInTheDocument();

    await userEvent.click(
      screen.getByText("searchFindings.tabResources.addRepository"),
    );

    await waitFor((): void => {
      expect(screen.queryByTestId("file")).toBeInTheDocument();
    });

    await userEvent.upload(screen.getByTestId("file"), file);

    await userEvent.type(
      screen.getByRole("textbox", { name: "description" }),
      "Test description",
    );
    await waitFor((): void => {
      expect(screen.getByText(btnConfirm)).not.toBeDisabled();
    });
    await userEvent.click(screen.getByText(btnConfirm));
    await waitFor(
      (): void => {
        expect(screen.queryAllByRole("row")).toHaveLength(3);
      },
      { timeout: 2000 },
    );
    server.remove();
    jest.clearAllMocks();
  });

  it("should sort files", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Files
        groupName={mockProps.groupName}
        organizationName={mockProps.organizationName}
      />,
      { mocks: mocksFiles },
    );
    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(NUMBER_OF_ROWS);
    });

    expect(screen.queryAllByRole("row")[1].textContent).toBe(
      "test.zipTest2019-03-01",
    );

    await userEvent.click(
      within(
        screen.getByRole("columnheader", {
          name: "searchFindings.filesTable.file",
        }),
      ).getAllByTestId("sort-icon")[0],
    );
    await waitFor((): void => {
      expect(screen.queryAllByRole("row")[1].textContent).toBe(
        "shell.exeshell2019-04-24",
      );
    });

    jest.clearAllMocks();
  });

  it("should remove a file", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const mocksMutation = [
      graphqlMocked.mutation(
        REMOVE_FILE_MUTATION,
        ({
          variables,
        }): StrictResponse<IErrorMessage | { data: RemoveFile }> => {
          const { filesDataInput, groupName } = variables;
          if (
            isEqual(filesDataInput, {
              fileName: "test.zip",
            }) &&
            groupName === "TEST"
          ) {
            return HttpResponse.json({
              data: {
                removeFiles: { success: true },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error removing files")],
          });
        },
      ),
    ];
    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_remove_files_mutate" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Files
          groupName={mockProps.groupName}
          organizationName={mockProps.organizationName}
        />
      </authzPermissionsContext.Provider>,
      { mocks: [...mocksFiles, ...mocksMutation] },
    );
    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(NUMBER_OF_ROWS);
    });

    expect(screen.queryByText("test.zip")).toBeInTheDocument();

    await userEvent.click(screen.getByText("test.zip"));
    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.tabResources.modalOptionsTitle"),
      ).toBeInTheDocument();
    });
    await userEvent.click(
      screen.getByText("searchFindings.tabResources.removeRepository"),
    );
    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.tabResources.files.confirm.title"),
      ).toBeInTheDocument();
    });
    await userEvent.click(screen.getByText("Confirm"));
    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledTimes(1);
    });
  });

  it("should handle error when remove a file", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const mocksMutation = [
      graphqlMocked.mutation(
        REMOVE_FILE_MUTATION,
        (): StrictResponse<IErrorMessage> => {
          return HttpResponse.json({
            errors: [new GraphQLError("Error removing files")],
          });
        },
      ),
    ];
    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_remove_files_mutate" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Files
          groupName={mockProps.groupName}
          organizationName={mockProps.organizationName}
        />
      </authzPermissionsContext.Provider>,
      { mocks: [...mocksFiles, ...mocksMutation] },
    );
    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(NUMBER_OF_ROWS);
    });

    expect(screen.queryByText("test.zip")).toBeInTheDocument();

    await userEvent.click(screen.getByText("test.zip"));
    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.tabResources.modalOptionsTitle"),
      ).toBeInTheDocument();
    });
    await userEvent.click(
      screen.getByText("searchFindings.tabResources.removeRepository"),
    );
    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.tabResources.files.confirm.title"),
      ).toBeInTheDocument();
    });
    await userEvent.click(screen.getByText("Confirm"));
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith("groupAlerts.errorTextsad");
    });
  });

  it("should download a file", async (): Promise<void> => {
    expect.hasAssertions();

    const open: jest.Mock = jest.fn();
    open.mockReturnValue({ opener: "" });
    window.open = open; // eslint-disable-line functional/immutable-data
    const mocksMutation = [
      graphqlMocked.mutation(
        DOWNLOAD_FILE_MUTATION,
        ({
          variables,
        }): StrictResponse<IErrorMessage | { data: DownloadFile }> => {
          const { filesData, groupName } = variables;
          if (filesData === "test.zip" && groupName === "TEST") {
            return HttpResponse.json({
              data: {
                downloadFile: { success: true, url: "https://test.com/file" },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error downloading files")],
          });
        },
      ),
    ];
    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_remove_files_mutate" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Files
          groupName={mockProps.groupName}
          organizationName={mockProps.organizationName}
        />
      </authzPermissionsContext.Provider>,
      { mocks: [...mocksFiles, ...mocksMutation] },
    );
    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(NUMBER_OF_ROWS);
    });

    expect(screen.queryByText("test.zip")).toBeInTheDocument();

    await userEvent.click(screen.getByText("test.zip"));
    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.tabResources.modalOptionsTitle"),
      ).toBeInTheDocument();
    });
    await userEvent.click(
      screen.getByText("searchFindings.tabResources.download"),
    );
    await waitFor((): void => {
      expect(open).toHaveBeenCalledWith(
        "https://test.com/file",
        undefined,
        "noopener,noreferrer,",
      );
    });
  });

  it("should handle errors when download a file", async (): Promise<void> => {
    expect.hasAssertions();

    const open: jest.Mock = jest.fn();
    open.mockReturnValue({ opener: "" });
    window.open = open; // eslint-disable-line functional/immutable-data
    const mocksMutation = [
      graphqlMocked.mutation(
        DOWNLOAD_FILE_MUTATION,
        (): StrictResponse<IErrorMessage> => {
          return HttpResponse.json({
            errors: [new GraphQLError("Error downloading files")],
          });
        },
      ),
    ];
    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_remove_files_mutate" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Files
          groupName={mockProps.groupName}
          organizationName={mockProps.organizationName}
        />
      </authzPermissionsContext.Provider>,
      { mocks: [...mocksFiles, ...mocksMutation] },
    );
    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(NUMBER_OF_ROWS);
    });

    expect(screen.queryByText("test.zip")).toBeInTheDocument();

    await userEvent.click(screen.getByText("test.zip"));
    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.tabResources.modalOptionsTitle"),
      ).toBeInTheDocument();
    });
    await userEvent.click(
      screen.getByText("searchFindings.tabResources.download"),
    );
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith("groupAlerts.errorTextsad");
    });
  });

  it("should handle errors when adding a file", async (): Promise<void> => {
    expect.hasAssertions();

    const file: File = new File(["image.png"], "image.png", {
      type: "image/png",
    });
    const mocksMutation = [
      graphqlMocked.mutation(
        ADD_FILES_TO_DB_MUTATION,
        ({
          variables,
        }): StrictResponse<
          Record<"errors", IMessage[]> | { data: AddFilesToDb }
        > => {
          const { filesDataInput, groupName } = variables;
          if (
            isEqual(filesDataInput, [
              {
                description: "Test description",
                fileName: "image.png",
              },
            ]) &&
            groupName === "TEST"
          ) {
            return HttpResponse.json({
              errors: [
                new GraphQLError("Error updating url roots data"),
                new GraphQLError("Exception - Invalid field in form"),
                new GraphQLError("Exception - Invalid characters"),
              ],
            });
          }

          return HttpResponse.json({
            data: {
              addFilesToDb: { success: true },
            },
          });
        },
      ),
      graphqlMocked.mutation(
        SIGN_POST_URL_MUTATION,
        ({
          variables,
        }): StrictResponse<IErrorMessage | { data: SignPostUrl }> => {
          const { filesDataInput, groupName } = variables;
          if (
            isEqual(filesDataInput, [
              {
                description: "Test description",
                fileName: "image.png",
              },
            ]) &&
            groupName === "TEST"
          ) {
            return HttpResponse.json({
              data: {
                signPostUrl: {
                  success: true,
                  url: {
                    fields: {
                      algorithm: "",
                      credential: "",
                      date: "",
                      key: "",
                      policy: "",
                      securitytoken: "",
                      signature: "",
                    },
                    url: "https://mocked.test",
                  },
                },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error signing urls")],
          });
        },
      ),
    ];

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_sign_post_url_mutate" },
    ]);
    const server = newServer({
      post: [
        "https://mocked.test",
        {
          status: 403,
        },
      ],
    });

    server.install();
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Files
          groupName={mockProps.groupName}
          organizationName={mockProps.organizationName}
        />
      </authzPermissionsContext.Provider>,
      { mocks: [...mocksFiles, ...mocksMutation] },
    );
    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(NUMBER_OF_ROWS);
    });

    expect(
      screen.queryByText("searchFindings.tabResources.addRepository"),
    ).toBeInTheDocument();

    await userEvent.click(
      screen.getByText("searchFindings.tabResources.addRepository"),
    );

    await waitFor((): void => {
      expect(screen.queryByTestId("file")).toBeInTheDocument();
    });

    await userEvent.upload(screen.getByTestId("file"), file);

    await userEvent.type(
      screen.getByRole("textbox", { name: "description" }),
      "Test description",
    );
    await waitFor((): void => {
      expect(screen.getByText(btnConfirm)).not.toBeDisabled();
    });
    await userEvent.click(screen.getByText(btnConfirm));

    await waitFor(
      (): void => {
        expect(screen.queryAllByRole("row")).toHaveLength(3);
      },
      { timeout: 2000 },
    );
    server.remove();
    jest.clearAllMocks();
  });

  it("should handle error when there are repeated files", async (): Promise<void> => {
    expect.hasAssertions();

    const file: File = new File([""], "test.zip", { type: "application/zip" });
    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_sign_post_url_mutate" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Files
          groupName={mockProps.groupName}
          organizationName={mockProps.organizationName}
        />
      </authzPermissionsContext.Provider>,
      { mocks: mocksFiles },
    );
    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(NUMBER_OF_ROWS);
    });

    expect(
      screen.queryByText("searchFindings.tabResources.addRepository"),
    ).toBeInTheDocument();

    await userEvent.click(
      screen.getByText("searchFindings.tabResources.addRepository"),
    );

    await waitFor((): void => {
      expect(screen.queryByTestId("file")).toBeInTheDocument();
    });

    fireEvent.change(screen.getByTestId("file"), {
      target: { files: [file] },
    });

    await userEvent.type(
      screen.getByRole("textbox", { name: "description" }),
      "Test description",
    );
    await waitFor((): void => {
      expect(screen.getByText(btnConfirm)).not.toBeDisabled();
    });
    await userEvent.click(screen.getByText(btnConfirm));

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        "searchFindings.tabResources.repeatedItem",
      );
    });
    jest.clearAllMocks();
  });

  it("should render error getting files", async (): Promise<void> => {
    expect.hasAssertions();

    const mocksMutation = [
      graphqlMocked.query(GET_FILES, (): StrictResponse<IErrorMessage> => {
        return HttpResponse.json({
          errors: [new GraphQLError("Error getting group data")],
        });
      }),
    ];

    render(
      <Files
        groupName={mockProps.groupName}
        organizationName={mockProps.organizationName}
      />,
      {
        mocks: [...mocksMutation, initialGitRootsQueriesMockGlobal],
      },
    );
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith("groupAlerts.errorTextsad");
    });
    jest.clearAllMocks();
  });

  it("should manage invalid fileName adding a file", async (): Promise<void> => {
    expect.hasAssertions();

    const file = new File(["okada test.zip"], "okada test.zip", {
      type: "application/zip",
    });
    const mocksMutation = [
      graphqlMocked.mutation(
        ADD_FILES_TO_DB_MUTATION,
        ({
          variables,
        }): StrictResponse<IErrorMessage | { data: AddFilesToDb }> => {
          const { filesDataInput, groupName } = variables;
          if (
            isEqual(filesDataInput, [
              {
                description: "Test description",
                fileName: "okada-test.zip",
              },
            ]) &&
            groupName === "TEST"
          ) {
            return HttpResponse.json({
              data: {
                addFilesToDb: { success: true },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error updating url roots data")],
          });
        },
      ),
      graphqlMocked.mutation(
        SIGN_POST_URL_MUTATION,
        ({
          variables,
        }): StrictResponse<IErrorMessage | { data: SignPostUrl }> => {
          const { filesDataInput, groupName } = variables;
          if (
            isEqual(filesDataInput, [
              {
                description: "Test description",
                fileName: "okada-test.zip",
              },
            ]) &&
            groupName === "TEST"
          ) {
            return HttpResponse.json({
              data: {
                signPostUrl: {
                  success: true,
                  url: {
                    fields: {
                      algorithm: "",
                      credential: "",
                      date: "",
                      key: "",
                      policy: "",
                      securitytoken: "",
                      signature: "",
                    },
                    url: "https://mocked.test",
                  },
                },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error signing urls")],
          });
        },
      ),
      graphqlMocked.query(
        GET_FILES,
        (): StrictResponse<{ data: GetFiles }> => {
          return HttpResponse.json({
            data: {
              resources: {
                files: [
                  {
                    description: "Test",
                    fileName: "test.zip",
                    uploadDate: "2019-03-01 15:21",
                    uploader: "unittest@fluidattacks.com",
                  },
                  {
                    description: "shell",
                    fileName: "shell.exe",
                    uploadDate: "2019-04-24 14:56",
                    uploader: "unittest@fluidattacks.com",
                  },
                  {
                    description: "Test description",
                    fileName: "okada-test.zip",
                    uploadDate: "2019-03-01 15:21",
                    uploader: "unittest@fluidattacks.com",
                  },
                ],
              },
            },
          });
        },
        { once: true },
      ),
    ];
    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_sign_post_url_mutate" },
    ]);
    const server = newServer({
      post: [
        "https://mocked.test",
        {
          status: 200,
        },
      ],
    });

    server.install();

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Files
          groupName={mockProps.groupName}
          organizationName={mockProps.organizationName}
        />
      </authzPermissionsContext.Provider>,
      { mocks: [...mocksFiles, ...mocksMutation] },
    );
    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(NUMBER_OF_ROWS);
    });

    expect(
      screen.queryByText("searchFindings.tabResources.addRepository"),
    ).toBeInTheDocument();

    await userEvent.click(
      screen.getByText("searchFindings.tabResources.addRepository"),
    );

    await waitFor((): void => {
      expect(screen.queryByTestId("file")).toBeInTheDocument();
    });

    fireEvent.change(screen.getByTestId("file"), { target: { files: [file] } });

    await userEvent.type(
      screen.getByRole("textbox", { name: "description" }),
      "Test description",
    );
    await waitFor((): void => {
      expect(screen.getByText(btnConfirm)).not.toBeDisabled();
    });

    await userEvent.click(screen.getByText(btnConfirm));

    expect(
      screen.queryByText("searchFindings.tabResources.uploadingProgress"),
    ).toBeInTheDocument();

    await waitFor(
      (): void => {
        expect(screen.queryAllByRole("row")).toHaveLength(3);
      },
      { timeout: 2000 },
    );
    server.remove();
    jest.clearAllMocks();
  });
});
