/* eslint-disable @typescript-eslint/no-unnecessary-condition */
import { useMutation, useQuery } from "@apollo/client";
import { useAbility } from "@casl/react";
import { Button, Text } from "@fluidattacks/design";
import type { ColumnDef, Row } from "@tanstack/react-table";
import _ from "lodash";
import mixpanel from "mixpanel-browser";
import { useCallback, useMemo, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

import type { IAddFilesToDbResults, IFile, IFilesProps } from "./types";

import { getEnvsFromResult } from "../git-roots/utils";
import {
  ADD_FILES_TO_DB_MUTATION,
  DOWNLOAD_FILE_MUTATION,
  GET_FILES,
  GET_GIT_ROOTS_QUERIES,
  GET_GROUP_ENV_URLS_FRAGMENT,
  REMOVE_FILE_MUTATION,
  SIGN_POST_URL_MUTATION,
} from "../queries";
import type { IEnvironmentUrlAttr } from "../types";
import { Table } from "components/table";
import { Can } from "context/authz/can";
import { authzPermissionsContext } from "context/authz/config";
import { getFragmentData } from "gql/fragment-masking";
import type { GroupFile } from "gql/graphql";
import { useModal, useTable } from "hooks";
import { useAudit } from "hooks/use-audit";
import { AddFilesModal } from "pages/group/scope/files/add-files-modal";
import type { IAddFileValues } from "pages/group/scope/files/add-files-modal/types";
import { FileOptionsModal } from "pages/group/scope/files/file-options-modal";
import { formatDate } from "utils/date";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";
import { openUrl } from "utils/resource-helpers";
import { uploadGroupFileToS3 } from "utils/upload-file-to-s3";
import { sanitizeFileName } from "utils/validations";

const MIN_SUCCESS_STATUS_CODE = 200;
const MAX_SUCCESS_STATUS_CODE = 300;
const Files: React.FC<IFilesProps> = ({
  groupName,
  organizationName,
}): JSX.Element => {
  const { t } = useTranslation();
  const permissions = useAbility(authzPermissionsContext);
  const [progressPercentage, setProgressPercentage] = useState(0);
  const [isAddingFileInfoToDB, setIsAddingFileInfoToDB] = useState(false);

  const theme = useTheme();
  const tableRef = useTable("tblFiles");

  // State management
  const [values, setValues] = useState<IAddFileValues>({
    description: "",
    file: undefined as unknown as FileList,
  });
  const modal = useModal("file-modal");

  const [isAddModalOpen, setIsAddModalOpen] = useState(false);
  const openAddModal = useCallback((): void => {
    setIsAddModalOpen(true);
  }, []);

  const closeAddModal = useCallback((): void => {
    setIsAddModalOpen(false);
    setValues({
      description: "",
      file: undefined as unknown as FileList,
    });
  }, []);

  const closeOptionsModal = useCallback((): void => {
    modal.close();
  }, [modal]);

  const [currentRow, setCurrentRow] = useState<IFile>({
    description: "",
    fileName: "",
  });
  const handleRowClick = useCallback(
    (rowInfo: Row<IFile>): ((event: React.FormEvent) => void) => {
      return (event: React.FormEvent): void => {
        setCurrentRow(rowInfo.original);
        modal.open();
        event.preventDefault();
      };
    },
    [modal],
  );

  const [isButtonEnabled, setIsButtonEnabled] = useState(false);
  const disableButton = useCallback((): void => {
    setIsButtonEnabled(true);
  }, []);

  const enableButton = useCallback((): void => {
    setIsButtonEnabled(false);
  }, []);

  // GraphQL operations
  const { addAuditEvent } = useAudit();
  const { data, refetch } = useQuery(GET_FILES, {
    onCompleted: (): void => {
      addAuditEvent("Group.Files", groupName);
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.error("An error occurred loading group files", error);
      });
    },
    variables: { groupName },
  });
  const { data: gitRootsData } = useQuery(GET_GIT_ROOTS_QUERIES, {
    fetchPolicy: "cache-first",
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.error("Couldn't load Git roots environments", error);
      });
    },
    variables: {
      canGetSecrets: permissions.can(
        "integrates_api_resolvers_git_environment_url_secrets_resolve",
      ),
      groupName,
      organizationName,
    },
  });

  const envUrlsResult = getFragmentData(
    GET_GROUP_ENV_URLS_FRAGMENT,
    gitRootsData,
  );
  const gitEnvironmentUrls = useMemo(
    (): IEnvironmentUrlAttr[] => getEnvsFromResult(envUrlsResult),
    [envUrlsResult],
  );
  const apkEnvironments = useMemo(
    (): Record<string, IEnvironmentUrlAttr> =>
      gitEnvironmentUrls
        .filter((environment): boolean => environment.urlType === "APK")
        .reduce(
          (previous, current): Record<string, IEnvironmentUrlAttr> => ({
            [current.url]: current,
            ...previous,
          }),
          {},
        ),
    [gitEnvironmentUrls],
  );

  const [downloadFile] = useMutation(DOWNLOAD_FILE_MUTATION, {
    onCompleted: (downloadData): void => {
      openUrl(downloadData.downloadFile.url ?? "");
      addAuditEvent("Group.Files.Download", currentRow.fileName);
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.error("An error occurred downloading group files", error);
      });
    },
    variables: {
      filesData: currentRow.fileName,
      groupName,
    },
  });

  const [removeFile] = useMutation(REMOVE_FILE_MUTATION, {
    onCompleted: (): void => {
      void refetch();
      mixpanel.track("RemoveGroupFiles");
      msgSuccess(
        t("searchFindings.tabResources.successRemove"),
        t("searchFindings.tabUsers.titleSuccess"),
      );
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.error("An error occurred removing group files", error);
      });
    },
  });
  const handleRemoveFile = useCallback(async (): Promise<void> => {
    closeOptionsModal();
    mixpanel.track("RemoveFile");
    await removeFile({
      variables: {
        filesDataInput: { fileName: currentRow.fileName },
        groupName,
      },
    });
  }, [closeOptionsModal, currentRow.fileName, groupName, removeFile]);

  const [uploadFile] = useMutation(SIGN_POST_URL_MUTATION, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        if (error.message === "Exception - Invalid characters in filename") {
          msgError(t("searchFindings.tabResources.invalidChars"));
        } else {
          msgError(t("groupAlerts.errorTextsad"));
          Logger.error("An error occurred uploading group files", error);
        }
      });
    },
    variables: {
      filesDataInput: [
        {
          description: "",
          fileName: currentRow.fileName,
        },
      ],
      groupName,
    },
  });

  const [addFilesToDb] = useMutation(ADD_FILES_TO_DB_MUTATION, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        if (error.message === "File name already exists in group files") {
          msgError(t("validations.invalidFileName"));
        } else {
          msgError(t("groupAlerts.errorTextsad"));
          Logger.error("An error occurred adding files to the db", error);
        }
      });
    },
    variables: {
      filesDataInput: [
        {
          description: "",
          fileName: currentRow.fileName,
          uploadDate: new Date().toISOString(),
        },
      ],
      groupName,
    },
  });

  const resourcesFiles = useMemo((): GroupFile[] => {
    if (!data?.resources) {
      return [];
    }

    return data.resources.files ?? [];
  }, [data?.resources]);

  const filesDataset = useMemo((): IFile[] => {
    return resourcesFiles === undefined ? [] : (resourcesFiles as IFile[]);
  }, [resourcesFiles]);

  const handleUpload = useCallback(
    async (formikValues: IAddFileValues): Promise<void> => {
      setValues(formikValues);

      const sanitizedFileName = sanitizeFileName(formikValues.file[0].name);
      const newFile = new File([formikValues.file[0]], sanitizedFileName, {
        type: formikValues.file[0].type,
      });
      const repeatedFiles = filesDataset.filter(
        (file): boolean => file.fileName === sanitizedFileName,
      );
      if (repeatedFiles.length > 0) {
        msgError(t("searchFindings.tabResources.repeatedItem"));

        return;
      }

      disableButton();

      const firstFile: IFile = {
        description: formikValues.description,
        fileName: sanitizedFileName,
        uploadDate: new Date().toISOString(),
      };

      const results = await uploadFile({
        onCompleted: (): void => {
          mixpanel.track("UploadGroupFile", {
            fileName: firstFile.fileName,
            group: groupName,
          });
          addAuditEvent(
            "UploadGroupFile",
            [firstFile.fileName, groupName].join("/"),
          );
        },
        variables: {
          filesDataInput: [firstFile],
          groupName,
        },
      });

      if (!results.data) return;

      const onError = (): void => {
        enableButton();
        closeAddModal();
      };

      try {
        const status = await uploadGroupFileToS3(
          results,
          newFile,
          groupName,
          onError,
          setProgressPercentage,
        );

        if (
          status < MIN_SUCCESS_STATUS_CODE ||
          status >= MAX_SUCCESS_STATUS_CODE
        ) {
          msgError(t("groupAlerts.errorTextsad"));

          return;
        }

        setIsAddingFileInfoToDB(true);
        await addFilesToDb({
          onCompleted: (resultData: IAddFilesToDbResults): void => {
            const { addFilesToDb: mutationResults } = resultData;
            const { success } = mutationResults;

            if (!success) {
              msgError(t("groupAlerts.errorTextsad"));
              Logger.error("An error occurred adding group files to the db");
              setIsAddingFileInfoToDB(false);
              enableButton();
              closeAddModal();

              return;
            }
            msgSuccess(
              t("searchFindings.tabResources.success"),
              t("searchFindings.tabUsers.titleSuccess"),
            );
            void refetch();
            mixpanel.track("AddGroupFiles");
          },
          variables: {
            filesDataInput: [firstFile],
            groupName,
          },
        });
      } catch (err) {
        Logger.error("Error uploading file to s3", err);
      }

      enableButton();
      setIsAddingFileInfoToDB(false);
      closeAddModal();
    },
    [
      addAuditEvent,
      setValues,
      filesDataset,
      groupName,
      disableButton,
      enableButton,
      closeAddModal,
      t,
      refetch,
      uploadFile,
      addFilesToDb,
    ],
  );

  if (_.isUndefined(data) || _.isEmpty(data)) {
    return <div />;
  }

  const tableHeaders: ColumnDef<IFile>[] = [
    {
      accessorKey: "fileName",
      header: t("searchFindings.filesTable.file"),
    },
    {
      accessorKey: "description",
      header: t("searchFindings.filesTable.description"),
    },
    {
      accessorKey: "uploadDate",
      cell: (cell): string => formatDate(String(cell.getValue())),
      header: t("searchFindings.filesTable.uploadDate"),
    },
  ];

  return (
    <React.Fragment>
      <Table
        columns={tableHeaders}
        data={filesDataset}
        onRowClick={handleRowClick}
        rightSideComponents={
          <Can do={"integrates_api_mutations_sign_post_url_mutate"}>
            <Button
              icon={"plus"}
              id={"file-add"}
              onClick={openAddModal}
              tooltip={t("searchFindings.tabResources.files.btnTooltip")}
              variant={"primary"}
            >
              {t("searchFindings.tabResources.addRepository")}
            </Button>
          </Can>
        }
        tableRef={tableRef}
      />
      <Text
        color={theme.palette.gray[800]}
        fontWeight={"bold"}
        mt={1.25}
        size={"sm"}
      >
        {t("searchFindings.tabResources.totalFiles")}&nbsp;
        {filesDataset.length}
      </Text>
      <AddFilesModal
        isAddingFileToDB={isAddingFileInfoToDB}
        isOpen={isAddModalOpen}
        isUploading={isButtonEnabled}
        onClose={closeAddModal}
        onSubmit={handleUpload}
        progressPercentage={progressPercentage}
        values={values}
      />
      <Can
        do={"integrates_api_mutations_remove_files_mutate"}
        passThrough={true}
      >
        {(canRemove: boolean): JSX.Element => (
          <FileOptionsModal
            apkEnvironments={apkEnvironments}
            canRemove={canRemove}
            fileName={currentRow.fileName}
            filesDataSet={filesDataset}
            groupName={groupName}
            modalRef={modal}
            onClose={closeOptionsModal}
            onDelete={handleRemoveFile}
            onDownload={downloadFile}
            organizationName={organizationName}
          />
        )}
      </Can>
    </React.Fragment>
  );
};

export type { IFilesProps };
export { Files };
