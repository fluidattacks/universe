import { Alert } from "@fluidattacks/design";
import { useConfirmDialog } from "@fluidattacks/design";
import { StrictMode, useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { Modal } from "./modal";
import type { IFileOptionsModalProps } from "./types";

import { Logger } from "utils/logger";

const FileOptionsModal: React.FC<IFileOptionsModalProps> = ({
  apkEnvironments,
  canRemove,
  fileName,
  filesDataSet,
  groupName,
  organizationName,
  modalRef,
  onClose,
  onDelete,
  onDownload,
}): JSX.Element => {
  const { t } = useTranslation();
  const { confirm, ConfirmDialog } = useConfirmDialog();

  const onConfirmDelete = useCallback((): void => {
    if (fileName in apkEnvironments) {
      confirm({
        message:
          fileName in apkEnvironments ? (
            <Alert>
              {t("searchFindings.tabResources.files.confirm.message")}
            </Alert>
          ) : (
            ""
          ),
        title: t("searchFindings.tabResources.files.confirm.title"),
      }).catch((): void => {
        Logger.error("An error occurred during the deleting process");
      });
    } else {
      confirm({
        title: t("searchFindings.tabResources.files.confirm.title"),
      })
        .then((result: boolean): void => {
          if (result) {
            onDelete();
          }
        })
        .catch((): void => {
          Logger.error("An error occurred during the deleting process");
        });
    }
  }, [confirm, onDelete, t, apkEnvironments, fileName]);

  return (
    <StrictMode>
      <Modal
        ConfirmDialog={ConfirmDialog}
        apkEnvironments={apkEnvironments}
        canRemove={canRemove}
        fileName={fileName}
        filesDataSet={filesDataSet}
        groupName={groupName}
        modalRef={modalRef}
        onClose={onClose}
        onConfirmDelete={onConfirmDelete}
        onDelete={onDelete}
        onDownload={onDownload}
        organizationName={organizationName}
      />
    </StrictMode>
  );
};

export { FileOptionsModal };
