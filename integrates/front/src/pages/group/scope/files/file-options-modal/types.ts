import type { IEnvironmentUrlAttr } from "../../types";
import type { IFile } from "../types";
import type { IUseModal } from "hooks/use-modal";

interface IFileOptionsModalProps {
  apkEnvironments: Record<string, IEnvironmentUrlAttr>;
  canRemove: boolean;
  fileName: string;
  groupName: string;
  organizationName: string;
  modalRef: IUseModal;
  onClose: () => void;
  onDelete: () => void;
  onDownload: () => void;
  filesDataSet: IFile[];
}

export type { IFileOptionsModalProps };
