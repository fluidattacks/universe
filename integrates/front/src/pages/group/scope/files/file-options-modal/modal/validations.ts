import type { InferType, Schema } from "yup";
import { mixed, object } from "yup";

import { MOBILE_EXTENSIONS } from "pages/group/scope/git-roots/management-modal/environments/add-environment/utils";
import { translate } from "utils/translations/translate";

const MAX_FILE_SIZE = 5000;

const validationSchema = (oldFileName: string): InferType<Schema> =>
  object().shape({
    file: mixed<FileList>()
      .required(translate.t("validations.required"))
      .isValidFileName()
      .isValidFileSize(undefined, MAX_FILE_SIZE)
      .isValidFunction((value): boolean => {
        if (value === undefined) {
          return true;
        }
        const fileName = (value as FileList)[0].name;

        return fileName !== oldFileName;
      }, translate.t("searchFindings.tabResources.files.form.wrongImageName"))
      .isValidFileType(
        MOBILE_EXTENSIONS.map((extension): string =>
          extension.replace(".", ""),
        ),
        translate.t("groupAlerts.fileTypeEnvironment"),
      ),
  });

export { validationSchema };
