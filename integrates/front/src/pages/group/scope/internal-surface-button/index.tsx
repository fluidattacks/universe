import { useAbility } from "@casl/react";
import { Button, useConfirmDialog } from "@fluidattacks/design";
import { useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";

import { authzPermissionsContext } from "context/authz/config";
import { useGroupUrl } from "pages/group/hooks";
import { Logger } from "utils/logger";

const InternalSurfaceButton: React.FC = (): JSX.Element => {
  const { url: groupUrl } = useGroupUrl();
  const permissions = useAbility(authzPermissionsContext);
  const navigate = useNavigate();
  const { t } = useTranslation();
  const { confirm, ConfirmDialog } = useConfirmDialog();

  const handleInternalSurfaceClick = useCallback((): void => {
    navigate(`${groupUrl}/internal/surface/lines`);
  }, [groupUrl, navigate]);

  const canGetToeLines = permissions.can(
    "integrates_api_resolvers_group_toe_lines_connection_resolve",
  );
  const canGetToeInputs = permissions.can(
    "integrates_api_resolvers_group_toe_inputs_resolve",
  );
  const canGetToePorts = permissions.can(
    "integrates_api_resolvers_group_toe_ports_resolve",
  );
  const canSeeInternalToe = permissions.can("see_internal_toe");

  const handleClick = useCallback((): void => {
    confirm({
      title: t("group.scope.internalSurface.confirmDialog.title"),
    })
      .then((result: boolean): void => {
        if (result) {
          handleInternalSurfaceClick();
        }
      })
      .catch((): void => {
        Logger.error("An error occurred during the filtering process");
      });
  }, [confirm, handleInternalSurfaceClick, t]);

  return (
    <React.Fragment>
      {canSeeInternalToe &&
      (canGetToeInputs || canGetToeLines || canGetToePorts) ? (
        <Button
          id={"git-root-internal-surface"}
          onClick={handleClick}
          tooltip={t("group.tabs.toe.tooltip")}
          variant={"secondary"}
        >
          {t("group.tabs.toe.text")}
        </Button>
      ) : undefined}
      <ConfirmDialog />
    </React.Fragment>
  );
};

export { InternalSurfaceButton };
