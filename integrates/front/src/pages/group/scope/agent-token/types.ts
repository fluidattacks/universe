import type { IUseModal } from "hooks/use-modal";

interface IApiTokenValues {
  expDate: string;
  sessionJwt: string;
}

interface IGetForcesTokenAttr {
  group: {
    forcesExpDate: string | undefined;
    forcesToken: string | undefined;
  };
}

interface IAPITokenForcesModalProps {
  groupName: string;
  modalRef: IUseModal;
}

interface IModalProps {
  currentToken: string | undefined;
  getTokenCalled: boolean;
  getTokenLoading: boolean;
  handleCopy: () => Promise<void>;
  handleReveal: () => void;
  handleUpdateAPIToken: () => Promise<void>;
  open: boolean;
  onClose: () => void;
  values: IApiTokenValues;
}

export type {
  IAPITokenForcesModalProps,
  IApiTokenValues,
  IGetForcesTokenAttr,
  IModalProps,
};
