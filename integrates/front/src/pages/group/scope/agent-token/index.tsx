import { Button, Gap, Text, useModal } from "@fluidattacks/design";
import { useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { APITokenForcesModal } from "./api-token-forces-modal";

import type { IServicesProps } from "../services/types";
import { openUrl } from "utils/resource-helpers";

const AgentToken: React.FC<Pick<IServicesProps, "groupName">> = ({
  groupName,
}): JSX.Element => {
  const { t } = useTranslation();
  const apiTokenModal = useModal("api-token-forces-modal");

  const handleOpen = useCallback((): void => {
    apiTokenModal.open();
  }, [apiTokenModal]);
  const handleInstall = useCallback((): void => {
    openUrl(
      "https://help.fluidattacks.com/portal/en/kb/articles/install-the-ci-agent-to-break-the-build",
    );
  }, []);

  return (
    <React.StrictMode>
      <Text size={"sm"}>{t("searchFindings.agentTokenSection.about")}</Text>
      <Gap>
        <Button onClick={handleInstall} variant={"primary"}>
          {t("searchFindings.agentTokenSection.install")}
        </Button>
        <Button onClick={handleOpen} variant={"secondary"}>
          {t("searchFindings.agentTokenSection.generate")}
        </Button>
      </Gap>
      <APITokenForcesModal groupName={groupName} modalRef={apiTokenModal} />
    </React.StrictMode>
  );
};

export { AgentToken };
