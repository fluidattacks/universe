import type { ApolloError } from "@apollo/client";
import { useQuery } from "@apollo/client";
import { Button, Container } from "@fluidattacks/design";
import _ from "lodash";
import { useCallback, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { useUpdateAPIToken } from "./hooks";
import { GET_FORCES_TOKEN } from "./queries";

import type { IAPITokenForcesModalProps } from "../types";
import { InnerForm } from "components/form";
import { FormModal } from "components/form-modal";
import { Input, TextArea } from "components/input";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

const APITokenForcesModal: React.FC<IAPITokenForcesModalProps> = ({
  modalRef,
  groupName,
}): JSX.Element => {
  const { t } = useTranslation();
  const { close, isOpen } = modalRef;
  const [updateApiToken] = useUpdateAPIToken(groupName);
  const [tokenIsExpired, setTokenIsExpired] = useState(false);
  const handleOnError = ({ graphQLErrors }: ApolloError): void => {
    graphQLErrors.forEach((error): void => {
      switch (error.message) {
        case "Exception - Forces token has expired":
          setTokenIsExpired(true);
          break;
        case "Exception - Invalid Authorization":
          msgError(t("groupAlerts.invalidAuthorization"));
          break;
        default:
          Logger.warning("An error occurred getting forces token", error);
          msgError(t("groupAlerts.errorTextsad"));
      }
    });
  };
  const { data, loading, refetch } = useQuery(GET_FORCES_TOKEN, {
    fetchPolicy: "network-only",
    onError: handleOnError,
    skip: !isOpen,
    variables: {
      groupName,
    },
  });

  const currentToken = data?.group.forcesToken;
  const expDate = data?.group.forcesExpDate;
  const tokenNotSet = currentToken === null;
  const [show, setShow] = useState(false);

  const handleReveal = useCallback((): void => {
    void refetch();
    setShow(true);
  }, [setShow, refetch]);

  const handleUpdateAPIToken = useCallback(async (): Promise<void> => {
    setTokenIsExpired(false);
    await updateApiToken({ variables: { groupName } });
    void refetch();
    setShow(true);
  }, [groupName, updateApiToken, refetch]);

  const handleCopy = useCallback(async (): Promise<void> => {
    const { clipboard } = navigator;

    if (_.isUndefined(clipboard)) {
      msgError(t("updateForcesToken.copy.failed"));
    } else {
      await clipboard.writeText(currentToken ?? "");
      msgSuccess(
        t("updateForcesToken.copy.successfully"),
        t("updateForcesToken.copy.success"),
      );
    }
  }, [currentToken, t]);

  const expiredText = t("updateForcesToken.expiredForcesToken");
  const noSetText = t("updateForcesToken.unsettledForcesToken");
  const revealTokenText = t("updateForcesToken.revealForcesToken");
  const initialJwtValue = ((): string | undefined => {
    if (currentToken === null) {
      return noSetText;
    } else if (show) {
      return currentToken;
    }

    return revealTokenText;
  })();
  const disableReveal = loading || show || tokenIsExpired || tokenNotSet;
  const disableUpdate = tokenIsExpired
    ? false
    : loading || (!tokenNotSet && !show);

  return (
    <FormModal
      enableReinitialize={true}
      initialValues={{
        expDate: show ? (expDate ?? "") : "",
        sessionJwt: tokenIsExpired ? expiredText : initialJwtValue,
      }}
      modalRef={modalRef}
      name={"updateForcesToken"}
      onSubmit={handleUpdateAPIToken}
      size={"sm"}
      title={t("updateForcesToken.title")}
    >
      <InnerForm
        onCancel={close}
        submitButtonLabel={t(
          `updateForcesToken.${_.isEmpty(currentToken) ? "generate" : "reset"}`,
        )}
        submitDisabled={disableUpdate}
      >
        <Input
          disabled={true}
          label={t("updateForcesToken.expDate")}
          name={"expDate"}
          placeholder={"YYYY-MM-DD"}
        />
        <br />
        <TextArea
          disabled={true}
          label={t("updateForcesToken.accessToken")}
          maskValue={true}
          name={"sessionJwt"}
          rows={7}
        />
        <Container display={"flex"} gap={0.25}>
          <Button
            disabled={disableReveal}
            icon={"eye"}
            iconSize={"xxss"}
            onClick={handleReveal}
            variant={"ghost"}
          >
            {t("updateForcesToken.revealToken")}
          </Button>
          <Button
            disabled={_.isEmpty(currentToken)}
            icon={"copy"}
            iconSize={"xxss"}
            onClick={handleCopy}
            variant={"ghost"}
          >
            {t("updateForcesToken.copy.copy")}
          </Button>
        </Container>
      </InnerForm>
    </FormModal>
  );
};

export type { IAPITokenForcesModalProps };
export { APITokenForcesModal };
