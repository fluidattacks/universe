import { graphql } from "gql";

const GET_FORCES_TOKEN = graphql(`
  query GetForcesToken($groupName: String!) {
    group(groupName: $groupName) {
      forcesExpDate
      forcesToken
      name
    }
  }
`);

const UPDATE_FORCES_TOKEN_MUTATION = graphql(`
  mutation UpdateForcesAccessTokenMutation($groupName: String!) {
    updateForcesAccessToken(groupName: $groupName) {
      sessionJwt
      success
    }
  }
`);

export { GET_FORCES_TOKEN, UPDATE_FORCES_TOKEN_MUTATION };
