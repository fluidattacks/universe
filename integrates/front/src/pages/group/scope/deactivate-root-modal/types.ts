import type { IUseModal } from "@fluidattacks/design";

interface IGroup {
  name: string;
  organization: string;
  service: string;
}

interface IDeactivateRootModalProps {
  groupName: string;
  modalRef: IUseModal;
  rootId: string;
  onUpdate: () => void;
}

export type { IDeactivateRootModalProps, IGroup };
