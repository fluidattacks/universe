import { PureAbility } from "@casl/ability";
import { useModal } from "@fluidattacks/design";
import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import { HttpResponse, type StrictResponse, graphql } from "msw";

import { DeactivateRootModal } from ".";
import {
  DEACTIVATE_ROOT,
  GET_GROUPS,
  GET_ROOT_VULNS,
  MOVE_ROOT,
} from "../queries";
import { authzPermissionsContext } from "context/authz/config";
import type {
  DeactivateRootMutation as DeactivateRoot,
  GetGroupsQuery as GetGroups,
  GetRootVulnsQuery as GetRootVulns,
  MoveRootMutation as MoveRoot,
} from "gql/graphql";
import { ServiceType, Technique, VulnerabilityState } from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";
import { msgError, msgSuccess } from "utils/notifications";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

const onClose: jest.Mock = jest.fn();
const onUpdate: jest.Mock = jest.fn();
const WrapperComponent = ({
  groupName,
  rootId = "4039d098-ffc5-4984-8ed3-eb17bca98e19",
}: Readonly<{ groupName: string; rootId?: string }>): JSX.Element => {
  const modalTest = useModal("test-deactivate-root");

  return (
    <DeactivateRootModal
      groupName={groupName}
      modalRef={{
        ...modalTest,
        close: onClose,
        isOpen: true,
      }}
      onUpdate={onUpdate}
      rootId={rootId}
    />
  );
};

describe("deactivate Root Modal", (): void => {
  const graphqlMocked = graphql.link(LINK);

  const meGoupAndRootVulnsQueryMock = [
    graphqlMocked.query(GET_GROUPS, (): StrictResponse<{ data: GetGroups }> => {
      return HttpResponse.json({
        data: {
          me: {
            __typename: "Me",
            organizations: [
              {
                __typename: "Organization",
                groups: [
                  {
                    __typename: "Group",
                    name: "unittesting",
                    organization: "okada",
                    service: ServiceType.White,
                  },
                  {
                    __typename: "Group",
                    name: "TEST",
                    organization: "okada",
                    service: ServiceType.White,
                  },
                ],
                name: "okada",
              },
            ],
            userEmail: "",
          },
        },
      });
    }),
    graphqlMocked.query(
      GET_ROOT_VULNS,
      (): StrictResponse<{ data: GetRootVulns }> => {
        return HttpResponse.json({
          data: {
            root: {
              __typename: "GitRoot",
              id: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
              vulnerabilities: [
                {
                  id: "test",
                  state: VulnerabilityState.Vulnerable,
                  technique: Technique.Ptaas,
                  vulnerabilityType: "lines",
                },
              ],
            },
          },
        });
      },
    ),
    graphqlMocked.query(
      GET_ROOT_VULNS,
      (): StrictResponse<{ data: GetRootVulns }> => {
        return HttpResponse.json({
          data: {
            root: {
              __typename: "GitRoot",
              id: "4039d098-ffc5-4984-8ed3-eb17bca98e20",
              vulnerabilities: [
                {
                  id: "test",
                  state: VulnerabilityState.Vulnerable,
                  technique: null,
                  vulnerabilityType: "lines",
                },
              ],
            },
          },
        });
      },
    ),
  ];
  const mutationMock = graphqlMocked.mutation(
    DEACTIVATE_ROOT,
    ({
      variables,
    }): StrictResponse<IErrorMessage | { data: DeactivateRoot }> => {
      const { groupName, id, other } = variables;
      if (
        groupName === "unittesting" &&
        id === "4039d098-ffc5-4984-8ed3-eb17bca98e19" &&
        other === ""
      ) {
        return HttpResponse.json({
          data: {
            deactivateRoot: { __typename: "SimplePayload", success: true },
          },
        });
      }

      if (
        groupName === "unittesting" &&
        id === "4039d098-ffc5-4984-8ed3-eb17bca98e19" &&
        other === "deactivation root other test"
      ) {
        return HttpResponse.json({
          data: {
            deactivateRoot: { __typename: "SimplePayload", success: true },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Error deactivating roots")],
      });
    },
  );

  const moveRootMutationMock = graphqlMocked.mutation(
    MOVE_ROOT,
    ({ variables }): StrictResponse<IErrorMessage | { data: MoveRoot }> => {
      const { groupName, id, targetGroupName } = variables;
      if (
        groupName === "unittesting" &&
        id === "4039d098-ffc5-4984-8ed3-eb17bca98e19" &&
        targetGroupName === "TEST"
      ) {
        return HttpResponse.json({
          data: {
            moveRoot: { __typename: "SimplePayload", success: true },
          },
        });
      }

      if (
        groupName === "TEST" &&
        id === "4039d098-ffc5-4984-8ed3-eb17bca98e19" &&
        targetGroupName === "unittesting"
      ) {
        return HttpResponse.json({
          errors: [
            new GraphQLError(
              "Exception - Root with the same URL/branch already exists",
            ),
          ],
        });
      }

      if (
        groupName === "TEST" &&
        id === "4039d098-ffc5-4984-8ed3-eb17bca98e20" &&
        targetGroupName === "unittesting"
      ) {
        return HttpResponse.json({
          errors: [
            new GraphQLError(
              "Exception - Root with the same nickname already exists",
            ),
          ],
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Error moving roots")],
      });
    },
  );

  it("should render deactivate modal", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    render(
      <WrapperComponent groupName={"unittesting"} />,

      {
        mocks: [...meGoupAndRootVulnsQueryMock, mutationMock],
      },
    );

    await waitFor((): void => {
      expect(
        screen.queryByText("group.scope.common.deactivation.title"),
      ).toBeInTheDocument();
    });

    await userEvent.click(
      screen.getByText("group.scope.common.deactivation.reason.label"),
    );

    expect(
      screen.queryByText("group.scope.common.deactivation.reason.mistake"),
    ).toBeInTheDocument();

    expect(
      screen.queryByText("group.scope.common.deactivation.reason.other"),
    ).toBeInTheDocument();

    expect(
      screen.queryByText("group.scope.common.deactivation.reason.moved"),
    ).not.toBeInTheDocument();

    expect(screen.getByText("Confirm")).toBeDisabled();
    expect(screen.getByText("Cancel")).not.toBeDisabled();
  });

  it("should render deactivate modal with permisions", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            { action: "integrates_api_mutations_move_root_mutate" },
          ])
        }
      >
        <WrapperComponent groupName={"unittesting"} />
      </authzPermissionsContext.Provider>,
      {
        mocks: [...meGoupAndRootVulnsQueryMock, mutationMock],
      },
    );

    await waitFor((): void => {
      expect(
        screen.queryByText("group.scope.common.deactivation.title"),
      ).toBeInTheDocument();
    });

    await userEvent.click(
      screen.getByText("group.scope.common.deactivation.reason.label"),
    );

    expect(
      screen.queryByText("group.scope.common.deactivation.reason.mistake"),
    ).toBeInTheDocument();

    expect(
      screen.queryByText("group.scope.common.deactivation.reason.other"),
    ).toBeInTheDocument();

    expect(
      screen.queryByText("group.scope.common.deactivation.reason.moved"),
    ).toBeInTheDocument();

    expect(screen.getByText("Confirm")).toBeDisabled();
    expect(screen.getByText("Cancel")).not.toBeDisabled();

    await userEvent.click(screen.getByText("Cancel"));

    await waitFor((): void => {
      expect(onClose).toHaveBeenCalledTimes(1);
    });
  });

  it("should deactivate a root by selecting move to other group", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            { action: "integrates_api_mutations_move_root_mutate" },
          ])
        }
      >
        <WrapperComponent groupName={"unittesting"} />
      </authzPermissionsContext.Provider>,
      {
        mocks: [
          ...meGoupAndRootVulnsQueryMock,
          mutationMock,
          moveRootMutationMock,
        ],
      },
    );

    await waitFor((): void => {
      expect(
        screen.queryByText("group.scope.common.deactivation.title"),
      ).toBeInTheDocument();
    });

    expect(screen.getByText("Confirm")).toBeDisabled();

    await userEvent.click(
      screen.getByText("group.scope.common.deactivation.reason.label"),
    );
    await userEvent.click(
      screen.getByText("group.scope.common.deactivation.reason.moved"),
    );

    expect(
      screen.queryByText("group.scope.common.changeWarning"),
    ).toBeInTheDocument();
    expect(
      screen.queryByText("group.scope.common.deactivation.targetGroup"),
    ).toBeInTheDocument();

    const targetGroup = screen.getByRole("combobox", {
      name: "targetGroupName",
    });

    await userEvent.type(targetGroup, "TEST");

    expect(screen.getByText("Confirm")).not.toBeDisabled();

    await userEvent.click(screen.getByText("Confirm"));

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "group.scope.common.deactivation.success.message",
        "group.scope.common.deactivation.success.title",
      );
    });

    expect(onUpdate).toHaveBeenCalledTimes(1);
    expect(onClose).toHaveBeenCalledTimes(1);
  });

  it("should deactivate a root by selecting other", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    render(<WrapperComponent groupName={"unittesting"} />, {
      mocks: [...meGoupAndRootVulnsQueryMock, mutationMock],
    });

    await waitFor((): void => {
      expect(
        screen.queryByText("group.scope.common.deactivation.title"),
      ).toBeInTheDocument();
    });

    expect(screen.getByText("Confirm")).toBeDisabled();

    await userEvent.click(
      screen.getByText("group.scope.common.deactivation.reason.label"),
    );
    await userEvent.click(
      screen.getByText("group.scope.common.deactivation.reason.other"),
    );

    expect(
      screen.queryByText("group.scope.common.deactivation.warning"),
    ).toBeInTheDocument();

    expect(
      screen.queryByText("group.scope.common.deactivation.other"),
    ).toBeInTheDocument();

    await userEvent.type(
      screen.getByRole("textbox", { name: "other" }),
      "deactivation root other test",
    );

    expect(
      screen.queryByText("group.scope.common.deactivation.warning"),
    ).toBeInTheDocument();

    expect(screen.getByText("Confirm")).not.toBeDisabled();

    await userEvent.click(screen.getByText("Confirm"));

    const element = await screen.findByText(
      "group.scope.common.deactivation.confirm",
    );

    expect(element).toBeInTheDocument();

    await userEvent.click(screen.getAllByText("Confirm")[1]);

    expect(onUpdate).toHaveBeenCalledTimes(1);
    expect(onClose).toHaveBeenCalledTimes(1);
  });

  it("should only call the onClose event", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            { action: "integrates_api_mutations_move_root_mutate" },
          ])
        }
      >
        <WrapperComponent groupName={"unittesting"} />
      </authzPermissionsContext.Provider>,
      {
        mocks: [...meGoupAndRootVulnsQueryMock],
      },
    );

    await waitFor((): void => {
      expect(
        screen.queryByText("group.scope.common.deactivation.title"),
      ).toBeInTheDocument();
    });

    expect(screen.getByText("Confirm")).toBeDisabled();

    await userEvent.click(
      screen.getByText("group.scope.common.deactivation.reason.label"),
    );
    await userEvent.click(
      screen.getByText("group.scope.common.deactivation.reason.mistake"),
    );

    expect(screen.getByText("Confirm")).not.toBeDisabled();

    await userEvent.click(screen.getByText("Cancel"));

    await userEvent.click(
      screen.getByText("group.scope.common.deactivation.reason.label"),
    );
    await userEvent.click(
      screen.getByText("group.scope.common.deactivation.reason.moved"),
    );

    await userEvent.click(screen.getByText("Cancel"));

    await userEvent.click(
      screen.getByText("group.scope.common.deactivation.reason.label"),
    );
    await userEvent.click(
      screen.getByText("group.scope.common.deactivation.reason.other"),
    );

    await userEvent.click(screen.getByText("Cancel"));

    expect(onUpdate).toHaveBeenCalledTimes(0);
    expect(onClose).toHaveBeenCalledTimes(3);
  });

  it("shouldn't deactivate a root by selecting move to other group", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            { action: "integrates_api_mutations_move_root_mutate" },
          ])
        }
      >
        <WrapperComponent groupName={"TEST"} />
      </authzPermissionsContext.Provider>,
      {
        mocks: [
          ...meGoupAndRootVulnsQueryMock,
          mutationMock,
          moveRootMutationMock,
        ],
      },
    );

    await waitFor((): void => {
      expect(
        screen.queryByText("group.scope.common.deactivation.title"),
      ).toBeInTheDocument();
    });

    expect(screen.getByText("Confirm")).toBeDisabled();

    await userEvent.click(
      screen.getByText("group.scope.common.deactivation.reason.label"),
    );
    await userEvent.click(
      screen.getByText("group.scope.common.deactivation.reason.moved"),
    );

    expect(
      screen.queryByText("group.scope.common.changeWarning"),
    ).toBeInTheDocument();
    expect(
      screen.queryByText("group.scope.common.deactivation.targetGroup"),
    ).toBeInTheDocument();

    const targetGroup = screen.getByRole("combobox", {
      name: "targetGroupName",
    });

    await userEvent.type(targetGroup, "unittesting");

    expect(screen.getByText("Confirm")).not.toBeDisabled();

    await userEvent.click(screen.getByText("Confirm"));

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        "group.scope.common.errors.duplicateUrlInTargetGroup",
      );
    });
  });

  it("shouldn't deactivate a root", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();
    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            { action: "integrates_api_mutations_move_root_mutate" },
          ])
        }
      >
        <WrapperComponent
          groupName={"TEST"}
          rootId={"4039d098-ffc5-4984-8ed3-eb17bca98e20"}
        />
      </authzPermissionsContext.Provider>,
      {
        mocks: [
          ...meGoupAndRootVulnsQueryMock,
          mutationMock,
          moveRootMutationMock,
        ],
      },
    );

    await waitFor((): void => {
      expect(
        screen.queryByText("group.scope.common.deactivation.title"),
      ).toBeInTheDocument();
    });

    expect(screen.getByText("Confirm")).toBeDisabled();

    await userEvent.click(
      screen.getByText("group.scope.common.deactivation.reason.label"),
    );
    await userEvent.click(
      screen.getByText("group.scope.common.deactivation.reason.moved"),
    );

    expect(
      screen.queryByText("group.scope.common.changeWarning"),
    ).toBeInTheDocument();
    expect(
      screen.queryByText("group.scope.common.deactivation.targetGroup"),
    ).toBeInTheDocument();

    const targetGroup = screen.getByRole("combobox", {
      name: "targetGroupName",
    });

    await userEvent.type(targetGroup, "unittesting");

    expect(screen.getByText("Confirm")).not.toBeDisabled();

    await userEvent.click(screen.getByText("Confirm"));

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        "group.scope.common.errors.duplicateNickname",
      );
    });
  });
});
