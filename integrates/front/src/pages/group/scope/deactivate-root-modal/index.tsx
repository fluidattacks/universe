import { useMutation, useQuery } from "@apollo/client";
import { useAbility } from "@casl/react";
import {
  Alert,
  ComboBox,
  Form,
  InnerForm,
  Input,
  Modal,
  useConfirmDialog,
} from "@fluidattacks/design";
import type { Dictionary } from "lodash";
import { countBy, sum } from "lodash";
import { Fragment, StrictMode, useCallback, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import type { Schema } from "yup";
import { object, string } from "yup";

import type { IDeactivateRootModalProps, IGroup } from "./types";

import {
  DEACTIVATE_ROOT,
  GET_GROUPS,
  GET_ROOT_VULNS,
  MOVE_ROOT,
} from "../queries";
import { authzPermissionsContext } from "context/authz/config";
import { Technique, VulnerabilityState } from "gql/graphql";
import type { RootDeactivationReason } from "gql/graphql";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

export const DeactivateRootModal = ({
  groupName,
  modalRef,
  rootId,
  onUpdate,
}: IDeactivateRootModalProps): JSX.Element => {
  const { t } = useTranslation();
  const { confirm, ConfirmDialog } = useConfirmDialog();
  const { close, isOpen } = modalRef;
  const permissions = useAbility(authzPermissionsContext);
  const canMoveRoot = permissions.can(
    "integrates_api_mutations_move_root_mutate",
  );
  const prefix = "group.scope.common.deactivation.";

  const [countVulns, setCountVulns] = useState<Dictionary<number>>({});

  // Graphql operations
  const [deactivateRoot] = useMutation(DEACTIVATE_ROOT, {
    onCompleted: (): void => {
      close();
      onUpdate();
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        if (error.message === "Exception - Error empty value is not valid") {
          msgError(t(`${prefix}errors.changed`));
          onUpdate();
        } else {
          msgError(t("groupAlerts.errorTextsad"));
          Logger.error("Couldn't deactivate root", error);
        }
      });
    },
  });
  const [moveRoot] = useMutation(MOVE_ROOT, {
    onCompleted: (): void => {
      msgSuccess(t(`${prefix}success.message`), t(`${prefix}success.title`));
      close();
      onUpdate();
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        switch (error.message) {
          case "Exception - Root with the same nickname already exists":
            msgError(t("group.scope.common.errors.duplicateNickname"));
            break;
          case "Exception - Root with the same URL/branch already exists":
            msgError(t("group.scope.common.errors.duplicateUrlInTargetGroup"));
            break;
          case "Exception - Git repository was not accessible with given credentials":
            msgError(
              t(
                "group.scope.git.addGitRootFile.errors.invalidCredentialAccess",
              ),
            );
            break;
          default:
            msgError(t("groupAlerts.errorTextsad"));
            Logger.error("Couldn't move root", error);
        }
      });
    },
  });
  const { data } = useQuery(GET_GROUPS, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.error("Couldn't load group names", error);
      });
    },
    skip: !isOpen,
  });
  const { data: rootVulnsData } = useQuery(GET_ROOT_VULNS, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.error("Couldn't load root vulnerabilities", error);
      });
    },
    skip: !isOpen,
    variables: { groupName, rootId },
  });

  // Data management
  const groups =
    data === undefined
      ? []
      : data.me.organizations.reduce<IGroup[]>(
          (previousValue, currentValue): IGroup[] => [
            ...previousValue,
            ...(currentValue.groups as IGroup[]),
          ],
          [],
        );
  const currentGroup = groups.find(
    (group): boolean => group.name === groupName,
  );
  const suggestions =
    currentGroup === undefined
      ? []
      : groups
          .filter(
            (group): boolean =>
              group.name !== currentGroup.name &&
              group.organization === currentGroup.organization &&
              group.service === currentGroup.service,
          )
          .map((group): string => group.name);

  // Form management
  const handleSubmit = useCallback(
    async (values: Record<string, string>): Promise<void> => {
      if (values.reason === "MOVED_TO_ANOTHER_GROUP") {
        await moveRoot({
          variables: {
            groupName,
            id: rootId,
            targetGroupName: values.targetGroupName,
          },
        });
      } else {
        await deactivateRoot({
          variables: {
            groupName,
            id: rootId,
            other: values.other,
            reason: values.reason as RootDeactivationReason,
          },
        });
      }
    },
    [deactivateRoot, groupName, moveRoot, rootId],
  );
  const confirmAndSubmit = useCallback(
    async (values: Record<string, string>): Promise<void> => {
      if (
        ["OUT_OF_SCOPE", "REGISTERED_BY_MISTAKE", "OTHER"].includes(
          values.reason,
        )
      ) {
        const confirmResult = await confirm({
          message: t(`${prefix}confirm`),
          title: t("group.scope.common.confirm"),
        });

        if (confirmResult) return handleSubmit(values);

        return Promise.resolve();
      }

      return handleSubmit(values);
    },
    [confirm, handleSubmit, t],
  );
  const validationSchema = object().shape({
    other: string().when("reason", ([reason]: string[]): Schema => {
      return reason === "OTHER"
        ? string().required(t("validations.required"))
        : string();
    }),
    reason: string().required(t("validations.required")),
    targetGroupName: string().when("reason", ([reason]: string[]): Schema => {
      return reason === "MOVED_TO_ANOTHER_GROUP"
        ? string()
            .required(t("validations.required"))
            .oneOf(suggestions, t("validations.oneOf"))
        : string();
    }),
  });

  useEffect((): void => {
    const currentRootVulns = rootVulnsData?.root;
    setCountVulns(
      countBy(
        currentRootVulns?.vulnerabilities
          ?.filter(
            (item): boolean => item.state === VulnerabilityState.Vulnerable,
          )
          .map(
            (
              item,
            ): {
              __typename?: "Vulnerability";
              id: string;
              state: VulnerabilityState;
              technique: Technique | null;
              vulnerabilityType: string;
            } => ({
              ...item,
              technique: item.technique ?? Technique.Ptaas,
            }),
          ),
        "technique",
      ),
    );
  }, [rootId, rootVulnsData, setCountVulns]);

  return (
    <StrictMode>
      <Modal modalRef={modalRef} size={"sm"} title={t(`${prefix}title`)}>
        <Form
          cancelButton={{ onClick: close }}
          defaultValues={{ other: "", reason: "", targetGroupName: "" }}
          onSubmit={confirmAndSubmit}
          yupSchema={validationSchema}
        >
          <InnerForm>
            {({
              formState: { errors },
              getFieldState,
              control,
              register,
              watch,
            }): JSX.Element => {
              const reason = watch("reason");
              const otherState = getFieldState("other");
              const targetState = getFieldState("targetGroupName");

              return (
                <Fragment>
                  <ComboBox
                    control={control}
                    items={[
                      {
                        name: t(`${prefix}reason.mistake`),
                        value: "REGISTERED_BY_MISTAKE",
                      },
                      ...(canMoveRoot
                        ? [
                            {
                              name: t(`${prefix}reason.moved`),
                              value: "MOVED_TO_ANOTHER_GROUP",
                            },
                          ]
                        : []),
                      { name: t(`${prefix}reason.other`), value: "OTHER" },
                    ]}
                    label={t(`${prefix}reason.label`)}
                    name={"reason"}
                    placeholder={t(`${prefix}reason.placeholder`)}
                  />
                  {reason === "OTHER" ? (
                    <Input
                      error={errors.other?.message?.toString()}
                      isTouched={otherState.isTouched}
                      isValid={!otherState.invalid}
                      label={t(`${prefix}other`)}
                      name={"other"}
                      register={register}
                    />
                  ) : undefined}
                  {reason === "MOVED_TO_ANOTHER_GROUP" ? (
                    <Input
                      error={errors.targetGroupName?.message?.toString()}
                      isTouched={targetState.isTouched}
                      isValid={!targetState.invalid}
                      label={t(`${prefix}targetGroup`)}
                      name={"targetGroupName"}
                      placeholder={t(`${prefix}targetPlaceholder`)}
                      register={register}
                      suggestions={suggestions}
                    />
                  ) : undefined}
                  {["OUT_OF_SCOPE", "REGISTERED_BY_MISTAKE", "OTHER"].includes(
                    reason,
                  ) ? (
                    <Fragment>
                      <Alert>{t(`${prefix}warning`)}</Alert>
                      {sum(Object.values(countVulns)) > 0 && (
                        <Alert>
                          {t(`${prefix}closingVulnsTitle`)}
                          {Object.keys(countVulns).map(
                            (key): JSX.Element => (
                              <Fragment key={key}>
                                <br />
                                {countVulns[key] +
                                  t(`${prefix}closedVulnsWarning`, {
                                    technique: key,
                                  })}
                              </Fragment>
                            ),
                          )}
                        </Alert>
                      )}
                      <Alert>{t(`${prefix}closedEnvironmentWarning`)}</Alert>
                    </Fragment>
                  ) : undefined}
                  {reason === "MOVED_TO_ANOTHER_GROUP" ? (
                    <Alert>{t("group.scope.common.changeWarning")}</Alert>
                  ) : undefined}
                  <ConfirmDialog />
                </Fragment>
              );
            }}
          </InnerForm>
        </Form>
      </Modal>
    </StrictMode>
  );
};
