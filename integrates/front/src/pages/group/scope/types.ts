import type { RootCriticality } from "gql/graphql";

type TProvider = "" | "AZURE" | "BITBUCKET" | "GITHUB" | "GITLAB";

interface ISecret {
  createdAt: Date;
  description: string | null;
  key: string;
  owner: string;
  value: string;
}
interface ISecretItem {
  description: string;
  element: JSX.Element;
  key: string;
}
interface ISecretRow {
  description: string;
  key: string;
  owner?: string;
}
interface IEnvironmentUrl {
  cloudName: string | undefined;
  id: string;
  include: boolean;
  url: string;
  countSecrets?: number;
  secrets: ISecret[];
  createdAt: Date | null;
  createdBy: string | null;
  urlType: string;
}

interface IDockerImageAttr {
  createdAt: string | null;
  createdBy: string | null;
  include: boolean;
  root: {
    id: string;
    state: "ACTIVE" | "INACTIVE";
    url: string;
  };
  rootId: string;
  uri: string;
}

interface IEnvironmentUrlAttr {
  __typename?: "GitEnvironmentUrl";
  cloudName: string | null;
  createdAt: string | null;
  createdBy: string | null;
  id: string;
  countSecrets?: number;
  include: boolean;
  root: {
    id: string;
    state: "ACTIVE" | "INACTIVE";
    url: string;
  };
  rootId: string;
  url: string;
  urlType: string;
  useEgress: boolean;
  useVpn: boolean;
  useZtna: boolean;
}
interface IRootAttr {
  id: string;
  nickname: string;
}
interface IEventsData {
  closingDate: string;
  detail: string;
  eventDate: string;
  eventStatus: string;
  eventType: string;
  id: string;
  groupName: string;
  root: IRootAttr | null;
  environment: string | null;
}
interface IEnvironmentUrlData {
  cloudName: string | null;
  createdAt: string | null;
  createdBy: string | null;
  id: string;
  url: string;
  repositoryUrls: IEnvironmentGitRootData[];
  urlType: string;
  useEgress: boolean;
  useVpn: boolean;
  useZtna: boolean;
}
interface IBasicEnvironmentUrl {
  id: string;
  include: boolean;
  url: string;
  urlType: string;
  useEgress: boolean;
  useVpn: boolean;
  useZtna: boolean;
}

interface ICredentials {
  arn: string;
  auth: "" | "TOKEN" | "USER";
  azureOrganization: string;
  id: string;
  isPat: boolean;
  isToken: boolean;
  key: string;
  name: string;
  password: string;
  token: string;
  type: "" | "AWSROLE" | "HTTPS" | "SSH";
  typeCredential: "" | "AWSROLE" | "OAUTH" | "SSH" | "TOKEN" | "USER";
  user: string;
}
interface ICredentialsAttr {
  id: string;
  isPat: boolean;
  isToken: boolean;
  name: string;
  type: string;
}

interface IDockerImage {
  credentials?: ICredentials;
  id: string;
  uri: string;
  include: boolean;
}

type TCloningStatusType = "FAIL" | "INACTIVE" | "OK" | "QUEUED" | "UNKNOWN";

interface IGitRootAttr {
  __typename: "GitRoot";
  branch: string;
  cloningStatus: {
    message: string;
    status: TCloningStatusType;
  };
  credentials: {
    name: string;
  };
  criticality: RootCriticality;
  includesHealthCheck: boolean | string;
  id: string;
  nickname: string;
  state: "ACTIVE" | "INACTIVE";
  url: string;
}

interface IGitRootData {
  __typename: "GitRoot";
  branch: string;
  cloningStatus: {
    message: string;
    status: TCloningStatusType;
  };
  credentials: {
    name: string;
  };
  criticality: RootCriticality | null;
  gitEnvironmentUrls: IEnvironmentUrlAttr[];
  includesHealthCheck: boolean | string;
  id: string;
  nickname: string;
  state: "ACTIVE" | "INACTIVE";
  url: string;
}

interface IGitRootEventsData extends IGitRootData {
  eventId?: string;
  eventStatus?: string;
}

interface IManagementEnvironmentUrlAttr {
  __typename?: "GitEnvironmentUrl";
  id: string;
  rootId: string;
  url: string;
  urlType: string;
  secrets:
    | ({
        __typename?: "Secret";
        description: string | null;
        key: string;
        owner: string | null;
      } | null)[]
    | null;
}

interface IGitRootManagementDetailsAttr {
  __typename?: "GitRoot";
  branch: string;
  criticality: RootCriticality | null;
  gitignore: string[];
  id: string;
  includesHealthCheck: boolean;
  nickname: string;
  state: "ACTIVE" | "INACTIVE";
  url: string;
  useEgress: boolean;
  useVpn: boolean;
  useZtna: boolean;
  cloningStatus: {
    __typename?: "GitRootCloningStatus";
    message: string;
    status: TCloningStatusType;
  };
  credentials: ICredentials;
  gitEnvironmentUrls: IManagementEnvironmentUrlAttr[];
}

interface IEnvironmentGitRootData {
  root: IEnvironmentUrlAttr["root"];
  include: boolean;
  countSecrets?: number;
}

interface IIPRootAttr {
  __typename: "IPRoot";
  address: string;
  id: string;
  nickname: string;
  state: "ACTIVE" | "INACTIVE";
}

interface IURLRootAttr {
  __typename: "URLRoot";
  host: string;
  id: string;
  nickname: string;
  path: string;
  port: number;
  protocol: "HTTP" | "HTTPS";
  query: string | null;
  state: "ACTIVE" | "INACTIVE";
}

interface IUpdateGitEnvironments extends IGitRootAttr {
  reason?: string;
  other?: string;
}

type TRoot = IGitRootAttr | IIPRootAttr | IURLRootAttr;

interface IFormValues {
  branch: string;
  cloningStatus: {
    message: string;
    status: TCloningStatusType;
  };
  credentials: ICredentials;
  hasExclusions: string;
  gitEnvironmentUrls: IManagementEnvironmentUrlAttr[];
  gitignore: string[];
  healthCheckConfirm: string[] | undefined;
  includesHealthCheck: boolean | string;
  id: string;
  nickname: string;
  priority: RootCriticality | null;
  state: "ACTIVE" | "INACTIVE";
  url: string;
  useEgress: boolean;
  useVpn: boolean;
  useZtna: boolean;
}

interface IGroupFileAttr {
  description: string;
  uploader: string;
  uploadDate: string | null;
  fileName: string;
}

interface IGitRootsConnection {
  __typename?: "gitRootsConnection";
  total: number;
  edges: { node: IGitRootAttr }[];
  pageInfo: {
    __typename?: "PageInfo";
    endCursor: string;
    hasNextPage: boolean;
  };
}

interface IIPRootsConnection {
  __typename?: "IPRootsConnection";
  total: number;
  edges: { node: IIPRootAttr }[];
  pageInfo: {
    __typename?: "PageInfo";
    endCursor: string;
    hasNextPage: boolean;
  };
}

interface IURLRootsConnection {
  __typename?: "URLRootsConnection";
  total: number;
  edges: { node: IURLRootAttr }[];
  pageInfo: {
    __typename?: "PageInfo";
    endCursor: string;
    hasNextPage: boolean;
  };
}

export type {
  IBasicEnvironmentUrl,
  ICredentials,
  ICredentialsAttr,
  IDockerImage,
  IEnvironmentUrl,
  IEnvironmentGitRootData,
  IEventsData,
  IFormValues,
  IGitRootAttr,
  IGitRootData,
  IGitRootManagementDetailsAttr,
  IGitRootsConnection,
  IGroupFileAttr,
  IIPRootAttr,
  IIPRootsConnection,
  IManagementEnvironmentUrlAttr,
  ISecret,
  ISecretItem,
  ISecretRow,
  IUpdateGitEnvironments,
  IURLRootAttr,
  IEnvironmentUrlAttr,
  IEnvironmentUrlData,
  IURLRootsConnection,
  TProvider,
  IDockerImageAttr,
  TRoot,
  IGitRootEventsData,
};
