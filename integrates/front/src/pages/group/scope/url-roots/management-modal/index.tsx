import { useAbility } from "@casl/react";
import { Modal, useModal } from "@fluidattacks/design";
import isEmpty from "lodash/isEmpty";
import { useCallback, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { ExcludedSubPaths } from "./excluded-sub-paths";
import type { IManagementModalProps, TManagementTabs } from "./types";
import { Url } from "./url";

import { authzPermissionsContext } from "context/authz/config";
import { Secrets } from "pages/group/scope/secrets";

const ManagementModal: React.FC<IManagementModalProps> = ({
  groupName,
  initialValues = {
    __typename: "URLRoot",
    host: "",
    id: "",
    nickname: "",
    path: "",
    port: 0,
    protocol: "HTTPS",
    query: "",
    state: "ACTIVE",
  },
  onClose,
  onSubmit,
}): JSX.Element => {
  const permissions = useAbility(authzPermissionsContext);
  const canUpdateRootState = permissions.can(
    "integrates_api_mutations_update_url_root_mutate",
  );
  const canSeeSecrets = permissions.can(
    "integrates_api_resolvers_git_root_secrets_resolve",
  );
  const managementModal = useModal("management-modal");
  const { t } = useTranslation();
  const isEditing = initialValues.host !== "";

  const [currentTab, setCurrentTab] = useState<TManagementTabs>(
    (canUpdateRootState ? "url" : "secrets") as TManagementTabs,
  );

  const toTab = useCallback((event: React.MouseEvent): void => {
    event.preventDefault();

    const target = event.target as HTMLButtonElement;

    setCurrentTab(target.id as TManagementTabs);
  }, []);

  const tabContent: Record<TManagementTabs, JSX.Element> = {
    excludedSubPathsTab: (
      <ExcludedSubPaths
        groupName={groupName}
        initialValues={initialValues}
        onClose={onClose}
      />
    ),
    secrets: (
      <Secrets
        groupName={groupName}
        onCloseModal={onClose}
        resourceId={initialValues.id}
      />
    ),
    url: (
      <Url
        initialValues={initialValues}
        isEditing={isEditing}
        onClose={onClose}
        onSubmit={onSubmit}
      />
    ),
  };

  return (
    <Modal
      modalRef={{ ...managementModal, close: onClose, isOpen: true }}
      size={"sm"}
      tabItems={
        isEditing
          ? [
              ...(canUpdateRootState
                ? [
                    {
                      id: "url",
                      isActive: currentTab === "url",
                      label: t("group.scope.url.modal.title"),
                      link: "/url",
                      onClick: toTab,
                      tooltip: t("group.scope.url.modal.title"),
                    },
                  ]
                : []),
              ...(canSeeSecrets
                ? [
                    {
                      id: "secrets",
                      isActive: currentTab === "secrets",
                      label: "Secrets",
                      link: "/secrets",
                      onClick: toTab,
                    },
                  ]
                : []),
              ...(isEmpty(initialValues.query)
                ? [
                    {
                      id: "excludedSubPathsTab",
                      isActive: currentTab === "excludedSubPathsTab",
                      label: t("group.scope.url.modal.excludedSubPaths.title"),
                      link: "/excluded-sub-paths",
                      onClick: toTab,
                    },
                  ]
                : []),
            ]
          : undefined
      }
      title={t(`group.scope.common.${isEditing ? "edit" : "add"}`)}
    >
      {tabContent[currentTab]}
    </Modal>
  );
};

export { ManagementModal };
