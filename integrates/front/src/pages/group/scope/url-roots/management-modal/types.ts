import type { IURLRootAttr } from "../../types";

interface IManagementModalProps {
  readonly groupName: string;
  readonly initialValues: IURLRootAttr | undefined;
  readonly onClose: () => void;
  readonly onSubmit: (values: {
    id: string;
    nickname: string;
    url: string;
  }) => Promise<void>;
}

type TManagementTabs = "excludedSubPathsTab" | "secrets" | "url";

export type { IManagementModalProps, TManagementTabs };
