import { useMutation, useQuery } from "@apollo/client";
import { useAbility } from "@casl/react";
import { ModalConfirm, useModal } from "@fluidattacks/design";
import type { CellContext, ColumnDef } from "@tanstack/react-table";
import isUndefined from "lodash/isUndefined";
import { Fragment, useCallback, useState } from "react";
import { useTranslation } from "react-i18next";

import { AddExcludedSubPath } from "./add-excluded-sub-path";
import type { IExcludedSubPathsProps, ISubPathData } from "./types";

import {
  GET_URL_ROOT_EXCLUDED_SUB_PATHS,
  UPDATE_URL_ROOT_EXCLUDED_SUB_PATHS,
} from "../../../queries";
import { Table } from "components/table";
import { deleteFormatter } from "components/table/table-formatters";
import { authzPermissionsContext } from "context/authz/config";
import { useTable } from "hooks";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

const ExcludedSubPaths = ({
  initialValues,
  groupName,
  onClose,
}: IExcludedSubPathsProps): JSX.Element => {
  const { t } = useTranslation();
  const permissions = useAbility(authzPermissionsContext);
  const addExcludedPathModal = useModal("add-excluded-path-modal");

  const tableRef = useTable("tblGitRootSecrets");
  const canUpdateURLRoot = permissions.can(
    "integrates_api_mutations_update_url_root_mutate",
  );
  const [isAddModalOpen, setIsAddModalOpen] = useState(false);
  const closeAddModal = useCallback((): void => {
    setIsAddModalOpen(false);
    addExcludedPathModal.close();
  }, [addExcludedPathModal]);
  const openAddModal = useCallback((): void => {
    setIsAddModalOpen(true);
    addExcludedPathModal.open();
  }, [addExcludedPathModal]);

  const { data } = useQuery(GET_URL_ROOT_EXCLUDED_SUB_PATHS, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.error("Couldn't load url root excluded sub paths", error);
      });
    },
    variables: { groupName, rootId: initialValues.id },
  });
  const [updateURLRootExcludedSubPaths, { loading: updating }] = useMutation(
    UPDATE_URL_ROOT_EXCLUDED_SUB_PATHS,
    {
      onCompleted: (): void => {
        msgSuccess(
          t("group.scope.url.modal.excludedSubPaths.alerts.remove.success"),
          t("groupAlerts.titleSuccess"),
        );
      },
      onError: ({ graphQLErrors }): void => {
        graphQLErrors.forEach((error): void => {
          msgError(t("groupAlerts.errorTextsad"));
          Logger.warning(
            "An error occurred updating URL root excluded sub paths",
            error,
          );
        });
      },
      refetchQueries: [GET_URL_ROOT_EXCLUDED_SUB_PATHS],
    },
  );
  const handleDeleteExcludedSubPath = useCallback(
    (subPathData: ISubPathData | undefined): void => {
      if (isUndefined(subPathData) || isUndefined(data) || updating) {
        return;
      }
      if (data.root.__typename === "URLRoot") {
        const updatedExcludedSubPaths = data.root.excludedSubPaths.filter(
          (subPath): boolean => subPathData.subPath !== subPath,
        );
        void updateURLRootExcludedSubPaths({
          variables: {
            excludedSubPaths: updatedExcludedSubPaths,
            groupName,
            rootId: initialValues.id,
          },
        });
      }
    },
    [
      data,
      groupName,
      initialValues.id,
      updateURLRootExcludedSubPaths,
      updating,
    ],
  );
  const deleteAction = useCallback(
    (cell: CellContext<ISubPathData, unknown>): JSX.Element =>
      deleteFormatter(cell.row.original, handleDeleteExcludedSubPath),
    [handleDeleteExcludedSubPath],
  );
  const deleteColumn: ColumnDef<ISubPathData>[] = [
    {
      accessorKey: "subPath",
      cell: deleteAction,
      enableColumnFilter: false,
      header: t("searchFindings.tabDescription.action"),
      id: "remove",
    },
  ];
  const excludedSubPaths =
    data?.root.__typename === "URLRoot"
      ? data.root.excludedSubPaths.map((subPath): ISubPathData => ({ subPath }))
      : [];

  return (
    <Fragment>
      <Table
        columns={[
          {
            accessorKey: "subPath",
            header: String(t("group.scope.url.subPath")),
          },
          ...(canUpdateURLRoot ? deleteColumn : []),
        ]}
        data={excludedSubPaths}
        tableRef={tableRef}
      />
      {isAddModalOpen && data?.root.__typename === "URLRoot" ? (
        <AddExcludedSubPath
          excludedSubPaths={data.root.excludedSubPaths}
          groupName={groupName}
          modalRef={{
            ...addExcludedPathModal,
          }}
          onClose={closeAddModal}
          rootId={initialValues.id}
        />
      ) : undefined}
      {canUpdateURLRoot ? (
        <ModalConfirm
          id={"add-sub-path"}
          onCancel={onClose}
          onConfirm={openAddModal}
          txtConfirm={t(
            "group.scope.url.modal.excludedSubPaths.buttons.addSubPath",
          )}
        />
      ) : undefined}
    </Fragment>
  );
};

export { ExcludedSubPaths };
