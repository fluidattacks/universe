import { useMutation } from "@apollo/client";
import { useAbility } from "@casl/react";
import { Form, InnerForm, Input, Modal } from "@fluidattacks/design";
import { useCallback } from "react";
import { useTranslation } from "react-i18next";
import type { StringSchema } from "yup";
import { object, string } from "yup";

import type { IAddExcludedSubPathProps } from "./types";

import {
  GET_URL_ROOT_EXCLUDED_SUB_PATHS,
  UPDATE_URL_ROOT_EXCLUDED_SUB_PATHS,
} from "../../../../queries";
import { authzPermissionsContext } from "context/authz/config";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

const AddExcludedSubPath = ({
  excludedSubPaths,
  groupName,
  modalRef,
  rootId,
  onClose,
}: IAddExcludedSubPathProps): JSX.Element => {
  const { t } = useTranslation();
  const permissions = useAbility(authzPermissionsContext);
  const canUpdateURLRoot = permissions.can(
    "integrates_api_mutations_update_url_root_mutate",
  );
  const startsWithSlashRegex = /^(?!\/)/u;
  const endsWithSlashRegex = /(?<!\/)$/u;
  const validationSchema = object().shape({
    subPath: (string() as StringSchema<string>)
      .isValidValue(
        t("validations.invalidStartsWithSlash"),
        startsWithSlashRegex,
      )
      .isValidValue(t("validations.invalidEndsWithSlash"), endsWithSlashRegex)
      .required(t("validations.required")),
  });

  const [updateURLRootExcludedSubPaths, { loading: updating }] = useMutation(
    UPDATE_URL_ROOT_EXCLUDED_SUB_PATHS,
    {
      onCompleted: (): void => {
        msgSuccess(
          t("group.scope.url.modal.excludedSubPaths.alerts.add.success"),
          t("groupAlerts.titleSuccess"),
        );
        onClose();
      },
      onError: ({ graphQLErrors }): void => {
        graphQLErrors.forEach((error): void => {
          switch (error.message) {
            case "Exception - The sub-path is not valid":
              msgError(
                t(
                  "group.scope.url.modal.excludedSubPaths.alerts.add.invalidSubPath",
                ),
              );
              break;
            case "Exception - Unsanitized input found":
              msgError(t("validations.unsanitizedInputFound"));
              break;
            default:
              msgError(t("groupAlerts.errorTextsad"));
              Logger.warning(
                "An error occurred adding URL root excluded sub paths",
                error,
              );
          }
        });
      },
      refetchQueries: [GET_URL_ROOT_EXCLUDED_SUB_PATHS],
    },
  );

  const handleFormSubmit = useCallback(
    ({ subPath }: { subPath: string }): void => {
      const updatedExcludedSubPaths = [...excludedSubPaths, subPath];
      if (!updating) {
        void updateURLRootExcludedSubPaths({
          variables: {
            excludedSubPaths: updatedExcludedSubPaths,
            groupName,
            rootId,
          },
        });
      }
    },
    [
      excludedSubPaths,
      groupName,
      rootId,
      updateURLRootExcludedSubPaths,
      updating,
    ],
  );

  return (
    <Modal
      id={"addExcludedSubPa"}
      modalRef={modalRef}
      size={"md"}
      title={t("group.scope.url.modal.excludedSubPaths.addModal.title")}
    >
      <Form
        cancelButton={{ onClick: onClose }}
        confirmButton={{ disabled: !canUpdateURLRoot }}
        defaultValues={{ subPath: "" }}
        onSubmit={handleFormSubmit}
        yupSchema={validationSchema}
      >
        <InnerForm>
          {({ formState, getFieldState, register }): JSX.Element => {
            const { invalid, isTouched } = getFieldState("subPath");

            return (
              <Input
                error={formState.errors.subPath?.message?.toString()}
                isTouched={isTouched}
                isValid={!invalid}
                name={"subPath"}
                register={register}
                required={true}
              />
            );
          }}
        </InnerForm>
      </Form>
    </Modal>
  );
};

export { AddExcludedSubPath };
