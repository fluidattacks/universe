import { useMutation } from "@apollo/client";
import { Button, Text } from "@fluidattacks/design";
import mixpanel from "mixpanel-browser";
import { useCallback, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useNavigate, useParams } from "react-router-dom";

import { DeleteGroupModal } from "./delete-group-modal";
import { REMOVE_GROUP_MUTATION } from "./delete-group-modal/queries";
import type { IDeleteGroupValues } from "./delete-group-modal/types";

import {
  GET_GIT_ROOTS,
  GET_GROUP_DATA,
  GET_IP_ROOTS,
  GET_URL_ROOTS,
} from "../queries";
import type { RemoveGroupReason } from "gql/graphql";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

const DeleteGroup: React.FC = (): JSX.Element => {
  const { groupName } = useParams() as { groupName: string };
  const [isModalOpen, setIsModalOpen] = useState(false);
  const navigate = useNavigate();
  const { t } = useTranslation();
  const [values, setValues] = useState<IDeleteGroupValues>({
    comments: "",
    confirmation: "",
    reason: "DEFAULT",
  });

  const [removeGroupMutation, removeGroupMutationStatus] = useMutation(
    REMOVE_GROUP_MUTATION,
    {
      onCompleted: (): void => {
        msgSuccess(
          t("searchFindings.servicesTable.success"),
          t("searchFindings.servicesTable.successTitle"),
        );

        navigate("/home");
      },
      onError: (error): void => {
        error.graphQLErrors.forEach(({ message }): void => {
          if (message.includes("Exception - The group has pending actions")) {
            msgError(
              t(
                `searchFindings.servicesTable.deleteGroup.alerts.pendingActionsError`,
              ),
            );
            void removeGroupMutationStatus.client.refetchQueries({
              include: [
                GET_GIT_ROOTS,
                GET_IP_ROOTS,
                GET_URL_ROOTS,
                GET_GROUP_DATA,
              ],
            });
          } else if (
            message ===
            "Exception - The action is not allowed during the free trial"
          ) {
            msgError(
              t(
                `searchFindings.servicesTable.deleteGroup.alerts.trialRestrictionError`,
              ),
            );
          } else {
            Logger.error("An error occurred deleting group", error);
            msgError(t("groupAlerts.errorTextsad"));
          }
        });
      },
    },
  );

  const handleChange = useCallback((): void => {
    setIsModalOpen(!isModalOpen);
  }, [isModalOpen]);

  const handleSubmit = useCallback(
    (formikValues: IDeleteGroupValues): void => {
      setValues(formikValues);
      const { comments, reason } = formikValues;
      mixpanel.track("DeleteGroup");
      void removeGroupMutation({
        variables: { comments, groupName, reason: reason as RemoveGroupReason },
      });
      setIsModalOpen(!isModalOpen);
    },
    [groupName, isModalOpen, removeGroupMutation],
  );

  return (
    <React.Fragment>
      <Text size={"sm"}>
        {t("searchFindings.servicesTable.deleteGroup.warning")}
      </Text>
      <div>
        <Button
          id={"delete-group-button"}
          onClick={handleChange}
          variant={"secondary"}
        >
          {t("searchFindings.servicesTable.deleteGroup.deleteGroup")}
        </Button>
      </div>
      <DeleteGroupModal
        groupName={groupName}
        isOpen={isModalOpen}
        onClose={handleChange}
        onSubmit={handleSubmit}
        values={values}
      />
    </React.Fragment>
  );
};

export { DeleteGroup };
