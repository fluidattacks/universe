import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import type { AnyObject, InferType, Schema } from "yup";
import { ValidationError } from "yup";

import type { IDeleteGroupValues } from "./types";
import { validationSchema } from "./validations";

import { DeleteGroupModal } from ".";
import { CustomThemeProvider } from "components/colors";
import { render } from "mocks";

describe("delete Group Modal", (): void => {
  const testValidation = (
    groupName: string,
    obj: AnyObject,
  ): InferType<Schema> => {
    return validationSchema(groupName).validateSync(obj);
  };

  it("should render", async (): Promise<void> => {
    expect.hasAssertions();

    const values: IDeleteGroupValues = {
      comments: "",
      confirmation: "",
      reason: "DEFAULT",
    };

    const onClose: jest.Mock = jest.fn();
    const onSubmit: jest.Mock = jest.fn();
    render(
      <CustomThemeProvider>
        <DeleteGroupModal
          groupName={"TEST"}
          isOpen={true}
          onClose={onClose}
          onSubmit={onSubmit}
          values={values}
        />
      </CustomThemeProvider>,
    );

    expect(
      screen.queryByText(
        "searchFindings.servicesTable.deleteGroup.deleteGroup",
      ),
    ).toBeInTheDocument();

    await userEvent.click(screen.getByText("Cancel"));
    await waitFor((): void => {
      expect(onClose).toHaveBeenCalledTimes(1);
    });
  });

  it("should display required field errors of validation schema", (): void => {
    expect.hasAssertions();

    const groupName = "Unittesting";
    const errorString = "Required field";

    const testRequiredField = (): void => {
      testValidation(groupName, {});
    };
    const testCommentsRequiredField = (): void => {
      testValidation(groupName, { confirmation: groupName });
    };
    const testConfirmationRequiredField = (): void => {
      testValidation(groupName, { comments: "Testing group deletion" });
    };

    expect(testRequiredField).toThrow(new ValidationError(errorString));
    expect(testCommentsRequiredField).toThrow(new ValidationError(errorString));
    expect(testConfirmationRequiredField).toThrow(
      new ValidationError(errorString),
    );
  });

  it("should display comments field validation errors", (): void => {
    expect.hasAssertions();

    const groupName = "unittesting";

    const testInvalidTextBeginning = (): void => {
      testValidation(groupName, { comments: "=", confirmation: groupName });
    };
    const testInvalidTextField = (): void => {
      testValidation(groupName, {
        comments: "Test ^ invalid character",
        confirmation: groupName,
      });
    };

    expect(testInvalidTextBeginning).toThrow(
      new ValidationError(
        "Field cannot begin with the following character: '='",
      ),
    );
    expect(testInvalidTextField).toThrow(
      new ValidationError("Field cannot contain the following characters: '^'"),
    );
  });

  it("should display confirmation field validation errors", (): void => {
    expect.hasAssertions();

    const groupName = "unittesting";

    const testMatchGroupName = (): void => {
      testValidation(groupName, {
        comments: "Testing group deletion",
        confirmation: "test",
      });
    };

    expect(testMatchGroupName).toThrow(
      new ValidationError(`Expected: ${groupName}`),
    );
  });

  it("should display reason field validation errors", (): void => {
    expect.hasAssertions();

    const groupName = "unittesting";

    const testMatchDefaultOption = (): void => {
      testValidation(groupName, {
        comments: "Testing group deletion",
        confirmation: "unittesting",
        reason: "DEFAULT",
      });
    };

    expect(testMatchDefaultOption).toThrow(
      new ValidationError("Please select a valid option"),
    );
  });
});
