import { Toggle } from "@fluidattacks/design";
import { useCallback } from "react";

interface IChangeFormatterProps {
  readonly name: string;
  readonly row: Record<string, string>;
  readonly changeFunction: (arg: Record<string, string>) => void;
}

const ChangeFormatter = ({
  name,
  row,
  changeFunction,
}: IChangeFormatterProps): JSX.Element => {
  const handleOnChange = useCallback((): void => {
    changeFunction(row);
  }, [changeFunction, row]);

  return (
    <Toggle
      defaultChecked={
        !("state" in row) || row.state.toUpperCase() !== "INACTIVE"
      }
      leftDescription={{ off: "Inactive", on: "Active" }}
      leftDescriptionMinWidth={"50px"}
      name={name}
      onChange={handleOnChange}
    />
  );
};

export const changeFormatter = (
  name: string,
  row: Record<string, string>,
  changeFunction: (arg: Record<string, string>) => void,
): JSX.Element => {
  return (
    <ChangeFormatter changeFunction={changeFunction} name={name} row={row} />
  );
};
