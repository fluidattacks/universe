import type { IUseModal } from "@fluidattacks/design";

interface IUnsubscribeModalProps {
  groupName: string;
  modalRef: IUseModal;
  onSubmit: (values: { confirmation: string }) => void;
}

export type { IUnsubscribeModalProps };
