import { useMutation } from "@apollo/client";
import { Button, Text, useModal } from "@fluidattacks/design";
import mixpanel from "mixpanel-browser";
import { useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useNavigate, useParams } from "react-router-dom";

import { UnsubscribeModal } from "./modal";
import { UNSUBSCRIBE_FROM_GROUP_MUTATION } from "./modal/queries";

import { GET_ORGANIZATION_GROUP_NAMES } from "pages/group/group-selector/queries";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

const Unsubscribe: React.FC = (): JSX.Element => {
  const { t } = useTranslation();
  const { groupName } = useParams() as { groupName: string };
  const navigate = useNavigate();
  const unsubscribeModal = useModal("unsubscribe-modal");

  const [unsubscribeFromGroupMutation] = useMutation(
    UNSUBSCRIBE_FROM_GROUP_MUTATION,
    {
      onCompleted: (): void => {
        msgSuccess(
          t("searchFindings.servicesTable.unsubscribe.success", {
            groupName,
          }),
          t("searchFindings.servicesTable.unsubscribe.successTitle"),
        );

        navigate("/home");
      },
      onError: (error): void => {
        error.graphQLErrors.forEach((): void => {
          Logger.warning("An error occurred unsubscribing from group", error);
          msgError(t("groupAlerts.errorTextsad"));
        });
      },
      refetchQueries: [GET_ORGANIZATION_GROUP_NAMES],
      variables: {
        groupName,
      },
    },
  );

  const handleChange = useCallback((): void => {
    unsubscribeModal.open();
  }, [unsubscribeModal]);

  const handleSubmit = useCallback((): void => {
    mixpanel.track("UnsubscribeFromGroup");
    void unsubscribeFromGroupMutation();
    unsubscribeModal.close();
  }, [unsubscribeFromGroupMutation, unsubscribeModal]);

  return (
    <React.Fragment>
      <Text size={"sm"}>
        {t("searchFindings.servicesTable.unsubscribe.warning")}
      </Text>
      <div>
        <Button onClick={handleChange} variant={"secondary"}>
          {t("searchFindings.servicesTable.unsubscribe.button")}
        </Button>
      </div>
      <UnsubscribeModal
        groupName={groupName}
        modalRef={unsubscribeModal}
        onSubmit={handleSubmit}
      />
    </React.Fragment>
  );
};

export { Unsubscribe };
