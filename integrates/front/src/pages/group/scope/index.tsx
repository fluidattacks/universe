import { Container, Divider, GridContainer, Text } from "@fluidattacks/design";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";
import { useTheme } from "styled-components";

import { AccessInfo } from "./access-info";
import { AgentToken } from "./agent-token";
import { DeleteGroup } from "./delete-group";
import { Files } from "./files";
import { GitRoots } from "./git-roots";
import { GroupInformation } from "./info";
import { IPRoots } from "./ip-roots";
import { Portfolio } from "./portfolio";
import { Services } from "./services";
import { Unsubscribe } from "./unsubscribe";
import { URLRoots } from "./url-roots";

import { CardContainer } from "components/card-container";
import { Can } from "context/authz/can";
import { Have } from "context/authz/have";

export const GroupScope: React.FC = (): JSX.Element => {
  const { groupName, organizationName } = useParams() as {
    groupName: string;
    organizationName: string;
  };
  const { t } = useTranslation();
  const theme = useTheme();
  const textColor = theme.palette.gray[800];

  return (
    <Container bgColor={theme.palette.gray[50]} px={1.25} py={1.25}>
      <Have I={"has_service_white"}>
        <GitRoots groupName={groupName} organizationName={organizationName} />
        <Divider color={theme.palette.gray[200]} mb={1.25} mt={1.25} />
      </Have>
      <Have I={"has_service_black"}>
        <Text color={textColor} fontWeight={"bold"} mb={1.25} size={"xl"}>
          {t("group.scope.ip.title")}
        </Text>
        <IPRoots groupName={groupName} />
        <Divider color={theme.palette.gray[200]} mb={1.25} mt={1.25} />
        <Text color={textColor} fontWeight={"bold"} mb={1.25} size={"xl"}>
          {t("group.scope.url.title")}
        </Text>
        <URLRoots groupName={groupName} />
        <Divider color={theme.palette.gray[200]} mb={1.25} mt={1.25} />
      </Have>
      <div id={"resources"}>
        <Text
          color={textColor}
          fontWeight={"bold"}
          mb={1.25}
          mt={1.25}
          size={"xl"}
        >
          {t("searchFindings.tabResources.files.title")}
        </Text>
        <Files groupName={groupName} organizationName={organizationName} />
        <Divider color={theme.palette.gray[200]} mb={1.25} mt={1.25} />
        <Text
          color={textColor}
          fontWeight={"bold"}
          mb={1.25}
          mt={1.25}
          size={"xl"}
        >
          {t("searchFindings.tabResources.tags.title")}
        </Text>
        <Portfolio groupName={groupName} />
        <Divider color={theme.palette.gray[200]} mb={1.25} mt={1.25} />
        <Can do={"see_group_services_info"}>
          <Text
            color={textColor}
            fontWeight={"bold"}
            mb={1.25}
            mt={1.25}
            size={"xl"}
          >
            {t("searchFindings.servicesTable.services")}
          </Text>
          <Services groupName={groupName} organizationName={organizationName} />
          <Divider color={theme.palette.gray[200]} mb={1.25} mt={1.25} />
        </Can>
        <Text
          color={textColor}
          fontWeight={"bold"}
          mb={1.25}
          mt={1.25}
          size={"xl"}
        >
          {t("searchFindings.infoTable.title")}
        </Text>
        <GroupInformation />
        <Divider color={theme.palette.gray[200]} mb={1.25} mt={1.25} />
        <Text
          color={textColor}
          fontWeight={"bold"}
          mb={1.25}
          mt={1.25}
          size={"xl"}
        >
          {t("group.scope.settings.title")}
        </Text>
        <AccessInfo />
        <GridContainer lg={3} md={3} my={2} sm={2} xl={3}>
          <Can do={"integrates_api_resolvers_group_forces_token_resolve"}>
            <Have I={"has_forces"}>
              <CardContainer justify={"space-between"} px={1.25} py={1.25}>
                <Text
                  color={theme.palette.gray[800]}
                  fontWeight={"bold"}
                  size={"md"}
                >
                  {t("searchFindings.agentTokenSection.title")}
                </Text>
                <AgentToken groupName={groupName} />
              </CardContainer>
            </Have>
          </Can>
          <Can do={"integrates_api_mutations_unsubscribe_from_group_mutate"}>
            <CardContainer justify={"space-between"} px={1.25} py={1.25}>
              <Text
                color={theme.palette.gray[800]}
                fontWeight={"bold"}
                size={"md"}
              >
                {t("searchFindings.servicesTable.unsubscribe.title")}
              </Text>
              <Unsubscribe />
            </CardContainer>
          </Can>
          <Can do={"integrates_api_mutations_remove_group_mutate"}>
            <CardContainer justify={"space-between"} px={1.25} py={1.25}>
              <Text
                color={theme.palette.gray[800]}
                fontWeight={"bold"}
                size={"md"}
              >
                {t("searchFindings.servicesTable.deleteGroup.deleteGroup")}
              </Text>
              <DeleteGroup />
            </CardContainer>
          </Can>
        </GridContainer>
      </div>
    </Container>
  );
};
