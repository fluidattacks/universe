import type { PureAbility } from "@casl/ability";
import { Fragment } from "react";
import { useTranslation } from "react-i18next";

import { CardWithDropdown } from "components/card/card-with-dropdown";
import type { ICardWithDropdown } from "components/card/card-with-dropdown/types";
import { CardWithInput } from "components/card/card-with-input";
import type { ICardWithInputProps } from "components/card/card-with-input/types";
import { Language, ManagedType } from "gql/graphql";

const Cards = ({
  permissions,
}: Readonly<{
  permissions: PureAbility<string>;
}>): JSX.Element => {
  const { t } = useTranslation();

  const cardsWithInput: (ICardWithInputProps & {
    id: string;
    tooltip: string;
  })[] = [
    {
      description: t("organization.tabs.groups.newGroup.businessId.text"),
      disabled: permissions.cannot(
        "integrates_api_mutations_update_group_stakeholder_mutate",
      ),
      id: "information-businessId",
      name: "businessId",
      tooltip: t("organization.tabs.groups.newGroup.businessId.tooltip"),
    },
    {
      description: t("organization.tabs.groups.newGroup.businessName.text"),
      disabled: permissions.cannot(
        "integrates_api_mutations_update_group_stakeholder_mutate",
      ),
      id: "information-businessName",
      name: "businessName",
      tooltip: t("organization.tabs.groups.newGroup.businessName.tooltip"),
    },
    {
      description: t("organization.tabs.groups.newGroup.description.text"),
      disabled: permissions.cannot(
        "integrates_api_mutations_update_group_stakeholder_mutate",
      ),
      id: "information-description",
      name: "description",
      tooltip: t("organization.tabs.groups.newGroup.description.tooltip"),
    },
    {
      description: t("organization.tabs.groups.newGroup.sprintDuration.text"),
      disabled: permissions.cannot(
        "integrates_api_mutations_update_group_stakeholder_mutate",
      ),
      id: "information-sprintDuration",
      name: "sprintDuration",
      tooltip: t("organization.tabs.groups.newGroup.sprintDuration.tooltip"),
    },
    {
      description: t("organization.tabs.groups.editGroup.sprintStartDate.text"),
      disabled: permissions.cannot(
        "integrates_api_mutations_update_group_stakeholder_mutate",
      ),
      id: "information-sprintStartDate",
      inputType: "date",
      name: "sprintStartDate",
      tooltip: t("organization.tabs.groups.editGroup.sprintStartDate.tooltip"),
    },
  ];

  const cardsWithSelect: ICardWithDropdown[] = [
    {
      disabled: permissions.cannot(
        "integrates_api_mutations_update_group_stakeholder_mutate",
      ),
      id: "information-language",
      items: [
        {
          header: t("organization.tabs.groups.newGroup.language.EN"),
          value: Language.En,
        },
        {
          header: t("organization.tabs.groups.newGroup.language.ES"),
          value: Language.Es,
        },
      ],
      name: "language",
      required: true,
      title: t("organization.tabs.groups.newGroup.language.text"),
      tooltip: t("organization.tabs.groups.newGroup.language.tooltip"),
    },
    {
      disabled: permissions.cannot(
        "integrates_api_mutations_update_group_managed_mutate",
      ),
      id: "information-managed",
      items: [
        {
          header: t("organization.tabs.groups.newGroup.managed.managed"),
          value: ManagedType.Managed,
        },
        {
          header: t("organization.tabs.groups.newGroup.managed.notManaged"),
          value: ManagedType.NotManaged,
        },
        {
          header: t("organization.tabs.groups.newGroup.managed.underReview"),
          value: ManagedType.UnderReview,
        },
        {
          header: t("organization.tabs.groups.newGroup.managed.trial"),
          value: ManagedType.Trial,
        },
      ],
      name: "managed",
      required: true,
      title: t("organization.tabs.groups.newGroup.managed.text"),
      tooltip: t("organization.tabs.groups.newGroup.managed.tooltip"),
    },
  ];

  return (
    <Fragment>
      {cardsWithInput.map(
        ({
          description,
          disabled,
          id,
          inputType,
          name,
          tooltip,
        }): JSX.Element => (
          <CardWithInput
            disabled={disabled}
            id={id}
            inputType={inputType}
            key={name}
            name={name}
            title={description}
            tooltip={tooltip}
          />
        ),
      )}
      {cardsWithSelect.map(
        ({
          disabled,
          id,
          title,
          name,
          required,
          items,
          tooltip,
        }): JSX.Element => (
          <CardWithDropdown
            disabled={disabled}
            id={id}
            items={items}
            key={name}
            minHeight={"auto"}
            minWidth={"auto"}
            name={name}
            required={required}
            title={title}
            tooltip={tooltip}
            variant={"select"}
          />
        ),
      )}
    </Fragment>
  );
};

export { Cards };
