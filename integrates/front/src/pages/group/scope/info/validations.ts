import { object, string } from "yup";

import { translate } from "utils/translations/translate";

const MAX_BUSINESS_INFO_LENGTH = 60;
const MAX_DESCRIPTION_LENGTH = 200;
const min = 1;
const max = 10;

const validationSchema = object().shape({
  businessId: string()
    .max(
      MAX_BUSINESS_INFO_LENGTH,
      translate.t("validations.maxLength", { count: MAX_BUSINESS_INFO_LENGTH }),
    )
    .isValidTextBeginning()
    .isValidTextField()
    .nullable(),
  businessName: string()
    .max(
      MAX_BUSINESS_INFO_LENGTH,
      translate.t("validations.maxLength", { count: MAX_BUSINESS_INFO_LENGTH }),
    )
    .isValidTextBeginning()
    .isValidTextField()
    .nullable(),
  description: string()
    .nullable()
    .required(translate.t("validations.required"))
    .max(
      MAX_DESCRIPTION_LENGTH,
      translate.t("validations.maxLength", { count: MAX_DESCRIPTION_LENGTH }),
    )
    .isValidTextBeginning()
    .isValidTextField(),
  sprintDuration: string()
    .matches(/^\d+$/u, translate.t("validations.numeric"))
    .isValidFunction(
      (value: string | undefined): boolean => {
        if (value === undefined) {
          return false;
        }

        return Number(value) >= min && Number(value) <= max;
      },
      translate.t("validations.between", { max, min }),
    ),
  sprintStartDate: string()
    .required(translate.t("validations.required"))
    .isValidDate("greaterDate"),
});

export { validationSchema };
