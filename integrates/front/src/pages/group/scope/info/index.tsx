import { useMutation, useQuery } from "@apollo/client";
import { useAbility } from "@casl/react";
import { Button, GridContainer } from "@fluidattacks/design";
import dayjs from "dayjs";
import { Form, Formik } from "formik";
import _ from "lodash";
import mixpanel from "mixpanel-browser";
import { useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";

import { Cards } from "./cards";
import { validationSchema } from "./validations";

import { GET_GROUP_DATA, UPDATE_GROUP_INFO } from "../queries";
import { authzPermissionsContext } from "context/authz/config";
import type { Language, ManagedType } from "gql/graphql";
import { handleEditGroupDataError } from "pages/group/scope/services/utils";
import { formatDate } from "utils/date";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

const attributeMapper = (attribute: string): string => {
  /**
   * Needed for the new attribute headers of the dataset that do not match
   * the underlying field names
   */
  if (attribute === "Business registration number") {
    return "businessId";
  } else if (attribute === "Business name") {
    return "businessName";
  } else if (attribute === "Sprint length") {
    return "sprintDuration";
  } else if (attribute === "Sprint start date") {
    return "sprintStartDate";
  } else if (attribute === "Managed") {
    return "managed";
  }

  return attribute.toLocaleLowerCase();
};

const GroupInformation: React.FC = (): JSX.Element => {
  const { groupName } = useParams() as { groupName: string };
  const { t } = useTranslation();
  const permissions = useAbility(authzPermissionsContext);

  const formatDataSet = (
    attributes: {
      attribute: string;
      value: string;
    }[],
  ): Record<string, string> => {
    return attributes.reduce(
      (
        acc: Record<string, string>,
        cur: {
          attribute: string;
          value: string;
        },
      ): Record<string, string> => ({
        ...acc,
        [attributeMapper(cur.attribute)]: cur.value,
      }),
      {},
    );
  };

  const {
    data,
    loading: loadingGroupData,
    refetch: refetchGroupData,
  } = useQuery(GET_GROUP_DATA, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.warning("An error occurred getting group data", error);
      });
    },
    variables: { groupName },
  });

  const [editGroupInfo] = useMutation(UPDATE_GROUP_INFO, {
    onCompleted: (): void => {
      mixpanel.track("EditGroupData");
      msgSuccess(
        t("groupAlerts.groupInfoUpdated"),
        t("groupAlerts.titleSuccess"),
      );
      refetchGroupData({ groupName }).catch((): void => {
        Logger.error("An error occurred editing group alert");
      });
    },
    onError: (error): void => {
      handleEditGroupDataError(error);
    },
  });

  const handleFormSubmit = useCallback(
    async (values: Record<string, string>): Promise<void> => {
      const isManagedChanged =
        data === undefined
          ? false
          : (values.managed as ManagedType) !== data.group.managed;
      await editGroupInfo({
        variables: {
          businessId: values.businessId,
          businessName: values.businessName,
          comments: "",
          description: values.description,
          groupName,
          isManagedChanged,
          language: values.language as Language,
          managed: values.managed as ManagedType,
          sprintDuration: Number(values.sprintDuration),
          sprintStartDate: dayjs(values.sprintStartDate).toISOString(),
        },
      });
    },
    [data, editGroupInfo, groupName],
  );

  if (_.isUndefined(data) || _.isEmpty(data)) {
    return <div />;
  }
  const attributesDataset: {
    attribute: string;
    value: string;
  }[] = [
    {
      attribute: "Language",
      value: data.group.language,
    },
    {
      attribute: "Description",
      value: data.group.description ?? "",
    },
    {
      attribute: "Business registration number",
      value: data.group.businessId ?? "",
    },
    {
      attribute: "Business name",
      value: data.group.businessName ?? "",
    },
    {
      attribute: "Managed",
      value: data.group.managed,
    },
    {
      attribute: "Sprint length",
      value: String(data.group.sprintDuration),
    },
    {
      attribute: "Sprint start date",
      value: formatDate(data.group.sprintStartDate),
    },
  ];

  return (
    <React.StrictMode>
      <Formik
        enableReinitialize={true}
        initialValues={formatDataSet(attributesDataset)}
        name={"editGroupInformation"}
        onSubmit={handleFormSubmit}
        validationSchema={validationSchema}
      >
        {({ dirty, isSubmitting }): JSX.Element => {
          return (
            <Form>
              <GridContainer lg={3} md={3} sm={2} xl={3}>
                <Cards permissions={permissions} />
              </GridContainer>
              <div className={"mt2"} />
              {!dirty || loadingGroupData || isSubmitting ? undefined : (
                <Button type={"submit"} variant={"primary"}>
                  {t("searchFindings.servicesTable.modal.continue")}
                </Button>
              )}
            </Form>
          );
        }}
      </Formik>
    </React.StrictMode>
  );
};

export { GroupInformation };
