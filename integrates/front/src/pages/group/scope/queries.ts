/* eslint-disable max-lines */
import { graphql } from "gql";

const GET_ROOTS = graphql(`
  query GetRootNicknames($groupName: String!) {
    group(groupName: $groupName) {
      name
      roots {
        ... on GitRoot {
          id
          nickname
          state
        }
        ... on IPRoot {
          id
          nickname
          state
        }
        ... on URLRoot {
          id
          nickname
          state
        }
      }
    }
  }
`);

const GET_GIT_ROOT_DOCKER_IMAGES = graphql(`
  query GetGitRootDockerImages($groupName: String!, $rootId: ID!) {
    root(groupName: $groupName, rootId: $rootId) {
      ... on GitRoot {
        id
        dockerImages {
          include
          uri
          credentials {
            id
            azureOrganization
            isPat
            isToken
            name
            oauthType
            owner
            type
          }
        }
      }
    }
  }
`);

const GET_GROUP_DOCKER_IMAGES = graphql(`
  query GetGroupDockerImages($groupName: String!) {
    group(groupName: $groupName) {
      name
      dockerImages {
        createdAt
        createdBy
        include
        rootId
        uri
        root {
          id
          state
          url
        }
      }
    }
  }
`);

const GET_GIT_ROOTS_FRAGMENT = graphql(`
  fragment GetGitRootsViewFragment on Query {
    group(groupName: $groupName) {
      name
      gitRoots(
        after: $after
        branch: $branch
        cloningStatus: $cloningStatus
        first: $first
        includesHealthCheck: $includesHealthCheck
        nickname: $nickname
        search: $search
        state: $state
      ) {
        total
        edges {
          node {
            id
            branch
            criticality
            includesHealthCheck
            nickname
            state
            url
            cloningStatus {
              message
              status
            }
            credentials {
              name
            }
          }
        }
        pageInfo {
          endCursor
          hasNextPage
        }
      }
    }
  }
`);

const GET_IP_ROOTS_FRAGMENT = graphql(`
  fragment GetIPRootsViewFragment on Query {
    group(groupName: $groupName) {
      name
      ipRoots(after: $after, first: $first, search: $search) {
        total
        edges {
          node {
            id
            address
            nickname
            state
          }
        }
        pageInfo {
          endCursor
          hasNextPage
        }
      }
    }
  }
`);

const GET_URL_ROOTS_FRAGMENT = graphql(`
  fragment GetURLRootsViewFragment on Query {
    group(groupName: $groupName) {
      name
      urlRoots(after: $after, first: $first, search: $search) {
        total
        edges {
          node {
            id
            host
            nickname
            path
            port
            protocol
            query
            state
          }
        }
        pageInfo {
          endCursor
          hasNextPage
        }
      }
    }
  }
`);

const GET_GROUP_ENV_URLS_FRAGMENT = graphql(`
  fragment GetRootsEnvironmentUrls on Query {
    group(groupName: $groupName) {
      name
      gitEnvironmentUrls {
        id
        cloudName
        countSecrets @include(if: $canGetSecrets)
        createdAt
        createdBy
        include
        rootId
        url
        urlType
        useEgress
        useVpn
        useZtna
        root {
          id
          state
          url
        }
      }
    }
  }
`);

const GET_ROOT_SECRETS = graphql(`
  query GetRoot($groupName: String!, $rootId: ID!) {
    root(groupName: $groupName, rootId: $rootId) {
      ... on GitRoot {
        id
        secrets {
          description
          key
          owner
        }
      }
      ... on URLRoot {
        id
        secrets {
          description
          key
          owner
        }
      }
    }
  }
`);

const GET_ROOT_ENVIRONMENT_URLS = graphql(`
  query GetRootEnvironmentUrls($groupName: String!, $rootId: ID!) {
    root(groupName: $groupName, rootId: $rootId) {
      ... on GitRoot {
        id
        gitEnvironmentUrls {
          id
          include
          rootId
          url
          urlType
          useEgress
          useVpn
          useZtna
        }
      }
    }
  }
`);

const GET_ROOT_ENVIRONMENT_URLS_SECRETS = graphql(`
  query GetRootEnvironmentUrlsSecrets($groupName: String!, $rootId: ID!) {
    root(groupName: $groupName, rootId: $rootId) {
      ... on GitRoot {
        gitEnvironmentUrls {
          id
          rootId
          url
          urlType
          secrets {
            description
            key
          }
        }
      }
    }
  }
`);

const GET_GIT_ROOT_DETAILS = graphql(`
  query GetGitRootDetails($groupName: String!, $rootId: ID!) {
    root(groupName: $groupName, rootId: $rootId) {
      ... on GitRoot {
        id
        gitignore
        lastStateStatusUpdate
        cloningStatus {
          message
          modifiedDate
          status
        }
      }
    }
  }
`);

const GET_GIT_ROOT_MANAGEMENT_DETAILS = graphql(`
  query GetGitRootManagementDetails($groupName: String!, $rootId: ID!) {
    root(groupName: $groupName, rootId: $rootId) {
      ... on GitRoot {
        id
        branch
        criticality
        gitignore
        includesHealthCheck
        nickname
        state
        url
        useEgress
        useVpn
        useZtna
        cloningStatus {
          message
          status
        }
        credentials {
          id
          isToken
          name
          type
        }
        gitEnvironmentUrls {
          id
          rootId
          url
          urlType
          secrets {
            description
            key
            owner
          }
        }
      }
    }
  }
`);

const GET_ORGANIZATION_CREDENTIALS = graphql(`
  query GetOrganizationCredentials($organizationId: String!) {
    organization(organizationId: $organizationId) {
      __typename
      name
      credentials {
        id
        __typename
        azureOrganization
        isPat
        isToken
        name
        oauthType
        owner
        type
      }
    }
  }
`);

const GET_ORGANIZATION_EXTERNAL_ID = graphql(`
  fragment GetOrganizationExternalId on Query {
    organizationId(organizationName: $organizationName) {
      __typename
      awsExternalId
      name
    }
  }
`);

const GET_ORGANIZATION_ID = graphql(`
  query GetOrganizationId($organizationName: String!) {
    organizationId(organizationName: $organizationName) {
      id
      name
    }
  }
`);

const GET_URL_ROOT_EXCLUDED_SUB_PATHS = graphql(`
  query GetUrlRootExcludedSubPaths($groupName: String!, $rootId: ID!) {
    root(groupName: $groupName, rootId: $rootId) {
      ... on URLRoot {
        id
        excludedSubPaths
      }
    }
  }
`);

const ADD_GIT_ROOT = graphql(`
  mutation AddGitRoot(
    $branch: String!
    $credentials: RootCredentialsInput
    $criticality: RootCriticality
    $gitignore: [String!]
    $groupName: String!
    $includesHealthCheck: Boolean!
    $nickname: String!
    $url: String!
    $useEgress: Boolean!
    $useVpn: Boolean!
    $useZtna: Boolean!
  ) {
    addGitRoot(
      branch: $branch
      credentials: $credentials
      criticality: $criticality
      gitignore: $gitignore
      groupName: $groupName
      includesHealthCheck: $includesHealthCheck
      nickname: $nickname
      url: $url
      useEgress: $useEgress
      useVpn: $useVpn
      useZtna: $useZtna
    ) {
      success
    }
  }
`);

const ADD_SECRET = graphql(`
  mutation AddSecret(
    $description: String
    $groupName: String!
    $key: String!
    $resourceId: ID!
    $resourceType: ResourceType!
    $value: String!
  ) {
    addSecret(
      description: $description
      groupName: $groupName
      key: $key
      resourceId: $resourceId
      resourceType: $resourceType
      value: $value
    ) {
      success
    }
  }
`);

const ADD_ENVIRONMENT_URL = graphql(`
  mutation AddGitEnvironmentUrl(
    $groupName: String!
    $isProduction: Boolean
    $rootId: ID!
    $url: String!
    $urlType: GitEnvironmentCloud!
    $useEgress: Boolean
    $useVpn: Boolean
    $useZtna: Boolean
  ) {
    addGitEnvironmentUrl(
      groupName: $groupName
      isProduction: $isProduction
      rootId: $rootId
      url: $url
      urlType: $urlType
      useEgress: $useEgress
      useVpn: $useVpn
      useZtna: $useZtna
    ) {
      success
      urlId
    }
  }
`);

const ADD_ENVIRONMENT_CSPM = graphql(`
  mutation AddGitEnvironmentCspm(
    $azureClientId: String
    $azureClientSecret: String
    $azureSubscriptionId: String
    $azureTenantId: String
    $cloudName: String!
    $gcpPrivateKey: String
    $groupName: String!
    $rootId: ID!
    $url: String!
    $useEgress: Boolean
    $useVpn: Boolean
    $useZtna: Boolean
  ) {
    addGitEnvironmentCspm(
      azureClientId: $azureClientId
      azureClientSecret: $azureClientSecret
      azureSubscriptionId: $azureSubscriptionId
      azureTenantId: $azureTenantId
      cloudName: $cloudName
      gcpPrivateKey: $gcpPrivateKey
      groupName: $groupName
      rootId: $rootId
      url: $url
      useEgress: $useEgress
      useVpn: $useVpn
      useZtna: $useZtna
    ) {
      success
      urlId
    }
  }
`);

const ADD_ENVIRONMENT_INTEGRITY_EVENT = graphql(`
  mutation addEnvironmentIntegrityEvent($groupName: String!, $urlId: String!) {
    addEnvironmentIntegrityEvent(groupName: $groupName, urlId: $urlId) {
      success
    }
  }
`);

const DOWNLOAD_GIT_ROOTS_FILE_MUTATION = graphql(`
  mutation downloadGitRootsFile($groupName: String!) {
    downloadGitRootsFile(groupName: $groupName) {
      success
      url
    }
  }
`);

const REMOVE_SECRET = graphql(`
  mutation RemoveSecret(
    $groupName: String!
    $key: String!
    $resourceId: ID!
    $resourceType: ResourceType!
  ) {
    removeSecret(
      groupName: $groupName
      key: $key
      resourceId: $resourceId
      resourceType: $resourceType
    ) {
      success
    }
  }
`);

const UPDATE_GIT_ROOT = graphql(`
  mutation UpdateGitRoot(
    $id: ID!
    $branch: String!
    $credentials: RootCredentialsInput
    $criticality: RootCriticality
    $gitignore: [String!]
    $groupName: String!
    $includesHealthCheck: Boolean!
    $nickname: String
    $url: String!
    $useEgress: Boolean!
    $useVpn: Boolean!
    $useZtna: Boolean!
  ) {
    updateGitRoot(
      id: $id
      branch: $branch
      credentials: $credentials
      criticality: $criticality
      gitignore: $gitignore
      groupName: $groupName
      includesHealthCheck: $includesHealthCheck
      nickname: $nickname
      url: $url
      useEgress: $useEgress
      useVpn: $useVpn
      useZtna: $useZtna
    ) {
      success
    }
  }
`);

const UPDATE_IP_ROOT = graphql(`
  mutation UpdateIpRoot($groupName: String!, $nickname: String!, $rootId: ID!) {
    updateIpRoot(groupName: $groupName, nickname: $nickname, rootId: $rootId) {
      success
    }
  }
`);

const UPDATE_URL_ROOT = graphql(`
  mutation UpdateUrlRoot(
    $groupName: String!
    $nickname: String!
    $rootId: ID!
  ) {
    updateUrlRoot(groupName: $groupName, nickname: $nickname, rootId: $rootId) {
      success
    }
  }
`);

const ADD_IP_ROOT = graphql(`
  mutation AddIpRoot(
    $address: String!
    $groupName: String!
    $nickname: String!
  ) {
    addIpRoot(address: $address, groupName: $groupName, nickname: $nickname) {
      success
    }
  }
`);

const ADD_URL_ROOT = graphql(`
  mutation AddUrlRoot($groupName: String!, $nickname: String!, $url: String!) {
    addUrlRoot(groupName: $groupName, nickname: $nickname, url: $url) {
      success
    }
  }
`);

const ACTIVATE_ROOT = graphql(`
  mutation ActivateRoot($id: ID!, $groupName: String!) {
    activateRoot(id: $id, groupName: $groupName) {
      success
    }
  }
`);

const DEACTIVATE_ROOT = graphql(`
  mutation DeactivateRoot(
    $id: ID!
    $groupName: String!
    $other: String
    $reason: RootDeactivationReason!
  ) {
    deactivateRoot(
      id: $id
      groupName: $groupName
      other: $other
      reason: $reason
    ) {
      success
    }
  }
`);

const MOVE_ROOT = graphql(`
  mutation MoveRoot($id: ID!, $groupName: String!, $targetGroupName: String!) {
    moveRoot(
      id: $id
      groupName: $groupName
      targetGroupName: $targetGroupName
    ) {
      success
    }
  }
`);

const MOVE_ENVIRONMENT = graphql(`
  mutation MoveEnvironment(
    $groupName: String!
    $rootId: ID!
    $targetGroupName: String!
    $targetRootId: ID!
    $urlId: String!
  ) {
    moveEnvironment(
      groupName: $groupName
      rootId: $rootId
      targetGroupName: $targetGroupName
      targetRootId: $targetRootId
      urlId: $urlId
    ) {
      success
    }
  }
`);

const GET_GROUPS = graphql(`
  query GetGroups {
    me {
      userEmail
      organizations {
        name
        groups {
          name
          organization
          service
        }
      }
    }
  }
`);

const GET_ENVIRONMENT_URL = graphql(`
  query GetEnvironmentUrl($groupName: String!, $urlId: String!) {
    environmentUrl(groupName: $groupName, urlId: $urlId) {
      id
      createdAt
      rootId
      url
      secrets {
        description
        key
        owner
      }
    }
  }
`);

const GET_SECRET_BY_KEY = graphql(`
  query GetSecretByKey(
    $groupName: String!
    $resourceId: ID!
    $resourceType: ResourceType!
    $secretKey: String!
  ) {
    secret(
      groupName: $groupName
      resourceId: $resourceId
      resourceType: $resourceType
      secretKey: $secretKey
    ) {
      description
      key
      owner
      value
    }
  }
`);

const GET_ROOT_VULNS = graphql(`
  query GetRootVulns($groupName: String!, $rootId: ID!) {
    root(groupName: $groupName, rootId: $rootId) {
      ... on GitRoot {
        id
        vulnerabilities {
          id
          state
          technique
          vulnerabilityType
        }
      }
      ... on IPRoot {
        id
        vulnerabilities {
          id
          state
          technique
          vulnerabilityType
        }
      }
      ... on URLRoot {
        id
        vulnerabilities {
          id
          state
          technique
          vulnerabilityType
        }
      }
    }
  }
`);

const UPDATE_ENVIRONMENT_URL = graphql(`
  mutation UpdateGitRootEnvironmentUrl(
    $groupName: String!
    $include: Boolean
    $rootId: ID!
    $urlId: String!
  ) {
    updateGitRootEnvironmentUrl(
      groupName: $groupName
      include: $include
      rootId: $rootId
      urlId: $urlId
    ) {
      success
    }
  }
`);

const UPDATE_URL_ROOT_EXCLUDED_SUB_PATHS = graphql(`
  mutation UpdateUrlRootExcludedSubPaths(
    $excludedSubPaths: [String!]!
    $groupName: String!
    $rootId: ID!
  ) {
    updateUrlRoot(
      excludedSubPaths: $excludedSubPaths
      groupName: $groupName
      rootId: $rootId
    ) {
      success
    }
  }
`);

const VERIFY_ENVIRONMENT_INTEGRITY = graphql(`
  query verifyEnvironmentIntegrity(
    $azureClientId: String
    $azureClientSecret: String
    $azureTenantId: String
    $cloudName: String
    $gcpPrivateKey: String
    $groupName: String!
    $url: String!
    $urlType: String!
  ) {
    verifyEnvironmentIntegrity(
      azureClientId: $azureClientId
      azureClientSecret: $azureClientSecret
      azureTenantId: $azureTenantId
      cloudName: $cloudName
      gcpPrivateKey: $gcpPrivateKey
      groupName: $groupName
      url: $url
      urlType: $urlType
    )
  }
`);

const SYNC_GIT_ROOT = graphql(`
  mutation SyncGitRoot($groupName: String!, $rootId: String!) {
    syncGitRoot(groupName: $groupName, rootId: $rootId) {
      success
    }
  }
`);

const VALIDATE_GIT_ACCESS = graphql(`
  mutation ValidateGitAccess(
    $branch: String!
    $credentials: RootCredentialsInput!
    $organizationId: String
    $url: String!
  ) {
    validateGitAccess(
      branch: $branch
      credentials: $credentials
      organizationId: $organizationId
      url: $url
    ) {
      success
    }
  }
`);

const REMOVE_ENVIRONMENT_URL = graphql(`
  mutation RemoveEnvironmentUrl(
    $groupName: String!
    $rootId: ID!
    $urlId: String!
  ) {
    removeEnvironmentUrl(
      groupName: $groupName
      rootId: $rootId
      urlId: $urlId
    ) {
      success
    }
  }
`);

const REMOVE_GIT_ROOT_DOCKER_IMAGE = graphql(`
  mutation RemoveGitRootDockerImage(
    $groupName: String!
    $rootId: ID!
    $uri: String!
  ) {
    removeRootDockerImage(groupName: $groupName, rootId: $rootId, uri: $uri) {
      success
    }
  }
`);

const ADD_GIT_ROOT_DOCKER_IMAGE = graphql(`
  mutation AddGitRootDockerImage(
    $credentials: RootCredentialsInput
    $groupName: String!
    $rootId: ID!
    $uri: String!
  ) {
    addRootDockerImage(
      credentials: $credentials
      groupName: $groupName
      rootId: $rootId
      uri: $uri
    ) {
      sbomJobQueued
      success
      uri
    }
  }
`);

const ADD_FILES_TO_DB_MUTATION = graphql(`
  mutation addFilesToDbMutation(
    $filesDataInput: [FilesDataInput!]!
    $groupName: String!
  ) {
    addFilesToDb(filesDataInput: $filesDataInput, groupName: $groupName) {
      success
    }
  }
`);

const UPDATE_GIT_ROOT_ENVIRONMENT_FILE = graphql(`
  mutation UpdateGitRootEnvironmentFile(
    $fileName: String!
    $groupName: String!
    $oldFileName: String!
    $rootId: ID!
    $urlId: String!
  ) {
    updateGitRootEnvironmentFile(
      fileName: $fileName
      groupName: $groupName
      oldFileName: $oldFileName
      rootId: $rootId
      urlId: $urlId
    ) {
      success
    }
  }
`);

const ADD_GROUP_TAGS_MUTATION = graphql(`
  mutation AddGroupTagsMutation($groupName: String!, $tagsData: [String!]) {
    addGroupTags(groupName: $groupName, tagsData: $tagsData) {
      success
    }
  }
`);

const ADD_GROUP_HOOKS_MUTATION = graphql(`
  mutation AddGroupHooksMutation($groupName: String!, $hook: HookInput!) {
    addHook(groupName: $groupName, hook: $hook) {
      success
    }
  }
`);

const UPDATE_GROUP_HOOKS_MUTATION = graphql(`
  mutation UpdateGroupHooksMutation(
    $groupName: String!
    $hook: HookInput!
    $hookId: String!
  ) {
    updateHook(groupName: $groupName, hook: $hook, hookId: $hookId) {
      success
    }
  }
`);

const UPDATE_GIT_ROOT_DOCKER_IMAGE = graphql(`
  mutation UpdateGitRootDockerImage(
    $credentials: RootCredentialsInput
    $groupName: String!
    $rootId: ID!
    $uri: String!
  ) {
    updateRootDockerImage(
      credentials: $credentials
      groupName: $groupName
      rootId: $rootId
      uri: $uri
    ) {
      success
    }
  }
`);

const UPDATE_SECRET = graphql(`
  mutation updateSecret(
    $description: String
    $groupName: String!
    $key: String!
    $resourceId: ID!
    $resourceType: ResourceType!
    $value: String!
  ) {
    updateSecret(
      description: $description
      groupName: $groupName
      key: $key
      resourceId: $resourceId
      resourceType: $resourceType
      value: $value
    ) {
      success
    }
  }
`);

const REMOVE_GROUP_HOOK_MUTATION = graphql(`
  mutation RemoveGroupHook($groupName: String!, $hookId: String!) {
    removeGroupHook(groupName: $groupName, hookId: $hookId) {
      success
    }
  }
`);

const DOWNLOAD_FILE_MUTATION = graphql(`
  mutation DownloadFileMutation($filesData: String!, $groupName: String!) {
    downloadFile(filesDataInput: $filesData, groupName: $groupName) {
      success
      url
    }
  }
`);

const GET_FILES = graphql(`
  query GetFilesQuery($groupName: String!) {
    resources(groupName: $groupName) {
      files {
        description
        fileName
        uploadDate
        uploader
      }
    }
  }
`);

const GET_GROUP_ACCESS_INFO = graphql(`
  query GetGroupAccessInfo($groupName: String!) {
    group(groupName: $groupName) {
      disambiguation
      groupContext
      name
    }
  }
`);

const GET_GROUP_DATA = graphql(`
  query GetGroupData($groupName: String!) {
    group(groupName: $groupName) {
      businessId
      businessName
      description
      hasAdvanced
      hasEssential
      language
      managed
      name
      service
      sprintDuration
      sprintStartDate
      subscription
    }
  }
`);

const GET_HOOKS = graphql(`
  query GetHooksQuery($groupName: String!) {
    group(groupName: $groupName) {
      name
      hook {
        id
        entryPoint
        hookEvents
        name
        token
        tokenHeader
      }
    }
  }
`);

const GET_TAGS = graphql(`
  query GetTagsQuery($groupName: String!) {
    group(groupName: $groupName) {
      name
      tags
    }
  }
`);

const REMOVE_FILE_MUTATION = graphql(`
  mutation RemoveFileMutation(
    $filesDataInput: FilesDataInput!
    $groupName: String!
  ) {
    removeFiles(filesDataInput: $filesDataInput, groupName: $groupName) {
      success
    }
  }
`);

const REMOVE_GROUP_TAG_MUTATION = graphql(`
  mutation RemoveGroupTagMutation($groupName: String!, $tagToRemove: String!) {
    removeGroupTag(groupName: $groupName, tag: $tagToRemove) {
      success
    }
  }
`);

const SIGN_POST_URL_MUTATION = graphql(`
  mutation SignPostUrlMutation(
    $filesDataInput: [FilesDataInput!]!
    $groupName: String!
  ) {
    signPostUrl(filesDataInput: $filesDataInput, groupName: $groupName) {
      success
      url {
        url
        fields {
          algorithm
          credential
          date
          key
          policy
          securitytoken
          signature
        }
      }
    }
  }
`);

const UPDATE_GROUP_ACCESS_INFO = graphql(`
  mutation UpdateGroupAccessInfo($groupContext: String, $groupName: String!) {
    updateGroupAccessInfo(groupContext: $groupContext, groupName: $groupName) {
      success
    }
  }
`);

const UPDATE_GROUP_DATA = graphql(`
  mutation UpdateGroupData(
    $comments: String!
    $description: String
    $groupName: String!
    $hasAdvanced: Boolean!
    $hasASM: Boolean!
    $hasEssential: Boolean!
    $language: String
    $reason: UpdateGroupReason!
    $service: ServiceType!
    $subscription: SubscriptionType!
  ) {
    updateGroup(
      comments: $comments
      description: $description
      groupName: $groupName
      hasAdvanced: $hasAdvanced
      hasAsm: $hasASM
      hasEssential: $hasEssential
      language: $language
      reason: $reason
      service: $service
      subscription: $subscription
    ) {
      success
    }
  }
`);

const UPDATE_GROUP_DISAMBIGUATION = graphql(`
  mutation UpdateGroupDisambiguation(
    $disambiguation: String
    $groupName: String!
  ) {
    updateGroupDisambiguation(
      disambiguation: $disambiguation
      groupName: $groupName
    ) {
      success
    }
  }
`);

const UPDATE_GROUP_INFO = graphql(`
  mutation UpdateGroupInfo(
    $businessId: String
    $businessName: String
    $comments: String!
    $description: String!
    $groupName: String!
    $isManagedChanged: Boolean!
    $language: Language!
    $managed: ManagedType!
    $sprintDuration: Int
    $sprintStartDate: DateTime
  ) {
    updateGroupInfo(
      businessId: $businessId
      businessName: $businessName
      description: $description
      groupName: $groupName
      language: $language
      sprintDuration: $sprintDuration
      sprintStartDate: $sprintStartDate
    ) {
      success
    }
    updateGroupManaged(
      comments: $comments
      groupName: $groupName
      managed: $managed
    ) @include(if: $isManagedChanged) {
      success
    }
  }
`);

const GET_GIT_ROOTS_QUERIES = graphql(`
  query GetGitRootsViewQueries(
    $canGetSecrets: Boolean = false
    $groupName: String!
    $organizationName: String!
  ) {
    ...GetOrganizationExternalId
    ...GetRootsEnvironmentUrls
  }
`);

const GET_GIT_ROOTS = graphql(`
  query GetGitRoots(
    $after: String
    $branch: String
    $cloningStatus: CloningStatus
    $first: Int
    $groupName: String!
    $includesHealthCheck: Boolean
    $nickname: String
    $search: String
    $state: ResourceState
  ) {
    ...GetGitRootsViewFragment
  }
`);

const GET_IP_ROOTS = graphql(`
  query GetIPRoots(
    $after: String
    $first: Int
    $groupName: String!
    $search: String
  ) {
    ...GetIPRootsViewFragment
  }
`);

const GET_URL_ROOTS = graphql(`
  query GetUrlRoots(
    $after: String
    $first: Int
    $groupName: String!
    $search: String
  ) {
    ...GetURLRootsViewFragment
  }
`);

const UPLOAD_GIT_ROOT_FILE = graphql(`
  mutation UploadGitRootFile($file: Upload!, $groupName: String!) {
    uploadGitRootFile(file: $file, groupName: $groupName) {
      errorLines
      message
      success
      totalErrors
      totalSuccess
    }
  }
`);

const GET_ACTIVE_ROOTS = graphql(`
  query GetActiveRoots(
    $after: String
    $first: Int!
    $groupName: String!
    $search: String
  ) {
    group(groupName: $groupName) {
      name
      gitRoots(after: $after, first: $first, search: $search, state: ACTIVE) {
        total
        edges {
          node {
            id
            nickname
          }
        }
        pageInfo {
          endCursor
          hasNextPage
        }
      }
    }
  }
`);

export {
  ACTIVATE_ROOT,
  ADD_ENVIRONMENT_CSPM,
  ADD_ENVIRONMENT_URL,
  ADD_ENVIRONMENT_INTEGRITY_EVENT,
  ADD_FILES_TO_DB_MUTATION,
  ADD_GIT_ROOT_DOCKER_IMAGE,
  ADD_GIT_ROOT,
  ADD_GROUP_HOOKS_MUTATION,
  ADD_GROUP_TAGS_MUTATION,
  ADD_IP_ROOT,
  ADD_SECRET,
  ADD_URL_ROOT,
  DEACTIVATE_ROOT,
  DOWNLOAD_FILE_MUTATION,
  DOWNLOAD_GIT_ROOTS_FILE_MUTATION,
  GET_ACTIVE_ROOTS,
  GET_ENVIRONMENT_URL,
  GET_FILES,
  GET_GIT_ROOT_DETAILS,
  GET_GIT_ROOTS,
  GET_GIT_ROOT_DOCKER_IMAGES,
  GET_GIT_ROOTS_FRAGMENT,
  GET_GIT_ROOTS_QUERIES,
  GET_GIT_ROOT_MANAGEMENT_DETAILS,
  GET_GROUP_ACCESS_INFO,
  GET_GROUP_DATA,
  GET_GROUP_ENV_URLS_FRAGMENT,
  GET_GROUPS,
  GET_HOOKS,
  GET_IP_ROOTS_FRAGMENT,
  GET_IP_ROOTS,
  GET_ORGANIZATION_CREDENTIALS,
  GET_ORGANIZATION_EXTERNAL_ID,
  GET_ORGANIZATION_ID,
  GET_ROOT_ENVIRONMENT_URLS_SECRETS,
  GET_ROOT_ENVIRONMENT_URLS,
  GET_ROOT_SECRETS,
  GET_ROOT_VULNS,
  GET_ROOTS,
  GET_GROUP_DOCKER_IMAGES,
  GET_SECRET_BY_KEY,
  GET_TAGS,
  GET_URL_ROOT_EXCLUDED_SUB_PATHS,
  GET_URL_ROOTS,
  GET_URL_ROOTS_FRAGMENT,
  MOVE_ENVIRONMENT,
  MOVE_ROOT,
  REMOVE_ENVIRONMENT_URL,
  REMOVE_FILE_MUTATION,
  REMOVE_GIT_ROOT_DOCKER_IMAGE,
  REMOVE_GROUP_HOOK_MUTATION,
  REMOVE_GROUP_TAG_MUTATION,
  REMOVE_SECRET,
  SIGN_POST_URL_MUTATION,
  SYNC_GIT_ROOT,
  UPDATE_ENVIRONMENT_URL,
  UPDATE_SECRET,
  UPDATE_GIT_ROOT,
  UPDATE_GROUP_ACCESS_INFO,
  UPDATE_GROUP_DATA,
  UPDATE_GROUP_DISAMBIGUATION,
  UPDATE_GROUP_HOOKS_MUTATION,
  UPDATE_GROUP_INFO,
  UPDATE_GIT_ROOT_ENVIRONMENT_FILE,
  UPDATE_IP_ROOT,
  UPDATE_GIT_ROOT_DOCKER_IMAGE,
  UPDATE_URL_ROOT_EXCLUDED_SUB_PATHS,
  UPDATE_URL_ROOT,
  UPLOAD_GIT_ROOT_FILE,
  VALIDATE_GIT_ACCESS,
  VERIFY_ENVIRONMENT_INTEGRITY,
};
