import type { IUseModal } from "@fluidattacks/design";

interface IAddTagsModalProps {
  modalRef: IUseModal;
  onSubmit: (values: { tags: string[] }) => void;
}

export type { IAddTagsModalProps };
