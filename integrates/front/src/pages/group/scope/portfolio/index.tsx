import { NetworkStatus, useMutation, useQuery } from "@apollo/client";
import { Button, Container, useModal } from "@fluidattacks/design";
import type { ColumnDef } from "@tanstack/react-table";
import _ from "lodash";
import mixpanel from "mixpanel-browser";
import { Fragment, useCallback, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { AddTagsModal } from "./add-tags-modal";

import {
  ADD_GROUP_TAGS_MUTATION,
  GET_TAGS,
  REMOVE_GROUP_TAG_MUTATION,
} from "../queries";
import { Table } from "components/table";
import { Can } from "context/authz/can";
import { useTable } from "hooks";
import { useObserverAudit } from "hooks/use-audit";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

interface IPortfolioProps {
  readonly groupName: string;
}

const Portfolio: React.FC<IPortfolioProps> = ({ groupName }): JSX.Element => {
  const { t } = useTranslation();
  const tableRef = useTable("tblTags");
  const { ref } = useObserverAudit("Group.Portfolios", groupName);

  // State management
  const addTagsModal = useModal("add-tags-modal");
  const { open } = addTagsModal;
  const [currentRow, setCurrentRow] = useState<{ tagName: string }[]>([]);

  // GraphQL operations
  const { data, refetch, networkStatus } = useQuery(GET_TAGS, {
    onError: (error): void => {
      msgError(t("groupAlerts.errorTextsad"));
      Logger.warning("An error occurred loading group tags", error);
    },
    variables: { groupName },
  });

  const [addGroupTags] = useMutation(ADD_GROUP_TAGS_MUTATION, {
    onCompleted: (): void => {
      void refetch();
      mixpanel.track("AddGroupTags");
      msgSuccess(
        t("searchFindings.tabResources.success"),
        t("searchFindings.tabUsers.titleSuccess"),
      );
    },
    onError: (error): void => {
      error.graphQLErrors.forEach(({ message }): void => {
        if (message === "Exception - One or more values already exist") {
          msgError(t("searchFindings.tabResources.repeatedItem"));
        } else {
          msgError(t("groupAlerts.errorTextsad"));
          Logger.warning("An error occurred adding tags", error);
        }
      });
    },
  });

  const [removeGroupTag, { loading: removing }] = useMutation(
    REMOVE_GROUP_TAG_MUTATION,
    {
      onCompleted: (): void => {
        void refetch();
        mixpanel.track("RemoveTag");
        msgSuccess(
          t("searchFindings.tabResources.successRemove"),
          t("searchFindings.tabUsers.titleSuccess"),
        );
      },
      onError: ({ graphQLErrors }): void => {
        graphQLErrors.forEach((error): void => {
          msgError(t("groupAlerts.errorTextsad"));
          Logger.warning("An error occurred removing tags", error);
        });
      },
    },
  );

  const handleRemoveTag = useCallback((): void => {
    removeGroupTag({
      variables: {
        groupName,
        tagToRemove: currentRow[0].tagName,
      },
    }).catch((): void => {
      Logger.error("An error occurred removing tag");
    });
    setCurrentRow([]);
  }, [currentRow, groupName, removeGroupTag]);

  const groupTags =
    _.isUndefined(data) || _.isNull(data.group.tags) ? [] : data.group.tags;

  const tagsDataset: {
    tagName: string;
  }[] = groupTags.map((tag): { tagName: string } => ({ tagName: tag }));

  const handleTagsAdd = useCallback(
    (values: { tags: string[] }): void => {
      const repeatedInputs = values.tags.filter(
        (tag): boolean => values.tags.filter(_.matches(tag)).length > 1,
      );
      const repeatedTags = values.tags.filter(
        (tag): boolean =>
          tagsDataset.filter(_.matches({ tagName: tag })).length > 0,
      );

      if (repeatedInputs.length > 0) {
        msgError(t("searchFindings.tabResources.repeatedInput"));
      } else if (repeatedTags.length > 0) {
        msgError(t("searchFindings.tabResources.repeatedItem"));
      } else {
        addTagsModal.close();
        addGroupTags({
          variables: {
            groupName,
            tagsData: values.tags,
          },
        }).catch((): void => {
          Logger.error("An error occurred adding tag group");
        });
      }
    },
    [addGroupTags, addTagsModal, groupName, t, tagsDataset],
  );

  const columns: ColumnDef<{ tagName: string }>[] = [
    {
      accessorKey: "tagName",
      header: t("searchFindings.tabResources.tags.title"),
    },
  ];

  if (_.isUndefined(data) || _.isEmpty(data)) {
    return <div />;
  }

  return (
    <Fragment>
      <Can
        do={"integrates_api_mutations_remove_group_tag_mutate"}
        passThrough={true}
      >
        {(canDelete: boolean): JSX.Element => (
          <div ref={ref}>
            <Table
              columns={columns}
              data={tagsDataset}
              rightSideComponents={
                <Container display={"flex"} gap={0.5}>
                  <Can do={"integrates_api_mutations_add_group_tags_mutate"}>
                    <Button
                      icon={"add"}
                      id={"portfolio-add"}
                      onClick={open}
                      tooltip={t("searchFindings.tabResources.tags.addTooltip")}
                      variant={"primary"}
                    >
                      {t("searchFindings.tabResources.addRepository")}
                    </Button>
                  </Can>
                  <Can do={"integrates_api_mutations_remove_group_tag_mutate"}>
                    <Button
                      disabled={_.isEmpty(currentRow) || removing}
                      icon={"remove"}
                      id={"portfolio-remove"}
                      onClick={handleRemoveTag}
                      tooltip={t(
                        "searchFindings.tabResources.tags.removeTooltip",
                      )}
                      variant={"secondary"}
                    >
                      {t("searchFindings.tabResources.removeRepository")}
                    </Button>
                  </Can>
                </Container>
              }
              rowSelectionSetter={
                canDelete &&
                !(networkStatus === NetworkStatus.refetch || removing)
                  ? setCurrentRow
                  : undefined
              }
              rowSelectionState={currentRow}
              selectionMode={"radio"}
              tableRef={tableRef}
            />
          </div>
        )}
      </Can>
      <AddTagsModal modalRef={addTagsModal} onSubmit={handleTagsAdd} />
    </Fragment>
  );
};

export type { IPortfolioProps };
export { Portfolio };
