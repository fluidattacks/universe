import { PureAbility } from "@casl/ability";
import { screen, waitFor, within } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import isEqual from "lodash/isEqual";
import { HttpResponse, type StrictResponse, graphql } from "msw";

import { Portfolio } from ".";
import type { IPortfolioProps } from ".";
import {
  ADD_GROUP_TAGS_MUTATION,
  GET_TAGS,
  REMOVE_GROUP_TAG_MUTATION,
} from "../queries";
import { authzPermissionsContext } from "context/authz/config";
import type {
  AddGroupTagsMutationMutation as AddGroupTags,
  GetTagsQueryQuery as GetTags,
  RemoveGroupTagMutationMutation as RemoveGroupTag,
} from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage, IMessage } from "mocks/types";
import { msgError, msgSuccess } from "utils/notifications";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

describe("portfolio", (): void => {
  const graphqlMocked = graphql.link(LINK);
  const mockProps: IPortfolioProps = {
    groupName: "TEST",
  };

  const mocksTags = [
    graphqlMocked.query(GET_TAGS, (): StrictResponse<{ data: GetTags }> => {
      return HttpResponse.json({
        data: {
          group: {
            __typename: "Group",
            name: "TEST",
            tags: ["test-tag1", "test-tag2"],
          },
        },
      });
    }),
  ];

  it("should add a tag", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const mocksMutation = [
      graphqlMocked.mutation(
        ADD_GROUP_TAGS_MUTATION,
        ({
          variables,
        }): StrictResponse<IErrorMessage | { data: AddGroupTags }> => {
          const { groupName, tagsData } = variables;
          if (groupName === "TEST" && isEqual(tagsData, ["test-new-tag"])) {
            return HttpResponse.json({
              data: { addGroupTags: { success: true } },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error adding tags")],
          });
        },
      ),
    ];
    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_add_group_tags_mutate" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Portfolio groupName={mockProps.groupName} />
      </authzPermissionsContext.Provider>,
      { mocks: [...mocksTags, ...mocksMutation] },
    );

    await screen.findByText("searchFindings.tabResources.addRepository");
    await userEvent.click(
      screen.getByText("searchFindings.tabResources.addRepository"),
    );
    await userEvent.type(
      screen.getByRole("textbox", { name: "tags.0" }),
      "test-new-tag",
    );
    await userEvent.click(screen.getByText("Confirm"));

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledTimes(1);
    });
  });

  it("should remove a tag", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();
    const mocksTags1 = [
      graphqlMocked.query(
        GET_TAGS,
        (): StrictResponse<{ data: GetTags }> => {
          return HttpResponse.json({
            data: {
              group: {
                __typename: "Group",
                name: "TEST",
                tags: ["test-tag1", "test-tag2"],
              },
            },
          });
        },
        { once: true },
      ),
      graphqlMocked.query(
        GET_TAGS,
        (): StrictResponse<{ data: GetTags }> => {
          return HttpResponse.json({
            data: {
              group: {
                __typename: "Group",
                name: "TEST",
                tags: ["test-tag1"],
              },
            },
          });
        },
        { once: true },
      ),
    ];
    const mocksMutation = [
      graphqlMocked.mutation(
        REMOVE_GROUP_TAG_MUTATION,
        ({
          variables,
        }): StrictResponse<IErrorMessage | { data: RemoveGroupTag }> => {
          const { groupName, tagToRemove } = variables;
          if (groupName === "TEST" && tagToRemove === "test-tag1") {
            return HttpResponse.json({
              data: { removeGroupTag: { success: true } },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error removing tags")],
          });
        },
      ),
    ];
    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_remove_group_tag_mutate" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Portfolio groupName={mockProps.groupName} />
      </authzPermissionsContext.Provider>,
      { mocks: [...mocksTags1, ...mocksMutation] },
    );

    await waitFor((): void => {
      expect(screen.getByRole("table")).toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(screen.queryByText("test-tag1")).toBeInTheDocument();
    });
    const cell = screen.getByText("test-tag1");
    const row = cell.closest("tr");
    const radioButton = within(row as HTMLElement).getByRole("radio");

    await userEvent.click(radioButton);

    await userEvent.click(
      screen.getByText("searchFindings.tabResources.removeRepository"),
    );

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "searchFindings.tabResources.successRemove",
        "searchFindings.tabUsers.titleSuccess",
      );
    });
  });

  it("should handle errors when add a tag", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const mocksMutation = [
      graphqlMocked.mutation(
        ADD_GROUP_TAGS_MUTATION,
        ({
          variables,
        }): StrictResponse<
          Record<"errors", IMessage[]> | { data: AddGroupTags }
        > => {
          const { groupName, tagsData } = variables;
          if (groupName === "TEST" && isEqual(tagsData, ["test-new-tag"])) {
            return HttpResponse.json({
              errors: [
                new GraphQLError("Error adding tags"),
                new GraphQLError(
                  "Exception - One or more values already exist",
                ),
              ],
            });
          }

          return HttpResponse.json({
            data: { addGroupTags: { success: true } },
          });
        },
      ),
    ];

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_add_group_tags_mutate" },
    ]);

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Portfolio groupName={mockProps.groupName} />
      </authzPermissionsContext.Provider>,
      { mocks: [...mocksTags, ...mocksMutation] },
    );
    await screen.findByText("searchFindings.tabResources.addRepository");
    await userEvent.click(
      screen.getByText("searchFindings.tabResources.addRepository"),
    );
    await userEvent.type(
      screen.getByRole("textbox", { name: "tags.0" }),
      "test-new-tag",
    );
    await userEvent.click(screen.getByText("Confirm"));

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledTimes(2);
    });
  });

  it("should handle error when remove a tag", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const mocksMutation = [
      graphqlMocked.mutation(
        REMOVE_GROUP_TAG_MUTATION,
        ({
          variables,
        }): StrictResponse<IErrorMessage | { data: RemoveGroupTag }> => {
          const { groupName, tagToRemove } = variables;
          if (groupName === "TEST" && tagToRemove === "test-tag1") {
            return HttpResponse.json({
              errors: [new GraphQLError("Error removing tags")],
            });
          }

          return HttpResponse.json({
            data: { removeGroupTag: { success: true } },
          });
        },
      ),
    ];
    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_remove_group_tag_mutate" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Portfolio groupName={mockProps.groupName} />
      </authzPermissionsContext.Provider>,
      { mocks: [...mocksTags, ...mocksMutation] },
    );

    await waitFor((): void => {
      expect(screen.getByRole("table")).toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(screen.queryByText("test-tag1")).toBeInTheDocument();
    });

    const cell = screen.getByText("test-tag1");
    const row = cell.closest("tr");
    const radioButton = within(row as HTMLElement).getByRole("radio");

    await userEvent.click(radioButton);

    await userEvent.click(
      screen.getByText("searchFindings.tabResources.removeRepository"),
    );

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledTimes(1);
    });
  });

  it("should handle error when there are repeated tags", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();
    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_add_group_tags_mutate" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Portfolio groupName={mockProps.groupName} />
      </authzPermissionsContext.Provider>,
      { mocks: mocksTags },
    );

    await screen.findByText("searchFindings.tabResources.addRepository");
    await userEvent.click(
      screen.getByText("searchFindings.tabResources.addRepository"),
    );
    await userEvent.type(
      screen.getByRole("textbox", { name: "tags.0" }),
      "test-tag1",
    );
    await userEvent.click(screen.getByText("Confirm"));

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        "searchFindings.tabResources.repeatedItem",
      );
    });
  });
});
