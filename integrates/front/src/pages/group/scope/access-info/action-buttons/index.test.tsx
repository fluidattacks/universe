import { PureAbility } from "@casl/ability";
import { screen } from "@testing-library/react";

import { ActionButtons } from ".";
import type { IActionButtonsProps } from ".";
import { authzPermissionsContext } from "context/authz/config";
import { render } from "mocks";

describe("actionButtons", (): void => {
  const baseMockedProps: IActionButtonsProps = {
    editTooltip: "tooltip",
    isEditing: false,
    isPristine: false,
    onEdit: jest.fn(),
    onUpdate: jest.fn(),
    permission: "integrates_api_mutations_update_group_access_info_mutate",
  };

  it("should render a component", (): void => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_update_group_access_info_mutate" },
    ]);
    const { editTooltip, isEditing, isPristine, onEdit, onUpdate, permission } =
      baseMockedProps;
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <ActionButtons
          editTooltip={editTooltip}
          isEditing={isEditing}
          isPristine={isPristine}
          onEdit={onEdit}
          onUpdate={onUpdate}
          permission={permission}
        />
      </authzPermissionsContext.Provider>,
    );

    expect(screen.queryByRole("button")).toBeInTheDocument();
    expect(
      screen.queryByText("searchFindings.tabDescription.editable.text"),
    ).toBeInTheDocument();
  });
});
