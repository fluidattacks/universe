import { Button, Container } from "@fluidattacks/design";
import { Fragment } from "react";
import { useTranslation } from "react-i18next";

import { Can } from "context/authz/can";

interface IActionButtonsProps {
  editTooltip: string;
  isEditing: boolean;
  isPristine: boolean;
  onEdit: () => void;
  onUpdate: () => void;
  permission: string;
}

const ActionButtons = ({
  isEditing,
  isPristine,
  onEdit,
  onUpdate,
  permission,
}: Readonly<IActionButtonsProps>): JSX.Element => {
  const { t } = useTranslation();

  return (
    <Can do={permission}>
      <Container display={"flex"} gap={0.5}>
        {isEditing ? (
          <Fragment>
            <Button icon={"close"} onClick={onEdit} variant={"secondary"}>
              {t("searchFindings.tabDescription.editable.cancel")}
            </Button>
            <Button
              disabled={isPristine}
              icon={"rotate-right"}
              onClick={onUpdate}
              tooltip={t("searchFindings.tabDescription.save.tooltip")}
              variant={"primary"}
            >
              {t("searchFindings.tabDescription.save.text")}
            </Button>
          </Fragment>
        ) : (
          <Button icon={"edit"} onClick={onEdit} variant={"primary"}>
            {t("searchFindings.tabDescription.editable.text")}
          </Button>
        )}
      </Container>
    </Can>
  );
};

export type { IActionButtonsProps };
export { ActionButtons };
