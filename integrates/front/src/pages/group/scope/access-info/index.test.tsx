import { PureAbility } from "@casl/ability";
import { screen, waitFor } from "@testing-library/react";
import { Route, Routes } from "react-router-dom";

import { AccessInfo } from ".";
import { authzPermissionsContext } from "context/authz/config";
import { render } from "mocks";

describe("accessInfo", (): void => {
  it("should render a component", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_resolvers_group_disambiguation_resolve" },
      { action: "integrates_api_mutations_update_group_disambiguation_mutate" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<AccessInfo />}
            path={"/orgs/:organizationName/groups/:groupName/scope"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/TEST/scope"],
        },
      },
    );

    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.tabDescription.editable.text"),
      ).toBeInTheDocument();
    });

    expect(
      screen.queryByText(
        "searchFindings.groupAccessInfoSection.disambiguation",
      ),
    ).toBeInTheDocument();
  });
});
