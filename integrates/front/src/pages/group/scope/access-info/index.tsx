import { useMutation, useQuery } from "@apollo/client";
import { Container, Text } from "@fluidattacks/design";
import { Formik } from "formik";
import _ from "lodash";
import { useCallback, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";
import { useTheme } from "styled-components";

import { GroupContextForm } from "./context";
import { DisambiguationForm } from "./disambiguation";
import { contextValidations, disambiguationValidations } from "./validations";

import {
  GET_GROUP_ACCESS_INFO,
  UPDATE_GROUP_ACCESS_INFO,
  UPDATE_GROUP_DISAMBIGUATION,
} from "../queries";
import { CardContainer } from "components/card-container";
import { Can } from "context/authz/can";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

interface IGroupAccessInfo {
  group: {
    disambiguation: string | null;
    groupContext: string | null;
  };
}

const AccessInfo: React.FC = (): JSX.Element => {
  const { t } = useTranslation();
  const { groupName } = useParams() as { groupName: string };
  const theme = useTheme();
  const [isEditingGroupAccessInfo, setIsEditingGroupAccessInfo] =
    useState(false);
  const [isEditingDisambiguation, setIsEditingDisambiguation] = useState(false);

  const { data, refetch } = useQuery(GET_GROUP_ACCESS_INFO, {
    fetchPolicy: "no-cache",
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.warning("An error occurred while getting group info", error);
      });
    },
    variables: {
      groupName,
    },
  });

  const [updateGroupAccessInfo] = useMutation(UPDATE_GROUP_ACCESS_INFO, {
    onCompleted: (result: {
      updateGroupAccessInfo: { success: boolean };
    }): void => {
      if (result.updateGroupAccessInfo.success) {
        msgSuccess(t("groupAlerts.updated"), t("groupAlerts.updatedTitle"));
        refetch().catch((): void => {
          Logger.error("An error occurred updating group access");
        });
      }
    },
    onError: (updateError): void => {
      updateError.graphQLErrors.forEach(({ message }): void => {
        if (message === "Exception - Invalid markdown") {
          msgError(t("validations.invalidMarkdown"));
        } else {
          msgError(t("groupAlerts.errorTextsad"));
          Logger.warning(
            "An error occurred updating group access info",
            updateError,
          );
        }
      });
    },
  });

  const [updateGroupDisambiguation] = useMutation(UPDATE_GROUP_DISAMBIGUATION, {
    onCompleted: (result: {
      updateGroupDisambiguation: { success: boolean };
    }): void => {
      if (result.updateGroupDisambiguation.success) {
        msgSuccess(t("groupAlerts.updated"), t("groupAlerts.updatedTitle"));
        refetch().catch((): void => {
          Logger.error("An error occurred updating group disambiguation");
        });
      }
    },
    onError: (updateError): void => {
      updateError.graphQLErrors.forEach(({ message }): void => {
        if (message === "Exception - Invalid markdown") {
          msgError(t("validations.invalidMarkdown"));
        } else {
          msgError(t("groupAlerts.errorTextsad"));
          Logger.warning(
            "An error occurred updating group access info",
            updateError,
          );
        }
      });
    },
  });

  const handleGroupAccessInfoSubmit = useCallback(
    async (values: {
      disambiguation?: string;
      groupContext: string;
    }): Promise<void> => {
      setIsEditingGroupAccessInfo(false);
      await updateGroupAccessInfo({
        variables: {
          groupContext: values.groupContext,
          groupName,
        },
      });
    },
    [groupName, updateGroupAccessInfo],
  );

  const handleDisambiguationSubmit = useCallback(
    async (values: {
      disambiguation: string;
      groupContext: string;
    }): Promise<void> => {
      setIsEditingDisambiguation(false);
      await updateGroupDisambiguation({
        variables: {
          disambiguation: values.disambiguation,
          groupName,
        },
      });
    },
    [groupName, updateGroupDisambiguation],
  );

  if (_.isUndefined(data) || _.isEmpty(data)) {
    return <div />;
  }

  const dataset = data.group;

  return (
    <Container display={"flex"} gap={0.5}>
      <CardContainer
        bgColor={theme.palette.white}
        px={1.25}
        py={1.25}
        width={"50%"}
      >
        <Text color={theme.palette.gray[800]} fontWeight={"bold"} size={"md"}>
          {t("searchFindings.groupAccessInfoSection.groupContext")}
        </Text>
        <Container display={"flex"} height={"100%"}>
          <Formik
            enableReinitialize={true}
            initialValues={{
              disambiguation: dataset.disambiguation ?? "",
              groupContext: dataset.groupContext ?? "",
            }}
            name={"editGroupAccessInfo"}
            onSubmit={handleGroupAccessInfoSubmit}
            validationSchema={contextValidations}
          >
            <GroupContextForm
              data={data}
              isEditing={isEditingGroupAccessInfo}
              setEditing={setIsEditingGroupAccessInfo}
            />
          </Formik>
        </Container>
      </CardContainer>
      <CardContainer
        bgColor={theme.palette.white}
        px={1.25}
        py={1.25}
        width={"50%"}
      >
        <Text color={theme.palette.gray[800]} fontWeight={"bold"} size={"md"}>
          {t("searchFindings.groupAccessInfoSection.disambiguation")}
        </Text>
        <Can do={"integrates_api_resolvers_group_disambiguation_resolve"}>
          <Container display={"flex"} height={"100%"}>
            <Formik
              enableReinitialize={true}
              initialValues={{
                disambiguation: dataset.disambiguation ?? "",
                groupContext: dataset.groupContext ?? "",
              }}
              name={"editDisambiguation"}
              onSubmit={handleDisambiguationSubmit}
              validationSchema={disambiguationValidations}
            >
              <DisambiguationForm
                data={data}
                isEditing={isEditingDisambiguation}
                setEditing={setIsEditingDisambiguation}
              />
            </Formik>
          </Container>
        </Can>
      </CardContainer>
    </Container>
  );
};

export type { IGroupAccessInfo };
export { AccessInfo };
