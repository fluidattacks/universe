import type { IUseModal } from "@fluidattacks/design";

import type { ResourceType } from "gql/graphql";

interface ISecretsProps {
  environmentSecret?: boolean;
  isOpen?: boolean;
  groupName: string;
  onCloseModal: () => void;
  resourceId: string;
}

interface ISecret {
  groupName: string;
  resourceId: string;
  resourceType: ResourceType;
  secretDescription: string;
  secretKey: string;
}

interface IAddSecretsProps extends ISecret {
  isUpdate: boolean;
  secretValue: string;
  closeModal: () => void;
  handleSubmitSecret: () => void;
  isDuplicated: (key: string) => boolean;
}

interface IAddSecretsFPProps extends ISecret {
  isUpdate: boolean;
  secretValue: string;
  modalRef: IUseModal;
  handleSubmitSecret: () => void;
  isDuplicated: (key: string) => boolean;
}

interface ISecretValueProps extends ISecret {
  enableCondition: boolean;
  onEdit: (key: string, description: string) => void;
}

export type {
  IAddSecretsProps,
  IAddSecretsFPProps,
  ISecretsProps,
  ISecretValueProps,
};
