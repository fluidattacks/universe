import type { ApolloQueryResult } from "@apollo/client";
import { useQuery } from "@apollo/client";

import { GET_ENVIRONMENT_URL, GET_ROOT_SECRETS } from "../queries";
import type {
  GetEnvironmentUrlQuery,
  GetEnvironmentUrlQueryVariables,
  GetRootQuery,
  GetRootQueryVariables,
} from "gql/graphql";
import { Logger } from "utils/logger";

interface IEnvironmentSecretData {
  data?: GetEnvironmentUrlQuery;
  refetch: (
    variables?: GetEnvironmentUrlQueryVariables,
  ) => Promise<ApolloQueryResult<GetEnvironmentUrlQuery>>;
}

interface ISecretData {
  data?: GetRootQuery;
  refetch: (
    variables?: Partial<GetRootQueryVariables>,
  ) => Promise<ApolloQueryResult<GetRootQuery>>;
}

const useEnvironmentDataSecrets = ({
  groupName,
  urlId,
}: GetEnvironmentUrlQueryVariables): IEnvironmentSecretData => {
  const { data, refetch } = useQuery(GET_ENVIRONMENT_URL, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.error("Couldn't load secrets", error);
      });
    },
    variables: { groupName, urlId },
  });

  return { data, refetch };
};

const useRootDataSecrets = ({
  groupName,
  rootId,
}: GetRootQueryVariables): ISecretData => {
  const { data, refetch } = useQuery(GET_ROOT_SECRETS, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        if (error.message !== "Exception - Access denied or root not found") {
          Logger.error("Couldn't load secrets", error);
        }
      });
    },
    variables: { groupName, rootId },
  });

  return { data, refetch };
};

export { useEnvironmentDataSecrets, useRootDataSecrets };
