import { useLazyQuery } from "@apollo/client";
import { useAbility } from "@casl/react";
import { Modal, ModalConfirm, useModal } from "@fluidattacks/design";
import isNil from "lodash/isNil";
import {
  StrictMode,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from "react";
import { useTranslation } from "react-i18next";

import { AddSecret } from "./add-secret";
import { useEnvironmentDataSecrets, useRootDataSecrets } from "./hooks";
import { SecretValue } from "./secret-value";
import type { ISecretsProps } from "./types";

import { GET_SECRET_BY_KEY } from "../queries";
import type { ISecretItem, ISecretRow } from "../types";
import { Table } from "components/table";
import { authContext } from "context/auth";
import { authzPermissionsContext } from "context/authz/config";
import { Role as userRoleComp } from "features/user-role";
import { ResourceType } from "gql/graphql";
import { useTable } from "hooks";
import { useAudit } from "hooks/use-audit";
import { Logger } from "utils/logger";

const Secrets = ({
  environmentSecret = false,
  isOpen = false,
  groupName,
  onCloseModal,
  resourceId,
}: ISecretsProps): JSX.Element => {
  const { t } = useTranslation();
  const { userEmail } = useContext(authContext);
  const userRole = userRoleComp();
  const dataQuery = {
    [ResourceType.Root]: useRootDataSecrets({
      groupName,
      rootId: resourceId,
    }),
    [ResourceType.Url]: useEnvironmentDataSecrets({
      groupName,
      urlId: resourceId,
    }),
  };

  const defaultCurrentRow = useMemo((): ISecretRow => {
    return { description: "", key: "" };
  }, []);
  const permissions = useAbility(authzPermissionsContext);
  const canAddSecret = permissions.can(
    "integrates_api_mutations_add_secret_mutate",
  );
  const tableRef = useTable("tblGitRootSecrets");

  const [currentRow, setCurrentRow] = useState(defaultCurrentRow);
  const [secretValue, setSecretValue] = useState("");
  const [isUpdate, setIsUpdate] = useState(false);
  const addSecretModal = useModal("add-secret-modal");
  const environmentSecretModal = useModal("environment-secret-modal");
  const resourceType = environmentSecret ? ResourceType.Url : ResourceType.Root;

  const { addAuditEvent } = useAudit();
  useEffect((): void => {
    addAuditEvent(
      resourceType === ResourceType.Url
        ? "RootEnvironment.Secrets"
        : "Root.Secrets",
      resourceId,
    );
  }, [addAuditEvent, resourceId, resourceType]);

  // Graphql operations
  const { data, refetch } = dataQuery[resourceType];
  const [requestSecretValue] = useLazyQuery(GET_SECRET_BY_KEY, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.error("Couldn't load secrets", error);
      });
    },
  });

  // Data management
  const handleRequestSecretValue = useCallback(
    async (secretKey: string): Promise<string | null> => {
      try {
        const result = await requestSecretValue({
          variables: {
            groupName,
            resourceId,
            resourceType,
            secretKey,
          },
        });

        if (result.data?.secret) {
          const { value } = result.data.secret;
          setSecretValue(value);

          return value;
        }
      } catch (error) {
        Logger.error("Couldn't load secrets", error);
      }

      return null;
    },
    [groupName, requestSecretValue, resourceId, resourceType],
  );
  const editCurrentRow = useCallback(
    async (key: string, description: string): Promise<void> => {
      setCurrentRow({ description, key });
      setIsUpdate(true);
      await handleRequestSecretValue(key);
      addSecretModal.open();
    },
    [addSecretModal, handleRequestSecretValue],
  );

  const environmentUrlData =
    data && "environmentUrl" in data ? data.environmentUrl : undefined;
  const rootsData = data && "root" in data ? data.root : undefined;
  const rootWithSecrets =
    rootsData?.__typename === "GitRoot" || rootsData?.__typename === "URLRoot"
      ? rootsData.secrets
      : [];
  const secretsData = environmentSecret
    ? environmentUrlData?.secrets
    : rootWithSecrets;

  const secretsDataSet = useMemo(
    (): ISecretItem[] =>
      isNil(secretsData)
        ? []
        : secretsData.map((item): ISecretItem => {
            const allowedRoles: string[] = [
              "Admin",
              "User Manager",
              "Customer Manager",
              "Reattacker",
              "Resourcer",
              "Hacker",
              "Architect",
            ];

            return {
              description: item?.description,
              element: (
                <SecretValue
                  enableCondition={
                    allowedRoles.includes(userRole) || item?.owner === userEmail
                  }
                  groupName={groupName}
                  onEdit={editCurrentRow}
                  resourceId={resourceId}
                  resourceType={resourceType}
                  secretDescription={item?.description ?? ""}
                  secretKey={item?.key ?? ""}
                />
              ),
              key: item?.key,
              owner: item?.owner,
            } as ISecretItem;
          }),
    [
      editCurrentRow,
      groupName,
      resourceId,
      resourceType,
      secretsData,
      userEmail,
      userRole,
    ],
  );

  const closeModal = useCallback((): void => {
    setIsUpdate(false);
    setCurrentRow(defaultCurrentRow);
    addSecretModal.close();
  }, [addSecretModal, defaultCurrentRow]);

  const openModal = useCallback((): void => {
    addSecretModal.open();
  }, [addSecretModal]);

  const isSecretDuplicated = useCallback(
    (key: string): boolean => {
      return secretsDataSet.some((item): boolean => item.key === key);
    },
    [secretsDataSet],
  );

  const secret = (
    <StrictMode>
      <AddSecret
        groupName={groupName}
        handleSubmitSecret={refetch}
        isDuplicated={isSecretDuplicated}
        isUpdate={isUpdate}
        modalRef={{ ...addSecretModal, close: closeModal }}
        resourceId={resourceId}
        resourceType={resourceType}
        secretDescription={currentRow.description}
        secretKey={currentRow.key}
        secretValue={isUpdate ? secretValue : ""}
      />
      <Table
        columns={[
          {
            accessorKey: "key",
            header: t("group.scope.git.repo.credentials.secrets.key"),
          },
          {
            accessorKey: "description",
            header: t("group.scope.git.repo.credentials.secrets.description"),
          },
          {
            accessorKey: "owner",
            header: t("group.scope.git.repo.credentials.secrets.owner"),
          },
          {
            accessorKey: "element",
            cell: (cell): JSX.Element => cell.getValue() as JSX.Element,
            header: "",
          },
        ]}
        data={secretsDataSet}
        tableRef={tableRef}
      />
      <ModalConfirm
        disabled={!canAddSecret}
        id={"add-secret"}
        onCancel={onCloseModal}
        onConfirm={openModal}
        txtConfirm={t("group.scope.git.repo.credentials.secrets.add")}
      />
    </StrictMode>
  );

  if (environmentSecret) {
    return (
      <Modal
        modalRef={{ ...environmentSecretModal, close: onCloseModal, isOpen }}
        size={"md"}
        title={"Secrets"}
      >
        {secret}
      </Modal>
    );
  }

  return secret;
};

export { Secrets };
