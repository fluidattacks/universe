import { useMutation } from "@apollo/client";
import { useAbility } from "@casl/react";
import {
  Form,
  Gap,
  InnerForm,
  Input,
  Modal,
  Text,
  TextArea,
} from "@fluidattacks/design";
import isUndefined from "lodash/isUndefined";
import { Fragment, useCallback } from "react";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";
import { lazy, object, string } from "yup";
import type { InferType, Lazy, Schema } from "yup";

import { ADD_SECRET, UPDATE_SECRET } from "../../queries";
import type { IAddSecretsFPProps } from "../types";
import { authzPermissionsContext } from "context/authz/config";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";
import { translate } from "utils/translations/translate";

function validationSchema(
  duplicateValidator: (key: string) => boolean,
  isUpdate = false,
): Lazy<InferType<Schema>> {
  return lazy(
    (): Schema =>
      object().shape({
        key: string()
          .required(translate.t("validations.required"))
          .isValidFunction((value: string | undefined): boolean => {
            if (isUndefined(value) || isUpdate) {
              return true;
            }

            return !duplicateValidator(value);
          }, translate.t("validations.duplicateSecret")),
        value: string().required(translate.t("validations.required")),
      }),
  );
}

const AddSecret = ({
  groupName,
  isUpdate,
  modalRef,
  secretDescription,
  secretKey,
  resourceId,
  resourceType,
  secretValue,
  isDuplicated,
  handleSubmitSecret,
}: IAddSecretsFPProps): JSX.Element => {
  const { t } = useTranslation();
  const { close } = modalRef;
  const theme = useTheme();
  const permissions = useAbility(authzPermissionsContext);
  const canAddSecret = permissions.can(
    "integrates_api_mutations_add_secret_mutate",
  );
  const canUpdateSecret = permissions.can(
    "integrates_api_mutations_update_secret_mutate",
  );

  const [addSecret] = useMutation(ADD_SECRET, {
    onCompleted: (): void => {
      msgSuccess(
        t("group.scope.git.repo.credentials.secrets.successAdd"),
        t("group.scope.git.repo.credentials.secrets.successTitle"),
      );
      handleSubmitSecret();
      close();
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.error("Couldn't add secret", error);
      });
    },
  });

  const [updateSecret] = useMutation(UPDATE_SECRET, {
    onCompleted: (): void => {
      msgSuccess(
        t("group.scope.git.repo.credentials.secrets.successUpdate"),
        t("group.scope.git.repo.credentials.secrets.successTitle"),
      );
      handleSubmitSecret();
      close();
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.error("Couldn't update secret", error);
      });
    },
  });

  const handleSecretSubmit = useCallback(
    async ({
      description,
      key,
      value,
    }: {
      description: string;
      key: string;
      value: string;
    }): Promise<void> => {
      await addSecret({
        variables: {
          description,
          groupName,
          key,
          resourceId,
          resourceType,
          value,
        },
      });
    },
    [addSecret, groupName, resourceId, resourceType],
  );
  const handleUpdateSubmit = useCallback(
    async ({
      description,
      key,
      value,
    }: {
      description: string;
      key: string;
      value: string;
    }): Promise<void> => {
      await updateSecret({
        variables: {
          description,
          groupName,
          key,
          resourceId,
          resourceType,
          value,
        },
      });
    },
    [updateSecret, groupName, resourceId, resourceType],
  );

  return (
    <Modal
      id={"gitRootSecret"}
      modalRef={modalRef}
      size={"sm"}
      title={t("group.scope.git.repo.credentials.secrets.title")}
    >
      <Form
        cancelButton={{ onClick: close }}
        confirmButton={{ disabled: !canAddSecret || !canUpdateSecret }}
        defaultValues={{
          description: secretDescription,
          key: secretKey,
          value: secretValue,
        }}
        id={"git-root-add-secret"}
        onSubmit={isUpdate ? handleUpdateSubmit : handleSecretSubmit}
        yupSchema={validationSchema(isDuplicated, isUpdate)}
      >
        <InnerForm>
          {({ formState, getFieldState, register, watch }): JSX.Element => {
            const keyState = getFieldState("key");
            const valueState = getFieldState("value");

            return (
              <Fragment>
                <Text
                  color={theme.palette.gray[800]}
                  fontWeight={"bold"}
                  mb={1.25}
                  size={"lg"}
                >
                  {isUpdate
                    ? t("group.scope.git.repo.credentials.secrets.update")
                    : t("group.scope.git.repo.credentials.secrets.add")}
                </Text>
                <Gap disp={"block"} mh={0} mv={12}>
                  <Input
                    disabled={isUpdate}
                    error={formState.errors.key?.message?.toString()}
                    isTouched={keyState.isTouched}
                    isValid={!keyState.invalid}
                    label={"Key"}
                    name={"key"}
                    register={register}
                    required={true}
                    type={"text"}
                  />
                  <TextArea
                    error={formState.errors.value?.message?.toString()}
                    isTouched={valueState.isTouched}
                    isValid={!valueState.invalid}
                    label={t("group.scope.git.repo.credentials.secrets.value")}
                    maskValue={true}
                    maxLength={20000}
                    name={"value"}
                    register={register}
                    required={true}
                    watch={watch}
                  />
                  <TextArea
                    label={t(
                      "group.scope.git.repo.credentials.secrets.description",
                    )}
                    maxLength={20000}
                    name={"description"}
                    register={register}
                  />
                </Gap>
              </Fragment>
            );
          }}
        </InnerForm>
      </Form>
    </Modal>
  );
};

export { AddSecret };
