import { PureAbility } from "@casl/ability";
import { screen, waitFor, within } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import { Route, Routes } from "react-router-dom";

import { IPRoots } from ".";
import {
  ACTIVATE_ROOT,
  ADD_IP_ROOT,
  DEACTIVATE_ROOT,
  GET_IP_ROOTS,
  UPDATE_IP_ROOT,
} from "../queries";
import { authzPermissionsContext } from "context/authz/config";
import {
  type ActivateRootMutation,
  type AddIpRootMutation as AddIpRoot,
  type DeactivateRootMutation as DeactivateRoot,
  type GetIpRootsQuery as GetIpRoots,
  ResourceState,
  RootDeactivationReason,
  type UpdateIpRootMutation,
} from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";
import { msgError } from "utils/notifications";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();

  return mockedNotifications;
});

describe("iPRoots", (): void => {
  // eslint-disable-next-line jest/no-hooks
  beforeEach((): void => {
    jest.spyOn(console, "warn").mockImplementation();
  });

  const graphqlMocked = graphql.link(LINK);
  const initialIpRootsMockGlobal = graphqlMocked.query(
    GET_IP_ROOTS,
    (): StrictResponse<{ data: GetIpRoots }> => {
      return HttpResponse.json({
        data: {
          __typename: "Query",
          ...{
            group: {
              __typename: "Group",
              ipRoots: {
                __typename: "IPRootsConnection",
                edges: [
                  {
                    __typename: "IPRootEdge",
                    node: {
                      __typename: "IPRoot",
                      address: "127.0.0.1",
                      id: "d312f0b9-da49-4d2b-a881-bed438875e99",
                      nickname: "ip_root_1",
                      state: ResourceState.Active,
                    },
                  },
                  {
                    __typename: "IPRootEdge",
                    node: {
                      __typename: "IPRoot",
                      address: "192.168.50.111",
                      id: "80596eb9-8177-42bb-9f67-bea2101fe7c5",
                      nickname: "testing_ip_root",
                      state: ResourceState.Inactive,
                    },
                  },
                ],
                pageInfo: {
                  endCursor:
                    "ROOT#80596eb9-8177-42bb-9f67-bea2101fe7c5#GROUP#grouptest",
                  hasNextPage: false,
                },
                total: 2,
              },
              name: "grouptest",
            },
          },
        },
      });
    },
  );

  it("should render IP roots", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <authzPermissionsContext.Provider value={new PureAbility([])}>
        <IPRoots groupName={"grouptest"} />
      </authzPermissionsContext.Provider>,
      { mocks: [initialIpRootsMockGlobal] },
    );

    expect(screen.queryByRole("table")).toBeInTheDocument();

    await waitFor((): void => {
      expect(
        screen
          .queryAllByRole("row")[0]
          .textContent?.replace(/[^a-zA-Z ]/gu, ""),
      ).toStrictEqual(
        [
          "group.scope.ip.address",
          "group.scope.ip.nickname",
          "group.scope.common.state",
        ]
          .join("")
          .replace(/[^a-zA-Z ]/gu, ""),
      );
    });
    await waitFor((): void => {
      expect(
        screen
          .queryAllByRole("row")[1]
          .textContent?.replace(/[^a-zA-Z ]/gu, ""),
      ).toStrictEqual(
        ["127.0.0.1", "ip_root_1", "Active"]
          .join("")
          .replace(/[^a-zA-Z ]/gu, ""),
      );
    });

    jest.clearAllMocks();
  });

  it("should add ip roots", async (): Promise<void> => {
    expect.hasAssertions();

    const mutationMock = graphqlMocked.mutation(
      ADD_IP_ROOT,
      ({ variables }): StrictResponse<IErrorMessage | { data: AddIpRoot }> => {
        const { groupName, nickname, address } = variables;

        if (
          groupName === "grouptest" &&
          nickname === "ip_root_2" &&
          address === "127.0.0.0"
        ) {
          return HttpResponse.json({
            data: {
              addIpRoot: { __typename: "AddRootPayload", success: true },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error adding roots")],
        });
      },
    );
    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            { action: "integrates_api_mutations_add_ip_root_mutate" },
          ])
        }
      >
        <Routes>
          <Route
            element={<IPRoots groupName={"grouptest"} />}
            path={"/orgs/:organizationName/groups/:groupName/scope"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/grouptest/scope"],
        },
        mocks: [initialIpRootsMockGlobal, mutationMock],
      },
    );

    expect(screen.queryByRole("table")).toBeInTheDocument();

    await waitFor((): void => {
      expect(
        screen.getByRole("button", { name: "group.scope.common.add" }),
      ).toBeInTheDocument();
    });

    await userEvent.click(
      screen.getByRole("button", { name: "group.scope.common.add" }),
    );

    await waitFor((): void => {
      expect(
        screen.getByRole("textbox", { name: "address" }),
      ).toBeInTheDocument();
    });

    expect(screen.getByRole("button", { name: "Confirm" })).toBeDisabled();

    await userEvent.type(
      screen.getByRole("textbox", { name: "address" }),
      "127.0.0.0",
    );

    await userEvent.type(
      screen.getByRole("textbox", { name: "nickname" }),
      "ip_root_2",
    );

    await waitFor((): void => {
      expect(
        screen.getByRole("button", { name: "Confirm" }),
      ).not.toBeDisabled();
    });
    await userEvent.click(screen.getByText("Confirm"));

    expect(msgError).toHaveBeenCalledTimes(0);

    jest.clearAllMocks();
  });

  it("should handle error when adding ip roots", async (): Promise<void> => {
    expect.hasAssertions();

    const mutationMock = graphqlMocked.mutation(
      ADD_IP_ROOT,
      ({ variables }): StrictResponse<IErrorMessage | { data: AddIpRoot }> => {
        const { groupName, nickname, address } = variables;

        if (
          groupName === "grouptest" &&
          nickname === "ip_root_2" &&
          address === "127.0.0.1"
        ) {
          return HttpResponse.json({
            errors: [
              new GraphQLError(
                "Exception - Root with the same URL/branch already exists",
              ),
            ],
          });
        }

        if (
          groupName === "grouptest" &&
          nickname === "ip_root_1" &&
          address === "127.0.0.0"
        ) {
          return HttpResponse.json({
            errors: [
              new GraphQLError(
                "Exception - Root with the same nickname already exists",
              ),
            ],
          });
        }

        if (
          groupName === "grouptest" &&
          nickname === "ip_root_2" &&
          address === "615.0.0.0"
        ) {
          return HttpResponse.json({
            errors: [new GraphQLError("Exception - Error value is not valid")],
          });
        }

        return HttpResponse.json({
          data: {
            addIpRoot: { __typename: "AddRootPayload", success: true },
          },
        });
      },
    );
    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            { action: "integrates_api_mutations_add_ip_root_mutate" },
          ])
        }
      >
        <Routes>
          <Route
            element={<IPRoots groupName={"grouptest"} />}
            path={"/orgs/:organizationName/groups/:groupName/scope"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/grouptest/scope"],
        },
        mocks: [initialIpRootsMockGlobal, mutationMock],
      },
    );

    expect(screen.queryByRole("table")).toBeInTheDocument();

    await waitFor((): void => {
      expect(
        screen.getByRole("button", { name: "group.scope.common.add" }),
      ).toBeInTheDocument();
    });

    await userEvent.click(
      screen.getByRole("button", { name: "group.scope.common.add" }),
    );

    // Exception - Root with the same nickname already exists
    await userEvent.type(
      screen.getByRole("textbox", { name: "address" }),
      "127.0.0.0",
    );
    await userEvent.type(
      screen.getByRole("textbox", { name: "nickname" }),
      "ip_root_1",
    );
    await userEvent.click(screen.getByText("Confirm"));

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        "group.scope.common.errors.duplicateNickname",
      );
    });

    // Exception - Root with the same URL/branch already exists
    await userEvent.clear(screen.getByRole("textbox", { name: "address" }));
    await userEvent.type(
      screen.getByRole("textbox", { name: "address" }),
      "127.0.0.1",
    );
    await userEvent.clear(screen.getByRole("textbox", { name: "nickname" }));
    await userEvent.type(
      screen.getByRole("textbox", { name: "nickname" }),
      "ip_root_2",
    );
    await userEvent.click(screen.getByText("Confirm"));

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        "group.scope.common.errors.duplicateUrl",
      );
    });

    // Exception - Error value is not valid
    await userEvent.clear(screen.getByRole("textbox", { name: "address" }));
    await userEvent.type(
      screen.getByRole("textbox", { name: "address" }),
      "615.0.0.0",
    );
    await userEvent.click(screen.getByText("Confirm"));

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith("group.scope.ip.errors.invalid");
    });

    jest.clearAllMocks();
  });

  it("should update ip roots", async (): Promise<void> => {
    expect.hasAssertions();

    const mutationMock = graphqlMocked.mutation(
      UPDATE_IP_ROOT,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: UpdateIpRootMutation }> => {
        const { groupName, rootId } = variables;

        if (
          groupName === "grouptest" &&
          rootId === "d312f0b9-da49-4d2b-a881-bed438875e99"
        ) {
          return HttpResponse.json({
            data: {
              updateIpRoot: { success: true },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error updating roots")],
        });
      },
    );
    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            { action: "integrates_api_mutations_add_ip_root_mutate" },
            { action: "integrates_api_mutations_update_ip_root_mutate" },
          ])
        }
      >
        <Routes>
          <Route
            element={<IPRoots groupName={"grouptest"} />}
            path={"/orgs/:organizationName/groups/:groupName/scope"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/grouptest/scope"],
        },
        mocks: [initialIpRootsMockGlobal, mutationMock],
      },
    );

    expect(screen.queryByRole("table")).toBeInTheDocument();

    await waitFor((): void => {
      expect(
        screen.getByRole("cell", { name: "127.0.0.1" }),
      ).toBeInTheDocument();
    });
    await userEvent.click(screen.getByRole("cell", { name: "127.0.0.1" }));

    expect(screen.getByRole("textbox", { name: "address" })).toBeDisabled();
    expect(screen.getByRole("button", { name: "Confirm" })).toBeDisabled();

    await userEvent.type(
      screen.getByRole("textbox", { name: "nickname" }),
      "ip_root_2_test",
    );

    await waitFor((): void => {
      expect(
        screen.getByRole("button", { name: "Confirm" }),
      ).not.toBeDisabled();
    });
    await userEvent.click(screen.getByText("Confirm"));

    expect(msgError).toHaveBeenCalledTimes(0);

    jest.clearAllMocks();
  });

  it("should handle error when updating ip roots", async (): Promise<void> => {
    expect.hasAssertions();

    const mutationMock = graphqlMocked.mutation(
      UPDATE_IP_ROOT,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: UpdateIpRootMutation }> => {
        const { groupName, nickname, rootId } = variables;

        if (
          groupName === "grouptest" &&
          nickname === "testing_ip_root" &&
          rootId === "d312f0b9-da49-4d2b-a881-bed438875e99"
        ) {
          return HttpResponse.json({
            errors: [
              new GraphQLError(
                "Exception - Root with the same nickname already exists",
              ),
            ],
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error updating roots")],
        });
      },
    );
    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            { action: "integrates_api_mutations_add_ip_root_mutate" },
            { action: "integrates_api_mutations_update_ip_root_mutate" },
          ])
        }
      >
        <Routes>
          <Route
            element={<IPRoots groupName={"grouptest"} />}
            path={"/orgs/:organizationName/groups/:groupName/scope"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/grouptest/scope"],
        },
        mocks: [initialIpRootsMockGlobal, mutationMock],
      },
    );

    expect(screen.queryByRole("table")).toBeInTheDocument();

    await waitFor((): void => {
      expect(
        screen.getByRole("cell", { name: "127.0.0.1" }),
      ).toBeInTheDocument();
    });
    await userEvent.click(screen.getByRole("cell", { name: "127.0.0.1" }));

    expect(screen.getByRole("button", { name: "Confirm" })).toBeDisabled();

    await userEvent.type(
      screen.getByRole("textbox", { name: "nickname" }),
      "testing_ip",
    );

    await waitFor((): void => {
      expect(
        screen.getByRole("button", { name: "Confirm" }),
      ).not.toBeDisabled();
    });
    await userEvent.click(screen.getByText("Confirm"));

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith("groupAlerts.errorTextsad");
    });

    await userEvent.clear(screen.getByRole("textbox", { name: "nickname" }));

    await userEvent.type(
      screen.getByRole("textbox", { name: "nickname" }),
      "testing_ip_root",
    );
    await userEvent.click(screen.getByText("Confirm"));

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        "group.scope.common.errors.duplicateNickname",
      );
    });
    jest.clearAllMocks();
  });

  it("should activate ip roots", async (): Promise<void> => {
    expect.hasAssertions();

    const mutationMock = graphqlMocked.mutation(
      ACTIVATE_ROOT,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: ActivateRootMutation }> => {
        const { groupName, id } = variables;

        if (
          groupName === "grouptest" &&
          id === "80596eb9-8177-42bb-9f67-bea2101fe7c5"
        ) {
          return HttpResponse.json({
            data: {
              activateRoot: { success: true },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error activating roots")],
        });
      },
    );

    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            { action: "integrates_api_mutations_add_ip_root_mutate" },
            { action: "integrates_api_mutations_update_ip_root_mutate" },
            { action: "integrates_api_mutations_activate_root_mutate" },
          ])
        }
      >
        <Routes>
          <Route
            element={<IPRoots groupName={"grouptest"} />}
            path={"/orgs/:organizationName/groups/:groupName/scope"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/grouptest/scope"],
        },
        mocks: [initialIpRootsMockGlobal, mutationMock],
      },
    );

    expect(screen.queryByRole("table")).toBeInTheDocument();

    await waitFor((): void => {
      expect(
        within(
          screen.queryAllByRole("table")[0],
        ).getAllByRole<HTMLInputElement>("checkbox")[1].checked,
      ).toBe(false);
    });
    await userEvent.click(
      within(screen.queryAllByRole("table")[0]).getAllByRole("checkbox")[1],
    );

    expect(msgError).toHaveBeenCalledTimes(0);

    jest.clearAllMocks();
  });

  it("should deactivate ip roots", async (): Promise<void> => {
    expect.hasAssertions();

    const mutationMock = graphqlMocked.mutation(
      DEACTIVATE_ROOT,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: DeactivateRoot }> => {
        const { groupName, id, other, reason: reasonFromVariables } = variables;
        if (
          groupName === "unittesting" &&
          id === "8493c82f-2860-4902-86fa-75b0fef76034" &&
          other === "" &&
          reasonFromVariables === RootDeactivationReason.RegisteredByMistake
        ) {
          return HttpResponse.json({
            data: {
              deactivateRoot: { __typename: "SimplePayload", success: true },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error deactivating roots")],
        });
      },
    );
    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            { action: "integrates_api_mutations_add_ip_root_mutate" },
            { action: "integrates_api_mutations_update_ip_root_mutate" },
            { action: "integrates_api_mutations_activate_root_mutate" },
          ])
        }
      >
        <Routes>
          <Route
            element={<IPRoots groupName={"grouptest"} />}
            path={"/orgs/:organizationName/groups/:groupName/scope"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/grouptest/scope"],
        },
        mocks: [initialIpRootsMockGlobal, mutationMock],
      },
    );

    expect(screen.queryByRole("table")).toBeInTheDocument();

    await waitFor((): void => {
      expect(
        within(
          screen.queryAllByRole("table")[0],
        ).getAllByRole<HTMLInputElement>("checkbox")[0].checked,
      ).toBe(true);
    });
    await userEvent.click(
      within(screen.queryAllByRole("table")[0]).getAllByRole("checkbox")[0],
    );

    expect(
      screen.getByText(/group.scope.common.deactivation.title/iu),
    ).toBeInTheDocument();

    expect(screen.getByText("Confirm")).toBeDisabled();

    await userEvent.click(
      screen.getByText("group.scope.common.deactivation.reason.label"),
    );
    await userEvent.click(
      screen.getByText("group.scope.common.deactivation.reason.mistake"),
    );
    await waitFor((): void => {
      expect(screen.getByText("Confirm")).not.toBeDisabled();
    });
    await userEvent.click(screen.getByRole("button", { name: "Confirm" }));
    await waitFor((): void => {
      expect(
        screen.queryByText("group.scope.common.confirm"),
      ).toBeInTheDocument();
    });
    await userEvent.click(
      screen.getAllByRole("button", { name: "Confirm" })[1],
    );

    jest.clearAllMocks();
  });

  it("should handle error activating ip roots", async (): Promise<void> => {
    expect.hasAssertions();

    const mutationMock = graphqlMocked.mutation(
      ACTIVATE_ROOT,
      (): StrictResponse<IErrorMessage> => {
        return HttpResponse.json({
          errors: [new GraphQLError("Error activating roots")],
        });
      },
    );

    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            { action: "integrates_api_mutations_add_ip_root_mutate" },
            { action: "integrates_api_mutations_update_ip_root_mutate" },
            { action: "integrates_api_mutations_activate_root_mutate" },
          ])
        }
      >
        <Routes>
          <Route
            element={<IPRoots groupName={"grouptest"} />}
            path={"/orgs/:organizationName/groups/:groupName/scope"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/grouptest/scope"],
        },
        mocks: [initialIpRootsMockGlobal, mutationMock],
      },
    );

    expect(screen.queryByRole("table")).toBeInTheDocument();

    await waitFor((): void => {
      expect(
        within(
          screen.queryAllByRole("table")[0],
        ).getAllByRole<HTMLInputElement>("checkbox")[1].checked,
      ).toBe(false);
    });
    await userEvent.click(
      within(screen.queryAllByRole("table")[0]).getAllByRole("checkbox")[1],
    );

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith("groupAlerts.errorTextsad");
    });
    jest.clearAllMocks();
  });
});
