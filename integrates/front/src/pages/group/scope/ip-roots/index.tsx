import { useMutation, useQuery } from "@apollo/client";
import { useAbility } from "@casl/react";
import { Button, useConfirmDialog, useModal } from "@fluidattacks/design";
import type { Row } from "@tanstack/react-table";
import isNil from "lodash/isNil";
import { useCallback, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { ManagementModal } from "./management-modal";

import { DeactivateRootModal } from "../deactivate-root-modal";
import { changeFormatter } from "../formatters";
import { InternalSurfaceButton } from "../internal-surface-button";
import {
  ACTIVATE_ROOT,
  ADD_IP_ROOT,
  GET_IP_ROOTS,
  GET_IP_ROOTS_FRAGMENT,
  UPDATE_IP_ROOT,
} from "../queries";
import type { IIPRootAttr } from "../types";
import { Table } from "components/table";
import { Can } from "context/authz/can";
import { authzPermissionsContext } from "context/authz/config";
import { statusFormatter } from "features/vulnerabilities/formatters/status";
import { getFragmentData } from "gql/fragment-masking";
import { useDebouncedCallback, useTable } from "hooks";
import { useAudit } from "hooks/use-audit";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";

interface IIPRootsProps {
  readonly groupName: string;
}
const TIMEOUT_DURATION = 1000;
const DEBOUNCE_DELAY = 800;

export const IPRoots: React.FC<IIPRootsProps> = ({
  groupName,
}): JSX.Element => {
  const { t } = useTranslation();

  const tableRef = useTable("tblIPRoots");
  const { ConfirmDialog } = useConfirmDialog();

  const [currentRow, setCurrentRow] = useState<IIPRootAttr | undefined>(
    undefined,
  );
  const [isManagingRoot, setIsManagingRoot] = useState<
    false | { mode: "ADD" | "EDIT" }
  >(false);

  const closeModal = useCallback((): void => {
    setIsManagingRoot(false);
    setCurrentRow(undefined);
  }, []);
  const [currentRootId, setCurrentRootId] = useState("");
  const deactivateRootModal = useModal("deactivation-root-modal");
  const managementModal = useModal("management-modal");
  const closeDeactivationModal = useCallback((): void => {
    deactivateRootModal.close();
    setCurrentRootId("");
  }, [deactivateRootModal]);
  const openAddModal = useCallback((): void => {
    managementModal.open();
    setIsManagingRoot({ mode: "ADD" });
  }, [managementModal]);

  function handleRowClick(
    rowInfo: Row<IIPRootAttr>,
  ): (event: React.FormEvent) => void {
    return (event: React.FormEvent): void => {
      if (rowInfo.original.state === "ACTIVE") {
        setCurrentRow(rowInfo.original);
        managementModal.open();
        setIsManagingRoot({ mode: "EDIT" });
      }
      event.preventDefault();
    };
  }

  const { addAuditEvent } = useAudit();
  // GraphQL operations
  const { data, fetchMore, refetch } = useQuery(GET_IP_ROOTS, {
    fetchPolicy: "network-only",
    nextFetchPolicy: "cache-first",
    onCompleted: (): void => {
      addAuditEvent("Group.Roots", groupName);
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.error("Couldn't load IP roots", error);
      });
    },
    variables: {
      first: 150,
      groupName,
    },
  });
  const onUpdate = useCallback((): void => {
    setTimeout((): void => {
      void refetch();
    }, TIMEOUT_DURATION);
  }, [refetch]);

  const rootsResult = getFragmentData(GET_IP_ROOTS_FRAGMENT, data);

  const [addIpRoot] = useMutation(ADD_IP_ROOT, {
    onCompleted: (): void => {
      onUpdate();
      closeModal();
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        switch (error.message) {
          case "Exception - Error value is not valid":
            msgError(t("group.scope.ip.errors.invalid"));
            break;
          case "Exception - Root with the same URL/branch already exists":
            msgError(t("group.scope.common.errors.duplicateUrl"));
            break;
          case "Exception - Root with the same nickname already exists":
            msgError(t("group.scope.common.errors.duplicateNickname"));
            break;
          default:
            msgError(t("groupAlerts.errorTextsad"));
            Logger.error("Couldn't add ip roots", error);
        }
      });
    },
  });
  const [updateIpRoot] = useMutation(UPDATE_IP_ROOT, {
    onCompleted: (): void => {
      onUpdate();
      closeModal();
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        if (
          error.message ===
          "Exception - Root with the same nickname already exists"
        ) {
          msgError(t("group.scope.common.errors.duplicateNickname"));
        } else {
          msgError(t("groupAlerts.errorTextsad"));
          Logger.error("Couldn't update ip roots", error);
        }
      });
    },
  });

  const handleIpSubmit = useCallback(
    async ({
      address,
      id,
      nickname,
    }: {
      address: string;
      id: string;
      nickname: string;
    }): Promise<void> => {
      if (isManagingRoot !== false) {
        if (isManagingRoot.mode === "ADD") {
          await addIpRoot({
            variables: {
              address: address.trim(),
              groupName,
              nickname,
            },
          });
        } else {
          await updateIpRoot({
            variables: { groupName, nickname, rootId: id },
          });
        }
      }
    },
    [addIpRoot, groupName, isManagingRoot, updateIpRoot],
  );

  const [activateRoot] = useMutation(ACTIVATE_ROOT, {
    onCompleted: (): void => {
      onUpdate();
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        if (
          error.message ===
          "Exception - Root with the same URL/branch already exists"
        ) {
          msgError(t("group.scope.url.errors.invalid"));
        } else {
          msgError(t("groupAlerts.errorTextsad"));
          Logger.error("Couldn't activate ip root", error);
        }
      });
    },
  });

  const permissions = useAbility(authzPermissionsContext);
  const canUpdateRootState = permissions.can(
    "integrates_api_mutations_activate_root_mutate",
  );

  const handleStateUpdate = (row: Record<string, string>): void => {
    if (row.state === "ACTIVE") {
      deactivateRootModal.open();
      setCurrentRootId(row.id);
    } else {
      void activateRoot({ variables: { groupName, id: row.id } });
    }
  };

  const handleSearch = useDebouncedCallback((search: string): void => {
    void refetch({ search });
  }, DEBOUNCE_DELAY);
  const handleNextPage = useCallback(async (): Promise<void> => {
    const rootsInfo = rootsResult?.group;
    const pageInfo = isNil(rootsInfo)
      ? { endCursor: [], hasNextPage: false }
      : {
          endCursor: rootsInfo.ipRoots.pageInfo.endCursor,
          hasNextPage: rootsInfo.ipRoots.pageInfo.hasNextPage,
        };
    if (pageInfo.hasNextPage) {
      await fetchMore({ variables: { after: pageInfo.endCursor } });
    }
  }, [fetchMore, rootsResult]);

  const { total: size, edges } = rootsResult?.group.ipRoots ?? {
    edges: [],
    total: 0,
  };

  return (
    <React.Fragment>
      <Table
        columns={[
          {
            accessorKey: "address",
            header: String(t("group.scope.ip.address")),
          },
          {
            accessorKey: "nickname",
            header: String(t("group.scope.ip.nickname")),
          },
          {
            accessorKey: "state",
            cell: (cell): JSX.Element =>
              canUpdateRootState
                ? changeFormatter(
                    "ip-root-state",
                    cell.row.original as unknown as Record<string, string>,
                    handleStateUpdate,
                  )
                : statusFormatter(String(cell.getValue())),
            header: String(t("group.scope.common.state")),
          },
        ]}
        data={edges.map((edge): IIPRootAttr => edge.node as IIPRootAttr)}
        onNextPage={handleNextPage}
        onRowClick={
          permissions.can("integrates_api_mutations_update_ip_root_mutate")
            ? handleRowClick
            : undefined
        }
        onSearch={handleSearch}
        options={{ enableSorting: true, size }}
        rightSideComponents={
          <React.Fragment>
            <Can do={"integrates_api_mutations_add_ip_root_mutate"}>
              <Button icon={"add"} onClick={openAddModal} variant={"primary"}>
                {t("group.scope.common.add")}
              </Button>
            </Can>
            <InternalSurfaceButton />
            <ConfirmDialog />
          </React.Fragment>
        }
        tableRef={tableRef}
      />
      {isManagingRoot === false ? undefined : (
        <ManagementModal
          initialValues={
            isManagingRoot.mode === "EDIT" ? currentRow : undefined
          }
          modalRef={{ ...managementModal }}
          onClose={closeModal}
          onSubmit={handleIpSubmit}
        />
      )}
      <DeactivateRootModal
        groupName={groupName}
        modalRef={{
          ...deactivateRootModal,
          close: closeDeactivationModal,
        }}
        onUpdate={onUpdate}
        rootId={currentRootId}
      />
      <ConfirmDialog />
    </React.Fragment>
  );
};
