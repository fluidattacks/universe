import { useMutation } from "@apollo/client";
import { Button } from "@fluidattacks/design";
import mixpanel from "mixpanel-browser";
import type { FormEvent } from "react";
import { useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";

import { DOWNLOAD_GIT_ROOTS_FILE_MUTATION } from "../../queries";
import { Authorize } from "components/@core/authorize";
import { useAudit } from "hooks/use-audit";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";
import { openUrl } from "utils/resource-helpers";

const ExportButton: React.FC = (): JSX.Element => {
  const { groupName } = useParams() as { groupName: string };
  const { t } = useTranslation();

  const { addAuditEvent } = useAudit();
  const [downloadVulnerability] = useMutation(
    DOWNLOAD_GIT_ROOTS_FILE_MUTATION,
    {
      onCompleted: (result): void => {
        if (result.downloadGitRootsFile.success) {
          mixpanel.track("DownloadGitRootsFile", {
            fileName: result.downloadGitRootsFile.url,
            group: groupName,
          });
          addAuditEvent("DownloadGitRootsFile", groupName);
          openUrl(result.downloadGitRootsFile.url);
        }
      },
      onError: (downloadError): void => {
        downloadError.graphQLErrors.forEach(({ message }): void => {
          msgError(t("groupAlerts.errorTextsad"));
          switch (message) {
            case "Unable to upload file to S3 service":
              Logger.warning(
                "An error occurred downloading git roots file while uploading file to S3",
                downloadError,
              );
              break;
            case "Unable to download the requested file":
              Logger.warning(
                "An error occurred downloading git roots file while requesting the file to S3",
                downloadError,
              );
              break;
            default:
              Logger.warning(
                "An error occurred downloading git roots file",
                downloadError,
              );
          }
        });
      },
    },
  );
  const onRequestReport = useCallback(
    (event: FormEvent<HTMLButtonElement>): void => {
      event.stopPropagation();
      void downloadVulnerability({
        variables: {
          groupName,
        },
      });
    },
    [downloadVulnerability, groupName],
  );

  return (
    <React.StrictMode>
      <Authorize
        can={"integrates_api_mutations_download_git_roots_file_mutate"}
      >
        <Button
          icon={"file-export"}
          onClick={onRequestReport}
          tooltip={t("buttons.exportTip", { format: "CSV" })}
          variant={"ghost"}
        >
          {t("buttons.export")}
        </Button>
      </Authorize>
    </React.StrictMode>
  );
};

export { ExportButton };
