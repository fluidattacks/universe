import { Alert, Container } from "@fluidattacks/design";
import { useFormikContext } from "formik";
import { useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import type { IHealthCheckProps } from "./types";

import type { IFormValues } from "../../../../types";
import type { IFieldRootValues } from "../url-section/types";
import { updateHealthCheckConfirm } from "../utils";
import { Checkbox } from "components/checkbox";
import { InputContainer } from "components/input";
import { RadioButton } from "components/radio-button";
import { Have } from "context/authz/have";
import { formatBooleanHealthCheck } from "pages/group/scope/utils";

const HealthCheckSection = ({
  form,
  isEditing,
  isSameHealthCheck,
  setIsSameHealthCheck,
}: Readonly<IHealthCheckProps>): JSX.Element => {
  const { t } = useTranslation();
  const { initialValues, setFieldValue, values } =
    useFormikContext<IFormValues>();

  const onChangeHealthCheck = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>): void => {
      const includesHealthCheckValue = event.target.value;
      const health = formatBooleanHealthCheck(event.target.value);
      void setFieldValue("healthCheckConfirm", []);
      void setFieldValue("includesHealthCheck", health);
      updateHealthCheckConfirm(
        form,
        initialValues,
        isEditing,
        setIsSameHealthCheck,
        {
          branch: undefined,
        } as IFieldRootValues,
        includesHealthCheckValue,
      );
    },
    [form, initialValues, isEditing, setFieldValue, setIsSameHealthCheck],
  );

  return (
    <Have I={"has_advanced"}>
      <InputContainer
        label={t("group.scope.git.healthCheck.confirm")}
        name={"healthCheck"}
        required={true}
        tooltip={t("group.scope.git.healthCheck.tooltip")}
        weight={"bold"}
      >
        <Container display={"flex"} flexDirection={"row"} gap={1}>
          <RadioButton
            defaultChecked={
              formatBooleanHealthCheck(initialValues.includesHealthCheck) ??
              false
            }
            label={"Yes"}
            name={"includesHealthCheck"}
            onChange={onChangeHealthCheck}
            value={"yes"}
            variant={isEditing ? "input" : "formikField"}
          />
          <RadioButton
            defaultChecked={
              formatBooleanHealthCheck(initialValues.includesHealthCheck) ===
              false
            }
            label={"No"}
            name={"includesHealthCheck"}
            onChange={onChangeHealthCheck}
            value={"no"}
            variant={isEditing ? "input" : "formikField"}
          />
        </Container>
      </InputContainer>
      {!isSameHealthCheck &&
      (values.includesHealthCheck === "yes" ||
        values.includesHealthCheck === true) ? (
        <Alert>
          <Checkbox
            alert={true}
            label={t("group.scope.git.healthCheck.accept")}
            name={"healthCheckConfirm"}
            required={true}
            value={"includeA"}
            variant={"formikField"}
          />
        </Alert>
      ) : undefined}
      {!isSameHealthCheck &&
      (values.includesHealthCheck === "no" ||
        values.includesHealthCheck === false) ? (
        <Alert>
          <Container display={"flex"} flexDirection={"column"} gap={0.5}>
            <Checkbox
              alert={true}
              label={t("group.scope.git.healthCheck.rejectA")}
              name={"healthCheckConfirm"}
              required={true}
              value={"rejectA"}
              variant={"formikField"}
            />
            <Checkbox
              alert={true}
              label={t("group.scope.git.healthCheck.rejectB")}
              name={"healthCheckConfirm"}
              required={true}
              value={"rejectB"}
              variant={"formikField"}
            />
            <Checkbox
              alert={true}
              label={t("group.scope.git.healthCheck.rejectC")}
              name={"healthCheckConfirm"}
              required={true}
              value={"rejectC"}
              variant={"formikField"}
            />
          </Container>
        </Alert>
      ) : undefined}
    </Have>
  );
};

export type { IHealthCheckProps };
export { HealthCheckSection };
