import { useMutation } from "@apollo/client";
import {
  Container,
  Form,
  InnerForm,
  Input,
  Modal,
  OutlineContainer,
  RadioButton,
  useConfirmDialog,
} from "@fluidattacks/design";
import { useCallback, useEffect, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { CredentialsSection } from "./credentials-section";
import { validationSchema } from "./validations";

import type {
  IAddDockerImageModalSubmit,
  IAddDockerImageProps,
} from "../types";
import {
  baseCredentials,
  formatAuthentication,
  formatInitialAuthentication,
} from "../utils";
import {
  ADD_GIT_ROOT_DOCKER_IMAGE,
  GET_GIT_ROOT_DOCKER_IMAGES,
  UPDATE_GIT_ROOT_DOCKER_IMAGE,
} from "pages/group/scope/queries";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

const AddDockerImage = ({
  modalRef,
  rootId,
  groupName,
  isEditing,
  initialValues,
}: IAddDockerImageProps): JSX.Element => {
  const { t } = useTranslation();
  const { ConfirmDialog } = useConfirmDialog();
  const { close } = modalRef;
  const defaultCredentials = initialValues?.credentials ?? baseCredentials;

  // Graphql Operations
  const [addDockerImage] = useMutation(ADD_GIT_ROOT_DOCKER_IMAGE, {
    onCompleted: (): void => {
      msgSuccess(
        t("group.scope.git.dockerImages.add.success"),
        t("group.scope.git.dockerImages.add.successTitle"),
      );
      close();
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        switch (error.message) {
          case "Exception - Docker image URI already exists":
            msgError(t("validations.dockerImages.imageAlreadyExists"));
            break;
          case "Exception - Root image not found":
            msgError(t("validations.dockerImages.imageNotFound"));
            break;
          case "Exception - Access denied or credential not found":
            msgError(t("validations.dockerImages.wrongCredentials"));
            break;
          case "Exception - URI format incorrect":
            msgError(t("validations.dockerImages.wrongUriFormat"));
            break;
          default:
            msgError(t("groupAlerts.errorTextsad"));
            Logger.error("Couldn't add Docker image", error);
        }
      });
    },
    refetchQueries: [
      {
        query: GET_GIT_ROOT_DOCKER_IMAGES,
        variables: { groupName, rootId },
      },
    ],
  });

  const [updateDockerImage] = useMutation(UPDATE_GIT_ROOT_DOCKER_IMAGE, {
    onCompleted: (): void => {
      msgSuccess(
        t("group.scope.git.dockerImages.edit.success"),
        t("group.scope.git.dockerImages.edit.successTitle"),
      );
      close();
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        switch (error.message) {
          case "Exception - Root image not found":
            msgError(t("validations.dockerImages.imageNotFound"));
            break;
          case "Exception - Access denied or credential not found":
            msgError(t("validations.dockerImages.wrongCredentials"));
            break;
          default:
            msgError(t("groupAlerts.errorTextsad"));
            Logger.error("Couldn't add Docker image", error);
        }
      });
    },
    refetchQueries: [
      {
        query: GET_GIT_ROOT_DOCKER_IMAGES,
        variables: { groupName, rootId },
      },
    ],
  });

  // Data management
  const [requiresAuthentication, setRequiresAuthentication] = useState(
    Boolean(initialValues?.credentials),
  );
  const [credentialId, setCredentialId] = useState(defaultCredentials.id);

  const onChangeRequiresAuthentication = useCallback(
    (event: React.MouseEvent<HTMLInputElement>): void => {
      const { value } = event.target as HTMLInputElement;
      setRequiresAuthentication(formatAuthentication(value));
    },
    [],
  );
  const onChangeCredential = useCallback((value: string): void => {
    setCredentialId(value);
  }, []);

  useEffect((): void => {
    setRequiresAuthentication(Boolean(initialValues?.credentials));
  }, [initialValues]);

  const handleFormSubmit = useCallback(
    async ({ credentials, uri }: IAddDockerImageModalSubmit): Promise<void> => {
      const variables = {
        groupName,
        rootId,
        uri,
        ...(requiresAuthentication && { credentials: { id: credentials.id } }),
      };

      if (isEditing) {
        await updateDockerImage({ variables });
      } else {
        await addDockerImage({ variables });
      }
    },
    [
      addDockerImage,
      updateDockerImage,
      groupName,
      rootId,
      requiresAuthentication,
      isEditing,
    ],
  );

  return (
    <Modal
      id={"addGitRootDockerImage"}
      modalRef={modalRef}
      size={"sm"}
      title={
        isEditing
          ? t("group.scope.git.dockerImages.edit.title")
          : t("group.scope.git.dockerImages.add.title")
      }
    >
      <Form
        cancelButton={{ onClick: close }}
        confirmButton={{
          disabled: requiresAuthentication && credentialId === "",
        }}
        defaultValues={{
          credentials: defaultCredentials,
          groupName,
          requiresAuthentication: formatInitialAuthentication(
            isEditing,
            initialValues?.credentials,
          ),
          rootId,
          uri: initialValues ? initialValues.uri : "",
        }}
        onSubmit={handleFormSubmit}
        yupSchema={validationSchema}
      >
        <InnerForm<IAddDockerImageModalSubmit>>
          {({ formState, getFieldState, control, register }): JSX.Element => {
            const uriState = getFieldState("uri");

            return (
              <React.Fragment>
                <Input
                  disabled={isEditing}
                  error={formState.errors.uri?.message?.toString()}
                  isTouched={uriState.isTouched}
                  isValid={!uriState.invalid}
                  label={t("group.scope.git.dockerImages.add.inputLabel")}
                  name={"uri"}
                  register={register}
                  required={true}
                  tooltip={t("group.scope.git.dockerImages.add.inputToolTip")}
                />
                <OutlineContainer
                  error={formState.errors.requiresAuthentication?.message?.toString()}
                  htmlFor={"requiresAuthentication"}
                  label={t(
                    "group.scope.git.dockerImages.add.repositoryPrivacy",
                  )}
                  required={true}
                  weight={"bold"}
                >
                  <Container display={"flex"} flexDirection={"row"} gap={1}>
                    <RadioButton
                      {...register("requiresAuthentication")}
                      label={"Yes"}
                      name={"requiresAuthentication"}
                      onClick={onChangeRequiresAuthentication}
                      value={"yes"}
                    />
                    <RadioButton
                      {...register("requiresAuthentication")}
                      label={"No"}
                      name={"requiresAuthentication"}
                      onClick={onChangeRequiresAuthentication}
                      value={"no"}
                    />
                  </Container>
                </OutlineContainer>
                {requiresAuthentication ? (
                  <CredentialsSection
                    control={control}
                    onChange={onChangeCredential}
                  />
                ) : null}
                <ConfirmDialog />
              </React.Fragment>
            );
          }}
        </InnerForm>
      </Form>
    </Modal>
  );
};

export { AddDockerImage };
