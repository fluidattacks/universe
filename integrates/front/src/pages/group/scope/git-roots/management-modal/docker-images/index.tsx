import { useMutation, useQuery } from "@apollo/client";
import { useAbility } from "@casl/react";
import {
  Container,
  IconButton,
  ModalConfirm,
  useConfirmDialog,
} from "@fluidattacks/design";
import type { IUseModal } from "@fluidattacks/design";
import isNil from "lodash/isNil";
import { Fragment, useCallback, useEffect, useMemo, useState } from "react";
import { useTranslation } from "react-i18next";

import { AddDockerImage } from "./add-docker-image";
import type { IDockerImageItem, IDockerImagesProps } from "./types";

import {
  GET_GIT_ROOT_DOCKER_IMAGES,
  GET_GROUP_DOCKER_IMAGES,
  REMOVE_GIT_ROOT_DOCKER_IMAGE,
} from "../../../queries";
import type { IDockerImage } from "../../../types";
import { Table } from "components/table";
import { authzPermissionsContext } from "context/authz/config";
import { useTable } from "hooks";
import { useAudit } from "hooks/use-audit";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

const DockerImages = ({
  groupName,
  initialValues,
  onClose,
}: IDockerImagesProps): JSX.Element => {
  const { t } = useTranslation();
  const { confirm, ConfirmDialog } = useConfirmDialog();
  const tableRef = useTable("tblDockerImages");
  const permissions = useAbility(authzPermissionsContext);
  const canAddDockerImage = permissions.can(
    "integrates_api_mutations_add_root_docker_image_mutate",
  );
  const canEditDockerImage = permissions.can(
    "integrates_api_mutations_update_root_docker_image_mutate",
  );

  const { addAuditEvent } = useAudit();
  useEffect((): void => {
    addAuditEvent("Root.DockerImages", initialValues.id);
  }, [addAuditEvent, initialValues.id]);

  // Graphql operations
  const { data, loading } = useQuery(GET_GIT_ROOT_DOCKER_IMAGES, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.error("Couldn't load Docker images", error);
      });
    },
    variables: { groupName, rootId: initialValues.id },
  });

  const [removeDockerImage, { loading: isRemoving }] = useMutation(
    REMOVE_GIT_ROOT_DOCKER_IMAGE,
    {
      onCompleted: (): void => {
        msgSuccess(
          t("group.scope.git.dockerImages.remove.success"),
          t("group.scope.git.dockerImages.remove.successTitle"),
        );
      },
      onError: ({ graphQLErrors }): void => {
        graphQLErrors.forEach((error): void => {
          if (error.message === "Exception - Root image not found") {
            msgError(t("validations.dockerImages.imageAlreadyRemoved"));
          } else {
            msgError(t("groupAlerts.errorTextsad"));
            Logger.error("Couldn't remove Docker image", error);
          }
        });
      },
      refetchQueries: [
        {
          query: GET_GIT_ROOT_DOCKER_IMAGES,
          variables: { groupName, rootId: initialValues.id },
        },
        {
          query: GET_GROUP_DOCKER_IMAGES,
          variables: { groupName },
        },
      ],
    },
  );

  // Handle actions
  const [isEditing, setIsEditing] = useState(false);
  const [selectedDockerImage, setSelectedDockerImage] =
    useState<IDockerImage>();

  const useCustomModal = (name: string): IUseModal => {
    const [isOpen, setIsOpen] = useState(false);

    const toggle = useCallback((): void => {
      setIsOpen((prev): boolean => !prev);
    }, []);
    const open = useCallback((): void => {
      setIsOpen(true);
    }, []);
    const close = useCallback((): void => {
      setIsEditing(false);
      setIsOpen(false);
    }, []);

    return { close, isOpen, name, open, setIsOpen, toggle };
  };
  const addDockerImageModal = useCustomModal("add-docker-image");
  const { open } = addDockerImageModal;

  // Data management
  const handleRemoveClick = useCallback(
    (uri: string): void => {
      void removeDockerImage({
        variables: { groupName, rootId: initialValues.id, uri },
      });
    },
    [groupName, initialValues.id, removeDockerImage],
  );

  const handleEditClick = useCallback(
    (dockerImage: IDockerImage): (() => void) => {
      return (): void => {
        setIsEditing(true);
        setSelectedDockerImage(dockerImage);
        open();
      };
    },
    [open],
  );

  const onConfirmDelete = useCallback(
    (gitRootDockerImage: IDockerImage): (() => void) => {
      return (): void => {
        confirm({
          title: t("group.scope.git.dockerImages.remove.title"),
        })
          .then((result: boolean): void => {
            if (result) {
              handleRemoveClick(gitRootDockerImage.uri);
            }
          })
          .catch((): void => {
            Logger.error("An error occurred removing docker image");
          });
      };
    },
    [confirm, handleRemoveClick, t],
  );

  const gitRootDockerImages = useMemo((): IDockerImageItem[] => {
    if (isNil(data)) {
      return [];
    }

    return (data.root as { dockerImages: IDockerImage[] }).dockerImages.map(
      (gitRootDockerImage: IDockerImage): IDockerImageItem => {
        return {
          ...gitRootDockerImage,
          element: (
            <Container display={"flex"} flexDirection={"row"} gap={0.5}>
              <IconButton
                disabled={loading || isRemoving}
                icon={"pencil"}
                iconSize={"xxs"}
                iconType={"fa-light"}
                id={"git-root-edit-docker-image"}
                onClick={handleEditClick(gitRootDockerImage)}
                variant={"ghost"}
              />
              <IconButton
                disabled={loading || isRemoving}
                icon={"trash"}
                iconSize={"xxs"}
                iconType={"fa-light"}
                id={"git-root-remove-docker-image"}
                onClick={onConfirmDelete(gitRootDockerImage)}
                variant={"ghost"}
              />
            </Container>
          ),
        };
      },
    );
  }, [data, isRemoving, loading, onConfirmDelete, handleEditClick]);

  return (
    <Fragment>
      <Table
        columns={[
          {
            accessorKey: "uri",
            cell: (cell): string => {
              return String(cell.getValue());
            },
            header: "URI",
          },
          {
            accessorKey: "element",
            cell: (cell): JSX.Element => cell.getValue() as JSX.Element,
            header: "",
          },
        ]}
        data={gitRootDockerImages}
        tableRef={tableRef}
      />
      <ConfirmDialog />
      <AddDockerImage
        groupName={groupName}
        initialValues={
          isEditing && canEditDockerImage ? selectedDockerImage : undefined
        }
        isEditing={isEditing}
        modalRef={addDockerImageModal}
        rootId={initialValues.id}
      />
      <ModalConfirm
        disabled={!canAddDockerImage}
        onCancel={onClose}
        onConfirm={open}
        txtConfirm={t("group.scope.git.dockerImages.add.button")}
      />
    </Fragment>
  );
};

export { DockerImages };
