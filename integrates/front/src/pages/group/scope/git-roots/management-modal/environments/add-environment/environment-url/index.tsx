import { useQuery } from "@apollo/client";
import { Col, ComboBox, Input, TextArea } from "@fluidattacks/design";
import type { IItem } from "@fluidattacks/design";
import _ from "lodash";
import { Fragment, useMemo } from "react";
import { useTranslation } from "react-i18next";

import type { IEnvironmentUrlProps, IFile } from "./types";

import { GET_FILES } from "../../../../../queries";
import { MOBILE_EXTENSIONS } from "../utils";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";

const EnvironmentUrl = ({
  groupName,
  control,
  formState: { errors, touchedFields },
  getValues,
  register,
}: IEnvironmentUrlProps): JSX.Element => {
  const { t } = useTranslation();
  const [cloudName, urlType] = getValues(["cloudName", "urlType"]);

  const { data } = useQuery(GET_FILES, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.warning("An error occurred loading group files", error);
      });
    },
    variables: { groupName },
  });

  const filesDataset = useMemo(
    (): IFile[] =>
      _.isUndefined(data) || _.isEmpty(data) || _.isNull(data.resources.files)
        ? []
        : (data.resources.files as IFile[]).filter((resource): boolean =>
            MOBILE_EXTENSIONS.some((elm): boolean =>
              resource.fileName.endsWith(elm),
            ),
          ),
    [data],
  );
  const selectOptions = filesDataset.map(
    (file): IItem => ({ name: file.fileName, value: file.fileName }),
  );

  const urlInput: JSX.Element =
    urlType === "APK" ? (
      <ComboBox
        control={control}
        items={selectOptions}
        label={t("group.scope.git.addEnvironment.apk")}
        name={"url"}
        required={true}
      />
    ) : (
      <Input
        error={errors.url?.message}
        isTouched={"url" in touchedFields}
        isValid={!("url" in errors)}
        label={t("group.scope.git.addEnvironment.urlLabel")}
        name={"url"}
        register={register}
        required={true}
        tooltip={t("group.scope.git.addEnvironment.urlTooltip")}
      />
    );

  if (urlType === "CSPM" && cloudName !== undefined) {
    switch (cloudName) {
      case "AWS":
        return (
          <Col>
            <Input
              error={errors.url?.message}
              isTouched={"url" in touchedFields}
              isValid={!("url" in errors)}
              label={t("group.scope.git.addEnvironment.awsRoleArn")}
              name={"url"}
              register={register}
              required={true}
            />
          </Col>
        );
      case "GCP":
        return (
          <Fragment>
            <Input
              error={errors.url?.message}
              isTouched={"url" in touchedFields}
              isValid={!("url" in errors)}
              label={t("group.scope.git.addEnvironment.gcpProjectId")}
              name={"url"}
              register={register}
              required={true}
            />
            <TextArea
              error={errors.gcpPrivateKey?.message}
              isTouched={"gcpPrivateKey" in touchedFields}
              isValid={!("gcpPrivateKey" in errors)}
              label={t("group.scope.git.addEnvironment.gcpPrivateKey")}
              maxLength={20000}
              name={"gcpPrivateKey"}
              register={register}
              required={true}
            />
          </Fragment>
        );
      case "Azure":
      default:
        return (
          <Fragment>
            <Input
              error={errors.url?.message}
              isTouched={"url" in touchedFields}
              isValid={!("url" in errors)}
              label={t("group.scope.git.addEnvironment.azureSubscriptionId")}
              name={"url"}
              register={register}
              required={true}
            />
            <Input
              error={errors.azureClientId?.message}
              isTouched={"azureClientId" in touchedFields}
              isValid={!("azureClientId" in errors)}
              label={t("group.scope.git.addEnvironment.azureClientId")}
              name={"azureClientId"}
              register={register}
              required={true}
            />
            <Input
              error={errors.azureClientSecret?.message}
              isTouched={"azureClientSecret" in touchedFields}
              isValid={!("azureClientSecret" in errors)}
              label={t("group.scope.git.addEnvironment.azureClientSecret")}
              name={"azureClientSecret"}
              register={register}
              required={true}
            />
            <Input
              error={errors.azureTenantId?.message}
              isTouched={"azureTenantId" in touchedFields}
              isValid={!("azureTenantId" in errors)}
              label={t("group.scope.git.addEnvironment.azureTenantId")}
              name={"azureTenantId"}
              register={register}
              required={true}
            />
          </Fragment>
        );
    }
  }

  return <Col>{urlInput}</Col>;
};

export { EnvironmentUrl };
