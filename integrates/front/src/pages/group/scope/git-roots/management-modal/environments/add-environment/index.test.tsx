import { PureAbility } from "@casl/ability";
import { useModal } from "@fluidattacks/design";
import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import { HttpResponse, type StrictResponse, graphql } from "msw";

import { AddEnvironment } from ".";
import { authzPermissionsContext } from "context/authz/config";
import type {
  AddGitEnvironmentCspmMutation as AddEnvironmentCSPM,
  AddGitEnvironmentUrlMutation as AddEnvironmentURL,
  VerifyEnvironmentIntegrityQuery as EnvironmentIntegrity,
  GetFilesQueryQuery as GetFiles,
  GetRootEnvironmentUrlsQuery as GetRootEnvironmentUrls,
} from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";
import {
  ADD_ENVIRONMENT_CSPM,
  ADD_ENVIRONMENT_URL,
  GET_FILES,
  GET_ROOT_ENVIRONMENT_URLS,
  VERIFY_ENVIRONMENT_INTEGRITY,
} from "pages/group/scope/queries";
import { msgError, msgSuccess } from "utils/notifications";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

const handleClose: jest.Mock = jest.fn();
const Wrapper = (): JSX.Element => {
  const modalProps = useModal("add-environment-test");

  return (
    <authzPermissionsContext.Provider
      value={
        new PureAbility([
          { action: "integrates_api_mutations_add_secret_mutate" },
          {
            action:
              "integrates_api_resolvers_query_verify_environment_integrity_resolve",
          },
        ])
      }
    >
      <AddEnvironment
        closeFunction={handleClose}
        groupName={"unittesting"}
        modalRef={{ ...modalProps, close: handleClose, isOpen: true }}
        rootAwsExternalId={"44b02eb8-376f-4644-adf1-93d3aee726d7"}
        rootId={"4039d098-ffc5-4984-8ed3-eb17bca98e19"}
      />
    </authzPermissionsContext.Provider>
  );
};

const graphqlMocked = graphql.link(LINK);
const mockedFiles = graphqlMocked.query(
  GET_FILES,
  (): StrictResponse<{ data: GetFiles }> => {
    return HttpResponse.json({
      data: {
        resources: {
          files: [
            {
              description: "Test",
              fileName: "test.zip",
              uploadDate: "2019-03-01 15:21",
              uploader: "unittest@fluidattacks.com",
            },
            {
              description: "shell",
              fileName: "shell.exe",
              uploadDate: "2019-04-24 14:56",
              uploader: "unittest@fluidattacks.com",
            },
            {
              description: "Test description",
              fileName: "image.png",
              uploadDate: "2019-03-01 15:21",
              uploader: "unittest@fluidattacks.com",
            },
          ],
        },
      },
    });
  },
);

const mockedVerifyEnvironmentIntegrity = graphqlMocked.query(
  VERIFY_ENVIRONMENT_INTEGRITY,
  ({ variables }): StrictResponse<{ data: EnvironmentIntegrity }> => {
    const { url } = variables;

    if (url === "https://gitlab.com/fluidattacks/universe")
      return HttpResponse.json({ data: { verifyEnvironmentIntegrity: false } });

    return HttpResponse.json({ data: { verifyEnvironmentIntegrity: true } });
  },
);

const mockedAddEnvironmentUrl = graphqlMocked.mutation(
  ADD_ENVIRONMENT_URL,
  ({
    variables,
  }): StrictResponse<IErrorMessage | { data: AddEnvironmentURL }> => {
    const { groupName, rootId, url, useZtna } = variables;
    if (
      groupName === "unittesting" &&
      rootId === "4039d098-ffc5-4984-8ed3-eb17bca98e19" &&
      url === "https://gitlab.com/fluidattacks/universe"
    ) {
      return HttpResponse.json({
        data: {
          addGitEnvironmentUrl: {
            __typename: "AddEnvironmentUrlPayload",
            success: true,
            urlId: "c2926b0542bc6051faf9ecf3f2751d1962c29be0",
          },
        },
      });
    }
    if (
      groupName === "unittesting" &&
      rootId === "4039d098-ffc5-4984-8ed3-eb17bca98e19" &&
      url === "https://test.com/"
    ) {
      return HttpResponse.json({
        data: {
          addGitEnvironmentUrl: {
            __typename: "AddEnvironmentUrlPayload",
            success: false,
            urlId: "c2926b0542bc6051faf9ecf3f2751d1962c29be0",
          },
        },
      });
    }
    if (
      groupName === "unittesting" &&
      rootId === "4039d098-ffc5-4984-8ed3-eb17bca98e19" &&
      url === "https://testingztna.com/" &&
      useZtna === true
    ) {
      return HttpResponse.json({
        data: {
          addGitEnvironmentUrl: {
            __typename: "AddEnvironmentUrlPayload",
            success: true,
            urlId: "c2926b0542bc6051faf9ecf3f2751d1962c29be0",
          },
        },
      });
    }
    if (
      groupName === "unittesting" &&
      rootId === "4039d098-ffc5-4984-8ed3-eb17bca98e19" &&
      url === "test.zip"
    ) {
      return HttpResponse.json({
        data: {
          addGitEnvironmentUrl: {
            __typename: "AddEnvironmentUrlPayload",
            success: true,
            urlId: "c2926b0542bc6051faf9ecf3f2751d1962c29be0",
          },
        },
      });
    }

    if (
      groupName === "unittesting" &&
      rootId === "4039d098-ffc5-4984-8ed3-eb17bca98e19" &&
      url === "22:b02eb8-376f-4644-adf1-93d3aee726d"
    ) {
      return HttpResponse.json({
        data: {
          addGitEnvironmentUrl: {
            __typename: "AddEnvironmentUrlPayload",
            success: true,
            urlId: "c2926b0542bc6051faf9ecf3f2751d1962c29be0",
          },
        },
      });
    }

    if (
      groupName === "unittesting" &&
      rootId === "4039d098-ffc5-4984-8ed3-eb17bca98e19" &&
      url === "testId"
    ) {
      return HttpResponse.json({
        data: {
          addGitEnvironmentUrl: {
            __typename: "AddEnvironmentUrlPayload",
            success: true,
            urlId: "c2926b0542bc6051faf9ecf3f2751d1962c29be0",
          },
        },
      });
    }

    if (
      groupName === "unittesting" &&
      rootId === "4039d098-ffc5-4984-8ed3-eb17bca98e19" &&
      url === "https://test3.com/"
    ) {
      return HttpResponse.json({
        errors: [
          new GraphQLError(
            "Exception - Root environment with the same url already exists",
          ),
        ],
      });
    }

    return HttpResponse.json({
      errors: [new GraphQLError("Exception - The type of the root is invalid")],
    });
  },
);

const mockedAddEnvironmentCspm = graphqlMocked.mutation(
  ADD_ENVIRONMENT_CSPM,
  ({
    variables,
  }): StrictResponse<IErrorMessage | { data: AddEnvironmentCSPM }> => {
    const { groupName, rootId, cloudName } = variables;
    if (
      groupName === "unittesting" &&
      rootId === "4039d098-ffc5-4984-8ed3-eb17bca98e19" &&
      cloudName === "AWS"
    ) {
      return HttpResponse.json({
        errors: [
          new GraphQLError("Exception - Cannot assume cross account IAM role"),
        ],
      });
    }

    return HttpResponse.json({
      data: {
        addGitEnvironmentCspm: {
          __typename: "AddEnvironmentUrlPayload",
          success: true,
          urlId: "c2926b0542bc6051faf9ecf3f2751d1962c29be0",
        },
      },
    });
  },
);

const mockedEnvironments = graphqlMocked.query(
  GET_ROOT_ENVIRONMENT_URLS,
  ({
    variables,
  }): StrictResponse<IErrorMessage | { data: GetRootEnvironmentUrls }> => {
    const { groupName, rootId } = variables;
    if (
      groupName === "unittesting" &&
      rootId === "4039d098-ffc5-4984-8ed3-eb17bca98e19"
    ) {
      return HttpResponse.json({
        data: {
          root: {
            __typename: "GitRoot",
            gitEnvironmentUrls: [
              {
                id: "c2926b0542bc6051faf9ecf3f2751d1962c29be0",
                include: true,
                rootId: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
                url: "https://test.com",
                urlType: "URL",
                useEgress: false,
                useVpn: false,
                useZtna: false,
              },
            ],
            id: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
          },
        },
      });
    }

    return HttpResponse.json({
      errors: [new GraphQLError("Error getting root environment urls")],
    });
  },
);

const expectProductionOptions = (shouldExist: boolean): void => {
  const texts = [
    "group.scope.git.addEnvironment.noIsProduction",
    "group.scope.git.addEnvironment.yesIsProduction",
    "group.scope.git.addEnvironment.isProduction",
  ];

  texts.forEach((text): void => {
    const expectation = screen.queryByText(text);
    if (shouldExist) {
      expect(expectation).toBeInTheDocument();
    } else {
      expect(expectation).not.toBeInTheDocument();
    }
  });
};

describe("addEnvironment", (): void => {
  it("should render add environment modal", async (): Promise<void> => {
    expect.hasAssertions();

    render(<Wrapper />, { mocks: [mockedFiles] });

    await waitFor((): void => {
      expect(
        screen.queryByText("group.scope.git.addEnvironment.type"),
      ).toBeInTheDocument();
    });

    expect(
      screen.queryByText("group.scope.git.addEnvironment.urlLabel"),
    ).toBeInTheDocument();
    expect(
      screen.getByRole("combobox", { name: "urlType" }),
    ).toBeInTheDocument();
    expect(screen.getAllByRole("checkbox")).toHaveLength(2);
    expect(screen.getByRole("button", { name: "Confirm" })).toBeDisabled();

    await userEvent.click(
      screen.getByText("group.scope.git.addEnvironment.type"),
    );
    await userEvent.click(screen.getByText("CSPM"));
    expectProductionOptions(false);

    expect(
      screen.queryByText("group.scope.git.addEnvironment.cloud"),
    ).toBeInTheDocument();

    await userEvent.click(
      screen.getByText("group.scope.git.addEnvironment.type"),
    );
    await userEvent.click(screen.getByText("Mobile App"));
    expectProductionOptions(true);

    expect(
      screen.queryByText("group.scope.git.addEnvironment.apk"),
    ).toBeInTheDocument();

    await userEvent.click(
      screen.getByText("group.scope.git.addEnvironment.type"),
    );
    await userEvent.click(screen.getByText("URL"));
    expectProductionOptions(true);

    jest.clearAllMocks();
  });

  it("should render error in add environment modal", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedFilesError = graphqlMocked.query(
      GET_FILES,
      (): StrictResponse<IErrorMessage> => {
        return HttpResponse.json({
          errors: [new GraphQLError("An error occurred loading group files")],
        });
      },
    );
    render(<Wrapper />, { mocks: [mockedFilesError] });
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith("groupAlerts.errorTextsad");
    });

    jest.clearAllMocks();
  });

  it("should add an url environment", async (): Promise<void> => {
    expect.hasAssertions();

    render(<Wrapper />, {
      mocks: [
        mockedEnvironments,
        mockedFiles,
        mockedVerifyEnvironmentIntegrity,
        mockedAddEnvironmentUrl,
      ],
    });

    await userEvent.click(
      screen.getByText("group.scope.git.addEnvironment.type"),
    );
    await userEvent.click(screen.getByText("URL"));

    await userEvent.clear(screen.getByRole("textbox", { name: "url" }));
    await userEvent.type(
      screen.getByRole("textbox", { name: "url" }),
      "https://gitlab.com/fluidattacks/universe",
    );

    await userEvent.click(
      screen.getByText("group.scope.git.addEnvironment.yesIsProduction"),
    );

    await waitFor((): void => {
      expect(screen.getByRole("button", { name: "Confirm" })).toBeEnabled();
    });
    await userEvent.click(screen.getByRole("button", { name: "Confirm" }));
    await waitFor((): void => {
      expect(
        screen.queryByText("groupAlerts.noSuccessStatus"),
      ).toBeInTheDocument();
    });
    await userEvent.click(screen.getAllByRole("button", { name: "Cancel" })[1]);
    await waitFor((): void => {
      expect(screen.getByRole("button", { name: "Confirm" })).toBeEnabled();
    });
    await userEvent.click(screen.getByRole("button", { name: "Confirm" }));
    await waitFor((): void => {
      expect(
        screen.queryByText("groupAlerts.noSuccessStatus"),
      ).toBeInTheDocument();
    });
    await userEvent.click(
      screen.getAllByRole("button", { name: "Confirm" })[1],
    );
    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "group.scope.git.addEnvironment.success",
        "group.scope.git.addEnvironment.successTittle",
      );
    });
  });

  it("should add an url environment with egress", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    render(<Wrapper />, {
      mocks: [
        mockedEnvironments,
        mockedFiles,
        mockedVerifyEnvironmentIntegrity,
        mockedAddEnvironmentUrl,
      ],
    });

    await userEvent.click(
      screen.getByText("group.scope.git.addEnvironment.type"),
    );
    await userEvent.click(screen.getByText("URL"));

    await userEvent.clear(screen.getByRole("textbox", { name: "url" }));
    await userEvent.type(
      screen.getByRole("textbox", { name: "url" }),
      "https://gitlab.com/fluidattacks/universe",
    );
    await userEvent.click(screen.getByRole("checkbox", { name: "useEgress" }));

    expect(screen.getByRole("checkbox", { name: "useZtna" })).toBeDisabled();

    await userEvent.click(
      screen.getByText("group.scope.git.addEnvironment.yesIsProduction"),
    );

    await waitFor((): void => {
      expect(screen.getByRole("button", { name: "Confirm" })).toBeEnabled();
    });
    await userEvent.click(screen.getByRole("button", { name: "Confirm" }));
    await waitFor((): void => {
      expect(
        screen.queryByText("groupAlerts.noSuccessStatus"),
      ).toBeInTheDocument();
    });
    await userEvent.click(screen.getAllByRole("button", { name: "Cancel" })[1]);
    await waitFor((): void => {
      expect(screen.getByRole("button", { name: "Confirm" })).toBeEnabled();
    });
    await userEvent.click(screen.getByRole("button", { name: "Confirm" }));
    await waitFor((): void => {
      expect(
        screen.queryByText("groupAlerts.noSuccessStatus"),
      ).toBeInTheDocument();
    });
    await userEvent.click(
      screen.getAllByRole("button", { name: "Confirm" })[1],
    );
    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "group.scope.git.addEnvironment.success",
        "group.scope.git.addEnvironment.successTittle",
      );
    });
  });

  it("should add an url environment with ztna", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    render(<Wrapper />, {
      mocks: [
        mockedEnvironments,
        mockedFiles,
        mockedVerifyEnvironmentIntegrity,
        mockedAddEnvironmentUrl,
      ],
    });

    await userEvent.click(
      screen.getByText("group.scope.git.addEnvironment.type"),
    );
    await userEvent.click(screen.getByText("URL"));

    await userEvent.clear(screen.getByRole("textbox", { name: "url" }));
    await userEvent.type(
      screen.getByRole("textbox", { name: "url" }),
      "https://testingztna.com/",
    );
    await userEvent.click(screen.getByRole("checkbox", { name: "useZtna" }));

    expect(screen.getByRole("checkbox", { name: "useEgress" })).toBeDisabled();

    await userEvent.click(
      screen.getByText("group.scope.git.addEnvironment.yesIsProduction"),
    );

    await waitFor((): void => {
      expect(screen.getByRole("button", { name: "Confirm" })).toBeEnabled();
    });
    await userEvent.click(screen.getByRole("button", { name: "Confirm" }));

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "group.scope.git.addEnvironment.success",
        "group.scope.git.addEnvironment.successTittle",
      );
    });
  });

  it("should add an existent url environment", async (): Promise<void> => {
    expect.hasAssertions();

    render(<Wrapper />, {
      mocks: [
        mockedEnvironments,
        mockedFiles,
        mockedVerifyEnvironmentIntegrity,
        mockedAddEnvironmentUrl,
      ],
    });

    await userEvent.click(
      screen.getByText("group.scope.git.addEnvironment.type"),
    );
    await userEvent.click(screen.getByText("URL"));
    await userEvent.clear(screen.getByRole("textbox", { name: "url" }));
    await userEvent.type(
      screen.getByRole("textbox", { name: "url" }),
      "https://test.com",
    );

    await userEvent.click(
      screen.getByText("group.scope.git.addEnvironment.yesIsProduction"),
    );

    await waitFor((): void => {
      expect(screen.getByRole("button", { name: "Confirm" })).toBeEnabled();
    });
    await userEvent.click(screen.getByRole("button", { name: "Confirm" }));
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith("validations.environmentUrlExists");
    });
  });

  it("should show an error in add an existent url environment", async (): Promise<void> => {
    expect.hasAssertions();

    render(<Wrapper />, {
      mocks: [
        mockedEnvironments,
        mockedFiles,
        mockedVerifyEnvironmentIntegrity,
        mockedAddEnvironmentUrl,
      ],
    });

    await userEvent.click(
      screen.getByText("group.scope.git.addEnvironment.type"),
    );
    await userEvent.click(screen.getByText("URL"));
    await userEvent.clear(screen.getByRole("textbox", { name: "url" }));
    await userEvent.type(
      screen.getByRole("textbox", { name: "url" }),
      "https://test2.com",
    );

    await userEvent.click(
      screen.getByText("group.scope.git.addEnvironment.yesIsProduction"),
    );

    await waitFor((): void => {
      expect(screen.getByRole("button", { name: "Confirm" })).toBeEnabled();
    });
    await userEvent.click(screen.getByRole("button", { name: "Confirm" }));
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith("groupAlerts.errorTextsad");
    });
  });

  it("should show an error in add a repeated url environment", async (): Promise<void> => {
    expect.hasAssertions();

    render(<Wrapper />, {
      mocks: [
        mockedEnvironments,
        mockedFiles,
        mockedVerifyEnvironmentIntegrity,
        mockedAddEnvironmentUrl,
      ],
    });

    await userEvent.click(
      screen.getByText("group.scope.git.addEnvironment.type"),
    );
    await userEvent.click(screen.getByText("URL"));
    await userEvent.clear(screen.getByRole("textbox", { name: "url" }));
    await userEvent.type(
      screen.getByRole("textbox", { name: "url" }),
      "https://test3.com",
    );

    await userEvent.click(
      screen.getByText("group.scope.git.addEnvironment.yesIsProduction"),
    );

    await waitFor((): void => {
      expect(screen.getByRole("button", { name: "Confirm" })).toBeEnabled();
    });
    await userEvent.click(screen.getByRole("button", { name: "Confirm" }));
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        "group.scope.git.addEnvironment.repeatedEnvironment",
      );
    });
  });

  it("should not add an AWS cspm environment", async (): Promise<void> => {
    expect.hasAssertions();

    jest.spyOn(console, "warn").mockImplementation();
    render(<Wrapper />, {
      mocks: [
        mockedEnvironments,
        mockedFiles,
        mockedVerifyEnvironmentIntegrity,
        mockedAddEnvironmentCspm,
      ],
    });

    await userEvent.click(
      screen.getByText("group.scope.git.addEnvironment.type"),
    );
    await userEvent.click(screen.getByText("CSPM"));
    await userEvent.click(
      screen.getByText("group.scope.git.addEnvironment.cloud"),
    );
    await userEvent.click(screen.getByText("AWS"));

    await waitFor((): void => {
      expect(
        screen.queryByText("group.scope.git.addEnvironment.awsRoleArn"),
      ).toBeInTheDocument();
    });
    await userEvent.clear(screen.getByRole("textbox", { name: "url" }));
    await userEvent.type(
      screen.getByRole("textbox", { name: "url" }),
      "arn:aws:iam::205810638802:role/dev",
    );
    await waitFor((): void => {
      expect(screen.getByRole("button", { name: "Confirm" })).toBeEnabled();
    });
    await userEvent.click(screen.getByRole("button", { name: "Confirm" }));
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith("validations.cannotAssumeAWSRole");
    });
  });

  it("should be added a mobile app environment", async (): Promise<void> => {
    expect.hasAssertions();

    render(<Wrapper />, {
      mocks: [
        mockedEnvironments,
        mockedFiles,
        mockedVerifyEnvironmentIntegrity,
        mockedAddEnvironmentUrl,
      ],
    });

    await waitFor((): void => {
      expect(screen.queryAllByRole("checkbox")).toHaveLength(2);
    });

    await userEvent.click(
      screen.getByText("group.scope.git.addEnvironment.type"),
    );
    await userEvent.click(screen.getByText("Mobile App"));

    await waitFor((): void => {
      expect(
        screen.queryByText("group.scope.git.addEnvironment.apk"),
      ).toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(screen.queryAllByRole("checkbox")).toHaveLength(0);
    });

    await userEvent.click(
      screen.getByText("group.scope.git.addEnvironment.apk"),
    );
    await userEvent.click(screen.getByText("test.zip"));

    await userEvent.click(
      screen.getByText("group.scope.git.addEnvironment.yesIsProduction"),
    );

    await waitFor((): void => {
      expect(screen.getByRole("button", { name: "Confirm" })).toBeEnabled();
    });
    await userEvent.click(screen.getByRole("button", { name: "Confirm" }));
    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "group.scope.git.addEnvironment.success",
        "group.scope.git.addEnvironment.successTittle",
      );
    });
  });

  it("should be added a cspm environment azure", async (): Promise<void> => {
    expect.hasAssertions();

    jest.spyOn(console, "warn").mockImplementation();
    render(<Wrapper />, {
      mocks: [
        mockedEnvironments,
        mockedFiles,
        mockedVerifyEnvironmentIntegrity,
        mockedAddEnvironmentCspm,
      ],
    });

    await userEvent.click(
      screen.getByText("group.scope.git.addEnvironment.type"),
    );
    await userEvent.click(screen.getByText("CSPM"));
    await userEvent.click(
      screen.getByText("group.scope.git.addEnvironment.cloud"),
    );
    await userEvent.click(screen.getByText("Azure"));

    await userEvent.type(
      screen.getByRole("textbox", { name: "url" }),
      "22:b02eb8-376f-4644-adf1-93d3aee726d",
    );
    await userEvent.type(
      screen.getByRole("textbox", { name: "azureClientId" }),
      "44b02eb8-376f-4644-adf1-93d3aee726d7",
    );
    await userEvent.type(
      screen.getByRole("textbox", { name: "azureClientSecret" }),
      "edb02e:8-376f-4644-adf1-93d3aee726d7u8-d",
    );
    await userEvent.type(
      screen.getByRole("textbox", { name: "azureTenantId" }),
      "33-b2eb8-376f-4644-adf1-93d3aee726d7",
    );

    await waitFor((): void => {
      expect(screen.getByRole("button", { name: "Confirm" })).toBeEnabled();
    });
    await userEvent.click(screen.getByRole("button", { name: "Confirm" }));
    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "group.scope.git.addEnvironment.success",
        "group.scope.git.addEnvironment.successTittle",
      );
    });
  });

  it("should be added a cspm environment gcp", async (): Promise<void> => {
    expect.hasAssertions();

    jest.spyOn(console, "warn").mockImplementation();
    render(<Wrapper />, {
      mocks: [
        mockedEnvironments,
        mockedFiles,
        mockedVerifyEnvironmentIntegrity,
        mockedAddEnvironmentCspm,
      ],
    });

    await userEvent.click(
      screen.getByText("group.scope.git.addEnvironment.type"),
    );
    await userEvent.click(screen.getByText("CSPM"));
    await userEvent.click(
      screen.getByText("group.scope.git.addEnvironment.cloud"),
    );
    await userEvent.click(screen.getByText("Google Cloud Platform"));

    await userEvent.type(
      screen.getByRole("textbox", { name: "url" }),
      "testId",
    );
    await userEvent.type(
      screen.getByRole("textbox", { name: "gcpPrivateKey" }),
      "testprivatekey",
    );

    await waitFor((): void => {
      expect(screen.getByRole("button", { name: "Confirm" })).toBeEnabled();
    });
    await userEvent.click(screen.getByRole("button", { name: "Confirm" }));
    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "group.scope.git.addEnvironment.success",
        "group.scope.git.addEnvironment.successTittle",
      );
    });
  });
});
