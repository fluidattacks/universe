import { useQuery } from "@apollo/client";
import { ComboBox, type TFormMethods, Text } from "@fluidattacks/design";
import isEmpty from "lodash/isEmpty";
import { Fragment, useContext, useMemo } from "react";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

import type { IAddDockerImageModalSubmit } from "../../types";
import type { ICredentialsAttr } from "pages/group/scope/types";
import { organizationDataContext } from "pages/organization/context";
import { GET_ORGANIZATION_CREDENTIALS } from "pages/organization/credentials/queries";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";

const CredentialsSection = ({
  control,
  onChange,
}: {
  control: TFormMethods<IAddDockerImageModalSubmit>["control"];
  onChange: (value: string) => void;
}): JSX.Element => {
  const { t } = useTranslation();
  const theme = useTheme();
  const { organizationId } = useContext(organizationDataContext);

  // Graphql operations
  const {
    data: { organization: { credentials: rawCredentials } } = {
      organization: {
        credentials: [],
      },
    },
  } = useQuery(GET_ORGANIZATION_CREDENTIALS, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.error("Couldn't load organization credentials", error);
      });
    },
    variables: {
      organizationId,
    },
  });

  // Data management
  const formatCredentials = (
    creds: ICredentialsAttr[],
  ): Record<string, ICredentialsAttr> => {
    return Object.fromEntries(
      creds
        .filter(
          (cred): boolean => cred.type === "HTTPS" || cred.type === "AWSROLE",
        )
        .map((cred): [string, ICredentialsAttr] => [cred.id, cred]),
    );
  };

  const credentials = useMemo(
    (): Record<string, ICredentialsAttr> =>
      rawCredentials && rawCredentials.length > 0
        ? formatCredentials(rawCredentials)
        : {},
    [rawCredentials],
  );

  const selectOptions = Object.values(credentials).map(
    (cred): { name: string; value: string } => ({
      name: cred.name,
      value: cred.id,
    }),
  );

  return (
    <Fragment>
      <Text
        color={theme.palette.gray[800]}
        fontWeight={"bold"}
        lineSpacing={3.5}
        size={"md"}
      >
        {t("group.scope.git.dockerImages.add.credentialsTitle")}
      </Text>
      {isEmpty(credentials) ? null : (
        <ComboBox
          control={control}
          items={selectOptions}
          label={t("group.scope.git.dockerImages.add.existingCredential")}
          name={"credentials.id"}
          onChange={onChange}
        />
      )}
    </Fragment>
  );
};

export { CredentialsSection };
