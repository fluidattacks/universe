import { useMutation, useQuery } from "@apollo/client";
import { useAbility } from "@casl/react";
import {
  IconButton,
  ModalConfirm,
  useConfirmDialog,
  useModal,
} from "@fluidattacks/design";
import type { ColumnDef } from "@tanstack/react-table";
import isNil from "lodash/isNil";
import mixpanel from "mixpanel-browser";
import { Fragment, useCallback, useEffect, useMemo, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { AddEnvironment } from "./add-environment";
import { EditModal } from "./edit-modal";
import { MoveEnvironmentModal } from "./move-environment-modal";
import type { IEnvironmentUrlItem, IEnvironmentsProps } from "./types";

import {
  GET_ROOT_ENVIRONMENT_URLS,
  REMOVE_ENVIRONMENT_URL,
  UPDATE_ENVIRONMENT_URL,
} from "../../../queries";
import type { IBasicEnvironmentUrl } from "../../../types";
import { Table } from "components/table";
import { includeFormatter } from "components/table/table-formatters";
import { authzPermissionsContext } from "context/authz/config";
import { useTable } from "hooks";
import { useAudit } from "hooks/use-audit";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

const Environments = ({
  rootInitialValues,
  groupName,
  awsExternalId,
  organizationName,
  onClose,
  onUpdate,
}: IEnvironmentsProps): JSX.Element => {
  const { t } = useTranslation();
  const { confirm, ConfirmDialog } = useConfirmDialog();
  const addEnvironmentModal = useModal("add-environment-modal");
  const { close, open } = addEnvironmentModal;
  const [currentRow, setCurrentRow] = useState<
    IBasicEnvironmentUrl | undefined
  >(undefined);
  const editEnvironmentModal = useModal("edit-environment-modal");
  const moveEnvironmentModal = useModal("move-environment-modal");
  const permissions = useAbility(authzPermissionsContext);

  const canAddEnvironment = permissions.can(
    "integrates_api_mutations_add_git_environment_mutate",
  );
  const canUpdateEnvironmentUrl = permissions.can(
    "integrates_api_mutations_update_git_root_environment_url_mutate",
  );
  const canMoveEnvironment = permissions.can(
    "integrates_api_mutations_move_environment_mutate",
  );
  const canUpdateEnvironmentFile = permissions.can(
    "integrates_api_mutations_update_git_root_environment_file_mutate",
  );

  const tableRef = useTable("tblGitRootSecrets", {
    element: permissions.can(
      "integrates_api_mutations_remove_environment_url_mutate",
    ),
  });

  const { addAuditEvent } = useAudit();
  useEffect((): void => {
    addAuditEvent("Root.Environments", rootInitialValues.id);
  }, [addAuditEvent, rootInitialValues.id]);

  // Graphql operations
  const { data, loading } = useQuery(GET_ROOT_ENVIRONMENT_URLS, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        if (error.message !== "Exception - Access denied or root not found") {
          Logger.error("Couldn't load environment urls", error);
        }
      });
    },
    variables: { groupName, rootId: rootInitialValues.id },
  });
  const [removeEnvironmentUrl, { loading: isRemoving }] = useMutation(
    REMOVE_ENVIRONMENT_URL,
    {
      onCompleted: (): void => {
        mixpanel.track("DeleteEnvironmentUrl");
        msgSuccess(
          t("group.scope.git.removeEnvironment.success"),
          t("group.scope.git.removeEnvironment.successTitle"),
        );
        onUpdate?.();
      },
      onError: ({ graphQLErrors }): void => {
        graphQLErrors.forEach((error): void => {
          if (
            error.message ===
            "Exception - Access denied or root environment url not found"
          ) {
            msgError(t("validations.environmentUrlRemoved"));
          } else {
            msgError(t("groupAlerts.errorTextsad"));
            Logger.error("Couldn't remove environment url", error);
          }
        });
      },
      refetchQueries: [
        {
          query: GET_ROOT_ENVIRONMENT_URLS,
          variables: { groupName, rootId: rootInitialValues.id },
        },
      ],
    },
  );
  const [updateEnvironmentUrl, { loading: isUpdating }] = useMutation(
    UPDATE_ENVIRONMENT_URL,
    {
      onCompleted: (): void => {
        mixpanel.track("UpdateEnvironmentUrl");
        msgSuccess(
          t("group.scope.git.updateEnvironment.success"),
          t("group.scope.git.updateEnvironment.successTitle"),
        );
        onUpdate?.();
      },
      onError: ({ graphQLErrors }): void => {
        graphQLErrors.forEach((error): void => {
          if (
            error.message === "Exception - The environment has been excluded"
          ) {
            msgError(
              t("group.scope.git.updateEnvironment.excludedSupEnvironment"),
            );
          } else {
            msgError(t("groupAlerts.errorTextsad"));
            Logger.error("Couldn't update environment url", error);
          }
        });
      },
      refetchQueries: [
        {
          query: GET_ROOT_ENVIRONMENT_URLS,
          variables: { groupName, rootId: rootInitialValues.id },
        },
      ],
    },
  );

  // Data management
  const handleRemoveClick = useCallback(
    (urlId: string): void => {
      void removeEnvironmentUrl({
        variables: { groupName, rootId: rootInitialValues.id, urlId },
      });
    },
    [groupName, rootInitialValues.id, removeEnvironmentUrl],
  );
  const onConfirmDelete = useCallback(
    (gitEnvironment: IBasicEnvironmentUrl): (() => void) => {
      return (): void => {
        confirm({
          title: t("group.scope.git.removeEnvironment.title"),
        })
          .then((result: boolean): void => {
            if (result) {
              handleRemoveClick(gitEnvironment.id);
            }
          })
          .catch((): void => {
            Logger.error("An error occurred removing environment");
          });
      };
    },
    [confirm, handleRemoveClick, t],
  );
  const onConfirmEdit = useCallback(
    (gitEnvironment: IBasicEnvironmentUrl): VoidFunction => {
      return (): void => {
        setCurrentRow(gitEnvironment);
        editEnvironmentModal.open();
      };
    },
    [editEnvironmentModal],
  );

  const onConfirmMove = useCallback(
    (gitEnvironment: IBasicEnvironmentUrl): VoidFunction => {
      return (): void => {
        setCurrentRow(gitEnvironment);
        moveEnvironmentModal.open();
      };
    },
    [moveEnvironmentModal],
  );

  const handleIncludeStandard = (envUrl: IEnvironmentUrlItem): void => {
    if (!isUpdating) {
      void updateEnvironmentUrl({
        variables: {
          groupName,
          include: !envUrl.include,
          rootId: rootInitialValues.id,
          urlId: envUrl.id,
        },
      });
    }
  };

  const gitEnvironmentUrls = useMemo((): IEnvironmentUrlItem[] => {
    if (isNil(data)) {
      return [];
    }

    const getConnectionType = (env: IBasicEnvironmentUrl): string => {
      if (env.useEgress) return "Egress";
      if (env.useVpn) return "Legacy";
      if (env.useZtna) return "Connector";

      return "N/A";
    };

    return (
      data.root as { gitEnvironmentUrls: IBasicEnvironmentUrl[] }
    ).gitEnvironmentUrls.map(
      (gitEnvironment: IBasicEnvironmentUrl): IEnvironmentUrlItem => {
        const permissionToEdit =
          canUpdateEnvironmentFile &&
          gitEnvironment.urlType === "APK" &&
          gitEnvironment.include &&
          rootInitialValues.state === "ACTIVE";

        return {
          ...gitEnvironment,
          actions: (
            <React.Fragment>
              {canMoveEnvironment ? (
                <IconButton
                  disabled={loading || isRemoving}
                  icon={"inbox-out"}
                  iconSize={"xxs"}
                  iconType={"fa-light"}
                  id={"git-root-move-environment-url"}
                  onClick={onConfirmMove(gitEnvironment)}
                  tooltip={t("group.scope.git.moveEnvironment.actions.move")}
                  variant={"ghost"}
                />
              ) : null}
              {permissionToEdit ? (
                <IconButton
                  disabled={loading}
                  icon={"pen"}
                  iconSize={"xxs"}
                  iconType={"fa-light"}
                  id={"git-root-edit-environment-url"}
                  onClick={onConfirmEdit(gitEnvironment)}
                  variant={"ghost"}
                />
              ) : undefined}
              <IconButton
                disabled={loading || isRemoving}
                icon={"trash"}
                iconSize={"xxs"}
                iconType={"fa-light"}
                id={"git-root-remove-environment-url"}
                onClick={onConfirmDelete(gitEnvironment)}
                tooltip={t("group.scope.git.moveEnvironment.actions.remove")}
                variant={"ghost"}
              />
            </React.Fragment>
          ),
          connectionType: getConnectionType(gitEnvironment),
        };
      },
    );
  }, [
    canUpdateEnvironmentFile,
    canMoveEnvironment,
    data,
    isRemoving,
    loading,
    onConfirmEdit,
    onConfirmDelete,
    onConfirmMove,
    rootInitialValues,
    t,
  ]);

  const columns: ColumnDef<IEnvironmentUrlItem>[] = [
    {
      accessorKey: "url",
      cell: (cell): string => {
        return String(cell.getValue());
      },
      header: t("group.scope.git.envUrl"),
    },
    {
      accessorKey: "urlType",
      header: t("group.scope.git.envUrlType"),
    },
    {
      accessorKey: "include",
      cell: (cell): JSX.Element | string =>
        includeFormatter(
          cell.row.original,
          handleIncludeStandard,
          canUpdateEnvironmentUrl,
          true,
        ),
      header: t("group.scope.git.exclusionStatus"),
      id: "include",
      meta: { filterType: "select" },
    },
    {
      accessorKey: "connectionType",
      header: t("group.scope.git.connectionType"),
    },
    {
      accessorKey: "actions",
      cell: (cell): JSX.Element => cell.getValue() as JSX.Element,
      enableSorting: false,
      header: "Actions",
    },
  ];

  return (
    <Fragment>
      <Table columns={columns} data={gitEnvironmentUrls} tableRef={tableRef} />
      <ConfirmDialog />
      <AddEnvironment
        closeFunction={close}
        groupName={groupName}
        modalRef={addEnvironmentModal}
        onUpdate={onUpdate}
        rootAwsExternalId={awsExternalId}
        rootId={rootInitialValues.id}
      />
      {currentRow === undefined ? undefined : (
        <React.Fragment>
          <EditModal
            gitEnvironment={currentRow}
            groupName={groupName}
            modalRef={editEnvironmentModal}
            organizationName={organizationName}
            rootId={rootInitialValues.id}
          />
          <MoveEnvironmentModal
            environment={currentRow}
            groupName={groupName}
            modalRef={moveEnvironmentModal}
            organizationName={organizationName}
            rootId={rootInitialValues.id}
          />
        </React.Fragment>
      )}
      <ModalConfirm
        disabled={!canAddEnvironment}
        id={"add-env-url"}
        onCancel={onClose}
        onConfirm={open}
        txtConfirm={t("group.scope.git.addEnvUrl")}
      />
    </Fragment>
  );
};

export { Environments };
