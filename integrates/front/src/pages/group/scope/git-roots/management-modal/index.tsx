import { Tabs } from "@fluidattacks/design";
import { useCallback, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { DockerImages } from "./docker-images";
import { Environments } from "./environments";
import { Repository } from "./repository";
import type { IManagementModalProps, TManagementTabs } from "./types";
import { formInitialValues } from "./utils";

import { Modal } from "components/modal";
import { useAuthz } from "hooks/use-authz";
import { useModal } from "hooks/use-modal";
import { Secrets } from "pages/group/scope/secrets";

const ManagementModal = ({
  externalId,
  groupName,
  isEditing,
  initialValues = formInitialValues,
  manyRows = false,
  modalMessages,
  organizationName,
  onClose,
  onUpdate,
  onSubmitRepo,
}: Readonly<IManagementModalProps>): JSX.Element => {
  const { t } = useTranslation();
  const managementModal = useModal("management-modal");

  const { canMutate, canResolve } = useAuthz();
  const canUpdateRootState = canMutate("update_git_root");
  const canResolveDockerIMages = canResolve("git_root_docker_images");
  const canResolveRootSecrets = canResolve("git_root_secrets");
  const [currentTab, setCurrentTab] = useState<TManagementTabs>(
    (canUpdateRootState ? "repository" : "secrets") as TManagementTabs,
  );

  // Data management
  const title = t(
    isEditing ? "group.scope.common.edit" : "group.scope.common.add",
    {
      count: manyRows ? 2 : 1,
    },
  );
  const tabContent: Record<TManagementTabs, JSX.Element> = {
    dockerImages: (
      <DockerImages
        groupName={groupName}
        initialValues={initialValues}
        onClose={onClose}
      />
    ),
    environments: (
      <Environments
        awsExternalId={externalId}
        groupName={groupName}
        onClose={onClose}
        onUpdate={onUpdate}
        organizationName={organizationName}
        rootInitialValues={initialValues}
      />
    ),
    repository: (
      <Repository
        initialValues={initialValues}
        isEditing={isEditing}
        manyRows={manyRows}
        modalMessages={modalMessages}
        onClose={onClose}
        onSubmit={onSubmitRepo}
      />
    ),
    secrets: (
      <Secrets
        groupName={groupName}
        onCloseModal={onClose}
        resourceId={initialValues.id}
      />
    ),
  };
  const toTab = useCallback((event: React.MouseEvent): void => {
    event.preventDefault();
    const target = event.target as HTMLButtonElement;
    setCurrentTab(target.id as TManagementTabs);
  }, []);

  return (
    <Modal
      id={"edit-root-modal"}
      modalRef={{ ...managementModal, close: onClose, isOpen: true }}
      size={"md"}
      title={title}
    >
      {isEditing ? (
        <Tabs
          borders={false}
          box={true}
          items={[
            ...(canUpdateRootState
              ? [
                  {
                    id: "repository",
                    isActive: currentTab === "repository",
                    label: t("group.scope.git.repo.title"),
                    link: "/repository",
                    onClick: toTab,
                    tooltip: t("group.scope.git.repo.title"),
                  },
                ]
              : []),
            {
              id: "environments",
              isActive: currentTab === "environments",
              label: t("group.scope.git.envUrls"),
              link: "/environments",
              onClick: toTab,
              tooltip: t("group.scope.git.manageEnvsTooltip"),
            },
            ...(canResolveDockerIMages
              ? [
                  {
                    id: "dockerImages",
                    isActive: currentTab === "dockerImages",
                    label: t("group.scope.git.dockerImages.commonLabel"),
                    link: "/docker-images",
                    onClick: toTab,
                    tooltip: t("group.scope.git.dockerImages.commonToolTip"),
                  },
                ]
              : []),
            ...(canResolveRootSecrets
              ? [
                  {
                    id: "secrets",
                    isActive: currentTab === "secrets",
                    label: t("group.scope.git.repo.secrets"),
                    link: "/secrets",
                    onClick: toTab,
                    tooltip: t("group.scope.git.repo.title"),
                  },
                ]
              : []),
          ]}
        />
      ) : undefined}
      {tabContent[currentTab]}
    </Modal>
  );
};

export { ManagementModal };
