import type { IBasicEnvironmentUrl, IFormValues } from "../../../types";

interface IEnvironmentsProps {
  rootInitialValues: IFormValues;
  groupName: string;
  organizationName: string;
  awsExternalId: string;
  onClose: () => void;
  onUpdate?: () => void;
}

interface IEnvironmentUrlItem extends IBasicEnvironmentUrl {
  connectionType: string;
  actions: React.ReactNode;
}

export type { IEnvironmentsProps, IEnvironmentUrlItem };
