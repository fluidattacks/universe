import { Alert, Container, IconButton, Row, Text } from "@fluidattacks/design";
import { useFormikContext } from "formik";
import isEqual from "lodash/isEqual";
import { useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import type { IFormValues } from "../../../../types";
import { InputArray, InputContainer } from "components/input";
import { RadioButton } from "components/radio-button";
import { Can } from "context/authz/can";
import { openUrl } from "utils/resource-helpers";

interface IExclusionsProps {
  readonly isEditing: boolean;
  readonly manyRows: boolean;
}

const Exclusions = ({
  isEditing,
  manyRows,
}: IExclusionsProps): JSX.Element | null => {
  const { t } = useTranslation();
  const { initialValues, setFieldValue, values } =
    useFormikContext<IFormValues>();

  const goToDocumentation = useCallback((): void => {
    openUrl(
      "https://mirrors.edge.kernel.org/pub/software/scm/git/docs/gitignore.html#_pattern_format",
    );
  }, []);

  const handleHasExclusions = useCallback(
    (event: React.MouseEvent<HTMLInputElement>): void => {
      const hasExclusions = event.currentTarget.value;
      if (isEditing && isEqual(hasExclusions, initialValues.hasExclusions)) {
        void setFieldValue("gitignore", initialValues.gitignore);
      } else {
        void setFieldValue("gitignore", []);
      }
    },
    [initialValues, isEditing, setFieldValue],
  );

  if (manyRows) {
    return null;
  }

  return (
    <Can do={"update_git_root_filter"}>
      <Row>
        <InputContainer
          label={t("group.scope.git.filter.label")}
          name={"exclusions"}
          required={true}
          tooltip={t("group.scope.git.filter.tooltip")}
          weight={"bold"}
        >
          <Container display={"flex"} flexDirection={"row"} gap={1}>
            <RadioButton
              label={"Yes"}
              name={"hasExclusions"}
              onClick={handleHasExclusions}
              value={"yes"}
              variant={"formikField"}
            />
            <RadioButton
              label={"No"}
              name={"hasExclusions"}
              onClick={handleHasExclusions}
              value={"no"}
              variant={"formikField"}
            />
          </Container>
        </InputContainer>
        {values.hasExclusions === "yes" ? (
          <div>
            <Container my={0.75}>
              <Alert>{t("group.scope.git.filter.warning")}</Alert>
            </Container>
            <Container display={"inline-flex"} mb={1}>
              <IconButton
                icon={"question-circle"}
                iconSize={"xxs"}
                onClick={goToDocumentation}
                px={0.125}
                py={0.125}
                variant={"tertiary"}
              />
              <Text ml={0.5} size={"sm"}>
                {t("group.scope.git.filter.exclude")}
              </Text>
            </Container>
            <InputArray
              addButtonText={t("group.scope.git.filter.addExclusion")}
              initEmpty={false}
              name={"gitignore"}
              placeholder={t("group.scope.git.filter.placeholder")}
            />
          </div>
        ) : undefined}
      </Row>
    </Can>
  );
};

export { Exclusions };
