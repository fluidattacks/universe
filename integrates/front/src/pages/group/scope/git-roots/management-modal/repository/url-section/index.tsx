import { Alert, GridContainer } from "@fluidattacks/design";
import { useFormikContext } from "formik";
import type { ChangeEvent } from "react";
import { useCallback } from "react";
import { useTranslation } from "react-i18next";

import { StyledExpandedCell } from "./styles";
import type { IFieldRootValues, IUrlProps } from "./types";

import {
  chooseCredentialType,
  removeSSHFromUrl,
  updateHealthCheckConfirm,
} from "../utils";
import { Checkbox } from "components/checkbox";
import { Input } from "components/input";
import type { IFormValues } from "pages/group/scope/types";

const UrlSection = ({
  credExists,
  form,
  isEditing,
  manyRows,
  setCredExists,
  setIsSameHealthCheck,
}: IUrlProps): JSX.Element => {
  const { t } = useTranslation();
  const { initialValues, setFieldValue, values } =
    useFormikContext<IFormValues>();

  const onBlurUrl = useCallback(
    (event: ChangeEvent<HTMLInputElement>): void => {
      const urlValue = event.target.value;

      updateHealthCheckConfirm(
        form,
        initialValues,
        isEditing,
        setIsSameHealthCheck,
        {
          url: urlValue,
        } as IFieldRootValues,
      );

      chooseCredentialType(urlValue, credExists, setCredExists, setFieldValue);
      void setFieldValue("url", removeSSHFromUrl(urlValue));
    },
    [
      form,
      initialValues,
      isEditing,
      setIsSameHealthCheck,
      credExists,
      setCredExists,
      setFieldValue,
    ],
  );
  const onChangeBranch = useCallback(
    (event: ChangeEvent<HTMLInputElement>): void => {
      const branchValue = event.target.value;
      updateHealthCheckConfirm(
        form,
        initialValues,
        isEditing,
        setIsSameHealthCheck,
        {
          branch: branchValue,
        } as IFieldRootValues,
      );
    },
    [form, initialValues, isEditing, setIsSameHealthCheck],
  );
  const onChangeNickname = useCallback(
    (event: ChangeEvent<HTMLInputElement>): void => {
      const nicknameValue = event.target.value;

      updateHealthCheckConfirm(
        form,
        initialValues,
        isEditing,
        setIsSameHealthCheck,
        {
          nickname: nicknameValue,
        } as IFieldRootValues,
      );
    },
    [form, initialValues, isEditing, setIsSameHealthCheck],
  );

  return (
    <GridContainer lg={2} md={2} sm={1} xl={2}>
      {isEditing ? (
        <StyledExpandedCell>
          <Input
            label={t("group.scope.git.repo.nickname")}
            name={"nickname"}
            onChange={onChangeNickname}
            placeholder={t("group.scope.git.repo.nickname")}
            required={true}
            tooltip={t("group.scope.git.repo.nickname")}
          />
        </StyledExpandedCell>
      ) : undefined}
      {isEditing && values.nickname !== initialValues.nickname ? (
        <StyledExpandedCell>
          <Alert variant={"warning"}>
            {t("group.scope.common.changeNicknameWarning")}
          </Alert>
        </StyledExpandedCell>
      ) : undefined}
      {manyRows ? undefined : (
        <StyledExpandedCell>
          <Input
            label={t("group.scope.git.repo.url.text")}
            name={"url"}
            onBlur={onBlurUrl}
            required={true}
            tooltip={t("group.scope.git.repo.url.toolTip")}
          />
        </StyledExpandedCell>
      )}
      <StyledExpandedCell>
        <Input
          label={t("group.scope.git.repo.branch.text")}
          name={"branch"}
          onChange={onChangeBranch}
          required={true}
          tooltip={t("group.scope.git.repo.branch.toolTip")}
        />
      </StyledExpandedCell>
      <div className={"inline-flex"}>
        <div className={"mr3"}>
          <Checkbox
            disabled={values.useVpn || values.useEgress}
            label={t("group.scope.git.repo.useZtna.label")}
            name={"useZtna"}
            tooltip={t("group.scope.git.repo.useZtna.toolTip")}
            variant={"formikField"}
          />
        </div>
        <Checkbox
          disabled={values.useZtna || values.useVpn}
          label={t("group.scope.git.repo.useEgress.label")}
          name={"useEgress"}
          tooltip={t("group.scope.git.repo.useEgress.toolTip")}
          variant={"formikField"}
        />
      </div>
    </GridContainer>
  );
};

export { UrlSection };
