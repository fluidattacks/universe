import { ComboBox, Container, Search, Text } from "@fluidattacks/design";
import type { IItem, IUseModal, TFormMethods } from "@fluidattacks/design";
import { useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

import { useGroupsQuery } from "../../../hooks/use-groups-query";
import { useRootsQuery } from "../../../hooks/use-roots-query";
import { useSearch } from "components/search/hooks";
import type { IBasicEnvironmentUrl } from "pages/group/scope/types";

interface IModalProps {
  environment: IBasicEnvironmentUrl;
  groupName: string;
  organizationName: string;
  rootId: string;
  modalRef: IUseModal;
}

interface IValues {
  groupName: string;
  rootId: string;
  targetGroupName: string;
  targetRootId: string;
}

const FormContent = ({
  control,
  setValue,
  environment,
  organizationName,
  watch,
}: TFormMethods<IValues> & Readonly<IModalProps>): React.ReactNode => {
  const theme = useTheme();
  const targetGroupName = watch("targetGroupName");
  const { t } = useTranslation();
  const [search, setSearch] = useSearch();

  const onGroupChange = useCallback((): void => {
    setValue("targetRootId", "");
  }, [setValue]);

  const { data: groups, loading: groupsLoading } = useGroupsQuery({
    organizationName,
  });

  const { data: roots, loading: rootsLoading } = useRootsQuery({
    groupName: targetGroupName,
    search: search ?? "",
  });

  const allGroups = [
    ...groups.map(
      (group): IItem => ({
        name: group.name.toLowerCase(),
        value: group.name,
      }),
    ),
  ].sort((root1, root2): number => root1.name.localeCompare(root2.name));
  const allRoots = [
    ...roots.map((root): IItem => ({ name: root.nickname, value: root.id })),
  ].sort((root1, root2): number => root1.name.localeCompare(root2.name));

  const onSearchChange = useCallback(
    (event: React.FocusEvent<HTMLInputElement>): void => {
      setSearch(event.target.value);

      if (event.target.value === "") {
        setValue("targetRootId", "");
      }
    },
    [setSearch, setValue],
  );

  return (
    <React.Fragment>
      <Text color={theme.palette.gray[800]} mb={1} size={"sm"}>
        {"You are moving this environment: "}
        <b>{environment.url}</b>
      </Text>
      <Container
        display={"flex"}
        flexDirection={"column"}
        gap={1}
        maxWidth={"320px"}
      >
        <ComboBox
          control={control}
          disabled={groupsLoading}
          items={allGroups}
          label={t("group.scope.git.moveEnvironment.formFields.targetGroup")}
          name={"targetGroupName"}
          onChange={onGroupChange}
        />
        <Container>
          <Text color={theme.palette.gray[800]} size={"sm"}>
            {t("group.scope.git.moveEnvironment.formFields.targetRoot")}
          </Text>
          <Search
            data-testid={"search"}
            name={"search"}
            onBlur={onSearchChange}
          />
        </Container>
        <ComboBox
          control={control}
          disabled={targetGroupName === "" || search === "" || rootsLoading}
          items={allRoots}
          name={"targetRootId"}
        />
      </Container>
    </React.Fragment>
  );
};

export type { IModalProps, IValues };
export { FormContent };
