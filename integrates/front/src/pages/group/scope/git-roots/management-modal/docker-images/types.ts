import type { IUseModal } from "@fluidattacks/design";

import type { IDockerImage, IFormValues } from "../../../types";
import type { ICredentials } from "pages/group/scope/types";

interface IDockerImagesProps {
  initialValues: IFormValues;
  groupName: string;
  onClose: () => void;
}

interface IDockerImageItem extends IDockerImage {
  element: JSX.Element;
}

interface IAddDockerImageProps {
  isEditing: boolean;
  initialValues?: IDockerImage;
  modalRef: IUseModal;
  groupName: string;
  rootId: string;
}

interface IAddDockerImageModalSubmit {
  credentials: ICredentials;
  groupName: string;
  rootId: string;
  uri: string;
  requiresAuthentication: string;
}

export type {
  IDockerImagesProps,
  IDockerImageItem,
  IAddDockerImageProps,
  IAddDockerImageModalSubmit,
};
