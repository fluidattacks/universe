/* eslint-disable @typescript-eslint/no-invalid-void-type */
import type { FormikErrors, FormikProps } from "formik";
import isEqual from "lodash/isEqual";
import type { Dispatch, SetStateAction } from "react";

import type { IFieldRootValues } from "./url-section/types";

import type { IFormValues } from "../../../types";
import { formatBooleanHealthCheck } from "../../../utils";
import { translate } from "utils/translations/translate";

const cleanCredentialsValues = (
  setCredExists: React.Dispatch<React.SetStateAction<boolean>>,
  setFieldValue: (
    field: string,
    value: boolean | string,
    shouldValidate?: boolean,
  ) => Promise<FormikErrors<IFormValues> | void>,
): void => {
  setCredExists(false);
  void setFieldValue("credentials.arn", "", false);
  void setFieldValue("credentials.name", "", false);
  void setFieldValue("credentials.id", "", false);
  void setFieldValue("credentials.password", "", false);
  void setFieldValue("credentials.token", "", false);
  void setFieldValue("credentials.user", "", false);
  void setFieldValue("credentials.key", "", false);
  void setFieldValue("credentials.azureOrganization", "", false);
  void setFieldValue("credentials.type", "", false);
  void setFieldValue("credentials.typeCredential", "", false);
};

const checkCodeCommitFormat = (url: string | undefined): boolean => {
  if (url === undefined) return true;

  const region = /[a-z0-9-]+/u;
  const profile = /[\w-]+/u;
  const repo = /[\w-]+/u;

  const codeCommitRegex = new RegExp(
    `^codecommit::(${region.source})://(${profile.source}@)?${repo.source}$` +
      `|^codecommit://(${profile.source}@)?${repo.source}$`,
    "u",
  );

  return codeCommitRegex.test(url);
};

const chooseCredentialType = (
  url: string,
  isCredExisting: boolean,
  setCredExists: React.Dispatch<React.SetStateAction<boolean>>,
  setFieldValue: (
    field: string,
    value: boolean | string,
    shouldValidate?: boolean,
  ) => Promise<FormikErrors<IFormValues> | void>,
): void => {
  const httpsRegex = /^https?:\/\/(?:(?!\b.*dev\.azure\.com\b)).*/u;
  const azureRegex = /.*(?:dev\.azure\.com).*/u;
  const sshRegex = /^(?:ssh|git@).*/u;
  if (!isCredExisting) {
    if (azureRegex.test(url)) {
      cleanCredentialsValues(setCredExists, setFieldValue);
      void setFieldValue("credentials.typeCredential", "TOKEN", false);
      void setFieldValue("credentials.type", "HTTPS", false);
      void setFieldValue("credentials.auth", "TOKEN", false);
      void setFieldValue("credentials.isPat", true, false);
    }

    if (httpsRegex.test(url)) {
      cleanCredentialsValues(setCredExists, setFieldValue);
      void setFieldValue("credentials.typeCredential", "USER", false);
      void setFieldValue("credentials.type", "HTTPS", false);
      void setFieldValue("credentials.auth", "USER", false);
      void setFieldValue("credentials.isPat", false, false);
    }

    if (sshRegex.test(url)) {
      cleanCredentialsValues(setCredExists, setFieldValue);
      void setFieldValue("credentials.typeCredential", "SSH", false);
      void setFieldValue("credentials.type", "SSH", false);
      void setFieldValue("credentials.auth", "", false);
      void setFieldValue("credentials.isPat", false, false);
    }
    if (checkCodeCommitFormat(url)) {
      cleanCredentialsValues(setCredExists, setFieldValue);
      void setFieldValue("credentials.typeCredential", "AWSROLE", false);
      void setFieldValue("credentials.type", "AWSROLE", false);
      void setFieldValue("credentials.auth", "", false);
      void setFieldValue("credentials.isPat", false, false);
    }
    if (
      !azureRegex.test(url) &&
      !httpsRegex.test(url) &&
      !sshRegex.test(url) &&
      !checkCodeCommitFormat(url)
    ) {
      cleanCredentialsValues(setCredExists, setFieldValue);
    }
  }

  if (
    isCredExisting &&
    ((!azureRegex.test(url) && !httpsRegex.test(url) && !sshRegex.test(url)) ||
      url === "")
  ) {
    cleanCredentialsValues(setCredExists, setFieldValue);
  }
};

const validateArnFormat = (value: string | undefined): boolean => {
  if (value === undefined) {
    return true;
  }
  const regex = /^arn:aws:iam::\d{12}:role\/[\w+=,.@-]+$/u;

  return regex.test(value);
};

const hasSshFormat = (value: string): string | undefined => {
  const regex =
    /^-{5}BEGIN OPENSSH PRIVATE KEY-{5}\n(?:[a-zA-Z0-9+/=]+\n)+-{5}END OPENSSH PRIVATE KEY-{5}\n?$/u;

  if (!regex.test(value)) {
    return translate.t("validations.invalidSshFormat");
  }

  return undefined;
};

const isAwsSubmittable = (values: IFormValues): boolean => {
  if (
    values.credentials.typeCredential === "AWSROLE" &&
    (!values.credentials.name ||
      !values.credentials.arn ||
      validateArnFormat(values.credentials.arn))
  ) {
    return true;
  }

  return false;
};

const isSshSubmittable = (values: IFormValues): boolean => {
  if (
    values.credentials.typeCredential === "SSH" &&
    (!values.credentials.name ||
      !values.credentials.key ||
      hasSshFormat(values.credentials.key) !== undefined)
  ) {
    return true;
  }

  return false;
};

const submittableCredentials = (
  credExists: boolean,
  values: IFormValues,
): boolean => {
  if (values.useVpn || values.useZtna || values.useEgress || credExists) {
    return true;
  }
  if (values.credentials.typeCredential === "") {
    return true;
  }
  if (isSshSubmittable(values)) {
    return true;
  }
  if (isAwsSubmittable(values)) {
    return true;
  }
  if (
    values.credentials.typeCredential === "USER" &&
    (!values.credentials.name ||
      !values.credentials.user ||
      !values.credentials.password)
  ) {
    return true;
  }
  if (
    values.credentials.typeCredential === "TOKEN" &&
    (!values.credentials.name ||
      !values.credentials.token ||
      !values.credentials.azureOrganization)
  ) {
    return true;
  }

  return false;
};

const updateHealthCheckConfirm = (
  form: React.RefObject<FormikProps<IFormValues>>,
  initialValues: IFormValues,
  isEditing: boolean,
  setIsSameHealthCheck: Dispatch<SetStateAction<boolean>>,
  fields: IFieldRootValues,
  includesHealthCheck?: boolean | string,
): void => {
  if (form.current !== null) {
    const values = {
      branch: fields.branch ?? form.current.values.branch,
      includesHealthCheck:
        includesHealthCheck ?? form.current.values.includesHealthCheck,
      nickname: fields.nickname ?? form.current.values.nickname,
      url: fields.url ?? form.current.values.url,
    };

    if (
      isEditing &&
      isEqual(
        [
          values.url,
          values.branch,
          formatBooleanHealthCheck(values.includesHealthCheck),
        ].join(""),
        [
          initialValues.url,
          initialValues.branch,
          initialValues.includesHealthCheck,
        ].join(""),
      )
    ) {
      setIsSameHealthCheck(true);
      void form.current.setFieldValue(
        "healthCheckConfirm",
        initialValues.healthCheckConfirm,
      );
    } else {
      setIsSameHealthCheck(false);
      void form.current.setFieldValue("healthCheckConfirm", []);
    }
  }
};

const checkURLProtocol = (url: string | undefined): boolean => {
  if (url === undefined) return true;

  const protocol = /^https?:\/\//u;

  return protocol.test(url);
};

const checkSSHFormat = (url: string | undefined): boolean => {
  if (url === undefined) return true;

  const user = /[a-zA-Z][a-zA-Z0-9]*/u;
  const host = /.+/u;
  const port = /:\d{2,5}|:/u;
  const fullHost = new RegExp(`${host.source}(${port.source})?.*/.*?`, "u");
  const sshRegex = new RegExp(`^${user.source}@${fullHost.source}$`, "u");

  return sshRegex.test(url);
};

const removeSSHFromUrl = (url: string): string => {
  return url.replace(/^ssh:\/\//u, "");
};

const formatInitialValues = (initialValues: IFormValues): IFormValues => {
  return {
    ...initialValues,
    url: removeSSHFromUrl(initialValues.url),
  };
};

export {
  chooseCredentialType,
  submittableCredentials,
  updateHealthCheckConfirm,
  formatInitialValues,
  checkCodeCommitFormat,
  checkSSHFormat,
  checkURLProtocol,
  removeSSHFromUrl,
};
