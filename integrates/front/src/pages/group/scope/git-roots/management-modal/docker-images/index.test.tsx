import { PureAbility } from "@casl/ability";
import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import { HttpResponse, type StrictResponse, graphql } from "msw";

import { DockerImages } from ".";
import {
  ADD_GIT_ROOT_DOCKER_IMAGE,
  GET_GIT_ROOT_DOCKER_IMAGES,
  GET_GROUP_DOCKER_IMAGES,
  REMOVE_GIT_ROOT_DOCKER_IMAGE,
  UPDATE_GIT_ROOT_DOCKER_IMAGE,
} from "../../../queries";
import type { IFormValues } from "../../../types";
import { formInitialValues } from "../utils";
import { CustomThemeProvider } from "components/colors";
import { authzPermissionsContext } from "context/authz/config";
import type {
  AddGitRootDockerImageMutation as AddDockerImage,
  GetGitRootDockerImagesQuery as GetGitRootDockerImages,
  GetGroupDockerImagesQuery as GetGroupDockerImages,
  GetOrgCredentialsQuery as GetOrgCredentials,
  RemoveGitRootDockerImageMutation as RemoveDockerImage,
  UpdateGitRootDockerImageMutation as UpdateDockerImage,
} from "gql/graphql";
import { ResourceState } from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage, IMessage } from "mocks/types";
import { GET_ORGANIZATION_CREDENTIALS } from "pages/organization/credentials/queries";
import { msgError, msgSuccess } from "utils/notifications";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

const DockerImage = ({
  rootId,
  groupName,
}: Readonly<{
  rootId: string;
  groupName: string;
}>): JSX.Element => {
  const handleClose: jest.Mock = jest.fn();

  const initialValues: IFormValues = {
    ...formInitialValues,
    id: rootId,
  };

  return (
    <CustomThemeProvider>
      <DockerImages
        groupName={groupName}
        initialValues={initialValues}
        onClose={handleClose}
      />
    </CustomThemeProvider>
  );
};

describe("docker images tab", (): void => {
  const graphqlMocked = graphql.link(LINK);

  const mockQueryGetCreds = [
    graphqlMocked.query(
      GET_ORGANIZATION_CREDENTIALS,
      (): StrictResponse<{ data: GetOrgCredentials }> => {
        return HttpResponse.json({
          data: {
            organization: {
              __typename: "Organization",
              credentials: [
                {
                  __typename: "Credentials",
                  azureOrganization: null,
                  id: "8d38a7de-7102-4de3-b16c-43e7c4c8915c",
                  isPat: false,
                  isToken: false,
                  name: "TEST",
                  oauthType: "",
                  owner: "owner@fluidattacks.com",
                  type: "HTTPS",
                },
                {
                  __typename: "Credentials",
                  azureOrganization: null,
                  id: "1d38a7de-7102-4de3-b16c-43e7c4c8915j",
                  isPat: false,
                  isToken: false,
                  name: "TEST_2",
                  oauthType: "",
                  owner: "owner@fluidattacks.com",
                  type: "HTTPS",
                },
              ],
              groups: [
                {
                  __typename: "Group",
                  gitRoots: {
                    __typename: "GitRootsConnection",
                    edges: [],
                  },
                  name: "test-group",
                },
              ],
              name: "okada",
            },
          },
        });
      },
    ),
  ];

  const mockMutationUpdateDockerImage = [
    graphqlMocked.mutation(
      UPDATE_GIT_ROOT_DOCKER_IMAGE,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: UpdateDockerImage }> => {
        const { rootId, groupName, uri } = variables;
        if (
          groupName === "test-group" &&
          rootId === "c7118ab2-9345-4398-895f-11ce6e3cc1a6" &&
          uri === "docker.io/library/ubuntu:latest"
        ) {
          return HttpResponse.json({
            data: {
              updateRootDockerImage: { success: true },
            },
          });
        }

        return HttpResponse.json({
          errors: [
            new GraphQLError("An error occurred updating the Docker image"),
          ],
        });
      },
    ),
  ];

  const mockMutationAddDockerImage = [
    graphqlMocked.mutation(
      ADD_GIT_ROOT_DOCKER_IMAGE,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: AddDockerImage }> => {
        const { rootId, groupName, uri } = variables;
        if (
          groupName === "test-group" &&
          rootId === "c7118ab2-9345-4398-895f-11ce6e3cc1a6" &&
          uri === "docker.io/library/hello-world:latest"
        ) {
          return HttpResponse.json({
            data: {
              addRootDockerImage: {
                sbomJobQueued: true,
                success: true,
                uri: "docker.io/library/hello-world:latest",
              },
            },
          });
        }

        return HttpResponse.json({
          errors: [
            new GraphQLError("An error occurred adding the Docker image"),
          ],
        });
      },
    ),
  ];

  const mockGetGroupDockerImages = [
    graphqlMocked.query(
      GET_GROUP_DOCKER_IMAGES,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: GetGroupDockerImages }> => {
        const { groupName } = variables;
        if (groupName === "test-group") {
          return HttpResponse.json({
            data: {
              group: {
                __typename: "Group",
                dockerImages: [
                  {
                    createdAt: "2024-03-13T17:33:52.935263+00:00",
                    createdBy: "unittest@fluidattacks.com",
                    include: true,
                    root: {
                      id: "c7118ab2-9345-4398-895f-11ce6e3cc1a6",
                      state: ResourceState.Active,
                      url: "https://gitlab.com/fluidattacks/universe.git",
                    },
                    rootId: "c7118ab2-9345-4398-895f-11ce6e3cc1a6",
                    uri: "docker.io/library/ubuntu:latest",
                  },
                ],
                name: "unittesting",
              },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error getting root Docker images")],
        });
      },
    ),
  ];

  const mockGetSingleDockerImage = [
    graphqlMocked.query(
      GET_GIT_ROOT_DOCKER_IMAGES,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: GetGitRootDockerImages }> => {
        const { groupName, rootId } = variables;
        if (
          groupName === "test-group" &&
          rootId === "c7118ab2-9345-4398-895f-11ce6e3cc1a6"
        ) {
          return HttpResponse.json({
            data: {
              root: {
                __typename: "GitRoot",
                dockerImages: [
                  {
                    credentials: {
                      __typename: "Credentials",
                      azureOrganization: null,
                      id: "8d38a7de-7102-4de3-b16c-43e7c4c8915c",
                      isPat: false,
                      isToken: false,
                      name: "TEST",
                      oauthType: "",
                      owner: "owner@fluidattacks.com",
                      type: "HTTPS",
                    },
                    include: true,
                    uri: "docker.io/library/ubuntu:latest",
                  },
                ],
                id: "c7118ab2-9345-4398-895f-11ce6e3cc1a6",
              },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error getting root Docker images")],
        });
      },
    ),
  ];

  it("should render Docker image tab", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const mockQuery = [
      graphqlMocked.query(
        GET_GIT_ROOT_DOCKER_IMAGES,
        ({
          variables,
        }): StrictResponse<
          IErrorMessage | { data: GetGitRootDockerImages }
        > => {
          const { groupName, rootId } = variables;
          if (
            groupName === "test-group" &&
            rootId === "c7118ab2-9345-4398-895f-11ce6e3cc1a6"
          ) {
            return HttpResponse.json({
              data: {
                root: {
                  __typename: "GitRoot",
                  dockerImages: [
                    {
                      credentials: {
                        __typename: "Credentials",
                        azureOrganization: null,
                        id: "8d38a7de-7102-4de3-b16c-43e7c4c8915c",
                        isPat: false,
                        isToken: false,
                        name: "TEST",
                        oauthType: "",
                        owner: "owner@fluidattacks.com",
                        type: "HTTPS",
                      },
                      include: true,
                      uri: "docker.io/library/ubuntu:latest",
                    },
                    {
                      credentials: {
                        __typename: "Credentials",
                        azureOrganization: null,
                        id: "8d38a7de-7102-4de3-b16c-43e7c4c8915c",
                        isPat: false,
                        isToken: false,
                        name: "TEST",
                        oauthType: "",
                        owner: "owner@fluidattacks.com",
                        type: "HTTPS",
                      },
                      include: false,
                      uri: "docker.io/library/fedora:latest",
                    },
                    {
                      credentials: {
                        __typename: "Credentials",
                        azureOrganization: null,
                        id: "8d38a7de-7102-4de3-b16c-43e7c4c8915c",
                        isPat: false,
                        isToken: false,
                        name: "TEST",
                        oauthType: "",
                        owner: "owner@fluidattacks.com",
                        type: "HTTPS",
                      },
                      include: false,
                      uri: "docker.io/library/alpine:latest",
                    },
                  ],
                  id: "c7118ab2-9345-4398-895f-11ce6e3cc1a6",
                },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error getting root Docker images")],
          });
        },
      ),
    ];

    render(
      <DockerImage
        groupName={"test-group"}
        rootId={"c7118ab2-9345-4398-895f-11ce6e3cc1a6"}
      />,
      {
        mocks: mockQuery,
      },
    );

    await waitFor((): void => {
      expect(screen.queryAllByRole("table")).toHaveLength(1);
    });

    expect(screen.queryByText("URI")).toBeInTheDocument();

    await waitFor((): void => {
      expect(screen.getAllByRole("row")).toHaveLength(4);
    });

    await waitFor((): void => {
      expect(
        document.querySelectorAll("#git-root-remove-docker-image"),
      ).toHaveLength(3);
    });

    expect(
      screen.getByRole("button", {
        name: "group.scope.git.dockerImages.add.button",
      }),
    ).toBeInTheDocument();
  });

  it("should handle errors loading Docker images tab", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const mockError = [
      graphqlMocked.query(
        GET_GIT_ROOT_DOCKER_IMAGES,
        (): StrictResponse<Record<"errors", IMessage[]>> => {
          return HttpResponse.json({
            errors: [
              new GraphQLError("Exception - Access denied or root not found"),
              new GraphQLError("Couldn't load docker images"),
            ],
          });
        },
      ),
    ];

    render(
      <DockerImage
        groupName={"test-group"}
        rootId={"c7118ab2-9345-4398-895f-11ce6e3cc1a13"}
      />,
      {
        mocks: mockError,
      },
    );

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledTimes(2);
    });

    expect(msgError).toHaveBeenLastCalledWith("groupAlerts.errorTextsad");
  });

  it("should remove Docker image", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const mockMutation = [
      graphqlMocked.mutation(
        REMOVE_GIT_ROOT_DOCKER_IMAGE,
        ({
          variables,
        }): StrictResponse<IErrorMessage | { data: RemoveDockerImage }> => {
          const { groupName, rootId, uri } = variables;
          if (
            groupName === "test-group" &&
            rootId === "c7118ab2-9345-4398-895f-11ce6e3cc1a6" &&
            uri === "docker.io/library/ubuntu:latest"
          ) {
            return HttpResponse.json({
              data: {
                removeRootDockerImage: { success: true },
              },
            });
          }

          return HttpResponse.json({
            errors: [
              new GraphQLError("An error occurred removing the Docker image"),
            ],
          });
        },
      ),
    ];

    render(
      <DockerImage
        groupName={"test-group"}
        rootId={"c7118ab2-9345-4398-895f-11ce6e3cc1a6"}
      />,
      {
        mocks: [
          ...mockGetSingleDockerImage,
          ...mockMutation,
          ...mockGetGroupDockerImages,
        ],
      },
    );

    await waitFor((): void => {
      expect(screen.queryAllByRole("table")).toHaveLength(1);
    });

    expect(screen.queryByText("URI")).toBeInTheDocument();

    await waitFor((): void => {
      expect(screen.getAllByRole("row")).toHaveLength(2);
    });

    await userEvent.click(
      document.querySelectorAll("#git-root-remove-docker-image")[0]
        .firstChild as Element,
    );

    await waitFor((): void => {
      expect(
        screen.getByText("group.scope.git.dockerImages.remove.title"),
      ).toBeInTheDocument();
    });

    await userEvent.click(screen.getByRole("button", { name: "Confirm" }));

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "group.scope.git.dockerImages.remove.success",
        "group.scope.git.dockerImages.remove.successTitle",
      );
    });
  });

  it("should handle erros while removing Docker image", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const mockError = [
      graphqlMocked.mutation(
        REMOVE_GIT_ROOT_DOCKER_IMAGE,
        (): StrictResponse<Record<"errors", IMessage[]>> => {
          return HttpResponse.json({
            errors: [
              new GraphQLError("Exception - Root image not found"),
              new GraphQLError("Couldn't remove Docker image"),
            ],
          });
        },
      ),
    ];

    render(
      <DockerImage
        groupName={"test-group"}
        rootId={"c7118ab2-9345-4398-895f-11ce6e3cc1a6"}
      />,
      {
        mocks: [
          ...mockGetSingleDockerImage,
          ...mockError,
          ...mockGetGroupDockerImages,
        ],
      },
    );

    await waitFor((): void => {
      expect(screen.queryAllByRole("table")).toHaveLength(1);
    });

    expect(screen.queryByText("URI")).toBeInTheDocument();

    await waitFor((): void => {
      expect(screen.getAllByRole("row")).toHaveLength(2);
    });

    await userEvent.click(
      document.querySelectorAll("#git-root-remove-docker-image")[0]
        .firstChild as Element,
    );

    await waitFor((): void => {
      expect(
        screen.getByText("group.scope.git.dockerImages.remove.title"),
      ).toBeInTheDocument();
    });

    await userEvent.click(screen.getByRole("button", { name: "Confirm" }));

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledTimes(2);
    });

    expect(msgError).toHaveBeenLastCalledWith("groupAlerts.errorTextsad");
  });

  it("should add Docker image with credentials", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            { action: "integrates_api_mutations_add_root_docker_image_mutate" },
          ])
        }
      >
        <DockerImage
          groupName={"test-group"}
          rootId={"c7118ab2-9345-4398-895f-11ce6e3cc1a6"}
        />
      </authzPermissionsContext.Provider>,
      {
        mocks: [...mockQueryGetCreds, ...mockMutationAddDockerImage],
      },
    );

    await waitFor((): void => {
      expect(screen.queryAllByRole("table")).toHaveLength(1);
    });

    expect(screen.queryByText("URI")).toBeInTheDocument();

    await waitFor((): void => {
      expect(screen.getAllByRole("row")).toHaveLength(2);
    });

    await waitFor((): void => {
      expect(screen.queryAllByRole("button")).toHaveLength(2);
    });

    await userEvent.click(
      screen.getByText("group.scope.git.dockerImages.add.button"),
    );

    await waitFor((): void => {
      expect(
        screen.getByText("group.scope.git.dockerImages.add.title"),
      ).toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(
        screen.getByText("group.scope.git.dockerImages.add.inputLabel"),
      ).toBeInTheDocument();
    });

    await userEvent.type(
      screen.getByRole("textbox", { name: "uri" }),
      "docker.io/library/hello-world:latest",
    );

    await waitFor((): void => {
      expect(screen.getByRole("textbox", { name: "uri" })).toHaveValue(
        "docker.io/library/hello-world:latest",
      );
    });

    await waitFor((): void => {
      expect(
        screen.getByText("group.scope.git.dockerImages.add.repositoryPrivacy"),
      ).toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(screen.getAllByRole("radio")).toHaveLength(2);
    });

    await userEvent.click(screen.getByRole("radio", { name: "Yes" }));

    await waitFor((): void => {
      expect(
        screen.getByText("group.scope.git.dockerImages.add.credentialsTitle"),
      ).toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(
        screen.getByText("group.scope.git.dockerImages.add.existingCredential"),
      ).toBeInTheDocument();
    });

    expect(screen.getByRole("button", { name: "Confirm" })).toBeDisabled();

    await userEvent.click(
      screen.getByText("group.scope.git.dockerImages.add.existingCredential"),
    );
    await userEvent.click(screen.getByText("TEST"));

    await waitFor((): void => {
      expect(
        screen.getByRole("button", { name: "Confirm" }),
      ).not.toBeDisabled();
    });

    await userEvent.click(screen.getByRole("button", { name: "Confirm" }));

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "group.scope.git.dockerImages.add.success",
        "group.scope.git.dockerImages.add.successTitle",
      );
    });
  });

  it("should add docker image without credencials", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            { action: "integrates_api_mutations_add_root_docker_image_mutate" },
          ])
        }
      >
        <DockerImage
          groupName={"test-group"}
          rootId={"c7118ab2-9345-4398-895f-11ce6e3cc1a6"}
        />
      </authzPermissionsContext.Provider>,
      {
        mocks: [...mockQueryGetCreds, ...mockMutationAddDockerImage],
      },
    );

    await waitFor((): void => {
      expect(screen.queryAllByRole("table")).toHaveLength(1);
    });

    expect(screen.queryByText("URI")).toBeInTheDocument();

    await waitFor((): void => {
      expect(screen.getAllByRole("row")).toHaveLength(2);
    });

    await waitFor((): void => {
      expect(screen.queryAllByRole("button")).toHaveLength(2);
    });

    await userEvent.click(
      screen.getByText("group.scope.git.dockerImages.add.button"),
    );

    await waitFor((): void => {
      expect(
        screen.getByText("group.scope.git.dockerImages.add.title"),
      ).toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(
        screen.getByText("group.scope.git.dockerImages.add.inputLabel"),
      ).toBeInTheDocument();
    });

    await userEvent.type(
      screen.getByRole("textbox", { name: "uri" }),
      "docker.io/library/hello-world:latest",
    );

    await waitFor((): void => {
      expect(screen.getByRole("textbox", { name: "uri" })).toHaveValue(
        "docker.io/library/hello-world:latest",
      );
    });

    await waitFor((): void => {
      expect(
        screen.getByText("group.scope.git.dockerImages.add.repositoryPrivacy"),
      ).toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(screen.getAllByRole("radio")).toHaveLength(2);
    });

    await userEvent.click(screen.getByRole("radio", { name: "No" }));

    expect(screen.getByRole("button", { name: "Confirm" })).toBeEnabled();

    await waitFor((): void => {
      expect(
        screen.getByRole("button", { name: "Confirm" }),
      ).not.toBeDisabled();
    });

    await userEvent.click(screen.getByRole("button", { name: "Confirm" }));

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "group.scope.git.dockerImages.add.success",
        "group.scope.git.dockerImages.add.successTitle",
      );
    });
  });

  it("should handle errors while adding Docker images", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const mockError = [
      graphqlMocked.mutation(
        ADD_GIT_ROOT_DOCKER_IMAGE,
        (): StrictResponse<Record<"errors", IMessage[]>> => {
          return HttpResponse.json({
            errors: [
              new GraphQLError("Exception - Docker image URI already exists"),
              new GraphQLError("Exception - Root image not found"),
              new GraphQLError(
                "Exception - Access denied or credential not found",
              ),
              new GraphQLError("Exception - URI format incorrect"),
              new GraphQLError("Another error"),
            ],
          });
        },
      ),
    ];

    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            { action: "integrates_api_mutations_add_root_docker_image_mutate" },
          ])
        }
      >
        <DockerImage
          groupName={"test-group"}
          rootId={"c7118ab2-9345-4398-895f-11ce6e3cc1a6"}
        />
      </authzPermissionsContext.Provider>,
      {
        mocks: [...mockError, ...mockQueryGetCreds],
      },
    );

    await userEvent.click(
      screen.getByText("group.scope.git.dockerImages.add.button"),
    );

    await userEvent.type(
      screen.getByRole("textbox", { name: "uri" }),
      "docker.io/library/hello-world:latest",
    );

    await userEvent.click(screen.getByRole("radio", { name: "No" }));

    await userEvent.click(screen.getByRole("button", { name: "Confirm" }));

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledTimes(5);
    });

    expect(msgError).toHaveBeenCalledWith(
      "validations.dockerImages.imageAlreadyExists",
    );
    expect(msgError).toHaveBeenCalledWith(
      "validations.dockerImages.imageNotFound",
    );
    expect(msgError).toHaveBeenCalledWith(
      "validations.dockerImages.wrongCredentials",
    );
    expect(msgError).toHaveBeenCalledWith(
      "validations.dockerImages.wrongUriFormat",
    );

    expect(msgError).toHaveBeenLastCalledWith("groupAlerts.errorTextsad");
  });

  it("should handle errors credentials errors while adding Docker images", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const mockError = [
      graphqlMocked.query(
        GET_ORGANIZATION_CREDENTIALS,
        (): StrictResponse<Record<"errors", IMessage[]>> => {
          return HttpResponse.json({
            errors: [new GraphQLError("Error loading credentials")],
          });
        },
      ),
    ];

    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            { action: "integrates_api_mutations_add_root_docker_image_mutate" },
          ])
        }
      >
        <DockerImage
          groupName={"test-group"}
          rootId={"c7118ab2-9345-4398-895f-11ce6e3cc1a6"}
        />
      </authzPermissionsContext.Provider>,
      {
        mocks: [...mockError],
      },
    );

    await userEvent.click(
      screen.getByText("group.scope.git.dockerImages.add.button"),
    );

    await userEvent.click(screen.getByRole("radio", { name: "Yes" }));

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledTimes(1);
    });

    expect(msgError).toHaveBeenLastCalledWith("groupAlerts.errorTextsad");
  });

  it("should edit Docker image with credentials", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            {
              action:
                "integrates_api_mutations_update_root_docker_image_mutate",
            },
          ])
        }
      >
        <DockerImage
          groupName={"test-group"}
          rootId={"c7118ab2-9345-4398-895f-11ce6e3cc1a6"}
        />
      </authzPermissionsContext.Provider>,
      {
        mocks: [
          ...mockGetSingleDockerImage,
          ...mockGetGroupDockerImages,
          ...mockQueryGetCreds,
          ...mockMutationUpdateDockerImage,
        ],
      },
    );

    await waitFor((): void => {
      expect(screen.queryAllByRole("table")).toHaveLength(1);
    });

    expect(screen.queryByText("URI")).toBeInTheDocument();

    await waitFor((): void => {
      expect(screen.getAllByRole("row")).toHaveLength(2);
    });

    await waitFor((): void => {
      expect(screen.queryAllByRole("button")).toHaveLength(4);
    });

    await userEvent.click(
      document.querySelectorAll("#git-root-edit-docker-image")[0]
        .firstChild as Element,
    );

    await waitFor((): void => {
      expect(
        screen.getByText("group.scope.git.dockerImages.edit.title"),
      ).toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(
        screen.getByText("group.scope.git.dockerImages.add.inputLabel"),
      ).toBeInTheDocument();
    });

    expect(screen.getByRole("textbox", { name: "uri" })).toBeDisabled();

    await waitFor((): void => {
      expect(screen.getByRole("textbox", { name: "uri" })).toHaveValue(
        "docker.io/library/ubuntu:latest",
      );
    });

    await waitFor((): void => {
      expect(
        screen.getByText("group.scope.git.dockerImages.add.repositoryPrivacy"),
      ).toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(screen.getAllByRole("radio")).toHaveLength(2);
    });

    await userEvent.click(screen.getByRole("radio", { name: "Yes" }));

    await waitFor((): void => {
      expect(
        screen.getByText("group.scope.git.dockerImages.add.credentialsTitle"),
      ).toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(
        screen.getByText("group.scope.git.dockerImages.add.existingCredential"),
      ).toBeInTheDocument();
    });

    expect(screen.getByRole("button", { name: "Confirm" })).toBeDisabled();

    await userEvent.click(
      screen.getByText("group.scope.git.dockerImages.add.existingCredential"),
    );
    await userEvent.click(screen.getByText("TEST_2"));

    await waitFor((): void => {
      expect(
        screen.getByRole("button", { name: "Confirm" }),
      ).not.toBeDisabled();
    });

    await userEvent.click(screen.getByRole("button", { name: "Confirm" }));

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "group.scope.git.dockerImages.edit.success",
        "group.scope.git.dockerImages.edit.successTitle",
      );
    });
  });

  it("should edit Docker image without credencials", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            {
              action:
                "integrates_api_mutations_update_root_docker_image_mutate",
            },
          ])
        }
      >
        <DockerImage
          groupName={"test-group"}
          rootId={"c7118ab2-9345-4398-895f-11ce6e3cc1a6"}
        />
      </authzPermissionsContext.Provider>,
      {
        mocks: [
          ...mockGetSingleDockerImage,
          ...mockGetGroupDockerImages,
          ...mockQueryGetCreds,
          ...mockMutationUpdateDockerImage,
        ],
      },
    );

    await waitFor((): void => {
      expect(screen.queryAllByRole("table")).toHaveLength(1);
    });

    expect(screen.queryByText("URI")).toBeInTheDocument();

    await waitFor((): void => {
      expect(screen.getAllByRole("row")).toHaveLength(2);
    });

    await waitFor((): void => {
      expect(screen.queryAllByRole("button")).toHaveLength(4);
    });

    await userEvent.click(
      document.querySelectorAll("#git-root-edit-docker-image")[0]
        .firstChild as Element,
    );

    await waitFor((): void => {
      expect(
        screen.getByText("group.scope.git.dockerImages.edit.title"),
      ).toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(
        screen.getByText("group.scope.git.dockerImages.add.inputLabel"),
      ).toBeInTheDocument();
    });

    expect(screen.getByRole("textbox", { name: "uri" })).toBeDisabled();

    await waitFor((): void => {
      expect(screen.getByRole("textbox", { name: "uri" })).toHaveValue(
        "docker.io/library/ubuntu:latest",
      );
    });

    await waitFor((): void => {
      expect(
        screen.getByText("group.scope.git.dockerImages.add.repositoryPrivacy"),
      ).toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(screen.getAllByRole("radio")).toHaveLength(2);
    });

    expect(screen.getByRole("button", { name: "Confirm" })).toBeDisabled();

    await userEvent.click(screen.getByRole("radio", { name: "No" }));

    await waitFor((): void => {
      expect(
        screen.getByRole("button", { name: "Confirm" }),
      ).not.toBeDisabled();
    });

    await userEvent.click(screen.getByRole("button", { name: "Confirm" }));

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "group.scope.git.dockerImages.edit.success",
        "group.scope.git.dockerImages.edit.successTitle",
      );
    });
  });

  it("should handle errors while updating Docker images", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const mockError = [
      graphqlMocked.mutation(
        UPDATE_GIT_ROOT_DOCKER_IMAGE,
        (): StrictResponse<Record<"errors", IMessage[]>> => {
          return HttpResponse.json({
            errors: [
              new GraphQLError("Exception - Root image not found"),
              new GraphQLError(
                "Exception - Access denied or credential not found",
              ),
              new GraphQLError("Another error"),
            ],
          });
        },
      ),
    ];

    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            {
              action:
                "integrates_api_mutations_update_root_docker_image_mutate",
            },
          ])
        }
      >
        <DockerImage
          groupName={"test-group"}
          rootId={"c7118ab2-9345-4398-895f-11ce6e3cc1a6"}
        />
      </authzPermissionsContext.Provider>,
      {
        mocks: [
          ...mockGetSingleDockerImage,
          ...mockGetGroupDockerImages,
          ...mockQueryGetCreds,
          ...mockError,
        ],
      },
    );

    await waitFor((): void => {
      expect(screen.queryAllByRole("table")).toHaveLength(1);
    });

    expect(screen.queryByText("URI")).toBeInTheDocument();

    await waitFor((): void => {
      expect(screen.getAllByRole("row")).toHaveLength(2);
    });

    await waitFor((): void => {
      expect(screen.queryAllByRole("button")).toHaveLength(4);
    });

    await userEvent.click(
      document.querySelectorAll("#git-root-edit-docker-image")[0]
        .firstChild as Element,
    );

    await userEvent.click(screen.getByRole("radio", { name: "No" }));

    await userEvent.click(screen.getByRole("button", { name: "Confirm" }));

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledTimes(3);
    });

    expect(msgError).toHaveBeenCalledWith(
      "validations.dockerImages.imageNotFound",
    );
    expect(msgError).toHaveBeenCalledWith(
      "validations.dockerImages.wrongCredentials",
    );

    expect(msgError).toHaveBeenLastCalledWith("groupAlerts.errorTextsad");
  });
});
