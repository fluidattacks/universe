interface IAddEnvironmentProps {
  groupName: string;
  rootAwsExternalId: string;
  rootId: string;
  closeFunction: () => void;
  onUpdate?: () => void;
}

interface IFormProps {
  cloudName?: string;
  groupName: string;
  url: string;
  type: string;
  rootId: string;
  urlType: string;
}

interface IEnvironmentNoSuccess {
  cloudName?: string;
  url: string;
  urlType: string;
  useEgress: boolean;
  useVpn: boolean;
  useZtna: boolean;
  isProduction?: string;
}

interface IEnvironmentSubmit extends IEnvironmentNoSuccess {
  azureClientId: string;
  azureClientSecret: string;
  azureTenantId: string;
  gcpPrivateKey: string;
  isProduction?: string;
  groupName: string;
  rootId: string;
  type: string;
}

interface IAddEnvironmentURL {
  addGitEnvironmentUrl: { urlId: string; success: boolean };
}

interface IVerifyEnvironmentIntegrity {
  verifyEnvironmentIntegrity: boolean;
}

export type {
  IAddEnvironmentProps,
  IEnvironmentNoSuccess,
  IEnvironmentSubmit,
  IFormProps,
  IAddEnvironmentURL,
  IVerifyEnvironmentIntegrity,
};
