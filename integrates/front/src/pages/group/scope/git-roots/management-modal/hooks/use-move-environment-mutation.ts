import type { MutationTuple } from "@apollo/client";
import { useMutation } from "@apollo/client";
import { useTranslation } from "react-i18next";

import type {
  MoveEnvironmentMutation,
  MoveEnvironmentMutationVariables,
} from "gql/graphql";
import { MOVE_ENVIRONMENT } from "pages/group/scope/queries";
import { msgError, msgSuccess } from "utils/notifications";

const useMoveEnvironmentMutation = (): MutationTuple<
  MoveEnvironmentMutation,
  MoveEnvironmentMutationVariables
> => {
  const { t } = useTranslation();

  return useMutation(MOVE_ENVIRONMENT, {
    onCompleted: (): void => {
      msgSuccess(
        t(`group.scope.git.moveEnvironment.success.message`),
        t(`group.scope.git.moveEnvironment.success.title`),
      );
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        switch (error.message) {
          case "Exception - Access denied or root not found":
            msgError(t(`group.scope.git.moveEnvironment.errors.rootNotFound`));
            break;
          case "Exception - Access denied or root environment url not found":
            msgError(t(`group.scope.git.moveEnvironment.errors.envNotFound`));
            break;
          default:
            msgError(t("groupAlerts.errorTextsad"));
        }
      });
    },
  });
};

export { useMoveEnvironmentMutation };
