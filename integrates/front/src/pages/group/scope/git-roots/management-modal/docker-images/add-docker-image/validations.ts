import { object, string } from "yup";

import { translate } from "utils/translations/translate";

export const validationSchema = object().shape({
  requiresAuthentication: string().required(
    translate.t("validations.required"),
  ),
  uri: string().required(translate.t("validations.required")),
});
