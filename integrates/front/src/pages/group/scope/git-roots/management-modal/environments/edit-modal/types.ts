import type { IUseModal } from "hooks/use-modal";
import type { IBasicEnvironmentUrl } from "pages/group/scope/types";

interface IEditModalProps {
  groupName: string;
  organizationName: string;
  gitEnvironment: IBasicEnvironmentUrl;
  modalRef: IUseModal;
  rootId: string;
}

export type { IEditModalProps };
