import { useCallback, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import type { IOauthRootModalProps } from "../types";
import { Modal } from "components/modal";
import { AddOauthRootForm } from "features/add-oauth-root-form";

const OauthRootModal: React.FC<IOauthRootModalProps> = ({
  addOauthRootModal,
  onUpdate,
  oauthProvider,
}: IOauthRootModalProps): JSX.Element => {
  const { t } = useTranslation();
  const [isCredentialSelected, setIsCredentialSelected] = useState(false);

  const closeOauthRootModal = useCallback((): void => {
    addOauthRootModal.close();
    setIsCredentialSelected(false);
  }, [addOauthRootModal]);

  return (
    <Modal
      modalRef={{ ...addOauthRootModal, close: closeOauthRootModal }}
      size={"md"}
      title={
        isCredentialSelected
          ? t("group.scope.git.oauthModal.title2")
          : t("group.scope.git.oauthModal.title1")
      }
    >
      <AddOauthRootForm
        closeModal={closeOauthRootModal}
        isCredentialSelected={isCredentialSelected}
        onUpdate={onUpdate}
        provider={oauthProvider}
        setIsCredentialSelected={setIsCredentialSelected}
      />
    </Modal>
  );
};

export { OauthRootModal };
