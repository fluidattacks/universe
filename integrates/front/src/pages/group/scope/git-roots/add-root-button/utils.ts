import type { ICredentialsAttr } from "./types";

type TOAuthProvider = "Azure" | "Bitbucket" | "GitHub" | "GitLab";

const isAvailable = (
  credentials: ICredentialsAttr[],
  provider: ICredentialsAttr["oauthType"],
): boolean => {
  return (
    credentials.filter(
      (credential): boolean =>
        credential.type === "OAUTH" && credential.oauthType === provider,
    ).length >= 1
  );
};

const showProviders = (credentials: ICredentialsAttr[]): TOAuthProvider[] => {
  const providers: [TOAuthProvider, boolean][] = [
    ["Azure", isAvailable(credentials, "AZURE")],
    ["Bitbucket", isAvailable(credentials, "BITBUCKET")],
    ["GitHub", isAvailable(credentials, "GITHUB")],
    ["GitLab", isAvailable(credentials, "GITLAB")],
  ];

  return providers
    .filter(([, available]): boolean => available)
    .reduce((acc: TOAuthProvider[], [provider]): TOAuthProvider[] => {
      return [...acc, provider];
    }, []);
};

export { showProviders, type TOAuthProvider };
