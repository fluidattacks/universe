import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import { Route, Routes, useNavigate } from "react-router-dom";

import { GET_ORGANIZATION_CREDENTIALS } from "./queries";

import { AddRootButton } from ".";
import type { GetOrganizationCredentialsQuery as GetOrganizationCredentials } from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";

jest.mock(
  "react-router-dom",
  (): Record<string, unknown> => ({
    ...jest.requireActual("react-router-dom"),
    useNavigate: jest.fn(),
    useParams: jest.fn().mockReturnValue({ organizationName: "okada" }),
  }),
);

describe("addRootButton", (): void => {
  const graphqlMocked = graphql.link(LINK);

  const memoryRouter = {
    initialEntries: ["/orgs/okada/unittesting/scope"],
  };

  const mocks = [
    graphqlMocked.query(
      GET_ORGANIZATION_CREDENTIALS,
      (): StrictResponse<{ data: GetOrganizationCredentials }> => {
        return HttpResponse.json({
          data: {
            organization: {
              __typename: "Organization",
              credentials: [
                {
                  __typename: "Credentials",
                  azureOrganization: null,
                  id: "6e52c11c-abf7-4ca3-b7d0-635e394f41c1",
                  isPat: false,
                  isToken: false,
                  name: "GiltHub",
                  oauthType: "GITHUB",
                  owner: "owner@test.com",
                  type: "OAUTH",
                },
                {
                  __typename: "Credentials",
                  azureOrganization: null,
                  id: "6e52c11c-abf7-4ca3-b7d0-635e394f41c2",
                  isPat: false,
                  isToken: false,
                  name: "GiltLab",
                  oauthType: "GITLAB",
                  owner: "owner@test.com",
                  type: "OAUTH",
                },
              ],
              name: "okada",
            },
          },
        });
      },
    ),
  ];

  it("should display available providers", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route
          element={
            <AddRootButton manualClick={jest.fn()} providersClick={jest.fn()} />
          }
          path={"/orgs/:organizationName/:groupName/scope"}
        />
      </Routes>,
      { memoryRouter, mocks },
    );

    await waitFor((): void => {
      expect(screen.getByText("group.scope.common.add")).toBeInTheDocument();
    });

    await userEvent.click(screen.getByText("group.scope.common.add"));

    expect(screen.getByText("GitHub")).toBeInTheDocument();
    expect(screen.getByText("GitLab")).toBeInTheDocument();
    expect(
      screen.getByText("components.repositoriesDropdown.manual.text"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("components.repositoriesDropdown.otherButton.text"),
    ).toBeInTheDocument();
    expect(screen.queryByText("Bitbucket")).not.toBeInTheDocument();
    expect(screen.queryByText("Azure")).not.toBeInTheDocument();
  });

  it("should handle clicks", async (): Promise<void> => {
    expect.hasAssertions();

    const navigate = jest.fn();
    jest.mocked(useNavigate).mockReturnValue(navigate);
    const providersClick = jest.fn();
    const manualClick = jest.fn();

    render(
      <Routes>
        <Route
          element={
            <AddRootButton
              manualClick={manualClick}
              providersClick={providersClick}
            />
          }
          path={"/orgs/:organizationName/:groupName/scope"}
        />
      </Routes>,
      { memoryRouter, mocks },
    );

    await waitFor((): void => {
      expect(screen.getByText("group.scope.common.add")).toBeInTheDocument();
    });

    await userEvent.click(screen.getByText("group.scope.common.add"));

    await userEvent.click(
      screen.getByText("components.repositoriesDropdown.otherButton.text"),
    );

    expect(navigate).toHaveBeenCalledTimes(1);
    expect(navigate).toHaveBeenCalledWith("/orgs/okada/credentials");

    await userEvent.click(
      screen.getByText("components.repositoriesDropdown.manual.text"),
    );

    expect(manualClick).toHaveBeenCalledTimes(1);

    await userEvent.click(screen.getByText("GitHub"));

    expect(providersClick).toHaveBeenCalledWith("GITHUB");

    await userEvent.click(screen.getByText("GitLab"));

    expect(providersClick).toHaveBeenCalledWith("GITLAB");

    expect(screen.queryByText("Bitbucket")).not.toBeInTheDocument();

    expect(screen.queryByText("Azure")).not.toBeInTheDocument();
  });

  it("should handle clicks other providers", async (): Promise<void> => {
    expect.hasAssertions();

    const navigate = jest.fn();
    jest.mocked(useNavigate).mockReturnValue(navigate);
    const providersClick = jest.fn();
    const manualClick = jest.fn();
    const mocksProv = [
      graphqlMocked.query(
        GET_ORGANIZATION_CREDENTIALS,
        (): StrictResponse<{ data: GetOrganizationCredentials }> => {
          return HttpResponse.json({
            data: {
              organization: {
                __typename: "Organization",
                credentials: [
                  {
                    __typename: "Credentials",
                    azureOrganization: null,
                    id: "4e52c11c-abf7-4ca3-b7d0-635e394f41c1",
                    isPat: false,
                    isToken: false,
                    name: "Azure",
                    oauthType: "AZURE",
                    owner: "owner@test.com",
                    type: "OAUTH",
                  },
                  {
                    __typename: "Credentials",
                    azureOrganization: null,
                    id: "4e52c11c-abf7-4ca3-b7d0-635e394f41c2",
                    isPat: false,
                    isToken: false,
                    name: "Bitbucket",
                    oauthType: "BITBUCKET",
                    owner: "owner@test.com",
                    type: "OAUTH",
                  },
                ],
                name: "okada",
              },
            },
          });
        },
      ),
    ];
    render(
      <Routes>
        <Route
          element={
            <AddRootButton
              manualClick={manualClick}
              providersClick={providersClick}
            />
          }
          path={"/orgs/:organizationName/:groupName/scope"}
        />
      </Routes>,
      { memoryRouter, mocks: mocksProv },
    );

    await waitFor((): void => {
      expect(screen.getByText("group.scope.common.add")).toBeInTheDocument();
    });

    await userEvent.click(screen.getByText("group.scope.common.add"));

    await userEvent.click(
      screen.getByText("components.repositoriesDropdown.otherButton.text"),
    );

    expect(navigate).toHaveBeenCalledTimes(1);
    expect(navigate).toHaveBeenCalledWith("/orgs/okada/credentials");

    await userEvent.click(
      screen.getByText("components.repositoriesDropdown.manual.text"),
    );

    expect(manualClick).toHaveBeenCalledTimes(1);

    await userEvent.click(screen.getByText("Azure"));

    expect(providersClick).toHaveBeenCalledWith("AZURE");

    await userEvent.click(screen.getByText("Bitbucket"));

    expect(providersClick).toHaveBeenCalledWith("BITBUCKET");

    expect(screen.queryByText("GitHub")).not.toBeInTheDocument();

    expect(screen.queryByText("GitLab")).not.toBeInTheDocument();
  });
});
