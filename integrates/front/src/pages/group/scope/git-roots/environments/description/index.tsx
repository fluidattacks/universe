import { Heading, Link, Row } from "@fluidattacks/design";
import { useTranslation } from "react-i18next";

import type { IDescriptionProps } from "../types";
import { List } from "components/list";
import { formatDateTime } from "utils/date";
import { includeTagsFormatter } from "utils/format-helpers";

const Description = ({
  repositoryUrls,
  createdAt,
  createdBy,
  url,
}: IDescriptionProps): JSX.Element => {
  const { t } = useTranslation();

  return (
    <div>
      <Heading fontWeight={"bold"} mb={0.5} size={"xs"}>
        {t("group.scope.git.envUrl")}
      </Heading>
      <Row>
        <div className={"pl3"}>
          <List
            items={[
              <Link href={url} key={"envUrl"}>
                {url}
              </Link>,
              `${t(
                "group.scope.git.createdAt",
              )} ${formatDateTime(createdAt as unknown as string)}`,
              `${t("group.scope.git.createdBy")} ${createdBy ?? "-"}`,
            ]}
          />
        </div>
      </Row>
      <Heading fontWeight={"bold"} mb={0.5} size={"xs"}>
        {t("group.scope.git.title")}
      </Heading>
      <Row>
        <div className={"pl3"}>
          <List
            items={repositoryUrls.map(
              (_url): JSX.Element =>
                includeTagsFormatter({
                  excludeTag: !_url.include,
                  text: _url.root.url,
                }),
            )}
          />
        </div>
      </Row>
    </div>
  );
};

export const renderEnvDescription = (props: IDescriptionProps): JSX.Element => (
  <Description
    createdAt={props.createdAt}
    createdBy={props.createdBy}
    repositoryUrls={props.repositoryUrls}
    url={props.url}
  />
);
