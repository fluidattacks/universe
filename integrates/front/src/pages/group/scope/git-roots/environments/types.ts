import type { IEnvironmentGitRootData } from "../../types";
import type { IUseTable } from "hooks/use-table";

interface IRootAttr {
  id: string;
  nickname: string;
}
interface IEventsData {
  closingDate: string;
  detail: string;
  eventDate: string;
  eventStatus: string;
  eventType: string;
  id: string;
  groupName: string;
  root: IRootAttr | null;
  environment: string | null;
}

interface IEnvUrlsData {
  cloudName: string | null;
  createdAt: string | null;
  createdBy: string | null;
  id: string;
  url: string;
  urlType: string;
  countSecrets?: number;
  repositoryUrls: IEnvironmentGitRootData[];
}

interface ICombinedEnvUrlsData {
  groupName: string;
  countSecrets?: number;
  cloudName: string | null;
  createdAt: string | null;
  createdBy: string | null;
  id: string;
  url: string;
  urlType: string;
  useEgress: boolean;
  useVpn: boolean;
  useZtna: boolean;
  repositoryUrls: IEnvironmentGitRootData[];
  eventStatus: string | undefined;
  eventEnvironment: string | undefined;
  eventId: string | undefined;
}
interface IEnvironmentsProps {
  eventsDataSet: {
    closingDate: string | null;
    detail: string;
    eventDate: string;
    eventStatus: string;
    eventType: string;
    id: string;
    groupName: string;
    root: IRootAttr | null;
    environment: string | null;
  }[];
  envUrlsDataSet: {
    cloudName: string | null;
    createdAt: string | null;
    createdBy: string | null;
    id: string;
    url: string;
    urlType: string;
    useEgress: boolean;
    useVpn: boolean;
    useZtna: boolean;
    countSecrets?: number;
    repositoryUrls: IEnvironmentGitRootData[];
  }[];
  groupName: string;
  tableRef: IUseTable;
}

interface IDescriptionProps {
  repositoryUrls: IEnvironmentGitRootData[];
  createdAt: string | null;
  createdBy: string | null;
  url: string;
}

export type {
  IEnvironmentsProps,
  IDescriptionProps,
  ICombinedEnvUrlsData,
  IEnvUrlsData,
  IEventsData,
  IRootAttr,
};
