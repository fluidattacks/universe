import { Tag } from "@fluidattacks/design";
import type { CellContext } from "@tanstack/react-table";
import * as React from "react";
import { useTranslation } from "react-i18next";

import type { IEnvironmentGitRootData } from "pages/group/scope/types";
import { includeTagsFormatter } from "utils/format-helpers";

interface IEnvironmentStatusColumProps {
  readonly details: {
    cloudName: string | null;
    createdAt: string | null;
    createdBy: string | null;
    groupName: string;
    id: string;
    url: string;
    urlType: string;
    repositoryUrls: IEnvironmentGitRootData[];
    eventStatus: string | undefined;
    eventId: string | undefined;
    eventEnvironment: string | undefined;
  };
}
const SCOPE_PATH_LENGTH = 6;
const EnvironmentStatusColum: React.FC<IEnvironmentStatusColumProps> = ({
  details,
}): React.JSX.Element => {
  const { t } = useTranslation();

  const excludeTag = details.repositoryUrls.every(
    (envRootData): boolean => !envRootData.include,
  );

  const defaultTag: React.JSX.Element | undefined = excludeTag ? undefined : (
    <Tag
      icon={"circle-check"}
      priority={"low"}
      tagLabel={t("group.scope.git.repo.status.value.included")}
      variant={"success"}
    />
  );
  const currentUrl: string = window.location.href;
  const modifiedUrl: string = currentUrl.endsWith("/scope")
    ? currentUrl.slice(0, -SCOPE_PATH_LENGTH)
    : currentUrl;

  return includeTagsFormatter({
    customTag:
      details.eventId === undefined ? (
        defaultTag
      ) : (
        <Tag
          href={`${modifiedUrl}/events/${details.eventId}/description`}
          icon={"hexagon-exclamation"}
          priority={"medium"}
          tagLabel={t("group.scope.git.repo.status.value.events")}
          variant={"error"}
        />
      ),
    excludeTag,
    text: undefined,
  });
};

const formatEnvironmentStatusColumn = (
  cell: CellContext<
    {
      countSecrets?: number;
      cloudName: string | null;
      createdAt: string | null;
      createdBy: string | null;
      groupName: string;
      id: string;
      url: string;
      urlType: string;
      useEgress: boolean;
      useVpn: boolean;
      useZtna: boolean;
      repositoryUrls: IEnvironmentGitRootData[];
      eventStatus: string | undefined;
      eventId: string | undefined;
      eventEnvironment: string | undefined;
    },
    unknown
  >,
): React.JSX.Element => {
  const details = cell.row.original;

  return <EnvironmentStatusColum details={details} />;
};

export { formatEnvironmentStatusColumn };
