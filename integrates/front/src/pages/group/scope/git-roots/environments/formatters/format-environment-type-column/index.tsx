import { CloudImage, Container, Text } from "@fluidattacks/design";
import type { CellContext } from "@tanstack/react-table";
import * as React from "react";
import { useTheme } from "styled-components";

import type { IEnvironmentGitRootData } from "pages/group/scope/types";

interface IEnvironmentTypeColumProps {
  readonly details: {
    cloudName: string | null;
    createdAt: string | null;
    createdBy: string | null;
    groupName: string;
    id: string;
    url: string;
    urlType: string;
    repositoryUrls: IEnvironmentGitRootData[];
    eventStatus: string | undefined;
    eventId: string | undefined;
    eventEnvironment: string | undefined;
  };
}
const AWS_HEIGHT = 16;
const DEFAULT_HEIGHT = 24;
const EnvironmentTypeColum: React.FC<IEnvironmentTypeColumProps> = ({
  details,
}): React.JSX.Element => {
  const theme = useTheme();
  const getCloudPublicId = (cloudName: string | null): string => {
    switch (cloudName) {
      case "GCP":
        return "gcp";
      case "AZURE":
        return "azure";
      case null:
      default:
        return "aws";
    }
  };

  return (
    <Container alignItems={"center"} display={"flex"} gap={0.5}>
      <Text color={theme.palette.gray["800"]} size={"sm"}>
        {details.urlType}
      </Text>
      {details.urlType === "CSPM" ? (
        <React.Fragment>
          {" - "}
          <CloudImage
            height={details.cloudName === "AWS" ? AWS_HEIGHT : DEFAULT_HEIGHT}
            publicId={`integrates/resources/${getCloudPublicId(details.cloudName)}`}
            width={24}
          />
        </React.Fragment>
      ) : undefined}
    </Container>
  );
};

const formatEnvironmentTypeColumn = (
  cell: CellContext<
    {
      countSecrets?: number;
      cloudName: string | null;
      createdAt: string | null;
      createdBy: string | null;
      groupName: string;
      id: string;
      url: string;
      urlType: string;
      useEgress: boolean;
      useVpn: boolean;
      useZtna: boolean;
      repositoryUrls: IEnvironmentGitRootData[];
      eventStatus: string | undefined;
      eventId: string | undefined;
      eventEnvironment: string | undefined;
    },
    unknown
  >,
): React.JSX.Element => {
  const details = cell.row.original;

  return <EnvironmentTypeColum details={details} />;
};

export { formatEnvironmentTypeColumn };
