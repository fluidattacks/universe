import { PureAbility } from "@casl/ability";
import { screen, waitFor, within } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import type { StrictResponse } from "msw";
import { HttpResponse, graphql } from "msw";
import * as React from "react";

import { Environments } from ".";
import { GET_ENVIRONMENT_URL } from "../../queries";
import type { IEnvironmentUrlData, IEventsData } from "../../types";
import { authzPermissionsContext } from "context/authz/config";
import type { GetEnvironmentUrlQuery as GetEnvironmentUrl } from "gql/graphql";
import { useTable } from "hooks";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";

interface ITestComponentProps {
  readonly eventsDataSet: IEventsData[];
  readonly envUrlsDataSet: IEnvironmentUrlData[];
  readonly groupName: string;
}

const TestComponent: React.FC<ITestComponentProps> = ({
  envUrlsDataSet,
  eventsDataSet,
  groupName,
}): JSX.Element => {
  const tableRef = useTable("testTable");

  return (
    <Environments
      envUrlsDataSet={envUrlsDataSet}
      eventsDataSet={eventsDataSet}
      groupName={groupName}
      tableRef={tableRef}
    />
  );
};
const graphqlMocked = graphql.link(LINK);

describe("environments", (): void => {
  // eslint-disable-next-line jest/no-hooks
  beforeEach((): void => {
    jest.spyOn(console, "warn").mockImplementation();
  });

  it("should render table", (): void => {
    expect.hasAssertions();

    const eventsDataSet: IEventsData[] = [
      {
        closingDate: "-",
        detail: "Integrates unit test",
        environment: null,
        eventDate: "2018-06-27 07:00:00",
        eventStatus: "CREATED",
        eventType: "OTHER",
        groupName: "unittesting",
        id: "418900971",
        root: null,
      },
    ];

    const envUrlsDataSet: IEnvironmentUrlData[] = [
      {
        cloudName: null,
        createdAt: null,
        createdBy: null,
        id: "00fbe3579a253b43239954a545dc0536e6c83094",
        repositoryUrls: [
          {
            countSecrets: 0,
            include: true,
            root: {
              id: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
              state: "ACTIVE",
              url: "https://gitlab.com/fluidattacks/universe.git",
            },
          },
        ],
        url: "https://app.fluidattacks.com/test",
        urlType: "URL",
        useEgress: false,
        useVpn: false,
        useZtna: false,
      },
      {
        cloudName: null,
        createdAt: null,
        createdBy: null,
        id: "ba1598f6cf5e8512cd7d776d5dd8a5f79a4fed39",
        repositoryUrls: [
          {
            countSecrets: 0,
            include: true,
            root: {
              id: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
              state: "ACTIVE",
              url: "https://gitlab.com/fluidattacks/universe.git",
            },
          },
        ],
        url: "https://test.com",
        urlType: "URL",
        useEgress: false,
        useVpn: false,
        useZtna: false,
      },
    ];

    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            {
              action: "integrates_api_resolvers_query_environment_url_resolve",
            },
            {
              action:
                "integrates_api_resolvers_git_environment_url_secrets_resolve",
            },
          ])
        }
      >
        <TestComponent
          envUrlsDataSet={envUrlsDataSet}
          eventsDataSet={eventsDataSet}
          groupName={"unittesting"}
        />
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/unittesting"],
        },
      },
    );

    expect(screen.getAllByRole("row")[1].parentElement).toMatchSnapshot();
  });

  it("should update connection columns", async (): Promise<void> => {
    expect.hasAssertions();

    const eventsDataSet: IEventsData[] = [
      {
        closingDate: "-",
        detail: "Integrates unit test",
        environment: null,
        eventDate: "2018-06-27 07:00:00",
        eventStatus: "CREATED",
        eventType: "OTHER",
        groupName: "unittesting",
        id: "418900971",
        root: null,
      },
    ];

    const envUrlsDataSet: IEnvironmentUrlData[] = [
      {
        cloudName: null,
        createdAt: null,
        createdBy: null,
        id: "00fbe3579a253b43239954a545dc0536e6c83094",
        repositoryUrls: [
          {
            countSecrets: 0,
            include: true,
            root: {
              id: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
              state: "ACTIVE",
              url: "https://gitlab.com/fluidattacks/universe.git",
            },
          },
        ],
        url: "https://app.fluidattacks.com/test",
        urlType: "URL",
        useEgress: true,
        useVpn: false,
        useZtna: false,
      },
      {
        cloudName: null,
        createdAt: null,
        createdBy: null,
        id: "ba1598f6cf5e8512cd7d776d5dd8a5f79a4fed39",
        repositoryUrls: [
          {
            countSecrets: 0,
            include: true,
            root: {
              id: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
              state: "ACTIVE",
              url: "https://gitlab.com/fluidattacks/universe.git",
            },
          },
        ],
        url: "https://test.com",
        urlType: "URL",
        useEgress: false,
        useVpn: false,
        useZtna: false,
      },
    ];

    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            {
              action: "integrates_api_resolvers_query_environment_url_resolve",
            },
            {
              action:
                "integrates_api_resolvers_git_environment_url_secrets_resolve",
            },
          ])
        }
      >
        <TestComponent
          envUrlsDataSet={envUrlsDataSet}
          eventsDataSet={eventsDataSet}
          groupName={"unittesting"}
        />
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/unittesting"],
        },
      },
    );

    await waitFor((): void => {
      const connectionTypeCells = screen.getAllByText("Egress");

      expect(connectionTypeCells.length).toBeGreaterThan(0);
    });

    await waitFor((): void => {
      const connectionTypeCells = screen.getAllByText("N/A");

      expect(connectionTypeCells.length).toBeGreaterThan(0);
    });
  });

  it("should render environment description", async (): Promise<void> => {
    expect.hasAssertions();

    const envUrlsDataSet: IEnvironmentUrlData[] = [
      {
        cloudName: null,
        createdAt: null,
        createdBy: null,
        id: "00fbe3579a253b43239954a545dc0536e6c83094",
        repositoryUrls: [
          {
            countSecrets: 0,
            include: true,
            root: {
              id: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
              state: "ACTIVE",
              url: "https://gitlab.com/fluidattacks/universe.git",
            },
          },
        ],
        url: "https://app.fluidattacks.com/test",
        urlType: "URL",
        useEgress: false,
        useVpn: false,
        useZtna: false,
      },
      {
        cloudName: null,
        createdAt: null,
        createdBy: null,
        id: "ba1598f6cf5e8512cd7d776d5dd8a5f79a4fed39",
        repositoryUrls: [
          {
            countSecrets: 0,
            include: true,
            root: {
              id: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
              state: "ACTIVE",
              url: "https://gitlab.com/fluidattacks/universe.git",
            },
          },
        ],
        url: "https://test.com",
        urlType: "URL",
        useEgress: false,
        useVpn: false,
        useZtna: false,
      },
    ];
    const eventsDataSet: IEventsData[] = [
      {
        closingDate: "-",
        detail: "Integrates unit test",
        environment: null,
        eventDate: "2018-06-27 07:00:00",
        eventStatus: "CREATED",
        eventType: "OTHER",
        groupName: "unittesting",
        id: "418900971",
        root: null,
      },
    ];

    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            {
              action: "integrates_api_resolvers_query_environment_url_resolve",
            },
            {
              action:
                "integrates_api_resolvers_git_environment_url_secrets_resolve",
            },
          ])
        }
      >
        <TestComponent
          envUrlsDataSet={envUrlsDataSet}
          eventsDataSet={eventsDataSet}
          groupName={"unittesting"}
        />
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/unittesting"],
        },
      },
    );
    await waitFor((): void => {
      expect(
        screen.getByText(/https:\/\/app\.fluidattacks\.com\/test/iu),
      ).toBeInTheDocument();
    });

    const cell = screen.getByRole("cell", {
      name: /https:\/\/app\.fluidattacks\.com\/test/iu,
    });

    const arrow = within(cell).getByRole("button");

    expect(
      screen.queryByRole("link", {
        name: "https://app.fluidattacks.com/test",
      }),
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText(/group\.scope\.git\.createdAt/iu),
    ).not.toBeInTheDocument();

    await userEvent.click(arrow);

    expect(
      screen.getByRole("link", { name: "https://app.fluidattacks.com/test" }),
    ).toBeInTheDocument();
    expect(
      screen.getByText(/group\.scope\.git\.createdAt/iu),
    ).toBeInTheDocument();
  });

  it("should open secrets modal", async (): Promise<void> => {
    expect.hasAssertions();

    const eventsDataSet: IEventsData[] = [
      {
        closingDate: "-",
        detail: "Integrates unit test",
        environment: null,
        eventDate: "2018-06-27 07:00:00",
        eventStatus: "CREATED",
        eventType: "OTHER",
        groupName: "unittesting",
        id: "418900971",
        root: null,
      },
    ];
    const envUrlsDataSet: IEnvironmentUrlData[] = [
      {
        cloudName: null,
        createdAt: null,
        createdBy: null,
        id: "00fbe3579a253b43239954a545dc0536e6c83094",
        repositoryUrls: [
          {
            countSecrets: 1,
            include: true,
            root: {
              id: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
              state: "ACTIVE",
              url: "https://gitlab.com/fluidattacks/universe.git",
            },
          },
        ],
        url: "https://app.fluidattacks.com/test",
        urlType: "URL",
        useEgress: false,
        useVpn: false,
        useZtna: false,
      },
      {
        cloudName: null,
        createdAt: null,
        createdBy: null,
        id: "ba1598f6cf5e8512cd7d776d5dd8a5f79a4fed39",
        repositoryUrls: [
          {
            countSecrets: 0,
            include: true,
            root: {
              id: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
              state: "ACTIVE",
              url: "https://gitlab.com/fluidattacks/universe.git",
            },
          },
        ],
        url: "https://test.com",
        urlType: "URL",
        useEgress: false,
        useVpn: false,
        useZtna: false,
      },
    ];
    const mockedEnvUrl = graphqlMocked.query(
      GET_ENVIRONMENT_URL,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: GetEnvironmentUrl }> => {
        const { groupName, urlId } = variables;
        if (
          groupName === "unittesting" &&
          urlId === "00fbe3579a253b43239954a545dc0536e6c83094"
        ) {
          return HttpResponse.json({
            data: {
              environmentUrl: {
                __typename: "GitEnvironmentUrl",
                createdAt: null,
                id: "00fbe3579a253b43239954a545dc0536e6c83094",
                rootId: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
                secrets: [
                  {
                    __typename: "Secret",
                    description: "Environment secret description",
                    key: "Key value",
                    owner: "jdoe@testcompany.com",
                  },
                ],
                url: "https://app.fluidattacks.com/test",
              },
            },
          });
        }
        if (
          groupName === "unittesting" &&
          urlId === "ba1598f6cf5e8512cd7d776d5dd8a5f79a4fed39"
        ) {
          return HttpResponse.json({
            data: {
              environmentUrl: {
                __typename: "GitEnvironmentUrl",
                createdAt: null,
                id: "ba1598f6cf5e8512cd7d776d5dd8a5f79a4fed39",
                rootId: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
                secrets: [],
                url: "https://test.com",
              },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error getting root environment urls")],
        });
      },
    );
    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            {
              action: "integrates_api_resolvers_query_environment_url_resolve",
            },
            {
              action:
                "integrates_api_resolvers_git_environment_url_secrets_resolve",
            },
          ])
        }
      >
        <TestComponent
          envUrlsDataSet={envUrlsDataSet}
          eventsDataSet={eventsDataSet}
          groupName={"unittesting"}
        />
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/unittesting"],
        },
        mocks: [mockedEnvUrl],
      },
    );

    await userEvent.click(
      screen.getByRole("cell", {
        name: /https:\/\/app\.fluidattacks\.com\/test/iu,
      }),
    );

    await waitFor((): void => {
      expect(screen.getByText("Secrets")).toBeInTheDocument();
    });

    expect(
      screen.getByRole("row", {
        name: /group.scope.git.repo.credentials.secrets.key group.scope.git.repo.credentials.secrets.description group.scope.git.repo.credentials.secrets.owner/iu,
      }),
    ).toBeInTheDocument();
    expect(
      screen.getByRole("row", {
        name: /Key value Environment secret description jdoe@testcompany.com/iu,
      }),
    ).toBeInTheDocument();

    await userEvent.click(screen.getByRole("button", { name: "Cancel" }));

    await userEvent.click(
      screen.getByRole("cell", { name: /https:\/\/test\.com/iu }),
    );

    expect(
      screen.getByRole("row", { name: /table.noDataIndication/iu }),
    ).toBeInTheDocument();
  });
});
