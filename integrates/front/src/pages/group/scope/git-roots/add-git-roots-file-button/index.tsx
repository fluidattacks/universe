import { Button, useModal } from "@fluidattacks/design";
import { useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { AddGitRootFileModal } from "./modal";

const GitRootAddFileButton: React.FC = (): JSX.Element => {
  const { t } = useTranslation();
  const modal = useModal("add-git-root-file-modal");

  const onClick = useCallback((): void => {
    modal.open();
  }, [modal]);

  return (
    <React.Fragment>
      <Button
        icon={"file-import"}
        id={"git-root-upload-file"}
        onClick={onClick}
        tooltip={t("group.scope.git.addGitRootFile.tooltip")}
        tooltipPlace={"top"}
        variant={"ghost"}
      >
        {t("group.scope.git.addGitRootFile.button")}
      </Button>
      <AddGitRootFileModal modalRef={modal} />
    </React.Fragment>
  );
};

export { GitRootAddFileButton };
