import type { CellContext } from "@tanstack/react-table";
import * as React from "react";

import { CodeCell } from "../../styles";
import type { ILineError } from "../../types";

const LineCell = (props: CellContext<ILineError, unknown>): React.ReactNode => {
  const { row, getValue } = props;
  const fieldIndex = row.getValue("fieldIndex");
  const items = [...getValue<string>().split(",")];
  const lastItem = items.at(-1);

  return (
    <CodeCell>
      {items
        .slice(0, -1)
        .map((item, index): React.ReactNode | string =>
          index + 1 === fieldIndex ? (
            <b key={item}>{`${item},`}</b>
          ) : (
            `${item},`
          ),
        )}
      {items.length === fieldIndex ? <b>{lastItem}</b> : lastItem}
    </CodeCell>
  );
};

export { LineCell };
