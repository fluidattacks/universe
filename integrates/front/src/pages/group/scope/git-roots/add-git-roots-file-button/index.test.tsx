import { fireEvent, screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { graphql } from "msw";

import { GitRootAddFileButton } from ".";
import { UPLOAD_GIT_ROOT_FILE } from "../../queries";
import { LINK } from "mocks/constants";
import { createGraphQLMocks, render } from "mocks/index";

describe("add git root action", (): void => {
  const graphqlMocked = graphql.link(LINK);

  const mocks = createGraphQLMocks(graphqlMocked, [
    [
      UPLOAD_GIT_ROOT_FILE,
      "mutation",
      {
        uploadGitRootFile: {
          errorLines: [],
          message: "Roots created: 1",
          success: true,
          totalErrors: 0,
          totalSuccess: 1,
        },
      },
    ],
  ]);

  it("should show the modal when button is clicked", async (): Promise<void> => {
    expect.hasAssertions();

    render(<GitRootAddFileButton />, { mocks });

    expect(screen.getByRole("button")).toBeVisible();

    await userEvent.click(screen.getByRole("button"));

    expect(
      screen.getByText("group.scope.git.addGitRootFile.modal.title"),
    ).toBeVisible();
    expect(
      screen.getByRole("link", { name: /See more information/u }),
    ).toBeVisible();
    expect(
      screen.getByRole("link", { name: /example CSV import file/u }),
    ).toBeVisible();
    expect(screen.getByTestId("file")).toBeInTheDocument();
    expect(
      screen.getByRole("button", { name: /addGitRootFile.modal.button/u }),
    ).not.toBeEnabled();
  });

  it("should submit a file", async (): Promise<void> => {
    expect.hasAssertions();

    // [object ReadableStream]
    jest.spyOn(console, "warn").mockImplementation();
    const file = new File(["okada-test.csv"], "okada-test.csv", {
      type: "text/csv",
    });

    render(<GitRootAddFileButton />, { mocks });

    await userEvent.click(screen.getByRole("button"));

    fireEvent.change(screen.getByTestId("file"), { target: { files: [file] } });

    expect(
      screen.getByRole("button", { name: /addGitRootFile.modal.button/u }),
    ).toBeEnabled();

    await userEvent.click(
      screen.getByRole("button", { name: /addGitRootFile.modal.button/u }),
    );

    await waitFor((): void => {
      expect(screen.getByText("Uploading...")).toBeVisible();
    });
  });
});
