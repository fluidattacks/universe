import { styled } from "styled-components";

const CodeCell = styled.pre`
  padding: 0.5rem 1rem;
  font-size: ${({ theme }): string => theme.typography.text.xs};

  b {
    color: ${({ theme }): string => theme.palette.primary[400]};
  }
`;

export { CodeCell };
