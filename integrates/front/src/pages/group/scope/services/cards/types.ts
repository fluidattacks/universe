import type { IFormData } from "../types";

interface IInfoCardProps {
  handleEssentialBtnChange: (
    event: React.ChangeEvent<HTMLInputElement>,
  ) => void;
  handleAdvancedBtnChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  values: IFormData;
}

export type { IInfoCardProps };
