import { Link } from "@fluidattacks/design";
import type { CellContext } from "@tanstack/react-table";
import * as React from "react";

import type { TRoot } from "../../hooks";

interface IPackagesColumnProps {
  readonly details: TRoot;
}

const PackagesColumn: React.FC<IPackagesColumnProps> = ({
  details,
}): React.JSX.Element => {
  return (
    <Link href={`./${details.id}`}>
      {`View packages (${details.toePackages.total})`}
    </Link>
  );
};

const formatPackagesColumn = (
  cell: CellContext<TRoot, unknown>,
): React.JSX.Element => {
  const details = cell.row.original;

  return <PackagesColumn details={details} />;
};

export { formatPackagesColumn };
