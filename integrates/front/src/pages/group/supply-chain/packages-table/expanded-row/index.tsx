import { Container, Link, Text } from "@fluidattacks/design";
import type { Row } from "@tanstack/react-table";
import mixpanel from "mixpanel-browser";
import { useCallback } from "react";
import * as React from "react";

import type { TPackage } from "../../hooks";
import { CopyButton } from "components/copy-button";
import { useAudit } from "hooks/use-audit";

interface IExpandedRowProps {
  readonly details: TPackage;
}

const ExpandedRow: React.FC<IExpandedRowProps> = ({
  details,
}): React.JSX.Element => {
  const { addAuditEvent } = useAudit();
  const handleReachableClick = useCallback((): void => {
    mixpanel.track("OpenPackageRelatedVulnerabilitiesLink");
    addAuditEvent("PackageRelatedVulnerabilitiesLink", details.name);
  }, [addAuditEvent, details]);

  return (
    <Container display={"flex"} flexDirection={"column"} gap={0.25}>
      <Text fontWeight={"bold"} size={"sm"}>
        {"Locations"}
      </Text>
      {details.locations?.map((location): React.JSX.Element => {
        const MAX_LAYER_LENGTH = 17;
        const layerInfo =
          location.layer !== null && location.layer.trim() !== ""
            ? ` (${location.layer.slice(0, MAX_LAYER_LENGTH)})`
            : "";

        const lineInfo =
          location.line !== null && location.line.trim() !== ""
            ? `:${location.line}`
            : "";

        const content =
          location.layer === null
            ? `${location.path}${lineInfo}`
            : `${location.imageRef}${layerInfo} > ${location.path}${lineInfo}`;

        return (
          <Container
            alignItems={"center"}
            display={"flex"}
            gap={0.25}
            key={JSON.stringify(location)}
          >
            <CopyButton content={content} />
            <Text size={"sm"}>{content}</Text>
          </Container>
        );
      })}
      {details.vulnerabilityInfo && details.vulnerabilityInfo.length > 0 ? (
        <div>
          <Text fontWeight={"bold"} size={"sm"}>
            {"Related Vulnerabilities"}
          </Text>
          <ul>
            {details.vulnerabilityInfo.map((vulnInfo): React.JSX.Element => {
              return (
                <Container
                  alignItems={"center"}
                  display={"flex"}
                  gap={0.25}
                  key={vulnInfo.id}
                >
                  <li>
                    <Link
                      href={vulnInfo.id}
                      key={vulnInfo.id}
                      onClick={handleReachableClick}
                    >
                      {vulnInfo.id}
                    </Link>
                  </li>
                </Container>
              );
            })}
          </ul>
        </div>
      ) : null}
    </Container>
  );
};

export const renderExpandedRow = (row: Row<TPackage>): JSX.Element => {
  const details = row.original;

  return <ExpandedRow details={details} />;
};
