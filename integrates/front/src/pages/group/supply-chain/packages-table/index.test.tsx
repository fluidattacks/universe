import { PureAbility } from "@casl/ability";
import { CustomThemeProvider as DesignThemeProvider } from "@fluidattacks/design";
import { act, screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import mixpanel from "mixpanel-browser";
import { Route, Routes } from "react-router-dom";

import { PackagesTable } from ".";
import type { TPackage } from "../hooks";
import { CustomThemeProvider } from "components/colors";
import { authzPermissionsContext } from "context/authz/config";
import { render } from "mocks";

describe("packagesTable", (): void => {
  const packages: TPackage[] = [
    {
      advisories: [
        {
          __typename: "ToePackageAdvisory",
          epss: 0.54019,
          id: "MAL-2022-0001",
        },
        {
          __typename: "ToePackageAdvisory",
          epss: 0.84019,
          id: "CVE-2022-0001",
        },
      ],
      healthMetadata: {
        authors: null,
        latestVersion: "8.3",
        latestVersionCreatedAt: null,
      },
      locations: [
        {
          dependencyType: null,
          imageRef: null,
          layer: null,
          line: "80",
          path: "test/path/file.java",
          scope: "PROD",
        },
        {
          dependencyType: null,
          imageRef: "docker.io/image:latest",
          layer: "sha256:e0a9f5911802534ba097660206feabeb0247a81e409029167b30e",
          line: null,
          path: "var/lib/dpkg/status",
          scope: "PROD",
        },
      ],
      name: "com.nimbusds:nimbus-jose-jwt",
      outdated: false,
      platform: "MAVEN",
      root: {
        id: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
        nickname: "universe",
      },
      version: "8.3",
      vulnerabilityInfo: [
        {
          __typename: "ToePackageVulnerabilityInfo",
          cve: ["CVE-2023-1234"],
          id: "vuln-1",
          severityScore: 7.5,
        },
      ],
      vulnerable: true,
    },
    {
      advisories: [
        {
          __typename: "ToePackageAdvisory",
          epss: 0.32054,
          id: "CVE-2022-0004",
        },
      ],
      healthMetadata: {
        authors: null,
        latestVersion: "2.8",
        latestVersionCreatedAt: null,
      },
      locations: [
        {
          dependencyType: null,
          imageRef: null,
          layer: null,
          line: "81",
          path: "test/path/file.java",
          scope: "PROD",
        },
      ],
      name: "commons-io:commons-io",
      outdated: true,
      platform: "MAVEN",
      root: {
        id: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
        nickname: "universe",
      },
      version: "2.7",
      vulnerabilityInfo: [],
      vulnerable: false,
    },
    {
      advisories: [],
      healthMetadata: {
        authors: null,
        latestVersion: "1.5",
        latestVersionCreatedAt: null,
      },
      locations: [
        {
          dependencyType: null,
          imageRef: null,
          layer: null,
          line: "82",
          path: "test/path/file.java",
          scope: "PROD",
        },
      ],
      name: "javax.mail:mail",
      outdated: true,
      platform: "MAVEN",
      root: {
        id: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
        nickname: "universe",
      },
      version: "1.4",
      vulnerabilityInfo: [],
      vulnerable: true,
    },
    {
      advisories: [],
      healthMetadata: null,
      locations: [
        {
          dependencyType: null,
          imageRef: null,
          layer: null,
          line: "83",
          path: "test/path/file.java",
          scope: "PROD",
        },
      ],
      name: "org.json:json",
      outdated: null,
      platform: "MAVEN",
      root: {
        id: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
        nickname: "universe",
      },
      version: "20190722",
      vulnerabilityInfo: [],
      vulnerable: false,
    },
  ];

  it("should display table", async (): Promise<void> => {
    expect.hasAssertions();

    jest.spyOn(mixpanel, "track").mockImplementation(jest.fn());

    const mockedPermissions = new PureAbility<string>([]);

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <CustomThemeProvider>
          <DesignThemeProvider>
            <Routes>
              <Route
                element={
                  <PackagesTable
                    localStorageKey={""}
                    onFilter={jest.fn()}
                    onRequestSbom={jest.fn()}
                    onSearch={jest.fn()}
                    options={[]}
                    packagesQuery={{
                      fetchNextPage: jest.fn(),
                      hasNextPage: false,
                      loading: false,
                      packages,
                      total: packages.length,
                    }}
                  />
                }
                path={"/:groupName/supply-chain"}
              />
            </Routes>
          </DesignThemeProvider>
        </CustomThemeProvider>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/unittesting/supply-chain"],
        },
      },
    );
    await waitFor((): void => {
      expect(screen.queryAllByText("universe")[0]).toBeInTheDocument();
    });

    expect(screen.queryByRole("table")).toBeInTheDocument();

    await userEvent.click(
      screen.getByRole("button", {
        name: "group.findings.tableSet.btn.text (8 / 8)",
      }),
    );

    await waitFor((): void => {
      expect(screen.queryAllByText("Manage columns")[0]).toBeInTheDocument();
    });

    await userEvent.click(document.querySelectorAll("#modal-close")[0]);

    expect(screen.getAllByRole("row")[0].textContent).toStrictEqual(
      [
        "Dependency",
        "Root",
        "group.toe.packages.currentVersion",
        "group.toe.packages.status",
        "% EPSS",
        "group.toe.packages.latestVersion",
        "group.toe.packages.lastPublished",
        "Details",
      ].join(""),
    );
    expect(screen.getAllByRole("row")[1].textContent).toStrictEqual(
      [
        "com.nimbusds:nimbus-jose-jwt",
        "universe2 locs",
        "8.3",
        ["Reachable", "Malware", "Vulnerable", "Updated"].join(""),
        "84.02%",
        "8.3",
        "-",
        "View details",
      ].join(""),
    );

    const [expandCaret] = Array.from(
      screen.getAllByRole("row")[1].getElementsByClassName("fa-angle-down"),
    );
    await userEvent.click(expandCaret);

    expect(screen.getAllByRole("row")[2].textContent).toBe(
      [
        "Locations",
        "test/path/file.java:80",
        "docker.io/image:latest (sha256:e0a9f59118) > var/lib/dpkg/status",
        "Related Vulnerabilities",
        "vuln-1",
      ].join(""),
    );

    const user = userEvent.setup();
    await user.click(screen.getAllByTestId("copy-icon")[0]);

    const vulnLink = screen.getByRole("link", { name: /vuln-1/iu });
    act((): void => {
      vulnLink.addEventListener("click", (ev): void => {
        ev.preventDefault();
      });
    });
    await userEvent.click(vulnLink);

    expect(mixpanel.track).toHaveBeenCalledWith(
      "OpenPackageRelatedVulnerabilitiesLink",
    );

    await expect(window.navigator.clipboard.readText()).resolves.toBe(
      "test/path/file.java:80",
    );

    const [collapseCaret] = Array.from(
      screen.getAllByRole("row")[1].getElementsByClassName("fa-angle-up"),
    );
    await userEvent.click(collapseCaret);

    expect(screen.getAllByRole("row")[2].textContent).toStrictEqual(
      [
        "commons-io:commons-io",
        "universe1 locs",
        "2.7",
        "Outdated",
        "32.05%",
        "2.8",
        "-",
        "None",
      ].join(""),
    );

    expect(screen.getAllByRole("row")[3].textContent).toStrictEqual(
      [
        "javax.mail:mail",
        "universe1 locs",
        "1.4",
        ["Vulnerable", "Outdated"].join(""),
        "0.0%",
        "1.5",
        "-",
        "View details",
      ].join(""),
    );

    jest.clearAllMocks();
  });

  it("should render request sbom modal", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_request_sbom_file_mutate" },
    ]);
    const requestSbom = jest.fn();
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <CustomThemeProvider>
          <Routes>
            <Route
              element={
                <PackagesTable
                  localStorageKey={""}
                  onFilter={jest.fn()}
                  onRequestSbom={requestSbom}
                  onSearch={jest.fn()}
                  options={[]}
                  packagesQuery={{
                    fetchNextPage: jest.fn(),
                    hasNextPage: false,
                    loading: false,
                    packages,
                    total: packages.length,
                  }}
                />
              }
              path={"/:groupName/supply-chain"}
            />
          </Routes>
        </CustomThemeProvider>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/unittesting/supply-chain"],
        },
      },
    );
    await waitFor((): void => {
      expect(
        screen.queryByText("group.toe.packages.btn.text"),
      ).toBeInTheDocument();
    });
    await userEvent.click(screen.getByText("group.toe.packages.btn.text"));

    expect(requestSbom).toHaveBeenCalledTimes(1);

    jest.clearAllMocks();
  });
});
