import { Container, Tag } from "@fluidattacks/design";
import type { CellContext } from "@tanstack/react-table";
import * as React from "react";

import type { TPackage } from "../../../hooks";

interface IRootColumnProps {
  readonly details: TPackage;
}

const RootColumn: React.FC<IRootColumnProps> = ({
  details,
}): React.JSX.Element => {
  return (
    <Container
      alignItems={"center"}
      display={"flex"}
      gap={0.25}
      justify={"space-between"}
      width={"100%"}
    >
      {details.root.nickname}
      <Tag tagLabel={`${details.locations?.length} locs`} variant={"default"} />
    </Container>
  );
};

const formatRootColumn = (
  cell: CellContext<TPackage, unknown>,
): React.JSX.Element => {
  const details = cell.row.original;

  return <RootColumn details={details} />;
};

export { formatRootColumn };
