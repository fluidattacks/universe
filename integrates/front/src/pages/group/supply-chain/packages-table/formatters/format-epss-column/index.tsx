import { Container } from "@fluidattacks/design";
import type { CellContext } from "@tanstack/react-table";
import * as React from "react";

import type { TPackage } from "../../../hooks";

interface IEPSSColumnProps {
  readonly details: TPackage;
}

const EPSSColumn: React.FC<IEPSSColumnProps> = ({
  details,
}): React.JSX.Element => {
  const maxEpss =
    details.advisories?.reduce<number>((max, advisory): number => {
      if (advisory.epss !== null && advisory.epss > max) {
        return advisory.epss;
      }

      return max;
    }, 0) ?? 0;
  const EPSS_MULTIPLIER = 100;
  const displayEPSS = maxEpss
    ? `${(maxEpss * EPSS_MULTIPLIER).toFixed(2)}%`
    : "0.0%";

  return (
    <Container
      alignItems={"center"}
      display={"flex"}
      gap={0.25}
      justify={"space-between"}
      width={"100%"}
    >
      {displayEPSS}
    </Container>
  );
};

const formatEPSSColumn = (
  cell: CellContext<TPackage, unknown>,
): React.JSX.Element => {
  const details = cell.row.original;

  return <EPSSColumn details={details} />;
};

export { formatEPSSColumn };
