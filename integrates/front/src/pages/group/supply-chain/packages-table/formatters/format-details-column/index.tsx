import { Link, Text } from "@fluidattacks/design";
import type { CellContext } from "@tanstack/react-table";
import * as React from "react";
import { useParams } from "react-router-dom";
import { useTheme } from "styled-components";

import type { TPackage } from "../../../hooks";

interface IDetailsColumnProps {
  readonly details: TPackage;
}

const DetailsColumn: React.FC<IDetailsColumnProps> = ({
  details,
}): React.JSX.Element => {
  const { groupName, organizationName } = useParams() as Record<string, string>;
  const baseUrl = `/orgs/${organizationName}/groups/${groupName}/surface/packages`;

  const theme = useTheme();
  const packageId = encodeURIComponent(`${details.name}@${details.version}`);

  if (details.vulnerable === true) {
    return (
      <Link href={`${baseUrl}/roots/${details.root.id}/${packageId}`}>
        {`View details`}
      </Link>
    );
  }

  return (
    <Text color={theme.palette.gray[400]} size={"sm"}>
      {"None"}
    </Text>
  );
};

const formatDetailsColumn = (
  cell: CellContext<TPackage, unknown>,
): React.JSX.Element => {
  const details = cell.row.original;

  return <DetailsColumn details={details} />;
};

export { formatDetailsColumn };
