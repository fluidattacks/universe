import { Container, Tag } from "@fluidattacks/design";
import type { CellContext } from "@tanstack/react-table";
import * as React from "react";

import type { TPackage } from "../../../hooks";

const formatStatusColumn = (
  cell: CellContext<TPackage, unknown>,
): React.JSX.Element => {
  const details = cell.row.original;

  const hasMalwareAdvisory =
    details.advisories?.some((advisory): boolean =>
      advisory.id.startsWith("MAL-"),
    ) ?? false;

  return (
    <Container alignItems={"center"} display={"flex"} gap={0.25} wrap={"wrap"}>
      {details.vulnerabilityInfo && details.vulnerabilityInfo.length > 0 ? (
        <Tag
          icon={"triangle-exclamation"}
          iconColor={"#FFFFFF"}
          priority={"high"}
          tagLabel={"Reachable"}
          variant={"error"}
        />
      ) : undefined}
      {hasMalwareAdvisory ? (
        <Tag
          icon={"bug"}
          iconColor={"#FEF3F2"}
          priority={"medium"}
          tagLabel={"Malware"}
          variant={"error"}
        />
      ) : undefined}
      {details.vulnerable === true ? (
        <Tag
          icon={"flag"}
          priority={"low"}
          tagLabel={"Vulnerable"}
          variant={"error"}
        />
      ) : undefined}
      {details.outdated === true ? (
        <Tag
          icon={"calendar-xmark"}
          priority={"low"}
          tagLabel={"Outdated"}
          variant={"warning"}
        />
      ) : undefined}
      {details.outdated === false ? (
        <Tag
          icon={"calendar-check"}
          priority={"low"}
          tagLabel={"Updated"}
          variant={"success"}
        />
      ) : undefined}
    </Container>
  );
};

export { formatStatusColumn };
