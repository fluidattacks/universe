import { CloudImage, Container, Text } from "@fluidattacks/design";
import type { CellContext } from "@tanstack/react-table";
import * as React from "react";
import { useTheme } from "styled-components";

import type { TPackage } from "../../../hooks";

interface IDependencyColumnProps {
  readonly details: TPackage;
}

const DependencyColumn: React.FC<IDependencyColumnProps> = ({
  details,
}): React.JSX.Element => {
  const theme = useTheme();

  return (
    <Container alignItems={"center"} display={"flex"} gap={0.5}>
      <CloudImage
        height={24}
        publicId={`integrates/supply-chain/logo-${details.platform}`}
        width={24}
      />
      <Text color={theme.palette.gray["800"]} size={"sm"}>
        {details.name}
      </Text>
    </Container>
  );
};

const formatDependencyColumn = (
  cell: CellContext<TPackage, unknown>,
): React.JSX.Element => {
  const details = cell.row.original;

  return <DependencyColumn details={details} />;
};

export { formatDependencyColumn };
