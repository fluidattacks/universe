import { Container } from "@fluidattacks/design";
import * as React from "react";
import { useParams } from "react-router-dom";

import { usePackagesQuery } from "./hooks";

import { useFilters } from "../hooks";
import { PackagesTable } from "../packages-table";
import { useSearch } from "components/search/hooks";
import { SectionHeader } from "components/section-header";

const RootPackages: React.FC = (): React.JSX.Element => {
  const { groupName, organizationName, rootId } = useParams() as Record<
    string,
    string
  >;

  const [search, setSearch] = useSearch();
  const [filters, setFilters, options] = useFilters();
  const packagesQuery = usePackagesQuery(groupName, search, filters, rootId);

  return (
    <React.Fragment>
      <SectionHeader
        goBackTo={`/orgs/${organizationName}/groups/${groupName}/surface/packages/roots`}
        header={`Root | ${packagesQuery.nickname ?? "..."}`}
      />
      <Container px={1.25} py={1.25} scroll={"y"}>
        <PackagesTable
          localStorageKey={`tblToePackagesRootFilters-${groupName}`}
          onFilter={setFilters}
          onSearch={setSearch}
          options={options}
          packagesQuery={packagesQuery}
        />
      </Container>
    </React.Fragment>
  );
};

export { RootPackages };
