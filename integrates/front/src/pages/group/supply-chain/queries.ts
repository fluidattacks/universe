import { graphql } from "gql/gql";

const GET_PACKAGES = graphql(`
  query GetPackages(
    $after: String
    $first: Int!
    $groupName: String!
    $outdated: Boolean
    $platform: String
    $platforms: [String]
    $search: String
    $vulnerable: Boolean
  ) {
    group(groupName: $groupName) {
      name
      toePackages(
        after: $after
        first: $first
        outdated: $outdated
        platform: $platform
        platforms: $platforms
        search: $search
        vulnerable: $vulnerable
      ) {
        total
        edges {
          node {
            name
            outdated
            platform
            version
            vulnerable
            advisories {
              id
              epss
            }
            healthMetadata {
              authors
              latestVersion
              latestVersionCreatedAt
            }
            locations {
              dependencyType
              imageRef
              layer
              line
              path
              scope
            }
            root {
              id
              nickname
            }
            vulnerabilityInfo {
              id
              cve
              severityScore
            }
          }
        }
        pageInfo {
          endCursor
          hasNextPage
        }
      }
    }
  }
`);

const GET_TOTALS = graphql(`
  query GetTotals($groupName: String!) {
    group(groupName: $groupName) {
      name
      dockerImages(include: true) {
        __typename
      }
      gitRoots(first: 0, state: ACTIVE) {
        total
      }
      toePackages(first: 0) {
        total
      }
    }
  }
`);

export { GET_PACKAGES, GET_TOTALS };
