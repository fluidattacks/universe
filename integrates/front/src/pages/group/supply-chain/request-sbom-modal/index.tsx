import { Container, Loading, Text } from "@fluidattacks/design";
import { Fragment, useCallback } from "react";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";
import { array, object, string } from "yup";

import { useActiveRootsQuery, useRequestSbomFileMutation } from "./hooks";
import type { IFormValues, IRequestSbomModalProps } from "./types";

import { Checkbox } from "components/checkbox";
import { InnerForm } from "components/form";
import { FormModal } from "components/form-modal";
import { Select } from "components/input";
import { Search } from "components/search";
import { useSearch } from "components/search/hooks";
import type { UriWithCredentialsInput } from "gql/graphql";
import { useScroll } from "hooks";
import { msgSuccess } from "utils/notifications";

const initialValues: IFormValues = {
  fileFormat: "",
  rootNicknames: [],
  sbomFormat: "",
  uris: [],
};

const RequestSbomModal = ({
  groupName,
  modalRef,
}: IRequestSbomModalProps): JSX.Element => {
  const { close } = modalRef;

  const theme = useTheme();
  const { t } = useTranslation();

  const [search, setSearch] = useSearch();
  const { fetchNextPage, hasNextPage, loading, roots, images, total } =
    useActiveRootsQuery(groupName, search);
  const nicknames = roots.map((root): string => root.nickname);
  const rawUris = images.map(({ uri }): string => uri);
  const uriToCredentialIdMap: Record<string, string | null> = images.reduce<
    Record<string, string | null>
  >((acc, { uri, credentials }): Record<string, string | null> => {
    return {
      ...acc,
      [uri]: credentials?.id ?? null,
    };
  }, {});

  useScroll({
    containerId: "sbom-root-nicknames",
    onThresholdReached: (): void => {
      if (hasNextPage && !loading) {
        void fetchNextPage();
      }
    },
    percentageThreshold: 80,
  });

  const { requestSbomFile } = useRequestSbomFileMutation();
  const handleSubmit = useCallback(
    async (values: IFormValues): Promise<void> => {
      const urisWithCredentials = values.uris.map(
        (uri): UriWithCredentialsInput => ({
          credentialId: uriToCredentialIdMap[uri],
          uri,
        }),
      );

      const result = await requestSbomFile({
        fileFormat: values.fileFormat,
        groupName,
        rootNicknames: values.rootNicknames,
        sbomFormat: values.sbomFormat,
        urisWithCredentials,
      });
      close();

      if (result.data?.requestSbomFile.success === true) {
        msgSuccess(t("group.toe.packages.requestSuccess"));
      }
    },
    [close, groupName, requestSbomFile, t, uriToCredentialIdMap],
  );

  const validationSchema = object().shape({
    fileFormat: string().required(t("validations.required")),
    rootNicknames: array()
      .of(string())
      .default([])
      .isValidFunction((value, context): boolean => {
        if (value === undefined) {
          return false;
        }
        const parent = context.parent as IFormValues;

        return value.length > 0 || parent.uris.length > 0;
      }, t("validations.required")),
    sbomFormat: string().required(t("validations.required")),
    uris: array()
      .of(string())
      .default([])
      .isValidFunction((value, context): boolean => {
        if (value === undefined) {
          return false;
        }
        const parent = context.parent as IFormValues;

        return value.length > 0 || parent.rootNicknames.length > 0;
      }, t("validations.required")),
  });

  return (
    <FormModal
      initialValues={initialValues}
      modalRef={modalRef}
      name={"newSbomFile"}
      onSubmit={handleSubmit}
      size={"md"}
      subtitle={
        "Select the roots, SBOM format, and file format for your export. You can select multiple roots at once."
      }
      title={"Export SBOM file"}
      validationSchema={validationSchema}
    >
      {({ dirty, isSubmitting, values }): JSX.Element => {
        const sortedNicknames = nicknames.toSorted(
          (nicknameA, nicknameB): number => {
            const selectedComparison =
              Number(values.rootNicknames.includes(nicknameB)) -
              Number(values.rootNicknames.includes(nicknameA));

            if (selectedComparison === 0) {
              const alphabeticalComparison = nicknameA.localeCompare(nicknameB);

              return alphabeticalComparison;
            }

            return selectedComparison;
          },
        );
        const sortedUris = rawUris.toSorted((uriA, uriB): number => {
          const selectedComparison =
            Number(values.uris.includes(uriB)) -
            Number(values.uris.includes(uriA));

          if (selectedComparison === 0) {
            const alphabeticalComparison = uriA.localeCompare(uriB);

            return alphabeticalComparison;
          }

          return selectedComparison;
        });

        return (
          <InnerForm
            onCancel={close}
            submitButtonLabel={"Generate SBOM file"}
            submitDisabled={!dirty || isSubmitting}
          >
            <Container
              borderBottom={`1px solid ${theme.palette.gray["300"]}`}
              display={"flex"}
              flexDirection={"column"}
              gap={0.75}
              pb={1.25}
            >
              <Container display={"flex"} gap={0.75}>
                <Select
                  items={[
                    { header: "", value: "" },
                    { header: "CycloneDX", value: "cyclone_dx" },
                    { header: "SPDX", value: "spdx" },
                  ]}
                  label={t("group.toe.packages.form.sbomFormat")}
                  name={"sbomFormat"}
                />
                <Select
                  items={[
                    { header: "", value: "" },
                    { header: "JSON", value: "json" },
                    { header: "XML", value: "xml" },
                  ]}
                  label={t("group.toe.packages.form.fileFormat")}
                  name={"fileFormat"}
                />
              </Container>
              <Container>
                <Search handleOnKeyPressed={setSearch} placeHolder={"Search"} />
              </Container>
              <Text color={theme.palette.gray["400"]} size={"sm"}>
                {"You have selected"}&nbsp;
                <b>{values.rootNicknames.length}</b>&nbsp;
                {"resources of"}&nbsp;
                <b>{total}</b>
              </Text>
            </Container>
            <Container
              borderBottom={`1px solid ${theme.palette.gray["300"]}`}
              pb={1.25}
              pt={1.25}
            >
              {loading ? (
                <Container display={"flex"} justify={"center"} pb={1.25}>
                  <Loading />
                </Container>
              ) : (
                <Fragment>
                  <Container mb={1.25}>
                    <Container mb={1.25}>
                      <Text fontWeight={"bold"} size={"md"}>
                        {"Docker Images"}
                      </Text>
                    </Container>
                    <Container
                      display={"flex"}
                      gap={1.25}
                      id={"sbom-images-uris"}
                      maxHeight={"125px"}
                      scroll={"y"}
                      wrap={"wrap"}
                    >
                      {sortedUris.map(
                        (uri): JSX.Element => (
                          <Container
                            key={uri}
                            width={"170px"}
                            wordBreak={"break-word"}
                          >
                            <Checkbox
                              label={uri}
                              name={"uris"}
                              value={uri}
                              variant={"formikField"}
                            />
                          </Container>
                        ),
                      )}
                    </Container>
                  </Container>
                  <Container>
                    <Container mb={1.25}>
                      <Text fontWeight={"bold"} size={"md"}>
                        {"Roots"}
                      </Text>
                    </Container>
                    <Container
                      display={"flex"}
                      gap={1.25}
                      id={"sbom-root-nicknames"}
                      maxHeight={"125px"}
                      scroll={"y"}
                      wrap={"wrap"}
                    >
                      {sortedNicknames.map(
                        (nickname): JSX.Element => (
                          <Container
                            key={nickname}
                            width={"170px"}
                            wordBreak={"break-word"}
                          >
                            <Checkbox
                              label={nickname}
                              name={"rootNicknames"}
                              value={nickname}
                              variant={"formikField"}
                            />
                          </Container>
                        ),
                      )}
                    </Container>
                  </Container>
                </Fragment>
              )}
            </Container>
          </InnerForm>
        );
      }}
    </FormModal>
  );
};

export type { IFormValues };
export { RequestSbomModal };
