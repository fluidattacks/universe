import { PureAbility } from "@casl/ability";
import { screen } from "@testing-library/react";
import type { StrictResponse } from "msw";
import { HttpResponse, graphql } from "msw";

import { GET_ACTIVE_RESOURCES } from "./queries";

import { RequestSbomModal } from ".";
import { CustomThemeProvider } from "components/colors";
import { authzPermissionsContext } from "context/authz/config";
import type { GetActiveResourcesQuery } from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";

const graphqlMocked = graphql.link(LINK);

describe("requestSbomModal", (): void => {
  const activeRootsQuery = graphqlMocked.query(
    GET_ACTIVE_RESOURCES,
    (): StrictResponse<{ data: GetActiveResourcesQuery }> => {
      return HttpResponse.json({
        data: {
          group: {
            dockerImages: [
              {
                credentials: {
                  id: "2433d43d56df7jk890",
                },
                uri: "docker.io/test-image",
              },
            ],
            gitRoots: {
              edges: [{ node: { id: "123456789", nickname: "testroot" } }],
              pageInfo: { endCursor: "endCursor", hasNextPage: true },
              total: 1,
            },
            name: "unittesting",
          },
        },
      });
    },
  );

  it("should display sbom modal", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([]);

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <CustomThemeProvider>
          <RequestSbomModal
            groupName={"unittesting"}
            modalRef={{
              close: jest.fn(),
              isOpen: true,
              name: "",
              open: jest.fn(),
              setIsOpen: jest.fn(),
            }}
          />
        </CustomThemeProvider>
      </authzPermissionsContext.Provider>,
      { mocks: [activeRootsQuery] },
    );

    await expect(screen.findByText("testroot")).resolves.toBeInTheDocument();
  });
});
