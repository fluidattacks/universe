interface IAdvisoryOrCoordinates {
  __typename?: "ToePackageAdvisory" | "ToePackageCoordinates";
  id: string;
  percentile?: number | null;
  severity?: string;
  cpes?: string[];
  description?: string | null;
  epss?: number | null;
  namespace?: string;
  urls?: string[];
  versionConstraint?: string | null;

  dependencyType?: string | null;
  imageRef?: string | null;
  layer?: string | null;
  line?: string | null;
  path?: string;
  scope?: string | null;
}

export type { IAdvisoryOrCoordinates };
