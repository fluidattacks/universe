import { useQuery } from "@apollo/client";

import { GET_DOCKER_IMAGES } from "./queries";

import type { GetDockerImagesQuery } from "gql/graphql";

type TDockerImage = GetDockerImagesQuery["group"]["dockerImages"][0];
interface IUsePackagesQuery {
  dockerImages: TDockerImage[];
  loading: boolean;
}

const useDockerImagesQuery = (groupName: string): IUsePackagesQuery => {
  const { data, loading } = useQuery(GET_DOCKER_IMAGES, {
    variables: { groupName },
  });

  return { dockerImages: data?.group.dockerImages ?? [], loading };
};

export type { TDockerImage };
export { useDockerImagesQuery };
