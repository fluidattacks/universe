import { Container, Icon, Text, Tooltip } from "@fluidattacks/design";
import type { ColumnDef } from "@tanstack/react-table";
import * as React from "react";
import { useParams } from "react-router-dom";
import type { DefaultTheme } from "styled-components";
import { useTheme } from "styled-components";

import { renderExpandedRow } from "./expanded-row";
import { formatIdColumn } from "./formatters/format-id-column";
import { formatSeverityColumn } from "./formatters/format-severity-column";
import { usePackageQuery } from "./hooks";

import type { TPackage } from "../hooks";
import type { IAdvisoryOrCoordinates } from "../types";
import { SectionHeader } from "components/section-header";
import { Table } from "components/table";
import { useTable } from "hooks";

const parseId = (packageId: string): { name: string; version: string } => {
  const [version, ...nameParts] = decodeURIComponent(packageId)
    .split("@")
    .toReversed();

  return { name: nameParts.toReversed().join("@"), version };
};
const EPSS_MULTIPLIER = 100;
const getColumns = (
  theme: DefaultTheme,
  vulnInfo: TPackage["vulnerabilityInfo"] | undefined,
): ColumnDef<IAdvisoryOrCoordinates>[] => {
  return [
    {
      accessorKey: "dependencyType",
      cell: (cell): JSX.Element => {
        const knownType =
          cell.row.original.dependencyType === "TRANSITIVE" ? (
            <Tooltip
              id={"transitive"}
              place={"top"}
              tip={"Transitive"}
              width={"100%"}
            >
              <Icon
                icon={"square-t"}
                iconColor={"#175CD3"}
                iconSize={"sm"}
                iconType={"fa-solid"}
              />
            </Tooltip>
          ) : (
            <Tooltip id={"direct"} place={"top"} tip={"Direct"} width={"100%"}>
              <Icon
                icon={"square-d"}
                iconColor={"#B8075D"}
                iconSize={"sm"}
                iconType={"fa-solid"}
              />
            </Tooltip>
          );

        return (
          <Container
            left={"-10px"}
            position={"relative"}
            textAlign={"center"}
            width={"100%"}
          >
            {cell.row.original.dependencyType === null ||
            cell.row.original.dependencyType === "UNKNOWN" ? (
              <Tooltip
                id={"direct"}
                place={"top"}
                tip={"Unknown"}
                width={"100%"}
              >
                <Container
                  alignItems={"center"}
                  bgColor={theme.palette.gray["200"]}
                  borderRadius={"4px"}
                  display={"inline-flex"}
                  height={"21px"}
                  justify={"center"}
                  width={"21px"}
                >
                  <Icon
                    icon={"question"}
                    iconClass={"fa-duotone"}
                    iconColor={"#475467"}
                    iconSize={"xs"}
                    iconType={"fa-solid"}
                  />
                </Container>
              </Tooltip>
            ) : (
              knownType
            )}
          </Container>
        );
      },
      header: "Type",
    },
    {
      accessorKey: "path",
      cell: ({ row }): string | undefined => {
        const { imageRef, path } = row.original;
        if (imageRef === null) {
          return path;
        }

        return `${imageRef} > ${path}`;
      },
      header: "Location",
    },
    { accessorKey: "line", header: "Specific" },
    {
      accessorKey: "scope",
      cell: (cell): JSX.Element => (
        <Container alignItems={"center"} display={"flex"} gap={0.5}>
          {cell.row.original.scope === "PROD" ? (
            <React.Fragment>
              <Icon
                icon={"rocket-launch"}
                iconColor={theme.palette.black}
                iconSize={"xxs"}
                iconType={"fa-light"}
              />
              {"Run"}
            </React.Fragment>
          ) : (
            <React.Fragment>
              <Icon
                icon={"square-terminal"}
                iconColor={theme.palette.black}
                iconSize={"xxs"}
                iconType={"fa-light"}
              />
              {"Build"}
            </React.Fragment>
          )}
        </Container>
      ),
      header: "Environment",
    },
    {
      accessorKey: "id",
      cell: (cell): JSX.Element => formatIdColumn(theme, cell, vulnInfo),
      header: "ID",
    },
    { accessorKey: "namespace", header: "Namespace" },
    {
      accessorKey: "epss",
      cell: ({ getValue }): string =>
        `${(getValue<number>() * EPSS_MULTIPLIER).toFixed(2)}%`,
      header: "% EPSS",
    },
    { accessorKey: "severity", cell: formatSeverityColumn, header: "Severity" },
    { accessorKey: "versionConstraint", header: "Affected version" },
  ];
};
const flattenVulnerabilityInfo = (
  advisories: TPackage["advisories"] | undefined,
  locations: TPackage["locations"] | undefined,
): IAdvisoryOrCoordinates[] => {
  if (!advisories || advisories.length === 0) {
    return [];
  }

  return advisories.flatMap((advisory): IAdvisoryOrCoordinates[] => {
    if (locations && locations.length > 0) {
      return locations.map(
        (loc): IAdvisoryOrCoordinates => ({
          ...advisory,
          ...loc,
        }),
      );
    }

    return [advisory];
  });
};

const PackageDetails: React.FC = (): JSX.Element => {
  const theme = useTheme();
  const { groupName, organizationName, rootId, packageId } =
    useParams() as Record<string, string>;
  const { name, version } = parseId(packageId);

  const { pkg, loading } = usePackageQuery(groupName, name, rootId, version);
  const advisoriesTable = useTable("package-advisories-table");
  const flattenVulns = flattenVulnerabilityInfo(
    pkg?.advisories,
    pkg?.locations,
  );

  return (
    <React.Fragment>
      <SectionHeader
        goBackTo={`/orgs/${organizationName}/groups/${groupName}/surface/packages`}
        header={`Package | ${name}@${version}`}
      />
      <Container px={1.25} py={1.25} scroll={"y"}>
        <Text color={theme.palette.black} fontWeight={"bold"} size={"xl"}>
          {"Dependency detail"}
        </Text>
        <Table
          columns={getColumns(theme, pkg?.vulnerabilityInfo)}
          data={flattenVulns}
          expandedRow={renderExpandedRow}
          loadingData={loading}
          tableRef={advisoriesTable}
        />
      </Container>
    </React.Fragment>
  );
};

export { PackageDetails };
