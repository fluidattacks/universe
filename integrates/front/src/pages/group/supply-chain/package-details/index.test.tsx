import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import type { StrictResponse } from "msw";
import { HttpResponse, graphql } from "msw";
import { Route, Routes } from "react-router-dom";

import { GET_PACKAGE } from "./queries";

import { PackageDetails } from ".";
import { CustomThemeProvider } from "components/colors";
import type { GetPackageQuery } from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";

const graphqlMocked = graphql.link(LINK);
const getPackageMock = graphqlMocked.query(
  GET_PACKAGE,
  (): StrictResponse<{ data: GetPackageQuery }> => {
    return HttpResponse.json({
      data: {
        group: {
          name: "testgroup",
          package: {
            advisories: [
              {
                cpes: ["cpe:2.3:a:*:*:*.*.*.*.*.*.*"],
                description: "testdescription",
                epss: 0.1,
                id: "CVE-2024-12345",
                namespace: "nvd:cpe",
                percentile: 0.2,
                severity: "High",
                urls: ["https://nvd.nist.gov/vuln/detail/CVE-2024-12345"],
                versionConstraint: "< 1.2.4",
              },
            ],
            id: "package@1.2.3",
            locations: [
              {
                dependencyType: "DIRECT",
                imageRef: null,
                layer: null,
                line: "85",
                path: "build.gradle",
                scope: "PROD",
              },
            ],
            name: "package",
            version: "1.2.3",
            vulnerabilityInfo: [
              {
                cve: ["CVE-2024-12345"],
                id: "vuln-1",
                severityScore: 0.2,
              },
            ],
            vulnerable: true,
          },
        },
      },
    });
  },
);

describe("packageDetails", (): void => {
  it("should display details", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <CustomThemeProvider>
        <Routes>
          <Route
            element={<PackageDetails />}
            path={"/packages/:rootId/:packageId"}
          />
        </Routes>
      </CustomThemeProvider>,
      {
        memoryRouter: {
          initialEntries: ["/packages/root123/package%401.2.3"],
        },
        mocks: [getPackageMock],
      },
    );

    await waitFor((): void => {
      expect(screen.queryAllByText("CVE-2024-12345")[0]).toBeInTheDocument();
    });

    const iconElement = screen.getByTestId("triangle-exclamation-icon");

    expect(iconElement).toBeInTheDocument();

    expect(screen.queryByRole("table")).toBeInTheDocument();

    expect(screen.getAllByRole("row")[0].textContent).toStrictEqual(
      [
        "Type",
        "Location",
        "Specific",
        "Environment",
        "ID",
        "Namespace",
        "% EPSS",
        "Severity",
        "Affected version",
      ].join(""),
    );
    expect(screen.getAllByRole("row")[1].textContent).toStrictEqual(
      [
        "build.gradle",
        "85",
        "Run",
        "CVE-2024-12345",
        "nvd:cpe",
        "10.00%",
        "High",
        "< 1.2.4",
      ].join(""),
    );

    const [expandCaret] = Array.from(
      screen.getAllByRole("row")[1].getElementsByClassName("fa-angle-down"),
    );
    await userEvent.click(expandCaret);

    expect(screen.getAllByRole("row")[2].textContent).toBe(
      [
        "Description",
        "testdescription",
        "URLs (1)",
        "https://nvd.nist.gov/vuln/detail/CVE-2024-12345",
      ].join(""),
    );

    jest.clearAllMocks();
  });
});
