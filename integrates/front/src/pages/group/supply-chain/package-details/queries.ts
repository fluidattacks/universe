import { graphql } from "gql";

const GET_PACKAGE = graphql(`
  query GetPackage(
    $groupName: String!
    $name: String!
    $rootId: ID!
    $version: String!
  ) {
    group(groupName: $groupName) {
      name
      package(name: $name, rootId: $rootId, version: $version) {
        id
        name
        version
        vulnerable
        advisories {
          id
          cpes
          description
          epss
          namespace
          percentile
          severity
          urls
          versionConstraint
        }
        locations {
          dependencyType
          imageRef
          layer
          line
          path
          scope
        }
        vulnerabilityInfo {
          id
          cve
          severityScore
        }
      }
    }
  }
`);

export { GET_PACKAGE };
