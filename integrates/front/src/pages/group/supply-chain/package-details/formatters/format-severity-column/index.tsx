import { SeverityBadge } from "@fluidattacks/design";
import type { CellContext } from "@tanstack/react-table";
import * as React from "react";

import type { TSeverityBadgeVariant } from "components/severity-badge";
import type { IAdvisoryOrCoordinates } from "pages/group/supply-chain/types";

const isSeverityVariant = (value: unknown): value is TSeverityBadgeVariant => {
  return ["critical", "disable", "high", "low", "medium"].includes(
    value as string,
  );
};

const formatSeverityColumn = (
  cell: CellContext<IAdvisoryOrCoordinates, unknown>,
): React.JSX.Element => {
  const details = cell.row.original;

  const severity =
    details.severity === undefined ? "" : details.severity.trim().toLowerCase();

  const severityVariant: TSeverityBadgeVariant = isSeverityVariant(severity)
    ? severity
    : "disable";

  return (
    <SeverityBadge textR={details.severity ?? ""} variant={severityVariant} />
  );
};

export { formatSeverityColumn };
