import { PureAbility } from "@casl/ability";
import { fireEvent, screen, waitFor, within } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import dayjs from "dayjs";
import { GraphQLError } from "graphql";
import isEqual from "lodash/isEqual";
import { HttpResponse, type StrictResponse, graphql, http } from "msw";
import { Route, Routes } from "react-router-dom";

import { GET_VERIFIED_FINDING_INFO } from "./affected-reattack-accordion/queries";
import { GET_FINDING_VULNS_TO_REATTACK } from "./affected-reattack-accordion/vulnerabilities-to-reattack-table/queries";
import {
  ADD_EVENT_MUTATION,
  GET_EVENTS,
  REQUEST_EVENT_VERIFICATION_MUTATION,
  REQUEST_VULNS_HOLD_MUTATION,
} from "./queries";

import { GroupEvents } from ".";
import {
  authzGroupContext,
  authzPermissionsContext,
} from "context/authz/config";
import type {
  AddEventMutationMutation as AddEvent,
  GetEventsQuery as GetEvents,
  GetFindingVulnsToReattackQuery as GetFindingVulnsToReattack,
  GetRootEnvironmentUrlsQuery as GetRootEnvironmentUrls,
  GetRootNicknamesQuery as GetRoots,
  GetVerifiedFindingInfoQuery as GetVerifiedFindingInfo,
  RequestEventVerificationMutation as RequestEventVerification,
  RequestVulnerabilitiesHoldMutation,
} from "gql/graphql";
import { EventType, ResourceState } from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage, IMessage } from "mocks/types";
import {
  GET_ROOTS,
  GET_ROOT_ENVIRONMENT_URLS,
} from "pages/group/scope/queries";
import { msgError, msgSuccess } from "utils/notifications";
import { selectOption } from "utils/test-utils";

const mockNavigatePush: jest.Mock = jest.fn();
jest.mock(
  "react-router-dom",
  (): Record<string, unknown> => ({
    ...jest.requireActual("react-router-dom"),
    useNavigate: (): jest.Mock => mockNavigatePush,
  }),
);

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

describe("groupEvents", (): void => {
  const graphqlMocked = graphql.link(LINK);
  const mockError = [
    graphqlMocked.query(GET_EVENTS, (): StrictResponse<IErrorMessage> => {
      return HttpResponse.json({
        errors: [new GraphQLError("Error getting events error")],
      });
    }),
    graphqlMocked.query(
      GET_VERIFIED_FINDING_INFO,
      (): StrictResponse<IErrorMessage> => {
        return HttpResponse.json({
          errors: [new GraphQLError("Error getting events error")],
        });
      },
    ),
  ];
  const mockedVerified = graphqlMocked.query(
    GET_VERIFIED_FINDING_INFO,
    (): StrictResponse<{ data: GetVerifiedFindingInfo }> => {
      return HttpResponse.json({
        data: {
          group: {
            __typename: "Group",
            findings: [],
            name: "unittesting",
          },
        },
      });
    },
  );
  const mockedRoots = graphqlMocked.query(
    GET_ROOTS,
    (): StrictResponse<{ data: GetRoots }> => {
      return HttpResponse.json({
        data: {
          group: {
            __typename: "Group",
            name: "unittesting",
            roots: [],
          },
        },
      });
    },
  );
  const mockedEnvironments = graphqlMocked.query(
    GET_ROOT_ENVIRONMENT_URLS,
    (): StrictResponse<{ data: GetRootEnvironmentUrls }> => {
      return HttpResponse.json({
        data: {
          root: {
            __typename: "GitRoot",
            gitEnvironmentUrls: [
              {
                id: "c2926b0542bc6051faf9ecf3f2751d1962c29be0",
                include: true,
                rootId: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
                url: "https://test.com",
                urlType: "URL",
                useEgress: false,
                useVpn: false,
                useZtna: false,
              },
            ],
            id: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
          },
        },
      });
    },
  );
  const mockedEvent = graphqlMocked.query(
    GET_EVENTS,
    (): StrictResponse<{ data: GetEvents }> => {
      return HttpResponse.json({
        data: {
          group: {
            __typename: "Group",
            events: [
              {
                __typename: "Event",
                closingDate: "-",
                detail: "Test description",
                environment: null,
                eventDate: "2018-10-17 00:00:00",
                eventStatus: "SOLVED",
                eventType: EventType.AuthorizationSpecialAttack,
                groupName: "unittesting",
                id: "463457733",
                root: null,
              },
              {
                __typename: "Event",
                closingDate: "-",
                detail: "Test description",
                environment: null,
                eventDate: "2018-10-17 00:00:00",
                eventStatus: "CREATED",
                eventType: EventType.NetworkAccessIssues,
                groupName: "unittesting",
                id: "12314123",
                root: null,
              },
            ],
            name: "unittesting",
          },
        },
      });
    },
  );

  const mockedRootsInfo = graphqlMocked.query(
    GET_ROOTS,
    (): StrictResponse<{ data: GetRoots }> => {
      return HttpResponse.json({
        data: {
          group: {
            __typename: "Group",
            name: "unittesting",
            roots: [
              {
                __typename: "GitRoot",
                id: "ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19",
                nickname: "universe",
                state: ResourceState.Active,
              },
            ],
          },
        },
      });
    },
  );

  it("should render an error in component", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route element={<GroupEvents />} path={"/groups/:groupName/events"} />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/groups/unittesting/events"],
        },
        mocks: mockError,
      },
    );
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith("groupAlerts.errorTextsad");
    });
    jest.clearAllMocks();
  });

  it("should render events table and go to event", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route element={<GroupEvents />} path={"/groups/:groupName/events"} />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/groups/unittesting/events"],
        },
        mocks: [mockedEvent, mockedRoots, mockedVerified],
      },
    );
    await waitFor((): void => {
      expect(
        screen.getByRole("cell", {
          name: "Authorization for a special attack",
        }),
      ).toBeInTheDocument();
    });

    expect(screen.queryByRole("table")).toBeInTheDocument();

    await userEvent.click(screen.getByRole("cell", { name: "463457733" }));

    expect(mockNavigatePush).toHaveBeenCalledWith(
      "/groups/unittesting/events/463457733/description",
    );

    jest.clearAllMocks();
  });

  it("should render new event modal", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_add_event_mutate" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route element={<GroupEvents />} path={"/groups/:groupName/events"} />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/groups/unittesting/events"],
        },
        mocks: [mockedEvent, mockedRoots, mockedVerified],
      },
    );
    await waitFor((): void => {
      expect(screen.queryByText("group.events.btn.text")).toBeInTheDocument();
    });
    await userEvent.click(screen.getByText("group.events.btn.text"));
    await waitFor((): void => {
      expect(screen.queryByText("group.events.new")).toBeInTheDocument();
    });

    expect(screen.getAllByText("group.events.form.date")).toHaveLength(1);
    expect(screen.getAllByRole("combobox", { name: "eventType" })).toHaveLength(
      1,
    );
    expect(
      screen.getAllByRole("combobox", { name: "rootNickname" }),
    ).toHaveLength(1);
    expect(screen.getAllByRole("textbox", { name: "detail" })).toHaveLength(1);
    expect(screen.getAllByTestId("files")).toHaveLength(1);
    expect(screen.getAllByTestId("images")).toHaveLength(1);

    jest.clearAllMocks();
  });

  it("should render add event", async (): Promise<void> => {
    expect.hasAssertions();

    const images = [
      new File(
        ["okada-unittesting-0192837465"],
        "okada-unittesting-0192837465.png",
        { type: "image/png" },
      ),
      new File(
        ["okada-unittesting-5647382910"],
        "okada-unittesting-0192837465.png",
        { type: "image/png" },
      ),
    ];
    const file1 = new File(
      ["okada-unittesting-56789abcde"],
      "okada-unittesting-56789abcde.txt",
      {
        type: "text/plain",
      },
    );

    const mockedQueries = [
      mockedEvent,
      mockedVerified,
      mockedRootsInfo,
      mockedEnvironments,
    ];
    const mockedMutations = [
      graphqlMocked.mutation(
        ADD_EVENT_MUTATION,
        ({ variables }): StrictResponse<IErrorMessage | { data: AddEvent }> => {
          const { detail, eventDate, eventType, groupName, rootId } = variables;
          if (
            detail === "detail test" &&
            eventDate ===
              `${String(dayjs().year() - 1)}-09-07T00:00:00+00:00` &&
            eventType === EventType.CloningIssues &&
            groupName === "unittesting" &&
            rootId === "ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19"
          ) {
            return HttpResponse.json({
              data: {
                addEvent: {
                  eventId: "123",
                  success: true,
                },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Exception - Invalid characters")],
          });
        },
      ),
      http.post(LINK, (): HttpResponse => {
        return HttpResponse.json({
          data: {
            updateEventEvidence: {
              success: true,
            },
          },
        });
      }),
    ];

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_add_event_mutate" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<GroupEvents />}
            path={"/orgs/:organizationName/groups/:groupName/events"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/unittesting/events"],
        },
        mocks: [...mockedQueries, ...mockedMutations],
      },
    );
    await waitFor((): void => {
      expect(screen.queryByText("group.events.btn.text")).toBeInTheDocument();
    });
    await userEvent.click(screen.getByText("group.events.btn.text"));

    expect(screen.queryByText("group.events.new")).toBeInTheDocument();

    expect(screen.getByText("buttons.confirm")).toBeDisabled();

    await userEvent.type(
      screen.getByRole("combobox", { name: "rootNickname" }),
      "universe",
    );
    await userEvent.type(
      screen.getByRole("textbox", { name: "detail" }),
      "detail test",
    );

    // 09/07/202X 12:00 AM
    await userEvent.type(
      screen.getByRole("spinbutton", { name: "month," }),
      "09",
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: "day," }),
      "07",
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: "year," }),
      String(dayjs().year() - 1),
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: "hour," }),
      "12",
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: "minute," }),
      "00",
    );

    await selectOption("eventType", "CLONING_ISSUES");

    await userEvent.upload(screen.getByTestId("images"), images);
    await userEvent.upload(screen.getByTestId("files"), file1);

    expect(screen.getByText("buttons.confirm")).not.toBeDisabled();

    await userEvent.click(screen.getByText("buttons.confirm"));

    jest.clearAllMocks();
  });

  it("should add event with affected reattacks", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedQueries = [
      mockedEvent,
      graphqlMocked.query(
        GET_VERIFIED_FINDING_INFO,
        (): StrictResponse<{ data: GetVerifiedFindingInfo }> => {
          return HttpResponse.json({
            data: {
              group: {
                __typename: "Group",
                findings: [
                  {
                    __typename: "Finding",
                    id: "test-finding-id-01",
                    title: "038. Business information leak",
                    verified: false,
                  },
                  {
                    __typename: "Finding",
                    id: "test-finding-id-02",
                    title: "083. XML injection (XXE)",
                    verified: true,
                  },
                ],
                name: "unittesting",
              },
            },
          });
        },
      ),
      mockedRootsInfo,
      graphqlMocked.query(
        GET_FINDING_VULNS_TO_REATTACK,
        (): StrictResponse<
          IErrorMessage | { data: GetFindingVulnsToReattack }
        > => {
          return HttpResponse.json({
            data: {
              finding: {
                __typename: "Finding",
                id: "test-finding-id-01",
                vulnerabilitiesToReattackConnection: {
                  edges: [
                    {
                      node: {
                        __typename: "Vulnerability",
                        findingId: "test-finding-id-01",
                        id: "test-vuln-id",
                        specific: "1111",
                        where: "vulnerable entrance",
                      },
                    },
                  ],
                  pageInfo: {
                    endCursor: "cursor",
                    hasNextPage: true,
                  },
                },
              },
            },
          });
        },
      ),
    ];
    const mockedMutations = [
      graphqlMocked.mutation(
        ADD_EVENT_MUTATION,
        ({ variables }): StrictResponse<IErrorMessage | { data: AddEvent }> => {
          const { detail, eventDate, eventType, groupName, rootId } = variables;
          if (
            detail === "detail test" &&
            eventDate ===
              `${String(dayjs().year() - 1)}-09-07T00:00:00+00:00` &&
            eventType === EventType.CloningIssues &&
            groupName === "unittesting" &&
            rootId === "ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19"
          ) {
            return HttpResponse.json({
              data: {
                addEvent: {
                  eventId: "123",
                  success: true,
                },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Exception - Invalid characters")],
          });
        },
      ),
      graphqlMocked.mutation(
        REQUEST_VULNS_HOLD_MUTATION,
        ({
          variables,
        }): StrictResponse<
          IErrorMessage | { data: RequestVulnerabilitiesHoldMutation }
        > => {
          const { groupName } = variables;

          if (groupName === "unittesting") {
            return HttpResponse.json({
              data: {
                requestVulnerabilitiesHold: {
                  __typename: "SimplePayload",
                  success: true,
                },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error requesting reattack holds")],
          });
        },
      ),
      http.post(LINK, (): HttpResponse => {
        return HttpResponse.json({
          data: {
            updateEventEvidence: {
              success: true,
            },
          },
        });
      }),
    ];

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_add_event_mutate" },
    ]);
    jest.spyOn(console, "error").mockImplementation();

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <authzGroupContext.Provider
          value={new PureAbility([{ action: "can_report_vulnerabilities" }])}
        >
          <Routes>
            <Route
              element={<GroupEvents />}
              path={"/orgs/:organizationName/groups/:groupName/events"}
            />
          </Routes>
        </authzGroupContext.Provider>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/unittesting/events"],
        },
        mocks: [...mockedQueries, ...mockedMutations],
      },
    );
    await waitFor((): void => {
      expect(screen.queryByText("group.events.btn.text")).toBeInTheDocument();
    });
    await userEvent.click(screen.getByText("group.events.btn.text"));
    await waitFor((): void => {
      expect(screen.queryByText("group.events.new")).toBeInTheDocument();
    });

    expect(screen.getByText("buttons.confirm")).toBeDisabled();

    await userEvent.type(
      screen.getByRole("combobox", { name: "rootNickname" }),
      "universe",
    );
    await userEvent.type(
      screen.getByRole("textbox", { name: "detail" }),
      "detail test",
    );

    // 09/07/202X 12:00 AM
    await userEvent.type(
      screen.getByRole("spinbutton", { name: "month," }),
      "09",
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: "day," }),
      "07",
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: "year," }),
      String(dayjs().year() - 1),
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: "hour," }),
      "12",
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: "minute," }),
      "00",
    );

    await selectOption("eventType", "CLONING_ISSUES");

    const affectsReattacks = screen.getByRole("checkbox", {
      name: "affectsReattacks",
    });

    expect(affectsReattacks).not.toBeChecked();

    await userEvent.click(affectsReattacks);

    expect(affectsReattacks).toBeChecked();

    await userEvent.click(screen.getByText("038. Business information leak"));

    expect(screen.queryByText("Select at least one value")).toBeInTheDocument();

    await userEvent.click(
      within(screen.getAllByRole("table")[1]).getAllByTestId("check-icon")[0],
    );

    expect(
      screen.queryByText("Select at least one value"),
    ).not.toBeInTheDocument();

    await waitFor((): void => {
      expect(screen.getByText("buttons.confirm")).not.toBeDisabled();
    });
    await userEvent.click(screen.getByText("buttons.confirm"));

    jest.clearAllMocks();
  });

  it("should handle error adding event in file", async (): Promise<void> => {
    expect.hasAssertions();

    const images = [
      new File(["okada-nn-0192837465"], "okada-nn-0192837465.txt", {
        type: "text/plain",
      }),
    ];
    const file = new File(["okada-nn-56789abcde"], "okada-nn-56789abcde.img", {
      type: "image/png",
    });

    const mockedQueries = [
      mockedEvent,
      mockedVerified,
      mockedRootsInfo,
      mockedEnvironments,
    ];
    const mockedMutations = [
      graphqlMocked.mutation(
        ADD_EVENT_MUTATION,
        ({
          variables,
        }): StrictResponse<
          Record<"errors", IMessage[]> | { data: AddEvent }
        > => {
          const { detail, eventDate, eventType, groupName, rootId } = variables;
          if (
            eventDate ===
              `${String(dayjs().year() - 1)}-09-07T00:00:00+00:00` &&
            detail === "detail test" &&
            eventType === EventType.CloningIssues &&
            groupName === "unittesting" &&
            rootId === "ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19"
          ) {
            return HttpResponse.json({
              errors: [
                new GraphQLError("Exception - Invalid file type: EVENT_IMAGE"),
                new GraphQLError("Exception - Invalid file type: EVENT_FILE"),
                new GraphQLError("Exception - Invalid characters"),
                new GraphQLError(
                  "Exception - Invalid file name: Format organizationName-groupName-10 alphanumeric chars.extension",
                ),
              ],
            });
          }

          return HttpResponse.json({
            data: {
              addEvent: {
                eventId: "",
                success: true,
              },
            },
          });
        },
      ),
      http.post(LINK, (): HttpResponse => {
        return HttpResponse.json({
          data: {
            updateEventEvidence: {
              success: true,
            },
          },
        });
      }),
    ];

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_add_event_mutate" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<GroupEvents />}
            path={"/orgs/:organizationName/groups/:groupName/events"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/unittesting/events"],
        },
        mocks: [...mockedQueries, ...mockedMutations],
      },
    );
    await waitFor((): void => {
      expect(screen.queryByText("group.events.btn.text")).toBeInTheDocument();
    });
    await userEvent.click(screen.getByText("group.events.btn.text"));
    await waitFor((): void => {
      expect(screen.queryByText("group.events.new")).toBeInTheDocument();
    });

    expect(screen.getByText("buttons.confirm")).toBeDisabled();

    await userEvent.type(
      screen.getByRole("combobox", { name: "rootNickname" }),
      "universe",
    );

    const dropdownTrigger = screen.getByTestId(
      "environmentUrl-select-selected-option",
    );

    expect(dropdownTrigger).toBeInTheDocument();

    fireEvent.click(dropdownTrigger);

    const dropdownOptions = screen.getByTestId(
      "environmentUrl-select-dropdown-options",
    );

    expect(dropdownOptions).toBeInTheDocument();

    const expectedOptions = ["No related environment", "https://test.com"];

    expectedOptions.forEach((optionText): void => {
      expect(screen.getByText(optionText)).toBeInTheDocument();
    });

    await userEvent.type(
      screen.getByRole("textbox", { name: "detail" }),
      "detail test",
    );

    // 09/07/202X 12:00 AM
    await userEvent.type(
      screen.getByRole("spinbutton", { name: "month," }),
      "09",
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: "day," }),
      "07",
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: "year," }),
      String(dayjs().year() - 1),
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: "hour," }),
      "12",
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: "minute," }),
      "00",
    );

    await selectOption("eventType", "CLONING_ISSUES");

    await userEvent.upload(screen.getByTestId("images"), images);
    await userEvent.upload(screen.getByTestId("files"), file);
    await waitFor((): void => {
      expect(screen.getByText("buttons.confirm")).not.toBeDisabled();
    });
    await userEvent.click(screen.getByText("buttons.confirm"));

    jest.clearAllMocks();
  });

  it("should request verification", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedQueries = [mockedVerified, mockedRoots, mockedEvent];
    const mockedMutations = [
      graphqlMocked.mutation(
        REQUEST_EVENT_VERIFICATION_MUTATION,
        ({
          variables,
        }): StrictResponse<
          IErrorMessage | { data: RequestEventVerification }
        > => {
          const { comments, eventId, groupName } = variables;
          if (
            comments === "The solution test" &&
            eventId === "12314123" &&
            groupName === "unittesting"
          ) {
            return HttpResponse.json({
              data: {
                requestEventVerification: {
                  success: true,
                },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Exception - Invalid characters")],
          });
        },
      ),
    ];

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_request_event_verification_mutate" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route element={<GroupEvents />} path={"/groups/:groupName/events"} />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/groups/unittesting/events"],
        },
        mocks: [...mockedQueries, ...mockedMutations],
      },
    );
    await waitFor((): void => {
      expect(
        screen.getByRole("cell", { name: "Network access issues" }),
      ).toBeInTheDocument();
    });

    expect(
      within(
        screen.getByRole("row", {
          name: /12314123 network access issues 2018-10-17 test description unsolved -/iu,
        }),
      ).getByRole("checkbox"),
    ).not.toBeChecked();

    await userEvent.click(
      within(
        screen.getByRole("row", {
          name: /12314123 network access issues 2018-10-17 test description unsolved -/iu,
        }),
      ).getByRole("checkbox"),
    );
    await waitFor((): void => {
      expect(
        within(
          screen.getByRole("row", {
            name: /12314123 network access issues 2018-10-17 test description unsolved -/iu,
          }),
        ).getByRole("checkbox"),
      ).toBeChecked();
    });

    await userEvent.click(
      screen.getByRole("button", {
        name: /group.events.remediationmodal.btn.text/iu,
      }),
    );

    await waitFor((): void => {
      expect(
        screen.getByText("group.events.remediationModal.titleRequest"),
      ).toBeInTheDocument();
    });

    const justificationField = screen.getByRole("textbox", {
      name: /treatmentjustification/iu,
    });

    await userEvent.type(justificationField, "The solution test");

    await waitFor((): void => {
      expect(screen.getByText("Confirm")).not.toBeDisabled();
    });

    await userEvent.click(screen.getByText("Confirm"));

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "group.events.successRequestVerification",
        "groupAlerts.updatedTitle",
      );
    });
    jest.clearAllMocks();
  }, 120000);

  it("should handle error in request verification", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedMutations = [
      mockedVerified,
      graphqlMocked.mutation(
        REQUEST_EVENT_VERIFICATION_MUTATION,
        ({
          variables,
        }): StrictResponse<
          IErrorMessage | { data: RequestEventVerification }
        > => {
          const { comments, eventId, groupName } = variables;
          if (
            comments === "The solution test" &&
            eventId === "463457733" &&
            groupName === "unittesting"
          ) {
            return HttpResponse.json({
              errors: [
                new GraphQLError(
                  "Exception - The event has already been closed",
                ),
              ],
            });
          }

          return HttpResponse.json({
            data: {
              requestEventVerification: {
                success: true,
              },
            },
          });
        },
      ),
    ];

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_request_event_verification_mutate" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route element={<GroupEvents />} path={"/groups/:groupName/events"} />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/groups/unittesting/events"],
        },
        mocks: [mockedEvent, ...mockedMutations],
      },
    );
    await waitFor((): void => {
      expect(
        screen.queryByRole("cell", { name: "Network access issues" }),
      ).toBeInTheDocument();
    });

    expect(
      within(
        screen.getByRole("row", {
          name: /12314123 network access issues 2018-10-17 test description unsolved -/iu,
        }),
      ).getByRole("checkbox"),
    ).not.toBeChecked();

    await userEvent.click(
      within(
        screen.getByRole("row", {
          name: /12314123 network access issues 2018-10-17 test description unsolved -/iu,
        }),
      ).getByRole("checkbox"),
    );

    await waitFor((): void => {
      expect(
        within(
          screen.getByRole("row", {
            name: /12314123 network access issues 2018-10-17 test description unsolved -/iu,
          }),
        ).getByRole("checkbox"),
      ).toBeChecked();
    });

    expect(
      within(
        screen.getByRole("row", {
          name: /463457733 Authorization for a special attack 2018-10-17 Test description solved -/iu,
        }),
      ).getByRole("checkbox"),
    ).not.toBeChecked();

    await userEvent.click(
      within(
        screen.getByRole("row", {
          name: /463457733 Authorization for a special attack 2018-10-17 Test description solved -/iu,
        }),
      ).getByRole("checkbox"),
    );

    await waitFor((): void => {
      expect(screen.getAllByRole("checkbox", { checked: true })).toHaveLength(
        3,
      );
    });

    await userEvent.click(
      screen.getByRole("button", {
        name: /group.events.remediationmodal.btn.text/iu,
      }),
    );

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith("group.events.selectedError");
    });

    await waitFor((): void => {
      expect(screen.queryAllByRole("checkbox", { checked: true })).toHaveLength(
        2,
      );
    });

    jest.clearAllMocks();
  }, 120000);

  it("should update affected reattacks", async (): Promise<void> => {
    expect.hasAssertions();

    const dispatchRequest = jest.fn();

    const mockedQueries = [
      mockedEvent,
      graphqlMocked.query(
        GET_VERIFIED_FINDING_INFO,
        (): StrictResponse<{ data: GetVerifiedFindingInfo }> => {
          return HttpResponse.json({
            data: {
              group: {
                __typename: "Group",
                findings: [
                  {
                    __typename: "Finding",
                    id: "test-finding-id-01",
                    title: "038. Business information leak",
                    verified: false,
                  },
                  {
                    __typename: "Finding",
                    id: "test-finding-id-02",
                    title: "083. XML injection (XXE)",
                    verified: false,
                  },
                ],
                name: "unittesting",
              },
            },
          });
        },
      ),
      graphqlMocked.query(
        GET_FINDING_VULNS_TO_REATTACK,
        ({
          variables,
        }): StrictResponse<
          IErrorMessage | { data: GetFindingVulnsToReattack }
        > => {
          const { findingId } = variables;
          if (findingId === "test-finding-id-01") {
            return HttpResponse.json({
              data: {
                finding: {
                  __typename: "Finding",
                  id: "test-finding-id-01",
                  vulnerabilitiesToReattackConnection: {
                    edges: [
                      {
                        node: {
                          __typename: "Vulnerability",
                          findingId: "test-finding-id-01",
                          id: "test-vuln-id-01",
                          specific: "1111",
                          where: "vulnerable entrance-01",
                        },
                      },
                    ],
                    pageInfo: {
                      endCursor: "cursor",
                      hasNextPage: false,
                    },
                  },
                },
              },
            });
          }

          if (findingId === "test-finding-id-02") {
            return HttpResponse.json({
              data: {
                finding: {
                  __typename: "Finding",
                  id: "test-finding-id-02",
                  vulnerabilitiesToReattackConnection: {
                    edges: [
                      {
                        node: {
                          __typename: "Vulnerability",
                          findingId: "test-finding-id-02",
                          id: "test-vuln-id-02",
                          specific: "2222",
                          where: "vulnerable entrance-02",
                        },
                      },
                    ],
                    pageInfo: {
                      endCursor: "cursor",
                      hasNextPage: false,
                    },
                  },
                },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error getting vulnerabilities")],
          });
        },
      ),
    ];
    const mockedMutations = [
      graphqlMocked.mutation(
        REQUEST_VULNS_HOLD_MUTATION,
        ({
          variables,
        }): StrictResponse<
          IErrorMessage | { data: RequestVulnerabilitiesHoldMutation }
        > => {
          const { eventId, findingId, groupName, vulnerabilities } = variables;

          if (
            eventId === "12314123" &&
            findingId === "test-finding-id-01" &&
            groupName === "unittesting" &&
            isEqual(vulnerabilities, ["test-vuln-id-01"])
          ) {
            dispatchRequest();

            return HttpResponse.json({
              data: {
                requestVulnerabilitiesHold: {
                  __typename: "SimplePayload",
                  success: true,
                },
              },
            });
          }

          if (
            eventId === "12314123" &&
            findingId === "test-finding-id-02" &&
            groupName === "unittesting" &&
            isEqual(vulnerabilities, ["test-vuln-id-02"])
          ) {
            dispatchRequest();

            return HttpResponse.json({
              data: {
                requestVulnerabilitiesHold: {
                  __typename: "SimplePayload",
                  success: true,
                },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error requesting reattack holds")],
          });
        },
      ),
    ];
    const mockedPermissions = new PureAbility<string>([
      {
        action: "integrates_api_mutations_request_vulnerabilities_hold_mutate",
      },
    ]);

    jest.spyOn(console, "error").mockImplementation();

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route element={<GroupEvents />} path={"/groups/:groupName/events"} />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/groups/unittesting/events"],
        },
        mocks: [...mockedQueries, ...mockedMutations],
      },
    );
    await waitFor((): void => {
      expect(
        screen.getByRole("cell", { name: "Network access issues" }),
      ).toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(
        screen.getByRole("button", {
          name: "group.events.form.affectedReattacks.btn.text",
        }),
      ).toBeInTheDocument();
    });
    await userEvent.click(
      screen.getByRole("button", {
        name: "group.events.form.affectedReattacks.btn.text",
      }),
    );
    await waitFor((): void => {
      expect(
        screen.queryByText("group.events.form.affectedReattacks.title"),
      ).toBeInTheDocument();
    });

    expect(screen.getByText("buttons.confirm")).toBeDisabled();

    await selectOption("eventId", "12314123");
    await userEvent.click(screen.getByText("038. Business information leak"));
    await userEvent.click(screen.getByText("083. XML injection (XXE)"));

    expect(screen.queryAllByRole("checkbox", { checked: false })).toHaveLength(
      4,
    );

    await userEvent.click(screen.queryAllByRole("checkbox")[0]);
    await userEvent.click(screen.queryAllByRole("checkbox")[2]);

    expect(screen.queryAllByRole("checkbox", { checked: false })).toHaveLength(
      0,
    );

    expect(screen.getByText("buttons.confirm")).not.toBeDisabled();

    await userEvent.click(
      screen.getByRole("button", { name: "buttons.confirm" }),
    );
    await userEvent.click(
      screen.getByRole("button", { name: "buttons.confirm" }),
    );

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "group.events.form.affectedReattacks.holdsCreate",
        "group.events.titleSuccess",
      );
    });

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledTimes(0);
    });

    await waitFor((): void => {
      expect(dispatchRequest).toHaveBeenCalledTimes(2);
    });

    jest.clearAllMocks();
  });

  it("should render a error in update affected reattacks modal", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      {
        action: "integrates_api_mutations_request_vulnerabilities_hold_mutate",
      },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route element={<GroupEvents />} path={"/groups/:groupName/events"} />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/groups/unittesting/events"],
        },
        mocks: mockError,
      },
    );
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith("groupAlerts.errorTextsad");
    });

    jest.clearAllMocks();
  });

  it("should render empty events page", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedEventEmpty = graphqlMocked.query(
      GET_EVENTS,
      (): StrictResponse<{ data: GetEvents }> => {
        return HttpResponse.json({
          data: {
            group: {
              __typename: "Group",
              events: [],
              name: "unittesting",
            },
          },
        });
      },
    );
    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_add_event_mutate" },
    ]);
    const { rerender } = render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route element={<GroupEvents />} path={"/groups/:groupName/events"} />
        </Routes>
      </authzPermissionsContext.Provider>,

      {
        memoryRouter: {
          initialEntries: ["/groups/unittesting/events"],
        },
        mocks: [mockedEventEmpty, mockedRoots, mockedVerified],
      },
    );

    await waitFor((): void => {
      expect(
        screen.queryByText("group.events.empty.button"),
      ).toBeInTheDocument();
    });

    expect(screen.queryByText("group.events.empty.title")).toBeInTheDocument();

    rerender(
      <Routes>
        <Route element={<GroupEvents />} path={"/groups/:groupName/events"} />
      </Routes>,
    );
    await waitFor((): void => {
      expect(
        screen.queryByText("group.events.empty.button"),
      ).not.toBeInTheDocument();
    });
    jest.clearAllMocks();
  });
});
