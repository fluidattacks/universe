import { screen } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { Form, Formik } from "formik";
import { GraphQLError } from "graphql";
import { HttpResponse, type StrictResponse, graphql } from "msw";

import type { IFinding } from "./types";
import { GET_FINDING_VULNS_TO_REATTACK } from "./vulnerabilities-to-reattack-table/queries";

import { AffectedReattackAccordion } from ".";
import type { GetFindingVulnsToReattackQuery as GetFindingVulnsToReattack } from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";

describe("affected Reattack accordion", (): void => {
  it("should render accordion component", async (): Promise<void> => {
    expect.hasAssertions();

    const testFindings: IFinding[] = [
      {
        id: "test-finding-id",
        title: "038. Business information leak",
        verified: false,
      },
    ];

    const mocks = [
      graphql
        .link(LINK)
        .query(
          GET_FINDING_VULNS_TO_REATTACK,
          ({
            variables,
          }): StrictResponse<
            IErrorMessage | { data: GetFindingVulnsToReattack }
          > => {
            const { findingId } = variables;
            if (findingId === "test-finding-id") {
              return HttpResponse.json({
                data: {
                  finding: {
                    __typename: "Finding",
                    id: "test-finding-id",
                    vulnerabilitiesToReattackConnection: {
                      edges: [
                        {
                          node: {
                            __typename: "Vulnerability",
                            findingId: "test-finding-id",
                            id: "test-vuln-id",
                            specific: "9999",
                            where: "vulnerable entrance",
                          },
                        },
                      ],
                      pageInfo: {
                        endCursor: "cursor",
                        hasNextPage: false,
                      },
                    },
                  },
                },
              });
            }

            return HttpResponse.json({
              errors: [new GraphQLError("Error getting vulnerabilities")],
            });
          },
        ),
    ];

    render(
      <Formik initialValues={{ affectedReattacks: [] }} onSubmit={jest.fn()}>
        <Form name={""}>
          <AffectedReattackAccordion findings={testFindings} />
        </Form>
      </Formik>,
      { mocks },
    );

    expect(
      screen.queryByText("038. Business information leak"),
    ).toBeInTheDocument();

    await userEvent.click(screen.getByText("038. Business information leak"));

    expect(screen.queryByRole("table")).toBeInTheDocument();
  });
});
