import { Accordion } from "@fluidattacks/design";
import { ErrorMessage as Message } from "formik";

import type { IAffectedAccordionProps } from "./types";
import { VulnerabilitiesToReattackTable } from "./vulnerabilities-to-reattack-table";

import { ErrorMessage } from "components/input/styles";

const AffectedReattackAccordion = ({
  findings,
}: IAffectedAccordionProps): JSX.Element => {
  const panelOptions = findings.map((finding): JSX.Element => {
    if (finding.verified) {
      return <div key={finding.id} />;
    }

    return (
      <Accordion
        items={[
          {
            description: <VulnerabilitiesToReattackTable finding={finding} />,
            title: finding.title,
          },
        ]}
        key={finding.id}
      />
    );
  });

  return (
    <div>
      {panelOptions}
      <ErrorMessage $show={true}>
        <Message name={"affectedReattacks"} />
      </ErrorMessage>
    </div>
  );
};

export { AffectedReattackAccordion };
