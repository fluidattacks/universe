import type { ExecutionResult } from "graphql";

interface IReattackVuln {
  affected?: boolean;
  findingId: string;
  id: string;
  specific: string;
  where: string;
}

interface IFinding {
  id: string;
  title: string;
  verified: boolean;
}

interface IAffectedReattackModal {
  findings: IReattackVuln[];
  clearSelected: () => void;
  handleCloseModal: () => void;
  setRequestState: () => void;
}

interface IAffectedAccordionProps {
  findings: IFinding[];
}

interface IRequestVulnerabilitiesHold {
  requestVulnerabilitiesHold: {
    success: boolean;
  };
}

type TRequestVulnerabilitiesHoldResult =
  ExecutionResult<IRequestVulnerabilitiesHold>;

export type {
  IAffectedAccordionProps,
  IReattackVuln,
  IFinding,
  IAffectedReattackModal,
  TRequestVulnerabilitiesHoldResult,
};
