import { useQuery } from "@apollo/client";
import type { ColumnDef } from "@tanstack/react-table";
import { useFormikContext } from "formik";
import _, { isEmpty } from "lodash";
import { useEffect, useState } from "react";
import * as React from "react";

import { GET_FINDING_VULNS_TO_REATTACK } from "./queries";
import type {
  IVulnerabilitiesToReattackTableProps,
  IVulnerabilityAttr,
} from "./types";

import type { IFormValues } from "../../add-modal";
import { Table } from "components/table";
import { useTable } from "hooks";
import { Logger } from "utils/logger";

const VulnerabilitiesToReattackTable: React.FC<
  IVulnerabilitiesToReattackTableProps
> = ({ finding }): JSX.Element => {
  const tableRef = useTable(finding.id);
  const { setFieldValue, setFieldTouched, values } =
    useFormikContext<IFormValues>();

  const { data, fetchMore } = useQuery(GET_FINDING_VULNS_TO_REATTACK, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.error("Couldn't load vulns to reattack", error);
      });
    },
    variables: {
      findingId: finding.id,
    },
  });
  const vulnsToReattackConnection =
    data === undefined
      ? undefined
      : data.finding.vulnerabilitiesToReattackConnection;
  const pageInfo =
    vulnsToReattackConnection === undefined
      ? undefined
      : vulnsToReattackConnection.pageInfo;
  const vulnsToReattackEdges =
    vulnsToReattackConnection === undefined
      ? []
      : vulnsToReattackConnection.edges;
  const vulnsToReattack = vulnsToReattackEdges.map(
    (vulnEdge): IVulnerabilityAttr =>
      vulnEdge?.node ?? {
        __typename: undefined,
        findingId: "",
        id: "",
        specific: "",
        where: "",
      },
  );

  const [selectedVulns, setSelectedVulns] = useState<IVulnerabilityAttr[]>([]);

  const newAffectedReattacks = (selectedIds: string[]): string[] => {
    // eslint-disable-next-line @typescript-eslint/prefer-destructuring
    const { findingId } = vulnsToReattack[0];
    const currentIds = values.affectedReattacks
      .filter(
        (id): boolean => selectedIds.includes(id) || !id.startsWith(findingId),
      )
      .concat(
        selectedIds.filter(
          (id): boolean => !values.affectedReattacks.includes(id),
        ),
      );

    return currentIds;
  };

  useEffect((): void => {
    if (!_.isUndefined(pageInfo)) {
      if (pageInfo.hasNextPage) {
        void fetchMore({
          variables: { after: pageInfo.endCursor },
        });
      }
    }
  }, [pageInfo, fetchMore]);

  const columns: ColumnDef<IVulnerabilityAttr>[] = [
    { accessorKey: "where", header: "Where" },
    { accessorKey: "specific", header: "Specific" },
  ];

  useEffect((): void => {
    void setFieldTouched("affectedReattacks", true);
    const selectedIds = selectedVulns.map(
      (vuln): string => `${vuln.findingId} ${vuln.id}`,
    );

    if (!isEmpty(vulnsToReattack)) {
      void setFieldValue(
        "affectedReattacks",
        newAffectedReattacks(selectedIds),
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedVulns]);

  return (
    <Table
      columns={columns}
      data={vulnsToReattack}
      rowSelectionSetter={setSelectedVulns}
      rowSelectionState={selectedVulns}
      tableRef={tableRef}
    />
  );
};

export { VulnerabilitiesToReattackTable };
