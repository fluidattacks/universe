import { useAbility } from "@casl/react";
import { EmptyState } from "@fluidattacks/design";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";

import type { IFormValues } from "../add-modal";
import { AddModal } from "../add-modal";
import { authzPermissionsContext } from "context/authz/config";
import type { IUseModal } from "hooks/use-modal";

interface IEventsEmptyProps {
  readonly onSubmit: (values: IFormValues) => Promise<void>;
  readonly modalRef: IUseModal;
}

const EventsEmpty: React.FC<IEventsEmptyProps> = ({
  modalRef,
  onSubmit,
}): JSX.Element => {
  const { t } = useTranslation();
  const { open } = modalRef;
  const { groupName, organizationName } = useParams() as {
    groupName: string;
    organizationName: string;
  };

  const permissions = useAbility(authzPermissionsContext);
  const canAddEvent = permissions.can(
    "integrates_api_mutations_add_event_mutate",
  );

  return (
    <React.Fragment>
      <AddModal
        groupName={groupName}
        modalRef={modalRef}
        onSubmit={onSubmit}
        organizationName={organizationName}
      />
      <EmptyState
        confirmButton={
          canAddEvent
            ? {
                onClick: open,
                text: t("group.events.empty.button"),
              }
            : undefined
        }
        description={t("group.events.empty.description")}
        imageSrc={"integrates/empty/eventsIcon"}
        title={t("group.events.empty.title")}
      />
    </React.Fragment>
  );
};

export { EventsEmpty };
