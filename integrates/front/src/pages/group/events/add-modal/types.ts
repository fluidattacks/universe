import type { Dayjs } from "dayjs";

import type { IUseModal } from "hooks/use-modal";

interface IFormValues {
  eventDate: Dayjs | string;
  affectsReattacks: boolean;
  affectedReattacks: string[];
  eventType: string;
  detail: string;
  files?: FileList;
  images?: FileList;
  rootId: string;
  environmentUrl: string;
  rootNickname: string;
}

interface IAddModalProps {
  groupName: string;
  modalRef: IUseModal;
  organizationName: string;
  onSubmit: (values: IFormValues) => Promise<void>;
}

interface IAddModalFormProps {
  dirty: boolean;
  isSubmitting: boolean;
  values: {
    affectedReattacks: never[];
    affectsReattacks: boolean;
    detail: string;
    eventDate: string;
    eventType: string;
    files: undefined;
    images: undefined;
    rootId: string;
    environmentUrl: string;
    rootNickname: string;
  };
  setFieldValue: (
    field: string,
    value: boolean,
    shouldValidate?: boolean,
  ) => void;
  nicknames: string[];
  groupName: string;
  onClose: () => void;
}

interface IListItemProps {
  label: string;
  value: string;
}

interface IUrlObject {
  url: string;
}

interface IRoot {
  gitEnvironmentUrls: IUrlObject[];
}

interface IRootEnvironmentUrlsData {
  root: IRoot;
}

export type {
  IAddModalProps,
  IAddModalFormProps,
  IFormValues,
  IListItemProps,
  IUrlObject,
  IRoot,
  IRootEnvironmentUrlsData,
};
