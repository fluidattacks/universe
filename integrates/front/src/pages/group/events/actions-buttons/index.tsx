import { Button } from "@fluidattacks/design";
import { Fragment } from "react";
import { useTranslation } from "react-i18next";

import type { IActionsButtonsProps } from "./types";

import { ExportCsv } from "components/table/export-csv";
import { ExportMultipleCsv } from "components/table/export-multiple-csv";
import { Can } from "context/authz/can";

const ActionsButtons = ({
  closeOpenMode,
  isThereEventsThatAffectsReattacks,
  isOpenMode,
  data,
  filteredData,
  openAddModal,
  openUpdateAffectedModal,
}: IActionsButtonsProps): JSX.Element => {
  const { t } = useTranslation();

  const isEqualData = data.length === filteredData.length;

  return (
    <Fragment>
      {isEqualData ? (
        <ExportCsv csvConfig={{ export: true }} data={data}>
          <Button
            icon={"file-export"}
            id={"CSVExportBtn"}
            tooltip={t("buttons.exportTip", { format: "CSV" })}
            variant={"ghost"}
          >
            {t("group.findings.exportCsv.text")}
          </Button>
        </ExportCsv>
      ) : (
        <ExportMultipleCsv data={data} filteredData={filteredData} />
      )}
      {isOpenMode ? undefined : (
        <Can do={"integrates_api_mutations_add_event_mutate"}>
          <Button
            icon={"add"}
            id={"group.events.btn.id"}
            onClick={openAddModal}
            tooltip={t("group.events.btn.tooltip")}
            variant={"ghost"}
          >
            {t("group.events.btn.text")}
          </Button>
        </Can>
      )}
      {isOpenMode ? undefined : (
        <Can
          do={"integrates_api_mutations_request_vulnerabilities_hold_mutate"}
        >
          <Button
            disabled={!isThereEventsThatAffectsReattacks}
            icon={"arrow-rotate-right"}
            id={"group.events.form.affectedReattacks.btn.id"}
            onClick={openUpdateAffectedModal}
            tooltip={t("group.events.form.affectedReattacks.btn.tooltip")}
            variant={"ghost"}
          >
            {t("group.events.form.affectedReattacks.btn.text")}
          </Button>
        </Can>
      )}
      {isOpenMode ? (
        <Button
          icon={"close"}
          id={"searchFindings.tabVuln.buttonsTooltip.cancelVerify.id"}
          onClick={closeOpenMode}
          tooltip={t("searchFindings.tabVuln.buttonsTooltip.cancel")}
          variant={"secondary"}
        >
          {t("searchFindings.tabDescription.cancelVerified")}
        </Button>
      ) : undefined}
    </Fragment>
  );
};

export { ActionsButtons };
