import type { ApolloError } from "@apollo/client";
import _ from "lodash";

import type { TRequestVulnerabilitiesHoldResult } from "./affected-reattack-accordion/types";
import type { IEventAttr, IEventData } from "./types";

import { castEventStatus, castEventType } from "utils/format-helpers";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";
import { translate } from "utils/translations/translate";

const handleCreationError = (creationError: ApolloError): void => {
  creationError.graphQLErrors.forEach(({ message }): void => {
    switch (message) {
      case "Exception - Invalid file size":
        msgError(translate.t("validations.fileSize", { count: 10 }));
        break;
      case "Exception - Invalid characters in filename":
        msgError(translate.t("validations.invalidFileName"));
        break;
      case "Exception - Invalid file type: EVENT_IMAGE":
        msgError(translate.t("group.events.form.wrongImageType"));
        break;
      case "Exception - Invalid file type: EVENT_FILE":
        msgError(translate.t("group.events.form.wrongFileType"));
        break;
      case "Exception - Invalid characters":
        msgError(translate.t("validations.invalidChar"));
        break;
      case "Exception - Invalid file name: Format organizationName-groupName-10 alphanumeric chars.extension":
        msgError(translate.t("group.events.form.wrongImageName"));
        break;
      case "Exception - Unsanitized input found":
        msgError(translate.t("validations.unsanitizedInputFound"));
        break;
      default:
        msgError(translate.t("groupAlerts.errorTextsad"));
        Logger.warning(
          "An error occurred updating event evidence",
          creationError,
        );
    }
  });
};

const handleRequestHoldError = (holdError: ApolloError): void => {
  holdError.graphQLErrors.forEach(({ message }): void => {
    switch (message) {
      case "Exception - The event has already been closed":
        msgError(translate.t("group.events.alreadyClosed"));
        break;
      case "Exception - Request verification already on hold":
        msgError(
          translate.t("group.events.form.affectedReattacks.alreadyOnHold"),
        );
        break;
      case "Invalid, type vulnerability LINES cannot be associated":
        msgError(
          translate.t("group.events.form.affectedReattacks.invalidAssociation"),
        );
        break;
      case "Exception - The vulnerability has already been closed":
        msgError(
          translate.t("group.events.form.affectedReattacks.alreadyClosed"),
        );
        break;
      default:
        msgError(translate.t("groupAlerts.errorTextsad"));
        Logger.warning(
          "An error occurred requesting reattack holds",
          holdError,
        );
    }
  });
};

const handleRequestVerificationError = (error: ApolloError): void => {
  error.graphQLErrors.forEach(({ message }): void => {
    switch (message) {
      case "Exception - The event has already been closed":
        msgError(translate.t("group.events.alreadyClosed"));
        break;
      case "Exception - The event verification has been requested":
        msgError(translate.t("group.events.verificationAlreadyRequested"));
        break;
      default:
        msgError(translate.t("groupAlerts.errorTextsad"));
        Logger.warning(
          "An error occurred requesting event verification",
          error,
        );
    }
  });
};

const handleRequestHoldsHelper = async (
  requestHold: (
    variables: Record<string, unknown>,
  ) => Promise<TRequestVulnerabilitiesHoldResult>,
  formattedReattacks: Record<string, string[]>,
  eventId: string,
  groupName: string,
): Promise<boolean> => {
  const requestedHolds = Object.entries(formattedReattacks).map(
    async ([findingId, vulnIds]): Promise<
      TRequestVulnerabilitiesHoldResult[]
    > => {
      return Promise.all([
        requestHold({
          variables: {
            eventId,
            findingId,
            groupName,
            vulnerabilities: vulnIds,
          },
        }),
      ]);
    },
  );

  const holdResults = await Promise.all(requestedHolds);

  return holdResults.every((currentResult): boolean => {
    return currentResult[0].data?.requestVulnerabilitiesHold.success ?? false;
  });
};

const getNonSelectableEventIndexToRequestVerification = (
  allEvents: IEventData[],
): number[] => {
  const unsolved = translate.t(castEventStatus("CREATED").status);

  return allEvents.reduce(
    (
      selectedEventIndex: number[],
      currentEventData: IEventData,
      currentEventDataIndex: number,
    ): number[] =>
      currentEventData.eventStatus === unsolved
        ? selectedEventIndex
        : [...selectedEventIndex, currentEventDataIndex],
    [],
  );
};

const formatEvents = (dataset: IEventAttr[]): IEventData[] =>
  dataset.map((event): IEventData => {
    const eventType = translate.t(castEventType(event.eventType));
    const eventStatus = translate.t(castEventStatus(event.eventStatus).status);

    return {
      ...event,
      eventStatus,
      eventType,
    };
  });

function formatReattacks(reattacks: string[]): Record<string, string[]> {
  if (reattacks.length > 0) {
    return (
      _.chain(reattacks)
        // CompositeId = "findingId vulnId"
        .groupBy((compositeId): string => {
          // First group by findingId
          return compositeId.split(" ")[0];
        })

        // Then map key-value pairs to look like findingId: [vulnId1, ...]
        .mapValues((compositeArray): string[] => {
          return compositeArray.map(
            (compositeId): string => compositeId.split(" ")[1],
          );
        })
        .value()
    );
  }

  return {};
}

export {
  formatEvents,
  formatReattacks,
  getNonSelectableEventIndexToRequestVerification,
  handleCreationError,
  handleRequestHoldError,
  handleRequestHoldsHelper,
  handleRequestVerificationError,
};
