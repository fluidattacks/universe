import type { EventType } from "gql/graphql";

interface IRootAttr {
  id: string;
  nickname: string;
}

interface IEventAttr {
  closingDate: string | null;
  detail: string;
  eventDate: string;
  eventStatus: string;
  eventType: EventType;
  id: string;
  groupName: string;
  root: IRootAttr | null;
  environment: string | null;
}

interface IEventData {
  closingDate: string | null;
  detail: string;
  eventDate: string;
  eventStatus: string;
  eventType: string;
  id: string;
  groupName: string;
  root: IRootAttr | null;
  environment: string | null;
}

interface IEventsDataset {
  group: {
    events: IEventAttr[];
  };
}

export type { IEventAttr, IEventData, IEventsDataset };
