/* eslint-disable max-lines */
import { useMutation, useQuery } from "@apollo/client";
import type { FetchResult } from "@apollo/client";
import { useAbility } from "@casl/react";
import {
  AppliedFilters,
  Button,
  Container,
  Loading,
  useModal,
} from "@fluidattacks/design";
import type { ColumnDef, Row } from "@tanstack/react-table";
import dayjs, { extend } from "dayjs";
import utc from "dayjs/plugin/utc";
import _ from "lodash";
import mixpanel from "mixpanel-browser";
import { useCallback, useMemo, useState } from "react";
import * as React from "react";
import type { FormEvent } from "react";
import { useTranslation } from "react-i18next";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import { useTheme } from "styled-components";

import { ActionsButtons } from "./actions-buttons";
import { AddModal } from "./add-modal";
import type { IFormValues } from "./add-modal/types";
import { GET_VERIFIED_FINDING_INFO } from "./affected-reattack-accordion/queries";
import type { IFinding } from "./affected-reattack-accordion/types";
import { EventsEmpty } from "./empty";
import { useEventsQuery } from "./hooks";
import {
  ADD_EVENT_MUTATION,
  REQUEST_EVENT_VERIFICATION_MUTATION,
  REQUEST_VULNS_HOLD_MUTATION,
} from "./queries";
import type { IEventData, IEventsDataset } from "./types";
import { UpdateAffectedModal } from "./update-affected-modal";
import type { IUpdateAffectedValues } from "./update-affected-modal/types";
import {
  formatReattacks,
  getNonSelectableEventIndexToRequestVerification,
  handleCreationError,
  handleRequestHoldError,
  handleRequestHoldsHelper,
} from "./utils";

import { Authorize } from "components/@core/authorize";
import { Table } from "components/table";
import { filterDate } from "components/table/utils";
import { authzPermissionsContext } from "context/authz/config";
import { useEventsFilters } from "features/events/filters";
import { RemediationModal } from "features/remediation-modal";
import type { IRemediationValues } from "features/remediation-modal/types";
import { statusFormatter } from "features/vulnerabilities/formatters/status";
import { handleRequestVerificationError } from "features/vulnerabilities/update-verification-modal/utils";
import type {
  EventEvidenceType,
  EventType,
  RequestEventVerificationMutation,
} from "gql/graphql";
import { useTable } from "hooks";
import { UPDATE_EVIDENCE_MUTATION } from "pages/event/evidence/queries";
import type { IUpdateEventEvidenceResultAttr } from "pages/event/evidence/types";
import { formatDate } from "utils/date";
import { castEventStatus } from "utils/format-helpers";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";
import { translate } from "utils/translations/translate";

const GroupEvents: React.FC = (): JSX.Element => {
  const navigate = useNavigate();
  const { groupName, organizationName } = useParams() as {
    groupName: string;
    organizationName: string;
  };

  const { pathname } = useLocation();
  const { t } = useTranslation();
  const theme = useTheme();
  const [filteredDataset, setFilteredDataset] = useState<IEventData[]>([]);

  const permissions = useAbility(authzPermissionsContext);
  const canRequestVerification = permissions.can(
    "integrates_api_mutations_request_event_verification_mutate",
  );

  const { data, events, loading, refetch } = useEventsQuery(groupName);
  const { Filters, options, removeFilter } = useEventsFilters({
    dataset: events,
    groupName,
    setFilteredDataset,
  });

  const hasUnsolvedEvents = events.some(
    (event): boolean => event.eventStatus.toUpperCase() !== "SOLVED",
  );

  const columns = useMemo(
    (): ColumnDef<IEventData>[] => [
      {
        accessorKey: "id",
        cell: (cell): string => cell.getValue<string>().split("-")[0],
        header: t("searchFindings.tabEvents.id"),
      },
      {
        accessorFn: (row): string | undefined => {
          const nickname = row.root?.nickname ?? undefined;
          const environment = row.environment ?? undefined;

          if (nickname === undefined && environment === undefined) {
            return undefined;
          }
          if (environment === undefined) {
            return nickname;
          }

          return `${nickname} - ${environment}`;
        },
        header: String(t("searchFindings.tabEvents.root")),
        id: "root",
      },
      {
        accessorKey: "eventType",
        header: t("searchFindings.tabEvents.type"),
      },
      {
        accessorKey: "eventDate",
        cell: (cell): string => formatDate(String(cell.getValue())),
        filterFn: filterDate,
        header: t("searchFindings.tabEvents.date"),
      },
      {
        accessorKey: "detail",
        header: t("searchFindings.tabEvents.description"),
      },
      {
        accessorKey: "eventStatus",
        cell: (cell): JSX.Element => statusFormatter(String(cell.getValue())),
        header: t("searchFindings.tabEvents.status"),
      },
      {
        accessorKey: "closingDate",
        cell: (cell): string => formatDate(String(cell.getValue())),
        header: t("searchFindings.tabEvents.dateClosed"),
      },
    ],
    [t],
  );

  const tableRef = useTable(
    "tblEvents",
    {
      Assignees: false,
      Locations: false,
      Treatment: false,
      description: false,
      reattack: false,
      releaseDate: false,
    },
    [
      "id",
      "root",
      "eventType",
      "eventDate",
      "detail",
      "eventStatus",
      "closingDate",
    ],
  );

  function enabledRows(row: Row<IEventData>): boolean {
    const indexes = getNonSelectableEventIndexToRequestVerification(events);
    const nonselectables = indexes.map((index): IEventData => events[index]);

    return !nonselectables.includes(row.original);
  }

  const goToEvent = useCallback(
    (rowInfo: Row<IEventData>): ((event: FormEvent) => void) => {
      return (event: FormEvent): void => {
        mixpanel.track("ReadEvent");
        navigate(`${pathname}/${rowInfo.original.id}/description`);
        event.preventDefault();
      };
    },
    [navigate, pathname],
  );

  // State Management
  const [selectedEvents, setSelectedEvents] = useState<IEventData[]>([]);
  const unsolved = translate.t(castEventStatus("CREATED").status);
  const selectedUnsolvedEvents = selectedEvents.filter(
    (event): boolean => event.eventStatus === unsolved,
  );

  const [selectedReattacks, setSelectedReattacks] = useState({});
  const [remediationValues, setRemediationValues] =
    useState<IRemediationValues>({
      treatmentJustification: "",
    });
  const addEventModal = useModal("add-event-modal");
  const updateAffectedModal = useModal("update-affected-modal");
  const requestVerificationModal = useModal("remediation-modal");

  const [isOpenRequestVerificationMode, setIsOpenRequestVerificationMode] =
    useState(false);
  const openRequestVerificationMode = useCallback((): void => {
    if (
      selectedUnsolvedEvents.length === selectedEvents.length &&
      selectedEvents.length > 0
    ) {
      requestVerificationModal.open();
    } else {
      msgError(t("group.events.selectedError"));
      setSelectedEvents(selectedUnsolvedEvents);
    }

    setIsOpenRequestVerificationMode(true);
  }, [t, requestVerificationModal, selectedUnsolvedEvents, selectedEvents]);
  const closeRequestVerificationMode = useCallback((): void => {
    setIsOpenRequestVerificationMode(false);
    requestVerificationModal.close();
  }, [requestVerificationModal]);

  const closeOpenMode = useCallback((): void => {
    closeRequestVerificationMode();
  }, [closeRequestVerificationMode]);

  const { data: findingsData, refetch: refetchReattacks } = useQuery(
    GET_VERIFIED_FINDING_INFO,
    {
      onError: ({ graphQLErrors }): void => {
        graphQLErrors.forEach((error): void => {
          Logger.error("Couldn't load reattack vulns", error);
        });
      },
      variables: { groupName },
    },
  );
  const findings = findingsData?.group.findings ?? [];
  const hasReattacks = findings.some(
    (finding): boolean => finding !== null && !finding.verified,
  );

  const [requestHold] = useMutation(REQUEST_VULNS_HOLD_MUTATION, {
    onError: handleRequestHoldError,
  });

  const [requestVerification] = useMutation(
    REQUEST_EVENT_VERIFICATION_MUTATION,
    {
      onError: ({ graphQLErrors }): void => {
        graphQLErrors.forEach((error): void => {
          handleRequestVerificationError(error);
        });
      },
    },
  );

  const [addEvent] = useMutation(ADD_EVENT_MUTATION, {
    onError: handleCreationError,
  });

  const [updateEvidence] = useMutation(UPDATE_EVIDENCE_MUTATION);

  const handleUpdateEvidence = useCallback(
    (eventId: string, files: FileList | undefined): void => {
      if (!_.isUndefined(files) && !_.isUndefined(files[0])) {
        void updateEvidence({
          variables: {
            eventId,
            evidenceType: "FILE_1" as EventEvidenceType,
            file: files[0],
            groupName,
          },
        });
      }
    },
    [groupName, updateEvidence],
  );

  const handleSubmit = useCallback(
    async (values: IFormValues): Promise<void> => {
      const {
        affectsReattacks,
        affectedReattacks,
        detail,
        environmentUrl,
        eventDate,
        eventType,
        images,
        files,
        rootId,
      } = values;
      extend(utc);
      const selectedEventReattacks = formatReattacks(affectedReattacks);
      const environment =
        environmentUrl === "No related environment" ? "" : environmentUrl;
      const result = await addEvent({
        variables: {
          detail,
          environmentUrl: environment,
          eventDate: dayjs(eventDate).utc(false).format(),
          eventType: eventType as EventType,
          groupName,
          rootId,
        },
      });
      addEventModal.close();
      if (!_.isNil(result.data) && result.data.addEvent.success) {
        const { eventId } = result.data.addEvent;
        if (!_.isUndefined(images)) {
          void [...Array(images.length).keys()].map(
            async (
              index: number,
            ): Promise<
              FetchResult<IUpdateEventEvidenceResultAttr> | undefined
            > =>
              _.isUndefined(images[index])
                ? undefined
                : updateEvidence({
                    variables: {
                      eventId,
                      evidenceType: `IMAGE_${index + 1}` as EventEvidenceType,
                      file: images[index],
                      groupName,
                    },
                  }),
          );
        }

        handleUpdateEvidence(eventId, files);

        if (affectsReattacks && !_.isEmpty(selectedEventReattacks)) {
          const allHoldsValid = await handleRequestHoldsHelper(
            requestHold,
            selectedEventReattacks,
            eventId,
            groupName,
          );

          if (allHoldsValid) {
            msgSuccess(
              t("group.events.form.affectedReattacks.holdsCreate"),
              t("group.events.titleSuccess"),
            );
          }
        }
        msgSuccess(
          t("group.events.successCreate"),
          t("group.events.titleSuccess"),
        );

        await refetch();
        await refetchReattacks();
      }
    },
    [
      addEvent,
      addEventModal,
      groupName,
      handleUpdateEvidence,
      refetch,
      refetchReattacks,
      requestHold,
      t,
      updateEvidence,
    ],
  );

  const handleUpdateAffectedSubmit = useCallback(
    async (values: IUpdateAffectedValues): Promise<void> => {
      setSelectedReattacks(formatReattacks(values.affectedReattacks));

      if (!_.isEmpty(selectedReattacks)) {
        const allHoldsValid = await handleRequestHoldsHelper(
          requestHold,
          selectedReattacks,
          values.eventId,
          groupName,
        );

        if (allHoldsValid) {
          msgSuccess(
            t("group.events.form.affectedReattacks.holdsCreate"),
            t("group.events.titleSuccess"),
          );
        }

        updateAffectedModal.close();
        await refetchReattacks();
      }
    },
    [
      updateAffectedModal,
      refetchReattacks,
      requestHold,
      groupName,
      selectedReattacks,
      t,
    ],
  );

  const handleRequestVerification = useCallback(
    async (values: IRemediationValues): Promise<void> => {
      closeOpenMode();
      setRemediationValues(values);
      const results = await Promise.all(
        selectedEvents.map(
          async (
            event: IEventData,
          ): Promise<FetchResult<RequestEventVerificationMutation>> =>
            requestVerification({
              variables: {
                comments: values.treatmentJustification,
                eventId: event.id,
                groupName,
              },
            }),
        ),
      );
      await refetch();

      if (!_.isEmpty(results)) {
        if (
          !_.isNil(results[0].data) &&
          results[0].data.requestEventVerification.success
        ) {
          msgSuccess(
            t("group.events.successRequestVerification"),
            t("groupAlerts.updatedTitle"),
          );
        }
      }
      setSelectedEvents([]);
    },

    [selectedEvents, refetch, requestVerification, groupName, t, closeOpenMode],
  );

  const isOpenMode = isOpenRequestVerificationMode;
  const isUpdateAffectedReattackEnable = hasReattacks && hasUnsolvedEvents;

  const onClickRequestEventVerification = isOpenRequestVerificationMode
    ? requestVerificationModal.open
    : openRequestVerificationMode;

  if (loading) {
    return (
      <Container
        alignItems={"center"}
        display={"flex"}
        height={"100%"}
        justify={"center"}
      >
        <Loading color={"red"} label={"Loading..."} size={"lg"} />
      </Container>
    );
  }

  if (events.length === 0) {
    return <EventsEmpty modalRef={addEventModal} onSubmit={handleSubmit} />;
  }

  return (
    <Container
      bgColor={theme.palette.gray[50]}
      height={"100hv"}
      id={"users"}
      px={1.25}
      py={1.25}
    >
      <AddModal
        groupName={groupName}
        modalRef={addEventModal}
        onSubmit={handleSubmit}
        organizationName={organizationName}
      />
      <UpdateAffectedModal
        eventsInfo={data as IEventsDataset}
        findings={findings as IFinding[]}
        modalRef={updateAffectedModal}
        onSubmit={handleUpdateAffectedSubmit}
      />
      <RemediationModal
        isLoading={false}
        message={t("group.events.remediationModal.justification")}
        modalRef={{
          ...requestVerificationModal,
          close: closeRequestVerificationMode,
        }}
        onSubmit={handleRequestVerification}
        remediationType={"verifyEvent"}
        title={t("group.events.remediationModal.titleRequest")}
        values={remediationValues}
      />
      <Table
        columns={columns}
        data={filteredDataset}
        extraButtons={
          <ActionsButtons
            closeOpenMode={closeOpenMode}
            data={events}
            filteredData={filteredDataset}
            isOpenMode={isOpenMode}
            isThereEventsThatAffectsReattacks={isUpdateAffectedReattackEnable}
            onClickRequestEventVerification={onClickRequestEventVerification}
            openAddModal={addEventModal.open}
            openUpdateAffectedModal={updateAffectedModal.open}
            selectedUnsolvedEvents={selectedUnsolvedEvents}
          />
        }
        filters={<Filters />}
        filtersApplied={
          <AppliedFilters onClose={removeFilter} options={options} />
        }
        loadingData={loading}
        onRowClick={goToEvent}
        options={{
          columnToggle: true,
          enableRowSelection: isOpenRequestVerificationMode
            ? enabledRows
            : undefined,
        }}
        rightSideComponents={
          <Authorize
            can={"integrates_api_mutations_request_event_verification_mutate"}
          >
            <Button
              disabled={_.isEmpty(selectedUnsolvedEvents)}
              icon={"check"}
              id={"group.events.remediationModal.btn.id"}
              onClick={onClickRequestEventVerification}
              tooltip={t("group.events.remediationModal.btn.tooltip")}
              variant={"primary"}
            >
              {t("group.events.remediationModal.btn.text")}
            </Button>
          </Authorize>
        }
        rowSelectionSetter={
          canRequestVerification ? setSelectedEvents : undefined
        }
        rowSelectionState={selectedEvents}
        tableRef={tableRef}
      />
    </Container>
  );
};

export type { IEventsDataset };
export { GroupEvents };
