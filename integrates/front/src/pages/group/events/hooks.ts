import type { ApolloQueryResult } from "@apollo/client";
import { useQuery } from "@apollo/client";
import { useMemo } from "react";
import { useTranslation } from "react-i18next";

import { GET_EVENTS } from "./queries";
import type { IEventData } from "./types";
import { formatEvents } from "./utils";

import type { GetEventsQuery } from "gql/graphql";
import { useAudit } from "hooks/use-audit";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";

interface IUseEventsQuery {
  data: GetEventsQuery | undefined;
  events: IEventData[];
  loading: boolean;
  refetch: () => Promise<ApolloQueryResult<GetEventsQuery>>;
}

const useEventsQuery = (groupName: string): IUseEventsQuery => {
  const { addAuditEvent } = useAudit();
  const { t } = useTranslation();

  const { data, loading, refetch } = useQuery(GET_EVENTS, {
    onCompleted: (): void => {
      addAuditEvent("Group.Events", groupName);
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.warning("An error occurred loading group data", error);
        msgError(t("groupAlerts.errorTextsad"));
      });
    },
    variables: { groupName },
  });

  const events = useMemo(
    (): IEventData[] => formatEvents(data?.group.events ?? []),
    [data],
  );

  return { data, events, loading, refetch };
};

export { useEventsQuery };
