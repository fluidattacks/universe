import type { IEventsDataset } from "..";
import type { IFinding } from "../affected-reattack-accordion/types";
import type { IUseModal } from "hooks/use-modal";

interface IUpdateAffectedValues {
  eventId: string;
  affectedReattacks: string[];
}

interface IUpdateAffectedModalProps {
  eventsInfo: IEventsDataset | undefined;
  findings: IFinding[];
  modalRef: IUseModal;
  onSubmit: (values: IUpdateAffectedValues) => Promise<void>;
}

export type { IUpdateAffectedValues, IUpdateAffectedModalProps };
