import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql/error";
import { HttpResponse, type StrictResponse, graphql } from "msw";

import { UpdateAffectedModal } from ".";
import type { IFinding } from "../affected-reattack-accordion/types";
import { GET_FINDING_VULNS_TO_REATTACK } from "../affected-reattack-accordion/vulnerabilities-to-reattack-table/queries";
import type { IEventsDataset } from "../types";
import { CustomThemeProvider } from "components/colors";
import {
  EventType,
  type GetFindingVulnsToReattackQuery as GetFindingVulnsToReattack,
} from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";

describe("updateAffectedModal", (): void => {
  const testFindings: IFinding[] = [
    {
      id: "test-finding-id",
      title: "038. Business information leak",
      verified: false,
    },
  ];

  const testEventsInfo: IEventsDataset = {
    group: {
      events: [
        {
          closingDate: "-",
          detail: "Test description",
          environment: null,
          eventDate: "2018-10-17 00:00:00",
          eventStatus: "CREATED",
          eventType: EventType.DataUpdateRequired,
          groupName: "unittesting",
          id: "463457733",
          root: null,
        },
      ],
    },
  };

  it("should render modal component", async (): Promise<void> => {
    expect.hasAssertions();

    const mocks = [
      graphql
        .link(LINK)
        .query(
          GET_FINDING_VULNS_TO_REATTACK,
          ({
            variables,
          }): StrictResponse<
            IErrorMessage | { data: GetFindingVulnsToReattack }
          > => {
            const { findingId } = variables;
            if (findingId === "test-finding-id") {
              return HttpResponse.json({
                data: {
                  finding: {
                    __typename: "Finding",
                    id: "test-finding-id",
                    vulnerabilitiesToReattackConnection: {
                      edges: [
                        {
                          node: {
                            __typename: "Vulnerability",
                            findingId: "test-finding-id",
                            id: "test-vuln-id",
                            specific: "9999",
                            where: "vulnerable entrance",
                          },
                        },
                      ],
                      pageInfo: {
                        endCursor: "cursor",
                        hasNextPage: false,
                      },
                    },
                  },
                },
              });
            }

            return HttpResponse.json({
              errors: [
                new GraphQLError("Error getting vulnerabilities to reattack"),
              ],
            });
          },
        ),
    ];

    render(
      <CustomThemeProvider>
        <UpdateAffectedModal
          eventsInfo={testEventsInfo}
          findings={testFindings}
          modalRef={{
            close: jest.fn(),
            isOpen: true,
            name: "test",
            open: jest.fn(),
            setIsOpen: jest.fn(),
          }}
          onSubmit={jest.fn()}
        />
      </CustomThemeProvider>,
      { mocks },
    );

    await expect(
      screen.findByText("group.events.form.affectedReattacks.title"),
    ).resolves.toBeInTheDocument();

    expect(
      screen.getByRole("combobox", { name: "eventId" }),
    ).toBeInTheDocument();
    expect(
      screen.getByText("038. Business information leak"),
    ).toBeInTheDocument();

    await userEvent.click(screen.getByText("038. Business information leak"));

    await waitFor((): void => {
      expect(screen.queryByText("vulnerable entrance")).toBeInTheDocument();
    });

    expect(screen.getByRole("button", { name: /confirm/iu })).toBeDisabled();
  });
});
