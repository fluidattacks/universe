import { Text } from "@fluidattacks/design";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { array, object, string } from "yup";

import type { IUpdateAffectedModalProps } from "./types";

import { AffectedReattackAccordion } from "../affected-reattack-accordion";
import { InnerForm } from "components/form";
import { FormModal } from "components/form-modal";
import { Select } from "components/input";

export const UpdateAffectedModal: React.FC<IUpdateAffectedModalProps> = ({
  eventsInfo,
  findings,
  modalRef,
  onSubmit,
}): JSX.Element => {
  const { t } = useTranslation();
  const { close } = modalRef;

  const events = eventsInfo?.group.events ?? [];

  const eventOptions = [{ header: "", value: "" }].concat(
    events
      .filter(
        ({ eventStatus }): boolean => eventStatus.toUpperCase() !== "SOLVED",
      )
      .map(({ detail, id }): { header: string; value: string } => {
        return { header: detail, value: id };
      }),
  );

  const validationSchema = object().shape({
    affectedReattacks: array().min(1, t("validations.someRequired")),
    eventId: string().required(t("validations.required")),
  });

  return (
    <FormModal
      initialValues={{
        affectedReattacks: [],
        eventId: "",
      }}
      modalRef={modalRef}
      name={"updateAffected"}
      onSubmit={onSubmit}
      size={"md"}
      title={t("group.events.form.affectedReattacks.title")}
      validationSchema={validationSchema}
    >
      {({ dirty, isSubmitting }): JSX.Element => (
        <InnerForm onCancel={close} submitDisabled={!dirty || isSubmitting}>
          <Select
            items={eventOptions}
            label={t("group.events.form.affectedReattacks.eventSection")}
            name={"eventId"}
          />
          <Text mt={0.5} size={"sm"}>
            {t("group.events.form.affectedReattacks.selection")}
          </Text>
          <AffectedReattackAccordion findings={findings} />
        </InnerForm>
      )}
    </FormModal>
  );
};
