import { graphql } from "gql";

const GET_AUTHORS = graphql(`
  query GetAuthors($date: DateTime, $groupName: String!) {
    group(groupName: $groupName) {
      __typename
      name
      billing(date: $date) {
        authors {
          actor
          commit
          groups
          organization
          repository
        }
      }
    }
  }
`);

const GET_GROUP_DATA = graphql(`
  query GetGroupDataQuery($groupName: String!, $organizationName: String!) {
    group(groupName: $groupName) {
      managed
      name
      organization
      permissions
      serviceAttributes
    }
    organizationId(organizationName: $organizationName) {
      id
      name
    }
  }
`);

const GET_GROUP_EVENT_STATUS = graphql(`
  query GetGroupEventStatus($groupName: String!) {
    group(groupName: $groupName) {
      name
      events {
        id
        eventStatus
      }
    }
  }
`);

const GET_STAKEHOLDERS = graphql(`
  query GetStakeholders($groupName: String!) {
    group(groupName: $groupName) {
      __typename
      name
      stakeholders {
        email
        firstLogin
        invitationState
        lastLogin
        responsibility
        role
      }
    }
  }
`);

export {
  GET_AUTHORS,
  GET_GROUP_DATA,
  GET_GROUP_EVENT_STATUS,
  GET_STAKEHOLDERS,
};
