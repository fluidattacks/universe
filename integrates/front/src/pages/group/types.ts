interface IGetEventStatus {
  group: {
    events: {
      eventStatus: string;
    }[];
    name: string;
  };
}

interface IVulnerabilitiesContext {
  openVulnerabilities: number;
  setOpenVulnerabilities?: React.Dispatch<React.SetStateAction<number>>;
}

export type { IGetEventStatus, IVulnerabilitiesContext };
