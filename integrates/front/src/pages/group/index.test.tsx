import { screen, waitFor } from "@testing-library/react";
import { GraphQLError } from "graphql";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import { Route, Routes } from "react-router-dom";

import { GET_GROUP_DATA } from "./queries";

import { Group } from ".";
import { GroupLevelPermissionsProvider } from "context/authz/group-level-permissions-provider";
import { GET_ORG_EVENTS } from "features/event-bar/queries";
import { ManagedType, TrialState } from "gql/graphql";
import type {
  GetGroupDataQueryQuery as GetGroupData,
  GetGroupFindingsQueriesQuery as GetGroupFindingsQueries,
  GetOrganizationEventsQuery as GetOrganizationEvents,
  GetStakeholderTrialQuery,
} from "gql/graphql";
import { GET_STAKEHOLDER_TRIAL } from "hooks/queries";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";
import { GET_GROUP_FINDINGS_QUERIES } from "pages/group/findings/queries";
import { OrganizationBilling } from "pages/organization/billing";
import { msgError } from "utils/notifications";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();

  return mockedNotifications;
});

describe("group", (): void => {
  const graphqlMocked = graphql.link(LINK);

  const baseQuery = {
    group: {
      __typename: "Group" as const,
      managed: ManagedType.NotManaged,
      name: "test",
      organization: "okada",
      permissions: ["integrates_api_resolvers_group_consulting_resolve"],
      serviceAttributes: ["has_asm"],
    },
    organizationId: {
      __typename: "Organization" as const,
      id: "ORG#test",
      name: "okada",
    },
  };
  const underReviewQuery = graphqlMocked.query(
    GET_GROUP_DATA,
    (): StrictResponse<{ data: GetGroupData }> => {
      return HttpResponse.json({
        data: {
          ...baseQuery,
          group: { ...baseQuery.group, managed: ManagedType.UnderReview },
        },
      });
    },
  );
  const trialCompleteQuery = graphqlMocked.query(
    GET_STAKEHOLDER_TRIAL,
    (): StrictResponse<{ data: GetStakeholderTrialQuery }> => {
      return HttpResponse.json({
        data: {
          __typename: "Query",
          me: {
            __typename: "Me",
            trial: {
              __typename: "Trial",
              completed: true,
              extensionDate: "",
              extensionDays: 0,
              startDate: "",
              state: TrialState.TrialEnded,
            },
            userEmail: "test@fluidattacks.com",
          },
        },
      });
    },
  );
  const orgEventsQuery = graphqlMocked.query(
    GET_ORG_EVENTS,
    (): StrictResponse<{ data: GetOrganizationEvents }> => {
      return HttpResponse.json({
        data: {
          organizationId: {
            __typename: "Organization",
            groups: [{ __typename: "Group", events: [], name: "test" }],
            name: "okada",
          },
        },
      });
    },
  );
  const criteriaQuery = graphqlMocked.query(
    GET_GROUP_FINDINGS_QUERIES,
    (): StrictResponse<{ data: GetGroupFindingsQueries }> => {
      return HttpResponse.json({
        data: {
          __typename: "Query",
          ...{
            criteriaConnection: {
              edges: [
                {
                  cursor: "001",
                  node: {
                    en: {
                      description:
                        "Dynamic SQL statements are generated without the required data validation and without using parameterized statements or stored procedures.\n",
                      impact:
                        "Inject SQL statements, with the possibility of obtaining information about the database, as well as extract information from it.\n",
                      recommendation:
                        "Perform queries to the database through sentences or parameterized procedures.\n",
                      threat: "Authenticated attacker from the Internet.",
                      title: "001. SQL injection - C Sharp SQL API",
                    },
                    es: {
                      description:
                        "Se generan sentencias SQL dinámicas sin la validación requerida de datos y sin utilizar sentencias parametrizadas o procedimientos almacenados.\n",
                      impact:
                        "Inyectar sentencias SQL, con la posibilidad de obtener información sobre la base de datos, así como extraer información de la misma.\n",
                      recommendation:
                        "Realizar las consultas a la base de datos por medio de sentencias o procedimientos parametrizados.\n",
                      threat: "Atacante autenticado desde Internet.",
                      title: "001. SQL injection - C Sharp SQL API",
                    },
                    findingNumber: "001",
                    remediationTime: "15",
                    requirements: ["161", "173"],
                    score: {
                      base: {
                        attackComplexity: "L",
                        attackVector: "N",
                        availability: "N",
                        confidentiality: "N",
                        integrity: "L",
                        privilegesRequired: "L",
                        scope: "U",
                        userInteraction: "N",
                      },
                      temporal: {
                        exploitCodeMaturity: "U",
                        remediationLevel: "O",
                        reportConfidence: "C",
                      },
                    },
                    scoreV4: {
                      base: {
                        attackComplexity: "L",
                        attackRequirements: "N",
                        attackVector: "N",
                        availabilitySa: "N",
                        availabilityVa: "N",
                        confidentialitySc: "N",
                        confidentialityVc: "N",
                        integritySi: "L",
                        integrityVi: "L",
                        privilegesRequired: "L",
                        userInteraction: "N",
                      },
                      threat: {
                        exploitMaturity: "U",
                      },
                    },
                  },
                },
              ],
              pageInfo: { endCursor: "001", hasNextPage: false },
              total: 1,
            },
          },
        },
      });
    },
  );

  it("should render error in component", async (): Promise<void> => {
    expect.hasAssertions();

    jest.spyOn(console, "warn").mockImplementation();

    const mockError = [
      graphql
        .link(LINK)
        .query(GET_GROUP_DATA, (): StrictResponse<IErrorMessage> => {
          return HttpResponse.json({
            errors: [new GraphQLError("Error getting group services")],
          });
        }),
    ];
    render(
      <Routes>
        <Route
          element={<Group />}
          path={"/orgs/:organizationName/groups/:groupName/*"}
        />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/test"],
        },
        mocks: mockError,
      },
    );
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith("groupAlerts.errorTextsad");
    });

    jest.clearAllMocks();
  });

  it("should prevent access under review and trial", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    render(
      <Routes>
        <Route
          element={
            <GroupLevelPermissionsProvider>
              <Group />
            </GroupLevelPermissionsProvider>
          }
          path={"/orgs/:organizationName/groups/:groupName/*"}
        />
        <Route
          element={<OrganizationBilling organizationId={"ORG#test"} />}
          path={"/orgs/:organizationName/billing"}
        />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/test/vulns"],
        },
        mocks: [
          underReviewQuery,
          orgEventsQuery,
          criteriaQuery,
          trialCompleteQuery,
        ],
      },
    );

    await waitFor((): void => {
      expect(
        screen.queryByText("autoenrollment.freeTrialEnd.title"),
      ).toBeInTheDocument();
    });
  });
});
