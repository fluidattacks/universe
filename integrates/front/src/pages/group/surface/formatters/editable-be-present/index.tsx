import { Tag } from "@fluidattacks/design";
import { Toggle } from "@fluidattacks/design";
import { useCallback } from "react";
import { useTranslation } from "react-i18next";

import type { IToeInputData } from "../../inputs/types";
import type { IToePortData } from "../../ports/types";
import { formatBoolean } from "utils/format-helpers";

type TToeData = IToeInputData | IToePortData;

interface IEditableBePresentFormatter<TRow extends TToeData> {
  row: TRow;
  canEdit: boolean;
  handleUpdate: (row: TRow) => Promise<void>;
}

const EditableBePresentFormatter = <TData extends TToeData>({
  row,
  canEdit,
  handleUpdate,
}: Readonly<IEditableBePresentFormatter<TData>>): JSX.Element => {
  const value: boolean = row.bePresent;
  const { t } = useTranslation();

  const handleOnChange = useCallback((): void => {
    void handleUpdate(row);
  }, [handleUpdate, row]);

  if (!canEdit) {
    return <Tag tagLabel={formatBoolean(value)} />;
  }

  return (
    <Toggle
      defaultChecked={value}
      leftDescription={{ off: t("no"), on: t("yes") }}
      name={"bePresentSwitch"}
      onChange={handleOnChange}
    />
  );
};

function editableBePresentFormatter<TData extends TToeData>(
  canEdit: IEditableBePresentFormatter<TData>["canEdit"],
  handleUpdate: IEditableBePresentFormatter<TData>["handleUpdate"],
  row: TData,
): JSX.Element {
  return (
    <EditableBePresentFormatter
      canEdit={canEdit}
      handleUpdate={handleUpdate}
      row={row}
    />
  );
}

export { editableBePresentFormatter, EditableBePresentFormatter };
