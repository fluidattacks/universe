import isEmpty from "lodash/isEmpty";

const NOSEENFIRSTTIMEBY = "no seen first time by";

const markSeenFirstTimeBy = (seenFirstTimeBy: string): string =>
  isEmpty(seenFirstTimeBy) ? NOSEENFIRSTTIMEBY : seenFirstTimeBy;

export { markSeenFirstTimeBy };
