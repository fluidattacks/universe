import { NumberInput } from "@fluidattacks/design";
import _ from "lodash";
import { useCallback } from "react";

import type { IToeLinesData } from "../../types";
import { translate } from "utils/translations/translate";

export const useAttackedLinesFormatter = (
  canEdit: boolean,
  handleUpdateAttackedLines: (
    rootId: string,
    filename: string,
    comments: string,
    attackedLines: number,
  ) => Promise<void>,
  row: IToeLinesData,
): JSX.Element | string => {
  const value = row.attackedLines;

  const handleOnEnter = useCallback(
    (newValue?: number): void => {
      if (!_.isUndefined(newValue)) {
        void handleUpdateAttackedLines(
          row.rootId,
          row.filename,
          row.comments,
          newValue,
        );
      }
    },
    [handleUpdateAttackedLines, row],
  );

  if (!canEdit) {
    return String(row.attackedLines);
  }

  return (
    <NumberInput
      defaultValue={_.toNumber(value)}
      max={row.loc}
      min={0}
      onEnter={handleOnEnter}
      tooltipMessage={translate.t(
        "group.toe.lines.formatters.attackedLines.tooltip",
      )}
    />
  );
};
