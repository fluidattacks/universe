import isNil from "lodash/isNil";
import * as React from "react";

import { SortsSuggestionsButton } from "../../styles";
import type { ISortsSuggestionAttr } from "../../types";

const handleOnClick =
  (
    sortsSuggestions: ISortsSuggestionAttr[] | null,
    setSelectedToeLinesSortsSuggestions: (
      value: React.SetStateAction<ISortsSuggestionAttr[] | undefined>,
    ) => void,
    setIsSortsSuggestionsModalOpen: (value: boolean) => void,
  ): (() => void) =>
  (): void => {
    if (!isNil(sortsSuggestions)) {
      setSelectedToeLinesSortsSuggestions(sortsSuggestions);
      setIsSortsSuggestionsModalOpen(true);
    }
  };

const formatSortsSuggestions = (
  sortsSuggestions: ISortsSuggestionAttr[] | null,
  setSelectedToeLinesSortsSuggestions: (
    value: React.SetStateAction<ISortsSuggestionAttr[] | undefined>,
  ) => void,
  setIsSortsSuggestionsModalOpen: (value: boolean) => void,
): JSX.Element => {
  const value =
    isNil(sortsSuggestions) || sortsSuggestions.length === 0
      ? "None"
      : `${sortsSuggestions.length} available`;

  return (
    <SortsSuggestionsButton
      $isNone={value === "None"}
      onClick={handleOnClick(
        sortsSuggestions,
        setSelectedToeLinesSortsSuggestions,
        setIsSortsSuggestionsModalOpen,
      )}
    >
      {value}
    </SortsSuggestionsButton>
  );
};

export { formatSortsSuggestions };
