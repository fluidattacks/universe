import type { IUseModal } from "@fluidattacks/design";
import { Col, Modal, Row, Text } from "@fluidattacks/design";
import _ from "lodash";
import * as React from "react";
import { useTranslation } from "react-i18next";

import type { ISortsSuggestionAttr } from "../types";

interface ISortsSuggestionsModal {
  readonly modalRef: IUseModal;
  readonly selectedSortsSuggestions: ISortsSuggestionAttr[] | undefined;
}

const SortsSuggestionsModal: React.FC<ISortsSuggestionsModal> = ({
  modalRef,
  selectedSortsSuggestions,
}: ISortsSuggestionsModal): JSX.Element => {
  const { t } = useTranslation();

  if (
    _.isUndefined(selectedSortsSuggestions) ||
    _.isEmpty(selectedSortsSuggestions)
  ) {
    return <div />;
  }

  const sortsSuggestionsItems: JSX.Element[] = selectedSortsSuggestions.map(
    (item: ISortsSuggestionAttr): JSX.Element => {
      return (
        <Row justify={"around"} key={item.findingTitle}>
          <Col lg={78} md={78} sm={78}>
            <Text pb={0.5} size={"sm"}>
              {`${item.findingTitle}:`}
            </Text>
          </Col>
          <Col lg={22} md={22} sm={22}>
            <Text
              size={"md"}
              textAlign={"center"}
            >{`${item.probability} %`}</Text>
          </Col>
        </Row>
      );
    },
  );

  return (
    <Modal
      modalRef={modalRef}
      size={"sm"}
      title={t("group.toe.lines.sortsSuggestions")}
    >
      <div>{sortsSuggestionsItems}</div>
    </Modal>
  );
};

export { SortsSuggestionsModal };
