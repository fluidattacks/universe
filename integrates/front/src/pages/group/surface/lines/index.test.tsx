import { PureAbility } from "@casl/ability";
import { screen, waitFor, within } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import { Route, Routes } from "react-router-dom";

import { GET_GIT_ROOTS } from "./addition-modal/queries";
import { UPDATE_TOE_LINES_ATTACKED_LINES } from "./edition-modal/queries";
import { GET_TOE_LINES, TOE_LINES_FRAGMENT, VERIFY_TOE_LINES } from "./queries";

import { GroupToeLines } from ".";
import { authzPermissionsContext } from "context/authz/config";
import { makeFragmentData } from "gql/fragment-masking";
import type {
  GetGitRootsInfoQuery as GetGitRoots,
  GetToeLinesQuery as GetToeLines,
  VerifyToeLinesMutation as VerifyToeLines,
} from "gql/graphql";
import { ResourceState } from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";
import { msgSuccess } from "utils/notifications";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

describe("groupToeLines", (): void => {
  const graphqlMocked = graphql.link(LINK);
  const memoryRouter = {
    initialEntries: ["/unittesting/surface/lines"],
  };

  const edges: GetToeLines["group"]["toeLinesConnection"]["edges"] = [
    {
      __typename: "ToeLinesEdge",
      node: makeFragmentData(
        {
          __typename: "ToeLines",
          attackedAt: "2021-02-20T05:00:00+00:00",
          attackedBy: "test2@test.com",
          attackedLines: 4,
          bePresent: true,
          bePresentUntil: "2020-12-31T05:00:00+00:00",
          comments: "comment 1",
          filename: "test/test#.config",
          firstAttackAt: "2020-02-19T15:41:04+00:00",
          hasVulnerabilities: true,
          lastAuthor: "user@gmail.com",
          lastCommit: "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c2",
          loc: 8,
          modifiedDate: "2020-11-15T15:41:04+00:00",
          root: {
            __typename: "GitRoot",
            id: "63298a73-9dff-46cf-b42d-9b2f01a56690",
            nickname: "universe",
          },
          seenAt: "2020-02-01T15:41:04+00:00",
          sortsPriorityFactor: 70,
          sortsSuggestions: null,
        },
        TOE_LINES_FRAGMENT,
      ),
    },
    {
      __typename: "ToeLinesEdge",
      node: makeFragmentData(
        {
          __typename: "ToeLines",
          attackedAt: "2021-02-19T05:00:00+00:00",
          attackedBy: "test@test.com",
          attackedLines: 120,
          bePresent: true,
          bePresentUntil: "2021-01-01T15:41:04+00:00",
          comments: "comment 2",
          filename: "test2/test.sh",
          firstAttackAt: "2020-02-18T15:41:04+00:00",
          hasVulnerabilities: true,
          lastAuthor: "user@gmail.com",
          lastCommit: "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c1",
          loc: 172,
          modifiedDate: "2020-11-16T15:41:04+00:00",
          root: {
            __typename: "GitRoot",
            id: "765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a",
            nickname: "integrates_1",
          },
          seenAt: "2020-01-01T15:41:04+00:00",
          sortsPriorityFactor: 10,
          sortsSuggestions: [
            {
              __typename: "ToeLinesSortSuggestion",
              findingTitle: "027. Insecure file upload",
              probability: 100,
            },
            {
              __typename: "ToeLinesSortSuggestion",
              findingTitle: "083. XML injection (XXE)",
              probability: 90,
            },
          ],
        },
        TOE_LINES_FRAGMENT,
      ),
    },
  ];

  const safeEdges: GetToeLines["group"]["toeLinesConnection"]["edges"] = [
    {
      __typename: "ToeLinesEdge",
      node: makeFragmentData(
        {
          __typename: "ToeLines",
          attackedAt: "2021-02-20T05:00:00+00:00",
          attackedBy: "test2@test.com",
          attackedLines: 4,
          bePresent: true,
          bePresentUntil: "2020-12-31T05:00:00+00:00",
          comments: "comment 1",
          filename: "test/test#safe.config",
          firstAttackAt: "2020-02-19T15:41:04+00:00",
          hasVulnerabilities: false,
          lastAuthor: "user@gmail.com",
          lastCommit: "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c2",
          loc: 8,
          modifiedDate: "2020-11-15T15:41:04+00:00",
          root: {
            __typename: "GitRoot",
            id: "63298a73-9dff-46cf-b42d-9b2f01a56690",
            nickname: "makes",
          },
          seenAt: "2020-02-01T15:41:04+00:00",
          sortsPriorityFactor: 70,
          sortsSuggestions: null,
        },
        TOE_LINES_FRAGMENT,
      ),
    },
  ];

  const getToesLines = (
    toes: GetToeLines["group"]["toeLinesConnection"]["edges"],
  ): GetToeLines => ({
    group: {
      __typename: "Group",
      name: "unittesting",
      toeLinesConnection: {
        __typename: "ToeLinesConnection",
        edges: toes,
        pageInfo: {
          endCursor:
            '[99.0, "GROUP#unittesting#LINES#ROOT#root_id#FILENAME#test2/test.sh.py"]',
          hasNextPage: false,
        },
        total: toes?.length ?? 0,
      },
    },
  });

  const queryMock = graphqlMocked.query(
    GET_GIT_ROOTS,
    ({ variables }): StrictResponse<IErrorMessage | { data: GetGitRoots }> => {
      const { groupName } = variables;
      if (groupName === "unittesting") {
        return HttpResponse.json({
          data: {
            group: {
              __typename: "Group",
              name: "unittesting",
              roots: [
                {
                  __typename: "GitRoot",
                  id: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
                  nickname: "universe",
                  state: ResourceState.Active,
                },
              ],
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Error getting git roots")],
      });
    },
  );
  const mockedToeLines1 = graphqlMocked.query(
    GET_TOE_LINES,
    (): StrictResponse<IErrorMessage | { data: GetToeLines }> => {
      return HttpResponse.json({ data: getToesLines(edges) });
    },
  );
  const mockedPermissions = new PureAbility<string>([
    { action: "integrates_api_resolvers_toe_lines_attacked_at_resolve" },
    { action: "integrates_api_resolvers_toe_lines_attacked_by_resolve" },
    { action: "integrates_api_resolvers_toe_lines_attacked_lines_resolve" },
    { action: "integrates_api_mutations_add_toe_lines_mutate" },
    { action: "integrates_api_resolvers_toe_lines_be_present_until_resolve" },
    { action: "integrates_api_resolvers_toe_lines_comments_resolve" },
    { action: "integrates_api_resolvers_toe_lines_first_attack_at_resolve" },
    { action: "integrates_api_resolvers_query_csv_report_resolve" },
    { action: "see_toe_lines_coverage" },
  ]);

  it("should display group toe lines", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<GroupToeLines isInternal={true} />}
            path={"/:groupName/surface/lines"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      { memoryRouter, mocks: [queryMock, mockedToeLines1] },
    );
    const numberOfRows = 3;
    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(numberOfRows);
    });

    expect(
      screen.getAllByRole("row")[0].textContent?.replace(/[^a-zA-Z ]/gu, ""),
    ).toBe(
      "RootLOCStatusModified dateLast commitSorts Priority FactorCoverageAttacked linesAttacked atComments",
    );
    expect(screen.getAllByRole("row")[2].textContent).toStrictEqual(
      [
        "integrates_1",
        "172",
        "Vulnerable",
        "2020-11-16",
        "f9e4beb",
        "10 %",
        "69.76%",
        "120",
        "2021-02-19",
        "comment 2",
      ].join(""),
    );
    expect(screen.getAllByRole("row")[1].textContent).toStrictEqual(
      [
        "universe",
        "8",
        "Vulnerable",
        "2020-11-15",
        "f9e4beb",
        "70 %",
        "50.00%",
        "4",
        "2021-02-20",
        "comment 1",
      ].join(""),
    );

    await waitFor((): void => {
      expect(
        screen.queryByText("group.toe.lines.actionButtons.addButton.text"),
      ).toBeInTheDocument();
    });

    await userEvent.click(
      screen.getByText("group.toe.lines.actionButtons.addButton.text"),
    );
    await waitFor((): void => {
      expect(
        screen.queryByText("group.toe.lines.addModal.title"),
      ).toBeInTheDocument();
    });
    jest.clearAllMocks();
  });

  it("should display multiple export button", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<GroupToeLines isInternal={true} />}
            path={"/:groupName/surface/lines"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      { memoryRouter, mocks: [queryMock, mockedToeLines1] },
    );
    const numberOfRows = 3;

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(numberOfRows);
    });

    const exportButton = screen.getByText("group.findings.exportCsv.text");

    expect(exportButton).toBeInTheDocument();

    await userEvent.click(exportButton);

    expect(screen.getByTestId("export-filtered-data")).toBeInTheDocument();
    expect(screen.getByTestId("export-all-data")).toBeInTheDocument();

    jest.clearAllMocks();
  });

  it("should handle verify lines", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const mocksMutation = [
      graphqlMocked.mutation(
        VERIFY_TOE_LINES,

        ({
          variables,
        }): StrictResponse<IErrorMessage | { data: VerifyToeLines }> => {
          const {
            canGetAttackedBy,
            canGetAttackedLines,
            canGetBePresentUntil,
            canGetComments,
            canGetFirstAttackAt,
            comments,
            filename,
            groupName,
            rootId,
            shouldGetNewToeLines,
          } = variables;
          if (
            canGetAttackedBy &&
            canGetAttackedLines &&
            canGetBePresentUntil &&
            canGetComments &&
            canGetFirstAttackAt &&
            comments &&
            filename === "test/test#.config" &&
            groupName === "unittesting" &&
            rootId === "63298a73-9dff-46cf-b42d-9b2f01a56690" &&
            shouldGetNewToeLines
          ) {
            return HttpResponse.json({
              data: {
                updateToeLinesAttackedLines: {
                  success: true,
                  toeLines: {
                    __typename: "ToeLines",
                    ...{
                      attackedAt: "2021-02-20T05:00:00+00:00",
                      attackedBy: "test2@test.com",
                      attackedLines: 8,
                      bePresent: true,
                      bePresentUntil: "",
                      comments: "comment 1",
                      filename: "test/test#.config",
                      firstAttackAt: "2020-02-19T15:41:04+00:00",
                      hasVulnerabilities: false,
                      lastAuthor: "user@gmail.com",
                      lastCommit: "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c2",
                      loc: 8,
                      modifiedDate: "2020-11-15T15:41:04+00:00",
                      root: {
                        __typename: "GitRoot",
                        id: "63298a73-9dff-46cf-b42d-9b2f01a56690",
                        nickname: "universe1",
                      },
                      seenAt: "2020-02-01T15:41:04+00:00",
                      sortsPriorityFactor: 70,
                      sortsSuggestions: null,
                    },
                  },
                },
              },
            });
          }

          if (
            canGetAttackedBy &&
            canGetAttackedLines &&
            canGetBePresentUntil &&
            canGetComments &&
            canGetFirstAttackAt &&
            comments &&
            filename === "test2/test.sh" &&
            groupName === "unittesting" &&
            rootId === "765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a" &&
            shouldGetNewToeLines
          ) {
            return HttpResponse.json({
              data: {
                updateToeLinesAttackedLines: {
                  success: true,
                  toeLines: {
                    __typename: "ToeLines",
                    ...{
                      attackedAt: "2021-02-20T05:00:00+00:00",
                      attackedBy: "test2@test.com",
                      attackedLines: 172,
                      bePresent: true,
                      bePresentUntil: "",
                      comments: "comment 1",
                      filename: "test2/test.sh",
                      firstAttackAt: "2020-02-19T15:41:04+00:00",
                      hasVulnerabilities: false,
                      lastAuthor: "user@gmail.com",
                      lastCommit: "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c2",
                      loc: 172,
                      modifiedDate: "2020-11-15T15:41:04+00:00",
                      root: {
                        __typename: "GitRoot",
                        id: "765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a",
                        nickname: "universe2",
                      },
                      seenAt: "2020-02-01T15:41:04+00:00",
                      sortsPriorityFactor: 70,
                      sortsSuggestions: null,
                    },
                  },
                },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error verifying group toe lines")],
          });
        },
      ),
    ];
    const handleMockedPermissions = new PureAbility<string>([
      { action: "integrates_api_resolvers_toe_lines_attacked_at_resolve" },
      { action: "integrates_api_resolvers_toe_lines_attacked_by_resolve" },
      { action: "integrates_api_resolvers_toe_lines_attacked_lines_resolve" },
      { action: "integrates_api_resolvers_toe_lines_be_present_until_resolve" },
      { action: "integrates_api_resolvers_toe_lines_comments_resolve" },
      { action: "integrates_api_resolvers_toe_lines_first_attack_at_resolve" },
      {
        action:
          "integrates_api_mutations_update_toe_lines_attacked_lines_mutate",
      },
      { action: "see_toe_lines_coverage" },
    ]);
    render(
      <authzPermissionsContext.Provider value={handleMockedPermissions}>
        <Routes>
          <Route
            element={<GroupToeLines isInternal={true} />}
            path={
              "/orgs/:organizationName/groups/:groupName/internal/surface/lines"
            }
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: [
            "/orgs/okada/groups/unittesting/internal/surface/lines",
          ],
        },
        mocks: [queryMock, mockedToeLines1, ...mocksMutation],
      },
    );
    const numberOfRows = 3;
    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(numberOfRows);
    });

    expect(
      within(screen.queryAllByRole("row")[1]).getAllByRole("cell")[7]
        .textContent,
    ).toBe("50.00%");
    expect(
      within(screen.queryAllByRole("row")[2]).getAllByRole("cell")[7]
        .textContent,
    ).toBe("69.76%");

    expect(document.querySelectorAll("#verifyToeLines")[0]).toBeDisabled();

    await userEvent.click(screen.getAllByRole("checkbox")[1]);
    await userEvent.click(screen.getAllByRole("checkbox")[2]);

    expect(document.querySelectorAll("#verifyToeLines")[0]).not.toBeDisabled();

    await userEvent.click(screen.getByText("buttons.verify"));

    await waitFor((): void => {
      expect(
        within(screen.queryAllByRole("row")[1]).getAllByRole("cell")[7]
          .textContent,
      ).toBe("100.00%");
    });

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "group.toe.lines.alerts.verifyToeLines.success",
        "groupAlerts.updatedTitle",
      );
    });

    await waitFor((): void => {
      expect(
        within(screen.queryAllByRole("row")[1]).getAllByRole("cell")[7]
          .textContent,
      ).toBe("100.00%");
    });

    expect(
      within(screen.queryAllByRole("row")[2]).getAllByRole("cell")[7]
        .textContent,
    ).toBe("100.00%");
  });

  it("should handle edit attacked lines on cell", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const mocks = [
      queryMock,
      mockedToeLines1,
      graphqlMocked.mutation(
        VERIFY_TOE_LINES,
        ({
          variables,
        }): StrictResponse<IErrorMessage | { data: VerifyToeLines }> => {
          const {
            attackedLines,
            canGetAttackedAt,
            canGetAttackedBy,
            canGetAttackedLines,
            canGetBePresentUntil,
            canGetComments,
            canGetFirstAttackAt,
            comments,
            filename,
            groupName,
            rootId,
            shouldGetNewToeLines,
          } = variables;
          if (
            attackedLines === 6 &&
            canGetAttackedAt &&
            canGetAttackedBy &&
            canGetAttackedLines &&
            canGetBePresentUntil &&
            canGetComments &&
            canGetFirstAttackAt &&
            comments &&
            filename === "test/test#.config" &&
            groupName === "unittesting" &&
            rootId === "63298a73-9dff-46cf-b42d-9b2f01a56690" &&
            shouldGetNewToeLines
          ) {
            return HttpResponse.json({
              data: {
                updateToeLinesAttackedLines: {
                  success: true,
                  toeLines: {
                    __typename: "ToeLines",
                    ...{
                      attackedAt: "2021-02-20T05:00:00+00:00",
                      attackedBy: "test2@test.com",
                      attackedLines: 6,
                      bePresent: true,
                      bePresentUntil: "",
                      comments: "comment 1",
                      filename: "test/test#.config",
                      firstAttackAt: "2020-02-19T15:41:04+00:00",
                      hasVulnerabilities: false,
                      lastAuthor: "user@gmail.com",
                      lastCommit: "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c2",
                      loc: 8,
                      modifiedDate: "2020-11-15T15:41:04+00:00",
                      root: {
                        __typename: "GitRoot",
                        id: "63298a73-9dff-46cf-b42d-9b2f01a56690",
                        nickname: "universe",
                      },
                      seenAt: "2020-02-01T15:41:04+00:00",
                      sortsPriorityFactor: 70,
                      sortsSuggestions: null,
                    },
                  },
                },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error verifying group toe lines")],
          });
        },
      ),
    ];
    const handleMockedPermissions = new PureAbility<string>([
      { action: "integrates_api_resolvers_toe_lines_attacked_at_resolve" },
      { action: "integrates_api_resolvers_toe_lines_attacked_by_resolve" },
      { action: "integrates_api_resolvers_toe_lines_attacked_lines_resolve" },
      { action: "integrates_api_resolvers_toe_lines_be_present_until_resolve" },
      { action: "integrates_api_resolvers_toe_lines_comments_resolve" },
      { action: "integrates_api_resolvers_toe_lines_first_attack_at_resolve" },
      {
        action:
          "integrates_api_mutations_update_toe_lines_attacked_lines_mutate",
      },
      { action: "see_toe_lines_coverage" },
    ]);
    render(
      <authzPermissionsContext.Provider value={handleMockedPermissions}>
        <Routes>
          <Route
            element={<GroupToeLines isInternal={true} />}
            path={"/:groupName/surface/lines"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      { memoryRouter, mocks },
    );
    const numberOfRows = 3;
    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(numberOfRows);
    });

    expect(
      within(screen.queryAllByRole("row")[1]).getAllByRole("cell")[7]
        .textContent,
    ).toBe("50.00%");

    await userEvent.clear(screen.getAllByRole("spinbutton")[0]);
    await userEvent.type(screen.getAllByRole("spinbutton")[0], "6");
    await userEvent.type(screen.getAllByRole("spinbutton")[0], "{enter}");

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "group.toe.lines.alerts.verifyToeLines.success",
        "groupAlerts.updatedTitle",
      );
    });

    await waitFor((): void => {
      expect(
        within(screen.queryAllByRole("row")[1]).getAllByRole("cell")[7]
          .textContent,
      ).toBe("75.00%");
    });

    expect(screen.getAllByRole("spinbutton")[0]).toHaveValue(6);
  });

  it("should have filters", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();
    const mocks = [queryMock, mockedToeLines1];

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<GroupToeLines isInternal={true} />}
            path={"/:groupName/surface/lines"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      { memoryRouter, mocks },
    );
    const numberOfRows = 3;
    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(numberOfRows);
    });

    const filterBtn = document.querySelector("#filterBtn");

    expect(filterBtn).toBeInTheDocument();

    if (filterBtn) {
      await userEvent.click(filterBtn);
    }

    await waitFor((): void => {
      expect(
        screen.getByTestId("li-filter-option-group.toe.lines.filename"),
      ).toBeInTheDocument();
    });

    expect(
      screen.getByTestId("li-filter-option-group.toe.lines.loc"),
    ).toBeInTheDocument();
    expect(
      screen.getByTestId("li-filter-option-group.toe.lines.status"),
    ).toBeInTheDocument();
    expect(
      screen.getByTestId("li-filter-option-group.toe.lines.modifiedDate"),
    ).toBeInTheDocument();
    expect(
      screen.getByTestId("li-filter-option-group.toe.lines.lastCommit"),
    ).toBeInTheDocument();
    expect(
      screen.getByTestId("li-filter-option-group.toe.lines.lastAuthor"),
    ).toBeInTheDocument();
    expect(
      screen.getByTestId("li-filter-option-group.toe.lines.seenAt"),
    ).toBeInTheDocument();
    expect(
      screen.getByTestId(
        "li-filter-option-group.toe.lines.sortsPriorityFactor",
      ),
    ).toBeInTheDocument();
    expect(
      screen.getByTestId("li-filter-option-group.toe.lines.bePresent"),
    ).toBeInTheDocument();
  });

  it("should filter by filename", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedFilteredToeLines = graphqlMocked.query(
      GET_TOE_LINES,
      ({ variables }): StrictResponse<{ data: GetToeLines }> | undefined => {
        const { filename, groupName } = variables;
        if (filename === "test/test#.config" && groupName === "unittesting") {
          return HttpResponse.json({ data: getToesLines([edges[0]]) });
        }

        return undefined;
      },
    );

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<GroupToeLines isInternal={true} />}
            path={"/:groupName/surface/lines"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter,
        mocks: [queryMock, mockedFilteredToeLines, mockedToeLines1],
      },
    );
    const numberOfRows = 3;

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(numberOfRows);
    });

    const filterBtn = document.querySelector("#filterBtn");

    expect(filterBtn).toBeInTheDocument();

    if (filterBtn) {
      await userEvent.click(filterBtn);
    }

    await userEvent.click(
      screen.getByTestId("li-filter-option-group.toe.lines.filename"),
    );

    await userEvent.type(
      screen.getByRole("textbox", {
        name: "text.filename-caseInsensitive",
      }),
      "test/test#.config",
    );
    await userEvent.click(screen.getByText("Apply"));
    await userEvent.click(screen.getByRole("button", { name: "modal-close" }));

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(2);
    });
  });

  it("should filter by modified date", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedFilteredToeLines = graphqlMocked.query(
      GET_TOE_LINES,
      ({ variables }): StrictResponse<{ data: GetToeLines }> | undefined => {
        const { fromModifiedDate, toModifiedDate, groupName } = variables;
        if (
          fromModifiedDate === "2020-11-16" &&
          toModifiedDate === "2025-11-16" &&
          groupName === "unittesting"
        ) {
          return HttpResponse.json({ data: getToesLines([edges[1]]) });
        }

        return undefined;
      },
    );

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<GroupToeLines isInternal={true} />}
            path={"/:groupName/surface/lines"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter,
        mocks: [queryMock, mockedFilteredToeLines, mockedToeLines1],
      },
    );
    const numberOfRows = 3;

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(numberOfRows);
    });

    const filterBtn = document.querySelector("#filterBtn");

    expect(filterBtn).toBeInTheDocument();

    if (filterBtn) {
      await userEvent.click(filterBtn);
    }

    await userEvent.click(
      screen.getByTestId("li-filter-option-group.toe.lines.modifiedDate"),
    );

    const monthInput = screen.getAllByRole("spinbutton", { name: /month/iu });
    const dayInput = screen.getAllByRole("spinbutton", { name: /day/iu });
    const yearInput = screen.getAllByRole("spinbutton", { name: /year/iu });

    expect(monthInput).toHaveLength(2);

    await userEvent.type(monthInput[0], "11");
    await userEvent.type(dayInput[0], "16");
    await userEvent.type(yearInput[0], "2020");
    await userEvent.type(monthInput[1], "11");
    await userEvent.type(dayInput[1], "16");
    await userEvent.type(yearInput[1], "2025");
    await userEvent.click(screen.getByText("Apply"));
    await userEvent.click(screen.getByRole("button", { name: "modal-close" }));

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(2);
    });
  });

  it("should filter by last commit", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedFilteredToeLines = graphqlMocked.query(
      GET_TOE_LINES,
      ({ variables }): StrictResponse<{ data: GetToeLines }> | undefined => {
        const { groupName, lastCommit } = variables;
        if (
          lastCommit === "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c1" &&
          groupName === "unittesting"
        ) {
          return HttpResponse.json({ data: getToesLines([edges[1]]) });
        }

        return undefined;
      },
    );

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<GroupToeLines isInternal={true} />}
            path={"/:groupName/surface/lines"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter,
        mocks: [queryMock, mockedFilteredToeLines, mockedToeLines1],
      },
    );
    const numberOfRows = 3;

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(numberOfRows);
    });

    const filterBtn = document.querySelector("#filterBtn");

    expect(filterBtn).toBeInTheDocument();

    if (filterBtn) {
      await userEvent.click(filterBtn);
    }

    await userEvent.click(
      screen.getByTestId("li-filter-option-group.toe.lines.lastCommit"),
    );

    await userEvent.type(
      screen.getByRole("textbox", {
        name: "text.lastCommit-caseInsensitive",
      }),
      "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c1",
    );
    await userEvent.click(screen.getByText("Apply"));
    await userEvent.click(screen.getByRole("button", { name: "modal-close" }));

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(2);
    });
  });

  it("should filter by last author", async (): Promise<void> => {
    expect.hasAssertions();

    const toeLinesMock: GetToeLines = {
      group: {
        __typename: "Group",
        name: "unittesting",
        toeLinesConnection: {
          __typename: "ToeLinesConnection",
          edges: [
            {
              __typename: "ToeLinesEdge",
              node: makeFragmentData(
                {
                  __typename: "ToeLines",
                  attackedAt: "2021-02-20T05:00:00+00:00",
                  attackedBy: "test2@test.com",
                  attackedLines: 4,
                  bePresent: true,
                  bePresentUntil: "",
                  comments: "comment 1",
                  filename: "test/test#.config",
                  firstAttackAt: "2020-02-19T15:41:04+00:00",
                  hasVulnerabilities: false,
                  lastAuthor: "usertest@gmail.com",
                  lastCommit: "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c2",
                  loc: 8,
                  modifiedDate: "2020-11-15T15:41:04+00:00",
                  root: {
                    __typename: "GitRoot",
                    id: "63298a73-9dff-46cf-b42d-9b2f01a56690",
                    nickname: "universe",
                  },
                  seenAt: "2020-02-01T15:41:04+00:00",
                  sortsPriorityFactor: 70,
                  sortsSuggestions: null,
                },
                TOE_LINES_FRAGMENT,
              ),
            },
          ],
          pageInfo: {
            endCursor: "bnVsbA==",
            hasNextPage: false,
          },
          total: 1,
        },
      },
    };

    const mockedFilteredToeLines = graphqlMocked.query(
      GET_TOE_LINES,
      ({ variables }): StrictResponse<{ data: GetToeLines }> | undefined => {
        const { groupName, lastAuthor } = variables;
        if (
          lastAuthor === "usertest@gmail.com" &&
          groupName === "unittesting"
        ) {
          return HttpResponse.json({ data: toeLinesMock });
        }

        return undefined;
      },
    );

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<GroupToeLines isInternal={true} />}
            path={"/:groupName/surface/lines"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter,
        mocks: [queryMock, mockedFilteredToeLines, mockedToeLines1],
      },
    );
    const numberOfRows = 3;

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(numberOfRows);
    });

    const filterBtn = document.querySelector("#filterBtn");

    expect(filterBtn).toBeInTheDocument();

    if (filterBtn) {
      await userEvent.click(filterBtn);
    }

    await userEvent.click(
      screen.getByTestId("li-filter-option-group.toe.lines.lastAuthor"),
    );

    await userEvent.type(
      screen.getByRole("textbox", {
        name: "text.lastAuthor-caseInsensitive",
      }),
      "usertest@gmail.com",
    );
    await userEvent.click(screen.getByText("Apply"));
    await userEvent.click(screen.getByRole("button", { name: "modal-close" }));

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(2);
    });
  });

  it("should filter by seen at", async (): Promise<void> => {
    expect.hasAssertions();

    const toeLinesMock: GetToeLines = {
      group: {
        __typename: "Group",
        name: "unittesting",
        toeLinesConnection: {
          __typename: "ToeLinesConnection",
          edges: [
            {
              __typename: "ToeLinesEdge",
              node: makeFragmentData(
                {
                  __typename: "ToeLines",
                  attackedAt: "2021-02-20T05:00:00+00:00",
                  attackedBy: "test2@test.com",
                  attackedLines: 4,
                  bePresent: true,
                  bePresentUntil: "",
                  comments: "comment 1",
                  filename: "test/test#.config",
                  firstAttackAt: "2020-02-19T15:41:04+00:00",
                  hasVulnerabilities: false,
                  lastAuthor: "user@gmail.com",
                  lastCommit: "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c2",
                  loc: 8,
                  modifiedDate: "2020-11-15T15:41:04+00:00",
                  root: {
                    __typename: "GitRoot",
                    id: "63298a73-9dff-46cf-b42d-9b2f01a56690",
                    nickname: "universe",
                  },
                  seenAt: "2020-02-01T15:41:04+00:00",
                  sortsPriorityFactor: 70,
                  sortsSuggestions: null,
                },
                TOE_LINES_FRAGMENT,
              ),
            },
          ],
          pageInfo: {
            endCursor: "bnVsbA==",
            hasNextPage: false,
          },
          total: 1,
        },
      },
    };

    const mockedFilteredToeLines = graphqlMocked.query(
      GET_TOE_LINES,
      ({ variables }): StrictResponse<{ data: GetToeLines }> | undefined => {
        const { groupName, fromSeenAt, toSeenAt } = variables;
        if (
          fromSeenAt === "2020-02-01" &&
          toSeenAt === "2025-02-01" &&
          groupName === "unittesting"
        ) {
          return HttpResponse.json({ data: toeLinesMock });
        }

        return undefined;
      },
    );
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<GroupToeLines isInternal={true} />}
            path={"/:groupName/surface/lines"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter,
        mocks: [queryMock, mockedFilteredToeLines, mockedToeLines1],
      },
    );
    const numberOfRows = 3;

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(numberOfRows);
    });

    const filterBtn = document.querySelector("#filterBtn");

    expect(filterBtn).toBeInTheDocument();

    if (filterBtn) {
      await userEvent.click(filterBtn);
    }

    await userEvent.click(
      screen.getByTestId("li-filter-option-group.toe.lines.seenAt"),
    );

    const monthInput = screen.getAllByRole("spinbutton", { name: /month/iu });
    const dayInput = screen.getAllByRole("spinbutton", { name: /day/iu });
    const yearInput = screen.getAllByRole("spinbutton", { name: /year/iu });

    expect(monthInput).toHaveLength(2);

    await userEvent.type(monthInput[0], "02");
    await userEvent.type(dayInput[0], "01");
    await userEvent.type(yearInput[0], "2020");
    await userEvent.type(monthInput[1], "02");
    await userEvent.type(dayInput[1], "01");
    await userEvent.type(yearInput[1], "2025");
    await userEvent.click(screen.getByText("Apply"));
    await userEvent.click(screen.getByRole("button", { name: "modal-close" }));

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(2);
    });
  });

  it("should filter by sorts risk level", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedFilteredToeLines = graphqlMocked.query(
      GET_TOE_LINES,
      ({ variables }): StrictResponse<{ data: GetToeLines }> | undefined => {
        const { first, groupName, minSortsPriorityFactor } = variables;
        if (
          minSortsPriorityFactor === 70 &&
          first === 150 &&
          groupName === "unittesting"
        ) {
          return HttpResponse.json({ data: getToesLines([edges[0]]) });
        }

        return undefined;
      },
    );

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<GroupToeLines isInternal={true} />}
            path={"/:groupName/surface/lines"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter,
        mocks: [queryMock, mockedFilteredToeLines, mockedToeLines1],
      },
    );
    const numberOfRows = 3;

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(numberOfRows);
    });

    const filterBtn = document.querySelector("#filterBtn");

    expect(filterBtn).toBeInTheDocument();

    if (filterBtn) {
      await userEvent.click(filterBtn);
    }

    await userEvent.click(
      screen.getByTestId(
        "li-filter-option-group.toe.lines.sortsPriorityFactor",
      ),
    );
    await userEvent.type(
      document.querySelector("input[name='minValue']") as HTMLElement,
      "70",
    );
    await userEvent.click(screen.getByText("Apply"));
    await userEvent.click(screen.getByRole("button", { name: "modal-close" }));

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(2);
    });
  });

  it("should filter by be present", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedFilteredToeLines = graphqlMocked.query(
      GET_TOE_LINES,
      ({ variables }): StrictResponse<{ data: GetToeLines }> | undefined => {
        const { bePresent, first, groupName } = variables;
        if (
          bePresent === false &&
          first === 150 &&
          groupName === "unittesting"
        ) {
          return HttpResponse.json({ data: getToesLines([safeEdges[0]]) });
        }

        return undefined;
      },
    );
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<GroupToeLines isInternal={true} />}
            path={"/:groupName/surface/lines"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter,
        mocks: [queryMock, mockedFilteredToeLines, mockedToeLines1],
      },
    );
    const numberOfRows = 3;

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(numberOfRows);
    });

    const filterBtn = document.querySelector("#filterBtn");

    expect(filterBtn).toBeInTheDocument();

    if (filterBtn) {
      await userEvent.click(filterBtn);
    }

    await userEvent.click(
      screen.getByTestId("li-filter-option-group.toe.lines.bePresent"),
    );

    await userEvent.click(screen.getByRole("radiogroup", { name: "No" }));
    await userEvent.click(screen.getByText("Apply"));
    await userEvent.click(screen.getByRole("button", { name: "modal-close" }));

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(2);
    });

    expect(screen.getByText("makes")).toBeInTheDocument();
  });

  it("should filter by has vulnerabilities", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedFilteredToeLines = graphqlMocked.query(
      GET_TOE_LINES,
      ({ variables }): StrictResponse<{ data: GetToeLines }> | undefined => {
        const { first, groupName, hasVulnerabilities } = variables;
        if (
          hasVulnerabilities === false &&
          first === 150 &&
          groupName === "unittesting"
        ) {
          return HttpResponse.json({ data: getToesLines([safeEdges[0]]) });
        }

        return undefined;
      },
    );

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<GroupToeLines isInternal={true} />}
            path={"/:groupName/surface/lines"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter,
        mocks: [queryMock, mockedFilteredToeLines, mockedToeLines1],
      },
    );
    const numberOfRows = 3;

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(numberOfRows);
    });

    const filterBtn = document.querySelector("#filterBtn");

    expect(filterBtn).toBeInTheDocument();

    if (filterBtn) {
      await userEvent.click(filterBtn);
    }

    await userEvent.click(
      screen.getByTestId("li-filter-option-group.toe.lines.status"),
    );

    await userEvent.click(screen.getByRole("radiogroup", { name: "Safe" }));
    await userEvent.click(screen.getByText("Apply"));
    await userEvent.click(screen.getByRole("button", { name: "modal-close" }));

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(2);
    });

    expect(screen.getByText("makes")).toBeInTheDocument();
  });

  it("should filter by loc", async (): Promise<void> => {
    expect.hasAssertions();

    const toeLinesMockCoverage: GetToeLines = {
      group: {
        __typename: "Group",
        name: "unittesting",
        toeLinesConnection: {
          __typename: "ToeLinesConnection",
          edges: [
            {
              __typename: "ToeLinesEdge",
              node: makeFragmentData(
                {
                  __typename: "ToeLines",
                  attackedAt: "2021-02-20T05:00:00+00:00",
                  attackedBy: "test2@test.com",
                  attackedLines: 4,
                  bePresent: true,
                  bePresentUntil: "",
                  comments: "comment 1",
                  filename: "test/test#.config",
                  firstAttackAt: "2020-02-19T15:41:04+00:00",
                  hasVulnerabilities: false,
                  lastAuthor: "user@gmail.com",
                  lastCommit: "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c2",
                  loc: 8,
                  modifiedDate: "2020-11-15T15:41:04+00:00",
                  root: {
                    __typename: "GitRoot",
                    id: "63298a73-9dff-46cf-b42d-9b2f01a56690",
                    nickname: "universe",
                  },
                  seenAt: "2020-02-01T15:41:04+00:00",
                  sortsPriorityFactor: 70,
                  sortsSuggestions: null,
                },
                TOE_LINES_FRAGMENT,
              ),
            },
          ],
          pageInfo: {
            endCursor: "bnVsbA==",
            hasNextPage: false,
          },
          total: 1,
        },
      },
    };

    const mockedToeLinesCoverage = graphqlMocked.query(
      GET_TOE_LINES,
      ({ variables }): StrictResponse<{ data: GetToeLines }> | undefined => {
        const { groupName, maxLoc } = variables;
        if (maxLoc === 50 && groupName === "unittesting") {
          return HttpResponse.json({ data: toeLinesMockCoverage });
        }

        return undefined;
      },
    );
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<GroupToeLines isInternal={true} />}
            path={"/:groupName/surface/lines"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter,
        mocks: [queryMock, mockedToeLinesCoverage, mockedToeLines1],
      },
    );
    const numberOfRows = 3;

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(numberOfRows);
    });

    const filterBtn = document.querySelector("#filterBtn");

    expect(filterBtn).toBeInTheDocument();

    if (filterBtn) {
      await userEvent.click(filterBtn);
    }

    await userEvent.click(
      screen.getByTestId("li-filter-option-group.toe.lines.loc"),
    );
    await userEvent.type(
      document.querySelector("input[name='maxValue']") as HTMLElement,
      "50",
    );
    await userEvent.click(screen.getByText("Apply"));
    await userEvent.click(screen.getByRole("button", { name: "modal-close" }));

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(2);
    });

    expect(screen.getByText("universe")).toBeInTheDocument();
  });

  it("should handle edit attacked lines", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();
    // [object ReadableStream]
    jest.spyOn(console, "warn").mockImplementation();

    const mocks = [
      queryMock,
      mockedToeLines1,
      graphqlMocked.mutation(
        UPDATE_TOE_LINES_ATTACKED_LINES,
        ({
          variables,
        }): StrictResponse<IErrorMessage | { data: VerifyToeLines }> => {
          const { attackedLines, comments, groupName } = variables;
          if (attackedLines === 6 && comments && groupName === "unittesting") {
            return HttpResponse.json({
              data: {
                updateToeLinesAttackedLines: {
                  success: true,
                  toeLines: {
                    __typename: "ToeLines",
                    ...{
                      attackedAt: "2021-02-20T05:00:00+00:00",
                      attackedBy: "test2@test.com",
                      attackedLines: 6,
                      bePresent: true,
                      bePresentUntil: "",
                      comments: "comment 1",
                      filename: "test/test#.config",
                      firstAttackAt: "2020-02-19T15:41:04+00:00",
                      hasVulnerabilities: false,
                      lastAuthor: "user@gmail.com",
                      lastCommit: "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c2",
                      loc: 8,
                      modifiedDate: "2020-11-15T15:41:04+00:00",
                      root: {
                        __typename: "GitRoot",
                        id: "63298a73-9dff-46cf-b42d-9b2f01a56690",
                        nickname: "universe",
                      },
                      seenAt: "2020-02-01T15:41:04+00:00",
                      sortsPriorityFactor: 70,
                      sortsSuggestions: null,
                    },
                  },
                },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error verifying group toe lines")],
          });
        },
      ),
    ];
    const handleMockedPermissions = new PureAbility<string>([
      { action: "integrates_api_resolvers_toe_lines_attacked_at_resolve" },
      { action: "integrates_api_resolvers_toe_lines_attacked_by_resolve" },
      { action: "integrates_api_resolvers_toe_lines_attacked_lines_resolve" },
      { action: "integrates_api_resolvers_toe_lines_be_present_until_resolve" },
      { action: "integrates_api_resolvers_toe_lines_comments_resolve" },
      { action: "integrates_api_resolvers_toe_lines_first_attack_at_resolve" },
      {
        action:
          "integrates_api_mutations_update_toe_lines_attacked_lines_mutate",
      },
      { action: "see_toe_lines_coverage" },
    ]);
    jest.spyOn(console, "warn").mockImplementation();
    render(
      <authzPermissionsContext.Provider value={handleMockedPermissions}>
        <Routes>
          <Route
            element={<GroupToeLines isInternal={true} />}
            path={"/:groupName/surface/lines"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      { memoryRouter, mocks },
    );
    const numberOfRows = 3;
    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(numberOfRows);
    });

    expect(
      screen.getByRole("button", {
        name: "buttons.edit",
      }),
    ).toBeDisabled();

    await userEvent.click(
      within(screen.queryAllByRole("row")[1]).getAllByTestId("check-icon")[0],
    );

    expect(
      screen.getByRole("button", {
        name: "buttons.edit",
      }),
    ).not.toBeDisabled();

    await userEvent.click(
      screen.getByRole("button", {
        name: "buttons.edit",
      }),
    );

    await userEvent.clear(
      screen.getByRole("spinbutton", { name: "attackedLines" }),
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: "attackedLines" }),
      "6",
    );
    await userEvent.type(
      screen.getByRole("textbox", { name: "comments" }),
      "This is a test of updating toe lines",
    );

    await userEvent.click(screen.getByText("Confirm"));

    expect(msgSuccess).toHaveBeenCalledWith(
      "group.toe.lines.editModal.alerts.success",
      "groupAlerts.updatedTitle",
    );
  });

  it("should render sorts suggestions modal", async (): Promise<void> => {
    expect.hasAssertions();

    jest.spyOn(console, "warn").mockImplementation();
    jest.clearAllMocks();

    const additionalMockRequest = graphqlMocked.query(
      GET_TOE_LINES,
      (): StrictResponse<IErrorMessage | { data: GetToeLines }> => {
        return HttpResponse.json({ data: getToesLines(edges) });
      },
    );

    const handleMockedPermissions = new PureAbility<string>([
      { action: "integrates_api_resolvers_toe_lines_attacked_at_resolve" },
      { action: "integrates_api_resolvers_toe_lines_attacked_by_resolve" },
      { action: "integrates_api_resolvers_toe_lines_attacked_lines_resolve" },
      { action: "integrates_api_resolvers_toe_lines_be_present_until_resolve" },
      { action: "integrates_api_resolvers_toe_lines_comments_resolve" },
      { action: "integrates_api_resolvers_toe_lines_first_attack_at_resolve" },
      {
        action:
          "integrates_api_mutations_update_toe_lines_attacked_lines_mutate",
      },
      { action: "see_toe_lines_coverage" },
    ]);
    jest.spyOn(console, "error").mockImplementation();
    render(
      <authzPermissionsContext.Provider value={handleMockedPermissions}>
        <Routes>
          <Route
            element={<GroupToeLines isInternal={true} />}
            path={"/:groupName/surface/lines"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter,
        mocks: [queryMock, mockedToeLines1, additionalMockRequest],
      },
    );
    const numberOfRows = 3;
    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(numberOfRows);
    });

    await userEvent.click(
      screen.getByRole("button", {
        name: "group.findings.tableSet.btn.text (10 / 18)",
      }),
    );

    await userEvent.click(
      screen.getByRole("checkbox", {
        name: "Suggested vulnerabilities",
      }),
    );

    await userEvent.click(document.querySelectorAll("#modal-close")[0]);

    await userEvent.click(
      within(screen.queryAllByRole("row")[2]).getByText("2 available"),
    );

    expect(screen.getByText("027. Insecure file upload:")).toBeInTheDocument();
    expect(screen.getByText("100 %")).toBeInTheDocument();
    expect(screen.getByText("083. XML injection (XXE):")).toBeInTheDocument();
    expect(screen.getByText("90 %")).toBeInTheDocument();
  });

  it("should render loc modal", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const binaryFileEdge: GetToeLines["group"]["toeLinesConnection"]["edges"] =
      [
        {
          __typename: "ToeLinesEdge",
          node: makeFragmentData(
            {
              __typename: "ToeLines",
              attackedAt: null,
              attackedBy: "",
              attackedLines: 0,
              bePresent: true,
              bePresentUntil: null,
              comments: "",
              filename: "test2/test.bin",
              firstAttackAt: null,
              hasVulnerabilities: false,
              lastAuthor: "user@gmail.com",
              lastCommit: "a1c8c4096d4ee6d9c62271813f3759247088b1e5",
              loc: 0,
              modifiedDate: "2019-01-04T07:37:51+00:00",
              root: {
                __typename: "GitRoot",
                id: "765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a",
                nickname: "integrates_1",
              },
              seenAt: "2024-06-12T21:39:10.195661+00:00",
              sortsPriorityFactor: 99,
              sortsSuggestions: null,
            },
            TOE_LINES_FRAGMENT,
          ),
        },
      ];

    const additionalMockRequest = graphqlMocked.query(
      GET_TOE_LINES,
      (): StrictResponse<IErrorMessage | { data: GetToeLines }> => {
        return HttpResponse.json({
          data: getToesLines([...edges, ...binaryFileEdge]),
        });
      },
    );

    const handleMockedPermissions = new PureAbility<string>([
      { action: "integrates_api_resolvers_toe_lines_attacked_at_resolve" },
      { action: "integrates_api_resolvers_toe_lines_attacked_by_resolve" },
      { action: "integrates_api_resolvers_toe_lines_attacked_lines_resolve" },
      { action: "integrates_api_resolvers_toe_lines_be_present_until_resolve" },
      { action: "integrates_api_resolvers_toe_lines_comments_resolve" },
      { action: "integrates_api_resolvers_toe_lines_first_attack_at_resolve" },
      {
        action:
          "integrates_api_mutations_update_toe_lines_attacked_lines_mutate",
      },
      { action: "see_toe_lines_coverage" },
    ]);

    jest.spyOn(console, "error").mockImplementation();

    render(
      <authzPermissionsContext.Provider value={handleMockedPermissions}>
        <Routes>
          <Route
            element={<GroupToeLines isInternal={true} />}
            path={"/:groupName/surface/lines"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter,
        mocks: [queryMock, additionalMockRequest, additionalMockRequest],
      },
    );
    const numberOfRows = 4;

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(numberOfRows);
    });

    await userEvent.click(screen.getAllByRole("button", { name: "0" })[1]);

    expect(screen.getByRole("spinbutton", { name: "loc" })).toBeInTheDocument();
    expect(
      screen.getByRole("textbox", { name: "comments" }),
    ).toBeInTheDocument();
    expect(screen.getByText("buttons.confirm")).toBeInTheDocument();
    expect(screen.getByText("buttons.cancel")).toBeInTheDocument();
  });
});
