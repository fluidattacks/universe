import type { FetchResult } from "@apollo/client";
import { useMutation } from "@apollo/client";
import {
  Form,
  InnerForm,
  InputNumber,
  Modal,
  Text,
  TextArea,
} from "@fluidattacks/design";
import dayjs from "dayjs";
import _ from "lodash";
import { useCallback } from "react";
import { Fragment } from "react";
import { useTranslation } from "react-i18next";

import { UPDATE_TOE_LINES_ATTACKED_LINES } from "./queries";
import type {
  IEditionModalProps,
  IFormValues,
  IUpdateToeLinesAttackedLinesResultAttr,
} from "./types";
import { validations } from "./validations";

import { getErrors } from "utils/helpers";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

const EditionModal = ({
  groupName,
  selectedToeLinesData,
  modalRef,
  refetchData,
  setSelectedToeLinesData,
}: IEditionModalProps): JSX.Element | undefined => {
  const { t } = useTranslation();
  const { close } = modalRef;
  const isOneSelected = selectedToeLinesData.length === 1;

  // GraphQL operations
  const [handleUpdateToeLinesAttackedLines] = useMutation(
    UPDATE_TOE_LINES_ATTACKED_LINES,
    {
      onError: (errors): void => {
        errors.graphQLErrors.forEach((error): void => {
          switch (error.message) {
            case "Exception - The attack time must be between the previous attack and the current time":
              msgError(t("group.toe.lines.editModal.alerts.invalidAttackedAt"));
              break;
            case "Exception - The attacked lines must be between 0 and the loc (lines of code)":
              msgError(
                t("group.toe.lines.editModal.alerts.invalidAttackedLines"),
              );
              break;
            case "Exception - The toe lines has been updated by another operation":
              msgError(t("group.toe.lines.editModal.alerts.alreadyUpdate"));
              break;
            default:
              msgError(t("groupAlerts.errorTextsad"));
              Logger.warning(
                "An error occurred updating the toe lines attacked lines",
                error,
              );
          }
        });
      },
    },
  );

  // Data management
  const handleOnCompleted = useCallback(
    (result: FetchResult<IUpdateToeLinesAttackedLinesResultAttr>): void => {
      if (
        !_.isNil(result.data) &&
        result.data.updateToeLinesAttackedLines.success
      ) {
        msgSuccess(
          t("group.toe.lines.editModal.alerts.success"),
          t("groupAlerts.updatedTitle"),
        );
        refetchData();
        setSelectedToeLinesData([]);
        close();
      }
    },
    [close, refetchData, setSelectedToeLinesData, t],
  );
  const handleSubmit = useCallback(
    async (values: IFormValues): Promise<void> => {
      const results = await Promise.all(
        selectedToeLinesData.map(
          async (
            toeInputData,
          ): Promise<FetchResult<IUpdateToeLinesAttackedLinesResultAttr>> =>
            handleUpdateToeLinesAttackedLines({
              variables: {
                attackedLines: _.isNumber(values.attackedLines)
                  ? values.attackedLines
                  : undefined,
                comments: values.comments,
                filename: toeInputData.filename,
                groupName,
                rootId: toeInputData.rootId,
              },
            }),
        ),
      );
      const errors = getErrors<IUpdateToeLinesAttackedLinesResultAttr>(results);

      if (!_.isEmpty(results) && _.isEmpty(errors)) {
        handleOnCompleted(results[0]);
      } else {
        refetchData();
      }
    },
    [
      groupName,
      handleOnCompleted,
      handleUpdateToeLinesAttackedLines,
      refetchData,
      selectedToeLinesData,
    ],
  );

  return (
    <Modal
      id={"updateToeLinesAttackedLines"}
      modalRef={modalRef}
      size={"sm"}
      title={t("group.toe.lines.editModal.title.attacked")}
    >
      <Form
        defaultValues={{
          attackedAt: dayjs(),
          attackedLines: isOneSelected ? selectedToeLinesData[0].loc : "",
          comments: "",
        }}
        onSubmit={handleSubmit}
        yupSchema={validations(selectedToeLinesData)}
      >
        <InnerForm>
          {({
            register,
            watch,
            setValue,
            formState,
            getFieldState,
          }): JSX.Element => (
            <Fragment>
              <Text size={"sm"}>
                <b>{t("group.toe.lines.editModal.fields.attackedLines")}</b>
                &nbsp;
                <i>
                  {`(${t("group.toe.lines.editModal.fields.attackedLinesComment")})`}
                </i>
              </Text>
              <InputNumber
                error={formState.errors.attackedLines?.message?.toString()}
                min={0}
                name={"attackedLines"}
                register={register}
                setValue={setValue}
                watch={watch}
              />
              <TextArea
                isTouched={getFieldState("comments").isTouched}
                isValid={!getFieldState("comments").invalid}
                label={t("group.toe.lines.editModal.fields.comments")}
                maxLength={200}
                name={"comments"}
                register={register}
                watch={watch}
              />
            </Fragment>
          )}
        </InnerForm>
      </Form>
    </Modal>
  );
};

export { EditionModal };
