import { graphql } from "gql";

const UPDATE_TOE_LINES_ATTACKED_LINES = graphql(`
  mutation UpdateToeLinesAttackedLines(
    $attackedLines: Int
    $comments: String!
    $filename: String!
    $groupName: String!
    $rootId: String!
  ) {
    updateToeLinesAttackedLines(
      attackedLines: $attackedLines
      comments: $comments
      filename: $filename
      groupName: $groupName
      rootId: $rootId
    ) {
      success
    }
  }
`);

export { UPDATE_TOE_LINES_ATTACKED_LINES };
