import { PureAbility } from "@casl/ability";
import { fireEvent, screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import { HttpResponse, type StrictResponse, graphql } from "msw";

import { ADD_TOE_LINES, GET_GIT_ROOTS } from "./queries";

import { AdditionModal } from ".";
import { authzPermissionsContext } from "context/authz/config";
import { ResourceState } from "gql/graphql";
import type {
  AddToeLinesMutation as AddToeLines,
  GetGitRootsInfoQuery as GetGitRoots,
} from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage, IMessage } from "mocks/types";
import { msgError, msgSuccess } from "utils/notifications";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();

  return mockedNotifications;
});

describe("handle toe lines addition modal", (): void => {
  const btnConfirm = "buttons.confirm";
  const mocksMutation = [
    graphql
      .link(LINK)
      .mutation(
        ADD_TOE_LINES,
        ({
          variables,
        }): StrictResponse<IErrorMessage | { data: AddToeLines }> => {
          const {
            filename,
            groupName,
            lastAuthor,
            lastCommit,
            loc,
            modifiedDate,
            rootId,
          } = variables;
          if (
            filename === "test/filename.py" &&
            groupName === "groupname" &&
            lastAuthor === "test@test.com" &&
            lastCommit === "2fab76140221397cabbc0eae536b41ff38e7540a" &&
            loc === 10 &&
            modifiedDate === "2022-05-23T07:18:00Z" &&
            rootId === "4039d098-ffc5-4984-8ed3-eb17bca98e19"
          ) {
            return HttpResponse.json({
              data: {
                addToeLines: { success: true },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Exception - File is excluded")],
          });
        },
      ),
  ];
  const handleRefetchData: jest.Mock = jest.fn();
  const modalRef = {
    close: jest.fn(),
    isOpen: true,
    name: "test-addition-modal",
    open: jest.fn(),
    setIsOpen: jest.fn(),
    toggle: jest.fn(),
  };

  it("should handle lines addition", async (): Promise<void> => {
    expect.hasAssertions();

    const role = "combobox";
    jest.clearAllMocks();

    const queryMock = graphql
      .link(LINK)
      .query(GET_GIT_ROOTS, (): StrictResponse<{ data: GetGitRoots }> => {
        return HttpResponse.json({
          data: {
            group: {
              __typename: "Group",
              name: "test",
              roots: [
                {
                  __typename: "GitRoot",
                  id: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
                  nickname: "universe",
                  state: ResourceState.Active,
                },
              ],
            },
          },
        });
      });

    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            { action: "integrates_api_mutations_add_toe_lines_mutate" },
          ])
        }
      >
        <AdditionModal
          groupName={"groupname"}
          modalRef={modalRef}
          refetchData={handleRefetchData}
        />
      </authzPermissionsContext.Provider>,
      { mocks: [...mocksMutation, queryMock] },
    );

    await screen.findByText("group.toe.lines.addModal.title");

    // 05/23/2022 07:18 AM
    await userEvent.type(
      screen.getByRole("spinbutton", { name: "month," }),
      "05",
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: "day," }),
      "23",
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: "year," }),
      "2022",
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: "hour," }),
      "7",
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: "minute," }),
      "18",
    );

    fireEvent.change(screen.getByRole("textbox", { name: "filename" }), {
      target: { value: "test/filename.py" },
    });
    fireEvent.keyDown(screen.getByRole("spinbutton", { name: "loc" }), {
      key: "error",
    });

    fireEvent.change(screen.getByRole("spinbutton", { name: "loc" }), {
      target: { value: "10" },
    });
    fireEvent.change(screen.getByRole(role, { name: "lastAuthor" }), {
      target: { value: "test@test.com" },
    });
    fireEvent.change(screen.getByRole(role, { name: "lastCommit" }), {
      target: {
        value: "2fab76140221397cabbc0eae536b41ff38e7540a",
      },
    });

    await userEvent.click(screen.getByText(btnConfirm));

    await waitFor((): void => {
      expect(modalRef.close).toHaveBeenCalledTimes(1);
    });

    expect(handleRefetchData).toHaveBeenCalledTimes(1);
    expect(msgSuccess).toHaveBeenCalledWith(
      "group.toe.lines.addModal.alerts.success",
      "groupAlerts.titleSuccess",
    );
  });

  it("should handle error on lines addition", async (): Promise<void> => {
    expect.hasAssertions();

    const role = "combobox";
    jest.clearAllMocks();

    const queryMock = graphql
      .link(LINK)
      .query(GET_GIT_ROOTS, (): StrictResponse<{ data: GetGitRoots }> => {
        return HttpResponse.json({
          data: {
            group: {
              __typename: "Group",
              name: "test",
              roots: [
                {
                  __typename: "GitRoot",
                  id: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
                  nickname: "universe",
                  state: ResourceState.Active,
                },
              ],
            },
          },
        });
      });

    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            { action: "integrates_api_mutations_add_toe_lines_mutate" },
          ])
        }
      >
        <AdditionModal
          groupName={"groupname"}
          modalRef={modalRef}
          refetchData={handleRefetchData}
        />
      </authzPermissionsContext.Provider>,
      { mocks: [...mocksMutation, queryMock] },
    );

    await screen.findByText("group.toe.lines.addModal.title");

    await userEvent.click(screen.getByText(btnConfirm));

    expect(screen.queryAllByText("Required field")).toHaveLength(5);

    await userEvent.type(
      screen.getByRole("spinbutton", { name: "month," }),
      "05",
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: "day," }),
      "23",
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: "year," }),
      "2022",
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: "hour," }),
      "7",
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: "minute," }),
      "18",
    );

    fireEvent.change(screen.getByRole("textbox", { name: "filename" }), {
      target: { value: "node_modules/dep/filename.py" },
    });
    fireEvent.change(screen.getByRole("spinbutton", { name: "loc" }), {
      target: { value: "10" },
    });
    fireEvent.change(screen.getByRole(role, { name: "lastAuthor" }), {
      target: { value: "test@test.com" },
    });
    fireEvent.change(screen.getByRole(role, { name: "lastCommit" }), {
      target: {
        value: "2fab76140221397cabbc0eae536b41ff38e7540a",
      },
    });

    await userEvent.click(screen.getByText(btnConfirm));

    expect(msgError).toHaveBeenCalledWith(
      "group.toe.lines.addModal.alerts.fileExcluded",
    );
  });

  it("should handle duplicate and invalid filename errors on line addition", async (): Promise<void> => {
    expect.hasAssertions();

    const role = "combobox";
    jest.clearAllMocks();

    const queryMock = graphql
      .link(LINK)
      .query(GET_GIT_ROOTS, (): StrictResponse<{ data: GetGitRoots }> => {
        return HttpResponse.json({
          data: {
            group: {
              __typename: "Group",
              name: "test",
              roots: [
                {
                  __typename: "GitRoot",
                  id: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
                  nickname: "universe",
                  state: ResourceState.Active,
                },
              ],
            },
          },
        });
      });
    const mocksError = graphql.link(LINK).mutation(
      ADD_TOE_LINES,

      ({
        variables,
      }): StrictResponse<
        Record<"errors", IMessage[]> | { data: AddToeLines }
      > => {
        const {
          filename,
          groupName,
          lastAuthor,
          lastCommit,
          loc,
          modifiedDate,
          rootId,
        } = variables;

        if (
          filename === "test/filename.py" &&
          groupName === "groupname" &&
          lastAuthor === "test@test.com" &&
          lastCommit === "2fab76140221397cabbc0eae536b41ff38e7540a" &&
          loc === 10 &&
          modifiedDate === "2022-05-23T07:18:00Z" &&
          rootId === "4039d098-ffc5-4984-8ed3-eb17bca98e19"
        ) {
          return HttpResponse.json({
            errors: [
              new GraphQLError("Exception - Toe lines already exists"),
              new GraphQLError("An error occurred adding toe lines"),
            ],
          });
        }

        if (
          filename === "//test/filename.py" &&
          groupName === "groupname" &&
          lastAuthor === "test@test.com" &&
          lastCommit === "2fab76140221397cabbc0eae536b41ff38e7540a" &&
          loc === 10 &&
          modifiedDate === "2022-05-23T07:18:00Z" &&
          rootId === "4039d098-ffc5-4984-8ed3-eb17bca98e19"
        ) {
          return HttpResponse.json({
            errors: [
              new GraphQLError("Exception - Invalid file name"),
              new GraphQLError("An error occurred adding toe lines"),
            ],
          });
        }

        return HttpResponse.json({
          data: {
            addToeLines: { success: true },
          },
        });
      },
    );

    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            { action: "integrates_api_mutations_add_toe_lines_mutate" },
          ])
        }
      >
        <AdditionModal
          groupName={"groupname"}
          modalRef={modalRef}
          refetchData={handleRefetchData}
        />
      </authzPermissionsContext.Provider>,
      { mocks: [mocksError, queryMock] },
    );

    await screen.findByText("group.toe.lines.addModal.title");

    await userEvent.type(
      screen.getByRole("spinbutton", { name: "month," }),
      "05",
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: "day," }),
      "23",
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: "year," }),
      "2022",
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: "hour," }),
      "7",
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: "minute," }),
      "18",
    );

    fireEvent.change(screen.getByRole("textbox", { name: "filename" }), {
      target: { value: "test/filename.py" },
    });
    fireEvent.change(screen.getByRole("spinbutton", { name: "loc" }), {
      target: { value: "10" },
    });
    fireEvent.change(screen.getByRole(role, { name: "lastAuthor" }), {
      target: { value: "test@test.com" },
    });
    fireEvent.change(screen.getByRole(role, { name: "lastCommit" }), {
      target: {
        value: "2fab76140221397cabbc0eae536b41ff38e7540a",
      },
    });

    await userEvent.click(screen.getByText(btnConfirm));

    expect(msgError).toHaveBeenCalledWith(
      "group.toe.lines.addModal.alerts.alreadyExists",
    );

    fireEvent.change(screen.getByRole("textbox", { name: "filename" }), {
      target: { value: "//test/filename.py" },
    });

    await userEvent.click(screen.getByText(btnConfirm));

    expect(msgError).toHaveBeenCalledWith(
      "group.toe.lines.addModal.alerts.invalidFileName",
    );
  });
});
