import _ from "lodash";
import { mixed, number, object, string } from "yup";

import { translate } from "utils/translations/translate";

const localPartPattern = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+/u;
const subdomainPattern = /(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*/u;
const domainPattern = /@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+/u;
const tldPattern = /[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/u;
const emailRegex = new RegExp(
  localPartPattern.source +
    subdomainPattern.source +
    domainPattern.source +
    tldPattern.source,
  "u",
);

const REGEX_TEST = {
  INVALID_FILENAME_STRING:
    /^(?:[a-z]:)?[/\\]{0,2}(?:[./\\ ](?![./\\\n])|[^<>:"|?*./\\ \n])+$/iu,
  INVALID_TEXT_BEGINNING: /^=|^-|^\+|^@|^\t|^\r/u,
  INVALID_TEXT_END: /[./]$/u,
  INVALID_TEXT_FIELD: /[<>:"|?*\\ ]/u,
  INVALID_TEXT_PATTERN: /\/\/{2,}/u,
};

const validationSchema = object().shape({
  filename: string()
    .required(translate.t("validations.required"))
    .isValidCustomRegexValidation(
      REGEX_TEST.INVALID_TEXT_PATTERN,
      "validations.invalidTextPattern",
      "chars",
    )
    .isValidCustomRegexValidation(
      REGEX_TEST.INVALID_TEXT_FIELD,
      "validations.invalidTextField",
      "chars",
    )
    .isValidValue(
      translate.t("validations.invalidFileNameString"),
      REGEX_TEST.INVALID_FILENAME_STRING,
    )
    .isValidCustomRegexValidation(
      REGEX_TEST.INVALID_TEXT_END,
      "validations.invalidTextEnd",
      "chars",
    ),
  lastAuthor: string()
    .required(translate.t("validations.required"))
    .matches(emailRegex, translate.t("validations.email"))
    .isValidCustomRegexValidation(
      REGEX_TEST.INVALID_TEXT_BEGINNING,
      "validations.invalidTextBeginning",
      "chars",
    )
    .isValidCustomRegexValidation(
      REGEX_TEST.INVALID_TEXT_PATTERN,
      "validations.invalidTextPattern",
      "chars",
    ),
  lastCommit: string()
    .required(translate.t("validations.required"))
    .matches(
      /^[A-Fa-f0-9]{40}$|^[A-Fa-f0-9]{64}$/u,
      translate.t("validations.commitHash"),
    ),
  loc: number()
    .required(translate.t("validations.required"))
    .min(0, translate.t("validations.zeroOrPositive"))
    .isValidFunction((value): boolean => {
      return _.isInteger(value) || (_.isString(value) && _.isEmpty(value));
    }, translate.t("validations.integer")),
  modifiedDate: mixed()
    .required(translate.t("validations.required"))
    .isValidDate("datetime")
    .isValidDate("greaterDate"),
});

export { validationSchema, emailRegex, subdomainPattern, domainPattern };
