import { number, object, string } from "yup";

import { translate } from "utils/translations/translate";

const locRange = { max: 1000000, min: 1 };

const validationSchema = object().shape({
  comments: string().isValidTextBeginning().isValidTextField(),
  loc: number()
    .required(translate.t("validations.required"))
    .min(locRange.min, translate.t("validations.between", locRange))
    .max(locRange.max, translate.t("validations.between", locRange))
    .integer(translate.t("validations.integer")),
});

export { locRange, validationSchema };
