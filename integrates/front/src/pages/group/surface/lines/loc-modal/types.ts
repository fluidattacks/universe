import type { IToeLinesData } from "../types";

interface IFormValues {
  loc: number | string;
  comments: string;
}

interface ILocModalFormProps {
  selectedToeLineData: IToeLinesData;
  handleCloseModal: () => void;
}

interface IUpdateToeLinesLocResultAttr {
  updateToeLinesLoc: {
    success: boolean;
  };
}

interface ILocModalProps {
  groupName: string;
  selectedToeLineData: IToeLinesData;
  handleCloseModal: () => void;
  refetchData: () => void;
  setSelectedToeLinesData: (selectedToeLineData: IToeLinesData[]) => void;
}

export type {
  IFormValues,
  ILocModalProps,
  ILocModalFormProps,
  IUpdateToeLinesLocResultAttr,
};
