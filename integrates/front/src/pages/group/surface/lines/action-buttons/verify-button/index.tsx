import { Button } from "@fluidattacks/design";
import { useTranslation } from "react-i18next";

import { Authorize } from "components/@core/authorize";

interface IVerifyButtonProps {
  isDisabled: boolean;
  onVerify: () => void;
}

const VerifyButton = ({
  isDisabled,
  onVerify,
}: Readonly<IVerifyButtonProps>): JSX.Element => {
  const { t } = useTranslation();

  return (
    <Authorize
      can={"integrates_api_mutations_update_toe_lines_attacked_lines_mutate"}
    >
      <Button
        disabled={isDisabled}
        icon={"check"}
        id={"verifyToeLines"}
        onClick={onVerify}
        tooltip={t("group.toe.lines.actionButtons.verifyButton.tooltip")}
        variant={"secondary"}
      >
        {t("buttons.verify")}
      </Button>
    </Authorize>
  );
};

export type { IVerifyButtonProps };
export { VerifyButton };
