interface IGroupToePortsProps {
  isInternal: boolean;
}

interface IToePortEdge {
  node: IToePortAttr;
}

interface IToePortAttr {
  __typename: "ToePort";
  address: string;
  attackedAt: string | null;
  attackedBy: string;
  bePresent: boolean;
  bePresentUntil: string | null;
  firstAttackAt: string | null;
  hasVulnerabilities: boolean;
  port: number;
  root: IIPRootAttr | null;
  seenAt: string | null;
  seenFirstTimeBy: string;
}

interface IIPRootAttr {
  __typename: "IPRoot";
  id: string;
  nickname: string;
}

interface IToePortData {
  attackedAt: string;
  attackedBy: string;
  bePresent: boolean;
  bePresentUntil: string;
  address: string;
  port: number;
  firstAttackAt: string;
  hasVulnerabilities: boolean;
  markedSeenFirstTimeBy: string;
  root: IIPRootAttr | null;
  rootId: string;
  rootNickname: string;
  seenAt: string;
  seenFirstTimeBy: string;
}

export type {
  IGroupToePortsProps,
  IIPRootAttr,
  IToePortAttr,
  IToePortEdge,
  IToePortData,
};
