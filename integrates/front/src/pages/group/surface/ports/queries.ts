import { graphql } from "gql";

graphql(`
  fragment toePortFields on ToePort {
    __typename
    address
    attackedAt @include(if: $canGetAttackedAt)
    attackedBy @include(if: $canGetAttackedBy)
    bePresent
    bePresentUntil @include(if: $canGetBePresentUntil)
    firstAttackAt @include(if: $canGetFirstAttackAt)
    hasVulnerabilities
    port
    seenAt
    seenFirstTimeBy @include(if: $canGetSeenFirstTimeBy)
    root {
      id
      __typename
      nickname
    }
  }
`);

const GET_TOE_PORTS = graphql(`
  query GetToePorts(
    $after: String
    $bePresent: Boolean
    $canGetAttackedAt: Boolean!
    $canGetAttackedBy: Boolean!
    $canGetBePresentUntil: Boolean!
    $canGetFirstAttackAt: Boolean!
    $canGetSeenFirstTimeBy: Boolean!
    $first: Int
    $groupName: String!
    $rootId: ID
  ) {
    group(groupName: $groupName) {
      __typename
      name
      toePorts(
        after: $after
        bePresent: $bePresent
        first: $first
        rootId: $rootId
      ) {
        __typename
        edges {
          node {
            ...toePortFields
          }
        }
        pageInfo {
          endCursor
          hasNextPage
        }
      }
    }
  }
`);

const UPDATE_TOE_PORT = graphql(`
  mutation UpdateToePort(
    $address: String!
    $bePresent: Boolean!
    $canGetAttackedAt: Boolean!
    $canGetAttackedBy: Boolean!
    $canGetBePresentUntil: Boolean!
    $canGetFirstAttackAt: Boolean!
    $canGetSeenFirstTimeBy: Boolean!
    $groupName: String!
    $hasRecentAttack: Boolean
    $port: Int!
    $rootId: String!
    $shouldGetNewToePort: Boolean!
  ) {
    updateToePort(
      address: $address
      bePresent: $bePresent
      groupName: $groupName
      hasRecentAttack: $hasRecentAttack
      port: $port
      rootId: $rootId
    ) {
      success
      toePort @include(if: $shouldGetNewToePort) {
        ...toePortFields
      }
    }
  }
`);

export { GET_TOE_PORTS, UPDATE_TOE_PORT };
