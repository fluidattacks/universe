import { useApolloClient, useMutation, useQuery } from "@apollo/client";
import type { FetchResult } from "@apollo/client";
import { useAbility } from "@casl/react";
import { AppliedFilters } from "@fluidattacks/design";
import type { ColumnDef, Row } from "@tanstack/react-table";
import { isEmpty, isNil, isUndefined } from "lodash";
import { useCallback, useEffect, useMemo, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";

import { ActionButtons } from "./action-buttons";
import { AdditionModal } from "./addition-modal";
import { GET_TOE_PORTS, UPDATE_TOE_PORT } from "./queries";
import type {
  IGroupToePortsProps,
  IToePortAttr,
  IToePortData,
  IToePortEdge,
} from "./types";
import {
  formatToePortData,
  handleUpdateToePortError,
  isEqualRootId,
} from "./utils";

import { editableBePresentFormatter } from "../formatters/editable-be-present";
import { Table } from "components/table";
import { filterDate } from "components/table/utils";
import { authzPermissionsContext } from "context/authz/config";
import { useToEPortsFilters } from "features/toes-ports/filters";
import { filterColumns } from "features/toes-ports/utils";
import type {
  GetToePortsQuery,
  GetToePortsQueryVariables,
  ToePortEdge,
  UpdateToePortMutation,
} from "gql/graphql";
import { useModal, useTable } from "hooks";
import { useAudit } from "hooks/use-audit";
import { formatDate } from "utils/date";
import { formatBoolean, formatStatus, jsxStatus } from "utils/format-helpers";
import { getErrors } from "utils/helpers";
import { Logger } from "utils/logger";
import { msgSuccess } from "utils/notifications";

const GroupToePorts: React.FC<IGroupToePortsProps> = ({
  isInternal,
}): JSX.Element => {
  const { t } = useTranslation();
  const client = useApolloClient();
  const permissions = useAbility(authzPermissionsContext);
  const canGetAttackedAt = permissions.can(
    "integrates_api_resolvers_toe_port_attacked_at_resolve",
  );
  const canGetAttackedBy = permissions.can(
    "integrates_api_resolvers_toe_port_attacked_by_resolve",
  );
  const canGetBePresentUntil = permissions.can(
    "integrates_api_resolvers_toe_port_be_present_until_resolve",
  );
  const canGetFirstAttackAt = permissions.can(
    "integrates_api_resolvers_toe_port_first_attack_at_resolve",
  );
  const canGetSeenFirstTimeBy = permissions.can(
    "integrates_api_resolvers_toe_port_seen_first_time_by_resolve",
  );
  const canUpdateToePort = permissions.can(
    "integrates_api_mutations_update_toe_port_mutate",
  );

  const { groupName } = useParams() as { groupName: string };
  const additionModalProps = useModal("addition-modal");
  const [isMarkingAsAttacked, setIsMarkingAsAttacked] = useState(false);
  const [selectedToePortData, setSelectedToePortData] = useState<
    IToePortData[]
  >([]);
  const [backFilters, setBackFilters] = useState<{
    bePresent: GetToePortsQueryVariables["bePresent"];
    rootId: GetToePortsQueryVariables["rootId"];
  }>({ bePresent: true, rootId: undefined });
  const [filteredDataset, setFilteredDataset] = useState<IToePortData[]>([]);

  const tableRef = useTable(
    "tblToePorts",
    {
      address: false,
      attackedAt: true,
      attackedBy: false,
      bePresent: false,
      bePresentUntil: false,
      firstAttackAt: false,
      hasVulnerabilities: true,
      port: true,
      rootNickname: true,
      seenAt: true,
      seenFirstTimeBy: true,
    },
    [
      "rootNickname",
      "port",
      "hasVulnerabilities",
      "seenAt",
      "bePresent",
      "attackedAt",
      "seenFirstTimeBy",
      "bePresentUntil",
    ],
    { left: ["rootNickname"] },
  );

  const [handleUpdateToePort] = useMutation(UPDATE_TOE_PORT, {
    onError: handleUpdateToePortError,
  });

  const getToePortsVariables = useMemo(
    (): GetToePortsQueryVariables => ({
      canGetAttackedAt,
      canGetAttackedBy,
      canGetBePresentUntil,
      canGetFirstAttackAt,
      canGetSeenFirstTimeBy,
      groupName,
    }),
    [
      canGetAttackedAt,
      canGetAttackedBy,
      canGetBePresentUntil,
      canGetFirstAttackAt,
      canGetSeenFirstTimeBy,
      groupName,
    ],
  );
  const { addAuditEvent } = useAudit();
  const { data, fetchMore, loading, refetch } = useQuery(GET_TOE_PORTS, {
    fetchPolicy: "network-only",
    nextFetchPolicy: "cache-first",
    onCompleted: (): void => {
      addAuditEvent("Group.ToePorts", groupName);
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.error("Couldn't load toe ports", error);
      });
    },
    variables: {
      ...backFilters,
      ...getToePortsVariables,
      first: 150,
    },
  });
  const pageInfo =
    data === undefined ? undefined : data.group.toePorts.pageInfo;
  const toePorts = useMemo((): IToePortData[] => {
    const toePortsEdges = data === undefined ? [] : data.group.toePorts.edges;

    return (toePortsEdges ?? [])
      .filter(
        (edge): edge is ToePortEdge & { node: IToePortData } =>
          edge?.node !== null,
      )
      .map(({ node }): IToePortData => formatToePortData(node as IToePortAttr));
  }, [data]);

  const storeUpdatedPort = useCallback(
    (
      rootId: string,
      address: string,
      port: number,
      updatedToePort: IToePortAttr,
    ): void => {
      if (data?.group) {
        client.writeQuery({
          data: {
            ...data,
            group: {
              ...data.group,
              toePorts: {
                ...data.group.toePorts,
                edges: (data.group.toePorts.edges ?? [])
                  .filter((value): value is IToePortEdge => value !== null)
                  .map(
                    (value): IToePortEdge =>
                      value.node.address === address &&
                      value.node.port === port &&
                      isEqualRootId(value.node.root, rootId)
                        ? {
                            node: updatedToePort,
                          }
                        : {
                            node: value.node,
                          },
                  ),
              },
            },
          },
          query: GET_TOE_PORTS,
          variables: {
            ...getToePortsVariables,
            first: 150,
          },
        });
      }
    },
    [client, data, getToePortsVariables],
  );

  const handleUpdateToePortBePresent = useCallback(
    async (row: IToePortData): Promise<void> => {
      const { rootId, address, port, bePresent } = row;

      const result = await handleUpdateToePort({
        variables: {
          ...getToePortsVariables,
          address,
          bePresent: !bePresent,
          groupName,
          hasRecentAttack: undefined,
          port,
          rootId,
          shouldGetNewToePort: true,
        },
      });

      if (!isNil(result.data) && result.data.updateToePort.success) {
        const updatedToePort = result.data.updateToePort
          .toePort as IToePortAttr;
        if (!isUndefined(updatedToePort)) {
          setSelectedToePortData(
            selectedToePortData
              .map(
                (toePortData): IToePortData =>
                  toePortData.address === address &&
                  toePortData.port === port &&
                  toePortData.rootId === rootId
                    ? formatToePortData(updatedToePort)
                    : toePortData,
              )
              .filter((toePortData): boolean => toePortData.bePresent),
          );
          storeUpdatedPort(rootId, address, port, updatedToePort);
        }
        msgSuccess(
          t("group.toe.ports.alerts.updatePort"),
          t("groupAlerts.updatedTitle"),
        );
      }
    },
    [
      getToePortsVariables,
      groupName,
      handleUpdateToePort,
      selectedToePortData,
      storeUpdatedPort,
      t,
    ],
  );

  const columns = useMemo(
    (): ColumnDef<IToePortData>[] => [
      {
        accessorKey: "rootNickname",
        header: t("group.toe.ports.root"),
        meta: { filterType: "select" },
      },
      {
        accessorKey: "address",
        header: t("group.toe.ports.address"),
        meta: { filterType: "select" },
      },
      {
        accessorKey: "port",
        header: t("group.toe.ports.port"),
      },
      {
        accessorFn: (row: IToePortData): string =>
          formatStatus(row.hasVulnerabilities),
        cell: (cell): JSX.Element => jsxStatus(String(cell.getValue())),
        header: String(t("group.toe.ports.status")),
        id: "hasVulnerabilities",
        meta: { filterType: "select" },
      },
      {
        accessorKey: "seenAt",
        cell: (cell): string => formatDate(cell.getValue<string>()),
        filterFn: filterDate,
        header: t("group.toe.ports.seenAt"),
        meta: { filterType: "dateRange" },
      },
      {
        accessorFn: (row: IToePortData): string => {
          return formatBoolean(row.bePresent);
        },
        cell: (cell): JSX.Element =>
          editableBePresentFormatter(
            canUpdateToePort && isInternal,
            handleUpdateToePortBePresent,
            cell.row.original,
          ),
        header: t("group.toe.ports.bePresent"),
        id: "bePresent",
        meta: { filterType: "select" },
      },
      {
        accessorKey: "attackedAt",
        cell: (cell): string => formatDate(cell.getValue<string>()),
        filterFn: filterDate,
        header: t("group.toe.ports.attackedAt"),
        id: "attackedAt",
        meta: { filterType: "dateRange" },
      },
      {
        accessorKey: "attackedBy",
        header: t("group.toe.ports.attackedBy"),
        id: "attackedBy",
      },
      {
        accessorKey: "firstAttackAt",
        cell: (cell): string => formatDate(cell.getValue<string>()),
        filterFn: filterDate,
        header: t("group.toe.ports.firstAttackAt"),
        id: "firstAttackAt",
        meta: { filterType: "dateRange" },
      },
      {
        accessorKey: "seenFirstTimeBy",
        header: t("group.toe.ports.seenFirstTimeBy"),
        id: "seenFirstTimeBy",
      },
      {
        accessorKey: "bePresentUntil",
        cell: (cell): string => formatDate(cell.getValue<string>()),
        filterFn: filterDate,
        header: t("group.toe.ports.bePresentUntil"),
        id: "bePresentUntil",
        meta: { filterType: "dateRange" },
      },
    ],
    [handleUpdateToePortBePresent, canUpdateToePort, isInternal, t],
  );

  const tableColumns = filterColumns(columns, isInternal, getToePortsVariables);

  useEffect((): void => {
    if (!isUndefined(pageInfo)) {
      if (pageInfo.hasNextPage) {
        void fetchMore({
          updateQuery: (
            prev: GetToePortsQuery,
            { fetchMoreResult },
          ): GetToePortsQuery => {
            return {
              ...prev,
              group: {
                ...prev.group,
                toePorts: {
                  ...prev.group.toePorts,
                  edges: [
                    ...(prev.group.toePorts.edges ?? []),
                    ...(fetchMoreResult.group.toePorts.edges ?? []),
                  ],
                  pageInfo: fetchMoreResult.group.toePorts.pageInfo,
                },
              },
            };
          },
          variables: { after: pageInfo.endCursor, first: 1200 },
        });
      }
    }
  }, [pageInfo, fetchMore]);
  useEffect((): void => {
    setSelectedToePortData([]);
    void refetch();
  }, [refetch]);

  const toggleAdd = useCallback((): void => {
    additionModalProps.open();
  }, [additionModalProps]);

  const handleOnMarkAsAttackedCompleted = useCallback(
    (result: FetchResult<UpdateToePortMutation>): void => {
      if (!isNil(result.data) && result.data.updateToePort.success) {
        msgSuccess(
          t("group.toe.ports.alerts.markAsAttacked.success"),
          t("groupAlerts.updatedTitle"),
        );
        void refetch();
        setSelectedToePortData([]);
      }
    },
    [refetch, t],
  );

  const handleMarkAsAttacked = useCallback(async (): Promise<void> => {
    const presentSelectedToePortData = selectedToePortData.filter(
      (toePortData): boolean => toePortData.bePresent,
    );
    setIsMarkingAsAttacked(true);
    const results = await Promise.all(
      presentSelectedToePortData.map(
        async (toePortData): Promise<FetchResult<UpdateToePortMutation>> =>
          handleUpdateToePort({
            variables: {
              ...getToePortsVariables,
              address: toePortData.address,
              bePresent: toePortData.bePresent,
              groupName,
              hasRecentAttack: true,
              port: toePortData.port,
              rootId: toePortData.rootId,
              shouldGetNewToePort: false,
            },
          }),
      ),
    );
    const errors = getErrors<UpdateToePortMutation>(results);

    if (!isEmpty(results) && isEmpty(errors)) {
      handleOnMarkAsAttackedCompleted(results[0]);
    } else {
      void refetch();
    }
    setIsMarkingAsAttacked(false);
  }, [
    getToePortsVariables,
    groupName,
    handleOnMarkAsAttackedCompleted,
    handleUpdateToePort,
    refetch,
    selectedToePortData,
  ]);

  const enabledRows = useCallback((row: Row<IToePortData>): boolean => {
    return row.original.bePresent;
  }, []);

  const { Filters, options, removeFilter } = useToEPortsFilters({
    dataset: toePorts,
    groupName,
    setBackFilters,
    setFilteredDataset,
  });

  return (
    <React.StrictMode>
      <Table
        columns={tableColumns}
        csvConfig={{ export: true }}
        data={filteredDataset}
        extraButtons={
          <ActionButtons
            arePortsSelected={selectedToePortData.length > 0}
            isAdding={additionModalProps.isOpen}
            isInternal={isInternal}
            isMarkingAsAttacked={isMarkingAsAttacked}
            onAdd={toggleAdd}
            onMarkAsAttacked={handleMarkAsAttacked}
          />
        }
        filters={<Filters />}
        filtersApplied={
          <AppliedFilters onClose={removeFilter} options={options} />
        }
        loadingData={loading}
        options={{
          columnToggle: true,
          enableRowSelection: enabledRows,
        }}
        rowSelectionSetter={
          !isInternal || !canUpdateToePort ? undefined : setSelectedToePortData
        }
        rowSelectionState={selectedToePortData}
        tableRef={tableRef}
      />
      <AdditionModal
        groupName={groupName}
        modalRef={additionModalProps}
        refetchData={refetch}
      />
    </React.StrictMode>
  );
};

export { GroupToePorts };
