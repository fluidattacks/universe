import { Button } from "@fluidattacks/design";
import * as React from "react";
import { useTranslation } from "react-i18next";

import type { IAttackedButtonProps } from "./types";

import { Authorize } from "components/@core/authorize";

const AttackedButton: React.FC<IAttackedButtonProps> = ({
  isDisabled,
  onAttacked,
}: IAttackedButtonProps): JSX.Element => {
  const { t } = useTranslation();

  return (
    <Authorize can={"integrates_api_mutations_update_toe_port_mutate"}>
      <Button
        disabled={isDisabled}
        icon={"plus"}
        id={"attackedToePorts"}
        onClick={onAttacked}
        tooltip={t("group.toe.ports.actionButtons.attackedButton.tooltip")}
        variant={"secondary"}
      >
        {t("group.toe.ports.actionButtons.attackedButton.text")}
      </Button>
    </Authorize>
  );
};

export type { IAttackedButtonProps };
export { AttackedButton };
