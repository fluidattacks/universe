import * as React from "react";

import { AddButton } from "./add-button";
import { AttackedButton } from "./attacked-button";
import type { IActionButtonsProps } from "./types";

const ActionButtons: React.FC<IActionButtonsProps> = ({
  arePortsSelected,
  isAdding,
  isInternal,
  isMarkingAsAttacked,
  onAdd,
  onMarkAsAttacked,
}): JSX.Element | null => {
  return isInternal ? (
    <React.StrictMode>
      <AddButton isDisabled={isAdding} onAdd={onAdd} />
      <AttackedButton
        isDisabled={isMarkingAsAttacked || !arePortsSelected}
        onAttacked={onMarkAsAttacked}
      />
    </React.StrictMode>
  ) : null;
};

export { ActionButtons };
