import { graphql } from "gql";

const ADD_TOE_PORT = graphql(`
  mutation AddToePort(
    $address: String!
    $groupName: String!
    $port: Int!
    $rootId: String!
  ) {
    addToePort(
      address: $address
      groupName: $groupName
      port: $port
      rootId: $rootId
    ) {
      success
    }
  }
`);

const GET_ROOTS = graphql(`
  query GetRootsInfoAtPorts($groupName: String!) {
    group(groupName: $groupName) {
      __typename
      name
      roots {
        ... on GitRoot {
          __typename
        }
        ... on IPRoot {
          id
          __typename
          address
          nickname
          state
        }
        ... on URLRoot {
          __typename
        }
      }
    }
  }
`);

export { ADD_TOE_PORT, GET_ROOTS };
