import { useMutation, useQuery } from "@apollo/client";
import _ from "lodash";
import { StrictMode, useCallback, useState } from "react";
import { useTranslation } from "react-i18next";
import { object, string } from "yup";

import { ADD_TOE_PORT, GET_ROOTS } from "./queries";
import type { IAdditionModalProps, IFormValues, IIPRootAttr } from "./types";

import { InnerForm } from "components/form";
import { FormModal } from "components/form-modal";
import { InputNumber, Select } from "components/input";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

const AdditionModal = ({
  groupName,
  modalRef,
  refetchData,
}: IAdditionModalProps): JSX.Element => {
  const { t } = useTranslation();
  const [isSubmitting, setIsSubmitting] = useState(false);
  const { close, isOpen } = modalRef;

  // GraphQL operations
  const { data: rootsData } = useQuery(GET_ROOTS, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.error("Couldn't load roots", error);
      });
    },
    skip: !isOpen,
    variables: { groupName },
  });
  const [handleAddToePort] = useMutation(ADD_TOE_PORT, {
    onCompleted: (data): void => {
      setIsSubmitting(false);
      if (data.addToePort.success) {
        msgSuccess(
          t("group.toe.ports.addModal.alerts.success"),
          t("groupAlerts.titleSuccess"),
        );
        refetchData();
        close();
      }
    },
    onError: (errors): void => {
      setIsSubmitting(false);
      errors.graphQLErrors.forEach((error): void => {
        if (error.message === "Exception - Toe port already exists") {
          msgError(t("group.toe.ports.addModal.alerts.alreadyExists"));
        } else {
          msgError(t("groupAlerts.errorTextsad"));
          Logger.warning("An error occurred adding toe port", error);
        }
      });
    },
  });

  // Data management
  const ipRoots = rootsData?.group.roots
    ? rootsData.group.roots
        .map((root): IIPRootAttr | null =>
          root.__typename === "IPRoot" ? root : null,
        )
        .filter((root): root is IIPRootAttr => root !== null)
    : [];
  const activeIPRoots = ipRoots.filter(
    (root: IIPRootAttr): boolean => root.state === "ACTIVE",
  );
  const activeIPRootAddress = Object.fromEntries(
    activeIPRoots.map((root: IIPRootAttr): [string, string] => [
      root.id,
      root.address,
    ]),
  );
  const ipRootsOptions = activeIPRoots.map(
    (root: IIPRootAttr): { header: string; value: string } => ({
      header: `${root.nickname} - ${root.address}`,
      value: root.id,
    }),
  );

  const handleSubmit = useCallback(
    (values: IFormValues): void => {
      setIsSubmitting(true);
      void handleAddToePort({
        variables: {
          address: activeIPRootAddress[values.rootId],
          groupName,
          port: _.toInteger(values.port),
          rootId: values.rootId,
        },
      });
    },
    [activeIPRootAddress, groupName, handleAddToePort],
  );

  return (
    <StrictMode>
      {rootsData === undefined ? undefined : (
        <FormModal
          initialValues={{
            port: "",
            rootId: activeIPRoots.length > 0 ? activeIPRoots[0].id : "",
          }}
          modalRef={modalRef}
          name={"addToePort"}
          onSubmit={handleSubmit}
          size={"sm"}
          title={t("group.toe.ports.addModal.title")}
          validationSchema={object().shape({
            port: string()
              .required(t("validations.required"))
              .matches(/^\d+$/u, t("validations.numeric"))
              .isValidFunction((value): boolean => {
                if (value === undefined || _.isEmpty(value)) {
                  return false;
                }
                const port = _.toInteger(value);
                const MAX_PORT = 65535;

                return port >= 0 && port <= MAX_PORT;
              }, t("validations.portRange")),
            rootId: string().required(t("validations.required")),
          })}
        >
          {({ dirty }): JSX.Element => {
            return (
              <InnerForm
                onCancel={close}
                submitDisabled={isSubmitting || !dirty}
              >
                <Select
                  items={ipRootsOptions}
                  label={t("group.toe.ports.addModal.fields.IPRoot")}
                  name={"rootId"}
                />
                <InputNumber
                  label={t("group.toe.ports.addModal.fields.port")}
                  name={"port"}
                />
              </InnerForm>
            );
          }}
        </FormModal>
      )}
    </StrictMode>
  );
};

export { AdditionModal };
