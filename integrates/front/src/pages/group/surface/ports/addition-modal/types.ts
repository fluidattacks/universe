import type { IUseModal } from "hooks/use-modal";

interface IFormValues {
  port: string;
  rootId: string;
}

interface IAdditionModalProps {
  groupName: string;
  modalRef: IUseModal;
  refetchData: () => void;
}

interface IGitRootAttr {
  __typename: "GitRoot";
}

interface IIPRootAttr {
  __typename: "IPRoot";
  address: string;
  id: string;
  nickname: string;
  state: "ACTIVE" | "INACTIVE";
}

interface IURLRootAttr {
  __typename: "URLRoot";
}

type TRoot = IGitRootAttr | IIPRootAttr | IURLRootAttr;

export type {
  IFormValues,
  IAdditionModalProps,
  TRoot,
  IGitRootAttr,
  IIPRootAttr,
  IURLRootAttr,
};
