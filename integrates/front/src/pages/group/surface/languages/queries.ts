import { graphql } from "gql";

const GET_TOE_LANGUAGES = graphql(`
  query GetToeLanguages($groupName: String!) {
    group(groupName: $groupName) {
      name
      codeLanguages {
        language
        loc
      }
    }
  }
`);

export { GET_TOE_LANGUAGES };
