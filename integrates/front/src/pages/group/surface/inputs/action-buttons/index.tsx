import * as React from "react";

import { AddButton } from "./add-button";
import { AttackedButton } from "./attacked-button";
import type { IActionButtonsProps } from "./types";

import { ExportButton } from "components/table/export-multiple-csv/export-button";
import { ReportType } from "gql/graphql";

const ActionButtons: React.FC<IActionButtonsProps> = ({
  data,
  fetchReportData,
  areInputsSelected,
  isAdding,
  isInternal,
  isMarkingAsAttacked,
  onAdd,
  onMarkAsAttacked,
  size,
}): JSX.Element | null => {
  return (
    <React.Fragment>
      {isInternal ? (
        <React.StrictMode>
          <AddButton isDisabled={isAdding} onAdd={onAdd} />
          <AttackedButton
            isDisabled={isMarkingAsAttacked || !areInputsSelected}
            onAttacked={onMarkAsAttacked}
          />
        </React.StrictMode>
      ) : null}
      <ExportButton
        data={data}
        fetchReportData={fetchReportData}
        reportType={ReportType.ToeInputs}
        size={size}
      />
    </React.Fragment>
  );
};

export { ActionButtons };
