import isNil from "lodash/isNil";

import type { IGitRootAttr, IToeInputAttr, IToeInputData } from "./types";

import { markSeenFirstTimeBy } from "../utils";

const isEqualRootId = (root: IGitRootAttr | null, rootId: string): boolean => {
  if (isNil(root)) {
    return rootId === "";
  }

  return root.id === rootId;
};

const formatToeInputData = (toeInputAttr: IToeInputAttr): IToeInputData => ({
  ...toeInputAttr,
  attackedAt: toeInputAttr.attackedAt ?? "-",
  bePresentUntil: toeInputAttr.bePresentUntil ?? "-",
  firstAttackAt: toeInputAttr.firstAttackAt ?? "-",
  markedSeenFirstTimeBy: markSeenFirstTimeBy(toeInputAttr.seenFirstTimeBy),
  rootId: isNil(toeInputAttr.root) ? "" : toeInputAttr.root.id,
  rootNickname: isNil(toeInputAttr.root) ? "" : toeInputAttr.root.nickname,
  seenAt: toeInputAttr.seenAt ?? "-",
});

export { formatToeInputData, isEqualRootId };
