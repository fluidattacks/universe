import { useMutation } from "@apollo/client";
import { Col, Row, Span } from "@fluidattacks/design";
import _ from "lodash";
import { StrictMode, useCallback, useMemo, useState } from "react";
import { useTranslation } from "react-i18next";

import { EntryPointField } from "./entry-point-field";
import { EnvironmentUrlField } from "./environment-url-field";
import { ADD_TOE_INPUT } from "./queries";
import type {
  IAddToeInputResultAttr,
  IAdditionModalProps,
  IFormValues,
  IGitEnvironmentURLAttr,
  IGitRootAttr,
  IURLRootAttr,
} from "./types";
import {
  isActiveGitRoot,
  isActiveURLRoot,
  isGitRoot,
  isURLRoot,
} from "./utils";
import { validationSchema } from "./validations";

import { InnerForm } from "components/form";
import { FormModal } from "components/form-modal";
import { Input } from "components/input";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

const AdditionModal = ({
  groupName,
  modalRef,
  rootsData,
  refetchData,
}: Readonly<IAdditionModalProps>): JSX.Element => {
  const { t } = useTranslation();
  const [host, setHost] = useState<string | undefined>();
  const { close } = modalRef;

  // GraphQL operations
  const [handleAddToeInput] = useMutation(ADD_TOE_INPUT, {
    onCompleted: (data: IAddToeInputResultAttr): void => {
      if (data.addToeInput.success) {
        msgSuccess(
          t("group.toe.inputs.addModal.alerts.success"),
          t("groupAlerts.titleSuccess"),
        );
        refetchData();
        close();
      }
    },
    onError: (errors): void => {
      errors.graphQLErrors.forEach((error): void => {
        switch (error.message) {
          case "Exception - Toe input already exists":
            msgError(t("group.toe.inputs.addModal.alerts.alreadyExists"));
            break;
          case "Exception - The root does not have the component":
            msgError(t("group.toe.inputs.addModal.alerts.invalidComponent"));
            break;
          case "Exception - The URL is not valid":
            msgError(t("group.toe.inputs.addModal.alerts.invalidUrl"));
            break;
          case "Exception - Invalid characters":
            msgError(t("group.toe.inputs.addModal.alerts.invalidCharacter"));
            break;
          case "Exception - The environment has been excluded":
            msgError(t("group.toe.inputs.alerts.excludedUrlFromScope"));
            break;
          default:
            msgError(t("groupAlerts.errorTextsad"));
            Logger.warning("An error occurred adding toe input", error);
        }
      });
    },
  });

  // Data management
  const roots = useMemo(
    (): (IGitRootAttr | IURLRootAttr)[] =>
      rootsData === undefined
        ? []
        : [
            ...rootsData.group.roots.filter(isGitRoot).filter(isActiveGitRoot),
            ...rootsData.group.roots.filter(isURLRoot).filter(isActiveURLRoot),
          ],
    [rootsData],
  );
  const nicknames = roots.map((root): string => root.nickname);
  const envUrlsByRoot = _.defaultTo(
    rootsData?.group.gitEnvironmentUrls,
    [] as IGitEnvironmentURLAttr[],
  ).reduce<Record<string, IGitEnvironmentURLAttr[]>>(
    (previous, currentValue): Record<string, IGitEnvironmentURLAttr[]> => {
      return {
        ...previous,
        [currentValue.rootId]: [
          ..._.get(
            previous,
            currentValue.rootId,
            [] as IGitEnvironmentURLAttr[],
          ),
          currentValue,
        ],
      };
    },
    {},
  );
  const getEnvUrlByRoot = useCallback(
    (rootId: string | undefined): IGitEnvironmentURLAttr[] =>
      _.get(envUrlsByRoot, rootId ?? "", [] as IGitEnvironmentURLAttr[]),
    [envUrlsByRoot],
  );

  const handleSubmit = useCallback(
    async (values: IFormValues): Promise<void> => {
      if (!_.isUndefined(host)) {
        const selectedRoot = roots.find(
          (root): boolean => root.nickname === values.rootNickname,
        );

        const rootId = selectedRoot?.id ?? "";

        const environmentId =
          getEnvUrlByRoot(rootId).find(
            (envUrl: IGitEnvironmentURLAttr): boolean =>
              envUrl.url === values.environmentUrl,
          )?.id ?? "";

        await handleAddToeInput({
          variables: {
            component: `${host}${values.path}`,
            entryPoint: values.entryPoint,
            environmentId,
            groupName,
            rootId,
          },
        });
      }
    },
    [getEnvUrlByRoot, groupName, handleAddToeInput, host, roots],
  );

  return (
    <StrictMode>
      {rootsData === undefined ? undefined : (
        <FormModal
          initialValues={{
            entryPoint: "",
            environmentUrl:
              !_.isEmpty(roots) &&
              isGitRoot(roots[0]) &&
              !_.isEmpty(getEnvUrlByRoot(roots[0].id))
                ? getEnvUrlByRoot(roots[0].id)[0].url
                : "",
            path: "",
            rootId: _.isEmpty(roots) ? undefined : roots[0].id,
            rootNickname: _.isEmpty(roots) ? undefined : roots[0].nickname,
          }}
          modalRef={modalRef}
          name={"addToeInput"}
          onSubmit={handleSubmit}
          size={"sm"}
          title={t("group.toe.inputs.addModal.title")}
          validationSchema={validationSchema(roots, host)}
        >
          {({ setFieldValue, values }): JSX.Element => {
            const { environmentUrl, rootNickname } = values;
            const selectedRoot = _.isUndefined(rootNickname)
              ? undefined
              : roots.find((root): boolean => root.nickname === rootNickname);

            return (
              <InnerForm onCancel={close}>
                <Row>
                  <Col>
                    <Input
                      label={t("group.toe.inputs.addModal.fields.root")}
                      name={"rootNickname"}
                      suggestions={nicknames}
                    />
                  </Col>
                  <Col>
                    <EnvironmentUrlField
                      getEnvUrlByRoot={getEnvUrlByRoot}
                      selectedRoot={selectedRoot}
                      setFieldValue={setFieldValue}
                      values={values}
                    />
                  </Col>
                </Row>
                <Row>
                  {_.isUndefined(host) ? undefined : (
                    <Span size={"sm"}>{host}</Span>
                  )}
                  {_.isString(host) && host.includes("?") ? undefined : (
                    <Input
                      label={t("group.toe.inputs.addModal.fields.component")}
                      name={"path"}
                      placeholder={t("group.toe.inputs.addModal.fields.path")}
                    />
                  )}
                </Row>
                <Row>
                  <EntryPointField
                    environmentUrl={environmentUrl}
                    selectedRoot={selectedRoot}
                    setHost={setHost}
                  />
                </Row>
              </InnerForm>
            );
          }}
        </FormModal>
      )}
    </StrictMode>
  );
};

export { AdditionModal };
