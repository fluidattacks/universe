import { graphql } from "gql";

const ADD_TOE_INPUT = graphql(`
  mutation AddToeInput(
    $component: String!
    $entryPoint: String!
    $environmentId: String!
    $groupName: String!
    $rootId: String!
  ) {
    addToeInput(
      component: $component
      entryPoint: $entryPoint
      environmentId: $environmentId
      groupName: $groupName
      rootId: $rootId
    ) {
      success
    }
  }
`);

const GET_ROOTS = graphql(`
  query GetRootsInfoAtInputs($groupName: String!) {
    group(groupName: $groupName) {
      __typename
      name
      gitEnvironmentUrls {
        id
        rootId
        url
        urlType
      }
      roots {
        ... on GitRoot {
          id
          __typename
          nickname
          state
        }
        ... on IPRoot {
          id
          __typename
          nickname
          state
        }
        ... on URLRoot {
          id
          __typename
          host
          nickname
          path
          port
          protocol
          query
          state
        }
      }
    }
  }
`);

export { ADD_TOE_INPUT, GET_ROOTS };
