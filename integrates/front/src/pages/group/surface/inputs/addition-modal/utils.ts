import _ from "lodash";

import type { IGitRootAttr, IURLRootAttr, TRoot } from "./types";

const getGitRootHost = (environmentUrl: string): string => {
  if (environmentUrl.endsWith("/")) {
    return environmentUrl;
  }

  return `${environmentUrl}/`;
};

const getUrlRootHost = (root: IURLRootAttr): string => {
  const urlRootWithPort = root.port
    ? `${root.protocol.toLowerCase()}://${root.host}:${root.port}${root.path}`
    : `${root.protocol.toLowerCase()}://${root.host}${root.path}`;

  if (_.isNull(root.query)) {
    if (urlRootWithPort.endsWith("/")) {
      return urlRootWithPort;
    }

    return `${urlRootWithPort}/`;
  }

  return `${urlRootWithPort}?${root.query}`;
};

const isGitRoot = (root: TRoot): root is IGitRootAttr =>
  root.__typename === "GitRoot";

const isURLRoot = (root: TRoot): root is IURLRootAttr =>
  root.__typename === "URLRoot";

const isActiveGitRoot = (root: IGitRootAttr): boolean =>
  root.state === "ACTIVE";

const isActiveURLRoot = (root: IURLRootAttr): boolean =>
  root.state === "ACTIVE";

const getNewHost = (
  environmentUrl?: string,
  selectedRoot?: TRoot,
): string | undefined => {
  if (!_.isUndefined(selectedRoot)) {
    if (isGitRoot(selectedRoot) && !_.isUndefined(environmentUrl)) {
      return getGitRootHost(environmentUrl);
    } else if (isURLRoot(selectedRoot)) {
      return getUrlRootHost(selectedRoot);
    }
  }

  return undefined;
};

export { isActiveGitRoot, isActiveURLRoot, isGitRoot, isURLRoot, getNewHost };
