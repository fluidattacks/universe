import { PureAbility } from "@casl/ability";
import { screen, waitFor, within } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import { useTranslation } from "react-i18next";
import { Route, Routes } from "react-router-dom";

import { GET_ROOTS } from "./addition-modal/queries";
import {
  GET_TOE_INPUTS,
  GET_TOE_INPUTS_COMPONENTS,
  UPDATE_TOE_INPUT,
} from "./queries";

import { GroupToeInputs } from ".";
import { authzPermissionsContext } from "context/authz/config";
import {
  type GetRootsInfoAtInputsQuery as GetRootsInfoAtInputs,
  type GetToeInputComponentsQuery as GetToeInputComponents,
  type GetToeInputsQuery as GetToeInputs,
  ResourceState,
  type UpdateToeInputMutation as UpdateToeInput,
} from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage, IMessage } from "mocks/types";
import { msgError, msgSuccess } from "utils/notifications";
import { selectOption } from "utils/test-utils";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

describe("groupToeInputs", (): void => {
  const { t } = useTranslation();
  const graphqlMocked = graphql.link(LINK);
  const mockedPermissions = new PureAbility<string>([
    { action: "integrates_api_resolvers_toe_input_attacked_at_resolve" },
    { action: "integrates_api_resolvers_toe_input_attacked_by_resolve" },
    { action: "integrates_api_resolvers_toe_input_be_present_until_resolve" },
    { action: "integrates_api_resolvers_toe_input_first_attack_at_resolve" },
    { action: "integrates_api_resolvers_toe_input_seen_first_time_by_resolve" },
  ]);

  const clearFilters = async (): Promise<void> => {
    const cancelText = "filter.cancel";

    expect(screen.getByText(cancelText)).toBeInTheDocument();

    await userEvent.click(screen.getByText(cancelText));
  };

  const openFilters = async (): Promise<void> => {
    const filterId = "filterBtn";

    expect(document.getElementById(filterId)).toBeInTheDocument();

    await userEvent.click(document.getElementById(filterId) as HTMLElement);
  };

  const memoryRouter = {
    initialEntries: ["/unittesting/surface/inputs"],
  };

  const mockedToeInputs = graphqlMocked.query(
    GET_TOE_INPUTS,
    ({ variables }): StrictResponse<IErrorMessage | { data: GetToeInputs }> => {
      const {
        canGetAttackedAt,
        canGetAttackedBy,
        canGetBePresentUntil,
        canGetFirstAttackAt,
        canGetSeenFirstTimeBy,
        first,
        groupName,
        rootNickname,
        component,
      } = variables;

      if (rootNickname === "test_nickname") {
        return HttpResponse.json({
          data: {
            group: {
              __typename: "Group",
              name: "unittesting",
              toeInputs: {
                __typename: "ToeInputsConnection",
                edges: [
                  {
                    node: {
                      __typename: "ToeInput",
                      ...{
                        attackedAt: "2020-01-02T00:00:00-05:00",
                        attackedBy: "",
                        bePresent: true,
                        bePresentUntil: null,
                        component: "test.com/api/Test",
                        entryPoint: "idTest",
                        firstAttackAt: "2020-02-19T15:41:04+00:00",
                        hasVulnerabilities: false,
                        root: {
                          __typename: "GitRoot",
                          id: "1a32cab8-7b4c-4761-a0a5-85cb8b64ce68",
                          nickname: "test_nickname",
                        },
                        seenAt: "2000-01-01T05:00:00+00:00",
                        seenFirstTimeBy: "",
                      },
                    },
                  },
                ],
                pageInfo: {
                  endCursor: "bnVsbA==",
                  hasNextPage: false,
                },
                total: 1,
              },
            },
          },
        });
      }

      if (component === "test.component.com/api/Test") {
        return HttpResponse.json({
          data: {
            group: {
              __typename: "Group",
              name: "unittesting",
              toeInputs: {
                __typename: "ToeInputsConnection",
                edges: [
                  {
                    node: {
                      __typename: "ToeInput",
                      ...{
                        attackedAt: "2020-01-02T00:00:00-05:00",
                        attackedBy: "",
                        bePresent: true,
                        bePresentUntil: null,
                        component: "test.component.com/api/Test",
                        entryPoint: "idTest",
                        firstAttackAt: "2020-02-19T15:41:04+00:00",
                        hasVulnerabilities: false,
                        root: null,
                        seenAt: "2000-01-01T05:00:00+00:00",
                        seenFirstTimeBy: "",
                      },
                    },
                  },
                ],
                pageInfo: {
                  endCursor: "bnVsbA==",
                  hasNextPage: false,
                },
                total: 1,
              },
            },
          },
        });
      }
      if (
        canGetAttackedAt &&
        canGetAttackedBy &&
        canGetBePresentUntil &&
        canGetFirstAttackAt &&
        canGetSeenFirstTimeBy &&
        first === 150 &&
        groupName === "unittesting"
      ) {
        return HttpResponse.json({
          data: {
            group: {
              __typename: "Group",
              name: "unittesting",
              toeInputs: {
                __typename: "ToeInputsConnection",
                edges: [
                  {
                    node: {
                      __typename: "ToeInput",
                      ...{
                        attackedAt: "2020-01-02T00:00:00-05:00",
                        attackedBy: "",
                        bePresent: true,
                        bePresentUntil: null,
                        component: "test.com/api/Test",
                        entryPoint: "idTest",
                        firstAttackAt: "2020-02-19T15:41:04+00:00",
                        hasVulnerabilities: false,
                        root: {
                          __typename: "GitRoot",
                          id: "1a32cab8-7b4c-4761-a0a5-85cb8b64ce68",
                          nickname: "test_nickname2",
                        },
                        seenAt: "2000-01-01T05:00:00+00:00",
                        seenFirstTimeBy: "",
                      },
                    },
                  },
                  {
                    node: {
                      __typename: "ToeInput",
                      ...{
                        attackedAt: "2021-02-02T00:00:00-05:00",
                        attackedBy: "",
                        bePresent: true,
                        bePresentUntil: null,
                        component: "test.com/test/test.aspx",
                        entryPoint: "btnTest",
                        firstAttackAt: "",
                        hasVulnerabilities: true,
                        root: {
                          __typename: "GitRoot",
                          id: "1a32cab8-7b4c-4761-0a5-85cb8b64ce68",
                          nickname: "test_nickname",
                        },
                        seenAt: "2020-03-14T00:00:00-05:00",
                        seenFirstTimeBy: "test@test.com",
                      },
                    },
                  },
                  {
                    node: {
                      __typename: "ToeInput",
                      ...{
                        attackedAt: "2021-02-11T00:00:00-05:00",
                        attackedBy: "",
                        bePresent: false,
                        bePresentUntil: null,
                        component: "test.component.com/api/Test",
                        entryPoint: "-",
                        firstAttackAt: null,
                        hasVulnerabilities: true,
                        root: null,
                        seenAt: "2020-01-11T00:00:00-05:00",
                        seenFirstTimeBy: "test2@test.com",
                      },
                    },
                  },
                ],
                pageInfo: {
                  endCursor: "bnVsbA==",
                  hasNextPage: false,
                },
                total: 3,
              },
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Error getting group inputs surface")],
      });
    },
  );

  const mockedComponents = graphqlMocked.query(
    GET_TOE_INPUTS_COMPONENTS,
    ({
      variables,
    }): StrictResponse<IErrorMessage | { data: GetToeInputComponents }> => {
      const { groupName } = variables;
      if (groupName === "unittesting") {
        return HttpResponse.json({
          data: {
            group: {
              __typename: "Group",
              name: "unittesting",
              toeInputs: {
                __typename: "ToeInputsConnection",
                edges: [
                  {
                    node: {
                      component: "test.com/api/Test",
                    },
                  },
                  {
                    node: {
                      component: "test.com/test/test.aspx",
                    },
                  },
                  {
                    node: {
                      component: "test.component.com/api/Test",
                    },
                  },
                ],
              },
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [
          new GraphQLError("Error getting group inputs surface components"),
        ],
      });
    },
  );

  const mockedRoots = graphqlMocked.query(
    GET_ROOTS,
    (): StrictResponse<{ data: GetRootsInfoAtInputs }> => {
      return HttpResponse.json({
        data: {
          group: {
            __typename: "Group",
            gitEnvironmentUrls: [
              {
                __typename: "GitEnvironmentUrl",
                id: "00fbe3579a253b43239954a545dc0536e6c83094",
                rootId: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
                url: "https://app.fluidattacks.com/test",
                urlType: "URL",
              },
            ],
            name: "unittesting",
            roots: [
              {
                __typename: "GitRoot",
                id: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
                nickname: "universe",
                state: ResourceState.Active,
              },
              {
                __typename: "GitRoot",
                id: "1a32cab8-7b4c-4761-a0a5-85cb8b64ce68",
                nickname: "test_nickname2",
                state: ResourceState.Active,
              },
              {
                __typename: "GitRoot",
                id: "1a32cab8-7b4c-4761-0a5-85cb8b64ce68",
                nickname: "test_nickname",
                state: ResourceState.Active,
              },
            ],
          },
        },
      });
    },
  );

  it("should display group toe inputs", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermission = new PureAbility<string>([
      { action: "integrates_api_resolvers_toe_input_attacked_at_resolve" },
      { action: "integrates_api_resolvers_toe_input_attacked_by_resolve" },
      { action: "integrates_api_resolvers_toe_input_be_present_until_resolve" },
      { action: "integrates_api_resolvers_toe_input_first_attack_at_resolve" },
      {
        action: "integrates_api_resolvers_toe_input_seen_first_time_by_resolve",
      },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermission}>
        <Routes>
          <Route
            element={<GroupToeInputs isInternal={true} />}
            path={"/:groupName/surface/inputs"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter,
        mocks: [mockedComponents, mockedToeInputs, mockedRoots],
      },
    );

    expect(
      screen.queryByRole("button", {
        name: "group.toe.inputs.actionButtons.addButton.text",
      }),
    ).not.toBeInTheDocument();

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(4);
    });

    expect(
      screen.getAllByRole("row")[0].textContent?.replace(/[^a-zA-Z ]/gu, ""),
    ).toStrictEqual(
      [
        "group.toe.inputs.root",
        "group.toe.inputs.entryPoint",
        "group.toe.inputs.status",
        "group.toe.inputs.seenAt",
        "group.toe.inputs.attackedAt",
        "group.toe.inputs.seenFirstTimeBy",
      ]
        .join("")
        .replace(/[^a-zA-Z ]/gu, ""),
    );

    expect(
      screen.getAllByRole("row")[1].textContent?.replace(/[^a-zA-Z ]/gu, ""),
    ).toStrictEqual(
      ["test_nickname", "idTest", "Safe", "2000-01-01", "2020-01-02", ""]
        .join("")
        .replace(/[^a-zA-Z ]/gu, ""),
    );
    expect(
      screen.getAllByRole("row")[2].textContent?.replace(/[^a-zA-Z ]/gu, ""),
    ).toStrictEqual(
      [
        "test_nickname",
        "btnTest",
        "Vulnerable",
        "2020-03-14",
        "2021-02-02",
        "test@test.com",
      ]
        .join("")
        .replace(/[^a-zA-Z ]/gu, ""),
    );
    expect(
      screen.getAllByRole("row")[3].textContent?.replace(/[^a-zA-Z ]/gu, ""),
    ).toStrictEqual(
      ["", "-", "Vulnerable", "2020-01-11", "2021-02-11", "test2@test.com"]
        .join("")
        .replace(/[^a-zA-Z ]/gu, ""),
    );
  });

  it("should display error group toe inputs", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedError = [
      graphqlMocked.query(
        GET_TOE_INPUTS_COMPONENTS,
        (): StrictResponse<IErrorMessage> => {
          return HttpResponse.json({
            errors: [
              new GraphQLError("Error getting group inputs surface components"),
            ],
          });
        },
      ),
      graphqlMocked.query(GET_TOE_INPUTS, (): StrictResponse<IErrorMessage> => {
        return HttpResponse.json({
          errors: [new GraphQLError("Error getting group inputs")],
        });
      }),
      graphqlMocked.query(
        GET_ROOTS,

        (): StrictResponse<IErrorMessage> => {
          return HttpResponse.json({
            errors: [new GraphQLError("Error getting roots")],
          });
        },
      ),
    ];
    const mockedPermission = new PureAbility<string>([
      { action: "integrates_api_resolvers_toe_input_attacked_at_resolve" },
      { action: "integrates_api_resolvers_toe_input_attacked_by_resolve" },
      { action: "integrates_api_resolvers_toe_input_be_present_until_resolve" },
      { action: "integrates_api_resolvers_toe_input_first_attack_at_resolve" },
      {
        action: "integrates_api_resolvers_toe_input_seen_first_time_by_resolve",
      },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermission}>
        <Routes>
          <Route
            element={<GroupToeInputs isInternal={true} />}
            path={"/:groupName/surface/inputs"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter,
        mocks: [...mockedError],
      },
    );
    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(2);
    });
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith("groupAlerts.errorTextsad");
    });
  });

  it("should edit be present on cell", async (): Promise<void> => {
    expect.hasAssertions();

    const mocksMutation = [
      graphqlMocked.mutation(
        UPDATE_TOE_INPUT,
        ({
          variables,
        }): StrictResponse<IErrorMessage | { data: UpdateToeInput }> => {
          const {
            bePresent,
            canGetAttackedAt,
            canGetAttackedBy,
            canGetBePresentUntil,
            canGetFirstAttackAt,
            canGetSeenFirstTimeBy,
            component,
            entryPoint,
            groupName,
            hasRecentAttack,
            rootId,
            shouldGetNewToeInput,
          } = variables;
          if (
            !bePresent &&
            canGetAttackedAt &&
            canGetAttackedBy &&
            canGetBePresentUntil &&
            canGetFirstAttackAt &&
            canGetSeenFirstTimeBy &&
            component === "test.com/api/Test" &&
            entryPoint === "idTest" &&
            groupName === "unittesting" &&
            hasRecentAttack === undefined &&
            rootId === "1a32cab8-7b4c-4761-a0a5-85cb8b64ce68" &&
            shouldGetNewToeInput
          ) {
            return HttpResponse.json({
              data: {
                updateToeInput: {
                  success: true,
                  toeInput: {
                    __typename: "ToeInput",
                    ...{
                      attackedAt: "2020-01-02T00:00:00-05:00",
                      attackedBy: "",
                      bePresent: false,
                      bePresentUntil: "2022-01-02T00:00:00-05:00",
                      component: "test.com/api/Test",
                      entryPoint: "idTest",
                      firstAttackAt: "2020-02-19T15:41:04+00:00",
                      hasVulnerabilities: false,
                      root: {
                        __typename: "GitRoot",
                        id: "1a32cab8-7b4c-4761-a0a5-85cb8b64ce68",
                        nickname: "test_nickname",
                      },
                      seenAt: "2000-01-01T05:00:00+00:00",
                      seenFirstTimeBy: "",
                    },
                  },
                },
              },
            });
          }

          if (
            bePresent &&
            component === "test.component.com/api/Test" &&
            groupName === "unittesting" &&
            shouldGetNewToeInput
          ) {
            return HttpResponse.json({
              data: {
                updateToeInput: {
                  success: true,
                  toeInput: {
                    __typename: "ToeInput",
                    ...{
                      attackedAt: "2021-02-11T00:00:00-05:00",
                      attackedBy: "",
                      bePresent: true,
                      bePresentUntil: null,
                      component: "test.component.com/api/Test",
                      entryPoint: "-",
                      firstAttackAt: null,
                      hasVulnerabilities: true,
                      root: null,
                      seenAt: "2020-01-11T00:00:00-05:00",
                      seenFirstTimeBy: "test2@test.com",
                    },
                  },
                },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error updating group surface")],
          });
        },
      ),
    ];

    const mockedPermission = new PureAbility<string>([
      { action: "integrates_api_resolvers_toe_input_attacked_at_resolve" },
      { action: "integrates_api_resolvers_toe_input_attacked_by_resolve" },
      { action: "integrates_api_resolvers_toe_input_be_present_until_resolve" },
      { action: "integrates_api_resolvers_toe_input_first_attack_at_resolve" },
      {
        action: "integrates_api_resolvers_toe_input_seen_first_time_by_resolve",
      },
      { action: "integrates_api_mutations_update_toe_input_mutate" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermission}>
        <Routes>
          <Route
            element={<GroupToeInputs isInternal={true} />}
            path={"/:groupName/surface/inputs"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter,
        mocks: [
          ...mocksMutation,
          mockedComponents,
          mockedToeInputs,
          mockedRoots,
        ],
      },
    );
    await waitFor((): void => {
      expect(screen.queryAllByText("test_nickname")[0]).toBeInTheDocument();
    });

    await userEvent.click(
      screen.getByRole("button", {
        name: /group\.findings\.tableSet\.btn\.text .*/u,
      }),
    );

    await userEvent.click(
      screen.getByRole("checkbox", {
        checked: false,
        name: t("group.toe.inputs.bePresent"),
      }),
    );

    expect(
      screen.getAllByRole("row")[0].textContent?.replace(/[^a-zA-Z ]/gu, ""),
    ).toStrictEqual(
      [
        "group.toe.inputs.root",
        "group.toe.inputs.entryPoint",
        "group.toe.inputs.status",
        "group.toe.inputs.seenAt",
        "group.toe.inputs.bePresent",
        "group.toe.inputs.attackedAt",
        "group.toe.inputs.seenFirstTimeBy",
      ]
        .join("")
        .replace(/[^a-zA-Z ]/gu, ""),
    );

    const row = screen.getByRole("row", {
      name: "test_nickname2 idTest Safe 2000-01-01 yes bePresentSwitch Toggle Switch 2020-01-02",
    });

    await userEvent.click(
      within(row).getByRole("checkbox", { name: "bePresentSwitch" }),
    );

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "group.toe.inputs.alerts.updateInput",
        "groupAlerts.updatedTitle",
      );
    });

    const rowFalse = screen.getByRole("row", {
      name: "- Vulnerable 2020-01-11 no bePresentSwitch Toggle Switch 2021-02-11 test2@test.com",
    });

    await userEvent.click(
      within(rowFalse).getByRole("checkbox", { name: "bePresentSwitch" }),
    );

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "group.toe.inputs.alerts.updateInput",
        "groupAlerts.updatedTitle",
      );
    });
  });

  it("should mark input as attacked", async (): Promise<void> => {
    expect.hasAssertions();

    const mocksMutation = [
      graphqlMocked.mutation(
        UPDATE_TOE_INPUT,
        ({
          variables,
        }): StrictResponse<IErrorMessage | { data: UpdateToeInput }> => {
          const {
            bePresent,
            canGetAttackedAt,
            canGetAttackedBy,
            canGetBePresentUntil,
            canGetFirstAttackAt,
            canGetSeenFirstTimeBy,
            component,
            entryPoint,
            groupName,
            hasRecentAttack,
            rootId,
            shouldGetNewToeInput,
          } = variables;
          if (
            bePresent &&
            canGetAttackedAt &&
            canGetAttackedBy &&
            canGetBePresentUntil &&
            canGetFirstAttackAt &&
            canGetSeenFirstTimeBy &&
            component === "test.com/api/Test" &&
            entryPoint === "idTest" &&
            groupName === "unittesting" &&
            hasRecentAttack === true &&
            rootId === "1a32cab8-7b4c-4761-a0a5-85cb8b64ce68" &&
            !shouldGetNewToeInput
          ) {
            return HttpResponse.json({
              data: {
                updateToeInput: {
                  success: true,
                },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error updating group surface")],
          });
        },
      ),
      graphqlMocked.query(
        GET_TOE_INPUTS,
        ({
          variables,
        }): StrictResponse<IErrorMessage | { data: GetToeInputs }> => {
          const {
            canGetAttackedAt,
            canGetAttackedBy,
            canGetBePresentUntil,
            canGetFirstAttackAt,
            canGetSeenFirstTimeBy,
            first,
            groupName,
          } = variables;
          if (
            canGetAttackedAt &&
            canGetAttackedBy &&
            canGetBePresentUntil &&
            canGetFirstAttackAt &&
            canGetSeenFirstTimeBy &&
            first === 150 &&
            groupName === "unittesting"
          ) {
            return HttpResponse.json({
              data: {
                group: {
                  __typename: "Group",
                  name: "unittesting",
                  toeInputs: {
                    __typename: "ToeInputsConnection",
                    edges: [
                      {
                        node: {
                          __typename: "ToeInput",
                          ...{
                            attackedAt: "2020-01-02T00:00:00-05:00",
                            attackedBy: "",
                            bePresent: true,
                            bePresentUntil: null,
                            component: "test.com/api/Test",
                            entryPoint: "idTest",
                            firstAttackAt: "2020-02-19T15:41:04+00:00",
                            hasVulnerabilities: false,
                            root: {
                              __typename: "GitRoot",
                              id: "1a32cab8-7b4c-4761-a0a5-85cb8b64ce68",
                              nickname: "test_nickname",
                            },
                            seenAt: "2000-01-01T05:00:00+00:00",
                            seenFirstTimeBy: "",
                          },
                        },
                      },
                    ],
                    pageInfo: {
                      endCursor: "bnVsbA==",
                      hasNextPage: false,
                    },
                    total: 1,
                  },
                },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error getting group inputs surface")],
          });
        },
      ),
    ];

    const mockedPermission = new PureAbility<string>([
      { action: "integrates_api_resolvers_toe_input_attacked_at_resolve" },
      { action: "integrates_api_resolvers_toe_input_attacked_by_resolve" },
      { action: "integrates_api_resolvers_toe_input_be_present_until_resolve" },
      { action: "integrates_api_resolvers_toe_input_first_attack_at_resolve" },
      {
        action: "integrates_api_resolvers_toe_input_seen_first_time_by_resolve",
      },
      { action: "integrates_api_mutations_update_toe_input_mutate" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermission}>
        <Routes>
          <Route
            element={<GroupToeInputs isInternal={true} />}
            path={"/:groupName/surface/inputs"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter,
        mocks: [...mocksMutation, mockedComponents, mockedRoots],
      },
    );
    await waitFor((): void => {
      expect(screen.queryByRole("table")).toBeInTheDocument();
    });

    await userEvent.click(screen.getAllByRole("checkbox", { name: "" })[0]);
    await userEvent.click(
      screen.getByRole("button", {
        name: "group.toe.inputs.actionButtons.attackedButton.text",
      }),
    );

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "group.toe.inputs.alerts.markAsAttacked.success",
        "groupAlerts.updatedTitle",
      );
    });
  });

  it("should filter by root nickname", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<GroupToeInputs isInternal={true} />}
            path={"/:groupName/surface/inputs"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter,
        mocks: [mockedComponents, mockedToeInputs, mockedRoots],
      },
    );
    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(4);
    });

    await openFilters();

    await selectOption("rootNickname", "test_nickname");

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(2);
    });

    expect(
      screen.getAllByRole("row")[1].textContent?.replace(/[^a-zA-Z ]/gu, ""),
    ).toStrictEqual(
      ["test_nickname", "idTest", "Safe", "2000-01-01", "2020-01-02", ""]
        .join("")
        .replace(/[^a-zA-Z ]/gu, ""),
    );

    await clearFilters();
  });

  it("should filter by component", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<GroupToeInputs isInternal={true} />}
            path={"/:groupName/surface/inputs"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter,
        mocks: [mockedComponents, mockedToeInputs, mockedRoots],
      },
    );
    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(4);
    });

    await openFilters();

    await selectOption("component", "test.component.com/api/Test");

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(2);
    });

    expect(
      screen.getAllByRole("row")[1].textContent?.replace(/[^a-zA-Z ]/gu, ""),
    ).toStrictEqual(
      ["", "idTest", "Safe", "2000-01-01", "2020-01-02", ""]
        .join("")
        .replace(/[^a-zA-Z ]/gu, ""),
    );

    await clearFilters();
  });

  it("should handle error getting group inputs surface components", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<GroupToeInputs isInternal={true} />}
            path={"/:groupName/surface/inputs"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/error/surface/inputs"],
        },
        mocks: [mockedComponents, mockedToeInputs, mockedRoots],
      },
    );

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith("groupAlerts.errorTextsad");
    });
  });

  it("should display add group toe inputs modal", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermission = new PureAbility<string>([
      { action: "integrates_api_resolvers_toe_input_attacked_at_resolve" },
      { action: "integrates_api_resolvers_toe_input_attacked_by_resolve" },
      { action: "integrates_api_resolvers_toe_input_be_present_until_resolve" },
      { action: "integrates_api_resolvers_toe_input_first_attack_at_resolve" },
      {
        action: "integrates_api_resolvers_toe_input_seen_first_time_by_resolve",
      },
      { action: "integrates_api_mutations_add_toe_input_mutate" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermission}>
        <Routes>
          <Route
            element={<GroupToeInputs isInternal={true} />}
            path={"/:groupName/surface/inputs"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter,
        mocks: [mockedComponents, mockedToeInputs, mockedRoots],
      },
    );

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(4);
    });

    expect(
      screen.getByRole("button", {
        name: "group.toe.inputs.actionButtons.addButton.text",
      }),
    ).toBeInTheDocument();

    expect(
      screen.getAllByRole("row")[0].textContent?.replace(/[^a-zA-Z ]/gu, ""),
    ).toStrictEqual(
      [
        "group.toe.inputs.root",
        "group.toe.inputs.entryPoint",
        "group.toe.inputs.status",
        "group.toe.inputs.seenAt",
        "group.toe.inputs.attackedAt",
        "group.toe.inputs.seenFirstTimeBy",
      ]
        .join("")
        .replace(/[^a-zA-Z ]/gu, ""),
    );

    expect(
      screen.getAllByRole("row")[1].textContent?.replace(/[^a-zA-Z ]/gu, ""),
    ).toStrictEqual(
      ["test_nickname", "idTest", "Safe", "2000-01-01", "2020-01-02", ""]
        .join("")
        .replace(/[^a-zA-Z ]/gu, ""),
    );

    await userEvent.click(
      screen.getByRole("button", {
        name: "group.toe.inputs.actionButtons.addButton.text",
      }),
    );

    await screen.findByText("group.toe.inputs.addModal.title");
  });

  it("should display error editing input", async (): Promise<void> => {
    expect.hasAssertions();

    const mocksMutation = [
      graphqlMocked.mutation(
        UPDATE_TOE_INPUT,
        ({
          variables,
        }): StrictResponse<
          { data: UpdateToeInput } | { errors: [IMessage, IMessage] }
        > => {
          const {
            bePresent,
            component,
            entryPoint,
            groupName,
            hasRecentAttack,
            rootId,
            shouldGetNewToeInput,
          } = variables;
          if (
            bePresent &&
            component === "test.com/api/Test" &&
            entryPoint === "idTest" &&
            groupName === "unittesting" &&
            hasRecentAttack === true &&
            rootId === "1a32cab8-7b4c-4761-a0a5-85cb8b64ce68" &&
            !shouldGetNewToeInput
          ) {
            return HttpResponse.json({
              errors: [
                new GraphQLError(
                  "Exception - The attack time must be between the previous attack and the current time",
                ),
                new GraphQLError(
                  "Exception - The toe input has been updated by another operation",
                ),
              ],
            });
          }

          return HttpResponse.json({
            data: {
              updateToeInput: {
                success: true,
              },
            },
          });
        },
      ),
      graphqlMocked.query(
        GET_TOE_INPUTS,
        ({
          variables,
        }): StrictResponse<IErrorMessage | { data: GetToeInputs }> => {
          const {
            canGetAttackedAt,
            canGetAttackedBy,
            canGetBePresentUntil,
            canGetFirstAttackAt,
            canGetSeenFirstTimeBy,
            first,
            groupName,
          } = variables;
          if (
            canGetAttackedAt &&
            canGetAttackedBy &&
            canGetBePresentUntil &&
            canGetFirstAttackAt &&
            canGetSeenFirstTimeBy &&
            first === 150 &&
            groupName === "unittesting"
          ) {
            return HttpResponse.json({
              data: {
                group: {
                  __typename: "Group",
                  name: "unittesting",
                  toeInputs: {
                    __typename: "ToeInputsConnection",
                    edges: [
                      {
                        node: {
                          __typename: "ToeInput",
                          ...{
                            attackedAt: "2020-01-02T00:00:00-05:00",
                            attackedBy: "",
                            bePresent: true,
                            bePresentUntil: null,
                            component: "test.com/api/Test",
                            entryPoint: "idTest",
                            firstAttackAt: "2020-02-19T15:41:04+00:00",
                            hasVulnerabilities: false,
                            root: {
                              __typename: "GitRoot",
                              id: "1a32cab8-7b4c-4761-a0a5-85cb8b64ce68",
                              nickname: "test_nickname",
                            },
                            seenAt: "2000-01-01T05:00:00+00:00",
                            seenFirstTimeBy: "",
                          },
                        },
                      },
                    ],
                    pageInfo: {
                      endCursor: "bnVsbA==",
                      hasNextPage: false,
                    },
                    total: 1,
                  },
                },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error getting group inputs surface")],
          });
        },
      ),
    ];

    const mockedPermission = new PureAbility<string>([
      { action: "integrates_api_resolvers_toe_input_attacked_at_resolve" },
      { action: "integrates_api_resolvers_toe_input_attacked_by_resolve" },
      { action: "integrates_api_resolvers_toe_input_be_present_until_resolve" },
      { action: "integrates_api_resolvers_toe_input_first_attack_at_resolve" },
      {
        action: "integrates_api_resolvers_toe_input_seen_first_time_by_resolve",
      },
      { action: "integrates_api_mutations_update_toe_input_mutate" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermission}>
        <Routes>
          <Route
            element={<GroupToeInputs isInternal={true} />}
            path={"/:groupName/surface/inputs"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter,
        mocks: [...mocksMutation, mockedComponents, mockedRoots],
      },
    );
    await waitFor((): void => {
      expect(screen.queryByRole("table")).toBeInTheDocument();
    });

    await userEvent.click(screen.getAllByRole("checkbox", { name: "" })[0]);
    await userEvent.click(
      screen.getByRole("button", {
        name: "group.toe.inputs.actionButtons.attackedButton.text",
      }),
    );

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        "group.toe.inputs.alerts.invalidAttackedAt",
      );
    });
  });
});
