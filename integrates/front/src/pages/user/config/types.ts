import type { IToggleProps } from "@fluidattacks/design/dist/components/toggle/types";

interface IFormData {
  minimumSeverity: number;
}

interface INotificationsPreferences {
  email: string[];
  parameters: {
    minSeverity: number;
  };
}

interface ISubscriptionName {
  minSeverity: JSX.Element;
  name: string;
  tooltip: string;
  toggles: IToggleProps[];
}

export type { IFormData, INotificationsPreferences, ISubscriptionName };
