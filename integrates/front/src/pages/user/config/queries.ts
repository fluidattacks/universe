import { graphql } from "gql";

const GET_SUBSCRIPTIONS = graphql(`
  query GetSubscriptions {
    me {
      userEmail
      notificationsPreferences {
        email
        parameters {
          minSeverity
        }
      }
    }
    Notifications: __type(name: "NotificationsName") {
      enumValues {
        name
      }
    }
  }
`);

const UPDATE_NOTIFICATIONS_PREFERENCES = graphql(`
  mutation UpdateNotificationsPreferences(
    $email: [NotificationsName!]!
    $severity: Float!
  ) {
    updateNotificationsPreferences(
      notificationsPreferences: {
        email: $email
        parameters: { minSeverity: $severity }
      }
    ) {
      success
    }
  }
`);

export { GET_SUBSCRIPTIONS, UPDATE_NOTIFICATIONS_PREFERENCES };
