import * as React from "react";
import { Navigate, Route, Routes } from "react-router-dom";

import { UserConfig } from "./config";
import { TrustedDevices } from "./trusted-devices";

const User: React.FC = (): JSX.Element => {
  return (
    <Routes>
      <Route element={<UserConfig />} path={`/config`} />
      <Route element={<Navigate to={"/user/config"} />} path={"*"} />
      <Route element={<TrustedDevices />} path={`/trusted-devices`} />
      <Route element={<Navigate to={"/user/trusted-devices"} />} path={"*"} />
    </Routes>
  );
};

export { User };
