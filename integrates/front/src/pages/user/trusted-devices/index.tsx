import { useMutation, useQuery } from "@apollo/client";
import { Container, useConfirmDialog } from "@fluidattacks/design";
import type { ColumnDef } from "@tanstack/react-table";
import _ from "lodash";
import mixpanel from "mixpanel-browser";
import { useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

import { GET_TRUSTED_DEVICES, REMOVE_TRUSTED_DEVICES } from "./queries";
import { RenderDeleteButton } from "./remove-device";
import type { ITrustedDevice } from "./types";

import { SectionHeader } from "components/section-header";
import { Table } from "components/table";
import { useTable } from "hooks";
import { formatDate } from "utils/date";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

export const TrustedDevices: React.FC = (): JSX.Element => {
  const { t } = useTranslation();
  const theme = useTheme();
  const { confirm, ConfirmDialog } = useConfirmDialog();

  const { data, refetch } = useQuery(GET_TRUSTED_DEVICES, {
    fetchPolicy: "network-only",
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((errors): void => {
        Logger.warning(
          "An error occurred fetching user trusted devices",
          errors,
        );
      });
    },
  });

  const [removeTrustedDevice] = useMutation(REMOVE_TRUSTED_DEVICES, {
    onCompleted: (): void => {
      void refetch();
      mixpanel.track("DeleteTrustedDevice");
      msgSuccess(
        t("trustedDevices.successRemove"),
        t("searchFindings.tabUsers.titleSuccess"),
      );
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach(({ message }): void => {
        msgError(t("trustedDevices.errorText"));
        Logger.warning("An error occurred deleting trusted device", message);
      });
    },
  });

  const handleDelete = useCallback(
    (trustedDevice: ITrustedDevice): void => {
      void removeTrustedDevice({
        variables: {
          browser: trustedDevice.browser,
          device: trustedDevice.device,
          otpTokenJti: trustedDevice.otpTokenJti,
        },
      });
    },
    [removeTrustedDevice],
  );

  const onConfirmDelete = useCallback(
    async (trustedDevice: ITrustedDevice): Promise<void> => {
      const confirmResult = await confirm({
        title: t("trustedDevices.modalTitle"),
      });
      if (confirmResult) {
        handleDelete(trustedDevice);
      }
    },
    [confirm, handleDelete, t],
  );

  const tableRef = useTable("tblTrustedDevices");
  const columns: ColumnDef<ITrustedDevice>[] = [
    {
      accessorKey: "device",
      header: "Device",
    },
    {
      accessorKey: "location",
      header: "Location",
    },
    {
      accessorKey: "browser",
      header: "Browser",
    },
    {
      accessorKey: "firstLoginDate",
      cell: (cell): string => formatDate(String(cell.getValue() ?? "")),
      header: "First sign-in",
    },
    {
      accessorKey: "lastLoginDate",
      cell: (cell): string => formatDate(String(cell.getValue() ?? "")),
      header: "Recent activity",
    },
    {
      accessorKey: "delete",
      cell: (cell): JSX.Element =>
        // eslint-disable-next-line new-cap
        RenderDeleteButton(onConfirmDelete, cell.row.original),
      header: "",
    },
  ];

  const dataset =
    _.isUndefined(data) || _.isEmpty(data) ? [] : data.me.trustedDevices;

  return (
    <React.StrictMode>
      <SectionHeader header={t("trustedDevices.title")} />
      <Container
        bgColor={theme.palette.gray[50]}
        height={"100hv"}
        px={1.25}
        py={1.25}
      >
        <Table
          columns={columns}
          data={dataset as ITrustedDevice[]}
          options={{ enableSearchBar: false }}
          tableRef={tableRef}
        />
      </Container>
      <ConfirmDialog />
    </React.StrictMode>
  );
};
