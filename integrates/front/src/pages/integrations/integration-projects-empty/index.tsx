import { CloudImage, Container, Text } from "@fluidattacks/design";
import * as React from "react";
import { useTranslation } from "react-i18next";

interface IIntegrationProjectsEmptyProps {
  readonly name: string;
}

const IntegrationProjectsEmpty: React.FC<IIntegrationProjectsEmptyProps> = ({
  name,
}): React.JSX.Element => {
  const { t } = useTranslation();

  return (
    <Container display={"flex"} flexDirection={"column"} gap={0.5}>
      <CloudImage
        publicId={"integrates/integrations/projects-empty"}
        width={"32px"}
      />
      <Text size={"sm"}>
        <b>{t("integrations.projectsEmpty.title")}</b>
        <br />
        {t("integrations.projectsEmpty.subtitle", { name })}
        <br />
        {t("integrations.projectsEmpty.getStarted")}
      </Text>
    </Container>
  );
};

export { IntegrationProjectsEmpty };
