import * as React from "react";
import { useTranslation } from "react-i18next";

import { IntegrationCard } from "../integration-card";

const IntelliJ: React.FC = (): React.JSX.Element => {
  const { t } = useTranslation();

  return (
    <IntegrationCard
      description={t("integrations.intellij.description")}
      docsLink={
        "https://help.fluidattacks.com/portal/en/kb/articles/install-the-intellij-plugin"
      }
      iconPath={"integrates/integrations/icon-intellij-new"}
      name={"IntelliJ IDEA"}
    />
  );
};

export { IntelliJ };
