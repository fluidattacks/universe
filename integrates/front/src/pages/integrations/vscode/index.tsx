import * as React from "react";
import { useTranslation } from "react-i18next";

import { IntegrationCard } from "../integration-card";

const Vscode: React.FC = (): React.JSX.Element => {
  const { t } = useTranslation();

  return (
    <IntegrationCard
      description={t("integrations.vscode.description")}
      docsLink={
        "https://help.fluidattacks.com/portal/en/kb/articles/install-the-vs-code-extension"
      }
      iconPath={"integrates/integrations/icon-vscode"}
      name={"Visual Studio Code"}
    />
  );
};

export { Vscode };
