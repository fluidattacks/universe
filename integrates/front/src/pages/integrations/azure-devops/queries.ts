import { graphql } from "gql/gql";
import type { GetAzureIntegrationFragment } from "gql/graphql";

const GET_AZURE_INTEGRATION = graphql(`
  fragment GetAzureIntegration on Query {
    me {
      userEmail
      organizations {
        name
        groups {
          description
          name
          azureIssuesIntegration {
            assignedTo
            azureOrganization
            azureProject
            connectionDate
            issueAutomationEnabled
            tags
          }
        }
      }
    }
  }
`);

type TGroup =
  GetAzureIntegrationFragment["me"]["organizations"][0]["groups"][0];

export type { TGroup };
export { GET_AZURE_INTEGRATION };
