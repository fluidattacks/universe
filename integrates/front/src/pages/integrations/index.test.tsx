import { screen, waitFor } from "@testing-library/react";
import { HttpResponse, type StrictResponse, graphql } from "msw";

import { GET_INTEGRATIONS } from "./queries";

import { Integrations } from ".";
import type { GetIntegrationsQuery } from "gql/graphql";
import { LINK } from "mocks/constants";
import { render } from "mocks/index";

describe("integrations", (): void => {
  const graphqlMocked = graphql.link(LINK);
  const mockIntegrations = [
    graphqlMocked.query(
      GET_INTEGRATIONS,
      (): StrictResponse<{ data: GetIntegrationsQuery }> => {
        return HttpResponse.json({
          data: {
            __typename: "Query",
            ...{
              me: {
                __typename: "Me",
                organizations: [
                  {
                    __typename: "Organization" as const,
                    groups: [
                      {
                        __typename: "Group" as const,
                        azureIssuesIntegration: null,
                        description: "Group test",
                        gitlabIssuesIntegration: null,
                        hooks: [
                          {
                            __typename: "Hook" as const,
                            entryPoint:
                              "https://webhook.site/4039d098-ffc5-4984-8ed3-eb17bca98e19",
                            hookEvents: ["VULNERABILITY_CREATED"],
                            id: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
                            name: "test-hook",
                            token: "token",
                            tokenHeader: "token_header",
                          },
                        ],
                        name: "unittesting",
                        permissions: [
                          "grant_organization_level_role:admin",
                          "grant_group_level_role:hacker",
                        ],
                      },
                    ],
                    name: "okada",
                  },
                ],
                userEmail: "test@test.com",
              },
            },
          },
        });
      },
    ),
  ];

  it("should render", (): void => {
    expect.hasAssertions();

    render(<Integrations />, {
      memoryRouter: { initialEntries: ["/integrations"] },
      mocks: mockIntegrations,
    });

    expect(screen.getByText("integrations.title")).toBeInTheDocument();
  });

  it("should render integrations plugins categories", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    render(<Integrations />, {
      memoryRouter: { initialEntries: ["/integrations"] },
      mocks: [...mockIntegrations],
    });

    await waitFor((): void => {
      expect(screen.getByText("integrations.title")).toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(
        screen.getByText("integrations.categories.IDE"),
      ).toBeInTheDocument();
    });

    expect(screen.getByText("Visual Studio Code")).toBeInTheDocument();
    expect(screen.getByText("IntelliJ IDEA")).toBeInTheDocument();

    expect(screen.getByText("integrations.categories.BTS")).toBeInTheDocument();
    expect(screen.getByText("Jira Cloud")).toBeInTheDocument();
    expect(screen.getByText("Azure DevOps")).toBeInTheDocument();
    expect(screen.getByText("GitLab")).toBeInTheDocument();

    expect(
      screen.getByText("integrations.categories.OTHERS"),
    ).toBeInTheDocument();
    expect(screen.getByText("API")).toBeInTheDocument();
    expect(screen.getByText("Webhooks")).toBeInTheDocument();
  });
});
