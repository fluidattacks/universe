import { graphql } from "gql/gql";

const GET_INTEGRATIONS = graphql(`
  query GetIntegrations {
    ...GetAzureIntegration
    ...GetGitLabIntegration
    ...GetWebhooksIntegration
  }
`);

const REMOVE_GROUP_INTEGRATION = graphql(`
  mutation RemoveGroupIntegration(
    $groupName: String!
    $integration: GroupIntegration!
  ) {
    removeGroupIntegration(groupName: $groupName, integration: $integration) {
      success
    }
  }
`);

export { GET_INTEGRATIONS, REMOVE_GROUP_INTEGRATION };
