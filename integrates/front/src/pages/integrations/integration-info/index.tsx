import {
  CloudImage,
  Container,
  Heading,
  Icon,
  Modal,
  Text,
} from "@fluidattacks/design";
import type { IUseModal } from "@fluidattacks/design";
import dayjs, { extend } from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

extend(relativeTime);

interface IPermission {
  title: string;
  description: string;
}

interface IIntegrationInfoProps {
  readonly connectionDate?: string | null;
  readonly iconPath: `integrates/integrations/${string}`;
  readonly modalRef: IUseModal;
  readonly name: string;
  readonly permissions: IPermission[];
}

const IntegrationInfo: React.FC<IIntegrationInfoProps> = ({
  connectionDate,
  iconPath,
  modalRef,
  name,
  permissions,
}): React.JSX.Element => {
  const theme = useTheme();
  const { t } = useTranslation();

  return (
    <Modal
      modalRef={modalRef}
      size={"md"}
      title={
        <Container display={"flex"} gap={0.75}>
          <CloudImage height={"56px"} publicId={iconPath} width={"54px"} />
          <Container
            display={"flex"}
            flexDirection={"column"}
            justify={"center"}
          >
            <Heading fontWeight={"bold"} size={"xs"}>
              {name}
            </Heading>
            <Container alignItems={"center"} display={"flex"} gap={0.25}>
              <Icon icon={"clock"} iconSize={"xs"} iconType={"fa-light"} />
              <Text size={"sm"}>
                {t("integrations.info.connected", {
                  timeFromNow: dayjs(connectionDate).fromNow(),
                })}
              </Text>
            </Container>
          </Container>
        </Container>
      }
    >
      <Container display={"flex"} flexDirection={"column"} gap={0.5}>
        <Heading fontWeight={"bold"} size={"xs"}>
          {t("integrations.info.permissions")}
        </Heading>
        {permissions.map((permission): React.JSX.Element => {
          return (
            <Container
              display={"flex"}
              flexDirection={"column"}
              gap={0.25}
              key={permission.title}
            >
              <Container alignItems={"center"} display={"flex"} gap={0.25}>
                <Icon
                  icon={"check-circle"}
                  iconColor={theme.palette.success[500]}
                  iconSize={"xxs"}
                  iconType={"fa-light"}
                />
                <Text fontWeight={"bold"} size={"sm"}>
                  {permission.title}
                </Text>
              </Container>
              <Text size={"sm"}>{permission.description}</Text>
            </Container>
          );
        })}
      </Container>
    </Modal>
  );
};

export type { IPermission };
export { IntegrationInfo };
