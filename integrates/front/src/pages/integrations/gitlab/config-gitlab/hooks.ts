import { useQuery } from "@apollo/client";

import {
  GET_GITLAB_PROJECT_MEMBERS,
  GET_GITLAB_PROJECT_NAMES,
} from "./queries";

import type { GitLabProjectMember } from "gql/graphql";

const useProjectMembers = (
  gitlabProject: string | undefined,
  groupName: string,
): GitLabProjectMember[] | undefined => {
  const { data, loading } = useQuery(GET_GITLAB_PROJECT_MEMBERS, {
    fetchPolicy: "no-cache",
    skip: gitlabProject === undefined,
    variables: { gitlabProject: gitlabProject ?? "", groupName },
  });

  if (loading) {
    return undefined;
  }

  const members = data?.group.gitlabIssuesIntegration?.projectMembers ?? [];

  return members.toSorted((memberA, memberB): number =>
    memberA.username.localeCompare(memberB.username),
  );
};

const useProjectNames = (groupName: string): string[] | undefined => {
  const { data } = useQuery(GET_GITLAB_PROJECT_NAMES, {
    fetchPolicy: "no-cache",
    variables: { groupName },
  });

  return data?.group.gitlabIssuesIntegration?.projectNames;
};

export { useProjectMembers, useProjectNames };
