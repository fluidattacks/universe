import { graphql } from "gql/gql";

const GET_GITLAB_PROJECT_MEMBERS = graphql(`
  query GetGitLabProjectMembers($gitlabProject: String!, $groupName: String!) {
    group(groupName: $groupName) {
      gitlabIssuesIntegration {
        projectMembers(gitlabProject: $gitlabProject) {
          id
          name
          username
        }
      }
    }
  }
`);

const GET_GITLAB_PROJECT_NAMES = graphql(`
  query GetGitLabProjectNames($groupName: String!) {
    group(groupName: $groupName) {
      gitlabIssuesIntegration {
        projectNames
      }
    }
  }
`);

const UPDATE_GITLAB_ISSUES_INTEGRATION = graphql(`
  mutation UpdateGitLabIssuesIntegration(
    $assigneeIds: [Int!]
    $gitlabProjectName: String!
    $groupName: String!
    $issueAutomationEnabled: Boolean!
    $labels: [String!]
  ) {
    updateGitLabIssuesIntegration(
      assigneeIds: $assigneeIds
      gitlabProjectName: $gitlabProjectName
      groupName: $groupName
      issueAutomationEnabled: $issueAutomationEnabled
      labels: $labels
    ) {
      success
    }
  }
`);

export {
  GET_GITLAB_PROJECT_MEMBERS,
  GET_GITLAB_PROJECT_NAMES,
  UPDATE_GITLAB_ISSUES_INTEGRATION,
};
