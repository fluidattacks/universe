import { useFormikContext } from "formik";
import { useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import type { IDropDownOption } from "components/dropdown/types";
import { Select } from "components/input";

interface IProjectSelectProps {
  readonly onChange: (projectName: string) => void;
  readonly projectNames: string[];
}

const ProjectSelect: React.FC<IProjectSelectProps> = ({
  onChange,
  projectNames,
}): React.JSX.Element => {
  const { t } = useTranslation();
  const formikHelpers = useFormikContext();

  const handleChange = useCallback(
    async (selection: IDropDownOption): Promise<void> => {
      await formikHelpers.setFieldValue("assigneeId", undefined);
      await formikHelpers.setFieldTouched("assigneeId");
      onChange(selection.value ?? "");
    },
    [formikHelpers, onChange],
  );

  return (
    <Select
      handleOnChange={handleChange}
      items={projectNames.map((projectName): IDropDownOption => {
        return { value: projectName };
      })}
      label={t("integrations.gitlab.config.project.label")}
      name={"gitlabProject"}
      placeholder={t("integrations.gitlab.config.project.placeholder")}
      required={true}
    />
  );
};

export { ProjectSelect };
