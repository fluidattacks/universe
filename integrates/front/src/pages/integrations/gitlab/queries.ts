import { graphql } from "gql/gql";
import type { GetGitLabIntegrationFragment } from "gql/graphql";

const GET_GITLAB_INTEGRATION = graphql(`
  fragment GetGitLabIntegration on Query {
    me {
      userEmail
      organizations {
        name
        groups {
          description
          name
          gitlabIssuesIntegration {
            assigneeIds
            connectionDate
            gitlabProject
            issueAutomationEnabled
            labels
          }
        }
      }
    }
  }
`);

type TGroup =
  GetGitLabIntegrationFragment["me"]["organizations"][0]["groups"][0];

export type { TGroup };
export { GET_GITLAB_INTEGRATION };
