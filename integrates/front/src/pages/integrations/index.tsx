import { useQuery } from "@apollo/client";
import { Container, EmptyState, Loading } from "@fluidattacks/design";
import groupBy from "lodash/groupBy";
import { useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { Api } from "./api";
import { AzureDevOps } from "./azure-devops";
import { GitLab } from "./gitlab";
import { IntegrationsTemplate } from "./index.template";
import { IntegrationCategory } from "./integration-category";
import { IntelliJ } from "./intellij";
import { Jira } from "./jira";
import { GET_INTEGRATIONS } from "./queries";
import type { IIntegration, TCategory } from "./types";
import { Vscode } from "./vscode";
import { Webhooks } from "./webhooks";

import { Search } from "components/search";

const integrations: IIntegration[] = [
  {
    category: "IDE",
    component: (): React.JSX.Element => <Vscode />,
    name: "Visual Studio Code",
  },
  {
    category: "IDE",
    component: (): React.JSX.Element => <IntelliJ />,
    name: "IntelliJ IDEA",
  },
  {
    category: "BTS",
    component: (): React.JSX.Element => <Jira />,
    name: "Jira Cloud",
  },
  {
    category: "BTS",
    component: (data): React.JSX.Element => <AzureDevOps data={data} />,
    name: "Azure DevOps",
  },
  {
    category: "BTS",
    component: (data): React.JSX.Element => <GitLab data={data} />,
    name: "GitLab",
  },
  {
    category: "OTHERS",
    component: (): React.JSX.Element => <Api />,
    name: "API",
  },
  {
    category: "OTHERS",
    component: (data): React.JSX.Element => <Webhooks data={data} />,
    name: "Webhooks",
  },
];

const Integrations: React.FC = (): React.JSX.Element => {
  const { data } = useQuery(GET_INTEGRATIONS, {
    context: { skipGlobalErrorHandler: true },
    errorPolicy: "all",
  });
  const { t } = useTranslation();

  const [search, setSearch] = useState("");
  const filteredIntegrations = integrations.filter((integration): boolean => {
    return (
      search.trim() === "" ||
      integration.name.toLowerCase().includes(search.toLowerCase())
    );
  });
  const integrationsByCategory = Object.entries(
    groupBy(filteredIntegrations, ({ category }): TCategory => category),
  );

  return (
    <IntegrationsTemplate>
      <Container
        display={"flex"}
        flexDirection={"column"}
        gap={1.25}
        px={1.25}
        py={1.25}
      >
        <Container width={"250px"}>
          <Search
            handleOnKeyPressed={setSearch}
            placeHolder={t("integrations.search")}
          />
        </Container>
        {data !== undefined && filteredIntegrations.length === 0 ? (
          <EmptyState
            description={t("integrations.empty.description")}
            imageSrc={"integrates/integrations/empty"}
            title={t("integrations.empty.title")}
          />
        ) : undefined}
        {data === undefined ? (
          <Container display={"flex"} justify={"center"}>
            <Loading color={"red"} size={"lg"} />
          </Container>
        ) : (
          <React.Fragment>
            {integrationsByCategory.map(
              ([category, categoryIntegrations]): React.JSX.Element => {
                return (
                  <IntegrationCategory
                    data={data}
                    integrations={categoryIntegrations}
                    key={category}
                    title={t(`integrations.categories.${category}`)}
                  />
                );
              },
            )}
          </React.Fragment>
        )}
      </Container>
    </IntegrationsTemplate>
  );
};

export { Integrations };
