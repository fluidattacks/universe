import { Container, Heading } from "@fluidattacks/design";
import * as React from "react";

import type { IIntegration } from "../types";
import type { GetIntegrationsQuery } from "gql/graphql";

interface IIntegrationCategoryProps {
  readonly data: GetIntegrationsQuery;
  readonly integrations: IIntegration[];
  readonly title: string;
}

const IntegrationCategory: React.FC<IIntegrationCategoryProps> = ({
  data,
  integrations,
  title,
}): React.JSX.Element => {
  return (
    <React.Fragment>
      <Heading fontWeight={"bold"} size={"md"}>
        {title}
      </Heading>
      <Container display={"flex"} gap={1.25}>
        {integrations.map((integration): React.JSX.Element => {
          return (
            <React.Fragment key={integration.name}>
              {integration.component(data)}
            </React.Fragment>
          );
        })}
      </Container>
    </React.Fragment>
  );
};

export { IntegrationCategory };
