interface ICurrentUser {
  me: {
    awsSubscription: {
      contractedGroups: number | null;
      status: string;
      usedGroups: number;
    } | null;
    enrolled: boolean;
    permissions: string[];
    phone: {
      callingCountryCode: string;
      nationalNumber: string;
    } | null;
    sessionExpiration: string;
    tours: {
      newGroup: boolean;
      newRoot: boolean;
      welcome: boolean;
    };
    trial: {
      completed: boolean;
      groupRole: string | null;
    } | null;
    userEmail: string;
    userName: string;
  };
}

interface IRoot {
  id: string;
  nickname: string;
}

export type { ICurrentUser, IRoot };
