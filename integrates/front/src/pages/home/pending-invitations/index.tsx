import { Modal, useModal } from "@fluidattacks/design";
import mixpanel from "mixpanel-browser";
import { useCallback, useEffect, useRef, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { useHasPendingInvitation } from "../hooks";
import { ModalConfirm } from "components/modal";

interface IPendingInvitation {
  readonly isEnrolled?: boolean;
}

const PendingInvitation: React.FC<IPendingInvitation> = ({
  isEnrolled,
}: IPendingInvitation): JSX.Element => {
  const { t } = useTranslation();
  const shouldShowPendingMsg = useHasPendingInvitation(isEnrolled);
  const [open, setOpen] = useState(false);
  const resolveConfirm = useRef<((result: boolean) => void) | null>(null);
  const modal = useModal("useHasPendingInvitation-dialog-modal");

  const confirm = useCallback(async (): Promise<boolean> => {
    setOpen(true);

    return new Promise<boolean>((resolve): void => {
      // eslint-disable-next-line functional/immutable-data
      resolveConfirm.current = resolve;
    });
  }, []);

  const handleConfirm = useCallback((): void => {
    resolveConfirm.current?.(true);
  }, []);

  useEffect((): void => {
    modal.setIsOpen(open);
  }, [open, modal]);

  useEffect((): void => {
    async function modalMessage(): Promise<void> {
      if (shouldShowPendingMsg === true) {
        const confirmResult = await confirm();

        if (confirmResult) {
          mixpanel.reset();
          location.replace("/logout");
        }
      }
    }
    void modalMessage();
  }, [confirm, shouldShowPendingMsg, t]);

  const handleCancel = useCallback((): void => {
    setOpen(true);
    modal.setIsOpen(open);
  }, [modal, open]);

  return (
    <Modal
      modalRef={{ ...modal, close: handleCancel }}
      size={"sm"}
      title={t("autoenrollment.pendingInvitation.title")}
    >
      {t("autoenrollment.pendingInvitation.message")}
      <ModalConfirm onConfirm={handleConfirm} txtConfirm={t("navbar.logout")} />
    </Modal>
  );
};

export { PendingInvitation };
