import { useQuery } from "@apollo/client";
import Bugsnag from "@bugsnag/js";
import { CoralogixRum } from "@coralogix/browser";
import mixpanel from "mixpanel-browser";
import { useContext } from "react";
import { useTranslation } from "react-i18next";

import { GET_CURRENT_USER, GET_USER_INVITATION } from "./queries";
import type { ICurrentUser } from "./types";

import type { IAuthContext } from "context/auth";
import { authContext, setupSessionCheck } from "context/auth";
import { authzPermissionsContext } from "context/authz/config";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";
import { initializeDelighted } from "utils/widgets";

const useCurrentUser = (): ICurrentUser["me"] | undefined => {
  const user = useContext(authContext as React.Context<Required<IAuthContext>>);
  const permissions = useContext(authzPermissionsContext);
  const { t } = useTranslation();

  const { data, refetch } = useQuery(GET_CURRENT_USER, {
    fetchPolicy: "cache-and-network",
    onCompleted: ({ me }: ICurrentUser): void => {
      user.setUser({
        awsSubscription: me.awsSubscription,
        refetch,
        tours: {
          newGroup: me.tours.newGroup,
          newRoot: me.tours.newRoot,
          welcome: me.tours.welcome,
        },
        trialAdmin:
          me.trial !== null &&
          ["organization_manager", "group_manager"].includes(
            String(me.trial.groupRole),
          ),
        userEmail: me.userEmail,
        userInTrial: me.trial !== null && !me.trial.completed,
        userIntPhone:
          me.phone === null
            ? undefined
            : `+${me.phone.callingCountryCode}${me.phone.nationalNumber}`,
        userName: me.userName,
      });
      permissions.update(
        me.permissions.map((action): { action: string } => ({ action })),
      );
      Bugsnag.setUser(me.userEmail, me.userEmail, me.userName);
      // Update user context dynamically
      CoralogixRum.setUserContext({
        // eslint-disable-next-line camelcase
        user_email: me.userEmail,
        // eslint-disable-next-line camelcase
        user_id: me.userEmail,
        // eslint-disable-next-line camelcase
        user_name: me.userName,
      });

      mixpanel.identify(me.userEmail);
      mixpanel.register({
        User: me.userName,
        // Intentional snake case
        // eslint-disable-next-line camelcase
        integrates_user_email: me.userEmail,
      });
      mixpanel.people.set({ $email: me.userEmail, $name: me.userName });

      initializeDelighted(me.userEmail, me.userName);
      setupSessionCheck(me.sessionExpiration);
    },
    onError: (error): void => {
      error.graphQLErrors.forEach(({ message }): void => {
        if (message === "Access denied or stakeholder not found") {
          msgError(t("groupAlerts.accessDenied"));
        } else {
          Logger.error("Couldn't load current user", message);
        }
      });
    },
  });

  if (data === undefined) {
    return undefined;
  }

  return data.me as ICurrentUser["me"] | undefined;
};

const useHasPendingInvitation = (
  isEnrolled: boolean | undefined,
): boolean | undefined => {
  const { t } = useTranslation();

  const { data } = useQuery(GET_USER_INVITATION, {
    fetchPolicy: "cache-and-network",
    onError: (error): void => {
      error.graphQLErrors.forEach(({ message }): void => {
        if (message === "Access denied or stakeholder not found") {
          msgError(t("groupAlerts.accessDenied"));
        } else {
          Logger.error("Couldn't load user invitation", message);
        }
      });
    },
    skip: isEnrolled === undefined || isEnrolled,
  });

  if (data === undefined) {
    return undefined;
  }

  return data.me.hasAllInvitationsPending;
};

export { useCurrentUser, useHasPendingInvitation };
