/* eslint-disable import/no-default-export */
import * as React from "react";
import { useLocation } from "react-router-dom";

import { AddRoot } from "./add-root";
import { useCurrentUser } from "./hooks";
import { PendingInvitation } from "./pending-invitations";

import { Autoenrollment } from "pages/auto-enrollment";
import { Dashboard } from "pages/dashboard";

const Home: React.FC = (): JSX.Element => {
  const location = useLocation();
  const searchParams = new URLSearchParams(location.search);
  const user = useCurrentUser();

  if (user === undefined) {
    return <div />;
  }

  const comesFromFastTrack = searchParams.get("fast_track_end") === "true";
  const isInTrial = user.trial !== null && !user.trial.completed;

  if (user.enrolled) {
    if (comesFromFastTrack || isInTrial) {
      return <AddRoot />;
    }

    return <Dashboard />;
  }

  return (
    <React.Fragment>
      <PendingInvitation isEnrolled={user.enrolled} />
      <Autoenrollment />
    </React.Fragment>
  );
};

export default Home;
