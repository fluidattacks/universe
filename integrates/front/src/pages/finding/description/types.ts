import type { VulnerabilitiesSummary } from "gql/graphql";

interface IHistoricTreatment {
  acceptanceDate?: string;
  acceptanceStatus?: string;
  date: string;
  justification?: string;
  treatment: string;
  assigned?: string;
  user: string;
}

interface IUnfulfilledRequirement {
  id: string;
  summary: string;
}

interface IFinding {
  attackVectorDescription: string;
  description: string;
  hacker?: string;
  id: string;
  recommendation: string;
  releaseDate: string | null;
  severityScore: number;
  severityScoreV4: number | null;
  severityVector: string;
  severityVectorV4: string | null;
  sorts: string;
  status: "SAFE" | "VULNERABLE";
  threat: string;
  title: string;
  unfulfilledRequirements: IUnfulfilledRequirement[];
  vulnerabilitiesSummary: VulnerabilitiesSummary;
}

interface IFindingDescriptionData {
  finding: IFinding;
  criteriaVulnerabilities: Record<string, IVulnerabilityCriteriaData>;
  criteriaRequirements: Record<string, IVulnerabilityCriteriaRequirement>;
}

interface IFindingDescriptionVars {
  canRetrieveHacker: boolean;
  canRetrieveSorts: boolean;
  findingId: string;
}

interface IVulnerabilityLanguage {
  title: string;
  description: string;
  impact: string;
  recommendation: string;
  threat: string;
}

interface IVulnerabilityScore {
  base: {
    attack_vector: string;
    attack_complexity: string;
    privileges_required: string;
    user_interaction: string;
    scope: string;
    confidentiality: string;
    integrity: string;
    availability: string;
  };
  temporal: {
    exploit_code_maturity: string;
    remediation_level: string;
    report_confidence: string;
  };
}

interface IVulnerabilityCriteriaData {
  en: IVulnerabilityLanguage;
  es: IVulnerabilityLanguage;
  findingNumber: string;
  score: IVulnerabilityScore;
  remediation_time: string;
  requirements: string[];
  metadata: Record<string, unknown>;
}

interface IVulnerabilityCriteriaLanguage {
  title: string;
  summary: string;
  description: string;
}

interface IVulnerabilityCriteriaRequirement {
  en: IVulnerabilityCriteriaLanguage;
  es: IVulnerabilityCriteriaLanguage;
  category: string;
  references: string;
  metadata: Record<string, unknown>;
  requirementNumber: string;
}

interface ICriteriaVulnerability {
  cursor: string;
  node: IVulnerabilityCriteriaData;
}

export type {
  ICriteriaVulnerability,
  IHistoricTreatment,
  IFinding,
  IFindingDescriptionData,
  IFindingDescriptionVars,
  IUnfulfilledRequirement,
  IVulnerabilityCriteriaData,
  IVulnerabilityCriteriaRequirement,
};
