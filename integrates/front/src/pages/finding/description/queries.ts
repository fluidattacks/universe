import { graphql } from "gql";

const GET_FINDING_DESCRIPTION = graphql(`
  fragment GetFindingDescription on Query {
    finding(identifier: $findingId) {
      id
      attackVectorDescription
      description
      hacker @include(if: $canRetrieveHacker)
      recommendation
      releaseDate

      severityScore
      severityScoreV4

      severityVector
      severityVectorV4
      sorts @include(if: $canRetrieveSorts)
      status
      threat
      title
      unfulfilledRequirements {
        id
        summary
      }

      vulnerabilitiesSummary {
        closed
        open
        openCritical
        openHigh
        openLow
        openMedium
      }
    }
  }
`);

const GET_LANGUAGE = graphql(`
  query GetLanguageQuery($groupName: String!) {
    group(groupName: $groupName) {
      language
      name
    }
  }
`);

const UPDATE_DESCRIPTION_MUTATION = graphql(`
  mutation UpdateFindingDescription(
    $attackVectorDescription: String!
    $cvssVector: String
    $cvssVectorV4: String!
    $description: String!
    $findingId: String!
    $isSeverityChanged: Boolean!
    $recommendation: String!
    $sorts: Sorts!
    $threat: String!
    $title: String!
    $unfulfilledRequirements: [String!]
  ) {
    updateDescription(
      attackVectorDescription: $attackVectorDescription
      description: $description
      findingId: $findingId
      recommendation: $recommendation
      sorts: $sorts
      threat: $threat
      title: $title
      unfulfilledRequirements: $unfulfilledRequirements
    ) {
      success
    }
    updateSeverity(
      cvss4Vector: $cvssVectorV4
      cvssVector: $cvssVector
      findingId: $findingId
    ) @include(if: $isSeverityChanged) {
      success
    }
  }
`);

const GET_CRITERIA = graphql(`
  fragment GetCriteria on Query {
    criteriaConnection(after: "", first: null) {
      total
      edges {
        cursor
        node {
          findingNumber
          requirements
          en {
            title
          }
          es {
            title
          }
        }
      }
      pageInfo {
        endCursor
        hasNextPage
      }
    }
  }
`);

const GET_CRITERIA_REQUIREMENTS = graphql(`
  fragment GetCriteriaRequirements on Query {
    criteriaRequirementsConnection(after: "", first: null) {
      total
      edges {
        cursor
        node {
          requirementNumber
          en {
            summary
          }
          es {
            summary
          }
        }
      }
      pageInfo {
        endCursor
        hasNextPage
      }
    }
  }
`);

const GET_DESCRIPTION_FINDINGS_QUERIES = graphql(`
  query GetDescriptionFindingsQueries(
    $canRetrieveHacker: Boolean!
    $canRetrieveSorts: Boolean!
    $findingId: String!
  ) {
    ...GetCriteria
    ...GetCriteriaRequirements
    ...GetFindingDescription
  }
`);

export {
  GET_CRITERIA,
  GET_CRITERIA_REQUIREMENTS,
  GET_DESCRIPTION_FINDINGS_QUERIES,
  GET_FINDING_DESCRIPTION,
  GET_LANGUAGE,
  UPDATE_DESCRIPTION_MUTATION,
};
