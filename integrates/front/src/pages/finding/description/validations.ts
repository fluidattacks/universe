import type { InferType, ObjectSchema, Schema } from "yup";
import { array, object, string } from "yup";

import { translate } from "utils/translations/translate";

const validationSchema = (
  titleSuggestions: string[],
): ObjectSchema<InferType<Schema>> =>
  object().shape({
    attackVectorDescription: string()
      .required(translate.t("validations.required"))
      .isValidTextBeginning(undefined, /^=|^\+|^@|^\translate.t|^\r/u)
      .isValidTextField(),
    description: string()
      .required(translate.t("validations.required"))
      .isValidTextBeginning()
      .isValidTextField(),
    recommendation: string()
      .required(translate.t("validations.required"))
      .isValidTextBeginning(undefined, /^=|^\+|^@|^\translate.t|^\r/u)
      .isValidTextField(),
    requirementIds: array()
      .min(1, translate.t("validations.someRequired"))
      .of(string().required(translate.t("validations.required"))),
    severityVector: string()
      .required(translate.t("validations.required"))
      .isValidTextBeginning()
      .isValidTextField(),
    severityVectorV4: string()
      .required(translate.t("validations.required"))
      .isValidTextBeginning()
      .isValidTextField(),
    sorts: string().required(translate.t("validations.required")),
    threat: string()
      .required(translate.t("validations.required"))
      .isValidTextBeginning(undefined, /^=|^\+|^@|^\translate.t|^\r/u)
      .isValidTextField(),
    title: string()
      .required(translate.t("validations.required"))
      .isValidDraftTitle()
      .isValidFindingTypology(titleSuggestions),
  });

export { validationSchema };
