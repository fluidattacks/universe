import { useMutation, useQuery } from "@apollo/client";
import { useAbility } from "@casl/react";
import { Formik } from "formik";
import _ from "lodash";
import { useCallback, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";

import { validationSchema } from "./validations";

import { authzPermissionsContext } from "context/authz/config";
import { DescriptionForm } from "features/vulnerabilities/description-form";
import { getFragmentData } from "gql/fragment-masking";
import type { Sorts } from "gql/graphql";
import { useAudit } from "hooks/use-audit";
import {
  GET_CRITERIA,
  GET_CRITERIA_REQUIREMENTS,
  GET_DESCRIPTION_FINDINGS_QUERIES,
  GET_FINDING_DESCRIPTION,
  GET_LANGUAGE,
  UPDATE_DESCRIPTION_MUTATION,
} from "pages/finding/description/queries";
import type {
  IFinding,
  IFindingDescriptionData,
  IVulnerabilityCriteriaData,
  IVulnerabilityCriteriaRequirement,
} from "pages/finding/description/types";
import { GET_FINDING_HEADER } from "pages/finding/queries";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

const FindingDescription: React.FC = (): JSX.Element => {
  const { addAuditEvent } = useAudit();
  const { t } = useTranslation();
  const { findingId, groupName } = useParams() as {
    findingId: string;
    groupName: string;
  };
  const permissions = useAbility(authzPermissionsContext);

  const [isEditing, setIsEditing] = useState(false);
  const [titleSuggestions, setTitleSuggestions] = useState<string[]>([]);
  const [finding, setFinding] = useState<IFinding | undefined>(undefined);
  const [criteriaVulnerabilities, setCriteriaVulnerabilities] = useState<
    Record<string, IVulnerabilityCriteriaData>
  >({});
  const [criteriaRequirements, setCriteriaRequirements] = useState<
    Record<string, IVulnerabilityCriteriaRequirement>
  >({});

  // GraphQL operations
  const { data: groupData } = useQuery(GET_LANGUAGE, {
    fetchPolicy: "no-cache",
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.warning("An error occurred loading group language", error);
      });
    },
    variables: { groupName },
  });

  const { refetch } = useQuery(GET_DESCRIPTION_FINDINGS_QUERIES, {
    fetchPolicy: "cache-and-network",
    onCompleted: (result): void => {
      addAuditEvent("Finding.Description", findingId);
      const findingResult = getFragmentData(GET_FINDING_DESCRIPTION, result);
      setFinding(findingResult.finding as IFinding);

      const criteriaResult = getFragmentData(GET_CRITERIA, result);

      const formattedVulns = Object.fromEntries(
        criteriaResult.criteriaConnection.edges.map(
          (vuln): [string, IVulnerabilityCriteriaData] => [
            vuln.node.findingNumber,
            vuln.node as IVulnerabilityCriteriaData,
          ],
        ),
      );
      setCriteriaVulnerabilities(formattedVulns);

      const criteriaRequirementsResult = getFragmentData(
        GET_CRITERIA_REQUIREMENTS,
        result,
      );
      const formattedRequirements = Object.fromEntries(
        criteriaRequirementsResult.criteriaRequirementsConnection.edges.map(
          (req): [string, IVulnerabilityCriteriaRequirement] => [
            req.node.requirementNumber,
            req.node as IVulnerabilityCriteriaRequirement,
          ],
        ),
      );
      setCriteriaRequirements(formattedRequirements);
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.warning("An error occurred loading finding description", error);
      });
    },
    skip: groupData === undefined,
    variables: {
      canRetrieveHacker: permissions.can(
        "integrates_api_resolvers_finding_hacker_resolve",
      ),
      canRetrieveSorts: permissions.can(
        "integrates_api_resolvers_finding_sorts_resolve",
      ),
      findingId,
    },
  });

  const [updateDescription] = useMutation(UPDATE_DESCRIPTION_MUTATION, {
    onCompleted: (result: {
      updateDescription: { success: boolean };
    }): void => {
      if (result.updateDescription.success) {
        msgSuccess(t("groupAlerts.updated"), t("groupAlerts.updatedTitle"));
        void refetch();
      }
    },
    onError: (updateError): void => {
      updateError.graphQLErrors.forEach(({ message }): void => {
        switch (message) {
          case "Exception - Invalid field in form":
            msgError(t("validations.invalidValueInField"));
            break;
          case "Exception - Invalid characters":
            msgError(t("validations.invalidChar"));
            break;
          case "Exception - Finding with the same threat already exists":
            msgError(t("validations.addFindingModal.duplicatedThreat"));
            break;
          case "Exception - Finding with the same description already exists":
            msgError(t("validations.addFindingModal.duplicatedDescription"));
            break;
          case "Exception - Finding with the same description, threat and severity already exists":
            msgError(
              t("validations.addFindingModal.duplicatedMachineDescription"),
            );
            break;
          case "Exception - Error invalid severity CVSS v3.1 vector string":
            msgError(
              t(
                "searchFindings.tabVuln.severityInfo.alerts.invalidSeverityVector",
              ),
            );
            break;
          case "Exception - Error invalid severity CVSS v4 vector string":
            msgError(
              t(
                "searchFindings.tabVuln.severityInfo.alerts.invalidSeverityVectorV4",
              ),
            );
            break;
          case "Exception - The requirement is not valid in the vulnerability":
            msgError(t("validations.addFindingModal.invalidRequirement"));
            break;
          default:
            msgError(t("groupAlerts.errorTextsad"));
            Logger.warning(
              "An error occurred updating finding description",
              updateError,
            );
        }
      });
    },
    refetchQueries: [
      {
        query: GET_FINDING_HEADER,
        variables: {
          findingId,
        },
      },
    ],
  });

  const handleDescriptionSubmit = useCallback(
    async (values: IFinding & { requirementIds: string[] }): Promise<void> => {
      setIsEditing(false);
      const isSeverityChanged =
        values.severityVector !== finding?.severityVector ||
        values.severityVectorV4 !== finding.severityVectorV4;
      await updateDescription({
        variables: {
          attackVectorDescription: values.attackVectorDescription,
          cvssVector: isSeverityChanged ? values.severityVector : "",
          cvssVectorV4: isSeverityChanged
            ? (values.severityVectorV4 ?? "")
            : "",
          description: values.description,
          findingId,
          isSeverityChanged,
          recommendation: values.recommendation,
          sorts: values.sorts as Sorts,
          threat: values.threat,
          title: values.title,
          unfulfilledRequirements: values.requirementIds,
        },
      });
    },
    [
      finding?.severityVector,
      finding?.severityVectorV4,
      findingId,
      updateDescription,
    ],
  );

  if (_.isUndefined(finding) || _.isEmpty(finding)) {
    return <div />;
  }

  const isDraft = _.isEmpty(finding.releaseDate);
  const findingDescriptionData: IFindingDescriptionData = {
    criteriaRequirements,
    criteriaVulnerabilities,
    finding,
  };

  return (
    <React.StrictMode>
      <Formik
        enableReinitialize={true}
        initialValues={{
          ...finding,
          requirementIds: finding.unfulfilledRequirements.map(
            (unfulfilledRequirement): string => unfulfilledRequirement.id,
          ),
        }}
        name={"editDescription"}
        onSubmit={handleDescriptionSubmit}
        validationSchema={validationSchema(titleSuggestions)}
      >
        <DescriptionForm
          data={findingDescriptionData}
          groupLanguage={groupData?.group.language ?? undefined}
          isDraft={isDraft}
          isEditing={isEditing}
          setEditing={setIsEditing}
          setSuggestions={setTitleSuggestions}
        />
      </Formik>
    </React.StrictMode>
  );
};

export { FindingDescription };
