import { PureAbility } from "@casl/ability";
import { fireEvent, screen, waitFor } from "@testing-library/react";
import { GraphQLError } from "graphql";
import { get, isEqual } from "lodash";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import { Route, Routes } from "react-router-dom";
import {
  type AnyObject,
  type InferType,
  type Schema,
  ValidationError,
} from "yup";

import { validationSchema } from "./validations";

import {
  authzGroupContext,
  authzPermissionsContext,
} from "context/authz/config";
import type {
  GetDescriptionFindingsQueriesQuery as GetDescriptionFindingsQueries,
  UpdateFindingDescriptionMutation,
} from "gql/graphql";
import { Sorts } from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";
import { FindingDescription } from "pages/finding/description";
import {
  GET_DESCRIPTION_FINDINGS_QUERIES,
  UPDATE_DESCRIPTION_MUTATION,
} from "pages/finding/description/queries";
import type {
  IFinding,
  IFindingDescriptionVars,
} from "pages/finding/description/types";
import { msgSuccess } from "utils/notifications";
import { selectOption } from "utils/test-utils";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

describe("findingDescription", (): void => {
  const validObject = {
    attackVectorDescription: "Test attack vector.",
    description:
      "The system uses the version of a software or dependency with known " +
      "vulnerabilities.'",
    recommendation:
      "Update the affected software to the versions recommended by " +
      "the vendor.",
    requirementIds: ["262"],
    severityVector:
      "CVSS:3.1/AV:N/AC:H/PR:L/UI:N/S:U/C:L/I:L/A:L/E:P/RL:O/RC:C",
    severityVectorV4:
      "CVSS:4.0/AV:N/AC:H/AT:N/PR:L/UI:N/VC:L/VI:L/VA:L/SC:N/SI:N/SA:N/E:P",
    sorts: "NO",
    threat: "test",
    title: "011. Use of software with known vulnerabilities",
  };
  function validationObject(
    property: string,
    value: string,
  ): InferType<Schema> {
    switch (property) {
      case "attackVectorDescription": {
        return {
          attackVectorDescription: value,
          description: validObject.description,
          recommendation: validObject.recommendation,
          requirementIds: validObject.requirementIds,
          severityVector: validObject.severityVector,
          severityVectorV4: validObject.severityVectorV4,
          sorts: validObject.sorts,
          threat: validObject.threat,
          title: validObject.title,
        };
      }
      case "description": {
        return {
          attackVectorDescription: validObject.attackVectorDescription,
          description: value,
          recommendation: validObject.recommendation,
          requirementIds: validObject.requirementIds,
          severityVector: validObject.severityVector,
          severityVectorV4: validObject.severityVectorV4,
          sorts: validObject.sorts,
          threat: validObject.threat,
          title: validObject.title,
        };
      }
      case "recommendation": {
        return {
          attackVectorDescription: validObject.attackVectorDescription,
          description: validObject.description,
          recommendation: value,
          requirementIds: validObject.requirementIds,
          severityVector: validObject.severityVector,
          severityVectorV4: validObject.severityVectorV4,
          sorts: validObject.sorts,
          threat: validObject.threat,
          title: validObject.title,
        };
      }
      case "requirementIds": {
        return {
          attackVectorDescription: validObject.attackVectorDescription,
          description: validObject.description,
          recommendation: validObject.recommendation,
          requirementIds: [value],
          severityVector: validObject.severityVector,
          severityVectorV4: validObject.severityVectorV4,
          sorts: validObject.sorts,
          threat: validObject.threat,
          title: validObject.title,
        };
      }
      case "severityVector": {
        return {
          attackVectorDescription: validObject.attackVectorDescription,
          description: validObject.description,
          recommendation: validObject.recommendation,
          requirementIds: validObject.requirementIds,
          severityVector: value,
          severityVectorV4: validObject.severityVectorV4,
          sorts: validObject.sorts,
          threat: validObject.threat,
          title: validObject.title,
        };
      }
      case "severityVectorV4": {
        return {
          attackVectorDescription: validObject.attackVectorDescription,
          description: validObject.description,
          recommendation: validObject.recommendation,
          requirementIds: validObject.requirementIds,
          severityVector: validObject.severityVector,
          severityVectorV4: value,
          sorts: validObject.sorts,
          threat: validObject.threat,
          title: validObject.title,
        };
      }
      case "sorts": {
        return {
          attackVectorDescription: validObject.attackVectorDescription,
          description: validObject.description,
          recommendation: validObject.recommendation,
          requirementIds: validObject.requirementIds,
          severityVector: validObject.severityVector,
          severityVectorV4: validObject.severityVectorV4,
          sorts: value,
          threat: validObject.threat,
          title: validObject.title,
        };
      }
      case "threat": {
        return {
          attackVectorDescription: validObject.attackVectorDescription,
          description: validObject.description,
          recommendation: validObject.recommendation,
          requirementIds: validObject.requirementIds,
          severityVector: validObject.severityVector,
          severityVectorV4: validObject.severityVectorV4,
          sorts: validObject.sorts,
          threat: value,
          title: validObject.title,
        };
      }
      case "title": {
        return {
          attackVectorDescription: validObject.attackVectorDescription,
          description: validObject.description,
          recommendation: validObject.recommendation,
          requirementIds: validObject.requirementIds,
          severityVector: validObject.severityVector,
          severityVectorV4: validObject.severityVectorV4,
          sorts: validObject.sorts,
          threat: validObject.threat,
          title: value,
        };
      }
      default: {
        return validObject;
      }
    }
  }
  const testValidation = (
    titleSuggestions: string[],
    obj: AnyObject,
  ): InferType<Schema> => {
    return validationSchema(titleSuggestions).validateSync(obj);
  };
  const finding: IFinding = {
    attackVectorDescription: "Run a reverse shell",
    description: "It's possible to execute shell commands from the site",
    hacker: "hacker@test.com",
    id: "413372600",
    recommendation: "Use good security practices and standards".repeat(13),
    releaseDate: null,
    severityScore: 3.8,
    severityScoreV4: 3.8,
    severityVector:
      "CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:L/A:N/E:U/RL:O/RC:C",
    severityVectorV4:
      "CVSS:4.0/AV:N/AT:N/AC:L/PR:L/UI:N/VC:N/VI:L/VA:N/SC:N/SI:N/SA:N/E:U",
    sorts: "No",
    status: "VULNERABLE",
    threat: "External attack",
    title: "004. Remote command execution",
    unfulfilledRequirements: [
      {
        id: "161",
        summary:
          "The source code must have secure default options ensuring secure failures in the application (try, catch/except; default for switches).",
      },
      {
        id: "173",
        summary:
          "The system must discard all potentially harmful information received via data inputs.",
      },
    ],
    vulnerabilitiesSummary: {
      closed: 1,
      open: 1,
      openCritical: 1,
      openHigh: 1,
      openLow: 1,
      openMedium: 1,
    },
  };

  const criteriaVulnerabilities = [
    {
      cursor: "004",
      node: {
        en: {
          title: "004. Remote command execution",
        },
        es: {
          title: "004. Remote command execution",
        },
        findingNumber: "004",
        requirements: ["161", "173", "176"],
      },
    },
  ];

  const criteriaConnection = {
    edges: criteriaVulnerabilities,
    pageInfo: {
      endCursor: "004",
      hasNextPage: false,
    },
    total: 1,
  };

  const criteriaRequirements = [
    {
      cursor: "161",
      node: {
        en: {
          summary:
            "The source code must have secure default options ensuring secure failures in the application (try, catch/except; default for switches).",
        },
        es: {
          summary:
            "El código debe definir opciones por defecto seguras que garanticen fallos seguros en la aplicación. (try, catch/except; default en switches).",
        },
        requirementNumber: "161",
      },
    },
    {
      cursor: "173",
      node: {
        en: {
          summary:
            "The system must discard all potentially harmful information received via data inputs.",
        },
        es: {
          summary:
            "El sistema debe descartar toda la información potencialmente insegura que sea recibida por entradas de datos.",
        },
        requirementNumber: "173",
      },
    },
    {
      cursor: "176",
      node: {
        en: {
          summary:
            "The system must restrict access to system objects that have sensitive content. It should only allow access to authorized users.",
        },
        es: {
          summary:
            "El sistema debe restringir el acceso a objetos del sistema que tengan contenido sensible. Sólo permitirá su acceso a usuarios autorizados.",
        },
        requirementNumber: "176",
      },
    },
  ];

  const criteriaRequirementsConnection = {
    edges: criteriaRequirements,
    pageInfo: {
      endCursor: "004",
      hasNextPage: false,
    },
    total: 1,
  };

  const findingDescriptionVars: IFindingDescriptionVars = {
    canRetrieveHacker: true,
    canRetrieveSorts: true,
    findingId: "413372600",
  };

  const mocks = [
    graphql
      .link(LINK)
      .query(
        GET_DESCRIPTION_FINDINGS_QUERIES,
        ({
          variables,
        }): StrictResponse<
          IErrorMessage | { data: GetDescriptionFindingsQueries }
        > => {
          if (
            Object.keys(findingDescriptionVars).every(
              (key): boolean =>
                variables[key as keyof typeof variables] ===
                get(findingDescriptionVars, key),
            )
          ) {
            return HttpResponse.json({
              data: {
                __typename: "Query",
                ...{ criteriaConnection },
                ...{ criteriaRequirementsConnection },
                ...{ finding: { ...finding, sorts: finding.sorts as Sorts } },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error getting finding description")],
          });
        },
      ),
    graphql
      .link(LINK)
      .mutation(
        UPDATE_DESCRIPTION_MUTATION,
        ({
          variables,
        }):
          | StrictResponse<{ data: UpdateFindingDescriptionMutation }>
          | undefined => {
          const {
            findingId,
            title,
            threat,
            sorts,
            attackVectorDescription,
            recommendation,
            description,
            unfulfilledRequirements,
          } = variables;
          if (
            findingId === "413372600" &&
            title === "004. Remote command execution" &&
            attackVectorDescription === "test" &&
            threat === "test" &&
            recommendation === "test" &&
            description === "test" &&
            sorts === Sorts.Yes &&
            isEqual(unfulfilledRequirements, ["173"])
          ) {
            return HttpResponse.json({
              data: {
                updateDescription: {
                  success: true,
                },
                updateSeverity: {
                  success: true,
                },
              },
            });
          }

          return undefined;
        },
      ),
  ];

  it("should render a component", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_resolvers_finding_hacker_resolve" },
      { action: "integrates_api_resolvers_finding_sorts_resolve" },
    ]);

    const mockedServices = new PureAbility<string>([
      { action: "can_report_vulnerabilities" },
    ]);

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <authzGroupContext.Provider value={mockedServices}>
          <Routes>
            <Route
              element={<FindingDescription />}
              path={"/:groupName/vulns/:findingId/description/*"}
            />
          </Routes>
        </authzGroupContext.Provider>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/TEST/vulns/413372600/description"],
        },
        mocks,
      },
    );

    await waitFor((): void => {
      expect(screen.queryByText("Run a reverse shell")).toBeInTheDocument();
    });

    expect(
      screen.queryByText("searchFindings.tabDescription.hacker"),
    ).toBeInTheDocument();
    expect(screen.queryByText("hacker@test.com")).toBeInTheDocument();

    expect(
      screen.queryByText("searchFindings.tabDescription.description.text"),
    ).toBeInTheDocument();
    expect(
      screen.queryByText(
        "It's possible to execute shell commands from the site",
      ),
    ).toBeInTheDocument();

    expect(
      screen.queryByText("searchFindings.tabDescription.requirements.text"),
    ).toBeInTheDocument();
    expect(
      screen.getByText(
        "161. The source code must have secure default options ensuring secure failures in the application (try, catch/except; default for switches).",
      ),
    ).toBeInTheDocument();
    expect(
      screen.getByText(
        "173. The system must discard all potentially harmful information received via data inputs.",
      ),
    ).toBeInTheDocument();

    expect(
      screen.queryByText("searchFindings.tabDescription.threat.text"),
    ).toBeInTheDocument();
    expect(screen.queryByText("External attack")).toBeInTheDocument();

    expect(
      screen.queryByText("searchFindings.tabDescription.recommendation.text"),
    ).toBeInTheDocument();
    expect(
      screen.queryByText(
        "Use good security practices and standards".repeat(13),
      ),
    ).toBeInTheDocument();

    expect(
      screen.getByText("group.findings.buttons.help.button"),
    ).toBeInTheDocument();

    expect(
      screen.queryByText(
        "searchFindings.tabDescription.defaultSeverity.textVectorString",
      ),
    ).toBeInTheDocument();
    expect(
      screen.getByText(
        "CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:L/A:N/E:U/RL:O/RC:C",
      ),
    ).toBeInTheDocument();

    expect(
      screen.queryByText(
        "searchFindings.tabDescription.defaultSeverityV4.textVectorString",
      ),
    ).toBeInTheDocument();
    expect(
      screen.getByText(
        "CVSS:4.0/AV:N/AT:N/AC:L/PR:L/UI:N/VC:N/VI:L/VA:N/SC:N/SI:N/SA:N/E:U",
      ),
    ).toBeInTheDocument();

    expect(
      screen.queryByText(
        "searchFindings.tabDescription.defaultSeverity.textSeverityScore",
      ),
    ).toBeInTheDocument();
    expect(
      screen.queryByText(
        "searchFindings.tabDescription.defaultSeverityV4.textSeverityScore",
      ),
    ).toBeInTheDocument();

    expect(screen.getAllByText("3.8")).toHaveLength(2);
    expect(screen.getAllByText("Low")).toHaveLength(2);

    jest.clearAllMocks();
  });

  it("should update description", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_resolvers_finding_hacker_resolve" },
      { action: "integrates_api_mutations_update_finding_description_mutate" },
      { action: "integrates_api_resolvers_finding_sorts_resolve" },
    ]);

    const mockedServices = new PureAbility<string>([
      { action: "can_report_vulnerabilities" },
    ]);

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <authzGroupContext.Provider value={mockedServices}>
          <Routes>
            <Route
              element={<FindingDescription />}
              path={"/:groupName/vulns/:findingId/description/*"}
            />
          </Routes>
        </authzGroupContext.Provider>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/TEST/vulns/413372600/description"],
        },
        mocks,
      },
    );

    await waitFor((): void => {
      expect(screen.queryByText("Run a reverse shell")).toBeInTheDocument();
    });

    expect(
      screen.getByText("searchFindings.tabDescription.editable.text"),
    ).toBeInTheDocument();

    fireEvent.click(
      screen.getByText("searchFindings.tabDescription.editable.text"),
    );

    expect(
      screen.queryByText("searchFindings.tabDescription.editable.cancel"),
    ).toBeInTheDocument();

    expect(
      screen.queryByText("searchFindings.tabDescription.editable.text"),
    ).not.toBeInTheDocument();

    await waitFor((): void => {
      expect(
        screen.getByText("searchFindings.tabDescription.save.text"),
      ).toBeDisabled();
    });

    fireEvent.change(screen.getByRole("textbox", { name: "description" }), {
      target: { value: "test" },
    });

    await waitFor((): void => {
      expect(
        screen.getByText("searchFindings.tabDescription.save.text"),
      ).toBeEnabled();
    });

    expect(
      screen.getAllByRole("checkbox", {
        name: "requirementIds",
      })[0],
    ).toBeChecked();

    expect(
      screen.getAllByRole("checkbox", {
        name: "requirementIds",
      })[1],
    ).toBeChecked();

    fireEvent.click(
      screen.getAllByRole("checkbox", {
        name: "requirementIds",
      })[0],
    );

    await waitFor((): void => {
      expect(
        screen.getAllByRole("checkbox", {
          name: "requirementIds",
        })[0],
      ).not.toBeChecked();
    });

    fireEvent.change(
      screen.getByRole("textbox", { name: "attackVectorDescription" }),
      {
        target: { value: "test" },
      },
    );

    fireEvent.change(screen.getByRole("textbox", { name: "threat" }), {
      target: { value: "test" },
    });

    fireEvent.change(screen.getByRole("textbox", { name: "recommendation" }), {
      target: { value: "test" },
    });

    await selectOption("sorts", "YES");

    fireEvent.click(
      screen.getByText("searchFindings.tabDescription.save.text"),
    );

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "groupAlerts.updated",
        "groupAlerts.updatedTitle",
      );
    });

    jest.clearAllMocks();
  });

  it("should display required field errors of validation schema", (): void => {
    expect.hasAssertions();

    const errorString = "Required field";
    Object.keys(validObject).forEach((key): void => {
      const testRequiredField = (): void => {
        testValidation([validObject.title], validationObject(key, ""));
      };

      expect(testRequiredField).toThrow(new ValidationError(errorString));
    });
  });

  it("should display valid text beginning errors of validation schema", (): void => {
    expect.hasAssertions();

    const errorString = "Field cannot begin with the following character: '='";
    [
      "attackVectorDescription",
      "description",
      "recommendation",
      "severityVector",
      "severityVectorV4",
      "threat",
    ].forEach((key): void => {
      const testValidTextBeginning = (): void => {
        testValidation([validObject.title], validationObject(key, "=test"));
      };

      expect(testValidTextBeginning).toThrow(new ValidationError(errorString));
    });
  });

  it("should display valid text field errors of validation schema", (): void => {
    expect.hasAssertions();

    const errorString = "Field cannot contain the following characters: '^'";
    [
      "attackVectorDescription",
      "description",
      "recommendation",
      "severityVector",
      "severityVectorV4",
      "threat",
    ].forEach((key): void => {
      const testValidTextField = (): void => {
        testValidation([validObject.title], validationObject(key, "test^test"));
      };

      expect(testValidTextField).toThrow(new ValidationError(errorString));
    });
  });

  it("should display valid title field errors of validation schema", (): void => {
    expect.hasAssertions();

    const testValidDraftTitle = (): void => {
      testValidation([validObject.title], validationObject("title", "00.test"));
    };
    const testValidFindingTypology = (): void => {
      testValidation(
        [validObject.title],
        validationObject("title", "001. test"),
      );
    };

    expect(testValidDraftTitle).toThrow(
      new ValidationError("The title format is not valid"),
    );
    expect(testValidFindingTypology).toThrow(
      new ValidationError("The vulnerability typology is not valid"),
    );
  });
});
