import { graphql } from "gql";

const GET_ROOTS = graphql(`
  query GetRootsAtMachine($groupName: String!) {
    group(groupName: $groupName) {
      name
      roots {
        ... on GitRoot {
          nickname
          state
        }
        ... on URLRoot {
          nickname
          state
        }
        ... on IPRoot {
          nickname
          state
        }
      }
    }
  }
`);

const SUBMIT_MACHINE_JOB = graphql(`
  mutation SubmitMachineJob($findingId: String!, $rootNicknames: [String!]) {
    submitMachineJob(findingId: $findingId, rootNicknames: $rootNicknames) {
      message
      success
    }
  }
`);

export { SUBMIT_MACHINE_JOB, GET_ROOTS };
