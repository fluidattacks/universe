interface IGroupRoot {
  nickname: string;
  state: string;
}

interface IQueue {
  rootNicknames: string[];
  onClose: () => void;
  onSubmit: (rootNicknames: string[]) => Promise<void>;
}

export type { IGroupRoot, IQueue };
