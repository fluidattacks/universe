import { Button, Container } from "@fluidattacks/design";
import type { ITabProps } from "@fluidattacks/design/dist/components/tabs/types";
import { capitalize } from "lodash";
import { StrictMode, useContext } from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";

import { Authorize } from "components/@core/authorize";
import { SectionHeader } from "components/section-header";
import { featurePreviewContext } from "context/feature-preview";
import { useAuthz } from "hooks/use-authz";
import { useGroupUrl } from "pages/group/hooks";

interface IFindingOverviewProps {
  estRemediationTime: string;
  findingStatus: "DRAFT" | "SAFE" | "VULNERABLE";
  findingTitle: string;
  hacker?: string;
  isDraft: boolean;
  openDeleteModal: () => void;
}

const FindingHeader = ({
  estRemediationTime,
  findingStatus,
  findingTitle,
  hacker,
  isDraft,
  openDeleteModal,
}: Readonly<IFindingOverviewProps>): JSX.Element => {
  const { t } = useTranslation();
  const { url } = useGroupUrl("/vulns/:findingId/*");
  const { featurePreview } = useContext(featurePreviewContext);
  const { groupName, organizationName } = useParams() as {
    groupName: string;
    organizationName: string;
  };

  const { canResolve } = useAuthz();
  const canRenderMachine =
    canResolve("finding_machine_jobs") && hacker === "machine@fluidattacks.com";
  const canRenderConsulting = canResolve("finding_consulting") && !isDraft;
  const canRenderObservations = canResolve("finding_observations");
  const tabs: ITabProps[] = [
    {
      id: "vulnItem",
      label: t("searchFindings.tabVuln.tabTitle"),
      link: `${url}/locations`,
      tooltip: t("searchFindings.tabVuln.tooltip"),
    },
    {
      id: "infoItem",
      label: t("searchFindings.tabDescription.tabTitle"),
      link: `${url}/description`,
      tooltip: t("searchFindings.tabDescription.tooltip"),
    },
    {
      id: "cssv2Item",
      label: t("searchFindings.tabSeverity.tabTitle"),
      link: `${url}/severity`,
      tooltip: t("searchFindings.tabSeverity.tooltip"),
    },
    {
      id: "evidenceItem",
      label: t("searchFindings.tabEvidence.tabTitle"),
      link: `${url}/evidence`,
      tooltip: t("searchFindings.tabEvidence.tooltip"),
    },
    {
      id: "trackingItem",
      label: t("searchFindings.tabTracking.tabTitle"),
      link: `${url}/tracking`,
      tooltip: t("searchFindings.tabTracking.tooltip"),
    },
    {
      id: "recordsItem",
      label: t("searchFindings.tabRecords.tabTitle"),
      link: `${url}/records`,
      tooltip: t("searchFindings.tabRecords.tooltip"),
    },
    ...(canRenderMachine
      ? [
          {
            id: "machineItem",
            label: t("searchFindings.tabMachine.tabTitle"),
            link: `${url}/machine`,
            tooltip: t("searchFindings.tabMachine.tooltip"),
          },
        ]
      : []),
    ...(canRenderConsulting
      ? [
          {
            id: "commentItem",
            label: t("searchFindings.tabComments.tabTitle"),
            link: `${url}/consulting`,
            tooltip: t("searchFindings.tabComments.tooltip"),
          },
        ]
      : []),
    ...(canRenderObservations
      ? [
          {
            id: "observationsItem",
            label: t("searchFindings.tabObservations.tabTitle"),
            link: `${url}/observations`,
            tooltip: t("searchFindings.tabObservations.tooltip"),
          },
        ]
      : []),
  ];

  return (
    <StrictMode>
      <SectionHeader
        goBackTo={`/orgs/${organizationName}/groups/${groupName}/vulns`}
        header={`${capitalize(groupName)} | ${findingTitle}`}
        tabs={tabs}
        tag={
          findingStatus === "VULNERABLE"
            ? {
                label: t("vulnerability.header.remediationTime"),
                title: estRemediationTime,
              }
            : undefined
        }
      />
      {featurePreview ? undefined : (
        <Container display={"flex"} justify={"end"} px={1.25} py={1.25}>
          <Authorize
            can={"integrates_api_mutations_remove_finding_mutate"}
            have={"can_report_vulnerabilities"}
          >
            <Button
              id={"delete-finding"}
              onClick={openDeleteModal}
              tooltip={t("searchFindings.delete.btn.tooltip")}
              variant={"secondary"}
            >
              {t("searchFindings.delete.btn.text")}
            </Button>
          </Authorize>
        </Container>
      )}
    </StrictMode>
  );
};

export { FindingHeader };
