import { PureAbility } from "@casl/ability";
import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import * as React from "react";
import { Route, Routes } from "react-router-dom";

import { REMOVE_FINDING_MUTATION } from "./queries";

import { Finding } from ".";
import type { IAuthContext } from "context/auth";
import { authContext } from "context/auth";
import {
  authzGroupContext,
  authzPermissionsContext,
} from "context/authz/config";
import type {
  GetFindingVulnsQueriesQuery,
  GetVulnsQuery,
  RemoveFindingMutationMutation as RemoveFinding,
} from "gql/graphql";
import { RemoveFindingJustification } from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";
import {
  GET_FINDING_VULNS_QUERIES,
  GET_VULNS,
} from "pages/finding/vulnerabilities/queries";
import { msgError, msgSuccess } from "utils/notifications";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

interface IWrapperProps {
  readonly children: React.ReactNode;
  readonly mockedPermissions: PureAbility<string>;
}

const Wrapper = ({
  children,
  mockedPermissions,
}: IWrapperProps): JSX.Element => (
  <authzPermissionsContext.Provider value={mockedPermissions}>
    {children}
  </authzPermissionsContext.Provider>
);

describe("finding", (): void => {
  const btnCancel = "Cancel";
  const btnConfirm = "Confirm";
  const graphqlMocked = graphql.link(LINK);

  const memoryRouter = {
    initialEntries: ["/orgs/okada/groups/TEST/vulns/438679960/locations"],
  };
  const mockedVulnsQueries = [
    graphqlMocked.query(
      GET_FINDING_VULNS_QUERIES,
      (): StrictResponse<{ data: GetFindingVulnsQueriesQuery }> => {
        return HttpResponse.json({
          data: {
            __typename: "Query",
            ...{
              finding: {
                __typename: "Finding",
                draftsConnection: undefined,
                id: "438679960",
                vulnerabilitiesConnection: {
                  edges: [],
                  pageInfo: {
                    endCursor: "test-cursor=",
                    hasNextPage: false,
                  },
                },
                zeroRiskConnection: {
                  edges: [],
                  pageInfo: {
                    endCursor: "test-cursor=",
                    hasNextPage: false,
                  },
                },
              },
            },
          },
        });
      },
    ),
    graphqlMocked.query(
      GET_VULNS,
      (): StrictResponse<{ data: GetVulnsQuery }> => {
        return HttpResponse.json({
          data: {
            __typename: "Query",
            ...{
              finding: {
                __typename: "Finding",
                id: "438679960",
                vulnerabilities: {
                  edges: [],
                  pageInfo: {
                    endCursor: "test-cursor=",
                    hasNextPage: false,
                  },
                  total: 0,
                },
              },
            },
          },
        });
      },
    ),
  ];

  it("should render a component", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_submit_draft_mutate" },
      { action: "integrates_api_resolvers_finding_hacker_resolve" },
      { action: "see_expert_button" },
    ]);

    render(
      <Wrapper mockedPermissions={mockedPermissions}>
        <authzGroupContext.Provider
          value={
            new PureAbility([
              { action: "can_report_vulnerabilities" },
              { action: "integrates_api_resolvers_finding_hacker_resolve" },
              { action: "see_expert_button" },
            ])
          }
        >
          <Routes>
            <Route
              element={<Finding />}
              path={
                "/orgs/:organizationName/groups/:groupName/vulns/:findingId/*"
              }
            />
          </Routes>
        </authzGroupContext.Provider>
      </Wrapper>,
      { memoryRouter, mocks: mockedVulnsQueries },
    );

    await waitFor((): void => {
      expect(
        screen.queryByText("Test | 050. Guessed weak credentials"),
      ).toBeInTheDocument();
    });
  });

  it("should prompt delete justification", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();
    jest.spyOn(console, "warn").mockImplementation();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_remove_finding_mutate" },
      { action: "integrates_api_resolvers_finding_hacker_resolve" },
      { action: "see_expert_button" },
    ]);
    render(
      <Wrapper mockedPermissions={mockedPermissions}>
        <authzGroupContext.Provider
          value={
            new PureAbility([
              { action: "can_report_vulnerabilities" },
              { action: "integrates_api_resolvers_finding_hacker_resolve" },
              { action: "see_expert_button" },
            ])
          }
        >
          <Routes>
            <Route
              element={<Finding />}
              path={
                "/orgs/:organizationName/groups/:groupName/vulns/:findingId/*"
              }
            />
          </Routes>
        </authzGroupContext.Provider>
      </Wrapper>,
      { memoryRouter, mocks: mockedVulnsQueries },
    );

    await waitFor((): void => {
      expect(
        screen.queryByRole("checkbox", { name: "cvssToggle" }),
      ).toBeInTheDocument();
    });

    await userEvent.click(screen.getByRole("checkbox", { name: "cvssToggle" }));

    expect(
      screen.getByRole("checkbox", { name: "cvssToggle" }),
    ).not.toBeChecked();

    await userEvent.click(screen.getByRole("checkbox", { name: "cvssToggle" }));

    await waitFor((): void => {
      expect(
        screen.getByRole("checkbox", { name: "cvssToggle" }),
      ).toBeChecked();
    });

    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.delete.btn.text"),
      ).toBeInTheDocument();
    });
    await waitFor((): void => {
      expect(
        screen.queryByText("group.findings.buttons.help.button"),
      ).toBeInTheDocument();
    });

    await userEvent.click(screen.getByText("searchFindings.delete.btn.text"));
    await waitFor((): void => {
      expect(screen.queryByText(btnCancel)).toBeInTheDocument();
    });

    await userEvent.click(screen.getByText(btnCancel));
    await waitFor((): void => {
      expect(screen.queryByText(btnCancel)).not.toBeInTheDocument();
    });
  });

  it("should delete finding", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();
    jest.spyOn(console, "warn").mockImplementation();

    const deleteMutationMock = [
      graphqlMocked.mutation(
        REMOVE_FINDING_MUTATION,
        ({
          variables,
        }): StrictResponse<IErrorMessage | { data: RemoveFinding }> => {
          const { findingId, justification } = variables;
          if (
            findingId === "438679960" &&
            justification === RemoveFindingJustification.Duplicated
          ) {
            return HttpResponse.json({
              data: {
                removeFinding: {
                  success: true,
                },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error removing finding")],
          });
        },
      ),
    ];

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_remove_finding_mutate" },
      { action: "integrates_api_resolvers_finding_hacker_resolve" },
      { action: "see_expert_button" },
    ]);
    render(
      <Wrapper mockedPermissions={mockedPermissions}>
        <authzGroupContext.Provider
          value={
            new PureAbility([
              { action: "can_report_vulnerabilities" },
              { action: "integrates_api_resolvers_finding_hacker_resolve" },
              { action: "see_expert_button" },
            ])
          }
        >
          <Routes>
            <Route
              element={<Finding />}
              path={
                "/orgs/:organizationName/groups/:groupName/vulns/:findingId/*"
              }
            />
          </Routes>
        </authzGroupContext.Provider>
      </Wrapper>,
      { memoryRouter, mocks: [...mockedVulnsQueries, ...deleteMutationMock] },
    );

    await waitFor((): void => {
      expect(
        screen.queryByText("Test | 050. Guessed weak credentials"),
      ).toBeInTheDocument();
    });
    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.delete.btn.text"),
      ).not.toBeDisabled();
    });

    await waitFor((): void => {
      expect(
        screen.queryByText("group.findings.buttons.help.button"),
      ).toBeInTheDocument();
    });

    await userEvent.click(screen.getByText("searchFindings.delete.btn.text"));

    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.delete.title"),
      ).toBeInTheDocument();
    });
    await waitFor((): void => {
      expect(
        screen.getByRole("combobox", { name: "justification" }),
      ).toBeInTheDocument();
    });

    await userEvent.click(
      screen.getByText("searchFindings.delete.justif.label"),
    );
    await userEvent.click(
      screen.getByText("searchFindings.delete.justif.duplicated"),
    );

    await userEvent.click(screen.getByText(btnConfirm));

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledTimes(1);
    });
  });

  it("should handle deletion errors", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const deleteMutationMock = [
      graphqlMocked.mutation(
        REMOVE_FINDING_MUTATION,
        ({
          variables,
        }): StrictResponse<IErrorMessage | { data: RemoveFinding }> => {
          const { findingId, justification } = variables;
          if (
            findingId === "438679960" &&
            justification === RemoveFindingJustification.Duplicated
          ) {
            return HttpResponse.json({
              errors: [new GraphQLError("Unexpected error")],
            });
          }

          return HttpResponse.json({
            data: {
              removeFinding: {
                success: true,
              },
            },
          });
        },
      ),
    ];

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_remove_finding_mutate" },
      { action: "integrates_api_resolvers_finding_hacker_resolve" },
      { action: "see_expert_button" },
    ]);

    const contextEnv: IAuthContext = {
      awsSubscription: null,
      tours: {
        newGroup: false,
        newRoot: false,
        welcome: false,
      },
      userEmail: "jdoe@fluidattacks.com",
      userName: "Jhon Doe",
    };
    render(
      <authContext.Provider value={contextEnv}>
        <Wrapper mockedPermissions={mockedPermissions}>
          <authzGroupContext.Provider
            value={
              new PureAbility([
                { action: "can_report_vulnerabilities" },
                { action: "integrates_api_resolvers_finding_hacker_resolve" },
                { action: "see_expert_button" },
              ])
            }
          >
            <Routes>
              <Route
                element={<Finding />}
                path={
                  "/orgs/:organizationName/groups/:groupName/vulns/:findingId/*"
                }
              />
            </Routes>
          </authzGroupContext.Provider>
        </Wrapper>
      </authContext.Provider>,
      { memoryRouter, mocks: [...mockedVulnsQueries, ...deleteMutationMock] },
    );
    await waitFor((): void => {
      expect(
        screen.queryByText("Test | 050. Guessed weak credentials"),
      ).toBeInTheDocument();
    });
    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.delete.btn.text"),
      ).not.toBeDisabled();
    });

    await waitFor((): void => {
      expect(
        screen.queryByText("group.findings.buttons.help.button"),
      ).toBeInTheDocument();
    });

    await userEvent.click(screen.getByText("searchFindings.delete.btn.text"));

    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.delete.title"),
      ).toBeInTheDocument();
    });

    await userEvent.click(
      screen.getByText("searchFindings.delete.justif.label"),
    );
    await userEvent.click(
      screen.getByText("searchFindings.delete.justif.duplicated"),
    );

    await waitFor((): void => {
      expect(screen.queryByText(btnConfirm)).toBeInTheDocument();
    });

    await userEvent.click(screen.getByText(btnConfirm));

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledTimes(2);
    });
  });
});
