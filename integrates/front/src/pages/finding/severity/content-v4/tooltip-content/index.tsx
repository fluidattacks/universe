import { CloudImage, Container, Text } from "@fluidattacks/design";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

import { severityImages } from "../../utils";

interface ITooltipContent {
  metricOptions: Record<string, string>;
  metricValue: string;
  bgColor: Record<string, string>;
}

const TooltipContent = ({
  bgColor,
  metricOptions,
  metricValue,
}: Readonly<ITooltipContent>): JSX.Element => {
  const theme = useTheme();
  const { t } = useTranslation();

  const ind = 3;
  const translationKey = metricOptions[metricValue]
    .split(".")
    .slice(0, ind)
    .join(".");

  return (
    <Container>
      <Text
        color={theme.palette.white}
        fontWeight={"bold"}
        mb={0.25}
        size={"sm"}
        textAlign={"center"}
      >
        {t(`${translationKey}.label`)}
      </Text>
      <Text color={theme.palette.white} mb={1} size={"sm"} textAlign={"center"}>
        {t(`${translationKey}.tooltip`)}
      </Text>
      <Container>
        {Object.keys(metricOptions).map((key): JSX.Element => {
          const metricTip = metricOptions[key];
          const isSelected = metricOptions[key] === metricOptions[metricValue];

          const metricImage = severityImages[
            metricOptions[key].split(".")[2] +
              t(metricOptions[key]).split(" ")[0]
          ].replace(/^\/v\d+\//u, "");

          return (
            <Container
              display={"grid"}
              gap={0.25}
              key={key}
              mb={0.25}
              style={{
                gridTemplateColumns: "30% 70%",
              }}
            >
              <Container
                alignItems={"center"}
                bgColor={bgColor[key]}
                display={"flex"}
                height={"100%"}
                justify={"center"}
              >
                <CloudImage
                  alt={"severityTooltipImg"}
                  publicId={metricImage}
                  width={"50px"}
                />
              </Container>
              <Container
                bgColor={isSelected ? bgColor[key] : undefined}
                pb={0.5}
                pl={0.5}
                pr={0.5}
                pt={0.5}
              >
                <Text
                  color={isSelected ? theme.palette.black : theme.palette.white}
                  fontWeight={"bold"}
                  mb={0.25}
                  size={"sm"}
                >
                  {t(metricTip)}
                </Text>
                <Text
                  color={isSelected ? theme.palette.black : theme.palette.white}
                  size={"xs"}
                >
                  {t(metricTip.replace(/label/u, "tooltip"))}
                </Text>
              </Container>
            </Container>
          );
        })}
      </Container>
    </Container>
  );
};

export { TooltipContent };
