import { screen } from "@testing-library/react";

import { SeverityTile } from ".";
import { userInteractionBgColors } from "../cvss4/utils";
import { CustomThemeProvider } from "components/colors";
import { render } from "mocks";
import { userInteractionValues } from "utils/cvss4";

describe("severityTile", (): void => {
  it("should render a tile", (): void => {
    expect.hasAssertions();

    render(
      <CustomThemeProvider>
        <SeverityTile
          color={userInteractionBgColors.N}
          metricOptions={userInteractionValues}
          metricValue={"N"}
          name={"userInteraction"}
        />
      </CustomThemeProvider>,
    );

    expect(screen.getByText(userInteractionValues.N)).toBeInTheDocument();
    expect(
      screen.getByText("searchFindings.tabSeverity.userInteraction.label"),
    ).toBeInTheDocument();
    expect(
      screen.getByText(
        "searchFindings.tabSeverity.userInteraction.options.none.label",
      ),
    ).toBeInTheDocument();
  });
});
