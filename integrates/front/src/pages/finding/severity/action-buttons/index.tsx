import { Button, Container } from "@fluidattacks/design";
import type { IconName } from "@fortawesome/free-solid-svg-icons";
import { useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { Authorize } from "components/@core/authorize";
import { Can } from "context/authz/can";

interface IActionButtonsProps {
  readonly isEditing: boolean;
  readonly dirty: boolean;
  readonly mutationLoading: boolean;
  readonly resetForm: () => void;
  readonly setEditing: React.Dispatch<React.SetStateAction<boolean>>;
}

const ActionButtons: React.FC<IActionButtonsProps> = ({
  isEditing,
  dirty,
  mutationLoading,
  resetForm,
  setEditing,
}: IActionButtonsProps): JSX.Element => {
  const { t } = useTranslation();
  const handleEditClick = useCallback((): void => {
    if (dirty) {
      resetForm();
    }
    setEditing(!isEditing);
  }, [dirty, isEditing, resetForm, setEditing]);

  const buttonAttributes = (): {
    text: string;
    icon: IconName;
    tip: string | undefined;
  } => {
    if (isEditing) {
      return {
        icon: "times",
        text: t("searchFindings.tabSeverity.editable.cancel"),
        tip: undefined,
      };
    }

    return {
      icon: "pen",
      text: t("searchFindings.tabSeverity.editable.label"),
      tip: t("searchFindings.tabSeverity.editable.tooltip"),
    };
  };

  const { icon, text, tip } = buttonAttributes();

  return (
    <Can do={"integrates_api_mutations_update_severity_mutate"}>
      <Container display={"flex"} justify={"end"}>
        <Authorize extraCondition={isEditing}>
          <Button
            disabled={!dirty || mutationLoading}
            icon={"rotate-right"}
            mr={0.5}
            tooltip={t("searchFindings.tabSeverity.update.tooltip")}
            type={"submit"}
            variant={"primary"}
          >
            {t("searchFindings.tabSeverity.update.text")}
          </Button>
        </Authorize>
        <Button
          icon={icon}
          onClick={handleEditClick}
          tooltip={tip}
          variant={"secondary"}
        >
          {text}
        </Button>
      </Container>
    </Can>
  );
};

export type { IActionButtonsProps };
export { ActionButtons };
