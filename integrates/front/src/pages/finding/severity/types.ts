interface ISeverityField {
  currentValue: string;
  name: string;
  options: Record<string, string>;
  required?: boolean;
  title: string;
}

export type { ISeverityField };
