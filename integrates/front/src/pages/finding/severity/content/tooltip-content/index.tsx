import { CloudImage, Container, Text } from "@fluidattacks/design";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

import { severityImagesRoot, version } from "../../utils";

interface ITooltipContent {
  metricOptions: Record<string, string>;
  metricValue: string;
  bgColor: Record<string, string>;
}

const severityImages: Record<string, string> = {
  attackComplexityHigh: `${version[0]}${severityImagesRoot}attackComplexityHigh.webp`,
  attackComplexityLow: `${version[0]}${severityImagesRoot}attackComplexityLow.webp`,
  attackVectorAdjacent: `${version[0]}${severityImagesRoot}attackVectorAdjacent.webp`,
  attackVectorLocal: `${version[0]}${severityImagesRoot}attackVectorLocal.webp`,
  attackVectorNetwork: `${version[0]}${severityImagesRoot}attackVectorNetwork.webp`,
  attackVectorPhysical: `${version[0]}${severityImagesRoot}attackVectorPhysical.webp`,
  availabilityImpactHigh: `${version[1]}${severityImagesRoot}availabilityImpactHigh.webp`,
  availabilityImpactLow: `${version[1]}${severityImagesRoot}availabilityImpactLow.webp`,
  availabilityImpactNone: `${version[1]}${severityImagesRoot}availabilityImpactNone.webp`,
  confidentialityImpactHigh: `${version[1]}${severityImagesRoot}confidentialityImpactHigh.webp`,
  confidentialityImpactLow: `${version[2]}${severityImagesRoot}confidentialityImpactLow.webp`,
  confidentialityImpactNone: `${version[2]}${severityImagesRoot}confidentialityImpactNone.webp`,
  exploitabilityFunctional: `${version[3]}${severityImagesRoot}exploitabilityFunctional.webp`,
  exploitabilityHigh: `${version[3]}${severityImagesRoot}exploitabilityHigh.webp`,
  exploitabilityNot: `${version[3]}${severityImagesRoot}exploitabilityNot.webp`,
  exploitabilityProof: `${version[3]}${severityImagesRoot}exploitabilityProof.webp`,
  exploitabilityUnproven: `${version[3]}${severityImagesRoot}exploitabilityUnproven.webp`,
  integrityImpactHigh: `${version[4]}${severityImagesRoot}integrityImpactHigh.webp`,
  integrityImpactLow: `${version[5]}${severityImagesRoot}integrityImpactLow.webp`,
  integrityImpactNone: `${version[5]}${severityImagesRoot}integrityImpactNone.webp`,
  privilegesRequiredHigh: `${version[6]}${severityImagesRoot}privilegesRequiredHigh.webp`,
  privilegesRequiredLow: `${version[7]}${severityImagesRoot}privilegesRequiredLow.webp`,
  privilegesRequiredNone: `${version[8]}${severityImagesRoot}privilegesRequiredNone.webp`,
  remediationLevelNot: `${version[8]}${severityImagesRoot}remediationLevelNot.webp`,
  remediationLevelOfficial: `${version[8]}${severityImagesRoot}remediationLevelOfficial.webp`,
  remediationLevelTemporary: `${version[8]}${severityImagesRoot}remediationLevelTemporary.webp`,
  remediationLevelUnavailable: `${version[8]}${severityImagesRoot}remediationLevelUnavailable.webp`,
  remediationLevelWorkaround: `${version[8]}${severityImagesRoot}remediationLevelWorkaround.webp`,
  reportConfidenceConfirmed: `${version[9]}${severityImagesRoot}reportConfidenceConfirmed.webp`,
  reportConfidenceNot: `${version[9]}${severityImagesRoot}reportConfidenceNot.webp`,
  reportConfidenceReasonable: `${version[9]}${severityImagesRoot}reportConfidenceReasonable.webp`,
  reportConfidenceUnknown: `${version[9]}${severityImagesRoot}reportConfidenceUnknown.webp`,
  severityScopeChanged: `${version[10]}${severityImagesRoot}severityScopeChanged.webp`,
  severityScopeUnchanged: `${version[10]}${severityImagesRoot}severityScopeUnchanged.webp`,
  userInteractionNone: `${version[10]}${severityImagesRoot}userInteractionNone.webp`,
  userInteractionRequired: `${version[11]}${severityImagesRoot}userInteractionRequired.webp`,
};

const TooltipContent = ({
  bgColor,
  metricOptions,
  metricValue,
}: Readonly<ITooltipContent>): JSX.Element => {
  const theme = useTheme();
  const { t } = useTranslation();

  const ind = 3;
  const translationKey = metricOptions[metricValue]
    .split(".")
    .slice(0, ind)
    .join(".");

  return (
    <Container>
      <Text
        color={theme.palette.white}
        fontWeight={"bold"}
        mb={0.25}
        size={"sm"}
        textAlign={"center"}
      >
        {t(`${translationKey}.label`)}
      </Text>
      <Text color={theme.palette.white} mb={1} size={"sm"} textAlign={"center"}>
        {t(`${translationKey}.tooltip`)}
      </Text>
      <Container>
        {Object.keys(metricOptions).map((key): JSX.Element => {
          const metricTip = metricOptions[key];
          const isSelected = metricOptions[key] === metricOptions[metricValue];

          const metricImage = severityImages[
            metricOptions[key].split(".")[2] +
              t(metricOptions[key]).split(" ")[0]
          ].replace(/^\/v\d+\//u, "");

          return (
            <Container
              display={"grid"}
              gap={0.25}
              key={key}
              mb={0.25}
              style={{
                gridTemplateColumns: "30% 70%",
              }}
            >
              <Container
                alignItems={"center"}
                bgColor={bgColor[key]}
                display={"flex"}
                height={"100%"}
                justify={"center"}
              >
                <CloudImage
                  alt={"severityTooltipImg"}
                  publicId={metricImage}
                  width={"50px"}
                />
              </Container>
              <Container
                bgColor={isSelected ? bgColor[key] : undefined}
                pb={0.5}
                pl={0.5}
                pr={0.5}
                pt={0.5}
              >
                <Text
                  color={isSelected ? theme.palette.black : theme.palette.white}
                  fontWeight={"bold"}
                  mb={0.25}
                  size={"sm"}
                >
                  {t(metricTip)}
                </Text>
                <Text
                  color={isSelected ? theme.palette.black : theme.palette.white}
                  size={"xs"}
                >
                  {t(metricTip.replace(/label/u, "tooltip"))}
                </Text>
              </Container>
            </Container>
          );
        })}
      </Container>
    </Container>
  );
};

export { TooltipContent, severityImages };
