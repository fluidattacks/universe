import { GridContainer } from "@fluidattacks/design";
import * as React from "react";
import { styled } from "styled-components";

import {
  attackComplexityBgColors,
  attackVectorBgColors,
  availabilityVulnerableImpactBgColors,
  confidentialityVulnerableImpactBgColors,
  exploitabilityBgColors,
  integrityVulnerableImpactBgColors,
  privilegesRequiredBgColors,
  remediationLevelBgColors,
  reportConfidenceBgColors,
  severityScopeBgColors,
  userInteractionBgColors,
} from "../cvss4/utils";
import { SeverityTile } from "../tile";
import {
  attackComplexityValues,
  attackVectorValues,
  availabilityImpactValues,
  confidentialityImpactValues,
  exploitabilityValues,
  integrityImpactValues,
  privilegesRequiredValues,
  remediationLevelValues,
  reportConfidenceValues,
  severityScopeValues,
  userInteractionValues,
} from "utils/cvss";
import type { ICVSS3TemporalMetrics } from "utils/cvss";

const FlexCol = styled.div.attrs({
  className: "flex flex-column items-center",
})``;

export const SeverityContent: React.FC<ICVSS3TemporalMetrics> = ({
  attackComplexity,
  attackVector,
  availabilityImpact,
  confidentialityImpact,
  exploitability,
  integrityImpact,
  privilegesRequired,
  remediationLevel,
  reportConfidence,
  severityScope,
  userInteraction,
}): JSX.Element => {
  return (
    <GridContainer lg={4} md={4} sm={3} xl={4}>
      <FlexCol>
        <SeverityTile
          color={userInteractionBgColors[userInteraction]}
          metricOptions={userInteractionValues}
          metricValue={userInteraction}
          name={"userInteraction"}
        />
        <SeverityTile
          color={privilegesRequiredBgColors[privilegesRequired]}
          metricOptions={privilegesRequiredValues}
          metricValue={privilegesRequired}
          name={"privilegesRequired"}
        />
      </FlexCol>
      <FlexCol>
        <SeverityTile
          color={attackVectorBgColors[attackVector]}
          metricOptions={attackVectorValues}
          metricValue={attackVector}
          name={"attackVector"}
        />
        <SeverityTile
          color={attackComplexityBgColors[attackComplexity]}
          metricOptions={attackComplexityValues}
          metricValue={attackComplexity}
          name={"attackComplexity"}
        />
        <SeverityTile
          color={exploitabilityBgColors[exploitability]}
          metricOptions={exploitabilityValues}
          metricValue={exploitability}
          name={"exploitability"}
        />
        <SeverityTile
          color={severityScopeBgColors[severityScope]}
          metricOptions={severityScopeValues}
          metricValue={severityScope}
          name={"severityScope"}
        />
      </FlexCol>
      <FlexCol>
        <SeverityTile
          color={availabilityVulnerableImpactBgColors[availabilityImpact]}
          metricOptions={availabilityImpactValues}
          metricValue={availabilityImpact}
          name={"availabilityImpact"}
        />
        <SeverityTile
          color={confidentialityVulnerableImpactBgColors[confidentialityImpact]}
          metricOptions={confidentialityImpactValues}
          metricValue={confidentialityImpact}
          name={"confidentialityImpact"}
        />
        <SeverityTile
          color={integrityVulnerableImpactBgColors[integrityImpact]}
          metricOptions={integrityImpactValues}
          metricValue={integrityImpact}
          name={"integrityImpact"}
        />
      </FlexCol>
      <FlexCol>
        <SeverityTile
          color={remediationLevelBgColors[remediationLevel]}
          metricOptions={remediationLevelValues}
          metricValue={remediationLevel}
          name={"remediationLevel"}
        />
        <SeverityTile
          color={reportConfidenceBgColors[reportConfidence]}
          metricOptions={reportConfidenceValues}
          metricValue={reportConfidence}
          name={"reportConfidence"}
        />
      </FlexCol>
    </GridContainer>
  );
};
