import { useMutation, useQuery } from "@apollo/client";
import { Form, Formik } from "formik";
import _, { keys } from "lodash";
import mixpanel from "mixpanel-browser";
import { useCallback, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";

import { ActionButtons } from "./action-buttons";
import { validationSchema } from "./validations";

import { SeverityForm } from "features/vulnerabilities/severity-form";
import { useAudit } from "hooks/use-audit";
import { GET_FINDING_HEADER } from "pages/finding/queries";
import { SeverityContent } from "pages/finding/severity/content";
import { SeverityContent as SeverityContentV4 } from "pages/finding/severity/content-v4";
import {
  GET_FINDING_SEVERITY,
  UPDATE_FINDING_SEVERITY_MUTATION,
} from "pages/finding/severity/queries";
import { getCVSS31Values, getCVSS31VectorString } from "utils/cvss";
import type { ICVSS3EnvironmentalMetrics } from "utils/cvss";
import type { ICVSS4EnvironmentalMetrics } from "utils/cvss4";
import { getCVSS40Values, getCVSS4VectorString } from "utils/cvss4";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

const FindingSeverity: React.FC<Readonly<{ cvssVersion: string }>> = ({
  cvssVersion,
}): JSX.Element => {
  const { addAuditEvent } = useAudit();
  const { t } = useTranslation();
  const { findingId } = useParams() as { findingId: string };
  const [isEditing, setIsEditing] = useState(false);

  const { data, refetch } = useQuery(GET_FINDING_SEVERITY, {
    onCompleted: (): void => {
      addAuditEvent("Finding.Severity", findingId);
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.warning("An error occurred loading finding severity", error);
      });
    },
    variables: { identifier: findingId },
  });

  const [updateSeverity, mutationRes] = useMutation(
    UPDATE_FINDING_SEVERITY_MUTATION,
    {
      onCompleted: (mtResult): void => {
        if (!_.isUndefined(mtResult)) {
          if (mtResult.updateSeverity.success) {
            void refetch();
            msgSuccess(t("groupAlerts.updated"), t("groupAlerts.updatedTitle"));
            mixpanel.track("UpdateSeverity");
          }
        }
      },
      onError: ({ graphQLErrors }): void => {
        graphQLErrors.forEach((error): void => {
          switch (error.message) {
            case "Exception - Severity score is invalid":
              msgError(t("validations.addFindingModal.invalidSeverityScore"));
              break;
            case "Exception - Invalid characters":
              msgError(t("validations.invalidChar"));
              break;
            default:
              msgError(t("groupAlerts.errorTextsad"));
              Logger.warning("An error occurred updating severity", error);
          }
        });
      },
      refetchQueries: [
        {
          query: GET_FINDING_HEADER,
          variables: {
            findingId,
          },
        },
      ],
    },
  );

  const handleUpdateSeverity = useCallback(
    async (values: ICVSS3EnvironmentalMetrics): Promise<void> => {
      setIsEditing(false);
      await updateSeverity({
        variables: {
          cvss4Vector: getCVSS4VectorString(
            Object.fromEntries(
              Object.entries(values)
                .filter(([key]): boolean => key.startsWith("scoreV4"))
                .map(([key, value]): [string, unknown] => [
                  key.replace("scoreV4", ""),
                  value,
                ]),
            ) as unknown as ICVSS4EnvironmentalMetrics,
          ),

          cvssVector: getCVSS31VectorString(values),
          findingId,
        },
      });
    },
    [findingId, updateSeverity],
  );

  if (_.isUndefined(data) || _.isEmpty(data)) {
    return <div />;
  }
  const mutationLoading = mutationRes.loading;
  const cvss31Values = getCVSS31Values(data.finding.severityVector);
  const cvss40Values = getCVSS40Values(data.finding.severityVectorV4);
  const initialValues = {
    ...cvss31Values,
    ...Object.fromEntries(
      Object.entries(cvss40Values).map(([key, value]): [string, unknown] => [
        `scoreV4${key}`,
        value,
      ]),
    ),
  };

  return (
    <React.StrictMode>
      <React.Fragment>
        <Formik
          enableReinitialize={true}
          initialValues={initialValues}
          name={"editSeverity"}
          onSubmit={handleUpdateSeverity}
          validationSchema={validationSchema(keys(initialValues))}
        >
          {({ dirty, resetForm }): React.ReactNode => (
            <Form id={"editSeverity"}>
              <ActionButtons
                dirty={dirty}
                isEditing={isEditing}
                mutationLoading={mutationLoading}
                resetForm={resetForm}
                setEditing={setIsEditing}
              />
              {isEditing ? <SeverityForm data={data} /> : null}
            </Form>
          )}
        </Formik>
        {!isEditing &&
          (cvssVersion === "severityThreatScore" ? (
            <SeverityContentV4 {...cvss40Values} />
          ) : (
            <SeverityContent {...cvss31Values} />
          ))}
      </React.Fragment>
    </React.StrictMode>
  );
};

export { FindingSeverity };
