import type { AnyObject, InferType, ObjectSchema, Schema } from "yup";
import { object, string } from "yup";

import { translate } from "utils/translations/translate";

const validationSchema = (fields: string[]): ObjectSchema<InferType<Schema>> =>
  object().shape(
    fields.reduce(
      (acc, key): AnyObject => ({
        ...acc,
        [key]: string().required(translate.t("validations.required")),
      }),
      {},
    ),
  );

export { validationSchema };
