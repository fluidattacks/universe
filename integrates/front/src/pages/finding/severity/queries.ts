import { graphql } from "gql";

const GET_FINDING_SEVERITY = graphql(`
  query GetFindingSeverityQuery($identifier: String!) {
    finding(identifier: $identifier) {
      id

      severityVector
      severityVectorV4
    }
  }
`);

const UPDATE_FINDING_SEVERITY_MUTATION = graphql(`
  mutation UpdateFindingSeverityMutation(
    $cvss4Vector: String!
    $cvssVector: String
    $findingId: String!
  ) {
    updateSeverity(
      cvss4Vector: $cvss4Vector
      cvssVector: $cvssVector
      findingId: $findingId
    ) {
      success
    }
  }
`);

export { GET_FINDING_SEVERITY, UPDATE_FINDING_SEVERITY_MUTATION };
