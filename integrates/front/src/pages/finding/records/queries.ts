import { graphql } from "gql";

export const GET_FINDING_RECORDS = graphql(`
  query GetFindingRecords($findingId: String!) {
    finding(identifier: $findingId) {
      id
      records
    }
  }
`);
