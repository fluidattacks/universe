import { Button, ButtonToolbarRow } from "@fluidattacks/design";
import { useTranslation } from "react-i18next";

interface IActionButtonsProps {
  isEditing: boolean;
  handleEditClick: () => void;
}

const ActionButtons = ({
  isEditing,
  handleEditClick,
}: Readonly<IActionButtonsProps>): JSX.Element => {
  const { t } = useTranslation();

  return (
    <ButtonToolbarRow>
      <Button
        icon={isEditing ? "close" : "edit"}
        id={"editRecords"}
        onClick={handleEditClick}
        tooltip={
          isEditing
            ? t("searchFindings.tabRecords.cancelTooltip")
            : t("searchFindings.tabRecords.editableTooltip")
        }
        variant={"secondary"}
      >
        {isEditing ? t("buttons.cancel") : t("buttons.edit")}
      </Button>
    </ButtonToolbarRow>
  );
};

export { ActionButtons };
