import { useMutation } from "@apollo/client";
import { useAbility } from "@casl/react";
import {
  Button,
  EmptyState,
  Form,
  InnerForm,
  InputFile,
  Row,
} from "@fluidattacks/design";
import type { ColumnDef } from "@tanstack/react-table";
import mixpanel from "mixpanel-browser";
import { useCallback, useState } from "react";
import React from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";

import { ActionButtons } from "./action-buttons";
import { useRecordsQuery } from "./hooks";
import { validationSchema } from "./validations";

import { Table } from "components/table";
import { Can } from "context/authz/can";
import { authzPermissionsContext } from "context/authz/config";
import { EvidenceType } from "gql/graphql";
import { useTable } from "hooks";
import {
  REMOVE_EVIDENCE_MUTATION,
  UPDATE_EVIDENCE_MUTATION,
} from "pages/finding/evidence/queries";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";

const FindingRecords: React.FC = (): JSX.Element => {
  const { t } = useTranslation();

  const permissions = useAbility(authzPermissionsContext);

  const tableRef = useTable("tblRecords");
  const { findingId, groupName, organizationName } = useParams() as {
    findingId: string;
    groupName: string;
    organizationName: string;
  };

  const [isEditing, setIsEditing] = useState(false);
  const handleEditClick = useCallback((): void => {
    setIsEditing(!isEditing);
  }, [isEditing]);

  const { records, refetch } = useRecordsQuery(findingId);

  const [updateRecords, updateRes] = useMutation(UPDATE_EVIDENCE_MUTATION, {
    onCompleted: (): void => {
      void refetch();
    },
    onError: (updateError): void => {
      updateError.graphQLErrors.forEach(({ message }): void => {
        switch (message) {
          case "Exception - Invalid file name: Format organizationName-groupName-10 alphanumeric chars.extension":
            msgError(t("group.events.form.wrongImageName"));
            break;
          case "Exception - Wrong file structure":
            msgError(t("groupAlerts.invalidStructure"));
            break;
          case "Exception - Invalid file size":
            msgError(t("validations.fileSize", { count: 1 }));
            break;
          case "Exception - Invalid file type":
            msgError(t("groupAlerts.fileTypeCsv"));
            break;
          default:
            msgError(t("groupAlerts.errorTextsad"));
            Logger.warning("An error occurred updating records", updateError);
        }
      });
    },
  });

  const handleSubmit = useCallback(
    (values: Record<string, FileList>): void => {
      setIsEditing(false);
      void updateRecords({
        variables: {
          evidenceId: EvidenceType.Records,
          file: values.filename[0],
          findingId,
        },
      });
    },
    [findingId, updateRecords],
  );

  const [removeRecords, removeRes] = useMutation(REMOVE_EVIDENCE_MUTATION, {
    onCompleted: (): void => {
      void refetch();
    },
    onError: (removeError): void => {
      msgError(t("groupAlerts.errorTextsad"));
      Logger.warning("An error occurred removing records", removeError);
    },
  });

  const handleRemoveClick = useCallback((): void => {
    mixpanel.track("RemoveRecords");
    setIsEditing(false);
    void removeRecords({
      variables: { evidenceId: EvidenceType.Records, findingId },
    });
  }, [findingId, removeRecords]);

  if (records === undefined) {
    return <div />;
  }

  const columns = Object.keys(records[0] ?? {}).map(
    (key): ColumnDef<Record<string, string>> => {
      return {
        accessorFn: (originalRow): string => originalRow[key],
        header: key,
      };
    },
  );

  const canAddRecords = permissions.can(
    "integrates_api_mutations_update_evidence_mutate",
  );

  if (records.length === 0 && !isEditing) {
    return (
      <EmptyState
        confirmButton={
          canAddRecords
            ? {
                onClick: handleEditClick,
                text: t("group.findings.records.add"),
              }
            : undefined
        }
        description={t("group.findings.records.empty.description")}
        padding={0}
        title={t("group.findings.records.empty.title")}
      />
    );
  }

  return (
    <React.StrictMode>
      <React.Fragment>
        <Can do={"integrates_api_mutations_update_evidence_mutate"}>
          <ActionButtons
            handleEditClick={handleEditClick}
            isEditing={isEditing}
          />
        </Can>
        {isEditing ? (
          <Form
            confirmButton={{
              disabled: updateRes.loading,
              icon: "save",
              label: t("buttons.update"),
            }}
            defaultValues={{ filename: undefined }}
            id={"records"}
            maxButtonWidth={"95px"}
            onSubmit={handleSubmit}
            yupSchema={validationSchema(groupName, organizationName)}
          >
            <InnerForm>
              {({
                formState: { errors },
                register,
                setValue,
                watch,
              }): JSX.Element => (
                <InputFile
                  accept={".csv"}
                  error={errors.filename?.message?.toString()}
                  id={"recordsFile"}
                  name={"filename"}
                  register={register}
                  setValue={setValue}
                  watch={watch}
                />
              )}
            </InnerForm>
          </Form>
        ) : undefined}
        <Row justify={"center"}>
          {columns.length === 0 ? undefined : (
            <Table
              columns={columns}
              data={records}
              data-private={true}
              tableRef={tableRef}
            />
          )}
        </Row>
        {isEditing && records.length > 0 ? (
          <Button
            disabled={removeRes.loading}
            icon={"trash"}
            id={"removeRecords"}
            my={1.25}
            onClick={handleRemoveClick}
            tooltip={t("searchFindings.tabRecords.remove")}
            variant={"secondary"}
          >
            {t("buttons.delete")}
          </Button>
        ) : undefined}
      </React.Fragment>
    </React.StrictMode>
  );
};

export { FindingRecords };
