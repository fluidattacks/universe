import type { ApolloQueryResult } from "@apollo/client";
import { useQuery } from "@apollo/client";
import { useTranslation } from "react-i18next";

import { GET_FINDING_RECORDS } from "./queries";

import type { GetFindingRecordsQuery } from "gql/graphql";
import { useAudit } from "hooks/use-audit";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";

interface IUseRecordsQuery {
  records: Record<string, string>[] | undefined;
  refetch: () => Promise<ApolloQueryResult<GetFindingRecordsQuery>>;
}

const useRecordsQuery = (findingId: string): IUseRecordsQuery => {
  const { addAuditEvent } = useAudit();
  const { t } = useTranslation();

  const { data, refetch } = useQuery(GET_FINDING_RECORDS, {
    onCompleted: (): void => {
      addAuditEvent("Finding.Records", findingId);
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.error("An error occurred loading finding records", error);
      });
    },
    variables: { findingId },
  });

  const records =
    data === undefined ? undefined : JSON.parse(data.finding.records ?? "[]");

  return { records, refetch };
};

export { useRecordsQuery };
