import type { InferType, ObjectSchema, Schema } from "yup";
import { mixed, object } from "yup";

import { translate } from "utils/translations/translate";

const validationSchema = (
  groupName: string,
  organizationName: string,
): ObjectSchema<InferType<Schema>> =>
  object().shape({
    filename: mixed()
      .required(translate.t("validations.required"))
      .isValidFileType(["csv"], translate.t("groupAlerts.fileTypeCsv"))
      .isValidFileName(groupName, organizationName),
  });

export { validationSchema };
