import { graphql } from "gql";

graphql(`
  fragment vulnFields on Vulnerability {
    id
    closingDate
    customSeverity
    externalBugTrackingSystem
    findingId
    groupName
    hacker @include(if: $canRetrieveHacker)
    lastEditedBy
    lastStateDate
    lastTreatmentDate
    lastVerificationDate
    priority
    remediated
    reportDate
    rootNickname
    severityTemporalScore
    severityThreatScore

    severityVector
    severityVectorV4

    source
    specific
    state
    stateReasons
    stream
    tag
    technique
    treatmentAcceptanceDate
    treatmentAcceptanceStatus
    treatmentAssigned
    treatmentJustification
    treatmentStatus
    treatmentUser
    verification
    verificationJustification
    vulnerabilityType
    where
    zeroRisk
    advisories {
      cve
      epss
      package
      vulnerableVersion
    }
    root {
      __typename
      ... on GitRoot {
        branch
        criticality
      }
    }
  }
`);

const GET_FINDING_INFO = graphql(`
  query GetFindingInfo($findingId: String!) {
    finding(identifier: $findingId) {
      id
      assignees
      locations
      releaseDate
      remediated
      status
      verified
    }
  }
`);

const GET_FINDING_NZR_VULNS = graphql(`
  fragment GetFindingNzrVulns on Query {
    finding(identifier: $findingId) {
      id
      __typename
      vulnerabilitiesConnection(
        after: $afterNZR
        first: $first
        state: $state
        treatment: $treatment
        verification: $verification
        where: $where
      ) {
        edges {
          node {
            ...vulnFields
          }
        }
        pageInfo {
          endCursor
          hasNextPage
        }
      }
    }
  }
`);

const GET_FINDING_VULN_DRAFTS = graphql(`
  fragment GetFindingVulnDrafts on Query {
    finding(identifier: $findingId) {
      id
      __typename
      draftsConnection(
        after: $afterDrafts
        first: $first
        state: $state
        where: $where
      ) @include(if: $canRetrieveDrafts) {
        edges {
          node {
            ...vulnFields
          }
        }
        pageInfo {
          endCursor
          hasNextPage
        }
      }
    }
  }
`);

const GET_FINDING_ZR_VULNS = graphql(`
  fragment GetFindingZrVulns on Query {
    finding(identifier: $findingId) {
      id
      __typename
      zeroRiskConnection(after: $afterZR, first: $first)
        @include(if: $canRetrieveZeroRisk) {
        edges {
          node {
            ...vulnFields
          }
        }
        pageInfo {
          endCursor
          hasNextPage
        }
      }
    }
  }
`);

const RESUBMIT_VULNERABILITIES = graphql(`
  mutation ResubmitVulnerabilitiesRequest(
    $findingId: String!
    $vulnerabilities: [String!]!
  ) {
    resubmitVulnerabilities(
      findingId: $findingId
      vulnerabilities: $vulnerabilities
    ) {
      success
    }
  }
`);

const CLOSE_VULNERABILITIES = graphql(`
  mutation CloseVulnerabilities(
    $findingId: String!
    $justification: VulnerabilityStateReason
    $vulnerabilities: [String!]!
  ) {
    closeVulnerabilities(
      findingId: $findingId
      justification: $justification
      vulnerabilities: $vulnerabilities
    ) {
      success
    }
  }
`);

const SEND_VULNERABILITY_NOTIFICATION = graphql(`
  mutation SendVulnerabilityNotification($findingId: String!) {
    sendVulnerabilityNotification(findingId: $findingId) {
      success
    }
  }
`);

const GET_GROUP_FINDINGS_PRIORITY = graphql(`
  query GetGroupFindingsPriority($groupName: String!) {
    group(groupName: $groupName) {
      name
      totalOpenPriority
    }
  }
`);

const GET_FINDING_VULNS_QUERIES = graphql(`
  query GetFindingVulnsQueries(
    $afterDrafts: String
    $afterNZR: String
    $afterZR: String
    $canRetrieveDrafts: Boolean!
    $canRetrieveHacker: Boolean! = false
    $canRetrieveZeroRisk: Boolean!
    $findingId: String!
    $first: Int
    $state: VulnerabilityState
    $treatment: VulnerabilityTreatment
    $verification: VulnerabilityVerification
    $where: String
  ) {
    ...GetFindingNzrVulns
    ...GetFindingVulnDrafts
    ...GetFindingZrVulns
  }
`);

const GET_VULNS = graphql(`
  query GetVulns(
    $after: String
    $canRetrieveHacker: Boolean! = false
    $filters: VulnerabilityFiltersInput
    $findingId: String!
    $first: Int!
    $sortBy: [VulnerabilitySortInput!]
  ) {
    finding(identifier: $findingId) {
      id
      vulnerabilities(
        after: $after
        filters: $filters
        first: $first
        sortBy: $sortBy
      ) {
        total
        edges {
          node {
            ...vulnFields
          }
        }
        pageInfo {
          endCursor
          hasNextPage
        }
      }
    }
  }
`);

export {
  GET_VULNS,
  GET_FINDING_INFO,
  GET_FINDING_NZR_VULNS,
  GET_FINDING_VULN_DRAFTS,
  GET_FINDING_VULNS_QUERIES,
  GET_FINDING_ZR_VULNS,
  GET_GROUP_FINDINGS_PRIORITY,
  CLOSE_VULNERABILITIES,
  RESUBMIT_VULNERABILITIES,
  SEND_VULNERABILITY_NOTIFICATION,
};
