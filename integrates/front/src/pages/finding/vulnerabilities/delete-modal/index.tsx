import { useMutation } from "@apollo/client";
import {
  ComboBox,
  Form,
  InnerForm,
  Modal,
  useModal,
} from "@fluidattacks/design";
import { useCallback } from "react";
import { useTranslation } from "react-i18next";
import { useNavigate, useParams } from "react-router-dom";
import { mixed, object } from "yup";

import type { RemoveFindingJustification } from "gql/graphql";
import { REMOVE_FINDING_MUTATION } from "pages/finding/queries";
import { GET_FINDINGS } from "pages/group/findings/queries";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

const DeleteFindingModal = ({
  isOpen,
  onClose,
}: Readonly<{ isOpen: boolean; onClose: () => void }>): JSX.Element => {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const modalProps = useModal("delete-vulnerability-modal");
  const { findingId, groupName, organizationName } = useParams() as {
    findingId: string;
    groupName: string;
    organizationName: string;
  };

  // Graphql operations
  const [removeFinding, { loading: deleting }] = useMutation(
    REMOVE_FINDING_MUTATION,
    {
      onCompleted: (result: { removeFinding: { success: boolean } }): void => {
        if (result.removeFinding.success) {
          msgSuccess(
            t("searchFindings.findingDeleted", { findingId }),
            t("searchFindings.successTitle"),
          );
          navigate(`/orgs/${organizationName}/groups/${groupName}/vulns`, {
            replace: true,
          });
        }
      },
      onError: ({ graphQLErrors }): void => {
        graphQLErrors.forEach((error): void => {
          msgError(t("groupAlerts.errorTextsad"));
          Logger.warning("An error occurred deleting finding", error);
        });
      },
      refetchQueries: [GET_FINDINGS],
    },
  );

  // Data management
  const handleDelete = useCallback(
    async (values: Record<string, unknown>): Promise<void> => {
      await removeFinding({
        variables: {
          findingId,
          justification: values.justification as RemoveFindingJustification,
        },
      });
    },
    [removeFinding, findingId],
  );

  const validationSchema = object().shape({
    justification: mixed().required(t("validations.required")),
  });

  return (
    <Modal
      id={"removeFinding"}
      modalRef={{ ...modalProps, close: onClose, isOpen }}
      size={"sm"}
      title={t("searchFindings.delete.title")}
    >
      <Form
        cancelButton={{ onClick: onClose }}
        confirmButton={{ disabled: deleting }}
        defaultValues={{ justification: "" }}
        onSubmit={handleDelete}
        yupSchema={validationSchema}
      >
        <InnerForm>
          {({ control }): JSX.Element => (
            <ComboBox
              control={control}
              items={[
                {
                  name: t("searchFindings.delete.justif.duplicated"),
                  value: "DUPLICATED",
                },
                {
                  name: t("searchFindings.delete.justif.falsePositive"),
                  value: "FALSE_POSITIVE",
                },
                {
                  name: t("searchFindings.delete.justif.notRequired"),
                  value: "NOT_REQUIRED",
                },
              ]}
              label={t("searchFindings.delete.justif.label")}
              name={"justification"}
            />
          )}
        </InnerForm>
      </Form>
    </Modal>
  );
};

export { DeleteFindingModal };
