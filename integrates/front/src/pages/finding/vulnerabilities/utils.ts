/* eslint-disable max-lines */
import type {
  ApolloError,
  InternalRefetchQueriesInclude,
  MutationHookOptions,
} from "@apollo/client";
import {
  type CellContext,
  type ColumnDef,
  type Row,
  createColumnHelper,
} from "@tanstack/react-table";
import dayjs from "dayjs";
import type { ExecutionResult } from "graphql";
import _ from "lodash";

import type { IFindingsPriorityResponse } from "./hooks/types";
import { GET_FINDING_INFO } from "./queries";
import type {
  ICloseVulnerabilitiesResultAttr,
  IResubmitVulnerabilitiesResultAttr,
} from "./types";

import { GET_FINDING_HEADER } from "../queries";
import { filterDate } from "components/table/utils";
import { statusFormatter } from "features/vulnerabilities/formatters/status";
import { techniqueFormatter } from "features/vulnerabilities/formatters/technique";
import type { IVulnDataAttr } from "features/vulnerabilities/handle-acceptance-modal/types";
import { getLastTreatment } from "features/vulnerabilities/treatment-modal/update-treatment/utils";
import type { IVulnRowAttr } from "features/vulnerabilities/types";
import type {
  Exact,
  ResubmitVulnerabilitiesRequestMutation,
  VulnerabilityStateReason,
} from "gql/graphql";
import { VulnerabilityState } from "gql/graphql";
import type { IHistoricTreatment } from "pages/finding/description/types";
import { getPriorityScore } from "utils/cvss";
import {
  formatTreatmentColumn,
  includeTagsFormatter,
  severityFormatter,
} from "utils/format-helpers";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";
import { translate } from "utils/translations/translate";

type TGroup = IFindingsPriorityResponse["data"]["group"];
interface ICVSSVersion {
  accessorKey: "severityTemporalScore" | "severityThreatScore";
  header: string;
}

const getVulnsPendingOfAcceptance = (
  vulnerabilities: IVulnRowAttr[],
  treatmentType: "ACCEPTED_UNDEFINED" | "ACCEPTED" | "ALL_ACCEPTED",
): IVulnRowAttr[] =>
  vulnerabilities.reduce(
    (pendingVulns: IVulnRowAttr[], vuln: IVulnRowAttr): IVulnRowAttr[] => {
      const lastTreatment: IHistoricTreatment = getLastTreatment(
        vuln.historicTreatment,
      );
      const setTreatmentValue =
        treatmentType === "ALL_ACCEPTED"
          ? lastTreatment.treatment === "ACCEPTED" ||
            lastTreatment.treatment === "ACCEPTED_UNDEFINED"
          : lastTreatment.treatment === treatmentType;

      return setTreatmentValue && lastTreatment.acceptanceStatus === "SUBMITTED"
        ? [...pendingVulns, { acceptance: "APPROVED", ...vuln }]
        : pendingVulns;
    },
    [],
  );

const getRequestedZeroRiskVulns = (
  vulnerabilities: IVulnRowAttr[],
): IVulnDataAttr[] =>
  vulnerabilities.reduce(
    (
      requestedZeroRiskVulns: IVulnDataAttr[],
      vuln: IVulnRowAttr,
    ): IVulnDataAttr[] => {
      return vuln.zeroRisk === "Requested"
        ? [...requestedZeroRiskVulns, { acceptance: "", ...vuln }]
        : requestedZeroRiskVulns;
    },
    [],
  );

const getRejectedVulns = (vulnerabilities: IVulnRowAttr[]): IVulnRowAttr[] =>
  vulnerabilities.reduce(
    (submittedVulns: IVulnRowAttr[], vuln: IVulnRowAttr): IVulnRowAttr[] => {
      return vuln.state === "REJECTED"
        ? [...submittedVulns, vuln]
        : submittedVulns;
    },
    [],
  );

const getVunerableLocations = (
  vulnerabilities: IVulnRowAttr[],
): IVulnRowAttr[] =>
  vulnerabilities.reduce(
    (vulnerable: IVulnRowAttr[], vuln: IVulnRowAttr): IVulnRowAttr[] => {
      return vuln.state === "VULNERABLE" ? [...vulnerable, vuln] : vulnerable;
    },
    [],
  );

const getSubmittedVulns = (vulnerabilities: IVulnRowAttr[]): IVulnRowAttr[] =>
  vulnerabilities.reduce(
    (submittedVulns: IVulnRowAttr[], vuln: IVulnRowAttr): IVulnRowAttr[] => {
      return vuln.state === "SUBMITTED"
        ? [...submittedVulns, { acceptance: "", ...vuln }]
        : submittedVulns;
    },
    [],
  );

const getSafeVulnsCount = (vulnerabilities: IVulnRowAttr[]): number =>
  vulnerabilities.reduce((count: number, vuln: IVulnRowAttr): number => {
    return vuln.state === "SAFE" ? count + 1 : count;
  }, 0);

const allVulnsWithEqualStatus = (vulnerabilities: IVulnRowAttr[]): boolean =>
  vulnerabilities.every(
    (vuln: IVulnRowAttr): boolean => vulnerabilities[0].state === vuln.state,
  );

const allVulnsWithUpdatableSeverity = (
  vulnerabilities: IVulnRowAttr[],
): boolean =>
  vulnerabilities.every((vuln: IVulnRowAttr): boolean =>
    ["REJECTED", "SUBMITTED", "VULNERABLE"].includes(vuln.state),
  );

const filterVerification = (
  row: IVulnRowAttr,
  value: string | undefined,
): boolean => {
  if (value === "" || value === undefined) return true;

  const currentVerification = String(row.verification);
  if (value === "NotRequested") {
    return (
      row.state === "VULNERABLE" &&
      !_.includes(["On_hold", "Requested"], currentVerification)
    );
  }

  return currentVerification.toLowerCase().includes(value.toLowerCase());
};

const isRejectedOrSafeOrSubmitted = (row: Row<IVulnRowAttr>): boolean =>
  ["REJECTED", "SAFE", "SUBMITTED"].includes(row.original.state);

const isVerificationRequestedOrOnHold = (row: Row<IVulnRowAttr>): boolean =>
  ["requested", "on_hold"].includes(
    (row.original.verification ?? "").toLowerCase(),
  );

const isVerificationNotRequested = (row: Row<IVulnRowAttr>): boolean =>
  row.original.verification?.toLowerCase() !== "requested";

const isNotRejected = (row: Row<IVulnRowAttr>): boolean =>
  row.original.state !== "REJECTED";

const isNotVulnerable = (row: Row<IVulnRowAttr>): boolean =>
  row.original.state !== "VULNERABLE";

const isSafe = (row: Row<IVulnRowAttr>): boolean =>
  row.original.state === "SAFE";

const isHackerRemoveReleased = (
  row: Row<IVulnRowAttr>,
  isDeleting: boolean,
  shouldHideRelease: boolean,
): boolean =>
  isDeleting && shouldHideRelease && row.original.state === "VULNERABLE";

const resubmitVulnerabilityProps = (
  findingId: string,
  refetchData: () => void,
): MutationHookOptions<
  ResubmitVulnerabilitiesRequestMutation,
  Exact<{ findingId: string; vulnerabilities: string[] | string }>
> => {
  return {
    onCompleted: (data: IResubmitVulnerabilitiesResultAttr): void => {
      if (data.resubmitVulnerabilities.success) {
        msgSuccess(
          translate.t("groupAlerts.submittedVulnerabilitySuccess"),
          translate.t("groupAlerts.updatedTitle"),
        );
        refetchData();
      }
    },
    onError: ({ graphQLErrors }: ApolloError): void => {
      graphQLErrors.forEach((error): void => {
        msgError(translate.t("groupAlerts.errorTextsad"));
        Logger.warning("An error occurred resubmitting vulnerability", error);
      });
    },
    refetchQueries: (): InternalRefetchQueriesInclude => [
      {
        query: GET_FINDING_INFO,
        variables: {
          findingId,
        },
      },
      {
        query: GET_FINDING_HEADER,
        variables: {
          findingId,
        },
      },
    ],
  };
};

type TCloseVulnerabilitiesResult =
  ExecutionResult<ICloseVulnerabilitiesResultAttr>;

const isSuccessful = (results: TCloseVulnerabilitiesResult[]): boolean[] => {
  return results.map((result): boolean =>
    Boolean(result.data?.closeVulnerabilities.success),
  );
};

const areAllSuccessful = (
  results: TCloseVulnerabilitiesResult[][],
): boolean[] =>
  results
    .map(isSuccessful)
    .reduce(
      (previous: boolean[], current: boolean[]): boolean[] => [
        ...previous,
        ...current,
      ],
      [],
    );

const handleCloseVulnerabilitiesAux = async (
  closeVulnerabilities: (
    variables: Record<string, unknown>,
  ) => Promise<TCloseVulnerabilitiesResult>,
  findingId: string,
  justification: VulnerabilityStateReason,
  vulnerabilities: string[],
): Promise<TCloseVulnerabilitiesResult[]> => {
  const chunkSize = 64;
  const vulnChunks = _.chunk(vulnerabilities, chunkSize);
  const closedChunks = vulnChunks.map(
    (chunk): (() => Promise<TCloseVulnerabilitiesResult[]>) =>
      async (): Promise<TCloseVulnerabilitiesResult[]> =>
        Promise.all([
          closeVulnerabilities({
            variables: {
              findingId,
              justification,
              vulnerabilities: chunk,
            },
          }),
        ]),
  );

  return closedChunks.reduce(
    async (
      previousValue,
      currentValue,
    ): Promise<TCloseVulnerabilitiesResult[]> => [
      ...(await previousValue),
      ...(await currentValue()),
    ],
    Promise.resolve<TCloseVulnerabilitiesResult[]>([]),
  );
};

const handleCloseVulnerabilities = async (
  closeVulnerabilities: (
    variables: Record<string, unknown>,
  ) => Promise<TCloseVulnerabilitiesResult>,
  justification: VulnerabilityStateReason,
  vulnerabilities: IVulnRowAttr[],
): Promise<TCloseVulnerabilitiesResult[][]> => {
  const vulnerabilitiesByFinding = _.groupBy(
    vulnerabilities,
    (vuln: IVulnRowAttr): string => vuln.findingId,
  );
  const closedChunks = Object.entries(vulnerabilitiesByFinding).map(
    ([findingId, chunkedVulnerabilities]: [
      string,
      IVulnRowAttr[],
    ]): (() => Promise<TCloseVulnerabilitiesResult[][]>) =>
      async (): Promise<TCloseVulnerabilitiesResult[][]> => {
        return Promise.all([
          handleCloseVulnerabilitiesAux(
            closeVulnerabilities,
            findingId,
            justification,
            chunkedVulnerabilities.map((vuln): string => vuln.id),
          ),
        ]);
      },
  );

  return closedChunks.reduce(
    async (
      previousValue,
      currentValue,
    ): Promise<TCloseVulnerabilitiesResult[][]> => [
      ...(await previousValue),
      ...(await currentValue()),
    ],
    Promise.resolve<TCloseVulnerabilitiesResult[][]>([]),
  );
};

const isVulnerabilityState = (
  value: string | undefined,
): value is VulnerabilityState => {
  if (value === undefined) return false;

  return Object.values(VulnerabilityState).includes(
    value as VulnerabilityState,
  );
};

const columnHelper = createColumnHelper<IVulnRowAttr>();

const getColumns = (
  group: TGroup,
  cvssVersion: ICVSSVersion,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
): ColumnDef<IVulnRowAttr, any>[] => [
  columnHelper.group({
    columns: [
      columnHelper.accessor("where", {
        cell: (cell): JSX.Element | string => {
          const vuln: IVulnRowAttr = cell.row.original;
          const daysFromReport = dayjs().diff(vuln.reportDate, "days");
          const DAYS_IN_A_WEEK = 7;

          return includeTagsFormatter({
            dataPrivate: true,
            newTag:
              daysFromReport <= DAYS_IN_A_WEEK && vuln.state === "VULNERABLE",
            text: vuln.advisories
              ? `${vuln.where} (${vuln.advisories.package}, ${vuln.advisories.vulnerableVersion})`
              : vuln.where,
          });
        },
        header: translate.t("searchFindings.tabVuln.vulnTable.where"),
        maxSize: 800,
        minSize: 200,
      }),
      columnHelper.accessor("specific", {
        cell: (cell): JSX.Element | string => {
          const vuln: IVulnRowAttr = cell.row.original;

          return includeTagsFormatter({
            dataPrivate: true,
            text: vuln.specific,
          });
        },
        header: translate.t("searchFindings.tabVuln.vulnTable.specific"),
        sortingFn: "alphanumeric",
      }),
      columnHelper.accessor("state", {
        cell: (cell: CellContext<IVulnRowAttr, unknown>): JSX.Element => {
          const labels: Record<string, string> = {
            REJECTED: translate.t("searchFindings.tabVuln.rejected"),
            SAFE: translate.t("searchFindings.tabVuln.closed"),
            SUBMITTED: translate.t("searchFindings.tabVuln.submitted"),
            VULNERABLE: translate.t("searchFindings.tabVuln.open"),
          };

          return statusFormatter(labels[cell.getValue<string>()]);
        },
        header: translate.t("searchFindings.tabVuln.vulnTable.status"),
      }),
    ],
    footer: (props): string => props.column.id,
    header: "Location information",
  }),
  columnHelper.group({
    columns: [
      columnHelper.accessor(cvssVersion.accessorKey, {
        cell: (cell): JSX.Element => severityFormatter(Number(cell.getValue())),
        header: cvssVersion.header,
        id:
          cvssVersion.accessorKey === "severityTemporalScore"
            ? "CVSSF_SCORE"
            : "CVSSF_V4_SCORE",
        minSize: 150,
      }),
      columnHelper.accessor("priority", {
        cell: (cell): string =>
          getPriorityScore(
            cell.row.original.priority,
            group?.totalOpenPriority ?? 0,
            cell.row.original.state,
          ),
        header: translate.t("searchFindings.tabVuln.vulnTable.priority"),
        id: "priority_score",
        minSize: 150,
        sortUndefined: "last",
      }),
    ],
    footer: (props): string => props.column.id,
    header: "Risk and severity",
  }),
  columnHelper.group({
    columns: [
      columnHelper.accessor("reportDate", {
        filterFn: filterDate,
        header: translate.t("searchFindings.tabVuln.vulnTable.reportDate"),
        id: "REPORT_DATE",
        minSize: 170,
      }),
    ],
    footer: (props): string => props.column.id,
    header: "Time and tracking",
  }),
  columnHelper.group({
    columns: [
      columnHelper.accessor("technique", {
        cell: (cell): JSX.Element => {
          const labels: Record<string, string> = {
            CSPM: translate.t("searchFindings.tabVuln.technique.cspm"),
            DAST: translate.t("searchFindings.tabVuln.technique.dast"),
            PTAAS: translate.t("searchFindings.tabVuln.technique.ptaas"),
            RE: translate.t("searchFindings.tabVuln.technique.re"),
            SAST: translate.t("searchFindings.tabVuln.technique.sast"),
            SCA: translate.t("searchFindings.tabVuln.technique.sca"),
            SCR: translate.t("searchFindings.tabVuln.technique.scr"),
          };

          return techniqueFormatter(labels[cell.getValue<string>()]);
        },
        header: translate.t("searchFindings.tabVuln.vulnTable.technique"),
      }),
    ],
    footer: (props): string => props.column.id,
    header: "Techniques and methods",
  }),
  columnHelper.group({
    columns: [
      columnHelper.accessor("treatmentStatus", {
        cell: (cell): string => {
          return formatTreatmentColumn(
            cell.getValue<string>(),
            cell.row.original.treatmentAcceptanceStatus ?? "",
          );
        },
        header: translate.t("searchFindings.tabVuln.vulnTable.treatment"),
        id: "TREATMENT_STATUS",
      }),
      columnHelper.accessor("treatmentAcceptanceStatus", {
        header: translate.t(
          "searchFindings.tabVuln.vulnTable.treatmentAcceptance",
        ),
        minSize: 250,
      }),
      columnHelper.accessor("verification", {
        cell: (cell): string => {
          return String(
            cell.getValue() === "On_hold"
              ? translate.t("searchFindings.tabVuln.onHold")
              : (cell.getValue() ?? ""),
          );
        },
        header: translate.t("searchFindings.tabVuln.vulnTable.reattack"),
      }),
      columnHelper.accessor("tag", {
        enableSorting: false,
        header: translate.t("searchFindings.tabVuln.vulnTable.tags"),
      }),
      columnHelper.accessor("zeroRisk", {
        cell: (cell: CellContext<IVulnRowAttr, unknown>): JSX.Element => {
          return statusFormatter(cell.getValue<string>());
        },
        header: translate.t("searchFindings.tabVuln.vulnTable.zeroRisk"),
        id: "zeroRisk",
      }),
      columnHelper.accessor("treatmentAssigned", {
        cell: (cell): JSX.Element | string => {
          const vuln: IVulnRowAttr = cell.row.original;
          const { treatmentAssigned } = vuln;

          return includeTagsFormatter({
            dataPrivate: true,
            text:
              typeof treatmentAssigned === "string" ? treatmentAssigned : "",
          });
        },
        header: translate.t(
          "searchFindings.tabVuln.vulnTable.treatmentAssigned",
        ),
        id: "TREATMENT_ASSIGNED",
        minSize: 200,
      }),
    ],
    footer: (props): string => props.column.id,
    header: "Vulnerability management",
  }),
];

export {
  areAllSuccessful,
  allVulnsWithEqualStatus,
  allVulnsWithUpdatableSeverity,
  filterVerification,
  getColumns,
  getRejectedVulns,
  getRequestedZeroRiskVulns,
  getSafeVulnsCount,
  getSubmittedVulns,
  getVulnsPendingOfAcceptance,
  getVunerableLocations,
  handleCloseVulnerabilities,
  isNotRejected,
  isSafe,
  isHackerRemoveReleased,
  isNotVulnerable,
  isVulnerabilityState,
  isRejectedOrSafeOrSubmitted,
  isVerificationNotRequested,
  isVerificationRequestedOrOnHold,
  resubmitVulnerabilityProps,
};
