import type { Row } from "@tanstack/react-table";

import {
  isNotRejected,
  isNotVulnerable,
  isSafe,
  isVerificationNotRequested,
} from "../utils";
import type { IVulnRowAttr } from "features/vulnerabilities/types";
import type { TVulnAction } from "hooks/use-vulnerability-store";

const disableRowForSeverity = (
  row: Row<IVulnRowAttr>,
  vulnAction: TVulnAction,
): boolean => {
  if (vulnAction === "severity") {
    if (isSafe(row)) {
      return true;
    }
  }

  return false;
};

const disableRowForVerify = (
  row: Row<IVulnRowAttr>,
  vulnAction: TVulnAction,
): boolean => {
  if (vulnAction === "verify") {
    if (isVerificationNotRequested(row)) {
      return true;
    }
  }

  return false;
};

const disableRowForDelete = (
  row: Row<IVulnRowAttr>,
  vulnAction: TVulnAction,
): boolean => {
  if (vulnAction === "delete") {
    if (isNotVulnerable(row)) {
      return true;
    }
  }

  return false;
};

const disableRowForClose = (
  row: Row<IVulnRowAttr>,
  vulnAction: TVulnAction,
): boolean => {
  if (vulnAction === "close") {
    if (isNotVulnerable(row)) {
      return true;
    }
  }

  return false;
};

const disableRowForResubmit = (
  row: Row<IVulnRowAttr>,
  vulnAction: TVulnAction,
): boolean => {
  if (vulnAction === "resubmit") {
    if (isNotRejected(row)) {
      return true;
    }
  }

  return false;
};

export {
  disableRowForSeverity,
  disableRowForVerify,
  disableRowForDelete,
  disableRowForClose,
  disableRowForResubmit,
};
