import { useLazyQuery } from "@apollo/client";
import {
  Button,
  Container,
  Icon,
  ListItemsWrapper,
  Loading,
  useModal,
} from "@fluidattacks/design";
import {
  Fragment,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from "react";
import { useTranslation } from "react-i18next";

import { GET_VULNS } from "../../queries";
import type { IVulnerabilityEdge } from "../../types";
import type { IDropDownOption } from "components/dropdown/types";
import { authzGroupContext } from "context/authz/config";
import { SubmittedForm } from "features/vulnerabilities/handle-acceptance-modal/submitted-form";
import { TreatmentAcceptanceForm } from "features/vulnerabilities/handle-acceptance-modal/treatment-acceptance-form";
import { ZeroRiskForm } from "features/vulnerabilities/handle-acceptance-modal/zero-risk-form";
import type { IVulnRowAttr } from "features/vulnerabilities/types";
import { formatVulnerabilitiesTreatment } from "features/vulnerabilities/utils";
import { useAuthz } from "hooks/use-authz";
import { useClickOutside } from "hooks/use-click-outside";
import {
  type ITreatmentAcceptanceActionsDisabled,
  type ITreatmentAcceptanceOptions,
  type TTreatmentAcceptanceAction,
  getFilter,
} from "hooks/use-treatment-acceptance";

interface IHandleAcceptanceActionProps {
  readonly additionalRef: React.RefObject<HTMLDivElement> | null;
  readonly findingId: string;
  readonly onAction: () => Promise<void>;
  readonly disabled: boolean;
  readonly treatmentsDisabled: ITreatmentAcceptanceActionsDisabled;
}

const HandleAcceptanceAction = ({
  additionalRef,
  findingId,
  onAction,
  disabled,
  treatmentsDisabled,
}: IHandleAcceptanceActionProps): JSX.Element => {
  const { t } = useTranslation();
  const attributes = useContext(authzGroupContext);
  const { canMutate, canResolve } = useAuthz();
  const [open, setOpen] = useState(false);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const addition = additionalRef?.current as HTMLElement;
  const [modalContent, setModalContent] = useState<JSX.Element | undefined>(
    undefined,
  );
  const [isModalRendered, setIsModalRendered] = useState<boolean>(false);

  const [isProcessing, setIsProcessing] = useState<boolean>(false);
  const [isDisabled, setIsDisabled] = useState<boolean>(disabled);
  const allActionsDisabled =
    isDisabled ||
    Object.values(treatmentsDisabled).every((value): boolean => value === true);
  const handleDropdown = useCallback((): void => {
    setOpen((previousState): boolean => !previousState);
  }, []);

  const canRetrieveHacker = canResolve("vulnerability_hacker");

  const treatmentAcceptanceModalRef = useModal("treatment-acceptance-modal");
  const zeroRiskModalRef = useModal("zero-risk-modal");
  const submittedModalRef = useModal("submitted-form");

  const canHandleAcceptance = canMutate("handle_vulnerabilities_acceptance");
  const canConfirmVulnerabilities = canMutate("confirm_vulnerabilities");
  const canRejectVulnerabilities = canMutate("reject_vulnerabilities");

  const canConfirmZeroRiskVuln =
    canMutate("confirm_vulnerabilities_zero_risk") &&
    attributes.can("can_request_zero_risk");

  const canRejectZeroRiskVuln =
    canMutate("reject_vulnerabilities_zero_risk") &&
    attributes.can("can_request_zero_risk");

  const canUpdateVulns = attributes.can("can_report_vulnerabilities");

  const [treatment, setTreatment] = useState<string | undefined>(undefined);

  const setTreatmentHelper = useCallback(({ value }: IDropDownOption): void => {
    setTreatment(value);
  }, []);

  const handleInternalModal = useCallback((): void => {
    setModalContent(undefined);
    setIsModalOpen(false);
    setTreatment("");
  }, []);

  const treatmentOptions: ITreatmentAcceptanceOptions[] = [
    {
      disabled: treatmentsDisabled.acceptedUndefined,
      extraCondition: canHandleAcceptance,
      header: t("searchFindings.tabDescription.treatment.acceptedUndefined"),
      icon: "check",
      value: "ACCEPTED_UNDEFINED",
    },
    {
      disabled: treatmentsDisabled.accepted,
      extraCondition: canHandleAcceptance,
      header: t("searchFindings.tabDescription.treatment.accepted"),
      icon: "calendar-check",
      value: "ACCEPTED",
    },
    {
      disabled: treatmentsDisabled.confirmRejectVulnerability,
      extraCondition:
        canConfirmVulnerabilities && canRejectVulnerabilities && canUpdateVulns,
      header: t(
        "searchFindings.tabDescription.treatment.confirmRejectVulnerability",
      ),
      icon: "square-question",
      value: "CONFIRM_REJECT_VULNERABILITY",
    },
    {
      disabled: treatmentsDisabled.confirmRejectZeroRisk,
      extraCondition:
        canConfirmZeroRiskVuln && canRejectZeroRiskVuln && canUpdateVulns,
      header: t(
        "searchFindings.tabDescription.treatment.confirmRejectZeroRisk",
      ),
      icon: "circle-question",
      value: "CONFIRM_REJECT_ZERO_RISK",
    },
  ];

  const [fetchVulns, { data: vulnsData, loading: gqlLoading }] = useLazyQuery(
    GET_VULNS,
    {
      fetchPolicy: "no-cache",
      variables: {
        after: null,
        findingId,
        first: 200,
      },
    },
  );

  const newData = useMemo((): IVulnRowAttr[] => {
    const processedData = formatVulnerabilitiesTreatment({
      organizationsGroups: undefined,
      vulnerabilities: (
        (vulnsData?.finding.vulnerabilities.edges as
          | IVulnerabilityEdge[]
          | undefined) ?? []
      ).map(
        (edge): IVulnRowAttr => ({
          ...edge.node,
          groupName: edge.node.groupName,
          where:
            edge.node.vulnerabilityType === "lines" &&
            edge.node.rootNickname !== null &&
            edge.node.rootNickname !== "" &&
            !edge.node.where.startsWith(`${edge.node.rootNickname}/`)
              ? `${edge.node.rootNickname}/${edge.node.where}`
              : edge.node.where,
        }),
      ),
    });
    if (!isModalRendered) {
      setIsModalRendered(true);
    }
    setIsProcessing(false);

    return processedData;
  }, [isModalRendered, vulnsData?.finding.vulnerabilities.edges]);

  const treatmentModal: Record<
    TTreatmentAcceptanceAction,
    JSX.Element | undefined
  > = useMemo(
    (): Record<TTreatmentAcceptanceAction, JSX.Element | undefined> => ({
      ACCEPTED: (
        <TreatmentAcceptanceForm
          modalRef={{
            ...treatmentAcceptanceModalRef,
            close: handleInternalModal,
            isOpen: true,
          }}
          onCancel={handleInternalModal}
          refetchData={onAction}
          title={t("searchFindings.tabDescription.treatment.accepted")}
          treatmentType={"ACCEPTED"}
          vulnerabilities={newData}
        />
      ),
      ACCEPTED_UNDEFINED: (
        <TreatmentAcceptanceForm
          modalRef={{
            ...treatmentAcceptanceModalRef,
            close: handleInternalModal,
            isOpen: true,
          }}
          onCancel={handleInternalModal}
          refetchData={onAction}
          title={t("searchFindings.tabDescription.treatment.acceptedUndefined")}
          treatmentType={"ACCEPTED_UNDEFINED"}
          vulnerabilities={newData}
        />
      ),
      CONFIRM_REJECT_VULNERABILITY: (
        <SubmittedForm
          findingId={findingId}
          modalRef={{
            ...submittedModalRef,
            close: handleInternalModal,
            isOpen: true,
          }}
          onCancel={handleInternalModal}
          refetchData={onAction}
          title={t(
            "searchFindings.tabDescription.treatment.confirmRejectVulnerability",
          )}
          vulnerabilities={newData}
        />
      ),
      CONFIRM_REJECT_ZERO_RISK: (
        <ZeroRiskForm
          findingId={findingId}
          modalRef={{
            ...zeroRiskModalRef,
            close: handleInternalModal,
            isOpen: true,
          }}
          onCancel={handleInternalModal}
          refetchData={onAction}
          title={t(
            "searchFindings.tabDescription.treatment.confirmRejectZeroRisk",
          )}
          vulnerabilities={newData}
        />
      ),
      none: undefined,
    }),
    [
      findingId,
      handleInternalModal,
      newData,
      onAction,
      submittedModalRef,
      t,
      treatmentAcceptanceModalRef,
      zeroRiskModalRef,
    ],
  );

  const handleTreatment = useCallback(
    async (item: IDropDownOption): Promise<void> => {
      setOpen(false);
      setIsModalOpen(true);

      await fetchVulns({
        fetchPolicy: "no-cache",
        variables: {
          canRetrieveHacker,
          filters: getFilter(item.value ?? ""),
          findingId,
          first: 200,
        },
      });

      setTreatmentHelper(item);
    },
    [fetchVulns, canRetrieveHacker, findingId, setTreatmentHelper],
  );

  const handleOnClick = useCallback((): void => {
    if (!isModalRendered || modalContent) {
      return;
    }

    setModalContent(treatmentModal[treatment as TTreatmentAcceptanceAction]);
  }, [isModalRendered, modalContent, treatment, treatmentModal]);

  useClickOutside(
    addition,
    (): void => {
      setOpen(false);
    },
    true,
  );

  useEffect((): void => {
    if (gqlLoading) {
      setIsProcessing(true);
      setIsDisabled(true);
    }
    if (!gqlLoading && !isProcessing) {
      handleOnClick();
      setIsDisabled(false);
    }
  }, [gqlLoading, handleOnClick, isProcessing, newData.length]);

  return (
    <Fragment>
      <Button
        disabled={allActionsDisabled}
        icon={"ellipsis-vertical"}
        id={"handleAcceptanceButton"}
        onClick={handleDropdown}
        variant={"ghost"}
      >
        {t("searchFindings.tabVuln.buttons.handleAcceptance")}
      </Button>
      {gqlLoading || isProcessing ? <Loading /> : undefined}
      <Container
        display={open ? "block" : "none"}
        maxWidth={"100px"}
        mt={0.25}
        position={"absolute"}
        right={"250px"}
        top={"35px"}
        zIndex={2}
      >
        {open ? (
          <ListItemsWrapper
            aria-labelledby={"treatment-type"}
            data-testid={"treatment-type"}
            role={"menu"}
            tabIndex={-1}
          >
            {treatmentOptions.map((option, _): JSX.Element | null => {
              if (option.extraCondition) {
                return (
                  <Button
                    disabled={option.disabled}
                    id={option.value}
                    justify={"start"}
                    key={option.value}
                    // eslint-disable-next-line react/jsx-no-bind
                    onClick={async (): Promise<void> => {
                      await handleTreatment(option);
                    }}
                    variant={"ghost"}
                    width={"100%"}
                  >
                    <Container justify={"start"} pl={0.5}>
                      <Icon
                        icon={option.icon}
                        iconSize={"xs"}
                        iconType={"fa-light"}
                        mr={0.25}
                      ></Icon>
                      {option.header}
                    </Container>
                  </Button>
                );
              }

              return null;
            })}
          </ListItemsWrapper>
        ) : null}
      </Container>
      {isModalOpen && modalContent}
    </Fragment>
  );
};

export { HandleAcceptanceAction };
