import { useMutation } from "@apollo/client";
import {
  Button,
  Form,
  InnerForm,
  InputFile,
  Modal,
  Text,
  useModal,
} from "@fluidattacks/design";
import { Fragment, useCallback } from "react";
import { useTranslation } from "react-i18next";

import { GET_FINDING_INFO } from "../../queries";
import { UPLOAD_VULNERABILITIES } from "features/vulnerabilities/queries";
import { handleUploadError } from "features/vulnerabilities/upload-file/hooks";
import type { IUploadVulnFile } from "features/vulnerabilities/upload-file/types";
import { validationSchema } from "features/vulnerabilities/upload-file/validations";
import { GET_FINDING_HEADER } from "pages/finding/queries";
import { showNotification } from "utils/notifications";

interface IUploadVulnsModalProps {
  readonly findingId: string;
  readonly onAction: () => Promise<void>;
}

const UploadVulnsAction = ({
  findingId,
  onAction,
}: IUploadVulnsModalProps): JSX.Element => {
  const { t } = useTranslation();
  const modalRef = useModal("update-vulnerabilities-modal");
  const { close, open, isOpen } = modalRef;
  const [uploadVulnerability] = useMutation(UPLOAD_VULNERABILITIES, {
    onCompleted: (result): void => {
      if (result.uploadFile.success) {
        close();
        if (
          result.uploadFile.message === null ||
          result.uploadFile.message === ""
        ) {
          showNotification(
            "success",
            t("groupAlerts.fileUpdated"),
            t("groupAlerts.titleSuccess"),
          );
        } else {
          showNotification(
            "warning",
            result.uploadFile.message,
            t("groupAlerts.fileUpdatedWarning"),
          );
        }
      } else {
        showNotification(
          "error",
          t("searchFindings.tabVuln.alerts.uploadFile.noChangesWereMade"),
        );
      }
    },
    onError: handleUploadError,
    refetchQueries: [
      {
        query: GET_FINDING_INFO,
        variables: { findingId },
      },
      {
        query: GET_FINDING_HEADER,
        variables: { findingId },
      },
    ],
  });

  const handleUploadVulnerability = useCallback(
    async (values: IUploadVulnFile): Promise<void> => {
      await uploadVulnerability({
        variables: {
          file: values.filename[0],
          findingId,
        },
      });
      await onAction();
    },
    [findingId, uploadVulnerability, onAction],
  );

  return (
    <Fragment>
      <Button
        icon={"upload"}
        justify={"start"}
        onClick={open}
        variant={"secondary"}
        width={"100%"}
      >
        {t("searchFindings.tabDescription.updateVulnerabilities")}
      </Button>
      {isOpen ? (
        <Modal
          id={"uploadVulns"}
          modalRef={modalRef}
          size={"sm"}
          title={t("searchFindings.tabResources.modalFileTitle")}
        >
          <Form
            cancelButton={{ onClick: close }}
            defaultValues={{ filename: undefined as unknown as FileList }}
            onSubmit={handleUploadVulnerability}
            yupSchema={validationSchema}
          >
            <InnerForm>
              {({ formState, register, setValue, watch }): JSX.Element => (
                <Fragment>
                  <Text mb={1} size={"sm"}>
                    {t(
                      "searchFindings.tabDescription.updateVulnerabilitiesTooltip",
                    )}
                  </Text>
                  <InputFile
                    accept={".yaml,.yml"}
                    error={formState.errors.filename?.message?.toString()}
                    id={"filename"}
                    name={"filename"}
                    register={register}
                    setValue={setValue}
                    watch={watch}
                  />
                </Fragment>
              )}
            </InnerForm>
          </Form>
        </Modal>
      ) : null}
    </Fragment>
  );
};

export { UploadVulnsAction };
