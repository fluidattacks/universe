import { Button, Container, useModal } from "@fluidattacks/design";
import { useTranslation } from "react-i18next";

import { RemoveVulnerabilityModal } from "features/vulnerabilities/remove-vulnerability-modal";
import type { IVulnRowAttr } from "features/vulnerabilities/types";

interface IDeleteVulnsActionProps {
  readonly selectedVulns: IVulnRowAttr[];
  readonly onAction: () => Promise<void>;
  readonly onCancel: () => void;
}

const DeleteVulnsAction = ({
  selectedVulns,
  onAction,
  onCancel,
}: IDeleteVulnsActionProps): JSX.Element => {
  const { t } = useTranslation();
  const { open, isOpen, close } = useModal("delete-vulns-modal");

  return (
    <Container display={"flex"} gap={0.75}>
      <Button
        disabled={selectedVulns.length === 0}
        icon={"trash-alt"}
        id={"delete"}
        onClick={open}
        variant={"primary"}
        width={"100%"}
      >
        {t("searchFindings.tabVuln.buttons.delete.text")}
      </Button>
      <Button
        id={"cancel-severity-update"}
        onClick={onCancel}
        variant={"tertiary"}
        width={"100%"}
      >
        {t("searchFindings.tabVuln.buttons.cancel")}
      </Button>
      {isOpen ? (
        <RemoveVulnerabilityModal
          onClose={close}
          onRemoveVulnRes={onAction}
          open={isOpen}
          vulnerabilities={selectedVulns}
        />
      ) : undefined}
    </Container>
  );
};

export { DeleteVulnsAction };
