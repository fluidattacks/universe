import { useMutation } from "@apollo/client";
import mixpanel from "mixpanel-browser";
import { useCallback } from "react";
import { useTranslation } from "react-i18next";

import { DOWNLOAD_VULNERABILITIES } from "features/vulnerabilities/queries";
import { useAudit } from "hooks/use-audit";
import { Logger } from "utils/logger";
import { showNotification } from "utils/notifications";
import { openUrl } from "utils/resource-helpers";

interface IUseDownloadVulnMutation {
  downloadVulnerability: () => void;
}

const useDownloadVulnMutation = (
  findingId: string,
  groupName: string,
): IUseDownloadVulnMutation => {
  const { t } = useTranslation();

  const { addAuditEvent } = useAudit();
  const [downloadVulnMutation] = useMutation(DOWNLOAD_VULNERABILITIES, {
    onCompleted: ({ downloadVulnerabilityFile }): void => {
      if (
        downloadVulnerabilityFile.success &&
        downloadVulnerabilityFile.url !== ""
      ) {
        mixpanel.track("DownloadVulnFile", {
          fileName: downloadVulnerabilityFile.url,
          group: groupName,
        });
        addAuditEvent("Vulnerability.Download", downloadVulnerabilityFile.url);
        openUrl(downloadVulnerabilityFile.url);
      }
    },
    onError: (downloadError): void => {
      downloadError.graphQLErrors.forEach(({ message }): void => {
        showNotification("error", t("groupAlerts.errorTextsad"));
        if (message === "Exception - Error Uploading File to S3") {
          Logger.warning(
            "An error occurred downloading vuln file while uploading file to S3",
            downloadError,
          );
        } else {
          Logger.warning(
            "An error occurred downloading vuln file",
            downloadError,
          );
        }
      });
    },
  });

  const downloadVulnerability = useCallback((): void => {
    void downloadVulnMutation({ variables: { findingId } });
  }, [downloadVulnMutation, findingId]);

  return { downloadVulnerability };
};

export { useDownloadVulnMutation };
