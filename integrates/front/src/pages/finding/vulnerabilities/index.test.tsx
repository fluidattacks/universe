import { PureAbility } from "@casl/ability";
import { Toggle } from "@fluidattacks/design";
import { screen, waitFor, within } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import dayjs from "dayjs";
import { GraphQLError } from "graphql";
import isEqual from "lodash/isEqual";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import { Fragment, useCallback, useState } from "react";
import { Route, Routes } from "react-router-dom";

import { FindingVulnerabilities } from ".";
import {
  authzGroupContext,
  authzPermissionsContext,
} from "context/authz/config";
import type {
  GetFindingInfoQuery as GetFindingInfo,
  GetFindingVulnsQueriesQuery as GetFindingVulnsQueries,
  ResubmitVulnerabilitiesRequestMutation as ResubmitVulnerabilitiesRequest,
} from "gql/graphql";
import {
  RootCriticality,
  Technique,
  VulnerabilityState,
  VulnerabilityTreatment,
} from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";
import {
  GET_FINDING_INFO,
  GET_FINDING_VULNS_QUERIES,
  RESUBMIT_VULNERABILITIES,
} from "pages/finding/vulnerabilities/queries";
import { msgSuccess, msgWarning } from "utils/notifications";
import { translate } from "utils/translations/translate";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();
  jest.spyOn(mockedNotifications, "msgWarning").mockImplementation();

  return mockedNotifications;
});

const Wrapper = ({
  mockedPermissions,
  mockServices,
}: Readonly<{
  mockedPermissions: PureAbility<string>;
  mockServices: PureAbility<string>;
}>): JSX.Element => {
  const [cvssVersion, setCvssVersion] = useState<{
    accessorKey: "severityTemporalScore" | "severityThreatScore";
    header: string;
  }>({
    accessorKey: "severityTemporalScore",
    header: "Severity (v3.1)",
  });

  const onChangeCvssVersion = useCallback((): void => {
    setCvssVersion(
      (
        value,
      ): {
        accessorKey: "severityTemporalScore" | "severityThreatScore";
        header: string;
      } => {
        if (value.accessorKey === "severityThreatScore") {
          return {
            accessorKey: "severityTemporalScore",
            header: "Severity (v3.1)",
          };
        }

        return {
          accessorKey: "severityThreatScore",
          header: "Severity (v4.0)",
        };
      },
    );
  }, [setCvssVersion]);

  return (
    <authzPermissionsContext.Provider value={mockedPermissions}>
      <authzGroupContext.Provider value={mockServices}>
        <Routes>
          <Route
            element={
              <Fragment>
                <Toggle
                  defaultChecked={
                    cvssVersion.accessorKey === "severityThreatScore"
                  }
                  leftDescription={"CVSS 3.1"}
                  name={"cvssToggle"}
                  onChange={onChangeCvssVersion}
                  rightDescription={"CVSS 4.0"}
                />
                <FindingVulnerabilities cvssVersion={cvssVersion} />
              </Fragment>
            }
            path={
              "/orgs/:organizationName/groups/:groupName/vulns/:findingId/locations"
            }
          />
        </Routes>
      </authzGroupContext.Provider>
    </authzPermissionsContext.Provider>
  );
};

describe("vulnerabilities", (): void => {
  const totalRows = 7;

  const edges = [
    {
      node: {
        __typename: "Vulnerability",
        advisories: null,
        closingDate: null,
        customSeverity: 0,
        externalBugTrackingSystem: null,
        findingId: "422286126",
        groupName: "testgroup",
        hacker: "hacker1@test.test",
        id: "89521e9a-b1a3-4047-a16e-15d530dc1340",
        lastEditedBy: "test@test.test",
        lastStateDate: "2019-07-05 09:56:40",
        lastTreatmentDate: "2019-07-05 09:56:40",
        lastVerificationDate: null,
        priority: 0,
        remediated: true,
        reportDate: dayjs().format("YYYY-MM-DD hh:mm:ss A"),
        root: {
          __typename: "GitRoot",
          branch: "master",
          criticality: RootCriticality.Low,
        },
        rootNickname: "",
        severityTemporalScore: 6.7,
        severityThreatScore: 6.6,
        severityVector: "AV:A/AC:H/PR:L/UI:R/S:C/C:L/I:L/A:L/E:F/RL:O/RC:U",
        severityVectorV4:
          "CVSS:4.0/AV:A/AC:H/PR:L/UI:P/VC:L/VI:L/VA:L/SC:L/SI:L/SA:L/E:A",
        source: "asm",
        specific: "specific-1",
        state: VulnerabilityState.Vulnerable,
        stateReasons: null,
        stream: "home > blog > articulo",
        tag: "tag-1, tag-2",
        technique: Technique.Sast,
        treatmentAcceptanceDate: "",
        treatmentAcceptanceStatus: "",
        treatmentAssigned: "assigned-user-1",
        treatmentJustification: "test progress justification",
        treatmentStatus: VulnerabilityTreatment.InProgress,
        treatmentUser: "usertreatment@test.test",
        verification: "Requested",
        verificationJustification: null,
        vulnerabilityType: "inputs",
        where: "https://example.com/inputs",
        zeroRisk: null,
      },
    },
    {
      node: {
        __typename: "Vulnerability",
        advisories: null,
        closingDate: null,
        customSeverity: 0,
        externalBugTrackingSystem: null,
        findingId: "422286126",
        groupName: "testgroup",
        hacker: "hacker2@test.test",
        id: "6903f3e4-a8ee-4a5d-ac38-fb738ec7e540",
        lastEditedBy: "test@test.test",
        lastStateDate: "2019-07-05 09:56:40",
        lastTreatmentDate: "2019-07-05 09:56:40",
        lastVerificationDate: null,
        priority: 0,
        remediated: false,
        reportDate: "2020-07-05 09:56:40",
        root: {
          __typename: "GitRoot",
          branch: "master",
          criticality: RootCriticality.Low,
        },
        rootNickname: "https:",
        severityTemporalScore: 6.7,
        severityThreatScore: 6.5,
        severityVector: "AV:A/AC:H/PR:L/UI:R/S:C/C:L/I:L/A:L/E:F/RL:O/RC:U",
        severityVectorV4:
          "CVSS:4.0/AV:A/AC:H/PR:L/UI:P/VC:L/VI:L/VA:L/SC:L/SI:L/SA:L/E:A",
        source: "asm",
        specific: "specific-3",
        state: VulnerabilityState.Vulnerable,
        stateReasons: null,
        stream: null,
        tag: "tag-3",
        technique: Technique.Sast,
        treatmentAcceptanceDate: "",
        treatmentAcceptanceStatus: "",
        treatmentAssigned: "assigned-user-1",
        treatmentJustification: "test progress justification",
        treatmentStatus: VulnerabilityTreatment.InProgress,
        treatmentUser: "usertreatment@test.test",
        verification: null,
        verificationJustification: null,
        vulnerabilityType: "lines",
        where: "https://example.com/tests",
        zeroRisk: null,
      },
    },
    {
      node: {
        __typename: "Vulnerability",
        advisories: null,
        closingDate: null,
        customSeverity: 0,
        externalBugTrackingSystem: null,
        findingId: "422286126",
        groupName: "testgroup",
        hacker: "hacker@test.test",
        id: "7903f3e4-a8ee-4a5d-ac38-fb738ec7e540",
        lastEditedBy: "test@test.test",
        lastStateDate: "2019-07-05 09:56:40",
        lastTreatmentDate: "2019-07-05 09:56:40",
        lastVerificationDate: null,
        priority: 0,
        remediated: false,
        reportDate: "2019-07-05 09:56:40",
        root: {
          __typename: "GitRoot",
          branch: "master",
          criticality: RootCriticality.Low,
        },
        rootNickname: "test",
        severityTemporalScore: 6.7,
        severityThreatScore: 6.6,
        severityVector: "AV:A/AC:H/PR:L/UI:R/S:C/C:L/I:L/A:L/E:F/RL:O/RC:U",
        severityVectorV4:
          "CVSS:4.0/AV:A/AC:H/PR:L/UI:P/VC:L/VI:L/VA:L/SC:L/SI:L/SA:L/E:A",
        source: "machine",
        specific: "specific-4",
        state: VulnerabilityState.Vulnerable,
        stateReasons: null,
        stream: null,
        tag: "tag-5",
        technique: Technique.Sast,
        treatmentAcceptanceDate: "",
        treatmentAcceptanceStatus: "",
        treatmentAssigned: "assigned-user-1",
        treatmentJustification: "test progress justification",
        treatmentStatus: VulnerabilityTreatment.InProgress,
        treatmentUser: "usertreatment@test.test",
        verification: null,
        verificationJustification: null,
        vulnerabilityType: "lines",
        where: "https://example.com/location",
        zeroRisk: null,
      },
    },
    {
      node: {
        __typename: "Vulnerability",
        advisories: null,
        closingDate: null,
        customSeverity: 0,
        externalBugTrackingSystem: null,
        findingId: "422286126",
        groupName: "testgroup",
        hacker: "hacker1@test.test",
        id: "59521e9a-b1a3-4047-a16e-15d530dc1340",
        lastEditedBy: "test@test.test",
        lastStateDate: "2019-07-05 09:56:40",
        lastTreatmentDate: "2019-07-05 09:56:40",
        lastVerificationDate: null,
        priority: 0,
        remediated: true,
        reportDate: "2019-07-06 09:56:40",
        root: {
          __typename: "GitRoot",
          branch: "master",
          criticality: RootCriticality.Low,
        },
        rootNickname: "",
        severityTemporalScore: 6.7,
        severityThreatScore: 6.6,
        severityVector: "AV:A/AC:H/PR:L/UI:R/S:C/C:L/I:L/A:L/E:F/RL:O/RC:U",
        severityVectorV4:
          "CVSS:4.0/AV:A/AC:H/PR:L/UI:P/VC:L/VI:L/VA:L/SC:L/SI:L/SA:L/E:A",
        source: "asm",
        specific: "specific-5",
        state: VulnerabilityState.Submitted,
        stateReasons: null,
        stream: null,
        tag: "tag-5, tag-6, tag-7",
        technique: Technique.Sast,
        treatmentAcceptanceDate: "",
        treatmentAcceptanceStatus: "",
        treatmentAssigned: "usertreatment@test.test",
        treatmentJustification: "",
        treatmentStatus: VulnerabilityTreatment.Untreated,
        treatmentUser: "usertreatment@test.test",
        verification: null,
        verificationJustification: null,
        vulnerabilityType: "inputs",
        where: "https://example.com/status",
        zeroRisk: null,
      },
    },
    {
      node: {
        __typename: "Vulnerability",
        advisories: null,
        closingDate: null,
        customSeverity: 0,
        externalBugTrackingSystem: null,
        findingId: "422286126",
        groupName: "testgroup",
        hacker: "hacker1@test.test",
        id: "49521e9a-b1a3-4047-a16e-15d530dc1340",
        lastEditedBy: "test@test.test",
        lastStateDate: "2019-07-05 09:56:40",
        lastTreatmentDate: "2019-07-05 09:56:40",
        lastVerificationDate: null,
        priority: 0,
        remediated: true,
        reportDate: "2019-07-07 09:56:40",
        root: {
          __typename: "GitRoot",
          branch: "master",
          criticality: RootCriticality.Low,
        },
        rootNickname: "",
        severityTemporalScore: 6.7,
        severityThreatScore: 6.5,
        severityVector: "AV:A/AC:H/PR:L/UI:R/S:C/C:L/I:L/A:L/E:F/RL:O/RC:U",
        severityVectorV4:
          "CVSS:4.0/AV:A/AC:H/PR:L/UI:P/VC:L/VI:L/VA:L/SC:L/SI:L/SA:L/E:A",
        source: "asm",
        specific: "specific-5",
        state: VulnerabilityState.Safe,
        stateReasons: null,
        stream: null,
        tag: "tag-6, tag-7",
        technique: Technique.Sast,
        treatmentAcceptanceDate: "",
        treatmentAcceptanceStatus: "",
        treatmentAssigned: "usertreatment@test.test",
        treatmentJustification: "justification safe",
        treatmentStatus: VulnerabilityTreatment.Accepted,
        treatmentUser: "usertreatment@test.test",
        verification: null,
        verificationJustification: null,
        vulnerabilityType: "ports",
        where: "https://example.com/statusSafe",
        zeroRisk: null,
      },
    },
  ];
  const zeroRiskConnectionEdgesMock = [
    {
      node: {
        __typename: "Vulnerability",
        advisories: null,
        closingDate: null,
        customSeverity: 0,
        externalBugTrackingSystem: null,
        findingId: "422286126",
        groupName: "testgroup",
        hacker: "hacker@test.test",
        id: "a09c79fc-33fb-4abd-9f20-f3ab1f500bd0",
        lastEditedBy: "test@test.test",
        lastStateDate: "2019-07-05 09:56:40",
        lastTreatmentDate: "2019-07-05 09:56:40",
        lastVerificationDate: null,
        priority: 0,
        remediated: false,
        reportDate: "2019-07-05 09:56:40",
        root: {
          __typename: "GitRoot",
          branch: "master",
          criticality: RootCriticality.Low,
        },
        rootNickname: "https:",
        severityTemporalScore: 4.2,
        severityThreatScore: 4.1,
        severityVector: "AV:A/AC:H/PR:L/UI:R/S:C/C:L/I:L/A:L/E:F/RL:O/RC:U",
        severityVectorV4:
          "CVSS:4.0/AV:A/AC:H/PR:L/UI:P/VC:L/VI:L/VA:L/SC:L/SI:L/SA:L/E:A",
        source: "asm",
        specific: "specific-2",
        state: VulnerabilityState.Vulnerable,
        stateReasons: null,
        stream: null,
        tag: "tag-3",
        technique: Technique.Sast,
        treatmentAcceptanceDate: "",
        treatmentAcceptanceStatus: "",
        treatmentAssigned: "assigned-user-1",
        treatmentJustification: "test progress justification",
        treatmentStatus: VulnerabilityTreatment.InProgress,
        treatmentUser: "usertreatment@test.test",
        verification: "Verified",
        verificationJustification: null,
        vulnerabilityType: "lines",
        where: "https://example.com/lines",
        zeroRisk: "Requested",
      },
    },
  ];
  const graphqlMocked = graphql.link(LINK);
  const mocksQueryFindingVulns = graphqlMocked.query(
    GET_FINDING_VULNS_QUERIES,
    ({
      variables,
    }): StrictResponse<IErrorMessage | { data: GetFindingVulnsQueries }> => {
      const {
        afterNZR,
        canRetrieveHacker,
        findingId,
        canRetrieveDrafts,
        first,
        state,
      } = variables;
      const isSafeState = state === VulnerabilityState.Safe;

      if (
        afterNZR === undefined &&
        canRetrieveHacker === true &&
        findingId === "422286126" &&
        canRetrieveDrafts &&
        first === 200 &&
        (state === VulnerabilityState.Rejected || isSafeState)
      ) {
        return HttpResponse.json({
          data: {
            __typename: "Query",
            ...{
              finding: {
                __typename: "Finding",
                draftsConnection: {
                  edges: [],
                  pageInfo: {
                    endCursor: "test-cursor=",
                    hasNextPage: false,
                  },
                },
                id: "422286126",
                vulnerabilitiesConnection: {
                  edges: isSafeState ? [edges[0], edges[1]] : [],
                  pageInfo: {
                    endCursor: "test-cursor=",
                    hasNextPage: false,
                  },
                },
                zeroRiskConnection: {
                  edges: [],
                  pageInfo: {
                    endCursor: "test-cursor=",
                    hasNextPage: false,
                  },
                },
              },
            },
          },
        });
      }

      if (
        afterNZR === undefined &&
        canRetrieveHacker === true &&
        findingId === "422286126" &&
        canRetrieveDrafts &&
        first === 200
      ) {
        return HttpResponse.json({
          data: {
            __typename: "Query",
            ...{
              finding: {
                __typename: "Finding",
                draftsConnection: {
                  edges: [],
                  pageInfo: {
                    endCursor: "test-cursor=",
                    hasNextPage: false,
                  },
                },
                id: "422286126",
                vulnerabilitiesConnection: {
                  edges,
                  pageInfo: {
                    endCursor: "test-cursor=",
                    hasNextPage: false,
                  },
                },
                zeroRiskConnection: {
                  edges: zeroRiskConnectionEdgesMock,
                  pageInfo: {
                    endCursor: "test-cursor=",
                    hasNextPage: false,
                  },
                },
              },
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Error getting vulnerabilities")],
      });
    },
  );

  it("should render container", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      {
        action:
          "integrates_api_mutations_confirm_vulnerabilities_zero_risk_mutate",
      },
      { action: "integrates_api_resolvers_vulnerability_hacker_resolve" },
      {
        action: "integrates_api_resolvers_finding_zero_risk_connection_resolve",
      },
      { action: "integrates_api_resolvers_finding_drafts_connection_resolve" },
    ]);

    render(
      <Wrapper
        mockServices={
          new PureAbility([{ action: "can_report_vulnerabilities" }])
        }
        mockedPermissions={mockedPermissions}
      />,
      {
        memoryRouter: {
          initialEntries: [
            "/orgs/testorg/groups/testgroup/vulns/422286126/locations",
          ],
        },
        mocks: [mocksQueryFindingVulns],
      },
    );

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(totalRows);
    });

    expect(
      screen.getByRole("button", { name: "filter.title" }),
    ).toBeInTheDocument();

    await userEvent.click(screen.getByRole("button", { name: "filter.title" }));

    expect(
      screen.getByRole("button", { hidden: true, name: "filter.cancel" }),
    ).toBeInTheDocument();

    await userEvent.click(
      screen.getByRole("button", { hidden: true, name: "filter.cancel" }),
    );

    await userEvent.click(
      screen.getByTestId("verification-select-selected-option"),
    );
    await userEvent.click(
      screen.getByRole("listitem", {
        name: translate.t("On_hold"),
      }),
    );

    await waitFor((): void => {
      expect(screen.queryByText("table.noDataIndication")).toBeInTheDocument();
    });

    await userEvent.click(
      screen.getByTestId("verification-select-selected-option"),
    );
    await userEvent.click(
      screen.getByRole("listitem", {
        name: translate.t("Requested"),
      }),
    );

    await waitFor((): void => {
      expect(
        screen.queryByText("table.noDataIndication"),
      ).not.toBeInTheDocument();
    });

    expect(
      screen.queryByText("https://example.com/inputs"),
    ).toBeInTheDocument();

    await userEvent.click(
      screen.getByTestId("verification-select-selected-option"),
    );
    await userEvent.click(
      screen.getByRole("listitem", {
        name: translate.t("Verified"),
      }),
    );

    await waitFor((): void => {
      expect(
        screen.queryByText("https://example.com/inputs"),
      ).not.toBeInTheDocument();
    });

    await userEvent.click(
      screen.getByTestId("verification-select-selected-option"),
    );
    await userEvent.click(
      screen.getByRole("listitem", {
        name: translate.t("NotRequested"),
      }),
    );

    await waitFor((): void => {
      expect(
        screen.queryByText("https://example.com/tests"),
      ).toBeInTheDocument();
    });

    expect(
      screen.queryByText("table.noDataIndication"),
    ).not.toBeInTheDocument();

    await userEvent.click(
      screen.getByRole("button", { hidden: true, name: "filter.cancel" }),
    );

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(totalRows);
    });
  });

  it("should render container with additional permissions", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      {
        action:
          "integrates_api_mutations_confirm_vulnerabilities_zero_risk_mutate",
      },
      { action: "integrates_api_resolvers_vulnerability_hacker_resolve" },
      {
        action: "integrates_api_resolvers_finding_zero_risk_connection_resolve",
      },
      { action: "integrates_api_resolvers_finding_drafts_connection_resolve" },
    ]);

    render(
      <Wrapper
        mockServices={
          new PureAbility([
            { action: "can_report_vulnerabilities" },
            { action: "can_request_zero_risk" },
          ])
        }
        mockedPermissions={mockedPermissions}
      />,
      {
        memoryRouter: {
          initialEntries: [
            "/orgs/testorg/groups/testgroup/vulns/422286126/locations",
          ],
        },
        mocks: [mocksQueryFindingVulns],
      },
    );

    await waitFor((): void => {
      expect(screen.queryAllByRole("button")).toHaveLength(2);
    });

    expect(
      screen.queryByRole("combobox", { name: "treatment" }),
    ).not.toBeInTheDocument();

    await waitFor((): void => {
      expect(
        screen.queryByRole("button", {
          name: "searchFindings.tabVuln.buttons.handleAcceptance",
        }),
      ).toBeInTheDocument();
    });
    await userEvent.click(
      screen.getByRole("button", {
        name: "searchFindings.tabVuln.buttons.handleAcceptance",
      }),
    );
    await waitFor((): void => {
      expect(
        screen.queryByText(
          "searchFindings.tabDescription.handleAcceptanceModal.title",
        ),
      ).toBeInTheDocument();
    });

    expect(
      screen.getByTestId(`treatment-type-selected-option`),
    ).toBeInTheDocument();
  });

  it("should render container checking severity_score", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      {
        action:
          "integrates_api_mutations_confirm_vulnerabilities_zero_risk_mutate",
      },
      { action: "integrates_api_resolvers_vulnerability_hacker_resolve" },
      {
        action: "integrates_api_resolvers_finding_zero_risk_connection_resolve",
      },
      { action: "integrates_api_resolvers_finding_drafts_connection_resolve" },
    ]);

    render(
      <Wrapper
        mockServices={
          new PureAbility([
            { action: "can_report_vulnerabilities" },
            { action: "can_request_zero_risk" },
          ])
        }
        mockedPermissions={mockedPermissions}
      />,
      {
        memoryRouter: {
          initialEntries: [
            "/orgs/testorg/groups/testgroup/vulns/422286126/locations",
          ],
        },
        mocks: [
          mocksQueryFindingVulns,
          graphqlMocked.query(
            GET_FINDING_INFO,
            (): StrictResponse<IErrorMessage | { data: GetFindingInfo }> => {
              return HttpResponse.json({
                data: {
                  finding: {
                    __typename: "Finding",
                    assignees: [],
                    id: "422286126",
                    locations: [],
                    releaseDate: "2019-05-08",
                    remediated: false,
                    status: "VULNERABLE",
                    verified: false,
                  },
                },
              });
            },
          ),
        ],
      },
    );

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(totalRows);
    });

    expect(
      within(screen.queryAllByRole("row")[1]).getAllByRole("cell")[4]
        .textContent,
    ).toBe("4.2Medium");

    expect(screen.queryByText("Severity (v3.1)")).toBeInTheDocument();

    expect(
      screen.queryByText(
        translate.t("searchFindings.tabVuln.vulnTable.priority"),
      ),
    ).toBeInTheDocument();

    expect(
      screen.getByRole("checkbox", { name: "cvssToggle" }),
    ).not.toBeChecked();

    await userEvent.click(screen.getByRole("checkbox", { name: "cvssToggle" }));

    await waitFor((): void => {
      expect(
        screen.getByRole("checkbox", { name: "cvssToggle" }),
      ).toBeChecked();
    });

    expect(screen.queryByText("Severity (v3.1)")).not.toBeInTheDocument();
    expect(screen.queryByText("Severity (v4.0)")).toBeInTheDocument();

    await waitFor((): void => {
      expect(
        within(screen.queryAllByRole("row")[1]).getAllByRole("cell")[4]
          .textContent,
      ).toBe("4.1Medium");
    });

    await userEvent.click(screen.getByRole("checkbox", { name: "cvssToggle" }));

    await waitFor((): void => {
      expect(
        screen.getByRole("checkbox", { name: "cvssToggle" }),
      ).not.toBeChecked();
    });

    await waitFor((): void => {
      expect(
        within(screen.queryAllByRole("row")[1]).getAllByRole("cell")[4]
          .textContent,
      ).toBe("4.2Medium");
    });
  });

  it("should render container and warning request_button flow", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_resolvers_finding_drafts_connection_resolve" },
      { action: "integrates_api_resolvers_vulnerability_hacker_resolve" },
      {
        action:
          "integrates_api_mutations_request_vulnerabilities_verification_mutate",
      },
      {
        action: "integrates_api_resolvers_finding_zero_risk_connection_resolve",
      },
      {
        action:
          "integrates_api_mutations_update_vulnerabilities_treatment_mutate",
      },
    ]);
    const mockedServices = new PureAbility<string>([
      { action: "is_continuous" },
      { action: "can_report_vulnerabilities" },
    ]);
    render(
      <Wrapper
        mockServices={mockedServices}
        mockedPermissions={mockedPermissions}
      />,
      {
        memoryRouter: {
          initialEntries: [
            "/orgs/testorg/groups/testgroup/vulns/422286126/locations",
          ],
        },
        mocks: [mocksQueryFindingVulns],
      },
    );

    await waitFor((): void => {
      expect(screen.queryAllByRole("button")).toHaveLength(4);
    });
    await waitFor((): void => {
      expect(
        screen.queryByText("https://example.com/tests"),
      ).toBeInTheDocument();
    });

    const cell = screen.getByText("https://example.com/tests");
    const row = cell.closest("tr");
    const checkbox = within(row as HTMLElement).getByRole("checkbox");

    await userEvent.click(checkbox);

    const secondCell = screen.getByText("test/https://example.com/location");
    const secondRow = secondCell.closest("tr");
    const secondCheckbox = within(secondRow as HTMLElement).getByRole(
      "checkbox",
    );

    await userEvent.click(secondCheckbox);

    expect(
      screen.queryByText(
        "searchFindings.tabDescription.remediationModal.titleRequest",
      ),
    ).not.toBeInTheDocument();
    expect(
      screen.getByText("searchFindings.tabVuln.buttons.edit"),
    ).toBeInTheDocument();

    await userEvent.click(
      screen.getByRole("button", {
        name: "searchFindings.tabVuln.buttons.selectReattack",
      }),
    );

    await waitFor((): void => {
      expect(msgWarning).toHaveBeenCalledWith(
        "searchFindings.tabVuln.errors.badPlan",
      );
    });

    expect(
      within(
        screen
          .getByText("https://example.com/tests")
          .closest("tr") as HTMLElement,
      ).getByRole("checkbox"),
    ).not.toBeChecked();

    expect(
      within(
        screen
          .getByText("test/https://example.com/location")
          .closest("tr") as HTMLElement,
      ).getByRole("checkbox"),
    ).toBeChecked();

    expect(
      screen.getByText(
        "searchFindings.tabDescription.remediationModal.titleRequest",
      ),
    ).toBeInTheDocument();

    expect(
      screen.queryByText("searchFindings.tabVuln.buttons.edit"),
    ).not.toBeInTheDocument();

    await userEvent.click(
      screen.getByRole("button", {
        name: "searchFindings.tabVuln.buttons.cancel",
      }),
    );
  });

  it("should render container and test request_button flow", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_resolvers_finding_drafts_connection_resolve" },
      { action: "integrates_api_resolvers_vulnerability_hacker_resolve" },
      {
        action:
          "integrates_api_mutations_request_vulnerabilities_verification_mutate",
      },
      {
        action: "integrates_api_resolvers_finding_zero_risk_connection_resolve",
      },
      {
        action:
          "integrates_api_mutations_update_vulnerabilities_treatment_mutate",
      },
    ]);
    const mockedServices = new PureAbility<string>([
      { action: "is_continuous" },
      { action: "has_advanced" },
      { action: "can_report_vulnerabilities" },
    ]);
    render(
      <Wrapper
        mockServices={mockedServices}
        mockedPermissions={mockedPermissions}
      />,
      {
        memoryRouter: {
          initialEntries: [
            "/orgs/testorg/groups/testgroup/vulns/422286126/locations",
          ],
        },
        mocks: [mocksQueryFindingVulns],
      },
    );

    await waitFor((): void => {
      expect(screen.queryAllByRole("button")).toHaveLength(4);
    });

    await waitFor(async (): Promise<void> => {
      const cell = screen.getByText("https://example.com/tests");
      const row = cell.closest("tr");
      const checkbox = within(row as HTMLElement).getByRole("checkbox");

      await userEvent.click(checkbox);
    });

    expect(
      screen.queryByText(
        "searchFindings.tabDescription.remediationModal.titleRequest",
      ),
    ).not.toBeInTheDocument();
    expect(
      screen.getByText("searchFindings.tabVuln.buttons.edit"),
    ).toBeInTheDocument();

    await userEvent.click(
      screen.getByRole("button", {
        name: "searchFindings.tabVuln.buttons.selectReattack",
      }),
    );

    await waitFor((): void => {
      expect(
        screen.getByText(
          "searchFindings.tabDescription.remediationModal.titleRequest",
        ),
      ).toBeInTheDocument();
    });

    expect(
      screen.queryByText("searchFindings.tabVuln.buttons.edit"),
    ).not.toBeInTheDocument();

    await userEvent.click(
      screen.getByRole("button", {
        name: "searchFindings.tabVuln.buttons.cancel",
      }),
    );
  }, 60000);

  it("should filter vulns by location", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      {
        action:
          "integrates_api_mutations_confirm_vulnerabilities_zero_risk_mutate",
      },
      { action: "integrates_api_resolvers_vulnerability_hacker_resolve" },
      {
        action: "integrates_api_resolvers_finding_zero_risk_connection_resolve",
      },
      { action: "integrates_api_resolvers_finding_drafts_connection_resolve" },
    ]);

    render(
      <Wrapper
        mockServices={
          new PureAbility([{ action: "can_report_vulnerabilities" }])
        }
        mockedPermissions={mockedPermissions}
      />,
      {
        memoryRouter: {
          initialEntries: [
            "/orgs/testorg/groups/testgroup/vulns/422286126/locations",
          ],
        },
        mocks: [mocksQueryFindingVulns],
      },
    );

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(totalRows);
    });

    expect(
      screen.getByRole("button", { name: "filter.title" }),
    ).toBeInTheDocument();

    await userEvent.click(screen.getByRole("button", { name: "filter.title" }));

    expect(
      screen.getByRole("button", { hidden: true, name: "filter.cancel" }),
    ).toBeInTheDocument();

    await userEvent.click(
      screen.getByRole("button", { hidden: true, name: "filter.cancel" }),
    );

    await waitFor((): void => {
      expect(
        screen.queryByText("https://example.com/inputs"),
      ).toBeInTheDocument();
    });

    await userEvent.type(
      screen.getByRole("combobox", { hidden: true, name: "where" }),
      "https://example.com/location",
    );

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(2);
    });

    expect(screen.queryByText("filter.applied")).toBeInTheDocument();

    expect(
      screen.queryAllByText("Location")[0]?.nextElementSibling?.textContent,
    ).toBe("https://example.com/location");

    expect(
      screen.queryByText("https://example.com/inputs"),
    ).not.toBeInTheDocument();

    expect(
      screen.queryByText("test/https://example.com/location"),
    ).toBeInTheDocument();

    await userEvent.click(
      screen.getByRole("button", { hidden: true, name: "filter.cancel" }),
    );

    await userEvent.type(
      screen.getByRole("combobox", { hidden: true, name: "where" }),
      "https://example.com/notData",
    );

    await waitFor((): void => {
      expect(screen.queryByText("table.noDataIndication")).toBeInTheDocument();
    });

    await userEvent.click(
      screen.getByRole("button", { hidden: true, name: "filter.cancel" }),
    );

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(totalRows);
    });
  });

  it("should filter vulns by reported date", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_resolvers_vulnerability_hacker_resolve" },
      {
        action: "integrates_api_resolvers_finding_zero_risk_connection_resolve",
      },
      { action: "integrates_api_resolvers_finding_drafts_connection_resolve" },
    ]);

    render(
      <Wrapper
        mockServices={
          new PureAbility([{ action: "can_report_vulnerabilities" }])
        }
        mockedPermissions={mockedPermissions}
      />,
      {
        memoryRouter: {
          initialEntries: [
            "/orgs/testorg/groups/testgroup/vulns/422286126/locations",
          ],
        },
        mocks: [mocksQueryFindingVulns],
      },
    );

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(totalRows);
    });

    expect(
      screen.getByRole("button", { name: "filter.title" }),
    ).toBeInTheDocument();

    await userEvent.click(screen.getByRole("button", { name: "filter.title" }));

    await waitFor((): void => {
      expect(
        screen.getByRole("button", { name: "filter.cancel" }),
      ).toBeInTheDocument();
    });

    await userEvent.click(
      screen.getByRole("button", { name: "filter.cancel" }),
    );

    await waitFor((): void => {
      expect(
        screen.queryByRole("spinbutton", { name: /month, Start Date/u }),
      ).toBeInTheDocument();
    });

    await userEvent.type(
      screen.getByRole("spinbutton", { name: /month, Start Date/u }),
      "07",
    );

    await userEvent.type(
      screen.getByRole("spinbutton", { name: /day, Start Date/u }),
      "06",
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: /year, Start Date/u }),
      "2019",
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: /month, End Date/u }),
      "07",
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: /day, End Date/u }),
      "07",
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: /year, End Date/u }),
      "2019",
    );

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(3);
    });
    await userEvent.click(
      screen.getByRole("button", { hidden: true, name: "filter.cancel" }),
    );
  });

  it("should filter vulns by status and tags", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      {
        action:
          "integrates_api_mutations_confirm_vulnerabilities_zero_risk_mutate",
      },
      { action: "integrates_api_resolvers_vulnerability_hacker_resolve" },
      {
        action: "integrates_api_resolvers_finding_zero_risk_connection_resolve",
      },
      { action: "integrates_api_resolvers_finding_drafts_connection_resolve" },
    ]);

    render(
      <Wrapper
        mockServices={
          new PureAbility([{ action: "can_report_vulnerabilities" }])
        }
        mockedPermissions={mockedPermissions}
      />,
      {
        memoryRouter: {
          initialEntries: [
            "/orgs/testorg/groups/testgroup/vulns/422286126/locations",
          ],
        },
        mocks: [mocksQueryFindingVulns],
      },
    );

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(totalRows);
    });

    expect(
      screen.getByRole("button", { name: "filter.title" }),
    ).toBeInTheDocument();

    await userEvent.click(screen.getByRole("button", { name: "filter.title" }));

    expect(
      screen.getByRole("button", { hidden: true, name: "filter.cancel" }),
    ).toBeInTheDocument();

    await userEvent.click(
      screen.getByRole("button", { hidden: true, name: "filter.cancel" }),
    );

    await userEvent.click(
      screen.getByTestId("currentState-select-selected-option"),
    );
    await userEvent.click(
      screen.getByRole("listitem", {
        name: translate.t("REJECTED"),
      }),
    );

    expect(screen.queryByText("filter.applied")).toBeInTheDocument();

    expect(
      screen.queryAllByText(
        translate.t("searchFindings.tabVuln.vulnTable.status"),
      )[0]?.nextElementSibling?.textContent,
    ).toBe(translate.t("searchFindings.tabVuln.rejected"));

    await waitFor((): void => {
      expect(screen.queryByText("table.noDataIndication")).toBeInTheDocument();
    });

    await userEvent.click(
      screen.getByTestId("currentState-select-selected-option"),
    );
    await userEvent.click(
      screen.getByRole("listitem", {
        name: translate.t("SAFE"),
      }),
    );

    expect(
      screen.queryAllByText(
        translate.t("searchFindings.tabVuln.vulnTable.status"),
      )[0]?.nextElementSibling?.textContent,
    ).toBe(translate.t("searchFindings.tabVuln.closed"));
    expect(
      screen.queryByText("table.noDataIndication"),
    ).not.toBeInTheDocument();

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(3);
    });

    await userEvent.click(
      screen.getByRole("button", { hidden: true, name: "filter.cancel" }),
    );

    expect(screen.queryByText("filter.applied")).not.toBeInTheDocument();

    await userEvent.type(screen.getByRole("textbox", { name: "tag" }), "tag-5");
    await userEvent.tab();

    expect(screen.queryByText("filter.applied")).toBeInTheDocument();

    expect(
      screen.queryAllByText(
        translate.t("searchFindings.tabVuln.vulnTable.tags"),
      )[0]?.nextElementSibling?.textContent,
    ).toBe("tag-5");

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(3);
    });
    await userEvent.click(
      screen.getByRole("button", { hidden: true, name: "filter.cancel" }),
    );
  });

  it("should filter vulns by assignees and treatment", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      {
        action:
          "integrates_api_mutations_confirm_vulnerabilities_zero_risk_mutate",
      },
      { action: "integrates_api_resolvers_vulnerability_hacker_resolve" },
      {
        action: "integrates_api_resolvers_finding_zero_risk_connection_resolve",
      },
      { action: "integrates_api_resolvers_finding_drafts_connection_resolve" },
    ]);

    render(
      <Wrapper
        mockServices={
          new PureAbility([{ action: "can_report_vulnerabilities" }])
        }
        mockedPermissions={mockedPermissions}
      />,
      {
        memoryRouter: {
          initialEntries: [
            "/orgs/testorg/groups/testgroup/vulns/422286126/locations",
          ],
        },
        mocks: [mocksQueryFindingVulns],
      },
    );

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(totalRows);
    });

    expect(
      screen.getByRole("button", { name: "filter.title" }),
    ).toBeInTheDocument();

    await userEvent.click(screen.getByRole("button", { name: "filter.title" }));

    expect(
      screen.getByRole("button", { hidden: true, name: "filter.cancel" }),
    ).toBeInTheDocument();

    await userEvent.click(
      screen.getByRole("button", { hidden: true, name: "filter.cancel" }),
    );

    await userEvent.click(
      screen.getByTestId("treatmentAssigned-select-selected-option"),
    );
    await userEvent.click(
      screen.getByRole("listitem", { name: "usertreatment@test.test" }),
    );

    expect(screen.queryByText("filter.applied")).toBeInTheDocument();
    expect(
      screen.queryAllByText("Assignees")[0]?.nextElementSibling?.textContent,
    ).toBe("usertreatment@test.test");

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(3);
    });

    await userEvent.click(
      screen.getByTestId("treatment-select-selected-option"),
    );
    await userEvent.click(
      screen.getByRole("listitem", {
        name: "untreated",
      }),
    );

    expect(screen.queryByText("filter.applied")).toBeInTheDocument();

    expect(
      screen.queryAllByText(
        translate.t("searchFindings.tabVuln.vulnTable.treatment"),
      )[0]?.nextElementSibling?.textContent,
    ).toBe("Untreated");

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(2);
    });

    await userEvent.click(
      screen.getByRole("button", { hidden: true, name: "filter.cancel" }),
    );
  });

  it("should render container and test verify_button flow", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_resolvers_vulnerability_hacker_resolve" },
      {
        action: "integrates_api_resolvers_finding_zero_risk_connection_resolve",
      },
      {
        action:
          "integrates_api_mutations_verify_vulnerabilities_request_mutate",
      },
      { action: "integrates_api_resolvers_finding_drafts_connection_resolve" },
      {
        action:
          "integrates_api_mutations_update_vulnerabilities_treatment_mutate",
      },
      { action: "integrates_api_mutations_remove_vulnerability_mutate" },
    ]);

    render(
      <Wrapper
        mockServices={
          new PureAbility([{ action: "can_report_vulnerabilities" }])
        }
        mockedPermissions={mockedPermissions}
      />,
      {
        memoryRouter: {
          initialEntries: [
            "/orgs/testorg/groups/testgroup/vulns/422286126/locations",
          ],
        },
        mocks: [mocksQueryFindingVulns],
      },
    );

    await waitFor((): void => {
      expect(screen.queryAllByRole("button")).toHaveLength(4);
    });

    const rowCheckboxDisabled =
      screen
        .queryAllByRole("row")
        .slice(1)
        .find(
          (row): boolean =>
            within(row).getAllByRole("cell")[1].textContent ===
            "https://example.com/tests",
        ) ?? ((<div />) as unknown as HTMLElement);

    expect(
      screen.queryByText("searchFindings.tabVuln.buttons.edit"),
    ).toBeInTheDocument();
    expect(rowCheckboxDisabled).toBeInTheDocument();
    expect(
      within(within(rowCheckboxDisabled).getAllByRole("cell")[0]).getByRole(
        "checkbox",
      ),
    ).not.toBeDisabled();
    expect(
      screen.queryByText(
        "searchFindings.tabDescription.remediationModal.titleObservations",
      ),
    ).not.toBeInTheDocument();

    await waitFor((): void => {
      expect(
        screen.getByRole("button", {
          name: "searchFindings.tabVuln.additionalOptions",
        }),
      ).toBeInTheDocument();
    });

    await userEvent.click(
      screen.getByRole("button", {
        name: "searchFindings.tabVuln.additionalOptions",
      }),
    );
    await userEvent.click(
      screen.getByText("searchFindings.tabDescription.markVerified.text"),
    );
    await waitFor((): void => {
      expect(
        within(within(rowCheckboxDisabled).getAllByRole("cell")[0]).getByRole(
          "checkbox",
        ),
      ).toBeDisabled();
    });

    expect(
      screen.queryByText("searchFindings.tabVuln.buttons.edit"),
    ).not.toBeInTheDocument();

    expect(screen.getByText("New")).toBeInTheDocument();

    const rowCheckbox =
      screen
        .queryAllByRole("row")
        .slice(1)
        .find(
          (row): boolean =>
            within(row).getAllByRole("cell")[1].textContent ===
            "https://example.com/inputsNew",
        ) ?? ((<div />) as unknown as HTMLElement);

    expect(rowCheckbox).toBeInTheDocument();
    expect(
      within(within(rowCheckbox).getAllByRole("cell")[0]).getByRole("checkbox"),
    ).not.toBeDisabled();

    await userEvent.click(
      within(rowCheckbox).getAllByRole("cell")[0].firstChild as HTMLElement,
    );
    await userEvent.click(
      screen.getByText("searchFindings.tabDescription.markVerified.text"),
    );
    await userEvent.click(
      screen.getByText("searchFindings.tabDescription.cancelVerified"),
    );
  });

  it("should refecth vulns when hasNextPage", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      {
        action:
          "integrates_api_mutations_confirm_vulnerabilities_zero_risk_mutate",
      },
      { action: "integrates_api_resolvers_vulnerability_hacker_resolve" },
      {
        action: "integrates_api_resolvers_finding_zero_risk_connection_resolve",
      },
      { action: "integrates_api_resolvers_finding_drafts_connection_resolve" },
    ]);

    const mocksQueryFindingVulnsLocal = graphqlMocked.query(
      GET_FINDING_VULNS_QUERIES,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: GetFindingVulnsQueries }> => {
        const {
          afterNZR,
          canRetrieveHacker,
          findingId,
          canRetrieveDrafts,
          first,
        } = variables;
        if (
          afterNZR === undefined &&
          canRetrieveHacker === true &&
          findingId === "422286126" &&
          canRetrieveDrafts &&
          first === 200
        ) {
          return HttpResponse.json({
            data: {
              __typename: "Query",
              ...{
                finding: {
                  __typename: "Finding",
                  draftsConnection: {
                    edges: [],
                    pageInfo: {
                      endCursor: "test-cursor=",
                      hasNextPage: false,
                    },
                  },
                  id: "422286126",
                  vulnerabilitiesConnection: {
                    edges: edges.slice(0, 2),
                    pageInfo: {
                      endCursor: "test-cursor=",
                      hasNextPage: true,
                    },
                  },
                  zeroRiskConnection: {
                    edges: zeroRiskConnectionEdgesMock,
                    pageInfo: {
                      endCursor: "test-cursor=",
                      hasNextPage: false,
                    },
                  },
                },
              },
            },
          });
        }
        if (
          afterNZR === "test-cursor=" &&
          canRetrieveHacker === true &&
          findingId === "422286126" &&
          canRetrieveDrafts &&
          first === 800
        ) {
          return HttpResponse.json({
            data: {
              __typename: "Query",
              ...{
                finding: {
                  __typename: "Finding",
                  draftsConnection: {
                    edges: [],
                    pageInfo: {
                      endCursor: "test-cursor=",
                      hasNextPage: false,
                    },
                  },
                  id: "422286126",
                  vulnerabilitiesConnection: {
                    edges: edges.slice(2),
                    pageInfo: {
                      endCursor: "test-cursor=",
                      hasNextPage: false,
                    },
                  },
                  zeroRiskConnection: {
                    edges: zeroRiskConnectionEdgesMock,
                    pageInfo: {
                      endCursor: "test-cursor=",
                      hasNextPage: false,
                    },
                  },
                },
              },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error getting vulnerabilities")],
        });
      },
    );

    render(
      <Wrapper
        mockServices={
          new PureAbility([{ action: "can_report_vulnerabilities" }])
        }
        mockedPermissions={mockedPermissions}
      />,
      {
        memoryRouter: {
          initialEntries: [
            "/orgs/testorg/groups/testgroup/vulns/422286126/locations",
          ],
        },
        mocks: [mocksQueryFindingVulnsLocal],
      },
    );

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(totalRows);
    });

    await userEvent.click(screen.getByRole("button", { name: "filter.title" }));

    await userEvent.click(
      screen.getByTestId("treatmentAssigned-select-selected-option"),
    );
    await waitFor((): void => {
      expect(screen.getAllByRole("listitem", { name: "" })).toHaveLength(6);
    });
    await userEvent.click(
      screen.getByRole("listitem", { name: "usertreatment@test.test" }),
    );

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(3);
    });

    await userEvent.click(
      screen.getByRole("button", { hidden: true, name: "filter.cancel" }),
    );

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(totalRows);
    });
  });

  it("should resubmit vulnerabilities", async (): Promise<void> => {
    expect.hasAssertions();

    const mocksQueryResubmitVulns = graphqlMocked.query(
      GET_FINDING_VULNS_QUERIES,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: GetFindingVulnsQueries }> => {
        const { canRetrieveHacker, findingId, canRetrieveDrafts, first } =
          variables;
        if (
          canRetrieveHacker === true &&
          findingId === "422286126" &&
          canRetrieveDrafts &&
          first === 200
        ) {
          return HttpResponse.json({
            data: {
              __typename: "Query",
              ...{
                finding: {
                  __typename: "Finding",
                  draftsConnection: {
                    edges: [
                      {
                        node: {
                          __typename: "Vulnerability",
                          advisories: null,
                          closingDate: null,
                          customSeverity: 0,
                          externalBugTrackingSystem: null,
                          findingId: "422286126",
                          groupName: "testgroup",
                          hacker: "hacker1@test.test",
                          id: "6b855957-9b94-485f-ad43-82b5f184e826",
                          lastEditedBy: "test@test.test",
                          lastStateDate: "2019-07-05 09:56:40",
                          lastTreatmentDate: "2019-07-05 09:56:40",
                          lastVerificationDate: null,
                          priority: 0,
                          remediated: true,
                          reportDate: dayjs().format("YYYY-MM-DD hh:mm:ss A"),
                          root: {
                            __typename: "GitRoot",
                            branch: "master",
                            criticality: RootCriticality.Low,
                          },
                          rootNickname: "",
                          severityTemporalScore: 6.7,
                          severityThreatScore: 6.6,
                          severityVector:
                            "AV:A/AC:H/PR:L/UI:R/S:C/C:L/I:L/A:L/E:F/RL:O/RC:U",
                          severityVectorV4:
                            "CVSS:4.0/AV:A/AC:H/PR:L/UI:P/VC:L/VI:L/VA:L/SC:L/SI:L/SA:L/E:A",
                          source: "asm",
                          specific: "specific-4",
                          state: VulnerabilityState.Rejected,
                          stateReasons: null,
                          stream: "home > blog > articulo",
                          tag: "tag-1, tag-2",
                          technique: Technique.Dast,
                          treatmentAcceptanceDate: "",
                          treatmentAcceptanceStatus: "",
                          treatmentAssigned: "assigned-user-1",
                          treatmentJustification: "test progress justification",
                          treatmentStatus: VulnerabilityTreatment.InProgress,
                          treatmentUser: "usertreatment@test.test",
                          verification: null,
                          verificationJustification: null,
                          vulnerabilityType: "inputs",
                          where: "https://example.com/inputs-4",
                          zeroRisk: null,
                        },
                      },
                      {
                        node: {
                          __typename: "Vulnerability",
                          advisories: null,
                          closingDate: null,
                          customSeverity: 0,
                          externalBugTrackingSystem: null,
                          findingId: "422286126",
                          groupName: "testgroup",
                          hacker: "hacker2@test.test",
                          id: "6a9904a0-7c83-4444-8bf9-61b53b865129",
                          lastEditedBy: "test@test.test",
                          lastStateDate: "2019-07-05 09:56:40",
                          lastTreatmentDate: "2019-07-05 09:56:40",
                          lastVerificationDate: null,
                          priority: 0,
                          remediated: false,
                          reportDate: "2020-07-05 09:56:40",
                          root: {
                            __typename: "GitRoot",
                            branch: "master",
                            criticality: RootCriticality.Low,
                          },
                          rootNickname: "https:",
                          severityTemporalScore: 6.7,
                          severityThreatScore: 6.8,
                          severityVector:
                            "AV:A/AC:H/PR:L/UI:R/S:C/C:L/I:L/A:L/E:F/RL:O/RC:U",
                          severityVectorV4:
                            "CVSS:4.0/AV:A/AC:H/PR:L/UI:P/VC:L/VI:L/VA:L/SC:L/SI:L/SA:L/E:A",
                          source: "asm",
                          specific: "specific-5",
                          state: VulnerabilityState.Submitted,
                          stateReasons: null,
                          stream: null,
                          tag: "tag-3",
                          technique: Technique.Dast,
                          treatmentAcceptanceDate: "",
                          treatmentAcceptanceStatus: "",
                          treatmentAssigned: "assigned-user-1",
                          treatmentJustification: "test progress justification",
                          treatmentStatus: VulnerabilityTreatment.InProgress,
                          treatmentUser: "usertreatment@test.test",
                          verification: null,
                          verificationJustification: null,
                          vulnerabilityType: "inputs",
                          where: "https://example.com/tests-5",
                          zeroRisk: null,
                        },
                      },
                    ],
                    pageInfo: {
                      endCursor: "test-cursor=",
                      hasNextPage: false,
                    },
                  },
                  id: "422286126",
                  vulnerabilitiesConnection: {
                    edges: [
                      {
                        node: {
                          __typename: "Vulnerability",
                          advisories: null,
                          closingDate: null,
                          customSeverity: 0,
                          externalBugTrackingSystem: null,
                          findingId: "422286126",
                          groupName: "testgroup",
                          hacker: "hacker1@test.test",
                          id: "89521e9a-b1a3-4047-a16e-15d530dc1340",
                          lastEditedBy: "test@test.test",
                          lastStateDate: "2019-07-05 09:56:40",
                          lastTreatmentDate: "2019-07-05 09:56:40",
                          lastVerificationDate: null,
                          priority: 0,
                          remediated: true,
                          reportDate: dayjs().format("YYYY-MM-DD hh:mm:ss A"),
                          root: {
                            __typename: "GitRoot",
                            branch: "master",
                            criticality: RootCriticality.Low,
                          },
                          rootNickname: "",
                          severityTemporalScore: 6.7,
                          severityThreatScore: 6.4,
                          severityVector:
                            "AV:A/AC:H/PR:L/UI:R/S:C/C:L/I:L/A:L/E:F/RL:O/RC:U",
                          severityVectorV4:
                            "CVSS:4.0/AV:A/AC:H/PR:L/UI:P/VC:L/VI:L/VA:L/SC:L/SI:L/SA:L/E:A",
                          source: "asm",
                          specific: "specific-1",
                          state: VulnerabilityState.Vulnerable,
                          stateReasons: null,
                          stream: "home > blog > articulo",
                          tag: "tag-1, tag-2",
                          technique: Technique.Sast,
                          treatmentAcceptanceDate: "",
                          treatmentAcceptanceStatus: "",
                          treatmentAssigned: "assigned-user-1",
                          treatmentJustification: "test progress justification",
                          treatmentStatus: VulnerabilityTreatment.InProgress,
                          treatmentUser: "usertreatment@test.test",
                          verification: "Requested",
                          verificationJustification: null,
                          vulnerabilityType: "inputs",
                          where: "https://example.com/inputs",
                          zeroRisk: null,
                        },
                      },
                      {
                        node: {
                          __typename: "Vulnerability",
                          advisories: null,
                          closingDate: null,
                          customSeverity: 0,
                          externalBugTrackingSystem: null,
                          findingId: "422286126",
                          groupName: "testgroup",
                          hacker: "hacker2@test.test",
                          id: "6903f3e4-a8ee-4a5d-ac38-fb738ec7e540",
                          lastEditedBy: "test@test.test",
                          lastStateDate: "2019-07-05 09:56:40",
                          lastTreatmentDate: "2019-07-05 09:56:40",
                          lastVerificationDate: null,
                          priority: 0,
                          remediated: false,
                          reportDate: "2020-07-05 09:56:40",
                          root: {
                            __typename: "GitRoot",
                            branch: "master",
                            criticality: RootCriticality.Low,
                          },
                          rootNickname: "https:",
                          severityTemporalScore: 6.7,
                          severityThreatScore: 6.9,
                          severityVector:
                            "AV:A/AC:H/PR:L/UI:R/S:C/C:L/I:L/A:L/E:F/RL:O/RC:U",
                          severityVectorV4:
                            "CVSS:4.0/AV:A/AC:H/PR:L/UI:P/VC:L/VI:L/VA:L/SC:L/SI:L/SA:L/E:A",
                          source: "asm",
                          specific: "specific-3",
                          state: VulnerabilityState.Vulnerable,
                          stateReasons: null,
                          stream: null,
                          tag: "tag-3",
                          technique: Technique.Sast,
                          treatmentAcceptanceDate: "",
                          treatmentAcceptanceStatus: "",
                          treatmentAssigned: "assigned-user-1",
                          treatmentJustification: "test progress justification",
                          treatmentStatus: VulnerabilityTreatment.InProgress,
                          treatmentUser: "usertreatment@test.test",
                          verification: null,
                          verificationJustification: null,
                          vulnerabilityType: "lines",
                          where: "https://example.com/tests",
                          zeroRisk: null,
                        },
                      },
                      {
                        node: {
                          __typename: "Vulnerability",
                          advisories: null,
                          closingDate: null,
                          customSeverity: 0,
                          externalBugTrackingSystem: null,
                          findingId: "422286126",
                          groupName: "testgroup",
                          hacker: "hacker@test.test",
                          id: "7903f3e4-a8ee-4a5d-ac38-fb738ec7e540",
                          lastEditedBy: "test@test.test",
                          lastStateDate: "2019-07-05 09:56:40",
                          lastTreatmentDate: "2019-07-05 09:56:40",
                          lastVerificationDate: null,
                          priority: 0,
                          remediated: false,
                          reportDate: "2019-07-05 09:56:40",
                          root: {
                            __typename: "GitRoot",
                            branch: "master",
                            criticality: RootCriticality.Low,
                          },
                          rootNickname: "test",
                          severityTemporalScore: 6.7,
                          severityThreatScore: 6.8,
                          severityVector:
                            "AV:A/AC:H/PR:L/UI:R/S:C/C:L/I:L/A:L/E:F/RL:O/RC:U",
                          severityVectorV4:
                            "CVSS:4.0/AV:A/AC:H/PR:L/UI:P/VC:L/VI:L/VA:L/SC:L/SI:L/SA:L/E:A",
                          source: "asm",
                          specific: "specific-4",
                          state: VulnerabilityState.Vulnerable,
                          stateReasons: null,
                          stream: null,
                          tag: "tag-5",
                          technique: Technique.Sast,
                          treatmentAcceptanceDate: "",
                          treatmentAcceptanceStatus: "",
                          treatmentAssigned: "assigned-user-1",
                          treatmentJustification: "test progress justification",
                          treatmentStatus: VulnerabilityTreatment.InProgress,
                          treatmentUser: "usertreatment@test.test",
                          verification: null,
                          verificationJustification: null,
                          vulnerabilityType: "lines",
                          where: "https://example.com/location",
                          zeroRisk: null,
                        },
                      },
                      {
                        node: {
                          __typename: "Vulnerability",
                          advisories: null,
                          closingDate: null,
                          customSeverity: 0,
                          externalBugTrackingSystem: null,
                          findingId: "422286126",
                          groupName: "testgroup",
                          hacker: "hacker1@test.test",
                          id: "59521e9a-b1a3-4047-a16e-15d530dc1340",
                          lastEditedBy: "test@test.test",
                          lastStateDate: "2019-07-05 09:56:40",
                          lastTreatmentDate: "2019-07-05 09:56:40",
                          lastVerificationDate: null,
                          priority: 0,
                          remediated: true,
                          reportDate: "2019-07-06 09:56:40",
                          root: {
                            __typename: "GitRoot",
                            branch: "master",
                            criticality: RootCriticality.Low,
                          },
                          rootNickname: "",
                          severityTemporalScore: 6.7,
                          severityThreatScore: 6.9,
                          severityVector:
                            "AV:A/AC:H/PR:L/UI:R/S:C/C:L/I:L/A:L/E:F/RL:O/RC:U",
                          severityVectorV4:
                            "CVSS:4.0/AV:A/AC:H/PR:L/UI:P/VC:L/VI:L/VA:L/SC:L/SI:L/SA:L/E:A",
                          source: "asm",
                          specific: "specific-5",
                          state: VulnerabilityState.Submitted,
                          stateReasons: null,
                          stream: null,
                          tag: "tag-5, tag-6, tag-7",
                          technique: Technique.Sast,
                          treatmentAcceptanceDate: "",
                          treatmentAcceptanceStatus: "",
                          treatmentAssigned: "usertreatment@test.test",
                          treatmentJustification: "",
                          treatmentStatus: VulnerabilityTreatment.Untreated,
                          treatmentUser: "usertreatment@test.test",
                          verification: null,
                          verificationJustification: null,
                          vulnerabilityType: "inputs",
                          where: "https://example.com/status",
                          zeroRisk: null,
                        },
                      },
                      {
                        node: {
                          __typename: "Vulnerability",
                          advisories: null,
                          closingDate: null,
                          customSeverity: 0,
                          externalBugTrackingSystem: null,
                          findingId: "422286126",
                          groupName: "testgroup",
                          hacker: "hacker1@test.test",
                          id: "49521e9a-b1a3-4047-a16e-15d530dc1340",
                          lastEditedBy: "test@test.test",
                          lastStateDate: "2019-07-05 09:56:40",
                          lastTreatmentDate: "2019-07-05 09:56:40",
                          lastVerificationDate: null,
                          priority: 0,
                          remediated: true,
                          reportDate: "2019-07-07 09:56:40",
                          root: {
                            __typename: "GitRoot",
                            branch: "master",
                            criticality: RootCriticality.Low,
                          },
                          rootNickname: "",
                          severityTemporalScore: 6.7,
                          severityThreatScore: 6.5,
                          severityVector:
                            "AV:A/AC:H/PR:L/UI:R/S:C/C:L/I:L/A:L/E:F/RL:O/RC:U",
                          severityVectorV4:
                            "CVSS:4.0/AV:A/AC:H/PR:L/UI:P/VC:L/VI:L/VA:L/SC:L/SI:L/SA:L/E:A",
                          source: "asm",
                          specific: "specific-5",
                          state: VulnerabilityState.Safe,
                          stateReasons: null,
                          stream: null,
                          tag: "tag-6, tag-7",
                          technique: Technique.Sast,
                          treatmentAcceptanceDate: "",
                          treatmentAcceptanceStatus: "",
                          treatmentAssigned: "usertreatment@test.test",
                          treatmentJustification: "justification safe",
                          treatmentStatus: VulnerabilityTreatment.Accepted,
                          treatmentUser: "usertreatment@test.test",
                          verification: null,
                          verificationJustification: null,
                          vulnerabilityType: "ports",
                          where: "https://example.com/statusSafe",
                          zeroRisk: null,
                        },
                      },
                    ],
                    pageInfo: {
                      endCursor: "test-cursor=",
                      hasNextPage: false,
                    },
                  },
                },
              },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error getting vulnerabilities")],
        });
      },
    );
    const mockedMutation = graphqlMocked.mutation(
      RESUBMIT_VULNERABILITIES,
      ({
        variables,
      }): StrictResponse<
        IErrorMessage | { data: ResubmitVulnerabilitiesRequest }
      > => {
        const { findingId, vulnerabilities } = variables;
        if (
          isEqual(vulnerabilities, ["6b855957-9b94-485f-ad43-82b5f184e826"]) &&
          findingId === "422286126"
        ) {
          return HttpResponse.json({
            data: {
              resubmitVulnerabilities: { success: true },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error resubmitting vulnerabilities")],
        });
      },
    );
    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_resolvers_vulnerability_hacker_resolve" },
      {
        action: "integrates_api_resolvers_finding_zero_risk_connection_resolve",
      },
      {
        action:
          "integrates_api_mutations_verify_vulnerabilities_request_mutate",
      },
      { action: "integrates_api_resolvers_finding_drafts_connection_resolve" },
      { action: "integrates_api_mutations_resubmit_vulnerabilities_mutate" },
      {
        action:
          "integrates_api_mutations_update_vulnerabilities_treatment_mutate",
      },
    ]);

    render(
      <Wrapper
        mockServices={
          new PureAbility([{ action: "can_report_vulnerabilities" }])
        }
        mockedPermissions={mockedPermissions}
      />,
      {
        memoryRouter: {
          initialEntries: [
            "/orgs/testorg/groups/testgroup/vulns/422286126/locations",
          ],
        },
        mocks: [mockedMutation, mocksQueryResubmitVulns],
      },
    );
    await waitFor((): void => {
      expect(screen.getAllByRole("row")).toHaveLength(8);
    });
    await waitFor((): void => {
      expect(
        screen.queryByRole("button", {
          name: "searchFindings.tabVuln.additionalOptions",
        }),
      ).toBeInTheDocument();
    });

    await userEvent.click(
      screen.getByRole("button", {
        name: "searchFindings.tabVuln.additionalOptions",
      }),
    );

    expect(
      screen.getByRole("button", {
        name: /searchfindings\.tabvuln\.buttons\.resubmit/iu,
      }),
    ).toBeInTheDocument();

    await userEvent.click(screen.getByRole("button", { name: "filter.title" }));
    await waitFor((): void => {
      expect(screen.queryByText("filter.cancel")).toBeInTheDocument();
    });
    await userEvent.click(
      screen.getByRole("button", { hidden: true, name: "filter.cancel" }),
    );

    await waitFor((): void => {
      expect(
        screen.queryByText("https://example.com/inputs"),
      ).toBeInTheDocument();
    });

    const inputsCell = screen.getByText("https://example.com/inputs");

    expect(inputsCell).toBeInTheDocument();

    const inputsRow = inputsCell.closest("tr");

    expect(inputsRow).toBeInTheDocument();

    const inputsCheckbox = within(inputsRow as HTMLElement).getByRole(
      "checkbox",
    );

    await userEvent.click(inputsCheckbox);

    const inputs4Cell = screen.getByText(/https:\/\/example\.com\/inputs-4/iu);

    expect(inputs4Cell).toBeInTheDocument();

    const inputs4Row = inputs4Cell.closest("tr");

    expect(inputs4Row).toBeInTheDocument();

    const input4Checkbox = within(inputs4Row as HTMLElement).getByRole(
      "checkbox",
    );

    await userEvent.click(input4Checkbox);

    expect(inputsCheckbox).toBeEnabled();
    expect(input4Checkbox).toBeEnabled();

    await userEvent.click(
      screen.getByRole("button", {
        name: "searchFindings.tabVuln.additionalOptions",
      }),
    );
    await userEvent.click(
      screen.getByRole("button", {
        name: /searchfindings\.tabvuln\.buttons\.resubmit/iu,
      }),
    );
    await userEvent.click(
      screen.getByRole("button", {
        name: /searchfindings\.tabvuln\.buttons\.resubmit/iu,
      }),
    );

    expect(msgSuccess).toHaveBeenCalledWith(
      "Vulnerability has been submitted",
      "Correct!",
    );
  });
});
