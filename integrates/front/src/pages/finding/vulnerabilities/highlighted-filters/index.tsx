import { ComboBox } from "@fluidattacks/design";
import type { Table as ReactTable, RowData } from "@tanstack/react-table";
import { useCallback, useMemo } from "react";

import { StyledWrapper } from "./styles";

import type { IFindingInfoResponse } from "../hooks/types";
import { useFiltersStore } from "../hooks/use-filters-store";
import { Technique, VulnerabilityState } from "gql/graphql";
import { useAuthz } from "hooks/use-authz";
import { translate } from "utils/translations/translate";

interface IHighlightedFiltersProps<T extends RowData> {
  readonly table: ReactTable<T>;
  readonly finding: IFindingInfoResponse["data"]["finding"];
}

const TECHNIQUES = [
  {
    name: translate.t("searchFindings.tabVuln.technique.cspm"),
    value: "CSPM",
  },
  {
    name: translate.t("searchFindings.tabVuln.technique.dast"),
    value: "DAST",
  },
  {
    name: translate.t("searchFindings.tabVuln.technique.ptaas"),
    value: "PTAAS",
  },
  {
    name: translate.t("searchFindings.tabVuln.technique.re"),
    value: "RE",
  },
  {
    name: translate.t("searchFindings.tabVuln.technique.sast"),
    value: "SAST",
  },
  {
    name: translate.t("searchFindings.tabVuln.technique.sca"),
    value: "SCA",
  },
  {
    name: translate.t("searchFindings.tabVuln.technique.scr"),
    value: "SCR",
  },
];

const HighlightedFilters = <T extends RowData>({
  table,
  finding,
}: IHighlightedFiltersProps<T>): JSX.Element => {
  const filtersStore = useFiltersStore();
  const { can } = useAuthz();

  const canSeeDraft = can("see_draft_status");

  const locations = useMemo(
    (): { name: string; value: string }[] =>
      finding?.locations.map((value): { name: string; value: string } => ({
        name: value,
        value,
      })) ?? [],
    [finding],
  );

  const handleWhere = useCallback(
    (key: string): void => {
      const validValues = finding?.locations ?? [];
      if (validValues.includes(key)) {
        table.setPageIndex(0);
        filtersStore.setWhere?.(key);
      } else if (key === "") {
        table.setPageIndex(0);
        filtersStore.setWhere?.(undefined);
      }
    },
    [table, filtersStore, finding],
  );

  const handleState = useCallback(
    (key: string): void => {
      const validValues = Object.keys(VulnerabilityState).map((value): string =>
        value.toLowerCase(),
      );

      if (validValues.includes(key.toLowerCase())) {
        table.setPageIndex(0);
        filtersStore.setState?.([key as unknown as VulnerabilityState]);
      } else if (key === "") {
        table.setPageIndex(0);
        filtersStore.setState?.(undefined);
      }
    },
    [table, filtersStore],
  );

  const handleTechnique = useCallback(
    (key: string): void => {
      const validValues = Object.keys(Technique).map((value): string =>
        value.toLowerCase(),
      );

      if (validValues.includes(key.toLowerCase())) {
        table.setPageIndex(0);
        filtersStore.setTechnique?.([key as unknown as Technique]);
      } else if (key === "") {
        table.setPageIndex(0);
        filtersStore.setTechnique?.(undefined);
      }
    },
    [table, filtersStore],
  );

  return (
    <StyledWrapper>
      <ComboBox
        items={locations}
        name={"where"}
        onChange={handleWhere}
        placeholder={"Location"}
        value={filtersStore.where?.[0] ?? ""}
      />
      <ComboBox
        items={[
          { name: "Vulnerable", value: "VULNERABLE" },
          { name: "Safe", value: "SAFE" },
          ...(canSeeDraft
            ? [
                { name: "Submitted", value: "SUBMITTED" },
                { name: "Rejected", value: "REJECTED" },
              ]
            : []),
        ]}
        name={"state"}
        onChange={handleState}
        placeholder={"Status"}
        value={filtersStore.state?.[0] ?? ""}
      />
      <ComboBox
        items={TECHNIQUES}
        name={"technique"}
        onChange={handleTechnique}
        placeholder={"Technique"}
        value={filtersStore.technique?.[0] ?? ""}
      />
    </StyledWrapper>
  );
};

export { HighlightedFilters };
