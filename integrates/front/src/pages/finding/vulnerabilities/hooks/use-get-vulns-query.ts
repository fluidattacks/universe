import { useQuery } from "@apollo/client";
import { useMemo } from "react";

import { GET_VULNS } from "../queries";
import type { IVulnerabilityEdge } from "../types";
import type { TQueryHook } from "components/table/hooks/use-table";
import type { IVulnRowAttr } from "features/vulnerabilities/types";
import { formatVulnerabilitiesTreatment } from "features/vulnerabilities/utils";
import type { GetVulnsQueryVariables } from "gql/graphql";
import { useAudit } from "hooks/use-audit";
import type { TFlatten } from "utils/zustand";

type TVars = TFlatten<GetVulnsQueryVariables>;

type TVulnQueryHook = TQueryHook<IVulnRowAttr, TVars>;

const useGetVulnsQuery: TVulnQueryHook = ({
  findingId,
  first,
  filters,
  sortBy,
}): ReturnType<TVulnQueryHook> => {
  const { addAuditEvent } = useAudit();

  const { data, loading, error, fetchMore } = useQuery(GET_VULNS, {
    fetchPolicy: "cache-and-network",
    onCompleted: (): void => {
      addAuditEvent("Finding.Vulnerabilities", findingId);
    },
    variables: {
      after: null,
      filters: filters as unknown as GetVulnsQueryVariables["filters"],
      findingId,
      first,
      sortBy: sortBy as unknown as GetVulnsQueryVariables["sortBy"],
    },
  });

  const newData = useMemo((): IVulnRowAttr[] => {
    return formatVulnerabilitiesTreatment({
      organizationsGroups: undefined,
      vulnerabilities: (
        (data?.finding.vulnerabilities.edges as
          | IVulnerabilityEdge[]
          | undefined) ?? []
      ).map(
        (edge): IVulnRowAttr => ({
          ...edge.node,
          groupName: edge.node.groupName,
          where:
            edge.node.vulnerabilityType === "lines" &&
            edge.node.rootNickname !== null &&
            edge.node.rootNickname !== "" &&
            !edge.node.where.startsWith(`${edge.node.rootNickname}/`)
              ? `${edge.node.rootNickname}/${edge.node.where}`
              : edge.node.where,
        }),
      ),
    });
  }, [data]);

  const total = useMemo((): number => {
    return data?.finding.vulnerabilities
      ? (data.finding.vulnerabilities.total ?? 0)
      : 0;
  }, [data]);

  const endCursor = useMemo((): string => {
    return data?.finding.vulnerabilities.pageInfo
      ? data.finding.vulnerabilities.pageInfo.endCursor
      : "";
  }, [data]);

  return {
    data: newData,
    error,
    fetchMore: async (): Promise<void> => {
      await fetchMore({ variables: { after: endCursor } });
    },
    loading,
    total,
  };
};

export { useGetVulnsQuery };
