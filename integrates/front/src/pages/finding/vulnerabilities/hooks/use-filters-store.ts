import type { VulnerabilityFiltersInput, VulnerabilitySort } from "gql/graphql";
import {
  formatReattack,
  formatVulnerabilityState as formatState,
  formatTechnique,
  formatTreatment,
} from "utils/format-helpers";
import type { TStore } from "utils/zustand";
import { createFilterStore } from "utils/zustand";

type TFilter = VulnerabilityFiltersInput;
type TSort = VulnerabilitySort;
type TVulnStore = TStore<TFilter, TSort>;

const useFiltersStore = createFilterStore<TFilter, TSort>(
  {
    assignees: [],
    reattack: undefined,
    reportedAfter: undefined,
    reportedBefore: undefined,
    search: "",
    state: [],
    tags: [],
    technique: [],
    treatment: [],
    where: "",
  },
  (set): Partial<TVulnStore> => ({
    formatReattack,
    formatState,
    formatTechnique,
    formatTreatment,
    setAssignees: (assignees): void => {
      set({ assignees: assignees ?? [] });
    },
    setReattack: (reattack): void => {
      set({ reattack: reattack ?? undefined });
    },
    setReportedAfter: (reportedAfter): void => {
      set({ reportedAfter: reportedAfter ?? undefined });
    },
    setReportedBefore: (reportedBefore): void => {
      set({ reportedBefore: reportedBefore ?? undefined });
    },
    setSearch: (search): void => {
      set({ search: search ?? "" });
    },
    setState: (state): void => {
      set({ state: state ?? [] });
    },
    setTags: (tags): void => {
      set({ tags: tags ?? [] });
    },
    setTechnique: (technique): void => {
      set({ technique: technique ?? [] });
    },
    setTreatment: (treatment): void => {
      set({ treatment: treatment ?? [] });
    },
    setWhere: (where): void => {
      set({ where: where ?? "" });
    },
  }),
);

export { useFiltersStore };
