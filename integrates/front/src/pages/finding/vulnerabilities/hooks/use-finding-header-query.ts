import { useQuery } from "@apollo/client";

import type { IFindingHeaderResponse, TFindingHeaderVars } from "./types";

import { GET_FINDING_HEADER } from "pages/finding/queries";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";
import { translate } from "utils/translations/translate";

const useFindingHeaderQuery = ({
  findingId,
  canRetrieveHacker,
}: TFindingHeaderVars): IFindingHeaderResponse => {
  const { data, error, loading, refetch } = useQuery(GET_FINDING_HEADER, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((err): void => {
        msgError(translate.t("groupAlerts.errorTextsad"));
        Logger.warning("An error occurred loading finding header", err);
      });
    },
    variables: {
      canRetrieveHacker,
      findingId,
    },
  });

  const newData: IFindingHeaderResponse["data"] = {
    finding: data?.finding,
  };

  return {
    data: newData,
    error,
    loading,
    refetch,
  };
};

export { useFindingHeaderQuery };
