import { useQuery } from "@apollo/client";

import type { IFindingsPriorityResponse, TFindingsPriorityVars } from "./types";

import { GET_GROUP_FINDINGS_PRIORITY } from "../queries";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";
import { translate } from "utils/translations/translate";

const useFindingsPriority = ({
  groupName,
}: TFindingsPriorityVars): IFindingsPriorityResponse => {
  const { data } = useQuery(GET_GROUP_FINDINGS_PRIORITY, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((err): void => {
        msgError(translate.t("groupAlerts.errorTextsad"));
        Logger.warning(
          "An error occurred loading group findings priority",
          err,
        );
      });
    },
    variables: { groupName },
  });

  const newData: IFindingsPriorityResponse["data"] = {
    group: {
      name: groupName,
      totalOpenPriority: data?.group.totalOpenPriority ?? 0,
    },
  };

  return { data: newData };
};

export { useFindingsPriority };
