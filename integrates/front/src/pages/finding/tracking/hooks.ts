import { useQuery } from "@apollo/client";
import { useTranslation } from "react-i18next";

import { GET_FINDING_TRACKING } from "./queries";
import type { ITracking } from "./types";

import { useAudit } from "hooks/use-audit";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";

interface IUseTrackingQuery {
  loading: boolean;
  tracking: ITracking[];
}

const useTrackingQuery = (findingId: string): IUseTrackingQuery => {
  const { addAuditEvent } = useAudit();
  const { t } = useTranslation();

  const { data, loading } = useQuery(GET_FINDING_TRACKING, {
    onCompleted: (): void => {
      addAuditEvent("Finding.Tracking", findingId);
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.error("An error occurred loading finding tracking", error);
      });
    },
    variables: { findingId },
  });

  // REFAC NEEDED: Adjust API schema nullity on these fields
  const tracking = (data?.finding.tracking ?? []).map((track): ITracking => {
    return {
      ...track,
      accepted: track.accepted ?? 0,
      acceptedUndefined: track.acceptedUndefined ?? 0,
      cycle: track.cycle ?? 0,
      date: track.date ?? "",
      safe: track.safe ?? 0,
      vulnerable: track.vulnerable ?? 0,
    };
  });

  return { loading, tracking };
};

export { useTrackingQuery };
