import { Container, Loading, TimeLine } from "@fluidattacks/design";
import type { ITimelineCardProps } from "@fluidattacks/design/dist/components/timeline/types";
import React from "react";
import { useParams } from "react-router-dom";

import { useTrackingQuery } from "./hooks";
import { getTiTle, getTrackingInfo } from "./utils";

const FindingTracking: React.FC = (): JSX.Element => {
  const { findingId } = useParams() as { findingId: string };
  const { loading, tracking } = useTrackingQuery(findingId);

  const timelineItems: ITimelineCardProps[] = tracking.map(
    (closing): ITimelineCardProps => {
      return {
        date: closing.date,
        description: getTrackingInfo(closing),
        title: getTiTle(closing.cycle),
      };
    },
  );

  if (loading) {
    return (
      <Container
        alignItems={"center"}
        display={"flex"}
        height={"100%"}
        justify={"center"}
      >
        <Loading color={"red"} label={"Loading..."} size={"lg"} />
      </Container>
    );
  }

  return <TimeLine items={timelineItems} />;
};

export { FindingTracking };
