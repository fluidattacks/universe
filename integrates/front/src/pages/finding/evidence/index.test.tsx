import { PureAbility } from "@casl/ability";
import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import { Route, Routes } from "react-router-dom";

import { authzPermissionsContext } from "context/authz/config";
import type {
  ApproveEvidenceMutationMutation as ApproveEvidence,
  GetFindingEvidencesQuery as GetFindingEvidences,
  RejectEvidenceMutationMutation as RejectEvidence,
  RemoveEvidenceMutationMutation as RemoveEvidence,
} from "gql/graphql";
import { EvidenceDescriptionType, EvidenceType } from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";
import { FindingEvidence } from "pages/finding/evidence";
import {
  APPROVE_EVIDENCE_MUTATION,
  GET_FINDING_EVIDENCES,
  REJECT_EVIDENCE_MUTATION,
  REMOVE_EVIDENCE_MUTATION,
} from "pages/finding/evidence/queries";
import { msgSuccess } from "utils/notifications";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});
const Wrapper = ({
  mockedPermissions,
}: Readonly<{
  mockedPermissions: PureAbility<string>;
}>): JSX.Element => {
  return (
    <authzPermissionsContext.Provider value={mockedPermissions}>
      <Routes>
        <Route
          element={<FindingEvidence />}
          path={
            "/orgs/:organizationName/groups/:groupName/vulns/:findingId/evidence"
          }
        />
      </Routes>
    </authzPermissionsContext.Provider>
  );
};

describe("findingEvidence", (): void => {
  const evidences = {
    finding: {
      evidence: {
        animation: {
          authorEmail: "test@test.com",
          date: "",
          description: "Test description",
          isDraft: false,
          url: "some_file.png",
        },
        evidence1: {
          authorEmail: "test@test.com",
          date: "",
          description: "",
          isDraft: false,
          url: "",
        },
        evidence2: {
          authorEmail: "test@test.com",
          date: "",
          description: "",
          isDraft: false,
          url: "",
        },
        evidence3: {
          authorEmail: "test@test.com",
          date: "",
          description: "",
          isDraft: false,
          url: "",
        },
        evidence4: {
          authorEmail: "test@test.com",
          date: "",
          description: "",
          isDraft: false,
          url: "",
        },
        evidence5: {
          authorEmail: "test@test.com",
          date: "",
          description: "",
          isDraft: false,
          url: "",
        },
        exploitation: {
          authorEmail: "test@test.com",
          date: "",
          description: "",
          isDraft: false,
          url: "",
        },
      },
      id: "413372600",
    },
  };
  const graphqlMocked = graphql.link(LINK);
  const mocks = [
    graphqlMocked.query(
      GET_FINDING_EVIDENCES,
      (): StrictResponse<{ data: GetFindingEvidences }> => {
        return HttpResponse.json({ data: evidences });
      },
    ),
  ];
  const emptyMocks = [
    graphqlMocked.query(
      GET_FINDING_EVIDENCES,
      (): StrictResponse<{ data: GetFindingEvidences }> => {
        return HttpResponse.json({
          data: {
            finding: {
              __typename: "Finding",
              evidence: {
                __typename: "FindingEvidence",
                animation: {
                  authorEmail: "test@test.com",
                  date: "",
                  description: "",
                  isDraft: false,
                  url: "",
                },
                evidence1: {
                  authorEmail: "test@test.com",
                  date: "",
                  description: "",
                  isDraft: false,
                  url: "",
                },
                evidence2: {
                  authorEmail: "test@test.com",
                  date: "",
                  description: "",
                  isDraft: false,
                  url: "",
                },
                evidence3: {
                  authorEmail: "test@test.com",
                  date: "",
                  description: "",
                  isDraft: false,
                  url: "",
                },
                evidence4: {
                  authorEmail: "test@test.com",
                  date: "",
                  description: "",
                  isDraft: false,
                  url: "",
                },
                evidence5: {
                  authorEmail: "test@test.com",
                  date: "",
                  description: "",
                  isDraft: false,
                  url: "",
                },
                exploitation: {
                  authorEmail: "test@test.com",
                  date: "",
                  description: "",
                  isDraft: false,
                  url: "",
                },
              },
              id: "413372600",
            },
          },
        });
      },
    ),
  ];

  it("should render empty UI with edit button", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_update_evidence_mutate" },
    ]);

    render(<Wrapper mockedPermissions={mockedPermissions} />, {
      memoryRouter: {
        initialEntries: ["/orgs/okada/groups/test/vulns/413372600/evidence"],
      },
      mocks: emptyMocks,
    });

    await waitFor((): void => {
      expect(
        screen.queryByText("group.findings.evidence.noEvidences.title"),
      ).toBeInTheDocument();
    });

    expect(
      screen.queryByText("group.findings.evidence.add"),
    ).toBeInTheDocument();

    await userEvent.click(screen.getByText("group.findings.evidence.add"));

    expect(
      screen.queryByText("group.findings.evidence.add"),
    ).not.toBeInTheDocument();

    expect(screen.queryByText("buttons.save")).toBeInTheDocument();

    expect(screen.queryByText("buttons.cancel")).toBeInTheDocument();

    jest.clearAllMocks();
  });

  it("should render remove image", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_update_evidence_mutate" },
    ]);
    const mocksMutation = [
      graphqlMocked.query(
        GET_FINDING_EVIDENCES,
        (): StrictResponse<{ data: GetFindingEvidences }> => {
          return HttpResponse.json({ data: evidences });
        },
        { once: true },
      ),
      graphqlMocked.mutation(
        REMOVE_EVIDENCE_MUTATION,
        ({
          variables,
        }): StrictResponse<IErrorMessage | { data: RemoveEvidence }> => {
          const { evidenceId, findingId } = variables;
          if (
            evidenceId === EvidenceType.Animation &&
            findingId === "413372600"
          ) {
            return HttpResponse.json({
              data: {
                removeEvidence: { success: true },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error removing evidence")],
          });
        },
      ),
      graphqlMocked.query(
        GET_FINDING_EVIDENCES,
        (): StrictResponse<{ data: GetFindingEvidences }> => {
          return HttpResponse.json({
            data: {
              finding: {
                __typename: "Finding",
                evidence: {
                  __typename: "FindingEvidence",
                  animation: {
                    authorEmail: "test@test.com",
                    date: "",
                    description: "",
                    isDraft: false,
                    url: "",
                  },
                  evidence1: {
                    authorEmail: "test@test.com",
                    date: "",
                    description: "",
                    isDraft: false,
                    url: "",
                  },
                  evidence2: {
                    authorEmail: "test@test.com",
                    date: "",
                    description: "",
                    isDraft: false,
                    url: "",
                  },
                  evidence3: {
                    authorEmail: "test@test.com",
                    date: "",
                    description: "",
                    isDraft: false,
                    url: "",
                  },
                  evidence4: {
                    authorEmail: "test@test.com",
                    date: "",
                    description: "",
                    isDraft: false,
                    url: "",
                  },
                  evidence5: {
                    authorEmail: "test@test.com",
                    date: "",
                    description: "",
                    isDraft: false,
                    url: "",
                  },
                  exploitation: {
                    authorEmail: "test@test.com",
                    date: "",
                    description: "",
                    isDraft: false,
                    url: "",
                  },
                },
                id: "413372600",
              },
            },
          });
        },
        { once: true },
      ),
    ];

    render(<Wrapper mockedPermissions={mockedPermissions} />, {
      memoryRouter: {
        initialEntries: ["/orgs/okada/groups/test/vulns/413372600/evidence"],
      },
      mocks: mocksMutation,
    });
    await waitFor((): void => {
      expect(screen.queryAllByRole("img")).toHaveLength(1);
    });

    expect(screen.queryAllByRole("textbox")).toHaveLength(0);
    expect(
      screen.queryByText("group.findings.evidence.noEvidences.title"),
    ).not.toBeInTheDocument();

    expect(screen.getAllByRole("img")[0]).toHaveAttribute("alt", "animation");
    expect(screen.queryByText("buttons.edit")).toBeInTheDocument();

    await userEvent.click(screen.getByText("buttons.edit"));

    await waitFor((): void => {
      expect(screen.queryByText("buttons.delete")).toBeInTheDocument();
    });

    expect(screen.queryAllByRole("textbox")).toHaveLength(7);

    await userEvent.click(screen.getByText("buttons.delete"));

    await waitFor((): void => {
      expect(
        screen.queryByText("group.findings.evidence.noEvidences.title"),
      ).toBeInTheDocument();
    });

    jest.clearAllMocks();
  });

  it("should render evidence", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route
          element={<FindingEvidence />}
          path={"/:groupName/events/:findingId/evidence"}
        />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/TEST/events/413372600/evidence"],
        },
        mocks,
      },
    );
    await waitFor((): void => {
      expect(screen.queryAllByRole("img")).toHaveLength(1);
    });

    expect(screen.getAllByRole("img")[0]).toHaveAttribute("alt", "animation");
    expect(
      screen.getByText("searchFindings.tabEvidence.detail"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("Exploitation animation: Test description"),
    ).toBeInTheDocument();

    jest.clearAllMocks();
  });

  it("should render image viewer", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route
          element={<FindingEvidence />}
          path={"/:groupName/events/:findingId/evidence"}
        />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/TEST/events/413372600/evidence"],
        },
        mocks: [...mocks],
      },
    );
    await waitFor((): void => {
      expect(
        screen.queryByText("Exploitation animation: Test description"),
      ).toBeInTheDocument();
    });

    expect(screen.queryAllByRole("span", { hidden: true })).toHaveLength(0);

    await userEvent.click(screen.getAllByRole("img")[0]);
    await userEvent.hover(
      screen.getByRole("dialog", {
        hidden: true,
        name: "ImageViewer",
      }),
    );

    const reactImageViewerButtons = 2;
    await waitFor((): void => {
      expect(screen.queryAllByRole("img", { hidden: true })).toHaveLength(
        reactImageViewerButtons,
      );
    });

    jest.clearAllMocks();
  });

  it("should approve draft evidence", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_approve_evidence_mutate" },
      { action: "integrates_api_mutations_reject_evidence_mutate" },
    ]);
    const mocksMutation = [
      graphqlMocked.query(
        GET_FINDING_EVIDENCES,
        (): StrictResponse<{ data: GetFindingEvidences }> => {
          return HttpResponse.json({
            data: {
              finding: {
                __typename: "Finding",
                evidence: {
                  __typename: "FindingEvidence",
                  animation: {
                    authorEmail: "test@test.com",
                    date: "",
                    description: "Draf to approve",
                    isDraft: true,
                    url: "test_file.png",
                  },
                  evidence1: {
                    authorEmail: "test@test.com",
                    date: "",
                    description: "",
                    isDraft: false,
                    url: "",
                  },
                  evidence2: {
                    authorEmail: "test@test.com",
                    date: "",
                    description: "",
                    isDraft: false,
                    url: "",
                  },
                  evidence3: {
                    authorEmail: "test@test.com",
                    date: "",
                    description: "",
                    isDraft: false,
                    url: "",
                  },
                  evidence4: {
                    authorEmail: "test@test.com",
                    date: "",
                    description: "",
                    isDraft: false,
                    url: "",
                  },
                  evidence5: {
                    authorEmail: "test@test.com",
                    date: "",
                    description: "",
                    isDraft: false,
                    url: "",
                  },
                  exploitation: {
                    authorEmail: "test@test.com",
                    date: "",
                    description: "",
                    isDraft: false,
                    url: "",
                  },
                },
                id: "413372600",
              },
            },
          });
        },
        { once: true },
      ),
      graphqlMocked.mutation(
        APPROVE_EVIDENCE_MUTATION,
        ({
          variables,
        }): StrictResponse<IErrorMessage | { data: ApproveEvidence }> => {
          const { evidenceId, findingId } = variables;
          if (
            evidenceId === EvidenceDescriptionType.Animation &&
            findingId === "413372600"
          ) {
            return HttpResponse.json({
              data: {
                approveEvidence: { success: true },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error approving evidence")],
          });
        },
      ),
      graphqlMocked.query(
        GET_FINDING_EVIDENCES,
        (): StrictResponse<{ data: GetFindingEvidences }> => {
          return HttpResponse.json({
            data: {
              finding: {
                __typename: "Finding",
                evidence: {
                  __typename: "FindingEvidence",
                  animation: {
                    authorEmail: "test@test.com",
                    date: "",
                    description: "Draf to approve",
                    isDraft: false,
                    url: "test_file.png",
                  },
                  evidence1: {
                    authorEmail: "test@test.com",
                    date: "",
                    description: "",
                    isDraft: false,
                    url: "",
                  },
                  evidence2: {
                    authorEmail: "test@test.com",
                    date: "",
                    description: "",
                    isDraft: false,
                    url: "",
                  },
                  evidence3: {
                    authorEmail: "test@test.com",
                    date: "",
                    description: "",
                    isDraft: false,
                    url: "",
                  },
                  evidence4: {
                    authorEmail: "test@test.com",
                    date: "",
                    description: "",
                    isDraft: false,
                    url: "",
                  },
                  evidence5: {
                    authorEmail: "test@test.com",
                    date: "",
                    description: "",
                    isDraft: false,
                    url: "",
                  },
                  exploitation: {
                    authorEmail: "test@test.com",
                    date: "",
                    description: "",
                    isDraft: false,
                    url: "",
                  },
                },
                id: "413372600",
              },
            },
          });
        },
        { once: true },
      ),
    ];

    render(<Wrapper mockedPermissions={mockedPermissions} />, {
      memoryRouter: {
        initialEntries: ["/orgs/okada/groups/test/vulns/413372600/evidence"],
      },
      mocks: mocksMutation,
    });

    await waitFor((): void => {
      expect(screen.queryAllByRole("img")).toHaveLength(1);
    });

    expect(screen.queryAllByRole("textbox")).toHaveLength(0);
    expect(
      screen.queryByText("group.findings.evidence.noEvidences.title"),
    ).not.toBeInTheDocument();

    expect(
      screen.getByText("searchFindings.tabEvidenc..."),
    ).toBeInTheDocument();

    await userEvent.click(
      screen.getByText("searchFindings.tabEvidence.approve"),
    );
    await userEvent.click(await screen.findByText("Confirm"));

    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.tabEvidenc..."),
      ).not.toBeInTheDocument();
    });

    jest.clearAllMocks();
  });

  it("should reject draft evidence", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_approve_evidence_mutate" },
      { action: "integrates_api_mutations_reject_evidence_mutate" },
    ]);
    const mocksMutation = [
      graphqlMocked.query(
        GET_FINDING_EVIDENCES,
        (): StrictResponse<{ data: GetFindingEvidences }> => {
          return HttpResponse.json({
            data: {
              finding: {
                __typename: "Finding",
                evidence: {
                  __typename: "FindingEvidence",
                  animation: {
                    authorEmail: "test@test.com",
                    date: "",
                    description: "Draf to reject",
                    isDraft: true,
                    url: "test_file.png",
                  },
                  evidence1: {
                    authorEmail: "test@test.com",
                    date: "",
                    description: "",
                    isDraft: false,
                    url: "",
                  },
                  evidence2: {
                    authorEmail: "test@test.com",
                    date: "",
                    description: "",
                    isDraft: false,
                    url: "",
                  },
                  evidence3: {
                    authorEmail: "test@test.com",
                    date: "",
                    description: "",
                    isDraft: false,
                    url: "",
                  },
                  evidence4: {
                    authorEmail: "test@test.com",
                    date: "",
                    description: "",
                    isDraft: false,
                    url: "",
                  },
                  evidence5: {
                    authorEmail: "test@test.com",
                    date: "",
                    description: "",
                    isDraft: false,
                    url: "",
                  },
                  exploitation: {
                    authorEmail: "test@test.com",
                    date: "",
                    description: "",
                    isDraft: false,
                    url: "",
                  },
                },
                id: "413372600",
              },
            },
          });
        },
        { once: true },
      ),
      graphqlMocked.mutation(
        REJECT_EVIDENCE_MUTATION,
        ({
          variables,
        }): StrictResponse<IErrorMessage | { data: RejectEvidence }> => {
          const { evidenceId, findingId, justification } = variables;
          if (
            evidenceId === EvidenceDescriptionType.Animation &&
            findingId === "413372600" &&
            justification === "test reject evidence"
          ) {
            return HttpResponse.json({
              data: {
                rejectEvidence: { success: true },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error rejecting evidence")],
          });
        },
      ),
      graphqlMocked.query(
        GET_FINDING_EVIDENCES,
        (): StrictResponse<{ data: GetFindingEvidences }> => {
          return HttpResponse.json({
            data: {
              finding: {
                __typename: "Finding",
                evidence: {
                  __typename: "FindingEvidence",
                  animation: {
                    authorEmail: "test@test.com",
                    date: "",
                    description: "Draf to reject",
                    isDraft: true,
                    url: "test_file.png",
                  },
                  evidence1: {
                    authorEmail: "test@test.com",
                    date: "",
                    description: "",
                    isDraft: false,
                    url: "",
                  },
                  evidence2: {
                    authorEmail: "test@test.com",
                    date: "",
                    description: "",
                    isDraft: false,
                    url: "",
                  },
                  evidence3: {
                    authorEmail: "test@test.com",
                    date: "",
                    description: "",
                    isDraft: false,
                    url: "",
                  },
                  evidence4: {
                    authorEmail: "test@test.com",
                    date: "",
                    description: "",
                    isDraft: false,
                    url: "",
                  },
                  evidence5: {
                    authorEmail: "test@test.com",
                    date: "",
                    description: "",
                    isDraft: false,
                    url: "",
                  },
                  exploitation: {
                    authorEmail: "test@test.com",
                    date: "",
                    description: "",
                    isDraft: false,
                    url: "",
                  },
                },
                id: "413372600",
              },
            },
          });
        },
        { once: true },
      ),
    ];

    render(<Wrapper mockedPermissions={mockedPermissions} />, {
      memoryRouter: {
        initialEntries: ["/orgs/okada/groups/test/vulns/413372600/evidence"],
      },
      mocks: mocksMutation,
    });

    await waitFor((): void => {
      expect(screen.queryAllByRole("img")).toHaveLength(1);
    });

    expect(screen.queryAllByRole("textbox")).toHaveLength(0);
    expect(
      screen.queryByText("group.findings.evidence.noEvidences.title"),
    ).not.toBeInTheDocument();

    expect(
      screen.getByText("searchFindings.tabEvidenc..."),
    ).toBeInTheDocument();

    await userEvent.click(
      screen.getByText("searchFindings.tabEvidence.reject"),
    );

    expect(
      screen.getByText("searchFindings.tabEvidence.rejectModal.title"),
    ).toBeInTheDocument();

    await userEvent.type(screen.getByRole("textbox"), "test reject evidence");

    await userEvent.click(await screen.findByText("Confirm"));

    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.tabEvidenc..."),
      ).toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "searchFindings.tabEvidence.rejectModal.message",
        "searchFindings.tabEvidence.rejectModal.success",
      );
    });

    jest.clearAllMocks();
  });
});
