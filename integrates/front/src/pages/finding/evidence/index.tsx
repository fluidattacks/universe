import { NetworkStatus, useMutation, useQuery } from "@apollo/client";
import { useAbility } from "@casl/react";
import {
  Container,
  EmptyState,
  GridContainer,
  Loading,
} from "@fluidattacks/design";
import { Form, Formik } from "formik";
import { isEmpty, map } from "lodash";
import mixpanel from "mixpanel-browser";
import { useCallback, useContext, useMemo, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";

import { ActionButtons } from "./action-buttons";
import {
  findingEvidenceSchema,
  formatEvidenceImages,
  formatEvidenceList,
  setAltDescription,
  setPrefix,
  updateChangesHelper,
} from "./utils";

import { authzPermissionsContext } from "context/authz/config";
import { meetingModeContext } from "context/meeting-mode";
import { EvidencePreview } from "features/evidence-image";
import {
  filesTypeMapper,
  getFileExtension,
} from "features/evidence-image/evidence-form/utils";
import { EvidenceLightbox } from "features/evidence-lightbox";
import type { EvidenceDescriptionType, EvidenceType } from "gql/graphql";
import { useAudit } from "hooks/use-audit";
import {
  APPROVE_EVIDENCE_MUTATION,
  GET_FINDING_EVIDENCES,
  REMOVE_EVIDENCE_MUTATION,
  UPDATE_DESCRIPTION_MUTATION,
  UPDATE_EVIDENCE_MUTATION,
} from "pages/finding/evidence/queries";
import type {
  IEvidenceItem,
  IGetFindingEvidences,
} from "pages/finding/evidence/types";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";

const FindingEvidence: React.FC = (): JSX.Element => {
  const { addAuditEvent } = useAudit();
  const { findingId, groupName, organizationName } = useParams() as {
    findingId: string;
    groupName: string;
    organizationName: string;
  };
  const { t } = useTranslation();

  const { meetingMode } = useContext(meetingModeContext);
  const permissions = useAbility(authzPermissionsContext);
  const canApprove = permissions.can(
    "integrates_api_mutations_approve_evidence_mutate",
  );

  // State management
  const [isEditing, setIsEditing] = useState(false);

  const [currentImage, setCurrentImage] = useState(0);
  const [isViewerOpen, setIsViewerOpen] = useState(false);

  const closeImageViewer = useCallback(
    (index: number, isOpen: boolean): void => {
      setCurrentImage(index);
      setIsViewerOpen(isOpen);
    },
    [],
  );

  const setOpenImageViewer = useCallback((index: number): void => {
    setCurrentImage(index);
    setIsViewerOpen(true);
  }, []);

  const switchEditing = useCallback((): void => {
    setIsEditing(!isEditing);
  }, [isEditing]);

  // GraphQL operations
  const { data, loading, networkStatus, refetch } = useQuery(
    GET_FINDING_EVIDENCES,
    {
      notifyOnNetworkStatusChange: true,
      onCompleted: (): void => {
        addAuditEvent("Finding.Evidence", findingId);
      },
      onError: ({ graphQLErrors }): void => {
        graphQLErrors.forEach((error): void => {
          msgError(t("groupAlerts.errorTextsad"));
          Logger.warning("An error occurred loading finding evidences", error);
        });
      },
      variables: { findingId },
    },
  );
  const isRefetching = networkStatus === NetworkStatus.refetch;

  const [removeEvidence] = useMutation(REMOVE_EVIDENCE_MUTATION, {
    onCompleted: (): void => {
      refetch({ findingId }).catch((): void => {
        Logger.error("An error occurred removing evidence");
      });
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        if (error.message === "Exception - Evidence not found") {
          refetch({ findingId }).catch((): void => {
            Logger.error("An error occurred looking for evidence");
          });
        } else {
          msgError(t("groupAlerts.errorTextsad"));
          Logger.warning("An error occurred removing finding evidences", error);
        }
      });
    },
  });

  const [approveEvidence, { loading: approving }] = useMutation(
    APPROVE_EVIDENCE_MUTATION,
    {
      onCompleted: (): void => {
        refetch({ findingId }).catch((): void => {
          Logger.error("An error occurred approving evidence");
        });
      },
      onError: ({ graphQLErrors }): void => {
        refetch({ findingId }).catch((): void => {
          Logger.error("An error occurred finding evidence");
        });
        graphQLErrors.forEach((error): void => {
          msgError(t("groupAlerts.errorTextsad"));
          Logger.warning(
            "An error occurred approving finding evidences",
            error,
          );
        });
      },
    },
  );

  const [updateDescription] = useMutation(UPDATE_DESCRIPTION_MUTATION, {
    onError: (updateError): void => {
      updateError.graphQLErrors.forEach(({ message }): void => {
        switch (message) {
          case "Exception - Invalid field in form":
            msgError(t("validations.invalidValueInField"));
            break;
          case "Exception - Invalid characters":
            msgError(t("validations.invalidChar"));
            break;
          default:
            msgError(t("groupAlerts.errorTextsad"));
            Logger.warning(
              "An error occurred updating finding evidence",
              updateError,
            );
        }
      });
    },
  });

  const [updateEvidence] = useMutation(UPDATE_EVIDENCE_MUTATION, {
    onError: (updateError): void => {
      updateError.graphQLErrors.forEach(({ message }): void => {
        switch (message) {
          case "Exception - Invalid file size":
            msgError(t("validations.fileSize", { count: 20 }));
            break;
          case "Exception - Invalid file type":
            msgError(t("group.events.form.wrongImageType"));
            break;
          case "Exception - Invalid file name: Format organizationName-groupName-10 alphanumeric chars.extension":
            msgError(t("group.events.form.wrongImageName"));
            break;
          default:
            msgError(t("groupAlerts.errorTextsad"));
            Logger.warning(
              "An error occurred updating finding evidence",
              updateError,
            );
        }
      });
    },
  });

  const removeImage = useCallback(
    (evidenceId: string): VoidFunction => {
      return (): void => {
        mixpanel.track("RemoveEvidence");
        setIsEditing(false);
        removeEvidence({
          variables: {
            evidenceId: evidenceId.toUpperCase() as EvidenceType,
            findingId,
          },
        }).catch((): void => {
          Logger.error("An error occurred removing evidence");
        });
      };
    },
    [findingId, removeEvidence],
  );

  const approveImage = useCallback(
    (evidenceId: string): VoidFunction => {
      return (): void => {
        mixpanel.track("ApproveEvidence");
        setIsEditing(false);
        approveEvidence({
          variables: {
            evidenceId: evidenceId.toUpperCase() as EvidenceDescriptionType,
            findingId,
          },
        }).catch((): void => {
          Logger.error("An error occurred approving evidence");
        });
      };
    },
    [findingId, approveEvidence],
  );

  const evidenceImages = useMemo(
    (): Record<string, IEvidenceItem> =>
      data?.finding.evidence === undefined
        ? {}
        : formatEvidenceImages(
            data.finding
              .evidence as IGetFindingEvidences["finding"]["evidence"],
          ),
    [data?.finding.evidence],
  );

  const evidenceList = useMemo((): string[] => {
    if (isEmpty(evidenceImages)) {
      return [];
    }

    const formattedList = formatEvidenceList(
      evidenceImages,
      isEditing,
      meetingMode,
    );

    const sortedList = canApprove
      ? [...formattedList].sort((currentEvidence, nextEvidence): number => {
          const evidenceA = evidenceImages[currentEvidence];
          const evidenceB = evidenceImages[nextEvidence];

          return Number(evidenceB.isDraft) - Number(evidenceA.isDraft);
        })
      : formattedList;

    return sortedList;
  }, [canApprove, evidenceImages, isEditing, meetingMode]);

  const handleUpdate = useCallback(
    async (values: Record<string, IEvidenceItem>): Promise<void> => {
      setIsEditing(false);

      const updateChanges = async (
        evidence: IEvidenceItem & { file?: FileList },
        key: string,
      ): Promise<void> => {
        const { description, file } = evidence;
        const descriptionChanged =
          description !== evidenceImages[key].description;

        await updateChangesHelper(
          updateEvidence,
          updateDescription,
          file,
          key,
          description,
          findingId,
          descriptionChanged,
        );
      };

      await Promise.all(map(values, updateChanges));
      setCurrentImage(0);

      await refetch();
    },
    [findingId, evidenceImages, refetch, updateDescription, updateEvidence],
  );

  const validImages = evidenceList
    .filter((name): boolean => {
      const mediaUrl = evidenceImages[name].url || "";

      return filesTypeMapper[getFileExtension(mediaUrl)] === "image";
    })
    .map((name): string => evidenceImages[name].url);

  const canAddEvidence = permissions.can(
    "integrates_api_mutations_update_evidence_mutate",
  );

  if (loading) {
    return (
      <Container
        alignItems={"center"}
        display={"flex"}
        height={"100%"}
        justify={"center"}
      >
        <Loading color={"red"} label={"Loading..."} size={"lg"} />
      </Container>
    );
  }

  if (isEmpty(evidenceList) && !isEditing) {
    return (
      <EmptyState
        confirmButton={
          canAddEvidence
            ? { onClick: switchEditing, text: t("group.findings.evidence.add") }
            : undefined
        }
        description={t("group.findings.evidence.noEvidences.description")}
        padding={0}
        title={t("group.findings.evidence.noEvidences.title")}
      />
    );
  }

  return (
    <Container data-testid={"finding-evidences"}>
      <Formik
        enableReinitialize={true}
        initialValues={evidenceImages}
        name={"editEvidences"}
        onSubmit={handleUpdate}
        validationSchema={findingEvidenceSchema(
          organizationName,
          groupName,
          evidenceImages,
        )}
      >
        {({ dirty, resetForm }): JSX.Element => (
          <Form data-private={true}>
            <ActionButtons
              dirty={!dirty}
              isEditing={isEditing}
              resetForm={resetForm}
              setEditing={setIsEditing}
            />
            <GridContainer lg={4} md={3} sm={2} xl={4}>
              {evidenceList.map((name): JSX.Element => {
                const evidence = evidenceImages[name];
                const prefix = setPrefix(name);
                const altDescription = setAltDescription(prefix, evidence);
                const imageIndex = validImages.findIndex(
                  (url): boolean => url === evidence.url,
                );

                return (
                  <EvidencePreview
                    acceptedMimes={"image/png,video/webm"}
                    authorEmail={canApprove ? evidence.authorEmail : undefined}
                    date={evidence.date}
                    description={altDescription}
                    evidences={evidenceImages}
                    fileName={evidence.url}
                    findingId={findingId}
                    index={imageIndex}
                    instance={{ id: findingId, name: "Finding" }}
                    isDescriptionEditable={true}
                    isDraft={evidence.isDraft}
                    isEditing={isEditing}
                    isRefetching={isRefetching}
                    key={name}
                    name={name}
                    onApprove={
                      canApprove && !approving ? approveImage(name) : undefined
                    }
                    onDelete={removeImage(name)}
                    refetch={refetch}
                    setEditing={setIsEditing}
                    setOpenImageViewer={setOpenImageViewer}
                  />
                );
              })}
            </GridContainer>
          </Form>
        )}
      </Formik>
      {isViewerOpen ? (
        <EvidenceLightbox
          currentImage={currentImage}
          evidenceImages={validImages}
          evidences={evidenceImages}
          instance={{ id: findingId, name: "Finding" }}
          onClose={closeImageViewer}
        />
      ) : null}
    </Container>
  );
};

export type { IEvidenceItem };
export { FindingEvidence };
