import { Button, ButtonToolbarRow } from "@fluidattacks/design";
import { useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { Can } from "context/authz/can";

interface IActionButtonsProps {
  dirty: boolean;
  isEditing: boolean;
  resetForm: () => void;
  setEditing: React.Dispatch<React.SetStateAction<boolean>>;
}

const ActionButtons = ({
  dirty,
  isEditing,
  resetForm,
  setEditing,
}: Readonly<IActionButtonsProps>): JSX.Element => {
  const { t } = useTranslation();

  const handleEditClick = useCallback((): void => {
    if (!dirty) {
      resetForm();
    }
    setEditing(!isEditing);
  }, [dirty, isEditing, resetForm, setEditing]);

  return (
    <Can do={"integrates_api_mutations_update_evidence_mutate"}>
      <ButtonToolbarRow>
        {isEditing ? (
          <Button
            disabled={dirty}
            icon={"save"}
            id={"updateEvidences"}
            tooltip={t("searchFindings.tabEvidence.updateTooltip")}
            type={"submit"}
            variant={"primary"}
          >
            {t("buttons.save")}
          </Button>
        ) : undefined}
        <Button
          icon={isEditing ? "close" : "edit"}
          onClick={handleEditClick}
          tooltip={
            isEditing
              ? t("searchFindings.tabEvidence.editable.cancelTooltip")
              : t("searchFindings.tabEvidence.editable.editableTooltip")
          }
          variant={"secondary"}
        >
          {isEditing ? t("buttons.cancel") : t("buttons.edit")}
        </Button>
      </ButtonToolbarRow>
    </Can>
  );
};

export type { IActionButtonsProps };
export { ActionButtons };
