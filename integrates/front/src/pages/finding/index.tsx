import { Toggle } from "@fluidattacks/design";
import mixpanel from "mixpanel-browser";
import { useCallback, useState } from "react";
import * as React from "react";
import { Navigate, Route, Routes } from "react-router-dom";

import { FindingTemplate } from "./index.template";

import { useAudit } from "hooks/use-audit";
import { FindingConsulting } from "pages/finding/consulting";
import { FindingDescription } from "pages/finding/description";
import { FindingEvidence } from "pages/finding/evidence";
import { FindingMachine } from "pages/finding/machine";
import { FindingRecords } from "pages/finding/records";
import { FindingSeverity } from "pages/finding/severity";
import { FindingTracking } from "pages/finding/tracking";
import { FindingVulnerabilities as NewFindingVulnerabilities } from "pages/finding/vulnerabilities/new";
import { useGroupUrl } from "pages/group/hooks";

const Finding: React.FC = (): JSX.Element => {
  const { params, url } = useGroupUrl("/vulns/:findingId/*");
  const [cvssVersion, setCvssVersion] = useState<{
    accessorKey: "severityTemporalScore" | "severityThreatScore";
    header: string;
  }>({
    accessorKey: "severityThreatScore",
    header: "Severity (v4.0)",
  });

  const findingRegex =
    /[0-9A-Za-z]{8}-[0-9A-Za-z]{4}-4[0-9A-Za-z]{3}-[89ABab][0-9A-Za-z]{3}-[0-9A-Za-z]{12}|\d+/u;
  const { findingId } = params;

  const { addAuditEvent } = useAudit();
  const onChangeCvssVersion = useCallback((): void => {
    mixpanel.track("TouchCvssToggle");
    if (findingId !== undefined) addAuditEvent("CVSSToggle", findingId);
    setCvssVersion(
      (
        value,
      ): {
        accessorKey: "severityTemporalScore" | "severityThreatScore";
        header: string;
      } => {
        if (value.accessorKey === "severityThreatScore") {
          return {
            accessorKey: "severityTemporalScore",
            header: "Severity (v3.1)",
          };
        }

        return {
          accessorKey: "severityThreatScore",
          header: "Severity (v4.0)",
        };
      },
    );
  }, [addAuditEvent, findingId, setCvssVersion]);

  if (findingId !== undefined && !findingRegex.test(findingId)) {
    return <Navigate to={"/home"} />;
  }

  return (
    <FindingTemplate cvssVersion={cvssVersion.accessorKey}>
      <Routes>
        <Route
          element={
            <NewFindingVulnerabilities
              cvssVersion={cvssVersion}
              onChangeCvss={onChangeCvssVersion}
            />
          }
          path={`/locations/:vulnerabilityId`}
        />
        <Route
          element={
            <NewFindingVulnerabilities
              cvssVersion={cvssVersion}
              onChangeCvss={onChangeCvssVersion}
            />
          }
          path={`/locations`}
        />
        <Route element={<FindingDescription />} path={`/description`} />
        <Route
          element={
            <React.Fragment>
              <Toggle
                defaultChecked={
                  cvssVersion.accessorKey === "severityThreatScore"
                }
                leftDescription={"CVSS 3.1"}
                name={"cvssToggle"}
                onChange={onChangeCvssVersion}
                rightDescription={"CVSS 4.0"}
              />
              <FindingSeverity cvssVersion={cvssVersion.accessorKey} />
            </React.Fragment>
          }
          path={`/severity`}
        />
        <Route element={<FindingEvidence />} path={`/evidence`} />
        <Route element={<FindingMachine />} path={`/machine`} />
        <Route element={<FindingTracking />} path={`/tracking`} />
        <Route element={<FindingRecords />} path={`/records`} />
        <Route
          element={<FindingConsulting type={"consult"} />}
          path={`/consulting`}
        />
        <Route
          element={<FindingConsulting type={"observation"} />}
          path={`/observations`}
        />
        <Route
          element={<Navigate to={`${url}/locations`.replace("//", "/")} />}
          path={"*"}
        />
      </Routes>
    </FindingTemplate>
  );
};

export { Finding };
