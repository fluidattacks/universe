import { screen, waitFor } from "@testing-library/react";
import { Route, Routes } from "react-router-dom";

import { useCalendly } from "hooks/use-calendly";
import { render } from "mocks";
import { FindingConsulting } from "pages/finding/consulting";

jest.mock("hooks/use-calendly");
const closeUpgradeModalMock = jest.fn();
const openUpgradeModalMock = jest.fn();

describe("findingConsulting", (): void => {
  it("should render a component", async (): Promise<void> => {
    expect.hasAssertions();

    jest.mocked(useCalendly).mockReturnValue({
      closeUpgradeModal: closeUpgradeModalMock,
      isAdvancedActive: true,
      isAvailable: true,
      isUpgradeOpen: true,
      openUpgradeModal: openUpgradeModalMock,
      widgetProps: {
        prefill: {
          customAnswers: {
            a1: "",
          },
          email: "",
          name: "",
        },
        url: "",
      },
    });

    render(
      <Routes>
        <Route
          element={<FindingConsulting type={"consult"} />}
          path={"/:groupName/vulns/:findingId/consulting"}
        />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/TEST/vulns/422286126/consulting"],
        },
      },
    );

    await waitFor((): void => {
      expect(screen.queryByText("Consult comment")).toBeInTheDocument();
    });

    expect(screen.queryByText("comments.orderBy.label")).toBeInTheDocument();
    expect(screen.queryByText("comments.reply")).not.toBeInTheDocument();
    expect(screen.queryByText("Consult comment")).toBeInTheDocument();

    jest.clearAllMocks();
  });

  it("should render empty UI", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route
          element={<FindingConsulting type={"consult"} />}
          path={"/:groupName/vulns/:findingId/consulting"}
        />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/TEST/vulns/413372600/consulting"],
        },
      },
    );
    await waitFor((): void => {
      expect(
        screen.queryByText("vulnerability.consulting.description"),
      ).toBeInTheDocument();
    });

    expect(
      screen.getByText("group.consulting.empty.button"),
    ).toBeInTheDocument();

    jest.clearAllMocks();
  });

  it("should render observation", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route
          element={<FindingConsulting type={"observation"} />}
          path={"/:groupName/vulns/:findingId/observations"}
        />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/TEST/vulns/422286126/observations"],
        },
      },
    );
    await waitFor((): void => {
      expect(screen.queryByText("Observation comment")).toBeInTheDocument();
    });

    expect(screen.getByText("comments.reply")).toBeInTheDocument();

    jest.clearAllMocks();
  });
});
