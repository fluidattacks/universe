import { graphql } from "gql";

export const GET_EVENT_HEADER = graphql(`
  query GetEventHeader($eventId: String!, $groupName: String!) {
    event(groupName: $groupName, identifier: $eventId) {
      id
      eventDate
      eventStatus
      eventType
    }
  }
`);
