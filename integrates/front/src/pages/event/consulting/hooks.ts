import { useQuery } from "@apollo/client";
import { useContext } from "react";
import { useTranslation } from "react-i18next";

import { GET_EVENT_CONSULTING } from "./queries";

import { authContext } from "context/auth";
import type { ICommentStructure } from "features/comments/types";
import { useAudit } from "hooks/use-audit";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";

interface IUseConsultingQuery {
  comments: ICommentStructure[] | undefined;
  loading: boolean;
}

const useConsultingQuery = (
  eventId: string,
  groupName: string,
): IUseConsultingQuery => {
  const { addAuditEvent } = useAudit();
  const { userEmail } = useContext(authContext);
  const { t } = useTranslation();

  const { data, loading } = useQuery(GET_EVENT_CONSULTING, {
    fetchPolicy: "network-only",
    onCompleted: (): void => {
      addAuditEvent("Event.Consulting", eventId);
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.warning("An error occurred loading event comments", error);
      });
    },
    variables: { eventId, groupName },
  });

  return {
    comments: data?.event.consulting?.map(
      (comment): ICommentStructure => ({
        ...comment,
        createdByCurrentUser: comment.email === userEmail,
        id: Number(comment.id),
        parentComment: Number(comment.parentComment),
      }),
    ),
    loading,
  };
};

export { useConsultingQuery };
