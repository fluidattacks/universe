import { graphql } from "gql";

const GET_EVENT_CONSULTING = graphql(`
  query GetEventConsulting($eventId: String!, $groupName: String!) {
    event(groupName: $groupName, identifier: $eventId) {
      id
      consulting {
        id
        content
        created
        email
        fullName
        modified
        parentComment
      }
    }
  }
`);

const ADD_EVENT_CONSULT = graphql(`
  mutation AddEventConsult(
    $content: String!
    $eventId: String!
    $groupName: String!
    $parentComment: GenericScalar!
  ) {
    addEventConsult(
      content: $content
      eventId: $eventId
      groupName: $groupName
      parentComment: $parentComment
    ) {
      commentId
      success
    }
  }
`);

export { GET_EVENT_CONSULTING, ADD_EVENT_CONSULT };
