import { PureAbility } from "@casl/ability";
import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { Route, Routes } from "react-router-dom";

import { authzGroupContext } from "context/authz/config";
import { render } from "mocks";
import { EventConsulting } from "pages/event/consulting";
import { msgError } from "utils/notifications";
import { translate } from "utils/translations/translate";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();

  return mockedNotifications;
});

describe("event consulting", (): void => {
  it("should render a component", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route
          element={<EventConsulting />}
          path={"/:groupName/events/:eventId/consulting"}
        />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/TEST/events/413372600/consulting"],
        },
      },
    );
    await waitFor((): void => {
      expect(screen.queryByText("Hello world")).toBeInTheDocument();
    });

    jest.clearAllMocks();
  });

  it("should render error in component", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route
          element={<EventConsulting />}
          path={"/:groupName/events/:eventId/consulting"}
        />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/TEST/events/413372601/consulting"],
        },
      },
    );
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith("groupAlerts.errorTextsad");
    });

    jest.clearAllMocks();
  });

  it("should render empty UI", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <authzGroupContext.Provider
        value={new PureAbility([{ action: "has_advanced" }])}
      >
        <Routes>
          <Route
            element={<EventConsulting />}
            path={"/:groupName/events/:eventId/consulting"}
          />
        </Routes>
      </authzGroupContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/TEST/events/413372602/consulting"],
        },
      },
    );

    await waitFor((): void => {
      expect(
        screen.queryByText("group.events.consulting.empty.title"),
      ).toBeInTheDocument();
    });

    expect(
      screen.queryByRole("button", { name: "group.consulting.empty.button" }),
    ).toBeInTheDocument();

    expect(screen.queryByText("comments.noComments")).not.toBeInTheDocument();

    expect(
      screen.queryByPlaceholderText(translate.t("comments.editorPlaceholder")),
    ).not.toBeInTheDocument();

    jest.clearAllMocks();
  });

  it("should render comment", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route
          element={<EventConsulting />}
          path={"/:groupName/events/:eventId/consulting"}
        />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/TEST/events/413372600/consulting"],
        },
      },
    );
    await waitFor((): void => {
      expect(screen.queryByText("Second world")).toBeInTheDocument();
    });

    expect(screen.getByText("comments.orderBy.newest")).toBeInTheDocument();

    await userEvent.click(screen.getByText("comments.orderBy.newest"));

    expect(screen.getByText("comments.orderBy.oldest")).toBeInTheDocument();
    expect(screen.queryByText("comments.orderBy.label")).toBeInTheDocument();

    jest.clearAllMocks();
  });
});
