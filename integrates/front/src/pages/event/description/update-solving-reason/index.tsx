import { Col, Row } from "@fluidattacks/design";
import _ from "lodash";
import * as React from "react";
import { useTranslation } from "react-i18next";

import type { IUpdateSolvingReasonProps } from "../types";
import { Editable, Input, Select } from "components/input";

const UpdateSolvingReason: React.FC<IUpdateSolvingReasonProps> = ({
  allSolvingReasons,
  data,
  canUpdateEvent,
  isEditing,
  solvingReasons,
  solutionReasonByEventType,
  values,
}): JSX.Element => {
  const { t } = useTranslation();

  if (_.isUndefined(data) || _.isEmpty(data)) {
    return <div />;
  }

  const optSolvingReasons = _.map(
    solutionReasonByEventType[values.eventType],
    (reasonValue: string): { header: string; value: string } => ({
      header: solvingReasons[reasonValue],
      value: reasonValue,
    }),
  );

  if (isEditing && canUpdateEvent) {
    return (
      <Row>
        <Col>
          <Row>
            <Col lg={50} md={50}>
              <Select
                items={[{ header: "", value: "" }].concat(optSolvingReasons)}
                label={t("searchFindings.tabEvents.solvingReason")}
                name={"solvingReason"}
                weight={"bold"}
              />
            </Col>
            {values.solvingReason === "OTHER" ? (
              <Col lg={50} md={50}>
                <React.StrictMode>
                  <Editable
                    currentValue={
                      _.isNil(data.event.otherSolvingReason)
                        ? ""
                        : data.event.otherSolvingReason
                    }
                    isEditing={isEditing}
                    label={t(
                      "searchFindings.tabSeverity.common.deactivation.other",
                    )}
                  >
                    <Input
                      label={t(
                        "searchFindings.tabSeverity.common.deactivation.other",
                      )}
                      name={"otherSolvingReason"}
                      weight={"bold"}
                    />
                  </Editable>
                </React.StrictMode>
              </Col>
            ) : undefined}
          </Row>
        </Col>
      </Row>
    );
  }

  return data.event.solvingReason === "OTHER" ? (
    <Editable
      currentValue={
        _.isNil(data.event.otherSolvingReason)
          ? "-"
          : _.capitalize(data.event.otherSolvingReason)
      }
      isEditing={false}
      label={t("searchFindings.tabEvents.solvingReason")}
    >
      <Input
        label={t("searchFindings.tabEvents.solvingReason")}
        name={"otherSolvingReason"}
      />
    </Editable>
  ) : (
    <Editable
      currentValue={
        _.isNil(data.event.solvingReason)
          ? "-"
          : allSolvingReasons[data.event.solvingReason]
      }
      isEditing={false}
      label={t("searchFindings.tabEvents.solvingReason")}
    >
      <Input
        label={t("searchFindings.tabEvents.solvingReason")}
        name={"solvingReason"}
      />
    </Editable>
  );
};

export { UpdateSolvingReason };
