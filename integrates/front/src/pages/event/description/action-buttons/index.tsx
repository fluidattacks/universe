import { useAbility } from "@casl/react";
import { Button, Container } from "@fluidattacks/design";
import { Fragment, useCallback } from "react";
import { useTranslation } from "react-i18next";

import { Can } from "context/authz/can";
import { authzPermissionsContext } from "context/authz/config";

interface IActionButtonsProps {
  isDirtyForm: boolean;
  isEditing: boolean;
  eventStatus: string;
  onEdit: () => void;
  openRejectSolutionModal: () => void;
  openSolvingModal: () => void;
}

const ActionButtons = ({
  isDirtyForm,
  isEditing,
  eventStatus,
  onEdit: onToggleEdit,
  openRejectSolutionModal,
  openSolvingModal,
}: Readonly<IActionButtonsProps>): JSX.Element => {
  const { t } = useTranslation();
  const permissions = useAbility(authzPermissionsContext);
  const canUpdateEvent = permissions.can(
    "integrates_api_mutations_update_event_mutate",
  );

  const DisplayButtons = useCallback((): JSX.Element | null => {
    if (!(isEditing || eventStatus === "SOLVED")) {
      return (
        <Fragment>
          <Can do={"integrates_api_mutations_solve_event_mutate"}>
            <Button
              icon={"check"}
              onClick={openSolvingModal}
              variant={"primary"}
            >
              {t("group.events.description.markAsSolved")}
            </Button>
          </Can>
          {eventStatus === "VERIFICATION_REQUESTED" ? (
            <Can do={"integrates_api_mutations_reject_event_solution_mutate"}>
              <Button
                icon={"close"}
                id={
                  "group.events.description.rejectSolution.button.tooltip.btn"
                }
                onClick={openRejectSolutionModal}
                tooltip={t(
                  "group.events.description.rejectSolution.button.tooltip",
                )}
                variant={"secondary"}
              >
                {t("group.events.description.rejectSolution.button.text")}
              </Button>
            </Can>
          ) : undefined}
        </Fragment>
      );
    }

    return null;
  }, [eventStatus, isEditing, openRejectSolutionModal, openSolvingModal, t]);

  if (canUpdateEvent && isEditing) {
    return (
      <Container display={"flex"} gap={0.5} justify={"end"} width={"100%"}>
        <DisplayButtons />
        <Button icon={"close"} onClick={onToggleEdit} variant={"secondary"}>
          {t("group.events.description.cancel")}
        </Button>
        <Button
          disabled={!isDirtyForm}
          icon={"save"}
          id={"group.events.description.save.tooltip.btn"}
          tooltip={t("group.events.description.save.tooltip")}
          type={"submit"}
          variant={"primary"}
        >
          {t("group.events.description.save.text")}
        </Button>
      </Container>
    );
  }

  return (
    <Container display={"flex"} gap={0.5} justify={"end"} width={"100%"}>
      <DisplayButtons />
      {canUpdateEvent ? (
        <Button
          icon={"edit"}
          id={"group.events.description.edit.tooltip.btn"}
          onClick={onToggleEdit}
          tooltip={t("group.events.description.edit.tooltip")}
          variant={"secondary"}
        >
          {t("group.events.description.edit.text")}
        </Button>
      ) : undefined}
    </Container>
  );
};

export type { IActionButtonsProps };
export { ActionButtons };
