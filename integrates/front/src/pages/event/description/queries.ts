import { graphql } from "gql";

const GET_EVENT_DESCRIPTION = graphql(`
  query GetEventDescription(
    $canRetrieveHacker: Boolean!
    $eventId: String!
    $groupName: String!
  ) {
    event(groupName: $groupName, identifier: $eventId) {
      id
      closingDate
      detail
      environment
      eventStatus
      eventType
      hacker @include(if: $canRetrieveHacker)
      otherSolvingReason
      solvingReason
      affectedReattacks {
        id
        findingId
        specific
        where
      }
      root {
        ... on IPRoot {
          nickname
        }
        ... on GitRoot {
          nickname
        }
        ... on URLRoot {
          nickname
        }
      }
    }
  }
`);

const REJECT_EVENT_SOLUTION_MUTATION = graphql(`
  mutation RejectEventSolutionMutation(
    $comments: String!
    $eventId: String!
    $groupName: String!
  ) {
    rejectEventSolution(
      comments: $comments
      eventId: $eventId
      groupName: $groupName
    ) {
      success
    }
  }
`);

const SOLVE_EVENT_MUTATION = graphql(`
  mutation SolveEventMutation(
    $eventId: String!
    $groupName: String!
    $other: String
    $reason: SolveEventReason!
  ) {
    solveEvent(
      eventId: $eventId
      groupName: $groupName
      other: $other
      reason: $reason
    ) {
      success
    }
  }
`);

const UPDATE_EVENT_MUTATION = graphql(`
  mutation UpdateEventMutation(
    $eventDescription: String!
    $eventId: String!
    $eventType: EventType
    $groupName: String!
    $otherSolvingReason: String
    $solvingReason: SolveEventReason
  ) {
    updateEvent(
      eventDescription: $eventDescription
      eventId: $eventId
      eventType: $eventType
      groupName: $groupName
      otherSolvingReason: $otherSolvingReason
      solvingReason: $solvingReason
    ) {
      success
    }
  }
`);

export {
  GET_EVENT_DESCRIPTION,
  REJECT_EVENT_SOLUTION_MUTATION,
  SOLVE_EVENT_MUTATION,
  UPDATE_EVENT_MUTATION,
};
