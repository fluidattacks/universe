import fs from "fs";
import path from "path";

import { PureAbility } from "@casl/ability";
import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import { Route, Routes } from "react-router-dom";

import { SOLVE_EVENT_MUTATION } from "./queries";

import { authzPermissionsContext } from "context/authz/config";
import { type SolveEventMutationMutation, SolveEventReason } from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";
import {
  EventDescription,
  solutionReasonByEventType,
} from "pages/event/description";
import { msgError, msgSuccess } from "utils/notifications";
import { selectOption } from "utils/test-utils";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();

  return mockedNotifications;
});

describe("eventDescription", (): void => {
  const graphqlMocked = graphql.link(LINK);
  const mockedPermissions = new PureAbility<string>([
    { action: "integrates_api_mutations_reject_event_solution_mutate" },
    { action: "integrates_api_mutations_solve_event_mutate" },
    { action: "integrates_api_mutations_update_event_mutate" },
    { action: "integrates_api_resolvers_event_hacker_resolve" },
  ]);

  it("should render a component", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<EventDescription />}
            path={"/:groupName/events/:eventId/description"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/TEST/events/413372600/description"],
        },
      },
    );
    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.tabEvents.dateClosed"),
      ).not.toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.tabEvents.description"),
      ).toBeInTheDocument();
    });
  });

  it("should render solving modal", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<EventDescription />}
            path={"/:groupName/events/:eventId/description"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/TEST/events/413372600/description"],
        },
      },
    );
    await waitFor((): void => {
      expect(
        screen.getByText("group.events.description.markAsSolved"),
      ).toBeInTheDocument();
    });

    expect(
      screen.queryByText(
        "searchFindings.tabSeverity.common.deactivation.reason.label",
      ),
    ).not.toBeInTheDocument();

    await userEvent.click(
      screen.getByText("group.events.description.markAsSolved"),
    );
    await waitFor((): void => {
      expect(
        screen.getByText(
          "searchFindings.tabSeverity.common.deactivation.reason.label",
        ),
      ).toBeInTheDocument();
    });
  });

  it("should update event type", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<EventDescription />}
            path={"/:groupName/events/:eventId/description"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/TEST/events/413372601/description"],
        },
      },
    );

    await waitFor((): void => {
      expect(screen.getByText("Something happened")).toBeInTheDocument();
    });

    expect(screen.queryByText("Reason test")).toBeInTheDocument();

    await userEvent.click(
      screen.getByText("group.events.description.edit.text"),
    );
    await selectOption("eventType", "CREDENTIAL_ISSUES");
    await selectOption("solvingReason", "CREDENTIALS_ARE_WORKING_NOW");

    await waitFor((): void => {
      expect(
        screen.getByRole("button", {
          name: "group.events.description.save.text",
        }),
      ).not.toBeDisabled();
    });
    await userEvent.click(
      screen.getByRole("button", {
        name: "group.events.description.save.text",
      }),
    );
    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "group.events.description.alerts.editEvent.success",
        "groupAlerts.updatedTitle",
      );
    });
  });

  it("should reject event solution", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<EventDescription />}
            path={"/:groupName/events/:eventId/description"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/TEST/events/413372602/description"],
        },
      },
    );
    await waitFor((): void => {
      expect(screen.getByText("Something happened")).toBeInTheDocument();
    });
    await userEvent.click(
      screen.getByRole("button", {
        name: /group\.events\.description\.rejectsolution\.button\.text/iu,
      }),
    );
    await userEvent.type(screen.getByRole("textbox"), "Rejection reason test");

    await userEvent.click(screen.getByRole("button", { name: "Confirm" }));
    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "group.events.description.alerts.rejectSolution.success",
        "groupAlerts.updatedTitle",
      );
    });
  });

  it("should solve event fail", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const mocks = [
      graphqlMocked.mutation(
        SOLVE_EVENT_MUTATION,
        ({
          variables,
        }): StrictResponse<
          IErrorMessage | { data: SolveEventMutationMutation }
        > => {
          const { eventId, groupName, reason } = variables;
          if (
            eventId === "413372602" &&
            groupName === "TEST" &&
            reason === SolveEventReason.PermissionGranted
          ) {
            return HttpResponse.json({
              errors: [
                new GraphQLError("Exception - The git repository is outdated"),
              ],
            });
          }
          if (
            eventId === "413372602" &&
            groupName === "TEST" &&
            reason === SolveEventReason.PermissionDenied
          ) {
            return HttpResponse.json({
              errors: [
                new GraphQLError(
                  "Exception - The solving reason is not valid for the " +
                    "event type",
                ),
              ],
            });
          }

          return HttpResponse.json({
            data: {
              solveEvent: { success: true },
            },
          });
        },
      ),
    ];

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<EventDescription />}
            path={"/:groupName/events/:eventId/description"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/TEST/events/413372602/description"],
        },
        mocks,
      },
    );

    await waitFor((): void => {
      expect(screen.getByText("Something happened")).toBeInTheDocument();
    });

    const solveEventButton = screen.getByText(
      "group.events.description.markAsSolved",
    );

    expect(solveEventButton).toBeInTheDocument();

    await userEvent.click(solveEventButton);

    await selectOption("reason", "PERMISSION_GRANTED");

    await userEvent.click(screen.getByText("buttons.confirm"));

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledTimes(1);
    });

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        "Sync new changes to your repository before requesting a new re-attack.",
        "Oops!",
      );
    });

    await userEvent.click(solveEventButton);

    await selectOption("reason", "PERMISSION_DENIED");

    await userEvent.click(screen.getByText("buttons.confirm"));

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        "The solving reason is not valid for the event type",
      );
    });
  });

  it("should verify solving reasons mapping", (): void => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const getBackSolvingReasonsByEventType = (
      filePath: string,
    ): Record<string, string[]> => {
      const fileContent = fs.readFileSync(filePath, "utf-8");

      const textMatch = /SOLUTION_REASON_BY_EVENT_TYPE\s=\s\{[\s\w,.:{}]*\}/u;
      const match = textMatch.exec(fileContent);
      if (match) {
        const solvingReasons = JSON.parse(
          match[0]
            .replace(/SOLUTION_REASON_BY_EVENT_TYPE\s*=\s*/u, "")
            .replace(/\s/gu, "")
            .replace(/EventType\./gu, '"')
            .replace(/EventSolutionReason\./gu, '"')
            .replace(/:/gu, '":')
            .replace(/(?<!\}),/gu, '",')
            .replace(/(?<!^)\{/gu, "[")
            .replace(/\}(?!$)/gu, "]")
            .replace(/,\]/gu, "]")
            .replace(/,\}/gu, "}"),
        ) as Record<string, string[]>;

        return Object.fromEntries(
          Object.entries(solvingReasons).map(
            ([key, values]): [string, string[]] => [
              key,
              values
                .map((reason): string => reason)
                .sort((itemA, itemB): number => itemA.localeCompare(itemB)),
            ],
          ),
        );
      }

      return {};
    };

    const backSolvingReasonsByEventType = getBackSolvingReasonsByEventType(
      path.join(
        `${__dirname}../../../../../../back/integrates/events`,
        "constants.py",
      ),
    );

    const frontSolvingReasonsByEventType: Record<string, string[]> =
      Object.fromEntries(
        Object.entries(solutionReasonByEventType).map(
          ([key, values]): [string, string[]] => [
            key,
            values
              .map((reason): string => reason.toString())
              .sort((itemA, itemB): number => itemA.localeCompare(itemB)),
          ],
        ),
      );

    expect(backSolvingReasonsByEventType).toMatchObject(
      frontSolvingReasonsByEventType,
    );
  });
});
