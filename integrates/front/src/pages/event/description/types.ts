interface IAffectedReattacks {
  findingId: string;
  where: string;
  specific: string;
}

interface IEventDescriptionVariables {
  canRetrieveHacker: boolean;
  eventId: string;
  groupName: string;
}

interface IRootNickname {
  nickname: string;
}

interface IEventDescriptionData {
  event: {
    __typename: "Event";
    affectedReattacks: IAffectedReattacks[];
    closingDate: string;
    hacker: string;
    detail: string;
    eventType: string;
    eventStatus: string;
    id: string;
    environment: string | null;
    otherSolvingReason: string | null;
    solvingReason: string | null;
    root: IRootNickname | null;
  };
}

interface IDescriptionFormValues {
  detail: string;
  eventType: string;
  otherSolvingReason: string | null;
  solvingReason: string | null;
}

interface IRejectEventSolutionResultAttr {
  rejectEventSolution: {
    success: boolean;
  };
}

interface IUpdateEventAttr {
  updateEvent: {
    success: boolean;
  };
}

interface IUpdateSolvingReasonProps {
  allSolvingReasons: Record<string, string>;
  canUpdateEvent: boolean;
  data: IEventDescriptionData | undefined;
  isEditing: boolean;
  solvingReasons: Record<string, string>;
  solutionReasonByEventType: Record<string, string[]>;
  values: {
    affectedReattacks: IAffectedReattacks[];
    closingDate: string;
    hacker: string;
    detail: string;
    eventType: string;
    eventStatus: string;
    id: string;
    environment: string | null;
    otherSolvingReason: string | null;
    solvingReason: string | null;
    root: IRootNickname | null;
  };
}

export type {
  IAffectedReattacks,
  IRootNickname,
  IEventDescriptionData,
  IEventDescriptionVariables,
  IDescriptionFormValues,
  IRejectEventSolutionResultAttr,
  IUpdateEventAttr,
  IUpdateSolvingReasonProps,
};
