import { Text } from "@fluidattacks/design";
import _ from "lodash";
import * as React from "react";
import { useTranslation } from "react-i18next";
import type { Schema } from "yup";
import { object, string } from "yup";

import { InnerForm } from "components/form";
import { FormModal } from "components/form-modal";
import { Select, TextArea } from "components/input";
import type { EventType } from "gql/graphql";
import { useModal } from "hooks/use-modal";

const SolveEventModal = ({
  onClose,
  isOpen,
  onSubmit,
  solutionReasonByEventType,
  solvingReasons,
  submitting,
  numberOfAffectedReattacks,
  eventType,
}: Readonly<{
  onClose: VoidFunction;
  isOpen: boolean;
  onSubmit: (values: Record<string, unknown>) => void;
  solutionReasonByEventType: Record<string, string[]>;
  solvingReasons: Record<string, string>;
  submitting: boolean;
  numberOfAffectedReattacks: number;
  eventType: EventType;
}>): JSX.Element => {
  const { t } = useTranslation();
  const modalSolveEventRef = useModal("solve-event");

  const validationSchema = object().shape({
    other: string().when("reason", ([reason]: string[]): Schema => {
      return reason === "OTHER"
        ? string().required(t("validations.required"))
        : string().nullable();
    }),
    reason: string().required(t("validations.required")),
  });

  const newSelectOptions = _.map(
    solutionReasonByEventType[eventType],
    (reasonValue: string): { header: string; value: string } => {
      return { header: solvingReasons[reasonValue], value: reasonValue };
    },
  );

  return (
    <FormModal
      enableReinitialize={true}
      initialValues={{ other: "", reason: "" }}
      modalRef={{ ...modalSolveEventRef, close: onClose, isOpen }}
      name={"solvingReason"}
      onSubmit={onSubmit}
      size={"sm"}
      title={t("group.events.description.markAsSolved")}
      validationSchema={validationSchema}
    >
      {({ dirty, values }): React.ReactNode => (
        <InnerForm onCancel={onClose} submitDisabled={!dirty || submitting}>
          <Select
            items={newSelectOptions}
            label={t(
              "searchFindings.tabSeverity.common.deactivation.reason.label",
            )}
            name={"reason"}
            placeholder={""}
          />
          {values.reason === "OTHER" ? (
            <TextArea
              label={t("searchFindings.tabSeverity.common.deactivation.other")}
              name={"other"}
              required={true}
            />
          ) : undefined}
          {numberOfAffectedReattacks === 0 ? undefined : (
            <Text size={"sm"}>
              {t("group.events.description.solved.holds", {
                length: numberOfAffectedReattacks,
              })}
            </Text>
          )}
        </InnerForm>
      )}
    </FormModal>
  );
};

export { SolveEventModal };
