import { useQuery } from "@apollo/client";
import type { IIndicatorCardProps } from "@fluidattacks/design";
import { Container, Tag } from "@fluidattacks/design";
import type { ITabProps } from "@fluidattacks/design/dist/components/tabs/types";
import _ from "lodash";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";
import { useTheme } from "styled-components";

import { GET_EVENT_HEADER } from "./queries";

import { SectionHeader } from "components/section-header";
import type { IIndicatorsProps } from "components/section-header/indicators";
import { Indicators } from "components/section-header/indicators";
import { useTabTracking } from "hooks";
import { useIsGroupUnderReview } from "pages/finding/hooks";
import { FrozenGroupModal } from "pages/group/frozen-group-modal";
import { useGroupUrl } from "pages/group/hooks";
import { formatDateTime } from "utils/date";
import { castEventStatus, castEventType } from "utils/format-helpers";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";

const EventTemplate: React.FC<Readonly<{ children: JSX.Element }>> = ({
  children,
}): JSX.Element => {
  const { eventId, groupName, organizationName } = useParams() as {
    eventId: string;
    groupName: string;
    organizationName: string;
  };
  const { url } = useGroupUrl("/events/:eventId/*");
  const { t } = useTranslation();
  const theme = useTheme();
  const isGroupUnderReview = useIsGroupUnderReview();

  useTabTracking("Event");

  const { data } = useQuery(GET_EVENT_HEADER, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.warning("An error occurred loading event header", error);
      });
    },
    variables: { eventId, groupName },
  });

  if (_.isUndefined(data) || _.isEmpty(data)) {
    return <div />;
  }

  const { eventDate, eventStatus, eventType, id } = data.event;
  const { status, tagVariant } = castEventStatus(eventStatus);

  const tabsItems: ITabProps[] = [
    {
      id: "resources-tab",
      label: t("searchFindings.tabEvents.description"),
      link: `${url}/description`,
    },
    {
      id: "evidence-tab",
      label: t("searchFindings.tabEvents.evidence"),
      link: `${url}/evidence`,
    },
    {
      id: "consulting-tab",
      label: t("searchFindings.tabEvents.consulting"),
      link: `${url}/consulting`,
    },
  ];

  const indicatorsCards: React.PropsWithChildren<IIndicatorCardProps>[] = [
    {
      description: t("searchFindings.tabEvents.id"),
      title: id,
      variant: "left-aligned",
    },
    {
      description: t("searchFindings.tabEvents.date"),
      title: formatDateTime(eventDate),
      variant: "left-aligned",
    },
    {
      children: (
        <Tag
          fontSize={"20px"}
          priority={"low"}
          tagLabel={t(status)}
          variant={tagVariant}
        />
      ),
      description: t("searchFindings.tabEvents.status"),
      variant: "left-aligned",
    },
  ];

  const indicators: IIndicatorsProps = {
    cards: indicatorsCards,
  };

  return (
    <Container bgColor={theme.palette.gray[50]} height={"100%"}>
      <SectionHeader
        goBackTo={`/orgs/${organizationName}/groups/${groupName}/events`}
        header={`${_.capitalize(groupName)} | ${t(castEventType(eventType))}`}
        tabs={tabsItems}
      />
      <Container
        bgColor={"white"}
        borderBottom={`1px solid ${theme.palette.gray[300]}`}
        pb={1}
        pl={1.25}
        pr={1.25}
        pt={1}
        width={"100%"}
      >
        <Indicators
          alertProps={indicators.alertProps}
          cards={indicators.cards}
        />
      </Container>
      <Container px={1.25} py={1.25}>
        {children}
      </Container>
      <FrozenGroupModal isOpen={Boolean(isGroupUnderReview)} />
    </Container>
  );
};

export { EventTemplate };
