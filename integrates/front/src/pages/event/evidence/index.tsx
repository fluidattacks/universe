import { NetworkStatus, useMutation, useQuery } from "@apollo/client";
import type { FetchResult } from "@apollo/client";
import { useAbility } from "@casl/react";
import {
  Button,
  Container,
  EmptyState,
  GridContainer,
} from "@fluidattacks/design";
import { Form, Formik } from "formik";
import _ from "lodash";
import { useCallback, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";

import {
  DOWNLOAD_FILE_MUTATION,
  GET_EVENT_EVIDENCES,
  REMOVE_EVIDENCE_MUTATION,
  UPDATE_EVIDENCE_MUTATION,
} from "./queries";
import type {
  IEventEvidenceAttr,
  IUpdateEventEvidenceResultAttr,
} from "./types";
import { eventEvidenceSchema } from "./validations";

import { Authorize } from "components/@core/authorize";
import { authzPermissionsContext } from "context/authz/config";
import { EvidencePreview } from "features/evidence-image";
import { EvidenceLightbox } from "features/evidence-lightbox";
import type {
  EventEvidenceType,
  DownloadEventFileMutationMutation as TDownloadFile,
} from "gql/graphql";
import { useAudit } from "hooks/use-audit";
import { getErrors } from "utils/helpers";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";
import { openUrl } from "utils/resource-helpers";

const EventEvidence: React.FC = (): JSX.Element => {
  const { t } = useTranslation();
  const { eventId, groupName, organizationName } = useParams() as {
    eventId: string;
    groupName: string;
    organizationName: string;
  };

  const permissions = useAbility(authzPermissionsContext);
  const canUpdateEventEvidence = permissions.can(
    "integrates_api_mutations_update_event_evidence_mutate",
  );

  const [isEditing, setIsEditing] = useState(false);
  const [currentImage, setCurrentImage] = useState(0);
  const [isViewerOpen, setIsViewerOpen] = useState(false);

  const handleEditClick = useCallback((): void => {
    setIsEditing(!isEditing);
  }, [isEditing]);
  const closeImageViewer = useCallback(
    (index: number, isOpen: boolean): void => {
      setCurrentImage(index);
      setIsViewerOpen(isOpen);
    },
    [],
  );
  const setOpenImageViewer = useCallback((index: number): void => {
    setCurrentImage(index);
    setIsViewerOpen(true);
  }, []);

  const { addAuditEvent } = useAudit();
  const { data, networkStatus, refetch } = useQuery(GET_EVENT_EVIDENCES, {
    onCompleted: (): void => {
      addAuditEvent("Event.Evidence", eventId);
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.warning("An error occurred loading event evidences", error);
      });
    },
    variables: { eventId, groupName },
  });
  const isRefetching = networkStatus === NetworkStatus.refetch;

  const [downloadEvidence] = useMutation(DOWNLOAD_FILE_MUTATION, {
    onCompleted: (downloadData: TDownloadFile): void => {
      openUrl(downloadData.downloadEventFile.url);
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.warning("An error occurred downloading event file", error);
      });
    },
  });
  const [removeEvidence] = useMutation(REMOVE_EVIDENCE_MUTATION, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.warning("An error occurred removing event evidence", error);
      });
    },
  });
  const [updateEvidence] = useMutation(UPDATE_EVIDENCE_MUTATION, {
    onError: (updateError): void => {
      updateError.graphQLErrors.forEach(({ message }): void => {
        switch (message) {
          case "Exception - The event has already been closed":
            msgError(t("group.events.alreadyClosed"));
            break;
          case "Exception - Invalid file size":
            msgError(t("validations.fileSize", { count: 10 }));
            break;
          case "Exception - Invalid file type: EVENT_IMAGE":
            msgError(t("group.events.form.wrongImageType"));
            break;
          case "Exception - Invalid file type: EVENT_FILE":
            msgError(t("group.events.form.wrongFileType"));
            break;
          case "Exception - Unsanitized input found":
            msgError(t("validations.unsanitizedInputFound"));
            break;
          case "Exception - Invalid file name: Format organizationName-groupName-10 alphanumeric chars.extension":
            msgError(t("group.events.form.wrongImageName"));
            break;
          default:
            msgError(t("groupAlerts.errorTextsad"));
            Logger.warning(
              "An error occurred updating event evidence",
              updateError,
            );
        }
      });
    },
  });

  const handleUpdate = useCallback(
    async (values: Record<string, { file?: FileList }>): Promise<void> => {
      setIsEditing(false);

      const results = await Promise.all(
        _.map(
          _.omitBy(values, (evidence: { file?: FileList }): boolean =>
            _.isUndefined(evidence.file),
          ) as Record<string, { file: FileList }>,
          async (
            evidence,
            key,
          ): Promise<FetchResult<IUpdateEventEvidenceResultAttr>> =>
            updateEvidence({
              variables: {
                eventId,
                evidenceType: _.snakeCase(
                  key,
                ).toUpperCase() as EventEvidenceType,
                file: evidence.file[0],
                groupName,
              },
            }),
        ),
      );
      const errors = getErrors<IUpdateEventEvidenceResultAttr>(results);
      if (!_.isEmpty(results) && _.isEmpty(errors)) {
        msgSuccess(
          t("group.events.evidence.alerts.update.success"),
          t("groupAlerts.updatedTitle"),
        );
      }
      setCurrentImage(0);

      await refetch();
    },
    [refetch, updateEvidence, eventId, groupName, t],
  );
  const handleRemoveEvidence = useCallback(
    (evidenceName: string): (() => void) =>
      async (): Promise<void> => {
        setIsEditing(false);
        await removeEvidence({
          variables: {
            eventId,
            evidenceType: _.snakeCase(
              evidenceName,
            ).toUpperCase() as EventEvidenceType,
            groupName,
          },
        });
        await refetch();
      },
    [eventId, groupName, refetch, removeEvidence],
  );

  if (_.isEmpty(data) || _.isUndefined(data)) {
    return <div />;
  }

  const handleIncomingEvidence = (
    evidence: {
      __typename?: "EventEvidenceItem";
      fileName: string | null;
      date: string | null;
    } | null,
  ): IEventEvidenceAttr => {
    if (_.isNull(evidence)) {
      return { date: "", fileName: "" };
    }

    return evidence as IEventEvidenceAttr;
  };

  const evidenceImages: Record<string, IEventEvidenceAttr> = {
    image1: handleIncomingEvidence(data.event.evidences.image1),
    image2: handleIncomingEvidence(data.event.evidences.image2),
    image3: handleIncomingEvidence(data.event.evidences.image3),
    image4: handleIncomingEvidence(data.event.evidences.image4),
    image5: handleIncomingEvidence(data.event.evidences.image5),
    image6: handleIncomingEvidence(data.event.evidences.image6),
  };
  const imageList = Object.keys(evidenceImages).filter((name): boolean => {
    return _.isEmpty(evidenceImages[name].fileName) ? isEditing : true;
  });
  const evidenceFiles: Record<string, IEventEvidenceAttr> = {
    file1: handleIncomingEvidence(
      data.event.evidences.file1 as IEventEvidenceAttr,
    ),
  };
  const fileList = Object.keys(evidenceFiles).filter((name): boolean => {
    return _.isEmpty(evidenceFiles[name].fileName) ? isEditing : true;
  });
  const emptyState = _.isEmpty(imageList) && _.isEmpty(fileList) && !isEditing;

  return (
    <Container>
      {emptyState ? (
        <EmptyState
          confirmButton={
            canUpdateEventEvidence
              ? {
                  onClick: handleEditClick,
                  text: t("group.events.evidence.empty.button"),
                }
              : undefined
          }
          description={t("group.events.evidence.empty.description")}
          imageSrc={"integrates/empty/noEvidences"}
          padding={0}
          title={t("group.events.evidence.empty.title")}
        />
      ) : undefined}
      <Formik
        enableReinitialize={true}
        initialValues={{ ...evidenceImages, ...evidenceFiles }}
        name={"editEvidences"}
        onSubmit={handleUpdate}
        validationSchema={eventEvidenceSchema(organizationName, groupName, {
          ...evidenceImages,
          ...evidenceFiles,
        })}
      >
        {({ dirty }): JSX.Element => (
          <Form id={"editEvidences"}>
            <Container display={"flex"} gap={0.5} justify={"end"} pb={1.25}>
              {emptyState ? undefined : (
                <Authorize
                  can={"integrates_api_mutations_update_event_evidence_mutate"}
                >
                  <Button
                    disabled={data.event.eventStatus === "SOLVED"}
                    icon={isEditing ? "close" : "edit"}
                    id={t("group.events.evidence.editTooltip.id")}
                    onClick={handleEditClick}
                    tooltip={t("group.events.evidence.editTooltip")}
                    variant={"secondary"}
                  >
                    {t("group.events.evidence.edit")}
                  </Button>
                </Authorize>
              )}
              {isEditing ? (
                <Button
                  disabled={!dirty}
                  icon={"save"}
                  id={t("searchFindings.tabEvidence.updateTooltip.id")}
                  tooltip={t("searchFindings.tabEvidence.updateTooltip")}
                  type={"submit"}
                  variant={"primary"}
                >
                  {t("searchFindings.tabEvidence.update")}
                </Button>
              ) : undefined}
            </Container>
            <GridContainer lg={4} md={3} sm={2} xl={4}>
              {imageList.map((name, index): JSX.Element => {
                const evidence = evidenceImages[name];

                return (
                  <EvidencePreview
                    acceptedMimes={"image/png,video/webm"}
                    date={evidence.date}
                    description={""}
                    evidences={evidenceImages}
                    fileName={evidence.fileName}
                    index={index}
                    instance={{ id: eventId, name: "Event" }}
                    isDescriptionEditable={false}
                    isEditing={isEditing}
                    isRefetching={isRefetching}
                    key={name}
                    name={name}
                    onDelete={handleRemoveEvidence(name)}
                    setOpenImageViewer={setOpenImageViewer}
                  />
                );
              })}
              {fileList.map((name): JSX.Element => {
                const evidence = evidenceFiles[name];

                const handleDownload = async (): Promise<void> => {
                  if (!isEditing) {
                    await downloadEvidence({
                      variables: {
                        eventId,
                        fileName: evidence.fileName,
                        groupName,
                      },
                    });
                  }
                };

                return (
                  <EvidencePreview
                    acceptedMimes={
                      "application/pdf,application/zip,text/csv,text/plain"
                    }
                    date={evidence.date}
                    description={""}
                    evidences={evidenceFiles}
                    fileName={evidence.fileName}
                    instance={{ id: eventId, name: "Event" }}
                    isDescriptionEditable={false}
                    isDocument={true}
                    isEditing={isEditing}
                    isRefetching={isRefetching}
                    key={name}
                    name={name}
                    onDelete={handleRemoveEvidence(name)}
                    // Next annotations needed due to nested callbacks
                    // eslint-disable-next-line
                    onDownload={handleDownload}
                  />
                );
              })}
            </GridContainer>
          </Form>
        )}
      </Formik>
      {isViewerOpen ? (
        <EvidenceLightbox
          currentImage={currentImage}
          evidenceImages={imageList.map((name): string => {
            return evidenceImages[name].fileName;
          })}
          evidences={evidenceImages}
          instance={{ id: eventId, name: "Event" }}
          onClose={closeImageViewer}
        />
      ) : null}
    </Container>
  );
};

export { EventEvidence };
