import { graphql } from "gql";

const DOWNLOAD_FILE_MUTATION = graphql(`
  mutation DownloadEventFileMutation(
    $eventId: String!
    $fileName: String!
    $groupName: String!
  ) {
    downloadEventFile(
      eventId: $eventId
      fileName: $fileName
      groupName: $groupName
    ) {
      success
      url
    }
  }
`);

const GET_EVENT_EVIDENCES = graphql(`
  query GetEventEvidences($eventId: String!, $groupName: String!) {
    event(groupName: $groupName, identifier: $eventId) {
      id
      eventStatus
      evidences {
        file1 {
          date
          fileName
        }
        image1 {
          date
          fileName
        }
        image2 {
          date
          fileName
        }
        image3 {
          date
          fileName
        }
        image4 {
          date
          fileName
        }
        image5 {
          date
          fileName
        }
        image6 {
          date
          fileName
        }
      }
    }
  }
`);

const UPDATE_EVIDENCE_MUTATION = graphql(`
  mutation UpdateEventEvidenceMutation(
    $eventId: String!
    $evidenceType: EventEvidenceType!
    $file: Upload!
    $groupName: String!
  ) {
    updateEventEvidence(
      eventId: $eventId
      evidenceType: $evidenceType
      file: $file
      groupName: $groupName
    ) {
      success
    }
  }
`);

const REMOVE_EVIDENCE_MUTATION = graphql(`
  mutation RemoveEventEvidenceMutation(
    $eventId: String!
    $evidenceType: EventEvidenceType!
    $groupName: String!
  ) {
    removeEventEvidence(
      eventId: $eventId
      evidenceType: $evidenceType
      groupName: $groupName
    ) {
      success
    }
  }
`);

export {
  DOWNLOAD_FILE_MUTATION,
  GET_EVENT_EVIDENCES,
  UPDATE_EVIDENCE_MUTATION,
  REMOVE_EVIDENCE_MUTATION,
};
