import type { IVulnRowAttr } from "features/vulnerabilities/types";

const getSeverity = (
  cvssVersion: {
    accessorKey: string;
    header: string;
  },
  row: IVulnRowAttr,
): number => {
  if (cvssVersion.accessorKey === "severityThreatScore") {
    return row.severityThreatScore;
  }

  return row.severityTemporalScore;
};

const getProposedSeverity = (
  cvssVersion: {
    accessorKey: string;
    header: string;
  },
  row: IVulnRowAttr,
): number | null => {
  if (cvssVersion.accessorKey === "severityThreatScore") {
    return row.proposedSeverityThreatScore;
  }

  return row.proposedSeverityTemporalScore;
};

export { getSeverity, getProposedSeverity };
