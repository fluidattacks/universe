import { useQuery } from "@apollo/client";
import { Toggle } from "@fluidattacks/design";
import type { ColumnDef } from "@tanstack/react-table";
import _ from "lodash";
import mixpanel from "mixpanel-browser";
import { useCallback, useContext, useEffect, useMemo, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { ActionButtons } from "./action-buttons";
import { GET_VULNERABILITIES_SEVERITY_UPDATE_REQUESTS } from "./queries";
import { getProposedSeverity, getSeverity } from "./utils";

import { formatLinkHandler } from "components/table/table-formatters";
import { filterDate } from "components/table/utils";
import { allGroupContext } from "context/authz/all-group-permissions-provider";
import {
  authzGroupContext,
  authzPermissionsContext,
} from "context/authz/config";
import { VulnComponent } from "features/vulnerabilities";
import { statusFormatter } from "features/vulnerabilities/formatters/status";
import { HandleSeverityRequestsForm } from "features/vulnerabilities/severity-request-form";
import type { IVulnRowAttr } from "features/vulnerabilities/types";
import { useModal, useTabTracking } from "hooks";
import { useAudit } from "hooks/use-audit";
import { useVulnerabilityStore } from "hooks/use-vulnerability-store";
import type { IModalConfig } from "pages/finding/vulnerabilities/types";
import { severityFormatter } from "utils/format-helpers";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";

const VulnerabilitySeverityUpdateRequests: React.FC = (): JSX.Element => {
  const { t } = useTranslation();

  const { isApprovingSeverityUpdate, setIsApprovingSeverityUpdate } =
    useVulnerabilityStore();

  const [isConfirmModalOpen, setIsConfirmModalOpen] = useState(false);
  const [cvssVersion, setCvssVersion] = useState({
    accessorKey: "severityThreatScore",
    header: "Severity (v4.0)",
  });

  const formRef = useModal("handle-severity-request-form");

  const { onGroupChange, userData } = useContext(allGroupContext);
  const attributesContext = useContext(authzGroupContext);
  const permissionsContext = useContext(authzPermissionsContext);
  const DEBOUNCE_DELAY_MS = 1500;
  const { addAuditEvent } = useAudit();
  const {
    data: vulnsData,
    refetch: meVulnsRefetch,
    loading: vulnsLoading,
    fetchMore,
  } = useQuery(GET_VULNERABILITIES_SEVERITY_UPDATE_REQUESTS, {
    fetchPolicy: "no-cache",
    onCompleted: (): void => {
      addAuditEvent("ToDo.SeverityUpdateRequests", "unknown");
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.warning(
          "An error occurred fetching severity update requests",
          error,
        );
      });
    },
    skip: _.isUndefined(userData),
  });

  useTabTracking("Todos");

  const vulnerabilities = useMemo(
    (): IVulnRowAttr[] =>
      _.isUndefined(vulnsData)
        ? []
        : vulnsData.me.vulnerabilitiesSeverityRequests.edges.map(
            (edge): IVulnRowAttr => edge?.node as IVulnRowAttr,
          ),
    [vulnsData],
  );

  const vulnerabilitiesGroupName = useMemo(
    (): string[] =>
      Array.from(
        new Set(
          vulnerabilities.map((vulnerability): string =>
            vulnerability.groupName.toLowerCase(),
          ),
        ),
      ),
    [vulnerabilities],
  );

  useEffect((): void => {
    if (!_.isUndefined(onGroupChange)) {
      onGroupChange(vulnerabilitiesGroupName);
    }
  }, [
    attributesContext,
    onGroupChange,
    permissionsContext,
    userData,
    vulnerabilitiesGroupName,
  ]);

  const pageInfo = _.isUndefined(vulnsData)
    ? { endCursor: "", hasNextPage: false }
    : vulnsData.me.vulnerabilitiesSeverityRequests.pageInfo;

  const handleNextPage = useCallback(async (): Promise<void> => {
    if (pageInfo.hasNextPage) {
      await fetchMore({
        variables: { after: pageInfo.endCursor },
      });
    }
  }, [pageInfo.hasNextPage, pageInfo.endCursor, fetchMore]);

  const columns: ColumnDef<IVulnRowAttr>[] = [
    {
      accessorKey: "organizationName",
      cell: (cell): JSX.Element => {
        const orgName = cell.row.original.organizationName ?? "";
        const link = `/orgs/${orgName}/groups/${cell.row.original.groupName}`;
        const text = cell.getValue<string>();

        return formatLinkHandler(link, text);
      },
      header: t("searchFindings.tabVuln.vulnTable.organization"),
      meta: { filterType: "select" },
    },
    {
      accessorKey: "where",
      enableColumnFilter: false,
      header: t("searchFindings.tabVuln.vulnTable.where"),
    },
    {
      accessorKey: "specific",
      enableColumnFilter: false,
      header: t("searchFindings.tabVuln.vulnTable.specific"),
      sortingFn: "alphanumeric",
    },
    {
      accessorKey: "groupName",
      cell: (cell): JSX.Element => {
        const orgName = cell.row.original.organizationName ?? "";
        const link = `/orgs/${orgName}/groups/${cell.row.original.groupName}`;
        const text = cell.getValue<string>();

        return formatLinkHandler(link, text);
      },
      header: t("organization.tabs.groups.newGroup.name.text"),
      meta: { filterType: "select" },
    },
    {
      accessorKey: "state",
      cell: (cell): JSX.Element => {
        const labels: Record<string, string> = {
          REJECTED: t("searchFindings.tabVuln.rejected"),
          SAFE: t("searchFindings.tabVuln.closed"),
          SUBMITTED: t("searchFindings.tabVuln.submitted"),
          VULNERABLE: t("searchFindings.tabVuln.open"),
        };

        return statusFormatter(labels[cell.getValue<string>()]);
      },
      header: t("searchFindings.tabVuln.vulnTable.status"),
      meta: { filterType: "select" },
    },
    {
      accessorKey: "reportDate",
      filterFn: filterDate,
      header: t("searchFindings.tabVuln.vulnTable.reportDate"),
      meta: { filterType: "dateRange" },
    },
    {
      accessorFn: (row): number => getSeverity(cvssVersion, row),
      cell: (cell): JSX.Element => severityFormatter(Number(cell.getValue())),
      header: cvssVersion.header,
      id: "severity",
      meta: { filterType: "select" },
    },
    {
      accessorFn: (row): number | null => getProposedSeverity(cvssVersion, row),
      cell: (cell): JSX.Element => severityFormatter(Number(cell.getValue())),
      header: `Proposed ${cvssVersion.header}`,
      id: "proposedSeverity",
      meta: { filterType: "select" },
    },
    {
      accessorKey: "hacker",
      enableColumnFilter: false,
      header: t("searchFindings.tabVuln.vulnTable.hacker"),
    },
    {
      accessorKey: "proposedSeverityAuthor",
      enableColumnFilter: false,
      header: t("searchFindings.tabVuln.vulnTable.requester"),
    },
  ];

  const onChangeCvssVersion = useCallback((): void => {
    mixpanel.track("TouchCvssToggle");
    addAuditEvent("CVSSToggle", "unknown");
    setCvssVersion(
      (
        value,
      ): {
        accessorKey: string;
        header: string;
      } => {
        if (value.accessorKey === "severityThreatScore") {
          return {
            accessorKey: "severityTemporalScore",
            header: "Severity (v3.1)",
          };
        }

        return {
          accessorKey: "severityThreatScore",
          header: "Severity (v4.0)",
        };
      },
    );
  }, [addAuditEvent]);

  const closeConfirmModal = useCallback((): void => {
    setIsConfirmModalOpen(false);
    setIsApprovingSeverityUpdate(!isApprovingSeverityUpdate);
  }, [setIsApprovingSeverityUpdate, isApprovingSeverityUpdate]);

  const refetchVulns = useCallback(async (): Promise<void> => {
    await meVulnsRefetch();
  }, [meVulnsRefetch]);

  const [modalConfig, setModalConfig] = useState<IModalConfig>({
    clearSelected: (): void => undefined,
    selectedVulnerabilities: [],
  });

  const onVulnsSelect = useCallback(
    (vulns: IVulnRowAttr[], clearSelected: () => void): void => {
      setModalConfig({
        clearSelected,
        selectedVulnerabilities: vulns,
      });
    },
    [],
  );

  const onApprove = useCallback((): void => {
    if (isApprovingSeverityUpdate) {
      setIsApprovingSeverityUpdate(!isApprovingSeverityUpdate);
    } else {
      const { selectedVulnerabilities } = modalConfig;
      if (selectedVulnerabilities.length > 0) {
        setIsConfirmModalOpen(true);
      }
      setIsApprovingSeverityUpdate(!isApprovingSeverityUpdate);
    }
  }, [isApprovingSeverityUpdate, modalConfig, setIsApprovingSeverityUpdate]);

  return (
    <React.StrictMode>
      <VulnComponent
        columns={columns}
        extraBelowButtons={
          <Toggle
            defaultChecked={cvssVersion.accessorKey === "severityThreatScore"}
            leftDescription={"CVSS 3.1"}
            name={"cvssToggle"}
            onChange={onChangeCvssVersion}
            rightDescription={"CVSS 4.0"}
          />
        }
        extraButtons={
          <ActionButtons
            areVulnsSelected={modalConfig.selectedVulnerabilities.length > 0}
            onApprove={onApprove}
            refreshVulns={refetchVulns}
          />
        }
        loading={vulnsLoading}
        onNextPage={handleNextPage}
        onVulnSelect={onVulnsSelect}
        refetchData={refetchVulns}
        tableOptions={{ columnToggle: true }}
        tableRefProps={{
          columnOrder: [
            "where",
            "specific",
            "groupName",
            "state",
            "severity",
            "proposedSeverity",
            "hacker",
            "proposedSeverityAuthor",
          ],
          columnPinning: { left: ["where"] },
          columnVisibility: {
            organizationName: false,
            reportDate: false,
          },
          id: "severityRequestsTbl",
        }}
        vulnerabilities={vulnerabilities}
      />
      <HandleSeverityRequestsForm
        clearSelected={modalConfig.clearSelected}
        findingId={modalConfig.selectedVulnerabilities[0]?.findingId ?? ""}
        modalRef={{ ...formRef, isOpen: isConfirmModalOpen }}
        onCancel={closeConfirmModal}
        refetchData={_.debounce(refetchVulns, DEBOUNCE_DELAY_MS)}
        vulnerabilities={modalConfig.selectedVulnerabilities}
      />
    </React.StrictMode>
  );
};

export { VulnerabilitySeverityUpdateRequests };
