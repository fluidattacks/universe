import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import * as React from "react";
import { Route, Routes } from "react-router-dom";

import { GET_TODO_REATTACKS } from "./queries";

import { Reattacks } from ".";
import { GET_USER_ORGANIZATIONS_GROUPS } from "../queries";
import type {
  GetTodoReattacksVulnerableQuery as GetTodoReattacks,
  GetUserOrganizationsGroupsAtDashboardQuery as GetUserOrganizationsGroups,
} from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

const DummyComponent: React.FC = (): JSX.Element => {
  return <div>{"For redirect test only"}</div>;
};

describe("reattacks", (): void => {
  const graphqlMocked = graphql.link(LINK);
  const mocksReattacks = graphqlMocked.query(
    GET_TODO_REATTACKS,
    (): StrictResponse<{ data: GetTodoReattacks }> => {
      return HttpResponse.json({
        data: {
          me: {
            findingReattacksConnection: {
              edges: [
                {
                  node: {
                    groupName: "group1",
                    id: "436992569",
                    title: "038. Business information leak",
                    verificationSummary: {
                      onHold: 0,
                      requested: 23,
                      verified: 0,
                    },
                    vulnerabilitiesToReattackConnection: {
                      edges: [
                        {
                          node: {
                            id: "587c40de-09a0-4d85-a9f9-eaa46aa895d7",
                            lastRequestedReattackDate: "2022-07-12 16:42:53",
                          },
                        },
                      ],
                      pageInfo: {
                        endCursor: "bnVsbA==",
                        hasNextPage: false,
                      },
                      total: 1,
                    },
                  },
                },
              ],
              pageInfo: {
                endCursor: "bnVsbA==",
                hasNextPage: false,
              },
              total: 1,
            },
            userEmail: "test@fluidattacks.com",
          },
        },
      });
    },
  );
  const mocksUserGroups = graphqlMocked.query(
    GET_USER_ORGANIZATIONS_GROUPS,
    (): StrictResponse<{ data: GetUserOrganizationsGroups }> => {
      return HttpResponse.json({
        data: {
          me: {
            __typename: "Me",
            organizations: [
              {
                __typename: "Organization",
                groups: [
                  {
                    __typename: "Group",
                    name: "group1",
                    permissions: [
                      "integrates_api_mutations_confirm_vulnerabilities_mutate",
                    ],
                    serviceAttributes: [],
                  },
                ],
                name: "orgtest",
              },
            ],
            userEmail: "test@test.test",
          },
        },
      });
    },
  );

  it("should render table columns", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route element={<Reattacks />} path={"/todos/reattacks"} />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/todos/reattacks"],
        },
        mocks: [mocksReattacks, mocksUserGroups],
      },
    );

    expect(screen.queryAllByRole("table")).toHaveLength(1);

    expect(screen.getByText("Type")).toBeInTheDocument();
    expect(screen.getByText("Requested vulns")).toBeInTheDocument();
    expect(screen.getByText("Group name")).toBeInTheDocument();
    expect(screen.getByText("Reattack date")).toBeInTheDocument();

    await waitFor((): void => {
      expect(
        screen.queryByText("038. Business information leak"),
      ).toBeInTheDocument();
    });

    expect(screen.getByText("2022-07-12 04:42 PM")).toBeInTheDocument();
    expect(screen.getByText("23")).toBeInTheDocument();
    expect(screen.getByText("group1")).toBeInTheDocument();

    jest.clearAllMocks();
  });

  it("should redirect to locations", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route element={<Reattacks />} path={"/todos/reattacks"} />
        <Route
          element={<DummyComponent />}
          path={"/orgs/orgtest/groups/group1/vulns/436992569/locations"}
        />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/todos/reattacks"],
        },
        mocks: [mocksReattacks, mocksUserGroups],
      },
    );

    expect(screen.queryAllByRole("table")).toHaveLength(1);

    await waitFor((): void => {
      expect(
        screen.queryByText("038. Business information leak"),
      ).toBeInTheDocument();
    });

    expect(screen.queryByText("group1")).toBeInTheDocument();

    await userEvent.click(screen.getByText("group1"));

    expect(screen.getByText("For redirect test only")).toBeInTheDocument();

    jest.clearAllMocks();
  });
});
