import { useQuery } from "@apollo/client";
import type { ColumnDef, Row } from "@tanstack/react-table";
import _ from "lodash";
import { useCallback } from "react";
import * as React from "react";
import { useNavigate } from "react-router-dom";

import { GET_TODO_REATTACKS } from "./queries";
import type { IFindingFormatted, IFindingToReattackEdge } from "./types";
import { formatFindings } from "./utils";

import { GET_USER_ORGANIZATIONS_GROUPS } from "../queries";
import type { IOrganizationGroups } from "@types";
import { Table } from "components/table";
import { useTable } from "hooks";
import { useAudit } from "hooks/use-audit";
import { formatDateTime } from "utils/date";
import { Logger } from "utils/logger";

export const Reattacks: React.FC = (): JSX.Element => {
  const navigate = useNavigate();

  const tableRef = useTable("tblTodoReattacks");
  const columns: ColumnDef<IFindingFormatted>[] = [
    {
      accessorKey: "groupName",
      header: "Group name",
    },
    {
      accessorKey: "title",
      header: "Type",
    },
    {
      accessorFn: (row: IFindingFormatted): string =>
        row.verificationSummary.requested,
      header: "Requested vulns",
    },
    {
      accessorKey: "oldestReattackRequestedDate",
      cell: (cell): string => formatDateTime(String(cell.getValue() ?? "")),
      header: "Reattack date",
    },
  ];

  const { data: userData } = useQuery(GET_USER_ORGANIZATIONS_GROUPS, {
    fetchPolicy: "cache-first",
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.warning("An error occurred fetching user groups", error);
      });
    },
  });

  const { addAuditEvent } = useAudit();
  const { data, fetchMore } = useQuery(GET_TODO_REATTACKS, {
    onCompleted: (): void => {
      addAuditEvent("ToDo.Reattacks", "unknown");
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.warning(
          "An error occurred fetching user findings from dashboard",
          error,
        );
      });
    },
    skip: _.isUndefined(userData),
    variables: { first: 150 },
  });

  const organizations = userData === undefined ? [] : userData.me.organizations;
  const findings =
    _.isUndefined(data) || _.isEmpty(data)
      ? []
      : _.flatten(data.me.findingReattacksConnection.edges);
  const dataset = formatFindings(
    organizations as IOrganizationGroups[],
    findings as unknown as IFindingToReattackEdge[],
  );

  const handleNextPage = useCallback(async (): Promise<void> => {
    const pageInfo =
      data === undefined
        ? { endCursor: "", hasNextPage: false }
        : data.me.findingReattacksConnection.pageInfo;

    if (pageInfo.hasNextPage) {
      await fetchMore({ variables: { after: pageInfo.endCursor } });
    }
  }, [data, fetchMore]);

  const goToFinding = useCallback(
    (rowInfo: Row<IFindingFormatted>): ((event: React.FormEvent) => void) => {
      const orgName = rowInfo.original.organizationName ?? "";

      return (event: React.FormEvent): void => {
        navigate(
          `/orgs/${orgName}/groups/${rowInfo.original.groupName}/vulns/${rowInfo.original.id}/locations`,
        );
        event.preventDefault();
      };
    },
    [navigate],
  );

  return (
    <React.StrictMode>
      <Table
        columns={columns}
        csvConfig={{
          columns: [
            "organizationName",
            "groupName",
            "id",
            "title",
            "verificationSummary.requested",
            "oldestReattackRequestedDate",
            "url",
          ],
          export: true,
        }}
        data={dataset}
        onNextPage={handleNextPage}
        onRowClick={goToFinding}
        tableRef={tableRef}
      />
    </React.StrictMode>
  );
};
