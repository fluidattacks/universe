import { screen, waitFor } from "@testing-library/react";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import { Route, Routes } from "react-router-dom";

import {
  GET_FINDING_EMPTY_EVIDENCE,
  GET_FINDING_EVIDENCE_DRAFTS,
} from "./queries";

import { EvidenceDrafts } from ".";
import { GET_USER_ORGANIZATIONS_GROUPS } from "../queries";
import type {
  GetFindingEmptyEvidenceQuery as GetFindingEmptyEvidence,
  GetFindingEvidenceDraftsQuery as GetFindingEvidenceDrafts,
  GetUserOrganizationsGroupsAtDashboardQuery as GetUserOrganizationsGroups,
} from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";

describe("evidenceDrafts", (): void => {
  const evidence = {
    date: null,
    description: null,
    isDraft: null,
    url: null,
  };
  const evidences = {
    animation: evidence,
    evidence1: evidence,
    evidence2: evidence,
    evidence3: evidence,
    evidence4: evidence,
    evidence5: evidence,
    exploitation: evidence,
  };
  const graphqlMocked = graphql.link(LINK);
  const mockEmpty = graphqlMocked.query(
    GET_FINDING_EMPTY_EVIDENCE,
    (): StrictResponse<{ data: GetFindingEmptyEvidence }> => {
      return HttpResponse.json({
        data: {
          me: {
            __typename: "Me",
            findingEmptyEvidence: {
              __typename: "FindingsConnection",
              edges: [
                {
                  node: {
                    __typename: "Finding",
                    ...{
                      evidence: evidences,
                      groupName: "test2",
                      id: "3a4d494e-9fce-4a60-8a1e-58e728c7463e",
                      rejectedVulnerabilities: 1,
                      submittedVulnerabilities: 1,
                      title: "083. xml injection (xxe)",
                      vulnerabilitiesSummary: {
                        closed: 1,
                        open: 1,
                        openCritical: 1,
                        openHigh: 0,
                        openLow: 0,
                        openMedium: 0,
                      },
                    },
                  },
                },
              ],
              pageInfo: {
                __typename: "PageInfo",
                endCursor: "",
                hasNextPage: false,
              },
              total: 1,
            },
            userEmail: "test@fluidattacks.com",
          },
        },
      });
    },
  );

  const mockDrafts = graphqlMocked.query(
    GET_FINDING_EVIDENCE_DRAFTS,
    (): StrictResponse<{ data: GetFindingEvidenceDrafts }> => {
      return HttpResponse.json({
        data: {
          me: {
            __typename: "Me",
            findingEvidenceDrafts: {
              __typename: "FindingsConnection",
              edges: [
                {
                  node: {
                    __typename: "Finding",
                    ...{
                      evidence: {
                        ...evidences,
                        evidence1: {
                          date: "",
                          description: "Test description",
                          isDraft: true,
                          url: "some_file.png",
                        },
                      },
                      groupName: "test1",
                      id: "19fcafbb-109c-4168-a8ea-a4dfee06e46e",
                      rejectedVulnerabilities: 1,
                      submittedVulnerabilities: 1,
                      title: "014. insecure functionality",
                      vulnerabilitiesSummary: {
                        closed: 0,
                        open: 0,
                        openCritical: 0,
                        openHigh: 0,
                        openLow: 0,
                        openMedium: 0,
                      },
                    },
                  },
                },
              ],
              pageInfo: {
                __typename: "PageInfo",
                endCursor: "",
                hasNextPage: false,
              },
              total: 1,
            },
            userEmail: "test@fluidattacks.com",
          },
        },
      });
    },
  );

  const mockOrganizations = graphqlMocked.query(
    GET_USER_ORGANIZATIONS_GROUPS,
    (): StrictResponse<{ data: GetUserOrganizationsGroups }> => {
      return HttpResponse.json({
        data: {
          me: {
            __typename: "Me",
            organizations: [
              {
                __typename: "Organization",
                groups: [
                  {
                    __typename: "Group",
                    name: "test1",
                    permissions: [],
                    serviceAttributes: [],
                  },
                ],
                name: "orgtest",
              },
            ],
            userEmail: "test@fluidattacks.com",
          },
        },
      });
    },
  );

  it("should render a component and its columns and data", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route element={<EvidenceDrafts />} path={"/todos/evidence-drafts"} />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/todos/evidence-drafts"],
        },
        mocks: [mockDrafts, mockEmpty, mockOrganizations],
      },
    );

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(3);
    });

    expect(screen.queryAllByRole("table")).toHaveLength(1);
    expect(
      screen.queryByRole("link", { name: "083. xml injection (xxe)" }),
    ).toBeInTheDocument();
    expect(screen.getByText("Group name")).toBeInTheDocument();
    expect(screen.getByText("Type")).toBeInTheDocument();
    expect(
      screen.queryByRole("link", { name: "014. insecure functionality" }),
    ).toBeInTheDocument();
    expect(
      screen.queryByRole("columnheader", { name: "Has evidence drafts" }),
    ).toBeInTheDocument();
    expect(
      screen.queryByRole("columnheader", { name: "Has approved evidence" }),
    ).toBeInTheDocument();
    expect(
      screen.queryByRole("columnheader", { name: "Has location drafts" }),
    ).toBeInTheDocument();
    expect(screen.getAllByRole("row")[1].textContent).toBe(
      ["Test1", "014. insecure functionality", "Yes", "Yes", "No", "No"].join(
        "",
      ),
    );
    expect(screen.getAllByRole("row")[2].textContent).toBe(
      ["Test2", "083. xml injection (xxe)", "No", "Yes", "No", "Yes"].join(""),
    );

    await waitFor((): void => {
      expect(screen.getAllByRole("link")[2]).toHaveAttribute(
        "href",
        "/orgs/orgtest/groups/test1/vulns/19fcafbb-109c-4168-a8ea-a4dfee06e46e/evidence",
      );
    });

    expect(screen.getAllByRole("link")[4]).toHaveAttribute(
      "href",
      "/groups/test2/vulns/3a4d494e-9fce-4a60-8a1e-58e728c7463e/evidence",
    );
  });
});
