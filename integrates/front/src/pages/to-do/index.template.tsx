import { Container } from "@fluidattacks/design";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

import { SectionHeader } from "components/section-header";
import {
  authzPermissionsContext,
  userLevelPermissions,
} from "context/authz/config";

const ToDoTemplate: React.FC<Readonly<{ children: JSX.Element }>> = ({
  children,
}): JSX.Element => {
  const theme = useTheme();
  const { t } = useTranslation();
  const url = "/todos";

  const canRenderReattacks = userLevelPermissions.can(
    "front_can_retrieve_todo_reattacks",
  );
  const canRenderLocation = userLevelPermissions.can(
    "front_can_retrieve_todo_locations_drafts",
  );
  const canRenderDrafts = userLevelPermissions.can(
    "front_can_retrieve_todo_evidence_drafts",
  );
  const canRenderSeverity = userLevelPermissions.can(
    "front_can_retrieve_vulnerabilities_severity_requests",
  );

  return (
    <Container bgColor={theme.palette.gray[50]} height={"100%"}>
      <authzPermissionsContext.Provider value={userLevelPermissions}>
        <SectionHeader
          header={"Your to-do list"}
          tabs={[
            {
              id: "assignedLocations",
              label: t("todoList.tabs.assignedLocations"),
              link: `${url}/assigned-locations`,
              tooltip: t("todoList.tooltip.assignedLocations"),
            },
            {
              id: "fixPriority",
              label: t("todoList.tabs.fixPriority.title"),
              link: `${url}/fix-priority`,
              tooltip: t("todoList.tooltip.fixPriority"),
            },
            ...(canRenderReattacks
              ? [
                  {
                    id: "Reattacks",
                    label: t("todoList.tabs.reattacks"),
                    link: `${url}/reattacks`,
                    tooltip: t("todoList.tooltip.reattacks"),
                  },
                ]
              : []),
            ...(canRenderLocation
              ? [
                  {
                    id: "locationDrafts",
                    label: t("todoList.tabs.locationDrafts"),
                    link: `${url}/location-drafts`,
                    tooltip: t("todoList.tooltip.locationDrafts"),
                  },
                ]
              : []),
            ...(canRenderSeverity
              ? [
                  {
                    id: "vulnSeverityRequests",
                    label: t("todoList.tabs.vulnSeverityRequests"),
                    link: `${url}/vuln-severity-requests`,
                    tooltip: t("todoList.tooltip.vulnSeverityRequests"),
                  },
                ]
              : []),
            ...(canRenderDrafts
              ? [
                  {
                    id: "evidenceDrafts",
                    label: t("todoList.tabs.evidenceDrafts.title"),
                    link: `${url}/evidence-drafts`,
                    tooltip: t("todoList.tooltip.evidenceDrafts"),
                  },
                ]
              : []),
            {
              id: "tasksEvents",
              label: t("Events"),
              link: `${url}/events`,
              tooltip: t("todoList.tooltip.events"),
            },
          ]}
        />
      </authzPermissionsContext.Provider>
      <Container px={1.25} py={1.25}>
        {children}
      </Container>
    </Container>
  );
};

export { ToDoTemplate };
