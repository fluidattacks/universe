import * as React from "react";
import { Navigate, Route, Routes } from "react-router-dom";

import { AssignedVulnerabilities } from "./assigned-vulnerabilities";
import { Events } from "./events";
import { EvidenceDrafts } from "./evidence-drafts";
import { FixPriority } from "./fix-priority";
import { ToDoTemplate } from "./index.template";
import { Reattacks } from "./reattacks";
import { VulnerabilitySeverityUpdateRequests } from "./vuln-severity-requests";
import { VulnerabilityDrafts } from "./vulnerability-drafts";

const ToDo: React.FC = (): JSX.Element => {
  return (
    <ToDoTemplate>
      <Routes>
        <Route
          element={<AssignedVulnerabilities />}
          path={`/assigned-locations/:vulnerabilityId?`}
        />
        <Route
          element={<VulnerabilityDrafts />}
          path={`/location-drafts/:vulnerabilityId?`}
        />
        <Route element={<EvidenceDrafts />} path={`/evidence-drafts`} />
        <Route element={<Reattacks />} path={`/reattacks`} />
        <Route element={<Events />} path={`/events`} />
        <Route
          element={<FixPriority />}
          path={`/fix-priority/:vulnerabilityId?`}
        />
        <Route
          element={<Navigate replace={true} to={"/todos/assigned-locations"} />}
          path={"*"}
        />
        <Route
          element={<VulnerabilitySeverityUpdateRequests />}
          path={`/vuln-severity-requests/:vulnerabilityId?`}
        />
      </Routes>
    </ToDoTemplate>
  );
};

export { ToDo };
