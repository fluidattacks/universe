import type { Row } from "@tanstack/react-table";

import type { IGetUserOrganizationsGroups, IOrganizationGroups } from "@types";
import type { IVulnRowAttr } from "features/vulnerabilities/types";
import { getNonSelectableVulnerabilitiesOnReattackIds } from "features/vulnerabilities/utils";

const getSeverity = (
  cvssVersion: {
    accessorKey: string;
    header: string;
  },
  row: IVulnRowAttr,
): number => {
  if (cvssVersion.accessorKey === "severityThreatScore") {
    return row.severityThreatScore;
  }

  return row.severityTemporalScore;
};

const isValidVulnerability = (
  vulnerability: IVulnRowAttr,
  groups: IOrganizationGroups["groups"],
): boolean => {
  const firstFilteredVulnerabilities =
    getNonSelectableVulnerabilitiesOnReattackIds([vulnerability]);
  const filteredGroups = groups.filter(
    (group): boolean =>
      group.name.toLowerCase() === vulnerability.groupName.toLowerCase(),
  );

  if (filteredGroups.length === 0) {
    return false;
  }

  return (
    filteredGroups[0].serviceAttributes.includes("is_continuous") &&
    !firstFilteredVulnerabilities.includes(vulnerability.id) &&
    (filteredGroups[0].serviceAttributes.includes("has_advanced") ||
      vulnerability.source === "machine")
  );
};

const filteredContinuousVulnerabilitiesOnReattackIds = (
  vulnerabilities: IVulnRowAttr[],
  groups: IOrganizationGroups["groups"],
): string[] => {
  return vulnerabilities.reduce(
    (
      nonSelectableVulnerabilities: string[],
      vulnerability: IVulnRowAttr,
    ): string[] => {
      return isValidVulnerability(vulnerability, groups)
        ? [...nonSelectableVulnerabilities, vulnerability.id]
        : nonSelectableVulnerabilities;
    },
    [],
  );
};

const getUserGroups = (
  userData: IGetUserOrganizationsGroups | undefined,
): IOrganizationGroups["groups"] =>
  userData === undefined
    ? []
    : userData.me.organizations.reduce(
        (
          previousValue: IOrganizationGroups["groups"],
          currentValue,
        ): IOrganizationGroups["groups"] => [
          ...previousValue,
          ...currentValue.groups,
        ],
        [],
      );

const isRejectedOrSafeOrSubmitted = (row: Row<IVulnRowAttr>): boolean =>
  ["REJECTED", "SAFE", "SUBMITTED"].includes(row.original.state);

const isVerificationRequestedOrOnHold = (row: Row<IVulnRowAttr>): boolean =>
  ["requested", "on_hold"].includes(
    (row.original.verification ?? "").toLowerCase(),
  );

export {
  isRejectedOrSafeOrSubmitted,
  isVerificationRequestedOrOnHold,
  isValidVulnerability,
  filteredContinuousVulnerabilitiesOnReattackIds,
  getUserGroups,
  getSeverity,
};
