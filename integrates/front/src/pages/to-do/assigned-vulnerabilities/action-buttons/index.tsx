import { Button, Container } from "@fluidattacks/design";
import { Fragment, useMemo } from "react";
import { useTranslation } from "react-i18next";

import { Can } from "context/authz/can";
import { Have } from "context/authz/have";

interface IReattackVulnButtonProps {
  areVulnsSelected: boolean;
  areVulnerabilitiesReattacked: boolean;
  cvssVersion?: {
    accessorKey: string;
    header: string;
  };
  isEditing: boolean;
  isRequestingReattack: boolean;
  onChangeCvssVersion?: () => void;
  onRequestReattack: () => void;
  openModal: () => void;
  refreshAssigned: () => void;
}

export const ActionButtons = ({
  areVulnsSelected,
  isEditing,
  areVulnerabilitiesReattacked,
  isRequestingReattack,
  onRequestReattack,
  openModal,
  refreshAssigned,
}: Readonly<IReattackVulnButtonProps>): JSX.Element => {
  const { t } = useTranslation();

  const tooltipMessage = useMemo((): string => {
    if (isRequestingReattack) {
      return t("searchFindings.tabVuln.buttonsTooltip.cancel");
    }

    return t("searchFindings.tabDescription.requestVerify.tooltip");
  }, [isRequestingReattack, t]);

  const isNotEditing = !isEditing;

  return (
    <Fragment>
      <Button
        icon={"refresh"}
        id={"refresh-assigned"}
        onClick={refreshAssigned}
        variant={"ghost"}
      >
        {t("todoList.assignedVulnerabilities.refresh")}
      </Button>
      <Have I={"is_continuous"}>
        <Can
          do={
            "integrates_api_mutations_request_vulnerabilities_verification_mutate"
          }
        >
          <Container display={"flex"} gap={0.5}>
            {isRequestingReattack ? (
              <Button
                disabled={!areVulnsSelected}
                icon={"repeat"}
                id={"confirm-reattack"}
                onClick={openModal}
                variant={"secondary"}
              >
                {t("searchFindings.tabVuln.buttons.reattack")}
              </Button>
            ) : undefined}
            {isNotEditing ? (
              <Button
                disabled={areVulnerabilitiesReattacked || !areVulnsSelected}
                icon={"repeat"}
                id={"start-reattack"}
                onClick={onRequestReattack}
                tooltip={tooltipMessage}
                variant={"secondary"}
              >
                {isRequestingReattack
                  ? t("searchFindings.tabVuln.buttonsTooltip.cancel")
                  : t("searchFindings.tabDescription.requestVerify.text")}
              </Button>
            ) : undefined}
          </Container>
        </Can>
      </Have>
    </Fragment>
  );
};
