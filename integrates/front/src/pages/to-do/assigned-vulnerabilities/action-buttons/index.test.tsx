import { PureAbility } from "@casl/ability";
import { waitFor } from "@testing-library/react";

import { ActionButtons } from ".";
import {
  authzGroupContext,
  authzPermissionsContext,
} from "context/authz/config";
import { render } from "mocks";

describe("vulnerabilityDrafts", (): void => {
  it("does not show start review button if user do not have the required permissions", (): void => {
    expect.hasAssertions();

    render(
      <ActionButtons
        areVulnerabilitiesReattacked={false}
        areVulnsSelected={false}
        isEditing={false}
        isRequestingReattack={false}
        onRequestReattack={jest.fn()}
        openModal={jest.fn()}
        refreshAssigned={jest.fn()}
      />,
    );

    expect(document.getElementById("refresh-assigned")).toBeInTheDocument();
    expect(document.getElementById("confirm-reattack")).not.toBeInTheDocument();
    expect(document.getElementById("start-reattack")).not.toBeInTheDocument();
  });

  it("should call callbacks", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      {
        action:
          "integrates_api_mutations_request_vulnerabilities_verification_mutate",
      },
    ]);

    const hanldeReattackRequest = jest.fn();
    const refreshAssigned = jest.fn();

    render(
      <authzGroupContext.Provider
        value={new PureAbility([{ action: "is_continuous" }])}
      >
        <authzPermissionsContext.Provider value={mockedPermissions}>
          <ActionButtons
            areVulnerabilitiesReattacked={false}
            areVulnsSelected={true}
            isEditing={false}
            isRequestingReattack={false}
            onRequestReattack={hanldeReattackRequest}
            openModal={jest.fn()}
            refreshAssigned={refreshAssigned}
          />
        </authzPermissionsContext.Provider>
      </authzGroupContext.Provider>,
    );

    await waitFor((): void => {
      expect(document.getElementById("start-reattack")).toBeInTheDocument();
    });

    const refreshAssignedButton = document.getElementById("refresh-assigned");
    const startReattackwButton = document.getElementById("start-reattack");
    const confirmReattackwButton = document.getElementById("confirm-reattack");

    expect(refreshAssignedButton).toBeInTheDocument();
    expect(confirmReattackwButton).not.toBeInTheDocument();
    expect(startReattackwButton).not.toBeDisabled();

    startReattackwButton?.click();

    expect(hanldeReattackRequest).toHaveBeenCalledTimes(1);

    refreshAssignedButton?.click();

    expect(refreshAssigned).toHaveBeenCalledTimes(1);
  });
});
