import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import { Route, Routes } from "react-router-dom";

import { GET_ME_VULNERABILITIES_ASSIGNED } from "./queries";

import { AssignedVulnerabilities } from ".";
import { GET_USER_ORGANIZATIONS_GROUPS } from "../queries";
import { AllGroupPermissionsProvider } from "context/authz/all-group-permissions-provider";
import { GET_GROUP_USERS } from "features/vulnerabilities/queries";
import type {
  GetGroupUsersQuery as GetGroupUsers,
  GetMeVulnerabilitiesAssignedQuery as GetMeVulnerabilitiesAssigned,
  GetUserOrganizationsGroupsAtDashboardQuery as GetUserOrganizationsGroups,
} from "gql/graphql";
import {
  InvitationState,
  RootCriticality,
  Technique,
  VulnerabilityState,
  VulnerabilityTreatment,
} from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";

describe("assignedVulnerabilities", (): void => {
  const graphqlMocked = graphql.link(LINK);
  const mocksVulnerabilities = [
    graphqlMocked.query(
      GET_ME_VULNERABILITIES_ASSIGNED,
      (): StrictResponse<{ data: GetMeVulnerabilitiesAssigned }> => {
        return HttpResponse.json({
          data: {
            me: {
              __typename: "Me",
              userEmail: "test@test.test",
              vulnerabilitiesAssigned: [
                {
                  __typename: "Vulnerability",
                  finding: {
                    __typename: "Finding",
                    id: "",
                    title: "finding 1",
                  },
                  ...{
                    advisories: null,
                    closingDate: null,
                    customSeverity: 0,
                    externalBugTrackingSystem: null,
                    findingId: "422286126",
                    groupName: "group1",
                    id: "89521e9a-b1a3-4047-a16e-15d530dc1340",
                    lastEditedBy: "test@test.test",
                    lastStateDate: "2019-07-05 09:56:40",
                    lastTreatmentDate: "2019-07-05 09:56:40",
                    lastVerificationDate: null,
                    priority: 0,
                    remediated: false,
                    reportDate: "2019-07-05 09:56:40",
                    root: {
                      __typename: "GitRoot",
                      branch: "master",
                      criticality: RootCriticality.Low,
                    },
                    rootNickname: "https:",
                    severityTemporalScore: 3.9,
                    severityThreatScore: 3.9,
                    severityVector:
                      "AV:A/AC:H/PR:L/UI:R/S:C/C:L/I:L/A:L/E:F/RL:O/RC:U",
                    severityVectorV4:
                      "CVSS:4.0/AV:A/AC:H/PR:L/UI:P/VC:L/VI:L/VA:L/SC:L/SI:L/SA:L/E:A",
                    source: "asm",
                    specific: "specific-1",
                    state: VulnerabilityState.Vulnerable,
                    stateReasons: null,
                    stream: "home > blog > articulo",
                    tag: "tag-1, tag-2",
                    technique: Technique.Dast,
                    treatmentAcceptanceDate: "",
                    treatmentAcceptanceStatus: "",
                    treatmentAssigned: "assigned-user-1",
                    treatmentJustification: "test progress justification",
                    treatmentStatus: VulnerabilityTreatment.Accepted,
                    treatmentUser: "test@test.test",
                    verification: null,
                    verificationJustification: null,
                    vulnerabilityType: "inputs",
                    where: "https://example.com/inputs",
                    zeroRisk: null,
                  },
                },
                {
                  __typename: "Vulnerability",
                  finding: {
                    __typename: "Finding",
                    id: "",
                    title: "finding 2",
                  },
                  ...{
                    advisories: null,
                    closingDate: null,
                    customSeverity: 4.3,
                    externalBugTrackingSystem: null,
                    findingId: "422286126",
                    groupName: "group2",
                    id: "6903f3e4-a8ee-4a5d-ac38-fb738ec7e540",
                    lastEditedBy: "test@test.test",
                    lastStateDate: "2019-07-05 09:56:40",
                    lastTreatmentDate: "2019-07-05 09:56:40",
                    lastVerificationDate: null,
                    priority: 0,
                    remediated: false,
                    reportDate: "2020-07-05 09:56:40",
                    root: {
                      __typename: "GitRoot",
                      branch: "master",
                      criticality: RootCriticality.Low,
                    },
                    rootNickname: "https:",
                    severityTemporalScore: 3.4,
                    severityThreatScore: 3.3,
                    severityVector:
                      "AV:A/AC:H/PR:L/UI:R/S:C/C:L/I:L/A:L/E:F/RL:O/RC:U",
                    severityVectorV4:
                      "CVSS:4.0/AV:A/AC:H/PR:L/UI:P/VC:L/VI:L/VA:L/SC:L/SI:L/SA:L/E:A",
                    source: "asm",
                    specific: "specific-3",
                    state: VulnerabilityState.Vulnerable,
                    stateReasons: null,
                    stream: null,
                    tag: "tag-3",
                    technique: Technique.Sast,
                    treatmentAcceptanceDate: "",
                    treatmentAcceptanceStatus: "",
                    treatmentAssigned: "assigned-user-1",
                    treatmentJustification: "test progress justification",
                    treatmentStatus: VulnerabilityTreatment.InProgress,
                    treatmentUser: "test@test.test",
                    verification: null,
                    verificationJustification: null,
                    vulnerabilityType: "lines",
                    where: "https://example.com/tests",
                    zeroRisk: null,
                  },
                },
              ],
            },
          },
        });
      },
    ),
  ];
  const mocksUserGroups = [
    graphqlMocked.query(
      GET_USER_ORGANIZATIONS_GROUPS,
      (): StrictResponse<{ data: GetUserOrganizationsGroups }> => {
        return HttpResponse.json({
          data: {
            me: {
              __typename: "Me",
              organizations: [
                {
                  groups: [
                    {
                      __typename: "Group",
                      name: "group1",
                      permissions: [
                        "integrates_api_mutations_request_vulnerabilities_verification_mutate",
                        "integrates_api_mutations_update_vulnerabilities_treatment_mutate",
                        "integrates_api_resolvers_group_stakeholders_resolve",
                      ],
                      serviceAttributes: ["has_advanced", "is_continuous"],
                    },
                    {
                      __typename: "Group",
                      name: "group2",
                      permissions: [
                        "integrates_api_mutations_request_vulnerabilities_verification_mutate",
                        "integrates_api_mutations_update_vulnerabilities_treatment_mutate",
                      ],
                      serviceAttributes: [],
                    },
                  ],
                  name: "orgtest",
                },
              ],
              userEmail: "test@test.test",
            },
          },
        });
      },
    ),
  ];

  const mocksGroupStakeholder = [
    graphqlMocked.query(
      GET_GROUP_USERS,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: GetGroupUsers }> => {
        const { groupName } = variables;

        if (groupName === "group1") {
          return HttpResponse.json({
            data: {
              group: {
                __typename: "Group",
                name: groupName,
                stakeholders: [
                  {
                    __typename: "Stakeholder",
                    email: "manager1_test@test.test",
                    invitationState: InvitationState.Registered,
                    role: "user",
                  },
                ],
              },
            },
          });
        }
        if (groupName === "group2") {
          return HttpResponse.json({
            data: {
              group: {
                __typename: "Group",
                name: groupName,
                stakeholders: [
                  {
                    __typename: "Stakeholder",
                    email: "manager1_test@test.test",
                    invitationState: InvitationState.Registered,
                    role: "user",
                  },
                ],
              },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Get group user error")],
        });
      },
    ),
  ];

  it("should handle reattack button basic", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const { container } = render(
      <AllGroupPermissionsProvider>
        <Routes>
          <Route element={<AssignedVulnerabilities />} path={"/todos"} />
        </Routes>
      </AllGroupPermissionsProvider>,
      {
        memoryRouter: {
          initialEntries: ["/todos"],
        },
        mocks: [
          ...mocksVulnerabilities,
          ...mocksUserGroups,
          ...mocksGroupStakeholder,
        ],
      },
    );

    await waitFor((): void => {
      expect(
        screen.getByRole("cell", { name: "https://example.com/inputs" }),
      ).toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(screen.queryByText("Severity (v3.1)")).not.toBeInTheDocument();
    });

    expect(screen.queryByText("Severity (v4.0)")).toBeInTheDocument();

    await userEvent.click(screen.getByRole("checkbox", { name: "cvssToggle" }));

    await userEvent.click(
      container.querySelector("#refresh-assigned") as Element,
    );

    expect(screen.getAllByRole("checkbox")[2]).not.toBeChecked();

    await userEvent.click(screen.getAllByRole("checkbox")[2]);
    await userEvent.click(screen.getAllByRole("checkbox")[3]);

    expect(screen.getAllByRole("checkbox")[2]).toBeChecked();

    await waitFor((): void => {
      expect(
        screen.getByText("searchFindings.tabDescription.requestVerify.text"),
      ).not.toBeDisabled();
    });

    expect(
      screen.queryByText(
        "searchFindings.tabDescription.remediationModal.justification",
      ),
    ).not.toBeInTheDocument();

    await userEvent.click(
      screen.getByText("searchFindings.tabDescription.requestVerify.text"),
    );

    await waitFor((): void => {
      expect(
        screen.queryByText(
          "searchFindings.tabDescription.remediationModal.justification",
        ),
      ).toBeInTheDocument();
    });
  });

  it("should handle edit button basic", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    render(
      <AllGroupPermissionsProvider>
        <Routes>
          <Route element={<AssignedVulnerabilities />} path={"/todos"} />
        </Routes>
      </AllGroupPermissionsProvider>,
      {
        memoryRouter: {
          initialEntries: ["/todos"],
        },
        mocks: [
          ...mocksVulnerabilities,
          ...mocksUserGroups,
          ...mocksGroupStakeholder,
        ],
      },
    );

    await waitFor((): void => {
      expect(screen.getAllByRole("row")).toHaveLength(3);
    });

    await userEvent.click(screen.getAllByRole("checkbox")[2]);
    await userEvent.click(screen.getAllByRole("checkbox")[3]);

    await waitFor((): void => {
      expect(screen.getAllByRole("checkbox")[3]).toBeChecked();
    });

    await userEvent.click(
      screen.getByText("searchFindings.tabVuln.buttons.edit"),
    );
    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.tabDescription.editVuln"),
      ).toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(
        screen.getByText("searchFindings.tabDescription.treatment.title"),
      ).toBeInTheDocument();
    });

    await userEvent.click(screen.getByRole("button", { name: "Cancel" }));

    await waitFor((): void => {
      expect(
        screen.getByText("searchFindings.tabDescription.treatment.title"),
      ).toBeInTheDocument();
    });

    await userEvent.click(screen.getByRole("button", { name: "Cancel" }));

    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.tabDescription.editVuln"),
      ).not.toBeInTheDocument();
    });
  });

  it("should filter", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    render(
      <AllGroupPermissionsProvider>
        <Routes>
          <Route element={<AssignedVulnerabilities />} path={"/todos"} />
        </Routes>
      </AllGroupPermissionsProvider>,
      {
        memoryRouter: {
          initialEntries: ["/todos"],
        },
        mocks: [
          ...mocksVulnerabilities,
          ...mocksUserGroups,
          ...mocksGroupStakeholder,
        ],
      },
    );

    await waitFor((): void => {
      expect(screen.getAllByRole("row")).toHaveLength(3);
    });

    const [filter] = Array.from(document.querySelectorAll("#filterBtn"));

    expect(filter).toBeInTheDocument();

    const user = userEvent.setup();
    await user.click(filter);

    const clearFilter = async (): Promise<void> => {
      await user.click(screen.getByText("filter.cancel"));

      expect(screen.getAllByRole("row")).toHaveLength(3);
    };

    const [orgInput] = Array.from(document.querySelectorAll("#groupName"));

    expect(orgInput).toBeInTheDocument();

    await user.type(orgInput, "group1");

    expect(screen.getAllByRole("row")).toHaveLength(2);
    expect(screen.getByText(/finding 1/iu)).toBeInTheDocument();

    await user.click(screen.getByText("filter.cancel"));

    expect(screen.getAllByRole("row")).toHaveLength(3);

    const [findingTitleInput] = Array.from(
      document.querySelectorAll("#finding-title"),
    );

    expect(findingTitleInput).not.toBeNull();

    await user.type(findingTitleInput, "finding 2");

    expect(
      screen.getAllByText(
        "searchFindings.tabVuln.vulnTable.vulnerabilityType.title",
      )[0].nextElementSibling?.textContent,
    ).toBe("finding 2");
    expect(screen.getAllByRole("row")).toHaveLength(2);
    expect(screen.getByText(/finding 2/iu)).toBeInTheDocument();

    await clearFilter();

    const [tagInput] = Array.from(document.querySelectorAll("#tag"));

    expect(tagInput).not.toBeNull();

    await user.type(tagInput, "tag-1, tag-2");

    expect(screen.getAllByRole("row")).toHaveLength(2);

    expect(
      screen.getAllByText("searchFindings.tabVuln.vulnTable.tags")[0]
        .nextElementSibling?.textContent,
    ).toBe("tag-1, tag-2");
    expect(screen.getByText(/finding 1/iu)).toBeInTheDocument();

    sessionStorage.clear();
    localStorage.clear();
  });
});
