import type { ApolloQueryResult } from "@apollo/client";
import { useQuery } from "@apollo/client";
import { useMemo } from "react";

import { GET_ME_VULNERABILITIES_ASSIGNED } from "./queries";

import type { IGetUserOrganizationsGroups } from "@types";
import type { IVulnRowAttr } from "features/vulnerabilities/types";
import { formatVulnerabilitiesTreatment } from "features/vulnerabilities/utils";
import type { GetMeVulnerabilitiesAssignedQuery } from "gql/graphql";
import { useAudit } from "hooks/use-audit";
import { Logger } from "utils/logger";

interface IUseVulnsAssignedQuery {
  vulnerabilities: IVulnRowAttr[];
  refetchVulnerabilitiesAssigned: () => Promise<
    ApolloQueryResult<GetMeVulnerabilitiesAssignedQuery>
  >;
}

const useVulnsAssignedQuery = (
  userData: IGetUserOrganizationsGroups | undefined,
): IUseVulnsAssignedQuery => {
  const { addAuditEvent } = useAudit();

  const { data, refetch } = useQuery(GET_ME_VULNERABILITIES_ASSIGNED, {
    fetchPolicy: "cache-first",
    onCompleted: (): void => {
      addAuditEvent("ToDo.AssignedVulnerabilities", "unknown");
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.warning(
          "An error occurred fetching vulnerabilities assigned from dashboard",
          error,
        );
      });
    },
  });

  const vulnerabilities = useMemo(
    (): IVulnRowAttr[] =>
      formatVulnerabilitiesTreatment({
        organizationsGroups: userData?.me.organizations ?? [],
        vulnerabilities:
          data === undefined || userData === undefined
            ? []
            : (data.me.vulnerabilitiesAssigned as IVulnRowAttr[]),
      }),
    [data, userData],
  );

  return { refetchVulnerabilitiesAssigned: refetch, vulnerabilities };
};

export { useVulnsAssignedQuery };
