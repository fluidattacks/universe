interface IRootAttr {
  id: string;
  nickname: string;
}

interface IEventAttr {
  __typename: "Event";
  closingDate: string;
  detail: string;
  environment: string | null;
  eventDate: string;
  eventStatus: string;
  eventType: string;
  id: string;
  groupName: string;
  organization: string;
  root: IRootAttr | null;
  startClosingDate: string;
}

export type { IEventAttr };
