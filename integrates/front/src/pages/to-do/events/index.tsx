import type { ColumnDef, Row } from "@tanstack/react-table";
import mixpanel from "mixpanel-browser";
import type { FormEvent } from "react";
import { useCallback, useState } from "react";
import * as React from "react";
import { useNavigate } from "react-router-dom";

import { useEventsQuery } from "./hooks";
import type { IEventAttr } from "./types";

import { Filters } from "components/filter";
import type { IFilter, ISelectedOptions } from "components/filter/types";
import { Table } from "components/table";
import { filterDate } from "components/table/utils";
import { statusFormatter } from "features/vulnerabilities/formatters/status";
import { setFiltersUtil, useTable } from "hooks";
import { formatDate } from "utils/date";

const tableColumns: ColumnDef<IEventAttr>[] = [
  {
    accessorFn: (row): string => row.organization,
    enableColumnFilter: false,
    header: "Organization",
  },
  {
    accessorFn: (row): string => row.groupName,
    enableColumnFilter: false,
    header: "Group name",
  },
  {
    accessorFn: (row): string | undefined => row.root?.nickname,
    enableColumnFilter: false,
    header: "Root",
  },
  {
    accessorKey: "eventDate",
    cell: (cell): string => formatDate(String(cell.getValue() ?? "")),
    filterFn: filterDate,
    header: "Event date",
    meta: { filterType: "dateRange" },
  },
  {
    accessorKey: "detail",
    header: "Description",
  },
  {
    accessorKey: "eventType",
    header: "Type",
    meta: { filterType: "select" },
  },
  {
    accessorKey: "eventStatus",
    cell: (cell): JSX.Element => statusFormatter(String(cell.getValue())),
    header: "Status",
    meta: { filterType: "select" },
  },
];

const Events: React.FC = (): JSX.Element => {
  const navigate = useNavigate();

  const tableRef = useTable("tblGroupVulnerabilities");

  const [filters, setFilters] = useState<IFilter<IEventAttr>[]>([
    {
      id: "eventDate",
      key: "eventDate",
      label: "Event date",
      type: "dateRange",
      valueType: "rangeValues",
    },
    {
      id: "detail",
      key: "detail",
      label: "Description",
      type: "text",
    },
    {
      id: "eventType",
      key: "eventType",
      label: "Type",
      selectOptions: (events): ISelectedOptions[] =>
        events.map((event): ISelectedOptions => ({ value: event.eventType })),
      type: "select",
    },
    {
      id: "eventStatus",
      key: "eventStatus",
      label: "Status",
      selectOptions: (events): ISelectedOptions[] =>
        events.map((event): ISelectedOptions => ({ value: event.eventStatus })),
      type: "select",
    },
    {
      id: "organization",
      key: "organization",
      label: "Organization",
      type: "text",
    },
    {
      id: "groupName",
      key: "groupName",
      label: "Group name",
      type: "text",
    },
  ]);

  const { events, loading } = useEventsQuery();
  const filteredDataset = setFiltersUtil(events, filters);

  const goToEvent = useCallback(
    (rowInfo: Row<IEventAttr>): ((event: FormEvent) => void) => {
      return (event: FormEvent): void => {
        mixpanel.track("ReadEvent");
        navigate(
          `/groups/${rowInfo.original.groupName}/events/${rowInfo.original.id}/description`,
        );
        event.preventDefault();
      };
    },
    [navigate],
  );

  return (
    <Table
      columns={tableColumns}
      csvConfig={{ export: true }}
      data={filteredDataset}
      filters={
        <Filters dataset={events} filters={filters} setFilters={setFilters} />
      }
      loadingData={loading}
      onRowClick={goToEvent}
      tableRef={tableRef}
    />
  );
};

export { Events };
