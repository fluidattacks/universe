import { graphql } from "gql";

export const GET_TODO_EVENTS = graphql(`
  query GetTodoEvents($after: String!, $first: Int!) {
    me {
      userEmail
      pendingEvents(after: $after, first: $first) {
        edges {
          node {
            id
            detail
            environment
            eventDate
            eventStatus
            eventType
            groupName
            organization
            root {
              ... on GitRoot {
                id
                nickname
              }
              ... on URLRoot {
                id
                nickname
              }
              ... on IPRoot {
                id
                nickname
              }
            }
          }
        }
        pageInfo {
          endCursor
          hasNextPage
        }
      }
    }
  }
`);
