import { screen } from "@testing-library/react";

import { Login } from ".";
import { render } from "mocks";

describe("login", (): void => {
  it("should render a component", (): void => {
    expect.hasAssertions();

    render(<Login />);

    expect(
      screen.getByText("login.header.title1", { exact: false }),
    ).toBeInTheDocument();
    expect(
      screen.getByText("login.header.title2", { exact: false }),
    ).toBeInTheDocument();
    expect(
      screen.getByText("login.header.text", { exact: false }),
    ).toBeInTheDocument();
  });

  it("should allow login with google", (): void => {
    expect.hasAssertions();

    render(<Login />);

    expect(screen.getByText("Continue with Google")).toBeInTheDocument();
    expect(screen.getByText("Continue with Microsoft")).toBeInTheDocument();
    expect(screen.getByText("Continue with Bitbucket")).toBeInTheDocument();
  });

  it("should show footer links", (): void => {
    expect.hasAssertions();

    render(<Login />);

    expect(
      screen.getByRole("link", { name: "login.footer.term1" }),
    ).toHaveAttribute("href", "https://fluidattacks.com/terms-use/");
    expect(
      screen.getByRole("link", { name: "login.footer.term3" }),
    ).toHaveAttribute("href", "https://fluidattacks.com/privacy/");
  });
});
