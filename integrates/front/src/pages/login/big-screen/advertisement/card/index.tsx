import { placeholder } from "@cloudinary/react";
import { CloudImage, Container, Heading, Text } from "@fluidattacks/design";
import * as React from "react";
import { useTheme } from "styled-components";

interface ICardProps {
  readonly description: string;
  readonly imagePublicId: string;
  readonly title: string;
}

const Card: React.FC<ICardProps> = ({
  description,
  imagePublicId,
  title,
}: ICardProps): JSX.Element => {
  const theme = useTheme();

  return (
    <Container maxWidth={"766px"}>
      <CloudImage
        plugins={[placeholder()]}
        publicId={imagePublicId}
        width={"100%"}
      />
      <Container ml={0.75} mr={0.75}>
        <Heading
          color={"white"}
          fontWeight={"bold"}
          lineSpacing={2.5}
          size={"lg"}
          textAlign={"center"}
          wordBreak={"break-word"}
        >
          {title}
        </Heading>
        <Text
          color={theme.palette.gray[100]}
          lineSpacing={1.5}
          mt={0.75}
          size={"md"}
          textAlign={"center"}
          wordBreak={"break-word"}
        >
          {description}
        </Text>
      </Container>
    </Container>
  );
};

export { Card };
