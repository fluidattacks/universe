import { CloudImage } from "@fluidattacks/design";
import mixpanel from "mixpanel-browser";
import { useCallback } from "react";
import * as React from "react";

import { FormButton } from "./styles";
import type { IProvider } from "./types";

const LoginProvider: React.FC<IProvider> = ({
  imageAlt,
  imageId,
  name,
  provider,
  redirectUrl,
}: IProvider): JSX.Element => {
  const handleProviderLogin = useCallback((): void => {
    mixpanel.track(provider);
    window.location.assign(redirectUrl);
  }, [provider, redirectUrl]);

  return (
    <FormButton key={imageAlt} onClick={handleProviderLogin}>
      <CloudImage alt={imageAlt} publicId={imageId} width={"24px"} />
      {`Continue with ${name}`}
    </FormButton>
  );
};

export { LoginProvider };
