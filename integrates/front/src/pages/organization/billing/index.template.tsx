import { Container } from "@fluidattacks/design";
import type { IIndicatorCardProps } from "@fluidattacks/design";
import type { ITabProps } from "@fluidattacks/design/dist/components/tabs/types";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

import type { IBillingTemplateProps } from "./types";

import { SectionHeader } from "components/section-header";
import type { IIndicatorsProps } from "components/section-header/indicators";
import { Indicators } from "components/section-header/indicators";

const BillingTemplate: React.FC<IBillingTemplateProps> = ({
  url,
  children,
  costsTotal,
  numberAuthorsEssential,
  numberAuthorsAdvanced,
  numberGroupsEssential,
  numberGroupsAdvanced,
}): JSX.Element => {
  const { t } = useTranslation();
  const theme = useTheme();

  const tabsItems: ITabProps[] = [
    {
      id: "billing-tab",
      label: t("organization.tabs.billing.tabs.history"),
      link: `${url}/history`,
    },
    {
      id: "payment-tab",
      label: t("organization.tabs.billing.tabs.payment"),
      link: `${url}/payment`,
    },
  ];

  const indicatorsCards: IIndicatorCardProps[] = [
    {
      description: t(
        "organization.tabs.billing.overview.numberGroupsEssential.headerCard",
      ),
      title: `${numberGroupsEssential}`,
      variant: "centered",
    },
    {
      description: t(
        "organization.tabs.billing.overview.numberGroupsAdvanced.headerCard",
      ),
      title: `${numberGroupsAdvanced}`,
      variant: "centered",
    },
    {
      description: t(
        "organization.tabs.billing.overview.numberAuthorsEssential.headerCard",
      ),
      title: `${numberAuthorsAdvanced}`,
      variant: "centered",
    },
    {
      description: t(
        "organization.tabs.billing.overview.numberAuthorsAdvanced.headerCard",
      ),
      title: `${numberAuthorsEssential}`,
      variant: "centered",
    },
    {
      description: t("organization.tabs.billing.overview.costsTotal.title"),
      title: `${costsTotal}`,
      variant: "centered",
    },
  ];
  const indicators: IIndicatorsProps = {
    cards: indicatorsCards,
  };

  return (
    <React.StrictMode>
      <SectionHeader
        header={t("organization.tabs.billing.header")}
        tabs={tabsItems}
      />
      <Container
        bgColor={"white"}
        borderBottom={`1px solid ${theme.palette.gray[300]}`}
        pb={1}
        pl={1.25}
        pr={1.25}
        pt={1}
        width={"100%"}
      >
        <Indicators
          alertProps={indicators.alertProps}
          cards={indicators.cards}
        />
      </Container>
      <Container mt={0.25} px={1.25} py={1.25}>
        {children}
      </Container>
    </React.StrictMode>
  );
};

export { BillingTemplate };
