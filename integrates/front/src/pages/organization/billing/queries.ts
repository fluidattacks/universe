import { graphql } from "gql";

const DOWNLOAD_FILE_MUTATION = graphql(`
  mutation DownloadBillingFileMutation(
    $fileName: String!
    $organizationId: String!
    $paymentMethodId: String!
  ) {
    downloadBillingFile(
      fileName: $fileName
      organizationId: $organizationId
      paymentMethodId: $paymentMethodId
    ) {
      success
      url
    }
  }
`);

const GET_ORGANIZATION_BILLING = graphql(`
  query GetOrganizationBillingAtBillingView($organizationId: String!) {
    organization(organizationId: $organizationId) {
      country
      name
      billing {
        costsAuthors
        costsBase
        costsTotal
        numberAuthorsAdvanced
        numberAuthorsEssential
        numberAuthorsTotal
        numberGroupsAdvanced
        numberGroupsEssential
        numberGroupsTotal
        paymentMethods {
          id
          brand
          businessName
          city
          country
          default
          email
          expirationMonth
          expirationYear
          lastFourDigits
          state
          rut {
            fileName
            modifiedDate
          }
          taxId {
            fileName
            modifiedDate
          }
        }
      }
    }
  }
`);

const GET_ORGANIZATION_BILLING_BY_DATE = graphql(`
  query GetOrganizationBillingBydate(
    $date: DateTime
    $organizationId: String!
  ) {
    organization(organizationId: $organizationId) {
      name
      billing(date: $date) {
        authors {
          actor
          activeGroups {
            name
            tier
          }
        }
      }
      groups {
        hasAdvanced
        hasEssential
        hasForces
        managed
        name
        paymentId
        permissions
        service
        tier
        billing(date: $date) {
          costsAuthors
          costsBase
          costsTotal
          numberAuthors
        }
      }
    }
  }
`);

const REMOVE_PAYMENT_METHOD = graphql(`
  mutation removePaymentMethod(
    $organizationId: String!
    $paymentMethodId: String!
  ) {
    removePaymentMethod(
      organizationId: $organizationId
      paymentMethodId: $paymentMethodId
    ) {
      success
    }
  }
`);

const UPDATE_CREDIT_CARD_PAYMENT_METHOD = graphql(`
  mutation updateCreditCardPaymentMethod(
    $cardExpirationMonth: Int
    $cardExpirationYear: Int
    $makeDefault: Boolean!
    $organizationId: String!
    $paymentMethodId: String!
  ) {
    updateCreditCardPaymentMethod(
      cardExpirationMonth: $cardExpirationMonth
      cardExpirationYear: $cardExpirationYear
      makeDefault: $makeDefault
      organizationId: $organizationId
      paymentMethodId: $paymentMethodId
    ) {
      success
    }
  }
`);

const UPDATE_OTHER_PAYMENT_METHOD = graphql(`
  mutation updateOtherPaymentMethod(
    $businessName: String!
    $city: String!
    $country: String!
    $email: String!
    $organizationId: String!
    $paymentMethodId: String!
    $rut: Upload
    $state: String!
    $taxId: Upload
  ) {
    updateOtherPaymentMethod(
      businessName: $businessName
      city: $city
      country: $country
      email: $email
      organizationId: $organizationId
      paymentMethodId: $paymentMethodId
      rut: $rut
      state: $state
      taxId: $taxId
    ) {
      success
    }
  }
`);

export {
  DOWNLOAD_FILE_MUTATION,
  GET_ORGANIZATION_BILLING,
  GET_ORGANIZATION_BILLING_BY_DATE,
  REMOVE_PAYMENT_METHOD,
  UPDATE_CREDIT_CARD_PAYMENT_METHOD,
  UPDATE_OTHER_PAYMENT_METHOD,
};
