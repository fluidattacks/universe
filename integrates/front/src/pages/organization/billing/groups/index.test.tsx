import { PureAbility } from "@casl/ability";
import { screen, waitFor } from "@testing-library/react";
import { Route, Routes } from "react-router-dom";

import { authzPermissionsContext } from "context/authz/config";
import { ManagedType, ServiceType, TierType } from "gql/graphql";
import { render } from "mocks";
import { OrganizationGroups } from "pages/organization/billing/groups";

describe("organization billing groups view", (): void => {
  const memoryRouter = {
    initialEntries: ["/orgs/okada/billing"],
  };

  it("should render a component", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedInternalPermissions = new PureAbility<string>([
      { action: "integrates_api_resolvers_organization_billing_resolve" },
      { action: "see_billing_service_type" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedInternalPermissions}>
        <Routes>
          <Route
            element={
              <OrganizationGroups
                groups={[
                  {
                    advanced: "true",
                    billing: {
                      costsAuthors: 1,
                      costsBase: 1,
                      costsTotal: 1,
                      numberAuthors: 1,
                    },
                    essential: "",
                    forces: "",
                    hasAdvanced: true,
                    hasEssential: true,
                    hasForces: true,
                    managed: ManagedType.Managed,
                    name: "unittesting",
                    paymentId: "280fe281-e190-45af-b733-a24889b96fd1",
                    permissions: [
                      "integrates_api_mutations_update_group_managed_mutate",
                    ],
                    service: ServiceType.White,
                    tier: TierType.Advanced,
                  },
                ]}
              />
            }
            path={"/orgs/:organizationName/billing"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter,
      },
    );

    await waitFor((): void => {
      expect(screen.queryAllByRole("table")).toHaveLength(1);
    });

    expect(screen.getAllByRole("row")).toHaveLength(2);
    expect(
      screen.queryAllByText(
        "organization.tabs.billing.groups.headers.numberAuthors",
      ),
    ).toHaveLength(1);
    expect(
      screen.queryByText(
        "organization.tabs.billing.groups.updateSubscription.title",
      ),
    ).not.toBeInTheDocument();

    jest.clearAllMocks();
  });

  it("should render a component less permissions", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedExternalPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_update_group_managed_mutate" },
      { action: "integrates_api_resolvers_organization_billing_resolve" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedExternalPermissions}>
        <Routes>
          <Route
            element={
              <OrganizationGroups
                groups={[
                  {
                    advanced: "true",
                    billing: {
                      costsAuthors: 1,
                      costsBase: 1,
                      costsTotal: 1,
                      numberAuthors: 1,
                    },
                    essential: "",
                    forces: "",
                    hasAdvanced: true,
                    hasEssential: true,
                    hasForces: true,
                    managed: ManagedType.Managed,
                    name: "unittesting",
                    paymentId: "280fe281-e190-45af-b733-a24889b96fd1",
                    permissions: [
                      "integrates_api_mutations_update_group_managed_mutate",
                    ],
                    service: ServiceType.White,
                    tier: TierType.Advanced,
                  },
                ]}
              />
            }
            path={"/orgs/:organizationName/billing"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter,
      },
    );
    await waitFor((): void => {
      expect(screen.queryAllByRole("table")).toHaveLength(1);
    });

    expect(screen.getAllByRole("row")).toHaveLength(2);
    expect(
      screen.queryAllByText(
        "organization.tabs.billing.groups.headers.numberAuthors",
      ),
    ).toHaveLength(1);

    jest.clearAllMocks();
  });
});
