import { useAbility } from "@casl/react";
import { Heading } from "@fluidattacks/design";
import type { ColumnDef } from "@tanstack/react-table";
import capitalize from "lodash/capitalize";
import { useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import type { IGroupAttr } from "../types";
import { Filters } from "components/filter";
import type { IFilter } from "components/filter/types";
import { Table } from "components/table";
import { authzPermissionsContext } from "context/authz/config";
import { statusFormatter } from "features/vulnerabilities/formatters/status";
import type { TierType } from "gql/graphql";
import { setFiltersUtil, useTable } from "hooks";

interface IOrganizationGroupsProps {
  readonly groups: IGroupAttr[];
}

export const OrganizationGroups: React.FC<IOrganizationGroupsProps> = ({
  groups,
}): JSX.Element => {
  const { t } = useTranslation();
  const tableRef = useTable("tblGroups", {
    costTotal: false,
    managed: false,
    tier: false,
  });

  // Auxiliary functions
  const accessibleGroupsData = (groupData: IGroupAttr[]): IGroupAttr[] =>
    groupData.filter((group): boolean =>
      group.permissions.includes(
        "integrates_api_mutations_update_group_managed_mutate",
      ),
    );

  const formatGroupsData = (groupData: IGroupAttr[]): IGroupAttr[] =>
    groupData.map((group: IGroupAttr): IGroupAttr => {
      const name = capitalize(group.name);
      const tier = capitalize(group.tier);

      return {
        ...group,
        name,
        tier: tier as TierType,
      };
    });

  const permissions = useAbility(authzPermissionsContext);
  const canSeeSubscriptionType = permissions.can(
    "see_billing_subscription_type",
  );

  const tier: ColumnDef<IGroupAttr> = {
    accessorKey: "tier",
    cell: (cell): JSX.Element => statusFormatter(String(cell.getValue())),
    header: t("organization.tabs.billing.groups.headers.tier"),
    meta: { filterType: "select" },
  };

  const baseTableColumns: ColumnDef<IGroupAttr>[] = [
    {
      accessorKey: "name",
      header: t("organization.tabs.billing.groups.headers.groupName"),
    },
    {
      accessorKey: "managed",
      cell: (cell): JSX.Element => cell.getValue() as JSX.Element,
      enableColumnFilter: false,
      header: t("organization.tabs.billing.groups.headers.managed"),
    },
    tier,
    {
      accessorFn: (row): number | undefined => {
        return row.billing.numberAuthors;
      },
      accessorKey: "totalAuthors",
      cell: (cell): JSX.Element => statusFormatter(String(cell.getValue())),
      enableColumnFilter: false,
      header: t("organization.tabs.billing.groups.headers.numberAuthors"),
    },
    {
      accessorFn: (row): number | undefined => {
        return row.billing.costsTotal;
      },
      accessorKey: "costTotal",
      cell: (cell): JSX.Element => statusFormatter(String(cell.getValue())),
      enableColumnFilter: false,
      header: t("organization.tabs.billing.groups.headers.costsTotal"),
    },
  ];

  const tableColumns = baseTableColumns.filter((header): boolean => {
    if (header === tier) {
      return canSeeSubscriptionType;
    }

    return true;
  });

  const baseFilters: IFilter<IGroupAttr>[] = [
    {
      id: "name",
      key: "name",
      label: t("organization.tabs.billing.groups.headers.groupName"),
      type: "text",
    },
    {
      id: "tier",
      key: "tier",
      label: t("organization.tabs.billing.groups.headers.tier"),
      selectOptions: ["Advanced", "Essential", "Oneshot"],
      type: "select",
    },
  ];

  const [filters, setFilters] = useState<IFilter<IGroupAttr>[]>(
    baseFilters.filter((filter): boolean => {
      if (filter.id === "tier") return canSeeSubscriptionType;

      return true;
    }),
  );

  const dataset: IGroupAttr[] = formatGroupsData(accessibleGroupsData(groups));

  const filteredDataset = setFiltersUtil(dataset, filters);

  return (
    <div>
      <Heading fontWeight={"bold"} mb={2} mt={2} size={"md"}>
        {t("organization.tabs.billing.groups.title")}
      </Heading>
      <Table
        columns={tableColumns}
        data={filteredDataset}
        filters={<Filters filters={filters} setFilters={setFilters} />}
        tableRef={tableRef}
      />
    </div>
  );
};
