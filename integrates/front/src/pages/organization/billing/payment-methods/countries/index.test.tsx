import { screen, waitFor } from "@testing-library/react";
import { Form, Formik } from "formik";

import { CountriesSelect } from ".";
import { render } from "mocks";

describe("countriesSelect", (): void => {
  it("should render a component %s", async (): Promise<void> => {
    expect.hasAssertions();

    jest.spyOn(console, "error").mockImplementation();
    jest.spyOn(console, "warn").mockImplementation();
    render(
      <Formik initialValues={{ country: "Colombia" }} onSubmit={jest.fn()}>
        <Form>
          <CountriesSelect handleChange={jest.fn()} />
        </Form>
      </Formik>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/billing/history"],
        },
      },
    );

    await waitFor((): void => {
      expect(screen.getByText("Colombia")).toBeInTheDocument();
    });

    expect(
      screen.getByText("Country where the organization is based"),
    ).toBeInTheDocument();
  });
});
