import { findBusinessInfo } from "./utils";

describe("findBusinessInfo", (): void => {
  it("should return businessId and businessName when there is a group with business info", (): void => {
    expect.hasAssertions();

    const groupsInfo = {
      group1: {
        businessId: "businessId1",
        businessName: "businessName1",
      },
      group2: {
        businessId: "businessId2",
        businessName: "businessName2",
      },
    };
    const selectedGroups = ["group1", "group2"];

    const result = findBusinessInfo(selectedGroups, groupsInfo);

    expect(result).toStrictEqual({
      businessId: "businessId1",
      businessName: "businessName1",
    });
  });

  it("should return empty businessId and businessName when there is no group with business info", (): void => {
    expect.hasAssertions();

    const groupsInfo = {
      group1: {
        businessId: "businessId1",
        businessName: "businessName1",
      },
      group2: {
        businessId: "businessId2",
        businessName: "businessName2",
      },
    };
    const selectedGroups = ["group3", "group4"];

    const result = findBusinessInfo(selectedGroups, groupsInfo);

    expect(result).toStrictEqual({
      businessId: undefined,
      businessName: undefined,
    });
  });
});
