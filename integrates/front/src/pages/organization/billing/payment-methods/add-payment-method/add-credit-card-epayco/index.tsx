import { useMutation } from "@apollo/client";
import {
  Alert,
  Container,
  GridContainer,
  Icon,
  useConfirmDialog,
} from "@fluidattacks/design";
import isEmpty from "lodash/isEmpty";
import mixpanel from "mixpanel-browser";
import { Fragment, useCallback, useContext } from "react";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

import { ConfirmContent } from "./confirm-content";
import { StyledInput } from "./styles";
import type { IFormValues } from "./types";
import { createCardToken } from "./utils";
import { validationSchema } from "./validations";

import { GET_ORGANIZATION_BILLING } from "../../../queries";
import { CommonFieldsAdditionForm } from "../common-fields-addition-form";
import { ADD_CREDIT_CARD_PAYMENT_METHOD } from "../queries";
import { Form, InnerForm } from "components/form";
import { Label } from "components/input";
import { authContext } from "context/auth";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

const RegisterCreditCard = ({
  onClose,
  organizationId,
  groupsInfo,
  country,
}: Readonly<{
  onClose: () => void;
  organizationId: string;
  country: string;
  groupsInfo: Record<string, Record<string, string | null>>;
}>): JSX.Element => {
  const { t } = useTranslation();
  const { userName } = useContext(authContext);
  const theme = useTheme();
  const { confirm, ConfirmDialog } = useConfirmDialog();

  const [addCreditCardPaymentMethod, { loading }] = useMutation(
    ADD_CREDIT_CARD_PAYMENT_METHOD,
    {
      onCompleted: (): void => {
        onClose();
        msgSuccess(
          t("organization.tabs.billing.paymentMethods.add.success.body"),
          t("organization.tabs.billing.paymentMethods.add.success.title"),
        );
      },
      onError: ({ graphQLErrors }): void => {
        graphQLErrors.forEach((error): void => {
          if (
            error.message ===
            "Exception - Provided payment method could not be created"
          ) {
            msgError(
              t(
                "organization.tabs.billing.paymentMethods.add.errors.couldNotBeCreated",
              ),
            );
          } else {
            msgError(t("groupAlerts.errorTextsad"));
            Logger.error("Couldn't create payment method", error);
          }
        });
      },
      refetchQueries: [
        {
          query: GET_ORGANIZATION_BILLING,
          variables: {
            organizationId,
          },
        },
      ],
    },
  );

  const handleFormSubmit = useCallback(
    async (values: IFormValues): Promise<void> => {
      const confirmResult = await confirm({
        message: <ConfirmContent values={values} />,
        title: t("organization.billing.confirm.title"),
      });

      if (!confirmResult) {
        return;
      }

      const {
        billedGroups,
        cvc,
        expMonth,
        expYear,
        cardHolderName,
        email,
        cardNumber,
        makeDefault,
        businessId,
        businessName,
      } = values;

      try {
        const token = await createCardToken({
          cardHolderName,
          cardNumber,
          cvc,
          email,
          expMonth,
          expYear,
        });
        if (token.token === null || token.error !== null) {
          msgError(token.error ?? t("groupAlerts.errorTextsad"));

          return;
        }
        mixpanel.track("AddPaymentMethod", { method: "TC" });
        void addCreditCardPaymentMethod({
          variables: {
            billedGroups,
            billingEmail: email,
            businessId,
            businessName,
            country,
            makeDefault,
            organizationId,
            paymentMethodId: token.token,
          },
        });
      } catch (error) {
        Logger.error("Couldn't create payment method", error);
        msgError(t("groupAlerts.errorTextsad"));
      }
    },
    [t, addCreditCardPaymentMethod, organizationId, country, confirm],
  );

  return (
    <Fragment>
      <Form
        initialValues={{
          billedGroups: [],
          businessId: "",
          businessName: "",
          cardHolderName: userName,
          cardNumber: "",
          cvc: "",
          email: "",
          expMonth: "",
          expYear: "",
          makeDefault: false,
        }}
        name={"addCreditCardPaymentMethod"}
        onSubmit={handleFormSubmit}
        validationSchema={validationSchema}
      >
        {({
          dirty,
          isSubmitting,
          errors,
          touched,
          values,
          setFieldValue,
        }): JSX.Element => (
          <InnerForm
            onCancel={onClose}
            submitDisabled={
              isSubmitting || loading || !dirty || !isEmpty(errors)
            }
          >
            <Label htmlFor={"cardNumber"} isRequired={true}>
              {t("organization.billing.cardInformation.label")}
            </Label>
            <GridContainer
              border={`1px solid ${theme.palette.gray[300]}`}
              borderRadius={theme.spacing["0.5"]}
              gap={0}
              lg={2}
              mb={0.5}
              md={2}
              minHeight={"40px"}
              px={0.5}
              py={0.5}
              sm={2}
              xl={2}
            >
              <Container alignItems={"center"} display={"flex"} gap={0.5}>
                <Icon icon={"credit-card"} iconSize={"xxs"} />
                <StyledInput
                  name={"cardNumber"}
                  placeholder={"Card Number"}
                  required={true}
                />
              </Container>
              <GridContainer gap={0} lg={3} md={3} sm={3} xl={3}>
                <StyledInput
                  name={"expMonth"}
                  placeholder={"MM"}
                  required={true}
                />
                <StyledInput
                  name={"expYear"}
                  placeholder={"YYYY"}
                  required={true}
                />
                <StyledInput name={"cvc"} placeholder={"CVC"} required={true} />
              </GridContainer>
            </GridContainer>
            <CommonFieldsAdditionForm
              groupsInfo={groupsInfo}
              requiredBusinessInfo={true}
              selectedGroups={values.billedGroups}
              setFieldValue={setFieldValue}
            />
            <Alert
              show={!isEmpty(errors) && Object.keys(touched).length > 4}
              variant={"error"}
            >
              {Object.values(errors).join("\n")}
            </Alert>
          </InnerForm>
        )}
      </Form>
      <ConfirmDialog />
    </Fragment>
  );
};

export { RegisterCreditCard };
