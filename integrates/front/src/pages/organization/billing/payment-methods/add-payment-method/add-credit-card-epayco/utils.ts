/* eslint-disable camelcase */
import AES from "crypto-js/aes";
import Utf8 from "crypto-js/enc-utf8";

import type { ICreditCardValues } from "./types";

import { Logger } from "utils/logger";
import { translate } from "utils/translations/translate";

const BASE_URL = "https://api.secure.payco.co/";
const PUBLIC_KEY = "e18809971284f06f3afe236dee143b7f";
const CREDIT_CARD_LENGTH = 16;

const encrypt = (data: string, key: string): string => {
  return AES.encrypt(Utf8.parse(data), key).toString().toString();
};

interface IData {
  type: string;
  value: string;
}

interface IResponse {
  data: {
    status: "created" | "error";
    description: string | undefined;
    token: string | undefined;
  };
  message: string;
  status: "error" | "success";
}

interface IGetTokenResponse {
  data: {
    status: "created" | "error";
    description: string | undefined;
    token: string | undefined;
  };
  message: string;
  status: boolean;
}

type TValidationResults = {
  [K in keyof ICreditCardValues]: string | null;
};

const getToken = async (session: string): Promise<string | undefined> => {
  return (
    fetch(`${BASE_URL}token/encrypt`, {
      body: JSON.stringify({
        public_key: PUBLIC_KEY,
        session,
      }),
      headers: {
        "Content-Type": "application/json",
      },
      method: "POST",
      mode: "cors",
    })
      // eslint-disable-next-line @typescript-eslint/no-unsafe-return
      .then(async (response): Promise<IGetTokenResponse> => response.json())
      .then((response: IGetTokenResponse): string | undefined => {
        const { data, status } = response;
        if (!status) Logger.error("Error tokenizing card", response);

        return data.token;
      })
      .catch((error: unknown): undefined => {
        Logger.error("Error getting epayco token: ", error);

        return undefined;
      })
  );
};

const tokenize = async (postData: IData[]): Promise<string | undefined> => {
  return (
    fetch(`${BASE_URL}token/tokenize`, {
      body: JSON.stringify({
        values: postData,
      }),
      headers: {
        "Content-Type": "application/json",
      },
      method: "POST",
      mode: "cors",
    })
      // eslint-disable-next-line @typescript-eslint/no-unsafe-return
      .then(async (response): Promise<IResponse> => response.json())
      .then((response: IResponse): string | undefined => {
        const { data, status } = response;
        if (status !== "success")
          Logger.error("Error tokenizing card", response);

        return data.token;
      })
      .catch((error: unknown): undefined => {
        Logger.error("Error tokenizing card: ", error);

        return undefined;
      })
  );
};

const createGuid = (): string => {
  const VALUES_FOR_16_BITS = 65536;
  const BITS = 16;

  const generateRandom = (): string =>
    Math.floor(VALUES_FOR_16_BITS * (1 + Math.random()))
      .toString(BITS)
      .substring(1);

  const sections = [2, 1, 1, 1, 3].map((num): string =>
    Array.from({ length: num }, generateRandom).join(""),
  );

  return sections.join("-");
};

const getSessionId = (): string => {
  const sessionId = localStorage.getItem("keyUserIndex_session");
  if (sessionId === null) {
    const newId = createGuid();
    localStorage.setItem("keyUserIndex_session", newId);

    return newId;
  }

  return sessionId;
};

const getRequestData = (data: IData[], token: string): IData[] => {
  const values = data.reduce((acc: IData[], curr: IData): IData[] => {
    return [
      ...acc,
      {
        type: curr.type,
        value: encrypt(curr.value, token),
      },
    ];
  }, []);

  return [
    ...values,
    {
      type: "publicKey",
      value: PUBLIC_KEY,
    },
    {
      type: "session",
      value: getSessionId(),
    },
  ];
};

const createTokenEncrypt = async (
  session: string,
  collectedData: IData[],
): Promise<string | null> => {
  const token = await getToken(session);
  if (token === undefined) {
    return null;
  }

  const data = getRequestData(collectedData, token);
  const cardToken = await tokenize(data);

  return cardToken ?? null;
};

const validate = (cardData: ICreditCardValues): TValidationResults => {
  const validCreditCards = [
    {
      name: "visa",
      pattern: /^4/u,
      validLength: [CREDIT_CARD_LENGTH],
    },
    {
      name: "mastercard",
      pattern: /^5[1-5]/u,
      validLength: [CREDIT_CARD_LENGTH],
    },
  ];

  const validateEmail = (email: string): string | null => {
    if (/^[^\s@]+@[^\s@]+\.[^\s@]+$/iu.test(email)) return null;

    return translate.t("validations.email");
  };

  const validateCVC = (value: string): string | null => {
    if (/^\d{3,4}$/u.exec(value)) return null;

    return translate.t("organization.billing.CVCInvalid");
  };

  const validateExpMonth = (value: string): string | null => {
    if (/^\d{2}$/u.exec(value)) return null;

    return translate.t("organization.billing.creditCardExpMonthInvalid");
  };

  const validateExpYear = (value: string): string | null => {
    if (/^\d{4}$/u.exec(value)) return null;

    return translate.t("organization.billing.cardNumberInvalid");
  };

  const validateCardNumber = (value: string): string | null => {
    const data = validCreditCards.find((card): boolean =>
      card.pattern.test(value),
    );

    if (undefined === data)
      return translate.t("organization.billing.cardNumberInvalid");

    return data.validLength.includes(value.length)
      ? null
      : translate.t("organization.billing.cardNumberInvalid");
  };

  const validateName = (value: string): string | null => {
    if (value.length > 0) return null;

    return translate.t("validations.required");
  };

  const validation = {
    cardHolderName: validateName(cardData.cardHolderName),
    cardNumber: validateCardNumber(cardData.cardNumber),
    cvc: validateCVC(cardData.cvc),
    email: validateEmail(cardData.email),
    expMonth: validateExpMonth(cardData.expMonth),
    expYear: validateExpYear(cardData.expYear),
  };

  return validation;
};

const createCardToken = async (
  cardData: ICreditCardValues,
): Promise<{ token: string | null; error: string | null }> => {
  const validation = validate(cardData);
  const errors = Object.values(validation).filter(
    (value): boolean => value !== null,
  );

  if (errors.length > 0) {
    return { error: errors.join(", "), token: null };
  }

  const collectedData = [
    {
      type: "number",
      value: cardData.cardNumber,
    },
    {
      type: "name",
      value: cardData.cardHolderName,
    },
    {
      type: "email",
      value: cardData.email,
    },
    {
      type: "date_exp",
      value: `${cardData.expMonth}/${cardData.expYear}`,
    },
    {
      type: "cvc",
      value: cardData.cvc,
    },
  ];

  const session = getSessionId();
  const token = await createTokenEncrypt(session, collectedData);

  if (token === null) {
    return { error: "Card information invalid", token: null };
  }

  return { error: null, token };
};

export { createCardToken };
