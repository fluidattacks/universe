import { useMutation, useQuery } from "@apollo/client";
import { Elements } from "@stripe/react-stripe-js";
import { loadStripe } from "@stripe/stripe-js";
import { Form as InnerForm } from "formik";
import isEmpty from "lodash/isEmpty";
import mixpanel from "mixpanel-browser";
import { useCallback, useMemo, useState } from "react";
import { useTranslation } from "react-i18next";

import { AddCreditCardModal } from "./add-credit-card";
import { RegisterCreditCard } from "./add-credit-card-epayco";
import {
  ADD_OTHER_PAYMENT_METHOD,
  QUERY_ORGANIZATION_GROUPS_BUSINESS_INFO,
} from "./queries";

import { CountriesSelect } from "../countries";
import { OtherMethodModal } from "../other-method-modal";
import type { IAddMethodPaymentProps, IAddOtherMethodProps } from "../types";
import type { IDropDownOption } from "components/dropdown/types";
import { Form } from "components/form";
import { Select } from "components/input";
import { Modal } from "components/modal";
import { useModal } from "hooks/use-modal";
import { getEnvironment } from "utils/environment";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

const STRIPE_PK_LIVE_MODE = "pk_live_ATYIex0kuszLHUTMo5dJ7ffx";
const STRIPE_PK_TEST_MODE = "pk_test_hfBDuDH6n4MevubIYN0NrAAA";

export const AddPaymentMethod = ({
  country,
  organizationId,
  onClose,
  onUpdate,
}: IAddMethodPaymentProps): JSX.Element => {
  const { t } = useTranslation();
  const modalProps = useModal("add-payment-method");
  const prefix = "organization.tabs.billing.paymentMethods.";

  const stripePk =
    getEnvironment() === "production"
      ? STRIPE_PK_LIVE_MODE
      : STRIPE_PK_TEST_MODE;
  const stripePromise = loadStripe(stripePk);
  const [paymentType, setPaymentType] = useState("CREDIT_CARD");
  const [countryValue, setCountryValue] = useState<string | null>(country);

  // Graphql operation
  const [addOtherPaymentMethod] = useMutation(ADD_OTHER_PAYMENT_METHOD, {
    onCompleted: (): void => {
      onUpdate();
      onClose();
      msgSuccess(
        t(`${prefix}add.success.body`),
        t(`${prefix}add.success.title`),
      );
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.error("Couldn't create payment method", error);
      });
    },
  });

  const { data } = useQuery(QUERY_ORGANIZATION_GROUPS_BUSINESS_INFO, {
    variables: { organizationId },
  });

  const groupsInfo = useMemo(
    (): Record<string, Record<string, string | null>> =>
      data?.organization.groups.reduce(
        (acc, group): Record<string, Record<string, string | null>> => {
          const groupData = {
            [group.name]: {
              businessId: group.businessId,
              businessName: group.businessName,
            },
          };

          return { ...acc, ...groupData };
        },
        {} as Record<string, Record<string, string | null>>,
      ) ?? {},
    [data],
  );

  // Handle actions
  const handleFileListUpload = (
    file: FileList | undefined,
  ): File | undefined => {
    return isEmpty(file) ? undefined : (file as FileList)[0];
  };
  const handleSubmit = useCallback(
    (values: { country: string; paymentType: string }): void => {
      setPaymentType(values.paymentType);
    },
    [],
  );
  const handleSelection = useCallback((selection: IDropDownOption): void => {
    setPaymentType(selection.value ?? "");
  }, []);

  const handleCountryChange = useCallback(
    (selection: IDropDownOption): void => {
      setCountryValue(selection.value ?? null);
    },
    [],
  );

  const handleAddOtherMethodSubmit = useCallback(
    async ({
      businessName,
      city,
      country: orgCountry,
      email,
      rutList,
      state,
      taxIdList,
    }: IAddOtherMethodProps): Promise<void> => {
      const rut = handleFileListUpload(rutList);
      const taxId = handleFileListUpload(taxIdList);
      mixpanel.track("AddPaymentMethod", { method: "Wired" });
      await addOtherPaymentMethod({
        variables: {
          businessName,
          city,
          country: orgCountry,
          email,
          organizationId,
          rut,
          state,
          taxId,
        },
      });
    },
    [addOtherPaymentMethod, organizationId],
  );

  const creditCardModal: Record<string, JSX.Element> = {
    false: (
      <Elements stripe={stripePromise}>
        <AddCreditCardModal
          country={countryValue ?? ""}
          groupsInfo={groupsInfo}
          onClose={onClose}
          organizationId={organizationId}
        />
      </Elements>
    ),
    true: (
      <RegisterCreditCard
        country={"Colombia"}
        groupsInfo={groupsInfo}
        onClose={onClose}
        organizationId={organizationId}
      />
    ),
  };

  return (
    <Modal
      modalRef={{ ...modalProps, close: onClose, isOpen: true }}
      size={"md"}
      title={t(`${prefix}add.title`)}
    >
      <Form
        initialValues={{ country: country ?? "Select", paymentType }}
        onSubmit={handleSubmit}
      >
        <InnerForm>
          <CountriesSelect handleChange={handleCountryChange} />
          <Select
            handleOnChange={handleSelection}
            items={[
              { header: "", value: "" },
              {
                header: t(`${prefix}paymentType.creditCard`),
                value: "CREDIT_CARD",
              },
              { header: t(`${prefix}paymentType.otherMethod`), value: "OTHER" },
            ]}
            label={t(`${prefix}add.label`)}
            name={"paymentType"}
            required={true}
          />
        </InnerForm>
      </Form>
      {paymentType === "CREDIT_CARD" ? (
        creditCardModal[String(countryValue === "Colombia")]
      ) : (
        <OtherMethodModal
          country={{ value: countryValue ?? "" }}
          onClose={onClose}
          onSubmit={handleAddOtherMethodSubmit}
        />
      )}
    </Modal>
  );
};
