import { useMutation } from "@apollo/client";
import { Alert } from "@fluidattacks/design";
import { CardElement, useElements, useStripe } from "@stripe/react-stripe-js";
import type {
  StripeCardElementChangeEvent,
  StripeCardElementOptions,
} from "@stripe/stripe-js";
import { isEmpty } from "lodash";
import mixpanel from "mixpanel-browser";
import { useCallback, useState } from "react";
import { useTranslation } from "react-i18next";

import { CardContainer } from "./styles";
import { validationSchema } from "./validations";

import { GET_ORGANIZATION_BILLING } from "../../../queries";
import type { IAddCreditCardModalProps } from "../../types";
import { CommonFieldsAdditionForm } from "../common-fields-addition-form";
import { ADD_CREDIT_CARD_PAYMENT_METHOD } from "../queries";
import { Form, InnerForm } from "components/form";
import { Label } from "components/input";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

export const AddCreditCardModal = ({
  country,
  organizationId,
  groupsInfo,
  onClose,
}: IAddCreditCardModalProps): JSX.Element => {
  const { t } = useTranslation();
  const prefix = "organization.tabs.billing.paymentMethods.add.";

  const stripe = useStripe();
  const elements = useElements();
  const [errorMessage, setErrorMessage] = useState<string | undefined>();
  const [disabled, setDisabled] = useState(true);
  const options: StripeCardElementOptions = { hidePostalCode: true };

  const [addCreditCardPaymentMethod] = useMutation(
    ADD_CREDIT_CARD_PAYMENT_METHOD,
    {
      onCompleted: (): void => {
        onClose();
        msgSuccess(t(`${prefix}success.body`), t(`${prefix}success.title`));
      },
      onError: ({ graphQLErrors }): void => {
        graphQLErrors.forEach((error): void => {
          if (
            error.message ===
            "Exception - Provided payment method could not be created"
          ) {
            msgError(t(`${prefix}errors.couldNotBeCreated`));
          } else {
            msgError(t("groupAlerts.errorTextsad"));
            Logger.error("Couldn't create payment method", error);
          }
        });
      },
      refetchQueries: [
        {
          query: GET_ORGANIZATION_BILLING,
          variables: {
            organizationId,
          },
        },
      ],
    },
  );

  const handleChange = useCallback(
    (event: StripeCardElementChangeEvent): void => {
      setErrorMessage(event.error?.message);
      setDisabled(!event.complete);
    },
    [],
  );

  const addCreditCard = useCallback(
    async (values: {
      billedGroups: string[];
      email: string;
      makeDefault: boolean;
      businessId: string | undefined;
      businessName: string | undefined;
    }): Promise<void> => {
      if (stripe && elements) {
        const result = await stripe.createPaymentMethod({
          elements,
        });
        setErrorMessage(result.error?.message);
        if (result.error === undefined) {
          mixpanel.track("AddPaymentMethod", { method: "TC" });
          await addCreditCardPaymentMethod({
            variables: {
              billedGroups: values.billedGroups,
              billingEmail: values.email,
              businessId: values.businessId,
              businessName: values.businessName,
              country,
              makeDefault: values.makeDefault,
              organizationId,
              paymentMethodId: result.paymentMethod.id,
            },
          });
        }
      }
    },
    [addCreditCardPaymentMethod, elements, organizationId, stripe, country],
  );

  if (!stripe || !elements) {
    return <div />;
  }

  return (
    <CardContainer>
      <CardContainer>
        <Label htmlFor={"CardData"} isRequired={true}>
          {t("organization.billing.cardInformation.label")}
        </Label>
        <CardElement onChange={handleChange} options={options} />
        <Alert show={errorMessage !== undefined} variant={"error"}>
          {errorMessage}
        </Alert>
      </CardContainer>
      <Form
        initialValues={{
          billedGroups: [],
          businessId: undefined,
          businessName: undefined,
          email: "",
          makeDefault: false,
        }}
        name={"addCreditCard"}
        onSubmit={addCreditCard}
        validationSchema={validationSchema}
      >
        {({ errors, values, setFieldValue }): JSX.Element => (
          <InnerForm
            onCancel={onClose}
            submitDisabled={disabled || !isEmpty(errors)}
          >
            <CommonFieldsAdditionForm
              groupsInfo={groupsInfo}
              requiredBusinessInfo={false}
              selectedGroups={values.billedGroups}
              setFieldValue={setFieldValue}
            />
          </InnerForm>
        )}
      </Form>
    </CardContainer>
  );
};
