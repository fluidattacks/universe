interface ICreditCardValues {
  cvc: string;
  expMonth: string;
  expYear: string;
  cardHolderName: string;
  email: string;
  cardNumber: string;
}

interface IFormValues extends ICreditCardValues {
  makeDefault: boolean;
  billedGroups: string[];
  businessId: string;
  businessName: string;
}

export type { ICreditCardValues, IFormValues };
