import { graphql } from "gql";

const ADD_CREDIT_CARD_PAYMENT_METHOD = graphql(`
  mutation addCreditCardPaymentMethod(
    $billedGroups: [String!]!
    $billingEmail: String!
    $businessId: String
    $businessName: String
    $country: String
    $makeDefault: Boolean!
    $organizationId: String!
    $paymentMethodId: String!
  ) {
    addCreditCardPaymentMethod(
      billedGroups: $billedGroups
      billingEmail: $billingEmail
      businessId: $businessId
      businessName: $businessName
      country: $country
      makeDefault: $makeDefault
      organizationId: $organizationId
      paymentMethodId: $paymentMethodId
    ) {
      success
    }
  }
`);

const ADD_OTHER_PAYMENT_METHOD = graphql(`
  mutation addOtherPaymentMethod(
    $businessName: String!
    $city: String!
    $country: String!
    $email: String!
    $organizationId: String!
    $rut: Upload
    $state: String!
    $taxId: Upload
  ) {
    addOtherPaymentMethod(
      businessName: $businessName
      city: $city
      country: $country
      email: $email
      organizationId: $organizationId
      rut: $rut
      state: $state
      taxId: $taxId
    ) {
      success
    }
  }
`);

const QUERY_ORGANIZATION_GROUPS_BUSINESS_INFO = graphql(`
  query getOrganizationGroupsBusinessInfo($organizationId: String!) {
    organization(organizationId: $organizationId) {
      id
      country
      name
      groups {
        businessId
        businessName
        name
      }
    }
  }
`);

export {
  ADD_CREDIT_CARD_PAYMENT_METHOD,
  ADD_OTHER_PAYMENT_METHOD,
  QUERY_ORGANIZATION_GROUPS_BUSINESS_INFO,
};
