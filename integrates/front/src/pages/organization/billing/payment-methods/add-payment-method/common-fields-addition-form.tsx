import { Alert, Divider, ListItem } from "@fluidattacks/design";
import { Fragment, useCallback, useEffect, useMemo, useState } from "react";
import { useTranslation } from "react-i18next";

import { findBusinessInfo } from "./utils";

import { Checkbox } from "components/checkbox";
import { Dropdown } from "components/dropdown";
import { Input, Label } from "components/input";

const CommonFieldsAdditionForm = ({
  groupsInfo,
  setFieldValue,
  selectedGroups,
  requiredBusinessInfo,
}: Readonly<{
  groupsInfo: Record<string, Record<string, string | null>>;
  setFieldValue: (
    field: string,
    value: string[] | boolean | string | undefined,
    shouldValidate?: boolean,
  ) => Promise<unknown>;
  selectedGroups: string[];
  requiredBusinessInfo: boolean;
}>): JSX.Element => {
  const { t } = useTranslation();
  const [allGroupsSelected, setAllGroupsSelected] = useState(true);

  const groupsNames = useMemo(
    (): string[] => Object.keys(groupsInfo),
    [groupsInfo],
  );

  const handleSelectAllGroups = useCallback((): void => {
    setAllGroupsSelected((prev): boolean => {
      const selection = prev ? [] : groupsNames;
      void setFieldValue("billedGroups", selection);
      const businessInfo = findBusinessInfo(selection, groupsInfo);
      void setFieldValue("businessId", businessInfo.businessId);
      void setFieldValue("businessName", businessInfo.businessName);

      return !prev;
    });
  }, [setAllGroupsSelected, setFieldValue, groupsNames, groupsInfo]);

  useEffect((): void => {
    setAllGroupsSelected(selectedGroups.length === groupsNames.length);

    const businessInfo = findBusinessInfo(selectedGroups, groupsInfo);
    void setFieldValue("businessId", businessInfo.businessId);
    void setFieldValue("businessName", businessInfo.businessName);
  }, [selectedGroups, groupsNames, setFieldValue, groupsInfo]);

  return (
    <Fragment>
      <Input
        label={t("organization.billing.provideBillingEmail")}
        name={"email"}
        required={true}
        type={"email"}
      />
      <Divider />
      <Alert variant={"info"}>
        {t("organization.billing.segmentBillingInformation")}
      </Alert>
      <Label htmlFor={"billedGroups"}>
        {t("organization.billing.groupsToBeBilledSelectorLabel")}
      </Label>
      <Dropdown
        id={"billedGroups-dropdown"}
        initialSelection={{ value: selectedGroups.join(", ") }}
        items={[]}
        width={"100%"}
      >
        <ListItem key={"selectAll"} value={""}>
          <Checkbox
            defaultChecked={allGroupsSelected}
            label={t("organization.billing.selectAll")}
            name={"all"}
            onChange={handleSelectAllGroups}
          />
        </ListItem>
        {groupsNames.map(
          (groupName): JSX.Element => (
            <ListItem key={groupName} value={""}>
              <Checkbox
                key={groupName}
                label={groupName}
                name={"billedGroups"}
                value={groupName}
                variant={"formikField"}
              />
            </ListItem>
          ),
        )}
      </Dropdown>
      <Divider />
      {requiredBusinessInfo || selectedGroups.length ? (
        <Fragment>
          <Input
            label={t("organization.billing.businessId")}
            name={"businessId"}
            required={true}
          />
          <Input
            label={t("organization.billing.businessName")}
            name={"businessName"}
            required={true}
          />
        </Fragment>
      ) : null}
      <Checkbox
        label={t(
          "organization.tabs.billing.paymentMethods.add.creditCard.default",
        )}
        name={"makeDefault"}
        variant={"formikField"}
      />
    </Fragment>
  );
};

export { CommonFieldsAdditionForm };
