import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { useMemo } from "react";

import { CommonFieldsAdditionForm } from "./common-fields-addition-form";

import { Form, InnerForm } from "components/form";
import { render } from "mocks";

const Wrapper = ({
  onSubmit,
}: Readonly<{
  onSubmit: jest.Mock;
}>): JSX.Element => {
  const groupsInfo = useMemo((): Record<string, Record<string, string>> => {
    return {
      group1: {
        businessId: "123.456.789-3",
        businessName: "business1",
      },
      group2: {
        businessId: "987.654.321-8",
        businessName: "business2",
      },
    };
  }, []);

  return (
    <Form
      initialValues={{
        billedGroups: ["group1", "group2"],
        businessId: "",
        businessName: "",
        email: "",
      }}
      onSubmit={onSubmit}
    >
      {({ values, setFieldValue }): JSX.Element => (
        <InnerForm>
          <CommonFieldsAdditionForm
            groupsInfo={groupsInfo}
            requiredBusinessInfo={true}
            selectedGroups={values.billedGroups}
            setFieldValue={setFieldValue}
          />
        </InnerForm>
      )}
    </Form>
  );
};

describe("common-field-addition-form", (): void => {
  it("should render", async (): Promise<void> => {
    expect.hasAssertions();

    const mockOnSubmit = jest.fn();

    render(<Wrapper onSubmit={mockOnSubmit} />);

    await waitFor((): void => {
      expect(screen.queryByText("group1, group2")).toBeInTheDocument();
    });

    await userEvent.type(
      screen.getByRole("combobox", { name: "email" }),
      "billing@company.com",
    );

    await userEvent.click(
      screen.getByRole("button", { name: "buttons.confirm" }),
    );

    expect(mockOnSubmit).toHaveBeenCalledWith(
      {
        billedGroups: ["group1", "group2"],
        businessId: "123.456.789-3",
        businessName: "business1",
        email: "billing@company.com",
      },
      expect.anything(),
    );
  });

  it("unselect all groups", async (): Promise<void> => {
    expect.hasAssertions();

    const mockOnSubmit = jest.fn();

    render(<Wrapper onSubmit={mockOnSubmit} />);

    await waitFor((): void => {
      expect(screen.queryByText("group1, group2")).toBeInTheDocument();
    });

    await userEvent.click(
      screen.getByTestId("billedGroups-dropdown-selected-option"),
    );

    await userEvent.click(screen.getByText("organization.billing.selectAll"));

    await userEvent.click(
      screen.getByRole("button", { name: "buttons.confirm" }),
    );

    expect(mockOnSubmit).toHaveBeenCalledWith(
      {
        billedGroups: [],
        email: "",
      },
      expect.anything(),
    );

    await userEvent.click(
      screen.getByTestId("billedGroups-dropdown-selected-option"),
    );

    await userEvent.click(screen.getByText("organization.billing.selectAll"));

    await userEvent.click(
      screen.getByRole("button", { name: "buttons.confirm" }),
    );

    expect(mockOnSubmit).toHaveBeenCalledWith(
      {
        billedGroups: ["group1", "group2"],
        businessId: "123.456.789-3",
        businessName: "business1",
        email: "",
      },
      expect.anything(),
    );
  });
});
