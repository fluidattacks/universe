import { useMutation } from "@apollo/client";
import { useAbility } from "@casl/react";
import { Button, ButtonToolbarRow, Heading } from "@fluidattacks/design";
import { faPlus } from "@fortawesome/free-solid-svg-icons/faPlus";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import type { ColumnDef } from "@tanstack/react-table";
import _ from "lodash";
import { useCallback, useState } from "react";
import { useTranslation } from "react-i18next";

import { AddPaymentMethod } from "./add-payment-method";
import { CreditCardExtraButtons } from "./credit-card-extra-buttons";
import { downloadFormatterRender } from "./download-formatter/utils";
import {
  handleRemovePaymentError,
  handleUpdateCCError,
  handleUpdateOtherError,
} from "./hooks";
import { OtherMethodExtraButtons } from "./other-method-extra-buttons";
import { UpdateOtherMethodModal } from "./other-method-modal/update";
import type {
  IAddOtherMethodProps,
  ICardProps,
  IOrganizationPaymentMethodsProps,
} from "./types";
import { UpdateCreditCardModal } from "./update-credit-card-modal";

import {
  REMOVE_PAYMENT_METHOD,
  UPDATE_CREDIT_CARD_PAYMENT_METHOD,
  UPDATE_OTHER_PAYMENT_METHOD,
} from "../queries";
import type { IPaymentMethodAttr } from "../types";
import { Table } from "components/table";
import { Can } from "context/authz/can";
import { authzPermissionsContext } from "context/authz/config";
import { useTable } from "hooks";
import { msgSuccess } from "utils/notifications";

export const OrganizationPaymentMethods = ({
  country,
  paymentMethods,
  organizationId,
  onUpdate,
}: IOrganizationPaymentMethodsProps): JSX.Element => {
  const { t } = useTranslation();
  const prefix = "organization.tabs.billing.paymentMethods.";

  const tableRef1 = useTable("tblCreditCard");
  const tableRef2 = useTable("tblOtherMethods");
  const permissions = useAbility(authzPermissionsContext);
  const [currentCreditCardRow, setCurrentCreditCardRow] = useState<
    IPaymentMethodAttr[]
  >([]);
  const [currentOtherMethodRow, setCurrentOtherMethodRow] = useState<
    IPaymentMethodAttr[]
  >([]);

  // Data
  const creditCardData = paymentMethods
    .map((paymentMethodData): IPaymentMethodAttr => {
      const isDefault = paymentMethodData.default;
      const capitalized = _.capitalize(paymentMethodData.brand);
      const brand = isDefault
        ? `${t("organization.tabs.billing.paymentMethods.defaultPaymentMethod")} ${capitalized}`
        : capitalized;

      return {
        ...paymentMethodData,
        brand,
      };
    })
    .filter((paymentMethod): boolean => {
      return paymentMethod.lastFourDigits !== "";
    });

  const otherMethodData = paymentMethods.filter((paymentMethod): boolean => {
    return paymentMethod.lastFourDigits === "";
  });

  const otherMetodData = otherMethodData.map((method): IPaymentMethodAttr => {
    return {
      ...method,
      download: method.id,
    };
  });

  // Add payment method
  const [isAddingPaymentMethod, setIsAddingPaymentMethod] = useState(false);
  const closeAddModal = useCallback((): void => {
    setIsAddingPaymentMethod(false);
  }, []);
  const openAddModal = useCallback((): void => {
    setIsAddingPaymentMethod(true);
  }, []);

  // Remove payment method
  const canRemove = permissions.can(
    "integrates_api_mutations_remove_payment_method_mutate",
  );
  const [removePaymentMethod, { loading: removing }] = useMutation(
    REMOVE_PAYMENT_METHOD,
    {
      onCompleted: (): void => {
        onUpdate();
        setCurrentCreditCardRow([]);
        setCurrentOtherMethodRow([]);
        msgSuccess(
          t(`${prefix}remove.success.body`),
          t(`${prefix}remove.success.title`),
        );
      },
      onError: handleRemovePaymentError,
    },
  );
  const handleRemoveCreditCardPaymentMethod = useCallback((): void => {
    void removePaymentMethod({
      variables: {
        organizationId,
        paymentMethodId: currentCreditCardRow[0]?.id,
      },
    });
  }, [organizationId, currentCreditCardRow, removePaymentMethod]);

  const handleRemoveOtherPaymentMethod = useCallback((): void => {
    void removePaymentMethod({
      variables: {
        organizationId,
        paymentMethodId: currentOtherMethodRow[0]?.id,
      },
    });
  }, [organizationId, currentOtherMethodRow, removePaymentMethod]);

  // Update payment method
  const canUpdateCreditCard = permissions.can(
    "integrates_api_mutations_update_credit_card_payment_method_mutate",
  );
  const canUpdateOther = permissions.can(
    "integrates_api_mutations_update_other_payment_method_mutate",
  );
  const [isUpdatingCreditCard, setIsUpdatingCreditCard] = useState<
    false | { mode: "UPDATE" }
  >(false);
  const openUpdateCreditCardModal = useCallback((): void => {
    setIsUpdatingCreditCard({ mode: "UPDATE" });
  }, []);
  const closeUpdateCreditCardModal = useCallback((): void => {
    setIsUpdatingCreditCard(false);
  }, []);
  const [isUpdatingOhterMethod, setIsUpdatingOhterMethod] = useState<
    false | { mode: "UPDATE" }
  >(false);
  const openUpdateOhterMethodModal = useCallback((): void => {
    setIsUpdatingOhterMethod({ mode: "UPDATE" });
  }, []);
  const closeUpdateOhterMethodModal = useCallback((): void => {
    setIsUpdatingOhterMethod(false);
  }, []);
  const [updateCreditCardPaymentMethod, { loading: updatingCreditCard }] =
    useMutation(UPDATE_CREDIT_CARD_PAYMENT_METHOD, {
      onCompleted: (): void => {
        onUpdate();
        closeUpdateCreditCardModal();
        msgSuccess(
          t(`${prefix}update.success.body`),
          t(`${prefix}update.success.title`),
        );
      },
      onError: handleUpdateCCError,
    });
  const [updateOtherPaymentMethod, { loading: updatingOther }] = useMutation(
    UPDATE_OTHER_PAYMENT_METHOD,
    {
      onCompleted: (): void => {
        onUpdate();
        closeUpdateOhterMethodModal();
        msgSuccess(
          t(`${prefix}update.success.body`),
          t(`${prefix}update.success.title`),
        );
      },
      onError: handleUpdateOtherError,
    },
  );
  const handleUpdateCreditCardPaymentMethodSubmit = useCallback(
    async ({
      cardExpirationMonth,
      cardExpirationYear,
      makeDefault,
    }: ICardProps): Promise<void> => {
      await updateCreditCardPaymentMethod({
        variables: {
          cardExpirationMonth,
          cardExpirationYear,
          makeDefault,
          organizationId,
          paymentMethodId: currentCreditCardRow[0]?.id,
        },
      });
    },
    [updateCreditCardPaymentMethod, organizationId, currentCreditCardRow],
  );
  const handleFileListUpload = (
    file: FileList | undefined,
  ): File | undefined => {
    return _.isEmpty(file) ? undefined : (file as FileList)[0];
  };
  const handleUpdateOtherPaymentMethodSubmit = useCallback(
    async ({
      businessName,
      city,
      country: orgCountry,
      email,
      rutList,
      state,
      taxIdList,
    }: IAddOtherMethodProps): Promise<void> => {
      const rut = handleFileListUpload(rutList);
      const taxId = handleFileListUpload(taxIdList);
      await updateOtherPaymentMethod({
        variables: {
          businessName,
          city,
          country: orgCountry,
          email,
          organizationId,
          paymentMethodId: currentOtherMethodRow[0]?.id,
          rut,
          state,
          taxId,
        },
      });
    },
    [updateOtherPaymentMethod, organizationId, currentOtherMethodRow],
  );

  const additionalColumns = [
    {
      accessorKey: "expirationMonth",
      header: "Expiration month",
    },
    {
      accessorKey: "expirationYear",
      header: "Expiration year",
    },
  ];

  const creditCardTableHeaders: ColumnDef<IPaymentMethodAttr>[] = [
    {
      accessorKey: "brand",
      header: "Brand",
    },
    {
      accessorKey: "lastFourDigits",
      header: "Last four digits",
    },
  ].concat(country === "Colombia" ? [] : additionalColumns);

  const otherMethodsTableHeaders: ColumnDef<IPaymentMethodAttr>[] = [
    {
      accessorKey: "businessName",
      header: "Business name",
    },
    {
      accessorKey: "email",
      header: "e-factura email",
    },
    {
      accessorKey: "country",
      header: "Country",
    },
    {
      accessorKey: "download",
      cell: (cell): JSX.Element => {
        const paymentMethodId = String(cell.getValue());

        const [paymentMethod] = paymentMethods.filter((method): boolean => {
          return method.id === paymentMethodId;
        });

        return downloadFormatterRender(
          paymentMethodId,
          organizationId,
          paymentMethod,
        );
      },
      header: "Document",
    },
  ];

  return (
    <div>
      <ButtonToolbarRow>
        <Can
          do={"integrates_api_mutations_add_credit_card_payment_method_mutate"}
        >
          <Button
            id={"addPaymentMethod"}
            onClick={openAddModal}
            tooltip={t(`${prefix}add.button.tooltip`)}
            variant={"primary"}
          >
            <FontAwesomeIcon icon={faPlus} />
            &nbsp;
            {t(`${prefix}add.button.label`)}
          </Button>
        </Can>
      </ButtonToolbarRow>
      <Heading fontWeight={"bold"} mb={2} mt={3} size={"xs"}>
        {t(`${prefix}add.creditCard.label`)}
      </Heading>
      <Table
        columns={creditCardTableHeaders}
        data={creditCardData}
        extraButtons={
          <CreditCardExtraButtons
            currentCreditCardRow={currentCreditCardRow}
            handleRemoveCreditCardPaymentMethod={
              handleRemoveCreditCardPaymentMethod
            }
            openUpdateCreditCardModal={openUpdateCreditCardModal}
            removing={removing}
            updatingCreditCard={updatingCreditCard}
          />
        }
        rowSelectionSetter={
          canRemove || canUpdateCreditCard ? setCurrentCreditCardRow : undefined
        }
        rowSelectionState={currentCreditCardRow}
        selectionMode={"radio"}
        tableRef={tableRef1}
      />
      <Heading fontWeight={"bold"} mb={2} mt={3} size={"xs"}>
        {t(`${prefix}add.otherMethods.label`)}
      </Heading>
      <Table
        columns={otherMethodsTableHeaders}
        data={otherMetodData}
        extraButtons={
          <OtherMethodExtraButtons
            currentOtherMethodRow={currentOtherMethodRow}
            handleRemoveOtherPaymentMethod={handleRemoveOtherPaymentMethod}
            openUpdateOhterMethodModal={openUpdateOhterMethodModal}
            removing={removing}
            updatingOther={updatingOther}
          />
        }
        rowSelectionSetter={
          canRemove || canUpdateOther ? setCurrentOtherMethodRow : undefined
        }
        rowSelectionState={currentOtherMethodRow}
        selectionMode={"radio"}
        tableRef={tableRef2}
      />
      {isUpdatingCreditCard === false ? undefined : (
        <UpdateCreditCardModal
          country={country ?? ""}
          isDefault={currentCreditCardRow[0]?.default ?? false}
          onClose={closeUpdateCreditCardModal}
          onSubmit={handleUpdateCreditCardPaymentMethodSubmit}
        />
      )}
      {isAddingPaymentMethod ? (
        <AddPaymentMethod
          country={country}
          onClose={closeAddModal}
          onUpdate={onUpdate}
          organizationId={organizationId}
        />
      ) : undefined}
      {isUpdatingOhterMethod === false ? undefined : (
        <UpdateOtherMethodModal
          country={{ value: country ?? "" }}
          initialValues={{
            businessName: currentOtherMethodRow[0].businessName,
            city: currentOtherMethodRow[0].city,
            country: currentOtherMethodRow[0].country,
            email: currentOtherMethodRow[0].email,
            rutList: undefined,
            state: currentOtherMethodRow[0].state,
            taxIdList: undefined,
          }}
          onClose={closeUpdateOhterMethodModal}
          onSubmit={handleUpdateOtherPaymentMethodSubmit}
          title={t(`${prefix}update.modal.update`)}
        />
      )}
    </div>
  );
};
