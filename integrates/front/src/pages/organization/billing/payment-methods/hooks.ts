import type { ApolloError } from "@apollo/client";

import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";
import { translate } from "utils/translations/translate";

const handleRemovePaymentError = ({ graphQLErrors }: ApolloError): void => {
  graphQLErrors.forEach((error): void => {
    switch (error.message) {
      case "Exception - Cannot perform action. Please add a valid payment method first":
      case "Exception - Invalid payment method. Provided payment method does not exist for this organization":
        msgError(
          translate.t(
            "organization.tabs.billing.paymentMethods.remove.errors.noPaymentMethod",
          ),
        );
        break;
      case "Exception - Cannot perform action. The organization has active or trialing subscriptions":
        msgError(
          translate.t(
            "organization.tabs.billing.paymentMethods.remove.errors.activeSubscriptions",
          ),
        );
        break;
      default:
        msgError(translate.t("groupAlerts.errorTextsad"));
        Logger.error("Couldn't remove payment method", error);
    }
  });
};

const handleUpdateCCError = ({ graphQLErrors }: ApolloError): void => {
  graphQLErrors.forEach((error): void => {
    switch (error.message) {
      case "Exception - Cannot perform action. Please add a valid payment method first":
      case "Exception - Invalid payment method. Provided payment method does not exist for this organization":
        msgError(
          translate.t(
            "organization.tabs.billing.paymentMethods.update.errors.noPaymentMethod",
          ),
        );
        break;
      default:
        msgError(translate.t("groupAlerts.errorTextsad"));
        Logger.error("Couldn't update payment method", error);
    }
  });
};

const handleUpdateOtherError = ({ graphQLErrors }: ApolloError): void => {
  graphQLErrors.forEach((error): void => {
    if (
      error.message ===
      "Exception - Invalid payment method. Provided payment method does not exist for this organization"
    ) {
      msgError(
        translate.t(
          "organization.tabs.billing.paymentMethods.update.errors.noPaymentMethod",
        ),
      );
    } else {
      msgError(translate.t("groupAlerts.errorTextsad"));
      Logger.error("Couldn't update payment method", error);
    }
  });
};

export {
  handleRemovePaymentError,
  handleUpdateCCError,
  handleUpdateOtherError,
};
