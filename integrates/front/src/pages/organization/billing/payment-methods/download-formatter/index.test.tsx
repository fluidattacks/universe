import { screen } from "@testing-library/react";

import { DownloadFormatter } from ".";
import { render } from "mocks";

describe("downloadFormatter", (): void => {
  it("should render a component", (): void => {
    expect.hasAssertions();

    render(
      <DownloadFormatter
        organizationId={""}
        paymentMethod={undefined}
        paymentMethodId={"test"}
      />,
    );

    expect(screen.getByRole("button")).toBeInTheDocument();
  });
});
