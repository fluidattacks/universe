import { DownloadFormatter } from ".";
import type { IPaymentMethodAttr } from "../../types";

export const downloadFormatterRender = (
  paymentMethodId: string,
  organizationId: string,
  paymentMethod: IPaymentMethodAttr,
): JSX.Element => {
  return (
    <DownloadFormatter
      organizationId={organizationId}
      paymentMethod={paymentMethod}
      paymentMethodId={paymentMethodId}
    />
  );
};
