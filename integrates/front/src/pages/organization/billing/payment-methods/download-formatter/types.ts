import type { IPaymentMethodAttr } from "../../types";

interface IDownloadFormatterProps {
  organizationId: string;
  paymentMethod: IPaymentMethodAttr | undefined;
  paymentMethodId: string;
}

export type { IDownloadFormatterProps };
