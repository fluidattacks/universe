import { PureAbility } from "@casl/ability";
import { screen } from "@testing-library/react";
import * as React from "react";

import { OtherMethodExtraButtons } from ".";
import { authzPermissionsContext } from "context/authz/config";
import { render } from "mocks";

const WrapperComponent = ({
  children,
  permissions,
}: Readonly<{
  children: React.ReactNode;
  permissions: PureAbility<string>;
}>): JSX.Element => (
  <authzPermissionsContext.Provider value={permissions}>
    {children}
  </authzPermissionsContext.Provider>
);

const prefix = "organization.tabs.billing.paymentMethods.";
const mockedPermissions = new PureAbility<string>([
  { action: "integrates_api_mutations_update_other_payment_method_mutate" },
  { action: "integrates_api_mutations_remove_payment_method_mutate" },
]);

describe("otherMethodExtraButtons", (): void => {
  it("should render a component", (): void => {
    expect.hasAssertions();

    render(
      <WrapperComponent permissions={mockedPermissions}>
        <OtherMethodExtraButtons
          currentOtherMethodRow={[]}
          handleRemoveOtherPaymentMethod={jest.fn()}
          openUpdateOhterMethodModal={jest.fn()}
          removing={false}
          updatingOther={false}
        />
      </WrapperComponent>,
    );

    expect(screen.getByText(`${prefix}update.button`)).toBeInTheDocument();
    expect(screen.getByText(`${prefix}remove.button`)).toBeInTheDocument();
  });
});
