import { Button, Icon } from "@fluidattacks/design";
import isEmpty from "lodash/isEmpty";
import { Fragment } from "react";
import { useTranslation } from "react-i18next";

import type { IOtherMethodExtraButtonsProps } from "./types";

import { Can } from "context/authz/can";

export const OtherMethodExtraButtons = ({
  currentOtherMethodRow,
  handleRemoveOtherPaymentMethod,
  openUpdateOhterMethodModal,
  removing,
  updatingOther,
}: IOtherMethodExtraButtonsProps): JSX.Element => {
  const { t } = useTranslation();
  const prefix = "organization.tabs.billing.paymentMethods.";

  const disabled = isEmpty(currentOtherMethodRow) || removing || updatingOther;

  return (
    <Fragment>
      <Can do={"integrates_api_mutations_update_other_payment_method_mutate"}>
        <Button
          disabled={disabled}
          id={"updateOtherMethod"}
          onClick={openUpdateOhterMethodModal}
          variant={"ghost"}
        >
          <Icon
            clickable={false}
            icon={"arrow-rotate-right"}
            iconSize={"xs"}
            iconType={"fa-light"}
          />
          &nbsp;
          {t(`${prefix}update.button`)}
        </Button>
      </Can>
      <Can do={"integrates_api_mutations_remove_payment_method_mutate"}>
        <Button
          disabled={disabled}
          id={"removeOtherMethod"}
          onClick={handleRemoveOtherPaymentMethod}
          variant={"ghost"}
        >
          <Icon
            clickable={false}
            icon={"trash"}
            iconSize={"xs"}
            iconType={"fa-light"}
          />
          &nbsp;
          {t(`${prefix}remove.button`)}
        </Button>
      </Can>
    </Fragment>
  );
};
