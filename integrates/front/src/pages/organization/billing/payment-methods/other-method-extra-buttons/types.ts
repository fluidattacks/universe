import type { IPaymentMethodAttr } from "../../types";

interface IOtherMethodExtraButtonsProps {
  currentOtherMethodRow: IPaymentMethodAttr[];
  handleRemoveOtherPaymentMethod: () => void;
  openUpdateOhterMethodModal: () => void;
  removing: boolean;
  updatingOther: boolean;
}

export type { IOtherMethodExtraButtonsProps };
