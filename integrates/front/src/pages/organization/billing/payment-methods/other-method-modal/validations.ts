import type { InferType, Schema } from "yup";
import { lazy, mixed, object, string } from "yup";

import { translate } from "utils/translations/translate";

const emailRegex =
  /^[a-zA-Z0-9.!#$%&’*/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/u;

const validateLocation = (
  locations: string[],
): ((value: string | undefined) => boolean) => {
  return (value): boolean => {
    return locations.length === 0 || value !== undefined;
  };
};

const MAX_BUSINESS_NAME_LENGTH = 60;

const validationSchema = (
  cities: string[],
  states: string[],
): InferType<Schema> =>
  lazy(
    (): Schema =>
      object().shape({
        businessName: string()
          .required()
          .max(MAX_BUSINESS_NAME_LENGTH)
          .matches(
            /^[a-zA-Z0-9ñáéíóúäëïöüÑÁÉÍÓÚÄËÏÖÜ\s'~:;%@_$#!,.*\-?"[\]|()/{}>][a-zA-Z0-9ñáéíóúäëïöüÑÁÉÍÓÚÄËÏÖÜ\s'~:;%@_$#!,.*\-?"[\]|()/{}>=]+$/u,
          ),
        city: string().isValidFunction(
          validateLocation(cities),
          translate.t("validations.required"),
        ),
        country: string().required(),
        email: string()
          .email()
          .matches(emailRegex)
          .when("country", ([country]: string[]): Schema => {
            return country === "Colombia"
              ? string().email().required()
              : string();
          }),
        rutList: mixed().when("country", ([country]: string[]): Schema => {
          return country === "Colombia" ? mixed().required() : mixed();
        }),
        state: string().isValidFunction(
          validateLocation(states),
          translate.t("validations.required"),
        ),
        taxIdList: mixed().when("country", ([country]: string[]): Schema => {
          return country === "Colombia" ? mixed() : mixed().required();
        }),
      }),
  );

export { validationSchema };
