import { screen, waitFor } from "@testing-library/react";

import { UpdateOtherPaymentMethod } from ".";
import { render } from "mocks";

describe("newUpdateOtherPaymentMethod", (): void => {
  it("should render a component %s", async (): Promise<void> => {
    expect.hasAssertions();

    const prefix = "organization.tabs.billing.paymentMethods.add.otherMethods.";

    render(
      <UpdateOtherPaymentMethod
        initialValues={{
          businessName: "",
          city: "",
          country: "Colombia",
          email: "",
          rutList: undefined,
          state: "",
          taxIdList: undefined,
        }}
        onClose={jest.fn()}
        onSubmit={jest.fn()}
      />,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/billing/history"],
        },
      },
    );

    await waitFor((): void => {
      expect(screen.getByText(`${prefix}businessName`)).toBeInTheDocument();
    });

    expect(screen.getByText(`${prefix}rut`)).toBeInTheDocument();
    expect(screen.getByText(`${prefix}email`)).toBeInTheDocument();

    expect(screen.getByText(`${prefix}country`)).toBeInTheDocument();
  });
});
