import { UpdateOtherPaymentMethod } from "./modal";

import type { IOtherMethodModalProps } from "../../types";
import { Modal } from "components/modal";
import { useModal } from "hooks/use-modal";

const UpdateOtherMethodModal = ({
  initialValues,
  onClose,
  onSubmit,
  title,
}: IOtherMethodModalProps & Readonly<{ title: string }>): JSX.Element => {
  const modalProps = useModal("update-other-method");

  return (
    <Modal
      modalRef={{ ...modalProps, close: onClose, isOpen: true }}
      size={"md"}
      title={title}
    >
      <UpdateOtherPaymentMethod
        initialValues={initialValues}
        onClose={onClose}
        onSubmit={onSubmit}
      />
    </Modal>
  );
};

export { UpdateOtherMethodModal };
