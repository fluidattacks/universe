import { Col, Row } from "@fluidattacks/design";
import dayjs from "dayjs";
import { useTranslation } from "react-i18next";
import { boolean, number, object } from "yup";

import type { IUpdateCreditCardModalProps } from "../types";
import { Checkbox } from "components/checkbox";
import { InnerForm } from "components/form";
import { FormModal } from "components/form-modal";
import { InputNumber } from "components/input";
import { useModal } from "hooks/use-modal";

const BASE_YEAR = 2000;
export const UpdateCreditCardModal = ({
  country,
  isDefault,
  onClose,
  onSubmit,
}: IUpdateCreditCardModalProps): JSX.Element => {
  const { t } = useTranslation();
  const modalProps = useModal("update-credit-card");
  const prefix = "organization.tabs.billing.paymentMethods.add.creditCard.";
  const updPrefix = "organization.tabs.billing.paymentMethods.update.modal.";

  const validationSchema = object().shape({
    cardExpirationMonth: number()
      .integer(t("validations.integer"))
      .positive(t("validations.positive"))
      .required(t("validations.required")),
    cardExpirationYear: number()
      .integer(t("validations.integer"))
      .positive(t("validations.positive"))
      .required(t("validations.required"))
      .isValidFunction((value?: number): boolean => {
        if (value === undefined) {
          return false;
        }

        return value >= dayjs().year() - BASE_YEAR;
      }, t("validations.lowerDate")),
    makeDefault: boolean().required(),
  });

  return (
    <FormModal
      initialValues={{
        cardExpirationMonth: undefined,
        cardExpirationYear: undefined,
        makeDefault: isDefault,
      }}
      modalRef={{ ...modalProps, close: onClose, isOpen: true }}
      name={"updateCreditCard"}
      onSubmit={onSubmit}
      size={"md"}
      title={t(`${updPrefix}update`)}
      validationSchema={validationSchema}
    >
      {({ dirty, isSubmitting }): JSX.Element => (
        <InnerForm onCancel={onClose} submitDisabled={!dirty || isSubmitting}>
          {country === "Colombia" ? undefined : (
            <Row>
              <Col lg={50}>
                <InputNumber
                  id={"add-card-expiration-month"}
                  label={t(`${prefix}expirationMonth.label`)}
                  max={12}
                  min={1}
                  name={"cardExpirationMonth"}
                  placeholder={t(`${prefix}expirationMonth.placeholder`)}
                />
              </Col>
              <Col lg={50}>
                <InputNumber
                  id={"add-card-expiration-year"}
                  label={t(`${prefix}expirationYear.label`)}
                  max={99}
                  min={0}
                  name={"cardExpirationYear"}
                  placeholder={t(`${prefix}expirationYear.placeholder`)}
                  type={"number"}
                />
              </Col>
            </Row>
          )}
          <Row>
            <Checkbox
              label={t(`${updPrefix}default`)}
              name={"makeDefault"}
              variant={"formikField"}
            />
          </Row>
        </InnerForm>
      )}
    </FormModal>
  );
};
