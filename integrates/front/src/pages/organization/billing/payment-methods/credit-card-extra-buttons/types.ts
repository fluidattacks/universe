import type { IPaymentMethodAttr } from "../../types";

interface ICreditCardExtraButtonsProps {
  currentCreditCardRow: IPaymentMethodAttr[];
  handleRemoveCreditCardPaymentMethod: () => void;
  openUpdateCreditCardModal: () => void;
  removing: boolean;
  updatingCreditCard: boolean;
}

export type { ICreditCardExtraButtonsProps };
