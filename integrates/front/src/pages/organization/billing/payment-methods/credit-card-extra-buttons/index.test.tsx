import { PureAbility } from "@casl/ability";
import { screen } from "@testing-library/react";
import * as React from "react";

import { CreditCardExtraButtons } from ".";
import { authzPermissionsContext } from "context/authz/config";
import { render } from "mocks";

const WrapperComponent = ({
  children,
  permissions,
}: Readonly<{
  children: React.ReactNode;
  permissions: PureAbility<string>;
}>): JSX.Element => (
  <authzPermissionsContext.Provider value={permissions}>
    {children}
  </authzPermissionsContext.Provider>
);

const prefix = "organization.tabs.billing.paymentMethods.";
const mockedPermissions = new PureAbility<string>([
  {
    action: "integrates_api_mutations_update_credit_card_payment_method_mutate",
  },
  { action: "integrates_api_mutations_remove_payment_method_mutate" },
]);

describe("creditCardExtraButtons", (): void => {
  it("should render a component", (): void => {
    expect.hasAssertions();

    render(
      <WrapperComponent permissions={mockedPermissions}>
        <CreditCardExtraButtons
          currentCreditCardRow={[]}
          handleRemoveCreditCardPaymentMethod={jest.fn()}
          openUpdateCreditCardModal={jest.fn()}
          removing={false}
          updatingCreditCard={false}
        />
      </WrapperComponent>,
    );

    expect(screen.getByText(`${prefix}update.button`)).toBeInTheDocument();
    expect(screen.getByText(`${prefix}remove.button`)).toBeInTheDocument();
  });
});
