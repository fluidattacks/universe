import { screen, waitFor } from "@testing-library/react";

import { OrganizationAuthors } from ".";
import { render } from "mocks";

describe("organizationAuthors", (): void => {
  const TEST_DATE = 2020;
  const date = new Date(TEST_DATE, 0);

  it("should display the organization billing overview", async (): Promise<void> => {
    expect.hasAssertions();

    jest.useFakeTimers().setSystemTime(date);

    render(
      <OrganizationAuthors
        authors={[
          {
            activeGroups: [
              {
                name: "continuoustesting",
                tier: "ADVANCED",
              },
              {
                name: "unittesting",
                tier: "ADVANCED",
              },
            ],
            actor: "Dev 1 <dev1@fluidattacks.com>",
          },
          {
            activeGroups: [
              {
                name: "unittesting",
                tier: "ADVANCED",
              },
            ],
            actor: "Dev 2 <dev1@fluidattacks.com>",
          },
        ]}
      />,
    );
    await waitFor((): void => {
      expect(screen.getByText("Dev 2")).toBeInTheDocument();
    });

    expect(
      screen.getByText("organization.tabs.billing.authors.title"),
    ).toBeInTheDocument();

    expect(
      screen.getByText("organization.tabs.billing.authors.headers.authorEmail"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("organization.tabs.billing.authors.headers.authorName"),
    ).toBeInTheDocument();
    expect(
      screen.getByText(
        "organization.tabs.billing.authors.headers.activeGroups",
      ),
    ).toBeInTheDocument();

    expect(screen.getByText("Dev 1")).toBeInTheDocument();
    expect(
      screen.getByText("continuoustesting, unittesting"),
    ).toBeInTheDocument();

    expect(screen.getByText("unittesting")).toBeInTheDocument();

    jest.useRealTimers();
  });
});
