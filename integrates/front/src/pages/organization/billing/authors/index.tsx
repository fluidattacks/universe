import { Heading } from "@fluidattacks/design";
import type { ColumnDef } from "@tanstack/react-table";
import _, { isNull, isUndefined } from "lodash";
import { useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import type {
  IOrganizationActorAttr,
  IOrganizationAuthorAttr,
  IOrganizationAuthorsTable,
} from "../types";
import { Filters } from "components/filter";
import type { IFilter } from "components/filter/types";
import { Table } from "components/table";
import { setFiltersUtil, useTable } from "hooks";

interface IOrganizationAuthorAttrsProps {
  readonly authors: IOrganizationAuthorAttr[];
}

export const OrganizationAuthors: React.FC<IOrganizationAuthorAttrsProps> = ({
  authors,
}): JSX.Element => {
  const { t } = useTranslation();

  const tableRef = useTable("tblGroups");
  const formatActor = (actor: string): IOrganizationActorAttr | undefined => {
    const textMatch = /^(?<name>.+) <(?<email>[^>]+)>$/u.exec(actor);
    if (isNull(textMatch)) {
      return undefined;
    }

    if (isUndefined(textMatch.groups)) {
      return undefined;
    }
    const { email, name } = textMatch.groups;

    return { email, name };
  };

  const formatAuthorsData = (
    authorData: IOrganizationAuthorAttr[],
  ): IOrganizationAuthorsTable[] =>
    authorData.map((author): IOrganizationAuthorsTable => {
      const actor = formatActor(author.actor);
      const actorName = _.capitalize(actor?.name);
      const actorEmail = actor?.email;
      const groupsAuthors = author.activeGroups
        .map((group): string => group.name)
        .join(", ");

      return {
        actorEmail,
        actorName,
        groupsAuthors,
      };
    });

  const tableColumns: ColumnDef<IOrganizationAuthorsTable>[] = [
    {
      accessorKey: "actorName",
      header: t("organization.tabs.billing.authors.headers.authorName"),
    },
    {
      accessorKey: "actorEmail",
      header: t("organization.tabs.billing.authors.headers.authorEmail"),
    },
    {
      accessorKey: "groupsAuthors",
      header: t("organization.tabs.billing.authors.headers.activeGroups"),
    },
  ];

  const [filters, setFilters] = useState<IFilter<IOrganizationAuthorsTable>[]>([
    {
      id: "actorEmail",
      key: "actorEmail",
      label: t("organization.tabs.billing.authors.headers.authorEmail"),
      type: "text",
    },
    {
      id: "actorName",
      key: "actorName",
      label: t("organization.tabs.billing.authors.headers.authorName"),
      type: "text",
    },
    {
      id: "groupsAuthors",
      key: "groupsAuthors",
      label: t("organization.tabs.billing.authors.headers.activeGroups"),
      type: "text",
    },
  ]);

  const dataset = formatAuthorsData(authors);

  const filteredDataset = setFiltersUtil(dataset, filters);

  return (
    <div>
      <Heading fontWeight={"bold"} mb={2} mt={2} size={"md"}>
        {t("organization.tabs.billing.authors.title")}
      </Heading>
      <Table
        columns={tableColumns}
        csvConfig={{
          export: true,
          headers: {
            groupsAuthors: t(
              "organization.tabs.billing.authors.headers.activeGroups",
            ),
          },
          name: t("organization.tabs.billing.authors.title"),
        }}
        data={filteredDataset}
        filters={<Filters filters={filters} setFilters={setFilters} />}
        tableRef={tableRef}
      />
    </div>
  );
};
