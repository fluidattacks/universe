import _ from "lodash";
import * as React from "react";
import { useLocation } from "react-router-dom";

import { Charts } from "features/charts";
import type { IOrganizationAnalyticsProps } from "pages/organization/analytics/types";

const OrganizationAnalytics: React.FC<IOrganizationAnalyticsProps> = ({
  organizationId,
}): JSX.Element => {
  const searchParams = new URLSearchParams(useLocation().search);
  const maybeOrganizationId = searchParams.get("organization");

  /*
   * Attempt to read the organization ID from passed Components properties,
   *   or from URL search query, whatever is available first
   */
  const auxOrganizationId = _.isUndefined(organizationId) ? "" : organizationId;
  const subject = _.isNull(maybeOrganizationId)
    ? auxOrganizationId
    : maybeOrganizationId;

  return (
    <React.StrictMode>
      <Charts
        bgChange={searchParams.get("bgChange") === "true"}
        entity={"organization"}
        reportMode={searchParams.get("reportMode") === "true"}
        subject={subject}
      />
    </React.StrictMode>
  );
};

export { OrganizationAnalytics };
