import { Alert, Col, Container, IconButton, Row } from "@fluidattacks/design";
import type { FieldArrayRenderProps } from "formik";
import { Fragment, useCallback } from "react";
import { useTranslation } from "react-i18next";

import { policyOptions } from "./utils";

import type { IPolicy } from "../../types";
import { InputNumber, Select } from "components/input";

const MAX_TECHNIQUE = 7;

export const AddPriorityCriteria = ({
  criteriaName,
  name,
  form: { getFieldProps },
  push,
  remove,
}: FieldArrayRenderProps & Readonly<{ criteriaName: string }>): JSX.Element => {
  const { t } = useTranslation();
  const criteria = getFieldProps(name).value as IPolicy[];

  const addItem = useCallback((): void => {
    push({ policy: "", value: 0 });
  }, [push]);

  const removeItem = useCallback(
    (index: number): (() => void) =>
      (): void => {
        remove(index);
      },
    [remove],
  );

  return (
    <Row align={"center"}>
      {criteria.length > 0 ? (
        <Fragment>
          {criteria.map((_, index: number): JSX.Element => {
            const fieldName = `${name}.${index}`;

            return (
              <Container
                alignItems={"start"}
                display={"inline-flex"}
                gap={0.5}
                key={fieldName}
              >
                <Col lg={50} md={50} sm={50}>
                  <Select
                    items={policyOptions(criteriaName)}
                    label={t(
                      "organization.tabs.policies.priority.modal.criterion",
                    )}
                    name={`${fieldName}.policy`}
                    placeholder={t(
                      "organization.tabs.policies.priority.modal.placeholder",
                    )}
                  />
                </Col>
                <Col lg={35} md={35} sm={35}>
                  <InputNumber
                    label={t(
                      "organization.tabs.policies.priority.modal.priorityValue",
                    )}
                    max={1000}
                    min={-1000}
                    name={`${fieldName}.value`}
                  />
                </Col>
                <Container
                  alignItems={"center"}
                  display={"inline-flex"}
                  gap={0.5}
                  mt={1.25}
                >
                  {criteria.length > 1 ? (
                    <Col>
                      &nbsp;
                      <IconButton
                        icon={"trash"}
                        iconSize={"sm"}
                        iconType={"fa-light"}
                        onClick={removeItem(index)}
                        variant={"ghost"}
                      />
                    </Col>
                  ) : undefined}
                  {criteria.length < MAX_TECHNIQUE &&
                  index === criteria.length - 1 ? (
                    <Col>
                      &nbsp;
                      <IconButton
                        icon={"plus"}
                        iconSize={"xs"}
                        onClick={addItem}
                      />
                    </Col>
                  ) : undefined}
                </Container>
              </Container>
            );
          })}
        </Fragment>
      ) : undefined}
      <Alert variant={"warning"}>
        {t("organization.tabs.policies.priority.modal.help")}
      </Alert>
    </Row>
  );
};
