import { translate } from "utils/translations/translate";

const severityPrefix = "organization.tabs.policies.priority.criteria.";
// Attack vector
const network = translate.t(`${severityPrefix}attack.network`);
const adjacent = translate.t(`${severityPrefix}attack.adjacent`);
const local = translate.t(`${severityPrefix}attack.local`);
const physical = translate.t(`${severityPrefix}attack.physical`);
// Availability
const avHigh = translate.t(`${severityPrefix}availability.high`);
const avLow = translate.t(`${severityPrefix}availability.low`);
const avNone = translate.t(`${severityPrefix}availability.none`);
// Confidentiality
const high = translate.t(`${severityPrefix}confidentiality.high`);
const low = translate.t(`${severityPrefix}confidentiality.low`);
const none = translate.t(`${severityPrefix}confidentiality.none`);
// Exploitability
const attacked = translate.t(`${severityPrefix}exploitability.attacked`);
const notDefined = translate.t(`${severityPrefix}exploitability.notDefined`);
const pOC = translate.t(`${severityPrefix}exploitability.proofOfConcept`);
const unreported = translate.t(`${severityPrefix}exploitability.unreported`);
// Integrity
const intHigh = translate.t(`${severityPrefix}integrity.high`);
const intLow = translate.t(`${severityPrefix}integrity.low`);
const intNone = translate.t(`${severityPrefix}integrity.none`);
// Reachability
const reach = translate.t(`${severityPrefix}reachability.true`);
const notReach = translate.t(`${severityPrefix}reachability.false`);

const attackVectorOptions = [
  { header: network, value: `${network} (AV:${network[0]})` },
  { header: adjacent, value: `${adjacent} (AV:${adjacent[0]})` },
  { header: local, value: `${local} (AV:${local[0]})` },
  { header: physical, value: `${physical} (AV:${physical[0]})` },
];
const availabilityOptions = [
  { header: avHigh.split(" ")[0], value: `${avHigh} (VA:${avHigh[0]})` },
  { header: avLow.split(" ")[0], value: `${avLow} (VA:${avLow[0]})` },
  { header: avNone.split(" ")[0], value: `${avNone} (VA:${avNone[0]})` },
];
const confidentialityOptions = [
  { header: high.split(" ")[0], value: `${high} (VC:${high[0]})` },
  { header: low.split(" ")[0], value: `${low} (VC:${low[0]})` },
  { header: none.split(" ")[0], value: `${none} (VC:${none[0]})` },
];
const exploitabilityOptions = [
  { header: attacked, value: `${attacked} (E:${attacked[0]})` },
  { header: notDefined, value: `${notDefined} (E:${notDefined[0]})` },
  { header: pOC, value: `${pOC} (E:${pOC[0]})` },
  { header: unreported, value: `${unreported} (E:${unreported[0]})` },
];
const integrityOptions = [
  { header: intHigh.split(" ")[0], value: `${intHigh} (VI:${intHigh[0]})` },
  { header: intLow.split(" ")[0], value: `${intLow} (VI:${intLow[0]})` },
  { header: intNone.split(" ")[0], value: `${intNone} (VI:${intNone[0]})` },
];
const reachabilityOptions = [
  { header: reach, value: `${reach} (true)` },
  { header: notReach, value: `${notReach} (false)` },
];
const techniqueOptions = [
  {
    header: translate.t("searchFindings.tabVuln.technique.cspm"),
    value: "CSPM",
  },
  {
    header: translate.t("searchFindings.tabVuln.technique.dast"),
    value: "DAST",
  },
  {
    header: translate.t("searchFindings.tabVuln.technique.ptaas"),
    value: "PTAAS",
  },
  { header: translate.t("searchFindings.tabVuln.technique.re"), value: "RE" },
  {
    header: translate.t("searchFindings.tabVuln.technique.sast"),
    value: "SAST",
  },
  { header: translate.t("searchFindings.tabVuln.technique.sca"), value: "SCA" },
  { header: translate.t("searchFindings.tabVuln.technique.scr"), value: "SCR" },
];

const policyOptions = (opts: string): { header: string; value: string }[] => {
  switch (opts) {
    case "ATTACK_VECTOR":
      return attackVectorOptions;
    case "AVAILABILITY":
      return availabilityOptions;
    case "CONFIDENTIALITY":
      return confidentialityOptions;
    case "EXPLOITABILITY":
      return exploitabilityOptions;
    case "INTEGRITY":
      return integrityOptions;
    case "REACHABILITY":
      return reachabilityOptions;
    case "TECHNIQUE":
    default:
      return techniqueOptions;
  }
};

export { policyOptions };
