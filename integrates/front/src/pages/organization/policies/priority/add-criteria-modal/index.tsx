import { FieldArray, useFormikContext } from "formik";
import isEmpty from "lodash/isEmpty";
import { useCallback } from "react";
import { useTranslation } from "react-i18next";

import { AddPriorityCriteria } from "./priority-array";

import type { ICriteriaProps } from "../types";
import { InnerForm } from "components/form";
import { Select } from "components/input";
import { Modal } from "components/modal";

const AddCriteriaModal = ({
  loading,
  modalRef,
}: ICriteriaProps): JSX.Element => {
  const { t } = useTranslation();
  const { close } = modalRef;
  const { dirty, resetForm, getFieldProps } = useFormikContext();
  const { value } = getFieldProps<string>("optionsCriteria");
  const prefix = "organization.tabs.policies.priority.";

  const handleClose = useCallback((): void => {
    resetForm();
    close();
  }, [close, resetForm]);

  return (
    <Modal
      modalRef={{ ...modalRef, close: handleClose }}
      size={"md"}
      title={t(`${prefix}modal.title`)}
    >
      <InnerForm onCancel={handleClose} submitDisabled={!dirty || loading}>
        <Select
          items={[
            {
              header: t(`${prefix}criteria.options.attack`),
              value: "ATTACK_VECTOR",
            },
            {
              header: t(`${prefix}criteria.options.availability`),
              value: "AVAILABILITY",
            },
            {
              header: t(`${prefix}criteria.options.confidentiality`),
              value: "CONFIDENTIALITY",
            },
            {
              header: t(`${prefix}criteria.options.exploitability`),
              value: "EXPLOITABILITY",
            },
            {
              header: t(`${prefix}criteria.options.integrity`),
              value: "INTEGRITY",
            },
            {
              header: t(`${prefix}criteria.options.reachability`),
              value: "REACHABILITY",
            },
            {
              header: t(`${prefix}criteria.options.technique`),
              value: "TECHNIQUE",
            },
          ]}
          label={t(`${prefix}criteria.text`)}
          name={"optionsCriteria"}
          placeholder={t(`${prefix}criteria.placeholder`)}
        />
        {isEmpty(value) ? undefined : (
          <FieldArray name={"criteria"}>
            {(props): JSX.Element => (
              <AddPriorityCriteria criteriaName={value} {...props} />
            )}
          </FieldArray>
        )}
      </InnerForm>
    </Modal>
  );
};

export { AddCriteriaModal };
