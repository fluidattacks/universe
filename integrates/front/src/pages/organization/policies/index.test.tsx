import { PureAbility } from "@casl/ability";
import { screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import { Route, Routes } from "react-router-dom";

import { GET_CRITERIA } from "./acceptance/vulnerability-policies/queries";

import { authzPermissionsContext } from "context/authz/config";
import type {
  GetCriteriaPolicyQuery,
  GetOrganizationPriorityPoliciesQuery,
  GetPoliciesAtOrganizationQuery,
} from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";
import { OrganizationPolicies } from "pages/organization/policies";
import {
  GET_ORGANIZATION_PRIORITY_POLICIES,
  GET_POLICIES,
} from "pages/organization/policies/queries";
import type { IOrganizationPolicies } from "pages/organization/policies/types";

const graphqlMocked = graphql.link(LINK);

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");

  jest.spyOn(mockedNotifications, "msgError").mockImplementation();
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

describe("organizationPolicies", (): void => {
  const memoryRouter = {
    initialEntries: ["/orgs/okada/policies"],
  };
  const mockProps: IOrganizationPolicies = {
    organizationId: "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
  };

  const criteriaConnection = {
    edges: [],
    pageInfo: {
      endCursor: "060",
      hasNextPage: false,
    },
    total: 2,
  };

  const criteriaQuery = graphqlMocked.query(
    GET_CRITERIA,
    (): StrictResponse<{ data: GetCriteriaPolicyQuery }> => {
      return HttpResponse.json({
        data: {
          criteriaConnection,
        },
      });
    },
  );

  it("navigates", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      {
        action: "integrates_api_mutations_update_organization_policies_mutate",
      },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={
              <OrganizationPolicies organizationId={mockProps.organizationId} />
            }
            path={"/orgs/:organizationName/policies/*"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter,
        mocks: [
          graphqlMocked.query(
            GET_POLICIES,
            (): StrictResponse<{ data: GetPoliciesAtOrganizationQuery }> => {
              return HttpResponse.json({
                data: {
                  organization: {
                    __typename: "Organization",
                    daysUntilItBreaks: null,
                    groups: [],
                    inactivityPeriod: 90,
                    maxAcceptanceDays: 5,
                    maxAcceptanceSeverity: 7.5,
                    maxNumberAcceptances: 2,
                    minAcceptanceSeverity: 3,
                    minBreakingSeverity: 3,
                    name: "okada",
                    vulnerabilityGracePeriod: 1,
                  },
                },
              });
            },
          ),
          graphqlMocked.query(
            GET_ORGANIZATION_PRIORITY_POLICIES,
            ({
              variables,
            }): StrictResponse<
              IErrorMessage | { data: GetOrganizationPriorityPoliciesQuery }
            > => {
              const { organizationId } = variables;

              if (
                organizationId === "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
              ) {
                return HttpResponse.json({
                  data: {
                    organization: {
                      __typename: "Organization",
                      name: "okada",
                      priorityPolicies: [
                        {
                          __typename: "PriorityPolicy",
                          policy: "CSPM",
                          value: 20,
                        },
                        {
                          __typename: "PriorityPolicy",
                          policy: "PTAAS",
                          value: -45,
                        },
                      ],
                    },
                  },
                });
              }

              return HttpResponse.json({
                errors: [
                  new GraphQLError("Organization priority policies error"),
                ],
              });
            },
          ),
          criteriaQuery,
        ],
      },
    );

    expect(document.getElementById("policies-tab")).toBeInTheDocument();

    await userEvent.click(
      document.getElementById("policies-tab") as HTMLElement,
    );

    expect(screen.getByText("Okada")).toBeInTheDocument();

    await userEvent.click(
      document.getElementById("priority-tab") as HTMLElement,
    );

    expect(screen.getByText("Priority score")).toBeInTheDocument();

    await userEvent.click(
      document.getElementById("acceptance-tab") as HTMLElement,
    );

    expect(
      screen.getByText("organization.tabs.policies.findings.title.temporary"),
    ).toBeInTheDocument();
  });
});
