import { PureAbility } from "@casl/ability";
import { screen, waitFor, within } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import { Route, Routes } from "react-router-dom";

import { PoliciesSettings } from ".";
import { authzPermissionsContext } from "context/authz/config";
import type {
  GetPoliciesAtOrganizationQuery,
  UpdateGroupsPoliciesMutation,
  UpdateOrganizationPoliciesMutation,
} from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";
import {
  GET_POLICIES,
  UPDATE_GROUPS_POLICIES,
  UPDATE_ORGANIZATION_POLICIES,
} from "pages/organization/policies/queries";
import { msgSuccess } from "utils/notifications";

const graphqlMocked = graphql.link(LINK);

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");

  jest.spyOn(mockedNotifications, "msgError").mockImplementation();
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

describe("organizationPolicies", (): void => {
  const orgId = "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3";

  const memoryRouter = {
    initialEntries: ["/orgs/okada/policies/settings"],
  };

  const clearAndType = async (
    elementName: string,
    value: string,
  ): Promise<void> => {
    await userEvent.clear(
      screen.getByRole("spinbutton", { name: elementName }),
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: elementName }),
      value,
    );
  };

  const policiesQuery = graphqlMocked.query(
    GET_POLICIES,
    (): StrictResponse<{ data: GetPoliciesAtOrganizationQuery }> => {
      return HttpResponse.json({
        data: {
          organization: {
            __typename: "Organization",
            daysUntilItBreaks: null,
            groups: [
              {
                __typename: "Group",
                daysUntilItBreaks: null,
                maxAcceptanceDays: 3,
                maxAcceptanceSeverity: 4,
                maxNumberAcceptances: 2,
                minAcceptanceSeverity: 1,
                minBreakingSeverity: 3.5,
                name: "group1",
                vulnerabilityGracePeriod: 2,
              },
              {
                __typename: "Group",
                daysUntilItBreaks: 122,
                maxAcceptanceDays: 5,
                maxAcceptanceSeverity: 2.3,
                maxNumberAcceptances: 2,
                minAcceptanceSeverity: 0,
                minBreakingSeverity: 2,
                name: "group2",
                vulnerabilityGracePeriod: 3,
              },
            ],
            inactivityPeriod: 90,
            maxAcceptanceDays: 5,
            maxAcceptanceSeverity: 7.5,
            maxNumberAcceptances: 2,
            minAcceptanceSeverity: 3,
            minBreakingSeverity: 3,
            name: "okada",
            vulnerabilityGracePeriod: 1,
          },
        },
      });
    },
  );

  it("updates organization policies", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const mockedPermissions = new PureAbility<string>([
      {
        action: "integrates_api_mutations_update_organization_policies_mutate",
      },
    ]);
    render(
      <Routes>
        <Route
          element={
            <authzPermissionsContext.Provider value={mockedPermissions}>
              <PoliciesSettings organizationId={orgId} />
            </authzPermissionsContext.Provider>
          }
          path={"/orgs/:organizationName/policies/settings"}
        />
      </Routes>,
      {
        memoryRouter,
        mocks: [
          policiesQuery,
          graphqlMocked.mutation(
            UPDATE_ORGANIZATION_POLICIES,
            ({
              variables,
            }): StrictResponse<
              IErrorMessage | { data: UpdateOrganizationPoliciesMutation }
            > => {
              const expectedValues = {
                daysUntilItBreaks: null,
                inactivityPeriod: 90,
                maxAcceptanceDays: 1,
                maxAcceptanceSeverity: 6.5,
                maxNumberAcceptances: 4,
                minAcceptanceSeverity: 2,
                minBreakingSeverity: 6,
                organizationId: "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
                organizationName: "okada",
                vulnerabilityGracePeriod: 1,
              };

              if (JSON.stringify(variables) === JSON.stringify(expectedValues))
                return HttpResponse.json({
                  data: {
                    updateOrganizationPolicies: {
                      success: true,
                    },
                  },
                });

              return HttpResponse.json({
                errors: [new GraphQLError("Exception - Validation error")],
              });
            },
            { once: true },
          ),
        ],
      },
    );

    await waitFor((): void => {
      expect(screen.queryByText("buttons.edit")).toBeInTheDocument();
    });
    await userEvent.click(screen.getByText("buttons.edit"));

    expect(screen.getByText("policies.editOrgPolicies")).toBeInTheDocument();

    await clearAndType("maxAcceptanceDays", "1");
    await clearAndType("maxAcceptanceSeverity", "6.5");
    await clearAndType("minAcceptanceSeverity", "2");
    await clearAndType("maxNumberAcceptances", "4");
    await clearAndType("minBreakingSeverity", "6");
    await userEvent.click(screen.getByText("Confirm"));

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledTimes(1);
    });
  });

  it("updates groups policies", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const mockedPermissions = new PureAbility<string>([
      {
        action: "integrates_api_mutations_update_organization_policies_mutate",
      },
      {
        action: "integrates_api_mutations_update_groups_policies__update_group",
      },
    ]);
    render(
      <Routes>
        <Route
          element={
            <authzPermissionsContext.Provider value={mockedPermissions}>
              <PoliciesSettings organizationId={orgId} />
            </authzPermissionsContext.Provider>
          }
          path={"/orgs/:organizationName/policies/settings"}
        />
      </Routes>,
      {
        memoryRouter,
        mocks: [
          policiesQuery,
          graphqlMocked.mutation(
            UPDATE_GROUPS_POLICIES,
            ({
              variables,
            }): StrictResponse<
              IErrorMessage | { data: UpdateGroupsPoliciesMutation }
            > => {
              const expectedValues = {
                daysUntilItBreaks: 122,
                groups: ["group1"],
                maxAcceptanceDays: 1,
                maxAcceptanceSeverity: 4.5,
                maxNumberAcceptances: 4,
                minAcceptanceSeverity: 2,
                minBreakingSeverity: 6,
                organizationId: "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
                vulnerabilityGracePeriod: 2,
              };

              if (JSON.stringify(variables) === JSON.stringify(expectedValues))
                return HttpResponse.json({
                  data: {
                    updateGroupsPolicies: {
                      success: true,
                    },
                  },
                });

              return HttpResponse.json({
                errors: [new GraphQLError("Exception - Validation error")],
              });
            },
            { once: true },
          ),
        ],
      },
    );

    await waitFor((): void => {
      expect(
        screen.queryByRole("table")?.getElementsByTagName("tr"),
      ).toHaveLength(4);
    });

    await userEvent.click(
      within(
        screen
          .getByRole("table")
          .getElementsByTagName("tbody")[0]
          .getElementsByTagName("tr")[0],
      ).getByText("Edit"),
    );

    expect(screen.getByText("policies.editGroupPolicies")).toBeInTheDocument();

    await clearAndType("maxAcceptanceDays", "1");
    await clearAndType("maxAcceptanceSeverity", "4.5");
    await clearAndType("minAcceptanceSeverity", "2");
    await clearAndType("maxNumberAcceptances", "4");
    await clearAndType("minBreakingSeverity", "6");
    await clearAndType("daysUntilItBreaks", "122");
    await userEvent.click(screen.getByText("Confirm"));

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledTimes(1);
    });
  });

  it("resets groups policies to org policies", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const mockedPermissions = new PureAbility<string>([
      {
        action: "integrates_api_mutations_update_organization_policies_mutate",
      },
      {
        action: "integrates_api_mutations_update_groups_policies__update_group",
      },
    ]);
    render(
      <Routes>
        <Route
          element={
            <authzPermissionsContext.Provider value={mockedPermissions}>
              <PoliciesSettings organizationId={orgId} />
            </authzPermissionsContext.Provider>
          }
          path={"/orgs/:organizationName/policies/settings"}
        />
      </Routes>,
      {
        memoryRouter,
        mocks: [
          policiesQuery,
          graphqlMocked.mutation(
            UPDATE_GROUPS_POLICIES,
            ({
              variables,
            }): StrictResponse<
              IErrorMessage | { data: UpdateGroupsPoliciesMutation }
            > => {
              const expectedValues = {
                daysUntilItBreaks: null,
                groups: ["group1", "group2"],
                maxAcceptanceDays: 5,
                maxAcceptanceSeverity: 7.5,
                maxNumberAcceptances: 2,
                minAcceptanceSeverity: 3,
                minBreakingSeverity: 3,
                organizationId: "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
                vulnerabilityGracePeriod: 1,
              };

              if (JSON.stringify(variables) === JSON.stringify(expectedValues))
                return HttpResponse.json({
                  data: {
                    updateGroupsPolicies: {
                      success: true,
                    },
                  },
                });

              return HttpResponse.json({
                errors: [new GraphQLError("Exception - Validation error")],
              });
            },
            { once: true },
          ),
        ],
      },
    );

    await waitFor((): void => {
      expect(
        document.querySelector("input[name='select-all']"),
      ).toBeInTheDocument();
    });

    await userEvent.click(
      document.querySelector("input[name='select-all']") as Element,
    );

    expect(screen.getByText("policies.reset.label")).not.toBeDisabled();

    await userEvent.click(screen.getByText("policies.reset.label"));
    await userEvent.click(screen.getByText("Confirm"));

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledTimes(1);
    });
  });
});
