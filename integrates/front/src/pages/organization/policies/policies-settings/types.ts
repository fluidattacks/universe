interface IPoliciesSettings {
  organizationId: string;
}

export type { IPoliciesSettings };
