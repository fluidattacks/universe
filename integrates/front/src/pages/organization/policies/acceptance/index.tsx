import { useQuery } from "@apollo/client";
import { Container } from "@fluidattacks/design";
import orderBy from "lodash/orderBy";
import * as React from "react";
import { useTranslation } from "react-i18next";

import type { IAcceptance } from "./types";
import { VulnerabilityPolicies } from "./vulnerability-policies";
import type { IVulnerabilityPoliciesData } from "./vulnerability-policies/types";

import { GET_FINDINGS_POLICIES } from "../queries";
import { useAudit } from "hooks/use-audit";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";

const Acceptance: React.FC<IAcceptance> = ({
  organizationId,
}: Readonly<IAcceptance>): JSX.Element => {
  const { addAuditEvent } = useAudit();
  const { t } = useTranslation();

  // GraphQL Operations
  const { data } = useQuery(GET_FINDINGS_POLICIES, {
    onCompleted: (): void => {
      addAuditEvent("OrgFindingPolicy", organizationId);
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.warning(
          "An error occurred fetching organization policies",
          error,
        );
      });
    },
    variables: { organizationId },
  });

  if (!data?.organization) {
    return <div />;
  }

  const filterDataByTreatment = (
    policies: IVulnerabilityPoliciesData[],
    treatmentValue: "ACCEPTED_UNDEFINED" | "ACCEPTED",
  ): IVulnerabilityPoliciesData[] =>
    policies.filter(
      (policy: IVulnerabilityPoliciesData): boolean =>
        policy.treatmentAcceptance === treatmentValue,
    );

  const orderByStatusUpdate = (
    policies: IVulnerabilityPoliciesData[],
  ): IVulnerabilityPoliciesData[] =>
    orderBy(policies, "lastStatusUpdate", "desc");

  return (
    <Container mx={1.5} my={1.5}>
      <p className={"f3 fw7 mt4 mb3"}>
        {t("organization.tabs.policies.findings.title.permanent")}
      </p>
      <span id={"permanent-policies"}>
        <VulnerabilityPolicies
          organizationId={organizationId}
          userRole={data.organization.userRole}
          vulnerabilityPolicies={orderByStatusUpdate(
            filterDataByTreatment(
              data.organization.findingPolicies,
              "ACCEPTED_UNDEFINED",
            ),
          )}
          vulnerabilityTreatment={"ACCEPTED_UNDEFINED"}
        />
      </span>

      <br />
      <p className={"f3 fw7 mt4 mb3"}>
        {t("organization.tabs.policies.findings.title.temporary")}
      </p>
      <span id={"temporary-policies"}>
        <VulnerabilityPolicies
          organizationId={organizationId}
          userRole={data.organization.userRole}
          vulnerabilityPolicies={orderByStatusUpdate(
            filterDataByTreatment(
              data.organization.findingPolicies,
              "ACCEPTED",
            ),
          )}
          vulnerabilityTreatment={"ACCEPTED"}
        />
      </span>
    </Container>
  );
};

export { Acceptance };
