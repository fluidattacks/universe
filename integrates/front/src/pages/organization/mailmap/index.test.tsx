import { PureAbility } from "@casl/ability";
import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import { HttpResponse, graphql } from "msw";
import type { GraphQLHandler, StrictResponse } from "msw";
import { useTranslation } from "react-i18next";
import { Route, Routes } from "react-router-dom";

import {
  CONVERT_MAILMAP_SUBENTRIES_TO_NEW_ENTRY_MUTATION,
  CREATE_MAILMAP_ENTRY_MUTATION,
  CREATE_MAILMAP_SUBENTRY_MUTATION,
  DELETE_MAILMAP_ENTRY_WITH_SUBENTRIES_MUTATION,
  DELETE_MAILMAP_SUBENTRY_MUTATION,
  GET_MAILMAP_ENTRY_SUBENTRIES_QUERY,
  GET_MAILMAP_RECORDS_QUERY,
  IMPORT_MAILMAP_MUTATION,
  MOVE_MAILMAP_ENTRIES_WITH_SUBENTRIES_MUTATION,
  SET_MAILMAP_ENTRY_AS_MAILMAP_SUBENTRY_MUTATION,
  SET_MAILMAP_SUBENTRY_AS_MAILMAP_ENTRY_MUTATION,
  UPDATE_MAILMAP_ENTRY_MUTATION,
  UPDATE_MAILMAP_SUBENTRY_MUTATION,
} from "./queries";

import { Mailmap } from ".";
import { authzPermissionsContext } from "context/authz/config";
import { formatMailmapEntryEmail } from "features/mailmap/formatted-entry-email";
import type {
  ConvertMailmapSubentriesToNewEntryMutation,
  CreateMailmapEntryMutation,
  CreateMailmapSubentryMutation,
  DeleteMailmapEntryWithSubentriesMutation,
  DeleteMailmapSubentryMutation,
  ImportMailmapMutation,
  MailmapEntrySubentriesQuery,
  MailmapRecordsQuery,
  MoveMailmapEntryWithSubentriesMutation,
  SetMailmapEntryAsMailmapSubentryMutation,
  SetMailmapSubentryAsMailmapEntryMutation,
  UpdateMailmapEntryMutation,
  UpdateMailmapSubentryMutation,
} from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";
import { msgError, msgSuccess } from "utils/notifications";

const graphqlMocked = graphql.link(LINK);

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

jest.mock(
  "features/mailmap/formatted-entry-email",
  (): Record<string, unknown> => ({
    formatMailmapEntryEmail: jest.fn(),
  }),
);

const mailmapEntriesQuery = graphqlMocked.query(
  GET_MAILMAP_RECORDS_QUERY,
  (): StrictResponse<{ data: MailmapRecordsQuery }> => {
    return HttpResponse.json({
      data: {
        mailmapRecords: [
          {
            __typename: "MailmapRecord",
            mailmapEntryCreatedAt: null,
            mailmapEntryEmail: "alice@entry.com",
            mailmapEntryName: "Alice Garcia",
            mailmapEntryUpdatedAt: null,
            mailmapSubentryCreatedAt: null,
            mailmapSubentryEmail: "alice@entry.com",
            mailmapSubentryName: "Alice Garcia",
            mailmapSubentryUpdatedAt: null,
          },
          {
            __typename: "MailmapRecord",
            mailmapEntryCreatedAt: null,
            mailmapEntryEmail: "bob@entry.com",
            mailmapEntryName: "Bob Singh",
            mailmapEntryUpdatedAt: null,
            mailmapSubentryCreatedAt: null,
            mailmapSubentryEmail: "bob@subentry.com",
            mailmapSubentryName: "Bob Singh",
            mailmapSubentryUpdatedAt: null,
          },
          {
            __typename: "MailmapRecord",
            mailmapEntryCreatedAt: null,
            mailmapEntryEmail: "charlie@entry.com",
            mailmapEntryName: "Charlie Nguyen",
            mailmapEntryUpdatedAt: null,
            mailmapSubentryCreatedAt: null,
            mailmapSubentryEmail: "charlie@subentry.com",
            mailmapSubentryName: "Charlie Nguyen",
            mailmapSubentryUpdatedAt: null,
          },
          {
            __typename: "MailmapRecord",
            mailmapEntryCreatedAt: null,
            mailmapEntryEmail: "eve@entry.com",
            mailmapEntryName: "Eve Anderson",
            mailmapEntryUpdatedAt: null,
            mailmapSubentryCreatedAt: null,
            mailmapSubentryEmail: "eve@subentry.com",
            mailmapSubentryName: "Eve Anderson",
            mailmapSubentryUpdatedAt: null,
          },
          {
            __typename: "MailmapRecord",
            mailmapEntryCreatedAt: null,
            mailmapEntryEmail: "mallory@entry.com",
            mailmapEntryName: "Mallory Santos",
            mailmapEntryUpdatedAt: null,
            mailmapSubentryCreatedAt: null,
            mailmapSubentryEmail: "mallory@subentry.com",
            mailmapSubentryName: "Mallory Santos",
            mailmapSubentryUpdatedAt: null,
          },
          {
            __typename: "MailmapRecord",
            mailmapEntryCreatedAt: null,
            mailmapEntryEmail: "trent@entry.com",
            mailmapEntryName: "Trent Hernandez",
            mailmapEntryUpdatedAt: null,
            mailmapSubentryCreatedAt: null,
            mailmapSubentryEmail: "trent@subentry.com",
            mailmapSubentryName: "Trent Hernandez",
            mailmapSubentryUpdatedAt: null,
          },
          {
            __typename: "MailmapRecord",
            mailmapEntryCreatedAt: null,
            mailmapEntryEmail: "grace@entry.com",
            mailmapEntryName: "Grace Musa",
            mailmapEntryUpdatedAt: null,
            mailmapSubentryCreatedAt: null,
            mailmapSubentryEmail: "grace@subentry.com",
            mailmapSubentryName: "Grace Musa",
            mailmapSubentryUpdatedAt: null,
          },
          {
            __typename: "MailmapRecord",
            mailmapEntryCreatedAt: null,
            mailmapEntryEmail: "oscar@entry.com",
            mailmapEntryName: "Oscar Schmidt",
            mailmapEntryUpdatedAt: null,
            mailmapSubentryCreatedAt: null,
            mailmapSubentryEmail: "oscar@subentry.com",
            mailmapSubentryName: "Oscar Schmidt",
            mailmapSubentryUpdatedAt: null,
          },
          {
            __typename: "MailmapRecord",
            mailmapEntryCreatedAt: null,
            mailmapEntryEmail: "wendy@entry.com",
            mailmapEntryName: "Wendy Ozcan",
            mailmapEntryUpdatedAt: null,
            mailmapSubentryCreatedAt: null,
            mailmapSubentryEmail: "wendy@subentry.com",
            mailmapSubentryName: "Wendy Ozcan",
            mailmapSubentryUpdatedAt: null,
          },
        ],
      },
    });
  },
);

const mailmapEntriesQueryError = graphqlMocked.query(
  GET_MAILMAP_RECORDS_QUERY,
  (): StrictResponse<IErrorMessage> => {
    return HttpResponse.json({
      errors: [new GraphQLError("Error getting mailmap records")],
    });
  },
);

const mailmapEntrySubentriesQuery = graphqlMocked.query(
  GET_MAILMAP_ENTRY_SUBENTRIES_QUERY,
  (): StrictResponse<{ data: MailmapEntrySubentriesQuery }> => {
    return HttpResponse.json({
      data: {
        mailmapEntrySubentries: [
          {
            __typename: "MailmapSubentry",
            mailmapSubentryCreatedAt: null,
            mailmapSubentryEmail: "alice@subentry.com",
            mailmapSubentryName: "Alice Garcia",
            mailmapSubentryUpdatedAt: null,
          },
          {
            __typename: "MailmapSubentry",
            mailmapSubentryCreatedAt: null,
            mailmapSubentryEmail: "alice-b@subentry.com",
            mailmapSubentryName: "Alice B Garcia",
            mailmapSubentryUpdatedAt: null,
          },
        ],
      },
    });
  },
);

const mailmapEntrySubentriesQueryError = graphqlMocked.query(
  GET_MAILMAP_ENTRY_SUBENTRIES_QUERY,
  (): StrictResponse<IErrorMessage> => {
    return HttpResponse.json({
      errors: [new GraphQLError("Error getting mailmap subentries")],
    });
  },
);

const mailmapEntrySubentriesQueryNotFoundError = graphqlMocked.query(
  GET_MAILMAP_ENTRY_SUBENTRIES_QUERY,
  (): StrictResponse<IErrorMessage> => {
    return HttpResponse.json({
      errors: [new GraphQLError("Exception - Mailmap entry not found:")],
    });
  },
);

const createMailmapEntryMutation = graphqlMocked.mutation(
  CREATE_MAILMAP_ENTRY_MUTATION,
  ({ variables }): StrictResponse<{ data: CreateMailmapEntryMutation }> => {
    const { entry } = variables;

    return HttpResponse.json({
      data: {
        createMailmapEntry: {
          entryWithSubentries: {
            entry: {
              mailmapEntryCreatedAt: null,
              mailmapEntryEmail: entry.mailmapEntryEmail,
              mailmapEntryName: entry.mailmapEntryName,
              mailmapEntryUpdatedAt: null,
            },
            subentries: [
              {
                mailmapSubentryCreatedAt: null,
                mailmapSubentryEmail: entry.mailmapEntryEmail,
                mailmapSubentryName: entry.mailmapEntryName,
                mailmapSubentryUpdatedAt: null,
              },
            ],
          },
          success: true,
        },
      },
    });
  },
);

const createMailmapEntryMutationError = graphqlMocked.mutation(
  CREATE_MAILMAP_ENTRY_MUTATION,
  (): StrictResponse<IErrorMessage> => {
    return HttpResponse.json({
      errors: [new GraphQLError("Error creating mailmap entry")],
    });
  },
);

const createMailmapEntryMutationAlreadyExistsError = graphqlMocked.mutation(
  CREATE_MAILMAP_ENTRY_MUTATION,
  (): StrictResponse<IErrorMessage> => {
    return HttpResponse.json({
      errors: [new GraphQLError("Exception - Mailmap entry already exists:")],
    });
  },
);

const createMailmapSubentryMutation = graphqlMocked.mutation(
  CREATE_MAILMAP_SUBENTRY_MUTATION,
  (): StrictResponse<{ data: CreateMailmapSubentryMutation }> => {
    return HttpResponse.json({
      data: {
        createMailmapSubentry: {
          success: true,
        },
      },
    });
  },
);

const createMailmapSubentryMutationError = graphqlMocked.mutation(
  CREATE_MAILMAP_SUBENTRY_MUTATION,
  (): StrictResponse<IErrorMessage> => {
    return HttpResponse.json({
      errors: [new GraphQLError("Error creating mailmap subentry")],
    });
  },
);

const createMailmapSubentryMutationNotFoundError = graphqlMocked.mutation(
  CREATE_MAILMAP_SUBENTRY_MUTATION,
  (): StrictResponse<IErrorMessage> => {
    return HttpResponse.json({
      errors: [new GraphQLError("Exception - Mailmap entry not found:")],
    });
  },
);

const createMailmapSubentryMutationAlreadyExistsError = graphqlMocked.mutation(
  CREATE_MAILMAP_SUBENTRY_MUTATION,
  (): StrictResponse<IErrorMessage> => {
    return HttpResponse.json({
      errors: [
        new GraphQLError("Exception - Mailmap subentry already exists:"),
      ],
    });
  },
);

const createMailmapSubentryMutationAlreadyExistsInOrganizationError =
  graphqlMocked.mutation(
    CREATE_MAILMAP_SUBENTRY_MUTATION,
    (): StrictResponse<IErrorMessage> => {
      return HttpResponse.json({
        errors: [
          new GraphQLError(
            "Exception - Mailmap subentry already exists in organization:",
          ),
        ],
      });
    },
  );

const deleteMailmapSubentryMutation = graphqlMocked.mutation(
  DELETE_MAILMAP_SUBENTRY_MUTATION,
  (): StrictResponse<{ data: DeleteMailmapSubentryMutation }> => {
    return HttpResponse.json({
      data: {
        deleteMailmapSubentry: {
          success: true,
        },
      },
    });
  },
);

const deleteMailmapSubentryMutationError = graphqlMocked.mutation(
  DELETE_MAILMAP_SUBENTRY_MUTATION,
  (): StrictResponse<IErrorMessage> => {
    return HttpResponse.json({
      errors: [new GraphQLError("Error deleting mailmap subentry")],
    });
  },
);

const deleteMailmapSubentryMutationEntryNotFoundError = graphqlMocked.mutation(
  DELETE_MAILMAP_SUBENTRY_MUTATION,
  (): StrictResponse<IErrorMessage> => {
    return HttpResponse.json({
      errors: [new GraphQLError("Exception - Mailmap entry not found:")],
    });
  },
);

const deleteMailmapSubentryMutationSubentryNotFoundError =
  graphqlMocked.mutation(
    DELETE_MAILMAP_SUBENTRY_MUTATION,
    (): StrictResponse<IErrorMessage> => {
      return HttpResponse.json({
        errors: [new GraphQLError("Exception - Mailmap subentry not found:")],
      });
    },
  );

const deleteMailmapEntryWithSubentriesMutation = graphqlMocked.mutation(
  DELETE_MAILMAP_ENTRY_WITH_SUBENTRIES_MUTATION,
  (): StrictResponse<{ data: DeleteMailmapEntryWithSubentriesMutation }> => {
    return HttpResponse.json({
      data: {
        deleteMailmapEntryWithSubentries: {
          success: true,
        },
      },
    });
  },
);

const deleteMailmapEntryWithSubentriesMutationError = graphqlMocked.mutation(
  DELETE_MAILMAP_ENTRY_WITH_SUBENTRIES_MUTATION,
  (): StrictResponse<IErrorMessage> => {
    return HttpResponse.json({
      errors: [new GraphQLError("Error updating mailmap entry")],
    });
  },
);

const deleteMailmapEntryWithSubentriesMutationNotFoundError =
  graphqlMocked.mutation(
    DELETE_MAILMAP_ENTRY_WITH_SUBENTRIES_MUTATION,
    (): StrictResponse<IErrorMessage> => {
      return HttpResponse.json({
        errors: [new GraphQLError("Exception - Mailmap entry not found:")],
      });
    },
  );

const updateMailmapEntryMutation = graphqlMocked.mutation(
  UPDATE_MAILMAP_ENTRY_MUTATION,
  (): StrictResponse<{ data: UpdateMailmapEntryMutation }> => {
    return HttpResponse.json({
      data: {
        updateMailmapEntry: {
          success: true,
        },
      },
    });
  },
);

const updateMailmapEntryMutationError = graphqlMocked.mutation(
  UPDATE_MAILMAP_ENTRY_MUTATION,
  (): StrictResponse<IErrorMessage> => {
    return HttpResponse.json({
      errors: [new GraphQLError("Error updating mailmap entry")],
    });
  },
);

const updateMailmapEntryMutationNotFoundError = graphqlMocked.mutation(
  UPDATE_MAILMAP_ENTRY_MUTATION,
  (): StrictResponse<IErrorMessage> => {
    return HttpResponse.json({
      errors: [new GraphQLError("Exception - Mailmap entry not found:")],
    });
  },
);

const setMailmapEntryAsMailmapSubentryMutation = graphqlMocked.mutation(
  SET_MAILMAP_ENTRY_AS_MAILMAP_SUBENTRY_MUTATION,
  (): StrictResponse<{ data: SetMailmapEntryAsMailmapSubentryMutation }> => {
    return HttpResponse.json({
      data: {
        setMailmapEntryAsMailmapSubentry: {
          success: true,
        },
      },
    });
  },
);

const setMailmapEntryAsMailmapSubentryMutationError = graphqlMocked.mutation(
  SET_MAILMAP_ENTRY_AS_MAILMAP_SUBENTRY_MUTATION,
  (): StrictResponse<IErrorMessage> => {
    return HttpResponse.json({
      errors: [
        new GraphQLError("Error setting mailmap entry as mailmap subentry"),
      ],
    });
  },
);

const setMailmapEntryAsMailmapSubentryMutationNotFoundError =
  graphqlMocked.mutation(
    SET_MAILMAP_ENTRY_AS_MAILMAP_SUBENTRY_MUTATION,
    (): StrictResponse<IErrorMessage> => {
      return HttpResponse.json({
        errors: [new GraphQLError("Exception - Mailmap entry not found:")],
      });
    },
  );

const updateMailmapSubentryMutation = graphqlMocked.mutation(
  UPDATE_MAILMAP_SUBENTRY_MUTATION,
  (): StrictResponse<{ data: UpdateMailmapSubentryMutation }> => {
    return HttpResponse.json({
      data: {
        updateMailmapSubentry: {
          success: true,
        },
      },
    });
  },
);

const updateMailmapSubentryMutationError = graphqlMocked.mutation(
  UPDATE_MAILMAP_SUBENTRY_MUTATION,
  (): StrictResponse<IErrorMessage> => {
    return HttpResponse.json({
      errors: [new GraphQLError("Error updating mailmap subentry")],
    });
  },
);

const updateMailmapSubentryMutationEntryNotFoundError = graphqlMocked.mutation(
  UPDATE_MAILMAP_SUBENTRY_MUTATION,
  (): StrictResponse<IErrorMessage> => {
    return HttpResponse.json({
      errors: [new GraphQLError("Exception - Mailmap entry not found:")],
    });
  },
);

const updateMailmapSubentryMutationSubentryNotFoundError =
  graphqlMocked.mutation(
    UPDATE_MAILMAP_SUBENTRY_MUTATION,
    (): StrictResponse<IErrorMessage> => {
      return HttpResponse.json({
        errors: [new GraphQLError("Exception - Mailmap subentry not found:")],
      });
    },
  );

const setMailmapSubentryAsMailmapEntryMutation = graphqlMocked.mutation(
  SET_MAILMAP_SUBENTRY_AS_MAILMAP_ENTRY_MUTATION,
  ({
    variables,
  }): StrictResponse<{ data: SetMailmapSubentryAsMailmapEntryMutation }> => {
    const { subentryEmail } = variables;

    return HttpResponse.json({
      data: {
        setMailmapSubentryAsMailmapEntry: {
          entry: {
            mailmapEntryCreatedAt: null,
            mailmapEntryEmail: subentryEmail,
            mailmapEntryName: "",
            mailmapEntryUpdatedAt: null,
          },
          success: true,
        },
      },
    });
  },
);

const setMailmapSubentryAsMailmapEntryMutationError = graphqlMocked.mutation(
  SET_MAILMAP_SUBENTRY_AS_MAILMAP_ENTRY_MUTATION,
  (): StrictResponse<IErrorMessage> => {
    return HttpResponse.json({
      errors: [
        new GraphQLError("Error setting mailmap subentry as mailmap entry"),
      ],
    });
  },
);

const setMailmapSubentryAsMailmapEntryMutationEntryNotFoundError =
  graphqlMocked.mutation(
    SET_MAILMAP_SUBENTRY_AS_MAILMAP_ENTRY_MUTATION,
    (): StrictResponse<IErrorMessage> => {
      return HttpResponse.json({
        errors: [new GraphQLError("Exception - Mailmap entry not found:")],
      });
    },
  );

const setMailmapSubentryAsMailmapEntryMutationSubentryNotFoundError =
  graphqlMocked.mutation(
    SET_MAILMAP_SUBENTRY_AS_MAILMAP_ENTRY_MUTATION,
    (): StrictResponse<IErrorMessage> => {
      return HttpResponse.json({
        errors: [new GraphQLError("Exception - Mailmap subentry not found:")],
      });
    },
  );

const moveMailmapEntryWithSubentriesMutation = graphqlMocked.mutation(
  MOVE_MAILMAP_ENTRIES_WITH_SUBENTRIES_MUTATION,
  (): StrictResponse<{ data: MoveMailmapEntryWithSubentriesMutation }> => {
    return HttpResponse.json({
      data: {
        moveMailmapEntryWithSubentries: {
          success: true,
        },
      },
    });
  },
);

const convertMailmapSubentriesToNewEntryMutation = graphqlMocked.mutation(
  CONVERT_MAILMAP_SUBENTRIES_TO_NEW_ENTRY_MUTATION,
  (): StrictResponse<{ data: ConvertMailmapSubentriesToNewEntryMutation }> => {
    return HttpResponse.json({
      data: {
        convertMailmapSubentriesToNewEntry: {
          success: true,
        },
      },
    });
  },
);

const importMailmapMutation = graphqlMocked.mutation(
  IMPORT_MAILMAP_MUTATION,
  (): StrictResponse<{ data: ImportMailmapMutation }> => {
    return HttpResponse.json({
      data: {
        importMailmap: {
          __typename: "ImportMailmapPayload",
          exceptionsMessages: [],
          success: true,
          successCount: 5,
        },
      },
    });
  },
);

const importExistentMailmapMutation = graphqlMocked.mutation(
  IMPORT_MAILMAP_MUTATION,
  (): StrictResponse<{ data: ImportMailmapMutation }> => {
    return HttpResponse.json({
      data: {
        importMailmap: {
          __typename: "ImportMailmapPayload",
          exceptionsMessages: ["TestException"],
          success: true,
          successCount: 0,
        },
      },
    });
  },
);

const importMailmapMutationError = graphqlMocked.mutation(
  IMPORT_MAILMAP_MUTATION,
  (): StrictResponse<IErrorMessage> => {
    return HttpResponse.json({
      errors: [new GraphQLError("Error importing mailmap file")],
    });
  },
);

const importMailmapMutationConflictError = graphqlMocked.mutation(
  IMPORT_MAILMAP_MUTATION,
  (): StrictResponse<IErrorMessage> => {
    return HttpResponse.json({
      errors: [
        new GraphQLError(
          "Exception - Conflicting mappings detected in mailmap file: [conflict1, conflict2]",
        ),
      ],
    });
  },
);

const mockedPermissions = new PureAbility<string>([
  { action: "integrates_api_mutations_create_mailmap_entry_mutate" },
  { action: "integrates_api_mutations_create_mailmap_subentry_mutate" },
]);

describe("mailmap", (): void => {
  const { t } = useTranslation();

  function renderMailmap(mocks: GraphQLHandler[]): void {
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={
              <Mailmap
                organizationId={"ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"}
              />
            }
            path={"/mailmap"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/mailmap"],
        },
        mocks,
      },
    );
  }

  async function createEntry(
    entryEmail: string,
    entryName: string,
  ): Promise<void> {
    // "Create entry" text
    const createEntryText = t("mailmap.createEntry");
    // "Create mailmap entry" text
    const createMailmapEntryText = t("mailmap.createMailmapEntry");
    // Check if "Create entry" button is in the document
    await waitFor((): void => {
      expect(screen.queryByText(createEntryText)).toBeInTheDocument();
    });
    // Click "Create entry" button to open modal
    await userEvent.click(screen.getByText(createEntryText));
    // Check if modal is open by checking "Create mailmap entry" text
    await waitFor((): void => {
      expect(screen.queryByText(createMailmapEntryText)).toBeInTheDocument();
    });
    // Type entry email
    await userEvent.type(
      screen.getByRole("textbox", { name: "entryEmail" }),
      entryEmail,
    );
    // Type entry name
    await userEvent.type(
      screen.getByRole("textbox", { name: "entryName" }),
      entryName,
    );
    // Click "Create entry" button inside modal
    await userEvent.click(screen.getAllByText(createEntryText)[1]);
  }

  async function deleteEntry(idx: number): Promise<void> {
    // "Entry config" text
    const entryConfigText = t("mailmap.entryConfig");
    // "Delete entry" text
    const deleteEntryText = t("mailmap.deleteEntry");
    // "Delete mailmap entry" text
    const deleteMailmapEntryText = t("mailmap.deleteMailmapEntry");
    // Check if "Entry config" button is in the document
    await waitFor((): void => {
      expect(screen.queryByText(entryConfigText)).toBeInTheDocument();
    });

    // Check if mailmap table is in document
    await waitFor((): void => {
      expect(screen.queryByRole("table")).toBeInTheDocument();
    });
    // Check if checkbox in position `idx` is in document
    await waitFor((): void => {
      expect(screen.getAllByRole("checkbox")[idx]).toBeInTheDocument();
    });
    // Click checkbox in position `idx`
    await userEvent.click(screen.getAllByRole("checkbox")[idx]);
    // Check if "Entry config" button is enabled
    await waitFor((): void => {
      expect(screen.queryByText(entryConfigText)).toBeEnabled();
    });
    // Click "Entry config" button to open dropdown
    await userEvent.click(screen.getByText(entryConfigText));
    // Click "Delete entry" button to open modal
    await userEvent.click(screen.getByText(deleteEntryText));
    // Check if modal is open by checking "Delete mailmap entry" text
    await waitFor((): void => {
      expect(screen.queryByText(deleteMailmapEntryText)).toBeInTheDocument();
    });
    // Click "Delete entry" button inside modal
    await userEvent.click(screen.getByText(deleteEntryText));
  }

  async function updateEntry(
    idx: number,
    newEntryEmail: string,
    newEntryName: string,
  ): Promise<void> {
    // "Entry config" text
    const entryConfigText = t("mailmap.entryConfig");
    // "Update entry" text
    const updateEntryText = t("mailmap.updateEntry");
    // "Update mailmap entry" text
    const updateMailmapEntryText = t("mailmap.updateMailmapEntry");
    // Check if "Entry config" button is in the document
    await waitFor((): void => {
      expect(screen.queryByText(entryConfigText)).toBeInTheDocument();
    });
    // Check if mailmap table is in document
    await waitFor((): void => {
      expect(screen.queryByRole("table")).toBeInTheDocument();
    });
    // Check if checkbox in position `idx` is in document
    await waitFor((): void => {
      expect(screen.getAllByRole("checkbox")[idx]).toBeInTheDocument();
    });
    // Click checkbox in position `idx`
    await userEvent.click(screen.getAllByRole("checkbox")[idx]);
    // Check if "Entry config" button is enabled
    await waitFor((): void => {
      expect(screen.queryByText(entryConfigText)).toBeEnabled();
    });
    // Click "Entry config" button to open dropdown
    await userEvent.click(screen.getByText(entryConfigText));
    // Click "Update entry" button to open modal
    await userEvent.click(screen.getByText(updateEntryText));
    // Check if modal is open by checking "Update mailmap entry" text
    await waitFor((): void => {
      expect(screen.queryByText(updateMailmapEntryText)).toBeInTheDocument();
    });
    // Clear entry email
    await userEvent.clear(
      screen.getByRole("textbox", { name: "newEntryEmail" }),
    );
    // Clear entry name
    await userEvent.clear(
      screen.getByRole("textbox", { name: "newEntryName" }),
    );
    // Type new entry email
    await userEvent.type(
      screen.getByRole("textbox", { name: "newEntryEmail" }),
      newEntryEmail,
    );
    // Type new entry name
    await userEvent.type(
      screen.getByRole("textbox", { name: "newEntryName" }),
      newEntryName,
    );
    // Click "Update entry" button inside modal
    await userEvent.click(screen.getByText(updateEntryText));
  }

  async function setEntryAsSubentry(
    idx: number,
    targetEntryEmail: string,
  ): Promise<void> {
    // "Entry config" text
    const entryConfigText = t("mailmap.entryConfig");
    // "Set entry as subentry" text
    const setEntryAsSubentryText = t("mailmap.setEntryAsSubentry");
    // "Set mailmap entry as mailmap subentry" text
    const setMailmapEntryAsMailmapSubentryText = t(
      "mailmap.setMailmapEntryAsMailmapSubentry",
    );
    // Check if "Entry config" button is in the document
    await waitFor((): void => {
      expect(screen.queryByText(entryConfigText)).toBeInTheDocument();
    });
    // Check if mailmap table is in document
    await waitFor((): void => {
      expect(screen.queryByRole("table")).toBeInTheDocument();
    });
    // Check if checkbox in position `idx` is in document
    await waitFor((): void => {
      expect(screen.getAllByRole("checkbox")[idx]).toBeInTheDocument();
    });
    // Click checkbox in position `idx`
    await userEvent.click(screen.getAllByRole("checkbox")[idx]);
    // Check if "Entry config" button is in the document
    await waitFor((): void => {
      expect(screen.queryByText(entryConfigText)).toBeInTheDocument();
    });
    // Click "Entry config" button to open dropdown
    await userEvent.click(screen.getByText(entryConfigText));
    // Click "Set entry as subentry" button to open modal
    await userEvent.click(screen.getByText(setEntryAsSubentryText));
    // Check if modal is open by checking text
    await waitFor((): void => {
      expect(
        screen.queryByText(setMailmapEntryAsMailmapSubentryText),
      ).toBeInTheDocument();
    });
    // Clear target entry email
    await userEvent.clear(
      screen.getByRole("textbox", { name: "targetEntryEmail" }),
    );
    // Type target entry email
    await userEvent.type(
      screen.getByRole("textbox", { name: "targetEntryEmail" }),
      targetEntryEmail,
    );
    // Click "Set entry as subentry" button inside modal
    await userEvent.click(screen.getByText(setEntryAsSubentryText));
  }

  async function expandRow(idx: number): Promise<void> {
    // Check if mailmap table is in document
    await waitFor((): void => {
      expect(screen.queryByRole("table")).toBeInTheDocument();
    });
    // Get table element
    const table = screen.getByRole("table");
    // Check if `fa-angle-down` icon in position `idx` is in document
    await waitFor((): void => {
      expect(
        table.getElementsByClassName("fa-angle-down")[idx],
      ).toBeInTheDocument();
    });
    // Click `fa-angle-down` icon in position `idx` to expand row
    await userEvent.click(table.getElementsByClassName("fa-angle-down")[idx]);
  }

  async function createSubentry(
    idx: number,
    subentryEmail: string,
    subentryName: string,
  ): Promise<void> {
    // Expand row at position `idx`
    await expandRow(idx);
    // "Create subentry" text
    const createSubentryText = t("mailmap.createSubentry");
    // "Create mailmap subentry" text
    const createMailmapSubentryText = t("mailmap.createMailmapSubentry");
    // Check if "Create subentry" button is in document
    await waitFor((): void => {
      expect(screen.queryByText(createSubentryText)).toBeInTheDocument();
    });
    // Click "Create subentry" button to open modal
    await userEvent.click(screen.getByText(createSubentryText));
    // Check if modal is open by checking "Create mailmap subentry" text
    await waitFor((): void => {
      expect(screen.queryByText(createMailmapSubentryText)).toBeInTheDocument();
    });
    // Type new subentry email
    await userEvent.type(
      screen.getByRole("textbox", { name: "subentryEmail" }),
      subentryEmail,
    );
    // Type new subentry name
    await userEvent.type(
      screen.getByRole("textbox", { name: "subentryName" }),
      subentryName,
    );
    // Click "Create subentry" button inside modal
    await userEvent.click(screen.getAllByText(createSubentryText)[1]);
  }

  async function deleteSubentry(
    entryIndex: number,
    subentryIndex: number,
  ): Promise<void> {
    // Expand row at position `entryIndex`
    await expandRow(entryIndex);
    // "Subentry config" text
    const subentryConfigText = t("mailmap.subentryConfig");
    // "Delete subentry" text
    const deleteSubentryText = t("mailmap.deleteSubentry");
    // "Delete mailmap subentry" text
    const deleteMailmapSubentryText = t("mailmap.deleteMailmapSubentry");
    // Check if "Subentry config" button is in document
    await waitFor((): void => {
      expect(screen.queryByText(subentryConfigText)).toBeInTheDocument();
    });
    // Check if subentries table is in document
    await waitFor((): void => {
      expect(screen.getAllByRole("table")[1]).toBeInTheDocument();
    });
    // Check if checkbox in position `subentryIndex` is in document
    await waitFor((): void => {
      expect(
        screen.getAllByRole("checkbox")[subentryIndex],
      ).toBeInTheDocument();
    });
    // Click checkbox in position `subentryIndex`
    await userEvent.click(screen.getAllByRole("checkbox")[subentryIndex]);
    // Check if "Subentry config" button is enabled
    await waitFor((): void => {
      expect(screen.queryByText(subentryConfigText)).toBeEnabled();
    });
    // Click "Subentry config" button to open dropdown
    await userEvent.click(screen.getByText(subentryConfigText));
    // Click "Delete subentry" button to open modal
    await userEvent.click(screen.getByText(deleteSubentryText));
    // Check if modal is open by checking "Delete mailmap subentry" text
    await waitFor((): void => {
      expect(screen.queryByText(deleteMailmapSubentryText)).toBeInTheDocument();
    });
    // Click "Delete subentry" button inside modal
    await userEvent.click(screen.getByText(deleteSubentryText));
  }

  async function updateSubentry(
    entryIndex: number,
    subentryIndex: number,
    newSubentryEmail: string,
    newSubentryName: string,
  ): Promise<void> {
    // Expand row at position `entryIndex`
    await expandRow(entryIndex);
    // "Subentry config" text
    const subentryConfigText = t("mailmap.subentryConfig");
    // "Update subentry" text
    const updateSubentryText = t("mailmap.updateSubentry");
    // "Update mailmap subentry" text
    const updateMailmapSubentryText = t("mailmap.updateMailmapSubentry");
    // Check if "Subentry config" button is in document
    await waitFor((): void => {
      expect(screen.queryByText(subentryConfigText)).toBeInTheDocument();
    });
    // Check if subentries table is in document
    await waitFor((): void => {
      expect(screen.getAllByRole("table")[1]).toBeInTheDocument();
    });
    // Check if checkbox in position `subentryIndex` is in document
    await waitFor((): void => {
      expect(
        screen.getAllByRole("checkbox")[subentryIndex],
      ).toBeInTheDocument();
    });
    // Click checkbox in position `subentryIndex`
    await userEvent.click(screen.getAllByRole("checkbox")[subentryIndex]);
    // Check if "Subentry config" button is enabled
    await waitFor((): void => {
      expect(screen.queryByText(subentryConfigText)).toBeEnabled();
    });
    // Click "Subentry config" button to open dropdown
    await userEvent.click(screen.getByText(subentryConfigText));
    // Click "Update subentry" button to open modal
    await userEvent.click(screen.getByText(updateSubentryText));
    // Check if modal is open by checking "Update mailmap subentry" text
    await waitFor((): void => {
      expect(screen.queryByText(updateMailmapSubentryText)).toBeInTheDocument();
    });
    // Clear subentry email
    await userEvent.clear(
      screen.getByRole("textbox", { name: "newSubentryEmail" }),
    );
    // Clear subentry name
    await userEvent.clear(
      screen.getByRole("textbox", { name: "newSubentryName" }),
    );
    // Type new subentry email
    await userEvent.type(
      screen.getByRole("textbox", { name: "newSubentryEmail" }),
      newSubentryEmail,
    );
    // Type new subentry name
    await userEvent.type(
      screen.getByRole("textbox", { name: "newSubentryName" }),
      newSubentryName,
    );
    // Click "Update subentry" button inside modal
    await userEvent.click(screen.getByText(updateSubentryText));
  }

  async function setSubentryAsEntry(
    entryIndex: number,
    subentryIndex: number,
  ): Promise<void> {
    // Expand row at position `entryIndex`
    await expandRow(entryIndex);
    // "Subentry config" text
    const subentryConfigText = t("mailmap.subentryConfig");
    // "Set subentry as entry" text
    const setSubentryAsEntryText = t("mailmap.setSubentryAsEntry");
    // "Set mailmap subentry as mailmap entry" text
    const setMailmapSubentryAsMailmapEntryText = t(
      "mailmap.setMailmapSubentryAsMailmapEntry",
    );
    // Check if "Subentry config" button is in document
    await waitFor((): void => {
      expect(screen.queryByText(subentryConfigText)).toBeInTheDocument();
    });
    // Check if subentries table is in document
    await waitFor((): void => {
      expect(screen.getAllByRole("table")[1]).toBeInTheDocument();
    });
    // Check if checkbox in position `subentryIndex` is in document
    await waitFor((): void => {
      expect(
        screen.getAllByRole("checkbox")[subentryIndex],
      ).toBeInTheDocument();
    });
    // Click checkbox in position `subentryIndex`
    await userEvent.click(screen.getAllByRole("checkbox")[subentryIndex]);
    // Check if "Subentry config" button is enabled
    await waitFor((): void => {
      expect(screen.queryByText(subentryConfigText)).toBeEnabled();
    });
    // Click "Subentry config" button to open dropdown
    await userEvent.click(screen.getByText(subentryConfigText));
    // Click "Set subentry as entry" button to open modal
    await userEvent.click(screen.getByText(setSubentryAsEntryText));
    // Check if modal is open by checking text
    await waitFor((): void => {
      expect(
        screen.queryByText(setMailmapSubentryAsMailmapEntryText),
      ).toBeInTheDocument();
    });
    // Click "Set subentry as entry" button inside modal
    await userEvent.click(screen.getByText(setSubentryAsEntryText));
  }

  async function moveEntryWithSubentries(
    idx: number,
    newOrganizationId: string,
  ): Promise<void> {
    // "Entry config" text
    const entryConfigText = t("mailmap.entryConfig");
    // "Move entry with subentries" text
    const moveEntryWithSubentriesText = t("mailmap.moveEntryWithSubentries");
    // "Move mailmap entry with subentries" text
    const moveMailmapEntryWithSubentriesText = t(
      "mailmap.moveMailmapEntryWithSubentries",
    );
    // Check if "Entry config" button is in the document
    await waitFor((): void => {
      expect(screen.queryByText(entryConfigText)).toBeInTheDocument();
    });
    // Check if mailmap table is in document
    await waitFor((): void => {
      expect(screen.queryByRole("table")).toBeInTheDocument();
    });
    // Check if checkbox in position `idx` is in document
    await waitFor((): void => {
      expect(screen.getAllByRole("checkbox")[idx]).toBeInTheDocument();
    });
    // Click checkbox in position `idx`
    await userEvent.click(screen.getAllByRole("checkbox")[idx]);
    // Check if "Entry config" button is in the document
    await waitFor((): void => {
      expect(screen.queryByText(entryConfigText)).toBeInTheDocument();
    });
    // Click "Entry config" button to open dropdown
    await userEvent.click(screen.getByText(entryConfigText));
    // Click "Move entry with subentries" button to open modal
    await userEvent.click(screen.getByText(moveEntryWithSubentriesText));
    // Check if modal is open by checking text
    await waitFor((): void => {
      expect(
        screen.queryByText(moveMailmapEntryWithSubentriesText),
      ).toBeInTheDocument();
    });
    // Clear new organization id field
    await userEvent.clear(
      screen.getByRole("textbox", { name: "newOrganizationId" }),
    );
    // Type new organization id
    await userEvent.type(
      screen.getByRole("textbox", { name: "newOrganizationId" }),
      newOrganizationId,
    );
    // Click "Move entry with subentries" button inside modal
    await userEvent.click(screen.getByText(moveEntryWithSubentriesText));
  }

  async function convertSubentriesToNewEntry(
    entryIndex: number,
    subentryIndex: number,
  ): Promise<void> {
    // Expand row at position `entryIndex`
    await expandRow(entryIndex);
    // "Subentry config" text
    const subentryConfigText = t("mailmap.subentryConfig");
    // "Convert subentry to new entry" text
    const convertSubentryToNewEntryText = t(
      "mailmap.convertSubentryToNewEntry",
    );
    // "Convert subentries to new entry" text
    const convertSubentriesToNewEntryText = t(
      "mailmap.convertSubentriesToNewEntry",
    );
    // "Convert mailmap subentries to new entry" text
    const convertMailmapSubentriesToNewEntryText = t(
      "mailmap.convertMailmapSubentriesToNewEntry",
    );
    // Check if "Subentry config" button is in document
    await waitFor((): void => {
      expect(screen.queryByText(subentryConfigText)).toBeInTheDocument();
    });
    // Check if subentries table is in document
    await waitFor((): void => {
      expect(screen.getAllByRole("table")[1]).toBeInTheDocument();
    });
    // Check if checkbox in position `subentryIndex` is in document
    await waitFor((): void => {
      expect(
        screen.getAllByRole("checkbox")[subentryIndex],
      ).toBeInTheDocument();
    });
    // Click checkbox in position `subentryIndex`
    await userEvent.click(screen.getAllByRole("checkbox")[subentryIndex]);
    // Check if "Subentry config" button is enabled
    await waitFor((): void => {
      expect(screen.queryByText(subentryConfigText)).toBeEnabled();
    });
    // Click "Subentry config" button to open dropdown
    await userEvent.click(screen.getByText(subentryConfigText));
    // Click "Convert subentry to new entry" button to open modal
    await userEvent.click(screen.getByText(convertSubentryToNewEntryText));
    // Check if modal is open by checking text
    await waitFor((): void => {
      expect(
        screen.queryByText(convertMailmapSubentriesToNewEntryText),
      ).toBeInTheDocument();
    });
    // Click "Convert subentries to new entry" button inside modal
    await userEvent.click(screen.getByText(convertSubentriesToNewEntryText));
  }

  it("should render formatted mailmap emails", async (): Promise<void> => {
    expect.hasAssertions();

    const mocks = [mailmapEntriesQuery];
    renderMailmap(mocks);
    jest.spyOn(console, "warn").mockImplementation();

    await waitFor((): void => {
      expect(formatMailmapEntryEmail).toHaveBeenCalledWith({
        domainsCheck: true,
        entryEmail: "alice@entry.com",
      });
    });

    await waitFor((): void => {
      expect(formatMailmapEntryEmail).toHaveBeenCalledWith({
        domainsCheck: false,
        entryEmail: "bob@entry.com",
      });
    });

    jest.clearAllMocks();
  });

  it("should create mailmap entry", async (): Promise<void> => {
    expect.hasAssertions();

    const mocks = [createMailmapEntryMutation];
    renderMailmap(mocks);
    jest.spyOn(console, "warn").mockImplementation();
    await createEntry("john@entry.com", "John Doe");
    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledTimes(1);
    });
    jest.clearAllMocks();
  });

  it("should handle create mailmap entry errors", async (): Promise<void> => {
    expect.hasAssertions();

    const mocks = [createMailmapEntryMutationError];
    renderMailmap(mocks);
    jest.spyOn(console, "warn").mockImplementation();
    await createEntry("john@entry.com", "John Doe");
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        t("mailmap.errorCreateMailmapEntry"),
        t("mailmap.errorTitle"),
      );
    });
    jest.clearAllMocks();
  });

  it("should handle create mailmap entry already exists error", async (): Promise<void> => {
    expect.hasAssertions();

    const mocks = [createMailmapEntryMutationAlreadyExistsError];
    renderMailmap(mocks);
    jest.spyOn(console, "warn").mockImplementation();
    await createEntry("john@entry.com", "John Doe [NEW]");
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        t("mailmap.errorMailmapEntryAlreadyExists"),
        t("mailmap.errorTitle"),
      );
    });
    jest.clearAllMocks();
  });

  it("should create mailmap subentry", async (): Promise<void> => {
    expect.hasAssertions();

    const mocks = [mailmapEntriesQuery, createMailmapSubentryMutation];
    renderMailmap(mocks);
    jest.spyOn(console, "warn").mockImplementation();
    await createSubentry(1, "john@subentry.com", "John Doe");
    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledTimes(1);
    });
    jest.clearAllMocks();
  });

  it("should handle create mailmap subentry errors", async (): Promise<void> => {
    expect.hasAssertions();

    const mocks = [mailmapEntriesQuery, createMailmapSubentryMutationError];
    renderMailmap(mocks);
    jest.spyOn(console, "warn").mockImplementation();
    await createSubentry(1, "john@subentry.com", "John Doe");
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        t("mailmap.errorCreateMailmapSubentry"),
        t("mailmap.errorTitle"),
      );
    });
    jest.clearAllMocks();
  });

  it("should handle create mailmap subentry not found error", async (): Promise<void> => {
    expect.hasAssertions();

    const mocks = [
      mailmapEntriesQuery,
      createMailmapSubentryMutationNotFoundError,
    ];
    renderMailmap(mocks);
    jest.spyOn(console, "warn").mockImplementation();
    await createSubentry(1, "john@subentry.com", "John Doe");
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        t("mailmap.errorMailmapEntryNotFound"),
        t("mailmap.errorTitle"),
      );
    });
    jest.clearAllMocks();
  });

  it("should handle create mailmap subentry already exists error", async (): Promise<void> => {
    expect.hasAssertions();

    const mocks = [
      mailmapEntriesQuery,
      createMailmapSubentryMutationAlreadyExistsError,
    ];
    renderMailmap(mocks);
    jest.spyOn(console, "warn").mockImplementation();
    await createSubentry(1, "john@subentry.com", "John Doe [NEW]");
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        t("mailmap.errorMailmapSubentryAlreadyExists"),
        t("mailmap.errorTitle"),
      );
    });
    jest.clearAllMocks();
  });

  it("should handle create mailmap subentry already exists in organization error", async (): Promise<void> => {
    expect.hasAssertions();

    const mocks = [
      mailmapEntriesQuery,
      createMailmapSubentryMutationAlreadyExistsInOrganizationError,
    ];
    renderMailmap(mocks);
    jest.spyOn(console, "warn").mockImplementation();
    await createSubentry(1, "wendy@subentry.com", "Wendy Ozcan");
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        t("mailmap.errorMailmapSubentryAlreadyExistsInOrganization"),
        t("mailmap.errorTitle"),
      );
    });
    jest.clearAllMocks();
  });

  it("should delete mailmap subentry", async (): Promise<void> => {
    expect.hasAssertions();

    const mocks = [
      mailmapEntriesQuery,
      deleteMailmapSubentryMutation,
      mailmapEntrySubentriesQuery,
    ];
    renderMailmap(mocks);
    jest.spyOn(console, "warn").mockImplementation();
    await deleteSubentry(1, 3);
    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledTimes(1);
    });
    jest.clearAllMocks();
  });

  it("should handle delete mailmap subentry errors", async (): Promise<void> => {
    expect.hasAssertions();

    const mocks = [
      mailmapEntriesQuery,
      deleteMailmapSubentryMutationError,
      mailmapEntrySubentriesQuery,
    ];
    renderMailmap(mocks);
    jest.spyOn(console, "warn").mockImplementation();
    await deleteSubentry(1, 3);
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        t("mailmap.errorDeleteMailmapSubentry"),
        t("mailmap.errorTitle"),
      );
    });
    jest.clearAllMocks();
  });

  it("should handle delete mailmap subentry entry not found error", async (): Promise<void> => {
    expect.hasAssertions();

    const mocks = [
      mailmapEntriesQuery,
      deleteMailmapSubentryMutationEntryNotFoundError,
      mailmapEntrySubentriesQuery,
    ];
    renderMailmap(mocks);
    jest.spyOn(console, "warn").mockImplementation();
    await deleteSubentry(1, 3);
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        t("mailmap.errorMailmapEntryNotFound"),
        t("mailmap.errorTitle"),
      );
    });
    jest.clearAllMocks();
  });

  it("should handle delete mailmap subentry not found error", async (): Promise<void> => {
    expect.hasAssertions();

    const mocks = [
      mailmapEntriesQuery,
      deleteMailmapSubentryMutationSubentryNotFoundError,
      mailmapEntrySubentriesQuery,
    ];
    renderMailmap(mocks);
    jest.spyOn(console, "warn").mockImplementation();
    await deleteSubentry(1, 3);
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        t("mailmap.errorMailmapSubentryNotFound"),
        t("mailmap.errorTitle"),
      );
    });
    jest.clearAllMocks();
  });

  it("should update mailmap subentry", async (): Promise<void> => {
    expect.hasAssertions();

    const mocks = [
      mailmapEntriesQuery,
      updateMailmapSubentryMutation,
      mailmapEntrySubentriesQuery,
    ];
    renderMailmap(mocks);
    jest.spyOn(console, "warn").mockImplementation();
    await updateSubentry(1, 3, "grace-new@subentry.com", "Grace Musa [NEW]");
    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledTimes(1);
    });
    jest.clearAllMocks();
  });

  it("should handle update mailmap subentry errors", async (): Promise<void> => {
    expect.hasAssertions();

    const mocks = [
      mailmapEntriesQuery,
      updateMailmapSubentryMutationError,
      mailmapEntrySubentriesQuery,
    ];
    renderMailmap(mocks);
    jest.spyOn(console, "warn").mockImplementation();
    await updateSubentry(1, 3, "grace-new@subentry.com", "Grace Musa [NEW]");
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        t("mailmap.errorUpdateMailmapSubentry"),
        t("mailmap.errorTitle"),
      );
    });
    jest.clearAllMocks();
  });

  it("should handle update mailmap subentry entry not found error", async (): Promise<void> => {
    expect.hasAssertions();

    const mocks = [
      mailmapEntriesQuery,
      updateMailmapSubentryMutationEntryNotFoundError,
      mailmapEntrySubentriesQuery,
    ];
    renderMailmap(mocks);
    jest.spyOn(console, "warn").mockImplementation();
    await updateSubentry(1, 3, "grace-new@subentry.com", "Grace Musa [NEW]");
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        t("mailmap.errorMailmapEntryNotFound"),
        t("mailmap.errorTitle"),
      );
    });
    jest.clearAllMocks();
  });

  it("should handle update mailmap subentry not found error", async (): Promise<void> => {
    expect.hasAssertions();

    const mocks = [
      mailmapEntriesQuery,
      updateMailmapSubentryMutationSubentryNotFoundError,
      mailmapEntrySubentriesQuery,
    ];
    renderMailmap(mocks);
    jest.spyOn(console, "warn").mockImplementation();
    await updateSubentry(1, 3, "grace-new@subentry.com", "Grace Musa [NEW]");
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        t("mailmap.errorMailmapSubentryNotFound"),
        t("mailmap.errorTitle"),
      );
    });
    jest.clearAllMocks();
  });

  it("should set mailmap subentry as mailmap entry", async (): Promise<void> => {
    expect.hasAssertions();

    const mocks = [
      mailmapEntriesQuery,
      setMailmapSubentryAsMailmapEntryMutation,
      mailmapEntrySubentriesQuery,
    ];
    renderMailmap(mocks);
    jest.spyOn(console, "warn").mockImplementation();
    await setSubentryAsEntry(1, 3);
    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledTimes(1);
    });
    jest.clearAllMocks();
  });

  it("should handle set mailmap subentry as mailmap entry errors", async (): Promise<void> => {
    expect.hasAssertions();

    const mocks = [
      mailmapEntriesQuery,
      setMailmapSubentryAsMailmapEntryMutationError,
      mailmapEntrySubentriesQuery,
    ];
    renderMailmap(mocks);
    jest.spyOn(console, "warn").mockImplementation();
    await setSubentryAsEntry(1, 3);
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        t("mailmap.errorSetMailmapSubentryAsMailmapEntry"),
        t("mailmap.errorTitle"),
      );
    });
    jest.clearAllMocks();
  });

  it("should handle set mailmap subentry as mailmap entry not found error 1", async (): Promise<void> => {
    expect.hasAssertions();

    const mocks = [
      mailmapEntriesQuery,
      setMailmapSubentryAsMailmapEntryMutationEntryNotFoundError,
      mailmapEntrySubentriesQuery,
    ];
    renderMailmap(mocks);
    jest.spyOn(console, "warn").mockImplementation();
    await setSubentryAsEntry(1, 3);
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        t("mailmap.errorMailmapEntryNotFound"),
        t("mailmap.errorTitle"),
      );
    });
    jest.clearAllMocks();
  });

  it("should handle set mailmap subentry as mailmap entry not found error 2", async (): Promise<void> => {
    expect.hasAssertions();

    const mocks = [
      mailmapEntriesQuery,
      setMailmapSubentryAsMailmapEntryMutationSubentryNotFoundError,
      mailmapEntrySubentriesQuery,
    ];
    renderMailmap(mocks);
    jest.spyOn(console, "warn").mockImplementation();
    await setSubentryAsEntry(1, 3);
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        t("mailmap.errorMailmapSubentryNotFound"),
        t("mailmap.errorTitle"),
      );
    });
    jest.clearAllMocks();
  });

  it("should delete mailmap entry", async (): Promise<void> => {
    expect.hasAssertions();

    const mocks = [
      mailmapEntriesQuery,
      deleteMailmapEntryWithSubentriesMutation,
    ];
    renderMailmap(mocks);
    jest.spyOn(console, "warn").mockImplementation();
    await deleteEntry(1);
    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledTimes(1);
    });
    jest.clearAllMocks();
  });

  it("should handle delete mailmap entry errors", async (): Promise<void> => {
    expect.hasAssertions();

    const mocks = [
      mailmapEntriesQuery,
      deleteMailmapEntryWithSubentriesMutationError,
    ];
    renderMailmap(mocks);
    jest.spyOn(console, "warn").mockImplementation();
    await deleteEntry(1);
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        t("mailmap.errorDeleteMailmapEntry"),
        t("mailmap.errorTitle"),
      );
    });
    jest.clearAllMocks();
  });

  it("should handle delete mailmap entry not found error", async (): Promise<void> => {
    expect.hasAssertions();

    const mocks = [
      mailmapEntriesQuery,
      deleteMailmapEntryWithSubentriesMutationNotFoundError,
    ];
    renderMailmap(mocks);
    jest.spyOn(console, "warn").mockImplementation();
    await deleteEntry(1);
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        t("mailmap.errorDeleteMailmapEntryNotFound"),
        t("mailmap.errorTitle"),
      );
    });
    jest.clearAllMocks();
  });

  it("should update mailmap entry", async (): Promise<void> => {
    expect.hasAssertions();

    const mocks = [mailmapEntriesQuery, updateMailmapEntryMutation];
    renderMailmap(mocks);
    jest.spyOn(console, "warn").mockImplementation();
    await updateEntry(1, "grace-new@entry.com", "Grace Musa [NEW]");
    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledTimes(1);
    });
    jest.clearAllMocks();
  });

  it("should handle update mailmap entry errors", async (): Promise<void> => {
    expect.hasAssertions();

    const mocks = [mailmapEntriesQuery, updateMailmapEntryMutationError];
    renderMailmap(mocks);
    jest.spyOn(console, "warn").mockImplementation();
    await updateEntry(1, "grace-new@entry.com", "Grace Musa [NEW]");
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        t("mailmap.errorUpdateMailmapEntry"),
        t("mailmap.errorTitle"),
      );
    });
    jest.clearAllMocks();
  });

  it("should handle update mailmap entry not found error", async (): Promise<void> => {
    expect.hasAssertions();

    const mocks = [
      mailmapEntriesQuery,
      updateMailmapEntryMutationNotFoundError,
    ];
    renderMailmap(mocks);
    jest.spyOn(console, "warn").mockImplementation();
    await updateEntry(1, "grace-new@entry.com", "Grace Musa [NEW]");
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        t("mailmap.errorUpdateMailmapEntryNotFound"),
        t("mailmap.errorTitle"),
      );
    });
    jest.clearAllMocks();
  });

  it("should set mailmap entry as mailmap subentry", async (): Promise<void> => {
    expect.hasAssertions();

    const mocks = [
      mailmapEntriesQuery,
      setMailmapEntryAsMailmapSubentryMutation,
    ];
    renderMailmap(mocks);
    jest.spyOn(console, "warn").mockImplementation();
    await setEntryAsSubentry(1, "eve@entry.com");
    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledTimes(1);
    });
    jest.clearAllMocks();
  });

  it("should handle set mailmap entry as mailmap subentry errors", async (): Promise<void> => {
    expect.hasAssertions();

    const mocks = [
      mailmapEntriesQuery,
      setMailmapEntryAsMailmapSubentryMutationError,
    ];
    renderMailmap(mocks);
    jest.spyOn(console, "warn").mockImplementation();
    await setEntryAsSubentry(1, "eve@entry.com");
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        t("mailmap.errorSetMailmapEntryAsMailmapSubentry"),
        t("mailmap.errorTitle"),
      );
    });
    jest.clearAllMocks();
  });

  it("should handle set mailmap entry as mailmap subentry not found error", async (): Promise<void> => {
    expect.hasAssertions();

    const mocks = [
      mailmapEntriesQuery,
      setMailmapEntryAsMailmapSubentryMutationNotFoundError,
    ];
    renderMailmap(mocks);
    jest.spyOn(console, "warn").mockImplementation();
    await setEntryAsSubentry(1, "eve@entry.com");
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        t("mailmap.errorTargetMailmapEntryNotFound"),
        t("mailmap.errorTitle"),
      );
    });
    jest.clearAllMocks();
  });

  it("should handle get mailmap entries errors", async (): Promise<void> => {
    expect.hasAssertions();

    const mocks = [mailmapEntriesQueryError];
    renderMailmap(mocks);
    jest.spyOn(console, "warn").mockImplementation();
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        t("mailmap.errorGetMailmapEntries"),
        t("mailmap.errorTitle"),
      );
    });
    jest.clearAllMocks();
  });

  it("should handle get mailmap subentries errors", async (): Promise<void> => {
    expect.hasAssertions();

    const mocks = [mailmapEntriesQuery, mailmapEntrySubentriesQueryError];
    renderMailmap(mocks);
    jest.spyOn(console, "warn").mockImplementation();
    await expandRow(1);
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        t("mailmap.errorGetMailmapSubentries"),
        t("mailmap.errorTitle"),
      );
    });
    jest.clearAllMocks();
  });

  it("should handle get mailmap subentries not found error", async (): Promise<void> => {
    expect.hasAssertions();

    const mocks = [
      mailmapEntriesQuery,
      mailmapEntrySubentriesQueryNotFoundError,
    ];
    renderMailmap(mocks);
    jest.spyOn(console, "warn").mockImplementation();
    await expandRow(1);
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        t("mailmap.errorMailmapEntryNotFound"),
        t("mailmap.errorTitle"),
      );
    });
    jest.clearAllMocks();
  });

  it("should move mailmap entry with subentries", async (): Promise<void> => {
    expect.hasAssertions();

    const mocks = [mailmapEntriesQuery, moveMailmapEntryWithSubentriesMutation];
    renderMailmap(mocks);
    jest.spyOn(console, "warn").mockImplementation();
    await moveEntryWithSubentries(1, "imamura");
    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledTimes(1);
    });
    jest.clearAllMocks();
  });

  it("should convert mailmap subentries to new entry", async (): Promise<void> => {
    expect.hasAssertions();

    const mocks = [
      mailmapEntriesQuery,
      convertMailmapSubentriesToNewEntryMutation,
      mailmapEntrySubentriesQuery,
    ];
    renderMailmap(mocks);
    jest.spyOn(console, "warn").mockImplementation();
    await convertSubentriesToNewEntry(1, 3);
    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledTimes(1);
    });
    jest.clearAllMocks();
  });

  async function importMailmap(file: File): Promise<void> {
    const importMailmapText = t("group.scope.git.addGitRootFile.button");

    await waitFor((): void => {
      expect(screen.queryByText(importMailmapText)).toBeInTheDocument();
    });

    await userEvent.click(screen.getByText(importMailmapText));

    await waitFor((): void => {
      expect(screen.getByTestId("file")).toBeInTheDocument();
    });

    const fileInput = screen.getByTestId("file");

    await userEvent.upload(fileInput, file);

    const confirmButton = screen.getByRole("button", {
      name: "mailmap.importMailmap",
    });

    await waitFor((): void => {
      expect(confirmButton).not.toBeDisabled();
    });

    await userEvent.click(confirmButton);
  }

  it("should import mailmap file successfully", async (): Promise<void> => {
    expect.hasAssertions();

    const mocks = [mailmapEntriesQuery, importMailmapMutation];
    renderMailmap(mocks);
    jest.spyOn(console, "warn").mockImplementation();
    const file = new File(["content"], "file");
    await importMailmap(file);

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        `${t("mailmap.successImportMailmap")}\n${t("mailmap.successfullyImportedLines")}: 5`,
        t("mailmap.successTitle"),
      );
    });
    jest.clearAllMocks();
  });

  it("should handle import mailmap file with existent imports", async (): Promise<void> => {
    expect.hasAssertions();

    const mocks = [mailmapEntriesQuery, importExistentMailmapMutation];
    renderMailmap(mocks);
    jest.spyOn(console, "warn").mockImplementation();
    const file = new File(["content"], "file");
    await importMailmap(file);

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        t("mailmap.errorImportExistentMailmap"),
        t("mailmap.errorTitle"),
      );
    });
    jest.clearAllMocks();
  });

  it("should handle import mailmap file errors", async (): Promise<void> => {
    expect.hasAssertions();

    const mocks = [importMailmapMutationError];
    renderMailmap(mocks);
    jest.spyOn(console, "warn").mockImplementation();
    const file = new File(["content"], "file");
    await importMailmap(file);
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        t("mailmap.errorImportMailmap"),
        t("mailmap.errorTitle"),
      );
    });
    jest.clearAllMocks();
  });

  it("should handle import mailmap file conflict errors", async (): Promise<void> => {
    expect.hasAssertions();

    const mocks = [importMailmapMutationConflictError];
    renderMailmap(mocks);
    jest.spyOn(console, "warn").mockImplementation();
    const file = new File(["content"], "file");
    await importMailmap(file);
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        `${t("mailmap.errorConflictingMailmapMappingError")}: [conflict1, conflict2]`,
        t("mailmap.errorTitle"),
      );
    });
    jest.clearAllMocks();
  });
});
