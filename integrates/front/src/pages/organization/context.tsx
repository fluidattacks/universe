import { useQuery } from "@apollo/client";
import _ from "lodash";
import { createContext, useMemo } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";

import type { IPortfolios } from "./portfolios/types";
import { GET_ORGANIZATION_DATA } from "./queries";
import type { IOrganizationDataContext } from "./types";

import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";

const organizationDataContext: React.Context<IOrganizationDataContext> =
  createContext({
    hasZtnaRoots: Boolean(false),
    organizationId: "",
    portfolios: [] as IPortfolios[],
  });

const OrganizationDataProvider: React.FC<
  Readonly<{
    children: JSX.Element;
  }>
> = ({ children }): JSX.Element => {
  const { organizationName } = useParams() as {
    organizationName: string;
  };
  const { t } = useTranslation();

  const { data } = useQuery(GET_ORGANIZATION_DATA, {
    fetchPolicy: "cache-first",
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.error("Couldn't load organization data", error);
      });
    },
    variables: { organizationName: organizationName.toLowerCase() },
  });

  const organizationData = useMemo((): IOrganizationDataContext => {
    return {
      hasZtnaRoots: data?.organizationId.hasZtnaRoots ?? false,
      organizationId: data?.organizationId.id ?? "",
      portfolios: [...(data?.me.tags ?? [])] as IPortfolios[],
    };
  }, [data]);

  if (_.isUndefined(data) || _.isEmpty(data)) {
    return <div />;
  }

  return (
    <organizationDataContext.Provider value={organizationData}>
      {children}
    </organizationDataContext.Provider>
  );
};

export { organizationDataContext, OrganizationDataProvider };
