import { PureAbility } from "@casl/ability";
import { fireEvent, screen, waitFor } from "@testing-library/react";
import { GraphQLError } from "graphql";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import { Route, Routes } from "react-router-dom";

import { GET_ORGANIZATION_ZTNA_HTTP_LOGS } from "./http/queries";
import { GET_ORGANIZATION_ZTNA_NETWORK_LOGS } from "./network/queries";
import { GET_ORGANIZATION_ZTNA_SESSION_LOGS } from "./session/queries";

import { OrganizationLogs } from ".";
import { authzPermissionsContext } from "context/authz/config";
import type {
  GetOrganizationZtnaHttpLogsQuery as GetOrganizationZtnaHttpLogs,
  GetOrganizationZtnaNetworkLogsQuery as GetOrganizationZtnaNetworkLogs,
  GetOrganizationZtnaSessionLogsQuery as GetOrganizationZtnaSessionLogs,
} from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";

describe("organization logs", (): void => {
  const graphqlMocked = graphql.link(LINK);
  const mockedPermissions = new PureAbility<string>([
    { action: "integrates_api_resolvers_organization_ztna_http_logs_resolve" },
    {
      action: "integrates_api_resolvers_organization_ztna_network_logs_resolve",
    },
    {
      action: "integrates_api_resolvers_organization_ztna_session_logs_resolve",
    },
    { action: "see_ztna_logs" },
  ]);
  const date = new Date("2023-12-14");

  it("should display logs network tab", async (): Promise<void> => {
    expect.hasAssertions();

    jest.useFakeTimers().setSystemTime(date);

    const mocks = [
      graphqlMocked.query(
        GET_ORGANIZATION_ZTNA_NETWORK_LOGS,
        ({
          variables,
        }): StrictResponse<
          IErrorMessage | { data: GetOrganizationZtnaNetworkLogs }
        > => {
          const { after, organizationId, startDate } = variables;
          if (
            after === "" &&
            startDate === "2023-12-13T00+00:00" &&
            organizationId === "ORG#eb50af04-4d50-4e40-bab1-a3fe9f672f9d"
          ) {
            return HttpResponse.json({
              data: {
                organization: {
                  __typename: "Organization",
                  id: "ORG#eb50af04-4d50-4e40-bab1-a3fe9f672f9d",
                  name: "okada",
                  ztnaNetworkLogs: {
                    __typename: "ZtnaNetworkConnection",
                    edges: [
                      {
                        __typename: "ZtnaNetworkEdge",
                        node: {
                          __typename: "ZtnaNetworkItem",
                          date: "2023-12-13T20:24:41+00:00",
                          email: "non_identity@test.com",
                        },
                      },
                      {
                        __typename: "ZtnaNetworkEdge",
                        node: {
                          __typename: "ZtnaNetworkItem",
                          date: "2023-12-13T20:24:42+00:00",
                          email: "non_identity@test.com",
                        },
                      },
                    ],
                    pageInfo: {
                      __typename: "PageInfo",
                      endCursor: "",
                      hasNextPage: false,
                    },
                  },
                },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error getting ztna http logs")],
          });
        },
      ),
    ];

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={
              <OrganizationLogs
                organizationId={"ORG#eb50af04-4d50-4e40-bab1-a3fe9f672f9d"}
              />
            }
            path={"/logs/*"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/logs/network"],
        },
        mocks,
      },
    );

    await waitFor((): void => {
      expect(screen.getAllByRole("button")).toHaveLength(2);
    });
    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(3);
    });

    expect(screen.getAllByRole("row")[1].textContent).toStrictEqual(
      ["2023-12-13T20:24:41+00:00", "non_identity@test.com"].join(""),
    );

    jest.useRealTimers();
    jest.clearAllMocks();
  });

  it("should display logs session tab", async (): Promise<void> => {
    expect.hasAssertions();

    jest.useFakeTimers().setSystemTime(date);

    const mocks = [
      graphqlMocked.query(
        GET_ORGANIZATION_ZTNA_SESSION_LOGS,
        ({
          variables,
        }): StrictResponse<
          IErrorMessage | { data: GetOrganizationZtnaSessionLogs }
        > => {
          const { after, organizationId } = variables;
          if (
            after === "" &&
            organizationId === "ORG#eb50af04-4d50-4e40-bab1-a3fe9f672f9d"
          ) {
            return HttpResponse.json({
              data: {
                organization: {
                  __typename: "Organization",
                  id: "ORG#eb50af04-4d50-4e40-bab1-a3fe9f672f9d",
                  name: "okada",
                  ztnaSessionLogs: {
                    __typename: "ZtnaSessionConnection",
                    edges: [],
                    pageInfo: {
                      __typename: "PageInfo",
                      endCursor: "",
                      hasNextPage: false,
                    },
                  },
                },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error getting ztna session logs")],
          });
        },
      ),
    ];

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={
              <OrganizationLogs
                organizationId={"ORG#eb50af04-4d50-4e40-bab1-a3fe9f672f9d"}
              />
            }
            path={"/logs/*"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/logs/session"],
        },
        mocks,
      },
    );

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(2);
    });

    await waitFor((): void => {
      expect(screen.getAllByRole("button")).toHaveLength(2);
    });

    await waitFor((): void => {
      expect(screen.getAllByRole("row")[1].textContent).toBe(
        "table.noDataIndication",
      );
    });

    jest.useRealTimers();
    jest.clearAllMocks();
  });

  it("should display logs http tabs", async (): Promise<void> => {
    expect.hasAssertions();

    jest.useFakeTimers().setSystemTime(date);

    const mocks = [
      graphqlMocked.query(
        GET_ORGANIZATION_ZTNA_HTTP_LOGS,
        ({
          variables,
        }): StrictResponse<
          IErrorMessage | { data: GetOrganizationZtnaHttpLogs }
        > => {
          const { after, organizationId } = variables;
          if (
            after === "" &&
            organizationId === "ORG#eb50af04-4d50-4e40-bab1-a3fe9f672f9d"
          ) {
            return HttpResponse.json({
              data: {
                organization: {
                  __typename: "Organization",
                  id: "ORG#eb50af04-4d50-4e40-bab1-a3fe9f672f9d",
                  name: "okada",
                  ztnaHttpLogs: {
                    __typename: "ZtnaHttpConnection",
                    edges: [
                      {
                        __typename: "ZtnaHttpEdge",
                        node: {
                          __typename: "ZtnaHttpItem",
                          date: "2023-12-13T22:26:59+00:00",
                          destinationIP: "10.10.10.24",
                          email: "non_identity@test.test",
                          sourceIP: "44.44.44.24",
                        },
                      },
                      {
                        __typename: "ZtnaHttpEdge",
                        node: {
                          __typename: "ZtnaHttpItem",
                          date: "2023-12-13T22:26:59+00:00",
                          destinationIP: "10.10.10.24",
                          email: "non_identity@test.test",
                          sourceIP: "44.44.44.24",
                        },
                      },
                      {
                        __typename: "ZtnaHttpEdge",
                        node: {
                          __typename: "ZtnaHttpItem",
                          date: "2023-12-13T22:26:59+00:00",
                          destinationIP: "10.10.10.24",
                          email: "non_identity@test.test",
                          sourceIP: "44.44.44.24",
                        },
                      },
                    ],
                    pageInfo: {
                      __typename: "PageInfo",
                      endCursor: "",
                      hasNextPage: false,
                    },
                  },
                },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error getting ztna http logs")],
          });
        },
      ),
    ];

    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility<string>([
            {
              action:
                "integrates_api_resolvers_organization_ztna_http_logs_resolve",
            },
            {
              action:
                "integrates_api_resolvers_organization_ztna_network_logs_resolve",
            },
            {
              action:
                "integrates_api_resolvers_organization_ztna_session_logs_resolve",
            },
            { action: "see_ztna_logs" },
            {
              action:
                "integrates_api_resolvers_query_ztna_logs_report_url_resolve",
            },
          ])
        }
      >
        <Routes>
          <Route
            element={
              <OrganizationLogs
                organizationId={"ORG#eb50af04-4d50-4e40-bab1-a3fe9f672f9d"}
              />
            }
            path={"/logs/*"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: { initialEntries: ["/logs/http"] },
        mocks,
      },
    );

    await waitFor((): void => {
      expect(screen.queryAllByRole("link")).toHaveLength(3);
    });

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(4);
    });

    expect(screen.getAllByRole("button")).toHaveLength(3);
    expect(
      screen.queryByText("group.findings.report.modalTitle"),
    ).not.toBeInTheDocument();

    fireEvent.click(
      screen.getByRole("button", { name: "group.findings.exportCsv.text" }),
    );

    await waitFor((): void => {
      expect(
        screen.queryByText("group.findings.report.modalTitle"),
      ).toBeInTheDocument();
    });

    expect(screen.queryByText("verifyDialog.title")).not.toBeInTheDocument();

    await waitFor((): void => {
      expect(
        screen.getByText("organization.tabs.ztna.reports.advice"),
      ).toBeInTheDocument();
    });

    fireEvent.click(
      screen.getByRole("button", { name: "group.findings.report.data.text" }),
    );

    await waitFor((): void => {
      expect(screen.queryByText("verifyDialog.title")).toBeInTheDocument();
    });

    expect(screen.getAllByRole("row")[1].textContent).toStrictEqual(
      [
        "2023-12-13T22:26:59+00:00",
        "non_identity@test.test",
        "10.10.10.24",
        "44.44.44.24",
      ].join(""),
    );

    jest.useRealTimers();
    jest.clearAllMocks();
  });
});
