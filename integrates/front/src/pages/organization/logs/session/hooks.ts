import { useQuery } from "@apollo/client";
import { useEffect, useMemo } from "react";
import { useTranslation } from "react-i18next";

import { GET_ORGANIZATION_ZTNA_SESSION_LOGS } from "./queries";
import type { IZtnaSessionLogData } from "./types";

import { useAudit } from "hooks/use-audit";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";

interface IUseSessionLogsQuery {
  sessionLogs: IZtnaSessionLogData[];
}

const useSessionLogsQuery = (
  organizationId: string,
  startDate: string,
  endDate: string | undefined,
): IUseSessionLogsQuery => {
  const { addAuditEvent } = useAudit();
  const { t } = useTranslation();

  const { data, fetchMore } = useQuery(GET_ORGANIZATION_ZTNA_SESSION_LOGS, {
    onCompleted: (): void => {
      addAuditEvent("Organization.SessionLogs", organizationId);
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.error(
          "An error occurred loading organization ztna session logs",
          error,
        );
      });
    },
    variables: { after: "", endDate, organizationId, startDate },
  });

  // REFAC NEEDED: Implement proper server-side pagination with the table
  useEffect((): void => {
    if (data !== undefined) {
      if (data.organization.ztnaSessionLogs?.pageInfo.hasNextPage === true) {
        void fetchMore({
          variables: {
            after: data.organization.ztnaSessionLogs.pageInfo.endCursor,
          },
        });
      }
    }
  }, [data, fetchMore]);

  const sessionLogs = useMemo(
    (): IZtnaSessionLogData[] =>
      (data?.organization.ztnaSessionLogs?.edges ?? []).map(
        (edge): IZtnaSessionLogData => ({
          email: edge?.node.email ?? "",
          endSessionDate: edge?.node.endSessionDate ?? "",
          startSessionDate: edge?.node.startSessionDate ?? "",
        }),
      ),
    [data],
  );

  return { sessionLogs };
};

export { useSessionLogsQuery };
