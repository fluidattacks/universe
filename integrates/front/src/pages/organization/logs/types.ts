interface IZtnaLogsProps {
  organizationId: string;
}

interface IZtnaLogsTemplateProps {
  url: string;
  children: JSX.Element;
}

export type { IZtnaLogsProps, IZtnaLogsTemplateProps };
