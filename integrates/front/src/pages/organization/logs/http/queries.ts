import { graphql } from "gql";

export const GET_ORGANIZATION_ZTNA_HTTP_LOGS = graphql(`
  query GetOrganizationZtnaHttpLogs(
    $after: String
    $endDate: DateTime
    $organizationId: String!
    $startDate: DateTime!
  ) {
    organization(organizationId: $organizationId) {
      id
      name
      ztnaHttpLogs(after: $after, endDate: $endDate, startDate: $startDate) {
        edges {
          node {
            date
            destinationIP
            email
            sourceIP
          }
        }
        pageInfo {
          endCursor
          hasNextPage
        }
      }
    }
  }
`);
