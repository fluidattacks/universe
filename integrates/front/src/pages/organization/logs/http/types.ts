interface IZtnaHttpLogData {
  date: string;
  destinationIP: string;
  email: string;
  sourceIP: string;
}

export type { IZtnaHttpLogData };
