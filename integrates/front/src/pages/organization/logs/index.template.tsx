import { Container, Tabs } from "@fluidattacks/design";
import type { ITabProps } from "@fluidattacks/design/dist/components/tabs/types";
import * as React from "react";
import { useTranslation } from "react-i18next";

import type { IZtnaLogsTemplateProps } from "./types";

import { useAuthz } from "hooks/use-authz";

const ZtnaLogsTemplate: React.FC<IZtnaLogsTemplateProps> = ({
  url,
  children,
}): JSX.Element => {
  const { t } = useTranslation();

  const { canResolve } = useAuthz();
  const canResolveHttpZtna = canResolve("organization_ztna_http_logs");
  const canResolveNetZtna = canResolve("organization_ztna_network_logs");
  const canResolveSessionZtna = canResolve("organization_ztna_session_logs");

  const tabsItems: ITabProps[] = [
    ...(canResolveHttpZtna
      ? [
          {
            id: "http-tab",
            label: t("organization.tabs.ztna.tabs.http.text"),
            link: `${url}/http`,
            tooltip: t("organization.tabs.ztna.tabs.http.tooltip"),
          },
        ]
      : []),
    ...(canResolveNetZtna
      ? [
          {
            id: "network-tab",
            label: t("organization.tabs.ztna.tabs.network.text"),
            link: `${url}/network`,
            tooltip: t("organization.tabs.ztna.tabs.network.tooltip"),
          },
        ]
      : []),
    ...(canResolveSessionZtna
      ? [
          {
            id: "session-tab",
            label: t("organization.tabs.ztna.tabs.session.text"),
            link: `${url}/session`,
            tooltip: t("organization.tabs.ztna.tabs.session.tooltip"),
          },
        ]
      : []),
  ];

  return (
    <React.StrictMode>
      <Tabs items={tabsItems} />
      <Container mt={0.25}>{children}</Container>
    </React.StrictMode>
  );
};

export { ZtnaLogsTemplate };
