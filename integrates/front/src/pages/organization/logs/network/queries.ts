import { graphql } from "gql";

export const GET_ORGANIZATION_ZTNA_NETWORK_LOGS = graphql(`
  query GetOrganizationZtnaNetworkLogs(
    $after: String
    $endDate: DateTime
    $organizationId: String!
    $startDate: DateTime!
  ) {
    organization(organizationId: $organizationId) {
      id
      name
      ztnaNetworkLogs(after: $after, endDate: $endDate, startDate: $startDate) {
        edges {
          node {
            date
            email
          }
        }
        pageInfo {
          endCursor
          hasNextPage
        }
      }
    }
  }
`);
