import { useQuery } from "@apollo/client";
import { useEffect, useMemo } from "react";
import { useTranslation } from "react-i18next";

import { GET_ORGANIZATION_ZTNA_NETWORK_LOGS } from "./queries";
import type { IZtnaNetworkLogData } from "./types";

import { useAudit } from "hooks/use-audit";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";

interface IUseNetworkLogsQuery {
  networkLogs: IZtnaNetworkLogData[];
}

const useNetworkLogsQuery = (
  organizationId: string,
  startDate: string,
  endDate: string | undefined,
): IUseNetworkLogsQuery => {
  const { addAuditEvent } = useAudit();
  const { t } = useTranslation();

  const { data, fetchMore } = useQuery(GET_ORGANIZATION_ZTNA_NETWORK_LOGS, {
    onCompleted: (): void => {
      addAuditEvent("Organization.NetworkLogs", organizationId);
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.error(
          "An error occurred loading organization ztna network logs",
          error,
        );
      });
    },
    variables: { after: "", endDate, organizationId, startDate },
  });

  // REFAC NEEDED: Implement proper server-side pagination with the table
  useEffect((): void => {
    if (data !== undefined) {
      if (data.organization.ztnaNetworkLogs?.pageInfo.hasNextPage === true) {
        void fetchMore({
          variables: {
            after: data.organization.ztnaNetworkLogs.pageInfo.endCursor,
          },
        });
      }
    }
  }, [data, fetchMore]);

  const networkLogs = useMemo(
    (): IZtnaNetworkLogData[] =>
      (data?.organization.ztnaNetworkLogs?.edges ?? []).map(
        (edge): IZtnaNetworkLogData => ({
          date: edge?.node.date ?? "",
          email: edge?.node.email ?? "",
        }),
      ),
    [data],
  );

  return { networkLogs };
};

export { useNetworkLogsQuery };
