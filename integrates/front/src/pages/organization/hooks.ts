import { useMatch } from "react-router-dom";

const useOrgUrl = (path = "*"): { url: string } => {
  const match = useMatch(`/orgs/:organizationName/${path}`.replace("//", "/"));

  return { url: match?.pathnameBase ?? "" };
};

export { useOrgUrl };
