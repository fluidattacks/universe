interface IRoot {
  nickname: string;
  url: string;
  credentials?: ICredentialRow;
}

interface ICredential {
  id: string;
  owner: string;
  name: string;
  oauthType?: string;
  type: string;
}

interface ICredentialWithOrganization extends ICredential {
  organizationName: string;
}

interface ICredentialRow extends ICredential {
  isUsed: boolean;
  groupName: string;
}
interface ICredentialRowAttr extends ICredentialRow {
  organizationName: string;
}

export type {
  IRoot,
  ICredential,
  ICredentialRow,
  ICredentialRowAttr,
  ICredentialWithOrganization,
};
