import { useQuery } from "@apollo/client";
import { useConfirmDialog } from "@fluidattacks/design";
import { Fragment, useCallback, useMemo } from "react";
import { useTranslation } from "react-i18next";

import { GET_CREDENTIALS_AND_ROOTS } from "./queries";
import type { ICredentialWithOrganization, IRoot } from "./types";

import { CredManagementModal } from "features/cred-management-modal";
import type { ICredManagementModalDataProps } from "features/cred-management-modal/types";
import type { IUseModal } from "hooks/use-modal";
import type { IStakeholderDataSet } from "pages/organization/members/types";
import { Logger } from "utils/logger";

interface IRemoveStakeholderModalProps {
  modalRef: IUseModal;
  onRemove: () => Promise<void>;
  organizationId: string;
  selectedStakeholders: IStakeholderDataSet[];
}

const RemoveStakeholderModal = ({
  modalRef,
  onRemove,
  organizationId,
  selectedStakeholders,
}: Readonly<IRemoveStakeholderModalProps>): JSX.Element => {
  const { t } = useTranslation();
  const { confirm, ConfirmDialog } = useConfirmDialog();

  const { data } = useQuery(GET_CREDENTIALS_AND_ROOTS, {
    context: { skipGlobalErrorHandler: true },
    errorPolicy: "all",
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        if (error.message !== "is_under_review") {
          Logger.error(
            "An error occurred loading credentials and roots",
            error,
          );
        }
      });
    },
    variables: { organizationId },
  });

  const handleClick = useCallback(async (): Promise<void> => {
    const confirmResult = await confirm({
      message: t("organization.tabs.users.removeButton.confirmMessage", {
        users: selectedStakeholders
          .map((user): string => user.email)
          .join(", "),
      }),
      title: t("organization.tabs.users.removeButton.confirmTitle"),
    });
    modalRef.close();
    if (confirmResult) {
      await onRemove();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedStakeholders]);

  const selectedData = useMemo((): ICredManagementModalDataProps => {
    const emails = selectedStakeholders.map((user): string => user.email);
    const filteredCredentials =
      data?.organization.credentials
        ?.filter((credential): boolean => emails.includes(credential.owner))
        .map((credential): ICredentialWithOrganization => {
          const organizationName = data.organization.name;

          return {
            ...credential,
            organizationName,
          };
        }) ?? [];

    if (modalRef.isOpen && filteredCredentials.length === 0) {
      void handleClick();
    }

    return {
      credentials: filteredCredentials,
      groups:
        data?.organization.groups.map(
          (group): ICredManagementModalDataProps["groups"][0] => ({
            ...group,
            organizationName: data.organization.name,
            roots: group.roots as IRoot[],
          }),
        ) ?? [],
    };
  }, [modalRef.isOpen, handleClick, data, selectedStakeholders]);

  const handleClose = useCallback((): void => {
    modalRef.close();
  }, [modalRef]);

  return (
    <Fragment>
      {selectedData.credentials.length > 0 && (
        <CredManagementModal
          data={selectedData}
          onClose={handleClose}
          onContinue={handleClick}
          open={modalRef.isOpen}
          origin={"Organization"}
        />
      )}
      <ConfirmDialog />
    </Fragment>
  );
};

export { RemoveStakeholderModal };
export type { IRemoveStakeholderModalProps };
