import { graphql } from "gql";

const GET_CREDENTIALS_AND_ROOTS = graphql(`
  query GetCredentialsAndRoots($organizationId: String!) {
    organization(organizationId: $organizationId) {
      name
      credentials {
        id
        __typename
        name
        oauthType
        owner
        type
      }
      groups {
        __typename
        name
        roots {
          ... on GitRoot {
            __typename
            nickname
            url
            credentials {
              id
              __typename
              owner
            }
          }
        }
      }
    }
  }
`);

export { GET_CREDENTIALS_AND_ROOTS };
