import { Button } from "@fluidattacks/design";
import { Fragment } from "react";
import { useTranslation } from "react-i18next";

import type { IActionsButtonsProps } from "./types";

import { Authorize } from "components/@core/authorize";

const ActionsButtons = ({
  areThereMultipleSelections,
  areButtonsDisabled,
  handleOpenCredModal,
  openEditStakeholderModal,
}: IActionsButtonsProps): JSX.Element => {
  const { t } = useTranslation();

  return (
    <Fragment>
      <Authorize
        can={"integrates_api_mutations_update_organization_stakeholder_mutate"}
      >
        <Button
          disabled={areButtonsDisabled || areThereMultipleSelections}
          icon={"edit"}
          id={"editUser"}
          onClick={openEditStakeholderModal}
          tooltip={
            areButtonsDisabled || areThereMultipleSelections
              ? t("organization.tabs.users.editButton.disabled")
              : t("organization.tabs.users.editButton.tooltip")
          }
          variant={"ghost"}
        >
          {t("organization.tabs.users.editButton.text")}
        </Button>
      </Authorize>
      <Authorize
        can={
          "integrates_api_mutations_remove_stakeholder_organization_access_mutate"
        }
      >
        <Button
          disabled={areButtonsDisabled}
          icon={"trash"}
          id={"removeUser"}
          onClick={handleOpenCredModal}
          tooltip={t("organization.tabs.users.removeButton.tooltip")}
          variant={"ghost"}
        >
          {t("organization.tabs.users.removeButton.text")}
        </Button>
      </Authorize>
    </Fragment>
  );
};

export { ActionsButtons };
