import type { ApolloQueryResult } from "@apollo/client";
import { useQuery } from "@apollo/client";
import { useTranslation } from "react-i18next";

import { GET_ORGANIZATION_STAKEHOLDERS } from "./queries";

import type { GetOrganizationStakeholdersQuery } from "gql/graphql";
import { useAudit } from "hooks/use-audit";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";

type TStakeholder =
  GetOrganizationStakeholdersQuery["organization"]["stakeholders"][0];

interface IUseOrganizationStakeholdersQuery {
  loadingStakeholders: boolean;
  refetchStakeholders: () => Promise<
    ApolloQueryResult<GetOrganizationStakeholdersQuery>
  >;
  stakeholders: TStakeholder[];
}

const useOrganizationStakeholdersQuery = (
  organizationId: string,
): IUseOrganizationStakeholdersQuery => {
  const { addAuditEvent } = useAudit();
  const { t } = useTranslation();

  const { data, loading, refetch } = useQuery(GET_ORGANIZATION_STAKEHOLDERS, {
    onCompleted: (): void => {
      addAuditEvent("Organization.Stakeholders", organizationId);
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.warning(
          "An error occurred fetching organization members",
          error,
        );
      });
    },
    variables: { organizationId },
  });

  return {
    loadingStakeholders: loading,
    refetchStakeholders: refetch,
    stakeholders: data?.organization.stakeholders ?? [],
  };
};

export { useOrganizationStakeholdersQuery };
