import type { ApolloError } from "@apollo/client";
import type { ExecutionResult } from "graphql";
import _ from "lodash";

import type { RemoveStakeholderOrganizationAccessMutationMutation } from "gql/graphql";
import type { IStakeholderDataSet } from "pages/organization/members/types";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";
import { translate } from "utils/translations/translate";

type TRemoveResult =
  ExecutionResult<RemoveStakeholderOrganizationAccessMutationMutation>;

const removeMultipleAccess = async (
  removeStakeholderAccess: (
    variables: Record<string, unknown>,
  ) => Promise<TRemoveResult>,
  stakeholders: IStakeholderDataSet[],
  organizationId: string,
): Promise<TRemoveResult[]> => {
  const chunkSize = 5;
  const stakeholderChunks = _.chunk(stakeholders, chunkSize);
  const removeChunks = stakeholderChunks.map(
    (chunk): (() => Promise<TRemoveResult[]>) =>
      async (): Promise<TRemoveResult[]> => {
        const updates = chunk.map(
          async (stakeholder): Promise<TRemoveResult> =>
            removeStakeholderAccess({
              variables: { organizationId, userEmail: stakeholder.email },
            }),
        );

        return Promise.all(updates);
      },
  );

  return removeChunks.reduce(
    async (previousValue, currentValue): Promise<TRemoveResult[]> => [
      ...(await previousValue),
      ...(await currentValue()),
    ],
    Promise.resolve<TRemoveResult[]>([]),
  );
};

const getAreAllMutationValid = (results: TRemoveResult[]): boolean[] => {
  return results.map((result): boolean => {
    if (!_.isUndefined(result.data) && !_.isNull(result.data)) {
      const removeInfoSuccess = _.isUndefined(
        result.data.removeStakeholderOrganizationAccess,
      )
        ? true
        : result.data.removeStakeholderOrganizationAccess.success;

      return removeInfoSuccess;
    }

    return false;
  });
};

const handleRemoveError = (error: ApolloError): void => {
  error.graphQLErrors.forEach(({ message }): void => {
    if (message === "Exception - Invalid forces stakeholder") {
      msgError(translate.t("validations.canNotRemoveForcesUser"));
    } else {
      msgError(translate.t("groupAlerts.errorTextsad"));
      Logger.warning(
        "An error occurred removing stakeholder of the organization",
        error,
      );
    }
  });
};

const handleMtError = (mtError: ApolloError): void => {
  mtError.graphQLErrors.forEach(({ message }): void => {
    switch (message) {
      case "Exception - Email is not valid":
        msgError(translate.t("validations.email"));
        break;
      case "Exception - This role can only be granted to Fluid Attacks users":
        msgError(translate.t("validations.userIsNotFromFluidAttacks"));
        break;
      case "Exception - Invalid field in form":
        msgError(translate.t("validations.invalidValueInField"));
        break;
      case "Exception - Invalid characters":
        msgError(translate.t("validations.invalidChar"));
        break;
      case "Exception - Invalid email address in form":
        msgError(translate.t("validations.invalidEmailInField"));
        break;
      default:
        msgError(translate.t("groupAlerts.errorTextsad"));
        Logger.warning(
          "An error occurred adding user to organization",
          mtError,
        );
    }
  });
};

export {
  getAreAllMutationValid,
  handleMtError,
  handleRemoveError,
  removeMultipleAccess,
};
