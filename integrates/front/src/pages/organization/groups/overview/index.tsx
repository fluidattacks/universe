import type { IIndicatorCardProps } from "@fluidattacks/design";
import {
  Container,
  GridContainer,
  IndicatorCard,
  Text,
} from "@fluidattacks/design";
import * as React from "react";
import type { PropsWithChildren } from "react";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import { useTheme } from "styled-components";

interface IOrganizationGroupOverviewProps {
  readonly coveredAuthors: number;
  readonly coveredRepositories: number;
  readonly missedAuthors: number;
  readonly missedRepositories: number;
  readonly organizationName: string;
}

const OrganizationGroupOverview: React.FC<IOrganizationGroupOverviewProps> = ({
  coveredAuthors,
  coveredRepositories,
  missedAuthors,
  missedRepositories,
  organizationName,
}): JSX.Element => {
  const theme = useTheme();
  const { t } = useTranslation();

  const cards: PropsWithChildren<IIndicatorCardProps>[] = [
    {
      description: t("organization.tabs.groups.overview.coveredAuthors.title"),
      rightIconName: "circle-info",
      title: `${coveredAuthors}`,
      tooltipId: "covered-authors-id",
      tooltipTip: t("organization.tabs.groups.overview.coveredAuthors.info"),
    },
    {
      description: t(
        "organization.tabs.groups.overview.coveredRepositories.title",
      ),
      rightIconName: "circle-info",
      title: `${coveredRepositories}`,
      tooltipId: "covered-repositories-id",
      tooltipTip: t(
        "organization.tabs.groups.overview.coveredRepositories.info",
      ),
    },
    {
      description: t("organization.tabs.groups.overview.missedAuthors.title"),
      rightIconName: "circle-info",
      title: `${missedAuthors}`,
      tooltipId: "missed-authors-id",
      tooltipTip: t("organization.tabs.groups.overview.missedAuthors.info"),
    },
  ];

  const cols = cards.length + 1;

  return (
    <React.StrictMode>
      <Container
        bgColor={theme.palette.white}
        borderBottom={`1px solid ${theme.palette.gray[300]}`}
        pb={1.5}
        pl={1.5}
        pr={1.5}
        pt={1.5}
      >
        <Text
          color={theme.palette.gray["800"]}
          fontWeight={"bold"}
          mb={1}
          size={"xl"}
        >
          {t("organization.tabs.groups.overview.title.text")}
        </Text>
        <GridContainer lg={cols} md={3} sm={2} xl={cols}>
          {cards.map((properties, index): JSX.Element => {
            const keyProp = `${properties.title}#${index}`;

            return (
              <IndicatorCard
                description={properties.description}
                height={"100%"}
                key={keyProp}
                leftIconName={properties.leftIconName}
                rightIconName={properties.rightIconName}
                title={properties.title}
                tooltipId={properties.tooltipId}
                tooltipTip={properties.tooltipTip}
                variant={properties.variant}
                width={"auto"}
              >
                {properties.children}
              </IndicatorCard>
            );
          })}
          <Link to={`/orgs/${organizationName}/outside`}>
            <IndicatorCard
              description={t(
                "organization.tabs.groups.overview.missedRepositories.title",
              )}
              height={"100%"}
              key={t(
                "organization.tabs.groups.overview.missedRepositories.title",
              )}
              rightIconName={"circle-info"}
              title={`${missedRepositories}`}
              tooltipId={"missed-repositories-id"}
              tooltipTip={t(
                "organization.tabs.groups.overview.missedRepositories.info",
              )}
              width={"100%"}
            />
          </Link>
        </GridContainer>
      </Container>
    </React.StrictMode>
  );
};

export { OrganizationGroupOverview };
