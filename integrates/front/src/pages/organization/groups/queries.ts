import { graphql } from "gql";

const GET_ORGANIZATION_GROUPS = graphql(`
  query GetOrganizationGroups($organizationId: String!) {
    organization(organizationId: $organizationId) {
      coveredAuthors
      coveredRepositories
      missedAuthors
      missedRepositories
      name
      groups {
        closedVulnerabilities
        description
        hasAdvanced
        hasEssential
        managed
        name
        openFindings
        openVulnerabilities
        service
        subscription
        userRole
      }
      trial {
        completed
        extensionDate
        extensionDays
        startDate
        state
      }
    }
  }
`);

const GET_ORGANIZATION_ROOTS = graphql(`
  query GetOrganizationRoots($organizationId: String!) {
    organization(organizationId: $organizationId) {
      name
      groups {
        name
        roots {
          ... on GitRoot {
            cloningStatus {
              status
            }
            machineStatus {
              modifiedDate
              status
            }
          }
        }
      }
    }
  }
`);

const GET_ORGANIZATION_EVENTS = graphql(`
  query GetOrgEvents($organizationId: String!) {
    organization(organizationId: $organizationId) {
      name
      groups {
        name
        events {
          id
          eventStatus
        }
      }
    }
  }
`);

export {
  GET_ORGANIZATION_GROUPS,
  GET_ORGANIZATION_ROOTS,
  GET_ORGANIZATION_EVENTS,
};
