import type { ApolloQueryResult } from "@apollo/client";
import { useLazyQuery, useQuery } from "@apollo/client";
import { useEffect, useMemo } from "react";
import { useTranslation } from "react-i18next";

import {
  GET_ORGANIZATION_EVENTS,
  GET_ORGANIZATION_GROUPS,
  GET_ORGANIZATION_ROOTS,
} from "./queries";
import type {
  IGroupData,
  IGroupDataResult,
  IGroupEventsDataResult,
  IGroupRootsDataResult,
} from "./types";
import { formatGroupData } from "./utils";

import type { GetOrganizationGroupsQuery } from "gql/graphql";
import { useAudit } from "hooks/use-audit";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";

type TOrganization = GetOrganizationGroupsQuery["organization"];

const DEBOUNCE_DELAY_MS = 2000;

interface IUseOrganizationGroupsQuery {
  eventsLoading: boolean;
  groups: IGroupData[];
  organization?: TOrganization;
  loading: boolean;
  refetchGroups: () => Promise<ApolloQueryResult<GetOrganizationGroupsQuery>>;
  rootsLoading: boolean;
}

const useOrganizationGroupsQuery = (
  organizationId: string,
): IUseOrganizationGroupsQuery => {
  const { addAuditEvent } = useAudit();
  const { t } = useTranslation();

  const { data, loading, refetch } = useQuery(GET_ORGANIZATION_GROUPS, {
    onCompleted: (): void => {
      addAuditEvent("Organization.Groups", organizationId);
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.error("An error occurred loading organization groups", error);
      });
    },
    variables: { organizationId },
  });

  const [
    loadRootsInfo,
    { data: orgRootsData, called: rootsFetched, loading: rootsLoading },
  ] = useLazyQuery(GET_ORGANIZATION_ROOTS, {
    context: { skipGlobalErrorHandler: true },
    errorPolicy: "all",
    fetchPolicy: "no-cache",
    variables: { organizationId },
  });

  const [
    loadEventsInfo,
    { data: orgEventsData, called: eventsFetched, loading: eventsLoading },
  ] = useLazyQuery(GET_ORGANIZATION_EVENTS, {
    context: { skipGlobalErrorHandler: true },
    errorPolicy: "all",
    fetchPolicy: "no-cache",
    variables: { organizationId },
  });

  useEffect((): VoidFunction => {
    const extraInfoTimeout = setTimeout((): void => {
      void loadRootsInfo();
      void loadEventsInfo();
    }, DEBOUNCE_DELAY_MS);

    return (): void => {
      clearTimeout(extraInfoTimeout);
    };
  }, [loadRootsInfo, loadEventsInfo]);

  const groups = useMemo(
    (): IGroupData[] =>
      formatGroupData(
        (data?.organization.groups ?? []) as IGroupDataResult[],
        orgRootsData?.organization.groups as IGroupRootsDataResult[],
        orgEventsData?.organization.groups as IGroupEventsDataResult[],
      ),
    [data, orgRootsData?.organization, orgEventsData?.organization],
  );

  return {
    eventsLoading: !eventsFetched || eventsLoading,
    groups,
    loading,
    organization: data?.organization,
    refetchGroups: refetch,
    rootsLoading: !rootsFetched || rootsLoading,
  };
};

export { useOrganizationGroupsQuery };
