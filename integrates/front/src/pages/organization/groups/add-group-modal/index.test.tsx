import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import { HttpResponse, type StrictResponse, graphql } from "msw";

import { ADD_GROUP_MUTATION } from "./queries";

import type { AddGroupMutationMutation as AddGroup } from "gql/graphql";
import { Language, ServiceType, SubscriptionType } from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";
import { AddGroupModal } from "pages/organization/groups/add-group-modal";

describe("addGroupModal component", (): void => {
  const cancelBtn = "Cancel";
  const confirmBtn = "Confirm";

  const graphqlMocked = graphql.link(LINK);
  const memoryRouter = {
    initialEntries: ["/orgs/okada/groups"],
  };
  const mocksMutation = [
    graphqlMocked.mutation(
      ADD_GROUP_MUTATION,
      ({ variables }): StrictResponse<IErrorMessage | { data: AddGroup }> => {
        const {
          description,
          groupName,
          hasEssential,
          hasAdvanced,
          language,
          organizationName,
          service,
          subscription,
        } = variables;
        if (
          description === "group description" &&
          groupName === "GROUPNAME" &&
          hasEssential &&
          !hasAdvanced &&
          language === Language.En &&
          organizationName === "OKADA" &&
          service === ServiceType.White &&
          subscription === SubscriptionType.Continuous
        ) {
          return HttpResponse.json({
            data: {
              addGroup: {
                success: true,
              },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error adding a group")],
        });
      },
    ),
  ];

  it("should render add group modal", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const handleOnClose: jest.Mock = jest.fn();
    const onClickOpen: jest.Mock = jest.fn();
    render(
      <AddGroupModal
        isOpen={true}
        onClickOpen={onClickOpen}
        onClose={handleOnClose}
        organization={"okada"}
        runTour={false}
      />,
      {
        memoryRouter,
        mocks: mocksMutation,
      },
    );

    await waitFor((): void => {
      expect(screen.queryByText(cancelBtn)).toBeInTheDocument();
    });

    await userEvent.click(screen.getByText(cancelBtn));

    expect(handleOnClose.mock.calls).toHaveLength(1);
  });

  it("should render form fields", async (): Promise<void> => {
    expect.hasAssertions();

    const onClickOpen: jest.Mock = jest.fn();
    render(
      <AddGroupModal
        isOpen={true}
        onClickOpen={onClickOpen}
        onClose={jest.fn()}
        organization={"okada"}
        runTour={true}
      />,
      {
        memoryRouter,
        mocks: mocksMutation,
      },
    );

    await waitFor((): void => {
      expect(
        screen.getByRole("textbox", { name: "organization" }),
      ).toBeInTheDocument();
    });

    expect(screen.getByRole("textbox", { name: "name" })).toBeInTheDocument();
    expect(
      screen.getByRole("textbox", { name: "description" }),
    ).toBeInTheDocument();
    expect(screen.getAllByRole("radio")).toHaveLength(2);
    expect(
      screen.getByText("organization.tabs.groups.newGroup.service.title"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("organization.tabs.groups.newGroup.language.text"),
    ).toBeInTheDocument();
    expect(screen.getByText(confirmBtn)).toBeInTheDocument();
    expect(screen.getByText(cancelBtn)).toBeInTheDocument();
  });

  it("should allow to select any service", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const handleOnClose: jest.Mock = jest.fn();
    const onClickOpen: jest.Mock = jest.fn();
    render(
      <AddGroupModal
        isOpen={true}
        onClickOpen={onClickOpen}
        onClose={handleOnClose}
        organization={"okada"}
        runTour={false}
      />,
      {
        memoryRouter,
        mocks: mocksMutation,
      },
    );

    await waitFor((): void => {
      expect(screen.getAllByRole("radio")).toHaveLength(2);
    });

    expect(screen.getAllByRole("radio")[0]).toBeChecked();

    await userEvent.click(screen.getAllByRole("radio")[1]);

    expect(screen.getAllByRole("radio")[0]).not.toBeChecked();
    expect(screen.getAllByRole("radio")[1]).toBeChecked();
  });

  it("should validate required fields", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const handleOnClose: jest.Mock = jest.fn();
    const onClickOpen: jest.Mock = jest.fn();
    render(
      <AddGroupModal
        isOpen={true}
        onClickOpen={onClickOpen}
        onClose={handleOnClose}
        organization={"okada"}
        runTour={false}
      />,
      {
        memoryRouter,
        mocks: mocksMutation,
      },
    );
    const inputGroupElement = await screen.findByRole("textbox", {
      name: "name",
    });

    const inputDescriptionElement = await screen.findByRole("textbox", {
      name: "description",
    });

    await userEvent.type(inputGroupElement, "groupname");
    await userEvent.click(screen.getByText(confirmBtn));

    expect(screen.getAllByText("Required field")).toHaveLength(1);

    await userEvent.type(inputDescriptionElement, "group description");

    await userEvent.click(screen.getByText(confirmBtn));

    expect(screen.queryAllByText("validations.required")).toHaveLength(0);
  });
});
