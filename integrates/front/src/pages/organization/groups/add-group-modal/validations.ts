import { mixed, object, string } from "yup";

import { translate } from "utils/translations/translate";

const MAX_DESCRIPTION_LENGTH = 200;
const MAX_GROUP_NAME_LENGTH = 20;
const MIN_GROUP_NAME_LENGTH = 4;
const MAX_ORGANIZATION_LENGTH = 50;

const validationSchema = object().shape({
  description: string()
    .required(translate.t("validations.required"))
    .max(
      MAX_DESCRIPTION_LENGTH,
      translate.t("validations.maxLength", { count: MAX_DESCRIPTION_LENGTH }),
    )
    .isValidTextBeginning()
    .isValidTextField(),
  name: string()
    .required(translate.t("validations.required"))
    .matches(/^[a-zA-Z0-9]+$/u, translate.t("validations.alphanumeric"))
    .max(
      MAX_GROUP_NAME_LENGTH,
      translate.t("validations.maxLength", { count: MAX_GROUP_NAME_LENGTH }),
    )
    .min(
      MIN_GROUP_NAME_LENGTH,
      translate.t("validations.minLength", { count: MIN_GROUP_NAME_LENGTH }),
    ),
  organization: string()
    .required(translate.t("validations.required"))
    .max(
      MAX_ORGANIZATION_LENGTH,
      translate.t("validations.maxLength", { count: MAX_ORGANIZATION_LENGTH }),
    )
    .isValidTextBeginning()
    .isValidTextField(),
  type: mixed().required(translate.t("validations.required")),
});

export { validationSchema };
