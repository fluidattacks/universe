type TCloningStatusType = "CLONING" | "FAILED" | "OK" | "QUEUED" | "UNKNOWN";

type TMachineStatusType = "FAILED" | "IN_PROGRESS" | "SUCCESS";

enum VulnerabilityStatus {
  CLONING_ERROR = "CLONING_ERROR",
  CLONING = "CLONING",
  IN_PROCESS = "IN_PROCESS",
  NO_VULNERABILITIES = "NO_VULNERABILITIES",
  NO_ROOT = "NO_ROOT",
  OPEN = "OPEN",
}

interface IGitRootMachineStatus {
  modifiedDate: string;
  status: TMachineStatusType;
}

interface IGitRootAttr {
  __typename: "GitRoot";
  cloningStatus: {
    status: TCloningStatusType;
  };
  machineStatus: IGitRootMachineStatus | null;
}

interface IIPRootAttr {
  __typename: "IPRoot";
}

interface IURLRootAttr {
  __typename: "URLRoot";
}

type TRoot = IGitRootAttr | IIPRootAttr | IURLRootAttr;

interface IGroupDataResult {
  closedVulnerabilities: number | null;
  description: string;
  events: {
    eventStatus: string;
  }[];
  hasEssential: boolean;
  hasAdvanced: boolean;
  managed: "MANAGED" | "NOT_MANAGED" | "TRIAL" | "UNDER_REVIEW";
  name: string;
  openFindings: number;
  openVulnerabilities: number | null;
  roots: TRoot[];
  service: string;
  subscription: string;
  userRole: string;
}

interface IGroupRootsDataResult {
  name: string;
  roots: TRoot[];
}

interface IGroupEventsDataResult {
  name: string;
  events: {
    eventStatus: string;
  }[];
}

interface IGroupData extends IGroupDataResult {
  eventFormat: string;
  plan: string;
  status: string;
  vulnerabilities: string;
}

interface IOrganizationGroupsProps {
  organizationId: string;
}

interface ITrialData {
  completed: boolean;
  extensionDate: string;
  extensionDays: number;
  startDate: string;
  state: "EXTENDED_END" | "EXTENDED" | "TRIAL_ENDED" | "TRIAL";
}

interface IAddGroupModalStore {
  isAddGroupModalOpen: boolean;
  isAddGroupTourOpen: boolean;
  handleAddGroupModal: () => void;
  handleAddGroupTour: () => void;
}

export type {
  IAddGroupModalStore,
  IGroupData,
  IGroupDataResult,
  IGroupRootsDataResult,
  IGroupEventsDataResult,
  IOrganizationGroupsProps,
  IGitRootAttr,
  ITrialData,
  TRoot,
};

export { VulnerabilityStatus };
