import { Text, Tooltip } from "@fluidattacks/design";
import isEmpty from "lodash/isEmpty";

export const statusFormatter = (resources: string[]): JSX.Element => {
  const text =
    resources.length > 2 ? (
      <Tooltip id={"resources"} tip={resources.join(", ")}>
        <Text size={"sm"}>
          {`Used by ${resources[0]}, ${resources[1]} and ${resources.length - 2} more`}
        </Text>
      </Tooltip>
    ) : (
      <Text size={"sm"}>{`Used by ${resources.join(", ")}`}</Text>
    );

  return isEmpty(resources) ? <Text size={"sm"}>{"Unused"}</Text> : text;
};
