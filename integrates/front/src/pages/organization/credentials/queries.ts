import { graphql } from "gql";

const GET_ORGANIZATION_EXTERNAL_ID_BY_ID = graphql(`
  query GetOrganizationExternalIdById($organizationId: String!) {
    organization(organizationId: $organizationId) {
      __typename
      awsExternalId
      name
    }
  }
`);

const ADD_CREDENTIALS = graphql(`
  mutation AddCredentialsMutation(
    $credentials: CredentialsInput!
    $organizationId: ID!
  ) {
    addCredentials(credentials: $credentials, organizationId: $organizationId) {
      success
    }
  }
`);

const GET_ORGANIZATION_CREDENTIALS = graphql(`
  query GetOrgCredentials($organizationId: String!) {
    organization(organizationId: $organizationId) {
      __typename
      name
      credentials {
        id
        __typename
        azureOrganization
        isPat
        isToken
        name
        oauthType
        owner
        type
      }
      groups {
        name
        gitRoots {
          edges {
            node {
              nickname
              credentials {
                id
                name
              }
            }
          }
        }
      }
    }
  }
`);

const GET_ORGANIZATION_CREDENTIALS_AND_USAGE = graphql(`
  query GetOrgCredentialsAndUsage($organizationId: String!) {
    organization(organizationId: $organizationId) {
      __typename
      name
      credentials {
        id
        __typename
        azureOrganization
        isPat
        isToken
        name
        oauthType
        owner
        type
      }
      groups {
        name
        dockerImages {
          uri
          credentials {
            id
            name
          }
        }
        gitRoots {
          edges {
            node {
              nickname
              credentials {
                id
                name
              }
            }
          }
        }
      }
    }
  }
`);

const REMOVE_CREDENTIALS = graphql(`
  mutation RemoveCredentialsMutation(
    $credentialsId: ID!
    $organizationId: ID!
  ) {
    removeCredentials(
      credentialsId: $credentialsId
      organizationId: $organizationId
    ) {
      success
    }
  }
`);

const UPDATE_CREDENTIALS = graphql(`
  mutation UpdateCredentialsMutation(
    $credentials: CredentialsInput!
    $credentialsId: ID!
    $organizationId: ID!
  ) {
    updateCredentials(
      credentials: $credentials
      credentialsId: $credentialsId
      organizationId: $organizationId
    ) {
      success
    }
  }
`);

export {
  ADD_CREDENTIALS,
  GET_ORGANIZATION_CREDENTIALS,
  GET_ORGANIZATION_CREDENTIALS_AND_USAGE,
  GET_ORGANIZATION_EXTERNAL_ID_BY_ID,
  REMOVE_CREDENTIALS,
  UPDATE_CREDENTIALS,
};
