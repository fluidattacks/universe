import { PureAbility } from "@casl/ability";
import { screen, waitFor } from "@testing-library/react";
import { Route, Routes } from "react-router-dom";

import { ActionButtons } from ".";
import { CustomThemeProvider } from "components/colors";
import { authzPermissionsContext } from "context/authz/config";
import { render } from "mocks";

jest.mock(
  "utils/resource-helpers",
  (): Record<string, jest.Mock> => ({
    openUrl: jest.fn(),
  }),
);

const Wrapper = ({
  children,
  mockedPermissions,
}: Readonly<{
  children: JSX.Element;
  mockedPermissions: PureAbility<string>;
}>): JSX.Element => (
  <Routes>
    <Route
      element={
        <CustomThemeProvider>
          <authzPermissionsContext.Provider value={mockedPermissions}>
            {children}
          </authzPermissionsContext.Provider>
        </CustomThemeProvider>
      }
      path={"/orgs/:organizationName/credentials"}
    />
  </Routes>
);

describe("actionButtons", (): void => {
  const orgId = "6e52c11c-abf7-4ca3-b7d0-635e394f41c1";

  it("shoul render action buttons", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Wrapper
        mockedPermissions={
          new PureAbility<string>([
            { action: "integrates_api_mutations_update_credentials_mutate" },
            { action: "integrates_api_mutations_remove_credentials_mutate" },
          ])
        }
      >
        <ActionButtons
          isAdding={false}
          isEditing={false}
          isRemoving={false}
          onAdd={jest.fn()}
          onEdit={jest.fn()}
          onRemove={jest.fn()}
          organizationId={orgId}
          selectedCredentials={undefined}
        />
      </Wrapper>,
      { memoryRouter: { initialEntries: ["/orgs/okada/credentials"] } },
    );

    await waitFor((): void => {
      expect(
        screen.getByText(
          "organization.tabs.credentials.actionButtons.editButton.text",
        ),
      ).toBeInTheDocument();
    });

    expect(
      screen.getByText(
        "organization.tabs.credentials.actionButtons.editButton.text",
      ),
    ).toBeDisabled();
    expect(
      screen.getByText(
        "organization.tabs.credentials.actionButtons.removeButton.text",
      ),
    ).toBeDisabled();
  });
});
