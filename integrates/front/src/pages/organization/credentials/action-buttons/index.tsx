import { Button } from "@fluidattacks/design";
import isUndefined from "lodash/isUndefined";
import { Fragment } from "react";
import { useTranslation } from "react-i18next";

import { RemoveButton } from "./remove-credentials";
import type { IActionButtonsProps } from "./types";

import { Authorize } from "components/@core/authorize";

const ActionButtons = ({
  isAdding,
  isEditing,
  isRemoving,
  onAdd,
  onEdit,
  onRemove,
  organizationId,
  selectedCredentials,
}: IActionButtonsProps): JSX.Element => {
  const { t } = useTranslation();
  const disabled = isAdding || isEditing || isRemoving;
  const prefix = "organization.tabs.credentials.actionButtons.";

  return (
    <Fragment>
      <Authorize can={"integrates_api_mutations_update_credentials_mutate"}>
        <Button
          disabled={
            disabled ||
            isUndefined(selectedCredentials) ||
            selectedCredentials.type === "OAUTH"
          }
          icon={"pen"}
          id={"editCredentials"}
          onClick={onEdit}
          tooltip={t(`${prefix}editButton.tooltip`)}
          variant={"ghost"}
        >
          {t(`${prefix}editButton.text`)}
        </Button>
      </Authorize>
      <RemoveButton
        isAdding={isAdding}
        isEditing={isEditing}
        isRemoving={isRemoving}
        onAdd={onAdd}
        onEdit={onEdit}
        onRemove={onRemove}
        organizationId={organizationId}
        selectedCredentials={selectedCredentials}
      />
    </Fragment>
  );
};

export { ActionButtons };
