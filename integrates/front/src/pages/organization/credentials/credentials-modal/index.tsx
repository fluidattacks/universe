import { Buffer } from "buffer";

import { useMutation } from "@apollo/client";
import isUndefined from "lodash/isUndefined";
import { useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { CredentialsForm } from "./credentials-form";
import type { IFormValues } from "./credentials-form/types";
import {
  handleAddCredentialsError,
  handleUpdateCredentialsError,
} from "./hooks";
import type { ICredentialsModalProps } from "./types";

import {
  ADD_CREDENTIALS,
  GET_ORGANIZATION_CREDENTIALS,
  UPDATE_CREDENTIALS,
} from "../queries";
import type { ICredentialsAttr, TSecretsCredentials } from "../types";
import { Modal } from "components/modal";
import type { CredentialsInput } from "gql/graphql";
import { useModal } from "hooks/use-modal";
import {
  formatAuthCredentials,
  formatTypeCredentials,
} from "utils/credentials";
import { msgSuccess } from "utils/notifications";

const CredentialsModal: React.FC<ICredentialsModalProps> = ({
  isAdding,
  isEditing,
  organizationId,
  onClose,
  selectedCredentials,
  setSelectedCredentials,
}): JSX.Element => {
  const { t } = useTranslation();
  const modalProps = useModal("update-credentials");

  // GraphQl mutations
  const [handleAddCredentials] = useMutation(ADD_CREDENTIALS, {
    onCompleted: (data): void => {
      if (data.addCredentials.success) {
        msgSuccess(
          t("organization.tabs.credentials.alerts.addSuccess"),
          t("groupAlerts.titleSuccess"),
        );
        onClose();
        setSelectedCredentials([]);
      }
    },
    onError: handleAddCredentialsError,
    refetchQueries: [
      { query: GET_ORGANIZATION_CREDENTIALS, variables: { organizationId } },
    ],
  });
  const [handleUpdateCredentials] = useMutation(UPDATE_CREDENTIALS, {
    onCompleted: (data): void => {
      if (data.updateCredentials.success) {
        msgSuccess(
          t("organization.tabs.credentials.alerts.editSuccess"),
          t("groupAlerts.titleSuccess"),
        );
        onClose();
        setSelectedCredentials([]);
      }
    },
    onError: handleUpdateCredentialsError,
    refetchQueries: [
      { query: GET_ORGANIZATION_CREDENTIALS, variables: { organizationId } },
    ],
  });

  const formatSecrets = useCallback(
    (values: IFormValues): TSecretsCredentials => {
      if (values.type === "HTTPS") {
        if (values.auth === "USER") {
          return {
            password: values.password,
            type: "HTTPS",
            user: values.user,
          };
        }

        return {
          azureOrganization:
            isUndefined(values.azureOrganization) ||
            isUndefined(values.isPat) ||
            !values.isPat
              ? undefined
              : values.azureOrganization,
          isPat: isUndefined(values.isPat) ? false : values.isPat,
          token: values.token,
          type: "HTTPS",
        };
      }
      if (values.type === "AWSROLE") {
        return {
          arn: isUndefined(values.arn) ? "" : values.arn,
          type: "AWSROLE",
        };
      }

      return {
        key: Buffer.from(isUndefined(values.key) ? "" : values.key).toString(
          "base64",
        ),
        type: "SSH",
      };
    },
    [],
  );

  // Handle actions
  const handleSubmit = useCallback(
    async (values: IFormValues): Promise<void> => {
      const secrets = formatSecrets(values);

      if (isAdding) {
        await handleAddCredentials({
          variables: {
            credentials: {
              name: values.name,
              ...secrets,
            } as CredentialsInput,
            organizationId,
          },
        });
      }

      if (isEditing && !isUndefined(selectedCredentials)) {
        await handleUpdateCredentials({
          variables: {
            credentials: values.newSecrets
              ? ({
                  name: values.name,
                  ...secrets,
                } as CredentialsInput)
              : {
                  name: values.name,
                },
            credentialsId: selectedCredentials[0].id,
            organizationId,
          },
        });
      }
    },
    [
      formatSecrets,
      handleAddCredentials,
      handleUpdateCredentials,
      isAdding,
      isEditing,
      organizationId,
      selectedCredentials,
    ],
  );

  return (
    <Modal
      modalRef={{ ...modalProps, close: onClose, isOpen: true }}
      size={"md"}
      title={t("profile.credentialsModal.title")}
    >
      <CredentialsForm
        initialValues={
          isEditing && !isUndefined(selectedCredentials)
            ? ({
                arn: selectedCredentials[0].arn,
                auth: formatAuthCredentials(
                  selectedCredentials[0] as ICredentialsAttr,
                ),
                azureOrganization:
                  selectedCredentials[0].azureOrganization ?? "",
                isPat: selectedCredentials[0].isPat,
                key: "",
                name: selectedCredentials[0].name,
                newSecrets: false,
                password: "",
                token: "",
                type: selectedCredentials[0].type,
                typeCredential: formatTypeCredentials(
                  selectedCredentials[0] as ICredentialsAttr,
                ),
                user: "",
              } as IFormValues)
            : undefined
        }
        isAdding={isAdding}
        isEditing={isEditing}
        onCancel={onClose}
        onSubmit={handleSubmit}
        organizationId={organizationId}
      />
    </Modal>
  );
};

export { CredentialsModal };
