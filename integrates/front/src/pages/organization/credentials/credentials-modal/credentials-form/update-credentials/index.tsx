import { Alert, Col, Link, Row, Text, Toggle } from "@fluidattacks/design";
import { Fragment, useCallback, useMemo } from "react";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

import type { IUpdateCredentialsProps } from "../types";
import type { IDropDownOption } from "components/dropdown/types";
import { InnerForm } from "components/form";
import { Input, Select, TextArea } from "components/input";

export const UpdateCredentials = ({
  values,
  isSubmitting,
  isEditing,
  dirty,
  setFieldValue,
  onCancel,
  isAdding,
  externalId,
}: IUpdateCredentialsProps): JSX.Element => {
  const { t } = useTranslation();
  const theme = useTheme();
  const documentation =
    "https://help.fluidattacks.com/portal/en/kb/articles/set-up-an-aws-integration";
  const prefix = "organization.tabs.credentials.credentialsModal.form.";

  const buttonMessage = useMemo(
    (): string => (isAdding ? "add" : "edit"),
    [isAdding],
  );

  // Handle actions
  const toggleNewSecrets = useCallback((): void => {
    setFieldValue("newSecrets", !values.newSecrets);
  }, [setFieldValue, values.newSecrets]);

  const onTypeChange = useCallback(
    (selection: IDropDownOption): void => {
      if (selection.value === "SSH") {
        setFieldValue("type", "SSH");
        setFieldValue("auth", "");
        setFieldValue("isPat", false);
      }
      if (selection.value === "USER") {
        setFieldValue("type", "HTTPS");
        setFieldValue("auth", "USER");
        setFieldValue("isPat", false);
      }
      if (selection.value === "TOKEN") {
        setFieldValue("type", "HTTPS");
        setFieldValue("auth", "TOKEN");
        setFieldValue("isPat", true);
      }
      if (selection.value === "AWSROLE") {
        setFieldValue("type", "AWSROLE");
        setFieldValue("auth", "AWSROLE");
        setFieldValue("isPat", false);
      }
    },
    [setFieldValue],
  );

  return (
    <InnerForm
      onCancel={onCancel}
      submitButtonLabel={t(`${prefix}${buttonMessage}`)}
      submitDisabled={isSubmitting || !dirty}
    >
      <Row justify={"start"}>
        <Col lg={100} md={100} sm={100}>
          <Input
            label={t(`${prefix}name.label`)}
            name={"name"}
            placeholder={t(`${prefix}name.placeholder`)}
            required={true}
          />
        </Col>
        {isAdding || values.newSecrets ? (
          <Fragment>
            <Col lg={100} md={100} sm={100}>
              <Select
                handleOnChange={onTypeChange}
                items={[
                  { header: t(`${prefix}type.ssh`), value: "SSH" },
                  { header: t(`${prefix}auth.azureToken`), value: "TOKEN" },
                  { header: t(`${prefix}auth.user`), value: "USER" },
                  { header: t(`${prefix}auth.awsRole`), value: "AWSROLE" },
                ]}
                label={t(`${prefix}type.label`)}
                name={"typeCredential"}
                required={true}
              />
            </Col>
            {values.type === "SSH" && (
              <Col lg={100} md={100} sm={100}>
                <TextArea
                  label={t("group.scope.git.repo.credentials.sshKey.text")}
                  maskValue={true}
                  maxLength={20000}
                  name={"key"}
                  placeholder={t("group.scope.git.repo.credentials.sshHint")}
                  required={true}
                />
              </Col>
            )}
            {values.type === "HTTPS" && values.auth === "TOKEN" && (
              <Fragment>
                <Col lg={100} md={100} sm={100}>
                  <Input
                    label={t(`${prefix}token`)}
                    maskValue={true}
                    name={"token"}
                    required={true}
                  />
                </Col>
                <Col lg={100} md={100} sm={100}>
                  {values.isPat === true ? (
                    <Input
                      label={t(`${prefix}azureOrganization.text`)}
                      name={"azureOrganization"}
                      required={true}
                      tooltip={t(`${prefix}azureOrganization.tooltip`)}
                    />
                  ) : undefined}
                </Col>
              </Fragment>
            )}
            {values.type === "HTTPS" && values.auth === "USER" && (
              <Fragment>
                <Col lg={50} md={50} sm={100}>
                  <Input
                    label={t(`${prefix}user`)}
                    name={"user"}
                    required={true}
                  />
                </Col>
                <Col lg={50} md={50} sm={100}>
                  <Input
                    label={t(`${prefix}password`)}
                    maskValue={true}
                    name={"password"}
                    required={true}
                  />
                </Col>
              </Fragment>
            )}
            {values.type === "AWSROLE" && (
              <Fragment>
                <Alert variant={"info"}>
                  <Text color={theme.palette.gray[800]} size={"sm"}>
                    {t("group.scope.git.addEnvironment.awsExternalId", {
                      externalId,
                    })}
                  </Text>
                  <Text color={theme.palette.gray[800]} size={"sm"}>
                    {t("group.scope.git.addEnvironment.awsSetupDocs")}
                    &nbsp;
                    <Link href={documentation}>{t("app.link")}</Link>
                  </Text>
                </Alert>
                <Input label={t(`${prefix}arn`)} name={"arn"} required={true} />
              </Fragment>
            )}
          </Fragment>
        ) : undefined}
      </Row>
      {isEditing ? (
        <Toggle
          defaultChecked={values.newSecrets}
          leftDescription={t("profile.credentialsModal.form.newSecrets")}
          name={"newSecrets"}
          onChange={toggleNewSecrets}
        />
      ) : undefined}
    </InnerForm>
  );
};
