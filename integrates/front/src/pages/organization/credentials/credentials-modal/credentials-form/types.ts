import type { FormikHelpers } from "formik";

interface IFormValues {
  auth: "TOKEN" | "USER";
  arn: string | undefined;
  azureOrganization: string | undefined;
  isPat: boolean | undefined;
  key: string | undefined;
  name: string | undefined;
  newSecrets: boolean;
  password: string | undefined;
  token: string | undefined;
  type: "AWSROLE" | "HTTPS" | "OAUTH" | "SSH";
  typeCredential: "AWSROLE" | "OAUTH" | "SSH" | "TOKEN" | "USER";
  user: string | undefined;
}

interface ICredentialsFormProps {
  initialValues?: IFormValues;
  isAdding: boolean;
  isEditing: boolean;
  onCancel: () => void;
  onSubmit: (
    values: IFormValues,
    formikHelpers: FormikHelpers<IFormValues>,
  ) => void;
  organizationId: string;
}

interface IUpdateCredentialsProps {
  values: IFormValues;
  isSubmitting: boolean;
  isEditing: boolean;
  dirty: boolean;
  onCancel: () => void;
  isAdding: boolean;
  setFieldValue: (
    field: string,
    value: unknown,
    shouldValidate?: boolean,
  ) => void;
  externalId: string;
}

export type { IFormValues, ICredentialsFormProps, IUpdateCredentialsProps };
