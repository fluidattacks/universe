/* eslint-disable @typescript-eslint/explicit-function-return-type */
import { useMutation, useQuery } from "@apollo/client";
import { useAbility } from "@casl/react";
import { Container } from "@fluidattacks/design";
import type { ColumnDef } from "@tanstack/react-table";
import isUndefined from "lodash/isUndefined";
import { useCallback, useMemo, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

import { ActionButtons } from "./action-buttons";
import { CredentialSelector } from "./credential-selector";
import { CredentialsModal } from "./credentials-modal";
import {
  GET_ORGANIZATION_CREDENTIALS_AND_USAGE,
  REMOVE_CREDENTIALS,
} from "./queries";
import { statusFormatter } from "./state-formatter";
import type {
  ICredentialsAttr,
  ICredentialsData,
  IOrganizationCredentialsProps,
} from "./types";

import { SectionHeader } from "components/section-header";
import { Table } from "components/table";
import { authzPermissionsContext } from "context/authz/config";
import { useTable } from "hooks";
import { useAudit } from "hooks/use-audit";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

const OrganizationCredentials: React.FC<IOrganizationCredentialsProps> = ({
  organizationId,
}): JSX.Element => {
  const { t } = useTranslation();
  const theme = useTheme();
  const tableRef = useTable("tblOrganizationCredentials");

  const permissions = useAbility(authzPermissionsContext);
  const canRemove = permissions.can(
    "integrates_api_mutations_remove_credentials_mutate",
  );
  const canUpdate = permissions.can(
    "integrates_api_mutations_update_credentials_mutate",
  );

  const [selectedCredentials, setSelectedCredentials] = useState<
    ICredentialsData[]
  >([]);
  const [isCredentialsModalOpen, setIsCredentialsModalOpen] = useState(false);
  const [isAdding, setIsAdding] = useState(false);
  const [isEditing, setIsEditing] = useState(false);

  // GraphQl mutations
  const [handleRemoveCredentials, { loading: isRemoving }] = useMutation(
    REMOVE_CREDENTIALS,
    {
      onCompleted: (result): void => {
        if (result.removeCredentials.success) {
          msgSuccess(
            t("organization.tabs.credentials.alerts.removeSuccess"),
            t("groupAlerts.titleSuccess"),
          );
          setSelectedCredentials([]);
        }
      },
      onError: (errors): void => {
        errors.graphQLErrors.forEach((error): void => {
          if (
            error.message === "Exception - Credential is still in use by a root"
          ) {
            msgError(t("groupAlerts.credentialInUse"));
          } else if (error.message) {
            msgError(t("groupAlerts.errorTextsad"));
            Logger.warning("An error occurred adding credentials", error);
          }
        });
      },
      refetchQueries: [
        {
          query: GET_ORGANIZATION_CREDENTIALS_AND_USAGE,
          variables: { organizationId },
        },
      ],
    },
  );

  // GraphQl queries
  const { addAuditEvent } = useAudit();
  const { data } = useQuery(GET_ORGANIZATION_CREDENTIALS_AND_USAGE, {
    onCompleted: (): void => {
      addAuditEvent("Organization.Credentials", organizationId);
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.error("Couldn't load organization information", error);
      });
    },
    variables: { organizationId },
  });

  const formatType = useCallback(
    (value: ICredentialsAttr): string => {
      if (value.type === "HTTPS") {
        if (value.isToken) {
          if (value.isPat && value.azureOrganization !== null) {
            return t(
              "organization.tabs.credentials.credentialsModal.form.auth.azureToken",
            );
          }

          return t(
            "organization.tabs.credentials.credentialsModal.form.auth.token",
          );
        }

        return t(
          "organization.tabs.credentials.credentialsModal.form.auth.user",
        );
      }

      return value.type;
    },
    [t],
  );

  // Format data
  const credentialsAttrs = useMemo(
    () => (data === undefined ? [] : data.organization.credentials),
    [data],
  );
  const credentials =
    credentialsAttrs?.map((credentialAttr) => ({
      ...credentialAttr,
      formattedType: formatType(credentialAttr as ICredentialsAttr),
    })) ?? [];

  const rootsCred =
    data?.organization.groups
      .flatMap((group) =>
        group.gitRoots.edges.map((edge) => ({
          ...edge.node.credentials,
          resource: edge.node.nickname,
        })),
      )
      .filter(Boolean) ?? [];

  const dockerImageCred =
    data?.organization.groups.flatMap((group) =>
      group.dockerImages.map((dockerImage) => ({
        ...dockerImage.credentials,
        resource: dockerImage.uri,
      })),
    ) ?? [];

  const allCreds = [...rootsCred, ...dockerImageCred];

  const formattedCredentials = credentials.map(
    (credential): ICredentialsData => {
      const used = allCreds.filter(
        (cred): boolean => cred.id === credential.id,
      );
      const resources = used.map((cred): string => cred.resource);

      return {
        ...credential,
        usedBy: resources,
      };
    },
  );

  // Handle actions
  const openCredentialsModalToAdd = useCallback((): void => {
    setIsCredentialsModalOpen(true);
    setIsAdding(true);
  }, []);
  const openCredentialsModalToEdit = useCallback((): void => {
    setIsCredentialsModalOpen(true);
    setIsEditing(true);
  }, []);
  const closeCredentialsModal = useCallback((): void => {
    setIsCredentialsModalOpen(false);
    setIsAdding(false);
    setIsEditing(false);
  }, []);

  const removeCredentials = useCallback((): void => {
    if (!isUndefined(selectedCredentials)) {
      handleRemoveCredentials({
        variables: {
          credentialsId: selectedCredentials[0].id,
          organizationId,
        },
      }).catch((): void => {
        Logger.error("An error occurred removing credentials");
      });
    }
  }, [handleRemoveCredentials, selectedCredentials, organizationId]);

  // Table config
  const tableColumns = useMemo(
    (): ColumnDef<ICredentialsData>[] => [
      {
        accessorKey: "name",
        header: t("organization.tabs.credentials.table.columns.name"),
      },
      {
        accessorKey: "formattedType",
        header: t("organization.tabs.credentials.table.columns.type"),
      },
      {
        accessorKey: "owner",
        header: t("organization.tabs.credentials.table.columns.owner"),
      },
      {
        accessorKey: "usedBy",
        cell: (cell): JSX.Element =>
          statusFormatter(cell.getValue() as string[]),
        header: t("organization.tabs.credentials.table.columns.used"),
      },
    ],
    [t],
  );

  return (
    <React.Fragment>
      <SectionHeader header={t("organization.tabs.credentials.header")} />
      <Container
        bgColor={theme.palette.gray[50]}
        height={"100hv"}
        px={1.25}
        py={1.25}
      >
        <Table
          columns={tableColumns}
          data={formattedCredentials}
          extraButtons={
            <ActionButtons
              isAdding={isAdding}
              isEditing={isEditing}
              isRemoving={isRemoving}
              onAdd={openCredentialsModalToAdd}
              onEdit={openCredentialsModalToEdit}
              onRemove={removeCredentials}
              organizationId={organizationId}
              selectedCredentials={selectedCredentials[0]}
            />
          }
          rightSideComponents={
            <CredentialSelector
              onAdd={openCredentialsModalToAdd}
              organizationId={organizationId}
            />
          }
          rowSelectionSetter={
            canRemove || canUpdate ? setSelectedCredentials : undefined
          }
          rowSelectionState={selectedCredentials}
          selectionMode={"radio"}
          tableRef={tableRef}
        />
        {isCredentialsModalOpen ? (
          <CredentialsModal
            isAdding={isAdding}
            isEditing={isEditing}
            onClose={closeCredentialsModal}
            organizationId={organizationId}
            selectedCredentials={selectedCredentials}
            setSelectedCredentials={setSelectedCredentials}
          />
        ) : undefined}
      </Container>
    </React.Fragment>
  );
};

export { OrganizationCredentials };
