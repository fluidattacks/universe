interface ICredentialSelector {
  onAdd: () => void;
  organizationId: string;
}

export type { ICredentialSelector };
