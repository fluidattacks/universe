import { Container, ProgressBar, Text } from "@fluidattacks/design";
import { useTheme } from "styled-components";

import type { IBenchmarkCardProps } from "./types";

import { handleCompliancePercentageValue } from "../../utils";
import { translate } from "utils/translations/translate";

const labels = [
  translate.t(
    "organization.tabs.compliance.content.benchmark.labels.myOrganization",
  ),
  translate.t(
    "organization.tabs.compliance.content.benchmark.labels.bestOrganization",
  ),
  translate.t(
    "organization.tabs.compliance.content.benchmark.labels.avgOrganization",
  ),
  translate.t(
    "organization.tabs.compliance.content.benchmark.labels.worstOrganization",
  ),
];

const Card = ({
  avgOrg,
  bestOrg,
  myOrg,
  standard,
  worstOrg,
}: IBenchmarkCardProps): JSX.Element => {
  const items = [avgOrg, bestOrg, myOrg, worstOrg].map(
    (element, index): [string, number | null] => [labels[index], element],
  );

  const theme = useTheme();

  return (
    <Container
      bgColor={"#fff"}
      border={`1px solid ${theme.palette.gray[200]}`}
      borderRadius={"4px"}
      display={"inline-flex"}
      flexDirection={"column"}
      gap={1.25}
      pb={1.25}
      pl={1.25}
      pr={1.25}
      pt={1.25}
    >
      <Text
        color={theme.palette.gray[800]}
        fontWeight={"bold"}
        size={"md"}
        textAlign={"center"}
      >
        {standard}
      </Text>
      {items.map(
        ([label, indicator]): JSX.Element => (
          <Container
            display={"flex"}
            gap={1}
            justify={"space-between"}
            key={label}
          >
            <Text size={"sm"}>{label}</Text>
            <ProgressBar
              minWidth={50}
              percentage={handleCompliancePercentageValue(indicator)}
              showPercentage={true}
              variant={"compliance"}
            />
          </Container>
        ),
      )}
    </Container>
  );
};

export { Card };
