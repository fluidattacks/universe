import { Container, GridContainer, Heading } from "@fluidattacks/design";
import sortBy from "lodash/sortBy";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

import { Card } from "./cards";
import type { IBenchmarkCardProps } from "./cards/types";

import type { IStandardComplianceAttr } from "../types";

const Benchmark = ({
  standards,
}: Readonly<{ standards: IStandardComplianceAttr[] | null }>): JSX.Element => {
  const theme = useTheme();
  const { t } = useTranslation();

  const cardProps = sortBy(standards, (standard): string =>
    standard.standardTitle.toUpperCase(),
  ).map(
    (standard): IBenchmarkCardProps => ({
      avgOrg: standard.avgOrganizationComplianceLevel,
      bestOrg: standard.bestOrganizationComplianceLevel,
      myOrg: standard.complianceLevel,
      standard: standard.standardTitle,
      worstOrg: standard.worstOrganizationComplianceLevel,
    }),
  );

  return (
    <Container>
      <Heading
        color={theme.palette.gray[800]}
        fontWeight={"bold"}
        mb={1.5}
        size={"md"}
      >
        {t("organization.tabs.compliance.content.benchmark.title")}
      </Heading>
      <GridContainer lg={4} md={3} sm={2} xl={5}>
        {cardProps.map(
          (properties): JSX.Element => (
            <Card
              avgOrg={properties.avgOrg}
              bestOrg={properties.bestOrg}
              key={`card-container-${properties.standard}`}
              myOrg={properties.myOrg}
              standard={properties.standard}
              worstOrg={properties.worstOrg}
            />
          ),
        )}
      </GridContainer>
    </Container>
  );
};

export { Benchmark };
