import { graphql } from "gql";

const GET_GROUP_UNFULFILLED_STANDARDS = graphql(`
  query GetGroupUnfulfilledStandards($groupName: String!) {
    group(groupName: $groupName) {
      __typename
      name
      compliance {
        __typename
        unfulfilledStandards {
          __typename
          standardId
          title
          unfulfilledRequirements {
            id
            __typename
            title
          }
        }
      }
    }
  }
`);

const GET_UNFULFILLED_STANDARD_REPORT_URL = graphql(`
  query RequestGroupReportAtComplianceStandards(
    $groupName: String!
    $reportType: ReportType!
    $unfulfilledStandards: [String!]
    $verificationCode: String!
  ) {
    unfulfilledStandardReportUrl(
      groupName: $groupName
      reportType: $reportType
      unfulfilledStandards: $unfulfilledStandards
      verificationCode: $verificationCode
    )
  }
`);

export { GET_GROUP_UNFULFILLED_STANDARDS, GET_UNFULFILLED_STANDARD_REPORT_URL };
