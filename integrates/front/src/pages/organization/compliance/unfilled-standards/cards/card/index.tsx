import { useTranslation } from "react-i18next";

import type { IUnfulfilledStandards } from "../../../types";
import { CardWithDropdown } from "components/card/card-with-dropdown";
import type { IDropDownOption } from "components/dropdown/types";

const BASE_REQUIREMENTS_URL =
  "https://help.fluidattacks.com/portal/en/kb/articles/criteria-requirements";

const Card = ({
  title,
  unfulfilledRequirements,
}: IUnfulfilledStandards): JSX.Element => {
  const { t } = useTranslation();

  const dropdownItems =
    unfulfilledRequirements?.map((requirement): IDropDownOption => {
      if (requirement === null) return {};

      return {
        header: requirement.title,
        href: `${BASE_REQUIREMENTS_URL}-${requirement.id}`,
        value: requirement.title,
      };
    }) ?? [];

  return (
    <CardWithDropdown
      description={`${t(
        "organization.tabs.compliance.content.unfulfilledStandards.subtitle",
      )} (${
        unfulfilledRequirements?.length === undefined
          ? 0
          : unfulfilledRequirements.length
      })`}
      items={[
        {
          value: t(
            "organization.tabs.compliance.content.unfulfilledStandards.defaultOption",
          ),
        },
        ...dropdownItems,
      ]}
      textAlign={"center"}
      textSpacing={0}
      title={title ?? "No Requirements"}
    />
  );
};

export { Card };
