import { Container, GridContainer, Loading } from "@fluidattacks/design";
import sortBy from "lodash/sortBy";

import { Card } from "./card";

import type { IUnfilledStandardsCards } from "../../types";

const UnfilledStandardsCards = ({
  loading,
  unfilledStandards,
}: IUnfilledStandardsCards): JSX.Element => {
  if (loading)
    return (
      <Container
        alignItems={"center"}
        display={"flex"}
        justify={"center"}
        left={"50%"}
        position={"absolute"}
        top={"50%"}
      >
        <Loading size={"lg"} />
      </Container>
    );

  // Empty
  if (unfilledStandards === undefined) return <div />;

  return (
    <GridContainer lg={4} md={3} sm={2} xl={5}>
      {sortBy(
        unfilledStandards,
        (unfilledStandard): string =>
          unfilledStandard?.title?.toUpperCase() ?? "",
      ).map((standard): JSX.Element | undefined => {
        return standard === null ? undefined : (
          <Card
            key={standard.standardId}
            standardId={standard.standardId}
            title={standard.title}
            unfulfilledRequirements={standard.unfulfilledRequirements}
          />
        );
      })}
    </GridContainer>
  );
};

export { UnfilledStandardsCards };
