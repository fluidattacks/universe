import { Toggle } from "@fluidattacks/design";
import { useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import type { IUnfulfilledStandardData } from "../../../types";

interface IIncludeFormatterProps {
  readonly row: IUnfulfilledStandardData;
  readonly changeFunction: (row: IUnfulfilledStandardData) => void;
}

const IncludeFormatter: React.FC<IIncludeFormatterProps> = ({
  row,
  changeFunction,
}): JSX.Element => {
  const { t } = useTranslation();
  const handleOnChange = useCallback((): void => {
    changeFunction(row);
  }, [changeFunction, row]);

  return (
    <Toggle
      defaultChecked={row.include}
      leftDescription={{
        off: t(
          "organization.tabs.compliance.tabs.standards.generateReportModal.exclude",
        ),
        on: t(
          "organization.tabs.compliance.tabs.standards.generateReportModal.include",
        ),
      }}
      name={row.title ?? "includeToggle"}
      onChange={handleOnChange}
    />
  );
};

export const includeFormatter = (
  row: IUnfulfilledStandardData,
  changeFunction: (row: IUnfulfilledStandardData) => void,
): JSX.Element => (
  <IncludeFormatter changeFunction={changeFunction} row={row} />
);
