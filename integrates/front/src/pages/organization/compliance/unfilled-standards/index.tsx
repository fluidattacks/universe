import { useQuery } from "@apollo/client";
import { Button, Container, Heading } from "@fluidattacks/design";
import _ from "lodash";
import { useCallback, useState } from "react";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

import { UnfilledStandardsCards } from "./cards";
import { GroupsDropdown } from "./dropdown";
import { GenerateReportModal } from "./generate-report-modal";
import { GET_GROUP_UNFULFILLED_STANDARDS } from "./queries";

import type { IDropDownOption } from "components/dropdown/types";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";

const UnfilledStandards = ({
  organizationId,
}: Readonly<{
  organizationId: string;
}>): JSX.Element => {
  const { t } = useTranslation();
  const theme = useTheme();
  const [selectedGroupName, setSelectedGroupName] = useState<string>("");
  const [isGenerateReportModalOpen, setIsGenerateReportModalOpen] =
    useState(false);

  const selectionHandler = useCallback(
    (selectedItem: IDropDownOption): void => {
      if (!_.isNil(selectedItem.value))
        setSelectedGroupName(selectedItem.value);
    },
    [],
  );

  const { data: unfilledStandardsData, loading: loadingUnfilledStandards } =
    useQuery(GET_GROUP_UNFULFILLED_STANDARDS, {
      fetchPolicy: "cache-first",
      onError: ({ graphQLErrors }): void => {
        graphQLErrors.forEach((error): void => {
          msgError(t("groupAlerts.errorTextsad"));
          Logger.warning(
            "An error occurred loading group unfulfilled standards",
            error,
          );
        });
      },
      skip: _.isUndefined(selectedGroupName) || _.isEmpty(selectedGroupName),
      variables: {
        groupName: selectedGroupName,
      },
    });

  const toggleModalVisibility = useCallback((): void => {
    setIsGenerateReportModalOpen((previous): boolean => !previous);
  }, []);

  const generateReportModal = _.isUndefined(selectedGroupName) ? undefined : (
    <GenerateReportModal
      groupName={selectedGroupName}
      isOpen={isGenerateReportModalOpen}
      onClose={toggleModalVisibility}
      unfulfilledStandards={unfilledStandardsData}
    />
  );

  return (
    <Container>
      <Container
        alignItems={"center"}
        display={"flex"}
        justify={"space-between"}
        mb={1.5}
      >
        <Container alignItems={"center"} display={"inline-flex"} gap={2}>
          <Heading
            color={theme.palette.gray[800]}
            fontWeight={"bold"}
            size={"md"}
          >
            {t(
              "organization.tabs.compliance.content.unfulfilledStandards.title",
              {
                quantity:
                  _.isNil(unfilledStandardsData) ||
                  _.isNil(unfilledStandardsData.group)
                    ? 0
                    : unfilledStandardsData.group.compliance
                        .unfulfilledStandards.length,
              },
            )}
          </Heading>
          <GroupsDropdown
            organizationId={organizationId}
            selectionHandler={selectionHandler}
          />
        </Container>
        <Button
          id={"unfulfilled-standard-report"}
          onClick={toggleModalVisibility}
          tooltip={t(
            "organization.tabs.compliance.tabs.standards.buttons.generateReport.tooltip",
          )}
        >
          {t(
            "organization.tabs.compliance.content.unfulfilledStandards.bottom.text",
          )}
        </Button>
      </Container>
      <UnfilledStandardsCards
        loading={loadingUnfilledStandards}
        unfilledStandards={
          unfilledStandardsData?.group.compliance.unfulfilledStandards
        }
      />
      {generateReportModal}
    </Container>
  );
};

export { UnfilledStandards };
