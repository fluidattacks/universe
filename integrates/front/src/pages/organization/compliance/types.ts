import type { GetGroupUnfulfilledStandardsQuery } from "gql/graphql";

interface IStandardComplianceAttr {
  avgOrganizationComplianceLevel: number;
  bestOrganizationComplianceLevel: number;
  complianceLevel: number;
  standardTitle: string;
  worstOrganizationComplianceLevel: number;
}

interface IComplianceHeaderProps {
  complianceLevel: number | null;
  estimatedDaysToFullCompliance: number | null;
  organizationName: string;
  standards: IStandardComplianceAttr[] | null;
  url: string;
}

interface IUnfulfilledRequirements {
  __typename?: "Requirement";
  id: string;
  title: string;
}

interface IUnfulfilledStandards {
  __typename?: "UnfulfilledStandard";
  standardId?: string;
  title?: string | null;
  unfulfilledRequirements?: (IUnfulfilledRequirements | null)[];
}

interface IGroupUnfulfilledStandardsResponse {
  group: {
    __typename?: "Group";
    name: string;
    compliance: {
      __typename?: "GroupCompliance";
      unfulfilledStandards: (IUnfulfilledStandards | null)[];
    };
  };
}

interface IGenerateReportModalProps {
  groupName: string;
  isOpen: boolean;
  onClose: () => void;
  unfulfilledStandards?: GetGroupUnfulfilledStandardsQuery;
}

interface IUnfilledStandardsCards {
  loading: boolean;
  unfilledStandards:
    | IGroupUnfulfilledStandardsResponse["group"]["compliance"]["unfulfilledStandards"]
    | undefined;
}

interface IUnfulfilledStandardData extends IUnfulfilledStandards {
  include: boolean;
}

export type {
  IStandardComplianceAttr,
  IComplianceHeaderProps,
  IGenerateReportModalProps,
  IUnfulfilledStandards,
  IUnfilledStandardsCards,
  IUnfulfilledStandardData,
};
