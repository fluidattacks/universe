import { useQuery } from "@apollo/client";

import { GET_ORGANIZATION_COMPLIANCE } from "./queries";

import type { GetOrganizationComplianceQuery } from "gql/graphql";
import { useAudit } from "hooks/use-audit";
import { Logger } from "utils/logger";

type TOrganization = GetOrganizationComplianceQuery["organization"];

interface IUseOrganizationComplianceQuery {
  loading: boolean;
  organization: TOrganization | undefined;
}

const useOrganizationComplianceQuery = (
  organizationId: string,
): IUseOrganizationComplianceQuery => {
  const { addAuditEvent } = useAudit();

  const { data, loading } = useQuery(GET_ORGANIZATION_COMPLIANCE, {
    fetchPolicy: "cache-first",
    onCompleted: (): void => {
      addAuditEvent("Organization.Compliance", organizationId);
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.error("Couldn't load organization compliance", error);
      });
    },
    variables: { organizationId },
  });

  return {
    loading,
    organization: data?.organization,
  };
};

export { useOrganizationComplianceQuery };
