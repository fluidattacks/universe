import type { IIndicatorCardProps } from "@fluidattacks/design";
import { Container, ProgressBar } from "@fluidattacks/design";
import isUndefined from "lodash/isUndefined";
import type { PropsWithChildren } from "react";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

import type { IComplianceHeaderProps, IStandardComplianceAttr } from "../types";
import {
  handleCompliancePercentageValue,
  handleComplianceValue,
} from "../utils";
import type { IIndicatorsProps } from "components/section-header/indicators";
import { Indicators } from "components/section-header/indicators";

const ComplianceIndicators = ({
  complianceLevel,
  estimatedDaysToFullCompliance,
  organizationName,
  standards,
}: Readonly<IComplianceHeaderProps>): JSX.Element => {
  const { t } = useTranslation();
  const theme = useTheme();

  const compliancePercentage = handleCompliancePercentageValue(complianceLevel);
  const complianceValue = handleComplianceValue(estimatedDaysToFullCompliance);

  const standardWithLowestCompliance = standards?.reduce(
    (
      lowestStandard: IStandardComplianceAttr | undefined,
      standard: IStandardComplianceAttr,
    ): IStandardComplianceAttr =>
      isUndefined(lowestStandard) ||
      (!isUndefined(lowestStandard) &&
        standard.complianceLevel < lowestStandard.complianceLevel)
        ? standard
        : lowestStandard,
    undefined,
  );

  const standardWithLowestComplianceProps: PropsWithChildren<IIndicatorCardProps>[] =
    standardWithLowestCompliance === undefined
      ? []
      : [
          {
            description: t(
              "organization.tabs.compliance.header.cards.standardWithLowestCompliance.description",
            ),
            title: standardWithLowestCompliance.standardTitle.toUpperCase(),
            variant: "left-aligned",
          },
          {
            children: (
              <ProgressBar
                minWidth={100}
                percentage={handleCompliancePercentageValue(
                  standardWithLowestCompliance.complianceLevel,
                )}
              />
            ),
            description: t(
              "organization.tabs.compliance.header.cards.standardWithLowestCompliance.percentage.description",
            ),
            title: `${handleCompliancePercentageValue(
              standardWithLowestCompliance.complianceLevel,
            )}%`,
            variant: "left-aligned",
          },
        ];

  const cardsProps: PropsWithChildren<IIndicatorCardProps>[] = [
    {
      children: (
        <ProgressBar minWidth={100} percentage={compliancePercentage} />
      ),
      description: t(
        "organization.tabs.compliance.header.cards.complianceLevel.description",
        { organizationName },
      ),
      rightIconName: "circle-info",
      title: `${compliancePercentage}%`,
      tooltipId: t(
        "organization.tabs.compliance.header.cards.complianceLevel.tooltipId",
      ),
      tooltipTip: t(
        "organization.tabs.compliance.header.cards.complianceLevel.tip",
        { percentage: compliancePercentage },
      ),
      variant: "left-aligned",
    },
    {
      description: t(
        "organization.tabs.compliance.header.cards.complianceWeeklyTrend.description",
      ),
      rightIconName: "circle-info",
      title: Math.ceil(complianceValue).toString(),
      tooltipId: t(
        "organization.tabs.compliance.header.cards.complianceWeeklyTrend.tooltipId",
      ),
      tooltipTip: t(
        "organization.tabs.compliance.header.cards.complianceWeeklyTrend.tip",
        { percentage: Math.ceil(complianceValue).toString() },
      ),
      variant: "left-aligned",
    },
    {
      description: t(
        "organization.tabs.compliance.header.cards.etToFullCompliance.description",
      ),
      rightIconName: "circle-info",
      title: t(
        "organization.tabs.compliance.header.cards.etToFullCompliance.title",
        { days: Math.ceil(complianceValue) },
      ),
      tooltipId: t(
        "organization.tabs.compliance.header.cards.etToFullCompliance.tooltipId",
      ),
      tooltipTip: t(
        "organization.tabs.compliance.header.cards.etToFullCompliance.tip",
        { days: Math.ceil(complianceValue) },
      ),
      variant: "left-aligned",
    },
    ...standardWithLowestComplianceProps,
  ];

  const indicators: IIndicatorsProps = {
    cards: cardsProps,
  };

  return (
    <Container
      bgColor={"white"}
      borderBottom={`1px solid ${theme.palette.gray[300]}`}
      pb={1}
      pl={1.25}
      pr={1.25}
      pt={1}
      width={"100%"}
    >
      <Indicators alertProps={indicators.alertProps} cards={indicators.cards} />
    </Container>
  );
};

export { ComplianceIndicators };
