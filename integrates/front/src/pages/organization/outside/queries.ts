import { graphql } from "gql";

const GET_ORGANIZATION_INTEGRATION_REPOSITORIES = graphql(`
  query GetOrganizationIntegrationRepositories(
    $after: String
    $first: Int
    $organizationId: String!
  ) {
    organization(organizationId: $organizationId) {
      __typename
      name
      integrationRepositoriesConnection(after: $after, first: $first) {
        __typename
        edges {
          node {
            defaultBranch
            lastCommitDate
            url
          }
        }
        pageInfo {
          endCursor
          hasNextPage
        }
      }
    }
  }
`);

const GET_ORGANIZATION_GROUPS = graphql(`
  query GeOrganizationGroups($organizationId: String!) {
    organization(organizationId: $organizationId) {
      __typename
      name
      permissions
      groups {
        name
        permissions
        serviceAttributes
      }
    }
  }
`);

export { GET_ORGANIZATION_INTEGRATION_REPOSITORIES, GET_ORGANIZATION_GROUPS };
