import { screen } from "@testing-library/react";

import { plusFormatterRender } from "./utils";

import { PlusFormatter } from ".";
import { render } from "mocks";

function plusFunction(param?: object): void {
  expect(param).toBeDefined();
}

describe("plusFormatter", (): void => {
  it("should render a component", (): void => {
    expect.hasAssertions();

    const { container } = render(
      <PlusFormatter plusFunction={plusFunction} row={{ test: true }} />,
    );

    expect(screen.getByRole("button")).toBeInTheDocument();

    expect(container.querySelector(".fa-plus")).toBeInTheDocument();

    expect(screen.queryByText("no text")).not.toBeInTheDocument();
  });

  it("should render a component from util", (): void => {
    expect.hasAssertions();

    const { container } = render(
      plusFormatterRender({ test: true }, plusFunction),
    );

    expect(screen.getByRole("button")).toBeInTheDocument();

    expect(container.querySelector(".fa-plus")).toBeInTheDocument();

    expect(screen.queryByText("no text")).not.toBeInTheDocument();
  });
});
