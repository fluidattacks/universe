export interface IPlusFormatterProps<T extends object> {
  row: T;
  plusFunction: (arg?: T) => void;
}
