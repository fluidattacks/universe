import { PlusFormatter } from ".";

export const plusFormatterRender = <T extends object>(
  row: T,
  plusFunction: (arg?: T) => void,
): JSX.Element => <PlusFormatter plusFunction={plusFunction} row={row} />;
