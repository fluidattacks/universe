import type { ApolloQueryResult, OperationVariables } from "@apollo/client";
import type { ExecutionResult } from "graphql";

import type { IGroups } from "@types";
import type { GetOrganizationIntegrationRepositoriesQuery } from "gql/graphql";
import type { IUseModal } from "hooks/use-modal";

interface IAddGitRootMutation {
  addGitRoot: {
    success: boolean;
  };
}

type TAddGitRootResult = ExecutionResult<IAddGitRootMutation>;

interface IIntegrationRepositoriesAttr {
  __typename: "OrganizationIntegrationRepositories";
  defaultBranch: string;
  lastCommitDate: string | null;
  url: string;
}

interface IPlusModalProps {
  groupNames: string[];
  modalRef: IUseModal;
  repositories: IIntegrationRepositoriesAttr[];
  refetchRepositories: (
    variables?: Partial<OperationVariables>,
  ) => Promise<ApolloQueryResult<GetOrganizationIntegrationRepositoriesQuery>>;
  changeGroupPermissions: (groupName: string) => void;
  changeOrganizationPermissions: () => void;
  setSelectedRow: (row: IIntegrationRepositoriesAttr | undefined) => void;
  setSelectedRepositories: (rows: IIntegrationRepositoriesAttr[]) => void;
}

interface IOrganizationOutsideProps {
  organizationId: string;
}

interface IOrganizationGroups {
  __typename: "Organization";
  groups: IGroups[];
  name: string;
  permissions: string[];
}

interface IIntegrationRepositoriesEdge {
  __typename: "IntegrationRepositoriesEdge";
  node: IIntegrationRepositoriesAttr;
}

interface IIntegrationRepositoriesConnection {
  __typename: "IntegrationRepositoriesConnection";
  edges: IIntegrationRepositoriesEdge[];
  pageInfo: {
    hasNextPage: boolean;
    endCursor: string;
  };
}

interface IOrganizationIntegrationRepositoriesAttr {
  organization: {
    __typename: "Organization";
    integrationRepositoriesConnection: IIntegrationRepositoriesConnection;
    name: string;
  };
}

export type {
  IIntegrationRepositoriesConnection,
  IIntegrationRepositoriesEdge,
  IIntegrationRepositoriesAttr,
  IPlusModalProps,
  IOrganizationIntegrationRepositoriesAttr,
  IOrganizationOutsideProps,
  IOrganizationGroups,
  IAddGitRootMutation,
  TAddGitRootResult,
};
