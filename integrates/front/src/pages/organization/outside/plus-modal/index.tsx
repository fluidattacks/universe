import { useMutation } from "@apollo/client";
import _ from "lodash";
import mixpanel from "mixpanel-browser";
import { StrictMode, useCallback, useMemo, useState } from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";
import { object, string } from "yup";

import type { IPlusModalProps, TAddGitRootResult } from "../types";
import {
  getAddGitRootCredentialsVariables,
  getAreAllMutationValid,
  handleAddedError,
} from "../utils";
import { InnerForm } from "components/form";
import { FormModal } from "components/form-modal";
import { Select } from "components/input";
import { ManagementModal } from "pages/group/scope/git-roots/management-modal";
import { ADD_GIT_ROOT } from "pages/group/scope/queries";
import type { IFormValues } from "pages/group/scope/types";
import { formatBooleanHealthCheck } from "pages/group/scope/utils";
import { validateSSHFormat } from "utils/validations";

export const PlusModal = ({
  changeGroupPermissions,
  changeOrganizationPermissions,
  groupNames,
  modalRef,
  refetchRepositories,
  repositories,
  setSelectedRepositories,
  setSelectedRow,
}: IPlusModalProps): JSX.Element => {
  const { t } = useTranslation();
  const { close } = modalRef;
  const { organizationName } = useParams<{ organizationName: string }>();

  const branches = useMemo((): string[] => {
    return repositories.map((repository): string => {
      const [branch] = repository.defaultBranch.split("/").slice(-1);

      return branch;
    });
  }, [repositories]);

  const [isManagingRoot, setIsManagingRoot] = useState<
    false | { mode: "ADD" | "EDIT" }
  >(false);
  const [groupName, setGroupName] = useState<string>("");
  const [rootModalMessages, setRootModalMessages] = useState({
    message: "",
    type: "success",
  });

  const closeModal = useCallback((): void => {
    setSelectedRow(undefined);
    setIsManagingRoot(false);
    setGroupName("");
    changeOrganizationPermissions();
    setRootModalMessages({ message: "", type: "success" });
  }, [changeOrganizationPermissions, setRootModalMessages, setSelectedRow]);

  const [addGitRoot] = useMutation(ADD_GIT_ROOT);

  const handleGitSubmit = useCallback(
    async ({
      branch,
      credentials,
      gitignore,
      includesHealthCheck,
      nickname,
      priority,
      useEgress,
      useVpn,
      useZtna,
    }: IFormValues): Promise<void> => {
      mixpanel.track("AddGitRoot");
      try {
        const chunkSize = 1;
        const repositoriesChunks = _.chunk(repositories, chunkSize);
        const addedChunks = repositoriesChunks.map(
          (repoChunk): (() => Promise<TAddGitRootResult[]>) =>
            async (): Promise<TAddGitRootResult[]> => {
              const added = repoChunk.map(
                async (repository): Promise<TAddGitRootResult> => {
                  const formattedUrl = validateSSHFormat(repository.url.trim())
                    ? `ssh://${repository.url.trim()}`
                    : repository.url.trim();

                  return addGitRoot({
                    variables: {
                      branch: branch.trim(),
                      credentials:
                        getAddGitRootCredentialsVariables(credentials),
                      criticality: _.isEmpty(priority) ? null : priority,
                      gitignore,
                      groupName,
                      includesHealthCheck:
                        formatBooleanHealthCheck(includesHealthCheck) ?? false,
                      nickname,
                      url: formattedUrl,
                      useEgress,
                      useVpn,
                      useZtna,
                    },
                  });
                },
              );

              return Promise.all(added);
            },
        );

        const results = await addedChunks.reduce(
          async (previousValue, currentValue): Promise<TAddGitRootResult[]> => [
            ...(await previousValue),
            ...(await currentValue()),
          ],
          Promise.resolve<TAddGitRootResult[]>([]),
        );
        const areAllMutationValid = getAreAllMutationValid(results);
        if (areAllMutationValid.every(Boolean)) {
          await refetchRepositories();
          setSelectedRepositories([]);
          closeModal();
        }
      } catch (addError: unknown) {
        handleAddedError(addError, setRootModalMessages);
      }
    },
    [
      addGitRoot,
      closeModal,
      groupName,
      refetchRepositories,
      repositories,
      setSelectedRepositories,
    ],
  );

  const onConfirmPlus = useCallback(
    (values: { groupName: string }): void => {
      changeGroupPermissions(values.groupName);
      setGroupName(values.groupName);
      setIsManagingRoot({ mode: "ADD" });
      close();
    },
    [changeGroupPermissions, close],
  );

  const selectOptions = groupNames.map(
    (name): { header: string; value: string } => ({
      header: name,
      value: name,
    }),
  );
  const validationSchema = object().shape({
    groupName: string().required(t("validations.required")),
  });

  return (
    <StrictMode>
      <FormModal
        initialValues={{ groupName: "" }}
        modalRef={modalRef}
        name={"groupToAddRoot"}
        onSubmit={onConfirmPlus}
        size={"md"}
        title={t("organization.tabs.weakest.modal.title")}
        validationSchema={validationSchema}
      >
        <InnerForm onCancel={close}>
          <Select
            items={[{ header: "", value: "" }].concat(selectOptions)}
            label={t("organization.tabs.weakest.modal.select", {
              count: repositories.length,
            })}
            name={"groupName"}
          />
        </InnerForm>
      </FormModal>
      {isManagingRoot === false ? undefined : (
        <ManagementModal
          externalId={""}
          groupName={groupName}
          initialValues={{
            branch: branches[0],
            cloningStatus: {
              message: "",
              status: "UNKNOWN",
            },
            credentials: {
              arn: "",
              auth: "",
              azureOrganization: "",
              id: "",
              isPat: false,
              isToken: false,
              key: "",
              name: "",
              password: "",
              token: "",
              type: "",
              typeCredential: "",
              user: "",
            },
            gitEnvironmentUrls: [],
            gitignore: [],
            hasExclusions: "",
            healthCheckConfirm: [],
            id: "",
            includesHealthCheck: "",
            nickname: "",
            priority: null,
            state: "ACTIVE",
            url: repositories[0].url,
            useEgress: false,
            useVpn: false,
            useZtna: false,
          }}
          isEditing={isManagingRoot.mode === "EDIT"}
          manyRows={repositories.length > 1}
          modalMessages={rootModalMessages}
          onClose={closeModal}
          onSubmitRepo={handleGitSubmit}
          organizationName={organizationName ?? ""}
        />
      )}
    </StrictMode>
  );
};
