/* eslint-disable functional/no-let */
// Needed for compatibility with yup validations

/* eslint-disable max-lines */
/* eslint-disable @typescript-eslint/no-invalid-this */
/* eslint-disable functional/no-this-expressions */

import dayjs, { extend } from "dayjs";
import type { Dayjs } from "dayjs";
import customParseFormat from "dayjs/plugin/customParseFormat";
import isSameOrBefore from "dayjs/plugin/isSameOrBefore";
import _ from "lodash";
import isEmpty from "lodash/isEmpty";
import type {
  AnyObject,
  ArraySchema,
  Maybe,
  MixedSchema,
  NumberSchema,
  StringSchema,
  TestContext,
  ValidationError,
} from "yup";
import { addMethod, array, mixed, number, string } from "yup";

import type { IPhoneAttr } from "features/verify-dialog/types";
import type { AnyPresentValue, TDateValidationType } from "typings/yup";
import { translate } from "utils/translations/translate";

const getFileExtension = (file: File): string => {
  const splittedName: string[] = file.name.split(".");
  const extension: string =
    splittedName.length > 1 ? (splittedName.at(-1) as string) : "";

  return extension.toLowerCase();
};

const hasExtension = (
  allowedExtensions: string[] | string,
  file?: File,
): boolean => {
  if (file !== undefined) {
    return allowedExtensions.includes(getFileExtension(file));
  }

  return false;
};

const validateSshFormatInForm = (
  regex: RegExp,
  value: string | undefined,
  values: AnyObject,
): boolean => {
  if (value === undefined || values.type !== "SSH") {
    return true;
  }

  return regex.test(value);
};

const validateSSHFormat = (url: string | undefined): boolean => {
  if (url === undefined) return true;

  const user = /[a-zA-Z][a-zA-Z0-9]*/u;
  const host = /.+/u;
  const port = /:\d{2,5}|:/u;
  const fullHost = new RegExp(`${host.source}(${port.source})?.*/.*?`, "u");
  const sshRegex = new RegExp(`^${user.source}@${fullHost.source}$`, "u");

  return sshRegex.test(url);
};

const validCommitFormat = (url: string | undefined): boolean => {
  if (url === undefined) return true;

  const region = /[a-z0-9-]+/u;
  const profile = /[\w-]+/u;
  const repo = /[\w-]+/u;

  const codeCommitRegex = new RegExp(
    `^codecommit::(${region.source})://(${profile.source}@)?${repo.source}$` +
      `|^codecommit://(${profile.source}@)?${repo.source}$`,
    "u",
  );

  return codeCommitRegex.test(url);
};

const validURLProtocol = (url: string | undefined): boolean => {
  if (url === undefined) return true;

  const protocol = /^https?:\/\//u;

  return protocol.test(url);
};

const createValidationTest =
  (
    regex: RegExp,
    messageKey: string,
    i18nKey: string,
  ): ((
    value: string | undefined,
    context: TestContext<Maybe<AnyObject>>,
  ) => ValidationError | boolean) =>
  (
    value: string | undefined,
    context: TestContext<Maybe<AnyObject>>,
  ): ValidationError | boolean => {
    if (value === undefined) {
      return true;
    }
    const match: RegExpMatchArray | null = regex.exec(value);

    return _.isNull(match)
      ? true
      : context.createError({
          message: translate.t(messageKey, {
            [i18nKey]: `'${match[0]}'`,
          }),
        });
  };

const createDynamicValidationTest = (
  validationFn: (
    value: string | undefined,
    context: TestContext<Maybe<AnyObject>>,
  ) => ValidationError | boolean,
): ((
  value: string | undefined,
  context: TestContext<Maybe<AnyObject>>,
) => ValidationError | boolean) => {
  return validationFn;
};

addMethod(
  string,
  "isValidArnFormat",
  function isValidArnFormat(
    message?: string,
  ): StringSchema<string | undefined, Maybe<AnyObject>> {
    return this.test(
      "validArnFormat",
      message ?? "",
      function validArnFormat(
        value: string | undefined,
      ): ValidationError | true {
        const { path, createError } = this;

        if (value === undefined) {
          return true;
        }

        const regex = /^arn:aws:iam::\d{12}:role\/[\w+=,.@-]+$/u;

        return regex.test(value)
          ? true
          : createError({
              message: message ?? translate.t("validations.invalidArnFormat"),
              path,
            });
      },
    );
  },
);

addMethod(
  string,
  "isValidDraftTitle",
  function isValidDraftTitle(
    message?: string,
  ): StringSchema<string | undefined, Maybe<AnyObject>> {
    return this.test(
      "validDraftTitle",
      message ?? "",
      function validDraftTitle(
        value: string | undefined,
      ): ValidationError | true {
        const { path, createError } = this;

        if (value === undefined) {
          return true;
        }

        const textMatch: RegExpMatchArray | null = /^\d{3}\. .+/gu.exec(value);

        return textMatch === null
          ? createError({
              message: message ?? translate.t("validations.draftTitle"),
              path,
            })
          : true;
      },
    );
  },
);

addMethod(
  string,
  "isValidExcludeFormat",
  function isValidExcludeFormat(
    repoUrl?: string,
    message?: string,
  ): StringSchema<string | undefined, Maybe<AnyObject>> {
    return this.test(
      "validExcludeFormat",
      message ?? "",
      function validExcludeFormat(
        value: string | undefined,
      ): ValidationError | true {
        const { path, createError } = this;

        if (value === undefined || repoUrl === undefined) {
          return true;
        }

        const [urlBasename] = repoUrl.split("/").slice(-1);
        const repoName: string = urlBasename.endsWith(".git")
          ? urlBasename.replace(".git", "")
          : urlBasename;

        return value
          .toLowerCase()
          .split("/")
          .indexOf(repoName.toLowerCase()) === 0
          ? createError({
              message: message ?? translate.t("validations.excludeFormat"),
              path,
            })
          : true;
      },
    );
  },
);

addMethod(
  mixed,
  "isValidFileName",
  function isValidFileName(
    groupName?: string,
    organizationName?: string,
    message?: string,
  ): MixedSchema<AnyPresentValue | undefined> {
    return this.test(
      "validFileName",
      message ?? "",
      function validFileName(value: unknown): ValidationError | true {
        const { path, createError } = this;

        if (value === undefined || isEmpty(value)) {
          return true;
        }

        if (groupName !== undefined && organizationName !== undefined) {
          return [...Array((value as FileList).length).keys()].every(
            (index: number): boolean => {
              const filename = (value as FileList)[
                index
              ].name.toLocaleLowerCase();
              const extensions = getFileExtension((value as FileList)[index]);
              const starts = `${organizationName.toLocaleLowerCase()}-${groupName.toLocaleLowerCase()}-`;
              const [ends] = filename.split(starts).slice(-1);
              const regex = /^[a-zA-Z0-9]{10}$/u;

              return (
                filename.startsWith(starts) &&
                regex.test(ends.replace(`.${extensions}`, ""))
              );
            },
          )
            ? true
            : createError({
                message:
                  message ?? translate.t("group.events.form.wrongImageName"),
                path,
              });
        }

        return true;
      },
    );
  },
);

addMethod(
  mixed,
  "isValidFileSize",
  function isValidFileSize(
    message?: string,
    maxFileSize?: number,
  ): MixedSchema<AnyPresentValue | undefined> {
    return this.test(
      "validFileSize",
      message ?? "",
      function validFileSize(value: unknown): ValidationError | true {
        const { path, createError } = this;

        if (value === undefined) {
          return true;
        }
        const MIB = 1048576;
        const maxSize = MIB * (maxFileSize ?? 1);

        return [...Array((value as FileList).length).keys()].every(
          (index: number): boolean => (value as FileList)[index].size > maxSize,
        )
          ? createError({
              message:
                message ??
                translate.t("validations.fileSize", {
                  count: maxFileSize ?? 1,
                }),
              path,
            })
          : true;
      },
    );
  },
);

addMethod(
  mixed,
  "isValidFileType",
  function isValidFileType(
    extensions: string[],
    message?: string,
  ): MixedSchema<AnyPresentValue | undefined> {
    return this.test(
      "validFileType",
      message ?? "",
      function validFileType(value: unknown): ValidationError | true {
        const { path, createError } = this;
        if (value === undefined) {
          return true;
        }

        return hasExtension(extensions, (value as FileList)[0])
          ? true
          : createError({
              message:
                message ?? translate.t("group.events.form.wrongFileType"),
              path,
            });
      },
    );
  },
);

addMethod(
  string,
  "isValidFindingTypology",
  function isValidFindingTypology(
    titleSuggestions: string[],
    message?: string,
  ): StringSchema<string | undefined, Maybe<AnyObject>> {
    return this.test(
      "validFindingTypology",
      message ?? "",
      function validFindingTypology(
        value: string | undefined,
      ): ValidationError | true {
        const { path, createError } = this;

        if (value === undefined) {
          return true;
        }

        return titleSuggestions.includes(value)
          ? true
          : createError({
              message: message ?? translate.t("validations.draftTypology"),
              path,
            });
      },
    );
  },
);

addMethod(
  mixed,
  "isValidFunction",
  function isValidFunction(
    fn: (
      value: AnyPresentValue | undefined,
      options: TestContext<Maybe<AnyObject>>,
    ) => boolean,
    message: string,
  ): MixedSchema<AnyPresentValue | undefined> {
    return this.test(
      "validFunction",
      message,
      function validFunction(
        value: AnyPresentValue | undefined,
        options: TestContext<Maybe<AnyObject>>,
      ): ValidationError | true {
        const { path, createError } = this;

        return fn(value, options)
          ? true
          : createError({
              message,
              path,
            });
      },
    );
  },
);

addMethod(
  number,
  "isValidFunction",
  function isValidFunction(
    fn: (
      value: number | undefined,
      options: TestContext<Maybe<AnyObject>>,
    ) => boolean,
    message: string,
  ): NumberSchema<number | undefined, Maybe<AnyObject>> {
    return this.test(
      "validFunction",
      message,
      function validFunction(
        value: number | undefined,
        options: TestContext<Maybe<AnyObject>>,
      ): ValidationError | true {
        const { path, createError } = this;

        return fn(value, options)
          ? true
          : createError({
              message,
              path,
            });
      },
    );
  },
);

addMethod(
  string,
  "isValidFunction",
  function isValidFunction(
    fn: (
      value: string | undefined,
      options: TestContext<Maybe<AnyObject>>,
    ) => boolean,
    message: string,
  ): StringSchema<string | undefined, Maybe<AnyObject>> {
    return this.test(
      "validFunction",
      message,
      function validFunction(
        value: string | undefined,
        options: TestContext<Maybe<AnyObject>>,
      ): ValidationError | true {
        const { path, createError } = this;

        return fn(value, options)
          ? true
          : createError({
              message,
              path,
            });
      },
    );
  },
);

addMethod(
  array,
  "isValidFunction",
  function isValidFunction(
    fn: (
      value: AnyObject[] | AnyPresentValue[] | undefined,
      options: TestContext<AnyObject>,
    ) => boolean,
    message: string,
  ): ArraySchema<AnyObject[] | undefined, AnyObject> {
    return this.test(
      "validFunction",
      message,
      function validFunction(
        value: AnyObject[] | undefined,
        options: TestContext<AnyObject>,
      ): ValidationError | true {
        const { path, createError } = this;

        return fn(value, options)
          ? true
          : createError({
              message,
              path,
            });
      },
    );
  },
);

addMethod(
  mixed,
  "isValidPhoneNumber",
  function isValidPhoneNumber(
    message?: string,
  ): MixedSchema<AnyPresentValue | undefined> {
    return this.test(
      "validPhoneNumber",
      message ?? "",
      function validPhoneNumber(value: unknown): ValidationError | true {
        const { path, createError } = this;

        if (value === undefined) {
          return true;
        }
        const textMatch: RegExpMatchArray | null = /^(?:\d ?){6,11}\d$/u.exec(
          (value as IPhoneAttr).nationalNumber,
        );

        return textMatch === null
          ? createError({
              message: message ?? translate.t("validations.invalidPhoneNumber"),
              path,
            })
          : true;
      },
    );
  },
);

addMethod(
  string,
  "isValidTextBeginning",
  function isValidTextBeginning(
    message?: string,
    regex?: RegExp,
  ): StringSchema<string | undefined, Maybe<AnyObject>> {
    return this.test(
      "validTextBeginning",
      message ?? "",
      function validTextBeginning(
        value: string | undefined,
      ): ValidationError | true {
        const { path, createError } = this;

        if (value === undefined) {
          return true;
        }
        const beginTextMatch: RegExpMatchArray | null = regex
          ? regex.exec(value)
          : /^=|^-|^\+|^@|^\translate.t|^\r/u.exec(value);

        return beginTextMatch === null
          ? true
          : createError({
              message:
                message ??
                translate.t("validations.invalidTextBeginning", {
                  chars: `'${beginTextMatch[0]}'`,
                }),
              path,
            });
      },
    );
  },
);

addMethod(
  string,
  "isValidTextField",
  function isValidTextField(
    message?: string,
    regex?: RegExp,
  ): StringSchema<string | undefined, Maybe<AnyObject>> {
    return this.test(
      "validTextField",
      message ?? "",
      function validTextField(
        value: string | undefined,
      ): ValidationError | true {
        const { path, createError } = this;

        if (value === undefined) {
          return true;
        }
        const textMatch: RegExpMatchArray | null = regex
          ? regex.exec(value)
          : /[^a-zA-Z0-9ñáéíóúäëïöüÑÁÉÍÓÚÄËÏÖÜ\s(){}[\],./:;@&_$%'#*=¿?¡!+-]/u.exec(
              value,
            );

        return textMatch === null
          ? true
          : createError({
              message:
                message ??
                translate.t("validations.invalidTextField", {
                  chars: `'${textMatch[0]}'`,
                }),
              path,
            });
      },
    );
  },
);

addMethod(
  string,
  "isValidTextPattern",
  function isValidTextPattern(
    message?: string,
    regex?: RegExp,
  ): StringSchema<string | undefined, Maybe<AnyObject>> {
    return this.test(
      "validTextField",
      message ?? "",
      function validTextPattern(
        value: string | undefined,
      ): ValidationError | true {
        const { path, createError } = this;

        if (value === undefined) {
          return true;
        }
        const textMatch: RegExpMatchArray | null = regex
          ? regex.exec(value)
          : /["',;](?:[-=+@\t\r]|translate\.t)/u.exec(value);

        return textMatch === null
          ? true
          : createError({
              message:
                message ??
                translate.t("validations.invalidTextPattern", {
                  chars: `'${textMatch[0]}'`,
                }),
              path,
            });
      },
    );
  },
);

addMethod(
  string,
  "isValidUrlProtocolOrSSHFormat",
  function isValidUrlProtocolOrSSHFormat(
    message?: string,
  ): StringSchema<string | undefined, Maybe<AnyObject>> {
    return this.test(
      "validUrlProtocolOrSSHFormat",
      message ?? "",
      function validUrlProtocolOrSSHFormat(
        value: string | undefined,
      ): ValidationError | true {
        const { path, createError } = this;

        if (value === undefined) {
          return true;
        }

        return validURLProtocol(value) ||
          validateSSHFormat(value) ||
          validCommitFormat(value)
          ? true
          : createError({
              message: message ?? translate.t("validations.invalidUrlProtocol"),
              path,
            });
      },
    );
  },
);

addMethod(
  string,
  "isValidValue",
  function isValidValue(
    message?: string,
    regex?: RegExp,
  ): StringSchema<string | undefined, Maybe<AnyObject>> {
    return this.test(
      "validValue",
      message ?? "",
      function validValue(value: string | undefined): ValidationError | true {
        const { path, createError } = this;

        if (value === undefined) {
          return true;
        }

        const textMatch = regex ?? /\S/u;

        return textMatch.test(value)
          ? true
          : createError({
              message: message ?? translate.t("validations.invalidSpaceField"),
              path,
            });
      },
    );
  },
);

addMethod(
  string,
  "isValidSSHFormat",
  function isValidSSHFormat(
    regex: RegExp,
    formItems: AnyObject,
    message?: string,
  ): StringSchema<string | undefined, Maybe<AnyObject>> {
    return this.test(
      "validSSHFormat",
      message ?? "",
      function validSSHFormat(
        value: string | undefined,
      ): ValidationError | true {
        const { path, createError } = this;

        return validateSshFormatInForm(regex, value, formItems)
          ? true
          : createError({
              message: message ?? translate.t("validations.invalidSshFormat"),
              path,
            });
      },
    );
  },
);

addMethod(
  mixed,
  "isValidMaxNumberFiles",
  function isValidMaxNumberFiles(
    maxNumberFiles?: number,
    message?: string,
  ): MixedSchema<AnyPresentValue | undefined> {
    return this.test(
      "validMaxNumberFiles",
      message ?? "",
      function validMaxNumberFiles(value: unknown): ValidationError | true {
        const { path, createError } = this;

        if (value === undefined) {
          return true;
        }

        return (value as FileList).length <= (maxNumberFiles ?? 1)
          ? true
          : createError({
              message:
                message ??
                translate.t("validations.amountOfFiles", {
                  count: maxNumberFiles ?? 1,
                }),
              path,
            });
      },
    );
  },
);

addMethod(
  mixed,
  "isValidDate",
  function isValidDate(
    type: TDateValidationType,
    maxWeeksCreatedDate?: number,
  ): MixedSchema<AnyPresentValue | undefined> {
    return this.test(type, function validateDateTime(value: unknown):
      | ValidationError
      | true {
      const { path, createError } = this;

      if (value === undefined) {
        return true;
      }
      extend(customParseFormat);
      extend(isSameOrBefore);

      const formattedValue = dayjs(String(value), "YYYY/MM/DD hh:mm A", true);
      const today: Dayjs = dayjs();

      let isValid = false;
      switch (type) {
        case "datetime":
          isValid = formattedValue.isValid();
          break;
        case "greaterDate":
          isValid = formattedValue.isSameOrBefore(today);
          break;
        case "greaterThanDate":
          isValid =
            Math.abs(formattedValue.diff(today, "weeks")) <=
            (maxWeeksCreatedDate ?? 1);
          break;
        case "lowerDate":
        case "validDateToken":
        default:
          break;
      }

      return isValid
        ? true
        : createError({
            message: translate.t(`validations.${type}`),
            path,
          });
    });
  },
);

addMethod(
  string,
  "isValidDate",
  function isValidDate(
    type: TDateValidationType,
  ): StringSchema<string | undefined, Maybe<AnyObject>> {
    return this.test(type, function validateDateTime(value: unknown):
      | ValidationError
      | true {
      const { path, createError } = this;

      if (value === undefined) {
        return true;
      }
      extend(customParseFormat);
      extend(isSameOrBefore);
      const today: Dayjs = dayjs();

      const maxMonthsDifference = 6;
      let isValid = false;
      switch (type) {
        case "lowerDate":
          isValid = dayjs(String(value)).diff(today) > 0;
          break;
        case "validDateToken":
          isValid =
            dayjs(String(value)).diff(today, "month") < maxMonthsDifference;
          break;
        case "greaterDate":
          isValid = dayjs(String(value)).isSameOrBefore(dayjs());
          break;
        case "datetime":
        case "greaterThanDate":
        default:
          break;
      }

      return isValid
        ? true
        : createError({
            message: translate.t(`validations.${type}`),
            path,
          });
    });
  },
);

addMethod(
  string,
  "isValidLength",
  function isValidLength(
    min: number,
    max: number,
    message: string,
  ): StringSchema<string | undefined, Maybe<AnyObject>> {
    return this.test(
      "validLength",
      message,
      function validLength(value: string | undefined): ValidationError | true {
        const { path, createError } = this;

        if (value === undefined) {
          return true;
        }

        return value.length >= min && value.length <= max
          ? true
          : createError({
              message,
              path,
            });
      },
    );
  },
);

addMethod(
  string,
  "isValidBoolean",
  function isValidBoolean(
    booleanValue: boolean,
    message: string,
  ): StringSchema<string | undefined, Maybe<AnyObject>> {
    return this.test(
      "validBoolean",
      message,
      function validBoolean(value: string | undefined): ValidationError | true {
        const { path, createError } = this;

        if (value === undefined) {
          return true;
        }

        return booleanValue
          ? true
          : createError({
              message,
              path,
            });
      },
    );
  },
);

addMethod(
  string,
  "isValidCustomRegexValidation",
  function isValidCustomRegexValidation(
    regex: RegExp,
    message: string,
    i18nKey: string,
  ): StringSchema<string | undefined, Maybe<AnyObject>> {
    return this.test({
      exclusive: false,
      name: "isValidCustomRegexValidation",
      test: createValidationTest(regex, message, i18nKey),
    });
  },
);

addMethod(
  string,
  "isValidDynamicValidation",
  function isValidDynamicValidation(
    validationFn: (
      value: string | undefined,
      context: TestContext<Maybe<AnyObject>>,
    ) => ValidationError | true,
  ): StringSchema<string | undefined, Maybe<AnyObject>> {
    return this.test({
      exclusive: false,
      name: "isValidDynamicValidation",
      test: createDynamicValidationTest(validationFn),
    });
  },
);

const sanitizeFileName = (fileName: string): string => {
  const formattedFileName = fileName.replace(/\s+/gu, "-");

  return formattedFileName.replace(/[^A-Za-z0-9!\-_.*'()&$@=;:+,?]/gu, "-");
};

export { getFileExtension, hasExtension, sanitizeFileName, validateSSHFormat };
