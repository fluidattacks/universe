import dayjs from "dayjs";

import type {
  ICVSS4BaseMetrics,
  ICVSS4EnvironmentalMetrics,
  ICVSS4ThreatMetrics,
} from "./cvss4";
import { getCVSS40Values, getCVSS4VectorString } from "./cvss4";
import { translate } from "./translations/translate";

import type {
  ICVSS3BaseMetrics,
  ICVSS3EnvironmentalMetrics,
  ICVSS3TemporalMetrics,
} from "utils/cvss";
import { getCVSS31Values, getCVSS31VectorString } from "utils/cvss";
import {
  formatIsoDate,
  getDatePlusDeltaDays,
  getRemainingDays,
  isWithInAWeek,
} from "utils/date";
import { getEnvironment } from "utils/environment";
import { formatDuration } from "utils/format-helpers";
import { generateWord } from "utils/word-generator";

describe("validations", (): void => {
  const isLowerDate = (value: string): string | undefined => {
    const date: Date = new Date(value);
    const today: Date = new Date();
    if (date <= today) {
      return translate.t("validations.lowerDate");
    }

    return undefined;
  };

  it("should be a valid date", (): void => {
    expect.hasAssertions();

    const today: Date = new Date();
    const oneMonthLater: Date = new Date(today.setMonth(today.getMonth() + 1));
    const date: string | undefined = isLowerDate(oneMonthLater.toDateString());

    expect(date).toBeUndefined();
  });

  it("should't be a valid date", (): void => {
    expect.hasAssertions();

    const today: Date = new Date();
    const oneMonthEarlier: Date = new Date(
      today.setMonth(today.getMonth() - 1),
    );
    const date: string | undefined = isLowerDate(
      oneMonthEarlier.toDateString(),
    );

    expect(date).toBeDefined();
  });
});

describe("window mock to undefined", (): void => {
  jest.spyOn(global, "window", "get").mockRestore();

  it("should be undefined window", (): void => {
    expect.hasAssertions();

    jest
      .spyOn(global as unknown as { window: undefined }, "window", "get")
      .mockReturnValue(undefined);

    const expectedEnv = "development";
    const result = getEnvironment();
    jest.spyOn(global, "window", "get").mockRestore();

    expect(result).toBe(expectedEnv);
  });
});

describe("window mock to production", (): void => {
  const { location } = window;
  jest.spyOn(window, "location", "get").mockRestore();

  it("should be in production environment", (): void => {
    expect.hasAssertions();

    const mockedLocation = {
      ...location,
      hostname: "test.com",
    };
    jest.spyOn(window, "location", "get").mockReturnValue(mockedLocation);
    const expectedEnv = "production";
    const result = getEnvironment();
    jest.spyOn(window, "location", "get").mockRestore();

    expect(result).toBe(expectedEnv);
  });
});

describe("window mock to ephemeral", (): void => {
  const { location } = window;
  jest.spyOn(window, "location", "get").mockRestore();

  it("should be in ephemeral environment", (): void => {
    expect.hasAssertions();

    const mockedLocation = {
      ...location,
      hostname: "testatfluid.app.fluidattacks.com",
    };
    jest.spyOn(window, "location", "get").mockReturnValue(mockedLocation);
    const expectedEnv = "ephemeral";
    const result = getEnvironment();
    jest.spyOn(window, "location", "get").mockRestore();

    expect(result).toBe(expectedEnv);
  });
});

describe("window mock to development", (): void => {
  const { location } = window;
  jest.spyOn(window, "location", "get").mockRestore();

  it("should be in development environment", (): void => {
    expect.hasAssertions();

    const mockedLocation = {
      ...location,
      hostname: "localhost",
    };
    jest.spyOn(window, "location", "get").mockReturnValue(mockedLocation);
    const expectedEnv = "development";
    const result = getEnvironment();
    jest.spyOn(window, "location", "get").mockRestore();

    expect(result).toBe(expectedEnv);
  });
});

describe("formatHelpers", (): void => {
  it("should return correct duration", (): void => {
    expect.hasAssertions();

    const value = 123000;
    const checkFormat = formatDuration(value);

    expect(checkFormat).toBe("00:02:03");
  });

  it("should return incorrect duration", (): void => {
    expect.hasAssertions();

    const value = -1;
    const checkFormat = formatDuration(value);

    expect(checkFormat).toBe("-");
  });
});

describe("date", (): void => {
  it("should return correct remaining days", (): void => {
    expect.hasAssertions();

    const value = new Date(new Date().getTime() + 864001000).toISOString();
    const checkRemainingDays = getRemainingDays(value);

    expect(checkRemainingDays).toBe(10);
  });

  it("should return correct formatted iso date", (): void => {
    expect.hasAssertions();

    const value = "2022-12-01T00:00:00";
    const checkRemainingDays = formatIsoDate(value);

    expect(checkRemainingDays).toBe("2022-12-01 12:00:00 AM");
  });

  it("should return correct date plus delta days", (): void => {
    expect.hasAssertions();

    const date = "2022-12-01T00:00:00";
    const days = 7;
    const checkDatePlusDeltaDays = getDatePlusDeltaDays(date, days);

    expect(checkDatePlusDeltaDays).toBe("2022-12-08 12:00:00 AM");
  });

  it("should return true if date is whitin a week", (): void => {
    expect.hasAssertions();

    const date = dayjs(new Date(new Date().getTime() - 6 * 86400000));
    const checkDatePlusDeltaDays = isWithInAWeek(date);

    expect(checkDatePlusDeltaDays).toBe(true);
  });

  it("should return false if date is out of a week", (): void => {
    expect.hasAssertions();

    const date = dayjs(new Date(new Date().getTime() - 8 * 86400000));
    const checkDatePlusDeltaDays = isWithInAWeek(date);

    expect(checkDatePlusDeltaDays).toBe(false);
  });

  it("should return a generated fake word of some length", (): void => {
    expect.hasAssertions();

    const length = 6;
    const word = generateWord(length);

    expect(word).toHaveLength(length);
  });
});

describe("cvss", (): void => {
  it("should parse a cvss vector string", (): void => {
    expect.hasAssertions();

    const notDefined = `X`;
    const baseMetrics: ICVSS3BaseMetrics = {
      attackComplexity: "L",
      attackVector: "N",
      availabilityImpact: "N",
      confidentialityImpact: "N",
      integrityImpact: "L",
      privilegesRequired: "L",
      severityScope: "U",
      userInteraction: "N",
    };
    const temporalMetrics: ICVSS3TemporalMetrics = {
      ...baseMetrics,
      exploitability: "U",
      remediationLevel: "O",
      reportConfidence: "R",
    };
    const environmentalMetrics: ICVSS3EnvironmentalMetrics = {
      ...temporalMetrics,
      availabilityRequirement: notDefined,
      confidentialityRequirement: notDefined,
      integrityRequirement: notDefined,
      modifiedAttackComplexity: notDefined,
      modifiedAttackVector: notDefined,
      modifiedAvailabilityImpact: notDefined,
      modifiedConfidentialityImpact: notDefined,
      modifiedIntegrityImpact: notDefined,
      modifiedPrivilegesRequired: notDefined,
      modifiedSeverityScope: notDefined,
      modifiedUserInteraction: notDefined,
    };
    const cvss31Values: ICVSS3EnvironmentalMetrics = getCVSS31Values(
      "CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:L/A:N/E:U/RL:O/RC:R",
    );

    expect(cvss31Values).toStrictEqual(environmentalMetrics);
  });

  it("should get a cvss vector string from the given metrics", (): void => {
    expect.hasAssertions();

    const notDefined = `X`;
    const baseMetrics: ICVSS3BaseMetrics = {
      attackComplexity: "H",
      attackVector: "N",
      availabilityImpact: "H",
      confidentialityImpact: "H",
      integrityImpact: "H",
      privilegesRequired: "H",
      severityScope: "C",
      userInteraction: "N",
    };
    const temporalMetrics: ICVSS3TemporalMetrics = {
      ...baseMetrics,
      exploitability: notDefined,
      remediationLevel: notDefined,
      reportConfidence: notDefined,
    };
    const environmentalMetrics: ICVSS3EnvironmentalMetrics = {
      ...temporalMetrics,
      availabilityRequirement: notDefined,
      confidentialityRequirement: notDefined,
      integrityRequirement: notDefined,
      modifiedAttackComplexity: notDefined,
      modifiedAttackVector: notDefined,
      modifiedAvailabilityImpact: notDefined,
      modifiedConfidentialityImpact: notDefined,
      modifiedIntegrityImpact: notDefined,
      modifiedPrivilegesRequired: notDefined,
      modifiedSeverityScope: notDefined,
      modifiedUserInteraction: notDefined,
    };
    const resultVectorString = getCVSS31VectorString(environmentalMetrics);
    const expectedVectorString =
      "CVSS:3.1/AV:N/AC:H/PR:H/UI:N/S:C/C:H/I:H/A:H/E:X/RL:X/RC:X/CR:X/IR:X/AR:X/MAV:X/MAC:X/MPR:X/MUI:X/MS:X/MC:X/MI:X/MA:X";

    expect(resultVectorString).toBe(expectedVectorString);
  });

  it("should get a cvss 4 vector string from the given metrics", (): void => {
    expect.hasAssertions();

    const notDefined = `X`;
    const baseMetrics: ICVSS4BaseMetrics = {
      attackComplexity: "H",
      attackRequirements: "N",
      attackVector: "N",
      availabilitySubsequentImpact: "L",
      availabilityVulnerableImpact: "H",
      confidentialitySubsequentImpact: "H",
      confidentialityVulnerableImpact: "L",
      integritySubsequentImpact: "H",
      integrityVulnerableImpact: "L",
      privilegesRequired: "H",
      userInteraction: "N",
    };
    const threatMetrics: ICVSS4ThreatMetrics = {
      ...baseMetrics,
      exploitability: "A",
    };
    const environmentalMetrics: ICVSS4EnvironmentalMetrics = {
      ...threatMetrics,
      availabilityRequirement: notDefined,
      confidentialityRequirement: notDefined,
      integrityRequirement: notDefined,
      modifiedAttackComplexity: notDefined,
      modifiedAttackRequirements: notDefined,
      modifiedAttackVector: notDefined,
      modifiedAvailabilitySubsequentImpact: "H",
      modifiedAvailabilityVulnerableImpact: notDefined,
      modifiedConfidentialitySubsequentImpact: "H",
      modifiedConfidentialityVulnerableImpact: notDefined,
      modifiedIntegritySubsequentImpact: "H",
      modifiedIntegrityVulnerableImpact: notDefined,
      modifiedPrivilegesRequired: notDefined,
      modifiedUserInteraction: notDefined,
    };
    const resultVectorString = getCVSS4VectorString(environmentalMetrics);
    const expectedVectorString =
      "CVSS:4.0/AV:N/AC:H/AT:N/PR:H/UI:N/VC:L/VI:L/VA:H/SC:H/SI:H/SA:L/E:A/CR:X/IR:X/AR:X/MAV:X/MAC:X/MAT:X/MPR:X/MUI:X/MVC:X/MVI:X/MVA:X/MSC:H/MSI:H/MSA:H";

    expect(resultVectorString).toBe(expectedVectorString);

    const resultMetrics = getCVSS40Values(expectedVectorString);

    expect(resultMetrics).toStrictEqual(environmentalMetrics);
  });
});
