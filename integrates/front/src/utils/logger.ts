import Bugsnag from "@bugsnag/js";

import { getEnvironment } from "utils/environment";

interface ILoggerAttr {
  error: (msg: string, extra?: unknown) => void;
  warning: (msg: string, extra?: unknown) => void;
  info: (msg: string, extra?: unknown) => void;
}

type TSeverity = "error" | "info" | "warning";

const sendBugsnagReport = (
  msg: string,
  extra: unknown,
  severity: TSeverity,
): void => {
  Bugsnag.notify(msg, (event): void => {
    event.errors.forEach((error): void => {
      // eslint-disable-next-line functional/immutable-data
      error.errorClass = `Log${severity.toUpperCase()}`;

      // eslint-disable-next-line functional/immutable-data
      error.stacktrace.splice(0, 2);
    });
    event.addMetadata("extra", { extra });

    // eslint-disable-next-line functional/immutable-data
    event.severity = severity;
  });
};

const sendErrorReport = (msg: string, extra: unknown = {}): void => {
  sendBugsnagReport(msg, extra, "error");
};

const sendWarningReport = (msg: string, extra: unknown = {}): void => {
  if (getEnvironment() === "production") {
    sendBugsnagReport(msg, extra, "warning");
  }
};

const sendInfoReport = (msg: string, extra: unknown = {}): void => {
  if (getEnvironment() === "production") {
    sendBugsnagReport(msg, extra, "info");
  }
};

const logger: ILoggerAttr = {
  error: sendErrorReport,
  info: sendInfoReport,
  warning: sendWarningReport,
};

export { logger as Logger };
