import dayjs from "dayjs";

function isWithInAWeek(date: dayjs.Dayjs): boolean {
  const numberOfDays = 7;
  const weekOld = dayjs().subtract(numberOfDays, "days").startOf("day");

  return date.isAfter(weekOld);
}

const formatDate = (value: string): string =>
  value && value !== "-" ? dayjs(value).format("YYYY-MM-DD") : "-";

const formatDateTime = (value: string): string =>
  value && value !== "-" ? dayjs(value).format("YYYY-MM-DD hh:mm A") : "-";

const formatDateTimeInInput = (value: string): string => {
  return value && value !== "-"
    ? dayjs(value).format("YYYY/MM/DD hh:mm A")
    : "-";
};

const formatIsoDate = (value: string): string =>
  value ? dayjs(value).format("YYYY-MM-DD hh:mm:ss A") : "-";

const getDatePlusDeltaDays = (date: string, days: number): string =>
  dayjs(date).add(days, "days").format("YYYY-MM-DD hh:mm:ss A");

const getRemainingDays = (value: string): number =>
  dayjs(value).diff(dayjs(), "days");

export {
  formatDate,
  formatDateTime,
  formatIsoDate,
  formatDateTimeInInput,
  getDatePlusDeltaDays,
  getRemainingDays,
  isWithInAWeek,
};
