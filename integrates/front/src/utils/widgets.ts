// Third party embeddable scripts not designed for SPAs
import { Logger } from "./logger";

// https://app.delighted.com/docs/api/web#full-api-reference
interface IDelighted {
  delighted?: {
    survey: (options: {
      email: string;
      initialDelay: number;
      name: string;
      recurringPeriod: number;
    }) => void;
  };
}
// Delay between initial visit and survey display in seconds
const initialDelaySurvey = 2629800;
// Number in seconds to wait before showing recurring surveys
const recurringPeriodSurvey = 7889400;
// Widget constants for retries
const INITIAL_ATTEMPT = 0;
const INCREMENT = 1;
const MAX_RETRIES = 3;
const RETRY_INTERVAL = 1000;

const initializeDelighted = (userEmail: string, userName: string): void => {
  function initialize(attemptCount: number): void {
    try {
      const { delighted } = window as IDelighted & typeof window;
      if (!userEmail.endsWith("@fluidattacks.com")) {
        delighted?.survey({
          email: userEmail,
          initialDelay: initialDelaySurvey,
          name: userName,
          recurringPeriod: recurringPeriodSurvey,
        });
      }
    } catch (error) {
      if (attemptCount < MAX_RETRIES) {
        setTimeout((): void => {
          initialize(attemptCount + INCREMENT);
        }, RETRY_INTERVAL);
      } else {
        Logger.warning("Couldn't initialize delighted", error);
      }
    }
  }

  initialize(INITIAL_ATTEMPT);
};

export { initializeDelighted };
