/* eslint-disable line-comment-position, no-inline-comments, @typescript-eslint/ban-ts-comment */

import { create } from "zustand";
import type { Mutate, StateCreator, StoreApi, UseBoundStore } from "zustand";

import type { OrderSort } from "gql/graphql";

type TInternalFlatten<T> = null extends T
  ? TInternalFlatten<Exclude<T, null>> | undefined
  : undefined extends T
    ? TInternalFlatten<Exclude<T, undefined>> | undefined
    : T extends bigint | boolean | number
      ? T
      : T extends object
        ? TFlatten<T>
        : T extends string
          ? `${T}`
          : // @ts-ignore
            `${T}` extends `${infer N extends number}`
            ? N
            : never;

type TFlatten<T> = {
  [K in keyof T]: TInternalFlatten<T[K]>;
};

type TInternalArrayFlatten<T> = T extends (infer U)[]
  ? TInternalArrayFlatten<U>
  : null extends T
    ? TInternalFlatten<Exclude<T, null>> | ""
    : undefined extends T
      ? TInternalFlatten<Exclude<T, undefined>> | ""
      : T extends bigint | boolean | number
        ? T
        : T extends string
          ? `${T}`
          : // @ts-ignore
            `${T}` extends `${infer N extends number}`
            ? N
            : never;

type TArrayFlatten<T> = {
  [K in keyof T]: TInternalArrayFlatten<T[K]>;
};

type TFormFilters<T> = TArrayFlatten<{
  [K in keyof T]: TInternalFlatten<T[K]>;
}>;

type TStoreValues<F> = TFlatten<{
  [K in keyof F]: F[K];
}>;

type TStoreSetters<F> = {
  [K in keyof F as `set${Capitalize<K & string>}`]: (
    value: TInternalFlatten<F[K]>,
  ) => void;
};

type TStoreFormatters<F> = {
  [K in keyof F as `format${Capitalize<K & string>}`]: (
    value: string,
  ) => string;
};

interface IPagination {
  pagination: { pageIndex: number; pageSize: number; cursor: string };
  setPageIndex: (index: number) => void;
  setPageSize: (size: number) => void;
  setPageCursor: (cursor: string) => void;
}

interface ISortable<S extends string> {
  sortBy: { field: `${S}`; order: `${OrderSort}` }[];
  setSortBy: (field: `${S}`, order: `${OrderSort}`) => void;
  resetSortBy: () => void;
}

interface IResetable {
  reset: () => void;
}

type TStore<F, S extends string> = Partial<IPagination> &
  Partial<IResetable> &
  Partial<ISortable<S>> &
  TStoreFormatters<F> &
  TStoreSetters<F> &
  TStoreValues<F>;

type TCreate<F, S extends string> = UseBoundStore<
  Mutate<StoreApi<TStore<F, S>>, []>
>;

/**
 * ## Filters Store
 * Creates a new Zustand store to use filters and sorting in server-side tables.
 *
 * e.g.:
 *
 * ```tsx
 * const useVulnsFiltersStore = createStore<Filter, Sort>(
 *   {
 *     assignees: [],
 *     state: [],
 *     tags: [],
 *   },
 *   (set): Partial<TStore<Filter, Sort>> => ({
 *     setState: (newState): void => {
 *       set((state): TStore<Filter, Sort> => ({ ...state, state: newState }));
 *     },
 *   }),
 * );
 * ```
 * @param initialValues Initial values for the store
 * @param func
 * @returns
 */
const createFilterStore = <F, S extends string>(
  initialValues: TStoreValues<F>,
  func: StateCreator<TStore<F, S>>,
): TCreate<F, S> =>
  create<TStore<F, S>>()(
    (set, get, store): TStore<F, S> => ({
      ...initialValues,
      ...func(set, get, store),
      pagination: { cursor: "", pageIndex: 0, pageSize: 10 },
      reset: (): void => {
        set(initialValues as Partial<TStore<F, S>>);
      },
      resetSortBy: (): void => {
        set(
          (state): TStore<F, S> => ({
            ...state,
            sortBy: [],
          }),
        );
      },
      setPageCursor: (cursor): void => {
        set(
          (state): TStore<F, S> => ({
            ...state,
            pagination: { ...state.pagination, cursor },
          }),
        );
      },
      setPageIndex: (index): void => {
        set(
          (state): TStore<F, S> => ({
            ...state,
            pagination: { ...state.pagination, pageIndex: index },
          }),
        );
      },
      setPageSize: (size): void => {
        set(
          (state): TStore<F, S> => ({
            ...state,
            pagination: { ...state.pagination, pageSize: size },
          }),
        );
      },
      setSortBy: (field, order): void => {
        set(
          (state): TStore<F, S> => ({
            ...state,
            sortBy: [{ field, order }],
          }),
        );
      },
      sort: [],
    }),
  );

export { createFilterStore };
export type {
  TStore,
  TStoreValues,
  TStoreSetters,
  TCreate,
  TFlatten,
  TArrayFlatten,
  TFormFilters,
};
