import { CustomThemeProvider, Notification } from "@fluidattacks/design";
import type { TNotificationVariant } from "@fluidattacks/design/dist/components/notification/types";
import type { ToastOptions } from "react-toastify";
import { toast as toastify } from "react-toastify";

type TNotification = (
  text: string,
  title?: string,
  hideMessage?: boolean,
) => void;

const closeToastify = (id: string): VoidFunction => {
  return (): void => {
    toastify.dismiss(id);
  };
};

const variants: Record<
  "error" | "errorStick" | "info" | "success" | "warning",
  ToastOptions
> = {
  error: { autoClose: false },
  errorStick: { autoClose: 3000 },
  info: { autoClose: 3000 },
  success: { autoClose: 3000 },
  warning: { autoClose: 3000 },
};

const sanitizeLevel = (level: keyof typeof variants): TNotificationVariant => {
  if (level === "errorStick") {
    return "error";
  }

  return level;
};

const showNotification = (
  level: keyof typeof variants,
  message: string,
  title = "Oops!",
): void => {
  const id = title.toLowerCase() + message.toLowerCase();
  const variant = sanitizeLevel(level);

  toastify(
    <CustomThemeProvider>
      <Notification
        description={message}
        onClose={closeToastify(id)}
        title={title || ""}
        variant={variant}
      />
    </CustomThemeProvider>,
    {
      bodyStyle: { padding: 0 },
      closeButton: false,
      toastId: id,
      ...variants[level],
    },
  );
};

const msgSuccess: TNotification = (text: string, title = "Success!"): void => {
  showNotification("success", text, title);
};

const msgWarning: TNotification = (text: string, title = "Warning"): void => {
  showNotification("warning", text, title);
};

const msgError: TNotification = (text: string, title = "Oops!"): void => {
  showNotification("error", text, title);
};

const msgErrorStick: TNotification = (text: string, title = "Oops!"): void => {
  showNotification("errorStick", text, title);
};

const msgInfo: TNotification = (
  text: string,
  title = "",
  hideMessage = false,
): void => {
  if (hideMessage) {
    toastify.dismiss(title.toLocaleLowerCase() + text.toLocaleLowerCase());
  } else {
    showNotification("info", text, title);
  }
};

export type { TNotification };
export {
  msgSuccess,
  msgError,
  msgErrorStick,
  msgInfo,
  msgWarning,
  showNotification,
};
