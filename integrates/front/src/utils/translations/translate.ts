import type { TOptions } from "i18next";
import i18next from "i18next";
import { initReactI18next } from "react-i18next";

import { Logger } from "utils/logger";
import enTranslations from "utils/translations/en.json";

i18next
  .use(initReactI18next)
  .init({
    fallbackLng: "en",
    interpolation: {
      escapeValue: false,
    },
    lng: localStorage.lang,
    resources: {
      en: { translation: enTranslations },
    },
  })
  .catch((reason: unknown): void => {
    Logger.warning("There was an error initializing translations", reason);
  });

const translate = {
  t: (key: string[] | string, options?: TOptions): string =>
    i18next.t(key, options),
};

export { i18next, translate };
