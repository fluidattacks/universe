import type { ICredentialsAttr } from "pages/group/scope/types";

const formatAuthCredentials = (value: ICredentialsAttr): "TOKEN" | "USER" => {
  if (value.isToken) {
    return "TOKEN";
  }

  return "USER";
};

const formatTypeCredentials = (
  value: ICredentialsAttr,
): "AWSROLE" | "OAUTH" | "SSH" | "TOKEN" | "USER" => {
  if (value.type === "HTTPS") {
    return formatAuthCredentials(value);
  }

  if (value.type === "OAUTH") {
    return "OAUTH";
  }

  if (value.type === "AWSROLE") {
    return "AWSROLE";
  }

  return "SSH";
};

export { formatAuthCredentials, formatTypeCredentials };
