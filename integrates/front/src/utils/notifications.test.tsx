import type {
  INotificationProps,
  TNotificationVariant,
} from "@fluidattacks/design/dist/components/notification/types";
import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { Fragment, useCallback } from "react";

import { msgError, msgInfo, msgSuccess, msgWarning } from "./notifications";

import { ToastBox } from "components/toast-box";
import { render } from "mocks";

const messages: Record<TNotificationVariant, string> = {
  error: "Error message",
  info: "Info message",
  success: "Success message",
  warning: "Warning message",
};

const Wrapper = ({
  variant = "error",
}: Readonly<{
  variant: INotificationProps["variant"];
}>): JSX.Element => {
  const handleClick = useCallback((): void => {
    const functions: Record<TNotificationVariant, (message: string) => void> = {
      error: msgError,
      info: msgInfo,
      success: msgSuccess,
      warning: msgWarning,
    };
    functions[variant](messages[variant]);
  }, [variant]);

  return (
    <Fragment>
      <ToastBox autoClose={5000} position={"top-right"} />
      <button onClick={handleClick}>{"Show notification"}</button>
    </Fragment>
  );
};

const testCases: TNotificationVariant[] = [
  "error",
  "info",
  "success",
  "warning",
];

describe("notifications", (): void => {
  const clickButton = async (): Promise<void> => {
    const button = screen.getByText("Show notification");
    await userEvent.click(button);
  };

  it.each(testCases)(
    "should render %s notification",
    async (arg): Promise<void> => {
      expect.hasAssertions();

      render(<Wrapper variant={arg} />);
      await clickButton();
      await waitFor((): void => {
        expect(screen.getByText(messages[arg])).toBeInTheDocument();
      });
      await clickButton();
      await waitFor((): void => {
        expect(screen.queryAllByText(messages[arg])).toHaveLength(1);
      });
    },
  );
});
