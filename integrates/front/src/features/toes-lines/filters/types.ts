import type { GetToeLinesQueryVariables } from "gql/graphql";

interface IToElineFilters {
  groupName: string;
  setBackFilters: React.Dispatch<
    React.SetStateAction<Partial<GetToeLinesQueryVariables>>
  >;
}

export type { IToElineFilters };
