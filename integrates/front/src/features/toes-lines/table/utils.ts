import type { PureAbility } from "@casl/ability";
import type { ColumnDef } from "@tanstack/react-table";

import { filterColumns, formatSortsPriorityFactor } from "../utils";
import { filterDate } from "components/table/utils";
import { useAttackedLinesFormatter as editableAttackedLinesFormatter } from "pages/group/surface/lines/formatters/editable-attacked-lines";
import { formatLoc } from "pages/group/surface/lines/formatters/loc";
import { formatSortsSuggestions } from "pages/group/surface/lines/formatters/sorts-suggestions";
import type {
  ISortsSuggestionAttr,
  IToeLinesData,
} from "pages/group/surface/lines/types";
import { formatDate } from "utils/date";
import {
  formatBoolean,
  formatPercentage,
  formatStatus,
  jsxStatus,
} from "utils/format-helpers";
import { translate } from "utils/translations/translate";

const getColumns = ({
  canUpdateAttackedLines,
  handleUpdateAttackedLines,
  isInternal,
  permissions,
  setIsSortsSuggestionsModalOpen,
  setSelectedToeLinesData,
  setSelectedToeLinesSortsSuggestions,
  toggleLocModal,
}: {
  canUpdateAttackedLines: boolean;
  canSeeCoverage: boolean;
  canSeeDaysToAttack: boolean;
  handleUpdateAttackedLines: (
    rootId: string,
    filename: string,
    comments: string,
    attackedLines: number,
  ) => Promise<void>;
  isInternal: boolean;
  permissions: PureAbility<string>;
  setIsSortsSuggestionsModalOpen: (value: boolean) => void;
  setSelectedToeLinesData: React.Dispatch<
    React.SetStateAction<IToeLinesData[]>
  >;
  setSelectedToeLinesSortsSuggestions: (
    value: React.SetStateAction<ISortsSuggestionAttr[] | undefined>,
  ) => void;
  toggleLocModal: () => void;
}): ColumnDef<IToeLinesData>[] => {
  const columns: ColumnDef<IToeLinesData>[] = [
    {
      accessorKey: "rootNickname",
      header: translate.t("group.toe.lines.root"),
    },
    {
      accessorKey: "filename",
      header: translate.t("group.toe.lines.filename"),
    },
    {
      accessorKey: "loc",
      cell: (cell): JSX.Element | string =>
        formatLoc(
          cell.row.original,
          setSelectedToeLinesData,
          toggleLocModal,
          isInternal,
        ),
      header: translate.t("group.toe.lines.loc"),
    },
    {
      accessorFn: (row: IToeLinesData): string =>
        formatStatus(row.hasVulnerabilities),
      cell: (cell): JSX.Element => jsxStatus(String(cell.getValue())),
      header: translate.t("group.toe.lines.status"),
      id: "hasVulnerabilities",
    },
    {
      accessorKey: "modifiedDate",
      cell: (cell): string => formatDate(cell.getValue<string>()),
      filterFn: filterDate,
      header: translate.t("group.toe.lines.modifiedDate"),
    },
    {
      accessorKey: "lastCommit",
      header: translate.t("group.toe.lines.lastCommit"),
    },
    {
      accessorKey: "lastAuthor",
      header: translate.t("group.toe.lines.lastAuthor"),
    },
    {
      accessorKey: "seenAt",
      cell: (cell): string => formatDate(cell.getValue<string>()),
      filterFn: filterDate,
      header: translate.t("group.toe.lines.seenAt"),
    },
    {
      accessorKey: "sortsPriorityFactor",
      cell: (cell): string =>
        formatSortsPriorityFactor(Number(cell.getValue())),
      header: translate.t("group.toe.lines.sortsPriorityFactor"),
    },
    {
      accessorFn: (row: IToeLinesData): string => formatBoolean(row.bePresent),
      header: translate.t("group.toe.lines.bePresent"),
      id: "bePresent",
    },
    {
      accessorFn: (row): number => row.coverage * 100,
      cell: (cell): string =>
        formatPercentage(Number(cell.row.original.coverage), true),
      header: translate.t("group.toe.lines.coverage"),
      id: "coverage",
    },
    {
      accessorKey: "attackedLines",
      cell: (cell): JSX.Element | string =>
        editableAttackedLinesFormatter(
          canUpdateAttackedLines,
          handleUpdateAttackedLines,
          cell.row.original,
        ),
      header: translate.t("group.toe.lines.attackedLines"),
      id: "attackedLines",
    },
    {
      accessorKey: "daysToAttack",
      header: translate.t("group.toe.lines.daysToAttack"),
      id: "daysToAttack",
    },
    {
      accessorKey: "attackedAt",
      cell: (cell): string => formatDate(cell.getValue<string>()),
      filterFn: filterDate,
      header: translate.t("group.toe.lines.attackedAt"),
      id: "attackedAt",
    },
    {
      accessorKey: "attackedBy",
      header: translate.t("group.toe.lines.attackedBy"),
      id: "attackedBy",
      meta: { filterType: "select" },
    },
    {
      accessorKey: "firstAttackAt",
      cell: (cell): string => formatDate(cell.getValue<string>()),
      filterFn: filterDate,
      header: translate.t("group.toe.lines.firstAttackAt"),
      id: "firstAttackAt",
    },
    {
      accessorKey: "comments",
      header: translate.t("group.toe.lines.comments"),
      id: "comments",
    },
    {
      accessorKey: "sortsSuggestions",
      cell: (cell): JSX.Element =>
        formatSortsSuggestions(
          cell.getValue<ISortsSuggestionAttr[]>(),
          setSelectedToeLinesSortsSuggestions,
          setIsSortsSuggestionsModalOpen,
        ),
      header: translate.t("group.toe.lines.sortsSuggestions"),
      id: "sortsSuggestions",
    },
    {
      accessorKey: "bePresentUntil",
      cell: (cell): string => formatDate(cell.getValue<string>()),
      filterFn: filterDate,
      header: translate.t("group.toe.lines.bePresentUntil"),
      id: "bePresentUntil",
    },
  ];

  return filterColumns(columns, isInternal, permissions);
};

export { getColumns };
