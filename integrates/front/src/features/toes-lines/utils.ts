import type { PureAbility } from "@casl/ability";

const formatSortsPriorityFactor = (sortsPriorityFactor: number): string =>
  sortsPriorityFactor >= 0 ? `${sortsPriorityFactor.toString()} %` : "n/a";

function filterColumns<T extends { id?: string }>(
  data: T[],
  isInternal: boolean,
  permissions: PureAbility<string>,
): T[] {
  const canGetAttackedAt = permissions.can(
    "integrates_api_resolvers_toe_lines_attacked_at_resolve",
  );
  const canGetAttackedBy = permissions.can(
    "integrates_api_resolvers_toe_lines_attacked_by_resolve",
  );
  const canGetAttackedLines = permissions.can(
    "integrates_api_resolvers_toe_lines_attacked_lines_resolve",
  );
  const canGetBePresentUntil = permissions.can(
    "integrates_api_resolvers_toe_lines_be_present_until_resolve",
  );
  const canGetComments = permissions.can(
    "integrates_api_resolvers_toe_lines_comments_resolve",
  );
  const canGetFirstAttackAt = permissions.can(
    "integrates_api_resolvers_toe_lines_first_attack_at_resolve",
  );
  const canSeeCoverage = permissions.can("see_toe_lines_coverage");
  const canSeeDaysToAttack = permissions.can("see_toe_lines_days_to_attack");

  return data.filter((filter): boolean => {
    const { id } = filter;

    switch (id) {
      case "coverage":
        return isInternal && canSeeCoverage && canGetAttackedLines;
      case "attackedLines":
        return isInternal && canGetAttackedLines;
      case "daysToAttack":
        return isInternal && canSeeDaysToAttack && canGetAttackedAt;
      case "attackedAt":
        return isInternal && canGetAttackedAt;
      case "attackedBy":
        return isInternal && canGetAttackedBy;
      case "firstAttackAt":
        return isInternal && canGetFirstAttackAt;
      case "comments":
        return isInternal && canGetComments;
      case "sortsSuggestions":
        return isInternal;
      case "bePresentUntil":
        return isInternal && canGetBePresentUntil;
      case undefined:
      default:
        return true;
    }
  });
}

export { formatSortsPriorityFactor, filterColumns };
