import {
  CloudImage,
  ComboBox,
  Container,
  Form,
  InnerForm,
  Modal,
  Text,
  useModal,
} from "@fluidattacks/design";
import mixpanel from "mixpanel-browser";
import { Fragment, useCallback, useState } from "react";
import * as React from "react";
import { PopupModal } from "react-calendly";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

import type { ICheckDocumentationForm, ITalkToHackerModalProps } from "./types";

import { useCalendly } from "hooks";
import { useAudit } from "hooks/use-audit";
import { openUrl } from "utils/resource-helpers";

const TalkToHackerModal: React.FC<ITalkToHackerModalProps> = ({
  closeTalkToHackerModal,
}: ITalkToHackerModalProps): JSX.Element => {
  const theme = useTheme();
  const { t } = useTranslation();
  const { isAdvancedActive, widgetProps } = useCalendly();
  const modalRef = useModal("talk-to-hacker-modal");
  const root = document.getElementById("root");
  const prefix = "navbar.help.options.expert.documentation.";

  const { addAuditEvent } = useAudit();
  const [isWidgetOpen, setWidgetOpen] = useState(false);
  const [checkDoc, setCheckDoc] = useState("");
  const openWidget = useCallback((): void => {
    setWidgetOpen(true);
    mixpanel.track("ScheduleTalkToAHacker");
    addAuditEvent("TalkToAHacker", "unknown");
  }, [addAuditEvent]);
  const closeWidget = useCallback((): void => {
    setWidgetOpen(false);
    closeTalkToHackerModal();
  }, [closeTalkToHackerModal]);

  const handleSubmit = useCallback(
    (values: ICheckDocumentationForm): void => {
      if (values.checkDocumentation === "NO") {
        openUrl(
          "https://help.fluidattacks.com/portal/en/kb/criteria/vulnerabilities",
        );
        closeTalkToHackerModal();
      } else {
        openWidget();
      }
    },
    [openWidget, closeTalkToHackerModal],
  );

  const handleChange = useCallback((value: string): void => {
    setCheckDoc(value);
  }, []);

  return (
    <Modal
      id={"scheduleTalkToAHacker"}
      modalRef={{ ...modalRef, close: closeTalkToHackerModal, isOpen: true }}
      size={"sm"}
      title={t("navbar.help.options.expert.title")}
    >
      <Form
        cancelButton={{ onClick: closeTalkToHackerModal }}
        confirmButton={{
          label:
            checkDoc === "YES"
              ? t(`${prefix}btnConfirm`)
              : t(`${prefix}btnDeny`),
        }}
        defaultValues={{
          checkDocumentation: "",
        }}
        onSubmit={handleSubmit}
      >
        <InnerForm<ICheckDocumentationForm>>
          {({ watch, control }): JSX.Element => {
            const checkDocumentation = watch("checkDocumentation");

            return (
              <Fragment>
                <Text color={theme.palette.gray[800]} mb={0.25} size={"sm"}>
                  {t(`${prefix}check`)}
                </Text>
                <ComboBox
                  control={control}
                  id={"check"}
                  items={[
                    {
                      name: t(`${prefix}deny`),
                      value: "NO",
                    },
                    {
                      name: t(`${prefix}confirm`),
                      value: "YES",
                    },
                  ]}
                  name={"checkDocumentation"}
                  onChange={handleChange}
                  placeholder={t(`${prefix}placeholder`)}
                  required={true}
                />
                {checkDocumentation === "YES" ? (
                  <Fragment>
                    {isAdvancedActive && root ? (
                      <PopupModal
                        onModalClose={closeWidget}
                        open={isWidgetOpen}
                        prefill={widgetProps.prefill}
                        rootElement={root}
                        url={widgetProps.url}
                      />
                    ) : undefined}
                  </Fragment>
                ) : undefined}
                {checkDocumentation === "NO" ? (
                  <Container display={"block"} justify={"center"} mt={1}>
                    <CloudImage
                      publicId={"integrates/resources/documentation"}
                      width={"100%"}
                    />
                    <Text mt={1} size={"sm"} textAlign={"start"}>
                      {t(`${prefix}descriptionDeny`)}
                    </Text>
                  </Container>
                ) : undefined}
              </Fragment>
            );
          }}
        </InnerForm>
      </Form>
    </Modal>
  );
};

export { TalkToHackerModal };
