interface ITalkToHackerProps {
  isWidgetOpen: boolean;
  handleWidget: () => void;
}

interface ITalkToHackerModalProps {
  closeTalkToHackerModal: () => void;
}

interface ICheckDocumentationForm {
  checkDocumentation: string;
}

export type {
  ITalkToHackerProps,
  ITalkToHackerModalProps,
  ICheckDocumentationForm,
};
