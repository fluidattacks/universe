import { graphql } from "gql";

const GET_VULNERABILITIES_URL = graphql(`
  query GetOrgVulnerabilitiesUrl(
    $identifier: String!
    $verificationCode: String
  ) {
    organization(organizationId: $identifier) {
      name
      vulnerabilitiesUrl(verificationCode: $verificationCode)
    }
  }
`);

export { GET_VULNERABILITIES_URL };
