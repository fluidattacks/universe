/* eslint-disable max-lines */

import { Container, CustomThemeProvider } from "@fluidattacks/design";
import { Col, Row } from "@fluidattacks/design";
import { useCallback, useMemo } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { ChartsExtras } from "./extras";
import { PanelCollapseHeader } from "./styles";
import type { IChartsProps, TEntity } from "./types";

import { Graphic } from "graphics/components/graphic";

export const Charts: React.FC<IChartsProps> = ({
  bgChange,
  entity,
  reportMode,
  subject,
}): JSX.Element => {
  const { t } = useTranslation();
  const graphInfoLink = "https://help.fluidattacks.com/portal/en/kb/articles/";

  const doesEntityMatch = useCallback(
    (entities: TEntity[]): boolean => entities.includes(entity),
    [entity],
  );
  const entityWidth = useMemo(
    (): number => ({ group: 33, organization: 50, portfolio: 50 })[entity],
    [entity],
  );
  const reportModeClassName = useMemo((): string => {
    return reportMode ? "report-mode" : "";
  }, [reportMode]);
  const backgroundColorClassName = useMemo((): string => {
    return bgChange ? "report-bg-change" : "";
  }, [bgChange]);
  const reportClassName = useMemo((): string => {
    return `${reportModeClassName} ${backgroundColorClassName}`.trim();
  }, [reportModeClassName, backgroundColorClassName]);

  const paddingLarge = 1.25;
  const padding = useMemo(
    (): number => (doesEntityMatch(["organization"]) ? paddingLarge : 0),
    [doesEntityMatch],
  );

  return (
    <React.StrictMode>
      <CustomThemeProvider>
        <Container px={padding > 0 ? 0 : paddingLarge}>
          {reportMode ? undefined : (
            <ChartsExtras
              bgChange={bgChange}
              entity={entity}
              reportMode={reportMode}
              subject={subject}
            />
          )}
          <Container px={paddingLarge} py={paddingLarge}>
            <div className={reportClassName}>
              <Row justify={"center"}>
                <Col>
                  <Graphic
                    bsHeight={320}
                    className={"g1"}
                    documentName={"riskOverTimeCvssf"}
                    documentType={"stackedBarChart"}
                    entity={entity}
                    generatorName={"generic"}
                    generatorType={"stackedBarChart"}
                    infoLink={`${graphInfoLink}common-analytics`}
                    reportMode={reportMode}
                    subject={subject}
                    title={t("analytics.stackedBarChart.riskOverTime.title")}
                  />
                </Col>
              </Row>
              <Row justify={"between"}>
                <Col>
                  <Graphic
                    bsHeight={80}
                    className={"g3"}
                    documentName={"vulnerabilitiesRemediationCreated"}
                    documentType={"textBox"}
                    entity={entity}
                    generatorName={"generic"}
                    generatorType={"textBox"}
                    infoLink={`${graphInfoLink}common-analytics#Sprint_exposure_increment`}
                    reportMode={reportMode}
                    subject={subject}
                    title={t("analytics.textBox.remediationCreated.title")}
                  />
                </Col>
                <Col>
                  <Graphic
                    bsHeight={80}
                    className={"g3"}
                    documentName={"vulnerabilitiesRemediationSolved"}
                    documentType={"textBox"}
                    entity={entity}
                    generatorName={"generic"}
                    generatorType={"textBox"}
                    infoLink={`${graphInfoLink}common-analytics#Sprint_exposure_decrement`}
                    reportMode={reportMode}
                    subject={subject}
                    title={t("analytics.textBox.remediationSolved.title")}
                  />
                </Col>
                <Col>
                  <Graphic
                    bsHeight={80}
                    className={"g3"}
                    documentName={"vulnerabilitiesRemediationRemediated"}
                    documentType={"textBox"}
                    entity={entity}
                    generatorName={"generic"}
                    generatorType={"textBox"}
                    infoLink={`${graphInfoLink}common-analytics#Sprint_exposure_change_overall`}
                    reportMode={reportMode}
                    subject={subject}
                    title={t("analytics.textBox.remediationRemediated.title")}
                  />
                </Col>
              </Row>
              <Row justify={"center"}>
                <Col>
                  <Graphic
                    bsHeight={320}
                    className={"g1"}
                    documentName={"distributionOverTimeCvssf"}
                    documentType={"stackedBarChart"}
                    entity={entity}
                    generatorName={"generic"}
                    generatorType={"stackedBarChart"}
                    infoLink={`${graphInfoLink}common-analytics`}
                    reportMode={reportMode}
                    subject={subject}
                    title={t(
                      "analytics.stackedBarChart.distributionOverTimeCvssf.title",
                    )}
                  />
                </Col>
              </Row>
              <Row justify={"center"}>
                <Col>
                  <Graphic
                    bsHeight={320}
                    className={"g1"}
                    documentName={"exposureBenchmarkingCvssf"}
                    documentType={"barChart"}
                    entity={entity}
                    generatorName={"generic"}
                    generatorType={"barChart"}
                    infoLink={`${graphInfoLink}common-analytics#Exposure_benchmark`}
                    reportMode={reportMode}
                    subject={subject}
                    title={t("analytics.barChart.exposureBenchmarkingCvssf")}
                  />
                </Col>
              </Row>
              <Row justify={"center"}>
                <Col>
                  <Graphic
                    bsHeight={320}
                    className={"g1"}
                    documentName={"exposureTrendsByCategories"}
                    documentType={"barChart"}
                    entity={entity}
                    generatorName={"generic"}
                    generatorType={"barChart"}
                    infoLink={`${graphInfoLink}common-analytics#Exposure_trends_by_vulnerability_category`}
                    reportMode={reportMode}
                    shouldDisplayAll={false}
                    subject={subject}
                    title={t("analytics.barChart.exposureTrendsByCategories")}
                  />
                </Col>
              </Row>
              <Row justify={"center"}>
                <Col>
                  <Graphic
                    bsHeight={80}
                    className={"g3"}
                    documentName={"daysSinceLastRemediation"}
                    documentType={"textBox"}
                    entity={entity}
                    generatorName={"generic"}
                    generatorType={"textBox"}
                    infoLink={`${graphInfoLink}common-analytics#Days_since_last_remediation`}
                    reportMode={reportMode}
                    subject={subject}
                    title={t(
                      "analytics.textBox.daysSinceLastRemediation.title",
                    )}
                  />
                </Col>
                <Col>
                  <Graphic
                    bsHeight={80}
                    className={"g3"}
                    documentName={"meanTimeToReattack"}
                    documentType={"textBox"}
                    entity={entity}
                    generatorName={"generic"}
                    generatorType={"textBox"}
                    infoLink={`${graphInfoLink}common-analytics#Mean_time_to_request_reattacks`}
                    reportMode={reportMode}
                    subject={subject}
                    title={t("analytics.textBox.meanTimeToReattack.title")}
                  />
                </Col>
                <Col>
                  <Graphic
                    bsHeight={80}
                    className={"g3"}
                    documentName={"findingsBeingReattacked"}
                    documentType={"textBox"}
                    entity={entity}
                    generatorName={"generic"}
                    generatorType={"textBox"}
                    infoLink={`${graphInfoLink}common-analytics#Vulnerabilities_being_re_attacked`}
                    reportMode={reportMode}
                    subject={subject}
                    title={t("analytics.textBox.findingsBeingReattacked.title")}
                  />
                </Col>
                <Col>
                  <Graphic
                    bsHeight={80}
                    className={"g3"}
                    documentName={"daysUntilZeroExposition"}
                    documentType={"textBox"}
                    entity={entity}
                    generatorName={"generic"}
                    generatorType={"textBox"}
                    infoLink={`${graphInfoLink}common-analytics#Days_until_zero_exposure`}
                    reportMode={reportMode}
                    subject={subject}
                    title={t("analytics.textBox.daysUntilZeroExposition.title")}
                  />
                </Col>
              </Row>
            </div>
            {doesEntityMatch(["organization", "portfolio"]) ? (
              <div className={reportClassName}>
                <Row justify={"center"}>
                  <Col>
                    <Graphic
                      bsHeight={320}
                      className={"g1"}
                      documentName={"cvssfBenchmarking"}
                      documentType={"stackedBarChart"}
                      entity={entity}
                      generatorName={"generic"}
                      generatorType={"stackedBarChart"}
                      infoLink={`${graphInfoLink}${entity}-analytics#Remediation_rate_benchmark`}
                      reportMode={reportMode}
                      subject={subject}
                      title={t(
                        "analytics.stackedBarChart.cvssfBenchmarking.title",
                      )}
                    />
                  </Col>
                </Row>
              </div>
            ) : undefined}
            <div className={reportClassName}>
              <Row justify={"center"}>
                <Col>
                  <Graphic
                    bsHeight={320}
                    className={"g1"}
                    documentName={"mttrBenchmarkingCvssf"}
                    documentType={"barChart"}
                    entity={entity}
                    generatorName={"generic"}
                    generatorType={"barChart"}
                    infoLink={`${graphInfoLink}common-analytics`}
                    reportMode={reportMode}
                    subject={subject}
                    title={t("analytics.barChart.mttrBenchmarking.title")}
                  />
                </Col>
              </Row>
              <Row justify={"center"}>
                <Col>
                  <Graphic
                    bsHeight={320}
                    className={"g1"}
                    documentName={"meanTimeToRemediateCvssf"}
                    documentType={"barChart"}
                    entity={entity}
                    generatorName={"generic"}
                    generatorType={"barChart"}
                    infoLink={`${graphInfoLink}common-analytics`}
                    reportMode={reportMode}
                    subject={subject}
                    title={t("tagIndicator.meanRemediate")}
                  />
                </Col>
              </Row>
            </div>
            {doesEntityMatch(["organization", "portfolio"]) ? (
              <div className={reportClassName}>
                <Row>
                  <Col>
                    <Graphic
                      bsHeight={320}
                      className={"g1"}
                      documentName={"remediatedAcceptedGroup"}
                      documentType={"stackedBarChart"}
                      entity={entity}
                      generatorName={"generic"}
                      generatorType={"stackedBarChart"}
                      infoLink={`${graphInfoLink}${entity}-analytics#Distribution_of_vulnerabilities_by_group`}
                      reportMode={reportMode}
                      subject={subject}
                      title={t("tagIndicator.remediatedAcceptedVuln")}
                    />
                  </Col>
                </Row>
              </div>
            ) : undefined}
            <div className={reportClassName}>
              <Row>
                <Col>
                  <Graphic
                    bsHeight={320}
                    className={"g1"}
                    documentName={"acceptedVulnerabilitiesBySeverity"}
                    documentType={"stackedBarChart"}
                    entity={entity}
                    generatorName={"generic"}
                    generatorType={"stackedBarChart"}
                    infoLink={`${graphInfoLink}common-analytics#Accepted_vulnerabilities_by_CVSS_severity`}
                    reportMode={reportMode}
                    subject={subject}
                    title={t("tagIndicator.acceptedVulnerabilitiesBySeverity")}
                  />
                </Col>
              </Row>
              <Row>
                <Col lg={50} md={50} sm={50}>
                  <Graphic
                    bsHeight={160}
                    className={"g2"}
                    documentName={"assignedVulnerabilities"}
                    documentType={"pieChart"}
                    entity={entity}
                    generatorName={"generic"}
                    generatorType={"pieChart"}
                    infoLink={`${graphInfoLink}common-analytics#Vulnerabilities_by_assignment`}
                    reportMode={reportMode}
                    subject={subject}
                    title={t("tagIndicator.assignedVulnerabilities")}
                  />
                </Col>
                <Col lg={50} md={50} sm={50}>
                  <Graphic
                    bsHeight={160}
                    className={"g2"}
                    documentName={"assignedVulnerabilitiesStatus"}
                    documentType={"pieChart"}
                    entity={entity}
                    generatorName={"generic"}
                    generatorType={"c3"}
                    infoLink={`${graphInfoLink}common-analytics#Status_of_assigned_vulnerabilities`}
                    reportMode={reportMode}
                    subject={subject}
                    title={t("tagIndicator.assignedVulnerabilitiesStatus")}
                  />
                </Col>
              </Row>
            </div>
            {doesEntityMatch(["organization", "portfolio"]) ? (
              <div className={reportClassName}>
                <Row>
                  <Col lg={50} md={50} sm={50}>
                    <Graphic
                      bsHeight={160}
                      className={"g2"}
                      documentName={"openVulnerabilitiesStatus"}
                      documentType={"pieChart"}
                      entity={entity}
                      generatorName={"generic"}
                      generatorType={"pieChart"}
                      infoLink={`${graphInfoLink}${entity}-analytics#Open_vulnerabilities_by_group`}
                      reportMode={reportMode}
                      subject={subject}
                      title={t("tagIndicator.openVulnsGroups")}
                    />
                  </Col>
                  <Col lg={50} md={50} sm={50}>
                    <Graphic
                      bsHeight={160}
                      className={"g2"}
                      documentName={"vulnerabilitiesWithUndefinedTreatment"}
                      documentType={"pieChart"}
                      entity={entity}
                      generatorName={"generic"}
                      generatorType={"pieChart"}
                      infoLink={`${graphInfoLink}${entity}-analytics#Undefined_treatment_by_group`}
                      reportMode={reportMode}
                      subject={subject}
                      title={t("tagIndicator.undefinedTitle")}
                    />
                  </Col>
                </Row>
              </div>
            ) : undefined}
            {doesEntityMatch(["group"]) ? (
              <Row>
                <Col>
                  <Graphic
                    bsHeight={160}
                    className={"g2"}
                    documentName={"vulnerabilitiesClosingReason"}
                    documentType={"pieChart"}
                    entity={entity}
                    generatorName={"generic"}
                    generatorType={"pieChart"}
                    infoLink={`${graphInfoLink}common-analytics`}
                    reportMode={reportMode}
                    subject={subject}
                    title={t("analytics.pieChart.closingReason.title")}
                  />
                </Col>
              </Row>
            ) : undefined}
            <div className={reportClassName}>
              <Row>
                <Col>
                  <Graphic
                    bsHeight={320}
                    className={"g1"}
                    documentName={"topVulnerabilitiesCvssf"}
                    documentType={"barChart"}
                    entity={entity}
                    generatorName={"generic"}
                    generatorType={"barChart"}
                    infoLink={`${graphInfoLink}common-analytics`}
                    reportMode={reportMode}
                    subject={subject}
                    title={t("analytics.barChart.topVulnerabilities.title")}
                  />
                </Col>
              </Row>
            </div>
            {doesEntityMatch(["organization", "portfolio"]) ? (
              <div className={reportClassName}>
                <Row>
                  <Col>
                    <Graphic
                      bsHeight={320}
                      className={"g1"}
                      documentName={"exposedByGroups"}
                      documentType={"barChart"}
                      entity={entity}
                      generatorName={"generic"}
                      generatorType={"barChart"}
                      infoLink={`${graphInfoLink}${entity}-analytics#Exposure_by_group`}
                      reportMode={reportMode}
                      subject={subject}
                      title={t("analytics.barChart.exposureByGroups")}
                    />
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <Graphic
                      bsHeight={320}
                      className={"g1"}
                      documentName={"openFindings"}
                      documentType={"barChart"}
                      entity={entity}
                      generatorName={"generic"}
                      generatorType={"barChart"}
                      infoLink={`${graphInfoLink}${entity}-analytics#Open_vulnerability_types_by_group`}
                      reportMode={reportMode}
                      subject={subject}
                      title={t("tagIndicator.openFindingsGroup")}
                    />
                  </Col>
                </Row>
              </div>
            ) : undefined}
            <div className={reportClassName}>
              <Row>
                <Col lg={50} md={50} sm={50}>
                  <Graphic
                    bsHeight={160}
                    className={"g2"}
                    documentName={"treatmentCvssf"}
                    documentType={"pieChart"}
                    entity={entity}
                    generatorName={"generic"}
                    generatorType={"pieChart"}
                    infoLink={`${graphInfoLink}common-analytics#Vulnerabilities_treatment`}
                    reportMode={reportMode}
                    subject={subject}
                    title={t("analytics.pieChart.treatment.title")}
                  />
                </Col>
                <Col lg={50} md={50} sm={50}>
                  <Graphic
                    bsHeight={160}
                    className={"g2"}
                    documentName={"reportTechnique"}
                    documentType={"pieChart"}
                    entity={entity}
                    generatorName={"generic"}
                    generatorType={"pieChart"}
                    infoLink={`${graphInfoLink}common-analytics#Report_technique`}
                    reportMode={reportMode}
                    subject={subject}
                    title={t("tagIndicator.reportTechnique")}
                  />
                </Col>
              </Row>
              <Row>
                <Col lg={50} md={50} sm={50}>
                  <Graphic
                    bsHeight={160}
                    className={"g2"}
                    documentName={"rootResources"}
                    documentType={"pieChart"}
                    entity={entity}
                    generatorName={"generic"}
                    generatorType={"pieChart"}
                    infoLink={`${graphInfoLink}common-analytics#Active_resources_distribution`}
                    reportMode={reportMode}
                    subject={subject}
                    title={t("analytics.pieChart.resources.title")}
                  />
                </Col>
                {doesEntityMatch(["organization", "portfolio"]) ? (
                  <Col lg={50} md={50} sm={50}>
                    <Graphic
                      bsHeight={160}
                      className={"g2"}
                      documentName={"totalVulnerabilitiesStatus"}
                      documentType={"pieChart"}
                      entity={entity}
                      generatorName={"generic"}
                      generatorType={"pieChart"}
                      infoLink={`${graphInfoLink}${entity}-analytics#Vulnerabilities_by_group`}
                      reportMode={reportMode}
                      subject={subject}
                      title={t("tagIndicator.vulnsGroups")}
                    />
                  </Col>
                ) : undefined}
                {doesEntityMatch(["group"]) ? (
                  <Col lg={50} md={50} sm={50}>
                    <Graphic
                      bsHeight={160}
                      className={"g2"}
                      documentName={"availability"}
                      documentType={"pieChart"}
                      entity={entity}
                      generatorName={"generic"}
                      generatorType={"c3"}
                      infoLink={`${graphInfoLink}group-analytics#Group_availability`}
                      reportMode={reportMode}
                      subject={subject}
                      title={t("analytics.pieChart.availability.title")}
                    />
                  </Col>
                ) : undefined}
              </Row>
              <Row>
                <Col lg={entityWidth} md={entityWidth} sm={50}>
                  <Graphic
                    bsHeight={80}
                    className={"g3"}
                    documentName={"totalFindings"}
                    documentType={"textBox"}
                    entity={entity}
                    generatorName={"generic"}
                    generatorType={"textBox"}
                    infoLink={`${graphInfoLink}common-analytics#Total_types`}
                    reportMode={reportMode}
                    subject={subject}
                    title={t("analytics.textBox.totalTypes.title")}
                  />
                </Col>
                <Col lg={entityWidth} md={entityWidth} sm={50}>
                  <Graphic
                    bsHeight={80}
                    className={"g3"}
                    documentName={"totalVulnerabilities"}
                    documentType={"textBox"}
                    entity={entity}
                    generatorName={"generic"}
                    generatorType={"textBox"}
                    infoLink={`${graphInfoLink}common-analytics#Total_vulnerabilities`}
                    reportMode={reportMode}
                    subject={subject}
                    title={t("analytics.textBox.totalVulnerabilities.title")}
                  />
                </Col>
                {doesEntityMatch(["group"]) ? (
                  <Col lg={33} md={33} sm={50}>
                    <Graphic
                      bsHeight={80}
                      className={"g3"}
                      documentName={"exclusionsByGroup"}
                      documentType={"textBox"}
                      entity={entity}
                      generatorName={"generic"}
                      generatorType={"textBox"}
                      infoLink={`${graphInfoLink}common-analytics#Total_exclusions`}
                      reportMode={reportMode}
                      subject={subject}
                      title={t("analytics.textBox.totalExclusions.title")}
                    />
                  </Col>
                ) : undefined}
              </Row>
              {doesEntityMatch(["group"]) ? (
                <Row>
                  <Col>
                    <Graphic
                      bsHeight={320}
                      className={"g1"}
                      documentName={"exclusionsByRoot"}
                      documentType={"barChart"}
                      entity={entity}
                      generatorName={"generic"}
                      generatorType={"barChart"}
                      infoLink={`${graphInfoLink}group-analytics#Exclusions_by_root`}
                      reportMode={reportMode}
                      subject={subject}
                      title={t("analytics.barChart.exclusionsByRoot.title")}
                    />
                  </Col>
                </Row>
              ) : undefined}
              <Row>
                <Col>
                  <Graphic
                    bsHeight={320}
                    className={"g1"}
                    documentName={"vulnerabilitiesByTag"}
                    documentType={"barChart"}
                    entity={entity}
                    generatorName={"generic"}
                    generatorType={"barChart"}
                    infoLink={`${graphInfoLink}common-analytics#Vulnerabilities_by_tag`}
                    reportMode={reportMode}
                    subject={subject}
                    title={t("tagIndicator.vulnerabilitiesByTag")}
                  />
                </Col>
              </Row>
              <Row>
                <Col>
                  <Graphic
                    bsHeight={320}
                    className={"g1"}
                    documentName={"vulnerabilitiesByLevel"}
                    documentType={"barChart"}
                    entity={entity}
                    generatorName={"generic"}
                    generatorType={"barChart"}
                    infoLink={`${graphInfoLink}common-analytics#Vulnerabilities_by_level`}
                    reportMode={reportMode}
                    subject={subject}
                    title={t("tagIndicator.vulnerabilitiesByLevel")}
                  />
                </Col>
              </Row>
              <Row>
                <Col>
                  <Graphic
                    bsHeight={320}
                    className={"g1"}
                    documentName={"acceptedVulnerabilitiesByUser"}
                    documentType={"stackedBarChart"}
                    entity={entity}
                    generatorName={"generic"}
                    generatorType={"stackedBarChart"}
                    infoLink={`${graphInfoLink}common-analytics#Accepted_vulnerabilities_by_user`}
                    reportMode={reportMode}
                    subject={subject}
                    title={t("tagIndicator.acceptedVulnerabilitiesByUser")}
                  />
                </Col>
              </Row>
            </div>
            {doesEntityMatch(["organization", "portfolio"]) ? (
              <div className={reportClassName}>
                <Row>
                  <Col>
                    <Graphic
                      bsHeight={320}
                      className={"g1"}
                      documentName={"eventualities"}
                      documentType={"barChart"}
                      entity={entity}
                      generatorName={"generic"}
                      generatorType={"barChart"}
                      infoLink={`${graphInfoLink}${entity}-analytics#Unsolved_events_by_group`}
                      reportMode={reportMode}
                      subject={subject}
                      title={t("analytics.barChart.eventualities")}
                    />
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <Graphic
                      bsHeight={320}
                      className={"g1"}
                      documentName={"finding"}
                      documentType={"barChart"}
                      entity={entity}
                      generatorName={"generic"}
                      generatorType={"barChart"}
                      infoLink={`${graphInfoLink}${entity}-analytics#Vulnerability_types_by_group`}
                      reportMode={reportMode}
                      subject={subject}
                      title={t("tagIndicator.findingsGroup")}
                    />
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <Graphic
                      bsHeight={320}
                      className={"g1"}
                      documentName={"topOldestFindings"}
                      documentType={"barChart"}
                      entity={entity}
                      generatorName={"generic"}
                      generatorType={"barChart"}
                      infoLink={`${graphInfoLink}${entity}-analytics#Oldest_vulnerability_types`}
                      reportMode={reportMode}
                      subject={subject}
                      title={t("tagIndicator.topOldestFindings")}
                    />
                  </Col>
                </Row>
              </div>
            ) : undefined}
            <div className={reportClassName}>
              <Row>
                <Col>
                  <Graphic
                    bsHeight={320}
                    className={"g1"}
                    documentName={"assignedVulnerabilitiesCvssf"}
                    documentType={"stackedBarChart"}
                    entity={entity}
                    generatorName={"generic"}
                    generatorType={"stackedBarChart"}
                    infoLink={`${graphInfoLink}common-analytics`}
                    reportMode={reportMode}
                    subject={subject}
                    title={t(
                      "analytics.stackedBarChart.assignedVulnerabilities.title",
                    )}
                  />
                </Col>
              </Row>
              <Row>
                <Col>
                  <Graphic
                    bsHeight={320}
                    className={"g1"}
                    documentName={"touchedFiles"}
                    documentType={"barChart"}
                    entity={entity}
                    generatorName={"generic"}
                    generatorType={"barChart"}
                    infoLink={`${graphInfoLink}common-analytics#Files_with_open_vulnerabilities_in_the_last_20_weeks`}
                    reportMode={reportMode}
                    subject={subject}
                    title={t("analytics.barChart.touchedFiles.title")}
                  />
                </Col>
              </Row>
            </div>
            {doesEntityMatch(["organization", "portfolio"]) ? (
              <div className={reportClassName}>
                <Row>
                  <Col>
                    <Graphic
                      bsHeight={320}
                      className={"g1"}
                      documentName={"availability"}
                      documentType={"stackedBarChart"}
                      entity={entity}
                      generatorName={"generic"}
                      generatorType={"stackedBarChart"}
                      infoLink={`${graphInfoLink}${entity}-analytics#Overall_availability_of_groups`}
                      reportMode={reportMode}
                      subject={subject}
                      title={t("tagIndicator.groupsAvailability")}
                    />
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <Graphic
                      bsHeight={320}
                      className={"g1"}
                      documentName={"oldestEvents"}
                      documentType={"barChart"}
                      entity={entity}
                      generatorName={"generic"}
                      generatorType={"barChart"}
                      infoLink={`${graphInfoLink}${entity}-analytics#Days_since_groups_are_failing`}
                      reportMode={reportMode}
                      subject={subject}
                      title={t("tagIndicator.oldestGroupEvent")}
                    />
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <Graphic
                      bsHeight={320}
                      className={"g1"}
                      documentName={"groupsByTag"}
                      documentType={"heatMapChart"}
                      entity={entity}
                      generatorName={"generic"}
                      generatorType={"heatMapChart"}
                      infoLink={`${graphInfoLink}${entity}-analytics#Tags_by_groups`}
                      reportMode={reportMode}
                      subject={subject}
                      title={t("analytics.heatMapChart.groupsByTag")}
                    />
                  </Col>
                </Row>
              </div>
            ) : undefined}
            {doesEntityMatch(["group"]) ? (
              <div className={reportClassName}>
                <Row>
                  <Col>
                    <Graphic
                      bsHeight={320}
                      className={"g1"}
                      documentName={"oldestEvents"}
                      documentType={"barChart"}
                      entity={entity}
                      generatorName={"generic"}
                      generatorType={"barChart"}
                      infoLink={`${graphInfoLink}group-analytics#Days_since_group_is_failing`}
                      reportMode={reportMode}
                      subject={subject}
                      title={t("tagIndicator.oldestEvent")}
                    />
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <Graphic
                      bsHeight={320}
                      className={"g1"}
                      documentName={"findingsByTag"}
                      documentType={"heatMapChart"}
                      entity={entity}
                      generatorName={"generic"}
                      generatorType={"heatMapChart"}
                      infoLink={`${graphInfoLink}group-analytics#Findings_by_tags`}
                      reportMode={reportMode}
                      subject={subject}
                      title={t("analytics.heatMapChart.findingsByTag")}
                    />
                  </Col>
                </Row>
                <hr />
                <div className={"center"}>
                  <Row>
                    <Col>
                      <PanelCollapseHeader>
                        <h1>{t("analytics.sections.forces.title")}</h1>
                      </PanelCollapseHeader>
                    </Col>
                  </Row>
                  <div className={"pt4"} />
                  <Row>
                    <Col>
                      <Graphic
                        bsHeight={80}
                        className={"g3"}
                        documentName={"forcesStatus"}
                        documentType={"textBox"}
                        entity={entity}
                        generatorName={"generic"}
                        generatorType={"textBox"}
                        infoLink={`${graphInfoLink}group-analytics#Service_status`}
                        reportMode={reportMode}
                        subject={subject}
                        title={t("analytics.textBox.forcesStatus.title")}
                      />
                    </Col>
                    <Col>
                      <Graphic
                        bsHeight={80}
                        className={"g3"}
                        documentName={"forcesUsage"}
                        documentType={"textBox"}
                        entity={entity}
                        generatorName={"generic"}
                        generatorType={"textBox"}
                        infoLink={`${graphInfoLink}group-analytics#Service_usage`}
                        reportMode={reportMode}
                        subject={subject}
                        title={t("analytics.textBox.forcesUsage.title")}
                      />
                    </Col>
                    <Col>
                      <Graphic
                        bsHeight={80}
                        className={"g3"}
                        documentName={"forcesRepositoriesAndBranches"}
                        documentType={"textBox"}
                        entity={entity}
                        generatorName={"generic"}
                        generatorType={"textBox"}
                        infoLink={`${graphInfoLink}group-analytics#Repositories_and_branches`}
                        reportMode={reportMode}
                        subject={subject}
                        title={t(
                          "analytics.textBox.forcesRepositoriesAndBranches.title",
                        )}
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col lg={50} md={50} sm={50}>
                      <Graphic
                        bsHeight={160}
                        className={"g2"}
                        documentName={"forcesSecurityCommitment"}
                        documentType={"gauge"}
                        entity={entity}
                        generatorName={"generic"}
                        generatorType={"c3"}
                        infoLink={`${graphInfoLink}group-analytics#Your_commitment_towards_security`}
                        reportMode={reportMode}
                        subject={subject}
                        title={t(
                          "analytics.gauge.forcesSecurityCommitment.title",
                        )}
                      />
                    </Col>
                    <Col lg={50} md={50} sm={50}>
                      <Graphic
                        bsHeight={160}
                        className={"g2"}
                        documentName={"forcesBuildsRisk"}
                        documentType={"gauge"}
                        entity={entity}
                        generatorName={"generic"}
                        generatorType={"c3"}
                        infoLink={`${graphInfoLink}group-analytics#Builds_risk`}
                        reportMode={reportMode}
                        subject={subject}
                        title={t("analytics.gauge.forcesBuildsRisk.title")}
                      />
                    </Col>
                  </Row>
                </div>
              </div>
            ) : undefined}
          </Container>
        </Container>
      </CustomThemeProvider>
    </React.StrictMode>
  );
};
