import { object, string } from "yup";

import { translate } from "utils/translations/translate";

const MAX_TREATMENT_JUSTIFICATION_LENGTH = 1000;
const MIN_TREATMENT_JUSTIFICATION_LENGTH = 10;

const validationSchema = object().shape({
  treatmentJustification: string()
    .required(translate.t("validations.required"))
    .min(
      MIN_TREATMENT_JUSTIFICATION_LENGTH,
      translate.t("validations.minLength", {
        count: MIN_TREATMENT_JUSTIFICATION_LENGTH,
      }),
    )
    .isValidTextBeginning()
    .isValidTextField(),
});

export { MAX_TREATMENT_JUSTIFICATION_LENGTH, validationSchema };
