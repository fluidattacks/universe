import { useModal } from "@fluidattacks/design";
import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { Route, Routes } from "react-router-dom";

import type { IRemediationValues } from "./types";

import { RemediationModal } from ".";
import { render } from "mocks";

const values: IRemediationValues = { treatmentJustification: "" };
const closeModal = jest.fn();

const RemediationComponent = ({
  alertMsg,
}: Readonly<{ alertMsg?: string }>): JSX.Element => {
  const remediationModalTest = useModal("remediation-test");

  return (
    <RemediationModal
      alertMessage={alertMsg}
      isLoading={false}
      message={"test"}
      modalRef={{
        ...remediationModalTest,
        close: closeModal,
        isOpen: true,
      }}
      onSubmit={jest.fn()}
      remediationType={"verifyVulnerability"}
      title={"title"}
      values={values}
    />
  );
};

describe("remediation modal", (): void => {
  const memoryRouterInjectedVulns = {
    initialEntries: ["/orgs/okada/groups/unittesting/vulns"],
  };

  it("should render", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route
          element={<RemediationComponent />}
          path={"/orgs/okada/groups/unittesting/vulns"}
        />
      </Routes>,
      { memoryRouter: memoryRouterInjectedVulns },
    );

    expect(screen.queryByText("title")).toBeInTheDocument();
    expect(screen.getByRole("textbox")).toBeInTheDocument();

    await userEvent.click(screen.getByText("Cancel"));
    await waitFor((): void => {
      expect(closeModal).toHaveBeenCalledTimes(1);
    });
    jest.clearAllMocks();
  });

  it("should render with alert and link", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route
          element={
            <RemediationComponent alertMsg={"test alert message reported"} />
          }
          path={"/orgs/okada/groups/unittesting/vulns"}
        />
      </Routes>,
      { memoryRouter: memoryRouterInjectedVulns },
    );

    expect(screen.queryByText("title")).toBeInTheDocument();
    expect(screen.getByRole("textbox")).toBeInTheDocument();
    expect(
      screen.queryByText("test alert message reported"),
    ).toBeInTheDocument();

    expect(screen.getByRole("link", { name: "app.link" })).toBeInTheDocument();
    expect(screen.queryByText("app.link")).toBeInTheDocument();

    const linkTest = screen.getByRole("link", { name: "app.link" });

    expect(linkTest).toHaveAttribute(
      "href",
      "/orgs/okada/groups/unittesting/scope",
    );

    await userEvent.click(screen.getByText("Cancel"));
    await waitFor((): void => {
      expect(closeModal).toHaveBeenCalledTimes(1);
    });
    jest.clearAllMocks();
  });

  it("should render with alert", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route
          element={<RemediationComponent alertMsg={"test alert message"} />}
          path={"/orgs/okada/groups/unittesting/vulns"}
        />
      </Routes>,
      { memoryRouter: memoryRouterInjectedVulns },
    );

    expect(screen.queryByText("title")).toBeInTheDocument();
    expect(screen.queryByText("test alert message")).toBeInTheDocument();
    expect(screen.queryByText("app.link")).not.toBeInTheDocument();

    expect(screen.getByRole("textbox")).toBeInTheDocument();

    await userEvent.click(screen.getByText("Cancel"));
    await waitFor((): void => {
      expect(closeModal).toHaveBeenCalledTimes(1);
    });
    jest.clearAllMocks();
  });
});
