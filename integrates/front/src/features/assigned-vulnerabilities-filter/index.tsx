import isEmpty from "lodash/isEmpty";
import { useEffect, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { Filters } from "components/filter";
import type { IFilter, ISelectedOptions } from "components/filter/types";
import type { IVulnRowAttr } from "features/vulnerabilities/types";
import { filterVerification } from "pages/finding/vulnerabilities/utils";
import { setFiltersUtil } from "utils/set-filters";

const AssignedVulnerabilitiesFilter = ({
  setFilteredData,
  vulnerabilities,
}: Readonly<{
  setFilteredData: React.Dispatch<React.SetStateAction<IVulnRowAttr[]>>;
  vulnerabilities: IVulnRowAttr[];
}>): JSX.Element => {
  const { t } = useTranslation();
  const [filters, setFilters] = useState<IFilter<IVulnRowAttr>[]>([
    {
      id: "organizationName",
      key: "organizationName",
      label: "Organization",
      selectOptions: (vulns: IVulnRowAttr[]): ISelectedOptions[] =>
        vulns.map(
          (vuln): ISelectedOptions => ({ value: vuln.organizationName ?? "" }),
        ),
      type: "select",
    },
    {
      id: "groupName",
      key: "groupName",
      label: t("organization.tabs.groups.newGroup.name.text"),
      type: "text",
    },
    {
      id: "finding-title",
      key: (row, value): boolean => {
        if (!row.finding) return false;
        if (typeof value !== "string" || isEmpty(value)) return true;
        if (isEmpty(row.finding.title)) return false;

        return row.finding.title.includes(value);
      },
      label: t("searchFindings.tabVuln.vulnTable.vulnerabilityType.title"),
      type: "text",
    },
    {
      id: "verification",
      key: filterVerification,
      label: t("searchFindings.tabVuln.vulnTable.reattack"),
      selectOptions: [
        { header: t("searchFindings.tabVuln.onHold"), value: "On_hold" },
        {
          header: t("searchFindings.tabVuln.requested"),
          value: "Requested",
        },
        {
          header: t("searchFindings.tabVuln.verified"),
          value: "Verified",
        },
        {
          header: t("searchFindings.tabVuln.notRequested"),
          value: "NotRequested",
        },
      ],
      type: "select",
    },
    {
      id: "priority",
      key: "priority",
      label: t("searchFindings.tabDescription.businessPriority"),
      type: "numberRange",
      valueType: "numberRangeValues",
    },
    {
      id: "tag",
      key: "tag",
      label: t("searchFindings.tabVuln.vulnTable.tags"),
      type: "text",
    },
  ]);

  useEffect((): void => {
    setFilteredData(setFiltersUtil(vulnerabilities, filters));
  }, [filters, vulnerabilities, setFilteredData]);

  return (
    <Filters
      dataset={vulnerabilities}
      filters={filters}
      setFilters={setFilters}
    />
  );
};

export { AssignedVulnerabilitiesFilter };
