import type {
  ICredentialRowAttr,
  ICredentialWithOrganization,
  IRoot,
} from "pages/organization/members/remove-stakeholder-modal/types";

interface IInformationProps {
  data: ICredentialRowAttr;
}

interface ICredManagementModalDataProps {
  credentials: ICredentialWithOrganization[];
  groups: {
    name: string;
    organizationName: string;
    roots: IRoot[];
  }[];
}
interface ICredManagementModalProps {
  data: ICredManagementModalDataProps;
  onClose: () => void;
  onContinue: () => void;
  open: boolean;
  origin: "Me" | "Organization";
}

interface IGroupCredentialsInfo {
  ids: string[];
  name: string;
  orgName: string;
}

export type {
  IInformationProps,
  ICredManagementModalDataProps,
  ICredManagementModalProps,
  IGroupCredentialsInfo,
};
