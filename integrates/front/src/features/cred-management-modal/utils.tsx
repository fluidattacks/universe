import type { ColumnDef } from "@tanstack/react-table";

import { Information } from "./information";
import type { IGroupCredentialsInfo } from "./types";

import type { ICredentialRowAttr } from "pages/organization/members/remove-stakeholder-modal/types";

const columns: ColumnDef<ICredentialRowAttr>[] = [
  {
    accessorKey: "owner",
    header: "Owner",
  },
  {
    accessorKey: "name",
    header: "Name",
  },
  {
    accessorKey: "type",
    header: "Type",
  },
  {
    accessorKey: "isUsed",
    cell: ({ row }): JSX.Element => <Information data={row.original} />,
    header: "Details",
  },
];

const getGroupName = ({
  isUsed,
  currentGroups,
}: {
  isUsed: boolean;
  currentGroups: IGroupCredentialsInfo[];
}): string => {
  if (!isUsed) return "-";
  const [firstGroup] = currentGroups;
  const groupCount = currentGroups.length;
  if (groupCount === 1) return firstGroup.name;

  return `${firstGroup.name} and ${groupCount - 1} others`;
};

export { getGroupName, columns };
