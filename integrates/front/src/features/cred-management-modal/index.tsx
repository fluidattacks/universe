import { Modal, useModal } from "@fluidattacks/design";
import { useMemo } from "react";
import { useTranslation } from "react-i18next";

import type { ICredManagementModalProps, IGroupCredentialsInfo } from "./types";
import { columns, getGroupName } from "./utils";

import { Table } from "components/table";
import { useTable } from "hooks";
import type { ICredentialRowAttr } from "pages/organization/members/remove-stakeholder-modal/types";

const CredManagementModal = ({
  data,
  onClose,
  onContinue,
  open,
  origin,
}: Readonly<ICredManagementModalProps>): JSX.Element => {
  const { t } = useTranslation();
  const modalProps = useModal("cred-management-modal");
  const tableRef = useTable("credentials");
  const formattedCredentials = useMemo((): ICredentialRowAttr[] => {
    const { groups } = data;

    const credentialsByGroup = groups.map((group): IGroupCredentialsInfo => {
      // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
      const credentialsByRoot = (group.roots ?? []).reduce(
        (acc: string[], root): string[] => {
          if (root.credentials) {
            return [...acc, root.credentials.id];
          }

          return acc;
        },
        [],
      );

      return {
        ids: credentialsByRoot,
        name: group.name,
        orgName: group.organizationName,
      };
    });

    return data.credentials.map((credential): ICredentialRowAttr => {
      const currentGroups = credentialsByGroup.filter((group): boolean =>
        group.ids.includes(credential.id),
      );
      const isUsed = currentGroups.length > 0;

      return {
        groupName: getGroupName({ currentGroups, isUsed }),
        id: credential.id,
        isUsed,
        name: credential.name,
        organizationName: credential.organizationName,
        owner: credential.owner,
        type: `${credential.type} ${credential.oauthType ?? ""}`,
      };
    });
  }, [data]);

  const title = t(
    origin === "Me"
      ? "navbar.deleteAccount.text"
      : "organization.tabs.users.removeButton.confirmTitle",
  );
  const text = t(
    `organization.tabs.users.removeButton.credentialsAdvisory${origin}`,
  );
  const continueButton = t("organization.tabs.users.removeButton.continue");
  const cancelButton = t("organization.tabs.users.removeButton.cancel");

  const isAbleToContinue = useMemo((): boolean => {
    return formattedCredentials.every(
      (credential): boolean => !credential.isUsed,
    );
  }, [formattedCredentials]);

  return (
    <Modal
      cancelButton={{
        onClick: onClose,
        text: cancelButton,
      }}
      confirmButton={
        isAbleToContinue
          ? {
              onClick: onContinue,
              text: continueButton,
            }
          : undefined
      }
      description={formattedCredentials.length > 0 ? text : ""}
      modalRef={{ ...modalProps, close: onClose, isOpen: open }}
      size={"lg"}
      title={title}
    >
      {formattedCredentials.length > 0 ? (
        <Table
          columns={columns}
          data={formattedCredentials}
          options={{
            enableSearchBar: false,
          }}
          tableRef={tableRef}
        />
      ) : null}
    </Modal>
  );
};

export { CredManagementModal };
