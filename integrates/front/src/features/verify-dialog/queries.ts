import { graphql } from "gql";

const GET_STAKEHOLDER_PHONE = graphql(`
  query GetStakeholderPhoneQuery {
    me {
      __typename
      userEmail
      phone {
        callingCountryCode
        countryCode
        nationalNumber
      }
    }
  }
`);

const VERIFY_STAKEHOLDER_MUTATION = graphql(`
  mutation VerifyStakeholderMutationAtVerifyDialog(
    $channel: VerificationChannel
  ) {
    verifyStakeholder(channel: $channel) {
      success
    }
  }
`);

export { GET_STAKEHOLDER_PHONE, VERIFY_STAKEHOLDER_MUTATION };
