import {
  Button,
  Container,
  Heading,
  Icon,
  Span,
  Text,
} from "@fluidattacks/design";
import type { IconName } from "@fortawesome/free-solid-svg-icons";
import isNil from "lodash/isNil";
import type { FC } from "react";
import { useTranslation } from "react-i18next";
import { styled, useTheme } from "styled-components";

import type { IChannelsProps } from "./types";

const IconContainer = styled.div`
  svg {
    font-size: 34px;
  }
`;

const PAD = 1.5;

const Channels: FC<IChannelsProps> = ({
  isChannelOpen,
  onClick,
  padding = [0, PAD, PAD, PAD],
  phone,
}): JSX.Element => {
  const { t } = useTranslation();
  const theme = useTheme();

  const channels = [
    {
      icon: "message-sms",
      id: t("components.channels.smsButton.id"),
      text: t("components.channels.smsButton.text"),
      value: "SMS",
    },
    {
      icon: "whatsapp",
      id: t("components.channels.whatsappButton.id"),
      text: t("components.channels.whatsappButton.text"),
      value: "WHATSAPP",
    },
  ];

  return (
    <Container
      display={isChannelOpen ? "flex" : "none"}
      flexDirection={"column"}
      gap={1}
      padding={padding}
      width={"100%"}
    >
      {channels.map((repo): JSX.Element | undefined => {
        const { icon, id, text, value } = repo;
        const PHONE_NUMBER_LAST_DIGITS = 4;
        const description = isNil(phone)
          ? ""
          : `${t("components.channels.description")} ${phone.slice(-PHONE_NUMBER_LAST_DIGITS)}`;

        return (
          <Container
            border={"1px solid"}
            borderColor={theme.palette.gray[200]}
            borderRadius={"4px"}
            key={id}
          >
            <Button
              id={`verify-${value.toLowerCase()}`}
              justify={"center"}
              onClick={onClick}
              py={1}
              value={value}
              variant={"ghost"}
              width={"100%"}
            >
              <IconContainer>
                <Icon
                  icon={icon as IconName}
                  iconSize={"lg"}
                  iconType={value === "SMS" ? "fa-light" : "fa-brands"}
                  mr={1.5}
                />
              </IconContainer>
              <Span size={"sm"}>
                <Heading fontWeight={"bold"} size={"xs"}>
                  {text}
                </Heading>
                <Text color={theme.palette.gray[800]} size={"sm"}>
                  {description}
                </Text>
              </Span>
            </Button>
          </Container>
        );
      })}
    </Container>
  );
};

export { Channels };
