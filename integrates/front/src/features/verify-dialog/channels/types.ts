import type { IPaddingModifiable } from "@fluidattacks/design";
import type { IconDefinition } from "@fortawesome/free-solid-svg-icons";

interface IChannel {
  icon: IconDefinition | string;
  id: string;
  text: string;
  value: string;
}

interface IChannelsProps {
  onClick: (event: React.MouseEvent<HTMLButtonElement>) => void;
  padding?: IPaddingModifiable["padding"];
  phone: string | undefined;
  isChannelOpen: boolean;
  isMobileValidation?: boolean;
}

export type { IChannel, IChannelsProps };
