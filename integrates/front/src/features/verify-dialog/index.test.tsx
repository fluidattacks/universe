import { Button } from "@fluidattacks/design";
import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { useCallback } from "react";
import * as React from "react";

import type { TVerifyFn } from "./types";

import { VerifyDialog } from ".";
import { render } from "mocks";

describe("verifyDialog", (): void => {
  const verifyCallback: jest.Mock = jest.fn(
    async (_verificationCode: string): Promise<void> => Promise.resolve(),
  );
  const cancelCallback: jest.Mock = jest.fn();
  const TestComponent: React.FC = (): JSX.Element => {
    const handleClick = useCallback(
      (setVerifyCallbacks: TVerifyFn): (() => void) =>
        (): void => {
          setVerifyCallbacks(verifyCallback, cancelCallback);
        },
      [],
    );

    return (
      <VerifyDialog isOpen={true}>
        {(setVerifyCallbacks): React.ReactNode => {
          return (
            <Button
              onClick={handleClick(setVerifyCallbacks)}
              variant={"primary"}
            >
              {"Button Test"}
            </Button>
          );
        }}
      </VerifyDialog>
    );
  };

  it("should handle stakeholder verification", async (): Promise<void> => {
    expect.hasAssertions();

    render(<TestComponent />);

    await waitFor((): void => {
      expect(
        screen.getByText("components.channels.smsButton.text"),
      ).toBeInTheDocument();
    });

    await userEvent.click(
      screen.getByText("components.channels.smsButton.text"),
    );

    expect(screen.queryByText("verifyDialog.verify")).toBeInTheDocument();
    expect(screen.queryByText("Button Test")).toBeInTheDocument();
    expect(screen.queryByText("verifyDialog.title")).toBeInTheDocument();
    expect(
      screen.queryByText("verifyDialog.fields.verificationCode"),
    ).toBeInTheDocument();

    await userEvent.type(
      screen.getByRole("textbox", { name: "verificationCode" }),
      "1234",
    );

    await userEvent.click(screen.getByText("Button Test"));
    await userEvent.click(screen.getByText("verifyDialog.verify"));

    await waitFor((): void => {
      expect(verifyCallback).toHaveBeenCalledTimes(1);
    });

    expect(verifyCallback).toHaveBeenCalledWith("1234");
  });
});
