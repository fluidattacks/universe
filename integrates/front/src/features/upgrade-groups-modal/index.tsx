import { useMutation, useQuery } from "@apollo/client";
import {
  Checkbox,
  Container,
  Form,
  InnerForm,
  Link,
  Modal,
  Text,
  useModal,
} from "@fluidattacks/design";
import capitalize from "lodash/capitalize";
import { Fragment, useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { array, object } from "yup";

import type { IGroup } from "./queries";
import {
  GET_USER_ORGANIZATIONS_GROUPS,
  REQUEST_GROUPS_UPGRADE_MUTATION,
} from "./queries";

import { Logger } from "utils/logger";
import { msgSuccess } from "utils/notifications";

interface IUpgradeGroupsModalProps {
  readonly onClose: () => void;
}

const UpgradeGroupsModal: React.FC<IUpgradeGroupsModalProps> = ({
  onClose,
}): JSX.Element => {
  const { t } = useTranslation();
  const modalProps = useModal("upgrade-groups-modal");

  const { data } = useQuery(GET_USER_ORGANIZATIONS_GROUPS, {
    fetchPolicy: "cache-first",
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.warning("Couldn't load user organizations groups", error);
      });
    },
  });
  const organizations = data?.me.organizations ?? [];
  const groups = organizations.reduce<IGroup[]>(
    (previousValue, currentValue): IGroup[] => [
      ...previousValue,
      ...(currentValue.groups as IGroup[]),
    ],
    [],
  );
  const upgradableGroups = groups
    .filter(
      (group): boolean =>
        !group.serviceAttributes.includes("has_advanced") &&
        group.permissions.includes("request_group_upgrade"),
    )
    .map((group): string => group.name);
  const canUpgrade = upgradableGroups.length > 0;

  const [requestGroupsUpgrade] = useMutation(REQUEST_GROUPS_UPGRADE_MUTATION, {
    onCompleted: (): void => {
      msgSuccess(t("upgrade.success.text"), t("upgrade.success.title"));
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.error("Couldn't request groups upgrade", error);
      });
    },
  });

  const handleSubmit = useCallback(
    async (values: { groupNames: string[] }): Promise<void> => {
      onClose();
      await requestGroupsUpgrade({
        variables: { groupNames: values.groupNames },
      });
    },
    [onClose, requestGroupsUpgrade],
  );

  const validationSchema = object().shape({
    groupNames: array().min(1, t("validations.someRequired")),
  });

  return (
    <Modal
      modalRef={{ ...modalProps, close: onClose, isOpen: true }}
      size={"sm"}
      title={t("upgrade.title")}
    >
      <Form
        cancelButton={{ onClick: onClose }}
        confirmButton={{ label: t("upgrade.upgrade") }}
        defaultDisabled={false}
        defaultValues={{ groupNames: upgradableGroups }}
        onSubmit={handleSubmit}
        yupSchema={validationSchema}
      >
        <InnerForm>
          {({ register }): JSX.Element => (
            <Fragment>
              <Text display={"inline"} size={"sm"}>
                {t("upgrade.text")}&nbsp;
                <Link href={"https://fluidattacks.com/plans/"}>
                  {t("upgrade.link")}
                </Link>
              </Text>
              {canUpgrade ? (
                <Fragment>
                  <Text mb={1} size={"sm"}>
                    {t("upgrade.select")}
                  </Text>
                  <Container display={"flex"} flexDirection={"column"} gap={1}>
                    {upgradableGroups.map(
                      (groupName): JSX.Element => (
                        <Checkbox
                          {...register("groupNames")}
                          defaultChecked={true}
                          key={groupName}
                          label={capitalize(groupName)}
                          name={"groupNames"}
                          value={groupName}
                        />
                      ),
                    )}
                  </Container>
                </Fragment>
              ) : (
                <Text size={"sm"}>{t("upgrade.unauthorized")}</Text>
              )}
            </Fragment>
          )}
        </InnerForm>
      </Form>
    </Modal>
  );
};

export { UpgradeGroupsModal };
