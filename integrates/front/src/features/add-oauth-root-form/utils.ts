import type { ApolloError } from "@apollo/client";
import { useMutation } from "@apollo/client";
import _ from "lodash";
import { useCallback } from "react";
import type { AnyObject, InferType, Maybe, Schema, TestContext } from "yup";
import { array, boolean, lazy, object, string } from "yup";

import type { IOauthError } from "./oauth-errors-pop-up/types";
import { ADD_GIT_ROOT } from "./queries";
import type { IFormValues, IGitignore, IIntegrationRepository } from "./types";

import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";
import { translate } from "utils/translations/translate";
import { validateSSHFormat } from "utils/validations";

const validateStep = (values: IFormValues): boolean => {
  const filledBranchesValues = values.branches.filter(
    (branch): boolean => !_.isEmpty(branch),
  );

  if (filledBranchesValues.length === values.urls.length) {
    return false;
  }

  return true;
};

const validateExcludeFormat = (
  values: IFormValues,
): ((
  value: string | undefined,
  options: TestContext<Maybe<AnyObject>>,
) => boolean) => {
  return (value, options): boolean => {
    const parent = options.path;
    const index = parseInt(parent.split("[")[1].split("]")[0], 10);
    const repoUrl = values.urls[index];
    if (value !== undefined && repoUrl) {
      const [urlBasename] = repoUrl.split("/").slice(-1);
      const repoName = urlBasename.endsWith(".git")
        ? urlBasename.replace(".git", "")
        : urlBasename;

      return (
        value.toLowerCase().split("/").indexOf(repoName.toLowerCase()) !== 0
      );
    }

    return true;
  };
};

const validateHealthCheck = (values: IFormValues): boolean => {
  const { healthCheckConfirm, includesHealthCheck } = values;
  if (healthCheckConfirm === undefined) {
    return false;
  }

  return (
    (includesHealthCheck === "yes" &&
      healthCheckConfirm.includes("includeA")) ||
    (includesHealthCheck === "no" &&
      healthCheckConfirm.includes("rejectA") &&
      healthCheckConfirm.includes("rejectB") &&
      healthCheckConfirm.includes("rejectC"))
  );
};

const gitIgnoreRequired = (
  gitignore: IGitignore[],
): ((
  value: string | undefined,
  options: TestContext<Maybe<AnyObject>>,
) => boolean) => {
  return (
    value: string | undefined,
    options: TestContext<Maybe<AnyObject>>,
  ): boolean => {
    const parent = options.path;
    const index = parseInt(parent.split("[")[1].split("]")[0], 10);

    return (
      gitignore[index].paths.length === 1 ||
      (gitignore[index].paths.length > 1 && value !== undefined)
    );
  };
};

const validationSchema = (
  hasAdvanced: boolean,
  provider?: string,
): InferType<Schema> =>
  lazy(
    (values: IFormValues): Schema =>
      object().shape({
        branches: array().of(
          string().required(translate.t("validations.required")),
        ),
        credentials: object({
          id: string().required(translate.t("validations.required")),
          isPat: boolean(),
          isToken: boolean(),
          name: string(),
          oauthType: string(),
          type: string(),
        }),
        environments: array().of(string()),
        gitignore: array().of(
          object({
            paths: array().of(
              string()
                .isValidFunction(
                  gitIgnoreRequired(values.gitignore),
                  translate.t("validations.required"),
                )
                .isValidFunction(
                  validateExcludeFormat(values),
                  translate.t("validations.excludeFormat"),
                ),
            ),
          }),
        ),
        hasExclusions: string().required(translate.t("validations.required")),
        healthCheckConfirm: array()
          .of(string())
          .when("validateHealthCheck", (): Schema => {
            return _.isUndefined(provider) || !hasAdvanced
              ? array()
              : array().of(
                  string().isValidFunction((): boolean => {
                    return validateHealthCheck(values);
                  }, translate.t("validations.required")),
                );
          }),
        includesHealthCheck: string(),
      }),
  );

const formatGitIgnore = (paths: string[]): string[] => {
  if (paths.length === 1 && _.isEmpty(paths[0])) {
    return [];
  }

  return paths;
};

function useRootSubmit(
  groupName: string,
  repositories: Record<string, IIntegrationRepository>,
  fillErrors: (error: IOauthError) => void,
  finalActions: () => void,
): ({
  branches,
  credentials,
  gitignore,
  includesHealthCheck,
  urls,
}: IFormValues) => Promise<void> {
  const [addGitRoot] = useMutation(ADD_GIT_ROOT, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        if (error.message === "Exception - Invalid characters") {
          msgError(translate.t("validations.invalidChar"));
        } else {
          Logger.error("Couldn't add root", error);
        }
      });
    },
  });

  return useCallback(
    async ({
      branches,
      credentials,
      gitignore,
      includesHealthCheck,
      urls,
    }: IFormValues): Promise<void> => {
      const rootNicknames = urls.reduce(
        (result: string[], url: string): string[] => {
          const repository = repositories[url];
          const nickname = repository.name
            .split("/")
            .join("_")
            .split(" ")
            .join("");

          const updatedNickname = (
            _.includes(result, nickname)
              ? `${nickname}_${crypto.randomUUID().substring(0, 4)}`
              : nickname
          ).replace(/[^A-z_0-9-]/giu, "_");

          return /^[a-zA-Z_0-9-]{1,128}$/u.exec(updatedNickname)
            ? [...result, updatedNickname]
            : [...result, ""];
        },
        [],
      );

      await Promise.all(
        urls.map(async (url, index): Promise<void> => {
          const formattedUrl = validateSSHFormat(url.trim())
            ? `ssh://${url.trim()}`
            : url.trim();

          await addGitRoot({
            onError: (error: ApolloError): void => {
              const oauthError = {
                error: error.message,
                repoName: rootNicknames[index],
              };
              fillErrors(oauthError);
            },
            variables: {
              branch: branches[index].trim(),
              credentials: {
                id: credentials.id,
              },
              gitignore: formatGitIgnore(gitignore[index].paths),
              groupName,
              includesHealthCheck: includesHealthCheck === "yes",
              nickname: rootNicknames[index],
              url: formattedUrl,
              useEgress: false,
              useVpn: false,
              useZtna: false,
            },
          });
        }),
      );
      finalActions();
    },
    [addGitRoot, finalActions, groupName, repositories, fillErrors],
  );
}

export { useRootSubmit, validationSchema, validateStep };
