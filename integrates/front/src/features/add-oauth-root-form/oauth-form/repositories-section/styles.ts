import { styled } from "styled-components";

const StyledGrid = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  gap: 8px;
  padding: 0.75rem 1rem;
  min-height: 50px;
  max-height: 200px;
  overflow-y: auto;
`;

const StyledTitle = styled.div`
  padding: 0.75rem 1rem;
  border-bottom: solid 1px #d4d4db;
  min-height: 40px;

  & label {
    font-weight: 700;
    user-select: none;
  }
`;

const StyledBox = styled.div`
  background-color: ${({ theme }): string => theme.palette.white};
  border: solid 1px ${({ theme }): string => theme.palette.gray[200]};
  border-radius: 4px;
  margin-top: 0.625rem;
  scroll-behavior: unset;
  width: 100%;
`;

const StyledSearchBox = styled.div`
  display: flex;
  box-sizing: border-box;
  outline: none;
  width: 100%;
  padding: 0.5rem 1rem;

  font-family: ${({ theme }): string => theme.typography.type.primary};
  font-size: ${({ theme }): string => theme.typography.text.sm};

  background: ${({ theme }): string => theme.palette.white};
  border: 1px solid ${({ theme }): string => theme.palette.gray[300]};
  border-radius: 4px;
  color: ${({ theme }): string => theme.palette.gray[600]};
`;

const StyledSearchInput = styled.input`
  border: none;
  outline: none;
  width: 100%;
  margin-left: 0.5rem;

  &::placeholder {
    color: ${({ theme }): string => theme.palette.gray[400]};
  }
`;

export {
  StyledGrid,
  StyledTitle,
  StyledBox,
  StyledSearchBox,
  StyledSearchInput,
};
