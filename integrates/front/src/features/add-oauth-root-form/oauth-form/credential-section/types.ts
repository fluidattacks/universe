import type { TProvider } from "../../types";

interface ICredentialSection {
  trialOrgId?: string;
  provider?: TProvider;
}

export type { ICredentialSection };
