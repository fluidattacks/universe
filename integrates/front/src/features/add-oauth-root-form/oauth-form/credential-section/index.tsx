import { useQuery } from "@apollo/client";
import { useFormikContext } from "formik";
import { isEmpty, isNil, isUndefined } from "lodash";
import { useCallback, useContext, useEffect, useMemo } from "react";
import type { FC } from "react";
import { useTranslation } from "react-i18next";

import type { ICredentialSection } from "./types";
import { getCredentials } from "./utils";

import { GET_ORGANIZATION_CREDENTIALS } from "../../queries";
import type { ICredentialsAttr, IFormValues } from "../../types";
import type { IDropDownOption } from "components/dropdown/types";
import { Select } from "components/input";
import { organizationDataContext } from "pages/organization/context";
import { Logger } from "utils/logger";

const CredentialSection: FC<ICredentialSection> = ({
  trialOrgId,
  provider,
}): JSX.Element => {
  const { t } = useTranslation();
  const { setFieldValue, values } = useFormikContext<IFormValues>();
  const { organizationId } = useContext(organizationDataContext);

  const orgId = useMemo((): string => {
    if (isUndefined(provider) && !isUndefined(trialOrgId)) {
      return trialOrgId;
    }

    return organizationId;
  }, [organizationId, trialOrgId, provider]);

  const { data } = useQuery(GET_ORGANIZATION_CREDENTIALS, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        if (error.message !== "Access denied or organization not found") {
          Logger.error("Couldn't load organization credentials", error);
        }
      });
    },
    variables: {
      organizationId: orgId,
    },
  });

  const credentialsData = useMemo((): ICredentialsAttr[] => {
    if (isUndefined(data)) {
      return [];
    }

    if (isUndefined(provider)) {
      return (data.organization.credentials ?? []).filter(
        (credential): boolean => credential.type === "OAUTH",
      ) as ICredentialsAttr[];
    }

    return getCredentials(
      data.organization.credentials as ICredentialsAttr[],
      provider,
    );
  }, [data, provider]);
  const credentials = useMemo((): Record<string, ICredentialsAttr> => {
    return credentialsData.length > 0
      ? Object.fromEntries(
          credentialsData.map((cred): [string, ICredentialsAttr] => [
            cred.id,
            cred,
          ]),
        )
      : {};
  }, [credentialsData]);

  const handleOnChangeCredential = useCallback(
    (selection: IDropDownOption): void => {
      if (!isEmpty(selection.value) && !isNil(selection.value)) {
        const currentCred = credentials[selection.value];
        void setFieldValue("credentials.type", currentCred.type);
        void setFieldValue("credentials.oauthType", currentCred.oauthType);
        void setFieldValue("credentials.name", currentCred.name);
        void setFieldValue("credentials.isPat", currentCred.isPat);
        void setFieldValue("credentials.isToken", currentCred.isToken);
      }
    },
    [credentials, setFieldValue],
  );

  useEffect((): void => {
    if (
      values.credentials.id === "" &&
      isUndefined(provider) &&
      !isUndefined(credentialsData[0])
    ) {
      void setFieldValue("credentials.id", credentialsData[0].id);
      void setFieldValue("credentials.name", credentialsData[0].name);
    }
  }, [credentialsData, provider, setFieldValue, values]);

  return (
    <Select
      handleOnChange={handleOnChangeCredential}
      items={Object.values(credentials).map(
        (cred): { header: string; value: string } => ({
          header: cred.name,
          value: cred.id,
        }),
      )}
      label={t("components.oauthRootForm.credentialSection.label")}
      name={"credentials.id"}
      placeholder={t("components.oauthRootForm.credentialSection.placeholder")}
    />
  );
};

export { CredentialSection };
