/* eslint-disable functional/immutable-data */
import { Container, Heading } from "@fluidattacks/design";
import type { FieldArrayRenderProps } from "formik";
import { FieldArray, useFormikContext } from "formik";
import type { FC } from "react";
import { Fragment, useCallback } from "react";

import { Environments } from "./environments";
import { StyledGrid, StyledSelectorWrapper } from "./styles";
import type { IBranchesSectionProps } from "./types";

import type { IFormValues } from "../../types";
import type { IDropDownOption } from "components/dropdown/types";
import { Select } from "components/input";

const BranchesSection: FC<IBranchesSectionProps> = ({
  branchesArrayHelpersRef,
  envsArrayHelpersRef,
  repositories,
}): JSX.Element => {
  const { values } = useFormikContext<IFormValues>();

  const render = useCallback(
    (arrayHelpers: FieldArrayRenderProps): JSX.Element => {
      branchesArrayHelpersRef.current = arrayHelpers;

      const items = (url: string): IDropDownOption[] =>
        repositories[url].branches.map(
          (branch): IDropDownOption => ({
            value: branch,
          }),
        );

      return (
        <Fragment>
          {values.urls.map(
            (url, index): JSX.Element => (
              <StyledGrid key={url}>
                <Heading fontWeight={"bold"} mr={0.5} size={"xs"}>
                  {repositories[url].name}
                </Heading>
                <StyledSelectorWrapper>
                  <Select
                    items={[{ value: "" }, ...items(url)]}
                    label={"Branch"}
                    name={`branches.${index}`}
                  />
                </StyledSelectorWrapper>
              </StyledGrid>
            ),
          )}
        </Fragment>
      );
    },
    [branchesArrayHelpersRef, repositories, values.urls],
  );

  return (
    <Container
      bgColor={"#ffffff"}
      border={"1px solid #f4f4f6"}
      borderRadius={"4px"}
      maxWidth={"100%"}
      mt={0.625}
      px={1.5}
      py={1.5}
      width={"100%"}
    >
      <FieldArray name={"branches"} render={render} />
      <Environments envsArrayHelpersRef={envsArrayHelpersRef} />
    </Container>
  );
};

export { BranchesSection };
