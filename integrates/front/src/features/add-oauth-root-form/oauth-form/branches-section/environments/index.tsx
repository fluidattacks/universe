/* eslint-disable functional/immutable-data */
import { Container } from "@fluidattacks/design";
import type { FieldArrayRenderProps } from "formik";
import { FieldArray, useFormikContext } from "formik";
import isEmpty from "lodash/isEmpty";
import { useCallback } from "react";
import type { FC } from "react";

import type { IEnvironmentsProps } from "./types";

import type { IFormValues } from "../../../types";

const Environments: FC<IEnvironmentsProps> = ({
  envsArrayHelpersRef,
}): JSX.Element => {
  const { values } = useFormikContext<IFormValues>();
  const handleRender = useCallback(
    (arrayHelpers: FieldArrayRenderProps): JSX.Element => {
      envsArrayHelpersRef.current = arrayHelpers;

      return <div />;
    },
    [envsArrayHelpersRef],
  );

  return (
    <div>
      {isEmpty(values.urls) ? undefined : (
        <Container>
          <FieldArray name={"environments"} render={handleRender} />
        </Container>
      )}
    </div>
  );
};

export { Environments };
