import { useQuery } from "@apollo/client";
import {
  Alert,
  Container,
  Link,
  Lottie,
  Row,
  StepLapse,
  Text,
} from "@fluidattacks/design";
import { Form, useFormikContext } from "formik";
import type { ArrayHelpers } from "formik";
import _, { isEmpty } from "lodash";
import type { FC } from "react";
import { Fragment, useCallback, useContext, useRef, useState } from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";

import { BranchesSection } from "./branches-section";
import { CredentialSection } from "./credential-section";
import { ExclusionsSection } from "./exclusions-section";
import { RepositoriesSection } from "./repositories-section";
import type { IOauthFormProps } from "./types";

import { GET_INTEGRATION_REPOSITORIES } from "../queries";
import type { IFormValues, IIntegrationRepository } from "../types";
import { validateStep } from "../utils";
import { ModalConfirm } from "components/modal";
import { organizationDataContext } from "pages/organization/context";
import { redLoader, whiteLoader } from "resources";
import { Logger } from "utils/logger";

const ACTION_TYPE_1 = 100;
const ACTION_TYPE_2 = 90;
const ACTION_TYPE_3 = 50;
const ACTION_TYPE_4 = 70;
const OauthForm: FC<IOauthFormProps> = ({
  handleClose,
  trialOrgId,
  provider,
  setIsCredentialSelected,
  setProgress,
  setRepos,
}): JSX.Element => {
  const { t } = useTranslation();
  const { organizationName } = useParams() as {
    organizationName: string | undefined;
  };
  const { organizationId } = useContext(organizationDataContext);
  const { isSubmitting, isValid, values } = useFormikContext<IFormValues>();
  const [credentialStep, setCredentialStep] = useState(true);
  const [noRepos, setNoRepos] = useState(false);
  const [repositories, setRepositories] = useState<
    Record<string, IIntegrationRepository>
  >({});

  const branchesArrayHelpersRef = useRef<ArrayHelpers | null>(null);
  const exclusionsArrayHelpersRef = useRef<ArrayHelpers | null>(null);
  const envsArrayHelpersRef = useRef<ArrayHelpers | null>(null);

  const { loading } = useQuery(GET_INTEGRATION_REPOSITORIES, {
    onCompleted: (data): void => {
      const { credential } = data.organization;
      if (credential) {
        setRepositories(
          credential.integrationRepositories.length > 0
            ? Object.fromEntries(
                credential.integrationRepositories.map(
                  (repo): [string, IIntegrationRepository] => [
                    repo?.url ?? "",
                    repo as IIntegrationRepository,
                  ],
                ),
              )
            : {},
        );
        setRepos(
          credential.integrationRepositories.length > 0
            ? Object.fromEntries(
                credential.integrationRepositories.map(
                  (repo): [string, IIntegrationRepository] => [
                    repo?.url ?? "",
                    repo as IIntegrationRepository,
                  ],
                ),
              )
            : {},
        );
        setNoRepos(credential.integrationRepositories.length === 0);
        if (_.isUndefined(provider)) {
          setCredentialStep(false);
        }
      }
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.error("Couldn't load integration repositories", error);
      });
    },
    skip: values.credentials.id === "",
    variables: {
      credId: values.credentials.id,
      organizationId:
        provider === undefined ? (trialOrgId ?? "") : organizationId,
    },
  });

  const handleAction = useCallback(
    (progress: number): (() => void) =>
      (): void => {
        if (setProgress) {
          setProgress(progress);
        }
      },
    [setProgress],
  );
  const contentTitle = _.isUndefined(provider)
    ? t("components.oauthRootForm.steps.s3.titleEnrollment")
    : t("components.oauthRootForm.steps.s3.title");
  const steps = [
    {
      content: (
        <RepositoriesSection
          branchesArrayHelpersRef={branchesArrayHelpersRef}
          credentialName={values.credentials.name}
          envsArrayHelpersRef={envsArrayHelpersRef}
          exclusionsArrayHelpersRef={exclusionsArrayHelpersRef}
          repositories={repositories}
        />
      ),
      isDisabledNext: values.urls.length === 0,
      nextAction: handleAction(ACTION_TYPE_4),
      title: t("components.oauthRootForm.steps.s1.title"),
    },
    {
      content: (
        <BranchesSection
          branchesArrayHelpersRef={branchesArrayHelpersRef}
          envsArrayHelpersRef={envsArrayHelpersRef}
          repositories={repositories}
        />
      ),
      isDisabledNext: validateStep(values),
      nextAction: handleAction(ACTION_TYPE_2),
      previousAction: handleAction(ACTION_TYPE_3),
      title: t("components.oauthRootForm.steps.s2.title"),
    },
    {
      content: (
        <ExclusionsSection
          exclusionsArrayHelpersRef={exclusionsArrayHelpersRef}
          repositories={repositories}
        />
      ),
      isDisabledPrevious: isSubmitting,
      title: contentTitle,
    },
  ];

  const nextStep = useCallback((): void => {
    if (!isEmpty(repositories)) {
      if (setIsCredentialSelected) {
        setIsCredentialSelected(true);
      }
      setCredentialStep(false);
    }
  }, [repositories, setIsCredentialSelected]);

  const noReposFragment = (
    <Container alignItems={"center"} scroll={"none"}>
      <Row>
        <Text display={"inline"} size={"sm"}>
          {t("components.oauthRootForm.steps.noRoots.text1")}
          {provider === undefined ? undefined : (
            <Fragment>
              {t("components.oauthRootForm.steps.noRoots.text2")}
              &nbsp;
              <Link href={`/orgs/${organizationName}/credentials`}>
                {t("components.oauthRootForm.steps.noRoots.link")}
              </Link>
            </Fragment>
          )}
        </Text>
      </Row>

      <Alert mt={2} variant={"info"}>
        <Text size={"sm"}>
          {t("components.oauthRootForm.steps.noRoots.info")}
        </Text>
      </Alert>
    </Container>
  );

  const display = _.isUndefined(provider) ? "none" : "block";
  const loadingSection = (
    <Container
      alignItems={"center"}
      display={"flex"}
      justify={"center"}
      scroll={"none"}
    >
      <Container mr={0.25} scroll={"none"}>
        <Lottie animationData={whiteLoader} />
      </Container>
      {t("components.oauthRootForm.credentialSection.loading")}
    </Container>
  );
  const textConfirm = loading
    ? loadingSection
    : t("components.oauthRootForm.credentialSection.continue");
  const trialLoadingSection = (
    <Container
      alignItems={"center"}
      display={"flex"}
      justify={"center"}
      mt={0.75}
      scroll={"none"}
    >
      <Container display={"flex"} scroll={"none"}>
        <Container mr={0.25} scroll={"none"}>
          <Lottie animationData={redLoader} />
        </Container>
        <Text size={"sm"}>
          {t("components.oauthRootForm.credentialSection.trialLoading")}
        </Text>
      </Container>
    </Container>
  );
  const credentialStepFragment = (
    <Form>
      <Container scroll={"none"}>
        <Container display={display} scroll={"none"}>
          <CredentialSection provider={provider} trialOrgId={trialOrgId} />
          <ModalConfirm
            disabled={values.credentials.id === "" || loading}
            onCancel={handleClose}
            onConfirm={nextStep}
            txtConfirm={textConfirm}
          />
        </Container>
        {loading && !_.isEmpty(values.credentials.id)
          ? trialLoadingSection
          : null}
      </Container>
    </Form>
  );

  if (noRepos) {
    return noReposFragment;
  }
  if (credentialStep) {
    return credentialStepFragment;
  }
  const finalButtonText = t(
    `components.oauthRootForm.onSubmit${provider === undefined ? "2" : "1"}`,
  );
  const button = {
    disabled: !isValid || isSubmitting,
    onClick: handleAction(ACTION_TYPE_1),
    text: finalButtonText,
    type: "submit" as const,
  };

  return (
    <Form>
      <StepLapse button={button} steps={steps} />
    </Form>
  );
};

export { OauthForm };
