import { Alert, Container, PopUp } from "@fluidattacks/design";
import { useTranslation } from "react-i18next";

import type { IOauthErrorsPopUp } from "./types";

const OauthErrorsPopUp = ({
  confirmClick,
  errors,
}: Readonly<IOauthErrorsPopUp>): JSX.Element => {
  const { t } = useTranslation();

  return (
    <PopUp
      _portal={true}
      confirmButton={{
        onClick: confirmClick,
        text: t("components.oauthRootForm.errorsPopUp.buttonText"),
      }}
      darkBackground={true}
      description={t("components.oauthRootForm.errorsPopUp.description")}
      image={{
        alt: "OauthRootError",
        height: "150px",
        src: "integrates/login/enrolledUserCase",
        width: "150px",
      }}
      maxWidth={"800px"}
      title={t("components.oauthRootForm.errorsPopUp.title")}
    >
      <Container display={"flex"} gap={0.5} wrap={"wrap"}>
        {errors.map((oauthError): JSX.Element => {
          const { repoName, error } = oauthError;

          return (
            <Container key={repoName} width={"100%"}>
              <Alert variant={"error"}>{`${repoName}: ${error}`}</Alert>
            </Container>
          );
        })}
      </Container>
    </PopUp>
  );
};

export { OauthErrorsPopUp };
