interface IOauthErrorsPopUp {
  confirmClick: () => void;
  errors: IOauthError[];
}

interface IOauthError {
  repoName: string;
  error: string;
}

export type { IOauthErrorsPopUp, IOauthError };
