import {
  CardWithSelector,
  Container,
  Form,
  InnerForm,
  Modal,
  useModal,
} from "@fluidattacks/design";
import { useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { openUrl } from "utils/resource-helpers";

interface ILearningModalProps {
  readonly closeLearningModal: () => void;
}

const LearningModal: React.FC<ILearningModalProps> = ({
  closeLearningModal,
}: ILearningModalProps): JSX.Element => {
  const { t } = useTranslation();
  const modalProps = useModal("learning-modal");

  const handleSubmit = useCallback((values: { source: string }): void => {
    const tutorials = "https://training.fluidattacks.tech/collections";
    const demo =
      "https://calendar.google.com/calendar/u/0/appointments/schedules/AcZssZ2risTBon9b0rvtvpNr-rrMzYRIjcxjcaBGOxkAZUOjO8CtA8RBTVpp_4LT-t2UgyPbf4CclXS8";
    if (values.source === "VIDEO_TUTORIALS") {
      openUrl(tutorials);
    } else {
      openUrl(demo);
    }
  }, []);

  return (
    <Modal
      description={t("navbar.help.options.learn.description")}
      modalRef={{
        ...modalProps,
        close: closeLearningModal,
        isOpen: true,
      }}
      size={"md"}
      title={t("navbar.help.options.learn.title")}
    >
      <Form
        cancelButton={{ onClick: closeLearningModal }}
        confirmButton={{
          label: t("navbar.help.options.learn.confirmBtn"),
        }}
        defaultValues={{
          source: "",
        }}
        onSubmit={handleSubmit}
      >
        <InnerForm>
          {({ register }): JSX.Element => (
            <Container display={"flex"} flexDirection={"row"} gap={0.75}>
              <CardWithSelector
                icon={"file-certificate"}
                register={register}
                selectorProps={{ name: "source", value: "VIDEO_TUTORIALS" }}
                title={t("navbar.help.options.learn.videoTutorials")}
                width={"100%"}
              />
              <CardWithSelector
                icon={"chalkboard-user"}
                register={register}
                selectorProps={{ name: "source", value: "LIVE_DEMO" }}
                title={t("navbar.help.options.learn.liveDemo")}
                width={"100%"}
              />
            </Container>
          )}
        </InnerForm>
      </Form>
    </Modal>
  );
};

export type { ILearningModalProps };
export { LearningModal };
