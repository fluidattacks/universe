import { screen } from "@testing-library/react";

import { FreeTrialEnd } from ".";
import { render } from "mocks";

describe("freeTrialEnd", (): void => {
  it("should render a component", (): void => {
    expect.hasAssertions();

    render(<FreeTrialEnd isOpen={true} />);

    expect(
      screen.queryByText("autoenrollment.freeTrialEnd.title"),
    ).toBeInTheDocument();
  });

  it("should not render a component", (): void => {
    expect.hasAssertions();

    render(<FreeTrialEnd isOpen={false} />);

    expect(
      screen.queryByText("autoenrollment.freeTrialEnd.title"),
    ).not.toBeInTheDocument();
  });
});
