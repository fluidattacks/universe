import type { ApolloQueryResult, OperationVariables } from "@apollo/client";
import type { PureAbility } from "@casl/ability";

import type { GetFindingsQueryQuery } from "gql/graphql";
import type { IFindingAttr } from "pages/group/findings/types";

interface IFindingsFilters {
  featureActive?: boolean;
  findings: IFindingAttr[];
  groupName: string;
  cvssVersion: "maxOpenSeverityScore" | "maxOpenSeverityScoreV4";
  permissions: PureAbility<string>;
  refetch: (
    variables?: Partial<OperationVariables>,
  ) => Promise<ApolloQueryResult<GetFindingsQueryQuery>>;
  setFilteredFindings: React.Dispatch<React.SetStateAction<IFindingAttr[]>>;
}

export type { IFindingsFilters };
