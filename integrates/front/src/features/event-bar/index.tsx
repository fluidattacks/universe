import { useQuery } from "@apollo/client";
import { Alert, Container, Tooltip } from "@fluidattacks/design";
import dayjs from "dayjs";
import _ from "lodash";
import { useEffect, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";

import { GET_ORG_EVENTS } from "./queries";
import type { IEvent, IEventBarProps } from "./types";

import { getRemainingDays } from "utils/date";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";

const TIME = 12;
const oneThousand = 1000;

const EventBar: React.FC<IEventBarProps> = ({
  bgColor,
  organizationName,
  pb,
  pl,
  pr,
  pt,
}): JSX.Element | undefined => {
  const { t } = useTranslation();
  const [showBox, setShowBox] = useState(true);

  const { data } = useQuery(GET_ORG_EVENTS, {
    context: { skipGlobalErrorHandler: true },
    errorPolicy: "all",
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        if (error.message !== "is_under_review") {
          Logger.warning("An error occurred loading event bar data", error);
          msgError(t("groupAlerts.errorTextsad"));
        }
      });
    },
    variables: { organizationName },
  });

  const groups = data === undefined ? [] : data.organizationId.groups;
  const events = groups.reduce<IEvent[]>(
    (previousValue, currentValue): IEvent[] => [
      ...previousValue,
      ...((currentValue.events ?? []) as IEvent[]),
    ],
    [],
  );
  const openEvents = events.filter(
    (event): boolean => event.eventStatus === "CREATED",
  );
  const hasOpenEvents = openEvents.length > 0;

  const oldestDate = hasOpenEvents
    ? _.sortBy(openEvents, "eventDate")[0].eventDate
    : dayjs().toString();

  const vulnGroups = Object.keys(_.countBy(openEvents, "groupName"));

  const eventMessage = t("group.events.eventBar.message", {
    openEvents: openEvents.length,
    timeInDays: Math.abs(getRemainingDays(oldestDate)),
    vulnGroups: vulnGroups.length,
  });

  const tooltipMessage = t("group.events.eventBar.tooltip", {
    groups: vulnGroups.join(", "),
  });

  useEffect((): VoidFunction => {
    const timeout = setTimeout((): void => {
      if (showBox) {
        setShowBox(false);
      }
    }, TIME * oneThousand);

    return (): void => {
      clearTimeout(timeout);
    };
  }, [showBox]);

  if (!hasOpenEvents) return undefined;

  return (
    <Container
      bgColor={bgColor}
      display={showBox ? "block" : "none"}
      pb={pb}
      pl={pl}
      pr={pr}
      pt={pt}
    >
      <Tooltip display={"block"} id={"eventBarTooltip"} tip={tooltipMessage}>
        <Link to={`/orgs/${organizationName}/groups`}>
          <Alert autoHide={true} time={TIME}>
            {eventMessage}
          </Alert>
        </Link>
      </Tooltip>
    </Container>
  );
};

export { EventBar };
