import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql/error";
import { HttpResponse, type StrictResponse, graphql } from "msw";

import { GET_ORG_EVENTS } from "./queries";

import { EventBar } from ".";
import type { GetOrganizationEventsQuery as GetOrganizationEvents } from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";

describe("eventBar", (): void => {
  it("should render", async (): Promise<void> => {
    expect.hasAssertions();

    const organizationName = "testOrgName";
    render(<EventBar organizationName={organizationName} />, {
      mocks: [
        graphql
          .link(LINK)
          .query(
            GET_ORG_EVENTS,
            (): StrictResponse<{ data: GetOrganizationEvents }> => {
              return HttpResponse.json({
                data: {
                  organizationId: {
                    __typename: "Organization",
                    groups: [
                      {
                        __typename: "Group",
                        events: [
                          {
                            __typename: "Event",
                            eventDate: "2022-04-02 03:02:00",
                            eventStatus: "CREATED",
                            groupName: "group1",
                            id: "group1-event0",
                          },
                          {
                            __typename: "Event",
                            eventDate: "2022-05-01 06:18:00",
                            eventStatus: "CREATED",
                            groupName: "group1",
                            id: "group1-event1",
                          },
                        ],
                        name: "group1",
                      },
                      {
                        __typename: "Group",
                        events: [
                          {
                            __typename: "Event",
                            eventDate: "2022-02-01 09:30:00",
                            eventStatus: "SOLVED",
                            groupName: "group2",
                            id: "group2-event0",
                          },
                        ],
                        name: "group2",
                      },
                    ],
                    name: organizationName,
                  },
                },
              });
            },
          ),
      ],
    });

    await waitFor((): void => {
      expect(
        screen.queryByText("group.events.eventBar.message"),
      ).toBeInTheDocument();
    });

    expect(screen.queryByRole("link")).toBeInTheDocument();
    expect(screen.getByRole("link")).toHaveAttribute(
      "href",
      `/orgs/${organizationName}/groups`,
    );

    await userEvent.hover(screen.getByText("group.events.eventBar.message"));

    await waitFor((): void => {
      expect(
        screen.queryByText("group.events.eventBar.tooltip"),
      ).toBeInTheDocument();
    });
  });

  it("should render with error", async (): Promise<void> => {
    expect.hasAssertions();

    const organizationName = "orgNameTwo";
    render(<EventBar organizationName={organizationName} />, {
      mocks: [
        graphql
          .link(LINK)
          .query(
            GET_ORG_EVENTS,
            (): StrictResponse<
              IErrorMessage | { data: GetOrganizationEvents }
            > => {
              return HttpResponse.json({
                data: {
                  organizationId: {
                    __typename: "Organization",
                    groups: [
                      {
                        __typename: "Group",
                        events: [
                          {
                            __typename: "Event",
                            eventDate: "2022-02-01 09:30:00",
                            eventStatus: "CREATED",
                            groupName: "group2",
                            id: "group2-event0",
                          },
                        ],
                        name: "group2",
                      },
                      {
                        __typename: "Group",
                        events: null,
                        name: "group1",
                      },
                    ],
                    name: organizationName,
                  },
                },
                errors: [new GraphQLError("is_under_review")],
              });
            },
          ),
      ],
    });

    await waitFor((): void => {
      expect(
        screen.queryByText("group.events.eventBar.message"),
      ).toBeInTheDocument();
    });

    expect(screen.queryByRole("link")).toBeInTheDocument();
    expect(screen.getByRole("link")).toHaveAttribute(
      "href",
      `/orgs/${organizationName}/groups`,
    );

    await userEvent.hover(screen.getByText("group.events.eventBar.message"));

    await waitFor((): void => {
      expect(
        screen.queryByText("group.events.eventBar.tooltip"),
      ).toBeInTheDocument();
    });
  });
});
