import { PureAbility } from "@casl/ability";
import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import {
  type AnyObject,
  type InferType,
  type Schema,
  ValidationError,
} from "yup";

import { Comment } from "./comment";
import { CommentEditor } from "./comment-editor";
import { validationSchema } from "./comment-editor/validations";
import { commentContext } from "./context";
import type { ICommentStructure } from "./types";

import { Comments } from ".";
import { authzGroupContext } from "context/authz/config";
import { useCalendly } from "hooks/use-calendly";
import { render } from "mocks";

jest.mock("hooks/use-calendly");

describe("comments section", (): void => {
  const onPostComment: jest.Mock = jest.fn();

  const mockComment: ICommentStructure = {
    content: "Hello world",
    created: "2021/04/20 00:00:01",
    createdByCurrentUser: true,
    email: "unittest@fluidattacks.com",
    fullName: "Test User",
    id: 1337260012345,
    modified: "2021/04/20 00:00:01",
    parentComment: 0,
  };

  const testValidation = (obj: AnyObject): InferType<Schema> => {
    return validationSchema.validateSync(obj);
  };

  it("should render an empty comment section essential plan", (): void => {
    expect.hasAssertions();

    const closeUpgradeModalMock = jest.fn();
    const openUpgradeModalMock = jest.fn();
    jest.mocked(useCalendly).mockReturnValue({
      closeUpgradeModal: closeUpgradeModalMock,
      isAdvancedActive: false,
      isAvailable: true,
      isUpgradeOpen: true,
      openUpgradeModal: openUpgradeModalMock,
      widgetProps: {
        prefill: {
          customAnswers: {
            a1: "",
          },
          email: "",
          name: "",
        },
        url: "",
      },
    });

    render(
      <Comments
        comments={[]}
        isFindingComment={true}
        isObservation={true}
        onPostComment={jest.fn()}
      />,
    );

    expect(screen.queryByRole("button")).not.toBeInTheDocument();
    expect(screen.getByRole("img", { name: "empty-icon" })).toBeInTheDocument();
    expect(screen.queryByText("comments.noComments.title")).toBeInTheDocument();
    expect(
      screen.queryByText("comments.noComments.description"),
    ).toBeInTheDocument();
  });

  it("should render an empty comment section advanced plan", async (): Promise<void> => {
    expect.hasAssertions();

    const closeUpgradeModalMock = jest.fn();
    const openUpgradeModalMock = jest.fn();
    jest.mocked(useCalendly).mockReturnValue({
      closeUpgradeModal: closeUpgradeModalMock,
      isAdvancedActive: true,
      isAvailable: true,
      isUpgradeOpen: true,
      openUpgradeModal: openUpgradeModalMock,
      widgetProps: {
        prefill: {
          customAnswers: {
            a1: "",
          },
          email: "",
          name: "",
        },
        url: "",
      },
    });

    render(
      <Comments
        comments={[]}
        isFindingComment={true}
        isObservation={true}
        onPostComment={jest.fn()}
      />,
    );

    expect(screen.getAllByRole("button")).toHaveLength(1);
    expect(screen.queryByText("comments.noComments.title")).toBeInTheDocument();

    await userEvent.click(screen.getByText("comments.add"));

    expect(screen.getByRole("textbox")).toBeInTheDocument();

    await userEvent.clear(screen.getByRole("textbox"));
    await userEvent.type(screen.getByRole("textbox"), "test comment");
  });

  it("should post a comment", async (): Promise<void> => {
    expect.hasAssertions();

    render(<CommentEditor onPost={onPostComment} />);

    expect(screen.getByRole("textbox")).toBeInTheDocument();
    expect(screen.queryByText("comments.send")).not.toBeInTheDocument();

    await userEvent.clear(screen.getByRole("textbox"));
    await userEvent.type(screen.getByRole("textbox"), "test comment");
    await waitFor((): void => {
      expect(screen.queryByText("comments.send")).toBeInTheDocument();
    });
    await userEvent.click(screen.getByText("comments.send"));
    await waitFor((): void => {
      expect(onPostComment).toHaveBeenCalledTimes(1);
    });
  });

  it("should not post a comment", async (): Promise<void> => {
    expect.hasAssertions();

    render(<CommentEditor onPost={onPostComment} />);

    expect(screen.getByRole("textbox")).toBeInTheDocument();
    expect(screen.queryByText("comments.send")).not.toBeInTheDocument();

    await userEvent.clear(screen.getByRole("textbox"));
    await userEvent.type(screen.getByRole("textbox"), "=¬\\");

    await waitFor((): void => {
      expect(screen.queryByText("comments.send")).toBeInTheDocument();
    });
    await userEvent.click(screen.getByText("comments.send"));

    await waitFor(async (): Promise<void> => {
      await expect(
        screen.findByText(/Field cannot begin with the following character:/u),
      ).resolves.toBeInTheDocument();
    });
  });

  it("should not post a blank comment", async (): Promise<void> => {
    expect.hasAssertions();

    render(<CommentEditor onPost={onPostComment} />);

    expect(screen.getByRole("textbox")).toBeInTheDocument();
    expect(screen.queryByText("comments.send")).not.toBeInTheDocument();

    await userEvent.clear(screen.getByRole("textbox"));
    await userEvent.type(screen.getByRole("textbox"), "              ");

    await waitFor((): void => {
      expect(screen.queryByText("comments.send")).toBeInTheDocument();
    });
    await userEvent.click(screen.getByText("comments.send"));

    await waitFor((): void => {
      expect(screen.queryByText("comments.send")).not.toBeInTheDocument();
    });
  });

  it("should render a single comment", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <authzGroupContext.Provider
        value={new PureAbility([{ action: "has_advanced" }])}
      >
        <commentContext.Provider value={{ replying: mockComment.id }}>
          <Comment
            backgroundEnabled={false}
            comments={[mockComment]}
            id={mockComment.id}
            isObservation={false}
            onPost={jest.fn()}
            orderBy={"newest"}
          />
        </commentContext.Provider>
      </authzGroupContext.Provider>,
    );

    await userEvent.click(screen.getByText("comments.reply"));

    await waitFor((): void => {
      expect(
        screen.getByPlaceholderText("comments.editorPlaceholder"),
      ).toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(
        screen.queryAllByPlaceholderText("comments.editorPlaceholder"),
      ).toHaveLength(1);
    });
  });

  it("should display comment editor validation errors", (): void => {
    expect.hasAssertions();

    const groupName = "unittesting";

    const testInvalidTextBeginning = (): void => {
      testValidation({ "comment-editor": "=", confirmation: groupName });
    };
    const testInvalidTextField = (): void => {
      testValidation({
        "comment-editor": "Test ^ invalid character",
      });
    };

    expect(testInvalidTextBeginning).toThrow(
      new ValidationError(
        "Field cannot begin with the following character: '='",
      ),
    );
    expect(testInvalidTextField).toThrow(
      new ValidationError("Field cannot contain the following characters: '^'"),
    );
  });
});
