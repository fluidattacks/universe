import { Button } from "@fluidattacks/design";
import type { FormikProps } from "formik";
import { Form } from "formik";
import { useCallback } from "react";
import { useHotkeys } from "react-hotkeys-hook";
import { useTranslation } from "react-i18next";

import { MAX_LENGTH } from "../validations";
import { TextArea } from "components/input";
import { Logger } from "utils/logger";

const InnerForm = ({
  values,
  handleSubmit,
  setFieldValue,
}: FormikProps<{ "comment-editor": string }>): JSX.Element => {
  const { t } = useTranslation();
  const formatCommentInplace = useCallback((): void => {
    setFieldValue("comment-editor", values["comment-editor"].trim()).catch(
      (): void => {
        Logger.error("An error occurred copying text");
      },
    );
  }, [setFieldValue, values]);

  const handleHotkey = useCallback((): void => {
    formatCommentInplace();
    handleSubmit();
  }, [formatCommentInplace, handleSubmit]);

  useHotkeys("ctrl+enter", handleHotkey, { enableOnFormTags: ["TEXTAREA"] });

  return (
    <Form>
      <TextArea
        maxLength={MAX_LENGTH}
        name={"comment-editor"}
        onBlur={formatCommentInplace}
        placeholder={t("comments.editorPlaceholder")}
        rows={3}
      />
      {values["comment-editor"].length > 0 ? (
        <div className={"pv2"}>
          <Button type={"submit"}>{t("comments.send")}</Button>
        </div>
      ) : null}
    </Form>
  );
};

export { InnerForm };
