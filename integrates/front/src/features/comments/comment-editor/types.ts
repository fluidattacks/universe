interface ICommentEditorProps {
  onPost: (editorText: string) => void;
}

interface ICommentForm {
  "comment-editor": string;
}

export type { ICommentEditorProps, ICommentForm };
