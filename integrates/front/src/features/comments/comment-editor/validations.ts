import type { FormikHelpers } from "formik";
import { object, string } from "yup";

import type { ICommentForm } from "./types";

const MAX_LENGTH = 20000;

const validationSchema = object().shape({
  "comment-editor": string().isValidTextBeginning().isValidTextField(),
});

const validatePreSubmit = (
  values: ICommentForm,
  formikHelpers: FormikHelpers<ICommentForm>,
): { data: ICommentForm; error: Error | undefined } => {
  const comment: string = values["comment-editor"].trim();

  if (comment.length === 0) {
    formikHelpers.setErrors({
      "comment-editor": "Field cannot be empty",
    });

    return {
      data: values,
      error: new Error("Field cannot be empty"),
    };
  }

  const data = { "comment-editor": comment };

  return {
    data,
    error: undefined,
  };
};

export { MAX_LENGTH, validationSchema, validatePreSubmit };
