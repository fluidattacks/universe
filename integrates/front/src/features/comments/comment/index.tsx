import { Button, Container, Link, Text } from "@fluidattacks/design";
import Linkify from "linkify-react";
import _ from "lodash";
import { StrictMode, useCallback, useContext } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

import { CommentEditor } from "../comment-editor";
import { commentContext } from "../context";
import type { ICommentContext, ICommentStructure } from "../types";
import { Have } from "context/authz/have";
import { formatDateTime } from "utils/date";

interface ICommentProps {
  id: number;
  comments: ICommentStructure[];
  onPost: (editorText: string) => void;
  backgroundEnabled: boolean;
  orderBy: string;
  isObservation: boolean;
}

const Comment = ({
  id,
  comments,
  onPost,
  backgroundEnabled,
  orderBy,
  isObservation = false,
}: Readonly<ICommentProps>): JSX.Element => {
  const { t } = useTranslation();
  const { replying, setReplying }: ICommentContext = useContext(commentContext);
  const theme = useTheme();

  const isValidUrl = (text: string): boolean => {
    try {
      const url = new URL(text);

      return /^https?:\/\//u.test(url.href);
    } catch {
      return false;
    }
  };

  const rootComment: ICommentStructure = _.find(comments, [
    "id",
    id,
  ]) as ICommentStructure;
  const childrenComments: ICommentStructure[] = _.filter(comments, [
    "parentComment",
    id,
  ]);

  const orderComments = (
    unordered: ICommentStructure[],
    order: string,
  ): ICommentStructure[] => {
    return order === "oldest"
      ? _.orderBy(unordered, ["created"], ["asc"])
      : _.orderBy(unordered, ["created"], ["desc"]);
  };

  const replyHandler = useCallback((): void => {
    if (!_.isUndefined(setReplying)) {
      if (replying === id) {
        setReplying(0);
      } else {
        setReplying(id);
      }
    }
  }, [id, replying, setReplying]);

  const formatLinks = useCallback(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    ({ attributes, content }: any): React.ReactNode => {
      const { href, key } = attributes;

      return (
        <Link href={href} key={key}>
          {content}
        </Link>
      );
    },
    [],
  );

  return (
    <StrictMode>
      <Container>
        <Container
          alignItems={"start"}
          bgColor={theme.palette.gray[100]}
          border={`1px solid ${theme.palette.gray[300]}`}
          borderRadius={"4px"}
          display={"flex"}
          flexDirection={"column"}
          gap={0.5}
          pb={1.25}
          pl={1.25}
          pr={1.25}
          pt={1.25}
        >
          <Container display={"flex"} justify={"space-between"} width={"100%"}>
            <Text fontWeight={"bold"} size={"sm"}>
              {rootComment.createdByCurrentUser ? `You` : rootComment.fullName}
            </Text>
            <Container>
              <Text fontWeight={"bold"} size={"sm"} whiteSpace={"nowrap"}>
                {formatDateTime(rootComment.created)}
              </Text>
            </Container>
          </Container>
          <Container>
            <Linkify
              options={{
                render: {
                  url: formatLinks,
                },
                validate: {
                  url: isValidUrl,
                },
              }}
            >
              <Text
                size={"sm"}
                whiteSpace={"pre-wrap"}
                wordBreak={"break-word"}
              >
                {_.trim(rootComment.content)}
              </Text>
            </Linkify>
          </Container>
          <Have I={"has_advanced"} passThrough={isObservation}>
            <Button onClick={replyHandler}>{t("comments.reply")}</Button>
          </Have>
          {replying === rootComment.id && (
            <Container mt={1.25} width={"100%"}>
              <CommentEditor onPost={onPost} />
            </Container>
          )}
        </Container>
        <Container borderLeft={`2px solid ${theme.palette.gray[300]}`} pl={0.5}>
          {childrenComments.length > 0
            ? orderComments(childrenComments, orderBy).map(
                (childComment): JSX.Element => (
                  <Comment
                    backgroundEnabled={!backgroundEnabled}
                    comments={comments}
                    id={childComment.id}
                    isObservation={isObservation}
                    key={childComment.id}
                    onPost={onPost}
                    orderBy={orderBy}
                  />
                ),
              )
            : undefined}
        </Container>
      </Container>
    </StrictMode>
  );
};

export type { ICommentProps };
export { Comment };
