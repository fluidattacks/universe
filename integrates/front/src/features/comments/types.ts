import type { IEmptyProps } from "@fluidattacks/design/dist/components/empty-state/types";

interface ICommentResult {
  content: string;
  created: string;
  email: string;
  fullName: string;
  id: string;
  modified: string;
  parentComment: string;
}

interface ICommentStructure
  extends Omit<ICommentResult, "id" | "parentComment"> {
  createdByCurrentUser: boolean;
  id: number;
  parentComment: number;
}

interface ICommentsProps {
  comments: ICommentStructure[];
  emptyComponentsProps?: Partial<IEmptyProps>;
  onPostComment: (comment: ICommentStructure) => void;
  isFindingComment?: boolean;
  isObservation?: boolean;
}

interface ICommentContext {
  replying: number;
  setReplying?: React.Dispatch<React.SetStateAction<number>>;
}

export type {
  ICommentStructure,
  ICommentContext,
  ICommentsProps,
  ICommentResult,
};
