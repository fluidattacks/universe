import { useQuery } from "@apollo/client";
import camelCase from "lodash/camelCase";
import { useTranslation } from "react-i18next";
import { useMatch } from "react-router-dom";

import { GET_USER_ROLE } from "./queries";
import { getLevel } from "./utils";

const Role = (): string => {
  const { t } = useTranslation();

  const orgMatch = useMatch("/orgs/:organizationName/*");
  const groupMatch = useMatch("/orgs/:organizationName/groups/:groupName/*");
  const { groupName, organizationName } = {
    ...(orgMatch?.params ?? {}),
    ...(groupMatch?.params ?? {}),
  };
  const level = getLevel(groupName, organizationName);

  const { data } = useQuery(GET_USER_ROLE, {
    variables: {
      groupLevel: level === "group",
      groupName: groupName ?? "",
      organizationLevel: level === "organization",
      organizationName: organizationName ?? "",
      userLevel: level === "user",
    },
  });

  if (data === undefined) {
    return "";
  }

  const role = {
    group: data.group?.userRole,
    organization: data.organizationId?.userRole,
    user: data.me?.role,
  }[level];

  return t(`userModal.roles.${camelCase(role ?? "")}`);
};

export { Role };
