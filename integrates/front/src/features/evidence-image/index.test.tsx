import { Button } from "@fluidattacks/design";
import { fireEvent, screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { Form, Formik } from "formik";

import { EvidencePreview as EvidenceImage } from ".";
import { render } from "mocks";
import { findingEvidenceSchema } from "pages/finding/evidence/utils";

describe("evidence image", (): void => {
  const btnConfirm = "Confirm";
  const handlePreview: jest.Mock = jest.fn();
  // eslint-disable-next-line functional/immutable-data -- Mutation needed for the test
  window.URL.createObjectURL = handlePreview;
  const evidences = {
    image1: {
      authorEmail: "",
      date: "",
      description: "",
      isDraft: false,
      url: "https://test.com/test.png",
    },
  };

  it("should render img and description", (): void => {
    expect.hasAssertions();

    render(
      <Formik
        initialValues={{
          evidence1: {
            date: "",
            description: "",
            url: "",
          },
        }}
        onSubmit={jest.fn()}
      >
        <Form>
          <EvidenceImage
            acceptedMimes={"image/*"}
            description={"Test evidence"}
            evidences={evidences}
            fileName={"test.png"}
            index={0}
            instance={{ id: "00000", name: "Finding" }}
            isDescriptionEditable={false}
            isEditing={false}
            isRefetching={false}
            name={"evidence1"}
            onDownload={jest.fn()}
            setOpenImageViewer={jest.fn()}
          />
        </Form>
      </Formik>,
    );

    expect(screen.getByRole("img")).toBeInTheDocument();
    expect(
      screen.getByText("searchFindings.tabEvidence.detail"),
    ).toBeInTheDocument();
    expect(screen.getByText("Test evidence")).toBeInTheDocument();
    expect(screen.queryByRole("textbox")).not.toBeInTheDocument();
  });

  it("should render as editable", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Formik
        initialValues={{
          evidence1: {
            date: "",
            description: "",
            url: "",
          },
        }}
        onSubmit={jest.fn()}
      >
        <Form>
          <EvidenceImage
            acceptedMimes={"image/*"}
            description={"Test evidence"}
            evidences={evidences}
            fileName={"test.png"}
            index={0}
            instance={{ id: "00000", name: "Finding" }}
            isDescriptionEditable={true}
            isEditing={true}
            isRefetching={false}
            name={"evidence1"}
            onDownload={jest.fn()}
            setOpenImageViewer={jest.fn()}
          />
        </Form>
      </Formik>,
    );

    const descriptionInput = screen.getByRole("textbox");

    expect(descriptionInput).toHaveAttribute("name", "evidence1.description");

    expect(screen.getByText("0/5000")).toBeInTheDocument();

    await userEvent.type(descriptionInput, "New description");

    expect(screen.getByText("15/5000")).toBeInTheDocument();
  });

  it("should execute callbacks", async (): Promise<void> => {
    expect.hasAssertions();

    const handleUpdate: jest.Mock = jest.fn();
    const file: File[] = [new File([""], "image.png", { type: "image/png" })];
    const image0 = new File(["nonvalidone"], "nonvalidone.png", {
      type: "image/png",
    });
    render(
      <Formik
        initialValues={{
          evidence1: { date: "", description: "", file, url: "" },
        }}
        name={"editEvidences"}
        onSubmit={handleUpdate}
      >
        <Form>
          <EvidenceImage
            acceptedMimes={"application/*, text/*"}
            description={"Test evidence"}
            evidences={evidences}
            fileName={"file"}
            index={0}
            instance={{ id: "00000", name: "Finding" }}
            isDescriptionEditable={true}
            isDocument={true}
            isEditing={true}
            isRefetching={false}
            name={"evidence1"}
            onDownload={jest.fn()}
            setOpenImageViewer={jest.fn()}
          />
          <Button type={"submit"} variant={"primary"}>
            {btnConfirm}
          </Button>
        </Form>
      </Formik>,
    );

    expect(screen.queryByRole("textbox")).toBeInTheDocument();

    await userEvent.clear(
      screen.getByRole("textbox", { name: "evidence1.description" }),
    );
    await userEvent.type(
      screen.getByRole("textbox", { name: "evidence1.description" }),
      "New description",
    );
    await userEvent.upload(screen.getByTestId("evidence1.file"), image0);
    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.tabEvidence.fields.modal.title"),
      ).not.toBeInTheDocument();
    });

    await userEvent.click(screen.getByText(btnConfirm));
    await waitFor((): void => {
      expect(handleUpdate).toHaveBeenCalledTimes(1);
    });

    jest.clearAllMocks();
  });

  it("should execute callbacks with preview", async (): Promise<void> => {
    expect.hasAssertions();

    const handleClick: jest.Mock = jest.fn();
    const handleUpdate: jest.Mock = jest.fn();

    const image0 = new File(
      ["orgimage-groupimage-01#34b6789.csv"],
      "orgimage-groupimage-01#34b6789.csv",
      {
        type: "text/csv",
      },
    );
    const image1 = new File(
      ["orgimage-groupimage-01#34b6789.png"],
      "orgimage-groupimage-01#34b6789.png",
      {
        type: "image/png",
      },
    );
    const image2 = new File(
      ["orgimage-groupimage-01234b6789"],
      "orgimage-groupimage-012345g789.png",
      { type: "image/png" },
    );

    render(
      <Formik
        initialValues={{
          evidence1: {
            authorEmail: "",
            date: "",
            description: "",
            url: "",
          },
        }}
        name={"editEvidences"}
        onSubmit={handleUpdate}
        validationSchema={findingEvidenceSchema("orgimage", "groupimage", {
          evidence1: {
            authorEmail: "",
            date: "",
            description: "",
            isDraft: false,
            url: "",
          },
        })}
      >
        <Form>
          <EvidenceImage
            acceptedMimes={"image/*"}
            description={"Test evidence"}
            evidences={evidences}
            fileName={"test.png"}
            index={0}
            instance={{ id: "00000", name: "Finding" }}
            isDescriptionEditable={true}
            isEditing={true}
            isRefetching={false}
            isRemovable={true}
            name={"evidence1"}
            onDownload={jest.fn()}
            setOpenImageViewer={handleClick}
          />
          <Button type={"submit"} variant={"primary"}>
            {btnConfirm}
          </Button>
        </Form>
      </Formik>,
    );

    expect(screen.queryByRole("textbox")).toBeInTheDocument();

    await userEvent.clear(
      screen.getByRole("textbox", { name: "evidence1.description" }),
    );
    await userEvent.type(
      screen.getByRole("textbox", { name: "evidence1.description" }),
      "New description",
    );

    expect(
      screen.queryByText("searchFindings.tabEvidence.fields.modal.title"),
    ).not.toBeInTheDocument();
    expect(screen.getAllByRole("img", { name: "evidence1" })).toHaveLength(1);

    await userEvent.upload(screen.getByTestId("evidence1.file"), image0);

    expect(
      screen.queryByText("searchFindings.tabEvidence.fields.modal.title"),
    ).not.toBeInTheDocument();
    expect(screen.getByTestId("evidence1.file")).toHaveValue("");

    await userEvent.upload(screen.getByTestId("evidence1.file"), image1);
    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.tabEvidence.fields.modal.title"),
      ).toBeInTheDocument();
    });

    expect(
      screen.queryByText(
        "Evidence name must have the following format " +
          "organizationName-groupName-10 alphanumeric chars.extension",
      ),
    ).toBeInTheDocument();

    fireEvent.click(screen.getByText("Cancel"));

    expect(
      screen.queryByText("searchFindings.tabEvidence.fields.modal.title"),
    ).not.toBeInTheDocument();

    await userEvent.upload(screen.getByTestId("evidence1.file"), image2);
    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.tabEvidence.fields.modal.title"),
      ).toBeInTheDocument();
    });

    expect(screen.getAllByText(btnConfirm)[1]).not.toBeDisabled();
    expect(
      screen.queryByText(
        "Evidence name must have the following format " +
          "organizationName-groupName-10 alphanumeric chars.extension",
      ),
    ).not.toBeInTheDocument();

    fireEvent.click(screen.getAllByText(btnConfirm)[1]);
    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.tabEvidence.fields.modal.title"),
      ).not.toBeInTheDocument();
    });

    await userEvent.click(screen.getByText(btnConfirm));
    await waitFor((): void => {
      expect(handleUpdate).toHaveBeenCalledTimes(1);
    });

    jest.clearAllMocks();
  });
});
