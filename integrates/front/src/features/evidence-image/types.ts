import type { ApolloQueryResult, OperationVariables } from "@apollo/client";
import type { IFilePreviewProps } from "@fluidattacks/design";

import type { GetFindingEvidencesQuery } from "gql/graphql";
import type { IEventEvidenceAttr } from "pages/event/evidence/types";
import type { IEvidenceItem } from "pages/finding/evidence/types";

interface IEvidencePreviewProps {
  acceptedMimes: string;
  fileName?: string;
  findingId?: string;
  isDocument?: boolean;
  index?: number;
  authorEmail?: string;
  date?: string;
  description: string;
  evidences: Record<string, IEvidenceItem | IEventEvidenceAttr>;
  instance: { name: "Event" | "Finding"; id: string };
  isDescriptionEditable: boolean;
  isDraft?: boolean;
  isEditing: boolean;
  isRefetching: boolean;
  isRemovable?: boolean;
  name: string;
  onApprove?: () => void;
  onDelete?: () => void;
  onDownload?: () => void;
  evidenceType?: IFilePreviewProps["fileType"];
  refetch?: (
    variables?: Partial<OperationVariables>,
  ) => Promise<ApolloQueryResult<GetFindingEvidencesQuery>>;
  setEditing?: React.Dispatch<React.SetStateAction<boolean>>;
  setOpenImageViewer?: (index: number) => void;
}

export type { IEvidencePreviewProps };
