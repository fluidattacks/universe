import type { IFilePreviewProps } from "@fluidattacks/design";

import type { IEvidencePreviewProps } from "../types";

type TFIleType = IFilePreviewProps["fileType"];

interface IEvidenceFormProps extends Partial<IEvidencePreviewProps> {
  filesTypeMapper: Record<string, TFIleType>;
  isRemovable?: boolean;
  setPreviewUrl: React.Dispatch<React.SetStateAction<string>>;
  setFileType: React.Dispatch<React.SetStateAction<TFIleType | undefined>>;
  shouldRenderPreview: boolean;
}

export type { IEvidenceFormProps };
