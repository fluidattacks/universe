import {
  Button,
  Container,
  FilePreview,
  Text,
  useConfirmDialog,
} from "@fluidattacks/design";
import { useFormikContext } from "formik";
import _ from "lodash";
import { useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import type { IEvidenceFormProps } from "./types";
import { getFileExtension } from "./utils";

import { Editable, InputFile, TextArea } from "components/input";
import { Logger } from "utils/logger";

const MAX_DESCRIPTION_LENGTH = 5000;

const EvidenceForm = ({
  acceptedMimes,
  description,
  isDescriptionEditable = false,
  isRemovable = false,
  name,
  onDelete,
  setPreviewUrl,
  setFileType,
  filesTypeMapper,
  shouldRenderPreview,
}: Readonly<IEvidenceFormProps>): JSX.Element => {
  const { t } = useTranslation();
  const { setFieldTouched, setFieldValue } =
    useFormikContext<Record<string, unknown>>();

  const { confirm, ConfirmDialog } = useConfirmDialog();
  const handleChange = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>): void => {
      const files = event.target.value as unknown as FileList | undefined;
      if (files) {
        const preview = {
          name: files[0].name,
          url: URL.createObjectURL(files[0]),
        };

        const fileExtension = getFileExtension(preview.name);

        const fileType = ["png", "webm"].includes(fileExtension)
          ? filesTypeMapper[getFileExtension(preview.name)]
          : undefined;

        confirm({
          content: (
            <Container>
              {_.isUndefined(fileType) ? (
                <Text fontWeight={"bold"} size={"sm"}>
                  {t("searchFindings.tabEvidence.fields.modal.error")}
                </Text>
              ) : (
                <FilePreview
                  alt={name ?? "Evidence"}
                  fileType={fileType}
                  src={preview.url}
                />
              )}
            </Container>
          ),
          message: t("searchFindings.tabEvidence.fields.modal.message"),
          title: t("searchFindings.tabEvidence.fields.modal.title"),
        })
          .then((result: boolean): void => {
            if (result) {
              void setFieldTouched(`${name}.file`, true);
              setFileType(fileType);
              setPreviewUrl(preview.url);
            } else {
              void setFieldValue(`${name}.file`, undefined);
            }
          })
          .catch((): void => {
            Logger.error("An error occurred confirming files");
          });
      }
    },
    [
      confirm,
      filesTypeMapper,
      name,
      setFieldTouched,
      setFileType,
      setPreviewUrl,
      setFieldValue,
      t,
    ],
  );

  return (
    <Container display={"flex"} flexDirection={"column"} gap={0.5}>
      <InputFile
        accept={acceptedMimes}
        id={name}
        name={`${name}.file`}
        onChange={shouldRenderPreview ? handleChange : undefined}
      />
      <Editable
        currentValue={description}
        isEditing={isDescriptionEditable}
        label={""}
      >
        <TextArea
          label={t("searchFindings.tabEvidence.description")}
          maxLength={MAX_DESCRIPTION_LENGTH}
          name={`${name}.description`}
          tooltip={t("searchFindings.tabEvidence.descriptionTooltip")}
        />
      </Editable>
      {isRemovable ? (
        <Button
          icon={"trash"}
          id={t("searchFindings.tabEvidence.removeTooltip.id")}
          onClick={onDelete}
          tooltip={t("searchFindings.tabEvidence.removeTooltip")}
          variant={"secondary"}
        >
          {t("buttons.delete")}
        </Button>
      ) : undefined}
      <ConfirmDialog />
    </Container>
  );
};

export { EvidenceForm };
