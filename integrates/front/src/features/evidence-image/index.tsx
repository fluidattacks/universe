import { useAbility } from "@casl/react";
import type { IFilePreviewProps } from "@fluidattacks/design";
import {
  Button,
  ButtonToolbarStartRow,
  CardWithImage,
  CloudImage,
  Container,
  Tag,
  useConfirmDialog,
} from "@fluidattacks/design";
import isEmpty from "lodash/isEmpty";
import { useCallback, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

import { EvidenceForm } from "./evidence-form";
import type { IEvidencePreviewProps } from "./types";

import { authzPermissionsContext } from "context/authz/config";
import {
  filesTypeMapper,
  getFileExtension,
} from "features/evidence-image/evidence-form/utils";
import { useEvidenceAudit } from "hooks/use-audit";
import { RejectEvidenceModal } from "pages/finding/evidence/reject-evidence-modal";
import { Logger } from "utils/logger";

const EvidencePreview = ({
  acceptedMimes,
  authorEmail,
  date,
  description,
  evidences,
  instance,
  findingId,
  fileName,
  index,
  isDescriptionEditable,
  isDraft,
  isDocument = false,
  isEditing,
  isRefetching,
  name,
  onApprove,
  onDelete,
  onDownload,
  refetch,
  setEditing,
  setOpenImageViewer,
}: Readonly<IEvidencePreviewProps>): JSX.Element => {
  const { t } = useTranslation();
  const theme = useTheme();
  const { addEvidenceAudit } = useEvidenceAudit();

  const url =
    isEmpty(fileName) || isRefetching ? "" : `${location.href}/${fileName}`;

  const { confirm, ConfirmDialog } = useConfirmDialog();
  const [isRejectModalOpen, setIsRejectModalOpen] = useState(false);
  const openRejectModal = useCallback((): void => {
    setIsRejectModalOpen(true);
  }, []);
  const closeDeleteModal = useCallback((): void => {
    setIsRejectModalOpen(false);
  }, []);

  const [previewUrl, setPreviewUrl] = React.useState<string>(url);
  const [fileType, setFileType] = React.useState<
    IFilePreviewProps["fileType"] | undefined
  >(filesTypeMapper[getFileExtension(fileName)]);
  const isImage = fileType === "image";
  const handleImageActions = useCallback((): void => {
    if (
      !isEditing &&
      !isRefetching &&
      setOpenImageViewer &&
      isImage &&
      index !== undefined
    ) {
      setOpenImageViewer(index);
    } else {
      onDownload?.();
      if (typeof fileName === "string") {
        addEvidenceAudit(evidences, fileName, instance);
      }
    }
  }, [
    isEditing,
    isRefetching,
    setOpenImageViewer,
    isImage,
    index,
    onDownload,
    addEvidenceAudit,
    evidences,
    fileName,
    instance,
  ]);

  const confirmApproval = useCallback((): void => {
    confirm({
      title: t("searchFindings.tabEvidence.approvalConfirm"),
    })
      .then((result: boolean): void => {
        if (result) {
          onApprove?.();
        }
      })
      .catch((): void => {
        Logger.error("An error occurred approving confirmation");
      });
  }, [confirm, onApprove, t]);
  const permissions = useAbility(authzPermissionsContext);
  const canReject = permissions.can(
    "integrates_api_mutations_reject_evidence_mutate",
  );

  const sendVideoAudit = useCallback(
    (stage: "start" | "half" | "end"): void => {
      if (typeof fileName === "string")
        addEvidenceAudit(evidences, fileName, instance, stage);
    },
    [addEvidenceAudit, evidences, fileName, instance],
  );

  const cardChildren = React.useMemo((): JSX.Element | undefined => {
    if (isDocument) {
      const isDownloadDisabled = isEditing && fileName === "";

      return (
        <Container
          alignItems={"center"}
          bgColor={theme.palette.gray[100]}
          display={"flex"}
          height={"147px"}
          justify={"center"}
          width={"344px"}
        >
          <Button
            disabled={isDownloadDisabled}
            icon={"download"}
            onClick={handleImageActions}
          >
            {t("searchFindings.tabEvidence.downloadFile")}
          </Button>
        </Container>
      );
    } else if (previewUrl === "") {
      return (
        <Container>
          <CloudImage
            alt={name}
            height={"147px"}
            publicId={"/integrates/resources/autoEnrollmentBg"}
            width={"344px"}
          />
        </Container>
      );
    }

    return undefined;
  }, [
    isDocument,
    isEditing,
    fileName,
    name,
    handleImageActions,
    previewUrl,
    t,
    theme.palette.gray,
  ]);

  return (
    <Container data-testid={"evidence-preview"}>
      <Container maxWidth={"335px"}>
        <CardWithImage
          alt={name}
          authorEmail={authorEmail}
          date={date}
          description={description}
          headerType={fileType}
          hideDescription={isEditing || previewUrl === ""}
          isEditing={isEditing}
          onClick={handleImageActions}
          showMaximize={isImage}
          src={fileType === undefined ? "" : previewUrl}
          title={t("searchFindings.tabEvidence.detail")}
          videoViewStatus={sendVideoAudit}
        >
          {cardChildren}
        </CardWithImage>
        <Container display={"flex"} flexDirection={"column"} gap={0.5} mt={0.5}>
          {isEditing ? (
            <EvidenceForm
              acceptedMimes={acceptedMimes}
              description={description}
              filesTypeMapper={filesTypeMapper}
              isDescriptionEditable={isDescriptionEditable}
              isRemovable={!isEmpty(fileName)}
              name={name}
              onDelete={onDelete}
              setFileType={setFileType}
              setPreviewUrl={setPreviewUrl}
              shouldRenderPreview={!isDocument}
            />
          ) : (
            <Container>
              {isDraft === true ? (
                <Tag
                  tagLabel={t("searchFindings.tabEvidence.draftEvidence")}
                  variant={"inactive"}
                />
              ) : undefined}
            </Container>
          )}
          {findingId !== undefined && setEditing && refetch ? (
            <RejectEvidenceModal
              evidenceId={name}
              findingId={findingId}
              isOpen={isRejectModalOpen}
              onClose={closeDeleteModal}
              refetch={refetch}
              setEditing={setEditing}
            />
          ) : undefined}
          {isDraft === true && onApprove && canReject ? (
            <ButtonToolbarStartRow>
              <Button
                icon={"check"}
                onClick={confirmApproval}
                variant={"primary"}
              >
                {t("searchFindings.tabEvidence.approve")}
              </Button>
              <Button
                icon={"check"}
                onClick={openRejectModal}
                variant={"tertiary"}
              >
                {t("searchFindings.tabEvidence.reject")}
              </Button>
            </ButtonToolbarStartRow>
          ) : undefined}
        </Container>
      </Container>
      <ConfirmDialog />
    </Container>
  );
};

export { EvidencePreview };
export type { IEvidencePreviewProps };
