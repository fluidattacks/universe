import { Alert, Container, Link, Loading } from "@fluidattacks/design";
import * as React from "react";
import { useTranslation } from "react-i18next";

import type { IImportMailmapModalProps } from "../types";
import { InnerForm } from "components/form";
import { FormModal } from "components/form-modal";
import { InputFile } from "components/input";

const ImportMailmapModal: React.FC<IImportMailmapModalProps> = ({
  onClose,
  onSubmit,
  modalRef,
  submitting,
}: IImportMailmapModalProps): JSX.Element => {
  const { t } = useTranslation();

  return (
    <FormModal
      enableReinitialize={true}
      initialValues={{ file: undefined as unknown as FileList }}
      modalRef={{ ...modalRef, close: onClose }}
      onSubmit={onSubmit}
      size={"sm"}
      title={t("mailmap.importMailmap")}
    >
      <InnerForm
        onCancel={onClose}
        submitButtonLabel={t("mailmap.importMailmap")}
        submitDisabled={submitting}
      >
        <InputFile
          accept={".txt, text/plain"}
          id={"file"}
          name={"file"}
          required={true}
        />
        <Alert variant={"info"}>
          {t("mailmap.alertImportMailmap")}&nbsp;
          <Link href={"https://git-scm.com/docs/gitmailmap#_syntax"}>
            {"mailmap syntax"}
          </Link>
        </Alert>
        {submitting ? (
          <Container
            alignItems={"center"}
            display={"flex"}
            height={"100%"}
            justify={"center"}
          >
            <Loading color={"red"} label={"Uploading..."} size={"lg"} />
          </Container>
        ) : undefined}
      </InnerForm>
    </FormModal>
  );
};

export { ImportMailmapModal };
