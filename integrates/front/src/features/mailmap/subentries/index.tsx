/* eslint-disable max-lines */
import { useMutation, useQuery } from "@apollo/client";
import { useAbility } from "@casl/react";
import {
  Button,
  Container,
  Divider,
  Icon,
  ListItem,
  ListItemsWrapper,
  useModal,
} from "@fluidattacks/design";
import type { ColumnDef } from "@tanstack/react-table";
import type { GraphQLError } from "graphql";
import { useCallback, useMemo, useRef, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { ConvertMailmapSubentriesToNewEntryModal } from "../convert-subentries-to-new-entry-modal";
import { CreateMailmapSubentryModal } from "../create-subentry-modal";
import { DeleteMailmapSubentryModal } from "../delete-subentry-modal";
import { SetMailmapSubentryAsMailmapEntryModal } from "../set-subentry-as-entry-modal";
import type {
  IConvertMailmapSubentryToNewEntryFormValues,
  ICreateMailmapSubentryFormValues,
  IDeleteMailmapSubentryFormValues,
  IMailmapSubentriesProps,
  IMailmapSubentry,
  ISetMailmapSubentryAsMailmapEntryFormValues,
  IUpdateMailmapSubentryFormValues,
} from "../types";
import { UpdateMailmapSubentryModal } from "../update-subentry-modal";
import { Authorize } from "components/@core/authorize";
import { ChildrenContainer } from "components/dropdown/styles";
import { Table } from "components/table";
import { authzPermissionsContext } from "context/authz/config";
import { useTable } from "hooks";
import { useClickOutside } from "hooks/use-click-outside";
import {
  CONVERT_MAILMAP_SUBENTRIES_TO_NEW_ENTRY_MUTATION,
  CREATE_MAILMAP_SUBENTRY_MUTATION,
  DELETE_MAILMAP_SUBENTRY_MUTATION,
  GET_MAILMAP_ENTRY_SUBENTRIES_QUERY,
  SET_MAILMAP_SUBENTRY_AS_MAILMAP_ENTRY_MUTATION,
  UPDATE_MAILMAP_SUBENTRY_MUTATION,
} from "pages/organization/mailmap/queries";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

const MailmapSubentries: React.FC<IMailmapSubentriesProps> = ({
  entriesEmails,
  entryEmail,
  organizationId,
  refetchEntries,
}: IMailmapSubentriesProps): JSX.Element => {
  const { t } = useTranslation();

  const permissions = useAbility(authzPermissionsContext);
  // This particular mutation below was chosen arbitrarily
  const mutation = "integrates_api_mutations_create_mailmap_subentry_mutate";
  const canMutate = permissions.can(mutation);

  const tableRef = useTable(`tblMailmapSubentries-${entryEmail}`);

  const createSubentryModal = useModal("create-subentry-modal");
  const onCloseCreateSubentryModal = createSubentryModal.close;
  const onOpenCreateSubentryModal = createSubentryModal.open;

  const deleteSubentryModal = useModal("delete-subentry-modal");
  const onCloseDeleteSubentryModal = deleteSubentryModal.close;
  const onOpenDeleteSubentryModal = deleteSubentryModal.open;

  const updateSubentryModal = useModal("update-subentry-modal");
  const onCloseUpdateSubentryModal = updateSubentryModal.close;
  const onOpenUpdateSubentryModal = updateSubentryModal.open;

  const setSubentryAsEntryModal = useModal("set-subentry-as-entry-modal");
  const onCloseSetSubentryAsEntryModal = setSubentryAsEntryModal.close;
  const onOpenSetSubentryAsEntryModal = setSubentryAsEntryModal.open;

  const convertSubentriesToNewEntryModal = useModal(
    "convert-subentries-to-new-entry-modal",
  );
  const onCloseConvertSubentriesToNewEntryModal =
    convertSubentriesToNewEntryModal.close;
  const onOpenConvertSubentriesToNewEntryModal =
    convertSubentriesToNewEntryModal.open;

  const [selectedSubentries, setSelectedSubentries] = useState<
    IMailmapSubentry[]
  >([]);

  const [isSubentryConfigMenuVisible, setIsSubentryConfigMenuVisible] =
    useState<boolean>(false);

  const { data } = useQuery(GET_MAILMAP_ENTRY_SUBENTRIES_QUERY, {
    fetchPolicy: "cache-first",
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error: GraphQLError): void => {
        if (error.message.startsWith("Exception - Mailmap entry not found:")) {
          msgError(
            t("mailmap.errorMailmapEntryNotFound"),
            t("mailmap.errorTitle"),
          );
          Logger.error("Exception - Mailmap entry not found:", error);
        } else {
          msgError(
            t("mailmap.errorGetMailmapSubentries"),
            t("mailmap.errorTitle"),
          );
          Logger.error(
            `Error loading mailmap subentries for ${entryEmail}:`,
            error,
          );
        }
      });
    },
    variables: {
      entryEmail,
      organizationId,
    },
  });

  const subentries: IMailmapSubentry[] =
    data?.mailmapEntrySubentries?.map(
      (subentry: IMailmapSubentry): IMailmapSubentry => ({
        mailmapSubentryEmail: subentry.mailmapSubentryEmail,
        mailmapSubentryName: subentry.mailmapSubentryName,
      }),
    ) ?? [];

  const [createMailmapSubentryMutation] = useMutation(
    CREATE_MAILMAP_SUBENTRY_MUTATION,
    {
      onCompleted: (): void => {
        msgSuccess(
          t("mailmap.successCreateSubentry"),
          t("mailmap.successTitle"),
        );
        refetchEntries().catch((): void => {
          Logger.error("An error occurred creating subentry");
        });
      },
      onError: ({ graphQLErrors }): void => {
        graphQLErrors.forEach((error: GraphQLError): void => {
          if (
            error.message.startsWith("Exception - Mailmap entry not found:")
          ) {
            msgError(
              t("mailmap.errorMailmapEntryNotFound"),
              t("mailmap.errorTitle"),
            );
            Logger.error("Exception - Mailmap entry not found:", error);
          } else if (
            error.message.startsWith(
              "Exception - Mailmap subentry already exists:",
            )
          ) {
            msgError(
              t("mailmap.errorMailmapSubentryAlreadyExists"),
              t("mailmap.errorTitle"),
            );
            Logger.error("Exception - Mailmap subentry already exists:", error);
          } else if (
            error.message.startsWith(
              "Exception - Mailmap subentry already exists in organization:",
            )
          ) {
            msgError(
              t("mailmap.errorMailmapSubentryAlreadyExistsInOrganization"),
              t("mailmap.errorTitle"),
            );
            Logger.error(
              "Exception - Mailmap subentry already exists in organization:",
              error,
            );
          } else {
            msgError(
              t("mailmap.errorCreateMailmapSubentry"),
              t("mailmap.errorTitle"),
            );
            Logger.error("Error creating mailmap subentry", error);
          }
        });
      },
      refetchQueries: [GET_MAILMAP_ENTRY_SUBENTRIES_QUERY],
    },
  );

  const [deleteMailmapSubentryMutation] = useMutation(
    DELETE_MAILMAP_SUBENTRY_MUTATION,
    {
      onCompleted: (): void => {
        msgSuccess(
          t("mailmap.successDeleteSubentry"),
          t("mailmap.successTitle"),
        );
        refetchEntries().catch((): void => {
          Logger.error("An error occurred deleting subentry");
        });
      },
      onError: ({ graphQLErrors }): void => {
        graphQLErrors.forEach((error: GraphQLError): void => {
          if (
            error.message.startsWith("Exception - Mailmap entry not found:")
          ) {
            msgError(
              t("mailmap.errorMailmapEntryNotFound"),
              t("mailmap.errorTitle"),
            );
            Logger.error("Exception - Mailmap entry not found:", error);
          } else if (
            error.message.startsWith("Exception - Mailmap subentry not found:")
          ) {
            msgError(
              t("mailmap.errorMailmapSubentryNotFound"),
              t("mailmap.errorTitle"),
            );
            Logger.error("Exception - Mailmap subentry not found:", error);
          } else {
            msgError(
              t("mailmap.errorDeleteMailmapSubentry"),
              t("mailmap.errorTitle"),
            );
            Logger.error("Error deleting mailmap subentry", error);
          }
        });
      },
      refetchQueries: [GET_MAILMAP_ENTRY_SUBENTRIES_QUERY],
    },
  );

  const [updateMailmapSubentryMutation] = useMutation(
    UPDATE_MAILMAP_SUBENTRY_MUTATION,
    {
      onCompleted: (): void => {
        msgSuccess(
          t("mailmap.successUpdateSubentry"),
          t("mailmap.successTitle"),
        );
        refetchEntries().catch((): void => {
          Logger.error("An error occurred updating subentry");
        });
      },
      onError: ({ graphQLErrors }): void => {
        graphQLErrors.forEach((error: GraphQLError): void => {
          if (
            error.message.startsWith("Exception - Mailmap entry not found:")
          ) {
            msgError(
              t("mailmap.errorMailmapEntryNotFound"),
              t("mailmap.errorTitle"),
            );
            Logger.error("Exception - Mailmap entry not found:", error);
          } else if (
            error.message.startsWith("Exception - Mailmap subentry not found:")
          ) {
            msgError(
              t("mailmap.errorMailmapSubentryNotFound"),
              t("mailmap.errorTitle"),
            );
            Logger.error("Exception - Mailmap subentry not found:", error);
          } else {
            msgError(
              t("mailmap.errorUpdateMailmapSubentry"),
              t("mailmap.errorTitle"),
            );
            Logger.error("Error updating mailmap subentry", error);
          }
        });
      },
      refetchQueries: [GET_MAILMAP_ENTRY_SUBENTRIES_QUERY],
    },
  );

  const [setMailmapSubentryAsMailmapEntryMutation] = useMutation(
    SET_MAILMAP_SUBENTRY_AS_MAILMAP_ENTRY_MUTATION,
    {
      onCompleted: (): void => {
        msgSuccess(
          t("mailmap.successSetSubentryAsEntry"),
          t("mailmap.successTitle"),
        );
        refetchEntries().catch((): void => {
          Logger.error("An error occurred setting subentry");
        });
      },
      onError: ({ graphQLErrors }): void => {
        graphQLErrors.forEach((error: GraphQLError): void => {
          if (
            error.message.startsWith("Exception - Mailmap entry not found:")
          ) {
            msgError(
              t("mailmap.errorMailmapEntryNotFound"),
              t("mailmap.errorTitle"),
            );
            Logger.error("Exception - Mailmap entry not found:", error);
          } else if (
            error.message.startsWith("Exception - Mailmap subentry not found:")
          ) {
            msgError(
              t("mailmap.errorMailmapSubentryNotFound"),
              t("mailmap.errorTitle"),
            );
            Logger.error("Exception - Mailmap subentry not found:", error);
          } else {
            msgError(
              t("mailmap.errorSetMailmapSubentryAsMailmapEntry"),
              t("mailmap.errorTitle"),
            );
            Logger.error(
              "Error setting mailmap subentry as mailmap entry",
              error,
            );
          }
        });
      },
      refetchQueries: [GET_MAILMAP_ENTRY_SUBENTRIES_QUERY],
    },
  );

  const [convertMailmapSubentriesToNewEntryMutation] = useMutation(
    CONVERT_MAILMAP_SUBENTRIES_TO_NEW_ENTRY_MUTATION,
    {
      onCompleted: (): void => {
        msgSuccess(
          t("mailmap.successConvertSubentriesToNewEntry"),
          t("mailmap.successTitle"),
        );
        refetchEntries().catch((): void => {
          Logger.error("An error occurred converting subentry");
        });
      },
      onError: ({ graphQLErrors }): void => {
        graphQLErrors.forEach((error: GraphQLError): void => {
          Logger.error(
            "Error converting mailmap subentries to new entry",
            error,
          );
        });
      },
      refetchQueries: [GET_MAILMAP_ENTRY_SUBENTRIES_QUERY],
    },
  );

  const createMailmapSubentry = useCallback(
    async (values: ICreateMailmapSubentryFormValues): Promise<void> => {
      const { subentryEmail, subentryName } = values;
      await createMailmapSubentryMutation({
        variables: {
          entryEmail,
          organizationId,
          subentry: {
            mailmapSubentryEmail: subentryEmail,
            mailmapSubentryName: subentryName,
          },
        },
      });
      onCloseCreateSubentryModal();
    },
    [
      createMailmapSubentryMutation,
      onCloseCreateSubentryModal,
      entryEmail,
      organizationId,
    ],
  );

  const deleteMailmapSubentry = useCallback(
    async (values: IDeleteMailmapSubentryFormValues): Promise<void> => {
      const { subentryEmail, subentryName } = values;
      await deleteMailmapSubentryMutation({
        variables: {
          entryEmail,
          organizationId,
          subentryEmail,
          subentryName,
        },
      });
      onCloseDeleteSubentryModal();
      setSelectedSubentries([]);
    },
    [
      deleteMailmapSubentryMutation,
      entryEmail,
      organizationId,
      onCloseDeleteSubentryModal,
    ],
  );

  const updateMailmapSubentry = useCallback(
    async (values: IUpdateMailmapSubentryFormValues): Promise<void> => {
      const {
        oldSubentryEmail,
        oldSubentryName,
        newSubentryEmail,
        newSubentryName,
      } = values;
      await updateMailmapSubentryMutation({
        variables: {
          entryEmail,
          organizationId,
          subentry: {
            mailmapSubentryEmail: newSubentryEmail,
            mailmapSubentryName: newSubentryName,
          },
          subentryEmail: oldSubentryEmail,
          subentryName: oldSubentryName,
        },
      });
      onCloseUpdateSubentryModal();
      setSelectedSubentries([]);
    },
    [
      entryEmail,
      organizationId,
      updateMailmapSubentryMutation,
      onCloseUpdateSubentryModal,
    ],
  );

  const setMailmapSubentryAsMailmapEntry = useCallback(
    async (
      values: ISetMailmapSubentryAsMailmapEntryFormValues,
    ): Promise<void> => {
      const { subentryEmail, subentryName } = values;
      await setMailmapSubentryAsMailmapEntryMutation({
        variables: {
          entryEmail,
          organizationId,
          subentryEmail,
          subentryName,
        },
      });
      onCloseSetSubentryAsEntryModal();
      setSelectedSubentries([]);
    },
    [
      setMailmapSubentryAsMailmapEntryMutation,
      entryEmail,
      organizationId,
      onCloseSetSubentryAsEntryModal,
    ],
  );

  const convertMailmapSubentriesToNewEntry = useCallback(
    async (
      values: IConvertMailmapSubentryToNewEntryFormValues,
    ): Promise<void> => {
      const { mainSubentry, otherSubentries } = values;
      await convertMailmapSubentriesToNewEntryMutation({
        variables: {
          entryEmail,
          mainSubentry,
          organizationId,
          otherSubentries,
        },
      });
      onCloseConvertSubentriesToNewEntryModal();
      setSelectedSubentries([]);
    },
    [
      convertMailmapSubentriesToNewEntryMutation,
      entryEmail,
      organizationId,
      onCloseConvertSubentriesToNewEntryModal,
    ],
  );

  const columns = useMemo(
    (): ColumnDef<IMailmapSubentry>[] => [
      {
        accessorKey: "mailmapSubentryName",
        header: t("mailmap.subentryName"),
      },
      {
        accessorKey: "mailmapSubentryEmail",
        header: t("mailmap.subentryEmail"),
      },
    ],
    [t],
  );

  const getSelectedSubentryEmail = (): string =>
    selectedSubentries.length === 1
      ? selectedSubentries[0].mailmapSubentryEmail
      : "";

  const getSelectedSubentryName = (): string =>
    selectedSubentries.length === 1
      ? selectedSubentries[0].mailmapSubentryName
      : "";

  // Components Declaration

  const modals = (
    <React.Fragment>
      <CreateMailmapSubentryModal
        entryEmail={entryEmail}
        modalRef={createSubentryModal}
        onClose={onCloseCreateSubentryModal}
        onSubmit={createMailmapSubentry}
      />
      <DeleteMailmapSubentryModal
        entryEmail={entryEmail}
        modalRef={deleteSubentryModal}
        onClose={onCloseDeleteSubentryModal}
        onSubmit={deleteMailmapSubentry}
        subentryEmail={getSelectedSubentryEmail()}
        subentryName={getSelectedSubentryName()}
      />
      <UpdateMailmapSubentryModal
        entryEmail={entryEmail}
        modalRef={updateSubentryModal}
        onClose={onCloseUpdateSubentryModal}
        onSubmit={updateMailmapSubentry}
        subentryEmail={getSelectedSubentryEmail()}
        subentryName={getSelectedSubentryName()}
      />
      <SetMailmapSubentryAsMailmapEntryModal
        entryEmail={entryEmail}
        modalRef={setSubentryAsEntryModal}
        onClose={onCloseSetSubentryAsEntryModal}
        onSubmit={setMailmapSubentryAsMailmapEntry}
        subentryEmail={getSelectedSubentryEmail()}
        subentryName={getSelectedSubentryName()}
      />
      <ConvertMailmapSubentriesToNewEntryModal
        entryEmail={entryEmail}
        modalRef={convertSubentriesToNewEntryModal}
        onClose={onCloseConvertSubentriesToNewEntryModal}
        onSubmit={convertMailmapSubentriesToNewEntry}
        organizationId={organizationId}
        subentryEmail={getSelectedSubentryEmail()}
        subentryName={getSelectedSubentryName()}
      />
    </React.Fragment>
  );

  const toggleSubentryConfigMenu = useCallback((): void => {
    setIsSubentryConfigMenuVisible((previous): boolean => !previous);
  }, [setIsSubentryConfigMenuVisible]);

  const subentryConfigMenu = ((): JSX.Element | undefined => {
    if (!isSubentryConfigMenuVisible) {
      return undefined;
    }

    const enableConvertToNewEntry = selectedSubentries.every(
      (subentry): boolean =>
        !entriesEmails.includes(subentry.mailmapSubentryEmail),
    );

    return (
      <ListItemsWrapper>
        <ListItem
          icon={"pencil"}
          iconType={"fa-light"}
          onClick={onOpenUpdateSubentryModal}
          value={t("mailmap.updateSubentry")}
        >
          {t("mailmap.updateSubentry")}
        </ListItem>
        <Divider />
        <ListItem
          icon={"money-check-pen"}
          iconType={"fa-light"}
          onClick={onOpenSetSubentryAsEntryModal}
          value={t("mailmap.setSubentryAsEntry")}
        >
          {t("mailmap.setSubentryAsEntry")}
        </ListItem>
        <Divider />
        <ListItem
          icon={"trash"}
          iconType={"fa-light"}
          onClick={onOpenDeleteSubentryModal}
          value={t("mailmap.deleteSubentry")}
        >
          {t("mailmap.deleteSubentry")}
        </ListItem>
        <Divider />
        <ListItem
          disabled={!enableConvertToNewEntry}
          icon={"user-plus"}
          iconType={"fa-light"}
          onClick={onOpenConvertSubentriesToNewEntryModal}
          value={t("mailmap.convertSubentryToNewEntry")}
        >
          {t("mailmap.convertSubentryToNewEntry")}
        </ListItem>
      </ListItemsWrapper>
    );
  })();

  const subentryConfigButtonRef = useRef<HTMLButtonElement>(null);

  const subentryConfigButton = (
    <Button
      disabled={selectedSubentries.length !== 1}
      onClick={toggleSubentryConfigMenu}
      ref={subentryConfigButtonRef}
      variant={"ghost"}
    >
      <Icon
        clickable={false}
        icon={"gear"}
        iconSize={"xs"}
        iconType={"fa-light"}
      />
      {t("mailmap.subentryConfig")}
      <Container zIndex={2}>
        <ChildrenContainer>{subentryConfigMenu}</ChildrenContainer>
      </Container>
    </Button>
  );

  useClickOutside(subentryConfigButtonRef.current, (): void => {
    setIsSubentryConfigMenuVisible(false);
  });

  const buttons = (
    <React.Fragment>
      {subentryConfigButton}
      <Button icon={"plus"} onClick={onOpenCreateSubentryModal}>
        {t("mailmap.createSubentry")}
      </Button>
    </React.Fragment>
  );

  const table = (
    <Table
      columns={columns}
      data={subentries}
      extraButtons={<Authorize can={mutation}>{buttons}</Authorize>}
      options={{ enableSearchBar: false }}
      rowSelectionSetter={canMutate ? setSelectedSubentries : undefined}
      rowSelectionState={canMutate ? selectedSubentries : undefined}
      tableRef={tableRef}
    />
  );

  return (
    <React.Fragment>
      {modals}
      {table}
    </React.Fragment>
  );
};

const renderMailmapSubentries = ({
  entryEmail,
  organizationId,
  entriesEmails,
  refetchEntries,
}: IMailmapSubentriesProps): JSX.Element => (
  <MailmapSubentries
    entriesEmails={entriesEmails}
    entryEmail={entryEmail}
    organizationId={organizationId}
    refetchEntries={refetchEntries}
  />
);

export { renderMailmapSubentries };
