import { Alert, Form, InnerForm, Input, Modal } from "@fluidattacks/design";
import { Fragment } from "react";
import { useTranslation } from "react-i18next";

import type { IDeleteMailmapEntryModalProps } from "../types";

const DeleteMailmapEntryModal = ({
  modalRef,
  onClose,
  onSubmit,
  entryEmail,
}: IDeleteMailmapEntryModalProps): JSX.Element => {
  const { t } = useTranslation();

  return (
    <Modal
      modalRef={modalRef}
      size={"sm"}
      title={t("mailmap.deleteMailmapEntry")}
    >
      <Form
        cancelButton={{ onClick: onClose }}
        confirmButton={{ label: t("mailmap.deleteEntry") }}
        defaultDisabled={false}
        defaultValues={{
          entryEmail,
        }}
        onSubmit={onSubmit}
      >
        <InnerForm>
          {({ register }): JSX.Element => (
            <Fragment>
              <Input
                disabled={true}
                label={t("mailmap.entryEmail")}
                name={"entryEmail"}
                placeholder={t("mailmap.entryEmail")}
                register={register}
              />
              <Alert>{t("mailmap.alertDeleteMailmapEntry")}</Alert>
            </Fragment>
          )}
        </InnerForm>
      </Form>
    </Modal>
  );
};

export { DeleteMailmapEntryModal };
