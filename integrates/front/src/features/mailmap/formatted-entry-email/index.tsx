import { Icon, Tooltip } from "@fluidattacks/design";
import * as React from "react";
import { useTranslation } from "react-i18next";

import type { IFormattedMailmapEntryEmailProps } from "pages/organization/mailmap/types";

const FormattedMailmapEntryEmail: React.FC<
  IFormattedMailmapEntryEmailProps
> = ({
  entryEmail,
  domainsCheck,
}: IFormattedMailmapEntryEmailProps): JSX.Element => {
  const { t } = useTranslation();

  return (
    <React.Fragment>
      <Tooltip
        id={`${entryEmail}-icon-tooltip`}
        place={"top"}
        tip={
          domainsCheck
            ? t("mailmap.tooltipDomainsCheckTrue")
            : t("mailmap.tooltipDomainsCheckFalse")
        }
      >
        <Icon
          icon={domainsCheck ? "check-circle" : "warning"}
          iconColor={domainsCheck ? "green" : "orange"}
          iconSize={"xxs"}
          iconType={"fa-solid"}
          mr={0.5}
        />
      </Tooltip>
      <span>{entryEmail}</span>
    </React.Fragment>
  );
};

const formatMailmapEntryEmail = ({
  entryEmail,
  domainsCheck,
}: IFormattedMailmapEntryEmailProps): JSX.Element => (
  <FormattedMailmapEntryEmail
    domainsCheck={domainsCheck}
    entryEmail={entryEmail}
  />
);

export { FormattedMailmapEntryEmail, formatMailmapEntryEmail };
