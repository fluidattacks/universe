import { Form, InnerForm, Input, Modal } from "@fluidattacks/design";
import * as React from "react";
import { Fragment } from "react";
import { useTranslation } from "react-i18next";

import type { IDeleteMailmapSubentryModalProps } from "../types";

const DeleteMailmapSubentryModal: React.FC<
  IDeleteMailmapSubentryModalProps
> = ({
  onClose,
  onSubmit,
  entryEmail,
  subentryEmail,
  subentryName,
  modalRef,
}: IDeleteMailmapSubentryModalProps): JSX.Element => {
  const { t } = useTranslation();

  return (
    <Modal
      modalRef={modalRef}
      size={"sm"}
      title={t("mailmap.deleteMailmapSubentry")}
    >
      <Form
        cancelButton={{ onClick: onClose }}
        confirmButton={{ label: t("mailmap.deleteSubentry") }}
        defaultDisabled={false}
        defaultValues={{
          entryEmail,
          subentryEmail,
          subentryName,
        }}
        onSubmit={onSubmit}
      >
        <InnerForm>
          {({ register }): JSX.Element => (
            <Fragment>
              <Input
                disabled={true}
                label={t("mailmap.entryEmail")}
                name={"entryEmail"}
                placeholder={t("mailmap.entryEmail")}
                register={register}
              />

              <Input
                disabled={true}
                label={t("mailmap.subentryName")}
                name={"subentryName"}
                placeholder={t("mailmap.subentryName")}
                register={register}
              />
              <Input
                disabled={true}
                label={t("mailmap.subentryEmail")}
                name={"subentryEmail"}
                placeholder={t("mailmap.subentryEmail")}
                register={register}
              />
            </Fragment>
          )}
        </InnerForm>
      </Form>
    </Modal>
  );
};

export { DeleteMailmapSubentryModal };
