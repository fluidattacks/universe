import { Alert, Form, InnerForm, Input, Modal } from "@fluidattacks/design";
import { Fragment } from "react";
import { useTranslation } from "react-i18next";

import type { ISetMailmapSubentryAsMailmapEntryModalProps } from "../types";

const SetMailmapSubentryAsMailmapEntryModal: React.FC<
  ISetMailmapSubentryAsMailmapEntryModalProps
> = ({
  onClose,
  onSubmit,
  entryEmail,
  subentryEmail,
  subentryName,
  modalRef,
}: ISetMailmapSubentryAsMailmapEntryModalProps): JSX.Element => {
  const { t } = useTranslation();

  return (
    <Modal
      modalRef={modalRef}
      size={"sm"}
      title={t("mailmap.setMailmapSubentryAsMailmapEntry")}
    >
      <Form
        cancelButton={{ onClick: onClose }}
        confirmButton={{ label: t("mailmap.setSubentryAsEntry") }}
        defaultDisabled={false}
        defaultValues={{
          entryEmail,
          subentryEmail,
          subentryName,
        }}
        onSubmit={onSubmit}
      >
        <InnerForm>
          {({ register }): JSX.Element => (
            <Fragment>
              <Input
                disabled={true}
                label={t("mailmap.entryEmail")}
                name={"entryEmail"}
                placeholder={t("mailmap.entryEmail")}
                register={register}
              />
              <Input
                disabled={true}
                label={t("mailmap.subentryName")}
                name={"subentryName"}
                placeholder={t("mailmap.subentryName")}
                register={register}
              />
              <Input
                disabled={true}
                label={t("mailmap.subentryEmail")}
                name={"subentryEmail"}
                placeholder={t("mailmap.subentryEmail")}
                register={register}
              />
              <Alert>
                {t("mailmap.alertSetMailmapSubentryAsMailmapEntry")}
              </Alert>
            </Fragment>
          )}
        </InnerForm>
      </Form>
    </Modal>
  );
};

export { SetMailmapSubentryAsMailmapEntryModal };
