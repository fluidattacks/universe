import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";

import type { IPriorityFilters } from "../types";
import { Filters } from "components/filter";
import type { IFilter, IPermanentData } from "components/filter/types";
import type { IVulnRowAttr } from "features/vulnerabilities/types";
import { VulnerabilityTreatment } from "gql/graphql";
import { useStoredState } from "hooks/use-stored-state";

const PriorityFilters = ({
  setTreatment,
}: Readonly<IPriorityFilters>): JSX.Element => {
  const { t } = useTranslation();
  const [filters, setFilters] = useState<IFilter<IVulnRowAttr>[]>([
    {
      id: "treatment",
      key: "treatmentStatus",
      label: t("searchFindings.tabVuln.vulnTable.treatment"),
      selectOptions: [
        {
          header: t("searchFindings.tabDescription.treatment.new"),
          value: VulnerabilityTreatment.Untreated,
        },
        {
          header: t("searchFindings.tabDescription.treatment.inProgress"),
          value: VulnerabilityTreatment.InProgress,
        },
        {
          header: t("searchFindings.tabDescription.treatment.accepted"),
          value: VulnerabilityTreatment.Accepted,
        },
        {
          header: t(
            "searchFindings.tabDescription.treatment.acceptedUndefined",
          ),
          value: VulnerabilityTreatment.AcceptedUndefined,
        },
      ],
      type: "select",
    },
  ]);

  const [filtersPermaset, setFiltersPermaset] = useStoredState<
    IPermanentData[]
  >(
    "tblPriorityFilters",
    [{ id: "treatment", value: VulnerabilityTreatment.Untreated }],
    localStorage,
  );

  useEffect((): void => {
    setTreatment(
      filtersPermaset.find((filter): boolean => filter.id === "treatment")
        ?.value ?? "",
    );
  }, [filters, filtersPermaset, setTreatment]);

  return (
    <Filters
      filters={filters}
      permaset={[filtersPermaset, setFiltersPermaset]}
      setFilters={setFilters}
    />
  );
};

export { PriorityFilters };
