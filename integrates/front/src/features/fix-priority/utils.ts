const formatTreatment = (treatment: string): string => {
  switch (treatment) {
    case "ACCEPTED":
      return "Temporarily accepted";
    case "ACCEPTED_UNDEFINED":
      return "Permanently accepted";
    case "IN_PROGRESS":
      return "In progress";
    default:
      return "Untreated";
  }
};

export { formatTreatment };
