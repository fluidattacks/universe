import type { ColumnDef } from "@tanstack/react-table";

import type { IVulnRowAttr } from "features/vulnerabilities/types";

interface IPriorityFilters {
  setTreatment: React.Dispatch<React.SetStateAction<string>>;
}

interface IPriorityRanking {
  columns: ColumnDef<IVulnerabilityAttr>[];
  groupName?: string;
}

interface IVulnerabilityAttr extends IVulnRowAttr {
  __typename?: "Vulnerability";
  finding: {
    __typename?: "Finding";
    id: string;
    organizationName: string;
    title: string;
  };
  totalOpenPriority?: number;
}

interface IPriorityVulnerabilities {
  group?: {
    __typename: "Group";
    name: string;
    totalOpenPriority: number;
  };
  me: {
    __typename: "Me";
    vulnerabilitiesPriorityRanking: {
      __typename: "VulnerabilitiesConnection";
      edges: { node: IVulnerabilityAttr }[];
      pageInfo: {
        __typename: "PageInfo";
        endCursor: string;
        hasNextPage: boolean;
      };
      total: number | undefined;
    };
    userEmail: string;
  };
}

export type {
  IVulnerabilityAttr,
  IPriorityVulnerabilities,
  IPriorityFilters,
  IPriorityRanking,
};
