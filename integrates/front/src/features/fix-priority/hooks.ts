import type { Row } from "@tanstack/react-table";
import mixpanel from "mixpanel-browser";
import type { Dispatch, SetStateAction } from "react";
import { useCallback, useContext, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";

import type { IVulnerabilityAttr } from "./types";

import { allGroupContext } from "context/authz/all-group-permissions-provider";
import type { IVulnRowAttr } from "features/vulnerabilities/types";

const useAdditionalInfo = (): [
  IVulnRowAttr | undefined,
  Dispatch<SetStateAction<IVulnRowAttr | undefined>>,
  (row: Row<IVulnerabilityAttr>) => (event: React.FormEvent) => void,
  () => void,
] => {
  const { changePermissions } = useContext(allGroupContext);
  const { pathname } = useLocation();
  const [currentRow, setCurrentRow] = useState<IVulnRowAttr>();
  const navigate = useNavigate();
  const url = pathname.split("/").slice(0, -1).join("/");

  const openAdditionalInfoModal = useCallback(
    (row: Row<IVulnerabilityAttr>): ((event: React.FormEvent) => void) => {
      return (event: React.FormEvent): void => {
        if (changePermissions) {
          changePermissions(row.original.groupName);
        }
        const vulnId = row.original.id;
        navigate(`${pathname}/${vulnId}`, { replace: true });
        setCurrentRow(row.original);
        mixpanel.track("ViewPriorityVulnerabilityModal", {
          groupName: row.original.groupName,
        });
        event.stopPropagation();
      };
    },
    [changePermissions, navigate, pathname],
  );
  const closeAdditionalInfoModal = useCallback((): void => {
    navigate(url, { replace: true });
    setCurrentRow(undefined);
  }, [navigate, url]);

  return [
    currentRow,
    setCurrentRow,
    openAdditionalInfoModal,
    closeAdditionalInfoModal,
  ];
};

export { useAdditionalInfo };
