import { isNil } from "lodash";
import isNaN from "lodash/isNaN";
import type { AnyObject, InferType, Maybe, Schema, TestContext } from "yup";
import { lazy, number, object } from "yup";

import type { IFormikPoliciesValues } from "./types";

import { translate } from "utils/translations/translate";

const minInactivityPeriod = 21;
const maxInactivityPeriod = 999;
const minSeverity = 0.0;
const maxSeverity = 10.0;
const minValue = 0;

const numberBetweenInactivity = (
  isInactivityPeriodHide: boolean,
): ((
  value: number | undefined,
  options: TestContext<Maybe<AnyObject>>,
) => boolean) => {
  return (value, _options): boolean => {
    if (isInactivityPeriodHide) {
      return true;
    }
    if (value === undefined) {
      return false;
    }

    return value >= minInactivityPeriod && value <= maxInactivityPeriod;
  };
};

const numberBetweenSeverity: (
  value: number | undefined,
  options: TestContext<Maybe<AnyObject>>,
) => boolean = (value, _options): boolean => {
  if (isNil(value)) {
    return false;
  }

  return Number(value) >= minSeverity && Number(value) <= maxSeverity;
};

const numberBetweenRange = (
  values: IFormikPoliciesValues,
): ((
  value: number | undefined,
  options: TestContext<Maybe<AnyObject>>,
) => boolean) => {
  return (_value, _options): boolean => {
    const { daysUntilItBreaks, vulnerabilityGracePeriod } = values;
    if (!vulnerabilityGracePeriod || daysUntilItBreaks === null) {
      return true;
    }
    if (
      isNaN(Number(vulnerabilityGracePeriod)) ||
      isNaN(Number(daysUntilItBreaks))
    ) {
      return true;
    }

    return (
      parseInt(String(vulnerabilityGracePeriod), 10) <
      parseInt(String(daysUntilItBreaks), 10)
    );
  };
};

const validationSchema = (isInactivityPeriodHide: boolean): InferType<Schema> =>
  lazy(
    (values: IFormikPoliciesValues): Schema =>
      object().shape({
        daysUntilItBreaks: number()
          .nullable()
          .integer(translate.t("validations.integer"))
          .positive(translate.t("validations.positive"))
          .isValidFunction(
            numberBetweenRange(values),
            translate.t(
              "organization.tabs.policies.errors.daysUntilItBreaksRange",
            ),
          ),
        inactivityPeriod: number().isValidFunction(
          numberBetweenInactivity(isInactivityPeriodHide),
          translate.t("validations.between", {
            max: maxInactivityPeriod,
            min: minInactivityPeriod,
          }),
        ),
        maxAcceptanceDays: number()
          .nullable()
          .min(minValue, translate.t("validations.zeroOrPositive")),
        maxAcceptanceSeverity: number().isValidFunction(
          numberBetweenSeverity,
          translate.t("validations.between", {
            max: maxSeverity,
            min: minSeverity,
          }),
        ),
        maxNumberAcceptances: number()
          .nullable()
          .min(minValue, translate.t("validations.zeroOrPositive")),
        minAcceptanceSeverity: number().isValidFunction(
          numberBetweenSeverity,
          translate.t("validations.between", {
            max: maxSeverity,
            min: minSeverity,
          }),
        ),
        minBreakingSeverity: number()
          .nullable()
          .isValidFunction(
            numberBetweenSeverity,
            translate.t("validations.between", {
              max: maxSeverity,
              min: minSeverity,
            }),
          ),
        vulnerabilityGracePeriod: number()
          .min(minValue, translate.t("validations.zeroOrPositive"))
          .isValidFunction(
            numberBetweenRange(values),
            translate.t(
              "organization.tabs.policies.errors.daysUntilItBreaksRange",
            ),
          ),
      }),
  );

export { validationSchema };
