import { PureAbility } from "@casl/ability";
import { screen, waitFor } from "@testing-library/react";

import { Policies } from ".";
import { authzPermissionsContext } from "context/authz/config";
import type { GetPoliciesAtOrganizationQuery } from "gql/graphql";
import { render } from "mocks";

describe("organizationPolicies", (): void => {
  const organization: GetPoliciesAtOrganizationQuery["organization"] = {
    __typename: "Organization",
    daysUntilItBreaks: null,
    groups: [
      {
        __typename: "Group",
        daysUntilItBreaks: null,
        maxAcceptanceDays: 3,
        maxAcceptanceSeverity: 4,
        maxNumberAcceptances: 2,
        minAcceptanceSeverity: 1,
        minBreakingSeverity: 3.5,
        name: "group1",
        vulnerabilityGracePeriod: 2,
      },
      {
        __typename: "Group",
        daysUntilItBreaks: 122,
        maxAcceptanceDays: 5,
        maxAcceptanceSeverity: 2.3,
        maxNumberAcceptances: 2,
        minAcceptanceSeverity: 0,
        minBreakingSeverity: 2,
        name: "group2",
        vulnerabilityGracePeriod: 3,
      },
    ],
    inactivityPeriod: 90,
    maxAcceptanceDays: 5,
    maxAcceptanceSeverity: 7.5,
    maxNumberAcceptances: 2,
    minAcceptanceSeverity: 3,
    minBreakingSeverity: 3,
    name: "okada",
    vulnerabilityGracePeriod: 1,
  };

  it("renders policies information", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Policies
        data={organization}
        handleResetToOrgPolicies={jest.fn()}
        handleSubmit={jest.fn()}
      />,
    );

    await waitFor((): void => {
      expect(screen.queryByText("Okada")).toBeInTheDocument();
    });

    expect(
      screen.getByText("policies.temporaryAcceptance"),
    ).toBeInTheDocument();

    expect(screen.getByText("policies.duration")).toBeInTheDocument();

    expect(screen.getByText("5 days, 2 times.")).toBeInTheDocument();

    expect(screen.getAllByText("policies.severity")).toHaveLength(2);

    expect(screen.getByText("3 min, 7.5 max.")).toBeInTheDocument();

    expect(screen.getByText("policies.devSecOps")).toBeInTheDocument();

    expect(screen.getByText("policies.gracePeriod")).toBeInTheDocument();

    expect(screen.getByText("1 days")).toBeInTheDocument();

    expect(screen.getByText("3")).toBeInTheDocument();

    expect(screen.getByText("policies.inactivityPolicy")).toBeInTheDocument();

    expect(screen.getByText("policies.inactivityRemoval")).toBeInTheDocument();

    expect(screen.getByText("90 days.")).toBeInTheDocument();

    expect(screen.getByText("Temporary Acceptance")).toBeInTheDocument();

    expect(screen.getByText("DevSecOps")).toBeInTheDocument();

    expect(screen.getByText("Group Name")).toBeInTheDocument();

    expect(screen.getByText("Duration")).toBeInTheDocument();

    expect(screen.getByText("Severity")).toBeInTheDocument();

    expect(screen.getByText("Grace Period")).toBeInTheDocument();

    expect(screen.getByText("Severity CVSS")).toBeInTheDocument();

    expect(screen.getByText("Group1")).toBeInTheDocument();

    expect(screen.getByText("Group2")).toBeInTheDocument();

    expect(screen.getAllByText("policies.durationDaysAndTimes")).toHaveLength(
      2,
    );

    expect(screen.getAllByText("minMax")).toHaveLength(2);

    expect(screen.getAllByText("policies.days")).toHaveLength(2);

    expect(screen.getAllByRole("row")).toHaveLength(4);

    expect(screen.getAllByRole("cell")).toHaveLength(10);
  });

  it("renders edit options", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      {
        action: "integrates_api_mutations_update_organization_policies_mutate",
      },
      {
        action: "integrates_api_mutations_update_groups_policies__update_group",
      },
    ]);

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Policies
          data={organization}
          handleResetToOrgPolicies={jest.fn()}
          handleSubmit={jest.fn()}
        />
      </authzPermissionsContext.Provider>,
    );

    await waitFor((): void => {
      expect(screen.queryByText("Okada")).toBeInTheDocument();
    });

    expect(screen.getByText("buttons.edit")).toBeInTheDocument();

    expect(screen.getByText("policies.reset.label")).toBeInTheDocument();

    /* Including checkboxes and edit action */
    expect(screen.getAllByRole("cell")).toHaveLength(14);
  });
});
