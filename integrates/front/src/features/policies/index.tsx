/* eslint-disable react/jsx-handler-names */
import { useAbility } from "@casl/react";
import {
  Button,
  Container,
  Divider,
  GridContainer,
  Heading,
  Icon,
  Link,
  Text,
  useConfirmDialog,
  useModal,
} from "@fluidattacks/design";
import type {
  ColumnDef,
  RowSelectionState,
  SortingState,
  Updater,
} from "@tanstack/react-table";
import { getCoreRowModel, useReactTable } from "@tanstack/react-table";
import { capitalize, isEmpty } from "lodash";
import { Fragment, useCallback, useMemo, useState } from "react";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

import type {
  IFormikPoliciesValues,
  IPolicies,
  ISelectedPolicies,
} from "./types";
import { UpdateModal } from "./update-modal";
import { addExtraColumns, returnDashIfNil } from "./utils";

import { Table } from "components/table/new";
import { Can } from "context/authz/can";
import { authzPermissionsContext } from "context/authz/config";
import type { GetPoliciesAtOrganizationQuery } from "gql/graphql";

const SPACING = 1.25;
const POLICIES_URL =
  "https://help.fluidattacks.com/portal/en/kb/articles/manage-general-policies";

const Policies = ({
  handleSubmit,
  handleResetToOrgPolicies,
  data,
}: Readonly<IPolicies>): JSX.Element => {
  const { t } = useTranslation();
  const theme = useTheme();
  const permissions = useAbility(authzPermissionsContext);
  const [rowSelection, setRowSelection] = useState<RowSelectionState>({});
  const [searchedValue, setSearchedValue] = useState("");
  const [sorting, setSorting] = useState<SortingState>([]);
  const canEdit = permissions.can(
    "integrates_api_mutations_update_groups_policies__update_group",
  );
  const [selectedPolicies, setSelectedPolicies] =
    useState<ISelectedPolicies>(data);
  const modalRef = useModal("organization-policies-update");
  const { confirm, ConfirmDialog } = useConfirmDialog();

  const handleOnClickEdit = useCallback(
    (
      row: GetPoliciesAtOrganizationQuery["organization"]["groups"][0],
    ): VoidFunction =>
      (): void => {
        setSelectedPolicies(row);
        modalRef.open();
      },
    [setSelectedPolicies, modalRef],
  );

  const handlePoliciesSubmit = useCallback(
    (values: IFormikPoliciesValues): void => {
      handleSubmit(values);
      setSelectedPolicies(data);
    },
    [handleSubmit, data],
  );

  const handleSearch = useCallback((search: string): void => {
    setSearchedValue(search);
  }, []);

  const handleCloseOrCancel = useCallback((): void => {
    setSelectedPolicies(data);
    modalRef.close();
  }, [data, modalRef]);

  const handleReset = useCallback(async (): Promise<void> => {
    const confirmResult = await confirm({
      id: "reset-policies-confirm",
      message: (
        <Fragment>
          <Text size={"sm"}>{t("policies.reset.description")}</Text>
          <Text mt={1} size={"sm"}>
            {t("policies.reset.selectedGroups", {
              groups: Object.keys(rowSelection).map(capitalize).join(", "),
            })}
          </Text>
        </Fragment>
      ),
      title: t("policies.reset.title"),
    });
    if (confirmResult)
      await handleResetToOrgPolicies(Object.keys(rowSelection));
  }, [handleResetToOrgPolicies, rowSelection, confirm, t]);

  const {
    maxAcceptanceDays,
    maxAcceptanceSeverity,
    maxNumberAcceptances,
    minAcceptanceSeverity,
    minBreakingSeverity,
    vulnerabilityGracePeriod,
    inactivityPeriod,
    name: organizationName,
  } = data;

  const tableHeaders = useMemo((): ColumnDef<
    GetPoliciesAtOrganizationQuery["organization"]["groups"][0]
  >[] => {
    const minSize = 190;
    const size = minSize;
    const maxSize = 1000;

    return [
      {
        accessorFn: (row): string => capitalize(row.name),
        header: "Group Name",
        id: "name",
        maxSize,
        minSize,
        size: minSize,
      },
      {
        columns: [
          {
            accessorFn: (row): string =>
              t("policies.durationDaysAndTimes", {
                days: returnDashIfNil(row.maxAcceptanceDays),
                times: returnDashIfNil(row.maxNumberAcceptances),
              }),
            header: "Duration",
            id: "maxAcceptanceDays",
            maxSize,
            minSize,
            size,
          },
          {
            accessorFn: (row): string =>
              t("minMax", {
                max: returnDashIfNil(row.maxAcceptanceSeverity),
                min: returnDashIfNil(row.minAcceptanceSeverity),
              }),
            header: "Severity",
            id: "maxAcceptanceSeverity",
            maxSize,
            minSize,
            size,
          },
        ],
        header: "Temporary Acceptance",
      },
      {
        columns: [
          {
            accessorFn: (row): string =>
              t("policies.days", {
                days: returnDashIfNil(row.vulnerabilityGracePeriod),
              }),
            header: "Grace Period",
            id: "vulnerabilityGracePeriod",
            maxSize,
            minSize,
            size,
          },
          {
            cell: (cell): string =>
              returnDashIfNil(cell.row.original.minBreakingSeverity),
            header: "Severity CVSS",
            id: "minBreakingSeverity",
            maxSize,
            minSize,
            size,
          },
        ],
        header: "DevSecOps",
      },
    ];
  }, [t]);

  const groupsData = data.groups.filter((group): boolean =>
    group.name.toLowerCase().includes(searchedValue.toLowerCase()),
  );

  const descendingOrder = sorting.length > 0 && sorting[0].desc;
  const columnId =
    sorting.length === 0
      ? "name"
      : (sorting[0].id as keyof (typeof groupsData)[0]);

  const sortedGroupsData =
    sorting.length === 0
      ? groupsData
      : groupsData.toSorted((left, right): number => {
          const leftValue = left[columnId];
          const rightValue = right[columnId];
          if (
            leftValue === rightValue ||
            leftValue === null ||
            rightValue === null
          )
            return 0;
          if (descendingOrder) return leftValue > rightValue ? -1 : 1;

          return leftValue > rightValue ? 1 : -1;
        });

  const handleSorting = useCallback(
    (updater: Updater<SortingState>): void => {
      const newSortingValue =
        updater instanceof Function ? updater(sorting) : updater;
      setSorting(newSortingValue);
    },
    [sorting],
  );

  const table = useReactTable<
    GetPoliciesAtOrganizationQuery["organization"]["groups"][0]
  >({
    autoResetAll: false,
    columnResizeMode: "onChange",
    columns: canEdit
      ? addExtraColumns(tableHeaders, handleOnClickEdit)
      : tableHeaders,
    data: sortedGroupsData,
    enableColumnFilters: false,
    enableColumnResizing: true,
    enableMultiRowSelection: true,
    enableRowSelection: true,
    enableSorting: true,
    getCoreRowModel: getCoreRowModel(),
    getRowId: (row): string => row.name,
    manualSorting: false,
    meta: { filters: undefined, onSearch: handleSearch },
    onRowSelectionChange: setRowSelection,
    onSortingChange: handleSorting,
    rowCount: data.groups.length,
    sortDescFirst: true,
    state: {
      columnVisibility: undefined,
      rowSelection,
      sorting,
    },
  });

  return (
    <Container
      display={"flex"}
      flexDirection={"column"}
      gap={1}
      px={SPACING}
      py={SPACING}
    >
      <Heading size={"sm"}>{"Organization"}</Heading>
      <Container
        bgColor={theme.palette.white}
        border={`1px solid${theme.palette.gray[200]}`}
        borderRadius={"4px"}
      >
        <Container
          display={"flex"}
          justify={"space-between"}
          px={SPACING}
          py={SPACING}
        >
          <Container>
            <Heading size={"sm"}>{capitalize(organizationName)}</Heading>
            <Container alignItems={"start"} display={"flex"} gap={0.25}>
              <Icon
                icon={"info-circle"}
                iconColor={theme.palette.gray[400]}
                iconSize={"xxss"}
                iconType={"fa-light"}
                mt={0.25}
              />
              <Text color={theme.palette.gray[400]} size={"sm"}>
                {t(
                  `policies.organizationInformativeText.${
                    canEdit ? "withEditPermission" : "withoutEditPermission"
                  }`,
                )}
              </Text>
            </Container>
          </Container>
          <Can
            do={"integrates_api_mutations_update_organization_policies_mutate"}
          >
            <Button height={"44px"} icon={"edit"} onClick={modalRef.open}>
              {t("buttons.edit")}
            </Button>
          </Can>
        </Container>
        <Divider />
        <GridContainer lg={3} md={3} px={SPACING} py={SPACING} sm={3} xl={3}>
          <Container
            borderRight={`1px solid${theme.palette.gray[200]}`}
            display={"inline-flex"}
            flexDirection={"column"}
            gap={0.5}
          >
            <Container
              alignItems={"center"}
              display={"inline-flex"}
              gap={0.5}
              width={"fit-content"}
            >
              <Heading my={0} size={"xs"}>
                {t("policies.temporaryAcceptance")}
              </Heading>
              <Link
                href={`${
                  POLICIES_URL
                }#Maximum_number_of_times_a_finding_can_be_accepted`}
                target={"_blank"}
              />
            </Container>
            <GridContainer gap={0.25} lg={2} md={2} sm={2} xl={2}>
              <Text
                color={theme.palette.gray[400]}
                fontWeight={"bold"}
                size={"sm"}
              >
                {t("policies.duration")}
              </Text>
              <Text size={"sm"}>
                {`${returnDashIfNil(maxAcceptanceDays)} days, ${returnDashIfNil(maxNumberAcceptances)} times.`}
              </Text>
              <Text
                color={theme.palette.gray[400]}
                fontWeight={"bold"}
                size={"sm"}
              >
                {t("policies.severity")}
              </Text>
              <Text size={"sm"}>
                {`${minAcceptanceSeverity} min, ${maxAcceptanceSeverity} max.`}
              </Text>
            </GridContainer>
          </Container>
          <Container
            borderRight={`1px solid${theme.palette.gray[200]}`}
            display={"inline-flex"}
            flexDirection={"column"}
            gap={0.5}
          >
            <Container
              alignItems={"center"}
              display={"inline-flex"}
              gap={0.5}
              width={"fit-content"}
            >
              <Heading size={"xs"}>{t("policies.devSecOps")}</Heading>
              <Link
                href={`${
                  POLICIES_URL
                }#Minimum_CVSS_score_of_an_open_vulnerability_to_break_the_build`}
                target={"_blank"}
              />
            </Container>
            <GridContainer gap={0.25} lg={2} md={2} sm={2} xl={2}>
              <Text
                color={theme.palette.gray[400]}
                fontWeight={"bold"}
                size={"sm"}
              >
                {t("policies.gracePeriod")}
              </Text>
              <Text size={"sm"}>{`${vulnerabilityGracePeriod} days`}</Text>
              <Text
                color={theme.palette.gray[400]}
                fontWeight={"bold"}
                size={"sm"}
              >
                {t("policies.severity")}
              </Text>
              <Text size={"sm"}>{returnDashIfNil(minBreakingSeverity)}</Text>
            </GridContainer>
          </Container>
          <Container display={"inline-flex"} flexDirection={"column"} gap={0.5}>
            <Container
              alignItems={"center"}
              display={"inline-flex"}
              gap={0.5}
              width={"fit-content"}
            >
              <Heading size={"xs"}> {t("policies.inactivityPolicy")}</Heading>
              <Link
                href={`${
                  POLICIES_URL
                }#Number_of_days_after_which_a_member_is_removed_due_to_inactivity`}
                target={"_blank"}
              />
            </Container>
            <GridContainer gap={0.25} lg={2} md={2} sm={2} xl={2}>
              <Text
                color={theme.palette.gray[400]}
                fontWeight={"bold"}
                size={"sm"}
              >
                {t("policies.inactivityRemoval")}
              </Text>
              <Text size={"sm"}>{`${inactivityPeriod} days.`}</Text>
            </GridContainer>
          </Container>
        </GridContainer>
      </Container>
      <Heading size={"sm"}>{"Groups"}</Heading>
      <Table
        actionsContent={
          <Can
            do={"integrates_api_mutations_update_groups_policies__update_group"}
          >
            <Container display={"flex"} justify={"end"} width={"100%"}>
              <Button
                disabled={isEmpty(Object.keys(rowSelection))}
                icon={"undo"}
                onClick={handleReset}
                variant={"tertiary"}
              >
                {t("policies.reset.label")}
              </Button>
            </Container>
          </Can>
        }
        groupHeaders={true}
        id={"policies"}
        table={table}
      />
      <UpdateModal
        {...selectedPolicies}
        handleCancel={handleCloseOrCancel}
        handleSubmit={handlePoliciesSubmit}
        modalRef={modalRef}
        title={
          selectedPolicies.__typename === "Organization"
            ? t("policies.editOrgPolicies")
            : t("policies.editGroupPolicies", {
                group: capitalize(selectedPolicies.name),
              })
        }
      />
      <ConfirmDialog />
    </Container>
  );
};

export { Policies };
