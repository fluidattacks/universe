/* eslint-disable new-cap */
/* eslint-disable @typescript-eslint/no-unsafe-call */

import { Button } from "@fluidattacks/design";
import { type ColumnDef, createColumnHelper } from "@tanstack/react-table";
import { isNil } from "lodash";
import type React from "react";

import { Checkbox } from "components/checkbox";
import type { GetPoliciesAtOrganizationQuery } from "gql/graphql";

const addExtraColumns = (
  cols: ColumnDef<
    GetPoliciesAtOrganizationQuery["organization"]["groups"][0]
  >[],
  handleOnClickEdit: (
    row: GetPoliciesAtOrganizationQuery["organization"]["groups"][0],
  ) => VoidFunction,
): ColumnDef<GetPoliciesAtOrganizationQuery["organization"]["groups"][0]>[] => {
  const selectionColumn = createColumnHelper<
    GetPoliciesAtOrganizationQuery["organization"]["groups"][0]
  >().display({
    cell: ({ row }): React.ReactNode =>
      Checkbox({
        defaultChecked: row.getIsSelected(),
        disabled: !row.getCanSelect(),
        name: `select-${row.id}`,
        onChange: row.getToggleSelectedHandler(),
        value: String(row.getValue("name")),
        variant: "inputOnly",
      }) as React.ReactNode,
    enableResizing: false,
    header: ({ table }): React.ReactNode =>
      Checkbox({
        defaultChecked: table.getIsAllRowsSelected(),
        name: "select-all",
        onChange: table.getToggleAllPageRowsSelectedHandler(),
        value: "select-all",
        variant: "inputOnly",
      }) as React.ReactNode,
    id: "selection",
    maxSize: 18,
    minSize: 18,
    size: 18,
  });

  const editColumn = createColumnHelper<
    GetPoliciesAtOrganizationQuery["organization"]["groups"][0]
  >().display({
    cell: ({ row }): React.ReactNode =>
      // @ts-expect-error using forwardRef makes the type of the component incompatible with the expected type
      Button.render({
        children: "Edit",
        disabled: !row.getCanSelect(),
        icon: "edit",
        id: `${String(row.getValue("name"))}-edit`,
        name: `edit-${row.id}`,
        onClick: handleOnClickEdit(row.original),
        variant: "gosh",
      }) as React.ReactNode,
    enableResizing: false,
    header: "Action",
    id: "edit",
    maxSize: 100,
    minSize: 100,
    size: 100,
  });

  return [selectionColumn, ...cols, editColumn];
};

const returnDashIfNil = (value: number | string | null): string => {
  return isNil(value) ? "-" : String(value);
};

export { returnDashIfNil, addExtraColumns };
