import { Form, InnerForm, Modal } from "@fluidattacks/design";
import isNil from "lodash/isNil";
import { useCallback } from "react";

import { UpdateModalInputs } from "./inputs";

import type { IFormikPoliciesValues, IUpdateModalProps } from "../types";
import { validationSchema } from "../validations";

const UpdateModal = ({
  handleSubmit,
  title,
  modalRef,
  __typename,
  daysUntilItBreaks,
  maxAcceptanceDays,
  maxAcceptanceSeverity,
  maxNumberAcceptances,
  minAcceptanceSeverity,
  minBreakingSeverity,
  vulnerabilityGracePeriod,
  inactivityPeriod,
  name,
  handleCancel,
}: Readonly<IUpdateModalProps>): JSX.Element => {
  const initialValues: IFormikPoliciesValues = {
    __typename,
    daysUntilItBreaks,
    groups: __typename === "Group" ? [name] : undefined,
    inactivityPeriod: isNil(inactivityPeriod) ? undefined : inactivityPeriod,
    maxAcceptanceDays,
    maxAcceptanceSeverity,
    maxNumberAcceptances,
    minAcceptanceSeverity,
    minBreakingSeverity,
    vulnerabilityGracePeriod,
  };

  const onSubmit = useCallback(
    (values: IFormikPoliciesValues): void => {
      handleSubmit(values);
      modalRef.close();
    },
    [handleSubmit, modalRef],
  );

  return (
    <Modal modalRef={modalRef} onClose={handleCancel} size={"md"} title={title}>
      <Form
        cancelButton={{ onClick: handleCancel }}
        defaultValues={initialValues}
        id={"policies"}
        onSubmit={onSubmit}
        yupSchema={validationSchema(isNil(inactivityPeriod))}
      >
        <InnerForm<IFormikPoliciesValues>>
          {(form): JSX.Element => <UpdateModalInputs form={form} />}
        </InnerForm>
      </Form>
    </Modal>
  );
};

export { UpdateModal };
