import {
  Button,
  Container,
  Icon,
  Link,
  PremiumFeature,
  Text,
} from "@fluidattacks/design";
import { Fragment, useCallback, useState } from "react";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

import { TalkToHackerModal } from "features/talk-to-a-hacker";
import { UpgradeFreeTrialModal } from "features/upgrade-free-trial-users";
import { UpgradeGroupsModal } from "features/upgrade-groups-modal";
import { ManagedType } from "gql/graphql";
import { useCalendly, useTrial } from "hooks";

interface IExpertButton {
  display?: "block" | "flex";
  text?: string;
  featurePreview?: boolean;
}
const talkToAHackerAdvancedImg = "integrates/plans/talk-to-a-hacker";

const ExpertButton = ({
  display = "flex",
  text,
  featurePreview = false,
}: Readonly<IExpertButton>): JSX.Element => {
  const theme = useTheme();
  const { t } = useTranslation();
  const trial = useTrial();
  const {
    closeUpgradeModal,
    isAdvancedActive,
    isAvailable,
    isUpgradeOpen,
    openUpgradeModal,
  } = useCalendly();
  const [isTalkToHackerModalOpen, setIsTalkToHackerModalOpen] = useState(false);

  const openTalkToHackerModal = useCallback((): void => {
    if (isAdvancedActive) {
      setIsTalkToHackerModalOpen(true);
    } else {
      openUpgradeModal();
    }
  }, [isAdvancedActive, openUpgradeModal]);
  const closeTalkToHackerModal = useCallback((): void => {
    setIsTalkToHackerModalOpen(false);
  }, []);

  const SPACING = 0.25;

  if (featurePreview) {
    return (
      <Fragment>
        <Container alignItems={"center"} display={"flex"} gap={1}>
          <Text
            color={theme.palette.black}
            display={"inline"}
            lineSpacing={1}
            size={"sm"}
            textAlign={"center"}
          >
            {"Need help with these vulnerabilities?"}
          </Text>
          <Container alignItems={"center"} display={"flex"}>
            {isAdvancedActive ? (
              <Icon
                icon={"circle-question"}
                iconColor={theme.palette.primary[500]}
                iconSize={"xxs"}
                iconType={"fa-light"}
                ml={0.5}
                mr={0.25}
              />
            ) : (
              <PremiumFeature margin={0.25} />
            )}

            <Link
              href={"#"}
              iconPosition={"left"}
              onClick={openTalkToHackerModal}
              variant={"highRelevance"}
            >
              {t("group.findings.buttons.help.button")}
            </Link>
          </Container>
        </Container>
        {isUpgradeOpen && trial?.trial.state === ManagedType.Trial && (
          <UpgradeFreeTrialModal
            description={t("components.sideBar.help.expert.trial.description")}
            img={talkToAHackerAdvancedImg}
            onClose={closeUpgradeModal}
            title={t("components.sideBar.help.expert.trial.title")}
          />
        )}
        {isUpgradeOpen && trial === null && (
          <UpgradeGroupsModal onClose={closeUpgradeModal} />
        )}
        {isTalkToHackerModalOpen ? (
          <TalkToHackerModal closeTalkToHackerModal={closeTalkToHackerModal} />
        ) : undefined}
      </Fragment>
    );
  }

  return (
    <Fragment>
      <Container
        display={isAvailable ? "flex" : "none"}
        justify={"start"}
        mt={1}
      >
        <Container alignItems={"center"} display={display}>
          <Text
            color={theme.palette.gray[800]}
            fontWeight={"bold"}
            mb={display === "block" ? SPACING : 0}
            mr={1}
            size={"sm"}
          >
            {text}
          </Text>
          <Button icon={"headset"} onClick={openTalkToHackerModal}>
            {t("group.findings.buttons.help.button")}
            {!isAdvancedActive && <PremiumFeature />}
          </Button>
        </Container>
      </Container>
      {isUpgradeOpen && trial?.trial.state === ManagedType.Trial && (
        <UpgradeFreeTrialModal
          description={t("components.sideBar.help.expert.trial.description")}
          img={talkToAHackerAdvancedImg}
          onClose={closeUpgradeModal}
          title={t("components.sideBar.help.expert.trial.title")}
        />
      )}
      {isUpgradeOpen && trial === null && (
        <UpgradeGroupsModal onClose={closeUpgradeModal} />
      )}
      {isTalkToHackerModalOpen ? (
        <TalkToHackerModal closeTalkToHackerModal={closeTalkToHackerModal} />
      ) : undefined}
    </Fragment>
  );
};

export { ExpertButton };
