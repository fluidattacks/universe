import { type IUseFilterProps, useFilters } from "@fluidattacks/design";
import type { IOptionsProps } from "@fluidattacks/design";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { selectOptionType } from "pages/group/events/constants";
import type { IEventData } from "pages/group/events/types";
import { castEventStatus } from "utils/format-helpers";

const useEventsFilters = ({
  dataset,
  groupName,
  setFilteredDataset,
}: Readonly<{
  dataset: IEventData[];
  groupName: string;
  setFilteredDataset: React.Dispatch<React.SetStateAction<IEventData[]>>;
}>): IUseFilterProps<IEventData> => {
  const { t } = useTranslation();

  const options: IOptionsProps<IEventData>[] = React.useMemo(
    (): IOptionsProps<IEventData>[] => [
      {
        filterOptions: [
          {
            key: "eventDate",
            label: t("searchFindings.tabEvents.date"),
            type: "dateRange",
          },
        ],
        label: t("searchFindings.tabEvents.date"),
      },
      {
        filterOptions: [
          {
            key: "eventType",
            label: t("searchFindings.tabEvents.type"),
            options: selectOptionType,
            type: "checkboxes",
          },
        ],
        label: t("searchFindings.tabEvents.type"),
      },
      {
        filterOptions: [
          {
            key: "eventStatus",
            label: t("searchFindings.tabEvents.status"),
            options: [
              {
                label: t(castEventStatus("VERIFICATION_REQUESTED").status),
                value: "Pending verification",
              },
              {
                label: t(castEventStatus("CREATED").status),
                value: "Unsolved",
              },
              { label: t(castEventStatus("SOLVED").status), value: "Solved" },
            ],
            type: "checkboxes",
          },
        ],
        label: t("searchFindings.tabEvents.status"),
      },
      {
        filterOptions: [
          {
            key: "closingDate",
            label: t("searchFindings.tabEvents.dateClosed"),
            type: "dateRange",
          },
        ],
        label: t("searchFindings.tabEvents.dateClosed"),
      },
    ],
    [t],
  );

  return useFilters({
    dataset,
    localStorageKey: `tblEventsFilters-${groupName}`,
    options,
    setFilteredDataset,
  });
};

export { useEventsFilters };
