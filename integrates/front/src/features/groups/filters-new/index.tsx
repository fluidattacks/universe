import type { IOptionsProps } from "@fluidattacks/design";
import { type IUseFilterProps, useFilters } from "@fluidattacks/design";
import { useMemo } from "react";
import { useTranslation } from "react-i18next";

import type { IGroupFilters } from "./types";

import type { IGroupData } from "pages/organization/groups/types";

const useGroupsFilters = ({
  dataset,
  setFilteredDataset,
}: Readonly<IGroupFilters>): IUseFilterProps<IGroupData> => {
  const { t } = useTranslation();
  const options: IOptionsProps<IGroupData>[] = useMemo(
    (): IOptionsProps<IGroupData>[] => [
      {
        filterOptions: [
          {
            key: "name",
            label: t("organization.tabs.groups.newGroup.name.text"),
            type: "text",
          },
        ],
        label: t("organization.tabs.groups.newGroup.name.text"),
      },
      {
        filterOptions: [
          {
            key: "plan",
            label: t("organization.tabs.groups.plan"),
            options: [
              {
                label: "Advanced",
                value: "Advanced",
              },
              {
                label: "Essential",
                value: "Essential",
              },
              {
                label: "Oneshot",
                value: "Oneshot",
              },
            ],
            type: "checkboxes",
          },
        ],
        label: t("organization.tabs.groups.plan"),
      },
    ],
    [t],
  );

  return useFilters({
    dataset,
    localStorageKey: "tblGroupsFilters",
    options,
    setFilteredDataset,
  });
};

export { useGroupsFilters };
