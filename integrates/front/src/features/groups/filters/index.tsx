import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";

import type { IGroupFilters } from "./types";

import { Filters } from "components/filter";
import type { IFilter, IPermanentData } from "components/filter/types";
import { useStoredState } from "hooks/use-stored-state";
import type { IGroupData } from "pages/organization/groups/types";
import { setFiltersUtil } from "utils/set-filters";

const GroupsFilters = ({
  dataset,
  setFilteredDataSet,
}: Readonly<IGroupFilters>): JSX.Element => {
  const { t } = useTranslation();
  const [filters, setFilters] = useState<IFilter<IGroupData>[]>([
    {
      filterFn: "includesInsensitive",
      id: "name",
      key: "name",
      label: t("organization.tabs.groups.newGroup.name.text"),
      type: "text",
    },
    {
      id: "plan",
      key: "plan",
      label: t("organization.tabs.groups.plan"),
      selectOptions: ["Advanced", "Essential", "Oneshot"],
      type: "select",
    },
  ]);

  const [filtersPermaset, setFiltersPermaset] = useStoredState<
    IPermanentData[]
  >(
    "tblGroupsFilters",
    [
      { id: "name", value: "" },
      { id: "plan", value: "" },
    ],
    localStorage,
  );

  useEffect((): void => {
    setFilteredDataSet(setFiltersUtil(dataset, filters));
  }, [filters, dataset, setFilteredDataSet]);

  return (
    <Filters
      filters={filters}
      permaset={[filtersPermaset, setFiltersPermaset]}
      setFilters={setFilters}
    />
  );
};

export { GroupsFilters };
