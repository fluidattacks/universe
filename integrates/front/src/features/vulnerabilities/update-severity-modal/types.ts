import type { IVulnDataTypeAttr } from "../types";

interface IUpdateSeverityProps {
  findingId: string;
  isOpen: boolean;
  vulnerabilities: IVulnDataTypeAttr[];
  handleCloseModal: () => void;
  refetchData: () => void;
}

export type { IUpdateSeverityProps };
