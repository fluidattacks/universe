import { Container, Link } from "@fluidattacks/design";
import { useTranslation } from "react-i18next";

import { Editable, TextArea } from "components/input";
import { Can } from "context/authz/can";

interface IFindingDescription {
  baseCriteriaUrl: string;
  description: string;
  findingNumber: string;
  isEditing: boolean;
  track: (url: string) => () => void;
}

const FindingDescription = ({
  baseCriteriaUrl,
  description,
  findingNumber,
  isEditing,
  track,
}: Readonly<IFindingDescription>): JSX.Element | null => {
  const MAX_DESCRIPTION_LENGTH = 1000;
  const { t } = useTranslation();

  return (
    <Can
      do={"integrates_api_mutations_update_finding_description_mutate"}
      passThrough={true}
    >
      {(canEdit: boolean): JSX.Element => (
        <Container mb={1}>
          <Editable
            currentValue={description}
            isEditing={isEditing ? canEdit : false}
            label={t("searchFindings.tabDescription.description.text")}
          >
            <TextArea
              id={"searchFindings.tabDescription.description.tooltip"}
              label={t("searchFindings.tabDescription.description.text")}
              maxLength={MAX_DESCRIPTION_LENGTH}
              name={"description"}
              tooltip={t("searchFindings.tabDescription.description.tooltip")}
              weight={"bold"}
            />
          </Editable>
          {isEditing && canEdit ? undefined : (
            <Link
              href={`${baseCriteriaUrl}-vulnerabilities-${findingNumber}`}
              onClick={track(
                `${baseCriteriaUrl}-vulnerabilities-${findingNumber}`,
              )}
            >
              {t("searchFindings.tabDescription.description.infoLinkText")}
            </Link>
          )}
        </Container>
      )}
    </Can>
  );
};

export { FindingDescription };
