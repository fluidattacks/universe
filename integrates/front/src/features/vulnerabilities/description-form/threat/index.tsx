import { Container } from "@fluidattacks/design";
import { useTranslation } from "react-i18next";

import { Editable, TextArea } from "components/input";
import { Can } from "context/authz/can";

interface IThreat {
  isEditing: boolean;
  threat: string;
}

const Threat = ({
  isEditing,
  threat,
}: Readonly<IThreat>): JSX.Element | null => {
  const MAX_THREAT_LENGTH = 650;
  const { t } = useTranslation();

  return (
    <Can
      do={"integrates_api_mutations_update_finding_description_mutate"}
      passThrough={true}
    >
      {(canEdit: boolean): JSX.Element => (
        <Container mb={1}>
          <Editable
            currentValue={threat}
            isEditing={isEditing ? canEdit : false}
            label={t("searchFindings.tabDescription.threat.text")}
          >
            <TextArea
              id={"searchFindings.tabDescription.threat.tooltip"}
              label={t("searchFindings.tabDescription.threat.text")}
              maxLength={MAX_THREAT_LENGTH}
              name={"threat"}
              tooltip={t("searchFindings.tabDescription.threat.tooltip")}
              weight={"bold"}
            />
          </Editable>
        </Container>
      )}
    </Can>
  );
};

export { Threat };
