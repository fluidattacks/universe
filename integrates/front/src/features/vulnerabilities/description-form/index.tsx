import {
  Alert,
  Container,
  Divider,
  GridContainer,
  Text,
} from "@fluidattacks/design";
import { Form, useFormikContext } from "formik";
import _ from "lodash";
import mixpanel from "mixpanel-browser";
import { useCallback, useEffect, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

import { FindingDescription } from "./finding-description";
import { FindingTitle } from "./finding-title";
import { Hacker } from "./hacker";
import { Impacts } from "./impacts";
import { Recommendation } from "./recommendation";
import { Requirements } from "./requirements";
import { SeverityVector } from "./severity-vector";
import { Sorts } from "./sorts";
import { Threat } from "./threat";

import { Have } from "context/authz/have";
import { ExpertButton } from "features/expert-button";
import { useAudit } from "hooks/use-audit";
import { ActionButtons } from "pages/finding/description/action-buttons";
import type {
  IFinding,
  IFindingDescriptionData,
  IUnfulfilledRequirement,
} from "pages/finding/description/types";
import {
  formatRequirements,
  validateNotEmpty,
} from "pages/finding/description/utils";
import { severityFormatter } from "utils/format-helpers";

interface IDescriptionFormProps {
  readonly data: IFindingDescriptionData;
  readonly isDraft: boolean;
  readonly isEditing: boolean;
  readonly groupLanguage: string | undefined;
  readonly setEditing: React.Dispatch<React.SetStateAction<boolean>>;
  readonly setSuggestions: React.Dispatch<React.SetStateAction<string[]>>;
}

const DescriptionForm: React.FC<IDescriptionFormProps> = ({
  data,
  isDraft,
  isEditing,
  groupLanguage,
  setEditing,
  setSuggestions,
}): JSX.Element => {
  const theme = useTheme();
  const { dirty, resetForm, submitForm, setValues, values, initialValues } =
    useFormikContext<IFinding & { requirementIds: string[] }>();
  const { t } = useTranslation();

  const isDescriptionPristine = !dirty;

  const criteriaIdSlice = 3;
  const findingNumber = values.title.slice(0, criteriaIdSlice);
  const baseCriteriaUrl =
    "https://help.fluidattacks.com/portal/en/kb/articles/criteria";

  const [titleSuggestions, setTitleSuggestions] = useState<string[]>([]);

  const [criteriaBaseRequirements, setCriteriaBaseRequirements] = useState<
    IUnfulfilledRequirement[] | undefined
  >(undefined);

  const toggleEdit = useCallback((): void => {
    if (!isDescriptionPristine) {
      resetForm();
    }
    setEditing(!isEditing);
  }, [isDescriptionPristine, isEditing, resetForm, setEditing]);

  const handleSubmit = useCallback((): void => {
    if (!isDescriptionPristine) {
      void submitForm();
    }
  }, [isDescriptionPristine, submitForm]);

  const { addAuditEvent } = useAudit();
  const track = useCallback(
    (url: string): VoidFunction =>
      (): void => {
        mixpanel.track("VulnerabilityLink", { url });
        addAuditEvent("VulnerabilityLink", url);
      },
    [addAuditEvent],
  );

  useEffect((): void => {
    if (!_.isNil(data.criteriaVulnerabilities) && !_.isNil(findingNumber)) {
      const titlesList = Object.keys(data.criteriaVulnerabilities).map(
        (key: string): string => {
          return groupLanguage === "ES"
            ? validateNotEmpty(data.criteriaVulnerabilities[key].es.title)
            : validateNotEmpty(data.criteriaVulnerabilities[key].en.title);
        },
      );
      setTitleSuggestions(titlesList);
      setSuggestions(titlesList);
    }
  }, [
    data.criteriaVulnerabilities,
    findingNumber,
    groupLanguage,
    setSuggestions,
  ]);

  useEffect(
    (): void => {
      if (
        isEditing &&
        !_.isUndefined(data.criteriaVulnerabilities) &&
        !_.isUndefined(data.criteriaRequirements) &&
        !_.isEmpty(findingNumber)
      ) {
        const EMPTY_REQUIREMENTS = { requirements: [] };
        const { requirements } = _.isUndefined(
          data.criteriaVulnerabilities[findingNumber],
        )
          ? EMPTY_REQUIREMENTS
          : data.criteriaVulnerabilities[findingNumber];
        setCriteriaBaseRequirements(
          formatRequirements(requirements, data.criteriaRequirements),
        );
        if (data.finding.title.startsWith(findingNumber)) {
          void setValues({
            ...values,
            requirementIds: initialValues.requirementIds,
          });
        } else {
          void setValues({ ...values, requirementIds: requirements });
        }
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [
      data.criteriaRequirements,
      data.criteriaVulnerabilities,
      data.finding.title,
      findingNumber,
      isEditing,
      setValues,
    ],
  );

  if (_.isUndefined(data) || _.isEmpty(data)) {
    return <div />;
  }

  const dataset: IFinding = data.finding;

  return (
    <Form data-private={true} id={"editDescription"}>
      <Have I={"can_report_vulnerabilities"}>
        <ActionButtons
          canEdit={false}
          isEditing={isEditing}
          isPristine={isDescriptionPristine}
          onEdit={toggleEdit}
          onUpdate={handleSubmit}
        />
      </Have>
      <Container mb={1}>
        <Hacker hacker={dataset.hacker} />
      </Container>
      <GridContainer lg={2} md={2} sm={2} xl={2}>
        <Container>
          <FindingTitle
            isDraft={isDraft}
            isEditing={isEditing}
            titleSuggestions={titleSuggestions}
          />
          <FindingDescription
            baseCriteriaUrl={baseCriteriaUrl}
            description={dataset.description}
            findingNumber={findingNumber}
            isEditing={isEditing}
            track={track}
          />
          <Requirements
            baseCriteriaUrl={baseCriteriaUrl}
            criteriaBaseRequirements={criteriaBaseRequirements}
            isEditing={isEditing}
            track={track}
            unfulfilledRequirement={dataset.unfulfilledRequirements}
          />
          <Impacts
            attackVectorDescription={dataset.attackVectorDescription}
            isEditing={isEditing}
          />
        </Container>
        <Container>
          <Threat isEditing={isEditing} threat={dataset.threat} />
          <Recommendation
            isEditing={isEditing}
            recommendation={dataset.recommendation}
          />
          <Sorts isEditing={isEditing} />
          {isEditing ? undefined : (
            <ExpertButton
              display={"block"}
              text={t("searchFindings.tabDescription.helpButton")}
            />
          )}
        </Container>
      </GridContainer>
      <Divider color={theme.palette.gray[300]} mb={1} mt={1} />
      {isEditing ? undefined : (
        <Container mb={1}>
          <Alert closable={true} variant={"info"}>
            {t("searchFindings.tabDescription.defaultSeverity.textAlertInfo")}
          </Alert>
        </Container>
      )}
      <GridContainer lg={2} md={2} sm={2} xl={2}>
        <GridContainer gap={1} lg={1} md={1} sm={1} xl={1}>
          <SeverityVector
            isEditing={isEditing}
            label={t(
              "searchFindings.tabDescription.defaultSeverity.textVectorString",
            )}
            severityVector={dataset.severityVector}
            tooltip={t(
              "searchFindings.tabDescription.defaultSeverity.editableTooltip",
            )}
            variant={"v3"}
          />
          <SeverityVector
            isEditing={isEditing}
            label={t(
              "searchFindings.tabDescription.defaultSeverityV4.textVectorString",
            )}
            severityVector={dataset.severityVectorV4 ?? undefined}
            tooltip={t(
              "searchFindings.tabDescription.defaultSeverityV4.editableTooltip",
            )}
            variant={"v4"}
          />
        </GridContainer>
        <GridContainer gap={1} lg={1} md={1} sm={1} xl={1}>
          {isEditing ? undefined : (
            <Container>
              <Text
                color={theme.palette.gray["800"]}
                fontWeight={"bold"}
                size={"sm"}
              >
                {t(
                  "searchFindings.tabDescription.defaultSeverity.textSeverityScore",
                )}
              </Text>
              {severityFormatter(dataset.severityScore)}
            </Container>
          )}
          {isEditing || dataset.severityScoreV4 === null ? undefined : (
            <Container>
              <Text
                color={theme.palette.gray["800"]}
                fontWeight={"bold"}
                size={"sm"}
              >
                {t(
                  "searchFindings.tabDescription.defaultSeverityV4.textSeverityScore",
                )}
              </Text>
              {severityFormatter(dataset.severityScoreV4)}
            </Container>
          )}
        </GridContainer>
      </GridContainer>
    </Form>
  );
};

export { DescriptionForm };
