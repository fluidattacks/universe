import { Container } from "@fluidattacks/design";
import sortBy from "lodash/sortBy";
import { useTranslation } from "react-i18next";

import { Input } from "components/input";
import { Can } from "context/authz/can";

interface IFindingTitle {
  isEditing: boolean;
  isDraft: boolean;
  titleSuggestions: string[];
}

const FindingTitle = ({
  isEditing,
  isDraft,
  titleSuggestions,
}: Readonly<IFindingTitle>): JSX.Element | null => {
  const { t } = useTranslation();

  return (
    <Can do={"integrates_api_mutations_update_finding_description_mutate"}>
      {isEditing && isDraft ? (
        <Container mb={1}>
          <Input
            label={t("searchFindings.tabDescription.title.text")}
            list={"title-list"}
            name={"title"}
            suggestions={sortBy(titleSuggestions)}
            tooltip={t("searchFindings.tabDescription.title.tooltip")}
            weight={"bold"}
          />
        </Container>
      ) : undefined}
    </Can>
  );
};

export { FindingTitle };
