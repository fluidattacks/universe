import { useTranslation } from "react-i18next";

import { Editable, TextArea } from "components/input";
import { Can } from "context/authz/can";

interface IRecommendation {
  isEditing: boolean;
  recommendation: string;
}

const Recommendation = ({
  isEditing,
  recommendation,
}: Readonly<IRecommendation>): JSX.Element | null => {
  const MAX_RECOMMENDATION_LENGTH = 500;
  const { t } = useTranslation();

  return (
    <Can
      do={"integrates_api_mutations_update_finding_description_mutate"}
      passThrough={true}
    >
      {(canEdit: boolean): JSX.Element => (
        <Editable
          currentValue={recommendation}
          isEditing={isEditing ? canEdit : false}
          label={t("searchFindings.tabDescription.recommendation.text")}
        >
          <TextArea
            id={"searchFindings.tabDescription.recommendation.tooltip"}
            label={t("searchFindings.tabDescription.recommendation.text")}
            maxLength={MAX_RECOMMENDATION_LENGTH}
            name={"recommendation"}
            tooltip={t("searchFindings.tabDescription.recommendation.tooltip")}
            weight={"bold"}
          />
        </Editable>
      )}
    </Can>
  );
};

export { Recommendation };
