import { Container } from "@fluidattacks/design";
import { useTranslation } from "react-i18next";

import { Editable, TextArea } from "components/input";
import { Can } from "context/authz/can";

interface IImpacts {
  attackVectorDescription: string;
  isEditing: boolean;
}

const Impacts = ({
  attackVectorDescription,
  isEditing,
}: Readonly<IImpacts>): JSX.Element | null => {
  const MAX_IMPACTS_LENGTH = 650;
  const { t } = useTranslation();

  return (
    <Can
      do={"integrates_api_mutations_update_finding_description_mutate"}
      passThrough={true}
    >
      {(canEdit: boolean): JSX.Element => (
        <Container>
          <Editable
            currentValue={attackVectorDescription}
            isEditing={isEditing ? canEdit : false}
            label={t("searchFindings.tabDescription.attackVectors.text")}
          >
            <TextArea
              id={"searchFindings.tabDescription.attackVectors.tooltip"}
              label={t("searchFindings.tabDescription.attackVectors.text")}
              maxLength={MAX_IMPACTS_LENGTH}
              name={"attackVectorDescription"}
              tooltip={t("searchFindings.tabDescription.attackVectors.tooltip")}
              weight={"bold"}
            />
          </Editable>
        </Container>
      )}
    </Can>
  );
};

export { Impacts };
