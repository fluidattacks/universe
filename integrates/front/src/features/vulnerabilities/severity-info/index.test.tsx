import { PureAbility } from "@casl/ability";
import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import isEqual from "lodash/isEqual";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import { Route, Routes } from "react-router-dom";

import { UPDATE_VULNERABILITIES_SEVERITY } from "./queries";

import { SeverityInfo } from ".";
import { authzPermissionsContext } from "context/authz/config";
import type { UpdateVulnerabilitiesSeverityMutation as UpdateVulnerabilitiesSeverity } from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";
import { msgSuccess } from "utils/notifications";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

describe("vulnerabilitySeverityInfo", (): void => {
  const vulnId = "af7a48b8-d8fc-41da-9282-d424fff563f0";
  const findingId = "438679960";

  it("should render a component", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <SeverityInfo
        findingId={findingId}
        refetchData={jest.fn()}
        vulnerabilityId={vulnId}
      />,
      {
        memoryRouter: {
          initialEntries: [
            "/TEST/vulns/438679960/locations/af7a48b8-d8fc-41da-9282-d424fff563f0",
          ],
        },
      },
    );

    await waitFor((): void => {
      expect(
        screen.getByText(
          "searchFindings.tabVuln.severityInfo.severityVectorTitle",
        ),
      ).toBeInTheDocument();
    });

    expect(
      screen.getByText(
        "CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:L/A:N/E:U/RL:O/RC:C",
      ),
    ).toBeInTheDocument();
    expect(
      screen.getByText(
        "CVSS:4.0/AV:N/AC:L/AT:N/PR:L/UI:N/VC:N/VI:L/VA:N/SC:N/SI:N/SA:N/E:U",
      ),
    ).toBeInTheDocument();
    expect(screen.getByText("3.8")).toBeInTheDocument();
    expect(screen.getByText("3.9")).toBeInTheDocument();
    expect(screen.getAllByText("Low")).toHaveLength(2);

    expect(
      screen.queryByText("searchFindings.tabSeverity.privilegesRequired.label"),
    ).toBeInTheDocument();
    expect(
      screen.queryByText(
        "searchFindings.tabSeverity.exploitability.options.proofOfConcept.label",
      ),
    ).not.toBeInTheDocument();
  });

  it("should update vulnerability severity vector", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();
    jest.spyOn(console, "error").mockImplementation();

    const modifiedCvssVector = "CVSS:3.1/AV:N/AC:H/PR:H/UI:N/S:C/C:H/I:H/A:H";
    const modifiedCvss4Vector =
      "CVSS:4.0/AV:N/AC:H/AT:N/PR:H/UI:N/VC:H/VI:H/VA:H/SC:L/SI:L/SA:L";
    const mockedMutation = [
      graphql
        .link(LINK)
        .mutation(
          UPDATE_VULNERABILITIES_SEVERITY,
          ({
            variables,
          }): StrictResponse<
            IErrorMessage | { data: UpdateVulnerabilitiesSeverity }
          > => {
            const {
              cvssVector,
              cvss4Vector,
              findingId: id,
              vulnerabilityIds,
            } = variables;

            if (
              cvssVector === modifiedCvssVector &&
              cvss4Vector === modifiedCvss4Vector &&
              id === findingId &&
              isEqual(vulnerabilityIds, [vulnId])
            ) {
              return HttpResponse.json({
                data: {
                  updateVulnerabilitiesSeverity: {
                    success: true,
                  },
                },
              });
            }

            return HttpResponse.json({
              errors: [new GraphQLError("An error occurred updating severity")],
            });
          },
        ),
    ];
    const mockedPermissions = new PureAbility<string>([
      {
        action:
          "integrates_api_mutations_update_vulnerabilities_severity_mutate",
      },
    ]);

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={
              <SeverityInfo
                findingId={findingId}
                refetchData={jest.fn()}
                vulnerabilityId={vulnId}
              />
            }
            path={
              "/orgs/:organizationName/groups/:groupName/vulns/:findingId/locations/:vulnId/*"
            }
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: [
            "/orgs/okada/groups/TEST/vulns/438679960/locations/af7a48b8-d8fc-41da-9282-d424fff563f0/severity",
          ],
        },
        mocks: mockedMutation,
      },
    );

    await waitFor((): void => {
      expect(
        screen.getByText(
          "searchFindings.tabVuln.severityInfo.severityVectorTitle",
        ),
      ).toBeInTheDocument();
    });

    const editButton = screen.getByRole("button", {
      name: "searchFindings.tabVuln.additionalInfo.buttons.edit",
    });

    expect(editButton).toBeInTheDocument();
    expect(editButton).toBeEnabled();

    await userEvent.click(editButton);

    await waitFor((): void => {
      expect(
        screen.getByRole("button", {
          name: "searchFindings.tabVuln.additionalInfo.buttons.save",
        }),
      ).toBeInTheDocument();
    });

    // Severity score pill nor Severity Tiles are not visible on Edit Mode
    expect(screen.queryByText("3.8")).not.toBeInTheDocument();
    expect(screen.queryByText("Low")).not.toBeInTheDocument();

    expect(
      screen.queryByText("searchFindings.tabSeverity.privilegesRequired.label"),
    ).not.toBeInTheDocument();

    const saveButton = screen.getByRole("button", {
      name: "searchFindings.tabVuln.additionalInfo.buttons.save",
    });

    expect(saveButton).toBeDisabled();

    await userEvent.clear(
      screen.getByRole("combobox", { name: "severityVector" }),
    );
    await userEvent.clear(
      screen.getByRole("combobox", { name: "severityVectorV4" }),
    );
    await userEvent.type(
      screen.getByRole("combobox", { name: "severityVector" }),
      modifiedCvssVector,
    );
    await userEvent.type(
      screen.getByRole("combobox", { name: "severityVectorV4" }),
      modifiedCvss4Vector,
    );

    await waitFor((): void => {
      expect(saveButton).toBeEnabled();
    });

    await userEvent.click(saveButton);

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "searchFindings.tabVuln.severityInfo.alerts.updatedSeverity",
        "groupAlerts.updatedTitle",
      );
    });
  });
});
