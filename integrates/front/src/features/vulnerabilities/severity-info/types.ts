interface ISeverityInfoProps {
  findingId: string;
  refetchData: () => void;
  cvssVersion?: string;
  vulnerabilityId: string;
}

export type { ISeverityInfoProps };
