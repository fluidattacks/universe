import { screen, waitFor } from "@testing-library/react";
import { GraphQLError } from "graphql";
import { HttpResponse, type StrictResponse, graphql } from "msw";

import { GET_VULN_TREATMENT } from "./queries";

import { TreatmentTracking } from ".";
import { type IVulnRowAttr, vulnerabilityStates } from "../types";
import { authContext } from "context/auth";
import {
  type GetVulnTreatmentQuery as GetVulnTreatment,
  RootCriticality,
  VulnerabilityRejectionReason,
} from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";
import { formatDropdownField } from "utils/format-helpers";

describe("trackingTreatment", (): void => {
  const mock: IVulnRowAttr = {
    advisories: null,
    assigned: "",
    closingDate: null,
    customSeverity: 3,
    externalBugTrackingSystem: null,
    findingId: "testgroup",
    groupName: "480857698",
    historicTreatment: [
      {
        acceptanceDate: "",
        acceptanceStatus: "",
        assigned: "usertreatment@test.test",
        date: "2019-07-05 09:56:40",
        justification: "test progress justification",
        treatment: "IN_PROGRESS",
        user: "usertreatment@test.test",
      },
    ],
    id: "af7a48b8-d8fc-41da-9282-d424fff563f0",
    lastEditedBy: "usertreatment@test.test",
    lastStateDate: "2019-07-05 09:56:40",
    lastTreatmentDate: "2019-07-05 09:56:40",
    lastVerificationDate: null,
    organizationName: undefined,
    priority: 0,
    proposedSeverityAuthor: null,
    proposedSeverityTemporalScore: null,
    proposedSeverityThreatScore: null,
    proposedSeverityVector: null,
    proposedSeverityVectorV4: null,
    remediated: true,
    reportDate: "",
    root: {
      __typename: "GitRoot",
      branch: "master",
      criticality: RootCriticality.Low,
    },
    rootNickname: "https:",
    severityTemporalScore: 3.0,
    severityThreatScore: 3.0,
    severityVector: "AV:A/AC:H/PR:L/UI:R/S:C/C:L/I:L/A:L/E:F/RL:O/RC:U",
    severityVectorV4:
      "CVSS:4.0/AV:A/AC:H/PR:L/UI:P/VC:L/VI:L/VA:L/SC:L/SI:L/SA:L/E:A",
    source: "asm",
    specific: "specific-1",
    state: "VULNERABLE",
    stateReasons: null,
    stream: null,
    tag: "tag-1, tag-2",
    technique: "SCR",
    treatmentAcceptanceDate: "",
    treatmentAcceptanceStatus: "",
    treatmentAssigned: "",
    treatmentDate: "2019-07-05 09:56:40",
    treatmentJustification: "test progress justification",
    treatmentStatus: formatDropdownField("IN_PROGRESS"),
    treatmentUser: "usertreatment@test.test",
    verification: "Requested",
    verificationJustification: null,
    vulnerabilityType: "inputs",
    where: "https://example.com/inputs",
    zeroRisk: null,
  };

  const memoryRouter = {
    initialEntries: ["/TEST/vulns/438679960/locations"],
  };

  const mockQueryVulnTreatment1 = graphql
    .link(LINK)
    .query(
      GET_VULN_TREATMENT,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: GetVulnTreatment }> => {
        const { vulnId: id } = variables;
        if (id === mock.id) {
          return HttpResponse.json({
            data: {
              vulnerability: {
                __typename: "Vulnerability",
                historicTreatmentConnection: {
                  edges: [
                    {
                      node: {
                        acceptanceDate: "",
                        acceptanceStatus: "",
                        assigned: "",
                        date: "2019-01-16 10:36:04",
                        justification: "",
                        treatment: "UNTREATED",
                        user: "",
                      },
                    },
                    {
                      node: {
                        acceptanceDate: "",
                        acceptanceStatus: "",
                        assigned: "",
                        date: "2019-01-17 10:06:04",
                        justification: "",
                        treatment: "UNTREATED",
                        user: "",
                      },
                    },
                    {
                      node: {
                        acceptanceDate: "",
                        acceptanceStatus: "SUBMITTED",
                        assigned: "usermanager1@test.test",
                        date: "2020-02-17 18:36:24",
                        justification: "Some of the resources",
                        treatment: "ACCEPTED_UNDEFINED",
                        user: "usertreatment1@test.test",
                      },
                    },
                    {
                      node: {
                        acceptanceDate: "",
                        acceptanceStatus: "APPROVED",
                        assigned: "usermanager2@test.test",
                        date: "2020-02-18 18:36:24",
                        justification: "The headers must be",
                        treatment: "ACCEPTED_UNDEFINED",
                        user: "usertreatment2@test.test",
                      },
                    },
                    {
                      node: {
                        acceptanceDate: "2020-10-09 15:29:48",
                        acceptanceStatus: "",
                        assigned: "usermanager3@test.test",
                        date: "2020-10-02 15:29:48",
                        justification: "The headers must be",
                        treatment: "ACCEPTED",
                        user: "usertreatment3@test.test",
                      },
                    },
                    {
                      node: {
                        acceptanceDate: "",
                        acceptanceStatus: "",
                        assigned: "usermanager4@test.test",
                        date: "2020-10-08 15:29:48",
                        justification: "The headers must be",
                        treatment: "IN_PROGRESS",
                        user: "usertreatment4@test.test",
                      },
                    },
                  ],
                  pageInfo: {
                    endCursor: "",
                    hasNextPage: false,
                  },
                },
                id: "af7a48b8-d8fc-41da-9282-d424fff563f0",
              },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error getting vulnerability treatment")],
        });
      },
    );

  const rejectedObservation = "The treatment was rejected";
  const mockQueryVulnTreatment2 = graphql
    .link(LINK)
    .query(
      GET_VULN_TREATMENT,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: GetVulnTreatment }> => {
        const { vulnId: id } = variables;
        if (id === mock.id) {
          return HttpResponse.json({
            data: {
              vulnerability: {
                __typename: "Vulnerability",
                historicTreatmentConnection: {
                  edges: [
                    {
                      node: {
                        acceptanceDate: "",
                        acceptanceStatus: "",
                        assigned: "",
                        date: "2019-01-16 10:36:04",
                        justification: "",
                        treatment: "UNTREATED",
                        user: "",
                      },
                    },
                    {
                      node: {
                        acceptanceDate: "",
                        acceptanceStatus: "",
                        assigned: "",
                        date: "2019-01-17 10:06:04",
                        justification: "",
                        treatment: "UNTREATED",
                        user: "",
                      },
                    },
                    {
                      node: {
                        acceptanceDate: "",
                        acceptanceStatus: "SUBMITTED",
                        assigned: "usermanager1@test.test",
                        date: "2020-02-17 18:36:24",
                        justification: "Some of the resources",
                        treatment: "ACCEPTED_UNDEFINED",
                        user: "usertreatment1@test.test",
                      },
                    },
                    {
                      node: {
                        acceptanceDate: "",
                        acceptanceStatus: "REJECTED",
                        assigned: "usermanager2@test.test",
                        date: "2020-02-18 18:36:24",
                        justification: rejectedObservation,
                        treatment: "ACCEPTED_UNDEFINED",
                        user: "usertreatment2@test.test",
                      },
                    },
                    {
                      node: {
                        acceptanceDate: "",
                        acceptanceStatus: "",
                        assigned: "",
                        date: "2020-02-18 18:36:25",
                        justification: "",
                        treatment: "UNTREATED",
                        user: "",
                      },
                    },
                  ],
                  pageInfo: {
                    endCursor: "",
                    hasNextPage: false,
                  },
                },
                id: "af7a48b8-d8fc-41da-9282-d424fff563f0",
              },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error getting vulnerability treatment")],
        });
      },
    );

  it("should render in treatment tracking 1 external user", async (): Promise<void> => {
    expect.hasAssertions();

    const { container } = render(<TreatmentTracking vuln={mock} />, {
      memoryRouter,
      mocks: [mockQueryVulnTreatment1],
    });

    await waitFor((): void => {
      expect(
        screen.getByText("searchFindings.tabDescription.treatment.inProgress"),
      ).toBeInTheDocument();
    });

    expect(container.getElementsByClassName("timeline-card")).toHaveLength(4);
  });

  it("should render in treatment tracking 1 internal user", async (): Promise<void> => {
    expect.hasAssertions();

    const { container } = render(
      <authContext.Provider
        value={{
          awsSubscription: null,
          tours: {
            newGroup: true,
            newRoot: true,
            welcome: true,
          },
          userEmail: "test@fluidattacks.com",
          userName: "test",
        }}
      >
        <TreatmentTracking vuln={mock} />
      </authContext.Provider>,
      {
        memoryRouter,
        mocks: [mockQueryVulnTreatment1],
      },
    );

    await waitFor((): void => {
      expect(
        screen.getByText("searchFindings.tabDescription.treatment.inProgress"),
      ).toBeInTheDocument();
    });

    expect(container.getElementsByClassName("timeline-card")).toHaveLength(5);
  });

  it("should render in treatment tracking 2 external user", async (): Promise<void> => {
    expect.hasAssertions();

    const { container } = render(<TreatmentTracking vuln={mock} />, {
      memoryRouter,
      mocks: [mockQueryVulnTreatment2],
    });

    await waitFor((): void => {
      expect(
        screen.queryAllByText("searchFindings.tabDescription.treatment.new"),
      ).toHaveLength(2);
    });

    expect(
      screen.getByText(formatDropdownField("ACCEPTED_UNDEFINED")),
    ).toBeInTheDocument();
    expect(
      screen.getByText(
        "searchFindings.tabDescription.treatment.pendingApproval",
      ),
    ).toBeInTheDocument();

    expect(
      screen.getByText(`searchFindings.tabTracking.justification`),
    ).toBeInTheDocument();

    expect(container.getElementsByClassName("timeline-card")).toHaveLength(3);
  });

  it("should render in treatment tracking 2 internal user", async (): Promise<void> => {
    expect.hasAssertions();

    const { container } = render(
      <authContext.Provider
        value={{
          awsSubscription: null,
          tours: {
            newGroup: true,
            newRoot: true,
            welcome: true,
          },
          userEmail: "test@fluidattacks.com",
          userName: "test",
        }}
      >
        <TreatmentTracking vuln={mock} />
      </authContext.Provider>,
      {
        memoryRouter,
        mocks: [mockQueryVulnTreatment2],
      },
    );

    await waitFor((): void => {
      expect(
        screen.queryAllByText("searchFindings.tabDescription.treatment.new"),
      ).toHaveLength(3);
    });

    expect(
      screen.getByText(formatDropdownField("ACCEPTED_UNDEFINED")),
    ).toBeInTheDocument();
    expect(
      screen.getByText(
        "searchFindings.tabDescription.treatment.pendingApproval",
      ),
    ).toBeInTheDocument();

    expect(
      screen.getByText(`searchFindings.tabTracking.justification`),
    ).toBeInTheDocument();

    expect(container.getElementsByClassName("timeline-card")).toHaveLength(4);
  });

  it("should render in treatment tracking when remediated", async (): Promise<void> => {
    expect.hasAssertions();

    const mockWithClosingDate = {
      ...mock,
      closingDate: "2019-07-05 09:56:40",
      lastEditedBy: "machine@fluidattacks.com",
      state: vulnerabilityStates.SAFE,
    };

    render(<TreatmentTracking vuln={mockWithClosingDate} />, {
      memoryRouter,
      mocks: [mockQueryVulnTreatment1],
    });

    await waitFor((): void => {
      expect(
        screen.getByText("searchFindings.tabDescription.treatment.closed"),
      ).toBeInTheDocument();
    });

    expect(
      screen.getAllByText("searchFindings.tabTracking.justification"),
    ).not.toHaveLength(0);
  });

  it("should render in treatment tracking when remediated by machine", async (): Promise<void> => {
    expect.hasAssertions();

    const mockWithClosingDate = {
      ...mock,
      closingDate: "2019-07-05 09:56:40",
      state: vulnerabilityStates.SAFE,
      verification: "Verified",
      verificationJustification: "Solved by updating the package",
    };

    render(<TreatmentTracking vuln={mockWithClosingDate} />, {
      memoryRouter,
      mocks: [mockQueryVulnTreatment1],
    });

    await waitFor((): void => {
      expect(
        screen.getByText("searchFindings.tabDescription.treatment.closed"),
      ).toBeInTheDocument();
    });

    expect(screen.getByText("2019-07-05 09:56 AM")).toBeInTheDocument();
    expect(
      screen.getAllByText("searchFindings.tabTracking.justification"),
    ).not.toHaveLength(0);
  });

  it("should render in treatment tracking when remediated by rebase", async (): Promise<void> => {
    expect.hasAssertions();

    const mockWithClosingDate = {
      ...mock,
      closingDate: "2019-07-05 09:56:40",
      lastEditedBy: "rebase@fluidattacks.com",
      state: vulnerabilityStates.SAFE,
    };

    render(<TreatmentTracking vuln={mockWithClosingDate} />, {
      memoryRouter,
      mocks: [mockQueryVulnTreatment1],
    });

    await waitFor((): void => {
      expect(
        screen.getByText("searchFindings.tabDescription.treatment.closed"),
      ).toBeInTheDocument();
    });

    expect(
      screen.getAllByText("searchFindings.tabTracking.justification"),
    ).not.toHaveLength(0);
  });

  it("should render in treatment tracking when Excluded", async (): Promise<void> => {
    expect.hasAssertions();

    const mockWithClosingDate = {
      ...mock,
      closingDate: "2019-07-05 09:56:40",
      reasons: [VulnerabilityRejectionReason.Exclusion],
      state: vulnerabilityStates.SAFE,
      verification: null,
    };

    render(<TreatmentTracking vuln={mockWithClosingDate} />, {
      memoryRouter,
      mocks: [mockQueryVulnTreatment1],
    });

    await waitFor((): void => {
      expect(
        screen.getByText("searchFindings.tabDescription.treatment.closed"),
      ).toBeInTheDocument();
    });

    expect(
      screen.getAllByText("searchFindings.tabTracking.justification"),
    ).not.toHaveLength(0);
  });

  it("should render in treatment tracking no justification", async (): Promise<void> => {
    expect.hasAssertions();

    const mockWithClosingDate = {
      ...mock,
      closingDate: "2019-07-05 09:56:40",
      state: vulnerabilityStates.SAFE,
      verification: null,
    };

    render(<TreatmentTracking vuln={mockWithClosingDate} />, {
      memoryRouter,
      mocks: [mockQueryVulnTreatment1],
    });

    await waitFor((): void => {
      expect(
        screen.getByText("searchFindings.tabDescription.treatment.closed"),
      ).toBeInTheDocument();
    });
  });
});
