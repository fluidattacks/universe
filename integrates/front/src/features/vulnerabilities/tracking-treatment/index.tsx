import { useQuery } from "@apollo/client";
import { Container, TimeLine } from "@fluidattacks/design";
import type { ITimelineCardProps } from "@fluidattacks/design/dist/components/timeline/types";
import isEmpty from "lodash/isEmpty";
import { useContext, useEffect, useMemo } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { GET_VULN_TREATMENT } from "./queries";
import type { ITreatmentTrackingAttr } from "./types";
import {
  getClosedTreatment,
  isFirstTreatmentOrNextDifferent,
  isLastTreatmentOrNextDifferent,
} from "./utils";

import { authContext } from "context/auth";
import { useAudit } from "hooks/use-audit";
import type { IHistoricTreatment } from "pages/finding/description/types";
import { formatDateTime } from "utils/date";
import { formatDropdownField } from "utils/format-helpers";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";

export const TreatmentTracking: React.FC<ITreatmentTrackingAttr> = ({
  vuln,
}: ITreatmentTrackingAttr): JSX.Element => {
  const { t } = useTranslation();
  const { userEmail } = useContext(authContext);
  const { addAuditEvent } = useAudit();
  const { data, fetchMore } = useQuery(GET_VULN_TREATMENT, {
    fetchPolicy: "network-only",
    nextFetchPolicy: "cache-first",
    onCompleted: (): void => {
      addAuditEvent("Vulnerability.Tracking", vuln.id);
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.warning(
          "An error occurred loading the vulnerability historic treatment",
          error,
        );
      });
    },
    variables: {
      vulnId: vuln.id,
    },
  });

  const {
    pageInfo: treatmentPageInfo = {
      endCursor: "",
      hasNextPage: false,
    },
    edges: treatmentEdges = [],
  } = data === undefined ? {} : data.vulnerability.historicTreatmentConnection;

  const getRequestedDate = (
    treatment: IHistoricTreatment,
    index: number,
    array: IHistoricTreatment[],
  ): string => {
    if (
      index > 0 &&
      (array[index - 1].treatment === "ACCEPTED_UNDEFINED" ||
        array[index - 1].treatment === "ACCEPTED") &&
      array[index - 1].acceptanceStatus === "SUBMITTED"
    ) {
      return array[index - 1].date;
    }

    return treatment.date;
  };
  const reversedHistoricTreatment = useMemo(
    (): IHistoricTreatment[] => [
      ...getClosedTreatment(vuln),
      ...treatmentEdges
        .map((edge): IHistoricTreatment => edge.node as IHistoricTreatment)
        .reduce(
          (
            currentValue: IHistoricTreatment[],
            treatment: IHistoricTreatment,
            index: number,
            array: IHistoricTreatment[],
          ): IHistoricTreatment[] => {
            const isAcceptedUndefined =
              treatment.treatment === "ACCEPTED_UNDEFINED";
            const isAccepted = treatment.treatment === "ACCEPTED";

            if (
              isFirstTreatmentOrNextDifferent(treatment, array, index) &&
              !isAcceptedUndefined
            ) {
              return [...currentValue, treatment];
            }
            if (
              (isAcceptedUndefined || isAccepted) &&
              treatment.acceptanceStatus === "APPROVED"
            ) {
              return [
                ...currentValue,
                {
                  ...treatment,
                  acceptanceDate: getRequestedDate(treatment, index, array),
                },
              ];
            }
            if (
              (isAcceptedUndefined &&
                treatment.acceptanceStatus === "REJECTED") ||
              isLastTreatmentOrNextDifferent(treatment, array, index)
            ) {
              return [...currentValue, treatment];
            }

            return currentValue;
          },
          [],
        )
        .reduce(
          (
            previousValue: IHistoricTreatment[],
            current: IHistoricTreatment,
          ): IHistoricTreatment[] => [current, ...previousValue],
          [],
        ),
    ],
    [treatmentEdges, vuln],
  );

  useEffect((): void => {
    if (treatmentPageInfo.hasNextPage) {
      void fetchMore({
        variables: {
          after: treatmentPageInfo.endCursor,
        },
      });
    }
  }, [fetchMore, treatmentPageInfo]);

  const timelineItems: ITimelineCardProps[] = reversedHistoricTreatment
    .map((historicTreatment): ITimelineCardProps | null => {
      const {
        acceptanceDate,
        acceptanceStatus,
        assigned,
        date,
        justification,
        treatment,
        user,
      } = historicTreatment;
      const isAcceptedUndefined = treatment === "ACCEPTED_UNDEFINED";
      const isAccepted = treatment === "ACCEPTED";

      const pendingApproval =
        (isAcceptedUndefined || isAccepted) && acceptanceStatus !== "APPROVED";

      const approved =
        (isAcceptedUndefined || isAccepted) && acceptanceStatus === "APPROVED";

      const assignedUser = isEmpty(assigned) ? user : assigned;

      if (
        historicTreatment ===
          reversedHistoricTreatment[reversedHistoricTreatment.length - 1] &&
        !userEmail.endsWith("@fluidattacks.com")
      ) {
        return null;
      }

      return {
        date: formatDateTime(date),
        description: [
          t(formatDropdownField(treatment)),
          pendingApproval
            ? t("searchFindings.tabDescription.treatment.pendingApproval")
            : "",
          assignedUser === undefined ||
          treatment === "UNTREATED" ||
          treatment === "CLOSED"
            ? ""
            : t("searchFindings.tabTracking.assigned", {
                assigned: assignedUser,
              }),
          isEmpty(justification)
            ? ""
            : t("searchFindings.tabTracking.justification", {
                justification,
              }),
          approved
            ? (t("searchFindings.tabVuln.contentTab.tracking.requestDate"),
              formatDateTime(acceptanceDate ?? ""),
              t("searchFindings.tabVuln.contentTab.tracking.requestApproval"),
              user)
            : "",
        ],
      };
    })
    .filter((item): item is ITimelineCardProps => item !== null);

  return (
    <Container width={"100%"}>
      <TimeLine items={timelineItems} />
    </Container>
  );
};
