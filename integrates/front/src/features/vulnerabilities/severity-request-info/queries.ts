import { graphql } from "gql";

const GET_VULN_SEVERITY_INFO_AND_PROPOSALS = graphql(`
  query GetVulnSeverityInfoAndProposals($vulnId: String!) {
    vulnerability(uuid: $vulnId) {
      id
      hacker
      proposedSeverityAuthor

      proposedSeverityTemporalScore
      proposedSeverityThreatScore

      proposedSeverityVector
      proposedSeverityVectorV4

      severityTemporalScore
      severityThreatScore

      severityVector
      severityVectorV4
      state
    }
  }
`);

export { GET_VULN_SEVERITY_INFO_AND_PROPOSALS };
