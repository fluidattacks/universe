import { Col, Container, Link, Row } from "@fluidattacks/design";
import * as React from "react";
import { useTranslation } from "react-i18next";

import type { ISeverityDiffProps } from "./types";

import { Detail } from "../../additional-info/detail";
import { HighlightDiff } from "../highlight-diff";
import { Label } from "components/input";
import { CVSS3_CALCULATOR_BASE_URL } from "utils/cvss";
import { CVSS4_CALCULATOR_BASE_URL } from "utils/cvss4";

export const SeverityDiff: React.FC<ISeverityDiffProps> = ({
  currentSeverityVector,
  cvssVersion,
  proposedSeverityVector,
}: ISeverityDiffProps): JSX.Element => {
  const { t } = useTranslation();
  const prefix = "searchFindings.tabVuln.severityRequestInfo.";

  const baseUrl =
    cvssVersion === "CVSS:3.1"
      ? CVSS3_CALCULATOR_BASE_URL
      : CVSS4_CALCULATOR_BASE_URL;

  return (
    <React.StrictMode>
      <Row>
        <Col>
          <Detail
            field={
              <Container>
                <Container>
                  <Label>
                    <b>{t(`${prefix}current`)}</b>
                  </Label>
                </Container>
                <Link
                  href={`${baseUrl}#${currentSeverityVector}`}
                  iconPosition={"hidden"}
                >
                  {currentSeverityVector}
                </Link>

                <Container>
                  <Label>
                    <b>{t(`${prefix}proposal`)}</b>
                  </Label>
                </Container>

                <Link
                  href={`${baseUrl}#${proposedSeverityVector}`}
                  iconPosition={"hidden"}
                >
                  {proposedSeverityVector}
                </Link>

                <Container>
                  <Label>
                    <b>{t(`${prefix}diff`)}</b>
                  </Label>
                </Container>

                <Link
                  href={`${baseUrl}#${proposedSeverityVector}`}
                  iconPosition={"hidden"}
                >
                  <HighlightDiff
                    newString={proposedSeverityVector}
                    oldString={currentSeverityVector}
                  />
                </Link>
              </Container>
            }
            isEditing={false}
            label={<b>{t(`${prefix}severityVectorTitleV4`)}</b>}
          />
        </Col>
      </Row>
    </React.StrictMode>
  );
};
