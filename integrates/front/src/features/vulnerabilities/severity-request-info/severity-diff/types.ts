interface ISeverityDiffProps {
  cvssVersion: "CVSS:3.1" | "CVSS:4.0";
  proposedSeverityVector: string;
  currentSeverityVector: string;
}
export type { ISeverityDiffProps };
