interface IHighlightDiffProps {
  oldString: string;
  newString: string;
}

export type { IHighlightDiffProps };
