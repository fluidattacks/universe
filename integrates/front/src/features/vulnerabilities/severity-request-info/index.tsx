import { useQuery } from "@apollo/client";
import { Col, Row } from "@fluidattacks/design";
import _ from "lodash";
import { Fragment } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { GET_VULN_SEVERITY_INFO_AND_PROPOSALS } from "./queries";
import { SeverityDiff } from "./severity-diff";
import type { ISeverityRequestInfoProps } from "./types";

import { Detail } from "../additional-info/detail";
import { Value } from "../additional-info/value";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";

export const SeverityRequestInfo: React.FC<ISeverityRequestInfoProps> = ({
  vulnerabilityId,
}: ISeverityRequestInfoProps): JSX.Element => {
  const { t } = useTranslation();
  const prefix = "searchFindings.tabVuln.severityRequestInfo.";

  const { data } = useQuery(GET_VULN_SEVERITY_INFO_AND_PROPOSALS, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.warning("An error occurred loading the severity info", error);
      });
    },
    variables: {
      vulnId: vulnerabilityId,
    },
  });

  if (_.isUndefined(data) || _.isEmpty(data)) {
    return <div />;
  }
  if (_.isNil(data.vulnerability) || _.isEmpty(data.vulnerability)) {
    return <div />;
  }

  return (
    <React.StrictMode>
      <Row>
        <Fragment>
          <SeverityDiff
            currentSeverityVector={data.vulnerability.severityVectorV4}
            cvssVersion={"CVSS:4.0"}
            proposedSeverityVector={
              data.vulnerability.proposedSeverityVectorV4 ?? ""
            }
          />
          <SeverityDiff
            currentSeverityVector={data.vulnerability.severityVector}
            cvssVersion={"CVSS:3.1"}
            proposedSeverityVector={
              data.vulnerability.proposedSeverityVector ?? ""
            }
          />
          <Row>
            <Col>
              <Detail
                field={<Value value={data.vulnerability.hacker} />}
                isEditing={false}
                label={<b>{t(`${prefix}hacker`)}</b>}
              />
            </Col>
            <Col>
              <Detail
                field={
                  <Value value={data.vulnerability.proposedSeverityAuthor} />
                }
                isEditing={false}
                label={<b>{t(`${prefix}proposedBy`)}</b>}
              />
            </Col>
          </Row>
        </Fragment>
      </Row>
      <br />
    </React.StrictMode>
  );
};
