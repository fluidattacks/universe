import { graphql } from "gql";

const UPLOAD_VULNERABILITIES = graphql(`
  mutation UploadVulnerabilities($file: Upload!, $findingId: String!) {
    uploadFile(file: $file, findingId: $findingId) {
      message
      success
    }
  }
`);

const GET_GROUP_USERS = graphql(`
  query GetGroupUsers($groupName: String!) {
    group(groupName: $groupName) {
      name
      stakeholders {
        email
        invitationState
        role
      }
    }
  }
`);

const DOWNLOAD_VULNERABILITIES = graphql(`
  mutation downloadVulnerabilityFile($findingId: String!) {
    downloadVulnerabilityFile(findingId: $findingId) {
      success
      url
    }
  }
`);

export { DOWNLOAD_VULNERABILITIES, GET_GROUP_USERS, UPLOAD_VULNERABILITIES };
