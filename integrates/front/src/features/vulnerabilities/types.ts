import type {
  ColumnDef,
  ColumnOrderState,
  ColumnPinningState,
  RowData,
  VisibilityState,
} from "@tanstack/react-table";

import type { IOrganizationGroups } from "@types";
import type { ICsvConfig, ITableOptions } from "components/table/types";
import type { RootCriticality, VulnerabilityStateReason } from "gql/graphql";
import type {
  IHistoricTreatment,
  IVulnerabilityCriteriaData,
  IVulnerabilityCriteriaRequirement,
} from "pages/finding/description/types";

enum vulnerabilityStates {
  REJECTED = "REJECTED",
  SAFE = "SAFE",
  SUBMITTED = "SUBMITTED",
  VULNERABLE = "VULNERABLE",
}

type TVulnerabilityStatesStrings = keyof typeof vulnerabilityStates;

interface IFinding {
  id: string;
  title: string;
}
interface IFormatVulns {
  requirementsData?: Record<string, IVulnerabilityCriteriaRequirement>;
  vulnerabilities: IVulnRowAttr[];
  vulnsData?: Record<string, IVulnerabilityCriteriaData>;
}
interface IFormatVulnsTreatment {
  organizationsGroups?: IOrganizationGroups[];
  vulnerabilities: IVulnRowAttr[];
}

interface IGetVulnById {
  vulnerabilities: IVulnRowAttr[];
  vulnerabilityId: string;
}

interface IReqFormat {
  findingTitle: string | undefined;
  vulnsData?: Record<string, IVulnerabilityCriteriaData>;
  requirementsData?: Record<string, IVulnerabilityCriteriaRequirement>;
}

interface IVulnerabilityAdvisories {
  cve: string[];
  epss: number | undefined;
  package: string;
  vulnerableVersion: string;
}

interface IGitRoot {
  __typename: "GitRoot";
  branch: string | null;
  criticality: RootCriticality | null;
}

interface IVulnRowAttr {
  advisories: IVulnerabilityAdvisories | null;
  acceptance?: "" | "APPROVED" | "REJECTED";
  acceptanceStatus?: "APPROVED" | "REJECTED";
  closingDate: string | null;
  externalBugTrackingSystem: string | null;
  findingId: string;
  groupName: string;
  hacker?: string;
  organizationName: string | undefined;
  historicTreatment: IHistoricTreatment[];
  id: string;
  lastEditedBy: string;
  lastStateDate: string;
  lastTreatmentDate: string;
  lastVerificationDate: string | null;
  priority: number;
  remediated: boolean;
  reportDate: string | null;
  root: IGitRoot | null;
  rootNickname: string | null;
  customSeverity: number | null;
  proposedSeverityAuthor: string | null;
  proposedSeverityTemporalScore: number | null;
  proposedSeverityThreatScore: number | null;
  proposedSeverityVector: string | null;
  proposedSeverityVectorV4: string | null;
  severityAcceptance?: "" | "APPROVED" | "REJECTED";
  severityTemporalScore: number;
  severityThreatScore: number;
  severityVector: string;
  severityVectorV4: string;
  source: string;
  sourceType?: string;
  specific: string;
  state: TVulnerabilityStatesStrings;
  stateReasons: VulnerabilityStateReason[] | null;
  stream: string | null;
  tag: string;
  technique: string;
  treatmentDate: string;
  treatmentAcceptanceDate: string | null;
  treatmentAcceptanceStatus: string | null;
  treatmentAssigned: string | null;
  treatmentJustification: string | null;
  treatmentStatus: string;
  treatmentUser: string | null;
  assigned: string;
  verification: string | null;
  verificationJustification: string | null;
  vulnerabilityType: string;
  where: string;
  zeroRisk: string | null;
  finding?: IFinding;
  requirements?: string[];
}

interface IVulnDataTypeAttr {
  externalBugTrackingSystem: string | null;
  findingId: string;
  groupName: string;
  historicTreatment: IHistoricTreatment[];
  id: string;
  customSeverity?: number | null;
  source: string;
  specific: string;
  state: TVulnerabilityStatesStrings;
  tag: string;
  assigned: string;
  where: string;
  severityTemporalScore?: number;
  severityThreatScore?: number;
}

interface ITableRefProps {
  id: string;
  columnOrder?: ColumnOrderState;
  columnPinning?: ColumnPinningState;
  columnVisibility?: VisibilityState;
}

interface IVulnStateProps {
  findingState?: "SAFE" | "VULNERABLE";
  isFindingReleased?: boolean;
}

interface IVulnComponentProps<TData extends RowData> {
  clearFiltersButton?: () => void;
  columns: ColumnDef<IVulnRowAttr>[];
  csvConfig?: ICsvConfig;
  csvColumns?: string[];
  csvHeaders?: Record<string, string>;
  cvssVersion?: string;
  exportCsv?: boolean;
  extraButtons?: JSX.Element;
  extraBelowButtons?: JSX.Element;
  featureActive?: boolean;
  filters?: JSX.Element;
  shouldHideRelease?: boolean;
  nzrFetchMoreData?: () => void;
  refetchData: () => void;
  tableOptions: ITableOptions<TData>;
  tableRefProps: ITableRefProps;
  nonValidOnReattackVulns?: IVulnRowAttr[];
  vulnerabilities: IVulnRowAttr[];
  vulnStateProps?: IVulnStateProps;
  onNextPage?: () => Promise<void>;
  onSearch?: (value: string) => void;
  onVulnSelect?: (
    vulnerabilities: IVulnRowAttr[],
    clearSelected: () => void,
  ) => void;
  loading?: boolean;
}

interface IUpdateForm {
  acceptanceDate?: string;
  justification?: string;
  treatment: string;
  assigned?: string;
}

interface IUpdateVulnerabilityForm extends IUpdateForm {
  customSeverity?: number | null;
  externalBugTrackingSystem: string | null;
  source?: string;
  tag?: string;
}

interface IVulnerabilityModalValues
  extends Array<
    | IUpdateVulnerabilityForm
    | React.Dispatch<React.SetStateAction<IUpdateVulnerabilityForm>>
  > {
  0: IUpdateVulnerabilityForm;
  1: React.Dispatch<React.SetStateAction<IUpdateVulnerabilityForm>>;
}

export type {
  IFinding,
  IFormatVulns,
  IFormatVulnsTreatment,
  IGetVulnById,
  IGitRoot,
  IReqFormat,
  IVulnRowAttr,
  IVulnerabilityModalValues,
  IUpdateVulnerabilityForm,
  IVulnDataTypeAttr,
  IVulnComponentProps,
  TVulnerabilityStatesStrings,
  ICsvConfig,
  ITableOptions,
  IVulnStateProps,
};

export { vulnerabilityStates };
