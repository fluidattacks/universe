import type { IVulnRowAttr } from "features/vulnerabilities/types";

interface IConfirmVulnerabilityCheckBoxProps {
  approveFunction: (vulnerabilityRow: IVulnRowAttr) => void;
  rejectFunction: (vulnerabilityRow: IVulnRowAttr) => void;
  vulnerabilityRow: IVulnRowAttr;
}

export type { IConfirmVulnerabilityCheckBoxProps };
