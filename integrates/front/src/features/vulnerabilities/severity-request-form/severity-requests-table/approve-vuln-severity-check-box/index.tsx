import { Container, TableButton } from "@fluidattacks/design";
import { useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import type { IConfirmVulnerabilityCheckBoxProps } from "./types";

const ApproveSeverityCheckBox: React.FC<IConfirmVulnerabilityCheckBoxProps> = (
  props: IConfirmVulnerabilityCheckBoxProps,
): JSX.Element => {
  const { approveFunction, rejectFunction, vulnerabilityRow } = props;

  const { t } = useTranslation();

  const handleOnApprove = useCallback((): void => {
    approveFunction(vulnerabilityRow);
  }, [approveFunction, vulnerabilityRow]);

  const handleOnReject = useCallback((): void => {
    rejectFunction(vulnerabilityRow);
  }, [rejectFunction, vulnerabilityRow]);

  return (
    <React.StrictMode>
      <Container display={"flex"} gap={0.25}>
        <TableButton
          id={"approve-severity-button"}
          name={t("severityUpdateRequests.handleRequestApproval.approve")}
          onClick={handleOnApprove}
          variant={"success"}
        />
        <TableButton
          id={"reject-severity-button"}
          name={t("severityUpdateRequests.handleRequestApproval.reject")}
          onClick={handleOnReject}
          variant={"reject"}
        />
      </Container>
    </React.StrictMode>
  );
};

export { ApproveSeverityCheckBox };
