import type { IVulnRowAttr } from "../types";
import type { IUseModal } from "hooks/use-modal";

interface IHandleSeverityRequestFormProps {
  findingId: string;
  modalRef: IUseModal;
  vulnerabilities: IVulnRowAttr[];
  clearSelected: () => void;
  onCancel: () => void;
  refetchData: () => void;
}

interface IFormValues {
  justification: string;
}

export type { IHandleSeverityRequestFormProps, IFormValues };
