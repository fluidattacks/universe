import { Button, ButtonToolbarRow } from "@fluidattacks/design";
import * as React from "react";
import { useTranslation } from "react-i18next";

import type { IActionButtonsProps } from "../types";

const ActionButtons: React.FC<IActionButtonsProps> = ({
  isEditing,
  isPristine,
  onEdit,
  onUpdate,
}: IActionButtonsProps): JSX.Element => {
  const { t } = useTranslation();

  return (
    <ButtonToolbarRow>
      {isEditing ? (
        <Button disabled={isPristine} icon={"rotate-right"} onClick={onUpdate}>
          {t("searchFindings.tabVuln.additionalInfo.buttons.save")}
        </Button>
      ) : undefined}
      <Button
        icon={isEditing ? "times" : "pen"}
        onClick={onEdit}
        variant={"secondary"}
      >
        {isEditing
          ? t("searchFindings.tabVuln.additionalInfo.buttons.cancel")
          : t("searchFindings.tabVuln.additionalInfo.buttons.edit")}
      </Button>
    </ButtonToolbarRow>
  );
};

export type { IActionButtonsProps };
export { ActionButtons };
