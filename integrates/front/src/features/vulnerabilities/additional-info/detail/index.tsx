/* eslint-disable react/jsx-props-no-spreading */
import { Span } from "@fluidattacks/design";
import isUndefined from "lodash/isUndefined";
import * as React from "react";

import type { IDetailProps } from "./types";

export const Detail: React.FC<IDetailProps> = ({
  dataPrivate = true,
  isEditing = false,
  editableField,
  label,
  field,
  color,
}): JSX.Element => {
  return (
    <div
      className={"justify-start items-end ma0 pv1"}
      {...(dataPrivate && { "data-private": true })}
    >
      {isUndefined(label) ? undefined : (
        <div style={{ minWidth: "85px" }}>
          <Span color={color} content={": "} size={"sm"}>
            {label}
          </Span>
        </div>
      )}
      <div>{isEditing ? editableField : field}</div>
    </div>
  );
};
