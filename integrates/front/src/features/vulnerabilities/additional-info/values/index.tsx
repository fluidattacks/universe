import { Text } from "@fluidattacks/design";
import _ from "lodash";
import { StrictMode } from "react";
import { useTranslation } from "react-i18next";

import { Value } from "../value";

export const Values = ({
  values,
}: Readonly<{
  values: (number | string)[] | null | undefined;
}>): JSX.Element => {
  const { t } = useTranslation();
  if (_.isEmpty(values) || _.isNil(values)) {
    return (
      <StrictMode>
        <Text size={"sm"} wordWrap={"normal"}>
          {t("searchFindings.tabVuln.notApplicable")}
        </Text>
      </StrictMode>
    );
  }

  return (
    <StrictMode>
      <ul>
        {values.map((value, index): JSX.Element => {
          const key = `value-${index}`;

          return (
            <li key={key}>
              <Value value={value} />
            </li>
          );
        })}
      </ul>
    </StrictMode>
  );
};
