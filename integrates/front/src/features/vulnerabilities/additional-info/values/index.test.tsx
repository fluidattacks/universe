import { screen } from "@testing-library/react";

import { Values } from ".";
import { render } from "mocks";

describe("values", (): void => {
  it("should render in values", (): void => {
    expect.hasAssertions();

    const values = [1, "test", 3];
    render(<Values values={values} />);

    expect(screen.getByText("1")).toBeInTheDocument();
    expect(screen.getByText("test")).toBeInTheDocument();
    expect(screen.getByText("3")).toBeInTheDocument();
  });

  it("should display notApplicable when values is null", (): void => {
    expect.hasAssertions();

    const values = null;
    render(<Values values={values} />);

    expect(
      screen.getByText("searchFindings.tabVuln.notApplicable"),
    ).toBeInTheDocument();
  });
});
