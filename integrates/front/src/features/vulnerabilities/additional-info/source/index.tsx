import { useMemo } from "react";
import { useTranslation } from "react-i18next";

import { Detail } from "../detail";
import { Value } from "../value";
import { Select } from "components/input";

const Source = ({
  canSeeSource,
  isEditing,
  vulnerabilitySource,
}: Readonly<{
  canSeeSource: boolean;
  isEditing: boolean;
  vulnerabilitySource: string | undefined;
}>): JSX.Element | undefined => {
  const { t } = useTranslation();
  const prefix = "searchFindings.tabVuln.vulnTable.vulnerabilitySource.";

  const source = useMemo((): string => {
    if (vulnerabilitySource === undefined) {
      return t(`${prefix}ASM`);
    }

    return t(
      `searchFindings.tabVuln.vulnTable.vulnerabilitySource.${vulnerabilitySource.toUpperCase()}`,
    );
  }, [t, vulnerabilitySource]);

  if (!canSeeSource) return undefined;

  const editableField = (
    <Select
      items={[
        { header: "", value: "" },
        { header: t(`${prefix}ANALYST`), value: "ANALYST" },
        { header: t(`${prefix}CUSTOMER`), value: "CUSTOMER" },
        { header: t(`${prefix}DETERMINISTIC`), value: "DETERMINISTIC" },
        { header: t(`${prefix}ESCAPE`), value: "ESCAPE" },
        { header: t(`${prefix}MACHINE`), value: "MACHINE" },
      ]}
      name={"source"}
    />
  );

  return (
    <Detail
      editableField={editableField}
      field={<Value value={source} />}
      isEditing={isEditing}
      label={t("searchFindings.tabVuln.vulnTable.source")}
    />
  );
};

export { Source };
