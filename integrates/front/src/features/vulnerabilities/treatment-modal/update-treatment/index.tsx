import { useAbility } from "@casl/react";
import type { IItem } from "@fluidattacks/design";
import { Alert, Form, InnerForm, useConfirmDialog } from "@fluidattacks/design";
import _ from "lodash";
import { useCallback, useContext, useEffect, useMemo, useState } from "react";
import { useTranslation } from "react-i18next";

import { TreatmentContent } from "./content";
import { useGetGroupUsersQuery } from "./hooks/get-group-users";
import { useGetPoliciesQuery } from "./hooks/get-policies";
import { useFindingTitleAndPolicies } from "./hooks/use-finding-policies";
import { useTreatmentMutations } from "./hooks/use-treatment-mutations";
import type { ITreatmentForm } from "./types";
import { getTreatmentState, useDefaultValues } from "./utils";
import {
  groupExternalBugTrackingSystem,
  groupLastHistoricTreatment,
  handleSubmitHelper,
  sortTags,
} from "./utils";
import { validationSchema } from "./validations";

import type { IUpdateVulnerabilityForm } from "../../types";
import {
  authzGroupContext,
  authzPermissionsContext,
} from "context/authz/config";
import {
  OrganizationFindingPolicyStatus,
  TreatmentAcceptancePolicy,
  VulnerabilitySource,
} from "gql/graphql";
import { useAudit } from "hooks/use-audit";
import type { IHistoricTreatment } from "pages/finding/description/types";
import { formatDate } from "utils/date";

const TreatmentForm = ({
  close,
  findingId,
  groupName,
  handleClearSelected,
  refetchData,
  vulnerabilities,
  onActionCompleted,
}: ITreatmentForm): JSX.Element => {
  const { t } = useTranslation();
  const [dialogMessage, setDialogMessage] = useState<string | null>(null);
  const [updateTreatment, setUpdateTreatment] = useState(false);
  const [treatmentState, setTreatmentState] = useState(false);
  const [isRunning, setIsRunning] = useState(false);
  const { confirm, ConfirmDialog } = useConfirmDialog();

  const { addAuditEvent } = useAudit();
  useEffect((): void => {
    vulnerabilities.forEach((vuln): void => {
      addAuditEvent("Vulnerability.Treatments", vuln.id);
    });
  }, [addAuditEvent, vulnerabilities]);

  // Handle authorization
  const attributes = useContext(authzGroupContext);
  const permissions = useAbility(authzPermissionsContext);
  const canRequestZeroRiskVuln: boolean =
    permissions.can(
      "integrates_api_mutations_request_vulnerabilities_zero_risk_mutate",
    ) &&
    attributes.can("can_report_vulnerabilities") &&
    attributes.can("can_request_zero_risk");
  const canUpdateVulnsTreatment = permissions.can(
    "integrates_api_mutations_update_vulnerabilities_treatment_mutate",
  );
  const canSeeSource = permissions.can("see_vulnerability_source");
  const canUpdateVulnDescription = permissions.can(
    "integrates_api_mutations_update_vulnerability_description_mutate",
  );
  const canDeleteVulnsTags = permissions.can(
    "integrates_api_mutations_remove_vulnerability_tags_mutate",
  );

  // Retrieve treatment default values
  const lastTreatment = useMemo(
    (): IHistoricTreatment => ({
      ...groupLastHistoricTreatment(vulnerabilities),
    }),
    [vulnerabilities],
  );
  const vulnsTags = useMemo(
    (): string[][] =>
      vulnerabilities.map((vuln): string[] => sortTags(vuln.tag)),
    [vulnerabilities],
  );
  const vulnSource =
    vulnerabilities.length === 1 &&
    vulnerabilities[0].source.toUpperCase() !== "ASM"
      ? vulnerabilities[0].source.toUpperCase()
      : "";

  const treatmentOptions = useMemo(
    (): IItem[] => [
      {
        name: t("searchFindings.tabDescription.treatment.untreated"),
        value: "",
      },
      ...(canUpdateVulnsTreatment
        ? [
            {
              name: t("searchFindings.tabDescription.treatment.inProgress"),
              value: "IN_PROGRESS",
            },
            {
              name: t("searchFindings.tabDescription.treatment.accepted"),
              value: "ACCEPTED",
            },
            {
              name: t(
                "searchFindings.tabDescription.treatment.acceptedUndefined",
              ),
              value: "ACCEPTED_UNDEFINED",
            },
          ]
        : []),
      ...(canRequestZeroRiskVuln
        ? [
            {
              name: t(
                "searchFindings.tabDescription.treatment.requestZeroRisk",
              ),
              value: "REQUEST_ZERO_RISK",
            },
          ]
        : []),
    ],
    [canUpdateVulnsTreatment, canRequestZeroRiskVuln, t],
  );
  const sourceOptions = Object.values(VulnerabilitySource).map(
    (source: string): IItem => ({
      name: t(`searchFindings.tabVuln.vulnTable.vulnerabilitySource.${source}`),
      value: source,
    }),
  );

  const defaultValues = useDefaultValues(
    lastTreatment,
    Number(vulnerabilities[0].customSeverity ?? 0),
    groupExternalBugTrackingSystem(vulnerabilities) ?? "",
    vulnSource,
    _.join(_.intersection(...vulnsTags), ","),
  );

  // Graphql operations
  const { data: policies } = useGetPoliciesQuery({ groupName });
  const {
    data: { assignableUsers },
    loading: loadingUsers,
  } = useGetGroupUsersQuery({ groupName, numberOfGroups: 1 });
  const { findingTitle, policies: findingPolicies } =
    useFindingTitleAndPolicies(findingId ?? vulnerabilities[0].findingId);
  const {
    handleDeletion,
    handleUpdateVulnerability,
    loading,
    requestZeroRisk,
  } = useTreatmentMutations(
    close,
    handleClearSelected,
    refetchData,
    setIsRunning,
    vulnerabilities,
  );

  // Data management and form submission
  const activeFindingPolicy = findingPolicies?.find(
    (treatment): boolean =>
      [findingTitle, "All vulnerability types"].includes(treatment.name) &&
      treatment.status === OrganizationFindingPolicyStatus.Approved,
  );
  const isActiveFindingPolicy =
    activeFindingPolicy?.treatmentAcceptance ===
    TreatmentAcceptancePolicy.Accepted;

  const confirmApproval = useCallback(
    async (confirmAction: () => void): Promise<void> => {
      const confirmResult = await confirm({
        id: "manage-treatment-modal",
        message: dialogMessage,
        title: t("searchFindings.tabDescription.approvalTitle"),
      });
      if (onActionCompleted) {
        onActionCompleted();
      }
      if (confirmResult) {
        confirmAction();
      }
    },
    [confirm, dialogMessage, onActionCompleted, t],
  );

  const handleSubmit = useCallback(
    async (values: IUpdateVulnerabilityForm): Promise<void> => {
      const acceptanceDate = formatDate(values.acceptanceDate ?? "");
      const submitValues = {
        ...values,
        acceptanceDate: acceptanceDate === "-" ? "" : acceptanceDate,
      };
      const {
        isDescriptionPristine,
        isTreatmentAdditionalValuesPristine,
        isTreatmentPristine,
      } = getTreatmentState({
        additionalFields: [
          "externalBugTrackingSystem",
          "tag",
          "customSeverity",
        ],
        initialValues: defaultValues,
        values: submitValues,
      });

      const changedToRequestZeroRisk = values.treatment === "REQUEST_ZERO_RISK";
      const changedToUndefined =
        values.treatment === "ACCEPTED_UNDEFINED" &&
        lastTreatment.treatment !== "ACCEPTED_UNDEFINED";
      const changedToTemporary = values.treatment === "ACCEPTED";
      const isTemporaryOrUndefinedTreatment =
        changedToUndefined || changedToTemporary;

      await handleSubmitHelper({
        changedToRequestZeroRisk,
        confirm: confirmApproval,
        findingId: findingId ?? vulnerabilities[0].findingId,
        handleUpdateVulnerability,
        isDescriptionPristine,
        isTemporaryOrUndefinedTreatment,
        isTreatmentDescriptionPristine: isTreatmentAdditionalValuesPristine,
        isTreatmentPristine,
        requestZeroRisk,
        values: submitValues,
        vulnerabilities,
      });
    },
    [
      confirmApproval,
      defaultValues,
      findingId,
      handleUpdateVulnerability,
      lastTreatment,
      requestZeroRisk,
      vulnerabilities,
    ],
  );

  return (
    <Form
      alert={
        updateTreatment ? (
          <Alert variant={"warning"}>
            {t("searchFindings.tabVuln.alerts.treatmentChange")}
          </Alert>
        ) : undefined
      }
      cancelButton={{ onClick: close }}
      confirmButton={{ disabled: loading || isRunning }}
      defaultValues={defaultValues}
      id={"vulnerabilityTreatment"}
      onSubmit={handleSubmit}
      yupSchema={validationSchema(treatmentState, vulnSource)}
    >
      <InnerForm<IUpdateVulnerabilityForm>>
        {(methods): JSX.Element => (
          <TreatmentContent
            canDeleteVulnsTags={canDeleteVulnsTags}
            canRequestZeroRiskVuln={canRequestZeroRiskVuln}
            canUpdateVulnsTreatment={canUpdateVulnsTreatment}
            handleDeletion={handleDeletion}
            handleMessage={setDialogMessage}
            isActiveFindingPolicy={isActiveFindingPolicy}
            isPristine={treatmentState}
            items={treatmentOptions}
            lastTreatment={lastTreatment}
            loadingUsers={loadingUsers}
            maxDays={policies.maxDays}
            setIsPristine={setTreatmentState}
            setUpdateTreatment={setUpdateTreatment}
            showSource={canSeeSource ? canUpdateVulnDescription : false}
            sourceOptions={sourceOptions}
            userEmails={assignableUsers}
            {...methods}
          />
        )}
      </InnerForm>
      <ConfirmDialog />
    </Form>
  );
};

export { TreatmentForm };
