import { graphql } from "gql";
import type { IVulnerabilityPoliciesData } from "pages/organization/policies/acceptance/vulnerability-policies/types";

const UPDATE_VULNERABILITY_MUTATION = graphql(`
  mutation UpdateVulnerabilityMutation(
    $acceptanceDate: String
    $assigned: String
    $customSeverity: Int
    $externalBugTrackingSystem: String
    $findingId: String!
    $isVulnDescriptionChanged: Boolean!
    $isVulnTreatmentChanged: Boolean!
    $isVulnTreatmentDescriptionChanged: Boolean!
    $justification: String!
    $source: VulnerabilitySource
    $tag: String
    $treatment: UpdateClientDescriptionTreatment!
    $vulnerabilityId: ID!
  ) {
    updateVulnerabilitiesTreatment(
      acceptanceDate: $acceptanceDate
      assigned: $assigned
      findingId: $findingId
      justification: $justification
      treatment: $treatment
      vulnerabilityId: $vulnerabilityId
    ) @include(if: $isVulnTreatmentChanged) {
      success
    }
    updateVulnerabilityDescription(
      source: $source
      vulnerabilityId: $vulnerabilityId
    ) @include(if: $isVulnDescriptionChanged) {
      success
    }
    updateVulnerabilityTreatment(
      customSeverity: $customSeverity
      externalBugTrackingSystem: $externalBugTrackingSystem
      findingId: $findingId
      tag: $tag
      vulnerabilityId: $vulnerabilityId
    ) @include(if: $isVulnTreatmentDescriptionChanged) {
      success
    }
  }
`);

interface IGroupOrganization {
  group: {
    name: string;
    organization: string;
  };
}

interface IOrganizationPolicies {
  organizationId: {
    findingPolicies: IVulnerabilityPoliciesData[];
  };
}

const GET_ORGANIZATION_POLICIES = graphql(`
  query GetOrganizationPoliciesAtUpdateDesc($organizationName: String!) {
    organizationId(organizationName: $organizationName) {
      name
      findingPolicies {
        id
        lastStatusUpdate
        name
        status
        tags
        treatmentAcceptance
      }
    }
  }
`);

interface IFindingDescriptionData {
  finding: {
    id: string;
    title: string;
  };
}

const GET_FINDING_TITLE_AND_ORG = graphql(`
  query GetFindingTitleAndOrg($findingId: String!) {
    finding(identifier: $findingId) {
      id
      organizationName
      title
    }
  }
`);

const SEND_ASSIGNED_NOTIFICATION = graphql(`
  mutation SendAssignedNotification(
    $findingId: String!
    $vulnerabilities: [String]!
  ) {
    sendAssignedNotification(
      findingId: $findingId
      vulnerabilities: $vulnerabilities
    ) {
      success
    }
  }
`);

const REMOVE_TAGS_MUTATION = graphql(`
  mutation RemoveTagsVuln(
    $findingId: String!
    $tag: String
    $vulnerabilities: [String]!
  ) {
    removeTags(
      findingId: $findingId
      tag: $tag
      vulnerabilities: $vulnerabilities
    ) {
      success
    }
  }
`);

const REQUEST_VULNS_ZERO_RISK = graphql(`
  mutation RequestVulnerabilitiesZeroRisk(
    $findingId: String!
    $justification: String!
    $vulnerabilities: [String]!
  ) {
    requestVulnerabilitiesZeroRisk(
      findingId: $findingId
      justification: $justification
      vulnerabilities: $vulnerabilities
    ) {
      success
    }
  }
`);

const GET_POLICIES = graphql(`
  query GetPolicies($groupName: String!) {
    group(groupName: $groupName) {
      maxAcceptanceDays
      maxAcceptanceSeverity
      maxNumberAcceptances
      minAcceptanceSeverity
      name
    }
  }
`);

export {
  GET_FINDING_TITLE_AND_ORG,
  GET_ORGANIZATION_POLICIES,
  GET_POLICIES,
  REMOVE_TAGS_MUTATION,
  REQUEST_VULNS_ZERO_RISK,
  UPDATE_VULNERABILITY_MUTATION,
  SEND_ASSIGNED_NOTIFICATION,
};
export type {
  IFindingDescriptionData,
  IGroupOrganization,
  IOrganizationPolicies,
};
