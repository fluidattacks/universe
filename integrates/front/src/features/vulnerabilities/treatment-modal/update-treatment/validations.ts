import isEmpty from "lodash/isEmpty";
import { date, number, object, string } from "yup";
import type {
  AnyObject,
  InferType,
  Maybe,
  ObjectSchema,
  Schema,
  TestContext,
} from "yup";

import { translate } from "utils/translations/translate";

const MAX_TAG_LENGTH = 30;
const MIN_CUSTOM_SEVERITY = -1000;
const MAX_CUSTOM_SEVERITY = 1000;
const MAX_BUG_TRACKING_LENGTH = 80;

const validateMaxLengthTag: (
  value: string | undefined,
  options: TestContext<Maybe<AnyObject>>,
) => boolean = (value, _options): boolean => {
  const tagsArray = value?.split(",");

  return (
    tagsArray?.every((tag): boolean => {
      return tag.length < MAX_TAG_LENGTH;
    }) ?? true
  );
};

const validationSchema = (
  isTreatmentPristine: boolean,
  initialSource: string,
): ObjectSchema<InferType<Schema>> =>
  object().shape({
    acceptanceDate: date().when(
      "treatment",
      ([treatment]: unknown[]): Schema => {
        return String(treatment) === "ACCEPTED"
          ? date()
              .min(new Date(), translate.t("validations.lowerDate"))
              .required(translate.t("validations.required"))
              .typeError(translate.t("validations.required"))
          : string().nullable();
      },
    ),
    assigned: string().when("treatment", ([treatment]: unknown[]): Schema => {
      return String(treatment) === "REQUEST_ZERO_RISK" ||
        String(treatment) === "UNTREATED" ||
        treatment === "" ||
        treatment === undefined
        ? string().nullable()
        : string().required(translate.t("validations.required"));
    }),
    customSeverity: number()
      .min(
        MIN_CUSTOM_SEVERITY,
        translate.t("validations.between", {
          max: MAX_CUSTOM_SEVERITY,
          min: MIN_CUSTOM_SEVERITY,
        }),
      )
      .max(
        MAX_CUSTOM_SEVERITY,
        translate.t("validations.between", {
          max: MAX_CUSTOM_SEVERITY,
          min: MIN_CUSTOM_SEVERITY,
        }),
      )
      .integer(translate.t("validations.integer"))
      .typeError(translate.t("validations.numeric")),
    externalBugTrackingSystem: string().when(
      "treatment",
      ([treatment]: unknown[]): Schema => {
        return String(treatment) === "REQUEST_ZERO_RISK"
          ? string().nullable()
          : string()
              .max(MAX_BUG_TRACKING_LENGTH)
              .isValidTextBeginning()
              .url(translate.t("validations.invalidUrl"))
              .nullable();
      },
    ),
    justification: string().when([], (): Schema => {
      return isTreatmentPristine
        ? string()
        : string()
            .required(translate.t("validations.required"))
            .isValidTextBeginning()
            .isValidTextField()
            .isValidTextPattern();
    }),
    source: string().when("$isEmptySource", (): Schema => {
      return isEmpty(initialSource)
        ? string()
        : string().required(translate.t("validations.required"));
    }),
    tag: string().isValidFunction(
      validateMaxLengthTag,
      translate.t("validations.tagsLength"),
    ),
    treatment: string().when([], (): Schema => {
      return isTreatmentPristine
        ? string()
        : string().required(translate.t("validations.required"));
    }),
  });

export { validationSchema };
