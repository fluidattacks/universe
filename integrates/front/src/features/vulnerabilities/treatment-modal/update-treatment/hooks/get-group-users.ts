import { useQuery } from "@apollo/client";
import { useAbility } from "@casl/react";
import _ from "lodash";
import { useContext, useMemo } from "react";

import type { IGetGroupUsersResult } from "./types";

import type { IAuthContext } from "context/auth";
import { authContext } from "context/auth";
import {
  authzPermissionsContext,
  userLevelPermissions,
} from "context/authz/config";
import { GET_GROUP_USERS } from "features/vulnerabilities/queries";
import type { GetGroupAccessInfoQueryVariables } from "gql/graphql";
import { InvitationState } from "gql/graphql";

const useGetGroupUsersQuery = ({
  groupName,
  numberOfGroups,
}: GetGroupAccessInfoQueryVariables & {
  numberOfGroups: number;
}): IGetGroupUsersResult => {
  const { userEmail }: IAuthContext = useContext(authContext);
  const permissions = useAbility(authzPermissionsContext);
  const { data, error, loading, refetch } = useQuery(GET_GROUP_USERS, {
    skip:
      permissions.cannot(
        "integrates_api_resolvers_group_stakeholders_resolve",
      ) || numberOfGroups > 1,
    variables: { groupName },
  });

  const canAssignVulnsToFluid: boolean = userLevelPermissions.can(
    "can_assign_vulnerabilities_to_fluidattacks_staff",
  );

  const assignableUsers = useMemo((): string[] => {
    const isRegistered = (invitation: InvitationState | null): boolean =>
      invitation === InvitationState.Registered;

    const canAssignToFluid = (email: string): boolean =>
      !email.endsWith("@fluidattacks.com") || canAssignVulnsToFluid;

    const isAssignable = (role: string): boolean =>
      ["user", "vulnerability_manager", "group_manager"].includes(role);

    return _.isUndefined(data) || _.isEmpty(data) || numberOfGroups > 1
      ? [userEmail]
      : data.group.stakeholders
          .filter(
            (stakeholder): boolean =>
              stakeholder.email !== null &&
              isRegistered(stakeholder.invitationState) &&
              canAssignToFluid(stakeholder.email) &&
              isAssignable(stakeholder.role ?? ""),
          )
          .map((stakeholder): string => stakeholder.email ?? "");
  }, [data, numberOfGroups, userEmail, canAssignVulnsToFluid]);

  const newData: IGetGroupUsersResult["data"] = {
    assignableUsers,
  };

  return {
    data: newData,
    error,
    loading,
    refetch,
  };
};

export { useGetGroupUsersQuery };
