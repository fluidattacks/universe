import type {
  ApolloError,
  ApolloQueryResult,
  FetchResult,
} from "@apollo/client";

import type { IUpdateVulnerabilityForm } from "features/vulnerabilities/types";
import type {
  GetGroupUsersQuery,
  GetGroupUsersQueryVariables,
  GetPoliciesQuery,
  GetPoliciesQueryVariables,
} from "gql/graphql";

interface IPolicies {
  maxDays: number;
  maxSeverity: number;
  minSeverity: number;
}

interface IGetPolicyQueryResult {
  data: IPolicies;
  error: ApolloError | undefined;
  loading: boolean;
  refetch: (
    variables: GetPoliciesQueryVariables,
  ) => Promise<ApolloQueryResult<GetPoliciesQuery>>;
}

interface IGetGroupUsersResult {
  data: { assignableUsers: string[] };
  error: ApolloError | undefined;
  loading: boolean;
  refetch: (
    variables: GetGroupUsersQueryVariables,
  ) => Promise<ApolloQueryResult<GetGroupUsersQuery>>;
}

interface IUpdateVulnerability {
  handleUpdateVulnerability: (
    values: IUpdateVulnerabilityForm,
    isDescriptionPristineP: boolean,
    isTreatmentAdditionalValuesPristineP: boolean,
    isTreatmentPristineP: boolean,
  ) => Promise<void>;
  updatingVulnerability: boolean;
}

interface IDeleteTag {
  handleDeletion: (tag: string) => Promise<void>;
  deletingTag: boolean;
}

interface IRequestZeroRisk {
  requestZeroRisk: (
    variables: Record<string, unknown>,
  ) => Promise<FetchResult<unknown>>;
  requestingZeroRisk: boolean;
}

interface ITreatmentMutations {
  handleDeletion: (tag: string) => Promise<void>;
  handleUpdateVulnerability: (
    values: IUpdateVulnerabilityForm,
    isDescriptionPristineP: boolean,
    isTreatmentAdditionalValuesPristineP: boolean,
    isTreatmentPristineP: boolean,
  ) => Promise<void>;
  loading: boolean;
  requestZeroRisk: (
    variables: Record<string, unknown>,
  ) => Promise<FetchResult<unknown>>;
}

export type {
  IDeleteTag,
  IGetGroupUsersResult,
  IGetPolicyQueryResult,
  IPolicies,
  IRequestZeroRisk,
  ITreatmentMutations,
  IUpdateVulnerability,
};
