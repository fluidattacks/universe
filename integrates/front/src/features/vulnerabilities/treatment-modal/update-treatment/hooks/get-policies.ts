import { useQuery } from "@apollo/client";

import type { IGetPolicyQueryResult } from "./types";

import { GET_POLICIES } from "../queries";
import type { GetPoliciesQueryVariables } from "gql/graphql";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";
import { translate } from "utils/translations/translate";

const useGetPoliciesQuery = ({
  groupName,
}: GetPoliciesQueryVariables): IGetPolicyQueryResult => {
  const { data, error, loading, refetch } = useQuery(GET_POLICIES, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((err): void => {
        msgError(translate.t("groupAlerts.errorTextsad"));
        Logger.warning("An error occurred fetching group policies", err);
      });
    },
    variables: { groupName },
  });
  const MAX_ACCEPTANCE_DAYS = 60;
  const MAX_SEVERITY = 10.0;
  const newData: IGetPolicyQueryResult["data"] = {
    maxDays: data?.group.maxAcceptanceDays ?? MAX_ACCEPTANCE_DAYS,
    maxSeverity: data?.group.maxAcceptanceSeverity ?? MAX_SEVERITY,
    minSeverity: data?.group.minAcceptanceSeverity ?? 0.0,
  };

  return {
    data: newData,
    error,
    loading,
    refetch,
  };
};

export { useGetPoliciesQuery };
