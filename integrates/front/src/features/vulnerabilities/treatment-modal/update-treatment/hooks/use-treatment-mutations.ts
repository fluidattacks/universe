import { useMutation } from "@apollo/client";
import mixpanel from "mixpanel-browser";
import { useCallback } from "react";
import { useTranslation } from "react-i18next";
import { useLocation } from "react-router-dom";

import type {
  IDeleteTag,
  IRequestZeroRisk,
  ITreatmentMutations,
  IUpdateVulnerability,
} from "./types";

import {
  REMOVE_TAGS_MUTATION,
  REQUEST_VULNS_ZERO_RISK,
  SEND_ASSIGNED_NOTIFICATION,
  UPDATE_VULNERABILITY_MUTATION,
} from "../queries";
import type {
  IRemoveTagResultAttr,
  IRequestVulnZeroRiskResultAttr,
} from "../types";
import {
  dataTreatmentTrackHelper,
  deleteTagVulnHelper,
  getAllNotifications,
  getAllResults,
  getAreAllChunkedMutationValid,
  getAreAllNotificationValid,
  handleRequestZeroRiskError,
  handleUpdateVulnTreatmentError,
  requestZeroRiskHelper,
  validMutationsHelper,
} from "../utils";
import type {
  IUpdateVulnerabilityForm,
  IVulnDataTypeAttr,
} from "features/vulnerabilities/types";
import { GET_FINDING_HEADER } from "pages/finding/queries";
import { GET_FINDING_INFO } from "pages/finding/vulnerabilities/queries";
import { sleep } from "utils/helpers";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

const useUpdateVulnerability = (
  handleCloseModal: () => void,
  refetchData: () => void,
  setIsRunning: React.Dispatch<React.SetStateAction<boolean>>,
  vulnerabilities: IVulnDataTypeAttr[],
): IUpdateVulnerability => {
  const { t } = useTranslation();
  const { pathname } = useLocation();

  const [updateVulnerability, { loading: updatingVulnerability }] = useMutation(
    UPDATE_VULNERABILITY_MUTATION,
    {
      onCompleted: (): void => {
        if (pathname.includes("priority")) {
          mixpanel.track("PriorityVulnerabilityTreatmentUpdated");
        }
      },
    },
  );

  const [sendNotification] = useMutation(SEND_ASSIGNED_NOTIFICATION, {
    onError: (updateError): void => {
      updateError.graphQLErrors.forEach((error): void => {
        msgError(
          t(
            "searchFindings.tabDescription.notification.emailNotificationError",
          ),
        );
        Logger.warning("An error occurred sending the notification", error);
      });
    },
  });

  const handleUpdateVulnerability = useCallback(
    async (
      values: IUpdateVulnerabilityForm,
      isDescriptionPristineP: boolean,
      isTreatmentAdditionalValuesPristineP: boolean,
      isTreatmentPristineP: boolean,
    ): Promise<void> => {
      if (vulnerabilities.length === 0) {
        msgError(t("searchFindings.tabResources.noSelection"));
      } else {
        dataTreatmentTrackHelper(values);

        try {
          setIsRunning(true);
          const results = await getAllResults(
            updateVulnerability,
            vulnerabilities,
            values,
            isDescriptionPristineP,
            isTreatmentAdditionalValuesPristineP,
            isTreatmentPristineP,
          );

          const areAllMutationValid = getAreAllChunkedMutationValid(results);
          validMutationsHelper(
            handleCloseModal,
            areAllMutationValid,
            values,
            vulnerabilities,
            isTreatmentPristineP,
          );
          const SLEEP_DELAY_MS = 1000;
          await sleep(SLEEP_DELAY_MS).then((): void => {
            refetchData();
          });

          if (!isTreatmentPristineP) {
            const notificationsResults = await getAllNotifications(
              sendNotification,
              vulnerabilities,
            );
            const areAllNotificationValid =
              getAreAllNotificationValid(notificationsResults);
            if (areAllNotificationValid.every(Boolean)) {
              msgSuccess(
                t(
                  "searchFindings.tabDescription.notification.emailNotificationText",
                ),
                t(
                  "searchFindings.tabDescription.notification.emailNotificationTitle",
                ),
              );
            }
          }
        } catch (updateError: unknown) {
          handleUpdateVulnTreatmentError(updateError);
        } finally {
          setIsRunning(false);
        }
      }
    },
    [
      handleCloseModal,
      refetchData,
      sendNotification,
      setIsRunning,
      t,
      updateVulnerability,
      vulnerabilities,
    ],
  );

  return { handleUpdateVulnerability, updatingVulnerability };
};

const useDeleteTag = (
  refetchData: () => void,
  vulnerabilities: IVulnDataTypeAttr[],
): IDeleteTag => {
  const { t } = useTranslation();

  const [deleteTagVuln, { loading: deletingTag }] = useMutation(
    REMOVE_TAGS_MUTATION,
    {
      onCompleted: (result: IRemoveTagResultAttr): void => {
        deleteTagVulnHelper(result);
        refetchData();
      },
      onError: (updateError): void => {
        updateError.graphQLErrors.forEach((error): void => {
          msgError(t("groupAlerts.errorTextsad"));
          Logger.warning(
            "An error occurred deleting vulnerabilities tags",
            error,
          );
        });
      },
    },
  );

  const handleDeletion = useCallback(
    async (tag: string): Promise<void> => {
      await deleteTagVuln({
        variables: {
          findingId: vulnerabilities[0].findingId,
          tag,
          vulnerabilities: vulnerabilities.map(
            (vuln: IVulnDataTypeAttr): string => vuln.id,
          ),
        },
      });
    },
    [deleteTagVuln, vulnerabilities],
  );

  return { deletingTag, handleDeletion };
};

const useRequestZeroRisk = (
  handleCloseModal: () => void,
  handleClearSelected: (() => void) | undefined,
  refetchData: () => void,
  vulnerabilities: IVulnDataTypeAttr[],
): IRequestZeroRisk => {
  const { pathname } = useLocation();

  const [requestZeroRisk, { loading: requestingZeroRisk }] = useMutation(
    REQUEST_VULNS_ZERO_RISK,
    {
      onCompleted: (
        requestZeroRiskVulnResult: IRequestVulnZeroRiskResultAttr,
      ): void => {
        if (pathname.includes("priority")) {
          mixpanel.track("PriorityVulnerabilityZeroRiskRequested");
        }
        requestZeroRiskHelper(
          handleCloseModal,
          refetchData,
          requestZeroRiskVulnResult,
          handleClearSelected,
        );
      },
      onError: ({ graphQLErrors }): void => {
        handleRequestZeroRiskError(graphQLErrors);
      },
      refetchQueries: [
        {
          query: GET_FINDING_INFO,
          variables: {
            findingId: vulnerabilities[0].findingId,
          },
        },
        {
          query: GET_FINDING_HEADER,
          variables: {
            findingId: vulnerabilities[0].findingId,
          },
        },
      ],
    },
  );

  return {
    requestZeroRisk,
    requestingZeroRisk,
  };
};

const useTreatmentMutations = (
  handleCloseModal: () => void,
  handleClearSelected: (() => void) | undefined,
  refetchData: () => void,
  setIsRunning: React.Dispatch<React.SetStateAction<boolean>>,
  vulnerabilities: IVulnDataTypeAttr[],
): ITreatmentMutations => {
  const { handleDeletion, deletingTag } = useDeleteTag(
    refetchData,
    vulnerabilities,
  );

  const { handleUpdateVulnerability, updatingVulnerability } =
    useUpdateVulnerability(
      handleCloseModal,
      refetchData,
      setIsRunning,
      vulnerabilities,
    );

  const { requestZeroRisk, requestingZeroRisk } = useRequestZeroRisk(
    handleCloseModal,
    handleClearSelected,
    refetchData,
    vulnerabilities,
  );

  return {
    handleDeletion,
    handleUpdateVulnerability,
    loading: deletingTag || requestingZeroRisk || updatingVulnerability,
    requestZeroRisk,
  };
};

export {
  useDeleteTag,
  useRequestZeroRisk,
  useTreatmentMutations,
  useUpdateVulnerability,
};
