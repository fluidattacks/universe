import type { FetchResult } from "@apollo/client";
import type { IItem, TFormMethods } from "@fluidattacks/design";
import type { ExecutionResult } from "graphql";

import type { IUpdateVulnerabilityForm, IVulnDataTypeAttr } from "../../types";
import type { IHistoricTreatment } from "pages/finding/description/types";

interface ITreatmentContent extends TFormMethods<IUpdateVulnerabilityForm> {
  canDeleteVulnsTags: boolean;
  canRequestZeroRiskVuln: boolean;
  canUpdateVulnsTreatment: boolean;
  handleDeletion: (tag: string) => void;
  handleMessage: (value: React.SetStateAction<string | null>) => void;
  items: IItem[];
  isActiveFindingPolicy: boolean;
  isPristine: boolean;
  lastTreatment: IHistoricTreatment;
  loadingUsers: boolean;
  maxDays: number;
  setUpdateTreatment?: React.Dispatch<React.SetStateAction<boolean>>;
  setIsPristine: React.Dispatch<React.SetStateAction<boolean>>;
  showSource: boolean;
  sourceOptions: IItem[];
  userEmails: string[];
}

interface ITreatmentForm {
  close: () => void;
  findingId?: string;
  groupName: string;
  handleClearSelected?: () => void;
  refetchData: () => void;
  vulnerabilities: IVulnDataTypeAttr[];
  onActionCompleted?: () => void;
}

interface ISubmitHelper {
  changedToRequestZeroRisk: boolean;
  confirm: (confirmCallback: () => void) => Promise<void>;
  findingId: string;
  handleUpdateVulnerability: (
    values: IUpdateVulnerabilityForm,
    isDescriptionPristine: boolean,
    isTreatmentDescriptionPristine: boolean,
    isTreatmentPristine: boolean,
  ) => Promise<void>;
  isDescriptionPristine: boolean;
  isTemporaryOrUndefinedTreatment: boolean;
  isTreatmentDescriptionPristine: boolean;
  isTreatmentPristine: boolean;
  requestZeroRisk: (
    variables: Record<string, unknown>,
  ) => Promise<FetchResult<unknown>>;
  values: IUpdateVulnerabilityForm;
  vulnerabilities: IVulnDataTypeAttr[];
}

interface IUpdateVulnerabilityResultAttr {
  updateVulnerabilityDescription?: {
    success: boolean;
  };
  updateVulnerabilityTreatment?: {
    success: boolean;
  };
  updateVulnerabilitiesTreatment?: {
    success: boolean;
  };
}

interface IRemoveTagResultAttr {
  removeTags: {
    success: boolean;
  };
}

interface IRequestVulnZeroRiskResultAttr {
  requestVulnerabilitiesZeroRisk: {
    success: boolean;
  };
}

interface ISendNotificationResultAttr {
  sendAssignedNotification: {
    success: boolean;
  };
}

type TNotificationResult = ExecutionResult<ISendNotificationResultAttr>;
type TVulnUpdateResult = ExecutionResult<IUpdateVulnerabilityResultAttr>;

export type {
  ITreatmentContent,
  ITreatmentForm,
  IRemoveTagResultAttr,
  IRequestVulnZeroRiskResultAttr,
  ISubmitHelper,
  IUpdateVulnerabilityResultAttr,
  TNotificationResult,
  TVulnUpdateResult,
};
