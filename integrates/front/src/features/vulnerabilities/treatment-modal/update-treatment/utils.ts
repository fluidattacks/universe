/* eslint-disable max-lines */
import type { ExecutionResult, GraphQLError } from "graphql";
import _ from "lodash";
import mixpanel from "mixpanel-browser";

import type {
  IRemoveTagResultAttr,
  IRequestVulnZeroRiskResultAttr,
  ISubmitHelper,
  IUpdateVulnerabilityResultAttr,
  TNotificationResult,
  TVulnUpdateResult,
} from "./types";

import type { IUpdateVulnerabilityForm, IVulnDataTypeAttr } from "../../types";
import type { IHistoricTreatment } from "pages/finding/description/types";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";
import { translate } from "utils/translations/translate";

interface ITreatmentForm {
  isDescriptionPristine: boolean;
  isTreatmentAdditionalValuesPristine: boolean;
  isTreatmentPristine: boolean;
}

const emptyTreatment: IHistoricTreatment = {
  acceptanceDate: "",
  acceptanceStatus: "",
  assigned: "",
  date: "",
  justification: "",
  treatment: "",
  user: "",
};

const getDiff = (
  initValues: Record<string, unknown>,
  values: Record<string, unknown>,
): string[] => {
  return _.reduce(
    initValues,
    (result: string[], value: unknown, key: string): string[] => {
      return _.isEqual(value, values[key]) ? result : result.concat(key);
    },
    [],
  );
};

const useDefaultValues = (
  lastTreatment: IHistoricTreatment,
  defaultSeverity: number,
  defaultBugTrackingSystem: string,
  defaultSource: string,
  defaultTag: string,
): Record<string, unknown> => {
  return {
    acceptanceDate: lastTreatment.acceptanceDate ?? "",
    acceptanceUser: lastTreatment.user,
    assigned: lastTreatment.assigned ?? "",
    customSeverity: defaultSeverity,
    externalBugTrackingSystem: defaultBugTrackingSystem,
    justification: lastTreatment.justification ?? "",
    source: defaultSource,
    tag: defaultTag,
    treatment: lastTreatment.treatment,
  };
};

const getTreatmentPristine = ({
  initialValues,
  values,
}: {
  initialValues: Record<string, unknown>;
  values: IUpdateVulnerabilityForm;
}): boolean => {
  const diffs: string[] = getDiff(initialValues, { ...values });
  const isTreatmentValuesPristine =
    diffs.filter((diff: string): boolean =>
      ["acceptanceDate", "treatment", "assigned"].includes(diff),
    ).length === 0;

  return (
    isTreatmentValuesPristine &&
    (_.isEmpty(values.justification) ||
      (initialValues.justification as string) === values.justification)
  );
};

const getTreatmentState = ({
  additionalFields = [],
  initialValues,
  values,
}: {
  additionalFields?: string[];
  initialValues: Record<string, unknown>;
  values: IUpdateVulnerabilityForm;
}): ITreatmentForm => {
  const diffs: string[] = getDiff(initialValues, { ...values });

  return {
    isDescriptionPristine:
      diffs.filter((diff: string): boolean => ["source"].includes(diff))
        .length === 0,
    isTreatmentAdditionalValuesPristine: _.isEmpty(additionalFields)
      ? false
      : diffs.filter((diff: string): boolean => additionalFields.includes(diff))
          .length === 0,
    isTreatmentPristine: getTreatmentPristine({ initialValues, values }),
  };
};

const sortTags = (tags: string): string[] => {
  const tagSplit = tags.trim().split(",");

  return tagSplit.map((tag): string => tag.trim());
};

const groupExternalBugTrackingSystem = (
  vulnerabilities: IVulnDataTypeAttr[],
): string | null => {
  const bts = vulnerabilities.reduce<string>(
    (acc, vuln): string =>
      _.isEmpty(vuln.externalBugTrackingSystem)
        ? acc
        : (vuln.externalBugTrackingSystem as string),
    "",
  );

  if (_.isEmpty(bts)) return null;

  return vulnerabilities.every((row): boolean =>
    _.isEmpty(row.externalBugTrackingSystem)
      ? true
      : row.externalBugTrackingSystem === bts,
  )
    ? bts
    : null;
};

const getLastTreatment = (
  historic: IHistoricTreatment[],
): IHistoricTreatment => {
  const lastTreatment =
    historic.length > 0
      ? (_.last(historic) as IHistoricTreatment)
      : emptyTreatment;

  return {
    ...lastTreatment,
    acceptanceDate: _.isNull(lastTreatment.acceptanceDate)
      ? ""
      : _.get(lastTreatment, "acceptanceDate", "").split(" ")[0],
    treatment: lastTreatment.treatment.replace(" ", "_"),
  };
};

const groupLastHistoricTreatment = (
  vulnerabilities: IVulnDataTypeAttr[],
): IHistoricTreatment => {
  const attributeToOmitWhenComparing = ["date", "user"];
  const treatment: IHistoricTreatment = vulnerabilities.reduce(
    (acc, vuln): IHistoricTreatment => {
      const lastTreatment = getLastTreatment(vuln.historicTreatment);

      return lastTreatment.treatment !== "UNTREATED" &&
        !_.isEmpty(lastTreatment.treatment)
        ? lastTreatment
        : acc;
    },
    emptyTreatment,
  );

  return vulnerabilities.every((vuln): boolean => {
    const lastTreatment = getLastTreatment(vuln.historicTreatment);

    return _.some(lastTreatment, _.isEmpty)
      ? _.isEqual(
          _.omit(lastTreatment, attributeToOmitWhenComparing),
          _.omit(treatment, attributeToOmitWhenComparing),
        )
      : true;
  })
    ? treatment
    : emptyTreatment;
};

const getAreAllMutationValid = (
  results: ExecutionResult<IUpdateVulnerabilityResultAttr>[],
): boolean[] => {
  return results.map((result): boolean => {
    if (!_.isUndefined(result.data) && !_.isNull(result.data)) {
      const updateInfoSuccess = _.isUndefined(
        result.data.updateVulnerabilityTreatment,
      )
        ? true
        : result.data.updateVulnerabilityTreatment.success;
      const updateTreatmentSuccess = _.isUndefined(
        result.data.updateVulnerabilitiesTreatment,
      )
        ? true
        : result.data.updateVulnerabilitiesTreatment.success;

      return updateInfoSuccess && updateTreatmentSuccess;
    }

    return false;
  });
};

const getAreAllChunkedMutationValid = (
  results: ExecutionResult<IUpdateVulnerabilityResultAttr>[][],
): boolean[] =>
  results
    .map(getAreAllMutationValid)
    .reduce((previous, current): boolean[] => [...previous, ...current], []);

const dataTreatmentTrackHelper = (
  dataTreatment: IUpdateVulnerabilityForm,
): void => {
  if (dataTreatment.tag !== undefined) {
    mixpanel.track("AddVulnerabilityTag");
  }
  if (!_.isEmpty(dataTreatment.customSeverity)) {
    mixpanel.track("AddVulnerabilityLevel");
  }
};

const deleteTagVulnHelper = (result: IRemoveTagResultAttr): void => {
  if (!_.isUndefined(result)) {
    if (result.removeTags.success) {
      msgSuccess(
        translate.t("searchFindings.tabDescription.updateVulnerabilities"),
        translate.t("groupAlerts.titleSuccess"),
      );
    }
  }
};

const getResults = async (
  updateVulnerability: (
    variables: Record<string, unknown>,
  ) => Promise<TVulnUpdateResult>,
  vulnerabilities: IVulnDataTypeAttr[],
  values: IUpdateVulnerabilityForm,
  findingId: string,
  isDescriptionPristine: boolean,
  isTreatmentDescriptionPristine: boolean,
  isTreatmentPristine: boolean,
): Promise<TVulnUpdateResult[]> => {
  const chunkSize = 10;
  const vulnChunks = _.chunk(vulnerabilities, chunkSize);
  const updateChunks = vulnChunks.map(
    (chunk): (() => Promise<TVulnUpdateResult[]>) =>
      async (): Promise<TVulnUpdateResult[]> => {
        const updates = chunk.map(
          async (vuln): Promise<TVulnUpdateResult> =>
            updateVulnerability({
              variables: {
                acceptanceDate: values.acceptanceDate,
                assigned: _.isEmpty(values.assigned)
                  ? undefined
                  : values.assigned,
                customSeverity: _.isEmpty(String(values.customSeverity))
                  ? 0
                  : Number(values.customSeverity),
                externalBugTrackingSystem: values.externalBugTrackingSystem,
                findingId,
                isVulnDescriptionChanged: !isDescriptionPristine,
                isVulnTreatmentChanged: !isTreatmentPristine,
                isVulnTreatmentDescriptionChanged:
                  !isTreatmentDescriptionPristine,
                justification: values.justification,
                source: _.isEmpty(values.source) ? undefined : values.source,
                tag: values.tag,
                treatment: isTreatmentPristine
                  ? "IN_PROGRESS"
                  : values.treatment,
                vulnerabilityId: vuln.id,
              },
            }),
        );

        return Promise.all(updates);
      },
  );

  // Sequentially execute chunks
  return updateChunks.reduce(
    async (previousValue, currentValue): Promise<TVulnUpdateResult[]> => [
      ...(await previousValue),
      ...(await currentValue()),
    ],
    Promise.resolve<TVulnUpdateResult[]>([]),
  );
};

const getAllResults = async (
  updateVulnerability: (
    variables: Record<string, unknown>,
  ) => Promise<TVulnUpdateResult>,
  vulnerabilities: IVulnDataTypeAttr[],
  dataTreatment: IUpdateVulnerabilityForm,
  isDescriptionPristine: boolean,
  isTreatmentDescriptionPristine: boolean,
  isTreatmentPristine: boolean,
): Promise<TVulnUpdateResult[][]> => {
  const vulnerabilitiesByFinding = _.groupBy(
    vulnerabilities,
    (vuln: IVulnDataTypeAttr): string => vuln.findingId,
  );
  const requestedChunks = Object.entries(vulnerabilitiesByFinding).map(
    ([findingId, chunkedVulnerabilities]: [
      string,
      IVulnDataTypeAttr[],
    ]): (() => Promise<TVulnUpdateResult[][]>) =>
      async (): Promise<TVulnUpdateResult[][]> => {
        return Promise.all([
          getResults(
            updateVulnerability,
            chunkedVulnerabilities,
            dataTreatment,
            findingId,
            isDescriptionPristine,
            isTreatmentDescriptionPristine,
            isTreatmentPristine,
          ),
        ]);
      },
  );

  return requestedChunks.reduce(
    async (previousValue, currentValue): Promise<TVulnUpdateResult[][]> => [
      ...(await previousValue),
      ...(await currentValue()),
    ],
    Promise.resolve<TVulnUpdateResult[][]>([]),
  );
};

const getAllNotifications = async (
  sendNotification: (
    variables: Record<string, unknown>,
  ) => Promise<TNotificationResult>,
  vulnerabilities: IVulnDataTypeAttr[],
): Promise<TNotificationResult[]> => {
  const vulnerabilitiesByFinding = _.groupBy(
    vulnerabilities,
    (vuln): string => vuln.findingId,
  );
  const requestedChunks = Object.entries(vulnerabilitiesByFinding).map(
    ([findingId, chunkedVulnerabilities]: [
      string,
      IVulnDataTypeAttr[],
    ]): (() => Promise<TNotificationResult[]>) =>
      async (): Promise<TNotificationResult[]> => {
        return Promise.all([
          await sendNotification({
            variables: {
              findingId,
              vulnerabilities: chunkedVulnerabilities.map(
                ({ id }): string => id,
              ),
            },
          }),
        ]);
      },
  );

  return requestedChunks.reduce(
    async (previousValue, currentValue): Promise<TNotificationResult[]> => [
      ...(await previousValue),
      ...(await currentValue()),
    ],
    Promise.resolve<TNotificationResult[]>([]),
  );
};

const getAreAllNotificationValid = (
  results: TNotificationResult[],
): boolean[] => {
  return results.map((result): boolean => {
    if (!_.isUndefined(result.data) && !_.isNull(result.data)) {
      return _.isUndefined(result.data.sendAssignedNotification)
        ? true
        : result.data.sendAssignedNotification.success;
    }

    return false;
  });
};

const validMutationsHelper = (
  handleCloseModal: () => void,
  areAllMutationValid: boolean[],
  dataTreatment: IUpdateVulnerabilityForm,
  vulnerabilities: IVulnDataTypeAttr[],
  isTreatmentPristine: boolean,
): void => {
  if (areAllMutationValid.every(Boolean)) {
    mixpanel.track("UpdatedTreatmentVulnerabilities", {
      batchSize: vulnerabilities.length,
    });
    if (!isTreatmentPristine && !_.isEmpty(dataTreatment.assigned)) {
      const assignedChanged = vulnerabilities.filter(
        (vulnerability): boolean =>
          vulnerability.assigned !== dataTreatment.assigned,
      ).length;
      if (assignedChanged > 0) {
        mixpanel.track("UpdatedAssignedVulnerabilities", {
          batchSize: assignedChanged,
        });
      }
    }
    msgSuccess(
      translate.t("searchFindings.tabDescription.updateVulnerabilities"),
      translate.t("groupAlerts.titleSuccess"),
    );
    handleCloseModal();
  }
};

const handleUpdateVulnTreatmentError = (updateError: unknown): void => {
  if (_.includes(String(updateError), "Assigned not valid")) {
    msgError(translate.t("groupAlerts.invalidAssigned"));
  } else if (
    _.includes(
      String(updateError),
      translate.t("searchFindings.tabVuln.alerts.maximumNumberOfAcceptances"),
    )
  ) {
    msgError(
      translate.t("searchFindings.tabVuln.alerts.maximumNumberOfAcceptances"),
    );
  } else if (
    _.includes(
      String(updateError),
      translate.t("groupAlerts.organizationPolicies.exceedsAcceptanceDate"),
    )
  ) {
    msgError(
      translate.t("groupAlerts.organizationPolicies.exceedsAcceptanceDate"),
    );
  } else if (
    _.includes(String(updateError), "The vulnerability has already been closed")
  ) {
    msgError(translate.t("groupAlerts.vulnClosed"));
  } else if (
    _.includes(
      String(updateError),
      translate.t("searchFindings.tabVuln.exceptions.severityOutOfRange"),
    )
  ) {
    msgError(
      translate.t("groupAlerts.organizationPolicies.severityOutOfRange"),
    );
  } else if (
    _.includes(
      String(updateError),
      translate.t("searchFindings.tabVuln.exceptions.sameValues"),
    )
  ) {
    msgError(translate.t("searchFindings.tabVuln.exceptions.sameValues"));
  } else {
    msgError(translate.t("groupAlerts.errorTextsad"));
    Logger.warning("An error occurred updating vuln treatment", updateError);
  }
};

const requestZeroRiskHelper = (
  handleCloseModal: () => void,
  refetchData: () => void,
  requestZeroRiskVulnResult: IRequestVulnZeroRiskResultAttr,
  handleClearSelected?: () => void,
): void => {
  if (requestZeroRiskVulnResult.requestVulnerabilitiesZeroRisk.success) {
    msgSuccess(
      translate.t("groupAlerts.requestedZeroRiskSuccess"),
      translate.t("groupAlerts.updatedTitle"),
    );
    if (handleClearSelected) {
      handleClearSelected();
    }
    handleCloseModal();
    refetchData();
  }
};

const handleRequestZeroRiskError = (
  graphQLErrors: readonly GraphQLError[],
): void => {
  graphQLErrors.forEach((error): void => {
    if (
      error.message ===
      "Exception - Zero risk vulnerability is already requested"
    ) {
      msgError(translate.t("groupAlerts.zeroRiskAlreadyRequested"));
    } else if (
      error.message ===
      "Exception - Justification must have a maximum of 5000 characters"
    ) {
      msgError(translate.t("validations.invalidFieldLength"));
    } else {
      msgError(translate.t("groupAlerts.errorTextsad"));
      Logger.warning("An error occurred requesting zero risk vuln", error);
    }
  });
};

const handleSubmitHelper = async ({
  changedToRequestZeroRisk,
  confirm,
  findingId,
  handleUpdateVulnerability,
  isDescriptionPristine,
  isTemporaryOrUndefinedTreatment,
  isTreatmentDescriptionPristine,
  isTreatmentPristine,
  requestZeroRisk,
  values,
  vulnerabilities,
}: ISubmitHelper): Promise<void> => {
  if (changedToRequestZeroRisk) {
    await requestZeroRisk({
      variables: {
        findingId,
        justification: values.justification,
        vulnerabilities: vulnerabilities.map((vuln): string => vuln.id),
      },
    });
  } else if (isTemporaryOrUndefinedTreatment) {
    void confirm((): void => {
      void handleUpdateVulnerability(
        values,
        isDescriptionPristine,
        isTreatmentDescriptionPristine,
        isTreatmentPristine,
      );
    });
  } else {
    await handleUpdateVulnerability(
      values,
      isDescriptionPristine,
      isTreatmentDescriptionPristine,
      isTreatmentPristine,
    );
  }
};

export {
  dataTreatmentTrackHelper,
  deleteTagVulnHelper,
  getAllResults,
  getAllNotifications,
  getAreAllChunkedMutationValid,
  getAreAllNotificationValid,
  getDiff,
  getTreatmentPristine,
  getTreatmentState,
  groupExternalBugTrackingSystem,
  groupLastHistoricTreatment,
  getLastTreatment,
  handleRequestZeroRiskError,
  handleSubmitHelper,
  handleUpdateVulnTreatmentError,
  requestZeroRiskHelper,
  sortTags,
  useDefaultValues,
  validMutationsHelper,
};
