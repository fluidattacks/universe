import _ from "lodash";

import type { TVulnFilter } from "./types";

import type { IVulnRowAttr } from "../types";
import { formatVulnerabilities } from "../utils";
import type { IFilter, ISelectedOptions } from "components/filter/types";
import { filterVerification } from "pages/finding/vulnerabilities/utils";
import type { IFindingAttr } from "pages/group/findings/types";
import { translate } from "utils/translations/translate";

const getTreatmentFilterOptions = (): ISelectedOptions[] => [
  { header: "-", value: "-" },
  {
    header: translate.t("searchFindings.tabDescription.treatment.new"),
    value: "untreated",
  },
  {
    header: translate.t("searchFindings.tabDescription.treatment.inProgress"),
    value: "inProgress",
  },
  {
    header: translate.t("searchFindings.tabDescription.treatment.accepted"),
    value: "accepted",
  },
  {
    header: translate.t(
      "searchFindings.tabDescription.treatment.acceptedUndefined",
    ),
    value: "acceptedUndefined",
  },
];

const getStateLegacyValue = (
  findingFilters: IFilter<IFindingAttr>[],
): string => {
  const findingState = findingFilters.find(
    (findingFilter): boolean => findingFilter.id === "state",
  )?.value;
  const state = findingState === "DRAFT" ? "" : findingState;

  return state ?? "";
};

const getTechniqueLegacyValue = (
  findingFilters: IFilter<IFindingAttr>[],
): string => {
  const techniqueValue =
    findingFilters.find(
      (findingFilter): boolean => findingFilter.id === "technique",
    )?.value ?? "";

  return techniqueValue;
};

const getTreatmentLegacyValue = (
  findingFilters: IFilter<IFindingAttr>[],
): string => {
  const treatmentValue =
    findingFilters.find(
      (findingFilter): boolean => findingFilter.id === "treatment",
    )?.value ?? "";

  return treatmentValue;
};

const getLocationLegacyValue = (
  findingFilters: IFilter<IFindingAttr>[],
): string => {
  const locationValue =
    findingFilters.find((findingFilter): boolean => findingFilter.id === "root")
      ?.value ?? "";

  return locationValue;
};

const getFiltersFromStorage = (groupName: string): IFilter<IFindingAttr>[] =>
  JSON.parse(
    localStorage.getItem(`tblFindFilters-${groupName}`) ?? "[]",
  ) as IFilter<IFindingAttr>[];

const getFilters = (groupName: string): TVulnFilter[] => [
  {
    id: "where",
    isLegacyFilter: true,
    key: "where",
    label: "Location",
    legacyValue: getLocationLegacyValue(getFiltersFromStorage(groupName)),
    type: "text",
  },
  {
    id: "currentState",
    isBackFilter: true,
    isLegacyFilter: true,
    key: "state",
    label: translate.t("searchFindings.tabVuln.vulnTable.status"),
    legacyValue: getStateLegacyValue(getFiltersFromStorage(groupName)),
    selectOptions: [
      {
        header: translate.t("searchFindings.tabVuln.open"),
        value: "VULNERABLE",
      },
      { header: translate.t("searchFindings.tabVuln.closed"), value: "SAFE" },
      {
        header: translate.t("searchFindings.tabVuln.rejected"),
        value: "REJECTED",
      },
      {
        header: translate.t("searchFindings.tabVuln.submitted"),
        value: "SUBMITTED",
      },
    ],
    type: "select",
  },
  {
    id: "technique",
    isLegacyFilter: true,
    key: "technique",
    label: translate.t("searchFindings.tabVuln.vulnTable.technique"),
    legacyValue: getTechniqueLegacyValue(getFiltersFromStorage(groupName)),
    selectOptions: [
      {
        header: translate.t("searchFindings.tabVuln.technique.cspm"),
        value: "CSPM",
      },
      {
        header: translate.t("searchFindings.tabVuln.technique.dast"),
        value: "DAST",
      },
      {
        header: translate.t("searchFindings.tabVuln.technique.ptaas"),
        value: "PTAAS",
      },
      {
        header: translate.t("searchFindings.tabVuln.technique.re"),
        value: "RE",
      },
      {
        header: translate.t("searchFindings.tabVuln.technique.sast"),
        value: "SAST",
      },
      {
        header: translate.t("searchFindings.tabVuln.technique.sca"),
        value: "SCA",
      },
      {
        header: translate.t("searchFindings.tabVuln.technique.scr"),
        value: "SCR",
      },
    ],
    type: "select",
  },
  {
    id: "reportDate",
    key: "reportDate",
    label: translate.t("searchFindings.tabVuln.vulnTable.reportDate"),
    type: "dateRange",
    valueType: "rangeValues",
  },
  {
    id: "verification",
    key: filterVerification,
    label: translate.t("searchFindings.tabVuln.vulnTable.reattack"),
    selectOptions: [
      {
        header: translate.t("searchFindings.tabVuln.onHold"),
        value: "On_hold",
      },
      {
        header: translate.t("searchFindings.tabVuln.requested"),
        value: "Requested",
      },
      {
        header: translate.t("searchFindings.tabVuln.verified"),
        value: "Verified",
      },
      {
        header: translate.t("searchFindings.tabVuln.notRequested"),
        value: "NotRequested",
      },
    ],
    type: "select",
  },
  {
    id: "treatment",
    isLegacyFilter: true,
    key: (vuln, value): boolean => {
      if (_.isEmpty(value)) return true;
      const formattedvuln = formatVulnerabilities({
        vulnerabilities: [vuln],
      });

      const filterOption = getTreatmentFilterOptions().find(
        (option): boolean => option.value === value,
      );

      return formattedvuln[0].treatmentStatus === filterOption?.header;
    },
    label: translate.t("searchFindings.tabVuln.vulnTable.treatment"),
    legacyValue: getTreatmentLegacyValue(getFiltersFromStorage(groupName)),
    selectOptions: getTreatmentFilterOptions,
    type: "select",
  },
  {
    id: "tag",
    key: "tag",
    label: translate.t("searchFindings.tabVuln.vulnTable.tags"),
    type: "tags",
  },
  {
    id: "treatmentAssigned",
    key: "treatmentAssigned",
    label: "Assignees",
    selectOptions: (vulns: IVulnRowAttr[]): ISelectedOptions[] =>
      vulns.map(
        (vuln): ISelectedOptions => ({ value: vuln.treatmentAssigned ?? "" }),
      ),
    type: "select",
  },
];

export {
  getStateLegacyValue,
  getTechniqueLegacyValue,
  getTreatmentFilterOptions,
  getTreatmentLegacyValue,
  getFilters,
};
