import type React from "react";

import type { IVulnRowAttr } from "../types";
import type { IFilter } from "components/filter/types";
import type { VulnerabilityState } from "gql/graphql";

interface IVulnerabilitiesFilterProps {
  vulnerabilities: IVulnRowAttr[];
  setFilteredVulns: React.Dispatch<IVulnRowAttr[]>;
  setVulnerabilitiesState: React.Dispatch<
    React.SetStateAction<VulnerabilityState | undefined>
  >;
  groupName: string;
}

type TVulnFilter = IFilter<IVulnRowAttr>;

export type { TVulnFilter, IVulnerabilitiesFilterProps };
