import { Button } from "@fluidattacks/design";
import { StrictMode, useCallback } from "react";
import { useTranslation } from "react-i18next";

import { useVulnerabilityStore } from "hooks/use-vulnerability-store";

const HandleAcceptanceButton = ({
  shouldRender,
}: Readonly<{ shouldRender: boolean }>): JSX.Element | null => {
  const { t } = useTranslation();
  const { isAcceptanceModalOpen, setIsAcceptanceModalOpen } =
    useVulnerabilityStore();

  const toggleHandleAcceptanceModal = useCallback((): void => {
    setIsAcceptanceModalOpen(!isAcceptanceModalOpen);
  }, [isAcceptanceModalOpen, setIsAcceptanceModalOpen]);

  return shouldRender ? (
    <StrictMode>
      <Button
        icon={"shield-check"}
        id={"handleAcceptanceButton"}
        mr={0.5}
        onClick={toggleHandleAcceptanceModal}
        tooltip={t("searchFindings.tabVuln.buttonsTooltip.handleAcceptance")}
        variant={"ghost"}
      >
        {t("searchFindings.tabVuln.buttons.handleAcceptance")}
      </Button>
    </StrictMode>
  ) : null;
};

export { HandleAcceptanceButton };
