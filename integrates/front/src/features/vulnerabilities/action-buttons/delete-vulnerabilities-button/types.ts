export interface IDeleteVulnerabilitiesButtonProps {
  full?: boolean;
  shouldRender: boolean;
  areVulnsSelected: boolean;
  onDeleting: () => void;
}
