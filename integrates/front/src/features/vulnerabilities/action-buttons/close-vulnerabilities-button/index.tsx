import {
  Button,
  ComboBox,
  Form,
  type IItem,
  InnerForm,
  Modal,
  useModal,
} from "@fluidattacks/design";
import type { IconName } from "@fortawesome/free-solid-svg-icons";
import { Fragment, useCallback, useMemo } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import type { ICloseVulnerabilitiesButtonProps } from "./types";

import { Authorize } from "components/@core/authorize";
import { VulnerabilityStateReason } from "gql/graphql";
import { useVulnerabilityStore } from "hooks/use-vulnerability-store";

export const CloseVulnerabilitiesButton: React.FC<
  ICloseVulnerabilitiesButtonProps
> = ({ shouldRender, areVulnsSelected, onClosing }): JSX.Element => {
  const { t } = useTranslation();
  const { isClosing, setIsClosing } = useVulnerabilityStore();
  const modalProps = useModal("close-vulnerabilities-modal");

  const onSubmit = useCallback(
    ({ closingReason }: { closingReason: VulnerabilityStateReason }): void => {
      setIsClosing(false);
      modalProps.close();
      onClosing(closingReason);
    },
    [modalProps, onClosing, setIsClosing],
  );

  const toggleOpen = useCallback((): void => {
    modalProps.toggle();
  }, [modalProps]);

  const onCancelClose = useCallback((): void => {
    setIsClosing(!isClosing);
  }, [isClosing, setIsClosing]);

  const { buttonIcon, buttonId, buttonOnClick, buttonText } = useMemo((): {
    buttonIcon: IconName;
    buttonId: string;
    buttonOnClick: () => void;
    buttonText: string;
  } => {
    if (isClosing) {
      return {
        buttonIcon: "times",
        buttonId: "close-cancel",
        buttonOnClick: onCancelClose,
        buttonText: t("searchFindings.tabVuln.buttons.cancel"),
      };
    }

    return {
      buttonIcon: "check",
      buttonId: "close",
      buttonOnClick: toggleOpen,
      buttonText: t("searchFindings.tabVuln.buttons.close.text"),
    };
  }, [isClosing, onCancelClose, t, toggleOpen]);

  const translations = useMemo((): Record<string, string> => {
    return {
      [VulnerabilityStateReason.VerifiedAsSafe]: t(
        "vulnerability.stateReasons.verifiedAsSafe",
      ),
      [VulnerabilityStateReason.RootOrEnvironmentDeactivated]: t(
        "vulnerability.stateReasons.rootOrEnvironmentDeactivated",
      ),
      [VulnerabilityStateReason.Exclusion]: t(
        "vulnerability.stateReasons.exclusion",
      ),
      [VulnerabilityStateReason.NoJustification]: t(
        "vulnerability.stateReasons.noJustification",
      ),
      [VulnerabilityStateReason.FunctionalityNoLongerExists]: t(
        "vulnerability.stateReasons.functionalityNoLongerExists",
      ),
      [VulnerabilityStateReason.RootMovedToAnotherGroup]: t(
        "vulnerability.stateReasons.rootMovedToAnotherGroup",
      ),
    };
  }, [t]);

  const justificationOptions = useMemo(
    (): IItem[] => [
      {
        name: translations[VulnerabilityStateReason.VerifiedAsSafe],
        value: VulnerabilityStateReason.VerifiedAsSafe,
      },
      {
        name: translations[
          VulnerabilityStateReason.RootOrEnvironmentDeactivated
        ],
        value: VulnerabilityStateReason.RootOrEnvironmentDeactivated,
      },
      {
        name: translations[VulnerabilityStateReason.Exclusion],
        value: VulnerabilityStateReason.Exclusion,
      },
      {
        name: translations[
          VulnerabilityStateReason.FunctionalityNoLongerExists
        ],
        value: VulnerabilityStateReason.FunctionalityNoLongerExists,
      },
    ],
    [translations],
  );

  return (
    <Fragment>
      <Authorize
        can={"integrates_api_mutations_close_vulnerabilities_mutate"}
        have={"can_report_vulnerabilities"}
      >
        <Fragment>
          {isClosing ? (
            <Button
              disabled={!areVulnsSelected}
              icon={"check"}
              id={"close"}
              mr={0.5}
              onClick={toggleOpen}
              tooltip={t("searchFindings.tabVuln.buttonsTooltip.close")}
              variant={"ghost"}
            >
              {t("searchFindings.tabVuln.buttons.close.text")}
            </Button>
          ) : undefined}
          {shouldRender ? (
            <Button
              disabled={!areVulnsSelected}
              icon={buttonIcon}
              id={buttonId}
              mr={0.5}
              onClick={buttonOnClick}
              tooltip={
                isClosing
                  ? undefined
                  : t("searchFindings.tabVuln.buttonsTooltip.close")
              }
              variant={"ghost"}
            >
              {buttonText}
            </Button>
          ) : undefined}
        </Fragment>
      </Authorize>
      <Modal
        id={"close-vulnerabilities-modal"}
        modalRef={modalProps}
        size={"sm"}
        title={t("searchFindings.tabVuln.buttons.close.text")}
      >
        <Form
          cancelButton={{ onClick: toggleOpen }}
          defaultValues={{
            closingReason: VulnerabilityStateReason.NoJustification,
          }}
          onSubmit={onSubmit}
        >
          <InnerForm>
            {({ control }): JSX.Element => (
              <ComboBox
                control={control}
                items={justificationOptions}
                label={"Closing reason"}
                name={"closingReason"}
                required={true}
              />
            )}
          </InnerForm>
        </Form>
      </Modal>
    </Fragment>
  );
};
