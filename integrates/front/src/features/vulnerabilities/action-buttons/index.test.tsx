/* eslint-disable jest/no-hooks */
import { PureAbility } from "@casl/ability";
import { act, screen } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";

import { ActionButtons } from ".";
import {
  authzGroupContext,
  authzPermissionsContext,
} from "context/authz/config";
import { useVulnerabilityStore } from "hooks/use-vulnerability-store";
import { render } from "mocks";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgInfo").mockImplementation();

  return mockedNotifications;
});

describe("actionButtons", (): void => {
  const initialStoreState = useVulnerabilityStore.getState();

  beforeEach((): void => {
    useVulnerabilityStore.setState(initialStoreState, true);
  });

  it("should render a component without permissions", (): void => {
    expect.hasAssertions();

    render(
      <authzPermissionsContext.Provider value={new PureAbility([])}>
        <authzGroupContext.Provider
          value={new PureAbility([{ action: "can_report_vulnerabilities" }])}
        >
          <ActionButtons
            allSelectedVulnsWithUpdatableSeverity={true}
            areRejectedVulns={false}
            areRequestedZeroRiskVulns={true}
            areSelectedVulnsManual={true}
            areSubmittedVulns={true}
            areVulnsPendingOfAcceptance={true}
            areVulnsSelected={false}
            isFindingReleased={true}
            isVerified={false}
            onClosing={jest.fn()}
            onDeleting={jest.fn()}
            onRequestReattack={jest.fn()}
            onResubmit={jest.fn()}
            onVerify={jest.fn()}
            status={"VULNERABLE"}
          />
        </authzGroupContext.Provider>
      </authzPermissionsContext.Provider>,
    );

    expect(screen.queryByRole("button")).not.toBeInTheDocument();
  });

  it("should render a component with only the edit button", (): void => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      {
        action:
          "integrates_api_mutations_update_vulnerabilities_treatment_mutate",
      },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <authzGroupContext.Provider
          value={new PureAbility([{ action: "can_report_vulnerabilities" }])}
        >
          <ActionButtons
            areRejectedVulns={false}
            areRequestedZeroRiskVulns={true}
            areSelectedVulnsManual={true}
            areSubmittedVulns={true}
            areVulnerableLocations={true}
            areVulnsPendingOfAcceptance={true}
            areVulnsSelected={false}
            isFindingReleased={true}
            isVerified={false}
            onClosing={jest.fn()}
            onDeleting={jest.fn()}
            onRequestReattack={jest.fn()}
            onResubmit={jest.fn()}
            onVerify={jest.fn()}
            status={"VULNERABLE"}
          />
        </authzGroupContext.Provider>
      </authzPermissionsContext.Provider>,
    );

    expect(
      screen.getByText("searchFindings.tabVuln.buttons.edit"),
    ).toBeInTheDocument();
  });

  it("should render request verification", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      {
        action:
          "integrates_api_mutations_request_vulnerabilities_verification_mutate",
      },
      {
        action:
          "integrates_api_mutations_update_vulnerabilities_severity_mutate",
      },
      {
        action:
          "integrates_api_mutations_update_vulnerabilities_treatment_mutate",
      },
    ]);
    const mockedServices = new PureAbility<string>([
      { action: "is_continuous" },
      { action: "can_report_vulnerabilities" },
    ]);

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <authzGroupContext.Provider value={mockedServices}>
          <ActionButtons
            areRejectedVulns={false}
            areRequestedZeroRiskVulns={true}
            areSelectedVulnsManual={true}
            areSubmittedVulns={true}
            areVulnerableLocations={true}
            areVulnsPendingOfAcceptance={true}
            areVulnsSelected={false}
            isFindingReleased={true}
            isVerified={false}
            onClosing={jest.fn()}
            onDeleting={jest.fn()}
            onRequestReattack={jest.fn()}
            onResubmit={jest.fn()}
            onVerify={jest.fn()}
            status={"VULNERABLE"}
          />
        </authzGroupContext.Provider>
      </authzPermissionsContext.Provider>,
    );

    expect(screen.queryAllByRole("button")).toHaveLength(3);

    expect(
      screen.queryByText("searchFindings.tabVuln.buttons.edit"),
    ).toBeInTheDocument();
    expect(
      screen.queryByText("searchFindings.tabVuln.buttons.selectReattack"),
    ).toBeInTheDocument();
    expect(
      screen.queryByText(
        "searchFindings.tabDescription.updateVulnSeverityButton",
      ),
    ).toBeInTheDocument();

    await userEvent.click(
      screen.getByText("searchFindings.tabVuln.buttons.selectReattack"),
    );

    act((): void => {
      const { isReattacking } = useVulnerabilityStore.getState();

      expect(isReattacking).toBe(true);
    });

    expect(
      screen.getByText("searchFindings.tabVuln.buttons.cancel"),
    ).toBeInTheDocument();
    expect(
      screen.queryByText("searchFindings.tabVuln.buttons.edit"),
    ).not.toBeInTheDocument();

    await userEvent.click(
      screen.getByText("searchFindings.tabVuln.buttons.cancel"),
    );

    expect(
      screen.queryByText("searchFindings.tabDescription.cancelVerify"),
    ).not.toBeInTheDocument();

    act((): void => {
      const { isReattacking } = useVulnerabilityStore.getState();

      expect(isReattacking).toBe(false);
    });
  });

  it("should render update severity", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      {
        action:
          "integrates_api_mutations_update_vulnerabilities_severity_mutate",
      },
    ]);
    const { rerender } = render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <ActionButtons
          allSelectedVulnsWithUpdatableSeverity={true}
          areRequestedZeroRiskVulns={false}
          areSelectedVulnsManual={true}
          areSubmittedVulns={false}
          areVulnsPendingOfAcceptance={false}
          areVulnsSelected={false}
          onClosing={jest.fn()}
          onDeleting={jest.fn()}
          onRequestReattack={jest.fn()}
          onResubmit={jest.fn()}
          onVerify={jest.fn()}
        />
      </authzPermissionsContext.Provider>,
    );

    expect(screen.queryAllByRole("button")).toHaveLength(1);

    const updateSeverityButtonDisabled = screen.getByRole("button", {
      name: "searchFindings.tabDescription.updateVulnSeverityButton",
    });

    expect(updateSeverityButtonDisabled).toBeInTheDocument();
    expect(updateSeverityButtonDisabled).toBeDisabled();

    rerender(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <ActionButtons
          allSelectedVulnsWithUpdatableSeverity={true}
          areRequestedZeroRiskVulns={false}
          areSelectedVulnsManual={true}
          areSubmittedVulns={false}
          areVulnsPendingOfAcceptance={false}
          areVulnsSelected={true}
          onClosing={jest.fn()}
          onDeleting={jest.fn()}
          onRequestReattack={jest.fn()}
          onResubmit={jest.fn()}
          onVerify={jest.fn()}
        />
      </authzPermissionsContext.Provider>,
    );

    const updateSeverityButton = screen.getByRole("button", {
      name: "searchFindings.tabDescription.updateVulnSeverityButton",
    });

    expect(updateSeverityButton).toBeInTheDocument();
    expect(updateSeverityButton).toBeEnabled();

    act((): void => {
      const { isUpdatingSeverity } = useVulnerabilityStore.getState();

      expect(isUpdatingSeverity).toBe(false);
    });

    await userEvent.click(updateSeverityButton);

    act((): void => {
      const { isUpdatingSeverity } = useVulnerabilityStore.getState();

      expect(isUpdatingSeverity).toBe(true);
    });
  });

  it("should render delete vulns", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_remove_vulnerability_mutate" },
    ]);
    const mockedServices = new PureAbility<string>([
      { action: "can_report_vulnerabilities" },
    ]);
    const { rerender } = render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <authzGroupContext.Provider value={mockedServices}>
          <ActionButtons
            areRequestedZeroRiskVulns={false}
            areSelectedVulnsManual={true}
            areSubmittedVulns={true}
            areVulnsPendingOfAcceptance={false}
            areVulnsSelected={false}
            onClosing={jest.fn()}
            onDeleting={jest.fn()}
            onRequestReattack={jest.fn()}
            onResubmit={jest.fn()}
            onVerify={jest.fn()}
          />
        </authzGroupContext.Provider>
      </authzPermissionsContext.Provider>,
    );

    expect(screen.queryAllByRole("button")).toHaveLength(1);

    const getInitialDeleteButton = (): HTMLElement =>
      screen.getByRole("button", {
        name: "searchFindings.tabVuln.buttons.delete.text",
      });

    act((): void => {
      const { isDeleting, isDeleteModalOpen } =
        useVulnerabilityStore.getState();

      expect(isDeleting).toBe(false);
      expect(isDeleteModalOpen).toBe(false);
    });

    expect(getInitialDeleteButton()).toBeInTheDocument();

    await userEvent.click(getInitialDeleteButton());

    const deleteVulnsButton = screen.getByRole("button", {
      name: "searchFindings.tabVuln.buttons.delete.text",
    });

    expect(deleteVulnsButton).toBeInTheDocument();

    expect(deleteVulnsButton).toBeDisabled();

    expect(
      screen.getByText("searchFindings.tabVuln.buttons.cancel"),
    ).toBeInTheDocument();

    rerender(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <authzGroupContext.Provider value={mockedServices}>
          <ActionButtons
            areRequestedZeroRiskVulns={false}
            areSelectedVulnsManual={true}
            areSubmittedVulns={true}
            areVulnsPendingOfAcceptance={false}
            areVulnsSelected={true}
            onClosing={jest.fn()}
            onDeleting={jest.fn()}
            onRequestReattack={jest.fn()}
            onResubmit={jest.fn()}
            onVerify={jest.fn()}
          />
        </authzGroupContext.Provider>
      </authzPermissionsContext.Provider>,
    );

    act((): void => {
      const { isDeleting, isDeleteModalOpen } =
        useVulnerabilityStore.getState();

      expect(isDeleting).toBe(true);
      expect(isDeleteModalOpen).toBe(false);
    });

    expect(deleteVulnsButton).toBeEnabled();

    await userEvent.click(deleteVulnsButton);

    act((): void => {
      const { isDeleting, isDeleteModalOpen } =
        useVulnerabilityStore.getState();

      expect(isDeleting).toBe(true);
      expect(isDeleteModalOpen).toBe(true);
    });

    await userEvent.click(
      screen.getByText("searchFindings.tabVuln.buttons.cancel"),
    );

    act((): void => {
      const { isDeleting } = useVulnerabilityStore.getState();

      expect(isDeleting).toBe(false);
    });

    expect(getInitialDeleteButton()).toBeInTheDocument();
    expect(deleteVulnsButton).not.toBeInTheDocument();
    expect(
      screen.queryByText("searchFindings.tabVuln.buttons.cancel"),
    ).not.toBeInTheDocument();
  });

  it("should render verify", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      {
        action:
          "integrates_api_mutations_verify_vulnerabilities_request_mutate",
      },
    ]);
    const mockedServices = new PureAbility<string>([
      { action: "can_report_vulnerabilities" },
    ]);
    const { rerender } = render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <authzGroupContext.Provider value={mockedServices}>
          <ActionButtons
            areRequestedZeroRiskVulns={false}
            areSelectedVulnsManual={true}
            areSubmittedVulns={false}
            areVulnsPendingOfAcceptance={false}
            areVulnsSelected={false}
            isVerified={true}
            onClosing={jest.fn()}
            onDeleting={jest.fn()}
            onRequestReattack={jest.fn()}
            onResubmit={jest.fn()}
            onVerify={jest.fn()}
          />
        </authzGroupContext.Provider>
      </authzPermissionsContext.Provider>,
    );

    expect(screen.queryAllByRole("button")).toHaveLength(0);

    expect(
      screen.queryByText("searchFindings.tabDescription.markVerified.text"),
    ).not.toBeInTheDocument();

    rerender(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <authzGroupContext.Provider value={mockedServices}>
          <ActionButtons
            areRequestedZeroRiskVulns={false}
            areSelectedVulnsManual={true}
            areSubmittedVulns={false}
            areVulnsPendingOfAcceptance={false}
            areVulnsSelected={false}
            onClosing={jest.fn()}
            onDeleting={jest.fn()}
            onRequestReattack={jest.fn()}
            onResubmit={jest.fn()}
            onVerify={jest.fn()}
          />
        </authzGroupContext.Provider>
      </authzPermissionsContext.Provider>,
    );

    expect(screen.queryAllByRole("button")).toHaveLength(1);

    const getInitialVerifyButton = (): HTMLElement =>
      screen.getByRole("button", {
        name: "searchFindings.tabDescription.markVerified.text",
      });

    expect(getInitialVerifyButton()).toBeInTheDocument();

    act((): void => {
      const { isVerifying, isVerifyModalOpen } =
        useVulnerabilityStore.getState();

      expect(isVerifying).toBe(false);
      expect(isVerifyModalOpen).toBe(false);
    });

    await userEvent.click(getInitialVerifyButton());

    const verifyVulnsButton = screen.getByRole("button", {
      name: "searchFindings.tabDescription.markVerified.text",
    });

    expect(verifyVulnsButton).toBeInTheDocument();
    expect(verifyVulnsButton).toBeDisabled();

    expect(
      screen.getByText("searchFindings.tabDescription.cancelVerified"),
    ).toBeInTheDocument();

    rerender(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <authzGroupContext.Provider value={mockedServices}>
          <ActionButtons
            areRequestedZeroRiskVulns={false}
            areSelectedVulnsManual={true}
            areSubmittedVulns={false}
            areVulnsPendingOfAcceptance={false}
            areVulnsSelected={true}
            onClosing={jest.fn()}
            onDeleting={jest.fn()}
            onRequestReattack={jest.fn()}
            onResubmit={jest.fn()}
            onVerify={jest.fn()}
          />
        </authzGroupContext.Provider>
      </authzPermissionsContext.Provider>,
    );

    act((): void => {
      const { isVerifying, isVerifyModalOpen } =
        useVulnerabilityStore.getState();

      expect(isVerifying).toBe(true);
      expect(isVerifyModalOpen).toBe(false);
    });

    expect(verifyVulnsButton).toBeInTheDocument();
    expect(verifyVulnsButton).toBeEnabled();

    await userEvent.click(verifyVulnsButton);

    act((): void => {
      const { isVerifying, isVerifyModalOpen } =
        useVulnerabilityStore.getState();

      expect(isVerifying).toBe(true);
      expect(isVerifyModalOpen).toBe(true);
    });

    await userEvent.click(
      screen.getByText("searchFindings.tabDescription.cancelVerified"),
    );

    act((): void => {
      const { isVerifying } = useVulnerabilityStore.getState();

      expect(isVerifying).toBe(false);
    });

    expect(getInitialVerifyButton()).toBeInTheDocument();
    expect(verifyVulnsButton).not.toBeInTheDocument();
    expect(
      screen.queryByText("searchFindings.tabVuln.buttons.cancel"),
    ).not.toBeInTheDocument();
  });

  it("should render close", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_close_vulnerabilities_mutate" },
    ]);
    const mockedServices = new PureAbility<string>([
      { action: "can_report_vulnerabilities" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <authzGroupContext.Provider value={mockedServices}>
          <ActionButtons
            areRequestedZeroRiskVulns={false}
            areSelectedVulnsManual={true}
            areSubmittedVulns={false}
            areVulnerableLocations={true}
            areVulnsPendingOfAcceptance={false}
            areVulnsSelected={true}
            isVerified={false}
            onClosing={jest.fn()}
            onDeleting={jest.fn()}
            onRequestReattack={jest.fn()}
            onResubmit={jest.fn()}
            onVerify={jest.fn()}
          />
        </authzGroupContext.Provider>
      </authzPermissionsContext.Provider>,
    );

    expect(screen.queryAllByRole("button")).toHaveLength(1);

    const getInitialCloseButton = screen.getByRole("button", {
      name: "searchFindings.tabVuln.buttons.close.text",
    });

    expect(getInitialCloseButton).toBeInTheDocument();

    const { isClosing } = useVulnerabilityStore.getState();

    expect(isClosing).toBe(false);

    await userEvent.click(getInitialCloseButton);

    expect(screen.getByText("Confirm")).toBeInTheDocument();

    const cancelBtn = screen.getByText("Cancel");
    await userEvent.click(cancelBtn);

    expect(screen.queryByText("Confirm")).not.toBeInTheDocument();

    expect(getInitialCloseButton).toBeInTheDocument();
  });
});
