import { useAbility } from "@casl/react";
import sum from "lodash/sum";
import { useContext } from "react";

import type { IPermissions } from "./types";

import {
  authzGroupContext,
  authzPermissionsContext,
} from "context/authz/config";
import { useVulnerabilityStore } from "hooks/use-vulnerability-store";

const totalActionsButtons = (permissions: boolean[]): number =>
  sum(permissions.map((permission): number => (permission ? 1 : 0)));

const notifyButton = (
  canNotifyVulnerability: boolean,
  isFindingReleased: boolean,
  status: IPermissions["status"],
): boolean =>
  isFindingReleased && canNotifyVulnerability && status === "VULNERABLE";

const verifyButton = (
  isVerified: boolean,
  isEditing: boolean,
  isReattacking: boolean,
  isResubmitting: boolean,
  isClosing: boolean,
  isDeleting: boolean,
  canVerifyVulns: boolean,
): boolean =>
  canVerifyVulns &&
  !isVerified &&
  !(isEditing || isReattacking || isResubmitting || isClosing || isDeleting);

const resubmitButton = (
  areRejectedVulns: boolean,
  isEditing: boolean,
  isReattacking: boolean,
  isVerifying: boolean,
  isClosing: boolean,
  isDeleting: boolean,
): boolean =>
  areRejectedVulns &&
  !(isEditing || isReattacking || isVerifying || isClosing || isDeleting);

const closeButton = (
  areVulnerableLocations: boolean,
  isEditing: boolean,
  isReattacking: boolean,
  isVerifying: boolean,
  isResubmitting: boolean,
  isDeleting: boolean,
): boolean =>
  areVulnerableLocations &&
  !(isEditing || isReattacking || isVerifying || isResubmitting || isDeleting);

const reattackButton = (
  isFindingReleased: boolean,
  status: IPermissions["status"],
  isEditing: boolean,
  isVerifying: boolean,
  isResubmitting: boolean,
  isClosing: boolean,
  isDeleting: boolean,
): boolean =>
  isFindingReleased &&
  status === "VULNERABLE" &&
  !(isEditing || isVerifying || isResubmitting || isClosing || isDeleting);

const deleteButton = (
  areDeletableLocations: boolean,
  isEditing: boolean,
  isReattacking: boolean,
  isVerifying: boolean,
  isResubmitting: boolean,
  isClosing: boolean,
  canDeleteVulns: boolean,
): boolean =>
  canDeleteVulns &&
  areDeletableLocations &&
  !(isEditing || isReattacking || isVerifying || isResubmitting || isClosing);

const editButton = (
  isFindingReleased: boolean,
  isReattacking: boolean,
  isVerifying: boolean,
  isResubmitting: boolean,
  isClosing: boolean,
  isDeleting: boolean,
  vulnTreatment: boolean,
): boolean =>
  isFindingReleased &&
  !(
    isReattacking ||
    isVerifying ||
    isResubmitting ||
    isClosing ||
    isDeleting
  ) &&
  vulnTreatment;

const manageVulnsDropdownButtons = (
  vulnsStore: {
    isClosing: boolean;
    isDeleting: boolean;
    isEditing: boolean;
    isReattacking: boolean;
    isResubmitting: boolean;
    isVerifying: boolean;
  },
  canUpdateVulns: boolean,
  isVerified: boolean,
  areRejectedVulns: boolean,
  canVerifyVulns: boolean,
  areDeletableLocations: boolean,
  canDeleteVuln: boolean,
): {
  shouldRenderVerifyBtn: boolean;
  shouldRenderResubmitBtn: boolean;
  shouldRenderDeleteBtn: boolean;
} => {
  const {
    isClosing,
    isDeleting,
    isEditing,
    isReattacking,
    isResubmitting,
    isVerifying,
  } = vulnsStore;

  const shouldRenderVerifyBtn = verifyButton(
    isVerified,
    isEditing,
    isReattacking,
    isResubmitting,
    isClosing,
    isDeleting,
    canVerifyVulns && canUpdateVulns,
  );

  const shouldRenderResubmitBtn = resubmitButton(
    areRejectedVulns,
    isEditing,
    isReattacking,
    isVerifying,
    isClosing,
    isDeleting,
  );

  const shouldRenderDeleteBtn = deleteButton(
    areDeletableLocations || areRejectedVulns,
    isEditing,
    isReattacking,
    isVerifying,
    isResubmitting,
    isClosing,
    canDeleteVuln && canUpdateVulns,
  );

  return {
    shouldRenderDeleteBtn,
    shouldRenderResubmitBtn,
    shouldRenderVerifyBtn,
  };
};

const useVulnerabilitiesPermissions = ({
  areRejectedVulns,
  areRequestedZeroRiskVulns,
  areSubmittedVulns,
  areVulnerableLocations,
  areVulnsPendingOfAcceptance,
  isFindingReleased,
  isVerified,
  status,
}: IPermissions): [
  {
    shouldRenderCloseBtn: boolean;
    shouldRenderDeleteBtn: boolean;
    shouldRenderEditBtn: boolean;
    shouldRenderAcceptanceBtn: boolean;
    shouldRenderNotifyBtn: boolean;
    shouldRenderRequestVerifyBtn: boolean;
    shouldRenderResubmitBtn: boolean;
    shouldRenderUpSeverityBtn: boolean;
    shouldRenderVerifyBtn: boolean;
  },
  actionsAllowed: number,
] => {
  const {
    isClosing,
    isDeleting,
    isEditing,
    isReattacking,
    isResubmitting,
    isVerifying,
  } = useVulnerabilityStore();

  const permissions = useAbility(authzPermissionsContext);
  const attributes = useContext(authzGroupContext);
  const canHandleVulnsAcceptance = permissions.can(
    "integrates_api_mutations_handle_vulnerabilities_acceptance_mutate",
  );
  const canConfirmZeroRiskVuln: boolean =
    permissions.can(
      "integrates_api_mutations_confirm_vulnerabilities_zero_risk_mutate",
    ) && attributes.can("can_request_zero_risk");
  const canRejectZeroRiskVuln: boolean =
    permissions.can(
      "integrates_api_mutations_reject_vulnerabilities_zero_risk_mutate",
    ) && attributes.can("can_request_zero_risk");
  const canConfirmVulnerabilities = permissions.can(
    "integrates_api_mutations_confirm_vulnerabilities_mutate",
  );
  const canRejectVulnerabilities = permissions.can(
    "integrates_api_mutations_reject_vulnerabilities_mutate",
  );
  const canUpdateVulns = attributes.can("can_report_vulnerabilities");
  const canNotifyVulnerability = permissions.can(
    "integrates_api_mutations_send_vulnerability_notification_mutate",
  );
  const canRequestZeroRiskVuln = permissions.can(
    "integrates_api_mutations_request_vulnerabilities_zero_risk_mutate",
  );
  const canUpdateVulnsTreatment = permissions.can(
    "integrates_api_mutations_update_vulnerabilities_treatment_mutate",
  );
  const canVerifyVulns = permissions.can(
    "integrates_api_mutations_verify_vulnerabilities_request_mutate",
  );
  const canDeleteVuln = permissions.can(
    "integrates_api_mutations_remove_vulnerability_mutate",
  );

  // Check buttons render
  const shouldRenderUpSeverityBtn = permissions.can(
    "integrates_api_mutations_update_vulnerabilities_severity_mutate",
  );
  const shouldRenderNotifyBtn = notifyButton(
    canNotifyVulnerability,
    isFindingReleased,
    status,
  );

  //Handle "Manage vulnerabilities" dropdown buttons
  const {
    shouldRenderDeleteBtn,
    shouldRenderResubmitBtn,
    shouldRenderVerifyBtn,
  } = manageVulnsDropdownButtons(
    {
      isClosing,
      isDeleting,
      isEditing,
      isReattacking,
      isResubmitting,
      isVerifying,
    },
    canUpdateVulns,
    isVerified,
    areRejectedVulns,
    canVerifyVulns,
    areVulnerableLocations || areSubmittedVulns,
    canDeleteVuln,
  );

  const shouldRenderCloseBtn = closeButton(
    areVulnerableLocations,
    isEditing,
    isReattacking,
    isVerifying,
    isResubmitting,
    isDeleting,
  );
  const shouldRenderRequestVerifyBtn = reattackButton(
    isFindingReleased,
    status,
    isEditing,
    isVerifying,
    isResubmitting,
    isClosing,
    isDeleting,
  );

  const shouldRenderEditBtn = editButton(
    isFindingReleased,
    isReattacking,
    isVerifying,
    isResubmitting,
    isClosing,
    isDeleting,
    canRequestZeroRiskVuln || canUpdateVulnsTreatment,
  );

  const shouldRenderAcceptanceBtn =
    ((canHandleVulnsAcceptance && areVulnsPendingOfAcceptance) ||
      ((canConfirmZeroRiskVuln || canRejectZeroRiskVuln) &&
        areRequestedZeroRiskVulns) ||
      ((canConfirmVulnerabilities || canRejectVulnerabilities) &&
        canUpdateVulns &&
        areSubmittedVulns)) &&
    !(
      isEditing ||
      isReattacking ||
      isVerifying ||
      isResubmitting ||
      isDeleting ||
      isClosing
    );

  return [
    {
      shouldRenderAcceptanceBtn,
      shouldRenderCloseBtn,
      shouldRenderDeleteBtn,
      shouldRenderEditBtn,
      shouldRenderNotifyBtn,
      shouldRenderRequestVerifyBtn,
      shouldRenderResubmitBtn,
      shouldRenderUpSeverityBtn,
      shouldRenderVerifyBtn,
    },
    totalActionsButtons([
      shouldRenderCloseBtn,
      shouldRenderDeleteBtn,
      shouldRenderEditBtn,
      shouldRenderAcceptanceBtn,
      shouldRenderNotifyBtn,
      shouldRenderRequestVerifyBtn,
      shouldRenderResubmitBtn,
      shouldRenderUpSeverityBtn,
      shouldRenderVerifyBtn,
    ]),
  ];
};

export { useVulnerabilitiesPermissions };
