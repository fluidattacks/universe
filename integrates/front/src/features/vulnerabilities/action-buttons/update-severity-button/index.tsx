import { Button } from "@fluidattacks/design";
import { useCallback } from "react";
import { useTranslation } from "react-i18next";

import { useVulnerabilityStore } from "hooks/use-vulnerability-store";

interface IUpdateSeverityButtonProps {
  readonly full?: boolean;
  readonly isDisabled: boolean;
  readonly shouldRender: boolean;
}

const UpdateSeverityButton = ({
  full = false,
  isDisabled,
  shouldRender,
}: IUpdateSeverityButtonProps): JSX.Element | undefined => {
  const { t } = useTranslation();
  const { isUpdatingSeverity, setIsUpdatingSeverity } = useVulnerabilityStore();

  const toggleUpdateSeverity = useCallback((): void => {
    setIsUpdatingSeverity(!isUpdatingSeverity);
  }, [isUpdatingSeverity, setIsUpdatingSeverity]);

  return shouldRender ? (
    <Button
      disabled={isDisabled}
      icon={"ruler"}
      id={"vulnerabilities-update-severity"}
      mr={0.5}
      onClick={toggleUpdateSeverity}
      tooltip={t("searchFindings.tabDescription.updateVulnSeverityTooltip")}
      variant={"ghost"}
      width={full ? "100%" : undefined}
    >
      {t("searchFindings.tabDescription.updateVulnSeverityButton")}
    </Button>
  ) : undefined;
};

export { UpdateSeverityButton };
