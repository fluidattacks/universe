import {
  Button,
  ButtonToolbarRow,
  Container,
  ListItemsWrapper,
} from "@fluidattacks/design";
import { useCallback, useRef, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { CloseVulnerabilitiesButton } from "./close-vulnerabilities-button";
import { DeleteVulnerabilitiesButton } from "./delete-vulnerabilities-button";
import { EditButton } from "./edit-button";
import { HandleAcceptanceButton } from "./handle-acceptance-button";
import { useVulnerabilitiesPermissions } from "./hooks";
import { NotifyButton } from "./notify-button";
import { ReattackVulnButton } from "./reattack-vuln-button";
import { ResubmitVulnerabilitiesButton } from "./resubmit-vulnerabilities-button";
import type { IActionButtonsProps } from "./types";
import { UpdateSeverityButton } from "./update-severity-button";
import { VerifyVulnerabilitiesButton } from "./verify-vulnerabilities-button";

import { useClickOutside } from "hooks/use-click-outside";
import { useVulnerabilityStore } from "hooks/use-vulnerability-store";

const ActionButtons: React.FC<IActionButtonsProps> = ({
  allVulnsSelectedAreSafe = false,
  allSelectedVulnsWithUpdatableSeverity = false,
  areVulnerableLocations = false,
  areRejectedVulns = false,
  areVulnsSelected,
  areVulnsPendingOfAcceptance,
  areRequestedZeroRiskVulns,
  areSelectedVulnsManual,
  areSubmittedVulns,
  isFindingReleased = true,
  isVerified = false,
  status = "VULNERABLE",
  onClosing,
  onDeleting,
  onRequestReattack,
  onResubmit,
  onVerify,
}: Readonly<IActionButtonsProps>): JSX.Element => {
  const { t } = useTranslation();
  const { isClosing } = useVulnerabilityStore();
  const [open, setOpen] = useState(false);
  const additionalRef = useRef<HTMLDivElement>(null);
  const [
    {
      shouldRenderAcceptanceBtn,
      shouldRenderNotifyBtn,
      shouldRenderVerifyBtn,
      shouldRenderResubmitBtn,
      shouldRenderCloseBtn,
      shouldRenderRequestVerifyBtn,
      shouldRenderDeleteBtn,
      shouldRenderEditBtn,
      shouldRenderUpSeverityBtn,
    },
    numButtons,
  ] = useVulnerabilitiesPermissions({
    areRejectedVulns,
    areRequestedZeroRiskVulns,
    areSubmittedVulns,
    areVulnerableLocations,
    areVulnsPendingOfAcceptance,
    isFindingReleased,
    isVerified,
    status,
  });

  const openDropdown = useCallback((): void => {
    setOpen((previousState): boolean => !previousState);
  }, []);

  useClickOutside(
    additionalRef.current,
    (): void => {
      setOpen(false);
    },
    true,
  );

  const hasActions =
    shouldRenderUpSeverityBtn ||
    shouldRenderVerifyBtn ||
    shouldRenderResubmitBtn ||
    shouldRenderDeleteBtn;

  const dropdownOptions = (
    <ListItemsWrapper>
      <Container display={"flex"} flexDirection={"column"}>
        <UpdateSeverityButton
          full={true}
          isDisabled={
            !areVulnsSelected || !allSelectedVulnsWithUpdatableSeverity
          }
          shouldRender={shouldRenderUpSeverityBtn}
        />
        <VerifyVulnerabilitiesButton
          areVulnsSelected={areVulnsSelected}
          full={true}
          onVerify={onVerify}
          shouldRender={shouldRenderVerifyBtn}
        />
        <ResubmitVulnerabilitiesButton
          areVulnsSelected={areVulnsSelected}
          full={true}
          onResubmit={onResubmit}
          shouldRender={shouldRenderResubmitBtn}
        />
        <DeleteVulnerabilitiesButton
          areVulnsSelected={areVulnsSelected}
          full={true}
          onDeleting={onDeleting}
          shouldRender={shouldRenderDeleteBtn}
        />
      </Container>
    </ListItemsWrapper>
  );

  return numButtons <= 4 && !isClosing ? (
    <ButtonToolbarRow>
      <HandleAcceptanceButton shouldRender={shouldRenderAcceptanceBtn} />
      <NotifyButton shouldRender={shouldRenderNotifyBtn} />
      <VerifyVulnerabilitiesButton
        areVulnsSelected={areVulnsSelected}
        onVerify={onVerify}
        shouldRender={shouldRenderVerifyBtn}
      />
      <ResubmitVulnerabilitiesButton
        areVulnsSelected={areVulnsSelected}
        onResubmit={onResubmit}
        shouldRender={shouldRenderResubmitBtn}
      />
      <CloseVulnerabilitiesButton
        areVulnsSelected={areVulnsSelected}
        onClosing={onClosing}
        shouldRender={shouldRenderCloseBtn}
      />
      <ReattackVulnButton
        areSelectedVulnsManual={areSelectedVulnsManual}
        areVulnsSelected={areVulnsSelected}
        onRequestReattack={onRequestReattack}
        shouldRender={shouldRenderRequestVerifyBtn}
      />
      <UpdateSeverityButton
        isDisabled={!areVulnsSelected || !allSelectedVulnsWithUpdatableSeverity}
        shouldRender={shouldRenderUpSeverityBtn}
      />
      <DeleteVulnerabilitiesButton
        areVulnsSelected={areVulnsSelected}
        onDeleting={onDeleting}
        shouldRender={shouldRenderDeleteBtn}
      />
      <EditButton
        isDisabled={!areVulnsSelected || allVulnsSelectedAreSafe}
        shouldRender={shouldRenderEditBtn}
      />
    </ButtonToolbarRow>
  ) : (
    <ButtonToolbarRow>
      {hasActions ? (
        <Container position={"relative"} ref={additionalRef}>
          <Button
            icon={"gear"}
            id={"manage-vulns"}
            onClick={openDropdown}
            variant={"ghost"}
          >
            {t("searchFindings.tabVuln.additionalOptions")}
          </Button>
          <Container position={"absolute"} zIndex={2}>
            {open ? dropdownOptions : undefined}
          </Container>
        </Container>
      ) : undefined}
      <HandleAcceptanceButton shouldRender={shouldRenderAcceptanceBtn} />
      <NotifyButton shouldRender={shouldRenderNotifyBtn} />
      <CloseVulnerabilitiesButton
        areVulnsSelected={areVulnsSelected}
        onClosing={onClosing}
        shouldRender={shouldRenderCloseBtn}
      />
      <ReattackVulnButton
        areSelectedVulnsManual={areSelectedVulnsManual}
        areVulnsSelected={areVulnsSelected}
        onRequestReattack={onRequestReattack}
        shouldRender={shouldRenderRequestVerifyBtn}
      />
      <EditButton
        isDisabled={!areVulnsSelected || allVulnsSelectedAreSafe}
        shouldRender={shouldRenderEditBtn}
      />
    </ButtonToolbarRow>
  );
};

export type { IActionButtonsProps };
export { ActionButtons };
