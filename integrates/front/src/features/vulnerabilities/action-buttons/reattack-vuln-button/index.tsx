import { Button } from "@fluidattacks/design";
import type { IconName } from "@fortawesome/free-solid-svg-icons";
import { Fragment, useCallback, useContext } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { Authorize } from "components/@core/authorize";
import { authzGroupContext } from "context/authz/config";
import { useVulnerabilityStore } from "hooks/use-vulnerability-store";
import { msgWarning } from "utils/notifications";

interface IReattackVulnButtonProps {
  readonly areSelectedVulnsManual: boolean;
  readonly areVulnsSelected: boolean;
  readonly shouldRender: boolean;
  readonly onRequestReattack: () => void;
}

const ReattackVulnButton: React.FC<IReattackVulnButtonProps> = ({
  areSelectedVulnsManual,
  areVulnsSelected,
  shouldRender,
  onRequestReattack,
}): JSX.Element => {
  const { t } = useTranslation();
  const attributes = useContext(authzGroupContext);
  const isContinuous = attributes.can("is_continuous");
  const hasAdvanced = attributes.can("has_advanced");
  const canReattack = hasAdvanced ? true : !areSelectedVulnsManual;

  const { isReattacking, setIsReattacking, setIsVerifyModalOpen } =
    useVulnerabilityStore();

  const toggleModal = useCallback((): void => {
    setIsVerifyModalOpen(true);
  }, [setIsVerifyModalOpen]);

  const onRequestingReattack = useCallback((): void => {
    setIsReattacking(!isReattacking);
    if (!isReattacking) {
      onRequestReattack();
    }
  }, [isReattacking, onRequestReattack, setIsReattacking]);

  const buttonAttributes = useCallback((): {
    buttonIcon: IconName;
    buttonId: string;
    buttonText: string;
    buttonTooltip?: string;
  } => {
    if (isReattacking) {
      return {
        buttonIcon: "times",
        buttonId: "reattack-cancel",
        buttonText: t("searchFindings.tabVuln.buttons.cancel"),
      };
    }

    return {
      buttonIcon: "rotate",
      buttonId: "reattack",
      buttonText: t("searchFindings.tabVuln.buttons.selectReattack"),
      buttonTooltip: t("searchFindings.tabVuln.buttons.selectReattackTooltip"),
    };
  }, [isReattacking, t]);

  if (!canReattack && isReattacking) {
    msgWarning(t("searchFindings.tabVuln.errors.badPlan"));
  }

  const { buttonIcon, buttonId, buttonText, buttonTooltip } =
    buttonAttributes();

  return (
    <Authorize
      can={
        "integrates_api_mutations_request_vulnerabilities_verification_mutate"
      }
      extraCondition={isContinuous}
      have={"can_report_vulnerabilities"}
    >
      <Fragment>
        {isReattacking ? (
          <Button
            disabled={!areVulnsSelected || !canReattack}
            icon={"rotate"}
            id={"confirm-reattack"}
            mr={0.5}
            onClick={toggleModal}
            tooltip={
              canReattack
                ? t("searchFindings.tabDescription.requestVerify.tooltip")
                : t(
                    "searchFindings.tabDescription.requestVerify.tooltipWarning",
                  )
            }
            variant={"ghost"}
          >
            {t("searchFindings.tabVuln.buttons.reattack")}
          </Button>
        ) : undefined}
        {shouldRender ? (
          <Button
            icon={buttonIcon}
            id={buttonId}
            mr={0.5}
            onClick={onRequestingReattack}
            tooltip={buttonTooltip}
            variant={"ghost"}
          >
            {buttonText}
          </Button>
        ) : undefined}
      </Fragment>
    </Authorize>
  );
};

export type { IReattackVulnButtonProps };
export { ReattackVulnButton };
