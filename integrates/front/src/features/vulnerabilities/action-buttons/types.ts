import type { VulnerabilityStateReason } from "gql/graphql";

interface IHandleAcceptanceButtonProps {
  areVulnsPendingOfAcceptance: boolean;
  areRequestedZeroRiskVulns: boolean;
  areSubmittedVulns: boolean;
}

interface IActionButtonsProps {
  allVulnsSelectedAreSafe?: boolean;
  allSelectedVulnsWithUpdatableSeverity?: boolean;
  areSelectedVulnsManual: boolean;
  areVulnerableLocations?: boolean;
  areVulnsSelected: boolean;
  areVulnsPendingOfAcceptance: boolean;
  areRejectedVulns?: boolean;
  areRequestedZeroRiskVulns: boolean;
  areSubmittedVulns: boolean;
  isFindingReleased?: boolean;
  isVerified?: boolean;
  status?: "SAFE" | "VULNERABLE";
  onClosing: (justification: VulnerabilityStateReason) => void;
  onCancel?: () => void;
  onDeleting: () => void;
  onRequestReattack: () => void;
  onResubmit: () => void;
  onVerify: () => void;
}

interface IPermissions extends IHandleAcceptanceButtonProps {
  areRejectedVulns: boolean;
  areVulnerableLocations: boolean;
  isFindingReleased: boolean;
  isVerified: boolean;
  status: "SAFE" | "VULNERABLE";
}

export type { IActionButtonsProps, IPermissions };
