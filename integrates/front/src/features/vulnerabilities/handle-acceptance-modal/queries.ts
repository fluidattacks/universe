import { graphql } from "gql";

const ADD_FINDING_CONSULT = graphql(`
  mutation AddFindingConsult(
    $content: String!
    $findingId: String!
    $parentComment: GenericScalar!
    $type: FindingConsultType!
  ) {
    addFindingConsult(
      content: $content
      findingId: $findingId
      parentComment: $parentComment
      type: $type
    ) {
      commentId
      success
    }
  }
`);

const HANDLE_VULNS_ACCEPTANCE = graphql(`
  mutation HandleVulnerabilitiesAcceptance(
    $acceptedVulnerabilities: [String]!
    $findingId: String!
    $justification: String!
    $rejectedVulnerabilities: [String]!
  ) {
    handleVulnerabilitiesAcceptance(
      acceptedVulnerabilities: $acceptedVulnerabilities
      findingId: $findingId
      justification: $justification
      rejectedVulnerabilities: $rejectedVulnerabilities
    ) {
      success
    }
  }
`);

const CONFIRM_VULNERABILITIES = graphql(`
  mutation ConfirmVulnerabilities(
    $findingId: String!
    $vulnerabilities: [String!]!
  ) {
    confirmVulnerabilities(
      findingId: $findingId
      vulnerabilities: $vulnerabilities
    ) {
      success
    }
  }
`);

const CONFIRM_VULNERABILITIES_ZERO_RISK = graphql(`
  mutation ConfirmVulnerabilitiesZeroRisk(
    $findingId: String!
    $justification: String!
    $vulnerabilities: [String]!
  ) {
    confirmVulnerabilitiesZeroRisk(
      findingId: $findingId
      justification: $justification
      vulnerabilities: $vulnerabilities
    ) {
      success
    }
  }
`);

const REJECT_VULNERABILITIES = graphql(`
  mutation RejectVulnerabilities(
    $findingId: String!
    $otherReason: String
    $reasons: [VulnerabilityRejectionReason!]!
    $vulnerabilities: [String!]!
  ) {
    rejectVulnerabilities(
      findingId: $findingId
      otherReason: $otherReason
      reasons: $reasons
      vulnerabilities: $vulnerabilities
    ) {
      success
    }
  }
`);

const REJECT_VULNERABILITIES_ZERO_RISK = graphql(`
  mutation RejectVulnerabilitiesZeroRisk(
    $findingId: String!
    $justification: String!
    $vulnerabilities: [String]!
  ) {
    rejectVulnerabilitiesZeroRisk(
      findingId: $findingId
      justification: $justification
      vulnerabilities: $vulnerabilities
    ) {
      success
    }
  }
`);

export {
  ADD_FINDING_CONSULT,
  CONFIRM_VULNERABILITIES,
  CONFIRM_VULNERABILITIES_ZERO_RISK,
  HANDLE_VULNS_ACCEPTANCE,
  REJECT_VULNERABILITIES,
  REJECT_VULNERABILITIES_ZERO_RISK,
};
