import { Container, TableButton } from "@fluidattacks/design";
import { useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import type { IVulnDataAttr } from "../../../types";

interface IChangeZeroRiskFormatterProps {
  readonly row: IVulnDataAttr;
  readonly approveFunction: (arg1?: IVulnDataAttr) => void;
  readonly deleteFunction: (arg1?: IVulnDataAttr) => void;
}
const ChangeZeroRiskFormatter: React.FC<IChangeZeroRiskFormatterProps> = ({
  row,
  approveFunction,
  deleteFunction,
}: IChangeZeroRiskFormatterProps): JSX.Element => {
  const { t } = useTranslation();
  const handleOnApprove = useCallback((): void => {
    approveFunction(row);
  }, [approveFunction, row]);

  const handleOnDelete = useCallback((): void => {
    deleteFunction(row);
  }, [deleteFunction, row]);

  return (
    <React.StrictMode>
      <Container display={"flex"} gap={0.25}>
        <TableButton
          name={t(
            "searchFindings.tabVuln.handleAcceptanceModal.submittedForm.submittedTable.confirm",
          )}
          onClick={handleOnApprove}
          variant={"success"}
        />
        <TableButton
          name={t(
            "searchFindings.tabVuln.handleAcceptanceModal.submittedForm.submittedTable.reject",
          )}
          onClick={handleOnDelete}
          variant={"reject"}
        />
      </Container>
    </React.StrictMode>
  );
};

export const changeZeroRiskFormatter = (
  row: IVulnDataAttr,
  approveFunction: (arg1?: IVulnDataAttr) => void,
  deleteFunction: (arg1?: IVulnDataAttr) => void,
): JSX.Element => {
  return (
    <ChangeZeroRiskFormatter
      approveFunction={approveFunction}
      deleteFunction={deleteFunction}
      row={row}
    />
  );
};
