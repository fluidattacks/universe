/* eslint-disable jest/no-hooks */
import { PureAbility } from "@casl/ability";
import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import _ from "lodash";
import { HttpResponse, type StrictResponse, graphql } from "msw";

import {
  CONFIRM_VULNERABILITIES,
  CONFIRM_VULNERABILITIES_ZERO_RISK,
  HANDLE_VULNS_ACCEPTANCE,
  REJECT_VULNERABILITIES,
  REJECT_VULNERABILITIES_ZERO_RISK,
} from "./queries";

import { HandleAcceptanceModal } from ".";
import {
  authzGroupContext,
  authzPermissionsContext,
} from "context/authz/config";
import type { IVulnRowAttr } from "features/vulnerabilities/types";
import {
  type ConfirmVulnerabilitiesMutation as ConfirmVulnerabilities,
  type ConfirmVulnerabilitiesZeroRiskMutation as ConfirmVulnerabilitiesZeroRisk,
  type HandleVulnerabilitiesAcceptanceMutation as HandleVulnerabilitiesAcceptance,
  type RejectVulnerabilitiesMutation as RejectVulnerabilities,
  type RejectVulnerabilitiesZeroRiskMutation as RejectVulnerabilitiesZeroRisk,
  RootCriticality,
} from "gql/graphql";
import { useVulnerabilityStore } from "hooks/use-vulnerability-store";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";
import { msgError, msgSuccess } from "utils/notifications";
import { getByValue, selectOption } from "utils/test-utils";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

describe("handle vulns acceptance modal", (): void => {
  const graphqlMocked = graphql.link(LINK);
  const memoryRouter = {
    initialEntries: ["/orgs/okada"],
  };
  const initialStoreState = useVulnerabilityStore.getState();

  beforeEach((): void => {
    useVulnerabilityStore.setState(initialStoreState, true);
    useVulnerabilityStore.setState({ isAcceptanceModalOpen: true });
  });

  it("should handle vulns acceptance", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const handleOnClose: jest.Mock = jest.fn();
    const handleRefetchData: jest.Mock = jest.fn();
    const mockedPermissions = new PureAbility<string>([
      {
        action:
          "integrates_api_mutations_handle_vulnerabilities_acceptance_mutate",
      },
    ]);
    const mocks = [
      graphqlMocked.mutation(
        HANDLE_VULNS_ACCEPTANCE,
        ({
          variables,
        }): StrictResponse<
          IErrorMessage | { data: HandleVulnerabilitiesAcceptance }
        > => {
          const {
            acceptedVulnerabilities,
            findingId,
            justification,
            rejectedVulnerabilities,
          } = variables;
          if (
            _.isEqual(acceptedVulnerabilities, []) &&
            findingId === "1" &&
            justification === "This is a justification test" &&
            _.isEqual(rejectedVulnerabilities, ["test"])
          ) {
            return HttpResponse.json({
              data: {
                handleVulnerabilitiesAcceptance: { success: true },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error handling vulnerabilities")],
          });
        },
      ),
    ];
    const mokedVulns: IVulnRowAttr[] = [
      {
        advisories: null,
        assigned: "",
        closingDate: null,
        customSeverity: 3,
        externalBugTrackingSystem: null,
        findingId: "1",
        groupName: "test",
        historicTreatment: [
          {
            acceptanceDate: "",
            acceptanceStatus: "SUBMITTED",
            assigned: "assigned-user-1",
            date: "2019-07-05 09:56:40",
            justification: "test justification",
            treatment: "ACCEPTED_UNDEFINED",
            user: "user@test.com",
          },
        ],
        id: "test",
        lastEditedBy: "usertreatment@test.test",
        lastStateDate: "2019-07-05 09:56:40",
        lastTreatmentDate: "2019-07-05 09:56:40",
        lastVerificationDate: null,
        organizationName: undefined,
        priority: 0,
        proposedSeverityAuthor: null,
        proposedSeverityTemporalScore: null,
        proposedSeverityThreatScore: null,
        proposedSeverityVector: null,
        proposedSeverityVectorV4: null,
        remediated: true,
        reportDate: "",
        root: {
          __typename: "GitRoot",
          branch: "master",
          criticality: RootCriticality.Low,
        },
        rootNickname: "https:",
        severityTemporalScore: 3.0,
        severityThreatScore: 3.2,
        severityVector: "AV:A/AC:H/PR:L/UI:R/S:C/C:L/I:L/A:L/E:F/RL:O/RC:U",
        severityVectorV4:
          "CVSS:4.0/AV:A/AC:H/PR:L/UI:P/VC:L/VI:L/VA:L/SC:L/SI:L/SA:L/E:A",
        source: "asm",
        specific: "",
        state: "VULNERABLE",
        stateReasons: null,
        stream: null,
        tag: "tag-1, tag-2",
        technique: "SCR",
        treatmentAcceptanceDate: "",
        treatmentAcceptanceStatus: "",
        treatmentAssigned: "assigned-user-1",
        treatmentDate: "2019-07-05 09:56:40",
        treatmentJustification: "test progress justification",
        treatmentStatus: "",
        treatmentUser: "usertreatment@test.test",
        verification: "Requested",
        verificationJustification: null,
        vulnerabilityType: "inputs",
        where: "",
        zeroRisk: "Requested",
      },
    ];
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <authzGroupContext.Provider
          value={new PureAbility([{ action: "can_report_vulnerabilities" }])}
        >
          <HandleAcceptanceModal
            findingId={"1"}
            handleCloseModal={handleOnClose}
            refetchData={handleRefetchData}
            vulns={mokedVulns}
          />
        </authzGroupContext.Provider>
      </authzPermissionsContext.Provider>,
      { memoryRouter, mocks },
    );

    await selectOption("treatment-type", "ACCEPTED_UNDEFINED", false);
    await waitFor((): void => {
      expect(
        screen.queryByRole("textbox", { name: "justification" }),
      ).toBeInTheDocument();
    });
    await userEvent.type(
      screen.getByRole("textbox", { name: "justification" }),
      "This is a justification test",
    );

    expect(
      screen.getByRole("checkbox", { hidden: true, name: "vuln-treatment" }),
    ).toBeChecked();

    await userEvent.click(
      screen.getByRole("checkbox", { hidden: true, name: "vuln-treatment" }),
    );

    expect(
      screen.getByRole("checkbox", { hidden: true, name: "vuln-treatment" }),
    ).not.toBeChecked();

    await waitFor((): void => {
      expect(screen.queryByText("Confirm")).toBeInTheDocument();
    });
    await userEvent.click(screen.getByText("Confirm"));

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "searchFindings.tabVuln.alerts.acceptanceSuccess",
        "groupAlerts.updatedTitle",
      );
    });

    expect(handleRefetchData).toHaveBeenCalledTimes(1);
    expect(handleOnClose).toHaveBeenCalledTimes(1);
  });

  it("should handle vulns acceptance errors", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const handleRefetchData: jest.Mock = jest.fn();
    const mockedPermissions = new PureAbility<string>([
      {
        action:
          "integrates_api_mutations_handle_vulnerabilities_acceptance_mutate",
      },
    ]);
    const mocks = [
      graphqlMocked.mutation(
        HANDLE_VULNS_ACCEPTANCE,
        ({
          variables,
        }): StrictResponse<
          IErrorMessage | { data: HandleVulnerabilitiesAcceptance }
        > => {
          const {
            acceptedVulnerabilities,
            findingId,
            justification,
            rejectedVulnerabilities,
          } = variables;
          if (
            _.isEqual(acceptedVulnerabilities, ["test_error"]) &&
            findingId === "1" &&
            justification === "This is a justification test error" &&
            _.isEqual(rejectedVulnerabilities, [])
          ) {
            return HttpResponse.json({
              errors: [
                new GraphQLError(
                  "Exception - It cant handle acceptance without being requested",
                ),
              ],
            });
          }

          return HttpResponse.json({
            data: {
              handleVulnerabilitiesAcceptance: { success: true },
            },
          });
        },
      ),
    ];
    const mokedVulns: IVulnRowAttr[] = [
      {
        advisories: null,
        assigned: "",
        closingDate: null,
        customSeverity: 3,
        externalBugTrackingSystem: null,
        findingId: "1",
        groupName: "test",
        historicTreatment: [
          {
            acceptanceDate: "",
            acceptanceStatus: "SUBMITTED",
            assigned: "assigned-user-1",
            date: "2019-07-05 09:56:40",
            justification: "test justification",
            treatment: "ACCEPTED_UNDEFINED",
            user: "user@test.com",
          },
        ],
        id: "test_error",
        lastEditedBy: "usertreatment@test.test",
        lastStateDate: "2019-07-05 09:56:40",
        lastTreatmentDate: "2019-07-05 09:56:40",
        lastVerificationDate: null,
        organizationName: undefined,
        priority: 0,
        proposedSeverityAuthor: null,
        proposedSeverityTemporalScore: null,
        proposedSeverityThreatScore: null,
        proposedSeverityVector: null,
        proposedSeverityVectorV4: null,
        remediated: true,
        reportDate: "",
        root: {
          __typename: "GitRoot",
          branch: "master",
          criticality: RootCriticality.Low,
        },
        rootNickname: "https:",
        severityTemporalScore: 3.0,
        severityThreatScore: 2.9,
        severityVector: "AV:A/AC:H/PR:L/UI:R/S:C/C:L/I:L/A:L/E:F/RL:O/RC:U",
        severityVectorV4:
          "CVSS:4.0/AV:A/AC:H/PR:L/UI:P/VC:L/VI:L/VA:L/SC:L/SI:L/SA:L/E:A",
        source: "asm",
        specific: "",
        state: "VULNERABLE",
        stateReasons: null,
        stream: null,
        tag: "tag-1, tag-2",
        technique: "SCR",
        treatmentAcceptanceDate: "",
        treatmentAcceptanceStatus: "",
        treatmentAssigned: "assigned-user-1",
        treatmentDate: "2019-07-05 09:56:40",
        treatmentJustification: "test progress justification",
        treatmentStatus: "",
        treatmentUser: "usertreatment@test.test",
        verification: "Requested",
        verificationJustification: null,
        vulnerabilityType: "inputs",
        where: "",
        zeroRisk: "Requested",
      },
    ];

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <authzGroupContext.Provider
          value={new PureAbility([{ action: "can_report_vulnerabilities" }])}
        >
          <HandleAcceptanceModal
            findingId={"1"}
            handleCloseModal={jest.fn()}
            refetchData={jest.fn()}
            vulns={mokedVulns}
          />
        </authzGroupContext.Provider>
      </authzPermissionsContext.Provider>,
      { memoryRouter, mocks },
    );

    await selectOption("treatment-type", "ACCEPTED_UNDEFINED", false);
    await waitFor((): void => {
      expect(
        screen.queryByRole("textbox", { name: "justification" }),
      ).toBeInTheDocument();
    });
    await userEvent.type(
      screen.getByRole("textbox", { name: "justification" }),
      "This is a justification test error",
    );
    await waitFor((): void => {
      expect(screen.queryByText("Confirm")).toBeInTheDocument();
    });
    await userEvent.click(screen.getByText("Confirm"));

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledTimes(1);
    });

    expect(handleRefetchData).not.toHaveBeenCalled();
  });

  it("should handle confirm zero risk", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const handleRefetchData: jest.Mock = jest.fn();
    const handleCloseModal: jest.Mock = jest.fn();
    const mockedPermissions = new PureAbility<string>([
      {
        action:
          "integrates_api_mutations_confirm_vulnerabilities_zero_risk_mutate",
      },
      {
        action:
          "integrates_api_mutations_reject_vulnerabilities_zero_risk_mutate",
      },
      {
        action:
          "integrates_api_mutations_handle_vulnerabilities_acceptance_mutate",
      },
    ]);
    const mocks = [
      graphqlMocked.mutation(
        CONFIRM_VULNERABILITIES_ZERO_RISK,
        ({
          variables,
        }): StrictResponse<
          IErrorMessage | { data: ConfirmVulnerabilitiesZeroRisk }
        > => {
          const { vulnerabilities, findingId, justification } = variables;
          if (
            findingId === "422286126" &&
            justification === "This is a test of confirming zero risk vulns" &&
            _.isEqual(vulnerabilities, ["ab25380d-dfe1-4cde-aefd-acca6990d6aa"])
          ) {
            return HttpResponse.json({
              data: {
                confirmVulnerabilitiesZeroRisk: { success: true },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error confirming zr vulnerabilities")],
          });
        },
      ),
    ];
    const mokedVulns: IVulnRowAttr[] = [
      {
        advisories: null,
        assigned: "",
        closingDate: null,
        customSeverity: 3,
        externalBugTrackingSystem: null,
        findingId: "422286126",
        groupName: "test",
        historicTreatment: [
          {
            acceptanceDate: "",
            acceptanceStatus: "SUBMITTED",
            assigned: "assigned-user-1",
            date: "2019-07-05 09:56:40",
            justification: "test justification",
            treatment: "ACCEPTED_UNDEFINED",
            user: "user@test.com",
          },
        ],
        id: "ab25380d-dfe1-4cde-aefd-acca6990d6aa",
        lastEditedBy: "usertreatment@test.test",
        lastStateDate: "2019-07-05 09:56:40",
        lastTreatmentDate: "2019-07-05 09:56:40",
        lastVerificationDate: null,
        organizationName: undefined,
        priority: 0,
        proposedSeverityAuthor: null,
        proposedSeverityTemporalScore: null,
        proposedSeverityThreatScore: null,
        proposedSeverityVector: null,
        proposedSeverityVectorV4: null,
        remediated: true,
        reportDate: "",
        root: {
          __typename: "GitRoot",
          branch: "master",
          criticality: RootCriticality.Low,
        },
        rootNickname: "https:",
        severityTemporalScore: 3.0,
        severityThreatScore: 3.0,
        severityVector: "AV:A/AC:H/PR:L/UI:R/S:C/C:L/I:L/A:L/E:F/RL:O/RC:U",
        severityVectorV4:
          "CVSS:4.0/AV:A/AC:H/PR:L/UI:P/VC:L/VI:L/VA:L/SC:L/SI:L/SA:L/E:A",
        source: "asm",
        specific: "",
        state: "VULNERABLE",
        stateReasons: null,
        stream: null,
        tag: "tag-1, tag-2",
        technique: "SCR",
        treatmentAcceptanceDate: "",
        treatmentAcceptanceStatus: "",
        treatmentAssigned: "assigned-user-1",
        treatmentDate: "2019-07-05 09:56:40",
        treatmentJustification: "test progress justification",
        treatmentStatus: "",
        treatmentUser: "usertreatment@test.test",
        verification: "Requested",
        verificationJustification: null,
        vulnerabilityType: "inputs",
        where: "",
        zeroRisk: "Requested",
      },
    ];
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <authzGroupContext.Provider
          value={
            new PureAbility([
              { action: "can_report_vulnerabilities" },
              { action: "can_request_zero_risk" },
            ])
          }
        >
          <HandleAcceptanceModal
            findingId={"422286126"}
            handleCloseModal={handleCloseModal}
            refetchData={handleRefetchData}
            vulns={mokedVulns}
          />
        </authzGroupContext.Provider>
      </authzPermissionsContext.Provider>,
      { mocks },
    );

    await selectOption("treatment-type", "CONFIRM_REJECT_ZERO_RISK", false);

    await userEvent.click(
      screen.getByRole("button", {
        name: "searchFindings.tabVuln.handleAcceptanceModal.submittedForm.submittedTable.confirm",
      }),
    );
    await userEvent.type(
      screen.getByRole("textbox", { name: "justification" }),
      "This is a test of confirming zero risk vulns",
    );
    await waitFor((): void => {
      expect(screen.queryByText("Confirm")).not.toBeDisabled();
    });
    await userEvent.click(screen.getByText("Confirm"));
    await waitFor((): void => {
      expect(handleRefetchData).toHaveBeenCalledTimes(1);
    });

    expect(handleCloseModal).toHaveBeenCalledTimes(1);
    expect(msgSuccess).toHaveBeenCalledWith(
      "Zero risk vulnerability has been confirmed",
      "Correct!",
    );
  });

  it("should handle confirm zero risk error", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const handleRefetchData: jest.Mock = jest.fn();
    const handleCloseModal: jest.Mock = jest.fn();
    const mockedPermissions = new PureAbility<string>([
      {
        action:
          "integrates_api_mutations_confirm_vulnerabilities_zero_risk_mutate",
      },
      {
        action:
          "integrates_api_mutations_reject_vulnerabilities_zero_risk_mutate",
      },
      {
        action:
          "integrates_api_mutations_handle_vulnerabilities_acceptance_mutate",
      },
    ]);
    const mocks = [
      graphqlMocked.mutation(
        CONFIRM_VULNERABILITIES_ZERO_RISK,
        ({
          variables,
        }): StrictResponse<
          IErrorMessage | { data: ConfirmVulnerabilitiesZeroRisk }
        > => {
          const { vulnerabilities, findingId, justification } = variables;
          if (
            findingId === "422286126" &&
            justification === "This is a test of confirming zero risk vulns" &&
            _.isEqual(vulnerabilities, ["ab25380d-dfe1-4cde-aefd-acca6990d6aa"])
          ) {
            return HttpResponse.json({
              errors: [
                new GraphQLError(
                  "Exception - Zero risk vulnerability is not requested",
                ),
              ],
            });
          }

          return HttpResponse.json({
            data: {
              confirmVulnerabilitiesZeroRisk: { success: true },
            },
          });
        },
      ),
    ];
    const mokedVulns: IVulnRowAttr[] = [
      {
        advisories: null,
        assigned: "",
        closingDate: null,
        customSeverity: 3,
        externalBugTrackingSystem: null,
        findingId: "422286126",
        groupName: "test",
        historicTreatment: [
          {
            acceptanceDate: "",
            acceptanceStatus: "SUBMITTED",
            assigned: "assigned-user-1",
            date: "2019-07-05 09:56:40",
            justification: "test justification",
            treatment: "ACCEPTED_UNDEFINED",
            user: "user@test.com",
          },
        ],
        id: "ab25380d-dfe1-4cde-aefd-acca6990d6aa",
        lastEditedBy: "usertreatment@test.test",
        lastStateDate: "2019-07-05 09:56:40",
        lastTreatmentDate: "2019-07-05 09:56:40",
        lastVerificationDate: null,
        organizationName: undefined,
        priority: 0,
        proposedSeverityAuthor: null,
        proposedSeverityTemporalScore: null,
        proposedSeverityThreatScore: null,
        proposedSeverityVector: null,
        proposedSeverityVectorV4: null,
        remediated: true,
        reportDate: "",
        root: {
          __typename: "GitRoot",
          branch: "master",
          criticality: RootCriticality.Low,
        },
        rootNickname: "https:",
        severityTemporalScore: 3.0,
        severityThreatScore: 1.9,
        severityVector: "AV:A/AC:H/PR:L/UI:R/S:C/C:L/I:L/A:L/E:F/RL:O/RC:U",
        severityVectorV4:
          "CVSS:4.0/AV:A/AC:H/PR:L/UI:P/VC:L/VI:L/VA:L/SC:L/SI:L/SA:L/E:A",
        source: "asm",
        specific: "",
        state: "VULNERABLE",
        stateReasons: null,
        stream: null,
        tag: "tag-1, tag-2",
        technique: "SCR",
        treatmentAcceptanceDate: "",
        treatmentAcceptanceStatus: "",
        treatmentAssigned: "assigned-user-1",
        treatmentDate: "2019-07-05 09:56:40",
        treatmentJustification: "test progress justification",
        treatmentStatus: "",
        treatmentUser: "usertreatment@test.test",
        verification: "Requested",
        verificationJustification: null,
        vulnerabilityType: "inputs",
        where: "",
        zeroRisk: "Requested",
      },
    ];
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <authzGroupContext.Provider
          value={
            new PureAbility([
              { action: "can_report_vulnerabilities" },
              { action: "can_request_zero_risk" },
            ])
          }
        >
          <HandleAcceptanceModal
            findingId={"422286126"}
            handleCloseModal={handleCloseModal}
            refetchData={handleRefetchData}
            vulns={mokedVulns}
          />
        </authzGroupContext.Provider>
      </authzPermissionsContext.Provider>,
      { mocks },
    );

    await selectOption("treatment-type", "CONFIRM_REJECT_ZERO_RISK", false);

    await userEvent.click(
      screen.getByRole("button", {
        name: "searchFindings.tabVuln.handleAcceptanceModal.submittedForm.submittedTable.confirm",
      }),
    );
    await userEvent.clear(
      screen.getByRole("textbox", { name: "justification" }),
    );
    await userEvent.type(
      screen.getByRole("textbox", { name: "justification" }),
      "This is a test of confirming zero risk vulns",
    );
    await waitFor((): void => {
      expect(screen.queryByText("Confirm")).toBeInTheDocument();
    });

    await userEvent.click(screen.getByText("Confirm"));
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        "Zero risk vulnerability is not requested",
      );
    });

    expect(handleRefetchData).not.toHaveBeenCalledTimes(1);
    expect(handleCloseModal).not.toHaveBeenCalledTimes(1);
  });

  it("should handle reject zero risk", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const handleRefetchData: jest.Mock = jest.fn();
    const handleCloseModal: jest.Mock = jest.fn();
    const mockedPermissions = new PureAbility<string>([
      {
        action:
          "integrates_api_mutations_confirm_vulnerabilities_zero_risk_mutate",
      },
      {
        action:
          "integrates_api_mutations_reject_vulnerabilities_zero_risk_mutate",
      },
      {
        action:
          "integrates_api_mutations_handle_vulnerabilities_acceptance_mutate",
      },
    ]);
    const mocks = [
      graphqlMocked.mutation(
        REJECT_VULNERABILITIES_ZERO_RISK,
        ({
          variables,
        }): StrictResponse<
          IErrorMessage | { data: RejectVulnerabilitiesZeroRisk }
        > => {
          const { vulnerabilities, findingId, justification } = variables;
          if (
            findingId === "422286126" &&
            justification === "This is a test of rejecting zero risk vulns" &&
            _.isEqual(vulnerabilities, ["ab25380d-dfe1-4cde-aefd-acca6990d6aa"])
          ) {
            return HttpResponse.json({
              data: {
                rejectVulnerabilitiesZeroRisk: { success: true },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error rejecting vulnerabilities")],
          });
        },
      ),
    ];

    const mokedVulns: IVulnRowAttr[] = [
      {
        advisories: null,
        assigned: "",
        closingDate: null,
        customSeverity: 3,
        externalBugTrackingSystem: null,
        findingId: "422286126",
        groupName: "test",
        historicTreatment: [
          {
            acceptanceDate: "",
            acceptanceStatus: "SUBMITTED",
            assigned: "assigned-user-1",
            date: "2019-07-05 09:56:40",
            justification: "test justification",
            treatment: "ACCEPTED_UNDEFINED",
            user: "user@test.com",
          },
        ],
        id: "ab25380d-dfe1-4cde-aefd-acca6990d6aa",
        lastEditedBy: "usertreatment@test.test",
        lastStateDate: "2019-07-05 09:56:40",
        lastTreatmentDate: "2019-07-05 09:56:40",
        lastVerificationDate: null,
        organizationName: undefined,
        priority: 0,
        proposedSeverityAuthor: null,
        proposedSeverityTemporalScore: null,
        proposedSeverityThreatScore: null,
        proposedSeverityVector: null,
        proposedSeverityVectorV4: null,
        remediated: true,
        reportDate: "",
        root: {
          __typename: "GitRoot",
          branch: "master",
          criticality: RootCriticality.Low,
        },
        rootNickname: "https:",
        severityTemporalScore: 3.0,
        severityThreatScore: 3.0,
        severityVector: "AV:A/AC:H/PR:L/UI:R/S:C/C:L/I:L/A:L/E:F/RL:O/RC:U",
        severityVectorV4:
          "CVSS:4.0/AV:A/AC:H/PR:L/UI:P/VC:L/VI:L/VA:L/SC:L/SI:L/SA:L/E:A",
        source: "asm",
        specific: "",
        state: "VULNERABLE",
        stateReasons: null,
        stream: null,
        tag: "tag-1, tag-2",
        technique: "SCR",
        treatmentAcceptanceDate: "",
        treatmentAcceptanceStatus: "",
        treatmentAssigned: "assigned-user-1",
        treatmentDate: "2019-07-05 09:56:40",
        treatmentJustification: "test progress justification",
        treatmentStatus: "",
        treatmentUser: "usertreatment@test.test",
        verification: "Requested",
        verificationJustification: null,
        vulnerabilityType: "inputs",
        where: "",
        zeroRisk: "Requested",
      },
    ];
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <authzGroupContext.Provider
          value={
            new PureAbility([
              { action: "can_report_vulnerabilities" },
              { action: "can_request_zero_risk" },
            ])
          }
        >
          <HandleAcceptanceModal
            findingId={"422286126"}
            handleCloseModal={handleCloseModal}
            refetchData={handleRefetchData}
            vulns={mokedVulns}
          />
        </authzGroupContext.Provider>
      </authzPermissionsContext.Provider>,
      { mocks },
    );

    await selectOption("treatment-type", "CONFIRM_REJECT_ZERO_RISK", false);

    await userEvent.click(
      screen.getByRole("button", {
        name: "searchFindings.tabVuln.handleAcceptanceModal.submittedForm.submittedTable.reject",
      }),
    );
    await userEvent.clear(
      screen.getByRole("textbox", { name: "justification" }),
    );
    await userEvent.type(
      screen.getByRole("textbox", { name: "justification" }),
      "This is a test of rejecting zero risk vulns",
    );
    await waitFor((): void => {
      expect(screen.queryByText("Confirm")).toBeInTheDocument();
    });

    await userEvent.click(screen.getByText("Confirm"));
    await waitFor((): void => {
      expect(handleRefetchData).toHaveBeenCalledTimes(1);
    });

    expect(handleCloseModal).toHaveBeenCalledTimes(1);
    expect(msgSuccess).toHaveBeenCalledWith(
      "Zero risk vulnerability has been rejected",
      "Correct!",
    );
  });

  it("should handle reject zero risk error", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const handleRefetchData: jest.Mock = jest.fn();
    const handleCloseModal: jest.Mock = jest.fn();
    const mockedPermissions = new PureAbility<string>([
      {
        action:
          "integrates_api_mutations_confirm_vulnerabilities_zero_risk_mutate",
      },
      {
        action:
          "integrates_api_mutations_reject_vulnerabilities_zero_risk_mutate",
      },
      {
        action:
          "integrates_api_mutations_handle_vulnerabilities_acceptance_mutate",
      },
    ]);
    const mocks = [
      graphqlMocked.mutation(
        REJECT_VULNERABILITIES_ZERO_RISK,
        ({
          variables,
        }): StrictResponse<
          IErrorMessage | { data: RejectVulnerabilitiesZeroRisk }
        > => {
          const { vulnerabilities, findingId, justification } = variables;
          if (
            findingId === "422286126" &&
            justification === "This is a test of rejecting zero risk vulns" &&
            _.isEqual(vulnerabilities, ["ab25380d-dfe1-4cde-aefd-acca6990d6aa"])
          ) {
            return HttpResponse.json({
              errors: [
                new GraphQLError(
                  "Exception - Zero risk vulnerability is not requested",
                ),
              ],
            });
          }

          return HttpResponse.json({
            data: {
              rejectVulnerabilitiesZeroRisk: { success: true },
            },
          });
        },
      ),
    ];

    const mokedVulns: IVulnRowAttr[] = [
      {
        advisories: null,
        assigned: "",
        closingDate: null,
        customSeverity: 3,
        externalBugTrackingSystem: null,
        findingId: "422286126",
        groupName: "test",
        historicTreatment: [
          {
            acceptanceDate: "",
            acceptanceStatus: "SUBMITTED",
            assigned: "assigned-user-1",
            date: "2019-07-05 09:56:40",
            justification: "test justification",
            treatment: "ACCEPTED_UNDEFINED",
            user: "user@test.com",
          },
        ],
        id: "ab25380d-dfe1-4cde-aefd-acca6990d6aa",
        lastEditedBy: "usertreatment@test.test",
        lastStateDate: "2019-07-05 09:56:40",
        lastTreatmentDate: "2019-07-05 09:56:40",
        lastVerificationDate: null,
        organizationName: undefined,
        priority: 0,
        proposedSeverityAuthor: null,
        proposedSeverityTemporalScore: null,
        proposedSeverityThreatScore: null,
        proposedSeverityVector: null,
        proposedSeverityVectorV4: null,
        remediated: true,
        reportDate: "",
        root: {
          __typename: "GitRoot",
          branch: "master",
          criticality: RootCriticality.Low,
        },
        rootNickname: "https:",
        severityTemporalScore: 3.0,
        severityThreatScore: 2.8,
        severityVector: "AV:A/AC:H/PR:L/UI:R/S:C/C:L/I:L/A:L/E:F/RL:O/RC:U",
        severityVectorV4:
          "CVSS:4.0/AV:A/AC:H/PR:L/UI:P/VC:L/VI:L/VA:L/SC:L/SI:L/SA:L/E:A",
        source: "asm",
        specific: "",
        state: "VULNERABLE",
        stateReasons: null,
        stream: null,
        tag: "tag-1, tag-2",
        technique: "SCR",
        treatmentAcceptanceDate: "",
        treatmentAcceptanceStatus: "",
        treatmentAssigned: "assigned-user-1",
        treatmentDate: "2019-07-05 09:56:40",
        treatmentJustification: "test progress justification",
        treatmentStatus: "",
        treatmentUser: "usertreatment@test.test",
        verification: "Requested",
        verificationJustification: null,
        vulnerabilityType: "inputs",
        where: "",
        zeroRisk: "Requested",
      },
    ];
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <authzGroupContext.Provider
          value={
            new PureAbility([
              { action: "can_report_vulnerabilities" },
              { action: "can_request_zero_risk" },
            ])
          }
        >
          <HandleAcceptanceModal
            findingId={"422286126"}
            handleCloseModal={handleCloseModal}
            refetchData={handleRefetchData}
            vulns={mokedVulns}
          />
        </authzGroupContext.Provider>
      </authzPermissionsContext.Provider>,
      { mocks },
    );

    await selectOption("treatment-type", "CONFIRM_REJECT_ZERO_RISK", false);

    await userEvent.click(
      screen.getByRole("button", {
        name: "searchFindings.tabVuln.handleAcceptanceModal.submittedForm.submittedTable.reject",
      }),
    );
    await userEvent.clear(
      screen.getByRole("textbox", { name: "justification" }),
    );
    await userEvent.type(
      screen.getByRole("textbox", { name: "justification" }),
      "This is a test of rejecting zero risk vulns",
    );
    await waitFor((): void => {
      expect(screen.queryByText("Confirm")).toBeInTheDocument();
    });

    await userEvent.click(screen.getByText("Confirm"));
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        "Zero risk vulnerability is not requested",
      );
    });

    expect(handleRefetchData).not.toHaveBeenCalledTimes(1);
    expect(handleCloseModal).not.toHaveBeenCalledTimes(1);
  });

  it("should display dropdown to confirm zero risk", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const handleRefetchData: jest.Mock = jest.fn();
    const handleCloseModal: jest.Mock = jest.fn();
    const mockedPermissions = new PureAbility<string>([
      {
        action:
          "integrates_api_mutations_confirm_vulnerabilities_zero_risk_mutate",
      },
      {
        action:
          "integrates_api_mutations_reject_vulnerabilities_zero_risk_mutate",
      },
      {
        action:
          "integrates_api_mutations_handle_vulnerabilities_acceptance_mutate",
      },
      { action: "see_dropdown_to_confirm_zero_risk" },
    ]);
    const mokedVulns: IVulnRowAttr[] = [
      {
        advisories: null,
        assigned: "",
        closingDate: null,
        customSeverity: 3,
        externalBugTrackingSystem: null,
        findingId: "422286126",
        groupName: "test",
        historicTreatment: [
          {
            acceptanceDate: "",
            acceptanceStatus: "SUBMITTED",
            assigned: "assigned-user-1",
            date: "2019-07-05 09:56:40",
            justification: "test justification",
            treatment: "ACCEPTED_UNDEFINED",
            user: "user@test.com",
          },
        ],
        id: "ab25380d-dfe1-4cde-aefd-acca6990d6aa",
        lastEditedBy: "usertreatment@test.test",
        lastStateDate: "2019-07-05 09:56:40",
        lastTreatmentDate: "2019-07-05 09:56:40",
        lastVerificationDate: null,
        organizationName: undefined,
        priority: 0,
        proposedSeverityAuthor: null,
        proposedSeverityTemporalScore: null,
        proposedSeverityThreatScore: null,
        proposedSeverityVector: null,
        proposedSeverityVectorV4: null,
        remediated: true,
        reportDate: "",
        root: {
          __typename: "GitRoot",
          branch: "master",
          criticality: RootCriticality.Low,
        },
        rootNickname: "https:",
        severityTemporalScore: 3.0,
        severityThreatScore: 2.9,
        severityVector: "AV:A/AC:H/PR:L/UI:R/S:C/C:L/I:L/A:L/E:F/RL:O/RC:U",
        severityVectorV4:
          "CVSS:4.0/AV:A/AC:H/PR:L/UI:P/VC:L/VI:L/VA:L/SC:L/SI:L/SA:L/E:A",
        source: "asm",
        specific: "",
        state: "VULNERABLE",
        stateReasons: null,
        stream: null,
        tag: "tag-1, tag-2",
        technique: "SCR",
        treatmentAcceptanceDate: "",
        treatmentAcceptanceStatus: "",
        treatmentAssigned: "assigned-user-1",
        treatmentDate: "2019-07-05 09:56:40",
        treatmentJustification: "test progress justification",
        treatmentStatus: "",
        treatmentUser: "usertreatment@test.test",
        verification: "Requested",
        verificationJustification: null,
        vulnerabilityType: "inputs",
        where: "",
        zeroRisk: "Requested",
      },
    ];
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <authzGroupContext.Provider
          value={
            new PureAbility([
              { action: "can_report_vulnerabilities" },
              { action: "can_request_zero_risk" },
            ])
          }
        >
          <HandleAcceptanceModal
            findingId={"422286126"}
            handleCloseModal={handleCloseModal}
            refetchData={handleRefetchData}
            vulns={mokedVulns}
          />
        </authzGroupContext.Provider>
      </authzPermissionsContext.Provider>,
    );

    await selectOption("treatment-type", "CONFIRM_REJECT_ZERO_RISK", false);

    await userEvent.click(
      screen.getByRole("button", {
        name: "searchFindings.tabVuln.handleAcceptanceModal.submittedForm.submittedTable.confirm",
      }),
    );

    await userEvent.click(
      screen.getByRole("combobox", { name: "justification" }),
    );
    await userEvent.click(screen.getByText(/confirmation.fpReport/iu));

    expect(screen.getByRole("combobox", { name: "justification" })).toHaveValue(
      "searchFindings.tabDescription.handleAcceptanceModal.zeroRiskJustification.confirmation.fpReport",
    );

    await userEvent.clear(
      screen.getByRole("combobox", { name: "justification" }),
    );
    await userEvent.click(screen.getByText(/confirmation.fpContext/iu));

    expect(screen.getByRole("combobox", { name: "justification" })).toHaveValue(
      "searchFindings.tabDescription.handleAcceptanceModal.zeroRiskJustification.confirmation.fpContext",
    );

    await userEvent.clear(
      screen.getByRole("combobox", { name: "justification" }),
    );
    await userEvent.click(screen.getByText(/confirmation.outOfTheScope/iu));

    expect(screen.getByRole("combobox", { name: "justification" })).toHaveValue(
      "searchFindings.tabDescription.handleAcceptanceModal.zeroRiskJustification.confirmation.outOfTheScope",
    );

    await userEvent.clear(
      screen.getByRole("combobox", { name: "justification" }),
    );

    expect(
      screen.queryByText(
        "searchFindings.tabDescription.handleAcceptanceModal.zeroRiskJustification.rejection.fn",
      ),
    ).not.toBeInTheDocument();
  });

  it("should display dropdown to reject zero risk", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const handleRefetchData: jest.Mock = jest.fn();
    const handleCloseModal: jest.Mock = jest.fn();
    const mockedPermissions = new PureAbility<string>([
      {
        action:
          "integrates_api_mutations_confirm_vulnerabilities_zero_risk_mutate",
      },
      {
        action:
          "integrates_api_mutations_reject_vulnerabilities_zero_risk_mutate",
      },
      {
        action:
          "integrates_api_mutations_handle_vulnerabilities_acceptance_mutate",
      },
      { action: "see_dropdown_to_confirm_zero_risk" },
    ]);
    const mokedVulns: IVulnRowAttr[] = [
      {
        advisories: null,
        assigned: "",
        closingDate: null,
        customSeverity: 3,
        externalBugTrackingSystem: null,
        findingId: "422286126",
        groupName: "test",
        historicTreatment: [
          {
            acceptanceDate: "",
            acceptanceStatus: "SUBMITTED",
            assigned: "assigned-user-1",
            date: "2019-07-05 09:56:40",
            justification: "test justification",
            treatment: "ACCEPTED_UNDEFINED",
            user: "user@test.com",
          },
        ],
        id: "ab25380d-dfe1-4cde-aefd-acca6990d6aa",
        lastEditedBy: "usertreatment@test.test",
        lastStateDate: "2019-07-05 09:56:40",
        lastTreatmentDate: "2019-07-05 09:56:40",
        lastVerificationDate: null,
        organizationName: undefined,
        priority: 0,
        proposedSeverityAuthor: null,
        proposedSeverityTemporalScore: null,
        proposedSeverityThreatScore: null,
        proposedSeverityVector: null,
        proposedSeverityVectorV4: null,
        remediated: true,
        reportDate: "",
        root: {
          __typename: "GitRoot",
          branch: "master",
          criticality: RootCriticality.Low,
        },
        rootNickname: "https:",
        severityTemporalScore: 3.0,
        severityThreatScore: 3.1,
        severityVector: "AV:A/AC:H/PR:L/UI:R/S:C/C:L/I:L/A:L/E:F/RL:O/RC:U",
        severityVectorV4:
          "CVSS:4.0/AV:A/AC:H/PR:L/UI:P/VC:L/VI:L/VA:L/SC:L/SI:L/SA:L/E:A",
        source: "asm",
        specific: "",
        state: "VULNERABLE",
        stateReasons: null,
        stream: null,
        tag: "tag-1, tag-2",
        technique: "SCR",
        treatmentAcceptanceDate: "",
        treatmentAcceptanceStatus: "",
        treatmentAssigned: "assigned-user-1",
        treatmentDate: "2019-07-05 09:56:40",
        treatmentJustification: "test progress justification",
        treatmentStatus: "",
        treatmentUser: "usertreatment@test.test",
        verification: "Requested",
        verificationJustification: null,
        vulnerabilityType: "inputs",
        where: "",
        zeroRisk: "Requested",
      },
    ];
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <authzGroupContext.Provider
          value={
            new PureAbility([
              { action: "can_report_vulnerabilities" },
              { action: "can_request_zero_risk" },
            ])
          }
        >
          <HandleAcceptanceModal
            findingId={"422286126"}
            handleCloseModal={handleCloseModal}
            refetchData={handleRefetchData}
            vulns={mokedVulns}
          />
        </authzGroupContext.Provider>
      </authzPermissionsContext.Provider>,
    );

    await selectOption("treatment-type", "CONFIRM_REJECT_ZERO_RISK", false);

    await userEvent.click(
      screen.getByRole("button", {
        name: "searchFindings.tabVuln.handleAcceptanceModal.submittedForm.submittedTable.reject",
      }),
    );

    await userEvent.click(
      screen.getByRole("combobox", { name: "justification" }),
    );
    await userEvent.click(screen.getByText(/rejection.fn/iu));

    expect(screen.getByRole("combobox", { name: "justification" })).toHaveValue(
      "searchFindings.tabDescription.handleAcceptanceModal.zeroRiskJustification.rejection.fn",
    );

    await userEvent.clear(
      screen.getByRole("combobox", { name: "justification" }),
    );
    await userEvent.click(screen.getByText(/rejection.complementaryControl/iu));

    expect(screen.getByRole("combobox", { name: "justification" })).toHaveValue(
      "searchFindings.tabDescription.handleAcceptanceModal.zeroRiskJustification.rejection.complementaryControl",
    );

    await userEvent.clear(
      screen.getByRole("combobox", { name: "justification" }),
    );

    expect(
      screen.queryByText(
        "searchFindings.tabDescription.handleAcceptanceModal.zeroRiskJustification.confirmation.fp",
      ),
    ).not.toBeInTheDocument();
  });

  it("should handle confirm vulnerability", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const handleRefetchData: jest.Mock = jest.fn();
    const handleCloseModal: jest.Mock = jest.fn();
    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_confirm_vulnerabilities_mutate" },
      { action: "integrates_api_mutations_reject_vulnerabilities_mutate" },
      { action: "can_report_vulnerabilities" },
    ]);
    const mocksMutation = [
      graphqlMocked.mutation(
        CONFIRM_VULNERABILITIES,
        ({
          variables,
        }): StrictResponse<
          IErrorMessage | { data: ConfirmVulnerabilities }
        > => {
          const { vulnerabilities, findingId } = variables;
          if (
            findingId === "422286126" &&
            _.isEqual(vulnerabilities, ["ab25380d-dfe1-4cde-aefd-acca6990d6aa"])
          ) {
            return HttpResponse.json({
              data: {
                confirmVulnerabilities: { success: true },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error confirming vulnerabilities")],
          });
        },
      ),
    ];
    const mokedVulns: IVulnRowAttr[] = [
      {
        advisories: null,
        assigned: "",
        closingDate: null,
        customSeverity: 3,
        externalBugTrackingSystem: null,
        findingId: "422286126",
        groupName: "test",
        historicTreatment: [
          {
            acceptanceDate: "",
            acceptanceStatus: "SUBMITTED",
            assigned: "assigned-user-1",
            date: "2019-07-05 09:56:40",
            justification: "test justification",
            treatment: "ACCEPTED_UNDEFINED",
            user: "user@test.com",
          },
        ],
        id: "ab25380d-dfe1-4cde-aefd-acca6990d6aa",
        lastEditedBy: "usertreatment@test.test",
        lastStateDate: "2019-07-05 09:56:40",
        lastTreatmentDate: "2019-07-05 09:56:40",
        lastVerificationDate: null,
        organizationName: undefined,
        priority: 0,
        proposedSeverityAuthor: null,
        proposedSeverityTemporalScore: null,
        proposedSeverityThreatScore: null,
        proposedSeverityVector: null,
        proposedSeverityVectorV4: null,
        remediated: true,
        reportDate: "",
        root: {
          __typename: "GitRoot",
          branch: "master",
          criticality: RootCriticality.Low,
        },
        rootNickname: "https:",
        severityTemporalScore: 3.0,
        severityThreatScore: 3.0,
        severityVector: "AV:A/AC:H/PR:L/UI:R/S:C/C:L/I:L/A:L/E:F/RL:O/RC:U",
        severityVectorV4:
          "CVSS:4.0/AV:A/AC:H/PR:L/UI:P/VC:L/VI:L/VA:L/SC:L/SI:L/SA:L/E:A",
        source: "asm",
        specific: "",
        state: "SUBMITTED",
        stateReasons: [],
        stream: null,
        tag: "tag-1, tag-2",
        technique: "SCR",
        treatmentAcceptanceDate: "",
        treatmentAcceptanceStatus: "",
        treatmentAssigned: "assigned-user-1",
        treatmentDate: "2019-07-05 09:56:40",
        treatmentJustification: "test progress justification",
        treatmentStatus: "",
        treatmentUser: "usertreatment@test.test",
        verification: "Requested",
        verificationJustification: null,
        vulnerabilityType: "inputs",
        where: "",
        zeroRisk: null,
      },
    ];
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <authzGroupContext.Provider
          value={
            new PureAbility([
              { action: "can_report_vulnerabilities" },
              { action: "can_request_zero_risk" },
            ])
          }
        >
          <HandleAcceptanceModal
            findingId={"422286126"}
            handleCloseModal={handleCloseModal}
            refetchData={handleRefetchData}
            vulns={mokedVulns}
          />
        </authzGroupContext.Provider>
      </authzPermissionsContext.Provider>,
      { mocks: mocksMutation },
    );
    await userEvent.click(
      screen.getByText("searchFindings.tabDescription.treatment.placeholder"),
    );
    await userEvent.click(
      screen.getByText(
        "searchFindings.tabDescription.treatment.confirmRejectVulnerability",
      ),
    );
    await userEvent.click(
      screen.getByText(
        /searchfindings\.tabvuln\.handleacceptancemodal\.submittedform\.submittedtable\.confirm/iu,
      ),
    );
    await waitFor((): void => {
      expect(screen.queryByText("Confirm")).not.toBeDisabled();
    });
    await userEvent.click(screen.getByText("Confirm"));
    await waitFor((): void => {
      expect(handleRefetchData).toHaveBeenCalledTimes(1);
    });

    expect(handleCloseModal).toHaveBeenCalledTimes(1);
    expect(msgSuccess).toHaveBeenCalledWith(
      "Vulnerability has been confirmed",
      "Correct!",
    );
  });

  it("should handle reject vulnerability", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();
    jest.spyOn(console, "warn").mockImplementation();

    const handleRefetchData: jest.Mock = jest.fn();
    const handleCloseModal: jest.Mock = jest.fn();
    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_confirm_vulnerabilities_mutate" },
      { action: "integrates_api_mutations_reject_vulnerabilities_mutate" },
      { action: "can_report_vulnerabilities" },
    ]);
    const mocksMutation = [
      graphqlMocked.mutation(
        REJECT_VULNERABILITIES,
        ({
          variables,
        }): StrictResponse<IErrorMessage | { data: RejectVulnerabilities }> => {
          const { otherReason, reasons, vulnerabilities, findingId } =
            variables;
          if (
            findingId === "422286126" &&
            otherReason === "Other reason test" &&
            _.isEqual(reasons, [
              "CONSISTENCY",
              "DUPLICATED",
              "FALSE_POSITIVE",
              "OTHER",
            ]) &&
            _.isEqual(vulnerabilities, ["ab25380d-dfe1-4cde-aefd-acca6990d6aa"])
          ) {
            return HttpResponse.json({
              data: {
                rejectVulnerabilities: { success: true },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error confirming vulnerabilities")],
          });
        },
      ),
    ];
    const mokedVulns: IVulnRowAttr[] = [
      {
        advisories: null,
        assigned: "",
        closingDate: null,
        customSeverity: 3,
        externalBugTrackingSystem: null,
        findingId: "422286126",
        groupName: "test",
        historicTreatment: [
          {
            acceptanceDate: "",
            acceptanceStatus: "SUBMITTED",
            assigned: "assigned-user-1",
            date: "2019-07-05 09:56:40",
            justification: "test justification",
            treatment: "ACCEPTED_UNDEFINED",
            user: "user@test.com",
          },
        ],
        id: "ab25380d-dfe1-4cde-aefd-acca6990d6aa",
        lastEditedBy: "usertreatment@test.test",
        lastStateDate: "2019-07-05 09:56:40",
        lastTreatmentDate: "2019-07-05 09:56:40",
        lastVerificationDate: null,
        organizationName: undefined,
        priority: 0,
        proposedSeverityAuthor: null,
        proposedSeverityTemporalScore: null,
        proposedSeverityThreatScore: null,
        proposedSeverityVector: null,
        proposedSeverityVectorV4: null,
        remediated: true,
        reportDate: "",
        root: {
          __typename: "GitRoot",
          branch: "master",
          criticality: RootCriticality.Low,
        },
        rootNickname: "https:",
        severityTemporalScore: 3.0,
        severityThreatScore: 3.0,
        severityVector: "AV:A/AC:H/PR:L/UI:R/S:C/C:L/I:L/A:L/E:F/RL:O/RC:U",
        severityVectorV4:
          "CVSS:4.0/AV:A/AC:H/PR:L/UI:P/VC:L/VI:L/VA:L/SC:L/SI:L/SA:L/E:A",
        source: "asm",
        specific: "",
        state: "SUBMITTED",
        stateReasons: [],
        stream: null,
        tag: "tag-1, tag-2",
        technique: "SCR",
        treatmentAcceptanceDate: "",
        treatmentAcceptanceStatus: "",
        treatmentAssigned: "assigned-user-1",
        treatmentDate: "2019-07-05 09:56:40",
        treatmentJustification: "test progress justification",
        treatmentStatus: "",
        treatmentUser: "usertreatment@test.test",
        verification: "Requested",
        verificationJustification: null,
        vulnerabilityType: "inputs",
        where: "",
        zeroRisk: null,
      },
    ];
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <authzGroupContext.Provider
          value={
            new PureAbility([
              { action: "can_report_vulnerabilities" },
              { action: "can_request_zero_risk" },
            ])
          }
        >
          <HandleAcceptanceModal
            findingId={"422286126"}
            handleCloseModal={handleCloseModal}
            refetchData={handleRefetchData}
            vulns={mokedVulns}
          />
        </authzGroupContext.Provider>
      </authzPermissionsContext.Provider>,
      { mocks: mocksMutation },
    );
    const dropdown = screen.getByTestId("treatment-type-selected-option");
    await userEvent.click(dropdown);
    const optionsContainer = screen.getByTestId(
      `treatment-type-dropdown-options`,
    );
    await userEvent.click(
      getByValue(optionsContainer, "CONFIRM_REJECT_VULNERABILITY"),
    );

    await userEvent.click(
      screen.getByText(
        /searchfindings\.tabvuln\.handleacceptancemodal\.submittedform\.submittedtable\.reject/iu,
      ),
    );
    await userEvent.click(screen.getByText(/consistency/iu));
    await userEvent.click(screen.getByText(/other/iu));
    await userEvent.click(screen.getByText(/duplicated/iu));
    await userEvent.click(screen.getByText(/false_positive/iu));
    await userEvent.type(
      screen.getByRole("textbox", { name: /otherrejectionreason/iu }),
      "Other reason test",
    );
    await waitFor((): void => {
      expect(screen.queryByText("Confirm")).not.toBeDisabled();
    });
    await userEvent.click(screen.getByText("Confirm"));
    await waitFor((): void => {
      expect(handleRefetchData).toHaveBeenCalledTimes(1);
    });

    expect(handleCloseModal).toHaveBeenCalledTimes(1);

    expect(msgSuccess).toHaveBeenCalledWith(
      "Vulnerability has been rejected",
      "Correct!",
    );
  });
});
