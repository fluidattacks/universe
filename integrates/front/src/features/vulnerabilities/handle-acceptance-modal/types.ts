import type { ExecutionResult } from "graphql";

import type { IFinding, IVulnRowAttr } from "features/vulnerabilities/types";

interface IVulnDataAttr {
  acceptance: "" | "APPROVED" | "REJECTED";
  finding?: IFinding;
  findingId: string;
  groupName?: string;
  hacker?: string;
  id: string;
  severityTemporalScore: number;
  severityThreatScore: number;
  specific: string;
  tag: string;
  where: string;
}

interface IFormValues {
  justification: string;
}

interface IHandleVulnerabilitiesAcceptanceModalProps {
  findingId?: string;
  vulns: IVulnRowAttr[];
  handleCloseModal: () => void;
  refetchData: () => void;
}

interface IHandleVulnerabilitiesAcceptanceModalFormProps {
  acceptanceVulnerabilities: IVulnDataAttr[];
  acceptedVulnerabilities: IVulnDataAttr[];
  rejectedVulnerabilities: IVulnDataAttr[];
  hasAcceptedVulns: boolean;
  hasRejectedVulns: boolean;
  handlingAcceptance: boolean;
  confirmingZeroRisk: boolean;
  rejectingZeroRisk: boolean;
  setAcceptanceVulns: React.Dispatch<React.SetStateAction<IVulnDataAttr[]>>;
  treatment: string;
  handleCloseModal: () => void;
  vulns: IVulnRowAttr[];
}

interface IHandleVulnerabilitiesAcceptanceResultAttr {
  handleVulnerabilitiesAcceptance: {
    success: boolean;
  };
}

interface IConfirmVulnerabilitiesResultAttr {
  confirmVulnerabilities: {
    success: boolean;
  };
}

type TVulnUpdateResult =
  ExecutionResult<IHandleVulnerabilitiesAcceptanceResultAttr>;

interface IConfirmVulnZeroRiskResultAttr {
  confirmVulnerabilitiesZeroRisk: {
    success: boolean;
  };
}

interface IRejectZeroRiskVulnResultAttr {
  rejectVulnerabilitiesZeroRisk: {
    success: boolean;
  };
}

interface IRejectVulnerabilitiesResultAttr {
  rejectVulnerabilities: {
    success: boolean;
  };
}

export type {
  IConfirmVulnerabilitiesResultAttr,
  IConfirmVulnZeroRiskResultAttr,
  IFormValues,
  IHandleVulnerabilitiesAcceptanceModalFormProps,
  IHandleVulnerabilitiesAcceptanceModalProps,
  IHandleVulnerabilitiesAcceptanceResultAttr,
  IRejectZeroRiskVulnResultAttr,
  IRejectVulnerabilitiesResultAttr,
  IVulnDataAttr,
  TVulnUpdateResult,
};
