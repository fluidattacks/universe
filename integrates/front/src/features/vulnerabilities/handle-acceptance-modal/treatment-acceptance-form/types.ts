import type { IVulnRowAttr } from "../../types";
import type { IVulnDataAttr } from "../types";
import type { TVulnUpdateResult } from "features/vulnerabilities/treatment-modal/update-treatment/types";

interface IFormValues {
  justification: string;
  tag?: string;
}

interface IHandleTagModificationParameters {
  acceptanceVulnerabilities: IVulnDataAttr[];
  treatmentType: "ACCEPTED_UNDEFINED" | "ACCEPTED";
  updateVulnerability: (
    variables: Record<string, unknown>,
  ) => Promise<TVulnUpdateResult>;
  onCancel: VoidFunction;
  vulnerabilities: IVulnRowAttr[];
  formValues: IFormValues;
  wasTagModified: boolean;
}

export type { IFormValues, IHandleTagModificationParameters };
