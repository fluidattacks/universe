import { Toggle } from "@fluidattacks/design";
import type { ColumnDef, Row } from "@tanstack/react-table";
import type { Dispatch, SetStateAction } from "react";
import { useCallback } from "react";

import { Table } from "components/table";
import type { IVulnRowAttr } from "features/vulnerabilities/types";
import { useTable } from "hooks";
import { severityFormatter } from "utils/format-helpers";

interface IAcceptanceTableProps {
  vulns: IVulnRowAttr[];
  setVulns: Dispatch<SetStateAction<IVulnRowAttr[]>>;
}

const TreatmentAcceptanceTable = (
  props: IAcceptanceTableProps,
): JSX.Element => {
  const { vulns, setVulns } = props;
  const tableRef = useTable("vulnsToHandleAcceptance");

  const onChange = useCallback(
    (cell: Row<IVulnRowAttr>): VoidFunction => {
      return (): void => {
        setVulns((items): IVulnRowAttr[] => [
          ...items.slice(0, cell.index),
          {
            ...items[cell.index],
            acceptance:
              items[cell.index].acceptance === "APPROVED"
                ? "REJECTED"
                : "APPROVED",
          },
          ...items.slice(cell.index + 1),
        ]);
      };
    },
    [setVulns],
  );

  const columns: ColumnDef<IVulnRowAttr>[] = [
    {
      accessorKey: "where",
      header: "Where",
    },
    {
      accessorKey: "specific",
      header: "Specific",
    },
    {
      accessorKey: "severityThreatScore",
      cell: (cell): JSX.Element => severityFormatter(Number(cell.getValue())),
      header: "Severity",
    },
    {
      accessorKey: "acceptance",
      cell: ({ row, cell }): JSX.Element => (
        <Toggle
          defaultChecked={cell.getValue() === "APPROVED"}
          leftDescription={{ off: "REJECTED", on: "APPROVED" }}
          name={"vuln-treatment"}
          onChange={onChange(row)}
        />
      ),
      header: "Acceptance",
    },
  ];

  return (
    <Table
      columns={columns}
      data={vulns}
      options={{ enableSearchBar: false }}
      tableRef={tableRef}
    />
  );
};

export { TreatmentAcceptanceTable };
