/* eslint-disable max-lines */
import type {
  FetchResult,
  InternalRefetchQueriesInclude,
  MutationFunctionOptions,
  MutationHookOptions,
} from "@apollo/client";
import _ from "lodash";

import type {
  IConfirmVulnZeroRiskResultAttr,
  IConfirmVulnerabilitiesResultAttr,
  IRejectVulnerabilitiesResultAttr,
  IRejectZeroRiskVulnResultAttr,
  IVulnDataAttr,
  TVulnUpdateResult,
} from "./types";

import type { IVulnRowAttr } from "features/vulnerabilities/types";
import type {
  AddFindingConsultMutation,
  ConfirmVulnerabilitiesMutation,
  ConfirmVulnerabilitiesZeroRiskMutation,
  Exact,
  FindingConsultType,
  InputMaybe,
  RejectVulnerabilitiesMutation,
  RejectVulnerabilitiesZeroRiskMutation,
  VulnerabilityRejectionReason,
} from "gql/graphql";
import { GET_FINDING_HEADER } from "pages/finding/queries";
import { GET_FINDING_INFO } from "pages/finding/vulnerabilities/queries";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";
import { translate } from "utils/translations/translate";

const getInitialTreatment = (
  canHandleVulnsAccept: boolean,
  canConfirmVulnerability: boolean,
  canConfirmZeroRisk: boolean,
): string => {
  if (canHandleVulnsAccept) {
    return "ACCEPTED_UNDEFINED";
  } else if (canConfirmVulnerability) {
    return "CONFIRM_REJECT_VULNERABILITY";
  } else if (canConfirmZeroRisk) {
    return "CONFIRM_REJECT_ZERO_RISK";
  }

  return "";
};

const confirmVulnerabilityHelper = (
  isConfirmRejectVulnerabilitySelected: boolean,
  confirmVulnerabilities: (
    options?: MutationFunctionOptions<
      ConfirmVulnerabilitiesMutation,
      Exact<{ findingId: string; vulnerabilities: string[] | string }>
    >,
  ) => Promise<FetchResult>,
  confirmedVulns: IVulnRowAttr[],
): void => {
  if (isConfirmRejectVulnerabilitySelected && !_.isEmpty(confirmedVulns)) {
    Object.entries(
      _.groupBy(
        confirmedVulns,
        (vulnerability: IVulnRowAttr): string => vulnerability.findingId,
      ),
    ).forEach(
      ([findingId, chunkedVulnerabilities]: [string, IVulnRowAttr[]]): void => {
        const confirmedVulnIds: string[] = chunkedVulnerabilities.map(
          (vulnerability: IVulnRowAttr): string => vulnerability.id,
        );
        confirmVulnerabilities({
          variables: {
            findingId,
            vulnerabilities: confirmedVulnIds,
          },
        }).catch((): void => {
          Logger.error("An error occurred confirming vulnerabilities");
        });
      },
    );
  }
};

const confirmVulnerabilityProps = (
  refetchData: () => void,
  handleCloseModal: () => void,
  findingId?: string,
): MutationHookOptions<
  ConfirmVulnerabilitiesMutation,
  Exact<{ findingId: string; vulnerabilities: string[] | string }>
> => {
  return {
    onCompleted: (data: IConfirmVulnerabilitiesResultAttr): void => {
      if (data.confirmVulnerabilities.success) {
        msgSuccess(
          translate.t("groupAlerts.confirmedVulnerabilitySuccess"),
          translate.t("groupAlerts.updatedTitle"),
        );
        refetchData();
        handleCloseModal();
      }
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        if (
          error.message ===
          "Exception - The vulnerability has not been submitted"
        ) {
          msgError(translate.t("groupAlerts.vulnerabilityIsNotSubmitted"));
        } else if (error.message === "Invalid, missing approved evidence") {
          msgError(translate.t("groupAlerts.missingEvidence"));
        } else if (error.message === "Invalid, still evidence to approve") {
          msgError(translate.t("groupAlerts.pendingEvidence"));
        } else {
          msgError(translate.t("groupAlerts.errorTextsad"));
          Logger.warning("An error occurred confirming vulnerability", error);
        }
      });
    },
    refetchQueries: (): InternalRefetchQueriesInclude =>
      findingId === undefined
        ? []
        : [
            {
              query: GET_FINDING_INFO,
              variables: {
                findingId,
              },
            },
            {
              query: GET_FINDING_HEADER,
              variables: {
                findingId,
              },
            },
          ],
  };
};

const confirmZeroRiskProps = (
  refetchData: () => void,
  handleCloseModal: () => void,
  findingId?: string,
): MutationHookOptions<
  ConfirmVulnerabilitiesZeroRiskMutation,
  Exact<{
    findingId: string;
    justification: string;
    vulnerabilities: InputMaybe<string> | InputMaybe<string>[];
  }>
> => {
  return {
    onCompleted: (data: IConfirmVulnZeroRiskResultAttr): void => {
      if (data.confirmVulnerabilitiesZeroRisk.success) {
        msgSuccess(
          translate.t("groupAlerts.confirmedZeroRiskSuccess"),
          translate.t("groupAlerts.updatedTitle"),
        );
        refetchData();
        handleCloseModal();
      }
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        if (
          error.message ===
          "Exception - Zero risk vulnerability is not requested"
        ) {
          msgError(translate.t("groupAlerts.zeroRiskIsNotRequested"));
        } else {
          msgError(translate.t("groupAlerts.errorTextsad"));
          Logger.warning("An error occurred confirming zero risk vuln", error);
        }
      });
    },
    refetchQueries: (): InternalRefetchQueriesInclude =>
      findingId === undefined
        ? []
        : [
            {
              query: GET_FINDING_INFO,
              variables: {
                findingId,
              },
            },
            {
              query: GET_FINDING_HEADER,
              variables: {
                findingId,
              },
            },
          ],
  };
};

const rejectZeroRiskProps = (
  refetchData: () => void,
  handleCloseModal: () => void,
  findingId?: string,
): MutationHookOptions<
  RejectVulnerabilitiesZeroRiskMutation,
  Exact<{
    findingId: string;
    justification: string;
    vulnerabilities: InputMaybe<string> | InputMaybe<string>[];
  }>
> => {
  return {
    onCompleted: (data: IRejectZeroRiskVulnResultAttr): void => {
      if (data.rejectVulnerabilitiesZeroRisk.success) {
        msgSuccess(
          translate.t("groupAlerts.rejectedZeroRiskSuccess"),
          translate.t("groupAlerts.updatedTitle"),
        );
        refetchData();
        handleCloseModal();
      }
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        if (
          error.message ===
          "Exception - Zero risk vulnerability is not requested"
        ) {
          msgError(translate.t("groupAlerts.zeroRiskIsNotRequested"));
        } else {
          msgError(translate.t("groupAlerts.errorTextsad"));
          Logger.warning("An error occurred rejecting zero risk vuln", error);
        }
      });
    },
    refetchQueries: (): InternalRefetchQueriesInclude =>
      findingId === undefined
        ? []
        : [
            {
              query: GET_FINDING_INFO,
              variables: {
                findingId,
              },
            },
            {
              query: GET_FINDING_HEADER,
              variables: {
                findingId,
              },
            },
          ],
  };
};

const handleSubmitHelper = async (
  handleAcceptance: (
    variables: Record<string, unknown>,
  ) => Promise<TVulnUpdateResult>,
  findingId: string,
  values: { justification: string },
  vulnerabilitiesList: IVulnRowAttr[],
): Promise<TVulnUpdateResult[]> => {
  const maxChunkSize = 64;
  const minChunkSize = 16;
  const vulnerabilitiesListChunks = _.chunk(vulnerabilitiesList, maxChunkSize);
  const acceptanceChunks = vulnerabilitiesListChunks.map(
    (chunkedVulnerabilitiesList): (() => Promise<TVulnUpdateResult[]>) =>
      async (): Promise<TVulnUpdateResult[]> => {
        const chunkedVulnerabilities: IVulnRowAttr[][] = _.chunk(
          chunkedVulnerabilitiesList,
          minChunkSize,
        );
        const allPromises = chunkedVulnerabilities.map(
          async (chunkedVulns): Promise<TVulnUpdateResult> => {
            const approvedVulnsId: string[] = chunkedVulns.reduce(
              (acc: string[], vuln: IVulnRowAttr): string[] =>
                vuln.acceptanceStatus === "APPROVED" ? [...acc, vuln.id] : acc,
              [],
            );
            const rejectedVulnsId: string[] = chunkedVulns.reduce(
              (acc: string[], vuln: IVulnRowAttr): string[] =>
                vuln.acceptanceStatus === "REJECTED" ? [...acc, vuln.id] : acc,
              [],
            );

            return handleAcceptance({
              variables: {
                acceptedVulnerabilities: approvedVulnsId,
                findingId,
                justification: values.justification,
                rejectedVulnerabilities: rejectedVulnsId,
              },
            });
          },
        );

        return Promise.all(allPromises);
      },
  );

  return acceptanceChunks.reduce(
    async (previousValue, currentValue): Promise<TVulnUpdateResult[]> => [
      ...(await previousValue),
      ...(await currentValue()),
    ],
    Promise.resolve<TVulnUpdateResult[]>([]),
  );
};

const isAcceptedUndefinedSelectedHelper = async (
  handleAcceptance: (
    variables: Record<string, unknown>,
  ) => Promise<TVulnUpdateResult>,
  acceptedVulns: IVulnRowAttr[],
  values: {
    justification: string;
  },
  rejectedVulns: IVulnRowAttr[],
): Promise<TVulnUpdateResult[][]> => {
  const vulnerabilitiesList = [...acceptedVulns, ...rejectedVulns];
  const acceptedVulnsIds = acceptedVulns.map(
    (vuln: IVulnRowAttr): string => vuln.id,
  );
  const vulnerabilitiesByFinding = _.groupBy(
    vulnerabilitiesList,
    (vuln: IVulnRowAttr): string => vuln.findingId,
  );
  const acceptanceChunks = Object.entries(vulnerabilitiesByFinding).map(
    ([findingId, chunkedVulnerabilities]: [
      string,
      IVulnRowAttr[],
    ]): (() => Promise<TVulnUpdateResult[][]>) =>
      async (): Promise<TVulnUpdateResult[][]> => {
        return Promise.all([
          handleSubmitHelper(
            handleAcceptance,
            findingId,
            values,
            chunkedVulnerabilities.map(
              (vuln): IVulnRowAttr => ({
                ...vuln,
                acceptanceStatus: _.includes(acceptedVulnsIds, vuln.id)
                  ? "APPROVED"
                  : "REJECTED",
              }),
            ),
          ),
        ]);
      },
  );

  return acceptanceChunks.reduce(
    async (previousValue, currentValue): Promise<TVulnUpdateResult[][]> => [
      ...(await previousValue),
      ...(await currentValue()),
    ],
    Promise.resolve<TVulnUpdateResult[][]>([]),
  );
};

const isConfirmZeroRiskSelectedHelper = (
  isConfirmZeroRiskSelected: boolean,
  confirmZeroRisk: (
    options?: MutationFunctionOptions<
      ConfirmVulnerabilitiesZeroRiskMutation,
      Exact<{
        findingId: string;
        justification: string;
        vulnerabilities: InputMaybe<string> | InputMaybe<string>[];
      }>
    >,
  ) => Promise<FetchResult>,
  acceptedVulns: IVulnDataAttr[],
  values: {
    justification: string;
  },
): void => {
  if (isConfirmZeroRiskSelected && !_.isEmpty(acceptedVulns)) {
    Object.entries(
      _.groupBy(acceptedVulns, (vuln: IVulnDataAttr): string => vuln.findingId),
    ).forEach(
      ([findingId, chunkedVulnerabilities]: [
        string,
        IVulnDataAttr[],
      ]): void => {
        const acceptedVulnIds: string[] = chunkedVulnerabilities.map(
          (vuln: IVulnDataAttr): string => vuln.id,
        );
        confirmZeroRisk({
          variables: {
            findingId,
            justification: values.justification,
            vulnerabilities: acceptedVulnIds,
          },
        }).catch((): void => {
          Logger.error(
            "An error occurred while confirming the vulnerabilities",
          );
        });
      },
    );
  }
};

const isRejectZeroRiskSelectedHelper = (
  isRejectZeroRiskSelected: boolean,
  rejectZeroRisk: (
    options?: MutationFunctionOptions<
      RejectVulnerabilitiesZeroRiskMutation,
      Exact<{
        findingId: string;
        justification: string;
        vulnerabilities: InputMaybe<string> | InputMaybe<string>[];
      }>
    >,
  ) => Promise<FetchResult>,
  values: {
    justification: string;
  },
  rejectedVulns: IVulnDataAttr[],
): void => {
  if (isRejectZeroRiskSelected && !_.isEmpty(rejectedVulns)) {
    Object.entries(
      _.groupBy(rejectedVulns, (vuln: IVulnDataAttr): string => vuln.findingId),
    ).forEach(
      ([findingId, chunkedVulnerabilities]: [
        string,
        IVulnDataAttr[],
      ]): void => {
        const rejectedVulnIds: string[] = chunkedVulnerabilities.map(
          (vuln: IVulnDataAttr): string => vuln.id,
        );
        rejectZeroRisk({
          variables: {
            findingId,
            justification: values.justification,
            vulnerabilities: rejectedVulnIds,
          },
        }).catch((): void => {
          Logger.error("An error occurred while rejecting the vulnerabilities");
        });
      },
    );
  }
};

const rejectVulnerabilityHelper = (
  isConfirmRejectVulnerabilitySelected: boolean,
  rejectVulnerabilities: (
    options?: MutationFunctionOptions<
      RejectVulnerabilitiesMutation,
      Exact<{
        findingId: string;
        reasons: VulnerabilityRejectionReason | VulnerabilityRejectionReason[];
        otherReason?: InputMaybe<string>;
        vulnerabilities: string[] | string;
      }>
    >,
  ) => Promise<FetchResult>,
  values: {
    reasons: string[];
    otherReason?: string;
  },
  rejectedVulns: IVulnRowAttr[],
): void => {
  if (isConfirmRejectVulnerabilitySelected && !_.isEmpty(rejectedVulns)) {
    Object.entries(
      _.groupBy(
        rejectedVulns,
        (vulnerability: IVulnRowAttr): string => vulnerability.findingId,
      ),
    ).forEach(
      ([findingId, chunkedVulnerabilities]: [string, IVulnRowAttr[]]): void => {
        const rejectedVulnIds: string[] = chunkedVulnerabilities.map(
          (vulnerability: IVulnRowAttr): string => vulnerability.id,
        );
        rejectVulnerabilities({
          variables: {
            findingId,
            otherReason: values.otherReason,
            reasons: values.reasons as VulnerabilityRejectionReason[],
            vulnerabilities: rejectedVulnIds,
          },
        }).catch((): void => {
          Logger.error("An error occurred while rejecting the vulnerabilities");
        });
      },
    );
  }
};

const rejectVulnerabilityProps = (
  refetchData: () => void,
  handleCloseModal: () => void,
  findingId?: string,
): MutationHookOptions<
  RejectVulnerabilitiesMutation,
  Exact<{
    findingId: string;
    reasons: VulnerabilityRejectionReason | VulnerabilityRejectionReason[];
    otherReason?: InputMaybe<string>;
    vulnerabilities: string[] | string;
  }>
> => {
  return {
    onCompleted: (data: IRejectVulnerabilitiesResultAttr): void => {
      if (data.rejectVulnerabilities.success) {
        msgSuccess(
          translate.t("groupAlerts.rejectedVulnerabilitySuccess"),
          translate.t("groupAlerts.updatedTitle"),
        );
        refetchData();
        handleCloseModal();
      }
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        if (
          error.message ===
          "Exception - Zero risk vulnerability is not requested"
        ) {
          msgError(translate.t("groupAlerts.zeroRiskIsNotRequested"));
        } else {
          msgError(translate.t("groupAlerts.errorTextsad"));
          Logger.warning("An error occurred confirming zero risk vuln", error);
        }
      });
    },
    refetchQueries: (): InternalRefetchQueriesInclude =>
      findingId === undefined
        ? []
        : [
            {
              query: GET_FINDING_INFO,
              variables: {
                findingId,
              },
            },
            {
              query: GET_FINDING_HEADER,
              variables: {
                findingId,
              },
            },
          ],
  };
};

const addObservationRejectionProps = (
  type: string,
): MutationHookOptions<
  AddFindingConsultMutation,
  Exact<{
    content: string;
    findingId: string;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    parentComment: any;
    type: FindingConsultType;
  }>
> => {
  return {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(translate.t("groupAlerts.errorTextsad"));
        Logger.warning(`An error occurred posting ${type}`, error);
      });
    },
  };
};

export {
  addObservationRejectionProps,
  confirmVulnerabilityHelper,
  confirmVulnerabilityProps,
  confirmZeroRiskProps,
  getInitialTreatment,
  isAcceptedUndefinedSelectedHelper,
  isConfirmZeroRiskSelectedHelper,
  isRejectZeroRiskSelectedHelper,
  rejectVulnerabilityHelper,
  rejectVulnerabilityProps,
  rejectZeroRiskProps,
};
