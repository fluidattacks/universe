import type { IVulnRowAttr } from "features/vulnerabilities/types";

interface ISubmittedTableProps {
  acceptanceVulns: IVulnRowAttr[];
  isConfirmRejectVulnerabilitySelected: boolean;
  displayGlobalColumns?: boolean;
  setAcceptanceVulns: (vulns: IVulnRowAttr[]) => void;
  refetchData: () => void;
}

export type { ISubmittedTableProps };
