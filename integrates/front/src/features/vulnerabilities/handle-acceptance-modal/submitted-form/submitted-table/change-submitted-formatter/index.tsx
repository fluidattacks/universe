import { ConfirmVulnerabilityCheckBox } from "../confirm-vulnerability-check-box";
import type { IVulnRowAttr } from "features/vulnerabilities/types";

export const changeSubmittedFormatter = (
  row: IVulnRowAttr,
  approveFunction: (arg1?: IVulnRowAttr) => void,
  deleteFunction: (arg1?: IVulnRowAttr) => void,
): JSX.Element => {
  return (
    <ConfirmVulnerabilityCheckBox
      approveFunction={approveFunction}
      deleteFunction={deleteFunction}
      vulnerabilityRow={row}
    />
  );
};
