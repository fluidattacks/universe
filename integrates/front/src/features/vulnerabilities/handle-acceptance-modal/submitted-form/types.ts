import type { IUseModal } from "@fluidattacks/design";

import type { IVulnRowAttr } from "../../types";

interface ISubmittedFormProps {
  findingId?: string;
  displayGlobalColumns?: boolean;
  onCancel: () => void;
  refetchData: () => void;
  vulnerabilities: IVulnRowAttr[];
  modalRef: IUseModal;
  title?: string;
}

interface IFormValues {
  justification?: string;
  rejectionReasons: string[];
  otherRejectionReason?: string;
}

export type { IFormValues, ISubmittedFormProps };
