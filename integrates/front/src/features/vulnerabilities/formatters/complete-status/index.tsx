import { Tag } from "@fluidattacks/design";
import capitalize from "lodash/capitalize";
import * as React from "react";

import type { IStatus } from "../status";
import { getTagVariant } from "utils/colors";

const CompleteStatus: React.FC<IStatus> = ({
  status,
}: IStatus): JSX.Element => {
  if (["-", "", undefined, null].includes(status)) return <span />;

  const formatedStatus: string = capitalize(status).replace("_", " ");
  const currentStateVariant = getTagVariant(capitalize(status));

  return (
    <Tag
      priority={currentStateVariant === "inactive" ? "default" : "low"}
      tagLabel={formatedStatus}
      variant={currentStateVariant}
    />
  );
};

export { CompleteStatus };
