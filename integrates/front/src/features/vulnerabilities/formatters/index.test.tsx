import type { ColumnDef } from "@tanstack/react-table";
import { screen } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import * as React from "react";

import { statusFormatter } from "./status";

import { Table } from "components/table";
import type { ITableOptions } from "components/table/types";
import { useTable } from "hooks";
import { render } from "mocks";

describe("formatter", (): void => {
  interface IRandomData {
    currentState: string;
    where?: string;
    specific?: string;
    verification?: string;
    vulnerabilityType?: string;
    treatment?: string;
  }

  const columns: ColumnDef<IRandomData>[] = [
    {
      accessorKey: "currentState",
      cell: (cell): JSX.Element => statusFormatter(String(cell.getValue())),
      header: "Status",
    },
  ];

  const columnsCompleteStatus: ColumnDef<IRandomData>[] = [
    {
      accessorKey: "currentState",
      cell: (cell): JSX.Element =>
        statusFormatter(String(cell.getValue()), true),
      header: "Status",
    },
  ];

  const data: IRandomData[] = [
    {
      currentState: "Active",
    },
    {
      currentState: "Closed",
    },
    {
      currentState: "OK",
    },
    {
      currentState: "Verified (closed)",
    },
    {
      currentState: "Accepted",
    },
    {
      currentState: "In progress",
    },
    {
      currentState: "New",
    },
    {
      currentState: "On_hold",
    },
    {
      currentState: "Pending verification",
    },
    {
      currentState: "Permanently accepted",
    },
    {
      currentState: "Temporarily accepted",
    },
    {
      currentState: "Disabled",
    },
    {
      currentState: "Open",
    },
    {
      currentState: "Unsolved",
    },
    {
      currentState: "Verified (open)",
    },
    {
      currentState: "N/A",
    },
    {
      currentState: "Should be gray",
    },
    {
      currentState: "App",
    },
    {
      currentState: "Code",
    },
  ];
  interface ITestComponentProps {
    testColumns: ColumnDef<IRandomData>[];
    testData: IRandomData[];
    testOptions?: ITableOptions<IRandomData>;
  }
  const TestComponent: React.FC<ITestComponentProps> = ({
    testColumns,
    testData,
    testOptions,
  }): JSX.Element => {
    const tableRef = useTable("testTable");

    return (
      <Table
        columns={testColumns}
        data={testData}
        options={testOptions}
        tableRef={tableRef}
      />
    );
  };

  it("should filter select", async (): Promise<void> => {
    expect.hasAssertions();

    render(<TestComponent testColumns={columns} testData={data} />);

    expect(screen.getByRole("table")).toBeInTheDocument();

    await userEvent.click(screen.getByText("1 - 10 of 19"));

    expect(screen.queryAllByRole("row")).toHaveLength(11);
  });

  it("should not have a long text in status format", async (): Promise<void> => {
    expect.hasAssertions();

    render(<TestComponent testColumns={columns} testData={data} />);

    expect(screen.getByRole("table")).toBeInTheDocument();

    await userEvent.click(screen.getByText("1 - 10 of 19"));

    expect(screen.queryByText("Pending verification")).not.toBeInTheDocument();
    expect(screen.queryByText("Permanently accepted")).not.toBeInTheDocument();
    expect(screen.queryByText("Temporarily accepted")).not.toBeInTheDocument();
    expect(screen.queryByText("Should be gray")).not.toBeInTheDocument();
    expect(screen.queryByText("Verified (closed)")).not.toBeInTheDocument();
    expect(screen.queryByText("Verified (open)")).not.toBeInTheDocument();
  });

  it("should have a long text in complete status format", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <TestComponent testColumns={columnsCompleteStatus} testData={data} />,
    );

    expect(screen.getByRole("table")).toBeInTheDocument();

    await userEvent.click(screen.getByText("1 - 10 of 19"));

    expect(screen.getByText("Pending verification")).toBeInTheDocument();
    expect(screen.getByText("Permanently accepted")).toBeInTheDocument();
    expect(screen.getByText("In progress")).toBeInTheDocument();
    expect(screen.getByText("Verified (closed)")).toBeInTheDocument();
  });

  it("should not have value on complete status", (): void => {
    expect.hasAssertions();

    const dataCompleteStatus: IRandomData[] = [
      {
        currentState: "-",
      },
    ];

    render(
      <TestComponent
        testColumns={columnsCompleteStatus}
        testData={dataCompleteStatus}
      />,
    );

    expect(screen.getByRole("table")).toBeInTheDocument();
    expect(screen.queryAllByRole("row")).toHaveLength(2);
    expect(screen.queryByText("-")).not.toBeInTheDocument();
  });
});
