import { Tag } from "@fluidattacks/design";
import _ from "lodash";
import * as React from "react";

interface ITechnique {
  readonly technique: string | undefined;
}

const Technique: React.FC<ITechnique> = ({
  technique,
}: ITechnique): JSX.Element => {
  const formatedTechnique: string = _.toUpper(technique);

  return (
    <Tag fontSize={"14px"} tagLabel={formatedTechnique} variant={"technique"} />
  );
};

const techniqueFormatter = (value: string | undefined): JSX.Element => {
  return <Technique technique={value} />;
};

export type { ITechnique };
export { techniqueFormatter, Technique };
