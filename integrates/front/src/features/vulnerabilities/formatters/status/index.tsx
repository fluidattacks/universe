import { Tag } from "@fluidattacks/design";
import capitalize from "lodash/capitalize";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { CompleteStatus } from "../complete-status";
import { getTagVariant } from "utils/colors";

interface IStatus {
  readonly size?: string;
  readonly status: string | undefined;
}

const Status: React.FC<IStatus> = ({ size, status }: IStatus): JSX.Element => {
  const { t } = useTranslation();
  const formatedStatus: string = capitalize(status);
  const currentStateVariant = getTagVariant(capitalize(status));
  const tagText =
    formatedStatus === "On_hold"
      ? t("searchFindings.tabVuln.onHold")
      : formatedStatus.split(" ")[0];

  return (
    <Tag
      fontSize={size}
      priority={currentStateVariant === "inactive" ? "default" : "low"}
      tagLabel={tagText}
      variant={currentStateVariant}
    />
  );
};

const statusFormatter = (
  value: string | undefined,
  completeStatus = false,
): JSX.Element => {
  return completeStatus ? (
    <CompleteStatus status={value} />
  ) : (
    <Status status={value} />
  );
};

export type { IStatus };
export { statusFormatter, Status };
