import { PureAbility } from "@casl/ability";
import type { ColumnDef } from "@tanstack/react-table";
import { act, screen, within } from "@testing-library/react";
import dayjs from "dayjs";
import { useState } from "react";
import * as React from "react";

import { statusFormatter } from "./formatters/status";
import type { IVulnRowAttr } from "./types";
import { formatVulnerabilitiesReason, getTimeToDetect } from "./utils";

import { VulnComponent } from ".";
import { Filters } from "components/filter";
import type { IFilter } from "components/filter/types";
import { filterDate } from "components/table/utils";
import { authzPermissionsContext } from "context/authz/config";
import { RootCriticality, VulnerabilityStateReason } from "gql/graphql";
import { useVulnerabilityStore } from "hooks/use-vulnerability-store";
import { render } from "mocks";

describe("vulnComponent", (): void => {
  const initialStoreState = useVulnerabilityStore.getState();

  // eslint-disable-next-line jest/no-hooks
  beforeEach((): void => {
    useVulnerabilityStore.setState(initialStoreState, true);
  });

  const numberOfDaysOldThanAWeek = 12;
  const numberOfDays = 5;
  const memoryRouter = {
    initialEntries: ["/orgs/okada/groups/TEST/vulns/438679960/locations"],
  };
  const columns: ColumnDef<IVulnRowAttr>[] = [
    {
      accessorKey: "where",
      enableColumnFilter: false,
    },
    {
      accessorKey: "specific",
      enableColumnFilter: false,
    },
    {
      accessorKey: "state",
      cell: (cell): JSX.Element => statusFormatter(String(cell.getValue())),
      meta: { filterType: "select" },
    },
    {
      accessorKey: "reportDate",
      filterFn: filterDate,
      meta: { filterType: "dateRange" },
    },
    {
      accessorKey: "verification",
      meta: { filterType: "select" },
    },
    {
      accessorKey: "treatmentStatus",
      meta: { filterType: "select" },
    },
    {
      accessorKey: "tag",
    },
    {
      accessorKey: "treatmentAcceptanceStatus",
      meta: { filterType: "select" },
    },
    {
      accessorKey: "treatmentAssigned",
      meta: { filterType: "select" },
    },
  ];
  const mockedPermissions = new PureAbility<string>([
    {
      action:
        "integrates_api_mutations_request_vulnerabilities_zero_risk_mutate",
    },
    {
      action: "integrates_api_mutations_update_vulnerability_treatment_mutate",
    },
    {
      action:
        "integrates_api_mutations_update_vulnerabilities_treatment_mutate",
    },
  ]);
  const mocks: IVulnRowAttr[] = [
    {
      advisories: null,
      assigned: "",
      closingDate: null,
      customSeverity: 3,
      externalBugTrackingSystem: null,
      findingId: "438679960",
      groupName: "test",
      historicTreatment: [
        {
          acceptanceDate: "",
          acceptanceStatus: "",
          assigned: "assigned-user-1",
          date: "2019-07-05 09:56:40",
          justification: "test progress justification",
          treatment: "IN PROGRESS",
          user: "usertreatment@test.test",
        },
      ],
      id: "89521e9a-b1a3-4047-a16e-15d530dc1340",
      lastEditedBy: "usertreatment@test.test",
      lastStateDate: "2019-07-05 09:56:40",
      lastTreatmentDate: "2019-07-05 09:56:40",
      lastVerificationDate: null,
      organizationName: undefined,
      priority: 0,
      proposedSeverityAuthor: null,
      proposedSeverityTemporalScore: null,
      proposedSeverityThreatScore: null,
      proposedSeverityVector: null,
      proposedSeverityVectorV4: null,
      remediated: true,
      reportDate: "",
      root: {
        __typename: "GitRoot",
        branch: "master",
        criticality: RootCriticality.Low,
      },
      rootNickname: "https:",
      severityTemporalScore: 3.0,
      severityThreatScore: 3.0,
      severityVector: "AV:A/AC:H/PR:L/UI:R/S:C/C:L/I:L/A:L/E:F/RL:O/RC:U",
      severityVectorV4:
        "CVSS:4.0/AV:A/AC:H/PR:L/UI:P/VC:L/VI:L/VA:L/SC:L/SI:L/SA:L/E:A",
      source: "asm",
      specific: "specific-1",
      state: "VULNERABLE",
      stateReasons: null,
      stream: null,
      tag: "tag-1, tag-2",
      technique: "SCR",
      treatmentAcceptanceDate: "",
      treatmentAcceptanceStatus: "",
      treatmentAssigned: "assigned-user-1",
      treatmentDate: "2019-07-05 09:56:40",
      treatmentJustification: "test progress justification",
      treatmentStatus: "",
      treatmentUser: "usertreatment@test.test",
      verification: "Requested",
      verificationJustification: null,
      vulnerabilityType: "inputs",
      where: "https://example.com/inputs",
      zeroRisk: "Requested",
    },
    {
      advisories: null,
      assigned: "",
      closingDate: null,
      customSeverity: 1,
      externalBugTrackingSystem: null,
      findingId: "438679960",
      groupName: "test",
      historicTreatment: [
        {
          acceptanceDate: "",
          acceptanceStatus: "",
          assigned: "assigned-user-3",
          date: "2019-07-05 09:56:40",
          justification: "test progress justification",
          treatment: "IN PROGRESS",
          user: "usertreatment@test.test",
        },
      ],
      id: "a09c79fc-33fb-4abd-9f20-f3ab1f500bd0",
      lastEditedBy: "usertreatment@test.test",
      lastStateDate: "2019-07-05 09:56:40",
      lastTreatmentDate: "2019-07-05 09:56:40",
      lastVerificationDate: dayjs()
        .subtract(numberOfDays, "days")
        .format("YYYY-MM-DD hh:mm:ss A"),
      organizationName: undefined,
      priority: 0,
      proposedSeverityAuthor: null,
      proposedSeverityTemporalScore: null,
      proposedSeverityThreatScore: null,
      proposedSeverityVector: null,
      proposedSeverityVectorV4: null,
      remediated: false,
      reportDate: "",
      root: {
        __typename: "GitRoot",
        branch: "master",
        criticality: RootCriticality.Low,
      },
      rootNickname: "https:",
      severityTemporalScore: 1.0,
      severityThreatScore: 1.0,
      severityVector: "AV:A/AC:H/PR:L/UI:R/S:C/C:L/I:L/A:L/E:F/RL:O/RC:U",
      severityVectorV4:
        "CVSS:4.0/AV:A/AC:H/PR:L/UI:P/VC:L/VI:L/VA:L/SC:L/SI:L/SA:L/E:A",
      source: "asm",
      specific: "specific-2",
      state: "SAFE",
      stateReasons: null,
      stream: null,
      tag: "tag-5, tag-6",
      technique: "SCR",
      treatmentAcceptanceDate: "",
      treatmentAcceptanceStatus: "",
      treatmentAssigned: "assigned-user-3",
      treatmentDate: "2019-07-05 09:56:40",
      treatmentJustification: "test progress justification",
      treatmentStatus: "",
      treatmentUser: "usertreatment@test.test",
      verification: "Verified",
      verificationJustification: null,
      vulnerabilityType: "lines",
      where: "https://example.com/lines",
      zeroRisk: null,
    },
    {
      advisories: null,
      assigned: "assigned-user-4",
      closingDate: null,
      customSeverity: 1,
      externalBugTrackingSystem: null,
      findingId: "438679960",
      groupName: "test",
      historicTreatment: [
        {
          acceptanceDate: "",
          acceptanceStatus: "",
          assigned: "assigned-user-4",
          date: "2019-07-05 09:56:40",
          justification: "test progress justification",
          treatment: "IN PROGRESS",
          user: "usertreatment@test.test",
        },
      ],
      id: "af7a48b8-d8fc-41da-9282-d424fff563f0",
      lastEditedBy: "usertreatment@test.test",
      lastStateDate: "2019-07-05 09:56:40",
      lastTreatmentDate: "2019-07-05 09:56:40",
      lastVerificationDate: dayjs()
        .subtract(numberOfDaysOldThanAWeek, "days")
        .format("YYYY-MM-DD hh:mm:ss A"),
      organizationName: undefined,
      priority: 0,
      proposedSeverityAuthor: null,
      proposedSeverityTemporalScore: null,
      proposedSeverityThreatScore: null,
      proposedSeverityVector: null,
      proposedSeverityVectorV4: null,
      remediated: false,
      reportDate: "",
      root: {
        __typename: "GitRoot",
        branch: "master",
        criticality: RootCriticality.Low,
      },
      rootNickname: "https:",
      severityTemporalScore: 1.0,
      severityThreatScore: 1.1,
      severityVector: "AV:A/AC:H/PR:L/UI:R/S:C/C:L/I:L/A:L/E:F/RL:O/RC:U",
      severityVectorV4:
        "CVSS:4.0/AV:A/AC:H/PR:L/UI:P/VC:L/VI:L/VA:L/SC:L/SI:L/SA:L/E:A",
      source: "asm",
      specific: "specific-3",
      state: "VULNERABLE",
      stateReasons: null,
      stream: null,
      tag: "tag-7, tag-8",
      technique: "SCR",
      treatmentAcceptanceDate: "",
      treatmentAcceptanceStatus: "",
      treatmentAssigned: "assigned-user-4",
      treatmentDate: "2019-07-05 09:56:40",
      treatmentJustification: "test progress justification",
      treatmentStatus: "IN PROGRESS",
      treatmentUser: "usertreatment@test.test",
      verification: "Verified",
      verificationJustification: null,
      vulnerabilityType: "lines",
      where: "https://example.com/lines",
      zeroRisk: null,
    },
  ];

  interface ITestComponentProps {
    enabledRows: boolean;
    handleRefetchData: jest.Mock;
    isFindingReleased: boolean;
  }
  const TestComponent: React.FC<ITestComponentProps> = ({
    enabledRows,
    handleRefetchData,
    isFindingReleased,
  }): JSX.Element => {
    interface IRandomData {
      dummyFilter: string;
    }

    const [filters, setFilters] = useState<IFilter<IRandomData>[]>([
      {
        id: "dummyFilter",
        key: "dummyFilter",
        label: "Dummy Filter",
        type: "text",
      },
    ]);

    return (
      <VulnComponent
        columns={columns}
        extraButtons={<div />}
        filters={<Filters filters={filters} setFilters={setFilters} />}
        onVulnSelect={jest.fn()}
        refetchData={handleRefetchData}
        tableOptions={{ enableRowSelection: enabledRows }}
        tableRefProps={{ id: "test" }}
        vulnStateProps={{
          isFindingReleased,
        }}
        vulnerabilities={mocks}
      />
    );
  };

  it("should render in vulnerabilities", (): void => {
    expect.hasAssertions();

    const handleRefetchData: jest.Mock = jest.fn();

    act((): void => {
      useVulnerabilityStore.setState({
        isEditing: true,
        isReattacking: false,
        isVerifying: false,
      });
    });

    const { rerender } = render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <TestComponent
          enabledRows={true}
          handleRefetchData={handleRefetchData}
          isFindingReleased={true}
        />
      </authzPermissionsContext.Provider>,
      {
        memoryRouter,
      },
    );

    expect(screen.queryByRole("table")).toBeInTheDocument();
    expect(
      within(screen.getAllByRole("row")[1]).getByRole("checkbox"),
    ).not.toBeDisabled();
    expect(
      within(screen.getAllByRole("row")[3]).getByRole("checkbox"),
    ).not.toBeDisabled();

    act((): void => {
      useVulnerabilityStore.setState({
        isEditing: false,
        isReattacking: true,
        isVerifying: false,
      });
    });

    rerender(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <TestComponent
          enabledRows={false}
          handleRefetchData={handleRefetchData}
          isFindingReleased={true}
        />
      </authzPermissionsContext.Provider>,
    );

    expect(
      within(screen.getAllByRole("row")[1]).getByRole("checkbox"),
    ).toBeDisabled();
    expect(
      within(screen.getAllByRole("row")[3]).getByRole("checkbox"),
    ).toBeDisabled();
    expect(handleRefetchData).not.toHaveBeenCalled();
  });

  it("should render in vulnerabilities in draft", (): void => {
    expect.hasAssertions();

    const handleRefetchData: jest.Mock = jest.fn();

    act((): void => {
      useVulnerabilityStore.setState({
        isEditing: true,
      });
    });

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <TestComponent
          enabledRows={false}
          handleRefetchData={handleRefetchData}
          isFindingReleased={false}
        />
      </authzPermissionsContext.Provider>,
      {
        memoryRouter,
      },
    );

    expect(screen.queryByRole("table")).toBeInTheDocument();
    expect(
      screen.queryByText("searchFindings.tabDescription.editVuln"),
    ).not.toBeInTheDocument();
    expect(screen.queryAllByRole("button")).toHaveLength(1);
    expect(screen.queryAllByRole("checkbox")).toHaveLength(0);
    expect(handleRefetchData).not.toHaveBeenCalled();
  });

  it("should return capitalized strings", (): void => {
    expect.hasAssertions();

    const reasons = [
      VulnerabilityStateReason.Consistency,
      VulnerabilityStateReason.Duplicated,
      VulnerabilityStateReason.Evidence,
    ];
    const result = formatVulnerabilitiesReason(reasons);

    expect(result).toStrictEqual(["Consistency", "Duplicated", "Evidence"]);
  });

  it("should return null when reasons is null", (): void => {
    expect.hasAssertions();

    const reasons = null;
    const result = formatVulnerabilitiesReason(reasons);

    expect(result).toBeNull();
  });

  it("should return time to detect", (): void => {
    expect.hasAssertions();

    const reportDate = "2019-09-12 08:45:48";
    const commitDate = "2019-01-01T00:00:00+00:00";
    const result = getTimeToDetect(reportDate, commitDate);

    expect(result).toBe(254);
  });

  it("should return undefined time to detect if commit date is greater than report date", (): void => {
    expect.hasAssertions();

    const reportDate = "2019-01-01T00:00:00+00:00";
    const commitDate = "2019-09-12 08:45:48";
    const result = getTimeToDetect(reportDate, commitDate);

    expect(result).toBeUndefined();
  });

  it("should return undefined time to detect if a commit date is null", (): void => {
    expect.hasAssertions();

    const reportDate = "2019-01-01T00:00:00+00:00";
    const commitDate = null;
    const result = getTimeToDetect(reportDate, commitDate);

    expect(result).toBeUndefined();
  });
});
