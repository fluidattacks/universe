import type { ApolloError } from "@apollo/client";
import { useMutation } from "@apollo/client";
import { useModal } from "@fluidattacks/design";
import { useCallback, useMemo, useState } from "react";
import { useTranslation } from "react-i18next";

import {
  REQUEST_VULNERABILITIES_VERIFICATION,
  VERIFY_VULNERABILITIES,
} from "./queries";
import { ReportedBy } from "./reported-by";
import type { IUpdateVerificationModal, IVulnData } from "./types";
import {
  getAreAllChunkedMutationValid,
  handleAltSubmitHelper,
  handleRequestVerification,
  handleRequestVerificationError,
  handleVerifyRequest,
  handleVerifyRequestError,
} from "./utils";
import { VulnerabilitiesToVerify } from "./vulnerabilities-to-verify";

import { RemediationModal } from "features/remediation-modal";
import type { IRemediationValues } from "features/remediation-modal/types";
import { VulnerabilityStateReason } from "gql/graphql";
import { useTable, useVulnerabilityStore } from "hooks";

const UpdateVerificationModal = ({
  vulns,
  clearSelected,
  handleCloseModal,
  setRequestState,
  setVerifyState,
  refetchData,
  refetchFindingAndGroup = undefined,
  refetchFindingHeader = undefined,
}: IUpdateVerificationModal): JSX.Element => {
  const { t } = useTranslation();
  const tableRef = useTable("vulnstoverify");
  const remediationModal = useModal("remediation-modal");
  const { isReattacking, isVerifying } = useVulnerabilityStore();

  // State management
  const [vulnerabilitiesList, setVulnerabilitiesList] = useState(vulns);
  const [isOpen, setIsOpen] = useState(true);
  const [isRunning, setIsRunning] = useState(false);
  const closeRemediationModal = useCallback((): void => {
    handleCloseModal();
  }, [handleCloseModal]);

  const allHackersAreMachine = useMemo(
    (): boolean =>
      vulnerabilitiesList.every((vuln): boolean => vuln.source === "machine"),
    [vulnerabilitiesList],
  );

  const values = useMemo(
    (): IRemediationValues => ({
      treatmentJustification: allHackersAreMachine
        ? t(
            "searchFindings.tabDescription.remediationModal.machineJustification",
          )
        : "",
      verificationReason: VulnerabilityStateReason.VerifiedAsSafe,
    }),
    [allHackersAreMachine, t],
  );

  // GraphQL operations
  const [requestVerification, { loading: submittingRequest }] = useMutation(
    REQUEST_VULNERABILITIES_VERIFICATION,
  );

  const [verifyRequest, { loading: submittingVerify }] = useMutation(
    VERIFY_VULNERABILITIES,
  );

  const handleSubmit = useCallback(
    async (formikValues: IRemediationValues): Promise<void> => {
      setIsRunning(true);
      try {
        const results = await handleAltSubmitHelper(
          requestVerification,
          verifyRequest,
          formikValues,
          vulnerabilitiesList,
          isReattacking,
        );
        const areAllMutationValid = getAreAllChunkedMutationValid(results);
        if (areAllMutationValid.every(Boolean)) {
          refetchData();
          if (refetchFindingAndGroup !== undefined) {
            refetchFindingAndGroup();
          }
          if (isVerifying && refetchFindingHeader !== undefined) {
            refetchFindingHeader();
          }
          if (isReattacking) {
            handleRequestVerification(clearSelected, setRequestState, true);
          } else {
            handleVerifyRequest(
              clearSelected,
              setVerifyState,
              true,
              vulns.length,
            );
          }
        }
      } catch (requestError: unknown) {
        if (isReattacking) {
          (requestError as ApolloError).graphQLErrors.forEach((error): void => {
            handleRequestVerificationError(error);
          });
        } else {
          (requestError as ApolloError).graphQLErrors.forEach((error): void => {
            handleVerifyRequestError(error);
          });
        }
      } finally {
        setIsRunning(false);
        closeRemediationModal();
      }
    },
    [
      clearSelected,
      closeRemediationModal,
      isReattacking,
      isVerifying,
      requestVerification,
      refetchData,
      refetchFindingAndGroup,
      refetchFindingHeader,
      setRequestState,
      setVerifyState,
      verifyRequest,
      vulnerabilitiesList,
      vulns.length,
    ],
  );

  const handleOnChange = useCallback((): void => {
    setIsOpen((currentValue: boolean): boolean => {
      const newVulnList = vulnerabilitiesList.map(
        (vuln): IVulnData => ({
          ...vuln,
          state: currentValue ? "SAFE" : "VULNERABLE",
        }),
      );
      setVulnerabilitiesList([...newVulnList]);

      return !currentValue;
    });
  }, [vulnerabilitiesList]);

  function getAlertMessage(): string | undefined {
    const [head] = vulnerabilitiesList;
    const vulnsLineType = vulnerabilitiesList.filter(
      (vuln): boolean => vuln.vulnerabilityType === "code",
    );
    const vulnsLineTypeAndSameRepo: IVulnData[] = vulnerabilitiesList.filter(
      (vuln): boolean =>
        vuln.rootNickname === head.rootNickname &&
        vuln.root?.branch === head.root?.branch,
    );

    if (isReattacking && vulnsLineType.length === vulnerabilitiesList.length) {
      if (vulnsLineTypeAndSameRepo.length === vulnerabilitiesList.length) {
        return t(
          "searchFindings.tabDescription.remediationModal.alertMessageOne",
          {
            branch: head.root?.branch,
            repository: head.rootNickname,
          },
        );
      }

      return t(
        "searchFindings.tabDescription.remediationModal.alertMessageMultiple",
      );
    }

    return undefined;
  }

  return (
    <RemediationModal
      alertMessage={getAlertMessage()}
      allHackersAreMachine={allHackersAreMachine}
      isLoading={submittingRequest || submittingVerify || isRunning}
      message={
        isReattacking
          ? t("searchFindings.tabDescription.remediationModal.justification")
          : t("searchFindings.tabDescription.remediationModal.observations")
      }
      modalRef={{
        ...remediationModal,
        close: closeRemediationModal,
        isOpen: true,
      }}
      onSubmit={handleSubmit}
      remediationType={"verifyVulnerability"}
      title={
        isReattacking
          ? t("searchFindings.tabDescription.remediationModal.titleRequest")
          : t(
              "searchFindings.tabDescription.remediationModal.titleObservations",
            )
      }
      values={values}
    >
      {allHackersAreMachine && isReattacking ? <ReportedBy /> : undefined}
      {isVerifying ? (
        <VulnerabilitiesToVerify
          handleOnChange={handleOnChange}
          isOpen={isOpen}
          setVulnerabilitiesList={setVulnerabilitiesList}
          tableRef={tableRef}
          vulnerabilitiesList={vulnerabilitiesList}
        />
      ) : undefined}
    </RemediationModal>
  );
};

export { UpdateVerificationModal };
