import { Container, Icon, Text, Tooltip } from "@fluidattacks/design";
import { useTranslation } from "react-i18next";

const ReportedBy = (): JSX.Element => {
  const { t } = useTranslation();

  return (
    <Container>
      <Text size={"sm"}>{"Justification is not required."}</Text>
      <br />
      <Text display={"inline-block"} mr={0.5} size={"sm"}>
        {t(
          "searchFindings.tabDescription.remediationModal.machineJustification",
        )}
      </Text>
      <Tooltip id={""}>
        <Icon icon={"robot"} iconSize={"xs"} iconType={"fa-light"} />
      </Tooltip>
    </Container>
  );
};

export { ReportedBy };
