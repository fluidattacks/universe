import type { ExecutionResult } from "graphql";

import type { IGitRoot } from "../types";

interface IRequestVulnVerificationResult {
  requestVulnerabilitiesVerification: {
    success: boolean;
  };
}

interface IVerifyRequestVulnResult {
  verifyVulnerabilitiesRequest: {
    success: boolean;
  };
}

interface IVulnData {
  findingId: string;
  groupName: string;
  hacker?: string;
  id: string;
  root: IGitRoot | null;
  rootNickname: string | null;
  source: string;
  specific: string;
  state: "REJECTED" | "SAFE" | "SUBMITTED" | "VULNERABLE";
  vulnerabilityType: string;
  where: string;
}

interface IUpdateVerificationModal {
  vulns: IVulnData[];
  clearSelected: () => void;
  handleCloseModal: () => void;
  setRequestState: () => void;
  setVerifyState: () => void;
  refetchData: () => void;
  refetchFindingAndGroup?: () => void;
  refetchFindingHeader?: () => void;
}

type TReattackVulnerabilitiesResult =
  ExecutionResult<IRequestVulnVerificationResult>;
type TVerifyVulnerabilitiesResult = ExecutionResult<IVerifyRequestVulnResult>;

type TVerificationResult =
  | TReattackVulnerabilitiesResult[]
  | TVerifyVulnerabilitiesResult[];

export type {
  IRequestVulnVerificationResult,
  IUpdateVerificationModal,
  IVerifyRequestVulnResult,
  IVulnData,
  TReattackVulnerabilitiesResult,
  TVerificationResult,
  TVerifyVulnerabilitiesResult,
};
