import { graphql } from "gql";

const REQUEST_VULNERABILITIES_VERIFICATION = graphql(`
  mutation requestVulnerabilitiesVerification(
    $findingId: String!
    $justification: String!
    $vulnerabilities: [String]!
  ) {
    requestVulnerabilitiesVerification(
      findingId: $findingId
      justification: $justification
      vulnerabilities: $vulnerabilities
    ) {
      success
    }
  }
`);

const VERIFY_VULNERABILITIES = graphql(`
  mutation VerifyVulnerabilitiesRequest(
    $closedVulns: [String]!
    $findingId: String!
    $justification: String!
    $openVulns: [String]!
    $verificationReason: VulnerabilityStateReason
  ) {
    verifyVulnerabilitiesRequest(
      closedVulnerabilities: $closedVulns
      findingId: $findingId
      justification: $justification
      openVulnerabilities: $openVulns
      verificationReason: $verificationReason
    ) {
      success
    }
  }
`);

export { REQUEST_VULNERABILITIES_VERIFICATION, VERIFY_VULNERABILITIES };
