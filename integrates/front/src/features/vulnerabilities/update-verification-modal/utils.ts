import type { GraphQLError } from "graphql";
import _ from "lodash";
import mixpanel from "mixpanel-browser";

import type {
  IRequestVulnVerificationResult,
  IVerifyRequestVulnResult,
  IVulnData,
  TReattackVulnerabilitiesResult,
  TVerificationResult,
  TVerifyVulnerabilitiesResult,
} from "./types";

import type { VulnerabilityStateReason } from "gql/graphql";
import { Logger } from "utils/logger";
import { msgError, msgInfo, msgSuccess, msgWarning } from "utils/notifications";
import { translate } from "utils/translations/translate";

const DEFAULT_CHUNK_SIZE = 80;

const handleRequestVerification = (
  clearSelected: () => void,
  setRequestState: () => void,
  data: boolean,
): void => {
  if (data) {
    msgSuccess(
      translate.t("groupAlerts.requestedReattackSuccess"),
      translate.t("groupAlerts.updatedTitle"),
    );
    setRequestState();
    clearSelected();
  }
};

const isNotAllVulnsUpdatedError = (error: GraphQLError): boolean => {
  const notAllVulnsUpdatedRegex =
    /Exception - Reattack request was rejected in \d+ vulnerabilities/gu;

  if (notAllVulnsUpdatedRegex.test(error.message)) {
    msgWarning(error.message.split("- ")[1]);

    msgInfo(
      translate.t("groupAlerts.requestedReattackFailed"),
      translate.t("groupAlerts.retryReattackRequest"),
    );

    return true;
  }

  return false;
};

const handleRequestVerificationError = (error: GraphQLError): void => {
  if (isNotAllVulnsUpdatedError(error)) return;

  switch (error.message.trim()) {
    case "Exception - Reattack request was rejected":
      msgError(translate.t("groupAlerts.verificationRejected"));
      break;
    case "Exception - The vulnerability has already been closed":
      msgError(translate.t("groupAlerts.vulnClosed"));
      break;
    case "Exception - Vulnerability not found":
      msgError(translate.t("groupAlerts.noFound"));
      break;
    case "Exception - Access denied or credential not found":
      msgError(translate.t("group.scope.git.sync.noCredentials"));
      break;
    case "Exception - The git repository is outdated":
      msgError(translate.t("groupAlerts.outdatedRepository"), "Oops!");
      break;
    case "Exception - The location have unresolved events":
      msgError(translate.t("groupAlerts.unsolvedEvents"), "Oops!");
      break;
    case "Exception - Error value is not valid":
      msgError(translate.t("group.scope.git.errors.invalidGitCredentials"));
      break;
    default:
      msgError(translate.t("groupAlerts.errorTextsad"));
      Logger.warning("An error occurred requesting verification", error);
  }
};

const handleVerifyRequest = (
  clearSelected: () => void,
  setVerifyState: () => void,
  data: boolean,
  numberOfVulnerabilities: number,
): void => {
  if (data) {
    msgSuccess(
      translate.t(
        `groupAlerts.verifiedSuccess${
          numberOfVulnerabilities > 1 ? "Plural" : ""
        }`,
      ),
      translate.t("groupAlerts.updatedTitle"),
    );
    setVerifyState();
    clearSelected();
  }
};

const handleVerifyRequestError = (error: GraphQLError): void => {
  if (isNotAllVulnsUpdatedError(error)) return;

  switch (error.message) {
    case "Exception - Error verification not requested":
      msgError(translate.t("groupAlerts.noVerificationRequested"));
      break;
    case "Exception - Vulnerability not found":
      msgError(translate.t("groupAlerts.noFound"));
      break;
    case "Exception - The git repository is outdated":
      msgError(translate.t("groupAlerts.outdatedRepository"));
      break;
    default:
      msgError(translate.t("groupAlerts.errorTextsad"));
      Logger.warning("An error occurred verifying a request", error);
  }
};

const getAreAllMutationValid = (
  results: TReattackVulnerabilitiesResult[] | TVerifyVulnerabilitiesResult[],
): boolean[] => {
  return results.map(
    (
      result: TReattackVulnerabilitiesResult | TVerifyVulnerabilitiesResult,
    ): boolean => {
      if (!_.isUndefined(result.data) && !_.isNull(result.data)) {
        const reattackSuccess: boolean = _.isUndefined(
          (result.data as IRequestVulnVerificationResult)
            .requestVulnerabilitiesVerification,
        )
          ? false
          : (result.data as IRequestVulnVerificationResult)
              .requestVulnerabilitiesVerification.success;

        const verifySuccess: boolean = _.isUndefined(
          (result.data as IVerifyRequestVulnResult)
            .verifyVulnerabilitiesRequest,
        )
          ? false
          : (result.data as IVerifyRequestVulnResult)
              .verifyVulnerabilitiesRequest.success;

        return reattackSuccess || verifySuccess;
      }

      return false;
    },
  );
};

const getAreAllChunkedMutationValid = (
  results: TVerificationResult[],
): boolean[] =>
  results
    .map(getAreAllMutationValid)
    .reduce(
      (previous: boolean[], current: boolean[]): boolean[] => [
        ...previous,
        ...current,
      ],
      [],
    );

const onReattack = async (
  requestVerification: (
    variables: Record<string, unknown>,
  ) => Promise<TReattackVulnerabilitiesResult>,
  findingId: string,
  values: {
    treatmentJustification: string;
    verificationReason?: VulnerabilityStateReason;
  },
  vulns: IVulnData[],
  chunkSize = DEFAULT_CHUNK_SIZE,
): Promise<TReattackVulnerabilitiesResult[]> => {
  const vulnerabilitiesId: string[] = vulns.map(
    (vuln: IVulnData): string => vuln.id,
  );

  mixpanel.track("RequestReattack");
  const vulnerabilitiesIdsChunks: string[][] = _.chunk(
    vulnerabilitiesId,
    chunkSize,
  );
  const requestedChunks = vulnerabilitiesIdsChunks.map(
    (
      chunkedVulnerabilitiesIds,
    ): (() => Promise<TReattackVulnerabilitiesResult[]>) =>
      async (): Promise<TReattackVulnerabilitiesResult[]> => {
        return Promise.all([
          requestVerification({
            variables: {
              findingId,
              justification: values.treatmentJustification,
              vulnerabilities: chunkedVulnerabilitiesIds,
            },
          }),
        ]);
      },
  );

  return requestedChunks.reduce(
    async (
      previousValue,
      currentValue,
    ): Promise<TReattackVulnerabilitiesResult[]> => [
      ...(await previousValue),
      ...(await currentValue()),
    ],
    Promise.resolve<TReattackVulnerabilitiesResult[]>([]),
  );
};

const handleSubmitHelper = async (
  requestVerification: (
    variables: Record<string, unknown>,
  ) => Promise<TReattackVulnerabilitiesResult>,
  verifyRequest: (
    variables: Record<string, unknown>,
  ) => Promise<TVerifyVulnerabilitiesResult>,
  findingId: string,
  values: {
    treatmentJustification: string;
    verificationReason?: VulnerabilityStateReason;
  },
  vulns: IVulnData[],
  vulnerabilitiesList: IVulnData[],
  isReattacking: boolean,
): Promise<
  TReattackVulnerabilitiesResult[] | TVerifyVulnerabilitiesResult[]
> => {
  const chunkSize = 80;
  if (isReattacking) {
    return onReattack(requestVerification, findingId, values, vulns, chunkSize);
  }
  const vulnerabilitiesListChunks = _.chunk(vulnerabilitiesList, chunkSize);
  const verifiedChunks = vulnerabilitiesListChunks.map(
    (
      chunkedVulnerabilitiesList,
    ): (() => Promise<TVerifyVulnerabilitiesResult[]>) =>
      async (): Promise<TVerifyVulnerabilitiesResult[]> => {
        const openVulnsId: string[] = chunkedVulnerabilitiesList.reduce(
          (acc: string[], vuln: IVulnData): string[] =>
            vuln.state === "VULNERABLE" ? [...acc, vuln.id] : acc,
          [],
        );
        const closedVulnsId: string[] = chunkedVulnerabilitiesList.reduce(
          (acc: string[], vuln: IVulnData): string[] =>
            vuln.state === "SAFE" ? [...acc, vuln.id] : acc,
          [],
        );

        return Promise.all([
          verifyRequest({
            variables: {
              closedVulns: closedVulnsId,
              findingId,
              justification: values.treatmentJustification,
              openVulns: openVulnsId,
              verificationReason: values.verificationReason,
            },
          }),
        ]);
      },
  );

  return verifiedChunks.reduce(
    async (
      previousValue,
      currentValue,
    ): Promise<TVerifyVulnerabilitiesResult[]> => [
      ...(await previousValue),
      ...(await currentValue()),
    ],
    Promise.resolve<TVerifyVulnerabilitiesResult[]>([]),
  );
};

const handleAltSubmitHelper = async (
  requestVerification: (
    variables: Record<string, unknown>,
  ) => Promise<TReattackVulnerabilitiesResult>,
  verifyRequest: (
    variables: Record<string, unknown>,
  ) => Promise<TVerifyVulnerabilitiesResult>,
  values: {
    treatmentJustification: string;
    verificationReason?: VulnerabilityStateReason;
  },
  vulnerabilitiesList: IVulnData[],
  isReattacking: boolean,
): Promise<TVerificationResult[]> => {
  const vulnerabilitiesByFinding = _.groupBy(
    vulnerabilitiesList,
    (vuln: IVulnData): string => vuln.findingId,
  );
  const requestedChunks = Object.entries(vulnerabilitiesByFinding).map(
    ([findingId, chunkedVulnerabilities]: [
      string,
      IVulnData[],
    ]): (() => Promise<TVerificationResult[]>) =>
      async (): Promise<TVerificationResult[]> => {
        return Promise.all([
          handleSubmitHelper(
            requestVerification,
            verifyRequest,
            findingId,
            values,
            chunkedVulnerabilities,
            chunkedVulnerabilities,
            isReattacking,
          ),
        ]);
      },
  );

  return requestedChunks.reduce(
    async (previousValue, currentValue): Promise<TVerificationResult[]> => [
      ...(await previousValue),
      ...(await currentValue()),
    ],
    Promise.resolve<TVerificationResult[]>([]),
  );
};

export {
  getAreAllChunkedMutationValid,
  getAreAllMutationValid,
  handleRequestVerification,
  handleRequestVerificationError,
  handleVerifyRequest,
  handleVerifyRequestError,
  handleAltSubmitHelper,
  handleSubmitHelper,
  onReattack,
};
