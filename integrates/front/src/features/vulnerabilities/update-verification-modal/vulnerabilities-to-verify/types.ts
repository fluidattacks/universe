import type { IVulnData } from "../types";
import type { IUseTable } from "hooks/use-table";

interface IVulnerabilitiesToVerifyProps {
  handleOnChange: () => void;
  isOpen: boolean;
  setVulnerabilitiesList: (value: React.SetStateAction<IVulnData[]>) => void;
  tableRef: IUseTable;
  vulnerabilitiesList: IVulnData[];
}

export type { IVulnerabilitiesToVerifyProps };
