import { PureAbility } from "@casl/ability";
import { act, screen, waitFor, within } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import _ from "lodash";
import { HttpResponse, type StrictResponse, graphql } from "msw";

import {
  REQUEST_VULNERABILITIES_VERIFICATION,
  VERIFY_VULNERABILITIES,
} from "./queries";

import { UpdateVerificationModal } from ".";
import {
  authzGroupContext,
  authzPermissionsContext,
} from "context/authz/config";
import {
  type GetFindingVulnsQueriesQuery as GetFindingVulnsQueries,
  type RequestVulnerabilitiesVerificationMutation as RequestVulnerabilitiesVerification,
  RootCriticality,
  type VerifyVulnerabilitiesRequestMutation as VerifyVulnerabilities,
} from "gql/graphql";
import { useVulnerabilityStore } from "hooks/use-vulnerability-store";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage, IMessage } from "mocks/types";
import { GET_FINDING_VULNS_QUERIES } from "pages/finding/vulnerabilities/queries";
import { msgError, msgInfo, msgWarning } from "utils/notifications";

jest.mock("utils/notifications", (): Record<string, VoidFunction> => {
  const mocked = jest.requireActual<Record<string, VoidFunction>>(
    "utils/notifications",
  );
  jest.spyOn(mocked, "msgError").mockImplementation();
  jest.spyOn(mocked, "msgWarning").mockImplementation();
  jest.spyOn(mocked, "msgInfo").mockImplementation();

  return mocked;
});

describe("update verification component", (): void => {
  const initialStoreState = useVulnerabilityStore.getState();

  // eslint-disable-next-line jest/no-hooks
  beforeEach((): void => {
    useVulnerabilityStore.setState(initialStoreState, true);
  });

  const graphqlMocked = graphql.link(LINK);
  const memoryRouter = {
    initialEntries: ["/orgs/okada"],
  };
  const mocksVulns = [
    graphqlMocked.query(
      GET_FINDING_VULNS_QUERIES,
      (): StrictResponse<{ data: GetFindingVulnsQueries }> => {
        return HttpResponse.json({
          data: {
            __typename: "Query",
            ...{
              finding: {
                __typename: "Finding",
                draftsConnection: undefined,
                id: "438679960",
                vulnerabilitiesConnection: {
                  edges: [],
                  pageInfo: {
                    endCursor: "test-cursor=",
                    hasNextPage: false,
                  },
                },
                zeroRiskConnection: {
                  edges: [],
                  pageInfo: {
                    endCursor: "test-cursor=",
                    hasNextPage: false,
                  },
                },
              },
            },
          },
        });
      },
    ),
  ];

  it("should handle request verification", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const handleOnClose: jest.Mock = jest.fn();
    const handleRequestState: jest.Mock = jest.fn();
    const handleRefetchData: jest.Mock = jest.fn();
    const handleRefetchFindingAndGroup: jest.Mock = jest.fn();
    const handleRefetchFindingHeader: jest.Mock = jest.fn();
    const mocksMutation = [
      graphqlMocked.mutation(
        REQUEST_VULNERABILITIES_VERIFICATION,
        ({
          variables,
        }): StrictResponse<
          IErrorMessage | { data: RequestVulnerabilitiesVerification }
        > => {
          const { vulnerabilities, findingId, justification } = variables;
          if (
            findingId === "" &&
            justification ===
              "This is a commenting test of a request verification in vulns" &&
            _.isEqual(vulnerabilities, ["test"])
          ) {
            return HttpResponse.json({
              data: {
                requestVulnerabilitiesVerification: { success: true },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Exception requesting verification")],
          });
        },
      ),
      ...mocksVulns,
    ];

    act((): void => {
      useVulnerabilityStore.setState({ isReattacking: true });
    });

    render(
      <authzPermissionsContext.Provider value={new PureAbility([])}>
        <authzGroupContext.Provider value={new PureAbility([])}>
          <UpdateVerificationModal
            clearSelected={jest.fn()}
            handleCloseModal={handleOnClose}
            refetchData={handleRefetchData}
            refetchFindingAndGroup={handleRefetchFindingAndGroup}
            refetchFindingHeader={handleRefetchFindingHeader}
            setRequestState={handleRequestState}
            setVerifyState={jest.fn()}
            vulns={[
              {
                findingId: "",
                groupName: "",
                id: "test",
                root: {
                  __typename: "GitRoot",
                  branch: "master",
                  criticality: RootCriticality.Low,
                },
                rootNickname: "universe",
                source: "",
                specific: "",
                state: "VULNERABLE",
                vulnerabilityType: "lines",
                where: "",
              },
            ]}
          />
        </authzGroupContext.Provider>
      </authzPermissionsContext.Provider>,
      { memoryRouter, mocks: mocksMutation },
    );

    await waitFor((): void => {
      expect(
        screen.queryByRole("textbox", { name: "treatmentJustification" }),
      ).toBeInTheDocument();
    });

    await userEvent.type(
      screen.getByRole("textbox", { name: "treatmentJustification" }),
      "This is a commenting test of a request verification in vulns",
    );

    expect(screen.getByText("Confirm")).not.toBeDisabled();

    await userEvent.click(screen.getByText("Confirm"));
    await waitFor((): void => {
      expect(handleOnClose).toHaveBeenCalledTimes(1);
    });

    expect(handleRequestState).toHaveBeenCalledTimes(1);
    expect(handleRefetchData).toHaveBeenCalledTimes(1);
  });

  it("should handle request machine vuln verification", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const handleOnClose: jest.Mock = jest.fn();
    const handleRequestState: jest.Mock = jest.fn();
    const handleRefetchData: jest.Mock = jest.fn();
    const handleRefetchFindingAndGroup: jest.Mock = jest.fn();
    const handleRefetchFindingHeader: jest.Mock = jest.fn();
    const mocksMutation = [
      graphqlMocked.mutation(
        REQUEST_VULNERABILITIES_VERIFICATION,
        ({
          variables,
        }): StrictResponse<
          IErrorMessage | { data: RequestVulnerabilitiesVerification }
        > => {
          const { vulnerabilities, findingId, justification } = variables;
          if (
            findingId === "" &&
            justification ===
              "searchFindings.tabDescription.remediationModal.machineJustification" &&
            _.isEqual(vulnerabilities, ["test"])
          ) {
            return HttpResponse.json({
              data: {
                requestVulnerabilitiesVerification: { success: true },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Exception requesting verification")],
          });
        },
      ),
      ...mocksVulns,
    ];

    act((): void => {
      useVulnerabilityStore.setState({ isReattacking: true });
    });

    render(
      <authzPermissionsContext.Provider value={new PureAbility([])}>
        <authzGroupContext.Provider value={new PureAbility([])}>
          <UpdateVerificationModal
            clearSelected={jest.fn()}
            handleCloseModal={handleOnClose}
            refetchData={handleRefetchData}
            refetchFindingAndGroup={handleRefetchFindingAndGroup}
            refetchFindingHeader={handleRefetchFindingHeader}
            setRequestState={handleRequestState}
            setVerifyState={jest.fn()}
            vulns={[
              {
                findingId: "",
                groupName: "",
                hacker: "machine@fluidattacks.com",
                id: "test",
                root: {
                  __typename: "GitRoot",
                  branch: "master",
                  criticality: RootCriticality.Low,
                },
                rootNickname: "universe",
                source: "machine",
                specific: "",
                state: "VULNERABLE",
                vulnerabilityType: "lines",
                where: "",
              },
            ]}
          />
        </authzGroupContext.Provider>
      </authzPermissionsContext.Provider>,
      { memoryRouter, mocks: mocksMutation },
    );

    await waitFor((): void => {
      expect(
        screen.queryByRole("textbox", { name: "treatmentJustification" }),
      ).not.toBeInTheDocument();
    });

    await userEvent.click(screen.getByText("Confirm"));
    await waitFor((): void => {
      expect(handleOnClose).toHaveBeenCalledTimes(1);
    });

    expect(handleRequestState).toHaveBeenCalledTimes(1);
    expect(handleRefetchData).toHaveBeenCalledTimes(1);
  });

  it("should handle request verification error", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const handleOnClose: jest.Mock = jest.fn();
    const handleRequestState: jest.Mock = jest.fn();
    const handleRefetchData: jest.Mock = jest.fn();
    const handleRefetchFindingAndGroup: jest.Mock = jest.fn();
    const handleRefetchFindingHeader: jest.Mock = jest.fn();
    const mocksMutation = [
      graphqlMocked.mutation(
        REQUEST_VULNERABILITIES_VERIFICATION,
        ({
          variables,
        }): StrictResponse<
          { data: RequestVulnerabilitiesVerification } | { errors: IMessage[] }
        > => {
          const { vulnerabilities, findingId, justification } = variables;
          if (
            findingId === "123" &&
            _.isEqual(vulnerabilities, ["test_error"])
          ) {
            if (
              justification ===
              "Verification no requested for all vulnerabilities"
            ) {
              return HttpResponse.json({
                errors: [
                  new GraphQLError(
                    "Exception - Reattack request was rejected in 2 vulnerabilities",
                  ),
                ],
              });
            } else if (justification === "Verification rejected") {
              return HttpResponse.json({
                errors: [
                  new GraphQLError("Exception - Reattack request was rejected"),
                ],
              });
            }

            return HttpResponse.json({
              errors: [
                new GraphQLError(
                  "Exception - The vulnerability has already been closed",
                ),
                new GraphQLError("Exception - Vulnerability not found"),
              ],
            });
          }

          return HttpResponse.json({
            data: {
              requestVulnerabilitiesVerification: { success: true },
            },
          });
        },
      ),
      ...mocksVulns,
    ];

    act((): void => {
      useVulnerabilityStore.setState({ isReattacking: true });
    });

    render(
      <authzPermissionsContext.Provider value={new PureAbility([])}>
        <authzGroupContext.Provider value={new PureAbility([])}>
          <UpdateVerificationModal
            clearSelected={jest.fn()}
            handleCloseModal={handleOnClose}
            refetchData={handleRefetchData}
            refetchFindingAndGroup={handleRefetchFindingAndGroup}
            refetchFindingHeader={handleRefetchFindingHeader}
            setRequestState={handleRequestState}
            setVerifyState={jest.fn()}
            vulns={[
              {
                findingId: "123",
                groupName: "",
                id: "test_error",
                root: {
                  __typename: "GitRoot",
                  branch: "master",
                  criticality: RootCriticality.Low,
                },
                rootNickname: "universe",
                source: "",
                specific: "",
                state: "VULNERABLE",
                vulnerabilityType: "lines",
                where: "",
              },
            ]}
          />
        </authzGroupContext.Provider>
      </authzPermissionsContext.Provider>,
      { memoryRouter, mocks: mocksMutation },
    );

    const justificationInput = screen.getByRole("textbox", {
      name: "treatmentJustification",
    });
    await userEvent.type(
      justificationInput,
      "Verification no requested for all vulnerabilities",
    );

    await userEvent.click(screen.getByText("Confirm"));

    await waitFor((): void => {
      expect(handleOnClose).toHaveBeenCalledTimes(1);
    });

    expect(msgWarning).toHaveBeenCalledWith(
      `Reattack request was rejected in 2 vulnerabilities`,
    );
    expect(msgInfo).toHaveBeenCalledTimes(1);

    await userEvent.clear(justificationInput);
    await userEvent.type(justificationInput, "Verification rejected");
    await userEvent.click(screen.getByText("Confirm"));

    await waitFor((): void => {
      expect(handleOnClose).toHaveBeenCalledTimes(2);
    });

    expect(msgError).toHaveBeenCalledWith(
      "The verification has been rejected, possible reasons:\n1) Verification already requested",
    );

    expect(handleRequestState).not.toHaveBeenCalled();
    expect(handleRefetchData).not.toHaveBeenCalled();
    expect(msgError).not.toHaveBeenCalledWith("There is an error :(");
  });

  it("should handle verify a request", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const handleOnClose: jest.Mock = jest.fn();
    const handleVerifyState: jest.Mock = jest.fn();
    const handleRefetchData: jest.Mock = jest.fn();
    const handleRefetchFindingAndGroup: jest.Mock = jest.fn();
    const handleRefetchFindingHeader: jest.Mock = jest.fn();
    const mocks = [
      graphqlMocked.mutation(
        VERIFY_VULNERABILITIES,
        ({
          variables,
        }): StrictResponse<IErrorMessage | { data: VerifyVulnerabilities }> => {
          const { closedVulns, findingId, justification, openVulns } =
            variables;
          if (
            findingId === "" &&
            justification ===
              "This is a commenting test of a verifying request verification in vulns" &&
            _.isEqual(openVulns, []) &&
            _.isEqual(closedVulns, ["test"])
          ) {
            return HttpResponse.json({
              data: {
                verifyVulnerabilitiesRequest: { success: true },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error verifying vulnerability")],
          });
        },
      ),
      ...mocksVulns,
    ];

    act((): void => {
      useVulnerabilityStore.setState({ isVerifying: true });
    });

    render(
      <authzPermissionsContext.Provider value={new PureAbility([])}>
        <authzGroupContext.Provider value={new PureAbility([])}>
          <UpdateVerificationModal
            clearSelected={jest.fn()}
            handleCloseModal={handleOnClose}
            refetchData={handleRefetchData}
            refetchFindingAndGroup={handleRefetchFindingAndGroup}
            refetchFindingHeader={handleRefetchFindingHeader}
            setRequestState={jest.fn()}
            setVerifyState={handleVerifyState}
            vulns={[
              {
                findingId: "",
                groupName: "",
                id: "test",
                root: {
                  __typename: "GitRoot",
                  branch: "master",
                  criticality: RootCriticality.Low,
                },
                rootNickname: "universe",
                source: "",
                specific: "",
                state: "VULNERABLE",
                vulnerabilityType: "lines",
                where: "",
              },
            ]}
          />
        </authzGroupContext.Provider>
      </authzPermissionsContext.Provider>,
      { memoryRouter, mocks },
    );

    await userEvent.type(
      screen.getByRole("textbox", { name: "treatmentJustification" }),
      "This is a commenting test of a verifying request verification in vulns",
    );
    await userEvent.click(
      within(screen.getByRole("table")).getByRole("checkbox"),
    );

    await userEvent.click(screen.getByText("Confirm"));

    await waitFor((): void => {
      expect(handleOnClose).toHaveBeenCalledTimes(1);
    });

    expect(handleVerifyState).toHaveBeenCalledTimes(1);
    expect(handleRefetchData).toHaveBeenCalledTimes(1);
  });

  it("should handle verify a request error", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const handleOnClose: jest.Mock = jest.fn();
    const handleVerifyState: jest.Mock = jest.fn();
    const handleRefetchData: jest.Mock = jest.fn();
    const handleRefetchFindingAndGroup: jest.Mock = jest.fn();
    const handleRefetchFindingHeader: jest.Mock = jest.fn();
    const mocks = [
      graphqlMocked.mutation(
        VERIFY_VULNERABILITIES,
        ({
          variables,
        }): StrictResponse<
          | { data: VerifyVulnerabilities }
          | { errors: [IMessage, IMessage, IMessage] }
        > => {
          const { closedVulns, findingId, justification, openVulns } =
            variables;
          if (
            findingId === "" &&
            justification ===
              "This is a commenting test of a verifying request verification in vulns" &&
            _.isEqual(openVulns, ["test_error"]) &&
            _.isEqual(closedVulns, [])
          ) {
            return HttpResponse.json({
              errors: [
                new GraphQLError(
                  "Exception - Error verification not requested",
                ),
                new GraphQLError("Exception - Vulnerability not found"),
                new GraphQLError("Unexpected error"),
              ],
            });
          }

          return HttpResponse.json({
            data: {
              verifyVulnerabilitiesRequest: { success: true },
            },
          });
        },
      ),
      ...mocksVulns,
    ];

    act((): void => {
      useVulnerabilityStore.setState({ isVerifying: true });
    });

    render(
      <authzPermissionsContext.Provider value={new PureAbility([])}>
        <authzGroupContext.Provider value={new PureAbility([])}>
          <UpdateVerificationModal
            clearSelected={jest.fn()}
            handleCloseModal={handleOnClose}
            refetchData={handleRefetchData}
            refetchFindingAndGroup={handleRefetchFindingAndGroup}
            refetchFindingHeader={handleRefetchFindingHeader}
            setRequestState={jest.fn()}
            setVerifyState={handleVerifyState}
            vulns={[
              {
                findingId: "",
                groupName: "",
                id: "test_error",
                root: {
                  __typename: "GitRoot",
                  branch: "master",
                  criticality: RootCriticality.Low,
                },
                rootNickname: "universe",
                source: "",
                specific: "",
                state: "VULNERABLE",
                vulnerabilityType: "lines",
                where: "",
              },
            ]}
          />
        </authzGroupContext.Provider>
      </authzPermissionsContext.Provider>,
      { memoryRouter, mocks },
    );

    await userEvent.type(
      screen.getByRole("textbox", { name: "treatmentJustification" }),
      "This is a commenting test of a verifying request verification in vulns",
    );
    await userEvent.click(screen.getByText("Confirm"));
    await waitFor((): void => {
      expect(handleOnClose).toHaveBeenCalledTimes(1);
    });

    expect(handleVerifyState).not.toHaveBeenCalled();
    expect(handleRefetchData).not.toHaveBeenCalled();
  });
});
