import { useSubscription } from "@apollo/client";
import mixpanel from "mixpanel-browser";
import { useState } from "react";
import { useTranslation } from "react-i18next";

import { GET_CUSTOM_FIX_SUBSCRIPTION } from "../vulnerability-modal/queries";
import { useAudit } from "hooks/use-audit";
import { Logger } from "utils/logger";

interface IUseCustomFix {
  customFixMarkdown: string;
  loading: boolean;
}

const useCustomFix = (vulnId: string): IUseCustomFix => {
  const { addAuditEvent } = useAudit();
  const { t } = useTranslation();

  const [customFixMarkdown, setCustomFixMarkdown] = useState("");

  const { loading } = useSubscription(GET_CUSTOM_FIX_SUBSCRIPTION, {
    onComplete: (): void => {
      mixpanel.track("GetCustomFix");
      addAuditEvent("Vulnerability.CustomFix", vulnId);
    },
    onData: (options): void => {
      const { data } = options.data;
      if (data !== undefined) {
        setCustomFixMarkdown((previous): string =>
          previous.concat(data.getCustomFix),
        );
      }
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.warning("An error occurred generating the remediation", error);
        setCustomFixMarkdown(
          t("searchFindings.tabVuln.contentTab.customFix.noData", {
            link: "https://help.fluidattacks.com/portal/en/kb/criteria/fixes",
          }),
        );
      });
    },
    variables: { vulnId },
  });

  return { customFixMarkdown, loading };
};

export { useCustomFix };
