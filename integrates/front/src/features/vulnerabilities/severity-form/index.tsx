import { Container, GridContainer, Text } from "@fluidattacks/design";
import map from "lodash/map";
import { useTranslation } from "react-i18next";

import type { IDropDownOption } from "components/dropdown/types";
import { Editable, Select } from "components/input";
import type { GetFindingSeverityQueryQuery } from "gql/graphql";
import { castFieldsCVSS4 } from "pages/finding/severity//cvss4/utils";
import { castFieldsCVSS3 } from "pages/finding/severity/utils";
import { getCVSS31Values } from "utils/cvss";
import { getCVSS40Values } from "utils/cvss4";

interface ISeverityForm {
  data: GetFindingSeverityQueryQuery;
}

const SeverityForm = ({ data }: Readonly<ISeverityForm>): JSX.Element => {
  const { t } = useTranslation();
  const CVSS_VERSION = "3.1";

  const cvss31Values = getCVSS31Values(data.finding.severityVector);
  const cvss40Values = getCVSS40Values(data.finding.severityVectorV4);

  return (
    <Container>
      <GridContainer lg={2} md={2} sm={2} xl={2}>
        <Container>
          <Text fontWeight={"bold"} mb={1} size={"sm"}>
            {`${t("searchFindings.tabSeverity.cvssVersion")}: 4.0`}
          </Text>
          {castFieldsCVSS4(cvss40Values).map((field): JSX.Element => {
            const currentOption = field.options[field.currentValue];
            const options = map(
              field.options,
              (text, value): IDropDownOption => {
                const item = {
                  header: t(text),
                  value,
                };

                return item;
              },
            );

            return (
              <Container key={field.name}>
                <Editable
                  currentValue={`${field.currentValue} | ${t(currentOption)}`}
                  isEditing={true}
                  label={field.title}
                >
                  <Select
                    id={`scoreV4${field.name}`}
                    items={options}
                    label={field.title}
                    name={`scoreV4${field.name}`}
                  />
                </Editable>
              </Container>
            );
          })}
        </Container>
        <Container>
          <Text fontWeight={"bold"} mb={1} size={"sm"}>
            {`${t("searchFindings.tabSeverity.cvssVersion")}: ${CVSS_VERSION}`}
          </Text>
          {castFieldsCVSS3(cvss31Values).map((field): JSX.Element => {
            const currentOption = field.options[field.currentValue];
            const options = map(
              field.options,
              (text, value): IDropDownOption => {
                const item = {
                  header: t(text),
                  value,
                };

                return item;
              },
            );

            return (
              <Container key={field.name}>
                <Editable
                  currentValue={`${field.currentValue} | ${t(currentOption)}`}
                  isEditing={true}
                  label={field.name}
                >
                  <Select
                    id={field.name}
                    items={options}
                    label={field.title}
                    name={field.name}
                  />
                </Editable>
              </Container>
            );
          })}
        </Container>
      </GridContainer>
    </Container>
  );
};

export { SeverityForm };
