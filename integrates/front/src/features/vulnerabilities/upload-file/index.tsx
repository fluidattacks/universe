import { useMutation } from "@apollo/client";
import {
  Button,
  Form,
  Gap,
  InnerForm,
  InputFile,
  Modal,
  Text,
  useModal,
} from "@fluidattacks/design";
import isUndefined from "lodash/isUndefined";
import mixpanel from "mixpanel-browser";
import { useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";

import { handleUploadError } from "./hooks";
import type { IUploadVulnFile, IUploadVulnProps } from "./types";
import { validationSchema } from "./validations";

import { DOWNLOAD_VULNERABILITIES, UPLOAD_VULNERABILITIES } from "../queries";
import { GET_FINDING_HEADER } from "pages/finding/queries";
import { GET_FINDING_INFO } from "pages/finding/vulnerabilities/queries";
import { Logger } from "utils/logger";
import { msgError, msgSuccess, msgWarning } from "utils/notifications";
import { openUrl } from "utils/resource-helpers";

const UploadVulnerabilities: React.FC<IUploadVulnProps> = ({
  findingId,
  refetchData,
}): JSX.Element => {
  const { t } = useTranslation();
  const updateModalProps = useModal("update-vulnerabilities-modal");
  const { close, isOpen, open } = updateModalProps;
  const { groupName } = useParams() as { groupName: string };

  const [uploadVulnerability, { loading }] = useMutation(
    UPLOAD_VULNERABILITIES,
    {
      onCompleted: (result): void => {
        if (!isUndefined(result)) {
          if (result.uploadFile.success) {
            updateModalProps.close();
            if (
              result.uploadFile.message === null ||
              result.uploadFile.message === ""
            ) {
              msgSuccess(
                t("groupAlerts.fileUpdated"),
                t("groupAlerts.titleSuccess"),
              );
            } else {
              msgWarning(
                result.uploadFile.message,
                t("groupAlerts.fileUpdatedWarning"),
              );
            }
            refetchData();
          } else {
            msgError(
              t("searchFindings.tabVuln.alerts.uploadFile.noChangesWereMade"),
            );
          }
        }
      },
      onError: handleUploadError,
      refetchQueries: [
        {
          query: GET_FINDING_INFO,
          variables: {
            findingId,
          },
        },
        {
          query: GET_FINDING_HEADER,
          variables: {
            findingId,
          },
        },
      ],
    },
  );
  const [downloadVulnerability] = useMutation(DOWNLOAD_VULNERABILITIES, {
    onCompleted: (result): void => {
      if (!isUndefined(result)) {
        if (
          result.downloadVulnerabilityFile.success &&
          result.downloadVulnerabilityFile.url !== ""
        ) {
          mixpanel.track("DownloadVulnFile", {
            fileName: result.downloadVulnerabilityFile.url,
            group: groupName,
          });
          openUrl(result.downloadVulnerabilityFile.url);
        }
      }
    },
    onError: (downloadError): void => {
      downloadError.graphQLErrors.forEach(({ message }): void => {
        msgError(t("groupAlerts.errorTextsad"));
        if (message === "Exception - Error Uploading File to S3") {
          Logger.warning(
            "An error occurred downloading vuln file while uploading file to S3",
            downloadError,
          );
        } else {
          Logger.warning(
            "An error occurred downloading vuln file",
            downloadError,
          );
        }
      });
    },
  });

  const handleUploadVulnerability = useCallback(
    async (values: IUploadVulnFile): Promise<void> => {
      await uploadVulnerability({
        variables: {
          file: values.filename[0],
          findingId,
        },
      });
    },
    [findingId, uploadVulnerability],
  );
  const handleDownloadVulnerability = useCallback((): void => {
    void downloadVulnerability({
      variables: {
        findingId,
      },
    });
  }, [downloadVulnerability, findingId]);

  const modal = (
    <Modal
      id={"uploadVulns"}
      modalRef={updateModalProps}
      size={"sm"}
      title={t("searchFindings.tabResources.modalFileTitle")}
    >
      <Form
        cancelButton={{ onClick: close }}
        defaultValues={{ filename: undefined as unknown as FileList }}
        onSubmit={handleUploadVulnerability}
        yupSchema={validationSchema}
      >
        <InnerForm>
          {({ register, formState, watch, setValue }): JSX.Element => (
            <React.Fragment>
              <Text mb={1} size={"sm"}>
                {t(
                  "searchFindings.tabDescription.updateVulnerabilitiesTooltip",
                )}
              </Text>
              <InputFile
                accept={".yaml,.yml"}
                error={formState.errors.filename?.message?.toString()}
                id={"filename"}
                name={"filename"}
                register={register}
                setValue={setValue}
                watch={watch}
              />
            </React.Fragment>
          )}
        </InnerForm>
      </Form>
    </Modal>
  );

  return (
    <React.Fragment>
      <Gap>
        <Button
          disabled={loading}
          icon={"download"}
          ml={0}
          mr={0.5}
          my={0}
          onClick={handleDownloadVulnerability}
          tooltip={t(
            "searchFindings.tabDescription.downloadVulnerabilitiesTooltip",
          )}
        >
          {t("searchFindings.tabDescription.downloadVulnerabilities")}
        </Button>
        <Button
          icon={"upload"}
          onClick={open}
          tooltip={t(
            "searchFindings.tabDescription.updateVulnerabilitiesTooltip",
          )}
          variant={"secondary"}
        >
          {t("searchFindings.tabDescription.updateVulnerabilities")}
        </Button>
      </Gap>
      {isOpen ? modal : null}
    </React.Fragment>
  );
};

export { UploadVulnerabilities };
