interface IUploadVulnProps {
  findingId: string;
  refetchData: () => void;
}

interface IErrorInfoAttr {
  keys: string[];
  msg: string;
  values: string[] & string;
}

interface IUploadVulnFile {
  filename: FileList;
}

export type { IErrorInfoAttr, IUploadVulnFile, IUploadVulnProps };
