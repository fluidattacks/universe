import type { MockedResponse } from "@apollo/client/testing";
import { MockedProvider } from "@apollo/client/testing";
import type { RenderResult } from "@testing-library/react";
import { fireEvent, screen, waitFor } from "@testing-library/react";
import { Route, Routes } from "react-router-dom";
import type { AnyObject, InferType, Schema } from "yup";
import { ValidationError } from "yup";

import { validationSchema } from "./validations";

import { DOWNLOAD_VULNERABILITIES, UPLOAD_VULNERABILITIES } from "../queries";
import { UploadVulnerabilities } from "features/vulnerabilities/upload-file";
import { render } from "mocks";
import { GET_FINDING_HEADER } from "pages/finding/queries";
import { GET_FINDING_INFO } from "pages/finding/vulnerabilities/queries";
import { msgSuccess } from "utils/notifications";

jest.mock(
  "mixpanel-browser",
  (): Record<string, unknown> => ({
    track: jest.fn(),
  }),
);

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

const mockRefetchData = jest.fn();

describe("uploadFile", (): void => {
  const defaultProps = {
    findingId: "test-finding-id",
    refetchData: mockRefetchData,
  };

  const renderComponent = (
    mocks: MockedResponse[] = [],
    props = defaultProps,
  ): RenderResult => {
    return render(
      <MockedProvider addTypename={false} mocks={mocks}>
        <Routes>
          <Route
            element={<UploadVulnerabilities {...props} />}
            path={"/group/:groupName"}
          />
        </Routes>
      </MockedProvider>,
      { memoryRouter: { initialEntries: ["/group/test-group"] } },
    );
  };

  const testValidation = (obj: AnyObject): InferType<Schema> => {
    return validationSchema.validateSync(obj);
  };

  it("should render", (): void => {
    expect.hasAssertions();

    renderComponent();

    expect(screen.getByText(/downloadVulnerabilities/iu)).toBeInTheDocument();

    expect(screen.getByText(/updateVulnerabilities/iu)).toBeInTheDocument();

    jest.clearAllMocks();
  });

  it("should handle successful file download", async (): Promise<void> => {
    expect.hasAssertions();

    const mockOpen = jest.fn();
    // eslint-disable-next-line functional/immutable-data
    Object.defineProperty(window, "open", {
      value: mockOpen,
      writable: true,
    });

    const downloadMock = {
      request: {
        query: DOWNLOAD_VULNERABILITIES,
        variables: { findingId: "test-finding-id" },
      },
      result: {
        data: {
          downloadVulnerabilityFile: {
            success: true,
            url: "http://test-url.com",
          },
        },
      },
    };

    renderComponent([downloadMock]);

    expect(screen.getByText(/downloadVulnerabilities/iu)).toBeInTheDocument();

    fireEvent.click(screen.getByText(/downloadVulnerabilities/iu));

    await waitFor((): void => {
      expect(mockOpen).toHaveBeenCalledWith(
        "http://test-url.com",
        undefined,
        "noopener,noreferrer,",
      );
    });
    jest.clearAllMocks();
  });

  it("should handle successful file upload", async (): Promise<void> => {
    expect.hasAssertions();

    const btnConfirm = "Confirm";
    const file = new File(["test content"], "test.yml", { type: "text/yaml" });
    const uploadMock = {
      request: {
        query: UPLOAD_VULNERABILITIES,
        variables: { file, findingId: "test-finding-id" },
      },
      result: {
        data: {
          uploadFile: {
            message: "",
            success: true,
          },
        },
      },
    };
    const findingMock = {
      request: {
        query: GET_FINDING_INFO,
        variables: { findingId: "test-finding-id" },
      },
      result: {
        data: {
          finding: {
            __typename: "Finding",
            assignees: [],
            id: "test-finding-id",
            locations: [],
            releaseDate: "2019-05-08",
            remediated: false,
            status: "VULNERABLE",
            verified: false,
          },
        },
      },
    };
    const findingHeaderMock = {
      request: {
        query: GET_FINDING_HEADER,
        variables: {
          canGetZRSummary: false,
          canRetrieveHacker: false,
          findingId: "test-finding-id",
        },
      },
      result: {
        data: {
          finding: {
            __typename: "Finding",
            currentState: "CREATED",
            hacker: "integratesmanager@gmail.com",
            id: "test-finding-id",
            maxOpenSeverityScore: 2.7,
            maxOpenSeverityScoreV4: 6.9,
            minTimeToRemediate: 18,
            releaseDate: "2019-08-30 09:30:13",
            status: "VULNERABLE",
            title: "038. Business information leak",
            vulnerabilitiesSummary: {
              __typename: "VulnerabilitiesSummary",
              closed: 4,
              open: 25,
              openCritical: 0,
              openHigh: 0,
              openLow: 25,
              openMedium: 0,
            },
          },
        },
      },
    };

    renderComponent([uploadMock, findingMock, findingHeaderMock]);

    fireEvent.click(screen.getByText(/updateVulnerabilities/iu));

    expect(
      screen.getByText(/searchfindings\.tabresources\.modalfiletitle/iu),
    ).toBeInTheDocument();

    expect(
      screen.getByRole("button", {
        name: "Cancel",
      }),
    ).toBeEnabled();
    expect(
      screen.getByRole("button", {
        name: btnConfirm,
      }),
    ).toBeDisabled();

    const fileInput = screen.getByLabelText(/filename/iu);
    fireEvent.change(fileInput, { target: { files: [file] } });

    expect(
      screen.getByRole("button", {
        name: btnConfirm,
      }),
    ).toBeEnabled();

    fireEvent.click(
      screen.getByRole("button", {
        name: btnConfirm,
      }),
    );
    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenLastCalledWith(
        "groupAlerts.fileUpdated",
        "groupAlerts.titleSuccess",
      );
    });
    await waitFor((): void => {
      expect(mockRefetchData).toHaveBeenCalledTimes(1);
    });
    jest.clearAllMocks();
  });

  it("should display empty file error", (): void => {
    expect.hasAssertions();

    const testEmptyFilename = (): void => {
      testValidation({});
    };

    expect(testEmptyFilename).toThrow(new ValidationError("No file selected"));
  });

  it("should display not a yml file error", (): void => {
    expect.hasAssertions();

    const testNotYMLFile = (): void => {
      testValidation({
        filename: [
          new File([""], "testing.png", {
            lastModified: 1708631811207,
            type: "image/png",
          }),
        ],
      });
    };

    expect(testNotYMLFile).toThrow(
      new ValidationError("The file must be .yaml or .yml type"),
    );
  });

  it("should display wrong size error", (): void => {
    expect.hasAssertions();

    const fileObject = new File([""], "testing.yml", {
      lastModified: 1708631811207,
      type: "text/plain",
    });
    // Necesary for test purposes of generate size info whitout BLOB data
    // eslint-disable-next-line functional/immutable-data
    Object.defineProperty(fileObject, "size", {
      value: 1024 ** 4,
      writable: false,
    });

    const testWrongFilename = (): void => {
      testValidation({
        filename: [fileObject],
      });
    };

    expect(testWrongFilename).toThrow(
      new ValidationError("The file size must be less than 1MB"),
    );
  });

  it("should accept a valid yml file", (): void => {
    expect.hasAssertions();

    const testObject = {
      filename: [
        new File([""], "testing.yml", {
          lastModified: 1708631811207,
          type: "text/plain",
        }),
      ],
    };

    expect(testValidation(testObject)).toBe(testObject);
  });
});
