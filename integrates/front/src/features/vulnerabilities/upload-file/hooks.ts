import type { ApolloError } from "@apollo/client";
import includes from "lodash/includes";

import type { IErrorInfoAttr } from "./types";

import { errorMessageHelper } from "../utils";
import { msgError, msgErrorStick } from "utils/notifications";
import { translate } from "utils/translations/translate";

function handleFinalElse(message: string): void {
  if (
    message.includes(
      "Exception - The vulnerability path does not exist in the toe lines",
    )
  ) {
    const destructMsg: { msg: string; path: string } = JSON.parse(message);
    msgError(
      translate.t(
        "searchFindings.tabVuln.alerts.uploadFile.linesPathDoesNotExist",
        {
          path: destructMsg.path,
        },
      ),
    );
  } else if (
    message.includes(
      "Exception -  The vulnerability URL and field do not exist in the toe inputs",
    )
  ) {
    const destructMsg: { msg: string; path: string } = JSON.parse(message);
    msgError(
      translate.t(
        "searchFindings.tabVuln.alerts.uploadFile.inputUrlAndFieldDoNotExist",
        {
          path: destructMsg.path,
        },
      ),
    );
  } else if (
    message.includes(
      "Exception -  The vulnerability URL and field are not present in the toe inputs",
    )
  ) {
    const destructMsg: { msg: string; path: string } = JSON.parse(message);
    msgError(
      translate.t(
        "searchFindings.tabVuln.alerts.uploadFile.inputUrlAndFieldAreNotPresent",
        {
          path: destructMsg.path,
        },
      ),
    );
  } else if (
    message.includes(
      "Exception -  The vulnerability address and port do not exist in the toe ports",
    )
  ) {
    const destructMsg: { msg: string; path: string } = JSON.parse(message);
    msgError(
      translate.t(
        "searchFindings.tabVuln.alerts.uploadFile.addressAndPortDoNotExist",
        {
          path: destructMsg.path,
        },
      ),
    );
  } else if (
    message.includes("Exception - Error invalid severity CVSS v4 vector string")
  ) {
    msgError(
      translate.t(
        "searchFindings.tabVuln.severityInfo.alerts.invalidSeverityVectorV4",
      ),
    );
  } else if (
    message.includes(
      "Exception - Error invalid severity CVSS v3.1 vector string",
    )
  ) {
    msgError(
      translate.t(
        "searchFindings.tabVuln.severityInfo.alerts.invalidSeverityVector",
      ),
    );
  } else if (message.startsWith("Exception - Error missing severity")) {
    if (message.includes("v4.0")) {
      msgError(
        translate.t(
          "searchFindings.tabVuln.severityInfo.alerts.invalidSeverity",
        ),
        translate.t(
          "searchFindings.tabVuln.severityInfo.alerts.missingSeverity",
          {
            version: "v4.0",
          },
        ),
      );
    } else {
      msgError(
        translate.t(
          "searchFindings.tabVuln.severityInfo.alerts.invalidSeverity",
        ),
        translate.t(
          "searchFindings.tabVuln.severityInfo.alerts.missingSeverity",
          {
            version: "v3.1",
          },
        ),
      );
    }
  } else {
    errorMessageHelper(message);
  }
}

function handleUploadError(updateError: ApolloError): void {
  updateError.graphQLErrors.forEach(({ message }): void => {
    if (message.includes("Exception - Error in range limit numbers")) {
      const errorObject: IErrorInfoAttr = JSON.parse(message);
      msgError(
        `${translate.t("groupAlerts.rangeError")} ${errorObject.values}`,
      );
    } else if (
      message.startsWith(
        "Exception - Uploaded vulnerability is a confirmed Zero Risk",
      )
    ) {
      msgError(
        translate.t("groupAlerts.zeroRiskAlreadyUploaded", {
          info: message.split("Zero Risk:")[1],
        }),
      );
    } else if (
      message.includes(
        "Exception -  The line does not exist in the range of 0 and lines of code",
      )
    ) {
      const destructMsg: { msg: string; path: string } = JSON.parse(message);
      msgError(
        translate.t(
          "searchFindings.tabVuln.alerts.uploadFile.lineDoesNotExistInLoc",
          {
            line: destructMsg.msg.split("code: ")[1],
            path: destructMsg.path,
          },
        ),
      );
    } else if (message.includes("Exception - Invalid Schema")) {
      const errorObject: IErrorInfoAttr = JSON.parse(message);
      if (errorObject.values.length > 0 || errorObject.keys.length > 0) {
        const listValuesFormated: string[] = Array.from(
          new Set(
            errorObject.values.map((valX: string): string => {
              return translate.t(
                "searchFindings.tabVuln.alerts.uploadFile.value",
                {
                  path: valX[1],
                  pattern: valX[0],
                },
              );
            }),
          ),
        );
        const listKeysFormated: string[] = Array.from(
          new Set(
            errorObject.keys.map((valY: string): string => {
              const key = valY.split(",")[0].trim();
              const path = valY.split(",")[1].trim();

              return translate.t(
                "searchFindings.tabVuln.alerts.uploadFile.key",
                {
                  key,
                  path,
                },
              );
            }),
          ),
        );
        msgErrorStick(
          listKeysFormated.join("") + listValuesFormated.join(""),
          translate.t("groupAlerts.invalidSchema"),
        );
      } else {
        msgError(translate.t("groupAlerts.invalidSchema"));
      }
    } else if (
      includes(message, "Exception - This finding has missing fields")
    ) {
      msgError(
        translate.t(
          "searchFindings.tabVuln.alerts.uploadFile.missingFindingInfo",
          {
            missingFields: message.split("fields: ")[1],
          },
        ),
      );
    } else {
      handleFinalElse(message);
    }
  });
}

export { handleUploadError };
