import { isEmpty, isNil } from "lodash";
import { mixed, object } from "yup";
import type { InferType, ObjectSchema, Schema } from "yup";

import { translate } from "utils/translations/translate";

const validationSchema: ObjectSchema<InferType<Schema>> = object().shape({
  filename: mixed()
    .isValidFunction((value: unknown): boolean => {
      if (value === undefined || isEmpty(value)) {
        return false;
      }

      return (value as FileList).length !== 0 || !isNil(value as FileList);
    }, translate.t("groupAlerts.noFileSelected"))
    .isValidFileSize()
    .isValidFileType(["yml", "yaml"], translate.t("groupAlerts.fileTypeYaml")),
});

export { validationSchema };
