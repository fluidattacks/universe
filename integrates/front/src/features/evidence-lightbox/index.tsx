import { faAngleLeft } from "@fortawesome/free-solid-svg-icons/faAngleLeft";
import { faAngleRight } from "@fortawesome/free-solid-svg-icons/faAngleRight";
import { faXmark } from "@fortawesome/free-solid-svg-icons/faXmark";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import * as React from "react";
import ImageViewer from "react-simple-image-viewer";

import { useEvidenceAudit } from "hooks/use-audit";
import type { IEventEvidenceAttr } from "pages/event/evidence/types";
import type { IEvidenceItem } from "pages/finding/evidence/types";

interface IEvidenceLightboxProps {
  readonly instance: { name: "Event" | "Finding"; id: string };
  readonly evidenceImages: string[];
  readonly currentImage: number;
  readonly onClose: (index: number, isOpen: boolean) => void;
  readonly evidences: Record<string, IEvidenceItem | IEventEvidenceAttr>;
}

const EvidenceLightbox: React.FC<IEvidenceLightboxProps> = ({
  instance,
  currentImage,
  evidenceImages,
  onClose,
  evidences,
}): JSX.Element => {
  const { addEvidenceAudit } = useEvidenceAudit();
  const [imageIndex, setImageIndex] = React.useState(currentImage);

  const handleOnClose = React.useCallback((): void => {
    onClose(0, false);
  }, [onClose]);

  const leftArrowClick = (currentValue: number): (() => void) => {
    return (): void => {
      if (currentValue === 0) setImageIndex(evidenceImages.length - 1);
      else setImageIndex(currentValue - 1);
    };
  };
  const rightArrowClick = (currentValue: number): (() => void) => {
    return (): void => {
      if (currentValue === evidenceImages.length - 1) setImageIndex(0);
      else setImageIndex(currentValue + 1);
    };
  };
  React.useEffect((): void => {
    addEvidenceAudit(evidences, evidenceImages[imageIndex], instance);
  }, [addEvidenceAudit, evidenceImages, evidences, imageIndex, instance]);

  return currentImage >= 0 ? (
    <div aria-label={"ImageViewer"} role={"dialog"}>
      <ImageViewer
        backgroundStyle={{
          backgroundColor: "rgba(0,0,0,0.9)",
          zIndex: "99999",
        }}
        closeComponent={<FontAwesomeIcon icon={faXmark} />}
        closeOnClickOutside={true}
        currentIndex={currentImage}
        disableScroll={true}
        leftArrowComponent={
          <FontAwesomeIcon
            icon={faAngleLeft}
            onClick={leftArrowClick(imageIndex)}
          />
        }
        onClose={handleOnClose}
        rightArrowComponent={
          <FontAwesomeIcon
            icon={faAngleRight}
            onClick={rightArrowClick(imageIndex)}
          />
        }
        src={evidenceImages.map((url): string => `${location.href}/${url}`)}
      />
    </div>
  ) : (
    <div />
  );
};

export { EvidenceLightbox };
