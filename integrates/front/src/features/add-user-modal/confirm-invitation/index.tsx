import { useConfirmDialog } from "@fluidattacks/design";
import { useCallback, useContext } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { useGroupActors, useOrganizationActors } from "./hooks";

import { authContext } from "context/auth";

const useConfirmInvitation = (
  shouldConfirm: boolean,
  type: "group" | "organization" | "user",
  groupName: string | undefined,
  organizationId: string | undefined,
): {
  ConfirmInvitationDialog: React.FC;
  confirmInvitation: (email: string) => Promise<boolean>;
} => {
  const { t } = useTranslation();
  const { userEmail } = useContext(authContext);
  const { confirm, ConfirmDialog: ConfirmInvitationDialog } =
    useConfirmDialog();

  const groupActors = useGroupActors(shouldConfirm ? groupName : undefined);
  const organizationActors = useOrganizationActors(
    shouldConfirm ? organizationId : undefined,
  );
  const actorEmails = [...groupActors, ...organizationActors]
    .map((actor): string | undefined => /<(?<email>.*?)>/u.exec(actor)?.[1])
    .filter((actorEmail): actorEmail is string => actorEmail !== undefined);

  const confirmInvitation = useCallback(
    async (email: string): Promise<boolean> => {
      const sameEmailDomain = userEmail.split("@")[1] === email.split("@")[1];
      const inAuthors = actorEmails.includes(email);

      if (!shouldConfirm || sameEmailDomain || inAuthors) {
        return true;
      }

      const target = {
        group: `group ${groupName ?? ""}`,
        organization: "this organization",
        user: "the platform",
      }[type];

      return confirm({
        message: (
          <p>
            {t("userModal.confirm.subtitle1")}&nbsp;
            <b>{email}</b>&nbsp;
            {t("userModal.confirm.subtitle2")} <br />
            {`${t("userModal.confirm.subtitle3")}${target}?`}
          </p>
        ),
        title: t("userModal.confirm.title"),
      });
    },
    [actorEmails, confirm, groupName, shouldConfirm, t, type, userEmail],
  );

  return { ConfirmInvitationDialog, confirmInvitation };
};

export { useConfirmInvitation };
