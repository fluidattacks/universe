import { useAbility } from "@casl/react";
import { ComboBox, Input } from "@fluidattacks/design";
import type { IItem } from "@fluidattacks/design";
import camelCase from "lodash/camelCase";
import { Fragment, useEffect } from "react";
import { useTranslation } from "react-i18next";

import { useStakeholderAutofill } from "../hooks";
import type { IAddStakeholderFormProps } from "../types";
import { groupLevelRoles, organizationLevelRoles } from "../utils";
import { authzPermissionsContext } from "context/authz/config";

const AddUserForm = ({
  action,
  allSuggestions,
  control,
  formState,
  groupName,
  organizationId,
  register,
  reset,
  type,
}: IAddStakeholderFormProps): JSX.Element => {
  const { t } = useTranslation();
  const permissions = useAbility(authzPermissionsContext);
  const { autofill, loadAutofill } = useStakeholderAutofill(
    type,
    groupName,
    organizationId,
  );

  const groupRoles = type === "group" ? groupLevelRoles : [];
  const orgRoles = type === "organization" ? organizationLevelRoles : [];

  const selectItems = [
    ...groupRoles
      .filter((role): boolean =>
        permissions.can(`grant_group_level_role:${role}`),
      )
      .map(
        (role): IItem => ({
          name: t(`userModal.roles.${camelCase(role)}`),
          value: role.toUpperCase(),
        }),
      ),
    ...orgRoles
      .filter((role): boolean =>
        permissions.can(`grant_organization_level_role:${role}`),
      )
      .map(
        (role): IItem => ({
          name: t(`userModal.roles.${camelCase(role)}`),
          value: role.toUpperCase(),
        }),
      ),
  ];

  useEffect((): void => {
    if (autofill)
      reset({ email: autofill.email, responsibility: autofill.responsibility });
  }, [autofill, reset]);

  return (
    <Fragment>
      <Input
        disabled={action === "edit"}
        error={formState.errors.email?.message?.toString()}
        isTouched={"email" in formState.touchedFields}
        isValid={!("email" in formState.errors)}
        label={t("userModal.emailText")}
        name={"email"}
        onBlur={type === "group" ? loadAutofill : undefined}
        placeholder={t("userModal.emailPlaceholder")}
        register={register}
        required={true}
        suggestions={allSuggestions}
        type={"email"}
      />
      <ComboBox
        control={control}
        items={selectItems}
        label={t("userModal.role")}
        name={"role"}
        placeholder={t("userModal.rolePlaceholder")}
        required={true}
      />
      {type === "group" ? (
        <Input
          error={formState.errors.responsibility?.message?.toString()}
          isTouched={"responsibility" in formState.touchedFields}
          isValid={!("responsibility" in formState.errors)}
          label={t("userModal.responsibility")}
          name={"responsibility"}
          placeholder={t("userModal.responsibilityPlaceholder")}
          register={register}
          required={true}
        />
      ) : undefined}
    </Fragment>
  );
};

export { AddUserForm };
