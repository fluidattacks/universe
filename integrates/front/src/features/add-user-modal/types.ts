import type { TFormMethods } from "@fluidattacks/design";

interface IStakeholderFormValues {
  email: string;
  responsibility?: string;
  role: string;
}

interface IAddStakeholderModalProps {
  action: "add" | "edit";
  editTitle: string;
  initialValues?: IStakeholderFormValues;
  open: boolean;
  organizationId?: string;
  groupName?: string;
  domainSuggestions: string[];
  suggestions: string[];
  title: string;
  type: "group" | "organization";
  onClose: () => void;
  onSubmit: (values: IStakeholderFormValues) => Promise<void>;
}

interface IAddStakeholderFormProps
  extends TFormMethods<IStakeholderFormValues> {
  action: "add" | "edit";
  allSuggestions: string[];
  groupName?: string;
  organizationId?: string;
  type: "group" | "organization";
}

export type {
  IAddStakeholderFormProps,
  IAddStakeholderModalProps,
  IStakeholderFormValues,
};
