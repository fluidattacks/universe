import type { IGroupData } from "pages/organization/groups/types";
import { getPlan } from "pages/organization/groups/utils";

const isOnlyEssential = (group: IGroupData): boolean =>
  getPlan(group) === "Essential" && !group.hasAdvanced && group.hasEssential;

export { isOnlyEssential };
