import { screen, waitFor } from "@testing-library/react";
import { Route, Routes } from "react-router-dom";

import { HelperButtons } from "./helper-buttons";

import { EmptyVulnerabilities } from ".";
import { render } from "mocks";

const mockNavigatePush: jest.Mock = jest.fn();
jest.mock(
  "react-router-dom",
  (): Record<string, unknown> => ({
    ...jest.requireActual("react-router-dom"),
    useNavigate: (): jest.Mock => mockNavigatePush,
  }),
);

describe("empty Vulnerabilities", (): void => {
  const memoryRouter = {
    initialEntries: ["/orgs/testorg/groups/unittesting/vulns"],
  };

  it("should render with correct props and data", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route
          element={<EmptyVulnerabilities />}
          path={"/orgs/:organizationName/groups/:groupName/vulns"}
        />
      </Routes>,
      {
        memoryRouter,
      },
    );

    await waitFor((): void => {
      expect(
        screen.getByText("Add an application to be tested"),
      ).toBeInTheDocument();
    });

    expect(
      screen.getByText(
        "Add your first repository and (optionally) an environment to get started",
      ),
    ).toBeInTheDocument();
    expect(
      screen.getByText("1.components.empty.steps.addRoot.stepTitle"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("2.components.empty.steps.cloning.stepTitle"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("3.components.empty.steps.testing"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("4.components.empty.steps.findings.stepTitle"),
    ).toBeInTheDocument();

    expect(
      screen.getByRole("button", {
        name: "components.empty.steps.addRoot.button",
      }),
    ).toBeInTheDocument();
  });

  it("should render with case 1 and error", (): void => {
    expect.hasAssertions();

    render(<HelperButtons state={"error"} stepNumber={1} />, {
      memoryRouter,
    });

    expect(
      screen.getByText("components.empty.steps.addRoot.button"),
    ).toBeInTheDocument();
  });

  it("should render with case 1 and redirect to scope", (): void => {
    expect.hasAssertions();

    render(<HelperButtons state={"error"} stepNumber={1} />, {
      memoryRouter,
    });

    expect(
      screen.getByText("components.empty.steps.addRoot.button"),
    ).toBeInTheDocument();

    screen.getByText("components.empty.steps.addRoot.button").click();

    expect(mockNavigatePush).toHaveBeenCalledWith(
      "/orgs/testorg/groups/unittesting/scope",
    );
  });

  it("should render with case 2 and error", (): void => {
    expect.hasAssertions();

    render(<HelperButtons state={"error"} stepNumber={2} />, {
      memoryRouter,
    });

    expect(
      screen.getByText("components.empty.steps.cloning.button"),
    ).toBeInTheDocument();
  });

  it("should render with case 4", (): void => {
    expect.hasAssertions();

    render(<HelperButtons state={"error"} stepNumber={4} />, {
      memoryRouter,
    });

    expect(
      screen.getByText("components.empty.steps.findings.button"),
    ).toBeInTheDocument();
  });
});
