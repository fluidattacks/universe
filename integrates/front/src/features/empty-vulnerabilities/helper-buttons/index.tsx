import { Button, Container, Link, Text } from "@fluidattacks/design";
import { useCallback } from "react";
import { useTranslation } from "react-i18next";
import { useLocation, useNavigate } from "react-router-dom";
import { useTheme } from "styled-components";

import type { TStepState } from "components/progress-indicator/types";

interface IHelperButtonsProps {
  readonly stepNumber: number;
  readonly state: TStepState;
}

const HelperButtons = ({
  stepNumber,
  state,
}: IHelperButtonsProps): JSX.Element => {
  const theme = useTheme();
  const { t } = useTranslation();
  const location = useLocation();
  const navigate = useNavigate();

  const goToScope = useCallback((): void => {
    const url = location.pathname;
    navigate(url.replace("/vulns", "/scope"));
  }, [navigate, location]);

  return (
    <Container
      alignItems={"center"}
      display={"flex"}
      flexDirection={"column"}
      gap={2}
      justify={"center"}
    >
      {stepNumber === 1 && state === "error" && (
        <Button onClick={goToScope} variant={"primary"}>
          {t("components.empty.steps.addRoot.button")}
        </Button>
      )}
      {stepNumber === 2 && state === "error" && (
        <Link href={"mailto:help@fluidattacks.com"}>
          <Button variant={"primary"}>
            {t("components.empty.steps.cloning.button")}
          </Button>
        </Link>
      )}
      {stepNumber === 4 && (
        <Container
          alignItems={"center"}
          display={"flex"}
          flexDirection={"column"}
          gap={2}
          justify={"center"}
        >
          <Button onClick={goToScope} variant={"primary"}>
            {t("components.empty.steps.findings.button")}
          </Button>
          <Container
            alignItems={"flex-start"}
            border={`1px solid ${theme.palette.gray[300]}`}
            borderRadius={"4px"}
            display={"flex"}
            flexDirection={"column"}
            gap={0.5}
            maxWidth={"700px"}
            pb={1.25}
            pl={1.25}
            pr={1.25}
            pt={1.25}
          >
            <Text color={theme.palette.black} fontWeight={"bold"} size={"sm"}>
              {t("components.empty.steps.findings.card.title")}
            </Text>
            <ol style={{ paddingLeft: "20px" }} type={"A"}>
              <li>{t("components.empty.steps.findings.card.reasons.a")}</li>
              <li>{t("components.empty.steps.findings.card.reasons.b")}</li>
              <li>{t("components.empty.steps.findings.card.reasons.c")}</li>
            </ol>
            <Text size={"sm"}>
              {t("components.empty.steps.findings.card.note")}
              <Link href={"https://calendar.app.google/yZKUtRBXT5swdHnC7"}>
                <b>{t("components.empty.steps.findings.card.contact")}</b>
              </Link>
            </Text>
          </Container>
        </Container>
      )}
    </Container>
  );
};

export { HelperButtons };
