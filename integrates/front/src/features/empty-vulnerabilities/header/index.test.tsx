import { screen } from "@testing-library/react";

import { Header } from ".";
import { render } from "mocks";

describe("header", (): void => {
  it("should render header component", (): void => {
    expect.hasAssertions();

    const headerProps = {
      icon: "text.png",
      subtitle: "Test Subtitle",
      title: "Test Title",
    };

    render(<Header headerProps={headerProps} />);

    expect(screen.getByText("Test Title")).toBeInTheDocument();
    expect(screen.getByText("Test Subtitle")).toBeInTheDocument();
  });
});
