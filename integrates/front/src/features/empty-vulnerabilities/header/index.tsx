import { Container, EmptyState, Text } from "@fluidattacks/design";
import isNil from "lodash/isNil";
import { useTheme } from "styled-components";

import type { IHeader } from "./types";

import { translate } from "utils/translations/translate";

const Header = ({
  headerProps,
}: Readonly<{ headerProps: IHeader }>): JSX.Element => {
  const theme = useTheme();
  const { icon, title, subtitle, description } = headerProps;
  const { t } = translate;

  return (
    <Container>
      <EmptyState
        description={t(subtitle)}
        imageSrc={icon}
        padding={0}
        title={t(title)}
      />
      {!isNil(description) && (
        <Text
          color={theme.palette.gray[400]}
          mt={0.75}
          size={"sm"}
          textAlign={"center"}
        >
          {t(description)}
        </Text>
      )}
    </Container>
  );
};

export { Header };
