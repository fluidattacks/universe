import type { IHeader } from "./header/types";

import type { IStepProps } from "components/progress-indicator/types";

interface IStepItemsFormat extends IStepProps {
  header: IHeader;
}

export type { IStepItemsFormat };
