import {
  type IFilterOptions,
  type IOptionsProps,
  type IUseFilterProps,
  useFilters,
} from "@fluidattacks/design";
import type { IOptions } from "@fluidattacks/design/dist/components/modal/filters-modal/types";
import { useCallback, useMemo } from "react";
import { useTranslation } from "react-i18next";

import { getCheckedFilter } from "components/filter/utils";
import type { GetToePortsQueryVariables } from "gql/graphql";
import type { IToePortData } from "pages/group/surface/ports/types";
import { formatBoolean, formatStatus } from "utils/format-helpers";

const useToEPortsFilters = ({
  dataset,
  groupName,
  setBackFilters,
  setFilteredDataset,
}: Readonly<{
  dataset: IToePortData[];
  groupName: string;
  setBackFilters: React.Dispatch<
    React.SetStateAction<{
      bePresent: GetToePortsQueryVariables["bePresent"];
      rootId: GetToePortsQueryVariables["rootId"];
    }>
  >;
  setFilteredDataset: React.Dispatch<React.SetStateAction<IToePortData[]>>;
}>): IUseFilterProps<IToePortData> => {
  const { t } = useTranslation();

  const options: IOptionsProps<IToePortData>[] = useMemo(
    (): IOptionsProps<IToePortData>[] => [
      {
        filterOptions: [
          {
            key: "rootNickname",
            label: t("group.toe.ports.root"),
            options: Array.from(
              new Set(dataset.map((toePort): string => toePort.rootNickname)),
            ).map(
              (nickname): IOptions => ({
                label: nickname,
                value: nickname,
              }),
            ),
            type: "select",
          },
        ],
        label: t("group.toe.ports.root"),
      },
      {
        filterOptions: [
          {
            key: "address",
            label: t("group.toe.ports.address"),
            options: Array.from(
              new Set(dataset.map((toePort): string => toePort.address)),
            ).map(
              (address): IOptions => ({
                label: address,
                value: address,
              }),
            ),
            type: "select",
          },
        ],
        label: t("group.toe.ports.address"),
      },
      {
        filterOptions: [
          {
            key: "port",
            label: t("group.toe.ports.port"),
            type: "numberRange",
          },
        ],
        label: t("group.toe.ports.port"),
      },
      {
        filterOptions: [
          {
            key: "hasVulnerabilities",
            label: t("group.toe.ports.status"),
            options: [
              { label: formatStatus(true), value: "true" },
              { label: formatStatus(false), value: "false" },
            ],
            type: "radioButton",
          },
        ],
        label: t("group.toe.ports.status"),
      },
      {
        filterOptions: [
          {
            key: "seenAt",
            label: t("group.toe.ports.seenAt"),
            type: "dateRange",
          },
        ],
        label: t("group.toe.ports.seenAt"),
      },
      {
        filterOptions: [
          {
            key: "bePresent",
            label: t("group.toe.ports.bePresent"),
            options: [
              { label: formatBoolean(true), value: "true" },
              { label: formatBoolean(false), value: "false" },
            ],
            type: "radioButton",
          },
        ],
        label: t("group.toe.ports.bePresent"),
      },
    ],
    [dataset, t],
  );

  const handleFilterChange = useCallback(
    (newFilters: readonly IFilterOptions<IToePortData>[]): void => {
      const newFiltersArray = newFilters as IFilterOptions<IToePortData>[];
      const rootNickname = getCheckedFilter(
        newFiltersArray,
        "rootNickname",
      )?.value;
      const bePresent = getCheckedFilter(newFiltersArray, "bePresent")?.value;
      const newBackFilters = {
        bePresent: bePresent === "" ? undefined : bePresent === "true",
        rootId: rootNickname === "" ? undefined : rootNickname,
      };
      setBackFilters(newBackFilters);
    },
    [setBackFilters],
  );

  return useFilters({
    dataset,
    localStorageKey: `toePortsTableFilters-${groupName}`,
    onFiltersChange: handleFilterChange,
    options,
    setFilteredDataset,
  });
};

export { useToEPortsFilters };
