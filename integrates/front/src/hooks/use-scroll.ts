import { useCallback, useEffect } from "react";

interface IUseScrollParams {
  containerId: string;
  percentageThreshold: number;
  onThresholdReached: () => void;
}

/**
 * Detects when the user scrolls to a certain percentage
 * of the bottom of a scrollable container.
 */
const useScroll = ({
  containerId,
  percentageThreshold,
  onThresholdReached,
}: IUseScrollParams): void => {
  const handleScroll = useCallback(
    (event: Event): void => {
      const target = event.target as HTMLDivElement;
      const currentScroll = target.scrollTop + target.clientHeight;
      const PERCENTAGE_DIVIDER = 100;
      const reachedThreshold =
        currentScroll / target.scrollHeight >=
        percentageThreshold / PERCENTAGE_DIVIDER;

      if (reachedThreshold) {
        onThresholdReached();
      }
    },
    [onThresholdReached, percentageThreshold],
  );

  useEffect((): VoidFunction => {
    const container = document.getElementById(containerId);
    container?.addEventListener("scroll", handleScroll);

    return (): void => {
      container?.removeEventListener("scroll", handleScroll);
    };
  }, [containerId, handleScroll]);
};

export { useScroll };
