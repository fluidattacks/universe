import { useMemo } from "react";

import { useStoredState } from "./use-stored-state";

const timeDifferenceThreshold = 10000;

const useDailyAlert = (alertName: string, id: string): boolean => {
  const [storedDate, setStoredDate] = useStoredState(
    `alert-show-${alertName}-${id}`,
    "",
    localStorage,
  );

  const actualDate = useMemo((): Date => new Date(), []);

  if (!storedDate) {
    setStoredDate(actualDate.toISOString());

    return true;
  }

  const usedDate = new Date(storedDate);

  if (actualDate.getDay() !== usedDate.getDay()) {
    setStoredDate(actualDate.toISOString());

    return true;
  }

  return actualDate.getTime() - usedDate.getTime() < timeDifferenceThreshold;
};

export { useDailyAlert };
