/* eslint-disable react-hooks/exhaustive-deps, functional/immutable-data */
import { useCallback, useEffect, useRef } from "react";

interface IHandledTimeout {
  addTimeout: (callback: () => void, ms: number) => void;
}

const TIMEOUT_MS = 1000;

const useHandledTimeout = (): IHandledTimeout => {
  const timeoutRef = useRef<NodeJS.Timeout[]>([]);

  useEffect((): VoidFunction => {
    return (): void => {
      timeoutRef.current.forEach((timeId): void => {
        clearTimeout(timeId);
      });
    };
  }, []);

  const addTimeout = useCallback(
    (callback: () => void, ms = TIMEOUT_MS): void => {
      const timeId = setTimeout(callback, ms);
      timeoutRef.current.push(timeId);
    },
    [timeoutRef],
  );

  return { addTimeout };
};

export { useHandledTimeout };
