import { useQuery } from "@apollo/client";
import { useTranslation } from "react-i18next";

import type { GetOrganizationPoliciesAtOrganizationQuery } from "gql/graphql";
import { GET_ORGANIZATION_POLICIES } from "pages/organization/policies/queries";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";

interface IUsePoliciesQuery {
  orgPolicies: GetOrganizationPoliciesAtOrganizationQuery | undefined;
  loadingPolicies: boolean;
}
const usePolicies = (organizationId: string): IUsePoliciesQuery => {
  const { t } = useTranslation();
  const { data, loading } = useQuery(GET_ORGANIZATION_POLICIES, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        if (organizationId) {
          msgError(t("groupAlerts.errorTextsad"));
          Logger.warning(
            "An error occurred fetching organization policies",
            error,
          );
        }
      });
    },
    variables: { organizationId },
  });

  return {
    loadingPolicies: loading,
    orgPolicies: data,
  };
};

export { usePolicies };
