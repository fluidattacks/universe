import { useEffect } from "react";

const useClickOutside = (
  element: HTMLElement | null,
  onClickOutside: VoidFunction,
  addEventListenerToFullDocument = false,
): void => {
  useEffect((): VoidFunction => {
    const handleClick = (event: MouseEvent): void => {
      if (element && !element.contains(event.target as Node)) {
        onClickOutside();
      }
    };

    if (addEventListenerToFullDocument) {
      document.addEventListener("click", handleClick, true);

      return (): void => {
        document.removeEventListener("click", handleClick, true);
      };
    }
    document
      .getElementById("root")
      ?.addEventListener("click", handleClick, true);

    return (): void => {
      document
        .getElementById("root")
        ?.removeEventListener("click", handleClick, true);
    };
  }, [addEventListenerToFullDocument, element, onClickOutside]);
};

export { useClickOutside };
