import { useQuery } from "@apollo/client";
import { useCallback, useState } from "react";
import { useMatch } from "react-router-dom";

import { GET_USER_ORGANIZATIONS } from "./queries";
import {
  getOrgByDefault,
  getOrgFromLocalStorage,
  getOrgFromUrl,
} from "./utils";

import type { GetUserOrganizationsQuery } from "gql/graphql";
import { Logger } from "utils/logger";

interface IInitialOrganizationResult {
  data: {
    organizations: string[];
  };
  orgName: string;
  loading: boolean;
}

/** Determines the organization to show by default  */
const useInitialOrganization = (): IInitialOrganizationResult => {
  const orgMatch = useMatch("/orgs/:organizationName/*");
  const { organizationName: orgFromUrl } = orgMatch?.params ?? {};

  const [orgName, setOrgName] = useState<string>("");

  const handleComplete = useCallback(
    (data: GetUserOrganizationsQuery): void => {
      if (data.me.organizations.length > 0) {
        const validOrganizations = data.me.organizations.map(
          (organization): string => organization.name.toLowerCase(),
        );

        const org =
          getOrgFromUrl(orgFromUrl, validOrganizations) ??
          getOrgFromLocalStorage(validOrganizations) ??
          getOrgByDefault(validOrganizations);

        setOrgName(org ?? "");
      } else {
        setOrgName("");
      }
    },
    [orgFromUrl],
  );

  const { data, loading } = useQuery(GET_USER_ORGANIZATIONS, {
    onCompleted: handleComplete,
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.error("Couldn't get user organizations", error);
      });
    },
  });

  return {
    data: {
      organizations:
        data?.me.organizations.map((org): string => org.name) ?? [],
    },
    loading: loading || orgName === "",
    orgName,
  };
};

export { useInitialOrganization };
