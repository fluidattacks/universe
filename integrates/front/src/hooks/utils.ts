import _ from "lodash";

interface IOrgStorage {
  deleted?: boolean;
  name?: string;
}

const getOrgFromUrl = (
  orgFromUrl: string | undefined,
  organizations: string[],
): string | undefined => {
  if (orgFromUrl === undefined || !organizations.includes(orgFromUrl)) {
    return undefined;
  }

  const orgObject: IOrgStorage = {
    deleted: false,
    name: orgFromUrl.toLowerCase(),
  };
  localStorage.setItem("organizationV2", JSON.stringify(orgObject));

  return orgFromUrl;
};

const getOrgFromLocalStorage = (orgs: string[]): string | undefined => {
  const lastOrganization = localStorage.getItem("organizationV2");

  if (_.isNull(lastOrganization)) {
    return undefined;
  }

  const { name = "", deleted = false }: IOrgStorage =
    JSON.parse(lastOrganization);

  if (name && !deleted && orgs.includes(name.toLowerCase())) {
    return name.toLowerCase();
  }

  return undefined;
};

const getOrgByDefault = (organizations: string[]): string | undefined => {
  const orgObject: IOrgStorage = { deleted: false, name: organizations[0] };
  localStorage.setItem("organizationV2", JSON.stringify(orgObject));

  return organizations[0];
};

export { getOrgFromUrl, getOrgFromLocalStorage, getOrgByDefault };
