import { useCallback, useState } from "react";

import { useStoredState } from "./use-stored-state";

interface IUseModal {
  open: () => void;
  close: () => void;
  isOpen: boolean;
  name: string;
  dontShow?: boolean;
  setDontShow?: (value: boolean) => void;
  setIsOpen: (value: boolean) => void;
}

const useModal = (name: string): IUseModal => {
  const [isOpen, setIsOpen] = useState(false);
  const [dontShow, setDontShow] = useStoredState(
    `dont-show-${name}`,
    false,
    localStorage,
  );

  const open = useCallback((): void => {
    if (dontShow) return;
    setIsOpen(true);
  }, [dontShow]);
  const close = useCallback((): void => {
    setIsOpen(false);
  }, []);

  return { close, dontShow, isOpen, name, open, setDontShow, setIsOpen };
};

export type { IUseModal };
export { useModal };
