import {
  Divider,
  IconButton,
  ListItem,
  ListItemsWrapper,
  ShowOnHover,
  Tooltip,
} from "@fluidattacks/design";
import { StrictMode, useCallback, useMemo } from "react";
import { useTranslation } from "react-i18next";

import type { IGButton, ITimeFilterButton, ITypeFilterButton } from "./types";

import type { IDocumentValues } from "../../ctx";
import { mergedDocuments } from "../../ctx";
import { DaysLabel } from "../graphic/helpers";

const GButton = ({
  alternative,
  changeToAlternative,
  currentDocumentName,
  index,
}: IGButton): JSX.Element => {
  const onClick = useCallback((): void => {
    changeToAlternative(index);
  }, [changeToAlternative, index]);

  return (
    <Tooltip
      id={alternative.tooltip.split(" ").join("_")}
      tip={alternative.tooltip}
    >
      <ListItem
        onClick={onClick}
        selected={alternative.documentName === currentDocumentName}
        value={""}
      >
        {alternative.label}
      </ListItem>
    </Tooltip>
  );
};

const TimeFilterButton = ({
  shouldDisplayAll = true,
  subjectName,
  subject,
  timeFilter,
  changeToThirtyDays,
  changeToSixtyDays = undefined,
  changeToNinety,
  changeToOneHundredEighty = undefined,
  changeToAll,
}: ITimeFilterButton): JSX.Element => {
  const { t } = useTranslation();
  if (!timeFilter) {
    return <StrictMode />;
  }

  return (
    <StrictMode>
      <Tooltip
        id={"analytics.limitData.thirtyDays.tooltip.id"}
        tip={t("analytics.limitData.thirtyDays.tooltip")}
      >
        <ListItem
          onClick={changeToThirtyDays}
          selected={
            shouldDisplayAll
              ? subjectName === `${subject}_30`
              : subjectName === subject
          }
          value={"analytics.limitData.thirtyDays"}
        >
          <DaysLabel days={"30"} />
        </ListItem>
      </Tooltip>
      {changeToSixtyDays !== undefined && (
        <Tooltip
          id={"analytics.limitData.sixtyDays.tooltip.id"}
          tip={t("analytics.limitData.sixtyDays.tooltip")}
        >
          <ListItem
            onClick={changeToSixtyDays}
            selected={subjectName === `${subject}_60`}
            value={"analytics.limitData.sixtyDays"}
          >
            <DaysLabel days={"60"} />
          </ListItem>
        </Tooltip>
      )}
      <Tooltip
        id={"analytics.limitData.ninetyDays.tooltip.id"}
        tip={t("analytics.limitData.ninetyDays.tooltip")}
      >
        <ListItem
          onClick={changeToNinety}
          selected={subjectName === `${subject}_90`}
          value={"analytics.limitData.ninetyDays"}
        >
          <DaysLabel days={"90"} />
        </ListItem>
      </Tooltip>
      {changeToOneHundredEighty !== undefined && (
        <Tooltip
          id={"analytics.limitData.oneHundredEighty.tooltip.id"}
          tip={t("analytics.limitData.oneHundredEighty.tooltip")}
        >
          <ListItem
            onClick={changeToOneHundredEighty}
            selected={subjectName === `${subject}_180`}
            value={"analytics.limitData.oneHundredEighty"}
          >
            <DaysLabel days={"180"} />
          </ListItem>
        </Tooltip>
      )}
      {shouldDisplayAll ? (
        <Tooltip
          id={"analytics.limitData.all.tooltip.id"}
          tip={t("analytics.limitData.all.tooltip")}
        >
          <ListItem
            onClick={changeToAll}
            selected={subjectName === subject}
            value={"analytics.limitData.all"}
          >
            <DaysLabel days={"allTime"} />
          </ListItem>
        </Tooltip>
      ) : null}
    </StrictMode>
  );
};

const TypeFilterButton = ({
  documentName,
  currentDocumentName,
  documentNameFilter,
  changeToAlternative,
  changeToDefault,
}: ITypeFilterButton): JSX.Element => {
  const tooltip: string = useMemo(
    (): string =>
      documentNameFilter ? mergedDocuments[documentName].default.tooltip : "",
    [documentName, documentNameFilter],
  );

  if (!documentNameFilter) {
    return <StrictMode />;
  }

  return (
    <StrictMode>
      <Tooltip id={tooltip.split(" ").join("_")} tip={tooltip}>
        <ListItem
          onClick={changeToDefault}
          selected={documentName === currentDocumentName}
          value={""}
        >
          {mergedDocuments[documentName].default.label}
        </ListItem>
      </Tooltip>
      {mergedDocuments[documentName].alt.map(
        (alternative: IDocumentValues, index: number): JSX.Element => (
          <GButton
            alternative={alternative}
            changeToAlternative={changeToAlternative}
            currentDocumentName={currentDocumentName}
            index={index}
            key={alternative.documentName}
          />
        ),
      )}
    </StrictMode>
  );
};

export const FilterButton = ({
  shouldDisplayAll = true,
  subjectName,
  subject,
  documentName,
  currentDocumentName,
  timeFilter,
  documentNameFilter,
  changeToAlternative,
  changeToThirtyDays,
  changeToSixtyDays = undefined,
  changeToNinety,
  changeToOneHundredEighty = undefined,
  changeToAll,
  changeToDefault,
}: ITimeFilterButton & ITypeFilterButton): JSX.Element => {
  const { t } = useTranslation();

  return (
    <StrictMode>
      {documentNameFilter || timeFilter ? (
        <ShowOnHover
          hiddenElement={
            <ListItemsWrapper id={"filter-button-list-items-wrapper"}>
              <TypeFilterButton
                changeToAlternative={changeToAlternative}
                changeToDefault={changeToDefault}
                currentDocumentName={currentDocumentName}
                documentName={documentName}
                documentNameFilter={documentNameFilter}
              />
              {Boolean(documentNameFilter && timeFilter) && <Divider />}
              <TimeFilterButton
                changeToAll={changeToAll}
                changeToNinety={changeToNinety}
                changeToOneHundredEighty={changeToOneHundredEighty}
                changeToSixtyDays={changeToSixtyDays}
                changeToThirtyDays={changeToThirtyDays}
                shouldDisplayAll={shouldDisplayAll}
                subject={subject}
                subjectName={subjectName}
                timeFilter={timeFilter}
              />
            </ListItemsWrapper>
          }
          visibleElement={
            <IconButton
              icon={"filter"}
              iconSize={"xxs"}
              iconType={"fa-light"}
              id={"filter-button"}
              tooltip={t("analytics.buttonToolbar.filter.tooltip")}
              tooltipPlace={"top"}
              variant={"ghost"}
            />
          }
        />
      ) : undefined}
    </StrictMode>
  );
};
