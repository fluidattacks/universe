import { Text } from "@fluidattacks/design";
import { faHourglassHalf } from "@fortawesome/free-solid-svg-icons/faHourglassHalf";
import { faWrench } from "@fortawesome/free-solid-svg-icons/faWrench";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Fragment, StrictMode } from "react";
import { useTranslation } from "react-i18next";

import type { TGraphicState } from "../types";

const LoadingChart = ({
  iframeState,
  glyphSize,
  glyphSizeTop,
}: Readonly<{
  iframeState: TGraphicState;
  glyphSize: number;
  glyphSizeTop: number;
}>): JSX.Element => {
  const { t } = useTranslation();
  if (iframeState === "ready") {
    return <StrictMode />;
  }

  return (
    <div
      className={"absolute lh-solid tc"}
      style={{
        color: "#eee",
        fontSize: glyphSize,
        left: "50%",
        top: glyphSizeTop,
        transform: "translate(-50%)",
      }}
    >
      {iframeState === "loading" ? (
        <FontAwesomeIcon icon={faHourglassHalf} />
      ) : (
        <Fragment>
          <FontAwesomeIcon icon={faWrench} />
          <Text size={"md"}>{t("analytics.emptyChart.text")}</Text>
        </Fragment>
      )}
    </div>
  );
};

export { LoadingChart };
