import * as React from "react";

import { translate } from "utils/translations/translate";

interface IDaysLabelProps {
  readonly days: string;
  readonly isEqual?: boolean;
}

const labels: Record<string, string> = {
  "180": translate.t("analytics.limitData.oneHundredEighty.text"),
  "30": translate.t("analytics.limitData.thirtyDays.text"),
  "60": translate.t("analytics.limitData.sixtyDays.text"),
  "90": translate.t("analytics.limitData.ninetyDays.text"),
  allTime: translate.t("analytics.limitData.all.text"),
};

const DaysLabel: React.FC<IDaysLabelProps> = ({
  days,
  isEqual = false,
}: IDaysLabelProps): JSX.Element => {
  const label = labels[days];

  return <div className={"pointer"}>{isEqual ? <b>{label}</b> : label}</div>;
};

export { DaysLabel };
