import { styled } from "styled-components";

const GraphicIframe = styled.iframe.attrs({
  className: "sr-block frame bn h-100 overflow-hidden w-100",
})``;

const GraphicPanelCollapse = styled.div.attrs({
  className: "mb4 items-center" as string,
})``;

const GraphicPanelCollapseBody = styled.div.attrs({
  className: "pa2 items-center panel-cb",
})``;

const GraphicPanelCollapseHeader = styled.div.attrs({
  className: "ph3 flex items-center panel-ch",
})`
  height: 67px;
`;

export {
  GraphicIframe,
  GraphicPanelCollapse,
  GraphicPanelCollapseBody,
  GraphicPanelCollapseHeader,
};
