/* eslint-disable max-lines */
import { Container, IconButton, Link, Text } from "@fluidattacks/design";
import capitalize from "lodash/capitalize";
import includes from "lodash/includes";
import mixpanel from "mixpanel-browser";
import {
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useRef,
  useState,
} from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";
import type { ObservedSize } from "use-resize-observer";
import useResizeObserver from "use-resize-observer";

import {
  GraphicIframe,
  GraphicPanelCollapse,
  GraphicPanelCollapseBody,
} from "./styles";

import { ChartInfo } from "../chart-info";
import { LoadingChart } from "../loading-chart";
import type { IComponentSizeProps, TGraphicState } from "../types";
import { Modal } from "components/modal";
import { FilterButton } from "graphics/components/filter-button";
import {
  buildCsvUrl,
  buildUrl,
  hasIFrameError,
  isDocumentAllowed,
} from "graphics/components/graphic/utils";
import { mergedDocuments } from "graphics/ctx";
import type { IGraphicProps } from "graphics/types";
import { useAudit, useObserverAudit } from "hooks/use-audit";
import { useModal } from "hooks/use-modal";
import type { ISecureStoreConfig } from "utils/secure-store";
import { secureStoreContext } from "utils/secure-store";

const MAX_RETRIES = 5;
const DELAY_BETWEEN_RETRIES_MS = 300;

const glyphPadding = 15;
const fontSize = 16;
const minWidthToShowButtons = 320;
const bigGraphicSize: IComponentSizeProps = {
  height: 400,
  width: 1000,
};

export const Graphic = (props: Readonly<IGraphicProps>): JSX.Element => {
  const {
    bsHeight,
    className,
    shouldDisplayAll = true,
    documentName,
    documentType,
    entity,
    infoLink,
    reportMode,
    subject,
    title,
  } = props;
  const { t } = useTranslation();
  const theme = useTheme();
  const graphicModal = useModal("graphics-modal");

  // Hooks
  const fullRef: React.MutableRefObject<HTMLDivElement | null> = useRef(null);
  const headRef: React.MutableRefObject<HTMLDivElement | null> = useRef(null);
  const bodyRef: React.MutableRefObject<HTMLIFrameElement | null> =
    useRef(null);
  const modalRef: React.MutableRefObject<HTMLIFrameElement | null> =
    useRef(null);
  const modalBodyRef: React.MutableRefObject<HTMLIFrameElement | null> =
    useRef(null);

  // More hooks
  const { addAuditEvent } = useAudit();
  const object = `${capitalize(entity)}.Analytics.${
    documentName.charAt(0).toUpperCase() + documentName.slice(1)
  }`;
  const { ref } = useObserverAudit(object, subject);
  const { width: fullSizeWidth = 0 }: ObservedSize = useResizeObserver({
    ref: fullRef,
  });
  const { width: bodySizeWidth = 0, height: bodySizeHeight = 0 }: ObservedSize =
    useResizeObserver({ ref: bodyRef });
  const {
    width: modalSizeWidth = 0,
    height: modalSizeHeight = 0,
  }: ObservedSize = useResizeObserver({ ref: modalBodyRef });

  const [modalRetries, setModalRetries] = useState(0);
  const [modalIframeState, setModalIframeState] =
    useState<TGraphicState>("loading");
  const [subjectName, setSubjectName] = useState(subject);
  const [currentDocumentName, setCurrentDocumentName] = useState(documentName);
  const [currentTitle, setCurrentTitle] = useState(title);
  const [expanded, setExpanded] = useState(reportMode);
  const [fullScreen, setFullScreen] = useState(false);
  const [iframeState, setIframeState] = useState<TGraphicState>("loading");
  const [retries, setRetries] = useState(0);
  const [iFrameKey, setIFrameKey] = useState(0);
  const [modalIFrameKey, setModalIFrameKey] = useState(0);

  const secureStore: ISecureStoreConfig = useContext(secureStoreContext);

  // Yet more hooks
  const iframeSrc: string = useMemo(
    (): string =>
      secureStore.retrieveBlob(
        buildUrl(
          { ...props, documentName: currentDocumentName, subject: subjectName },
          { height: bodySizeHeight, width: bodySizeWidth },
          subjectName,
          currentDocumentName,
        ),
      ),
    [
      bodySizeWidth,
      bodySizeHeight,
      props,
      secureStore,
      subjectName,
      currentDocumentName,
    ],
  );
  const modalIframeSrc: string = useMemo(
    (): string =>
      secureStore.retrieveBlob(
        buildUrl(
          { ...props, documentName: currentDocumentName, subject: subjectName },
          { height: modalSizeHeight, width: modalSizeWidth },
          subjectName,
          currentDocumentName,
        ),
      ),
    [
      modalSizeWidth,
      modalSizeHeight,
      props,
      secureStore,
      subjectName,
      currentDocumentName,
    ],
  );

  const panelOnMouseEnter = useCallback((): void => {
    setExpanded(true);
  }, []);
  const panelOnMouseLeave = useCallback((): void => {
    setExpanded(reportMode);
  }, [reportMode]);
  const frameOnLoad = useCallback((): void => {
    setIframeState("ready");
    secureStore.storeIframeContent(bodyRef);
  }, [secureStore]);
  const frameOnFullScreen = useCallback((): void => {
    setFullScreen(true);
  }, []);
  const frameOnFullScreenExit = useCallback((): void => {
    setFullScreen(false);
  }, []);
  const frameOnRefresh = useCallback((): void => {
    if (bodyRef.current?.contentWindow !== null) {
      setRetries(0);
      setIframeState("loading");
      setIFrameKey((value: number): number => {
        if (value >= DELAY_BETWEEN_RETRIES_MS) {
          return 0;
        }

        return value + 1;
      });
    }
  }, []);
  const modalFrameOnLoad = useCallback((): void => {
    setModalIframeState("ready");
    secureStore.storeIframeContent(modalBodyRef);
  }, [secureStore]);
  const modalFrameOnRefresh = useCallback((): void => {
    if (modalBodyRef.current?.contentWindow !== null) {
      setModalIframeState("loading");
      setModalRetries(0);
      setModalIFrameKey((value: number): number => {
        if (value >= DELAY_BETWEEN_RETRIES_MS) {
          return 0;
        }

        return value + 1;
      });
    }
  }, []);
  function buildFileName(size: IComponentSizeProps): string {
    return `${currentTitle}-${subject}-${size.width}x${size.height}.html`;
  }
  const csvFileName: string = useMemo(
    (): string => `${subject}-${currentTitle}.csv`,
    [currentTitle, subject],
  );
  const shouldDisplayExtraButtons: boolean = useMemo((): boolean => {
    return expanded && !reportMode && fullSizeWidth > minWidthToShowButtons;
  }, [expanded, reportMode, fullSizeWidth]);

  function changeTothirtyDays(): void {
    setSubjectName(`${subject}_30`);
    frameOnRefresh();
  }

  const changeToNinety = useCallback((): void => {
    setSubjectName(`${subject}_90`);
    frameOnRefresh();
  }, [frameOnRefresh, subject]);

  function changeToSixtyDays(): void {
    setSubjectName(`${subject}_60`);
    frameOnRefresh();
  }
  function changeToOneHundredEighty(): void {
    setSubjectName(`${subject}_180`);
    frameOnRefresh();
  }

  const changeToAll = useCallback((): void => {
    setSubjectName(subject);
    frameOnRefresh();
  }, [frameOnRefresh, subject]);

  const changeToDefault = useCallback((): void => {
    setCurrentDocumentName(documentName);
    setCurrentTitle(title);
    frameOnRefresh();
  }, [documentName, frameOnRefresh, title]);

  const changeToAlternative = useCallback(
    (index: number): void => {
      if (includes(Object.keys(mergedDocuments), documentName)) {
        setCurrentDocumentName(
          mergedDocuments[documentName].alt[index].documentName,
        );
        setCurrentTitle(mergedDocuments[documentName].alt[index].title);
        frameOnRefresh();
      }
    },
    [documentName, frameOnRefresh],
  );

  const isDocumentMerged = useCallback(
    (name: string, type: string): boolean => {
      return (
        includes(Object.keys(mergedDocuments), name) &&
        mergedDocuments[name].documentType === type
      );
    },
    [],
  );

  function retryFrame(): void {
    if (bodyRef.current?.contentWindow !== null) {
      setIframeState("loading");
      setIFrameKey((value: number): number => {
        if (value >= DELAY_BETWEEN_RETRIES_MS) {
          return 0;
        }

        return value + 1;
      });
    }
  }

  function retryModalIFrame(): void {
    if (modalBodyRef.current?.contentWindow !== null) {
      setModalIframeState("loading");
      setModalIFrameKey((value: number): number => {
        if (value >= DELAY_BETWEEN_RETRIES_MS) {
          return 0;
        }

        return value + 1;
      });
    }
  }

  if (iframeState === "ready" && hasIFrameError(bodyRef)) {
    setIframeState("error");
  }

  if (modalIframeState === "ready" && hasIFrameError(modalBodyRef)) {
    setModalIframeState("error");
  }

  const glyphSize = Math.min(bodySizeHeight, bodySizeWidth) / 2;
  const glyphSizeTop = glyphPadding + glyphSize / 2 - fontSize;

  const track = useCallback((): void => {
    mixpanel.track("DownloadGraphic", { currentDocumentName, entity });
    addAuditEvent("Graphic.Download", [currentDocumentName, entity].join("/"));
  }, [addAuditEvent, currentDocumentName, entity]);

  const trackCsv = useCallback((): void => {
    mixpanel.track("DownloadCsvGraphic", { currentDocumentName, entity });
    addAuditEvent(
      "CsvGraphic.Download",
      [currentDocumentName, entity].join("/"),
    );
  }, [addAuditEvent, currentDocumentName, entity]);

  useEffect((): void => {
    if (iframeState === "error" && retries < MAX_RETRIES) {
      setTimeout((): void => {
        secureStore.removeBlob(
          buildUrl(
            {
              ...props,
              documentName: currentDocumentName,
              subject: subjectName,
            },
            { height: bodySizeHeight, width: bodySizeWidth },
            subjectName,
            currentDocumentName,
          ),
        );
        setRetries((value: number): number => value + 1);
        retryFrame();
      }, DELAY_BETWEEN_RETRIES_MS);
    }
    if (modalIframeState === "error" && modalRetries < MAX_RETRIES) {
      setTimeout((): void => {
        secureStore.removeBlob(
          buildUrl(
            {
              ...props,
              documentName: currentDocumentName,
              subject: subjectName,
            },
            { height: modalSizeHeight, width: modalSizeWidth },
            subjectName,
            currentDocumentName,
          ),
        );
        setModalRetries((value: number): number => value + 1);
        retryModalIFrame();
      }, DELAY_BETWEEN_RETRIES_MS);
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    addAuditEvent,
    documentName,
    entity,
    subject,
    iframeState,
    modalIframeState,
  ]);

  const sharedButtons = (
    <React.Fragment>
      <FilterButton
        changeToAll={changeToAll}
        changeToAlternative={changeToAlternative}
        changeToDefault={changeToDefault}
        changeToNinety={changeToNinety}
        changeToOneHundredEighty={
          shouldDisplayAll ? undefined : changeToOneHundredEighty
        }
        changeToSixtyDays={shouldDisplayAll ? undefined : changeToSixtyDays}
        changeToThirtyDays={shouldDisplayAll ? changeTothirtyDays : changeToAll}
        currentDocumentName={currentDocumentName}
        documentName={documentName}
        documentNameFilter={isDocumentMerged(documentName, documentType)}
        shouldDisplayAll={shouldDisplayAll}
        subject={subject}
        subjectName={subjectName}
        timeFilter={isDocumentAllowed(documentName, documentType)}
      />
      <ChartInfo
        currentDocumentName={currentDocumentName}
        documentName={documentName}
        documentType={documentType}
        infoLink={infoLink}
        isDocumentMerged={isDocumentMerged}
      />
      {documentType === "textBox" ? undefined : (
        <Link
          download={csvFileName}
          href={buildCsvUrl(
            {
              ...props,
              documentName: currentDocumentName,
              subject: subjectName,
            },
            subjectName,
            currentDocumentName,
          )}
          iconPosition={"hidden"}
          onClick={trackCsv}
        >
          <IconButton
            icon={"file-csv"}
            iconSize={"xxs"}
            iconType={"fa-light"}
            id={"csv-button"}
            tooltip={t("analytics.buttonToolbar.fileCsv.tooltip")}
            tooltipPlace={"top"}
            variant={"ghost"}
          />
        </Link>
      )}
    </React.Fragment>
  );

  return (
    <React.Fragment>
      <Modal
        modalRef={{
          ...graphicModal,
          close: frameOnFullScreenExit,
          isOpen: fullScreen,
        }}
        size={"lg"}
        title={
          <div className={"flex justify-between w-100"}>
            <div>{currentTitle}</div>
            <Container alignItems={"center"} display={"flex"} gap={0.25}>
              {sharedButtons}
              <Link
                download={buildFileName({
                  height: modalSizeHeight,
                  width: modalSizeWidth,
                })}
                href={buildUrl(
                  {
                    ...props,
                    documentName: currentDocumentName,
                    subject: subjectName,
                  },
                  { height: modalSizeHeight, width: modalSizeWidth },
                  subjectName,
                  currentDocumentName,
                )}
                iconPosition={"hidden"}
                onClick={track}
              >
                <IconButton
                  icon={"download"}
                  iconSize={"xxs"}
                  iconType={"fa-light"}
                  id={"download-button"}
                  tooltip={t("analytics.buttonToolbar.download.tooltip")}
                  tooltipPlace={"top"}
                  variant={"ghost"}
                />
              </Link>
              <IconButton
                icon={"refresh"}
                iconSize={"xxs"}
                iconType={"fa-light"}
                id={"refresh-button"}
                onClick={modalFrameOnRefresh}
                tooltip={t("analytics.buttonToolbar.refresh.tooltip")}
                tooltipPlace={"top"}
                variant={"ghost"}
              />
            </Container>
          </div>
        }
      >
        <div
          className={"relative"}
          ref={modalRef}
          style={{ height: bigGraphicSize.height }}
        >
          <GraphicIframe
            frameBorder={"no"}
            key={modalIFrameKey}
            onLoad={modalFrameOnLoad}
            ref={modalBodyRef}
            sandbox={
              "allow-modals allow-scripts allow-popups allow-popups-to-escape-sandbox"
            }
            scrolling={"no"}
            src={modalIframeSrc}
            style={{
              opacity: modalIframeState === "ready" ? 1 : 0,
            }}
            title={currentTitle}
          />
          <LoadingChart
            glyphSize={glyphSize}
            glyphSizeTop={glyphSizeTop}
            iframeState={modalIframeState}
          />
        </div>
      </Modal>
      <div ref={fullRef}>
        <GraphicPanelCollapse
          className={className}
          onMouseEnter={panelOnMouseEnter}
          onMouseLeave={panelOnMouseLeave}
        >
          <Container
            alignItems={"center"}
            bgColor={theme.palette.white}
            borderBottom={`1px solid ${theme.palette.gray[300]}`}
            display={"flex"}
            height={"67px"}
            justify={"space-between"}
            pl={1.5}
            pr={1.5}
            ref={headRef}
          >
            <Text display={"inline"} size={"md"}>
              {currentTitle}
            </Text>
            <Container alignItems={"center"} display={"flex"} gap={0.25}>
              {shouldDisplayExtraButtons ? (
                <React.Fragment>
                  {sharedButtons}
                  {documentType === "textBox" ? undefined : (
                    <React.Fragment>
                      <Link
                        download={buildFileName(bigGraphicSize)}
                        href={buildUrl(
                          {
                            ...props,
                            documentName: currentDocumentName,
                            subject: subjectName,
                          },
                          bigGraphicSize,
                          subjectName,
                          currentDocumentName,
                        )}
                        iconPosition={"hidden"}
                        onClick={track}
                      >
                        <IconButton
                          icon={"download"}
                          iconSize={"xxs"}
                          iconType={"fa-light"}
                          id={"download-button"}
                          tooltip={t(
                            "analytics.buttonToolbar.download.tooltip",
                          )}
                          tooltipPlace={"top"}
                          variant={"ghost"}
                        />
                      </Link>
                      <IconButton
                        icon={"refresh"}
                        iconSize={"xxs"}
                        iconType={"fa-light"}
                        id={"refresh-button"}
                        onClick={frameOnRefresh}
                        tooltip={t("analytics.buttonToolbar.refresh.tooltip")}
                        tooltipPlace={"top"}
                        variant={"ghost"}
                      />
                      <IconButton
                        icon={"expand-arrows-alt"}
                        iconSize={"xxs"}
                        iconType={"fa-light"}
                        id={"expand-button"}
                        onClick={frameOnFullScreen}
                        tooltip={t("analytics.buttonToolbar.expand.tooltip")}
                        tooltipPlace={"top"}
                        variant={"ghost"}
                      />
                    </React.Fragment>
                  )}
                </React.Fragment>
              ) : (
                <span style={{ height: "28px", width: "36px" }} />
              )}
            </Container>
          </Container>
          <GraphicPanelCollapseBody>
            <div className={"relative"} ref={ref} style={{ height: bsHeight }}>
              <GraphicIframe
                frameBorder={"no"}
                key={iFrameKey}
                loading={reportMode ? "eager" : "lazy"}
                onLoad={frameOnLoad}
                ref={bodyRef}
                sandbox={
                  "allow-scripts allow-popups allow-popups-to-escape-sandbox"
                }
                scrolling={"no"}
                src={iframeSrc}
                style={{
                  /*
                   * The element must be rendered for C3 legends to work,
                   * so lets just hide it from the user
                   */
                  opacity: iframeState === "ready" ? 1 : 0,
                }}
                title={currentTitle}
              />
              <LoadingChart
                glyphSize={glyphSize}
                glyphSizeTop={glyphSizeTop}
                iframeState={iframeState}
              />
            </div>
          </GraphicPanelCollapseBody>
        </GraphicPanelCollapse>
      </div>
    </React.Fragment>
  );
};
