/* eslint-disable functional/immutable-data, jest/no-hooks, jest/require-top-level-describe */
import { configure } from "@testing-library/react";
import failOnConsole from "jest-fail-on-console";
import "cross-fetch/polyfill";
import "@testing-library/jest-dom";
import mixpanel from "mixpanel-browser";

import { server } from "mocks/server";

// https://github.com/facebook/react/pull/22114
const ignoreErrors = [
  /React does not recognize the `.+` prop on a DOM element/u,
  /React Router Future Flag Warning:/u,
];

failOnConsole({
  silenceMessage: (errorMessage): boolean =>
    ignoreErrors.find((ignoreError): boolean =>
      ignoreError.test(errorMessage),
    ) !== undefined,
});

// Disable mixpanel
mixpanel.init("123");
mixpanel.disable();

/*
 * Given that tests run in a CI machine with limited resources
 * we need to increase the timeout for reliable results.
 */
const timeout = 60000;
const TIMEOUT_ADJUSTMENT = 1000;
jest.setTimeout(timeout);
configure({ asyncUtilTimeout: timeout - TIMEOUT_ADJUSTMENT });

// https://github.com/jsdom/jsdom/issues/1695
Object.assign(Element.prototype, { scrollIntoView: jest.fn() });

// Mock i18n
jest.mock(
  "react-i18next",
  (): Record<string, unknown> => ({
    ...jest.requireActual("react-i18next"),
    useTranslation: jest
      .fn()
      .mockReturnValue({ t: jest.fn((key: string): string => key) }),
  }),
);

// Mock bugsnag
jest.mock("@bugsnag/js");

// Mock idle-timer
jest.mock(
  "react-idle-timer",
  (): { useIdleTimer: () => Record<string, jest.Mock> } => ({
    useIdleTimer: (): Record<string, jest.Mock> => ({ active: jest.fn() }),
  }),
);

// Mock intersectionObserver
const mockIntersectionObserver = jest.fn();
mockIntersectionObserver.mockReturnValue({
  disconnect: (): void => {
    // No return value needed for this function
  },
  observe: (): void => {
    // No return value needed for this function
  },
  unobserve: (): void => {
    // No return value needed for this function
  },
});
// eslint-disable-next-line functional/immutable-data
window.IntersectionObserver = mockIntersectionObserver;

beforeAll((): void => {
  const ignoredPathnames = ["/engage/", "/test-file-stub"];

  server.listen({
    onUnhandledRequest(request, print): void {
      if (
        ignoredPathnames.some((pathname): boolean =>
          new URL(request.url).pathname.startsWith(pathname),
        )
      ) {
        return;
      }
      print.warning();
    },
  });
});

afterEach((): void => {
  server.restoreHandlers();
  server.resetHandlers();
  jest.clearAllMocks();
  sessionStorage.clear();
  localStorage.clear();
});

afterAll((): void => {
  server.close();
});
