import eslintPluginPrettierRecommended from "eslint-plugin-prettier/recommended";
import functional from "eslint-plugin-functional";
import globals from "globals";
import graphqlPlugin from "@graphql-eslint/eslint-plugin";
import importPlugin from "eslint-plugin-import";
import jestFormatting from "eslint-plugin-jest-formatting";
import jestPlugin from "eslint-plugin-jest";
import js from "@eslint/js";
import jsxA11y from "eslint-plugin-jsx-a11y";
import projectStructure from "eslint-plugin-project-structure";
import react from "eslint-plugin-react";
import reactHooks from "eslint-plugin-react-hooks";
import testingLibrary from "eslint-plugin-testing-library";
import typescript from "@typescript-eslint/eslint-plugin";
import typescriptParser from "@typescript-eslint/parser";
import { folderStructureConfig } from "./folderStructure.mjs";

export default [
  {
    files: ["src/**/*.{ts,tsx}", "src/**/*.{.test.ts,.test.tsx}"],
    plugins: {
      "@typescript-eslint": typescript,
      functional,
      import: importPlugin,
      jest: jestPlugin,
      jestFormatting,
      "jsx-a11y": jsxA11y,
      "project-structure": projectStructure,
      react,
      "testing-library": testingLibrary,
      "react-hooks": reactHooks,
    },
    languageOptions: {
      ...jsxA11y.flatConfigs.recommended.languageOptions,
      ecmaVersion: "latest",
      parser: typescriptParser,
      parserOptions: {
        ecmaFeatures: {
          jsx: true,
        },
        project: "./tsconfig.json",
      },
      globals: {
        ...globals.browser,
        ...globals.serviceworker,
      },
    },
    settings: {
      "import/resolver": {
        typescript: {
          alwaysTryTypes: true,
          project: "./tsconfig.json",
        },
      },
      "project-structure/config-path": "./structure.json",
      react: {
        version: "detect",
      },
    },
    rules: {
      ...js.configs.all.rules,
      ...typescript.configs.all.rules,
      ...functional.configs.externalVanillaRecommended.rules,
      ...functional.configs.externalTypeScriptRecommended.rules,
      ...functional.configs.all.rules,
      ...functional.configs.stylistic.rules,
      ...importPlugin.flatConfigs.recommended.rules,
      ...importPlugin.flatConfigs.typescript.rules,
      ...jestPlugin.configs["flat/all"].rules,
      ...jestFormatting.configs.rules,
      ...testingLibrary.configs.react.rules,
      ...jsxA11y.configs.strict.rules,
      ...react.configs.flat.recommended.rules,
      ...react.configs.flat["jsx-runtime"].rules,
      "project-structure/folder-structure": ["error", folderStructureConfig],
      "capitalized-comments": [
        "error",
        "always",
        { ignoreConsecutiveComments: true },
      ],
      eqeqeq: ["error", "smart"],
      "func-style": ["error", "declaration", { allowArrowFunctions: true }],
      /*
       * Given exceptions for lodash, jquery and translation wildcard "t"
       */
      "id-length": [
        "error",
        { exceptions: ["_", "$", "t"], properties: "never" },
      ],
      "line-comment-position": ["error", "above"],
      "max-lines": ["error", { max: 500 }],
      "max-lines-per-function": "off",
      "max-params": ["error", { max: 7 }],
      "max-statements": ["error", { max: 70 }],
      "multiline-comment-style": ["error", "starred-block"],
      /*
       * Replaced by @typescript-eslint/no-duplicate-imports to avoid
       * conflicts with type imports
       * https://github.com/typescript-eslint/typescript-eslint/issues/2315
       */
      "no-duplicate-imports": "off",
      /*
       * Rules regarding the ternary op are disabled, as it is common its usage in React
       */
      "no-ternary": "off",
      /*
       * This rule is deactivated as the codebase uses extensively "undefined"
       * It was established by a previous TSLint rule:
       * https://palantir.github.io/tslint/rules/no-null-keyword/
       * For consistency, we will prefer "undefined" over "null"
       */
      "no-undefined": "off",
      /*
       * Disabled while globals and parsers get properly updated and configured
       * https://eslint.org/docs/rules/no-undef
       *
       */
      "no-undef": "off",
      /*
       * Exception to the next rule: The variable "__typename" is required by Apollo Library
       */
      "no-underscore-dangle": ["error", { allow: ["__typename"] }],
      "no-void": ["error", { allowAsStatement: true }],
      "one-var": ["error", "never"],
      "padding-line-between-statements": [
        "error",
        { blankLine: "always", prev: "*", next: "return" },
      ],
      "sort-imports": ["error", { ignoreDeclarationSort: true }],
      "@typescript-eslint/ban-ts-comment": "error",
      /*
       * Useful when migrating from TSLint to ESLint
       */
      "@typescript-eslint/ban-tslint-comment": "error",
      "@typescript-eslint/explicit-function-return-type": [
        "error",
        {
          allowConciseArrowFunctionExpressionsStartingWithVoid: false,
          allowTypedFunctionExpressions: false,
          allowHigherOrderFunctions: false,
        },
      ],
      "@typescript-eslint/naming-convention": [
        "error",
        {
          selector: "variable",
          format: ["camelCase", "UPPER_CASE"],
        },
        {
          selector: "variable",
          format: ["camelCase", "PascalCase"],
          types: ["function"],
        },
        {
          selector: "interface",
          format: ["PascalCase"],
          prefix: ["I"],
        },
        {
          selector: "typeAlias",
          format: ["PascalCase"],
          prefix: ["T"],
        },
      ],
      "@typescript-eslint/max-params": ["error", { max: 7 }],
      /*
       * Disabled until the migration to newer versions of eslint is done
       */
      "@typescript-eslint/no-magic-numbers": [
        "error",
        {
          ignore: [-1, 0, 1, 2, 3, 4, 100],
          /*
           * We need type indexes to retrieve specific objects in interface lists
           */
          ignoreTypeIndexes: true,
          ignoreArrayIndexes: true,
          ignoreNumericLiteralTypes: true,
        },
      ],
      /*
       * Disabled until the migration to newer versions of eslint is done
       */
      "@typescript-eslint/no-misused-promises": [
        "error",
        {
          checksVoidReturn: {
            attributes: false,
          },
        },
      ],
      /*
       * Using non-null assertions cancels the benefits of the strict null-checking mode
       */
      "@typescript-eslint/no-non-null-assertion": "error",
      "@typescript-eslint/consistent-type-definitions": "error",
      /*
       * This rule is incompatible with "@typescript-eslint/no-non-null-assertion" and it
       * is not a problem to have verbose type casts
       */
      "@typescript-eslint/non-nullable-type-assertion-style": "off",
      /*
       * Since we are working with external libraries that use any in there type
       * definitions. We need to disable the NEXT couple of rules, until they
       * update to the new unknown type
       */
      "@typescript-eslint/no-unsafe-argument": "off",
      "@typescript-eslint/no-unsafe-assignment": "off",
      "@typescript-eslint/no-unused-vars": [
        "error",
        { argsIgnorePattern: "^_" },
      ],
      /*
       * This rule must be disabled as it can report incorrect errors in conflict
       * with with rule eslint/object-curly-spacing. Official plugin doc
       */
      "@typescript-eslint/object-curly-spacing": "off",
      /*
       * This is a good rule in theory, but in practice we found:
       * 1. We tried to use a DeepReadonly utility type to solve types nesting
       * problems; but it conflicts with wrapper components, since you cannot
       * modify the types of the component being wrapped, if it is from a third
       * party library.
       * 2. We are using the functional eslint plugin that is already in charge of
       * avoiding side effects as much as possible
       */
      "@typescript-eslint/prefer-readonly-parameter-types": "off",
      /*
       * In the functional react world, you likely will never have a function that
       * actually cares about the this context. Refer to:
       * https://github.com/typescript-eslint/typescript-eslint/issues/2245#issuecomment-648712540
       */
      "@typescript-eslint/unbound-method": "off",
      /*
       * Since we need side effects to make our program meaningful, for instance:
       * callbacks, DOM mutations, Node processes, etc. We need to disable the
       * next TWO rules, the result is: from now on it should be inferred that
       * any method call without an assignment, produce some kind of side effect
       */
      "functional/no-expression-statements": "off",
      "functional/no-return-void": "off",
      "functional/prefer-immutable-types": "off",
      "functional/type-declaration-immutability": "off",
      "functional/no-conditional-statements": "off",
      "functional/no-mixed-types": "off",
      "functional/no-try-statements": "off",
      "functional/functional-parameters": [
        "error",
        {
          enforceParameterCount: false,
        },
      ],
      /*
       * These rules are disabled as suggested by typescript-eslint
       * for performance.
       * https://typescript-eslint.io/troubleshooting/typed-linting/performance
       */
      "import/default": "off",
      "import/no-named-as-default-member": "off",
      "import/no-namespace": "off",
      /* */

      "import/export": "error",
      "import/exports-last": "error",
      "import/first": "error",
      "import/group-exports": "error",
      "import/newline-after-import": "error",
      "import/no-absolute-path": ["error", { commonjs: false }],
      "import/no-cycle": ["error", { ignoreExternal: true }],
      "import/no-default-export": "error",
      "import/no-deprecated": "error",
      /*
       * This rule conflicts with the rule eslint/consistent-type-imports when
       * trying to import types and default modules in the same import
       * https://github.com/benmosher/eslint-plugin-import/pull/334
       */
      "import/no-duplicates": "off",
      "import/no-extraneous-dependencies": [
        "error",
        { optionalDependencies: false },
      ],
      "import/no-named-as-default": "error",
      "import/no-named-default": "error",
      "import/no-self-import": "error",
      "import/no-useless-path-segments": ["error", { noUselessIndex: true }],
      "import/no-webpack-loader-syntax": "error",
      "import/order": [
        "error",
        {
          alphabetize: { order: "asc", caseInsensitive: true },
          groups: ["builtin", "external", "sibling"],
          "newlines-between": "always",
        },
      ],
      /*
       * This rule has been deprecated in favor of jest/prefer-lowercase-title
       * Said rule should be enabled as soon as all eslint versions are upgraded
       */
      "jest/lowercase-name": "off",
      /**

       * This rule can help to ensure that the Jest globals
       * are imported explicitly and facilitate a migration to @jest/global.
       * Will be kept as disabled until all eslint versions are upgraded and
       * we consider whether this gives us any value.
       */
      "jest/prefer-importing-jest-globals": "off",
      /*
       * This rule is turned off until all eslint versions are upgraded
       */
      "jest/no-conditional-in-test": "off",
      "jest/no-restricted-matchers": [
        "error",
        {
          toBeTruthy: "Avoid `toBeTruthy`, use `toBe(true)` instead.",
          toBeFalsy: "Avoid `toBeFalsy`, use `toBe(false)` instead.",
        },
      ],
      /*
       * This rule has been deprecated in favor of jest/no-restricted-matchers
       */
      "jest/no-truthy-falsy": "off",
      /*
       * This rule is turned off as we don't want to go back to setup/cleanup hooks
       */
      "jest/require-hook": "off",
      /*
       * In the functional react world, you likely will never have a function that
       * actually cares about the this context. Refer to:
       * https://github.com/typescript-eslint/typescript-eslint/issues/2245#issuecomment-648712540
       */
      "jest/unbound-method": "off",
      /*
       * This rule has been deprecated in favor of
       * jsx-a11y/label-has-associated-control
       */
      "jsx-a11y/label-has-for": "off",
      /*
       * This rule has been deprecated
       * because browsers fixed it.
       * https://github.com/jsx-eslint/eslint-plugin-jsx-a11y/blob/master/docs/rules/no-onchange.md
       */
      "jsx-a11y/no-onchange": "off",
      /*
       * This rule requires the type attribute to be a string literal, because of
       * that it has a conflict with react/jsx-curly-brace-presence
       */
      "react/button-has-type": "off",
      "react/jsx-boolean-value": ["error", "always"],
      "react/jsx-curly-brace-presence": ["error", "always"],
      "react/jsx-filename-extension": [
        "error",
        {
          extensions: [".tsx"],
        },
      ],
      "react/jsx-fragments": ["error", "element"],
      "react/jsx-max-depth": "off",
      "react/jsx-no-bind": [
        "error",
        {
          allowFunctions: true,
        },
      ],
      /*
       * Turned off until an easy way to exclude context provider values in
       * tests is found
       */
      "react/jsx-no-constructed-context-values": "off",
      /*
       * "allowedStrings" is not working once the @typescript-eslint@4.15.2
       * plugin is updated. Affected string changed by its Unicode equivalent
       */
      "react/jsx-no-literals": ["error"],
      "react/jsx-sort-props": ["error"],
      "react/function-component-definition": [
        "error",
        {
          namedComponents: "arrow-function",
        },
      ],
      "react-hooks/exhaustive-deps": "error",
      "react-hooks/rules-of-hooks": "error",
      "jest/max-expects": "off",
      "react/no-deprecated": "off",
      "react/no-object-type-as-default-prop": "off",
      "react/require-default-props": "off",
      "react/jsx-props-no-spreading": [
        "error",
        {
          custom: "ignore",
        },
      ],
      "react/jsx-handler-names": [
        "error",
        { eventHandlerPrefix: "(on|handle)" },
      ],
      /*
       * The following rules are disabled only for facilitating eslint flat
       * config support migration and will reviewed/enabled soon.
       */
      complexity: ["error", { max: 37 }],
      "react/display-name": "off",
      "@typescript-eslint/no-unsafe-type-assertion": "off",
      "@typescript-eslint/no-deprecated": "off",
      "@typescript-eslint/no-base-to-string": "off",
      "@typescript-eslint/no-misused-spread": "off",
    },
  },
  {
    files: ["src/**/*.test.tsx", "src/**/*.test.ts"],
    plugins: { jest: jestPlugin, functional },
    languageOptions: {
      globals: jestPlugin.environments.globals.globals,
    },
    rules: {
      "max-lines": "off",
      "max-statements": "off",
      "testing-library/no-container": "off",
      "testing-library/no-node-access": "off",
      "testing-library/prefer-presence-queries": "off",
      "functional/no-try-statements": "error",
    },
  },
  {
    files: ["src/components/**/*"],
    plugins: { functional },
    rules: {
      "functional/no-try-statements": "error",
    },
  },
  {
    files: ["src/**/queries.ts"],
    processor: graphqlPlugin.processor,
  },
  {
    files: ["**/*.graphql"],
    plugins: { "@graphql-eslint": graphqlPlugin },
    rules: {
      ...graphqlPlugin.configs["operations-all"].rules,
      "@graphql-eslint/naming-convention": [
        "error",
        {
          allowLeadingUnderscore: true,
          types: "PascalCase",
        },
      ],
      "@graphql-eslint/no-one-place-fragments": "off",
      "@graphql-eslint/require-id-when-available": "off",
      "@graphql-eslint/unique-operation-name": "off",
      "@graphql-eslint/no-deprecated": "off",
      "@graphql-eslint/require-import-fragment": "off",
    },
    languageOptions: {
      parser: graphqlPlugin.parser,
    },
  },
  {
    files: ["src/typings/*.d.ts"],
    rules: {
      "@typescript-eslint/naming-convention": "off",
      "@typescript-eslint/no-empty-interface": "off",
      "@typescript-eslint/no-redundant-type-constituents": "off",
      "@typescript-eslint/no-unused-vars": "off",
      "import/exports-last": "off",
      "import/group-exports": "off",
    },
  },
  {
    files: ["src/**/index.test.tsx"],
    rules: {
      "@typescript-eslint/no-unsafe-call": "off",
      "@typescript-eslint/no-magic-numbers": "off",
    },
  },
  {
    files: ["src/**/styles.ts", "src/components/@core/index.d.ts"],
    rules: {
      "@typescript-eslint/no-magic-numbers": "off",
    },
  },
  {
    ignores: ["src/gql/*.ts"],
  },
  eslintPluginPrettierRecommended,
];
