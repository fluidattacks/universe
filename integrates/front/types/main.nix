{ inputs, makeScript, ... }:
makeScript {
  entrypoint = ./entrypoint.sh;
  name = "integrates-front-types";
  searchPaths.bin = [ inputs.nixpkgs.bash inputs.nixpkgs.nodejs_20 ];
}
