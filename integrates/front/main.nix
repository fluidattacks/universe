{ inputs, makeScript, outputs, projectPath, ... }:
makeScript {
  replace = {
    __argCertsDevelopment__ = outputs."/integrates/certs/dev";
    __argPackagesPath__ = projectPath "/integrates/front";
  };
  entrypoint = ./entrypoint.sh;
  name = "integrates-front";
  searchPaths = {
    bin = [ inputs.nixpkgs.bash inputs.nixpkgs.nodejs_20 ]
      ++ inputs.nixpkgs.lib.optionals inputs.nixpkgs.stdenv.isDarwin [
        (inputs.makeImpureCmd {
          cmd = "open";
          path = "/usr/bin/open";
        })
      ];
  };
}
