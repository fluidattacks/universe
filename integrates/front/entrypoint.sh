# shellcheck shell=bash

function main {
  local action="${1:-start}"
  local packages_path="integrates/front"
  export FI_WEBPACK_TLS_CERT=__argCertsDevelopment__/cert.crt
  export FI_WEBPACK_TLS_KEY=__argCertsDevelopment__/cert.key

  if running_in_ci_cd_provider; then
    packages_path=__argPackagesPath__
  fi

  : && pushd "${packages_path}" \
    && npm ci \
    && export PATH="${PWD}/node_modules/.bin:${PATH}" \
    && export NODE_PATH="${PWD}/node_modules:${NODE_PATH}" \
    && popd \
    && pushd integrates/front \
    && npm run "${action}" \
    && popd \
    || return 1
}

main "${@}"
