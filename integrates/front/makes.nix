{
  imports = [
    ./coverage/makes.nix
    ./deploy/makes.nix
    ./pipeline/makes.nix
    ./test/makes.nix
  ];
}
