{ inputs, makeScript, fetchArchive, outputs, ... }:
makeScript {
  entrypoint = "stop_code_server";
  name = "retrieves-code-server-stop";
  searchPaths = { source = [ outputs."/integrates/retrieves/code-server" ]; };
}
