{ inputs, makeScript, fetchArchive, outputs, ... }:
makeScript {
  entrypoint = "start_code_server $@";
  name = "retrieves-code-server-start";
  searchPaths = { source = [ outputs."/integrates/retrieves/code-server" ]; };
}
