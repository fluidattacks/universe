{ inputs, isLinux, fetchArchive, makeTemplate, outputs, ... }:
makeTemplate {
  name = "retrieves-code-server";
  replace = {
    __argCodeServer__ = outputs."/integrates/retrieves/code-server/pkg";
  };
  searchPaths = {
    bin = [ inputs.nixpkgs.git inputs.nixpkgs.procps inputs.nixpkgs.nodejs_20 ]
      ++ inputs.nixpkgs.lib.optionals isLinux [ inputs.nixpkgs.psmisc ];
    source = [ outputs."/common/utils/aws" outputs."/common/utils/sops" ];
  };
  template = ./template.sh;
}
