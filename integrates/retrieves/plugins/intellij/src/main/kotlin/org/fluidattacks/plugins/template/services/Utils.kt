package org.fluidattacks.plugins.template.services

import com.apollographql.apollo3.ApolloClient
import com.apollographql.apollo3.api.ApolloResponse
import com.apollographql.apollo3.api.Query
import com.intellij.openapi.diagnostic.Logger

object Utils {
    private const val URL = "https://app.fluidattacks.com/api"
    private val logger: Logger = Logger.getInstance("Utils")

    private fun getApolloClient(token: String): ApolloClient =
        ApolloClient
            .Builder()
            .serverUrl(URL)
            .addHttpHeader("Authorization", "Bearer $token")
            .build()

    suspend fun <D : Query.Data, T> executeQuery(
        token: String,
        query: Query<D>,
        handleResponse: (ApolloResponse<D>) -> T,
    ): T? {
        val apolloClient = getApolloClient(token)

        return try {
            val response: ApolloResponse<D> = apolloClient.query(query).execute()
            handleResponse(response)
        } catch (e: Exception) {
            logger.error("Error executing query", e)
            null
        }
    }

    fun getGitRemoteUrl(path: String): String? =
        try {
            val process =
                ProcessBuilder("git", "-C", path, "remote", "get-url", "origin")
                    .redirectErrorStream(true)
                    .start()

            val output =
                process.inputStream
                    .bufferedReader()
                    .readText()
                    .trim()
            if (process.waitFor() == 0) output else null
        } catch (e: Exception) {
            logger.error("Error retrieving Git remote URL", e)
            null
        }
}
