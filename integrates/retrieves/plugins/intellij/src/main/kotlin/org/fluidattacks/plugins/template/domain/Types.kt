package org.fluidattacks.plugins.template.domain

interface Root {
    val id: String
    val nickname: String
}

data class GitRoot(
    override val id: String,
    override val nickname: String,
    val state: String,
    val url: String,
    val groupName: String?,
    val userRole: String?,
    val vulnerabilitiesConnection: VulnerabilitiesConnection?,
) : Root

data class VulnerabilitiesConnection(
    val edges: List<VulnerabilityEdge>?,
    val pageInfo: PageInfo,
)

data class VulnerabilityEdge(
    val node: Vulnerability,
)

data class Vulnerability(
    val id: String,
    val specific: String,
    val where: String,
    val rootNickname: String?,
    val finding: Finding,
    val reportDate: String?,
    val treatmentStatus: String?,
    val verification: String?,
)

data class Finding(
    val id: String,
    val title: String,
    val description: String?,
    val severityScoreV4: Double?,
)

data class PageInfo(
    val endCursor: String?,
    val hasNextPage: Boolean?,
)

data class Group(
    val name: String,
    val userRole: String,
    val subscription: String?,
    val roots: List<GitRoot>,
)

data class VulnerabilityQueryResult(
    val vulnerabilities: List<Vulnerability>,
    val pageInfo: PageInfo,
)
