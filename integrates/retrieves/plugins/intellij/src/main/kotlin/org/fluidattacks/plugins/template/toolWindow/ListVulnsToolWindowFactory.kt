package org.fluidattacks.plugins.template.toolWindow

import com.intellij.openapi.project.DumbAware
import com.intellij.openapi.project.Project
import com.intellij.openapi.wm.ToolWindow
import com.intellij.openapi.wm.ToolWindowFactory

class ListVulnsToolWindowFactory :
    ToolWindowFactory,
    DumbAware {
    override fun createToolWindowContent(
        project: Project,
        toolWindow: ToolWindow,
    ) {
        val listVulnsToolWindow = ListVulnsToolWindow(project)
        val content = toolWindow.contentManager.factory.createContent(listVulnsToolWindow.getContent(), "", false)
        toolWindow.contentManager.addContent(content)
    }
}
