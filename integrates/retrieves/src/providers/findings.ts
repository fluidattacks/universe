/* eslint-disable functional/immutable-data, @typescript-eslint/no-invalid-void-type */
import _ from "lodash";
import { simpleGit } from "simple-git";
import type {
  DiagnosticCollection,
  Event,
  TreeDataProvider,
  TreeItem,
  WorkspaceFolder,
} from "vscode";
import { EventEmitter, TreeItemCollapsibleState, workspace } from "vscode";

import { refetchVulnerability } from "@retrieves/api/vulnerabilities";
import { setDiagnosticsToAllFiles } from "@retrieves/diagnostics/vulnerabilities";
import { FindingTreeItem } from "@retrieves/treeItems/finding";
import {
  VulnerabilityTreeItem,
  toVulnerability,
} from "@retrieves/treeItems/vulnerability";
import type { IFinding, IGitRoot, IVulnerability } from "@retrieves/types";
import {
  commitExists,
  fetchGitRootVulnerabilities,
} from "@retrieves/utils/vulnerabilities";

type EventGroup = FindingTreeItem | VulnerabilityTreeItem | undefined | void;
type TreeItems = FindingTreeItem[] | VulnerabilityTreeItem[];

export class FindingsProvider implements TreeDataProvider<FindingTreeItem> {
  private readonly onDidChangeTreeDataEventEmitter: EventEmitter<EventGroup> =
    new EventEmitter<EventGroup>();
  private shouldRefresh = true;
  private vulnerabilities: IVulnerability[] = [];

  private vulnerabilitiesByFinding: _.Dictionary<IVulnerability[]> = {};

  public constructor(
    private readonly diagnostics: DiagnosticCollection,
    private readonly gitRoot: IGitRoot,
    private readonly rootPath: string,
  ) {}

  // eslint-disable-next-line @typescript-eslint/member-ordering
  public readonly onDidChangeTreeData: Event<EventGroup> =
    this.onDidChangeTreeDataEventEmitter.event;

  public async refresh(item: EventGroup): Promise<void> {
    if (item instanceof VulnerabilityTreeItem) {
      const newVulnerability = await refetchVulnerability(item.vulnerability);
      const index = this.vulnerabilitiesByFinding[
        newVulnerability.finding.id
      ].findIndex((vulnerability): boolean => {
        return vulnerability.id === newVulnerability.id;
      });
      this.vulnerabilitiesByFinding[newVulnerability.finding.id][index] =
        newVulnerability;
    }

    this.shouldRefresh = true;
    this.onDidChangeTreeDataEventEmitter.fire(item);
  }

  // eslint-disable-next-line @typescript-eslint/class-methods-use-this
  public getTreeItem(element: FindingTreeItem): TreeItem {
    return element;
  }

  public async getChildren(element?: FindingTreeItem): Promise<TreeItems> {
    if (element?.contextValue === "finding") {
      return _.sortBy(this.vulnerabilitiesByFinding[element.id], [
        (vuln): string => vuln.where,
        (vuln): number => Number(vuln.specific),
      ]).map(
        (vulnerability): VulnerabilityTreeItem =>
          toVulnerability(this.rootPath, vulnerability),
      );
    } else if (element) {
      return [];
    }

    return this.updateTreeView();
  }

  private updateDiagnostics(): void {
    this.diagnostics.clear();
    setDiagnosticsToAllFiles(
      this.diagnostics,
      this.gitRoot.nickname,
      this.rootPath,
      this.vulnerabilities,
    );
  }

  private async updateTreeView(): Promise<FindingTreeItem[]> {
    // Avoid double querying and filtering
    if (this.shouldRefresh) {
      // Windows compatibility, the folder array is guaranteed to exist here
      const currentRepo = simpleGit(
        (workspace.workspaceFolders as WorkspaceFolder[])[0].uri.fsPath,
      );
      this.vulnerabilities = (
        await Promise.all(
          (await fetchGitRootVulnerabilities(this.gitRoot, this.rootPath)).map(
            async (vuln): Promise<IVulnerability | null> => {
              if (await commitExists(currentRepo, vuln.commitHash)) {
                return vuln;
              }

              return null;
            },
          ),
        )
      ).filter((vuln): boolean => vuln !== null) as IVulnerability[];

      this.vulnerabilitiesByFinding = _.groupBy(
        this.vulnerabilities,
        (vulnerability): string => vulnerability.finding.id,
      );
    }

    // Tree view items and diagnostics should mirror each other
    this.updateDiagnostics();

    const findings = this.vulnerabilities.map(
      (vulnerability): IFinding => vulnerability.finding,
    );

    const findingItems = _.uniqBy(findings, "id").map(
      (finding): FindingTreeItem =>
        new FindingTreeItem(
          finding.title,
          TreeItemCollapsibleState.Collapsed,
          finding.id,
          finding,
        ),
    );

    this.shouldRefresh = false;

    return _.orderBy(findingItems, "finding.maxOpenSeverityScoreV4", "desc");
  }
}
