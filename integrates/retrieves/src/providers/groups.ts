/* eslint-disable @typescript-eslint/no-invalid-void-type */

import type { Event, TreeDataProvider, TreeItem } from "vscode";
import { EventEmitter, TreeItemCollapsibleState } from "vscode";

import type { GitRootTreeItem } from "@retrieves/treeItems/gitRoot";
import { getGitRoots } from "@retrieves/treeItems/gitRoot";
import { GroupTreeItem } from "@retrieves/treeItems/group";

type EventGroup = GroupTreeItem | undefined | void;
type TreeItems = GitRootTreeItem[] | GroupTreeItem[];
export class GroupsProvider implements TreeDataProvider<GroupTreeItem> {
  private readonly onDidChangeTreeDataEventEmitter: EventEmitter<EventGroup> =
    new EventEmitter<EventGroup>();

  // eslint-disable-next-line @typescript-eslint/member-ordering
  public readonly onDidChangeTreeData: Event<EventGroup> =
    this.onDidChangeTreeDataEventEmitter.event;

  public constructor(private readonly groups: string[]) {}

  public refresh(): void {
    this.onDidChangeTreeDataEventEmitter.fire();
  }

  // eslint-disable-next-line @typescript-eslint/class-methods-use-this
  public getTreeItem(element: GroupTreeItem): TreeItem {
    return element;
  }

  public getChildren(element?: GroupTreeItem): Thenable<TreeItems> {
    if (element && element.contextValue === "group") {
      return Promise.resolve(getGitRoots(element.label));
    } else if (element) {
      return Promise.resolve([]);
    }

    return Promise.resolve(this.getGroupsItems());
  }

  private getGroupsItems(): GroupTreeItem[] {
    const groupItems = this.groups.map(
      (groupName): GroupTreeItem =>
        new GroupTreeItem(groupName, TreeItemCollapsibleState.Collapsed),
    );

    return groupItems;
  }
}
