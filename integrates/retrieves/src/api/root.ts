import type { ApolloError, ApolloQueryResult } from "@apollo/client";

import {
  GET_GIT_ROOT,
  GET_GIT_ROOTS,
  GET_GIT_ROOTS_SIMPLE,
  GET_VULNERABILITIES,
  UPDATE_TOE_LINES_ATTACKED,
} from "../queries";
import type {
  IGetGitRoot,
  IGetGitRoots,
  IGetVulnerabilities,
  IUpdateAttackedFile,
  IUpdateAttackedFileMutation,
  IVulnerabilitiesEdge,
  IVulnerabilitiesPaginator,
} from "@retrieves/api/types";
import type { IAttackedFile, IGitRoot, IVulnerability } from "@retrieves/types";
import { API_CLIENT, handleGraphQlError } from "@retrieves/utils/apollo";
import { Logger } from "@retrieves/utils/logging";

const getGroupGitRoots = async (groupName: string): Promise<IGitRoot[]> => {
  const result: IGetGitRoots = await API_CLIENT.query({
    query: GET_GIT_ROOTS,
    variables: { groupName },
  }).catch(async (error: unknown): Promise<IGetGitRoots> => {
    await handleGraphQlError(error as ApolloError);
    Logger.error(`Failed to get GitRoots from ${groupName}:`, error);

    return { data: { group: { roots: [] } } };
  });

  return result.data.group.roots
    .filter((root): boolean => root.state === "ACTIVE")
    .map((root): IGitRoot => {
      return { ...root, groupName };
    });
};

const getGroupGitRootsSimple = async (
  groupName: string,
): Promise<IGitRoot[]> => {
  const result: IGetGitRoots = await API_CLIENT.query({
    query: GET_GIT_ROOTS_SIMPLE,
    variables: { groupName },
  }).catch(async (error: unknown): Promise<IGetGitRoots> => {
    await handleGraphQlError(error as ApolloError);
    Logger.error(`Failed to get GitRoots from ${groupName}:`, error);

    return { data: { group: { roots: [] } } };
  });

  return result.data.group.roots
    .filter((root): boolean => root.state === "ACTIVE")
    .map((root): IGitRoot => {
      return { ...root, groupName };
    });
};

const getVulnerabilityEdges = async ({
  edges,
  endCursor,
  hasNextPage,
  groupName,
  rootNickname,
}: {
  edges: IVulnerabilitiesEdge[];
  endCursor: string;
  groupName: string;
  hasNextPage: boolean;
  rootNickname: string;
}): Promise<IVulnerabilitiesEdge[]> => {
  if (!hasNextPage) {
    return edges;
  }

  const result = await Promise.resolve(
    API_CLIENT.query<IGetVulnerabilities>({
      query: GET_VULNERABILITIES,
      variables: {
        after: endCursor,
        first: 200,
        groupName,
        nickname: rootNickname,
      },
    })
      .then(
        (
          pageResult: ApolloQueryResult<IGetVulnerabilities>,
        ): IVulnerabilitiesPaginator => {
          return pageResult.data.group.vulnerabilities;
        },
      )
      .catch(async (error: unknown): Promise<IVulnerabilitiesPaginator> => {
        await handleGraphQlError(error as ApolloError);
        Logger.error(
          `Failed to get GitRoot vulnerabilities from a root in ${groupName}:`,
          error,
        );

        return { edges: [], pageInfo: { endCursor: "", hasNextPage: false } };
      }),
  );

  return getVulnerabilityEdges({
    edges: [...edges, ...result.edges],
    endCursor: result.pageInfo.endCursor,
    groupName,
    hasNextPage: result.pageInfo.hasNextPage,
    rootNickname,
  });
};

const getGitRootVulnerabilities = async (
  groupName: string,
  rootNickname: string,
): Promise<IVulnerability[]> => {
  const edges = await getVulnerabilityEdges({
    edges: [],
    endCursor: "",
    groupName,
    hasNextPage: true,
    rootNickname,
  });

  return edges.map((edge): IVulnerability => edge.node);
};

const getGitRoot = async (
  groupName: string,
  rootId: string,
): Promise<IGitRoot> => {
  const result: IGetGitRoot = await API_CLIENT.query({
    query: GET_GIT_ROOT,
    variables: { groupName, rootId },
  }).catch(async (error: unknown): Promise<IGetGitRoot> => {
    await handleGraphQlError(error as ApolloError);
    Logger.error(`Failed to get a GitRoot from ${groupName}:`, error);

    return { data: { root: {} as IGitRoot } };
  });

  return result.data.root;
};

const markFileAsAttacked = async (
  groupName: string,
  attackedLines: number,
  rootId: string,
  fileName: string,
  comments?: string,
): Promise<IAttackedFile> => {
  const result: IUpdateAttackedFile = (
    await API_CLIENT.mutate({
      mutation: UPDATE_TOE_LINES_ATTACKED,
      variables: { attackedLines, comments, fileName, groupName, rootId },
    }).catch(async (error: unknown): Promise<IUpdateAttackedFileMutation> => {
      await handleGraphQlError(error as ApolloError);
      Logger.error(`Failed to mark file ${fileName}:`, error);

      return {
        data: {
          updateToeLinesAttackedLines: {
            message: (error as ApolloError).message,
            success: false,
          },
        },
      };
    })
  ).data;

  return result.updateToeLinesAttackedLines;
};

export {
  getGitRootVulnerabilities,
  getGroupGitRoots,
  getGitRoot,
  getGroupGitRootsSimple,
  markFileAsAttacked,
};
