import type { ApolloError } from "@apollo/client";
import { window } from "vscode";

import type { IGetUserInfo } from "./types";

import { GET_USER_INFO } from "@retrieves/queries";
import type { IUserInfo } from "@retrieves/types";
import { API_CLIENT, handleGraphQlError } from "@retrieves/utils/apollo";
import { Logger } from "@retrieves/utils/logging";

const getUserInfo = async (): Promise<IUserInfo> => {
  const role = await Promise.resolve(
    API_CLIENT.query({ query: GET_USER_INFO })
      .then(async (result: IGetUserInfo): Promise<IUserInfo> => {
        const userInfo = result.data.me;
        if (
          userInfo.userEmail.startsWith("forces.") &&
          userInfo.userEmail.endsWith("@fluidattacks.com")
        ) {
          Logger.warn("Using DevSecOps token instead of the user's API token");
          await window.showWarningMessage(`You seem to be using a DevSecOps
          token. This token is not meant to be used for this extension, you
          cannot request reattacks nor accept vulnerabilities with it. We
          encourage you to use your own API token instead`);
        }

        return userInfo;
      })
      .catch(async (error: unknown): Promise<IUserInfo> => {
        await handleGraphQlError(error as ApolloError);
        Logger.error("Failed to get global role: ", error);

        return { error: error as ApolloError, role: "", userEmail: "" };
      }),
  );

  return role;
};

export { getUserInfo };
