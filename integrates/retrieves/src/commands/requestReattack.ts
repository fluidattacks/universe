import _ from "lodash";
import { all, groupBy, range } from "ramda";
import type { DiagnosticCollection } from "vscode";
import { window } from "vscode";

import { requestReattack as requestReattackMutation } from "@retrieves/api/vulnerabilities";
import type { FindingsProvider } from "@retrieves/providers/findings";
import type { VulnerabilityTreeItem } from "@retrieves/treeItems/vulnerability";
import type { VulnerabilityDiagnostic } from "@retrieves/types";
import { getJustificationForReattack } from "@retrieves/utils/prompts";

const requestReattack = async (
  retrievesDiagnostics: DiagnosticCollection,
): Promise<void> => {
  const { activeTextEditor } = window;
  if (!activeTextEditor) {
    return;
  }
  const fileDiagnostics: readonly VulnerabilityDiagnostic[] | undefined =
    retrievesDiagnostics.get(activeTextEditor.document.uri);

  if (!fileDiagnostics) {
    void window.showInformationMessage(
      "This line does not contain vulnerabilities",
    );

    return;
  }
  const diagnostics = fileDiagnostics.filter(
    (item): boolean =>
      item.source === "fluidattacks" &&
      range(
        activeTextEditor.selection.start.line,
        activeTextEditor.selection.end.line + 1,
      ).includes(item.range.start.line),
  );

  const diagnosticsGroupByFinding = groupBy(
    (item): string => item.findingId ?? "",
    diagnostics,
  );

  const justification = await getJustificationForReattack();
  if (_.isUndefined(justification)) {
    return;
  }

  const result = await Promise.all(
    Object.keys(diagnosticsGroupByFinding).map(
      async (findingId): Promise<boolean> => {
        const response = await requestReattackMutation(
          findingId,
          justification,
          (diagnosticsGroupByFinding[findingId] ?? []).map(
            (diagnostic): string => diagnostic.vulnerabilityId ?? "",
          ),
        );
        if (!response.requestVulnerabilitiesVerification.success) {
          await window.showWarningMessage(
            response.requestVulnerabilitiesVerification.message ??
              "Failed to request vulnerability reattack",
          );
        }

        return response.requestVulnerabilitiesVerification.success;
      },
    ),
  );

  if (all((item): boolean => item, result)) {
    void window.showInformationMessage("Reattack requested successfully");
  }
};

const requestReattackTreeItem = async (
  dataProvider: FindingsProvider,
  item: VulnerabilityTreeItem,
): Promise<void> => {
  const justification = await getJustificationForReattack();
  if (_.isUndefined(justification)) {
    return;
  }

  const response = await requestReattackMutation(
    item.vulnerability.finding.id,
    justification,
    [item.id],
  );

  if (!response.requestVulnerabilitiesVerification.success) {
    await window.showWarningMessage(
      response.requestVulnerabilitiesVerification.message ??
        "Failed to request vulnerability reattack",
    );
  }

  if (response.requestVulnerabilitiesVerification.success) {
    void window.showInformationMessage("Reattack requested successfully");
    await dataProvider.refresh(item);
  }
};

export { requestReattack, requestReattackTreeItem };
