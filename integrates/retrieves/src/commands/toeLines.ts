/* eslint-disable functional/immutable-data */
import { existsSync } from "fs";
import { join } from "path";

import type { MessageHandlerData } from "@estruyf/vscode";
import type { ExtensionContext } from "vscode";
import { Uri, ViewColumn, env, window } from "vscode";

import { getToeLines } from "@retrieves/api/toeLines";
import type { GitRootTreeItem } from "@retrieves/treeItems/gitRoot";
import type { IBugsnagConfig, IGitRoot, IToeLines } from "@retrieves/types";
import { getEnvironment } from "@retrieves/utils/environment";
import { getGroupsPath } from "@retrieves/utils/file";
import { getWebviewContent } from "@retrieves/utils/webview";

const toeLines = (context: ExtensionContext, node: GitRootTreeItem): void => {
  const panel = window.createWebviewPanel(
    "toe-lines",
    `Toe Lines • ${node.label}`,
    ViewColumn.One,
    {
      enableScripts: true,
      retainContextWhenHidden: true,
    },
  );

  panel.webview.onDidReceiveMessage(
    (message: {
      command: string;
      requestId: string;
      payload: { message: string };
    }): void => {
      const { command, requestId, payload } = message;
      switch (command) {
        case "GET_ROUTE": {
          void panel.webview.postMessage({
            command,
            payload: "toeLines",
            // The requestId is used to identify the response
            requestId,
          } as MessageHandlerData<string>);
          break;
        }
        case "GET_BUGSNAG_CONFIG": {
          void panel.webview.postMessage({
            command,
            payload: {
              isTelemetryEnabled: env.isTelemetryEnabled,
              retrievesVersion: (
                context.extension.packageJSON as Record<string, string>
              ).version,
              stage: getEnvironment(context),
            },
            // The requestId is used to identify the response
            requestId,
          } as MessageHandlerData<IBugsnagConfig>);
          break;
        }
        case "GET_DATA_TOE_LINES": {
          void getToeLines(node.groupName, node.rootId).then((toe): void => {
            const nodes = toe.map((edge): IToeLines => {
              const rootPath = join(
                getGroupsPath(),
                node.groupName,
                node.nickname,
              );
              const uri = Uri.parse(
                `file://${join(rootPath, edge.node.filename)}`,
              );

              return { ...edge.node, fileExists: existsSync(uri.fsPath) };
            });
            void panel.webview.postMessage({
              command,
              payload: nodes,
              // The requestId is used to identify the response
              requestId,
            } as MessageHandlerData<IToeLines[]>);
          });
          break;
        }
        case "GET_ROOT_ID": {
          void panel.webview.postMessage({
            command,
            payload: node.rootId,
            // The requestId is used to identify the response
            requestId,
          } as MessageHandlerData<string>);

          break;
        }
        case "POST_DATA": {
          void window.showInformationMessage(
            `Received data from the webview: ${payload.message}`,
          );
          break;
        }
        case "TOE_LINES_OPEN_FILE": {
          const rootPath = join(getGroupsPath(), node.groupName, node.nickname);
          const uri = Uri.parse(
            `file://${join(rootPath, String(payload.message))}`,
          );
          void window.showTextDocument(uri);
          break;
        }
        case "GET_ROOT": {
          void panel.webview.postMessage({
            command,
            payload: {
              gitEnvironmentUrls: [],
              gitignore: [],
              groupName: node.groupName,
              id: node.rootId,
              nickname: node.nickname,
              state: "ACTIVE",
              url: node.url,
            } as IGitRoot,
            // The requestId is used to identify the response
            requestId,
          } as MessageHandlerData<unknown>);
          break;
        }
        default:
          break;
      }
    },
    undefined,
    context.subscriptions,
  );

  panel.webview.html = getWebviewContent(context, panel.webview);
};

export { toeLines };
