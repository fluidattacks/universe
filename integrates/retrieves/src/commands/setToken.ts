import _ from "lodash";
import type { ExtensionContext, InputBoxValidationMessage } from "vscode";
import { InputBoxValidationSeverity, commands, window } from "vscode";

import { Logger } from "../utils/logging";
import { validTextField } from "../utils/validations";

export const setToken = async (context: ExtensionContext): Promise<void> => {
  const fluidToken = await window.showInputBox({
    password: true,
    placeHolder: "Paste your token here",
    title: "Fluid Attacks API Token",
    validateInput: (token): InputBoxValidationMessage | undefined => {
      const validationMessage = validTextField(token);

      if (!_.isNil(validationMessage)) {
        return {
          message: `Invalid JSON Web Token: ${validationMessage}`,
          severity: InputBoxValidationSeverity.Error,
        };
      }

      return undefined;
    },
  });

  if (!_.isNil(fluidToken)) {
    await context.secrets.store("fluidToken", fluidToken);
    Logger.info("Fluid Attacks token updated");

    const actions = ["Reload", "Later"];
    window.showInformationMessage("Reload to apply changes", ...actions).then(
      async (selectedAction): Promise<void> => {
        if (selectedAction === "Reload") {
          Logger.info("Reloading extension host");
          await commands.executeCommand(
            "workbench.action.restartExtensionHost",
          );
        }
      },
      (reason: unknown): void => {
        Logger.info("Dismissed reload:", reason);
      },
    );
  }
};
