import { simpleGit } from "simple-git";
import { window } from "vscode";

import { Logger } from "@retrieves/utils/logging";

const checkGitInstallation = async (fsPath: string): Promise<boolean> => {
  try {
    const git = simpleGit(fsPath);
    await git.raw(["--version"]);

    const isRepo = await git.checkIsRepo();

    if (!isRepo) {
      Logger.warn("Current dir is not within a Git repository");
      await window.showWarningMessage(
        `The current directory is not within a Git repository. Fluid Attacks
        uses and checks the Git commit history for some of its features.
        To enable these features, please initialize a Git repository or
        clone an existing one and try again.`,
      );
    }

    return isRepo;
  } catch (error) {
    Logger.warn("Git is not installed or not in the system PATH:", error);
    await window.showWarningMessage(
      `Git is not installed or not in the system PATH. Fluid Attacks requires
      Git for some of its features. To enable these features, please install
      Git, initialize a Git repository or clone an existing one and try again.`,
    );

    return false;
  }
};

const getGitRootPath = async (path: string): Promise<string> => {
  const repo = simpleGit(path);
  const rootPath = await repo.revparse(["--show-toplevel"]);

  return rootPath.trim();
};

export { checkGitInstallation, getGitRootPath };
