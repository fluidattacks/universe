import type { InputBoxValidationMessage } from "vscode";
import { InputBoxValidationSeverity, window } from "vscode";

import {
  validDateField,
  validJustification,
} from "@retrieves/utils/validations";

const getAcceptanceEndDate = async (): Promise<string | undefined> => {
  const date = await window.showInputBox({
    placeHolder: "yyyy-mm-dd",
    title: "Temporarily accepted until",
    validateInput: (message): InputBoxValidationMessage | undefined => {
      const validationMessage = validDateField(message);

      if (validationMessage !== undefined) {
        return {
          message: validationMessage,
          severity: InputBoxValidationSeverity.Error,
        };
      }

      return undefined;
    },
  });

  return date;
};

const getJustificationForReattack = async (): Promise<string | undefined> => {
  const justification = await window.showInputBox({
    placeHolder: "justification",
    title: "Reattack justification",
    validateInput: validJustification,
  });

  return justification;
};

const getJustificationForTreatment = async (): Promise<string | undefined> => {
  const justification = await window.showInputBox({
    placeHolder: "justification",
    title: "Treatment justification",
    validateInput: validJustification,
  });

  return justification;
};

export {
  getAcceptanceEndDate,
  getJustificationForReattack,
  getJustificationForTreatment,
};
