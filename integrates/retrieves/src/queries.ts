import type { DocumentNode } from "@apollo/client/core";
import { gql } from "@apollo/client/core";

const GET_GROUPS: DocumentNode = gql`
  query RetrievesGetGroups {
    me {
      userEmail
      organizations {
        groups {
          name
          userRole
          subscription

          roots {
            ... on GitRoot {
              id
              nickname
              state
              url
            }
          }
        }
        name
      }
    }
  }
`;

const GET_GIT_ROOTS = gql`
  query RetrievesGetGitRoots($groupName: String!) {
    group(groupName: $groupName) {
      name
      roots {
        ... on GitRoot {
          id
          nickname
          downloadUrl
          gitignore
          state
          url
          gitEnvironmentUrls {
            url
            id
          }
        }
      }
    }
  }
`;

const GET_GIT_ROOTS_SIMPLE = gql`
  query RetrievesGetGitRootsSimple($groupName: String!) {
    group(groupName: $groupName) {
      name
      roots {
        ... on GitRoot {
          id
          nickname
          state
          url
        }
      }
    }
  }
`;

const GET_USER_INFO: DocumentNode = gql`
  query RetrievesGetUserInfo {
    me {
      userEmail
      role
    }
  }
`;

const GET_GROUP_STAKEHOLDERS = gql`
  query RetrievesGetGroupStakeholders($groupName: String!) {
    group(groupName: $groupName) {
      name
      stakeholders {
        email
        invitationState
        role
      }
    }
  }
`;

const GET_TOE_LINES = gql`
  query RetrievesGetToeLines(
    $groupName: String!
    $after: String
    $bePresent: Boolean
    $first: Int
    $rootId: ID
  ) {
    group(groupName: $groupName) {
      name
      toeLines(
        bePresent: $bePresent
        after: $after
        first: $first
        rootId: $rootId
      ) {
        edges {
          node {
            attackedLines
            bePresent
            filename
            comments
            modifiedDate
            loc
            sortsPriorityFactor
          }
        }
        pageInfo {
          hasNextPage
          endCursor
        }
        total
        __typename
      }
      __typename
    }
  }
`;

const UPDATE_TOE_LINES_ATTACKED = gql`
  mutation RetrievesUpdateToeLinesAttackedLines(
    $groupName: String!
    $attackedLines: Int!
    $fileName: String!
    $rootId: String!
    $comments: String!
  ) {
    updateToeLinesAttackedLines(
      groupName: $groupName
      attackedLines: $attackedLines
      filename: $fileName
      rootId: $rootId
      comments: $comments
    ) {
      success
    }
  }
`;

const GET_VULNERABILITY = gql`
  query RetrievesRefetchVulnerability($identifier: String!) {
    vulnerability(uuid: $identifier) {
      id
      isAutoFixable
      where
      specific
      state
      reportDate
      rootNickname
      treatmentStatus
      verification
      finding {
        id
        title
        description
        groupName
        maxOpenSeverityScoreV4
      }
    }
  }
`;

const GET_VULNERABILITIES = gql`
  query RetrievesGetRootVulnerabilities(
    $groupName: String!
    $after: String!
    $nickname: String
    $first: Int
  ) {
    group(groupName: $groupName) {
      name
      vulnerabilities(
        type: "LINES"
        state: VULNERABLE
        after: $after
        first: $first
        root: $nickname
      ) {
        edges {
          node {
            commitHash
            id
            isAutoFixable
            where
            specific
            state
            reportDate
            rootNickname
            treatmentStatus
            finding {
              id
              title
              description
              groupName
              maxOpenSeverityScoreV4
            }
            verification
          }
        }
        pageInfo {
          endCursor
          hasNextPage
        }
      }
    }
  }
`;

const GET_GIT_ROOT = gql`
  query RetrievesGetRoot($groupName: String!, $rootId: ID!) {
    root(groupName: $groupName, rootId: $rootId) {
      ... on GitRoot {
        id
        nickname
        downloadUrl
        gitignore
        state
        gitEnvironmentUrls {
          url
          id
        }
        url
      }
    }
  }
`;

const GET_FINDING_DESCRIPTION = gql`
  query RetrievesGetFindingDescription($findingId: String!) {
    finding(identifier: $findingId) {
      attackVectorDescription
      description
      groupName
      id
      minTimeToRemediate
      recommendation
      maxOpenSeverityScoreV4
      severityVectorV4
      threat
      title
      vulnerabilitiesSummaryV4 {
        open
      }
    }
  }
`;

const REQUEST_VULNERABILITIES_VERIFICATION = gql`
  mutation RetrievesRequestVulnerabilitiesVerification(
    $findingId: String!
    $justification: String!
    $vulnerabilities: [String]!
  ) {
    requestVulnerabilitiesVerification(
      findingId: $findingId
      justification: $justification
      vulnerabilities: $vulnerabilities
    ) {
      success
    }
  }
`;

const ACCEPT_VULNERABILITY_TEMPORARILY = gql`
  mutation RetrievesAcceptVulnerability(
    $findingId: String!
    $vulnerabilityId: ID!
    $acceptanceDate: String
    $assigned: String
    $justification: String!
    $treatment: UpdateClientDescriptionTreatment!
  ) {
    updateVulnerabilitiesTreatment(
      acceptanceDate: $acceptanceDate
      assigned: $assigned
      findingId: $findingId
      justification: $justification
      treatment: $treatment
      vulnerabilityId: $vulnerabilityId
    ) {
      success
    }
  }
`;

const GET_CUSTOM_FIX = gql`
  query RetrievesGetCustomFix($vulnerabilityId: String!) {
    vulnerability(uuid: $vulnerabilityId) {
      customFix
    }
  }
`;

const GET_CUSTOM_FIX_SUBSCRIPTION = gql`
  subscription RetrievesGetCustomFix($vulnerabilityId: String!) {
    getCustomFix(vulnerabilityId: $vulnerabilityId, featurePreview: false)
  }
`;

const GET_SUGGESTED_FIX = gql`
  query RetrievesGetSuggestedFix(
    $vulnerabilityId: String!
    $vulnerableFunction: String!
    $vulnerableLineContent: String!
    $vulnerableCodeImports: String!
  ) {
    suggestedFix(
      vulnerabilityId: $vulnerabilityId
      vulnerableFunction: $vulnerableFunction
      vulnerableLineContent: $vulnerableLineContent
      vulnerableCodeImports: $vulnerableCodeImports
    ) {
      success
      message
    }
  }
`;

const GET_SUGGESTED_FIX_SUBS = gql`
  subscription RetrievesGetSuggestedFix(
    $vulnerabilityId: String!
    $vulnerableFunction: String!
    $vulnerableLineContent: String!
    $vulnerableCodeImports: String!
  ) {
    getSuggestedFix(
      vulnerabilityId: $vulnerabilityId
      vulnerableFunction: $vulnerableFunction
      vulnerableLineContent: $vulnerableLineContent
      vulnerableCodeImports: $vulnerableCodeImports
      featurePreview: false
    )
  }
`;

export {
  GET_SUGGESTED_FIX_SUBS,
  GET_SUGGESTED_FIX,
  GET_GROUPS,
  GET_GIT_ROOTS,
  GET_GIT_ROOTS_SIMPLE,
  GET_USER_INFO,
  GET_TOE_LINES,
  GET_VULNERABILITY,
  GET_VULNERABILITIES,
  UPDATE_TOE_LINES_ATTACKED,
  GET_FINDING_DESCRIPTION,
  GET_GIT_ROOT,
  GET_GROUP_STAKEHOLDERS,
  REQUEST_VULNERABILITIES_VERIFICATION,
  ACCEPT_VULNERABILITY_TEMPORARILY,
  GET_CUSTOM_FIX,
  GET_CUSTOM_FIX_SUBSCRIPTION,
};
