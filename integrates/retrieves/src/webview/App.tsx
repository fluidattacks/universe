import { ApolloProvider } from "@apollo/client";
import { messageHandler } from "@estruyf/vscode/dist/client";
import type React from "react";
import { useMemo, useState } from "react";
import { createRoot } from "react-dom/client";
import { MemoryRouter, Route, Routes } from "react-router-dom";

import { CustomFixForVulnerability } from "./containers/CustomFixForVulnerability";
import type { IBugsnagConfig } from "./types";

import { getClient } from "@retrieves/utils/api";
import { FindingDescription } from "@retrieves/webview/containers/FindingDescription";
import { GitEnvironmentUrls } from "@retrieves/webview/containers/GitEnvironmentUrls";
import { ToeLines } from "@retrieves/webview/containers/ToeLines";
import {
  BugsnagErrorBoundary,
  startBugsnag,
} from "@retrieves/webview/utils/telemetry";

import "@retrieves/webview/styles.css";

// eslint-disable-next-line no-useless-assignment
const App = (): React.JSX.Element => {
  const [route, setRoute] = useState<string>();

  const [bugsnagConfig, setBugsnagConfig] = useState<IBugsnagConfig>({
    isTelemetryEnabled: false,
    retrievesVersion: "auto",
    stage: "production",
  });

  useMemo((): void => {
    void messageHandler.request<string>("GET_ROUTE").then((msg): void => {
      setRoute(msg);
    });
  }, []);

  useMemo((): void => {
    void messageHandler
      .request<IBugsnagConfig>("GET_BUGSNAG_CONFIG")
      .then((config): void => {
        setBugsnagConfig(config);
      });
  }, []);

  if (route === undefined) {
    return <div />;
  }
  if (bugsnagConfig.retrievesVersion !== "auto") {
    startBugsnag(bugsnagConfig);
  }

  return (
    <BugsnagErrorBoundary>
      <ApolloProvider client={getClient()}>
        <div className={"app"}>
          <MemoryRouter initialEntries={[`/${route}`]}>
            <Routes>
              <Route
                element={<FindingDescription />}
                path={"findingDescription"}
              />
              <Route element={<ToeLines />} path={"toeLines"} />
              <Route
                element={<GitEnvironmentUrls />}
                path={"gitEnvironmentUrls"}
              />
              <Route
                element={<CustomFixForVulnerability />}
                path={"customFixForVulnerability"}
              />
            </Routes>
          </MemoryRouter>
        </div>
      </ApolloProvider>
    </BugsnagErrorBoundary>
  );
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
declare const acquireVsCodeApi: <T = unknown>() => {
  getState: () => T;
  setState: (data: T) => void;
  postMessage: (msg: unknown) => void;
};

const elm = document.querySelector("#root");
if (elm) {
  const root = createRoot(elm);
  root.render(<App />);
}
