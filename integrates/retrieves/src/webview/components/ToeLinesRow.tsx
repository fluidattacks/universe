import { messageHandler } from "@estruyf/vscode/dist/client";
import {
  VSCodeDataGridCell,
  VSCodeDataGridRow,
  VSCodeLink,
} from "@vscode/webview-ui-toolkit/react";
import type React from "react";
import { useCallback } from "react";

import type { IToeLines } from "@retrieves/types";

interface IToeLinesRowProps {
  readonly groupName: string;
  readonly node: IToeLines;
  readonly rootId: string;
}

const ToeLinesRow: React.FC<IToeLinesRowProps> = ({
  node,
  rootId,
  groupName,
}: IToeLinesRowProps): React.JSX.Element => {
  const {
    filename,
    attackedLines,
    loc,
    modifiedDate,
    comments,
    fileExists,
    sortsPriorityFactor,
  } = node;
  const useOpenFile = useCallback(
    // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
    (name: string) => (): void => {
      messageHandler.send("TOE_LINES_OPEN_FILE", { message: name });
    },
    [],
  );

  return (
    <VSCodeDataGridRow key={filename}>
      {[
        filename,
        String(attackedLines >= loc),
        attackedLines,
        loc,
        modifiedDate,
        comments,
        `${sortsPriorityFactor}%`,
      ].map((cell, index): React.JSX.Element => {
        const context = {
          comments,
          filename,
          groupName,
          loc,
          rootId,
          webviewSection: "filename",
        };

        return (
          <VSCodeDataGridCell
            data-vscode-context={JSON.stringify(context)}
            gridColumn={String(index + 1)}
            key={undefined}
          >
            {index === 0 && (fileExists ?? false) ? (
              <VSCodeLink
                href={String(cell)}
                // eslint-disable-next-line react-hooks/rules-of-hooks
                onClick={useOpenFile(String(cell))}
              >
                {cell}
              </VSCodeLink>
            ) : (
              cell
            )}
          </VSCodeDataGridCell>
        );
      })}
    </VSCodeDataGridRow>
  );
};

export { ToeLinesRow };
