import { messageHandler } from "@estruyf/vscode/dist/client";
import {
  VSCodeDataGrid,
  VSCodeDataGridCell,
  VSCodeDataGridRow,
  VSCodeDivider,
  VSCodeLink,
  VSCodeTag,
} from "@vscode/webview-ui-toolkit/react";
import type React from "react";
import { useMemo, useState } from "react";

import type { IFindingDescription } from "@retrieves/types";
import "@retrieves/webview/styles.css";
import { getAppUrl } from "@retrieves/utils/url";

const mapScoreToCategory = (severityScore: number): string => {
  if (severityScore > 0 && severityScore < 4.0) {
    return "Low";
  } else if (severityScore >= 4.0 && severityScore < 7.0) {
    return "Medium";
  } else if (severityScore >= 7.0 && severityScore < 9.0) {
    return "High";
  }

  return "Critical";
};

export const FindingDescription = (): React.JSX.Element => {
  const [description, setDescription] = useState<IFindingDescription>();

  useMemo((): void => {
    void messageHandler
      .request<IFindingDescription>("GET_DESCRIPTION")
      .then((desc): void => {
        setDescription(desc);
      });
  }, []);
  if (description === undefined) {
    return <div />;
  }

  const keys: (keyof Omit<IFindingDescription, "vulnerabilitiesSummaryV4">)[] =
    [
      "description",
      "attackVectorDescription",
      "threat",
      "maxOpenSeverityScoreV4",
      "recommendation",
    ];
  const labels: Record<string, string> = {
    attackVectorDescription: "Attack Vector",
    description: "Description",
    maxOpenSeverityScoreV4: "Severity score (v4.0)",
    minTime: "Average remediation time per vulnerability",
    recommendation: "Recommendation",
    threat: "Threat",
  };
  const baseUrl = getAppUrl();
  const fluidLink = `${baseUrl}/groups/${description.groupName}/vulns/${description.id}/locations`;
  const criteriaLink = `https://help.fluidattacks.com/portal/en/kb/articles/criteria-vulnerabilities-${
    description.title.split(".")[0]
  }`;
  const minTime =
    description.minTimeToRemediate === undefined ? (
      <time>{"Unknown"}</time>
    ) : (
      <time dateTime={`PT${description.minTimeToRemediate.toString()}m`}>
        {`${description.minTimeToRemediate}m`}
      </time>
    );

  return (
    <div>
      <h2>
        {description.title}{" "}
        <VSCodeTag>{`${description.vulnerabilitiesSummaryV4.open} vulnerable location(s)`}</VSCodeTag>
      </h2>

      <VSCodeDivider />

      <VSCodeDataGrid
        generateHeader={"none"}
        grid-template-columns={".5fr 1fr"}
      >
        {keys.map((item): React.JSX.Element => {
          const content =
            item === "maxOpenSeverityScoreV4"
              ? `${description[item]} (${mapScoreToCategory(description[item])})`
              : description[item];

          return (
            <VSCodeDataGridRow key={item}>
              <VSCodeDataGridCell gridColumn={"1"}>
                {labels[item]}
              </VSCodeDataGridCell>
              <VSCodeDataGridCell gridColumn={"2"}>
                {content}
              </VSCodeDataGridCell>
            </VSCodeDataGridRow>
          );
        })}
        <VSCodeDataGridRow key={"minTime"}>
          <VSCodeDataGridCell gridColumn={"1"}>
            {labels.minTime}
          </VSCodeDataGridCell>
          <VSCodeDataGridCell gridColumn={"2"}>{minTime}</VSCodeDataGridCell>
        </VSCodeDataGridRow>
      </VSCodeDataGrid>
      <br />
      <VSCodeLink href={fluidLink}>{"Link to Fluid Attacks"}</VSCodeLink>
      {" • "}
      <VSCodeLink href={criteriaLink}>{"Go to Criteria"}</VSCodeLink>
      <VSCodeDivider />
      <small>{description.severityVectorV4}</small>
    </div>
  );
};
