# shellcheck shell=bash

function main {
  local success=0

  : \
    && pushd integrates/retrieves/test/e2e \
    && npm ci \
    && if npm run lint-tsc; then
      success=$((success + 1))
    fi \
    && if npm run lint-eslint; then
      success=$((success + 1))
    fi \
    && if test "${success}" -eq 2; then
      info "Congratulations! Your code comply with the suggested style"
    else
      critical "Your code doesn't comply with the suggested style"
    fi
}

main "${@}"
