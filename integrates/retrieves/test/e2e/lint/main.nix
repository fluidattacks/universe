{ inputs, makeScript, outputs, projectPath, ... }:
makeScript {
  entrypoint = ./entrypoint.sh;
  name = "integrates-retrieves-e2e-lint";
  searchPaths.bin = [ inputs.nixpkgs.bash inputs.nixpkgs.nodejs_20 ];
}
