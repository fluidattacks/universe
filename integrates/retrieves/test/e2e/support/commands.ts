export const overrideCommands = () => {
  Cypress.Commands.overwrite("log", function (log, ...args) {
    if (Cypress.browser.isHeadless) {
      return cy.task("log", args, { log: false }).then(() => {
        return log(...args);
      });
    } else {
      console.log(...args);
      return log(...args);
    }
  });
};

Cypress.Commands.add("findExactText", (selector, text) => {
  cy.get(selector).then(($elements) => {
    const exactMatchElement = $elements.filter((_, element) => {
      return element.textContent?.trim() === text;
    });

    if (exactMatchElement.length) {
      cy.wrap(exactMatchElement);
    } else {
      throw new Error(`No element found with exact text: ${text}`);
    }
  });
});
