/* eslint-disable @typescript-eslint/no-namespace, @typescript-eslint/no-unused-vars */
declare namespace Cypress {
  interface Chainable<Subject> {
    findExactText(selector: string, text: string): Chainable<Subject>;
  }
}
