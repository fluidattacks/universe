import { configureKeyboard } from "./keyboard";
import { manageExceptions } from "./exceptions";
import { overrideCommands } from "./commands";

overrideCommands();
manageExceptions();
configureKeyboard()
