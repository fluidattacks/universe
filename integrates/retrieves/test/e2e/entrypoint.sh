# shellcheck shell=bash

function main {
  local cluster="common-k8s"
  local region="us-east-1"
  : \
    && aws_login "dev" "3600" \
    && sops_export_vars_cypress integrates/secrets/dev.yaml \
      TEST_RETRIEVES_TOKEN \
    && if test -n "${CI-}"; then
      aws_eks_update_kubeconfig "${cluster}" "${region}" \
        && kubectl rollout status \
          "deploy/integrates-${CI_COMMIT_REF_NAME}" \
          -n "dev" \
          --timeout=0
    fi \
    && export FLUID_API_TOKEN=${TEST_RETRIEVES_TOKEN} \
    && if test -n "${CI-}"; then
      export APP_URL="https://${CI_COMMIT_REF_NAME}.app.fluidattacks.com"
    else
      export NODE_TLS_REJECT_UNAUTHORIZED=0
      export APP_URL=https://localhost:8001
    fi \
    && info "Running against ${APP_URL}" \
    && start_code_server --standalone \
    && pushd integrates/retrieves/test/e2e \
    && npm ci \
    && CYPRESS_REPO_PATH="${PWD}/WebGoat" \
    && if [ ! -d "${CYPRESS_REPO_PATH}" ]; then
      git clone https://github.com/WebGoat/WebGoat \
        && git -C "${CYPRESS_REPO_PATH}" checkout 9d5ab5fb21921cf1a70de9bfb1d4e0f367bbbd4d
    fi \
    && export CYPRESS_REPO_PATH \
    && run_cypress_fa "firefox" "${@}" \
    && stop_code_server \
    && popd \
    || return 1
}

main "${@}"
