{ inputs, isLinux, isDarwin, makeScript, outputs, projectPath, ... }:
makeScript {
  entrypoint = ./entrypoint.sh;
  name = "integrates-retrieves-test-e2e";
  searchPaths = {
    bin = [ inputs.nixpkgs.git inputs.nixpkgs.kubectl inputs.nixpkgs.nodejs_20 ]
      ++ inputs.nixpkgs.lib.optionals isLinux [ inputs.nixpkgs.chromium ]
      ++ inputs.nixpkgs.lib.optionals isDarwin [ inputs.nixpkgs.electron ];
    source = [
      outputs."/common/utils/aws"
      outputs."/common/utils/sops"
      outputs."/integrates/retrieves/code-server"
      outputs."/common/utils/cypress-fa"
    ];
  };
}
