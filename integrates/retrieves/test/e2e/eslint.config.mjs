import typescriptEslint from "@typescript-eslint/eslint-plugin";
import cypress from "eslint-plugin-cypress";
import tsParser from "@typescript-eslint/parser";
import eslint from "@eslint/js";
import tseslint from "typescript-eslint";

export default [
  eslint.configs.recommended,
  ...tseslint.configs.recommended,
  {
    ignores: [
      "**/.eslintrc.js",
      "**/eslint.config.mjs",
      "./gql/*.ts",
      "cypress_bin/**/*",
      "coverage/**/*",
      "WebGoat/**/*",
    ],
  },
  {
    files: ["**/*.ts", "**/*.tsx"],
    plugins: {
      "@typescript-eslint": typescriptEslint,
      cypress,
    },
    languageOptions: {
      parser: tsParser,
      ecmaVersion: 5,
      sourceType: "module",
      parserOptions: {
        project: "./tsconfig.json",
        projectService: true,
      },
    },
  },
];
