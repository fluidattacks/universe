{ projectPath, ... }:
let
  arch = let
    commit = "3c8778c732597a2fdee42d7c20d1ab5d948e13d4";
    sha256 = "sha256:0ii4qam4ykq9gbi2lmkk9p9gixmrypmz54jgcji9hpypk4azxy3w";
    url =
      "https://gitlab.com/fluidattacks/universe/-/raw/${commit}/common/ci/arch.nix";
    src = builtins.fetchurl { inherit sha256 url; };
  in import src;

  rules = {
    all = {
      default = arch.core.rules.titleRule {
        products = [ "all" "integrates-retrieves" "integrates[^-]" ];
        types = [ ];
      };
      noRotate = arch.core.rules.titleRule {
        products = [ "all" "integrates-retrieves" "integrates" ];
        types = [ "feat" "fix" "refac" ];
      };
    };
    onlyRetrieves = {
      noRotate = arch.core.rules.titleRule {
        products = [ "all" "integrates-retrieves" ];
        types = [ "feat" "fix" "refac" ];
      };
    };
  };
in {
  pipelines = {
    integratesRetrievesDefault = {
      gitlabPath = "/integrates/retrieves/pipeline/default.yaml";
      jobs = [
        {
          output = "/pipelineOnGitlab/integratesRetrievesDefault";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.all.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/integrates/retrieves test";
          gitlabExtra = arch.extras.default // {
            artifacts = {
              paths = [ "integrates/retrieves/.coverage/lcov.info" ];
              expire_in = "1 day";
            };
            rules = arch.rules.dev ++ [ rules.all.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common-large ];
          };
        }
        {
          output = "/integrates/retrieves/test/e2e run";
          gitlabExtra = arch.extras.default // {
            artifacts = {
              paths =
                [ "integrates/retrieves/test/e2e/cypress/screenshots/**" ];
              expire_in = "1 day";
              when = "always";
            };
            needs = [ "/integrates/back/deploy/dev" ];
            rules = arch.rules.dev ++ [ rules.all.noRotate ];
            stage = arch.stages.post-deploy;
            tags = [ arch.tags.common-large ];
          };
        }
        {
          output = "/integrates/retrieves/test/e2e/lint";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.all.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/integrates/retrieves/plugins/intellij lint";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.all.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/integrates/retrieves deploy";
          gitlabExtra = arch.extras.default // {
            resource_group = "deploy/$CI_JOB_NAME";
            rules = arch.rules.prod ++ [ rules.all.default ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.common-large ];
          };
        }
        {
          output = "/integrates/retrieves lint";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.all.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common-large ];
          };
        }
        {
          output = "/integrates/retrieves/coverage";
          gitlabExtra = arch.extras.default // {
            needs = [ "/integrates/retrieves test" ];
            rules = arch.rules.dev ++ [ rules.all.noRotate ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.integrates ];
            variables.GIT_DEPTH = 1000;
          };
        }
      ];
    };
  };
}
