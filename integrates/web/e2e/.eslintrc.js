module.exports = {
  ignorePatterns: [".eslintrc.js", "./gql/*.ts", "cypress_bin/**", "coverage/**", "cypress/**"],
  overrides: [
    {
      files: ["*.ts", "*.tsx"],
      parser: "@typescript-eslint/parser",
      parserOptions: {
        sourceType: "module",
        project: "./tsconfig.json",
        tsconfigRootDir: __dirname,
      },
      plugins: [
        "@typescript-eslint",
        "cypress",
      ],
    }
  ],
};
