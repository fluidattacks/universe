{ inputs, makeScript, outputs, projectPath, ... }:
makeScript {
  entrypoint = ./entrypoint.sh;
  name = "integrates-web-e2e-cypress-lint";
  searchPaths.bin = [ inputs.nixpkgs.bash inputs.nixpkgs.nodejs_20 ];
}
