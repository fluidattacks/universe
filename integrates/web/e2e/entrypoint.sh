# shellcheck shell=bash

function filter_node_specs {
  local node_name="${1}"

  : && raw_occurrences=$(grep -r "_n$node_name" specs/) \
    && echo "${raw_occurrences}" | awk -F ":" '{print $1}' | sort | uniq | tr "\n" "," | sed 's/,$//'
}

function main {
  local cluster="common-k8s"
  local region="us-east-1"

  : && aws_login "dev" "3600" \
    && sops_export_vars_cypress integrates/secrets/dev.yaml \
      JWT_ENCRYPTION_KEY_DEV \
      JWT_SECRET_ES512_DEV \
      GOOGLE_OAUTH2_SECRET \
      GOOGLE_OAUTH2_KEY \
      GOOGLE_REFRESH_TOKEN \
    && if test -n "${CI-}"; then
      : && aws_eks_update_kubeconfig "${cluster}" "${region}" \
        && kubectl rollout status \
          "deploy/integrates-${CI_COMMIT_REF_NAME}" \
          -n "dev" \
          --timeout=0
    fi \
    && pushd integrates/web/e2e \
    && npm install \
    && if test -n "${CI-}"; then
      : && info "Running specs for node ${CI_NODE_INDEX}" \
        && specs=$(filter_node_specs "${CI_NODE_INDEX}") \
        && run_cypress_fa "chromium" "run" "${specs}"
    else
      run_cypress_fa "chromium" "${@}"
    fi \
    && popd \
    || return 1
}

main "${@}"
