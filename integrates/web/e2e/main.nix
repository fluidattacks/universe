{ inputs, makeScript, outputs, ... }:
makeScript {
  entrypoint = ./entrypoint.sh;
  name = "integrates-web-e2e";
  searchPaths = {
    bin = [
      inputs.nixpkgs.kubectl
      inputs.nixpkgs.gawk
      inputs.nixpkgs.gnused
      inputs.nixpkgs.nodejs_20
    ];
    source = [
      outputs."/common/utils/aws"
      outputs."/common/utils/sops"
      outputs."/common/utils/cypress-fa"
    ];
  };
}
