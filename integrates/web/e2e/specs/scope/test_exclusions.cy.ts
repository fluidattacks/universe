import {
  aliasMutation,
  assertMutationSuccess,
} from "../../support/grapqh_test_utils";
import { IntegratesUsers, runForUsers } from "../../support/user";

describe("Test scope, gitignore at git roots", () => {
  runForUsers([IntegratesUsers.integratesuser_n9], (user) => {
    beforeEach(() => {
      cy.appVisit(user, undefined, "/orgs/okada/groups/unittesting/scope");
      cy.contains("Git Roots", { timeout: 22000 });
    });

    it(`Test add git root invalid gitignore rule (${user})`, () => {
      cy.contains("Add new root").click();
      cy.contains("Add manually").click();
      cy.get("#modal-close");
      cy.get("input[name='url']").type("https://gitlab.com/fluidattacks/test");
      cy.get('[name="hasExclusions"][type="radio"][value="yes"]')
        .parent()
        .click();
      cy.get("input[name='gitignore.0']").type("bower_components\\test/*");
      cy.contains("Add another").click();
      cy.contains("Use '/' for directories");
    });

    it(`Test update git root invalid gitignore rule (${user})`, () => {
      cy.contains("https://gitlab.com/fluidattacks/universe.git").click();
      cy.get("input[name='gitignore.0']").clear();
      cy.get("input[name='gitignore.0']").type("bower_components\\test/*");
      cy.contains("Add another").click();
      cy.contains("Use '/' for directories");
    });

    it(`Test adding git root with empty gitignore (${user})`, () => {
      cy.contains("Add new root").click();
      cy.contains("Add manually").click();
      cy.get("#modal-close");
      cy.get("input[name='url']").type("https://gitlab.com/fluidattacks/test");
      cy.get("input[name='branch']").type("main");
      cy.get("[data-testid='credentials-id-select-selected-option']").click();
      cy.contains("manager (GitLab OAuth)", { timeout: 22000 }).click();
      cy.get('[name="hasExclusions"][type="radio"][value="yes"]')
        .parent()
        .click();
      cy.get('[name="includesHealthCheck"][type="radio"][value="yes"]')
        .parent()
        .click();
      cy.contains(
        "I accept the additional costs derived from the health check"
      ).click();
      cy.get("#modal-confirm").click();
      cy.contains("Required field");
    });

    it(`Test updating git root with gitignore success(${user})`, () => {
      const rootUrl = "https://gitlab.com/fluidattacks/demo";
      cy.intercept("POST", "/api", (req) => {
        aliasMutation(req, "UpdateGitRoot");
      });
      cy.contains(rootUrl, {
        timeout: 22000,
      }).click();
      cy.get("[data-testid='credentials-id-select-selected-option']").click();
      cy.contains("Test credential", { timeout: 22000 }).click();
      cy.get('[name="hasExclusions"][type="radio"][value="yes"]')
        .parent()
        .click();
      cy.get("input[name='gitignore.0']").type("back/**/test.py");
      cy.contains("Add another").click();
      cy.get("input[name='gitignore.1']").type("bower_components/*");
      cy.get("#modal-confirm").click();
      cy.contains("Do you want to continue?");
      cy.get("#exclusions-confirm").click();

      assertMutationSuccess("UpdateGitRoot");
    });
  });
});
