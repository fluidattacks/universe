import { aliasQuery } from "../../support/grapqh_test_utils";
import { IntegratesUsers, runForUsers } from "../../support/user";

describe("Test group root", () => {
  runForUsers([IntegratesUsers.integratesuser_n6], (user) => {
    beforeEach(() => {
      cy.intercept("POST", "/api", (req) => {
        aliasQuery(req, "GetGitRoots");
      });
    });
    it(`Test scope filter and export roots (${user})`, () => {
      cy.appVisit(user, undefined, "/orgs/okada/groups/unittesting/scope");
      cy.wait("@gqlGetGitRootsQuery", { timeout: 16000 })
        .its("response")
        .then((response) => {
          expect(response?.statusCode).to.eq(200);
        });
      cy.get("#filterBtn", { timeout: 36000 }).click();
      const clearFilterButton = cy.contains("Cancel");
      clearFilterButton.click({ force: true });

      let originalTableLen = 0;
      cy.get("table:visible")
        .first()
        .find("tbody")
        .children()
        .its("length")
        .then((len) => {
          originalTableLen = len;
          cy.log(`Original table length: ${originalTableLen}`);
          // filter branch
          cy.get("input[name='branch']").type("main");
          cy.get("table:visible")
            .first()
            .find("tbody")
            .children()
            .should("not.contain", "master", "develop")
            .and("have.length.below", originalTableLen);
          clearFilterButton.click({ force: true });

          // filter status
          cy.get("[data-testid='cloningStatus-select']").click();
          cy.get("[data-testid='cloningStatus-select-dropdown-options']")
            .find("[value='QUEUED']")
            .click();
          cy.get("table:visible")
            .first()
            .find("tbody")
            .children()
            .should("not.contain", "Ok", "Unknown")
            .and("have.length.below", originalTableLen);
          clearFilterButton.click({ force: true });

          // filter has health check
          cy.get("[data-testid='includesHealthCheck-select']").click();
          cy.get('[data-testid="includesHealthCheck-select-dropdown-options"]')
            .find("[aria-label='false']")
            .click();
          cy.get("table:visible")
            .first()
            .find("tbody")
            .children()
            .should("not.contain", "Medium")
            .and("have.length.below", originalTableLen);
          clearFilterButton.click({ force: true });

          // with clear filter, table should be back to original length
          cy.get("table:visible")
            .first()
            .find("tbody")
            .children()
            .should("have.length", originalTableLen);

          cy.get("#close-filters").click();

          cy.contains("Export").click();
        });
    });
  });
});
