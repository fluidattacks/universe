import { aliasQuery } from "../../support/grapqh_test_utils";
import { IntegratesUsers, runForUsers } from "../../support/user";

const forceRemoveSecret = () => {
  cy.log("Force secret removal");
  const query = `mutation RemoveSecret($groupName: String!, $key: String!,
  $resourceId: ID!, $resourceType: ResourceType!) {
  removeSecret(
    groupName: $groupName
    key: $key
    resourceId: $resourceId
    resourceType: $resourceType
  ) {
    success
    __typename
  }
}`;
  const variables = {
    groupName: "unittesting",
    key: "test_key_1",
    resourceId: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
    resourceType: "ROOT",
  };
  cy.request({
    body: {
      query,
      variables,
    },
    method: "POST",
    url: "/api",
  });
};

describe("Test secrets", () => {
  runForUsers([IntegratesUsers.integratesuser_n6], (user) => {
    beforeEach(() => {
      cy.intercept("POST", "/api", (req) => {
        aliasQuery(req, "GetRoot");
      });
    });
    it(`Test add, update and delete operations for root secret (${user})`, () => {
      cy.appVisit(user, undefined, "/orgs/okada/groups/unittesting/scope");
      cy.contains("Git Roots", { timeout: 22000 });
      const keyName = "test_key_1";
      cy.contains("https://gitlab.com/fluidattacks/universe.git").click();
      cy.get("#secrets").click();
      cy.wait("@gqlGetRootQuery", { timeout: 18000 })
        .its("response")
        .then((response) => {
          expect(response?.statusCode).to.eq(200);
          expect(response?.body.data.root).to.not.be.null;
        });
      cy.contains(keyName).should("not.exist");
      cy.get("#add-secret").click();
      cy.get("[name=key]").type(keyName);
      cy.get("[name=value]").type("1234");
      cy.get("[name=description]")
        .filter(":visible")
        .filter("[aria-label=description]")
        .type("Some test description");
      cy.get("#git-root-add-secret").click();
      cy.contains("Added secret");
      cy.contains(keyName)
        .parentsUntil("tr")
        .last()
        .siblings()
        .find("#edit-secret")
        .click();
      cy.get("[name=value]").clear().type("4321");
      cy.get("[name=description]")
        .filter(":visible")
        .filter("[aria-label=description]")
        .clear()
        .type("Updated test description");
      cy.get("#git-root-add-secret").click();
      cy.contains("Updated secret").wait(1000);
      cy.contains(keyName)
        .parentsUntil("tr")
        .last()
        .siblings()
        .find("#git-root-remove-secret")
        .click();
      cy.get("#modal-confirm").click();
      cy.contains("Removed secret");
    });
    afterEach(() => {
      forceRemoveSecret();
    });
  });
});
