import { IntegratesUsers, runForUsers } from "../../support/user";

describe("API playground", () => {
  runForUsers(
    [IntegratesUsers.integrateshacker_n5, IntegratesUsers.integratesmanager_n7],
    (user) => {
      it(`Test API playground (${user})`, () => {
        cy.bypassLogin(user);
        cy.visit("/api");
        cy.get("#graphiql");
      });
    }
  );
});
