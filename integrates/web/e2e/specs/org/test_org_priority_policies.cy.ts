import {
  aliasMutation,
  assertMutationSuccess,
} from "../../support/grapqh_test_utils";
import { IntegratesUsers, runForUsers } from "../../support/user";

describe("Test org priority policies", () => {
  runForUsers([IntegratesUsers.integratesmanager_n1], (user) => {
    beforeEach(() => {
      cy.intercept("POST", "/api", (req) => {
        aliasMutation(req, "UpdateOrganizationPriorityPolicies");
      });
    });
    it(`Test org priority policies (${user})`, () => {
      cy.appVisit(user, undefined, "/orgs/okada/policies/priority");
      cy.contains("Policy");
      cy.contains("Priority score");
      cy.contains("Add criteria");
      // Add a new policy
      cy.get("#add-priority").click();
      cy.contains("Add priority criteria");
      cy.get("[data-testid='optionsCriteria-select']").click();
      cy.get("[data-testid='optionsCriteria-select-dropdown-options']")
        .find("[value='TECHNIQUE']")
        .click();
      cy.get("[data-testid='criteria.0.policy-select']").click();
      cy.get("[data-testid='criteria.0.policy-select-dropdown-options']")
        .find("[value='DAST']")
        .click();
      cy.get("input[name='criteria.0.value']").clear();
      cy.get("input[name='criteria.0.value']").type("-99");
      cy.get("button[aria-label='minus']").click();
      cy.get("#modal-confirm:enabled").click();
      cy.get("#confirm-update:enabled").click();
      assertMutationSuccess("UpdateOrganizationPriorityPolicies");

      // Remove attempt
      cy.contains("DAST").parentsUntil("tr").siblings().find("button").click();
      cy.get("#modal-confirm:enabled").click();
      cy.contains("Oops!");
      cy.contains(
        "The update for priority scores is currently in progress. Please wait a few minutes before making any changes to the policies."
      );
    });
  });
});
