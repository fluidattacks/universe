import { IntegratesUsers, runForUsers } from "../../support/user";

describe("Test org standard compliance", () => {
  runForUsers([IntegratesUsers.integratesmanager_n1], (user) => {
    it(`Test org standard compliance (${user})`, () => {
      cy.appVisit(user, undefined, "/orgs/okada/compliance");
      cy.contains("Your organization's compliance");
      cy.contains("Standard least complied with");
      cy.contains("Standards").click();
      cy.contains("Unfulfilled standards");
      cy.contains("Requirements");
      cy.contains("Generate report");
    });
  });
});
