import { IntegratesUsers, runForUsers } from "../../support/user";

describe("Test org standard compliance", () => {
  runForUsers([IntegratesUsers.integratesmanager_n1], (user) => {
    it(`Test org standard compliance report generation (${user})`, () => {
      cy.appVisit(user, undefined, "/orgs/okada/compliance");
      cy.get("#standards-tab").click();
      cy.get("#unfulfilled-standard-report").click();
      cy.get("#standard-report").click();
      cy.contains("SMS");
      cy.contains("WhatsApp");
    });
  });
});
