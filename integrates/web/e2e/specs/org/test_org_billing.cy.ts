import { IntegratesUsers, runForUsers } from "../../support/user";

describe("Test org billing", () => {
  runForUsers([IntegratesUsers.integratesmanager_n1], (user) => {
    it(`Test org billing (${user})`, () => {
      cy.appVisit(user, undefined, "/orgs/okada/billing");
      cy.contains("Groups", { timeout: 18000 });
      cy.contains("Group name");
      cy.contains("Total authors");
      cy.contains("Unittesting");
      cy.contains("3");
      cy.contains("Authors");
      cy.contains("Author name");
      cy.contains("Author email");
      cy.contains("Active groups");
      cy.contains("Dev3");
      cy.contains("dev@dev.com");
    });
  });
});
