import {
  aliasMutation,
  assertMutationFailure,
} from "../../support/grapqh_test_utils";
import { IntegratesUsers, runForUsers } from "../../support/user";

describe("Test org outside", () => {
  runForUsers([IntegratesUsers.integratesmanager_n4], (user) => {
    beforeEach(() => {
      cy.intercept("POST", "/api", (req) => {
        aliasMutation(req, "AddGitRoot");
      });
    });
    it(`Test org outside (${user})`, () => {
      const repositoryUrl = "https://gitlab.com/fluidattacks/integration-repo";
      cy.appVisit(user, undefined, "/orgs/okada/outside");
      cy.contains(repositoryUrl).should("be.visible");
      cy.contains(repositoryUrl)
        .parentsUntil("tr")
        .siblings()
        .find("button")
        .click();
      cy.get("[data-testid='groupName-select']").click();
      cy.get("[data-testid='groupName-select-dropdown-options']")
        .last()
        .click();
      cy.get("#modal-confirm").click();
      cy.get("[data-testid='credentials-id-select']").click();
      cy.contains("Test credential").click();
      cy.get("input[value=no]").each((checkbox) =>
        checkbox.parent().trigger("click")
      );
      cy.get("input[name='healthCheckConfirm'][type='checkbox']").each(
        (checkbox) => checkbox.parent().trigger("click")
      );
      cy.get("#modal-confirm").click(); // Will fail but element must be clickeable
      assertMutationFailure("AddGitRoot");
    });
  });
});
