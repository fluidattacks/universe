import { IntegratesUsers, runForUsers } from "../../support/user";

describe("Test org members", () => {
  runForUsers([IntegratesUsers.integratesmanager_n3], (user) => {
    beforeEach(() => Cypress.config("defaultCommandTimeout", 18000));
    it(`Test org members (${user})`, () => {
      cy.appVisit(user, undefined, "/orgs/okada/members");
      const externalEmail = `dev_cypress_${Math.floor(
        Math.random() * (2048 - 9999) + 9999
      )}@dev.com`;
      cy.get("#add-user").click();
      cy.get("input[name='email']").type(externalEmail);
      cy.get("input[name='role']").click();
      cy.get("[data-key='USER']").click();
      cy.get("#modal-confirm").click();
      cy.contains("Confirm invitation")
        .parent()
        .parent()
        .siblings()
        .find("#modal-confirm")
        .click();
      cy.contains(externalEmail);
      const internalEmail = `dev_cypress_${Math.floor(
        Math.random() * (2048 - 9999) + 9999
      )}@fluidattacks.com`;
      cy.get("#add-user").click();
      cy.get("[name='email']").type(internalEmail);
      cy.get("input[name='role']").click();
      cy.get("[data-key='USER']").click();
      cy.get("#modal-confirm").click();
      cy.contains(internalEmail);
    });
  });
});
