import { IntegratesUsers, runForUsers } from "../../support/user";

describe("Test internal surface", () => {
  runForUsers([IntegratesUsers.integrateshacker_n5], (user) => {
    it(`Test internal surface page (${user})`, () => {
      cy.appVisit(user, undefined, "/orgs/okada/groups/unittesting/scope");
      cy.get("#git-root-internal-surface").click();
      cy.get("#modal-confirm").click();
      cy.url().should("include", "/internal/surface/lines");
      cy.contains("LOC");
      cy.contains("Attacked lines");
      cy.contains("universe")
        .parentsUntil("tr")
        .siblings()
        .find("input[type='checkbox']")
        .parent()
        .click();

      cy.get("#verifyToeLines").click();

      cy.contains("Lines has been verified", { timeout: 5000 });

      // Get attacked lines
      cy.contains("universe")
        .parentsUntil("tr")
        .siblings()
        .find("input[type='number']")
        .invoke("val")
        .then((val) => {
          // Check if the attacked lines are the same as LOC after verifying
          cy.get("#tblToeLines tbody tr:first-child td:nth-child(3)")
            .invoke("text")
            .should("eq", String(val));
        });
    });
  });
});
