import {
  aliasMutation,
  assertMutationSuccess,
} from "../../support/grapqh_test_utils";
import { IntegratesUsers, runForUsers } from "../../support/user";

const forceInitRootState = () => {
  cy.log("Force initial root state");
  const query = `mutation UpdateGitRoot(
        $branch: String!,
        $credentials:
        RootCredentialsInput,
        $criticality: RootCriticality!,
        $gitignore: [String!],
        $groupName: String!,
        $id: ID!,
        $includesHealthCheck: Boolean!,
        $nickname: String, $url: String!,
        $useEgress: Boolean!,
        $useVpn: Boolean!,
        $useZtna: Boolean!
      ) {
          updateGitRoot(
            branch: $branch
            credentials: $credentials
            criticality: $criticality
            gitignore: $gitignore
            groupName: $groupName
            id: $id
            includesHealthCheck: $includesHealthCheck
            nickname: $nickname
            url: $url
            useEgress: $useEgress
            useVpn: $useVpn
            useZtna: $useZtna
          ) {
            success
            __typename
          }
      }`;
  const variables = {
    branch: "main",
    credentials: {
      id: "dd2f08da-be59-4f97-8117-00f1cbbf6aec",
    },
    criticality: "LOW",
    gitignore: [],
    groupName: "unittesting",
    id: "765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a",
    includesHealthCheck: false,
    nickname: "integrates_1",
    url: "https://gitlab.com/fluidattacks/demo",
    useEgress: false,
    useVpn: false,
    useZtna: false,
  };
  cy.request({
    body: {
      query,
      variables,
    },
    method: "POST",
    url: "/api",
  });
};

describe("Test group scope edit to ssh", () => {
  runForUsers([IntegratesUsers.continuoushack2_n2], (user) => {
    beforeEach(() => {
      cy.intercept("POST", "/api", (req) => {
        aliasMutation(req, "UpdateGitRoot");
      });
    });
    it(`Test group scope edit to ssh (${user})`, () => {
      const oldRootUrl = "https://gitlab.com/fluidattacks/demo";
      const newRootUrl = "ssh://git@github.com:fluidattacks/test_git_roots.git";
      cy.appVisit(user, undefined, "/orgs/okada/groups/unittesting/scope");
      cy.contains(oldRootUrl).click();
      cy.get("[name='url']").clear().type(newRootUrl).blur();
      cy.get("[data-testid='credentials-id-select-selected-option']").click();
      cy.get("[data-testid='credentials-id-select-dropdown-options']", {
        timeout: 30000,
      })
        .children()
        .eq(1)
        .click();
      cy.get("input[name='healthCheckConfirm'][type='checkbox']").each(
        (checkbox) => checkbox.parent().trigger("click")
      );
      cy.get("#modal-confirm").click();
      cy.get("[data-testid='branch-changed-modal']").should("be.visible");
      cy.get("[data-testid='branch-changed-modal'] #modal-confirm").click();

      // Teardown
      assertMutationSuccess("UpdateGitRoot");
      cy.get("#tblGitRoots :nth-child(4) > .clickable").click();
      cy.get("[name='url']").clear().type(oldRootUrl).blur();
      cy.get("[data-testid='credentials-id-select-selected-option']").click();
      cy.contains("Test credential").click();
      cy.get("input[name='healthCheckConfirm'][type='checkbox']").each(
        (checkbox) => checkbox.parent().trigger("click")
      );
      cy.get("#modal-confirm").click();
      cy.get("[data-testid='branch-changed-modal']").should("be.visible");
      cy.get("[data-testid='branch-changed-modal'] #modal-confirm").click();
      assertMutationSuccess("UpdateGitRoot");
    });
    afterEach(() => {
      forceInitRootState();
    });
  });
});
