import { IntegratesUsers, runForUsers } from "../../support/user";

describe("Test group scope environments", () => {
  let session: string | undefined;
  runForUsers([IntegratesUsers.continuoushack2_n2], (user) => {
    it(`Test group scope environments (${user})`, () => {
      cy.appVisit(user, session, "/orgs/okada/groups/unittesting/scope");
      cy.setLocalStorage("rootTableSet", "{}");
      cy.contains("https://gitlab.com/fluidattacks/universe.git", {
        timeout: 22000,
      }).click();
      cy.get("#environments").click();
      cy.get("#add-env-url").click();
      cy.get("#add-env-url-confirm").should("be.disabled");
      cy.get("input[name='url']").type("https://fluidattacks.com/");
      cy.get("input[name='urlType']").click();
      cy.get("[data-key='URL']").click();
      cy.get("input[type='radio'][value='yes']").check({
        force: true,
      });
      cy.get("#add-env-url-confirm").click();
      cy.contains("Environment added successfully", {
        timeout: 22000,
      });
      cy.get("#modal-close").click();
      cy.contains("https://gitlab.com/fluidattacks/universe.git").click();
      cy.get("#environments").click();
      cy.get("#tblGitRootSecrets")
        .children()
        .contains("https://fluidattacks.com/")
        .parentsUntil("tr")
        .siblings()
        .find("#git-root-remove-environment-url")
        .click();
      cy.get("#modal-confirm").click({ force: true });
    });
  });
});
