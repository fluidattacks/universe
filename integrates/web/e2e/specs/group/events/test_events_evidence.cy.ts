import { IntegratesUsers, runForUsers } from "../../../support/user";

describe("Test group events", () => {
  runForUsers([IntegratesUsers.integratesmanager_n10], (user) => {
    it(`Test group events evidence (${user})`, () => {
      const sourceUrl = `${Cypress.config(
        "baseUrl"
      )}/orgs/okada/groups/unittesting/events/484763304/evidence/unittesting_484763304_evidence_image_1.webm`;
      cy.appVisit(user, undefined, "/orgs/okada/groups/unittesting/events");
      cy.contains("484763304").click();
      cy.get("#evidence-tab").click();
      cy.get("video").should("have.length", 1);
      cy.get("video")
        .children()
        .filter("source")
        .should("have.attr", "src", sourceUrl);
      cy.get("button").contains("Download File").click();
    });
  });
});
