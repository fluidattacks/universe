import { IntegratesUsers, runForUsers } from "../../../support/user";

describe("Test group events verification", () => {
  runForUsers([IntegratesUsers.integratesmanager_n10], (user) => {
    it(`Test group events request verifications(${user})`, () => {
      cy.appVisit(user, undefined, "/orgs/okada/groups/unittesting/events");
      cy.contains("540462638")
        .parentsUntil("tr")
        .siblings()
        .find("input[type=checkbox]")
        .parent()
        .click();
      cy.contains("Request verification").click();
      cy.get("[name=treatmentJustification]").type(
        "This is nothing but a simple test justification"
      );
      cy.get("#modal-confirm").click();
      cy.contains("Verification has been requested");
      cy.contains("540462638").click();
      cy.contains("Reject solution").click();
      cy.get("[name=treatmentJustification]").type(
        "This is a simple rejection test example"
      );
      cy.get("#modal-confirm").click();
      cy.contains("Event solution has been rejected");
    });
  });
});
