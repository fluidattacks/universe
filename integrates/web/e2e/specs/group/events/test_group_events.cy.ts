import { IntegratesUsers, runForUsers } from "../../../support/user";

describe("Test group events", () => {
  runForUsers([IntegratesUsers.integratesmanager_n10], (user) => {
    it(`Test group events (${user})`, () => {
      cy.appVisit(user, undefined, "/orgs/okada/groups/unittesting/events");
      cy.contains("This is an eventuality with evidence").click();
      cy.contains("Authorization for a special attack");
      cy.get("#ColumnsToggleBtn").click();
      cy.contains("Manage columns");
      cy.get("#modal-close").click();
      cy.contains("Manage columns").should("not.exist");
    });
  });
});
