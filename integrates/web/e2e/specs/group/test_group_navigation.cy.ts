import { IntegratesUsers, runForUsers } from "../../support/user";

describe("Test group navigation", () => {
  runForUsers([IntegratesUsers.integratesmanager_n4], (user) => {
    it(`Group navigation (${user})`, () => {
      const clickAndScrollUp = (id: string) => {
        cy.get("#dashboard").scrollTo("top", { ensureScrollable: false });

        cy.get(".header").should("have.class", "visible")

        cy.get(id).click();
      };

      cy.appVisit(user, undefined, "/orgs/okada");
      cy.contains("Unittesting").click();
      clickAndScrollUp("#findingsTab");
      cy.contains("038. Business information leak");
      clickAndScrollUp("#analyticsTab");
      cy.contains("Mean time to remediate (MTTR) benchmark");
      clickAndScrollUp("#forcesTab");
      cy.contains("Click on an execution to see more details");
      clickAndScrollUp("#eventsTab");
      cy.contains("This is an eventuality with evidence");
      clickAndScrollUp("#usersTab");
      cy.contains("User email");
      clickAndScrollUp("#authorsTab");
      cy.contains("Author");
      clickAndScrollUp("#toeTab");
      cy.contains("LOC");
      clickAndScrollUp("#toe-inputs-tab");
      cy.contains("Entry point");
      clickAndScrollUp("#toe-ports-tab");
      cy.contains("Port");
      clickAndScrollUp("#toe-languages-tab");
      cy.contains("Language");
      clickAndScrollUp("#resourcesTab");
      cy.contains("https://gitlab.com/fluidattacks/universe.git");
      cy.contains("https://app.fluidattacks.com/test");
      cy.contains("test.zip");
    });
  });
});
