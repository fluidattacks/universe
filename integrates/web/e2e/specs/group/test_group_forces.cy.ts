import { IntegratesUsers, runForUsers } from "../../support/user";

describe("Test group forces", () => {
  runForUsers([IntegratesUsers.integratesmanager_n10], (user) => {
    it(`Test group forces (${user})`, () => {
      cy.appVisit(user, undefined, "/orgs/okada/groups/unittesting/devsecops");
      cy.get("#tblForcesExecutionsSorting")
        .find("tbody")
        .children()
        .its("length")
        .should("be.gt", 0); // at least 1 row

      cy.get("#tblForcesExecutionsSorting")
        .find("thead > tr")
        .children()
        .should("have.length", 9); // 9 columns

      const expectedColumnsIds = [
        "date",
        "status",
        "breakBuild",
        "Accepted Vulnerabilities",
        "Unmanaged Vulnerabilities",
        "strictness",
        "kind",
        "gitRepo",
        "Identifier",
      ];

      expectedColumnsIds.forEach((columnId) => {
        cy.get(`[id="${columnId}"]`).should("exist");
      });
    });
  });
});
