import {
  aliasMutation,
  assertMutationSuccess,
} from "../../support/grapqh_test_utils";
import { getUserEmail, IntegratesUsers, runForUsers } from "../../support/user";

describe("Test mailmap", () => {
  runForUsers([IntegratesUsers.integratesmanager_n8], (user) => {
    let timestamp: number;
    beforeEach(() => {
      timestamp = new Date().getTime();
      cy.intercept("POST", "/api", (req) => {
        aliasMutation(req, "createMailmapEntry");
        aliasMutation(req, "deleteMailmapEntryWithSubentries");
      });
    });

    it(`Test mailmap (${user})`, () => {
      // Constants to use
      const entryEmail = `"vishy-${timestamp}@entry.com"`;
      const entryName = `"Vishy Entry ${timestamp}"`;
      const checkboxSelector =
        user === getUserEmail(IntegratesUsers.integratesmanager_n8)
          ? ".fa-check"
          : "input[type='checkbox']";

      cy.appVisit(user, undefined, "/orgs/okada/mailmap");
      // Create mailmap entry
      cy.contains("Create author", { timeout: 36000 });
      cy.contains("Create author").click();
      cy.contains("Create mailmap author");
      cy.get("[name='entryEmail']").type(entryEmail);
      cy.get("[name='entryName']").type(entryName);
      cy.get("#modal-confirm").click();
      assertMutationSuccess("createMailmapEntry");
      cy.contains("Success");

      // Delete mailmap entry
      cy.get("#tblMailmap")
        .contains(entryEmail)
        .parentsUntil("tr")
        .siblings()
        .find(checkboxSelector)
        .parent()
        .click();
      cy.contains("Author configuration").click();
      cy.contains("li", "Delete author").click();
      cy.contains("Delete mailmap author");
      cy.get("#modal-confirm").click();
      assertMutationSuccess("deleteMailmapEntryWithSubentries");
      cy.contains("Success");
      cy.contains(entryEmail).should("not.exist");
      cy.contains(entryName).should("not.exist");
    });
  });
});
