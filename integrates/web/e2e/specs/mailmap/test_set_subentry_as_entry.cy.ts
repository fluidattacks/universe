import {
  aliasMutation,
  assertMutationFailure,
  assertMutationSuccess,
} from "../../support/grapqh_test_utils";
import { getUserEmail, IntegratesUsers, runForUsers } from "../../support/user";
import { deleteMailmapEntrySubentries } from "./utils";

describe("Test mailmap", () => {
  runForUsers([IntegratesUsers.integratesmanager_n8], (user) => {
    let timestamp: number;
    let entryEmail: string;
    beforeEach(() => {
      timestamp = new Date().getTime();
      cy.intercept("POST", "/api", (req) => {
        aliasMutation(req, "createMailmapEntry");
        aliasMutation(req, "createMailmapSubentry");
        aliasMutation(req, "updateMailmapEntry");
        aliasMutation(req, "updateMailmapSubentry");
        aliasMutation(req, "setMailmapSubentryAsMailmapEntry");
      });
    });

    it(`Test mailmap (${user})`, () => {
      // Constants to use
      entryEmail = `"gukesh-${timestamp}@entry.com"`;
      const entryName = `"Gukesh" Entry ${timestamp}`;
      const subentryEmail = `gukesh-${timestamp}@subentry.com`;
      const subentryName = `Gukesh "Subentry" ${timestamp}`;
      const checkboxSelector =
        user === getUserEmail(IntegratesUsers.integratesmanager_n8)
          ? ".fa-check"
          : "input[type='checkbox']";

      cy.appVisit(user, undefined, "/orgs/okada/mailmap");
      // Create mailmap entry
      cy.contains("Create author", { timeout: 36000 });
      cy.contains("Create author").click();
      cy.contains("Create mailmap author");
      cy.get("[name='entryEmail']").type(entryEmail);
      cy.get("[name='entryName']").type(entryName);
      cy.get("#modal-confirm").click();
      assertMutationSuccess("createMailmapEntry");
      cy.contains("Success");

      // Test mailmap entry already exists error
      cy.contains("Create author").click();
      cy.contains("Create mailmap author");
      // Create entry with same entry email to raise error
      cy.get("[name='entryEmail']").type(entryEmail);
      // Name could be different since it's not considered
      cy.get("[name='entryName']").type(`${entryName} [NEW]`);
      cy.get("#modal-confirm").click();
      assertMutationFailure("createMailmapEntry");

      // Create mailmap subentry
      cy.get("#tblMailmap")
        .contains(entryEmail)
        .parentsUntil("tr")
        .siblings()
        .find(".fa-angle-down")
        .click();
      cy.contains("Create alias");
      cy.contains("Create alias").click();
      cy.contains("Create mailmap alias");
      cy.get("[name='subentryEmail']").type(subentryEmail);
      cy.get("[name='subentryName']").type(subentryName);
      cy.get("#modal-confirm").click();
      assertMutationSuccess("createMailmapSubentry");
      cy.contains("Success");

      // Update mailmap entry
      cy.get("#tblMailmap")
        .contains(entryEmail)
        .parentsUntil("tr")
        .siblings()
        .find(checkboxSelector)
        .parent()
        .click();
      cy.contains("Author configuration").click();
      cy.contains("li", "Update author").click();
      cy.contains("Update mailmap author");
      cy.get("[name='newEntryEmail']").clear().type(entryEmail);
      cy.get("[name='newEntryName']").clear().type(entryName);
      cy.get("#modal-confirm").click();
      assertMutationSuccess("updateMailmapEntry");
      cy.contains("Success");

      // Update mailmap subentry
      cy.get("div[id^='tblMailmapSubentries']")
        .contains(subentryEmail)
        .parentsUntil("tr")
        .siblings()
        .find(checkboxSelector)
        .parent()
        .click();
      cy.contains("Alias configuration").click();
      cy.contains("li", "Update alias").click();
      cy.contains("Update mailmap alias");
      cy.get("[name='newSubentryEmail']").clear().type(subentryEmail);
      cy.get("[name='newSubentryName']").clear().type(subentryName);
      cy.get("#modal-confirm").click();
      assertMutationSuccess("updateMailmapSubentry");
      cy.contains("Success");

      // Set mailmap subentry as mailmap entry
      cy.get("div[id^='tblMailmapSubentries']")
        .contains(subentryEmail)
        .parentsUntil("tr")
        .siblings()
        .find(checkboxSelector)
        .parent()
        .click();
      cy.contains("Alias configuration").click();
      cy.contains("li", "Set alias as author").click();
      cy.contains("Set mailmap alias as mailmap author");
      cy.get("#modal-confirm").click();
      assertMutationSuccess("setMailmapSubentryAsMailmapEntry");
      cy.contains("Success");
    });
    afterEach(() => deleteMailmapEntrySubentries(entryEmail));
  });
});
