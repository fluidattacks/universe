import {
  aliasMutation,
  assertMutationSuccess,
} from "../../support/grapqh_test_utils";
import { IntegratesUsers, runForUsers } from "../../support/user";
import { deleteMailmapEntry } from "./utils";

describe("Test mailmap", () => {
  runForUsers([IntegratesUsers.integratesmanager_n8], (user) => {
    let timestamp: number;
    let entryEmail: string;
    let entryName: string;

    beforeEach(() => {
      cy.intercept("POST", "/api", (req) => {
        aliasMutation(req, "createMailmapEntry");
      });
      timestamp = new Date().getTime();
      entryEmail = `kasparov-${timestamp}@entry.com`;
      entryName = `Kasparov Entry ${timestamp}`;
    });

    it(`Test mailmap (${user})`, () => {
      // Create mailmap entry
      cy.appVisit(user, undefined, "/orgs/okada/mailmap");
      cy.contains("Create author", { timeout: 36000 });
      cy.contains("Create author").click();
      cy.contains("Create mailmap author");
      cy.get("[name='entryEmail']").type(entryEmail);
      cy.get("[name='entryName']").type(entryName);
      cy.get("#modal-confirm").click();
      assertMutationSuccess("createMailmapEntry");
      cy.contains("Success");
      cy.contains(entryEmail);
      cy.contains(entryName);
    });

    afterEach(() => {
      deleteMailmapEntry(entryEmail);
    });
  });
});
