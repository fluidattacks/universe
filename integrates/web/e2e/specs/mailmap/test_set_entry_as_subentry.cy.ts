import {
  aliasMutation,
  assertMutationSuccess,
} from "../../support/grapqh_test_utils";
import { getUserEmail, IntegratesUsers, runForUsers } from "../../support/user";
import { deleteMailmapEntrySubentries } from "./utils";

describe("Test mailmap", () => {
  runForUsers([IntegratesUsers.integratesmanager_n8], (user) => {
    let timestamp: number;
    let entryEmail: string;
    beforeEach(() => {
      timestamp = new Date().getTime();
      cy.intercept("POST", "/api", (req) => {
        aliasMutation(req, "createMailmapEntry");
        aliasMutation(req, "createMailmapSubentry");
        aliasMutation(req, "deleteMailmapSubentry");
        aliasMutation(req, "setMailmapEntryAsMailmapSubentry");
      });
    });

    it(`Test mailmap (${user})`, () => {
      // Constants to use
      entryEmail = `nakamura-${timestamp}@entry.com`;
      const entryName = `Nakamura Entry ${timestamp}`;
      const subentryEmail = `nakamura-${timestamp}@subentry.com`;
      const subentryName = `Nakamura Subentry ${timestamp}`;
      const checkboxSelector =
        user === getUserEmail(IntegratesUsers.integratesmanager_n8)
          ? ".fa-check"
          : "input[type='checkbox']";

      cy.appVisit(user, undefined, "/orgs/okada/mailmap");
      // Create mailmap entry
      cy.contains("Create author", { timeout: 36000 });
      cy.contains("Create author").click();
      cy.contains("Create mailmap author");
      cy.get("[name='entryEmail']").type(entryEmail);
      cy.get("[name='entryName']").type(entryName);
      cy.get("#modal-confirm").click();
      assertMutationSuccess("createMailmapEntry");
      cy.contains("Success");

      // Create mailmap subentry
      cy.get("#tblMailmap")
        .contains(entryEmail)
        .parentsUntil("tr")
        .siblings()
        .find(".fa-angle-down")
        .click();
      cy.contains("Create alias");
      cy.contains("Create alias").click();
      cy.contains("Create mailmap alias");
      cy.get("[name='subentryEmail']").type(subentryEmail);
      cy.get("[name='subentryName']").type(subentryName);
      cy.get("#modal-confirm").click();
      assertMutationSuccess("createMailmapSubentry");
      cy.contains("Success");

      // Delete mailmap subentry
      cy.get("div[id^='tblMailmapSubentries']")
        .contains(subentryEmail)
        .parentsUntil("tr")
        .siblings()
        .find(checkboxSelector)
        .parent()
        .click();
      cy.contains("Alias configuration").click();
      cy.contains("li", "Delete alias").click();
      cy.contains("Delete mailmap alias");
      cy.get("#modal-confirm").click();
      assertMutationSuccess("deleteMailmapSubentry");
      cy.contains("Success");

      // Set mailmap entry as mailmap subentry
      cy.get("#tblMailmap")
        .contains(entryEmail)
        .parentsUntil("tr")
        .siblings()
        .find(checkboxSelector)
        .parent()
        .click();
      cy.contains("Author configuration").click();
      cy.contains("li", "Set author as alias").click();
      cy.contains("Set mailmap author as mailmap alias");
      cy.get("[name='targetEntryEmail']").clear().type(`wendy@entry.com`);
      cy.get("#modal-confirm").click();
      assertMutationSuccess("setMailmapEntryAsMailmapSubentry");
      cy.contains("Success");
    });
    afterEach(() => deleteMailmapEntrySubentries(entryEmail));
  });
});
