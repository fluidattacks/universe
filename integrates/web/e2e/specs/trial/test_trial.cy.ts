import { IntegratesUsers, runForUsers } from "../../support/user";
import {
  aliasMutation,
  aliasQuery,
  assertMutationSuccess,
} from "../../support/grapqh_test_utils";
import { createStakeholder } from "../../support/stakeholder";

describe("Test trial", () => {
  const creator = IntegratesUsers.integratesmanager_n4;
  runForUsers([creator], (user) => {
    beforeEach(() => {
      cy.intercept("POST", "/api", (req) => {
        aliasQuery(req, "GetRootsAtHome");
        aliasMutation(req, "AddGroup");
      });
    });
    it(`Test trial onboarding (${user})`, () => {
      const stakeholder = createStakeholder(creator);
      cy.log(`Running trial test for stakeholder: ${stakeholder}`);
      cy.appVisit(stakeholder, undefined, "/home");
      cy.contains("Add your repository manually", { timeout: 22000 }).click();
      cy.get("#git-root-add-repo-url").type(
        "https://gitlab.com/fluidattacks/demo"
      );
      cy.get("#branch").type("main");
      cy.get('input[name="credentials.name"]').type("demo");
      cy.get('input[name="credentials.user"]').type("demo");
      cy.get('input[name="credentials.password"]').type("demo");
      cy.contains("No").click();
      cy.contains("Check access").click();
      cy.contains("We have access to test your repository");
      cy.contains("Start scanning").click();

      assertMutationSuccess("AddGroup");

      cy.request({
        body: {
          query: `query {
              me {
                organizations {
                  groups {
                    name
                  }
                }
              }
            }`,
        },
        method: "POST",
        url: "/api",
      }).then((res) => {
        cy.log(res.body);
        cy.wrap(res)
          .its("body.data.me.organizations[0].groups")
          .should("have.length", 1);

        const groupName = res.body.data.me.organizations[0].groups[0].name;

        cy.request({
          body: {
            query: `mutation {
                updateGroupStakeholder(
                  groupName: "${groupName}",
                  email: "${stakeholder}",
                  responsibility: "tester",
                  role: GROUP_MANAGER
                ) {
                  success
                }
              }`,
          },
          method: "POST",
          url: "/api",
        }).then(() => {
          cy.get('[data-testid="welcome-modal"]').should("be.visible");
          cy.get('input[name="phone"]').type("+573202587410");
          cy.get("input[type='radio'][value='DEVELOP_SECURE']").check({
            force: true,
          });
          cy.contains("Continue").click();
          cy.wait("@gqlGetRootsAtHomeQuery")
            .its("response")
            .then((response) => {
              expect(response?.statusCode).to.eq(200);
              expect(response?.body.data.group.roots[0].nickname).to.be.string(
                "demo"
              );
            });
          cy.contains("You will see the vulnerabilities here soon", {
            timeout: 18000,
          });
          [
            "Vulnerabilities",
            "Analytics",
            "DevSecOps",
            "Events",
            "Members",
            "Authors",
            "Surface",
            "Scope",
          ].forEach((tab) => {
            (() => cy.contains(tab))();
          });
          cy.contains("Groups").click();
          [
            "Analytics",
            "Groups",
            "Members",
            "Policies",
            "Billing",
            "Outside",
            "Credentials",
            "Compliance",
          ].forEach((tab) => {
            (() => cy.contains(tab))();
          });
        });
      });
    });
  });
});
