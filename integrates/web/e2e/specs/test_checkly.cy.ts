import { IntegratesUsers, runForUsers } from "../support/user";

describe("Test dev chekly", () => {
  // Run a test identical to the one run by checkly
  runForUsers([IntegratesUsers.integratesadmin_n6], () => {
    it("Test integrates login", () => {
      cy.visit("/");
      cy.title().should("eq", "Login | Fluid Attacks");
    });
  });
});
