import { aliasQuery } from "../../support/grapqh_test_utils";
import { IntegratesUsers, runForUsers } from "../../support/user";

describe("Test register mobile", () => {
  runForUsers([IntegratesUsers.continuoushacking_n3], (user) => {
    beforeEach(() => {
      cy.intercept("POST", "/api", (req) => {
        aliasQuery(req, "GetStakeholderPhoneQuery");
      });
    });
    it(`Test modification of registered mobile for user (${user})`, () => {
      const randomPhone = Math.floor(Math.random() * 10000000000);
      cy.appVisit(user, undefined, "/orgs/okada");
      cy.get("#navbar-user-profile", { timeout: 32000 }).click();
      cy.contains("Mobile").click({ force: true });
      cy.get("[name=phone]").invoke("val").should("not.be.empty");
      cy.wait("@gqlGetStakeholderPhoneQueryQuery", { timeout: 18000 })
        .its("response")
        .then((response) => {
          expect(response?.statusCode).to.eq(200);
        });
      cy.wait(500);
      cy.get("#editPhone").click();
      cy.get("#verify-sms").click();
      cy.contains("A verification code has been sent");
      cy.get("[name=newPhone]").type(randomPhone + "");
      cy.get("[name=verificationCode]").type("0000");
      cy.get("#editPhone").click();
      cy.get("#verify-sms").click();
      cy.contains("A verification code has been sent");
      cy.get("[name=newVerificationCode]").type("0000");
      cy.get("#verifyEditionCode").click();
      cy.contains("Mobile has been updated");
    });
  });
});
