import { IntegratesUsers, runForUsers } from "../../support/user";

describe("Test to do locations drafts", () => {
  runForUsers([IntegratesUsers.integrateshacker_n5], (user) => {
    it(`Test todo locations drafts (${user})`, () => {
      cy.appVisit(user, undefined, "/todos");
      cy.get("#assignedLocations", { timeout: 16000 }).click();
      cy.get("#Reattacks").click();
      cy.contains("038. Business information leak").should("be.visible");
      cy.get("#locationDrafts").click();
      cy.contains("universe/universe/path/to/file3.ext").should("be.visible");
      cy.contains(
        "060. insecure service configuration - host verification"
      ).should("be.visible");
      cy.contains("universe/universe/path/to/file3.ext").click();
      cy.url().should(
        "include",
        "/location-drafts/08717ec8-53a4-409c-aeb3-883b8c0a2d82"
      );
      cy.get("#modal-close").click();
      cy.url().should("include", "/location-drafts");
      cy.get("#tasksEvents").click();
      cy.contains("Testing a new event type").should("be.visible");
      cy.contains("Integrates unit test").should("be.visible");
    });
  });
});
