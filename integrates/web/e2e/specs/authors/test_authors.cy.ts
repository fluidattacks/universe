import { aliasQuery } from "../../support/grapqh_test_utils";
import { IntegratesUsers, runForUsers } from "../../support/user";

describe("Test authors", () => {
  runForUsers([IntegratesUsers.integratesmanager_n10], (user) => {
    beforeEach(() => {
      cy.intercept("POST", "/api", (req) => {
        aliasQuery(req, "GetAuthors");
      });
    });
    it(`Test authors page (${user})`, () => {
      const numberAuthors = 3;
      cy.appVisit(user, undefined, "/orgs/okada/groups/unittesting/authors");
      cy.wait("@gqlGetAuthorsQuery", { timeout: 18000 })
        .its("response")
        .then((response) => {
          expect(response?.statusCode).to.eq(200);
          expect(response?.body.data.group.billing.authors).to.be.have.length(
            numberAuthors
          );
        });
      cy.get("#actor");
      cy.get("#commit");
      cy.get("#tblAuthorsList")
        .find("tbody")
        .children()
        .its("length")
        .should("eq", numberAuthors);
    });
  });
});
