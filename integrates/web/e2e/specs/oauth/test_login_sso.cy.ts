import { IntegratesUsers, runForUsers } from "../../support/user";

describe("User login with OAuth2 playground", (): undefined => {
  runForUsers([IntegratesUsers.integratesuser_n6], () => {
    it("Logs in with Google SSO", (): undefined => {
      cy.clearAllCookies();
      cy.loginByGoogleApi();
      cy.acceptCookiesIfAsked();
    });
  });
});
