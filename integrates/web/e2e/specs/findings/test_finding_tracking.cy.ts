import { IntegratesUsers, runForUsers } from "../../support/user";

describe("Test finding tracking", () => {
  let session: string | undefined;
  runForUsers([IntegratesUsers.integratesmanager_n7], (user) => {
    it(`Test finding tracking (${user})`, () => {
      cy.appVisit(user, session, "/orgs/okada/groups/unittesting/vulns");
      cy.get("#reports");
      cy.contains(
        "060. Insecure service configuration - Host verification"
      ).click();
      cy.get("#trackingItem").click();
      cy.contains("2020-01-03");
    });
  });
});
