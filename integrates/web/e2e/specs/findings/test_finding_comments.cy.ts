import { IntegratesUsers, runForUsers } from "../../support/user";

describe("Test finding comments", () => {
  runForUsers([IntegratesUsers.integratesmanager_n5], (user) => {
    beforeEach(() => {
      Cypress.config("defaultCommandTimeout", 22000);
    });

    it(`Test finding comments (${user})`, () => {
      cy.appVisit(user, undefined, "/orgs/okada/groups/unittesting/vulns");
      cy.contains(
        "060. Insecure service configuration - Host verification"
      ).click();
      cy.get("#commentItem").click();
      cy.contains("This is a commenting test");
      cy.contains("Reply");
      cy.get("[name='comment-editor']");
      cy.visit("/orgs/okada/groups/oneshottest/vulns");
      cy.contains("037. Technical information leak");
      cy.visit(
        "/orgs/okada/groups/oneshottest/vulns/457497318/consulting"
      );
      cy.contains("This is a comment.");
      cy.get("[name='comment-editor']").should("not.exist");
      cy.contains("Reply").should("not.exist");
      cy.visit(
        "/orgs/okada/groups/oneshottest/vulns/457497318/observations"
      );
      cy.get("[name='comment-editor']");
      cy.contains("Reply").click();
      cy.get("button")
        .contains("Reply")
        .siblings()
        .find("textarea")
        .type("This is a reply.");
      cy.contains("Comment").click();
    });
  });
});
