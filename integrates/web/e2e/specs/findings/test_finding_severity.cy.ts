import { IntegratesUsers, runForUsers } from "../../support/user";

describe("Test finding severity", () => {
  let session: string | undefined;
  runForUsers([IntegratesUsers.integratesmanager_n7], (user) => {
    it(`Test finding severity v3.1 (${user})`, () => {
      cy.appVisit(user, session, "/orgs/okada/groups/unittesting/vulns");
      cy.contains(
        "060. Insecure service configuration - Host verification"
      ).click();
      cy.get('input[name="cvssToggle"]').click({ force: true });
      cy.get("#cssv2Item").click();
      cy.contains("Confidentiality impact");
    });
    it(`Test finding severity v4.0 (${user})`, () => {
      cy.appVisit(user, session, "/orgs/okada/groups/unittesting/vulns");
      cy.contains(
        "060. Insecure service configuration - Host verification"
      ).click();
      cy.get("#cssv2Item").click();
      cy.contains("Confidentiality Impact to the Vulnerable System");
    });
  });
});
