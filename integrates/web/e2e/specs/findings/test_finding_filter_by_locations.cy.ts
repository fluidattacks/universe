import { IntegratesUsers, runForUsers } from "../../support/user";

describe("Test finding filter by locations", () => {
  runForUsers([IntegratesUsers.integratesmanager_n7], (user) => {
    beforeEach(() => {
      cy.appVisit(user, undefined, "/orgs/okada/groups/unittesting/vulns");
      Cypress.config("defaultCommandTimeout", 22000);
    });
    it(`Test finding filter by locations (${user})`, () => {
      cy.get("#filterBtn").click();
      cy.contains("Location");
      cy.get("[name='root']").type("http");
      cy.contains("037. Technical information leak");
    });
  });
});
