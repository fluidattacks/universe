export const configureKeyboard = () => {
  Cypress.Keyboard.defaults({
    keystrokeDelay: 3,
  });
};
