import { GeneralEncrypt, importJWK, SignJWT } from "jose";
import { addDays, format } from "date-fns";
import { load } from "js-yaml";

const BASE_URL = "https://gitlab.com/api/v4/projects/20741933/repository/files";
const FILE_REF = "common%2Fcriteria%2Fsrc%2Fvulnerabilities%2Fdata.yaml";
const REF = "trunk";

const generateJWE = async (
  user_email: string,
  firstName?: string,
  lastName?: string
): Promise<string> => {

  const encryptionKey = Cypress.env("JWT_ENCRYPTION_KEY") || Cypress.env("JWT_ENCRYPTION_KEY_DEV");
  const signingKey = Cypress.env("JWT_SECRET_ES512") || Cypress.env("JWT_SECRET_ES512_DEV");

  const encryptionJWK = await importJWK(encryptionKey, "A256GCMKW");
  const singingJWK = await importJWK(signingKey, "ES512");

  const payload = JSON.stringify({
    user_email,
    first_name: firstName ?? "Cypress",
    last_name: lastName ?? "Test",
  });

  const jwe = await new GeneralEncrypt(new TextEncoder().encode(payload))
    .setProtectedHeader({ alg: "A256GCMKW", enc: "A256GCM" })
    .addRecipient(encryptionJWK)
    .encrypt();

  const jwt = await new SignJWT({
    ciphertext: jwe.ciphertext,
    iv: jwe.iv,
    tag: jwe.tag,
    protected: jwe.protected,
    recipients: [{ encrypted_key: jwe.recipients[0].encrypted_key }],
  })
    .setExpirationTime(Math.round(Date.now() / 1000) + 60 * 60 * 5)
    .setSubject("cypress_test")
    .setProtectedHeader({ alg: "ES512" })
    .sign(singingJWK);

  return jwt;
};

const toTypeableDate = (date: Date) => {
  return format(date, "yyyy-MM-dd");
};

const toTypeableDatePlusDays = (date: Date, days: number) => {
  return format(addDays(date, days), "yyyy-MM-dd");
};

class Vuln {
  description?: string;
  public async buildRandom() {
    const url = `${BASE_URL}/${FILE_REF}/raw?ref=${REF}`;
    const requirementsResponseFile: Response = await fetch(url);
    const requirementsYamlFile: string = await requirementsResponseFile.text();

    const data: any = load(requirementsYamlFile);
    const keys = Object.keys(data);
    const max = keys.length;
    const index = this.randomIndex(0, max - 1);
    this.description = `${keys[index]}. ${data[keys[index]]["en"]["title"]}`;
  }
  public async buildFromId(id: string) {
    const url = `${BASE_URL}/${FILE_REF}/raw?ref=${REF}`;
    const requirementsResponseFile: Response = await fetch(url);
    const requirementsYamlFile: string = await requirementsResponseFile.text();

    const data: any = load(requirementsYamlFile);
    this.description = `${id}. ${data[id]["en"]["title"]}`;
  }
  private randomIndex(min: number, max: number) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1) + min);
  }
}

export { generateJWE, toTypeableDate, toTypeableDatePlusDays, Vuln };
