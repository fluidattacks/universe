import { IntegratesUsers, getUserEmail } from "./user";

export const createStakeholder = (
  creator: IntegratesUsers,
  grantOrganizationAccess: boolean = false
): string => {
  const userId = Math.floor(new Date().getTime() / 1000);
  const stakeholderEmail = `cypress${userId}@fluidattacks.com`;
  cy.clearAllCookies();
  cy.log(`Creating ${stakeholderEmail} stakeholder`);
  const firstName = "Cypress";
  const lastName = "User";
  cy.bypassLogin(getUserEmail(creator), firstName, lastName);
  cy.visit("/orgs/kamiya/groups");
  cy.request({
    body: {
      query: `mutation {
          addStakeholder(email: "${stakeholderEmail}", role: ADMIN) {
            success
          }
        }`,
    },
    method: "POST",
    url: "/api",
  });
  if (grantOrganizationAccess) {
    cy.request({
      body: {
        query: `mutation {
            grantStakeholderOrganizationAccess(
              organizationId: "ORG#956e9107-fd8d-49bc-b550-5609a7a1f6ac",
              role: USER,
              userEmail: "${stakeholderEmail}"
            ) {
              success
            }
          }`,
      },
      method: "POST",
      url: "/api",
    });
  }
  cy.contains(firstName).click();
  cy.contains("Log out").trigger("click", { force: true });
  cy.contains("Confirm").click();
  return stakeholderEmail;
};
