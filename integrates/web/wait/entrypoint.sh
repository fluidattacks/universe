# shellcheck shell=bash

function main {
  local cluster="common-k8s"
  local region="us-east-1"

  : \
    && aws_login "dev" "3600" \
    && aws_eks_update_kubeconfig "${cluster}" "${region}" \
    && start_time=$(date +%s) \
    && kubectl rollout status \
      "deploy/integrates-${CI_COMMIT_REF_NAME}" \
      -n "dev" \
      --timeout=0 \
    && end_time=$(date +%s) \
    && info "Time elapsed total: $((end_time - start_time))" \
    || return 1
}

main "${@}"
