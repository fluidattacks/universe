{ projectPath, makeDerivation, inputs, ... }:
makeDerivation {
  env = {
    envConfig = ./config.yaml;
    envYamlSecrets = builtins.map projectPath [
      "/integrates/secrets/dev.yaml"
      "/integrates/secrets/prod.yaml"
    ];
  };
  builder = ./builder.sh;
  name = "integrates-secrets-lint";
  searchPaths.bin =
    [ inputs.nixpkgs.python311Packages.yamllint inputs.nixpkgs.yq ];
}
