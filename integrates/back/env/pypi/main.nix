{ makeTemplate, makePythonEnvironment, projectPath, inputs, ... }:
let
  # Needed to properly cross-compile their native extensions
  self_pycurl = inputs.nixpkgs.python311Packages.pycurl;
  self_python_magic = inputs.nixpkgs.python311Packages.python_magic;

  pythonRequirements = makePythonEnvironment {
    pythonProjectDir = projectPath "/integrates/back";
    pythonVersion = "3.11";
    overrides = _self: super: {
      grimp = super.grimp.overridePythonAttrs (old: {
        preUnpack = ''
          export HOME=$(mktemp -d)
          rm -rf /homeless-shelter
        '' + (old.preUnpack or "");
        buildInputs = [ super.setuptools ];
      });
      matplotlib = super.matplotlib.overridePythonAttrs (old: {
        preUnpack = ''
          export HOME=$(mktemp -d)
          rm -rf /homeless-shelter
        '' + (old.preUnpack or "");
        nativeBuildInputs = [ ];
      });
    };
  };
in makeTemplate {
  name = "integrates-back-pypi-runtime";
  searchPaths = {
    bin = [
      inputs.nixpkgs.gnutar
      inputs.nixpkgs.gzip
      self_pycurl
      self_python_magic
    ];
    pythonPackage = [
      "${self_pycurl}/lib/python3.11/site-packages/"
      "${self_python_magic}/lib/python3.11/site-packages/"
      (projectPath "/integrates/back")
    ];
    source = [ pythonRequirements ];
  };
}
