{ inputs, libGit, makeTemplate, managePorts, projectPath, outputs, ... }:
makeTemplate {
  replace = {
    __argDbDesign__ = projectPath "/integrates/schemas/database-design.json";
    __argIntegrates__ = projectPath "/integrates";
    __argSecretsDev__ = projectPath "/integrates/secrets/dev.yaml";
    __argSecretsProd__ = projectPath "/integrates/secrets/prod.yaml";
    __argManifestFindings__ = projectPath "/skims/manifests/finding_codes.json";
    __argCriteriaCompliance__ =
      projectPath "/common/criteria/src/compliance/data.yaml";
    __argCriteriaRequirements__ =
      projectPath "/common/criteria/src/requirements/data.yaml";
    __argCriteriaVulnerabilities__ =
      projectPath "/common/criteria/src/vulnerabilities/data.yaml";
    __argQueueSize__ = projectPath "/common/compute/arch/sizes/data.yaml";
    __argSrcMachineAssets__ = projectPath "/integrates/back/assets";
    __argSrcTreeSitterParsers__ = outputs."/integrates/back/parsers";
    __argTiktokenDir__ =
      projectPath "/integrates/back/integrates/custom_utils/tiktoken_cache";
  };
  name = "integrates-back-env";
  searchPaths = {
    rpath = [
      # Libmagic
      inputs.nixpkgs.file
      # Required by matplotlib
      inputs.nixpkgs.gcc.cc.lib
    ];
    bin = [
      # The binary for pypi://GitPython
      inputs.nixpkgs.git
      inputs.nixpkgs.git-remote-codecommit
      # The binary for ssh
      inputs.nixpkgs.openssh
      # The binary to zip the data report
      inputs.nixpkgs.p7zip
      # Binaries to fetch latest authz store and model
      inputs.nixpkgs.jq
      inputs.nixpkgs.skopeo
      inputs.nixpkgs.tokei
    ];
    source = [
      libGit
      managePorts
      outputs."/common/utils/aws"
      outputs."/common/utils/sops"
      outputs."/integrates/back/env/pypi"
      outputs."/integrates/back/tools"
      outputs."/integrates/secrets/list"
    ];
  };
  template = ./template.sh;
}
