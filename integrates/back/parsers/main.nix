{ inputs, ... }:
let
  tree-sitter = inputs.nixpkgs.python311Packages.tree-sitter.overridePythonAttrs
    (oldAttrs: {
      src = inputs.nixpkgs.fetchFromGitHub {
        owner = "tree-sitter";
        repo = "py-tree-sitter";
        rev = "refs/tags/v0.21.1";
        sha256 = "sha256-U4ZdU0lxjZO/y0q20bG5CLKipnfpaxzV3AFR6fGS7m4=";
        fetchSubmodules = true;
      };

      dependencies = [ inputs.nixpkgs.python311Packages.setuptools ];
    });

  integratesTreeSitterCSharp = builtins.fetchTarball {
    url =
      "https://github.com/tree-sitter/tree-sitter-c-sharp/archive/31a64b28292aac6adf44071e449fa03fb80eaf4e.tar.gz";
    sha256 = "sha256:13f6rkc9kxrkwmsgr1c2ms8bdqj7zpiqlw3qi1pa9rqn4bl804sq";
  };
  integratesTreeSitterDart = builtins.fetchTarball {
    url =
      "https://github.com/UserNobody14/tree-sitter-dart/archive/f71e310a93010863f4b17a2a501ea8e2032c345b.tar.gz";
    sha256 = "sha256-6iRUtQ1bol0a7whK35MaJ3UKrxmTAzchQ8Yxy4TeerE=";
  };
  integratesTreeSitterGo = builtins.fetchTarball {
    url =
      "https://github.com/tree-sitter/tree-sitter-go/archive/ff86c7f1734873c8c4874ca4dd95603695686d7a.tar.gz";
    sha256 = "sha256-0oXApYf6Ht9xWrWMMaumac1Rqg52GMYW0bAhRbQ8To0=";
  };
  integratesTreeSitterHcl = builtins.fetchTarball {
    url =
      "https://github.com/MichaHoffmann/tree-sitter-hcl/archive/e135399cb31b95fac0760b094556d1d5ce84acf0.tar.gz";
    sha256 = "sha256-Ylxpj+e9YGhyRRRCCVs0g/zhwHpwGELxG2i+E3SFmpQ=";
  };
  integratesTreeSitterJava = builtins.fetchTarball {
    url =
      "https://github.com/tree-sitter/tree-sitter-java/archive/953abfc8bb3eb2f578e1f461edba4a9885f974b8.tar.gz";
    sha256 = "sha256:0f0mi8s0sm9l3hkz23rcfgmdww9lfadj2qmyg44s68skqwzc9sh8";
  };
  integratesTreeSitterJavaScript = builtins.fetchTarball {
    url =
      "https://github.com/tree-sitter/tree-sitter-javascript/archive/a5de24dc7939cb07a758f8d89c089cfdb6f479aa.tar.gz";
    sha256 = "sha256:17nhb7k675b1njzkvs22lx0x2mn74v7x6r4b0mpacnkxyzs5iiwf";
  };
  integratesTreeSitterJson = builtins.fetchTarball {
    url =
      "https://github.com/tree-sitter/tree-sitter-json/archive/3fef30de8aee74600f25ec2e319b62a1a870d51e.tar.gz";
    sha256 = "sha256-Msnct7JzPBIR9+PIBZCJTRdVMUzhaDTKkl3JaDUKAgo=";
  };
  integratesTreeSitterKotlin = builtins.fetchTarball {
    url =
      "https://github.com/fwcd/tree-sitter-kotlin/archive/0ef87892401bb01c84b40916e1f150197bc134b1.tar.gz";
    sha256 = "sha256-A48osbWX8rkM5UlHcSX3t7PJ97IhsNe+JUTB4fnkDAE=";
  };
  integratesTreeSitterPhp = builtins.fetchTarball {
    url =
      "https://github.com/tree-sitter/tree-sitter-php/archive/0a99deca13c4af1fb9adcb03c958bfc9f4c740a9.tar.gz";
    sha256 = "sha256-MfCws6WvaYJPoxoSdk1OUkqfVGCNtfMDTyndSZpABqI=";
  };
  integratesTreeSitterPython = builtins.fetchTarball {
    url =
      "https://github.com/tree-sitter/tree-sitter-python/archive/62827156d01c74dc1538266344e788da74536b8a.tar.gz";
    sha256 = "sha256-hVtX4Dyqrq+cSvKTmKMxLbAplcCdR8dfFDoIZNtPFA0=";
  };
  integratesTreeSitterRuby = builtins.fetchTarball {
    url =
      "https://github.com/tree-sitter/tree-sitter-ruby/archive/f257f3f57833d584050336921773738a3fd8ca22.tar.gz";
    sha256 = "sha256-0EaU9O67faGwtO1GIxjK4Uv1etd0p1vtfrVB3d6TDF8=";
  };
  integratesTreeSitterScala = builtins.fetchTarball {
    url =
      "https://github.com/tree-sitter/tree-sitter-scala/archive/1b4c2fa5c55c5fd83cbb0d2f818f916aba221a42.tar.gz";
    sha256 = "sha256-93uWT5KMqCUwntdL5U2Vc71ci+uP3OdP9y6kVZ3bYLo=";
  };
  integratesTreeSitterSwift = builtins.fetchTarball {
    url =
      "https://github.com/alex-pinkus/tree-sitter-swift/archive/d1a9c50c3ebe025548ee042119d5e1f5b0cbf4c3.tar.gz";
    sha256 = "sha256-DeeW2arTw7W2bvx0Y0O6sH734LoKJQlRs2K36qHBjRM=";
  };
  integratesTreeSitterTypeScript = builtins.fetchTarball {
    url =
      "https://github.com/tree-sitter/tree-sitter-typescript/archive/9f804be960f289a0acc59d8564acc16857b0b088.tar.gz";
    sha256 = "sha256:18cw7rmfkbl7wg0apw23nkj3hi1khikas3v27ipjh13bhrmjy9wf";
  };
  integratesTreeSitterYaml = builtins.fetchTarball {
    url =
      "https://github.com/tree-sitter-grammars/tree-sitter-yaml/archive/3975596d84650e8d8ab8ef3128b1b7f0fec324f8.tar.gz";
    sha256 = "1ydh8wjnc6qm22bg5l1bprgqbw17wp8xfk1j6dhb4rlqmais2fm3";
  };
in inputs.nixpkgs.stdenv.mkDerivation {
  buildPhase = ''
    export envTreeSitterCSharp="${integratesTreeSitterCSharp}"
    export envTreeSitterDart="${integratesTreeSitterDart}"
    export envTreeSitterGo="${integratesTreeSitterGo}"
    export envTreeSitterHcl="${integratesTreeSitterHcl}"
    export envTreeSitterJava="${integratesTreeSitterJava}"
    export envTreeSitterJavaScript="${integratesTreeSitterJavaScript}"
    export envTreeSitterJson="${integratesTreeSitterJson}"
    export envTreeSitterKotlin="${integratesTreeSitterKotlin}"
    export envTreeSitterPhp="${integratesTreeSitterPhp}"
    export envTreeSitterPython="${integratesTreeSitterPython}"
    export envTreeSitterRuby="${integratesTreeSitterRuby}"
    export envTreeSitterScala="${integratesTreeSitterScala}"
    export envTreeSitterSwift="${integratesTreeSitterSwift}"
    export envTreeSitterTypeScript="${integratesTreeSitterTypeScript}"
    export envTreeSitterYaml="${integratesTreeSitterYaml}"

    python build.py
  '';
  name = "integrates-custom-fix-parsers";
  nativeBuildInputs = [ tree-sitter ];
  src = ./.;
}
