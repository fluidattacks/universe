{ makeRubyGemsEnvironment, projectPath, ... }:
makeRubyGemsEnvironment {
  name = "integrates-tools-concurrent-ruby";
  ruby = "3.1";
  sourcesYaml =
    projectPath "/integrates/back/tools/concurrent-ruby/sources.yaml";
}
