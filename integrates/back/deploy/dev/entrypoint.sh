# shellcheck shell=bash

function main {
  local cluster="common-k8s"
  local region="us-east-1"
  export B64_CACHIX_AUTH_TOKEN
  export B64_CI_COMMIT_REF_NAME
  export B64_CI_COMMIT_SHA
  export B64_GITLAB_USER_EMAIL
  export UUID
  export DB_JOB

  if ! test -n "${CI-}"; then
    CI_COMMIT_REF_NAME="$(git rev-parse --abbrev-ref HEAD)"
    CI_COMMIT_SHA="$(git rev-parse HEAD)"
    CACHIX_AUTH_TOKEN="12345"
    GITLAB_USER_EMAIL="${OKTA_EMAIL}"
    info "Ephemeral environment $(echo "${GITLAB_USER_EMAIL}" | grep -oE "^\w+") will be deployed from a local execution"
  fi

  : \
    && aws_login "dev" "3600" \
    && aws_eks_update_kubeconfig "${cluster}" "${region}" \
    && sops_export_vars common/secrets/dev.yaml \
      CACHIX_AUTH_TOKEN \
    && B64_CACHIX_AUTH_TOKEN="$(b64 "${CACHIX_AUTH_TOKEN}")" \
    && B64_CI_COMMIT_REF_NAME="$(b64 "${CI_COMMIT_REF_NAME}")" \
    && B64_CI_COMMIT_SHA="$(b64 "${CI_COMMIT_SHA}")" \
    && B64_GITLAB_USER_EMAIL="$(b64 "${GITLAB_USER_EMAIL}")" \
    && UUID="$(uuidgen)" \
    && for manifest in __argManifests__/*; do
      apply_manifest "${manifest}"
    done
}

main "${@}"
