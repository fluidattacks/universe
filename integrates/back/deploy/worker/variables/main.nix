{ libGit, makeTemplate, outputs, ... }:
makeTemplate {
  name = "integrates-deploy-worker-export-repo-variables";
  searchPaths = {
    source =
      [ libGit outputs."/common/utils/aws" outputs."/common/utils/sops" ];
  };
  template = ./template.sh;
}
