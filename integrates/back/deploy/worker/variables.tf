variable "cluster_name" {
  default = "common-k8s"
  type    = string
}
variable "cluster_ca_certificate" {
  type = string
}
variable "cluster_endpoint" {
  type = string
}
variable "ci_commit_sha" {
  type = string
}

variable "deployment_name" {
  default = "trunk"
  type    = string
}
