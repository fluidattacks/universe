{ makeScript, outputs, ... }: {
  jobs."/integrates/back/test" = makeScript {
    entrypoint = ./entrypoint.sh;
    name = "integrates-back-test";
    replace.__argIntegratesBackEnv__ = outputs."/integrates/back/env";
    searchPaths.source = [ outputs."/integrates/back/env/pypi" ];
  };
}
