# shellcheck shell=bash

function main {
  local module="${1}"
  local target="integrates/back/integrates/${module}"
  local tests=("${@:2}")

  source __argIntegratesBackEnv__/template "dev"
  export PYTHONPATH="integrates/back:${PYTHONPATH}"
  export COVERAGE_FILE="integrates/back/.coverage.${module}"
  export INTEGRATES_MOTO_TEST="true"

  integrates-back-test --target "${target}" --tests "${tests[@]}"
}

main "${@}"
