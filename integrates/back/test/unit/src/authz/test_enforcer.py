from datetime import (
    datetime,
)
from decimal import (
    Decimal,
)
from integrates import (
    authz,
)
from integrates.db_model.groups.enums import (
    GroupLanguage,
    GroupManaged,
    GroupService,
    GroupStateStatus,
    GroupSubscriptionType,
    GroupTier,
)
from integrates.db_model.groups.types import (
    Group,
    GroupState,
)
from integrates.db_model.types import (
    Policies,
)
import pytest
from test.unit.src.utils import (
    get_module_at_test,
)

# Constants
pytestmark = [
    pytest.mark.asyncio,
]

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"
TABLE_NAME = "integrates_vms"


@pytest.mark.parametrize(
    ["group", "attributes", "results"],
    [
        [
            Group(
                business_name="Testing Company and Sons",
                policies=Policies(
                    max_number_acceptances=3,
                    min_acceptance_severity=Decimal("0"),
                    vulnerability_grace_period=10,
                    modified_by="integratesmanager@gmail.com",
                    min_breaking_severity=Decimal("3.9"),
                    max_acceptance_days=90,
                    modified_date=datetime.fromisoformat(
                        "2021-11-22T20:07:57+00:00"
                    ),
                    max_acceptance_severity=Decimal("3.9"),
                ),
                context="Group context test",
                disambiguation="Disambiguation test",
                description="Integrates unit test group",
                language=GroupLanguage.EN,
                created_by="integratesmanager@gmail.com",
                organization_id="38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
                name="unittesting",
                created_date=datetime.fromisoformat(
                    "2018-03-08T00:43:18+00:00"
                ),
                state=GroupState(
                    has_essential=True,
                    has_advanced=True,
                    managed=GroupManaged.NOT_MANAGED,
                    modified_by="integratesmanager@gmail.com",
                    modified_date=datetime.fromisoformat(
                        "2018-03-08T00:43:18+00:00"
                    ),
                    status=GroupStateStatus.ACTIVE,
                    tier=GroupTier.ESSENTIAL,
                    type=GroupSubscriptionType.CONTINUOUS,
                    tags=set(("test-groups", "test-updates", "test-tag")),
                    service=GroupService.WHITE,
                ),
                business_id="14441323",
                sprint_duration=2,
            ),
            [
                "can_report_vulnerabilities",
                "can_request_zero_risk",
                "has_advanced",
                "has_asm",
                "has_forces",
                "has_service_black",
                "has_service_white",
                "is_continuous",
                "is_fluidattacks_customer",
                "is_under_review",
                "must_only_have_fluidattacks_hackers",
                "non_existing_attribute",
            ],
            [
                True,
                True,
                True,
                True,
                True,
                False,
                True,
                True,
                True,
                False,
                True,
                False,
            ],
        ],
        [
            Group(
                business_name="Testing Company and Sons",
                policies=Policies(
                    max_number_acceptances=3,
                    min_acceptance_severity=Decimal("0"),
                    vulnerability_grace_period=10,
                    modified_by="integratesmanager@gmail.com",
                    min_breaking_severity=Decimal("3.9"),
                    max_acceptance_days=90,
                    modified_date=datetime.fromisoformat(
                        "2021-11-22T20:07:57+00:00"
                    ),
                    max_acceptance_severity=Decimal("3.9"),
                ),
                context="Group context test",
                disambiguation="Disambiguation test",
                description="Oneshottest test group",
                language=GroupLanguage.EN,
                created_by="integratesmanager@gmail.com",
                organization_id="38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
                name="oneshottest",
                created_date=datetime.fromisoformat(
                    "2019-01-20T22:00:00+00:00"
                ),
                state=GroupState(
                    has_essential=True,
                    has_advanced=False,
                    managed=GroupManaged.NOT_MANAGED,
                    modified_by="integratesmanager@gmail.com",
                    modified_date=datetime.fromisoformat(
                        "2019-01-20T22:00:00+00:00"
                    ),
                    status=GroupStateStatus.ACTIVE,
                    tier=GroupTier.ONESHOT,
                    type=GroupSubscriptionType.ONESHOT,
                    tags=set(("test-tag")),
                    service=GroupService.BLACK,
                ),
                business_id="14441323",
                sprint_duration=2,
            ),
            [
                "can_report_vulnerabilities",
                "can_request_zero_risk",
                "has_advanced",
                "has_asm",
                "has_forces",
                "has_service_black",
                "has_service_white",
                "is_continuous",
                "is_fluidattacks_customer",
                "is_under_review",
                "must_only_have_fluidattacks_hackers",
                "non_existing_attribute",
            ],
            [
                True,
                True,
                False,
                True,
                False,
                True,
                False,
                False,
                True,
                False,
                True,
                False,
            ],
        ],
    ],
)
async def test_group_service_attributes_enforcer(
    group: Group,
    attributes: list,
    results: list,
) -> None:
    # All attributes must be tested for this test to succeed
    # This prevents someone to add a new attribute without testing it

    attributes_remaining_to_test: set[str] = {
        (attr)
        for attrs in authz.SERVICE_ATTRIBUTES.values()
        for attr in set(attrs).union({"non_existing_attribute"})
    }

    enforcer = authz.get_group_service_attributes_enforcer(group)

    for attribute, result in zip(attributes, results):
        assert (
            enforcer(attribute) == result
        ), f"{group.name} attribute: {attribute}, should have value {result}"

        attributes_remaining_to_test.remove(attribute)

    assert not attributes_remaining_to_test, (
        f"Please add tests for the following pairs of (group, attribute)"
        f": {attributes_remaining_to_test}"
    )
