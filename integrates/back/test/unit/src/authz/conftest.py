from .mocked_data import (
    MOCK_GROUP_ACCESS,
    MOCK_ORGANIZATION_ACCESS,
    MOCK_USERS_ROLES,
)
from collections.abc import (
    Callable,
)
from integrates.authz import (
    get_group_level_actions_by_role,
    get_organization_level_actions_by_role,
    get_user_level_actions_by_role,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.group_access.types import (
    GroupAccess,
)
from integrates.db_model.organization_access.types import (
    OrganizationAccess,
)
import pytest

pytestmark = [
    pytest.mark.asyncio,
]


def _mock_get_user_level_role(email: str) -> str:
    return MOCK_USERS_ROLES.get(email, "")


def _mock_loaders_stakeholder_groups_access_load(
    email: str,
) -> list[GroupAccess]:
    return MOCK_GROUP_ACCESS.get(email, [])


def mock_loaders_stakeholder_organizations_access_load(
    email: str,
) -> list[OrganizationAccess]:
    return MOCK_ORGANIZATION_ACCESS.get(email, [])


def _is_regular_user_with_group_policy(
    access: GroupAccess,
    action: str,
    group_name_to_test: str,
) -> bool:
    return (
        group_name_to_test == access.group_name
        and access.state.role is not None
        and action in get_group_level_actions_by_role(access.state.role)
    )


def _is_regular_user_with_organization_policy(
    access: OrganizationAccess,
    action: str,
    organization_id_to_test: str,
) -> bool:
    return (
        organization_id_to_test == access.organization_id
        and access.state.role is not None
        and action in get_organization_level_actions_by_role(access.state.role)
    )


def _is_admin_group_level(
    user_level_role: str,
    action: str,
) -> bool:
    return (
        user_level_role == "admin"
        and action in get_group_level_actions_by_role("admin")
    )


def _is_admin_organization_level(
    user_level_role: str,
    action: str,
) -> bool:
    return (
        user_level_role == "admin"
        and action in get_organization_level_actions_by_role("admin")
    )


@pytest.fixture
def side_effect_get_group_level_enforcer() -> (
    Callable[[Dataloaders, str], Callable[[str, str], bool]]
):
    def _mock_get_group_level_enforcer(
        loaders: Dataloaders,
        email: str,
    ) -> Callable[[str, str], bool]:
        """Return a filtered group-level authorization
        for the provided email."""
        if loaders and email:
            groups_access = _mock_loaders_stakeholder_groups_access_load(email)
            user_level_role = _mock_get_user_level_role(email)

        def enforcer(group_name_to_test: str, action: str) -> bool:
            return any(
                map(
                    lambda access: _is_regular_user_with_group_policy(
                        access, action, group_name_to_test
                    ),
                    groups_access,
                )
            ) or _is_admin_group_level(user_level_role, action)

        return enforcer

    return _mock_get_group_level_enforcer


@pytest.fixture
def side_effect_get_organization_level_enforcer() -> (
    Callable[[Dataloaders, str], Callable[[str, str], bool]]
):
    def _mock_side_effect_get_organization_level_enforcer(
        loaders: Dataloaders,
        email: str,
    ) -> Callable[[str, str], bool]:
        """
        Return a filtered organization-level authorization
        for the provided email.
        """
        if loaders and email:
            orgs_access = mock_loaders_stakeholder_organizations_access_load(
                email
            )
            user_level_role = _mock_get_user_level_role(email)

        def enforcer(organization_id_to_test: str, action: str) -> bool:
            return any(
                map(
                    lambda access: _is_regular_user_with_organization_policy(
                        access, action, organization_id_to_test
                    ),
                    orgs_access,
                )
            ) or _is_admin_organization_level(user_level_role, action)

        return enforcer

    return _mock_side_effect_get_organization_level_enforcer


@pytest.fixture
def side_effect_get_user_level_enforcer() -> (
    Callable[[Dataloaders, str], Callable[[str], bool]]
):
    def _mock_get_user_level_enforcer(
        loaders: Dataloaders,
        email: str,
    ) -> Callable[[str], bool]:
        """Return a filtered group-level authorization
        for the provided email."""
        if loaders and email:
            user_level_role = _mock_get_user_level_role(email)

        def enforcer(action: str) -> bool:
            return bool(
                user_level_role
                and action in get_user_level_actions_by_role(user_level_role)
            )

        return enforcer

    return _mock_get_user_level_enforcer
