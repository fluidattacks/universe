from datetime import (
    datetime,
)
from integrates.db_model.group_access.types import (
    GroupAccess,
    GroupAccessState,
)
from integrates.db_model.organization_access.types import (
    OrganizationAccess,
    OrganizationAccessState,
)

MOCK_GROUP_ACCESS = {
    "continuoushacking@gmail.com": [
        GroupAccess(
            email="continuoushacking@gmail.com",
            group_name="oneshottest",
            state=GroupAccessState(
                modified_date=None,
                confirm_deletion=None,
                has_access=True,
                invitation=None,
                responsibility=None,
                role="group_manager",
            ),
            expiration_time=None,
        ),
        GroupAccess(
            email="continuoushacking@gmail.com",
            group_name="unittesting",
            state=GroupAccessState(
                modified_date=None,
                confirm_deletion=None,
                has_access=True,
                invitation=None,
                responsibility=None,
                role="group_manager",
            ),
            expiration_time=None,
        ),
    ],
    "integrateshacker@fluidattacks.com": [
        GroupAccess(
            email="integrateshacker@fluidattacks.com",
            group_name="oneshottest",
            state=GroupAccessState(
                modified_date=None,
                confirm_deletion=None,
                has_access=True,
                invitation=None,
                responsibility=None,
                role="reattacker",
            ),
            expiration_time=None,
        ),
        GroupAccess(
            email="integrateshacker@fluidattacks.com",
            group_name="unittesting",
            state=GroupAccessState(
                modified_date=None,
                confirm_deletion=None,
                has_access=True,
                invitation=None,
                responsibility=None,
                role="hacker",
            ),
            expiration_time=None,
        ),
    ],
    "integratesuser@gmail.com": [
        GroupAccess(
            email="integratesuser@gmail.com",
            group_name="oneshottest",
            state=GroupAccessState(
                modified_date=None,
                confirm_deletion=None,
                has_access=True,
                invitation=None,
                responsibility=None,
                role="user",
            ),
            expiration_time=None,
        ),
        GroupAccess(
            email="integratesuser@gmail.com",
            group_name="unittesting",
            state=GroupAccessState(
                modified_date=None,
                confirm_deletion=None,
                has_access=True,
                invitation=None,
                responsibility=None,
                role="group_manager",
            ),
            expiration_time=None,
        ),
    ],
    "unittest@fluidattacks.com": [
        GroupAccess(
            email="unittest@fluidattacks.com",
            group_name="unittesting",
            state=GroupAccessState(
                modified_date=datetime.fromisoformat(
                    "2020-01-01T20:07:57+00:00"
                ),
                confirm_deletion=None,
                has_access=True,
                invitation=None,
                responsibility="Tester",
                role=None,
            ),
            expiration_time=None,
        )
    ],
}

MOCK_ORGANIZATION_ACCESS = {
    "org_testgroupmanager1@gmail.com": [
        OrganizationAccess(
            organization_id="ORG#f2e2777d-a168-4bea-93cd-d79142b294d2",
            email="org_testgroupmanager1@gmail.com",
            expiration_time=None,
            state=OrganizationAccessState(
                modified_date=datetime.fromisoformat(
                    "2019-11-22T20:07:57+00:00"
                ),
                modified_by="fluidattacks@test.com",
                has_access=None,
                invitation=None,
                role="customer_manager",
            ),
        )
    ],
    "unittest2@fluidattacks.com": [
        OrganizationAccess(
            organization_id="ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
            email="unittest2@fluidattacks.com",
            expiration_time=None,
            state=OrganizationAccessState(
                modified_date=datetime.fromisoformat(
                    "2019-11-22T20:07:57+00:00"
                ),
                modified_by="fluidattacks@test.com",
                has_access=None,
                invitation=None,
                role="customer_manager",
            ),
        )
    ],
}

MOCK_USERS_ROLES: dict[str, str] = {
    "continuoushacking@gmail.com": "hacker",
    "integrateshacker@fluidattacks.com": "hacker",
    "integratesuser@gmail.com": "user",
    "integratesuser2@gmail.com": "user",
    "org_testgroupmanager1@gmail.com": "",
    "unittest@fluidattacks.com": "admin",
    "unittest2@fluidattacks.com": "hacker",
}
