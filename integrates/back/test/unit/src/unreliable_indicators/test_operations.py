from datetime import (
    datetime,
)
from decimal import (
    Decimal,
)
from freezegun.api import (
    freeze_time,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.findings.enums import (
    FindingStatus,
)
from integrates.db_model.findings.types import (
    FindingTreatmentSummary,
    FindingUnreliableIndicators,
    FindingVerificationSummary,
    FindingVulnerabilitiesSummary,
    FindingZeroRiskSummary,
)
from integrates.db_model.roots.enums import (
    RootCriticality,
)
from integrates.db_model.vulnerabilities.types import (
    VulnerabilityUnreliableIndicators,
)
from integrates.unreliable_indicators.enums import (
    EntityDependency,
)
from integrates.unreliable_indicators.operations import (
    update_unreliable_indicators_by_deps,
)
import pytest

# Constants
pytestmark = [
    pytest.mark.asyncio,
]


@freeze_time("2020-12-01")
async def test_update_unreliable_indicators_by_deps() -> None:
    loaders = get_new_context()
    finding_id = "422286126"
    vulnerability_id = "15375781-31f2-4953-ac77-f31134225747"
    await update_unreliable_indicators_by_deps(
        EntityDependency.upload_file,
        finding_ids=[finding_id],
        vulnerability_ids=[vulnerability_id],
    )
    expected_finding_output = FindingUnreliableIndicators(
        unreliable_newest_vulnerability_report_date=(
            datetime.fromisoformat("2020-01-03T17:46:10+00:00")
        ),
        unreliable_oldest_open_vulnerability_report_date=(
            datetime.fromisoformat("2020-01-03T17:46:10+00:00")
        ),
        unreliable_oldest_vulnerability_report_date=(
            datetime.fromisoformat("2020-01-03T17:46:10+00:00")
        ),
        unreliable_status=FindingStatus.VULNERABLE,
        unreliable_total_open_cvssf=Decimal("3.482"),
        unreliable_where="test/data/lib_path/f060/csharp.cs",
        unreliable_zero_risk_summary=FindingZeroRiskSummary(
            confirmed=1, rejected=0, requested=0
        ),
        submitted_vulnerabilities=1,
        rejected_vulnerabilities=1,
        max_open_severity_score=Decimal("4.9"),
        max_open_severity_score_v4=Decimal("2.1"),
        max_open_root_criticality=RootCriticality.MEDIUM,
        newest_vulnerability_report_date=(
            datetime.fromisoformat("2020-01-03T17:46:10+00:00")
        ),
        oldest_vulnerability_report_date=(
            datetime.fromisoformat("2020-01-03T17:46:10+00:00")
        ),
        total_open_cvssf=Decimal("3.482"),
        total_open_cvssf_v4=Decimal("0.072"),
        total_open_priority=Decimal(250),
        treatment_summary=FindingTreatmentSummary(
            accepted=0,
            accepted_undefined=0,
            in_progress=1,
            untreated=0,
        ),
        verification_summary=FindingVerificationSummary(
            requested=0,
            on_hold=0,
            verified=0,
        ),
        vulnerabilities_summary=FindingVulnerabilitiesSummary(
            closed=0,
            open=1,
            submitted=1,
            rejected=1,
            open_critical=0,
            open_high=0,
            open_low=1,
            open_medium=0,
            open_critical_v3=0,
            open_high_v3=0,
            open_low_v3=0,
            open_medium_v3=1,
        ),
    )
    finding = await loaders.finding.load(finding_id)
    assert finding
    assert finding.unreliable_indicators == expected_finding_output
    vulnerability = await loaders.vulnerability.load(vulnerability_id)
    assert vulnerability
    expected_vulnerability_output = VulnerabilityUnreliableIndicators(
        unreliable_efficacy=Decimal("0"),
        unreliable_last_reattack_date=datetime.fromisoformat(
            "2020-02-19T15:41:04+00:00"
        ),
        unreliable_last_reattack_requester="integratesuser@gmail.com",
        unreliable_last_requested_reattack_date=datetime.fromisoformat(
            "2020-02-18T15:41:04+00:00"
        ),
        unreliable_priority=Decimal("263.60"),
        unreliable_report_date=datetime.fromisoformat(
            "2019-09-13T13:17:41+00:00"
        ),
        unreliable_reattack_cycles=1,
        unreliable_treatment_changes=0,
    )
    assert vulnerability.unreliable_indicators == expected_vulnerability_output
