from integrates.telemetry.instrumentation import (
    check_span_http_host,
    FilteringExporter,
    span_filter,
)
from opentelemetry.sdk.trace import (
    ReadableSpan,
)
from opentelemetry.sdk.trace.export import (
    SpanExportResult,
)
import pytest
from pytest import (
    FixtureRequest,
)
from unittest.mock import (
    Mock,
)


@pytest.fixture
def mock_span() -> Mock:
    span = Mock(spec=ReadableSpan)
    span.context = Mock()
    span.context.trace_id = 123456
    span.attributes = {}
    return span


class TestInstrumentation:
    def test_filtering_exporter(self) -> None:
        mock_exporter = Mock()
        mock_exporter.export.return_value = SpanExportResult.SUCCESS

        def mock_filter(span: ReadableSpan) -> bool:
            attributes = span.attributes or {}
            return bool(attributes.get("include", False))

        filtering_exporter = FilteringExporter(mock_exporter, mock_filter)

        span1 = Mock(spec=ReadableSpan)
        span1.attributes = {"include": True}
        span2 = Mock(spec=ReadableSpan)
        span2.attributes = {"include": False}

        result = filtering_exporter.export([span1, span2])

        assert result == SpanExportResult.SUCCESS
        mock_exporter.export.assert_called_once_with([span1])

    def test_check_span_http_host(self, request: FixtureRequest) -> None:
        span = request.getfixturevalue("mock_span")
        span.attributes = {"http.host": "example.com"}
        assert check_span_http_host(span) is True

        span.attributes = {"http.host": "localhost:3000"}
        assert check_span_http_host(span) is False

        span.attributes = {"http.host": "127.0.0.1:8001"}
        assert check_span_http_host(span) is False

        span.attributes = {"http.host": "localhost:8001"}
        assert check_span_http_host(span) is False

        span.attributes = {
            "location_href": "https://test.app.fluidattacks.com"
        }
        assert check_span_http_host(span) is False

        span.attributes = {"http.host": "test.app.fluidattacks.com"}
        assert check_span_http_host(span) is False

    def test_span_filter(self, request: FixtureRequest) -> None:
        span = request.getfixturevalue("mock_span")
        span.attributes = {"user_id": "machine@fluidattacks.com"}
        assert span_filter(span) is False

        span.attributes = {"user_id": "status@fluidattacks.com"}
        assert span_filter(span) is False

        span.attributes = {"http.user_agent": "ELB-HealthChecker/2.0"}
        assert span_filter(span) is False

        span.attributes = {
            "http.user_agent": "Some-Agent LogRocket-AssetCacher"
        }
        assert span_filter(span) is False
