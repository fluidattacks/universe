from ariadne import (
    make_executable_schema,
    QueryType,
)
import pytest
from typing import (
    Any,
)


@pytest.fixture(name="type_defs")
def type_defs_fixture() -> str:
    return """
        scalar Upload

        type Query {
            testResolver(identifier: String): Entity
            status: Boolean
            testError: Boolean
        }

        type Entity {
            id: String!
            name: String!
            description: String
            hello(name: String): String
        }

        type Mutation {
            upload(file: Upload!): String
            echo(text: String!): String
        }

        type Subscription {
            ping: String!
            resolverError: Boolean
            sourceError: Boolean
            testContext: String
            testRoot: String
        }
    """


def resolve_hello(*_: tuple, name: str) -> str:
    return f"Hello, {name}!"


def resolve_status(*_: tuple) -> bool:
    return True


def resolve_test(*_: tuple, identifier: str) -> dict[str, Any]:
    return {
        "id": identifier,
        "name": "test",
        "description": "test",
        "hello": resolve_hello,
    }


def resolve_error(*_: tuple) -> None:
    # pylint: disable=broad-exception-raised
    raise Exception("Test exception")


@pytest.fixture
def schema(type_defs: str) -> Any:
    resolvers = QueryType()
    resolvers.set_field("status", resolve_status)
    resolvers.set_field("testResolver", resolve_test)
    resolvers.set_field("testError", resolve_status)

    return make_executable_schema(type_defs, [resolvers])
