from collections.abc import (
    Callable,
)
from datetime import (
    datetime,
)
from integrates.db_model.roots.enums import (
    RootEnvironmentUrlStateStatus,
)
from integrates.db_model.roots.types import (
    RootEnvironmentUrl,
    RootEnvironmentUrlState,
)
import pytest
from typing import (
    Any,
)

pytestmark = [
    pytest.mark.asyncio,
]


MOCKED_DATA: dict[str, dict[str, Any]] = {
    "integrates.reports.pdf.Dataloaders.root_evironment_urls": {
        '["group_1"]': [
            RootEnvironmentUrl(
                url="https://test.com",
                id="78dd64d3198473115a7f5263d27bed15f9f2fc07",
                group_name="group_1",
                root_id="",
                state=RootEnvironmentUrlState(
                    modified_by="admin@gmail.com",
                    modified_date=datetime.fromisoformat(
                        "2020-11-19T13:37:10+00:00"
                    ),
                    status=RootEnvironmentUrlStateStatus.CREATED,
                ),
            ),
        ],
    },
}


@pytest.fixture
def mock_data_for_module(
    *,
    resolve_mock_data: Callable,
) -> Any:
    def _mocked_data_for_module(
        mock_path: str, mock_args: list[Any], module_at_test: str
    ) -> Callable[[str, list[Any], str], Any]:
        return resolve_mock_data(
            mock_data=MOCKED_DATA,
            mock_path=mock_path,
            mock_args=mock_args,
            module_at_test=module_at_test,
        )

    return _mocked_data_for_module
