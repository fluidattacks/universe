from collections.abc import (
    Callable,
)
from datetime import (
    datetime,
)
from integrates.custom_utils.datetime import (
    get_now_minus_delta,
)
from integrates.db_model.azure_repositories.types import (
    BasicRepoData,
)
from integrates.db_model.roots.enums import (
    RootCloningStatus,
    RootCriticality,
    RootStatus,
    RootType,
)
from integrates.db_model.roots.types import (
    GitRoot,
    GitRootCloning,
    GitRootState,
)
import pytest
from test.unit.src.utils import (
    MockedInstance,
)
from typing import (
    Any,
)


@pytest.fixture
def mock_gitlab() -> Callable:
    mocked_branches = [
        MockedInstance(name="test1"),
        MockedInstance(name="develop"),
        MockedInstance(name="main"),
    ]

    mocked_projects = [
        MockedInstance(
            id="1",
            name="repo",
            attributes={
                "archived": False,
                "default_branch": "main",
                "empty_repo": False,
                "http_url_to_repo": "https://gitlab.com/username/repo.git",
                "last_activity_at": "2021-09-30T14:58:10+00:00",
                "name": "repo",
                "ssh_url_to_repo": "ssh://gitlab.com/username/repo.git",
                "web_url": "https://gitlab.com/username/repo",
            },
            branches=MockedInstance(
                __repr__=lambda *_, **__: mocked_branches,
                list=lambda *_, **__: mocked_branches,
            ),
        )
    ]

    return lambda *_, **__: MockedInstance(
        projects=MockedInstance(
            get=lambda id: next(
                (x for x in mocked_projects if x.id == id), None
            ),
            list=lambda *_, **__: mocked_projects,
        )
    )


@pytest.fixture
def mock_bitbucket_branches() -> tuple[str, ...]:
    mocked_branches = tuple(
        [
            "test1",
            "develop",
            "main2",
        ]
    )

    return mocked_branches


@pytest.fixture
def mock_bitbucket_repositories() -> tuple[BasicRepoData, ...]:
    mocked_repos = tuple(
        [
            BasicRepoData(
                id="testprojects#REPOSITORY#fourthrepo",
                ssh_url="ssh://git@bitbucket.com/testprojects/fourthtrepo",
                remote_url=(
                    "https://git@bitbucket.com/testprojects/fourthtrepo.git"
                ),
                web_url="https://bitbucket.com/testprojects/fourthtrepo",
                branch="refs/heads/main2",
                branches=tuple(["main2"]),
                name="fourthtrepo",
                last_activity_at=get_now_minus_delta(days=4),
            )
        ]
    )

    return mocked_repos


@pytest.fixture
def mock_roots() -> list[GitRoot]:
    test_email = "admin@gmail.com"
    date = "2022-02-10T14:58:10+00:00"
    roots = [
        GitRoot(
            cloning=GitRootCloning(
                modified_by=test_email,
                modified_date=datetime.fromisoformat(date),
                reason="Cloned successfully",
                status=RootCloningStatus.OK,
            ),
            created_by=test_email,
            created_date=datetime.fromisoformat(date),
            group_name="kibi",
            id="8a62109b-316a-4a88-a1f1-767b80383864",
            organization_name="wano",
            state=GitRootState(
                branch="main2",
                criticality=RootCriticality.LOW,
                gitignore=[],
                includes_health_check=False,
                modified_by="test@fluidattacks.com",
                modified_date=datetime.fromisoformat(date),
                nickname="inactive",
                status=RootStatus.INACTIVE,
                url="https://bitbucket.com/fluidattacks/inactive",
            ),
            type=RootType.GIT,
        ),
    ]
    return roots


@pytest.fixture
def mocked_data_for_module(
    *,
    resolve_mock_data: Callable,
) -> Any:
    def _mocked_data_for_module(
        mock_path: str, mock_args: list[Any], module_at_test: str
    ) -> Callable[[str, list[Any], str], Any]:
        return resolve_mock_data(
            mock_data=[],
            mock_path=mock_path,
            mock_args=mock_args,
            module_at_test=module_at_test,
        )

    return _mocked_data_for_module
