from collections.abc import (
    Callable,
)
from datetime import (
    datetime,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.azure_repositories.types import (
    ProjectStats,
)
from integrates.db_model.credentials.types import (
    Credentials,
    CredentialsState,
    OauthBitbucketSecret,
)
from integrates.db_model.enums import (
    CredentialType,
)
from integrates.outside_repositories.bitbucket import (
    _get_bitbucket_repositories_stats,
)
import pytest
from unittest.mock import (
    AsyncMock,
    patch,
)

pytestmark = [
    pytest.mark.asyncio,
]


@patch(
    "integrates.oauth.common.get_token", new=AsyncMock(return_value="testing")
)
@patch(
    "integrates.outside_repositories.bitbucket.get_credential_token",
    new=AsyncMock(return_value="testing"),
)
async def test_get_bitbucket_repositories_stats(
    mock_bitbucket_branches: Callable,
    mock_bitbucket_repositories: Callable,
    mock_roots: Callable,
) -> None:
    loaders: Dataloaders = get_new_context()
    mocked_credential = Credentials(
        id="3912827d-2b35-4e08-bd35-1bb24457951d",
        organization_id="ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db",
        state=CredentialsState(
            modified_by="admin@gmail.com",
            modified_date=datetime.fromisoformat("2023-09-30T10:00:00+00:00"),
            name="SSH Key",
            type=CredentialType.SSH,
            is_pat=False,
            owner="admin@gmail.com",
        ),
        secret=OauthBitbucketSecret(
            brefresh_token="testing2",
            access_token="testing",
            valid_until=datetime.fromisoformat("2023-09-30T11:00:00+00:00"),
        ),
    )
    with (
        patch(
            "integrates.outside_repositories.bitbucket."
            "_get_bitbucket_branches_names",
            return_value=mock_bitbucket_branches,
        ),
        patch(
            "integrates.db_model.azure_repositories"
            ".get._get_bitbucket_repositories",
            return_value=mock_bitbucket_repositories,
        ),
        patch(
            "integrates.oauth.bitbucket.get_organization_roots",
            AsyncMock(return_value=mock_roots),
        ),
    ):
        project_list = await _get_bitbucket_repositories_stats(
            loaders=loaders,
            credential=mocked_credential,
            urls=set(),
            nicknames=set(),
        )
        result = project_list[0]
    assert isinstance(result, ProjectStats)
    assert result.project.branch == "refs/heads/main2"
    assert result.project.name == "fourthtrepo"
    assert sorted(result.project.branches) == sorted(
        ("main2", "develop", "test1")
    )
