from collections.abc import (
    Callable,
)
from datetime import (
    datetime,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.azure_repositories.types import (
    ProjectStats,
)
from integrates.db_model.credentials.types import (
    Credentials,
    CredentialsState,
    OauthGitlabSecret,
)
from integrates.db_model.enums import (
    CredentialType,
)
from integrates.outside_repositories.gitlab import (
    _get_gitlab_outside_repositories_stats,
)
import pytest
from unittest.mock import (
    AsyncMock,
    patch,
)

pytestmark = [
    pytest.mark.asyncio,
]


@patch(
    "integrates.oauth.common.get_token", new=AsyncMock(return_value="testing")
)
async def test_get_gitlab_outside_repositories_stats(
    mock_gitlab: Callable,
) -> None:
    loaders: Dataloaders = get_new_context()
    mocked_credential = Credentials(
        id="3912827d-2b35-4e08-bd35-1bb24457951d",
        organization_id="ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db",
        state=CredentialsState(
            modified_by="admin@gmail.com",
            modified_date=datetime.fromisoformat("2023-09-30T10:00:00+00:00"),
            name="SSH Key",
            type=CredentialType.SSH,
            is_pat=False,
            owner="admin@gmail.com",
        ),
        secret=OauthGitlabSecret(
            refresh_token="testing2",
            redirect_uri="http://localhost",
            access_token="testing",
            valid_until=datetime.fromisoformat("2023-09-30T11:00:00+00:00"),
        ),
    )
    with patch("gitlab.Gitlab", mock_gitlab):
        project_list = await _get_gitlab_outside_repositories_stats(
            loaders=loaders,
            credential=mocked_credential,
            urls=set(),
            nicknames=set(),
        )
        result = project_list[0]
    assert isinstance(result, ProjectStats)
    assert sorted(result.project.branches) == sorted(
        ("main", "develop", "test1")
    )
