from unittest.mock import (
    AsyncMock,
    patch,
)

import pytest
from freezegun import freeze_time

from integrates.db_model.jira_installations.types import JiraInstallState
from integrates.db_model.stakeholders.types import (
    StakeholderMetadataToUpdate,
)
from integrates.jira_security_integration.utils import (
    add_association,
    check_installation_associated_email,
    parse_jira_jwt_token,
)

pytestmark = [
    pytest.mark.asyncio,
]


@pytest.mark.asyncio
@pytest.mark.parametrize(
    [
        "jwt_token",
    ],
    [
        [
            """
            eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.
            eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6Ik
            pvaG4gRG9lIiwiaXNzIjoiZXhhbXBsZS5jb20ifQ.
            signature
            """,
        ],
    ],
)
async def test_parse_jira_jwt_token(jwt_token: str) -> None:
    result = parse_jira_jwt_token(jwt_token)
    empty_result = parse_jira_jwt_token("")

    expected_headers = '{"alg":"HS256","typ":"JWT"}'
    expected_claims = (
        '{"sub":"1234567890","name":"John Doe","iss":"example.com"}'
    )

    assert not empty_result
    assert result[0] == expected_headers
    assert result[1] == expected_claims


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "jwt_token, base_url, expected_email",
    [
        (
            """
            eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.
            eyJpc3MiOiI0MnhqaG1kbC0yemdhLWU2ZTgtb
            3g1bS01djdleG4xNnVuMWsifQ.
            jVJEsXVccFnLpDF0_HtOWPYSDR7Oe2swFD-53KD88xE==
            """,
            "https://fluidattacks-dev.atlassian.net",
            "integratesuser@gmail.com",
        ),
    ],
)
async def test_check_installation_associated_email(
    jwt_token: str, base_url: str, expected_email: str
) -> None:
    result = await check_installation_associated_email(
        jwt=jwt_token, base_url=base_url
    )
    assert result == expected_email


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "base_url",
    [
        ("https://fluidattacks-dev.atlassian.net",),
    ],
)
async def test_check_installation_associated_email_no_jwt(
    base_url: str,
) -> None:
    result = await check_installation_associated_email(
        jwt=None, base_url=base_url
    )
    assert result == ""


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "jwt_token, base_url",
    [
        (
            """
            eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.
            eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6Ik
            pvaG4gRG9lIiwiaXNzIjoiZXhhbXBsZS5jb20ifQ.
            signature
            """,
            "https://no-install.atlassian.net",
        ),
    ],
)
async def test_check_installation_associated_email_no_install(
    jwt_token: str, base_url: str
) -> None:
    result = await check_installation_associated_email(
        jwt=jwt_token, base_url=base_url
    )
    assert result == ""


@freeze_time("2021-10-10")
@pytest.mark.asyncio
@patch(
    "integrates.jira_security_integration.utils."
    "jira_installations.update_associate_email"
)
@patch(
    "integrates.jira_security_integration.utils."
    "jira_installations.update_jira_install_state"
)
@patch(
    "integrates.jira_security_integration.utils."
    "stakeholders_model.update_metadata"
)
@patch(
    "integrates.jira_security_integration.utils.sessions_domain.decode_token"
)
async def test_add_association(
    mock_decode_token: AsyncMock,
    mock_update_metadata: AsyncMock,
    mock_update_jira_install_state: AsyncMock,
    mock_update_associate_email: AsyncMock,
) -> None:
    api_token = "test_api_token"
    jira_jwt = (
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9."
        "eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6Ik"
        "pvaG4gRG9lIiwiaXNzIjoiZXhhbXBsZS5jb20ifQ."
        "signature"
    )
    base_url = "https://fluidattacks-dev.atlassian.net"
    user_email = "user@example.com"
    client_key = "example.com"

    mock_decode_token.return_value = {"user_email": user_email}

    await add_association(api_token, jira_jwt, base_url)

    mock_decode_token.assert_called_once_with(api_token)
    mock_update_associate_email.assert_called_once_with(
        base_url=base_url,
        client_key=client_key,
        associated_email=user_email,
    )
    mock_update_jira_install_state.assert_called_once_with(
        base_url=base_url,
        client_key=client_key,
        state=JiraInstallState(
            modified_by=user_email,
            modified_date="2021-10-10T00:00:00+00:00",
            used_api_token=api_token,
            used_jira_jwt=jira_jwt,
        ),
    )
    mock_update_metadata.assert_called_once_with(
        metadata=StakeholderMetadataToUpdate(
            jira_client_key=client_key,
        ),
        email=user_email,
    )
