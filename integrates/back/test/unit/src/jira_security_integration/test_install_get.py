from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.jira_installations.types import (
    JiraSecurityInstall,
    JiraSecurityInstallRequest,
    UserJiraSecurityInstallRequest,
)
import pytest


@pytest.mark.asyncio
async def test_install_get() -> None:
    loaders: Dataloaders = get_new_context()
    jira_install_exists: (
        JiraSecurityInstall | None
    ) = await loaders.jira_install.load(
        JiraSecurityInstallRequest(
            base_url="https://fluidattacks-dev.atlassian.net",
            client_key="42xjhmdl-2zga-e6e8-ox5m-5v7exn16un1k",
        )
    )
    jira_install_not_exists: (
        JiraSecurityInstall | None
    ) = await loaders.jira_install.load(
        JiraSecurityInstallRequest(
            base_url="https://not-exists-url.com",
            client_key="no-exists-client-key",
        )
    )
    expected_install = {
        "description": "testing description",
        "server_version": "100100",
        "key": "com.testing.testing-jira-app",
    }

    assert jira_install_exists is not None
    assert jira_install_exists.description == expected_install["description"]
    assert (
        jira_install_exists.server_version
        == expected_install["server_version"]
    )
    assert jira_install_exists.key == expected_install["key"]
    assert jira_install_not_exists is None


@pytest.mark.asyncio
@pytest.mark.asyncio
async def test_install_user_install_get() -> None:
    loaders: Dataloaders = get_new_context()
    jira_install_exists: (
        list[JiraSecurityInstall] | None
    ) = await loaders.user_jira_install.load(
        UserJiraSecurityInstallRequest(
            client_key="42xjhmdl-2zga-e6e8-ox5m-5v7exn16un1k",
        )
    )
    jira_install_not_exists: (
        list[JiraSecurityInstall] | None
    ) = await loaders.user_jira_install.load(
        UserJiraSecurityInstallRequest(
            client_key="no-exists-client-key",
        )
    )
    expected_install = {
        "description": "testing description",
        "server_version": "100100",
        "key": "com.testing.testing-jira-app",
    }

    assert jira_install_exists is not None
    assert (
        jira_install_exists[0].description == expected_install["description"]
    )
    assert (
        jira_install_exists[0].server_version
        == expected_install["server_version"]
    )
    assert jira_install_exists[0].key == expected_install["key"]
    assert jira_install_not_exists == []
