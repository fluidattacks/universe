from collections.abc import (
    Iterable,
)
from datetime import (
    datetime,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.roots.enums import (
    RootCloningStatus,
    RootCriticality,
    RootStatus,
    RootType,
)
from integrates.db_model.roots.types import (
    GitRoot,
    GitRootCloning,
    GitRootState,
    Root,
    RootUnreliableIndicators,
)
from integrates.roots.domain import (
    _gitignore_enforcer,
    comment_vulnerabilities_by_exclusions,
    get_root_id_by_nickname,
)
import pytest
from test.unit.src.utils import (
    get_module_at_test,
)
from typing import (
    Callable,
)
from unittest.mock import (
    AsyncMock,
    patch,
)

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"

pytestmark = [
    pytest.mark.asyncio,
]


@pytest.mark.parametrize(
    ["nickname", "group_roots", "only_git_roots"],
    [
        [
            "universe",
            [
                GitRoot(
                    cloning=GitRootCloning(
                        modified_by="jdoe@fluidattacks.com",
                        modified_date=datetime.fromisoformat(
                            "2020-11-19T13:45:55+00:00"
                        ),
                        reason="root OK",
                        status=RootCloningStatus.OK,
                        commit="5b5c92105b5c92105b5c92105b5c92105b5c9210",
                        commit_date=datetime.fromisoformat(
                            "2022-02-15T18:45:06.493253+00:00"
                        ),
                    ),
                    created_by="jdoe@fluidattacks.com",
                    created_date=datetime.fromisoformat(
                        "2020-11-19T13:37:10+00:00"
                    ),
                    group_name="unittesting",
                    id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    organization_name="okada",
                    state=GitRootState(
                        branch="master",
                        criticality=RootCriticality.LOW,
                        includes_health_check=True,
                        modified_by="jdoe@fluidattacks.com",
                        modified_date=datetime.fromisoformat(
                            "2020-11-19T13:37:10+00:00"
                        ),
                        nickname="universe",
                        status=RootStatus.ACTIVE,
                        url="https://gitlab.com/fluidattacks/universe",
                        credential_id=None,
                        gitignore=["bower_components/*", "node_modules/*"],
                        other=None,
                        reason=None,
                        use_egress=False,
                        use_vpn=False,
                        use_ztna=False,
                    ),
                    type=RootType.GIT,
                    unreliable_indicators=RootUnreliableIndicators(
                        unreliable_code_languages=[],
                        unreliable_last_status_update=datetime.fromisoformat(
                            "2020-11-19T13:37:10+00:00"
                        ),
                    ),
                ),
                GitRoot(
                    cloning=GitRootCloning(
                        modified_by="jdoe@fluidattacks.com",
                        modified_date=datetime.fromisoformat(
                            "2020-11-19T13:37:10+00:00"
                        ),
                        reason="root creation",
                        status=RootCloningStatus.UNKNOWN,
                        commit="cdd48a681aa96082b3095dc06fb1b15ec4b5ea7b",
                        commit_date=datetime.fromisoformat(
                            "2022-02-15T18:45:06.493253+00:00"
                        ),
                    ),
                    created_by="jdoe@fluidattacks.com",
                    created_date=datetime.fromisoformat(
                        "2020-11-19T13:39:56+00:00"
                    ),
                    group_name="unittesting",
                    id="765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a",
                    organization_name="okada",
                    state=GitRootState(
                        branch="develop",
                        criticality=RootCriticality.LOW,
                        includes_health_check=False,
                        modified_by="jdoe@fluidattacks.com",
                        modified_date=datetime.fromisoformat(
                            "2020-11-19T13:39:56+00:00"
                        ),
                        nickname="integrates_1",
                        status=RootStatus.ACTIVE,
                        url="https://gitlab.com/fluidattacks/integrates",
                        credential_id=None,
                        gitignore=[],
                        other=None,
                        reason=None,
                        use_egress=False,
                        use_vpn=False,
                        use_ztna=False,
                    ),
                    type=RootType.GIT,
                    unreliable_indicators=RootUnreliableIndicators(
                        unreliable_code_languages=[],
                        unreliable_last_status_update=datetime.fromisoformat(
                            "2020-11-19T13:39:56+00:00"
                        ),
                    ),
                ),
            ],
            True,
        ],
    ],
)
async def test_get_root_id_by_nickname(
    nickname: str,
    group_roots: Iterable[Root],
    only_git_roots: bool,
) -> None:
    root_id = get_root_id_by_nickname(
        nickname, tuple(group_roots), only_git_roots=only_git_roots
    )
    assert root_id == "4039d098-ffc5-4984-8ed3-eb17bca98e19"


@pytest.mark.parametrize(
    ["exclusions", "expected_result"],
    [
        [
            ["**/*.md"],
            {
                ".gitignore": False,
                "file.md": True,
                "path/to/file.md": True,
                "path/to/file.ts": False,
            },
        ],
        [
            ["file.txt", "src/app/File.ts"],
            {
                ".gitignore": False,
                "file.txt": True,
                "dir/file.txt": True,
                "src/app/File.ts": True,
                "test/src/app/File.ts": False,
            },
        ],
        [
            ["/file.txt", "/src/app/File.ts"],
            {
                ".gitignore": False,
                "file.txt": True,
                "dir/file.txt": False,
                "src/app/File.ts": True,
                "test/src/app/File.ts": False,
            },
        ],
        [
            ["src", "dir/", "assets/*", "outs/**"],
            {
                "file.txt": False,
                "src/file.py": True,
                "src/test/file.py": True,
                "dir": False,
                "dir/file.java": True,
                "dir/test/file.java": True,
                "assets/file.jpg": True,
                "assets/test/file.jpg": True,
                "outs/file.txt": True,
                "outs/media/file.mov": True,
            },
        ],
        [
            ["src/*", "!src/test/file.py"],
            {
                ".gitignore": False,
                "src/a": True,
                "src/file.txt": True,
                "src/test/file.py": False,
                "src/test/file.pyc": True,
            },
        ],
        [
            ["src", "!src/test/file.py"],
            {
                ".gitignore": False,
                "src/a": True,
                "src/file.txt": True,
                "src/test/file.py": False,
                "src/test/file.pyc": True,
            },
        ],
        [
            ["**/a", "b/**/c"],
            {
                ".gitignore": False,
                "a/test.py": True,
                "path/to/a/test.py": True,
                "b/c/file.txt": True,
                "b/test/c/file.txt": True,
            },
        ],
        [
            [
                "#file1.txt",
                "\\#file2.txt",
                "\\!file3.txt",
                "hi!.py",
                "\\ hi.py",
            ],
            {
                "#file1.txt": False,
                "#file2.txt": True,
                "!file3.txt": True,
                "hi!.py": True,
                " hi.py": True,
            },
        ],
        [
            ["file?.txt"],
            {
                "file1.txt": True,
                "path/to/file2.txt": True,
                "file10.txt": False,
            },
        ],
    ],
)
async def test_gitignore_enforcer(
    exclusions: list[str],
    expected_result: dict,
) -> None:
    files = expected_result.keys()
    checker = _gitignore_enforcer(exclusions)
    results = {file: checker(file) for file in files}

    for file in files:
        assert results[file] == expected_result[file]


@pytest.mark.parametrize(
    ["root_id", "email", "date", "exclusions"],
    [
        [
            "fa5a5991-d184-44b4-9228-947d6832cab3",
            "actioner@example.com",
            "2023-09-30T15:00:00+00:00",
            ["test/data/*"],
        ],
    ],
)
@patch("integrates.finding_comments.domain.add", new_callable=AsyncMock)
async def test_comment_vulnerabilities_by_exclusions(
    mock_finding_comments_add: AsyncMock,
    *,
    root_id: str,
    email: str,
    date: str,
    exclusions: list[str],
    _populate_local: Callable,
) -> None:
    loaders: Dataloaders = get_new_context()
    await comment_vulnerabilities_by_exclusions(
        loaders=loaders,
        root_id=root_id,
        email=email,
        date=datetime.fromisoformat(date),
        exclusions=exclusions,
    )

    mock_finding_comments_add.assert_called()
    assert mock_finding_comments_add.call_count == 2
