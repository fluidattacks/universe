from collections.abc import (
    Callable,
)
from datetime import (
    datetime,
)
from integrates.db_model.enums import (
    Source,
    TreatmentStatus,
)
from integrates.db_model.roots.enums import (
    RootCloningStatus,
    RootCriticality,
    RootStatus,
    RootType,
)
from integrates.db_model.roots.types import (
    GitRoot,
    GitRootCloning,
    GitRootState,
    RootState,
)
from integrates.db_model.types import (
    Treatment,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
    VulnerabilityTechnique,
    VulnerabilityToolImpact,
    VulnerabilityType,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityState,
    VulnerabilityTool,
)
import pytest
from pytest_mock import (
    MockerFixture,
)
from typing import (
    Any,
)
from unittest.mock import (
    AsyncMock,
)

DEFAULT_DATE = datetime.fromisoformat("2023-09-30T15:00:00+00:00")
MOCKED_DATA: dict[str, dict[str, Any]] = {
    "integrates.roots.update.Dataloaders.root_historic_state": {
        '["4039d098-ffc5-4984-8ed3-eb17bca98e19"]': [
            RootState(
                modified_by="jdoe@fluidattacks.com",
                modified_date=datetime.fromisoformat(
                    "2023-01-01T12:00:00+00:00"
                ),
                nickname="MyAwesomeRepo",
                status=RootStatus.ACTIVE,
            ),
            RootState(
                modified_by="jdoe@fluidattacks.com",
                modified_date=datetime.fromisoformat(
                    "2023-01-01T12:01:00+00:00"
                ),
                nickname="MyAwesomeRepo",
                status=RootStatus.INACTIVE,
            ),
            RootState(
                modified_by="jdoe@fluidattacks.com",
                modified_date=datetime.fromisoformat(
                    "2023-01-01T12:02:00+00:00"
                ),
                nickname="MyAwesomeRepo",
                status=RootStatus.INACTIVE,
            ),
            RootState(
                modified_by="jdoe@fluidattacks.com",
                modified_date=datetime.fromisoformat(
                    "2023-01-01T12:03:00+00:00"
                ),
                nickname="MyAwesomeRepo",
                status=RootStatus.ACTIVE,
            ),
            RootState(
                modified_by="jdoe@fluidattacks.com",
                modified_date=datetime.fromisoformat(
                    "2023-01-01T12:04:00+00:00"
                ),
                nickname="MyAwesomeRepo",
                status=RootStatus.ACTIVE,
            ),
        ],
        '["808e6ccb-a5a3-4e6d-a9cf-99b21cdf0d41"]': [
            RootState(
                modified_by="jdoe@fluidattacks.com",
                modified_date=datetime.fromisoformat(
                    "2023-01-01T12:00:00+00:00"
                ),
                nickname="MyAwesomeRepo",
                status=RootStatus.ACTIVE,
            ),
        ],
    },
    "integrates.db_model.vulnerabilities.get.RootVulnerabilitiesLoader.load": {
        '["fa5a5991-d184-44b4-9228-947d6832cab3"]': [
            Vulnerability(
                created_by="unittest@fluidattacks.com",
                created_date=DEFAULT_DATE,
                finding_id="422286130",
                group_name="unittesting",
                organization_name="okada",
                hacker_email="unittest@fluidattacks.com",
                id="9ef4a9fd-ef41-49f8-a5e9-3eb378f8226f",
                root_id="fa5a5991-d184-44b4-9228-947d6832cab3",
                state=VulnerabilityState(
                    modified_by="unittest@fluidattacks.com",
                    modified_date=DEFAULT_DATE,
                    source=Source.ASM,
                    specific="12",
                    status=VulnerabilityStateStatus.VULNERABLE,
                    where="test/data/lib_path/f060/csharp.cs",
                    commit="ea871eee64cfd5ce293411efaf4d3b446d04eb4a",
                    reasons=None,
                    other_reason=None,
                    tool=VulnerabilityTool(
                        name="tool-2",
                        impact=VulnerabilityToolImpact.INDIRECT,
                    ),
                ),
                technique=VulnerabilityTechnique.SCR,
                type=VulnerabilityType.LINES,
                treatment=Treatment(
                    modified_date=DEFAULT_DATE,
                    status=TreatmentStatus.IN_PROGRESS,
                    acceptance_status=None,
                    accepted_until=None,
                    justification="test justification",
                    assigned="integratesuser2@gmail.com",
                    modified_by="integratesuser@gmail.com",
                ),
            ),
            Vulnerability(
                created_by="unittest@fluidattacks.com",
                created_date=DEFAULT_DATE,
                finding_id="422286131",
                group_name="unittesting",
                organization_name="okada",
                hacker_email="unittest@fluidattacks.com",
                id="351f3be8-878e-417c-bb28-93bf1b6826d3",
                root_id="fa5a5991-d184-44b4-9228-947d6832cab3",
                state=VulnerabilityState(
                    modified_by="unittest@fluidattacks.com",
                    modified_date=DEFAULT_DATE,
                    source=Source.ASM,
                    specific="10",
                    status=VulnerabilityStateStatus.VULNERABLE,
                    where="test/data/lib_path/f062/test.py",
                    commit="11efaf4d3b446d04eb4aea871eee64cfd5ce2934",
                    reasons=None,
                    other_reason=None,
                    tool=VulnerabilityTool(
                        name="tool-2",
                        impact=VulnerabilityToolImpact.INDIRECT,
                    ),
                ),
                technique=VulnerabilityTechnique.SCR,
                type=VulnerabilityType.LINES,
                treatment=Treatment(
                    modified_date=DEFAULT_DATE,
                    status=TreatmentStatus.IN_PROGRESS,
                    acceptance_status=None,
                    accepted_until=None,
                    justification="test justification",
                    assigned="integratesuser2@gmail.com",
                    modified_by="integratesuser@gmail.com",
                ),
            ),
            Vulnerability(
                created_by="unittest@fluidattacks.com",
                created_date=DEFAULT_DATE,
                finding_id="988493279",
                group_name="unittesting",
                organization_name="okada",
                hacker_email="unittest@fluidattacks.com",
                id="47ce0fb0-4108-49b0-93cc-160dce8168a6",
                root_id="fa5a5991-d184-44b4-9228-947d6832cab3",
                state=VulnerabilityState(
                    modified_by="unittest@fluidattacks.com",
                    modified_date=DEFAULT_DATE,
                    source=Source.ASM,
                    specific="8888",
                    status=VulnerabilityStateStatus.VULNERABLE,
                    where="192.168.1.19",
                    commit=None,
                    reasons=None,
                    other_reason=None,
                    tool=VulnerabilityTool(
                        name="tool-1",
                        impact=VulnerabilityToolImpact.INDIRECT,
                    ),
                ),
                technique=VulnerabilityTechnique.PTAAS,
                type=VulnerabilityType.PORTS,
                treatment=Treatment(
                    modified_date=DEFAULT_DATE,
                    status=TreatmentStatus.IN_PROGRESS,
                    acceptance_status=None,
                    accepted_until=None,
                    justification="test justification",
                    assigned="integratesuser2@gmail.com",
                    modified_by="integratesuser@gmail.com",
                ),
            ),
        ]
    },
    "integrates.db_model.roots.get.RootLoader.load": {
        '["unittesting", "fa5a5991-d184-44b4-9228-947d6832cab3"]': GitRoot(
            cloning=GitRootCloning(
                modified_by="jdoe@fluidattacks.com",
                modified_date=DEFAULT_DATE,
                reason="root OK",
                status=RootCloningStatus.OK,
                commit="5b5c92105b5c92105b5c92105b5c92105b5c9210",
                commit_date=DEFAULT_DATE,
            ),
            created_by="jdoe@fluidattacks.com",
            created_date=DEFAULT_DATE,
            group_name="unittesting",
            id="fa5a5991-d184-44b4-9228-947d6832cab3",
            organization_name="okada",
            state=GitRootState(
                branch="master",
                criticality=RootCriticality.LOW,
                includes_health_check=True,
                modified_by="jdoe@fluidattacks.com",
                modified_date=DEFAULT_DATE,
                nickname="universe",
                status=RootStatus.ACTIVE,
                url="https://gitlab.com/fluidattacks/universe",
                credential_id=None,
                gitignore=["bower_components/*", "node_modules/*"],
                other=None,
                reason=None,
                use_egress=False,
                use_vpn=False,
                use_ztna=False,
            ),
            type=RootType.GIT,
        ),
    },
}


@pytest.fixture
def mocked_data_for_module(
    *,
    resolve_mock_data: Callable,
) -> Any:
    def _mocked_data_for_module(
        mock_path: str, mock_args: list[Any], module_at_test: str
    ) -> Callable[[str, list[Any], str], Any]:
        return resolve_mock_data(
            mock_data=MOCKED_DATA,
            mock_path=mock_path,
            mock_args=mock_args,
            module_at_test=module_at_test,
        )

    return _mocked_data_for_module


@pytest.fixture
def _populate_local(mocker: MockerFixture) -> None:
    for path, mocked in MOCKED_DATA.items():
        for _, value in mocked.items():
            mocker.patch(
                path,
                return_value=value,
                new_callable=AsyncMock,
            )
