from integrates.forces.utils import (
    is_forces_user,
)
import pytest


@pytest.mark.parametrize(
    [
        "email",
        "expected_result",
    ],
    [
        ["forces.unittesting@fluidattacks.com", True],
        ["unittesting@fluidattacks.com", False],
    ],
)
def test_is_forces_user(email: str, expected_result: bool) -> None:
    result = is_forces_user(email)
    assert result == expected_result
