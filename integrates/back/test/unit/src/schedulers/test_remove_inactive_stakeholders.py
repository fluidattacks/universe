from datetime import (
    datetime,
)
from decimal import (
    Decimal,
)
from freezegun import (
    freeze_time,
)
from integrates.custom_exceptions import (
    CredentialInUse,
)
from integrates.db_model.organizations.enums import (
    OrganizationStateStatus,
)
from integrates.db_model.organizations.types import (
    Organization,
    OrganizationState,
)
from integrates.db_model.stakeholders.types import (
    NotificationsParameters,
    NotificationsPreferences,
    Stakeholder,
    StakeholderLogin,
    StakeholderState,
    StakeholderTours,
)
from integrates.db_model.types import (
    Policies,
)
from integrates.schedulers.remove_inactive_stakeholders import (
    _remove_access,
    main,
    process_organization,
    process_stakeholder,
    remove_inactive_stakeholders,
)
import pytest
from test.unit.src.utils import (
    get_module_at_test,
)
from typing import (
    Any,
)
from unittest.mock import (
    AsyncMock,
    patch,
)

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"

pytestmark = [
    pytest.mark.asyncio,
]


@freeze_time("2021-04-01T00:00:00+00:00")
@pytest.mark.parametrize(
    ["stakeholder"],
    [
        [
            Stakeholder(
                email="unittest@fluidattacks.com",
                enrolled=True,
                first_name="Miguel",
                is_concurrent_session=False,
                is_registered=True,
                last_login_date=datetime.fromisoformat(
                    "2020-12-31T18:40:37+00:00"
                ),
                last_name="de Orellana",
                legal_remember=True,
                phone=None,
                registration_date=datetime.fromisoformat(
                    "2019-02-28T16:54:12+00:00"
                ),
                role="admin",
                session_key=None,
                session_token=None,
                state=StakeholderState(
                    modified_by=None,
                    modified_date=None,
                    notifications_preferences=NotificationsPreferences(
                        available=[],
                        email=[],
                        sms=[],
                        parameters=NotificationsParameters(
                            min_severity=Decimal("3.0")
                        ),
                    ),
                ),
                login=StakeholderLogin(
                    modified_by="integrates@fluidattacks.com",
                    modified_date=datetime.fromisoformat(
                        "2020-12-31T18:40:37+00:00"
                    ),
                    expiration_time=2026859315,
                    browser="Chrome 114.0.0.0",
                    country_code="CO",
                    device="Linux",
                    ip_address="127.0.0.1",
                    provider="GOOGLE",
                    subject="GOOGLE#2222223333333344445555",
                ),
                tours=StakeholderTours(
                    new_group=False,
                    new_root=False,
                    welcome=False,
                ),
            )
        ],
        [
            Stakeholder(
                email="testing@fluidattacks.com",
                enrolled=True,
                first_name="",
                is_concurrent_session=False,
                is_registered=True,
                last_login_date=None,
                last_name="",
                legal_remember=True,
                phone=None,
                registration_date=None,
                role="user",
                session_key=None,
                session_token=None,
                state=StakeholderState(
                    modified_by=None,
                    modified_date=None,
                    notifications_preferences=NotificationsPreferences(
                        available=[],
                        email=[],
                        sms=[],
                        parameters=NotificationsParameters(
                            min_severity=Decimal("3.0")
                        ),
                    ),
                ),
                login=StakeholderLogin(
                    modified_by="integrates@fluidattacks.com",
                    modified_date=datetime.fromisoformat(
                        "2024-03-22T18:32:20.800014+00:00"
                    ),
                    expiration_time=2026859315,
                    browser="Chrome 114.0.0.0",
                    country_code="CO",
                    device="Linux",
                    ip_address="127.0.0.1",
                    provider="GOOGLE",
                    subject="GOOGLE#2222223333333344445555",
                ),
                tours=StakeholderTours(
                    new_group=False,
                    new_root=False,
                    welcome=False,
                ),
            )
        ],
    ],
)
@patch(MODULE_AT_TEST + "stakeholders_domain.remove", new_callable=AsyncMock)
@patch(
    MODULE_AT_TEST + "Dataloaders.stakeholder_organizations_access",
    new_callable=AsyncMock,
)
async def test_process_stakeholder(
    mock_loaders_stakeholder_organizations_access: AsyncMock,
    mock_stakeholders_domain_remove: AsyncMock,
    stakeholder: Stakeholder,
    mocked_data_for_module: Any,
) -> None:
    mocks_setup_list: list[tuple[AsyncMock, str, list[Any]]] = [
        (
            mock_loaders_stakeholder_organizations_access.load,
            "Dataloaders.stakeholder_organizations_access",
            [stakeholder.email],
        ),
        (
            mock_stakeholders_domain_remove,
            "stakeholders_domain.remove",
            [stakeholder.email],
        ),
    ]
    # Set up mocks' results using mocked_data_for_module fixture
    for item in mocks_setup_list:
        mock, path, arguments = item
        mock.return_value = mocked_data_for_module(
            mock_path=path,
            mock_args=arguments,
            module_at_test=MODULE_AT_TEST,
        )
    await process_stakeholder(stakeholder)
    if stakeholder.last_login_date:
        mock_loaders_stakeholder_organizations_access.load.assert_called_with(
            stakeholder.email
        )
        mock_stakeholders_domain_remove.assert_called_with(stakeholder.email)


@pytest.mark.parametrize(
    ["stakeholder"],
    [
        [
            Stakeholder(
                email="admin@fluidattacks.com",
                enrolled=True,
                first_name="Miguel",
                is_concurrent_session=False,
                is_registered=True,
                last_login_date=datetime.fromisoformat(
                    "2020-12-31T18:40:37+00:00"
                ),
                last_name="de Orellana",
                legal_remember=True,
                phone=None,
                registration_date=datetime.fromisoformat(
                    "2019-02-28T16:54:12+00:00"
                ),
                role="admin",
                session_key=None,
                session_token=None,
                state=StakeholderState(
                    modified_by=None,
                    modified_date=None,
                    notifications_preferences=NotificationsPreferences(
                        available=[],
                        email=[],
                        sms=[],
                        parameters=NotificationsParameters(
                            min_severity=Decimal("3.0")
                        ),
                    ),
                ),
                login=StakeholderLogin(
                    modified_by="integrates@fluidattacks.com",
                    modified_date=datetime.fromisoformat(
                        "2024-03-22T18:32:20.800014+00:00"
                    ),
                    expiration_time=2026859315,
                    browser="Chrome 114.0.0.0",
                    country_code="CO",
                    device="Linux",
                    ip_address="127.0.0.1",
                    provider="GOOGLE",
                    subject="GOOGLE#2222223333333344445555",
                ),
                tours=StakeholderTours(
                    new_group=False,
                    new_root=False,
                    welcome=False,
                ),
            )
        ],
        [
            Stakeholder(
                email="forcestesting@fluidattacks.com",
                enrolled=True,
                first_name="",
                is_concurrent_session=False,
                is_registered=True,
                last_login_date=datetime.fromisoformat(
                    "2020-12-31T18:40:37+00:00"
                ),
                last_name="",
                legal_remember=True,
                phone=None,
                registration_date=datetime.fromisoformat(
                    "2019-02-28T16:54:12+00:00"
                ),
                role="user",
                session_key=None,
                session_token=None,
                state=StakeholderState(
                    modified_by=None,
                    modified_date=None,
                    notifications_preferences=NotificationsPreferences(
                        available=[],
                        email=[],
                        sms=[],
                        parameters=NotificationsParameters(
                            min_severity=Decimal("3.0")
                        ),
                    ),
                ),
                login=StakeholderLogin(
                    modified_by="integrates@fluidattacks.com",
                    modified_date=datetime.fromisoformat(
                        "2024-03-22T18:32:20.800014+00:00"
                    ),
                    expiration_time=2026859315,
                    browser="Chrome 114.0.0.0",
                    country_code="CO",
                    device="Linux",
                    ip_address="127.0.0.1",
                    provider="GOOGLE",
                    subject="GOOGLE#2222223333333344445555",
                ),
                tours=StakeholderTours(
                    new_group=False,
                    new_root=False,
                    welcome=False,
                ),
            )
        ],
    ],
)
@patch(
    MODULE_AT_TEST + "Dataloaders.stakeholder_organizations_access",
    new_callable=AsyncMock,
)
@freeze_time("2021-02-01T00:00:00+00:00")
async def test_process_stakeholder_return(
    mock_dataloaders_stakeholder_organizations_access: AsyncMock,
    stakeholder: Stakeholder,
    mocked_data_for_module: Any,
) -> None:
    mock_dataloaders_stakeholder_organizations_access.load.return_value = (
        mocked_data_for_module(
            mock_path="Dataloaders.stakeholder_organizations_access",
            mock_args=[stakeholder.email],
            module_at_test=MODULE_AT_TEST,
        )
    )

    await process_stakeholder(stakeholder)
    mock_dataloaders_stakeholder_organizations_access.load.assert_called_with(
        stakeholder.email
    )


@freeze_time("2023-04-01T00:00:00+00:00")
@pytest.mark.parametrize(
    ["organization"],
    [
        [
            Organization(
                created_by="unknown",
                created_date=datetime.fromisoformat(
                    "2019-11-22T20:07:57+00:00"
                ),
                id="ORG#33c08ebd-2068-47e7-9673-e1aa03dc9448",
                name="kiba",
                policies=Policies(
                    modified_date=datetime.fromisoformat(
                        "2019-11-22T20:07:57+00:00"
                    ),
                    modified_by="unknown",
                    inactivity_period=90,
                    max_acceptance_days=None,
                    max_acceptance_severity=Decimal("10.0"),
                    max_number_acceptances=None,
                    min_acceptance_severity=Decimal("0.0"),
                    min_breaking_severity=Decimal("0.0"),
                    vulnerability_grace_period=None,
                ),
                state=OrganizationState(
                    aws_external_id=("82be0cf9-5aa0-4d0a-81e3-5e51424a3318"),
                    status=OrganizationStateStatus.ACTIVE,
                    modified_by="unknown",
                    modified_date=datetime.fromisoformat(
                        "2019-11-22T20:07:57+00:00"
                    ),
                    pending_deletion_date=None,
                ),
                country="Colombia",
                payment_methods=[],
            ),
        ],
    ],
)
@patch(MODULE_AT_TEST + "orgs_domain.remove_access", new_callable=AsyncMock)
@patch(
    MODULE_AT_TEST + "orgs_domain.get_stakeholders",
    new_callable=AsyncMock,
)
async def test_process_organization(
    mock_orgs_domain_get_stakeholders: AsyncMock,
    mock_orgs_domain_remove_access: AsyncMock,
    organization: Organization,
    mocked_data_for_module: Any,
) -> None:
    mock_orgs_domain_get_stakeholders.return_value = mocked_data_for_module(
        mock_path="orgs_domain.get_stakeholders",
        mock_args=[organization.id],
        module_at_test=MODULE_AT_TEST,
    )

    mock_orgs_domain_remove_access.side_effect = mocked_data_for_module(
        mock_path="orgs_domain.remove_access",
        mock_args=[],
        module_at_test=MODULE_AT_TEST,
    )

    await process_organization(organization)
    mock_orgs_domain_remove_access.assert_awaited()


@freeze_time("2023-04-01T00:00:00+00:00")
@pytest.mark.parametrize(
    ["organization"],
    [
        [
            Organization(
                created_by="organization_manager@domain.com",
                created_date=datetime.fromisoformat(
                    "2022-09-12T23:00:00+00:00"
                ),
                country="Colombia",
                id="ORG#c4fc4bde-93fa-44d1-981b-9ce16c5435e8",
                name="test_organization_1",
                policies=Policies(
                    max_acceptance_days=15,
                    modified_by="organization_manager@domain.com",
                    modified_date=datetime.fromisoformat(
                        "2022-09-12T23:00:00+00:00"
                    ),
                ),
                state=OrganizationState(
                    aws_external_id=("960aac14-7f0f-4ab7-a3b6-35e714064363"),
                    status=OrganizationStateStatus.ACTIVE,
                    modified_by="organization_manager@domain.com",
                    modified_date=datetime.fromisoformat(
                        "2022-09-12T23:00:00+00:00"
                    ),
                ),
            )
        ],
    ],
)
@patch(
    MODULE_AT_TEST + "orgs_domain.get_stakeholders",
    new_callable=AsyncMock,
)
async def test_process_organization_not_inactive_stakeholders(
    mock_orgs_domain_get_stakeholders: AsyncMock,
    organization: Organization,
    mocked_data_for_module: Any,
) -> None:
    mock_orgs_domain_get_stakeholders.return_value = mocked_data_for_module(
        mock_path="orgs_domain.get_stakeholders",
        mock_args=[organization.id],
        module_at_test=MODULE_AT_TEST,
    )

    await process_organization(organization)
    mock_orgs_domain_get_stakeholders.assert_awaited()


@patch(MODULE_AT_TEST + "process_stakeholder", new_callable=AsyncMock)
@patch(
    MODULE_AT_TEST + "stakeholders_model.get_all_stakeholders",
    new_callable=AsyncMock,
)
@patch(MODULE_AT_TEST + "process_organization", new_callable=AsyncMock)
@patch(
    MODULE_AT_TEST + "orgs_model.get_all_organizations", new_callable=AsyncMock
)
async def test_remove_inactive_stakeholders_(
    mock_orgs_model_get_all_organizations: AsyncMock,
    mock_process_organization: AsyncMock,
    mock_stakeholders_model_get_all_stakeholders: AsyncMock,
    mock_process_stakeholder: AsyncMock,
    mocked_data_for_module: Any,
) -> None:
    mocks_setup_list: list[tuple[AsyncMock, str, list[Any]]] = [
        (
            mock_orgs_model_get_all_organizations,
            "orgs_model.get_all_organizations",
            [],
        ),
        (
            mock_stakeholders_model_get_all_stakeholders,
            "stakeholders_model.get_all_stakeholders",
            [],
        ),
    ]
    mocks_setup_list_se: list[tuple[AsyncMock, str, list[Any]]] = [
        (
            mock_process_organization,
            "process_organization",
            [],
        ),
        (
            mock_process_stakeholder,
            "process_stakeholder",
            [],
        ),
    ]
    # Set up mocks' results using mocked_data_for_module fixture
    for item in mocks_setup_list:
        mock, path, arguments = item
        mock.return_value = mocked_data_for_module(
            mock_path=path,
            mock_args=arguments,
            module_at_test=MODULE_AT_TEST,
        )
    # Functions inside collect have to be mocked using side_effect
    # so that the iterations work
    for item in mocks_setup_list_se:
        mock, path, arguments = item
        mock.side_effect = mocked_data_for_module(
            mock_path=path,
            mock_args=arguments,
            module_at_test=MODULE_AT_TEST,
        )
    await remove_inactive_stakeholders()
    assert mock_orgs_model_get_all_organizations.called is True
    assert mock_stakeholders_model_get_all_stakeholders.called is True
    assert mock_process_organization.call_count == 11
    assert mock_process_stakeholder.call_count == 7


async def test_remove_access() -> None:
    organization_id = "organization_id"
    email = "test@gmail.com"
    modified_by = "integrates@fluidattacks.com"

    organization_mock = AsyncMock(id=organization_id)
    stakeholder_mock = AsyncMock(email=email)
    errors: dict[str, int] = {}

    with patch(
        MODULE_AT_TEST + "orgs_domain.remove_access"
    ) as mock_remove_access:
        mock_remove_access.side_effect = CredentialInUse()
        await _remove_access(organization_mock, stakeholder_mock, errors)
        mock_remove_access.assert_called_once_with(
            organization_id=organization_id,
            email=email,
            modified_by=modified_by,
        )
        assert "Exception - Credential is still in use by a root" in errors
        assert errors["Exception - Credential is still in use by a root"] == 1


@pytest.mark.asyncio
async def test_main() -> None:
    with patch(
        MODULE_AT_TEST + "remove_inactive_stakeholders", new_callable=AsyncMock
    ) as mock:
        await main()
        mock.assert_awaited_once()
