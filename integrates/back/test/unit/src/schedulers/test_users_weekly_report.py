from freezegun import (
    freeze_time,
)
from integrates.schedulers.users_weekly_report import (
    get_inactive_users,
    main,
    send_users_weekly_report,
)
import pytest
from test.unit.src.utils import (
    get_module_at_test,
)
from typing import (
    Any,
)
from unittest.mock import (
    AsyncMock,
    patch,
)

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"


@freeze_time("2023-09-08T14:30:00Z")
@pytest.mark.parametrize(
    ["group_stakeholders"],
    [
        [
            [
                {
                    "email": "user1@example.com",
                    "last_login": "2023-09-05T14:30:00Z",
                },
                {
                    "email": "user2@example.com",
                    "last_login": "2022-09-01T10:45:00Z",
                },
                {
                    "email": "user3@example.com",
                    "last_login": "2023-07-20T08:15:00Z",
                },
                {"email": "user4@example.com", "last_login": None},
            ]
        ]
    ],
)
def test_get_inactive_users(
    group_stakeholders: tuple[dict[str, str], ...],
) -> None:
    result = get_inactive_users(group_stakeholders=group_stakeholders)
    assert "user1@example.com" not in result


@pytest.mark.asyncio
@patch(MODULE_AT_TEST + "FI_ENVIRONMENT", "production")
@patch(
    MODULE_AT_TEST + "groups_mail.send_mail_users_weekly_report",
    new_callable=AsyncMock,
)
@patch(
    MODULE_AT_TEST + "group_access_domain.get_stakeholder_role",
    new_callable=AsyncMock,
)
@patch(
    MODULE_AT_TEST + "group_access_domain.get_group_stakeholders",
    new_callable=AsyncMock,
)
@patch(
    MODULE_AT_TEST + "orgs_domain.get_all_active_group_names",
    new_callable=AsyncMock,
)
async def test_send_users_weekly_report(
    mock_orgs_domain_get_all_active_group_names: AsyncMock,
    mock_group_access_domain_get_group_stakeholders: AsyncMock,
    mock_group_access_domain_get_stakeholder_role: AsyncMock,
    mock_groups_mail_send_mail_users_weekly_report: AsyncMock,
    mocked_data_for_module: Any,
) -> None:
    mocks = [
        mock_orgs_domain_get_all_active_group_names,
        mock_group_access_domain_get_group_stakeholders,
        mock_group_access_domain_get_stakeholder_role,
    ]
    paths = [
        "orgs_domain.get_all_active_group_names",
        "group_access_domain.get_group_stakeholders",
        "group_access_domain.get_stakeholder_role",
    ]

    callbacks = [
        mock_orgs_domain_get_all_active_group_names.assert_awaited_once,
        mock_group_access_domain_get_group_stakeholders.assert_awaited,
        mock_group_access_domain_get_stakeholder_role.assert_awaited,
        mock_groups_mail_send_mail_users_weekly_report.assert_awaited_once,
    ]

    for mock, path in zip(mocks, paths):
        mock.return_value = mocked_data_for_module(
            mock_path=path,
            mock_args=[],
            module_at_test=MODULE_AT_TEST,
        )
    await send_users_weekly_report()
    list(map(lambda x: x(), callbacks))


@pytest.mark.asyncio
async def test_main() -> None:
    with patch(
        MODULE_AT_TEST + "send_users_weekly_report", new_callable=AsyncMock
    ) as mock:
        await main()
        mock.assert_awaited_once()
