from collections.abc import (
    Iterable,
)
from datetime import (
    datetime,
)
from integrates.custom_utils.vulnerabilities import (
    get_date_last_vulns,
)
from integrates.db_model.enums import (
    Source,
    TreatmentStatus,
)
from integrates.db_model.types import (
    Treatment,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
    VulnerabilityTechnique,
    VulnerabilityToolImpact,
    VulnerabilityType,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityState,
    VulnerabilityTool,
    VulnerabilityUnreliableIndicators,
)
from integrates.schedulers.update_indicators import (
    update_indicators,
)
import pytest
from test.unit.src.utils import (
    get_module_at_test,
)
from typing import (
    Any,
)
from unittest.mock import (
    AsyncMock,
    patch,
)

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"

pytestmark = [
    pytest.mark.asyncio,
]


@pytest.mark.parametrize(
    ["vulns"],
    [
        [
            [
                Vulnerability(
                    created_by="unittest@fluidattacks.com",
                    created_date=(
                        datetime.fromisoformat("2020-01-03T17:46:10+00:00")
                    ),
                    finding_id="422286126",
                    group_name="unittesting",
                    organization_name="okada",
                    hacker_email="unittest@fluidattacks.com",
                    id="0a848781-b6a4-422e-95fa-692151e6a98f",
                    state=VulnerabilityState(
                        modified_by="unittest@fluidattacks.com",
                        modified_date=(
                            datetime.fromisoformat("2020-01-03T17:46:10+00:00")
                        ),
                        source=Source.ASM,
                        specific="12",
                        status=VulnerabilityStateStatus.VULNERABLE,
                        where="test/data/lib_path/f060/csharp.cs",
                        commit="ea871eee64cfd5ce293411efaf4d3b446d04eb4a",
                        reasons=None,
                        other_reason=None,
                        tool=VulnerabilityTool(
                            name="tool-2",
                            impact=VulnerabilityToolImpact.INDIRECT,
                        ),
                    ),
                    technique=VulnerabilityTechnique.PTAAS,
                    type=VulnerabilityType.LINES,
                    bug_tracking_system_url=None,
                    custom_severity=None,
                    developer=None,
                    event_id=None,
                    hash=None,
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    skims_method=None,
                    stream=None,
                    tags=None,
                    treatment=Treatment(
                        modified_date=(
                            datetime.fromisoformat("2020-01-03T17:46:10+00:00")
                        ),
                        status=TreatmentStatus.IN_PROGRESS,
                        acceptance_status=None,
                        accepted_until=None,
                        justification="test justification",
                        assigned="integratesuser2@gmail.com",
                        modified_by="integratesuser@gmail.com",
                    ),
                    unreliable_indicators=VulnerabilityUnreliableIndicators(
                        unreliable_reattack_cycles=0,
                        unreliable_report_date=(
                            datetime.fromisoformat("2020-01-03T17:46:10+00:00")
                        ),
                        unreliable_treatment_changes=1,
                    ),
                    verification=None,
                    zero_risk=None,
                ),
            ],
        ]
    ],
)
async def test_get_date_last_vulns(
    vulns: Iterable[Vulnerability],
) -> None:
    result = get_date_last_vulns(vulns)
    expected_output = datetime.fromisoformat("2019-12-30T17:46:10+00:00")
    assert result == expected_output


@patch(MODULE_AT_TEST + "update_group_indicators", new_callable=AsyncMock)
@patch(
    MODULE_AT_TEST + "orgs_domain.get_all_active_groups",
    new_callable=AsyncMock,
)
async def test_update_group_indicators(
    mock_orgs_domain_get_all_active_groups: AsyncMock,
    mock_update_group_indicators: AsyncMock,
    mocked_data_for_module: Any,
) -> None:
    # Set up mocks' results using mocked_data_for_module fixture
    mock_orgs_domain_get_all_active_groups.return_value = (
        mocked_data_for_module(
            mock_path="orgs_domain.get_all_active_groups",
            mock_args=[],
            module_at_test=MODULE_AT_TEST,
        )
    )
    # Functions inside collect have to be mocked using side_effect
    # so that the iterations work
    mock_update_group_indicators.side_effect = mocked_data_for_module(
        mock_path="update_group_indicators",
        mock_args=[],
        module_at_test=MODULE_AT_TEST,
    )
    await update_indicators()
    assert mock_orgs_domain_get_all_active_groups.called is True
    assert mock_update_group_indicators.call_count == 13
