from cryptography.fernet import (
    Fernet,
)
import csv
from datetime import (
    datetime,
    timedelta,
)
from freezegun import (
    freeze_time,
)
from integrates.custom_exceptions import (
    ToeLinesAlreadyUpdated,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.roots.enums import (
    RootCloningStatus,
    RootCriticality,
    RootStatus,
    RootType,
)
from integrates.db_model.roots.types import (
    GitRoot,
    GitRootCloning,
    GitRootState,
    Root,
)
from integrates.db_model.toe_lines.types import (
    SortsSuggestion,
    ToeLine,
    ToeLineState,
)
from integrates.schedulers.update_group_toe_sorts import (
    get_toe_lines_dict,
    main,
    process_group,
    update_sorts_attributes,
    update_toe_lines,
)
from integrates.toe.lines.types import (
    ToeLinesAttributesToUpdate,
)
import pytest
from test.unit.src.utils import (
    get_module_at_test,
)
from typing import (
    Any,
)
from unittest.mock import (
    AsyncMock,
    MagicMock,
    patch,
)

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"

pytestmark = [
    pytest.mark.asyncio,
]


@pytest.mark.parametrize(
    ["group_toe_lines", "group_roots"],
    [
        [
            [
                ToeLine(
                    filename="path/to/file3.ext",
                    group_name="unittesting",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    state=ToeLineState(
                        attacked_at=(
                            datetime.fromisoformat("2021-02-20T05:00:00+00:00")
                        ),
                        attacked_by="test2@test.com",
                        attacked_lines=4,
                        be_present=True,
                        be_present_until=None,
                        comments="comment 1",
                        first_attack_at=(
                            datetime.fromisoformat("2020-02-19T15:41:04+00:00")
                        ),
                        has_vulnerabilities=False,
                        last_author="user@gmail.com",
                        last_commit="e17059d1e17059d1e17"
                        "059d1e17059d1e17059d1",
                        last_commit_date=(
                            datetime.fromisoformat("2020-11-15T15:41:04+00:00")
                        ),
                        loc=350,
                        modified_by="test2@test.com",
                        modified_date=(
                            datetime.fromisoformat("2020-11-15T15:41:04+00:00")
                        ),
                        seen_at=(
                            datetime.fromisoformat("2020-02-01T15:41:04+00:00")
                        ),
                        sorts_risk_level=80,
                        sorts_risk_level_date=(
                            datetime.fromisoformat("2021-03-30T05:00:00+00:00")
                        ),
                        sorts_suggestions=[
                            SortsSuggestion(
                                finding_title="083. XML injection (XXE)",
                                probability=90,
                            ),
                            SortsSuggestion(
                                finding_title="033. Password change"
                                " without identity check",
                                probability=50,
                            ),
                        ],
                    ),
                    seen_first_time_by=None,
                )
            ],
            [
                GitRoot(
                    cloning=GitRootCloning(
                        modified_by="Violeta Parra",
                        reason="Reason for git root cloning",
                        modified_date=datetime.now() - timedelta(days=4),
                        status=RootCloningStatus.CLONING,
                    ),
                    created_by="root creator",
                    created_date=datetime.now(),
                    group_name="Some Group Name",
                    id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    state=GitRootState(
                        branch="trunk",
                        criticality=RootCriticality.LOW,
                        modified_by="some git root state modifier",
                        modified_date=datetime.now(),
                        nickname="status_nickname",
                        includes_health_check=False,
                        url="https://url.org",
                        status=RootStatus.ACTIVE,
                    ),
                    type=RootType.GIT,
                    organization_name="Some org name",
                )
            ],
        ],
    ],
)
def test_get_toe_lines_dict(
    group_toe_lines: list[ToeLine],
    group_roots: list[Root],
) -> None:
    result = get_toe_lines_dict(group_toe_lines, group_roots)
    assert "status_nickname/path/to/file3.ext" in result


@freeze_time("2020-12-01")
@pytest.mark.parametrize(
    ["group_name"],
    [["unittesting"]],
)
@patch(MODULE_AT_TEST + "update_sorts_attributes", new_callable=AsyncMock)
@patch(MODULE_AT_TEST + "S3_BUCKET.Object")
@patch(MODULE_AT_TEST + "tempfile.TemporaryDirectory")
@patch(MODULE_AT_TEST + "Dataloaders.group_toe_lines", new_callable=AsyncMock)
async def test_process_group(
    mock_dataloaders_group_toe_lines: AsyncMock,
    mock_tempfile_temporary_directory: MagicMock,
    mock_s3_bucket_object: MagicMock,
    mock_update_sorts_attributes: AsyncMock,
    *,
    group_name: str,
    mocked_data_for_module: Any,
) -> None:
    mocks_setup_list: list[tuple[AsyncMock, str, list[Any]]] = [
        (
            mock_dataloaders_group_toe_lines.load_nodes,
            "Dataloaders.group_toe_lines",
            [group_name],
        ),
        (
            mock_tempfile_temporary_directory.return_value.__enter__,
            "tempfile.TemporaryDirectory",
            [group_name],
        ),
        (
            mock_s3_bucket_object.return_value.download_file,
            "S3_BUCKET.Object",
            [group_name],
        ),
        (
            mock_update_sorts_attributes,
            "update_sorts_attributes",
            [group_name, datetime.now()],
        ),
    ]
    # Set up mocks' results using mocked_data_for_module fixture
    for item in mocks_setup_list:
        mock, path, arguments = item
        mock.return_value = mocked_data_for_module(
            mock_path=path,
            mock_args=arguments,
            module_at_test=MODULE_AT_TEST,
        )
    await process_group(group_name=group_name, current_date=datetime.now())

    mocks_list = [mock[0] for mock in mocks_setup_list]
    assert all(mock_object.called is True for mock_object in mocks_list)


@patch(MODULE_AT_TEST + "process_group", new_callable=AsyncMock)
@patch(
    MODULE_AT_TEST + "orgs_domain.get_all_active_group_names",
    new_callable=AsyncMock,
)
async def test_update_group_toe_sorts(
    mock_orgs_domain_get_all_active_group_names: AsyncMock,
    mock_process_group: AsyncMock,
    mocked_data_for_module: Any,
) -> None:
    # Set up mock's result using mocked_data_for_module fixture
    mock_orgs_domain_get_all_active_group_names.return_value = (
        mocked_data_for_module(
            mock_path="orgs_domain.get_all_active_group_names",
            mock_args=[],
            module_at_test=MODULE_AT_TEST,
        )
    )

    # Functions inside collect have to be mocked using side_effect
    # so that the iterations work
    mock_process_group.side_effect = mocked_data_for_module(
        mock_path="process_group",
        mock_args=[],
        module_at_test=MODULE_AT_TEST,
    )
    await main()
    assert mock_orgs_domain_get_all_active_group_names.called is True
    assert mock_process_group.call_count == 11


@pytest.mark.parametrize(
    (
        "current_value",
        "attributes",
    ),
    (
        (
            ToeLine(
                filename="test/new.new",
                group_name="unittesting",
                root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                state=ToeLineState(
                    attacked_at=datetime.fromisoformat(
                        "2021-08-01T05:00:00+00:00"
                    ),
                    attacked_by="hacker2@test.com",
                    attacked_lines=434,
                    be_present=True,
                    be_present_until=None,
                    comments="comment test 2",
                    first_attack_at=datetime.fromisoformat(
                        "2020-08-01T05:00:00+00:00"
                    ),
                    has_vulnerabilities=False,
                    last_author="customer2@gmail.com",
                    last_commit="f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c4",
                    last_commit_date=datetime.fromisoformat(
                        "2020-08-01T05:00:00+00:00"
                    ),
                    loc=1111,
                    modified_by="hacker2@test.com",
                    modified_date=datetime.fromisoformat(
                        "2022-08-01T05:00:00+00:00"
                    ),
                    seen_at=datetime.fromisoformat(
                        "2019-08-01T05:00:00+00:00"
                    ),
                    sorts_risk_level=50,
                    sorts_priority_factor=70,
                    sorts_risk_level_date=None,
                    sorts_suggestions=None,
                ),
                seen_first_time_by=None,
            ),
            ToeLinesAttributesToUpdate(
                attacked_at=datetime.fromisoformat(
                    "2021-08-01T05:00:00+00:00"
                ),
                attacked_by="hacker2@test.com",
                attacked_lines=434,
                comments="comment test 2",
                last_author="customer2@gmail.com",
                has_vulnerabilities=False,
                loc=1111,
                last_commit="f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c4",
                last_commit_date=datetime.fromisoformat(
                    "2020-08-01T05:00:00+00:00"
                ),
                seen_at=datetime.fromisoformat("2019-08-01T05:00:00+00:00"),
                sorts_risk_level=50,
                sorts_priority_factor=70,
            ),
        ),
    ),
)
@patch(MODULE_AT_TEST + "toe_lines_model.update_state", new_callable=AsyncMock)
@freeze_time("2022-08-01T05:00:00+00:00")
async def test_update_toe_lines(
    mock_toe_lines_model_update_state: AsyncMock,
    current_value: ToeLine,
    attributes: ToeLinesAttributesToUpdate,
    mocked_data_for_module: Any,
) -> None:
    # Set up mock's result using mock_data_for_module fixture
    mock_toe_lines_model_update_state.return_value = mocked_data_for_module(
        mock_path="toe_lines_model.update_state",
        mock_args=[current_value, attributes],
        module_at_test=MODULE_AT_TEST,
    )

    await update_toe_lines(
        current_value,
        sorts_risk_level=50,
        sorts_priority_factor=70,
        sorts_risk_level_date=datetime.now(),
    )

    mock_toe_lines_model_update_state.assert_called()
    assert mock_toe_lines_model_update_state.called is True


@freeze_time("2022-08-01T05:00:00+00:00")
@pytest.mark.parametrize(
    (
        "group_name",
        "current_date",
        "predicted_files",
        "fernet",
    ),
    (
        (
            "unittesting",
            datetime.now(),
            [
                {
                    "file": """
                    gAAAAABj0UZ1bIlRLZVNJAYIjCwNjk5LoweFYYuZGqKmSpe1K8UPRVd7
                    yqZ9p7RV8PmCp9ISu1kD5nrpsFY-GLRJKsWTaOYlZCVw6wr7f8Tn-
                    cx_YgrtYXY=
                    """,
                    "prob_vuln": "0.0",
                }
            ],
            Fernet("cudK8BSD-eApPCivGAX05i1NOqjiCwu9xXiRRKhB1CY="),
        ),
    ),
)
async def test_update_sorts_attributes(
    group_name: str,
    current_date: datetime,
    predicted_files: csv.DictReader,
    fernet: Fernet,
    mocked_data_for_module: Any,
) -> None:
    toe_lines: list[ToeLine] = [
        ToeLine(
            filename="front/test/index.html",
            group_name="unittesting",
            root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
            state=ToeLineState(
                attacked_at=datetime.fromisoformat(
                    "2021-02-20T05:00:00+00:00"
                ),
                attacked_by="test2@test.com",
                attacked_lines=4,
                be_present=True,
                be_present_until=None,
                comments="comment test",
                first_attack_at=datetime.fromisoformat(
                    "2020-02-19T15:41:04+00:00"
                ),
                has_vulnerabilities=False,
                last_author="user@gmail.com",
                last_commit="e17059d1e17059d1e17059d1e17059d1e118059d1",
                last_commit_date=datetime.fromisoformat(
                    "2020-11-15T15:41:04+00:00"
                ),
                loc=350,
                modified_by="test2@test.com",
                modified_date=datetime.fromisoformat(
                    "2020-11-15T15:41:04+00:00"
                ),
                seen_at=datetime.fromisoformat("2020-02-01T15:41:04+00:00"),
                sorts_risk_level=80,
                sorts_risk_level_date=datetime.fromisoformat(
                    "2021-03-30T05:00:00+00:00"
                ),
            ),
            seen_first_time_by=None,
        ),
        ToeLine(
            filename="back/src/test/index.js",
            group_name="unittesting",
            root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
            state=ToeLineState(
                attacked_at=datetime.fromisoformat(
                    "2021-02-20T05:00:00+00:00"
                ),
                attacked_by="test2@test.com",
                attacked_lines=4,
                be_present=True,
                be_present_until=None,
                comments="comment test 1",
                first_attack_at=datetime.fromisoformat(
                    "2020-02-19T15:41:04+00:00"
                ),
                has_vulnerabilities=False,
                last_author="user@gmail.com",
                last_commit="f9e4beba70c4f34d6117c3b0c23ebe6b2bge66c2",
                last_commit_date=datetime.fromisoformat(
                    "2020-11-15T15:41:04+00:00"
                ),
                loc=180,
                modified_by="test2@test.com",
                modified_date=datetime.fromisoformat(
                    "2020-11-15T15:41:04+00:00"
                ),
                seen_at=datetime.fromisoformat("2020-02-01T15:41:04+00:00"),
                sorts_risk_level=-1,
                sorts_risk_level_date=datetime.fromisoformat(
                    "2021-02-20T05:00:00+00:00"
                ),
            ),
            seen_first_time_by=None,
        ),
    ]
    loaders: Any = get_new_context()
    loaders.group_roots.load = AsyncMock(
        return_value=mocked_data_for_module(
            mock_path="Dataloaders.group_roots.load",
            mock_args=[group_name],
            module_at_test=MODULE_AT_TEST,
        )
    )

    with patch(
        MODULE_AT_TEST + "get_active_toe_lines"
    ) as mock_get_active_toe_lines:
        with pytest.raises(ToeLinesAlreadyUpdated):
            mock_get_active_toe_lines.return_value = toe_lines
            await update_sorts_attributes(
                group_name, current_date, toe_lines, predicted_files, fernet
            )
