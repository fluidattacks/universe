from freezegun import (
    freeze_time,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.trials.types import (
    TrialMetadataToUpdate,
)
from integrates.schedulers.reminder_notification import (
    is_trial_end,
    main,
    send_reminder_notification,
)
from integrates.trials import (
    domain as trials_domain,
)
import pytest
from test.unit.src.utils import (
    get_module_at_test,
)
from typing import (
    Any,
)
from unittest.mock import (
    AsyncMock,
    patch,
)

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"


pytestmark = [
    pytest.mark.asyncio,
]


@pytest.mark.asyncio
@pytest.mark.parametrize(
    [
        "emails",
        "outputs",
    ],
    [
        [
            ["notregistered@unknown.com", "unittest@fluidattacks.com"],
            [False, True],
        ],
    ],
)
async def test_is_trial_end(emails: list[str], outputs: list[bool]) -> None:
    loaders = get_new_context()
    for index, email in enumerate(emails):
        assert await is_trial_end(loaders, email) == outputs[index]

    await trials_domain.update_metadata(
        email=emails[1],
        metadata=TrialMetadataToUpdate(completed=False),
    )
    assert await is_trial_end(get_new_context(), emails[1]) == outputs[0]


@patch(MODULE_AT_TEST + "FI_ENVIRONMENT", "production")
@patch(
    MODULE_AT_TEST + "orgs_domain.get_all_active_group_names",
    new_callable=AsyncMock,
)
@freeze_time("2023-09-18T10:00:00.0")
async def test_send_reminder_notification(
    mock_orgs_domain_get_all_active_group_names: AsyncMock,
    mocked_data_for_module: Any,
) -> None:
    mock_orgs_domain_get_all_active_group_names.return_value = (
        mocked_data_for_module(
            mock_path="orgs_domain.get_all_active_group_names",
            mock_args=[],
            module_at_test=MODULE_AT_TEST,
        )
    )

    await send_reminder_notification()
    mock_orgs_domain_get_all_active_group_names.assert_awaited()


@pytest.mark.asyncio
async def test_main() -> None:
    with patch(
        MODULE_AT_TEST + "send_reminder_notification",
        new_callable=AsyncMock,
    ) as mock:
        await main()
        mock.assert_awaited_once()
