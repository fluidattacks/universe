from datetime import (
    datetime,
)
from freezegun import (
    freeze_time,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.db_model.events.enums import (
    EventStateStatus,
    EventType,
)
from integrates.db_model.events.types import (
    Event,
    EventEvidences,
    EventState,
)
from integrates.schedulers.event_report import (
    days_to_date,
    main,
    send_event_report,
)
import pytest
from test.unit.src.utils import (
    get_module_at_test,
)
from typing import (
    Any,
)
from unittest.mock import (
    AsyncMock,
    patch,
)

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"


@freeze_time("2022-09-19")
def test_days_to_date() -> None:
    date = datetime.fromisoformat("2022-09-10T00:00:00+00:00")
    days = days_to_date(date)
    assert days == 9


@freeze_time("2022-09-19")
@pytest.mark.asyncio
@patch(MODULE_AT_TEST + "FI_ENVIRONMENT", "production")
async def test_send_event_report() -> None:
    new_event = Event(
        client="c2ee2d15-04ab-4f39-9795-fbe30cdeee86",
        created_by="unittesting@fluidattacks.com",
        created_date=datetime_utils.get_utc_now(),
        description="Something happened.",
        event_date=datetime.fromisoformat("2022-09-10T00:00:00+00:00"),
        evidences=EventEvidences(),
        group_name="unittesting",
        hacker="unittesting@fluidattacks.com",
        id="11111111",
        state=EventState(
            modified_by="unittesting@fluidattacks.com",
            modified_date=datetime.fromisoformat("2022-09-10T00:00:00+00:00"),
            status=EventStateStatus.CREATED,
        ),
        type=EventType["AUTHORIZATION_SPECIAL_ATTACK"],
    )
    unsolved_events_mock = [new_event]
    with patch(
        "integrates.schedulers.event_report.events_domain.get_unsolved_events",
        coroutine=unsolved_events_mock,
    ) as mock_event:
        await send_event_report()
        assert mock_event.called is True


@freeze_time("2022-09-20")
@pytest.mark.asyncio
@patch(
    MODULE_AT_TEST + "events_mail.send_mail_event_report",
    new_callable=AsyncMock,
)
@patch(
    MODULE_AT_TEST + "events_domain.get_unsolved_events",
    new_callable=AsyncMock,
)
async def test_send_event_report_filtered(
    mock_get_unsolved_events: AsyncMock,
    mock_send_mail: AsyncMock,
    mocked_data_for_module: Any,
) -> None:
    group_name: str = "oneshottest"
    mocks_setup_list: list[tuple[AsyncMock, str, list[Any]]] = [
        (
            mock_get_unsolved_events,
            "events_domain.get_unsolved_events",
            [group_name],
        ),
        (
            mock_send_mail,
            "events_mail.send_mail_event_report",
            [],
        ),
    ]

    # Set up mocks' results using mocked_data_for_module fixture
    for item in mocks_setup_list:
        mock, path, arguments = item
        mock.return_value = mocked_data_for_module(
            mock_path=path,
            mock_args=arguments,
            module_at_test=MODULE_AT_TEST,
        )

    await send_event_report()
    assert mock_get_unsolved_events.called is True
    assert mock_send_mail.called is True


@pytest.mark.asyncio
async def test_main() -> None:
    with patch(
        MODULE_AT_TEST + "send_event_report",
        new_callable=AsyncMock,
    ) as mock:
        await main()
        mock.assert_awaited_once()
