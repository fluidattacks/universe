from aioextensions import (
    collect,
)
from datetime import (
    datetime,
    timedelta,
    timezone,
)
from integrates.context import (
    FI_MARKETPLACE_PRODUCT_CODE,
)
from integrates.custom_exceptions import (
    InvalidAWSMarketplaceProductCode,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.groups.enums import (
    GroupManaged,
)
from integrates.db_model.marketplace.enums import (
    AWSMarketplacePricingDimension,
    AWSMarketplaceSubscriptionStatus,
)
from integrates.db_model.marketplace.types import (
    AWSMarketplaceSubscriptionEntitlement,
    AWSMarketplaceSubscriptionSNSUpdate,
)
from integrates.groups.domain import (
    get_groups_owned_by_user,
)
from integrates.marketplace.core import (
    ASYNC_SERVER_TASK_ERROR_1,
    check_user_subscription_usage_surpass_contract,
    create_marketplace_subscription,
    link_user_marketplace_subscription,
    process_marketplace_sns_update,
    resolve_aws_marketplace_token,
    update_marketplace_subscription_terms,
)
from integrates.marketplace.utils import (
    get_groups_entitlement,
)
import pytest
from starlette.requests import (
    Request,
)
from typing import (
    Any,
)
from unittest.mock import (
    patch,
)


@pytest.mark.asyncio
@pytest.mark.changes_db
async def test_core() -> None:
    loaders = get_new_context()
    aws_account_ids = ["426493693614", "019083691449", "856428009671"]
    aws_customer_ids = ["bem14zlbykR", "81rRj1ZqkQz", "wMC7if83Cgr"]
    contracted_groups = [2, 5, 15]

    await collect(
        create_marketplace_subscription(
            aws_account_id=aws_account_id,
            aws_customer_id=aws_customer_id,
            has_free_trial=False,
            entitlements=[
                AWSMarketplaceSubscriptionEntitlement(
                    dimension=AWSMarketplacePricingDimension.GROUPS,
                    expiration_date=(
                        datetime.now(tz=timezone.utc) + timedelta(days=365)
                    ),
                    value=groups,
                )
            ],
            private_offer_id=None,
        )
        for aws_account_id, aws_customer_id, groups in zip(
            aws_account_ids, aws_customer_ids, contracted_groups
        )
    )

    subscriptions = await loaders.aws_marketplace_subscriptions.load_many(
        aws_customer_ids + ["test"]
    )
    assert len(subscriptions) == 4
    assert subscriptions[-1] is None
    assert all(
        (
            get_groups_entitlement(subscription).value == groups
            and subscription.aws_account_id == aws_account_id
            and subscription.state.status
            == AWSMarketplaceSubscriptionStatus.PENDING
        )
        for subscription, aws_account_id, groups in zip(
            subscriptions[:-1], aws_account_ids, contracted_groups
        )
        if subscription is not None
    )

    subscription_to_update = subscriptions[0]
    assert subscription_to_update is not None
    await update_marketplace_subscription_terms(
        entitlements=[
            AWSMarketplaceSubscriptionEntitlement(
                dimension=AWSMarketplacePricingDimension.GROUPS,
                expiration_date=(
                    datetime.now(tz=timezone.utc) + timedelta(days=365)
                ),
                value=20,
            )
        ],
        subscription=subscription_to_update,
    )

    loaders.aws_marketplace_subscriptions.clear_all()
    subscription = await loaders.aws_marketplace_subscriptions.load(
        aws_customer_ids[0]
    )
    assert subscription is not None
    assert get_groups_entitlement(subscription).value == 20


@pytest.mark.asyncio
@pytest.mark.changes_db
async def test_link_aws_marketplace_subscription() -> None:
    loaders = get_new_context()
    aws_customer_id = "D1Hv8MM4v6Q"
    email = "integratesuser@gmail.com"

    user = await loaders.stakeholder.load(email)
    subscription = await loaders.aws_marketplace_subscriptions.load(
        aws_customer_id
    )
    assert user is not None
    assert subscription is not None
    assert subscription.user is None
    assert user.aws_customer_id is None

    await link_user_marketplace_subscription(
        aws_customer_id=aws_customer_id, email=email, loaders=loaders
    )

    loaders.stakeholder.clear_all()
    loaders.aws_marketplace_subscriptions.clear_all()

    user = await loaders.stakeholder.load(email)
    subscription = await loaders.aws_marketplace_subscriptions.load(
        aws_customer_id
    )
    assert user is not None
    assert subscription is not None
    assert user.aws_customer_id == aws_customer_id
    assert subscription.user == email


@pytest.mark.asyncio
@pytest.mark.changes_db
@pytest.mark.parametrize(
    ("sns_update", "entitlements_response"),
    (
        (
            {
                "action": "entitlement-update",
                "customer-identifier": "Yk4MNZMV6CY",
                "product-code": FI_MARKETPLACE_PRODUCT_CODE,
            },
            {
                "Entitlements": [
                    {
                        "Dimension": "groups",
                        "ExpirationDate": (
                            datetime.now(tz=timezone.utc) + timedelta(days=365)
                        ),
                        "Value": {
                            "IntegerValue": 3,
                        },
                    }
                ]
            },
        ),
        (
            {
                "action": "subscribe-fail",
                "customer-identifier": "Yk4MNZMV6CY",
                "product-code": FI_MARKETPLACE_PRODUCT_CODE,
            },
            None,
        ),
        (
            {
                "action": "subscribe-success",
                "customer-identifier": "Yk4MNZMV6CY",
                "product-code": FI_MARKETPLACE_PRODUCT_CODE,
            },
            None,
        ),
        (
            {
                "action": "unsubscribe-pending",
                "customer-identifier": "Yk4MNZMV6CY",
                "product-code": FI_MARKETPLACE_PRODUCT_CODE,
            },
            None,
        ),
        (
            {
                "action": "unsubscribe-success",
                "customer-identifier": "Yk4MNZMV6CY",
                "product-code": FI_MARKETPLACE_PRODUCT_CODE,
            },
            None,
        ),
    ),
)
async def test_process_sns_updates(
    sns_update: AWSMarketplaceSubscriptionSNSUpdate,
    entitlements_response: dict[str, list[dict[str, Any]]] | None,
) -> None:
    loaders = get_new_context()
    with patch(
        "integrates.marketplace.core.get_marketplace_entitlement_client",
    ) as ent_patch:
        ent_patch.return_value.exceptions.InternalServiceErrorException = (
            Exception
        )
        ent_patch.return_value.exceptions.ThrottlingException = Exception

        ent_patch.return_value.get_entitlements.return_value = (
            entitlements_response
        )
        await process_marketplace_sns_update(
            loaders=loaders, message=sns_update
        )

        loaders.aws_marketplace_subscriptions.clear_all()
        subscription = await loaders.aws_marketplace_subscriptions.load(
            sns_update["customer-identifier"]
        )
        assert subscription is not None
        if sns_update["action"] == "entitlement-update":
            assert entitlements_response is not None
            assert (
                subscription.state.status
                == AWSMarketplaceSubscriptionStatus.PENDING
            )
            assert (
                get_groups_entitlement(subscription).value
                == entitlements_response["Entitlements"][0]["Value"][
                    "IntegerValue"
                ]
            )
        elif sns_update["action"] == "subscribe-fail":
            assert (
                subscription.state.status
                == AWSMarketplaceSubscriptionStatus.FAILED
            )
        elif sns_update["action"] == "subscribe-success":
            assert (
                subscription.state.status
                == AWSMarketplaceSubscriptionStatus.ACTIVE
            )
        elif sns_update["action"] == "unsubscribe-success":
            assert (
                subscription.state.status
                == AWSMarketplaceSubscriptionStatus.EXPIRED
            )


@pytest.mark.asyncio
@pytest.mark.changes_db
@pytest.mark.parametrize(
    ("sns_update", "entitlements_response"),
    (
        (
            {
                "action": "entitlement-update",
                "customer-identifier": "oA7gWjeOpOH",
                "product-code": FI_MARKETPLACE_PRODUCT_CODE,
            },
            {"Entitlements": []},
        ),
        (
            {
                "action": "subscribe-fail",
                "customer-identifier": "2WgAMK3VR4B",
                "product-code": FI_MARKETPLACE_PRODUCT_CODE,
            },
            None,
        ),
        (
            {
                "action": "subscribe-success",
                "customer-identifier": "2WgAMK3VR4B",
                "product-code": FI_MARKETPLACE_PRODUCT_CODE,
            },
            None,
        ),
        (
            {
                "action": "unsubscribe-pending",
                "customer-identifier": "2WgAMK3VR4B",
                "product-code": FI_MARKETPLACE_PRODUCT_CODE,
            },
            None,
        ),
        (
            {
                "action": "unsubscribe-success",
                "customer-identifier": "2WgAMK3VR4B",
                "product-code": FI_MARKETPLACE_PRODUCT_CODE,
            },
            None,
        ),
    ),
)
async def test_process_sns_updates_failed(
    sns_update: AWSMarketplaceSubscriptionSNSUpdate,
    entitlements_response: dict[str, list[dict[str, Any]]] | None,
) -> None:
    loaders = get_new_context()
    with patch(
        "integrates.marketplace.core.get_marketplace_entitlement_client",
    ) as ent_patch:
        ent_patch.return_value.exceptions.InternalServiceErrorException = (
            Exception
        )
        ent_patch.return_value.exceptions.ThrottlingException = Exception

        ent_patch.return_value.get_entitlements.return_value = (
            entitlements_response
        )
        success, details = await process_marketplace_sns_update(
            loaders=loaders, message=sns_update
        )
        if sns_update["action"] != "unsubscribe-pending":
            assert not success

            if sns_update["action"] == "entitlement-update":
                assert details == (
                    "No entitlements found for customer "
                    f"{sns_update['customer-identifier']}"
                )
            else:
                assert details == ASYNC_SERVER_TASK_ERROR_1
        else:
            assert success


@pytest.mark.asyncio
@pytest.mark.changes_db
@pytest.mark.parametrize(
    ("resolve_response", "entitlements_response", "raises_exception"),
    (
        (
            {
                "CustomerAWSAccountId": "845798736664",
                "CustomerIdentifier": "beekT0YP3P0",
                "ProductCode": FI_MARKETPLACE_PRODUCT_CODE,
            },
            {
                "Entitlements": [
                    {
                        "Dimension": "groups",
                        "ExpirationDate": (
                            datetime.now(tz=timezone.utc) + timedelta(days=365)
                        ),
                        "Value": {
                            "IntegerValue": 13,
                        },
                    }
                ]
            },
            False,
        ),
        (
            {
                "CustomerAWSAccountId": "394130994830",
                "CustomerIdentifier": "f5ii08ts6uk",
                "ProductCode": "fnwttj5uonnvlpfabsmovrcsq",
            },
            {
                "Entitlements": [
                    {
                        "Dimension": "groups",
                        "ExpirationDate": (
                            datetime.now(tz=timezone.utc) + timedelta(days=365)
                        ),
                        "Value": {
                            "IntegerValue": 2,
                        },
                    }
                ]
            },
            True,
        ),
    ),
)
async def test_resolve_aws_token(
    resolve_response: dict[str, str],
    entitlements_response: dict[str, list[dict[str, Any]]],
    raises_exception: bool,
) -> None:
    loaders = get_new_context()
    subscription = await loaders.aws_marketplace_subscriptions.load(
        resolve_response["CustomerIdentifier"]
    )

    if raises_exception:
        assert subscription is None
    else:
        assert subscription is not None
        assert get_groups_entitlement(subscription).value == 7

    with patch(
        "integrates.marketplace.core.get_marketplace_metering_client",
    ) as resolve_patch, patch(
        "integrates.marketplace.core.get_marketplace_entitlement_client",
    ) as ent_patch:
        resolve_patch.return_value.exceptions.DisabledApiException = Exception
        resolve_patch.return_value.exceptions.ExpiredTokenException = Exception
        resolve_patch.return_value.exceptions.InternalServiceErrorException = (
            Exception
        )
        resolve_patch.return_value.exceptions.InvalidTokenException = Exception
        resolve_patch.return_value.exceptions.ThrottlingException = Exception

        ent_patch.return_value.exceptions.InternalServiceErrorException = (
            Exception
        )
        ent_patch.return_value.exceptions.ThrottlingException = Exception

        resolve_patch.return_value.resolve_customer.return_value = (
            resolve_response
        )
        ent_patch.return_value.get_entitlements.return_value = (
            entitlements_response
        )

        if raises_exception:
            with pytest.raises(InvalidAWSMarketplaceProductCode):
                await resolve_aws_marketplace_token(
                    request=Request(scope={"headers": {}, "type": "http"}),
                    token="token",
                )
        else:
            await resolve_aws_marketplace_token(
                request=Request(scope={"headers": {}, "type": "http"}),
                token="token",
            )
            loaders.aws_marketplace_subscriptions.clear_all()
            subscription = await loaders.aws_marketplace_subscriptions.load(
                resolve_response["CustomerIdentifier"]
            )
            assert subscription is not None
            assert (
                get_groups_entitlement(subscription).value
                == entitlements_response["Entitlements"][0]["Value"][
                    "IntegerValue"
                ]
            )
            assert (
                subscription.aws_account_id
                == resolve_response["CustomerAWSAccountId"]
            )
            assert (
                subscription.aws_customer_id
                == resolve_response["CustomerIdentifier"]
            )


@pytest.mark.asyncio
@pytest.mark.changes_db
async def test_group_suspension_unsubscribe() -> None:
    email = "integratesmanager@gmail.com"

    loaders = get_new_context()
    user = await loaders.stakeholder.load(email)
    assert user is not None
    assert user.aws_customer_id is not None

    subscription = await loaders.aws_marketplace_subscriptions.load(
        user.aws_customer_id
    )
    assert subscription is not None
    assert subscription.user == email
    assert get_groups_entitlement(subscription).value == 1
    assert await check_user_subscription_usage_surpass_contract(
        email=email, loaders=loaders
    )

    owned_groups_names = await get_groups_owned_by_user(
        loaders=loaders, email=email
    )
    assert all(
        group is not None and group.state.managed == GroupManaged.MANAGED
        for group in await loaders.group.load_many(owned_groups_names)
    )

    await process_marketplace_sns_update(
        loaders=loaders,
        message={
            "action": "unsubscribe-success",
            "customer-identifier": user.aws_customer_id,
            "product-code": FI_MARKETPLACE_PRODUCT_CODE,
        },
    )
    loaders.group.clear_all()

    assert all(
        group is not None and group.state.managed == GroupManaged.UNDER_REVIEW
        for group in await loaders.group.load_many(owned_groups_names)
    )
