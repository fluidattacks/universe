from datetime import (
    datetime,
)
from decimal import (
    Decimal,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.enums import (
    Source,
    TreatmentStatus,
)
from integrates.db_model.findings.enums import (
    FindingStateStatus,
)
from integrates.db_model.findings.types import (
    Finding,
    FindingState,
)
from integrates.db_model.organization_finding_policies.enums import (
    PolicyStateStatus,
)
from integrates.db_model.organization_finding_policies.types import (
    OrgFindingPolicy,
    OrgFindingPolicyRequest,
    OrgFindingPolicyState,
)
from integrates.db_model.types import (
    SeverityScore,
    Treatment,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
    VulnerabilityTechnique,
    VulnerabilityType,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityState,
)
from integrates.organizations_finding_policies.domain import (
    _add_accepted_treatment,
    _add_new_treatment,
    _add_tags_to_vulnerabilities,
    _apply_finding_policy,
    update_finding_policy_in_groups,
)
import pytest
from test.unit.src.utils import (
    get_module_at_test,
)
from typing import (
    Any,
)
from unittest.mock import (
    AsyncMock,
    patch,
)

# Constants
pytestmark = [
    pytest.mark.asyncio,
]

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"


async def test_get_by_id() -> None:
    loaders: Dataloaders = get_new_context()
    org_name = "okada"
    finding_policy_id = "8b35ae2a-56a1-4f64-9da7-6a552683bf46"
    assert await loaders.organization_finding_policy.load(
        OrgFindingPolicyRequest(
            organization_name=org_name,
            policy_id=finding_policy_id,
        )
    ) == OrgFindingPolicy(
        id="8b35ae2a-56a1-4f64-9da7-6a552683bf46",
        organization_name="okada",
        name="007. Cross-site request forgery",
        state=OrgFindingPolicyState(
            modified_date=datetime.fromisoformat("2021-04-26T13:37:10+00:00"),
            modified_by="test2@test.com",
            status=PolicyStateStatus.APPROVED,
        ),
        tags=set(),
        treatment_acceptance=(TreatmentStatus.ACCEPTED_UNDEFINED),
    )


async def test_get_finding_policies_by_org_name() -> None:
    loaders: Dataloaders = get_new_context()
    org_name = "okada"
    org_findings_policies = await loaders.organization_finding_policies.load(
        org_name
    )
    org_findings_policies.sort()

    assert org_findings_policies == [
        OrgFindingPolicy(
            id="5f1de801-5c4b-428a-ba33-6a1b8d63e45c",
            organization_name="okada",
            name="010. Stored cross-site scripting (XSS)",
            state=OrgFindingPolicyState(
                modified_date=datetime.fromisoformat(
                    "2024-04-16T13:37:10+00:00"
                ),
                modified_by="test2@test.com",
                status=PolicyStateStatus.APPROVED,
            ),
            tags=set(),
            treatment_acceptance=(TreatmentStatus.ACCEPTED),
        ),
        OrgFindingPolicy(
            id="8b35ae2a-56a1-4f64-9da7-6a552683bf46",
            organization_name="okada",
            name="007. Cross-site request forgery",
            state=OrgFindingPolicyState(
                modified_date=datetime.fromisoformat(
                    "2021-04-26T13:37:10+00:00"
                ),
                modified_by="test2@test.com",
                status=PolicyStateStatus.APPROVED,
            ),
            tags=set(),
            treatment_acceptance=(
                TreatmentStatus.ACCEPTED_UNDEFINED
            ),
        ),
    ]


@pytest.mark.parametrize(
    [
        "email",
        "finding_name",
        "group_names",
        "status",
        "tags",
        "return_values",
    ],
    [
        [
            "user@wmail.org",
            "Some amazing finding",
            ["group1", "group2", "group3"],
            PolicyStateStatus.ACTIVE,
            set(["tag1", "tag2", "tag3"]),
            {
                "group_findings": {
                    "load_many_chained": [
                        Finding(
                            group_name="group",
                            id="finding_id",
                            severity_score=SeverityScore(
                                base_score=Decimal("4.5"),
                                temporal_score=Decimal("4.1"),
                                cvss_v3="CVSS:3.1/AV:P/AC:H/PR:L/UI:N/S:C/C:L/"
                                "I:L/A:L/E:P/RL:O/CR:L/AR:H/MAV:N/MAC:H/MPR:H/"
                                "MUI:R/MS:U/MC:L/MA:L",
                                threat_score=Decimal("1.1"),
                                cvssf_v4=Decimal("0.018"),
                                cvss_v4="CVSS:4.0/AV:P/AC:H/AT:N/PR:L/UI:N/"
                                "VC:L/VI:L/VA:L/SC:L/SI:L/SA:L/E:P/AR:H/MAV:N/"
                                "MAC:H/MPR:H/MUI:P/MVC:L/MVA:L",
                                cvssf=Decimal("1.149"),
                            ),
                            state=FindingState(
                                modified_by="some_modifier",
                                modified_date=datetime.now(),
                                source=Source.CUSTOMER,
                                status=FindingStateStatus.CREATED,
                            ),
                            title="Some amazing finding",
                            attack_vector_description="Description",
                        )
                    ]
                },
                "finding_vulnerabilities_released_nzr": {
                    "load_many_chained": [
                        Vulnerability(
                            created_by="Some vuln creator",
                            created_date=datetime.now(),
                            finding_id="finding_id",
                            group_name="group",
                            hacker_email="yourfavoritehacker@fluidattacks.com",
                            id="vuln123",
                            organization_name="your org name",
                            root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                            state=VulnerabilityState(
                                modified_by="modifier",
                                modified_date=datetime.now(),
                                source=Source.ANALYST,
                                specific="spec",
                                status=VulnerabilityStateStatus.MASKED,
                                where="somewhere",
                            ),
                            technique=VulnerabilityTechnique.PTAAS,
                            type=VulnerabilityType.PORTS,
                        )
                    ]
                },
            },
        ]
    ],
)
@patch(MODULE_AT_TEST + "_apply_finding_policy", new_callable=AsyncMock)
async def test_update_finding_policy_in_groups(
    mock_apply_finding_policy: AsyncMock,
    *,
    email: str,
    finding_name: str,
    group_names: list[str],
    status: PolicyStateStatus,
    tags: set[str],
    return_values: dict,
) -> None:
    dataloaders: Any = get_new_context()
    dataloaders.group_findings.load_many_chained = AsyncMock(
        return_value=return_values["group_findings"]["load_many_chained"]
    )

    dataloaders.finding_vulnerabilities_released_nzr.load_many_chained = (
        AsyncMock(
            return_value=return_values["finding_vulnerabilities_released_nzr"][
                "load_many_chained"
            ]
        )
    )

    finding_ids, vuln_ids = await update_finding_policy_in_groups(
        loaders=dataloaders,
        email=email,
        finding_name=finding_name,
        group_names=group_names,
        status=status,
        tags=tags,
    )
    my_finding_ids = list(
        map(
            lambda x: x.id,
            return_values["group_findings"]["load_many_chained"],
        )
    )

    my_vuln_ids = list(
        map(
            lambda x: x.id,
            return_values["finding_vulnerabilities_released_nzr"][
                "load_many_chained"
            ],
        )
    )
    assert my_finding_ids == finding_ids
    assert my_vuln_ids == vuln_ids
    mock_apply_finding_policy.assert_awaited_once()


@pytest.mark.parametrize(
    ["vulns", "statuses", "email", "tags"],
    [
        [
            [
                Vulnerability(
                    created_by="Some vuln creator",
                    created_date=datetime.now(),
                    finding_id="finding_id",
                    group_name="group",
                    hacker_email="yourfavoritehacker@fluidattacks.com",
                    id="vuln123",
                    organization_name="your org name",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    state=VulnerabilityState(
                        modified_by="modifier",
                        modified_date=datetime.now(),
                        source=Source.ANALYST,
                        specific="spec",
                        status=VulnerabilityStateStatus.MASKED,
                        where="somewhere",
                    ),
                    technique=VulnerabilityTechnique.PTAAS,
                    type=VulnerabilityType.PORTS,
                )
            ],
            [PolicyStateStatus.APPROVED, PolicyStateStatus.INACTIVE],
            "user@example.org",
            ["tag1", "tag2", "tag3"],
        ]
    ],
)
@patch(MODULE_AT_TEST + "_add_accepted_treatment", new_callable=AsyncMock)
@patch(MODULE_AT_TEST + "_add_tags_to_vulnerabilities", new_callable=AsyncMock)
@patch(MODULE_AT_TEST + "_add_new_treatment", new_callable=AsyncMock)
async def test_apply_finding_policy(
    mock_add_new_treatment: AsyncMock,
    mock_add_tags_to_vulnerabilities: AsyncMock,
    mock_add_accepted_treatment: AsyncMock,
    *,
    vulns: list[Vulnerability],
    statuses: list[PolicyStateStatus],
    email: str,
    tags: set[str],
) -> None:
    def assert_approved() -> None:
        mock_add_accepted_treatment.assert_awaited_once()
        mock_add_tags_to_vulnerabilities.assert_awaited_once()
        mock_add_new_treatment.assert_not_awaited()

    def assert_inactive() -> None:
        mock_add_accepted_treatment.assert_not_awaited()
        mock_add_tags_to_vulnerabilities.assert_not_awaited()
        mock_add_new_treatment.assert_awaited()

    callbacks = [assert_approved, assert_inactive]

    def reset_mocks() -> None:
        mock_add_accepted_treatment.reset_mock()
        mock_add_tags_to_vulnerabilities.reset_mock()
        mock_add_new_treatment.reset_mock()

    for callback, status in zip(callbacks, statuses):
        await _apply_finding_policy(
            email=email, status=status, tags=tags, vulns=vulns
        )
        callback()
        reset_mocks()


@pytest.mark.parametrize(
    [
        "modified_date",
        "vulns",
        "email",
    ],
    [
        [
            datetime.now(),
            [
                Vulnerability(
                    created_by="Some vuln creator",
                    created_date=datetime.now(),
                    finding_id="finding_id",
                    group_name="group",
                    hacker_email="yourfavoritehacker@fluidattacks.com",
                    id="vuln123",
                    organization_name="your org name",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    state=VulnerabilityState(
                        modified_by="modifier",
                        modified_date=datetime.now(),
                        source=Source.ANALYST,
                        specific="spec",
                        status=VulnerabilityStateStatus.VULNERABLE,
                        where="somewhere",
                    ),
                    technique=VulnerabilityTechnique.PTAAS,
                    type=VulnerabilityType.PORTS,
                    treatment=Treatment(
                        modified_by="modifier",
                        status=TreatmentStatus.ACCEPTED,
                        modified_date=datetime.now(),
                    ),
                )
            ],
            "user@example,org",
        ]
    ],
)
async def test_add_accepted_treatment(
    modified_date: datetime,
    vulns: list[Vulnerability],
    email: str,
) -> None:
    with patch(
        MODULE_AT_TEST + "vulns_model.update_treatment", new_callable=AsyncMock
    ) as mock:
        await _add_accepted_treatment(
            modified_date=modified_date, vulns=vulns, email=email
        )
        mock.assert_awaited()


@pytest.mark.parametrize(
    [
        "vulns",
        "tags",
    ],
    [
        [
            [
                Vulnerability(
                    created_by="Some vuln creator",
                    created_date=datetime.now(),
                    finding_id="finding_id",
                    group_name="group",
                    hacker_email="yourfavoritehacker@fluidattacks.com",
                    id="vuln123",
                    organization_name="your org name",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    state=VulnerabilityState(
                        modified_by="modifier",
                        modified_date=datetime.now(),
                        source=Source.ANALYST,
                        specific="spec",
                        status=VulnerabilityStateStatus.MASKED,
                        where="somewhere",
                    ),
                    technique=VulnerabilityTechnique.PTAAS,
                    type=VulnerabilityType.PORTS,
                )
            ],
            ["tag1", "tag2", "tag3"],
        ]
    ],
)
@patch(MODULE_AT_TEST + "vulns_domain.add_tags", new_callable=AsyncMock)
async def test_add_tags_to_vulnerabilities(
    mock: AsyncMock,
    vulns: list[Vulnerability],
    tags: set[str],
) -> None:
    await _add_tags_to_vulnerabilities(vulns=vulns, tags=tags)
    mock.assert_awaited_once()


@pytest.mark.parametrize(
    [
        "modified_date",
        "vulns",
        "email",
    ],
    [
        [
            datetime.now(),
            [
                Vulnerability(
                    created_by="Some vuln creator",
                    created_date=datetime.now(),
                    finding_id="finding_id",
                    group_name="group",
                    hacker_email="yourfavoritehacker@fluidattacks.com",
                    id="vuln123",
                    organization_name="your org name",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    state=VulnerabilityState(
                        modified_by="modifier1",
                        modified_date=datetime.now(),
                        source=Source.ANALYST,
                        specific="spec",
                        status=VulnerabilityStateStatus.MASKED,
                        where="somewhere",
                    ),
                    technique=VulnerabilityTechnique.SCR,
                    type=VulnerabilityType.LINES,
                    treatment=Treatment(
                        modified_by="modifier",
                        status=TreatmentStatus.UNTREATED,
                        modified_date=datetime.now(),
                    ),
                ),
                Vulnerability(
                    created_by="Some vuln creator",
                    created_date=datetime.now(),
                    finding_id="finding_id_2",
                    group_name="group",
                    hacker_email="yourfavoritehacker@fluidattacks.com",
                    id="vuln456",
                    organization_name="your org name 2",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    state=VulnerabilityState(
                        modified_by="modifier2",
                        modified_date=datetime.now(),
                        source=Source.ANALYST,
                        specific="spec",
                        status=VulnerabilityStateStatus.VULNERABLE,
                        where="somewhere",
                    ),
                    technique=VulnerabilityTechnique.PTAAS,
                    type=VulnerabilityType.PORTS,
                    treatment=Treatment(
                        modified_by="another_modifier",
                        status=TreatmentStatus.ACCEPTED,
                        modified_date=datetime.now(),
                    ),
                ),
            ],
            "user@example.org",
        ]
    ],
)
@patch(MODULE_AT_TEST + "vulns_model.update_treatment", new_callable=AsyncMock)
async def test_add_new_treatment(
    mock_vulns_model_update_treatment: AsyncMock,
    modified_date: datetime,
    vulns: list[Vulnerability],
    email: str,
) -> None:
    await _add_new_treatment(
        modified_date=modified_date, vulns=vulns, email=email
    )
    mock_vulns_model_update_treatment.assert_awaited_once()
