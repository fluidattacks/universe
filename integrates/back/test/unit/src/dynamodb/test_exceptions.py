from botocore.exceptions import (
    ClientError,
)
from integrates.dynamodb.exceptions import (
    ConditionalCheckFailedException,
    handle_error,
    UnavailabilityError,
    ValidationException,
)
import pytest
from test.unit.src.utils import (
    get_module_at_test,
)
from unittest.mock import (
    MagicMock,
    patch,
)

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"


@pytest.mark.parametrize(
    ["client_error", "raised_error", "message"],
    [
        [
            ClientError(
                {"Error": {"Code": "connection"}}, operation_name="test"
            ),
            UnavailabilityError,
            "retry",
        ],
        [
            ClientError(
                {"Error": {"Code": "ValidationException"}},
                operation_name="test",
            ),
            ValidationException,
            "validation",
        ],
    ],
)
@patch(MODULE_AT_TEST + "LOGGER.error", new_callable=MagicMock)
def test_handle_error(
    mock_logger: MagicMock,
    client_error: ClientError,
    raised_error: type[Exception],
    message: str,
) -> None:
    with pytest.raises(raised_error):
        handle_error(
            error=client_error,
            message=message,
            argument="this is a test argument",
        )

    mock_logger.assert_called_with(
        message,
        extra={
            "extra": {
                "message": message,
                "argument": "this is a test argument",
                "exception": client_error,
            }
        },
    )


@pytest.mark.parametrize(
    ["client_error", "raised_error", "message"],
    [
        [
            ClientError(
                {"Error": {"Code": "ConditionalCheckFailedException"}},
                operation_name="test",
            ),
            ConditionalCheckFailedException,
            "conditional error",
        ],
    ],
)
@patch(MODULE_AT_TEST + "LOGGER.error", new_callable=MagicMock)
def test_handle_error_conditional_exception(
    mock_logger: MagicMock,
    client_error: ClientError,
    raised_error: type[Exception],
    message: str,
) -> None:
    with pytest.raises(raised_error):
        handle_error(
            error=client_error,
            message=message,
            argument="this is a test argument",
        )

    mock_logger.assert_not_called()
