import pytest

from integrates.db_model.items import OrganizationBillingItem
from integrates.db_model.organizations.utils import (
    format_billing_information,
)

pytestmark = [
    pytest.mark.asyncio,
]


@pytest.mark.parametrize(
    "items",
    [
        [
            {
                "billing_email": "test@fluidattacks.com",
                "customer_id": "cus_1234567890",
                "last_modified_by": "test@fluidattacks.com",
                "billed_groups": ["group_name"],
                "subscription_id": 3232321,
                "modified_at": "2022-01-01T00:00:00",
            },
            {
                "billing_email": "test2@fluidattacks.com",
                "customer_id": "cus_1234567891",
                "last_modified_by": "test2@fluidattacks.com",
                "billed_groups": ["group_name2"],
                "subscription_id": 3232322,
                "modified_at": "2022-01-01T00:00:00",
            },
        ],
    ],
)
async def test_format_billing_information_new_format(
    items: list[OrganizationBillingItem],
) -> None:
    billing_information = format_billing_information(items)
    assert billing_information
    for index, billing_info in enumerate(billing_information):
        assert billing_info.billing_email == items[index]["billing_email"]
        assert billing_info.customer_id == items[index]["customer_id"]
        assert billing_info.last_modified_by == items[index]["last_modified_by"]
        assert billing_info.billed_groups == items[index]["billed_groups"]
        assert billing_info.subscription_id == items[index]["subscription_id"]
