from datetime import (
    datetime,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.roots.enums import (
    RootMachineStatus,
)
from integrates.db_model.roots.types import (
    GitRoot,
    GitRootMachine,
    MachineExecutionsDate,
    RootRequest,
)
from integrates.db_model.roots.update import (
    update_git_root_machine,
)
import pytest

pytestmark = [
    pytest.mark.asyncio,
]


@pytest.mark.changes_db
async def test_update_git_root_machine() -> None:
    loaders: Dataloaders = get_new_context()
    group_name = "unittesting"
    root_id = "9eccb9ed-e835-4dbc-b28e-8cecd0296e27"
    root = await loaders.root.load(RootRequest(group_name, root_id))
    assert root
    assert isinstance(root, GitRoot)

    new_machine_info = GitRootMachine(
        modified_by="integrates@fluidattacks.com",
        modified_date=datetime.fromisoformat("2022-10-21T15:00:00+00:00"),
        reason="unknown",
        status=RootMachineStatus.SUCCESS,
        commit="d9483264b231635a851bf3b431d1fc5851dc63eb",
        commit_date=datetime.fromisoformat("2022-10-21T14:00:00+00:00"),
        last_executions_date=MachineExecutionsDate(
            apk=datetime_utils.get_utc_now(),
            cspm=datetime_utils.get_utc_now(),
            dast=datetime_utils.get_utc_now(),
            sast=datetime_utils.get_utc_now(),
            sca=datetime_utils.get_utc_now(),
        ),
    )
    assert root.machine != new_machine_info
    await update_git_root_machine(
        current_value=root.machine,
        group_name=group_name,
        machine=new_machine_info,
        root_id=root.id,
    )

    loaders.root.clear_all()
    root = await loaders.root.load(RootRequest(group_name, root_id))
    assert root
    assert isinstance(root, GitRoot)
    assert root.machine == new_machine_info
