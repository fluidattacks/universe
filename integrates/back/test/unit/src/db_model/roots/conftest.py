from collections.abc import (
    Callable,
)
from datetime import (
    datetime,
)
from integrates.db_model.roots.enums import (
    RootCloningStatus,
    RootCriticality,
    RootStatus,
    RootType,
)
from integrates.db_model.roots.types import (
    GitRoot,
    GitRootCloning,
    GitRootState,
    RootUnreliableIndicators,
)
import pytest
from typing import (
    Any,
)

MOCKED_DATA: dict[str, dict[str, Any]] = {
    "integrates.db_model.roots.get._get_roots": {
        '["2851c4f5-fa0a-4a41-991e-8ad2c36aead9"]': [
            [
                GitRoot(
                    cloning=GitRootCloning(
                        modified_by="admin@gmail.com",
                        modified_date=datetime.fromisoformat(
                            "2022-09-13T00:00:00+00:00"
                        ),
                        reason="Cloned successfully",
                        status=RootCloningStatus.OK,
                    ),
                    created_by="admin@gmail.com",
                    created_date=datetime.fromisoformat(
                        "2022-09-13T01:00:00+00:00"
                    ),
                    group_name="test_group_1",
                    id="2851c4f5-fa0a-4a41-991e-8ad2c36aead9",
                    organization_name="test_organization_1",
                    state=GitRootState(
                        branch="master",
                        criticality=RootCriticality.LOW,
                        includes_health_check=False,
                        modified_by="resources@domain.com",
                        modified_date=datetime.fromisoformat(
                            "2022-09-13T01:00:00+00:00"
                        ),
                        nickname="nickname1",
                        status=RootStatus.ACTIVE,
                        url="https://domain.nickname1.git",
                    ),
                    type=RootType.GIT,
                    unreliable_indicators=RootUnreliableIndicators(
                        unreliable_last_status_update=datetime.fromisoformat(
                            "2022-09-13T01:00:00+00:00"
                        ),
                        nofluid_quantity=1,
                    ),
                ),
            ]
        ],
    },
}


@pytest.fixture
def mocked_data_for_module(
    *,
    resolve_mock_data: Callable,
) -> Any:
    def _mocked_data_for_module(
        mock_path: str, mock_args: list[Any], module_at_test: str
    ) -> Callable[[str, list[Any], str], Any]:
        return resolve_mock_data(
            mock_data=MOCKED_DATA,
            mock_path=mock_path,
            mock_args=mock_args,
            module_at_test=module_at_test,
        )

    return _mocked_data_for_module
