import pytest

from integrates.db_model.vulnerabilities.utils import (
    get_search_params,
    set_conditions,
)
from integrates.search.operations import SearchParams

pytestmark = [
    pytest.mark.asyncio,
]


@pytest.mark.parametrize(
    "conditions,expected_output",
    [
        (
            {
                "first": 9,
                "state": ["VULNERABLE"],
                "treatment": ["UNTREATED"],
                "severity_rating": "HIGH",
                "zero_risk_not": ["REQUESTED", "CONFIRMED"],
            },
            SearchParams(
                index_value="vulns_index",
                limit=9,
                must_filters=[
                    {"group_name": "unittesting"},
                    {"sk": "FIN#123"},
                ],
                must_not_filters=[
                    {"state.status": "DELETED"},
                    {"state.status": "MASKED"},
                    {"zero_risk.status": "REQUESTED"},
                    {"zero_risk.status": "CONFIRMED"},
                ],
                should_filters=[
                    {"state.status": "VULNERABLE"},
                    {"treatment.status": "UNTREATED"},
                ],
                range_filters=[
                    {
                        "severity_score.threat_score": {"gte": 7.0, "lt": 9.0},
                    }
                ],
            ),
        ),
    ],
)
def test_wellformed_opensearch_rules(
    conditions: dict,
    expected_output: SearchParams,
) -> None:
    group_name = "unittesting"
    finding_id = "123"
    new_conditions = set_conditions(filters=conditions)

    search = get_search_params(
        group_name=group_name,
        after=None,
        first=9,
        sort_by=None,
        finding_id=finding_id,
        conditions=new_conditions,
    )

    assert search.index_value == expected_output.index_value
    assert search.limit == expected_output.limit
    assert search.must_filters == expected_output.must_filters
    assert search.must_not_filters == expected_output.must_not_filters
    assert search.range_filters == expected_output.range_filters
    assert search.should_filters == expected_output.should_filters
