from azure.devops.v7_1.git.models import (
    GitRepository,
)
from collections.abc import (
    Iterable,
)
from datetime import (
    datetime,
)
from integrates.db_model.azure_repositories.get import (
    get_repositories,
)
from integrates.db_model.credentials.types import (
    Credentials,
    CredentialsState,
    HttpsPatSecret,
)
from integrates.db_model.enums import (
    CredentialType,
)
import pytest
from unittest.mock import (
    AsyncMock,
    MagicMock,
    patch,
)


# Fixture para simular credenciales
@pytest.fixture(name="credentials_mock")
def credentials_fixture() -> Iterable[Credentials]:
    return [
        Credentials(
            id="id",
            organization_id="org123",
            secret=HttpsPatSecret(token="fake_token"),
            state=CredentialsState(
                azure_organization="fake_organization",
                name="fake_credential",
                type=CredentialType.AWSROLE,
                owner="owner",
                is_pat=False,
                modified_by="ugomez",
                modified_date=datetime.fromisoformat(
                    "2022-02-09T16:54:12+00:00"
                ),
            ),
        )
    ]


@pytest.mark.asyncio
@patch(
    "integrates.db_model.azure_repositories.get.DataLoader.load",
    new_callable=AsyncMock,
)
async def test_get_repositories(
    credentials_mock: MagicMock,
) -> None:
    mock_dataloader_load = MagicMock()
    mock_dataloader_load.return_value = [
        [GitRepository(name="repo1", is_disabled=False)],
        [GitRepository(name="repo2", is_disabled=True)],
    ]

    repositories = await get_repositories(credentials=credentials_mock)

    assert len(repositories) == 0
