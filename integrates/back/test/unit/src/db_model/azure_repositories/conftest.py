from collections.abc import (
    Callable,
)
import pytest
from test.unit.src.utils import (
    MockedInstance,
)


@pytest.fixture
def mock_gitlab() -> Callable:
    mocked_projects = [
        MockedInstance(
            id="1",
            http_url_to_repo="https://gitlab.com/jdoe/repo.git",
            ssh_url_to_repo="git@gitlab.com:jdoe/repo.git",
            commits=MockedInstance(
                list=lambda *_, **__: (
                    x
                    for x in [
                        MockedInstance(
                            id="a",
                            committed_date="2023-09-30T15:00:00+00:00",
                        ),
                    ]
                ),
            ),
        ),
    ]

    return lambda *_, **__: MockedInstance(
        projects=MockedInstance(
            get=lambda id: next(
                (x for x in mocked_projects if x.id == id), None
            ),
            list=lambda *_, **__: mocked_projects,
        )
    )
