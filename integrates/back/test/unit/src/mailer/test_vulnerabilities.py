from collections.abc import Callable
from typing import (
    Any,
)
from unittest.mock import (
    AsyncMock,
    patch,
)

import pytest

from integrates.context import (
    BASE_URL,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.mailer.vulnerabilities import (
    send_mail_assigned_vulnerability,
    send_mail_updated_treatment,
)
from test.unit.src.utils import (
    get_module_at_test,
)

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"


def _set_up_mocks_results(
    mocks_setup_list: list[tuple[AsyncMock, str, list[Any]]],
    mock_data_for_module: Any,
) -> None:
    # Set up mocks' results using mock_data_for_module fixture
    for item in mocks_setup_list:
        mock, path, arguments = item
        mock.return_value = mock_data_for_module(
            mock_path=path,
            mock_args=arguments,
            module_at_test=MODULE_AT_TEST,
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(
    [
        "email_to",
        "is_finding_released",
        "group_name",
        "finding_title",
        "finding_id",
        "responsible",
        "where",
    ],
    [
        [
            ["admin@gmail.com", "hacker@gmail.com"],
            True,
            "unittesting",
            "001. SQL injection - C Sharp SQL API",
            "3c475384-834c-47b0-ac71-a41a022e401c",
            "admin@gmail.com",
            "MyVulnerability",
        ],
    ],
)
@patch(MODULE_AT_TEST + "send_mails_async", new_callable=AsyncMock)
@patch(MODULE_AT_TEST + "get_organization_name", new_callable=AsyncMock)
async def test_send_mail_assigned_vulnerability(
    mock_get_organization_name: AsyncMock,
    mock_send_mails_async: AsyncMock,
    *,
    email_to: list[str],
    is_finding_released: bool,
    group_name: str,
    finding_title: str,
    finding_id: str,
    responsible: str,
    where: str,
    mock_data_for_module: Callable,
) -> None:
    loaders: Any = get_new_context()

    mock_get_organization_name.return_value = mock_data_for_module(
        mock_path="get_organization_name",
        mock_args=[group_name],
        module_at_test=MODULE_AT_TEST,
    )

    await send_mail_assigned_vulnerability(
        loaders=loaders,
        email_to=email_to,
        is_finding_released=is_finding_released,
        group_name=group_name,
        finding_title=finding_title,
        finding_id=finding_id,
        responsible=responsible,
        where=where,
    )
    mock_get_organization_name.assert_awaited_once_with(loaders, group_name)
    expected_email_context = {
        "finding_title": finding_title,
        "group": group_name,
        "finding_url": (
            f"{BASE_URL}/orgs/okada/groups/{group_name}/"
            f'{"vulns" if is_finding_released else "drafts"}/{finding_id}/'
            "locations"
        ),
        "responsible": responsible,
        "where": where.splitlines(),
    }

    mock_send_mails_async.assert_awaited_once_with(
        loaders=loaders,
        email_to=email_to,
        context=expected_email_context,
        subject=("Newly assigned vulnerability in " f"[{finding_title}] for [{group_name}]"),
        template_name="vulnerability_assigned",
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(
    [
        "assigned",
        "finding_id",
        "finding_title",
        "group_name",
        "justification",
        "treatment",
        "vulnerabilities",
        "modified_by",
    ],
    [
        [
            "admin@gmail.com",
            "3c475384-834c-47b0-ac71-a41a022e401c",
            "001. SQL injection - C Sharp SQL API",
            "unittesting",
            "this is a test",
            "ACCEPTED",
            "skims/test/data/lib_path/f065/vulnerable.html (37)",
            "admin@gmail.com",
        ],
    ],
)
@patch(MODULE_AT_TEST + "send_mails_async", new_callable=AsyncMock)
@patch(
    MODULE_AT_TEST + "authz.get_group_level_role",
    new_callable=AsyncMock,
)
@patch(
    MODULE_AT_TEST + "get_group_emails_by_notification",
    new_callable=AsyncMock,
)
@patch(MODULE_AT_TEST + "get_organization_name", new_callable=AsyncMock)
async def test_send_mail_updated_treatment(
    mock_get_organization_name: AsyncMock,
    mock_stakeholders: AsyncMock,
    mock_authz_get_group_level_role: AsyncMock,
    mock_send_mails_async: AsyncMock,
    *,
    assigned: str,
    finding_id: str,
    finding_title: str,
    group_name: str,
    justification: str,
    treatment: str,
    vulnerabilities: str,
    modified_by: str,
    mock_data_for_module: Callable,
) -> None:
    mocks_setup_list: list[tuple[AsyncMock, str, list[Any]]] = [
        (
            mock_get_organization_name,
            "get_organization_name",
            [group_name],
        ),
        (
            mock_stakeholders,
            "get_group_emails_by_notification",
            [group_name, "updated_treatment"],
        ),
        (
            mock_authz_get_group_level_role,
            "authz.get_group_level_role",
            [modified_by, group_name],
        ),
    ]
    _set_up_mocks_results(mocks_setup_list, mock_data_for_module)
    await send_mail_updated_treatment(
        loaders=get_new_context(),
        assigned=assigned,
        finding_id=finding_id,
        finding_title=finding_title,
        group_name=group_name,
        justification=justification,
        treatment=treatment,
        vulnerabilities=vulnerabilities,
        modified_by=modified_by,
    )

    mock_send_mails_async.assert_awaited_once()
