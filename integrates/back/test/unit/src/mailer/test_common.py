from collections.abc import (
    Callable,
)
from datetime import (
    datetime,
)
from integrates.context import (
    BASE_URL,
)
from integrates.custom_exceptions import (
    MailerClientError,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.mailer.common import (
    _add_recipients,
    _create_attachments,
    _generate_email_message,
    _get_unsubscription_url,
    _process_content,
    get_instance,
    get_recipient_first_name,
    report_login_activity,
    send_mail_async,
    unsubscribe,
)
import pytest
from pytest_mock import (
    MockerFixture,
)
from requests import (
    Response,
)
from sendgrid.helpers.mail import (
    Attachment,
    Personalization,
)
from test.unit.src.utils import (
    get_module_at_test,
)
from unittest.mock import (
    AsyncMock,
    patch,
)

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"

pytestmark = [
    pytest.mark.asyncio,
]


@pytest.mark.parametrize(
    ["email", "is_access_granted", "expected_output"],
    [
        ["nonexistinguser@fluidattacks.com", False, None],
        ["integratesmanager@gmail.com", False, "Integrates"],
        ["nonexistinguser@fluidattacks.com", True, "nonexistinguser"],
    ],
)
@patch(
    MODULE_AT_TEST + "Dataloaders.stakeholder",
    new_callable=AsyncMock,
)
async def test_get_recipient_first_name(
    mock_dataloaders_stakeholder: AsyncMock,
    email: str,
    is_access_granted: bool,
    expected_output: str | None,
    mock_data_for_module: Callable,
) -> None:
    # Set up mock's result using mock_data_for_module fixture
    mock_dataloaders_stakeholder.load.return_value = mock_data_for_module(
        mock_path="Dataloaders.stakeholder",
        mock_args=[email],
        module_at_test=MODULE_AT_TEST,
    )
    loaders: Dataloaders = get_new_context()
    assert (
        await get_recipient_first_name(loaders, email, is_access_granted)
        == expected_output
    )
    assert mock_dataloaders_stakeholder.load.called is True


@pytest.mark.parametrize(
    "email_to, first_name",
    [
        (
            "integratesmanager@gmail.com",
            "Integrates",
        ),
    ],
)
def test_single_recipient_added_correctly(
    email_to: str,
    first_name: str,
) -> None:
    personalization = Personalization()

    _add_recipients(
        email_cc=None,
        email_to=email_to,
        first_name=first_name,
        personalization=personalization,
    )

    assert len(personalization.tos) == 1
    assert personalization.tos[0]["email"] == email_to
    assert personalization.tos[0]["name"] == first_name


@pytest.mark.parametrize(
    "email_to, first_name, context, template_name, expected_content",
    [
        (
            "integratesmanager@gmail.com",
            "Integrates",
            {"variable1": "value1", "variable2": "value2"},
            "testing",
            "Processed content",
        ),
    ],
)
def test_process_content(
    mocker: MockerFixture,
    *,
    email_to: str,
    first_name: str,
    context: dict[str, str],
    template_name: str,
    expected_content: str,
) -> None:
    mock_get_content = mocker.patch(
        "integrates.mailer.common.get_content",
        return_value=expected_content,
    )

    result = _process_content(email_to, first_name, context, template_name)

    assert result == expected_content
    mock_get_content.assert_called_once_with(template_name, context)


def test_get_instance_multiple_times(mocker: MockerFixture) -> None:
    mock_sendgrid_client = mocker.Mock()
    mocker.patch(
        "integrates.mailer.common.SendGridAPIClient",
        return_value=mock_sendgrid_client,
    )

    instance1 = get_instance()
    instance2 = get_instance()
    instance3 = get_instance()

    assert instance1 == instance2
    assert instance1 == instance3
    assert instance2 == instance3


@pytest.mark.parametrize(
    [
        "browser",
        "ip_address",
        "location",
        "operating_system",
        "report_date",
        "user_email",
    ],
    [
        [
            "Chrome",
            "192.168.0.1",
            "New York",
            "Windows 10",
            datetime.now(),
            "integratesmanager@gmail.com",
        ],
    ],
)
@pytest.mark.asyncio
async def test_send_email_with_login_activity_report(
    mocker: MockerFixture,
    *,
    browser: str,
    ip_address: str,
    location: str,
    operating_system: str,
    report_date: datetime,
    user_email: str,
) -> None:
    loaders = mocker.Mock()
    send_mails_async_mock = mocker.patch(
        "integrates.mailer.common.send_mails_async"
    )

    await report_login_activity(
        loaders=loaders,
        browser=browser,
        ip_address=ip_address,
        location=location,
        operating_system=operating_system,
        report_date=report_date,
        user_email=user_email,
    )

    send_mails_async_mock.assert_called_once_with(
        context={
            "location": location,
            "ip_address": ip_address,
            "browser": browser,
            "operating_system": operating_system,
            "report_date": report_date.strftime("on %Y/%m/%d at %H:%M:%S"),
        },
        email_to=[user_email],
        loaders=loaders,
        subject="Login to your account",
        template_name="new_sign_in_report",
    )


@pytest.mark.parametrize(
    "attachments",
    [
        (
            [
                {
                    "content": "dGVzdCBjb250ZW50",
                    "name": "testfile.txt",
                    "type": "text/plain",
                }
            ]
        ),
    ],
)
def test_returns_list_of_attachments(
    attachments: list[dict[str, str]],
) -> None:
    result = _create_attachments(attachments)

    assert isinstance(result, list)
    assert all(isinstance(item, Attachment) for item in result)


@pytest.mark.parametrize(
    "email_to, first_name, context, subject, template_name",
    [
        (
            "integratesmanager@gmail.com",
            "Integrates",
            {"variable1": "value1", "variable2": "value2"},
            "test subject",
            "testing",
        ),
    ],
)
def test_generate_email(
    mocker: MockerFixture,
    *,
    email_to: str,
    first_name: str,
    context: dict[str, str],
    subject: str,
    template_name: str,
) -> None:
    mocker.patch(
        "integrates.mailer.common._process_content",
        return_value="Processed content",
    )
    mocker.patch("integrates.mailer.common._add_mail_settings")
    mocker.patch("integrates.mailer.common._add_recipients")
    mocker.patch("integrates.mailer.common._add_attachments")

    result = _generate_email_message(
        email_to=email_to,
        first_name=first_name,
        context=context,
        subject=subject,
        template_name=template_name,
    )

    assert result.from_email.email == "noreply@fluidattacks.com"
    assert result.from_email.name == "Fluid Attacks"


@pytest.mark.parametrize(
    "email_to, first_name, context, subject, template_name",
    [
        (
            "integratesmanager@gmail.com",
            "Integrates",
            {"variable1": "value1", "variable2": "value2"},
            "test subject",
            "testing",
        ),
    ],
)
@pytest.mark.asyncio
async def test_send_mail_successfully(
    mocker: MockerFixture,
    *,
    email_to: str,
    first_name: str,
    context: dict[str, str],
    subject: str,
    template_name: str,
) -> None:
    mocker.patch(
        "integrates.mailer.common._process_content",
        return_value="Processed content",
    )
    mocker.patch("integrates.mailer.common.get_instance")
    mocker.patch("integrates.mailer.common.in_thread", return_value=Response())
    mocker.patch("integrates.mailer.common.TRANSACTIONS_LOGGER.info")

    with patch(
        "integrates.mailer.common.TRANSACTIONS_LOGGER.info"
    ) as mock_logger:
        await send_mail_async(
            email_to=email_to,
            first_name=first_name,
            email_cc=None,
            context=context,
            subject=subject,
            template_name=template_name,
        )

        mock_logger.assert_called_once()


@pytest.mark.parametrize(
    "email",
    [
        ("integratesmanager@gmail.com"),
    ],
)
@pytest.mark.asyncio
async def test_successfully_unsubscribes_valid_email(
    mocker: MockerFixture,
    *,
    email: str,
) -> None:
    mock_sendgrid_client = mocker.patch(
        "integrates.mailer.common.get_instance"
    )
    mock_sendgrid_client.return_value.client.asm.suppressions._(
        "global"
    ).post.return_value = None

    await unsubscribe(email)

    mock_sendgrid_client.return_value.client.asm.suppressions._(
        "global"
    ).post.assert_called_once_with(request_body={"recipient_emails": [email]})


@pytest.mark.parametrize(
    "email",
    [
        ("integratesmanager@gmail.com"),
    ],
)
@pytest.mark.asyncio
async def test_handles_invalid_email_format(
    mocker: MockerFixture,
    *,
    email: str,
) -> None:
    mock_sendgrid_client = mocker.patch(
        "integrates.mailer.common.get_instance"
    )
    mock_sendgrid_client.return_value.client.asm.suppressions._(
        "global"
    ).post.side_effect = MailerClientError

    with pytest.raises(MailerClientError):
        await unsubscribe(email)

    mock_sendgrid_client.return_value.client.asm.suppressions._(
        "global"
    ).post.assert_called_once_with(request_body={"recipient_emails": [email]})


@pytest.mark.parametrize(
    "user_email, expected_url_prefix",
    [
        (
            "integratesmanager@gmail.com",
            f"{BASE_URL}/unsubscribe/",
        ),
    ],
)
@pytest.mark.asyncio
async def test_generates_valid_unsubscription_url(
    mocker: MockerFixture,
    *,
    user_email: str,
    expected_url_prefix: str,
) -> None:
    mocker.patch(
        "integrates.sessions.domain.encode_token", return_value="mocked_token"
    )

    unsubscribe_url = _get_unsubscription_url(user_email)

    assert unsubscribe_url == f"{expected_url_prefix}mocked_token"
