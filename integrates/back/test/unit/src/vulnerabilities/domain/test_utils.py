from datetime import (
    datetime,
)
from unittest.mock import (
    AsyncMock,
    patch,
)

import pytest

from integrates.custom_exceptions import (
    AcceptanceNotRequested,
    InvalidParameter,
)
from integrates.custom_utils.vulnerabilities import (
    get_path_from_integrates_vulnerability,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.enums import (
    AcceptanceStatus,
    Source,
    TreatmentStatus,
)
from integrates.db_model.types import (
    Treatment,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
    VulnerabilityTechnique,
    VulnerabilityType,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityState,
)
from integrates.vulnerabilities.domain import (
    utils as vulnerabilities_utils,
)
from test.unit.src.utils import (
    get_module_at_test,
    set_mocks_return_values,
)

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"

pytestmark = [
    pytest.mark.asyncio,
]


@pytest.mark.parametrize(
    ("treatment_status", "acceptance_status", "should_raise"),
    [
        (
            TreatmentStatus.ACCEPTED,
            AcceptanceStatus.SUBMITTED,
            False,
        ),
        (
            TreatmentStatus.IN_PROGRESS,
            AcceptanceStatus.APPROVED,
            True,
        ),
    ],
)
def test_validate_acceptance(
    treatment_status: TreatmentStatus,
    acceptance_status: AcceptanceStatus,
    should_raise: bool,
) -> None:
    vuln = (
        Vulnerability(
            created_by="user1",
            created_date=datetime.now(),
            finding_id="FINDING123",
            group_name="security",
            hacker_email="hacker@example.com",
            id="VULN1",
            organization_name="ACME Corp",
            root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
            state=VulnerabilityState(
                modified_by="admin",
                modified_date=datetime.now(),
                source=Source.DETERMINISTIC,
                specific="Input validation",
                status=VulnerabilityStateStatus.VULNERABLE,
                where="app/src/main.py",
            ),
            technique=VulnerabilityTechnique.PTAAS,
            type=VulnerabilityType.INPUTS,
            treatment=Treatment(
                modified_date=datetime(2023, 8, 29, 15, 30),
                status=treatment_status,
                acceptance_status=acceptance_status,
                accepted_until=datetime(2023, 9, 30, 12, 0),
                justification="Critical vulnerability.",
                assigned="engineer1",
                modified_by="admin",
            ),
        ),
    )
    if should_raise:
        with pytest.raises(AcceptanceNotRequested):
            vulnerabilities_utils.validate_acceptance(vuln[0])
    else:
        vulnerabilities_utils.validate_acceptance(vuln[0])


@pytest.mark.parametrize(
    ("field", "url", "expected_result"),
    [
        ("archive.exe", "Inputs vuln", True),
        ("Inputs vuln", "script.sh", True),
        ("document.doc", "application.apk/", True),
        ("imagen.png", "default url", False),
    ],
)
def test_is_executable(field: str, url: str, expected_result: bool) -> None:
    assert vulnerabilities_utils.is_executable(field, url) == expected_result


@pytest.mark.parametrize(
    (
        "group_name",
        "vulns",
        "expected_result",
    ),
    [
        (
            "unittesting",
            [
                Vulnerability(
                    created_by="Some vuln creator",
                    created_date=datetime.now(),
                    finding_id="436992569",
                    group_name="unittesting",
                    hacker_email="machine@fluidattacks.com",
                    id="vuln123",
                    organization_name="your org name",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    state=VulnerabilityState(
                        modified_by="modifier",
                        modified_date=datetime.now(),
                        source=Source.ANALYST,
                        specific="spec",
                        status=VulnerabilityStateStatus.MASKED,
                        where="somewhere",
                    ),
                    skims_method=None,
                    technique=VulnerabilityTechnique.PTAAS,
                    type=VulnerabilityType.PORTS,
                ),
                Vulnerability(
                    created_by="unittest@fluidattacks.com",
                    created_date=datetime.fromisoformat("2018-11-27T19:54:08+00:00"),
                    finding_id="4574973146",
                    group_name="unittesting",
                    organization_name="okada",
                    hacker_email="unittest@fluidattacks.com",
                    id="6192c72f-2e10-4259-9207-717b2d90d8d2",
                    state=VulnerabilityState(
                        modified_by="unittest@fluidattacks.com",
                        modified_date=datetime.fromisoformat("2019-09-16T14:40:37+00:00"),
                        source=Source.ASM,
                        specific="userToken",
                        status=VulnerabilityStateStatus.VULNERABLE,
                        where="https://10.1.1.1/",
                        commit=None,
                        reasons=None,
                        other_reason=None,
                        tool=None,
                    ),
                    type=VulnerabilityType.LINES,
                    bug_tracking_system_url=None,
                    custom_severity=None,
                    developer=None,
                    event_id=None,
                    hash=None,
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    skims_method=None,
                    stream=None,
                    tags=None,
                    technique=VulnerabilityTechnique.SCR,
                    treatment=Treatment(
                        modified_date=datetime.fromisoformat("2019-09-16T10:10:00+00:00"),
                        status=TreatmentStatus.UNTREATED,
                        acceptance_status=None,
                        accepted_until=None,
                        justification=None,
                        assigned="integratesuser@gmail.com",
                        modified_by=None,
                    ),
                    verification=None,
                    zero_risk=None,
                ),
            ],
            {"universe": ["https://10.1.1.1/"]},
        )
    ],
)
async def test_get_vulnerabilities_paths_by_root(
    group_name: str,
    vulns: list[Vulnerability],
    expected_result: dict[str, list[str]],
) -> None:
    loaders = get_new_context()

    assert (
        await vulnerabilities_utils.get_vulnerabilities_paths_by_root(loaders, group_name, vulns)
        == expected_result
    )


@pytest.mark.parametrize(
    ("vulnerability_where", "vulnerability_type", "expected_result", "ignore_cve"),
    [
        ("README.md", VulnerabilityType.LINES, ("", "README.md"), True),
        ("192.168.1.20", VulnerabilityType.PORTS, ("", "192.168.1.20"), False),
        (
            "test/data/lib_path/f060/csharp.cs",
            VulnerabilityType.LINES,
            ("", "test/data/lib_path/f060/csharp.cs"),
            False,
        ),
        (
            "https://example.com (nameespace)",
            VulnerabilityType.INPUTS,
            ("nameespace", "https://example.com"),
            False,
        ),
    ],
)
def test_get_path_from_integrates_vulnerability(
    vulnerability_where: str,
    vulnerability_type: VulnerabilityType,
    expected_result: tuple[str, str],
    ignore_cve: bool,
) -> None:
    assert (
        get_path_from_integrates_vulnerability(vulnerability_where, vulnerability_type, ignore_cve)
        == expected_result
    )


@pytest.mark.parametrize(
    ("vuln", "expected_result"),
    [
        (
            Vulnerability(
                created_by="Some vuln creator",
                created_date=datetime.now(),
                finding_id="436992569",
                group_name="group",
                hacker_email="machine@fluidattacks.com",
                id="vuln123",
                organization_name="your org name",
                root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                state=VulnerabilityState(
                    modified_by="modifier",
                    modified_date=datetime.now(),
                    source=Source.ANALYST,
                    specific="spec",
                    status=VulnerabilityStateStatus.MASKED,
                    where="somewhere",
                ),
                skims_method="analyze_protocols.cbc_enabled",
                technique=VulnerabilityTechnique.PTAAS,
                type=VulnerabilityType.PORTS,
            ),
            12264070390385814621,
        ),
    ],
)
@patch(MODULE_AT_TEST + "findings_utils.get_finding", new_callable=AsyncMock)
async def test_get_hash_from_machine_vuln(
    mock_get_finding: AsyncMock,
    vuln: Vulnerability,
    expected_result: int,
) -> None:
    loaders = get_new_context()
    mocked_objects, mocked_paths, mocks_args = [
        [mock_get_finding],
        ["get_finding"],
        [[vuln.finding_id]],
    ]
    assert set_mocks_return_values(
        mocks_args=mocks_args,
        mocked_objects=mocked_objects,
        paths_list=mocked_paths,
        module_at_test=MODULE_AT_TEST,
    )
    assert await vulnerabilities_utils.get_hash_from_machine_vuln(loaders, vuln) == expected_result


@pytest.mark.parametrize(
    "vuln",
    [
        Vulnerability(
            created_by="Some vuln creator",
            created_date=datetime.now(),
            finding_id="436992569",
            group_name="group",
            hacker_email="machine@fluidattacks.com",
            id="vuln123",
            organization_name="your org name",
            root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
            state=VulnerabilityState(
                modified_by="modifier",
                modified_date=datetime.now(),
                source=Source.ANALYST,
                specific="spec",
                status=VulnerabilityStateStatus.MASKED,
                where="somewhere",
            ),
            skims_method=None,
            technique=VulnerabilityTechnique.PTAAS,
            type=VulnerabilityType.PORTS,
        ),
    ],
)
async def test_get_hash_from_machine_vuln_raises(
    vuln: Vulnerability,
) -> None:
    loaders = get_new_context()
    with pytest.raises(InvalidParameter):
        await vulnerabilities_utils.get_hash_from_machine_vuln(loaders, vuln)


@pytest.mark.parametrize(
    ("treatment_value", "expected_result"),
    [
        ("ACCEPTED_UNDEFINED", True),
        ("ACCEPTED", False),
    ],
)
def test_is_permanent_acceptance(treatment_value: str, expected_result: bool) -> None:
    assert vulnerabilities_utils.is_permanent_acceptance(treatment_value) == expected_result


@pytest.mark.parametrize(
    ("treatment_status", "acceptance_status", "expected_result"),
    [
        (
            TreatmentStatus.ACCEPTED,
            AcceptanceStatus.APPROVED,
            True,
        ),
        (
            TreatmentStatus.ACCEPTED,
            AcceptanceStatus.REJECTED,
            False,
        ),
        (TreatmentStatus.ACCEPTED, None, True),
        (
            TreatmentStatus.IN_PROGRESS,
            AcceptanceStatus.APPROVED,
            False,
        ),
        (TreatmentStatus.UNTREATED, None, False),
    ],
)
def test_is_valid_accepted_treatment(
    treatment_status: TreatmentStatus,
    acceptance_status: AcceptanceStatus,
    expected_result: bool,
) -> None:
    treatment = Treatment(
        modified_date=datetime(2023, 8, 29, 15, 30),
        status=treatment_status,
        acceptance_status=acceptance_status,
        accepted_until=datetime(2023, 9, 30, 12, 0),
        justification="Critical vulnerability.",
        assigned="engineer1",
        modified_by="admin",
    )
    assert vulnerabilities_utils.is_valid_accepted_treatment(treatment) == expected_result
