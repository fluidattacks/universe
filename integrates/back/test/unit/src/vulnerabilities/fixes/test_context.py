from integrates.custom_exceptions import (
    FindingNotFound,
    VulnNotFound,
)
from integrates.custom_utils.criteria import (
    CRITERIA_VULNERABILITIES,
)
from integrates.vulnerabilities.fixes.context import (
    generate_vuln_context,
    get_vuln_criteria,
    get_vuln_line,
)
import pytest


def test_get_vuln_criteria() -> None:
    assert (
        "description"
        in get_vuln_criteria(
            CRITERIA_VULNERABILITIES,
            "094",
        )["en"]
    )

    with pytest.raises(FindingNotFound):
        assert get_vuln_criteria(
            CRITERIA_VULNERABILITIES,
            "non-existent-criteria",
        )


def test_get_vuln_line() -> None:
    assert (
        get_vuln_line(
            """def greet(name):\nprint(f"Hello, {name}!")\n\ngreet('Alice')""",
            4,
        )
        == "greet('Alice')"
    )

    with pytest.raises(VulnNotFound):
        assert get_vuln_line(
            "",
            3,
        )


def test_generate_vuln_context() -> None:
    vuln_context = generate_vuln_context(
        CRITERIA_VULNERABILITIES["094"],
    )
    for subtitle in [
        "# Vulnerability Context:",
        "Title:",
        "Description:",
        "Recommendation:",
    ]:
        assert subtitle in vuln_context
