from integrates.vulnerabilities.fixes.estimation import (
    get_tokens_per_message,
)
from integrates.vulnerabilities.fixes.types import (
    ConverseMessage,
)
import pytest


@pytest.mark.parametrize(
    "prompt,number_of_tokens",
    [
        (
            ConverseMessage(
                content="You can use the tool below to understand how",
                role="user",
            ),
            17,
        ),
        (
            ConverseMessage(
                content="and excel at producing the next token in",
                role="user",
            ),
            16,
        ),
        (
            ConverseMessage(
                content="tokens for the same input text.", role="user"
            ),
            15,
        ),
        (
            ConverseMessage(content="some text", role="user"),
            10,
        ),
    ],
)
def test_num_tokens_from_messages_gpt(
    prompt: ConverseMessage, number_of_tokens: int
) -> None:
    assert get_tokens_per_message(prompt) == number_of_tokens
