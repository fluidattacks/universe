from integrates.vulnerabilities.fixes.context import (
    get_vuln_line,
)
from integrates.vulnerabilities.fixes.prompt import (
    generate_custom_fix_prompt,
    generate_suggested_fix_prompt,
)


def test_generate_custom_fix_prompt() -> None:
    snippet = """
def greet(name):\n
  print(f'Hello, {name}!')

  greet('Alice')"""
    line = 2
    vuln_line = get_vuln_line(
        snippet,
        line,
    )
    prompt = generate_custom_fix_prompt(
        finding_code="094",
        file_content=snippet,
        language="python",
        vulnerability_line=line,
    )
    assert prompt is not None and len(prompt) == 11
    assert prompt[0].role == "system"
    assert prompt[1].role == "user"
    assert "python" in prompt[3].content and "python" in prompt[5].content
    assert snippet[1:] in prompt[5].content
    assert prompt[7].content == (f"The vulnerability is in line `{vuln_line}` of that code snippet")
    assert prompt[-1].role == "assistant"


def test_generate_suggested_fix_prompt() -> None:
    snippet = "def greet(name):\nprint(f'Hello, {name}!')\n\ngreet('Alice')"
    line = 2
    vuln_line = get_vuln_line(
        snippet,
        line,
    )
    prompt = generate_suggested_fix_prompt(
        finding_code="094",
        vulnerable_function=snippet,
        vulnerable_line_content=vuln_line,
    )
    assert prompt is not None and len(prompt) == 8
    assert prompt[0].role == "system"
    assert prompt[1].role == "user"
    assert snippet in prompt[3].content
    assert vuln_line in prompt[5].content
    assert "Requirements" in prompt[-1].content
    assert prompt[-1].role == "user"
