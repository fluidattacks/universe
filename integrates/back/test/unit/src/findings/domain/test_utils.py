from integrates.custom_exceptions import (
    InvalidCommentParent,
)
from integrates.custom_utils.datetime import (
    get_utc_now,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.finding_comments.enums import (
    CommentType,
)
from integrates.db_model.finding_comments.types import (
    FindingComment,
    FindingCommentsRequest,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
    VulnerabilityVerificationStatus,
)
from integrates.findings.domain import (
    add_comment,
)
from integrates.findings.domain.utils import (
    get_vulnerabilities_to_reattack,
)
import pytest
from test.unit.src.utils import (
    get_module_at_test,
)
import time
from typing import (
    Any,
)
from unittest.mock import (
    AsyncMock,
    patch,
)

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"

pytestmark = [
    pytest.mark.asyncio,
]


@pytest.mark.changes_db
async def test_add_comment() -> None:
    loaders = get_new_context()
    finding_id = "463461507"
    current_time = get_utc_now()
    comment_id = str(round(time.time() * 1000))
    comment_data = FindingComment(
        finding_id=finding_id,
        comment_type=CommentType.COMMENT,
        id=comment_id,
        content="Test comment",
        creation_date=current_time,
        full_name="unittesting",
        parent_id="0",
        email="unittest@fluidattacks.com",
    )
    await add_comment(
        loaders=loaders,
        user_email="unittest@fluidattacks.com",
        comment_data=comment_data,
        finding_id=finding_id,
        group_name="unittesting",
    )
    loaders = get_new_context()
    finding_comments = await loaders.finding_comments.load(
        FindingCommentsRequest(
            comment_type=CommentType.COMMENT, finding_id=finding_id
        )
    ) + await loaders.finding_comments.load(
        FindingCommentsRequest(
            comment_type=CommentType.VERIFICATION, finding_id=finding_id
        )
    )
    assert finding_comments[-1].content == "Test comment"
    assert finding_comments[-1].full_name == "unittesting"

    current_time = get_utc_now()
    new_comment_data = comment_data._replace(
        id=str(round(time.time() * 1000)),
        creation_date=current_time,
        parent_id=str(comment_id),
    )
    await add_comment(
        loaders=loaders,
        user_email="unittest@fluidattacks.com",
        comment_data=new_comment_data,
        finding_id=finding_id,
        group_name="unittesting",
    )
    new_loaders = get_new_context()
    new_finding_comments = await new_loaders.finding_comments.load(
        FindingCommentsRequest(
            comment_type=CommentType.COMMENT, finding_id=finding_id
        )
    )
    assert new_finding_comments[-1].content == "Test comment"
    assert new_finding_comments[-1].parent_id == str(comment_id)


@pytest.mark.changes_db
async def test_add_comment_fail() -> None:
    loaders = get_new_context()
    finding_id = "463461507"
    current_time = get_utc_now()
    comment_id = str(round(time.time() * 1000))
    comment_data = FindingComment(
        finding_id=finding_id,
        comment_type=CommentType.COMMENT,
        id=comment_id,
        content="Test comment fail",
        creation_date=current_time,
        full_name="unittesting",
        parent_id="12354666",
        email="unittest@fluidattacks.com",
    )
    with pytest.raises(InvalidCommentParent):
        await add_comment(
            loaders=loaders,
            user_email="unittest@fluidattacks.com",
            comment_data=comment_data,
            finding_id=finding_id,
            group_name="unittesting",
        )


@patch(
    MODULE_AT_TEST + "Dataloaders.finding_vulnerabilities_released_nzr",
    new_callable=AsyncMock,
)
async def test_get_vulnerabilities_to_reattack(
    mock_dataloaders_finding_vulnerabilities_released_nzr: AsyncMock,
    mock_data_for_module: Any,
) -> None:
    loaders = get_new_context()
    finding_id = "463558592"

    mock_dataloaders_finding_vulnerabilities_released_nzr.load.return_value = (
        mock_data_for_module(
            mock_path="Dataloaders.finding_vulnerabilities_released_nzr",
            mock_args=[finding_id],
            module_at_test=MODULE_AT_TEST,
        )
    )

    vulns_to_reattack = await get_vulnerabilities_to_reattack(
        loaders=loaders,
        finding_id=finding_id,
    )

    assert len(vulns_to_reattack) == 2
    for vulnerability in vulns_to_reattack:
        assert (
            vulnerability.state.status == VulnerabilityStateStatus.VULNERABLE
        )
        assert vulnerability.verification
        assert (
            vulnerability.verification.status
            == VulnerabilityVerificationStatus.REQUESTED
        )
