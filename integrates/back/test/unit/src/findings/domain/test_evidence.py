import aiofiles
from collections import (
    OrderedDict,
)
import csv
from datetime import (
    datetime,
)
from decimal import (
    Decimal,
)
from integrates.custom_exceptions import (
    InvalidFileSize,
    InvalidFileType,
)
from integrates.custom_utils.findings import (
    append_records_to_file,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.enums import (
    Source,
    StateRemovalJustification,
)
from integrates.db_model.findings.enums import (
    FindingSorts,
    FindingStateStatus,
    FindingVerificationStatus,
)
from integrates.db_model.findings.types import (
    Finding,
    FindingEvidence,
    FindingEvidences,
    FindingState,
    FindingVerification,
)
from integrates.db_model.types import (
    SeverityScore,
)
from integrates.findings.domain.evidence import (
    download_evidence_file,
    get_records_from_file,
    validate_evidence,
)
import io
from io import (
    BytesIO,
)
import json
import os
import pytest
from starlette.datastructures import (
    Headers,
    UploadFile,
)
from test.unit.src.utils import (
    get_mock_response,
    get_mocked_path,
    get_module_at_test,
)
from typing import (
    Any,
)
from unittest.mock import (
    AsyncMock,
    patch,
)

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"

pytestmark = [
    pytest.mark.asyncio,
]


def _set_up_mocks_results(
    mocks_setup_list: list[tuple[AsyncMock, str, list[Any]]],
    mock_data_for_module: Any,
) -> None:
    # Set up mocks' results using mock_data_for_module fixture
    for item in mocks_setup_list:
        mock, path, arguments = item
        mock.return_value = mock_data_for_module(
            mock_path=path,
            mock_args=arguments,
            module_at_test=MODULE_AT_TEST,
        )


@pytest.mark.parametrize(
    ["header", "records", "file_name", "input_records", "output_records"],
    [
        [
            ["song", "artist", "year"],
            [["a million little pieces", "placebo", "2010"]],
            "unittesting-422286126-evidence_file.csv",
            [
                OrderedDict(
                    [
                        ("song", "heart shaped box"),
                        ("artist", "nirvana"),
                        ("year", "1992"),
                    ]
                ),
                OrderedDict(
                    [("song", "zenith"), ("artist", "ghost"), ("year", "2015")]
                ),
                OrderedDict(
                    [
                        ("song", "hysteria"),
                        ("artist", "def leppard"),
                        ("year", "1987"),
                    ]
                ),
            ],
            [
                ["heart shaped box", "nirvana", "1992"],
                ["zenith", "ghost", "2015"],
                ["hysteria", "def leppard", "1987"],
                ["a million little pieces", "placebo", "2010"],
            ],
        ],
    ],
)
async def test_append_records_to_file(
    header: list[str],
    records: list[list[str]],
    file_name: str,
    input_records: list[dict[str, str]],
    output_records: list[list[str]],
) -> None:
    new_csv = io.StringIO()
    writer = csv.writer(new_csv)
    writer.writerow(header)
    writer.writerows(records)
    buff = io.BytesIO(
        new_csv.getvalue()
        .encode("latin1")
        .decode("unicode_escape")
        .encode("utf-8")
    )
    records_file = UploadFile(filename=file_name, file=io.BytesIO())
    await records_file.write(buff.read())
    await records_file.seek(0)
    tested_file = await append_records_to_file(input_records, records_file)
    tested_file_records = await tested_file.read()
    await tested_file.seek(0)
    csv_reader = csv.reader(
        io.StringIO(tested_file_records.decode("utf-8")), skipinitialspace=True
    )
    assert next(csv_reader) == header
    assert list(csv_reader) == output_records


@pytest.mark.parametrize(
    [
        "group_name",
        "finding_id",
        "file_name",
    ],
    [
        [
            "unittesting",
            "422286126",
            "unittesting-422286126-evidence_route_1.png",
        ],
    ],
)
@patch(
    get_mocked_path("findings_storage.search_evidence"),
    new_callable=AsyncMock,
)
@patch(
    get_mocked_path("findings_storage.download_evidence"),
    new_callable=AsyncMock,
)
async def test_download_evidence_file(
    mock_download_evidence: AsyncMock,
    mock_search_evidence: AsyncMock,
    group_name: str,
    finding_id: str,
    file_name: str,
) -> None:
    mock_parameters = json.dumps([group_name, finding_id, file_name])
    mock_search_evidence.return_value = get_mock_response(
        get_mocked_path("findings_storage.search_evidence"), mock_parameters
    )
    mock_download_evidence.return_value = get_mock_response(
        get_mocked_path("findings_storage.download_evidence"), mock_parameters
    )
    test_data = await download_evidence_file(group_name, finding_id, file_name)

    expected_output = os.path.abspath(
        # FP: local testing
        "/tmp/unittesting-422286126-evidence_route_1.png"
    )
    assert test_data == expected_output


@pytest.mark.parametrize(
    ["group_name", "finding_id", "file_name", "expected_output"],
    [
        [
            "unittesting",
            "422286126",
            "unittesting-422286126-evidence_file.csv",
            [
                OrderedDict(
                    [
                        ("song", "a million little pieces"),
                        ("artist", "placebo"),
                        ("year", "2010"),
                    ]
                ),
                OrderedDict(
                    [
                        ("song", "heart shaped box"),
                        ("artist", "nirvana"),
                        ("year", "1992"),
                    ]
                ),
                OrderedDict(
                    [("song", "zenith"), ("artist", "ghost"), ("year", "2015")]
                ),
                OrderedDict(
                    [
                        ("song", "hysteria"),
                        ("artist", "def leppard"),
                        ("year", "1987"),
                    ]
                ),
                OrderedDict(
                    [("song", "mañana"), ("artist", "zoë"), ("year", "2021")]
                ),
            ],
        ],
    ],
)
@patch(get_mocked_path("download_evidence_file"), new_callable=AsyncMock)
async def test_get_records_from_file(
    mock_download_evidence_file: AsyncMock,
    group_name: str,
    finding_id: str,
    file_name: str,
    expected_output: list[dict[object, object]],
) -> None:
    mock_download_evidence_file.return_value = get_mock_response(
        get_mocked_path("download_evidence_file"),
        json.dumps([group_name, finding_id, file_name]),
    )

    test_data = await get_records_from_file(group_name, finding_id, file_name)

    assert test_data == expected_output
    assert mock_download_evidence_file.called is True


@pytest.mark.parametrize(
    ["evidence_id", "file_name", "finding", "validate_name"],
    [
        [
            "fileRecords",
            "okada-unittesting-records123.csv",
            Finding(
                group_name="unittesting",
                id="463558592",
                state=FindingState(
                    modified_by="integratesmanager@gmail.com",
                    modified_date=datetime.fromisoformat(
                        "2018-12-17T05:00:00+00:00"
                    ),
                    source=Source.ASM,
                    status=FindingStateStatus.CREATED,
                    rejection=None,
                    justification=StateRemovalJustification.NO_JUSTIFICATION,
                ),
                title="007. Cross-site request forgery",
                attack_vector_description="This is an attack vector",
                creation=FindingState(
                    modified_by="integratesmanager@gmail.com",
                    modified_date=datetime.fromisoformat(
                        "2018-04-08T00:43:18+00:00"
                    ),
                    source=Source.ASM,
                    status=FindingStateStatus.CREATED,
                    rejection=None,
                    justification=StateRemovalJustification.NO_JUSTIFICATION,
                ),
                description=(
                    "La aplicación permite engañar a un usuario "
                    "autenticado por medio de links manipulados para "
                    "ejecutaracciones sobre la aplicación sin su "
                    " consentimiento."
                ),
                evidences=FindingEvidences(
                    animation=None,
                    evidence1=FindingEvidence(
                        description="test",
                        modified_date=datetime.fromisoformat(
                            "2018-12-17T05:00:00+00:00"
                        ),
                        url="unittesting-463558592-evidence_route_1.png",
                    ),
                    evidence2=FindingEvidence(
                        description="Test2",
                        modified_date=datetime.fromisoformat(
                            "2018-12-17T05:00:00+00:00"
                        ),
                        url="unittesting-463558592-evidence_route_2.jpg",
                    ),
                    evidence3=FindingEvidence(
                        description="Test3",
                        modified_date=datetime.fromisoformat(
                            "2018-12-17T05:00:00+00:00"
                        ),
                        url="unittesting-463558592-evidence_route_3.png",
                    ),
                    evidence4=FindingEvidence(
                        description="Test4",
                        modified_date=datetime.fromisoformat(
                            "2018-12-17T05:00:00+00:00"
                        ),
                        url="unittesting-463558592-evidence_route_4.png",
                    ),
                    evidence5=FindingEvidence(
                        description="Test5",
                        modified_date=datetime.fromisoformat(
                            "2018-12-17T05:00:00+00:00"
                        ),
                        url="unittesting-463558592-evidence_route_5.png",
                    ),
                    exploitation=None,
                    records=None,
                ),
                min_time_to_remediate=18,
                recommendation=(
                    "Hacer uso de tokens en los formularios para la "
                    "verificación de las peticiones realizadas por usuarios "
                    "legítimos."
                ),
                requirements=(
                    "REQ.0174. La aplicación debe garantizar que las "
                    "peticiones que ejecuten transacciones no sigan un "
                    "patrón discernible."
                ),
                severity_score=SeverityScore(
                    base_score=Decimal("5.1"),
                    temporal_score=Decimal("4.3"),
                    cvss_v3="CVSS:3.1/AV:A/AC:H/PR:L/UI:R/S:U/C:H/I:L/A:N/E:U/"
                    "RL:O/RC:R/IR:H/MAV:A/MAC:H/MPR:L/MUI:R/MS:U/MC:H/MI:L",
                    threat_score=Decimal("0.7"),
                    cvss_v4="CVSS:4.0/AV:A/AC:H/AT:N/PR:L/UI:A/VC:H/VI:L/VA:N"
                    "/SC:L/SI:L/SA:N/E:U/IR:H/MAV:A/MAC:H/MPR:L/MUI:P/"
                    "MVC:H/MVI:L",
                    cvssf_v4=Decimal("0.010"),
                    cvssf=Decimal("1.516"),
                ),
                sorts=FindingSorts.NO,
                threat="Test.",
                unfulfilled_requirements=["029", "174"],
                verification=FindingVerification(
                    comment_id="1558048727999",
                    modified_by="integratesuser@gmail.com",
                    modified_date=datetime.fromisoformat(
                        "2020-01-19T15:41:04+00:00"
                    ),
                    status=FindingVerificationStatus.REQUESTED,
                    vulnerability_ids={
                        "3bcdb384-5547-4170-a0b6-3b397a245465",
                        "74632c0c-db08-47c2-b013-c70e5b67c49f",
                    },
                ),
            ),
            True,
        ],
    ],
)
@patch(MODULE_AT_TEST + "validate_filename", new_callable=AsyncMock)
@patch(MODULE_AT_TEST + "files_utils.get_file_size", new_callable=AsyncMock)
@patch(
    MODULE_AT_TEST + "files_utils.assert_uploaded_file_mime",
    new_callable=AsyncMock,
)
async def test_validate_evidence(
    mock_files_utils_assert_uploaded_file_mime: AsyncMock,
    mock_files_utils_get_file_size: AsyncMock,
    mock_validate_filename: AsyncMock,
    *,
    evidence_id: str,
    file_name: str,
    finding: Finding,
    validate_name: bool,
    mock_data_for_module: Any,
) -> None:
    mocks_setup_list: list[tuple[AsyncMock, str, list[Any]]] = [
        (
            mock_files_utils_assert_uploaded_file_mime,
            "files_utils.assert_uploaded_file_mime",
            [file_name, evidence_id],
        ),
        (
            mock_files_utils_get_file_size,
            "files_utils.get_file_size",
            [file_name],
        ),
        (
            mock_validate_filename,
            "validate_filename",
            [file_name, finding.id],
        ),
    ]
    _set_up_mocks_results(
        mocks_setup_list=mocks_setup_list,
        mock_data_for_module=mock_data_for_module,
    )
    filename = os.path.dirname(os.path.abspath(__file__))
    filename = os.path.join(filename, "mock/evidences/" + file_name)
    loaders = get_new_context()
    async with aiofiles.open(filename, "rb") as test_file:
        file_contents = await test_file.read()
        test_data = await validate_evidence(
            evidence_id=evidence_id,
            file=UploadFile(
                filename=file_name,
                headers=Headers(headers={"content_type": "text/csv"}),
                file=BytesIO(file_contents),
            ),
            loaders=loaders,
            finding=finding,
            validate_name=validate_name,
        )
    assert isinstance(test_data, bool)
    assert test_data
    mocks_list = [mock[0] for mock in mocks_setup_list]
    assert all(mock_object.called is True for mock_object in mocks_list)


@pytest.mark.parametrize(
    ("evidence_id", "file_name", "finding"),
    (
        (
            "fileRecords",
            "test-file-records.csv",
            Finding(
                group_name="unittesting",
                id="422286126",
                state=FindingState(
                    modified_by="integratesmanager@gmail.com",
                    modified_date=datetime.fromisoformat(
                        "2018-07-09T05:00:00+00:00"
                    ),
                    source=Source.ASM,
                    status=FindingStateStatus.CREATED,
                    rejection=None,
                    justification=StateRemovalJustification.NO_JUSTIFICATION,
                ),
                title="060. Insecure service configuration - Host "
                "verification",
                attack_vector_description="This is an attack vector",
                creation=FindingState(
                    modified_by="integratesmanager@gmail.com",
                    modified_date=datetime.fromisoformat(
                        "2018-04-08T00:43:18+00:00"
                    ),
                    source=Source.ASM,
                    status=FindingStateStatus.CREATED,
                    rejection=None,
                    justification=StateRemovalJustification.NO_JUSTIFICATION,
                ),
                description=(
                    "The source code uses generic exceptions to handle "
                    "unexpected errors. Catching generic exceptions obscures "
                    "the problem that caused the error and promotes a "
                    "generic way to handle different categories or sources "
                    "of error. This may cause security vulnerabilities to "
                    "materialize, as some special flows go unnoticed."
                ),
                evidences=FindingEvidences(
                    animation=FindingEvidence(
                        description="Test description",
                        modified_date=datetime.fromisoformat(
                            "2018-07-09T05:00:00+00:00"
                        ),
                        url="unittesting-422286126-animation.gif",
                    ),
                    evidence1=FindingEvidence(
                        description="this is a test description",
                        modified_date=datetime.fromisoformat(
                            "2018-07-09T05:00:00+00:00"
                        ),
                        url="unittesting-422286126-evidence_route_1.png",
                    ),
                    evidence2=FindingEvidence(
                        description="exception",
                        modified_date=datetime.fromisoformat(
                            "2018-07-09T05:00:00+00:00"
                        ),
                        url="unittesting-422286126-evidence_route_2.jpg",
                    ),
                    evidence3=FindingEvidence(
                        description="Description",
                        modified_date=datetime.fromisoformat(
                            "2018-07-09T05:00:00+00:00"
                        ),
                        url="unittesting-422286126-evidence_route_3.png",
                    ),
                    evidence4=FindingEvidence(
                        description="changed for testing purposesese",
                        modified_date=datetime.fromisoformat(
                            "2018-07-09T05:00:00+00:00"
                        ),
                        url="unittesting-422286126-evidence_route_4.png",
                    ),
                    evidence5=FindingEvidence(
                        description="Test description",
                        modified_date=datetime.fromisoformat(
                            "2018-07-09T05:00:00+00:00"
                        ),
                        url="unittesting-422286126-evidence_route_5.png",
                    ),
                    exploitation=FindingEvidence(
                        description="test",
                        modified_date=datetime.fromisoformat(
                            "2018-07-09T05:00:00+00:00"
                        ),
                        url="unittesting-422286126-exploitation.png",
                    ),
                    records=FindingEvidence(
                        description="test",
                        modified_date=datetime.fromisoformat(
                            "2018-07-09T05:00:00+00:00"
                        ),
                        url="unittesting-422286126-evidence_file.csv",
                    ),
                ),
                min_time_to_remediate=18,
                recommendation=(
                    "Implement password politicies with the best "
                    "practices for strong passwords."
                ),
                requirements="R359. Avoid using generic exceptions.",
                severity_score=SeverityScore(
                    base_score=Decimal("3.5"),
                    cvss_v4="CVSS:4.0/AV:A/AC:H/AT:N/PR:L/UI:N/VC:L/VI:L/VA:L/"
                    "SC:N/SI:N/SA:N/MAV:A/MAC:H/MPR:L/MUI:N/MVC:L/MVI:L/MVA:L/"
                    "MSC:N/MSI:N/MSA:N/E:P",
                    threat_score=Decimal("1.2"),
                    cvssf_v4=Decimal("0.021"),
                    temporal_score=Decimal("2.9"),
                    cvss_v3="CVSS:3.1/AV:A/AC:L/PR:L/UI:N/S:U/C:N/I:L/A:N/E:U/"
                    "RL:W/RC:U/MAV:A/MAC:L/MPR:L/MUI:N/MS:U/MI:L",
                    cvssf=Decimal("0.218"),
                ),
                sorts=FindingSorts.NO,
                threat=(
                    "An attacker can get passwords of users and "
                    "impersonatethem or used the credentials for practices "
                    "maliciosus."
                ),
                unfulfilled_requirements=["266"],
                verification=None,
            ),
        ),
    ),
)
@patch(
    MODULE_AT_TEST + "files_utils.assert_uploaded_file_mime",
    new_callable=AsyncMock,
)
async def test_validate_evidence_invalid_type(
    mock_files_utils_assert_uploaded_file_mime: AsyncMock,
    evidence_id: str,
    file_name: str,
    finding: Finding,
    mock_data_for_module: Any,
) -> None:
    # Set up mock's result using mock_data_for_module fixture
    mock_files_utils_assert_uploaded_file_mime.return_value = (
        mock_data_for_module(
            mock_path="files_utils.assert_uploaded_file_mime",
            mock_args=[file_name, evidence_id],
            module_at_test=MODULE_AT_TEST,
        )
    )
    loaders = get_new_context()
    filename = os.path.dirname(os.path.abspath(__file__))
    filename = os.path.join(filename, "mock/evidences/" + file_name)
    mime_type = "image/png"

    async with aiofiles.open(filename, "rb") as test_file:
        headers = Headers(headers={"content_type": mime_type})
        file_contents = await test_file.read()
        uploaded_file = UploadFile(
            filename="test-file-records.csv",
            headers=headers,
            file=BytesIO(file_contents),
        )
        with pytest.raises(InvalidFileType):
            await validate_evidence(
                evidence_id=evidence_id,
                file=uploaded_file,
                loaders=loaders,
                finding=finding,
            )
    assert mock_files_utils_assert_uploaded_file_mime.called is True


@pytest.mark.parametrize(
    ("evidence_id", "file_name", "finding"),
    (
        (
            "animation",
            "test-big-image.png",
            Finding(
                group_name="unittesting",
                id="422286126",
                state=FindingState(
                    modified_by="integratesmanager@gmail.com",
                    modified_date=datetime.fromisoformat(
                        "2018-07-09T05:00:00+00:00"
                    ),
                    source=Source.ASM,
                    status=FindingStateStatus.CREATED,
                    rejection=None,
                    justification=StateRemovalJustification.NO_JUSTIFICATION,
                ),
                title="060. Insecure service configuration - Host "
                "verification",
                attack_vector_description="This is an attack vector",
                creation=FindingState(
                    modified_by="integratesmanager@gmail.com",
                    modified_date=datetime.fromisoformat(
                        "2018-04-08T00:43:18+00:00"
                    ),
                    source=Source.ASM,
                    status=FindingStateStatus.CREATED,
                    rejection=None,
                    justification=StateRemovalJustification.NO_JUSTIFICATION,
                ),
                description=(
                    "The source code uses generic exceptions to handle "
                    "unexpected errors. Catching generic exceptions obscures "
                    "the problem that caused the error and promotes a "
                    "generic way to handle different categories or sources "
                    "of error. This may cause security vulnerabilities to "
                    "materialize, as some special flows go unnoticed."
                ),
                evidences=FindingEvidences(
                    animation=FindingEvidence(
                        description="Test description",
                        modified_date=datetime.fromisoformat(
                            "2018-07-09T05:00:00+00:00"
                        ),
                        url="unittesting-422286126-animation.gif",
                    ),
                    evidence1=FindingEvidence(
                        description="this is a test description",
                        modified_date=datetime.fromisoformat(
                            "2018-07-09T05:00:00+00:00"
                        ),
                        url="unittesting-422286126-evidence_route_1.png",
                    ),
                    evidence2=FindingEvidence(
                        description="exception",
                        modified_date=datetime.fromisoformat(
                            "2018-07-09T05:00:00+00:00"
                        ),
                        url="unittesting-422286126-evidence_route_2.jpg",
                    ),
                    evidence3=FindingEvidence(
                        description="Description",
                        modified_date=datetime.fromisoformat(
                            "2018-07-09T05:00:00+00:00"
                        ),
                        url="unittesting-422286126-evidence_route_3.png",
                    ),
                    evidence4=FindingEvidence(
                        description="changed for testing purposesese",
                        modified_date=datetime.fromisoformat(
                            "2018-07-09T05:00:00+00:00"
                        ),
                        url="unittesting-422286126-evidence_route_4.png",
                    ),
                    evidence5=FindingEvidence(
                        description="Test description",
                        modified_date=datetime.fromisoformat(
                            "2018-07-09T05:00:00+00:00"
                        ),
                        url="unittesting-422286126-evidence_route_5.png",
                    ),
                    exploitation=FindingEvidence(
                        description="test",
                        modified_date=datetime.fromisoformat(
                            "2018-07-09T05:00:00+00:00"
                        ),
                        url="unittesting-422286126-exploitation.png",
                    ),
                    records=FindingEvidence(
                        description="test",
                        modified_date=datetime.fromisoformat(
                            "2018-07-09T05:00:00+00:00"
                        ),
                        url="unittesting-422286126-evidence_file.csv",
                    ),
                ),
                min_time_to_remediate=18,
                recommendation=(
                    "Implement password politicies with the best "
                    "practices for strong passwords."
                ),
                requirements="R359. Avoid using generic exceptions.",
                severity_score=SeverityScore(
                    base_score=Decimal("3.5"),
                    temporal_score=Decimal("2.9"),
                    cvss_v3="CVSS:3.1/AV:A/AC:L/PR:L/UI:N/S:U/C:N/I:L/A:N/E:U/"
                    "RL:W/RC:U/MAV:A/MAC:L/MPR:L/MUI:N/MS:U/MI:L",
                    threat_score=Decimal("1.1"),
                    cvss_v4="CVSS:4.0/AV:P/AC:H/AT:N/PR:L/UI:N/VC:L/VI:L/VA:L"
                    "/SC:L/SI:L/SA:L/E:P/CR:L/AR:L/MAV:N/MAC:H/MPR:H/MUI:P/"
                    "MVC:L/MVA:L",
                    cvssf_v4=Decimal("0.018"),
                    cvssf=Decimal("0.218"),
                ),
                sorts=FindingSorts.NO,
                threat=(
                    "An attacker can get passwords of users and "
                    "impersonatethem or used the credentials for practices "
                    "maliciosus."
                ),
                unfulfilled_requirements=["266"],
                verification=None,
            ),
        ),
    ),
)
@patch(MODULE_AT_TEST + "files_utils.get_file_size", new_callable=AsyncMock)
@patch(
    MODULE_AT_TEST + "files_utils.assert_uploaded_file_mime",
    new_callable=AsyncMock,
)
async def test_validate_evidence_invalid_size(
    mock_files_utils_assert_uploaded_file_mime: AsyncMock,
    mock_files_utils_get_file_size: AsyncMock,
    *,
    evidence_id: str,
    file_name: str,
    finding: Finding,
    mock_data_for_module: Any,
) -> None:
    mocks_setup_list: list[tuple[AsyncMock, str, list[Any]]] = [
        (
            mock_files_utils_assert_uploaded_file_mime,
            "files_utils.assert_uploaded_file_mime",
            [file_name, evidence_id],
        ),
        (
            mock_files_utils_get_file_size,
            "files_utils.get_file_size",
            [file_name],
        ),
    ]
    _set_up_mocks_results(
        mocks_setup_list=mocks_setup_list,
        mock_data_for_module=mock_data_for_module,
    )
    loaders = get_new_context()
    filename = os.path.dirname(os.path.abspath(__file__))
    filename = os.path.join(filename, "mock/evidences/" + file_name)
    async with aiofiles.open(filename, "rb") as test_file:
        headers = Headers(headers={"content_type": "image/png"})
        file_contents = await test_file.read()
        uploaded_file = UploadFile(
            filename=file_name, headers=headers, file=BytesIO(file_contents)
        )
        with pytest.raises(InvalidFileSize):
            await validate_evidence(
                evidence_id=evidence_id,
                file=uploaded_file,
                loaders=loaders,
                finding=finding,
            )
    mocks_list = [mock[0] for mock in mocks_setup_list]
    assert all(mock_object.called is True for mock_object in mocks_list)
