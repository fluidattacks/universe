# pylint: disable=too-many-lines
from datetime import UTC, datetime

import pytest

from integrates.custom_utils.datetime import get_report_dates
from integrates.dataloaders import get_new_context
from integrates.db_model.toe_lines.types import (
    GroupHistoricToeLinesRequest,
    GroupToeLinesRequest,
    RootToeLinesRequest,
    SortsSuggestion,
    ToeLine,
    ToeLinesConnection,
    ToeLineEdge,
    ToeLineEnrichedState,
    ToeLinesEnrichedStatesConnection,
    ToeLineRequest,
    ToeLineState,
)
from integrates.db_model.types import Edge
from integrates.dynamodb.types import PageInfo

# Constants
pytestmark = [
    pytest.mark.asyncio,
]


async def test_get() -> None:
    loaders = get_new_context()
    group_name = "unittesting"
    root_id = "4039d098-ffc5-4984-8ed3-eb17bca98e19"
    filename = "test/test#.config"
    toe_lines = await loaders.toe_lines.load(
        ToeLineRequest(group_name=group_name, root_id=root_id, filename=filename)
    )
    assert toe_lines == ToeLine(
        filename="test/test#.config",
        group_name="unittesting",
        root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
        state=ToeLineState(
            attacked_at=datetime.fromisoformat("2021-02-20T05:00:00+00:00"),
            attacked_by="test2@test.com",
            attacked_lines=4,
            be_present=True,
            be_present_until=None,
            comments="comment 1",
            has_vulnerabilities=True,
            last_author="user@gmail.com",
            last_commit="f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c2",
            last_commit_date=datetime.fromisoformat("2020-11-15T15:41:04+00:00"),
            loc=180,
            first_attack_at=datetime.fromisoformat("2020-02-19T15:41:04+00:00"),
            modified_by="test2@test.com",
            modified_date=datetime.fromisoformat("2020-11-15T15:41:04+00:00"),
            seen_at=datetime.fromisoformat("2020-02-01T15:41:04+00:00"),
            sorts_risk_level=80,
            sorts_priority_factor=70,
            sorts_risk_level_date=datetime.fromisoformat("2021-02-20T05:00:00+00:00"),
            sorts_suggestions=[
                SortsSuggestion(finding_title="083. XML injection (XXE)", probability=90),
                SortsSuggestion(
                    finding_title=("033. Password change without identity check"),
                    probability=50,
                ),
            ],
        ),
    )
    group_name = "unittesting"
    root_id = "765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a"
    filename = "test2/test.sh"
    toe_lines = await loaders.toe_lines.load(
        ToeLineRequest(group_name=group_name, root_id=root_id, filename=filename)
    )
    assert toe_lines == ToeLine(
        filename="test2/test.sh",
        group_name="unittesting",
        root_id="765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a",
        state=ToeLineState(
            attacked_at=datetime.fromisoformat("2021-01-20T05:00:00+00:00"),
            attacked_by="test@test.com",
            attacked_lines=120,
            be_present=False,
            be_present_until=datetime.fromisoformat("2021-01-01T15:41:04+00:00"),
            comments="comment 2",
            first_attack_at=datetime.fromisoformat("2020-01-19T15:41:04+00:00"),
            has_vulnerabilities=False,
            last_author="user@gmail.com",
            last_commit="f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c1",
            last_commit_date=datetime.fromisoformat("2020-11-16T15:41:04+00:00"),
            loc=172,
            modified_by="test2@test.com",
            modified_date=datetime.fromisoformat("2020-11-16T15:41:04+00:00"),
            seen_at=datetime.fromisoformat("2020-01-01T15:41:04+00:00"),
            sorts_risk_level=0,
            sorts_priority_factor=10,
            sorts_risk_level_date=datetime.fromisoformat("2021-04-10T05:00:00+00:00"),
            sorts_suggestions=[
                SortsSuggestion(finding_title="027. Insecure file upload", probability=100),
            ],
        ),
    )


async def test_get_historic_by_group() -> None:
    group_name = "unittesting"
    loaders = get_new_context()
    test_dates = get_report_dates(datetime.fromisoformat("2021-02-21T00:00:00+00:00"))
    group_historic_toe_lines = await loaders.group_toe_lines_enriched_historic_state.load(
        GroupHistoricToeLinesRequest(
            group_name=group_name,
            start_date=test_dates.past_day.start_date,
            end_date=test_dates.today.end_date,
        )
    )
    assert group_historic_toe_lines == ToeLinesEnrichedStatesConnection(
        edges=(
            Edge(
                node=ToeLineEnrichedState(
                    filename="path/to/test4.py",
                    group_name="unittesting",
                    root_id="765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a",
                    state=ToeLineState(
                        attacked_at=datetime(2021, 2, 20, 5, 0, tzinfo=UTC),
                        attacked_by="test2@test.com",
                        attacked_lines=10,
                        be_present=True,
                        be_present_until=None,
                        comments="comment 6",
                        first_attack_at=datetime(
                            2020,
                            2,
                            19,
                            15,
                            41,
                            4,
                            tzinfo=UTC,
                        ),
                        has_vulnerabilities=False,
                        last_author="user@gmail.com",
                        last_commit="e17059d1e17059d1e17059d1e17059d1e17059d1",
                        last_commit_date=datetime(2021, 2, 20, 5, 0, tzinfo=UTC),
                        loc=34,
                        modified_by="test2@test.com",
                        modified_date=datetime(2021, 2, 20, 5, 0, tzinfo=UTC),
                        seen_at=datetime(2020, 2, 1, 15, 41, 4, tzinfo=UTC),
                        sorts_risk_level=80,
                        sorts_priority_factor=70,
                        sorts_risk_level_date=datetime(2021, 3, 30, 5, 0, tzinfo=UTC),
                        sorts_suggestions=[
                            SortsSuggestion(
                                finding_title="083. XML injection (XXE)",
                                probability=90,
                            ),
                            SortsSuggestion(
                                finding_title="033. Password change without identity check",
                                probability=50,
                            ),
                        ],
                    ),
                ),
                cursor=(
                    "eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyNMSU5FUyNST09UIzc2NWIxZ"
                    "DBmLWI2ZmItNDQ4NS1iNGUyLTJjMmNiMTU1NWIxYSNGSUxFTkFNRSNwYX"
                    "RoL3RvL3Rlc3Q0LnB5IiwgInNrIjogIlNUQVRFI3N0YXRlI0RBVEUjMjA"
                    "yMS0wMi0yMFQwNTowMDowMCswMDowMCIsICJwa18yIjogIkdST1VQI0xJ"
                    "TkVTI1JPT1QjRklMRU5BTUUjdW5pdHRlc3RpbmciLCAic2tfMiI6ICJTV"
                    "EFURSNzdGF0ZSNEQVRFIzIwMjEtMDItMjBUMDU6MDA6MDArMDA6MDAifQ"
                    "=="
                ),
            ),
            Edge(
                node=ToeLineEnrichedState(
                    filename="path/to/test1.py",
                    group_name="unittesting",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    state=ToeLineState(
                        attacked_at=datetime(2021, 2, 20, 5, 0, tzinfo=UTC),
                        attacked_by="test2@test.com",
                        attacked_lines=500,
                        be_present=False,
                        be_present_until=None,
                        comments="comment 3",
                        first_attack_at=datetime(
                            2020,
                            2,
                            19,
                            15,
                            41,
                            4,
                            tzinfo=UTC,
                        ),
                        has_vulnerabilities=False,
                        last_author="user@gmail.com",
                        last_commit="e17059d1e17059d1e17059d1e17059d1e17059d1",
                        last_commit_date=datetime(2021, 2, 20, 5, 0, tzinfo=UTC),
                        loc=500,
                        modified_by="test2@test.com",
                        modified_date=datetime(2021, 2, 20, 5, 0, tzinfo=UTC),
                        seen_at=datetime(2020, 2, 1, 15, 41, 4, tzinfo=UTC),
                        sorts_risk_level=50,
                        sorts_priority_factor=40,
                        sorts_risk_level_date=datetime(2021, 3, 30, 5, 0, tzinfo=UTC),
                        sorts_suggestions=[
                            SortsSuggestion(
                                finding_title="083. XML injection (XXE)",
                                probability=90,
                            )
                        ],
                    ),
                ),
                cursor=(
                    "eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyNMSU5FUyNST09UIzQwMzlkM"
                    "Dk4LWZmYzUtNDk4NC04ZWQzLWViMTdiY2E5OGUxOSNGSUxFTkFNRSNwYX"
                    "RoL3RvL3Rlc3QxLnB5IiwgInNrIjogIlNUQVRFI3N0YXRlI0RBVEUjMjA"
                    "yMS0wMi0yMFQwNTowMDowMCswMDowMCIsICJwa18yIjogIkdST1VQI0xJ"
                    "TkVTI1JPT1QjRklMRU5BTUUjdW5pdHRlc3RpbmciLCAic2tfMiI6ICJTV"
                    "EFURSNzdGF0ZSNEQVRFIzIwMjEtMDItMjBUMDU6MDA6MDArMDA6MDAifQ"
                    "=="
                ),
            ),
            Edge(
                node=ToeLineEnrichedState(
                    filename="path/to/test2.py",
                    group_name="unittesting",
                    root_id="765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a",
                    state=ToeLineState(
                        attacked_at=datetime(2021, 2, 20, 5, 0, tzinfo=UTC),
                        attacked_by="test2@test.com",
                        attacked_lines=0,
                        be_present=True,
                        be_present_until=None,
                        comments="comment 4",
                        first_attack_at=datetime(
                            2020,
                            2,
                            19,
                            15,
                            41,
                            4,
                            tzinfo=UTC,
                        ),
                        has_vulnerabilities=False,
                        last_author="user@gmail.com",
                        last_commit="e17059d1e17059d1e17059d1e17059d1e17059d1",
                        last_commit_date=datetime(2021, 2, 20, 5, 0, tzinfo=UTC),
                        loc=100,
                        modified_by="test2@test.com",
                        modified_date=datetime(2021, 2, 20, 5, 0, tzinfo=UTC),
                        seen_at=datetime(2020, 2, 1, 15, 41, 4, tzinfo=UTC),
                        sorts_risk_level=80,
                        sorts_priority_factor=70,
                        sorts_risk_level_date=datetime(2021, 3, 30, 5, 0, tzinfo=UTC),
                        sorts_suggestions=[
                            SortsSuggestion(
                                finding_title="033. Password change without identity check",
                                probability=50,
                            )
                        ],
                    ),
                ),
                cursor=(
                    "eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyNMSU5FUyNST09UIzc2NWIxZ"
                    "DBmLWI2ZmItNDQ4NS1iNGUyLTJjMmNiMTU1NWIxYSNGSUxFTkFNRSNwYX"
                    "RoL3RvL3Rlc3QyLnB5IiwgInNrIjogIlNUQVRFI3N0YXRlI0RBVEUjMjA"
                    "yMS0wMi0yMFQwNTowMDowMCswMDowMCIsICJwa18yIjogIkdST1VQI0xJ"
                    "TkVTI1JPT1QjRklMRU5BTUUjdW5pdHRlc3RpbmciLCAic2tfMiI6ICJTV"
                    "EFURSNzdGF0ZSNEQVRFIzIwMjEtMDItMjBUMDU6MDA6MDArMDA6MDAifQ"
                    "=="
                ),
            ),
            Edge(
                node=ToeLineEnrichedState(
                    filename="test2/test.sh",
                    group_name="unittesting",
                    root_id="765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a",
                    state=ToeLineState(
                        attacked_at=datetime(2021, 1, 20, 5, 0, tzinfo=UTC),
                        attacked_by="test@test.com",
                        attacked_lines=120,
                        be_present=False,
                        be_present_until=datetime(2021, 1, 1, 15, 41, 4, tzinfo=UTC),
                        comments="comment 2",
                        first_attack_at=datetime(
                            2020,
                            1,
                            19,
                            15,
                            41,
                            4,
                            tzinfo=UTC,
                        ),
                        has_vulnerabilities=False,
                        last_author="user@gmail.com",
                        last_commit="f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c1",
                        last_commit_date=datetime(2021, 2, 20, 5, 0, tzinfo=UTC),
                        loc=172,
                        modified_by="test2@test.com",
                        modified_date=datetime(2021, 2, 20, 5, 0, tzinfo=UTC),
                        seen_at=datetime(2020, 1, 1, 15, 41, 4, tzinfo=UTC),
                        sorts_risk_level=0,
                        sorts_priority_factor=15,
                        sorts_risk_level_date=datetime(2021, 4, 10, 5, 0, tzinfo=UTC),
                        sorts_suggestions=[
                            SortsSuggestion(
                                finding_title="027. Insecure file upload",
                                probability=100,
                            )
                        ],
                    ),
                ),
                cursor=(
                    "eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyNMSU5FUyNST09UIzc2NWIxZ"
                    "DBmLWI2ZmItNDQ4NS1iNGUyLTJjMmNiMTU1NWIxYSNGSUxFTkFNRSN0ZX"
                    "N0Mi90ZXN0LnNoIiwgInNrIjogIlNUQVRFI3N0YXRlI0RBVEUjMjAyMS0"
                    "wMi0yMFQwNTowMDowMCswMDowMCIsICJwa18yIjogIkdST1VQI0xJTkVT"
                    "I1JPT1QjRklMRU5BTUUjdW5pdHRlc3RpbmciLCAic2tfMiI6ICJTVEFUR"
                    "SNzdGF0ZSNEQVRFIzIwMjEtMDItMjBUMDU6MDA6MDArMDA6MDAifQ=="
                ),
            ),
            Edge(
                node=ToeLineEnrichedState(
                    filename="test/test#.config",
                    group_name="unittesting",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    state=ToeLineState(
                        attacked_at=datetime(2021, 2, 20, 5, 0, tzinfo=UTC),
                        attacked_by="test2@test.com",
                        attacked_lines=4,
                        be_present=True,
                        be_present_until=None,
                        comments="comment 1",
                        first_attack_at=datetime(
                            2020,
                            2,
                            19,
                            15,
                            41,
                            4,
                            tzinfo=UTC,
                        ),
                        has_vulnerabilities=False,
                        last_author="user@gmail.com",
                        last_commit="f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c2",
                        last_commit_date=datetime(2021, 2, 20, 5, 0, tzinfo=UTC),
                        loc=180,
                        modified_by="test2@test.com",
                        modified_date=datetime(2021, 2, 20, 5, 0, tzinfo=UTC),
                        seen_at=datetime(2020, 2, 1, 15, 41, 4, tzinfo=UTC),
                        sorts_risk_level=80,
                        sorts_priority_factor=70,
                        sorts_risk_level_date=datetime(2021, 2, 20, 5, 0, tzinfo=UTC),
                        sorts_suggestions=[
                            SortsSuggestion(
                                finding_title="083. XML injection (XXE)",
                                probability=90,
                            ),
                            SortsSuggestion(
                                finding_title="033. Password change without identity check",
                                probability=50,
                            ),
                        ],
                    ),
                ),
                cursor=(
                    "eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyNMSU5FUyNST09UIzQwMzlkM"
                    "Dk4LWZmYzUtNDk4NC04ZWQzLWViMTdiY2E5OGUxOSNGSUxFTkFNRSN0ZX"
                    "N0L3Rlc3QjLmNvbmZpZyIsICJzayI6ICJTVEFURSNzdGF0ZSNEQVRFIzI"
                    "wMjEtMDItMjBUMDU6MDA6MDArMDA6MDAiLCAicGtfMiI6ICJHUk9VUCNM"
                    "SU5FUyNST09UI0ZJTEVOQU1FI3VuaXR0ZXN0aW5nIiwgInNrXzIiOiAiU"
                    "1RBVEUjc3RhdGUjREFURSMyMDIxLTAyLTIwVDA1OjAwOjAwKzAwOjAwIn"
                    "0="
                ),
            ),
            Edge(
                node=ToeLineEnrichedState(
                    filename="path/to/test3.py",
                    group_name="unittesting",
                    root_id="765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a",
                    state=ToeLineState(
                        attacked_at=datetime(2021, 2, 20, 5, 0, tzinfo=UTC),
                        attacked_by="test2@test.com",
                        attacked_lines=50,
                        be_present=True,
                        be_present_until=None,
                        comments="comment 5",
                        first_attack_at=datetime(
                            2020,
                            2,
                            19,
                            15,
                            41,
                            4,
                            tzinfo=UTC,
                        ),
                        has_vulnerabilities=False,
                        last_author="user@gmail.com",
                        last_commit="e17059d1e17059d1e17059d1e17059d1e17059d1",
                        last_commit_date=datetime(2021, 2, 20, 5, 0, tzinfo=UTC),
                        loc=350,
                        modified_by="test2@test.com",
                        modified_date=datetime(2021, 2, 20, 5, 0, tzinfo=UTC),
                        seen_at=datetime(2020, 2, 1, 15, 41, 4, tzinfo=UTC),
                        sorts_risk_level=100,
                        sorts_priority_factor=90,
                        sorts_risk_level_date=datetime(2021, 3, 30, 5, 0, tzinfo=UTC),
                        sorts_suggestions=[
                            SortsSuggestion(
                                finding_title="027. Insecure file upload",
                                probability=100,
                            )
                        ],
                    ),
                ),
                cursor=(
                    "eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyNMSU5FUyNST09UIzc2NWIxZ"
                    "DBmLWI2ZmItNDQ4NS1iNGUyLTJjMmNiMTU1NWIxYSNGSUxFTkFNRSNwYX"
                    "RoL3RvL3Rlc3QzLnB5IiwgInNrIjogIlNUQVRFI3N0YXRlI0RBVEUjMjA"
                    "yMS0wMi0yMFQwNTowMDowMCswMDowMCIsICJwa18yIjogIkdST1VQI0xJ"
                    "TkVTI1JPT1QjRklMRU5BTUUjdW5pdHRlc3RpbmciLCAic2tfMiI6ICJTV"
                    "EFURSNzdGF0ZSNEQVRFIzIwMjEtMDItMjBUMDU6MDA6MDArMDA6MDAifQ"
                    "=="
                ),
            ),
        ),
        page_info=PageInfo(has_next_page=False, end_cursor="bnVsbA=="),
    )


async def test_get_by_group() -> None:
    group_name = "unittesting"
    loaders = get_new_context()
    group_toe_lines = await loaders.group_toe_lines.load(
        GroupToeLinesRequest(group_name=group_name)
    )
    assert group_toe_lines == ToeLinesConnection(
        edges=(
            ToeLineEdge(
                node=ToeLine(
                    filename="path/to/test1.py",
                    group_name="unittesting",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    state=ToeLineState(
                        attacked_at=datetime(2021, 2, 20, 5, 0, tzinfo=UTC),
                        attacked_by="test2@test.com",
                        attacked_lines=500,
                        be_present=False,
                        be_present_until=None,
                        comments="comment 3",
                        first_attack_at=datetime(2020, 2, 19, 15, 41, 4, tzinfo=UTC),
                        has_vulnerabilities=False,
                        last_author="user@gmail.com",
                        last_commit="e17059d1e17059d1e17059d1e17059d1e17059d1",
                        last_commit_date=datetime(2020, 11, 15, 15, 41, 4, tzinfo=UTC),
                        loc=500,
                        modified_by="test2@test.com",
                        modified_date=datetime(2020, 11, 15, 15, 41, 4, tzinfo=UTC),
                        seen_at=datetime(2020, 2, 1, 15, 41, 4, tzinfo=UTC),
                        sorts_risk_level=50,
                        sorts_priority_factor=40,
                        sorts_risk_level_date=datetime(2021, 3, 30, 5, 0, tzinfo=UTC),
                        sorts_suggestions=[
                            SortsSuggestion(
                                finding_title="083. XML injection (XXE)", probability=90
                            )
                        ],
                    ),
                    seen_first_time_by=None,
                ),
                cursor="eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyIsICJzayI6ICJMSU5FUyNST09UIzQwMzlkMDk4LWZmYzUtNDk4NC04ZWQzLWViMTdiY2E5OGUxOSNGSUxFTkFNRSNwYXRoL3RvL3Rlc3QxLnB5In0=",
            ),
            ToeLineEdge(
                node=ToeLine(
                    filename="test/test#.config",
                    group_name="unittesting",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    state=ToeLineState(
                        attacked_at=datetime(2021, 2, 20, 5, 0, tzinfo=UTC),
                        attacked_by="test2@test.com",
                        attacked_lines=4,
                        be_present=True,
                        be_present_until=None,
                        comments="comment 1",
                        first_attack_at=datetime(2020, 2, 19, 15, 41, 4, tzinfo=UTC),
                        has_vulnerabilities=True,
                        last_author="user@gmail.com",
                        last_commit="f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c2",
                        last_commit_date=datetime(2020, 11, 15, 15, 41, 4, tzinfo=UTC),
                        loc=180,
                        modified_by="test2@test.com",
                        modified_date=datetime(2020, 11, 15, 15, 41, 4, tzinfo=UTC),
                        seen_at=datetime(2020, 2, 1, 15, 41, 4, tzinfo=UTC),
                        sorts_risk_level=80,
                        sorts_priority_factor=70,
                        sorts_risk_level_date=datetime(2021, 2, 20, 5, 0, tzinfo=UTC),
                        sorts_suggestions=[
                            SortsSuggestion(
                                finding_title="083. XML injection (XXE)", probability=90
                            ),
                            SortsSuggestion(
                                finding_title="033. Password change without identity check",
                                probability=50,
                            ),
                        ],
                    ),
                    seen_first_time_by=None,
                ),
                cursor="eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyIsICJzayI6ICJMSU5FUyNST09UIzQwMzlkMDk4LWZmYzUtNDk4NC04ZWQzLWViMTdiY2E5OGUxOSNGSUxFTkFNRSN0ZXN0L3Rlc3QjLmNvbmZpZyJ9",
            ),
            ToeLineEdge(
                node=ToeLine(
                    filename="universe/path/to/file3.ext",
                    group_name="unittesting",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    state=ToeLineState(
                        attacked_at=datetime(2021, 2, 20, 5, 0, tzinfo=UTC),
                        attacked_by="test2@test.com",
                        attacked_lines=4,
                        be_present=True,
                        be_present_until=None,
                        comments="comment 1",
                        first_attack_at=datetime(2020, 2, 19, 15, 41, 4, tzinfo=UTC),
                        has_vulnerabilities=False,
                        last_author="user@gmail.com",
                        last_commit="e17059d1e17059d1e17059d1e17059d1e17059d1",
                        last_commit_date=datetime(2020, 11, 15, 15, 41, 4, tzinfo=UTC),
                        loc=350,
                        modified_by="test2@test.com",
                        modified_date=datetime(2020, 11, 15, 15, 41, 4, tzinfo=UTC),
                        seen_at=datetime(2020, 2, 1, 15, 41, 4, tzinfo=UTC),
                        sorts_risk_level=80,
                        sorts_priority_factor=70,
                        sorts_risk_level_date=datetime(2021, 3, 30, 5, 0, tzinfo=UTC),
                        sorts_suggestions=[
                            SortsSuggestion(
                                finding_title="083. XML injection (XXE)", probability=90
                            ),
                            SortsSuggestion(
                                finding_title="033. Password change without identity check",
                                probability=50,
                            ),
                        ],
                    ),
                    seen_first_time_by=None,
                ),
                cursor="eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyIsICJzayI6ICJMSU5FUyNST09UIzQwMzlkMDk4LWZmYzUtNDk4NC04ZWQzLWViMTdiY2E5OGUxOSNGSUxFTkFNRSN1bml2ZXJzZS9wYXRoL3RvL2ZpbGUzLmV4dCJ9",
            ),
            ToeLineEdge(
                node=ToeLine(
                    filename="path/to/test2.py",
                    group_name="unittesting",
                    root_id="765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a",
                    state=ToeLineState(
                        attacked_at=datetime(2021, 2, 20, 5, 0, tzinfo=UTC),
                        attacked_by="test2@test.com",
                        attacked_lines=0,
                        be_present=True,
                        be_present_until=None,
                        comments="comment 4",
                        first_attack_at=datetime(2020, 2, 19, 15, 41, 4, tzinfo=UTC),
                        has_vulnerabilities=False,
                        last_author="user@gmail.com",
                        last_commit="e17059d1e17059d1e17059d1e17059d1e17059d1",
                        last_commit_date=datetime(2020, 11, 15, 15, 41, 4, tzinfo=UTC),
                        loc=100,
                        modified_by="test2@test.com",
                        modified_date=datetime(2020, 11, 15, 15, 41, 4, tzinfo=UTC),
                        seen_at=datetime(2020, 2, 1, 15, 41, 4, tzinfo=UTC),
                        sorts_risk_level=80,
                        sorts_priority_factor=70,
                        sorts_risk_level_date=datetime(2021, 3, 30, 5, 0, tzinfo=UTC),
                        sorts_suggestions=[
                            SortsSuggestion(
                                finding_title="033. Password change without identity check",
                                probability=50,
                            )
                        ],
                    ),
                    seen_first_time_by=None,
                ),
                cursor="eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyIsICJzayI6ICJMSU5FUyNST09UIzc2NWIxZDBmLWI2ZmItNDQ4NS1iNGUyLTJjMmNiMTU1NWIxYSNGSUxFTkFNRSNwYXRoL3RvL3Rlc3QyLnB5In0=",
            ),
            ToeLineEdge(
                node=ToeLine(
                    filename="path/to/test3.py",
                    group_name="unittesting",
                    root_id="765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a",
                    state=ToeLineState(
                        attacked_at=datetime(2021, 2, 20, 5, 0, tzinfo=UTC),
                        attacked_by="test2@test.com",
                        attacked_lines=50,
                        be_present=True,
                        be_present_until=None,
                        comments="comment 5",
                        first_attack_at=datetime(2020, 2, 19, 15, 41, 4, tzinfo=UTC),
                        has_vulnerabilities=False,
                        last_author="user@gmail.com",
                        last_commit="e17059d1e17059d1e17059d1e17059d1e17059d1",
                        last_commit_date=datetime(2020, 11, 15, 15, 41, 4, tzinfo=UTC),
                        loc=350,
                        modified_by="test2@test.com",
                        modified_date=datetime(2020, 11, 15, 15, 41, 4, tzinfo=UTC),
                        seen_at=datetime(2020, 2, 1, 15, 41, 4, tzinfo=UTC),
                        sorts_risk_level=100,
                        sorts_priority_factor=90,
                        sorts_risk_level_date=datetime(2021, 3, 30, 5, 0, tzinfo=UTC),
                        sorts_suggestions=[
                            SortsSuggestion(
                                finding_title="027. Insecure file upload", probability=100
                            )
                        ],
                    ),
                    seen_first_time_by=None,
                ),
                cursor="eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyIsICJzayI6ICJMSU5FUyNST09UIzc2NWIxZDBmLWI2ZmItNDQ4NS1iNGUyLTJjMmNiMTU1NWIxYSNGSUxFTkFNRSNwYXRoL3RvL3Rlc3QzLnB5In0=",
            ),
            ToeLineEdge(
                node=ToeLine(
                    filename="path/to/test4.py",
                    group_name="unittesting",
                    root_id="765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a",
                    state=ToeLineState(
                        attacked_at=datetime(2021, 2, 20, 5, 0, tzinfo=UTC),
                        attacked_by="test2@test.com",
                        attacked_lines=10,
                        be_present=True,
                        be_present_until=None,
                        comments="comment 6",
                        first_attack_at=datetime(2020, 2, 19, 15, 41, 4, tzinfo=UTC),
                        has_vulnerabilities=False,
                        last_author="user@gmail.com",
                        last_commit="e17059d1e17059d1e17059d1e17059d1e17059d1",
                        last_commit_date=datetime(2020, 11, 15, 15, 41, 4, tzinfo=UTC),
                        loc=34,
                        modified_by="test2@test.com",
                        modified_date=datetime(2020, 11, 15, 15, 41, 4, tzinfo=UTC),
                        seen_at=datetime(2020, 2, 1, 15, 41, 4, tzinfo=UTC),
                        sorts_risk_level=80,
                        sorts_priority_factor=70,
                        sorts_risk_level_date=datetime(2021, 3, 30, 5, 0, tzinfo=UTC),
                        sorts_suggestions=[
                            SortsSuggestion(
                                finding_title="083. XML injection (XXE)", probability=90
                            ),
                            SortsSuggestion(
                                finding_title="033. Password change without identity check",
                                probability=50,
                            ),
                        ],
                    ),
                    seen_first_time_by=None,
                ),
                cursor="eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyIsICJzayI6ICJMSU5FUyNST09UIzc2NWIxZDBmLWI2ZmItNDQ4NS1iNGUyLTJjMmNiMTU1NWIxYSNGSUxFTkFNRSNwYXRoL3RvL3Rlc3Q0LnB5In0=",
            ),
            ToeLineEdge(
                node=ToeLine(
                    filename="test2/test.sh",
                    group_name="unittesting",
                    root_id="765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a",
                    state=ToeLineState(
                        attacked_at=datetime(2021, 1, 20, 5, 0, tzinfo=UTC),
                        attacked_by="test@test.com",
                        attacked_lines=120,
                        be_present=False,
                        be_present_until=datetime(2021, 1, 1, 15, 41, 4, tzinfo=UTC),
                        comments="comment 2",
                        first_attack_at=datetime(2020, 1, 19, 15, 41, 4, tzinfo=UTC),
                        has_vulnerabilities=False,
                        last_author="user@gmail.com",
                        last_commit="f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c1",
                        last_commit_date=datetime(2020, 11, 16, 15, 41, 4, tzinfo=UTC),
                        loc=172,
                        modified_by="test2@test.com",
                        modified_date=datetime(2020, 11, 16, 15, 41, 4, tzinfo=UTC),
                        seen_at=datetime(2020, 1, 1, 15, 41, 4, tzinfo=UTC),
                        sorts_risk_level=0,
                        sorts_priority_factor=10,
                        sorts_risk_level_date=datetime(2021, 4, 10, 5, 0, tzinfo=UTC),
                        sorts_suggestions=[
                            SortsSuggestion(
                                finding_title="027. Insecure file upload", probability=100
                            )
                        ],
                    ),
                    seen_first_time_by=None,
                ),
                cursor="eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyIsICJzayI6ICJMSU5FUyNST09UIzc2NWIxZDBmLWI2ZmItNDQ4NS1iNGUyLTJjMmNiMTU1NWIxYSNGSUxFTkFNRSN0ZXN0Mi90ZXN0LnNoIn0=",
            ),
            ToeLineEdge(
                node=ToeLine(
                    filename="foo_file.py",
                    group_name="unittesting",
                    root_id="9eccb9ed-e835-4dbc-b28e-8cecd0296e27",
                    state=ToeLineState(
                        attacked_at=datetime(2024, 11, 15, 18, 0, tzinfo=UTC),
                        attacked_by="test2@test.com",
                        attacked_lines=500,
                        be_present=True,
                        be_present_until=None,
                        comments="no comments",
                        first_attack_at=datetime(2020, 2, 19, 15, 41, 4, tzinfo=UTC),
                        has_vulnerabilities=True,
                        last_author="user@gmail.com",
                        last_commit="293abe78145b8f097f1709635527faf9b4ea69b2",
                        last_commit_date=datetime(2024, 11, 14, 18, 0, tzinfo=UTC),
                        loc=500,
                        modified_by="test2@test.com",
                        modified_date=datetime(2024, 11, 16, 18, 0, tzinfo=UTC),
                        seen_at=datetime(2024, 11, 15, 18, 0, tzinfo=UTC),
                        sorts_risk_level=50,
                        sorts_priority_factor=40,
                        sorts_risk_level_date=datetime(2024, 3, 30, 5, 0, tzinfo=UTC),
                        sorts_suggestions=[
                            SortsSuggestion(
                                finding_title="083. XML injection (XXE)", probability=90
                            )
                        ],
                    ),
                    seen_first_time_by=None,
                ),
                cursor="eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyIsICJzayI6ICJMSU5FUyNST09UIzllY2NiOWVkLWU4MzUtNGRiYy1iMjhlLThjZWNkMDI5NmUyNyNGSUxFTkFNRSNmb29fZmlsZS5weSJ9",
            ),
            ToeLineEdge(
                node=ToeLine(
                    filename="foo_file2.py",
                    group_name="unittesting",
                    root_id="9eccb9ed-e835-4dbc-b28e-8cecd0296e27",
                    state=ToeLineState(
                        attacked_at=datetime(2024, 11, 15, 18, 0, tzinfo=UTC),
                        attacked_by="test2@test.com",
                        attacked_lines=500,
                        be_present=True,
                        be_present_until=None,
                        comments="no comments",
                        first_attack_at=datetime(2020, 2, 19, 15, 41, 4, tzinfo=UTC),
                        has_vulnerabilities=True,
                        last_author="user@gmail.com",
                        last_commit="293abe78145b8f097f1709635527faf9b4ea69b2",
                        last_commit_date=datetime(2024, 11, 14, 18, 0, tzinfo=UTC),
                        loc=500,
                        modified_by="test2@test.com",
                        modified_date=datetime(2024, 11, 16, 18, 0, tzinfo=UTC),
                        seen_at=datetime(2024, 11, 15, 18, 0, tzinfo=UTC),
                        sorts_risk_level=50,
                        sorts_priority_factor=40,
                        sorts_risk_level_date=datetime(2024, 3, 30, 5, 0, tzinfo=UTC),
                        sorts_suggestions=[
                            SortsSuggestion(
                                finding_title="083. XML injection (XXE)", probability=90
                            )
                        ],
                    ),
                    seen_first_time_by=None,
                ),
                cursor="eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyIsICJzayI6ICJMSU5FUyNST09UIzllY2NiOWVkLWU4MzUtNGRiYy1iMjhlLThjZWNkMDI5NmUyNyNGSUxFTkFNRSNmb29fZmlsZTIucHkifQ==",
            ),
            ToeLineEdge(
                node=ToeLine(
                    filename="lib/foo_file.py",
                    group_name="unittesting",
                    root_id="9eccb9ed-e835-4dbc-b28e-8cecd0296e27",
                    state=ToeLineState(
                        attacked_at=datetime(2024, 11, 15, 18, 0, tzinfo=UTC),
                        attacked_by="test2@test.com",
                        attacked_lines=500,
                        be_present=True,
                        be_present_until=None,
                        comments="no comments",
                        first_attack_at=datetime(2020, 2, 19, 15, 41, 4, tzinfo=UTC),
                        has_vulnerabilities=True,
                        last_author="user@gmail.com",
                        last_commit="293abe78145b8f097f1709635527faf9b4ea69b2",
                        last_commit_date=datetime(2024, 11, 14, 18, 0, tzinfo=UTC),
                        loc=500,
                        modified_by="test2@test.com",
                        modified_date=datetime(2024, 11, 16, 18, 0, tzinfo=UTC),
                        seen_at=datetime(2024, 11, 15, 18, 0, tzinfo=UTC),
                        sorts_risk_level=50,
                        sorts_priority_factor=40,
                        sorts_risk_level_date=datetime(2024, 3, 30, 5, 0, tzinfo=UTC),
                        sorts_suggestions=[
                            SortsSuggestion(
                                finding_title="083. XML injection (XXE)", probability=90
                            )
                        ],
                    ),
                    seen_first_time_by=None,
                ),
                cursor="eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyIsICJzayI6ICJMSU5FUyNST09UIzllY2NiOWVkLWU4MzUtNGRiYy1iMjhlLThjZWNkMDI5NmUyNyNGSUxFTkFNRSNsaWIvZm9vX2ZpbGUucHkifQ==",
            ),
            ToeLineEdge(
                node=ToeLine(
                    filename="main.py",
                    group_name="unittesting",
                    root_id="9eccb9ed-e835-4dbc-b28e-8cecd0296e27",
                    state=ToeLineState(
                        attacked_at=datetime(2024, 11, 15, 18, 0, tzinfo=UTC),
                        attacked_by="test2@test.com",
                        attacked_lines=500,
                        be_present=True,
                        be_present_until=None,
                        comments="no comments",
                        first_attack_at=datetime(2020, 2, 19, 15, 41, 4, tzinfo=UTC),
                        has_vulnerabilities=True,
                        last_author="user@gmail.com",
                        last_commit="293abe78145b8f097f1709635527faf9b4ea69b2",
                        last_commit_date=datetime(2024, 11, 14, 18, 0, tzinfo=UTC),
                        loc=500,
                        modified_by="test2@test.com",
                        modified_date=datetime(2024, 11, 16, 18, 0, tzinfo=UTC),
                        seen_at=datetime(2024, 11, 15, 18, 0, tzinfo=UTC),
                        sorts_risk_level=50,
                        sorts_priority_factor=40,
                        sorts_risk_level_date=datetime(2024, 3, 30, 5, 0, tzinfo=UTC),
                        sorts_suggestions=[
                            SortsSuggestion(
                                finding_title="083. XML injection (XXE)", probability=90
                            )
                        ],
                    ),
                    seen_first_time_by=None,
                ),
                cursor="eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyIsICJzayI6ICJMSU5FUyNST09UIzllY2NiOWVkLWU4MzUtNGRiYy1iMjhlLThjZWNkMDI5NmUyNyNGSUxFTkFNRSNtYWluLnB5In0=",
            ),
        ),
        page_info=PageInfo(has_next_page=False, end_cursor="bnVsbA=="),
        total=None,
    )


async def test_get_by_root() -> None:
    loaders = get_new_context()
    group_name = "unittesting"
    root_id = "4039d098-ffc5-4984-8ed3-eb17bca98e19"
    root_toe_lines = await loaders.root_toe_lines.load(
        RootToeLinesRequest(group_name=group_name, root_id=root_id)
    )
    assert root_toe_lines == ToeLinesConnection(
        edges=(
            ToeLineEdge(
                node=ToeLine(
                    filename="path/to/test1.py",
                    group_name="unittesting",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    state=ToeLineState(
                        attacked_at=datetime.fromisoformat("2021-02-20T05:00:00+00:00"),
                        attacked_by="test2@test.com",
                        attacked_lines=500,
                        be_present=False,
                        be_present_until=None,
                        comments="comment 3",
                        first_attack_at=datetime.fromisoformat("2020-02-19T15:41:04+00:00"),
                        has_vulnerabilities=False,
                        last_author="user@gmail.com",
                        last_commit="e17059d1e17059d1e17059d1e17059d1e17059d1",
                        last_commit_date=datetime.fromisoformat("2020-11-15T15:41:04+00:00"),
                        loc=500,
                        modified_by="test2@test.com",
                        modified_date=datetime.fromisoformat("2020-11-15T15:41:04+00:00"),
                        seen_at=datetime.fromisoformat("2020-02-01T15:41:04+00:00"),
                        sorts_risk_level=50,
                        sorts_priority_factor=40,
                        sorts_risk_level_date=datetime.fromisoformat("2021-03-30T05:00:00+00:00"),
                        sorts_suggestions=[
                            SortsSuggestion(
                                finding_title="083. XML injection (XXE)",
                                probability=90,
                            )
                        ],
                    ),
                ),
                cursor=(
                    "eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyIsICJzayI6ICJMSU"
                    "5FUyNST09UIzQwMzlkMDk4LWZmYzUtNDk4NC04ZWQzLWViMTdi"
                    "Y2E5OGUxOSNGSUxFTkFNRSNwYXRoL3RvL3Rlc3QxLnB5In0="
                ),
            ),
            ToeLineEdge(
                node=ToeLine(
                    filename="test/test#.config",
                    group_name="unittesting",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    state=ToeLineState(
                        attacked_at=datetime.fromisoformat("2021-02-20T05:00:00+00:00"),
                        attacked_by="test2@test.com",
                        attacked_lines=4,
                        be_present=True,
                        be_present_until=None,
                        comments="comment 1",
                        first_attack_at=datetime.fromisoformat("2020-02-19T15:41:04+00:00"),
                        has_vulnerabilities=True,
                        last_author="user@gmail.com",
                        last_commit="f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c2",
                        last_commit_date=datetime.fromisoformat("2020-11-15T15:41:04+00:00"),
                        loc=180,
                        modified_by="test2@test.com",
                        modified_date=datetime.fromisoformat("2020-11-15T15:41:04+00:00"),
                        seen_at=datetime.fromisoformat("2020-02-01T15:41:04+00:00"),
                        sorts_risk_level=80,
                        sorts_priority_factor=70,
                        sorts_risk_level_date=datetime.fromisoformat("2021-02-20T05:00:00+00:00"),
                        sorts_suggestions=[
                            SortsSuggestion(
                                finding_title="083. XML injection (XXE)",
                                probability=90,
                            ),
                            SortsSuggestion(
                                finding_title="033. Password change without identity check",
                                probability=50,
                            ),
                        ],
                    ),
                ),
                cursor=(
                    "eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyIsICJzayI6ICJMSU"
                    "5FUyNST09UIzQwMzlkMDk4LWZmYzUtNDk4NC04ZWQzLWViMTdi"
                    "Y2E5OGUxOSNGSUxFTkFNRSN0ZXN0L3Rlc3QjLmNvbmZpZyJ9"
                ),
            ),
            ToeLineEdge(
                node=ToeLine(
                    filename="universe/path/to/file3.ext",
                    group_name="unittesting",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    seen_first_time_by=None,
                    state=ToeLineState(
                        attacked_at=datetime.fromisoformat("2021-02-20T05:00:00+00:00"),
                        attacked_by="test2@test.com",
                        attacked_lines=4,
                        be_present=True,
                        be_present_until=None,
                        comments="comment 1",
                        first_attack_at=datetime.fromisoformat("2020-02-19T15:41:04+00:00"),
                        has_vulnerabilities=False,
                        last_author="user@gmail.com",
                        last_commit="e17059d1e17059d1e17059d1e17059d1e17059d1",
                        last_commit_date=datetime.fromisoformat("2020-11-15T15:41:04+00:00"),
                        loc=350,
                        modified_by="test2@test.com",
                        modified_date=datetime.fromisoformat("2020-11-15T15:41:04+00:00"),
                        seen_at=datetime.fromisoformat("2020-02-01T15:41:04+00:00"),
                        sorts_risk_level=80,
                        sorts_priority_factor=70,
                        sorts_risk_level_date=datetime.fromisoformat("2021-03-30T05:00:00+00:00"),
                        sorts_suggestions=[
                            SortsSuggestion(
                                finding_title="083. XML injection (XXE)",
                                probability=90,
                            ),
                            SortsSuggestion(
                                finding_title=("033. Password change without identity check"),
                                probability=50,
                            ),
                        ],
                    ),
                ),
                cursor=(
                    "eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyIsICJzayI6ICJMSU"
                    "5FUyNST09UIzQwMzlkMDk4LWZmYzUtNDk4NC04ZWQzLWViMTdi"
                    "Y2E5OGUxOSNGSUxFTkFNRSN1bml2ZXJzZS9wYXRoL3RvL2ZpbG"
                    "UzLmV4dCJ9"
                ),
            ),
        ),
        page_info=PageInfo(has_next_page=False, end_cursor="bnVsbA=="),
    )
    group_name = "unittesting"
    root_id = "765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a"
    root_toe_lines = await loaders.root_toe_lines.load(
        RootToeLinesRequest(group_name=group_name, root_id=root_id)
    )
    assert root_toe_lines == ToeLinesConnection(
        edges=(
            ToeLineEdge(
                node=ToeLine(
                    filename="path/to/test2.py",
                    group_name="unittesting",
                    root_id="765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a",
                    state=ToeLineState(
                        attacked_at=datetime.fromisoformat("2021-02-20T05:00:00+00:00"),
                        attacked_by="test2@test.com",
                        attacked_lines=0,
                        be_present=True,
                        be_present_until=None,
                        comments="comment 4",
                        first_attack_at=datetime.fromisoformat("2020-02-19T15:41:04+00:00"),
                        has_vulnerabilities=False,
                        last_author="user@gmail.com",
                        last_commit="e17059d1e17059d1e17059d1e17059d1e17059d1",
                        last_commit_date=datetime.fromisoformat("2020-11-15T15:41:04+00:00"),
                        loc=100,
                        modified_by="test2@test.com",
                        modified_date=datetime.fromisoformat("2020-11-15T15:41:04+00:00"),
                        seen_at=datetime.fromisoformat("2020-02-01T15:41:04+00:00"),
                        sorts_risk_level=80,
                        sorts_priority_factor=70,
                        sorts_risk_level_date=datetime.fromisoformat("2021-03-30T05:00:00+00:00"),
                        sorts_suggestions=[
                            SortsSuggestion(
                                finding_title=("033. Password change without identity check"),
                                probability=50,
                            )
                        ],
                    ),
                ),
                cursor=(
                    "eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyIsICJzayI6ICJMSU"
                    "5FUyNST09UIzc2NWIxZDBmLWI2ZmItNDQ4NS1iNGUyLTJjMmNi"
                    "MTU1NWIxYSNGSUxFTkFNRSNwYXRoL3RvL3Rlc3QyLnB5In0="
                ),
            ),
            ToeLineEdge(
                node=ToeLine(
                    filename="path/to/test3.py",
                    group_name="unittesting",
                    root_id="765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a",
                    state=ToeLineState(
                        attacked_at=datetime.fromisoformat("2021-02-20T05:00:00+00:00"),
                        attacked_by="test2@test.com",
                        attacked_lines=50,
                        be_present=True,
                        be_present_until=None,
                        comments="comment 5",
                        first_attack_at=datetime.fromisoformat("2020-02-19T15:41:04+00:00"),
                        has_vulnerabilities=False,
                        last_author="user@gmail.com",
                        last_commit="e17059d1e17059d1e17059d1e17059d1e17059d1",
                        last_commit_date=datetime.fromisoformat("2020-11-15T15:41:04+00:00"),
                        loc=350,
                        modified_by="test2@test.com",
                        modified_date=datetime.fromisoformat("2020-11-15T15:41:04+00:00"),
                        seen_at=datetime.fromisoformat("2020-02-01T15:41:04+00:00"),
                        sorts_risk_level=100,
                        sorts_priority_factor=90,
                        sorts_risk_level_date=datetime.fromisoformat("2021-03-30T05:00:00+00:00"),
                        sorts_suggestions=[
                            SortsSuggestion(
                                finding_title="027. Insecure file upload",
                                probability=100,
                            )
                        ],
                    ),
                ),
                cursor=(
                    "eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyIsICJzayI6ICJMSU"
                    "5FUyNST09UIzc2NWIxZDBmLWI2ZmItNDQ4NS1iNGUyLTJjMmNi"
                    "MTU1NWIxYSNGSUxFTkFNRSNwYXRoL3RvL3Rlc3QzLnB5In0="
                ),
            ),
            ToeLineEdge(
                node=ToeLine(
                    filename="path/to/test4.py",
                    group_name="unittesting",
                    root_id="765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a",
                    state=ToeLineState(
                        attacked_at=datetime.fromisoformat("2021-02-20T05:00:00+00:00"),
                        attacked_by="test2@test.com",
                        attacked_lines=10,
                        be_present=True,
                        be_present_until=None,
                        comments="comment 6",
                        first_attack_at=datetime.fromisoformat("2020-02-19T15:41:04+00:00"),
                        has_vulnerabilities=False,
                        last_author="user@gmail.com",
                        last_commit="e17059d1e17059d1e17059d1e17059d1e17059d1",
                        last_commit_date=datetime.fromisoformat("2020-11-15T15:41:04+00:00"),
                        loc=34,
                        modified_by="test2@test.com",
                        modified_date=datetime.fromisoformat("2020-11-15T15:41:04+00:00"),
                        seen_at=datetime.fromisoformat("2020-02-01T15:41:04+00:00"),
                        sorts_risk_level=80,
                        sorts_priority_factor=70,
                        sorts_risk_level_date=datetime.fromisoformat("2021-03-30T05:00:00+00:00"),
                        sorts_suggestions=[
                            SortsSuggestion(
                                finding_title="083. XML injection (XXE)",
                                probability=90,
                            ),
                            SortsSuggestion(
                                finding_title=("033. Password change without identity check"),
                                probability=50,
                            ),
                        ],
                    ),
                    seen_first_time_by=None,
                ),
                cursor=(
                    "eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyIsICJzayI6ICJMSU"
                    "5FUyNST09UIzc2NWIxZDBmLWI2ZmItNDQ4NS1iNGUyLTJjMmNi"
                    "MTU1NWIxYSNGSUxFTkFNRSNwYXRoL3RvL3Rlc3Q0LnB5In0="
                ),
            ),
            ToeLineEdge(
                node=ToeLine(
                    filename="test2/test.sh",
                    group_name="unittesting",
                    root_id="765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a",
                    state=ToeLineState(
                        attacked_at=datetime.fromisoformat("2021-01-20T05:00:00+00:00"),
                        attacked_by="test@test.com",
                        attacked_lines=120,
                        be_present=False,
                        be_present_until=datetime.fromisoformat("2021-01-01T15:41:04+00:00"),
                        comments="comment 2",
                        first_attack_at=datetime.fromisoformat("2020-01-19T15:41:04+00:00"),
                        has_vulnerabilities=False,
                        last_author="user@gmail.com",
                        last_commit="f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c1",
                        last_commit_date=datetime.fromisoformat("2020-11-16T15:41:04+00:00"),
                        loc=172,
                        modified_by="test2@test.com",
                        modified_date=datetime.fromisoformat("2020-11-16T15:41:04+00:00"),
                        seen_at=datetime.fromisoformat("2020-01-01T15:41:04+00:00"),
                        sorts_risk_level=0,
                        sorts_priority_factor=10,
                        sorts_risk_level_date=datetime.fromisoformat("2021-04-10T05:00:00+00:00"),
                        sorts_suggestions=[
                            SortsSuggestion(
                                finding_title="027. Insecure file upload",
                                probability=100,
                            ),
                        ],
                    ),
                ),
                cursor="eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyIsICJzayI6ICJMSU5FUy"
                "NST09UIzc2NWIxZDBmLWI2ZmItNDQ4NS1iNGUyLTJjMmNiMTU1NWIx"
                "YSNGSUxFTkFNRSN0ZXN0Mi90ZXN0LnNoIn0=",
            ),
        ),
        page_info=PageInfo(has_next_page=False, end_cursor="bnVsbA=="),
    )
