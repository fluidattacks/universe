from datetime import (
    datetime,
)
from decimal import (
    Decimal,
)

import pytest
from freezegun import (
    freeze_time,
)

from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.toe_packages.types import (
    GroupToePackagesRequest,
    RootToePackagesRequest,
    ToePackage,
    ToePackageAdvisory,
    ToePackageCoordinates,
    ToePackageEdge,
    ToePackageHealthMetadata,
    ToePackagesConnection,
)
from integrates.dynamodb.types import (
    PageInfo,
)

# Constants
pytestmark = [
    pytest.mark.asyncio,
]


@freeze_time("2024-01-05T14:38:51.241508+00:00")
async def test_get_by_group() -> None:
    group_name = "unittesting"
    loaders = get_new_context()
    group_toe_packages = await loaders.group_toe_packages.load(
        GroupToePackagesRequest(group_name=group_name)
    )
    assert group_toe_packages == ToePackagesConnection(
        edges=(
            ToePackageEdge(
                node=ToePackage(
                    group_name="unittesting",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    name="com.nimbusds:nimbus-jose-jwt",
                    version="8.3",
                    type_="java-archive",
                    language="java",
                    package_url="pkg:maven/com.nimbusds/nimbus-jose-jwt@8.3",
                    found_by="java-parse-gradle-lock",
                    outdated=True,
                    modified_date=datetime.fromisoformat("2024-01-05T14:38:51.241508+00:00"),
                    be_present=True,
                    id="nb931d6e982c13ff",
                    locations=[
                        ToePackageCoordinates(
                            path="build.gradle",
                            line="85",
                            layer=None,
                            image_ref=None,
                            dependency_type="DIRECT",
                            scope="PROD",
                        ),
                        ToePackageCoordinates(
                            path="/lib/apk/db/installed",
                            line=None,
                            layer="sha256:d4fc045c9e3a848011de66f34b81f0"
                            "52d4f2c15a17bb196d637e526349601820",
                            image_ref="docker.io/mongo:latest",
                        ),
                    ],
                    package_advisories=[
                        ToePackageAdvisory(
                            cpes=[],
                            description="Denial of Service in Connect2id Nimbus JOSE+JWT",
                            id="GHSA-pg98-6v7f-2xfv",
                            namespace="github:language:java",
                            severity="Medium",
                            urls=["https://github.com/advisories/GHSA-pg98-6v7f-2xfv"],
                            version_constraint="<9.37.2",
                            percentile=Decimal("0.54019"),
                            epss=Decimal("0.00189"),
                        )
                    ],
                    licenses=["MIT"],
                    url="https://repo1.maven.org/maven2/com/nimbusds/nimbus-jose-jwt/8.3/",
                    vulnerable=True,
                    vulnerability_ids=None,
                    has_related_vulnerabilities=True,
                    health_metadata=ToePackageHealthMetadata(
                        artifact=None,
                        authors="Vladimir Dzhuvinov <vladimir@dzhuvinov.com>",
                        latest_version_created_at="2024-06-06T14:55:15Z",
                        latest_version="9.40",
                    ),
                    platform="MAVEN",
                ),
                cursor="eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyIsICJzayI6ICJQS0dTI"
                "1JPT1QjNDAzOWQwOTgtZmZjNS00OTg0LThlZDMtZWIxN2JjYTk4ZTE5I1BB"
                "Q0tBR0UjY29tLm5pbWJ1c2RzOm5pbWJ1cy1qb3NlLWp3dCNWRVJTSU9O"
                "IzguMyJ9",
            ),
            ToePackageEdge(
                node=ToePackage(
                    group_name="unittesting",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    name="commons-io:commons-io",
                    version="2.7",
                    type_="java-archive",
                    language="java",
                    package_url="pkg:maven/commons-io/commons-io@2.7",
                    found_by="java-parse-gradle-lock",
                    outdated=True,
                    modified_date=datetime.fromisoformat("2024-01-05T14:38:51.241508+00:00"),
                    be_present=True,
                    id="f75fd9178b7945bf",
                    locations=[
                        ToePackageCoordinates(
                            path="build.gradle",
                            line="88",
                            layer=None,
                            image_ref=None,
                        )
                    ],
                    package_advisories=[],
                    licenses=["MIT"],
                    url="https://repo1.maven.org/maven2/commons-io/commons-io/2.7/",
                    vulnerable=False,
                    vulnerability_ids=None,
                    has_related_vulnerabilities=False,
                    health_metadata=ToePackageHealthMetadata(
                        artifact=None,
                        authors="Scott Sanders <sanders@apache.org>",
                        latest_version_created_at="2024-04-08T16:54:45Z",
                        latest_version="2.16.1",
                    ),
                    platform="MAVEN",
                ),
                cursor="eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyIsICJzayI6ICJQS0dTI"
                "1JPT1QjNDAzOWQwOTgtZmZjNS00OTg0LThlZDMtZWIxN2JjYTk4ZTE5I1B"
                "BQ0tBR0UjY29tbW9ucy1pbzpjb21tb25zLWlvI1ZFUlNJT04jMi43In0=",
            ),
            ToePackageEdge(
                node=ToePackage(
                    group_name="unittesting",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    name="javax.mail:mail",
                    version="1.4",
                    type_="java-archive",
                    language="java",
                    package_url="pkg:maven/javax.mail/mail@1.4",
                    found_by="java-parse-gradle-lock",
                    outdated=False,
                    modified_date=datetime.fromisoformat("2024-01-05T14:38:51.241508+00:00"),
                    be_present=True,
                    id="aa931d6e982c13ff",
                    locations=[
                        ToePackageCoordinates(
                            path="build.gradle",
                            line="85",
                            layer=None,
                            image_ref=None,
                        )
                    ],
                    package_advisories=[],
                    licenses=["MIT"],
                    url="https://repo1.maven.org/maven2/javax/mail/mail/1.4/",
                    vulnerable=False,
                    vulnerability_ids=None,
                    has_related_vulnerabilities=False,
                    health_metadata=ToePackageHealthMetadata(
                        artifact=None,
                        authors="Bill Shannon <bill.shannon@oracle.com>",
                        latest_version_created_at="2013-03-09T01:41:31Z",
                        latest_version="1.4",
                    ),
                    platform="MAVEN",
                ),
                cursor="eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyIsICJzayI6ICJQS0dT"
                "I1JPT1QjNDAzOWQwOTgtZmZjNS00OTg0LThlZDMtZWIxN2JjYTk4ZTE5I1B"
                "BQ0tBR0UjamF2YXgubWFpbDptYWlsI1ZFUlNJT04jMS40In0=",
            ),
            ToePackageEdge(
                node=ToePackage(
                    group_name="unittesting",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    name="org.json:json",
                    version="20190722",
                    type_="java-archive",
                    language="java",
                    package_url="pkg:maven/org.json/json@20190722",
                    found_by="java-parse-gradle-lock",
                    outdated=True,
                    modified_date=datetime.fromisoformat("2024-01-05T14:38:51.241508+00:00"),
                    be_present=True,
                    id="8413009465d8b195",
                    locations=[
                        ToePackageCoordinates(
                            path="build.gradle",
                            line="82",
                            layer=None,
                            image_ref=None,
                        )
                    ],
                    package_advisories=[
                        ToePackageAdvisory(
                            cpes=[],
                            description="json stack overflow vulnerability",
                            id="GHSA-3vqj-43w4-2q58",
                            namespace="github:language:java",
                            severity="High",
                            urls=["https://github.com/advisories/GHSA-3vqj-43w4-2q58"],
                            version_constraint="<20230227",
                            percentile=Decimal("0.74919"),
                            epss=Decimal("0.00237"),
                        ),
                        ToePackageAdvisory(
                            cpes=[],
                            description="Java: DoS Vulnerability in JSON-JAVA",
                            id="GHSA-4jq9-2xhw-jpx7",
                            namespace="github:language:java",
                            severity="High",
                            urls=["https://github.com/advisories/GHSA-4jq9-2xhw-jpx7"],
                            version_constraint="<=20230618",
                            percentile=Decimal("0.94519"),
                            epss=Decimal("0.00164"),
                        ),
                    ],
                    licenses=["MIT"],
                    url="https://repo1.maven.org/maven2/org/json/json/20190722/",
                    vulnerable=True,
                    vulnerability_ids=None,
                    has_related_vulnerabilities=True,
                    health_metadata=ToePackageHealthMetadata(
                        artifact=None,
                        authors="Douglas Crockford <douglas@crockford.com>",
                        latest_version_created_at="2024-03-03T15:19:10Z",
                        latest_version="20240303",
                    ),
                    platform="MAVEN",
                ),
                cursor="eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyIsICJzayI6ICJQS0dT"
                "I1JPT1QjNDAzOWQwOTgtZmZjNS00OTg0LThlZDMtZWIxN2JjYTk4ZTE5I1"
                "BBQ0tBR0Ujb3JnLmpzb246anNvbiNWRVJTSU9OIzIwMTkwNzIyIn0=",
            ),
            ToePackageEdge(
                node=ToePackage(
                    id="84130094455d8b195",
                    be_present=True,
                    group_name="unittesting",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    name="vyper",
                    version="0.3.7",
                    type_="python-wheel",
                    language="python",
                    modified_date=datetime.fromisoformat("2024-01-05T14:38:51.241508+00:00"),
                    locations=[
                        ToePackageCoordinates(
                            path="requirements.txt",
                            line="45",
                            layer=None,
                            image_ref=None,
                        )
                    ],
                    found_by=None,
                    outdated=True,
                    package_advisories=[
                        ToePackageAdvisory(
                            cpes=["cpe:2.3:a:vyperlang:vyper:*:*:*:*:*:*:*:*"],
                            description="Vyper is a smart contract language "
                            "for the Ethereum Virtual Machine. In versions "
                            "0.3.1 to 0.3.7, the Vyper compiler generates "
                            "incorrect bytecode. Any contract using "
                            "'raw_call' with 'revert_on_failure=False' and "
                            "'max_outsize=0' receives an incorrect response "
                            "from 'raw_call'. Depending on memory content, "
                            "the result may be either 'True' or 'False'. "
                            "A patch is available in version 0.3.8. As a "
                            "workaround, set 'max_outsize>0'.",
                            id="CVE-2022-24439",
                            namespace="github:language:python",
                            severity="High",
                            urls=[
                                "https://nvd.nist.gov/vuln/detail/CVE-2022-24439",
                                "https://github.com/vyperlang/vyper/security/"
                                "advisories/GHSA-w9g2-3w7p-72g9",
                            ],
                            version_constraint=">=0.3.1, <0.3.8",
                            percentile=Decimal("0.92345"),
                            epss=Decimal("0.00173"),
                        ),
                        ToePackageAdvisory(
                            cpes=["cpe:2.3:a:vyperlang:vyper:*:*:*:*:*:*:*:*"],
                            description="In Vyper versions 0.3.10 and "
                            "earlier, the bounds check for slices does "
                            "not prevent overflow when 'start + length' is "
                            "not a literal, allowing potential exploitation.",
                            id="CVE-2024-24561",
                            namespace="github:language:python",
                            severity="Medium",
                            urls=["https://nvd.nist.gov/vuln/detail/CVE-2024-24561"],
                            version_constraint="<=0.3.10",
                            percentile=Decimal("0.85712"),
                            epss=Decimal("0.00158"),
                        ),
                        ToePackageAdvisory(
                            cpes=["cpe:2.3:a:vyperlang:vyper:*:*:*:*:*:*:*:*"],
                            description="Arrays in Vyper can be indexed by "
                            "signed integers, though they are defined for "
                            "unsigned integers only, which may cause "
                            "unexpected behavior.",
                            id="CVE-2024-24563",
                            namespace="github:language:python",
                            severity="Medium",
                            urls=["https://nvd.nist.gov/vuln/detail/CVE-2024-24563"],
                            version_constraint="<=0.3.10",
                            percentile=Decimal("0.97689"),
                            epss=Decimal("0.00182"),
                        ),
                        ToePackageAdvisory(
                            cpes=["cpe:2.3:a:vyperlang:vyper:*:*:*:*:*:*:*:*"],
                            description="The Vyper compiler allows passing a "
                            "value in 'raw_call' with 'delegatecall' or "
                            "'staticcall', but silently ignores it, leading "
                            "to potential vulnerabilities.",
                            id="CVE-2024-24567",
                            namespace="github:language:python",
                            severity="Low",
                            urls=["https://nvd.nist.gov/vuln/detail/CVE-2024-24567"],
                            version_constraint="<0.3.8",
                            percentile=Decimal("0.89723"),
                            epss=Decimal("0.00147"),
                        ),
                        ToePackageAdvisory(
                            cpes=[],
                            description="Malicious code in vyper (pypi)",
                            epss=Decimal("0"),
                            id="MAL-2024-8857",
                            namespace="github:language:python",
                            percentile=Decimal("0"),
                            severity="Critical",
                            urls=["https://github.com/advisories/GHSA-2446-89wx-689q"],
                            version_constraint="=0.0.1",
                        ),
                    ],
                    package_url="pkg:pypi/vyper@0.3.7",
                    platform="PIP",
                    licenses=["Apache-2.0"],
                    url="https://pypi.org/project/vyper/0.3.7/",
                    vulnerable=True,
                    has_related_vulnerabilities=True,
                    vulnerability_ids=None,
                    health_metadata=ToePackageHealthMetadata(
                        artifact=None,
                        authors="Vyper Team <vyperlang@ethereum.org>",
                        latest_version_created_at="2024-03-03T15:19:10Z",
                        latest_version="0.3.8",
                    ),
                ),
                cursor="eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyIsICJzayI6ICJQS0dTI1"
                "JPT1QjNDAzOWQwOTgtZmZjNS00OTg0LThlZDMtZWIxN2JjYTk4ZTE5I1BBQ0t"
                "BR0UjdnlwZXIjVkVSU0lPTiMwLjMuNyJ9",
            ),
        ),
        page_info=PageInfo(has_next_page=False, end_cursor="bnVsbA=="),
        total=None,
    )


@freeze_time("2024-01-05T14:38:51.241508+00:00")
async def test_get_by_root() -> None:
    group_name = "unittesting"
    root_id = "4039d098-ffc5-4984-8ed3-eb17bca98e19"
    loaders = get_new_context()
    root_toe_packages = await loaders.root_toe_packages.load(
        RootToePackagesRequest(group_name=group_name, root_id=root_id)
    )

    assert root_toe_packages == ToePackagesConnection(
        edges=(
            ToePackageEdge(
                node=ToePackage(
                    id="nb931d6e982c13ff",
                    be_present=True,
                    group_name="unittesting",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    name="com.nimbusds:nimbus-jose-jwt",
                    version="8.3",
                    type_="java-archive",
                    language="java",
                    modified_date=datetime.fromisoformat("2024-01-05T14:38:51.241508+00:00"),
                    locations=[
                        ToePackageCoordinates(
                            path="build.gradle",
                            line="85",
                            layer=None,
                            image_ref=None,
                            dependency_type="DIRECT",
                            scope="PROD",
                        ),
                        ToePackageCoordinates(
                            path="/lib/apk/db/installed",
                            line=None,
                            layer="sha256:d4fc045c9e3a848011de66f34b81"
                            "f052d4f2c15a17bb196d637e526349601820",
                            image_ref="docker.io/mongo:latest",
                        ),
                    ],
                    found_by="java-parse-gradle-lock",
                    outdated=True,
                    package_advisories=[
                        ToePackageAdvisory(
                            cpes=[],
                            description="Denial of Service in Connect2id Nimbus JOSE+JWT",
                            epss=Decimal("0.00189"),
                            id="GHSA-pg98-6v7f-2xfv",
                            namespace="github:language:java",
                            percentile=Decimal("0.54019"),
                            severity="Medium",
                            urls=["https://github.com/advisories/GHSA-pg98-6v7f-2xfv"],
                            version_constraint="<9.37.2",
                        )
                    ],
                    package_url="pkg:maven/com.nimbusds/nimbus-jose-jwt@8.3",
                    platform="MAVEN",
                    licenses=["MIT"],
                    url="https://repo1.maven.org/maven2/com/nimbusds/nimbus-jose-jwt/8.3/",
                    vulnerable=True,
                    has_related_vulnerabilities=True,
                    vulnerability_ids=None,
                    health_metadata=ToePackageHealthMetadata(
                        artifact=None,
                        authors="Vladimir Dzhuvinov <vladimir@dzhuvinov.com>",
                        latest_version_created_at="2024-06-06T14:55:15Z",
                        latest_version="9.40",
                    ),
                ),
                cursor="eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyIsICJzayI6ICJQS0dTI"
                "1JPT1QjNDAzOWQwOTgtZmZjNS00OTg0LThlZDMtZWIxN2JjYTk4ZTE5I1BBQ"
                "0tBR0UjY29tLm5pbWJ1c2RzOm5pbWJ1cy1qb3NlLWp3dCNWRVJTSU9O"
                "IzguMyJ9",
            ),
            ToePackageEdge(
                node=ToePackage(
                    id="f75fd9178b7945bf",
                    be_present=True,
                    group_name="unittesting",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    name="commons-io:commons-io",
                    version="2.7",
                    type_="java-archive",
                    language="java",
                    modified_date=datetime.fromisoformat("2024-01-05T14:38:51.241508+00:00"),
                    locations=[
                        ToePackageCoordinates(
                            path="build.gradle",
                            line="88",
                            layer=None,
                            image_ref=None,
                        )
                    ],
                    found_by="java-parse-gradle-lock",
                    outdated=True,
                    package_advisories=[],
                    package_url="pkg:maven/commons-io/commons-io@2.7",
                    platform="MAVEN",
                    licenses=["MIT"],
                    url="https://repo1.maven.org/maven2/commons-io/commons-io/2.7/",
                    vulnerable=False,
                    has_related_vulnerabilities=False,
                    vulnerability_ids=None,
                    health_metadata=ToePackageHealthMetadata(
                        artifact=None,
                        authors="Scott Sanders <sanders@apache.org>",
                        latest_version_created_at="2024-04-08T16:54:45Z",
                        latest_version="2.16.1",
                    ),
                ),
                cursor="eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyIsICJzayI6ICJQS0dTI1"
                "JPT1QjNDAzOWQwOTgtZmZjNS00OTg0LThlZDMtZWIxN2JjYTk4ZTE5I1BBQ0"
                "tBR0UjY29tbW9ucy1pbzpjb21tb25zLWlvI1ZFUlNJT04jMi43In0=",
            ),
            ToePackageEdge(
                node=ToePackage(
                    id="aa931d6e982c13ff",
                    be_present=True,
                    group_name="unittesting",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    name="javax.mail:mail",
                    version="1.4",
                    type_="java-archive",
                    language="java",
                    modified_date=datetime.fromisoformat("2024-01-05T14:38:51.241508+00:00"),
                    locations=[
                        ToePackageCoordinates(
                            path="build.gradle",
                            line="85",
                            layer=None,
                            image_ref=None,
                        )
                    ],
                    found_by="java-parse-gradle-lock",
                    outdated=False,
                    package_advisories=[],
                    package_url="pkg:maven/javax.mail/mail@1.4",
                    platform="MAVEN",
                    licenses=["MIT"],
                    url="https://repo1.maven.org/maven2/javax/mail/mail/1.4/",
                    vulnerable=False,
                    has_related_vulnerabilities=False,
                    vulnerability_ids=None,
                    health_metadata=ToePackageHealthMetadata(
                        artifact=None,
                        authors="Bill Shannon <bill.shannon@oracle.com>",
                        latest_version_created_at="2013-03-09T01:41:31Z",
                        latest_version="1.4",
                    ),
                ),
                cursor="eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyIsICJzayI6ICJQS0dTI1"
                "JPT1QjNDAzOWQwOTgtZmZjNS00OTg0LThlZDMtZWIxN2JjYTk4ZTE5I1BBQ0"
                "tBR0UjamF2YXgubWFpbDptYWlsI1ZFUlNJT04jMS40In0=",
            ),
            ToePackageEdge(
                node=ToePackage(
                    id="8413009465d8b195",
                    be_present=True,
                    group_name="unittesting",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    name="org.json:json",
                    version="20190722",
                    type_="java-archive",
                    language="java",
                    modified_date=datetime.fromisoformat("2024-01-05T14:38:51.241508+00:00"),
                    locations=[
                        ToePackageCoordinates(
                            path="build.gradle",
                            line="82",
                            layer=None,
                            image_ref=None,
                        )
                    ],
                    found_by="java-parse-gradle-lock",
                    outdated=True,
                    package_advisories=[
                        ToePackageAdvisory(
                            cpes=[],
                            description="json stack overflow vulnerability",
                            epss=Decimal("0.00237"),
                            id="GHSA-3vqj-43w4-2q58",
                            namespace="github:language:java",
                            percentile=Decimal("0.74919"),
                            severity="High",
                            urls=["https://github.com/advisories/GHSA-3vqj-43w4-2q58"],
                            version_constraint="<20230227",
                        ),
                        ToePackageAdvisory(
                            cpes=[],
                            description="Java: DoS Vulnerability in JSON-JAVA",
                            epss=Decimal("0.00164"),
                            id="GHSA-4jq9-2xhw-jpx7",
                            namespace="github:language:java",
                            percentile=Decimal("0.94519"),
                            severity="High",
                            urls=["https://github.com/advisories/GHSA-4jq9-2xhw-jpx7"],
                            version_constraint="<=20230618",
                        ),
                    ],
                    package_url="pkg:maven/org.json/json@20190722",
                    platform="MAVEN",
                    licenses=["MIT"],
                    url="https://repo1.maven.org/maven2/org/json/json/20190722/",
                    vulnerable=True,
                    has_related_vulnerabilities=True,
                    vulnerability_ids=None,
                    health_metadata=ToePackageHealthMetadata(
                        artifact=None,
                        authors="Douglas Crockford <douglas@crockford.com>",
                        latest_version_created_at="2024-03-03T15:19:10Z",
                        latest_version="20240303",
                    ),
                ),
                cursor="eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyIsICJzayI6ICJQS0dTI"
                "1JPT1QjNDAzOWQwOTgtZmZjNS00OTg0LThlZDMtZWIxN2JjYTk4ZTE5I1BBQ"
                "0tBR0Ujb3JnLmpzb246anNvbiNWRVJTSU9OIzIwMTkwNzIyIn0=",
            ),
            ToePackageEdge(
                node=ToePackage(
                    id="84130094455d8b195",
                    be_present=True,
                    group_name="unittesting",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    name="vyper",
                    version="0.3.7",
                    type_="python-wheel",
                    language="python",
                    modified_date=datetime.fromisoformat("2024-01-05T14:38:51.241508+00:00"),
                    locations=[
                        ToePackageCoordinates(
                            path="requirements.txt",
                            line="45",
                            layer=None,
                            image_ref=None,
                        )
                    ],
                    found_by=None,
                    outdated=True,
                    package_advisories=[
                        ToePackageAdvisory(
                            cpes=["cpe:2.3:a:vyperlang:vyper:*:*:*:*:*:*:*:*"],
                            description="Vyper is a smart contract language "
                            "for the Ethereum Virtual Machine. In versions "
                            "0.3.1 to 0.3.7, the Vyper compiler generates "
                            "incorrect bytecode. Any contract using "
                            "'raw_call' with 'revert_on_failure=False' and "
                            "'max_outsize=0' receives an incorrect response "
                            "from 'raw_call'. Depending on memory content, "
                            "the result may be either 'True' or 'False'. "
                            "A patch is available in version 0.3.8. As a "
                            "workaround, set 'max_outsize>0'.",
                            epss=Decimal("0.00173"),
                            id="CVE-2022-24439",
                            namespace="github:language:python",
                            percentile=Decimal("0.92345"),
                            severity="High",
                            urls=[
                                "https://nvd.nist.gov/vuln/detail/CVE-2022-24439",
                                "https://github.com/vyperlang/vyper/security/"
                                "advisories/GHSA-w9g2-3w7p-72g9",
                            ],
                            version_constraint=">=0.3.1, <0.3.8",
                        ),
                        ToePackageAdvisory(
                            cpes=["cpe:2.3:a:vyperlang:vyper:*:*:*:*:*:*:*:*"],
                            description="In Vyper versions 0.3.10 and "
                            "earlier, the bounds check for slices does "
                            "not prevent overflow when 'start + length' is "
                            "not a literal, allowing potential exploitation.",
                            epss=Decimal("0.00158"),
                            id="CVE-2024-24561",
                            namespace="github:language:python",
                            percentile=Decimal("0.85712"),
                            severity="Medium",
                            urls=["https://nvd.nist.gov/vuln/detail/CVE-2024-24561"],
                            version_constraint="<=0.3.10",
                        ),
                        ToePackageAdvisory(
                            cpes=["cpe:2.3:a:vyperlang:vyper:*:*:*:*:*:*:*:*"],
                            description="Arrays in Vyper can be indexed by "
                            "signed integers, though they are defined for "
                            "unsigned integers only, which may cause "
                            "unexpected behavior.",
                            epss=Decimal("0.00182"),
                            id="CVE-2024-24563",
                            namespace="github:language:python",
                            percentile=Decimal("0.97689"),
                            severity="Medium",
                            urls=["https://nvd.nist.gov/vuln/detail/CVE-2024-24563"],
                            version_constraint="<=0.3.10",
                        ),
                        ToePackageAdvisory(
                            cpes=["cpe:2.3:a:vyperlang:vyper:*:*:*:*:*:*:*:*"],
                            description="The Vyper compiler allows passing a "
                            "value in 'raw_call' with 'delegatecall' or "
                            "'staticcall', but silently ignores it, leading "
                            "to potential vulnerabilities.",
                            epss=Decimal("0.00147"),
                            id="CVE-2024-24567",
                            namespace="github:language:python",
                            percentile=Decimal("0.89723"),
                            severity="Low",
                            urls=["https://nvd.nist.gov/vuln/detail/CVE-2024-24567"],
                            version_constraint="<0.3.8",
                        ),
                        ToePackageAdvisory(
                            cpes=[],
                            description="Malicious code in vyper (pypi)",
                            epss=Decimal("0"),
                            id="MAL-2024-8857",
                            namespace="github:language:python",
                            percentile=Decimal("0"),
                            severity="Critical",
                            urls=["https://github.com/advisories/GHSA-2446-89wx-689q"],
                            version_constraint="=0.0.1",
                        ),
                    ],
                    package_url="pkg:pypi/vyper@0.3.7",
                    platform="PIP",
                    licenses=["Apache-2.0"],
                    url="https://pypi.org/project/vyper/0.3.7/",
                    vulnerable=True,
                    has_related_vulnerabilities=True,
                    vulnerability_ids=None,
                    health_metadata=ToePackageHealthMetadata(
                        artifact=None,
                        authors="Vyper Team <vyperlang@ethereum.org>",
                        latest_version_created_at="2024-03-03T15:19:10Z",
                        latest_version="0.3.8",
                    ),
                ),
                cursor="eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyIsICJzayI6ICJQS0dTI1"
                "JPT1QjNDAzOWQwOTgtZmZjNS00OTg0LThlZDMtZWIxN2JjYTk4ZTE5I1BBQ0t"
                "BR0UjdnlwZXIjVkVSU0lPTiMwLjMuNyJ9",
            ),
        ),
        page_info=PageInfo(has_next_page=False, end_cursor="bnVsbA=="),
        total=None,
    )
