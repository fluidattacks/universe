from datetime import UTC, datetime

import pytest

from integrates.custom_utils.datetime import get_report_dates
from integrates.dataloaders import get_new_context
from integrates.db_model.toe_inputs.types import (
    GroupHistoricToeInputsRequest,
    GroupToeInputsRequest,
    ToeInput,
    ToeInputEdge,
    ToeInputEnrichedState,
    ToeInputsConnection,
    ToeInputState,
)
from integrates.db_model.types import Connection, Edge
from integrates.dynamodb.types import PageInfo

# Constants
pytestmark = [
    pytest.mark.asyncio,
]


async def test_get_by_group() -> None:
    group_name = "unittesting"
    loaders = get_new_context()
    group_toe_inputs = await loaders.group_toe_inputs.load(
        GroupToeInputsRequest(group_name=group_name)
    )
    assert group_toe_inputs == ToeInputsConnection(
        edges=(
            ToeInputEdge(
                node=ToeInput(
                    component="https://gitlab.com/fluidattacks",
                    entry_point="/",
                    environment_id="00fbe3579a253b43239954a545dc0536e6c83098",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    group_name="unittesting",
                    state=ToeInputState(
                        attacked_at=datetime(2021, 2, 9, 5, 0, tzinfo=UTC),
                        attacked_by="test2@test.com",
                        be_present=True,
                        be_present_until=None,
                        first_attack_at=datetime(2021, 2, 9, 5, 0, tzinfo=UTC),
                        has_vulnerabilities=False,
                        modified_by="test2@test.com",
                        modified_date=datetime(2021, 2, 9, 5, 0, tzinfo=UTC),
                        seen_at=datetime(2020, 1, 9, 5, 0, tzinfo=UTC),
                        seen_first_time_by="test2@test.com",
                    ),
                ),
                cursor="eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyIsICJzayI6ICJJTlBVVFMjUk9PVCM0MDM5ZDA5OC1mZmM1LTQ5ODQtOGVkMy1lYjE3YmNhOThlMTkjQ09NUE9ORU5UI2h0dHBzOi8vZ2l0bGFiLmNvbS9mbHVpZGF0dGFja3MvdGVzdDIvdGVzdC5hc3B4I0VOVFJZUE9JTlQjLyJ9",
            ),
            ToeInputEdge(
                node=ToeInput(
                    component="https://test.com/",
                    entry_point="/",
                    environment_id="ba1598f6cf5e8512cd7d776d5dd8a5f79a4fed39",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    group_name="unittesting",
                    state=ToeInputState(
                        attacked_at=datetime(2022, 1, 1, 5, 0, tzinfo=UTC),
                        attacked_by="unittest@fluidattacks.com",
                        be_present=True,
                        be_present_until=None,
                        first_attack_at=datetime(2022, 1, 1, 5, 0, tzinfo=UTC),
                        has_vulnerabilities=False,
                        modified_by="unittest@fluidattacks.com",
                        modified_date=datetime(2022, 1, 1, 5, 0, tzinfo=UTC),
                        seen_at=datetime(2022, 1, 1, 5, 0, tzinfo=UTC),
                        seen_first_time_by="unittest@fluidattacks.com",
                    ),
                ),
                cursor="eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyIsICJzayI6ICJJTlBVVFMjUk9PVCM0MDM5ZDA5OC1mZmM1LTQ5ODQtOGVkMy1lYjE3YmNhOThlMTkjQ09NUE9ORU5UI2h0dHBzOi8vdGVzdC5jb20vI0VOVFJZUE9JTlQjLyJ9",
            ),
            ToeInputEdge(
                node=ToeInput(
                    component="https://test.com/test2/test.aspx",
                    entry_point="/",
                    environment_id="ba1598f6cf5e8512cd7d776d5dd8a5f79a4fed39",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    group_name="unittesting",
                    state=ToeInputState(
                        attacked_at=datetime(2021, 2, 11, 5, 0, tzinfo=UTC),
                        attacked_by="test2@test.com",
                        be_present=True,
                        be_present_until=None,
                        first_attack_at=datetime(2021, 2, 11, 5, 0, tzinfo=UTC),
                        has_vulnerabilities=False,
                        modified_by="test2@test.com",
                        modified_date=datetime(2021, 2, 11, 5, 0, tzinfo=UTC),
                        seen_at=datetime(2020, 1, 11, 5, 0, tzinfo=UTC),
                        seen_first_time_by="test2@test.com",
                    ),
                ),
                cursor="eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyIsICJzayI6ICJJTlBVVFMjUk9PVCM0MDM5ZDA5OC1mZmM1LTQ5ODQtOGVkMy1lYjE3YmNhOThlMTkjQ09NUE9ORU5UI2h0dHBzOi8vdGVzdC5jb20vdGVzdDIvdGVzdC5hc3B4I0VOVFJZUE9JTlQjLyJ9",
            ),
            ToeInputEdge(
                node=ToeInput(
                    component="https://test.com/test2/test.aspx",
                    entry_point="idTest (en-us)",
                    environment_id="ba1598f6cf5e8512cd7d776d5dd8a5f79a4fed39",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    group_name="unittesting",
                    state=ToeInputState(
                        attacked_at=datetime(2021, 2, 11, 5, 0, tzinfo=UTC),
                        attacked_by="test2@test.com",
                        be_present=True,
                        be_present_until=None,
                        first_attack_at=datetime(2021, 2, 11, 5, 0, tzinfo=UTC),
                        has_vulnerabilities=True,
                        modified_by="test2@test.com",
                        modified_date=datetime(2021, 2, 11, 5, 0, tzinfo=UTC),
                        seen_at=datetime(2020, 1, 11, 5, 0, tzinfo=UTC),
                        seen_first_time_by="test2@test.com",
                    ),
                ),
                cursor="eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyIsICJzayI6ICJJTlBVVFMjUk9PVCM0MDM5ZDA5OC1mZmM1LTQ5ODQtOGVkMy1lYjE3YmNhOThlMTkjQ09NUE9ORU5UI2h0dHBzOi8vdGVzdC5jb20vdGVzdDIvdGVzdC5hc3B4I0VOVFJZUE9JTlQjaWRUZXN0IChlbi11cykifQ==",
            ),
            ToeInputEdge(
                node=ToeInput(
                    component="https://test.com/",
                    entry_point="/dashboard",
                    environment_id="9c88cd5a4eda379f6200c3092e6e65b47b18a2a5",
                    root_id="9eccb9ed-e835-4dbc-b28e-8cecd0296e27",
                    group_name="unittesting",
                    state=ToeInputState(
                        attacked_at=datetime(2024, 11, 14, 10, 0, tzinfo=UTC),
                        attacked_by="unittest@fluidattacks.com",
                        be_present=True,
                        be_present_until=None,
                        first_attack_at=datetime(2024, 11, 14, 10, 0, tzinfo=UTC),
                        has_vulnerabilities=False,
                        modified_by="unittest@fluidattacks.com",
                        modified_date=datetime(2024, 11, 14, 10, 0, tzinfo=UTC),
                        seen_at=datetime(2024, 11, 14, 10, 0, tzinfo=UTC),
                        seen_first_time_by="unittest@fluidattacks.com",
                    ),
                ),
                cursor="eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyIsICJzayI6ICJJTlBVVFMjUk9PVCM5ZWNjYjllZC1lODM1LTRkYmMtYjI4ZS04Y2VjZDAyOTZlMjcjQ09NUE9ORU5UI2h0dHBzOi8vdGVzdC5jb20vI0VOVFJZUE9JTlQjL2Rhc2hib2FyZCJ9",
            ),
        ),
        page_info=PageInfo(has_next_page=False, end_cursor="bnVsbA=="),
        total=None,
    )

    group_toe_inputs_be_present = await loaders.group_toe_inputs.load(
        GroupToeInputsRequest(group_name=group_name, be_present=True)
    )
    assert group_toe_inputs_be_present == ToeInputsConnection(
        edges=(
            ToeInputEdge(
                node=ToeInput(
                    component="https://gitlab.com/fluidattacks",
                    entry_point="/",
                    environment_id="00fbe3579a253b43239954a545dc0536e6c83098",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    group_name="unittesting",
                    state=ToeInputState(
                        attacked_at=datetime(2021, 2, 9, 5, 0, tzinfo=UTC),
                        attacked_by="test2@test.com",
                        be_present=True,
                        be_present_until=None,
                        first_attack_at=datetime(2021, 2, 9, 5, 0, tzinfo=UTC),
                        has_vulnerabilities=False,
                        modified_by="test2@test.com",
                        modified_date=datetime(2021, 2, 9, 5, 0, tzinfo=UTC),
                        seen_at=datetime(2020, 1, 9, 5, 0, tzinfo=UTC),
                        seen_first_time_by="test2@test.com",
                    ),
                ),
                cursor="eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyIsICJzayI6ICJJTlBVVFMjUk9PVCM0MDM5ZDA5OC1mZmM1LTQ5ODQtOGVkMy1lYjE3YmNhOThlMTkjQ09NUE9ORU5UI2h0dHBzOi8vZ2l0bGFiLmNvbS9mbHVpZGF0dGFja3MvdGVzdDIvdGVzdC5hc3B4I0VOVFJZUE9JTlQjLyIsICJwa18yIjogIkdST1VQI3VuaXR0ZXN0aW5nIiwgInNrXzIiOiAiSU5QVVRTI1BSRVNFTlQjdHJ1ZSNST09UIzQwMzlkMDk4LWZmYzUtNDk4NC04ZWQzLWViMTdiY2E5OGUxOSNDT01QT05FTlQjaHR0cHM6Ly9naXRsYWIuY29tL2ZsdWlkYXR0YWNrcy90ZXN0Mi90ZXN0LmFzcHgjRU5UUllQT0lOVCMvIn0=",
            ),
            ToeInputEdge(
                node=ToeInput(
                    component="https://test.com/",
                    entry_point="/",
                    environment_id="ba1598f6cf5e8512cd7d776d5dd8a5f79a4fed39",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    group_name="unittesting",
                    state=ToeInputState(
                        attacked_at=datetime(2022, 1, 1, 5, 0, tzinfo=UTC),
                        attacked_by="unittest@fluidattacks.com",
                        be_present=True,
                        be_present_until=None,
                        first_attack_at=datetime(2022, 1, 1, 5, 0, tzinfo=UTC),
                        has_vulnerabilities=False,
                        modified_by="unittest@fluidattacks.com",
                        modified_date=datetime(2022, 1, 1, 5, 0, tzinfo=UTC),
                        seen_at=datetime(2022, 1, 1, 5, 0, tzinfo=UTC),
                        seen_first_time_by="unittest@fluidattacks.com",
                    ),
                ),
                cursor="eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyIsICJzayI6ICJJTlBVVFMjUk9PVCM0MDM5ZDA5OC1mZmM1LTQ5ODQtOGVkMy1lYjE3YmNhOThlMTkjQ09NUE9ORU5UI2h0dHBzOi8vdGVzdC5jb20vI0VOVFJZUE9JTlQjLyIsICJwa18yIjogIkdST1VQI3VuaXR0ZXN0aW5nIiwgInNrXzIiOiAiSU5QVVRTI1BSRVNFTlQjdHJ1ZSNST09UIzQwMzlkMDk4LWZmYzUtNDk4NC04ZWQzLWViMTdiY2E5OGUxOSNDT01QT05FTlQjaHR0cHM6Ly90ZXN0LmNvbS8jRU5UUllQT0lOVCMvIn0=",
            ),
            ToeInputEdge(
                node=ToeInput(
                    component="https://test.com/test2/test.aspx",
                    entry_point="/",
                    environment_id="ba1598f6cf5e8512cd7d776d5dd8a5f79a4fed39",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    group_name="unittesting",
                    state=ToeInputState(
                        attacked_at=datetime(2021, 2, 11, 5, 0, tzinfo=UTC),
                        attacked_by="test2@test.com",
                        be_present=True,
                        be_present_until=None,
                        first_attack_at=datetime(2021, 2, 11, 5, 0, tzinfo=UTC),
                        has_vulnerabilities=False,
                        modified_by="test2@test.com",
                        modified_date=datetime(2021, 2, 11, 5, 0, tzinfo=UTC),
                        seen_at=datetime(2020, 1, 11, 5, 0, tzinfo=UTC),
                        seen_first_time_by="test2@test.com",
                    ),
                ),
                cursor="eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyIsICJzayI6ICJJTlBVVFMjUk9PVCM0MDM5ZDA5OC1mZmM1LTQ5ODQtOGVkMy1lYjE3YmNhOThlMTkjQ09NUE9ORU5UI2h0dHBzOi8vdGVzdC5jb20vdGVzdDIvdGVzdC5hc3B4I0VOVFJZUE9JTlQjLyIsICJwa18yIjogIkdST1VQI3VuaXR0ZXN0aW5nIiwgInNrXzIiOiAiSU5QVVRTI1BSRVNFTlQjdHJ1ZSNST09UIzQwMzlkMDk4LWZmYzUtNDk4NC04ZWQzLWViMTdiY2E5OGUxOSNDT01QT05FTlQjaHR0cHM6Ly90ZXN0LmNvbS90ZXN0Mi90ZXN0LmFzcHgjRU5UUllQT0lOVCMvIn0=",
            ),
            ToeInputEdge(
                node=ToeInput(
                    component="https://test.com/test2/test.aspx",
                    entry_point="idTest (en-us)",
                    environment_id="ba1598f6cf5e8512cd7d776d5dd8a5f79a4fed39",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    group_name="unittesting",
                    state=ToeInputState(
                        attacked_at=datetime(2021, 2, 11, 5, 0, tzinfo=UTC),
                        attacked_by="test2@test.com",
                        be_present=True,
                        be_present_until=None,
                        first_attack_at=datetime(2021, 2, 11, 5, 0, tzinfo=UTC),
                        has_vulnerabilities=True,
                        modified_by="test2@test.com",
                        modified_date=datetime(2021, 2, 11, 5, 0, tzinfo=UTC),
                        seen_at=datetime(2020, 1, 11, 5, 0, tzinfo=UTC),
                        seen_first_time_by="test2@test.com",
                    ),
                ),
                cursor="eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyIsICJzayI6ICJJTlBVVFMjUk9PVCM0MDM5ZDA5OC1mZmM1LTQ5ODQtOGVkMy1lYjE3YmNhOThlMTkjQ09NUE9ORU5UI2h0dHBzOi8vdGVzdC5jb20vdGVzdDIvdGVzdC5hc3B4I0VOVFJZUE9JTlQjaWRUZXN0IChlbi11cykiLCAicGtfMiI6ICJHUk9VUCN1bml0dGVzdGluZyIsICJza18yIjogIklOUFVUUyNQUkVTRU5UI3RydWUjUk9PVCM0MDM5ZDA5OC1mZmM1LTQ5ODQtOGVkMy1lYjE3YmNhOThlMTkjQ09NUE9ORU5UI2h0dHBzOi8vdGVzdC5jb20vdGVzdDIvdGVzdC5hc3B4I0VOVFJZUE9JTlQjaWRUZXN0IChlbi11cykifQ==",
            ),
            ToeInputEdge(
                node=ToeInput(
                    component="https://test.com/",
                    entry_point="/dashboard",
                    environment_id="9c88cd5a4eda379f6200c3092e6e65b47b18a2a5",
                    root_id="9eccb9ed-e835-4dbc-b28e-8cecd0296e27",
                    group_name="unittesting",
                    state=ToeInputState(
                        attacked_at=datetime(2024, 11, 14, 10, 0, tzinfo=UTC),
                        attacked_by="unittest@fluidattacks.com",
                        be_present=True,
                        be_present_until=None,
                        first_attack_at=datetime(2024, 11, 14, 10, 0, tzinfo=UTC),
                        has_vulnerabilities=False,
                        modified_by="unittest@fluidattacks.com",
                        modified_date=datetime(2024, 11, 14, 10, 0, tzinfo=UTC),
                        seen_at=datetime(2024, 11, 14, 10, 0, tzinfo=UTC),
                        seen_first_time_by="unittest@fluidattacks.com",
                    ),
                ),
                cursor="eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyIsICJzayI6ICJJTlBVVFMjUk9PVCM5ZWNjYjllZC1lODM1LTRkYmMtYjI4ZS04Y2VjZDAyOTZlMjcjQ09NUE9ORU5UI2h0dHBzOi8vdGVzdC5jb20vI0VOVFJZUE9JTlQjL2Rhc2hib2FyZCIsICJwa18yIjogIkdST1VQI3VuaXR0ZXN0aW5nIiwgInNrXzIiOiAiSU5QVVRTI1BSRVNFTlQjdHJ1ZSNST09UIzllY2NiOWVkLWU4MzUtNGRiYy1iMjhlLThjZWNkMDI5NmUyNyNDT01QT05FTlQjaHR0cHM6Ly90ZXN0LmNvbS8jRU5UUllQT0lOVCMvZGFzaGJvYXJkIn0=",
            ),
        ),
        page_info=PageInfo(has_next_page=False, end_cursor="bnVsbA=="),
        total=None,
    )


async def test_get_historics_by_group() -> None:
    group_name = "unittesting"
    loaders = get_new_context()
    test_dates = get_report_dates(datetime.fromisoformat("2021-02-12T00:00:00+00:00"))
    historic_toe_inputs = await loaders.group_toe_inputs_enriched_historic_state.load(
        GroupHistoricToeInputsRequest(
            group_name=group_name,
            start_date=test_dates.past_day.start_date,
            end_date=test_dates.today.end_date,
        )
    )
    assert historic_toe_inputs == Connection(
        edges=(
            Edge(
                node=ToeInputEnrichedState(
                    component="https://test.com/test2/test.aspx",
                    entry_point="idTest (en-us)",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    group_name="unittesting",
                    state=ToeInputState(
                        attacked_at=datetime(2021, 2, 11, 5, 0, tzinfo=UTC),
                        attacked_by="test2@test.com",
                        be_present=True,
                        be_present_until=None,
                        first_attack_at=datetime(2021, 2, 11, 5, 0, tzinfo=UTC),
                        has_vulnerabilities=True,
                        modified_by="test2@test.com",
                        modified_date=datetime(2021, 2, 11, 5, 0, tzinfo=UTC),
                        seen_at=datetime(2020, 1, 11, 5, 0, tzinfo=UTC),
                        seen_first_time_by="test2@test.com",
                    ),
                ),
                cursor="eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyNJTlBVVFMjUk9PVCM0MD"
                "M5ZDA5OC1mZmM1LTQ5ODQtOGVkMy1lYjE3YmNhOThlMTkjQ09NUE9ORU5UI2h"
                "0dHBzOi8vdGVzdC5jb20vdGVzdDIvdGVzdC5hc3B4I0VOVFJZUE9JTlQjaWRU"
                "ZXN0IChlbi11cykiLCAic2siOiAiU1RBVEUjc3RhdGUjREFURSMyMDIxLTAyL"
                "TExVDA1OjAwOjAwKzAwOjAwIiwgInBrXzIiOiAiR1JPVVAjSU5QVVRTI1JPT1"
                "QjQ09NUE9ORU5UI0VOVFJZUE9JTlQjdW5pdHRlc3RpbmciLCAic2tfMiI6ICJ"
                "TVEFURSNzdGF0ZSNEQVRFIzIwMjEtMDItMTFUMDU6MDA6MDArMDA6MDAif"
                "Q==",
            ),
            Edge(
                node=ToeInputEnrichedState(
                    component="https://test.com/test2/test.aspx",
                    entry_point="/",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    group_name="unittesting",
                    state=ToeInputState(
                        attacked_at=datetime(2021, 2, 11, 5, 0, tzinfo=UTC),
                        attacked_by="test2@test.com",
                        be_present=True,
                        be_present_until=None,
                        first_attack_at=datetime(2021, 2, 11, 5, 0, tzinfo=UTC),
                        has_vulnerabilities=False,
                        modified_by="test2@test.com",
                        modified_date=datetime(2021, 2, 11, 5, 0, tzinfo=UTC),
                        seen_at=datetime(2020, 1, 11, 5, 0, tzinfo=UTC),
                        seen_first_time_by="test2@test.com",
                    ),
                ),
                cursor="eyJwayI6ICJHUk9VUCN1bml0dGVzdGluZyNJTlBVVFMjUk9PVCM0MD"
                "M5ZDA5OC1mZmM1LTQ5ODQtOGVkMy1lYjE3YmNhOThlMTkjQ09NUE9ORU5UI2h"
                "0dHBzOi8vdGVzdC5jb20vdGVzdDIvdGVzdC5hc3B4I0VOVFJZUE9JTlQjLyIs"
                "ICJzayI6ICJTVEFURSNzdGF0ZSNEQVRFIzIwMjEtMDItMTFUMDU6MDA6MDArM"
                "DA6MDAiLCAicGtfMiI6ICJHUk9VUCNJTlBVVFMjUk9PVCNDT01QT05FTlQjRU"
                "5UUllQT0lOVCN1bml0dGVzdGluZyIsICJza18yIjogIlNUQVRFI3N0YXRlI0R"
                "BVEUjMjAyMS0wMi0xMVQwNTowMDowMCswMDowMCJ9",
            ),
        ),
        page_info=PageInfo(has_next_page=False, end_cursor="bnVsbA=="),
        total=None,
    )
