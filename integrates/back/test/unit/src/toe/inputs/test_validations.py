from datetime import (
    datetime,
)
from integrates.custom_exceptions import (
    ExcludedEnvironment,
    InvalidRootComponent,
    InvalidUrl,
)
from integrates.custom_utils import (
    roots as roots_utils,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.toe_inputs.types import (
    ToeInput,
    ToeInputState,
)
from integrates.toe.inputs.validations import (
    validate_component,
    validate_input_excluded_from_scope,
)
import pytest
from test.unit.src.utils import (
    get_mocked_path,
)
from typing import (
    Any,
    Callable,
)
from unittest.mock import (
    AsyncMock,
    patch,
)

pytestmark = [
    pytest.mark.asyncio,
]


async def test_validate_component() -> None:
    loaders = get_new_context()
    git_root = await roots_utils.get_root(
        loaders, "4039d098-ffc5-4984-8ed3-eb17bca98e19", "unittesting"
    )
    await validate_component(
        loaders=loaders,
        root=git_root,
        component="https://app.fluidattacks.com/test",
        env_id="00fbe3579a253b43239954a545dc0536e6c83094",
    )
    url_root = await roots_utils.get_root(
        loaders, "8493c82f-2860-4902-86fa-75b0fef76034", "oneshottest"
    )
    await validate_component(
        loaders=loaders,
        root=url_root,
        component="https://app.fluidattacks.com:443/test",
        env_id="",
    )
    ip_root = await roots_utils.get_root(
        loaders, "d312f0b9-da49-4d2b-a881-bed438875e99", "oneshottest"
    )
    await validate_component(
        loaders=loaders,
        root=ip_root,
        component="127.0.0.1:8080/test",
        env_id="",
    )
    with pytest.raises(InvalidRootComponent):
        await validate_component(
            loaders=loaders,
            root=git_root,
            component="https://app.invalid.com/test",
            env_id="",
        )
    with pytest.raises(InvalidRootComponent):
        await validate_component(
            loaders=loaders,
            root=url_root,
            component="https://app.fluidattacks.com:440",
            env_id="",
        )
    with pytest.raises(InvalidUrl):
        await validate_component(
            loaders=loaders,
            root=git_root,
            component="://app.invalid.com:66000/test",
            env_id="00fbe3579a253b43239954a545dc0536e6c83094",
        )
    with pytest.raises(InvalidUrl):
        await validate_component(
            loaders=loaders,
            root=url_root,
            component="://app.invalid.com:66000/test",
            env_id="",
        )


@patch(get_mocked_path("loaders.root.load"), new_callable=AsyncMock)
@patch(
    get_mocked_path("loaders.root_environment_urls.load"),
    new_callable=AsyncMock,
)
async def test_validate_input_excluded_from_scope(
    mock_loaders_root_env_urls_load: AsyncMock,
    mock_loaders_root_load: AsyncMock,
    mock_data_for_module: Callable[[str], Any],
) -> None:
    toe_input = ToeInput(
        component="https://test.test/GetList",
        entry_point="input:list:user",
        environment_id="",
        group_name="group1234",
        root_id="ad63c994-cd29-4b31-a21f-60306e2a3e56",
        state=ToeInputState(
            attacked_at=datetime.fromisoformat("2020-01-02T05:00:00+00:00"),
            attacked_by="",
            be_present=True,
            be_present_until=None,
            first_attack_at=datetime.fromisoformat(
                "2020-01-02T05:00:00+00:00"
            ),
            has_vulnerabilities=False,
            modified_by="hacker@fluidattacks.com",
            modified_date=datetime.fromisoformat("2022-02-02T06:00:00+00:00"),
            seen_at=None,
            seen_first_time_by="",
        ),
    )
    try:
        mock_loaders_root_load.return_value = mock_data_for_module("git_root")
        mock_loaders_root_env_urls_load.return_value = mock_data_for_module(
            "root_env_urls"
        )
        await validate_input_excluded_from_scope(get_new_context(), toe_input)
    except ExcludedEnvironment as exc:
        assert False, f"validation with git root raised an exception {exc}"
    try:
        mock_loaders_root_load.return_value = mock_data_for_module("url_root")
        await validate_input_excluded_from_scope(get_new_context(), toe_input)
    except ExcludedEnvironment as exc:
        assert False, f"validation with url root raised an exception {exc}"

    mock_loaders_root_load.return_value = mock_data_for_module("git_root")
    mock_loaders_root_env_urls_load.return_value = mock_data_for_module(
        "root_env_urls_excluded"
    )
    with pytest.raises(ExcludedEnvironment):
        await validate_input_excluded_from_scope(
            get_new_context(),
            toe_input._replace(component="https://test.test/GetList/doc"),
        )
    with pytest.raises(ExcludedEnvironment):
        await validate_input_excluded_from_scope(
            get_new_context(),
            toe_input._replace(component="https://test_2.test/"),
        )

    mock_loaders_root_load.return_value = mock_data_for_module(
        "url_root_excluded"
    )
    with pytest.raises(ExcludedEnvironment):
        await validate_input_excluded_from_scope(
            get_new_context(),
            toe_input._replace(
                component="https://test.test:443/GetError/excluded"
            ),
        )
    with pytest.raises(ExcludedEnvironment):
        await validate_input_excluded_from_scope(
            get_new_context(),
            toe_input._replace(
                component="https://test.test:443/GetError/excluded/sub-path"
            ),
        )


@patch(
    get_mocked_path("loaders.root_environment_urls.load"),
    new_callable=AsyncMock,
)
async def test_validate_component_cloud(
    mock_loaders_root_env_urls_load: AsyncMock,
    mock_data_for_module: Callable[[str], Any],
) -> None:
    loaders = get_new_context()
    git_root = await roots_utils.get_root(
        loaders, "4039d098-ffc5-4984-8ed3-eb17bca98e19", "unittesting"
    )
    try:
        mock_loaders_root_env_urls_load.return_value = mock_data_for_module(
            "root_env_urls"
        )
        await validate_component(
            loaders=loaders,
            root=git_root,
            component="arn:aws:ec2:10210120:ImageId:ami-07c26",
            env_id="a83beb1ed10e7ba4166aa3d6af0835bf57604801",
        )
    except ExcludedEnvironment as exc:
        assert False, f"validation with git root raised an exception {exc}"

    with pytest.raises(InvalidUrl):
        await validate_component(
            loaders=loaders,
            root=git_root,
            component="arn:aws:ec2:10210121:ImageId:ami-07c27",
            env_id="a83beb1ed10e7ba4166aa3d6af0835bf57604801",
        )
