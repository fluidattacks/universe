from collections.abc import Callable
from datetime import (
    datetime,
)
from unittest.mock import (
    AsyncMock,
    MagicMock,
    patch,
)

import pytest
from freezegun import (
    freeze_time,
)
from starlette.responses import (
    RedirectResponse,
)

from integrates.app.views.oauth import (
    AZURE_REDIRECT_URI,
    RepoProvider,
    _begin_repo_oauth,
    _end_repo_oauth,
    _generate_name,
    _get_fast_track_org,
    _get_organization_id,
    _get_params_from_uri,
    _validate,
)
from integrates.custom_exceptions import (
    CredentialAlreadyExists,
)
from test.unit.src.utils import (
    get_module_at_test,
)

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"

pytestmark = [
    pytest.mark.asyncio,
]


@pytest.mark.parametrize(
    "test_case",
    ["fast_track_in_query_params", "fast_track_no_in_query_params"],
)
@patch(MODULE_AT_TEST + "get_authorized_redirect", new_callable=AsyncMock)
@patch(MODULE_AT_TEST + "get_jwt_content", new_callable=AsyncMock)
@patch(MODULE_AT_TEST + "_validate", new_callable=AsyncMock)
async def test_begin_repo_oauth(
    mock_validate: AsyncMock,
    mock_get_jwt_content: AsyncMock,
    mock_get_authorized_redirect: AsyncMock,
    *,
    request_fixture: Callable[[str], MagicMock],
    set_mock: Callable,
    mocked_data_for_module: dict,
    test_case: str,
) -> None:
    mocks_info: list[tuple[str, AsyncMock]] = [
        ("get_jwt_content", mock_get_jwt_content),
        ("get_authorized_redirect", mock_get_authorized_redirect),
        ("_validate", mock_validate),
    ]
    for functionality, mock in mocks_info:
        set_mock(
            mock=mock,
            mocked_functionality_path=functionality,
            mock_key="test_begin_repo_oauth",
            module_at_test=MODULE_AT_TEST,
            mocked_data=mocked_data_for_module,
            side_effect=True,
        )
    request = request_fixture("testing")
    request.query_params["subject"] = "434223ewr49204wkf02"
    if test_case == "fast_track_in_query_params":
        request.query_params["fast_track"] = True

    await _begin_repo_oauth(request, RepoProvider.BITBUCKET)
    mock_get_authorized_redirect.assert_awaited_once_with(request, RepoProvider.BITBUCKET)
    if test_case == "fast_track_no_in_query_params":
        for _, mock in mocks_info:
            mock.assert_awaited_once()


@pytest.mark.parametrize(
    "test_case",
    [
        "no_code_in_query_params",
        "no_subject_in_query_params",
        "error_when_awaiting_get_secret",
    ],
)
@patch(MODULE_AT_TEST + "_get_organization_id", new_callable=AsyncMock)
@patch(
    MODULE_AT_TEST + "get_jwt_content",
    AsyncMock(return_value={"user_email": "testing@fluidattacks.com"}),
)
@patch(MODULE_AT_TEST + "_validate", AsyncMock(side_effect=None))
@patch(MODULE_AT_TEST + "_get_azure_secret", new_callable=AsyncMock)
@patch(
    MODULE_AT_TEST + "validate_credentials_name_in_organization",
    AsyncMock(side_effect=CredentialAlreadyExists()),
)
@patch(
    MODULE_AT_TEST + "validate_credentials_oauth",
    AsyncMock(side_effect=CredentialAlreadyExists()),
)
async def test_end_repo_oauth(
    mock_get_azure_secret: AsyncMock,
    mock_get_organization_id: AsyncMock,
    *,
    request_fixture: Callable[[str], MagicMock],
    set_mock: Callable,
    mocked_data_for_module: dict,
    test_case: str,
) -> None:
    mocks_info: list[tuple[str, AsyncMock]] = [
        ("_get_azure_secret", mock_get_azure_secret),
        ("_get_organization_id", mock_get_organization_id),
    ]
    for functionality, mock in mocks_info:
        set_mock(
            mock=mock,
            mocked_functionality_path=functionality,
            mock_key=test_case,
            module_at_test=MODULE_AT_TEST,
            mocked_data=mocked_data_for_module,
            side_effect=True,
        )
    request: MagicMock = request_fixture("")
    request.configure_mock(query_params={})

    if test_case != "no_code_in_query_params":
        request.query_params["code"] = "mock_code"

    response: RedirectResponse = await _end_repo_oauth(request, RepoProvider.AZURE)
    assert response.status_code == 307
    if test_case != "no_code_in_query_params":
        mock_get_organization_id.assert_awaited_once_with(request, RepoProvider.AZURE)
        if test_case != "no_subject_in_query_params":
            mock_get_azure_secret.assert_awaited_once_with(request)


@pytest.mark.parametrize(
    ["test_case", "provider", "expected_id"],
    [
        ["azure_as_provider", RepoProvider.AZURE, "1404973626"],
        ["fast_track_in_query_params", RepoProvider.AZURE, "1404973626"],
        ["neither_azure_nor_fast_track", RepoProvider.GITHUB, "1404973626"],
    ],
)
@patch(MODULE_AT_TEST + "_get_fast_track_org")
async def test_get_organization_id(
    mock_get_fast_track_org: MagicMock,
    *,
    test_case: str,
    provider: RepoProvider,
    expected_id: str,  # it is the same as in the conftest file
    request_fixture: Callable[[str], MagicMock],
    mocked_data_for_module: dict,
    set_mock: Callable,
) -> None:
    set_mock(
        mock=mock_get_fast_track_org,
        mocked_functionality_path="_get_fast_track_org",
        mock_key="test_get_organization_id",
        module_at_test=MODULE_AT_TEST,
        mocked_data=mocked_data_for_module,
        side_effect=True,
    )
    request = request_fixture("testing")
    request.configure_mock(query_params={})
    if test_case == "azure_as_provider":
        request.session[AZURE_REDIRECT_URI] = f"azure.com/path?subject={expected_id}"
    elif test_case == "fast_track_in_query_params":
        request.query_params["fast_track"] = True
    request.query_params["subject"] = expected_id

    assert await _get_organization_id(request, provider) == expected_id


@patch(MODULE_AT_TEST + "orgs_domain.add_organization", new_callable=AsyncMock)
@patch(MODULE_AT_TEST + "groups_domain.add_group", new_callable=AsyncMock)
@patch(
    MODULE_AT_TEST + "stakeholders_domain.add_enrollment",
    new_callable=AsyncMock,
)
@patch(MODULE_AT_TEST + "get_jwt_content", new_callable=AsyncMock)
@freeze_time("2020-12-31T18:40:37+00:00")
async def test_get_fast_track_org(
    mock_get_jwt_content: AsyncMock,
    mock_add_enrollment: AsyncMock,
    mock_add_group: AsyncMock,
    mock_add_org: AsyncMock,
    *,
    request_fixture: Callable[[str], MagicMock],
    mocked_data_for_module: dict,
    set_mock: Callable,
) -> None:
    mocks_info: list[tuple[str, AsyncMock]] = [
        ("orgs_domain.add_organization", mock_add_org),
        ("groups_domain.add_group", mock_add_group),
        ("stakeholders_domain.add_enrollment", mock_add_enrollment),
        ("get_jwt_content", mock_get_jwt_content),
    ]

    for functionality, mock in mocks_info:
        set_mock(
            mock=mock,
            mocked_functionality_path=functionality,
            mock_key="test_get_fast_track_org",
            module_at_test=MODULE_AT_TEST,
            mocked_data=mocked_data_for_module,
        )
    request: MagicMock = request_fixture("testing")
    request.headers["cf-ipcountry"] = "CO"
    organization = await _get_fast_track_org(request)
    assert organization is not None
    assert organization.name == "unitfluidattacks"
    assert organization.created_by == "unitest@fluidattacks.com"
    assert organization.country == "CO"
    assert organization.created_date == datetime(2020, 12, 31, 18, 40, 37)
    mock_get_jwt_content.assert_awaited_once_with(request)
    mock_add_enrollment.assert_awaited_once()
    mock_add_group.assert_awaited_once()
    mock_add_org.assert_awaited_once()


@pytest.mark.parametrize(
    ["email", "expected_output"],
    [
        ["s@testing.com", "stesting"],
        ["test_email@testing.com", "testetesting"],
        ["test2@testing_long_domain.com", "testtestinglon"],
    ],
)
async def test_generate_name(email: str, expected_output: str) -> None:
    generated_name: str = _generate_name(email)
    assert generated_name == expected_output


@pytest.mark.parametrize(
    "uri",
    ["https://testing.com/funtion?email=test@domain.com&&org=testing"],
)
async def test_get_params_from_uri(uri: str) -> None:
    query_params = _get_params_from_uri(uri)
    assert str(query_params) == "email=test%40domain.com&org=testing"


@pytest.mark.asyncio
async def test_validate_raises_permission_error() -> None:
    loaders_mock = AsyncMock()

    def enforcer(_organization_id_to_test: str, _action: str) -> None:
        return None

    get_organization_level_enforcer_mock = AsyncMock(return_value=enforcer)
    orgs_access_has_access_mock = AsyncMock(return_value=False)

    email = "test@example.com"
    organization_id = "org_123"

    with (
        patch(
            MODULE_AT_TEST + "get_organization_level_enforcer",
            get_organization_level_enforcer_mock,
        ),
        patch(MODULE_AT_TEST + "orgs_access.has_access", orgs_access_has_access_mock),
    ):
        with pytest.raises(PermissionError) as exc_info:
            await _validate(
                loaders=loaders_mock,
                email=email,
                organization_id=organization_id,
            )

        assert str(exc_info.value) == "Access denied"

    get_organization_level_enforcer_mock.assert_awaited_once_with(loaders_mock, email)
    orgs_access_has_access_mock.assert_not_called()
