from ariadne import (
    graphql,
    make_executable_schema,
    MutationType,
    QueryType,
)
from asyncio import (
    sleep,
)
from integrates.api.extension import (
    LowercaseGroupNameExtension,
)
import pytest

# Constants
pytestmark = [
    pytest.mark.asyncio,
]


def resolve_test_not_kwargs(*_: tuple) -> str:
    return "EchoResolver Kwargs!"


def resolve_test_resolve(*_: tuple, group_name: str, name: str) -> str:
    return f"Test {group_name} in {name}!"


async def mutation_echo(
    *_: tuple, group_name: str, organization_name: str
) -> str:
    await sleep(0.1)

    return f"Echo {group_name}!! {organization_name}"


def mutation_echo_second(*_: tuple, non_group_name: str) -> str:
    return f"EchoSecond {non_group_name}!!!"


@pytest.mark.asyncio
async def test_extensions() -> None:
    resolvers = QueryType()
    resolvers.set_field("testResolver", resolve_test_resolve)
    resolvers.set_field("echoResolver", resolve_test_not_kwargs)
    mutation = MutationType()
    mutation.set_field("echoSecond", mutation_echo_second)
    mutation.set_field("echo", mutation_echo)
    schema = make_executable_schema(
        """
            type Query {
                echoResolver: String
                testResolver(groupName: String, name: String): String
            }
            type Mutation {
                echoSecond(nonGroupName: String!): String
                echo(groupName: String!, organizationName: String): String
            }
        """,
        [resolvers, mutation],
        convert_names_case=True,
    )

    result_1 = await graphql(
        schema,
        {"query": 'query { testResolver(groupName: "Test", name: "Test") }'},
        extensions=[LowercaseGroupNameExtension],
    )
    assert "errors" not in result_1[1]
    assert result_1[1]["data"]["testResolver"] == "Test test in Test!"

    result_2 = await graphql(
        schema,
        {"query": "query GetEchoResolve { echoResolver }"},
        extensions=[LowercaseGroupNameExtension],
    )
    assert "errors" not in result_2[1]
    assert result_2[1]["data"]["echoResolver"] == "EchoResolver Kwargs!"

    result_mt_1 = await graphql(
        schema,
        {"query": ' mutation { echoSecond(nonGroupName: "WOrLD") }'},
        extensions=[LowercaseGroupNameExtension],
    )
    assert "errors" not in result_mt_1[1]
    assert result_mt_1[1]["data"]["echoSecond"] == "EchoSecond WOrLD!!!"

    result_mt_2 = await graphql(
        schema,
        {
            "query": """
                mutation { echo(groupName: "World", organizationName: "WOrLD")}
            """
        },
        extensions=[LowercaseGroupNameExtension],
    )
    assert "errors" not in result_mt_2[1]
    assert result_mt_2[1]["data"]["echo"] == "Echo world!! world"
