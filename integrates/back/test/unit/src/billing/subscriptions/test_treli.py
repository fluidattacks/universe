import json
from collections.abc import Awaitable, Callable
from datetime import (
    datetime,
)
from functools import (
    wraps,
)
from typing import (
    Any,
    TypeVar,
    cast,
)
from unittest.mock import (
    AsyncMock,
    MagicMock,
    patch,
)

import pytest
from aiohttp import (
    ClientSession,
)

from integrates.billing.subscriptions.treli import (
    _add_card_token,
    _create_subscription,
    _format_subscription_data_to_post,
    _get_data_to_subscription,
    _update_subscription,
    create_organization_subscription,
    update_subscription_status,
)
from integrates.billing.types import (
    EPaycoCard,
    EPaycoCustomer,
    ProductDict,
    TreliSubscription,
)
from integrates.custom_exceptions import (
    CouldNotCreateSubscription,
)
from integrates.db_model.organizations.enums import (
    OrganizationStateStatus,
)
from integrates.db_model.organizations.types import (
    Organization,
    OrganizationBilling,
    OrganizationState,
)
from integrates.db_model.types import (
    Policies,
)
from test.unit.src.utils import (
    get_module_at_test,
)

from ...decorators_util import (
    assert_called_with,
    patch_aiohttp_clientsession_post,
)

T = TypeVar("T", bound=Callable[..., Awaitable[Any]])

PLAN_ID = "753380"
PAYMENT_METHOD_ID = "122345566789"
ORGANIZATION_NAME = "testorg"

EPAYCO_CARD = EPaycoCard(
    id="1234567890",
    last_four_digits="1234",
    franchise="visa",
    default=True,
    mask="1234",
)

EPAYCO_CUSTOMER = EPaycoCustomer(
    id="1234567890",
    email="jondoe@fluidattacks.com",
    cards=[EPAYCO_CARD],
    created="03/31/2020",
)


ORGANIZATION = Organization(
    created_by="testing@fluid.com",
    created_date=datetime.now(),
    id="1234567890",
    name="testorg",
    billing_information=[
        OrganizationBilling(
            billing_email=EPAYCO_CUSTOMER.email,
            customer_id=EPAYCO_CUSTOMER.id,
            last_modified_by=EPAYCO_CUSTOMER.email,
            modified_at=datetime.now(),
            billed_groups=["group_name"],
            subscription_id=3232321,
        )
    ],
    country="Colombia",
    policies=Policies(
        modified_by="testing@fluid.com",
        modified_date=datetime.now(),
    ),
    state=OrganizationState(
        aws_external_id="1234567890",
        status=OrganizationStateStatus.ACTIVE,
        modified_by="testing@fluid.com",
        modified_date=datetime.now(),
    ),
)

UPDATE_DATA = TreliSubscription.UpdateData(
    subscription_id=399229220,
    payment=TreliSubscription.PaymentObjectDict(
        cardtoken=2, gateway="epaycodirect", payment_method="card"
    ),
)

SUBSCRIPTION_DATA = TreliSubscription.CreateTreliParamsDict(
    email="test@fluidattacks.com",
    billing_address=TreliSubscription.BillingAddressDict(
        first_name="Sons",
        last_name="Company",
        country="CO",
        company="testorg",
        cedula=1111111111,
        address_1="-----",
        city="Medellín",
        state="ANT",
        phone="1234567890",
    ),
    products=[
        ProductDict(
            id=PLAN_ID,
            quantity=1,
            subscription_period="month",
            subscription_period_interval=1,
            subscription_price=0,
        )
    ],
    payment=TreliSubscription.PaymentObjectDict(
        cardtoken=122345566789,
        gateway="epaycodirect",
        payment_method="card",
    ),
    currency="COP",
    meta_data=[
        TreliSubscription.MetadataDict(
            key="billed_groups",
            value="group1, group2",
        )
    ],
)

HEADERS = None

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"

SUBSCRIPTIONS_DATA = {
    "pagination": {"page": 1, "page_size": 2, "has_more": False},
    "results": [
        {
            "id": 731332,
            "total": "$0",
            "total_raw": "0.00",
            "period": "month",
            "interval": "1",
            "fees": "null",
            "coupons": "null",
            "status": "active",
            "items": [
                {
                    "name": "Test Services",
                    "quantity": 1,
                    "total": "0",
                    "id": 753323,
                    "sku": "test",
                }
            ],
            "customer": {
                "first_name": "Jonh",
                "last_name": "[orgtest]",
                "company": "orgtest",
                "address_1": "-----",
                "address_2": "",
                "city": "Medellín",
                "state": "ANT",
                "postcode": "",
                "country": "CO",
                "email": "jdoe@fluidattacks.com",
                "phone": "1234567890",
                "identification": "1111111111",
            },
            "first_payment": "06/28/2024 11:52:56 AM",
            "last_payment": "06/28/2024 12:01:43 PM",
            "next_payment": "07/28/2024 12:01:43 PM",
            "free_trial": False,
            "currency": "COP",
            "payment_gateway": "epaycodirect",
            "last_payment_url": "null",
            "exclude_from_bulk": False,
            "meta_data": [
                {"key": "report_date", "value": "2024-06-28T17:01:45.248800"},
                {"key": "organization", "value": "orgtest"},
                {"key": "number_authors_advanced", "value": 3},
                {"key": "number_authors_essential", "value": 3},
                {"key": "number_groups_essential", "value": 1},
                {"key": "number_groups_advanced", "value": 1},
            ],
            "custom_fields": [],
        },
        {
            "id": 762221,
            "total": "$0",
            "total_raw": "0.00",
            "period": "month",
            "interval": "1",
            "fees": "null",
            "coupons": "null",
            "status": "active",
            "items": [
                {
                    "name": "Test Services",
                    "quantity": 1,
                    "total": "0",
                    "id": 753380,
                    "sku": "test_plan",
                }
            ],
            "customer": {
                "first_name": "Jonh",
                "last_name": "[doe]",
                "company": "testorg2",
                "address_1": "-----",
                "address_2": "",
                "city": "Medellín",
                "state": "ANT",
                "postcode": "",
                "country": "CO",
                "email": "jdoe@fluidattacks.com",
                "phone": "1234567890",
                "identification": "1111111111",
            },
            "first_payment": "06/27/2024 03:00:14 PM",
            "last_payment": "06/27/2024 03:00:16 PM",
            "next_payment": "07/27/2024 03:00:15 PM",
            "free_trial": False,
            "currency": "COP",
            "payment_gateway": "epaycodirect",
            "last_payment_url": "null",
            "exclude_from_bulk": False,
            "meta_data": [
                {"key": "organization", "value": "testorg2"},
                {"key": "number_authors_advanced", "value": "6"},
                {"key": "number_authors_essential", "value": "0"},
                {"key": "number_groups_essential", "value": "0"},
                {"key": "number_groups_advanced", "value": "19"},
                {"key": "report_date", "value": "2024-06-27T20:00:13.655809"},
            ],
            "custom_fields": [],
        },
    ],
}

pytestmark = [
    pytest.mark.asyncio,
]


def patch_secrets(func: T) -> T:
    @wraps(func)
    async def decorated(*args: Any, **kwargs: Any) -> T:
        with (
            patch(
                f"{MODULE_AT_TEST}FI_TRELI_PASSWORD",
                "PASSWORD",
            ),
            patch(
                f"{MODULE_AT_TEST}FI_TRELI_USER_NAME",
                "USERNAME",
            ),
        ):
            return await func(*args, **kwargs)

    return cast(T, decorated)


async def test_get_data_to_subscription() -> None:
    assert (
        await _get_data_to_subscription(
            organization_name=ORGANIZATION.name,
            business_id="1111111111",
            business_name="Sons Company",
            billed_groups=["group1", "group2"],
            token_id=int(PAYMENT_METHOD_ID),
            email="test@fluidattacks.com",
        )
        == SUBSCRIPTION_DATA
    )


async def test_format_subscription_data_to_post() -> None:
    assert (
        await _format_subscription_data_to_post(
            organization_name=ORGANIZATION.name,
            business_id="1111111111",
            business_name="Sons Company",
            billed_groups=["group1", "group2"],
            payment_method_id=int(PAYMENT_METHOD_ID),
            plan_id=PLAN_ID,
            email="test@fluidattacks.com",
        )
        == SUBSCRIPTION_DATA
    )


@patch_secrets
@patch_aiohttp_clientsession_post(
    TreliSubscription.CreateResponse(
        status="ok",
        payment_id=3223,
        payment_gateway="ePayco",
        subscription_ids=[3241],
        payment_status="Aprobado",
        payment_response=TreliSubscription.PaymentResponse(success=True),
    ),
    assert_called_with(
        "/wp-json/api/subscriptions/create",
        headers=HEADERS,
        data=json.dumps(SUBSCRIPTION_DATA),
    ),
)
async def test_create_subscription() -> None:
    async with ClientSession() as mock_client_session:
        assert (
            await _create_subscription(
                mock_client_session,
                data=SUBSCRIPTION_DATA,
            )
            == 3241
        )


@patch_secrets
async def test_create_subscription_error() -> None:
    with patch.object(
        ClientSession,
        "post",
        return_value=AsyncMock(
            __aenter__=AsyncMock(
                return_value=MagicMock(
                    ok=False,
                    text=AsyncMock(return_value="error"),
                )
            ),
        ),
    ) as mock_post_request:
        async with ClientSession() as mock_client_session:
            with pytest.raises(CouldNotCreateSubscription):
                await _create_subscription(
                    mock_client_session,
                    data=SUBSCRIPTION_DATA,
                )

            mock_post_request.assert_called_once_with(
                "/wp-json/api/subscriptions/create",
                headers=HEADERS,
                data=json.dumps(SUBSCRIPTION_DATA),
            )


@patch_aiohttp_clientsession_post({})
@patch(
    f"{MODULE_AT_TEST}_add_card_token",
    return_value=AsyncMock(return_value=12111211),
)
@patch(
    f"{MODULE_AT_TEST}_get_data_to_subscription",
    return_value=AsyncMock(return_value=SUBSCRIPTION_DATA),
)
@patch(
    f"{MODULE_AT_TEST}_create_subscription",
    return_value=AsyncMock(return_value=True),
)
async def test_create_organization_subscription(
    mock_create_subscription: AsyncMock,
    mock_get_data_to_subscription: AsyncMock,
    mock_get_headers: AsyncMock,
) -> None:
    assert await create_organization_subscription(
        organization_name=ORGANIZATION.name,
        business_id="1111111111",
        business_name="Sons Company",
        billed_groups=["group1", "group2"],
        customer=EPAYCO_CUSTOMER,
        email="test@fluidattacks.com",
    )
    mock_get_headers.assert_called_once()
    mock_get_data_to_subscription.assert_called_once()
    mock_create_subscription.assert_called_once()


@patch_secrets
@patch_aiohttp_clientsession_post(
    {"token_id": 1},
    assert_called_with(
        "/wp-json/api/cards/add-token",
        headers=HEADERS,
        data=json.dumps(
            {
                "gateway": "epaycodirect",
                "token_info": {
                    "email": EPAYCO_CUSTOMER.email,
                    "cardTokenId": "1234567890",
                    "customerId": EPAYCO_CUSTOMER.id,
                    "last": EPAYCO_CARD.last_four_digits,
                    "ctype": EPAYCO_CARD.franchise,
                },
            }
        ),
    ),
)
async def test_add_card_token() -> None:
    async with ClientSession() as mock_client_session:
        assert (
            await _add_card_token(
                mock_client_session,
                customer=EPAYCO_CUSTOMER,
                card=EPAYCO_CARD,
            )
            == 1
        )


@patch_aiohttp_clientsession_post(
    {"code": "subs_updated"},
    assert_called_with(
        "/wp-json/api/subscriptions/update",
        headers=HEADERS,
        data=json.dumps(UPDATE_DATA),
    ),
)
@patch_secrets
async def test_update_subscription() -> None:
    async with ClientSession() as mock_client_session:
        assert await _update_subscription(mock_client_session, update_data=UPDATE_DATA)


@patch_secrets
@patch_aiohttp_clientsession_post(
    {"code": "subs_paused"},
    assert_called_with(
        "/wp-json/api/subscriptions/actions",
        headers=HEADERS,
        data=json.dumps(
            {
                "subscription_id": (
                    ORGANIZATION.billing_information[0].subscription_id
                    if ORGANIZATION.billing_information
                    else None
                ),
                "action": "pause",
            }
        ),
    ),
)
async def test_update_subscription_status() -> None:
    assert ORGANIZATION.billing_information
    assert ORGANIZATION.billing_information[0].subscription_id
    assert await update_subscription_status(
        subscription_id=ORGANIZATION.billing_information[0].subscription_id,
        action="pause",
    )
