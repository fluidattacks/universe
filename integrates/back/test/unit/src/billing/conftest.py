import pytest
from stripe import (
    Customer,
    PaymentMethod,
    Subscription as StripeSubscription,
)
from typing import (
    Callable,
    Literal,
    TypeAlias,
)

# pylint:disable=invalid-name
TStatus: TypeAlias = Literal[
    "active",
    "canceled",
    "incomplete",
    "incomplete_expired",
    "past_due",
    "paused",
    "trialing",
    "unpaid",
]

TPlan: TypeAlias = Literal["essential", "advanced"]


@pytest.fixture
def make_stripe_subscription() -> (
    Callable[[TStatus, TPlan], StripeSubscription]
):
    def make(
        status: TStatus = "active", plan: TPlan = "essential"
    ) -> StripeSubscription:
        params = "subscription=sub_1MowQVLkdIwHu7ixeRlqHVzs"

        return StripeSubscription.construct_from(
            {
                "id": f"sub_1MowQVLkdIwHu7ixeRlqHVzs_{status}_{plan}",
                "object": "subscription",
                "application": None,
                "application_fee_percent": None,
                "automatic_tax": {"enabled": False, "liability": None},
                "billing_cycle_anchor": 1679609767,
                "billing_thresholds": None,
                "cancel_at": None,
                "cancel_at_period_end": False,
                "canceled_at": None,
                "cancellation_details": {
                    "comment": None,
                    "feedback": None,
                    "reason": None,
                },
                "collection_method": "charge_automatically",
                "created": 1679609767,
                "currency": "usd",
                "current_period_end": 1682288167,
                "current_period_start": 1679609767,
                "customer": "cus_Na6dX7aXxi11N4",
                "days_until_due": None,
                "default_payment_method": None,
                "default_source": None,
                "default_tax_rates": [],
                "description": None,
                "discount": None,
                "discounts": None,
                "ended_at": None,
                "invoice_settings": {"issuer": {"type": "self"}},
                "items": {
                    "object": "list",
                    "data": [
                        {
                            "id": "si_Na6dzxczY5fwHx",
                            "object": "subscription_item",
                            "billing_thresholds": None,
                            "created": 1679609768,
                            "metadata": {"advanced": None},
                            "plan": {
                                "id": "price_1MowQULkdIwHu7ixraBm864M",
                                "object": "plan",
                                "active": True,
                                "aggregate_usage": None,
                                "amount": 1000,
                                "amount_decimal": "1000",
                                "billing_scheme": "per_unit",
                                "created": 1679609766,
                                "currency": "usd",
                                "discounts": None,
                                "interval": "month",
                                "interval_count": 1,
                                "livemode": False,
                                "metadata": {},
                                "nickname": None,
                                "product": "prod_Na6dGcTsmU0I4R",
                                "tiers_mode": None,
                                "transform_usage": None,
                                "trial_period_days": None,
                                "usage_type": "licensed",
                            },
                            "price": {
                                "id": "price_1MowQULkdIwHu7ixraBm864M",
                                "object": "price",
                                "active": True,
                                "billing_scheme": "per_unit",
                                "created": 1679609766,
                                "currency": "usd",
                                "custom_unit_amount": None,
                                "livemode": False,
                                "lookup_key": None,
                                "metadata": {},
                                "nickname": None,
                                "product": "prod_Na6dGcTsmU0I4R",
                                "recurring": {
                                    "aggregate_usage": None,
                                    "interval": "month",
                                    "interval_count": 1,
                                    "trial_period_days": None,
                                    "usage_type": "licensed",
                                },
                                "tax_behavior": "unspecified",
                                "tiers_mode": None,
                                "transform_quantity": None,
                                "type": "recurring",
                                "unit_amount": 1000,
                                "unit_amount_decimal": "1000",
                            },
                            "quantity": 1,
                            "subscription": "sub_1MowQVLkdIwHu7ixeRlqHVzs",
                            "tax_rates": [],
                        }
                    ],
                    "has_more": False,
                    "total_count": 1,
                    "url": f"/v1/subscription_items?{params}",
                },
                "latest_invoice": "in_1MowQWLkdIwHu7ixuzkSPfKd",
                "livemode": False,
                "metadata": {
                    "subscription": plan,
                    "group": "unittesting",
                    "organization": "test_org",
                },
                "next_pending_invoice_item_invoice": None,
                "on_behalf_of": None,
                "pause_collection": None,
                "payment_settings": {
                    "payment_method_options": None,
                    "payment_method_types": None,
                    "save_default_payment_method": "off",
                },
                "pending_invoice_item_interval": None,
                "pending_setup_intent": None,
                "pending_update": None,
                "schedule": None,
                "start_date": 1679609767,
                "status": status,
                "test_clock": None,
                "transfer_data": None,
                "trial_end": None,
                "trial_settings": {
                    "end_behavior": {
                        "missing_payment_method": "create_invoice"
                    }
                },
                "trial_start": None,
            },
            "test_key",
        )

    return make


@pytest.fixture
def make_stripe_customer() -> Callable[[bool], Customer]:
    def make(has_default_payment_method: bool = True) -> Customer:
        return Customer.construct_from(
            {
                "id": "cus_NffrFeUfNV2Hib",
                "object": "customer",
                "address": None,
                "balance": 0,
                "created": 1680893993,
                "currency": None,
                "default_source": None,
                "delinquent": False,
                "description": None,
                "discount": None,
                "email": "jennyrosen@example.com",
                "invoice_prefix": "0759376C",
                "invoice_settings": {
                    "custom_fields": None,
                    "default_payment_method": "card_id"
                    if has_default_payment_method
                    else None,
                    "footer": None,
                    "rendering_options": None,
                },
                "livemode": False,
                "metadata": {},
                "name": "Jenny Rosen",
                "next_invoice_sequence": 1,
                "phone": None,
                "preferred_locales": [],
                "shipping": None,
                "tax_exempt": "none",
                "test_clock": None,
            },
            "test_key",
        )

    return make


@pytest.fixture
def make_stripe_pyment_method() -> Callable[[bool, str | None], PaymentMethod]:
    def make(
        cvc_check_fails: bool,
        figerprint: str | None,
    ) -> PaymentMethod:
        cvc_check = "fail" if cvc_check_fails else "unchecked"
        return PaymentMethod.construct_from(
            {
                "id": "pm_1MqLiJLkdIwHu7ixUEgbFdYF",
                "object": "payment_method",
                "billing_details": {
                    "address": {
                        "city": None,
                        "country": None,
                        "line1": None,
                        "line2": None,
                        "postal_code": None,
                        "state": None,
                    },
                    "email": None,
                    "name": None,
                    "phone": None,
                },
                "card": {
                    "brand": "visa",
                    "checks": {
                        "address_line1_check": None,
                        "address_postal_code_check": None,
                        "cvc_check": cvc_check,
                    },
                    "country": "US",
                    "exp_month": 8,
                    "exp_year": 2026,
                    "fingerprint": figerprint or "mToisGZ01V71BCos",
                    "funding": "credit",
                    "generated_from": None,
                    "last4": "4242",
                    "networks": {"available": ["visa"], "preferred": None},
                    "three_d_secure_usage": {"supported": True},
                    "wallet": None,
                },
                "created": 1679945299,
                "customer": "cus_9s6XKzkNRiz8i3",
                "livemode": False,
                "metadata": {},
                "type": "card",
            },
            "test_key",
        )

    return make
