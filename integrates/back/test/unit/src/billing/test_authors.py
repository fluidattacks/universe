from datetime import (
    datetime,
)
from unittest.mock import (
    AsyncMock,
    MagicMock,
    patch,
)

import pytest
from botocore.exceptions import (
    ClientError,
)

from integrates.billing.authors import (
    _get_billing_buffer,
    get_group_billing,
    get_organization_authors,
    get_organization_billing,
)
from integrates.billing.types import (
    GroupAuthor,
    OrganizationActiveGroup,
    OrganizationAuthor,
    Price,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.groups.enums import (
    GroupLanguage,
    GroupManaged,
    GroupStateStatus,
    GroupSubscriptionType,
    GroupTier,
)
from integrates.db_model.groups.types import (
    Group,
    GroupState,
)
from integrates.db_model.organizations.enums import (
    OrganizationStateStatus,
)
from integrates.db_model.organizations.types import (
    Organization,
    OrganizationState,
)
from integrates.db_model.types import (
    Policies,
)
from test.unit.src.utils import (
    get_module_at_test,
)

DATALOADERS_CLASS = "integrates.dataloaders.Dataloaders"
ENTITIES_MODULE = "integrates.db_model.organizations.types."
MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"

pytestmark = [
    pytest.mark.asyncio,
]

DATE = datetime.now()

GROUP = Group(
    created_by="jdoe@fluidattacks.com",
    created_date=DATE,
    description="Testing group",
    language=GroupLanguage.EN,
    name="unitttesting",
    organization_id="33688336-8cae-4a8c-a5ee-1855bdd06c",
    state=GroupState(
        has_essential=True,
        has_advanced=True,
        modified_by="jdoe@fluidattacks.com",
        modified_date=DATE,
        status=GroupStateStatus.ACTIVE,
        managed=GroupManaged.MANAGED,
        tier=GroupTier.ADVANCED,
        type=GroupSubscriptionType.CONTINUOUS,
    ),
)


ORGANIZATION = Organization(
    created_by="jdoe@fluidattacks.com",
    created_date=DATE,
    id="33688336-8cae-4a8c-a5ee-1855bdd06c",
    name="testorg",
    policies=Policies(
        modified_date=DATE,
        modified_by="a_policy_modifier",
    ),
    state=OrganizationState(
        aws_external_id="33688336-8cae-4a8c-a5ee-1855bdd06c37",
        status=OrganizationStateStatus.ACTIVE,
        modified_by="some_org_modifier",
        modified_date=datetime(2023, 5, 11),
    ),
    country="Panama",
)

GROUP_AUTHOR = GroupAuthor(
    actor="jdoe@fluidattacks.com",
    groups=frozenset("unittesting"),
    commit="121232433211213342112",
    organization="testorg",
    repository="fluid_testing",
)


@patch(
    MODULE_AT_TEST + "get_organization_authors",
    new_callable=AsyncMock,
    return_value=(
        OrganizationAuthor(
            actor="jdoe@fluidattacks.com",
            active_groups=(
                OrganizationActiveGroup(
                    name="unitttesting",
                    tier=GroupTier.ADVANCED,
                ),
                OrganizationActiveGroup(
                    name="oneshotest",
                    tier=GroupTier.ESSENTIAL,
                ),
            ),
        ),
    ),
)
@patch(
    MODULE_AT_TEST + "get_group_authors",
    new_callable=AsyncMock,
    return_value=(GROUP_AUTHOR,),
)
@patch(
    MODULE_AT_TEST + "billing_domain.get_prices",
    new_callable=AsyncMock,
    return_value={
        "essential": Price(
            id="12122",
            currency="COP",
            amount=1,
        ),
        "advanced": Price(
            id="12123",
            currency="COP",
            amount=1,
        ),
    },
)
async def test_get_group_billing(
    mock_get_prices: AsyncMock,
    mock_get_group_authors: AsyncMock,
    mock_get_organization_authors: AsyncMock,
) -> None:
    loaders: Dataloaders = get_new_context()
    await get_group_billing(date=DATE, group=GROUP, org=ORGANIZATION, loaders=loaders)
    mock_get_group_authors.assert_called_once_with(date=DATE, group=GROUP.name)
    mock_get_organization_authors.assert_called_once_with(
        date=DATE, org=ORGANIZATION, loaders=loaders
    )
    mock_get_prices.assert_awaited_once()


@patch.object(
    Dataloaders,
    "organization_groups",
    MagicMock(load=AsyncMock(return_value=[GROUP])),
)
@patch(
    MODULE_AT_TEST + "get_organization_authors",
    new_callable=AsyncMock,
    return_value=[],
)
@patch(
    MODULE_AT_TEST + "billing_domain.get_prices",
    new_callable=AsyncMock,
    return_value={
        "essential": Price(
            id="12122",
            currency="COP",
            amount=1,
        ),
        "advanced": Price(
            id="12123",
            currency="COP",
            amount=1,
        ),
    },
)
async def test_get_organization_billing(
    mock_billing_domain_get_prices: AsyncMock,
    mock_get_organization_authors: AsyncMock,
) -> None:
    mock_loaders = get_new_context()
    res = await get_organization_billing(date=DATE, org=ORGANIZATION, loaders=mock_loaders)
    assert res.organization == ORGANIZATION.id
    mock_get_organization_authors.assert_called_once_with(
        date=DATE, org=ORGANIZATION, loaders=mock_loaders
    )
    mock_billing_domain_get_prices.assert_awaited_once()


@patch(DATALOADERS_CLASS, new_callable=AsyncMock)
@patch(
    MODULE_AT_TEST + "get_group_authors",
    new_callable=AsyncMock,
    return_value=(GROUP_AUTHOR,),
)
async def test_get_organization_authors(
    mock_get_group_authors: AsyncMock,
    loaders: MagicMock,
) -> None:
    loaders.organization_groups.load.return_value = [GROUP]
    res = await get_organization_authors(date=DATE, org=ORGANIZATION, loaders=loaders)
    assert len(res) == len(mock_get_group_authors.return_value)
    assert res[0].actor == GROUP_AUTHOR.actor
    mock_get_group_authors.assert_called_with(date=DATE, group=GROUP.name)
    loaders.organization_groups.load.assert_called_once_with(ORGANIZATION.id)


@pytest.mark.parametrize(
    "group_name",
    ["test_group_name"],
)
@patch("aiobotocore.client", new_callable=AsyncMock)
async def test_get_billing_buffer_raises_ex(
    mock_client: AsyncMock,
    group_name: str,
) -> None:
    date = datetime.now()
    mock_client.S3.download_fileobj.side_effect = ClientError
    res = await _get_billing_buffer(date=date, group=group_name)
    # Assert empty return if exception is triggered
    assert res.getvalue().decode() == ""
