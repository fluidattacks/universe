from ..utils import (
    get_module_at_test,
)
from datetime import (
    datetime,
)
from integrates.billing.alerts import (
    notify_billing_alert,
)
from integrates.custom_exceptions import (
    OrganizationNotFound,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.organizations.enums import (
    OrganizationStateStatus,
)
from integrates.db_model.organizations.types import (
    Organization,
    OrganizationState,
)
from integrates.db_model.reports.types import (
    Report,
    ReportState,
)
from integrates.db_model.types import (
    Policies,
)
import pytest
from unittest.mock import (
    AsyncMock,
    MagicMock,
    patch,
)
import uuid

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"

pytestmark = [
    pytest.mark.asyncio,
]

report = Report(
    entity_id=uuid.uuid4(),
    origin="billing",
    state=ReportState(
        modified_at=datetime.fromisoformat("2020-01-02T03:04:05Z"),
        modified_by="modified_by@fluidattacks.com",
        status="open",
        status_reason="status_reason",
    ),
    message="message",
)

ORGANIZATION = Organization(
    created_by="created_by@fluidattacks.com",
    created_date=None,
    id="33688336-8cae-4a8c-a5ee-1855bdd06c",
    name="name",
    state=OrganizationState(
        modified_by="modified_by@fluidattacks.com",
        modified_date=datetime.now(),
        status=OrganizationStateStatus.ACTIVE,
        aws_external_id="33688336-8cae-4a8c-a5ee-1855bdd06c37",
    ),
    country="Colombia",
    policies=Policies(
        modified_date=datetime.now(),
        modified_by="modified_by@fluidattacks.com",
    ),
)


@patch.object(
    Dataloaders,
    "organization",
    MagicMock(load=AsyncMock(return_value=ORGANIZATION)),
)
@patch(
    f"{MODULE_AT_TEST}create_ticket_for_billing_anomaly_alert",
    new_callable=AsyncMock,
    return_value=None,
)
async def test_notify_billing_alert(
    mock_create_ticket_for_billing_anomaly_alert: MagicMock,
) -> None:
    with patch(
        MODULE_AT_TEST
        + "organization_access_domain.get_members_email_by_roles",
        return_value=["customer_manager@fluidattacks.com"],
    ):
        await notify_billing_alert(
            report=report,
            requester_email="requester_email@fluidattacks.com",
        )

    mock_create_ticket_for_billing_anomaly_alert.assert_called_once_with(
        report=report,
        organization_name="name",
        requester_email="requester_email@fluidattacks.com",
        customer_manager="customer_manager@fluidattacks.com",
    )


@patch.object(
    Dataloaders,
    "organization",
    MagicMock(load=AsyncMock(return_value=None)),
)
async def test_notify_billing_alert_org_not_found() -> None:
    with pytest.raises(OrganizationNotFound):
        await notify_billing_alert(
            report=report,
            requester_email="requester_email@fluidattacks.com",
        )
