import json
from datetime import (
    datetime,
)
from unittest.mock import (
    AsyncMock,
    patch,
)

import pytest
from aiohttp import (
    ClientSession,
)

from integrates.billing.epayco_customers import (
    _associate_token_with_customer,
    _get_customer_epayco,
    _get_headers,
    associate_card_with_customer,
    create_customer,
    find_card_by_id,
    format_cards_info,
    list_customer_credit_cards,
    remove_card_token,
    tokenize_customer,
    use_credit_card_as_default,
)
from integrates.billing.types import (
    EPaycoCard,
    EPaycoCustomer,
    PaymentMethod,
)
from integrates.custom_exceptions import (
    CouldNotCreatePaymentMethod,
    CouldNotRemovePaymentMethod,
    InvalidAuthorization,
    InvalidBillingCustomer,
)
from integrates.db_model.organizations.enums import (
    OrganizationStateStatus,
)
from integrates.db_model.organizations.types import (
    Organization,
    OrganizationBilling,
    OrganizationState,
)
from integrates.db_model.types import (
    Policies,
)

from ..decorators_util import (
    assert_called_with,
    patch_aiohttp_clientsession_post,
)
from ..utils import (
    get_module_at_test,
)

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"

pytestmark = [
    pytest.mark.asyncio,
]


EPAICO_CARDS = [
    EPaycoCard(
        last_four_digits="0327",
        franchise="visa",
        id="FqctY4",
        default=False,
        mask="122321******0327",
    ),
    EPaycoCard(
        last_four_digits="0322",
        franchise="visa",
        id="Xwk5pF",
        default=False,
        mask="123456******0322",
    ),
]

EPAYCO_CUSTOMER = EPaycoCustomer(
    id="cDrjtT6J3NXgkorWB",
    email="jondoe@gmail.com",
    cards=EPAICO_CARDS,
    created="03/31/2020",
)

ORGANIZATION_WITH_BILLING_CUSTOMER = Organization(
    created_date=datetime.now(),
    created_by="created_by",
    id="organization_id",
    name="organization_name",
    billing_information=[
        OrganizationBilling(
            billing_email="jdoe@org.com",
            customer_id="cDrjtT6J3NXgkorWB",
            last_modified_by="jdoe@org.com",
            modified_at=datetime.now(),
            billed_groups=["group_name"],
        )
    ],
    country="Colombia",
    state=OrganizationState(
        aws_external_id="aws_external_id",
        status=OrganizationStateStatus.ACTIVE,
        modified_by="modified_by",
        modified_date=datetime.now(),
    ),
    policies=Policies(
        modified_by="modified_by",
        modified_date=datetime.now(),
    ),
)

ORGANIZATION_WITHOUT_BILLING_CUSTOMER = Organization(
    created_date=datetime.now(),
    created_by="created_by",
    id="organization_id",
    name="organization_name",
    country="Colombia",
    state=OrganizationState(
        aws_external_id="aws_external_id",
        status=OrganizationStateStatus.ACTIVE,
        modified_by="modified_by",
        modified_date=datetime.now(),
    ),
    policies=Policies(
        modified_by="modified_by",
        modified_date=datetime.now(),
    ),
)

HEADERS = {
    "Accept": "application/json",
    "Content-Type": "application/json",
    "Authorization": "Bearer token",
}


@patch_aiohttp_clientsession_post({"token": "token"})
async def test_get_headers() -> None:
    async with ClientSession("https://test") as session:
        result = await _get_headers()(session)
        assert result == {
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": "Bearer token",
        }


@patch_aiohttp_clientsession_post({"error": "token"})
async def test_get_headers_error() -> None:
    async with ClientSession("https://test") as session:
        with pytest.raises(InvalidAuthorization):
            await _get_headers()(session)


@patch_aiohttp_clientsession_post(
    {"error": "error"},
    assert_called_with(
        "/token/customer",
        headers=None,
        data=json.dumps(
            {
                "docType": "Nit",
                "docNumber": "000000000",
                "cardTokenId": "card_token",
                "email": "test@fluidattacks.com",
                "lastName": "-",
                "name": "Sons Company",
                "cellPhone": "0000000000",
                "phone": "0000000000",
                "requireCardToken": True,
            }
        ),
    ),
)
async def test_tokenize_customer_error() -> None:
    async with ClientSession("https://test") as session:
        with pytest.raises(CouldNotCreatePaymentMethod):
            await tokenize_customer(
                session,
                card_token_id="card_token",
                business_name="Sons Company",
                business_id="000000000",
                user_email="test@fluidattacks.com",
            )


@patch_aiohttp_clientsession_post(
    {
        "success": True,
        "token": "token",
        "titleResponse": "Success token generate",
        "textResponse": "Success token generate",
        "lastAction": "token_customer",
        "data": {
            "status": True,
            "success": True,
            "type": "Create customer",
            "data": {
                "status": "exitoso",
                "description": "El cliente fue creado exitosamente",
                "customerId": "DiYPdhgncppes9KAb",
                "name": "jon",
                "email": "jondoe@hotmail.com",
            },
            "object": "customer",
        },
    },
    assert_called_with(
        "/token/customer",
        headers=None,
        data=json.dumps(
            {
                "docType": "Nit",
                "docNumber": "000000000",
                "cardTokenId": "card_token",
                "email": "test@fluidattacks.com",
                "lastName": "-",
                "name": "Sons Company",
                "cellPhone": "0000000000",
                "phone": "0000000000",
                "requireCardToken": True,
            }
        ),
    ),
)
async def test_tokenize_customer() -> None:
    async with ClientSession("https://test") as session:
        assert (
            await tokenize_customer(
                session,
                card_token_id="card_token",
                business_name="Sons Company",
                business_id="000000000",
                user_email="test@fluidattacks.com",
            )
            == "DiYPdhgncppes9KAb"
        )


@patch_aiohttp_clientsession_post(
    {
        "success": True,
        "token": "token",
        "titleResponse": "Customer successfully recovered",
        "textResponse": "Customer successfully recovered",
        "lastAction": "get_customer",
        "data": {
            "status": True,
            "success": True,
            "type": "Find customer",
            "data": {
                "id_customer": "cDrjtT6J3NXgkorWB",
                "name": "Jon Doe",
                "email": "jondoe@gmail.com",
                "doc_type": "NIT",
                "doc_number": "1035863428-3",
                "created": "03/31/2020",
                "cards": [
                    {
                        "token": "FqctY4",
                        "franchise": "visa",
                        "mask": "122321******0327",
                        "created": "11/29/2019",
                        "default": False,
                    },
                    {
                        "token": "Xwk5pF",
                        "franchise": "visa",
                        "mask": "123456******0322",
                        "created": "11/29/2019",
                        "default": False,
                    },
                ],
            },
            "object": "customer",
        },
    }
)
async def test_get_customer_epayco() -> None:
    async with ClientSession("https://test") as session:
        assert await _get_customer_epayco(session, customer_id="customerId") == EPAYCO_CUSTOMER


@patch_aiohttp_clientsession_post({"error": "error", "textResponse": "error"})
async def test_get_customer_epayco_error() -> None:
    async with ClientSession("https://test") as session:
        with pytest.raises(InvalidBillingCustomer):
            await _get_customer_epayco(session, customer_id="customerId")


@patch_aiohttp_clientsession_post({"success": True})
@patch(
    MODULE_AT_TEST + "_get_customer_epayco",
    return_value=EPAYCO_CUSTOMER,
)
async def test_default_credit_card(
    mock_get_customer_epayco: AsyncMock,
) -> None:
    async with ClientSession("https://test") as session:
        assert await use_credit_card_as_default(
            session,
            card_token_id="FqctY4",
            customer_id="cDrjtT6J3NXgkorWB",
        )

    mock_get_customer_epayco.assert_called_once()


def test_format_cards_info() -> None:
    assert format_cards_info(
        cards=[
            {
                "token": "FqctY4",
                "franchise": "visa",
                "mask": "122321******0327",
                "created": "11/29/2019",
                "default": False,
            },
            {
                "token": "Xwk5pF",
                "franchise": "visa",
                "mask": "123456******0322",
                "created": "11/29/2019",
                "default": False,
            },
        ],
    ) == list(EPAICO_CARDS)


def test_find_card_by_id() -> None:
    assert find_card_by_id(
        "FqctY4",
        EPAICO_CARDS,
    ) == EPaycoCard(
        last_four_digits="0327",
        franchise="visa",
        id="FqctY4",
        default=False,
        mask="122321******0327",
    )


@patch(
    MODULE_AT_TEST + "tokenize_customer",
    return_value="customerId",
)
@patch(
    MODULE_AT_TEST + "_get_customer_epayco",
    return_value=EPAYCO_CUSTOMER,
)
async def test_create_billing_customer_and_associate_card(
    mock_get_customer_epayco: AsyncMock,
    mock_tokenize_customer: AsyncMock,
) -> None:
    await create_customer(
        business_id="business_id",
        business_name="business_name",
        payment_method_id="payment_method_id",
        user_email="test@fluidattacks.com",
    )
    mock_tokenize_customer.assert_called_once()
    mock_get_customer_epayco.assert_called_once()


@patch_aiohttp_clientsession_post(
    {"success": True},
    assert_called_with(
        "/subscriptions/customer/add/new/token",
        headers=None,
        data=json.dumps({"cardToken": "432456649506930327", "customerId": "customerId"}),
    ),
)
async def test_associate_token_with_customer() -> None:
    async with ClientSession("https://test") as session:
        assert await _associate_token_with_customer(
            session,
            card_token_id="432456649506930327",
            customer_id="customerId",
        )


@patch_aiohttp_clientsession_post({"success": True})
@patch(
    MODULE_AT_TEST + "_associate_token_with_customer",
    new_callable=AsyncMock,
    return_value=True,
)
@patch(
    MODULE_AT_TEST + "_get_customer_epayco",
    new_callable=AsyncMock,
    return_value=EPAYCO_CUSTOMER,
)
@patch(
    MODULE_AT_TEST + "treli.add_card_token_to_customer",
    new_callable=AsyncMock,
    return_value=1,
)
async def test_associate_card_with_customer_customer_alredy_exist(
    mock_treli_add_card_token_to_customer: AsyncMock,
    mock_get_customer_epayco: AsyncMock,
    mock_associate_token_with_customer: AsyncMock,
) -> None:
    card = await associate_card_with_customer(
        payment_method_id="FqctY4",
        organization=ORGANIZATION_WITH_BILLING_CUSTOMER,
        make_default=False,
        billing_email=EPAYCO_CUSTOMER.email,
    )

    assert card == EPAICO_CARDS[0]
    mock_associate_token_with_customer.assert_awaited_once()
    mock_treli_add_card_token_to_customer.assert_awaited_once()
    mock_get_customer_epayco.assert_awaited()


@patch_aiohttp_clientsession_post({"success": True})
@patch(
    MODULE_AT_TEST + "_get_customer_epayco",
    return_value=EPAYCO_CUSTOMER,
)
@patch(
    MODULE_AT_TEST + "_remove_card_from_customer",
    return_value=EPAYCO_CUSTOMER,
)
async def test_remove_card_token(
    mock_remove_card_from_customer: AsyncMock,
    mock_get_customer_epayco: AsyncMock,
) -> None:
    await remove_card_token(
        organization=ORGANIZATION_WITH_BILLING_CUSTOMER,
        card_token_id="FqctY4",
    )
    mock_remove_card_from_customer.assert_called_once()
    mock_get_customer_epayco.assert_called_once()


@patch_aiohttp_clientsession_post({"error": "error", "textResponse": "error"})
@patch(
    MODULE_AT_TEST + "_get_customer_epayco",
    return_value=EPAYCO_CUSTOMER,
)
async def test_remove_card_token_error(
    mock_get_customer_epayco: AsyncMock,
) -> None:
    with pytest.raises(CouldNotRemovePaymentMethod):
        await remove_card_token(
            organization=ORGANIZATION_WITH_BILLING_CUSTOMER,
            card_token_id="FqctY4",
        )

    mock_get_customer_epayco.assert_called_once()


@patch(
    MODULE_AT_TEST + "_get_customer_epayco",
    return_value=EPAYCO_CUSTOMER,
)
async def test_list_customer_credit_cards(
    mock_get_customer_epayco: AsyncMock,
) -> None:
    assert await list_customer_credit_cards(
        customer_id="customerId",
    ) == [
        PaymentMethod(
            id=card.id,
            fingerprint=card.mask,
            last_four_digits=card.last_four_digits,
            expiration_month="",
            expiration_year="",
            brand=card.franchise,
            default=card.default,
            business_name="",
            city="",
            country="Colombia",
            email="",
            state="",
            rut=None,
            tax_id=None,
        )
        for card in EPAICO_CARDS
    ]

    mock_get_customer_epayco.assert_called_once()
