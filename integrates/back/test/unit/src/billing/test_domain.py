# pylint: disable=too-many-lines

import os
from collections.abc import Callable
from datetime import (
    datetime,
)
from decimal import (
    Decimal,
)
from io import (
    BytesIO,
)
from typing import (
    Any,
)
from unittest.mock import (
    AsyncMock,
    MagicMock,
    patch,
)

import aiofiles
import pytest
from starlette.datastructures import (
    Headers,
    UploadFile,
)
from stripe import (
    Customer,
)
from stripe import (
    PaymentMethod as StripePaymentMethod,
)

from integrates.billing.domain import (
    _add_billing_information,
    _list_credit_cards_info,
    create_credit_card_payment_method,
    create_other_payment_method,
    get_document_link,
    get_prices,
    list_organization_payment_methods,
    remove_payment_method,
)
from integrates.billing.types import (
    EPaycoCard,
    EPaycoCustomer,
    PaymentMethod,
    Price,
)
from integrates.custom_exceptions import (
    InvalidBillingPaymentMethod,
)
from integrates.db_model.groups.enums import (
    GroupLanguage,
    GroupManaged,
    GroupStateStatus,
    GroupSubscriptionType,
    GroupTier,
)
from integrates.db_model.groups.types import (
    Group,
    GroupState,
)
from integrates.db_model.organizations.enums import (
    OrganizationStateStatus,
)
from integrates.db_model.organizations.types import (
    Organization,
    OrganizationBilling,
    OrganizationDocuments,
    OrganizationPaymentMethods,
    OrganizationState,
)
from integrates.db_model.types import (
    Policies,
)
from integrates.resources.domain import (
    save_file,
)
from test.unit.src.utils import (
    get_module_at_test,
    set_mocks_return_values,
)

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"

pytestmark = [
    pytest.mark.asyncio,
]


EPAICO_CARDS = [
    EPaycoCard(
        last_four_digits="0327",
        franchise="visa",
        id="FqctY4",
        default=False,
        mask="122321******0327",
    ),
    EPaycoCard(
        last_four_digits="0322",
        franchise="visa",
        id="Xwk5pF",
        default=False,
        mask="123456******0322",
    ),
]

EPAYCO_CUSTOMER = EPaycoCustomer(
    id="cDrjtT6J3NXgkorWB",
    email="jondoe@gmail.com",
    cards=EPAICO_CARDS,
    created="03/31/2020",
)

PAYMENT_METHODS = [
    OrganizationPaymentMethods(
        id="38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
        business_name="Fluid",
        email="test@fluidattacks.com",
        country="Colombia",
        state="Antioquia",
        city="Medellín",
        documents=OrganizationDocuments(rut=None, tax_id=None),
    ),
    OrganizationPaymentMethods(
        id="4722b0b7-cfeb-4898-8308-185dfc2523bc",
        business_name="Testing Company and Sons",
        email="test@fluidattacks.com",
        country="Colombia",
        state="Antioquia",
        city="Medellín",
        documents=OrganizationDocuments(rut=None, tax_id=None),
    ),
]

ORGANIZATION = Organization(
    created_by="unknown@unknown.com",
    created_date=datetime.fromisoformat("2018-02-08T00:43:18+00:00"),
    id="ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
    name="okada",
    policies=Policies(
        modified_date=datetime.fromisoformat("2019-11-22T20:07:57+00:00"),
        modified_by="integratesmanager@gmail.com",
        inactivity_period=90,
        max_acceptance_days=60,
        max_acceptance_severity=Decimal("10.0"),
        max_number_acceptances=2,
        min_acceptance_severity=Decimal("0.0"),
        min_breaking_severity=Decimal("0"),
        vulnerability_grace_period=0,
    ),
    state=OrganizationState(
        aws_external_id=("d1042a09-582f-4fbb-b3ac-956478f053ad"),
        status=OrganizationStateStatus.ACTIVE,
        modified_by="unknown",
        modified_date=datetime.fromisoformat("2018-02-08T00:43:18+00:00"),
        pending_deletion_date=datetime.fromisoformat("2019-11-22T20:07:57+00:00"),
    ),
    country="Panama",
    payment_methods=PAYMENT_METHODS,
    billing_information=[
        OrganizationBilling(
            billing_email="billing@fluidatatcks.com",
            customer_id="21kelwko123p20303",
            last_modified_by="jdoe@org.com",
            modified_at=datetime.fromisoformat("2018-02-08T00:43:18+00:00"),
            billed_groups=["group1"],
        )
    ],
)

ORG_GROUP1 = Group(
    created_by="test@fluidattacks.com",
    created_date=datetime.fromisoformat("2018-02-08T00:43:18+00:00"),
    description="Test group",
    language=GroupLanguage.EN,
    organization_id=ORGANIZATION.id,
    name="group1",
    business_id="223.324.443-1",
    business_name="Fluid",
    state=GroupState(
        status=GroupStateStatus.ACTIVE,
        modified_by="",
        modified_date=datetime.fromisoformat("2018-02-08T00:43:18+00:00"),
        has_advanced=True,
        has_essential=True,
        managed=GroupManaged.MANAGED,
        tier=GroupTier.ESSENTIAL,
        type=GroupSubscriptionType.CONTINUOUS,
    ),
)


@patch(MODULE_AT_TEST + "stripe_customers.get_prices", new_callable=AsyncMock)
async def test_get_prices(mock_stripe_customers_get_prices: AsyncMock) -> None:
    price = Price(id="2334", currency="us", amount=100)
    mock_stripe_customers_get_prices.return_value = {"item": price}

    result = await get_prices()

    assert result == mock_stripe_customers_get_prices.return_value


@pytest.mark.parametrize(
    ("expected_result"),
    [
        (
            PaymentMethod(
                id="38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
                fingerprint="",
                last_four_digits="",
                expiration_month="",
                expiration_year="",
                brand="",
                default=False,
                business_name="Fluid",
                city="Medellín",
                country="Colombia",
                email="test@fluidattacks.com",
                state="Antioquia",
                rut=None,
                tax_id=None,
            ),
            PaymentMethod(
                id="4722b0b7-cfeb-4898-8308-185dfc2523bc",
                fingerprint="",
                last_four_digits="",
                expiration_month="",
                expiration_year="",
                brand="",
                default=False,
                business_name="Testing Company and Sons",
                city="Medellín",
                country="Colombia",
                email="test@fluidattacks.com",
                state="Antioquia",
                rut=None,
                tax_id=None,
            ),
        )
    ],
)
@patch(
    MODULE_AT_TEST + "_list_credit_cards_info",
    new_callable=AsyncMock,
    return_value=[],
)
async def test_customer_payment_methods(
    mock_list_credit_cards_info: AsyncMock,
    expected_result: tuple[PaymentMethod],
) -> None:
    result = await list_organization_payment_methods(org=ORGANIZATION)
    assert result == list(expected_result)
    mock_list_credit_cards_info.assert_awaited_once()


@pytest.mark.parametrize(
    ("organization", "payment_id", "file_name", "expected_result"),
    [
        (
            ORGANIZATION,
            "4722b0b7-cfeb-4898-8308-185dfc2523bc",
            "test_file.pdf",
            "https://s3.amazonaws.com/integrates/johndoeatfluid-test-unit/"
            "resources/billing/okada/testing%20company%20and%20sons/"
            "test_file.pdf?X-Amz-Algorithm=TestX-Amz-Credential=Testus-east-1"
            "%2Fs3%2Faws4_request&X-Amz-Date=20230117T170631Z&X-Amz-Expires=10"
            "&X-Amz-SignedHeaders=host&X-Amz-Security-Token=TestX-Amz-"
            "Signature=Test",
        ),
    ],
)
@patch(MODULE_AT_TEST + "s3_ops.sign_url", new_callable=AsyncMock)
async def test_get_document_link(
    mock_s3_ops_sign_url: AsyncMock,
    organization: Organization,
    payment_id: str,
    file_name: str,
    expected_result: str,
) -> None:
    assert set_mocks_return_values(
        mocks_args=[[organization.name, payment_id, file_name]],
        mocked_objects=[mock_s3_ops_sign_url],
        module_at_test=MODULE_AT_TEST,
        paths_list=["s3_ops.sign_url"],
    )
    result = await get_document_link(organization, payment_id, file_name)
    assert mock_s3_ops_sign_url.called is True
    assert result == expected_result


@pytest.mark.parametrize(
    ("organization", "payment_id"),
    [
        (
            ORGANIZATION,
            "4722b0b7-cfeb-4898-8308-185dfc2523",
        ),
    ],
)
async def test_get_document_link_raises_ex(
    organization: Organization,
    payment_id: str,
) -> None:
    with pytest.raises(InvalidBillingPaymentMethod):
        await get_document_link(organization, payment_id, "")


@pytest.mark.parametrize(
    ("file_name", "content_type"),
    [
        ("billing-test-file.png", "image/png"),
        ("unittesting-test-file.csv", "text/csv"),
    ],
)
@patch(MODULE_AT_TEST + "s3_ops.upload_memory_file", new_callable=AsyncMock)
async def test_save_file(
    mock_s3_ops_upload_memory_file: AsyncMock,
    file_name: str,
    content_type: str,
) -> None:
    assert set_mocks_return_values(
        mocks_args=[[file_name]],
        mocked_objects=[mock_s3_ops_upload_memory_file],
        module_at_test=MODULE_AT_TEST,
        paths_list=["s3_ops.upload_memory_file"],
    )

    file_location = os.path.dirname(os.path.abspath(__file__))
    file_location = os.path.join(file_location, "mock/resources/" + file_name)
    async with aiofiles.open(file_location, "rb") as data:
        headers = Headers(headers={"content_type": content_type})
        file_contents = await data.read()
        test_file = UploadFile(
            filename=str(data.name),
            headers=headers,
            file=BytesIO(file_contents),
        )
        await save_file(file_object=test_file, file_name=file_name)
    mock_s3_ops_upload_memory_file.assert_called_with(test_file, f"resources/{file_name}")


@pytest.mark.parametrize(
    ("organization", "stripe_payment_method"),
    [
        (
            ORGANIZATION,
            {
                "id": "pm_1234567890",
                "card": {
                    "fingerprint": "abcd1234",
                    "last4": "1234",
                    "exp_month": 12,
                    "exp_year": 2024,
                    "brand": "Visa",
                },
            },
        )
    ],
)
@patch(
    MODULE_AT_TEST + "_list_credit_cards_info",
    new_callable=AsyncMock,
    return_value=[],
)
async def test_list_organization_payment_methods_with_org_billing_customer(
    mock_get_customer_payment_methods: AsyncMock,
    organization: Organization,
    stripe_payment_method: list[dict[str, Any]],
) -> None:
    mock_get_customer_payment_methods.return_value = [stripe_payment_method]

    results = await list_organization_payment_methods(org=organization)
    assert results
    mock_get_customer_payment_methods.assert_awaited_once()


@pytest.mark.parametrize(
    ("args"),
    [
        (
            ORGANIZATION._replace(payment_methods=[]),
            {
                "user_email": "user@example.org",
                "business_name": "Awesome business",
                "city": "Manizales",
                "country": "Colombia",
                "email": "manager@organization.org",
                "state": "Caldas",
                "rut": None,
                "tax_id": None,
            },
        ),
    ],
)
@patch(
    MODULE_AT_TEST + "update_documents",
    return_value=True,
    new_callable=AsyncMock,
)
@patch(MODULE_AT_TEST + "tickets_domain.request_other_payment_methods")
@patch(MODULE_AT_TEST + "organizations_model.update_metadata")
@patch(MODULE_AT_TEST + "billing_utils.validate_legal_document")
async def test_create_other_payment_method(
    mock_billing_utils_validate_legal_document: AsyncMock,
    mock_organizations_model_update_metadata: AsyncMock,
    mock_tickets_domain_request_other_payment_methods: AsyncMock,
    mock_billing_utils_update_documents: AsyncMock,
    args: tuple,
) -> None:
    org = args[0]
    assert await create_other_payment_method(
        org=org,
        user_email=args[1]["user_email"],
        business_name=args[1]["business_name"],
        city=args[1]["city"],
        country=args[1]["country"],
        email=args[1]["email"],
        state=args[1]["state"],
        rut=args[1]["rut"],
        tax_id=args[1]["tax_id"],
    )
    mocks: list[AsyncMock] = [
        mock_billing_utils_validate_legal_document,
        mock_organizations_model_update_metadata,
        mock_tickets_domain_request_other_payment_methods,
        mock_billing_utils_update_documents,
    ]
    map(lambda x: x.assert_awaited_once, mocks)



@patch(MODULE_AT_TEST + "_remove_other_payment_method")
async def test_remove_other_payment_method(
    mock_remove_other_payment_method: AsyncMock,
) -> None:
    assert await remove_payment_method(
        payment_method_id=ORGANIZATION.payment_methods[0].id
        if ORGANIZATION.payment_methods
        else "",
        org=ORGANIZATION._replace(billing_information=None),
    )
    mock_remove_other_payment_method.assert_awaited_once()


@pytest.mark.parametrize(
    "country",
    [
        [
            "Panama",
        ],
        [
            "Colombia",
        ],
    ],
)
@patch(
    MODULE_AT_TEST + "stripe_customers.create_credit_card_payment_method",
    new_callable=AsyncMock,
    return_value={"checks": {"cvc_check": "passed"}, "last4": "0322"},
)
@patch(
    MODULE_AT_TEST + "epayco_customers.associate_card_with_customer",
    new_callable=AsyncMock,
    return_value=EPaycoCard(
        last_four_digits="0322",
        franchise="visa",
        id="Xwk5pF",
        default=False,
        mask="123456******0322",
    ),
)
@patch(
    MODULE_AT_TEST + "billing_utils.send_payment_method_added_notification",
    new_callable=AsyncMock,
    return_value=None,
)
async def test_create_credit_card_payment_method(
    mock_send_payment_method_added_notification: AsyncMock,
    mock_epayco_associate_card_with_customer: AsyncMock,
    mock_stripe_create_card_payment_method: AsyncMock,
    country: str,
) -> None:
    assert await create_credit_card_payment_method(
        org=ORGANIZATION,
        user_email="user@example.org",
        make_default=True,
        payment_method_id="21kelwko123p20303",
        country=country,
        billing_email="billing@fluidatatcks.com",
        billed_groups=["group1"],
        loaders=AsyncMock(
            group=AsyncMock(load_many=AsyncMock(return_value=[ORG_GROUP1])),
        ),
    )

    if country == "Colombia":
        mock_epayco_associate_card_with_customer.assert_awaited_once_with(
            organization=ORGANIZATION,
            make_default=True,
            payment_method_id="21kelwko123p20303",
            billing_email="billing@fluidatatcks.com",
        )
    else:
        mock_stripe_create_card_payment_method.assert_awaited_once_with(
            org=ORGANIZATION,
            billing_email="billing@fluidatatcks.com",
            make_default=True,
            payment_method_id="21kelwko123p20303",
        )

    mock_send_payment_method_added_notification.assert_awaited_once_with(
        org_name=ORGANIZATION.name, user_email="user@example.org", last4="0322"
    )


@patch(
    MODULE_AT_TEST + "epayco_customers.list_customer_credit_cards",
    new_callable=AsyncMock,
    return_value=[],
)
async def test_list_credit_card_info_colombia(
    mock_epayco_list_customer_credit_cards: AsyncMock,
) -> None:
    await _list_credit_cards_info(
        billing_customer_id="1234",
        organization_name="testorg",
        organization_country="Colombia",
    )
    mock_epayco_list_customer_credit_cards.assert_awaited_once()


@patch(
    MODULE_AT_TEST + "stripe.Customer.retrieve_async",
    new_callable=AsyncMock,
    return_value=MagicMock(id="1", default_payment_method="32"),
)
@patch(
    MODULE_AT_TEST + "stripe_customers.get_customer_payment_methods",
    new_callable=AsyncMock,
    return_value=[],
)
async def test_list_credit_card_info(
    mock_stripe_customers_get_customer_payment_methods: AsyncMock,
    mock_stripe_customer_retrieve_async: AsyncMock,
) -> None:
    await _list_credit_cards_info(
        billing_customer_id="1234",
        organization_name="testorg",
        organization_country="Panama",
    )
    mock_stripe_customer_retrieve_async.assert_awaited_once()
    mock_stripe_customers_get_customer_payment_methods.assert_awaited_once()


@patch(
    MODULE_AT_TEST + "epayco_customers.create_customer",
    new_callable=AsyncMock,
    return_value=EPAYCO_CUSTOMER,
)
@patch(
    MODULE_AT_TEST + "treli.create_organization_subscription",
    new_callable=AsyncMock,
)
@patch(
    MODULE_AT_TEST + "orgs_domain.add_billing_information",
    new_callable=AsyncMock,
)
@patch(
    MODULE_AT_TEST + "billing_utils.send_payment_method_added_notification",
    new_callable=AsyncMock,
)
async def test_add_billing_info_epayco(
    mock_send_payment_method_added_notification: AsyncMock,
    mock_update_billing_information: AsyncMock,
    mock_treli_create_organization_subscription: AsyncMock,
    mock_epayco_create_customer: AsyncMock,
) -> None:
    await _add_billing_information(
        organization=ORGANIZATION,
        user_email="jdoe@fluidattacks.com",
        payment_method_id=EPAICO_CARDS[0].id,
        country="Colombia",
        billed_groups=[ORG_GROUP1],
        billing_email="billing@fluidatatcks.com",
        business_id="223.324.443-1",
        business_name="Fluid",
    )

    mock_epayco_create_customer.assert_awaited_once()
    mock_treli_create_organization_subscription.assert_awaited_once()
    mock_update_billing_information.assert_awaited_once()
    mock_send_payment_method_added_notification.assert_awaited_once()


@patch(
    MODULE_AT_TEST + "stripe.Customer.create_async",
    new_callable=AsyncMock,
)
@patch(
    MODULE_AT_TEST + "stripe_customers.attach_payment_method",
    new_callable=AsyncMock,
)
@patch(
    MODULE_AT_TEST + "orgs_domain.add_billing_information",
    new_callable=AsyncMock,
)
@patch(
    MODULE_AT_TEST + "billing_utils.send_payment_method_added_notification",
    new_callable=AsyncMock,
)
async def test_add_billing_info_stripe(
    mock_send_payment_method_added_notification: AsyncMock,
    mock_update_billing_information: AsyncMock,
    mock_stripe_customers_attach_payment_method: AsyncMock,
    mock_customer_stripe_create_async: AsyncMock,
    *,
    make_stripe_customer: Callable[[bool], Customer],
    make_stripe_pyment_method: Callable[[bool, str | None], StripePaymentMethod],
) -> None:
    stripe_customer = make_stripe_customer(True)
    mock_customer_stripe_create_async.return_value = stripe_customer
    stripe_card = (make_stripe_pyment_method(True, None)).card
    mock_stripe_customers_attach_payment_method.return_value = stripe_card

    await _add_billing_information(
        organization=ORGANIZATION,
        user_email="jdoe@fluidattacks.com",
        payment_method_id="pm_98899",
        country="Panama",
        billed_groups=[ORG_GROUP1],
        billing_email="billing@fluidatatcks.com",
        business_id="223.324.443-1",
        business_name="Fluid",
    )

    mock_customer_stripe_create_async.assert_awaited_once_with(
        name=ORGANIZATION.name, email="billing@fluidatatcks.com"
    )
    mock_stripe_customers_attach_payment_method.assert_awaited_once_with(
        payment_method_id="pm_98899", org_billing_customer=stripe_customer.id
    )
    mock_update_billing_information.assert_awaited_once()
    assert stripe_card
    mock_send_payment_method_added_notification.assert_awaited_once_with(
        org_name=ORGANIZATION.name,
        user_email="jdoe@fluidattacks.com",
        last4=stripe_card.last4,
    )

    update_args = mock_update_billing_information.call_args

    billing_info: OrganizationBilling = update_args[1]["billing_information"]
    assert billing_info.customer_id == stripe_customer.id
    assert billing_info.billed_groups == ["group1"]
    assert billing_info.last_modified_by == "jdoe@fluidattacks.com"
    assert billing_info.billing_email == "billing@fluidatatcks.com"
