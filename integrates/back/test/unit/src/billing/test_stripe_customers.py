from collections.abc import Callable
from datetime import (
    datetime,
)
from decimal import (
    Decimal,
)
from unittest.mock import (
    AsyncMock,
    MagicMock,
    patch,
)

import pytest
from freezegun import (
    freeze_time,
)
from stripe import (
    Customer,
    PaymentMethod,
    Subscription,
)

from integrates.billing.stripe_customers import (
    _get_subscription_usage,
    _pay_advanced_authors_to_date,
    attach_payment_method,
    create_credit_card_payment_method,
    get_customer_payment_methods,
    get_group_subscriptions,
    report_subscription_usage,
    update_credit_card_info,
    update_default_payment_method,
    update_subscription,
)
from integrates.billing.types import (
    GroupAuthor,
    Price,
)
from integrates.custom_exceptions import (
    CouldNotCreatePaymentMethod,
    PaymentMethodAlreadyExists,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.db_model.organizations.enums import (
    OrganizationStateStatus,
)
from integrates.db_model.organizations.types import (
    DocumentFile,
    Organization,
    OrganizationBilling,
    OrganizationDocuments,
    OrganizationPaymentMethods,
    OrganizationState,
)
from integrates.db_model.types import (
    Policies,
)
from test.unit.src.utils import (
    get_module_at_test,
)

from .conftest import (
    TPlan,
    TStatus,
)

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"

pytestmark = [
    pytest.mark.asyncio,
]


ORGANIZATION = Organization(
    created_by="unknown@unknown.com",
    created_date=datetime.fromisoformat("2018-02-08T00:43:18+00:00"),
    id="ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
    name="okada",
    policies=Policies(
        modified_date=datetime.fromisoformat("2019-11-22T20:07:57+00:00"),
        modified_by="integratesmanager@gmail.com",
        inactivity_period=90,
        max_acceptance_days=60,
        max_acceptance_severity=Decimal("10.0"),
        max_number_acceptances=2,
        min_acceptance_severity=Decimal("0.0"),
        min_breaking_severity=Decimal("0"),
        vulnerability_grace_period=0,
    ),
    state=OrganizationState(
        aws_external_id=("d1042a09-582f-4fbb-b3ac-956478f053ad"),
        status=OrganizationStateStatus.ACTIVE,
        modified_by="unknown",
        modified_date=datetime.fromisoformat("2018-02-08T00:43:18+00:00"),
        pending_deletion_date=datetime.fromisoformat("2019-11-22T20:07:57+00:00"),
    ),
    country="Colombia",
    billing_information=[
        OrganizationBilling(
            billing_email="jdoe@org.com",
            customer_id="21kelwko123p20303",
            last_modified_by="jdoe@org.com",
            modified_at=datetime.fromisoformat("2023-04-17T00:00:00+00:00"),
            billed_groups=["group1"],
        )
    ],
    payment_methods=[
        OrganizationPaymentMethods(
            id="pm123",
            business_name="Sample Business",
            email="business@example.com",
            country="Colombia",
            state="Stateness",
            city="Cityville",
            documents=OrganizationDocuments(
                rut=DocumentFile(
                    "path/to/rut_document.pdf",
                    datetime(2023, 4, 17),
                ),
                tax_id=DocumentFile(
                    "path/to/tax_id_document.pdf",
                    datetime(2023, 9, 8),
                ),
            ),
        )
    ],
)


@patch(
    MODULE_AT_TEST + "stripe.PaymentMethod.retrieve_async",
    new_callable=AsyncMock,
)
@patch(
    MODULE_AT_TEST + "get_customer_payment_methods",
    new_callable=AsyncMock,
)
@patch(
    MODULE_AT_TEST + "stripe.Customer.retrieve_async",
    new_callable=AsyncMock,
)
@patch(
    MODULE_AT_TEST + "attach_payment_method",
    new_callable=AsyncMock,
)
@patch(
    MODULE_AT_TEST + "update_default_payment_method",
    new_callable=AsyncMock,
)
async def test_create_credit_card_payment(
    mock_dal_update_default_payment_method: AsyncMock,
    mock_dal_attach_payment_method: AsyncMock,
    mock_stripe_customer_retrieve_async: AsyncMock,
    mock_get_customer_payment_methods: AsyncMock,
    mock_stripe_payment_method_retrieve_async: AsyncMock,
    *,
    make_stripe_customer: Callable[[bool], Customer],
    make_stripe_pyment_method: Callable[[bool, str | None], PaymentMethod],
) -> None:
    customer = make_stripe_customer(False)
    mock_stripe_customer_retrieve_async.return_value = customer
    new_payment_method = make_stripe_pyment_method(False, "mEoisGZ01V71BCos")
    mock_stripe_payment_method_retrieve_async.return_value = new_payment_method
    mock_get_customer_payment_methods.return_value = [make_stripe_pyment_method(False, None)]
    mock_dal_attach_payment_method.return_value = new_payment_method.card
    card = await create_credit_card_payment_method(
        org=ORGANIZATION,
        make_default=True,
        payment_method_id="38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
        billing_email=customer.email or "",
    )

    mock_stripe_customer_retrieve_async.assert_awaited_once()
    mock_get_customer_payment_methods.assert_awaited_once()
    mock_stripe_payment_method_retrieve_async.assert_awaited_once()
    mock_dal_update_default_payment_method.assert_awaited_once()
    mock_dal_attach_payment_method.assert_awaited_once()
    assert card == mock_dal_attach_payment_method.return_value


@patch(
    MODULE_AT_TEST + "stripe.PaymentMethod.retrieve_async",
    new_callable=AsyncMock,
)
@patch(
    MODULE_AT_TEST + "get_customer_payment_methods",
    new_callable=AsyncMock,
)
@patch(
    MODULE_AT_TEST + "stripe.Customer.retrieve_async",
    new_callable=AsyncMock,
)
async def test_create_credit_card_payment_method_raises_payment_exists(
    mock_stripe_customer_retrieve_async: AsyncMock,
    mock_get_customer_payment_methods: AsyncMock,
    mock_stripe_payment_method_retrieve_async: AsyncMock,
    make_stripe_customer: Callable[[bool], Customer],
    make_stripe_pyment_method: Callable[[bool, str | None], PaymentMethod],
) -> None:
    customer = make_stripe_customer(True)
    mock_stripe_customer_retrieve_async.return_value = customer
    mock_stripe_payment_method_retrieve_async.return_value = make_stripe_pyment_method(False, None)
    mock_get_customer_payment_methods.return_value = [make_stripe_pyment_method(False, None)]
    with pytest.raises(PaymentMethodAlreadyExists):
        await create_credit_card_payment_method(
            org=ORGANIZATION,
            make_default=True,
            payment_method_id="38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
            billing_email=customer.email or "",
        )

    mock_stripe_customer_retrieve_async.assert_awaited_once()
    mock_get_customer_payment_methods.assert_awaited_once()
    mock_stripe_payment_method_retrieve_async.assert_awaited_once()


@patch(
    MODULE_AT_TEST + "stripe.Customer.list_payment_methods_async",
    new_callable=AsyncMock,
)
async def test_get_customer_payment_methods(
    mock_stripe_customer_list_payment_methods_async: AsyncMock,
) -> None:
    mock_stripe_customer_list_payment_methods_async.return_value = MagicMock(data=[])
    await get_customer_payment_methods(customer_id="customer", limit=12)
    mock_stripe_customer_list_payment_methods_async.assert_awaited_once_with(
        "customer",
        type="card",
        limit=12,
    )


@patch(
    MODULE_AT_TEST + "get_prices",
    new_callable=AsyncMock,
    return_value={
        "advanced": Price("price_id_123", "USD", 1500),
        "essential": Price("price_id_456", "USD", 2000),
    },
)
@patch(MODULE_AT_TEST + "stripe.Subscription.modify_async", new_callable=AsyncMock)
async def test_update_subscription(
    mock_stripe_ubscription_modify_async: AsyncMock,
    mock_get_prices: AsyncMock,
    make_stripe_subscription: Callable[[TStatus, TPlan], Subscription],
) -> None:
    _subscription = make_stripe_subscription("active", "essential")
    mock_stripe_ubscription_modify_async.return_value = _subscription

    assert await update_subscription(subscription=_subscription, upgrade=True)
    mock_get_prices.assert_awaited_once()
    mock_stripe_ubscription_modify_async.assert_awaited_once()


@patch(MODULE_AT_TEST + "stripe.Subscription.modify_async", new_callable=AsyncMock)
@patch(
    MODULE_AT_TEST + "_pay_advanced_authors_to_date",
    new_callable=AsyncMock,
    return_value=True,
)
@patch(
    MODULE_AT_TEST + "get_prices",
    return_value={
        "advanced": Price("price_id_123", "USD", 1500),
        "essential": Price("price_id_456", "USD", 2000),
    },
)
async def test_update_subscription_no_downgrade(
    mock_get_prices: AsyncMock,
    mock_pay_advanced_authors_to_date: AsyncMock,
    mock_stripe_subscription_modify_async: AsyncMock,
    make_stripe_subscription: Callable[[TStatus, TPlan], Subscription],
) -> None:
    _subscription = make_stripe_subscription("active", "advanced")
    mock_stripe_subscription_modify_async.return_value = _subscription

    assert await update_subscription(
        subscription=_subscription,
        upgrade=False,
    )
    mock_get_prices.assert_awaited_once()
    mock_pay_advanced_authors_to_date.assert_awaited_once_with(
        prices=mock_get_prices.return_value, subscription=_subscription
    )
    mock_stripe_subscription_modify_async.assert_awaited_once_with(
        _subscription.id,
        **Subscription.ModifyParams(
            items=[
                Subscription.ModifyParamsItem(
                    clear_usage=True,
                    deleted=True,
                )
            ],
            metadata={"subscription": "essential"},
        ),
    )


@patch(MODULE_AT_TEST + "_get_subscription_usage", return_value=5)
@patch(MODULE_AT_TEST + "stripe.SubscriptionItem.create_usage_record_async")
async def test_report_subscription_usage(
    mock_stripe_subscription_create_usage_record_async: AsyncMock,
    mock_get_subscription_usage: AsyncMock,
    make_stripe_subscription: Callable[[TStatus, TPlan], Subscription],
) -> None:
    result = await report_subscription_usage(
        subscription=make_stripe_subscription("active", "advanced")
    )
    assert result
    mock_get_subscription_usage.assert_awaited_once()
    mock_stripe_subscription_create_usage_record_async.assert_awaited_once()


@patch(MODULE_AT_TEST + "_get_subscription_usage", return_value=2)
@patch(
    MODULE_AT_TEST + "stripe.Customer.retrieve_async",
    new_callable=AsyncMock,
)
@patch(
    MODULE_AT_TEST + "stripe.PaymentIntent.create_async",
    return_value=MagicMock(status="succeeded"),
)
async def test_pay_advanced_authors_to_date(
    mock_stripe_payment_intent_create_async: AsyncMock,
    mock_stripe_customer_retrieve_async: AsyncMock,
    mock_get_subscription_usage: AsyncMock,
    make_stripe_subscription: Callable[[TStatus, TPlan], Subscription],
    make_stripe_customer: Callable[[bool], Customer],
) -> None:
    _subscription = make_stripe_subscription("active", "advanced")
    mock_stripe_customer_retrieve_async.return_value = make_stripe_customer(True)
    assert await _pay_advanced_authors_to_date(
        prices={"advanced": Price(id="90fce479b25", currency="USD", amount=900)},
        subscription=_subscription,
    )
    mock_stripe_payment_intent_create_async.assert_awaited_once()
    mock_stripe_customer_retrieve_async.assert_awaited_once_with("cus_Na6dX7aXxi11N4")
    mock_get_subscription_usage.assert_awaited_once_with(subscription=_subscription)


@patch(
    MODULE_AT_TEST + "billing_authors.get_group_authors",
    return_value=(
        GroupAuthor(
            actor="testing",
            commit="04f6b8bcb2d286ee4667f0c90fce479b2545s",
            groups=frozenset(["group1", "group2", "group3"]),
            organization="fluid_org",
            repository="repo_fluid",
        ),
    ),
)
@freeze_time("2020-12-31T18:40:37+00:00")
async def test_get_subscription_usage(
    mock_get_group_authors: AsyncMock,
    make_stripe_subscription: Callable[[TStatus, TPlan], Subscription],
) -> None:
    result: int = await _get_subscription_usage(
        subscription=make_stripe_subscription("active", "advanced")
    )
    assert result == 1
    mock_get_group_authors.assert_awaited_once_with(
        date=datetime_utils.get_utc_now(), group="unittesting"
    )


@patch(
    MODULE_AT_TEST + "stripe.PaymentMethod.attach_async",
    new_callable=AsyncMock,
)
async def test_attach_payment_method(
    mock_stripe_pyment_method_attach_async: AsyncMock,
    make_stripe_pyment_method: Callable[[bool, str | None], PaymentMethod],
) -> None:
    mock_stripe_pyment_method_attach_async.return_value = make_stripe_pyment_method(False, None)
    await attach_payment_method(
        payment_method_id="pm_1NDD72eZvKYlo2CkMTDiXDQ",
        org_billing_customer="cus_9s6XBnVi5gbXub",
    )
    mock_stripe_pyment_method_attach_async.assert_awaited_once()


@patch(
    MODULE_AT_TEST + "stripe.PaymentMethod.attach_async",
    new_callable=AsyncMock,
)
@patch(
    MODULE_AT_TEST + "stripe.PaymentMethod.detach_async",
    new_callable=AsyncMock,
    return_value=None,
)
async def test_attach_payment_method_fail(
    mock_stripe_payment_method_detach_async: AsyncMock,
    mock_stripe_pyment_method_attach_async: AsyncMock,
    make_stripe_pyment_method: Callable[[bool, str | None], PaymentMethod],
) -> None:
    mock_stripe_pyment_method_attach_async.return_value = make_stripe_pyment_method(True, None)
    with pytest.raises(CouldNotCreatePaymentMethod):
        await attach_payment_method(
            payment_method_id="pm_1NDD72eZvKYlo2CkMTDiXDQ",
            org_billing_customer="cus_9s6XBnVi5gbXub",
        )
    mock_stripe_payment_method_detach_async.assert_awaited_once_with("pm_1NDD72eZvKYlo2CkMTDiXDQ")
    mock_stripe_pyment_method_attach_async.assert_awaited_once()


@patch(
    MODULE_AT_TEST + "stripe.PaymentMethod.modify_async",
    new_callable=AsyncMock,
)
async def test_update_payment_method(
    mock_stripe_payment_method_modify_async: AsyncMock,
    make_stripe_pyment_method: Callable[[bool, str | None], PaymentMethod],
) -> None:
    mock_stripe_payment_method_modify_async.return_value = make_stripe_pyment_method(False, None)
    await update_credit_card_info(
        payment_method_id="payment_method_id",
        card_expiration_month=12,
        card_expiration_year=2025,
    )
    mock_stripe_payment_method_modify_async.assert_awaited_once_with(
        "payment_method_id",
        card={
            "exp_month": 12,
            "exp_year": 2025,
        },
    )


@patch(MODULE_AT_TEST + "stripe.Customer.modify_async", new_callable=AsyncMock)
async def test_update_default_payment_method(
    mock_stripe_customer_modify_async: AsyncMock,
) -> None:
    mock_stripe_customer_modify_async.return_value = MagicMock(
        invoice_settings=MagicMock(default_payment_method="payment_method_id")
    )
    assert await update_default_payment_method(
        payment_method_id="payment_method_id",
        org_billing_customer="org_billing_customer",
    )
    mock_stripe_customer_modify_async.assert_awaited_once_with(
        "org_billing_customer",
        invoice_settings={"default_payment_method": "payment_method_id"},
    )


@patch(MODULE_AT_TEST + "stripe.Subscription.list_async")
async def test_get_group_subscriptions(
    mock_stripe_subscription_list_async: AsyncMock,
    make_stripe_subscription: Callable[[TStatus, TPlan], Subscription],
) -> None:
    _subscriptions = [
        make_stripe_subscription("active", "advanced"),
        make_stripe_subscription("canceled", "advanced"),
    ]

    mock_stripe_subscription_list_async.return_value = MagicMock(data=_subscriptions)

    result = await get_group_subscriptions(
        group_name="unittesting",
        org_billing_customer="org_billing_customer",
        status="active",
    )

    assert result[0] == _subscriptions[0]
