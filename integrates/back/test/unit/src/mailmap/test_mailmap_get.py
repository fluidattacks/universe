from integrates.custom_exceptions import (
    MailmapEntryNotFound,
    MailmapSubentryNotFound,
)
from integrates.db_model.mailmap.types import (
    MailmapEntry,
    MailmapEntryWithSubentries,
    MailmapRecord,
    MailmapSubentry,
)
from integrates.mailmap.get import (
    get_mailmap_entries,
    get_mailmap_entries_with_subentries,
    get_mailmap_entry,
    get_mailmap_entry_subentries,
    get_mailmap_entry_with_subentries,
    get_mailmap_records,
    get_mailmap_subentry,
)
import pytest
from typing import (
    List,
)


@pytest.mark.asyncio
@pytest.mark.parametrize(
    ("organization_id", "_entry_emails", "_subentry_emails"),
    [
        (
            "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
            [
                "alice@entry.com",
                "bob@entry.com",
                "eve@entry.com",
                "trent@entry.com",
                "grace@entry.com",
            ],
            [
                "alice@subentry.com",
                "alice-b@subentry.com",
                "bob@subentry.com",
                "bob-c@subentry.com",
                "eve@subentry.com",
                "eve-f@subentry.com",
                "trent@subentry.com",
                "trent-s@subentry.com",
                "grace@subentry.com",
                "grace-h@subentry.com",
            ],
        ),
    ],
)
async def test_get_mailmap_entries_with_subentries(
    organization_id: str,
    _entry_emails: List[str],
    _subentry_emails: List[str],
) -> None:
    entries_with_subentries = await get_mailmap_entries_with_subentries(
        organization_id=organization_id,
    )
    # verification
    entry_emails = list(
        map(
            lambda ews: ews["entry"]["mailmap_entry_email"],
            entries_with_subentries,
        )
    )
    assert all(
        map(lambda _entry_email: _entry_email in entry_emails, _entry_emails)
    )
    subentry_emails = []
    list(
        map(
            lambda ews: subentry_emails.extend(
                list(
                    map(
                        lambda subentry: subentry["mailmap_subentry_email"],
                        ews["subentries"],
                    )
                )
            ),
            entries_with_subentries,
        )
    )
    assert all(
        map(
            lambda _subentry_email: _subentry_email in subentry_emails,
            _subentry_emails,
        )
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(
    ("organization_id", "_records"),
    [
        (
            "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
            [
                MailmapRecord(
                    mailmap_entry_name="Grace Musa",
                    mailmap_entry_email="grace@entry.com",
                    mailmap_subentry_name="Grace H Musa",
                    mailmap_subentry_email="grace-h@subentry.com",
                ),
                MailmapRecord(
                    mailmap_entry_name="Grace Musa",
                    mailmap_entry_email="grace@entry.com",
                    mailmap_subentry_name="Grace Musa",
                    mailmap_subentry_email="grace@subentry.com",
                ),
                MailmapRecord(
                    mailmap_entry_name="Trent Hernandez",
                    mailmap_entry_email="trent@entry.com",
                    mailmap_subentry_name="Trent S Hernandez",
                    mailmap_subentry_email="trent-s@subentry.com",
                ),
                MailmapRecord(
                    mailmap_entry_name="Trent Hernandez",
                    mailmap_entry_email="trent@entry.com",
                    mailmap_subentry_name="Trent Hernandez",
                    mailmap_subentry_email="trent@subentry.com",
                ),
            ],
        ),
    ],
)
async def test_get_mailmap_records(
    organization_id: str,
    _records: List[MailmapRecord],
) -> None:
    records = await get_mailmap_records(
        organization_id=organization_id,
    )
    # verification
    assert all(map(lambda _record: _record in records, _records))


@pytest.mark.asyncio
@pytest.mark.parametrize(
    ("organization_id", "_entry_emails"),
    [
        (
            "okada",
            [
                "alice@entry.com",
                "bob@entry.com",
                "eve@entry.com",
                "trent@entry.com",
                "grace@entry.com",
            ],
        ),
    ],
)
async def test_get_mailmap_entries(
    organization_id: str,
    _entry_emails: List[str],
) -> None:
    connection = await get_mailmap_entries(
        organization_id=organization_id,
        after="",
        first=100,
    )
    entries = [edge.node for edge in connection.edges]
    # verification
    entry_emails = list(
        map(
            lambda entry: entry["mailmap_entry_email"],
            entries,
        )
    )
    assert all(
        map(lambda _entry_email: _entry_email in entry_emails, _entry_emails)
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(
    ("entry_email", "organization_id", "_entry_with_subentries"),
    [
        (
            "alice@entry.com",
            "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
            MailmapEntryWithSubentries(
                entry=MailmapEntry(
                    mailmap_entry_name="Alice Garcia",
                    mailmap_entry_email="alice@entry.com",
                ),
                subentries=[
                    MailmapSubentry(
                        mailmap_subentry_name="Alice Garcia",
                        mailmap_subentry_email="alice@subentry.com",
                    ),
                    MailmapSubentry(
                        mailmap_subentry_name="Alice B Garcia",
                        mailmap_subentry_email="alice-b@subentry.com",
                    ),
                ],
            ),
        ),
    ],
)
async def test_get_mailmap_entry_with_subentries(
    entry_email: str,
    organization_id: str,
    _entry_with_subentries: MailmapEntryWithSubentries,
) -> None:
    entry_with_subentries = await get_mailmap_entry_with_subentries(
        entry_email=entry_email,
        organization_id=organization_id,
    )
    # verification
    entry = entry_with_subentries["entry"]
    _entry = _entry_with_subentries["entry"]
    assert entry == _entry
    subentries = entry_with_subentries["subentries"]
    _subentries = _entry_with_subentries["subentries"]
    assert all(map(lambda subentry: subentry in _subentries, subentries))


@pytest.mark.asyncio
@pytest.mark.parametrize(
    ("entry_email", "organization_id"),
    [
        ("not-found@entry.com", "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"),
    ],
)
async def test_get_mailmap_entry_with_subentries_exception(
    entry_email: str,
    organization_id: str,
) -> None:
    with pytest.raises(MailmapEntryNotFound):
        await get_mailmap_entry_with_subentries(
            entry_email=entry_email,
            organization_id=organization_id,
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(
    ("entry_email", "organization_id", "_entry"),
    [
        (
            "alice@entry.com",
            "okada",
            MailmapEntry(
                mailmap_entry_name="Alice Garcia",
                mailmap_entry_email="alice@entry.com",
            ),
        ),
    ],
)
async def test_get_mailmap_entry(
    entry_email: str,
    organization_id: str,
    _entry: MailmapEntry,
) -> None:
    entry = await get_mailmap_entry(
        entry_email=entry_email,
        organization_id=organization_id,
    )
    # verification
    assert entry == _entry


@pytest.mark.asyncio
@pytest.mark.parametrize(
    ("entry_email", "organization_id"),
    [
        ("not-found@entry.com", "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"),
    ],
)
async def test_get_mailmap_entry_exception(
    entry_email: str,
    organization_id: str,
) -> None:
    with pytest.raises(MailmapEntryNotFound):
        await get_mailmap_entry(
            entry_email=entry_email,
            organization_id=organization_id,
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(
    ("entry_email", "organization_id", "_subentries"),
    [
        (
            "alice@entry.com",
            "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
            [
                MailmapSubentry(
                    mailmap_subentry_name="Alice Garcia",
                    mailmap_subentry_email="alice@subentry.com",
                ),
                MailmapSubentry(
                    mailmap_subentry_name="Alice B Garcia",
                    mailmap_subentry_email="alice-b@subentry.com",
                ),
            ],
        ),
    ],
)
async def test_get_mailmap_entry_subentries(
    entry_email: str, organization_id: str, _subentries: list[MailmapSubentry]
) -> None:
    entry_subentries = await get_mailmap_entry_subentries(
        entry_email=entry_email,
        organization_id=organization_id,
    )
    # verification
    assert all(
        map(
            lambda _subentry: _subentry in entry_subentries,
            _subentries,
        )
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(
    ("entry_email", "organization_id"),
    [
        ("not-found@entry.com", "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"),
    ],
)
async def test_get_mailmap_entry_subentries_exception(
    entry_email: str,
    organization_id: str,
) -> None:
    with pytest.raises(MailmapEntryNotFound):
        await get_mailmap_entry_subentries(
            entry_email=entry_email,
            organization_id=organization_id,
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(
    (
        "entry_email",
        "subentry_email",
        "subentry_name",
        "organization_id",
        "_subentry",
    ),
    [
        (
            "alice@entry.com",
            "alice@subentry.com",
            "Alice Garcia",
            "okada",
            MailmapSubentry(
                mailmap_subentry_name="Alice Garcia",
                mailmap_subentry_email="alice@subentry.com",
            ),
        ),
    ],
)
async def test_get_mailmap_subentry(
    entry_email: str,
    subentry_email: str,
    subentry_name: str,
    organization_id: str,
    _subentry: MailmapSubentry,
) -> None:
    subentry = await get_mailmap_subentry(
        entry_email=entry_email,
        subentry_email=subentry_email,
        subentry_name=subentry_name,
        organization_id=organization_id,
    )
    # verification
    assert subentry == _subentry


@pytest.mark.asyncio
@pytest.mark.parametrize(
    ("entry_email", "subentry_email", "subentry_name", "organization_id"),
    [
        (
            "not-found@entry.com",
            "alice@subentry.com",
            "Alice Garcia",
            "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
        ),
    ],
)
async def test_get_mailmap_subentry_exception_a(
    entry_email: str,
    subentry_email: str,
    subentry_name: str,
    organization_id: str,
) -> None:
    with pytest.raises(MailmapEntryNotFound):
        await get_mailmap_subentry(
            entry_email=entry_email,
            subentry_email=subentry_email,
            subentry_name=subentry_name,
            organization_id=organization_id,
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(
    ("entry_email", "subentry_email", "subentry_name", "organization_id"),
    [
        (
            "alice@entry.com",
            "not-found@subentry.com",
            "Alice Garcia",
            "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
        ),
    ],
)
async def test_get_mailmap_subentry_exception_b(
    entry_email: str,
    subentry_email: str,
    subentry_name: str,
    organization_id: str,
) -> None:
    with pytest.raises(MailmapSubentryNotFound):
        await get_mailmap_subentry(
            entry_email=entry_email,
            subentry_email=subentry_email,
            subentry_name=subentry_name,
            organization_id=organization_id,
        )
