from integrates.custom_exceptions import (
    MailmapEntryNotFound,
    MailmapSubentryNotFound,
)
from integrates.mailmap.delete import (
    delete_mailmap_entry,
    delete_mailmap_entry_subentries,
    delete_mailmap_entry_with_subentries,
    delete_mailmap_subentry,
)
from integrates.mailmap.get import (
    get_mailmap_entry,
    get_mailmap_entry_with_subentries,
    get_mailmap_subentry,
)
import pytest


@pytest.mark.asyncio
@pytest.mark.parametrize(
    ("entry_email", "organization_id"),
    [
        ("oscar@entry.com", "okada"),
    ],
)
async def test_delete_mailmap_entry(
    entry_email: str,
    organization_id: str,
) -> None:
    await delete_mailmap_entry(
        entry_email=entry_email,
        organization_id=organization_id,
    )
    # verification
    with pytest.raises(MailmapEntryNotFound):
        await get_mailmap_entry(
            entry_email=entry_email,
            organization_id=organization_id,
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(
    ("entry_email", "organization_id"),
    [
        ("not-found@entry.com", "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"),
    ],
)
async def test_delete_mailmap_entry_exception(
    entry_email: str,
    organization_id: str,
) -> None:
    with pytest.raises(MailmapEntryNotFound):
        await delete_mailmap_entry(
            entry_email=entry_email,
            organization_id=organization_id,
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(
    ("entry_email", "subentry_email", "subentry_name", "organization_id"),
    [
        (
            "charlie@entry.com",
            "charlie@entry.com",
            "Charlie Nguyen D",
            "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
        ),
    ],
)
async def test_delete_mailmap_subentry(
    entry_email: str,
    subentry_email: str,
    subentry_name: str,
    organization_id: str,
) -> None:
    await delete_mailmap_subentry(
        entry_email=entry_email,
        subentry_email=subentry_email,
        subentry_name=subentry_name,
        organization_id=organization_id,
    )
    # verification
    with pytest.raises(MailmapSubentryNotFound):
        await get_mailmap_subentry(
            entry_email=entry_email,
            subentry_email=subentry_email,
            subentry_name=subentry_name,
            organization_id=organization_id,
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(
    ("entry_email", "subentry_email", "subentry_name", "organization_id"),
    [
        (
            "not-found@entry.com",
            "alice@subentry.com",
            "Alice Garcia",
            "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
        ),
    ],
)
async def test_delete_mailmap_subentry_exception_a(
    entry_email: str,
    subentry_email: str,
    subentry_name: str,
    organization_id: str,
) -> None:
    with pytest.raises(MailmapEntryNotFound):
        await delete_mailmap_subentry(
            entry_email=entry_email,
            subentry_email=subentry_email,
            subentry_name=subentry_name,
            organization_id=organization_id,
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(
    ("entry_email", "subentry_email", "subentry_name", "organization_id"),
    [
        (
            "eve@entry.com",
            "not-found@subentry.com",
            "Eve Anderson",
            "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
        ),
    ],
)
async def test_delete_mailmap_subentry_exception_b(
    entry_email: str,
    subentry_email: str,
    subentry_name: str,
    organization_id: str,
) -> None:
    with pytest.raises(MailmapSubentryNotFound):
        await delete_mailmap_subentry(
            entry_email=entry_email,
            subentry_email=subentry_email,
            subentry_name=subentry_name,
            organization_id=organization_id,
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(
    ("entry_email", "organization_id"),
    [
        ("wendy@entry.com", "okada"),
    ],
)
async def test_delete_mailmap_entry_subentries(
    entry_email: str,
    organization_id: str,
) -> None:
    await delete_mailmap_entry_subentries(
        entry_email=entry_email,
        organization_id=organization_id,
    )
    assert isinstance(entry_email, str)
    # verification
    entry_with_subentries = await get_mailmap_entry_with_subentries(
        entry_email=entry_email,
        organization_id=organization_id,
    )
    subentries = entry_with_subentries["subentries"]
    assert len(subentries) == 1
    subentry = subentries[0]
    subentry_email = subentry["mailmap_subentry_email"]
    assert entry_email == subentry_email
    entry = entry_with_subentries["entry"]
    entry_name = entry["mailmap_entry_name"]
    subentry_name = subentry["mailmap_subentry_name"]
    assert entry_name == subentry_name


@pytest.mark.asyncio
@pytest.mark.parametrize(
    ("entry_email", "organization_id"),
    [
        ("not-found@entry.com", "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"),
    ],
)
async def test_delete_mailmap_entry_subentries_exception(
    entry_email: str,
    organization_id: str,
) -> None:
    with pytest.raises(MailmapEntryNotFound):
        await delete_mailmap_entry_subentries(
            entry_email=entry_email,
            organization_id=organization_id,
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(
    ("entry_email", "organization_id"),
    [
        ("mallory@entry.com", "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"),
    ],
)
async def test_delete_mailmap_entry_with_subentries(
    entry_email: str,
    organization_id: str,
) -> None:
    await delete_mailmap_entry_with_subentries(
        entry_email=entry_email,
        organization_id=organization_id,
    )
    # verification
    with pytest.raises(MailmapEntryNotFound):
        await get_mailmap_entry(
            entry_email=entry_email,
            organization_id=organization_id,
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(
    ("entry_email", "organization_id"),
    [
        ("not-found@entry.com", "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"),
    ],
)
async def test_delete_mailmap_entry_with_subentries_exception(
    entry_email: str,
    organization_id: str,
) -> None:
    with pytest.raises(MailmapEntryNotFound):
        await delete_mailmap_entry_with_subentries(
            entry_email=entry_email,
            organization_id=organization_id,
        )
