from integrates.custom_exceptions import (
    MailmapEntryAlreadyExists,
    MailmapOrganizationNotFound,
)
from integrates.db_model.mailmap.create import (
    create_mailmap_entry,
)
from integrates.db_model.mailmap.types import (
    MailmapEntry,
    MailmapEntryWithSubentries,
    MailmapSubentry,
)
from integrates.mailmap.create import (
    create_mailmap_entry_with_subentries,
    create_mailmap_subentry,
)
from integrates.mailmap.get import (
    get_mailmap_entry_with_subentries,
    get_mailmap_subentry,
)
import pytest


@pytest.mark.asyncio
@pytest.mark.parametrize(
    ("entry", "organization_id", "_subentry"),
    [
        (
            MailmapEntry(
                mailmap_entry_name="John Doe",
                mailmap_entry_email="john@entry.com",
            ),
            "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
            MailmapSubentry(
                mailmap_subentry_name="John Doe",
                mailmap_subentry_email="john@entry.com",
            ),
        ),
    ],
)
async def test_create_mailmap_entry(
    entry: MailmapEntry,
    organization_id: str,
    _subentry: MailmapSubentry,
) -> None:
    entry_with_subentries = await create_mailmap_entry(
        entry=entry,
        organization_id=organization_id,
    )
    # verification
    _entry = entry_with_subentries["entry"]
    assert entry == _entry
    subentry = entry_with_subentries["subentries"][0]
    assert subentry == _subentry


@pytest.mark.asyncio
@pytest.mark.parametrize(
    ("subentry", "entry_email", "organization_id"),
    [
        (
            MailmapSubentry(
                mailmap_subentry_name="John Doe",
                mailmap_subentry_email="john@subentry.com",
            ),
            "john@entry.com",
            "okada",
        ),
    ],
)
async def test_create_mailmap_subentry(
    subentry: MailmapSubentry,
    entry_email: str,
    organization_id: str,
) -> None:
    await create_mailmap_subentry(
        subentry=subentry,
        entry_email=entry_email,
        organization_id=organization_id,
    )
    # verification
    subentry_email = subentry["mailmap_subentry_email"]
    subentry_name = subentry["mailmap_subentry_name"]
    _subentry = await get_mailmap_subentry(
        entry_email=entry_email,
        subentry_email=subentry_email,
        subentry_name=subentry_name,
        organization_id=organization_id,
    )
    assert subentry == _subentry


@pytest.mark.asyncio
@pytest.mark.parametrize(
    ("entry_with_subentries", "organization_id"),
    [
        (
            MailmapEntryWithSubentries(
                entry=MailmapEntry(
                    mailmap_entry_name="Jane Doe",
                    mailmap_entry_email="jane@entry.com",
                ),
                subentries=[
                    MailmapSubentry(
                        mailmap_subentry_name="Jane Doe",
                        mailmap_subentry_email="jane@subentry.com",
                    ),
                    MailmapSubentry(
                        mailmap_subentry_name="Jane K Doe",
                        mailmap_subentry_email="jane-k@subentry.com",
                    ),
                ],
            ),
            "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
        ),
        (
            MailmapEntryWithSubentries(
                entry=MailmapEntry(
                    mailmap_entry_name="Walter Moreno",
                    mailmap_entry_email="walter@entry.com",
                ),
                subentries=[
                    MailmapSubentry(
                        mailmap_subentry_name="Walter Moreno",
                        mailmap_subentry_email="walter@subentry.com",
                    ),
                    MailmapSubentry(
                        mailmap_subentry_name="Walter X Moreno",
                        mailmap_subentry_email="walter-x@subentry.com",
                    ),
                ],
            ),
            "okada",
        ),
    ],
)
async def test_create_mailmap_entry_with_subentries(
    entry_with_subentries: MailmapEntryWithSubentries,
    organization_id: str,
) -> None:
    await create_mailmap_entry_with_subentries(
        entry_with_subentries=entry_with_subentries,
        organization_id=organization_id,
    )
    # verification
    _entry_email = entry_with_subentries["entry"]["mailmap_entry_email"]
    _entry_with_subentries = await get_mailmap_entry_with_subentries(
        entry_email=_entry_email,
        organization_id=organization_id,
    )
    entry = entry_with_subentries["entry"]
    _entry = _entry_with_subentries["entry"]
    assert entry == _entry
    subentries = entry_with_subentries["subentries"]
    _subentries = _entry_with_subentries["subentries"]
    assert all(map(lambda subentry: subentry in _subentries, subentries))


@pytest.mark.asyncio
@pytest.mark.parametrize(
    ("entry_with_subentries", "organization_id"),
    [
        (
            MailmapEntryWithSubentries(
                entry=MailmapEntry(
                    mailmap_entry_name="Jane Doe",
                    mailmap_entry_email="jane@entry.com",
                ),
                subentries=[
                    MailmapSubentry(
                        mailmap_subentry_name="Jane Doe",
                        mailmap_subentry_email="jane@subentry.com",
                    ),
                    MailmapSubentry(
                        mailmap_subentry_name="Jane K Doe",
                        mailmap_subentry_email="jane-k@subentry.com",
                    ),
                ],
            ),
            "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
        ),
    ],
)
async def test_create_mailmap_entry_with_subentries_exception(
    entry_with_subentries: MailmapEntryWithSubentries,
    organization_id: str,
) -> None:
    with pytest.raises(MailmapEntryAlreadyExists):
        await create_mailmap_entry_with_subentries(
            entry_with_subentries=entry_with_subentries,
            organization_id=organization_id,
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(
    ("entry_with_subentries", "organization_id"),
    [
        (
            MailmapEntryWithSubentries(
                entry=MailmapEntry(
                    mailmap_entry_name="Jane Doe",
                    mailmap_entry_email="jane-new@entry.com",
                ),
                subentries=[
                    MailmapSubentry(
                        mailmap_subentry_name="Jane Doe",
                        mailmap_subentry_email="jane@subentry.com",
                    ),
                ],
            ),
            "ORG#not-found",
        ),
    ],
)
async def test_create_mailmap_entry_with_subentries_org_exception(
    entry_with_subentries: MailmapEntryWithSubentries,
    organization_id: str,
) -> None:
    with pytest.raises(MailmapOrganizationNotFound):
        await create_mailmap_entry_with_subentries(
            entry_with_subentries=entry_with_subentries,
            organization_id=organization_id,
        )
