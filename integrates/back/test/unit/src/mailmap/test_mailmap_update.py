from integrates.custom_exceptions import (
    MailmapEntryNotFound,
    MailmapSubentryNotFound,
)
from integrates.db_model.mailmap.types import (
    MailmapEntry,
    MailmapEntryWithSubentries,
    MailmapSubentry,
)
from integrates.mailmap.get import (
    get_mailmap_entry,
    get_mailmap_entry_subentries,
    get_mailmap_entry_with_subentries,
    get_mailmap_subentry,
)
from integrates.mailmap.update import (
    convert_mailmap_subentries_to_new_entry,
    move_mailmap_entry_with_subentries,
    set_mailmap_entry_as_mailmap_subentry,
    set_mailmap_subentry_as_mailmap_entry,
    update_mailmap_entry,
    update_mailmap_subentry,
)
import pytest


@pytest.mark.asyncio
@pytest.mark.parametrize(
    ("entry_email", "entry", "organization_id"),
    [
        (
            "grace@entry.com",
            MailmapEntry(
                mailmap_entry_name="Grace Musa [NEW]",
                mailmap_entry_email="grace-new@entry.com",
            ),
            "okada",
        ),
    ],
)
async def test_update_mailmap_entry(
    entry_email: str,
    entry: MailmapEntry,
    organization_id: str,
) -> None:
    await update_mailmap_entry(
        entry_email=entry_email,
        entry=entry,
        organization_id=organization_id,
    )
    # verification
    _entry_email = entry["mailmap_entry_email"]
    _entry = await get_mailmap_entry(
        entry_email=_entry_email,
        organization_id=organization_id,
    )
    assert entry == _entry


@pytest.mark.asyncio
@pytest.mark.parametrize(
    ("entry_email", "entry", "organization_id"),
    [
        (
            "not-found@entry.com",
            MailmapEntry(
                mailmap_entry_name="Alice Garcia [NEW]",
                mailmap_entry_email="alice-new@entry.com",
            ),
            "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
        ),
    ],
)
async def test_update_mailmap_entry_exception(
    entry_email: str,
    entry: MailmapEntry,
    organization_id: str,
) -> None:
    with pytest.raises(MailmapEntryNotFound):
        await update_mailmap_entry(
            entry_email=entry_email,
            entry=entry,
            organization_id=organization_id,
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(
    (
        "subentry",
        "entry_email",
        "subentry_email",
        "subentry_name",
        "organization_id",
    ),
    [
        (
            MailmapSubentry(
                mailmap_subentry_name="Grace Musa [NEW]",
                mailmap_subentry_email="grace-new@subentry.com",
            ),
            "grace-new@entry.com",
            "grace@subentry.com",
            "Grace Musa",
            "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
        ),
    ],
)
async def test_update_mailmap_subentry(
    subentry: MailmapSubentry,
    entry_email: str,
    subentry_email: str,
    subentry_name: str,
    organization_id: str,
) -> None:
    await update_mailmap_subentry(
        subentry=subentry,
        entry_email=entry_email,
        subentry_email=subentry_email,
        subentry_name=subentry_name,
        organization_id=organization_id,
    )
    # verification
    _subentry_email = subentry["mailmap_subentry_email"]
    _subentry_name = subentry["mailmap_subentry_name"]
    _subentry = await get_mailmap_subentry(
        entry_email=entry_email,
        subentry_email=_subentry_email,
        subentry_name=_subentry_name,
        organization_id=organization_id,
    )
    assert subentry == _subentry


@pytest.mark.asyncio
@pytest.mark.parametrize(
    (
        "subentry",
        "entry_email",
        "subentry_email",
        "subentry_name",
        "organization_id",
    ),
    [
        (
            MailmapSubentry(
                mailmap_subentry_name="Alice Garcia [NEW]",
                mailmap_subentry_email="alice-new@subentry.com",
            ),
            "not-found@entry.com",
            "alice@subentry.com",
            "Alice Garcia",
            "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
        ),
    ],
)
async def test_update_mailmap_subentry_exception_a(
    subentry: MailmapSubentry,
    entry_email: str,
    subentry_email: str,
    subentry_name: str,
    organization_id: str,
) -> None:
    with pytest.raises(MailmapEntryNotFound):
        await update_mailmap_subentry(
            subentry=subentry,
            entry_email=entry_email,
            subentry_email=subentry_email,
            subentry_name=subentry_name,
            organization_id=organization_id,
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(
    (
        "subentry",
        "entry_email",
        "subentry_email",
        "subentry_name",
        "organization_id",
    ),
    [
        (
            MailmapSubentry(
                mailmap_subentry_name="Eve Anderson [NEW]",
                mailmap_subentry_email="eve[NEW]@subentry.com",
            ),
            "eve@entry.com",
            "not-found@subentry.com",
            "Eve Anderson",
            "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
        ),
    ],
)
async def test_update_mailmap_subentry_exception_b(
    subentry: MailmapSubentry,
    entry_email: str,
    subentry_email: str,
    subentry_name: str,
    organization_id: str,
) -> None:
    with pytest.raises(MailmapSubentryNotFound):
        await update_mailmap_subentry(
            subentry=subentry,
            entry_email=entry_email,
            subentry_email=subentry_email,
            subentry_name=subentry_name,
            organization_id=organization_id,
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(
    ("entry_email", "subentry_email", "subentry_name", "organization_id"),
    [
        (
            "trent@entry.com",
            "trent@subentry.com",
            "Trent Hernandez",
            "okada",
        ),
    ],
)
async def test_set_mailmap_subentry_as_mailmap_entry(
    entry_email: str,
    subentry_email: str,
    subentry_name: str,
    organization_id: str,
) -> None:
    # preparation for verification
    old_subentry = await get_mailmap_subentry(
        entry_email=entry_email,
        subentry_email=subentry_email,
        subentry_name=subentry_name,
        organization_id=organization_id,
    )
    old_entry = await get_mailmap_entry(
        entry_email=entry_email,
        organization_id=organization_id,
    )
    # execution
    new_entry = await set_mailmap_subentry_as_mailmap_entry(
        entry_email=entry_email,
        subentry_email=subentry_email,
        subentry_name=subentry_name,
        organization_id=organization_id,
    )
    # verification
    new_entry_email = new_entry["mailmap_entry_email"]
    old_subentry_email = old_subentry["mailmap_subentry_email"]
    assert new_entry_email == old_subentry_email
    new_entry_name = new_entry["mailmap_entry_name"]
    old_subentry_name = old_subentry["mailmap_subentry_name"]
    assert new_entry_name == old_subentry_name
    subentries = await get_mailmap_entry_subentries(
        entry_email=new_entry_email,
        organization_id=organization_id,
    )
    old_entry_email = old_entry["mailmap_entry_email"]
    assert old_entry_email in list(
        map(
            lambda subentry: subentry["mailmap_subentry_email"],
            subentries,
        )
    )
    old_entry_name = old_entry["mailmap_entry_name"]
    assert old_entry_name in list(
        map(
            lambda subentry: subentry["mailmap_subentry_name"],
            subentries,
        )
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(
    ("entry_email", "subentry_email", "subentry_name", "organization_id"),
    [
        (
            "not-found@@entry.com",
            "alice@subentry.com",
            "Alice Garcia",
            "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
        ),
    ],
)
async def test_set_mailmap_subentry_as_mailmap_entry_exception_a(
    entry_email: str,
    subentry_email: str,
    subentry_name: str,
    organization_id: str,
) -> None:
    with pytest.raises(MailmapEntryNotFound):
        await set_mailmap_subentry_as_mailmap_entry(
            entry_email=entry_email,
            subentry_email=subentry_email,
            subentry_name=subentry_name,
            organization_id=organization_id,
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(
    ("entry_email", "subentry_email", "subentry_name", "organization_id"),
    [
        (
            "eve@entry.com",
            "not-found@subentry.com",
            "Eve Anderson",
            "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
        ),
    ],
)
async def test_set_mailmap_subentry_as_mailmap_entry_exception_b(
    entry_email: str,
    subentry_email: str,
    subentry_name: str,
    organization_id: str,
) -> None:
    with pytest.raises(MailmapSubentryNotFound):
        await set_mailmap_subentry_as_mailmap_entry(
            entry_email=entry_email,
            subentry_email=subentry_email,
            subentry_name=subentry_name,
            organization_id=organization_id,
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(
    ("entry_email", "target_entry_email", "organization_id"),
    [
        (
            "wendy@entry.com",
            "eve@entry.com",
            "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
        ),
    ],
)
async def test_set_mailmap_entry_as_mailmap_subentry(
    entry_email: str,
    target_entry_email: str,
    organization_id: str,
) -> None:
    # preparation for verification
    old_entry_with_subentries = await get_mailmap_entry_with_subentries(
        entry_email=entry_email,
        organization_id=organization_id,
    )
    old_entry = old_entry_with_subentries["entry"]
    old_subentries = old_entry_with_subentries["subentries"]
    # execution
    await set_mailmap_entry_as_mailmap_subentry(
        entry_email=entry_email,
        target_entry_email=target_entry_email,
        organization_id=organization_id,
    )
    # verification
    new_target_entry_subentries = await get_mailmap_entry_subentries(
        entry_email=target_entry_email,
        organization_id=organization_id,
    )
    # check for old entry to be a subentry of target entry
    new_subentry = MailmapSubentry(
        mailmap_subentry_email=old_entry["mailmap_entry_email"],
        mailmap_subentry_name=old_entry["mailmap_entry_name"],
    )
    assert new_subentry in new_target_entry_subentries
    # check for old entry subentries to be subentries of target entry
    assert all(
        map(
            lambda old_subentry: old_subentry in new_target_entry_subentries,
            old_subentries,
        )
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(
    ("entry_email", "target_entry_email", "organization_id"),
    [
        (
            "not-found@entry.com",
            "eve@entry.com",
            "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
        ),
        (
            "eve@entry.com",
            "not-found@entry.com",
            "okada",
        ),
    ],
)
async def test_set_mailmap_entry_as_mailmap_subentry_exception(
    entry_email: str,
    target_entry_email: str,
    organization_id: str,
) -> None:
    with pytest.raises(MailmapEntryNotFound):
        await set_mailmap_entry_as_mailmap_subentry(
            entry_email=entry_email,
            target_entry_email=target_entry_email,
            organization_id=organization_id,
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(
    ("entry_email", "organization_id", "new_organization_id"),
    [
        (
            "walter@entry.com",
            "okada",
            "imamura",
        ),
    ],
)
async def test_move_mailmap_entry_with_subentries(
    entry_email: str,
    organization_id: str,
    new_organization_id: str,
) -> None:
    # preparation for verification
    old_entry_with_subentries = await get_mailmap_entry_with_subentries(
        entry_email=entry_email,
        organization_id=organization_id,
    )

    # execution
    await move_mailmap_entry_with_subentries(
        entry_email=entry_email,
        organization_id=organization_id,
        new_organization_id=new_organization_id,
    )

    # verification
    new_entry_with_subentries = await get_mailmap_entry_with_subentries(
        entry_email=entry_email,
        organization_id=new_organization_id,
    )

    assert old_entry_with_subentries == new_entry_with_subentries

    with pytest.raises(MailmapEntryNotFound):
        await get_mailmap_entry_with_subentries(
            entry_email=entry_email,
            organization_id=organization_id,
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(
    (
        "entry_email",
        "main_subentry",
        "other_subentries",
        "organization_id",
        "_new_entry_with_subentries",
    ),
    [
        (
            "eve@entry.com",
            MailmapSubentry(
                mailmap_subentry_email="wendy@entry.com",
                mailmap_subentry_name="Wendy Ozcan",
            ),
            [
                MailmapSubentry(
                    mailmap_subentry_email="eve-f@subentry.com",
                    mailmap_subentry_name="Eve F Anderson",
                ),
                MailmapSubentry(
                    mailmap_subentry_email="eve@subentry.com",
                    mailmap_subentry_name="Eve Anderson",
                ),
            ],
            "okada",
            MailmapEntryWithSubentries(
                entry=MailmapEntry(
                    mailmap_entry_email="wendy@entry.com",
                    mailmap_entry_name="Wendy Ozcan",
                ),
                subentries=[
                    MailmapSubentry(
                        mailmap_subentry_email="eve-f@subentry.com",
                        mailmap_subentry_name="Eve F Anderson",
                    ),
                    MailmapSubentry(
                        mailmap_subentry_email="eve@subentry.com",
                        mailmap_subentry_name="Eve Anderson",
                    ),
                ],
            ),
        ),
    ],
)
async def test_convert_mailmap_subentries_to_new_entry(
    entry_email: str,
    main_subentry: MailmapSubentry,
    other_subentries: list[MailmapSubentry],
    organization_id: str,
    _new_entry_with_subentries: MailmapEntryWithSubentries,
) -> None:
    await convert_mailmap_subentries_to_new_entry(
        entry_email=entry_email,
        main_subentry=main_subentry,
        other_subentries=other_subentries,
        organization_id=organization_id,
    )
    # verification
    new_entry_with_subentries = await get_mailmap_entry_with_subentries(
        entry_email=main_subentry["mailmap_subentry_email"],
        organization_id=organization_id,
    )
    assert new_entry_with_subentries == _new_entry_with_subentries

    source_entry_with_subentries = await get_mailmap_entry_with_subentries(
        entry_email=entry_email,
        organization_id=organization_id,
    )

    # The next check does not apply if:
    # 1. The source entry is equal to one of the moved subentries, and
    # 2. All subentries were moved, creating a new subentry equal to the
    # source entry.
    # The test data being used does not match these conditions, so it is
    # acceptable to use the check below.
    source_entry_subentries = source_entry_with_subentries["subentries"]
    assert main_subentry not in source_entry_subentries
    assert all(
        map(
            lambda subentry: subentry not in source_entry_subentries,
            other_subentries,
        )
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(
    (
        "entry_email",
        "main_subentry",
        "other_subentries",
        "organization_id",
        "_new_entry_with_subentries",
    ),
    [
        (
            "wendy@entry.com",
            MailmapSubentry(
                mailmap_subentry_email="eve-f@subentry.com",
                mailmap_subentry_name="Eve F Anderson",
            ),
            [],
            "okada",
            MailmapEntryWithSubentries(
                entry=MailmapEntry(
                    mailmap_entry_email="eve-f@subentry.com",
                    mailmap_entry_name="Eve F Anderson",
                ),
                subentries=[
                    MailmapSubentry(
                        mailmap_subentry_email="eve-f@subentry.com",
                        mailmap_subentry_name="Eve F Anderson",
                    ),
                ],
            ),
        ),
    ],
)
async def test_convert_mailmap_subentries_to_new_entry_empty_other_subentries(
    entry_email: str,
    main_subentry: MailmapSubentry,
    other_subentries: list[MailmapSubentry],
    organization_id: str,
    _new_entry_with_subentries: MailmapEntryWithSubentries,
) -> None:
    await convert_mailmap_subentries_to_new_entry(
        entry_email=entry_email,
        main_subentry=main_subentry,
        other_subentries=other_subentries,
        organization_id=organization_id,
    )
    # verification
    new_entry_with_subentries = await get_mailmap_entry_with_subentries(
        entry_email=main_subentry["mailmap_subentry_email"],
        organization_id=organization_id,
    )
    assert new_entry_with_subentries == _new_entry_with_subentries

    source_entry_with_subentries = await get_mailmap_entry_with_subentries(
        entry_email=entry_email,
        organization_id=organization_id,
    )

    # The next check does not apply if:
    # 1. The source entry is equal to one of the moved subentries, and
    # 2. All subentries were moved, creating a new subentry equal to the
    # source entry.
    # The test data being used does not match these conditions, so it is
    # acceptable to use the check below.
    source_entry_subentries = source_entry_with_subentries["subentries"]
    assert main_subentry not in source_entry_subentries
    assert all(
        map(
            lambda subentry: subentry not in source_entry_subentries,
            other_subentries,
        )
    )
