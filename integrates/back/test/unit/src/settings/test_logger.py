import datetime
import decimal
import logging
from unittest.mock import (
    MagicMock,
    Mock,
    patch,
)

import pytest
from graphql import (
    GraphQLError,
)

from integrates.custom_exceptions import (
    DocumentNotFound,
    UnavailabilityError,
)
from integrates.settings.logger import (
    AsyncStreamHandler,
    CustomJsonFormatter,
    customize_bugsnag_error_reports,
    json_default,
)
from test.unit.src.utils import (
    get_module_at_test,
)

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"


logging.config.dictConfig(
    {
        "version": 1,
        "formatters": {
            "json": {
                "format": "%(timestamp)s %(level)s %(message)s",
                "()": CustomJsonFormatter,
            },
        },
        "handlers": {
            "console": {
                "()": AsyncStreamHandler,
                "level": "INFO",
                "formatter": "json",
            },
        },
        "loggers": {
            "": {
                "handlers": ["console"],
                "level": "INFO",
                "propagate": True,
            },
        },
    }
)

TEST_LOGGER = logging.getLogger("")


@patch(MODULE_AT_TEST + "bugsnag_remove_nix_hash", Mock(side_effect=None))
def test_customize_bugsnag_error_reports_returns_false() -> None:
    notification = MagicMock()
    attrs = {
        "exception": GraphQLError("Testing"),
        "grouping_hash": "testing",
        "errors": [MagicMock()],
    }
    notification.configure_mock(**attrs)
    assert not customize_bugsnag_error_reports(notification)


@pytest.mark.parametrize(
    "error",
    [
        DocumentNotFound(),
        UnavailabilityError({}),
    ],
)
@patch(MODULE_AT_TEST + "bugsnag_remove_nix_hash", Mock(side_effect=None))
def test_customize_bugsnag_error_reports_returns_true(
    error: Exception,
) -> None:
    notification = MagicMock()
    original_error = MagicMock()
    original_error.configure_mock(
        stacktrace=[{"file": "integrates/back/migration/0399_fix_queue_state.py"}]
    )
    attrs = {
        "exception": error,
        "grouping_hash": "testing",
        "errors": [original_error],
    }
    notification.configure_mock(**attrs)
    assert customize_bugsnag_error_reports(notification)


@patch(MODULE_AT_TEST + "bugsnag_remove_nix_hash", Mock(side_effect=None))
def test_customize_bugsnag_error_migrations_report() -> None:
    notification = MagicMock()
    original_error = MagicMock()
    original_error.configure_mock(
        stacktrace=[{"file": "integrates/back/migrations/0399_fix_queue_state.py"}]
    )
    attrs = {
        "exception": Exception({"Error": "from migration"}),
        "grouping_hash": "testing",
        "errors": [original_error],
        "unhandled": True,
        "severity": "error",
    }
    notification.configure_mock(**attrs)
    assert customize_bugsnag_error_reports(notification)
    assert not notification.unhandled
    assert notification.severity == "info"


def test_json_message_log(caplog: pytest.LogCaptureFixture) -> None:
    logging.disable(logging.NOTSET)
    with caplog.at_level(logging.INFO):
        TEST_LOGGER.info("Test message", extra={"test": "extra"})
        assert "Test message" in caplog.text
    with caplog.at_level(logging.INFO):
        TEST_LOGGER.info("Test with str timestamp", extra={"timestamp": "121212"})
        assert "Test with str timestamp" in caplog.text
    with caplog.at_level(logging.INFO):
        TEST_LOGGER.info("Test with int timestamp", extra={"timestamp": 121212})
        assert "Test with int timestamp" in caplog.text


@pytest.mark.parametrize(
    "input_value, expected",
    [
        (set([1, 2, 3]), [1, 2, 3]),
        (datetime.datetime(2020, 1, 1), "2020-01-01T00:00:00+00:00"),
        ("string", "string"),
        (1.5, decimal.Decimal("1.5")),
    ],
)
def test_json_default(input_value: object, expected: object) -> None:
    assert json_default(input_value) == expected
