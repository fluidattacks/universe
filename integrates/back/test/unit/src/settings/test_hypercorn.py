from integrates.settings.hypercorn import (
    include_server_header,
)


def test_constants() -> None:
    assert not include_server_header
