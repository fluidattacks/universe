from datetime import (
    datetime,
    timedelta,
)
from integrates.custom_exceptions import (
    ExpiredToken,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.sessions.domain import (
    encode_token,
)
from integrates.sessions.utils import (
    calculate_hash_token,
)
from integrates.settings import (
    SESSION_COOKIE_AGE,
)
import pytest
from typing import (
    Any,
    Callable,
    Generator,
)

MOCKED_DATA: dict[str, dict[str, Any]] = {
    "integrates.sessions.domain.decode_token": {
        "api_token": {
            "user_email": "unitTest@fluidattacks.com",
            "sub": "api_token",
        },
        "starlette_session": {
            "user_email": "UNITTEST@fluidattacks.com",
            "sub": "starlette_session",
        },
        "test_get_jwt_content_catches_ExpiredToken_Error": ExpiredToken(),
    },
    "integrates.sessions.domain.verify_session_token": {
        "api_token": None,
        "starlette_session": None,
    },
}


@pytest.fixture(scope="session")
def mocked_data_for_module() -> dict[str, dict[str, Any]]:
    return MOCKED_DATA


@pytest.fixture
def create_token() -> (
    Callable[[str | None], Generator[str | dict[str, str], None, None]]
):
    def _create_token(
        subject: str | None = None,
    ) -> Generator[str | dict[str, str], None, None]:
        expiration_time: int = datetime_utils.get_as_epoch(
            datetime.utcnow() + timedelta(seconds=SESSION_COOKIE_AGE)
        )
        payload: dict[str, str] = {
            "user_email": "unittest@fluidattacks.com",
            "jti": calculate_hash_token()["jti"],
        }
        token: str = encode_token(
            expiration_time=expiration_time,
            payload=payload,
            subject=subject if subject else "",
        )
        yield token
        yield payload

    return _create_token
