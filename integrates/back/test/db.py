from aioextensions import (
    collect,
)
import asyncio
from collections.abc import (
    Awaitable,
)
from datetime import (
    datetime,
)
import hashlib
from integrates import (
    authz,
)
from integrates.batch import (
    dal as batch_dal,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    compliance as compliance_model,
    credentials as creds_model,
    event_comments as event_comments_model,
    events as events_model,
    finding_comments as finding_comments_model,
    findings as findings_model,
    forces as forces_model,
    group_access as group_access_model,
    groups as groups_model,
    hook as hook_model,
    mailmap as mailmap_model,
    marketplace as marketplace_model,
    organization_access as org_access_model,
    organization_finding_policies as policies_model,
    organizations as orgs_model,
    portfolios as portfolios_model,
    roots as roots_model,
    stakeholders as stakeholders_model,
    toe_inputs as toe_inputs_model,
    toe_lines as toe_lines_model,
    toe_packages as toe_packages_model,
    toe_ports as toe_ports_model,
    trials as trials_model,
    vulnerabilities as vulns_model,
)
from integrates.db_model.credentials.types import (
    Credentials,
)
from integrates.db_model.events.types import (
    Event,
)
from integrates.db_model.findings.enums import (
    FindingStateStatus,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.db_model.group_access.types import (
    GroupAccess,
    GroupAccessMetadataToUpdate,
    GroupAccessState,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.db_model.integration_repositories.types import (
    OrganizationIntegrationRepository,
)
from integrates.db_model.integration_repositories.update import (
    update_unreliable_repositories,
)
from integrates.db_model.mailmap.types import (
    MailmapEntryWithSubentries,
)
from integrates.db_model.marketplace.types import (
    AWSMarketplaceSubscription,
)
from integrates.db_model.organization_access.types import (
    OrganizationAccess,
    OrganizationAccessMetadataToUpdate,
)
from integrates.db_model.organization_finding_policies.types import (
    OrgFindingPolicy,
)
from integrates.db_model.organizations.types import (
    Organization,
)
from integrates.db_model.roots.types import (
    GitRoot,
    IPRoot,
    Root,
    Secret,
    URLRoot,
)
from integrates.db_model.stakeholders.types import (
    NotificationsPreferences,
    Stakeholder,
    StakeholderMetadataToUpdate,
    StakeholderState,
    TrustedDevice,
)
from integrates.db_model.toe_inputs.types import (
    ToeInput,
    ToeInputUpdate,
)
from integrates.db_model.toe_lines.types import (
    ToeLine,
)
from integrates.db_model.toe_packages.add import (
    add_docker_image_package,
)
from integrates.db_model.toe_packages.types import (
    ToePackage,
    ToePackageVulnerability,
)
from integrates.db_model.toe_ports.types import (
    ToePort,
)
from integrates.db_model.trials.types import (
    Trial,
)
from integrates.db_model.types import (
    PoliciesToUpdate,
)
from typing import (
    Any,
    Iterator,
)


async def populate_stakeholders(data: list[Stakeholder]) -> bool:
    await collect(
        stakeholders_model.update_metadata(
            email=item.email,
            metadata=StakeholderMetadataToUpdate(
                access_tokens=item.access_tokens,
                aws_customer_id=item.aws_customer_id,
                enrolled=item.enrolled,
                first_name=item.first_name,
                is_concurrent_session=item.is_concurrent_session,
                is_registered=item.is_registered,
                last_login_date=item.last_login_date,
                last_name=item.last_name,
                legal_remember=item.legal_remember,
                phone=item.phone,
                registration_date=item.registration_date,
                role=item.role,
                tours=item.tours,
            ),
        )
        for item in data
    )

    await collect(
        stakeholders_model.update_state(
            user_email=item.email,
            state=StakeholderState(
                modified_by=item.email,
                modified_date=datetime.fromisoformat(
                    "2022-10-21T15:58:31.280182+00:00"
                ),
                notifications_preferences=NotificationsPreferences(
                    email=[
                        "ACCESS_GRANTED",
                        "AGENT_TOKEN",
                        "EVENT_DIGEST",
                        "EVENT_REPORT",
                        "FILE_UPDATE",
                        "GROUP_INFORMATION",
                        "GROUP_REPORT",
                        "NEWSLETTER",
                        "NEW_COMMENT",
                        "NEW_DRAFT",
                        "PORTFOLIO_UPDATE",
                        "REMEDIATE_FINDING",
                        "REMINDER_NOTIFICATION",
                        "ROOT_UPDATE",
                        "SERVICE_UPDATE",
                        "UNSUBSCRIPTION_ALERT",
                        "UPDATED_TREATMENT",
                        "VULNERABILITY_ASSIGNED",
                        "VULNERABILITY_REPORT",
                    ]
                ),
                trusted_devices=[
                    TrustedDevice(
                        browser="chrome",
                        device="ubuntu",
                        first_login_date="2024-02-10T10:55:12+00:00",
                        ip_address="192.168.1.1",
                        last_attempt="2024-02-14T10:55:12+00:00",
                        last_login_date="2024-02-14T10:55:12+00:00",
                        location="Toronto",
                        otp_token_jti="xvc123",
                    ),
                    TrustedDevice(
                        browser="safari",
                        device="mac",
                        first_login_date="2024-02-11T10:55:12+00:00",
                        ip_address="192.168.1.1",
                        last_attempt="2024-02-12T10:55:12+00:00",
                        last_login_date="2024-02-12T10:55:12+00:00",
                        location="Toronto",
                        otp_token_jti="xvc123",
                    ),
                ],
            ),
        )
        for item in data
    )
    return True


async def populate_organization_access(data: list[OrganizationAccess]) -> bool:
    await collect(
        org_access_model.update_metadata(
            email=item.email,
            organization_id=item.organization_id,
            metadata=OrganizationAccessMetadataToUpdate(state=item.state),
        )
        for item in data
    )
    return True


async def _populate_organization_unreliable_indicators(
    data: dict[str, Any]
) -> None:
    organization: Organization = data["organization"]
    if data.get("unreliable_indicators"):
        await orgs_model.update_unreliable_indicators(
            organization_id=organization.id,
            organization_name=organization.name,
            indicators=data["unreliable_indicators"],
        )


async def populate_organizations(data: list[dict[str, Any]]) -> bool:
    await collect(
        orgs_model.add(
            organization=item["organization"],
        )
        for item in data
    )
    await collect(
        [_populate_organization_unreliable_indicators(item) for item in data]
    )
    return True


async def _populate_environment_secret(
    group_name: str, url: str, environment_secrets: Iterator[Secret]
) -> None:
    # NOFLUID SHA1 is used to generate a unique identifier
    url_id = hashlib.sha1(url.encode()).hexdigest()
    await collect(
        (
            roots_model.add_root_environment_secret(
                group_name, url_id, environment_secret
            )
            for environment_secret in environment_secrets
        )
    )


async def _populate_group_environment_secrets(
    data: list[dict[str, Any]]
) -> None:
    group: Group = data["group"]
    if environment_secrets := data.get("environment_secrets"):
        await collect(
            (
                _populate_environment_secret(group.name, url, secrets)
                for url, secrets in environment_secrets.items()
            )
        )


async def _populate_group_policies(data: dict[str, Any]) -> None:
    group: Group = data["group"]
    if data.get("policies") and group.policies:
        await groups_model.update_policies(
            group_name=group.name,
            modified_by=group.policies.modified_by,
            modified_date=group.policies.modified_date,
            organization_id=group.organization_id,
            policies=PoliciesToUpdate(
                max_acceptance_days=(group.policies.max_acceptance_days),
                max_acceptance_severity=(
                    group.policies.max_acceptance_severity
                ),
                max_number_acceptances=(group.policies.max_number_acceptances),
            ),
        )


async def _populate_group_unreliable_indicators(data: dict[str, Any]) -> None:
    group: Group = data["group"]
    if data.get("unreliable_indicators"):
        await groups_model.update_unreliable_indicators(
            group_name=group.name,
            indicators=data["unreliable_indicators"],
        )


async def _populate_group_historic_state(data: dict[str, Any]) -> None:
    group: Group = data["group"]
    historic = data.get("historic_state", [])
    for state in historic:
        await groups_model.update_state(
            group_name=group.name,
            organization_id=group.organization_id,
            state=state,
        )


async def populate_groups(data: list[dict[str, Any]]) -> bool:
    await collect(
        groups_model.add(
            group=item["group"],
        )
        for item in data
    )
    await collect([_populate_group_historic_state(item) for item in data])
    await collect(
        [_populate_group_unreliable_indicators(item) for item in data]
    )
    await collect(
        tuple(_populate_group_policies(item) for item in data),
        workers=16,
    )
    await collect([_populate_group_environment_secrets(item) for item in data])

    return True


async def populate_group_access(data: list[GroupAccess]) -> bool:
    await collect(
        group_access_model.update_metadata(
            email=item.email,
            group_name=item.group_name,
            metadata=GroupAccessMetadataToUpdate(state=item.state),
        )
        for item in data
    )
    return True


async def populate_organization_unreliable_integration_repository(
    data: tuple[OrganizationIntegrationRepository, ...],
) -> bool:
    await collect(
        tuple(
            update_unreliable_repositories(
                repository=repository,
            )
            for repository in data
        ),
        workers=4,
    )
    return True


async def _populate_finding_unreliable_indicator(data: dict[str, Any]) -> None:
    finding = data["finding"]
    if data.get("unreliable_indicator"):
        await findings_model.update_unreliable_indicators(
            current_value=(
                await get_new_context().finding.load(finding.id)
            ).unreliable_indicators,
            group_name=finding.group_name,
            finding_id=finding.id,
            indicators=data["unreliable_indicator"],
        )


async def _populate_finding_historic_state(data: dict[str, Any]) -> None:
    # Update the finding state sequentially is important to
    # not generate a race condition
    finding: Finding = data["finding"]
    historic = (finding.state, *data["historic_state"])
    for previous, current in zip(historic, historic[1:]):
        await findings_model.update_state(
            current_value=previous,
            group_name=finding.group_name,
            finding_id=finding.id,
            state=current,
        )


async def _populate_finding_historic_verification(
    data: dict[str, Any]
) -> None:
    # Update the finding verification sequentially is important to
    # not generate a race condition
    finding: Finding = data["finding"]
    historic = (finding.verification, *data["historic_verification"])
    for previous, current in zip(historic, historic[1:]):
        await findings_model.update_verification(
            current_value=previous,
            group_name=finding.group_name,
            finding_id=finding.id,
            verification=current,
        )


async def _populate_root_historic_state(data: dict[str, Any]) -> None:
    root: Root = data["root"]
    historic = (root.state, *data["historic_state"])
    for previous, current in zip(historic, historic[1:]):
        await roots_model.update_root_state(
            current_value=previous,
            group_name=root.group_name,
            root_id=root.id,
            state=current,
        )


async def populate_findings(data: list[dict[str, Any]]) -> bool:
    await collect(
        [findings_model.add(finding=item["finding"]) for item in data]
    )
    await collect([_populate_finding_historic_state(item) for item in data])
    await collect(
        [_populate_finding_historic_verification(item) for item in data]
    )
    await asyncio.sleep(10)
    await collect(
        [_populate_finding_unreliable_indicator(item) for item in data]
    )
    await collect(
        [
            findings_model.remove(
                created_by=item["finding"].creation.modified_by
                if item["finding"].creation
                else item["finding"].state.modified_by,
                finding_code=item["finding"].get_criteria_code(),
                finding_id=item["finding"].id,
                group_name=item["finding"].group_name,
            )
            for item in data
            if item["historic_state"]
            and item["historic_state"][-1].status == FindingStateStatus.DELETED
        ]
    )
    return True


async def populate_vulnerabilities(data: list[dict[str, Any]]) -> bool:
    await collect(
        [
            vulns_model.add(vulnerability=vulnerability["vulnerability"])
            for vulnerability in data
        ]
    )
    vuln_ids = [item["vulnerability"].id for item in data]
    loaders = get_new_context()
    current_vulnerabilities = await loaders.vulnerability.load_many(vuln_ids)
    await collect(
        [
            vulns_model.update_historic(
                current_value=current_value,
                historic=vulnerability["historic_state"],
            )
            for current_value, vulnerability in zip(
                current_vulnerabilities, data
            )
            if current_value and "historic_state" in vulnerability
        ]
    )
    loaders = get_new_context()
    current_vulnerabilities = await loaders.vulnerability.load_many(vuln_ids)
    await collect(
        [
            vulns_model.update_new_historic(
                current_value=current_value,
                historic=vulnerability["historic_state"],
            )
            for current_value, vulnerability in zip(
                current_vulnerabilities, data
            )
            if current_value and "historic_state" in vulnerability
        ]
    )
    loaders = get_new_context()
    current_vulnerabilities = await loaders.vulnerability.load_many(vuln_ids)
    await collect(
        [
            vulns_model.update_historic(
                current_value=current_value,
                historic=vulnerability["historic_treatment"],
            )
            for current_value, vulnerability in zip(
                current_vulnerabilities, data
            )
            if current_value and "historic_treatment" in vulnerability
        ]
    )
    loaders = get_new_context()
    current_vulnerabilities = await loaders.vulnerability.load_many(vuln_ids)
    await collect(
        [
            vulns_model.update_new_historic(
                current_value=current_value,
                historic=vulnerability["historic_treatment"],
            )
            for current_value, vulnerability in zip(
                current_vulnerabilities, data
            )
            if current_value and "historic_treatment" in vulnerability
        ]
    )
    loaders = get_new_context()
    current_vulnerabilities = await loaders.vulnerability.load_many(vuln_ids)
    await collect(
        [
            vulns_model.update_historic(
                current_value=current_value,
                historic=vulnerability["historic_verification"],
            )
            for current_value, vulnerability in zip(
                current_vulnerabilities, data
            )
            if current_value and "historic_verification" in vulnerability
        ]
    )
    loaders = get_new_context()
    current_vulnerabilities = await loaders.vulnerability.load_many(vuln_ids)
    await collect(
        [
            vulns_model.update_new_historic(
                current_value=current_value,
                historic=vulnerability["historic_verification"],
            )
            for current_value, vulnerability in zip(
                current_vulnerabilities, data
            )
            if current_value and "historic_verification" in vulnerability
        ]
    )
    loaders = get_new_context()
    current_vulnerabilities = await loaders.vulnerability.load_many(vuln_ids)
    await collect(
        [
            vulns_model.update_historic(
                current_value=current_value,
                historic=vulnerability["historic_zero_risk"],
            )
            for current_value, vulnerability in zip(
                current_vulnerabilities, data
            )
            if current_value and "historic_zero_risk" in vulnerability
        ]
    )
    loaders = get_new_context()
    current_vulnerabilities = await loaders.vulnerability.load_many(vuln_ids)
    await collect(
        [
            vulns_model.update_new_historic(
                current_value=current_value,
                historic=vulnerability["historic_zero_risk"],
            )
            for current_value, vulnerability in zip(
                current_vulnerabilities, data
            )
            if current_value and "historic_zero_risk" in vulnerability
        ]
    )

    return True


async def _populate_environment_urls(data: dict[str, Any]) -> None:
    await collect(
        [
            roots_model.add_root_environment_url(
                group_name=data["root"].group_name,
                root_id=data["root"].id,
                url=url,
            )
            for url in data.get("git_environment_urls", [])
            if isinstance(data["root"], GitRoot)
        ]
    )
    await collect(
        [
            roots_model.add_root_environment_secret(
                group_name=data["root"].group_name,
                resource_id=url.id,
                secret=secret,
            )
            for url in data.get("git_environment_urls", [])
            for secret in data.get("environment_secrets", [])
            if isinstance(data["root"], GitRoot)
        ]
    )


async def _populate_docker_images(data: dict[str, Any]) -> None:
    await collect(
        [
            roots_model.add_root_docker_image(
                group_name=data["root"].group_name,
                root_id=data["root"].id,
                image=image,
            )
            for image in data.get("root_docker_images", [])
            if isinstance(data["root"], GitRoot)
        ]
    )


async def _populate_docker_image_layers(data: dict[str, Any]) -> None:
    await collect(
        [
            roots_model.add_root_docker_image_layer(
                group_name=data["root"].group_name,
                root_id=data["root"].id,
                layer=layer,
            )
            for layer in data.get("root_docker_image_layers", [])
            if isinstance(data["root"], GitRoot)
        ]
    )


async def populate_roots(data: list[dict[str, Any]]) -> bool:
    await collect(tuple(roots_model.add(root=item["root"]) for item in data))
    await collect([_populate_root_historic_state(item) for item in data])
    await collect([_populate_environment_urls(item) for item in data])
    await collect([_populate_docker_images(item) for item in data])
    await collect([_populate_docker_image_layers(item) for item in data])
    await collect(
        [
            roots_model.add_secret(resource_id=item["root"].id, secret=secret)
            for item in data
            if isinstance(item["root"], GitRoot | IPRoot | URLRoot)
            for secret in item.get("root_secrets", [])
        ]
    )

    return True


async def populate_consultings_event(data: list[dict[str, Any]]) -> bool:
    await collect(
        event_comments_model.add(event_comment=item["event_comment"])
        for item in data
    )
    return True


async def _populate_event_historic_state(data: dict[str, Any]) -> None:
    event: Event = data["event"]
    historic = data.get("historic_state", [])
    current_value = event
    for state in historic:
        await events_model.update_state(
            current_value=current_value,
            group_name=event.group_name,
            state=state,
        )
        current_value = event._replace(state=state)


async def populate_events(data: list[dict[str, Any]]) -> bool:
    await collect(
        events_model.add(
            event=item["event"],
        )
        for item in data
    )
    await collect([_populate_event_historic_state(item) for item in data])
    return True


async def populate_event_comments(data: list[dict[str, Any]]) -> bool:
    await collect(
        event_comments_model.add(
            event_comment=item["event_comment"],
        )
        for item in data
    )
    return True


async def populate_finding_comments(data: list[dict[str, Any]]) -> bool:
    await collect(
        finding_comments_model.add(
            finding_comment=item["finding_comment"],
        )
        for item in data
    )
    return True


async def populate_policies(data: list[dict[str, Any]]) -> bool:
    loaders: Dataloaders = get_new_context()
    await collect(
        [
            authz.grant_user_level_role(
                email=policy["subject"],
                role=policy["role"],
            )
            for policy in data
            if policy["level"] == "user"
        ]
    )
    await collect(
        [
            authz.grant_organization_level_role(
                loaders=loaders,
                email=policy["subject"],
                organization_id=policy["object"],
                role=policy["role"],
                modified_by=policy["subject"],
            )
            for policy in data
            if policy["level"] == "organization"
        ]
    )
    await collect(
        [
            authz.grant_group_level_role(
                loaders=loaders,
                email=policy["subject"],
                group_name=policy["object"],
                role=policy["role"],
                modified_by=policy["subject"],
            )
            for policy in data
            if policy["level"] == "group"
        ]
    )
    await collect(
        [
            group_access_model.update_metadata(
                email=policy["subject"],
                group_name=policy["object"],
                metadata=GroupAccessMetadataToUpdate(
                    state=GroupAccessState(
                        modified_date=datetime_utils.get_utc_now(),
                        has_access=True,
                        role=policy["role"],
                    ),
                ),
            )
            for policy in data
            if policy["level"] == "group"
        ]
    )

    return True


async def populate_organization_finding_policies(
    data: tuple[OrgFindingPolicy, ...]
) -> bool:
    await collect(
        policies_model.add(policy=finding_policy) for finding_policy in data
    )

    return True


async def populate_executions(data: list[dict[str, Any]]) -> bool:
    await collect(
        forces_model.add(
            forces_execution=item["execution"],
        )
        for item in data
    )
    return True


async def populate_toe_pkgs(data: tuple[ToePackage, ...]) -> bool:
    await collect(
        [
            toe_packages_model.add(toe_package=toe_package)
            for toe_package in data
        ]
    )
    return True


async def populate_toe_pkgs_vulnerabilities(
    data: tuple[ToePackageVulnerability, ...]
) -> bool:
    await collect(
        [
            toe_packages_model.add_package_vulnerability(
                toe_package=toe_package
            )
            for toe_package in data
        ]
    )
    return True


async def populate_root_image_toe_pkgs(
    data: tuple[tuple[str, ToePackage | dict[str, str]], ...]
) -> bool:
    await collect(
        [
            add_docker_image_package(uri=uri, toe_package=toe_package)
            for uri, toe_package in data
        ]
    )
    return True


async def populate_toe_packages(data: tuple[ToePackage, ...]) -> bool:
    await collect(
        [
            toe_packages_model.add(toe_package=toe_package)
            for toe_package in data
        ]
    )
    return True


async def populate_toe_inputs(data: tuple[ToeInput, ...]) -> bool:
    await collect(
        [toe_inputs_model.add(toe_input=toe_input) for toe_input in data]
    )
    return True


async def populate_toe_inputs_updates(
    data: tuple[ToeInputUpdate, ...]
) -> bool:
    await collect(
        [
            toe_inputs_model.update_state(update_info=toe_input_update)
            for toe_input_update in data
        ]
    )
    return True


async def populate_toe_lines(data: tuple[ToeLine, ...]) -> bool:
    await collect(
        [toe_lines_model.add(toe_lines=toe_lines) for toe_lines in data]
    )
    return True


async def populate_toe_ports(data: tuple[ToePort, ...]) -> bool:
    await collect(
        [toe_ports_model.add(toe_port=toe_port) for toe_port in data]
    )
    return True


async def populate_credentials(data: tuple[Credentials, ...]) -> bool:
    await collect(
        (creds_model.add(credential=credential)) for credential in data
    )
    return True


async def populate_actions(data: tuple[dict[str, Any], ...]) -> bool:
    return all(
        await collect(
            batch_dal.put_action_to_dynamodb(**action) for action in data
        )
    )


async def populate_compliances(data: list[dict[str, Any]]) -> bool:
    await collect(
        compliance_model.update_unreliable_indicators(
            indicators=compliance["compliance"]["unreliable_indicators"]
        )
        for compliance in data
    )
    return True


async def populate_hook(data: list[dict[str, Any]]) -> bool:
    modified_date = datetime_utils.get_utc_now()

    await collect(
        hook_model.add_hook(
            user_email="admin@fluidattacks.com",
            hook_id=item["hook"].id,
            modified_date=modified_date,
            group_name=item["hook"].group_name,
            hook=item["hook"],
        )
        for item in data
    )
    return True


async def populate_portfolios(data: list[dict[str, Any]]) -> bool:
    await collect(
        portfolios_model.update(portfolio=item["portfolio"]) for item in data
    )
    return True


async def populate_trials(data: list[Trial]) -> bool:
    await collect(trials_model.add(trial=trial) for trial in data)
    return True


async def populate_mailmap(
    data: list[tuple[MailmapEntryWithSubentries, str]],
) -> bool:
    await collect(
        mailmap_model.create_mailmap_entry_with_subentries(
            entry_with_subentries=item[0], organization_id=item[1]
        )
        for item in data
    )
    return True


async def populate_aws_subscriptions(
    data: tuple[AWSMarketplaceSubscription, ...]
) -> bool:
    await collect(
        (
            marketplace_model.add_marketplace_subscription(subscription)
            for subscription in data
        ),
        workers=32,
    )

    return True


async def populate(data: Any) -> bool:
    coroutines: list[Awaitable[bool]] = []
    functions: dict[str, Any] = globals()
    for name, dataset in sorted(data.items(), key=lambda item: item[0]):
        coroutines.append(functions[f"populate_{name}"](dataset))
    results = all(await collect(coroutines))
    # Give OpenSearch some time to replicate
    await asyncio.sleep(30)
    return results
