{ inputs, makeScript, outputs, ... }: {
  jobs."/integrates/back/coverage" = makeScript {
    entrypoint = ''
      aws_login dev 3600
      sops_export_vars common/secrets/dev.yaml CODECOV_TOKEN

      codecov-cli upload-process --flag integrates-back --dir integrates/back
    '';
    name = "integrates-back-coverage";
    searchPaths = {
      bin = [
        inputs.codecov-cli
        inputs.nixpkgs.git
        inputs.nixpkgs.python311Packages.coverage
      ];
      source = [ outputs."/common/utils/aws" outputs."/common/utils/sops" ];
    };
  };
}
