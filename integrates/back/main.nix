{ makeScript, outputs, managePorts, ... }:
makeScript {
  entrypoint = ./entrypoint.sh;
  name = "integrates-back";
  replace = {
    __argCertsDevelopment__ = outputs."/integrates/certs/dev";
    __argIntegratesBackEnv__ = outputs."/integrates/back/env";
  };
  searchPaths.source = [ managePorts ];
}
