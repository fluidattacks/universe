import asyncio
import importlib
import logging
import logging.config
import sys

from integrates.audit import AuditContext, add_audit_context
from integrates.dynamodb.resource import (
    dynamo_shutdown,
    dynamo_startup,
)
from integrates.settings import (
    LOGGING,
)
from integrates.sqs.resource import (
    sqs_shutdown,
    sqs_startup,
)

logging.config.dictConfig(LOGGING)

CLOUDWATCH_SCHEDULERS_LOGGER = logging.getLogger("cw_schedulers")


async def main() -> None:
    module, func = sys.argv[1].rsplit(".", maxsplit=1)
    scheduler_name = module.rsplit(".", maxsplit=1)[-1]
    to_invoke = getattr(importlib.import_module(module), func)

    if module.startswith("integrates.schedulers"):
        add_audit_context(AuditContext(mechanism="SCHEDULER"))
        CLOUDWATCH_SCHEDULERS_LOGGER.info(
            "Starting scheduler",
            extra={
                "extra": {
                    "scheduler_name": scheduler_name,
                },
            },
        )
    await dynamo_startup()
    await sqs_startup()
    try:
        if asyncio.iscoroutinefunction(to_invoke):
            await to_invoke()
        else:
            await asyncio.to_thread(to_invoke)
    except Exception as exc:
        if module.startswith("integrates.schedulers"):
            CLOUDWATCH_SCHEDULERS_LOGGER.error(
                "Finishing scheduler",
                exc_info=exc,
                extra={
                    "extra": {
                        "scheduler_name": scheduler_name,
                        "status": "FAILED",
                    },
                },
            )
        raise
    else:
        if module.startswith("integrates.schedulers"):
            CLOUDWATCH_SCHEDULERS_LOGGER.info(
                "Finishing scheduler",
                extra={
                    "extra": {
                        "scheduler_name": scheduler_name,
                        "status": "SUCCEEDED",
                    },
                },
            )
    finally:
        await dynamo_shutdown()
        await sqs_shutdown()


if __name__ == "__main__":
    asyncio.run(main())
