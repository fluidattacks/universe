from datetime import datetime

from integrates.testing.fakers import DATE_2024
from integrates.testing.utils import freeze_time


def func() -> str:
    return datetime.now().isoformat()


@freeze_time(DATE_2024.isoformat())
def test_neeww() -> None:
    value = func()

    assert value.startswith("2024")


def test_otherrr() -> None:
    value = func()

    assert value.startswith("2025")
