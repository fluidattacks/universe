from typing import (
    Generic,
    NamedTuple,
)

from integrates.class_types.types import Item, TItem

Value = bool | list[str] | str | None
FilterExpression = frozenset[tuple[str, Value]]


class PrimaryKey(NamedTuple):  # Composite key
    partition_key: str
    sort_key: str


class SimpleKey(NamedTuple):
    partition_key: str


class Facet(NamedTuple):
    attrs: tuple[str, ...]
    pk_alias: str
    sk_alias: str


class Index(NamedTuple):
    name: str
    primary_key: PrimaryKey


class Table(NamedTuple):
    facets: dict[str, Facet]
    indexes: dict[str, Index]
    name: str
    primary_key: PrimaryKey


class PageInfo(NamedTuple):
    has_next_page: bool
    end_cursor: str


class QueryResponse(NamedTuple):
    items: tuple[Item, ...]
    page_info: PageInfo


class ItemQueryResponse(Generic[TItem], NamedTuple):
    items: tuple[TItem, ...]
    page_info: PageInfo
