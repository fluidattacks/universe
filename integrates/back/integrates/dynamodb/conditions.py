from boto3.dynamodb.conditions import (
    Attr,
    ConditionBase,
)

from .types import (
    FilterExpression,
    Value,
)


def get_condition(attribute: str, filter_value: Value) -> ConditionBase:
    if isinstance(filter_value, bool):
        return Attr(attribute).not_exists()

    if isinstance(filter_value, list):
        return Attr(attribute).between(filter_value[0], filter_value[1])

    return Attr(attribute).contains(filter_value)


def get_filter_expression(
    filters: FilterExpression | None,
) -> ConditionBase | None:
    if filters is None:
        return None

    filter_expression = None

    for attribute, filter_value in filters:
        if filter_value is not None:
            condition = get_condition(attribute, filter_value)

            if filter_expression is None:
                filter_expression = condition
            else:
                filter_expression &= condition

    return filter_expression
