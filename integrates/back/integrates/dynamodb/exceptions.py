import logging
import logging.config
import sys
from typing import (
    Any,
)

from botocore.exceptions import (
    ClientError,
)

from integrates.settings import (
    LOGGING,
)

# Constants
logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger(__name__)


class DynamoDbBaseException(Exception):
    pass


class ConditionalCheckFailedException(DynamoDbBaseException):
    pass


class UnavailabilityError(DynamoDbBaseException):
    def __init__(self) -> None:
        msg = "Service unavailable, please retry"
        super().__init__(msg)


class ValidationException(DynamoDbBaseException):
    pass


def handle_error(
    *,
    error: ClientError,
    **kwargs: Any,
) -> None:
    code: str = error.response["Error"]["Code"]
    if error.response["Error"]["Code"] == "ConditionalCheckFailedException":
        raise ConditionalCheckFailedException() from error

    # Sending message as argument will allow bugsnag group related errors
    # (see settings.logger)
    if kwargs.get("message"):
        LOGGER.error(kwargs["message"], extra={"extra": {**kwargs, "exception": error}})
    else:
        LOGGER.exception(error, extra={"extra": kwargs})

    custom_exception: Exception | None = getattr(sys.modules[__name__], code, None)
    if custom_exception:
        raise custom_exception from error

    raise UnavailabilityError() from error
