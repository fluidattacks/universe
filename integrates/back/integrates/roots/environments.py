from datetime import (
    datetime,
)

from aioextensions import schedule

from integrates.custom_exceptions import (
    InvalidField,
    InvalidRootType,
)
from integrates.custom_utils import datetime as datetime_utils
from integrates.custom_utils import validations_deco as validation_deco_utils
from integrates.custom_utils.environments_validations import (
    validate_role_status,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model import roots as roots_model
from integrates.db_model.roots.enums import (
    RootEnvironmentCloud,
    RootEnvironmentUrlStateStatus,
    RootEnvironmentUrlType,
)
from integrates.db_model.roots.types import (
    GitRoot,
    RootEnvironmentUrl,
    RootEnvironmentUrlState,
    RootRequest,
    Secret,
    SecretState,
)
from integrates.roots import (
    domain,
    utils,
    validations,
)
from integrates.verify.enums import (
    ResourceType,
)


def _format_env_to_add(
    *,
    group_name: str,
    root_id: str,
    url: str,
    url_type: RootEnvironmentUrlType,
    user_email: str,
    cloud_type: RootEnvironmentCloud | None,
    use_connection: dict[str, bool | None],
    is_production: bool | None,
) -> RootEnvironmentUrl:
    formatted_url = url.strip()
    created_at = datetime_utils.get_utc_now()
    return RootEnvironmentUrl(
        id=domain.format_environment_id(formatted_url),
        created_at=created_at,
        created_by=user_email,
        url=formatted_url,
        root_id=root_id,
        group_name=group_name,
        state=RootEnvironmentUrlState(
            modified_date=created_at,
            modified_by=user_email,
            status=RootEnvironmentUrlStateStatus.CREATED,
            url_type=url_type,
            cloud_name=cloud_type,
            use_egress=use_connection["use_egress"],
            use_vpn=use_connection["use_vpn"],
            use_ztna=use_connection["use_ztna"],
            is_production=is_production,
        ),
    )


@validation_deco_utils.validate_url_deco("url")
async def add_root_environment_url(
    *,
    loaders: Dataloaders,
    group_name: str,
    root_id: str,
    url: str,
    url_type: str,
    user_email: str,
    is_production: bool,
    should_notify: bool = False,
    cloud_type: RootEnvironmentCloud | None = None,
    use_connection: dict[str, bool | None],
    is_moving_environment: bool = False,
) -> bool:
    try:
        _url_type = RootEnvironmentUrlType[url_type]
    except KeyError as exc:
        raise InvalidField("urlType") from exc

    if cloud_type and not is_moving_environment:
        raise InvalidField("cloudType")

    root = await loaders.root.load(RootRequest(group_name, root_id))
    if not isinstance(root, GitRoot):
        raise InvalidRootType()

    environment = _format_env_to_add(
        group_name=group_name,
        root_id=root_id,
        url=url,
        user_email=user_email,
        use_connection=use_connection,
        cloud_type=cloud_type,
        url_type=_url_type,
        is_production=is_production,
    )
    await validations.validate_env_url(loaders, environment)
    result_environment = await roots_model.add_root_environment_url(
        group_name=group_name,
        root_id=root_id,
        url=environment,
    )
    if not result_environment:
        return False

    if should_notify:
        schedule(
            utils.send_mail_environment(
                loaders=loaders,
                modified_date=datetime_utils.get_utc_now(),
                group_name=group_name,
                git_root=root.state.nickname,
                git_root_url=root.state.url,
                urls_added=[environment.url],
                urls_deleted=[],
                user_email=user_email,
                other=None,
                reason=None,
            ),
        )

    return True


def get_required_environment_secrets(
    cloud_type: RootEnvironmentCloud, secrets: dict[str, str]
) -> dict[str, str]:
    try:
        if cloud_type == RootEnvironmentCloud.AZURE:
            return {
                "AZURE_CLIENT_ID": secrets["AZURE_CLIENT_ID"],
                "AZURE_CLIENT_SECRET": secrets["AZURE_CLIENT_SECRET"],
                "AZURE_TENANT_ID": secrets["AZURE_TENANT_ID"],
                "AZURE_SUBSCRIPTION_ID": secrets["AZURE_SUBSCRIPTION_ID"],
            }
        if cloud_type == RootEnvironmentCloud.GCP:
            return {
                "GCP_PRIVATE_KEY": secrets["GCP_PRIVATE_KEY"],
            }
        return {}
    except KeyError as exc:
        raise InvalidField("Invalid Secrets") from exc


@validation_deco_utils.validate_url_deco("url")
async def add_root_environment_cspm(
    *,
    loaders: Dataloaders,
    group_name: str,
    root_id: str,
    url: str,
    user_email: str,
    cloud_type: str,
    secrets: dict[str, str],
    use_connection: dict[str, bool | None] | None = None,
    should_notify: bool = False,
) -> bool:
    url_type = RootEnvironmentUrlType.CSPM
    try:
        _cloud_type = RootEnvironmentCloud[cloud_type]
    except KeyError as exc:
        raise InvalidField("urlCloudType") from exc

    root = await loaders.root.load(RootRequest(group_name, root_id))
    if not isinstance(root, GitRoot):
        raise InvalidRootType()

    required_secrets = get_required_environment_secrets(_cloud_type, secrets)
    if not await validate_role_status(
        loaders, url, _cloud_type.value, group_name, required_secrets
    ):
        raise InvalidField("Invalid CSPM Role")

    environment = _format_env_to_add(
        group_name=group_name,
        root_id=root_id,
        url=url,
        user_email=user_email,
        use_connection=use_connection or {"use_egress": None, "use_vpn": None, "use_ztna": None},
        cloud_type=_cloud_type,
        url_type=url_type,
        is_production=None,
    )
    await validations.validate_env_url_cspm(loaders, environment)
    result_environment = await roots_model.add_root_environment_url(
        group_name=group_name,
        root_id=root_id,
        url=environment,
    )
    if not result_environment:
        return False

    if _cloud_type in {RootEnvironmentCloud.AZURE, RootEnvironmentCloud.GCP}:
        for secret_key, secret_value in required_secrets.items():
            await domain.add_secret(
                loaders=loaders,
                secret=Secret(
                    key=secret_key,
                    value=secret_value,
                    created_at=datetime.now(),
                    state=SecretState(
                        owner=user_email,
                        modified_by=user_email,
                        modified_date=datetime.now(),
                    ),
                ),
                resource_id=environment.id,
                resource_type=ResourceType.URL,
                group_name=group_name,
            )

    if should_notify:
        schedule(
            utils.send_mail_environment(
                loaders=loaders,
                modified_date=datetime_utils.get_utc_now(),
                group_name=group_name,
                git_root=root.state.nickname,
                git_root_url=root.state.url,
                urls_added=[environment.url],
                urls_deleted=[],
                user_email=user_email,
                other=None,
                reason=None,
            ),
        )

    return True
