import asyncio
import base64
import json
import re
import shutil
from dataclasses import (
    dataclass,
    field,
)
from typing import (
    Any,
)

import aioboto3
import botocore

from integrates.custom_exceptions import (
    CredentialNotFound,
    InvalidAWSRoleTrustPolicy,
    RootDockerImageNotFound,
    UriFormatError,
)
from integrates.db_model.credentials.types import (
    AWSRoleSecret,
    Credentials,
    HttpsPatSecret,
    HttpsSecret,
)


@dataclass(frozen=True)
class AwsCredentials:
    access_key_id: str = field(repr=False)
    secret_access_key: str = field(repr=False)
    session_token: str | None = field(repr=False)


@dataclass(frozen=True)
class AwsRole:
    external_id: str | None = field(repr=False)
    role: str = field(repr=False)


DOCKER_IMAGE_REGEX = re.compile(
    r"^(?:(?P<host>[\w\.\-]+(?:\:\d+)?)/)?"
    r"(?P<namespace>(?:[\w\.\-]+(?:/[\w\.\-]+)*)?/)?"
    r"(?P<image>[\w\.\-]+)"
    r"(?::(?P<tag>[\w\.\-]+))?"
    r"(?:@(?P<digest>sha256:[A-Fa-f0-9]{64}))?$",
)


def format_image_ref(image_ref: str) -> str:
    if image_ref.startswith("docker://"):
        image_ref = image_ref.replace("docker://", "", 1)

    if re.match(DOCKER_IMAGE_REGEX, image_ref):
        return f"docker://{image_ref}"

    raise UriFormatError()


async def get_credentials(credentials: AwsRole) -> AwsCredentials | None:
    try:
        if credentials.external_id:
            async with aioboto3.Session().client(service_name="sts") as sts_client:
                response = await sts_client.assume_role(
                    ExternalId=credentials.external_id,
                    RoleArn=credentials.role,
                    RoleSessionName="FluidAttacksRoleVerification",
                )
                aws_credentials = AwsCredentials(
                    access_key_id=response["Credentials"]["AccessKeyId"],
                    secret_access_key=response["Credentials"]["SecretAccessKey"],
                    session_token=response["Credentials"]["SessionToken"],
                )
    except botocore.exceptions.ClientError as err:
        raise InvalidAWSRoleTrustPolicy() from err
    else:
        return aws_credentials


async def run_boto3_fun(
    credentials: AwsCredentials,
    service: str,
    function: str,
    region: str | None = None,
    parameters: dict[str, object] | None = None,
) -> dict[str, dict | list]:
    try:
        session = aioboto3.Session(
            aws_access_key_id=credentials.access_key_id,
            aws_secret_access_key=credentials.secret_access_key,
            aws_session_token=credentials.session_token,
        )
        async with session.client(  # type: ignore
            service_name=service,
            region_name=region,
        ) as client:
            return await getattr(client, function)(**(parameters or {}))
    except botocore.exceptions.ClientError as err:
        raise InvalidAWSRoleTrustPolicy() from err
    return {}


async def get_token(credentials: AwsCredentials) -> str:
    get_authorization_token = await run_boto3_fun(
        credentials=credentials,
        service="ecr",
        function="get_authorization_token",
    )

    auth_resp = get_authorization_token["authorizationData"][0]
    token = auth_resp["authorizationToken"]
    _username, password = base64.b64decode(token).decode("utf-8").split(":")
    return password


async def ecr_connection(
    role: AwsRole,
) -> str:
    credentials = await get_credentials(role)
    token = ""
    aws_creds = None
    if credentials:
        token = await get_token(credentials)
        aws_creds = f"AWS:{token}"

    if not aws_creds:
        raise CredentialNotFound()
    return aws_creds


async def get_image_manifest(
    image_ref: str,
    *,
    credentials: Credentials | None,
    config: bool = False,
    external_id: str | None = None,
) -> dict[str, Any]:
    skopeo_path = shutil.which("skopeo") or "skopeo"
    formated_image_ref = format_image_ref(image_ref or "")

    command_args = [
        "inspect",
        "--override-os=linux",
        "--tls-verify=false",
        formated_image_ref,
    ]
    if config:
        # The config parameter changes the formart of the manifest,
        # the config parameter brings layers with the digest that docker uses
        command_args.append("--config")

    if credentials and isinstance(credentials.secret, HttpsSecret):
        command_args.extend(
            [
                "--username",
                credentials.secret.user,
                "--password",
                credentials.secret.password,
            ],
        )
    elif credentials and isinstance(credentials.secret, HttpsPatSecret):
        command_args.extend(["--registry-token", credentials.secret.token])

    elif credentials and isinstance(credentials.secret, AWSRoleSecret):
        try:
            token = await ecr_connection(
                AwsRole(role=credentials.secret.arn, external_id=external_id)
            )
        except InvalidAWSRoleTrustPolicy as exc:
            raise CredentialNotFound from exc

        command_args.extend(["--creds", token])

    process = await asyncio.subprocess.create_subprocess_exec(
        skopeo_path,
        *command_args,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE,
    )
    stdout, stderr = await process.communicate()
    if process.returncode == 0:
        image_metadata = json.loads(stdout)
        return image_metadata
    std_err = stderr.decode("utf-8")
    if any(
        msg in std_err
        for msg in (
            "invalid username/password",
            "invalid registry token",
            "authentication required",
            "requested access to the resource is denied",
        )
    ):
        raise CredentialNotFound()
    raise RootDockerImageNotFound()
