from collections.abc import Iterable
from datetime import datetime

from integrates.custom_exceptions import (
    InvalidChar,
    InvalidGitCredentials,
    InvalidParameter,
    RepeatedRootEnvironmentUrl,
)
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.credentials.types import HttpsSecret
from integrates.db_model.roots.enums import (
    RootCloningStatus,
    RootEnvironmentCloud,
    RootEnvironmentUrlType,
    RootStatus,
    RootType,
)
from integrates.db_model.roots.types import (
    GitRoot,
    GitRootCloning,
    GitRootState,
    Root,
    RootEnvironmentUrl,
)
from integrates.roots.validations import (
    is_exclude_valid,
    is_git_unique,
    validate_credential_in_organization,
    validate_env_url,
    validate_nickname,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    EMAIL_GENERIC,
    CredentialsFaker,
    GitRootFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    RootEnvironmentUrlFaker,
    RootEnvironmentUrlStateFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises


def _get_roots() -> list[GitRoot]:
    return [
        GitRoot(
            created_by="jdoe@fluidattacks.com",
            created_date=datetime.fromisoformat("2024-01-01T00:00:00Z"),
            group_name="unittesting",
            id="db1fd803-b3b6-4e7e-b957-03e73d2cc09c",
            organization_name="okada",
            state=GitRootState(
                url="https://gitlab.com/fluidattacks/universe.git",
                branch="trunk",
                includes_health_check=False,
                modified_by="jdoe@fluidattacks.com",
                modified_date=datetime.fromisoformat("2024-01-01T00:00:00Z"),
                nickname="universe",
                status=RootStatus.ACTIVE,
            ),
            cloning=GitRootCloning(
                modified_by="jdoe@fluidattacks.com",
                modified_date=datetime.fromisoformat("2024-01-01T00:00:00Z"),
                reason="",
                status=RootCloningStatus.OK,
            ),
            type=RootType.GIT,
        ),
    ]


@parametrize(
    args=["url", "branch", "group_name", "roots", "root_id"],
    cases=[
        [
            "https://gitlab.com/fluidattacks/new.git",
            "trunk",
            "unittesting",
            _get_roots(),
            None,
        ],
    ],
)
async def test_is_git_unique(
    url: str,
    branch: str,
    group_name: str,
    roots: Iterable[Root],
    root_id: str,
) -> None:
    # Act
    result = is_git_unique(
        url=url,
        branch=branch,
        group_name=group_name,
        roots=roots,
        root_id=root_id,
    )

    # Assert
    assert result is True


@parametrize(
    args=["url", "branch", "group_name", "roots", "root_id"],
    cases=[
        [
            "https://gitlab.com/fluidattacks/universe.git",
            "trunk",
            "group_1",
            _get_roots(),
            None,
        ],
        [
            "https://gitlab.com/fluidattacks/universe.git",
            "master",
            "unittesting",
            _get_roots(),
            None,
        ],
    ],
)
async def test_is_git_unique_fail(
    url: str,
    branch: str,
    group_name: str,
    roots: Iterable[Root],
    root_id: str,
) -> None:
    # Act
    result = is_git_unique(
        url=url,
        branch=branch,
        group_name=group_name,
        roots=roots,
        root_id=root_id,
    )

    # Assert
    assert result is False


async def test_validate_nickname() -> None:
    validate_nickname(nickname="valid-username_1")
    # Assert
    with raises(InvalidChar):
        validate_nickname(nickname="invalidusername!")


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db"),
            ],
            credentials=[
                CredentialsFaker(
                    credential_id="test-credential",
                    organization_id="ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db",
                    secret=HttpsSecret(user=EMAIL_GENERIC, password="pass"),  # noqa: S106
                ),
            ],
        ),
    )
)
async def test_validate_credential_in_organization() -> None:
    # Arrange
    loaders: Dataloaders = get_new_context()

    # Act
    await validate_credential_in_organization(
        loaders=loaders,
        credential_id="test-credential",
        organization_id="ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db",
    )

    with raises(InvalidGitCredentials):
        await validate_credential_in_organization(
            loaders=loaders,
            credential_id="test_id",
            organization_id="ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db",
        )


async def test_is_exclude_valid() -> None:
    repo_url: str = "https://fluidattacks.com/universe"
    repo_git: str = "git@gitlab.com:fluidattacks/universe.git"
    assert is_exclude_valid(["*/test.py", "production/test.py", "test/universe/test.py"], repo_url)
    assert is_exclude_valid(["*/test.py", "production/test.py", "test/universe/test.py"], repo_git)
    assert not is_exclude_valid(["Universe/test.py"], repo_url)
    assert not is_exclude_valid(["universe/**/test.py"], repo_url)


@parametrize(
    args=["environment"],
    cases=[
        [
            RootEnvironmentUrlFaker(
                url="https://test.com/",
                root_id="root1",
                state=RootEnvironmentUrlStateFaker(
                    modified_by="admin@fluidattacks.com",
                    url_type=RootEnvironmentUrlType.URL,
                    cloud_name=RootEnvironmentCloud.GCP,
                ),
            )
        ],
        [
            RootEnvironmentUrlFaker(
                url="https://test.com/",
                root_id="root1",
                state=RootEnvironmentUrlStateFaker(
                    modified_by="admin@fluidattacks.com",
                    url_type=RootEnvironmentUrlType.APK,
                    cloud_name=RootEnvironmentCloud.GCP,
                ),
            )
        ],
        [RootEnvironmentUrlFaker(url="https://test.com/test/", root_id="root1")],
        [RootEnvironmentUrlFaker(url="https://test.com/test2", root_id="root1")],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker()],
            group_access=[
                GroupAccessFaker(
                    email="admin@fluidattacks.com",
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
            stakeholders=[StakeholderFaker(email="admin@fluidattacks.com")],
            roots=[GitRootFaker(id="root1")],
            root_environment_urls=[
                RootEnvironmentUrlFaker(
                    url="https://test.com/test",
                    root_id="root1",
                ),
                RootEnvironmentUrlFaker(
                    url="https://test.com/test2/",
                    root_id="root1",
                ),
            ],
        ),
    )
)
async def test_validate_env_url(environment: RootEnvironmentUrl) -> None:
    with raises((InvalidParameter, RepeatedRootEnvironmentUrl)):
        await validate_env_url(loaders=get_new_context(), environment=environment)
