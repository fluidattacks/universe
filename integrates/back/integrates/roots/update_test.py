from datetime import (
    datetime,
)

from integrates.custom_exceptions import (
    CustomBaseException,
    ExcludedEnvironment,
    InvalidChar,
    InvalidGroupService,
    InvalidRoleSetGitIgnore,
    InvalidRootExclusion,
    InvalidRootType,
    PermissionDenied,
    RepeatedRoot,
    RequiredValue,
    RootNicknameUsed,
    RootNotFound,
    SecretValueNotFound,
)
from integrates.custom_utils.groups import get_group
from integrates.custom_utils.roots import get_root, get_root_vulnerabilities
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.enums import Source
from integrates.db_model.roots.enums import RootCriticality
from integrates.db_model.roots.types import (
    GitRoot,
    RootEnvironmentSecretsRequest,
    RootEnvironmentUrlsRequest,
    Secret,
    SecretState,
)
from integrates.db_model.toe_inputs.types import RootToeInputsRequest
from integrates.db_model.vulnerabilities.enums import VulnerabilityStateStatus, VulnerabilityType
from integrates.roots import utils
from integrates.roots.domain import format_environment_id
from integrates.roots.types import RootEnvironmentUrlAttributesToUpdate
from integrates.roots.update import update_git_root, update_root_environment_url, update_secret
from integrates.testing.aws import (
    IntegratesAws,
    IntegratesDynamodb,
    RootEnvironmentSecretsToUpdate,
    RootSecretsToUpdate,
)
from integrates.testing.fakers import (
    FindingFaker,
    GitRootFaker,
    GitRootStateFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    GroupStateFaker,
    OrganizationAccessFaker,
    OrganizationAccessStateFaker,
    OrganizationFaker,
    RootEnvironmentUrlFaker,
    RootEnvironmentUrlStateFaker,
    SecretFaker,
    SecretStateFaker,
    StakeholderFaker,
    ToeInputFaker,
    UrlRootFaker,
    VulnerabilityFaker,
    VulnerabilityStateFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import parametrize, raises
from integrates.verify.enums import ResourceType

ORG_ID = "ORG#6b110f25-cd46-e94c-2bad-b9e182cf91a8"
EMAIL_TEST = "admin@fluidattacks.com"
GROUP_NAME = "group1"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            groups=[
                GroupFaker(
                    name="testgroup",
                    organization_id=ORG_ID,
                ),
            ],
            roots=[
                GitRootFaker(
                    id="root1",
                    group_name="testgroup",
                ),
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(
                    url="https://test.com/",
                    group_name="testgroup",
                    root_id="root1",
                    state=RootEnvironmentUrlStateFaker(modified_by="hacker@fluidattacks.com"),
                ),
                RootEnvironmentUrlFaker(
                    url="https://test.com/test",
                    group_name="testgroup",
                    root_id="root1",
                    state=RootEnvironmentUrlStateFaker(
                        modified_by="hacker@fluidattacks.com", include=False
                    ),
                ),
                RootEnvironmentUrlFaker(
                    url="https://test.com/test/test2",
                    group_name="testgroup",
                    root_id="root1",
                    state=RootEnvironmentUrlStateFaker(
                        modified_by="hacker@fluidattacks.com", include=True
                    ),
                ),
            ],
            toe_inputs=[
                ToeInputFaker(
                    group_name="testgroup",
                    root_id="root1",
                    component="https://test.com/",
                    entry_point="user",
                ),
            ],
            findings=[
                FindingFaker(group_name="testgroup", id="find1"),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    group_name="testgroup",
                    root_id="root1",
                    finding_id="find1",
                    hacker_email="machine@fluidattacks.com",
                    id="vuln1",
                    created_by="machine@fluidattacks.com",
                    type=VulnerabilityType.INPUTS,
                    state=VulnerabilityStateFaker(where="https://test.com/", source=Source.MACHINE),
                ),
            ],
            stakeholders=[
                StakeholderFaker(email="customer_manager@fluidattacks.com", enrolled=True),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email="customer_manager@fluidattacks.com",
                    state=OrganizationAccessStateFaker(has_access=True, role="customer_manager"),
                ),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name="testgroup",
                    email="customer_manager@fluidattacks.com",
                    state=GroupAccessStateFaker(has_access=True, role="customer_manager"),
                ),
            ],
        )
    )
)
async def test_update_git_root_environment_url() -> None:
    # Arrange
    loaders: Dataloaders = get_new_context()
    group = "testgroup"
    root_id = "root1"
    url_id = format_environment_id("https://test.com/")
    attributes = RootEnvironmentUrlAttributesToUpdate(include=False)
    modified_by = "hacker@fluidattacks.com"

    # Act

    await update_root_environment_url(
        loaders=loaders,
        attributes=attributes,
        group_name=group,
        modified_by=modified_by,
        root_id=root_id,
        url_id=url_id,
    )

    # Assert
    env_url = next(
        (
            url
            for url in await loaders.root_environment_urls.load(
                RootEnvironmentUrlsRequest(root_id=root_id, group_name=group)
            )
            if url.id == url_id
        ),
        None,
    )
    assert env_url is not None
    assert env_url.state.include is False

    vulnerabilities = await get_root_vulnerabilities(root_id)
    assert vulnerabilities[0].state.status is VulnerabilityStateStatus.SAFE
    present_toe_inputs = await loaders.root_toe_inputs.load_nodes(
        RootToeInputsRequest(group_name=group, root_id=root_id, be_present=True)
    )
    assert len(present_toe_inputs) == 0
    not_present_toe_inputs = await loaders.root_toe_inputs.load_nodes(
        RootToeInputsRequest(group_name=group, root_id=root_id, be_present=False)
    )
    assert len(not_present_toe_inputs) == 1


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            groups=[
                GroupFaker(
                    name="testgroup",
                    organization_id=ORG_ID,
                ),
            ],
            roots=[
                UrlRootFaker(
                    root_id="root1",
                    group_name="testgroup",
                ),
            ],
            stakeholders=[
                StakeholderFaker(email="customer_manager@fluidattacks.com", enrolled=True),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email="customer_manager@fluidattacks.com",
                    state=OrganizationAccessStateFaker(has_access=True, role="customer_manager"),
                ),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name="testgroup",
                    email="customer_manager@fluidattacks.com",
                    state=GroupAccessStateFaker(has_access=True, role="customer_manager"),
                ),
            ],
        )
    )
)
async def test_update_git_root_environment_url_invalid_root() -> None:
    # Arrange
    loaders: Dataloaders = get_new_context()
    attributes = RootEnvironmentUrlAttributesToUpdate(include=True)
    group_name = "testgroup"
    modified_by = "customer_manager@fluidattacks.com"
    root_id = "root1"
    url_id = format_environment_id("https://test.com/")

    # Act
    with raises(InvalidRootType):
        await update_root_environment_url(
            loaders=loaders,
            attributes=attributes,
            group_name=group_name,
            modified_by=modified_by,
            root_id=root_id,
            url_id=url_id,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            groups=[
                GroupFaker(
                    name="testgroup",
                    organization_id=ORG_ID,
                ),
            ],
            roots=[
                GitRootFaker(
                    id="root1",
                    group_name="testgroup",
                ),
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(
                    url="https://fluidattacks.com/test",
                    group_name="testgroup",
                    root_id="root1",
                    state=RootEnvironmentUrlStateFaker(
                        modified_by="hacker@fluidattacks.com", include=False
                    ),
                ),
                RootEnvironmentUrlFaker(
                    group_name="testgroup",
                    root_id="root1",
                    state=RootEnvironmentUrlStateFaker(
                        modified_by="hacker@fluidattacks.com", include=False
                    ),
                ),
            ],
            stakeholders=[
                StakeholderFaker(email="customer_manager@fluidattacks.com", enrolled=True),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email="customer_manager@fluidattacks.com",
                    state=OrganizationAccessStateFaker(has_access=True, role="customer_manager"),
                ),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name="testgroup",
                    email="customer_manager@fluidattacks.com",
                    state=GroupAccessStateFaker(has_access=True, role="customer_manager"),
                ),
            ],
        )
    )
)
async def test_update_git_root_environment_excluded_env() -> None:
    # Arrange
    loaders: Dataloaders = get_new_context()
    attributes = RootEnvironmentUrlAttributesToUpdate(include=True)
    group_name = "testgroup"
    modified_by = "customer_manager@fluidattacks.com"
    root_id = "root1"
    url_id = format_environment_id("https://fluidattacks.com/test")

    # Act
    with raises(ExcludedEnvironment):
        await update_root_environment_url(
            loaders=loaders,
            attributes=attributes,
            group_name=group_name,
            modified_by=modified_by,
            root_id=root_id,
            url_id=url_id,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            groups=[
                GroupFaker(
                    name="testgroup",
                    organization_id=ORG_ID,
                ),
            ],
            stakeholders=[
                StakeholderFaker(email="customer_manager@fluidattacks.com", enrolled=True),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email="customer_manager@fluidattacks.com",
                    state=OrganizationAccessStateFaker(has_access=True, role="customer_manager"),
                ),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name="testgroup",
                    email="customer_manager@fluidattacks.com",
                    state=GroupAccessStateFaker(has_access=True, role="customer_manager"),
                ),
            ],
        )
    )
)
async def test_update_git_root_environment_url_not_found() -> None:
    # Arrange
    loaders: Dataloaders = get_new_context()
    attributes = RootEnvironmentUrlAttributesToUpdate(include=True)
    group_name = "testgroup"
    modified_by = "customer_manager@fluidattacks.com"
    root_id = "rootfail"
    url_id = format_environment_id("https://fluidattacks.com/test")

    # Act
    with raises(RootNotFound):
        await update_root_environment_url(
            loaders=loaders,
            attributes=attributes,
            group_name=group_name,
            modified_by=modified_by,
            root_id=root_id,
            url_id=url_id,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="admin")],
            groups=[
                GroupFaker(
                    name=GROUP_NAME,
                    organization_id="org1",
                    state=GroupStateFaker(has_advanced=False),
                )
            ],
            roots=[GitRootFaker(id="root1", group_name=GROUP_NAME)],
        )
    )
)
async def test_validate_add_git_root_invalid_group_service() -> None:
    loaders = get_new_context()
    group = await get_group(loaders, GROUP_NAME)
    with raises(InvalidGroupService):
        await update_git_root(
            loaders=loaders,
            user_email=EMAIL_TEST,
            group=group,
            branch="trunk",
            credentials=None,
            criticality=RootCriticality.CRITICAL,
            gitignore=[],
            includes_health_check=True,
            id="root1",
            nickname="universe",
            url="https://gitlab.com/fluidattacks/universe.git",
            use_egress=True,
            use_vpn=False,
            use_ztna=False,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="hacker")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL_TEST, state=GroupAccessStateFaker(has_access=True, role="user")
                )
            ],
            roots=[
                GitRootFaker(
                    id="root1",
                    group_name=GROUP_NAME,
                    state=GitRootStateFaker(
                        branch="trunk", url="https://gitlab.com/fluidattacks/universe.git"
                    ),
                )
            ],
        )
    )
)
async def test_update_git_root_invalid_role_set_gitignore() -> None:
    loaders = get_new_context()
    group = await get_group(loaders, GROUP_NAME)
    with raises(InvalidRoleSetGitIgnore):
        await update_git_root(
            loaders=loaders,
            branch="trunk",
            credentials={},
            gitignore=["**/test.py"],
            includes_health_check=False,
            url="https://gitlab.com/fluidattacks/universe.git",
            id="root1",
            nickname="nickname",
            user_email=EMAIL_TEST,
            group=group,
            use_egress=False,
            use_vpn=False,
            use_ztna=True,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="hacker")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL_TEST, state=GroupAccessStateFaker(has_access=True, role="user")
                )
            ],
            roots=[GitRootFaker(id="root1", group_name=GROUP_NAME)],
        )
    )
)
async def test_update_git_root_permission_denied() -> None:
    loaders = get_new_context()
    group = await get_group(loaders, GROUP_NAME)
    with raises(PermissionDenied):
        await update_git_root(
            loaders=loaders,
            branch="trunk",
            credentials={},
            gitignore=[],
            includes_health_check=False,
            url="https://gitlab.com/fluidattacks/universe.git",
            id="root1",
            nickname="nickname",
            user_email=EMAIL_TEST,
            group=group,
            use_egress=False,
            use_vpn=False,
            use_ztna=True,
        )


@parametrize(
    args=["gitignore"],
    cases=[[["universe/**/test.py"]], [["Universe/test.py"]], [["*"]]],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="admin")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL_TEST, state=GroupAccessStateFaker(has_access=True, role="user")
                )
            ],
            roots=[
                GitRootFaker(
                    id="root1",
                    group_name=GROUP_NAME,
                    state=GitRootStateFaker(
                        branch="trunk", url="https://gitlab.com/fluidattacks/universe.git"
                    ),
                )
            ],
        )
    )
)
async def test_update_git_root_invalid_root_exclusion(gitignore: list[str]) -> None:
    loaders = get_new_context()
    group = await get_group(loaders, GROUP_NAME)
    with raises(InvalidRootExclusion):
        await update_git_root(
            loaders=loaders,
            branch="trunk",
            credentials={},
            gitignore=gitignore,
            includes_health_check=False,
            url="https://gitlab.com/fluidattacks/universe.git",
            id="root1",
            nickname="nickname",
            user_email=EMAIL_TEST,
            group=group,
            use_egress=False,
            use_vpn=False,
            use_ztna=True,
        )


@parametrize(
    args=["nickname", "url", "gitignore", "exception"],
    cases=[
        ["back", "https://gitlab.com/fluidattacks/nickname1", [], RootNicknameUsed()],
        ["back1", "https://gitlab.com/fluidattacks/nickname1  ", [], RepeatedRoot()],
        ["nickname2", "https://gitlab.com/fluidattacks/nickname2", ["   "], RequiredValue()],
        ["nickname2", "https://gitlab.com/fluidattacks/nickname2", ["test\\data\\"], InvalidChar()],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="admin")],
            groups=[
                GroupFaker(
                    name=GROUP_NAME,
                    organization_id="org1",
                    state=GroupStateFaker(has_advanced=False),
                )
            ],
            roots=[
                GitRootFaker(group_name=GROUP_NAME, state=GitRootStateFaker(nickname="back")),
                GitRootFaker(
                    id="root1",
                    group_name=GROUP_NAME,
                    state=GitRootStateFaker(
                        branch="trunk", url="https://gitlab.com/fluidattacks/universe.git"
                    ),
                ),
            ],
        )
    )
)
async def test_update_git_root_repeated(
    nickname: str, url: str, gitignore: list[str], exception: CustomBaseException
) -> None:
    loaders = get_new_context()
    group = await get_group(loaders, GROUP_NAME)
    with raises(CustomBaseException, match=str(exception)):
        await update_git_root(
            id="root1",
            loaders=loaders,
            branch="master",
            credentials={},
            gitignore=gitignore,
            includes_health_check=False,
            url=url,
            nickname=nickname,
            user_email=EMAIL_TEST,
            group=group,
            use_egress=False,
            use_vpn=False,
            use_ztna=True,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="admin")],
            groups=[
                GroupFaker(
                    name=GROUP_NAME,
                    organization_id="org1",
                    state=GroupStateFaker(has_advanced=False),
                )
            ],
            roots=[
                GitRootFaker(
                    id="root1",
                    group_name=GROUP_NAME,
                    state=GitRootStateFaker(
                        branch="trunk",
                        url="https://gitlab.com/fluidattacks/universe.git",
                        criticality=RootCriticality.HIGH,
                    ),
                ),
            ],
        )
    )
)
async def test_update_git_root_criticallity() -> None:
    loaders = get_new_context()
    group = await get_group(loaders, GROUP_NAME)
    await update_git_root(
        id="root1",
        loaders=loaders,
        branch="trunk",
        credentials={},
        includes_health_check=False,
        url="https://gitlab.com/fluidattacks/universe.git",
        user_email=EMAIL_TEST,
        group=group,
        use_egress=False,
        use_vpn=False,
        use_ztna=True,
    )

    root = await get_root(loaders, "root1", GROUP_NAME, clear_loader=True)
    assert isinstance(root, GitRoot)
    assert root.state.url == "https://gitlab.com/fluidattacks/universe.git"
    assert root.state.gitignore == []
    assert root.state.branch == "trunk"
    assert root.state.nickname == "universe"
    assert root.state.criticality == RootCriticality.HIGH
    assert root.state.modified_by == EMAIL_TEST


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="admin")],
            groups=[
                GroupFaker(
                    name=GROUP_NAME,
                    organization_id="org1",
                    state=GroupStateFaker(has_advanced=False),
                )
            ],
            roots=[
                GitRootFaker(group_name=GROUP_NAME, state=GitRootStateFaker(nickname="back")),
                GitRootFaker(
                    id="root1",
                    group_name=GROUP_NAME,
                    state=GitRootStateFaker(
                        branch="trunk",
                        url="ssh://fluidattacks@vs-ssh.visualstudio.com:v3/fluidattacks/Test%20-%20BackEnd",
                    ),
                ),
            ],
        )
    ),
    others=[Mock(utils, "send_mail_updated_root", "async", None)],
)
async def test_update_git_root_quoted() -> None:
    loaders = get_new_context()
    group = await get_group(loaders, GROUP_NAME)
    kwargs = {
        "id": "root1",
        "branch": "trunk",
        "credentials": {},
        "gitignore": ["node_modules/"],
        "includes_health_check": False,
        "url": "ssh://fluidattacks@vs-ssh.visualstudio.com:v3/fluidattacks/Test - BackEnd",
        "use_egress": False,
        "use_vpn": False,
        "criticality": RootCriticality.MEDIUM,
        "use_ztna": True,
    }
    await update_git_root(loaders=loaders, user_email=EMAIL_TEST, group=group, **kwargs)

    root = await get_root(loaders, "root1", GROUP_NAME, clear_loader=True)
    assert isinstance(root, GitRoot)
    assert root.state.url == kwargs["url"]
    assert root.state.gitignore == kwargs["gitignore"]
    assert root.state.branch == kwargs["branch"]
    assert root.state.nickname == "Test_-_BackEnd"
    assert root.state.modified_by == EMAIL_TEST
    assert root.state.criticality == kwargs["criticality"]


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME)],
            root_environment_urls=[
                RootEnvironmentUrlFaker(id="url1", root_id="root1", group_name=GROUP_NAME)
            ],
            root_environment_secrets=[
                RootEnvironmentSecretsToUpdate(
                    group_name=GROUP_NAME,
                    resource_id="url1",
                    secret=SecretFaker(
                        value="key_123456",
                        key="api_key",
                        state=SecretStateFaker(owner=EMAIL_TEST),
                    ),
                ),
            ],
        ),
    )
)
async def test_update_secret_env_url() -> None:
    loaders = get_new_context()
    secrets = await loaders.environment_secrets.load(
        RootEnvironmentSecretsRequest(url_id="url1", group_name=GROUP_NAME)
    )
    assert len(secrets) == 1
    new_value = "key_new_value"
    await update_secret(
        loaders=loaders,
        group_name=GROUP_NAME,
        resource_id="url1",
        resource_type=ResourceType.URL,
        new_secret=Secret(
            key="api_key",
            value=new_value,
            created_at=secrets[0].created_at,
            state=SecretState(
                owner=secrets[0].state.owner,
                description="New secret Description",
                modified_by=EMAIL_TEST,
                modified_date=datetime.now(),
            ),
        ),
    )
    updated_secrets = await get_new_context().environment_secrets.load(
        RootEnvironmentSecretsRequest(url_id="url1", group_name=GROUP_NAME)
    )
    assert len(updated_secrets) == len(secrets)
    user_secret_value = next(
        secret.value for secret in updated_secrets if secret.state.owner == EMAIL_TEST
    )
    assert user_secret_value == new_value


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME)],
            roots=[
                GitRootFaker(
                    id="root_id_1",
                    group_name=GROUP_NAME,
                    state=GitRootStateFaker(
                        branch="trunk", url="https://gitlab.com/fluidattacks/universe.git"
                    ),
                ),
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(id="url1", root_id="root1", group_name=GROUP_NAME)
            ],
            root_secrets=[
                RootSecretsToUpdate(
                    resource_id="root_id_1",
                    secret=SecretFaker(
                        value="key_123456",
                        key="api_key",
                        state=SecretStateFaker(owner=EMAIL_TEST),
                    ),
                ),
            ],
        ),
    )
)
async def test_update_secret_root() -> None:
    loaders = get_new_context()
    secrets = await loaders.root_secrets.load("root_id_1")
    assert len(secrets) == 1
    new_value = "key_new_value"
    await update_secret(
        loaders=loaders,
        group_name=GROUP_NAME,
        resource_id="root_id_1",
        resource_type=ResourceType.ROOT,
        new_secret=Secret(
            key="api_key",
            value=new_value,
            created_at=secrets[0].created_at,
            state=SecretState(
                owner=secrets[0].state.owner,
                description="New secret Description",
                modified_by=EMAIL_TEST,
                modified_date=datetime.now(),
            ),
        ),
    )
    updated_secrets = await get_new_context().root_secrets.load("root_id_1")
    assert len(updated_secrets) == len(secrets)
    user_secret_value = next(
        secret.value for secret in updated_secrets if secret.state.owner == EMAIL_TEST
    )
    assert user_secret_value == new_value


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME)],
            roots=[
                GitRootFaker(
                    id="root_id_1",
                    group_name=GROUP_NAME,
                    state=GitRootStateFaker(
                        branch="trunk", url="https://gitlab.com/fluidattacks/universe.git"
                    ),
                ),
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(id="url1", root_id="root1", group_name=GROUP_NAME)
            ],
            root_secrets=[
                RootSecretsToUpdate(
                    resource_id="root_id_1",
                    secret=SecretFaker(
                        value="key_123456",
                        key="api_key",
                        state=SecretStateFaker(owner=EMAIL_TEST),
                    ),
                ),
            ],
        ),
    )
)
async def test_update_secret_root_invalid_secret() -> None:
    loaders = get_new_context()
    secrets = await loaders.root_secrets.load("root_id_1")
    assert len(secrets) == 1
    new_value = "key_new_value"
    with raises(SecretValueNotFound):
        await update_secret(
            loaders=loaders,
            group_name=GROUP_NAME,
            resource_id="root_id_1",
            resource_type=ResourceType.ROOT,
            new_secret=Secret(
                key="invalid_secret_key",
                value=new_value,
                created_at=secrets[0].created_at,
                state=SecretState(
                    owner=secrets[0].state.owner,
                    description="New secret Description",
                    modified_by=EMAIL_TEST,
                    modified_date=datetime.now(),
                ),
            ),
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME)],
            root_environment_secrets=[
                RootEnvironmentSecretsToUpdate(
                    group_name=GROUP_NAME,
                    resource_id="url1",
                    secret=SecretFaker(
                        value="key_123456",
                        key="api_key",
                        state=SecretStateFaker(owner=EMAIL_TEST),
                    ),
                ),
            ],
        ),
    )
)
async def test_update_secret_env_url_wrong_resource_id() -> None:
    loaders = get_new_context()
    secrets = await loaders.environment_secrets.load(
        RootEnvironmentSecretsRequest(url_id="url1", group_name=GROUP_NAME)
    )
    assert len(secrets) == 1
    new_value = "key_new_value"
    with raises(InvalidRootType):
        await update_secret(
            loaders=loaders,
            group_name=GROUP_NAME,
            resource_id="url1",
            resource_type=ResourceType.URL,
            new_secret=Secret(
                key="api_key",
                value=new_value,
                created_at=secrets[0].created_at,
                state=SecretState(
                    owner=secrets[0].state.owner,
                    description="New secret Description",
                    modified_by=EMAIL_TEST,
                    modified_date=datetime.now(),
                ),
            ),
        )
