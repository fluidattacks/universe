import asyncio
import contextlib
import os
from collections.abc import AsyncIterator
from urllib.parse import quote_plus as percent_encode

import aiofiles
from aioextensions import in_thread, run
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, TimeoutException, WebDriverException
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.firefox.service import Service
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait

from integrates.charts import charts_utils
from integrates.custom_utils.encodings import safe_encode
from integrates.decorators import retry_on_exceptions

# Environment
GECKO = os.environ["ENV_GECKO_DRIVER"]
FIREFOX = os.environ["ENV_FIREFOX"]

# Finding bugs?
DEBUGGING: bool = False

# Constants
TARGET_URL: str = "https://app.fluidattacks.com"
INTEGRATES_API_TOKEN: str = os.environ["INTEGRATES_API_TOKEN"]
PROXY = "http://127.0.0.1:9000" if DEBUGGING else None
COOKIE_MESSAGE = "Allow all cookies"
WIDTH = 1300
TIMEOUT = 40


@contextlib.asynccontextmanager
async def selenium_web_driver(height: int) -> AsyncIterator[webdriver.Firefox]:
    def create() -> webdriver.Firefox:
        options = Options()
        options.add_argument(f"--width={WIDTH}")
        options.add_argument(f"--height={height}")
        options.add_argument("--disable-gpu")
        options.add_argument("--auto-open-devtools-for-tabs")
        options.add_argument("-headless")
        options.binary_location = f"{FIREFOX}/bin/firefox"
        service = Service(f"{GECKO}/bin/geckodriver")

        driver = webdriver.Firefox(options=options, service=service)

        return driver

    # Exception: WF(AsyncIterator is subtype of iterator)
    yield await in_thread(create)


@retry_on_exceptions(
    exceptions=(
        NoSuchElementException,
        TimeoutException,
        WebDriverException,
    ),
    max_attempts=10,
    sleep_seconds=float("1.0"),
)
async def take_snapshot(
    *,
    driver: webdriver.Firefox,
    save_as: str,
    url: str,
) -> None:
    driver.get(TARGET_URL)
    driver.add_cookie({"name": "integrates_session", "value": INTEGRATES_API_TOKEN})
    await asyncio.sleep(1)

    with contextlib.suppress(NoSuchElementException):
        if driver.find_element(
            By.XPATH,
            f"//*[text()[contains(., '{COOKIE_MESSAGE}')]]",
        ):
            allow_cookies = WebDriverWait(driver, TIMEOUT).until(
                ec.presence_of_element_located(
                    (
                        By.XPATH,
                        f"//*[text()[contains(., '{COOKIE_MESSAGE}')]]",
                    )
                )
            )
            allow_cookies.click()
            await asyncio.sleep(2)

    await asyncio.sleep(1)
    driver.get(url)
    await asyncio.sleep(10)
    if not WebDriverWait(driver, TIMEOUT).until(
        ec.presence_of_element_located(
            (
                By.ID,
                "root",
            )
        )
    ):
        raise TimeoutException()

    with contextlib.suppress(NoSuchElementException):
        if driver.find_element(
            By.XPATH,
            "//*[text()[contains(., 'Error code')]]",
        ):
            raise TimeoutException()

    if WebDriverWait(driver, TIMEOUT).until(
        ec.presence_of_element_located((By.CLASS_NAME, "items-center"))
    ):
        async with aiofiles.open(save_as, "wb") as file:
            await file.write(driver.get_full_page_screenshot_as_png())


@retry_on_exceptions(
    exceptions=(
        TimeoutException,
        WebDriverException,
    ),
    max_attempts=5,
    sleep_seconds=float("1.0"),
)
async def clear_cookies(driver: webdriver.Firefox) -> None:
    driver.delete_all_cookies()
    await asyncio.sleep(1)


async def main() -> None:
    base: str

    async with selenium_web_driver(11750) as driver:
        # Organization reports
        base = f"{TARGET_URL}/graphics-for-organization?reportMode=true&bgChange=true"
        async for org_id, _, _ in charts_utils.iterate_organizations_and_groups():
            await take_snapshot(
                driver=driver,
                save_as=charts_utils.get_result_path(
                    name=f"organization:{safe_encode(org_id.lower())}.png",
                ),
                url=f"{base}&organization={percent_encode(org_id)}",
            )
            await clear_cookies(driver)

    async with selenium_web_driver(8810) as driver:
        # Group reports
        base = f"{TARGET_URL}/graphics-for-group?reportMode=true&bgChange=true"
        async for group in charts_utils.iterate_groups():
            await take_snapshot(
                driver=driver,
                save_as=charts_utils.get_result_path(
                    name=f"group:{safe_encode(group.lower())}.png",
                ),
                url=f"{base}&group={percent_encode(group)}",
            )
            await clear_cookies(driver)

    async with selenium_web_driver(11750) as driver:
        # Portfolio reports
        base = f"{TARGET_URL}/graphics-for-portfolio?reportMode=true&bgChange=true"
        separator = "PORTFOLIO#"
        async for (
            org_id,
            org_name,
            _,
        ) in charts_utils.iterate_organizations_and_groups():
            for portfolio, _ in await charts_utils.get_portfolios_groups(org_name):
                subject = percent_encode(org_id + separator + portfolio)
                await take_snapshot(
                    driver=driver,
                    save_as=charts_utils.get_result_path(
                        name="portfolio:"
                        + safe_encode(org_id.lower() + separator.lower() + portfolio.lower())
                        + ".png",
                    ),
                    url=f"{base}&portfolio={subject}",
                )
                await clear_cookies(driver)


if __name__ == "__main__":
    run(main())
