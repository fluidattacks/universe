from typing import (
    NamedTuple,
)


# Containers
class _SCALE(NamedTuple):
    more_passive: str
    optional_passive: str
    passive: str
    neutral: str
    aggressive: str
    optional_aggressive: str
    more_aggressive: str


class _TREATMENTSCALE(NamedTuple):
    closed: str
    temporarily_accepted: str
    permanently_accepted: str
    open: str
    in_progress: str
    untreated: str


class _COLORSCALE(NamedTuple):
    informational_high: str
    informational_low: str
    complementary_high: str
    complementary_low: str
    warning_high: str
    warning_low: str
    success_high: str
    success_low: str


# https://coolors.co/33cc99-1e8c7d-2f394b-177e89-fdb022-ed7340-da1e28
RISK = _SCALE(
    more_passive="#33cc99",
    optional_passive="#1e8c7d",
    passive="#2f394b",
    neutral="#177e89",
    aggressive="#fdb022",
    optional_aggressive="#ed7340",
    more_aggressive="#da1e28",
)

# https://coolors.co/fdd25e-fecf49-ffcc33-f8903a-f77d26-f67014-f46201
TREATMENT = _SCALE(
    more_passive="#084c61",
    optional_passive="#fecf49",
    passive="#ffcc33",
    neutral="#f8903a",
    aggressive="#f77d26",
    optional_aggressive="#f67014",
    more_aggressive="#f46201",
)


# https://coolors.co/12B76A-2E90FA-D0D5DD-FE7D0D-FFD562-BF0B1A
TREATMENT_COLORS = _TREATMENTSCALE(
    closed="#12B76A",
    in_progress="#2E90FA",
    untreated="#D0D5DD",
    permanently_accepted="#FE7D0D",
    temporarily_accepted="#FFD562",
    open="#BF0B1A",
)

# https://coolors.co/d0d5dd-97a8a5-839794-6f8683-657e7b-607a77-161b25
OTHER = _SCALE(
    more_passive="#d0d5dd",
    optional_passive="#97a8a5",
    passive="#839794",
    neutral="#6f8683",
    aggressive="#657e7b",
    optional_aggressive="#607a77",
    more_aggressive="#161b25",
)

# https://coolors.co/a6f4c5-59042d-b2ddff-dc6803-b8075d-027a48-ffd562-175cd3
VULNS_COLORS = _COLORSCALE(
    success_low="#a6f4c5",
    complementary_high="#59042d",
    informational_low="#b2ddff",
    warning_high="#dc6803",
    complementary_low="#b8075d",
    success_high="#027a48",
    warning_low="#ffd562",
    informational_high="#175cd3",
)

GRAY_JET: str = "#323031"
EXPOSURE: str = "#ac0a17"
VULNERABILITIES_COUNT: str = "#f32637"
TYPES_COUNT: str = "#7f0540"
OTHER_COUNT: str = "#fda6ab"
