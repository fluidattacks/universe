from collections import (
    Counter,
    defaultdict,
)
from decimal import (
    Decimal,
)

from aioextensions import (
    collect,
    run,
)
from async_lru import (
    alru_cache,
)

from integrates.charts import charts_utils
from integrates.charts.generators.stacked_bar_chart.util_class import (
    AssignedFormatted,
)
from integrates.charts.generators.stacked_bar_chart.utils.utils import (
    format_stacked_vulnerabilities_data,
    get_assigned_vulnerabilities,
    get_vulnerabilities_for_group,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.enums import (
    TreatmentStatus,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
)


@alru_cache(maxsize=None, typed=True)
async def get_data_one_group(group: str) -> dict[str, list[Vulnerability]]:
    loaders: Dataloaders = get_new_context()
    vulnerabilities = await get_vulnerabilities_for_group(group, loaders)

    return get_assigned_vulnerabilities(vulnerabilities)


async def get_data_many_groups(
    groups: tuple[str, ...],
) -> dict[str, list[Vulnerability]]:
    groups_data: tuple[dict[str, list[Vulnerability]], ...] = await collect(
        map(get_data_one_group, groups), workers=32
    )
    assigned: dict[str, list[Vulnerability]] = defaultdict(list)

    for group in groups_data:
        for user, vulnerabilities in group.items():
            assigned[user].extend(vulnerabilities)

    return assigned


def format_assigned(user: str, vulnerabilities: list[Vulnerability]) -> AssignedFormatted:
    status: Counter[str] = Counter(vulnerability.state.status for vulnerability in vulnerabilities)

    treatment: Counter[str] = Counter(
        vulnerability.treatment.status
        for vulnerability in vulnerabilities
        if vulnerability.state.status == VulnerabilityStateStatus.VULNERABLE
        and vulnerability.treatment
        and vulnerability.treatment.status
        in {
            TreatmentStatus.ACCEPTED,
            TreatmentStatus.ACCEPTED_UNDEFINED,
        }
    )

    remaining_open = Decimal(
        status[VulnerabilityStateStatus.VULNERABLE]
        - treatment[TreatmentStatus.ACCEPTED_UNDEFINED]
        - treatment[TreatmentStatus.ACCEPTED]
    )

    return AssignedFormatted(
        accepted=Decimal(treatment[TreatmentStatus.ACCEPTED]),
        accepted_undefined=Decimal(treatment[TreatmentStatus.ACCEPTED_UNDEFINED]),
        closed_vulnerabilities=Decimal(status[VulnerabilityStateStatus.SAFE]),
        open_vulnerabilities=Decimal(status[VulnerabilityStateStatus.VULNERABLE]),
        remaining_open_vulnerabilities=max(Decimal("0.0"), remaining_open),
        name=user,
    )


def format_data(assigned_data: dict[str, list[Vulnerability]]) -> tuple[dict, charts_utils.CsvData]:
    data: tuple[AssignedFormatted, ...] = tuple(
        format_assigned(user, vulnerabilities) for user, vulnerabilities in assigned_data.items()
    )
    sorted_data = sorted(
        data,
        key=lambda x: (
            x.open_vulnerabilities / (x.closed_vulnerabilities + x.open_vulnerabilities)
            if (x.closed_vulnerabilities + x.open_vulnerabilities) > 0
            else 0
        ),
        reverse=True,
    )

    return format_stacked_vulnerabilities_data(all_data=sorted_data, header="User")


async def generate_all() -> None:
    async for group in charts_utils.iterate_groups():
        json_document, csv_document = format_data(assigned_data=await get_data_one_group(group))
        charts_utils.json_dump(
            document=json_document,
            entity="group",
            subject=group,
            csv_document=csv_document,
        )

    async for org_id, _, org_groups in charts_utils.iterate_organizations_and_groups():
        json_document, csv_document = format_data(
            assigned_data=await get_data_many_groups(org_groups)
        )
        charts_utils.json_dump(
            document=json_document,
            entity="organization",
            subject=org_id,
            csv_document=csv_document,
        )

    async for org_id, org_name, _ in charts_utils.iterate_organizations_and_groups():
        for portfolio, groups in await charts_utils.get_portfolios_groups(org_name):
            json_document, csv_document = format_data(
                assigned_data=await get_data_many_groups(groups)
            )
            charts_utils.json_dump(
                document=json_document,
                entity="portfolio",
                subject=f"{org_id}PORTFOLIO#{portfolio}",
                csv_document=csv_document,
            )


def main() -> None:
    run(generate_all())
