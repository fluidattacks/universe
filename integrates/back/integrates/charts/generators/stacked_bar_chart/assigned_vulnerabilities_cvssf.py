from collections import (
    Counter,
    defaultdict,
)
from decimal import (
    Decimal,
)

from aioextensions import (
    collect,
    run,
)
from async_lru import (
    alru_cache,
)

from integrates.charts.charts_utils import (
    CsvData,
    get_portfolios_groups,
    iterate_groups,
    iterate_organizations_and_groups,
    json_dump,
)
from integrates.charts.generators.stacked_bar_chart.util_class import (
    AssignedFormatted,
)
from integrates.charts.generators.stacked_bar_chart.utils.utils import (
    format_stacked_vulnerabilities_data,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.enums import (
    TreatmentStatus,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
)
from integrates.groups import (
    domain as groups_domain,
)


@alru_cache(maxsize=None, typed=True)
async def get_data_one_group(*, group: str) -> dict[str, list[tuple[Vulnerability, Decimal]]]:
    loaders = get_new_context()
    assigned: dict[str, list[tuple[Vulnerability, Decimal]]] = defaultdict(list)
    vulnerabilities = await groups_domain.get_group_vulns_with_severity(loaders, group)

    for vulnerability in vulnerabilities:
        risk_exposure = (
            vulnerability.severity_score.cvssf_v4
            if vulnerability.severity_score
            else Decimal("0.0")
        )
        if vulnerability.treatment and vulnerability.treatment.assigned:
            assigned[vulnerability.treatment.assigned].append((vulnerability, risk_exposure))

    return assigned


async def get_data_many_groups(
    *, groups: tuple[str, ...]
) -> dict[str, list[tuple[Vulnerability, Decimal]]]:
    groups_data = await collect(
        tuple(get_data_one_group(group=group) for group in groups),
        workers=32,
    )
    assigned: dict[str, list[tuple[Vulnerability, Decimal]]] = defaultdict(list)

    for group in groups_data:
        for user, vulnerabilities in group.items():
            assigned[user].extend(vulnerabilities)

    return assigned


def format_assigned(
    *, user: str, vulnerabilities: list[tuple[Vulnerability, Decimal]]
) -> AssignedFormatted:
    status: Counter[str] = Counter()
    treatment: Counter[str] = Counter()

    for vulnerability, cvssf in vulnerabilities:
        status.update({vulnerability.state.status: cvssf})
        if (
            vulnerability.state.status == VulnerabilityStateStatus.VULNERABLE
            and vulnerability.treatment
            and vulnerability.treatment.status
            in {
                TreatmentStatus.ACCEPTED,
                TreatmentStatus.ACCEPTED_UNDEFINED,
            }
        ):
            treatment.update({vulnerability.treatment.status: cvssf})

    remaining_open: Decimal = Decimal(
        status[VulnerabilityStateStatus.VULNERABLE]
        - treatment[TreatmentStatus.ACCEPTED_UNDEFINED]
        - treatment[TreatmentStatus.ACCEPTED]
    )

    return AssignedFormatted(
        accepted=Decimal(treatment[TreatmentStatus.ACCEPTED]),
        accepted_undefined=Decimal(treatment[TreatmentStatus.ACCEPTED_UNDEFINED]),
        closed_vulnerabilities=Decimal(status[VulnerabilityStateStatus.SAFE]),
        open_vulnerabilities=Decimal(status[VulnerabilityStateStatus.VULNERABLE]),
        remaining_open_vulnerabilities=max(Decimal("0.0"), remaining_open),
        name=user,
    )


def format_data(
    *, assigned_data: dict[str, list[tuple[Vulnerability, Decimal]]]
) -> tuple[dict, CsvData]:
    data = tuple(
        format_assigned(user=user, vulnerabilities=vulnerabilities)
        for user, vulnerabilities in assigned_data.items()
    )
    sorted_data = sorted(
        data,
        key=lambda x: (
            x.open_vulnerabilities / (x.closed_vulnerabilities + x.open_vulnerabilities)
            if (x.closed_vulnerabilities + x.open_vulnerabilities) > 0
            else 0
        ),
        reverse=True,
    )
    return format_stacked_vulnerabilities_data(all_data=sorted_data, header="User")


async def generate_all() -> None:
    async for group in iterate_groups():
        json_document, csv_document = format_data(
            assigned_data=await get_data_one_group(group=group),
        )
        json_dump(
            document=json_document,
            entity="group",
            subject=group,
            csv_document=csv_document,
        )

    async for org_id, _, org_groups in iterate_organizations_and_groups():
        json_document, csv_document = format_data(
            assigned_data=await get_data_many_groups(groups=org_groups),
        )
        json_dump(
            document=json_document,
            entity="organization",
            subject=org_id,
            csv_document=csv_document,
        )

    async for org_id, org_name, _ in iterate_organizations_and_groups():
        for portfolio, groups in await get_portfolios_groups(org_name):
            json_document, csv_document = format_data(
                assigned_data=await get_data_many_groups(groups=tuple(groups)),
            )
            json_dump(
                document=json_document,
                entity="portfolio",
                subject=f"{org_id}PORTFOLIO#{portfolio}",
                csv_document=csv_document,
            )


def main() -> None:
    run(generate_all())
