from collections import (
    Counter,
)
from decimal import (
    Decimal,
)
from itertools import (
    chain,
)
from statistics import (
    mean,
)
from typing import (
    NamedTuple,
)

from aioextensions import (
    collect,
    run,
)
from async_lru import (
    alru_cache,
)
from more_itertools import (
    chunked,
)

from integrates.charts import charts_utils
from integrates.charts.generators.bar_chart.mttr_benchmarking_cvssf import (
    get_historic_verification,
)
from integrates.charts.generators.bar_chart.utils import (
    ORGANIZATION_CATEGORIES,
    PORTFOLIO_CATEGORIES,
    get_vulnerability_reattacks,
)
from integrates.charts.generators.common.colors import (
    TREATMENT,
    TREATMENT_COLORS,
)
from integrates.charts.generators.common.utils import (
    BAR_RATIO_WIDTH,
)
from integrates.charts.generators.stacked_bar_chart import (
    format_csv_data,
)
from integrates.charts.generators.stacked_bar_chart.util_class import (
    MIN_PERCENTAGE,
)
from integrates.charts.generators.stacked_bar_chart.utils.utils import (
    get_percentage,
)
from integrates.custom_utils import (
    vulnerabilities as vulns_utils,
)
from integrates.custom_utils.findings import (
    get_group_findings,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.enums import (
    TreatmentStatus,
)
from integrates.db_model.roots.enums import (
    RootStatus,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
)

OrganizationsAndPortfolios = list[tuple[str, tuple[str, ...]]]


class OrganizationCvssfBenchmarking(NamedTuple):
    accepted: Decimal
    closed: Decimal
    open: Decimal
    is_valid: bool
    organization_id: str
    total: Decimal


class GroupBenchmarking(NamedTuple):
    counter: Counter[str]
    number_of_roots: int
    number_of_reattacks: int


@alru_cache(maxsize=None, typed=True)
async def get_group_data(group: str) -> GroupBenchmarking:
    loaders = get_new_context()
    group_findings = await get_group_findings(group_name=group, loaders=loaders)
    finding_vulns_loader = loaders.finding_vulnerabilities_released_nzr
    vulnerabilities: tuple[Vulnerability, ...] = tuple(
        chain.from_iterable(
            await collect(
                tuple(
                    finding_vulns_loader.load_many_chained(chunked_findings)
                    for chunked_findings in chunked([finding.id for finding in group_findings], 8)
                ),
                workers=2,
            )
        )
    )
    historics_verification = await collect(
        tuple(
            get_historic_verification(vulnerability)
            for vulnerability in vulnerabilities
            if vulnerability.verification
        ),
        workers=4,
    )

    number_of_reattacks = sum(
        get_vulnerability_reattacks(historic_verification=historic)
        for historic in historics_verification
    )

    number_of_roots = sum(
        1
        for root in await loaders.group_roots.load(group)
        if root.state.status is RootStatus.ACTIVE
    )
    counter: Counter[str] = Counter()
    for vulnerability in vulnerabilities:
        risk_exposure = vulns_utils.get_severity_cvssf_v4(
            vulnerability,
            next(finding for finding in group_findings if finding.id == vulnerability.finding_id),
        )
        counter.update({"total": risk_exposure})
        if vulnerability.state.status == VulnerabilityStateStatus.VULNERABLE:
            if vulnerability.treatment and vulnerability.treatment.status in {
                TreatmentStatus.ACCEPTED,
                TreatmentStatus.ACCEPTED_UNDEFINED,
            }:
                counter.update({"accepted": risk_exposure})
            else:
                counter.update({"open": risk_exposure})
        else:
            counter.update({"closed": risk_exposure})

    return GroupBenchmarking(
        counter=counter,
        number_of_reattacks=number_of_reattacks,
        number_of_roots=number_of_roots,
    )


@alru_cache(maxsize=None, typed=True)
async def get_data_one_organization(
    *, organization_id: str, groups: tuple[str, ...]
) -> OrganizationCvssfBenchmarking:
    groups_data = await collect(
        tuple(get_group_data(group=group.lower()) for group in groups),
        workers=8,
    )

    counter: Counter[str] = sum([group.counter for group in groups_data], Counter())
    number_of_roots = sum(group.number_of_roots for group in groups_data)
    number_of_reattacks = sum(group.number_of_reattacks for group in groups_data)

    return OrganizationCvssfBenchmarking(
        is_valid=number_of_reattacks > 100 and number_of_roots > 0,
        accepted=Decimal(counter["accepted"]).quantize(Decimal("0.1")),
        closed=Decimal(counter["closed"]).quantize(Decimal("0.1")),
        open=Decimal(counter["open"]).quantize(Decimal("0.1")),
        organization_id=organization_id,
        total=Decimal(counter["total"]).quantize(Decimal("0.1")),
    )


def get_best_organization(
    *, all_organizations_data: tuple[OrganizationCvssfBenchmarking, ...]
) -> OrganizationCvssfBenchmarking:
    organizations = tuple(
        organization for organization in all_organizations_data if organization.is_valid
    )
    if organizations:
        return max(
            organizations,
            key=lambda organization: Decimal(organization.closed / organization.total).quantize(
                Decimal("0.0001")
            )
            if organization.total > Decimal("0.0")
            else Decimal("0.0"),
        )

    return OrganizationCvssfBenchmarking(
        accepted=Decimal("0.0"),
        closed=Decimal("1.0"),
        open=Decimal("0.0"),
        is_valid=True,
        organization_id="",
        total=Decimal("1.0"),
    )


def get_worst_organization(
    *, all_organizations_data: tuple[OrganizationCvssfBenchmarking, ...]
) -> OrganizationCvssfBenchmarking:
    organizations = tuple(
        organization for organization in all_organizations_data if organization.is_valid
    )
    if organizations:
        return min(
            organizations,
            key=lambda organization: Decimal(organization.closed / organization.total).quantize(
                Decimal("0.0001")
            )
            if organization.total > Decimal("0.0")
            else Decimal("1.0"),
        )

    return OrganizationCvssfBenchmarking(
        accepted=Decimal("0.0"),
        closed=Decimal("0.0"),
        open=Decimal("1.0"),
        is_valid=True,
        organization_id="",
        total=Decimal("1.0"),
    )


def get_mean_organizations(
    *, organizations: list[OrganizationCvssfBenchmarking]
) -> OrganizationCvssfBenchmarking:
    if organizations:
        accepted = Decimal(
            mean([organization.accepted for organization in organizations])
        ).quantize(Decimal("0.1"))
        opened = Decimal(mean([organization.open for organization in organizations])).quantize(
            Decimal("0.1")
        )
        closed = Decimal(mean([organization.closed for organization in organizations])).quantize(
            Decimal("0.1")
        )

        return OrganizationCvssfBenchmarking(
            accepted=accepted,
            closed=closed,
            open=opened,
            organization_id="",
            is_valid=True,
            total=accepted + closed + opened,
        )

    return OrganizationCvssfBenchmarking(
        accepted=Decimal("0.0"),
        closed=Decimal("0.0"),
        open=Decimal("0.0"),
        organization_id="",
        is_valid=True,
        total=Decimal("0.0"),
    )


def get_valid_organizations(
    *,
    subjects: tuple[OrganizationCvssfBenchmarking, ...],
) -> list[OrganizationCvssfBenchmarking]:
    return [subject for subject in subjects if subject.is_valid]


def format_data(
    *,
    organization: OrganizationCvssfBenchmarking,
    best_cvssf: OrganizationCvssfBenchmarking,
    mean_cvssf: OrganizationCvssfBenchmarking,
    worst_cvssf: OrganizationCvssfBenchmarking,
    categories: list[str],
) -> dict:
    _total_bar = [
        (organization.closed + organization.accepted + organization.open)
        if organization.total > Decimal("0.0")
        else Decimal("0.1"),
        best_cvssf.closed + best_cvssf.accepted + best_cvssf.open,
        (mean_cvssf.closed + mean_cvssf.accepted + mean_cvssf.open)
        if mean_cvssf.total > Decimal("0.0")
        else Decimal("0.1"),
        worst_cvssf.closed + worst_cvssf.accepted + worst_cvssf.open,
    ]
    total_bar = [total if total > Decimal("0.0") else Decimal("0.1") for total in _total_bar]
    percentage_values: list[list[Decimal]] = [
        [
            organization.closed / total_bar[0],
            organization.accepted / total_bar[0],
            organization.open / total_bar[0],
        ],
        [
            best_cvssf.closed / total_bar[1],
            best_cvssf.accepted / total_bar[1],
            best_cvssf.open / total_bar[1],
        ],
        [
            mean_cvssf.closed / total_bar[2],
            mean_cvssf.accepted / total_bar[2],
            mean_cvssf.open / total_bar[2],
        ],
        [
            worst_cvssf.closed / total_bar[3],
            worst_cvssf.accepted / total_bar[3],
            worst_cvssf.open / total_bar[3],
        ],
    ]
    my_organization = get_percentage(percentage_values[0])
    best_organization = get_percentage(percentage_values[1])
    average_organization = get_percentage(percentage_values[2])
    worst_organization = get_percentage(percentage_values[3])

    max_percentage_values = {
        "Closed": [
            my_organization[0] if my_organization[0] >= MIN_PERCENTAGE else "",
            best_organization[0] if best_organization[0] >= MIN_PERCENTAGE else "",
            average_organization[0] if average_organization[0] >= MIN_PERCENTAGE else "",
            worst_organization[0] if worst_organization[0] >= MIN_PERCENTAGE else "",
        ],
        "Accepted": [
            my_organization[1] if my_organization[1] >= MIN_PERCENTAGE else "",
            best_organization[1] if best_organization[1] >= MIN_PERCENTAGE else "",
            average_organization[1] if average_organization[1] >= MIN_PERCENTAGE else "",
            worst_organization[1] if worst_organization[1] >= MIN_PERCENTAGE else "",
        ],
        "Open": [
            my_organization[2] if my_organization[2] >= MIN_PERCENTAGE else "",
            best_organization[2] if best_organization[2] >= MIN_PERCENTAGE else "",
            average_organization[2] if average_organization[2] >= MIN_PERCENTAGE else "",
            worst_organization[2] if worst_organization[2] >= MIN_PERCENTAGE else "",
        ],
    }

    return {
        "data": {
            "columns": [
                [
                    "Closed",
                    organization.closed,
                    best_cvssf.closed,
                    mean_cvssf.closed,
                    worst_cvssf.closed,
                ],
                [
                    "Accepted",
                    organization.accepted,
                    best_cvssf.accepted,
                    mean_cvssf.accepted,
                    worst_cvssf.accepted,
                ],
                [
                    "Open",
                    organization.open,
                    best_cvssf.open,
                    mean_cvssf.open,
                    worst_cvssf.open,
                ],
            ],
            "colors": {
                "Closed": TREATMENT_COLORS.closed,
                "Accepted": TREATMENT.passive,
                "Open": TREATMENT_COLORS.open,
            },
            "type": "bar",
            "labels": {"format": {"Closed": None}},
            "groups": [
                [
                    "Closed",
                    "Accepted",
                    "Open",
                ],
            ],
            "order": None,
            "stack": {"normalize": True},
        },
        "legend": {"position": "bottom"},
        "grid": {
            "x": {"show": False},
            "y": {"show": False},
        },
        "axis": {
            "x": {
                "categories": categories,
                "type": "category",
                "tick": {"multiline": False},
            },
            "y": {
                "min": 0,
                "padding": {"bottom": 0},
                "label": {
                    "text": "CVSSF",
                    "position": "inner-top",
                },
                "tick": {"count": 2},
            },
        },
        "tooltip": {"format": {"value": None}},
        "percentageValues": {
            "Closed": [
                my_organization[0],
                best_organization[0],
                average_organization[0],
                worst_organization[0],
            ],
            "Accepted": [
                my_organization[1],
                best_organization[1],
                average_organization[1],
                worst_organization[1],
            ],
            "Open": [
                my_organization[2],
                best_organization[2],
                average_organization[2],
                worst_organization[2],
            ],
        },
        "bar": {"width": {"ratio": BAR_RATIO_WIDTH}},
        "centerLabel": True,
        "maxPercentageValues": max_percentage_values,
        "hideXTickLine": True,
        "hideYAxisLine": True,
    }


async def _get_organizations_and_portfolios() -> (
    tuple[
        OrganizationsAndPortfolios,
        OrganizationsAndPortfolios,
    ]
):
    organizations: OrganizationsAndPortfolios = []
    portfolios: OrganizationsAndPortfolios = []
    async for org_id, org_name, org_groups in charts_utils.iterate_organizations_and_groups():
        organizations.append((org_id, org_groups))
        for portfolio, groups in await charts_utils.get_portfolios_groups(org_name):
            portfolios.append((f"{org_id}PORTFOLIO#{portfolio}", tuple(groups)))
    return organizations, portfolios


async def generate_all() -> None:
    organizations, portfolios = await _get_organizations_and_portfolios()

    all_organizations_data: tuple[OrganizationCvssfBenchmarking, ...] = await collect(
        tuple(
            get_data_one_organization(organization_id=organization[0], groups=organization[1])
            for organization in organizations
        ),
        workers=8,
    )

    all_portfolios_data: tuple[OrganizationCvssfBenchmarking, ...] = await collect(
        tuple(
            get_data_one_organization(
                organization_id=portfolios[0],
                groups=portfolios[1],
            )
            for portfolios in portfolios
        ),
        workers=8,
    )

    header: str = "Categories"
    async for org_id, _, org_groups in charts_utils.iterate_organizations_and_groups():
        document = format_data(
            organization=await get_data_one_organization(organization_id=org_id, groups=org_groups),
            best_cvssf=get_best_organization(all_organizations_data=all_organizations_data),
            mean_cvssf=get_mean_organizations(
                organizations=get_valid_organizations(
                    subjects=all_organizations_data,
                )
            ),
            worst_cvssf=get_worst_organization(all_organizations_data=all_organizations_data),
            categories=ORGANIZATION_CATEGORIES,
        )
        charts_utils.json_dump(
            document=document,
            entity="organization",
            subject=org_id,
            csv_document=format_csv_data(document=document, header=header),
        )

    async for org_id, org_name, _ in charts_utils.iterate_organizations_and_groups():
        for portfolio, groups in await charts_utils.get_portfolios_groups(org_name):
            document = format_data(
                organization=await get_data_one_organization(
                    organization_id=f"{org_id}PORTFOLIO#{portfolio}",
                    groups=tuple(groups),
                ),
                best_cvssf=get_best_organization(all_organizations_data=all_portfolios_data),
                mean_cvssf=get_mean_organizations(
                    organizations=get_valid_organizations(
                        subjects=all_portfolios_data,
                    )
                ),
                worst_cvssf=get_worst_organization(all_organizations_data=all_portfolios_data),
                categories=PORTFOLIO_CATEGORIES,
            )
            charts_utils.json_dump(
                document=document,
                entity="portfolio",
                subject=f"{org_id}PORTFOLIO#{portfolio}",
                csv_document=format_csv_data(document=document, header=header),
            )


def main() -> None:
    run(generate_all())
