from collections.abc import (
    Awaitable,
    Callable,
)
from datetime import (
    datetime,
)
from decimal import (
    Decimal,
)

from aioextensions import (
    collect,
    run,
)
from async_lru import (
    alru_cache,
)

from integrates.charts import charts_utils
from integrates.charts.generators.stacked_bar_chart import (
    format_csv_data_over_time,
)
from integrates.charts.generators.stacked_bar_chart.util_class import (
    DISTRIBUTION_OVER_TIME,
    GroupDocumentData,
    RiskOverTime,
    TimeRangeType,
)
from integrates.charts.generators.stacked_bar_chart.utils.utils import (
    format_distribution_document,
    get_current_time_range,
    get_min_date_formatted,
    get_min_date_unformatted,
    get_quarter,
    get_semester,
    get_time_range,
    sum_over_time_many_groups,
    translate_date,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.groups.types import (
    RegisterByTime,
)


def get_distribution_over_rangetime(
    *,
    group_data: dict[str, dict[datetime, Decimal]],
    get_time: Callable[[datetime], datetime],
) -> dict[str, dict[datetime, Decimal]]:
    return {
        "date": {get_time(key): Decimal("0") for key, _ in group_data["date"].items()},
        "Closed": {get_time(key): value for key, value in group_data["Closed"].items()},
        "Open": {get_time(key): value for key, value in group_data["Open"].items()},
        "Accepted": {get_time(key): value for key, value in group_data["Accepted"].items()},
    }


@alru_cache(maxsize=None, typed=True)
async def _get_group_document(group: str) -> RiskOverTime:
    loaders = get_new_context()
    group_indicators = await loaders.group_unreliable_indicators.load(group)

    return get_document_formatted(
        group_over_time=[
            elements[-12:] for elements in group_indicators.remediated_over_time or []
        ],
        group_over_time_monthly=group_indicators.remediated_over_time_month,
        group_over_time_yearly=group_indicators.remediated_over_time_year,
        weekly_data_size=len(
            group_indicators.remediated_over_time[0]
            if group_indicators.remediated_over_time
            else []
        ),
        monthly_data_size=len(
            group_indicators.remediated_over_time_month[0]
            if group_indicators.remediated_over_time_month
            else []
        ),
    )


def process_group_over_time(
    group_over_time: RegisterByTime | None,
    date_func: Callable[[str], datetime],
    is_accepted: bool = False,
) -> list[GroupDocumentData]:
    data: list[GroupDocumentData] = []
    if group_over_time:
        group_opened_over_time = group_over_time[4]
        group_closed_over_time = group_over_time[1]
        group_accepted_over_time = group_over_time[2]

        for accepted, closed, opened in zip(
            group_accepted_over_time,
            group_closed_over_time,
            group_opened_over_time,
            strict=False,
        ):
            data.append(
                GroupDocumentData(
                    accepted=Decimal(accepted["y"]),
                    closed=Decimal(closed["y"]),
                    opened=Decimal(opened["y"]),
                    date=date_func(str(accepted["x"] if is_accepted else closed["x"])),
                    total=(Decimal(opened["y"]) + Decimal(closed["y"]) + Decimal(accepted["y"])),
                )
            )
    return data


def get_document_formatted(
    *,
    group_over_time: RegisterByTime | None,
    group_over_time_monthly: RegisterByTime | None,
    group_over_time_yearly: RegisterByTime | None,
    weekly_data_size: int,
    monthly_data_size: int,
) -> RiskOverTime:
    data = process_group_over_time(group_over_time, translate_date, is_accepted=True)
    data_monthly = process_group_over_time(group_over_time_monthly, get_min_date_unformatted)
    data_yearly = process_group_over_time(group_over_time_yearly, get_min_date_formatted)

    monthly: dict[str, dict[datetime, Decimal]] = {
        "date": {datum.date: Decimal("0") for datum in data_monthly},
        "Closed": {datum.date: datum.closed for datum in data_monthly},
        "Accepted": {datum.date: datum.accepted for datum in data_monthly},
        "Open": {datum.date: datum.opened for datum in data_monthly},
    }
    quarterly = get_distribution_over_rangetime(group_data=monthly, get_time=get_quarter)
    semesterly = get_distribution_over_rangetime(group_data=monthly, get_time=get_semester)
    yearly: dict[str, dict[datetime, Decimal]] = {
        "date": {datum.date: Decimal("0") for datum in data_yearly},
        "Closed": {datum.date: datum.closed for datum in data_yearly},
        "Accepted": {datum.date: datum.accepted for datum in data_yearly},
        "Open": {datum.date: datum.opened for datum in data_yearly},
    }

    return RiskOverTime(
        time_range=get_time_range(
            weekly_size=weekly_data_size,
            monthly_size=monthly_data_size,
            quarterly_size=len(quarterly["date"]),
            semesterly_size=len(semesterly["date"]),
        ),
        monthly=monthly,
        quarterly=quarterly,
        semesterly=semesterly,
        yearly=yearly,
        weekly={
            "date": {datum.date: Decimal("0") for datum in data},
            "Closed": {datum.date: datum.closed for datum in data},
            "Accepted": {datum.date: datum.accepted for datum in data},
            "Open": {datum.date: datum.opened for datum in data},
        },
    )


async def get_many_groups_document(
    groups: tuple[str, ...],
    get_group_document: Callable[[str], Awaitable[RiskOverTime]],
) -> tuple[tuple[dict[str, dict[datetime, Decimal]], ...], TimeRangeType]:
    group_documents = await collect(
        tuple(get_group_document(group) for group in groups),
        workers=32,
    )

    return sum_over_time_many_groups(
        get_current_time_range(group_documents),
        DISTRIBUTION_OVER_TIME,
    )


async def generate_all(
    y_label: str,
    get_group_document: Callable[[str], Awaitable[RiskOverTime]],
) -> None:
    header = "Dates"
    async for group in charts_utils.iterate_groups():
        group_document = await get_group_document(group)
        document = format_distribution_document(
            data_document=get_current_time_range(tuple([group_document])),
            y_label=y_label,
        )
        charts_utils.json_dump(
            document=document,
            entity="group",
            subject=group,
            csv_document=format_csv_data_over_time(document=document, header=header),
        )

    async for org_id, _, org_groups in charts_utils.iterate_organizations_and_groups():
        document = format_distribution_document(
            data_document=await get_many_groups_document(org_groups, get_group_document),
            y_label=y_label,
        )
        charts_utils.json_dump(
            document=document,
            entity="organization",
            subject=org_id,
            csv_document=format_csv_data_over_time(document=document, header=header),
        )

    async for org_id, org_name, _ in charts_utils.iterate_organizations_and_groups():
        for portfolio, groups in await charts_utils.get_portfolios_groups(org_name):
            document = format_distribution_document(
                data_document=await get_many_groups_document(groups, get_group_document),
                y_label=y_label,
            )
            charts_utils.json_dump(
                document=document,
                entity="portfolio",
                subject=f"{org_id}PORTFOLIO#{portfolio}",
                csv_document=format_csv_data_over_time(document=document, header=header),
            )


def main() -> None:
    run(generate_all("Vulnerabilities", _get_group_document))
