from collections.abc import (
    Awaitable,
    Callable,
)
from datetime import (
    date,
)
from decimal import (
    ROUND_CEILING,
    Decimal,
)
from typing import (
    Any,
)

from integrates.charts import charts_utils
from integrates.charts.generators.bar_chart.utils import (
    Remediate,
    format_csv_data,
)
from integrates.charts.generators.common.colors import (
    RISK,
)
from integrates.charts.generators.common.utils import (
    BAR_RATIO_WIDTH,
    get_max_axis,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)


async def _dump_document(
    *,
    format_data: Callable[[Remediate], dict[str, str]],
    get_data_groups: Callable[[Any, date | None], Awaitable[Remediate]],
    alternative: str,
    min_date: date | None,
    entity: str,
    subject: str,
    groups: str | tuple[str, ...],
) -> None:
    header: str = "Categories"
    document = format_data(
        await get_data_groups(groups, min_date),
    )
    charts_utils.json_dump(
        document=document,
        entity=entity,
        subject=subject,
        csv_document=format_csv_data(document=document, header=header, alternative=alternative),
    )


async def generate_all(
    *,
    format_data: Callable[[Remediate], dict[str, str]],
    get_data_one_group: Callable[[str, date | None], Awaitable[Remediate]],
    get_data_many_groups: Callable[[tuple[str, ...], date | None], Awaitable[Remediate]],
    alternative: str,
) -> None:
    list_days: list[int] = [30, 90]
    dates: list[date] = [
        datetime_utils.get_now_minus_delta(days=list_days[0]).date(),
        datetime_utils.get_now_minus_delta(days=list_days[1]).date(),
    ]
    for days, min_date in zip([None, *list_days], [None, *dates], strict=False):
        async for group in charts_utils.iterate_groups():
            await _dump_document(
                format_data=format_data,
                get_data_groups=get_data_one_group,
                alternative=alternative,
                min_date=min_date,
                entity="group",
                subject=group + charts_utils.get_subject_days(days),
                groups=group,
            )

        async for (
            org_id,
            _,
            org_groups,
        ) in charts_utils.iterate_organizations_and_groups():
            await _dump_document(
                format_data=format_data,
                get_data_groups=get_data_many_groups,
                alternative=alternative,
                min_date=min_date,
                entity="organization",
                subject=org_id + charts_utils.get_subject_days(days),
                groups=org_groups,
            )

        async for (
            org_id,
            org_name,
            _,
        ) in charts_utils.iterate_organizations_and_groups():
            for portfolio, groups in await charts_utils.get_portfolios_groups(org_name):
                await _dump_document(
                    format_data=format_data,
                    get_data_groups=get_data_many_groups,
                    alternative=alternative,
                    min_date=min_date,
                    entity="portfolio",
                    subject=f"{org_id}PORTFOLIO#{portfolio}" + charts_utils.get_subject_days(days),
                    groups=groups,
                )


def format_data_non_cvssf(data: Remediate) -> dict:
    translations: dict[str, str] = {
        "critical_severity": "Critical",
        "high_severity": "High",
        "medium_severity": "Medium",
        "low_severity": "Low",
    }
    values: list[Decimal] = [
        Decimal(getattr(data, key)).to_integral_exact(rounding=ROUND_CEILING)
        for key, _ in translations.items()
    ]
    max_value: Decimal = sorted(
        [abs(value) for value in values],
        reverse=True,
    )[0]
    max_axis_value: Decimal = (
        get_max_axis(value=max_value) if max_value > Decimal("0.0") else Decimal("0.0")
    )

    return {
        "data": {
            "columns": [["Mean time to remediate", *values]],
            "colors": {"Mean time to remediate": RISK.neutral},
            "labels": True,
            "type": "bar",
        },
        "bar": {"width": {"ratio": BAR_RATIO_WIDTH}},
        "legend": {"show": False},
        "tooltip": {"show": False},
        "hideXTickLine": True,
        "hideYAxisLine": True,
        "axis": {
            "x": {
                "categories": [value for _, value in translations.items()],
                "type": "category",
            },
            "y": {
                "min": 0,
                "padding": {
                    "bottom": 0,
                    "top": 0,
                },
                "label": {
                    "text": "Days",
                    "position": "inner-top",
                },
                **({} if max_axis_value == Decimal("0.0") else {"max": max_axis_value}),
                "tick": {"count": 5},
            },
        },
        "barChartYTickFormat": True,
        "grid": {"y": {"show": True}},
        "mttrCvssf": True,
    }
