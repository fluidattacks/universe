import math
from collections.abc import (
    Callable,
)
from datetime import (
    datetime,
)
from decimal import (
    Decimal,
)
from statistics import (
    mean,
)
from typing import (
    Any,
    NamedTuple,
)

from aioextensions import (
    collect,
    run,
)
from async_lru import (
    alru_cache,
)

from integrates.charts.charts_utils import (
    format_cvssf,
    get_portfolios_groups,
    iterate_groups,
    iterate_organizations_and_groups,
    json_dump,
)
from integrates.charts.generators.bar_chart import (
    format_csv_data,
)
from integrates.charts.generators.bar_chart.utils import (
    GROUP_CATEGORIES,
    ORGANIZATION_CATEGORIES,
    PORTFOLIO_CATEGORIES,
    Benchmarking,
    get_valid_subjects,
)
from integrates.charts.generators.common.colors import (
    EXPOSURE,
)
from integrates.charts.generators.common.utils import (
    BAR_RATIO_WIDTH,
    get_max_axis,
)
from integrates.charts.generators.stacked_bar_chart.util_class import (
    RiskOverTime,
)
from integrates.charts.generators.stacked_bar_chart.utils.utils import (
    get_current_time_range,
    get_min_date_formatted,
    get_min_date_unformatted,
    get_quarter,
    get_semester,
    get_time_range,
    translate_date,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.groups.types import RegisterByTime
from integrates.db_model.roots.enums import (
    RootStatus,
)
from integrates.groups import (
    domain as groups_domain,
)
from integrates.organizations.domain import (
    get_all_active_group_names,
)


class GroupDocumentCvssfData(NamedTuple):
    data_date: datetime
    low: Decimal
    medium: Decimal
    high: Decimal
    critical: Decimal


def format_cvssf_log(cvssf: Decimal) -> Decimal:
    if cvssf <= Decimal("0.0"):
        return cvssf.quantize(Decimal("0.1"))

    return Decimal(math.log2(cvssf))


def format_max_value(data: tuple[Decimal, ...]) -> Decimal:
    if data:
        return sorted(data, reverse=True)[0]

    return Decimal("1.0")


def get_rangetime(
    *,
    group_data: dict[str, dict[datetime, Decimal]],
    get_time: Callable[[datetime], datetime],
) -> dict[str, dict[datetime, Decimal]]:
    return {
        "date": {get_time(key): value for key, value in group_data["date"].items()},
        "Exposure": {get_time(key): value for key, value in group_data["Exposure"].items()},
    }


def _get_risk_over_time(
    *,
    group_indicators: Any,
    data: list[GroupDocumentCvssfData],
    data_monthly: list[GroupDocumentCvssfData],
    data_yearly: list[GroupDocumentCvssfData],
) -> RiskOverTime:
    weekly_data_size: int = len(
        group_indicators.exposed_over_time_cvssf[0]
        if group_indicators.exposed_over_time_cvssf
        else []
    )

    monthly_data_size: int = len(
        group_indicators.exposed_over_time_month_cvssf[0]
        if group_indicators.exposed_over_time_month_cvssf
        else []
    )
    monthly: dict[str, dict[datetime, Decimal]] = {
        "date": {datum.data_date: Decimal("0.0") for datum in data_monthly},
        "Exposure": {
            datum.data_date: Decimal(datum.low + datum.medium + datum.high + datum.critical)
            for datum in data_monthly
        },
    }
    yearly: dict[str, dict[datetime, Decimal]] = {
        "date": {datum.data_date: Decimal("0.0") for datum in data_yearly},
        "Exposure": {
            datum.data_date: Decimal(datum.low + datum.medium + datum.high + datum.critical)
            for datum in data_yearly
        },
    }
    quarterly = get_rangetime(group_data=monthly, get_time=get_quarter)
    semesterly = get_rangetime(group_data=monthly, get_time=get_semester)

    return RiskOverTime(
        time_range=get_time_range(
            weekly_size=weekly_data_size,
            monthly_size=monthly_data_size,
            quarterly_size=len(quarterly["date"]),
            semesterly_size=len(semesterly["date"]),
        ),
        monthly=monthly,
        quarterly=quarterly,
        semesterly=semesterly,
        yearly=yearly,
        weekly={
            "date": {datum.data_date: Decimal("0.0") for datum in data},
            "Exposure": {
                datum.data_date: Decimal(datum.low + datum.medium + datum.high + datum.critical)
                for datum in data
            },
        },
    )


def process_group_over_time(
    group_over_time: RegisterByTime | None, data_date_func: Callable[[str], datetime]
) -> list[GroupDocumentCvssfData]:
    data = []
    if group_over_time:
        group_low_over_time = group_over_time[0]
        group_medium_over_time = group_over_time[1]
        group_high_over_time = group_over_time[2]
        group_critical_over_time = group_over_time[3]

        for low, medium, high, critical in zip(
            group_low_over_time,
            group_medium_over_time,
            group_high_over_time,
            group_critical_over_time,
            strict=False,
        ):
            data.append(
                GroupDocumentCvssfData(
                    low=Decimal(low["y"]),
                    medium=Decimal(medium["y"]),
                    high=Decimal(high["y"]),
                    critical=Decimal(critical["y"]),
                    data_date=data_date_func(str(low["x"])),
                )
            )

    return data


async def get_group_document(group: str, loaders: Dataloaders) -> RiskOverTime:
    group_indicators = await loaders.group_unreliable_indicators.load(group)
    group_over_time = [
        elements[-12:] for elements in group_indicators.exposed_over_time_cvssf or []
    ]
    group_over_time_monthly = group_indicators.exposed_over_time_month_cvssf
    group_over_time_yearly = group_indicators.exposed_over_time_year_cvssf
    data = process_group_over_time(group_over_time, translate_date)
    data_monthly = process_group_over_time(group_over_time_monthly, get_min_date_unformatted)
    data_yearly = process_group_over_time(group_over_time_yearly, get_min_date_formatted)

    return _get_risk_over_time(
        group_indicators=group_indicators,
        data=data,
        data_monthly=data_monthly,
        data_yearly=data_yearly,
    )


@alru_cache(maxsize=None, typed=True)
async def get_data_one_group(group: str) -> Benchmarking:
    loaders = get_new_context()
    group_vulns = await groups_domain.get_group_vulns_with_severity(loaders, group)
    active_roots = [
        root
        for root in await loaders.group_roots.load(group)
        if root.state.status is RootStatus.ACTIVE
    ]

    number_of_reattacks = sum(
        vulnerability.unreliable_indicators.unreliable_reattack_cycles or 0
        for vulnerability in group_vulns
    )

    group_document = await get_group_document(group, loaders)
    document = get_current_time_range(tuple([group_document]))[0][0]
    values = [
        Decimal(document[name][date]).quantize(Decimal("0.1"))
        for date in tuple(document["date"])[-12:]
        for name in document
        if name != "date"
    ]

    return Benchmarking(
        is_valid=number_of_reattacks > 10 and len(active_roots) > 0,
        number_of_active_roots=len(active_roots),
        subject=group.lower(),
        mttr=values[-1] if len(values) > 0 else Decimal("0.0"),
        number_of_reattacks=number_of_reattacks,
    )


@alru_cache(maxsize=None, typed=True)
async def get_data_many_groups(
    organization_id: str,
    groups: tuple[str, ...],
) -> Benchmarking:
    groups_data: tuple[Benchmarking, ...] = await collect(
        tuple(get_data_one_group(group) for group in groups),
        workers=16,
    )

    exposure = (
        Decimal(sum(group_data.mttr for group_data in groups_data)).quantize(Decimal("0.1"))
        if groups_data
        else Decimal("0.0")
    )
    number_of_reattacks = sum(group_data.number_of_reattacks for group_data in groups_data)
    number_of_active_roots = sum(group_data.number_of_active_roots for group_data in groups_data)

    return Benchmarking(
        is_valid=number_of_reattacks > 100 and number_of_active_roots > 0,
        subject=organization_id,
        mttr=exposure,
        number_of_reattacks=number_of_reattacks,
        number_of_active_roots=number_of_active_roots,
    )


def get_average_entities(*, entities: list[Benchmarking]) -> Decimal:
    return (
        Decimal(mean([subject.mttr for subject in entities])).quantize(Decimal("0.1"))
        if entities
        else Decimal("0.0")
    )


def get_best_exposure(*, subjects: list[Benchmarking]) -> Decimal:
    return (
        Decimal(min(subject.mttr for subject in subjects)).quantize(Decimal("0.1"))
        if subjects
        else Decimal("0.0")
    )


def get_worst_exposure(*, subjects: list[Benchmarking]) -> Decimal:
    return (
        Decimal(max(subject.mttr for subject in subjects)).quantize(Decimal("0.1"))
        if subjects
        else Decimal("0.0")
    )


def format_data(
    all_data: tuple[Decimal, Decimal, Decimal, Decimal],
    categories: list[str],
) -> dict:
    data = tuple(format_cvssf(value) for value in all_data)
    max_value = sorted(
        [abs(value) for value in data],
        reverse=True,
    )[0]

    max_axis_value = get_max_axis(value=max_value) if max_value > Decimal("0.0") else Decimal("0.0")

    return {
        "data": {
            "columns": [
                [
                    "Exposure",
                    *[format_cvssf_log(value) for value in data],
                ]
            ],
            "colors": {
                "Exposure": EXPOSURE,
            },
            "labels": True,
            "type": "bar",
        },
        "axis": {
            "x": {
                "categories": categories,
                "type": "category",
            },
            "y": {
                "min": 0,
                "padding": {
                    "bottom": 0,
                    "top": 0,
                },
                "label": {
                    "text": "CVSSF",
                    "position": "inner-top",
                },
                "tick": {
                    "count": 5,
                },
                **(
                    {}
                    if max_axis_value == Decimal("0.0")
                    else {"max": format_cvssf_log(max_axis_value)}
                ),
            },
        },
        "bar": {
            "width": {
                "ratio": BAR_RATIO_WIDTH,
            },
        },
        "tooltip": {
            "show": False,
        },
        "legend": {
            "show": False,
        },
        "mttrBenchmarking": True,
        "maxValue": format_max_value(data),
        "maxValueLog": format_max_value(tuple(format_cvssf_log(value) for value in data)),
        "originalValues": [Decimal(value).quantize(Decimal("0.1")) for value in data],
        "grid": {
            "x": {"show": False},
            "y": {"show": True},
        },
        "hideYAxisLine": True,
        "hideXTickLine": True,
        "exposureBenchmarkingCvssf": True,
    }


async def _get_all_data() -> (
    tuple[
        tuple[Benchmarking, ...],
        tuple[Benchmarking, ...],
        tuple[Benchmarking, ...],
    ]
):
    organizations: list[tuple[str, tuple[str, ...]]] = []
    portfolios: list[tuple[str, tuple[str, ...]]] = []
    group_names: list[str] = sorted(
        await get_all_active_group_names(get_new_context()),
        reverse=True,
    )

    async for org_id, org_name, org_groups in iterate_organizations_and_groups():
        organizations.append((org_id, org_groups))
        for portfolio, p_groups in await get_portfolios_groups(org_name):
            portfolios.append((f"{org_id}PORTFOLIO#{portfolio}", tuple(p_groups)))

    all_groups_data = await collect(
        tuple(get_data_one_group(group_name) for group_name in group_names),
        workers=8,
    )

    all_organizations_data: tuple[Benchmarking, ...] = await collect(
        tuple(
            get_data_many_groups(
                organization_id=organization[0],
                groups=organization[1],
            )
            for organization in organizations
        ),
        workers=8,
    )

    all_portfolios_data: tuple[Benchmarking, ...] = await collect(
        tuple(
            get_data_many_groups(
                organization_id=portfolio[0],
                groups=portfolio[1],
            )
            for portfolio in portfolios
        ),
        workers=8,
    )
    return all_groups_data, all_organizations_data, all_portfolios_data


async def _dump_group_data(
    *,
    all_groups_data: tuple[Benchmarking, ...],
    best_group_exposure: Decimal,
    worst_group_exposure: Decimal,
    header: str,
    alternative: str,
) -> None:
    async for group in iterate_groups():
        document = format_data(
            all_data=(
                (await get_data_one_group(group)).mttr,
                best_group_exposure,
                get_average_entities(
                    entities=get_valid_subjects(
                        all_subjects=all_groups_data,
                    )
                ),
                worst_group_exposure,
            ),
            categories=GROUP_CATEGORIES,
        )
        json_dump(
            document=document,
            entity="group",
            subject=group,
            csv_document=format_csv_data(document=document, header=header, alternative=alternative),
        )


async def _dump_organization_data(
    *,
    all_organizations_data: tuple[Benchmarking, ...],
    best_exposure: Decimal,
    worst_organization_exposure: Decimal,
    header: str,
    alternative: str,
) -> None:
    async for org_id, _, org_groups in iterate_organizations_and_groups():
        document = format_data(
            all_data=(
                (
                    await get_data_many_groups(
                        organization_id=org_id,
                        groups=org_groups,
                    )
                ).mttr,
                best_exposure,
                get_average_entities(
                    entities=get_valid_subjects(
                        all_subjects=all_organizations_data,
                    )
                ),
                worst_organization_exposure,
            ),
            categories=ORGANIZATION_CATEGORIES,
        )
        json_dump(
            document=document,
            entity="organization",
            subject=org_id,
            csv_document=format_csv_data(document=document, header=header, alternative=alternative),
        )


async def _dump_portfolio_data(
    *,
    all_portfolios_data: tuple[Benchmarking, ...],
    best_portfolio_exposure: Decimal,
    worst_portfolio_exposure: Decimal,
    header: str,
    alternative: str,
) -> None:
    async for org_id, org_name, _ in iterate_organizations_and_groups():
        for portfolio, pgroup_names in await get_portfolios_groups(org_name):
            document = format_data(
                all_data=(
                    (
                        await get_data_many_groups(
                            organization_id=f"{org_id}PORTFOLIO#{portfolio}",
                            groups=pgroup_names,
                        )
                    ).mttr,
                    best_portfolio_exposure,
                    get_average_entities(
                        entities=get_valid_subjects(
                            all_subjects=all_portfolios_data,
                        )
                    ),
                    worst_portfolio_exposure,
                ),
                categories=PORTFOLIO_CATEGORIES,
            )
            json_dump(
                document=document,
                entity="portfolio",
                subject=f"{org_id}PORTFOLIO#{portfolio}",
                csv_document=format_csv_data(
                    document=document,
                    header=header,
                    alternative=alternative,
                ),
            )


async def generate() -> None:
    (
        all_groups_data,
        all_organizations_data,
        all_portfolios_data,
    ) = await _get_all_data()
    best_exposure = get_best_exposure(
        subjects=[organization for organization in all_organizations_data if organization.is_valid]
    )

    worst_organization_exposure = get_worst_exposure(
        subjects=[organization for organization in all_organizations_data if organization.is_valid],
    )

    best_group_exposure: Decimal = get_best_exposure(
        subjects=[group for group in all_groups_data if group.is_valid]
    )

    worst_group_exposure: Decimal = get_worst_exposure(
        subjects=[group for group in all_groups_data if group.is_valid],
    )

    best_portfolio_exposure: Decimal = get_best_exposure(
        subjects=[portfolio for portfolio in all_portfolios_data if portfolio.is_valid]
    )
    worst_portfolio_exposure: Decimal = get_worst_exposure(
        subjects=[portfolio for portfolio in all_portfolios_data if portfolio.is_valid],
    )

    header = "Categories"
    alternative = "Exposure"

    await _dump_group_data(
        all_groups_data=all_groups_data,
        best_group_exposure=best_group_exposure,
        worst_group_exposure=worst_group_exposure,
        header=header,
        alternative=alternative,
    )
    await _dump_organization_data(
        all_organizations_data=all_organizations_data,
        best_exposure=best_exposure,
        worst_organization_exposure=worst_organization_exposure,
        header=header,
        alternative=alternative,
    )
    await _dump_portfolio_data(
        all_portfolios_data=all_portfolios_data,
        best_portfolio_exposure=best_portfolio_exposure,
        worst_portfolio_exposure=worst_portfolio_exposure,
        header=header,
        alternative=alternative,
    )


def main() -> None:
    run(generate())
