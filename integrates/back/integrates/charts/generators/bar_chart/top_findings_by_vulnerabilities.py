from collections import (
    Counter,
)
from decimal import (
    Decimal,
)
from itertools import (
    groupby,
)

from aioextensions import (
    collect,
    run,
)
from async_lru import (
    alru_cache,
)

from integrates.charts.charts_utils import (
    CsvData,
)
from integrates.charts.generators.bar_chart.utils import (
    LIMIT,
    format_data_csv,
    generate_all_top_vulnerabilities,
)
from integrates.charts.generators.bar_chart.utils_top_vulnerabilities_by_source import (
    format_max_value,
)
from integrates.charts.generators.common.colors import (
    VULNERABILITIES_COUNT,
)
from integrates.charts.generators.common.utils import (
    get_finding_name,
    get_finding_url,
)
from integrates.custom_utils.findings import (
    get_group_findings,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
)
from integrates.mailer.utils import (
    get_organization_name,
)


@alru_cache(maxsize=None, typed=True)
async def get_data_one_group(group: str) -> Counter[str]:
    loaders = get_new_context()
    organization_name = await get_organization_name(loaders, group)
    group_findings = await get_group_findings(group_name=group, loaders=loaders)
    finding_ids = [finding.id for finding in group_findings]
    finding_vulns = await loaders.finding_vulnerabilities_released_nzr.load_many(finding_ids)
    counter = Counter(
        [
            f"orgs/{organization_name}/groups/{group}/vulns/{finding.id}/{finding.title}"
            for finding, vulnerabilities in zip(group_findings, finding_vulns, strict=False)
            for vulnerability in vulnerabilities
            if vulnerability.state.status == VulnerabilityStateStatus.VULNERABLE
        ]
    )

    return counter


async def get_data_many_groups(groups: list[str]) -> Counter[str]:
    groups_data: tuple[Counter[str], ...] = await collect(
        tuple(get_data_one_group(group) for group in groups),
        workers=32,
    )
    return sum(groups_data, Counter())


def format_data(counters: Counter[str], is_group: bool) -> tuple[dict, CsvData]:
    data: list[tuple[str, int]] = counters.most_common()
    merged_data: list[list[int | str]] = []
    original_ids: list[list[str | int]] = []
    for axis, columns in groupby(
        sorted(data, key=lambda x: get_finding_name([x[0]])),
        key=lambda x: get_finding_name([x[0]]),
    ):
        _columns = list(columns)
        merged_data.append([axis, sum(value for _, value in _columns)])
        if _columns:
            original_ids.append(
                [
                    get_finding_url(sorted(_columns, key=lambda a: a[1])[0][0]),
                    sum(value for _, value in _columns),
                ]
            )

    merged_data = sorted(merged_data, key=lambda x: x[1], reverse=True)
    original_ids = sorted(original_ids, key=lambda x: x[1], reverse=True)[:LIMIT]
    limited_merged_data = merged_data[:LIMIT]

    json_data: dict = {
        "data": {
            "columns": [
                [
                    "# Open Vulnerabilities",
                    *[value for _, value in limited_merged_data],
                ],
            ],
            "colors": {"# Open Vulnerabilities": VULNERABILITIES_COUNT},
            "labels": None,
            "type": "bar",
        },
        "legend": {"show": False},
        "padding": {"bottom": 0},
        "exposureTrendsByCategories": True,
        "keepToltipColor": True,
        "axis": {
            "rotated": True,
            "x": {
                "categories": [get_finding_name([str(key)]) for key, _ in limited_merged_data],
                "type": "category",
                "tick": {
                    "multiline": False,
                    "outer": False,
                    "rotate": 0,
                },
            },
            "y": {
                "min": 0,
                "padding": {"bottom": 0},
            },
        },
        "barChartYTickFormat": True,
        "maxValue": format_max_value(
            [(str(key), Decimal(value)) for key, value in limited_merged_data]
        ),
        "originalIds": [key for key, _ in original_ids] if is_group else None,
    }
    csv_data = format_data_csv(
        header_value=str(json_data["data"]["columns"][0][0]),
        values=[Decimal(value) for _, value in merged_data],
        categories=[str(group) for group, _ in merged_data],
        header_title="Type",
    )

    return (json_data, csv_data)


def main() -> None:
    run(
        generate_all_top_vulnerabilities(
            get_data_one_group=get_data_one_group,
            get_data_many_groups=get_data_many_groups,
            format_data=format_data,
        )
    )
