import re
from collections import (
    Counter,
)
from datetime import (
    datetime,
)
from decimal import (
    Decimal,
)

from aioextensions import (
    collect,
    run,
)
from async_lru import (
    alru_cache,
)

from integrates.charts.charts_utils import (
    CsvData,
    get_portfolios_groups,
    iterate_groups,
    iterate_organizations_and_groups,
    json_dump,
)
from integrates.charts.generators.bar_chart.utils import (
    LIMIT,
    format_data_csv,
)
from integrates.charts.generators.bar_chart.utils_top_vulnerabilities_by_source import (
    format_max_value,
)
from integrates.charts.generators.common.colors import (
    VULNERABILITIES_COUNT,
)
from integrates.custom_utils.datetime import (
    get_now_minus_delta,
)
from integrates.custom_utils.findings import (
    get_group_findings,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
    VulnerabilityType,
)


def format_where(where: str) -> str:
    # filename (package) [CVE]
    if match := re.match(r"(?P<where>.*)\s\(.*\)\s\[.*\]", where):
        return match.groupdict()["where"]

    return where


@alru_cache(maxsize=None, typed=True)
async def get_data_one_group(*, group: str, date_minus_delta: datetime) -> Counter[str]:
    loaders = get_new_context()
    group_findings = await get_group_findings(group_name=group, loaders=loaders)
    vulnerabilities = await loaders.finding_vulnerabilities_released_nzr.load_many_chained(
        [finding.id for finding in group_findings]
    )

    return Counter(
        tuple(
            format_where(vulnerability.state.where)
            for vulnerability in vulnerabilities
            if vulnerability.unreliable_indicators.unreliable_report_date
            and vulnerability.unreliable_indicators.unreliable_report_date > date_minus_delta
            and vulnerability.type == VulnerabilityType.LINES
            and vulnerability.state.status == VulnerabilityStateStatus.VULNERABLE
        )
    )


async def get_data_many_groups(
    *, groups: tuple[str, ...], date_minus_delta: datetime
) -> Counter[str]:
    groups_data = await collect(
        tuple(
            get_data_one_group(group=group, date_minus_delta=date_minus_delta) for group in groups
        ),
        workers=32,
    )

    return sum(groups_data, Counter())


def format_data(*, counters: Counter[str]) -> tuple[dict, CsvData]:
    merged_data: list[tuple[str, int]] = counters.most_common()
    limited_merged_data = merged_data[:LIMIT]

    json_data = {
        "data": {
            "columns": [
                [
                    "# Vulnerabilities",
                    *[value for _, value in limited_merged_data],
                ],
            ],
            "colors": {"# Vulnerabilities": VULNERABILITIES_COUNT},
            "labels": None,
            "type": "bar",
        },
        "legend": {"show": False},
        "axis": {
            "rotated": True,
            "x": {
                "categories": [key for key, _ in limited_merged_data],
                "type": "category",
                "tick": {
                    "multiline": False,
                    "outer": False,
                    "rotate": 0,
                },
            },
            "y": {
                "label": {"position": "outer-top"},
                "min": 0,
                "padding": {"bottom": 0},
            },
        },
        "barChartXTickFormat": True,
        "barChartYTickFormat": True,
        "maxValue": format_max_value([(key, Decimal(value)) for key, value in limited_merged_data]),
        "tooltip": {"format": {"value": None}},
        "exposureTrendsByCategories": True,
        "keepToltipColor": True,
    }
    csv_data = format_data_csv(
        header_value="Number of vulnerabilities",
        values=[Decimal(value) for _, value in merged_data],
        categories=[name for name, _ in merged_data],
        header_title="File path",
    )

    return (json_data, csv_data)


async def generate_all() -> None:
    date_minus_delta: datetime = get_now_minus_delta(weeks=20)
    async for group in iterate_groups():
        json_document, csv_document = format_data(
            counters=await get_data_one_group(
                group=group,
                date_minus_delta=date_minus_delta,
            ),
        )
        json_dump(
            document=json_document,
            entity="group",
            subject=group,
            csv_document=csv_document,
        )

    async for org_id, _, org_groups in iterate_organizations_and_groups():
        json_document, csv_document = format_data(
            counters=await get_data_many_groups(
                groups=org_groups,
                date_minus_delta=date_minus_delta,
            ),
        )
        json_dump(
            document=json_document,
            entity="organization",
            subject=org_id,
            csv_document=csv_document,
        )

    async for org_id, org_name, _ in iterate_organizations_and_groups():
        for portfolio, groups in await get_portfolios_groups(org_name):
            json_document, csv_document = format_data(
                counters=await get_data_many_groups(
                    groups=tuple(groups),
                    date_minus_delta=date_minus_delta,
                ),
            )
            json_dump(
                document=json_document,
                entity="portfolio",
                subject=f"{org_id}PORTFOLIO#{portfolio}",
                csv_document=csv_document,
            )


def main() -> None:
    run(generate_all())
