from collections import Counter, defaultdict
from contextlib import suppress
from decimal import Decimal
from typing import NamedTuple

from aioextensions import collect, run
from async_lru import alru_cache

from integrates.charts.charts_utils import (
    format_cvssf,
    get_portfolios_groups,
    iterate_groups,
    iterate_organizations_and_groups,
    json_dump,
)
from integrates.charts.generators.bar_chart import format_csv_data
from integrates.charts.generators.bar_chart.utils_top_vulnerabilities_by_source import (
    format_max_value,
)
from integrates.charts.generators.common.colors import RISK
from integrates.charts.generators.common.utils import format_cvssf_log_adjusted, get_max_axis
from integrates.charts.generators.text_box.utils_vulnerabilities_remediation import (
    had_state_by_then,
)
from integrates.custom_utils.criteria import CRITERIA_VULNERABILITIES
from integrates.custom_utils.datetime import get_now_minus_delta
from integrates.custom_utils.findings import get_group_findings
from integrates.dataloaders import get_new_context
from integrates.db_model.findings.types import Finding
from integrates.db_model.types import SeverityScore
from integrates.db_model.vulnerabilities.enums import VulnerabilityStateStatus
from integrates.db_model.vulnerabilities.types import VulnerabilityState
from integrates.findings import domain as findings_domain


class MaxSeverity(NamedTuple):
    value: Decimal
    name: str


class GroupInformation(NamedTuple):
    categories: dict[str, str]
    finding_vulnerabilities: list[list[tuple[VulnerabilityState, SeverityScore | None]]]
    findings: list[str]


def get_categories() -> defaultdict[str, str]:
    categories: defaultdict[str, str] = defaultdict(str)
    for code, data in CRITERIA_VULNERABILITIES.items():
        categories[code] = data["category"]

    return categories


CATEGORIES = get_categories()


def _get_category(*, finding: Finding, categories: dict[str, str]) -> str:
    with suppress(KeyError):
        return categories[finding.get_criteria_code()]

    return ""


@alru_cache(maxsize=None, typed=True)
async def get_data_vulnerabilities(*, group: str) -> GroupInformation:
    loaders = get_new_context()
    findings = await get_group_findings(group_name=group, loaders=loaders)
    finding_category = {
        finding.id: _get_category(finding=finding, categories=CATEGORIES) for finding in findings
    }
    finding_vulnerabilities = await collect(
        tuple(
            findings_domain.get_vulnerabilities_with_severity(loaders, finding.id)
            for finding in findings
        ),
        workers=8,
    )
    filtered_vulnerabilities = list(filter(None, finding_vulnerabilities))
    vulnerabilities = [
        [(vulnerability.state, vulnerability.severity_score) for vulnerability in vulnerabilities]
        for vulnerabilities in filtered_vulnerabilities
    ]

    return GroupInformation(
        categories=finding_category,
        finding_vulnerabilities=vulnerabilities,
        findings=[vulns[0].finding_id for vulns in filtered_vulnerabilities],
    )


@alru_cache(maxsize=None, typed=True)
async def get_data_one_group(*, group: str, days: int = 30) -> Counter[str]:
    last_day = get_now_minus_delta(days=days)
    data = await get_data_vulnerabilities(group=group)

    vulnerabilities_by_categories = [
        {
            f"{data.categories[finding_id]}/open": had_state_by_then(
                last_day=last_day,
                state=VulnerabilityStateStatus.VULNERABLE,
                vulnerabilities=vulnerabilities,
                sprint=True,
            ).quantize(Decimal("0.001")),
            f"{data.categories[finding_id]}/closed": had_state_by_then(
                last_day=last_day,
                state=VulnerabilityStateStatus.SAFE,
                vulnerabilities=vulnerabilities,
                sprint=True,
            ).quantize(Decimal("0.001")),
        }
        for finding_id, vulnerabilities in zip(
            data.findings, data.finding_vulnerabilities, strict=False
        )
    ]

    return sum([Counter(source) for source in vulnerabilities_by_categories], Counter())


async def get_data_many_groups(*, groups: tuple[str, ...], days: int) -> Counter[str]:
    groups_data = await collect(
        tuple(get_data_one_group(group=group, days=days) for group in groups),
        workers=8,
    )

    return sum(groups_data, Counter())


def sorter(item: MaxSeverity) -> tuple[Decimal, int]:
    return (format_cvssf(item.value), -ord(item.name[0]) if item.name else 0)


def format_data(data: Counter[str], categories: list[str]) -> dict:
    categories_trend = [
        MaxSeverity(
            name=category,
            value=Decimal(data[f"{category}/open"] - data[f"{category}/closed"]),
        )
        for category in categories
    ]
    categories_trend = [
        MaxSeverity(name=category.name, value=format_cvssf(category.value))
        for category in categories_trend
    ]
    categories_trend = sorted(categories_trend, key=sorter, reverse=True)
    max_value = sorted(
        [abs(format_cvssf(category.value)) for category in categories_trend],
        reverse=True,
    )[0]
    max_axis_value = get_max_axis(value=max_value) if max_value > Decimal("0.0") else Decimal("0.0")
    min_axis_value = max_axis_value * Decimal("-1.0")

    return {
        "data": {
            "columns": [
                ["Exposure"]
                + [str(format_cvssf_log_adjusted(category.value)) for category in categories_trend],
            ],
            "color": None,
            "colors": {"Exposure": RISK.more_aggressive},
            "labels": None,
            "type": "bar",
        },
        "legend": {"show": False},
        "grid": {
            "x": {"show": False},
            "y": {"show": True},
        },
        "axis": {
            "rotated": True,
            "x": {
                "categories": [category.name for category in categories_trend],
                "type": "category",
                "tick": {
                    "rotate": 0,
                    "multiline": False,
                },
            },
            "y": {
                "label": {
                    "text": "CVSSF",
                    "position": "outer-top",
                },
                "tick": {"count": 7},
                "min": format_cvssf_log_adjusted(min_axis_value),
                "max": format_cvssf_log_adjusted(max_axis_value),
            },
        },
        "maxValueLogAdjusted": format_max_value(
            [
                (category.name, format_cvssf_log_adjusted(abs(category.value)))
                for category in categories_trend
            ]
        ),
        "originalValues": [format_cvssf(category.value) for category in categories_trend],
        "exposureTrendsByCategories": True,
    }


def get_subject_days(days: int) -> str:
    if days == 30:
        return ""
    return f"_{days}"


async def generate_all() -> None:
    unique_categories = sorted(set(CATEGORIES.values()))
    category = "Categories"
    list_days = [30, 60, 90, 180]
    for days in list_days:
        async for group in iterate_groups():
            document = format_data(
                data=await get_data_one_group(
                    group=group,
                    days=days,
                ),
                categories=unique_categories,
            )
            json_dump(
                document=document,
                entity="group",
                subject=group + get_subject_days(days),
                csv_document=format_csv_data(document=document, header=category),
            )

        async for org_id, _, org_groups in iterate_organizations_and_groups():
            document = format_data(
                data=await get_data_many_groups(
                    groups=org_groups,
                    days=days,
                ),
                categories=unique_categories,
            )
            json_dump(
                document=document,
                entity="organization",
                subject=org_id + get_subject_days(days),
                csv_document=format_csv_data(document=document, header=category),
            )

        async for org_id, org_name, _ in iterate_organizations_and_groups():
            for portfolio, groups in await get_portfolios_groups(org_name):
                document = format_data(
                    data=await get_data_many_groups(
                        groups=groups,
                        days=days,
                    ),
                    categories=unique_categories,
                )
                json_dump(
                    document=document,
                    entity="portfolio",
                    subject=f"{org_id}PORTFOLIO#{portfolio}" + get_subject_days(days),
                    csv_document=format_csv_data(document=document, header=category),
                )


def main() -> None:
    run(generate_all())
