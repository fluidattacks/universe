from decimal import (
    Decimal,
)
from operator import (
    attrgetter,
)

from aioextensions import (
    collect,
    run,
)
from async_lru import (
    alru_cache,
)

from integrates.charts import charts_utils
from integrates.charts.generators.bar_chart.utils import (
    LIMIT,
    format_data_csv,
)
from integrates.charts.generators.bar_chart.utils_top_vulnerabilities_by_source import (
    format_max_value,
)
from integrates.charts.generators.common.colors import (
    TYPES_COUNT,
)
from integrates.charts.generators.pie_chart.utils import (
    PortfoliosGroupsInfo,
)
from integrates.custom_utils.findings import (
    get_group_findings,
)
from integrates.dataloaders import (
    get_new_context,
)


@alru_cache(maxsize=None, typed=True)
async def get_data_one_group(group: str) -> PortfoliosGroupsInfo:
    context = get_new_context()
    group_findings = await get_group_findings(group_name=group, loaders=context)
    findings_found = len(group_findings)

    return PortfoliosGroupsInfo(
        group_name=group.lower(),
        value=Decimal(findings_found),
    )


async def get_data_many_groups(
    groups: tuple[str, ...],
) -> list[PortfoliosGroupsInfo]:
    groups_data = await collect(map(get_data_one_group, groups), workers=16)

    return sorted(groups_data, key=attrgetter("value"), reverse=True)


def format_data(
    all_data: list[PortfoliosGroupsInfo],
) -> tuple[dict, charts_utils.CsvData]:
    data = [group for group in all_data[:LIMIT] if group.value > Decimal("0.0")]

    json_data: dict = {
        "data": {
            "columns": [
                ["Types of Vulnerabilities"] + [str(group.value) for group in data],
            ],
            "colors": {"Types of Vulnerabilities": TYPES_COUNT},
            "labels": None,
            "type": "bar",
        },
        "legend": {"show": False},
        "exposureTrendsByCategories": True,
        "keepToltipColor": True,
        "axis": {
            "rotated": True,
            "x": {
                "categories": [group.group_name.capitalize() for group in data],
                "type": "category",
                "tick": {
                    "rotate": 0,
                    "multiline": False,
                },
            },
            "y": {
                "min": 0,
                "padding": {"bottom": 0},
            },
        },
        "barChartYTickFormat": True,
        "maxValue": format_max_value(
            [(group.group_name.capitalize(), group.value) for group in data]
        ),
    }

    csv_data = format_data_csv(
        header_value=str(json_data["data"]["columns"][0][0]),
        values=[group.value for group in all_data],
        categories=[group.group_name.capitalize() for group in all_data],
    )

    return (json_data, csv_data)


async def generate_all() -> None:
    async for org_id, _, org_groups in charts_utils.iterate_organizations_and_groups():
        json_document, csv_document = format_data(
            all_data=await get_data_many_groups(groups=org_groups)
        )
        charts_utils.json_dump(
            document=json_document,
            entity="organization",
            subject=org_id,
            csv_document=csv_document,
        )

    async for org_id, org_name, _ in charts_utils.iterate_organizations_and_groups():
        for portfolio, groups in await charts_utils.get_portfolios_groups(org_name):
            json_document, csv_document = format_data(
                all_data=await get_data_many_groups(groups=groups),
            )
            charts_utils.json_dump(
                document=json_document,
                entity="portfolio",
                subject=f"{org_id}PORTFOLIO#{portfolio}",
                csv_document=csv_document,
            )


def main() -> None:
    run(generate_all())
