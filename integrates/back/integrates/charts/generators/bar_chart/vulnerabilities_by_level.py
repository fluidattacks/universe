from collections import (
    Counter,
)

from aioextensions import (
    collect,
    run,
)
from async_lru import (
    alru_cache,
)

from integrates.charts import charts_utils
from integrates.charts.generators.bar_chart.utils import (
    format_vulnerabilities_by_data,
)
from integrates.custom_utils.findings import (
    get_group_findings,
)
from integrates.dataloaders import (
    get_new_context,
)


@alru_cache(maxsize=None, typed=True)
async def get_data_one_group(group: str) -> Counter[str]:
    context = get_new_context()
    group_findings = await get_group_findings(group_name=group, loaders=context)
    finding_ids = [finding.id for finding in group_findings]

    vulnerabilities = await context.finding_vulnerabilities_released_nzr.load_many_chained(
        finding_ids
    )

    return Counter(
        [
            str(vulnerability.custom_severity)
            for vulnerability in vulnerabilities
            if vulnerability.custom_severity is not None and vulnerability.custom_severity >= 0
        ]
    )


async def get_data_many_groups(groups: tuple[str, ...]) -> Counter[str]:
    groups_data = await collect(map(get_data_one_group, groups), workers=32)

    return sum(groups_data, Counter())


async def generate_all() -> None:
    column: str = "Level"
    async for group in charts_utils.iterate_groups():
        json_document, csv_document = format_vulnerabilities_by_data(
            counters=await get_data_one_group(group),
            column=column,
        )
        charts_utils.json_dump(
            document=json_document,
            entity="group",
            subject=group,
            csv_document=csv_document,
        )

    async for org_id, _, org_groups in charts_utils.iterate_organizations_and_groups():
        json_document, csv_document = format_vulnerabilities_by_data(
            counters=await get_data_many_groups(org_groups),
            column=column,
        )
        charts_utils.json_dump(
            document=json_document,
            entity="organization",
            subject=org_id,
            csv_document=csv_document,
        )

    async for org_id, org_name, _ in charts_utils.iterate_organizations_and_groups():
        for portfolio, groups in await charts_utils.get_portfolios_groups(org_name):
            json_document, csv_document = format_vulnerabilities_by_data(
                counters=await get_data_many_groups(groups),
                column=column,
            )
            charts_utils.json_dump(
                document=json_document,
                entity="portfolio",
                subject=f"{org_id}PORTFOLIO#{portfolio}",
                csv_document=csv_document,
            )


def main() -> None:
    run(generate_all())
