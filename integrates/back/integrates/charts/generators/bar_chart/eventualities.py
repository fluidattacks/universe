from decimal import (
    Decimal,
)
from operator import (
    attrgetter,
)

from aioextensions import (
    collect,
    run,
)
from async_lru import (
    alru_cache,
)

from integrates.charts import charts_utils
from integrates.charts.generators.bar_chart.utils import (
    LIMIT,
    format_data_csv,
)
from integrates.charts.generators.bar_chart.utils_top_vulnerabilities_by_source import (
    format_max_value,
)
from integrates.charts.generators.common.colors import (
    OTHER_COUNT,
)
from integrates.charts.generators.pie_chart.utils import (
    PortfoliosGroupsInfo,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.events.enums import (
    EventStateStatus,
)
from integrates.db_model.events.types import (
    GroupEventsRequest,
)


@alru_cache(maxsize=None, typed=True)
async def get_data_one_group(*, group: str) -> PortfoliosGroupsInfo:
    loaders = get_new_context()
    events_group = await loaders.group_events.load(GroupEventsRequest(group_name=group))

    return PortfoliosGroupsInfo(
        group_name=group.lower(),
        value=Decimal(
            len([event for event in events_group if event.state.status != EventStateStatus.SOLVED])
        ),
    )


async def get_data_many_groups(*, groups: tuple[str, ...]) -> list[PortfoliosGroupsInfo]:
    groups_data = await collect(
        tuple(get_data_one_group(group=group) for group in groups),
        workers=32,
    )

    return sorted(groups_data, key=attrgetter("value"), reverse=True)


def format_data(
    data: list[PortfoliosGroupsInfo],
) -> tuple[dict, charts_utils.CsvData]:
    limited_data = [group for group in data[:LIMIT] if group.value > 0]

    json_data: dict = {
        "data": {
            "columns": [
                ["Unsolved Events"] + [str(group.value) for group in limited_data],
            ],
            "colors": {
                "Unsolved Events": OTHER_COUNT,
            },
            "labels": None,
            "type": "bar",
        },
        "legend": {
            "show": False,
        },
        "axis": {
            "rotated": True,
            "x": {
                "categories": [group.group_name.capitalize() for group in limited_data],
                "type": "category",
                "tick": {
                    "multiline": False,
                    "outer": False,
                    "rotate": 0,
                },
            },
            "y": {
                "min": 0,
                "padding": {
                    "bottom": 0,
                },
            },
        },
        "barChartYTickFormat": True,
        "maxValue": format_max_value([(group.group_name, group.value) for group in limited_data]),
        "exposureTrendsByCategories": True,
        "keepToltipColor": True,
    }
    csv_data = format_data_csv(
        header_value=str(json_data["data"]["columns"][0][0]),
        values=[group.value for group in data],
        categories=[group.group_name for group in data],
    )

    return (json_data, csv_data)


async def generate_all() -> None:
    async for org_id, _, org_groups in charts_utils.iterate_organizations_and_groups():
        json_document, csv_document = format_data(
            data=await get_data_many_groups(groups=org_groups)
        )
        charts_utils.json_dump(
            document=json_document,
            entity="organization",
            subject=org_id,
            csv_document=csv_document,
        )

    async for org_id, org_name, _ in charts_utils.iterate_organizations_and_groups():
        for portfolio, groups in await charts_utils.get_portfolios_groups(org_name):
            json_document, csv_document = format_data(
                data=await get_data_many_groups(groups=tuple(groups)),
            )
            charts_utils.json_dump(
                document=json_document,
                entity="portfolio",
                subject=f"{org_id}PORTFOLIO#{portfolio}",
                csv_document=csv_document,
            )


def main() -> None:
    run(generate_all())
