from datetime import (
    date as datetype,
)
from decimal import (
    Decimal,
)

from aioextensions import (
    collect,
    run,
)
from aiohttp import (
    ClientConnectorError,
)
from aiohttp.client_exceptions import (
    ClientPayloadError,
    ServerTimeoutError,
)
from async_lru import (
    alru_cache,
)
from botocore.exceptions import (
    ClientError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
)

from integrates.charts.generators.bar_chart.utils import (
    Benchmarking,
    generate_all_mttr_benchmarking,
    get_vulnerability_reattacks_date,
)
from integrates.custom_exceptions import (
    UnavailabilityError as CustomUnavailabilityError,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.roots.enums import (
    RootStatus,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityRequest,
    VulnerabilityVerification,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.dynamodb.exceptions import (
    UnavailabilityError,
)
from integrates.groups import (
    domain as groups_domain,
)
from integrates.groups.domain import (
    get_mean_remediate_severity_cvssf,
)


@retry_on_exceptions(
    exceptions=(
        ClientConnectorError,
        ClientError,
        ClientPayloadError,
        ConnectionResetError,
        ConnectTimeoutError,
        CustomUnavailabilityError,
        HTTPClientError,
        ReadTimeoutError,
        ServerTimeoutError,
        UnavailabilityError,
    ),
    sleep_seconds=40,
    max_attempts=10,
)
@alru_cache(maxsize=None, typed=True)
async def get_historic_verification(
    vulnerability: Vulnerability,
) -> list[VulnerabilityVerification]:
    loaders = get_new_context()

    return await loaders.vulnerability_historic_verification.load(
        VulnerabilityRequest(
            vulnerability_id=vulnerability.id,
            finding_id=vulnerability.finding_id,
        )
    )


@alru_cache(maxsize=None, typed=True)
async def get_data_one_group(group: str, min_date: datetype | None = None) -> Benchmarking:
    loaders = get_new_context()
    group_vulns = await groups_domain.get_group_vulns_with_severity(loaders, group)

    if min_date:
        historics_verification = await collect(
            [
                get_historic_verification(vulnerability)
                for vulnerability in group_vulns
                if vulnerability.verification
            ],
            workers=4,
        )
        number_of_reattacks = sum(
            get_vulnerability_reattacks_date(historic_verification=historic, min_date=min_date)
            for historic in historics_verification
        )
    else:
        number_of_reattacks = sum(
            vulnerability.unreliable_indicators.unreliable_reattack_cycles or 0
            for vulnerability in group_vulns
        )

    mttr = await get_mean_remediate_severity_cvssf(
        loaders,
        group.lower(),
        Decimal("0.0"),
        Decimal("10.0"),
        min_date=min_date,
    )
    valid_roots = tuple(
        root
        for root in await loaders.group_roots.load(group)
        if root.state.status == RootStatus.ACTIVE
    )

    return Benchmarking(
        is_valid=(number_of_reattacks > 10 and len(valid_roots) > 0),
        number_of_active_roots=len(valid_roots),
        subject=group.lower(),
        mttr=mttr,
        number_of_reattacks=number_of_reattacks,
    )


def main() -> None:
    run(
        generate_all_mttr_benchmarking(
            get_data_one_group=get_data_one_group,
            alternative="Mean time to remediate per exposure benchmarking",
        )
    )
