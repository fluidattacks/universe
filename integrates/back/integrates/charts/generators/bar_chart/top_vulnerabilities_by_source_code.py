from aioextensions import (
    run,
)

from integrates.charts.generators.bar_chart.utils_top_vulnerabilities_by_source import (
    generate_all,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityType,
)


def main() -> None:
    run(generate_all(source=VulnerabilityType.LINES))
