from collections import (
    Counter,
)
from decimal import (
    Decimal,
)
from itertools import (
    groupby,
)

from aioextensions import (
    collect,
    run,
)
from async_lru import (
    alru_cache,
)

from integrates.charts import charts_utils
from integrates.charts.generators.bar_chart.utils import (
    LIMIT,
    format_data_csv,
    generate_all_top_vulnerabilities,
)
from integrates.charts.generators.bar_chart.utils_top_vulnerabilities_by_source import (
    format_max_value,
)
from integrates.charts.generators.common.colors import (
    EXPOSURE,
)
from integrates.charts.generators.common.utils import (
    get_finding_name,
    get_finding_url,
)
from integrates.custom_utils.findings import (
    get_group_findings,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
)
from integrates.groups import (
    domain as groups_domain,
)
from integrates.mailer.utils import (
    get_organization_name,
)


@alru_cache(maxsize=None, typed=True)
async def get_data_one_group(group: str) -> Counter[str]:
    loaders = get_new_context()
    organization_name = await get_organization_name(loaders, group)
    group_findings = await get_group_findings(group_name=group, loaders=loaders)
    group_vulns = await groups_domain.get_group_vulns_with_severity(loaders, group)
    vulns_finding_title = [
        next(finding for finding in group_findings if finding.id == vuln.finding_id).title
        for vuln in group_vulns
    ]
    vulns_counter = [
        {
            f"orgs/{organization_name}/groups/{group}/"
            f"vulns/{vuln.finding_id}/{finding_title}": (vuln.severity_score.cvssf_v4)
        }
        for vuln, finding_title in zip(group_vulns, vulns_finding_title, strict=False)
        if vuln.state.status == VulnerabilityStateStatus.VULNERABLE and vuln.severity_score
    ]

    return sum([Counter(source) for source in vulns_counter], Counter())


async def get_data_many_groups(groups: list[str]) -> Counter[str]:
    groups_data = await collect(
        tuple(get_data_one_group(group) for group in groups),
        workers=32,
    )

    return sum(groups_data, Counter())


def format_data(counters: Counter[str], is_group: bool) -> tuple[dict, charts_utils.CsvData]:
    data: list[tuple[str, int]] = counters.most_common()
    merged_data: list[list[int | str]] = []
    original_ids: list[list[str | int]] = []
    for axis, columns in groupby(
        sorted(data, key=lambda x: get_finding_name([x[0]])),
        key=lambda x: get_finding_name([x[0]]),
    ):
        _columns = list(columns)
        merged_data.append([axis, sum(value for _, value in _columns)])
        if _columns:
            original_ids.append(
                [
                    get_finding_url(sorted(_columns, key=lambda a: a[1])[0][0]),
                    sum(value for _, value in _columns),
                ]
            )
    merged_data = sorted(merged_data, key=lambda x: x[1], reverse=True)
    original_ids = sorted(original_ids, key=lambda x: x[1], reverse=True)[:LIMIT]
    limited_merged_data = merged_data[:LIMIT]

    json_data: dict = {
        "data": {
            "columns": [
                [
                    "Open Exposure",
                    *[
                        charts_utils.format_cvssf_log(Decimal(value))
                        for _, value in limited_merged_data
                    ],
                ],
            ],
            "colors": {"Open Exposure": EXPOSURE},
            "labels": None,
            "type": "bar",
        },
        "legend": {"show": False},
        "padding": {"bottom": 0},
        "axis": {
            "rotated": True,
            "x": {
                "categories": [get_finding_name([str(key)]) for key, _ in limited_merged_data],
                "type": "category",
                "tick": {
                    "multiline": False,
                    "outer": False,
                    "rotate": 0,
                },
            },
            "y": {
                "label": {
                    "text": "CVSSF",
                    "position": "outer-top",
                },
                "min": 0,
                "padding": {"bottom": 0},
            },
        },
        "exposureTrendsByCategories": True,
        "keepToltipColor": True,
        "maxValue": format_max_value(
            [(str(key), Decimal(value)) for key, value in limited_merged_data]
        ),
        "maxValueLog": format_max_value(
            [
                (str(key), charts_utils.format_cvssf_log(Decimal(value)))
                for key, value in limited_merged_data
            ]
        ),
        "originalValues": [
            charts_utils.format_cvssf(Decimal(value)) for _, value in limited_merged_data
        ],
        "originalIds": [key for key, _ in original_ids] if is_group else None,
    }

    csv_data = format_data_csv(
        header_value=str(json_data["data"]["columns"][0][0]),
        values=[charts_utils.format_cvssf(Decimal(value)) for _, value in merged_data],
        categories=[str(group) for group, _ in merged_data],
    )

    return (json_data, csv_data)


def main() -> None:
    run(
        generate_all_top_vulnerabilities(
            get_data_one_group=get_data_one_group,
            get_data_many_groups=get_data_many_groups,
            format_data=format_data,
        )
    )
