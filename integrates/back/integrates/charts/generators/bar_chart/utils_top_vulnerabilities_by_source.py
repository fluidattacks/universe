from collections import (
    Counter,
)
from decimal import (
    Decimal,
)
from itertools import (
    groupby,
)

from aioextensions import (
    collect,
)
from async_lru import (
    alru_cache,
)

from integrates.charts.charts_utils import (
    CsvData,
    format_cvssf,
    format_cvssf_log,
    get_portfolios_groups,
    iterate_groups,
    iterate_organizations_and_groups,
    json_dump,
)
from integrates.charts.generators.bar_chart.utils import (
    LIMIT,
    format_data_csv,
)
from integrates.charts.generators.common.colors import (
    EXPOSURE,
)
from integrates.charts.generators.common.utils import (
    get_finding_name,
    get_finding_url,
)
from integrates.custom_utils.findings import (
    get_group_findings,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
    VulnerabilityType,
)
from integrates.groups import (
    domain as groups_domain,
)
from integrates.mailer.utils import (
    get_organization_name,
)


def format_max_value(data: list[tuple[str, Decimal]]) -> Decimal:
    if data:
        return data[0][1] if data[0][1] else Decimal("1.0")
    return Decimal("1.0")


@alru_cache(maxsize=None, typed=True)
async def get_data_one_group(group: str, source: VulnerabilityType) -> Counter[str]:
    loaders = get_new_context()
    organization_name = await get_organization_name(loaders, group)
    group_findings = await get_group_findings(group_name=group, loaders=loaders)
    group_vulns = await groups_domain.get_group_vulns_with_severity(loaders, group)
    vulns_finding_title = [
        next(finding for finding in group_findings if finding.id == vuln.finding_id).title
        for vuln in group_vulns
    ]
    vulns_by_source = [
        {
            f"orgs/{organization_name}/groups/{group}/"
            f"vulns/{vuln.finding_id}/{finding_title}": (vuln.severity_score.cvssf_v4)
        }
        for vuln, finding_title in zip(group_vulns, vulns_finding_title, strict=False)
        if vuln.state.status == VulnerabilityStateStatus.VULNERABLE
        and vuln.type == source
        and vuln.severity_score
    ]

    return sum([Counter(source) for source in vulns_by_source], Counter())


async def get_data_many_groups(groups: tuple[str, ...], source: VulnerabilityType) -> Counter[str]:
    groups_data = await collect(
        tuple(get_data_one_group(group, source) for group in groups),
        workers=32,
    )

    return sum(groups_data, Counter())


def format_data(
    counters: Counter[str], source: VulnerabilityType, is_group: bool
) -> tuple[dict, CsvData]:
    translations = {
        VulnerabilityType.INPUTS: "App",
        VulnerabilityType.LINES: "Code",
        VulnerabilityType.PORTS: "Infra",
    }
    all_data: list[tuple[str, int]] = counters.most_common()
    merged_data: list[list[str | int]] = []
    original_ids: list[list[str | int]] = []
    for axis, columns in groupby(
        sorted(all_data, key=lambda x: get_finding_name([x[0]])),
        key=lambda x: get_finding_name([x[0]]),
    ):
        _columns = list(columns)
        merged_data.append([axis, sum(value for _, value in _columns)])
        if _columns:
            original_ids.append(
                [
                    get_finding_url(sorted(_columns, key=lambda a: a[1])[0][0]),
                    sum(value for _, value in _columns),
                ]
            )

    merged_data = sorted(merged_data, key=lambda x: x[1], reverse=True)
    original_ids = sorted(original_ids, key=lambda x: x[1], reverse=True)[:LIMIT]
    data = merged_data[:LIMIT]
    legend: str = f"{translations[source]} open exposure"

    json_data = {
        "data": {
            "columns": [
                [
                    legend,
                    *[format_cvssf_log(Decimal(value)) for _, value in data],
                ],
            ],
            "colors": {legend: EXPOSURE},
            "labels": None,
            "type": "bar",
        },
        "legend": {"show": False},
        "padding": {"bottom": 0},
        "axis": {
            "rotated": True,
            "x": {
                "categories": [key for key, _ in data],
                "type": "category",
                "tick": {
                    "multiline": False,
                    "outer": False,
                    "rotate": 0,
                },
            },
            "y": {
                "label": {
                    "text": "CVSSF",
                    "position": "outer-top",
                },
                "min": 0,
                "padding": {"bottom": 0},
            },
        },
        "maxValue": format_max_value([(str(key), Decimal(value)) for key, value in data]),
        "maxValueLog": format_max_value(
            [(str(key), format_cvssf_log(Decimal(value))) for key, value in data]
        ),
        "originalValues": [format_cvssf(Decimal(value)) for _, value in data],
        "exposureTrendsByCategories": True,
        "keepToltipColor": True,
        "originalIds": [key for key, _ in original_ids] if is_group else None,
    }

    csv_data = format_data_csv(
        header_value=legend,
        values=[format_cvssf(Decimal(value)) for _, value in all_data],
        categories=[name for name, _ in all_data],
        header_title="Type",
    )

    return (json_data, csv_data)


async def generate_all(*, source: VulnerabilityType) -> None:
    async for group in iterate_groups():
        json_document, csv_document = format_data(
            await get_data_one_group(group, source), source, True
        )
        json_dump(
            document=json_document,
            entity="group",
            subject=group,
            csv_document=csv_document,
        )

    async for org_id, _, org_groups in iterate_organizations_and_groups():
        json_document, csv_document = format_data(
            await get_data_many_groups(org_groups, source),
            source,
            False,
        )
        json_dump(
            document=json_document,
            entity="organization",
            subject=org_id,
            csv_document=csv_document,
        )

    async for org_id, name, org_groups in iterate_organizations_and_groups():
        for portfolio, groups in await get_portfolios_groups(name, org_groups=org_groups):
            json_document, csv_document = format_data(
                await get_data_many_groups(tuple(groups), source),
                source,
                False,
            )
            json_dump(
                document=json_document,
                entity="portfolio",
                subject=f"{org_id}PORTFOLIO#{portfolio}",
                csv_document=csv_document,
            )
