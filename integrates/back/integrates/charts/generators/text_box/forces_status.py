from aioextensions import (
    run,
)

from integrates.charts import charts_utils
from integrates.charts.generators.text_box.utils import (
    ForcesReport,
    format_csv_data,
)


async def generate_one() -> ForcesReport:
    # By default, Forces is enabled for all groups
    # https://gitlab.com/fluidattacks/universe/-/issues/4880
    return ForcesReport(fontSizeRatio=0.5, text="Active")


async def generate_all() -> None:
    title: str = "Service status"
    async for group in charts_utils.iterate_groups():
        document = await generate_one()
        charts_utils.json_dump(
            document=document,
            entity="group",
            subject=group,
            csv_document=format_csv_data(header=title, value=document["text"]),
        )


def main() -> None:
    run(generate_all())
