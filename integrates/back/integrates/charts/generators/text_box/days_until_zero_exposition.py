from decimal import (
    ROUND_CEILING,
    Decimal,
)

from aioextensions import (
    collect,
    run,
)
from async_lru import (
    alru_cache,
)

from integrates.charts import charts_utils
from integrates.custom_utils.findings import (
    get_group_findings,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
)


@alru_cache(maxsize=None, typed=True)
async def get_data_one_group(group_name: str) -> Decimal:
    loaders = get_new_context()
    group_findings = await get_group_findings(group_name=group_name, loaders=loaders)
    finding_vulns = await loaders.finding_vulnerabilities_released_nzr.load_many(
        [finding.id for finding in group_findings]
    )

    counter = Decimal("0.0")
    for finding, vulnerabilities in zip(group_findings, finding_vulns, strict=False):
        for vulnerability in vulnerabilities:
            if vulnerability.state.status == VulnerabilityStateStatus.VULNERABLE:
                if finding.min_time_to_remediate:
                    counter += Decimal(finding.min_time_to_remediate)
                else:
                    counter += Decimal("60.0")

    minutes_days: Decimal = Decimal("24.0") * Decimal("60.0")

    return Decimal(counter / minutes_days).quantize(Decimal("0.001"))


async def get_data_many_groups(groups: tuple[str, ...]) -> Decimal:
    groups_data = await collect(
        tuple(get_data_one_group(group) for group in groups),
        workers=32,
    )

    return Decimal(sum(group for group in groups_data))


def format_data(days: Decimal) -> dict[str, float | Decimal]:
    return {
        "fontSizeRatio": 0.5,
        "text": days.to_integral_exact(rounding=ROUND_CEILING),
    }


def format_csv_data(days: Decimal) -> charts_utils.CsvData:
    return charts_utils.CsvData(
        headers=["Days until zero exposure"],
        rows=[[str(days.to_integral_exact(rounding=ROUND_CEILING))]],
    )


async def generate_all() -> None:
    days: Decimal
    async for group in charts_utils.iterate_groups():
        days = await get_data_one_group(group)
        charts_utils.json_dump(
            document=format_data(days=days),
            entity="group",
            subject=group,
            csv_document=format_csv_data(days=days),
        )

    async for org_id, _, org_groups in charts_utils.iterate_organizations_and_groups():
        days = await get_data_many_groups(org_groups)
        charts_utils.json_dump(
            document=format_data(days=days),
            entity="organization",
            subject=org_id,
            csv_document=format_csv_data(days=days),
        )

    async for org_id, org_name, _ in charts_utils.iterate_organizations_and_groups():
        for portfolio, groups in await charts_utils.get_portfolios_groups(org_name):
            days = await get_data_many_groups(tuple(groups))
            charts_utils.json_dump(
                document=format_data(days=days),
                entity="portfolio",
                subject=f"{org_id}PORTFOLIO#{portfolio}",
                csv_document=format_csv_data(days=days),
            )


def main() -> None:
    run(generate_all())
