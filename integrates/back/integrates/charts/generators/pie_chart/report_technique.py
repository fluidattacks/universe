from collections import (
    Counter,
)

from aioextensions import (
    collect,
    run,
)
from async_lru import (
    alru_cache,
)

from integrates.charts.generators.common.colors import (
    VULNS_COLORS,
)
from integrates.charts.generators.pie_chart.utils import (
    generate_all,
)
from integrates.custom_utils.findings import (
    get_group_findings,
)
from integrates.dataloaders import (
    get_new_context,
)


@alru_cache(maxsize=None, typed=True)
async def get_data_one_group(group: str) -> Counter[str]:
    loaders = get_new_context()
    findings = await get_group_findings(group_name=group, loaders=loaders)
    vulnerabilities = await loaders.finding_vulnerabilities_released_nzr.load_many_chained(
        [finding.id for finding in findings]
    )

    return Counter(
        tuple(
            vulnerability.technique if vulnerability.technique else "PTAAS"
            for vulnerability in vulnerabilities
        )
    )


async def get_data_many_groups(groups: tuple[str, ...]) -> Counter[str]:
    return sum(
        await collect(
            tuple(get_data_one_group(group=group) for group in groups),
            workers=32,
        ),
        Counter(),
    )


def format_data(data: Counter[str]) -> dict:
    return {
        "data": {
            "columns": [
                ["PTAAS", data["PTAAS"]],
                ["SCR", data["SCR"]],
                ["SAST", data["SAST"]],
                ["DAST", data["DAST"]],
                ["SCA", data["SCA"]],
                ["RE", data["RE"]],
                ["CSPM", data["CSPM"]],
            ],
            "type": "pie",
            "colors": {
                "PTAAS": VULNS_COLORS.complementary_low,
                "SCR": VULNS_COLORS.warning_high,
                "SAST": VULNS_COLORS.warning_low,
                "DAST": VULNS_COLORS.informational_low,
                "SCA": VULNS_COLORS.informational_high,
                "RE": VULNS_COLORS.success_high,
                "CSPM": VULNS_COLORS.success_low,
            },
        },
        "legend": {"position": "right"},
        "pie": {"label": {"show": True}},
    }


def main() -> None:
    run(
        generate_all(
            get_data_one_group=get_data_one_group,
            get_data_many_groups=get_data_many_groups,
            format_document=format_data,
            header=["Report Technique", "Occurrences"],
        )
    )
