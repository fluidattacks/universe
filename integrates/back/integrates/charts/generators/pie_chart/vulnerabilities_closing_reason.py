import logging
import logging.config
from collections import (
    Counter,
)
from collections.abc import (
    Iterable,
)

from aioextensions import (
    collect,
    run,
)

from integrates.charts.generators.common.colors import (
    VULNS_COLORS,
)
from integrates.charts.generators.pie_chart.utils import (
    generate_all,
)
from integrates.custom_utils.findings import (
    get_group_findings,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateReason,
    VulnerabilityStateStatus,
    VulnerabilityZeroRiskStatus,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
)
from integrates.settings import (
    LOGGING,
)

LOGGER = logging.getLogger(__name__)

logging.config.dictConfig(LOGGING)


def _filter_safe_and_non_zero_risk(
    vulnerabilities: list[Vulnerability],
) -> Iterable[Vulnerability]:
    zero_risk_status = {
        VulnerabilityZeroRiskStatus.CONFIRMED,
        VulnerabilityZeroRiskStatus.REQUESTED,
    }
    return (
        vuln
        for vuln in vulnerabilities
        if vuln.state.status == VulnerabilityStateStatus.SAFE
        and (not vuln.zero_risk or vuln.zero_risk.status not in zero_risk_status)
    )


async def get_data_one_group(group: str) -> Counter[str]:
    LOGGER.info("Processing group %s", group)
    loaders = get_new_context()
    group_findings = await get_group_findings(group_name=group, loaders=loaders)
    finding_ids = [finding.id for finding in group_findings]

    vulnerabilities = await loaders.finding_vulnerabilities.load_many_chained(finding_ids)

    return Counter(
        tuple(
            map(
                lambda vuln: vuln.state.reasons[0]
                if vuln.state.reasons
                else VulnerabilityStateReason.NO_JUSTIFICATION,
                _filter_safe_and_non_zero_risk(vulnerabilities),
            )
        )
    )


async def get_data_many_groups(groups: tuple[str, ...]) -> Counter[str]:
    return sum(
        await collect(
            tuple(get_data_one_group(group) for group in groups),
            workers=32,
        ),
        Counter(),
    )


def format_data(data: Counter[str]) -> dict:
    deactivation = VulnerabilityStateReason.ROOT_OR_ENVIRONMENT_DEACTIVATED
    return {
        "data": {
            "columns": [
                [
                    "Closed By Scanner",
                    data[VulnerabilityStateReason.CLOSED_BY_MACHINE]
                    + data[VulnerabilityStateReason.MODIFIED_BY_MACHINE],
                ],
                [
                    "Verified by Hackers",
                    data[VulnerabilityStateReason.VERIFIED_AS_SAFE]
                    + data[VulnerabilityStateReason.FUNCTIONALITY_NO_LONGER_EXISTS],
                ],
                ["Exclusion", data[VulnerabilityStateReason.EXCLUSION]],
                [
                    "Root Moved",
                    data[VulnerabilityStateReason.ROOT_MOVED_TO_ANOTHER_GROUP],
                ],
                ["Environment or Root Deactivated", data[deactivation]],
                [
                    "Environment or Root Deleted",
                    data[VulnerabilityStateReason.ENVIRONMENT_DELETED],
                ],
                ["Consistency", data[VulnerabilityStateReason.CONSISTENCY]],
                [
                    "No Justification",
                    data[VulnerabilityStateReason.NO_JUSTIFICATION],
                ],
            ],
            "type": "pie",
            "colors": {
                "Closed By Scanner": VULNS_COLORS.informational_high,
                "Verified by Hackers": VULNS_COLORS.success_low,
                "Exclusion": VULNS_COLORS.complementary_high,
                "Root Moved": VULNS_COLORS.complementary_low,
                "Environment or Root Deactivated": VULNS_COLORS.warning_high,
                "Environment or Root Deleted": VULNS_COLORS.warning_low,
                "Consistency": VULNS_COLORS.success_high,
                "No Justification": VULNS_COLORS.informational_low,
            },
        },
        "legend": {"position": "right"},
        "pie": {"label": {"show": True}},
    }


def main() -> None:
    run(
        generate_all(
            get_data_one_group=get_data_one_group,
            get_data_many_groups=get_data_many_groups,
            format_document=format_data,
            header=["Vulnerabilities closing reason", "Occurrences"],
        )
    )
