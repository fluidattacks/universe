import logging
import logging.config
from itertools import chain

import gitlab
from aioextensions import collect, in_thread
from gitlab.exceptions import GitlabAuthenticationError, GitlabListError
from requests.exceptions import RequestException

from integrates.dataloaders import Dataloaders
from integrates.db_model.azure_repositories.constants import MAX_BRANCHES
from integrates.db_model.azure_repositories.get import get_gitlab_projects
from integrates.db_model.azure_repositories.types import ProjectStats
from integrates.db_model.azure_repositories.utils import does_not_exist_in_gitroot_urls
from integrates.db_model.credentials.types import Credentials, OauthGitlabSecret
from integrates.db_model.integration_repositories.types import OrganizationIntegrationRepository
from integrates.oauth.common import get_credential_token
from integrates.outside_repositories.domain import __get_id
from integrates.settings import LOGGING

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger(__name__)


async def get_gitlab_branches_names(
    *,
    credential: Credentials,
    project_id: str,
    loaders: Dataloaders,
) -> tuple[str, ...]:
    token = await get_credential_token(
        credential=credential,
        loaders=loaders,
    )
    if not token:
        return tuple()

    return await in_thread(_get_gitlab_branches_names, token, project_id)


def _get_gitlab_branches_names(
    token: str,
    project_id: str,
) -> tuple[str, ...]:
    try:
        with gitlab.Gitlab(oauth_token=token, retry_transient_errors=True) as g_session:
            project = g_session.projects.get(project_id)
            if bool(project.attributes["empty_repo"]):
                return tuple()

            branches = project.branches.list(
                per_page=MAX_BRANCHES,
                page=1,
                sort="updated_desc",
                max_retries=15,
            )

            return tuple(branch.name for branch in branches)[:MAX_BRANCHES]
    except (
        gitlab.GitlabAuthenticationError,
        KeyError,
        gitlab.GitlabListError,
    ) as exc:
        if "401" in str(exc):
            LOGGER.error(
                "Invalid token",
                extra={
                    "extra": {
                        "exception": exc,
                        "project_id": project_id,
                    },
                },
            )
        elif "404" in str(exc):
            LOGGER.error(
                "Repository was not Found",
                extra={
                    "extra": {
                        "exception": exc,
                        "project_id": project_id,
                    },
                },
            )
        else:
            LOGGER.error(
                "Error getting gitlab branches",
                extra={"extra": {"exception": exc, "project_id": project_id}},
            )
    return tuple()


async def get_gitlab_commit(
    *,
    loaders: Dataloaders,
    credential: Credentials,
    project_id: str,
) -> tuple[dict, ...]:
    token = await get_credential_token(
        credential=credential,
        loaders=loaders,
    )
    if not token:
        return tuple()

    return await in_thread(_get_gitlab_commit, token=token, project_id=project_id)


def _get_gitlab_commit(token: str, project_id: str) -> tuple[dict, ...]:
    try:
        with gitlab.Gitlab(oauth_token=token) as g_session:
            project = g_session.projects.get(project_id)

            if bool(project.attributes["empty_repo"]):
                return tuple()
            commits = project.commits.list(get_all=True, order_by="default")

            return tuple(commit.attributes for commit in commits)
    except (
        GitlabAuthenticationError,
        GitlabListError,
        gitlab.GitlabGetError,
        ConnectionError,
        RequestException,
    ) as exc:
        LOGGER.exception(exc, extra={"extra": locals()})
    return tuple()


async def _get_gitlab_outside_repositories_stats(
    *,
    credential: Credentials,
    urls: set[str],
    nicknames: set[str],
    loaders: Dataloaders,
    get_all: bool = False,
) -> tuple[ProjectStats, ...]:
    if isinstance(credential.secret, OauthGitlabSecret):
        token = await get_credential_token(
            credential=credential,
            loaders=loaders,
        )
        if not token:
            return tuple()

        projects = await get_gitlab_projects(
            token=token,
            credential_id=credential.id,
        )
        projects = tuple(
            project
            for project in projects
            if does_not_exist_in_gitroot_urls(repository=project, urls=urls, nicknames=nicknames)
        )
        repositories_branches: tuple[tuple[str, ...], ...] = tuple([])
        if get_all:
            commits = await collect(
                [
                    get_gitlab_commit(
                        loaders=loaders,
                        credential=credential,
                        project_id=project.id,
                    )
                    for project in projects
                ],
                workers=2,
            )
            sorted_commits = tuple(
                tuple(
                    sorted(
                        p_commits,
                        key=lambda x: x["committed_date"],
                        reverse=True,
                    ),
                )
                for p_commits in commits
            )
        else:
            repositories_branches = await collect(
                [
                    get_gitlab_branches_names(
                        credential=credential,
                        project_id=project.id,
                        loaders=loaders,
                    )
                    for project in projects
                ],
                workers=20,
            )

        return tuple(
            ProjectStats(
                project=project._replace(
                    branches=tuple(set(repositories_branches[index] + project.branches))
                    if repositories_branches
                    else project.branches,
                ),
                commits=sorted_commits[index] if get_all and sorted_commits else tuple(),
                credential=credential,
            )
            for index, project in enumerate(projects)
        )

    return tuple()


async def get_gitlab_outside_authors_stats(
    *,
    credentials: list[Credentials],
    urls: set[str],
    nicknames: set[str],
    loaders: Dataloaders,
) -> set[str]:
    stats = tuple(
        chain.from_iterable(
            await collect(
                tuple(
                    _get_gitlab_outside_repositories_stats(
                        credential=credential,
                        urls=urls,
                        nicknames=nicknames,
                        loaders=loaders,
                        get_all=True,
                    )
                    for credential in credentials
                ),
                workers=1,
            ),
        ),
    )
    filtered_stats = tuple({stat.project.id: stat for stat in stats}.values())

    return {commit["author_email"].lower() for stat in filtered_stats for commit in stat.commits}


async def get_gitlab_outside_repositories_stats(
    *,
    credentials: list[Credentials],
    urls: set[str],
    nicknames: set[str],
    loaders: Dataloaders,
    organization_id: str,
) -> tuple[OrganizationIntegrationRepository, ...]:
    stats = tuple(
        chain.from_iterable(
            await collect(
                tuple(
                    _get_gitlab_outside_repositories_stats(
                        credential=credential,
                        urls=urls,
                        nicknames=nicknames,
                        loaders=loaders,
                    )
                    for credential in credentials
                ),
                workers=1,
            ),
        ),
    )
    filtered_stats = tuple({stat.project.id: stat for stat in stats}.values())

    return tuple(
        OrganizationIntegrationRepository(
            id=__get_id(stat.project.remote_url),
            organization_id=organization_id,
            branch=stat.project.branch,
            last_commit_date=stat.project.last_activity_at,
            url=stat.project.remote_url,
            credential_id=stat.credential.id,
            branches=stat.project.branches,
            name=stat.project.name,
        )
        for stat in filtered_stats
    )
