import json
import logging
import logging.config
from datetime import (
    datetime,
)
from itertools import (
    chain,
)

from aioextensions import (
    collect,
    in_thread,
)
from atlassian.bitbucket.cloud.base import (
    BitbucketCloudBase,
)
from requests.exceptions import (
    HTTPError,
)

from integrates.context import (
    FI_BITBUCKET_OAUTH2_REPOSITORY_APP_ID,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.azure_repositories.constants import (
    MAX_BRANCHES,
)
from integrates.db_model.azure_repositories.get import (
    get_bitbucket_authors,
    get_bitbucket_repositories,
)
from integrates.db_model.azure_repositories.types import (
    ProjectStats,
)
from integrates.db_model.azure_repositories.utils import (
    does_not_exist_in_gitroot_urls,
)
from integrates.db_model.credentials.types import (
    Credentials,
    OauthBitbucketSecret,
)
from integrates.db_model.integration_repositories.types import (
    OrganizationIntegrationRepository,
)
from integrates.oauth.common import (
    get_credential_token,
)
from integrates.outside_repositories.domain import (
    __get_id,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger(__name__)


async def get_bitbucket_outside_authors_stats(
    *,
    credentials: list[Credentials],
    urls: set[str],
    nicknames: set[str],
    loaders: Dataloaders,
) -> set[str]:
    stats = tuple(
        chain.from_iterable(
            await collect(
                tuple(
                    _get_bitbucket_repositories_stats(
                        credential=credential,
                        urls=urls,
                        nicknames=nicknames,
                        loaders=loaders,
                        get_all=True,
                    )
                    for credential in credentials
                ),
                workers=1,
            ),
        ),
    )
    filtered_stats = tuple({stat.project.id: stat for stat in stats}.values())

    return {str(commit["author"]).lower() for stat in filtered_stats for commit in stat.commits}


async def get_bitbucket_branches_names(
    *,
    credential: Credentials,
    repo_id: str,
    loaders: Dataloaders,
) -> tuple[str, ...]:
    token = await get_credential_token(
        credential=credential,
        loaders=loaders,
    )
    if not token:
        return tuple()

    oauth2_dict = {
        "client_id": FI_BITBUCKET_OAUTH2_REPOSITORY_APP_ID,
        "token": {"access_token": token},
    }
    workspace, slug = repo_id.rsplit("#REPOSITORY#", 1)

    return await in_thread(
        _get_bitbucket_branches_names,
        workspace,
        slug,
        oauth2_dict,
        credential.organization_id,
    )


def _get_bitbucket_branches_names(
    workspace_uuid: str,
    repo_slug: str,
    oauth2: dict,
    organization_id: str,
) -> tuple[str, ...]:
    branches = []
    try:
        base_cloud = BitbucketCloudBase(
            oauth2=oauth2,
            cloud=True,
            api_root=None,
            api_version="2.0",
            url=(
                "https://api.bitbucket.org/2.0/repositories"
                f"/{workspace_uuid}/{repo_slug}/refs/branches"
            ),
            backoff_and_retry=True,
        )
        for branch in base_cloud._get_paged(
            None,
            trailing=True,
            params={
                "sort": "-target.date",
                "pagelen": MAX_BRANCHES,
                "page": 1,
            },
        ):
            branches.append(branch["name"])
    except (
        HTTPError,
        ConnectionError,
        json.decoder.JSONDecodeError,
    ) as exc:
        LOGGER.error(
            "Error getting bitbucket repo branches data",
            extra={
                "extra": {
                    "exception": exc,
                    "repo_id": repo_slug,
                    "organization_id": organization_id,
                },
            },
        )

    return tuple(branches)[:MAX_BRANCHES]


async def get_bitbucket_authors_stats(
    *,
    credential: Credentials,
    loaders: Dataloaders,
    repos_ids: tuple[str, ...],
) -> tuple[tuple[dict, ...], ...]:
    return await collect(
        tuple(
            _get_bitbucket_authors(credential=credential, loaders=loaders, repo_id=repo_id)
            for repo_id in repos_ids
        ),
        workers=2,
    )


async def _get_bitbucket_authors(
    *,
    credential: Credentials,
    loaders: Dataloaders,
    repo_id: str,
) -> tuple[dict, ...]:
    token = await get_credential_token(
        credential=credential,
        loaders=loaders,
    )
    if not token:
        return tuple()

    return await get_bitbucket_authors(token=token, repo_id=repo_id)


async def _get_bitbucket_repositories_stats(
    *,
    credential: Credentials,
    loaders: Dataloaders,
    urls: set[str],
    nicknames: set[str],
    get_all: bool = False,
) -> tuple[ProjectStats, ...]:
    if isinstance(credential.secret, OauthBitbucketSecret):
        token = await get_credential_token(
            credential=credential,
            loaders=loaders,
        )
        if not token:
            return tuple()

        repositories = await get_bitbucket_repositories(token=token)
        filtered_repositories = tuple(
            repository
            for repository in repositories
            if does_not_exist_in_gitroot_urls(repository=repository, urls=urls, nicknames=nicknames)
        )
        if get_all:
            commits = await get_bitbucket_authors_stats(
                credential=credential,
                repos_ids=tuple(repository.id for repository in filtered_repositories),
                loaders=loaders,
            )
        else:
            branches = await collect(
                tuple(
                    get_bitbucket_branches_names(
                        credential=credential,
                        repo_id=repository.id,
                        loaders=loaders,
                    )
                    for repository in filtered_repositories
                ),
                workers=8,
            )

        return tuple(
            ProjectStats(
                project=project._replace(branches=branches[index] or tuple([project.branch]))
                if not get_all and branches
                else project,
                commits=commits[index] if get_all and commits else tuple(),
                credential=credential,
            )
            for index, project in enumerate(filtered_repositories)
        )

    return tuple()


async def get_bitbucket_outside_repositories_stats(
    *,
    credentials: list[Credentials],
    urls: set[str],
    nicknames: set[str],
    loaders: Dataloaders,
    organization_id: str,
) -> tuple[OrganizationIntegrationRepository, ...]:
    stats = tuple(
        chain.from_iterable(
            await collect(
                tuple(
                    _get_bitbucket_repositories_stats(
                        credential=credential,
                        urls=urls,
                        nicknames=nicknames,
                        loaders=loaders,
                    )
                    for credential in credentials
                ),
                workers=1,
            ),
        ),
    )

    filtered_stats = tuple({stat.project.id: stat for stat in stats}.values())

    return tuple(
        OrganizationIntegrationRepository(
            id=__get_id(stat.project.remote_url),
            organization_id=organization_id,
            branch=stat.project.branch,
            last_commit_date=datetime.fromtimestamp(int(stat.commits[0]["date"]))
            if stat.commits
            else stat.project.last_activity_at,
            url=stat.project.remote_url,
            credential_id=stat.credential.id,
            branches=stat.project.branches,
            name=stat.project.name,
        )
        for stat in filtered_stats
    )
