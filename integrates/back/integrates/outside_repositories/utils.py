import asyncio
import logging
import logging.config
import os
import re
import tempfile
from itertools import chain
from urllib.parse import unquote_plus, urlparse

from aioextensions import collect
from botocore.exceptions import ClientError

from integrates.custom_utils.roots import filter_active_git_roots
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.credentials.types import Credentials
from integrates.db_model.integration_repositories.remove import remove
from integrates.db_model.integration_repositories.types import OrganizationIntegrationRepository
from integrates.db_model.integration_repositories.update import update_unreliable_repositories
from integrates.db_model.organizations.types import (
    Organization,
    OrganizationUnreliableIndicatorsToUpdate,
)
from integrates.db_model.organizations.update import update_unreliable_org_indicators
from integrates.db_model.roots.enums import RootStatus
from integrates.db_model.roots.types import GitRoot, Root
from integrates.dynamodb.exceptions import UnavailabilityError
from integrates.organizations.domain import get_group_names
from integrates.outside_repositories.bitbucket import (
    get_bitbucket_outside_authors_stats,
    get_bitbucket_outside_repositories_stats,
)
from integrates.outside_repositories.domain import (
    get_azure_credentials_authors_stats,
    get_azure_credentials_stats,
    get_github_credentials_authors,
    get_github_credentials_stats,
    get_pat_credentials_authors_stats,
    get_pat_credentials_stats,
)
from integrates.outside_repositories.gitlab import (
    get_gitlab_outside_authors_stats,
    get_gitlab_outside_repositories_stats,
)
from integrates.roots.s3_mirror import download_repo
from integrates.settings import LOGGING

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger(__name__)


async def _get_covered_nickname_authors(git_root: GitRoot, repo_working_dir: str) -> set[str]:
    try:
        os.chdir(repo_working_dir)
        proc = await asyncio.create_subprocess_exec(
            "git",
            "shortlog",
            "-sne",
            git_root.state.branch,
            "--",
            repo_working_dir,
            stderr=asyncio.subprocess.PIPE,
            stdout=asyncio.subprocess.PIPE,
            stdin=asyncio.subprocess.DEVNULL,
        )
        stdout, stderr = await proc.communicate()
    except FileNotFoundError as exc:
        LOGGER.error(
            "Error getting data over repository",
            extra={
                "extra": {
                    "exc": exc,
                    "root_id": git_root.id,
                },
            },
        )

        return set()

    if proc.returncode == 0:
        pattern = r"(?<=\<).+(?=\>)"
        authors = re.findall(pattern, stdout.decode(encoding="latin1", errors="ignore"))

        return set(author.lower() for author in authors)

    LOGGER.error(
        "Error getting data over repository",
        extra={
            "extra": {
                "error": stderr.decode(encoding="latin1", errors="ignore"),
                "folder": os.getcwd(),
                "group": git_root.group_name,
                "repository": repo_working_dir,
                "branch": git_root.state.branch,
            },
        },
    )

    return set()


async def _get_covered_root(git_root: GitRoot) -> set[str]:
    with tempfile.TemporaryDirectory(
        prefix="integrates_outside_", ignore_cleanup_errors=True
    ) as tmpdir:
        repo = await download_repo(
            git_root.group_name,
            git_root.state.nickname,
            tmpdir,
            git_root.state.gitignore,
        )
        if not repo:
            return set()

        return await _get_covered_nickname_authors(git_root, str(repo.working_dir))


async def _get_covered_organization(groups_roots: list[list[Root]]) -> list[set[str]]:
    covered_organization: list[set[str]] = []
    for roots in groups_roots:
        active_git_roots = filter_active_git_roots(roots)
        covered_group = await collect(
            [_get_covered_root(git_root) for git_root in active_git_roots],
            workers=1,
        )
        covered_organization.append(set(set().union(*list(authors for authors in covered_group))))

    return covered_organization


async def update_organization_unreliable(
    *,
    organization: Organization,
    progress: float,
    all_group_names: set[str],
) -> None:
    loaders = get_new_context()
    organization_group_names = await get_group_names(loaders, organization.id)
    organization_group_names = list(
        all_group_names.intersection(set(group.lower() for group in organization_group_names)),
    )
    if not organization_group_names:
        await update_unreliable_org_indicators(
            organization_id=organization.id,
            organization_name=organization.name,
            indicators=OrganizationUnreliableIndicatorsToUpdate(
                covered_authors=0,
                missed_authors=0,
            ),
        )
        LOGGER.info(
            "Updated covered commit stats for organization",
            extra={
                "extra": {
                    "organization_id": organization.id,
                    "organization_name": organization.name,
                    "progress": round(progress, 2),
                    "active_git_roots": 0,
                    "covered_authors": 0,
                },
            },
        )

        return

    groups_roots = await loaders.group_roots.load_many(organization_group_names)
    covered_organization = await _get_covered_organization(groups_roots)
    credentials = await loaders.organization_credentials.load(organization.id)
    urls = {
        unquote_plus(urlparse(root.state.url.lower()).path)
        for root in tuple(chain.from_iterable(groups_roots))
        if isinstance(root, GitRoot)
    }
    nicknames = {
        root.state.nickname.lower()
        for root in tuple(chain.from_iterable(groups_roots))
        if isinstance(root, GitRoot)
    }
    authors_stats = await collect(
        [
            get_pat_credentials_authors_stats(
                credentials=credentials,
                urls=urls,
                nicknames=nicknames,
                loaders=loaders,
            ),
            get_gitlab_outside_authors_stats(
                credentials=credentials,
                urls=urls,
                nicknames=nicknames,
                loaders=loaders,
            ),
            get_github_credentials_authors(
                credentials=credentials,
                urls=urls,
                nicknames=nicknames,
            ),
            get_azure_credentials_authors_stats(
                credentials=credentials,
                urls=urls,
                nicknames=nicknames,
                loaders=loaders,
            ),
            get_bitbucket_outside_authors_stats(
                credentials=credentials,
                urls=urls,
                nicknames=nicknames,
                loaders=loaders,
            ),
        ],
        workers=1,
    )

    await update_unreliable_org_indicators(
        organization_id=organization.id,
        organization_name=organization.name,
        indicators=OrganizationUnreliableIndicatorsToUpdate(
            covered_authors=len(set(set().union(*list(covered_organization)))),
            missed_authors=len(set().union(*list(authors_stats))),
        ),
    )
    LOGGER.info(
        "Updated covered commit stats for organization",
        extra={
            "extra": {
                "organization_id": organization.id,
                "organization_name": organization.name,
                "progress": round(progress, 2),
                "covered_authors": len(set(set().union(*list(covered_organization)))),
                "missed_authors": len(list(set().union(*list(authors_stats)))),
            },
        },
    )


async def _update(
    *,
    organization_id: str,
    organization_name: str,
    repositories: tuple[OrganizationIntegrationRepository, ...],
    covered_repositories: int,
    has_ztna_roots: bool,
) -> None:
    await collect(
        tuple(update_unreliable_repositories(repository=repository) for repository in repositories),
        workers=4,
    )

    await update_unreliable_org_indicators(
        organization_id=organization_id,
        organization_name=organization_name,
        indicators=OrganizationUnreliableIndicatorsToUpdate(
            missed_repositories=len(list({repo.id: repo for repo in repositories}.values())),
            covered_repositories=covered_repositories,
            has_ztna_roots=has_ztna_roots,
        ),
    )


async def _remove(
    *,
    organization_id: str,
    valid_repositories_ids: set[str],
    loaders: Dataloaders,
) -> None:
    current_unreliable_repositories = (
        await loaders.organization_unreliable_outside_repositories.load(
            (organization_id, None, None),
        )
    )

    to_remove = tuple(
        repository
        for repository in current_unreliable_repositories
        if repository.id not in valid_repositories_ids
    )

    await collect(
        tuple(remove(repository=repository) for repository in to_remove),
        workers=4,
    )


async def get_org_repositories(
    *,
    loaders: Dataloaders,
    organization_id: str,
    credentials: list[Credentials],
    urls: set[str],
    nicknames: set[str],
) -> tuple[OrganizationIntegrationRepository, ...]:
    org_integration_repositories = tuple(
        chain.from_iterable(
            await collect(
                [
                    get_pat_credentials_stats(
                        credentials=credentials,
                        urls=urls,
                        nicknames=nicknames,
                        loaders=loaders,
                        organization_id=organization_id,
                    ),
                    get_gitlab_outside_repositories_stats(
                        credentials=credentials,
                        urls=urls,
                        nicknames=nicknames,
                        loaders=loaders,
                        organization_id=organization_id,
                    ),
                    get_github_credentials_stats(
                        credentials=credentials,
                        urls=urls,
                        nicknames=nicknames,
                        organization_id=organization_id,
                    ),
                    get_azure_credentials_stats(
                        credentials=credentials,
                        loaders=loaders,
                        urls=urls,
                        nicknames=nicknames,
                        organization_id=organization_id,
                    ),
                    get_bitbucket_outside_repositories_stats(
                        credentials=credentials,
                        loaders=loaders,
                        urls=urls,
                        nicknames=nicknames,
                        organization_id=organization_id,
                    ),
                ],
                workers=1,
            ),
        ),
    )

    return org_integration_repositories


async def get_credentials_repositories(
    loaders: Dataloaders,
    credentials: Credentials,
) -> tuple[OrganizationIntegrationRepository, ...]:
    groups = await loaders.organization_groups.load(credentials.organization_id)
    urls = {
        unquote_plus(urlparse(root.state.url.lower()).path)
        for root in await loaders.group_roots.load_many_chained([group.name for group in groups])
        if isinstance(root, GitRoot)
    }
    nicknames = {
        root.state.nickname.lower()
        for root in await loaders.group_roots.load_many_chained([group.name for group in groups])
        if isinstance(root, GitRoot)
    }
    organization_id = credentials.organization_id
    org_integration_repos = await get_org_repositories(
        loaders=loaders,
        organization_id=organization_id,
        credentials=[credentials],
        urls=urls,
        nicknames=nicknames,
    )

    cred_repositories = tuple({repo.id: repo for repo in org_integration_repos}.values())

    await collect(
        tuple(
            update_unreliable_repositories(repository=repository)
            for repository in cred_repositories
        ),
        workers=4,
    )

    return org_integration_repos


async def update_organization_repositories(
    *,
    organization: Organization,
    progress: float,
    all_group_names: set[str],
) -> None:
    loaders = get_new_context()
    organization_group_names = await get_group_names(loaders, organization.id)
    organization_group_names = list(
        all_group_names.intersection(set(group.lower() for group in organization_group_names)),
    )
    if not organization_group_names:
        await update_unreliable_org_indicators(
            organization_id=organization.id,
            organization_name=organization.name,
            indicators=OrganizationUnreliableIndicatorsToUpdate(
                missed_repositories=0,
                covered_repositories=0,
                has_ztna_roots=False,
            ),
        )
        LOGGER.info(
            "Organization integration repositories processed",
            extra={
                "extra": {
                    "organization_id": organization.id,
                    "organization_name": organization.name,
                    "progress": round(progress, 2),
                },
            },
        )

        return

    credentials = await loaders.organization_credentials.load(organization.id)
    if not credentials:
        return

    groups_roots = await loaders.group_roots.load_many_chained(organization_group_names)
    urls = {
        unquote_plus(urlparse(root.state.url.lower()).path)
        for root in groups_roots
        if isinstance(root, GitRoot) and root.state.status == RootStatus.ACTIVE
    }
    nicknames = {root.state.nickname.lower() for root in groups_roots if isinstance(root, GitRoot)}

    repositories_stats = await get_org_repositories(
        loaders=loaders,
        organization_id=organization.id,
        credentials=credentials,
        urls=urls,
        nicknames=nicknames,
    )

    try:
        has_ztna_roots = any(
            root.state.use_ztna
            for root in groups_roots
            if isinstance(root, GitRoot) and root.state.status == RootStatus.ACTIVE
        )
        await _update(
            organization_id=organization.id,
            organization_name=organization.name,
            repositories=repositories_stats,
            covered_repositories=len(urls),
            has_ztna_roots=has_ztna_roots,
        )
        await _remove(
            organization_id=organization.id,
            valid_repositories_ids={
                (
                    f"URL#{repository.id}#BRANCH#{repository.branch.lower()}"
                    f"#CRED#{repository.credential_id}"
                )
                for repository in repositories_stats
            },
            loaders=loaders,
        )

        LOGGER.info(
            "Organization integration repositories processed",
            extra={
                "extra": {
                    "organization_id": organization.id,
                    "organization_name": organization.name,
                    "progress": round(progress, 2),
                    "covered_repositories": len(urls),
                    "has_ztna_roots": has_ztna_roots,
                },
            },
        )
    except (ClientError, TypeError, UnavailabilityError) as ex:
        msg: str = "Error: An error occurred updating integration repositories in the database"
        LOGGER.error(
            msg,
            extra={
                "extra": {
                    "organization_id": organization.id,
                    "organization_name": organization.name,
                    "ex": ex,
                },
            },
        )
