from integrates.dataloaders import get_new_context
from integrates.db_model.groups.types import GroupGitLabIssues
from integrates.groups.gitlab_issues_integration import update_settings
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import GroupFaker, GroupGitLabIssuesFaker, OrganizationFaker
from integrates.testing.mocks import mocks

ORG_ID = "ORG#9812t7d3-34g9-2d0o-9ur457j1p3"
GROUP_NAME = "testgroup"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(
                    name=GROUP_NAME, organization_id=ORG_ID, gitlab_issues=GroupGitLabIssuesFaker()
                )
            ],
            organizations=[OrganizationFaker(id=ORG_ID, name="orgtest")],
        ),
    ),
)
async def test_update_settings() -> None:
    # Act
    loaders = get_new_context()
    await update_settings(
        assignee_ids=[1, 2, 3],
        gitlab_project_name="test-gitlab-project",
        group_name=GROUP_NAME,
        issue_automation_enabled=True,
        labels=["label1", "label2"],
        loaders=get_new_context(),
    )
    group_updated = await loaders.group.load(GROUP_NAME)

    # Assert
    assert group_updated
    assert group_updated.gitlab_issues == GroupGitLabIssues(
        redirect_uri="https://testing.com",
        refresh_token="refresh_token",  # noqa: S106
        assignee_ids=[1, 2, 3],
        connection_date=None,
        gitlab_project="test-gitlab-project",
        issue_automation_enabled=True,
        labels=["label1", "label2"],
    )
