from integrates.custom_exceptions import RepeatedValues
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.groups.validations import validate_group_tags
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import GroupFaker, GroupStateFaker, OrganizationFaker
from integrates.testing.mocks import mocks
from integrates.testing.utils import raises

ORG_ID = "ORG#9812t7d3-34g9-2d0o-9ur457j1p3"
GROUP_NAME = "testgroup"
TEST_TAGS = ["tag1", "tag2", "tag3"]


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            organizations=[OrganizationFaker(id=ORG_ID, name="orgtest")],
        )
    )
)
async def test_validate_group_tags() -> None:
    # Act
    loaders: Dataloaders = get_new_context()
    tags = await validate_group_tags(loaders, GROUP_NAME, TEST_TAGS)

    # Assert
    assert tags
    assert tags == TEST_TAGS


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(
                    name=GROUP_NAME,
                    organization_id=ORG_ID,
                    state=GroupStateFaker(tags=set(["tag1", "tag2"])),
                )
            ],
            organizations=[OrganizationFaker(id=ORG_ID, name="orgtest")],
        )
    )
)
async def test_validate_group_tags_fail() -> None:
    # Assert
    with raises(RepeatedValues):
        await validate_group_tags(get_new_context(), GROUP_NAME, TEST_TAGS)
