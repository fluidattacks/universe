from integrates.dataloaders import get_new_context
from integrates.db_model.azure_repositories.types import AccountInfo
from integrates.db_model.groups.types import GroupAzureIssues
from integrates.groups.azure_issues_integration import (
    get_organization_names,
    update_settings,
    update_token,
)
from integrates.oauth import azure
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    DATE_2024,
    GroupAzureIssuesFaker,
    GroupFaker,
    OrganizationFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import freeze_time

ORG_ID = "ORG#9812t7d3-34g9-2d0o-9ur457j1p3"
GROUP_NAME = "testgroup"
TEST_TKN = "new-refresh-token"
TEST_URI = "https://testing.com"
USER_EMAIL = "integratesuser@fluidattacks.com"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            organizations=[OrganizationFaker(id=ORG_ID, name="orgtest")],
        ),
    ),
    others=[
        Mock(azure, "get_fresh_token", "async", {"access_token": "123"}),
        Mock(azure, "get_azure_profile", "async", "test_profile"),
        Mock(
            azure,
            "get_azure_accounts",
            "async",
            tuple([AccountInfo(base_url="https://test.azure.com", name="testaccount")]),
        ),
    ],
)
async def test_get_organization_names() -> None:
    # Act
    org_names = await get_organization_names(GroupAzureIssuesFaker())

    # Assert
    assert org_names
    assert org_names == ["testaccount"]


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(
                    name=GROUP_NAME, organization_id=ORG_ID, azure_issues=GroupAzureIssuesFaker()
                )
            ],
            organizations=[OrganizationFaker(id=ORG_ID, name="orgtest")],
        ),
    ),
)
async def test_update_settings() -> None:
    # Act
    loaders = get_new_context()
    await update_settings(
        assigned_to=USER_EMAIL,
        azure_organization_name="test-azure-org",
        azure_project_name="test-project",
        group_name=GROUP_NAME,
        issue_automation_enabled=True,
        loaders=get_new_context(),
        tags=["tag1", "tag2"],
    )
    group_updated = await loaders.group.load(GROUP_NAME)

    # Assert
    assert group_updated
    assert group_updated.azure_issues == GroupAzureIssues(
        redirect_uri=TEST_URI,
        refresh_token="refresh_token",  # noqa: S106
        assigned_to=USER_EMAIL,
        azure_organization="test-azure-org",
        azure_project="test-project",
        connection_date=None,
        issue_automation_enabled=True,
        tags=["tag1", "tag2"],
    )


@freeze_time("2024-04-10T12:59:52+00:00")
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(
                    name=GROUP_NAME, organization_id=ORG_ID, azure_issues=GroupAzureIssuesFaker()
                )
            ],
            organizations=[OrganizationFaker(id=ORG_ID, name="orgtest")],
        ),
    ),
)
async def test_update_token() -> None:
    # Act
    loaders = get_new_context()
    await update_token(
        group_name=GROUP_NAME,
        loaders=get_new_context(),
        redirect_uri=TEST_URI,
        refresh_token=TEST_TKN,
    )
    group_updated = await loaders.group.load(GROUP_NAME)

    # Assert
    assert group_updated
    assert group_updated.azure_issues == GroupAzureIssues(
        redirect_uri=TEST_URI,
        refresh_token=TEST_TKN,
        assigned_to=None,
        azure_organization=None,
        azure_project=None,
        connection_date=DATE_2024,
        issue_automation_enabled=True,
        tags=None,
    )
