import hashlib
import logging
import logging.config
import re
import uuid
from collections import Counter
from contextlib import suppress
from datetime import date, datetime
from decimal import Decimal
from typing import (
    Any,
)

from aioextensions import (
    collect,
    in_thread,
    schedule,
)
from botocore.exceptions import (
    ClientError,
)
from urllib3.exceptions import (
    LocationParseError,
)
from urllib3.util.url import (
    parse_url,
)

from integrates import authz
from integrates.authz.model import get_group_level_roles_model, get_user_level_roles_model
from integrates.authz.policy import get_user_level_role, grant_user_level_role
from integrates.authz.validations import (
    validate_fluidattacks_staff_on_group_deco,
    validate_role_fluid_reqs,
    validate_role_fluid_reqs_deco,
)
from integrates.batch import (
    dal as batch_dal,
)
from integrates.batch.dal.put import (
    put_action,
)
from integrates.batch.enums import (
    Action,
    IntegratesBatchQueue,
)
from integrates.batch.types import (
    BatchProcessing,
)
from integrates.context import (
    BASE_URL,
    FI_ENVIRONMENT,
)
from integrates.custom_exceptions import (
    BillingSubscriptionSameActive,
    CredentialInUse,
    DocumentNotFound,
    ErrorUpdatingGroup,
    GroupHasPendingActions,
    GroupNotFound,
    HookNotFound,
    InvalidGroupName,
    InvalidGroupServicesConfig,
    InvalidGroupTier,
    InvalidManagedChange,
    NumberOutOfRange,
    RepeatedRootEnvironmentFile,
    RootEnvironmentUrlNotFound,
    StakeholderNotInGroup,
    StakeholderNotInOrganization,
    TrialRestriction,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.custom_utils import (
    findings as findings_utils,
)
from integrates.custom_utils import (
    groups as groups_utils,
)
from integrates.custom_utils import (
    validations,
)
from integrates.custom_utils import (
    vulnerabilities as vulns_utils,
)
from integrates.custom_utils.access import get_stakeholder_groups_names
from integrates.custom_utils.filter_vulnerabilities import (
    filter_non_deleted,
)
from integrates.custom_utils.findings import (
    get_group_findings,
)
from integrates.custom_utils.groups import get_group
from integrates.custom_utils.machine import find_root_nicknames_for_apk_file
from integrates.custom_utils.roots import (
    get_active_git_root,
)
from integrates.custom_utils.utils import (
    validate_filename,
    validate_personal_email_invitation_deco,
)
from integrates.custom_utils.validations import (
    assert_is_isalnum,
    check_range,
    is_fluid_staff,
)
from integrates.custom_utils.validations_deco import (
    validate_alphanumeric_field_deco,
    validate_email_address_deco,
    validate_fields_deco,
    validate_file_exists_deco,
    validate_group_language_deco,
    validate_length_deco,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    forces as forces_model,
)
from integrates.db_model import (
    groups as groups_model,
)
from integrates.db_model import (
    toe_inputs as toe_inputs_model,
)
from integrates.db_model import (
    toe_lines as toe_lines_model,
)
from integrates.db_model import (
    toe_ports as toe_ports_model,
)
from integrates.db_model.constants import (
    POLICIES_FORMATTED,
)
from integrates.db_model.enums import (
    TreatmentStatus,
)
from integrates.db_model.events.types import (
    GroupEventsRequest,
)
from integrates.db_model.group_access.types import (
    GroupAccess,
    GroupAccessMetadataToUpdate,
    GroupAccessRequest,
    GroupAccessState,
    GroupInvitation,
    GroupStakeholdersAccessRequest,
)
from integrates.db_model.groups.constants import (
    MASKED,
)
from integrates.db_model.groups.enums import (
    GroupLanguage,
    GroupManaged,
    GroupService,
    GroupStateJustification,
    GroupStateStatus,
    GroupSubscriptionType,
    GroupTier,
)
from integrates.db_model.groups.types import (
    Group,
    GroupFile,
    GroupMetadataToUpdate,
    GroupState,
    GroupTreatmentSummary,
    GroupUnreliableIndicators,
    NoUpdate,
)
from integrates.db_model.hook import (
    add_hook,
    remove,
    update,
)
from integrates.db_model.hook.types import (
    GroupHookPayload,
    GroupHookRequest,
)
from integrates.db_model.integration_repositories.remove import (
    remove as remove_integration_repository,
)
from integrates.db_model.organization_access.types import (
    OrganizationAccessRequest,
)
from integrates.db_model.organizations.types import (
    Organization,
)
from integrates.db_model.portfolios.remove import (
    remove as remove_portfolio,
)
from integrates.db_model.portfolios.types import (
    Portfolio,
)
from integrates.db_model.portfolios.update import (
    update as update_portfolio,
)
from integrates.db_model.roots.enums import (
    RootEnvironmentUrlStateStatus,
    RootEnvironmentUrlType,
    RootStateReason,
)
from integrates.db_model.roots.types import (
    GroupEnvironmentUrlsRequest,
    Root,
    RootEnvironmentUrlsRequest,
)
from integrates.db_model.stakeholders.types import (
    StakeholderMetadataToUpdate,
)
from integrates.db_model.types import (
    PoliciesToUpdate,
)
from integrates.db_model.utils import (
    get_min_iso_date,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
    VulnerabilityVerificationStatus,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.dynamodb.exceptions import (
    UnavailabilityError,
)
from integrates.events import (
    domain as events_domain,
)
from integrates.findings import (
    domain as findings_domain,
)
from integrates.group_access import (
    domain as group_access_domain,
)
from integrates.groups import (
    validations as groups_validations,
)
from integrates.jobs_orchestration.create_machine_config import (
    MachineJobOrigin,
    MachineJobTechnique,
    MachineModulesToExecute,
)
from integrates.jobs_orchestration.jobs import queue_machine_job
from integrates.mailer import (
    groups as groups_mail,
)
from integrates.mailer import (
    utils as mailer_utils,
)
from integrates.mailer.types import AccessGrantedContext, UpdatedPoliciesContext
from integrates.organization_access import (
    domain as org_access,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.organizations import (
    utils as orgs_utils,
)
from integrates.resources import (
    domain as resources_domain,
)
from integrates.roots import (
    domain as roots_domain,
)
from integrates.roots import (
    environments as roots_environments,
)
from integrates.sessions import (
    domain as sessions_domain,
)
from integrates.settings import (
    LOGGING,
)
from integrates.stakeholders import (
    domain as stakeholders_domain,
)
from integrates.tickets import (
    domain as tickets_domain,
)
from integrates.trials import (
    domain as trials_domain,
)
from integrates.verify.enums import (
    ResourceType,
)
from integrates.vulnerabilities import (
    domain as vulns_domain,
)

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger(__name__)
TRANSACTIONS_LOGGER: logging.Logger = logging.getLogger("transactional")


async def complete_register_for_group_invitation(
    loaders: Dataloaders,
    group_access: GroupAccess,
    modified_by: str,
) -> None:
    invitation = group_access.state.invitation
    if invitation and invitation.is_used:
        TRANSACTIONS_LOGGER.info(
            "Token already used",
            extra={
                "group_access": group_access,
                "task_name": "complete_group_invitation",
            },
        )
        return

    group_name = group_access.group_name
    email = group_access.email
    TRANSACTIONS_LOGGER.info(
        "User attempted to grant_group_level_role",
        extra={
            "group_access": group_access,
            "user_email": email,
        },
    )
    if invitation:
        responsibility = invitation.responsibility
        role = invitation.role
        updated_invitation = invitation._replace(is_used=True)
    if role not in get_group_level_roles_model():
        raise ValueError(f"Invalid role value: {role}")
    validate_role_fluid_reqs(email, role)

    group = await loaders.group.load(group_name)
    if not group or group.state.status == GroupStateStatus.DELETED:
        raise GroupNotFound()
    loaders.group_access.clear(GroupAccessRequest(email=email, group_name=group_name))
    _group_access = await loaders.group_access.load(
        GroupAccessRequest(email=email, group_name=group_name)
    )
    if _group_access is None:
        return

    await group_access_domain.update(
        loaders=loaders,
        email=email,
        group_name=group_name,
        metadata=GroupAccessMetadataToUpdate(
            expiration_time=0,
            state=group_access.state._replace(
                modified_by=modified_by,
                modified_date=datetime_utils.get_utc_now(),
                has_access=True,
                invitation=updated_invitation,
                responsibility=responsibility,
                role=role,
            ),
        ),
    )
    TRANSACTIONS_LOGGER.info(
        "User complete grant_group_level_role with access",
        extra={"user_email": email, "group_access": group_access},
    )

    if not await get_user_level_role(loaders, email):
        await grant_user_level_role(email, role if role in get_user_level_roles_model() else "user")

    loaders.stakeholder.clear(email)
    group = await get_group(loaders, group_name)
    organization_id = group.organization_id
    if not await loaders.organization_access.load(
        OrganizationAccessRequest(organization_id=organization_id, email=email),
    ):
        loaders.organization_access.clear(
            OrganizationAccessRequest(organization_id=organization_id, email=email),
        )
        await orgs_domain.add_stakeholder(
            loaders=loaders,
            organization_id=organization_id,
            email=email,
            role="user",
            modified_by=modified_by,
        )

    loaders.stakeholder.clear(email)
    stakeholder = await loaders.stakeholder.load(email)
    if stakeholder and (not stakeholder.is_registered or not stakeholder.enrolled):
        await stakeholders_domain.update(
            email=email,
            metadata=StakeholderMetadataToUpdate(
                enrolled=True,
                is_registered=True,
            ),
        )

    await orgs_domain.complete_trial_after_confirm_invitation(
        loaders=loaders,
        email=email,
        modified_by=group_access.state.modified_by or email,
        organization_id=organization_id,
    )
    loaders.organization_access.clear(
        OrganizationAccessRequest(organization_id=organization_id, email=email),
    )
    loaders.stakeholder.clear(email)
    loaders.group_access.clear(GroupAccessRequest(email=email, group_name=group_name))


async def reject_register_for_group_invitation(
    loaders: Dataloaders,
    group_access: GroupAccess,
) -> None:
    invitation = group_access.state.invitation

    if invitation and invitation.is_used:
        TRANSACTIONS_LOGGER.info(
            "Token already used",
            extra={
                "group_access": group_access,
                "task_name": "reject_group_invitation",
            },
        )

    await group_access_domain.remove_access(
        loaders=loaders,
        email=group_access.email,
        group_name=group_access.group_name,
    )


async def add_forces_stakeholder(
    loaders: Dataloaders,
    group_name: str,
    modified_by: str,
) -> None:
    forces_email = f"forces.{group_name}@fluidattacks.com"
    await invite_to_group(
        loaders=loaders,
        email=forces_email,
        responsibility="Forces service user",
        role="service_forces",
        group_name=group_name,
        modified_by=modified_by,
    )

    # Give permissions directly, no confirmation required
    loaders.group_access.clear(GroupAccessRequest(group_name=group_name, email=forces_email))
    group_access = await group_access_domain.get_group_access(loaders, group_name, forces_email)
    await complete_register_for_group_invitation(loaders, group_access, modified_by)


async def _determine_group_managed(
    *,
    loaders: Dataloaders,
    email: str,
    organization: Organization,
    has_advanced: bool,
    has_essential: bool,
    service: GroupService,
    subscription: GroupSubscriptionType,
) -> GroupManaged:
    user_role = await loaders.organization_access.load(
        OrganizationAccessRequest(organization_id=organization.id, email=email),
    )

    if user_role and user_role.state.role == "customer_manager":
        return GroupManaged.MANAGED

    if is_fluid_staff(organization.created_by):
        return GroupManaged.MANAGED

    org_groups = await loaders.organization_groups.load(organization.id)
    if any(group.state.managed == GroupManaged.TRIAL for group in org_groups):
        raise TrialRestriction()

    if await trials_domain.in_trial(loaders, email, organization):
        managed = GroupManaged.TRIAL
        restrictions_met = (
            org_groups
            or has_advanced
            or not has_essential
            or service != GroupService.WHITE
            or subscription != GroupSubscriptionType.CONTINUOUS
        )
        if restrictions_met:
            raise TrialRestriction()
    elif not organization.payment_methods and not organization.billing_information:
        return GroupManaged.UNDER_REVIEW
    else:
        managed = GroupManaged.MANAGED

    return managed


async def add_group_to_db(
    *,
    description: str,
    email: str,
    language: GroupLanguage,
    group_name: str,
    has_essential: bool,
    has_advanced: bool,
    managed: GroupManaged,
    organization_id: str,
    tier: GroupTier,
    service: GroupService,
    subscription: GroupSubscriptionType,
) -> None:
    await groups_model.add(
        group=Group(
            created_by=email,
            created_date=datetime_utils.get_utc_now(),
            description=description,
            language=language,
            name=group_name,
            state=GroupState(
                has_essential=has_essential,
                has_advanced=has_advanced,
                managed=managed,
                modified_by=email,
                modified_date=datetime_utils.get_utc_now(),
                service=service,
                status=GroupStateStatus.ACTIVE,
                tier=tier,
                type=subscription,
            ),
            organization_id=organization_id,
            sprint_duration=1,
            sprint_start_date=get_min_iso_date(datetime_utils.get_utc_now()),
        ),
    )


async def update_group_access(
    loaders: Dataloaders,
    email: str,
    group_name: str,
) -> None:
    # re
    try:
        group_access = await group_access_domain.get_group_access(
            loaders=loaders,
            group_name=group_name,
            email=email,
        )
        state = group_access.state._replace(
            modified_by=email,
            modified_date=datetime_utils.get_utc_now(),
            has_access=True,
        )
    except StakeholderNotInGroup:
        state = GroupAccessState(
            modified_by=email,
            modified_date=datetime_utils.get_utc_now(),
            has_access=True,
        )

    await group_access_domain.update(
        loaders=loaders,
        email=email,
        group_name=group_name,
        metadata=GroupAccessMetadataToUpdate(
            state=state,
        ),
    )


@assert_is_isalnum("group_name")
@validate_fields_deco(["description"])
@validate_length_deco("group_name", min_length=4, max_length=20)
@validate_length_deco("description", min_length=10, max_length=200)
@groups_validations.validate_group_services_config_deco(
    "has_essential",
    "has_advanced",
    has_arm_field=True,
)
async def add_group(
    *,
    loaders: Dataloaders,
    description: str,
    email: str,
    group_name: str,
    organization_name: str,
    service: GroupService,
    has_essential: bool = False,
    has_advanced: bool = False,
    language: GroupLanguage = GroupLanguage.EN,
    subscription: GroupSubscriptionType = GroupSubscriptionType.CONTINUOUS,
    tier: GroupTier = GroupTier.FREE,
) -> None:
    if subscription is GroupSubscriptionType.ONESHOT:
        raise InvalidGroupServicesConfig("OneShot service is no longer provided")

    if await exists(loaders, group_name):
        raise InvalidGroupName.new()

    organization = await orgs_utils.get_organization(loaders, organization_name)
    if not await org_access.has_access(loaders, organization.id, email):
        raise StakeholderNotInOrganization(organization.id)

    managed = await _determine_group_managed(
        loaders=loaders,
        email=email,
        organization=organization,
        has_advanced=has_advanced,
        has_essential=has_essential,
        service=service,
        subscription=subscription,
    )
    if description == "Trial group" and managed != GroupManaged.TRIAL:
        description = "Initial group"

    await add_group_to_db(
        description=description,
        email=email,
        language=language,
        group_name=group_name,
        has_essential=has_essential,
        has_advanced=has_advanced,
        managed=managed,
        organization_id=organization.id,
        tier=tier,
        service=service,
        subscription=subscription,
    )

    await _add_managers_access(loaders, organization.id, group_name, email)
    loaders.group.clear(group_name)
    await add_forces_stakeholder(loaders, group_name, email)

    await tickets_domain.new_group(
        loaders=loaders,
        description=description,
        group_name=group_name,
        has_essential=has_essential,
        has_advanced=has_advanced,
        organization=organization_name,
        requester_email=email,
        service=service,
        subscription=subscription,
    )


async def _add_managers_access(
    loaders: Dataloaders,
    organization_id: str,
    group_name: str,
    creator_email: str,
) -> None:
    stakeholders = await orgs_domain.get_stakeholders_emails(loaders, organization_id)

    if (
        creator_email not in stakeholders
        and await authz.get_user_level_role(loaders, creator_email) != "admin"
    ):
        raise StakeholderNotInOrganization(organization_id)

    stakeholders_roles = await collect(
        authz.get_organization_level_role(loaders, email, organization_id) for email in stakeholders
    )
    await collect(
        group_access_domain.add_access(
            loaders,
            stakeholder,
            group_name,
            "group_manager" if stakeholder_role == "organization_manager" else "customer_manager",
            creator_email,
        )
        for stakeholder, stakeholder_role in zip(stakeholders, stakeholders_roles, strict=False)
        if stakeholder_role in ("customer_manager", "organization_manager")
    )


async def _deactivate_group_root(
    *,
    loaders: Dataloaders,
    email: str,
    group_name: str,
    other: str,
    root: Root,
) -> None:
    root_vulns = await loaders.root_vulnerabilities.load(root.id)
    root_vulns_non_deleted = filter_non_deleted(root_vulns)
    await vulns_domain.deactivate_vulnerabilities(
        email=email,
        loaders=loaders,
        vulns_nzr=root_vulns_non_deleted,
    )
    await vulns_domain.close_released_vulnerabilities(
        email=email,
        loaders=loaders,
        vulns_nzr=root_vulns_non_deleted,
    )
    await roots_domain.deactivate_root(
        loaders=loaders,
        group_name=group_name,
        other=other,
        reason=RootStateReason.GROUP_CONFIG_CHANGED,
        root=root,
        email=email,
    )


async def _deactivate_all_roots(
    *,
    loaders: Dataloaders,
    email: str,
    group_name: str,
    other: str,
) -> None:
    all_group_roots = await loaders.group_roots.load(group_name)
    await collect(
        [
            _deactivate_group_root(
                loaders=loaders,
                group_name=group_name,
                other=other,
                root=root,
                email=email,
            )
            for root in all_group_roots
        ],
    )


async def _remove_group_from_portfolios(
    *,
    loaders: Dataloaders,
    group_name: str,
    organization_name: str,
) -> None:
    org_portfolios = await loaders.organization_portfolios.load(organization_name)
    portfolios_to_update: list[Portfolio] = []
    portfolios_to_delete: list[Portfolio] = []
    for portfolio in org_portfolios:
        if group_name in portfolio.groups:
            if len(portfolio.groups) == 1:
                portfolios_to_delete.append(portfolio)
            else:
                portfolios_to_update.append(
                    Portfolio(
                        id=portfolio.id,
                        groups={x for x in portfolio.groups if x != group_name},
                        organization_name=portfolio.organization_name,
                        unreliable_indicators=(portfolio.unreliable_indicators),
                    ),
                )
    await collect(
        (update_portfolio(portfolio=portfolio) for portfolio in portfolios_to_update),
        workers=8,
    )
    await collect(
        (
            remove_portfolio(
                organization_name=portfolio.organization_name,
                portfolio_id=portfolio.id,
            )
            for portfolio in portfolios_to_delete
        ),
        workers=8,
    )


@validate_fields_deco(["comments"])
@validate_length_deco("comments", max_length=250)
async def remove_group(
    *,
    loaders: Dataloaders,
    comments: str,
    email: str,
    group_name: str,
    justification: GroupStateJustification,
    unique_authors: int = 0,
    validate_pending_actions: bool = True,
) -> None:
    """
    Update group state to DELETED and update some related resources.
    remove_group_resources.
    """
    loaders.group.clear(group_name)
    group = await get_group(loaders, group_name)
    await groups_model.update_state(
        group_name=group_name,
        organization_id=group.organization_id,
        state=group.state._replace(
            comments=comments,
            modified_date=datetime_utils.get_utc_now(),
            has_essential=False,
            has_advanced=False,
            justification=justification,
            modified_by=email,
            status=GroupStateStatus.DELETED,
        ),
    )

    group_org = await loaders.organization.load(group.organization_id)
    if group_org:
        await _remove_group_from_portfolios(
            loaders=loaders,
            group_name=group_name,
            organization_name=group_org.name,
        )
    emails = await mailer_utils.get_group_emails_by_notification(
        loaders=loaders,
        group_name=group.name,
        notification="group_alert",
    )

    await batch_dal.put_action(
        action=Action.REMOVE_GROUP_RESOURCES,
        entity=group_name,
        subject=email,
        additional_info={
            "validate_pending_actions": validate_pending_actions,
            "subscription": group.state.type.value,
            "has_advanced": group.state.has_advanced,
            "emails": emails,
        },
        queue=IntegratesBatchQueue.SMALL,
        attempt_duration_seconds=86400,
    )

    await tickets_domain.delete_group(
        loaders=loaders,
        deletion_date=datetime_utils.get_utc_now(),
        group=group,
        requester_email=email,
        reason=justification.value,
        comments=comments,
        attempt=True,
        unique_authors=unique_authors,
    )

    if FI_ENVIRONMENT != "production":
        await remove_resources(
            loaders=loaders,
            email=email,
            group_name=group_name,
            subscription=group.state.type,
            has_advanced=group.state.has_advanced,
            unique_authors=unique_authors,
            emails=emails,
        )

    await in_thread(
        TRANSACTIONS_LOGGER.info,
        "Remove Group Resources action has been added",
        extra={
            "group_name": group_name,
            "justification": justification,
        },
    )


@validate_fields_deco(["comments"])
@validate_length_deco("comments", max_length=250)
async def update_group_managed(
    *,
    loaders: Dataloaders,
    comments: str,
    email: str,
    group_name: str,
    managed: GroupManaged,
    justification: GroupStateJustification = GroupStateJustification.NONE,
) -> None:
    group = await get_group(loaders, group_name)

    if managed != group.state.managed:
        if (
            managed == GroupManaged.MANAGED and group.state.managed == GroupManaged.UNDER_REVIEW
        ) or (
            managed == GroupManaged.UNDER_REVIEW
            and group.state.managed in {GroupManaged.MANAGED, GroupManaged.TRIAL}
        ):
            await update_state(
                group_name=group_name,
                organization_id=group.organization_id,
                state=GroupState(
                    comments=comments,
                    modified_date=datetime_utils.get_utc_now(),
                    has_essential=group.state.has_essential,
                    has_advanced=group.state.has_advanced,
                    managed=managed,
                    payment_id=group.state.payment_id,
                    justification=justification,
                    modified_by=email,
                    service=group.state.service,
                    status=GroupStateStatus.ACTIVE,
                    tags=group.state.tags,
                    tier=group.state.tier,
                    type=group.state.type,
                ),
            )
        else:
            raise InvalidManagedChange()

        if managed == GroupManaged.MANAGED:
            organization = await orgs_utils.get_organization(loaders, group.organization_id)
            await tickets_domain.request_managed(
                group_name=group_name,
                managed=managed,
                organization_name=organization.name,
                requester_email=email,
            )


@validate_fields_deco(["comments"])
@validate_length_deco("comments", max_length=250)
@groups_validations.validate_group_services_config_deco("has_essential", "has_advanced", "has_arm")
async def update_group(
    *,
    loaders: Dataloaders,
    comments: str,
    email: str,
    group_name: str,
    has_arm: bool,
    has_essential: bool,
    has_advanced: bool,
    justification: GroupStateJustification,
    service: GroupService | None,
    subscription: GroupSubscriptionType,
    tier: GroupTier = GroupTier.OTHER,
) -> None:
    group = await get_group(loaders, group_name)
    organization = await orgs_utils.get_organization(loaders, group.organization_id)

    if (
        group.state.type != GroupSubscriptionType.ONESHOT
        and subscription == GroupSubscriptionType.ONESHOT
    ):
        raise InvalidGroupServicesConfig("OneShot service is no longer provided")

    restricted_in_trial = (
        has_advanced
        or not has_essential
        or service != GroupService.WHITE
        or subscription != GroupSubscriptionType.CONTINUOUS
    )
    if (
        has_arm
        and await trials_domain.in_trial(loaders, email, organization)
        and restricted_in_trial
    ):
        raise TrialRestriction()

    if service != group.state.service:
        await _deactivate_all_roots(
            loaders=loaders,
            email=email,
            group_name=group_name,
            other=comments,
        )
    if tier == GroupTier.OTHER:
        tier = GroupTier.FREE

    if has_arm:
        await update_state(
            group_name=group_name,
            organization_id=group.organization_id,
            state=GroupState(
                comments=comments,
                modified_date=datetime_utils.get_utc_now(),
                has_essential=has_essential,
                has_advanced=has_advanced,
                managed=group.state.managed,
                justification=justification,
                modified_by=email,
                service=service,
                status=GroupStateStatus.ACTIVE,
                tags=group.state.tags,
                tier=tier,
                type=subscription,
            ),
        )
        await tickets_domain.update_group(
            loaders=loaders,
            comments=comments,
            group_name=group_name,
            group_state=group.state,
            had_arm=True,
            has_arm=has_arm,
            has_essential=has_essential,
            has_advanced=has_advanced,
            reason=justification.value,
            requester_email=email,
            service=service.value if service else "",
            subscription=str(subscription.value).lower(),
        )


async def update_group_tier(
    *,
    loaders: Dataloaders,
    comments: str,
    email: str,
    group_name: str,
    tier: GroupTier,
) -> None:
    """Set a new tier for a group."""
    if tier == GroupTier.ESSENTIAL:
        subscription = GroupSubscriptionType.CONTINUOUS
        has_essential = True
        has_advanced = False
        service = GroupService.WHITE
    elif tier == GroupTier.ADVANCED:
        subscription = GroupSubscriptionType.CONTINUOUS
        has_essential = True
        has_advanced = True
        service = GroupService.WHITE
    elif tier == GroupTier.ONESHOT:
        subscription = GroupSubscriptionType.ONESHOT
        has_essential = False
        has_advanced = False
        service = GroupService.BLACK
    elif tier == GroupTier.FREE:
        subscription = GroupSubscriptionType.CONTINUOUS
        has_essential = False
        has_advanced = False
        service = GroupService.WHITE
    else:
        raise InvalidGroupTier()

    await update_group(
        loaders=loaders,
        comments=comments,
        email=email,
        group_name=group_name,
        justification=GroupStateJustification.OTHER,
        has_arm=True,
        has_essential=has_essential,
        has_advanced=has_advanced,
        service=service,
        subscription=subscription,
        tier=tier,
    )


async def get_closed_vulnerabilities(loaders: Dataloaders, group_name: str) -> int:
    group_findings = await get_group_findings(group_name=group_name, loaders=loaders)
    findings_vulns = await loaders.finding_vulnerabilities_released_nzr.load_many_chained(
        [finding.id for finding in group_findings],
    )
    last_approved_status = [vuln.state.status for vuln in findings_vulns]

    return last_approved_status.count(VulnerabilityStateStatus.SAFE)


async def get_total_priority_findings(loaders: Dataloaders, group_name: str) -> Decimal:
    findings = await get_group_findings(group_name=group_name, loaders=loaders)
    return Decimal(
        sum(
            finding.unreliable_indicators.total_open_priority
            if finding.unreliable_indicators.total_open_priority
            and finding.unreliable_indicators.total_open_priority > Decimal(0)
            else Decimal(0)
            for finding in findings
        ),
    )


async def get_groups_by_stakeholder(
    loaders: Dataloaders,
    email: str,
    active: bool = True,
    organization_id: str = "",
) -> list[str]:
    group_names = await get_stakeholder_groups_names(loaders, email, active)
    if organization_id:
        org_groups = await loaders.organization_groups.load(organization_id)
        org_group_names: set[str] = set(group.name for group in org_groups)
        group_names = [group_name for group_name in group_names if group_name in org_group_names]

    group_level_roles = await authz.get_group_level_roles(loaders, email, group_names)

    return [
        group_name
        for role, group_name in zip(group_level_roles.values(), group_names, strict=False)
        if bool(role)
    ]


async def get_vulnerabilities_with_pending_attacks(
    *,
    loaders: Dataloaders,
    group_name: str,
) -> int:
    findings = await get_group_findings(group_name=group_name, loaders=loaders)
    vulnerabilities = await loaders.finding_vulnerabilities_released_nzr.load_many_chained(
        [finding.id for finding in findings],
    )

    return len(
        tuple(
            vulnerability
            for vulnerability in vulnerabilities
            if vulnerability.verification
            and vulnerability.verification.status == VulnerabilityVerificationStatus.REQUESTED
        ),
    )


async def get_max_severity(
    loaders: Dataloaders,
    group_name: str,
) -> Decimal:
    findings = await get_group_findings(group_name=group_name, loaders=loaders)
    findings_released_severity = await collect(
        findings_domain.get_max_released_severity_score(loaders, finding.id) for finding in findings
    )

    return max(
        findings_released_severity,
        default=Decimal("0.0"),
    )


async def get_group_vulns_with_severity(
    loaders: Dataloaders,
    group_name: str,
) -> list[Vulnerability]:
    """
    Retrieve all group released vulns with the severity score populated,
    either from itself or the parent finding.
    """
    group_findings = await get_group_findings(group_name=group_name, loaders=loaders)
    released_vulns = await loaders.finding_vulnerabilities_released_nzr.load_many_chained(
        [finding.id for finding in group_findings],
    )

    return [
        vuln._replace(
            severity_score=vulns_utils.get_severity_score(
                vuln,
                next(finding for finding in group_findings if finding.id == vuln.finding_id),
            ),
        )
        for vuln in released_vulns
    ]


async def get_mean_remediate_severity_cvssf(
    loaders: Dataloaders,
    group_name: str,
    min_severity: Decimal,
    max_severity: Decimal,
    min_date: date | None = None,
) -> Decimal:
    group_vulns = await get_group_vulns_with_severity(loaders, group_name)
    filtered_vulns_by_severity = [
        vuln
        for vuln in group_vulns
        if vuln.severity_score and min_severity <= vuln.severity_score.threat_score <= max_severity
    ]
    vulns_cvssf = [
        vuln.severity_score.cvssf_v4 if vuln.severity_score else Decimal("0.0")
        for vuln in filtered_vulns_by_severity
    ]

    return vulns_utils.get_mean_remediate_vulnerabilities_cvssf(
        filtered_vulns_by_severity,
        vulns_cvssf,
        min_date,
    )


async def get_mean_remediate_non_treated_severity_cvssf(
    loaders: Dataloaders,
    group_name: str,
    min_severity: Decimal,
    max_severity: Decimal,
    min_date: date | None = None,
) -> Decimal:
    group_vulns = await get_group_vulns_with_severity(loaders, group_name)
    filtered_vulns_by_severity = [
        vuln
        for vuln in group_vulns
        if vuln.severity_score and min_severity <= vuln.severity_score.threat_score <= max_severity
    ]
    non_accepted_undefined_vulns = [
        vuln
        for vuln in filtered_vulns_by_severity
        if not vulns_utils.is_accepted_undefined_vulnerability(vuln)
    ]
    vulns_cvssf = [
        vuln.severity_score.cvssf_v4 if vuln.severity_score else Decimal("0.0")
        for vuln in non_accepted_undefined_vulns
    ]

    return vulns_utils.get_mean_remediate_vulnerabilities_cvssf(
        non_accepted_undefined_vulns,
        vulns_cvssf,
        min_date,
    )


async def get_mean_remediate_severity(
    loaders: Dataloaders,
    group_name: str,
    min_severity: Decimal,
    max_severity: Decimal,
    min_date: date | None = None,
) -> Decimal:
    """Get mean time to remediate, in days. Also filtered by severity."""
    group_vulns = await get_group_vulns_with_severity(loaders, group_name)
    filtered_vulns_by_severity = [
        vuln
        for vuln in group_vulns
        if vuln.severity_score and min_severity <= vuln.severity_score.threat_score <= max_severity
    ]

    return vulns_utils.get_mean_remediate_vulnerabilities(
        filtered_vulns_by_severity,
        min_date,
    )


async def get_mean_remediate_non_treated_severity(
    loaders: Dataloaders,
    group_name: str,
    min_severity: Decimal,
    max_severity: Decimal,
    min_date: date | None = None,
) -> Decimal:
    group_vulns = await get_group_vulns_with_severity(loaders, group_name)
    filtered_vulns_by_severity = [
        vuln
        for vuln in group_vulns
        if vuln.severity_score and min_severity <= vuln.severity_score.threat_score <= max_severity
    ]
    non_accepted_undefined_vulns = [
        vuln
        for vuln in filtered_vulns_by_severity
        if not vulns_utils.is_accepted_undefined_vulnerability(vuln)
    ]

    return vulns_utils.get_mean_remediate_vulnerabilities(
        non_accepted_undefined_vulns,
        min_date,
    )


async def get_open_findings(loaders: Dataloaders, group_name: str) -> int:
    group_findings = await get_group_findings(group_name=group_name, loaders=loaders)
    finding_status = await collect(
        tuple(findings_domain.get_status(loaders, finding.id) for finding in group_findings),
        workers=32,
    )
    return finding_status.count("VULNERABLE")


async def get_open_vulnerabilities(
    loaders: Dataloaders,
    group_name: str,
) -> int:
    group_findings = await get_group_findings(group_name=group_name, loaders=loaders)
    findings_vulns = await loaders.finding_vulnerabilities_released_nzr.load_many_chained(
        [finding.id for finding in group_findings],
    )
    last_approved_status = [vuln.state.status for vuln in findings_vulns]

    return last_approved_status.count(VulnerabilityStateStatus.VULNERABLE)


@validate_length_deco("responsibility", min_length=1, max_length=50)
@validate_alphanumeric_field_deco("responsibility")
@validate_email_address_deco("email")
@validate_role_fluid_reqs_deco("email", "role")
@validate_personal_email_invitation_deco("email", "modified_by")
async def invite_to_group(
    *,
    loaders: Dataloaders,
    email: str,
    responsibility: str,
    role: str,
    group_name: str,
    modified_by: str,
) -> None:
    group = await get_group(loaders, group_name)

    expiration_time, url_token = generate_invitation_token(
        group=group,
        email=email,
        role=role,
    )

    await group_access_domain.update(
        loaders=loaders,
        email=email,
        group_name=group_name,
        metadata=GroupAccessMetadataToUpdate(
            expiration_time=expiration_time,
            state=GroupAccessState(
                modified_by=modified_by,
                modified_date=datetime_utils.get_utc_now(),
                has_access=False,
                invitation=GroupInvitation(
                    is_used=False,
                    responsibility=responsibility,
                    role=role,
                    url_token=url_token,
                ),
                responsibility=responsibility,
            ),
        ),
    )
    confirm_access_url = f"{BASE_URL}/confirm_access/{url_token}"
    reject_access_url = f"{BASE_URL}/reject_access/{url_token}"
    mail_to = [email]
    email_context: AccessGrantedContext = {
        "admin": email,
        "group": group_name,
        "responsible": modified_by,
        "group_description": group.description,
        "confirm_access_url": confirm_access_url,
        "reject_access_url": reject_access_url,
        "user_role": role.replace("_", " "),
    }
    schedule(groups_mail.send_mail_access_granted(loaders, mail_to, email_context))


@validate_fluidattacks_staff_on_group_deco("group", "email", "role")
def generate_invitation_token(group: Group, email: str, role: str) -> tuple[int, str]:
    if role:
        expiration_time = datetime_utils.get_as_epoch(datetime_utils.get_now_plus_delta(weeks=1))
        url_token = sessions_domain.encode_token(
            expiration_time=expiration_time,
            payload={
                "group_name": group.name,
                "user_email": email,
            },
            subject="starlette_session",
        )
    return expiration_time, url_token


async def exists(
    loaders: Dataloaders,
    group_name: str,
) -> bool:
    try:
        await get_group(loaders, group_name)
        return True
    except GroupNotFound:
        return False


async def is_valid(
    loaders: Dataloaders,
    group_name: str,
) -> bool:
    if await exists(loaders, group_name):
        group = await loaders.group.load(group_name)
        if group and group.state.status == GroupStateStatus.ACTIVE:
            return True
    return False


async def mask_files(
    loaders: Dataloaders,
    group_name: str,
) -> None:
    group = await get_group(loaders, group_name)
    resources_files = await resources_domain.search_file(f"{group_name}/")
    if resources_files:
        await collect(resources_domain.remove_file(file_name) for file_name in resources_files)
    if group.files:
        files: list[GroupFile] = [
            GroupFile(
                description=MASKED,
                file_name=MASKED,
                modified_by=MASKED,
                modified_date=file.modified_date,
            )
            for file in group.files
        ]
        await update_metadata(
            group_name=group_name,
            metadata=GroupMetadataToUpdate(files=files),
            organization_id=group.organization_id,
        )


@validate_file_exists_deco("file_name", "group_files")
def assign_files_to_update(
    *,
    file_name: str,
    group_files: list[GroupFile],
) -> list[GroupFile]:
    if file_name and not group_files:
        files_to_update: list[GroupFile] = []
        return files_to_update
    return group_files


async def add_file(
    *,
    loaders: Dataloaders,
    description: str,
    email: str,
    file_name: str,
    group_name: str,
    should_notify: bool = True,
    should_queue: bool = True,
) -> None:
    resources_files = await resources_domain.search_file(f"{group_name}/{file_name}")
    if not resources_files:
        raise DocumentNotFound()

    group = await get_group(loaders, group_name)
    modified_date = datetime_utils.get_utc_now()
    group_file_to_add = groups_validations.validate_file_data(
        description=description,
        file_name=file_name,
        email=email,
        modified_date=modified_date,
    )
    files_to_update = assign_files_to_update(
        file_name=file_name,
        group_files=group.files,
    )
    if group.files and should_notify:
        await send_mail_file_report(
            loaders=loaders,
            group_name=group_name,
            responsible=email,
            file_name=file_name,
            file_description=description,
            is_added=True,
            modified_date=modified_date.date(),
        )
    files_to_update.append(group_file_to_add)
    await update_metadata(
        group_name=group_name,
        metadata=GroupMetadataToUpdate(
            files=files_to_update,
        ),
        organization_id=group.organization_id,
    )
    if (
        should_queue
        and file_name.endswith(".apk")
        and (
            root_nicknames := await find_root_nicknames_for_apk_file(
                loaders,
                group_name,
                file_name,
            )
        )
    ):
        batch_action = await queue_machine_job(
            loaders=loaders,
            group_name=group_name,
            modified_by="integrates@fluidattacks.com",
            root_nicknames=root_nicknames,
            modules_to_execute=MachineModulesToExecute(
                APK=True,
                CSPM=False,
                DAST=False,
                SAST=False,
                SCA=False,
            ),
            job_origin=MachineJobOrigin.PLATFORM,
            job_technique=MachineJobTechnique.APK,
        )
        LOGGER.info(
            "Machine execution queued",
            extra={
                "extra": {
                    "batch_action": batch_action,
                    "group_name": group_name,
                    "root_nickname": root_nicknames,
                },
            },
        )


async def remove_file(
    *,
    loaders: Dataloaders,
    email: str,
    file_name: str,
    group_name: str,
    should_notify: bool = True,
) -> None:
    group = await get_group(loaders, group_name)
    if not group.files:
        raise ErrorUpdatingGroup.new()

    file_to_remove: GroupFile | None = next(
        (file for file in group.files if file.file_name == file_name),
        None,
    )
    if not file_to_remove:
        raise ErrorUpdatingGroup.new()

    if file_name.endswith(".apk"):
        environment_urls = await loaders.group_environment_urls.load(
            GroupEnvironmentUrlsRequest(group_name=group_name),
        )
        if any(root_env.url == file_name for root_env in environment_urls):
            raise ErrorUpdatingGroup.new()

    file_url = f"{group_name}/{file_name}"
    await resources_domain.remove_file(file_url)
    await update_metadata(
        group_name=group_name,
        metadata=GroupMetadataToUpdate(
            files=[file for file in group.files if file.file_name != file_to_remove.file_name],
        ),
        organization_id=group.organization_id,
    )
    if should_notify:
        uploaded_date = (
            file_to_remove.modified_date.date() if file_to_remove.modified_date else None
        )
        await send_mail_file_report(
            loaders=loaders,
            group_name=group_name,
            responsible=email,
            file_name=file_name,
            file_description=file_to_remove.description,
            modified_date=datetime_utils.get_utc_now().date(),
            uploaded_date=uploaded_date,
        )


async def send_mail_file_report(
    *,
    loaders: Dataloaders,
    group_name: str,
    responsible: str,
    file_name: str,
    file_description: str,
    is_added: bool = False,
    modified_date: date,
    uploaded_date: date | None = None,
) -> None:
    stakeholders_email = await mailer_utils.get_group_emails_by_notification(
        loaders=loaders,
        group_name=group_name,
        notification="file_report",
    )

    await groups_mail.send_mail_file_report(
        loaders=loaders,
        group_name=group_name,
        responsible=responsible,
        is_added=is_added,
        file_name=file_name,
        file_description=file_description,
        report_date=modified_date,
        email_to=stakeholders_email,
        uploaded_date=uploaded_date,
    )


async def remove_all_stakeholders(
    *,
    loaders: Dataloaders,
    group_name: str,
    modified_by: str,
) -> None:
    """Revoke stakeholders access to group."""
    stakeholders_access = await loaders.group_stakeholders_access.load(
        GroupStakeholdersAccessRequest(group_name=group_name),
    )
    for access in stakeholders_access:
        try:
            await remove_stakeholder(
                loaders=loaders,
                email_to_revoke=access.email,
                group_name=group_name,
                modified_by=modified_by,
            )
        except CredentialInUse:
            LOGGER.warning(
                "Stakeholder with credentials in use prior to deletion",
                extra={
                    "extra": {
                        "email": access.email,
                        "group_name": group_name,
                    },
                },
            )


async def _remove_all_roots(
    *,
    loaders: Dataloaders,
    email: str,
    group_name: str,
) -> None:
    await collect(
        tuple(
            roots_domain.remove_root(
                loaders=loaders,
                email=email,
                group_name=group_name,
                reason=RootStateReason.GROUP_DELETED,
                root=root,
            )
            for root in await loaders.group_roots.load(group_name)
        ),
        workers=1,
    )


@retry_on_exceptions(
    exceptions=(ClientError,),
    max_attempts=3,
    sleep_seconds=10,
)
async def _remove_all_toe(
    *,
    group_name: str,
) -> None:
    await toe_inputs_model.remove_group_toe_inputs(group_name=group_name)
    await toe_lines_model.remove_group_toe_lines(group_name=group_name)
    await toe_ports_model.remove_group_toe_ports(group_name=group_name)
    LOGGER.info(
        "Group's toe removed",
        extra={"extra": {"group_name": group_name}},
    )


async def _remove_all_batch_actions(
    group_name: str,
    validate_pending_actions: bool,
) -> None:
    group_actions: list[BatchProcessing] = [
        action
        for action in await batch_dal.get_actions()
        if action.entity == group_name and action.action_name != Action.REMOVE_GROUP_RESOURCES
    ]
    if validate_pending_actions:
        cancelable_actions = {
            Action.CLONE_ROOTS,
            Action.EXECUTE_MACHINE,
            Action.EXECUTE_MACHINE_SAST,
            Action.GENERATE_ROOT_SBOM,
            Action.GENERATE_IMAGE_SBOM,
            Action.REBASE,
            Action.REFRESH_TOE_INPUTS,
            Action.REFRESH_TOE_LINES,
            Action.REFRESH_TOE_PORTS,
            Action.UPDATE_GROUP_LANGUAGES,
            Action.UPDATE_TOE_INPUTS,
            Action.UPDATE_VULNS_AUTHOR,
        }
        pending_actions = [
            action
            for action in group_actions
            if action.action_name not in cancelable_actions and action.batch_job_id
        ]
        if pending_actions:
            raise GroupHasPendingActions(
                action_names=[action.action_name for action in pending_actions],
            )

    await collect(
        [
            batch_dal.cancel_batch_job(
                job_id=action.batch_job_id,
                reason="GROUP_REMOVAL",
            )
            for action in group_actions
            if action.batch_job_id
        ],
        workers=8,
    )
    await collect(
        [batch_dal.delete_action(action_key=action.key) for action in group_actions],
        workers=8,
    )
    LOGGER.info(
        "Group's batch actions removed",
        extra={
            "extra": {
                "group_name": group_name,
                "actions": [action.action_name for action in group_actions],
            },
        },
    )


@retry_on_exceptions(
    exceptions=(UnavailabilityError,),
    max_attempts=3,
    sleep_seconds=10,
)
async def remove_resources(
    *,
    loaders: Dataloaders,
    email: str,
    group_name: str,
    unique_authors: int = 0,
    validate_pending_actions: bool = True,
    subscription: GroupSubscriptionType = GroupSubscriptionType.CONTINUOUS,
    has_advanced: bool = False,
    should_notify: bool = True,
    emails: list[str] = [],
) -> None:
    loaders.group.clear(group_name)
    group = await get_group(loaders, group_name)

    await _remove_all_batch_actions(
        group_name=group_name,
        validate_pending_actions=validate_pending_actions,
    )

    await _remove_all_roots(
        loaders=loaders,
        email=email,
        group_name=group_name,
    )
    await remove_all_stakeholders(
        loaders=loaders,
        group_name=group_name,
        modified_by=email,
    )
    all_findings = await loaders.group_findings_all.load(group_name)
    await collect(
        tuple(findings_domain.mask_finding(loaders, finding, email) for finding in all_findings),
        workers=4,
    )
    group_events = await loaders.group_events.load(GroupEventsRequest(group_name=group_name))
    await collect(
        tuple(events_domain.remove_event(event.id, group_name) for event in group_events),
        workers=4,
    )
    await mask_files(loaders, group_name)
    await _remove_all_toe(group_name=group_name)
    await forces_model.remove_group_forces_executions(group_name=group_name)
    await groups_model.remove(group_name=group_name)

    if should_notify:
        reason = (
            group.state.justification
            if group.state.justification
            else GroupStateJustification.OTHER
        )
        comments = group.state.comments or "No comment."
        await tickets_domain.delete_group(
            loaders=loaders,
            deletion_date=datetime_utils.get_utc_now(),
            group=group._replace(
                state=group.state._replace(
                    has_advanced=has_advanced,
                    type=subscription,
                ),
            ),
            unique_authors=unique_authors,
            requester_email=email,
            reason=reason.value,
            comments=comments,
            attempt=False,
            emails=emails,
        )
    organization = await loaders.organization.load(group.organization_id)
    LOGGER.info(
        "Remove group resources completed",
        extra={
            "extra": {
                "group_name": group.name,
                "organization_id": organization.id if organization else None,
                "organization_name": organization.name if organization else None,
            },
        },
    )


async def remove_stakeholder(
    *,
    loaders: Dataloaders,
    email_to_revoke: str,
    group_name: str,
    modified_by: str,
) -> None:
    """
    Revoke stakeholder access to group.
    If the stakeholder has no access to other active groups in the
    organization, revoke access to organization.
    If no active groups are left for the stakeholder at this point, remove
    the stakeholder completely.
    """
    await group_access_domain.remove_access(loaders, email_to_revoke, group_name)

    loaders = get_new_context()
    group = await get_group(loaders, group_name)
    organization_id = group.organization_id
    has_org_access = await org_access.has_access(loaders, organization_id, email_to_revoke)
    stakeholder_org_groups_names = await get_groups_by_stakeholder(
        loaders,
        email_to_revoke,
        organization_id=organization_id,
    )
    stakeholder_org_groups = await collect(
        [
            get_group(loaders, stakeholder_org_group_name)
            for stakeholder_org_group_name in stakeholder_org_groups_names
        ],
        workers=32,
    )
    has_groups_in_org = bool(groups_utils.filter_active_groups(stakeholder_org_groups))
    if has_org_access and not has_groups_in_org:
        await orgs_domain.remove_access(
            organization_id=organization_id,
            email=email_to_revoke,
            modified_by=modified_by,
        )

    LOGGER.info(
        "Stakeholder removed from group",
        extra={
            "extra": {
                "email": email_to_revoke,
                "group_name": group_name,
                "modified_by": modified_by,
            },
        },
    )

    loaders = get_new_context()
    stakeholder_groups_names = await get_groups_by_stakeholder(loaders, email_to_revoke)
    all_groups_by_stakeholder = await collect(
        (
            get_group(loaders, stakeholder_group_name)
            for stakeholder_group_name in stakeholder_groups_names
        ),
        workers=32,
    )
    all_active_groups_by_stakeholder = groups_utils.filter_active_groups(all_groups_by_stakeholder)
    has_groups_in_asm = bool(all_active_groups_by_stakeholder)
    if not has_groups_in_asm:
        await stakeholders_domain.remove(email_to_revoke)


async def unsubscribe_from_group(
    *,
    loaders: Dataloaders,
    group_name: str,
    email: str,
) -> None:
    await remove_stakeholder(
        loaders=loaders,
        email_to_revoke=email,
        group_name=group_name,
        modified_by=email,
    )
    await send_mail_unsubscribed(
        loaders=loaders,
        group_name=group_name,
        email=email,
    )


async def send_mail_unsubscribed(
    *,
    loaders: Dataloaders,
    email: str,
    group_name: str,
) -> None:
    report_date = datetime_utils.get_utc_now()
    stakeholders_email = await mailer_utils.get_group_emails_by_notification(
        loaders=loaders,
        group_name=group_name,
        notification="user_unsubscribed",
    )

    await groups_mail.send_mail_stakeholder_unsubscribed(
        loaders=loaders,
        email=email,
        email_to=stakeholders_email,
        group_name=group_name,
        report_date=report_date.date(),
    )


@validate_length_deco("business_id", max_length=60)
@validate_length_deco("business_name", max_length=60)
@validate_fields_deco(["business_name"])
@validate_length_deco("description", max_length=200)
@validate_fields_deco(["description"])
@validate_group_language_deco("language")
def assign_metadata(
    *,
    business_id: str | None,
    business_name: str | None,
    description: str,
    language: str,
    sprint_start_date: datetime | None,
    sprint_duration: int | None,
    tzn: Any | None,
) -> GroupMetadataToUpdate:
    if sprint_duration is not None:
        check_range(sprint_duration, 1, 10, True, NumberOutOfRange(1, 10, True))

    return GroupMetadataToUpdate(
        business_id=business_id,
        business_name=business_name,
        description=description,
        language=GroupLanguage[language.upper()],
        sprint_duration=sprint_duration if sprint_duration else NoUpdate(),
        sprint_start_date=get_min_iso_date(sprint_start_date.astimezone(tzn))
        if sprint_start_date
        else None,
    )


@validate_length_deco("metadata.context", max_length=20000)
@validate_length_deco("metadata.disambiguation", max_length=10000)
async def update_metadata(
    *,
    group_name: str,
    metadata: GroupMetadataToUpdate,
    organization_id: str,
) -> None:
    await groups_model.update_metadata(
        group_name=group_name,
        metadata=metadata,
        organization_id=organization_id,
    )


async def update_group_info(
    *,
    loaders: Dataloaders,
    group_name: str,
    metadata: GroupMetadataToUpdate,
    email: str,
) -> None:
    group = await get_group(loaders, group_name)

    stakeholders_email = await mailer_utils.get_group_emails_by_notification(
        loaders=loaders,
        group_name=group_name,
        notification="updated_group_info",
    )

    await update_metadata(
        group_name=group_name,
        metadata=metadata,
        organization_id=group.organization_id,
    )

    if metadata:
        await groups_mail.send_mail_updated_group_information(
            loaders=loaders,
            group_name=group_name,
            responsible=email,
            group=group,
            metadata=metadata,
            report_date=datetime_utils.get_utc_now(),
            email_to=stakeholders_email,
        )


async def update_forces_access_token(
    *,
    loaders: Dataloaders,
    group_name: str,
    email: str,
    expiration_time: int,
    responsible: str,
) -> str:
    group = await get_group(loaders, group_name)
    had_token: bool = bool(group.agent_token)

    result = await stakeholders_domain.update_access_token(
        email=email,
        expiration_time=expiration_time,
        loaders=loaders,
        name="Forces Token",
    )
    await send_mail_devsecops_agent(
        loaders=loaders,
        group_name=group_name,
        responsible=responsible,
        had_token=had_token,
    )

    return result


async def send_mail_devsecops_agent(
    *,
    loaders: Dataloaders,
    group_name: str,
    responsible: str,
    had_token: bool,
) -> None:
    report_date = datetime_utils.get_utc_now()
    stakeholders_email = await mailer_utils.get_group_emails_by_notification(
        loaders=loaders,
        group_name=group_name,
        notification="devsecops_agent_token",
    )

    await groups_mail.send_mail_devsecops_agent_token(
        loaders=loaders,
        email=responsible,
        email_to=stakeholders_email,
        group_name=group_name,
        had_token=had_token,
        report_date=report_date.date(),
    )


async def update_state(
    *,
    group_name: str,
    state: GroupState,
    organization_id: str,
) -> None:
    await groups_model.update_state(
        group_name=group_name,
        state=state,
        organization_id=organization_id,
    )


async def update_indicators(
    *,
    group_name: str,
    indicators: GroupUnreliableIndicators,
) -> None:
    await groups_model.update_unreliable_indicators(
        group_name=group_name,
        indicators=indicators,
    )


async def set_pending_deletion_date(
    group: Group,
    modified_by: str,
    pending_deletion_date: datetime,
) -> None:
    """Update pending deletion date in group's state."""
    await update_state(
        group_name=group.name,
        organization_id=group.organization_id,
        state=group.state._replace(
            modified_by=modified_by,
            modified_date=datetime_utils.get_utc_now(),
            pending_deletion_date=pending_deletion_date,
        ),
    )


async def remove_pending_deletion_date(
    group: Group,
    modified_by: str,
) -> None:
    """Clear pending deletion date in group's state."""
    await update_state(
        group_name=group.name,
        organization_id=group.organization_id,
        state=group.state._replace(
            modified_by=modified_by,
            modified_date=datetime_utils.get_utc_now(),
            pending_deletion_date=None,
        ),
    )


async def update_group_tags(
    *,
    loaders: Dataloaders,
    group_name: str,
    email: str,
    updated_tags: set[str],
) -> None:
    group = await get_group(loaders, group_name)

    if updated_tags != group.state.tags:
        await update_state(
            group_name=group_name,
            organization_id=group.organization_id,
            state=GroupState(
                comments=group.state.comments,
                modified_date=datetime_utils.get_utc_now(),
                has_essential=group.state.has_essential,
                has_advanced=group.state.has_advanced,
                managed=group.state.managed,
                payment_id=group.state.payment_id,
                justification=GroupStateJustification.NONE,
                modified_by=email,
                service=group.state.service,
                status=GroupStateStatus.ACTIVE,
                tags=updated_tags,
                tier=group.state.tier,
                type=group.state.type,
            ),
        )


async def add_tags(
    *,
    loaders: Dataloaders,
    email: str,
    group: Group,
    tags_to_add: set[str],
) -> None:
    updated_tags = group.state.tags.union(tags_to_add) if group.state.tags else tags_to_add
    await update_group_tags(
        loaders=loaders,
        group_name=group.name,
        email=email,
        updated_tags=updated_tags,
    )
    schedule(
        send_mail_portfolio_report(
            loaders=loaders,
            group_name=group.name,
            responsible=email,
            portfolio=", ".join(tags_to_add),
            is_added=True,
            modified_date=datetime_utils.get_utc_now(),
        ),
    )


async def remove_tag(
    *,
    loaders: Dataloaders,
    email: str,
    group: Group,
    tag_to_remove: str,
) -> None:
    if group.state.tags:
        updated_tags: set[str] = {tag for tag in group.state.tags if tag != tag_to_remove}
        await update_group_tags(
            loaders=loaders,
            group_name=group.name,
            email=email,
            updated_tags=updated_tags,
        )
        schedule(
            send_mail_portfolio_report(
                loaders=loaders,
                group_name=group.name,
                responsible=email,
                portfolio=tag_to_remove,
                modified_date=datetime_utils.get_utc_now(),
            ),
        )


async def send_mail_portfolio_report(
    *,
    loaders: Dataloaders,
    group_name: str,
    responsible: str,
    portfolio: str,
    is_added: bool = False,
    modified_date: datetime,
) -> None:
    stakeholders_email = await mailer_utils.get_group_emails_by_notification(
        loaders=loaders,
        group_name=group_name,
        notification="portfolio_report",
    )

    await groups_mail.send_mail_portfolio_report(
        loaders=loaders,
        group_name=group_name,
        responsible=responsible,
        is_added=is_added,
        portfolio=portfolio,
        report_date=modified_date.date(),
        email_to=stakeholders_email,
    )


async def request_upgrade(
    *,
    loaders: Dataloaders,
    email: str,
    group_names: list[str],
) -> None:
    """
    Lead the stakeholder towards a subscription upgrade managed by our team.
    This is meant to be a temporary flow while the billing module gets ready.
    """
    enforcer = await authz.get_group_level_enforcer(loaders, email)
    if not all(enforcer(group_name, "request_group_upgrade") for group_name in group_names):
        raise GroupNotFound()

    groups = await collect([get_group(loaders, group_name) for group_name in group_names])
    if any(group.state.has_advanced for group in groups):
        raise BillingSubscriptionSameActive()

    await tickets_domain.request_groups_upgrade(loaders, email, groups)


async def get_treatment_summary(
    loaders: Dataloaders,
    group_name: str,
) -> GroupTreatmentSummary:
    """Get the total vulnerability treatment."""
    findings = await get_group_findings(group_name=group_name, loaders=loaders)
    non_deleted_findings = tuple(
        finding for finding in findings if not findings_utils.is_deleted(finding)
    )
    vulns = await loaders.finding_vulnerabilities_released_nzr.load_many_chained(
        [finding.id for finding in non_deleted_findings],
    )
    treatment_counter = Counter(
        vuln.treatment.status
        for vuln in vulns
        if vuln.treatment and vuln.state.status == VulnerabilityStateStatus.VULNERABLE
    )
    return GroupTreatmentSummary(
        accepted=treatment_counter[TreatmentStatus.ACCEPTED],
        accepted_undefined=treatment_counter[TreatmentStatus.ACCEPTED_UNDEFINED],
        in_progress=treatment_counter[TreatmentStatus.IN_PROGRESS],
        untreated=treatment_counter[TreatmentStatus.UNTREATED],
    )


async def get_oldest_finding_date(loaders: Dataloaders, group_name: str) -> datetime | None:
    findings = await get_group_findings(group_name=group_name, loaders=loaders)
    findings_indicators = [finding.unreliable_indicators for finding in findings]
    ages: list[datetime] = [
        finding_indicators.oldest_vulnerability_report_date
        for finding_indicators in findings_indicators
        if finding_indicators.oldest_vulnerability_report_date
    ]
    if ages:
        return min(ages)
    return None


async def update_policies(
    *,
    loaders: Dataloaders,
    email: str,
    group_name: str,
    organization_id: str,
    policies_to_update: PoliciesToUpdate,
) -> None:
    validated_policies = orgs_domain.get_validated_policies_to_update(policies_to_update)
    orgs_domain.validate_acceptance_days(values=policies_to_update)
    await groups_validations.validate_acceptance_severity_range(
        group_name=group_name,
        loaders=loaders,
        values=policies_to_update,
    )
    orgs_domain.validate_days_until_it_breaks_range(values=policies_to_update)

    if validated_policies:
        today = datetime_utils.get_utc_now()
        await groups_model.update_policies(
            group_name=group_name,
            modified_by=email,
            modified_date=today,
            organization_id=organization_id,
            policies=policies_to_update,
        )
        schedule(
            send_mail_policies(
                group_name=group_name,
                loaders=loaders,
                modified_date=today,
                new_policies=policies_to_update._asdict(),
                responsible=email,
            ),
        )


async def send_mail_policies(
    *,
    group_name: str,
    loaders: Dataloaders,
    modified_date: datetime,
    new_policies: dict[str, Decimal],
    responsible: str,
) -> None:
    group_data = await get_group(loaders, group_name)
    organization_data = await orgs_utils.get_organization(loaders, group_data.organization_id)

    policies_content: dict[str, Any] = {}
    for key, val in new_policies.items():
        old_value = (
            group_data.policies._asdict().get(key)
            if group_data.policies
            else organization_data.policies._asdict().get(key)
        )
        if val is not None and val != old_value:
            policies_content[POLICIES_FORMATTED[key]] = {
                "from": old_value,
                "to": val,
            }

    email_context: UpdatedPoliciesContext = {
        "entity_name": group_name,
        "entity_type": "group",
        "policies_link": (f"{BASE_URL}/orgs/{organization_data.name}/groups/{group_name}/scope"),
        "policies_content": policies_content,
        "responsible": responsible,
        "date": datetime_utils.get_as_str(modified_date),
    }

    members_emails = await mailer_utils.get_group_emails_by_notification(
        loaders=loaders,
        group_name=group_name,
        notification="updated_policies",
    )

    if policies_content:
        await groups_mail.send_mail_updated_policies(
            loaders=loaders,
            email_to=members_emails,
            context=email_context,
        )


async def update_group_hook(
    *,
    loaders: Dataloaders,
    group_name: str,
    hook_id: str,
    user_email: str,
    hook: GroupHookPayload,
) -> None:
    validations.check_protocol(hook.entry_point)
    db_hook = await loaders.hook.load(GroupHookRequest(group_name=group_name, id=hook_id))
    if not db_hook:
        raise HookNotFound()
    modified_date = datetime_utils.get_utc_now()

    await update(
        group_name=group_name,
        hook_id=hook_id,
        user_email=user_email,
        modified_date=modified_date,
        hook=hook,
    )


async def add_group_hook(
    *,
    loaders: Dataloaders,
    group_name: str,
    user_email: str,
    hook: GroupHookPayload,
) -> None:
    validations.check_protocol(hook.entry_point)
    modified_date = datetime_utils.get_utc_now()
    hook_id = str(uuid.uuid4())
    group_hooks = await loaders.group_hook.load(group_name)
    groups_validations.validate_hook_add(hook, group_hooks)

    await add_hook(
        group_name=group_name,
        hook_id=hook_id,
        user_email=user_email,
        modified_date=modified_date,
        hook=hook,
    )


async def delete_group_hook(
    *,
    loaders: Dataloaders,
    group_name: str,
    hook_id: str,
) -> None:
    db_hook = await loaders.hook.load(GroupHookRequest(group_name=group_name, id=hook_id))
    if not db_hook:
        raise HookNotFound()

    await remove(
        group_name=group_name,
        hook_id=hook_id,
    )


async def fetch_and_remove_repositories(
    loaders: Dataloaders,
    organization_id: str,
    url: str,
) -> None:
    with suppress(LocationParseError, UnavailabilityError):
        _repositories = await loaders.organization_unreliable_outside_repositories.load(
            (
                organization_id,
                hashlib.sha256(
                    (
                        url
                        if url.startswith("ssh://") or bool(re.match(r"^\w+@.*", url))
                        else parse_url(url)._replace(auth=None).url
                    ).encode("utf-8"),
                ).hexdigest(),
                None,
            ),
        )
        await collect(
            [remove_integration_repository(repository=repository) for repository in _repositories],
            workers=4,
        )


async def get_groups_owned_by_user(*, loaders: Dataloaders, email: str) -> list[str]:
    user_groups = await get_groups_by_stakeholder(loaders=loaders, email=email)
    groups = await loaders.group.load_many(user_groups)

    return [
        group.name
        for group in groups
        if group is not None and group.created_by.lower() == email.lower()
    ]


async def update_environment_url(
    *,
    loaders: Dataloaders,
    group_name: str,
    root_id: str,
    url_id: str,
    file_name: str,
    old_file_name: str,
    user_email: str,
) -> None:
    group = await get_group(loaders, group_name)
    if not group.files:
        raise ErrorUpdatingGroup.new()

    validate_filename(file_name.lower())
    file_to_remove = next((file for file in group.files if file.file_name == old_file_name), None)
    if not file_to_remove:
        raise ErrorUpdatingGroup.new()

    root = await get_active_git_root(loaders, root_id, group.name)
    environment_url = await roots_domain.get_environment_url(loaders, root.id, group.name, url_id)
    if (
        environment_url.url != old_file_name
        or environment_url.state.url_type is not RootEnvironmentUrlType.APK
    ):
        raise RootEnvironmentUrlNotFound()
    if file_to_remove.file_name == file_name:
        raise RepeatedRootEnvironmentFile()

    if not await resources_domain.search_file(f"{group.name}/{file_name}"):
        raise DocumentNotFound()

    await add_file(
        loaders=get_new_context(),
        description=file_to_remove.description,
        file_name=file_name,
        group_name=group.name,
        email=user_email,
        should_notify=False,
        should_queue=False,
    )
    group = await get_group(get_new_context(), group_name)

    await roots_environments.add_root_environment_url(
        loaders=get_new_context(),
        group_name=group.name,
        root_id=root.id,
        url=file_name,
        url_type="APK",
        user_email=user_email,
        use_connection={
            "use_egress": False,
            "use_vpn": False,
            "use_ztna": False,
        },
        is_production=environment_url.state.is_production,
    )
    urls = await get_new_context().root_environment_urls.load(
        RootEnvironmentUrlsRequest(root_id=root.id, group_name=group_name),
    )
    url_ids = [
        url.id
        for url in urls
        if url.url == file_name and url.state.status != RootEnvironmentUrlStateStatus.DELETED
    ]
    current_secrets = await roots_domain.get_environment_url_secrets(loaders, environment_url)
    await collect(
        [
            roots_domain.add_secret(
                secret=secret,
                resource_id=url_ids[-1],
                resource_type=ResourceType.URL,
                group_name=group.name,
                loaders=loaders,
            )
            for secret in current_secrets
        ],
    )
    await collect(
        [
            roots_domain.remove_secret(
                resource_id=environment_url.id,
                secret_key=secret.key,
                resource_type=ResourceType.URL,
                group_name=group_name,
                loaders=loaders,
            )
            for secret in current_secrets
        ],
    )
    await roots_domain.remove_environment_url_id(
        loaders=get_new_context(),
        root_id=root.id,
        url_id=environment_url.id,
        user_email=user_email,
        group_name=group.name,
        should_notify=False,
    )
    await remove_file(
        loaders=get_new_context(),
        group_name=group.name,
        file_name=old_file_name,
        email=user_email,
        should_notify=False,
    )

    if url_ids:
        await put_action(
            action=Action.UPDATE_TOE_INPUTS,
            attempt_duration_seconds=7200,
            entity=url_ids[-1],
            subject=user_email,
            additional_info={
                "group_name": group.name,
                "root_id": root.id,
                "old_url": environment_url.url,
            },
            queue=IntegratesBatchQueue.SMALL,
        )
