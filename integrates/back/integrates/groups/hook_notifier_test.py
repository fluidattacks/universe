import fluidattacks_core.http

from integrates.custom_exceptions import HostNotFound
from integrates.dataloaders import get_new_context
from integrates.groups.hook_notifier import check_hook, process_hook_event
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import GroupFaker, GroupHookFaker, OrganizationFaker
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import create_fake_client_response, raises

ORG_ID = "ORG#9812t7d3-34g9-2d0o-9ur457j1p3"
HOOK_ID = "23ft94-hook-id"
GROUP_NAME = "testgroup"
TEST_TKN = "test-token"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            group_hook=[
                GroupHookFaker(
                    group_name=GROUP_NAME,
                    id=HOOK_ID,
                    name="test-hook",
                    entry_point="https://webhook.site/test",
                    token=TEST_TKN,
                )
            ],
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            organizations=[OrganizationFaker(id=ORG_ID, name="orgtest")],
        ),
    ),
    others=[
        Mock(
            fluidattacks_core.http,
            "request",
            "async",
            create_fake_client_response(status="200", json_data={}),
        )
    ],
)
async def test_process_and_check_hook_event() -> None:
    # The hook notifier is an external process, here we check there is not exception raised.
    # Act
    loaders = get_new_context()
    await check_hook(
        event="VULNERABILITY_CREATED",
        group=GROUP_NAME,
        hook={
            "token": TEST_TKN,
            "token_header": "token header",
            "entry_point": "https://webhook.site/test",
        },
    )
    await process_hook_event(
        loaders=loaders,
        group_name=GROUP_NAME,
        info={},
        event="VULNERABILITY_CREATED",
    )


@mocks()
async def test_check_hook_event_fail() -> None:
    with raises(HostNotFound):
        await check_hook(
            event="VULNERABILITY_CREATED",
            group=GROUP_NAME,
            hook={
                "token": TEST_TKN,
                "token_header": "token header",
                "entry_point": "https://test-not-exist",
            },
        )
