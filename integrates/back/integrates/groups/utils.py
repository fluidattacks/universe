from integrates.dataloaders import (
    Dataloaders,
)


async def has_repeated_tags(loaders: Dataloaders, group_name: str, tags: list[str]) -> bool:
    has_repeated_tags_ = len(tags) != len(set(tags))
    if not has_repeated_tags_:
        group = await loaders.group.load(group_name)
        existing_tags = group.state.tags if group else None
        all_tags = list(existing_tags or {}) + tags
        has_repeated_tags_ = len(all_tags) != len(set(all_tags))
    return has_repeated_tags_
