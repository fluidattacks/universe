import asyncio
import logging
import tempfile
from collections import Counter
from collections.abc import Iterable
from contextlib import suppress
from operator import attrgetter

import simplejson as json

from integrates.custom_exceptions import GroupNotFound, IndicatorAlreadyUpdated, UnavailabilityError
from integrates.db_model.groups.types import GroupUnreliableIndicators
from integrates.db_model.roots.types import GitRoot, RootUnreliableIndicatorsToUpdate
from integrates.db_model.roots.update import update_unreliable_indicators as update_root_indicators
from integrates.db_model.types import CodeLanguage
from integrates.decorators import retry_on_exceptions
from integrates.groups import domain as groups_domain
from integrates.roots.s3_mirror import download_repo

LOGGER = logging.getLogger(__name__)


async def _run_tokei_over_repo(repo_working_dir: str) -> dict[str, int]:
    proc = await asyncio.create_subprocess_exec(
        "tokei",
        "-o",
        "json",
        repo_working_dir,
        stderr=asyncio.subprocess.PIPE,
        stdout=asyncio.subprocess.PIPE,
    )
    stdout, stderr = await proc.communicate()
    if proc.returncode != 0:
        LOGGER.error(
            "Error running tokei over repository",
            extra={
                "extra": {
                    "error": stderr.decode(),
                    "repository": repo_working_dir,
                },
            },
        )

        return {}

    result = json.loads(stdout.decode())
    result.pop("Total", None)
    languages_stats = {}
    for language in result.keys():
        loc = result[language]["code"] + result[language]["comments"]
        if children := result[language]["children"]:
            loc += sum(
                child["stats"]["code"] + child["stats"]["comments"]
                for child_lang in children.keys()
                for child in children[child_lang]
            )
        languages_stats.update({language: loc})

    return languages_stats


def format_code_languages(languages_stats: Iterable[CodeLanguage]) -> list[CodeLanguage]:
    return sorted(
        [item for item in languages_stats if item.loc > 0],
        key=attrgetter("language"),
    )


async def get_root_code_languages(git_root: GitRoot) -> list[CodeLanguage]:
    with tempfile.TemporaryDirectory(
        prefix="integrates_languages_", ignore_cleanup_errors=True
    ) as tmpdir:
        repo = await download_repo(
            git_root.group_name,
            git_root.state.nickname,
            tmpdir,
            git_root.state.gitignore,
        )
        if not repo:
            return []

        languages_stats = await _run_tokei_over_repo(str(repo.working_dir))

        return format_code_languages(
            [CodeLanguage(language, loc) for language, loc in languages_stats.items()]
        )


def _sum_roots_languages(roots_languages: Iterable[Iterable[CodeLanguage]]) -> list[CodeLanguage]:
    group_languages_count: Counter[str] = Counter()
    for language_stats in roots_languages:
        root_languages_count = Counter({entry.language: entry.loc for entry in language_stats})
        group_languages_count.update(root_languages_count)

    return format_code_languages(
        [CodeLanguage(language, loc) for language, loc in dict(group_languages_count).items()]
    )


@retry_on_exceptions(exceptions=(UnavailabilityError,), sleep_seconds=40, max_attempts=5)
async def update_language_indicators(
    group_name: str,
    git_roots: Iterable[GitRoot],
    roots_languages: Iterable[Iterable[CodeLanguage]],
) -> None:
    for root, root_languages in zip(git_roots, roots_languages, strict=True):
        with suppress(IndicatorAlreadyUpdated):
            await update_root_indicators(
                current_value=root.unreliable_indicators,
                group_name=root.group_name,
                indicators=RootUnreliableIndicatorsToUpdate(
                    unreliable_code_languages=format_code_languages(root_languages)
                ),
                root_id=root.id,
            )
            LOGGER.info(
                "Root language stats updated",
                extra={"extra": {"nickname": root.state.nickname, "root_id": root.id}},
            )

    with suppress(GroupNotFound):
        await groups_domain.update_indicators(
            group_name=group_name,
            indicators=GroupUnreliableIndicators(
                code_languages=_sum_roots_languages(roots_languages)
            ),
        )

    LOGGER.info("Group language stats were updated", extra={"extra": {"name": group_name}})
