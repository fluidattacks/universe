from datetime import datetime
from decimal import Decimal

import integrates.s3.operations
from integrates.authz.policy import (
    get_group_level_role,
    get_organization_level_role,
    get_user_level_role,
)
from integrates.batch.dal.get import get_actions_by_name
from integrates.batch.enums import Action
from integrates.custom_exceptions import (
    BillingSubscriptionSameActive,
    CustomBaseException,
    DocumentNotFound,
    ErrorFileNameAlreadyExists,
    ErrorUpdatingGroup,
    GroupHasPendingActions,
    GroupNotFound,
    HookNotFound,
    InvalidAcceptanceSeverityRange,
    InvalidField,
    InvalidFieldLength,
    InvalidGroupName,
    InvalidGroupServicesConfig,
    InvalidManagedChange,
    StakeholderNotInOrganization,
    TrialRestriction,
)
from integrates.custom_utils import datetime as datetime_utils
from integrates.custom_utils.groups import get_group
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.enums import TreatmentStatus
from integrates.db_model.findings.types import FindingVulnerabilitiesSummary
from integrates.db_model.group_access.types import GroupAccess, GroupAccessRequest
from integrates.db_model.groups.enums import (
    GroupLanguage,
    GroupManaged,
    GroupService,
    GroupStateJustification,
    GroupSubscriptionType,
    GroupTier,
)
from integrates.db_model.groups.types import GroupFile, GroupUnreliableIndicators
from integrates.db_model.hook.enums import HookEvent
from integrates.db_model.hook.types import GroupHookPayload, GroupHookRequest
from integrates.db_model.roots.enums import RootEnvironmentUrlStateStatus, RootEnvironmentUrlType
from integrates.db_model.roots.types import RootEnvironmentUrlsRequest, Secret, SecretState
from integrates.db_model.types import PoliciesToUpdate, SeverityScore, Treatment
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
    VulnerabilityVerificationStatus,
)
from integrates.db_model.vulnerabilities.types import VulnerabilityVerification
from integrates.groups.domain import (
    _add_managers_access,
    add_file,
    add_group,
    add_group_hook,
    add_tags,
    assign_metadata,
    complete_register_for_group_invitation,
    delete_group_hook,
    fetch_and_remove_repositories,
    get_closed_vulnerabilities,
    get_groups_by_stakeholder,
    get_groups_owned_by_user,
    get_max_severity,
    get_mean_remediate_non_treated_severity,
    get_mean_remediate_non_treated_severity_cvssf,
    get_mean_remediate_severity,
    get_mean_remediate_severity_cvssf,
    get_open_findings,
    get_open_vulnerabilities,
    get_total_priority_findings,
    get_treatment_summary,
    get_vulnerabilities_with_pending_attacks,
    invite_to_group,
    is_valid,
    reject_register_for_group_invitation,
    remove_file,
    remove_group,
    remove_pending_deletion_date,
    remove_tag,
    request_upgrade,
    set_pending_deletion_date,
    unsubscribe_from_group,
    update_environment_url,
    update_forces_access_token,
    update_group,
    update_group_hook,
    update_group_info,
    update_group_managed,
    update_group_tier,
    update_indicators,
    update_policies,
)
from integrates.mailer import groups
from integrates.resources import domain as resources_domain
from integrates.roots import domain as roots_domain
from integrates.roots.domain import format_environment_id
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb, IntegratesS3
from integrates.testing.fakers import (
    DATE_2024,
    DATE_2025,
    BatchProcessingFaker,
    FindingFaker,
    FindingUnreliableIndicatorsFaker,
    GitRootFaker,
    GitRootStateFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    GroupHookFaker,
    GroupStateFaker,
    OrganizationAccessFaker,
    OrganizationAccessStateFaker,
    OrganizationFaker,
    OrganizationIntegrationRepositoryFaker,
    PortfolioFaker,
    RootEnvironmentUrlFaker,
    RootEnvironmentUrlStateFaker,
    StakeholderFaker,
    TrialFaker,
    VulnerabilityFaker,
    VulnerabilityStateFaker,
    random_uuid,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import freeze_time, parametrize, raises
from integrates.verify.enums import ResourceType

ORG_ID = "ORG#6b110f25-cd46-e94c-2bad-b9e182cf91a8"
FIN_ID = "40rf1234-s0lq-6j32-a1p0-nt9o2sa1ln7c"
HOOK_ID = "hook-id-test"
USER_EMAIL = "integratesuser@fluidattacks.com"
FLUID_EMAIL = "integrates@fluidattacks.com"
EMAIL_2 = "nonexisting@testdomain.com"
EMAIL_TEST = "johndoe@johndoe.com"
TEST_FILE = "test1.zip"
APK_TEST_FILE = "client_apk_v1.apk"
ROOT_ID = "63298a73-9dff-46cf-b42d-9b2f01a56690"
GROUP_NAME = "testgroup"
GROUP_HOOK_ID = "test-hook-id"
ORG_NAME = "orgtest"
COMMENTS_TEST = "test-comments"
DESCRIPTION_TEST = "This is a new group"
ORG_INTEGRATION_REPO_URL = "https://github.com/testing"
TEST_TKN = "test-token"
TEST_TKN_HEADER = "test-token-header"
TEST_TAGS = ["test-tag1", "test-tag2", "test-tag3", "unkind-tag"]


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=USER_EMAIL,
                    state=OrganizationAccessStateFaker(has_access=True, role="customer_manager"),
                ),
            ],
            stakeholders=[StakeholderFaker(email=USER_EMAIL, enrolled=True)],
        ),
    )
)
async def test_add_group() -> None:
    # Arrange
    loaders: Dataloaders = get_new_context()

    # Act
    await add_group(
        loaders=loaders,
        description=DESCRIPTION_TEST,
        email=USER_EMAIL,
        group_name=GROUP_NAME,
        has_essential=True,
        has_advanced=True,
        organization_name=ORG_NAME,
        service=GroupService.WHITE,
        subscription=GroupSubscriptionType.CONTINUOUS,
    )
    loaders.organization_groups.clear(ORG_ID)
    organization_groups = await loaders.organization_groups.load(ORG_ID)
    loaders.group_access.clear(GroupAccessRequest(email=USER_EMAIL, group_name=GROUP_NAME))
    access = await loaders.group_access.load(
        GroupAccessRequest(email=USER_EMAIL, group_name=GROUP_NAME)
    )

    # Assert
    assert len(organization_groups) == 1
    assert access
    assert not access.state.invitation
    assert access.state.has_access
    assert access.state.role == "customer_manager"
    assert access.expiration_time is None


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=USER_EMAIL,
                    state=OrganizationAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            groups=[GroupFaker(name="testgroup", organization_id=ORG_ID)],
            stakeholders=[StakeholderFaker(email=USER_EMAIL, enrolled=True)],
        ),
    )
)
async def test_add_group_fail_invalid_name() -> None:
    # Assert
    with raises(InvalidGroupName):
        await add_group(
            loaders=get_new_context(),
            description=DESCRIPTION_TEST,
            email=USER_EMAIL,
            group_name="testgroup",
            has_essential=True,
            has_advanced=True,
            organization_name="test-org",
            service=GroupService.WHITE,
            subscription=GroupSubscriptionType.CONTINUOUS,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            stakeholders=[StakeholderFaker(email="group_manager@fluidattacks.com", enrolled=True)],
        ),
    )
)
async def test_add_group_fail_stakeholder_not_in_organization() -> None:
    # Assert
    with raises(StakeholderNotInOrganization):
        await add_group(
            loaders=get_new_context(),
            description=DESCRIPTION_TEST,
            email="group_manager@fluidattacks.com",
            group_name="testgroup",
            has_essential=True,
            has_advanced=True,
            organization_name="test-org",
            service=GroupService.WHITE,
            subscription=GroupSubscriptionType.CONTINUOUS,
        )


@parametrize(
    args=["has_advanced", "has_essential", "service", "subscription"],
    cases=[
        [False, False, GroupService.WHITE, GroupSubscriptionType.ONESHOT],
        [False, False, GroupService.BLACK, GroupSubscriptionType.ONESHOT],
        [True, True, GroupService.WHITE, GroupSubscriptionType.ONESHOT],
        [True, True, GroupService.BLACK, GroupSubscriptionType.ONESHOT],
        [False, True, GroupService.WHITE, GroupSubscriptionType.ONESHOT],
        [False, True, GroupService.BLACK, GroupSubscriptionType.ONESHOT],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, created_by=EMAIL_TEST)],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="group_manager")],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=EMAIL_TEST,
                    state=OrganizationAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
        ),
    )
)
async def test_add_group_fail_services_config(
    has_advanced: bool,
    has_essential: bool,
    service: GroupService,
    subscription: GroupSubscriptionType,
) -> None:
    # Assert
    with raises(InvalidGroupServicesConfig):
        await add_group(
            loaders=get_new_context(),
            description=DESCRIPTION_TEST,
            email=EMAIL_TEST,
            group_name="testgroup",
            has_essential=has_essential,
            has_advanced=has_advanced,
            organization_name="test-org",
            service=service,
            subscription=subscription,
        )


@parametrize(
    args=["group_name", "description", "exception"],
    cases=[
        [
            "group_test",
            "This is a new group from pytest fail",
            InvalidField("group name"),
        ],
        [
            "group",
            "",
            InvalidFieldLength(),
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=USER_EMAIL,
                    state=OrganizationAccessStateFaker(
                        has_access=True, role="organization_manager"
                    ),
                ),
            ],
            stakeholders=[StakeholderFaker(email=USER_EMAIL, enrolled=True)],
        ),
    ),
)
async def test_add_group_fail_invalid_parameter(
    group_name: str,
    description: str,
    exception: CustomBaseException,
) -> None:
    # Assert
    with raises(Exception, match=str(exception)):
        await add_group(
            loaders=get_new_context(),
            description=description,
            email=USER_EMAIL,
            group_name=group_name,
            has_essential=True,
            has_advanced=True,
            organization_name="test-org",
            service=GroupService.WHITE,
            subscription=GroupSubscriptionType.CONTINUOUS,
        )


@parametrize(
    args=["group_name", "description"],
    cases=[
        ["a" * 1, "Test"],
        ["a" * 2, "Test"],
        ["a" * 3, "Test"],
        ["a" * 21, "Test"],
        ["a" * 15, "b" * 201],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=USER_EMAIL,
                    state=OrganizationAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            stakeholders=[StakeholderFaker(email=USER_EMAIL, enrolled=True)],
        ),
    )
)
async def test_add_group_fail_invalid_length(group_name: str, description: str) -> None:
    # Assert
    with raises(InvalidFieldLength):
        await add_group(
            loaders=get_new_context(),
            description=description,
            email=USER_EMAIL,
            group_name=group_name,
            has_essential=True,
            has_advanced=True,
            organization_name="test-org",
            service=GroupService.WHITE,
            subscription=GroupSubscriptionType.CONTINUOUS,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            stakeholders=[StakeholderFaker(email=USER_EMAIL, enrolled=True)],
        ),
    )
)
async def test_add_group_hook() -> None:
    # Arrange
    loaders: Dataloaders = get_new_context()

    # Act
    await add_group_hook(
        loaders=get_new_context(),
        group_name=GROUP_NAME,
        user_email=USER_EMAIL,
        hook=GroupHookPayload(
            entry_point="https://webhook.site/048c940b-b89e-467c-9cee-b6633b482e0f",
            name="test-site",
            token=TEST_TKN,
            token_header=TEST_TKN_HEADER,
            hook_events=set([HookEvent.VULNERABILITY_CREATED]),
        ),
    )
    group_hooks = await loaders.group_hook.load(GROUP_NAME)

    # Assert
    assert len(group_hooks) == 1
    assert group_hooks[0].entry_point == "https://webhook.site/048c940b-b89e-467c-9cee-b6633b482e0f"
    assert group_hooks[0].name == "test-site"
    assert group_hooks[0].token == TEST_TKN
    assert group_hooks[0].token_header == TEST_TKN_HEADER
    assert group_hooks[0].hook_events == set([HookEvent.VULNERABILITY_CREATED])


@parametrize(
    args=["hook", "exception"],
    cases=[
        [
            GroupHookPayload(
                entry_point="https://test.hook",
                name="test-site",
                token=TEST_TKN,
                token_header=TEST_TKN_HEADER,
                hook_events=set(),
            ),
            "Hook data is invalid",
        ],
        [
            GroupHookPayload(
                entry_point="https://webhook.site/048c940b-b89e-467c-9cee-b6633b482e0f",
                name="test-site",
                token=TEST_TKN,
                token_header=TEST_TKN_HEADER,
                hook_events=set([HookEvent.VULNERABILITY_CREATED]),
            ),
            "Entry point already exists",
        ],
        [
            GroupHookPayload(
                entry_point="http://url.com",
                name="Hook3",
                token=TEST_TKN,
                token_header=TEST_TKN_HEADER,
                hook_events=set([HookEvent.VULNERABILITY_CREATED]),
            ),
            "Exception - Protocol http is invalid",
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            group_hook=[
                GroupHookFaker(
                    group_name=GROUP_NAME,
                    entry_point="https://webhook.site/048c940b-b89e-467c-9cee-b6633b482e0f",
                    name="test-site",
                    token=TEST_TKN,
                    token_header=TEST_TKN_HEADER,
                )
            ],
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            stakeholders=[StakeholderFaker(email=USER_EMAIL, enrolled=True)],
        ),
    )
)
async def test_add_group_hook_fail(hook: GroupHookPayload, exception: str) -> None:
    # Assert
    with raises(Exception, match=exception):
        await add_group_hook(
            loaders=get_new_context(),
            group_name=GROUP_NAME,
            user_email=USER_EMAIL,
            hook=hook,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(
                    name=GROUP_NAME, organization_id=ORG_ID, state=GroupStateFaker(tags=set())
                )
            ],
            organizations=[OrganizationFaker(id=ORG_ID)],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=USER_EMAIL,
                    state=OrganizationAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            stakeholders=[StakeholderFaker(email=USER_EMAIL, enrolled=True)],
        ),
    ),
    others=[Mock(groups, "send_mail_portfolio_report", "async", None)],
)
async def test_add_tags() -> None:
    # Arrange
    loaders: Dataloaders = get_new_context()

    # Act
    group = await get_group(loaders, GROUP_NAME)
    await add_tags(
        loaders=get_new_context(),
        email=USER_EMAIL,
        group=group,
        tags_to_add=set(TEST_TAGS),
    )
    loaders.group.clear(GROUP_NAME)
    group_tagged = await loaders.group.load(GROUP_NAME)

    # Assert
    assert group_tagged
    assert group_tagged.state.tags == set(TEST_TAGS)


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            groups=[
                GroupFaker(organization_id=ORG_ID, name=GROUP_NAME),
                GroupFaker(organization_id=ORG_ID, name="test-gname1"),
            ],
        ),
    )
)
async def test_complete_register_for_group_invitation_stakeholder() -> None:
    # Arrange
    loaders = get_new_context()

    # Act
    await invite_to_group(
        loaders=get_new_context(),
        email=USER_EMAIL,
        responsibility="Test",
        role="customer_manager",
        group_name=GROUP_NAME,
        modified_by=FLUID_EMAIL,
    )

    if group_access := await loaders.group_access.load(
        GroupAccessRequest(email=USER_EMAIL, group_name=GROUP_NAME)
    ):
        await complete_register_for_group_invitation(loaders, group_access, USER_EMAIL)

    access = await loaders.group_access.load(
        GroupAccessRequest(email=USER_EMAIL, group_name=GROUP_NAME)
    )
    stakeholder = await loaders.stakeholder.load(USER_EMAIL)

    # Assert
    assert await is_valid(get_new_context(), GROUP_NAME)
    assert access
    assert access.state.invitation
    assert access.state.invitation.is_used
    assert access.state.has_access
    assert access.state.role == "customer_manager"
    assert access.expiration_time is None
    assert stakeholder
    assert stakeholder.is_registered
    assert stakeholder.enrolled
    assert await get_user_level_role(loaders, USER_EMAIL) == "hacker"
    assert await get_group_level_role(loaders, USER_EMAIL, GROUP_NAME) == "customer_manager"
    assert not await get_group_level_role(loaders, USER_EMAIL, "test-gname1")
    assert await get_organization_level_role(loaders, USER_EMAIL, ORG_ID) == "user"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            groups=[GroupFaker(organization_id=ORG_ID, name=GROUP_NAME)],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=EMAIL_2,
                    state=OrganizationAccessStateFaker(has_access=True, role="user"),
                )
            ],
            stakeholders=[StakeholderFaker(email=EMAIL_2, enrolled=True, is_registered=True)],
        ),
    )
)
async def test_complete_register_for_group_invitation() -> None:
    loaders = get_new_context()
    await invite_to_group(
        loaders=get_new_context(),
        email=EMAIL_2,
        responsibility="Test",
        role="user",
        group_name=GROUP_NAME,
        modified_by=FLUID_EMAIL,
    )
    if group_access := await loaders.group_access.load(
        GroupAccessRequest(email=EMAIL_2, group_name=GROUP_NAME)
    ):
        await complete_register_for_group_invitation(loaders, group_access, EMAIL_2)

    access = await loaders.group_access.load(
        GroupAccessRequest(email=EMAIL_2, group_name=GROUP_NAME)
    )
    assert access
    assert access.state.invitation
    assert access.state.invitation.is_used
    assert access.state.has_access
    assert access.state.role == "user"
    assert access.expiration_time is None
    assert await get_organization_level_role(loaders, EMAIL_2, ORG_ID) == "user"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            groups=[
                GroupFaker(organization_id=ORG_ID, name=GROUP_NAME),
                GroupFaker(organization_id=ORG_ID, name="test-gname1"),
            ],
            stakeholders=[StakeholderFaker(email=USER_EMAIL, role="admin")],
        ),
    )
)
async def test_complete_register_for_group_invitation_stakeholder_admin() -> None:
    # Arrange
    loaders = get_new_context()

    # Act
    await invite_to_group(
        loaders=get_new_context(),
        email=USER_EMAIL,
        responsibility="Test",
        role="customer_manager",
        group_name=GROUP_NAME,
        modified_by=FLUID_EMAIL,
    )

    if group_access := await loaders.group_access.load(
        GroupAccessRequest(email=USER_EMAIL, group_name=GROUP_NAME)
    ):
        await complete_register_for_group_invitation(loaders, group_access, USER_EMAIL)

    access = await loaders.group_access.load(
        GroupAccessRequest(email=USER_EMAIL, group_name=GROUP_NAME)
    )
    stakeholder = await loaders.stakeholder.load(USER_EMAIL)

    # Assert
    assert await is_valid(get_new_context(), GROUP_NAME)
    assert access
    assert access.state.invitation
    assert access.state.invitation.is_used
    assert access.state.has_access
    assert access.state.role == "customer_manager"
    assert access.expiration_time is None
    assert stakeholder
    assert stakeholder.is_registered
    assert stakeholder.enrolled
    assert await get_user_level_role(loaders, USER_EMAIL) == "admin"
    assert await get_group_level_role(loaders, USER_EMAIL, GROUP_NAME) == "customer_manager"
    assert await get_organization_level_role(loaders, USER_EMAIL, ORG_ID) == "user"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            group_hook=[GroupHookFaker(id=GROUP_HOOK_ID, group_name=GROUP_NAME)],
            organizations=[OrganizationFaker(id=ORG_ID)],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=USER_EMAIL,
                    state=OrganizationAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            stakeholders=[StakeholderFaker(email=USER_EMAIL, enrolled=True)],
        ),
    )
)
async def test_delete_group_hook() -> None:
    # Arrange
    loaders: Dataloaders = get_new_context()

    # Act
    await delete_group_hook(
        loaders=get_new_context(),
        group_name=GROUP_NAME,
        hook_id=GROUP_HOOK_ID,
    )
    group_hooks = await loaders.group_hook.load(GROUP_NAME)

    # Assert
    assert not group_hooks


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            organizations=[OrganizationFaker(id=ORG_ID)],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=USER_EMAIL,
                    state=OrganizationAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            stakeholders=[StakeholderFaker(email=USER_EMAIL, enrolled=True)],
        ),
    )
)
async def test_delete_group_hook_not_found() -> None:
    with raises(HookNotFound):
        await delete_group_hook(
            loaders=get_new_context(),
            group_name=GROUP_NAME,
            hook_id=GROUP_HOOK_ID,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            organization_integration_repositories=[
                OrganizationIntegrationRepositoryFaker(
                    id=random_uuid(), organization_id=ORG_ID, url=ORG_INTEGRATION_REPO_URL
                )
            ],
        ),
    )
)
async def test_fetch_and_remove_repositories() -> None:
    # Arrange
    loaders = get_new_context()

    # Act
    await fetch_and_remove_repositories(get_new_context(), ORG_ID, ORG_INTEGRATION_REPO_URL)
    current_repositories = await loaders.organization_unreliable_outside_repositories.load(
        (ORG_ID, None, None)
    )

    # Assert
    assert current_repositories == []


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME, created_by=EMAIL_TEST)],
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID, created_by=EMAIL_TEST)],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL_TEST,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(has_access=True, role="customer_manager"),
                )
            ],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=EMAIL_TEST,
                    state=OrganizationAccessStateFaker(has_access=True, role="customer_manager"),
                ),
            ],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="customer_manager")],
        ),
    )
)
async def test_get_groups_owned_by_user() -> None:
    # Act
    groups_owned = await get_groups_owned_by_user(loaders=get_new_context(), email=EMAIL_TEST)

    # Assert
    assert groups_owned == [GROUP_NAME]


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME, created_by=EMAIL_TEST)],
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            findings=[FindingFaker(id=FIN_ID, group_name=GROUP_NAME)],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=EMAIL_TEST,
                    state=OrganizationAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="group_manager")],
            vulnerabilities=[
                VulnerabilityFaker(
                    id=random_uuid(),
                    group_name=GROUP_NAME,
                    organization_name=ORG_NAME,
                    finding_id=FIN_ID,
                    treatment=Treatment(
                        modified_date=DATE_2024,
                        status=TreatmentStatus.UNTREATED,
                        assigned=USER_EMAIL,
                    ),
                ),
                VulnerabilityFaker(
                    id=random_uuid(),
                    group_name=GROUP_NAME,
                    organization_name=ORG_NAME,
                    finding_id=FIN_ID,
                    treatment=Treatment(
                        modified_date=DATE_2024,
                        status=TreatmentStatus.ACCEPTED,
                        assigned=USER_EMAIL,
                    ),
                ),
                VulnerabilityFaker(
                    id=random_uuid(),
                    group_name=GROUP_NAME,
                    organization_name=ORG_NAME,
                    finding_id=FIN_ID,
                    treatment=Treatment(
                        modified_date=DATE_2024,
                        status=TreatmentStatus.ACCEPTED,
                        assigned=USER_EMAIL,
                    ),
                ),
                VulnerabilityFaker(
                    id=random_uuid(),
                    group_name=GROUP_NAME,
                    organization_name=ORG_NAME,
                    finding_id=FIN_ID,
                    treatment=Treatment(
                        modified_date=DATE_2024,
                        status=TreatmentStatus.ACCEPTED_UNDEFINED,
                        assigned=USER_EMAIL,
                    ),
                ),
                VulnerabilityFaker(
                    id=random_uuid(),
                    group_name=GROUP_NAME,
                    organization_name=ORG_NAME,
                    finding_id=FIN_ID,
                    treatment=Treatment(
                        modified_date=DATE_2024,
                        status=TreatmentStatus.IN_PROGRESS,
                        assigned=USER_EMAIL,
                    ),
                ),
            ],
        ),
    )
)
async def test_get_treatment_summary() -> None:
    # Act
    treatment_summary = await get_treatment_summary(get_new_context(), GROUP_NAME)

    # Assert
    assert treatment_summary
    assert treatment_summary.accepted == 2
    assert treatment_summary.accepted_undefined == 1
    assert treatment_summary.in_progress == 1
    assert treatment_summary.untreated == 1


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME, created_by=EMAIL_TEST)],
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            findings=[FindingFaker(id=FIN_ID, group_name=GROUP_NAME)],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=EMAIL_TEST,
                    state=OrganizationAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="group_manager")],
            vulnerabilities=[
                VulnerabilityFaker(
                    id=random_uuid(),
                    group_name=GROUP_NAME,
                    organization_name=ORG_NAME,
                    finding_id=FIN_ID,
                    severity_score=SeverityScore(
                        base_score=Decimal("4.0"),
                        temporal_score=Decimal("5.2"),
                        cvss_v3="CVSS:3.1/AV:L/AC:H/PR:L/UI:N/S:C/C:L/I:L/A:L"
                        "/E:P/RL:O/CR:L/AR:H/MAV:N/MAC:H/MPR:H/MUI:R/MS:U/MC:L"
                        "/MA:L",
                        cvss_v4="CVSS:4.0/AV:L/AC:H/AT:N/PR:L/UI:N/VC:L/VI:L/"
                        "VA:L/SC:L/SI:L/SA:L/MAV:N/MAC:H/MPR:H/MUI:A/MVC:L/"
                        "MVA:L/MSC:N/MSI:N/MSA:N/CR:L/AR:H/E:P",
                        cvssf_v4=Decimal("0.014"),
                        threat_score=Decimal("6.5"),
                        cvssf=Decimal("5.02"),
                    ),
                ),
                VulnerabilityFaker(
                    id=random_uuid(),
                    group_name=GROUP_NAME,
                    organization_name=ORG_NAME,
                    finding_id=FIN_ID,
                    severity_score=SeverityScore(
                        base_score=Decimal("6.0"),
                        temporal_score=Decimal("5.0"),
                        cvss_v3="CVSS:3.1/AV:L/AC:H/PR:L/UI:N/S:C/C:L/I:L/A:L"
                        "/E:P/RL:O/CR:L/AR:H/MAV:N/MAC:H/MPR:H/MUI:R/MS:U/MC:L"
                        "/MA:L",
                        cvss_v4="CVSS:4.0/AV:L/AC:H/AT:N/PR:L/UI:N/VC:L/VI:L/"
                        "VA:L/SC:L/SI:L/SA:L/MAV:N/MAC:H/MPR:H/MUI:A/MVC:L/"
                        "MVA:L/MSC:N/MSI:N/MSA:N/CR:L/AR:H/E:P",
                        cvssf_v4=Decimal("0.4"),
                        threat_score=Decimal("8.5"),
                        cvssf=Decimal("7.25"),
                    ),
                ),
            ],
        ),
    )
)
async def test_get_max_severity() -> None:
    # Act
    loaders: Dataloaders = get_new_context()
    max_severity = await get_max_severity(
        loaders=loaders,
        group_name=GROUP_NAME,
    )

    # Assert
    assert max_severity == Decimal("8.5")


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME, created_by=EMAIL_TEST)],
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            findings=[FindingFaker(id=FIN_ID, group_name=GROUP_NAME)],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=EMAIL_TEST,
                    state=OrganizationAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="group_manager")],
            vulnerabilities=[
                VulnerabilityFaker(
                    id=random_uuid(),
                    group_name=GROUP_NAME,
                    organization_name=ORG_NAME,
                    finding_id=FIN_ID,
                    severity_score=SeverityScore(
                        base_score=Decimal("4.0"),
                        temporal_score=Decimal("5.2"),
                        cvss_v3="CVSS:3.1/AV:L/AC:H/PR:L/UI:N/S:C/C:L/I:L/A:L"
                        "/E:P/RL:O/CR:L/AR:H/MAV:N/MAC:H/MPR:H/MUI:R/MS:U/MC:L"
                        "/MA:L",
                        cvss_v4="CVSS:4.0/AV:L/AC:H/AT:N/PR:L/UI:N/VC:L/VI:L/"
                        "VA:L/SC:L/SI:L/SA:L/MAV:N/MAC:H/MPR:H/MUI:A/MVC:L/"
                        "MVA:L/MSC:N/MSI:N/MSA:N/CR:L/AR:H/E:P",
                        cvssf_v4=Decimal("0.014"),
                        threat_score=Decimal("6.5"),
                        cvssf=Decimal("5.02"),
                    ),
                ),
                VulnerabilityFaker(
                    id=random_uuid(),
                    group_name=GROUP_NAME,
                    organization_name=ORG_NAME,
                    finding_id=FIN_ID,
                    severity_score=SeverityScore(
                        base_score=Decimal("6.0"),
                        temporal_score=Decimal("5.0"),
                        cvss_v3="CVSS:3.1/AV:L/AC:H/PR:L/UI:N/S:C/C:L/I:L/A:L"
                        "/E:P/RL:O/CR:L/AR:H/MAV:N/MAC:H/MPR:H/MUI:R/MS:U/MC:L"
                        "/MA:L",
                        cvss_v4="CVSS:4.0/AV:L/AC:H/AT:N/PR:L/UI:N/VC:L/VI:L/"
                        "VA:L/SC:L/SI:L/SA:L/MAV:N/MAC:H/MPR:H/MUI:A/MVC:L/"
                        "MVA:L/MSC:N/MSI:N/MSA:N/CR:L/AR:H/E:P",
                        cvssf_v4=Decimal("2.4"),
                        threat_score=Decimal("8.5"),
                        cvssf=Decimal("7.25"),
                    ),
                ),
            ],
        ),
    ),
)
@freeze_time("2024-12-31T05:00:00+00:00")
async def test_get_mean_remediate_severity() -> None:
    # Act
    loaders: Dataloaders = get_new_context()
    mean_severity = await get_mean_remediate_severity(
        loaders=loaders,
        group_name=GROUP_NAME,
        min_severity=Decimal("5.0"),
        max_severity=Decimal("9.0"),
    )
    mean_severity_cvssf = await get_mean_remediate_severity_cvssf(
        loaders=loaders,
        group_name=GROUP_NAME,
        min_severity=Decimal("5.0"),
        max_severity=Decimal("9.0"),
    )
    mean_severity_non_treated = await get_mean_remediate_non_treated_severity(
        loaders=loaders,
        group_name=GROUP_NAME,
        min_severity=Decimal("5.0"),
        max_severity=Decimal("9.0"),
    )
    mean_severity_non_treated_cvssf = await get_mean_remediate_non_treated_severity_cvssf(
        loaders=loaders,
        group_name=GROUP_NAME,
        min_severity=Decimal("5.0"),
        max_severity=Decimal("9.0"),
    )

    # Assert
    assert mean_severity == Decimal("2092")
    assert mean_severity_cvssf == mean_severity
    assert mean_severity_non_treated == mean_severity_cvssf
    assert mean_severity_non_treated_cvssf == mean_severity_non_treated


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME, created_by=EMAIL_TEST)],
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            findings=[
                FindingFaker(
                    id=FIN_ID,
                    group_name=GROUP_NAME,
                    unreliable_indicators=FindingUnreliableIndicatorsFaker(
                        vulnerabilities_summary=FindingVulnerabilitiesSummary(open=1)
                    ),
                )
            ],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=EMAIL_TEST,
                    state=OrganizationAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="group_manager")],
            vulnerabilities=[
                VulnerabilityFaker(
                    id=random_uuid(),
                    group_name=GROUP_NAME,
                    organization_name=ORG_NAME,
                    finding_id=FIN_ID,
                ),
                VulnerabilityFaker(
                    id=random_uuid(),
                    group_name=GROUP_NAME,
                    organization_name=ORG_NAME,
                    finding_id=FIN_ID,
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.SAFE),
                ),
            ],
        ),
    )
)
async def test_get_open_findings() -> None:
    # Act
    open_findings = await get_open_findings(get_new_context(), GROUP_NAME)

    # Assert
    assert open_findings == 1


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME, created_by=EMAIL_TEST)],
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            findings=[
                FindingFaker(
                    id=FIN_ID,
                    group_name=GROUP_NAME,
                    unreliable_indicators=FindingUnreliableIndicatorsFaker(
                        vulnerabilities_summary=FindingVulnerabilitiesSummary(open=2)
                    ),
                )
            ],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=EMAIL_TEST,
                    state=OrganizationAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="group_manager")],
            vulnerabilities=[
                VulnerabilityFaker(
                    id=random_uuid(),
                    group_name=GROUP_NAME,
                    organization_name=ORG_NAME,
                    finding_id=FIN_ID,
                ),
                VulnerabilityFaker(
                    id=random_uuid(),
                    group_name=GROUP_NAME,
                    organization_name=ORG_NAME,
                    finding_id=FIN_ID,
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.SAFE),
                ),
                VulnerabilityFaker(
                    id=random_uuid(),
                    group_name=GROUP_NAME,
                    organization_name=ORG_NAME,
                    finding_id=FIN_ID,
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.SAFE),
                ),
                VulnerabilityFaker(
                    id=random_uuid(),
                    group_name=GROUP_NAME,
                    organization_name=ORG_NAME,
                    finding_id=FIN_ID,
                ),
                VulnerabilityFaker(
                    id=random_uuid(),
                    group_name=GROUP_NAME,
                    organization_name=ORG_NAME,
                    finding_id=FIN_ID,
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.SAFE),
                ),
            ],
        ),
    )
)
async def test_get_vulnerabilities() -> None:
    # Act
    closed_vulns = await get_closed_vulnerabilities(get_new_context(), GROUP_NAME)
    open_vulns = await get_open_vulnerabilities(get_new_context(), GROUP_NAME)

    # Assert
    assert closed_vulns == 3
    assert open_vulns == 2


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME, created_by=EMAIL_TEST)],
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            findings=[FindingFaker(id=FIN_ID, group_name=GROUP_NAME)],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=EMAIL_TEST,
                    state=OrganizationAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="group_manager")],
            vulnerabilities=[
                VulnerabilityFaker(
                    id=random_uuid(),
                    group_name=GROUP_NAME,
                    organization_name=ORG_NAME,
                    finding_id=FIN_ID,
                    verification=VulnerabilityVerification(
                        modified_date=DATE_2024, status=VulnerabilityVerificationStatus.REQUESTED
                    ),
                ),
                VulnerabilityFaker(
                    id=random_uuid(),
                    group_name=GROUP_NAME,
                    organization_name=ORG_NAME,
                    finding_id=FIN_ID,
                    verification=VulnerabilityVerification(
                        modified_date=DATE_2024, status=VulnerabilityVerificationStatus.REQUESTED
                    ),
                ),
                VulnerabilityFaker(
                    id=random_uuid(),
                    group_name=GROUP_NAME,
                    organization_name=ORG_NAME,
                    finding_id=FIN_ID,
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.SAFE),
                ),
                VulnerabilityFaker(
                    id=random_uuid(),
                    group_name=GROUP_NAME,
                    organization_name=ORG_NAME,
                    finding_id=FIN_ID,
                ),
                VulnerabilityFaker(
                    id=random_uuid(),
                    group_name=GROUP_NAME,
                    organization_name=ORG_NAME,
                    finding_id=FIN_ID,
                    verification=VulnerabilityVerification(
                        modified_date=DATE_2024, status=VulnerabilityVerificationStatus.REQUESTED
                    ),
                ),
            ],
        ),
    )
)
async def test_get_vulnerabilities_with_pending_attacks() -> None:
    # Act
    loaders: Dataloaders = get_new_context()
    pending = await get_vulnerabilities_with_pending_attacks(
        loaders=loaders,
        group_name=GROUP_NAME,
    )

    # Assert
    assert pending == 3


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            findings=[
                FindingFaker(
                    id="FIN#40rf1234-s0lq-6j32-a1p0-nt9o2sa1ln7c",
                    group_name=GROUP_NAME,
                    unreliable_indicators=FindingUnreliableIndicatorsFaker(
                        total_open_priority=Decimal(100)
                    ),
                ),
                FindingFaker(
                    id="FIN#9a7b1234-x0y9-7z32-b2c0-uv9p3sa2mn8d",
                    group_name=GROUP_NAME,
                    unreliable_indicators=FindingUnreliableIndicatorsFaker(
                        total_open_priority=Decimal(800)
                    ),
                ),
                FindingFaker(
                    id="FIN#8t5e332-t0r4-61a2-p4bc-vtb94alqlv8t",
                    group_name=GROUP_NAME,
                    unreliable_indicators=FindingUnreliableIndicatorsFaker(
                        total_open_priority=Decimal(-1000)
                    ),
                ),
            ],
            groups=[GroupFaker(organization_id=ORG_ID, name=GROUP_NAME)],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=EMAIL_TEST,
                    state=OrganizationAccessStateFaker(has_access=True, role="user"),
                )
            ],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, enrolled=True, is_registered=True)],
        ),
    )
)
async def test_get_total_priority_findings() -> None:
    # Act
    result = await get_total_priority_findings(get_new_context(), GROUP_NAME)

    # Assert
    assert result == Decimal(900)


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name="trialorg", created_by=EMAIL_TEST)],
            groups=[
                GroupFaker(
                    name="trialgroup1",
                    organization_id=ORG_ID,
                    created_by=EMAIL_TEST,
                    state=GroupStateFaker(
                        managed=GroupManaged.TRIAL,
                        has_advanced=False,
                        modified_by=EMAIL_TEST,
                        service=GroupService.WHITE,
                        tier=GroupTier.FREE,
                    ),
                ),
            ],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="group_manager")],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=EMAIL_TEST,
                    state=OrganizationAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            trials=[TrialFaker(email=EMAIL_TEST)],
        ),
    )
)
async def test_has_only_one_group_during_trial() -> None:
    # Assert
    with raises(TrialRestriction):
        await add_group(
            loaders=get_new_context(),
            description="This is a new trial group",
            email=EMAIL_TEST,
            group_name="trialgroup2",
            has_essential=True,
            has_advanced=True,
            organization_name="trialorg",
            service=GroupService.WHITE,
            subscription=GroupSubscriptionType.CONTINUOUS,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=EMAIL_TEST,
                    state=OrganizationAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, enrolled=True)],
        ),
    )
)
async def test_reject_register_for_group_invitation() -> None:
    # Arrange
    loaders: Dataloaders = get_new_context()
    await invite_to_group(
        loaders=get_new_context(),
        email=EMAIL_TEST,
        responsibility="Test",
        role="group_manager",
        group_name=GROUP_NAME,
        modified_by="integrates@fluidattacks.com",
    )

    # Act
    if group_access := await loaders.group_access.load(
        GroupAccessRequest(email=EMAIL_TEST, group_name=GROUP_NAME)
    ):
        await reject_register_for_group_invitation(
            loaders=get_new_context(),
            group_access=GroupAccess(
                email=EMAIL_TEST, group_name=GROUP_NAME, state=group_access.state
            ),
        )
    loaders.group_access.clear_all()
    access = await loaders.group_access.load(
        GroupAccessRequest(email=EMAIL_TEST, group_name=GROUP_NAME)
    )

    # Assert
    assert not access


@parametrize(args=["user_email"], cases=[[USER_EMAIL], [EMAIL_TEST]])
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            stakeholders=[
                StakeholderFaker(email=EMAIL_TEST, enrolled=True),
                StakeholderFaker(email=USER_EMAIL, enrolled=True),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=EMAIL_TEST,
                    state=OrganizationAccessStateFaker(has_access=True, role="group_manager"),
                ),
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=USER_EMAIL,
                    state=OrganizationAccessStateFaker(has_access=True, role="user"),
                ),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id=ORG_ID),
                GroupFaker(name="another_group", organization_id=ORG_ID),
            ],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL_TEST,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="user"),
                ),
                GroupAccessFaker(
                    email=EMAIL_TEST,
                    group_name="another_group",
                    state=GroupAccessStateFaker(role="user"),
                ),
                GroupAccessFaker(
                    email=USER_EMAIL,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="user"),
                ),
            ],
        ),
    )
)
async def test_remove_stakeholder_from_group(user_email: str) -> None:
    loaders: Dataloaders = get_new_context()
    user_groups = await get_groups_by_stakeholder(loaders, user_email, organization_id=ORG_ID)
    assert len(user_groups) > 0

    await unsubscribe_from_group(
        loaders=get_new_context(),
        email=user_email,
        group_name=GROUP_NAME,
    )

    new_loaders: Dataloaders = get_new_context()
    group_access = await new_loaders.group_access.load(
        GroupAccessRequest(email=user_email, group_name=GROUP_NAME)
    )
    assert not group_access
    user_groups_after = await get_groups_by_stakeholder(
        new_loaders, user_email, organization_id=ORG_ID
    )
    assert len(user_groups_after) == len(user_groups) - 1

    stakeholder = await new_loaders.stakeholder.load(user_email)
    if len(user_groups_after) > 0:
        assert stakeholder
    else:
        assert not stakeholder


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            groups=[
                GroupFaker(
                    name=GROUP_NAME,
                    organization_id=ORG_ID,
                    state=GroupStateFaker(pending_deletion_date=DATE_2025),
                )
            ],
            group_access=[GroupAccessFaker(email=EMAIL_TEST, group_name=GROUP_NAME)],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=EMAIL_TEST,
                    state=OrganizationAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, enrolled=True)],
        ),
    )
)
async def test_remove_pending_deletion_date() -> None:
    # Arrange
    loaders: Dataloaders = get_new_context()
    group = GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)

    # Act
    await remove_pending_deletion_date(group=group, modified_by=EMAIL_TEST)
    group_updated = await loaders.group.load(GROUP_NAME)

    # Assert
    assert group_updated
    assert group_updated.state.modified_by == EMAIL_TEST
    assert not group_updated.state.pending_deletion_date


@freeze_time("2024-12-31T12:59:59+00:00")
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            group_access=[GroupAccessFaker(email=EMAIL_TEST, group_name=GROUP_NAME)],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=EMAIL_TEST,
                    state=OrganizationAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, enrolled=True)],
        ),
    )
)
async def test_set_pending_deletion_date() -> None:
    # Arrange
    loaders: Dataloaders = get_new_context()
    group = GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)

    # Act
    await set_pending_deletion_date(
        group=group,
        modified_by=EMAIL_TEST,
        pending_deletion_date=DATE_2025,
    )
    group_updated = await loaders.group.load(GROUP_NAME)

    # Assert
    assert group_updated
    assert group_updated.state.pending_deletion_date == DATE_2025
    assert group_updated.state.modified_by == EMAIL_TEST
    assert group_updated.state.modified_date == datetime.fromisoformat("2024-12-31T12:59:59+00:00")


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=EMAIL_TEST,
                    state=OrganizationAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, enrolled=True)],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL_TEST,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(has_access=True),
                ),
            ],
        ),
    ),
    others=[Mock(groups, "send_mail_portfolio_report", "async", None)],
)
async def test_remove_tag() -> None:
    # Arrange
    loaders = get_new_context()

    # Act
    group = await get_group(loaders, GROUP_NAME)
    await remove_tag(
        loaders=get_new_context(),
        email=EMAIL_TEST,
        group=group,
        tag_to_remove="test-tag",
    )
    loaders.group.clear(GROUP_NAME)
    group_untagged = await loaders.group.load(GROUP_NAME)

    # Assert
    assert group_untagged
    assert not group_untagged.state.tags


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            groups=[
                GroupFaker(
                    name=GROUP_NAME, organization_id=ORG_ID, state=GroupStateFaker(tags=set())
                )
            ],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=EMAIL_TEST,
                    state=OrganizationAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, enrolled=True)],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL_TEST,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(has_access=True),
                ),
            ],
        ),
    )
)
async def test_remove_tag_empty() -> None:
    loaders = get_new_context()

    group = await get_group(loaders, GROUP_NAME)
    await remove_tag(
        loaders=get_new_context(),
        email=EMAIL_TEST,
        group=group,
        tag_to_remove="test-tag",
    )
    loaders.group.clear(GROUP_NAME)
    group = await get_group(loaders, GROUP_NAME)

    assert group
    assert not group.state.tags


@parametrize(
    args=["group_name", "exception"],
    cases=[
        [GROUP_NAME, "Exception - Invalid subscription. Provided subscription is already active"],
        ["groupfake", "Access denied or group not found"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=EMAIL_TEST,
                    state=OrganizationAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, enrolled=True, role="group_manager")],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL_TEST,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
        ),
    )
)
async def test_request_upgrade_fail(group_name: str, exception: str) -> None:
    # Assert
    with raises(Exception, match=exception):
        await request_upgrade(
            loaders=get_new_context(),
            email=EMAIL_TEST,
            group_names=[group_name],
        )


@parametrize(
    args=["has_advanced", "has_essential", "service", "subscription"],
    cases=[
        # No services
        [False, False, GroupService.WHITE, GroupSubscriptionType.CONTINUOUS],
        [False, False, GroupService.BLACK, GroupSubscriptionType.CONTINUOUS],
        # Both services
        [True, True, GroupService.WHITE, GroupSubscriptionType.CONTINUOUS],
        [True, True, GroupService.BLACK, GroupSubscriptionType.CONTINUOUS],
        # Machine only except white+continuous
        [False, True, GroupService.BLACK, GroupSubscriptionType.CONTINUOUS],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name="trialorg", created_by=EMAIL_TEST)],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="group_manager")],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=EMAIL_TEST,
                    state=OrganizationAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            trials=[TrialFaker(email=EMAIL_TEST)],
        ),
    )
)
async def test_restrict_services_during_trial(
    has_advanced: bool,
    has_essential: bool,
    service: GroupService,
    subscription: GroupSubscriptionType,
) -> None:
    # Assert
    with raises(TrialRestriction):
        await add_group(
            loaders=get_new_context(),
            description=DESCRIPTION_TEST,
            email=EMAIL_TEST,
            group_name="trialgroup2",
            has_essential=has_essential,
            has_advanced=has_advanced,
            organization_name="trialorg",
            service=service,
            subscription=subscription,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            group_hook=[
                GroupHookFaker(
                    id=HOOK_ID,
                    group_name=GROUP_NAME,
                    entry_point="https://webhook.site/test",
                    name="test-site",
                    token=TEST_TKN,
                    token_header=TEST_TKN_HEADER,
                )
            ],
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            stakeholders=[StakeholderFaker(email=USER_EMAIL, enrolled=True)],
        ),
    )
)
async def test_update_group_hook() -> None:
    # Act
    loaders = get_new_context()
    await update_group_hook(
        loaders=get_new_context(),
        group_name=GROUP_NAME,
        hook_id=HOOK_ID,
        user_email=USER_EMAIL,
        hook=GroupHookPayload(
            entry_point="https://webhook.site/048c940b-b89e-467c-9cee-b6633b482e0f",
            name="test-new-hook-name",
            token=TEST_TKN,
            token_header=TEST_TKN_HEADER,
            hook_events=set([HookEvent.EVENT_CREATED]),
        ),
    )
    hook_updated = await loaders.hook.load(GroupHookRequest(group_name=GROUP_NAME, id=HOOK_ID))

    # Assert
    assert len(hook_updated) > 0
    assert hook_updated[0].name == "test-new-hook-name"
    assert (
        hook_updated[0].entry_point == "https://webhook.site/048c940b-b89e-467c-9cee-b6633b482e0f"
    )
    assert hook_updated[0].hook_events == set([HookEvent.EVENT_CREATED])


@parametrize(
    args=["hook", "exception", "hook_id"],
    cases=[
        [
            "https://webhook.site/048c940b-b89e-467c-9cee-b6633b482e0f",
            "Access denied or hook not found",
            "hook-id-test-error",
        ],
        [
            "http://url.com",
            "Exception - Protocol http is invalid",
            HOOK_ID,
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            group_hook=[
                GroupHookFaker(
                    id=HOOK_ID,
                    group_name=GROUP_NAME,
                    entry_point="https://webhook.site/test",
                    name="test-site",
                    token=TEST_TKN,
                    token_header=TEST_TKN_HEADER,
                )
            ],
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            stakeholders=[StakeholderFaker(email=USER_EMAIL, enrolled=True)],
        ),
    )
)
async def test_update_group_hook_fail(hook: str, exception: str, hook_id: str) -> None:
    # Act
    with raises(Exception, match=exception):
        await update_group_hook(
            loaders=get_new_context(),
            group_name=GROUP_NAME,
            hook_id=hook_id,
            user_email=USER_EMAIL,
            hook=GroupHookPayload(
                entry_point=hook,
                name="test-new-hook-name",
                token=TEST_TKN,
                token_header=TEST_TKN_HEADER,
                hook_events=set([HookEvent.EVENT_CREATED]),
            ),
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            organizations=[OrganizationFaker(id=ORG_ID, name="orgtest")],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=USER_EMAIL,
                    state=OrganizationAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            stakeholders=[StakeholderFaker(email=USER_EMAIL, enrolled=True)],
        ),
    )
)
async def test_update_group_indicators() -> None:
    # Act
    loaders: Dataloaders = get_new_context()

    await update_indicators(
        group_name=GROUP_NAME,
        indicators=GroupUnreliableIndicators(
            last_closed_vulnerability_days=20,
            max_open_severity=Decimal("8.6"),
            mean_remediate=Decimal("2.5"),
            nofluid_quantity=0,
        ),
    )
    group_indicators = await loaders.group_unreliable_indicators.load(GROUP_NAME)

    # Assert
    assert group_indicators
    assert group_indicators.last_closed_vulnerability_days == 20
    assert group_indicators.max_open_severity == Decimal("8.6")
    assert group_indicators.mean_remediate == Decimal("2.5")
    assert group_indicators.nofluid_quantity == 0


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            organizations=[OrganizationFaker(id=ORG_ID, name="orgtest")],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=USER_EMAIL,
                    state=OrganizationAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            stakeholders=[StakeholderFaker(email=USER_EMAIL, enrolled=True)],
        ),
    )
)
async def test_update_group_info() -> None:
    # Act
    loaders: Dataloaders = get_new_context()

    metadata = assign_metadata(
        business_id="business-id",
        business_name="total_new_business_name",
        description=DESCRIPTION_TEST,
        language="ES",
        sprint_start_date=None,
        sprint_duration=7,
        tzn=None,
    )
    await update_group_info(
        loaders=get_new_context(), group_name=GROUP_NAME, metadata=metadata, email=USER_EMAIL
    )
    group = await loaders.group.load(GROUP_NAME)

    # Assert
    assert group
    assert group.business_id == "business-id"
    assert group.business_name == "total_new_business_name"
    assert group.description == DESCRIPTION_TEST
    assert group.language == GroupLanguage.ES
    assert group.sprint_duration == 7


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(
                    name=GROUP_NAME,
                    organization_id=ORG_ID,
                    state=GroupStateFaker(managed=GroupManaged.TRIAL),
                ),
            ],
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=USER_EMAIL,
                    state=OrganizationAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            stakeholders=[StakeholderFaker(email=USER_EMAIL, enrolled=True)],
        ),
    )
)
async def test_update_group_managed() -> None:
    # Act
    loaders: Dataloaders = get_new_context()

    await update_group_managed(
        loaders=get_new_context(),
        comments=COMMENTS_TEST,
        email=USER_EMAIL,
        group_name=GROUP_NAME,
        managed=GroupManaged.UNDER_REVIEW,
        justification=GroupStateJustification.MIGRATION,
    )
    group = await loaders.group.load(GROUP_NAME)

    # Assert
    assert group
    assert group.name == GROUP_NAME
    assert group.state.managed == GroupManaged.UNDER_REVIEW


@parametrize(args=["managed"], cases=[[GroupManaged.NOT_MANAGED], [GroupManaged.TRIAL]])
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=USER_EMAIL,
                    state=OrganizationAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            stakeholders=[StakeholderFaker(email=USER_EMAIL, enrolled=True)],
        ),
    )
)
async def test_update_group_managed_fail(managed: GroupManaged) -> None:
    # Assert
    with raises(InvalidManagedChange):
        await update_group_managed(
            loaders=get_new_context(),
            comments=COMMENTS_TEST,
            email=USER_EMAIL,
            group_name=GROUP_NAME,
            managed=managed,
            justification=GroupStateJustification.MIGRATION,
        )


@parametrize(args=["tier"], cases=[[GroupTier.ESSENTIAL], [GroupTier.FREE], [GroupTier.ADVANCED]])
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            organizations=[OrganizationFaker(id=ORG_ID, name="orgtest")],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=USER_EMAIL,
                    state=OrganizationAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            stakeholders=[StakeholderFaker(email=USER_EMAIL, enrolled=True)],
        ),
    )
)
async def test_update_group_tier(tier: GroupTier) -> None:
    # Act
    loaders: Dataloaders = get_new_context()

    await update_group_tier(
        loaders=get_new_context(),
        comments=COMMENTS_TEST,
        email=USER_EMAIL,
        group_name=GROUP_NAME,
        tier=tier,
    )
    group = await loaders.group.load(GROUP_NAME)

    # Assert
    assert group
    assert group.state.comments == COMMENTS_TEST
    assert group.state.tier == tier


@parametrize(
    args=["tier", "exception"],
    cases=[
        [GroupTier.ONESHOT, "Exception - OneShot service is no longer provided"],
        [
            GroupTier.OTHER,
            "Exception - Invalid tier. Only 'oneshot', 'essential', 'advanced' and 'free' allowed.",
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            organizations=[OrganizationFaker(id=ORG_ID, name="orgtest")],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=USER_EMAIL,
                    state=OrganizationAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            stakeholders=[StakeholderFaker(email=USER_EMAIL, enrolled=True)],
        ),
    )
)
async def test_update_group_tier_fail(tier: GroupTier, exception: str) -> None:
    # Act
    with raises(Exception, match=exception):
        await update_group_tier(
            loaders=get_new_context(),
            comments=COMMENTS_TEST,
            email=USER_EMAIL,
            group_name=GROUP_NAME,
            tier=tier,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(
                    name=GROUP_NAME,
                    organization_id=ORG_ID,
                    state=GroupStateFaker(tier=GroupTier.ESSENTIAL, service=GroupService.BLACK),
                )
            ],
            findings=[FindingFaker(id=FIN_ID, group_name=GROUP_NAME)],
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=USER_EMAIL,
                    state=OrganizationAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            stakeholders=[StakeholderFaker(email=USER_EMAIL, enrolled=True)],
            roots=[
                GitRootFaker(
                    created_by=USER_EMAIL,
                    group_name=GROUP_NAME,
                    organization_name=ORG_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(credential_id="cred-id", nickname="git_1"),
                )
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    id=random_uuid(),
                    group_name=GROUP_NAME,
                    organization_name=ORG_NAME,
                    finding_id=FIN_ID,
                    root_id=ROOT_ID,
                    treatment=Treatment(
                        modified_date=DATE_2024,
                        status=TreatmentStatus.UNTREATED,
                        assigned=USER_EMAIL,
                    ),
                ),
                VulnerabilityFaker(
                    id=random_uuid(),
                    group_name=GROUP_NAME,
                    organization_name=ORG_NAME,
                    root_id=ROOT_ID,
                    finding_id=FIN_ID,
                    treatment=Treatment(
                        modified_date=DATE_2024,
                        status=TreatmentStatus.ACCEPTED,
                        assigned=USER_EMAIL,
                    ),
                ),
            ],
        ),
    )
)
async def test_update_group() -> None:
    # Act
    loaders: Dataloaders = get_new_context()

    await update_group(
        loaders=get_new_context(),
        comments=COMMENTS_TEST,
        email=USER_EMAIL,
        justification=GroupStateJustification.NONE,
        group_name=GROUP_NAME,
        has_arm=True,
        has_essential=True,
        has_advanced=True,
        service=GroupService.WHITE,
        subscription=GroupSubscriptionType.CONTINUOUS,
        tier=GroupTier.ADVANCED,
    )
    group = await loaders.group.load(GROUP_NAME)

    # Assert
    assert group
    assert group.state.comments == COMMENTS_TEST
    assert group.state.has_essential
    assert group.state.has_advanced
    assert group.state.service == GroupService.WHITE
    assert group.state.type == GroupSubscriptionType.CONTINUOUS
    assert group.state.tier == GroupTier.ADVANCED


@parametrize(
    args=[
        "group_name",
        "service",
        "subscription",
        "has_essential",
        "has_advanced",
        "has_arm",
        "tier",
        "error_message",
    ],
    cases=[
        [
            "group1",
            GroupService.WHITE,
            GroupSubscriptionType.CONTINUOUS,
            False,
            True,
            False,
            GroupTier.ADVANCED,
            "Exception - Advanced is only available when ASM is too",
        ],
        [
            "group1",
            GroupService.WHITE,
            GroupSubscriptionType.CONTINUOUS,
            False,
            True,
            True,
            GroupTier.ADVANCED,
            "Exception - Advanced is only available when Machine is too",
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=USER_EMAIL,
                    state=OrganizationAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            stakeholders=[StakeholderFaker(email=USER_EMAIL, enrolled=True)],
        ),
    )
)
async def test_update_group_plan_fail(
    group_name: str,
    service: GroupService,
    subscription: GroupSubscriptionType,
    has_essential: bool,
    has_advanced: bool,
    has_arm: bool,
    tier: GroupTier,
    error_message: str,
) -> None:
    # Assert
    with raises(Exception, match=error_message):
        await update_group(
            loaders=get_new_context(),
            comments="",
            email=USER_EMAIL,
            justification=GroupStateJustification.NONE,
            group_name=group_name,
            has_arm=has_arm,
            has_essential=has_essential,
            has_advanced=has_advanced,
            service=service,
            subscription=subscription,
            tier=tier,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name="trialorg", created_by=EMAIL_TEST)],
            groups=[
                GroupFaker(
                    name="trialgroup",
                    organization_id=ORG_ID,
                    created_by=EMAIL_TEST,
                    state=GroupStateFaker(
                        has_advanced=False,
                        managed=GroupManaged.TRIAL,
                        modified_by=EMAIL_TEST,
                        service=GroupService.WHITE,
                        tier=GroupTier.FREE,
                    ),
                ),
            ],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="group_manager")],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=EMAIL_TEST,
                    state=OrganizationAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            trials=[TrialFaker(email=EMAIL_TEST)],
        ),
    )
)
async def test_update_group_trial_restriction() -> None:
    # Assert
    with raises(TrialRestriction):
        await update_group(
            loaders=get_new_context(),
            comments="",
            email=EMAIL_TEST,
            justification=GroupStateJustification.NONE,
            group_name="trialgroup",
            has_essential=True,
            has_advanced=True,
            has_arm=True,
            service=GroupService.WHITE,
            subscription=GroupSubscriptionType.CONTINUOUS,
            tier=GroupTier.ESSENTIAL,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            groups=[GroupFaker(name="testgroup", organization_id=ORG_ID)],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=USER_EMAIL,
                    state=OrganizationAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            stakeholders=[StakeholderFaker(email=USER_EMAIL, enrolled=True)],
        ),
    )
)
async def test_update_group_invalid_service() -> None:
    # Assert
    with raises(Exception, match="Exception - OneShot service is no longer provided"):
        await update_group(
            loaders=get_new_context(),
            comments="",
            email=USER_EMAIL,
            justification=GroupStateJustification.NONE,
            group_name="testgroup",
            has_arm=False,
            has_essential=False,
            has_advanced=False,
            service=GroupService.WHITE,
            subscription=GroupSubscriptionType.ONESHOT,
            tier=GroupTier.ONESHOT,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME, created_by=EMAIL_TEST)],
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=EMAIL_TEST,
                    state=OrganizationAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="group_manager")],
        ),
    )
)
async def test_update_policies() -> None:
    # Act
    loaders = get_new_context()
    await update_policies(
        loaders=get_new_context(),
        email=EMAIL_TEST,
        group_name=GROUP_NAME,
        organization_id=ORG_ID,
        policies_to_update=PoliciesToUpdate(
            days_until_it_breaks=45,
            inactivity_period=30,
            max_acceptance_days=15,
            max_acceptance_severity=Decimal("2.0"),
            min_breaking_severity=Decimal("2.5"),
        ),
    )
    group = await loaders.group.load(GROUP_NAME)

    # Assert
    assert group
    assert group.policies
    assert group.policies.days_until_it_breaks == 45
    assert group.policies.inactivity_period == 30
    assert group.policies.max_acceptance_days == 15
    assert group.policies.max_acceptance_severity == Decimal("2.0")
    assert group.policies.min_breaking_severity == Decimal("2.5")


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME, created_by=EMAIL_TEST)],
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=EMAIL_TEST,
                    state=OrganizationAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="group_manager")],
        ),
    )
)
async def test_update_policies_fail() -> None:
    # Assert
    with raises(InvalidAcceptanceSeverityRange):
        await update_policies(
            loaders=get_new_context(),
            email=EMAIL_TEST,
            group_name=GROUP_NAME,
            organization_id=ORG_ID,
            policies_to_update=PoliciesToUpdate(
                max_acceptance_severity=Decimal("2.0"),
                min_acceptance_severity=Decimal("5.0"),
            ),
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME, created_by=EMAIL_TEST)],
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=EMAIL_TEST,
                    state=OrganizationAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="group_manager")],
        ),
    )
)
async def test_update_forces_access_token() -> None:
    # Act
    loaders = get_new_context()
    forces_token = await update_forces_access_token(
        loaders=loaders,
        group_name=GROUP_NAME,
        email=EMAIL_TEST,
        expiration_time=int(datetime_utils.get_now_plus_delta(days=180).timestamp()),
        responsible=USER_EMAIL,
    )

    # Assert
    assert forces_token


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="group_manager")],
            groups=[
                GroupFaker(
                    name=GROUP_NAME,
                    organization_id=ORG_ID,
                    files=[
                        GroupFile(
                            description="client apk test file",
                            file_name=APK_TEST_FILE,
                            modified_by=EMAIL_TEST,
                        )
                    ],
                )
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    organization_name=ORG_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(nickname="back"),
                )
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(
                    url=APK_TEST_FILE,
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=RootEnvironmentUrlStateFaker(
                        status=RootEnvironmentUrlStateStatus.CREATED,
                        url_type=RootEnvironmentUrlType.APK,
                    ),
                )
            ],
        ),
    ),
    others=[
        Mock(resources_domain, "search_file", "async", True),
        Mock(resources_domain, "remove_file", "async", None),
    ],
)
async def test_update_environment_url_apk() -> None:
    loaders = get_new_context()

    # Add secret since faker is not supported
    urls = await loaders.root_environment_urls.load(
        RootEnvironmentUrlsRequest(root_id=ROOT_ID, group_name=GROUP_NAME),
    )
    assert len(urls) == 1
    await roots_domain.add_secret(
        secret=Secret(
            key="apk_password",
            value="test_password",
            created_at=datetime_utils.get_now(),
            state=SecretState(
                owner=EMAIL_TEST,
                description="secret description",
                modified_by=EMAIL_TEST,
                modified_date=datetime_utils.get_now(),
            ),
        ),
        resource_id=urls[0].id,
        resource_type=ResourceType.URL,
        group_name=GROUP_NAME,
        loaders=loaders,
    )

    new_file_name = "client_apk_v2.apk"
    await update_environment_url(
        loaders=get_new_context(),
        group_name=GROUP_NAME,
        root_id=ROOT_ID,
        url_id=format_environment_id(APK_TEST_FILE),
        file_name=new_file_name,
        old_file_name=APK_TEST_FILE,
        user_email=EMAIL_TEST,
    )

    new_loaders = get_new_context()
    group = await get_group(new_loaders, GROUP_NAME)

    assert group
    assert group.files
    assert group.files[0].file_name == new_file_name

    urls = await new_loaders.root_environment_urls_all.load(
        RootEnvironmentUrlsRequest(root_id=ROOT_ID, group_name=GROUP_NAME)
    )
    assert len(urls) == 2

    urls_created = [
        url
        for url in urls
        if url.url == new_file_name and url.state.status == RootEnvironmentUrlStateStatus.CREATED
    ]
    assert len(urls_created) == 1
    new_secrets = await roots_domain.get_environment_url_secrets(loaders, urls_created[0])
    assert len(new_secrets) == 1


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="group_manager")],
            groups=[
                GroupFaker(
                    name=GROUP_NAME,
                    organization_id=ORG_ID,
                    files=[
                        GroupFile(
                            description="test file",
                            file_name=TEST_FILE,
                            modified_by=EMAIL_TEST,
                        )
                    ],
                )
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(
                    url=TEST_FILE,
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=RootEnvironmentUrlStateFaker(
                        status=RootEnvironmentUrlStateStatus.CREATED,
                        url_type=RootEnvironmentUrlType.APK,
                    ),
                )
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    organization_name=ORG_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(nickname="git_1"),
                )
            ],
        ),
        s3=IntegratesS3(autoload=True),
    ),
    others=[Mock(integrates.s3.operations, "FI_AWS_S3_PATH_PREFIX", "function", "")],
)
async def test_update_environment_url_with_s3() -> None:
    # Arrange
    loaders = get_new_context()

    # Act
    await update_environment_url(
        loaders=get_new_context(),
        group_name=GROUP_NAME,
        root_id=ROOT_ID,
        url_id=format_environment_id(TEST_FILE),
        file_name="brand-new-file.zip",
        old_file_name=TEST_FILE,
        user_email=EMAIL_TEST,
    )
    group = await get_group(loaders, GROUP_NAME)

    # Assert
    assert group
    assert group.files
    assert group.files[0].file_name == "brand-new-file.zip"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="group_manager")],
            groups=[
                GroupFaker(
                    name=GROUP_NAME,
                    organization_id=ORG_ID,
                    files=[
                        GroupFile(
                            description="client apk test file",
                            file_name="testfile.apk",
                            modified_by=EMAIL_TEST,
                        )
                    ],
                ),
                GroupFaker(
                    name="group_no_files",
                    organization_id=ORG_ID,
                    files=[],
                ),
            ],
        ),
    ),
    others=[
        Mock(resources_domain, "search_file", "async", True),
        Mock(resources_domain, "remove_file", "async", None),
    ],
)
async def test_update_environment_url_failed() -> None:
    with raises(ErrorUpdatingGroup):
        await update_environment_url(
            loaders=get_new_context(),
            group_name=GROUP_NAME,
            root_id=ROOT_ID,
            url_id=format_environment_id("not_existing_file"),
            file_name="client_apk_v2.apk",
            old_file_name="not_existing_file",
            user_email=EMAIL_TEST,
        )

    with raises(ErrorUpdatingGroup):
        await update_environment_url(
            loaders=get_new_context(),
            group_name="group_no_files",
            root_id=ROOT_ID,
            url_id=format_environment_id(APK_TEST_FILE),
            file_name="client_apk_v2.apk",
            old_file_name=APK_TEST_FILE,
            user_email=EMAIL_TEST,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="customer_manager")],
            groups=[
                GroupFaker(
                    name=GROUP_NAME,
                    organization_id="org1",
                    files=[
                        GroupFile(
                            description="client test file",
                            file_name="file_test.zip",
                            modified_by=EMAIL_TEST,
                        )
                    ],
                )
            ],
        )
    ),
)
async def test_remove_files() -> None:
    loaders = get_new_context()
    group = await get_group(get_new_context(), GROUP_NAME)

    assert group
    assert group.files
    assert group.files[0].file_name == "file_test.zip"

    await remove_file(
        loaders=loaders,
        email="user@client.com",
        file_name="file_test.zip",
        group_name=GROUP_NAME,
        should_notify=False,
    )

    group = await get_group(get_new_context(), GROUP_NAME)

    assert group
    assert group.files is None


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="customer_manager")],
            groups=[
                GroupFaker(
                    name=GROUP_NAME,
                    organization_id="org1",
                    files=[
                        GroupFile(
                            description="client test file",
                            file_name="test_file.apk",
                            modified_by=EMAIL_TEST,
                        )
                    ],
                )
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(nickname="back"),
                ),
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(
                    url="test_file.apk",
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=RootEnvironmentUrlStateFaker(modified_by="hacker@fluidattacks.com"),
                )
            ],
        )
    ),
    others=[Mock(resources_domain, "remove_file", "async", None)],
)
async def test_remove_files_failed() -> None:
    loaders = get_new_context()
    with raises(ErrorUpdatingGroup):
        await remove_file(
            loaders=loaders,
            email="user@client.com",
            file_name="test_file.zip",
            group_name=GROUP_NAME,
            should_notify=False,
        )

    with raises(ErrorUpdatingGroup):
        await remove_file(
            loaders=loaders,
            email="user@client.com",
            file_name="test_file.apk",
            group_name=GROUP_NAME,
            should_notify=False,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            stakeholders=[StakeholderFaker(email=USER_EMAIL, enrolled=True)],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=USER_EMAIL,
                    state=OrganizationAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            group_access=[
                GroupAccessFaker(
                    email=USER_EMAIL,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            portfolios=[
                PortfolioFaker(groups=set([GROUP_NAME, "group2"]), organization_name=ORG_NAME)
            ],
            roots=[
                GitRootFaker(
                    created_by=USER_EMAIL,
                    group_name=GROUP_NAME,
                    organization_name=ORG_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(nickname="back"),
                ),
            ],
            actions=[
                BatchProcessingFaker(
                    action_name=Action.EXECUTE_MACHINE,
                    batch_job_id="3d85e12c-9c93-5faf-847f-2f3b45630005",
                    entity=GROUP_NAME,
                    running=True,
                    subject=USER_EMAIL,
                    additional_info={
                        "roots": ["root1"],
                        "roots_config_files": ["root1.yaml"],
                    },
                ),
            ],
        ),
        s3=IntegratesS3(autoload=True),
    ),
    others=[Mock(integrates.s3.operations, "FI_AWS_S3_PATH_PREFIX", "function", "")],
)
async def test_remove_group() -> None:
    roots = await get_new_context().group_roots.load(GROUP_NAME)
    assert len(roots) == 1
    pending_executions = await get_actions_by_name(
        action_name=Action.EXECUTE_MACHINE,
        entity=GROUP_NAME,
    )
    assert len(pending_executions) == 1

    await remove_group(
        loaders=get_new_context(),
        comments="This is a test for the group's removal",
        email=USER_EMAIL,
        group_name=GROUP_NAME,
        justification=GroupStateJustification.MISTAKE,
    )

    loaders: Dataloaders = get_new_context()

    organization_groups = await loaders.organization_groups.load(ORG_ID)

    assert not await is_valid(loaders, GROUP_NAME)
    assert len(organization_groups) == 0

    org_portfolios = await loaders.organization_portfolios.load(ORG_NAME)
    assert len(org_portfolios) == 1
    assert org_portfolios[0].groups == {"group2"}

    roots = await loaders.group_roots.load(GROUP_NAME)
    assert len(roots) == 0

    pending_executions = await get_actions_by_name(
        action_name=Action.EXECUTE_MACHINE,
        entity=GROUP_NAME,
    )
    assert len(pending_executions) == 0

    remove_resources_action = await get_actions_by_name(
        action_name=Action.REMOVE_GROUP_RESOURCES,
        entity=GROUP_NAME,
    )
    assert len(remove_resources_action) == 1


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            stakeholders=[StakeholderFaker(email=USER_EMAIL, enrolled=True)],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=USER_EMAIL,
                    state=OrganizationAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            group_access=[
                GroupAccessFaker(
                    email=USER_EMAIL,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            portfolios=[PortfolioFaker(groups={GROUP_NAME, "group2"}, organization_name=ORG_NAME)],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(nickname="back"),
                ),
            ],
            actions=[
                BatchProcessingFaker(
                    action_name=Action.MOVE_ROOT,
                    batch_job_id="3d85e12c-9c93-5faf-847f-2f3b45630005",
                    entity=GROUP_NAME,
                    subject=USER_EMAIL,
                    additional_info={
                        "target_group_name": "group2",
                        "target_root_id": "root_id_2",
                        "source_group_name": GROUP_NAME,
                        "source_root_id": ROOT_ID,
                    },
                ),
            ],
        ),
    ),
)
async def test_remove_group_pending_actions() -> None:
    with raises(GroupHasPendingActions):
        await remove_group(
            loaders=get_new_context(),
            comments="This is a test for the group's removal",
            email=USER_EMAIL,
            group_name=GROUP_NAME,
            justification=GroupStateJustification.MISTAKE,
        )

    roots = await get_new_context().group_roots.load(GROUP_NAME)
    assert len(roots) == 1


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
        ),
    ),
)
async def test_remove_group_invalid_comment() -> None:
    way_too_long_comment = "a" * 1001
    with raises(InvalidFieldLength):
        await remove_group(
            loaders=get_new_context(),
            comments=way_too_long_comment,
            email=USER_EMAIL,
            group_name=GROUP_NAME,
            justification=GroupStateJustification.MISTAKE,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="group_manager")],
            groups=[
                GroupFaker(
                    name=GROUP_NAME,
                    organization_id="org1",
                    state=GroupStateFaker(has_advanced=False),
                ),
                GroupFaker(
                    name="group2",
                    organization_id="org1",
                    state=GroupStateFaker(has_advanced=True),
                ),
            ],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL_TEST,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                ),
                GroupAccessFaker(
                    email=EMAIL_TEST,
                    group_name="group2",
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
        )
    )
)
async def test_request_groups_upgrade_not_allowed() -> None:
    group = await get_group(get_new_context(), GROUP_NAME)
    assert group
    with raises(GroupNotFound):
        await request_upgrade(
            loaders=get_new_context(),
            email=EMAIL_TEST,
            group_names=[GROUP_NAME],
        )
    with raises(BillingSubscriptionSameActive):
        await request_upgrade(
            loaders=get_new_context(),
            email=EMAIL_TEST,
            group_names=["group2"],
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="group_manager")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1", files=[])],
        )
    )
)
async def test_add_file_not_found() -> None:
    loaders = get_new_context()
    with raises(DocumentNotFound):
        await add_file(
            loaders=loaders,
            description="description",
            file_name="file_name.apk",
            group_name=GROUP_NAME,
            email=EMAIL_TEST,
            should_notify=False,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="group_manager")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1", files=[])],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    organization_name=ORG_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(nickname="back"),
                )
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(
                    url="file_name.apk",
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=RootEnvironmentUrlStateFaker(
                        status=RootEnvironmentUrlStateStatus.CREATED,
                        url_type=RootEnvironmentUrlType.APK,
                    ),
                )
            ],
        ),
        s3=IntegratesS3(autoload=True),
    ),
    others=[Mock(integrates.s3.operations, "FI_AWS_S3_PATH_PREFIX", "function", "")],
)
async def test_add_file_queue() -> None:
    loaders = get_new_context()
    await add_file(
        loaders=loaders,
        description="description",
        file_name="file_name.apk",
        group_name=GROUP_NAME,
        email=EMAIL_TEST,
        should_notify=False,
    )

    group = await get_new_context().group.load(GROUP_NAME)
    actions = await get_actions_by_name(action_name=Action.EXECUTE_MACHINE, entity=GROUP_NAME)
    assert group
    assert group.files
    assert group.files[0].file_name == "file_name.apk"
    assert actions
    assert actions[0].additional_info["roots"] == ["back"]


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="group_manager")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1", files=[])],
        ),
        s3=IntegratesS3(autoload=True),
    ),
    others=[Mock(integrates.s3.operations, "FI_AWS_S3_PATH_PREFIX", "function", "")],
)
async def test_add_file() -> None:
    loaders = get_new_context()
    await add_file(
        loaders=loaders,
        description="description",
        file_name="file_name.zip",
        group_name=GROUP_NAME,
        email=EMAIL_TEST,
        should_notify=False,
    )

    group = await get_new_context().group.load(GROUP_NAME)
    actions = await get_actions_by_name(action_name=Action.EXECUTE_MACHINE, entity=GROUP_NAME)
    assert group
    assert group.files
    assert group.files[0].file_name == "file_name.zip"
    assert group.files[0].description == "description"
    assert group.files[0].modified_by == EMAIL_TEST
    assert group.files[0].modified_date is not None
    assert actions == []


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="group_manager")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
        ),
        s3=IntegratesS3(autoload=True),
    ),
    others=[Mock(integrates.s3.operations, "FI_AWS_S3_PATH_PREFIX", "function", "")],
)
async def test_add_file_already_exists() -> None:
    loaders = get_new_context()
    with raises(ErrorFileNameAlreadyExists):
        await add_file(
            loaders=loaders,
            description="description",
            file_name="test.zip",
            group_name=GROUP_NAME,
            email=EMAIL_TEST,
            should_notify=False,
        )


@parametrize(
    args=["email"],
    cases=[
        ["admin@fluidattacks.com"],
        ["organization_manager@fluidattacks.com"],
        ["customer_manager@fluidattacks.com"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(organization_id=ORG_ID, name="group-1")],
            organization_access=[
                OrganizationAccessFaker(
                    email=f"{role}@fluidattacks.com",
                    organization_id=ORG_ID,
                    state=OrganizationAccessStateFaker(
                        modified_by=EMAIL_TEST, has_access=True, role=role
                    ),
                )
                for role in ("customer_manager", "organization_manager")
            ],
            organizations=[OrganizationFaker(id=ORG_ID, name="org1")],
            stakeholders=[
                StakeholderFaker(
                    email=f"{role}@fluidattacks.com",
                    enrolled=True,
                    is_registered=True,
                    role="hacker",
                )
                for role in ("customer_manager", "organization_manager")
            ]
            + [
                StakeholderFaker(email="admin@fluidattacks.com", role="admin"),
            ],
        )
    )
)
async def test_add_group_access(email: str) -> None:
    # Act
    loaders = get_new_context()
    await _add_managers_access(loaders, ORG_ID, "group-1", email)
    group_access = await loaders.group_access.clear_all().load_many(
        [
            GroupAccessRequest(group_name="group-1", email=f"{role}@fluidattacks.com")
            for role in ("customer_manager", "organization_manager")
        ]
    )

    # Assert
    for access in group_access:
        assert access
        assert access.state.has_access
        assert access.state.role == (
            "customer_manager"
            if access.email == "customer_manager@fluidattacks.com"
            else "group_manager"
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(organization_id=ORG_ID, name="group-1")],
            organization_access=[],
            organizations=[OrganizationFaker(id=ORG_ID, name="org1")],
            stakeholders=[
                StakeholderFaker(
                    email=f"{role}@fluidattacks.com",
                    enrolled=True,
                    is_registered=True,
                    role="hacker",
                )
                for role in ("customer_manager", "organization_manager")
            ],
        )
    )
)
async def test_add_group_access_stakeholder_not_in_org() -> None:
    # Act
    loaders = get_new_context()
    with raises(StakeholderNotInOrganization):
        await _add_managers_access(loaders, ORG_ID, "group-1", "creator@company.com")
