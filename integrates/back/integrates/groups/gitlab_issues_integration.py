import asyncio
import json
import logging
from datetime import (
    datetime,
)
from typing import (
    cast,
)

from authlib.integrations.httpx_client import (
    AsyncOAuth2Client,
)
from authlib.integrations.starlette_client import (
    OAuthError,
)
from gitlab import (
    Gitlab,
)
from gitlab.const import (
    AccessLevel,
)
from gitlab.v4.objects import (
    Project,
    ProjectMemberAll,
)
from starlette.requests import (
    Request,
)
from starlette.responses import (
    RedirectResponse,
    Response,
)

from integrates.authz.enforcer import (
    get_group_level_enforcer,
)
from integrates.custom_exceptions import (
    InvalidAuthorization,
)
from integrates.custom_utils import (
    templates,
)
from integrates.custom_utils.groups import get_group
from integrates.custom_utils.requests import (
    get_redirect_url,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.groups.types import (
    GroupGitLabIssues,
    GroupMetadataToUpdate,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.groups import (
    domain as groups_domain,
)
from integrates.oauth import (
    OAUTH,
)
from integrates.sessions.domain import (
    get_jwt_content,
)

LOGGER = logging.getLogger(__name__)


async def update_settings(
    *,
    assignee_ids: list[int],
    gitlab_project_name: str,
    group_name: str,
    issue_automation_enabled: bool,
    labels: list[str],
    loaders: Dataloaders,
) -> None:
    group = await get_group(loaders, group_name)
    if not group.gitlab_issues:
        return
    await groups_domain.update_metadata(
        group_name=group_name,
        metadata=GroupMetadataToUpdate(
            gitlab_issues=group.gitlab_issues._replace(
                assignee_ids=assignee_ids,
                gitlab_project=gitlab_project_name,
                issue_automation_enabled=issue_automation_enabled,
                labels=labels,
            ),
        ),
        organization_id=group.organization_id,
    )


@retry_on_exceptions(exceptions=(OAuthError,))
async def _get_fresh_token(
    *,
    group_name: str,
    loaders: Dataloaders,
) -> str | None:
    async with OAUTH.gitlab_issues._get_oauth_client() as _client:
        client = cast(AsyncOAuth2Client, _client)
        loaders.group.clear(group_name)
        group = await get_group(loaders, group_name)
        settings = group.gitlab_issues
        if settings is None:
            return None

        token_data = await client.refresh_token(
            redirect_uri=settings.redirect_uri,
            refresh_token=settings.refresh_token,
            url=OAUTH.gitlab_issues.access_token_url,
        )
        await groups_domain.update_metadata(
            group_name=group_name,
            metadata=GroupMetadataToUpdate(
                gitlab_issues=settings._replace(
                    refresh_token=token_data["refresh_token"],
                ),
            ),
            organization_id=group.organization_id,
        )
        return token_data["access_token"]


async def get_project_members(
    *,
    gitlab_project: str,
    group_name: str,
    loaders: Dataloaders,
) -> list[ProjectMemberAll]:
    access_token = await _get_fresh_token(
        group_name=group_name,
        loaders=loaders,
    )
    if access_token is None:
        return []

    with Gitlab(oauth_token=access_token) as gitlab:
        project = await asyncio.to_thread(
            gitlab.projects.get,
            gitlab_project,
            lazy=True,
        )
        return cast(
            list[ProjectMemberAll],
            await asyncio.to_thread(project.members_all.list, get_all=True),
        )


async def get_project_names(
    *,
    group_name: str,
    loaders: Dataloaders,
) -> list[str]:
    access_token = await _get_fresh_token(
        group_name=group_name,
        loaders=loaders,
    )
    if access_token is None:
        return []

    with Gitlab(oauth_token=access_token) as gitlab:
        projects = cast(
            list[Project],
            await asyncio.to_thread(
                gitlab.projects.list,
                all=True,
                min_access_level=AccessLevel.REPORTER.value,
            ),
        )
        return [project.path_with_namespace for project in projects]


async def _is_oauth_authorized(
    loaders: Dataloaders,
    request: Request,
    group_name: str,
) -> bool:
    try:
        user = await get_jwt_content(request)
        enforcer = await get_group_level_enforcer(loaders, user["user_email"])
        return enforcer(group_name, "oauth_gitlab_issues")
    except InvalidAuthorization:
        return False


async def begin_oauth_flow(request: Request) -> Response:
    group_name = request.query_params["groupName"]
    loaders = get_new_context()

    if not await _is_oauth_authorized(loaders, request, group_name):
        return templates.login(request)

    redirect_uri = get_redirect_url(request, "end_gitlab_issues_oauth")
    return await OAUTH.gitlab_issues.authorize_redirect(
        request,
        redirect_uri,
        state=json.dumps({"group_name": group_name}),
    )


async def end_oauth_flow(request: Request) -> Response:
    try:
        state = json.loads(request.query_params["state"])
        group_name = state["group_name"]
        loaders = get_new_context()

        if not await _is_oauth_authorized(loaders, request, group_name):
            return templates.login(request)

        redirect_uri = get_redirect_url(request, "end_gitlab_issues_oauth")
        token_data = await OAUTH.gitlab_issues.authorize_access_token(request)
        if not token_data:
            raise OAuthError()
        group = await get_group(loaders, group_name)
        await groups_domain.update_metadata(
            group_name=group.name,
            metadata=GroupMetadataToUpdate(
                gitlab_issues=GroupGitLabIssues(
                    connection_date=datetime.now(),
                    redirect_uri=redirect_uri,
                    refresh_token=token_data["refresh_token"],
                ),
            ),
            organization_id=group.organization_id,
        )
        return RedirectResponse(url="/integrations")
    except (json.JSONDecodeError, KeyError, OAuthError):
        return templates.login(request)
