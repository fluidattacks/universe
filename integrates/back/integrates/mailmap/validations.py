import functools
from collections.abc import Callable
from typing import ParamSpec, TypeVar

from integrates.custom_exceptions import CustomBaseException, InvalidField
from integrates.custom_utils.validations import check_exp

T = TypeVar("T")
P = ParamSpec("P")


def mailmap_email(email: str, ex: CustomBaseException = InvalidField("email address")) -> None:
    local_part_pattern = r"^[a-zA-Z0-9!#$%&'“\"*+/=?^_`{|}[\]~-]+"
    tld_pattern = r"[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9”\"])?$"
    subdomain_pattern = r"(?:.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*"
    domain_pattern = r"@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?.)+"
    check_exp(
        email,
        local_part_pattern + subdomain_pattern + domain_pattern + tld_pattern,
        ex,
    )


def validate_mailmap_email_deco(field: str) -> Callable[[Callable[P, T]], Callable[P, T]]:
    def wrapper(func: Callable[P, T]) -> Callable[P, T]:
        @functools.wraps(func)
        def decorated(*args: P.args, **kwargs: P.kwargs) -> T:
            obj_name, obj_attr = field.split(".", 1)
            obj = kwargs.get(obj_name)
            if isinstance(obj, dict):
                mailmap_email(str(obj.get(obj_attr, "")))
            return func(*args, **kwargs)

        return decorated

    return wrapper
