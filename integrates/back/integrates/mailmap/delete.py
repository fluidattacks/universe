from integrates.db_model import (
    mailmap,
)


async def delete_mailmap_entry(
    entry_email: str,
    organization_id: str,
) -> None:
    await mailmap.delete_mailmap_entry(
        entry_email=entry_email,
        organization_id=organization_id,
    )


async def delete_mailmap_subentry(
    entry_email: str,
    subentry_email: str,
    subentry_name: str,
    organization_id: str,
) -> None:
    await mailmap.delete_mailmap_subentry(
        entry_email=entry_email,
        subentry_email=subentry_email,
        subentry_name=subentry_name,
        organization_id=organization_id,
    )


async def delete_mailmap_entry_subentries(
    entry_email: str,
    organization_id: str,
) -> None:
    await mailmap.delete_mailmap_entry_subentries(
        entry_email=entry_email,
        organization_id=organization_id,
    )


async def delete_mailmap_entry_with_subentries(
    entry_email: str,
    organization_id: str,
) -> None:
    await mailmap.delete_mailmap_entry_with_subentries(
        entry_email=entry_email,
        organization_id=organization_id,
    )
