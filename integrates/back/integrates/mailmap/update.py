from integrates.custom_utils.validations_deco import (
    validate_fields_deco,
    validate_length_field_dict_deco,
)
from integrates.db_model import mailmap
from integrates.db_model.mailmap.types import MailmapEntry, MailmapSubentry
from integrates.mailmap.validations import validate_mailmap_email_deco


@validate_mailmap_email_deco("subentry.mailmap_subentry_email")  # type: ignore[misc]
@validate_length_field_dict_deco(
    "subentry", ("mailmap_subentry_name", "mailmap_subentry_email"), max_length=200, min_length=1
)
@validate_fields_deco(["subentry.mailmap_subentry_name"])
async def update_mailmap_subentry(
    subentry: MailmapSubentry,
    entry_email: str,
    subentry_email: str,
    subentry_name: str,
    organization_id: str,
) -> None:
    await mailmap.update_mailmap_subentry(
        subentry=subentry,
        entry_email=entry_email,
        subentry_email=subentry_email,
        subentry_name=subentry_name,
        organization_id=organization_id,
    )


@validate_mailmap_email_deco("entry.mailmap_entry_email")  # type: ignore[misc]
@validate_length_field_dict_deco(
    "entry", ("mailmap_entry_name", "mailmap_entry_email"), max_length=200, min_length=1
)
@validate_fields_deco(["entry.mailmap_entry_name"])
async def update_mailmap_entry(
    entry_email: str,
    entry: MailmapEntry,
    organization_id: str,
) -> None:
    await mailmap.update_mailmap_entry(
        entry_email=entry_email,
        entry=entry,
        organization_id=organization_id,
    )


async def set_mailmap_subentry_as_mailmap_entry(
    entry_email: str,
    subentry_email: str,
    subentry_name: str,
    organization_id: str,
) -> MailmapEntry:
    entry = await mailmap.set_mailmap_subentry_as_mailmap_entry(
        entry_email=entry_email,
        subentry_email=subentry_email,
        subentry_name=subentry_name,
        organization_id=organization_id,
    )
    return entry


async def set_mailmap_entry_as_mailmap_subentry(
    entry_email: str,
    target_entry_email: str,
    organization_id: str,
) -> None:
    await mailmap.set_mailmap_entry_as_mailmap_subentry(
        entry_email=entry_email,
        target_entry_email=target_entry_email,
        organization_id=organization_id,
    )


async def move_mailmap_entry_with_subentries(
    entry_email: str,
    organization_id: str,
    new_organization_id: str,
) -> None:
    await mailmap.move_mailmap_entry_with_subentries(
        entry_email=entry_email,
        organization_id=organization_id,
        new_organization_id=new_organization_id,
    )


async def convert_mailmap_subentries_to_new_entry(
    entry_email: str,
    main_subentry: MailmapSubentry,
    other_subentries: list[MailmapSubentry],
    organization_id: str,
) -> None:
    await mailmap.convert_mailmap_subentries_to_new_entry(
        entry_email=entry_email,
        main_subentry=main_subentry,
        other_subentries=other_subentries,
        organization_id=organization_id,
    )
