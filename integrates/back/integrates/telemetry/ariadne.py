from collections.abc import Callable
from functools import partial
from inspect import iscoroutinefunction
from typing import Any, NamedTuple, Optional, Union

from ariadne.contrib.tracing.utils import copy_args_for_tracing, format_path, should_trace
from ariadne.types import Extension, Resolver
from graphql import GraphQLError, GraphQLResolveInfo
from opentelemetry.trace import Span, Status, StatusCode, Tracer, get_tracer, set_span_in_context

Item = dict[str, Any]
ArgFilter = Callable[[Item, GraphQLResolveInfo], Item]
RootSpanName = Union[str, Callable[[Any], str]]

DEFAULT_OPERATION_NAME = "GraphQL Operation"


class Operation(NamedTuple):
    name: str
    operation_type: str
    query: str
    variables: Item


class UserMetadata(NamedTuple):
    user_email: str
    group_name: str
    roles: list[str]


class Context(NamedTuple):
    operation: Operation
    user_metadata: UserMetadata
    decoded_body: str


class GraphQLMetadata(NamedTuple):
    operation_name: str
    operation_type: str
    query: str
    raw_query: str
    variables: dict[str, str]
    user_data: UserMetadata


def is_observable(info: GraphQLResolveInfo) -> bool:
    """
    If resolver function from info object is observable
    (has __observable__ attribute), return True
    """
    if info.field_name not in info.parent_type.fields:
        return False

    resolve_func = info.parent_type.fields[info.field_name].resolve
    if not resolve_func:
        return False

    return hasattr(resolve_func, "__observable__")


def get_metadata(context: Context) -> GraphQLMetadata:
    operation = (
        context.operation
        if getattr(context, "operation", False)
        else Operation(
            name=DEFAULT_OPERATION_NAME,
            operation_type="query",
            query="",
            variables={},
        )
    )
    user_data = (
        context.user_metadata
        if getattr(context, "user_metadata", False)
        else UserMetadata(
            user_email="unnamed",
            group_name="unknown",
            roles=[],
        )
    )
    variables: dict[str, str] = {
        key: str(val) for key, val in operation.variables.items() if operation.variables
    }

    return GraphQLMetadata(
        operation_name=operation.name,
        operation_type=operation.operation_type,
        query=operation.query,
        raw_query="",
        variables=variables or {},
        user_data=user_data,
    )


class OpenTelemetryExtension(Extension):
    _arg_filter: Optional[ArgFilter]
    _root_context: Optional[Context]
    _root_span: Span
    _root_span_name: RootSpanName
    _tracer: Tracer

    def __init__(
        self,
        *,
        arg_filter: Optional[ArgFilter] = None,
        root_context: Optional[Context] = None,
        root_span_name: RootSpanName = DEFAULT_OPERATION_NAME,
    ) -> None:
        self._tracer = get_tracer("ariadne")
        self._arg_filter = arg_filter
        self._root_context = root_context
        self._root_span_name = root_span_name

    def request_started(self, context: Any) -> None:
        if callable(self._root_span_name):
            root_span_name = self._root_span_name(context)
        else:
            root_span_name = self._root_span_name

        try:
            metadata = get_metadata(context)
        except Exception:
            metadata = GraphQLMetadata(
                operation_name=DEFAULT_OPERATION_NAME,
                operation_type="query",
                query="",
                raw_query="",
                variables={},
                user_data=UserMetadata(
                    user_email="unnamed",
                    roles=[],
                    group_name="unknown",
                ),
            )

        user_data = metadata.user_data

        root_span = self._tracer.start_span(
            root_span_name,
            context=self._root_context,  # type: ignore[arg-type]
        )
        root_span.set_attribute("component", "GraphQL")
        root_span.set_attribute("graphql.operation.type", metadata.operation_type)
        root_span.set_attribute("graphql.operation.name", metadata.operation_name)
        root_span.set_attribute("graphql.document", metadata.query)
        root_span.set_attribute("graphql.raw_document", metadata.raw_query)
        for variable in metadata.variables:
            root_span.set_attribute(
                f"graphql.operation.variables[{variable}]",
                metadata.variables[variable],
            )

        root_span.set_attribute("user.email", user_data.user_email)
        root_span.set_attribute("user.roles", user_data.roles)
        root_span.set_attribute("user.context.group_name", user_data.group_name)

        self._root_span = root_span

    def request_finished(
        self,
        _context: Optional[Context],  # type: ignore[override]
    ) -> None:
        self._root_span.end()

    def resolve(
        self,
        next_: Resolver,
        obj: Any,
        info: GraphQLResolveInfo,
        **kwargs: dict,
    ) -> Any:
        if not should_trace(info) or not is_observable(info):
            return next_(obj, info, **kwargs)

        graphql_path = ".".join(map(str, format_path(info.path)))
        operation_name = (
            info.operation.name.value if info.operation.name else DEFAULT_OPERATION_NAME
        )

        with self._tracer.start_as_current_span(
            info.field_name,
            context=set_span_in_context(self._root_span),
        ) as span:
            span.set_attribute("component", "GraphQL")
            span.set_attribute("graphql.operation.name", operation_name)
            span.set_attribute("graphql.parentType", info.parent_type.name)
            span.set_attribute("graphql.path", graphql_path)

            if kwargs:
                filtered_kwargs = self.filter_resolver_args(kwargs, info)
                for key, value in filtered_kwargs.items():
                    if not value:
                        continue

                    if isinstance(value, dict):
                        value = str(value)

                    span.set_attribute(f"graphql.arg[{key}]", value)

            if iscoroutinefunction(next_):
                return self.resolve_async(span, next_, obj, info, **kwargs)

            return self.resolve_sync(span, next_, obj, info, **kwargs)

    async def resolve_async(
        self,
        _span: Span,
        next_: Resolver,
        obj: Any,
        info: GraphQLResolveInfo,
        **kwargs: dict,
    ) -> Any:
        return await next_(obj, info, **kwargs)

    def resolve_sync(
        self,
        _span: Span,
        next_: Resolver,
        obj: Any,
        info: GraphQLResolveInfo,
        **kwargs: dict,
    ) -> Any:
        return next_(obj, info, **kwargs)

    def has_errors(
        self,
        errors: list[GraphQLError],
        context: Context,  # type: ignore[override]
    ) -> None:
        for error in errors:
            self._root_span.record_exception(error)
            self._root_span.set_status(
                status=Status(StatusCode.ERROR, error.message),
                description=error.message,
            )

    def filter_resolver_args(self, args: Item, info: GraphQLResolveInfo) -> Item:
        args_to_trace = copy_args_for_tracing(args)

        if not self._arg_filter:
            return args_to_trace

        return self._arg_filter(args_to_trace, info)


def opentelemetry_extension(
    *,
    arg_filter: Optional[ArgFilter] = None,
    root_context: Optional[Context] = None,
    root_span_name: Optional[RootSpanName] = None,
) -> Callable[[], Extension]:
    return partial(
        OpenTelemetryExtension,
        arg_filter=arg_filter,
        root_context=root_context,
        root_span_name=root_span_name,
    )
