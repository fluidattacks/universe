from collections.abc import Callable, Iterable

from grpc import (
    Compression,
)
from opentelemetry import (
    metrics,
    trace,
)
from opentelemetry.exporter.otlp.proto.grpc.metric_exporter import (
    OTLPMetricExporter,
)
from opentelemetry.exporter.otlp.proto.grpc.trace_exporter import (
    OTLPSpanExporter,
)
from opentelemetry.instrumentation.asyncio import AsyncioInstrumentor
from opentelemetry.instrumentation.botocore import (
    BotocoreInstrumentor,
)
from opentelemetry.instrumentation.httpx import (
    HTTPXClientInstrumentor,
)
from opentelemetry.instrumentation.jinja2 import (
    Jinja2Instrumentor,
)
from opentelemetry.instrumentation.requests import (
    RequestsInstrumentor,
)
from opentelemetry.instrumentation.starlette import (
    StarletteInstrumentor,
)
from opentelemetry.instrumentation.urllib import (
    URLLibInstrumentor,
)
from opentelemetry.instrumentation.urllib3 import (
    URLLib3Instrumentor,
)
from opentelemetry.sdk.metrics._internal import (
    MeterProvider,
)
from opentelemetry.sdk.metrics._internal.export import (
    PeriodicExportingMetricReader,
)
from opentelemetry.sdk.resources import (
    DEPLOYMENT_ENVIRONMENT,
    SERVICE_NAME,
    Resource,
)
from opentelemetry.sdk.trace import (
    ReadableSpan,
    TracerProvider,
)
from opentelemetry.sdk.trace.export import (
    BatchSpanProcessor,
    SpanExporter,
    SpanExportResult,
)
from starlette.applications import (
    Starlette,
)

from integrates.context import (
    FI_ENVIRONMENT,
)
from integrates.telemetry.aiobotocore import (
    AioBotocoreInstrumentor,
)
from integrates.telemetry.aioextensions import (
    AioextensionsInstrumentor,
)
from integrates.telemetry.aiohttp import AioHTTPInstrumentor
from integrates.telemetry.opensearch import (
    OpensearchInstrumentor,
)
from integrates.telemetry.standard_library import (
    StandardLibraryInstrumentor,
)

excluded_trace_ids: set[str] = set()

# Hard-coded way to ignore development traces.
# Maybe we can remove these exclusions in the future.
EXCLUDED_LOCATIONS = [".app.fluidattacks.com"]
EXCLUDED_USERS = ["machine@fluidattacks.com", "status@fluidattacks.com"]
EXCLUDED_AGENTS = ["ELB-HealthChecker/2.0"]
EXCLUDED_HOSTS = [
    "localhost",
    "127.0.0.1",
    ".app.fluidattacks.com",
]


class FilteringExporter(SpanExporter):
    def __init__(
        self,
        exporter: SpanExporter,
        filter_func: Callable[[ReadableSpan], bool],
    ) -> None:
        self._exporter = exporter
        self._filter_func = filter_func

    def export(self, spans: Iterable[ReadableSpan]) -> SpanExportResult:
        filtered_spans = [span for span in spans if self._filter_func(span)]
        return self._exporter.export(filtered_spans)


def check_span_http_host(span: ReadableSpan) -> bool:
    attributes: dict[str, str] = getattr(span, "attributes", {}) or {}
    for host in EXCLUDED_HOSTS:
        if host in attributes.get("http.host", ""):
            excluded_trace_ids.add(span.context.trace_id)
            return False

    for location in EXCLUDED_LOCATIONS:
        if location in attributes.get("location_href", ""):
            excluded_trace_ids.add(span.context.trace_id)
            return False

    return True


def span_filter(span: ReadableSpan) -> bool:
    attributes: dict[str, str] = getattr(span, "attributes", {}) or {}
    if span.context.trace_id in excluded_trace_ids:
        return False
    for user in EXCLUDED_USERS:
        if user == attributes.get("user_id", ""):
            excluded_trace_ids.add(span.context.trace_id)
            return False

    for agent in EXCLUDED_AGENTS:
        if agent == attributes.get("http.user_agent", ""):
            excluded_trace_ids.add(span.context.trace_id)
            return False

    return check_span_http_host(span)


def initialize(service_name: str = "integrates-back") -> None:
    """
    Initializes the OpenTelemetry exporters

    Automatic instrumentation was not suitable as it currently lacks support
    for servers that fork processes
    https://opentelemetry-python.readthedocs.io/en/latest/examples/fork-process-model
    """
    resource = Resource.create(
        attributes={
            DEPLOYMENT_ENVIRONMENT: FI_ENVIRONMENT,
            SERVICE_NAME: service_name,
        },
    )

    span_exporter = OTLPSpanExporter(compression=Compression.Gzip)
    filtered_exporter = FilteringExporter(span_exporter, span_filter)
    span_processor = BatchSpanProcessor(filtered_exporter)
    tracer_provider = TracerProvider(resource=resource)

    metric_exporter = OTLPMetricExporter(compression=Compression.Gzip)
    metric_reader = PeriodicExportingMetricReader(metric_exporter)
    meter_provider = MeterProvider(
        metric_readers=[metric_reader],
        resource=resource,
    )

    tracer_provider.add_span_processor(span_processor)
    trace.set_tracer_provider(tracer_provider)
    metrics.set_meter_provider(meter_provider)


def instrument_app(app: Starlette) -> None:
    """Initializes the OpenTelemetry instrumentation"""
    StarletteInstrumentor.instrument_app(app)


def instrument_libraries() -> None:
    """Initializes the OpenTelemetry instrumentation"""
    AioBotocoreInstrumentor().instrument()
    AioextensionsInstrumentor().instrument()
    AioHTTPInstrumentor().instrument()
    AsyncioInstrumentor().instrument()
    BotocoreInstrumentor().instrument()
    HTTPXClientInstrumentor().instrument()
    Jinja2Instrumentor().instrument()
    OpensearchInstrumentor().instrument()
    RequestsInstrumentor().instrument()
    StandardLibraryInstrumentor().instrument()
    URLLibInstrumentor().instrument()
    URLLib3Instrumentor().instrument()
