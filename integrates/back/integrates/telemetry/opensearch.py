from collections.abc import (
    Callable,
    Collection,
    Coroutine,
)
from typing import (
    Any,
)

import opensearchpy
from opentelemetry.instrumentation.instrumentor import (  # type: ignore
    BaseInstrumentor,
)
from opentelemetry.instrumentation.utils import (
    is_instrumentation_enabled,
    unwrap,
)
from opentelemetry.semconv.trace import (
    SpanAttributes,
)
from opentelemetry.trace import (
    SpanKind,
    Status,
    StatusCode,
    get_tracer,
)
from wrapt import (
    wrap_function_wrapper,
)

from integrates.context import (
    FI_AWS_OPENSEARCH_HOST,
)


class OpensearchInstrumentor(BaseInstrumentor):
    """OpenTelemetry instrumentation for Opensearch"""

    def instrumentation_dependencies(self) -> Collection[str]:
        return ["opensearch-py ~= 2.4"]

    def _instrument(self, **kwargs: dict) -> None:
        self.request_hook = kwargs.get("request_hook")
        self.response_hook = kwargs.get("response_hook")
        self._tracer = get_tracer(__name__)
        wrap_function_wrapper(
            opensearchpy,
            "AsyncOpenSearch.__init__",
            self._patched_init_config,
        )
        wrap_function_wrapper(
            opensearchpy,
            "AsyncTransport.perform_request",
            self._patched_perform_request,
        )

    def _uninstrument(self, **kwargs: dict) -> None:
        unwrap(opensearchpy, "AsyncTransport.perform_request")
        unwrap(opensearchpy, "AsyncOpenSearch.__init__")

    def _patched_init_config(
        self,
        original_func: Callable[..., None],
        _instance: opensearchpy.AsyncOpenSearch,
        args: tuple,
        kwargs: dict,
    ) -> None:
        self.host = kwargs.get("hosts", [FI_AWS_OPENSEARCH_HOST])[0]

        original_func(*args, **kwargs)

    async def _patched_perform_request(
        self,
        original_func: Callable[..., Coroutine],
        _instance: opensearchpy.AsyncTransport,
        args: tuple,
        kwargs: dict,
    ) -> None:
        if not is_instrumentation_enabled():
            return await original_func(*args, **kwargs)

        method = args[0]
        path = args[1]
        params = kwargs.get("params", {})
        body = kwargs.get("body")

        if len(path.split("/")) > 2:
            _, index, operation = path.split("/")
        else:
            index = "all"
            operation = path.split("/")[1]

        with self._tracer.start_as_current_span(
            f"{operation} {index}",
            kind=SpanKind.CLIENT,
        ) as span:
            if callable(self.request_hook):
                self.request_hook(span, method, path, args, kwargs)

            if span.is_recording():
                attributes = {
                    SpanAttributes.DB_SYSTEM: "opensearch",
                    SpanAttributes.HTTP_REQUEST_METHOD: method,
                    SpanAttributes.SERVER_ADDRESS: self.host,
                    SpanAttributes.URL_FULL: self._get_full_url(path, params),
                    "db.namespace": "integrates",
                    "db.collection.name": index,
                    "db.operation.name": operation,
                    "db.query.text": str(body),
                }
                for key, value in attributes.items():
                    span.set_attribute(key, value)

            try:
                result = await original_func(*args, **kwargs)

                if callable(self.response_hook):
                    self.response_hook(span, body)
                return result

            except Exception as exc:
                if span.is_recording():
                    span.set_status(Status(StatusCode.ERROR, str(exc)))
                raise exc

    def _get_full_url(self, path: str, params: dict[str, Any]) -> str:
        host = self.host.rstrip("/")
        params_string = "&".join(
            [
                f"{key}={val.decode('utf-8') if isinstance(val, bytes) else val}"
                for key, val in params.items()
            ],
        )

        return f"{host}{path}?{params_string}"
