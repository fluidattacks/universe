from typing import (
    TypeVar,
)

from opentelemetry.trace import (
    INVALID_SPAN,
    get_current_span,
)

TVar = TypeVar("TVar")


def is_root_span() -> bool:
    parent_span = get_current_span()
    return parent_span is INVALID_SPAN
