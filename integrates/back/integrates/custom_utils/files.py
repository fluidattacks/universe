import base64
import csv
import logging
import logging.config
import os
from collections.abc import (
    Iterable,
    Iterator,
)

import magic
from aioextensions import (
    in_thread,
)
from binaryornot.check import (
    is_binary,
)
from pathspec import (
    PathSpec,
)
from starlette.concurrency import (
    run_in_threadpool,
)
from starlette.datastructures import (
    UploadFile,
)
from wcmatch import (
    glob as w_glob,
)

from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger(__name__)


def assert_file_mime(filename: str, allowed_mimes: list[str]) -> bool:
    mime_type = magic.from_file(filename, mime=True)
    return mime_type in allowed_mimes


async def assert_uploaded_file_mime(file_instance: UploadFile, allowed_mimes: list[str]) -> bool:
    mime_type = await get_uploaded_file_mime(file_instance)
    return mime_type in allowed_mimes


async def get_file_size(file_object: UploadFile) -> int:
    file_info = file_object.file

    # Needed while upstream starlette implements a size method

    if file_object._in_memory:
        current_position = file_info.tell()
        file_info.seek(0, os.SEEK_END)
        size = file_info.tell()
        file_info.seek(current_position)
    else:
        current_position = await run_in_threadpool(file_info.tell)
        await run_in_threadpool(file_info.seek, 0, os.SEEK_END)
        size = await run_in_threadpool(file_info.tell)
        await run_in_threadpool(file_info.seek, current_position)
    return size


async def get_uploaded_file_mime(file_instance: UploadFile) -> str:
    await file_instance.seek(0)
    mime_type: str = magic.from_buffer(await file_instance.read(), mime=True)
    await file_instance.seek(0)
    return mime_type


async def get_lines_count(filename: str) -> int:
    """
    Get the number of lines in a file if is non binary.
    ***Warning***
    If you change this function, you must run the refresh toe lines in all
    roots again.
    """
    if not await in_thread(is_binary, filename):
        return await in_thread(_get_num_lines, filename)
    return 0


def _get_num_lines(filename: str) -> int:
    with open(filename, mode="rb") as content:
        num_lines = len(content.readlines())
    return num_lines


def transform_glob(pattern: str) -> str:
    if pattern.startswith("glob(") and pattern.endswith(")"):
        return pattern[5:-1]
    return pattern


def _path_in_pattern(path: str, pattern: str) -> bool:
    if pattern == ".":
        return True
    return path.startswith(pattern) or w_glob.globmatch(path, transform_glob(pattern))


def path_is_include(
    path: str,
    include_patterns: list[str] | None = None,
    exclude_patterns: list[str] | None = None,
) -> bool:
    is_include = False
    if not include_patterns and not exclude_patterns:
        return False
    for include_pattern in include_patterns or []:
        if _path_in_pattern(path, include_pattern):
            is_include = True
            break
    for exclude_path in exclude_patterns or []:
        if _path_in_pattern(path, exclude_path):
            is_include = False
            break

    return is_include


def match_files(patterns: list[str], files: Iterable[str]) -> Iterator[str | os.PathLike]:
    pattern: PathSpec = PathSpec.from_lines("gitwildmatch", patterns)
    yield from pattern.match_files(files)


def list_to_csv(data_list: list[dict[str, str]], csv_file_path: str) -> str | None:
    if not data_list:
        LOGGER.error("Error: Empty list.")
        return None

    if not all(isinstance(item, dict) for item in data_list):
        LOGGER.error("Error: List elements should be dictionaries.")
        return None

    header = data_list[0].keys()
    try:
        with open(
            os.path.join(csv_file_path),
            mode="w",
            encoding="utf-8",
        ) as csv_file:
            csv_writer = csv.DictWriter(csv_file, fieldnames=header)
            csv_writer.writeheader()

            csv_writer.writerows(data_list)
        with open(os.path.join(csv_file_path), "rb") as csv_file:
            encoded_content = base64.b64encode(csv_file.read()).decode("utf-8")
        return encoded_content
    except OSError as exception:
        LOGGER.error("Exception generating csv file %s", exception)
        return None
