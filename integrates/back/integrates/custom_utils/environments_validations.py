import asyncio
import json
import re
from typing import (
    Any,
)

import aioboto3
import botocore
from azure.core.exceptions import (
    ClientAuthenticationError,
)
from azure.identity.aio._credentials.client_secret import (
    ClientSecretCredential,
)
from botocore.exceptions import (
    ClientError,
)
from google.cloud.storage.client import (
    Client,
)
from google.oauth2.service_account import (
    Credentials,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.groups.types import (
    Group,
)


async def check_cspm_azure(secrets: dict[str, Any]) -> bool:
    azure_credentials = {key: value for key, value in secrets.items() if key.startswith("AZURE_")}
    try:
        async with ClientSecretCredential(
            tenant_id=azure_credentials.get("AZURE_TENANT_ID", ""),
            client_id=azure_credentials.get("AZURE_CLIENT_ID", ""),
            client_secret=azure_credentials.get("AZURE_CLIENT_SECRET", ""),
        ) as client_credential:
            await client_credential.get_token("https://management.azure.com/.default")
            return True
    except (ClientAuthenticationError, ValueError):
        return False


async def check_cspm_aws(url: str, cloud_name: str, group: Group, loaders: Dataloaders) -> bool:
    organization_id = group.organization_id
    org = await loaders.organization.load(organization_id)
    external_id = org.state.aws_external_id if org else None

    if (
        cloud_name == "AWS"
        and external_id
        and re.match(r"^arn:aws:iam::\d{12}:role\/[\w+=,.@-]+$", url)
    ):
        try:
            async with aioboto3.Session().client(service_name="sts") as sts_client:
                await sts_client.assume_role(
                    ExternalId=external_id,
                    RoleArn=url,
                    RoleSessionName="FluidAttacksRoleVerification",
                )
        except ClientError:
            return False
        return True
    return False


async def check_cspm_gcp(secrets: dict[str, Any]) -> bool:
    gcp_credentials = {key: value for key, value in secrets.items() if key.startswith("GCP_")}
    if not gcp_credentials.get("GCP_PRIVATE_KEY"):
        return False

    try:
        decoded_credentials = json.loads(gcp_credentials["GCP_PRIVATE_KEY"])
    except json.JSONDecodeError:
        decoded_credentials = None

    try:

        def validate_gcp_credentials(decoded_credentials: dict) -> bool:
            credentials = Credentials.from_service_account_info(
                decoded_credentials,
            )
            storage_client = Client(credentials=credentials)
            list(storage_client.list_buckets())
            return True

        is_valid = await asyncio.to_thread(validate_gcp_credentials, decoded_credentials)

        return is_valid
    except Exception:
        return False


async def validate_aws_cspm_permissions(
    url: str,
    secrets: dict,
) -> bool:
    aws_credentials = {key: value for key, value in secrets.items() if key.startswith("AWS")}
    if not aws_credentials.get("AWS_ACCESS_KEY_ID"):
        return False

    try:
        session = aioboto3.Session(
            aws_access_key_id=aws_credentials["AWS_ACCESS_KEY_ID"],
            aws_secret_access_key=aws_credentials["AWS_SECRET_ACCESS_KEY"],
            aws_session_token=aws_credentials["AWS_SESSION_TOKEN"],
        )
        async with session.client(service_name="iam") as iam_client:
            attached_policies = await iam_client.list_attached_role_policies(
                RoleName=url.split("/")[-1],
            )
            return bool(attached_policies)
    except botocore.exceptions.ClientError:
        return False


async def validate_role_status(
    loaders: Dataloaders,
    url: str,
    cloud_name: str,
    group_name: str,
    secrets: dict[str, Any],
) -> bool:
    group = await loaders.group.load(group_name)

    if group:
        if cloud_name == "AWS":
            return await check_cspm_aws(url, cloud_name, group, loaders)

        if cloud_name == "AZURE":
            return await check_cspm_azure(secrets)

        if cloud_name == "GCP":
            return await check_cspm_gcp(secrets)

    return False
