import yaml

from integrates.class_types.types import (
    Item,
)
from integrates.context import (
    FI_INTEGRATES_CRITERIA_COMPLIANCE,
    FI_INTEGRATES_CRITERIA_REQUIREMENTS,
    FI_INTEGRATES_CRITERIA_VULNERABILITIES,
)

with open(FI_INTEGRATES_CRITERIA_COMPLIANCE, encoding="utf-8") as handler:
    CRITERIA_COMPLIANCE: Item = yaml.safe_load(handler.read())

with open(FI_INTEGRATES_CRITERIA_REQUIREMENTS, encoding="utf-8") as handler:
    CRITERIA_REQUIREMENTS: Item = yaml.safe_load(handler.read())

with open(FI_INTEGRATES_CRITERIA_VULNERABILITIES, encoding="utf-8") as handler:
    CRITERIA_VULNERABILITIES: Item = yaml.safe_load(handler.read())
