import collections
import functools
import re
from collections.abc import Callable
from typing import Any

from urllib3.exceptions import LocationParseError
from urllib3.util import Url, parse_url

from integrates.class_types.types import Item
from integrates.custom_exceptions import (
    InvalidFileName,
    InvalidFilter,
    InvalidPort,
    InvalidProtocol,
    InvalidUrl,
    OnlyCorporateEmails,
)
from integrates.custom_utils.personal_email import is_personal_email
from integrates.custom_utils.validations import is_fluid_staff
from integrates.db_model.findings.types import Finding

from . import findings as findings_utils

IPV4_PAT = r"(?:[0-9]{1,3}\.){3}[0-9]{1,3}"  # from urllib3
IPV6SEG = r"[0-9a-fA-F]{1,4}"
IPV6_PAT = (
    r"\[("
    rf"({IPV6SEG}:){{7,7}}{IPV6SEG}|"
    rf"({IPV6SEG}:){{1,7}}:|"
    rf"({IPV6SEG}:){{1,6}}:{IPV6SEG}|"
    rf"({IPV6SEG}:){{1,5}}(:{IPV6SEG}){{1,2}}|"
    rf"({IPV6SEG}:){{1,4}}(:{IPV6SEG}){{1,3}}|"
    rf"({IPV6SEG}:){{1,3}}(:{IPV6SEG}){{1,4}}|"
    rf"({IPV6SEG}:){{1,2}}(:{IPV6SEG}){{1,5}}|"
    rf"{IPV6SEG}:((:{IPV6SEG}){{1,6}})|"
    rf":((:{IPV6SEG}){{1,7}}|:)|"
    rf"[fF][eE]80:(:{IPV6SEG}){{0,4}}%[a-zA-Z0-9]{{1,}}|"
    rf"::(ffff(:0{1, 4}){0, 1}:){0, 1}{IPV4_PAT}|"
    rf"({IPV6SEG}:){1, 4}:{IPV4_PAT}"
    r")\]"
)
REGNAME_PAT = r"[a-zA-Z0-9\-._~!$&()*+,;=%]+"
ALPHANUMERIC_PAT = r"[a-zA-Z][a-zA-Z0-9_-]+"

HOST_PAT = rf"^({REGNAME_PAT}|{IPV4_PAT}|{IPV6_PAT})$"
REPO_SSH_PAT = (
    rf"^ssh://(?P<auth>{ALPHANUMERIC_PAT})"
    rf"@(?P<host>{REGNAME_PAT}|{IPV4_PAT}|{IPV6_PAT})"
    rf"(?P<divisor>[:/])(?P<user>[a-zA-Z0-9_-]+)"
    r"/(?P<repo>[^?#]+)$"
)
CODECOMMIT_PAT = (
    rf"^codecommit:(:(?P<region>[a-zA-Z0-9-]+):)?//"
    rf"((?P<profile>{ALPHANUMERIC_PAT})@)?"
    rf"(?P<repo>[^?#]+)"
)
MOBILE_EXTENSIONS = [".zip", ".aab", ".ipa", ".apk"]


def camel_case_list_dict(elements: list[dict]) -> list[dict]:
    """Convert a the keys of a list of dicts to camelcase."""
    return [{snakecase_to_camelcase(k): element[k] for k in element} for element in elements]


def camelcase_to_snakecase(str_value: str) -> str:
    """Convert a camelcase string to snakecase."""
    my_str = re.sub("(.)([A-Z][a-z]+)", r"\1_\2", str_value)
    return re.sub("([a-z0-9])([A-Z])", r"\1_\2", my_str).lower()


def filter_findings(findings: list[Finding], filters: Item) -> list[Finding]:
    """Return filtered findings according to filters."""

    def satisfies_filter(finding: Finding) -> bool:
        filter_finding_value = {
            "verified": findings_utils.is_verified(
                finding.unreliable_indicators.verification_summary,
            ),
        }
        hits = 0
        for attr, value in filters.items():
            try:
                result = filter_finding_value[attr]
            except KeyError as ex:
                raise InvalidFilter(attr) from ex
            if str(result) == str(value):
                hits += 1
        return hits == len(filters)

    return [finding for finding in findings if satisfies_filter(finding)]


def list_to_dict(keys: list[Any], values: list[Any]) -> dict[object, object]:
    """Merge two lists into a {key: value} dictionary"""
    dct: dict[object, object] = collections.OrderedDict()
    index = 0

    if len(keys) < len(values):
        diff = len(values) - len(keys)
        for i in range(diff):
            del i
            keys.append("")
    elif len(keys) > len(values):
        diff = len(keys) - len(values)
        for i in range(diff):
            del i
            values.append("")
    else:
        # Each key has a value associated, so there's no need to empty-fill
        pass

    for item in values:
        if keys[index] == "":
            dct[index] = item
        else:
            dct[keys[index]] = item
        index += 1
    return dct


def snakecase_to_camelcase(str_value: str) -> str:
    """Convert a snakecase string to camelcase."""
    return re.sub("_.", lambda x: x.group()[1].upper(), str_value)


def replace_all(text: str, dic: dict[str, str]) -> str:
    for i, j in list(dic.items()):
        text = text.replace(i, j)
    return text


def get_missing_dependency(where: str) -> str:
    try:
        str_info = where.split(" ", maxsplit=1)[1]
    except IndexError:
        return ""
    if match := re.match(r"\(missing dependency: (?P<name>(.+))\)$", str_info):
        match_dict = match.groupdict()
        return match_dict["name"]

    return ""


def ignore_advisories(where: str | None) -> str:
    if where is not None:
        where = re.sub(r"(\s+\(.*\))?(\s+\[.*\])?", "", where)
    return str(where)


LOCAL_HOSTS = {"[::]", "[::1]", "0.0.0.0", "127.0.0.1", "localhost"}  # noqa: S104


def httpsparser(url: str) -> Url:
    """
    Returns url string as a Url object from `urllib3.util`.

    This method parses and validates the url according to our standards
    based on RFC 3986.

    Raises:
        InvalidProtocol: If the url has a protocol different to HTTP(S).
        InvalidUrl: If the hostname is invalid.
        InvalidPort: If the port is invalid.

    """
    try:
        formatted_url = url.strip(" ")
        url_object: Url = parse_url(formatted_url)
        host_regex = re.compile(HOST_PAT, re.UNICODE | re.DOTALL)

        scheme = url_object.scheme
        host = url_object.host

        if scheme not in {"http", "https"}:
            raise InvalidProtocol(scheme or "")

        if (
            host is None
            or not host_regex.match(host or "")
            or host in LOCAL_HOSTS
            or url_object.query is not None
        ):
            raise InvalidUrl()

        return url_object

    except LocationParseError as exc:
        raise InvalidPort() from exc


def sshparser(url: str) -> Url:
    """
    Returns url string as a Url object from `urllib3.util`.

    This method parses and validates the url according to repository
    SSH format.

    Raises:
        InvalidProtocol: If the url has a protocol different to SSH.
        InvalidUrl: If the host is invalid.

    """
    attributes = ["auth", "host", "user", "repo", "divisor"]
    formatted_url = url.strip(" ")
    ssh_regex = re.compile(REPO_SSH_PAT, re.UNICODE | re.DOTALL)

    values = {
        attr: match.group(attr) if (match := ssh_regex.match(formatted_url)) is not None else None
        for attr in attributes
    }

    if values["host"] is None:
        raise InvalidUrl()

    url_object = Url(
        scheme="ssh",
        auth=values["auth"],
        host=f"{values['host']}{values.get('divisor', ':')}{values['user']}",
        port=None,
        fragment=None,
        path=values["repo"],
        query=None,
    )

    return url_object


def codecommitparser(url: str) -> Url:
    """
    Returns url string as a Url object from `urllib3.util`.

    This method parses and validates the url according to CODECOMMIT format.

    Raises:
        InvalidUrl: If the repository is invalid.

    """
    attributes = ["region", "profile", "repo"]
    formatted_url = url.strip(" ")
    codecommit_regex = re.compile(CODECOMMIT_PAT, re.UNICODE | re.DOTALL)

    values = {
        attr: match.group(attr)
        if (match := codecommit_regex.match(formatted_url)) is not None
        else None
        for attr in attributes
    }

    if values["repo"] is None:
        raise InvalidUrl()

    scheme = f"codecommit::{values['region']}" if values["region"] is not None else "codecommit"

    url_object = Url(
        scheme=scheme,
        auth=values["profile"],
        host=values["repo"],
        port=None,
        fragment=None,
        path=None,
        query=None,
    )

    return url_object


def validate_personal_email_invitation_deco(email_field: str, modified_by_field: str) -> Callable:
    def wrapper(func: Callable) -> Callable:
        @functools.wraps(func)
        async def decorated(*args: str, **kwargs: str) -> str:
            email = str(kwargs.get(email_field))
            modified_by = str(kwargs.get(modified_by_field))
            if not is_fluid_staff(modified_by) and await is_personal_email(email):
                raise OnlyCorporateEmails()

            return await func(*args, **kwargs)

        return decorated

    return wrapper


def validate_filename(filename: str) -> None:
    if not any(filename.endswith(valid) for valid in MOBILE_EXTENSIONS):
        raise InvalidFileName()
