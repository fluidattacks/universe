import os
from collections import (
    Counter,
)
from datetime import (
    datetime,
)

from ariadne import (
    load_schema_from_path,
)

from integrates.custom_exceptions import (
    InvalidDateFormat,
)
from integrates.custom_utils.datetime import (
    get_from_str,
    get_now,
    get_now_minus_delta,
)
from integrates.custom_utils.deprecations.ast import (
    get_deprecations_by_period,
    get_due_date,
)
from integrates.testing.utils import freeze_time, raises

MOCK_SCHEMA_PATH: str = os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "..", ".."))

MOCK_SDL_CONTENT: str = load_schema_from_path(MOCK_SCHEMA_PATH)


@freeze_time("2025-01-09T07:56:00+00:00")
def test_get_deprecations_by_period() -> None:
    # Without a start date
    deprecations = get_deprecations_by_period(
        sdl_content=MOCK_SDL_CONTENT, end=get_now(), start=None
    )
    expected_fields: list[str] = [
        "OLD_VALUE",
        "deprecatedField",
        "deprecatedName",
        "deprecatedInput",
        "deprecatedArg",
    ]
    deprecation_values: list[str] = [
        deprecation.field
        for deprecated_fields in deprecations.values()
        for deprecation in deprecated_fields
    ]
    assert Counter(deprecation_values) == Counter(expected_fields)

    # With a start date not very close to the deprecation dates
    no_deprecations = get_deprecations_by_period(
        sdl_content=MOCK_SDL_CONTENT,
        end=get_now(),
        start=get_now_minus_delta(days=30),
    )
    assert no_deprecations == {}


def test_get_due_date() -> None:
    due_date: datetime = get_due_date(
        definition="TestDefinition",
        field="deprecatedField",
        reason="This field will be removed in 2020/01/01",
    )
    assert due_date == get_from_str("2020/01/01", "%Y/%m/%d")

    # No date
    with raises(InvalidDateFormat):
        get_due_date(
            definition="TestDefinition",
            field="deprecatedField",
            reason="This reason field does not have a date :(",
        )
    # DD/MM/YYYY or MM/DD/YYYY
    with raises(InvalidDateFormat):
        get_due_date(
            definition="TestDefinition",
            field="deprecatedField",
            reason="This reason field has a badly formatted date 01/01/2020",
        )
