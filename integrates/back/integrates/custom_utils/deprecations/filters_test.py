import os

from ariadne import load_schema_from_path

from integrates.custom_utils.datetime import (
    get_from_str,
    get_now,
    get_now_minus_delta,
    get_now_plus_delta,
)
from integrates.custom_utils.deprecations import (
    ApiDeprecation,
    ApiFieldType,
    filter_api_deprecation_list,
)
from integrates.testing.utils import freeze_time

MOCK_SCHEMA_PATH: str = os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "..", ".."))
MOCK_SDL_CONTENT: str = load_schema_from_path(MOCK_SCHEMA_PATH)


@freeze_time("2020-06-01")
def test_filter_api_deprecation_list() -> None:
    deprecations: list[ApiDeprecation] = [
        ApiDeprecation(
            parent="testParent",
            field="deprecatedField",
            reason="This field will be removed in 2020/01/01",
            due_date=get_from_str("2020/01/01", "%Y/%m/%d"),
            type=ApiFieldType.OBJECT,
        ),
        ApiDeprecation(
            parent="testParent2",
            field="deprecatedField2",
            reason="This field will be removed in 2020/02/15",
            due_date=get_from_str("2020/02/15", "%Y/%m/%d"),
            type=ApiFieldType.OBJECT,
        ),
        ApiDeprecation(
            parent="customDirective",
            field="deprecatedDirectiveField",
            reason="This field will be removed in 2020/06/15",
            due_date=get_from_str("2020/06/15", "%Y/%m/%d"),
            type=ApiFieldType.DIRECTIVE,
        ),
    ]
    assert (
        len(
            filter_api_deprecation_list(
                deprecations=deprecations,
                end=get_now_plus_delta(days=15),
                start=None,
            )
        )
        == 3
    )
    assert (
        len(filter_api_deprecation_list(deprecations=deprecations, end=get_now(), start=None)) == 2
    )
    assert (
        len(
            filter_api_deprecation_list(
                deprecations=deprecations,
                end=get_now(),
                start=get_now_minus_delta(days=30),
            )
        )
        == 0
    )
