from collections.abc import Iterable

from more_itertools import flatten

from integrates.custom_exceptions import GroupNotFound
from integrates.custom_utils.access import get_stakeholder_groups_names
from integrates.dataloaders import Dataloaders
from integrates.db_model.groups.enums import GroupManaged, GroupStateStatus
from integrates.db_model.groups.types import Group


async def get_group(loaders: Dataloaders, group_name: str) -> Group:
    group = await loaders.group.load(group_name)
    if not group:
        raise GroupNotFound()

    return group


def exclude_review_groups(groups: Iterable[Group]) -> list[Group]:
    return [group for group in groups if group.state.managed != GroupManaged.UNDER_REVIEW]


def filter_trial_groups(groups: Iterable[Group]) -> list[Group]:
    return [
        group
        for group in groups
        if group.state.managed in {GroupManaged.TRIAL, GroupManaged.UNDER_REVIEW}
    ]


def filter_active_groups(groups: Iterable[Group]) -> list[Group]:
    return [group for group in groups if group.state.status == GroupStateStatus.ACTIVE]


def filter_deleted_groups(groups: Iterable[Group]) -> list[Group]:
    return [group for group in groups if group.state.status == GroupStateStatus.DELETED]


async def get_group_names_except_test_groups(
    *,
    loaders: Dataloaders,
    stakeholder_email: str,
) -> list[str]:
    test_groups_names = {
        group.name
        for group in flatten(
            await loaders.organization_groups.load_many(
                [
                    "0d6d8f9d-3814-48f8-ba2c-f4fb9f8d4ffa",  # imamura
                    "a23457e2-f81f-44a2-867f-230082af676c",  # okada
                ],
            ),
        )
    }
    groups_names = await get_stakeholder_groups_names(loaders, stakeholder_email, True)

    group_names = [group_name for group_name in groups_names if group_name not in test_groups_names]

    return group_names
