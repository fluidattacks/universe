from integrates.custom_utils import personal_email
from integrates.custom_utils.personal_email import is_personal_email
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import parametrize


@parametrize(
    args=["email", "expect_result"],
    cases=[
        ["johndoe@gmail.com", True],
        ["johndoe@fluidattacks.com", False],
    ],
)
@mocks(
    others=[
        Mock(
            personal_email,
            "get_free_email_domains",
            "async",
            ["gmail.com"],
        ),
    ]
)
async def test_is_personal_email(email: str, expect_result: bool) -> None:
    result = await is_personal_email(email)
    assert result == expect_result
