import logging
from typing import (
    Any,
    TypeVar,
)

from integrates.s3 import (
    operations as s3_ops,
)

# Constants
LOGGER = logging.getLogger(__name__)
TContext = TypeVar(
    "TContext",
    dict,
    list,
    str,
    Any,
)


async def sign_url(path: str, expire_seconds: int = 60) -> str:
    return await s3_ops.sign_url(
        f"reports/{path}",
        expire_seconds,
    )


async def upload_report(file_name: str) -> str:
    s3_filename = file_name.split("_")[-1]
    await s3_ops.upload_file(
        object_key=f"reports/{s3_filename}",
        file_path=file_name,
    )

    return s3_filename


def get_ordinal_ending(number: int) -> str:
    """
    Get the ordinal representation ending of an integer::

    get_ordinal_ending(22)   => "nd"
    """
    if 11 <= (number % 100) <= 13:
        return "th"
    return ["th", "st", "nd", "rd", "th"][min(number % 10, 4)]


def get_extension(mime_type: str) -> str:
    try:
        return {
            "image/gif": ".gif",
            "image/jpeg": ".jpg",
            "image/png": ".png",
            "application/x-empty": ".exp",
            "text/x-python": ".exp",
            "application/csv": ".csv",
            "text/csv": ".csv",
            "text/plain": ".txt",
            "video/webm": ".webm",
        }[mime_type]
    except KeyError:
        return ""


def filter_context(context: TContext) -> TContext:
    def custom_escape(text: str) -> str:
        special_chars = [
            "*",
            "_",
            "^",
            "{",
            "}",
            "[",
            "]",
            "&",
            "=",
            "\\",
        ]
        for char in special_chars:
            text = text.replace(char, f"\\{char}")

        text = text.replace(r"\\{nbsp\\}", "{nbsp}")
        return text

    # Recursively process the dictionary
    if isinstance(context, dict):
        return {
            key: value if "signature" in key else filter_context(value)
            for key, value in context.items()
        }
    if isinstance(context, list):
        return list(filter_context(item) for item in context)
    if isinstance(context, str):
        return custom_escape(context)

    return context
