import logging.config

import bugsnag
from fluidattacks_core.bugsnag.client import (
    add_batch_metadata as bugsnag_add_batch_metadata,
)
from fluidattacks_core.bugsnag.client import (
    remove_nix_hash as bugsnag_remove_nix_hash,
)

from integrates.context import (
    BASE_URL,
    FI_BUGSNAG_API_KEY_SCHEDULER,
    FI_ENVIRONMENT,
)
from integrates.settings.logger import (
    LOGGING,
)


def start_scheduler_session() -> None:
    bugsnag.before_notify(bugsnag_add_batch_metadata)
    bugsnag.before_notify(bugsnag_remove_nix_hash)
    bugsnag.configure(
        api_key=FI_BUGSNAG_API_KEY_SCHEDULER,
        project_root=BASE_URL,
        release_stage=FI_ENVIRONMENT,
        send_environment=True,
    )
    logging.config.dictConfig(LOGGING)
    bugsnag.start_session()
