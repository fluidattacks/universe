from decimal import (
    Decimal,
)

from integrates.class_types.types import (
    Item,
)
from integrates.custom_exceptions import (
    CustomBaseException,
    InvalidCVSS3VectorString,
    InvalidCVSS4VectorString,
    InvalidCVSSVectorString,
    InvalidSeverityCweIds,
    _SingleMessageException,
)
from integrates.custom_utils import (
    cvss as cvss_utils,
)
from integrates.db_model.types import (
    SeverityScore,
)
from integrates.testing.utils import parametrize, raises
from integrates.vulnerability_files.utils import (
    get_from_v3,
)


@parametrize(
    args=["cwe_ids", "expected_result"],
    cases=[
        [None, None],
        [[], None],
        [
            ["CWE-1035", "CWE-770", "CWE-937"],
            ["CWE-1035", "CWE-770", "CWE-937"],
        ],
        [
            ["CWE-1035", "CWE-937", "CWE-770", "CWE-770", "CWE-770"],
            ["CWE-1035", "CWE-770", "CWE-937"],
        ],
    ],
)
async def test_parse_cwe_ids(cwe_ids: list[str] | None, expected_result: list[str] | None) -> None:
    assert cvss_utils.parse_cwe_ids(cwe_ids) == expected_result


@parametrize(
    args=["cwe_ids"],
    cases=[
        [["CWE-1035", "CWE-770", "CWE-93700"]],
        [["CWE-1035", "CWE-770", "CWE-937x"]],
        [["CWE-1035", "CWE-770", "cwe-937"]],
    ],
)
async def test_parse_cwe_ids_fail(cwe_ids: list[str] | None) -> None:
    with raises(InvalidSeverityCweIds):
        cvss_utils.parse_cwe_ids(cwe_ids)


@parametrize(
    args=["criteria_vulnerability", "vector_string"],
    cases=[
        [
            {  # F001 - SQL injection - C Sharp SQL API
                "score": {
                    "base": {
                        "attack_vector": "N",
                        "attack_complexity": "L",
                        "privileges_required": "L",
                        "user_interaction": "N",
                        "scope": "U",
                        "confidentiality": "N",
                        "integrity": "L",
                        "availability": "N",
                    },
                    "temporal": {
                        "exploit_code_maturity": "U",
                        "remediation_level": "O",
                        "report_confidence": "R",
                    },
                },
            },
            "CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:L/A:N/E:U/RL:O/RC:R",
        ],
        [
            {  # F005 - Privilege escalation
                "score": {
                    "base": {
                        "attack_vector": "N",
                        "attack_complexity": "H",
                        "privileges_required": "H",
                        "user_interaction": "N",
                        "scope": "C",
                        "confidentiality": "H",
                        "integrity": "H",
                        "availability": "H",
                    },
                    "temporal": {
                        "exploit_code_maturity": "X",
                        "remediation_level": "X",
                        "report_confidence": "X",
                    },
                },
            },
            "CVSS:3.1/AV:N/AC:H/PR:H/UI:N/S:C/C:H/I:H/A:H",
        ],
        [
            {  # F011 - Use of software with known vulnerabilities
                "score": {
                    "base": {
                        "attack_vector": "N",
                        "attack_complexity": "H",
                        "privileges_required": "L",
                        "user_interaction": "N",
                        "scope": "U",
                        "confidentiality": "L",
                        "integrity": "L",
                        "availability": "L",
                    },
                    "temporal": {
                        "exploit_code_maturity": "P",
                        "remediation_level": "O",
                        "report_confidence": "C",
                    },
                },
            },
            "CVSS:3.1/AV:N/AC:H/PR:L/UI:N/S:U/C:L/I:L/A:L/E:P/RL:O/RC:C",
        ],
    ],
)
async def test_get_criteria_cvss_vector(criteria_vulnerability: Item, vector_string: str) -> None:
    assert cvss_utils.get_criteria_cvss_vector(criteria_vulnerability) == vector_string


@parametrize(
    args=["criteria_vulnerability"],
    cases=[
        [
            {
                "score": {
                    "base": {
                        "attack_vector": "N",
                        "attack_complexity": "H",
                        "privileges_required": "L",
                        "user_interaction": "A",
                        "scope": "U",
                        "confidentiality": "L",
                        "integrity": "L",
                        "availability": "L",
                    },
                    "temporal": {
                        "exploit_code_maturity": "P",
                        "remediation_level": "O",
                        "report_confidence": "C",
                    },
                },
            },
        ],
    ],
)
async def test_get_criteria_cvss_vector_exception(
    criteria_vulnerability: Item,
) -> None:
    with raises(InvalidCVSS3VectorString):
        cvss_utils.get_criteria_cvss_vector(criteria_vulnerability)


@parametrize(
    args=["criteria_vulnerability", "vector_string"],
    cases=[
        [
            {  # F005 - Privilege escalation
                "score_v4": {
                    "base": {
                        "attack_vector": "N",
                        "attack_complexity": "H",
                        "attack_requirements": "N",
                        "privileges_required": "H",
                        "user_interaction": "N",
                        "confidentiality_vc": "H",
                        "integrity_vi": "H",
                        "availability_va": "H",
                        "confidentiality_sc": "L",
                        "integrity_si": "L",
                        "availability_sa": "L",
                    },
                    "threat": {
                        "exploit_maturity": "X",
                    },
                },
            },
            "CVSS:4.0/AV:N/AC:H/AT:N/PR:H/UI:N/VC:H/VI:H/VA:H/SC:L/SI:L/SA:L",
        ],
        [
            {  # F001 - SQL injection - C Sharp SQL API
                "score_v4": {
                    "base": {
                        "attack_vector": "N",
                        "attack_complexity": "L",
                        "attack_requirements": "N",
                        "privileges_required": "L",
                        "user_interaction": "N",
                        "confidentiality_vc": "N",
                        "integrity_vi": "L",
                        "availability_va": "N",
                        "confidentiality_sc": "N",
                        "integrity_si": "N",
                        "availability_sa": "N",
                    },
                    "threat": {
                        "exploit_maturity": "U",
                    },
                },
            },
            "CVSS:4.0/AV:N/AC:L/AT:N/PR:L/UI:N/VC:N/VI:L/VA:N/SC:N/SI:N/SA:N/E:U",
        ],
        [
            {  # F011 - Use of software with known vulnerabilities
                "score_v4": {
                    "base": {
                        "attack_vector": "N",
                        "attack_complexity": "H",
                        "attack_requirements": "N",
                        "privileges_required": "L",
                        "user_interaction": "N",
                        "confidentiality_vc": "L",
                        "integrity_vi": "L",
                        "availability_va": "L",
                        "confidentiality_sc": "N",
                        "integrity_si": "N",
                        "availability_sa": "N",
                    },
                    "threat": {
                        "exploit_maturity": "P",
                    },
                },
            },
            "CVSS:4.0/AV:N/AC:H/AT:N/PR:L/UI:N/VC:L/VI:L/VA:L/SC:N/SI:N/SA:N/E:P",
        ],
    ],
)
async def test_get_criteria_cvss4_vector(criteria_vulnerability: Item, vector_string: str) -> None:
    assert cvss_utils.get_criteria_cvss4_vector(criteria_vulnerability) == vector_string


@parametrize(
    args=["criteria_vulnerability"],
    cases=[
        [
            {
                "score_v4": {
                    "base": {
                        "attack_vector": "N",
                        "attack_complexity": "H",
                        "attack_requirements": "N",
                        "privileges_required": "L",
                        "user_interaction": "R",
                        "confidentiality_vc": "L",
                        "integrity_vi": "L",
                        "availability_va": "L",
                        "confidentiality_sc": "N",
                        "integrity_si": "N",
                        "availability_sa": "N",
                    },
                    "threat": {
                        "exploit_maturity": "P",
                    },
                },
            },
        ],
    ],
)
async def test_get_criteria_cvss4_vector_exception(
    criteria_vulnerability: Item,
) -> None:
    with raises(InvalidCVSS4VectorString):
        cvss_utils.get_criteria_cvss4_vector(criteria_vulnerability)


@parametrize(
    args=["expected_result", "vector_string"],
    cases=[
        [
            False,
            "CVSS:4.0/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:L/A:N/E:U/RL:O/RC:R",
        ],
        [
            False,
            "CVSS:4.0/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:L/A:N/E:P/RL:O/RC:R",
        ],
        [
            True,
            "CVSS:4.0/AV:N/AC:H/PR:H/UI:N/S:C/C:H/I:H/A:H",
        ],
        [
            True,
            "CVSS:4.0/AV:N/AC:H/PR:L/UI:N/S:U/C:L/I:L/A:L/E:A/RL:O/RC:C",
        ],
        [
            True,
            "CVSS:4.0/AV:N/AC:H/PR:L/UI:N/S:U/C:L/I:L/A:L/E:A/RL:O/RC:C",
        ],
        [
            True,
            "CVSS:4.0/AV:N/AC:H/PR:L/UI:N/S:U/C:L/I:L/A:L/E:X",
        ],
    ],
)
async def test_get_is_exploitable(expected_result: bool, vector_string: str) -> None:
    assert cvss_utils.get_is_exploitable(vector_string) == expected_result


@parametrize(
    args=["severity", "expected"],
    cases=[
        [Decimal("0"), "low"],
        [Decimal("3.9"), "low"],
        [Decimal("4"), "medium"],
        [Decimal("6.9"), "medium"],
        [Decimal("7"), "high"],
        [Decimal("8.9"), "high"],
        [Decimal("9"), "critical"],
        [Decimal("10"), "critical"],
    ],
)
async def test_get_severity_level(severity: Decimal, expected: str) -> None:
    assert cvss_utils.get_severity_level(severity) == expected


@parametrize(
    args=["temporal_score", "expected"],
    cases=[
        [Decimal("4.0"), Decimal("1.000")],
        [Decimal("5.0"), Decimal("4.000")],
        [Decimal("6.0"), Decimal("16.000")],
    ],
)
async def test_get_cvssf_score(temporal_score: Decimal, expected: Decimal) -> None:
    assert cvss_utils.get_cvssf_score(temporal_score) == expected


@parametrize(
    args=["vector_string"],
    cases=[
        ["CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H"],
        ["CVSS:3.1/AV:N/AC:L/PR:H/UI:N/S:C/C:L/I:L/A:N"],
    ],
)
async def test_validate_cvss_vector(vector_string: str) -> None:
    cvss_utils.validate_cvss_vector(vector_string)


@parametrize(
    args=["vector_string"],
    cases=[
        ["CVSS:4.0/AV:L/AC:L/AT:P/PR:L/UI:N/VC:H/VI:H/VA:H/SC:N/SI:N/SA:N"],
        ["CVSS:4.0/AV:N/AC:L/AT:P/PR:N/UI:P/VC:H/VI:H/VA:H/SC:N/SI:N/SA:N/E:U"],
    ],
)
async def test_validate_cvss4_vector(vector_string: str) -> None:
    cvss_utils.validate_cvss4_vector(vector_string)


async def test_validate_cvss4_vector_exception() -> None:
    with raises(InvalidCVSS4VectorString):
        cvss_utils.validate_cvss4_vector(
            "CVSS:4.0/AV:N/AC:L/PR:N/UI:P/VC:H/VI:H/VA:H/SC:N/SI:N/SA:N/E:U"
        )


async def test_validate_cvss_vector_exception() -> None:
    with raises(InvalidCVSS3VectorString):
        cvss_utils.validate_cvss_vector("CVSS:3.1/AV:N/AC:L/PR:N/UI:A/S:U/C:H/I:H/A:H")


@parametrize(
    args=["vector_v3", "vector_v4", "exception"],
    cases=[
        [
            "CVSS:2.0/AV:N/AC:L/PR:N/UI:N/S:C/C:L/I:N/A:N",
            "CVSS:4.0/AV:L/AC:L/AT:P/PR:L/UI:N/VC:H/VI:H/VA:H/SC:N/SI:N/SA:N",
            InvalidCVSS3VectorString,
        ],
        [
            "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H",
            "CVSS:4.0/AV:N/AC:L/AT:P/PR:N/UI:R/VC:H/VI:H/VA:H/SC:N/SI:N/SA:N/E:U",
            InvalidCVSS4VectorString,
        ],
    ],
)
async def test_get_severity_score_from_cvss_vector_invalid(
    vector_v3: str, vector_v4: str, exception: _SingleMessageException
) -> None:
    with raises(Exception, match=exception.msg):
        cvss_utils.get_severity_score_from_cvss_vector(vector_v3, vector_v4)


@parametrize(
    args=["vector_v3", "vector_v4"],
    cases=[
        [
            "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H",
            "CVSS:4.0/AV:N/AC:L/AT:P/PR:N/UI:A/VC:H/VI:H/VA:H/SC:N/SI:N/SA:N/E:U",
        ],
    ],
)
async def test_get_severity_score_from_cvss_vector_valid(vector_v3: str, vector_v4: str) -> None:
    assert cvss_utils.get_severity_score_from_cvss_vector(vector_v3, vector_v4) == SeverityScore(
        base_score=Decimal("9.8"),
        temporal_score=Decimal("9.8"),
        cvss_v3="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H",
        cvssf=Decimal("3104.188"),
        cvssf_v4=Decimal("3.031"),
        threat_score=Decimal("4.8"),
        cvss_v4="CVSS:4.0/AV:N/AC:L/AT:P/PR:N/UI:A/VC:H/VI:H/VA:H/SC:N/SI:N/SA:N/E:U",
    )


@parametrize(
    args=["vector_v3", "vector_v4", "exception"],
    cases=[
        [
            "",
            "CVSS:4.0/AV:L/AC:L/AT:P/PR:L/UI:N/VC:H/VI:H/VA:H/SC:N/SI:N/SA:N",
            InvalidCVSSVectorString(cvss_version="v3.1"),
        ],
        [
            "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H",
            "",
            InvalidCVSSVectorString(cvss_version="v4.0"),
        ],
    ],
)
async def test_get_severity_score_from_cvss_vector_exception(
    vector_v3: str, vector_v4: str, exception: CustomBaseException
) -> None:
    with raises(Exception, match=str(exception)):
        cvss_utils.get_severity_score_from_cvss_vector(vector_v3, vector_v4)


@parametrize(
    args=["vector_v3", "vector_v4"],
    cases=[
        [
            "CVSS:3.1/AV:L/AC:L/PR:L/UI:N/S:C/C:H/I:H/A:N/E:U/RL:O/RC:C",
            "CVSS:4.0/AV:L/AC:L/PR:L/UI:N/SC:L/SI:L/SA:L/VC:H/VI:H/VA:N/E:U/AT:N",
        ],
    ],
)
async def test_get_from_v3(vector_v3: str, vector_v4: str) -> None:
    assert get_from_v3(vector_v3) == vector_v4
