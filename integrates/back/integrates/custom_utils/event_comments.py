from integrates.class_types.types import (
    Item,
)
from integrates.custom_utils.datetime import (
    format_comment_datetime,
)
from integrates.custom_utils.validations import (
    is_fluid_staff,
)
from integrates.db_model.event_comments.types import (
    EventComment,
)


def _get_email(
    objective_data: EventComment,
    target_email: str = "",
) -> str:
    objective_email = objective_data.email
    if is_fluid_staff(objective_email) and not is_fluid_staff(target_email):
        return "help@fluidattacks.com"

    return objective_email


def _get_fullname(
    objective_data: EventComment,
    target_email: str = "",
) -> str:
    objective_email = objective_data.email
    objective_possible_fullname = objective_data.full_name if objective_data.full_name else None
    real_name = objective_possible_fullname or objective_email

    if is_fluid_staff(objective_email) and not is_fluid_staff(target_email):
        return "Fluid Attacks"

    return real_name


def format_event_consulting_resolve(
    event_comment: EventComment,
    target_email: str = "",
) -> Item:
    email = _get_email(objective_data=event_comment, target_email=target_email)
    fullname = _get_fullname(objective_data=event_comment, target_email=target_email)
    comment_date: str = format_comment_datetime(event_comment.creation_date)
    return {
        "content": event_comment.content,
        "created": comment_date,
        "email": email,
        "fullname": fullname if fullname else email,
        "id": event_comment.id,
        "modified": comment_date,
        "parent": event_comment.parent_id,
    }
