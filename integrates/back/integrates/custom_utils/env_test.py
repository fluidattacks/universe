import os

from integrates.custom_utils.env import (
    guess_environment,
)
from integrates.testing.utils import parametrize


@parametrize(
    args=["environment", "expect_result"],
    cases=[
        ["trunk", "production"],
        ["dev", "development"],
    ],
)
def test_guess_environment(environment: str, expect_result: str) -> None:
    os.environ["CI_COMMIT_REF_NAME"] = environment
    assert guess_environment() == expect_result
