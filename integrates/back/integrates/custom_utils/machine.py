from integrates.class_types.types import (
    Item,
)
from integrates.custom_utils import (
    cvss as cvss_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.roots.enums import (
    RootEnvironmentUrlType,
    RootStatus,
)
from integrates.db_model.roots.types import (
    GitRoot,
    Root,
    RootEnvironmentUrlsRequest,
)


def has_machine_description(
    *,
    criteria_vulnerability: Item,
    language: str,
    cvss_vector: str,
    description: str,
    threat: str,
    cvss4_vector: str | None = None,
) -> bool:
    return all(
        (
            description.strip() == criteria_vulnerability[language]["description"].strip(),
            threat.strip() == criteria_vulnerability[language]["threat"].strip(),
            cvss_vector == cvss_utils.get_criteria_cvss_vector(criteria_vulnerability),
            *(
                not cvss4_vector
                or cvss4_vector == cvss_utils.get_criteria_cvss4_vector(criteria_vulnerability),
            ),
        ),
    )


async def _has_env_url_for_apk(loaders: Dataloaders, root: Root, file_name: str) -> bool:
    return any(
        url.state.url_type == RootEnvironmentUrlType.APK and url.url == file_name
        for url in await loaders.root_environment_urls.load(
            RootEnvironmentUrlsRequest(root_id=root.id, group_name=root.group_name),
        )
    )


async def find_root_nicknames_for_apk_file(
    loaders: Dataloaders,
    group_name: str,
    file_name: str,
) -> set[str]:
    group_roots = await loaders.group_roots.load(group_name)
    return {
        root.state.nickname
        for root in group_roots
        if (
            isinstance(root, GitRoot)
            and root.state.status == RootStatus.ACTIVE
            and await _has_env_url_for_apk(loaders=loaders, root=root, file_name=file_name)
        )
    }
