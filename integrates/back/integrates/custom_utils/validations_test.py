from datetime import (
    datetime,
)

from integrates.custom_exceptions import (
    CustomBaseException,
    DuplicateDraftFound,
    ErrorFileNameAlreadyExists,
    InvalidAWSRoleTrustPolicy,
    InvalidChar,
    InvalidCommitHash,
    InvalidField,
    InvalidFieldChange,
    InvalidMarkdown,
    InvalidMinTimeToRemediate,
    InvalidReportFilter,
    NumberOutOfRange,
    OutdatedRepository,
    UnsanitizedInputFound,
)
from integrates.custom_utils import validations
from integrates.custom_utils.validations import (
    assert_is_isalnum,
    check_and_set_min_time_to_remediate,
    check_url,
    has_sequence,
    sequence_decreasing,
    sequence_increasing,
    validate_alphanumeric_field,
    validate_aws_role,
    validate_chart_field,
    validate_commit_hash,
    validate_email_address,
    validate_fields,
    validate_file_exists,
    validate_file_name,
    validate_finding_id,
    validate_finding_title_change_policy,
    validate_group_language,
    validate_include_lowercase,
    validate_include_number,
    validate_include_uppercase,
    validate_int_range,
    validate_markdown,
    validate_no_duplicate_drafts,
    validate_outdated_repository,
    validate_sanitized_csv_input,
    validate_sequence,
    validate_start_letter,
    validate_symbols,
)
from integrates.db_model.findings.enums import FindingStatus
from integrates.db_model.groups.types import (
    GroupFile,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityType,
)
from integrates.testing.fakers import FindingFaker, VulnerabilityFaker, VulnerabilityStateFaker
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import freeze_time, parametrize, raises


def test_check_url() -> None:
    check_url("https://test.com/%7Bvalue%7D")


def test_check_url_non_valid_braces() -> None:
    with raises(CustomBaseException):
        check_url("https://test.com/{value}", ex=CustomBaseException())


def test_check_url_non_valid() -> None:
    with raises(CustomBaseException):
        check_url("https://test.com/%3C*%3E", ex=CustomBaseException())


def test_validate_alphanumeric_field() -> None:
    assert validate_alphanumeric_field("one test")
    with raises(InvalidField):
        validate_alphanumeric_field("=test2@")


def test_validate_email_address() -> None:
    assert validate_email_address("test@unittesting.com")
    with raises(InvalidField):
        assert validate_email_address("testunittesting.com")
    with raises(InvalidField):
        assert validate_email_address("test+1@unittesting.com")


@parametrize(
    args=["fields"],
    cases=[
        [["valid", " =invalid"]],
        [["=testfield", "testfield2"]],
        [["testfield", "testfiel`d"]],
        [["testfield", "<testfield2"]],
    ],
)
def test_validate_fields(fields: list) -> None:
    validate_fields(["valid%", " valid="])
    validate_fields(["testfield", "testfield2"])
    with raises(InvalidChar):
        validate_fields(fields)


def test_validate_file_exists() -> None:
    file_name = "test1.txt"
    validate_file_exists(
        file_name,
        None,
    )
    group_files = [
        GroupFile(
            description="abc",
            file_name="test2.txt",
            modified_by="user@gmail.com",
        ),
        GroupFile(
            description="xyz",
            file_name="test3.txt",
            modified_by="user@gmail.com",
        ),
    ]
    validate_file_exists(
        file_name=file_name,
        group_files=group_files,
    )
    with raises(ErrorFileNameAlreadyExists):
        validate_file_exists(
            "test2.txt",
            group_files,
        )
    with raises(ErrorFileNameAlreadyExists):
        validate_file_exists(
            "test3.txt",
            group_files,
        )


def test_validate_file_name() -> None:
    validate_file_name("test123.py")
    with raises(InvalidChar):
        validate_file_name("test.test.py")
    with raises(InvalidChar):
        validate_file_name(
            "test|=$invalidname!.py",
        )


def test_validate_group_name() -> None:
    @assert_is_isalnum("group_name")
    def _test_group_name(*, group_name: str) -> None: ...

    _test_group_name(group_name="test")
    with raises(InvalidField):
        _test_group_name(group_name="=test2@")


@parametrize(
    args=["value", "lower_bound", "upper_bound", "inclusive"],
    cases=[
        [10, 11, 12, True],
        [10, 11, 12, False],
    ],
)
def test_validate_int_range(
    value: int, lower_bound: int, upper_bound: int, inclusive: bool
) -> None:
    with raises(NumberOutOfRange):
        validate_int_range(value, lower_bound, upper_bound, inclusive)


@parametrize(
    args=["field"],
    cases=[
        ['"=invalidField"'],
        ["'+invalidField"],
        [",-invalidField"],
        [";@invalidField"],
        ["=invalidField"],
        ["+invalidField"],
        ["-invalidField"],
        ["@invalidField"],
        ["\\ninvalidField"],
    ],
)
def test_validate_sanitized_csv_input(field: str) -> None:
    validate_sanitized_csv_input(
        "validfield@",
        "valid+field",
        "valid field",
        "http://localhost/bWAPP/sqli_1.php",
    )
    with raises(UnsanitizedInputFound):
        validate_sanitized_csv_input(field)


def test_sequence_decreasing() -> None:
    assert sequence_decreasing("a", ord("a"), [ord("c"), ord("b")], False) == [
        ord("c"),
        ord("b"),
        ord("a"),
    ]
    assert sequence_decreasing("c", ord("c"), [ord("a"), ord("b")], True) == [ord("c")]
    assert sequence_decreasing("$", ord("$"), [ord("c"), ord("b")], False) == [ord("$")]


def test_sequence_increasing() -> None:
    assert sequence_increasing("c", ord("c"), [ord("a"), ord("b")], True) == [
        ord("a"),
        ord("b"),
        ord("c"),
    ]
    assert sequence_increasing("a", ord("a"), [ord("c"), ord("b")], False) == [ord("a")]
    assert sequence_increasing("$", ord("$"), [ord("a"), ord("b")], True) == [ord("$")]


@parametrize(
    args=["value", "length", "should_fail"],
    cases=[
        ["a123b", 3, True],
        ["a123b", 4, False],
        ["a876b", 3, True],
        ["a876b", 4, False],
        ["aabcc", 3, True],
        ["aabcc", 4, False],
        ["ayxwc", 3, True],
        ["ayxwc", 4, False],
        ["aDEFc", 3, True],
        ["aDEFc", 4, False],
        ["aQPOc", 3, True],
        ["aQPOc", 4, False],
        ["a1221b", 3, False],
        ["a123321b", 4, False],
        ["a3455431b", 4, False],
        ["a1357b", 4, False],
        ["a9753b", 4, False],
        ["acdefghijklabcc", 7, True],
    ],
)
def test_has_sequence(value: str, length: int, should_fail: bool) -> None:
    assert has_sequence(value, length) == should_fail


def test_validate_sequence() -> None:
    validate_sequence(value="a1221b")
    validate_sequence(value="no")
    with raises(InvalidReportFilter):
        validate_sequence(value="aabcc")
    with raises(InvalidReportFilter):
        validate_sequence(value="6543221")


@parametrize(
    args=["value", "should_fail"],
    cases=[
        ["a123b", True],
        ["a'123b", False],
        ["a~876b", False],
        ["a87:6b", False],
        ["aa;bcc", False],
        ["aa<bcc", False],
        ["ayx%wc", False],
        ["ay>xwc", False],
        ["aDEFc", True],
        ["aDE=Fc", False],
        ["aQP@Oc", False],
        ["aQP-Oc", False],
        ["a12]21b", False],
        ["a123+321b", False],
        ["a34^55431b", False],
        ['a1"357b', False],
        ["a97?53b", False],
    ],
)
def test_validate_symbols(value: str, should_fail: bool) -> None:
    if should_fail:
        with raises(InvalidReportFilter):
            validate_symbols(value)
    else:
        validate_symbols(value)


def test_validate_finding_id() -> None:
    validate_finding_id(finding_id="3c475384-834c-47b0-ac71-a41a022e401c")

    validate_finding_id(finding_id="123456781234567812345678")

    with raises(InvalidField):
        validate_finding_id(finding_id="12345678-1234-1234-1234-1234567890a")
    with raises(InvalidField):
        validate_finding_id(finding_id="invalid_finding_id")


def test_validate_group_language() -> None:
    validate_group_language(language="es")
    validate_group_language(language="EN")
    with raises(InvalidField):
        validate_group_language(language="fr")
    with raises(InvalidField):
        validate_group_language(language="")


def test_validate_title_change() -> None:
    # Test valid input
    assert validate_finding_title_change_policy(
        old_title="old_title",
        new_title="new_title",
        status=FindingStatus.DRAFT,
    )

    # Test invalid input
    with raises(InvalidFieldChange):
        validate_finding_title_change_policy(
            old_title="old_title",
            new_title="new_title",
            status=FindingStatus.VULNERABLE,
        )
        validate_finding_title_change_policy(
            old_title="old_title",
            new_title="new_title",
            status=FindingStatus.SAFE,
        )


def test_validate_commit_hash() -> None:
    validate_commit_hash("da39a3ee5e6b4b0d3255bfef95601890afd80709")
    validate_commit_hash("e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855")
    with raises(InvalidCommitHash):
        validate_commit_hash("invalid Hash")


def test_validate_start_letter() -> None:
    validate_start_letter("abc123")
    with raises(InvalidReportFilter):
        validate_start_letter("123abc")


def test_validate_include_number() -> None:
    validate_include_number("abc123")
    with raises(InvalidReportFilter):
        validate_include_number("abcdef")


def test_validate_include_lowercase() -> None:
    validate_include_lowercase("abc123")
    with raises(InvalidReportFilter):
        validate_include_lowercase("ABC123")


def test_validate_include_uppercase() -> None:
    validate_include_uppercase("aBc123")
    with raises(InvalidReportFilter):
        validate_include_uppercase("abc123")


def test_validate_markdown() -> None:
    assert validate_markdown(text="<h1>Heading level\t 1</h1>")
    assert validate_markdown(text="ftp://user:password@ftp.example.com:21/path/to/file")
    with raises(InvalidMarkdown):
        validate_markdown(text="<span>Example Text</span>")


@freeze_time("2023-09-30")
async def test_validate_outdated_repository() -> None:
    vuln = VulnerabilityFaker(
        created_date=datetime.fromisoformat("2020-09-09T21:01:26+00:00"),
        id="fba2e223-5140-4b1f-bdd9-5d1685f477d3",
        state=VulnerabilityStateFaker(
            modified_date=datetime.fromisoformat("2023-09-30T10:00:00+00:00"),
            commit="aaaaa",
        ),
        type=VulnerabilityType.LINES,
    )
    validate_outdated_repository(
        vulnerability=vuln,
        last_commit_hash="aaaaa",
        cloning_date=datetime.fromisoformat("2023-09-30T15:00:00+00:00"),
    )
    with raises(OutdatedRepository):
        validate_outdated_repository(
            vulnerability=vuln,
            last_commit_hash="aaaaa",
            cloning_date=datetime.fromisoformat("2023-09-25T15:00:00+00:00"),
        )


def test_validate_no_duplicate_drafts() -> None:
    test_finding = (
        FindingFaker(
            id="3c475384-834c-47b0-ac71-a41a022e401c",
            group_name="group1",
            title="001. SQL injection - C Sharp SQL API",
        ),
    )

    assert validate_no_duplicate_drafts(new_title="New Title", drafts=(), findings=test_finding)
    assert validate_no_duplicate_drafts(new_title="New Title", drafts=test_finding, findings=())

    with raises(DuplicateDraftFound):
        validate_no_duplicate_drafts(
            new_title="001. SQL injection - C Sharp SQL API",
            drafts=(),
            findings=test_finding,
        )
    with raises(DuplicateDraftFound):
        validate_no_duplicate_drafts(
            new_title="001. SQL injection - C Sharp SQL API",
            drafts=test_finding,
            findings=(),
        )


def test_validate_chart_field() -> None:
    validate_chart_field("content", "field")
    with raises(InvalidChar):
        validate_chart_field("content!", "field")


def test_check_and_set_min_time_to_remediate() -> None:
    assert check_and_set_min_time_to_remediate(None) is None
    assert check_and_set_min_time_to_remediate(1) == 1
    assert check_and_set_min_time_to_remediate("10") == 10
    with raises(InvalidMinTimeToRemediate):
        check_and_set_min_time_to_remediate(0)
    with raises(InvalidMinTimeToRemediate):
        check_and_set_min_time_to_remediate(-5)
    with raises(InvalidMinTimeToRemediate):
        check_and_set_min_time_to_remediate("-5")
    with raises(InvalidMinTimeToRemediate):
        check_and_set_min_time_to_remediate("abc")


async def raise_aws_role(role: str, external_id: str | None) -> None | list:
    if role == "arn:aws:iam::123456789098:role/TestRole" and external_id == "TestExternalId":
        raise InvalidAWSRoleTrustPolicy()
    return []


@mocks(others=[Mock(validations, "fetch_temp_aws_creds", "function", raise_aws_role)])
async def test_validate_aws_role() -> None:
    role = "arn:aws:iam::123456789098:role/TestRole"
    external_id = "TestExternalId"
    with raises(InvalidAWSRoleTrustPolicy):
        await validate_aws_role(role, external_id)
