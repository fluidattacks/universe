from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.group_access.types import (
    GroupAccess,
)


async def handle_domain_user_managers(
    loaders: Dataloaders,
    domain: str,
    domain_access: list[GroupAccess],
) -> list[str]:
    domain_groups_names = list(set(access.group_name for access in domain_access))
    domain_groups = await loaders.group.load_many(domain_groups_names)
    domain_orgs_ids = list(set(group.organization_id for group in domain_groups if group))
    domain_orgs_access = await loaders.organization_stakeholders_access.load_many(domain_orgs_ids)
    return list(
        set(
            access.email
            for org_access in domain_orgs_access
            for access in org_access
            if access.state.role == "organization_manager" and access.email.endswith(domain)
        ),
    )


async def get_managers_company_domain(
    loaders: Dataloaders,
    user_email: str,
) -> list[str] | None:
    domain = user_email.split("@")[1]
    domain_access = await loaders.domain_group_access.load(
        user_email,
    )

    if domain_access:
        domain_user_managers = await handle_domain_user_managers(
            loaders=loaders,
            domain=domain,
            domain_access=domain_access,
        )

        if domain_user_managers:
            return domain_user_managers

    return None
