from datetime import datetime

from integrates.custom_exceptions import CustomBaseException, InactiveRoot, InvalidRootType
from integrates.custom_utils.roots import (
    get_active_git_root,
    get_active_git_roots,
    get_root,
    get_root_base_url,
    get_root_vulnerabilities,
    get_unsolved_events_by_root,
    sanitize_cloning_error_message,
)
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.events.enums import EventType
from integrates.db_model.roots.enums import RootStatus, RootType
from integrates.db_model.roots.types import GitRoot, URLRoot, URLRootState
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    EventFaker,
    FindingFaker,
    GitRootFaker,
    GitRootStateFaker,
    GroupFaker,
    OrganizationFaker,
    UrlRootFaker,
    VulnerabilityFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

ORG_ID = "ORG#dcb069b4-dd78-2180-460e-05f8b353b7bc"
ROOT_ID = "root1"


@parametrize(
    args=[
        "url_root",
        "expected",
    ],
    cases=[
        [
            URLRoot(
                created_by="admin@gmail.com",
                created_date=datetime.fromisoformat("2020-11-19T13:37:10+00:00"),
                group_name="group1",
                id="765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a",
                organization_name="orgtest",
                state=URLRootState(
                    host="app.fluidattacks.com",
                    modified_by="admin@gmail.com",
                    modified_date=datetime.fromisoformat("2020-11-19T13:37:10+00:00"),
                    nickname="test_nickname_3",
                    other=None,
                    path="/",
                    port="8080",
                    protocol="HTTPS",
                    reason=None,
                    status=RootStatus.INACTIVE,
                ),
                type=RootType.URL,
            ),
            "https://app.fluidattacks.com:8080",
        ],
        [
            URLRoot(
                created_by="jdoe@fluidattacks.com",
                created_date=datetime.fromisoformat("2020-11-19T13:45:55+00:00"),
                group_name="oneshottest",
                id="8493c82f-2860-4902-86fa-75b0fef76034",
                organization_name="okada",
                state=URLRootState(
                    host="app.fluidattacks.com",
                    modified_by="jdoe@fluidattacks.com",
                    modified_date=datetime.fromisoformat("2020-11-19T13:45:55+00:00"),
                    nickname="url_root_1",
                    other=None,
                    path="/path/to/",
                    port="443",
                    protocol="HTTPS",
                    reason=None,
                    status=RootStatus.ACTIVE,
                    query=None,
                ),
                type=RootType.URL,
            ),
            "https://app.fluidattacks.com:443/path/to",
        ],
        [
            URLRoot(
                created_by="jdoe@fluidattacks.com",
                created_date=datetime.fromisoformat("2020-11-19T13:45:55+00:00"),
                group_name="oneshottest",
                id="8493c82f-2860-4902-86fa-75b0fef76034",
                organization_name="okada",
                state=URLRootState(
                    host="app.fluidattacks.com",
                    modified_by="jdoe@fluidattacks.com",
                    modified_date=datetime.fromisoformat("2020-11-19T13:45:55+00:00"),
                    nickname="url_root_1",
                    other=None,
                    path="/path/to",
                    port="443",
                    protocol="FILE",
                    reason=None,
                    status=RootStatus.ACTIVE,
                    query=None,
                ),
                type=RootType.URL,
            ),
            "file://app.fluidattacks.com/path/to",
        ],
    ],
)
async def test_base_url(url_root: URLRoot, expected: str) -> None:
    assert get_root_base_url(url_root) == expected


@parametrize(
    args=["clear_loader"],
    cases=[
        [True],
        [False],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name="group1", organization_id=ORG_ID)],
            organizations=[
                OrganizationFaker(id=ORG_ID, name="org1"),
            ],
            roots=[GitRootFaker(group_name="group1", id=ROOT_ID, organization_name="org1")],
        ),
    )
)
async def test_get_root(clear_loader: bool) -> None:
    loaders: Dataloaders = get_new_context()

    root = await get_root(loaders, ROOT_ID, "group1", clear_loader)

    assert root
    assert root.id == ROOT_ID


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name="group1", organization_id=ORG_ID)],
            organizations=[
                OrganizationFaker(id=ORG_ID, name="org1"),
            ],
            roots=[GitRootFaker(group_name="group1", organization_name="org1")],
        ),
    )
)
async def test_get_root_not_found() -> None:
    loaders: Dataloaders = get_new_context()

    with raises(CustomBaseException):
        await get_root(loaders, "rootfail", "group1")


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name="group1", organization_id=ORG_ID)],
            organizations=[
                OrganizationFaker(id=ORG_ID, name="org1"),
            ],
            roots=[GitRootFaker(group_name="group1", id=ROOT_ID, organization_name="org1")],
        ),
    )
)
async def test_get_active_git_root() -> None:
    loaders: Dataloaders = get_new_context()

    active_git_root: GitRoot = await get_active_git_root(loaders, ROOT_ID, "group1")

    assert active_git_root
    assert active_git_root.id == ROOT_ID
    assert active_git_root.state.nickname == "test-root"
    assert active_git_root.state.status == RootStatus.ACTIVE


@parametrize(
    args=["root_id", "group_name"],
    cases=[
        [ROOT_ID, "group1"],
        ["urlid", "group2"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(name="group1", organization_id=ORG_ID),
                GroupFaker(name="group2", organization_id=ORG_ID),
            ],
            organizations=[OrganizationFaker(id=ORG_ID)],
            roots=[
                UrlRootFaker(group_name="group2", root_id="urlid"),
                GitRootFaker(
                    group_name="group1",
                    id=ROOT_ID,
                    state=GitRootStateFaker(status=RootStatus.INACTIVE),
                ),
            ],
        ),
    )
)
async def test_get_active_git_root_invalid(root_id: str, group_name: str) -> None:
    loaders: Dataloaders = get_new_context()

    with raises((InactiveRoot, InvalidRootType)):
        await get_active_git_root(loaders, root_id, group_name)


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name="group1", organization_id=ORG_ID)],
            organizations=[OrganizationFaker(id=ORG_ID, name="org1")],
            roots=[GitRootFaker(group_name="group1", id=ROOT_ID, organization_name="org1")],
            findings=[FindingFaker(group_name="group1", id="find1")],
            vulnerabilities=[
                VulnerabilityFaker(
                    group_name="group1",
                    root_id=ROOT_ID,
                    finding_id="find1",
                    id="vuln1",
                ),
                VulnerabilityFaker(
                    group_name="group1",
                    root_id=ROOT_ID,
                    finding_id="find1",
                    id="vuln2",
                ),
                VulnerabilityFaker(
                    group_name="group1",
                    root_id="root2",
                    id="vuln3",
                ),
            ],
        ),
    )
)
async def test_get_root_vulnerabilities() -> None:
    assert len(await get_root_vulnerabilities(ROOT_ID)) == 2


@parametrize(
    args=[
        "group_name",
        "specific_type",
        "expected",
    ],
    cases=[
        ["group1", None, 2],
        ["group2", EventType.CLONING_ISSUES, 1],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(name="group1", organization_id=ORG_ID),
                GroupFaker(name="group2", organization_id=ORG_ID),
            ],
            organizations=[OrganizationFaker(id=ORG_ID, name="org1")],
            roots=[GitRootFaker(group_name="group1", id=ROOT_ID, organization_name="org1")],
            findings=[FindingFaker(group_name="group1", id="find1")],
            events=[
                EventFaker(group_name="group1", root_id=ROOT_ID, id="event1"),
                EventFaker(group_name="group1", root_id=ROOT_ID, id="event2"),
                EventFaker(group_name="group2", id="event3", root_id="root2"),
                EventFaker(
                    group_name="group2", type=EventType.CLONING_ISSUES, id="event4", root_id="root2"
                ),
            ],
        ),
    )
)
async def test_get_unsolved_events_by_root(
    group_name: str, specific_type: EventType | None, expected: int
) -> None:
    loaders: Dataloaders = get_new_context()
    events = await get_unsolved_events_by_root(
        loaders=loaders, group_name=group_name, specific_type=specific_type
    )
    assert sum(len(event) for event in events.values()) == expected


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name="group1", organization_id=ORG_ID)],
            organizations=[OrganizationFaker(id=ORG_ID, name="org1")],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    state=GitRootStateFaker(
                        url="https://test.com/repo.git",
                        branch="main",
                        nickname="original",
                    ),
                ),
                GitRootFaker(
                    id="root2",
                    group_name="group1",
                    state=GitRootStateFaker(
                        url="https://test.com/repo2.git",
                        branch="main",
                        nickname="original",
                    ),
                ),
            ],
        ),
    )
)
async def test_get_active_git_roots() -> None:
    loaders: Dataloaders = get_new_context()

    active_git_roots: list[GitRoot] = await get_active_git_roots(loaders, "group1")

    assert len(active_git_roots) == 2


@parametrize(
    args=[
        "message",
        "expected",
    ],
    cases=[
        ["", ""],
        ["\n\n\nerror        ", "error"],
        [
            "Cloning into '/tmp/integrates_clone_acme_h2i3aeey'..."
            "fatal: unable to access "
            "'https://username:password@github.com/username/repository.git': "
            "Recv failure: Connection reset by peer",
            "fatal: unable to access MASKED_URL: Recv failure: Connection reset by peer",
        ],
        [
            "fatal: unable to access 'https://token@dev.azure.com': terminal prompts disabled",
            "fatal: unable to access MASKED_URL: terminal prompts disabled",
        ],
        [
            "fatal: unable to access 'ssh://token@dev.azure.com': terminal prompts disabled",
            "fatal: unable to access MASKED_URL: terminal prompts disabled",
        ],
        [
            "fatal: unable to access 'codecommit::us-east-1://Repo': terminal prompts disabled",
            "fatal: unable to access MASKED_URL: terminal prompts disabled",
        ],
    ],
)
async def test_sanitize_cloning_error_message(message: str, expected: str) -> None:
    assert sanitize_cloning_error_message(message) == expected
