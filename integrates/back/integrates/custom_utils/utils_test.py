from typing import (
    Any,
)

from integrates.custom_exceptions import (
    InvalidPort,
    InvalidProtocol,
    InvalidUrl,
)
from integrates.custom_utils.utils import (
    camelcase_to_snakecase,
    codecommitparser,
    get_missing_dependency,
    httpsparser,
    list_to_dict,
    replace_all,
    sshparser,
)
from integrates.testing.utils import parametrize, raises


@parametrize(
    args=["url", "expected_output"],
    cases=[
        ["https://github.com/fluidattacks/makes", {"scheme": "https", "host": "github.com"}],
        ["http://[2001:db8::]/", {"scheme": "http", "host": "[2001:db8::]"}],
    ],
)
def test_httpsparser(url: str, expected_output: dict[str, str]) -> None:
    result = httpsparser(url)
    assert result.scheme == expected_output["scheme"]
    assert result.host == expected_output["host"]


@parametrize(
    args=["url", "expected_output"],
    cases=[
        [
            "ssh://git@gitlab.com:fluidattacks/universe.git",
            {"scheme": "ssh", "host": "gitlab.com:fluidattacks"},
        ],
        [
            "ssh://user@ssh.dev.azure.com:v3/fluidattacks/universe",
            {"scheme": "ssh", "host": "ssh.dev.azure.com:v3"},
        ],
    ],
)
def test_sshparser(url: str, expected_output: dict[str, str]) -> None:
    result = sshparser(url)
    assert result.scheme == expected_output["scheme"]
    assert result.host == expected_output["host"]


@parametrize(
    args=["url", "expected_output"],
    cases=[
        [
            "codecommit://NewRepo",
            {"scheme": "codecommit", "host": "NewRepo"},
        ],
        [
            "codecommit::us-east-1://TestProfile@NewRepo",
            {"scheme": "codecommit::us-east-1", "host": "NewRepo"},
        ],
    ],
)
def test_codecommitparser(url: str, expected_output: dict[str, str]) -> None:
    result = codecommitparser(url)
    assert result.scheme == expected_output["scheme"]
    assert result.host == expected_output["host"]


@parametrize(
    args=["url"],
    cases=[
        [""],
        [" -- "],
        ["Non an URL"],
        ["test.com"],
        ["ftp://www.test.com/"],
    ],
)
def test_httpsparser_fail_1(url: str) -> None:
    with raises(InvalidProtocol):
        httpsparser(url)


def test_httpsparser_fail_2() -> None:
    with raises(InvalidPort):
        httpsparser("https://www.test.com:79999/")


@parametrize(
    args=["url"],
    cases=[
        ["https://- atk(S); '':80/#"],
        ["http://0.0.0.0:1234"],
        ["http://127.0.0.1:1234"],
        ["http://localhost:1234"],
        ["https://www.test.com?key=value"],
        ["http://[::]:8000/"],
        ["http://[::1]/"],
    ],
)
def test_httpsparser_fail_3(url: str) -> None:
    with raises(InvalidUrl):
        httpsparser(url)


@parametrize(
    args=["text", "dictionary", "expected_output"],
    cases=[
        [
            "replaced",
            {"a": "a", "b": "b", "c": "c"},
            "replaced",
        ],
        [
            "replaced",
            {"r": "d", "p": "sp", "de": "di"},
            "displaced",
        ],
    ],
)
def test_replace_all(text: str, dictionary: dict[str, str], expected_output: str) -> None:
    assert replace_all(text=text, dic=dictionary) == expected_output


@parametrize(
    args=["keys", "values", "expected_result"],
    cases=[
        [
            ["item", "item2", "item3"],
            ["hi", "this is a", "item"],
            {"item": "hi", "item2": "this is a", "item3": "item"},
        ],
        [
            ["item", "item2"],
            ["hi", "this is a", "item"],
            {"item": "hi", "item2": "this is a", 2: "item"},
        ],
        [
            ["item", "item2", "item3"],
            ["hi", "this is a"],
            {"item": "hi", "item2": "this is a", "item3": ""},
        ],
    ],
)
def test_list_to_dict(
    keys: list[Any],
    values: list[Any],
    expected_result: dict[str, str],
) -> None:
    assert list_to_dict(keys, values) == expected_result


@parametrize(
    args=["test_case", "expected_output"],
    cases=[
        ["thisIsATest", "this_is_a_test"],
        ["thi2%s0ter_Cas3", "thi2%s0ter__cas3"],
    ],
)
def test_camelcase_to_snakecase(test_case: str, expected_output: str) -> None:
    assert camelcase_to_snakecase(test_case) == expected_output


@parametrize(
    args=["where", "expect_dependency"],
    cases=[
        [
            "info (missing dependency: example_dependency)",
            "example_dependency",
        ],
        ["Some other error", ""],
        ["", ""],
    ],
)
def test_get_missing_dependency(where: str, expect_dependency: str) -> None:
    result = get_missing_dependency(where)
    assert result == expect_dependency
