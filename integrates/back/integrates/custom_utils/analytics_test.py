from integrates.custom_utils.analytics import (
    is_decimal,
)
from integrates.testing.utils import parametrize


@parametrize(
    args=["num", "expect_result"],
    cases=[["1.5", True], ["abc", False], ["1.5abc", False], ["1246.57543", True]],
)
async def test_is_decimal(num: str, expect_result: bool) -> None:
    assert is_decimal(num) == expect_result
