from datetime import datetime

from integrates.custom_utils.environments_validations import (
    validate_aws_cspm_permissions,
    validate_role_status,
)
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.roots.enums import (
    RootEnvironmentUrlStateStatus,
    RootEnvironmentUrlType,
)
from integrates.db_model.roots.types import (
    RootEnvironmentUrl,
    RootEnvironmentUrlState,
    Secret,
    SecretState,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GitRootFaker,
    GitRootStateFaker,
    GroupFaker,
    OrganizationFaker,
    OrganizationStateFaker,
)
from integrates.testing.mocks import mocks


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(
                    id="org1",
                    state=OrganizationStateFaker(
                        aws_external_id="be991c12-9b68-4312-85c4-16ce5fa2fca0",
                    ),
                ),
            ],
            groups=[GroupFaker(name="group1", organization_id="org1")],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    state=GitRootStateFaker(nickname="testroot"),
                ),
            ],
        ),
    )
)
async def test_validate_aws_cspm_permissions_no_secrets() -> None:
    parent = RootEnvironmentUrl(
        url="Dockerfile",
        id="root_env_id_1",
        root_id="root1",
        group_name="group1",
        state=RootEnvironmentUrlState(
            modified_by="group_manager@gmail.com",
            modified_date=datetime.fromisoformat("2024-01-19T13:37:10+00:00"),
            url_type=RootEnvironmentUrlType.APK,
            status=RootEnvironmentUrlStateStatus.CREATED,
        ),
    )
    secrets_list = [
        Secret(
            key="Test secret admin environment",
            value="Test value",
            created_at=datetime.fromisoformat("2020-05-20T22:00:00+00:00"),
            state=SecretState(
                owner="admin@gmail.com",
                description="Test description",
                modified_by="admin@gmail.com",
                modified_date=datetime.fromisoformat("2020-05-20T22:00:00+00:00"),
            ),
        ),
    ]
    secrets: dict[str, str] = {secret.key.lower(): secret.value for secret in secrets_list}
    is_role_status_ok = await validate_aws_cspm_permissions(parent.url, secrets)
    assert not is_role_status_ok


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(
                    id="org1",
                    state=OrganizationStateFaker(
                        aws_external_id="be991c12-9b68-4312-85c4-16ce5fa2fca0",
                    ),
                ),
            ],
            groups=[GroupFaker(name="group1", organization_id="org1")],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    state=GitRootStateFaker(nickname="testroot"),
                ),
            ],
        ),
    )
)
async def test_validate_role_status_no_cloud() -> None:
    loaders: Dataloaders = get_new_context()
    parent = RootEnvironmentUrl(
        url="Dockerfile",
        id="root_env_id_1",
        root_id="root1",
        group_name="group1",
        state=RootEnvironmentUrlState(
            modified_by="group_manager@gmail.com",
            modified_date=datetime.fromisoformat("2024-01-19T13:37:10+00:00"),
            url_type=RootEnvironmentUrlType.APK,
            status=RootEnvironmentUrlStateStatus.CREATED,
        ),
    )
    secrets_list = [
        Secret(
            key="Test secret admin environment",
            value="Test value",
            created_at=datetime.fromisoformat("2020-05-20T22:00:00+00:00"),
            state=SecretState(
                owner="admin@gmail.com",
                description="Test description",
                modified_by="admin@gmail.com",
                modified_date=datetime.fromisoformat("2020-05-20T22:00:00+00:00"),
            ),
        ),
    ]
    secrets: dict[str, str] = {secret.key.lower(): secret.value for secret in secrets_list}
    is_role_status_ok = await validate_role_status(
        loaders, parent.url, "KUBERNETES", parent.group_name, secrets
    )
    assert not is_role_status_ok


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(
                    id="org1",
                    state=OrganizationStateFaker(
                        aws_external_id="be991c12-9b68-4312-85c4-16ce5fa2fca0",
                    ),
                ),
            ],
            groups=[GroupFaker(name="group1", organization_id="org1")],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    state=GitRootStateFaker(nickname="testroot"),
                ),
            ],
        ),
    )
)
async def test_validate_role_status_gcp() -> None:
    loaders: Dataloaders = get_new_context()
    parent = RootEnvironmentUrl(
        url="Test",
        id="root_env_id_2",
        root_id="root1",
        group_name="group1",
        state=RootEnvironmentUrlState(
            modified_by="group_manager@gmail.com",
            modified_date=datetime.fromisoformat("2024-01-19T13:37:10+00:00"),
            url_type=RootEnvironmentUrlType.CSPM,
            status=RootEnvironmentUrlStateStatus.CREATED,
        ),
    )
    secrets_list = [
        Secret(
            key="Test secret admin environment",
            value="Test value",
            created_at=datetime.fromisoformat("2020-05-20T22:00:00+00:00"),
            state=SecretState(
                owner="admin@gmail.com",
                description="Test description",
                modified_by="admin@gmail.com",
                modified_date=datetime.fromisoformat("2020-05-20T22:00:00+00:00"),
            ),
        ),
    ]
    secrets: dict[str, str] = {secret.key.lower(): secret.value for secret in secrets_list}
    is_role_status_ok = await validate_role_status(
        loaders, parent.url, "GCP", parent.group_name, secrets
    )
    assert not is_role_status_ok
