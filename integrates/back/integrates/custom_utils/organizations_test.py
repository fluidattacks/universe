from typing import (
    NamedTuple,
)

from integrates.custom_utils.organizations import (
    get_organization_country,
)


class Context(NamedTuple):
    headers: dict[str, str]


def test_get_organization_country() -> None:
    inputs = [Context(headers={"cf-ipcountry": "CO"}), Context(headers={})]
    outputs: list[str] = ["Colombia", "undefined"]
    for index, context in enumerate(inputs):
        assert outputs[index] == get_organization_country(context)
