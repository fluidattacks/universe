from integrates.custom_exceptions import (
    InvalidFileName,
)
from integrates.custom_utils.findings import validate_evidence_name
from integrates.testing.utils import parametrize, raises


@parametrize(
    args=[
        "organization_name",
        "group_name",
        "filename",
        "invalid_file_name",
        "invalid_file_name_length",
    ],
    cases=[
        [
            "okada",
            "unittesting",
            "okada-unittesting-records123.csv",
            "organization-unittesting-records123.csv",
            "okada-unittesting-records.csv",
        ],
    ],
)
def test_validate_evidence_name(
    organization_name: str,
    group_name: str,
    filename: str,
    invalid_file_name: str,
    invalid_file_name_length: str,
) -> None:
    validate_evidence_name(
        organization_name=organization_name,
        group_name=group_name,
        filename=filename,
    )
    with raises(InvalidFileName):
        validate_evidence_name(
            organization_name=organization_name,
            group_name=group_name,
            filename=invalid_file_name,
        )
    with raises(InvalidFileName):
        validate_evidence_name(
            organization_name=organization_name,
            group_name=group_name,
            filename=invalid_file_name_length,
        )
