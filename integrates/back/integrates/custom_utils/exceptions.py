import asyncio
from typing import (
    TypeVar,
)

from aiohttp.client_exceptions import (
    ClientConnectorError,
    ClientOSError,
    ClientPayloadError,
    ServerDisconnectedError,
    ServerTimeoutError,
)
from botocore.exceptions import (
    ClientError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
)

from integrates.custom_exceptions import (
    CustomBaseException,
)
from integrates.dynamodb.exceptions import (
    UnavailabilityError,
)

T = TypeVar("T")

NETWORK_ERRORS = (
    asyncio.TimeoutError,
    ClientConnectorError,
    ClientError,
    ClientOSError,
    ClientPayloadError,
    ConnectionError,
    ConnectionResetError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
    ServerDisconnectedError,
    ServerTimeoutError,
    UnavailabilityError,
)


def raise_or_return(*, exc: CustomBaseException, raise_exc: bool, value_to_return: T) -> T:
    if raise_exc:
        raise exc
    return value_to_return
