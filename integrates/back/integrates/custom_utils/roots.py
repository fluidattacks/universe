import re
from collections import defaultdict
from collections.abc import Iterable
from datetime import datetime

import aioboto3
from botocore.exceptions import ClientError

from integrates.custom_exceptions import (
    InactiveRoot,
    InvalidAWSRoleTrustPolicy,
    InvalidRootType,
    RootNotFound,
)
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.events.enums import EventType
from integrates.db_model.events.types import Event, GroupEventsRequest
from integrates.db_model.roots.enums import RootStatus
from integrates.db_model.roots.types import GitRoot, Root, RootRequest, Secret, SecretState, URLRoot
from integrates.db_model.vulnerabilities.types import RootVulnerabilitiesRequest, Vulnerability

PAGE_SIZE = 1024
EMAIL_INTEGRATES = "integrates@fluidattacks.com"


def get_root_base_url(root: URLRoot) -> str:
    host_and_port = (
        f"{root.state.host}:{root.state.port}"
        if root.state.port and root.state.protocol != "FILE"
        else root.state.host
    )
    root_url = f"{root.state.protocol.lower()}://{host_and_port}{root.state.path.removesuffix('/')}"
    return root_url


async def get_root(
    loaders: Dataloaders,
    root_id: str,
    group_name: str,
    clear_loader: bool = False,
) -> Root:
    if clear_loader:
        loaders.root.clear(RootRequest(group_name, root_id))

    root = await loaders.root.load(RootRequest(group_name, root_id))
    if not root:
        raise RootNotFound()

    return root


async def get_active_git_root(loaders: Dataloaders, root_id: str, group_name: str) -> GitRoot:
    root = await get_root(loaders, root_id, group_name)
    if not isinstance(root, GitRoot):
        raise InvalidRootType()
    if root.state.status != RootStatus.ACTIVE:
        raise InactiveRoot()

    return root


def filter_active_git_roots(roots: Iterable[Root]) -> list[GitRoot]:
    return [
        root
        for root in roots
        if isinstance(root, GitRoot) and root.state.status == RootStatus.ACTIVE
    ]


async def get_active_git_roots(loaders: Dataloaders, group_name: str) -> list[GitRoot]:
    return filter_active_git_roots(await loaders.group_roots.load(group_name))


async def fetch_temp_aws_creds(role: str, external_id: str | None) -> list[Secret]:
    aws_creds = []
    if external_id is None:
        external_id = ""
    try:
        async with aioboto3.Session().client(service_name="sts") as sts_client:
            response = await sts_client.assume_role(
                ExternalId=external_id,
                RoleArn=role,
                RoleSessionName="FluidAttacksRoleVerification",
            )
            aws_creds.append(
                Secret(
                    key="AWS_ACCESS_KEY_ID",
                    value=response["Credentials"]["AccessKeyId"],
                    created_at=datetime.now(),
                    state=SecretState(
                        owner=EMAIL_INTEGRATES,
                        modified_by=EMAIL_INTEGRATES,
                        modified_date=datetime.now(),
                    ),
                ),
            )
            aws_creds.append(
                Secret(
                    key="AWS_SECRET_ACCESS_KEY",
                    value=response["Credentials"]["SecretAccessKey"],
                    created_at=datetime.now(),
                    state=SecretState(
                        owner=EMAIL_INTEGRATES,
                        modified_by=EMAIL_INTEGRATES,
                        modified_date=datetime.now(),
                    ),
                ),
            )
            aws_creds.append(
                Secret(
                    key="AWS_SESSION_TOKEN",
                    value=response["Credentials"]["SessionToken"],
                    created_at=datetime.now(),
                    state=SecretState(
                        owner=EMAIL_INTEGRATES,
                        modified_by=EMAIL_INTEGRATES,
                        modified_date=datetime.now(),
                    ),
                ),
            )
    except ClientError as exc:
        raise InvalidAWSRoleTrustPolicy() from exc

    return aws_creds


async def get_unsolved_events_by_root(
    loaders: Dataloaders,
    group_name: str,
    *,
    specific_type: EventType | None = None,
) -> dict[str, tuple[Event, ...]]:
    unsolved_events_by_root: defaultdict[str | None, list[Event]] = defaultdict(list[Event])
    unsolved_events = await loaders.group_events.load(
        GroupEventsRequest(group_name=group_name, is_solved=False),
    )
    if specific_type:
        unsolved_events = [event for event in unsolved_events if event.type == specific_type]
    for event in unsolved_events:
        unsolved_events_by_root[event.root_id].append(event)
    return {
        root_id: tuple(events) for root_id, events in unsolved_events_by_root.items() if root_id
    }


async def get_root_unsolved_cloning_issues_events(
    loaders: Dataloaders, group_name: str, root_id: str
) -> list[Event]:
    group_unsolved_events = await loaders.group_events.load(
        GroupEventsRequest(group_name=group_name, is_solved=False)
    )

    return [
        event
        for event in group_unsolved_events
        if event.root_id == root_id and event.type == EventType.CLONING_ISSUES
    ]


async def get_root_vulnerabilities(root_id: str) -> list[Vulnerability]:
    loaders = get_new_context()
    after = None
    root_vulnerabilities: list[Vulnerability] = []
    while True:
        connection = await loaders.root_vulnerabilities_connection.load(
            RootVulnerabilitiesRequest(root_id=root_id, after=after, first=100),
        )
        if connection is None:
            break
        root_vulnerabilities.extend([edge.node for edge in connection.edges])
        if not connection.page_info.has_next_page:
            break
        after = connection.page_info.end_cursor
    return root_vulnerabilities


def get_root_ids_by_nicknames(
    group_roots: Iterable[Root],
    only_git_roots: bool = False,
) -> dict[str, str]:
    """
    Returns a dict with root nicknames as keys and root ids as values.
    Active roots overrides inactive roots if they have the same name.

    Args:
        group_roots (Iterable[Root]): list with all roots.
        only_git_roots (bool, optional): Filter by GitRoots.

    """
    sorted_active_roots = sorted(
        [root for root in group_roots if root.state.status == RootStatus.ACTIVE],
        key=lambda root: root.state.modified_date,
        reverse=False,
    )
    sorted_inactive_roots = sorted(
        [root for root in group_roots if root.state.status == RootStatus.INACTIVE],
        key=lambda root: root.state.modified_date,
        reverse=False,
    )
    root_ids: dict[str, str] = {}
    for root in sorted_inactive_roots + sorted_active_roots:
        if not only_git_roots or isinstance(root, GitRoot):
            root_ids[root.state.nickname] = root.id

    return root_ids


async def get_root_id(loaders: Dataloaders, group_name: str, nickname: str) -> str:
    group_roots = await loaders.group_roots.load(group_name)
    root_ids_by_nicknames = get_root_ids_by_nicknames(
        group_roots=group_roots,
        only_git_roots=False,
    )

    return root_ids_by_nicknames.get(nickname, "")


def sanitize_cloning_error_message(message: str) -> str:
    masked_url = re.sub(r"\'(https?:\/\/|ssh:\/\/|codecommit::).*\'", "MASKED_URL", message)
    sanitized = re.sub(r"Cloning into \'.*\'.{3}", "", masked_url).strip()

    return sanitized
