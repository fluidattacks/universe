from integrates.custom_utils.registered_domain import (
    get_managers_company_domain,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationAccessFaker,
    OrganizationAccessStateFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize


@parametrize(
    args=["email", "expect_result"],
    cases=[
        ["johndoe@fluidattacks.com", True],
        ["johndoe@noexistingdomain.com", False],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="ORG#org1")],
            stakeholders=[
                StakeholderFaker(
                    email="organization_manager@fluidattacks.com", role="organization_manager"
                ),
            ],
            groups=[GroupFaker(name="group1", organization_id="ORG#org1")],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id="ORG#org1",
                    email="organization_manager@fluidattacks.com",
                    state=OrganizationAccessStateFaker(role="organization_manager"),
                ),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name="group1",
                    email="organization_manager@fluidattacks.com",
                    state=GroupAccessStateFaker(has_access=True, role="organization_manager"),
                    domain="fluidattacks.com",
                ),
            ],
        ),
    )
)
async def test_get_registered_domain(email: str, expect_result: bool) -> None:
    loaders = get_new_context()
    result = bool(await get_managers_company_domain(loaders=loaders, user_email=email))
    assert result == expect_result
