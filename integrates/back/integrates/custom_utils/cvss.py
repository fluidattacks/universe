import re
from collections.abc import (
    Iterable,
)
from datetime import (
    datetime,
)
from decimal import (
    Decimal,
)
from enum import (
    Enum,
)
from typing import (
    NamedTuple,
)

from cvss import (
    CVSS3,
    CVSS4,
    CVSS3Error,
    CVSS4Error,
)

from integrates.class_types.types import (
    Item,
)
from integrates.custom_exceptions import (
    InvalidCVSS3VectorString,
    InvalidCVSS4VectorString,
    InvalidCVSSVectorString,
    InvalidSeverityCweIds,
)
from integrates.db_model.types import (
    SeverityScore,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
)
from integrates.db_model.vulnerabilities.types import (
    VulnerabilityState,
)


# CVSS 4.0 Temporal metrics
class Exploitability(Enum):
    U: Decimal = Decimal("0.91")
    P: Decimal = Decimal("0.94")
    A: Decimal = Decimal("1.00")
    X: Decimal = Decimal("1.00")


# Custom classes
class CVSSFExposureByTimeRange(NamedTuple):
    low: Decimal
    medium: Decimal
    high: Decimal
    critical: Decimal


def get_severity_level(severity: Decimal) -> str:
    """
    Qualitative severity rating scale as defined in
    https://www.first.org/cvss/v3.1/specification-document section 5.
    """
    if severity < 4:
        return "low"
    if 4 <= severity < 7:
        return "medium"
    if 7 <= severity < 9:
        return "high"

    return "critical"


def get_cvssf_score(temporal_score: Decimal) -> Decimal:
    return Decimal(pow(Decimal("4.0"), temporal_score - Decimal("4.0"))).quantize(Decimal("0.001"))


def validate_cvss_vector(vector_string: str) -> None:
    try:
        CVSS3(vector_string)
    except CVSS3Error as ex:
        raise InvalidCVSS3VectorString.new() from ex


def parse_cwe_ids(raw_list: Iterable[str] | None) -> list[str] | None:
    if not raw_list:
        return None

    pattern = r"^CWE-\d{1,4}$"
    if not all(re.match(pattern, id) for id in raw_list):
        raise InvalidSeverityCweIds()

    return sorted(list(set(raw_list)))


def get_severity_score_from_cvss_vector(vector_v3: str, vector_v4: str) -> SeverityScore:
    try:
        cvss3 = CVSS3(vector_v3)
        base_score = cvss3.base_score
        temporal_score = cvss3.temporal_score
        cvss4 = CVSS4(vector_v4)

        return SeverityScore(
            base_score=base_score,
            temporal_score=temporal_score,
            cvss_v3=cvss3.clean_vector(),
            cvssf=get_cvssf_score(temporal_score),
            cvss_v4=cvss4.clean_vector(),
            threat_score=Decimal(str(cvss4.base_score)) if cvss4.base_score else Decimal("0"),
            cvssf_v4=get_cvssf_score(Decimal(str(cvss4.base_score)))
            if cvss4.base_score
            else Decimal("0"),
        )
    except CVSS3Error as ex:
        if not vector_v3:
            raise InvalidCVSSVectorString(cvss_version="v3.1") from ex
        raise InvalidCVSS3VectorString.new() from ex
    except CVSS4Error as ex:
        if not vector_v4:
            raise InvalidCVSSVectorString(cvss_version="v4.0") from ex
        raise InvalidCVSS4VectorString.new() from ex


def get_criteria_cvss_vector(criteria_vulnerability: Item) -> str:
    base = criteria_vulnerability["score"]["base"]
    temporal = criteria_vulnerability["score"]["temporal"]
    vector_string = (
        f'CVSS:3.1/AV:{base["attack_vector"]}'
        f'/AC:{base["attack_complexity"]}'
        f'/PR:{base["privileges_required"]}'
        f'/UI:{base["user_interaction"]}'
        f'/S:{base["scope"]}'
        f'/C:{base["confidentiality"]}'
        f'/I:{base["integrity"]}'
        f'/A:{base["availability"]}'
        f'/E:{temporal["exploit_code_maturity"]}'
        f'/RL:{temporal["remediation_level"]}'
        f'/RC:{temporal["report_confidence"]}'
    )
    try:
        cvss3 = CVSS3(vector_string)
    except CVSS3Error as ex:
        raise InvalidCVSS3VectorString.new() from ex

    return cvss3.clean_vector()


def get_is_exploitable(cvss_vector: str) -> bool:
    cvss_vector_dict = {
        metric.split(":")[0]: metric.split(":")[1]
        for metric in cvss_vector.split("/")
        if metric not in {"CVSS:4.0"}
    }
    exploitability = (
        Exploitability[cvss_vector_dict["E"]] if cvss_vector_dict.get("E") else Exploitability.X
    )

    return exploitability in {
        Exploitability.A,
        Exploitability.X,
    }


def get_exposed_cvssf(
    historic_state: Iterable[VulnerabilityState],
    severity: Decimal,
    last_day: datetime,
) -> CVSSFExposureByTimeRange:
    states = [
        state for state in historic_state if state.modified_date.timestamp() <= last_day.timestamp()
    ]
    cvssf: Decimal = Decimal("0.0")
    severity_level = get_severity_level(severity)

    if (
        states
        and states[-1].modified_date.timestamp() <= last_day.timestamp()
        and states[-1].status == VulnerabilityStateStatus.VULNERABLE
    ):
        cvssf = get_cvssf_score(severity)

    return CVSSFExposureByTimeRange(
        low=cvssf if severity_level == "low" else Decimal("0.0"),
        medium=cvssf if severity_level == "medium" else Decimal("0.0"),
        high=cvssf if severity_level == "high" else Decimal("0.0"),
        critical=cvssf if severity_level == "critical" else Decimal("0.0"),
    )


def get_exposed_cvssf_by_time_range(
    *,
    vulnerabilities_severity: Iterable[Decimal],
    vulnerabilities_historic_states: Iterable[Iterable[VulnerabilityState]],
    last_day: datetime,
) -> CVSSFExposureByTimeRange:
    exposed_cvssf = [
        get_exposed_cvssf(historic_state, severity, last_day)
        for historic_state, severity in zip(
            vulnerabilities_historic_states,
            vulnerabilities_severity,
            strict=False,
        )
    ]

    return CVSSFExposureByTimeRange(
        low=Decimal(sum(cvssf.low for cvssf in exposed_cvssf)),
        medium=Decimal(sum(cvssf.medium for cvssf in exposed_cvssf)),
        high=Decimal(sum(cvssf.high for cvssf in exposed_cvssf)),
        critical=Decimal(sum(cvssf.critical for cvssf in exposed_cvssf)),
    )


def validate_cvss4_vector(vector_string: str) -> None:
    try:
        CVSS4(vector_string)
    except CVSS4Error as ex:
        raise InvalidCVSS4VectorString.new() from ex


def get_criteria_cvss4_vector(criteria_vulnerability: Item) -> str:
    base = criteria_vulnerability["score_v4"]["base"]
    threat = criteria_vulnerability["score_v4"]["threat"]
    vector_string = (
        "CVSS:4.0/"
        # Exploitability Metrics
        f'AV:{base["attack_vector"]}'
        f'/AC:{base["attack_complexity"]}'
        f'/AT:{base["attack_requirements"]}'
        f'/PR:{base["privileges_required"]}'
        f'/UI:{base["user_interaction"]}'
        # Impact Metrics
        f'/VC:{base["confidentiality_vc"]}'
        f'/VI:{base["integrity_vi"]}'
        f'/VA:{base["availability_va"]}'
        f'/SC:{base["confidentiality_sc"]}'
        f'/SI:{base["integrity_si"]}'
        f'/SA:{base["availability_sa"]}'
        # Threat Metrics
        f'/E:{threat["exploit_maturity"]}'
    )
    try:
        cvss4 = CVSS4(vector_string)
    except CVSS4Error as ex:
        raise InvalidCVSS4VectorString.new() from ex

    return cvss4.clean_vector()
