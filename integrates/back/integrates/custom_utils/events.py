from integrates.custom_exceptions import (
    EventNotFound,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.events.types import (
    Event,
    EventRequest,
)


async def get_event(loaders: Dataloaders, request: EventRequest) -> Event:
    event = await loaders.event.load(request)
    if event is None:
        raise EventNotFound()

    return event
