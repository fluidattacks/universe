import logging

import aiohttp

LOGGER = logging.getLogger(__name__)

FREE_EMAIL_DOMAINS_URL = (
    "https://gist.githubusercontent.com/tbrianjones/5992856/raw/"
    "93213efb652749e226e69884d6c048e595c1280a/"
    "free_email_provider_domains.txt"
)


async def fetch_free_email_domains(session: aiohttp.ClientSession, url: str) -> list:
    async with session.get(url) as response:
        if response.status == 200:
            text = await response.text()
            return text.split("\n")
        LOGGER.error("Couldn't fetch free email provider domains %s", response)
        return []


async def get_free_email_domains() -> list:
    try:
        async with aiohttp.ClientSession() as session:
            return await fetch_free_email_domains(session, FREE_EMAIL_DOMAINS_URL)
    except aiohttp.ClientError as exception:
        LOGGER.error("Couldn't fetch free email provider domains %s", exception)
        return []


async def is_personal_email(user_email: str) -> bool:
    free_email_domains = await get_free_email_domains()
    email_domain = user_email.split("@")[1]
    return email_domain in free_email_domains
