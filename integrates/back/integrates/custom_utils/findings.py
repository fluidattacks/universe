import csv
import io
import itertools
import re
from datetime import (
    datetime,
)

from starlette.datastructures import (
    UploadFile,
)

from integrates.class_types.types import (
    Item,
)
from integrates.custom_exceptions import (
    FindingNotFound,
    InvalidFileName,
    InvalidFileStructure,
    InvalidFindingTitle,
)
from integrates.custom_utils import (
    cvss as cvss_utils,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.custom_utils.criteria import (
    CRITERIA_REQUIREMENTS,
    CRITERIA_VULNERABILITIES,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.enums import (
    TreatmentStatus,
)
from integrates.db_model.findings.enums import (
    FindingStateStatus,
)
from integrates.db_model.findings.types import (
    Finding,
    FindingEvidence,
    FindingVerificationSummary,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.db_model.organization_finding_policies.types import (
    OrgFindingPolicy,
)


async def append_records_to_file(records: list[dict[str, str]], new_file: UploadFile) -> UploadFile:
    header = list(records[0].keys())
    old_records = [list(v) for v in [record.values() for record in records]]
    new_file_records = await new_file.read()
    await new_file.seek(0)
    csv_reader = csv.reader(io.StringIO(new_file_records.decode("utf-8")), skipinitialspace=True)
    new_file_header = next(csv_reader)
    if new_file_header != header:
        raise InvalidFileStructure()
    new_records = list(itertools.islice(csv_reader, 1000))
    new_csv = io.StringIO()
    writer = csv.writer(new_csv)
    writer.writerow(header)
    writer.writerows(old_records)
    writer.writerows(new_records)
    buff = io.BytesIO(new_csv.getvalue().encode("latin1").decode("unicode_escape").encode("utf-8"))
    uploaded_file = UploadFile(filename=new_file.filename, file=io.BytesIO())
    await uploaded_file.write(buff.read())
    await uploaded_file.seek(0)
    return uploaded_file


def is_valid_finding_title(title: str) -> None:
    """
    Validates that new Draft and Finding titles conform to the standard
    format and are present in the whitelist.
    """
    if re.match(r"^\d{3}\. .+", title):
        vulns_info = CRITERIA_VULNERABILITIES
        try:
            vuln_number: str = title[:3]
            expected_vuln_title: str = vulns_info[vuln_number]["en"]["title"]
            if title == f"{vuln_number}. {expected_vuln_title}":
                return
            # Invalid non-standard title
            raise InvalidFindingTitle()
        # Invalid vuln number
        except KeyError as error:
            raise InvalidFindingTitle() from error
    # Invalid format
    raise InvalidFindingTitle()


def is_valid_finding_titles(titles: list[str]) -> bool:
    for title in titles:
        is_valid_finding_title(title=title)
    return True


def get_updated_evidence_date(finding: Finding, evidence: FindingEvidence) -> datetime:
    updated_date = evidence.modified_date
    if is_finding_released(finding):
        release_date = get_finding_release_date(finding)
        if release_date and release_date > evidence.modified_date:
            updated_date = release_date

    return updated_date


def format_evidence(finding: Finding, evidence: FindingEvidence | None) -> Item:
    if evidence is None:
        return {
            "author_email": None,
            "date": None,
            "description": None,
            "is_draft": None,
            "url": None,
        }

    return {
        "author_email": evidence.author_email,
        "date": datetime_utils.get_as_str(get_updated_evidence_date(finding, evidence)),
        "description": evidence.description,
        "is_draft": evidence.is_draft,
        "url": evidence.url,
    }


def get_formatted_evidence(parent: Finding) -> dict[str, Item]:
    return {
        "animation": format_evidence(parent, parent.evidences.animation),
        "evidence_1": format_evidence(parent, parent.evidences.evidence1),
        "evidence_2": format_evidence(parent, parent.evidences.evidence2),
        "evidence_3": format_evidence(parent, parent.evidences.evidence3),
        "evidence_4": format_evidence(parent, parent.evidences.evidence4),
        "evidence_5": format_evidence(parent, parent.evidences.evidence5),
        "exploitation": format_evidence(parent, parent.evidences.exploitation),
    }


def is_deleted(finding: Finding) -> bool:
    return finding.state.status in {
        FindingStateStatus.DELETED,
        FindingStateStatus.MASKED,
    }


def is_verified(
    verification_summary: FindingVerificationSummary,
) -> bool:
    return verification_summary.requested == 0


def filter_findings_not_in_groups(
    groups: list[Group],
    findings: list[Finding],
) -> list[Finding]:
    return list(
        finding
        for finding in findings
        if finding.group_name not in set(group.name for group in groups)
    )


def is_finding_released(finding: Finding) -> bool:
    return bool(
        finding.unreliable_indicators.vulnerabilities_summary.open
        + finding.unreliable_indicators.vulnerabilities_summary.closed,
    )


def get_finding_release_date(finding: Finding) -> datetime | None:
    indicators = finding.unreliable_indicators
    return indicators.oldest_vulnerability_report_date


async def get_group_findings(
    *,
    group_name: str,
    loaders: Dataloaders,
) -> list[Finding]:
    findings = await loaders.group_findings.load(group_name.lower())

    return get_group_released_findings(findings=findings)


def get_group_released_findings(
    *,
    findings: list[Finding],
) -> list[Finding]:
    return [finding for finding in findings if is_finding_released(finding)]


async def get_finding_criteria_cwe_ids(title: str) -> list[str]:
    vulnerability_type = title.split(". ")[0]
    if vulnerability_type not in CRITERIA_VULNERABILITIES:
        return []

    requirements = CRITERIA_VULNERABILITIES[vulnerability_type]["requirements"]
    cwe_ids: list[str] = []
    for requirement in requirements:
        if requirement not in CRITERIA_REQUIREMENTS:
            continue

        references: list[str] = CRITERIA_REQUIREMENTS[requirement]["references"]
        cwe_ids.extend(
            [
                f'CWE-{reference.split(".")[1]}'
                for reference in references
                if reference.startswith("cwe.")
            ],
        )

    return cvss_utils.parse_cwe_ids(cwe_ids) or []


async def get_finding_policy_by_name(
    *,
    loaders: Dataloaders,
    finding_name: str,
    organization_name: str,
    treatment: TreatmentStatus,
) -> OrgFindingPolicy | None:
    org_finding_policies = await loaders.organization_finding_policies.load(organization_name)

    return next(
        (
            finding_policy
            for finding_policy in org_finding_policies
            if finding_policy.name.lower().endswith(finding_name.lower())
            and finding_policy.treatment_acceptance == treatment
        ),
        None,
    )


async def get_finding(loaders: Dataloaders, finding_id: str) -> Finding:
    finding = await loaders.finding.load(finding_id)
    if not finding or is_deleted(finding):
        raise FindingNotFound()

    return finding


def validate_evidence_name(*, organization_name: str, group_name: str, filename: str) -> None:
    detail: str = "Format organizationName-groupName-10 alphanumeric chars.extension"
    starts: str = f"{organization_name.lower()}-{group_name.lower()}-"
    if not filename.startswith(starts):
        raise InvalidFileName(detail)

    ends: str = filename.rsplit(".", 1)[-1]
    value: str = filename.replace(starts, "").replace(f".{ends}", "")
    if len(value) != 10 or not value.isalnum():
        raise InvalidFileName(detail)
