import os

from integrates.custom_utils import (
    files as files_utils,
)


def test_assert_file_mime() -> None:
    path = os.path.dirname(__file__)
    filename = os.path.join(path, "mock/test-vulns.yaml")
    non_included_filename = os.path.join(path, "mock/test.7z")
    allowed_mimes = ["text/plain"]
    assert files_utils.assert_file_mime(filename, allowed_mimes)
    assert not files_utils.assert_file_mime(non_included_filename, allowed_mimes)
