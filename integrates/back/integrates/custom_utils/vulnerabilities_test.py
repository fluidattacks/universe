from datetime import (
    datetime,
)
from decimal import Decimal

from integrates.custom_exceptions import InvalidRange
from integrates.custom_utils.vulnerabilities import (
    as_range,
    format_vulnerabilities,
    get_closing_date,
    get_first_week_dates,
    get_priority_value,
    get_ranges,
    get_treatment_from_org_finding_policy,
    group_specific,
    is_range,
    range_to_list,
    ungroup_specific,
)
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.enums import Source
from integrates.db_model.organizations.types import OrganizationPriorityPolicy
from integrates.db_model.roots.enums import RootCloningStatus, RootCriticality, RootStatus, RootType
from integrates.db_model.roots.types import GitRoot
from integrates.db_model.types import SeverityScore, Treatment
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateReason,
    VulnerabilityStateStatus,
    VulnerabilityTechnique,
    VulnerabilityToolImpact,
    VulnerabilityType,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityTool,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    FindingFaker,
    GitRootCloningFaker,
    GitRootFaker,
    GitRootStateFaker,
    GroupFaker,
    OrganizationFaker,
    VulnerabilityFaker,
    VulnerabilityStateFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

FIN_ID = "3c475384-834c-47b0-ac71-a41a022e401c"
ORG_ID = "ORG#dcb069b4-dd78-2180-460e-05f8b353b7bc"
ROOT_ID = "4039d098-ffc5-4984-8ed3-eb13bca28e19"
VULN_ID = "ab7995ed-e8a3-90d5-16fb-6a9c25307t47"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            findings=[FindingFaker(group_name="group1", id=FIN_ID)],
            groups=[GroupFaker(name="group1", organization_id=ORG_ID)],
            organizations=[
                OrganizationFaker(
                    id=ORG_ID,
                    name="org1",
                    priority_policies=[
                        OrganizationPriorityPolicy("PTAAS", -1000),
                        OrganizationPriorityPolicy("AV:L", -50),
                        OrganizationPriorityPolicy("E:P", -978),
                    ],
                ),
            ],
            roots=[GitRootFaker(group_name="group1", id=ROOT_ID, organization_name="org1")],
            vulnerabilities=[
                VulnerabilityFaker(
                    finding_id=FIN_ID,
                    group_name="group1",
                    id=VULN_ID,
                    organization_name="org1",
                    root_id=ROOT_ID,
                ),
            ],
        ),
    )
)
async def test_priority_value() -> None:
    loaders: Dataloaders = get_new_context()
    vulnerability = await loaders.vulnerability.load(VULN_ID)

    # Act
    assert vulnerability
    priority_score = await get_priority_value(loaders, vulnerability)

    # Assert
    assert vulnerability.technique == VulnerabilityTechnique.PTAAS
    assert vulnerability.severity_score
    assert vulnerability.severity_score.cvss_v4.find("AV:L") > 0
    assert vulnerability.severity_score.cvss_v4.find("E:P") > 0
    assert priority_score == Decimal(-2028)


def test_as_range() -> None:
    range_to_stringify = [1, 2, 3, 4, 5]
    test_data = as_range(range_to_stringify)
    expected_output = "1-5"
    assert test_data == expected_output


@parametrize(
    args=["vulns", "root"],
    cases=[
        [
            [
                VulnerabilityFaker(
                    state=VulnerabilityStateFaker(
                        modified_by="integrateshacker@fluidattacks.com",
                        modified_date=datetime.fromisoformat("2023-04-17T16:44:44+00:00"),
                        source=Source.ANALYST,
                        specific="345",
                        status=VulnerabilityStateStatus.SUBMITTED,
                        where="universe/path/to/file3.ext",
                        commit="e17059d1e17059d1e17059d1e17059d1e17059d1",
                        tool=VulnerabilityTool(
                            name="tool-1", impact=VulnerabilityToolImpact.DIRECT
                        ),
                    ),
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    type=VulnerabilityType.LINES,
                    severity_score=None,
                ),
                VulnerabilityFaker(
                    state=VulnerabilityStateFaker(
                        modified_by="unittest@fluidattacks.com",
                        modified_date=datetime.fromisoformat("2020-01-03T17:46:10+00:00"),
                        source=Source.ASM,
                        specific="12",
                        status=VulnerabilityStateStatus.VULNERABLE,
                        where="test/data/lib_path/f060/csharp.cs",
                        commit="ea871eee64cfd5ce293411efaf4d3b446d04eb4a",
                        tool=VulnerabilityTool(
                            name="tool-2", impact=VulnerabilityToolImpact.INDIRECT
                        ),
                    ),
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    type=VulnerabilityType.LINES,
                    cwe_ids=["CWE-1035", "CWE-770", "CWE-937"],
                    severity_score=SeverityScore(
                        base_score=Decimal("5.4"),
                        temporal_score=Decimal("4.9"),
                        cvss_v3="CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:L/I:L/A:N/E:P/RL:O/RC:C",
                        cvss_v4="CVSS:4.0/AV:N/AC:L/AT:N/PR:L/UI:N/VC:L"
                        "/VI:L/VA:N/SC:N/SI:N/SA:N/E:P",
                        threat_score=Decimal("2.1"),
                        cvssf=Decimal("3.482"),
                        cvssf_v4=Decimal("0.072"),
                    ),
                ),
                VulnerabilityFaker(
                    state=VulnerabilityStateFaker(
                        modified_by="test@unittesting.com",
                        modified_date=datetime.fromisoformat("2020-09-09T21:01:26+00:00"),
                        source=Source.ASM,
                        specific="phone",
                        status=VulnerabilityStateStatus.VULNERABLE,
                        where="https://example.com",
                        tool=VulnerabilityTool(
                            name="tool-2", impact=VulnerabilityToolImpact.INDIRECT
                        ),
                    ),
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    type=VulnerabilityType.INPUTS,
                    cwe_ids=["CWE-1035", "CWE-770", "CWE-937"],
                    severity_score=SeverityScore(
                        base_score=Decimal("5.4"),
                        temporal_score=Decimal("4.9"),
                        cvss_v3="CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:L/I:L/A:N/E:P/RL:O/RC:C",
                        cvss_v4="CVSS:4.0/AV:N/AC:L/AT:N/PR:L/UI:N/VC:L/VI:L/VA:N"
                        "/SC:N/SI:N/SA:N/E:P",
                        cvssf_v4=Decimal("0.072"),
                        threat_score=Decimal("2.1"),
                        cvssf=Decimal("3.482"),
                    ),
                ),
                VulnerabilityFaker(
                    state=VulnerabilityStateFaker(
                        modified_by="integrateshacker@fluidattacks.com",
                        modified_date=datetime.fromisoformat("2023-04-17T16:46:15+00:00"),
                        source=Source.ANALYST,
                        specific="347",
                        status=VulnerabilityStateStatus.REJECTED,
                        where="universe/path/to/file3.ext",
                        commit="e17059d1e17059d1e17059d1e17059d1e17059d1",
                        reasons=[VulnerabilityStateReason.NAMING],
                        tool=VulnerabilityTool(
                            name="tool-1", impact=VulnerabilityToolImpact.DIRECT
                        ),
                    ),
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    type=VulnerabilityType.LINES,
                    severity_score=None,
                ),
            ],
            GitRootFaker(
                cloning=GitRootCloningFaker(
                    modified_by="jdoe@fluidattacks.com",
                    modified_date=datetime.fromisoformat("2020-11-19T13:39:10+00:00"),
                    reason="root OK",
                    status=RootCloningStatus.OK,
                    commit="5b5c92105b5c92105b5c92105b5c92105b5c9210",
                ),
                created_by="jdoe@fluidattacks.com",
                created_date=datetime.fromisoformat("2020-11-19T13:37:10+00:00"),
                group_name="unittesting",
                id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                organization_name="okada",
                state=GitRootStateFaker(
                    branch="master",
                    criticality=RootCriticality.LOW,
                    includes_health_check=True,
                    modified_by="jdoe@fluidattacks.com",
                    modified_date=datetime.fromisoformat("2020-11-19T13:37:10+00:00"),
                    nickname="universe",
                    status=RootStatus.ACTIVE,
                    url="https://gitlab.com/fluidattacks/universe",
                    gitignore=["bower_components/*", "node_modules/*"],
                ),
                type=RootType.GIT,
            ),
        ]
    ],
)
async def test_format_vulnerabilities(vulns: list[Vulnerability], root: GitRoot) -> None:
    roots = [root] * len(vulns)
    test_data = format_vulnerabilities(vulns, roots)

    expected_output = {
        "ports": [],
        "lines": [
            {
                "path": "universe/path/to/file3.ext",
                "line": "345",
                "state": "submitted",
                "source": "analyst",
                "tool": {"name": "tool-1", "impact": "direct"},
                "commit_hash": "e17059d1e17059d1e17059d1e17059d1e17059d1",
                "repo_nickname": "universe",
            },
            {
                "path": "test/data/lib_path/f060/csharp.cs",
                "line": "12",
                "state": "open",
                "source": "analyst",
                "tool": {"name": "tool-2", "impact": "indirect"},
                "commit_hash": "ea871eee64cfd5ce293411efaf4d3b446d04eb4a",
                "cvss_v3": ("CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:L/I:L/A:N/E:P/RL:O/RC:C"),
                "cvss_v4": ("CVSS:4.0/AV:N/AC:L/AT:N/PR:L/UI:N/VC:L/VI:L/VA:N/SC:N/SI:N/SA:N/E:P"),
                "cwe_ids": ["CWE-1035", "CWE-770", "CWE-937"],
                "repo_nickname": "universe",
            },
            {
                "path": "universe/path/to/file3.ext",
                "line": "347",
                "state": "rejected",
                "source": "analyst",
                "tool": {"name": "tool-1", "impact": "direct"},
                "commit_hash": "e17059d1e17059d1e17059d1e17059d1e17059d1",
                "repo_nickname": "universe",
            },
        ],
        "inputs": [
            {
                "url": "https://example.com",
                "field": "phone",
                "state": "open",
                "source": "analyst",
                "tool": {"name": "tool-2", "impact": "indirect"},
                "cvss_v3": ("CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:L/I:L/A:N/E:P/RL:O/RC:C"),
                "cvss_v4": ("CVSS:4.0/AV:N/AC:L/AT:N/PR:L/UI:N/VC:L/VI:L/VA:N/SC:N/SI:N/SA:N/E:P"),
                "cwe_ids": ["CWE-1035", "CWE-770", "CWE-937"],
                "repo_nickname": "universe",
            }
        ],
    }

    assert test_data == expected_output


@parametrize(
    args=["open_vuln", "closed_vuln"],
    cases=[
        [
            VulnerabilityFaker(
                state=VulnerabilityStateFaker(
                    modified_date=datetime.fromisoformat("2020-09-09T21:01:26+00:00"),
                    status=VulnerabilityStateStatus.VULNERABLE,
                )
            ),
            VulnerabilityFaker(
                state=VulnerabilityStateFaker(
                    modified_date=datetime.fromisoformat("2019-01-08T21:01:26+00:00"),
                    status=VulnerabilityStateStatus.SAFE,
                ),
            ),
        ]
    ],
)
def test_get_vuln_closing_date(open_vuln: Vulnerability, closed_vuln: Vulnerability) -> None:
    test_data = get_closing_date(closed_vuln)
    closing_date = datetime(2019, 1, 8).date()
    assert test_data == closing_date

    assert open_vuln
    test_data = get_closing_date(open_vuln)
    assert test_data is None


def test_get_ranges() -> None:
    working_list = [1, 2, 3, 7, 9, 10, 11, 12, 13, 19]
    test_data = get_ranges(working_list)
    expected_output = "1-3,7,9-13,19"
    assert test_data == expected_output


@parametrize(
    args=["modified_date", "user_email"],
    cases=[
        [
            datetime.fromisoformat("2020-01-01T20:07:57+00:00"),
            "unittesting@fluidattacks.com",
        ]
    ],
)
def test_get_treatment_from_org_finding_policy(modified_date: datetime, user_email: str) -> None:
    result = get_treatment_from_org_finding_policy(
        modified_date=modified_date, user_email=user_email
    )
    assert isinstance(result, tuple)
    assert len(result) == 2
    assert all(isinstance(result[i], Treatment) for i in range(len(result)))


@parametrize(
    args=["vulns", "expected"],
    cases=[
        [
            [
                VulnerabilityFaker(
                    id="08717ec8-53a4-409c-aeb3-883b8c0a2d82",
                    type=VulnerabilityType.LINES,
                    finding_id="d201e3e7-0af9-4900-8072-fbcc3a2bb450",
                    root_id="5fdae900-374a-48d8-8868-60c9de6c0f31",
                    state=VulnerabilityStateFaker(
                        status=VulnerabilityStateStatus.SUBMITTED,
                        where="universe/path/to/file3.ext",
                    ),
                ),
                VulnerabilityFaker(
                    id="0a848781-b6a4-422e-95fa-692151e6a98f",
                    type=VulnerabilityType.LINES,
                    finding_id="d201e3e7-0af9-4900-8072-fbcc3a2bb450",
                    root_id="5fdae900-374a-48d8-8868-60c9de6c0f31",
                    state=VulnerabilityStateFaker(
                        status=VulnerabilityStateStatus.VULNERABLE,
                        where="test/data/lib_path/f060/csharp.cs",
                    ),
                ),
                VulnerabilityFaker(
                    id="80d6a69f-a376-46be-98cd-2fdedcffdcc0",
                    type=VulnerabilityType.INPUTS,
                    finding_id="d201e3e7-0af9-4900-8072-fbcc3a2bb450",
                    root_id="5fdae900-374a-48d8-8868-60c9de6c0f31",
                    state=VulnerabilityStateFaker(
                        status=VulnerabilityStateStatus.VULNERABLE,
                        where="https://example.com",
                    ),
                ),
                VulnerabilityFaker(
                    id="d65e21fb-f399-46bb-b390-90781079a0e7",
                    type=VulnerabilityType.LINES,
                    finding_id="d201e3e7-0af9-4900-8072-fbcc3a2bb450",
                    root_id="5fdae900-374a-48d8-8868-60c9de6c0f31",
                    state=VulnerabilityStateFaker(
                        status=VulnerabilityStateStatus.REJECTED,
                        where="universe/path/to/file3.ext",
                    ),
                ),
            ],
            [
                Vulnerability(
                    created_by="jane@entry.com",
                    created_date=datetime.fromisoformat("2019-04-10T12:59:52+00:00"),
                    finding_id="d201e3e7-0af9-4900-8072-fbcc3a2bb450",
                    group_name="test-group",
                    hacker_email="hacker@fluidattacks.com",
                    id="80d6a69f-a376-46be-98cd-2fdedcffdcc0",
                    organization_name="test-org",
                    root_id="5fdae900-374a-48d8-8868-60c9de6c0f31",
                    state=VulnerabilityStateFaker(
                        status=VulnerabilityStateStatus.VULNERABLE,
                        where="https://example.com",
                    ),
                    type=VulnerabilityType.INPUTS,
                ),
                Vulnerability(
                    created_by="jane@entry.com",
                    created_date=datetime.fromisoformat("2019-04-10T12:59:52+00:00"),
                    finding_id="d201e3e7-0af9-4900-8072-fbcc3a2bb450",
                    group_name="test-group",
                    hacker_email="hacker@fluidattacks.com",
                    id="0a848781-b6a4-422e-95fa-692151e6a98f",
                    organization_name="test-org",
                    root_id="5fdae900-374a-48d8-8868-60c9de6c0f31",
                    state=VulnerabilityStateFaker(
                        status=VulnerabilityStateStatus.VULNERABLE,
                        where="test/data/lib_path/f060/csharp.cs",
                    ),
                    type=VulnerabilityType.LINES,
                ),
                Vulnerability(
                    created_by="jane@entry.com",
                    created_date=datetime.fromisoformat("2019-04-10T12:59:52+00:00"),
                    finding_id="d201e3e7-0af9-4900-8072-fbcc3a2bb450",
                    group_name="test-group",
                    hacker_email="hacker@fluidattacks.com",
                    id="08717ec8-53a4-409c-aeb3-883b8c0a2d82",
                    organization_name="test-org",
                    root_id="5fdae900-374a-48d8-8868-60c9de6c0f31",
                    state=VulnerabilityStateFaker(
                        status=VulnerabilityStateStatus.SUBMITTED,
                        where="universe/path/to/file3.ext",
                        specific="99,99",
                    ),
                    type=VulnerabilityType.LINES,
                ),
            ],
        ]
    ],
)
def test_group_specific(vulns: list[Vulnerability], expected: list[Vulnerability]) -> None:
    test_data = group_specific(vulns, VulnerabilityType.INPUTS)

    assert test_data == expected


def test_is_range() -> None:
    range_value = "100-200"
    no_range_value = "20"
    assert is_range(range_value)
    assert not is_range(no_range_value)


@parametrize(
    args=["range_value", "expected_output", "range_to_raise_exception"],
    cases=[["10-15", ["10", "11", "12", "13", "14", "15"], "13-12"]],
)
def test_range_to_list(
    range_value: str,
    expected_output: list,
    range_to_raise_exception: str,
) -> None:
    result = range_to_list(range_value)
    assert isinstance(result, list)
    assert result == expected_output
    with raises(InvalidRange):
        assert range_to_list(range_to_raise_exception)


def test_ungroup_specific() -> None:
    specific = "13,14,18-20,24-30,40"
    test_data = ungroup_specific(specific)
    expected_output = [
        "13",
        "14",
        "18",
        "19",
        "20",
        "24",
        "25",
        "26",
        "27",
        "28",
        "29",
        "30",
        "40",
    ]
    assert isinstance(test_data, list)
    assert test_data == expected_output


async def test_get_first_week_dates() -> None:
    test_data = get_first_week_dates(datetime.fromisoformat("2020-01-03T17:46:10+00:00"))
    expected_output = (
        datetime.fromisoformat("2019-12-30T00:00:00+00:00"),
        datetime.fromisoformat("2020-01-05T23:59:59+00:00"),
    )
    assert test_data == expected_output
