from datetime import (
    date,
    datetime,
)
from enum import (
    Enum,
)

from integrates.custom_utils.datetime import (
    ReportDates,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.toe_inputs.types import (
    GroupHistoricToeInputsRequest,
    ToeInputEnrichedState,
)
from integrates.db_model.toe_lines.types import (
    GroupHistoricToeLinesRequest,
    GroupToeLinesRequest,
    ToeLinesConnection,
    ToeLinesEnrichedStatesConnection,
)
from integrates.db_model.toe_ports.types import (
    GroupToePortStatesRequest,
    ToePortStatesConnection,
)
from integrates.db_model.types import (
    Connection,
)


class ToeType(str, Enum):
    INPUT = "toe_inputs"
    LINE = "toe_lines"
    PORT = "toe_ports"


def get_date(report_datetime: datetime | None) -> date | None:
    if report_datetime is None:
        return None
    return report_datetime.date()


async def get_connection(
    *,
    loaders: Dataloaders,
    toe_type: ToeType,
    group_name: str,
    cursor: str | None,
    page_size: int,
    report_dates: ReportDates,
    all_current_toes: bool = False,
) -> (
    ToeLinesEnrichedStatesConnection
    | Connection[ToeInputEnrichedState]
    | ToePortStatesConnection
    | ToeLinesConnection
    | None
):
    if toe_type == ToeType.INPUT:
        historic_inputs = await loaders.group_toe_inputs_enriched_historic_state.load(
            GroupHistoricToeInputsRequest(
                group_name=group_name,
                start_date=report_dates.past_day.start_date.date(),
                end_date=report_dates.today.end_date.date(),
                first=page_size,
                after=cursor,
                paginate=False,
            ),
        )
        unique_inputs = tuple(
            {
                (
                    edge.node.root_id,
                    edge.node.state.attacked_by,
                    edge.node.component,
                    edge.node.entry_point,
                    get_date(edge.node.state.attacked_at),
                ): edge
                for edge in historic_inputs.edges
            }.values(),
        )
        return historic_inputs._replace(edges=unique_inputs)
    if toe_type == ToeType.LINE:
        if all_current_toes:
            return await loaders.group_toe_lines.load(
                GroupToeLinesRequest(
                    group_name=group_name,
                    first=page_size,
                    after=cursor,
                    paginate=False,
                    be_present=True,
                ),
            )
        historic_lines = await loaders.group_toe_lines_enriched_historic_state.load(
            GroupHistoricToeLinesRequest(
                group_name=group_name,
                start_date=report_dates.past_day.start_date.date(),
                end_date=report_dates.today.end_date.date(),
                first=page_size,
                after=cursor,
                paginate=False,
            ),
        )
        unique_lines = tuple(
            {
                (
                    edge.node.root_id,
                    edge.node.filename,
                    edge.node.state.attacked_by,
                    get_date(edge.node.state.attacked_at),
                ): edge
                for edge in historic_lines.edges
                if edge.node.state.attacked_lines
            }.values(),
        )
        return historic_lines._replace(edges=unique_lines)
    if toe_type == ToeType.PORT:
        return await loaders.group_toe_port_states.load(
            GroupToePortStatesRequest(
                group_name=group_name,
                start_date=report_dates.past_day.start_date.date(),
                end_date=report_dates.today.end_date.date(),
                first=page_size,
                after=cursor,
                paginate=False,
            ),
        )
    return None
