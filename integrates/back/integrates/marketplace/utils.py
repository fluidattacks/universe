from integrates.db_model.marketplace.enums import (
    AWSMarketplacePricingDimension,
)
from integrates.db_model.marketplace.types import (
    AWSMarketplaceSubscription,
    AWSMarketplaceSubscriptionEntitlement,
)


def get_groups_entitlement(
    subscription: AWSMarketplaceSubscription,
) -> AWSMarketplaceSubscriptionEntitlement:
    return next(
        filter(
            lambda x: x.dimension == AWSMarketplacePricingDimension.GROUPS,
            subscription.state.entitlements,
        ),
    )
