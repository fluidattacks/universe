import time
from collections.abc import (
    Awaitable,
    Callable,
)
from contextlib import (
    AsyncExitStack,
)
from typing import (
    Any,
)

import aioboto3
from aiobotocore.config import (
    AioConfig,
)
from types_aiobotocore_marketplace_entitlement.client import (
    MarketplaceEntitlementServiceClient,
)
from types_aiobotocore_meteringmarketplace.client import (
    MarketplaceMeteringClient,
)

from integrates.context import (
    FI_AWS_REGION_NAME,
    FI_ENVIRONMENT,
    FI_MARKETPLACE_EXTERNAL_ID,
    FI_MARKETPLACE_ROLE_ARN,
)
from integrates.custom_utils.roots import (
    fetch_temp_aws_creds,
)

MarketplaceContext = tuple[
    Callable[[], Awaitable[None]],
    Callable[[], Awaitable[None]],
    Callable[[], Awaitable[MarketplaceEntitlementServiceClient]],
    Callable[[], Awaitable[MarketplaceMeteringClient]],
]


def create_marketplace_context() -> MarketplaceContext:
    context_stack = AsyncExitStack()
    session_duration: float = 0.0
    entitlement_client: Any
    metering_client: Any

    async def _check_clients() -> None:
        nonlocal session_duration

        if time.time() - session_duration > 3600.0:
            await shutdown()
            await _get_clients()

    async def _get_clients() -> None:
        nonlocal context_stack, entitlement_client, metering_client
        nonlocal session_duration

        session_duration = time.time()
        if FI_ENVIRONMENT == "production":
            secrets = await fetch_temp_aws_creds(
                FI_MARKETPLACE_ROLE_ARN,
                FI_MARKETPLACE_EXTERNAL_ID,
            )
            session = aioboto3.Session(
                aws_access_key_id=secrets[0].value,
                aws_secret_access_key=secrets[1].value,
                aws_session_token=secrets[2].value,
                region_name=FI_AWS_REGION_NAME,
            )
            client_config = AioConfig(
                connect_timeout=60,
                max_pool_connections=1024,
                read_timeout=60,
                retries={"max_attempts": 10, "mode": "standard"},
            )
            entitlement_client = await context_stack.enter_async_context(
                session.client(
                    config=client_config,
                    service_name="marketplace-entitlement",
                    use_ssl=True,
                    verify=True,
                ),
            )
            metering_client = await context_stack.enter_async_context(
                session.client(
                    config=client_config,
                    service_name="meteringmarketplace",
                    use_ssl=True,
                    verify=True,
                ),
            )

    async def shutdown() -> None:
        await context_stack.aclose()

    async def get_entitlement_client() -> MarketplaceEntitlementServiceClient:
        nonlocal entitlement_client

        await _check_clients()
        return entitlement_client

    async def get_metering_client() -> MarketplaceMeteringClient:
        nonlocal metering_client

        await _check_clients()
        return metering_client

    return _get_clients, shutdown, get_entitlement_client, get_metering_client


(
    marketplace_startup,
    marketplace_shutdown,
    get_marketplace_entitlement_client,
    get_marketplace_metering_client,
) = create_marketplace_context()
