import asyncio
import logging
import logging.config
from collections.abc import (
    Awaitable,
    Callable,
    Coroutine,
)
from contextlib import (
    suppress,
)
from datetime import (
    UTC,
    datetime,
)
from enum import (
    Enum,
)
from functools import (
    partial,
)
from typing import (
    Any,
    TypeVar,
)

from aioextensions import (
    collect,
)
from starlette.requests import (
    Request,
)
from types_aiobotocore_meteringmarketplace.type_defs import (
    UsageRecordTypeDef,
)

from integrates.context import (
    FI_ENVIRONMENT,
    FI_MARKETPLACE_PRODUCT_CODE,
)
from integrates.custom_exceptions import (
    InvalidAWSMarketplaceProductCode,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.groups.enums import (
    GroupManaged,
    GroupStateJustification,
)
from integrates.db_model.marketplace.add import (
    add_marketplace_subscription,
)
from integrates.db_model.marketplace.enums import (
    AWSMarketplacePricingDimension,
    AWSMarketplaceSubscriptionStatus,
)
from integrates.db_model.marketplace.types import (
    AWSMarketplaceSubscription,
    AWSMarketplaceSubscriptionEntitlement,
    AWSMarketplaceSubscriptionSNSUpdate,
    AWSMarketplaceSubscriptionState,
    AWSMarketplaceSubscriptionUpdate,
)
from integrates.db_model.marketplace.update import (
    update_marketplace_subscription_metadata,
    update_marketplace_subscription_state,
)
from integrates.db_model.stakeholders import (
    update_metadata as update_stakeholder_metadata,
)
from integrates.db_model.stakeholders.types import (
    StakeholderMetadataToUpdate,
)
from integrates.groups.domain import (
    get_groups_owned_by_user,
    update_group_managed,
)
from integrates.marketplace.client import (
    get_marketplace_entitlement_client,
    get_marketplace_metering_client,
)
from integrates.marketplace.utils import (
    get_groups_entitlement,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger(__name__)

T = TypeVar("T")
ASYNC_SERVER_TASK_ERROR_1: str = "AWS Marketplace subscription not found"
ASYNC_SERVER_TASK_ERROR_2: str = "AWS Marketplace subscription did not have the expected state"


async def _handle_sns_entitlement_update(
    *,
    customer_identifier: str,
    loaders: Dataloaders,
    has_free_trial: bool,
    private_offer_id: str | None,
) -> tuple[bool, str]:
    success: bool = True
    details: str = ""
    if not await update_marketplace_subscription_entitlements(
        aws_customer_id=customer_identifier,
        loaders=loaders,
        has_free_trial=has_free_trial,
        private_offer_id=private_offer_id,
    ):
        success = False
        details = f"No entitlements found for customer {customer_identifier}"

    return success, details


async def _handle_sns_subscribe_success(
    *,
    customer_identifier: str,
    loaders: Dataloaders,
    **_kwargs: Any,
) -> tuple[bool, str]:
    success: bool = False
    details: str = ""
    subscription = await loaders.aws_marketplace_subscriptions.load(customer_identifier)
    if subscription:
        if subscription.state.status in (
            AWSMarketplaceSubscriptionStatus.EXPIRED,
            AWSMarketplaceSubscriptionStatus.FAILED,
            AWSMarketplaceSubscriptionStatus.PENDING,
        ):
            await update_marketplace_subscription_terms(
                subscription=subscription,
                status=AWSMarketplaceSubscriptionStatus.ACTIVE,
            )
            success = True
        else:
            details = ASYNC_SERVER_TASK_ERROR_2
    else:
        details = ASYNC_SERVER_TASK_ERROR_1

    return success, details


async def _handle_sns_subscribe_fail(
    *,
    customer_identifier: str,
    loaders: Dataloaders,
    **_kwargs: Any,
) -> tuple[bool, str]:
    success: bool = False
    details: str = ""
    subscription = await loaders.aws_marketplace_subscriptions.load(customer_identifier)
    if subscription is not None:
        if subscription.state.status == AWSMarketplaceSubscriptionStatus.PENDING:
            await update_marketplace_subscription_terms(
                subscription=subscription,
                status=AWSMarketplaceSubscriptionStatus.FAILED,
            )
            success = True
        else:
            details = ASYNC_SERVER_TASK_ERROR_2
    else:
        details = ASYNC_SERVER_TASK_ERROR_1

    return success, details


async def _handle_sns_unsubscribe_pending(**_kwargs: Any) -> tuple[bool, str]:
    """
    For the time being, there is no need to do anything
    in response to this event
    """
    return True, ""


async def _handle_sns_unsubscribe_success(
    *,
    customer_identifier: str,
    loaders: Dataloaders,
    **_kwargs: Any,
) -> tuple[bool, str]:
    success: bool = False
    details: str = ""
    subscription = await loaders.aws_marketplace_subscriptions.load(customer_identifier)
    if subscription:
        if subscription.state.status == AWSMarketplaceSubscriptionStatus.ACTIVE:
            await update_marketplace_subscription_terms(
                subscription=subscription,
                status=AWSMarketplaceSubscriptionStatus.EXPIRED,
            )
            success = True

            if subscription.user:
                groups = await get_groups_owned_by_user(loaders=loaders, email=subscription.user)
                justification = GroupStateJustification.AWS_SUBSCRIPTION_EXPIRED
                await collect(
                    (
                        update_group_managed(
                            loaders=loaders,
                            comments="AWS subscription expired",
                            email="integrates@fluidattacks.com",
                            group_name=group,
                            managed=GroupManaged.UNDER_REVIEW,
                            justification=justification,
                        )
                        for group in groups
                    ),
                    workers=16,
                )
        else:
            details = ASYNC_SERVER_TASK_ERROR_2
    else:
        details = ASYNC_SERVER_TASK_ERROR_1

    return success, details


async def _retry_aws_call_on_exceptions(
    coroutine: Coroutine[Any, Any, T],
    exceptions: tuple[type[Exception], ...],
) -> T:
    for _ in range(2):
        with suppress(*exceptions):
            return await coroutine

        await asyncio.sleep(0.2)
    return await coroutine


SNSExecutorFunction = Callable[
    [str, Dataloaders, str, str | None, str | None],
    Awaitable[tuple[bool, str]],
]


class SNSUpdateExecutor(Enum):
    ENTITLEMENT_UPDATE: SNSExecutorFunction = partial(_handle_sns_entitlement_update)
    SUBSCRIBE_FAIL: SNSExecutorFunction = partial(_handle_sns_subscribe_fail)
    SUBSCRIBE_SUCCESS: SNSExecutorFunction = partial(_handle_sns_subscribe_success)
    UNSUBSCRIBE_PENDING: SNSExecutorFunction = partial(_handle_sns_unsubscribe_pending)
    UNSUBSCRIBE_SUCCESS: SNSExecutorFunction = partial(_handle_sns_unsubscribe_success)


async def check_user_subscription_usage_surpass_contract(
    *,
    email: str,
    loaders: Dataloaders,
) -> bool:
    usage_surpass_contract = False
    user = await loaders.stakeholder.load(email)
    owned_groups = await get_groups_owned_by_user(loaders=loaders, email=email)
    if user is not None and user.aws_customer_id is not None:
        subscription = await loaders.aws_marketplace_subscriptions.load(user.aws_customer_id)
        if (
            subscription is not None
            and subscription.state.status == AWSMarketplaceSubscriptionStatus.ACTIVE
            and len(owned_groups) > get_groups_entitlement(subscription).value
        ):
            usage_surpass_contract = True

    return usage_surpass_contract


async def create_marketplace_subscription(
    *,
    aws_customer_id: str,
    entitlements: list[AWSMarketplaceSubscriptionEntitlement],
    has_free_trial: bool | None,
    private_offer_id: str | None,
    aws_account_id: str | None = None,
) -> None:
    now = datetime.now(tz=UTC)
    subscription = AWSMarketplaceSubscription(
        aws_account_id=aws_account_id,
        aws_customer_id=aws_customer_id,
        created_at=now,
        state=AWSMarketplaceSubscriptionState(
            has_free_trial=(has_free_trial if has_free_trial is not None else False),
            entitlements=entitlements,
            modified_date=now,
            private_offer_id=private_offer_id,
            status=AWSMarketplaceSubscriptionStatus.PENDING,
        ),
    )
    await add_marketplace_subscription(subscription=subscription)


async def get_marketplace_subscription_entitlements(
    *,
    aws_customer_id: str,
) -> list[AWSMarketplaceSubscriptionEntitlement]:
    entitlements: list[AWSMarketplaceSubscriptionEntitlement] = []
    client = await get_marketplace_entitlement_client()
    try:
        response = await _retry_aws_call_on_exceptions(
            coroutine=client.get_entitlements(
                ProductCode=FI_MARKETPLACE_PRODUCT_CODE,
                Filter={"CUSTOMER_IDENTIFIER": [aws_customer_id]},
            ),
            exceptions=(
                client.exceptions.InternalServiceErrorException,
                client.exceptions.ThrottlingException,
            ),
        )
        if response["Entitlements"]:
            entitlements = [
                AWSMarketplaceSubscriptionEntitlement(
                    dimension=AWSMarketplacePricingDimension[entitlement["Dimension"].upper()],
                    expiration_date=entitlement["ExpirationDate"],
                    value=entitlement["Value"]["IntegerValue"],
                )
                for entitlement in response["Entitlements"]
            ]
    except (
        client.exceptions.InternalServiceErrorException,
        client.exceptions.ThrottlingException,
    ) as exc:
        LOGGER.error(
            "AWS Marketplace could not return entitlements",
            extra={
                "extra": {
                    "aws_customer_id": aws_customer_id,
                    "exception": exc,
                },
            },
        )

    return entitlements


async def link_user_marketplace_subscription(
    *,
    aws_customer_id: str,
    email: str,
    loaders: Dataloaders,
) -> None:
    subscription = await loaders.aws_marketplace_subscriptions.load(aws_customer_id)
    if subscription is not None and subscription.user is None:
        await update_stakeholder_metadata(
            email=email,
            metadata=StakeholderMetadataToUpdate(aws_customer_id=aws_customer_id),
        )
        await update_marketplace_subscription_metadata(
            metadata=AWSMarketplaceSubscriptionUpdate(
                user=email,
            ),
            subscription=subscription,
        )


async def process_marketplace_sns_update(
    *,
    loaders: Dataloaders,
    message: AWSMarketplaceSubscriptionSNSUpdate,
) -> tuple[bool, str]:
    success: bool = False
    details: str = ""

    action = message["action"]
    customer_identifier = message["customer-identifier"]
    product_code = message["product-code"]
    has_free_trial = message.get("isFreeTrialTermPresent") == "true"
    private_offer_id = message.get("offer-identifier")

    if product_code == FI_MARKETPLACE_PRODUCT_CODE:
        success, details = await SNSUpdateExecutor[action.upper().replace("-", "_")].value(
            loaders=loaders,
            customer_identifier=customer_identifier,
            has_free_trial=has_free_trial,
            private_offer_id=private_offer_id,
        )
    else:
        details = "AWS Marketplace message from SNS contains wrong product code"
        LOGGER.error(details, extra={"extra": {"message": message}})

    return success, details


async def resolve_aws_customer_information(request: Request, token: str) -> tuple[str | None, ...]:
    aws_account_id: str | None = None
    aws_customer_id: str | None = None
    invalid_product_code: bool = False

    client = await get_marketplace_metering_client()
    try:
        response = await _retry_aws_call_on_exceptions(
            coroutine=client.resolve_customer(RegistrationToken=token),
            exceptions=(
                client.exceptions.InternalServiceErrorException,
                client.exceptions.ThrottlingException,
            ),
        )
        aws_account_id = response["CustomerAWSAccountId"]
        aws_customer_id = response["CustomerIdentifier"]
        product_code = response["ProductCode"]
        if product_code != FI_MARKETPLACE_PRODUCT_CODE:
            LOGGER.error(
                (
                    "Token set from the AWS Marketplace during sign up "
                    "contains wrong product code"
                ),
                extra={"extra": {"response": response}},
            )
            invalid_product_code = True
    except (
        client.exceptions.InternalServiceErrorException,
        client.exceptions.ThrottlingException,
        client.exceptions.DisabledApiException,
    ) as exc:
        LOGGER.error(
            "AWS Marketplace server could not resolve customer information",
            extra={"extra": {"exception": exc, "headers": request.headers}},
        )
    except (
        client.exceptions.ExpiredTokenException,
        client.exceptions.InvalidTokenException,
    ) as exc:
        LOGGER.error(
            "AWS Marketplace token was invalid or expired",
            extra={"extra": {"exception": exc, "headers": request.headers}},
        )

    if invalid_product_code:
        raise InvalidAWSMarketplaceProductCode()

    return aws_account_id, aws_customer_id


async def resolve_aws_marketplace_token(request: Request, token: str) -> str | None:
    (
        aws_account_id,
        aws_customer_id,
    ) = await resolve_aws_customer_information(request, token)

    if aws_customer_id is not None:
        await update_marketplace_subscription_entitlements(
            aws_account_id=aws_account_id,
            aws_customer_id=aws_customer_id,
            loaders=get_new_context(),
        )

    return aws_customer_id


async def send_metering_records(*, email: str, loaders: Dataloaders, value: int) -> None:
    if FI_ENVIRONMENT == "production":
        user = await loaders.stakeholder.load(email)
        if user is not None and user.aws_customer_id is not None:
            client = await get_marketplace_metering_client()
            usage_record = UsageRecordTypeDef(
                Timestamp=datetime.now(tz=UTC),
                CustomerIdentifier=user.aws_customer_id,
                Dimension="groups",
                Quantity=value,
            )
            try:
                response = await _retry_aws_call_on_exceptions(
                    coroutine=client.batch_meter_usage(
                        UsageRecords=[usage_record],
                        ProductCode=FI_MARKETPLACE_PRODUCT_CODE,
                    ),
                    exceptions=(
                        client.exceptions.InternalServiceErrorException,
                        client.exceptions.ThrottlingException,
                    ),
                )
                unprocessed_records = response["UnprocessedRecords"]
                if len(unprocessed_records) > 0:
                    LOGGER.error(
                        "AWS Marketplace returned record unprocessed",
                        extra={
                            "extra": {
                                "record": unprocessed_records,
                                "user": email,
                            },
                        },
                    )
            except (
                client.exceptions.InternalServiceErrorException,
                client.exceptions.ThrottlingException,
                client.exceptions.DisabledApiException,
            ) as exc:
                LOGGER.error(
                    "AWS Marketplace server could not process metering record",
                    extra={
                        "extra": {
                            "exception": exc,
                            "record": usage_record,
                            "user": email,
                        },
                    },
                )
            except (
                client.exceptions.InvalidCustomerIdentifierException,
                client.exceptions.InvalidTagException,
                client.exceptions.InvalidUsageAllocationsException,
                client.exceptions.InvalidUsageDimensionException,
                client.exceptions.TimestampOutOfBoundsException,
            ) as exc:
                LOGGER.error(
                    "Metering record sent to AWS Marketplace was malformed",
                    extra={
                        "extra": {
                            "exception": exc,
                            "record": usage_record,
                            "user": email,
                        },
                    },
                )


async def update_marketplace_subscription_entitlements(
    *,
    aws_customer_id: str,
    loaders: Dataloaders,
    aws_account_id: str | None = None,
    has_free_trial: bool | None = None,
    private_offer_id: str | None = None,
) -> bool:
    entitlements_updated: bool = False
    subscription = await loaders.aws_marketplace_subscriptions.load(aws_customer_id)
    entitlements = await get_marketplace_subscription_entitlements(aws_customer_id=aws_customer_id)
    if len(entitlements) > 0:
        if subscription is None:
            await create_marketplace_subscription(
                aws_account_id=aws_account_id,
                aws_customer_id=aws_customer_id,
                has_free_trial=has_free_trial,
                entitlements=entitlements,
                private_offer_id=private_offer_id,
            )
        else:
            await update_marketplace_subscription_terms(
                has_free_trial=has_free_trial,
                entitlements=entitlements,
                private_offer_id=private_offer_id,
                subscription=subscription,
            )

            if aws_account_id is not None and subscription.aws_account_id is None:
                await update_marketplace_subscription_metadata(
                    metadata=AWSMarketplaceSubscriptionUpdate(aws_account_id=aws_account_id),
                    subscription=subscription,
                )

        entitlements_updated = True

    return entitlements_updated


async def update_marketplace_subscription_terms(
    *,
    subscription: AWSMarketplaceSubscription,
    entitlements: list[AWSMarketplaceSubscriptionEntitlement] | None = None,
    has_free_trial: bool | None = None,
    private_offer_id: str | None = None,
    status: AWSMarketplaceSubscriptionStatus | None = None,
) -> None:
    await update_marketplace_subscription_state(
        new_state=AWSMarketplaceSubscriptionState(
            entitlements=entitlements or subscription.state.entitlements,
            has_free_trial=(
                has_free_trial if has_free_trial is not None else subscription.state.has_free_trial
            ),
            modified_date=datetime.now(tz=UTC),
            private_offer_id=(private_offer_id or subscription.state.private_offer_id),
            status=status or subscription.state.status,
        ),
        subscription=subscription,
    )
