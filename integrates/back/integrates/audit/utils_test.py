from decimal import Decimal

from integrates.audit.model import AuditEvent
from integrates.audit.utils import dataclass_to_json


async def test_dataclass_to_json() -> None:
    output = dataclass_to_json(
        AuditEvent(
            action="CREATE",
            author="test@gmail.com",
            metadata={"a": "test", "b": {1, 2, 3}, "c": Decimal("9.3")},
            object="Stakeholder",
            object_id="test@gmail.com",
        )
    )
    assert output
