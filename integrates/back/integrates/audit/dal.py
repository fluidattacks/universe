import logging
import sys

from types_aiobotocore_sqs.type_defs import SendMessageBatchRequestEntryTypeDef

from integrates.audit.model import AuditEvent
from integrates.audit.utils import dataclass_to_json
from integrates.context import FI_AWS_SQS_AUDIT_QUEUE_URL
from integrates.sqs.resource import get_client

LOGGER = logging.getLogger(__name__)

# SQS limits
# https://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/quotas-messages.html
MAX_BATCH_SIZE = 10
MAX_BATCH_SIZE_BYTES = 256 * 1024


async def add_audit_batch(events: list[AuditEvent]) -> None:
    if not FI_AWS_SQS_AUDIT_QUEUE_URL:
        # Local/test setup not yet available
        LOGGER.info("Received audit events %s", events)
        return

    batches = list[list[SendMessageBatchRequestEntryTypeDef]]()
    current_batch = list[SendMessageBatchRequestEntryTypeDef]()
    current_batch_size = 0

    for event in events:
        event_json = dataclass_to_json(event)
        event_size = sys.getsizeof(event_json.encode("utf-8"))

        if (
            len(current_batch) >= MAX_BATCH_SIZE
            or (current_batch_size + event_size) > MAX_BATCH_SIZE_BYTES
        ):
            batches.append(current_batch)
            current_batch = []
            current_batch_size = 0

        current_batch.append(
            {
                "Id": str(id(event)),
                "MessageBody": event_json,
                "MessageGroupId": "#".join([event.mechanism, event.action, event.object]),
            }
        )
        current_batch_size += event_size

    if current_batch:
        batches.append(current_batch)

    sqs_client = await get_client()
    for batch in batches:
        try:
            await sqs_client.send_message_batch(
                Entries=batch,
                QueueUrl=FI_AWS_SQS_AUDIT_QUEUE_URL,
            )
        except Exception:
            LOGGER.exception("Failed adding audit events", extra={"extra": {"batch": batch}})
