from collections.abc import Mapping
from dataclasses import dataclass, field
from datetime import UTC, datetime
from typing import Literal

TMechanism = Literal[
    "API", "FORCES", "JIRA", "MELTS", "MIGRATION", "RETRIEVES", "SCHEDULER", "TASK", "WEB"
]


@dataclass(frozen=True, kw_only=True)
class AuditContext:
    author: str | None = None
    author_ip: str | None = None
    author_role: str | None = None
    author_user_agent: str | None = None
    mechanism: TMechanism | None = None


@dataclass(frozen=True, kw_only=True)
class AuditEvent(AuditContext):
    action: Literal["CREATE", "READ", "UPDATE", "DELETE"]
    author: str
    date: str = field(
        default_factory=lambda: datetime.isoformat(datetime.now(UTC)),
        init=False,
    )
    mechanism: TMechanism = "MIGRATION"
    metadata: Mapping[str, object]
    object: str
    object_id: str
