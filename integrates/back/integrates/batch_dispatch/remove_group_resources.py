import logging
import logging.config

from integrates.batch.types import (
    BatchProcessing,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.groups.enums import (
    GroupSubscriptionType,
)
from integrates.groups import (
    domain as groups_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger(__name__)


async def remove_group_resources(*, item: BatchProcessing) -> None:
    group_name = item.entity
    email = item.subject
    message = f"Removing resources requested by {email} for group {group_name}"
    LOGGER.info(":".join([item.subject, message]), extra={"extra": {"action": item}})
    await groups_domain.remove_resources(
        loaders=get_new_context(),
        email=email,
        group_name=group_name,
        validate_pending_actions=bool(item.additional_info["validate_pending_actions"]),
        subscription=GroupSubscriptionType[str(item.additional_info["subscription"]).upper()],
        has_advanced=bool(item.additional_info["has_advanced"]),
        unique_authors=item.additional_info.get("unique_authors", 0),
        emails=item.additional_info.get("emails", []),
    )
    message = f"Removal result: {True}"
    LOGGER.info(
        ":".join([item.subject, message]),
        extra={"extra": {"action": item, "success": True}},
    )
