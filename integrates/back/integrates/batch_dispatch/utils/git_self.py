import logging
import logging.config
import shutil
from contextlib import suppress
from datetime import datetime
from typing import NamedTuple

import fluidattacks_core.git as git_utils
from git import GitError, Repo

from integrates.custom_exceptions import ErrorUpdatingCredential, InvalidParameter
from integrates.dataloaders import Dataloaders
from integrates.db_model.credentials.types import (
    AWSRoleSecret,
    Credentials,
    HttpsPatSecret,
    HttpsSecret,
    OauthAzureSecret,
    OauthBitbucketSecret,
    OauthGithubSecret,
    OauthGitlabSecret,
    SshSecret,
)
from integrates.jobs_orchestration.jobs import PostCloningConfig
from integrates.oauth.common import get_credential_token, get_oauth_type
from integrates.organizations import domain as orgs_domain
from integrates.roots.s3_mirror import upload_cloned_repo_to_s3_tar
from integrates.settings.logger import LOGGING

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)


class CloneResult(NamedTuple):
    success: bool
    commit_date: datetime | None = None
    commit: str | None = None
    config: PostCloningConfig | None = None
    message: str | None = None
    repo: Repo | None = None


async def clone_repo(
    *,
    branch: str,
    root_url: str,
    cred: Credentials | None,
    loaders: Dataloaders,
    temp_dir: str,
    org_external_id: str | None = None,
    follow_redirects: bool,
) -> tuple[str | None, str | None]:
    stderr: str | None = None
    folder_to_clone_root: str | None = None
    if cred is None:
        if root_url.startswith("http"):
            # it can be a public repository
            folder_to_clone_root, stderr = await git_utils.clone(
                repo_branch=branch,
                repo_url=root_url,
                temp_dir=temp_dir,
                follow_redirects=follow_redirects,
            )
    elif isinstance(cred.secret, SshSecret):
        folder_to_clone_root, stderr = await git_utils.clone(
            repo_branch=branch,
            credential_key=cred.secret.key,
            repo_url=root_url,
            temp_dir=temp_dir,
            follow_redirects=follow_redirects,
        )
    elif isinstance(cred.secret, HttpsPatSecret):
        folder_to_clone_root, stderr = await git_utils.clone(
            repo_branch=branch,
            password=None,
            repo_url=root_url,
            temp_dir=temp_dir,
            token=cred.secret.token,
            user=None,
            is_pat=cred.state.is_pat,
            follow_redirects=follow_redirects,
        )
    elif isinstance(cred.secret, HttpsSecret):
        folder_to_clone_root, stderr = await git_utils.clone(
            repo_branch=branch,
            password=cred.secret.password,
            repo_url=root_url,
            temp_dir=temp_dir,
            token=None,
            user=cred.secret.user,
            follow_redirects=follow_redirects,
        )
    elif isinstance(
        cred.secret,
        (OauthAzureSecret, OauthGitlabSecret, OauthBitbucketSecret),
    ):
        _credential = await orgs_domain.get_credentials(
            loaders=loaders,
            credentials_id=cred.id,
            organization_id=cred.organization_id,
        )
        with suppress(ErrorUpdatingCredential):
            token = await get_credential_token(
                credential=_credential,
                loaders=loaders,
            )
            folder_to_clone_root, stderr = await git_utils.clone(
                repo_branch=branch,
                password=None,
                repo_url=root_url,
                temp_dir=temp_dir,
                token=token,
                user=None,
                provider=get_oauth_type(_credential),
                follow_redirects=follow_redirects,
            )
    elif isinstance(cred.secret, OauthGithubSecret):
        folder_to_clone_root, stderr = await git_utils.clone(
            repo_branch=branch,
            password=None,
            repo_url=root_url,
            temp_dir=temp_dir,
            token=cred.secret.access_token,
            user=None,
            provider=get_oauth_type(cred),
            follow_redirects=follow_redirects,
        )
    elif isinstance(cred.secret, AWSRoleSecret):
        folder_to_clone_root, stderr = await git_utils.clone(
            repo_branch=branch,
            repo_url=root_url,
            temp_dir=temp_dir,
            arn=cred.secret.arn,
            org_external_id=org_external_id,
            follow_redirects=follow_redirects,
        )
    else:
        shutil.rmtree(temp_dir, ignore_errors=True)
        raise InvalidParameter()

    return folder_to_clone_root, stderr


async def clone_root(
    *,
    loaders: Dataloaders,
    group_name: str,
    root_nickname: str,
    branch: str,
    root_url: str,
    cred: Credentials | None,
    temp_dir: str,
    org_external_id: str | None = None,
    follow_redirects: bool,
) -> CloneResult:
    folder_to_clone_root, stderr = await clone_repo(
        branch=branch,
        root_url=root_url,
        cred=cred,
        loaders=loaders,
        temp_dir=temp_dir,
        org_external_id=org_external_id,
        follow_redirects=follow_redirects,
    )
    if folder_to_clone_root is None:
        return CloneResult(success=False, message=stderr)

    success = await upload_cloned_repo_to_s3_tar(
        repo_path=folder_to_clone_root,
        group_name=group_name,
        nickname=root_nickname,
    )
    if not success:
        return CloneResult(success=False, message=stderr)

    try:
        repo = Repo(folder_to_clone_root, search_parent_directories=True)

        return CloneResult(
            success=success,
            commit=repo.head.commit.hexsha,
            commit_date=datetime.fromtimestamp(repo.head.commit.authored_date),
            message=stderr,
            repo=repo,
        )
    except (GitError, AttributeError) as exc:
        LOGGER.exception(
            exc,
            extra={
                "extra": {
                    "group_name": group_name,
                    "root_nickname": root_nickname,
                },
            },
        )

        return CloneResult(success=False, message=stderr)
