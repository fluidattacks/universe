import asyncio
import functools
from collections.abc import Callable
from typing import (
    Any,
    TypeVar,
    cast,
)

from opentelemetry.trace import (
    Tracer,
)

from integrates.batch.types import (
    BatchProcessing,
)
from integrates.dataloaders import (
    Dataloaders,
)

TVar = TypeVar("TVar")
TFun = TypeVar("TFun", bound=Callable[..., Any])


def is_observable(tracer: Tracer, process_name: str = "") -> Callable[[TVar], TVar]:
    """
    Starts a span for tracing the function using the OpenTelemetry tracer.

    Args:
        tracer (Tracer): OpenTelemetry tracer.
        process_name (str): Name of the process (overrides function name).

    """

    def wrapper(func: TVar) -> TVar:
        _func = cast(Callable[..., Any], func)

        @functools.wraps(_func)
        async def decorator_start_span(*args: tuple, **kwargs: dict) -> None:
            func_name = _func.__name__
            with tracer.start_as_current_span(process_name or func_name) as span:
                item = kwargs.get("item")

                if isinstance(item, BatchProcessing):
                    # A root process
                    span.set_attribute("job.name", func_name)
                    span.set_attribute("job.action_name", str(item.action_name))
                    span.set_attribute("job.entity", str(item.entity))
                    span.set_attribute("job.queue", str(item.queue))
                else:
                    # A child process
                    args_repr = [repr(a) for a in args if not isinstance(a, Dataloaders)]
                    kwargs_repr = [f"{k}={v!r}" for k, v in kwargs.items()]
                    signature = ", ".join(args_repr + kwargs_repr)
                    span.set_attribute("task.name", func_name)
                    span.set_attribute("task.params", signature)

                if asyncio.iscoroutinefunction(_func):
                    return await _func(*args, **kwargs)

                return _func(*args, **kwargs)

        return cast(TVar, decorator_start_span)

    return wrapper
