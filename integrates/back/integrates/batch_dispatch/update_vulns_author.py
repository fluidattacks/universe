import logging
import logging.config
import os
import tempfile

import fluidattacks_core.git as git_utils
from aioextensions import collect
from git import Repo

from integrates.batch.types import BatchProcessing
from integrates.custom_utils.roots import get_active_git_roots
from integrates.custom_utils.utils import ignore_advisories
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.roots.types import GitRoot
from integrates.db_model.vulnerabilities.enums import VulnerabilityStateStatus, VulnerabilityType
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityAuthor,
    VulnerabilityMetadataToUpdate,
)
from integrates.db_model.vulnerabilities.update import update_metadata
from integrates.roots.s3_mirror import download_repo
from integrates.settings import LOGGING

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger(__name__)


async def update_vuln_author(vuln: Vulnerability, repo_working_dir: str) -> None:
    commit_author = await git_utils.get_line_author(
        repo_path=repo_working_dir,
        filename=ignore_advisories(vuln.state.where),
        line=int(vuln.state.specific),
        rev=vuln.state.commit or "HEAD",
    )
    if not commit_author:
        LOGGER.info(
            "No commit author found for vulnerability",
            extra={
                "extra": {
                    "group_name": vuln.group_name,
                    "root_id": vuln.root_id,
                    "vuln_id": vuln.id,
                },
            },
        )
        return

    if commit_author.modified_date > vuln.created_date:
        LOGGER.info(
            "Commit author is unreliable",
            extra={
                "extra": {
                    "commit_author": commit_author,
                    "group_name": vuln.group_name,
                    "root_id": vuln.root_id,
                    "vuln_id": vuln.id,
                },
            },
        )
        return

    await update_metadata(
        finding_id=vuln.finding_id,
        root_id=vuln.root_id,
        vulnerability_id=vuln.id,
        metadata=VulnerabilityMetadataToUpdate(
            author=VulnerabilityAuthor(
                author_email=commit_author.author,
                commit=commit_author.hash,
                commit_date=commit_author.modified_date,
            ),
        ),
    )


async def get_vulns_to_process(
    loaders: Dataloaders,
    git_root: GitRoot,
) -> list[Vulnerability]:
    return [
        vuln
        for vuln in await loaders.root_vulnerabilities.load(git_root.id)
        if not vuln.author
        and vuln.state.commit
        and vuln.type == VulnerabilityType.LINES
        and int(vuln.state.specific) != 0
        and vuln.state.status == VulnerabilityStateStatus.VULNERABLE
    ]


async def update_root_vulns_author(git_root: GitRoot, modified_by: str, repo: Repo) -> None:
    LOGGER.info(
        "Updating vulnerabilities line author",
        extra={
            "extra": {
                "group_name": git_root.group_name,
                "modified_by": modified_by,
                "root_id": git_root.id,
                "root_nickname": git_root.state.nickname,
            },
        },
    )

    loaders: Dataloaders = get_new_context()
    vulns = await get_vulns_to_process(loaders, git_root)
    await collect(
        [update_vuln_author(vuln, str(repo.working_dir)) for vuln in vulns],
        workers=32,
    )

    LOGGER.info(
        "Line authors processed",
        extra={
            "extra": {
                "group_name": git_root.group_name,
                "modified_by": modified_by,
                "root_id": git_root.id,
                "root_nickname": git_root.state.nickname,
                "vulns": len(vulns),
            },
        },
    )


async def update_vulns_author(*, item: BatchProcessing) -> None:
    loaders: Dataloaders = get_new_context()
    group_name = item.entity
    root_ids = set(item.additional_info["root_ids"])
    valid_roots = [
        root for root in await get_active_git_roots(loaders, group_name) if root.id in root_ids
    ]
    LOGGER.info(
        "Roots to be processed",
        extra={
            "extra": {
                "group_name": group_name,
                "roots": [(root.id, root.state.nickname) for root in valid_roots],
            },
        },
    )

    for root in valid_roots:
        with tempfile.TemporaryDirectory(
            prefix=f"integrates_vulns_authors_{group_name}_",
            ignore_cleanup_errors=True,
        ) as tmpdir:
            os.chdir(tmpdir)
            repo_path = os.path.join(tmpdir, root.state.nickname)
            repo = await download_repo(
                root.group_name,
                root.state.nickname,
                tmpdir,
                root.state.gitignore,
            )
            if not repo:
                return

            os.chdir(repo_path)
            await update_root_vulns_author(root, item.subject, repo)
