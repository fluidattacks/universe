import logging
import logging.config

from aioextensions import (
    collect,
)

from integrates.batch.types import (
    BatchProcessing,
)
from integrates.custom_exceptions import (
    RepeatedToePort,
    ToePortAlreadyUpdated,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.roots.enums import (
    RootStatus,
)
from integrates.db_model.roots.types import (
    IPRoot,
)
from integrates.db_model.toe_ports.types import (
    RootToePortsRequest,
    ToePort,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.dynamodb.exceptions import (
    UnavailabilityError,
)
from integrates.settings import (
    LOGGING,
)
from integrates.toe.ports import (
    domain as toe_ports_domain,
)
from integrates.toe.ports.types import (
    ToePortAttributesToUpdate,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)


toe_ports_remove = retry_on_exceptions(exceptions=(UnavailabilityError,), sleep_seconds=5)(
    toe_ports_domain.remove,
)
toe_ports_update = retry_on_exceptions(exceptions=(UnavailabilityError,), sleep_seconds=5)(
    toe_ports_domain.update,
)


def get_non_present_toe_ports_to_update(
    root: IPRoot,
    root_toe_ports: list[ToePort],
) -> tuple[tuple[ToePort, ToePortAttributesToUpdate], ...]:
    LOGGER.info(
        "Getting non present toe ports to update",
        extra={
            "extra": {
                "repo_nickname": root.state.nickname,
            },
        },
    )
    return tuple(
        (
            toe_port,
            ToePortAttributesToUpdate(be_present=False),
        )
        for toe_port in root_toe_ports
        if root.state.status == RootStatus.INACTIVE and toe_port.state.be_present
    )


def get_present_toe_ports_to_update(
    root: IPRoot,
    root_toe_ports: list[ToePort],
) -> tuple[tuple[ToePort, ToePortAttributesToUpdate], ...]:
    LOGGER.info(
        "Getting present toe ports to update",
        extra={
            "extra": {
                "repo_nickname": root.state.nickname,
            },
        },
    )
    return tuple(
        (
            toe_port,
            ToePortAttributesToUpdate(be_present=True),
        )
        for toe_port in root_toe_ports
        if root.state.status == RootStatus.ACTIVE and not toe_port.state.be_present
    )


async def refresh_active_root_toe_ports(
    loaders: Dataloaders,
    group_name: str,
    root: IPRoot,
    modified_by: str,
) -> None:
    LOGGER.info(
        "Refreshing active toe ports",
        extra={
            "extra": {
                "repo_nickname": root.state.nickname,
            },
        },
    )
    root_toe_ports = await loaders.root_toe_ports.load_nodes(
        RootToePortsRequest(group_name=group_name, root_id=root.id),
    )
    present_toe_ports_to_update = get_present_toe_ports_to_update(root, root_toe_ports)
    await collect(
        tuple(
            toe_ports_update(
                current_value,
                attrs_to_update,
                modified_by,
                is_moving_toe_port=True,
            )
            for current_value, attrs_to_update in present_toe_ports_to_update
        ),
    )
    LOGGER.info(
        "Finish refreshing active toe ports",
        extra={
            "extra": {
                "repo_nickname": root.state.nickname,
            },
        },
    )


async def refresh_inactive_root_toe_ports(
    loaders: Dataloaders,
    group_name: str,
    root: IPRoot,
    modified_by: str,
) -> None:
    LOGGER.info(
        "Refreshing inactive toe ports",
        extra={
            "extra": {
                "repo_nickname": root.state.nickname,
            },
        },
    )
    root_toe_ports = await loaders.root_toe_ports.load_nodes(
        RootToePortsRequest(group_name=group_name, root_id=root.id),
    )
    non_present_toe_ports_to_update = get_non_present_toe_ports_to_update(root, root_toe_ports)
    await collect(
        tuple(
            toe_ports_update(
                current_value,
                attrs_to_update,
                modified_by,
                is_moving_toe_port=True,
            )
            for current_value, attrs_to_update in (non_present_toe_ports_to_update)
        ),
    )
    LOGGER.info(
        "Finish refreshing inactive toe ports",
        extra={
            "extra": {
                "repo_nickname": root.state.nickname,
            },
        },
    )


@retry_on_exceptions(
    exceptions=(
        RepeatedToePort,
        ToePortAlreadyUpdated,
    ),
)
async def refresh_root_toe_ports(group_name: str, root: IPRoot, modified_by: str) -> None:
    loaders: Dataloaders = get_new_context()
    if root.state.status == RootStatus.ACTIVE:
        await refresh_active_root_toe_ports(loaders, group_name, root, modified_by)
    else:
        await refresh_inactive_root_toe_ports(loaders, group_name, root, modified_by)


async def refresh_toe_ports(*, item: BatchProcessing) -> None:
    loaders: Dataloaders = get_new_context()
    group_name = item.entity
    root_ids = set(item.additional_info["root_ids"])
    valid_roots = [
        root
        for root in await loaders.group_roots.load(group_name)
        if root.id in root_ids and isinstance(root, IPRoot)
    ]
    LOGGER.info(
        "Roots to be processed",
        extra={
            "extra": {
                "group_name": group_name,
                "roots": [(root.id, root.state.nickname) for root in valid_roots],
            },
        },
    )

    for root in valid_roots:
        await refresh_root_toe_ports(group_name, root, item.subject)
