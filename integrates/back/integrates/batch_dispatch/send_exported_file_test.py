import integrates.mailer.groups
from integrates.batch.enums import Action, IntegratesBatchQueue
from integrates.batch.types import BatchProcessing
from integrates.batch_dispatch import send_exported_file
from integrates.custom_exceptions import OrgFindingPolicyNotFound
from integrates.custom_utils.datetime import get_utc_now
from integrates.dataloaders import get_new_context
from integrates.db_model.forces.types import GroupForcesExecutionsRequest
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    ExecutionVulnerabilitiesFaker,
    ForcesExecutionFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import raises

EMAIL_TEST = "admin@fluidattacks.com"
GROUP_NAME = "group1"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="admin")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            forces_execution=[
                ForcesExecutionFaker(
                    id="exec1",
                    group_name=GROUP_NAME,
                    vulnerabilities=ExecutionVulnerabilitiesFaker(
                        num_of_accepted_vulnerabilities=0,
                        num_of_open_vulnerabilities=0,
                        num_of_closed_vulnerabilities=3,
                        num_of_open_managed_vulnerabilities=3,
                        num_of_open_unmanaged_vulnerabilities=0,
                    ),
                ),
                ForcesExecutionFaker(
                    id="exec2",
                    group_name=GROUP_NAME,
                    commit="5a721993",
                    repo="repository2",
                    strictness="strict",
                    origin="https://test1.com",
                    vulnerabilities=ExecutionVulnerabilitiesFaker(
                        num_of_accepted_vulnerabilities=1,
                        num_of_open_vulnerabilities=1,
                        num_of_closed_vulnerabilities=1,
                        num_of_open_managed_vulnerabilities=1,
                        num_of_open_unmanaged_vulnerabilities=1,
                    ),
                ),
            ],
        ),
    ),
    others=[
        Mock(
            integrates.mailer.groups,
            "send_mail_group_report",
            "async",
            None,
        ),
    ],
)
async def test_send_exported_file() -> None:
    loaders = get_new_context()
    forces_executions = await loaders.group_forces_executions.load(
        GroupForcesExecutionsRequest(group_name=GROUP_NAME)
    )
    assert len(forces_executions) == 2

    action = BatchProcessing(
        action_name=Action.SEND_EXPORTED_FILE,
        entity=GROUP_NAME,
        subject="admin@fluidattacks.com",
        key="2",
        additional_info={},
        queue=IntegratesBatchQueue.SMALL,
        time=get_utc_now(),
    )
    await send_exported_file.send_exported_file(item=action)


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="admin")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
        ),
    ),
    others=[
        Mock(
            integrates.mailer.groups,
            "send_mail_group_report",
            "async",
            None,
        ),
    ],
)
async def test_send_exported_file_error() -> None:
    loaders = get_new_context()
    forces_executions = await loaders.group_forces_executions.load(
        GroupForcesExecutionsRequest(group_name=GROUP_NAME)
    )
    assert len(forces_executions) == 0

    action = BatchProcessing(
        action_name=Action.SEND_EXPORTED_FILE,
        entity=GROUP_NAME,
        subject="admin@fluidattacks.com",
        key="2",
        additional_info={},
        queue=IntegratesBatchQueue.SMALL,
        time=get_utc_now(),
    )
    with raises(OrgFindingPolicyNotFound):
        await send_exported_file.send_exported_file(item=action)
