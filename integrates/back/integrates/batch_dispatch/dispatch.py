import logging
import logging.config
import sys

from aioextensions import (
    collect,
    run,
)

from integrates.audit import AuditContext, add_audit_context
from integrates.batch import (
    dal as batch_dal,
)
from integrates.batch.types import (
    BatchProcessing,
    BatchProcessingToUpdate,
)
from integrates.batch.utils import (
    validate_additional_info_schema,
)
from integrates.batch_dispatch.bulk_jira_vulnerabilities import (
    bulk_jira_vulnerabilities,
)
from integrates.batch_dispatch.clone_roots import (
    clone_roots,
)
from integrates.batch_dispatch.deactivate_root import (
    deactivate_root,
)
from integrates.batch_dispatch.handle_finding_policy import (
    handle_finding_policy,
)
from integrates.batch_dispatch.move_environment import (
    move_environment,
)
from integrates.batch_dispatch.move_root import (
    move_root,
)
from integrates.batch_dispatch.rebase import (
    rebase,
)
from integrates.batch_dispatch.refresh_repositories import (
    refresh_repositories,
)
from integrates.batch_dispatch.refresh_toe_inputs import (
    refresh_toe_inputs,
)
from integrates.batch_dispatch.refresh_toe_lines import (
    refresh_toe_lines,
)
from integrates.batch_dispatch.refresh_toe_ports import (
    refresh_toe_ports,
)
from integrates.batch_dispatch.remove_group_resources import (
    remove_group_resources,
)
from integrates.batch_dispatch.report import (
    report,
)
from integrates.batch_dispatch.send_exported_file import (
    send_exported_file,
)
from integrates.batch_dispatch.update_group_languages import (
    update_group_languages,
)
from integrates.batch_dispatch.update_organization_priority_policies import (
    update_organization_priority_policies,
)
from integrates.batch_dispatch.update_organization_repositories import (
    update_organization_repositories,
)
from integrates.batch_dispatch.update_toe_inputs import (
    update_toe_inputs,
)
from integrates.batch_dispatch.update_vulns_author import (
    update_vulns_author,
)
from integrates.dynamodb.resource import (
    dynamo_shutdown,
    dynamo_startup,
)
from integrates.settings import (
    LOGGING,
)
from integrates.telemetry import (
    instrumentation,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)
ACTIONS = {
    act.__name__: act
    for act in [  # The action's name must match item.action_name
        bulk_jira_vulnerabilities,
        clone_roots,
        deactivate_root,
        handle_finding_policy,
        move_root,
        move_environment,
        rebase,
        refresh_repositories,
        refresh_toe_inputs,
        refresh_toe_lines,
        refresh_toe_ports,
        remove_group_resources,
        report,
        send_exported_file,
        update_group_languages,
        update_organization_priority_policies,
        update_organization_repositories,
        update_toe_inputs,
        update_vulns_author,
    ]
}


async def _queue_dependent_actions(item: BatchProcessing) -> None:
    if not item.dependent_actions:
        return

    if not all(
        validate_additional_info_schema(dependent.action_name, dependent.additional_info)
        for dependent in item.dependent_actions
    ):
        LOGGER.error(
            "Additional info schema invalid for dependent actions",
            extra={
                "extra": {
                    "action_key": item.key,
                    "action_name": item.action_name,
                    "entity": item.entity,
                    "dependents": item.dependent_actions,
                },
            },
        )
        return

    await collect(
        [
            batch_dal.put_action(
                action=dependent.action_name,
                additional_info=dependent.additional_info,
                attempt_duration_seconds=dependent.attempt_duration_seconds,
                entity=item.entity,
                queue=dependent.queue,
                subject=item.subject,
                job_name=dependent.job_name,
            )
            for dependent in item.dependent_actions
        ],
        workers=4,
    )


async def dispatch(action_dynamo_pk: str | None = None) -> None:
    try:
        action_dynamo_pk = action_dynamo_pk or sys.argv[1]
    except IndexError as ex:
        LOGGER.exception(ex, extra={"extra": locals()})
        return

    if not (item := await batch_dal.get_action(action_key=action_dynamo_pk)):
        LOGGER.error(
            "No jobs were found",
            extra={"extra": {"action_dynamo_pk": action_dynamo_pk}},
        )
        return

    add_audit_context(AuditContext(author=item.subject, mechanism="TASK"))
    action = item.action_name
    await batch_dal.update_action_to_dynamodb(
        action=action,
        action_key=item.key,
        attributes=BatchProcessingToUpdate(
            running=True,
        ),
    )

    if action in ACTIONS and validate_additional_info_schema(action, item.additional_info):
        await ACTIONS[action](item=item)
        await _queue_dependent_actions(item)
    else:
        LOGGER.error("Invalid action", extra={"extra": locals()})

    await batch_dal.delete_action(action_key=item.key)


async def main(action_dynamo_pk: str | None = None) -> None:
    await dynamo_startup()
    instrumentation.initialize("integrates-batch")
    try:
        await dispatch(action_dynamo_pk)
    finally:
        await dynamo_shutdown()


if __name__ == "__main__":
    run(main())
