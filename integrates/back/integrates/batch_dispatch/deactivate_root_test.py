from integrates.batch.enums import Action, IntegratesBatchQueue
from integrates.batch.types import BatchProcessing
from integrates.batch_dispatch.deactivate_root import deactivate_root
from integrates.dataloaders import get_new_context
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateReason,
    VulnerabilityStateStatus,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    DATE_2024,
    FindingFaker,
    GitRootFaker,
    GitRootStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
    VulnerabilityFaker,
    VulnerabilityStateFaker,
    random_uuid,
)
from integrates.testing.mocks import mocks

FIN_ID = "3c475384-834c-47b0-ac71-a41a022e401c"
EMAIL_TEST = "test@fluidattacks.com"
GROUP_NAME = "grouptest"
ORG_NAME = "orgtest"
ROOT_ID = "root1"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            findings=[FindingFaker(id=FIN_ID, group_name=GROUP_NAME)],
            organizations=[OrganizationFaker(id="org1", name=ORG_NAME)],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="admin", enrolled=True)],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(nickname="git1"),
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    id=random_uuid(),
                    created_by=EMAIL_TEST,
                    finding_id=FIN_ID,
                    root_id=ROOT_ID,
                    group_name=GROUP_NAME,
                    organization_name=ORG_NAME,
                    state=VulnerabilityStateFaker(
                        specific="12", commit="6be17a6aa6b2d0fae7b887af72e5fa59bda512c5"
                    ),
                ),
                VulnerabilityFaker(
                    id=random_uuid(),
                    created_by=EMAIL_TEST,
                    finding_id=FIN_ID,
                    root_id=ROOT_ID,
                    group_name=GROUP_NAME,
                    organization_name=ORG_NAME,
                    state=VulnerabilityStateFaker(
                        specific="1014", commit="c8dd25a1ade4e80cef986b49d8e15d1814994627"
                    ),
                ),
            ],
        ),
    ),
)
async def test_deactivate_root() -> None:
    # Act
    loaders = get_new_context()

    await deactivate_root(
        item=BatchProcessing(
            key=random_uuid(),
            action_name=Action.DEACTIVATE_ROOT,
            time=DATE_2024,
            entity=GROUP_NAME,
            subject=EMAIL_TEST,
            queue=IntegratesBatchQueue.SMALL,
            additional_info={"root_ids": [ROOT_ID]},
        )
    )
    vulns = await loaders.root_vulnerabilities.load(ROOT_ID)

    # Assert
    assert vulns
    assert len(vulns) == 2
    assert all(vuln.state.status == VulnerabilityStateStatus.SAFE for vuln in vulns)
    assert all(
        vuln.state.reasons == [VulnerabilityStateReason.ROOT_OR_ENVIRONMENT_DEACTIVATED]
        for vuln in vulns
    )
