from integrates.batch.types import (
    BatchProcessing,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.organizations.domain import (
    get_all_active_group_names,
)
from integrates.organizations.utils import (
    get_organization,
)
from integrates.outside_repositories.utils import (
    update_organization_repositories as update_repositories,
)


async def update_organization_repositories(*, item: BatchProcessing) -> None:
    organization_id = f'ORG#{item.entity.lstrip("org#")}'
    loaders = get_new_context()
    organization = await get_organization(loaders, organization_id)
    all_group_names = set(await get_all_active_group_names(loaders))

    await update_repositories(
        organization=organization,
        progress=0,
        all_group_names=all_group_names,
    )
