from datetime import datetime

from integrates.batch.enums import Action, IntegratesBatchQueue
from integrates.batch.types import BatchProcessing
from integrates.batch_dispatch.refresh_toe_inputs import refresh_toe_inputs
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.roots.enums import RootStatus
from integrates.db_model.toe_inputs.types import RootToeInputsRequest
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GitRootFaker,
    GitRootStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
    ToeInputFaker,
    ToeInputStateFaker,
    UrlRootFaker,
    UrlRootStateFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import freeze_time

EMAIL_TEST = "admin@fluidattacks.com"
CLIENT_EMAIL_TEST = "client@client_app.com"
GROUP_NAME = "group1"
ACTIVE_ROOT_ID = "root1"
INACTIVE_ROOT_ID = "root2"


@freeze_time("2024-10-14T00:00:00")
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="admin")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ACTIVE_ROOT_ID,
                    state=GitRootStateFaker(
                        nickname="back",
                    ),
                ),
                UrlRootFaker(
                    group_name="group1",
                    organization_name="org1",
                    root_id=INACTIVE_ROOT_ID,
                    state=UrlRootStateFaker(nickname="testurl", status=RootStatus.INACTIVE),
                ),
            ],
            toe_inputs=[
                ToeInputFaker(
                    component="https://myapp.com/",
                    entry_point="home",
                    group_name="group1",
                    root_id=ACTIVE_ROOT_ID,
                    state=ToeInputStateFaker(
                        be_present=False,
                    ),
                ),
                ToeInputFaker(
                    component="https://myapp.com/",
                    entry_point="about",
                    group_name="group1",
                    root_id=ACTIVE_ROOT_ID,
                    state=ToeInputStateFaker(
                        be_present=False,
                    ),
                ),
                ToeInputFaker(
                    component="https://myapp2.com/",
                    entry_point="api",
                    group_name="group1",
                    root_id=INACTIVE_ROOT_ID,
                    state=ToeInputStateFaker(
                        be_present=True,
                    ),
                ),
                ToeInputFaker(
                    component="https://myapp2.com/",
                    entry_point="test",
                    group_name="group1",
                    root_id=INACTIVE_ROOT_ID,
                    state=ToeInputStateFaker(
                        be_present=True,
                        seen_at=None,
                    ),
                ),
            ],
        ),
    )
)
async def test_refresh_toe_inputs_full() -> None:
    await refresh_toe_inputs(
        item=BatchProcessing(
            key="123456",
            action_name=Action.REFRESH_TOE_INPUTS,
            entity="group1",
            subject="hacker@fluidattacks.com",
            time=datetime.fromisoformat("2024-01-18T17:41:00+00:00"),
            additional_info={
                "root_ids": [ACTIVE_ROOT_ID, INACTIVE_ROOT_ID],
            },
            queue=IntegratesBatchQueue.SMALL,
        )
    )

    loaders: Dataloaders = get_new_context()
    root_toe_inputs = await loaders.root_toe_inputs.load_nodes(
        RootToeInputsRequest(group_name=GROUP_NAME, root_id=ACTIVE_ROOT_ID),
    )

    result_toe_inputs: list[dict[str, str | bool]] = [
        {
            "component": toe_input.component,
            "entry_point": toe_input.entry_point,
            "be_present": toe_input.state.be_present,
        }
        for toe_input in root_toe_inputs
    ]
    sorted_toe_inputs = sorted(
        result_toe_inputs, key=lambda x: f'{x["component"]}/{x["entry_point"]}'
    )

    expected_toe_inputs = [
        {"component": "https://myapp.com/", "entry_point": "about", "be_present": True},
        {"component": "https://myapp.com/", "entry_point": "home", "be_present": True},
    ]

    assert sorted_toe_inputs == expected_toe_inputs

    inactive_root_toe_inputs = await loaders.root_toe_inputs.load_nodes(
        RootToeInputsRequest(group_name=GROUP_NAME, root_id=INACTIVE_ROOT_ID),
    )
    assert len(inactive_root_toe_inputs) == 1
    assert all(not toe_input.state.be_present for toe_input in inactive_root_toe_inputs)
