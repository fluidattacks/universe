import logging
import logging.config
import os
import tempfile
from datetime import datetime, timedelta

from integrates.batch.types import BatchProcessing
from integrates.custom_exceptions import DocumentNotFound, OrgFindingPolicyNotFound
from integrates.custom_utils import datetime as datetime_utils
from integrates.custom_utils.files import list_to_csv
from integrates.custom_utils.reports import upload_report
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.forces.types import GroupForcesExecutionsRequest
from integrates.db_model.forces.utils import format_forces_to_file
from integrates.db_model.notifications.types import ReportStatus
from integrates.mailer import groups as groups_mail
from integrates.notifications.reports import update_report_notification
from integrates.settings import LOGGING

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)
TRANSACTIONS_LOGGER: logging.Logger = logging.getLogger("transactional")


async def send_exported_file(*, item: BatchProcessing) -> None:
    await update_report_notification(
        expiration_time=None,
        notification_id=item.additional_info.get("notification_id", ""),
        s3_file_path=None,
        size=None,
        status=ReportStatus.PROCESSING,
        user_email=item.subject,
    )
    TRANSACTIONS_LOGGER.info(
        "Processing send forces executions file",
        extra={
            "user_email": item.subject,
            "group_name": item.entity,
        },
    )

    group_name = item.entity
    user_email = item.subject
    today = datetime_utils.get_now()
    date = datetime_utils.get_as_str(today, date_format="%Y-%m-%dT%H-%M-%S")
    csv_file_path = os.path.join(
        tempfile.gettempdir(),
        f"devsecops-agent-{group_name}-{date}.csv",
    )
    loaders: Dataloaders = get_new_context()
    forces_executions = await loaders.group_forces_executions.load(
        GroupForcesExecutionsRequest(group_name=group_name),
    )
    if not forces_executions:
        raise OrgFindingPolicyNotFound()
    sorted_executions = sorted(forces_executions, key=lambda x: x.execution_date, reverse=True)
    forces_executions_items = [format_forces_to_file(forces) for forces in sorted_executions]

    try:
        content = list_to_csv(
            data_list=forces_executions_items,
            csv_file_path=csv_file_path,
        )
        if content:
            await update_report_notification(
                expiration_time=(datetime.now() + timedelta(days=7)),
                notification_id=item.additional_info.get("notification_id", ""),
                s3_file_path=f"reports/{await upload_report(csv_file_path)}",
                size=os.path.getsize(csv_file_path),
                status=ReportStatus.READY,
                user_email=item.subject,
            )

            subject = " Forces executions report for " + group_name
            await groups_mail.send_mail_group_report(
                loaders=loaders,
                email_to=[user_email],
                context={
                    "filetype": "CSV",
                    "fname": "user",
                    "date": datetime_utils.get_as_str(today, "%Y-%m-%d"),
                    "year": datetime_utils.get_as_str(today, "%Y"),
                    "time": datetime_utils.get_as_str(today, "%H:%M"),
                    "group_name": group_name,
                    "subject": subject,
                    "template": "send_exported_file",
                },
                report=True,
            )
    except Exception as ex:
        raise DocumentNotFound() from ex
