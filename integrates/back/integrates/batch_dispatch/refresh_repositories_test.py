from datetime import datetime

from integrates.batch.enums import Action, IntegratesBatchQueue
from integrates.batch.types import BatchProcessing
from integrates.batch_dispatch.refresh_repositories import refresh_repositories
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.credentials.types import HttpsSecret
from integrates.db_model.organizations.types import OrganizationUnreliableIndicators
from integrates.db_model.roots.enums import RootStatus
from integrates.organizations import (
    utils as orgs_utils,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    CredentialsFaker,
    GitRootFaker,
    GitRootStateFaker,
    GroupFaker,
    OrganizationFaker,
    OrganizationIntegrationRepositoryFaker,
)
from integrates.testing.mocks import mocks

ACTIVE_ROOT_ID = "root1"
INACTIVE_ROOT_ID = "root2"
GROUP_NAME = "group1"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ACTIVE_ROOT_ID,
                    state=GitRootStateFaker(nickname="back", use_ztna=True),
                ),
                GitRootFaker(
                    group_name=GROUP_NAME,
                    organization_name="org1",
                    id=INACTIVE_ROOT_ID,
                    state=GitRootStateFaker(
                        nickname="testurl",
                        status=RootStatus.INACTIVE,
                        url="https://gitlab.com/fluidattacks/test",
                    ),
                ),
            ],
            credentials=[
                CredentialsFaker(
                    credential_id="cred1",
                    organization_id="ORG#org1",
                    secret=HttpsSecret(user="user@test.test", password=""),
                )
            ],
            organization_integration_repositories=[
                OrganizationIntegrationRepositoryFaker(
                    id="549fa00adacf621dd920afb648fef37422d416da52a1902c378e8b62e5f46902",
                    organization_id="org1",
                    url="https://github.com/fluidattacks/makes.git",
                    credential_id="cred1",
                ),
                OrganizationIntegrationRepositoryFaker(
                    id="349fa00adacf621dd920afb648fef37422d416da52a1902c378e8b62e5f46909",
                    organization_id="org1",
                    url="https://github.com/test2",
                ),
            ],
        ),
    ),
)
async def test_successful_refresh_repositories() -> None:
    loaders: Dataloaders = get_new_context()

    group = await loaders.group.load(GROUP_NAME)
    assert group
    organization = await orgs_utils.get_organization(loaders, group.organization_id)
    assert organization
    organization_repositories = await loaders.organization_unreliable_outside_repositories.load(
        ("org1", None, None)
    )
    assert organization_repositories

    await refresh_repositories(
        item=BatchProcessing(
            key="2",
            action_name=Action.REFRESH_REPOSITORIES,
            entity=GROUP_NAME,
            subject="",
            time=datetime.now(),
            additional_info={},
            queue=IntegratesBatchQueue.SMALL,
        )
    )

    org_indicators = await loaders.organization_unreliable_indicators.load(organization.id)
    assert org_indicators == OrganizationUnreliableIndicators(
        covered_authors=0,
        covered_repositories=1,
        has_ztna_roots=True,
        missed_authors=0,
        missed_repositories=0,
        compliance_level=None,
        compliance_weekly_trend=None,
        estimated_days_to_full_compliance=None,
        standard_compliances=None,
    )
