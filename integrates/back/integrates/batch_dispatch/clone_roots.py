import asyncio
import logging
import logging.config
import shutil
import tempfile
from collections.abc import Iterable
from contextlib import suppress
from urllib.parse import urlparse
from uuid import uuid4

from aioextensions import collect
from fluidattacks_core.git.delete_files import delete_out_of_scope_files
from fluidattacks_core.git.download_repo import reset_repo
from fluidattacks_core.git.warp import (
    WarpError,
    is_using_split_tunnel,
    public_ip_ready,
    warp_cli,
    warp_cli_connect_virtual_network,
)

from integrates.batch import dal as batch_dal
from integrates.batch.enums import Action
from integrates.batch.types import BatchProcessing, BatchProcessingToUpdate
from integrates.batch_dispatch.rebase import rebase_root
from integrates.batch_dispatch.refresh_toe_lines import process_active_root_toe_lines
from integrates.batch_dispatch.update_vulns_author import update_root_vulns_author
from integrates.batch_dispatch.utils.git_self import CloneResult, clone_root
from integrates.custom_exceptions import UnavailabilityError
from integrates.custom_utils.constants import SCHEDULE_CLONE_GROUPS_ROOTS_EMAIL
from integrates.custom_utils.groups import get_group
from integrates.custom_utils.roots import (
    get_active_git_roots,
    get_root,
    get_unsolved_events_by_root,
)
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.credentials.types import AWSRoleSecret, Credentials, CredentialsRequest
from integrates.db_model.events.enums import EventSolutionReason, EventType
from integrates.db_model.roots.enums import RootCloningStatus, RootMachineStatus, RootStatus
from integrates.db_model.roots.types import GitRoot
from integrates.events.domain import add_cloning_issues_event
from integrates.findings.domain.events import solve_event_and_process_vulnerabilities
from integrates.jobs_orchestration.jobs import (
    PostCloningConfig,
    generate_post_cloning_config,
    queue_post_cloning_action,
    queue_sbom_job,
)
from integrates.jobs_orchestration.model.types import SbomFormat, SbomJobType
from integrates.organizations.utils import get_organization
from integrates.roots import utils as roots_utils
from integrates.settings import LOGGING
from integrates.unreliable_indicators.enums import EntityDependency
from integrates.unreliable_indicators.operations import update_unreliable_indicators_by_deps

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger(__name__)


INVALID_CREDENTIALS_MSG = "Invalid credentials"


def _get_domain(url: str) -> str | None:
    return urlparse(url).hostname


async def _log_split_tunnel_usage(roots: Iterable[GitRoot]) -> None:
    async def log_usage(git_root: GitRoot) -> None:
        if url_domain := _get_domain(git_root.state.url):
            is_split = await is_using_split_tunnel(url_domain)
            LOGGER.info("'%s' is using split tunnel: %s", url_domain, is_split)

    valid_roots = filter(lambda root: root.state and root.state.url, roots)
    await asyncio.gather(*map(log_usage, valid_roots))


async def _remove_root_id_from_action(action_key: str | None, root_id: str) -> None:
    if not action_key:
        return

    action = await batch_dal.get_action(action_key=action_key)
    if not action:
        return

    with suppress(UnavailabilityError, KeyError):
        root_ids = set(action.additional_info["git_root_ids"])
        root_ids.remove(root_id)
        if root_ids:
            await batch_dal.update_action_to_dynamodb(
                action=Action.CLONE_ROOTS,
                action_key=action_key,
                attributes=BatchProcessingToUpdate(
                    additional_info={
                        **action.additional_info,
                        "git_root_ids": list(root_ids),
                    },
                ),
            )


async def _warp_connect(group_name: str, network_name: str) -> None:
    try:
        await warp_cli("connect")
        await warp_cli_connect_virtual_network(network_name)
    except WarpError as ex:
        LOGGER.exception(
            ex,
            extra={"extra": {"group": group_name, "network": network_name}},
        )


async def _warp_connect_egress(group_name: str) -> None:
    await _warp_connect(group_name, "egress-ips")
    await public_ip_ready(expected_ip="104.30.132.78", attempts=10, seconds_per_attempt=5)


async def _queue_machine_post_cloning(
    *,
    group_name: str,
    modified_by: str,
    generated_config_files: tuple[PostCloningConfig, ...],
    post_cloning_config_uuid: str,
) -> None:
    batch_action = await queue_post_cloning_action(
        group_name=group_name,
        modified_by=modified_by,
        generated_config_files=generated_config_files,
        config_uuid=post_cloning_config_uuid,
    )
    await collect(
        [
            roots_utils.update_root_machine_status(
                group_name=config.root.group_name,
                root_id=config.root.id,
                status=RootMachineStatus.IN_PROGRESS,
                message="Queued after successful cloning",
                modified_by=modified_by,
            )
            for config in generated_config_files
        ],
        workers=8,
    )
    LOGGER.info(
        "Machine execution queued",
        extra={
            "extra": {
                "batch_action": str(batch_action),
                "roots": [
                    (config.root.id, config.root.state.nickname)
                    for config in generated_config_files
                ],
                "post_cloning_config_uuid": post_cloning_config_uuid,
            },
        },
    )


async def _queue_sbom(
    *,
    loaders: Dataloaders,
    modified_by: str,
    root: GitRoot,
) -> None:
    batch_action = await queue_sbom_job(
        loaders=loaders,
        group_name=root.group_name,
        root_nicknames=[root.state.nickname],
        sbom_format=SbomFormat.FLUID_JSON,
        modified_by=modified_by,
        job_type=SbomJobType.SCHEDULER,
    )
    LOGGER.info(
        "SBOM execution queued",
        extra={
            "extra": {
                "batch_action": {
                    "success": batch_action.success,
                    "batch_job_id": batch_action.batch_job_id,
                    "dynamo_pk": batch_action.dynamo_pk,
                },
                "group_name": root.group_name,
                "root_id": root.id,
                "root_nickname": root.state.nickname,
            },
        },
    )


async def solve_root_events(
    *,
    loaders: Dataloaders,
    root: GitRoot,
    reason: EventSolutionReason,
    user_info: dict[str, str],
    specific_type: EventType | None = None,
    other: str | None = None,
) -> None:
    unsolved_group_events = await get_unsolved_events_by_root(
        loaders=loaders,
        group_name=root.group_name,
        specific_type=specific_type,
    )
    unsolved_root_events = unsolved_group_events.get(root.id, None)
    if unsolved_root_events:
        for event in unsolved_root_events:
            reattacks_dict, verifications_dict = await solve_event_and_process_vulnerabilities(
                event_id=event.id,
                loaders=loaders,
                group_name=root.group_name,
                hacker_email=user_info["user_email"],
                reason=reason,
                user_info=user_info,
                other=other,
            )
            if bool(reattacks_dict):
                await update_unreliable_indicators_by_deps(
                    EntityDependency.request_vulnerabilities_verification,
                    finding_ids=list(reattacks_dict.keys()),
                    vulnerability_ids=[
                        vuln_id
                        for reattack_ids in reattacks_dict.values()
                        for vuln_id in reattack_ids
                    ],
                )
            if bool(verifications_dict):
                await update_unreliable_indicators_by_deps(
                    EntityDependency.verify_vulnerabilities_request,
                    finding_ids=list(verifications_dict.keys()),
                    vulnerability_ids=[
                        vuln_id
                        for verification_ids in verifications_dict.values()
                        for vuln_id in verification_ids
                    ],
                )


async def _clone_roots(
    *,
    loaders: Dataloaders,
    action_key: str,
    modified_by: str,
    roots: Iterable[GitRoot],
    organization_id: str,
    post_cloning_config_uuid: str,
    enable_concurrency: bool = True,
    should_queue_machine: bool = False,
    should_queue_sbom: bool = False,
) -> tuple[CloneResult, ...]:
    return await collect(
        [
            _clone_root(
                loaders=loaders,
                modified_by=modified_by,
                root=root,
                organization_id=organization_id,
                post_cloning_config_uuid=post_cloning_config_uuid,
                action_key=action_key,
                should_queue_machine=should_queue_machine,
                should_queue_sbom=should_queue_sbom,
            )
            for root in set(roots)
        ],
        workers=2 if enable_concurrency else 1,
    )


async def _clone_root(
    *,
    loaders: Dataloaders,
    modified_by: str,
    root: GitRoot,
    organization_id: str,
    post_cloning_config_uuid: str,
    action_key: str | None = None,
    should_queue_machine: bool = False,
    should_queue_sbom: bool = False,
) -> CloneResult:
    await roots_utils.update_root_cloning_status(
        loaders=loaders,
        group_name=root.group_name,
        root_id=root.id,
        status=RootCloningStatus.CLONING,
        message="Cloning in progress...",
        modified_by=modified_by,
    )
    root_cred: Credentials | None = (
        await loaders.credentials.load(
            CredentialsRequest(
                id=root.state.credential_id,
                organization_id=organization_id,
            ),
        )
        if root.state.credential_id
        else None
    )

    if not root_cred and not root.state.url.startswith("http"):
        await roots_utils.update_root_cloning_status(
            loaders=loaders,
            group_name=root.group_name,
            root_id=root.id,
            status=RootCloningStatus.FAILED,
            message=INVALID_CREDENTIALS_MSG,
            commit=root.cloning.commit,
            modified_by=modified_by,
        )
        await add_cloning_issues_event(
            loaders=loaders,
            git_root=root,
            message=INVALID_CREDENTIALS_MSG,
            branch=root.state.branch,
        )

        await _remove_root_id_from_action(action_key, root.id)

        return CloneResult(success=False, message=INVALID_CREDENTIALS_MSG)

    external_id: str | None = None
    if root_cred and isinstance(root_cred.secret, AWSRoleSecret):
        org = await loaders.organization.load(root.organization_name)
        external_id = org.state.aws_external_id if org else None

    with tempfile.TemporaryDirectory(
        prefix=f"integrates_clone_{root.group_name}_",
        ignore_cleanup_errors=True,
    ) as temp_dir:
        root_cloned = await clone_root(
            loaders=loaders,
            group_name=root.group_name,
            root_nickname=root.state.nickname,
            branch=root.state.branch,
            root_url=root.state.url,
            cred=root_cred,
            temp_dir=temp_dir,
            org_external_id=external_id,
            follow_redirects=root.state.use_egress or root.state.use_ztna or root.state.use_vpn,
        )
        if not root_cloned.success or root_cloned.repo is None:
            message = root_cloned.message or "Failed to clone without message"
            await roots_utils.update_root_cloning_status(
                loaders=loaders,
                group_name=root.group_name,
                root_id=root.id,
                status=RootCloningStatus.FAILED,
                message=message,
                commit=root_cloned.commit,
                commit_date=root_cloned.commit_date,
                modified_by=modified_by,
            )
            await add_cloning_issues_event(
                loaders=loaders,
                git_root=root,
                message=message,
                branch=root.state.branch,
            )
            shutil.rmtree(temp_dir, ignore_errors=True)

            await _remove_root_id_from_action(action_key, root.id)

            return CloneResult(success=False, message=message)

        await roots_utils.update_root_cloning_status(
            loaders=loaders,
            group_name=root.group_name,
            root_id=root.id,
            status=RootCloningStatus.OK,
            message="Cloned successfully",
            commit=root_cloned.commit,
            commit_date=root_cloned.commit_date,
            modified_by=modified_by,
        )
        await solve_root_events(
            loaders=loaders,
            root=root,
            reason=EventSolutionReason.CLONED_SUCCESSFULLY,
            user_info={
                "user_email": SCHEDULE_CLONE_GROUPS_ROOTS_EMAIL,
                "extra": "Event resolved by successful root cloning",
                "first_name": "-",
                "last_name": "-",
            },
            specific_type=EventType.CLONING_ISSUES,
        )

        await reset_repo(str(root_cloned.repo.working_dir))
        delete_out_of_scope_files(root.state.gitignore, str(root_cloned.repo.working_dir))
        await process_active_root_toe_lines(
            git_root=root,
            modified_by=modified_by,
            repo=root_cloned.repo,
        )
        await rebase_root(git_root=root, modified_by=modified_by, repo=root_cloned.repo)
        await update_root_vulns_author(
            git_root=root,
            modified_by=modified_by,
            repo=root_cloned.repo,
        )

        if should_queue_sbom:
            await _queue_sbom(loaders=loaders, modified_by=modified_by, root=root)

        if should_queue_machine and (
            config := await generate_post_cloning_config(
                loaders=loaders,
                root=root,
                repo_working_dir=str(root_cloned.repo.working_dir),
                config_uuid=post_cloning_config_uuid,
            )
        ):
            root_cloned = root_cloned._replace(config=config)

        await _remove_root_id_from_action(action_key, root.id)
        root_cloned = root_cloned._replace(repo=None)
        shutil.rmtree(temp_dir, ignore_errors=True)

        return root_cloned


async def clone_git_root_simple_args(
    *,
    group_name: str,
    git_root_id: str,
    user_email: str,
    should_queue_machine: bool = True,
    should_queue_sbom: bool = True,
) -> tuple[bool, str | None]:
    loaders = get_new_context()
    git_root = await get_root(loaders, git_root_id, group_name)
    group = await get_group(loaders, group_name)

    if (
        not git_root
        or not isinstance(git_root, GitRoot)
        or git_root.state.status != RootStatus.ACTIVE
    ):
        return False, "Root validations failed"

    post_cloning_config_uuid = str(uuid4())
    root_cloned = await _clone_root(
        loaders=loaders,
        modified_by=user_email,
        root=git_root,
        organization_id=group.organization_id,
        should_queue_machine=should_queue_machine,
        should_queue_sbom=should_queue_sbom,
        post_cloning_config_uuid=post_cloning_config_uuid,
    )
    if should_queue_machine and root_cloned.config:
        await _queue_machine_post_cloning(
            group_name=group_name,
            modified_by=user_email,
            generated_config_files=(root_cloned.config,),
            post_cloning_config_uuid=post_cloning_config_uuid,
        )

    return root_cloned.success, root_cloned.message


async def clone_roots(*, item: BatchProcessing) -> None:
    loaders: Dataloaders = get_new_context()
    group_name = item.entity
    user_email = item.subject
    should_queue_machine: bool = item.additional_info.get("should_queue_machine", True)
    should_queue_sbom: bool = item.additional_info.get("should_queue_sbom", True)
    root_ids = set(item.additional_info["git_root_ids"])
    valid_roots = [
        root for root in await get_active_git_roots(loaders, group_name) if root.id in root_ids
    ]
    LOGGER.info(
        "Roots to be processed",
        extra={
            "extra": {
                "group_name": group_name,
                "roots": [(root.id, root.state.nickname) for root in valid_roots],
            },
        },
    )

    group = await get_group(loaders, group_name)
    organization = await get_organization(loaders, group.organization_id)

    roots_by_use: dict[str, list] = {"egress": [], "ztna": [], "cluster": [], "codecommit": []}
    for root in valid_roots:
        if root.state.url.startswith("codecommit"):
            roots_by_use["codecommit"].append(root)
        elif root.state.use_egress:
            roots_by_use["egress"].append(root)
        elif root.state.use_ztna:
            roots_by_use["ztna"].append(root)
        else:
            roots_by_use["cluster"].append(root)

    clone_results: list[CloneResult] = []
    post_cloning_config_uuid = str(uuid4())
    if roots_by_use["cluster"]:
        clone_results += await _clone_roots(
            loaders=loaders,
            action_key=item.key,
            modified_by=user_email,
            roots=roots_by_use["cluster"],
            organization_id=organization.id,
            post_cloning_config_uuid=post_cloning_config_uuid,
            should_queue_machine=should_queue_machine,
            should_queue_sbom=should_queue_sbom,
        )

    if roots_by_use["codecommit"]:
        clone_results += await _clone_roots(
            loaders=loaders,
            action_key=item.key,
            modified_by=user_email,
            roots=roots_by_use["codecommit"],
            organization_id=organization.id,
            post_cloning_config_uuid=post_cloning_config_uuid,
            should_queue_machine=should_queue_machine,
            should_queue_sbom=should_queue_sbom,
        )

    if roots_by_use["egress"]:
        await _warp_connect_egress(group_name)
        await _log_split_tunnel_usage(roots_by_use["egress"])
        clone_results += await _clone_roots(
            loaders=loaders,
            action_key=item.key,
            modified_by=user_email,
            roots=roots_by_use["egress"],
            organization_id=organization.id,
            post_cloning_config_uuid=post_cloning_config_uuid,
            enable_concurrency=False,
            should_queue_machine=should_queue_machine,
            should_queue_sbom=should_queue_sbom,
        )

    if roots_by_use["ztna"]:
        await _warp_connect(group_name, organization.name)
        clone_results += await _clone_roots(
            loaders=loaders,
            action_key=item.key,
            modified_by=user_email,
            roots=roots_by_use["ztna"],
            organization_id=organization.id,
            post_cloning_config_uuid=post_cloning_config_uuid,
            enable_concurrency=False,
            should_queue_machine=should_queue_machine,
            should_queue_sbom=should_queue_sbom,
        )

    if should_queue_machine:
        generated_config_files = tuple(result.config for result in clone_results if result.config)
        await _queue_machine_post_cloning(
            group_name=group_name,
            modified_by=user_email,
            generated_config_files=generated_config_files,
            post_cloning_config_uuid=post_cloning_config_uuid,
        )
