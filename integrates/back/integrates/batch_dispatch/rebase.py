import difflib
import logging
import logging.config
import os
import tempfile
from contextlib import suppress
from datetime import datetime, timedelta
from subprocess import SubprocessError

import fluidattacks_core.git as git_utils
from aioextensions import collect
from fluidattacks_core.serializers.snippet import (
    SnippetViewport,
    make_snippet,
    make_snippet_function,
)
from fluidattacks_core.serializers.syntax import get_language_from_path
from git import Commit, GitCommandError, GitError
from git.repo.base import Repo
from more_itertools import chunked

from integrates.batch.types import BatchProcessing
from integrates.custom_exceptions import (
    FindingNotFound,
    InvalidFileType,
    InvalidVulnerabilityAlreadyExists,
    LineDoesNotExistInTheLinesOfCodeRange,
    VulnerabilityPathDoesNotExistInToeLines,
    VulnerabilityUrlFieldDoNotExistInToeInputs,
    VulnNotFound,
)
from integrates.custom_utils import datetime as datetime_utils
from integrates.custom_utils.roots import get_active_git_roots
from integrates.custom_utils.utils import ignore_advisories
from integrates.custom_utils.vulnerabilities import is_reattack_requested
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model import vulnerabilities as vulns_model
from integrates.db_model.roots.enums import RootRebaseStatus
from integrates.db_model.roots.types import GitRoot
from integrates.db_model.vulnerabilities.enums import VulnerabilityStateStatus, VulnerabilityType
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityRequest,
    VulnerabilityState,
)
from integrates.decorators import retry_on_exceptions
from integrates.dynamodb.exceptions import ValidationException
from integrates.roots.s3_mirror import download_repo
from integrates.roots.utils import update_root_rebase_status
from integrates.settings import LOGGING
from integrates.unreliable_indicators.enums import EntityDependency
from integrates.unreliable_indicators.operations import update_unreliable_indicators_by_deps
from integrates.vulnerabilities.domain import generate_snippet
from integrates.vulnerabilities.domain.rebase import close_vulnerability
from integrates.vulnerabilities.domain.rebase import rebase as rebase_vulnerability
from integrates.vulnerabilities.domain.snippet import search_snippet, set_snippet
from integrates.vulnerabilities.fixes.utils import get_vulnerability_language

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger("rebase")

git_is_commit_in_branch = retry_on_exceptions(
    exceptions=(
        FileNotFoundError,
        IndexError,
        OSError,
        SubprocessError,
        ValueError,
    ),
)(git_utils.is_commit_in_branch)
git_rebase = retry_on_exceptions(
    exceptions=(
        FileNotFoundError,
        IndexError,
        OSError,
        SubprocessError,
        ValueError,
    ),
)(git_utils.rebase)


def generate_patch(string1: str, string2: str) -> str:
    # Split the strings into lines for comparison
    lines1 = string1.splitlines(keepends=True)
    lines2 = string2.splitlines(keepends=True)

    # Generate the diff
    diff = list(difflib.unified_diff(lines1, lines2))
    return "".join(diff)


def get_file_content(
    repo: Repo,
    commit_hash: str,
    file_path: str,
) -> bytes | None:
    commit = repo.commit(commit_hash)

    blob = commit.tree / file_path
    file_content = blob.data_stream.read()

    return file_content


def get_recent_commit_after(
    repo: Repo,
    file_path: str,
    target_date: datetime,
) -> tuple[str, str] | None:
    try:
        return _process_commit_history(repo, file_path, target_date)
    except GitError:
        return None


def _process_commit_history(
    repo: Repo,
    file_path: str,
    target_date: datetime,
) -> tuple[str, str] | None:
    logs: str = repo.git.log(
        "--reverse",
        "--follow",
        "--format=%H-%P",
        "--name-status",
        "--",
        file_path,
    )
    if not logs:
        return None

    commits_candidate = []
    last_known_path = file_path
    chunks: list[list[str]] = list(chunked(logs.split("\n"), 3))

    for commit_hash, _, file_changes in chunks:
        commit, last_known_path = _process_commit(
            repo,
            commit_hash,
            file_changes,
            last_known_path,
        )
        commits_candidate.append((commit, last_known_path))
        if commit.committed_datetime >= target_date:
            break

    if not commits_candidate:
        return None

    return _find_most_recent_valid_commit(repo, commits_candidate)


def _process_commit(
    repo: Repo,
    commit_hash: str,
    file_changes: str,
    last_known_path: str,
) -> tuple[Commit, str]:
    commit = repo.commit(commit_hash.split("-")[0])
    change_parts = file_changes.split("\t")

    if change_parts[0].startswith(("R", "C")):
        old_path, new_path = change_parts[1:]
        if old_path == last_known_path:
            last_known_path = new_path
    elif change_parts[0] in {"A", "M"} and change_parts[1] == last_known_path:
        last_known_path = change_parts[1]
    elif change_parts[0] == "D":
        commit = repo.commit(commit_hash.split("-")[1])

    return commit, last_known_path


def _find_most_recent_valid_commit(
    repo: Repo,
    commits_candidate: list[tuple[Commit, str]],
) -> tuple[str, str] | None:
    for commit, path in reversed(commits_candidate):
        try:
            get_file_content(repo, commit.hexsha, path)
            return commit.hexsha, path
        except KeyError:
            continue
    return None


async def _get_vulnerabilities_to_process(
    loaders: Dataloaders,
    git_root: GitRoot,
    head_commit: str,
    *,
    safe: bool = False,
) -> list[Vulnerability]:
    f117_ids = {
        finding.id
        for finding in await loaders.group_findings.load(git_root.group_name)
        if finding.title.startswith("117.")
    }
    root_vulns = await loaders.root_vulnerabilities.load(git_root.id)
    vuln_to_process = [
        vuln
        for vuln in root_vulns
        if vuln.state.commit
        and (vuln.state.commit != head_commit if not safe else True)
        and vuln.type == VulnerabilityType.LINES
        and int(vuln.state.specific) != 0
        and (
            (
                vuln.state.status
                not in (
                    VulnerabilityStateStatus.REJECTED,
                    VulnerabilityStateStatus.SUBMITTED,
                    VulnerabilityStateStatus.VULNERABLE,
                )
            )
            if safe
            else (
                vuln.state.status
                in (
                    VulnerabilityStateStatus.REJECTED,
                    VulnerabilityStateStatus.SUBMITTED,
                    VulnerabilityStateStatus.VULNERABLE,
                )
            )
        )
        and vuln.finding_id not in f117_ids
    ]

    return vuln_to_process


async def _is_commit_valid(commit: str, repo_working_dir: str, branch: str) -> bool:
    return commit != "0000000000000000000000000000000000000000" and await git_is_commit_in_branch(
        repo_working_dir,
        branch,
        commit,
    )


def _is_rebase_valid(result: git_utils.RebaseResult | None, head_commit_hexsha: str) -> bool:
    return result is not None and result.rev == head_commit_hexsha


async def _rebase_vulnerability(
    git_root: GitRoot,
    repo: Repo,
    vulnerability: Vulnerability,
) -> git_utils.RebaseResult | None:
    latest_commit = vulnerability.state.commit
    result: git_utils.RebaseResult | None = None

    if latest_commit and await _is_commit_valid(
        latest_commit,
        str(repo.working_dir),
        git_root.state.branch,
    ):
        result = git_rebase(
            repo,
            path=ignore_advisories(vulnerability.state.where),
            line=int(vulnerability.state.specific),
            rev_a=latest_commit,
            rev_b="HEAD",
        )

    if not _is_rebase_valid(result, repo.head.commit.hexsha):
        # Rebase was not successful with the latest vuln commit,
        # give it a try with previous commits in the historic state
        result = None
        loaders = get_new_context()
        historic_state = await loaders.vulnerability_historic_state.load(
            VulnerabilityRequest(
                vulnerability_id=vulnerability.id,
                finding_id=vulnerability.finding_id,
            ),
        )
        previous_commits: set[str] = {
            state.commit
            for state in historic_state
            if state.commit and state.commit != latest_commit
        }
        for commit in previous_commits:
            if not await _is_commit_valid(commit, str(repo.working_dir), git_root.state.branch):
                continue

            state = next(state for state in reversed(historic_state) if state.commit == commit)
            rebase_result = git_rebase(
                repo,
                path=ignore_advisories(state.where),
                line=int(state.specific),
                rev_a=commit,
                rev_b="HEAD",
            )
            if _is_rebase_valid(rebase_result, repo.head.commit.hexsha):
                result = rebase_result
                break

    if not _is_rebase_valid(result, repo.head.commit.hexsha):
        LOGGER.error(
            "Failed to rebase vulnerability",
            extra={
                "extra": {
                    "group_name": git_root.group_name,
                    "head_commit": repo.head.commit.hexsha,
                    "root_id": git_root.id,
                    "root_nickname": git_root.state.nickname,
                    "status": RootRebaseStatus.FAILED,
                    "vulnerability_commit": vulnerability.state.commit,
                    "vulnerability_id": vulnerability.id,
                },
            },
        )

    return result


async def _rebase_vulnerability_integrates(
    *,
    loaders: Dataloaders,
    finding_id: str,
    finding_vulns_data: tuple[Vulnerability, ...],
    git_root: GitRoot,
    vulnerability_commit: str,
    vulnerability_id: str,
    vulnerability_where: str,
    vulnerability_specific: str,
) -> None:
    vuln_to_update: Vulnerability = next(
        vuln for vuln in finding_vulns_data if vuln.id == vulnerability_id
    )

    if (
        vuln_to_update.state.commit == vulnerability_commit
        and vuln_to_update.state.specific == vulnerability_specific
        and vuln_to_update.state.where == vulnerability_where
    ):
        return

    with suppress(VulnNotFound):
        try:
            await rebase_vulnerability(
                loaders=loaders,
                finding_id=finding_id,
                finding_vulns=finding_vulns_data,
                vuln_to_update=vuln_to_update,
                vulnerability_commit=vulnerability_commit,
                vulnerability_where=vulnerability_where,
                vulnerability_specific=vulnerability_specific,
            )
        except (
            VulnerabilityPathDoesNotExistInToeLines,
            LineDoesNotExistInTheLinesOfCodeRange,
            VulnerabilityUrlFieldDoNotExistInToeInputs,
            InvalidVulnerabilityAlreadyExists,
            FindingNotFound,
        ) as exc:
            local_vars = locals()
            local_vars.pop("loaders", None)
            local_vars.pop("finding_vulns_data", None)
            LOGGER.error(
                "Failed to rebase vulnerability in integrates",
                extra={
                    "extra": {
                        "exception": str(exc),
                        "group_name": git_root.group_name,
                        "root_id": git_root.id,
                        "root_nickname": git_root.state.nickname,
                        "status": RootRebaseStatus.FAILED,
                        **local_vars,
                    },
                },
            )


async def _update_vulnerability_snippet(
    loaders: Dataloaders, vulnerability_id: str, repo: Repo
) -> None:
    vulnerability = await (loaders.vulnerability.clear(vulnerability_id)).load(vulnerability_id)
    if not vulnerability:
        return
    try:
        file_content = repo.git.show(f"HEAD:{ignore_advisories(vulnerability.state.where)}")
    except (GitCommandError, ValueError):
        return

    try:
        language = get_vulnerability_language(vulnerability)
    except InvalidFileType as ex:
        LOGGER.exception(
            ex,
            extra={
                "extra": {
                    "vuln_id": vulnerability_id,
                    "vuln_where": vulnerability.state.where,
                },
            },
        )
        return

    snippet = make_snippet_function(
        file_content=file_content,
        language=language,
        viewport=SnippetViewport(
            int(vulnerability.state.specific),
            show_line_numbers=False,
            highlight_line_number=False,
        ),
    )
    if not snippet:
        snippet = make_snippet(
            content=file_content,
            viewport=SnippetViewport(
                int(vulnerability.state.specific),
                line_context=8,
                show_line_numbers=False,
                highlight_line_number=False,
            ),
        )
    await set_snippet(
        contents=snippet,
        vulnerability_id=vulnerability.id,
    )


def _find_state(
    states: list[VulnerabilityState],
    status: tuple[VulnerabilityStateStatus, ...],
    reversed_: bool = False,
) -> VulnerabilityState | None:
    return next(
        (state for state in (reversed(states) if reversed_ else states) if state.status in status),
        None,
    )


async def _update_vulnerability_snippet_safe(  # noqa: PLR0911
    loaders: Dataloaders,
    repo: Repo,
    vulnerability: Vulnerability,
) -> Vulnerability | None:
    # 1. Get historic states, sorted descending by modified date
    states = sorted(
        await loaders.vulnerability_historic_state.load(
            VulnerabilityRequest(vulnerability.finding_id, vulnerability.id)
        ),
        key=lambda x: x.modified_date,
        reverse=True,
    )

    # 2. Identify 'vulnerable' and 'safe' states
    state_vulnerable = _find_state(
        states,
        (
            VulnerabilityStateStatus.REJECTED,
            VulnerabilityStateStatus.SUBMITTED,
            VulnerabilityStateStatus.VULNERABLE,
        ),
    )
    state_safe = _find_state(
        states,
        (VulnerabilityStateStatus.SAFE,),
        True,
    )

    # 3. Early return if we don't have both states
    if not state_vulnerable or not state_safe:
        return None

    # 4. Additional snippet checks: skip if vulnerable already has a snippet
    # AND there's a subsequent state with a snippet that is a function.
    # This is the original nested condition flattened.
    if (
        await loaders.vulnerability_snippet.load((vulnerability.id, state_vulnerable.modified_date))
        is not None
        and state_safe
        and (
            snippet_safe := await loaders.vulnerability_snippet.load(
                (
                    vulnerability.id,
                    state_vulnerable.modified_date,
                )
            )
        )
        and snippet_safe.is_function
    ):
        return None

    # 5. Make sure the repo recognizes the vulnerable commit; if not, attempt to update it
    if not repo.is_valid_object(state_vulnerable.commit or ""):
        commit_vulnerable = get_recent_commit_after(
            repo, state_vulnerable.where, state_vulnerable.modified_date
        )
        if not commit_vulnerable:
            return None
        state_vulnerable = state_vulnerable._replace(
            commit=commit_vulnerable[0],
            where=commit_vulnerable[1],
        )

    # 6. Generate the vulnerable snippet
    snippet_vulnerable = generate_snippet(state_vulnerable, repo, force=False)
    if not snippet_vulnerable:
        return None

    # 7. If both states share the same commit, we need to find a more recent
    # commit for the safe state
    if state_vulnerable.commit == state_safe.commit:
        commit_safe = get_recent_commit_after(repo, state_safe.where, state_safe.modified_date)
        if not (
            commit_safe
            and state_vulnerable.commit
            and commit_safe[0] != state_vulnerable.commit
            and repo.is_valid_object(commit_safe[0])
        ):
            return None
        state_safe = state_safe._replace(commit=commit_safe[0], where=commit_safe[1])

    snippet_safe = await search_snippet(
        loaders, repo, vulnerability.id, state_vulnerable, state_safe
    )

    if not snippet_safe:
        return None

    patch = generate_patch(snippet_vulnerable.content, snippet_safe.content)
    if not patch:
        return None

    # 8. Everything is valid, so update states and commit changes
    await set_snippet(
        contents=snippet_safe,
        vulnerability_id=vulnerability.id,
        loaders=loaders,
    )

    with suppress(ValidationException, VulnNotFound):
        await vulns_model.update_new_historic(
            current_value=vulnerability,
            historic=(
                *states,
                state_vulnerable._replace(
                    modified_date=state_vulnerable.modified_date + timedelta(milliseconds=777),
                    modified_by="rebase_snippet@fluidattacks.com",
                ),
                state_safe._replace(
                    modified_date=datetime_utils.get_utc_now(),
                    modified_by="rebase_snippet@fluidattacks.com",
                ),
            ),
        )
        return vulnerability

    return None


async def rebase_root(
    git_root: GitRoot,
    modified_by: str,
    repo: Repo,
) -> tuple[bool, str]:
    await update_root_rebase_status(
        group_name=git_root.group_name,
        root_id=git_root.id,
        status=RootRebaseStatus.IN_PROGRESS,
        message="Starting rebase for root",
        modified_by=modified_by,
    )

    loaders: Dataloaders = get_new_context()
    vulnerabilities_safe = await _get_vulnerabilities_to_process(
        loaders, git_root, repo.head.commit.hexsha, safe=True
    )
    vulnerabilities_safe = [
        vuln
        for vuln in vulnerabilities_safe
        if get_language_from_path(ignore_advisories(vuln.state.where))
        not in ("yaml", "json", "hcl", "txt", "xml", None)
        and not any(
            x in vuln.state.where.lower()
            for x in (
                "test",
                "plugin",
                "library",
                ".min",
                "libs",
                "static",
                "assets",
                "/js/",
                "themes",
                "node_modules",
                "migrat",
            )
        )
    ]
    fixed_vulnerabilities = [
        x
        for x in (
            await collect(
                tuple(
                    _update_vulnerability_snippet_safe(loaders, repo, vuln)
                    for vuln in vulnerabilities_safe
                ),
                workers=30,
            )
        )
        if x
    ]
    LOGGER.info(
        "Fixed vulnerabilities",
        extra={"extra": {"total": len(fixed_vulnerabilities), "root_id": git_root.id}},
    )

    vulnerabilities = await _get_vulnerabilities_to_process(
        loaders,
        git_root,
        repo.head.commit.hexsha,
    )
    rebase_results = await collect(
        tuple(
            _rebase_vulnerability(
                git_root,
                repo,
                vuln,
            )
            for vuln in vulnerabilities
        ),
        workers=16,
    )
    to_rebase: tuple[
        tuple[git_utils.RebaseResult, Vulnerability],
        ...,
    ] = tuple(
        (rebase_result, vuln)
        for rebase_result, vuln in zip(
            rebase_results,
            vulnerabilities,
            strict=False,
        )
        if rebase_result
        and (
            rebase_result.path != vuln.state.where or str(rebase_result.line) != vuln.state.specific
        )
        and rebase_result.rev == repo.head.commit.hexsha
    )
    await collect(
        [
            _rebase_vulnerability_integrates(
                loaders=loaders,
                finding_id=vuln.finding_id,
                finding_vulns_data=tuple(
                    item for item in vulnerabilities if item.finding_id == vuln.finding_id
                ),
                git_root=git_root,
                vulnerability_commit=rebase_result.rev,
                vulnerability_id=vuln.id,
                vulnerability_where=rebase_result.path,
                vulnerability_specific=str(rebase_result.line),
            )
            for rebase_result, vuln in to_rebase
        ],
        workers=16,
    )
    LOGGER.info(
        "Vulnerabilities rebased",
        extra={
            "extra": {
                "group_name": git_root.group_name,
                "len_vulns": len(to_rebase),
                "root_id": git_root.id,
                "root_nickname": git_root.state.nickname,
                "status": RootRebaseStatus.IN_PROGRESS,
            },
        },
    )

    to_close: list[tuple[git_utils.RebaseResult, Vulnerability]] = [
        (rebase_result, vuln)
        for rebase_result, vuln in zip(rebase_results, vulnerabilities, strict=False)
        if rebase_result
        and rebase_result.rev != repo.head.commit.hexsha
        and vuln.created_by == "machine@fluidattacks.com"
        and not is_reattack_requested(vuln)
    ]
    await collect(
        [
            close_vulnerability(
                loaders,
                vuln,
                rebase_result.rev,
                rebase_result.path,
                str(rebase_result.line),
            )
            for rebase_result, vuln in to_close
        ],
        workers=16,
    )
    await update_unreliable_indicators_by_deps(
        EntityDependency.upload_file,
        finding_ids=list({vuln.finding_id for _, vuln in to_close}),
        vulnerability_ids=list({vuln.id for _, vuln in to_close}),
    )
    LOGGER.info(
        "Vulnerabilities closed",
        extra={
            "extra": {
                "group_name": git_root.group_name,
                "len_vulns": len(to_close),
                "root_id": git_root.id,
                "root_nickname": git_root.state.nickname,
                "status": RootRebaseStatus.IN_PROGRESS,
            },
        },
    )

    await collect(
        tuple(
            _update_vulnerability_snippet(
                loaders=loaders,
                vulnerability_id=vulnerability.id,
                repo=repo,
            )
            for vulnerability in vulnerabilities
        ),
        workers=16,
    )

    message = "Git root has been rebased"
    await update_root_rebase_status(
        group_name=git_root.group_name,
        root_id=git_root.id,
        status=RootRebaseStatus.SUCCESS,
        message=message,
        modified_by=modified_by,
        commit=repo.head.commit.hexsha,
        commit_date=datetime.fromtimestamp(repo.head.commit.authored_date),
    )

    return True, message


async def rebase(*, item: BatchProcessing) -> None:
    loaders: Dataloaders = get_new_context()
    group_name = item.entity
    root_ids = set(item.additional_info["root_ids"])
    valid_roots = [
        root for root in await get_active_git_roots(loaders, group_name) if root.id in root_ids
    ]
    LOGGER.info(
        "Roots to be processed",
        extra={
            "extra": {
                "group_name": group_name,
                "roots": [(root.id, root.state.nickname) for root in valid_roots],
            },
        },
    )

    for root in valid_roots:
        with tempfile.TemporaryDirectory(
            prefix=f"integrates_rebase_{group_name}_",
            ignore_cleanup_errors=True,
        ) as tmpdir:
            os.chdir(tmpdir)
            repo = await download_repo(
                root.group_name,
                root.state.nickname,
                tmpdir,
                root.state.gitignore,
            )
            if not repo:
                return
            os.chdir(repo.common_dir)
            await rebase_root(root, item.subject, repo)
