import itertools
import logging
import logging.config
import uuid
from collections.abc import Callable, Iterator
from datetime import datetime
from operator import attrgetter
from typing import Any

from aioextensions import collect
from opentelemetry import trace

from integrates.batch.types import BatchProcessing
from integrates.batch_dispatch.utils.telemetry import is_observable
from integrates.custom_exceptions import RepeatedToeInput, ToeInputAlreadyUpdated, ToeInputNotFound
from integrates.custom_utils import filter_vulnerabilities as filter_vulns_utils
from integrates.custom_utils import findings as findings_utils
from integrates.custom_utils import roots as roots_utils
from integrates.custom_utils.stakeholders import get_stakeholder
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model import findings as findings_model
from integrates.db_model import roots as roots_model
from integrates.db_model import toe_inputs as toe_inputs_model
from integrates.db_model import vulnerabilities as vulns_model
from integrates.db_model.constants import MACHINE_EMAIL
from integrates.db_model.enums import Notification, TreatmentStatus
from integrates.db_model.findings.enums import FindingStateStatus
from integrates.db_model.findings.types import Finding, FindingState
from integrates.db_model.roots.types import (
    Root,
    RootEnvironmentSecretsRequest,
    RootEnvironmentUrl,
    RootEnvironmentUrlRequest,
)
from integrates.db_model.stakeholders.types import Stakeholder
from integrates.db_model.toe_inputs.types import GroupToeInputsRequest, ToeInput, ToeInputRequest
from integrates.db_model.types import Treatment
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateReason,
    VulnerabilityStateStatus,
    VulnerabilityType,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityRequest,
    VulnerabilityState,
    VulnerabilityVerification,
    VulnerabilityZeroRisk,
)
from integrates.decorators import retry_on_exceptions
from integrates.dynamodb.exceptions import UnavailabilityError
from integrates.mailer import utils as mailer_utils
from integrates.mailer.common import send_mails_async
from integrates.roots import (
    domain as roots_domain,
)
from integrates.roots import (
    environments as roots_environments,
)
from integrates.settings import LOGGING
from integrates.toe.inputs import domain as toe_inputs_domain
from integrates.toe.inputs.types import ToeInputAttributesToAdd, ToeInputAttributesToUpdate
from integrates.vulnerabilities import domain as vulns_domain

logging.config.dictConfig(LOGGING)

# Constants
TRACER = trace.get_tracer("batch")
LOGGER = logging.getLogger(__name__)


toe_inputs_add = retry_on_exceptions(exceptions=(UnavailabilityError,), sleep_seconds=5)(
    toe_inputs_domain.add,
)
toe_inputs_update = retry_on_exceptions(exceptions=(UnavailabilityError,), sleep_seconds=5)(
    toe_inputs_domain.update,
)
toe_inputs_remove_bulk = retry_on_exceptions(exceptions=(UnavailabilityError,), sleep_seconds=5)(
    toe_inputs_model.remove_toe_inputs,
)


async def _update_historic(
    loaders: Dataloaders,
    new_id: str,
    modified_date: datetime,
    item_subject: str,
    to_update: Callable,
    historic_state: list[VulnerabilityState],
    historic_treatment: list[Treatment],
    historic_verification: list[VulnerabilityVerification],
    historic_zero_risk: list[VulnerabilityZeroRisk],
) -> None:
    new_historic_treatment = Treatment(
        modified_by=item_subject,
        modified_date=modified_date,
        justification="Environment was moved to the current root",
        status=historic_treatment[-1].status if historic_treatment else TreatmentStatus.UNTREATED,
        acceptance_status=historic_treatment[-1].acceptance_status if historic_treatment else None,
        accepted_until=historic_treatment[-1].accepted_until if historic_treatment else None,
        assigned=historic_treatment[-1].assigned if historic_treatment else None,
    )
    await to_update(
        current_value=await vulns_domain.get_vulnerability(loaders, new_id, clear_loader=True),
        historic=tuple(
            [*historic_treatment, new_historic_treatment]
            if historic_treatment
            else [new_historic_treatment]
        ),
    )

    if historic_state:
        await to_update(
            current_value=await vulns_domain.get_vulnerability(loaders, new_id, clear_loader=True),
            historic=tuple(historic_state),
        )
    if historic_verification:
        await to_update(
            current_value=await vulns_domain.get_vulnerability(loaders, new_id, clear_loader=True),
            historic=tuple(historic_verification),
        )
    if historic_zero_risk:
        await to_update(
            current_value=await vulns_domain.get_vulnerability(loaders, new_id, clear_loader=True),
            historic=tuple(historic_zero_risk),
        )


async def _process_vuln(
    *,
    loaders: Dataloaders,
    vuln: Vulnerability,
    target_finding_id: str,
    target_group_name: str,
    target_root_id: str,
    item_subject: str,
    timestamp: datetime,
) -> str:
    LOGGER.info(
        "Processing vuln",
        extra={
            "extra": {
                "vuln_id": vuln.id,
                "target_finding_id": target_finding_id,
                "target_root_id": target_root_id,
            },
        },
    )
    historic_state = await loaders.vulnerability_historic_state.load(
        VulnerabilityRequest(vulnerability_id=vuln.id, finding_id=vuln.finding_id),
    )
    historic_treatment = await loaders.vulnerability_historic_treatment.load(
        VulnerabilityRequest(vulnerability_id=vuln.id, finding_id=vuln.finding_id),
    )
    historic_verification = await loaders.vulnerability_historic_verification.load(
        VulnerabilityRequest(vulnerability_id=vuln.id, finding_id=vuln.finding_id),
    )
    historic_zero_risk = await loaders.vulnerability_historic_zero_risk.load(
        VulnerabilityRequest(vulnerability_id=vuln.id, finding_id=vuln.finding_id),
    )
    new_id = str(uuid.uuid4())
    await vulns_model.add(
        vulnerability=vuln._replace(
            finding_id=target_finding_id,
            group_name=target_group_name,
            id=new_id,
            root_id=target_root_id,
            state=vuln.state,
            verification=VulnerabilityVerification(
                event_id=None,
                modified_by=vuln.verification.modified_by,
                modified_date=vuln.verification.modified_date,
                status=vuln.verification.status,
            )
            if vuln.verification
            else None,
            event_id=None,
        ),
    )
    LOGGER.info(
        "Created new vuln",
        extra={
            "extra": {
                "target_finding_id": target_finding_id,
                "new_id": new_id,
            },
        },
    )
    await _update_historic(
        loaders,
        new_id,
        timestamp,
        item_subject,
        vulns_model.update_historic,
        historic_state,
        historic_treatment,
        historic_verification,
        historic_zero_risk,
    )

    await _update_historic(
        loaders,
        new_id,
        timestamp,
        item_subject,
        vulns_model.update_new_historic,
        historic_state,
        historic_treatment,
        historic_verification,
        historic_zero_risk,
    )

    await vulns_domain.remove_vulnerability(
        loaders=loaders,
        finding_id=vuln.finding_id,
        vulnerability_id=vuln.id,
        justification=VulnerabilityStateReason.MOVED_TO_ANOTHER_GROUP,
        email=item_subject,
        include_closed_vuln=True,
    )

    LOGGER.info(
        "Old vuln closed because of root move",
        extra={
            "extra": {
                "finding_id": vuln.finding_id,
                "vuln_id": vuln.id,
            },
        },
    )
    return new_id


async def _get_target_finding(
    *,
    loaders: Dataloaders,
    source_finding: Finding,
    target_group_name: str,
) -> Finding | None:
    target_group_findings = await loaders.group_findings.load(target_group_name)
    source_created_by = (
        source_finding.creation.modified_by
        if source_finding.creation
        else source_finding.state.modified_by
    )
    if source_created_by == MACHINE_EMAIL:
        return next(
            (
                finding
                for finding in target_group_findings
                if finding.get_criteria_code() == source_finding.get_criteria_code()
                and finding.creation
                and finding.creation.modified_by == MACHINE_EMAIL
            ),
            None,
        )

    return next(
        (
            finding
            for finding in target_group_findings
            if finding.title == source_finding.title
            and finding.description == source_finding.description
            and finding.recommendation == source_finding.recommendation
        ),
        None,
    )


@is_observable(TRACER)
async def _process_finding(
    *,
    loaders: Dataloaders,
    source_group_name: str,
    target_group_name: str,
    target_root_id: str,
    source_finding_id: str,
    vulns: tuple[Vulnerability, ...],
    item_subject: str,
    timestamp: datetime,
) -> None:
    if not vulns:
        return

    LOGGER.info(
        "Processing finding",
        extra={
            "extra": {
                "source_group_name": source_group_name,
                "target_group_name": target_group_name,
                "target_root_id": target_root_id,
                "source_finding_id": source_finding_id,
                "vulns": len(vulns),
            },
        },
    )
    source_finding = await loaders.finding.load(source_finding_id)
    if source_finding is None or findings_utils.is_deleted(source_finding):
        LOGGER.info("Not found", extra={"extra": {"finding_id": source_finding_id}})
        return

    target_finding = await _get_target_finding(
        loaders=loaders,
        source_finding=source_finding,
        target_group_name=target_group_name,
    )
    if target_finding:
        target_finding_id = target_finding.id
        LOGGER.info(
            "Found equivalent finding in target_group_findings",
            extra={
                "extra": {
                    "target_group_name": target_group_name,
                    "target_finding_id": target_finding_id,
                },
            },
        )
    else:
        target_finding_id = str(uuid.uuid4())
        if source_finding.creation:
            initial_state = FindingState(
                modified_by=source_finding.creation.modified_by,
                modified_date=source_finding.creation.modified_date,
                source=source_finding.creation.source,
                status=FindingStateStatus.CREATED,
            )
        await findings_model.add(
            finding=Finding(
                attack_vector_description=(source_finding.attack_vector_description),
                description=source_finding.description,
                group_name=target_group_name,
                id=target_finding_id,
                state=initial_state,
                recommendation=source_finding.recommendation,
                requirements=source_finding.requirements,
                severity_score=source_finding.severity_score,
                title=source_finding.title,
                threat=source_finding.threat,
                unfulfilled_requirements=(source_finding.unfulfilled_requirements),
            ),
        )
        LOGGER.info(
            "Equivalent finding not found. Created new one",
            extra={
                "extra": {
                    "target_group_name": target_group_name,
                    "target_finding_id": target_finding_id,
                },
            },
        )

    await collect(
        tuple(
            _process_vuln(
                loaders=loaders,
                vuln=vuln,
                target_finding_id=target_finding_id,
                target_group_name=target_group_name,
                target_root_id=target_root_id,
                item_subject=item_subject,
                timestamp=timestamp,
            )
            for vuln in vulns
        ),
        workers=100,
    )


@retry_on_exceptions(
    exceptions=(ToeInputAlreadyUpdated,),
)
@is_observable(TRACER)
async def _process_toe_input(
    loaders: Dataloaders,
    target_group_name: str,
    target_root_id: str,
    toe_input: ToeInput,
    vulns: tuple[Vulnerability, ...],
) -> None:
    if toe_input.state.seen_at is None:
        return
    attributes_to_add = ToeInputAttributesToAdd(
        attacked_at=toe_input.state.attacked_at,
        attacked_by=toe_input.state.attacked_by,
        be_present=True,
        first_attack_at=toe_input.state.first_attack_at,
        has_vulnerabilities=len(vulns) > 0,
        seen_first_time_by=toe_input.state.seen_first_time_by,
        seen_at=toe_input.state.seen_at,
    )
    try:
        await toe_inputs_add(
            loaders=loaders,
            entry_point=toe_input.entry_point,
            environment_id=toe_input.environment_id,
            component=toe_input.component,
            group_name=target_group_name,
            root_id=target_root_id,
            attributes=attributes_to_add,
            is_moving_toe_input=True,
        )
    except RepeatedToeInput as exc:
        current_value = await loaders.toe_input.load(
            ToeInputRequest(
                component=toe_input.component,
                entry_point=toe_input.entry_point,
                group_name=target_group_name,
                root_id=target_root_id,
            ),
        )
        attributes_to_update = ToeInputAttributesToUpdate(
            attacked_at=toe_input.state.attacked_at,
            attacked_by=toe_input.state.attacked_by,
            be_present=True,
            first_attack_at=toe_input.state.first_attack_at,
            has_vulnerabilities=len(vulns) > 0,
            seen_at=toe_input.state.seen_at,
            seen_first_time_by=toe_input.state.seen_first_time_by,
        )
        if current_value:
            await toe_inputs_update(
                loaders=loaders,
                current_value=current_value,
                attributes=attributes_to_update,
                modified_by="machine@fluidattacks.com",
                is_moving_toe_input=True,
            )
        else:
            raise ToeInputNotFound() from exc


@is_observable(TRACER)
async def move_environment_url(
    *,
    loaders: Dataloaders,
    environment_url: RootEnvironmentUrl,
    root: Root,
    target_root: Root,
) -> None:
    LOGGER.info("Moving environment url")
    environment_secrets = await loaders.environment_secrets.load(
        RootEnvironmentSecretsRequest(url_id=environment_url.id, group_name=root.group_name),
    )

    await roots_environments.add_root_environment_url(
        loaders=loaders,
        group_name=target_root.group_name,
        url=environment_url.url,
        root_id=target_root.id,
        url_type=environment_url.state.url_type,
        user_email=environment_url.state.modified_by,
        should_notify=False,
        cloud_type=environment_url.state.cloud_name,
        use_connection={
            "use_egress": environment_url.state.use_egress,
            "use_vpn": environment_url.state.use_vpn,
            "use_ztna": environment_url.state.use_ztna,
        },
        is_moving_environment=True,
        is_production=environment_url.state.is_production,
    )
    await roots_domain.remove_environment_url_id(
        loaders=loaders,
        root_id=root.id,
        url_id=environment_url.id,
        user_email=environment_url.state.modified_by,
        group_name=environment_url.group_name,
        should_notify=False,
    )

    await collect(
        tuple(
            roots_model.remove_environment_url_secret(
                group_name=root.group_name,
                resource_id=environment_url.id,
                secret_key=environment_secret.key,
            )
            for environment_secret in environment_secrets
        ),
    )
    await collect(
        tuple(
            roots_model.add_root_environment_secret(
                group_name=target_root.group_name,
                resource_id=environment_url.id,
                secret=environment_secret,
            )
            for environment_secret in environment_secrets
        ),
    )


@is_observable(TRACER)
async def send_notification(
    loaders: Dataloaders,
    stakeholder: Stakeholder,
    environment: RootEnvironmentUrl,
    root: Root,
    target_root: Root,
) -> None:
    email_to = []
    source_group_emails = await mailer_utils.get_group_emails_by_notification(
        loaders=loaders,
        group_name=root.group_name,
        notification="environment_moved",
    )
    email_to.extend(source_group_emails)
    target_group_emails = await mailer_utils.get_group_emails_by_notification(
        loaders=loaders,
        group_name=target_root.group_name,
        notification="environment_moved",
    )
    email_to.extend(target_group_emails)

    if (stakeholder.email not in email_to) and (
        Notification.ROOT_UPDATE in stakeholder.state.notifications_preferences.email
    ):
        email_to.append(stakeholder.email)

    LOGGER.info("Notifying stakeholders", extra={"extra": {"email_to": email_to}})
    is_same_group = root.group_name == target_root.group_name
    subject = (
        f"Environment moved within [{root.group_name}]"
        if is_same_group
        else (f"Environment moved from [{root.group_name}] to [{target_root.group_name}]")
    )
    await send_mails_async(
        loaders=get_new_context(),
        email_to=email_to,
        context={
            "url": environment.url,
            "root": root.state.nickname,
            "group": root.group_name,
            "targetRoot": target_root.state.nickname,
            "targetGroup": target_root.group_name,
        },
        subject=subject,
        template_name="environment_moved",
    )


@is_observable(TRACER)
async def move_toes(
    *,
    loaders: Dataloaders,
    toe_inputs: tuple[ToeInput, ...],
    vulns: tuple[Vulnerability, ...],
    root: Root,
    target_root: Root,
) -> None:
    LOGGER.info("Updating ToE inputs")

    # Same group means remove and create again
    if root.group_name == target_root.group_name:
        await toe_inputs_remove_bulk(toe_inputs=toe_inputs)

    await collect(
        tuple(
            _process_toe_input(
                loaders,
                target_root.group_name,
                target_root.id,
                toe_input,
                vulns=tuple(
                    vuln
                    for vuln in vulns
                    if vuln.state.status == VulnerabilityStateStatus.VULNERABLE
                    and vuln.state.where == toe_input.component
                    and vuln.state.specific == toe_input.entry_point
                ),
            )
            for toe_input in toe_inputs
        ),
    )

    # Different group menas add and hide the old ones
    if root.group_name != target_root.group_name:
        LOGGER.info("Removing old ToE inputs")
        attributes_to_update = ToeInputAttributesToUpdate(be_present=False)
        await collect(
            tuple(
                toe_inputs_update(
                    loaders=loaders,
                    current_value=toe_input,
                    attributes=attributes_to_update,
                    modified_by="machine@fluidattacks.com",
                    is_moving_toe_input=True,
                )
                for toe_input in toe_inputs
            ),
        )


async def validate_rules(
    loaders: Dataloaders,
    info: dict[str, Any],
    modified_by: str,
) -> tuple[
    Root,
    Root,
    RootEnvironmentUrl,
    tuple[ToeInput, ...],
    tuple[Vulnerability, ...],
    Stakeholder,
]:
    target_group_name = info["target_group_name"]
    target_root_id = info["target_root_id"]
    source_group_name = info["source_group_name"]
    source_root_id = info["source_root_id"]
    source_env_id = info["source_env_id"]

    LOGGER.info("Retrieving data to move")
    # Raises exceptions if roots are not found in their groups
    root = await roots_utils.get_root(loaders, source_root_id, source_group_name)
    target_root = await roots_utils.get_root(loaders, target_root_id, target_group_name)

    # Raises an exception if environment_url is not found
    environment_url = await loaders.environment_url.load(
        RootEnvironmentUrlRequest(
            group_name=source_group_name,
            root_id=source_root_id,
            url_id=source_env_id,
        ),
    )

    group_toe_inputs = await loaders.group_toe_inputs.load_nodes(
        GroupToeInputsRequest(group_name=source_group_name),
    )
    env_toe_inputs = tuple(
        toe_input
        for toe_input in group_toe_inputs
        if toe_input.root_id == source_root_id and environment_url.url in toe_input.component
    )
    locations = list(set(input.component for input in env_toe_inputs))

    root_vulns = await loaders.root_vulnerabilities.load(root.id)
    env_vulns = tuple(
        vuln
        for vuln in filter_vulns_utils.filter_non_deleted(root_vulns)
        if vuln.type == VulnerabilityType.INPUTS and vuln.state.where in locations
    )

    stakeholder = await get_stakeholder(loaders, modified_by)

    LOGGER.info("Validating rules")
    if root.organization_name != target_root.organization_name:
        raise ValueError("Roots must belong to the same organization to be moved")

    if len(env_vulns) == 0:
        LOGGER.info("No vulnerabilities to be moved")

    return (
        root,
        target_root,
        environment_url,
        env_toe_inputs,
        env_vulns,
        stakeholder,
    )


@is_observable(TRACER)
async def move_environment(*, item: BatchProcessing) -> None:
    target_group_name = item.additional_info["target_group_name"]
    target_root_id = item.additional_info["target_root_id"]
    source_group_name = item.additional_info["source_group_name"]
    loaders: Dataloaders = get_new_context()

    LOGGER.info("Moving environment", extra={"extra": item.additional_info})
    (
        root,
        target_root,
        environment_url,
        env_toe_inputs,
        env_vulns,
        stakeholder,
    ) = await validate_rules(loaders, item.additional_info, item.subject)

    await move_toes(
        loaders=loaders,
        toe_inputs=env_toe_inputs,
        vulns=env_vulns,
        root=root,
        target_root=target_root,
    )

    await move_environment_url(
        loaders=loaders,
        environment_url=environment_url,
        root=root,
        target_root=target_root,
    )

    env_vulns_by_finding: Iterator[tuple] = itertools.groupby(
        sorted(env_vulns, key=attrgetter("finding_id")),
        key=attrgetter("finding_id"),
    )

    await collect(
        tuple(
            _process_finding(
                loaders=loaders,
                source_group_name=source_group_name,
                target_group_name=target_group_name,
                target_root_id=target_root_id,
                source_finding_id=source_finding_id,
                vulns=tuple(vulns),
                item_subject=item.subject,
                timestamp=item.time,
            )
            for source_finding_id, vulns in env_vulns_by_finding
        ),
        workers=10,
    )

    await send_notification(
        loaders=loaders,
        stakeholder=stakeholder,
        environment=environment_url,
        root=root,
        target_root=target_root,
    )

    LOGGER.info("Environment moved successfully", extra={"extra": item.additional_info})
