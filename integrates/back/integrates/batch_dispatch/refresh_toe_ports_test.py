from datetime import datetime

from integrates.batch.enums import Action, IntegratesBatchQueue
from integrates.batch.types import BatchProcessing
from integrates.batch_dispatch.refresh_toe_ports import refresh_toe_ports
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.roots.enums import RootStatus
from integrates.db_model.toe_ports.types import GroupToePortsRequest
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    DATE_2019,
    GroupFaker,
    IPRootFaker,
    IPRootStateFaker,
    OrganizationFaker,
    ToePortFaker,
    ToePortStateFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize

ACTIVE_ROOT_ID = "root1"
INACTIVE_ROOT_ID = "root2"


@parametrize(
    args=["email", "group_name", "root_ids"],
    cases=[
        ["admin@fluidattacks.com", "test-group", [ACTIVE_ROOT_ID, INACTIVE_ROOT_ID]],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(organization_id="org1"),
            ],
            roots=[
                IPRootFaker(
                    id=ACTIVE_ROOT_ID,
                    state=IPRootStateFaker(
                        address="192.168.1.1",
                        nickname="root1",
                    ),
                ),
                IPRootFaker(
                    id="root2",
                    state=IPRootStateFaker(
                        address=INACTIVE_ROOT_ID,
                        nickname="root2",
                        status=RootStatus.INACTIVE,
                    ),
                ),
            ],
            toe_ports=[
                ToePortFaker(address="192.168.1.1", port="8080", root_id="root1"),
                ToePortFaker(
                    address="192.168.1.1",
                    port="8081",
                    root_id="root1",
                    state=ToePortStateFaker(be_present=False, be_present_until=DATE_2019),
                ),
                ToePortFaker(address="192.168.1.2", port="8080", root_id="root2"),
                ToePortFaker(
                    address="192.168.1.2",
                    port="8081",
                    root_id="root2",
                    state=ToePortStateFaker(be_present=False, be_present_until=DATE_2019),
                ),
            ],
        ),
    ),
)
async def test_refresh_toe_ports(email: str, group_name: str, root_ids: list[str]) -> None:
    await refresh_toe_ports(
        item=BatchProcessing(
            key="000000",
            action_name=Action.REFRESH_TOE_PORTS,
            entity=group_name,
            subject=email,
            time=datetime.fromisoformat("2021-11-10T20:35:20+00:00"),
            additional_info={
                "root_ids": root_ids,
            },
            queue=IntegratesBatchQueue.SMALL,
        )
    )
    loaders: Dataloaders = get_new_context()
    root_toe_ports = await loaders.group_toe_ports.load(
        GroupToePortsRequest(group_name=group_name),
    )

    result_toe_ports: list[dict[str, str | bool]] = [
        {
            "root_id": toe_port.node.root_id,
            "address": toe_port.node.address,
            "port": toe_port.node.port,
            "be_present": toe_port.node.state.be_present,
        }
        for toe_port in root_toe_ports.edges
    ]

    for toe_port in result_toe_ports:
        if toe_port["root_id"] == ACTIVE_ROOT_ID:
            assert toe_port["address"] == "192.168.1.1"
            assert toe_port["port"] in ["8080", "8081"]
            assert toe_port["be_present"] is True
        else:
            assert toe_port["address"] == "192.168.1.2"
            assert toe_port["port"] in ["8080", "8081"]
            assert toe_port["be_present"] is False
