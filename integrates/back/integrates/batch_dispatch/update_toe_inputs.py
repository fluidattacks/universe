import logging
import logging.config
from contextlib import suppress

from aioextensions import (
    collect,
)

from integrates.batch.dal.put import (
    put_action,
)
from integrates.batch.dal.update import (
    update_action_to_dynamodb,
)
from integrates.batch.enums import (
    Action,
    IntegratesBatchQueue,
)
from integrates.batch.types import (
    BatchProcessing,
    BatchProcessingToUpdate,
    DependentAction,
)
from integrates.custom_exceptions import (
    InvalidRootType,
    RepeatedToeInput,
    ToeInputAlreadyUpdated,
    ToeInputNotFound,
    VulnNotFound,
)
from integrates.custom_utils.roots import (
    get_root,
    get_root_vulnerabilities,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.roots.enums import (
    RootEnvironmentUrlType,
)
from integrates.db_model.roots.types import (
    GitRoot,
    RootEnvironmentUrl,
)
from integrates.db_model.toe_inputs.types import (
    RootToeInputsRequest,
    ToeInput,
    ToeInputRequest,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.dynamodb.exceptions import (
    UnavailabilityError,
)
from integrates.jobs_orchestration.create_machine_config import (
    MachineJobOrigin,
    MachineJobTechnique,
    MachineModulesToExecute,
    generate_batch_action_config,
)
from integrates.roots.domain import (
    get_environment_url,
)
from integrates.roots.validations import (
    validate_active_root,
)
from integrates.settings import (
    LOGGING,
)
from integrates.toe.inputs.domain import (
    add,
    update,
    validate_entrypoint,
)
from integrates.toe.inputs.types import (
    ToeInputAttributesToAdd,
    ToeInputAttributesToUpdate,
)
from integrates.vulnerabilities.domain import (
    update_description,
)
from integrates.vulnerabilities.types import (
    VulnerabilityDescriptionToUpdate,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)
toe_inputs_add = retry_on_exceptions(exceptions=(UnavailabilityError,), sleep_seconds=5)(add)
toe_inputs_update = retry_on_exceptions(exceptions=(UnavailabilityError,), sleep_seconds=5)(update)


async def get_root_env_toe_inputs(*, url: str, root: GitRoot) -> list[ToeInput]:
    loaders = get_new_context()
    root_toe_inputs = await loaders.root_toe_inputs.load_nodes(
        RootToeInputsRequest(
            group_name=root.group_name,
            root_id=root.id,
            paginate=False,
        ),
    )

    return [
        toe_input
        for toe_input in root_toe_inputs
        if toe_input.component.startswith(url.rstrip("/"))
    ]


async def update_toe_inputs(*, item: BatchProcessing) -> None:
    group_name = str(item.additional_info["group_name"])
    email = item.subject
    loaders = get_new_context()
    root = await get_root(loaders, str(item.additional_info["root_id"]), group_name)

    if not isinstance(root, GitRoot):
        raise InvalidRootType()
    validate_active_root(root=root)
    environment_url = await get_environment_url(loaders, root.id, group_name, item.entity)
    if environment_url.state.url_type is not RootEnvironmentUrlType.APK:
        return

    message = (
        "Updating toe and vulnerabilities in environment "
        f"{environment_url.id} requested by {email} for group {group_name}"
    )
    LOGGER.info(":".join([item.subject, message]), extra={"extra": {"action": item}})
    old_root_toe_inputs = await get_root_env_toe_inputs(
        root=root,
        url=str(item.additional_info["old_url"]),
    )
    await process_toe_input(
        group_name=group_name,
        root_id=root.id,
        toe_inputs=old_root_toe_inputs,
        email=email,
        environment_url=environment_url,
        old_url=str(item.additional_info["old_url"]),
    )
    await process_vulnerabilities(
        email=email,
        environment_url=environment_url,
        old_root_toe_inputs=old_root_toe_inputs,
        old_url=str(item.additional_info["old_url"]),
    )

    queued_action = await put_action(
        action=Action.REFRESH_TOE_INPUTS,
        attempt_duration_seconds=7200,
        entity=root.group_name,
        subject=email,
        additional_info={"root_ids": [root.id]},
        queue=IntegratesBatchQueue.SMALL,
    )
    if queued_action.batch_job_id and queued_action.dynamo_pk:
        (
            additional_info,
            job_name,
            batch_queue,
        ) = await generate_batch_action_config(
            loaders=get_new_context(),
            group_name=root.group_name,
            root_nicknames=[root.state.nickname],
            modules_to_execute=MachineModulesToExecute(
                APK=True,
                CSPM=False,
                DAST=False,
                SAST=False,
                SCA=False,
            ),
            job_origin_and_technique=(
                MachineJobOrigin.PLATFORM,
                MachineJobTechnique.APK,
            ),
        )
        await update_action_to_dynamodb(
            action=Action.REFRESH_TOE_INPUTS,
            action_key=queued_action.dynamo_pk,
            attributes=BatchProcessingToUpdate(
                dependent_actions=[
                    DependentAction(
                        action_name=Action.EXECUTE_MACHINE,
                        additional_info=additional_info,
                        queue=batch_queue,
                        attempt_duration_seconds=43200,
                        job_name=job_name,
                    ),
                ],
            ),
        )


async def process_vulnerabilities(
    *,
    email: str,
    environment_url: RootEnvironmentUrl,
    old_root_toe_inputs: list[ToeInput],
    old_url: str,
) -> None:
    loaders = get_new_context()
    vulnerabilities = await get_root_vulnerabilities(environment_url.root_id)
    await collect(
        _process_vulnerabilities(
            email=email,
            environment_url=environment_url,
            loaders=loaders,
            vulnerabilities=vulnerabilities,
            toe_input=toe_input,
            old_url=old_url,
        )
        for toe_input in old_root_toe_inputs
    )


async def _update_description(
    *,
    email: str,
    environment_url: RootEnvironmentUrl,
    loaders: Dataloaders,
    vulnerability: Vulnerability,
    old_url: str,
) -> None:
    with suppress(VulnNotFound):
        await update_description(
            loaders=loaders,
            vulnerability_id=vulnerability.id,
            description=VulnerabilityDescriptionToUpdate(
                where=vulnerability.state.where.replace(
                    old_url.rstrip("/"),
                    environment_url.url.rstrip("/"),
                    1,
                ),
            ),
            stakeholder_email=email,
        )


async def _process_vulnerabilities(
    *,
    email: str,
    environment_url: RootEnvironmentUrl,
    loaders: Dataloaders,
    vulnerabilities: list[Vulnerability],
    toe_input: ToeInput,
    old_url: str,
) -> None:
    url_vulnerabilities = [
        vulnerability
        for vulnerability in vulnerabilities
        if vulnerability.state.where.startswith(old_url.rstrip("/"))
    ]
    vulnerabilities_in_toe = await collect(
        validate_entrypoint(vulnerability, toe_input, loaders)
        for vulnerability in url_vulnerabilities
    )
    await collect(
        _update_description(
            loaders=loaders,
            vulnerability=vulnerability,
            email=email,
            environment_url=environment_url,
            old_url=old_url,
        )
        for vulnerability, vulnerability_in_toe in zip(
            url_vulnerabilities, vulnerabilities_in_toe, strict=False
        )
        if vulnerability_in_toe
    )


async def process_toe_input(
    *,
    group_name: str,
    root_id: str,
    toe_inputs: list[ToeInput],
    email: str,
    environment_url: RootEnvironmentUrl,
    old_url: str,
) -> None:
    loaders = get_new_context()
    await collect(
        tuple(
            _process_toe_input(
                loaders=loaders,
                group_name=group_name,
                root_id=root_id,
                toe_input=toe_input,
                old_url=old_url,
                new_url=environment_url.url,
            )
            for toe_input in toe_inputs
        ),
    )
    await collect(
        tuple(
            update(
                loaders=loaders,
                current_value=current_value,
                attributes=ToeInputAttributesToUpdate(
                    be_present=False,
                ),
                modified_by=email,
            )
            for current_value in toe_inputs
            if current_value.component.startswith(old_url.rstrip("/"))
        ),
    )


@retry_on_exceptions(
    exceptions=(ToeInputAlreadyUpdated,),
)
async def _process_toe_input(
    *,
    loaders: Dataloaders,
    group_name: str,
    root_id: str,
    toe_input: ToeInput,
    new_url: str,
    old_url: str,
) -> None:
    if not toe_input.component.startswith(old_url.rstrip("/")):
        return
    full_new_url = toe_input.component.replace(old_url.rstrip("/"), new_url.rstrip("/"), 1)
    if toe_input.state.seen_at is None:
        return
    attributes_to_add = ToeInputAttributesToAdd(
        attacked_at=toe_input.state.attacked_at,
        attacked_by=toe_input.state.attacked_by,
        be_present=True,
        first_attack_at=toe_input.state.first_attack_at,
        has_vulnerabilities=toe_input.state.has_vulnerabilities,
        seen_first_time_by=toe_input.state.seen_first_time_by,
        seen_at=toe_input.state.seen_at,
    )
    try:
        await toe_inputs_add(
            loaders=loaders,
            entry_point=toe_input.entry_point,
            environment_id=toe_input.environment_id,
            component=full_new_url,
            group_name=group_name,
            root_id=root_id,
            attributes=attributes_to_add,
            is_moving_toe_input=True,
        )
    except RepeatedToeInput as exc:
        current_value = await loaders.toe_input.load(
            ToeInputRequest(
                component=full_new_url,
                entry_point=toe_input.entry_point,
                group_name=group_name,
                root_id=root_id,
            ),
        )
        attributes_to_update = ToeInputAttributesToUpdate(
            attacked_at=toe_input.state.attacked_at,
            attacked_by=toe_input.state.attacked_by,
            be_present=True,
            first_attack_at=toe_input.state.first_attack_at,
            has_vulnerabilities=toe_input.state.has_vulnerabilities,
            seen_at=toe_input.state.seen_at,
            seen_first_time_by=toe_input.state.seen_first_time_by,
        )
        if current_value:
            await toe_inputs_update(
                loaders=loaders,
                current_value=current_value,
                attributes=attributes_to_update,
                modified_by="machine@fluidattacks.com",
                is_moving_toe_input=True,
            )
        else:
            raise ToeInputNotFound() from exc
