from integrates.batch.enums import (
    Action,
    IntegratesBatchQueue,
)
from integrates.batch.types import (
    BatchProcessing,
)
from integrates.batch_dispatch.update_organization_repositories import (
    update_organization_repositories,
)
from integrates.custom_utils.datetime import get_utc_now
from integrates.testing.aws import (
    IntegratesAws,
    IntegratesDynamodb,
)
from integrates.testing.fakers import OrganizationFaker
from integrates.testing.mocks import mocks
from integrates.testing.utils import raises


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org")],
        ),
    ),
)
async def test_update_organization_repositories_not_organization() -> None:
    with raises(Exception, match="Access denied"):
        await update_organization_repositories(
            item=BatchProcessing(
                action_name=Action.CLONE_ROOTS,
                entity="org1",
                subject="admin@fluidattacks.com",
                time=get_utc_now(),
                additional_info={"git_root_ids": ["1"]},
                batch_job_id=None,
                queue=IntegratesBatchQueue.SMALL,
                key="2",
            )
        )
