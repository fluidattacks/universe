import logging
import logging.config
import os
import tempfile
from contextlib import suppress
from datetime import datetime
from enum import Enum
from os import path
from subprocess import SubprocessError

import fluidattacks_core.git as git_utils
from aioextensions import collect, in_thread
from git.objects.blob import Blob
from git.repo import Repo
from more_itertools import chunked

from integrates.batch.types import BatchProcessing
from integrates.batch_dispatch.rebase import rebase_root
from integrates.custom_exceptions import (
    FileIsExcluded,
    InvalidToeLinesAttackAt,
    InvalidToeLinesAttackedLines,
    RepeatedToeLines,
    ToeLinesAlreadyUpdated,
)
from integrates.custom_utils import files as files_utils
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.roots.enums import RootStatus, RootToeLinesStatus
from integrates.db_model.roots.types import GitRoot, RootRequest
from integrates.db_model.toe_lines.types import RootToeLinesRequest, ToeLine
from integrates.decorators import retry_on_exceptions
from integrates.dynamodb.exceptions import UnavailabilityError
from integrates.roots.s3_mirror import download_repo
from integrates.roots.utils import update_root_toe_lines_status
from integrates.settings import LOGGING
from integrates.toe.lines import domain as toe_lines_domain
from integrates.toe.lines.types import ToeLinesAttributesToAdd, ToeLinesAttributesToUpdate

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger("refresh_toe_lines")
LOGGER_CONSOLE = logging.getLogger("console")
FILENAMES_CHUNK_SIZE = 1024


class RefreshToeLinesStatus(str, Enum):
    FAILED: str = "FAILED"
    IN_PROGRESS: str = "IN_PROGRESS"
    SUCCESS: str = "SUCCESS"


@retry_on_exceptions(exceptions=(UnavailabilityError,), sleep_seconds=5)
async def toe_lines_add(
    *,
    loaders: Dataloaders,
    group_name: str,
    root_id: str,
    filename: str,
    attributes: ToeLinesAttributesToAdd,
    rules_excluded: bool = False,
) -> None:
    with suppress(RepeatedToeLines, FileIsExcluded):
        return await toe_lines_domain.add(
            loaders=loaders,
            group_name=group_name,
            root_id=root_id,
            filename=filename,
            attributes=attributes,
            rules_excluded=rules_excluded,
        )


@retry_on_exceptions(exceptions=(UnavailabilityError,), sleep_seconds=5)
async def toe_lines_update(
    current_value: ToeLine,
    attributes: ToeLinesAttributesToUpdate,
) -> None:
    with suppress(
        ToeLinesAlreadyUpdated,
        InvalidToeLinesAttackAt,
        InvalidToeLinesAttackedLines,
    ):
        return await toe_lines_domain.update(current_value, attributes)


files_get_lines_count = retry_on_exceptions(
    exceptions=(FileNotFoundError, OSError),
    max_attempts=10,
)(files_utils.get_lines_count)
git_get_last_commit_info = retry_on_exceptions(
    exceptions=(
        FileNotFoundError,
        IndexError,
        OSError,
        SubprocessError,
        ValueError,
    ),
)(git_utils.get_last_commit_info_new)
git_get_modified_filenames = retry_on_exceptions(
    exceptions=(
        FileNotFoundError,
        IndexError,
        OSError,
        SubprocessError,
        ValueError,
    ),
)(git_utils.get_modified_filenames)
git_is_commit_in_branch = retry_on_exceptions(
    exceptions=(
        FileNotFoundError,
        IndexError,
        OSError,
        SubprocessError,
        ValueError,
    ),
)(git_utils.is_commit_in_branch)


async def get_present_filenames(repo: Repo, git_root: GitRoot) -> set[str]:
    LOGGER.info(
        "Getting present filenames",
        extra={
            "extra": {
                "group_name": git_root.group_name,
                "root_id": git_root.id,
                "root_nickname": git_root.state.nickname,
                "status": RefreshToeLinesStatus.IN_PROGRESS,
            },
        },
    )
    try:
        trees = repo.head.commit.tree.traverse()
    except OSError as ex:
        LOGGER_CONSOLE.exception(
            ex,
            extra={
                "extra": {
                    "git_root_id": git_root.id,
                    "git_root_nickname": git_root.state.nickname,
                    "group_name": git_root.group_name,
                    "head_commit": repo.head.commit.hexsha,
                }
            },
        )
        raise ex

    included_head_filenames = tuple(str(tree.path) for tree in trees if isinstance(tree, Blob))
    file_exists = await collect(
        in_thread(os.path.exists, path.join(str(repo.working_dir), filename))
        for filename in included_head_filenames
    )

    file_islink = await collect(
        in_thread(os.path.islink, path.join(str(repo.working_dir), filename))
        for filename in included_head_filenames
    )

    return {
        filename
        for filename, exists, islink in zip(
            included_head_filenames,
            file_exists,
            file_islink,
            strict=False,
        )
        if exists and not islink
    }


async def add_present_toe_lines_chunk(
    *,
    loaders: Dataloaders,
    git_root: GitRoot,
    present_filenames: list[str],
    repo_toe_lines: dict[str, ToeLine],
    repo_working_dir: str,
) -> int:
    non_db_filenames = tuple(
        filename for filename in present_filenames if not repo_toe_lines.get(filename)
    )
    last_locs = await collect(
        tuple(
            files_get_lines_count(path.join(repo_working_dir, filename))
            for filename in non_db_filenames
        ),
        workers=256,
    )
    last_commit_infos = await collect(
        tuple(
            git_get_last_commit_info(repo_working_dir, filename) for filename in non_db_filenames
        ),
        workers=256,
    )
    present_toe_lines_to_add = tuple(
        (
            filename,
            ToeLinesAttributesToAdd(
                attacked_at=None,
                attacked_by="",
                attacked_lines=0,
                last_author=last_commit_info.author,
                comments="",
                loc=last_loc,
                last_commit=last_commit_info.hash,
                last_commit_date=last_commit_info.modified_date,
            ),
        )
        for (
            filename,
            last_commit_info,
            last_loc,
        ) in zip(
            non_db_filenames,
            last_commit_infos,
            last_locs,
            strict=False,
        )
        if last_commit_info
    )
    await collect(
        tuple(
            toe_lines_add(
                loaders=loaders,
                group_name=git_root.group_name,
                root_id=git_root.id,
                filename=filename,
                attributes=toe_lines_to_add,
                rules_excluded=True,
            )
            for filename, toe_lines_to_add in present_toe_lines_to_add
        ),
        workers=128,
    )
    LOGGER_CONSOLE.info(
        "Add present toe lines chunk",
        extra={
            "extra": {
                "group_name": git_root.group_name,
                "len_toe_lines": len(present_toe_lines_to_add),
                "root_id": git_root.id,
                "root_nickname": git_root.state.nickname,
                "status": RefreshToeLinesStatus.IN_PROGRESS,
            },
        },
    )

    return len(present_toe_lines_to_add)


async def process_present_toe_lines_to_add(
    *,
    loaders: Dataloaders,
    git_root: GitRoot,
    present_filenames: set[str],
    repo_toe_lines: dict[str, ToeLine],
    repo_working_dir: str,
) -> None:
    len_toe_lines = await collect(
        tuple(
            add_present_toe_lines_chunk(
                loaders=loaders,
                git_root=git_root,
                present_filenames=chunk,
                repo_toe_lines=repo_toe_lines,
                repo_working_dir=repo_working_dir,
            )
            for chunk in chunked(present_filenames, FILENAMES_CHUNK_SIZE)
        ),
        workers=4,
    )
    LOGGER.info(
        "Processed present ToE lines to add",
        extra={
            "extra": {
                "group_name": git_root.group_name,
                "len_toe_lines": sum(len_toe_lines),
                "root_id": git_root.id,
                "root_nickname": git_root.state.nickname,
                "status": RefreshToeLinesStatus.IN_PROGRESS,
            },
        },
    )


def _are_different_locs(toe_lines: ToeLine, last_loc: int) -> bool:
    return toe_lines.state.loc != last_loc


async def update_present_toe_lines_chunk(
    *,
    db_filenames: list[str],
    git_root: GitRoot,
    repo_toe_lines: dict[str, ToeLine],
    repo_working_dir: str,
) -> int:
    last_locs = await collect(
        tuple(
            files_get_lines_count(path.join(repo_working_dir, filename))
            for filename in db_filenames
        ),
        workers=256,
    )
    last_commit_infos = await collect(
        tuple(git_get_last_commit_info(repo_working_dir, filename) for filename in db_filenames),
        workers=256,
    )
    present_toe_lines_to_update = tuple(
        (
            repo_toe_lines[filename],
            ToeLinesAttributesToUpdate(
                be_present=True,
                last_author=last_commit_info.author,
                attacked_lines=(
                    0 if _are_different_locs(repo_toe_lines[filename], last_loc) else None
                ),
                loc=last_loc,
                last_commit=last_commit_info.hash,
                last_commit_date=last_commit_info.modified_date,
            ),
        )
        for (
            filename,
            last_commit_info,
            last_loc,
        ) in zip(
            db_filenames,
            last_commit_infos,
            last_locs,
            strict=False,
        )
        if last_commit_info
        and (
            True,
            last_commit_info.author,
            last_loc,
            last_commit_info.hash,
            last_commit_info.modified_date,
        )
        != (
            repo_toe_lines[filename].state.be_present,
            repo_toe_lines[filename].state.last_author,
            repo_toe_lines[filename].state.loc,
            repo_toe_lines[filename].state.last_commit,
            repo_toe_lines[filename].state.last_commit_date,
        )
    )
    await collect(
        tuple(
            toe_lines_update(current_value, attrs_to_update)
            for current_value, attrs_to_update in present_toe_lines_to_update
        ),
        workers=128,
    )
    LOGGER_CONSOLE.info(
        "Updated present toe lines chunk",
        extra={
            "extra": {
                "group_name": git_root.group_name,
                "len_toe_lines": len(present_toe_lines_to_update),
                "root_id": git_root.id,
                "root_nickname": git_root.state.nickname,
                "status": RefreshToeLinesStatus.IN_PROGRESS,
            },
        },
    )

    return len(present_toe_lines_to_update)


async def process_present_toe_lines_to_update(
    *,
    git_root: GitRoot,
    present_filenames: set[str],
    repo_toe_lines: dict[str, ToeLine],
    repo_working_dir: str,
) -> None:
    db_filenames = tuple(filename for filename in present_filenames if repo_toe_lines.get(filename))
    if (
        git_root.toe_lines
        and git_root.toe_lines.commit
        and await git_is_commit_in_branch(
            repo_working_dir,
            git_root.state.branch,
            git_root.toe_lines.commit,
        )
        and (
            modified_filenames := await git_get_modified_filenames(
                repo_working_dir,
                git_root.toe_lines.commit,
            )
        )
    ):
        db_filenames = tuple(set(db_filenames) & set(modified_filenames))

    len_toe_lines = await collect(
        tuple(
            update_present_toe_lines_chunk(
                db_filenames=chunk,
                git_root=git_root,
                repo_toe_lines=repo_toe_lines,
                repo_working_dir=repo_working_dir,
            )
            for chunk in chunked(db_filenames, FILENAMES_CHUNK_SIZE)
        ),
        workers=4,
    )
    LOGGER.info(
        "Processed present ToE lines to update",
        extra={
            "extra": {
                "group_name": git_root.group_name,
                "len_toe_lines": sum(len_toe_lines),
                "root_id": git_root.id,
                "root_nickname": git_root.state.nickname,
                "status": RefreshToeLinesStatus.IN_PROGRESS,
            },
        },
    )


async def process_non_present_toe_lines_to_update(
    git_root: GitRoot,
    present_filenames: set[str],
    repo_toe_lines: dict[str, ToeLine],
) -> None:
    non_present_toe_lines_to_update = tuple(
        (
            repo_toe_lines[db_filename],
            ToeLinesAttributesToUpdate(
                be_present=False,
            ),
        )
        for db_filename in repo_toe_lines
        if db_filename not in present_filenames and repo_toe_lines[db_filename].state.be_present
    )
    await collect(
        tuple(
            toe_lines_update(current_value, attrs_to_update)
            for current_value, attrs_to_update in (non_present_toe_lines_to_update)
        ),
        workers=128,
    )
    LOGGER.info(
        "Processed non-present ToE lines to update",
        extra={
            "extra": {
                "group_name": git_root.group_name,
                "len_toe_lines": len(non_present_toe_lines_to_update),
                "root_id": git_root.id,
                "root_nickname": git_root.state.nickname,
                "status": RefreshToeLinesStatus.IN_PROGRESS,
            },
        },
    )


@retry_on_exceptions(
    exceptions=(UnavailabilityError,),
    max_attempts=3,
    sleep_seconds=5,
)
async def process_active_root_toe_lines(
    git_root: GitRoot,
    modified_by: str,
    repo: Repo,
) -> tuple[bool, str]:
    loaders: Dataloaders = get_new_context()
    await git_utils.disable_quotepath(path.join(str(repo.working_dir), ".git"))
    present_filenames = await get_present_filenames(repo, git_root)
    repo_toe_lines = {
        toe_lines.filename: toe_lines
        for toe_lines in await loaders.root_toe_lines.load_nodes(
            RootToeLinesRequest(group_name=git_root.group_name, root_id=git_root.id),
        )
    }
    await process_present_toe_lines_to_add(
        loaders=loaders,
        git_root=git_root,
        present_filenames=present_filenames,
        repo_toe_lines=repo_toe_lines,
        repo_working_dir=str(repo.working_dir),
    )
    await process_non_present_toe_lines_to_update(
        git_root,
        present_filenames,
        repo_toe_lines,
    )
    await process_present_toe_lines_to_update(
        git_root=git_root,
        present_filenames=present_filenames,
        repo_toe_lines=repo_toe_lines,
        repo_working_dir=str(repo.working_dir),
    )

    message = "Finished refreshing toe lines on ACTIVE root"
    await update_root_toe_lines_status(
        group_name=git_root.group_name,
        root_id=git_root.id,
        status=RootToeLinesStatus.SUCCESS,
        message=message,
        modified_by=modified_by,
        commit=repo.head.commit.hexsha,
        commit_date=datetime.fromtimestamp(repo.head.commit.authored_date),
    )

    return True, message


@retry_on_exceptions(
    exceptions=(UnavailabilityError,),
    max_attempts=3,
    sleep_seconds=5,
)
async def process_inactive_root_toe_lines(git_root: GitRoot, modified_by: str) -> tuple[bool, str]:
    loaders: Dataloaders = get_new_context()
    repo_toe_lines = {
        toe_lines.filename: toe_lines
        for toe_lines in await loaders.root_toe_lines.load_nodes(
            RootToeLinesRequest(group_name=git_root.group_name, root_id=git_root.id),
        )
    }
    await process_non_present_toe_lines_to_update(git_root, set(), repo_toe_lines)

    message = "Finished refreshing toe lines on INACTIVE root"
    await update_root_toe_lines_status(
        group_name=git_root.group_name,
        root_id=git_root.id,
        status=RootToeLinesStatus.SUCCESS,
        message=message,
        modified_by=modified_by,
    )

    return True, message


async def download_repo_and_process_it(
    git_root: GitRoot,
    modified_by: str,
) -> tuple[bool, str]:
    await update_root_toe_lines_status(
        group_name=git_root.group_name,
        root_id=git_root.id,
        status=RootToeLinesStatus.IN_PROGRESS,
        message="Refreshing toe lines",
        modified_by=modified_by,
    )
    if git_root.state.status == RootStatus.INACTIVE:
        return await process_inactive_root_toe_lines(git_root, modified_by)

    with tempfile.TemporaryDirectory(
        prefix=f"integrates_refresh_toe_lines_{git_root.group_name}_",
        ignore_cleanup_errors=True,
    ) as tmpdir:
        if not (
            repo := await download_repo(
                git_root.group_name,
                git_root.state.nickname,
                tmpdir,
                git_root.state.gitignore,
            )
        ):
            message = "Repository could not be downloaded"
            await update_root_toe_lines_status(
                group_name=git_root.group_name,
                root_id=git_root.id,
                status=RootToeLinesStatus.FAILED,
                message=message,
                modified_by=modified_by,
            )

            return False, message

        result = await process_active_root_toe_lines(git_root, modified_by, repo)

        await rebase_root(git_root, modified_by, repo)

        return result


async def refresh_toe_lines(*, item: BatchProcessing) -> None:
    loaders: Dataloaders = get_new_context()
    group_name = item.entity
    user_email = item.subject
    root_ids = set(item.additional_info["root_ids"])
    valid_roots = [
        root
        for root in await loaders.group_roots.load(group_name)
        if root.id in root_ids and isinstance(root, GitRoot)
    ]
    LOGGER.info(
        "Roots to be processed",
        extra={
            "extra": {
                "group_name": group_name,
                "roots": [(root.id, root.state.nickname) for root in valid_roots],
            },
        },
    )

    for root in valid_roots:
        await download_repo_and_process_it(root, user_email)


async def refresh_toe_lines_simple_args(
    *,
    group_name: str,
    git_root_id: str,
    user_email: str,
) -> tuple[bool, str]:
    loaders: Dataloaders = get_new_context()
    git_root = await loaders.root.load(RootRequest(group_name, git_root_id))
    if not git_root or not isinstance(git_root, GitRoot):
        message = "Invalid parameters"
        await update_root_toe_lines_status(
            group_name=group_name,
            root_id=git_root_id,
            status=RootToeLinesStatus.FAILED,
            message=message,
            modified_by=user_email,
        )

        return False, message

    return await download_repo_and_process_it(git_root, user_email)
