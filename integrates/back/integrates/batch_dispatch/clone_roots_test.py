import os
from datetime import datetime

import integrates.jobs_orchestration.jobs
from integrates.api.mutations.update_git_root import mutate
from integrates.batch import (
    dal as batch_dal,
)
from integrates.batch.enums import Action, IntegratesBatchQueue
from integrates.batch.types import BatchProcessing
from integrates.batch_dispatch.clone_roots import clone_git_root_simple_args, clone_roots
from integrates.custom_utils.constants import SCHEDULE_CLONE_GROUPS_ROOTS_EMAIL
from integrates.custom_utils.datetime import get_utc_now
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.credentials.types import (
    CredentialsState,
    HttpsSecret,
    SshSecret,
)
from integrates.db_model.enums import CredentialType
from integrates.db_model.events.types import GroupEventsRequest
from integrates.db_model.roots.enums import RootCloningStatus
from integrates.db_model.roots.types import GitRoot, RootRequest
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute,
    require_attribute_internal,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    CredentialsFaker,
    GitRootCloningFaker,
    GitRootFaker,
    GitRootStateFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import parametrize

EMAIL_TEST = "admin@gmail.com"
SCHEDULE_EMAIL_TEST = SCHEDULE_CLONE_GROUPS_ROOTS_EMAIL
GROUP_NAME = "group1"
ROOT_ID = "root1"
TEST_MODIFIED_DATE = "2024-01-19T13:37:10+00:00"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_TEST, role="admin"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            credentials=[
                CredentialsFaker(
                    credential_id="cred1",
                    organization_id="ORG#org1",
                    secret=HttpsSecret(user="user@test.test", password=""),
                )
            ],
            roots=[
                GitRootFaker(
                    cloning=GitRootCloningFaker(
                        modified_date=datetime.fromisoformat(TEST_MODIFIED_DATE),
                        status=RootCloningStatus.UNKNOWN,
                    ),
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(
                        branch="main",
                        url="https://github.com/fluidattacks/makes.git",
                        nickname="",
                        credential_id="cred1",
                    ),
                ),
            ],
        ),
    ),
)
async def test_clone_roots() -> None:
    loaders: Dataloaders = get_new_context()
    action = BatchProcessing(
        action_name=Action.CLONE_ROOTS,
        entity=GROUP_NAME,
        subject=EMAIL_TEST,
        time=get_utc_now(),
        additional_info={
            "git_root_ids": [ROOT_ID],
        },
        batch_job_id=None,
        queue=IntegratesBatchQueue.SMALL,
        key="2",
    )

    await clone_roots(item=action)
    root_1 = await loaders.root.load(RootRequest(GROUP_NAME, ROOT_ID))
    assert isinstance(root_1, GitRoot)
    assert root_1.cloning.status == RootCloningStatus.OK
    assert root_1.cloning.commit == "764adcb18bf08ceefdbfb6ccfb83adfc433e52af"

    assert (
        len(
            await batch_dal.get_actions_by_name(action_name=Action.EXECUTE_MACHINE, entity="group1")
        )
        == 0
    )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_TEST, role="admin"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            credentials=[
                CredentialsFaker(
                    credential_id="cred1",
                    organization_id="ORG#org1",
                    secret=HttpsSecret(user="user@test.test", password=""),
                )
            ],
            roots=[
                GitRootFaker(
                    cloning=GitRootCloningFaker(
                        status=RootCloningStatus.OK,
                        commit="6d2059f5d5b3954feb65fcbc5a368e8ef9964b62",
                    ),
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(
                        branch="master",
                        url="https://gitlab.com/fluidattacks/nickname2",
                        nickname="",
                        credential_id="cred1",
                    ),
                ),
            ],
        ),
    ),
)
async def test_clone_roots_failed() -> None:
    loaders: Dataloaders = get_new_context()
    action = BatchProcessing(
        action_name=Action.CLONE_ROOTS,
        entity=GROUP_NAME,
        subject=EMAIL_TEST,
        time=get_utc_now(),
        additional_info={
            "git_root_ids": [ROOT_ID],
        },
        batch_job_id=None,
        queue=IntegratesBatchQueue.SMALL,
        key="2",
    )

    await clone_roots(item=action)
    loaders.root.clear_all()
    root_1 = await loaders.root.load(RootRequest(GROUP_NAME, ROOT_ID))
    assert isinstance(root_1, GitRoot)
    assert root_1.cloning.status == RootCloningStatus.FAILED
    assert root_1.cloning.commit == "6d2059f5d5b3954feb65fcbc5a368e8ef9964b62"
    assert root_1.cloning.failed_count == 1


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_TEST, role="admin"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            credentials=[
                CredentialsFaker(
                    credential_id="cred1",
                    organization_id="ORG#org1",
                    state=CredentialsState(
                        modified_by=EMAIL_TEST,
                        modified_date=datetime.fromisoformat("2022-02-10T14:58:10+00:00"),
                        name="SSH Key",
                        type=CredentialType.SSH,
                        is_pat=False,
                        owner=EMAIL_TEST,
                    ),
                    secret=SshSecret(key=os.environ["TEST_GITHUB_SSH_PRIVATE_KEY"]),
                )
            ],
            roots=[
                GitRootFaker(
                    cloning=GitRootCloningFaker(
                        modified_by=EMAIL_TEST,
                        modified_date=datetime.fromisoformat(TEST_MODIFIED_DATE),
                        status=RootCloningStatus.UNKNOWN,
                    ),
                    group_name=GROUP_NAME,
                    created_date=datetime.fromisoformat(TEST_MODIFIED_DATE),
                    id=ROOT_ID,
                    created_by=EMAIL_TEST,
                    state=GitRootStateFaker(
                        modified_date=datetime.fromisoformat(TEST_MODIFIED_DATE),
                        branch="main",
                        credential_id="cred1",
                        modified_by=EMAIL_TEST,
                        url="ssh://git@github.com:fluidattacks/test_git_roots.git",
                    ),
                ),
            ],
        ),
    ),
)
async def test_clone_roots_real_ssh() -> None:
    loaders: Dataloaders = get_new_context()
    action = BatchProcessing(
        action_name=Action.CLONE_ROOTS,
        entity=GROUP_NAME,
        subject=EMAIL_TEST,
        time=get_utc_now(),
        additional_info={
            "git_root_ids": [ROOT_ID],
        },
        batch_job_id=None,
        queue=IntegratesBatchQueue.SMALL,
        key="2",
    )

    await clone_roots(item=action)
    loaders.root.clear_all()
    root_1 = await loaders.root.load(RootRequest(GROUP_NAME, ROOT_ID))
    assert isinstance(root_1, GitRoot)
    assert root_1.cloning.status == RootCloningStatus.OK
    assert root_1.cloning.commit == "63afdb8d9cc5230a0137593d20a2fd2c4c73b92b"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_TEST, role="admin"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            credentials=[
                CredentialsFaker(
                    credential_id="cred1",
                    organization_id="ORG#org1",
                    state=CredentialsState(
                        modified_by=EMAIL_TEST,
                        modified_date=datetime.fromisoformat("2022-02-10T14:58:10+00:00"),
                        name="SSH Key",
                        type=CredentialType.SSH,
                        is_pat=False,
                        owner=EMAIL_TEST,
                    ),
                    secret=SshSecret(key=os.environ["TEST_GITHUB_SSH_PRIVATE_KEY"]),
                )
            ],
            roots=[
                GitRootFaker(
                    cloning=GitRootCloningFaker(
                        modified_by=EMAIL_TEST,
                        modified_date=datetime.fromisoformat(TEST_MODIFIED_DATE),
                        status=RootCloningStatus.UNKNOWN,
                    ),
                    group_name=GROUP_NAME,
                    created_date=datetime.fromisoformat(TEST_MODIFIED_DATE),
                    id=ROOT_ID,
                    created_by=EMAIL_TEST,
                    state=GitRootStateFaker(
                        modified_date=datetime.fromisoformat(TEST_MODIFIED_DATE),
                        branch="main",
                        credential_id="cred1",
                        modified_by=EMAIL_TEST,
                        url="ssh://git@github.com:fluidattacks/test_git_roots.git",
                    ),
                ),
            ],
        ),
    ),
)
async def test_clone_git_root_simple_args() -> None:
    success, _ = await clone_git_root_simple_args(
        group_name=GROUP_NAME,
        git_root_id=ROOT_ID,
        user_email=EMAIL_TEST,
        should_queue_machine=False,
    )
    assert success


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_TEST, role="admin"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            credentials=[
                CredentialsFaker(
                    credential_id="cred1",
                    organization_id="ORG#org1",
                    state=CredentialsState(
                        modified_by="admin@gmail.com",
                        modified_date=datetime.fromisoformat("2022-02-10T14:58:10+00:00"),
                        name="SSH Key",
                        type=CredentialType.SSH,
                        is_pat=False,
                        owner="admin@gmail.com",
                    ),
                    secret=SshSecret(key=os.environ["TEST_GITHUB_SSH_PRIVATE_KEY"]),
                )
            ],
            roots=[
                GitRootFaker(
                    cloning=GitRootCloningFaker(
                        modified_by=EMAIL_TEST,
                        modified_date=datetime.fromisoformat(TEST_MODIFIED_DATE),
                        status=RootCloningStatus.UNKNOWN,
                    ),
                    group_name=GROUP_NAME,
                    created_date=datetime.fromisoformat(TEST_MODIFIED_DATE),
                    id=ROOT_ID,
                    created_by=EMAIL_TEST,
                    state=GitRootStateFaker(
                        modified_date=datetime.fromisoformat(TEST_MODIFIED_DATE),
                        branch="main",
                        credential_id="cred1",
                        modified_by=EMAIL_TEST,
                        url="ssh://git@github.com:fluidattacks/test_git_roots.git",
                    ),
                ),
            ],
        ),
    ),
    others=[Mock(integrates.jobs_orchestration.jobs, "queue_machine_job", "async", None)],
)
async def test_clone_git_root_no_machine() -> None:
    success, _ = await clone_git_root_simple_args(
        group_name=GROUP_NAME,
        git_root_id=ROOT_ID,
        user_email=EMAIL_TEST,
    )
    assert success


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_TEST, role="admin"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    cloning=GitRootCloningFaker(
                        modified_by=EMAIL_TEST,
                        modified_date=datetime.fromisoformat(TEST_MODIFIED_DATE),
                        status=RootCloningStatus.FAILED,
                        failed_count=2,
                    ),
                    group_name=GROUP_NAME,
                    created_date=datetime.fromisoformat(TEST_MODIFIED_DATE),
                    id=ROOT_ID,
                    created_by=EMAIL_TEST,
                    state=GitRootStateFaker(
                        modified_date=datetime.fromisoformat(TEST_MODIFIED_DATE),
                        nickname="",
                        modified_by=EMAIL_TEST,
                        gitignore=["bower_components/*", "node_modules/*"],
                        includes_health_check=True,
                        url="https://gitlab.com/fluidattacks/nickname9",
                    ),
                ),
            ],
        ),
    ),
)
async def test_clone_git_root_simple_args_fails() -> None:
    success, _ = await clone_git_root_simple_args(
        group_name=GROUP_NAME, git_root_id=ROOT_ID, user_email=EMAIL_TEST
    )
    assert not success


@parametrize(
    args=["events_init", "events_end", "branch"],
    cases=[[0, 1, "nonexistent"]],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_TEST, role="admin"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            group_access=[
                GroupAccessFaker(
                    email=SCHEDULE_EMAIL_TEST,
                    state=GroupAccessStateFaker(has_access=True, role="customer_manager"),
                ),
            ],
            roots=[
                GitRootFaker(
                    cloning=GitRootCloningFaker(
                        modified_by=EMAIL_TEST,
                        modified_date=datetime.fromisoformat(TEST_MODIFIED_DATE),
                        status=RootCloningStatus.UNKNOWN,
                        failed_count=2,
                    ),
                    group_name=GROUP_NAME,
                    created_date=datetime.fromisoformat(TEST_MODIFIED_DATE),
                    id=ROOT_ID,
                    created_by=EMAIL_TEST,
                    state=GitRootStateFaker(
                        modified_date=datetime.fromisoformat(TEST_MODIFIED_DATE),
                        nickname="",
                        modified_by=EMAIL_TEST,
                        gitignore=["bower_components/*", "node_modules/*"],
                        includes_health_check=True,
                        url="https://gitlab.com/fluidattacks/universe.git",
                    ),
                ),
            ],
        ),
    ),
)
async def test_create_events_on_failed_cloning(
    events_init: int, events_end: int, branch: str
) -> None:
    loaders: Dataloaders = get_new_context()

    unsolved_events = await loaders.group_events.load(
        GroupEventsRequest(group_name=GROUP_NAME, is_solved=False),
    )
    assert len(unsolved_events) == events_init
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_service_white", GROUP_NAME],
            [require_attribute, "is_continuous", GROUP_NAME],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )
    await mutate(
        _parent=None,
        info=info,
        id=ROOT_ID,
        group_name=GROUP_NAME,
        branch=branch,
        includes_health_check=False,
        url="https://gitlab.com/fluidattacks/universe.git",
        gitignore=["node_modules/"],
    )
    action = BatchProcessing(
        action_name=Action.CLONE_ROOTS,
        entity=GROUP_NAME,
        subject=SCHEDULE_EMAIL_TEST,
        time=get_utc_now(),
        additional_info={
            "git_root_ids": [ROOT_ID],
        },
        batch_job_id=None,
        queue=IntegratesBatchQueue.SMALL,
        key="2",
    )
    await clone_roots(item=action)
    loaders.group_events.clear_all()
    unsolved_events2 = await loaders.group_events.load(
        GroupEventsRequest(group_name=GROUP_NAME, is_solved=False),
    )
    assert len(unsolved_events2) == events_init
    await mutate(
        _parent=None,
        info=info,
        id=ROOT_ID,
        group_name=GROUP_NAME,
        branch=branch,
        includes_health_check=False,
        url="https://gitlab.com/fluidattacks/universe.git",
        gitignore=["node_modules/"],
    )
    action = BatchProcessing(
        action_name=Action.CLONE_ROOTS,
        entity=GROUP_NAME,
        subject=SCHEDULE_EMAIL_TEST,
        time=get_utc_now(),
        additional_info={
            "git_root_ids": [ROOT_ID],
        },
        batch_job_id=None,
        queue=IntegratesBatchQueue.SMALL,
        key="2",
    )
    await clone_roots(item=action)
    loaders.group_events.clear_all()
    unsolved_events3 = await loaders.group_events.load(
        GroupEventsRequest(group_name=GROUP_NAME, is_solved=False),
    )
    assert len(unsolved_events3) == events_end
