import json
import logging
import logging.config
import os
import tempfile
from datetime import (
    datetime,
)
from typing import (
    Any,
)

from aioextensions import (
    in_thread,
)
from botocore.exceptions import (
    ClientError,
)
from jwcrypto.jws import (
    InvalidJWSSignature,
)
from jwcrypto.jwt import (
    JWTExpired,
)
from starlette.datastructures import (
    UploadFile,
)

from integrates.custom_exceptions import (
    ErrorDownloadingFile,
    ExpiredForcesToken,
    InvalidAuthorization,
)
from integrates.custom_utils import (
    datetime as date_utils,
)
from integrates.custom_utils.forces import (
    format_forces_vulnerabilities_to_add,
)
from integrates.db_model import (
    forces as forces_model,
)
from integrates.db_model.forces.types import (
    ForcesExecution,
)
from integrates.db_model.groups.types import (
    GroupMetadataToUpdate,
)
from integrates.groups import (
    domain as groups_domain,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.s3 import (
    operations as s3_ops,
)
from integrates.sessions import (
    domain as sessions_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)


async def save_log_execution(file_object: object, file_name: str) -> None:
    await s3_ops.upload_memory_file(
        file_object,
        f"forces/{file_name}",
    )


async def add_forces_execution(
    *,
    group_name: str,
    json_log: str,
    log: UploadFile | None = None,
    **execution_attributes: Any,
) -> None:
    orgs_domain.validate_min_breaking_severity(execution_attributes["severity_threshold"])
    orgs_domain.validate_vulnerability_grace_period(execution_attributes["grace_period"])
    orgs_domain.validate_days_until_it_breaks(execution_attributes.get("days_until_it_breaks"))
    forces_execution = ForcesExecution(
        id=execution_attributes["execution_id"],
        group_name=group_name,
        execution_date=execution_attributes["date"],
        commit=execution_attributes["git_commit"],
        repo=execution_attributes["git_repo"],
        branch=execution_attributes["git_branch"],
        kind=execution_attributes["kind"],
        exit_code=execution_attributes["exit_code"],
        strictness=execution_attributes["strictness"],
        origin=execution_attributes["git_origin"],
        grace_period=int(execution_attributes["grace_period"]),
        severity_threshold=execution_attributes["severity_threshold"],
        vulnerabilities=format_forces_vulnerabilities_to_add(
            execution_attributes["vulnerabilities"],
            managed_vulnerabilities=execution_attributes.get("managed_vulnerabilities"),
            un_managed_vulnerabilities=execution_attributes.get("un_managed_vulnerabilities"),
        ),
        days_until_it_breaks=int(execution_attributes["days_until_it_breaks"])
        if execution_attributes.get("days_until_it_breaks") is not None
        else None,
    )
    vulnerabilities = execution_attributes.pop("vulnerabilities")
    log_name = f'{group_name}/{execution_attributes["execution_id"]}.log'
    json_log_name = f'{group_name}/{execution_attributes["execution_id"]}_log.json'
    vulns_name = f'{group_name}/{execution_attributes["execution_id"]}.json'

    # Create a file for vulnerabilities
    with tempfile.NamedTemporaryFile() as vulns_file:
        await in_thread(vulns_file.write, json.dumps(vulnerabilities).encode("utf-8"))
        await in_thread(vulns_file.seek, os.SEEK_SET)
        await save_log_execution(vulns_file, vulns_name)
    # And other for the json log
    with tempfile.NamedTemporaryFile() as json_log_file:
        await in_thread(json_log_file.write, json_log.encode("utf-8"))
        await in_thread(json_log_file.seek, os.SEEK_SET)
        await save_log_execution(json_log_file, json_log_name)

    await save_log_execution(log, log_name)
    await forces_model.add(forces_execution=forces_execution)


async def get_log_execution(group_name: str, execution_id: str) -> str:
    with tempfile.NamedTemporaryFile(mode="w+") as file:
        await s3_ops.download_file(
            f"forces/{group_name}/{execution_id}.log",
            file.name,
        )
        with open(file.name, encoding="utf-8") as reader:
            return await in_thread(reader.read)


async def get_json_log_execution(group_name: str, execution_id: str) -> dict[str, Any] | None:
    with tempfile.NamedTemporaryFile(mode="w+") as file:
        try:
            await s3_ops.download_file(
                f"forces/{group_name}/{execution_id}_log.json",
                file.name,
            )
            with open(file.name, encoding="utf-8") as reader:
                return await in_thread(json.load, reader)
        # Not all executions have a JSON log
        except (ClientError, ErrorDownloadingFile):
            return None


async def get_vulns_execution(group_name: str, execution_id: str) -> dict[str, Any]:
    with tempfile.NamedTemporaryFile(mode="w+") as file:
        await s3_ops.download_file(
            f"forces/{group_name}/{execution_id}.json",
            file.name,
        )
        with open(file.name, encoding="utf-8") as reader:
            return await in_thread(json.load, reader)


async def update_token(
    group_name: str,
    organization_id: str,
    token: str,
) -> None:
    return await groups_domain.update_metadata(
        group_name=group_name,
        metadata=GroupMetadataToUpdate(
            agent_token=token,
        ),
        organization_id=organization_id,
    )


def get_expiration_date(token: str) -> datetime:
    try:
        decoded_token = sessions_domain.decode_token(token)
    except InvalidAuthorization as exc:
        LOGGER.error(
            "Failed to get expiration date",
            extra={"extra": {"exc": exc, "cause": exc.__cause__}},
        )
        if isinstance(exc.__cause__, (JWTExpired, InvalidJWSSignature)):
            raise ExpiredForcesToken() from exc

        raise exc
    exp = decoded_token["exp"]
    exp_as_datetime = date_utils.get_from_epoch(exp)
    return exp_as_datetime
