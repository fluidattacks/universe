from typing import (
    Literal,
)

SESSION_COOKIE_AGE = 8 * 60 * 60
OTP_COOKIE_AGE = 180 * 24 * 60 * 60
SESSION_COOKIE_SAME_SITE: Literal["lax"] = "lax"
