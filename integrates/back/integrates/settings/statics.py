from integrates.context import CI_COMMIT_REF_NAME, STARTDIR

STATIC_URL: str = f"https://integrates.front.fluidattacks.com/{CI_COMMIT_REF_NAME}/static"
TEMPLATES_DIR: str = f"{STARTDIR}/integrates/back/integrates/app/templates"
