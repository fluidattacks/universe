from typing import (
    Literal,
)

from integrates.context import (
    FI_JWT_SECRET,
    FI_JWT_SECRET_API,
)

JWT_ALGORITHM = "RS512"
JWT_COOKIE_NAME = "integrates_session"
JWT_COOKIE_SAMESITE: Literal["lax"] = "lax"
JWT_OTP_COOKIE_NAME = "otp_token"
JWT_SECRET = FI_JWT_SECRET
JWT_SECRET_API = FI_JWT_SECRET_API
