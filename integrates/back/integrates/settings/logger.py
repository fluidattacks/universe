# Settings logger-related configs


import json
import logging.config
import re
from datetime import (
    UTC,
    datetime,
)
from decimal import (
    Decimal,
)
from logging import (
    Filter,
    Formatter,
    LogRecord,
    StreamHandler,
)
from logging.handlers import (
    QueueHandler,
    QueueListener,
)
from queue import (
    Queue,
)
from typing import (
    Any,
    Literal,
)

import bugsnag
import opentelemetry.trace
import pytz
import requests
import simplejson
from boto3.session import (
    Session,
)
from bugsnag.notification import (
    Notification,
)
from fluidattacks_core.bugsnag.client import (
    remove_nix_hash as bugsnag_remove_nix_hash,
)
from graphql import (
    GraphQLError,
)
from pythonjsonlogger import (
    jsonlogger,
)
from requests.exceptions import (
    ConnectionError as RequestConnectionError,
)
from requests.exceptions import (
    ReadTimeout,
)

from integrates.context import (
    CI_COMMIT_SHA,
    CI_COMMIT_SHORT_SHA,
    FI_AWS_REGION_NAME,
    FI_BUGSNAG_API_KEY_BACK,
    FI_ENVIRONMENT,
)
from integrates.custom_exceptions import (
    DocumentNotFound,
    UnavailabilityError,
)

from .various import (
    BASE_DIR,
    DEBUG,
)

BOTO3_SESSION = Session(
    region_name=FI_AWS_REGION_NAME,
)
LOG_LEVELS = {"NOTSET", "DEBUG", "INFO", "WARN", "ERROR", "CRITICAL"}


class RequireDebugFalse(Filter):
    def filter(self, _: LogRecord) -> bool:
        return not DEBUG


class ExtraMessageFormatter(Formatter):
    def __init__(
        self,
        datefmt: str = "%Y-%m-%dT%H:%M:%S%z",
        fmt: str = ("{asctime} <{trace_id:6.6}> {levelname:4.4} | {message}, extra={extra}"),
        style: Literal["{"] = "{",
    ) -> None:
        logging.Formatter.__init__(self, fmt=fmt, datefmt=datefmt, style=style)

    def format(self, record: LogRecord) -> str:
        """Add "None" to missing fields in the log records to avoid exceptions"""
        arg_pattern = re.compile(r"\{(\w+)\}")
        arg_names = [x.group(1) for x in arg_pattern.finditer(str(self._fmt))]
        for field in arg_names:
            if field not in record.__dict__:
                record.__dict__[field] = None

        return super().format(record)


class AsyncStreamHandler(QueueHandler):
    def __init__(self) -> None:
        self.queue = Queue()
        self.listener = QueueListener(self.queue, StreamHandler())
        self.listener.start()
        self.shutting_down = False
        super().__init__(self.queue)

    def emit(self, record: LogRecord) -> None:
        if self.shutting_down:
            return
        super().emit(record)

    def close(self) -> None:
        self.shutting_down = True
        self.listener.stop()
        super().close()


def json_default(object_: object) -> Any:
    if isinstance(object_, set):
        return list(object_)
    if isinstance(object_, datetime):
        return object_.astimezone(tz=UTC).isoformat()
    if isinstance(object_, float):
        return Decimal(str(object_))

    return object_


class CustomJsonFormatter(jsonlogger.JsonFormatter):
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(
            *args,
            **kwargs,
            json_serializer=simplejson.dumps,
            json_default=json_default,
        )

    def add_fields(
        self,
        log_record: dict[str, Any],
        record: LogRecord,
        message_dict: dict[str, Any],
    ) -> None:
        super().add_fields(log_record, record, message_dict)
        log_timestamp = None
        if timestamp := log_record.get("timestamp"):
            if isinstance(timestamp, int):
                log_timestamp = timestamp
            elif isinstance(timestamp, str) and timestamp.isdigit():
                log_timestamp = int(timestamp)

        log_record["timestamp"] = log_timestamp or round(
            datetime.now(tz=pytz.timezone("UTC")).timestamp() * 1000,
        )
        if not (log_level := log_record.get("level")) or log_level not in LOG_LEVELS:
            log_record["level"] = record.levelname

        if log_record.get("extra") is None:
            log_record.pop("extra", None)
        elif isinstance(log_record.get("extra"), dict):
            log_record.update(log_record["extra"])
            log_record.pop("extra", None)


class BugsnagLoggingHandler(bugsnag.handlers.BugsnagHandler):
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)

        def config_options(record: logging.LogRecord, options: dict) -> None:
            if record.levelname in {"ERROR", "WARNING"}:
                options["unhandled"] = True

        super().add_callback(config_options)


def _get_watchtower_handler(*, stream_name: str) -> dict:
    return {
        "boto3_client": BOTO3_SESSION.client("logs"),
        "class": "watchtower.CloudWatchLogHandler",
        "create_log_group": False,
        "create_log_stream": False,
        "filters": ["require_debug_false"],
        "formatter": "json",
        "level": "INFO",
        "log_group_name": "integrates",
        "log_stream_name": stream_name,
        "use_queues": True,
    }


LOGGING = {
    "disable_existing_loggers": False,
    "filters": {
        "require_debug_false": {"()": RequireDebugFalse},
    },
    "formatters": {
        "json": {
            "format": "%(timestamp)s %(level)s %(message)s",
            "()": CustomJsonFormatter,
        },
        "level_message_extra": {
            "()": ExtraMessageFormatter,
        },
    },
    "handlers": {
        "bugsnag": {
            "extra_fields": {"extra": ["extra"]},
            "filters": ["require_debug_false"],
            "()": BugsnagLoggingHandler,
            "level": "WARNING",
        },
        "console": {
            "()": AsyncStreamHandler,
            "level": "INFO",
            "formatter": "level_message_extra",
        },
        "watchtower": _get_watchtower_handler(stream_name="default"),
        "watchtower_process_sbom": _get_watchtower_handler(stream_name="process_sbom"),
        "watchtower_cloning": _get_watchtower_handler(stream_name="cloning"),
        "watchtower_machine": _get_watchtower_handler(stream_name="machine"),
        "watchtower_rebase": _get_watchtower_handler(stream_name="rebase"),
        "watchtower_refresh_toe_lines": _get_watchtower_handler(stream_name="refresh_toe_lines"),
        "watchtower_schedulers": _get_watchtower_handler(stream_name="schedulers"),
        "watchtower_sqs_tasks": _get_watchtower_handler(stream_name="sqs_tasks"),
    },
    "loggers": {
        "integrates": {
            "handlers": ["bugsnag"],
            "level": "INFO",
        },
        "transactional": {
            "handlers": ["watchtower"],
            "level": "INFO",
        },
        "cloning": {
            "handlers": ["watchtower_cloning"],
            "level": "INFO",
        },
        "cw_schedulers": {
            "handlers": ["watchtower_schedulers"],
            "level": "INFO",
        },
        "machine": {
            "handlers": ["watchtower_machine"],
            "level": "INFO",
        },
        "rebase": {
            "handlers": ["watchtower_rebase"],
            "level": "INFO",
        },
        "refresh_toe_lines": {
            "handlers": ["watchtower_refresh_toe_lines"],
            "level": "INFO",
        },
        "sqs_tasks": {
            "handlers": ["watchtower_sqs_tasks"],
            "level": "INFO",
        },
        "process_sbom": {
            "handlers": ["watchtower_process_sbom"],
            "level": "INFO",
        },
    },
    "root": {
        "handlers": ["console"],
        "level": "INFO",
    },
    "version": 1,
}

# Force logging to load the config right away
# This is important otherwise loggers are not going to work in CI jobs
logging.config.dictConfig(LOGGING)

old_factory = logging.getLogRecordFactory()


def record_factory(*args: tuple, **kwargs: dict) -> LogRecord:
    record = old_factory(*args, **kwargs)
    span = opentelemetry.trace.get_current_span()
    ctx = span.get_span_context()
    record.span_id = "0"
    record.trace_id = "0"
    if span.is_recording():
        record.span_id = format(ctx.span_id, "016x")
        record.trace_id = format(ctx.trace_id, "032x")
    return record


logging.setLogRecordFactory(record_factory)

# bugsnag
bugsnag.configure(
    api_key=FI_BUGSNAG_API_KEY_BACK,
    app_version=CI_COMMIT_SHORT_SHA,
    asynchronous=True,
    auto_capture_sessions=True,
    notify_release_stages=["ephemeral", "production"],
    project_root=BASE_DIR,
    release_stage=FI_ENVIRONMENT,
    send_environment=True,
)

if FI_ENVIRONMENT == "production":
    URL = "https://build.bugsnag.com"
    HEADERS = {"Content-Type": "application/json", "server": "None"}
    PAYLOAD = {
        "apiKey": FI_BUGSNAG_API_KEY_BACK,
        "appVersion": CI_COMMIT_SHORT_SHA,
        "releaseStage": FI_ENVIRONMENT,
        "sourceControl": {
            "provider": "gitlab",
            "repository": "https://gitlab.com/fluidattacks/universe.git",
            "revision": f"{CI_COMMIT_SHA}/integrates/back/integrates",
        },
    }
    try:
        requests.post(URL, headers=HEADERS, data=json.dumps(PAYLOAD), timeout=3)
    except (ReadTimeout, RequestConnectionError) as exc:
        logging.exception(exc, f"request to {URL} was not completed")


def customize_bugsnag_error_reports(notification: Notification) -> bool:
    """Handle for expected errors and customization"""
    bugsnag_remove_nix_hash(notification)
    ex_msg = str(notification.exception)

    notification.grouping_hash = ex_msg
    if isinstance(notification.exception, GraphQLError):
        return False
    if isinstance(notification.exception, UnavailabilityError):
        notification.unhandled = False
    if isinstance(notification.exception, DocumentNotFound):
        notification.severity = "info"
    if notification.errors:
        errors_messages = [
            trace.get("file", "") for error in notification.errors for trace in error.stacktrace
        ]
        if "/migrations/" in "".join(errors_messages):
            notification.unhandled = False
            notification.severity = "info"

    if not ("ec2" in notification.hostname or "integrates" in notification.hostname):
        notification.unhandled = False
    return True


bugsnag.before_notify(customize_bugsnag_error_reports)
