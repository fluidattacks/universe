from collections.abc import (
    Awaitable,
    Callable,
)
from contextlib import (
    AsyncExitStack,
)
from typing import (
    cast,
)

import aioboto3
from aiobotocore.config import (
    AioConfig,
)
from types_aiobotocore_sqs import SQSClient

from integrates.context import (
    FI_AWS_REGION_NAME,
)

SESSION = aioboto3.Session()
StartupCallable = Callable[[], Awaitable[None]]
ShutdownCallable = Callable[[], Awaitable[None]]
GetClientCallable = Callable[[], Awaitable[SQSClient]]
SQSContext = tuple[StartupCallable, ShutdownCallable, GetClientCallable]


def create_sqs_context() -> SQSContext:
    context_stack = None
    client = None

    async def startup() -> None:
        nonlocal context_stack, client

        context_stack = AsyncExitStack()
        client = await context_stack.enter_async_context(
            SESSION.client(
                config=AioConfig(
                    # The time in seconds until a timeout exception is raised
                    # when attempting to make a connection. [60]
                    connect_timeout=60,
                    # Maximum amount of simultaneously opened connections. [10]
                    # https://docs.aiohttp.org/en/stable/client_advanced.html#limiting-connection-pool-size
                    max_pool_connections=2000,
                    # The time in seconds until a timeout exception is raised
                    # when attempting to read from a connection. [60]
                    read_timeout=60,
                    # https://boto3.amazonaws.com/v1/documentation/api/latest/guide/retries.html
                    retries={"max_attempts": 10, "mode": "standard"},
                    # Signature version for signing URLs
                    # https://boto3.amazonaws.com/v1/documentation/api/1.9.42/guide/sqs.html#generating-presigned-urls
                ),
                region_name=FI_AWS_REGION_NAME,
                service_name="sqs",
                use_ssl=True,
                verify=True,
            ),
        )

    async def shutdown() -> None:
        if context_stack:
            await context_stack.aclose()

    async def get_resource() -> SQSClient:
        if client is None:
            await startup()

        return cast(SQSClient, client)

    return startup, shutdown, get_resource


sqs_startup, sqs_shutdown, get_client = create_sqs_context()
