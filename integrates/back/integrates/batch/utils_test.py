from datetime import datetime
from decimal import Decimal

import simplejson as json

from integrates.batch import dal as batch_dal
from integrates.batch.enums import Action, IntegratesBatchQueue, SkimsBatchQueue
from integrates.batch.types import ACTIONS_SCHEMA, BatchProcessing, DependentAction
from integrates.batch.utils import mapping_to_key, validate_additional_info_schema
from integrates.class_types.types import Item
from integrates.db_model.enums import TreatmentStatus
from integrates.db_model.groups.enums import GroupService, GroupSubscriptionType
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
    VulnerabilityVerificationStatus,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    BatchProcessingFaker,
    GroupFaker,
    GroupStateFaker,
    OrganizationFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize

MACHINE_EMAIL = "machine@fluidattacks.com"


@parametrize(
    args=["action_name", "entity", "subject", "additional_info", "expected_result"],
    cases=[
        [
            "report",
            "unittesting",
            "unittesting@fluidattacks.com",
            {
                "report_type": "XLS",
                "treatments": ["ACCEPTED", "UNTREATED"],
                "states": ["VULNERABLE"],
                "verifications": [],
                "closing_date": None,
                "finding_title": "038",
                "age": 1100,
                "min_severity": Decimal("2.4"),
                "max_severity": Decimal("6.4"),
            },
            "807c9ea4b5f7957e15658e6d2a08b07e0af3306f7ba600b38b12b68d6cb0a0b2",
        ],
        [
            "report",
            "unittesting",
            "unittesting@fluidattacks.com",
            {
                "report_type": "XLS",
                "treatments": [
                    "ACCEPTED",
                    "ACCEPTED_UNDEFINED",
                    "IN_PROGRESS",
                    "UNTREATED",
                ],
                "states": ["SAFE", "VULNERABLE"],
                "verifications": [],
                "closing_date": None,
                "finding_title": "068",
                "age": 1300,
                "min_severity": Decimal("2.9"),
                "max_severity": Decimal("4.3"),
                "last_report": None,
                "min_release_date": None,
                "max_release_date": None,
                "location": "",
            },
            "327210df9b86c7724e5b45028170c8258709d3d07838f35c2bb2d904a44f31f1",
        ],
    ],
)
async def test_mapping_to_key(
    action_name: str,
    entity: str,
    subject: str,
    additional_info: Item,
    expected_result: str,
) -> None:
    key = mapping_to_key(
        [
            action_name,
            entity,
            subject,
            json.dumps(additional_info),
        ]
    )

    assert key == expected_result


@parametrize(
    args=["action"],
    cases=[
        [
            BatchProcessing(
                key="78ebd9f895b8efcd4e6d4cf40d3dbcf3f6fc2ac655537edc0b0465bd3a80871c",
                action_name=Action.REPORT,
                entity="unittesting",
                subject="unittesting@fluidattacks.com",
                time=datetime.fromisoformat("2024-01-18T17:41:00+00:00"),
                additional_info={
                    "report_type": "XLS",
                    "treatments": [
                        "ACCEPTED",
                        "ACCEPTED_UNDEFINED",
                        "IN_PROGRESS",
                        "UNTREATED",
                    ],
                    "states": ["SAFE"],
                    "verifications": ["VERIFIED"],
                    "closing_date": "2020-06-01T00:00:00",
                    "finding_title": "065",
                    "age": None,
                    "min_severity": None,
                    "max_severity": None,
                    "last_report": None,
                    "min_release_date": None,
                    "max_release_date": None,
                    "location": "",
                },
                queue=IntegratesBatchQueue.SMALL,
                batch_job_id=None,
                retries=0,
                running=False,
            ),
        ],
        [
            BatchProcessing(
                key="ecfa753fb705d90f4636906dcd2fb8db7ddb06cb356e14fe0fb57c23e92fafb5",
                action_name=Action.REPORT,
                entity="unittesting",
                subject="unittesting@fluidattacks.com",
                time=datetime.fromisoformat("2024-01-18T17:42:00+00:00"),
                additional_info={
                    "report_type": "XLS",
                    "treatments": [
                        "ACCEPTED",
                        "ACCEPTED_UNDEFINED",
                        "IN_PROGRESS",
                        "UNTREATED",
                    ],
                    "states": ["SAFE", "VULNERABLE"],
                    "verifications": [],
                    "closing_date": None,
                    "finding_title": "068",
                    "age": 1300,
                    "min_severity": Decimal("2.9"),
                    "max_severity": Decimal("4.3"),
                    "last_report": None,
                    "min_release_date": None,
                    "max_release_date": None,
                    "location": "",
                },
                queue=IntegratesBatchQueue.SMALL,
                batch_job_id=None,
                retries=0,
                running=False,
            ),
        ],
    ],
)
async def test_put_action_to_batch(action: BatchProcessing) -> None:
    assert (
        await batch_dal.put_action_to_batch(
            entity=action.entity,
            action=action.action_name,
            action_dynamo_pk=action.key,
            queue=action.queue,
        )
        is None
    )


@parametrize(
    args=[
        "action_name",
        "entity",
        "subject",
        "time",
        "additional_info",
        "queue",
        "attempt_duration_seconds",
        "dependent_actions",
    ],
    cases=[
        [
            "report",
            "unittesting",
            "unittesting@fluidattacks.com",
            datetime.fromisoformat("2024-01-18T17:41:00+00:00"),
            {
                "report_type": "XLS",
                "treatments": [
                    "ACCEPTED",
                    "ACCEPTED_UNDEFINED",
                    "IN_PROGRESS",
                    "UNTREATED",
                ],
                "states": ["SAFE"],
                "verifications": ["VERIFIED"],
                "closing_date": "2020-06-01T05:00:00+00:00",
                "finding_title": "039",
                "age": 1200,
                "min_severity": Decimal("2.7"),
                "max_severity": None,
            },
            IntegratesBatchQueue.SMALL,
            3600,
            [
                DependentAction(
                    action_name=Action.REFRESH_TOE_LINES,
                    additional_info={"root_ids": ["id1"]},
                    queue=IntegratesBatchQueue.LARGE,
                    attempt_duration_seconds=7200,
                ),
            ],
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[
                GroupFaker(
                    name="group1",
                    organization_id="org1",
                    state=GroupStateFaker(service=GroupService.WHITE),
                ),
            ],
            actions=[
                BatchProcessingFaker(
                    action_name=Action.EXECUTE_MACHINE,
                    additional_info={
                        "roots": ["nickname1"],
                        "roots_config_files": ["config_nickname1_123.yaml"],
                    },
                    batch_job_id="2c95e12c-8b93-4faf-937f-1f2b34530004",
                    entity="group1",
                    queue=SkimsBatchQueue.SMALL,
                    subject="unittesting@fluidattacks.com",
                ),
            ],
        ),
    )
)
async def test_put_action_to_dynamodb(
    *,
    action_name: str,
    entity: str,
    subject: str,
    time: datetime,
    additional_info: Item,
    queue: IntegratesBatchQueue | SkimsBatchQueue,
    attempt_duration_seconds: int,
    dependent_actions: list[DependentAction],
) -> None:
    result = await batch_dal.put_action_to_dynamodb(
        action=Action[action_name.upper()],
        entity=entity,
        subject=subject,
        time=time,
        additional_info=additional_info,
        queue=queue,
        attempt_duration_seconds=attempt_duration_seconds,
        dependent_actions=dependent_actions,
    )
    assert result is True


@parametrize(
    args=["action", "additional_info", "expected"],
    cases=[
        [
            Action.CLONE_ROOTS,
            {"git_root_ids": ["id1", "id2"], "should_queue_machine": True},
            True,
        ],
        [
            Action.CLONE_ROOTS,
            {
                "git_root_ids": ["id1", "id2"],
                "should_queue_machine": True,
                "should_queue_sbom": False,
            },
            True,
        ],
        [Action.DEACTIVATE_ROOT, {"root_ids": ["id1", "id2"]}, True],
        [
            Action.EXECUTE_MACHINE,
            {
                "roots": [],
                "roots_config_files": [
                    "config_nickname1_123.yaml",
                    "config_nickname2_345.yaml",
                ],
            },
            False,
        ],
        [
            Action.EXECUTE_MACHINE,
            {
                "roots": ["nickname1", "nickname2"],
                "roots_config_files": [],
            },
            False,
        ],
        [
            Action.EXECUTE_MACHINE,
            {
                "roots": ["nickname1", "nickname2"],
            },
            False,
        ],
        [
            Action.EXECUTE_MACHINE,
            {
                "checks": ["F001", "F999"],
                "included_paths": [["/path1", "/path2"], ["/path/path3"]],
                "roots": ["nickname1", "nickname2"],
            },
            False,
        ],
        [
            Action.EXECUTE_MACHINE,
            {
                "network_name": "acme",
                "roots": ["nickname1", "nickname2"],
                "roots_config_files": [
                    "config_nickname1_123.yaml",
                    "config_nickname2_345.yaml",
                ],
            },
            True,
        ],
        [
            Action.EXECUTE_MACHINE_SAST,
            {
                "checks": ["F001", "F999"],
                "included_paths": [["/path1", "/path2"], ["/path/path3"]],
                "roots": ["nickname1", "nickname2"],
            },
            False,
        ],
        [
            Action.EXECUTE_MACHINE_SAST,
            {
                "network_name": "acme",
                "roots": ["nickname1", "nickname2"],
                "roots_config_files": [
                    "config_nickname1_123.yaml",
                    "config_nickname2_345.yaml",
                ],
                "roots_sbom_config_files": [
                    "config_nickname1_123.yaml",
                    "config_nickname2_345.yaml",
                ],
            },
            True,
        ],
        [
            Action.GENERATE_ROOT_SBOM,
            {
                "root_nicknames": ["nickname1", "nickname2"],
                "roots_config_files": [
                    "config_nickname1_123.yaml",
                    "config_nickname2_345.yaml",
                ],
                "sbom_format": "fluid-json",
                "job_type": "scheduler",
            },
            True,
        ],
        [
            Action.GENERATE_IMAGE_SBOM,
            {
                "images": [
                    {
                        "image_ref": "test-dummy-image:0.0.1",
                        "root_nickname": "nickname1",
                    }
                ],
                "images_config_files": [
                    "sbom_group1_test-dummy-image:0.0.1_1704067200_config.yaml"
                ],
                "sbom_format": "fluid-json",
                "job_type": "scheduler",
            },
            True,
        ],
        [Action.HANDLE_FINDING_POLICY, {"organization_name": "acme"}, True],
        [
            Action.MOVE_ROOT,
            {
                "source_group_name": "group1",
                "source_root_id": "id1",
                "target_group_name": "group2",
                "target_root_id": "id2",
            },
            True,
        ],
        [Action.REBASE, {}, False],
        [Action.REBASE, {"root_nicknames": ["nickname1"]}, False],
        [Action.REBASE, {"root_ids": ["id1", "id2"], "key2": "value2"}, False],
        [Action.REBASE, {"root_ids": [123, 456], "key2": "value2"}, False],
        [Action.REBASE, {"root_ids": ["id1", "id2"]}, True],
        [Action.REFRESH_TOE_INPUTS, {"root_ids": ["id1", "id2"]}, True],
        [Action.REFRESH_TOE_LINES, {"root_ids": ["id1", "id2"]}, True],
        [Action.REFRESH_TOE_PORTS, {"root_ids": ["id1", "id2"]}, True],
        [
            Action.REMOVE_GROUP_RESOURCES,
            {
                "validate_pending_actions": True,
                "subscription": GroupSubscriptionType.CONTINUOUS.value,
                "has_advanced": True,
                "emails": [],
            },
            True,
        ],
        [
            Action.REPORT,
            {"report_type": "XLS"},
            True,
        ],
        [
            Action.REPORT,
            {
                "report_type": "XLS",
                "treatments": [
                    TreatmentStatus.ACCEPTED.value,
                    TreatmentStatus.ACCEPTED_UNDEFINED.value,
                    TreatmentStatus.IN_PROGRESS.value,
                    TreatmentStatus.UNTREATED.value,
                ],
                "states": [VulnerabilityStateStatus.SAFE.value],
                "verifications": [VulnerabilityVerificationStatus.VERIFIED.value],
                "closing_date": None,
                "finding_title": "",
                "age": None,
                "min_severity": None,
                "max_severity": None,
                "last_report": None,
                "min_release_date": None,
                "max_release_date": None,
                "location": "",
            },
            True,
        ],
        [
            Action.REPORT,
            {
                "report_type": "XLS",
                "treatments": [
                    TreatmentStatus.ACCEPTED.value,
                    TreatmentStatus.ACCEPTED_UNDEFINED.value,
                    TreatmentStatus.IN_PROGRESS.value,
                    TreatmentStatus.UNTREATED.value,
                ],
                "states": [VulnerabilityStateStatus.SAFE.value],
                "verifications": [VulnerabilityVerificationStatus.VERIFIED.value],
                "closing_date": "2024-01-18T17:41:00+00:00",
                "finding_title": "065",
                "age": 1,
                "min_severity": Decimal("0.0"),
                "max_severity": Decimal("10.0"),
                "last_report": 10,
                "min_release_date": "2024-01-18T00:00:00+00:00",
                "max_release_date": "2024-01-19T23:59:59+00:00",
                "location": "abc",
            },
            True,
        ],
        [Action.SEND_EXPORTED_FILE, {}, True],
        [Action.UPDATE_GROUP_LANGUAGES, {"root_ids": ["id1", "id2"]}, True],
        [
            Action.UPDATE_ORGANIZATION_REPOSITORIES,
            {"credentials_id": "id1"},
            True,
        ],
        [
            Action.REFRESH_REPOSITORIES,
            {},
            True,
        ],
        [Action.UPDATE_VULNS_AUTHOR, {"root_ids": ["id1", "id2"]}, True],
    ],
)
async def test_validate_additional_info_schema(
    action: Action, additional_info: Item, expected: bool
) -> None:
    assert validate_additional_info_schema(action, additional_info) == expected


async def test_validate_action_schemas() -> None:
    for action in Action:
        assert action in ACTIONS_SCHEMA, f"{action} is missing from ACTIONS_SCHEMA"
