import hashlib
import logging
import logging.config
from datetime import (
    datetime,
)

import simplejson as json
from jsonschema import (
    validate,
)
from jsonschema.exceptions import (
    ValidationError,
)

from integrates.batch.constants import (
    DEFAULT_ACTION_TIMEOUT,
)
from integrates.batch.enums import (
    Action,
    IntegratesBatchQueue,
    Product,
    SkimsBatchQueue,
)
from integrates.batch.types import (
    ACTIONS_SCHEMA,
    BatchProcessing,
    BatchProcessingToUpdate,
    DependentAction,
)
from integrates.class_types.types import (
    Item,
)
from integrates.custom_utils.encodings import (
    safe_encode,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)


def format_dependent_actions(raw: list[Item]) -> list[DependentAction]:
    return [
        DependentAction(
            action_name=Action[str(item["action_name"]).upper()],
            additional_info=json.loads(item["additional_info"]),
            attempt_duration_seconds=int(
                item.get("attempt_duration_seconds", DEFAULT_ACTION_TIMEOUT),
            ),
            job_name=item.get("job_name"),
            queue=format_queue(item["queue"]),
        )
        for item in raw
    ]


def format_dependent_actions_items(
    dependent_actions: list[DependentAction],
) -> list[Item]:
    return [
        {
            "action_name": dependent.action_name.value,
            "additional_info": json.dumps(dependent.additional_info),
            "attempt_duration_seconds": dependent.attempt_duration_seconds,
            "job_name": dependent.job_name,
            "queue": dependent.queue.value,
        }
        for dependent in dependent_actions
    ]


def format_queue(raw: str) -> IntegratesBatchQueue | SkimsBatchQueue:
    try:
        return IntegratesBatchQueue(raw.lower())
    except (KeyError, ValueError):
        return SkimsBatchQueue(raw.lower())


def format_batch_processing(item: Item) -> BatchProcessing:
    return BatchProcessing(
        key=item["pk"],
        action_name=Action[str(item["action_name"]).upper()],
        entity=item["entity"].lower(),
        subject=item["subject"].lower(),
        time=datetime.fromisoformat(item["time"]),
        additional_info=json.loads(item["additional_info"]),
        attempt_duration_seconds=int(item.get("attempt_duration_seconds", DEFAULT_ACTION_TIMEOUT)),
        queue=format_queue(item["queue"]),
        batch_job_id=item.get("batch_job_id"),
        dependent_actions=format_dependent_actions(item["dependent_actions"])
        if item.get("dependent_actions")
        else None,
        retries=item.get("retries", 0),
        running=item.get("running", False),
    )


def format_batch_processing_to_update_item(
    attributes: BatchProcessingToUpdate,
) -> Item:
    item = {
        "additional_info": json.dumps(attributes.additional_info)
        if attributes.additional_info
        else None,
        "attempt_duration_seconds": attributes.attempt_duration_seconds
        if attributes.attempt_duration_seconds
        else None,
        "batch_job_id": attributes.batch_job_id if attributes.batch_job_id is not None else None,
        "retries": attributes.retries if attributes.retries is not None else None,
        "running": attributes.running if attributes.running is not None else None,
        "dependent_actions": format_dependent_actions_items(attributes.dependent_actions)
        if attributes.dependent_actions is not None
        else None,
    }

    return {key: value for key, value in item.items() if value is not None}


def generate_key_to_dynamodb(
    *,
    action: Action,
    additional_info: Item,
    entity: str,
    subject: str,
) -> str:
    return mapping_to_key([action.value, json.dumps(additional_info), entity, subject])


def get_product(action: Action) -> Product:
    return (
        Product.SKIMS
        if action
        in {
            Action.EXECUTE_MACHINE,
            Action.EXECUTE_MACHINE_SAST,
        }
        else Product.INTEGRATES
    )


def mapping_to_key(items: list[str]) -> str:
    key = ".".join([safe_encode(attribute_value) for attribute_value in sorted(items)])
    return hashlib.sha256(key.encode()).hexdigest()


def validate_additional_info_schema(action: Action, additional_info: Item) -> bool:
    if action not in ACTIONS_SCHEMA:
        LOGGER.error(
            "Action with no schema for additional info",
            extra={"action": action, "additional_info": additional_info},
        )
        return False

    try:
        validate(additional_info, ACTIONS_SCHEMA[action])
        return True
    except ValidationError as ex:
        LOGGER.exception(
            ex,
            extra={"extra": {"action": action, "additional_info": additional_info}},
        )
        return False
