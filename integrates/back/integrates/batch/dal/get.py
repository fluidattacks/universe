from boto3.dynamodb.conditions import (
    Key,
)

from integrates.batch.enums import (
    Action,
)
from integrates.batch.resources import (
    TABLE,
)
from integrates.batch.types import (
    BatchProcessing,
)
from integrates.batch.utils import (
    format_batch_processing,
)
from integrates.dynamodb import (
    operations as dynamodb_ops,
)
from integrates.dynamodb.types import (
    SimpleKey,
)


async def get_action(*, action_key: str) -> BatchProcessing | None:
    item = await dynamodb_ops.get_item(
        facets=(TABLE.facets["action_metadata"],),
        key=SimpleKey(partition_key=action_key),
        table=TABLE,
    )

    return format_batch_processing(item) if item else None


async def get_actions_by_name(*, action_name: Action, entity: str) -> list[BatchProcessing]:
    index = TABLE.indexes["gsi-1"]
    response = await dynamodb_ops.query(
        condition_expression=(
            Key(index.primary_key.partition_key).eq(action_name)
            & Key(index.primary_key.sort_key).eq(entity)
        ),
        facets=(TABLE.facets["action_metadata"],),
        table=TABLE,
        index=index,
    )

    return [format_batch_processing(item) for item in response.items]


async def get_actions() -> list[BatchProcessing]:
    items = await dynamodb_ops.scan(table=TABLE)

    return [format_batch_processing(item) for item in items]
