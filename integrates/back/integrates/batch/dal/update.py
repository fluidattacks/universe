import logging
import logging.config

from boto3.dynamodb.conditions import (
    Attr,
)
from botocore.exceptions import (
    ClientError,
)

from integrates.batch.enums import (
    Action,
)
from integrates.batch.resources import (
    TABLE,
)
from integrates.batch.types import (
    BatchProcessingToUpdate,
)
from integrates.batch.utils import (
    format_batch_processing_to_update_item,
    validate_additional_info_schema,
)
from integrates.dynamodb import (
    operations as dynamodb_ops,
)
from integrates.dynamodb.types import (
    SimpleKey,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)


async def update_action_to_dynamodb(
    *,
    action: Action,
    action_key: str,
    attributes: BatchProcessingToUpdate,
) -> bool:
    if attributes.additional_info and not validate_additional_info_schema(
        action,
        attributes.additional_info,
    ):
        return False

    if attributes.dependent_actions and not all(
        validate_additional_info_schema(dependent.action_name, dependent.additional_info)
        for dependent in attributes.dependent_actions
    ):
        return False

    if not (item := format_batch_processing_to_update_item(attributes)):
        return False

    key_structure = TABLE.primary_key
    condition_expression = Attr(key_structure.partition_key).exists()
    try:
        await dynamodb_ops.update_item(
            condition_expression=condition_expression,
            item=item,
            key=SimpleKey(partition_key=action_key),
            table=TABLE,
        )

        return True
    except ClientError as ex:
        LOGGER.exception(ex, extra={"extra": locals()})

    return False
