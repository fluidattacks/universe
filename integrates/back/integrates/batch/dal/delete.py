import logging
import logging.config

from botocore.exceptions import (
    ClientError,
)

from integrates.batch.resources import (
    TABLE,
)
from integrates.dynamodb import (
    operations as dynamodb_ops,
)
from integrates.dynamodb.types import (
    SimpleKey,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)


async def delete_action(*, action_key: str) -> None:
    try:
        await dynamodb_ops.delete_item(
            key=SimpleKey(partition_key=action_key),
            table=TABLE,
        )
    except ClientError as exc:
        LOGGER.exception(exc, extra={"extra": locals()})
