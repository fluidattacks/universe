import json
from datetime import datetime

from integrates.batch.dal.get import get_action, get_actions
from integrates.batch.enums import Action, IntegratesBatchQueue, SkimsBatchQueue
from integrates.dynamodb import (
    operations as dynamodb_ops,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    BatchProcessingFaker,
    GitRootFaker,
    GitRootStateFaker,
    GroupFaker,
    OrganizationFaker,
)
from integrates.testing.mocks import Mock, mocks


@mocks(
    others=[
        Mock(
            dynamodb_ops,
            "get_item",
            "async",
            {
                "additional_info": json.dumps(
                    {
                        "report_type": "XLS",
                        "treatments": ["ACCEPTED", "UNTREATED"],
                        "states": ["VULNERABLE"],
                        "verifications": ["REQUESTED"],
                        "closing_date": "null",
                        "finding_title": "038",
                        "age": 1100,
                        "min_severity": "2.7",
                        "max_severity": "null",
                    }
                ),
                "subject": "unittesting@fluidattacks.com",
                "action_name": "report",
                "pk": "key1",
                "time": "2021-03-19T01:12:28+00:00",
                "entity": "unittesting",
                "queue": "integrates",
            },
        )
    ],
)
async def test_get_action() -> None:
    action = await get_action(action_key="key1")
    assert bool(action) is True


@mocks(
    others=[
        Mock(
            dynamodb_ops,
            "get_item",
            "async",
            None,
        )
    ],
)
async def test_get_no_action() -> None:
    action = await get_action(action_key="key2")
    assert bool(action) is False


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[GroupFaker(name="group1", organization_id="org1")],
            roots=[
                GitRootFaker(
                    group_name="group1", id="root1", state=GitRootStateFaker(nickname="root1")
                ),
            ],
            actions=[
                BatchProcessingFaker(
                    action_name=Action.EXECUTE_MACHINE,
                    additional_info={
                        "roots": ["nickname1"],
                        "roots_config_files": ["config_nickname1_123.yaml"],
                    },
                    batch_job_id="fda5fcbe-8986-4af7-9e54-22a7d8e7981f",
                    entity="group2",
                    queue=SkimsBatchQueue.SMALL,
                    subject="unittesting@fluidattacks.com",
                    time=datetime.fromisoformat("2024-01-18T17:42:00+00:00"),
                ),
                BatchProcessingFaker(
                    action_name=Action.REFRESH_TOE_INPUTS,
                    additional_info={"root_ids": ["id4"]},
                    batch_job_id="42d5b400-89f3-498c-b7ce-cc29d2e7f254",
                    entity="group6",
                    queue=IntegratesBatchQueue.SMALL,
                    subject="unittesting@fluidattacks.com",
                    time=datetime.fromisoformat("2024-01-18T17:46:00+00:00"),
                ),
            ],
        ),
    ),
)
async def test_get_actions() -> None:
    all_actions = await get_actions()
    assert all_actions
    assert len(all_actions) == 2
