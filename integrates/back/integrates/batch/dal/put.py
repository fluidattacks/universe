import logging
import logging.config
from datetime import (
    datetime,
)

import simplejson as json
from botocore.exceptions import (
    ClientError,
    CredentialRetrievalError,
    EndpointConnectionError,
    ParamValidationError,
)

from integrates.batch import (
    dal as batch_dal,
)
from integrates.batch.constants import (
    DEFAULT_ACTION_TIMEOUT,
)
from integrates.batch.enums import (
    Action,
    IntegratesBatchQueue,
    SkimsBatchQueue,
)
from integrates.batch.resources import (
    CPU_SIZES,
    JOB_DEFINITIONS,
    MEMORY_SIZES,
    TABLE,
    get_batch_client,
)
from integrates.batch.types import (
    BatchProcessingToUpdate,
    DependentAction,
    PutActionResult,
)
from integrates.batch.utils import (
    format_dependent_actions_items,
    generate_key_to_dynamodb,
    get_product,
    validate_additional_info_schema,
)
from integrates.class_types.types import (
    Item,
)
from integrates.context import (
    FI_ENVIRONMENT,
)
from integrates.custom_utils.datetime import (
    get_as_utc_iso_format,
    get_utc_now,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.dynamodb import (
    operations as dynamodb_ops,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)


async def put_action_to_dynamodb(
    *,
    action: Action,
    entity: str,
    subject: str,
    time: datetime,
    additional_info: Item,
    queue: IntegratesBatchQueue | SkimsBatchQueue,
    attempt_duration_seconds: int = DEFAULT_ACTION_TIMEOUT,
    batch_job_id: str | None = None,
    dependent_actions: list[DependentAction] | None = None,
) -> bool:
    if not validate_additional_info_schema(action, additional_info):
        return False

    if dependent_actions and not all(
        validate_additional_info_schema(dependent.action_name, dependent.additional_info)
        for dependent in dependent_actions
    ):
        return False

    action_key = generate_key_to_dynamodb(
        action=action,
        additional_info=additional_info,
        entity=entity,
        subject=subject,
    )
    item = {
        "pk": action_key,
        "action_name": action.value,
        "additional_info": json.dumps(additional_info),
        "attempt_duration_seconds": attempt_duration_seconds,
        "entity": entity,
        "subject": subject,
        "time": get_as_utc_iso_format(time),
        "queue": queue.value,
        "batch_job_id": batch_job_id,
        "dependent_actions": format_dependent_actions_items(dependent_actions)
        if dependent_actions
        else None,
    }
    try:
        await dynamodb_ops.put_item(
            facet=TABLE.facets["action_metadata"],
            item=item,
            table=TABLE,
        )

        return True
    except ClientError as exc:
        LOGGER.exception(exc, extra={"extra": locals()})

    return False


async def put_action_to_batch(
    *,
    action: Action,
    action_dynamo_pk: str,
    entity: str,
    queue: IntegratesBatchQueue | SkimsBatchQueue,
    attempt_duration_seconds: int = DEFAULT_ACTION_TIMEOUT,
    job_name: str | None = None,
) -> str | None:
    if FI_ENVIRONMENT != "production":
        return None

    product = get_product(action)

    match action:
        case Action.EXECUTE_MACHINE:
            command_name = "/integrates/jobs/execute_machine"
        case Action.EXECUTE_MACHINE_SAST:
            command_name = "/integrates/jobs/execute_machine_sast"
        case Action.GENERATE_ROOT_SBOM | Action.GENERATE_IMAGE_SBOM:
            command_name = "/integrates/jobs/execute_sbom"
        case _:
            command_name = f"/{product.value}/batch"

    batch_client = await get_batch_client()
    batch_client_submit_job = retry_on_exceptions(
        exceptions=(ClientError, EndpointConnectionError),
        max_attempts=3,
        sleep_seconds=5.0,
    )(batch_client.submit_job)

    try:
        return (
            await batch_client_submit_job(
                jobName=job_name or f"{product.value}-{action.value}-{entity}",
                jobQueue=queue.value,
                jobDefinition=JOB_DEFINITIONS[queue.value],
                containerOverrides={
                    "command": [
                        "m",
                        "gitlab:fluidattacks/universe@trunk",
                        command_name,
                        "prod",
                        action_dynamo_pk,
                    ],
                    "environment": [
                        {
                            "name": "CI",
                            "value": "true",
                        },
                        {
                            "name": "MAKES_AWS_BATCH_COMPAT",
                            "value": "true",
                        },
                        {
                            "name": "FLUIDATTACKS_EXECUTION",
                            "value": "true",
                        },
                    ],
                    "resourceRequirements": [
                        {
                            "type": "MEMORY",
                            "value": str(MEMORY_SIZES[queue.value]),
                        },
                        {
                            "type": "VCPU",
                            "value": str(CPU_SIZES[queue.value]),
                        },
                    ],
                },
                retryStrategy={
                    "attempts": 1,
                },
                timeout={"attemptDurationSeconds": attempt_duration_seconds},
            )
        )["jobId"]
    except (
        ClientError,
        CredentialRetrievalError,
        EndpointConnectionError,
        ParamValidationError,
    ) as exc:
        LOGGER.error(
            "Error executing batch request",
            extra={
                "extra": {
                    "action_name": action.value,
                    "action_dynamo_pk": action_dynamo_pk,
                    "job_name": f"{product.value}-{action.value}-{entity}",
                    "exc": exc,
                },
            },
        )
        return None


async def put_action(
    *,
    action: Action,
    additional_info: Item,
    entity: str,
    subject: str,
    queue: IntegratesBatchQueue | SkimsBatchQueue,
    attempt_duration_seconds: int = DEFAULT_ACTION_TIMEOUT,
    dependent_actions: list[DependentAction] | None = None,
    job_name: str | None = None,
) -> PutActionResult:
    action_key = generate_key_to_dynamodb(
        action=action,
        additional_info=additional_info,
        entity=entity,
        subject=subject,
    )
    if (current_action := await batch_dal.get_action(action_key=action_key)) and (
        not current_action.running
    ):
        LOGGER.info(
            "Action already in queue",
            extra={
                "extra": {
                    "action_key": action_key,
                    "action_name": action.value,
                    "batch_job_id": current_action.batch_job_id,
                    "entity": entity,
                },
            },
        )

        return PutActionResult(
            success=False,
            batch_job_id=current_action.batch_job_id,
            dynamo_pk=action_key,
        )

    if not await put_action_to_dynamodb(
        action=action,
        entity=entity,
        subject=subject,
        time=get_utc_now(),
        additional_info=additional_info,
        attempt_duration_seconds=attempt_duration_seconds,
        queue=queue,
        dependent_actions=dependent_actions,
    ):
        return PutActionResult(success=False)

    batch_job_id = await put_action_to_batch(
        action=action,
        queue=queue,
        entity=entity,
        attempt_duration_seconds=attempt_duration_seconds,
        action_dynamo_pk=action_key,
        job_name=job_name,
    )
    await batch_dal.update_action_to_dynamodb(
        action=action,
        action_key=action_key,
        attributes=BatchProcessingToUpdate(batch_job_id=batch_job_id),
    )
    LOGGER.info(
        "Action queued",
        extra={
            "extra": {
                "action_key": action_key,
                "action_name": action.value,
                "additional_info": additional_info,
                "batch_job_id": batch_job_id,
                "entity": entity,
                "subject": subject,
            },
        },
    )

    return PutActionResult(
        success=True,
        batch_job_id=batch_job_id,
        dynamo_pk=action_key,
    )
