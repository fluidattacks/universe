"""
Query okada mailmap to get the entries with different domains

This migration script queries the Okada mailmap to identify entries that
have subentries with different domains.

This migration is to be executed locally.
"""

import logging
import logging.config
import time

from aioextensions import (
    run,
)

from integrates.mailmap.get import (
    get_mailmap_entries_with_subentries,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


ORGANIZATION_ID = "okada"


async def main() -> None:
    entries_with_subentries = await get_mailmap_entries_with_subentries(
        organization_id=ORGANIZATION_ID,
    )

    conflicting_entries = 0

    for entry_with_subentries in entries_with_subentries:
        entry = entry_with_subentries["entry"]
        subentries = entry_with_subentries["subentries"]

        entry_email = entry["mailmap_entry_email"]
        subentries_emails = [subentry["mailmap_subentry_email"] for subentry in subentries]

        emails = [entry_email] + subentries_emails

        if any("@" not in email for email in emails):
            LOGGER_CONSOLE.info(
                "Entry %s has incorrect emails: %s\n\n",
                entry_email,
                emails,
            )

        domains = [email.split("@")[1] for email in emails if "@" in email]

        different_domains = set(domains)

        if len(different_domains) > 1:
            LOGGER_CONSOLE.info(
                "Entry %s has subentries with different domains: %s\n%s\n\n",
                entry_email,
                emails,
                different_domains,
            )

            conflicting_entries += 1

    LOGGER_CONSOLE.info(
        "Conflicting entries: %s out of %s\n\n",
        conflicting_entries,
        len(entries_with_subentries),
    )


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:        %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
