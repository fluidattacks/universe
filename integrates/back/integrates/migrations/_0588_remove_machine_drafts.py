# type: ignore
"""
Remove machine drafts from deleted findings and F372 due to FP reports

Start Time:        2024-08-30 at 15:20:59 UTC
Finalization Time: 2024-08-30 at 15:44:05 UTC

"""

import csv
import time

from aioextensions import (
    collect,
    run,
)

from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.enums import (
    Source,
    StateRemovalJustification,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateReason,
    VulnerabilityStateStatus,
)
from integrates.findings.domain import (
    remove_finding,
)
from integrates.jobs_orchestration.jobs import (
    FINDINGS as MACHINE_FINDINGS,
)
from integrates.organizations.domain import (
    get_all_active_group_names,
)
from integrates.vulnerabilities.domain import (
    remove_vulnerability,
)


async def delete_machine_drafts_locations(loaders, group: str) -> None:
    findings = await loaders.group_findings.load(group)
    findings_numb = [
        finding
        for finding in findings
        if finding.creation
        and finding.creation.modified_by == "machine@fluidattacks.com"
        and (fin_code := f"F{finding.title[:3]}")
        and (fin_code not in MACHINE_FINDINGS or fin_code == "F372")
    ]

    findings_vulns = await loaders.finding_vulnerabilities.load_many(
        [finding.id for finding in findings_numb],
    )
    rows = []

    for finding, vulns in zip(findings_numb, findings_vulns, strict=False):
        if len(vulns) == 0:
            continue
        draft_vulns = [
            vuln
            for vuln in vulns
            if (
                (
                    vuln.state.source == Source.MACHINE
                    or vuln.hacker_email == "machine@fluidattacks.com"
                )
                and vuln.skims_method is not None
                and vuln.state.status
                in {
                    VulnerabilityStateStatus.REJECTED,
                    VulnerabilityStateStatus.SUBMITTED,
                }
            )
        ]
        if len(draft_vulns) == len(vulns):
            try:
                await remove_finding(
                    loaders,
                    "flagos@fluidattacks.com",
                    finding.id,
                    StateRemovalJustification.NO_JUSTIFICATION,
                    Source.MACHINE,
                )

                rows.append(
                    [
                        group,
                        finding.title[:3],
                        finding.id,
                        "FINDING REMOVED",
                        len(draft_vulns),
                    ],
                )
            except Exception:
                rows.append([group, finding.title[:3], finding.id, "ERROR REMOVING"])

        elif len(draft_vulns) > 0:
            try:
                await collect(
                    (
                        remove_vulnerability(
                            loaders=loaders,
                            finding_id=vuln.finding_id,
                            vulnerability_id=vuln.id,
                            justification=VulnerabilityStateReason.CONSISTENCY,
                            email="flagos@fluidattacks.com",
                            include_closed_vuln=True,
                        )
                        for vuln in draft_vulns
                    ),
                    workers=15,
                )

                rows.append(
                    [
                        group,
                        finding.title[:3],
                        finding.id,
                        "VULNS REMOVED",
                        len(draft_vulns),
                    ],
                )
            except Exception:
                rows.append([group, finding.title[:3], finding.id, "ERROR REMOVING"])

    with open("removed_drafts.csv", "a+", encoding="utf-8") as handler:
        writer = csv.writer(handler)
        writer.writerows(rows)


async def main() -> None:
    loaders = get_new_context()
    groups = sorted(await get_all_active_group_names(loaders))
    for group in groups:
        print(f"Processing group {group}")
        await delete_machine_drafts_locations(loaders, group)


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    print(execution_time, "\n", finalization_time)
