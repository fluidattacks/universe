"""
Execution Time:    2025-01-09 at 21:09:43 UTC
Finalization Time: 2025-01-09 at 21:09:53 UTC
Execution Time:    2025-01-10 at 01:32:48 UTC
Finalization Time: 2025-01-10 at 02:18:59 UTC
Execution Time:    2025-01-10 at 18:05:07 UTC
Finalization Time: 2025-01-10 at 18:38:14 UTC
Execution Time:    2025-01-10 at 21:06:39 UTC
Finalization Time: 2025-01-10 at 21:17:19 UTC
Execution Time:    2025-01-11 at 01:42:35 UTC
Finalization Time: 2025-01-11 at 02:15:34 UTC
Execution Time:    2025-01-13 at 14:17:06 UTC
Finalization Time: 2025-01-13 at 14:39:04 UTC
Execution Time:    2025-01-13 at 17:17:06 UTC
Finalization Time: 2025-01-13 at 17:32:12 UTC
Execution Time:    2025-01-13 at 21:44:12 UTC
Finalization Time: 2025-01-13 at 22:03:18 UTC

Update severity_score in some vulnerabilities
"""

import logging
from datetime import datetime

from aioextensions import collect
from boto3.dynamodb.conditions import Key

from integrates.dataloaders import get_new_context
from integrates.db_model import TABLE
from integrates.db_model.groups.types import Group
from integrates.db_model.items import VulnerabilityItem
from integrates.db_model.vulnerabilities import update_metadata
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
    VulnerabilityVerificationStatus,
)
from integrates.db_model.vulnerabilities.types import (
    GroupVulnerabilitiesRequest,
    Vulnerability,
    VulnerabilityMetadataToUpdate,
)
from integrates.db_model.vulnerabilities.utils import format_vulnerability
from integrates.dynamodb import keys, operations
from integrates.migrations.utils import log_time
from integrates.organizations.domain import get_all_groups

LOGGER = logging.getLogger("migrations")


async def get_vulnerability(*, vulnerability_id: str, backup_name: str) -> Vulnerability | None:
    primary_key = keys.build_key(
        facet=TABLE.facets["vulnerability_metadata"], values={"id": vulnerability_id}
    )

    key_structure = TABLE.primary_key
    response = await operations.DynamoClient[VulnerabilityItem].query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(TABLE.facets["vulnerability_metadata"],),
        limit=1,
        table=TABLE._replace(name=f"integrates_vms_{backup_name}"),
    )

    if not response.items:
        return None

    return format_vulnerability(response.items[0])


async def update_vulnerability(*, vulnerability: Vulnerability, backup_name: str) -> None:
    if backup_name == "2024-02":
        return None

    _vulnerability = await get_vulnerability(
        vulnerability_id=vulnerability.id, backup_name=backup_name
    )
    if _vulnerability is None or _vulnerability.state.status in [
        VulnerabilityStateStatus.DELETED,
        VulnerabilityStateStatus.SAFE,
    ]:
        return await update_vulnerability(
            vulnerability=vulnerability,
            backup_name=f"{backup_name[:-1]}{int(backup_name[-1]) - 1}",
        )
    if (
        _vulnerability.state.status == VulnerabilityStateStatus.VULNERABLE
        and _vulnerability.severity_score is not None
    ):
        await update_metadata(
            finding_id=_vulnerability.finding_id,
            vulnerability_id=_vulnerability.id,
            metadata=VulnerabilityMetadataToUpdate(severity_score=_vulnerability.severity_score),
        )

    if (
        _vulnerability.state.status == VulnerabilityStateStatus.VULNERABLE
        and _vulnerability.severity_score is None
    ):
        return None


async def process_group(group: Group) -> None:
    loaders = get_new_context()
    vulnerabilities: list[Vulnerability] = [
        edge.node
        for edge in (
            await loaders.group_vulnerabilities.load(
                GroupVulnerabilitiesRequest(group_name=group.name)
            )
        ).edges
    ]
    verified_vulnerabilities = [
        vulnerability
        for vulnerability in vulnerabilities
        if vulnerability.verification
        and vulnerability.state.status
        in [VulnerabilityStateStatus.DELETED, VulnerabilityStateStatus.SAFE]
        and vulnerability.verification.status == VulnerabilityVerificationStatus.VERIFIED
        and vulnerability.verification.modified_date
        > datetime.fromisoformat("2024-03-15T00:00:00+00:00")
        and vulnerability.verification.modified_date
        < datetime.fromisoformat("2025-01-09T00:00:00+00:00")
        and vulnerability.severity_score is None
    ]
    await collect(
        [
            update_vulnerability(
                vulnerability=vulnerability,
                backup_name=vulnerability.verification.modified_date.isoformat()[:7],
            )
            for vulnerability in verified_vulnerabilities
            if vulnerability.verification
            and vulnerability.verification.status == VulnerabilityVerificationStatus.VERIFIED
        ],
        workers=12,
    )


@log_time(LOGGER)
async def main() -> None:
    _all_groups = await get_all_groups(get_new_context())
    all_groups = sorted(_all_groups, key=lambda x: x.name, reverse=True)
    await collect([process_group(group) for group in all_groups], workers=4)


if __name__ == "__main__":
    main()  # type: ignore[unused-coroutine]
