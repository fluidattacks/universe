"""
Join duplicated finding reported by hackers on a specific group
Execution Time:    2024-07-29 at 21:14:27 UTC
Finalization Time: 2024-07-29 at 21:14:34 UTC
"""

import logging
import logging.config
import time
import uuid
from contextlib import (
    suppress,
)
from io import (
    BytesIO,
)

import aiofiles
from aioextensions import (
    collect,
    run,
)
from botocore.exceptions import (
    ConnectTimeoutError,
    ReadTimeoutError,
)
from starlette.datastructures import (
    UploadFile,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    finding_comments as finding_comments_model,
)
from integrates.db_model import (
    vulnerabilities as vulns_model,
)
from integrates.db_model.enums import (
    Source,
    StateRemovalJustification,
)
from integrates.db_model.finding_comments.enums import (
    CommentType,
)
from integrates.db_model.finding_comments.types import (
    FindingComment,
    FindingCommentsRequest,
)
from integrates.db_model.findings.types import (
    Finding,
    FindingEvidence,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityRequest,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.findings import (
    domain as findings_domain,
)
from integrates.findings.domain.evidence import (
    EVIDENCE_NAMES,
    download_evidence_file,
    update_evidence,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")

GROUP_NAME = "GROUP1"

DUPLICATED_FINDINGS = [
    {
        "latest_report": "latest_report_id",
        "oldest_report": "oldest_report_id",
    },
]


def log_details(message: str, source_finding: Finding, target_finding: Finding) -> None:
    LOGGER_CONSOLE.info(
        message,
        extra={
            "extra": {
                "title": source_finding.title,
                "source_finding": {
                    "id": source_finding.id,
                    "reported_by": source_finding.creation.modified_by
                    if source_finding.creation
                    else source_finding.state.modified_by,
                },
                "target_finding": {
                    "id": target_finding.id,
                    "reported_by": target_finding.creation.modified_by
                    if target_finding.creation
                    else target_finding.state.modified_by,
                },
            },
        },
    )


@retry_on_exceptions(
    exceptions=(ReadTimeoutError, ConnectTimeoutError),
    sleep_seconds=3,
)
async def process_finding_evidence(
    *,
    loaders: Dataloaders,
    target_finding: Finding,
    source_finding: Finding,
) -> None:
    for evidence_id, evidence_name in EVIDENCE_NAMES.items():
        evidence: FindingEvidence | None = getattr(source_finding.evidences, evidence_name)
        if not evidence:
            continue
        loaders.finding.clear(target_finding.id)
        with suppress(Exception):
            file_path = await download_evidence_file(
                source_finding.group_name,
                source_finding.id,
                evidence.url,
            )
            async with aiofiles.open(file_path, "rb") as new_file:
                file_contents = await new_file.read()
                await update_evidence(
                    loaders=loaders,
                    finding_id=target_finding.id,
                    evidence_id=evidence_id,
                    file_object=UploadFile(
                        filename=new_file.name,  # type: ignore
                        file=BytesIO(file_contents),
                    ),
                    author_email=evidence.author_email,
                    is_draft=evidence.is_draft,
                    description=evidence.description,
                )
    log_details("Evidence processed", source_finding, target_finding)


@retry_on_exceptions(
    exceptions=(ReadTimeoutError, ConnectTimeoutError),
    sleep_seconds=3,
)
async def process_finding_comments(
    *,
    loaders: Dataloaders,
    source_finding_id: str,
    target_finding_id: str,
) -> None:
    all_comments: list[FindingComment] = await loaders.finding_comments.load(
        FindingCommentsRequest(
            comment_type=CommentType.COMMENT,
            finding_id=source_finding_id,
        ),
    )

    if not all_comments:
        return

    await collect(
        tuple(
            finding_comments_model.add(
                finding_comment=comment._replace(finding_id=target_finding_id),
            )
            for comment in all_comments
        ),
    )

    LOGGER_CONSOLE.info(
        "Comments processed",
        extra={
            "extra": {
                "comments": len(all_comments),
            },
        },
    )


@retry_on_exceptions(
    exceptions=(ReadTimeoutError, ConnectTimeoutError),
    sleep_seconds=3,
)
async def process_vulnerability(
    *,
    loaders: Dataloaders,
    target_finding_id: str,
    vulnerability: Vulnerability,
) -> None:
    LOGGER_CONSOLE.info(
        "Processing vulnerability",
        extra={
            "extra": {
                "vulnerability": vulnerability.id,
            },
        },
    )
    await vulns_model.add(
        vulnerability=vulnerability._replace(id=str(uuid.uuid4()), finding_id=target_finding_id),
    )

    vulnerability_request = VulnerabilityRequest(
        finding_id=vulnerability.finding_id,
        vulnerability_id=vulnerability.id,
    )

    historic_state = await loaders.vulnerability_historic_state.load(vulnerability_request)
    historic_treatment = await loaders.vulnerability_historic_treatment.load(vulnerability_request)
    historic_verification = await loaders.vulnerability_historic_verification.load(
        vulnerability_request,
    )
    historic_zero_risk = await loaders.vulnerability_historic_zero_risk.load(vulnerability_request)

    await vulns_model.update_historic(
        current_value=vulnerability,
        historic=tuple(historic_state) or (vulnerability.state,),
    )
    if historic_treatment:
        await vulns_model.update_historic(
            current_value=vulnerability,
            historic=tuple(historic_treatment),
        )
    if historic_verification:
        await vulns_model.update_historic(
            current_value=vulnerability,
            historic=tuple(historic_verification),
        )
    if historic_zero_risk:
        await vulns_model.update_historic(
            current_value=vulnerability,
            historic=tuple(historic_zero_risk),
        )

    LOGGER_CONSOLE.info(
        "Vulnerability processed",
        extra={
            "extra": {
                "vulnerability": vulnerability.id,
            },
        },
    )


async def process_finding(
    *,
    loaders: Dataloaders,
    source_finding: Finding,
    target_finding: Finding,
) -> None:
    if source_finding.title != target_finding.title:
        log_details("Title mismatch between findings", source_finding, target_finding)
        return

    log_details("Processing finding", source_finding, target_finding)
    user_email = "integrates@fluidattacks.com"

    vulnerabilities = await loaders.finding_vulnerabilities_all.load(source_finding.id)

    await process_finding_evidence(
        loaders=loaders,
        source_finding=source_finding,
        target_finding=target_finding,
    )
    await process_finding_comments(
        loaders=loaders,
        target_finding_id=target_finding.id,
        source_finding_id=source_finding.id,
    )

    for vulnerability in vulnerabilities:
        await process_vulnerability(
            loaders=loaders,
            vulnerability=vulnerability,
            target_finding_id=target_finding.id,
        )

    await findings_domain.remove_finding(
        loaders=loaders,
        email=user_email,
        finding_id=source_finding.id,
        justification=StateRemovalJustification.DUPLICATED,
        source=Source.ASM,
    )

    log_details("Finding processed", source_finding, target_finding)


async def main() -> None:
    loaders = get_new_context()
    sources = [
        await loaders.finding.load(findings_info["latest_report"])
        for findings_info in DUPLICATED_FINDINGS
    ]
    targets = [
        await loaders.finding.load(findings_info["oldest_report"])
        for findings_info in DUPLICATED_FINDINGS
    ]

    await collect(
        [
            process_finding(
                source_finding=source,
                target_finding=objective,
                loaders=loaders,
            )
            for source, objective in zip(sources, targets, strict=False)
            if source and objective
        ],
    )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
