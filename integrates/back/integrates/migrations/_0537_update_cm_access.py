"""
This migration takes users with the CM role within the organization and
corrects the role to CM for the groups in the organization, or adds the user
to the groups they were not included in.

Execution Time:    2024-05-20 at 20:35:41 UTC
Finalization Time: 2024-05-20 at 20:36:45 UTC

Execution Time:    2024-05-22 at 18:05:21 UTC
Finalization Time: 2024-05-22 at 18:06:00 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    run,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.group_access.types import (
    GroupAccess,
    GroupStakeholdersAccessRequest,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.group_access import (
    domain as group_access_domain,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")
EMAIL_INTEGRATES = "integrates@fluidattacks.com"


async def process_stakeholder(
    stakeholder: str,
    group: str,
    stakeholder_access: GroupAccess | None,
) -> None:
    if not stakeholder_access or (
        stakeholder_access is not None
        and stakeholder_access.state.invitation
        and (
            not stakeholder_access.state.invitation.is_used
            or stakeholder_access.state.invitation.role != "customer_manager"
        )
    ):
        try:
            await group_access_domain.remove_access(get_new_context(), stakeholder, group)
            await group_access_domain.add_access(
                get_new_context(),
                stakeholder,
                group,
                "customer_manager",
                stakeholder_access.state.modified_by
                if stakeholder_access and stakeholder_access.state.modified_by
                else EMAIL_INTEGRATES,
            )
            LOGGER.info(
                "Stakeholder Fixed",
                extra={
                    "extra": {
                        "user": stakeholder,
                        "group_name": group,
                    },
                },
            )
        except Exception as error:
            logging.error(error)
            LOGGER.info(
                "Stakeholder could not be fixed",
                extra={
                    "extra": {
                        "user": stakeholder,
                        "group_name": group,
                    },
                },
            )


async def process_group(
    loaders: Dataloaders,
    group: Group,
    org_stakeholders: list[str],
) -> None:
    stakeholders_access = {
        access.email: access
        for access in await loaders.group_stakeholders_access.load(
            GroupStakeholdersAccessRequest(group_name=group.name),
        )
    }

    for stakeholder in org_stakeholders:
        await process_stakeholder(
            stakeholder,
            group.name,
            stakeholders_access.get(stakeholder),
        )

    LOGGER.info(
        "Group processed",
        extra={
            "extra": {
                "group_name": group.name,
                "organization_id": group.organization_id,
            },
        },
    )


async def process_organization(
    loaders: Dataloaders,
    organization_id: str,
) -> None:
    stakeholders_access = await loaders.organization_stakeholders_access.load(organization_id)

    organization_groups = await loaders.organization_groups.load(organization_id)

    cm_organization_stakeholders = [
        access.email for access in stakeholders_access if access.state.role == "customer_manager"
    ]
    for count, group in enumerate(organization_groups, start=1):
        LOGGER_CONSOLE.info(
            "Group to process",
            extra={
                "extra": {
                    "group": group.name,
                    "organization_id": organization_id,
                    "count": count,
                },
            },
        )
        await process_group(loaders, group, cm_organization_stakeholders)


async def main() -> None:
    loaders: Dataloaders = get_new_context()

    organization_ids = [
        organization.id async for organization in orgs_domain.iterate_organizations()
    ]

    for count, organization_id in enumerate(organization_ids, start=1):
        LOGGER_CONSOLE.info(
            "Organization to process",
            extra={
                "extra": {
                    "organization_id": organization_id,
                    "count": count,
                },
            },
        )
        await process_organization(loaders, organization_id)


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
