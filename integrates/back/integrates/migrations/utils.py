import functools
import logging
import time
from asyncio import iscoroutinefunction, run
from collections.abc import Callable
from logging import Logger
from typing import ParamSpec, TypeVar

from integrates.dataloaders import Dataloaders
from integrates.organizations import domain as orgs_domain
from integrates.settings import LOGGING

logging.config.dictConfig(LOGGING)

P = ParamSpec("P")
T = TypeVar("T")


def log_time(logger: Logger) -> Callable[[Callable[P, T]], Callable[P, T]]:
    """Decorator to add execution and finalization time in logs"""

    def inner(_func: Callable[P, T]) -> Callable[P, T]:
        @functools.wraps(_func)
        def wrapper(*args: P.args, **kwargs: P.kwargs) -> T:
            start = time.strftime("%Y-%m-%d at %H:%M:%S %Z")

            result: T = (
                run(_func(*args, **kwargs))
                if iscoroutinefunction(_func)
                else _func(*args, **kwargs)
            )

            end = time.strftime("%Y-%m-%d at %H:%M:%S %Z")
            logger.info("Execution Time: %s", start)
            logger.info("Finalization Time: %s", end)

            return result

        return wrapper

    return inner


async def get_all_group_names(loaders: Dataloaders) -> list[str]:
    return sorted(await orgs_domain.get_all_group_names(loaders=loaders))
