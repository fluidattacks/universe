# type: ignore

"""
Populate state field in environments secrets

Start Time: 2024-03-08 at 19:06:14 UTC
Finalization Time: 2024-03-08 at 19:07:31 UTC
"""

import logging
import logging.config
import time
from datetime import (
    datetime,
)
from operator import (
    attrgetter,
)
from typing import (
    NamedTuple,
)

from aioextensions import (
    collect,
    run,
)
from boto3.dynamodb.conditions import (
    Attr,
)

from integrates.class_types.types import (
    Item,
)
from integrates.custom_utils.datetime import (
    get_iso_date,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.group_access.types import (
    GroupAccess,
    GroupStakeholdersAccessRequest,
)
from integrates.db_model.roots.types import (
    Root,
    RootEnvironmentSecretsRequest,
    RootEnvironmentUrl,
    Secret,
)
from integrates.db_model.stakeholders.types import (
    Stakeholder,
)
from integrates.db_model.utils import (
    get_as_utc_iso_format,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")
EMAIL_INTEGRATES = "integrates@fluidattacks.com"


class StakeHolder(NamedTuple):
    email: str
    last_login: datetime | None
    group: str


async def _get_root_environment_urls(loaders: Dataloaders, root: Root) -> list[RootEnvironmentUrl]:
    return await loaders.root_environment_urls.load(root.id)


def _get_owner(
    old_secret: Secret,
    environment_created_by: str | None,
    user_manager: str | None,
) -> str | None:
    old_secret_owner = old_secret.state.owner if old_secret.state else None
    return old_secret_owner or environment_created_by or user_manager


def _get_modified_by(old_secret: Secret) -> str | None:
    if old_secret.state:
        return (
            EMAIL_INTEGRATES
            if (old_secret.state.owner is None or old_secret.state.description is None)
            else old_secret.state.modified_by
        )
    return EMAIL_INTEGRATES


def _get_modified_date(old_secret: Secret) -> str | None:
    if old_secret.state:
        modified_date = (
            get_as_utc_iso_format(old_secret.state.modified_date)
            if old_secret.state.modified_date
            else None
        )
        return (
            get_iso_date()
            if (old_secret.state.owner is None or old_secret.state.description is None)
            else modified_date
        )
    return get_iso_date()


def _format_state(
    old_secret: Secret,
    environment: RootEnvironmentUrl,
    user_manager: str | None,
) -> Item:
    return {
        "owner": _get_owner(old_secret, environment.created_by, user_manager),
        "description": old_secret.description,
        "modified_by": _get_modified_by(old_secret),
        "modified_date": _get_modified_date(old_secret),
    }


async def _get_user_manager(loaders: Dataloaders, group_name: str) -> str | None:
    stakeholders_access: list[GroupAccess] = await loaders.group_stakeholders_access.load(
        GroupStakeholdersAccessRequest(group_name=group_name),
    )
    stakeholder_list: list[StakeHolder] = []
    for access in stakeholders_access:
        if access.state.role == "user_manager" and (email := access.email):
            stake_holder: Stakeholder | None = await loaders.stakeholder.load(email)
            if stake_holder:
                stakeholder_list.append(
                    StakeHolder(
                        email=email,
                        last_login=stake_holder.last_login_date,
                        group=access.group_name,
                    ),
                )

    if stakeholder_list:
        stakeholder_list_filtered = [
            stakeholder for stakeholder in stakeholder_list if stakeholder.last_login is not None
        ]
        return (
            max(stakeholder_list_filtered, key=attrgetter("last_login")).email
            if stakeholder_list_filtered
            else EMAIL_INTEGRATES
        )
    return EMAIL_INTEGRATES


async def process_environment_secret(
    environment: RootEnvironmentUrl,
    old_secret: Secret,
    group_name: str,
    user_manager: str | None,
) -> None:
    key_structure = TABLE.primary_key
    secret_key = keys.build_key(
        facet=TABLE.facets["root_environment_secret"],
        values={
            "group_name": group_name,
            "hash": environment.id,
            "key": old_secret.key,
        },
    )
    secret_item = {
        "key": old_secret.key,
        "value": old_secret.value,
        "description": old_secret.description,
        "created_at": get_as_utc_iso_format(old_secret.created_at)
        if old_secret.created_at
        else None,
        "state": _format_state(
            old_secret=old_secret,
            environment=environment,
            user_manager=user_manager,
        ),
    }
    await operations.update_item(
        condition_expression=Attr(key_structure.partition_key).exists(),
        item=secret_item,
        key=secret_key,
        table=TABLE,
    )


async def process_environment(
    loaders: Dataloaders,
    environment: RootEnvironmentUrl,
    group_name: str,
    user_manager: str | None,
) -> None:
    environment_secrets = await loaders.environment_secrets.load(
        RootEnvironmentSecretsRequest(url_id=environment.id, group_name=group_name),
    )
    await collect(
        tuple(
            process_environment_secret(environment, environment_secret, group_name, user_manager)
            for environment_secret in environment_secrets
        ),
        workers=32,
    )


async def process_group(
    loaders: Dataloaders,
    group_name: str,
) -> None:
    group_roots = await loaders.group_roots.load(group_name)
    root_and_environments = await collect(
        tuple(_get_root_environment_urls(loaders, root) for root in group_roots),
        workers=32,
    )
    user_manager = await _get_user_manager(loaders=loaders, group_name=group_name)
    await collect(
        tuple(
            process_environment(loaders, environment, group_name, user_manager)
            for environments in root_and_environments
            for environment in environments
        ),
        workers=32,
    )
    LOGGER_CONSOLE.info("Group processed  %s", group_name)


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    all_group_names = sorted(await orgs_domain.get_all_group_names(loaders=loaders))
    count = 0
    LOGGER_CONSOLE.info("all_group_names %s", len(all_group_names))
    for group_name in all_group_names:
        count += 1
        LOGGER_CONSOLE.info("group %s %s", group_name, count)
        await process_group(loaders, group_name)


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:    %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("%s", execution_time)
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
