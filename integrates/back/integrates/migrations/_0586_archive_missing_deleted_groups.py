"""
Archive in Redshift the data related to some missing DELETED groups.
Data is taken from restored backup "integrates_vms" tables.

Facets to process:
events
findings
groups
organizations
roots
toe_inputs
toe_lines
vulnerabilities

"""

import hashlib
import logging
import logging.config
import time
from datetime import (
    datetime,
    timedelta,
)

from aioextensions import (
    collect,
    run,
)
from boto3.dynamodb.conditions import (
    Key,
)

from integrates.class_types.types import (
    Item,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model import (
    TABLE as INTEGRATES_VMS_TABLE,
)
from integrates.db_model.organizations.utils import (
    remove_org_id_prefix,
)
from integrates.db_model.utils import (
    get_as_utc_iso_format,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.types import (
    Facet,
    Index,
    PrimaryKey,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger("console")
BACKUP_TABLE_NAME = "vms_backup_aug_01"
BACKUP_TABLE = INTEGRATES_VMS_TABLE._replace(name=BACKUP_TABLE_NAME)
INVERTED_INDEX = BACKUP_TABLE.indexes["inverted_index"]
EMAIL_INTEGRATES = "integrates@fluidattacks.com"
GROUP_NAMES: list[str] = []  # MASKED


def encode_for_archive(raw: str) -> str:
    return hashlib.shake_256(raw.encode("utf-8")).hexdigest(10)


async def restore_and_delete_items(items: tuple[Item, ...]) -> None:
    if not items:
        return

    await operations.batch_put_item(
        items=items,
        table=INTEGRATES_VMS_TABLE,
    )
    await operations.batch_delete_item(
        keys=tuple(PrimaryKey(partition_key=item["pk"], sort_key=item["sk"]) for item in items),
        table=INTEGRATES_VMS_TABLE,
    )


async def run_query_on_backup(
    primary_key: PrimaryKey,
    facets: tuple[Facet, ...],
    index: Index | None = None,
) -> tuple[Item, ...]:
    if index:
        key_structure = index.primary_key
        condition_expression = Key(key_structure.partition_key).eq(primary_key.sort_key) & Key(
            key_structure.sort_key,
        ).begins_with(primary_key.partition_key)
    else:
        key_structure = BACKUP_TABLE.primary_key
        condition_expression = Key(key_structure.partition_key).eq(primary_key.partition_key) & Key(
            key_structure.sort_key,
        ).begins_with(primary_key.sort_key)

    response = await operations.query(
        condition_expression=condition_expression,
        facets=facets,
        index=index,
        table=BACKUP_TABLE,
    )

    return response.items


async def _process_group_events(group_name: str) -> None:
    items = await run_query_on_backup(
        primary_key=keys.build_key(
            facet=BACKUP_TABLE.facets["event_metadata"],
            values={"name": group_name},
        ),
        facets=(BACKUP_TABLE.facets["event_metadata"],),
        index=INVERTED_INDEX,
    )
    await restore_and_delete_items(items)
    LOGGER.info("Processed events: %s", group_name)


def _get_vuln_masked_state_item(vuln_id: str, raw: Item) -> Item | None:
    if raw["status"] != "DELETED":
        return None

    key_structure = BACKUP_TABLE.primary_key
    modified_date_str = get_as_utc_iso_format(
        datetime.fromisoformat(raw["modified_date"]) + timedelta(seconds=1),
    )
    primary_key = keys.build_key(
        facet=BACKUP_TABLE.facets["vulnerability_historic_state"],
        values={
            "id": vuln_id,
            "iso8601utc": modified_date_str,
        },
    )

    return {
        **raw,
        key_structure.partition_key: primary_key.partition_key,
        key_structure.sort_key: primary_key.sort_key,
        "modified_date": modified_date_str,
        "modified_by": EMAIL_INTEGRATES,
        "status": "MASKED",
    }


async def _process_vuln_historic_state(vuln_id: str, masked_state: Item | None) -> None:
    items = await run_query_on_backup(
        primary_key=keys.build_key(
            facet=BACKUP_TABLE.facets["vulnerability_historic_state"],
            values={"id": vuln_id},
        ),
        facets=(BACKUP_TABLE.facets["vulnerability_historic_state"],),
    )
    if masked_state:
        items = (*items, masked_state)

    await restore_and_delete_items(items)
    LOGGER.info("Processed vulnerability historic state: %s", vuln_id)


async def _process_vuln_historic_treatment(vuln_id: str) -> None:
    items = await run_query_on_backup(
        primary_key=keys.build_key(
            facet=BACKUP_TABLE.facets["vulnerability_historic_treatment"],
            values={"id": vuln_id},
        ),
        facets=(BACKUP_TABLE.facets["vulnerability_historic_treatment"],),
    )
    await restore_and_delete_items(items)
    LOGGER.info("Processed vulnerability historic treatment: %s", vuln_id)


async def _process_vuln_historic_verification(vuln_id: str) -> None:
    items = await run_query_on_backup(
        primary_key=keys.build_key(
            facet=BACKUP_TABLE.facets["vulnerability_historic_verification"],
            values={"id": vuln_id},
        ),
        facets=(BACKUP_TABLE.facets["vulnerability_historic_verification"],),
    )
    await restore_and_delete_items(items)
    LOGGER.info("Processed vulnerability historic verification: %s", vuln_id)


async def _process_vuln_historic_zero_risk(vuln_id: str) -> None:
    items = await run_query_on_backup(
        primary_key=keys.build_key(
            facet=BACKUP_TABLE.facets["vulnerability_historic_zero_risk"],
            values={"id": vuln_id},
        ),
        facets=(BACKUP_TABLE.facets["vulnerability_historic_zero_risk"],),
    )
    await restore_and_delete_items(items)
    LOGGER.info("Processed vulnerability historic zero risk: %s", vuln_id)


async def _process_vuln(metadata_item: Item) -> None:
    vuln_id = str(metadata_item["pk"]).split("#")[1]
    if masked_state := _get_vuln_masked_state_item(vuln_id=vuln_id, raw=metadata_item["state"]):
        metadata_item["state"] = masked_state

    # Process metadata ahead of historics
    await restore_and_delete_items((metadata_item,))
    await collect(
        [
            _process_vuln_historic_state(vuln_id, masked_state),
            _process_vuln_historic_treatment(vuln_id),
            _process_vuln_historic_verification(vuln_id),
            _process_vuln_historic_zero_risk(vuln_id),
        ],
        workers=4,
    )


async def _process_finding_vulns(finding_id: str) -> None:
    items = await run_query_on_backup(
        primary_key=keys.build_key(
            facet=BACKUP_TABLE.facets["vulnerability_metadata"],
            values={"finding_id": finding_id},
        ),
        facets=(BACKUP_TABLE.facets["vulnerability_metadata"],),
        index=INVERTED_INDEX,
    )
    await collect(
        [_process_vuln(metadata_item) for metadata_item in items],
        workers=4,
    )
    LOGGER.info("Processed finding vulnerabilities: %s", finding_id)


def _get_finding_masked_state_item(finding_id: str, raw: Item) -> Item | None:
    if raw["status"] != "DELETED":
        return None

    key_structure = BACKUP_TABLE.primary_key
    modified_date_str = get_as_utc_iso_format(
        datetime.fromisoformat(raw["modified_date"]) + timedelta(seconds=1),
    )
    primary_key = keys.build_key(
        facet=BACKUP_TABLE.facets["finding_historic_state"],
        values={
            "id": finding_id,
            "iso8601utc": modified_date_str,
        },
    )

    return {
        **raw,
        key_structure.partition_key: primary_key.partition_key,
        key_structure.sort_key: primary_key.sort_key,
        "modified_date": modified_date_str,
        "modified_by": EMAIL_INTEGRATES,
        "status": "MASKED",
    }


async def _process_finding_historic_state(finding_id: str, masked_state: Item | None) -> None:
    items = await run_query_on_backup(
        primary_key=keys.build_key(
            facet=BACKUP_TABLE.facets["finding_historic_state"],
            values={"id": finding_id},
        ),
        facets=(BACKUP_TABLE.facets["finding_historic_state"],),
    )
    if masked_state:
        items = (*items, masked_state)

    await restore_and_delete_items(items)
    LOGGER.info("Processed finding historic state: %s", finding_id)


async def _process_finding_historic_verification(finding_id: str) -> None:
    items = await run_query_on_backup(
        primary_key=keys.build_key(
            facet=BACKUP_TABLE.facets["finding_historic_verification"],
            values={"id": finding_id},
        ),
        facets=(BACKUP_TABLE.facets["finding_historic_verification"],),
    )
    await restore_and_delete_items(items)
    LOGGER.info("Processed finding historic verification: %s", finding_id)


async def _process_finding(metadata_item: Item) -> None:
    finding_id = str(metadata_item["pk"]).split("#")[1]
    if masked_state := _get_finding_masked_state_item(
        finding_id=finding_id,
        raw=metadata_item["state"],
    ):
        metadata_item["state"] = masked_state

    # Process metadata ahead of historics
    await restore_and_delete_items((metadata_item,))
    await _process_finding_historic_state(finding_id, masked_state)
    await _process_finding_historic_verification(finding_id)


async def _process_group_findings(group_name: str) -> None:
    items = await run_query_on_backup(
        primary_key=keys.build_key(
            facet=BACKUP_TABLE.facets["finding_metadata"],
            values={"group_name": group_name},
        ),
        facets=(BACKUP_TABLE.facets["finding_metadata"],),
        index=INVERTED_INDEX,
    )
    # Process individual vulns prior to finding metadata
    await collect(
        [_process_finding_vulns(str(metadata_item["pk"]).split("#")[1]) for metadata_item in items],
        workers=4,
    )
    await collect(
        [_process_finding(metadata_item) for metadata_item in items],
        workers=16,
    )
    LOGGER.info("Processed findings: %s", group_name)


async def _process_group_roots(group_name: str) -> None:
    items = await run_query_on_backup(
        primary_key=keys.build_key(
            facet=BACKUP_TABLE.facets["git_root_metadata"],
            values={"name": group_name},
        ),
        facets=(
            BACKUP_TABLE.facets["git_root_metadata"],
            BACKUP_TABLE.facets["ip_root_metadata"],
            BACKUP_TABLE.facets["url_root_metadata"],
        ),
        index=INVERTED_INDEX,
    )
    await restore_and_delete_items(items)
    LOGGER.info("Processed roots: %s", group_name)


async def _process_organization_historic_state(org_id: str) -> None:
    items = await run_query_on_backup(
        primary_key=keys.build_key(
            facet=BACKUP_TABLE.facets["organization_historic_state"],
            values={"id": remove_org_id_prefix(org_id)},
        ),
        facets=(BACKUP_TABLE.facets["organization_historic_state"],),
    )
    await restore_and_delete_items(items)
    LOGGER.info("Processed organization historic state: %s", org_id)


async def _process_organization(org_id: str) -> None:
    if await get_new_context().organization.load(org_id):
        LOGGER.info("Organization still present on main table: %s", org_id)
        return

    items = await run_query_on_backup(
        primary_key=keys.build_key(
            facet=BACKUP_TABLE.facets["organization_metadata"],
            values={"id": remove_org_id_prefix(org_id)},
        ),
        facets=(BACKUP_TABLE.facets["organization_metadata"],),
    )
    if not items:
        LOGGER.warning("Organization %s NOT found in backup table", org_id)

        return

    # Process metadata ahead of historics
    await restore_and_delete_items(items)
    await _process_organization_historic_state(org_id)
    LOGGER.info("Processed organization: %s", org_id)


async def _process_group_toe_inputs(group_name: str) -> None:
    primary_key = keys.build_key(
        facet=BACKUP_TABLE.facets["toe_input_metadata"],
        values={"group_name": group_name},
    )
    key_structure = BACKUP_TABLE.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(
                primary_key.sort_key.replace("#COMPONENT#ENTRYPOINT", ""),
            )
        ),
        facets=(BACKUP_TABLE.facets["toe_input_metadata"],),
        table=BACKUP_TABLE,
    )
    if not response.items:
        return

    await restore_and_delete_items(response.items)

    LOGGER.info("Processed ToE inputs: %s", group_name)


async def _process_group_toe_lines(group_name: str) -> None:
    primary_key = keys.build_key(
        facet=BACKUP_TABLE.facets["toe_lines_metadata"],
        values={"group_name": group_name},
    )
    key_structure = BACKUP_TABLE.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key.replace("#FILENAME", ""))
        ),
        facets=(BACKUP_TABLE.facets["toe_lines_metadata"],),
        table=BACKUP_TABLE,
    )
    if not response.items:
        return

    await restore_and_delete_items(response.items)

    LOGGER.info("Processed ToE lines: %s", group_name)


def _get_group_deleted_state_item(group_name: str, raw: Item) -> Item | None:
    if raw["status"] == "DELETED":
        return None

    key_structure = BACKUP_TABLE.primary_key
    modified_date_str = get_as_utc_iso_format(
        datetime.fromisoformat(raw["modified_date"]) + timedelta(seconds=1),
    )
    primary_key = keys.build_key(
        facet=BACKUP_TABLE.facets["group_historic_state"],
        values={
            "name": group_name,
            "iso8601utc": modified_date_str,
        },
    )

    return {
        **raw,
        key_structure.partition_key: primary_key.partition_key,
        key_structure.sort_key: primary_key.sort_key,
        "comments": "Added for data consistency",
        "has_essential": False,
        "has_advanced": False,
        "has_machine": False,
        "has_squad": False,
        "justification": "MIGRATION",
        "modified_date": modified_date_str,
        "modified_by": EMAIL_INTEGRATES,
        "status": "DELETED",
    }


async def _process_group_historic_state(group_name: str, deleted_state: Item | None) -> None:
    items = await run_query_on_backup(
        primary_key=keys.build_key(
            facet=BACKUP_TABLE.facets["group_historic_state"],
            values={"name": group_name},
        ),
        facets=(BACKUP_TABLE.facets["group_historic_state"],),
    )
    if deleted_state:
        items = (*items, deleted_state)

    await restore_and_delete_items(items)
    LOGGER.info("Processed group historic state: %s", group_name)


async def _process_group_unreliable_indicators(group_name: str) -> None:
    items = await run_query_on_backup(
        primary_key=keys.build_key(
            facet=BACKUP_TABLE.facets["group_unreliable_indicators"],
            values={"name": group_name},
        ),
        facets=(BACKUP_TABLE.facets["group_unreliable_indicators"],),
    )
    await restore_and_delete_items(items)
    LOGGER.info("Processed group unreliable indicators: %s", group_name)


async def _process_group(group_name: str) -> None:
    LOGGER.info("Processing group: %s, %s", group_name, encode_for_archive(group_name))
    items = await run_query_on_backup(
        primary_key=keys.build_key(
            facet=BACKUP_TABLE.facets["group_metadata"],
            values={"name": group_name},
        ),
        facets=(BACKUP_TABLE.facets["group_metadata"],),
    )
    if not items:
        LOGGER.warning("Group %s NOT found in backup table", group_name)

        return

    await collect(
        [
            _process_group_findings(group_name),
            _process_group_events(group_name),
            _process_group_roots(group_name),
            _process_organization(items[0]["sk"]),
            _process_group_toe_inputs(group_name),
            _process_group_toe_lines(group_name),
        ],
        workers=2,
    )

    # Process metadata ahead of historics
    metadata_item = items[0]
    if deleted_state := _get_group_deleted_state_item(
        group_name=group_name,
        raw=metadata_item["state"],
    ):
        metadata_item["state"] = deleted_state

    await restore_and_delete_items((metadata_item,))
    await collect(
        [
            _process_group_historic_state(group_name, deleted_state),
            _process_group_unreliable_indicators(group_name),
        ],
        workers=2,
    )
    LOGGER.info("Processed group: %s", group_name)


async def main() -> None:
    for group_name in GROUP_NAMES:
        await _process_group(group_name=group_name)


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER.info("\n%s\n%s", execution_time, finalization_time)
