# type: ignore
"""
Execution Time:    2024-06-18 at 20:50:37 UTC
Finalization Time: 2024-06-18 at 20:50:39 UTC
"""

import logging
import logging.config
import time
import uuid

from aioextensions import (
    collect,
    run,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    credentials as credentials_model,
)
from integrates.db_model.credentials.types import (
    CredentialsRequest,
    CredentialType,
)
from integrates.db_model.roots.types import (
    GitRoot,
    Root,
)
from integrates.db_model.roots.update import (
    update_root_state,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")


async def update_git_root_state(credential_id: str, root: Root) -> None:
    new_state = root.state._replace(credential_id=credential_id)

    try:
        await update_root_state(
            current_value=root.state,
            group_name=root.group_name,
            root_id=root.id,
            state=new_state,
        )
    except Exception as ex:
        logging.error(ex)

    LOGGER.info(
        "Root state updated",
        extra={
            "extra": {
                "root": root.id,
            },
        },
    )


async def update_credential(
    loaders: Dataloaders,
    credential_id: str,
    organization_id: str,
    new_organization_id: str,
) -> str | None:
    credential = await loaders.credentials.load(
        CredentialsRequest(
            id=credential_id,
            organization_id=organization_id,
        ),
    )

    if not credential or credential.state.type not in [
        CredentialType.SSH,
        CredentialType.HTTPS,
    ]:
        return None

    new_credential_id = str(uuid.uuid4())
    credential = credential._replace(id=new_credential_id, organization_id=new_organization_id)

    try:
        await credentials_model.add(credential=credential)
        return new_credential_id
    except Exception as ex:
        logging.error(ex)

    LOGGER.info(
        "Credential updated",
        extra={
            "extra": {
                "credential": new_credential_id,
            },
        },
    )


async def update_roots(
    loaders: Dataloaders,
    roots: list[Root],
    credential_id: str,
    organization_id: str,
    new_organization_id: str,
) -> None:
    new_credential_id = await update_credential(
        loaders,
        credential_id,
        organization_id,
        new_organization_id,
    )

    if new_credential_id:
        await collect(
            tuple(update_git_root_state(new_credential_id, root) for root in roots),
            workers=8,
        )


async def process_roots(
    loaders: Dataloaders,
    group_name: str,
    organization_id: str,
    new_organization_id: str,
) -> None:
    roots = await loaders.group_roots.load(group_name)
    if not roots:
        LOGGER.info(
            "No roots to processed",
            extra={
                "extra": {
                    "group_name": group_name,
                },
            },
        )
        return

    grouped_roots: dict[str, list[Root]] = {}
    for root in roots:
        if isinstance(root, GitRoot):
            credential_id = root.state.credential_id
            if credential_id is not None:
                if credential_id not in grouped_roots:
                    grouped_roots[credential_id] = []
                grouped_roots[credential_id].append(root)

    await collect(
        tuple(
            update_roots(
                loaders=loaders,
                roots=roots,
                credential_id=credential_id,
                organization_id=organization_id,
                new_organization_id=new_organization_id,
            )
            for credential_id, roots in grouped_roots.items()
        ),
        workers=8,
    )

    LOGGER.info(
        "Roots processed",
        extra={
            "extra": {
                "group_name": group_name,
                "roots": len(roots),
            },
        },
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()

    await process_roots(
        loaders,
        "unittesting",
        "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
        "ORG#c6cecc0e-bb92-4079-8b6d-c4e815c10bb1",
    )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
