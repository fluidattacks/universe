import logging

from aioextensions import collect
from boto3.dynamodb.conditions import Key

from integrates.dataloaders import get_new_context
from integrates.db_model import TABLE
from integrates.dynamodb import operations
from integrates.dynamodb.types import PrimaryKey
from integrates.migrations.utils import get_all_group_names, log_time

LOGGER = logging.getLogger("migrations")


async def _get_group_roots(group_name: str) -> tuple[list[dict], list[dict]]:
    index = TABLE.indexes["inverted_index"]
    key_structure = index.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(f"GROUP#{group_name}")
            & Key(key_structure.sort_key).begins_with("ROOT#")
        ),
        facets=(TABLE.facets["git_root_metadata"],),
        index=index,
        table=TABLE,
    )
    uniqueness_keys = [item for item in response.items if item["pk"].startswith("ROOT#URL#")]
    roots = [item for item in response.items if not item["pk"].startswith("ROOT#URL#")]

    return uniqueness_keys, roots


def _get_orphans(uniqueness_keys: list[dict], roots: list[dict]) -> list[dict]:
    return [
        key
        for key in uniqueness_keys
        if (root_id := key["pk"].removeprefix("ROOT#"))
        and not any(root["sk_3"].endswith(root_id) for root in roots)
    ]


async def _remove_orphans(group_name: str, orphans: list[dict]) -> None:
    LOGGER.info("[%s] Orphan keys to delete: %s", group_name, len(orphans))
    LOGGER.info("[%s] Orphan keys: %s", group_name, orphans)

    await operations.batch_delete_item(
        keys=tuple(PrimaryKey(partition_key=item["pk"], sort_key=item["sk"]) for item in orphans),
        table=TABLE,
    )


async def process(group_name: str) -> None:
    uniqueness_keys, roots = await _get_group_roots(group_name)

    orphan_keys = _get_orphans(uniqueness_keys, roots)

    if len(orphan_keys) > 0:
        await _remove_orphans(group_name, orphan_keys)
    else:
        LOGGER.info("[%s] No orphan keys detected", group_name)


@log_time(LOGGER)
async def main() -> None:
    loaders = get_new_context()
    groups = await get_all_group_names(loaders=loaders)

    await collect(
        [process(group) for group in groups],
        workers=8,
    )


if __name__ == "__main__":
    main()  # type: ignore[unused-coroutine]
