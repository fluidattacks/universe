# type: ignore
"""
Fix CVSS 3.1 severity metrics for all machine findings. In particular, the
Exploit Code Maturity (E) or "exploitability" metric that had an error as
identified in commit f2ae0d4d20313eea5f998ee1984d2817bfa4dd87.

Execution Time:    2023-03-31 at 15:46:28 UTC
Finalization Time: 2023-03-31 at 15:53:01 UTC
"""

import logging
import logging.config
import time
from decimal import (
    Decimal,
)

from aioextensions import (
    collect,
    run,
)

from integrates.custom_utils import (
    cvss as cvss_utils,
)
from integrates.custom_utils.criteria import (
    CRITERIA_VULNERABILITIES,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    findings as findings_model,
)
from integrates.db_model.enums import (
    Source,
)
from integrates.db_model.findings.enums import (
    Exploitability,
)
from integrates.db_model.findings.types import (
    Finding,
    FindingMetadataToUpdate,
)
from integrates.organizations.domain import (
    get_all_group_names,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


async def process_finding(
    finding: Finding,
) -> None:
    finding_code = finding.title.split(". ")[0].strip()
    criteria_vulnerability = CRITERIA_VULNERABILITIES[finding_code]

    finding_severity = finding.severity
    if criteria_vulnerability["score"]["temporal"][
        "exploit_code_maturity"
    ] == "X" and finding.severity.exploitability == Decimal("0.94"):
        finding_severity = finding_severity._replace(exploitability=Exploitability.X.value)

    if finding.severity_score != cvss_utils.get_severity_score_summary(finding_severity):
        await findings_model.update_metadata(
            group_name=finding.group_name,
            finding_id=finding.id,
            metadata=FindingMetadataToUpdate(
                severity=finding_severity if finding_severity != finding.severity else None,
                severity_score=cvss_utils.get_severity_score_summary(finding_severity),
            ),
        )


async def process_group(
    loaders: Dataloaders,
    group_name: str,
    progress: float,
) -> None:
    group_findings = await loaders.group_drafts_and_findings_all.load(group_name)
    machine_findings = [
        finding
        for finding in group_findings
        if finding.creation and finding.creation.source == Source.MACHINE
    ]
    if not machine_findings:
        return

    await collect(
        tuple(
            process_finding(
                finding=finding,
            )
            for finding in machine_findings
        ),
        workers=16,
    )
    LOGGER_CONSOLE.info("Group processed %s %s", group_name, f"{round(progress, 2)!s}")


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    group_names = sorted(await get_all_group_names(loaders))
    LOGGER_CONSOLE.info("%s", f"{group_names=}")
    LOGGER_CONSOLE.info("%s", f"{len(group_names)=}")
    await collect(
        tuple(
            process_group(
                loaders=loaders,
                group_name=group,
                progress=count / len(group_names),
            )
            for count, group in enumerate(group_names)
        ),
        workers=1,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
