"""
Remove duplicates from open machine reports
Start Time: 2024-02-22 at 17:36:27 UTC
Finalization Time: 2024-02-22 at 22:50:08 UTC

Execution Time:    2024-02-29 at 16:35:41 UTC
Finalization Time: 2024-02-29 at 16:52:57 UTC

Execution Time:    2024-04-10 at 16:00:41 UTC
Finalization Time: 2024-04-10 at 16:52:57 UTC

"""

import csv
import time

from aioextensions import (
    collect,
    run,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.enums import (
    Source,
    TreatmentStatus,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateReason,
    VulnerabilityStateStatus,
)
from integrates.db_model.vulnerabilities.types import (
    FindingVulnerabilitiesZrRequest,
    Vulnerability,
)
from integrates.organizations.domain import (
    get_all_active_group_names,
)
from integrates.vulnerabilities.domain import (
    remove_vulnerability,
)


async def process_finding_vulns(loaders: Dataloaders, finding_id: str) -> None:
    connection = await loaders.finding_vulnerabilities_released_nzr_c.load(
        FindingVulnerabilitiesZrRequest(
            finding_id=finding_id,
            paginate=False,
            state_status=VulnerabilityStateStatus.VULNERABLE,
        ),
    )
    finding_open_vulns = [edge.node for edge in connection.edges]
    vulns = [
        vuln
        for vuln in finding_open_vulns
        if (vuln.hacker_email == "machine@fluidattacks.com" or vuln.state.source == Source.MACHINE)
        and vuln.state.status == VulnerabilityStateStatus.VULNERABLE
    ]

    duplicates: dict[str, list[Vulnerability]] = {}
    for vuln in vulns:
        if not vuln.hash:
            continue
        vuln_hash = f"{vuln.root_id}/{vuln.hash}"
        if vuln_hash not in duplicates:
            duplicates[vuln_hash] = [vuln]
        else:
            duplicates[vuln_hash].append(vuln)

    duplicates_csv = []
    for vuln_hash, vulns in duplicates.items():
        if (len(vulns)) < 2:
            continue
        vulns = [
            *sorted(
                [
                    x
                    for x in vulns
                    if x.treatment and x.treatment.status != TreatmentStatus.UNTREATED
                ],
                key=lambda x: x.state.modified_date,
                reverse=True,
            ),
            *sorted(
                [
                    x
                    for x in vulns
                    if not x.treatment or x.treatment.status == TreatmentStatus.UNTREATED
                ],
                key=lambda x: x.state.modified_date,
            ),
        ]
        keep_vuln = vulns[0]
        vulns_to_delete = vulns[1:]

        if len(vulns_to_delete) > 0:
            await collect(
                [
                    remove_vulnerability(
                        loaders=loaders,
                        finding_id=vuln.finding_id,
                        vulnerability_id=vuln.id,
                        justification=VulnerabilityStateReason.DUPLICATED,
                        email="flagos@fluidattacks.com",
                        include_closed_vuln=True,
                    )
                    for vuln in vulns_to_delete
                ],
                workers=32,
            )

            duplicates_csv.append(
                [
                    keep_vuln.group_name,
                    keep_vuln.root_id,
                    keep_vuln.finding_id,
                    keep_vuln.id,
                    keep_vuln.state.where,
                    keep_vuln.state.specific,
                    "KEEP",
                    keep_vuln.state.status.value,
                ],
            )
            duplicates_csv.extend(
                [
                    [
                        vuln.group_name,
                        vuln.root_id,
                        vuln.finding_id,
                        vuln.id,
                        vuln.state.where,
                        vuln.state.specific,
                        "DELETE",
                        vuln.state.status.value,
                    ]
                    for vuln in vulns_to_delete
                ],
            )

    with open("duplicates_machine.csv", "a+", encoding="utf-8") as handler:
        writer = csv.writer(handler)
        writer.writerows(duplicates_csv)


async def process_group_findings(loaders: Dataloaders, group: str) -> None:
    print(f"Processing group {group}")
    findings = await loaders.group_findings.load(group)
    machine_findings = [
        finding
        for finding in findings
        if finding.creation and finding.creation.modified_by == "machine@fluidattacks.com"
    ]

    for finding in machine_findings:
        await process_finding_vulns(loaders, finding.id)


async def main() -> None:
    loaders = get_new_context()
    group_names = sorted(await get_all_active_group_names(loaders))

    for group in group_names:
        await process_group_findings(loaders, group)


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    print(f"{execution_time}\n{finalization_time}")
