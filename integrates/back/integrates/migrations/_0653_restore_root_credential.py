"""
For the given groups, restore the credential_id on
roots with missing credentials after a state update

Execution Time: 2025-01-24 at 16:12:20 UTC, extra=None
Finalization Time: 2025-01-24 at 16:23:18 UTC, extra=None
"""

import logging
import logging.config

from aioextensions import collect

from integrates.custom_utils import datetime as datetime_utils
from integrates.custom_utils import roots as roots_utils
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model import roots as roots_model
from integrates.db_model.credentials.types import CredentialsRequest
from integrates.db_model.roots.types import GitRoot, RootRequest
from integrates.migrations.utils import log_time
from integrates.organizations.utils import get_organization
from integrates.roots import domain as roots_domain

LOGGER = logging.getLogger("console")
EMAIL_MODIFIED_BY = "integrates@fluidattacks.com"
GROUP_NAMES: list[str] = []  # masked


async def get_credential_id(loaders: Dataloaders, root: GitRoot) -> str | None:
    historic_state = await loaders.root_historic_state.load(RootRequest(root.group_name, root.id))
    credential_id_candidate = next(
        (state.credential_id for state in reversed(historic_state) if state.credential_id), None
    )
    if not credential_id_candidate:
        return None

    org = await get_organization(loaders=loaders, organization_key=root.organization_name)
    credential_candidate = await loaders.credentials.load(
        CredentialsRequest(id=credential_id_candidate, organization_id=org.id)
    )
    if not credential_candidate:
        return None

    return credential_candidate.id


async def process_root(loaders: Dataloaders, root: GitRoot) -> None:
    credential_id = await get_credential_id(loaders, root)
    if not credential_id:
        LOGGER.error(
            "Credential NOT found: ROOT#%s, %s, %s",
            root.id,
            root.state.nickname,
            root.group_name,
        )

    await roots_model.update_root_state(
        current_value=root.state,
        group_name=root.group_name,
        root_id=root.id,
        state=root.state._replace(
            credential_id=credential_id,
            modified_by=EMAIL_MODIFIED_BY,
            modified_date=datetime_utils.get_utc_now(),
        ),
    )
    LOGGER.info(
        "Root processed: ROOT#%s, %s, %s, CRED#%s",
        root.id,
        root.state.nickname,
        root.group_name,
        credential_id,
    )


async def process_group(group_name: str) -> None:
    loaders = get_new_context()
    group_active_git_roots = await roots_utils.get_active_git_roots(loaders, group_name)
    group_git_roots_failed_no_cred = [
        root
        for root in group_active_git_roots
        if root.cloning.status != "OK" and not root.state.credential_id
    ]
    if not group_git_roots_failed_no_cred:
        return

    await collect(
        [process_root(loaders, root) for root in group_git_roots_failed_no_cred], workers=8
    )
    await roots_domain.queue_sync_git_roots(
        loaders=loaders,
        group_name=group_name,
        roots=group_git_roots_failed_no_cred,
        modified_by=EMAIL_MODIFIED_BY,
        should_queue_machine=True,
        force=True,
    )
    LOGGER.info(
        "Group processed: %s, %s",
        group_name,
        len(group_git_roots_failed_no_cred),
    )


@log_time(LOGGER)
async def main() -> None:
    await collect([process_group(group_name) for group_name in GROUP_NAMES], workers=4)


if __name__ == "__main__":
    main()  # type: ignore[unused-coroutine]
