"""
Distribute okada mailmap entries to other organizations and
remove entries belonging to non-existent organizations

This migration script distributes the okada mailmap entries to their
corresponding organizations based on the email domains. If it finds an
entry belonging to a non-existent organization, determined by its domain, the
entry is removed.

This version ignores domains that don't match any organization and is
case-insensitive, allowing mailmap entries to be moved to organizations
even when the domains are upper-cased or contain generic domains like gmail.

This migration focuses on organizations with alternative email domains and
will remove entries associated with non-existent organizations.

The purpose of this migration is to distribute or remove any remaining
entries originating from the `_0586_split_authors_in_okada_mailmap` migration

This migration uses a CSV file that maps email domains to organization names.
The file should be located in the corresponding S3 bucket with the
correct name.

Start Time:        2024-08-29 at 21:03:59 UTC
Finalization Time: 2024-08-29 at 21:04:05 UTC

Start Time:        2024-08-30 at 16:34:58 UTC
Finalization Time: 2024-08-30 at 16:35:11 UTC

"""

import csv
import logging
import logging.config
import os
import time

from aioextensions import (
    run,
)

from integrates.custom_exceptions import (
    ErrorDownloadingFile,
    MailmapEntryAlreadyExists,
    MailmapEntryNotFound,
    MailmapOrganizationNotFound,
    MailmapSubentryAlreadyExists,
    MailmapSubentryNotFound,
)
from integrates.mailmap.delete import (
    delete_mailmap_entry_with_subentries,
)
from integrates.mailmap.get import (
    get_mailmap_entries_with_subentries,
)
from integrates.mailmap.update import (
    move_mailmap_entry_with_subentries,
)
from integrates.s3.operations import (
    download_file,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


ORGANIZATION_ID = "okada"
ORGANIZATIONS_FILE_PATH = "orgs-names-domains-mapping.csv"


async def download_organizations_file(file_path: str) -> None:
    try:
        await download_file(
            file_name="mailmap-resources/orgs-names-domains-mapping.csv",
            file_path=file_path,
            bucket="integrates.dev",
        )
    except ErrorDownloadingFile as ex:
        LOGGER_CONSOLE.error("%s\n\n", ex)


def remove_organizations_file(file_path: str) -> None:
    if os.path.exists(file_path):
        os.remove(file_path)
        LOGGER_CONSOLE.info("Organizations file removed successfully.\n\n")
    else:
        LOGGER_CONSOLE.error("Organizations file doesn't exist.\n\n")


def build_orgs_domain_name_map(file_path: str) -> dict[str, str]:
    orgs_domain_name_map: dict[str, str] = {}

    with open(file_path, newline="", encoding="utf-8") as csv_file:
        reader = csv.DictReader(csv_file)
        for row in reader:
            domain: str = row["domain"].strip().lower()
            name: str = row["name"]
            if domain:
                orgs_domain_name_map[domain] = name

    return orgs_domain_name_map


async def main() -> None:
    await download_organizations_file(ORGANIZATIONS_FILE_PATH)
    orgs_domain_name_map = build_orgs_domain_name_map(ORGANIZATIONS_FILE_PATH)
    remove_organizations_file(ORGANIZATIONS_FILE_PATH)

    entries_with_subentries = await get_mailmap_entries_with_subentries(
        organization_id=ORGANIZATION_ID,
    )

    moved_entries = 0
    removed_entries = 0
    for entry_with_subentries in entries_with_subentries:
        entry = entry_with_subentries["entry"]
        subentries = entry_with_subentries["subentries"]

        entry_email = entry["mailmap_entry_email"]
        subentries_emails = [subentry["mailmap_subentry_email"] for subentry in subentries]

        emails = [entry_email] + subentries_emails
        lowercased_emails = [email.lower() for email in emails]

        matched_orgs = {
            org_name
            for org_domain, org_name in orgs_domain_name_map.items()
            if any(email.endswith(org_domain) for email in lowercased_emails)
        }

        if len(matched_orgs) == 1:
            matched_org_name = next(iter(matched_orgs))
            LOGGER_CONSOLE.info(
                "Mailmap entry %s with emails %s can be moved to %s\n\n",
                entry_email,
                emails,
                matched_org_name,
            )
            try:
                await move_mailmap_entry_with_subentries(
                    entry_email=entry_email,
                    organization_id=ORGANIZATION_ID,
                    new_organization_id=matched_org_name,
                )
                LOGGER_CONSOLE.info(
                    "Mailmap entry %s moved to %s successfully\n\n",
                    entry_email,
                    matched_org_name,
                )
                moved_entries += 1
            except MailmapOrganizationNotFound as ex:
                LOGGER_CONSOLE.error("%s\n\n", ex)
                LOGGER_CONSOLE.info(
                    "Mailmap entry %s: %s - %s can be removed\n\n",
                    entry_email,
                    subentries_emails,
                    matched_org_name,
                )
                try:
                    await delete_mailmap_entry_with_subentries(
                        entry_email=entry_email,
                        organization_id=ORGANIZATION_ID,
                    )
                    LOGGER_CONSOLE.info(
                        "Mailmap entry %s - %s removed successfully\n\n",
                        entry_email,
                        matched_org_name,
                    )
                    removed_entries += 1
                except (
                    MailmapEntryAlreadyExists,
                    MailmapEntryNotFound,
                    MailmapSubentryAlreadyExists,
                    MailmapSubentryNotFound,
                ):
                    LOGGER_CONSOLE.error("%s\n\n", ex)
            except (
                MailmapEntryAlreadyExists,
                MailmapEntryNotFound,
                MailmapSubentryAlreadyExists,
                MailmapSubentryNotFound,
            ) as ex:
                LOGGER_CONSOLE.error("%s\n\n", ex)

    LOGGER_CONSOLE.info(
        "Distributed entries: %s out of %s\n\n",
        moved_entries,
        len(entries_with_subentries),
    )

    LOGGER_CONSOLE.info(
        "Deleted entries: %s out of %s\n\n",
        removed_entries,
        len(entries_with_subentries),
    )


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:        %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
