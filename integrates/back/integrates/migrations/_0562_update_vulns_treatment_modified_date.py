"""
Update treatment modified date in vulnerabilities.

Execution Time:    2024-07-18 at 03:34:07 UTC
Finalization Time: 2024-07-18 at 09:41:05 UTC, extra=None
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)
from botocore.exceptions import (
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
)

from integrates.custom_exceptions import (
    VulnNotFound,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.db_model.vulnerabilities import (
    update_treatment,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.organizations.domain import (
    get_all_group_names,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")


@retry_on_exceptions(
    exceptions=(
        HTTPClientError,
        ReadTimeoutError,
        ConnectTimeoutError,
        VulnNotFound,
    ),
    sleep_seconds=5,
)
async def process_vuln(
    vuln_id: str,
) -> None:
    loaders = get_new_context()

    try:
        vuln = await loaders.vulnerability.load(vuln_id)

        if vuln is None:
            return
        if vuln.treatment is None:
            return

        report_date = (
            vuln.unreliable_indicators.unreliable_report_date
            if vuln.unreliable_indicators.unreliable_report_date
            else vuln.created_date
        )

        if vuln.treatment.modified_date < report_date:
            vuln_treatment_entry = vuln.treatment._replace(modified_date=report_date)

            await update_treatment(
                current_value=vuln,
                finding_id=vuln.finding_id,
                vulnerability_id=vuln.id,
                treatment=vuln_treatment_entry,
            )

            LOGGER_CONSOLE.info(
                "Vulnerability updated",
                extra={
                    "extra": {
                        "vuln_id": vuln.id,
                        "report_date": report_date,
                        "old_treatment_date": vuln.treatment.modified_date,
                        "new_treatment_date": report_date,
                    },
                },
            )
    except VulnNotFound as exc:
        LOGGER_CONSOLE.info(
            "Vulnerability not updated",
            extra={
                "extra": {
                    "vuln_id": vuln_id,
                },
            },
        )
        raise VulnNotFound from exc


async def process_finding(loaders: Dataloaders, finding: Finding) -> None:
    vulns = await loaders.finding_vulnerabilities_all.load(finding.id)

    await collect(
        [process_vuln(vuln.id) for vuln in vulns],
        workers=8,
    )


async def process_group(group_name: str, progress: float) -> None:
    loaders = get_new_context()
    all_findings = await loaders.group_findings_all.load(group_name)

    await collect(
        [process_finding(loaders, finding) for finding in all_findings],
        workers=8,
    )

    LOGGER_CONSOLE.info(
        "Group updated",
        extra={
            "extra": {
                "group_name": group_name,
                "progress": str(round(progress, 2)),
            },
        },
    )


async def main() -> None:
    loaders = get_new_context()
    group_names = sorted(await get_all_group_names(loaders=loaders))

    for count, group in enumerate(group_names):
        await process_group(
            group_name=group,
            progress=count / len(group_names),
        )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
