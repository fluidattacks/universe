# type: ignore
"""
Remove whitespaces from existing EnvironmentUrls, or values that do not begin
with http:// or https://

Start Time: 2023-07-27 at 23:50:23 UTC
Finalization Time: 2023-07-28 at 00:09:02 UTC
"""

import logging
import logging.config
import time
from collections import (
    defaultdict,
)
from datetime import (
    datetime,
)
from typing import (
    TypedDict,
)

from aioextensions import (
    collect,
    run,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.roots.types import (
    Root,
    RootEnvironmentUrl,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


class EnvInfo(TypedDict):
    group_name: str
    roots: list[Root]
    url: str


class EnvKeyData(TypedDict):
    group_name: str
    datetime: datetime


EnvConsistencyData = defaultdict[str, dict[str, EnvKeyData]]


def update_url(url: str):
    if not url.startswith(("http://", "https://")):
        url = "https://" + url

    if " " in url:
        url = (url.split(" "))[0]

    return url


async def update_git_environment_urls(
    root_id: str,
    url: RootEnvironmentUrl,
) -> None:
    url_key = keys.build_key(
        facet=TABLE.facets["root_environment_url"],
        values={"uuid": root_id, "hash": url.id},
    )

    new_url = update_url(url.url)
    LOGGER_CONSOLE.info("New URL: %s\n", new_url)
    url_item = {"url": new_url}

    await operations.update_item(
        item=url_item,
        key=url_key,
        table=TABLE,
    )


async def _get_root_environment_urls(loaders: Dataloaders, root: Root) -> list[RootEnvironmentUrl]:
    return await loaders.root_environment_urls.load(root.id)


async def _get_environment(
    loaders: Dataloaders,
    group_name: str,
) -> defaultdict[str, EnvInfo]:
    group_roots = await loaders.group_roots.load(group_name)

    for root in group_roots:
        urls = await _get_root_environment_urls(loaders, root)
        if urls:
            for url in urls:
                condition = (
                    url.url_type == "URL"
                    and not url.url.endswith((".apk", ".ipa", ".aab", ".zip"))
                    and not url.url.startswith("ftp")
                    and "." in url.url
                    and root.state.status == "ACTIVE"
                )
                if condition and (
                    " " in url.url or not url.url.startswith(("http://", "https://"))
                ):
                    LOGGER_CONSOLE.info("Group Name: %s", root.group_name)
                    LOGGER_CONSOLE.info("Previous URL: %s", url.url)
                    await update_git_environment_urls(root.id, url)


async def _get_environment_by_group(
    loaders: Dataloaders,
    all_group_names: list[str],
) -> None:
    await collect(
        [_get_environment(loaders, group_name) for group_name in all_group_names],
        workers=10,
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()

    all_group_names = sorted(await orgs_domain.get_all_group_names(loaders=loaders))
    await _get_environment_by_group(loaders, all_group_names)


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:    %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("%s", execution_time)
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
