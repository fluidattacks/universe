# type: ignore

"""
Populates OpenSearch with all the vulns from active groups

Execution Time:    2022-08-09 at 13:59:41 UTC
Finalization Time: 2022-08-09 at 16:20:23 UTC

Execution Time:    2022-08-10 at 13:18:44 UTC
Finalization Time: 2022-08-10 at 13:37:27 UTC

Execution Time:    2022-11-03 at 23:28:48 UTC
Finalization Time: 2022-11-03 at 23:44:21 UTC

Execution Time:    2023-09-01 at 14:24:06 UTC
Finalization Time: 2023-09-01 at 15:09:35 UTC

Execution Time:    2023-09-07 at 14:44:47 UTC
Finalization Time: 2023-09-07 at 15:26:24 UTC

Execution Time:    2023-10-27 at 21:57:59 UTC
Finalization Time: 2023-10-27 at 22:56:05 UTC

Execution Time:    2023-10-31 at 20:54:48 UTC
Finalization Time: 2023-10-31 at 21:49:12 UTC

Execution Time:    2023-11-08 at 15:13:13 UTC
Finalization Time: 2023-11-08 at 16:12:55 UTC

Execution Time:    2023-11-09 at 02:26:50 UTC
Finalization Time: 2023-11-09 at 03:23:51 UTC

Execution Time:    2023-11-17 at 02:47:15 UTC
Finalization Time: 2023-11-17 at 03:06:02 UTC
"""

import asyncio
import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)
from aiohttp.client_exceptions import (
    ClientConnectorError,
    ClientPayloadError,
    ServerDisconnectedError,
    ServerTimeoutError,
)
from boto3.dynamodb.conditions import (
    Key,
)
from botocore.exceptions import (
    ClientError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
)
from opensearchpy.helpers import (
    BulkIndexError,
    async_bulk,
)

from integrates.class_types.types import (
    Item,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.organizations.domain import (
    get_all_active_group_names,
)
from integrates.search.client import (
    get_client,
    search_shutdown,
    search_startup,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger(__name__)
NETWORK_ERRORS = (
    asyncio.TimeoutError,
    ClientConnectorError,
    ClientError,
    ClientPayloadError,
    ConnectionError,
    ConnectionResetError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
    ServerDisconnectedError,
    ServerTimeoutError,
)


def _format_vulnerability(vulnerability: dict[str, str]) -> dict[str, str]:
    # Needed as it doesn't fit in OpenSearch long data type (2^63)
    if "hash" in vulnerability:
        return {**vulnerability, "hash": str(vulnerability["hash"])}
    return vulnerability


async def process_vulnerabilities(
    group_name: str,
    vulnerabilities: tuple[dict[str, str], ...],
) -> None:
    actions = [
        {
            "_id": "#".join([vulnerability["pk"], vulnerability["sk"]]),
            "_index": "vulns_index",
            "_op_type": "index",
            "_source": _format_vulnerability(vulnerability),
        }
        for vulnerability in vulnerabilities
    ]
    client = await get_client()
    try:
        await async_bulk(client=client, actions=actions)
    except BulkIndexError as ex:
        for error in ex.errors:
            LOGGER.info("%s %s", group_name, error["index"]["error"]["reason"])


async def get_vulns(finding: Finding) -> tuple[Item]:
    primary_key = keys.build_key(
        facet=TABLE.facets["vulnerability_metadata"],
        values={"finding_id": finding.id},
    )
    index = TABLE.indexes["inverted_index"]
    key_structure = index.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.sort_key)
            & Key(key_structure.sort_key).begins_with(primary_key.partition_key)
        ),
        facets=(TABLE.facets["vulnerability_metadata"],),
        table=TABLE,
        index=index,
    )
    return response.items


@retry_on_exceptions(exceptions=NETWORK_ERRORS, max_attempts=3, sleep_seconds=3.0)
async def process_group(loaders: Dataloaders, group_name: str) -> None:
    group_findings = await loaders.group_findings_all.load(group_name)
    vulnerabilities = [
        vuln
        for finding_vulns in await collect(
            tuple(get_vulns(finding) for finding in group_findings),
        )
        for vuln in finding_vulns
    ]
    await process_vulnerabilities(group_name, vulnerabilities)

    LOGGER.info(
        "Group processed",
        extra={
            "extra": {
                "group_name": group_name,
                "vulnerabilities": len(vulnerabilities),
            },
        },
    )


async def main() -> None:
    loaders = get_new_context()
    active_group_names = sorted(await get_all_active_group_names(loaders))
    await search_startup()
    client = await get_client()
    await client.indices.delete(index="vulns_index")
    await client.indices.create(index="vulns_index")
    try:
        await collect(
            tuple(process_group(loaders, group_name) for group_name in active_group_names),
            workers=10,
        )
    finally:
        await search_shutdown()


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER.info("\n%s\n%s", execution_time, finalization_time)
