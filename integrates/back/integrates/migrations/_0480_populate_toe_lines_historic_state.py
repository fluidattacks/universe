"""
Populate toe lines historic state in integrates_vms_historic table.

Start Time: 2024-01-30 at 21:40:00 UTC
Finalization Time: 2024-01-31 at 04:10:00 UTC

Start Time: 2024-01-31 at 16:40:00 UTC
Finalization Time: 2024-01-31 at 17:20:00 UTC

Start Time: 2024-01-31 at 19:53:00 UTC
Finalization Time: 2024-02-01 at 06:08:00 UTC

Start Time: 2024-02-01 at 17:27:00 UTC
Finalization Time: 2024-02-01 at 19:49:00 UTC

Start Time: 2024-02-02 at 20:40:00 UTC
Finalization Time: 2024-02-03 at 00:12:00 UTC

Start Time: 2024-02-03 at 17:34:00 UTC
Finalization Time: 2024-02-03 at 19:30:00 UTC

Start Time: 2024-02-05 at 21:13:00 UTC
Finalization Time: 2024-02-06 at 01:22:00 UTC

Start Time: 2024-02-06 at 17:54:04 UTC
Finalization Time: 2024-02-08 at 21:03:45 UTC
"""

import asyncio
import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)
from aiohttp.client_exceptions import (
    ClientConnectorError,
    ClientOSError,
    ClientPayloadError,
    ServerDisconnectedError,
    ServerTimeoutError,
)
from boto3.dynamodb.conditions import (
    Key,
)
from botocore.exceptions import (
    ClientError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
)
from more_itertools import (
    chunked,
)

from integrates.class_types.types import (
    Item,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    HISTORIC_TABLE,
    TABLE,
)
from integrates.db_model.toe_lines.types import (
    ToeLineRequest,
)
from integrates.db_model.utils import (
    get_historic_gsi_2_key,
    get_historic_gsi_3_key,
    get_historic_gsi_sk,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.exceptions import (
    UnavailabilityError,
)
from integrates.dynamodb.types import (
    PageInfo,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


NETWORK_ERRORS = (
    asyncio.TimeoutError,
    ClientConnectorError,
    ClientError,
    ClientOSError,
    ClientPayloadError,
    ConnectionError,
    ConnectionResetError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
    ServerDisconnectedError,
    ServerTimeoutError,
    UnavailabilityError,
)


batch_put_item = retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=300,
)(operations.batch_put_item)


@retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=10,
)
async def _get_toe_lines_by_group(
    group_name: str,
    after: str | None,
) -> tuple[tuple[Item, ...], PageInfo]:
    primary_key = keys.build_key(
        facet=TABLE.facets["toe_lines_metadata"],
        values={"group_name": group_name},
    )
    index = None
    key_structure = TABLE.primary_key
    response = await operations.query(
        paginate=True,
        after=after,
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key.replace("#FILENAME", ""))
        ),
        facets=(TABLE.facets["toe_lines_metadata"],),
        index=index,
        table=TABLE,
    )

    return response.items, response.page_info


@retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=10,
)
async def _get_historic_toe_lines(
    request: ToeLineRequest,
) -> tuple[Item, ...]:
    primary_key = keys.build_key(
        facet=TABLE.facets["toe_lines_historic_metadata"],
        values={
            "filename": request.filename,
            "group_name": request.group_name,
            "root_id": request.root_id,
        },
    )
    key_structure = TABLE.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(TABLE.facets["toe_lines_historic_metadata"],),
        table=TABLE,
    )

    return response.items


def format_historic_item(item: Item, historic_item: Item) -> Item:
    item_pk = f"{item['pk']}#{item['sk']}"
    item_sk = get_historic_gsi_sk("state", historic_item["state"]["modified_date"])
    group_name = historic_item["group_name"]
    gsi_2_key = get_historic_gsi_2_key(
        HISTORIC_TABLE.facets["toe_lines_state"],
        group_name,
        "state",
        historic_item["state"]["modified_date"],
    )
    gsi_3_key = get_historic_gsi_3_key(group_name, "state", historic_item["state"]["modified_date"])
    gsi_keys = {
        "pk_2": gsi_2_key.partition_key,
        "sk_2": gsi_2_key.sort_key,
        "pk_3": gsi_3_key.partition_key,
        "sk_3": gsi_3_key.sort_key,
    }
    return {
        "pk": item_pk,
        "sk": item_sk,
        **gsi_keys,
        **(
            item["state"]
            if historic_item["state"]["modified_date"] == item["state"]["modified_date"]
            else historic_item["state"]
        ),
    }


async def process_historic_toe_lines(toe_item: Item) -> None:
    historic_toe_lines: tuple[Item, ...] = await _get_historic_toe_lines(
        ToeLineRequest(
            filename=toe_item["filename"],
            group_name=toe_item["group_name"],
            root_id=toe_item["root_id"],
        ),
    )
    if not historic_toe_lines:
        historic_toe_lines = (toe_item,)

    formatted_items = tuple(format_historic_item(toe_item, state) for state in historic_toe_lines)
    for chunked_formatted_items in chunked(formatted_items, 25):
        await batch_put_item(
            items=tuple(chunked_formatted_items),
            table=HISTORIC_TABLE,
        )


async def process_group(group_name: str, count: int) -> None:
    LOGGER_CONSOLE.info(
        "Group",
        extra={
            "extra": {
                "group_name": group_name,
                "count": count,
            },
        },
    )
    after = None
    counter = 0
    while True:
        group_toe_lines, page_info = await _get_toe_lines_by_group(group_name, after)
        await collect(
            tuple(process_historic_toe_lines(item) for item in group_toe_lines),
            workers=64,
        )
        LOGGER_CONSOLE.info(
            "Processing",
            extra={
                "extra": {
                    "group_name": group_name,
                    "count": count,
                    "group_toe_lines": len(group_toe_lines),
                },
            },
        )
        counter += len(group_toe_lines)
        after = page_info.end_cursor
        if not page_info.has_next_page:
            break

    LOGGER_CONSOLE.info(
        "Group processed",
        extra={
            "extra": {
                "group_name": group_name,
                "count": count,
                "group_toe_lines": counter,
            },
        },
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    all_group_names = sorted(await orgs_domain.get_all_group_names(loaders))
    LOGGER_CONSOLE.info(
        "All group names",
        extra={
            "extra": {
                "total": len(all_group_names),
            },
        },
    )
    for count, group_name in enumerate(all_group_names, start=1):
        if count > 219:
            await process_group(group_name, count)


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:        %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
