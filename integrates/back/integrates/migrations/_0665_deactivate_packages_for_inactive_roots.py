"""
Deactivate packages for inactive git roots.

Execution Time: 2025-02-19 at 15:16:45 UTC, extra=None
Finalization Time: 2025-02-19 at 15:43:20 UTC, extra=None

"""

import logging
import logging.config

from aioextensions import (
    collect,
)

from integrates.dataloaders import get_new_context
from integrates.db_model.groups.types import Group
from integrates.db_model.roots.enums import RootStatus
from integrates.db_model.roots.types import GitRoot
from integrates.migrations.utils import log_time
from integrates.organizations.domain import get_all_groups
from integrates.roots.domain import deactivate_root_toe_packages

LOGGER = logging.getLogger("migrations")


async def process_group(group: Group) -> None:
    loaders = get_new_context()
    git_roots = [
        root
        for root in await loaders.group_roots.load(group.name)
        if isinstance(root, GitRoot) and root.state.status == RootStatus.INACTIVE
    ]

    if not git_roots:
        LOGGER.warning("No inactive GitRoots found for group %s", group.name)
        return

    LOGGER.info("Processing %s roots for %s", len(git_roots), group.name)

    await collect(
        [
            deactivate_root_toe_packages(loaders=loaders, root_id=root.id, group_name=group.name)
            for root in git_roots
        ],
        workers=4,
    )


@log_time(LOGGER)
async def main() -> None:
    LOGGER.info("Starting package deactivation process")
    _all_groups = await get_all_groups(get_new_context())
    all_groups = sorted(_all_groups, key=lambda x: x.name, reverse=True)
    await collect(
        [process_group(group) for group in all_groups],
        workers=1,
    )
    LOGGER.info("Package deactivation process completed")


if __name__ == "__main__":
    main()  # type: ignore[unused-coroutine]
