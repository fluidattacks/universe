"""
Populate stakeholder historic state in integrates_vms_historic table.

Start Time: 2024-03-06 at 20:32:44 UTC
Finalization Time: 2024-03-06 at 20:33:36 UTC
"""

import asyncio
import logging
import logging.config
import time

from aioextensions import (
    run,
)
from aiohttp.client_exceptions import (
    ClientConnectorError,
    ClientOSError,
    ClientPayloadError,
    ServerDisconnectedError,
    ServerTimeoutError,
)
from boto3.dynamodb.conditions import (
    Key,
)
from botocore.exceptions import (
    ClientError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
)
from more_itertools import (
    chunked,
)

from integrates.class_types.types import (
    Item,
)
from integrates.custom_exceptions import (
    ErrorLoadingStakeholders,
)
from integrates.db_model import (
    HISTORIC_TABLE,
    TABLE,
)
from integrates.db_model.stakeholders.constants import (
    ALL_STAKEHOLDERS_INDEX_METADATA,
)
from integrates.db_model.utils import (
    get_historic_gsi_sk,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.exceptions import (
    UnavailabilityError,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")
NETWORK_ERRORS = (
    asyncio.TimeoutError,
    ClientConnectorError,
    ClientError,
    ClientOSError,
    ClientPayloadError,
    ConnectionError,
    ConnectionResetError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
    ServerDisconnectedError,
    ServerTimeoutError,
    UnavailabilityError,
)


batch_put_item = retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=300,
)(operations.batch_put_item)


async def _get_all_stakeholders() -> tuple[Item, ...]:
    primary_key = keys.build_key(
        facet=ALL_STAKEHOLDERS_INDEX_METADATA,
        values={"all": "all"},
    )
    index = TABLE.indexes["gsi_2"]
    key_structure = index.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(ALL_STAKEHOLDERS_INDEX_METADATA,),
        table=TABLE,
        index=index,
    )

    if not response.items:
        raise ErrorLoadingStakeholders()
    return response.items


@retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=10,
)
async def _get_historic_state(
    stakeholder_email: str,
) -> tuple[Item, ...]:
    primary_key = keys.build_key(
        facet=TABLE.facets["stakeholder_historic_state"],
        values={"email": stakeholder_email},
    )
    key_structure = TABLE.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(TABLE.facets["stakeholder_historic_state"],),
        table=TABLE,
    )
    return response.items


def format_state_item(state_item: Item, stakeholder_item: Item) -> Item:
    item_pk = f"{stakeholder_item['pk']}#{stakeholder_item['sk']}"
    item_sk = get_historic_gsi_sk("state", state_item["modified_date"])
    return {
        **(
            stakeholder_item["state"]
            if stakeholder_item.get("state")
            and state_item["modified_date"] == stakeholder_item["state"]["modified_date"]
            else state_item
        ),
        "pk": item_pk,
        "sk": item_sk,
    }


async def process_stakeholder(stakeholder_item: Item) -> None:
    stakeholder_email = str(stakeholder_item["pk"]).split("#")[1].lower().strip()
    historic_state: tuple[Item, ...] = await _get_historic_state(
        stakeholder_email=stakeholder_email,
    )
    if not historic_state and stakeholder_item.get("state"):
        historic_state = (stakeholder_item["state"],)

    formatted_items = tuple(
        format_state_item(state_item, stakeholder_item) for state_item in historic_state
    )
    for chunked_formatted_items in chunked(formatted_items, 25):
        await batch_put_item(
            items=tuple(chunked_formatted_items),
            table=HISTORIC_TABLE,
        )
    LOGGER_CONSOLE.info(
        "stakeholder processed",
        extra={
            "extra": {
                "pk": stakeholder_item["pk"],
                "sk": stakeholder_item["sk"],
                "historic_state": len(historic_state),
            },
        },
    )


async def main() -> None:
    stakeholders = await _get_all_stakeholders()
    LOGGER_CONSOLE.info(
        "All stakeholders",
        extra={
            "extra": {
                "total": len(stakeholders),
            },
        },
    )
    for count, stakeholder in enumerate(stakeholders, start=1):
        LOGGER_CONSOLE.info(
            "stakeholder",
            extra={
                "extra": {
                    "pk": stakeholder["pk"],
                    "sk": stakeholder["sk"],
                    "count": count,
                },
            },
        )
        await process_stakeholder(stakeholder)


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:        %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
