"""
Refresh code snippet in all LINES vulns that apply.

Start Time:    2023-07-18 at 04:42:32 UTC
Finalization Time: 2023-07-18 at 09:47:09 UTC

Start Time:    2023-07-18 at 22:18:27 UTC
Finalization Time: 2023-07-19 at 00:35:31 UTC
"""

import logging
import logging.config
import os
import tempfile
import time

from aioextensions import (
    collect,
    run,
)
from git.repo import (
    Repo,
)

from integrates.custom_exceptions import (
    GroupNotFound,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.enums import (
    Source,
)
from integrates.db_model.groups.enums import (
    GroupService,
)
from integrates.db_model.roots.enums import (
    RootStatus,
)
from integrates.db_model.roots.types import (
    GitRoot,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
    VulnerabilityType,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.roots.s3_mirror import (
    download_repo,
)
from integrates.settings import (
    LOGGING,
)
from integrates.vulnerabilities.domain.snippet import (
    generate_snippet,
    set_snippet,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")
EMAIL_INTEGRATES = "integrates@fluidattacks.com"


async def process_vulnerability(vulnerability: Vulnerability, repo: Repo) -> None:
    snippet = generate_snippet(vulnerability.state, repo)
    if snippet and snippet.content:
        await set_snippet(
            contents=snippet,
            vulnerability_id=vulnerability.id,
        )


async def process_root(
    loaders: Dataloaders,
    root: GitRoot,
    finding_ids: list[str],
) -> None:
    LOGGER_CONSOLE.info("%s", f"{root.id=} {root.state.nickname=}")
    try:
        vulns = await loaders.root_vulnerabilities.load(root.id)
        vulns_to_update = [
            vuln
            for vuln in vulns
            if vuln.finding_id in finding_ids
            and vuln.state.status == VulnerabilityStateStatus.VULNERABLE
            and vuln.state.source == Source.MACHINE
            and vuln.type == VulnerabilityType.LINES
        ]
        vulns_snippet_to_update = [
            vuln
            for vuln in vulns_to_update
            if not await loaders.vulnerability_snippet.load((vuln.id, vuln.state.modified_date))
        ]
        if not vulns_snippet_to_update:
            return

        with tempfile.TemporaryDirectory(
            prefix="integrates_rebase_root_",
            ignore_cleanup_errors=True,
        ) as tmpdir:
            os.chdir(tmpdir)
            repo_path = os.path.join(tmpdir, root.state.nickname)
            if not (
                repo := await download_repo(
                    root.group_name,
                    root.state.nickname,
                    tmpdir,
                    root.state.gitignore,
                )
            ):
                LOGGER_CONSOLE.info(
                    "REPO not found %s",
                    f"{root.group_name=} {root.id=} {repo_path=}",
                )
                return

            os.chdir(repo_path)
            await collect(
                tuple(process_vulnerability(vuln, repo) for vuln in vulns_snippet_to_update),
                workers=64,
            )
    except KeyError as ex:
        LOGGER_CONSOLE.info("[ERROR] Format exception: %s", ex)


async def process_group(
    loaders: Dataloaders,
    group_name: str,
    progress: float,
) -> None:
    group = await loaders.group.load(group_name)
    if not group:
        raise GroupNotFound()
    group_findings = await loaders.group_findings.load(group_name)
    findings_filtered = [
        finding.id
        for finding in group_findings
        if finding.state.status and any(finding.title.startswith(code) for code in ["011", "393"])
    ]
    if not findings_filtered:
        return

    roots = tuple(
        root
        for root in await loaders.group_roots.load(group_name)
        if isinstance(root, GitRoot) and root.state.status == RootStatus.ACTIVE
    )
    if not roots:
        return

    await collect(
        tuple(process_root(loaders, root, findings_filtered) for root in roots),
        workers=1,
    )
    LOGGER_CONSOLE.info("Group processed %s %s", group_name, f"{round(progress, 2)!s}")
    LOGGER_CONSOLE.info("Group processed %s %s", group_name, f"{round(progress, 2)!s}")


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    groups = await orgs_domain.get_all_active_groups(loaders)
    machine_group_names = sorted(
        group.name
        for group in groups
        if group.state.has_essential is True and group.state.service == GroupService.WHITE
    )
    LOGGER_CONSOLE.info("%s", f"{machine_group_names=}")
    LOGGER_CONSOLE.info("%s", f"{len(machine_group_names)=}")
    await collect(
        tuple(
            process_group(
                loaders=loaders,
                group_name=group,
                progress=count / len(machine_group_names),
            )
            for count, group in enumerate(machine_group_names)
        ),
        workers=1,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:    %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("%s", execution_time)
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
