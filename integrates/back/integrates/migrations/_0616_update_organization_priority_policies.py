"""
Update organization priority policies setup. Move MTP to PTAAS technique.

Execution Time:    2024-10-24 at 19:14:05
Finalization Time: 2024-10-24 at 19:14:05

"""

import logging
import time

from aioextensions import (
    collect,
    run,
)

from integrates.db_model.organizations import (
    get_all_organizations,
    update_metadata,
)
from integrates.db_model.organizations.types import (
    OrganizationMetadataToUpdate,
    OrganizationPriorityPolicy,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")
OLD_PRIORITY_POLICY = "MPT"
NEW_PRIORITY_POLICY = "PTAAS"


async def update_priority_policies(
    organization_id: str,
    organization_name: str,
    priority_policies: list[OrganizationPriorityPolicy],
) -> None:
    updated_priority_policies: list[OrganizationPriorityPolicy] = [
        policy_criteria
        if policy_criteria.policy != OLD_PRIORITY_POLICY
        else OrganizationPriorityPolicy(policy=NEW_PRIORITY_POLICY, value=policy_criteria.value)
        for policy_criteria in priority_policies
    ]

    await update_metadata(
        metadata=OrganizationMetadataToUpdate(priority_policies=updated_priority_policies),
        organization_id=organization_id,
        organization_name=organization_name,
    )

    LOGGER_CONSOLE.info(
        "%s organization processed",
        organization_name,
        extra={
            "extra": {
                "old_priority_policies": priority_policies,
                "new_priority_policies": updated_priority_policies,
            },
        },
    )


async def main() -> None:
    organizations = await get_all_organizations()
    await collect(
        [
            update_priority_policies(
                organization.id,
                organization.name,
                organization.priority_policies,
            )
            for organization in organizations
            if organization.priority_policies
            and OLD_PRIORITY_POLICY in [item.policy for item in organization.priority_policies]
        ],
        workers=15,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
