# type: ignore
"""
Restore toe lines historic states.

Execution Time:    2023-12-05 at 16:55:33 UTC
Finalization Time: 2023-12-05 at 17:42:21 UTC

Execution Time:    2024-06-28 at 15:39:41 UTC
Finalization Time: 2024-06-28 at 16:04:18 UTC

Execution Time:    2024-06-28 at 16:32:34 UTC
Finalization Time: 2024-06-28 at 16:32:40 UTC
"""

import asyncio
import logging
import logging.config
import time

from aioextensions import (
    run,
)
from aiohttp.client_exceptions import (
    ClientConnectorError,
    ClientOSError,
    ClientPayloadError,
    ServerDisconnectedError,
    ServerTimeoutError,
)
from botocore.exceptions import (
    ClientError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
)
from more_itertools import (
    chunked,
)

from integrates.class_types.types import (
    Item,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    HISTORIC_TABLE,
    TABLE,
)
from integrates.db_model.toe_inputs.get import (
    get_toe_inputs_enriched_historic_state_by_group,
)
from integrates.db_model.toe_inputs.types import (
    GroupHistoricToeInputsRequest,
)
from integrates.db_model.toe_lines.get import (
    get_toe_lines_enriched_historic_state_by_group,
)
from integrates.db_model.toe_lines.types import (
    GroupHistoricToeLinesRequest,
)
from integrates.db_model.utils import (
    get_historic_gsi_2_key,
    get_historic_gsi_3_key,
    get_historic_gsi_sk,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.dynamodb import (
    operations,
)
from integrates.dynamodb.exceptions import (
    UnavailabilityError,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
NETWORK_ERRORS = (
    asyncio.TimeoutError,
    ClientConnectorError,
    ClientError,
    ClientOSError,
    ClientPayloadError,
    ConnectionError,
    ConnectionResetError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
    ServerDisconnectedError,
    ServerTimeoutError,
    UnavailabilityError,
)

LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")
BACKUP_TABLE = TABLE._replace(name="back_up_table_name")
CURRENT_TABLE = TABLE
GROUP_NAME = "group_name"


batch_put_item = retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=300,
)(operations.batch_put_item)


def format_historic_item(historic_item: Item, facet: str) -> Item:
    item_pk = historic_item["pk"]
    item_sk = get_historic_gsi_sk("state", historic_item["modified_date"])
    gsi_2_key = get_historic_gsi_2_key(
        HISTORIC_TABLE.facets[facet],
        GROUP_NAME,
        "state",
        historic_item["modified_date"],
    )
    gsi_3_key = get_historic_gsi_3_key(GROUP_NAME, "state", historic_item["modified_date"])
    gsi_keys = {
        "pk_2": gsi_2_key.partition_key,
        "sk_2": gsi_2_key.sort_key,
        "pk_3": gsi_3_key.partition_key,
        "sk_3": gsi_3_key.sort_key,
    }
    return {
        **historic_item,
        "pk": item_pk,
        "sk": item_sk,
        **gsi_keys,
    }


async def restore_toe_inputs_historic() -> None:
    _backup_toe_data = await get_toe_inputs_enriched_historic_state_by_group(
        GroupHistoricToeInputsRequest(group_name=GROUP_NAME),
        BACKUP_TABLE._replace(name="back_up_table_name"),
    )
    backup_toe_data = [edge.node for edge in _backup_toe_data.edges]

    _current_toe_data = await get_toe_inputs_enriched_historic_state_by_group(
        GroupHistoricToeInputsRequest(GROUP_NAME),
    )
    current_toe_data = [edge.node for edge in _current_toe_data.edges]

    historic_to_update = [
        historic_toe for historic_toe in backup_toe_data if historic_toe not in current_toe_data
    ]

    if not historic_to_update:
        return

    LOGGER.info(
        "Historic toe input to process",
        extra={
            "extra": {
                "items": len(historic_to_update),
            },
        },
    )
    formatted_items = tuple(
        format_historic_item(state.item, "toe_input_state")
        for state in backup_toe_data
        if state.item is not None
    )

    for chunked_formatted_items in chunked(formatted_items, 25):
        await batch_put_item(
            items=tuple(chunked_formatted_items),
            table=HISTORIC_TABLE,
        )


async def restore_toe_lines_historic() -> None:
    _backup_toe_data = await get_toe_lines_enriched_historic_state_by_group(
        GroupHistoricToeLinesRequest(group_name=GROUP_NAME),
        BACKUP_TABLE._replace(name="back_up_table_name"),
    )
    backup_toe_data = [edge.node for edge in _backup_toe_data.edges]

    _current_toe_data = await get_toe_lines_enriched_historic_state_by_group(
        GroupHistoricToeLinesRequest(GROUP_NAME),
    )
    current_toe_data = [edge.node for edge in _current_toe_data.edges]

    historic_to_update = [
        historic_toe for historic_toe in backup_toe_data if historic_toe not in current_toe_data
    ]

    if not historic_to_update:
        return

    LOGGER.info(
        "Historic toe line to process",
        extra={
            "extra": {
                "items": len(historic_to_update),
            },
        },
    )
    formatted_items = tuple(
        format_historic_item(state.item, "toe_lines_state")
        for state in backup_toe_data
        if state.item is not None
    )

    for chunked_formatted_items in chunked(formatted_items, 25):
        await batch_put_item(
            items=tuple(chunked_formatted_items),
            table=HISTORIC_TABLE,
        )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    group_info = await loaders.group.load(GROUP_NAME)
    if group_info:
        await restore_toe_lines_historic()
        await restore_toe_inputs_historic()

        LOGGER.info(
            "Group processed",
            extra={
                "extra": {
                    "group_name": group_info.name,
                },
            },
        )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
