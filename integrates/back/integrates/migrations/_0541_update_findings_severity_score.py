"""
Populate the cvss_v4 field in some findings.

Execution Time:    2024-05-23 at 01:32:21 UTC
Finalization Time: 2024-05-23 at 01:39:40 UTC
"""

import contextlib
import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)
from botocore.exceptions import (
    HTTPClientError,
)
from cvss import (
    CVSS4,
)

from integrates.custom_exceptions import (
    FindingNotFound,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.findings.domain.core import (
    update_severity,
)
from integrates.organizations.domain import (
    get_all_group_names,
)
from integrates.settings import (
    LOGGING,
)
from integrates.vulnerability_files.utils import (
    get_from_v3,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")


def _get_ui(field: str) -> str:
    value = field.split(":")[1]
    if value == "N":
        return field
    return "UI:P"


def _get_modified_ui(field: str) -> str:
    first_value = field.split(":")[1]
    if first_value == "N":
        return field
    return "MUI:P"


def _get_exploit_maturity(field: str) -> str:
    first_value = field.split(":")[1]
    if first_value in {"F", "H"}:
        return "E:A"
    return field


def get_field(field: str) -> str:
    value = ""
    if field in {"CVSS:3.1", "CVSS:3.0"}:
        return "CVSS:4.0"
    if any(
        field.startswith(starts)
        for starts in [
            "AV:",
            "AC:",
            "PR:",
            "MAV:",
            "MAC:",
            "MPR:",
            "CR:",
            "IR:",
            "AR:",
        ]
    ):
        value = field
    if field.startswith("UI:"):
        value = _get_ui(field)
    if any(field.startswith(starts) for starts in ["C:", "I:", "A:"]):
        value = f"V{field[:1]}:N/S{field}"
    if field.startswith("E:"):
        value = _get_exploit_maturity(field)
    if any(field.startswith(starts) for starts in ["MC:", "MI:", "MA:"]):
        value = field[:1] + "V" + field[1:2] + ":N/" + field[:1] + "S" + field[1:]
    if field.startswith("MUI:"):
        value = _get_modified_ui(field)

    return value


def get_from_v3_types(field: str | None) -> str | None:
    if field is None:
        return None

    return (
        "/".join(
            filter(
                None,
                [get_field(field) for field in field.split("/")],
            ),
        )
        + "/AT:N"
    )


async def process_finding(loaders: Dataloaders, finding: Finding) -> None:
    if not any(finding for code in ["009", "359", "367", "411"] if finding.title.startswith(code)):
        return
    if (
        CVSS4(str(get_from_v3(finding.severity_score.cvss_v3))).clean_vector()
        != finding.severity_score.cvss_v4
    ):
        LOGGER_CONSOLE.info(
            "Finding not updated",
            extra={
                "extra": {
                    "finding_id": finding.id,
                    "cvss_v3": finding.severity_score.cvss_v3,
                    "old_cvss_v4": finding.severity_score.cvss_v4,
                    **(
                        {"modified_date": finding.creation.modified_date}
                        if finding.creation
                        else {}
                    ),
                },
            },
        )
        return

    cvss_v4 = str(get_from_v3_types(finding.severity_score.cvss_v3))

    with contextlib.suppress(FindingNotFound):
        await update_severity(loaders, finding.id, finding.severity_score.cvss_v3, cvss_v4)

    LOGGER_CONSOLE.info(
        "Finding updated",
        extra={
            "extra": {
                "finding_id": finding.id,
                "cvss_v3": finding.severity_score.cvss_v3,
                "old_cvss_v4": finding.severity_score.cvss_v4,
                "new_cvss_v4": CVSS4(cvss_v4).clean_vector(),
                "code": finding.title[:3],
                **({"modified_date": finding.creation.modified_date} if finding.creation else {}),
            },
        },
    )


@retry_on_exceptions(
    exceptions=(HTTPClientError,),
    sleep_seconds=10,
)
async def process_group(group_name: str, progress: float) -> None:
    loaders = get_new_context()
    all_findings = await loaders.group_findings_all.load(group_name)

    await collect(
        [process_finding(loaders, finding) for finding in all_findings],
        workers=16,
    )

    LOGGER_CONSOLE.info(
        "Group updated",
        extra={
            "extra": {
                "group_name": group_name,
                "progress": str(round(progress, 2)),
            },
        },
    )


async def main() -> None:
    loaders = get_new_context()
    group_names = sorted(await get_all_group_names(loaders=loaders))

    for count, group in enumerate(group_names):
        await process_group(
            group_name=group,
            progress=count / len(group_names),
        )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC%Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC%Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
