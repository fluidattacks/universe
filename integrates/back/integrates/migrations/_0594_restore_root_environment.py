"""
Restore root environments
Execution Time:    2024-09-11 at 03:10:13 UTC
Finalization Time: 2024-09-11 at 03:10:14 UTC
"""

import logging
import logging.config
import time
from datetime import (
    datetime,
)

from aioextensions import (
    collect,
    run,
)
from boto3.dynamodb.conditions import (
    Key,
)

from integrates.class_types.types import (
    Item,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model import (
    TABLE as CURRENT,
)
from integrates.db_model.items import RootEnvironmentUrlItem
from integrates.db_model.roots.add import (
    add_root_environment_secret,
    add_root_environment_url,
)
from integrates.db_model.roots.types import (
    Root,
    RootEnvironmentSecretsRequest,
    RootEnvironmentUrl,
    Secret,
)
from integrates.db_model.roots.utils import (
    format_environment_url,
    format_secret_state,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

# Constants
TABLE = CURRENT._replace(name="")
LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")


async def get_group(*, group_name: str) -> Item | None:
    primary_key = keys.build_key(
        facet=TABLE.facets["group_metadata"],
        values={"name": group_name},
    )

    key_structure = TABLE.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(TABLE.facets["group_metadata"],),
        limit=1,
        table=TABLE,
    )

    if not response.items:
        return None

    return response.items[0]


async def get_root_environment_urls(*, root_id: str, group_name: str) -> list[RootEnvironmentUrl]:
    primary_key = keys.build_key(
        facet=TABLE.facets["root_environment_url"],
        values={
            "uuid": root_id,
            "group_name": group_name,
        },
    )
    key_structure = TABLE.primary_key
    response = await operations.DynamoClient[RootEnvironmentUrlItem].query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & (Key(key_structure.sort_key).begins_with(primary_key.sort_key))
        ),
        facets=(TABLE.facets["root_environment_url"],),
        table=TABLE,
    )
    return [format_environment_url(item) for item in response.items]


async def get_environment_secrets(*, request: RootEnvironmentSecretsRequest) -> list[Secret]:
    key_structure = TABLE.primary_key
    primary_key = keys.build_key(
        facet=TABLE.facets["root_environment_secret"],
        values={
            "group_name": request.group_name,
            "hash": request.url_id,
        },
    )
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(TABLE.facets["root_environment_secret"],),
        table=TABLE,
    )
    return [
        Secret(
            key=item["key"],
            value=item["value"],
            created_at=datetime.fromisoformat(item["created_at"]),
            state=format_secret_state(item["state"]),
        )
        for item in response.items
    ]


async def process_environment(environment: RootEnvironmentUrl) -> None:
    await add_root_environment_url(
        group_name=environment.group_name,
        root_id=environment.root_id,
        url=environment,
    )
    env_secs = await get_environment_secrets(
        request=RootEnvironmentSecretsRequest(
            group_name=environment.group_name,
            url_id=environment.id,
        ),
    )
    await collect(
        [
            add_root_environment_secret(
                group_name=environment.group_name,
                resource_id=environment.id,
                secret=env_sec,
            )
            for env_sec in env_secs
        ],
    )


async def process_root(root: Root) -> None:
    environments = await get_root_environment_urls(root_id=root.id, group_name=root.group_name)
    await collect([process_environment(environment) for environment in environments])


async def main() -> None:
    loaders = get_new_context()
    roots = await loaders.group_roots.load("")
    await collect([process_root(root) for root in roots])


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
