# type: ignore
"""
Restore vulnerabilities from backup.

Execution Time:    2024-04-30 at 15:45:03 UTC
Finalization Time: 2024-04-30 at 15:46:54 UTC

Execution Time:    2024-05-02 at 23:01:56 UTC
Finalization Time: 2024-05-02 at 23:02:37 UTC
"""

import logging
import logging.config
import time

import simplejson as json
from aioextensions import (
    collect,
    run,
)
from boto3.dynamodb.conditions import (
    Key,
)

from integrates.db_model import (
    TABLE,
)
from integrates.db_model.findings.get import (
    get_finding_by_id,
    get_findings_by_group,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.db_model.types import (
    Treatment,
)
from integrates.db_model.utils import (
    get_as_utc_iso_format,
    serialize,
)
from integrates.db_model.vulnerabilities.add import (
    get_vulnerability_item,
)
from integrates.db_model.vulnerabilities.constants import (
    HASH_INDEX_METADATA,
)
from integrates.db_model.vulnerabilities.get import (
    get_finding_vulnerabilities,
    get_historic_state,
    get_historic_treatment,
    get_historic_verification,
    get_historic_zero_risk,
    get_vulnerability,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityState,
    VulnerabilityVerification,
    VulnerabilityZeroRisk,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")
BACKUP_TABLE = TABLE._replace(name="database_backup_name")
CURRENT_TABLE = TABLE
GROUP_NAME = "old_group"
ROOTS_TO_REPLACE = {"source_root": "target_root"}


async def add_vuln_historic_zero_risk(vuln_id: str, zero_risk: VulnerabilityZeroRisk) -> None:
    items = []
    zero_risk_key = keys.build_key(
        facet=TABLE.facets["vulnerability_historic_zero_risk"],
        values={
            "id": vuln_id,
            "iso8601utc": get_as_utc_iso_format(zero_risk.modified_date),
        },
    )
    key_structure = TABLE.primary_key
    historic_zero_risk_item = {
        key_structure.partition_key: zero_risk_key.partition_key,
        key_structure.sort_key: zero_risk_key.sort_key,
        **json.loads(json.dumps(zero_risk, default=serialize)),
    }
    items.append(historic_zero_risk_item)
    await operations.batch_put_item(items=tuple(items), table=TABLE)
    LOGGER.info(
        "Vuln historic zero risk added",
        extra={
            "extra": {
                "vuln_id": vuln_id,
                "state": get_as_utc_iso_format(zero_risk.modified_date),
            },
        },
    )


async def restore_vuln_historic_zero_risk(vuln_id: str) -> None:
    zero_risk_backup_data = await get_historic_zero_risk(
        vulnerability_id=vuln_id,
        table=BACKUP_TABLE,
    )
    if not zero_risk_backup_data:
        LOGGER.info(
            "No zero risk to process",
            extra={
                "extra": {
                    "vuln_id": vuln_id,
                },
            },
        )
        return

    zero_risk_current_data = await get_historic_zero_risk(vulnerability_id=vuln_id)
    historic_to_update = [
        zero_risk for zero_risk in zero_risk_backup_data if zero_risk not in zero_risk_current_data
    ]

    if not historic_to_update:
        LOGGER.info(
            "Zero risk up to date",
            extra={
                "extra": {
                    "vuln_id": vuln_id,
                },
            },
        )
        return

    LOGGER.info(
        "Historic zero risk to process",
        extra={
            "extra": {
                "vuln_id": vuln_id,
                "items": len(historic_to_update),
            },
        },
    )

    await collect(
        tuple(add_vuln_historic_zero_risk(vuln_id, zero_risk) for zero_risk in historic_to_update),
        workers=5,
    )


async def add_vuln_historic_verification(
    vuln_id: str,
    verification: VulnerabilityVerification,
) -> None:
    items = []
    verification_key = keys.build_key(
        facet=TABLE.facets["vulnerability_historic_verification"],
        values={
            "id": vuln_id,
            "iso8601utc": get_as_utc_iso_format(verification.modified_date),
        },
    )
    key_structure = TABLE.primary_key
    historic_verification_item = {
        key_structure.partition_key: verification_key.partition_key,
        key_structure.sort_key: verification_key.sort_key,
        **json.loads(json.dumps(verification, default=serialize)),
    }
    items.append(historic_verification_item)
    await operations.batch_put_item(items=tuple(items), table=TABLE)
    LOGGER.info(
        "Vuln historic verification added",
        extra={
            "extra": {
                "vuln_id": vuln_id,
                "state": get_as_utc_iso_format(verification.modified_date),
            },
        },
    )


async def restore_vuln_historic_verification(vuln_id: str) -> None:
    verification_backup_data = await get_historic_verification(
        vulnerability_id=vuln_id,
        table=BACKUP_TABLE,
    )
    if not verification_backup_data:
        LOGGER.info(
            "No verification to process",
            extra={
                "extra": {
                    "vuln_id": vuln_id,
                },
            },
        )
        return

    verification_current_data = await get_historic_verification(vulnerability_id=vuln_id)
    historic_to_update = [
        verification
        for verification in verification_backup_data
        if verification not in verification_current_data
    ]

    if not historic_to_update:
        LOGGER.info(
            "Verification up to date",
            extra={
                "extra": {
                    "vuln_id": vuln_id,
                },
            },
        )
        return

    LOGGER.info(
        "Historic verification to process",
        extra={
            "extra": {
                "vuln_id": vuln_id,
                "items": len(historic_to_update),
            },
        },
    )

    await collect(
        tuple(
            add_vuln_historic_verification(vuln_id, verification)
            for verification in historic_to_update
        ),
        workers=5,
    )


async def add_vuln_historic_treatment(vuln_id: str, treatment: Treatment) -> None:
    items = []
    treatment_key = keys.build_key(
        facet=TABLE.facets["vulnerability_historic_treatment"],
        values={
            "id": vuln_id,
            "iso8601utc": get_as_utc_iso_format(treatment.modified_date),
        },
    )
    key_structure = TABLE.primary_key
    historic_treatment_item = {
        key_structure.partition_key: treatment_key.partition_key,
        key_structure.sort_key: treatment_key.sort_key,
        **json.loads(json.dumps(treatment, default=serialize)),
    }
    items.append(historic_treatment_item)
    await operations.batch_put_item(items=tuple(items), table=TABLE)
    LOGGER.info(
        "Vuln historic treatment added",
        extra={
            "extra": {
                "vuln_id": vuln_id,
                "state": get_as_utc_iso_format(treatment.modified_date),
            },
        },
    )


async def restore_vuln_historic_treatment(vuln_id: str) -> None:
    treatment_backup_data = await get_historic_treatment(
        vulnerability_id=vuln_id,
        table=BACKUP_TABLE,
    )
    if not treatment_backup_data:
        LOGGER.info(
            "No treatment to process",
            extra={
                "extra": {
                    "vuln_id": vuln_id,
                },
            },
        )
        return

    treatment_current_data = await get_historic_treatment(vulnerability_id=vuln_id)
    historic_to_update = [
        treatment for treatment in treatment_backup_data if treatment not in treatment_current_data
    ]

    if not historic_to_update:
        LOGGER.info(
            "Treatment up to date",
            extra={
                "extra": {
                    "vuln_id": vuln_id,
                },
            },
        )
        return

    LOGGER.info(
        "Historic treatment to process",
        extra={
            "extra": {
                "vuln_id": vuln_id,
                "items": len(historic_to_update),
            },
        },
    )

    await collect(
        tuple(add_vuln_historic_treatment(vuln_id, treatment) for treatment in historic_to_update),
        workers=5,
    )


async def add_vuln_historic_state(vuln_id: str, state: VulnerabilityState) -> None:
    items = []
    state_key = keys.build_key(
        facet=TABLE.facets["vulnerability_historic_state"],
        values={
            "id": vuln_id,
            "iso8601utc": get_as_utc_iso_format(state.modified_date),
        },
    )
    key_structure = TABLE.primary_key
    historic_state_item = {
        key_structure.partition_key: state_key.partition_key,
        key_structure.sort_key: state_key.sort_key,
        **json.loads(json.dumps(state, default=serialize)),
    }
    items.append(historic_state_item)
    await operations.batch_put_item(items=tuple(items), table=TABLE)
    LOGGER.info(
        "Vuln historic state added",
        extra={
            "extra": {
                "vuln_id": vuln_id,
                "state": get_as_utc_iso_format(state.modified_date),
            },
        },
    )


async def restore_vuln_historic_state(vuln_id: str) -> None:
    historic_state_backup_data = await get_historic_state(
        vulnerability_id=vuln_id,
        table=BACKUP_TABLE,
    )
    historic_state_current_data = await get_historic_state(vulnerability_id=vuln_id)
    historic_to_update = [
        state for state in historic_state_backup_data if state not in historic_state_current_data
    ]
    if not historic_to_update:
        LOGGER.info(
            "No state to process",
            extra={
                "extra": {
                    "vuln_id": vuln_id,
                },
            },
        )
        return

    LOGGER.info(
        "Historic states to process",
        extra={
            "extra": {
                "vuln_id": vuln_id,
                "state_items": len(historic_to_update),
            },
        },
    )

    await collect(
        tuple(add_vuln_historic_state(vuln_id, state) for state in historic_to_update),
        workers=5,
    )


async def process_vulnerability_historics(vuln_id: str) -> None:
    await restore_vuln_historic_state(vuln_id)
    await restore_vuln_historic_treatment(vuln_id)
    await restore_vuln_historic_verification(vuln_id)
    await restore_vuln_historic_zero_risk(vuln_id)


async def add_vulnerability(vulnerability: Vulnerability) -> None:
    items = []
    key_structure = TABLE.primary_key
    vulnerability_key = keys.build_key(
        facet=TABLE.facets["vulnerability_metadata"],
        values={
            "finding_id": vulnerability.finding_id,
            "id": vulnerability.id,
        },
    )
    response = await operations.query(
        condition_expression=(Key(key_structure.partition_key).eq(vulnerability_key.partition_key)),
        facets=(TABLE.facets["vulnerability_metadata"],),
        limit=1,
        table=TABLE,
    )

    if response.items:
        LOGGER.info(
            "Vuln already exists",
            extra={
                "extra": {
                    "vuln_id": vulnerability.id,
                },
            },
        )
        return

    vulnerability_item = get_vulnerability_item(
        vulnerability=vulnerability,
        key_structure=key_structure,
        vulnerability_key=vulnerability_key,
    )

    if vulnerability.hash:
        gsi_hash_key = keys.build_key(
            facet=HASH_INDEX_METADATA,
            values={
                "root_id": vulnerability.root_id,
                "hash": str(vulnerability.hash),
            },
        )
        gsi_hash_index = TABLE.indexes["gsi_hash"]
        vulnerability_item[gsi_hash_index.primary_key.partition_key] = gsi_hash_key.partition_key
        vulnerability_item[gsi_hash_index.primary_key.sort_key] = gsi_hash_key.sort_key

    items.append(vulnerability_item)
    await operations.batch_put_item(items=tuple(items), table=TABLE)
    await process_vulnerability_historics(vulnerability.id)
    LOGGER.info(
        "Vulnerability added",
        extra={
            "extra": {
                "vuln_id": vulnerability.id,
            },
        },
    )


async def restore_vulnerability(
    vuln_id: str,
    target_group_name: str,
    target_finding_id: str,
) -> None:
    vulnerability_backup_data = await get_vulnerability(
        vulnerability_id=vuln_id,
        table=BACKUP_TABLE,
    )
    vulnerability_current_data = await get_vulnerability(vulnerability_id=vuln_id)
    vulnerability_to_update = (
        vulnerability_backup_data._replace(
            root_id=ROOTS_TO_REPLACE[vulnerability_backup_data.root_id],
        )
        if vulnerability_backup_data
        and vulnerability_current_data != vulnerability_backup_data
        and vulnerability_backup_data.root_id in ROOTS_TO_REPLACE
        else None
    )
    if not vulnerability_to_update:
        LOGGER.info(
            "No vulnerability to process",
            extra={
                "extra": {
                    "vuln_id": vuln_id,
                },
            },
        )
        return

    vulnerability_to_update = vulnerability_to_update._replace(
        finding_id=target_finding_id,
        group_name=target_group_name,
    )

    LOGGER.info(
        "Vulnerability to process",
        extra={
            "extra": {
                "vulnerability_id": vuln_id,
            },
        },
    )

    await add_vulnerability(vulnerability_to_update)


async def restore_all_vulnerabilities(
    finding_id: str,
    target_group_name: str,
    source_finding_id: str,
) -> None:
    vulnerabilities = await get_finding_vulnerabilities(
        finding_id=source_finding_id,
        table=BACKUP_TABLE,
    )

    if not vulnerabilities:
        LOGGER.info(
            "No vulnerabilities to process",
            extra={
                "extra": {
                    "finding_id": source_finding_id,
                },
            },
        )
        return

    await collect(
        tuple(
            restore_vulnerability(vulnerability.id, target_group_name, finding_id)
            for vulnerability in vulnerabilities
        ),
        workers=5,
    )

    LOGGER.info(
        "Vulnerabilities processed",
        extra={
            "extra": {
                "finding_id": source_finding_id,
                "vulns": len(vulnerabilities),
            },
        },
    )


async def process_finding(
    finding_id: str,
    target_group_name: str,
    source_group_findings: list[Finding],
) -> None:
    target_finding = await get_finding_by_id(finding_id)
    if not target_finding:
        LOGGER.info(
            "Target finding not found",
            extra={
                "extra": {
                    "finding_id": finding_id,
                },
            },
        )
        return

    source_finding = next(
        (
            finding
            for finding in source_group_findings
            if finding.title == target_finding.title
            and finding.description == target_finding.description
            and finding.recommendation == target_finding.recommendation
        ),
        None,
    )

    if not source_finding:
        LOGGER.info(
            "Source finding not found",
            extra={
                "extra": {
                    "target_finding_id": finding_id,
                },
            },
        )
        return

    await restore_all_vulnerabilities(finding_id, target_group_name, source_finding.id)


async def restore_all_findings(
    target_group_name: str,
    findings_id: list[str],
) -> None:
    group_findings = await get_findings_by_group(GROUP_NAME, BACKUP_TABLE)
    await collect(
        tuple(
            process_finding(finding_id, target_group_name, group_findings)
            for finding_id in findings_id
        ),
        workers=1,
    )

    LOGGER.info(
        "Findings processed",
        extra={
            "extra": {
                "findings": len(findings_id),
                "findings_ids": list(findings_id),
            },
        },
    )


async def main() -> None:
    groups_and_findings = {
        "group_name": ["finding_id"],
    }

    for group_name, findings_id in groups_and_findings.items():
        await restore_all_findings(group_name, findings_id)
        LOGGER.info(
            "Group processed",
            extra={
                "extra": {
                    "group_name": group_name,
                },
            },
        )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
