"""
Review vulnerability technique field to restore CSPM values.

Execution Time:    2024-10-29 at 03:19:20 UTC
Finalization Time: 2024-10-29 at 03:32:05 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)
from boto3.dynamodb.conditions import (
    Key,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    TABLE as INTEGRATES_VMS_TABLE,
)
from integrates.db_model import (
    vulnerabilities as vulns_model,
)
from integrates.db_model.items import VulnerabilityItem
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityTechnique,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityMetadataToUpdate,
)
from integrates.db_model.vulnerabilities.utils import (
    format_vulnerability,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.organizations.domain import (
    get_all_group_names,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")
BACKUP_TABLE_NAME = "vms_backup_oct_28"
BACKUP_TABLE = INTEGRATES_VMS_TABLE._replace(name=BACKUP_TABLE_NAME)


async def get_vulnerability_from_backup(
    vulnerability_id: str,
) -> Vulnerability | None:
    primary_key = keys.build_key(
        facet=BACKUP_TABLE.facets["vulnerability_metadata"],
        values={"id": vulnerability_id},
    )

    key_structure = BACKUP_TABLE.primary_key
    response = await operations.DynamoClient[VulnerabilityItem].query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(BACKUP_TABLE.facets["vulnerability_metadata"],),
        limit=1,
        table=BACKUP_TABLE,
    )

    if not response.items:
        return None

    return format_vulnerability(response.items[0])


async def _update_vulnerability(vulnerability: Vulnerability) -> None:
    vulnerability_bkp = await get_vulnerability_from_backup(vulnerability.id)
    if vulnerability_bkp is None:
        LOGGER_CONSOLE.info(
            "Vulnerability %s from %s not in the database backup",
            vulnerability.id,
            vulnerability.group_name,
        )
        return

    if vulnerability_bkp.technique and vulnerability_bkp.technique == VulnerabilityTechnique.CSPM:
        await vulns_model.update_metadata(
            finding_id=vulnerability.finding_id,
            vulnerability_id=vulnerability.id,
            metadata=VulnerabilityMetadataToUpdate(technique=VulnerabilityTechnique.CSPM),
        )
        LOGGER_CONSOLE.info(
            "Updated vulnerability %s from %s",
            vulnerability.id,
            vulnerability.group_name,
        )


async def process_group(
    loaders: Dataloaders,
    group_name: str,
) -> None:
    if group_name.lower().startswith(tuple("s,t,u,v,w,x,y,z")):
        group_findings = await loaders.group_findings.load(group_name)

        vulns = await loaders.finding_vulnerabilities.load_many_chained(
            [fin.id for fin in group_findings],
        )

        LOGGER_CONSOLE.info("Group to process  %s", group_name)

        await collect(
            tuple(
                _update_vulnerability(vuln)
                for vuln in vulns
                if vuln.technique and vuln.technique == VulnerabilityTechnique.PTAAS
            ),
            workers=20,
        )

        LOGGER_CONSOLE.info("Group processed  %s", group_name)


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    groups = sorted(await get_all_group_names(loaders))
    await collect([process_group(loaders, group) for group in groups], workers=8)


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
