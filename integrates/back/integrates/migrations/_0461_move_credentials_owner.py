"""
Move the owner to the credentials state.

Execution Time:    2023-11-23 at 23:06:59 UTC
Finalization Time: 2023-11-23 at 23:07:05 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)
from boto3.dynamodb.conditions import (
    Attr,
    Key,
)

from integrates.class_types.types import (
    Item,
)
from integrates.db_model import (
    TABLE,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.types import (
    PrimaryKey,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


async def process_credentials_item(
    *,
    item: Item,
) -> None:
    key_structure = TABLE.primary_key
    credential_key = PrimaryKey(partition_key=item["pk"], sort_key=item["sk"])
    if item.get("owner") is None:
        return
    item = {
        "owner": None,
        "state.owner": item["owner"],
    }
    await operations.update_item(
        condition_expression=Attr(key_structure.partition_key).exists(),
        item=item,
        key=credential_key,
        table=TABLE,
    )


async def process_organizations_credentials(
    organization_id: str,
) -> None:
    credentials_items = await _get_organization_credentials_items(organization_id=organization_id)
    await collect(
        tuple(
            process_credentials_item(
                item=credentials_item,
            )
            for credentials_item in credentials_items
        ),
        workers=100,
    )


async def _get_organization_credentials_items(*, organization_id: str) -> tuple[Item, ...]:
    primary_key = keys.build_key(
        facet=TABLE.facets["credentials_metadata"],
        values={"organization_id": organization_id},
    )
    index = TABLE.indexes["inverted_index"]
    key_structure = index.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.sort_key)
            & Key(key_structure.sort_key).begins_with(primary_key.partition_key)
        ),
        facets=(TABLE.facets["credentials_metadata"],),
        index=index,
        table=TABLE,
    )

    return response.items


async def main() -> None:
    count = 0
    organization_ids = [
        organization.id async for organization in orgs_domain.iterate_organizations()
    ]
    for organization_id in organization_ids:
        count += 1
        LOGGER_CONSOLE.info(
            "Organization to process",
            extra={
                "extra": {
                    "organization_id": organization_id,
                    "count": count,
                },
            },
        )
        await process_organizations_credentials(organization_id)


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
