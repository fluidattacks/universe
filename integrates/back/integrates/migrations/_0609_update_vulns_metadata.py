"""Update some NULL field in vulnerabilities_metadata"""

import sys
import time

from aioextensions import (
    collect,
    in_thread,
    run,
)
from boto3.dynamodb.conditions import (
    Key,
)
from psycopg2 import (  # type: ignore[import-untyped]
    sql,
)
from psycopg2.extensions import (  # type: ignore[import-untyped]
    cursor as cursor_cls,
)

from integrates.archive.utils import (
    mask_by_encoding,
)
from integrates.class_types.types import (
    Item,
)
from integrates.db_model import (
    TABLE as INTEGRATES_VMS_TABLE,
)
from integrates.db_model.last_sync import (
    get_db_cursor,
)
from integrates.dynamodb import (
    keys,
    operations,
)

GROUP_NAMES: list[str] = []
BACKUP_TABLE_NAME = ""
BACKUP_TABLE = INTEGRATES_VMS_TABLE._replace(name=BACKUP_TABLE_NAME)


async def get_vulnerability(*, vulnerability_id: str) -> Item | None:
    primary_key = keys.build_key(
        facet=BACKUP_TABLE.facets["vulnerability_metadata"],
        values={"id": vulnerability_id},
    )

    key_structure = BACKUP_TABLE.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(BACKUP_TABLE.facets["vulnerability_metadata"],),
        limit=1,
        table=BACKUP_TABLE,
    )

    if not response.items:
        return None

    return response.items[0]


def _update_vulns_metadata(
    *,
    cursor: cursor_cls,
    _id: str,
    where_str: str | None,
    root_id: str | None,
) -> None:
    cursor.execute(
        sql.SQL(
            """
                UPDATE
                    integrates.vulnerabilities_metadata
                SET
                    where_str = %(where_str)s,
                    root_id = %(root_id)s
                WHERE
                    id = %(id)s;
            """,
        ),
        {
            "id": _id,
            "where_str": where_str,
            "root_id": root_id,
        },
    )


async def update_vulns_metadata(
    *,
    cursor: cursor_cls,
    vuln_redshift: tuple[str, str | None, str | None, str | None, str | None],
    vuln_dynamodb: Item,
) -> None:
    root_id = vuln_dynamodb.get("root_id") or ""
    pk_2 = vuln_dynamodb.get("pk_2") or ""
    if not root_id:
        root_id = pk_2.split("#")[1] if pk_2.startswith("ROOT#") else ""
    where_str = (
        vuln_dynamodb["state"]["where"].split(".")[-1].split(" ")[0]
        if vuln_dynamodb["type"] == "LINES" and len(vuln_dynamodb["state"]["where"].split(".")) > 1
        else None
    )
    await in_thread(
        _update_vulns_metadata,
        cursor=cursor,
        _id=vuln_redshift[0],
        where_str=where_str,
        root_id=root_id,
    )


def get_vulns_by_group_name(
    cursor: cursor_cls,
    group_name: str,
) -> list[tuple[str, str | None, str | None, str | None, str | None]]:
    cursor.execute(
        sql.SQL(
            """
            SELECT
                v.id, v.finding_id, v.where_str, v.root_id
            FROM
                integrates.vulnerabilities_metadata v JOIN
                integrates.findings_metadata f ON
                v.finding_id = f.id
            WHERE
                f.group_name = %(group_name)s AND
                v.where_str is NULL AND
                v.root_id is NULL;
            """,
        ),
        {
            "group_name": mask_by_encoding(group_name),
        },
    )

    return list(cursor.fetchall())


async def main(*args: str) -> None:
    groups_to_process: list[str] = GROUP_NAMES
    raw_groups: list[str] = args[0].split(",")
    groups_to_process.extend([group.strip() for group in raw_groups if len(group.strip())])

    global BACKUP_TABLE_NAME, BACKUP_TABLE
    BACKUP_TABLE_NAME = args[1].strip() or BACKUP_TABLE_NAME
    BACKUP_TABLE = INTEGRATES_VMS_TABLE._replace(name=BACKUP_TABLE_NAME)
    with get_db_cursor() as cursor:
        for group_name in GROUP_NAMES:
            vulns_redshift = get_vulns_by_group_name(cursor, group_name)
            vulns_dynamo = await collect(
                (get_vulnerability(vulnerability_id=vuln[0]) for vuln in vulns_redshift),
                workers=4,
            )
            await collect(
                (
                    update_vulns_metadata(
                        cursor=cursor,
                        vuln_redshift=vuln_r,
                        vuln_dynamodb=vuln_d,
                    )
                    for vuln_r, vuln_d in zip(vulns_redshift, vulns_dynamo, strict=False)
                    if vuln_r and vuln_d
                ),
                workers=4,
            )


if __name__ == "__main__":
    migration_args = sys.argv[1:]
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main(*migration_args))
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    print("\n%s\n%s", execution_time, finalization_time)
