"""
Move the metadata attributes to the state in the group access.

Execution Time:    2023-12-18 at 19:20:56 UTC
Finalization Time: 2023-12-18 at 19:24:08 UTC

Execution Time: 2024-01-17 at 19:54:16 UTC
Finalization Time: 2024-01-17 at 19:57:25 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)
from boto3.dynamodb.conditions import (
    Attr,
    Key,
)
from botocore.exceptions import (
    ConnectTimeoutError,
    ReadTimeoutError,
)

from integrates.class_types.types import (
    Item,
)
from integrates.custom_utils.datetime import (
    get_iso_date,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.types import (
    PrimaryKey,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")
EMAIL_INTEGRATES = "integrates@fluidattacks.com"


ATTRS_TO_MIGRATE = [
    "modified_by",
    "confirm_deletion",
    "has_access",
    "invitation",
    "responsibility",
    "role",
]


async def _get_historic_group_access(*, email: str, group_name: str) -> tuple[Item, ...]:
    historic_key = keys.build_key(
        facet=TABLE.facets["group_historic_access"],
        values={
            "email": email.lower().strip(),
            "name": group_name.lower().strip(),
        },
    )
    key_structure = TABLE.primary_key
    condition_expression = Key(key_structure.partition_key).eq(historic_key.partition_key) & Key(
        key_structure.sort_key,
    ).begins_with(historic_key.sort_key)
    response = await operations.query(
        condition_expression=condition_expression,
        facets=(TABLE.facets["group_historic_access"],),
        table=TABLE,
    )

    return response.items


async def _get_group_stakeholders_access(
    *,
    group_name: str,
) -> tuple[Item, ...]:
    group_name = group_name.lower().strip()
    primary_key = keys.build_key(
        facet=TABLE.facets["group_access"],
        values={
            "name": group_name,
        },
    )

    index = TABLE.indexes["inverted_index"]
    key_structure = index.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.sort_key)
            & Key(key_structure.sort_key).begins_with(primary_key.partition_key)
        ),
        facets=(TABLE.facets["group_access"],),
        table=TABLE,
        index=index,
    )

    return response.items


@retry_on_exceptions(
    exceptions=(ReadTimeoutError, ConnectTimeoutError),
    sleep_seconds=3,
)
async def process_item(item: Item) -> None:
    key_structure = TABLE.primary_key
    state = item.get("state", {})
    new_item = {
        f"state.{attr}": state[attr] if attr in state else item.get(attr)
        for attr in ATTRS_TO_MIGRATE
    }
    # cleanup
    new_item.update({attr: None for attr in ATTRS_TO_MIGRATE})
    if "state" not in item:
        modified_date = get_iso_date()
        await operations.update_item(
            condition_expression=Attr(key_structure.partition_key).exists(),
            item={
                "state": {
                    "modified_date": modified_date,
                    "modified_by": EMAIL_INTEGRATES,
                },
            },
            key=PrimaryKey(partition_key=item["pk"], sort_key=item["sk"]),
            table=TABLE,
        )
        new_item["state.modified_by"] = EMAIL_INTEGRATES
        new_item["state.modified_date"] = modified_date
    await operations.update_item(
        condition_expression=Attr(key_structure.partition_key).exists(),
        item=new_item,
        key=PrimaryKey(partition_key=item["pk"], sort_key=item["sk"]),
        table=TABLE,
    )


async def process_group_access(item: Item) -> None:
    email = str(item["email"]).lower().strip()
    group_name = item["group_name"]
    await process_item(item)
    historic = await _get_historic_group_access(email=email, group_name=group_name)
    await collect([process_item(historic_item) for historic_item in historic])


async def process_group(group_name: str) -> None:
    group_stakeholders_access = await _get_group_stakeholders_access(group_name=group_name)
    await collect(
        tuple(process_group_access(group_access) for group_access in group_stakeholders_access),
        workers=1000,
    )
    LOGGER_CONSOLE.info(
        "Group processed",
        extra={
            "extra": {
                "group_name": group_name,
                "group_stakeholders_access": len(group_stakeholders_access),
            },
        },
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    all_group_names = sorted(await orgs_domain.get_all_group_names(loaders))
    LOGGER_CONSOLE.info(
        "All group names",
        extra={
            "extra": {
                "total": len(all_group_names),
            },
        },
    )
    for count, group_name in enumerate(all_group_names, start=1):
        LOGGER_CONSOLE.info(
            "Group",
            extra={
                "extra": {
                    "group_name": group_name,
                    "count": count,
                },
            },
        )
        await process_group(group_name)


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
