"""
Update the indicator "total-open-priority" of the findings in every group.

Start Time:         2024-08-15 at 23:03:57 UTC
Finalization Time:  2024-08-15 at 23:10:16 UTC
Start Time:         2024-08-16 at 17:22:10 UTC
Finalization Time:  2024-08-16 at 17:29:08 UTC
Start Time:         2024-11-12 at 21:25:35 UTC
Finalization Time:  2024-11-12 at 21:33:06 UTC
Start Time:         2024-11-23 at 00:09:52 UTC
Finalization Time:  2024-11-23 at 00:20:12 UTC
Start Time:         2024-11-26 at 13:53:46 UTC
Finalization Time:  2024-11-26 at 14:04:00 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)

from integrates.custom_exceptions import (
    IndicatorAlreadyUpdated,
    UnavailabilityError,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    findings as findings_model,
)
from integrates.db_model.findings.types import (
    Finding,
    FindingUnreliableIndicatorsToUpdate,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.dynamodb.exceptions import (
    ConditionalCheckFailedException,
)
from integrates.findings.domain import (
    get_total_open_priority,
)
from integrates.organizations.domain import (
    get_all_group_names,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")


@retry_on_exceptions(
    exceptions=(UnavailabilityError,),
    sleep_seconds=10,
)
async def process_finding(loaders: Dataloaders, finding: Finding) -> None:
    total_open_priority = await get_total_open_priority(loaders, finding.id)

    try:
        await findings_model.update_unreliable_indicators(
            current_value=finding.unreliable_indicators._replace(
                total_open_priority=None,
            ),
            group_name=finding.group_name,
            finding_id=finding.id,
            indicators=FindingUnreliableIndicatorsToUpdate(
                total_open_priority=total_open_priority,
            ),
        )
    except (ConditionalCheckFailedException, IndicatorAlreadyUpdated) as ex:
        LOGGER.error("Finding update error - %s - %s", finding.id, str(ex))


async def process_group(group_name: str, progress: float) -> None:
    loaders: Dataloaders = get_new_context()
    all_findings = await loaders.group_findings_all.load(group_name)
    await collect(
        [process_finding(loaders, finding) for finding in all_findings],
        workers=8,
    )

    LOGGER.info(
        "Group processed",
        extra={
            "extra": {
                "group_name": group_name,
                "findings_processed": len(all_findings),
                "progress": round(progress, 2),
            },
        },
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    group_names = sorted(await get_all_group_names(loaders=loaders))
    LOGGER_CONSOLE.info("Processing %s groups...", len(group_names))
    await collect(
        [
            process_group(
                group_name=group_name,
                progress=count / len(group_names),
            )
            for count, group_name in enumerate(group_names)
        ],
        workers=1,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:    %Y-%m-%d at %H:%M:%S UTC")
    LOGGER.info("%s", execution_time)
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER.info("\n%s\n%s\n", execution_time, finalization_time)
