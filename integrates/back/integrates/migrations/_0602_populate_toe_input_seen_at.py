"""
Populate the seen at in the toe inputs state.

Execution Time:    2024-10-10 at 22:43:40 UTC
Finalization Time: 2024-10-10 at 22:49:23 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)
from botocore.exceptions import (
    ConnectTimeoutError,
    ReadTimeoutError,
)

from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    toe_inputs as toe_inputs_model,
)
from integrates.db_model.toe_inputs.types import (
    GroupToeInputsRequest,
    ToeInput,
    ToeInputMetadataToUpdate,
    ToeInputUpdate,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER_CONSOLE = logging.getLogger("console")


@retry_on_exceptions(
    exceptions=(ReadTimeoutError, ConnectTimeoutError),
    sleep_seconds=3,
)
async def process_toe_input(toe_input: ToeInput) -> None:
    state = toe_input.state
    if state.seen_at:
        return

    # Date of introduction of seen at field
    seen_at_entry_date: str = "2021-12-21 00:00:00"
    default_date = datetime_utils.get_from_str(seen_at_entry_date)
    seen_at = state.first_attack_at or state.attacked_at or state.modified_date or default_date

    await toe_inputs_model.update_state(
        ToeInputUpdate(
            current_value=toe_input,
            new_state=toe_input.state._replace(
                seen_at=seen_at,
            ),
            metadata=ToeInputMetadataToUpdate(
                clean_attacked_at=False,
                clean_be_present_until=False,
                clean_first_attack_at=False,
                clean_seen_at=False,
            ),
        ),
    )


async def process_group(loaders: Dataloaders, group_name: str) -> None:
    group_toe_inputs = await loaders.group_toe_inputs.load_nodes(
        GroupToeInputsRequest(group_name=group_name),
    )
    await collect(
        tuple(process_toe_input(toe_input) for toe_input in group_toe_inputs),
        workers=1000,
    )
    LOGGER_CONSOLE.info(
        "Group processed",
        extra={
            "extra": {
                "group_name": group_name,
                "group_toe_inputs": len(group_toe_inputs),
            },
        },
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    all_group_names = sorted(await orgs_domain.get_all_group_names(loaders))
    LOGGER_CONSOLE.info(
        "All group names",
        extra={
            "extra": {
                "total": len(all_group_names),
            },
        },
    )
    for count, group_name in enumerate(all_group_names, start=1):
        LOGGER_CONSOLE.info(
            "Group",
            extra={
                "extra": {
                    "group_name": group_name,
                    "count": count,
                },
            },
        )
        await process_group(loaders, group_name)


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
