"""
Update the author info for the selected vulns.
Commit date should be consistent.

Execution Time: 2025-01-14 at 17:54:30 UTC, extra=None
Finalization Time: 2025-01-14 at 17:57:26 UTC, extra=None
"""

import csv
import logging
import logging.config
from itertools import groupby
from operator import attrgetter
from typing import NamedTuple

from aioextensions import collect
from more_itertools import (
    chunked,
)

from integrates.batch.dal.put import (
    put_action,
)
from integrates.batch.enums import (
    Action,
    IntegratesBatchQueue,
)
from integrates.dataloaders import get_new_context
from integrates.db_model.vulnerabilities.types import Vulnerability, VulnerabilityMetadataToUpdate
from integrates.db_model.vulnerabilities.update import update_metadata
from integrates.migrations.utils import log_time

LOGGER = logging.getLogger("migrations")
MAX_ROOTS_PER_JOB = 100
EMAIL_MODIFIED_BY = "integrates@fluidattacks.com"


class CsvRow(NamedTuple):
    group_name: str
    vuln_id: str


async def clear_vuln_author(vuln: Vulnerability) -> None:
    if vuln.author is None:
        return

    if vuln.author.commit_date < vuln.created_date:
        return

    await update_metadata(
        finding_id=vuln.finding_id,
        root_id=vuln.root_id,
        vulnerability_id=vuln.id,
        metadata=VulnerabilityMetadataToUpdate(author=None),
    )


@log_time(LOGGER)
async def main() -> None:
    csv_rows: list[CsvRow] = []
    with open("data.csv", encoding="utf-8") as file:
        csv_file = csv.reader(file)
        for line in csv_file:
            if "UUID" not in line[0]:
                csv_rows.append(CsvRow(vuln_id=line[0], group_name=line[1]))

    LOGGER.info("Rows on csv: %s", str(len(csv_rows)))

    loaders = get_new_context()
    for group_name, group_rows in groupby(
        sorted(csv_rows, key=attrgetter("group_name")),
        key=attrgetter("group_name"),
    ):
        vuln_ids = list({row.vuln_id for row in group_rows})
        vulns = list(filter(None, await loaders.vulnerability.load_many(vuln_ids)))
        await collect(
            [clear_vuln_author(vuln) for vuln in vulns],
            workers=8,
        )
        root_ids_to_process = list({vuln.root_id for vuln in vulns})
        for chunk in chunked(root_ids_to_process, MAX_ROOTS_PER_JOB):
            result = await put_action(
                action=Action.UPDATE_VULNS_AUTHOR,
                additional_info={"root_ids": list(chunk)},
                entity=group_name,
                queue=IntegratesBatchQueue.SMALL,
                subject=EMAIL_MODIFIED_BY,
            )
            LOGGER.info(
                "QUEUED: %s %s %s",
                group_name,
                str(chunk),
                result.batch_job_id,
            )

        LOGGER.info("Group processed: %s, %s", group_name, str(len(vulns)))


if __name__ == "__main__":
    main()  # type: ignore[unused-coroutine]
