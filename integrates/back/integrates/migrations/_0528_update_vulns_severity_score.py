"""
Populate the cvss_v4 field in vulnerabilities.
Execution Time:    2024-04-18 at 04:11:21 UTC
Finalization Time: 2024-04-18 at 06:14:48 UTC

Second Execution Time:    2024-05-07 at 14:00:32 UTC
Second Finalization Time: 2024-05-07 at 15:29:07 UTC
"""

import contextlib
import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)
from botocore.exceptions import (
    HTTPClientError,
)

from integrates.custom_exceptions import (
    VulnNotFound,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.organizations.domain import (
    get_all_group_names,
)
from integrates.settings import (
    LOGGING,
)
from integrates.vulnerabilities.domain.core import (
    update_severity_score,
)
from integrates.vulnerability_files.utils import (
    get_from_v3,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")


async def process_vuln(loaders: Dataloaders, vuln: Vulnerability) -> None:
    if vuln.severity_score is None or (
        vuln.severity_score and vuln.severity_score.cvss_v4 is not None
    ):
        return

    cvss_v3 = vuln.severity_score.cvss_v3
    cvss_v4 = str(get_from_v3(cvss_v3))

    with contextlib.suppress(VulnNotFound):
        await update_severity_score(loaders, vuln.id, cvss_v3, cvss_v4)

    LOGGER_CONSOLE.info(
        "Vulnerability updated",
        extra={
            "extra": {
                "vuln_id": vuln.id,
                "cvss_v4": cvss_v4,
                "cvss_v3": cvss_v3,
            },
        },
    )


async def process_finding(loaders: Dataloaders, finding: Finding) -> None:
    vulns = await loaders.finding_vulnerabilities_all.load(finding.id)

    await collect([process_vuln(loaders, vuln) for vuln in vulns], workers=16)


@retry_on_exceptions(
    exceptions=(HTTPClientError,),
    sleep_seconds=10,
)
async def process_group(group_name: str, progress: float) -> None:
    loaders = get_new_context()
    all_findings = await loaders.group_findings_all.load(group_name)

    await collect(
        [process_finding(loaders, finding) for finding in all_findings],
        workers=16,
    )

    LOGGER_CONSOLE.info(
        "Group updated",
        extra={
            "extra": {
                "group_name": group_name,
                "progress": str(round(progress, 2)),
            },
        },
    )


async def main() -> None:
    loaders = get_new_context()
    group_names = sorted(await get_all_group_names(loaders=loaders), reverse=True)

    for count, group in enumerate(group_names):
        await process_group(
            group_name=group,
            progress=count / len(group_names),
        )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
