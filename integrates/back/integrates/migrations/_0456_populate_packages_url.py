# type: ignore
"""
Populate field url in toe_packages table
Start Time:    2023-11-21 at 14:38:31 UTC
Finalization Time: 2023-11-21 at 17:18:59 UTC

Start Time:    2023-12-13 at 17:31:45 UTC
Finalization Time: 2023-12-13 at 17:54:49 UTC
"""

import logging
import logging.config
import re
import time
import urllib.parse

from aioextensions import (
    collect,
    run,
)
from packageurl import (
    PackageURL,
)
from packageurl.contrib import (
    purl2url,
)

from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.toe_packages import (
    update_package,
)
from integrates.db_model.toe_packages.types import (
    GroupToePackagesRequest,
    ToePackageMetadataToUpdate,
)
from integrates.organizations.domain import (
    get_all_group_names,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


def has_range_version(version: str) -> bool:
    range_pattern = re.compile(r"^(~|\^|\*|x|>=|<=)\d?")

    return bool(range_pattern.search(version))


def purl_to_url(purl: str, version: str) -> str | None:
    if has_range_version(version):
        return "-"
    try:
        return purl2url.get_repo_url(purl)
    except ValueError as ex:
        LOGGER_CONSOLE.warning(
            "%s",
            ex,
            extra={"extra": {"purl": purl, "version": version}},
        )
        return None


def build_pkg_url(platform: str, name: str, version: str) -> str | None:
    namespace = ""
    try:
        match platform:
            case "MAVEN":
                namespace, name = name.split(":", 1)
                purl = PackageURL(platform, namespace, name, version).to_string()
            case "NPM":
                fields = name.split("/", 1)
                if len(fields) > 1:
                    namespace = fields[0]
                    name = fields[1]
                purl = PackageURL(platform, namespace, name, version).to_string()
            case "PIP":
                purl = PackageURL("pypi", "", name, version).to_string()
            case "GEM":
                purl = PackageURL("rubygems", "", name, version).to_string()
            case "GO":
                purl = PackageURL("golang", "", name, version=f"v{version}").to_string()
            case platform if platform in [
                "NUGET",
                "COMPOSER",
                "CONAN",
                "PUB",
                "CARGO",
                "ERLANG",
                "SWIFT",
                "GITHUBACTIONS",
            ]:
                purl = PackageURL(platform, "", name, version).to_string()
    except ValueError as ex:
        LOGGER_CONSOLE.error("%s", ex)
        LOGGER_CONSOLE.info("name : %s, version: %s", name, version)
        return None
    return urllib.parse.unquote(purl)


async def populate_url(group_name: str) -> None:
    LOGGER_CONSOLE.info("Processing group %s", group_name)
    loaders = get_new_context()
    group_packages_present = await loaders.group_toe_packages.load_nodes(
        GroupToePackagesRequest(group_name=group_name, be_present=True),
    )
    for package in group_packages_present:
        updated_purl = build_pkg_url(package.platform, package.name, package.version)
        updated_url = purl_to_url(updated_purl, package.version) if updated_purl else None
        if package.package_url != updated_purl:
            purl = updated_purl
            url = updated_url
        elif package.url != updated_url:
            purl = None
            url = updated_url
        else:
            purl = None
            url = None

        if not url and not purl:
            continue
        await update_package(
            current_value=package,
            package=ToePackageMetadataToUpdate(purl=purl, url=url),
        )


async def main() -> None:
    loaders = get_new_context()
    groups = await get_all_group_names(loaders)
    await collect(populate_url(group) for group in groups)


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:    %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("%s", execution_time)
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
