"""
Remove git root environments with empty urls fields

Execution Time:    2024-10-29 at 16:59:47 UTC
Finalization Time: 2024-10-29 at 17:05:43 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)

from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model import (
    roots as roots_model,
)
from integrates.db_model.roots.enums import (
    RootEnvironmentUrlStateStatus,
)
from integrates.db_model.roots.types import (
    GroupEnvironmentUrlsRequest,
)
from integrates.organizations.domain import (
    get_all_group_names,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")
EMAIL_INTEGRATES = "integrates@fluidattacks.com"


async def process_group(group_name: str, progress: float) -> None:
    loaders = get_new_context()
    group_roots = await loaders.group_roots.load(group_name)
    if not group_roots:
        return

    group_env_urls = await loaders.group_environment_urls.load(
        GroupEnvironmentUrlsRequest(group_name=group_name),
    )
    env_urls_to_delete = [env_url for env_url in group_env_urls if not env_url.url.strip()]
    if not env_urls_to_delete:
        return

    await collect(
        [
            roots_model.update_root_environment_url_state(
                current_value=url.state,
                root_id=url.root_id,
                url_id=url.id,
                group_name=group_name,
                state=url.state._replace(
                    modified_date=datetime_utils.get_now(),
                    modified_by=EMAIL_INTEGRATES,
                    include=False,
                    status=RootEnvironmentUrlStateStatus.DELETED,
                ),
            )
            for url in env_urls_to_delete
        ],
        workers=4,
    )

    LOGGER_CONSOLE.info(
        "Group updated",
        extra={
            "extra": {
                "group_name": group_name,
                "env_urls_to_delete": len(env_urls_to_delete),
                "progress": str(round(progress, 2)),
            },
        },
    )


async def main() -> None:
    loaders = get_new_context()
    group_names = sorted(await get_all_group_names(loaders=loaders))
    for count, group in enumerate(group_names):
        await process_group(
            group_name=group,
            progress=count / len(group_names),
        )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
