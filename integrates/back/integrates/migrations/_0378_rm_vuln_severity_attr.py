# type: ignore
"""
Remove `severity` attribute on vulnerabilities that have it. This map holds
the floating point weights of the severity vector, but now we'll use the
CVSS:3.1 vector string.

Execution Time:    2023-04-13 at 00:23:47 UTC
Finalization Time: 2023-04-13 at 01:00:26 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)
from boto3.dynamodb.conditions import (
    Attr,
    Key,
)

from integrates.class_types.types import (
    Item,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.types import (
    PrimaryKey,
)
from integrates.organizations.domain import (
    get_all_group_names,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


async def get_finding_vulnerabilities_items(
    finding_id: str,
) -> tuple[Item, ...]:
    primary_key = keys.build_key(
        facet=TABLE.facets["vulnerability_metadata"],
        values={"finding_id": finding_id},
    )
    index = TABLE.indexes["inverted_index"]
    key_structure = index.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.sort_key)
            & Key(key_structure.sort_key).begins_with(primary_key.partition_key)
        ),
        filter_expression=Attr("severity").exists(),
        facets=(TABLE.facets["vulnerability_metadata"],),
        table=TABLE,
        index=index,
    )

    return response.items


async def process_vulnerability_item(item: Item) -> None:
    primary_key = PrimaryKey(partition_key=item["pk"], sort_key=item["sk"])
    key_structure = TABLE.primary_key
    await operations.update_item(
        condition_expression=Attr(key_structure.partition_key).exists(),
        item={"severity": None},
        key=primary_key,
        table=TABLE,
    )


async def process_finding(finding: Finding) -> None:
    items = await get_finding_vulnerabilities_items(finding_id=finding.id)
    if not items:
        return

    await collect(
        tuple(process_vulnerability_item(item) for item in items),
        workers=4,
    )


async def process_group(
    loaders: Dataloaders,
    group_name: str,
    progress: float,
) -> None:
    group_findings = await loaders.group_drafts_and_findings_all.load(group_name)
    if not group_findings:
        return

    await collect(
        tuple(process_finding(finding=finding) for finding in group_findings),
        workers=4,
    )
    LOGGER_CONSOLE.info("Group processed %s %s", group_name, f"{round(progress, 2)!s}")


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    group_names = sorted(await get_all_group_names(loaders))
    LOGGER_CONSOLE.info("%s", f"{group_names=}")
    LOGGER_CONSOLE.info("%s", f"{len(group_names)=}")
    await collect(
        tuple(
            process_group(
                loaders=loaders,
                group_name=group,
                progress=count / len(group_names),
            )
            for count, group in enumerate(group_names)
        ),
        workers=1,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
