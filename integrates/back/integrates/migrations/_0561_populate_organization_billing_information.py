"""
Populate organization billing information field
Execution Time: 2024-07-17 at 01:05:00
Finalization Time: 2024-07-17 at 01:05:01

Execution Time: 2024-09-25 at 00:08:03
Finalization Time: 2024-09-25 at 00:08:06
"""

import logging
import time
from datetime import (
    datetime,
)

import stripe
from aioextensions import (
    collect,
    run,
)
from aiohttp import (
    ClientSession,
    ClientTimeout,
)

from integrates.billing.epayco_customers import (
    BASE_URL,
    _get_customer_epayco,
)
from integrates.billing.subscriptions.treli import (
    update_subscription,
)
from integrates.billing.types import (
    EPaycoCustomer,
    TreliSubscription,
)
from integrates.context import (
    FI_STRIPE_API_KEY,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.groups.enums import (
    GroupStateStatus,
)
from integrates.db_model.organizations import (
    get_all_organizations,
    update_metadata,
)
from integrates.db_model.organizations.types import (
    Organization,
    OrganizationBilling,
    OrganizationMetadataToUpdate,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")

API_VERSION = "2024-06-20"

stripe.api_key = FI_STRIPE_API_KEY
stripe.api_version = API_VERSION


async def populate_billing_information(organization: Organization) -> None:
    if not organization.billing_information:
        return

    if len(organization.billing_information) > 1:
        LOGGER_CONSOLE.error(
            "organization %s has more than one billing information",
            organization.name,
            extra={"billing_information": organization.billing_information},
        )

        return

    loaders = get_new_context()
    org_groups = await loaders.organization_groups.load(organization.id)

    billed_groups = [
        group.name for group in org_groups if group.state.status == GroupStateStatus.ACTIVE
    ]

    if organization.billing_information[0].customer_id.startswith("cus_"):
        customer_stripe = await stripe.Customer.retrieve_async(
            organization.billing_information[0].customer_id,
        )
        created_date = datetime.fromtimestamp(customer_stripe.created)
        customer_email = customer_stripe.email or ""
        customer_id = customer_stripe.id

        await update_metadata(
            metadata=OrganizationMetadataToUpdate(
                billing_information=[
                    OrganizationBilling(
                        billing_email=customer_email,
                        last_modified_by=customer_email,
                        subscription_id=organization.billing_information[0].subscription_id,
                        customer_id=customer_id,
                        modified_at=created_date,
                        billed_groups=billed_groups,
                    ),
                ],
            ),
            organization_id=organization.id,
            organization_name=organization.name,
        )
        LOGGER.info("organization %s updated", organization.name)

    else:
        updated = False

        async with ClientSession(BASE_URL, timeout=ClientTimeout(total=3 * 60)) as session:
            customer_epayco: EPaycoCustomer = await _get_customer_epayco(
                session,
                customer_id=organization.billing_information[0].customer_id,
            )

            created_date = datetime.strptime(customer_epayco.created, "%m/%d/%Y")

        await update_metadata(
            metadata=OrganizationMetadataToUpdate(
                billing_information=[
                    OrganizationBilling(
                        billing_email=customer_epayco.email,
                        last_modified_by=organization.billing_information[0].last_modified_by,
                        subscription_id=organization.billing_information[0].subscription_id,
                        customer_id=customer_epayco.id,
                        modified_at=created_date,
                        billed_groups=billed_groups,
                    ),
                ],
            ),
            organization_id=organization.id,
            organization_name=organization.name,
        )

        await update_subscription(
            data=TreliSubscription.UpdateData(
                subscription_id=int(organization.billing_information[0].subscription_id or 0),
                meta_data=[
                    TreliSubscription.MetadataDict(
                        key="billed_groups",
                        value=", ".join(billed_groups),
                    ),
                ],
            ),
        )

        LOGGER.info(
            "organization %s updated",
            organization.name,
            extra={"updated": updated},
        )


async def main() -> None:
    organizations = await get_all_organizations()
    await collect(
        [
            populate_billing_information(organization)
            for organization in organizations
            if organization.billing_information
        ],
        workers=15,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
