"""
Clear this Git Root status so it can be re-populated with the expected
Machine Executions info

Start Time:    2024-04-16 at 11:52:39 UTC
Finalization Time: 2024-04-16 at 12:00:25 UTC

Start Time:    2024-05-29 at 16:23:00 UTC
Finalization Time: 2024-05-29 at 16:31:15 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)
from boto3.dynamodb.conditions import (
    Attr,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.db_model.roots.types import (
    GitRoot,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger("console")


async def update_git_root_machine(group_name: str, root_id: str) -> None:
    key_structure = TABLE.primary_key
    primary_key = keys.build_key(
        facet=TABLE.facets["git_root_metadata"],
        values={"name": group_name, "uuid": root_id},
    )
    await operations.update_item(
        condition_expression=Attr(key_structure.partition_key).exists(),
        item={"machine": None},
        key=primary_key,
        table=TABLE,
    )


async def process_group(group: Group, progress: float) -> None:
    loaders: Dataloaders = get_new_context()
    git_roots = [
        root
        for root in await loaders.group_roots.load(group.name)
        if isinstance(root, GitRoot) and root.machine
    ]
    if not git_roots:
        return

    await collect(
        [update_git_root_machine(root.group_name, root.id) for root in git_roots],
        workers=8,
    )
    LOGGER.info(
        "Group processed",
        extra={
            "extra": {
                "group_name": group.name,
                "roots_processed": len(git_roots),
                "progress": round(progress, 2),
            },
        },
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    all_groups = await orgs_domain.get_all_groups(loaders=loaders)
    LOGGER.info(
        "All groups",
        extra={
            "extra": {
                "groups_len": len(all_groups),
                "group_names": [group.name for group in all_groups],
            },
        },
    )
    await collect(
        [
            process_group(
                group=group,
                progress=count / len(all_groups),
            )
            for count, group in enumerate(all_groups)
        ],
        workers=1,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:    %Y-%m-%d at %H:%M:%S UTC")
    LOGGER.info("%s", execution_time)
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER.info("\n%s\n%s\n", execution_time, finalization_time)
