"""
Remove open location duplicates

Execution Time:    2024-12-03 at 20:35:17 UTC
Finalization Time: 2024-12-03 at 20:42:33 UTC

Execution Time:    2024-12-04 at 20:30:26 UTC
Finalization Time: 2024-12-04 at 22:29:07 UTC

Execution Time:    2025-01-18 at 00:02:44 UTC
Finalization Time: 2025-01-18 at 00:47:32 UTC

Execution Time:    2025-01-24 at 02:42:17 UTC
Finalization Time: 2025-01-24 at 03:52:11 UTC
"""

import logging
import time

from aioextensions import run

from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateReason,
    VulnerabilityStateStatus,
)
from integrates.db_model.vulnerabilities.types import GroupVulnerabilitiesRequest, Vulnerability
from integrates.organizations.domain import get_all_active_group_names
from integrates.vulnerabilities.domain import remove_vulnerability

LOGGER = logging.getLogger("migrations")


def find_duplicates(items: list[str]) -> list[str]:
    element_count: dict[str, int] = {}
    for item in items:
        if item in element_count:
            element_count[item] += 1
        else:
            element_count[item] = 1
    return [item for item, count in element_count.items() if count > 1]


def get_temporal_identifier(vuln: Vulnerability) -> str:
    return (
        f"{vuln.finding_id}/{vuln.root_id}/{vuln.state.where}/{vuln.state.specific}/{vuln.hash}"
        if vuln.created_by == "machine@fluidattacks.com"
        else f"{vuln.finding_id}/{vuln.root_id}/{vuln.state.where}/{vuln.state.specific}"
    )


def get_vuln_to_keep(vulns: list[Vulnerability]) -> Vulnerability:
    complete_vulns = [vuln for vuln in vulns if vuln.skims_description]
    if complete_vulns:
        return complete_vulns[0]
    return vulns[0]


async def process_duplicates(duplicates: dict[str, list[Vulnerability]]) -> None:
    for duplicate_id, duplicate_vulns in duplicates.items():
        # Sort vulnerabilities by modified date, keeping the latest one
        sorted_vulns = sorted(
            duplicate_vulns, key=lambda vuln: vuln.state.modified_date, reverse=True
        )
        vuln_to_keep = get_vuln_to_keep(sorted_vulns)
        vulns_to_remove = [vuln for vuln in sorted_vulns if vuln.id != vuln_to_keep.id]

        LOGGER.info(
            "Processing duplicate vulnerabilities",
            extra={
                "extra": {
                    "duplicate_id": duplicate_id,
                    "sorted_vulns": [
                        f"{vuln.id}:{vuln.state.modified_date.isoformat()}" for vuln in sorted_vulns
                    ],
                    "vuln_to_keep": vuln_to_keep.id,
                    "vulns_to_remove": [vuln.id for vuln in vulns_to_remove],
                }
            },
        )

        for vuln in vulns_to_remove:
            await remove_vulnerability(
                loaders=get_new_context(),
                finding_id=vuln.finding_id,
                vulnerability_id=vuln.id,
                justification=VulnerabilityStateReason.DUPLICATED,
                email="migrations@fluidattacks.com",
                include_closed_vuln=True,
            )
            LOGGER.info("Removed duplicate vulnerability", extra={"extra": {"vuln_id": vuln.id}})

    LOGGER.info(
        "Finished processing duplicate vulnerabilities",
        extra={"extra": {"duplicate_id": duplicate_id}},
    )


async def process_group(group_name: str) -> None:
    loaders = get_new_context()
    vulns_conn = await loaders.group_vulnerabilities.load(
        GroupVulnerabilitiesRequest(
            group_name=group_name,
            state_status=VulnerabilityStateStatus.VULNERABLE,
        ),
    )

    vulns: list[Vulnerability] = [edge.node for edge in vulns_conn.edges]
    duplicates: dict[str, list[Vulnerability]] = {
        duplicate_id: []
        for duplicate_id in find_duplicates(
            sorted([get_temporal_identifier(vuln) for vuln in vulns])
        )
    }

    if duplicates:
        for vuln in vulns:
            vuln_temporal_identifier = get_temporal_identifier(vuln)
            if vuln_temporal_identifier in duplicates:
                duplicates[vuln_temporal_identifier].append(vuln)

        LOGGER.info(
            "Vulnerability duplicates found in %s group",
            group_name,
            extra={
                "extra": {
                    "duplicates": {
                        key: [vuln.id for vuln in value] for key, value in duplicates.items()
                    }
                }
            },
        )
        await process_duplicates(duplicates)
    else:
        LOGGER.info("Vulnerability duplicates not found in %s group", group_name)


async def main() -> None:
    loaders = get_new_context()
    group_names = sorted(await get_all_active_group_names(loaders))

    for group_name in group_names:
        LOGGER.info("Processing %s group", group_name)
        await process_group(group_name)


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER.info("\n%s\n%s", execution_time, finalization_time)
