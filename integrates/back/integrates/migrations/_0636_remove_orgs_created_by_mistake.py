"""
Remove two organizations created by mistake.
Execution Time: 2024-11-28 at 20:07:47 UTC
Finalization Time: 2024-11-28 at 20:07:57 UTC
"""

import logging
import time

from aioextensions import (
    collect,
    run,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.groups.enums import (
    GroupStateJustification,
)
from integrates.groups import (
    domain as groups_domain,
)
from integrates.organizations import (
    domain as orgs_domain,
)

LOGGER = logging.getLogger(__name__)
MODIFIED_BY = "integrates@fluidattacks.com"


async def _remove_group(
    loaders: Dataloaders,
    group_name: str,
) -> None:
    await groups_domain.remove_group(
        loaders=loaders,
        comments="Scheduled removal for organization.",
        email=MODIFIED_BY,
        group_name=group_name,
        justification=GroupStateJustification.MISTAKE,
        validate_pending_actions=False,
    )


async def _remove_organization(
    *,
    loaders: Dataloaders,
    aws_external_id: str,
    organization_id: str,
    organization_name: str,
) -> None:
    group_names = await orgs_domain.get_group_names(loaders, organization_id)
    await collect(
        [_remove_group(loaders, group_name) for group_name in group_names],
        workers=2,
    )
    await orgs_domain.remove_organization(
        loaders=loaders,
        organization_id=organization_id,
        organization_name=organization_name,
        modified_by=MODIFIED_BY,
        aws_external_id=aws_external_id,
    )
    LOGGER.info("Organization removed %s groups removed: %s", organization_name, group_names)


async def main() -> None:
    loaders = get_new_context()
    organizations = await loaders.organization.load_many(["ciiothers", "testidi"])

    await collect(
        [
            _remove_organization(
                loaders=loaders,
                aws_external_id=organization.state.aws_external_id,
                organization_id=organization.id,
                organization_name=organization.name,
            )
            for organization in organizations
            if organization
        ]
    )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER.info("\n%s\n%s", execution_time, finalization_time)
