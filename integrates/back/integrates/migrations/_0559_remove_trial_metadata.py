"""
Remove trial_metadata for an user
Execution Time:    2024-06-26 at 22:59:38
Finalization Time: 2024-06-26 at 22:59:39
Execution Time:    2024-09-06 at 17:12:28
Finalization Time: 2024-09-06 at 17:12:28
Execution Time:    2024-09-06 at 19:30:54
Finalization Time: 2024-09-06 at 19:30:55
"""

import logging
import time

from aioextensions import (
    run,
)
from boto3.dynamodb.conditions import (
    Key,
)

from integrates.db_model import (
    TABLE,
)
from integrates.db_model.trials.types import (
    TrialMetadataToUpdate,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.types import (
    PrimaryKey,
)
from integrates.settings import (
    LOGGING,
)
from integrates.trials.domain import (
    update_metadata,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")


async def remove(*, email: str) -> None:
    email = email.lower().strip()
    primary_key = keys.build_key(
        facet=TABLE.facets["trial_metadata"],
        values={"email": email},
    )
    key_structure = TABLE.primary_key
    condition_expression = Key(key_structure.partition_key).eq(primary_key.partition_key)
    response = await operations.query(
        condition_expression=condition_expression,
        facets=(TABLE.facets["trial_metadata"],),
        table=TABLE,
    )
    keys_to_delete = set(
        PrimaryKey(
            partition_key=item[TABLE.primary_key.partition_key],
            sort_key=item[TABLE.primary_key.sort_key],
        )
        for item in response.items
    )

    await operations.batch_delete_item(
        keys=tuple(keys_to_delete),
        table=TABLE,
    )


async def main() -> None:
    await update_metadata(
        email="",
        metadata=TrialMetadataToUpdate(completed=True),
    )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
