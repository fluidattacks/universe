"""
Remove unused secrets from environment urls

Execution Time:
Finalization Time:
"""

import logging
import time
from collections.abc import Callable

from aioextensions import (
    collect,
    run,
)
from botocore.exceptions import (
    ClientError,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    roots as roots_model,
)
from integrates.db_model.roots.types import (
    GroupEnvironmentUrlsRequest,
    RootEnvironmentSecretsRequest,
    RootEnvironmentUrl,
    Secret,
)
from integrates.organizations.domain import (
    get_all_active_group_names,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")


def print_execution_time(func: Callable) -> Callable:
    async def wrapper(*args: tuple, **kwargs: dict) -> None:
        start_time = time.strftime("%Y-%m-%d at %H:%M:%S UTC")
        LOGGER.info("Execution Time:    %s", start_time)
        await func(*args, **kwargs)
        end_time = time.strftime("%Y-%m-%d at %H:%M:%S UTC")
        LOGGER.info("Finalization Time: %s", end_time)

    return wrapper


async def do_something(group_name: str) -> bool:
    """Return True if the action was successful, False otherwise."""
    loaders: Dataloaders = get_new_context()
    env_roots: list[RootEnvironmentUrl] = await loaders.group_environment_urls.load(
        GroupEnvironmentUrlsRequest(group_name=group_name),
    )
    try:
        to_remove = []
        for root in env_roots:
            url_id = root.id
            secrets: list[Secret] = await loaders.environment_secrets.load(
                RootEnvironmentSecretsRequest(url_id=url_id, group_name=group_name),
            )
            for secret in secrets:
                if secret.value.strip() == "":
                    to_remove.append((group_name, root.id, secret.key))

        for elem in to_remove:
            LOGGER.info(
                "%s | %s (%s) - Empty secret removed",
                elem[0],
                elem[1],
                elem[2],
            )
            await roots_model.remove_environment_url_secret(elem[0], elem[1], elem[2])

        return True

    except (KeyError, TypeError, ValueError) as ex:
        LOGGER.info("Implementation error", extra={"group_name": group_name, "ex": ex})
        return False

    except ClientError as ex:
        LOGGER.info("Connection error", extra={"group_name": group_name, "ex": ex})
        return False


@print_execution_time
async def main() -> None:
    loaders: Dataloaders = get_new_context()
    groups: list[str] = await get_all_active_group_names(loaders)
    sorted_groups: list[str] = sorted(groups)

    await collect(
        tuple(do_something(group_name=group) for group in sorted_groups),
        workers=1,
    )


if __name__ == "__main__":
    run(main())
