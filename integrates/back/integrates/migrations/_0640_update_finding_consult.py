"""
Update consult content
Execution Time: 2024-12-05 at 03:32:06 UTC
Finalization Time: 2024-12-05 at 04:41:19 UTC
"""

import logging
from datetime import timedelta

from aioextensions import collect
from boto3.dynamodb.conditions import Attr

from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model import TABLE
from integrates.db_model.finding_comments.enums import CommentType
from integrates.db_model.finding_comments.types import FindingComment, FindingCommentsRequest
from integrates.db_model.findings.types import Finding
from integrates.db_model.groups.types import Group
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateReason,
    VulnerabilityStateStatus,
)
from integrates.dynamodb import keys, operations
from integrates.dynamodb.exceptions import ConditionalCheckFailedException
from integrates.finding_comments.domain import get_vuln_nickname
from integrates.migrations.utils import log_time
from integrates.organizations.domain import get_all_groups

LOGGER = logging.getLogger("migrations")


async def remove(
    *,
    finding_id: str,
    consult_id: str,
) -> None:
    primary_key = keys.build_key(
        facet=TABLE.facets["finding_comment"],
        values={
            "id": consult_id,
            "finding_id": finding_id,
        },
    )
    await operations.delete_item(key=primary_key, table=TABLE)


async def update_metadata(
    *,
    finding_id: str,
    consult_id: str,
    new_content: str,
) -> None:
    key_structure = TABLE.primary_key
    primary_key = keys.build_key(
        facet=TABLE.facets["finding_comment"],
        values={
            "id": consult_id,
            "finding_id": finding_id,
        },
    )
    try:
        await operations.update_item(
            condition_expression=Attr(key_structure.partition_key).exists(),
            item={"content": new_content},
            key=primary_key,
            table=TABLE,
        )
    except ConditionalCheckFailedException as exc:
        LOGGER.exception(
            "Failed to update",
            extra={"extra": {"finding_id": finding_id, "consult_id": consult_id, "exc": exc}},
        )


async def process_consult(consult: FindingComment, loaders: Dataloaders) -> None:
    wheres = [
        val.removeprefix("  - ") for val in consult.content.splitlines() if val.startswith("  - ")
    ]
    _finding_vulns = await loaders.finding_vulnerabilities_all.load(consult.finding_id)
    vulns_formatted = await collect([get_vuln_nickname(loaders, vuln) for vuln in _finding_vulns])
    finding_vulns = [
        vuln
        for vuln, formatted in zip(_finding_vulns, vulns_formatted, strict=True)
        if formatted in wheres and vuln.state.status is VulnerabilityStateStatus.SAFE
    ]
    vulns_with_diff_exclusion = [
        vuln
        for vuln in finding_vulns
        if (
            vuln.state.reasons is None
            or VulnerabilityStateReason.ROOT_OR_ENVIRONMENT_DEACTIVATED not in vuln.state.reasons
        )
        and consult.creation_date > vuln.created_date
        and (abs(vuln.state.modified_date - consult.creation_date) > timedelta(days=1))
    ]
    same_where = [
        vuln
        for vuln in finding_vulns
        if vuln.state.reasons
        and VulnerabilityStateReason.ROOT_OR_ENVIRONMENT_DEACTIVATED in vuln.state.reasons
        and consult.creation_date > vuln.created_date
        and (abs(vuln.state.modified_date - consult.creation_date) < timedelta(days=1))
    ]
    same_where_formatted = await collect([get_vuln_nickname(loaders, vuln) for vuln in same_where])
    vulns_with_diff_exclusion_formatted = await collect(
        [get_vuln_nickname(loaders, vuln) for vuln in vulns_with_diff_exclusion]
    )
    vulns_with_diff_exclusion = [
        vuln
        for vuln, where in zip(
            vulns_with_diff_exclusion, vulns_with_diff_exclusion_formatted, strict=True
        )
        if where not in same_where_formatted
    ]
    if vulns_with_diff_exclusion:
        selected_vulns = [
            f"  - {await get_vuln_nickname(loaders, vuln)}" for vuln in vulns_with_diff_exclusion
        ]
        selected_vulns = list(set(selected_vulns))
        new_content = [val for val in consult.content.splitlines() if val not in selected_vulns]
        if len(new_content) == 3:
            content = consult.content.splitlines()
            if (
                new_content[0] == content[0]
                and new_content[-1] == content[-1]
                and new_content[1] == ""
            ):
                LOGGER.info(
                    "Remove",
                    extra={"extra": {"finding_id": consult.finding_id, "consult_id": consult.id}},
                )
                await remove(finding_id=consult.finding_id, consult_id=consult.id)
        else:
            LOGGER.info(
                "Update",
                extra={"extra": {"finding_id": consult.finding_id, "consult_id": consult.id}},
            )
            await update_metadata(
                finding_id=consult.finding_id,
                consult_id=consult.id,
                new_content="\n".join(new_content),
            )


async def process_finding(finding: Finding, loaders: Dataloaders) -> None:
    consults = [
        consult
        for consult in await loaders.finding_comments.load(
            FindingCommentsRequest(comment_type=CommentType.COMMENT, finding_id=finding.id)
        )
        if consult.content.endswith("due to EXCLUSION")
        and consult.content.startswith("Regarding vulnerabilities")
    ]
    await collect([process_consult(consult, loaders) for consult in consults], workers=8)


async def process_group(group: Group) -> None:
    loaders = get_new_context()
    findings = await loaders.group_findings_all.load(group.name)
    await collect([process_finding(finding, loaders) for finding in findings], workers=2)


@log_time(LOGGER)
async def main() -> None:
    all_groups = await get_all_groups(get_new_context())
    await collect(
        [process_group(group) for group in sorted(all_groups, key=lambda x: x.name) if group],
        workers=2,
    )


if __name__ == "__main__":
    main()  # type: ignore[unused-coroutine]
