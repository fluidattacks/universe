# type: ignore
"""
Update `url_type` field for all Environment URLs

CLOUD -> CSPM in all cases that the `url_type` is equal to `CLOUD`

Start Time:        2023-09-07 at 19:49:33 UTC
Finalization Time: 2023-09-07 at 19:54:43 UTC

"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)

from integrates.custom_exceptions import (
    GroupNotFound,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    roots as roots_model,
)
from integrates.db_model.roots.enums import (
    RootEnvironmentUrlType,
)
from integrates.db_model.roots.types import (
    GitRoot,
    Root,
    RootEnvironmentUrl,
    RootEnvironmentUrlToUpdate,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger(__name__)


async def _get_root_environment_urls(loaders: Dataloaders, root: Root) -> list[RootEnvironmentUrl]:
    return await loaders.root_environment_urls.load(root.id)


async def process_root(root_id: str, url: RootEnvironmentUrl) -> None:
    LOGGER_CONSOLE.info("Root: %s", root_id)
    replace_to = RootEnvironmentUrlType.CSPM
    await roots_model.update_root_environment_url(
        root_id=root_id,
        url_id=url.id,
        url=RootEnvironmentUrlToUpdate(url_type=replace_to),
    )


async def process_group(loaders: Dataloaders, group_name: str) -> None:
    group = await loaders.group.load(group_name)
    if not group:
        raise GroupNotFound()

    roots = tuple(
        root for root in await loaders.group_roots.load(group_name) if isinstance(root, GitRoot)
    )

    for root in roots:
        urls = await _get_root_environment_urls(loaders, root)
        if urls:
            for url in urls:
                if url.url_type == RootEnvironmentUrlType.CLOUD:
                    await process_root(root_id=root.id, url=url)


async def main() -> None:
    loaders: Dataloaders = get_new_context()

    all_group_names = sorted(await orgs_domain.get_all_group_names(loaders=loaders))

    await collect(
        tuple(process_group(loaders, group) for group in all_group_names),
        workers=5,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:    %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("%s", execution_time)
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
