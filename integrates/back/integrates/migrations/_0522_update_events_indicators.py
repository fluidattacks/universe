# type: ignore
"""
Add missing closing date to events indicators
Start Time: 2024-04-10 at 21:05:52 UTC
Finalization Time: 2024-04-10 at 21:06:35 UTC
"""

import logging
import logging.config
import time
from datetime import (
    datetime,
)

from aioextensions import (
    collect,
    run,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    events as events_model,
)
from integrates.db_model.events.enums import (
    EventStateStatus,
)
from integrates.db_model.events.types import (
    Event,
    EventRequest,
    EventState,
    EventUnreliableIndicatorsToUpdate,
    GroupEventsRequest,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.dynamodb.exceptions import (
    UnavailabilityError,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


async def get_solving_state(
    loaders: Dataloaders,
    event_id: str,
    group_name: str,
) -> EventState | None:
    historic_states = await loaders.event_historic_state.load(
        EventRequest(event_id=event_id, group_name=group_name),
    )
    for state in sorted(
        historic_states,
        key=lambda state: state.modified_date,
    ):
        if state.status == EventStateStatus.SOLVED:
            return state

    return None


async def get_solving_date(loaders: Dataloaders, event_id: str, group_name: str) -> datetime | None:
    closing_state = await get_solving_state(loaders, event_id, group_name)

    return closing_state.modified_date if closing_state else None


async def get_unsolved_events_without_closing_date(
    loaders: Dataloaders,
    group_name: str,
) -> list[Event]:
    events = await loaders.group_events.load(GroupEventsRequest(group_name=group_name))
    solved: list[Event] = [
        event
        for event in events
        if event.state.status == EventStateStatus.SOLVED and event.solving_date is None
    ]
    return solved


@retry_on_exceptions(
    exceptions=(UnavailabilityError,),
    max_attempts=2,
    sleep_seconds=1,
)
async def update_event_unreliable_indicators(
    *,
    event: Event,
    loaders: Dataloaders,
) -> None:
    closing_date = await get_solving_date(
        loaders=loaders,
        event_id=event.id,
        group_name=event.group_name,
    )
    await events_model.update_unreliable_indicators(
        current_value=event,
        indicators=EventUnreliableIndicatorsToUpdate(
            unreliable_solving_date=closing_date,
            clean_unreliable_solving_date=False,
        ),
    )
    LOGGER_CONSOLE.info(
        "Event unreliable indicators updated",
        extra={
            "extra": {
                "event_id": event.id,
                "group_name": event.group_name,
            },
        },
    )


async def process_group(loaders: Dataloaders, group_name: str) -> None:
    group_solved_events = await get_unsolved_events_without_closing_date(
        loaders=loaders,
        group_name=group_name,
    )

    LOGGER_CONSOLE.info(
        "Group to process",
        extra={
            "extra": {
                "group_name": group_name,
                "group_events_without_closing_date": len(group_solved_events),
            },
        },
    )

    await collect(
        tuple(
            update_event_unreliable_indicators(event=event, loaders=loaders)
            for event in group_solved_events
        ),
        workers=64,
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    all_group_names = sorted(await orgs_domain.get_all_group_names(loaders))
    for count, group_name in enumerate(all_group_names, start=1):
        LOGGER_CONSOLE.info(
            "Group",
            extra={
                "extra": {
                    "group_name": group_name,
                    "count": count,
                },
            },
        )
        await process_group(loaders, group_name)


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:        %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
