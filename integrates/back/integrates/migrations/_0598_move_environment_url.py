# type: ignore
"""
Execution Time:    2024-09-19 at 17:58:10
Finalization Time: 2024-09-19 at 17:58:40

Execution Time:    2024-09-20 at 17:04:01
Finalization Time: 2024-09-20 at 17:06:01
"""

import logging
import logging.config
import time
import uuid

from aioextensions import (
    collect,
    run,
)

from integrates.custom_utils import (
    findings as findings_utils,
)
from integrates.custom_utils import (
    vulnerabilities as vulns_utils,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    findings as findings_model,
)
from integrates.db_model import (
    vulnerabilities as vulns_model,
)
from integrates.db_model.findings.enums import (
    FindingStateStatus,
)
from integrates.db_model.findings.types import (
    Finding,
    FindingState,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateReason,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityRequest,
)
from integrates.roots import (
    domain as roots_domain,
)
from integrates.roots import (
    environments as roots_environments,
)
from integrates.settings import (
    LOGGING,
)
from integrates.unreliable_indicators.enums import (
    EntityDependency,
)
from integrates.unreliable_indicators.operations import (
    update_unreliable_indicators_by_deps,
)
from integrates.vulnerabilities import (
    domain as vulns_domain,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")
EMAIL_INTEGRATES = "integrates@fluidattacks.com"


async def process_vuln(
    *,
    loaders: Dataloaders,
    vuln: Vulnerability,
    target_finding_id: str,
    target_root_id: str,
    target_group_name: str,
) -> str:
    LOGGER.info(
        "Processing vuln",
        extra={
            "extra": {
                "vuln_id": vuln.id,
                "target_finding_id": target_finding_id,
            },
        },
    )
    historic_state = await loaders.vulnerability_historic_state.load(
        VulnerabilityRequest(vulnerability_id=vuln.id, finding_id=vuln.finding_id),
    )
    historic_treatment = await loaders.vulnerability_historic_treatment.load(
        VulnerabilityRequest(vulnerability_id=vuln.id, finding_id=vuln.finding_id),
    )
    historic_verification = await loaders.vulnerability_historic_verification.load(
        VulnerabilityRequest(vulnerability_id=vuln.id, finding_id=vuln.finding_id),
    )
    historic_zero_risk = await loaders.vulnerability_historic_zero_risk.load(
        VulnerabilityRequest(vulnerability_id=vuln.id, finding_id=vuln.finding_id),
    )
    new_id = str(uuid.uuid4())
    await vulns_model.add(
        vulnerability=vuln._replace(
            finding_id=target_finding_id,
            id=new_id,
            state=vuln.state,
            verification=vuln.verification,
            root_id=target_root_id,
            group_name=target_group_name,
        ),
    )
    LOGGER.info(
        "Created new vuln",
        extra={
            "extra": {
                "target_finding_id": target_finding_id,
                "new_id": new_id,
            },
        },
    )
    new_vulnerability = await vulns_domain.get_vulnerability(loaders, new_id)
    await vulns_model.update_historic(
        current_value=new_vulnerability,
        historic=tuple(historic_state) or (vuln.state,),
    )
    new_vulnerability = await vulns_domain.get_vulnerability(loaders, new_id, clear_loader=True)
    await vulns_model.update_new_historic(
        current_value=new_vulnerability,
        historic=tuple(historic_state) or (vuln.state,),
    )
    if historic_treatment:
        new_vulnerability = await vulns_domain.get_vulnerability(loaders, new_id, clear_loader=True)
        await vulns_model.update_historic(
            current_value=new_vulnerability,
            historic=tuple(historic_treatment),
        )
        new_vulnerability = await vulns_domain.get_vulnerability(loaders, new_id, clear_loader=True)
        await vulns_model.update_new_historic(
            current_value=new_vulnerability,
            historic=tuple(historic_treatment),
        )
    if historic_verification:
        new_vulnerability = await vulns_domain.get_vulnerability(loaders, new_id, clear_loader=True)
        await vulns_model.update_historic(
            current_value=new_vulnerability,
            historic=tuple(historic_verification),
        )
        new_vulnerability = await vulns_domain.get_vulnerability(loaders, new_id, clear_loader=True)
        await vulns_model.update_new_historic(
            current_value=new_vulnerability,
            historic=tuple(historic_verification),
        )
    if historic_zero_risk:
        new_vulnerability = await vulns_domain.get_vulnerability(loaders, new_id, clear_loader=True)
        await vulns_model.update_historic(
            current_value=new_vulnerability,
            historic=tuple(historic_zero_risk),
        )
        new_vulnerability = await vulns_domain.get_vulnerability(loaders, new_id, clear_loader=True)
        await vulns_model.update_new_historic(
            current_value=new_vulnerability,
            historic=tuple(historic_zero_risk),
        )
    await vulns_utils.close_vulnerability(
        vulnerability=vuln,
        modified_by=EMAIL_INTEGRATES,
        loaders=loaders,
        closing_reason=VulnerabilityStateReason.ROOT_MOVED_TO_ANOTHER_GROUP,
    )
    LOGGER.info(
        "Old vuln closed by exclusion",
        extra={
            "extra": {
                "finding_id": vuln.finding_id,
                "vuln_id": vuln.id,
            },
        },
    )
    return new_id


async def _process_target_finding(
    loaders: Dataloaders,
    target_group_name: str,
    source_finding: Finding,
) -> Finding:
    target_group_findings = await loaders.group_findings_all.load(target_group_name)

    if target_group_findings:
        target_finding = next(
            (
                finding
                for finding in target_group_findings
                if finding.state.status and finding.title == source_finding.title
            ),
            None,
        )

        if target_finding:
            return target_finding

    if source_finding.creation:
        initial_state = FindingState(
            modified_by=source_finding.creation.modified_by,
            modified_date=source_finding.creation.modified_date,
            source=source_finding.creation.source,
            status=FindingStateStatus.CREATED,
        )

    target_finding = Finding(
        attack_vector_description=(source_finding.attack_vector_description),
        description=source_finding.description,
        group_name=target_group_name,
        id=str(uuid.uuid4()),
        state=initial_state or source_finding.state,
        recommendation=source_finding.recommendation,
        requirements=source_finding.requirements,
        severity_score=source_finding.severity_score,
        title=source_finding.title,
        threat=source_finding.threat,
        unfulfilled_requirements=source_finding.unfulfilled_requirements,
    )

    LOGGER_CONSOLE.info(
        "Finding created",
        extra={
            "extra": {
                "finding_title": target_finding.title,
                "finding_id": target_finding.id,
            },
        },
    )

    await findings_model.add(finding=target_finding)

    return target_finding


async def _process_finding(
    loaders: Dataloaders,
    source_finding_id: str,
    target_root_id: str,
    target_group_name: str,
    vulns: tuple[Vulnerability, ...],
) -> None:
    LOGGER.info(
        "Processing finding",
        extra={
            "extra": {
                "source_finding_id": source_finding_id,
                "vulns": len(vulns),
            },
        },
    )
    source_finding = await loaders.finding.load(source_finding_id)
    if source_finding is None or findings_utils.is_deleted(source_finding):
        LOGGER.info(
            "Not found",
            extra={"extra": {"finding_id": source_finding_id}},
        )
        return

    target_finding = await _process_target_finding(loaders, target_group_name, source_finding)

    target_vuln_ids = await collect(
        tuple(
            process_vuln(
                loaders=loaders,
                vuln=vuln,
                target_finding_id=target_finding.id,
                target_root_id=target_root_id,
                target_group_name=target_group_name,
            )
            for vuln in vulns
            if (
                vuln.state.reasons is None
                or (
                    vuln.state.reasons
                    and VulnerabilityStateReason.EXCLUSION not in vuln.state.reasons
                )
            )
        ),
        workers=32,
    )
    LOGGER.info(
        "Updating finding indicators",
        extra={
            "extra": {
                "source_finding_id": source_finding_id,
                "target_finding_id": target_finding.id,
            },
        },
    )
    await collect(
        (
            update_unreliable_indicators_by_deps(
                EntityDependency.move_root,
                finding_ids=[source_finding_id],
                vulnerability_ids=[vuln.id for vuln in vulns],
            ),
            update_unreliable_indicators_by_deps(
                EntityDependency.move_root,
                finding_ids=[target_finding.id],
                vulnerability_ids=list(target_vuln_ids),
            ),
        ),
    )


def _get_vulns_by_finding_id(vulns: tuple[Vulnerability, ...]) -> dict[str, list[Vulnerability]]:
    vulns_by_finding_id: dict[str, list[Vulnerability]] = {}
    for vuln in vulns:
        if vuln.finding_id not in vulns_by_finding_id:
            vulns_by_finding_id[vuln.finding_id] = []
        vulns_by_finding_id[vuln.finding_id].append(vuln)
    return vulns_by_finding_id


async def _process_environment(
    *,
    loaders: Dataloaders,
    source_root_id: str,
    target_root_id: str,
    source_group_name: str,
    target_group_name: str,
    environment_url_id: str,
) -> None:
    environment_url = await roots_domain.get_environment_url(
        loaders,
        source_root_id,
        source_group_name,
        environment_url_id,
    )
    if not environment_url:
        return
    use_connection = {
        "use_egress": environment_url.state.use_egress,
        "use_vpn": environment_url.state.use_vpn,
        "use_ztna": environment_url.state.use_ztna,
    }
    await roots_environments.add_root_environment_url(
        loaders=loaders,
        group_name=target_group_name,
        url=environment_url.url,
        root_id=target_root_id,
        url_type=environment_url.state.url_type,
        user_email=environment_url.state.modified_by,
        should_notify=False,
        cloud_type=environment_url.state.cloud_name,
        use_connection=use_connection,
    )
    root_vulnerabilities = await loaders.root_vulnerabilities.load(source_root_id)
    root_vulnerabilities = tuple(
        root_vulnerability
        for root_vulnerability in root_vulnerabilities
        if root_vulnerability.state.where.startswith(environment_url.url)
    )

    vulns_by_finding_id = _get_vulns_by_finding_id(root_vulnerabilities)

    for finding_id, vulns in vulns_by_finding_id.items():
        await _process_finding(
            loaders=loaders,
            source_finding_id=finding_id,
            target_root_id=target_root_id,
            target_group_name=target_group_name,
            vulns=tuple(vulns),
        )

    await roots_domain.remove_environment_url_id(
        loaders=loaders,
        root_id=source_root_id,
        url_id=environment_url_id,
        user_email=environment_url.state.modified_by,
        group_name=environment_url.group_name,
        should_notify=False,
    )


async def main() -> None:
    loaders = get_new_context()

    await _process_environment(
        loaders=loaders,
        source_root_id="179389bc-f3c1-456b-babb-bee399c5515b",
        target_root_id="123ce365-8491-455a-ab70-025d775190cc",
        source_group_name="conley",
        target_group_name="enfield",
        environment_url_id="93f5e6343db9439fa5e8546dfac07149f0bdd313",
    )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
