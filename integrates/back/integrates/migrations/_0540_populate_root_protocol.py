"""
Check all GitRoots in the system and reports urls using HTTP.

Start Time:        2024-04-23 at 00:57:20 UTC
Finalization Time: 2024-04-23 at 01:05:50 UTC
"""

import asyncio
import logging
import logging.config
import tempfile
import time

import fluidattacks_core.git as git_utils
from aioextensions import (
    collect,
    run,
)
from aiohttp.client_exceptions import (
    ClientConnectorError,
    ClientOSError,
    ClientPayloadError,
    ServerDisconnectedError,
    ServerTimeoutError,
)
from botocore.exceptions import (
    ClientError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.credentials.types import (
    CredentialsRequest,
    HttpsSecret,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.db_model.roots.enums import (
    RootStatus,
)
from integrates.db_model.roots.types import (
    GitRoot,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.dynamodb.exceptions import (
    UnavailabilityError,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger("console")
NETWORK_ERRORS = (
    asyncio.TimeoutError,
    ClientConnectorError,
    ClientError,
    ClientOSError,
    ClientPayloadError,
    ConnectionError,
    ConnectionResetError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
    ServerDisconnectedError,
    ServerTimeoutError,
    UnavailabilityError,
)


@retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=5,
)
async def check_if_tls_is_supported(root: GitRoot) -> bool:
    loaders: Dataloaders = get_new_context()

    group = await loaders.group.load(root.group_name)
    if not group:
        return False

    credential = await loaders.credentials.load(
        CredentialsRequest(
            id=root.state.credential_id or "",
            organization_id=group.organization_id,
        ),
    )
    if not credential or not isinstance(credential.secret, HttpsSecret):
        return False

    url = root.state.url.replace("http://", "https://")

    with tempfile.TemporaryDirectory(
        prefix=f"integrates_clone_{url.split('//')[1].split('/')[0]}_",
        ignore_cleanup_errors=True,
    ) as temp_dir:
        _, stderr = await git_utils.clone(
            repo_url=url,
            repo_branch=root.state.branch,
            user=credential.secret.user,
            password=credential.secret.password,
            temp_dir=temp_dir,
        )

    if stderr is not None:
        LOGGER.info(
            "Impossible to clone: %s,%s,%s,%s,%s,no",
            root.id,
            group.organization_id,
            root.state.modified_by,
            group.name,
            url,
        )
        return False

    LOGGER.info(
        "Root accessed via HTTPS: %s,%s,%s,%s,%s,yes",
        root.id,
        group.organization_id,
        root.state.modified_by,
        group.name,
        url,
    )
    return True


@retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=5,
)
async def process_group(group: Group, progress: float) -> None:
    loaders: Dataloaders = get_new_context()
    roots = await loaders.group_roots.load(group.name)
    git_roots = [
        root
        for root in roots
        if isinstance(root, GitRoot)
        and root.state.status == RootStatus.ACTIVE
        and root.state.url.startswith("http://")
    ]
    await collect(
        [check_if_tls_is_supported(root) for root in git_roots],
        workers=8,
    )
    LOGGER.info(
        "Group processed",
        extra={
            "extra": {
                "group": group.name,
                "organization_id": group.organization_id,
                "total_roots": len(roots),
                "total_git_roots": len(git_roots),
                "progress": progress,
            },
        },
    )


@retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=5,
)
async def main() -> None:
    loaders: Dataloaders = get_new_context()
    all_groups = await orgs_domain.get_all_groups(loaders=loaders)
    await collect(
        [
            process_group(
                group=group,
                progress=count / len(all_groups),
            )
            for count, group in enumerate(all_groups)
        ],
        workers=1,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:    %Y-%m-%d at %H:%M:%S UTC")
    LOGGER.info("%s", execution_time)
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER.info("\n%s\n%s\n", execution_time, finalization_time)
