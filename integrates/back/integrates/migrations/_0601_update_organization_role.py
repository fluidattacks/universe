"""
Update organization role
Execution Time:    2024-10-04 at 05:26:49 UTC
Finalization Time: 2024-10-04 at 05:28:40 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    run,
)

from integrates.authz.validations import (
    FLUIDATTACKS_EMAIL_SUFFIX,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.group_access.domain import (
    EMAIL_INTEGRATES,
)
from integrates.organizations.domain import (
    iterate_organizations,
    update_stakeholder_role,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")


async def main() -> None:
    loaders = get_new_context()
    async for organization in iterate_organizations():
        orgs_accesses = await loaders.organization_stakeholders_access.load(organization.id)
        for organization_access in orgs_accesses:
            if (
                organization_access.state.role == "resourcer"
                and not organization_access.email.endswith(FLUIDATTACKS_EMAIL_SUFFIX)
            ):
                await update_stakeholder_role(
                    loaders=loaders,
                    user_email=organization_access.email,
                    organization_id=organization.id,
                    organization_access=organization_access,
                    new_role="user",
                    modified_by=EMAIL_INTEGRATES,
                )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
