"""Remove items from deprecated facet `machine_git_root_execution`"""

import io
import json
import logging
import time
from itertools import (
    chain,
)
from pathlib import (
    Path,
)

from aioextensions import (
    collect,
    run,
)
from boto3.dynamodb.conditions import (
    Key,
)

from integrates.class_types.types import (
    Item,
)
from integrates.context import (
    FI_ENVIRONMENT,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.organizations.get import (
    get_all_organizations,
)
from integrates.db_model.organizations.types import (
    Organization,
)
from integrates.dynamodb import (
    keys,
)
from integrates.dynamodb import (
    operations as dynamodb_operations,
)
from integrates.dynamodb.types import (
    Facet,
    PrimaryKey,
)
from integrates.s3.resource import (
    get_client,
)

LOGGER = logging.getLogger("console")

FACET_NAME = "machine_git_root_execution"
FACET = TABLE.facets[FACET_NAME]
ORGS_ITER_BATCH_SIZE = 50
PROGRESS_FILE = "remove_machine_git_root_execution.json"
TARGET_BUCKET = "integrates.dev"
S3_FOLDER = "migrations_progress"


async def _fetch_progress_local() -> tuple[list[str], list[str]]:
    if not Path(PROGRESS_FILE).exists():
        with open(PROGRESS_FILE, "w", encoding="utf-8") as file:
            file.write("{}")
        return [], []
    with open(PROGRESS_FILE, encoding="utf-8") as file:
        try:
            data = json.load(file)
            if not data:
                return [], []
            processed_orgs = data["processed_orgs"]
            missing_orgs = data["missing_orgs"]

            return processed_orgs, missing_orgs
        except json.JSONDecodeError:
            return [], []


async def _fetch_progress_s3() -> tuple[list[str], list[str]]:
    processed_orgs: list[str] = []
    missing_orgs: list[str] = []
    client = await get_client()
    try:
        object_stream = await client.get_object(
            Bucket=TARGET_BUCKET,
            Key=f"{S3_FOLDER}/{PROGRESS_FILE}",
        )
        async with object_stream["Body"] as stream:
            content = await stream.read()

            data = json.loads(content.decode("utf-8"))
            if not data:
                return [], []
            processed_orgs = data["processed_orgs"]
            missing_orgs = data["missing_orgs"]
            return processed_orgs, missing_orgs
    except json.JSONDecodeError:
        return [], []


async def _store_progress_local(processed_orgs: list[str], missing_orgs: list[str]) -> None:
    data = {
        "processed_orgs": processed_orgs,
        "missing_orgs": missing_orgs,
    }
    with open(PROGRESS_FILE, "w", encoding="utf-8") as file:
        file.write(json.dumps(data, indent=4))


async def _store_progress_s3(processed_orgs: list[str], missing_orgs: list[str]) -> None:
    client = await get_client()
    data = {
        "processed_orgs": processed_orgs,
        "missing_orgs": missing_orgs,
    }
    await client.upload_fileobj(
        io.BytesIO(json.dumps(data).encode("utf-8")),
        TARGET_BUCKET,
        f"{S3_FOLDER}/{PROGRESS_FILE}",
    )


async def _get_machine_git_root_executions(
    root_id: str,
) -> tuple[Item, ...]:
    simplified_facet = Facet(
        attrs=("pk", "sk"),
        pk_alias=FACET.pk_alias,
        sk_alias=FACET.sk_alias,
    )
    primary_key = keys.build_key(
        facet=simplified_facet,
        values={"uuid": root_id},
    )
    key_structure = TABLE.primary_key
    response = await dynamodb_operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with("MACHINE#")
        ),
        facets=(simplified_facet,),
        table=TABLE,
        index=None,
    )

    return response.items


async def _delete_machine_git_root_executions(
    root_ids: list[str],
) -> int:
    target_items = tuple(
        chain.from_iterable(
            await collect(
                [_get_machine_git_root_executions(root_id) for root_id in root_ids],
                workers=2,
            ),
        ),
    )
    if not target_items:
        return 0

    await dynamodb_operations.batch_delete_item(
        keys=tuple(
            PrimaryKey(
                partition_key=item["pk"],
                sort_key=item["sk"],
            )
            for item in target_items
        ),
        table=TABLE,
    )
    return len(target_items)


async def _delete_machine_git_root_execution_for_group(
    loaders: Dataloaders,
    group_name: str,
) -> int:
    roots = await loaders.group_roots.load(group_name)
    if not roots:
        LOGGER.info(
            "No items found for facet %s in group %s",
            FACET_NAME,
            group_name,
        )
        return 0
    root_ids = [root.id for root in roots]

    deleted = await _delete_machine_git_root_executions(root_ids)
    LOGGER.info(
        "Deleted %d items from facet %s in group %s",
        deleted,
        FACET_NAME,
        group_name,
    )
    return deleted


async def _delete_machine_git_root_execution_for_org(
    loaders: Dataloaders,
    org: Organization,
) -> int:
    LOGGER.info("Processing org %s", org.name)
    deleted_items_for_org = 0
    groups = await loaders.organization_groups.load(org.id)
    for group in groups:
        deleted = await _delete_machine_git_root_execution_for_group(loaders, group.name)
        deleted_items_for_org += deleted
    LOGGER.info(
        "Deleted %d items from facet %s in org %s",
        deleted_items_for_org,
        FACET_NAME,
        org.name,
    )
    return deleted_items_for_org


async def main() -> None:
    loaders: Dataloaders = get_new_context()

    all_orgs = await get_all_organizations()

    fetchers = {
        "development": _fetch_progress_local,
        "production": _fetch_progress_s3,
    }

    persisters = {
        "development": _store_progress_local,
        "production": _store_progress_s3,
    }

    fetch_progress = fetchers.get(FI_ENVIRONMENT, _fetch_progress_local)

    processed_orgs, missing_orgs = await fetch_progress()

    if not missing_orgs and not processed_orgs:
        LOGGER.info("No progress found. Starting from beginning")
        missing_orgs = [org.name for org in all_orgs]
    else:
        LOGGER.info("Skipping %d processed orgs", len(processed_orgs))

    orgs_batch = [org for org in all_orgs if org.name in missing_orgs][:ORGS_ITER_BATCH_SIZE]

    if not orgs_batch:
        LOGGER.info("No more orgs to process. Exiting")
        return

    LOGGER.info("Processing orgs: %s", [org.name for org in orgs_batch])

    total = list(
        await collect(
            [_delete_machine_git_root_execution_for_org(loaders, org) for org in orgs_batch],
            workers=2,
        ),
    )
    LOGGER.info(
        "Deleted %d items from facet %s in total",
        sum(total),
        FACET_NAME,
    )

    persist_progress = persisters.get(FI_ENVIRONMENT, _store_progress_local)
    all_org_names = [org.name for org in all_orgs]
    processed_orgs.extend([org.name for org in orgs_batch])

    await persist_progress(
        processed_orgs=processed_orgs,
        missing_orgs=[org for org in all_org_names if org not in processed_orgs],
    )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER.info("\n%s\n%s", execution_time, finalization_time)
