"""
Remove vulnerabilities with non deleted status from deleted findings

Execution Time:    2023-09-07 at 13:20:30 UTC
Finalization Time: 2023-09-07 at 13:25:36 UTC
"""

import csv
import logging
import logging.config
import time
from itertools import (
    chain,
)

from aioextensions import (
    collect,
    run,
)

from integrates.custom_utils.filter_vulnerabilities import (
    filter_non_deleted,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.findings.enums import (
    FindingStateStatus,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateReason,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)
from integrates.vulnerabilities import (
    domain as vulns_domain,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")
EMAIL_INTEGRATES = "integrates@fluidattacks.com"


async def process_finding(loaders: Dataloaders, finding: Finding) -> list[list[str]]:
    vulns_non_deleted = filter_non_deleted(await loaders.finding_vulnerabilities.load(finding.id))

    await collect(
        [
            vulns_domain.remove_vulnerability(
                loaders=loaders,
                finding_id=vuln.finding_id,
                vulnerability_id=vuln.id,
                justification=VulnerabilityStateReason.EXCLUSION,
                email=EMAIL_INTEGRATES,
                include_closed_vuln=True,
            )
            for vuln in vulns_non_deleted
        ],
        workers=32,
    )

    LOGGER_CONSOLE.info(
        "%s %s %s",
        f"{finding.group_name=}",
        f"{finding.id=}",
        f"{len(vulns_non_deleted)=}",
    )

    return [
        [
            finding.group_name,
            finding.id,
            finding.state.status.value,
            vuln.id,
            vuln.state.status.value,
        ]
        for vuln in vulns_non_deleted
    ]


async def process_group(
    *,
    loaders: Dataloaders,
    group_name: str,
    progress: float,
) -> None:
    deleted_findings = [
        finding
        for finding in await loaders.group_findings_all.load(group_name)
        if finding.state.status == FindingStateStatus.DELETED
    ]
    if not deleted_findings:
        return

    results = chain.from_iterable(
        await collect(
            [process_finding(loaders=loaders, finding=finding) for finding in deleted_findings],
            workers=8,
        ),
    )
    with open("0432.csv", "a+", encoding="utf-8") as handler:
        writer = csv.writer(handler)
        writer.writerows(results)

    LOGGER_CONSOLE.info(
        "Group processed %s %s %s",
        group_name,
        f"{round(progress, 2)!s}",
        f"{len(deleted_findings)=}",
    )


async def main() -> None:
    loaders = get_new_context()
    group_names = sorted(await orgs_domain.get_all_active_group_names(loaders))
    LOGGER_CONSOLE.info("%s", f"{group_names=}")
    LOGGER_CONSOLE.info("%s", f"{len(group_names)=}")
    for count, group_name in enumerate(group_names):
        await process_group(
            loaders=loaders,
            group_name=group_name,
            progress=count / len(group_names),
        )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
