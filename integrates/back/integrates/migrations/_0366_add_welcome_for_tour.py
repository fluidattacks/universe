"""
Add welcome tour attribute to stakeholders

Execution Time:    2023-02-27 at 20:51:06 UTC
Finalization Time: 2023-02-27 at 20:51:22 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)
from boto3.dynamodb.conditions import (
    Attr,
)

from integrates.db_model import (
    TABLE,
)
from integrates.db_model.stakeholders import (
    get_all_stakeholders,
)
from integrates.db_model.stakeholders.types import (
    Stakeholder,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


async def process_stakeholder(stakeholder: Stakeholder, progress: float) -> None:
    LOGGER_CONSOLE.info(
        "Working on %s progress: %s",
        stakeholder.email,
        f"{round(progress, 2)}",
    )

    stakeholder_tour = stakeholder.tours._asdict() | {"welcome": True}

    key_structure = TABLE.primary_key
    primary_key = keys.build_key(
        facet=TABLE.facets["stakeholder_metadata"],
        values={
            "email": stakeholder.email,
        },
    )

    condition_expression = Attr(key_structure.partition_key).exists()
    await operations.update_item(
        condition_expression=condition_expression,
        item={"tours": stakeholder_tour},
        key=primary_key,
        table=TABLE,
    )


async def main() -> None:
    all_stakeholders = await get_all_stakeholders()

    LOGGER_CONSOLE.info("%s", f"{len(all_stakeholders)=}")
    await collect(
        tuple(
            process_stakeholder(
                stakeholder=stakeholder,
                progress=count / len(all_stakeholders),
            )
            for count, stakeholder in enumerate(all_stakeholders)
        ),
        workers=100,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
