"""
Execution Time:    2024-12-12 at 02:54:51 UTC
Finalization Time: 2024-12-12 at 02:56:35 UTC
Execution Time:    2024-12-12 at 13:37:09 UTC
Finalization Time: 2024-12-12 at 13:47:12 UTC
Execution Time:    2024-12-18 at 16:14:44 UTC
Finalization Time: 2024-12-18 at 16:17:57 UTC
Execution Time:    2024-12-18 at 17:40:18 UTC
Finalization Time: 2024-12-18 at 17:46:39 UTC
Execution Time:    2024-12-23 at 13:42:05 UTC
Finalization Time: 2024-12-23 at 13:43:51 UTC
Execution Time:    2024-12-23 at 17:50:53 UTC
Finalization Time: 2024-12-23 at 17:51:00 UTC
Update url in git roots
"""

import logging
from contextlib import suppress

import aiohttp
from aioextensions import collect
from fluidattacks_core.git import InvalidParameter as GitInvalidParameter
from fluidattacks_core.git.https_utils import get_redirected_url
from fluidattacks_core.http.validations import HTTPValidationError

from integrates.custom_exceptions import InvalidAuthorization, InvalidGitCredentials, InvalidUrl
from integrates.custom_utils.datetime import get_utc_now
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model import roots as roots_model
from integrates.db_model.credentials.types import (
    Credentials,
    HttpsPatSecret,
    HttpsSecret,
    OauthAzureSecret,
    OauthBitbucketSecret,
    OauthGithubSecret,
    OauthGitlabSecret,
)
from integrates.db_model.groups.types import Group
from integrates.db_model.roots.enums import RootStatus
from integrates.db_model.roots.types import GitRoot
from integrates.decorators import retry_on_exceptions
from integrates.migrations.utils import log_time
from integrates.oauth.common import get_credential_token
from integrates.organizations.domain import get_all_groups
from integrates.roots.utils import ls_remote_root

LOGGER = logging.getLogger("migrations")


async def __get_redirected_url(
    root: GitRoot,
    cred: Credentials,
    loaders: Dataloaders,
) -> str | None:
    try:
        token = await get_credential_token(
            loaders=loaders,
            credential=cred,
        )
        return await ___get_redirected_url(
            repo_url=root.state.url,
            token=token,
        )
    except (InvalidAuthorization, InvalidGitCredentials):
        return None
    except GitInvalidParameter:
        return None


@retry_on_exceptions(
    exceptions=(aiohttp.ClientError, TimeoutError), sleep_seconds=5, max_attempts=3
)
async def ___get_redirected_url(
    repo_url: str,
    user: str | None = None,
    password: str | None = None,
    token: str | None = None,
    is_pat: bool = False,
) -> str | None:
    try:
        return await get_redirected_url(
            url=repo_url,
            user=user,
            password=password,
            token=token,
            is_pat=is_pat,
        )
    except (
        ValueError,
        HTTPValidationError,
    ):
        return None


async def _get_redirected_url(
    root: GitRoot,
    cred: Credentials | None,
    loaders: Dataloaders,
) -> str | None:
    repo_url = root.state.url
    if cred is None:
        if repo_url.startswith("http"):
            return await ___get_redirected_url(
                repo_url=repo_url,
            )
    elif isinstance(cred.secret, HttpsSecret):
        return await ___get_redirected_url(
            repo_url=repo_url,
            user=cred.secret.user,
            password=cred.secret.password,
        )
    elif isinstance(cred.secret, HttpsPatSecret):
        return await ___get_redirected_url(
            repo_url=repo_url,
            token=cred.secret.token,
            is_pat=cred.state.is_pat,
        )
    elif isinstance(
        cred.secret,
        (
            OauthGithubSecret,
            OauthAzureSecret,
            OauthGitlabSecret,
            OauthBitbucketSecret,
        ),
    ):
        return await __get_redirected_url(root=root, cred=cred, loaders=loaders)

    return None


async def _format_url(
    root: GitRoot, credentials: dict[str, Credentials], loaders: Dataloaders
) -> None:
    if (
        root.state.credential_id in credentials
        and root.state.url.startswith("http")
        and root.state.status == RootStatus.ACTIVE
        and not root.state.use_egress
        and not root.state.use_vpn
        and not root.state.use_ztna
    ):
        with suppress(InvalidUrl):
            if (
                await ls_remote_root(
                    root=root, cred=credentials[root.state.credential_id], loaders=loaders
                )
            )[0]:
                with suppress(TimeoutError):
                    redirected_url = await _get_redirected_url(
                        root=root,
                        cred=credentials[root.state.credential_id],
                        loaders=get_new_context(),
                    )
                    if (
                        redirected_url is not None
                        and redirected_url.removesuffix("/") != root.state.url
                    ):
                        await update_root(root, redirected_url.removesuffix("/"))


async def process_group(group: Group) -> None:
    loaders = get_new_context()
    _credentials = await loaders.organization_credentials.load(group.organization_id)
    credentials = {credential.id: credential for credential in _credentials}
    git_roots = [
        root for root in await loaders.group_roots.load(group.name) if isinstance(root, GitRoot)
    ]

    await collect([_format_url(root, credentials, loaders) for root in git_roots], workers=4)


async def update_root(root: GitRoot, new_url: str) -> None:
    await roots_model.update_root_state(
        current_value=root.state,
        group_name=root.group_name,
        root_id=root.id,
        state=root.state._replace(
            url=new_url, modified_date=get_utc_now(), modified_by="integrates@fluidattacks.com"
        ),
    )


@log_time(LOGGER)
async def main() -> None:
    _all_groups = await get_all_groups(get_new_context())
    all_groups = sorted(_all_groups, key=lambda x: x.name, reverse=True)
    await collect(
        [process_group(group) for group in all_groups],
        workers=1,
    )


if __name__ == "__main__":
    main()  # type: ignore[unused-coroutine]
