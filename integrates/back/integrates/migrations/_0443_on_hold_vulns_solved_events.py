"""
Resolve ON_HOLD verification status for vulnerabilities
for which the event is already solved.

Start Time:         2023-09-23 at 15:20:00 UTC
Finalization Time:  2023-09-18 at 15:50:00 UTC
"""

from itertools import (
    chain,
)

from aioextensions import (
    collect,
    run,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.events.types import (
    Event,
    GroupEventsRequest,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateReason,
    VulnerabilityStateStatus,
    VulnerabilityVerificationStatus,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
)
from integrates.findings import (
    domain as findings_domain,
)
from integrates.organizations import (
    domain as orgs_domain,
)


def _get_safe_and_vulnerable(
    vulns: list[Vulnerability],
) -> tuple[dict[str, list[Vulnerability]], dict[str, list[Vulnerability]]]:
    safe: dict[str, list[Vulnerability]] = {}
    vulnerable: dict[str, list[Vulnerability]] = {}
    for vuln in vulns:
        if vuln.state.status == VulnerabilityStateStatus.SAFE:
            if vuln.finding_id not in safe:
                safe[vuln.finding_id] = [vuln]
            else:
                safe[vuln.finding_id].append(vuln)
        elif vuln.state.status == VulnerabilityStateStatus.VULNERABLE:
            if vuln.finding_id not in vulnerable:
                vulnerable[vuln.finding_id] = [vuln]
            else:
                vulnerable[vuln.finding_id].append(vuln)
    return safe, vulnerable


async def process_group(loaders: Dataloaders, group: str, solved_events: list[Event]) -> None:
    if not solved_events:
        return

    on_hold_vulnerabilities = [
        vuln
        for vuln in list(
            chain.from_iterable(
                await loaders.event_vulnerabilities_loader.load_many(
                    [event.id for event in solved_events],
                ),
            ),
        )
        if vuln.state.status != VulnerabilityStateStatus.DELETED
        and vuln.verification
        and vuln.verification.status == VulnerabilityVerificationStatus.ON_HOLD
    ]

    if not on_hold_vulnerabilities:
        return

    print(
        f"{len(on_hold_vulnerabilities)} "
        f"vulnerabilities on hold found on group {group}\n\t"
        "\n\t".join([f"{vuln.id} - {vuln.state.status.value}" for vuln in on_hold_vulnerabilities]),
    )

    safe: dict[str, list[Vulnerability]] = {}
    vulnerable: dict[str, list[Vulnerability]] = {}
    safe, vulnerable = _get_safe_and_vulnerable(on_hold_vulnerabilities)

    if vulnerable:
        await collect(
            (
                findings_domain.request_vulnerabilities_verification(
                    loaders=loaders,
                    finding_id=finding_id,
                    user_info={
                        "first_name": "Andrés",
                        "last_name": "Cuberos",
                        "user_email": "acuberos@fluidattacks.com",
                    },
                    justification=(
                        f"""Event #{", #".join(
                            set(
                                vuln.event_id
                                for vuln in vulns if vuln.event_id
                            )
                        ).replace("EVENT#", "")} was solved.
                        The reattacks are back to the Requested
                        stage."""
                    ),
                    vulnerability_ids=set(vuln.id for vuln in vulns),
                    is_closing_event=True,
                )
                for finding_id, vulns in vulnerable.items()
            ),
            workers=4,
        )
    if safe:
        reason = VulnerabilityStateReason.VERIFIED_AS_SAFE
        await collect(
            (
                findings_domain.verify_vulnerabilities(
                    finding_id=finding_id,
                    user_info={
                        "first_name": "Andrés",
                        "last_name": "Cuberos",
                        "user_email": "acuberos@fluidattacks.com",
                    },
                    justification=(
                        f"""Event #{", #".join(
                            set(
                                vuln.event_id
                                for vuln in vulns if vuln.event_id
                            )
                        ).replace("EVENT#", "")} was solved.
                        As these vulnerabilities were closed,
                        the reattacks are set to Verified."""
                    ),
                    open_vulns_ids=[],
                    closed_vulns_ids=[vuln.id for vuln in vulns],
                    vulns_to_close_from_file=[],
                    is_closing_event=True,
                    loaders=loaders,
                    verification_reason=reason,
                )
                for finding_id, vulns in vulnerable.items()
            ),
            workers=4,
        )


async def main() -> None:
    loaders = get_new_context()
    groups = await orgs_domain.get_all_active_group_names(loaders=loaders)
    solved_events = await loaders.group_events.load_many(
        [GroupEventsRequest(group_name=group, is_solved=True) for group in groups],
    )

    print(f"Processing {len(groups)} groups...")
    await collect(
        (
            process_group(loaders, group, group_solved_events)
            for group, group_solved_events in zip(groups, solved_events, strict=False)
        ),
        workers=16,
    )


if __name__ == "__main__":
    run(main())
