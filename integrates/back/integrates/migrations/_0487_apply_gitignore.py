"""
Exclude vulnerabilities according to gitignore in the roots.

Execution Time:    2024-02-14 at 15:36:33 UTC
Finalization Time: 2024-02-14 at 16:06:11 UTC
"""

import asyncio
import logging
import logging.config
import time
from datetime import (
    datetime,
)

from aioextensions import (
    collect,
    run,
)
from aiohttp.client_exceptions import (
    ClientConnectorError,
    ClientOSError,
    ClientPayloadError,
    ServerDisconnectedError,
    ServerTimeoutError,
)
from botocore.exceptions import (
    ClientError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
)

from integrates.custom_exceptions import (
    FindingNotFound,
    UnavailabilityError,
)
from integrates.custom_utils import (
    vulnerabilities as vulns_utils,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.roots.enums import (
    RootStatus,
)
from integrates.db_model.roots.types import (
    GitRoot,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateReason,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.organizations.domain import (
    get_all_group_names,
)
from integrates.roots import (
    domain as roots_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")
NETWORK_ERRORS = (
    asyncio.TimeoutError,
    ClientConnectorError,
    ClientError,
    ClientOSError,
    ClientPayloadError,
    ConnectionError,
    ConnectionResetError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
    ServerDisconnectedError,
    ServerTimeoutError,
    UnavailabilityError,
)


@retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=10,
)
async def process_root(loaders: Dataloaders, root: GitRoot) -> None:
    try:
        vulns = await roots_domain.comment_vulnerabilities_by_exclusions(
            loaders=loaders,
            root_id=root.id,
            email="machine@fluidattacks.com",
            date=datetime.now(),
            exclusions=root.state.gitignore,
        )
        if len(vulns) > 0:
            LOGGER_CONSOLE.info("[%s] Ignored list: %s ", root.group_name, root.state.gitignore)

        for vuln in vulns:
            await vulns_utils.close_vulnerability(
                loaders=loaders,
                modified_by="machine@fluidattacks.com",
                vulnerability=vuln,
                closing_reason=VulnerabilityStateReason.EXCLUSION,
            )
            LOGGER_CONSOLE.info(
                "[%s,%s] Vuln (%s) closed",
                root.group_name,
                vuln.finding_id,
                vuln.state.where,
            )
    except (FindingNotFound, KeyError) as ex:
        LOGGER.error("gitignore error - %s - %s", root.id, str(ex))


async def process_group(
    loaders: Dataloaders,
    group_name: str,
    count: int,
    total: int,
) -> None:
    roots = await loaders.group_roots.load(group_name)
    filtered_roots = [
        root
        for root in roots
        if isinstance(root, GitRoot)
        and root.state.gitignore
        and root.state.status == RootStatus.ACTIVE
    ]

    start = time.time()
    await collect(
        tuple(process_root(loaders=loaders, root=root) for root in filtered_roots),
        workers=1,
    )
    end = time.time()

    LOGGER_CONSOLE.info(
        "[%s] %s processed in %s (Roots with gitignore: %s)",
        f"{count / total * 100:.2f} %",
        group_name,
        f"{(end - start) * 1000:.2f} ms",
        len(filtered_roots),
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    group_names = sorted(await get_all_group_names(loaders=loaders))

    LOGGER_CONSOLE.info("Processing %s groups...", len(group_names))
    await collect(
        tuple(
            process_group(
                loaders=loaders,
                group_name=group,
                count=count + 1,
                total=len(group_names),
            )
            for count, group in enumerate(group_names)
        ),
        workers=1,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
