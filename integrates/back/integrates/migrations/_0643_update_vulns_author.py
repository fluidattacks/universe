"""
Queue batch action update_vulns_author on selected roots.

Execution Time:    2024-11-20 at 15:24:33
Finalization Time: 2024-11-20 at 15:26:28, extra=None
"""

import asyncio
import logging
import os
import re
import tempfile
import time
from datetime import UTC, datetime
from subprocess import (
    SubprocessError,
)
from typing import (
    NamedTuple,
)

from aioextensions import (
    collect,
    run,
)
from git.repo import (
    Repo,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.roots.types import (
    GitRoot,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
    VulnerabilityType,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityAuthor,
    VulnerabilityMetadataToUpdate,
)
from integrates.db_model.vulnerabilities.update import (
    update_metadata,
)
from integrates.organizations.domain import get_all_active_group_names
from integrates.roots.s3_mirror import (
    download_repo,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")


class CommitInfo(NamedTuple):
    hash: str
    author: str
    modified_date: datetime


def _get_safe_int_specific(vuln_specific: str) -> int | None:
    try:
        return int(vuln_specific)
    except ValueError:
        return None


async def get_line_author(
    repo_path: str,
    filename: str,
    line: int,
    rev: str = "HEAD",
) -> CommitInfo | None:
    try:
        proc = await asyncio.create_subprocess_exec(
            "git",
            "blame",
            "-L",
            f"{line!s},+1",
            "-l",
            "-p",
            rev,
            "--",
            filename,
            stderr=asyncio.subprocess.PIPE,
            stdout=asyncio.subprocess.PIPE,
            cwd=repo_path,
        )
        stdout, stderr = await proc.communicate()
        cmd_output = stdout.decode("utf-8", "ignore")
    except (
        FileNotFoundError,
        SubprocessError,
        UnicodeDecodeError,
    ) as exc:
        LOGGER_CONSOLE.error(
            "Unable to perform git blame operation in repo %s for file %s due to error: %s",
            repo_path,
            filename,
            exc,
        )
        return None

    if stderr or proc.returncode != 0 or not cmd_output:
        error_msg = stderr.decode("utf-8", "ignore")
        LOGGER_CONSOLE.error(
            "Unable to perform git blame operation in repo %s for file %s \n Due to error %s",
            repo_path,
            filename,
            error_msg,
        )
        return None

    commit_hash = cmd_output.splitlines()[0].split(" ")[0]
    mail_search = re.search(r"author-mail <(.*?)>", cmd_output)
    author_email = mail_search.group(1) if mail_search else ""
    time_search = re.search(r"committer-time (\d*)", cmd_output)
    committer_time = time_search.group(1) if time_search else "0"
    commit_date = datetime.fromtimestamp(float(committer_time), UTC)

    return CommitInfo(
        hash=commit_hash,
        author=author_email,
        modified_date=commit_date,
    )


async def update_vuln_author(
    vuln: Vulnerability, vuln_commit: str | None, repo_working_dir: str
) -> Vulnerability | None:
    commit_author = await get_line_author(
        repo_path=repo_working_dir,
        filename=vuln.state.where,
        line=int(vuln.state.specific),
        rev=vuln_commit or "HEAD",
    )
    if not commit_author:
        return None

    await update_metadata(
        finding_id=vuln.finding_id,
        root_id=vuln.root_id,
        vulnerability_id=vuln.id,
        metadata=VulnerabilityMetadataToUpdate(
            author=VulnerabilityAuthor(
                author_email=commit_author.author,
                commit=commit_author.hash,
                commit_date=commit_author.modified_date,
            ),
        ),
    )
    return vuln


async def process_root(loaders: Dataloaders, root: GitRoot, group_name: str) -> None:
    root_vulns = [
        vuln
        for vuln in await loaders.root_vulnerabilities.load(root.id)
        if vuln.type == VulnerabilityType.LINES
        and vuln.state.status == VulnerabilityStateStatus.VULNERABLE
        and _get_safe_int_specific(vuln.state.specific) != 0
    ]
    vulns_without_author: list[Vulnerability] = [vuln for vuln in root_vulns if not vuln.author]
    if not vulns_without_author:
        return

    LOGGER_CONSOLE.info(
        "Trying to update vuln author info for %s vulns in root %s group %s",
        len(vulns_without_author),
        root.state.nickname,
        group_name,
    )

    with tempfile.TemporaryDirectory(
        prefix=f"integrates_vulns_authors_{root.id}_", ignore_cleanup_errors=True
    ) as tmpdir:
        repo: Repo | None = await download_repo(
            root.group_name,
            root.state.nickname,
            tmpdir,
            root.state.gitignore,
        )
        if not repo:
            LOGGER_CONSOLE.error(
                "Unable to download repo %s group %s", root.state.nickname, group_name
            )
            return
        repo_path = os.path.join(tmpdir, root.state.nickname)
        results = await collect(
            (
                update_vuln_author(vuln, vuln.state.commit, repo_path)
                for vuln in vulns_without_author
            ),
            workers=32,
        )

        updated_vulns = list(filter(None, results))
        LOGGER_CONSOLE.info(
            "Updated %s vulns",
            len(updated_vulns),
            extra={"vuln_ids": [vuln.id for vuln in updated_vulns]},
        )


async def main() -> None:
    loaders = get_new_context()
    group_names = sorted(await get_all_active_group_names(loaders))

    for group_name in group_names:
        roots = await loaders.group_roots.load(group_name)
        LOGGER_CONSOLE.info("Processing %s roots in group %s", len(roots), group_name)
        for root in roots:
            if not isinstance(root, GitRoot) or root.state.status != "ACTIVE":
                continue

            await process_root(loaders, root, group_name)


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    print(execution_time, finalization_time)
