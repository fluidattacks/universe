# type: ignore

"""
Restore vulnerabilities from backup.

Execution Time:    2024-05-02 at 15:24:29 UTC
Finalization Time: 2024-05-02 at 15:24:53 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.db_model.vulnerabilities.add import (
    get_vulnerability_item,
)
from integrates.db_model.vulnerabilities.constants import (
    HASH_INDEX_METADATA,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")
ROOTS_TO_REPLACE = {"source_root": "target_root"}


async def add_vulnerability(vulnerability: Vulnerability) -> None:
    items = []
    key_structure = TABLE.primary_key
    vulnerability_key = keys.build_key(
        facet=TABLE.facets["vulnerability_metadata"],
        values={
            "finding_id": vulnerability.finding_id,
            "id": vulnerability.id,
        },
    )

    vulnerability_item = get_vulnerability_item(
        vulnerability=vulnerability,
        key_structure=key_structure,
        vulnerability_key=vulnerability_key,
    )

    if vulnerability.hash:
        gsi_hash_key = keys.build_key(
            facet=HASH_INDEX_METADATA,
            values={
                "root_id": vulnerability.root_id,
                "hash": str(vulnerability.hash),
            },
        )
        gsi_hash_index = TABLE.indexes["gsi_hash"]
        vulnerability_item[gsi_hash_index.primary_key.partition_key] = gsi_hash_key.partition_key
        vulnerability_item[gsi_hash_index.primary_key.sort_key] = gsi_hash_key.sort_key

    items.append(vulnerability_item)
    await operations.batch_put_item(items=tuple(items), table=TABLE)
    LOGGER.info(
        "Vulnerability added",
        extra={
            "extra": {
                "vuln_id": vulnerability.id,
            },
        },
    )


async def process_vulnerability(vulnerability: Vulnerability) -> None:
    try:
        await add_vulnerability(vulnerability)
    except Exception as error:
        logging.error(error)

    LOGGER.info(
        "Vulnerability processed",
        extra={
            "extra": {
                "vulnerability": vulnerability.id,
            },
        },
    )


async def process_finding(loaders: Dataloaders, finding: Finding) -> None:
    vulns = await loaders.finding_vulnerabilities_all.load(finding.id)
    vulns_to_update = [
        vuln._replace(root_id=ROOTS_TO_REPLACE[vuln.root_id])
        for vuln in vulns
        if vuln.root_id in ROOTS_TO_REPLACE
    ]
    if not vulns_to_update:
        LOGGER.info(
            "No vulnerabilities to process",
            extra={
                "extra": {
                    "finding_id": finding.id,
                },
            },
        )
        return

    await collect(
        tuple(process_vulnerability(vuln) for vuln in vulns_to_update),
        workers=8,
    )
    LOGGER_CONSOLE.info("%s %s", f"{finding.id=}", f"{len(vulns_to_update)=}")


async def process_group(
    loaders: Dataloaders,
    group_name: str,
) -> None:
    all_findings = await loaders.group_findings_all.load(group_name)
    await collect(
        tuple(process_finding(loaders, finding_id) for finding_id in all_findings),
        workers=1,
    )

    LOGGER.info(
        "Findings in group processed",
        extra={
            "extra": {
                "findings": len(all_findings),
                "group": group_name,
            },
        },
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()

    groups = ["unittesting"]

    for group_name in groups:
        await process_group(loaders, group_name)
        LOGGER.info(
            "Group processed",
            extra={
                "extra": {
                    "group_name": group_name,
                },
            },
        )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
