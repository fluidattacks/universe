"""
Remove seen_at and seen_first_time_by fields from toe port metadata.

Start Time: 2024-03-15 at 21:05:42 UTC
Finalization Time: 2024-03-15 at 21:05:50 UTC
"""

import asyncio
import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)
from aiohttp.client_exceptions import (
    ClientConnectorError,
    ClientOSError,
    ClientPayloadError,
    ServerDisconnectedError,
    ServerTimeoutError,
)
from boto3.dynamodb.conditions import (
    Attr,
    Key,
)
from botocore.exceptions import (
    ClientError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
)

from integrates.class_types.types import (
    Item,
)
from integrates.custom_exceptions import (
    ToePortAlreadyUpdated,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.toe_ports.constants import (
    GSI_2_FACET,
)
from integrates.db_model.toe_ports.types import (
    ToePort,
)
from integrates.db_model.toe_ports.utils import (
    format_toe_port,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.exceptions import (
    ConditionalCheckFailedException,
    UnavailabilityError,
)
from integrates.dynamodb.types import (
    PageInfo,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


NETWORK_ERRORS = (
    asyncio.TimeoutError,
    ClientConnectorError,
    ClientError,
    ClientOSError,
    ClientPayloadError,
    ConnectionError,
    ConnectionResetError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
    ServerDisconnectedError,
    ServerTimeoutError,
    UnavailabilityError,
)


@retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=10,
)
async def _get_toe_ports_by_group(
    group_name: str,
    after: str | None,
) -> tuple[tuple[Item, ...], PageInfo]:
    primary_key = keys.build_key(
        facet=TABLE.facets["toe_port_metadata"],
        values={"group_name": group_name},
    )
    key_structure = TABLE.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(
                primary_key.sort_key.replace("#ROOT#ADDRESS#PORT", ""),
            )
        ),
        facets=(TABLE.facets["toe_port_metadata"],),
        index=None,
        table=TABLE,
        after=after,
    )

    return response.items, response.page_info


async def update_metadata(
    current_value: ToePort,
) -> None:
    key_structure = TABLE.primary_key
    gsi_2_index = TABLE.indexes["gsi_2"]

    try:
        metadata_key = keys.build_key(
            facet=TABLE.facets["toe_port_metadata"],
            values={
                "group_name": current_value.group_name,
                "address": current_value.address,
                "port": current_value.port,
                "root_id": current_value.root_id,
            },
        )
        gsi_2_key = keys.build_key(
            facet=GSI_2_FACET,
            values={
                "be_present": str(current_value.state.be_present).lower(),
                "group_name": current_value.group_name,
                "address": current_value.address,
                "port": current_value.port,
                "root_id": current_value.root_id,
            },
        )
        gsi_2_index = TABLE.indexes["gsi_2"]
        item = {
            "seen_at": None,
            "seen_first_time_by": None,
            gsi_2_index.primary_key.sort_key: gsi_2_key.sort_key,
            gsi_2_index.primary_key.partition_key: gsi_2_key.partition_key,
        }
        condition_expression = Attr(key_structure.partition_key).exists()
        await operations.update_item(
            condition_expression=condition_expression,
            item=item,
            key=metadata_key,
            table=TABLE,
        )
    except ConditionalCheckFailedException as ex:
        raise ToePortAlreadyUpdated() from ex


async def process_toe_port(toe_item: Item) -> None:
    toe_port = format_toe_port(toe_item)  # type: ignore [arg-type]
    await update_metadata(current_value=toe_port)


async def process_group(group_name: str, count: int) -> None:
    LOGGER_CONSOLE.info(
        "Group",
        extra={
            "extra": {
                "group_name": group_name,
                "count": count,
            },
        },
    )
    after = None
    counter = 0
    while True:
        group_toe_ports, page_info = await _get_toe_ports_by_group(group_name, after)
        await collect(
            tuple(process_toe_port(item) for item in group_toe_ports),
            workers=32,
        )
        LOGGER_CONSOLE.info(
            "Processing",
            extra={
                "extra": {
                    "group_name": group_name,
                    "count": count,
                    "group_toe_port": len(group_toe_ports),
                },
            },
        )
        counter += len(group_toe_ports)
        after = page_info.end_cursor
        if not page_info.has_next_page:
            break

    LOGGER_CONSOLE.info(
        "Group processed",
        extra={
            "extra": {
                "group_name": group_name,
                "count": count,
                "group_toe_ports": counter,
            },
        },
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    all_group_names = sorted(await orgs_domain.get_all_group_names(loaders))
    LOGGER_CONSOLE.info(
        "All group names",
        extra={
            "extra": {
                "total": len(all_group_names),
            },
        },
    )
    for count, group_name in enumerate(all_group_names, start=1):
        await process_group(group_name, count)


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:        %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
