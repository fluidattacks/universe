"""
Fix toe lines from files that are analyzed by sca machine and did not
have attacked_lines in the state

Execution Time:    2024-10-15 at 16:15:16 UTC
Finalization Time: 2024-10-15 at 19:55:42 UTC
"""

import asyncio
import csv
import os
import time
from contextlib import (
    suppress,
)

from aioextensions import (
    collect,
    run,
)
from aiohttp.client_exceptions import (
    ClientConnectorError,
    ClientPayloadError,
    ServerDisconnectedError,
    ServerTimeoutError,
)
from botocore.exceptions import (
    ClientError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
)

from integrates.custom_exceptions import (
    ToeLinesAlreadyUpdated,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    toe_lines as toe_lines_model,
)
from integrates.db_model.roots.enums import (
    RootStatus,
)
from integrates.db_model.roots.types import (
    GitRoot,
)
from integrates.db_model.toe_lines.types import (
    RootToeLinesRequest,
    ToeLine,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.toe.lines.constants import (
    MACHINE_SCA_CHECKS,
)

NETWORK_ERRORS = (
    asyncio.TimeoutError,
    ClientConnectorError,
    ClientError,
    ClientPayloadError,
    ConnectionError,
    ConnectionResetError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
    ServerDisconnectedError,
    ServerTimeoutError,
)


@retry_on_exceptions(exceptions=NETWORK_ERRORS, max_attempts=2, sleep_seconds=3.0)
async def _update_toe_lines(toe_lines: ToeLine) -> ToeLine | None:
    with suppress(ToeLinesAlreadyUpdated):
        file_info = os.path.split(toe_lines.filename)[1]
        if file_info in MACHINE_SCA_CHECKS and not toe_lines.state.attacked_lines:
            seen_at = toe_lines.state.seen_at
            new_attacked_at = seen_at or datetime_utils.get_utc_now()
            await toe_lines_model.update_state(
                current_value=toe_lines,
                new_state=toe_lines.state._replace(
                    modified_date=datetime_utils.get_utc_now(),
                    attacked_lines=toe_lines.state.loc,
                    attacked_at=new_attacked_at,
                    attacked_by="machine@fluidattacks.com",
                ),
            )
            return toe_lines
    return None


async def process_root(
    loaders: Dataloaders,
    group_name: str,
    root_id: str,
) -> None:
    root_toe_lines = await loaders.root_toe_lines.load_nodes(
        RootToeLinesRequest(group_name=group_name, root_id=root_id),
    )
    if not root_toe_lines:
        return

    toe_lines_updated = await collect(
        tuple(_update_toe_lines(toe_lines) for toe_lines in root_toe_lines),
        workers=32,
    )

    toe_lines_csv = [
        [
            toe_line.group_name,
            toe_line.root_id,
            toe_line.filename,
            toe_line.state.loc,
            toe_line.state.attacked_lines,
        ]
        for toe_line in toe_lines_updated
        if toe_line
    ]

    with open("updated_toe_lines.csv", "a+", encoding="utf-8") as handler:
        writer = csv.writer(handler)
        writer.writerows(toe_lines_csv)


async def process_group(
    loaders: Dataloaders,
    group_name: str,
) -> None:
    group_roots = [
        root.id
        for root in await loaders.group_roots.load(group_name)
        if isinstance(root, GitRoot) and root.state.status == RootStatus.ACTIVE
    ]

    for root_id in group_roots:
        await process_root(loaders, group_name, root_id)


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    all_group_names = sorted(await orgs_domain.get_all_active_group_names(loaders))
    for group_name in all_group_names:
        print(f"Processing group {group_name}")
        await process_group(
            loaders=loaders,
            group_name=group_name,
        )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    print(execution_time)
    print(finalization_time)
