"""
Remove duplicated 107029944 event

Execution Time:     2024-02-21 at 16:20:43 UTC
Finalization Time:  2024-02-21 at 16:20:43 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    run,
)

from integrates.db_model.events.remove import (
    remove as remove_event,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


async def main() -> None:
    event_id = "107029944"
    await remove_event(event_id=event_id)
    LOGGER_CONSOLE.info("Removed event: %s", event_id)


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:     %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time:  %Y-%m-%d at %H:%M:%S %Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
