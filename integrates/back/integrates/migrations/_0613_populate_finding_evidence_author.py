"""
Populate the author_email attribute in finding evidences and
validate the date format.

Execution Time:    2024-10-24 at 02:38:03 UTC
Finalization Time: 2021-10-24 at 02:43:10 UTC
"""

import logging
import logging.config
import time
from datetime import (
    datetime,
)

from aioextensions import (
    collect,
    run,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    findings as findings_model,
)
from integrates.db_model.findings.enums import (
    FindingEvidenceName,
)
from integrates.db_model.findings.types import (
    Finding,
    FindingEvidenceToUpdate,
)
from integrates.organizations.domain import (
    get_all_group_names,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


async def process_finding(finding: Finding) -> None:
    await collect(
        findings_model.update_evidence(
            current_value=evidence,
            group_name=finding.group_name,
            finding_id=finding.id,
            evidence_name=FindingEvidenceName[evidence_id],
            evidence=FindingEvidenceToUpdate(
                author_email=evidence.author_email or "machine@fluidattacks.com",
                modified_date=(
                    datetime.fromisoformat(str(evidence.modified_date))
                    if evidence.modified_date
                    else evidence.modified_date
                ),
            ),
        )
        for evidence_id, evidence in finding.evidences._asdict().items()
        if evidence is not None
    )


async def process_group(
    loaders: Dataloaders,
    group_name: str,
    progress: float,
) -> None:
    group_findings = await loaders.group_findings_all.load(group_name)
    if not group_findings:
        return

    await collect(
        tuple(process_finding(finding=finding) for finding in group_findings),
        workers=64,
    )
    LOGGER_CONSOLE.info("Group processed %s %s", group_name, f"{round(progress, 2)!s}")


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    group_names = sorted(await get_all_group_names(loaders))
    LOGGER_CONSOLE.info("%s", f"{group_names=}")
    LOGGER_CONSOLE.info("%s", f"{len(group_names)=}")
    await collect(
        tuple(
            process_group(
                loaders=loaders,
                group_name=group,
                progress=count / len(group_names),
            )
            for count, group in enumerate(group_names)
        ),
        workers=4,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
