"""
Update vulnerability historic state inconsistencies

Execution Time:    2024-11-26 at 23:44:19 UTC
Finalization Time: 2024-11-26 at 23:44:39 UTC
"""

import io
import logging
import time
from collections.abc import AsyncGenerator, Coroutine
from contextlib import (
    asynccontextmanager,
)
from csv import (
    DictReader,
)
from typing import (
    Any,
    Literal,
)

import simplejson
from aioextensions import (
    collect,
    run,
)

from integrates.context import (
    FI_ENVIRONMENT,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import HISTORIC_TABLE
from integrates.db_model.utils import get_as_utc_iso_format, serialize
from integrates.db_model.vulnerabilities.get import _get_historic_state
from integrates.db_model.vulnerabilities.types import VulnerabilityRequest, VulnerabilityState
from integrates.db_model.vulnerabilities.update import add_historic_entry as add_vms_entry
from integrates.dynamodb import keys, operations
from integrates.migrations._0611_find_vuln_inconsistent_status import _get_vuln_states_vms
from integrates.s3.resource import (
    get_client,
)

VULNS_PATH = "vuln_inconsistencies/vuln_inconsistencies.csv"
BUCKET = "integrates.dev"
LOGGER = logging.getLogger("console")

Target = Literal["local", "s3"]


@asynccontextmanager
async def _init_vulns_reader(
    target: Target,
) -> AsyncGenerator[DictReader, None]:
    with io.BytesIO() as buffer:
        if target == "s3":
            client = await get_client()
            await client.download_fileobj(
                BUCKET,
                VULNS_PATH,
                buffer,
            )

            buffer.seek(0)

            buffer_str: io.StringIO = io.StringIO(buffer.read().decode())
            yield DictReader(buffer_str)
        elif target == "local":
            with open(VULNS_PATH, encoding="utf-8") as file:
                yield DictReader(file)


async def _add_vms_historic_entry(
    *,
    entry: VulnerabilityState,
    vulnerability_id: str,
    finding_id: str,
) -> None:
    key_structure = HISTORIC_TABLE.primary_key
    entry_item = simplejson.loads(simplejson.dumps(entry, default=serialize))

    historic_entry_key = keys.build_key(
        facet=HISTORIC_TABLE.facets["vulnerability_state"],
        values={
            "id": vulnerability_id,
            "iso8601utc": get_as_utc_iso_format(entry.modified_date),
            "state": "state",
            "finding_id": finding_id,
        },
    )
    historic_item = {
        key_structure.partition_key: historic_entry_key.partition_key,
        key_structure.sort_key: historic_entry_key.sort_key,
        **entry_item,
    }
    await operations.put_item(
        facet=HISTORIC_TABLE.facets["vulnerability_state"],
        item=historic_item,
        table=HISTORIC_TABLE,
    )


async def _fix_vuln_inconsistencies(
    loaders: Dataloaders,
    row: dict[str, Any],
) -> None:
    vuln_id = row["vuln_id"]
    group_name = row["group"]
    vuln = await loaders.vulnerability.load(vuln_id)
    if vuln is None:
        LOGGER.error("Vuln: %s from group %s not found", vuln_id, group_name)
        return

    states_integrates_vms_historic, states_integrates_vms = await collect(
        [
            _get_historic_state(
                request=VulnerabilityRequest(finding_id=vuln.finding_id, vulnerability_id=vuln_id)
            ),
            _get_vuln_states_vms(vuln_id=vuln_id),
        ],
        workers=2,
    )
    coroutines: list[Coroutine] = []
    historic_inconsistent = False
    vms_inconsistent = False

    if (
        not states_integrates_vms_historic
        or states_integrates_vms_historic[-1].status != vuln.state.status
    ):
        historic_inconsistent = True
        coroutines.append(
            _add_vms_historic_entry(
                entry=vuln.state,
                vulnerability_id=vuln_id,
                finding_id=vuln.finding_id,
            )
        )

    if not states_integrates_vms or states_integrates_vms[-1].status != vuln.state.status:
        vms_inconsistent = True
        coroutines.append(
            add_vms_entry(
                entry=vuln.state,
                vulnerability_id=vuln_id,
            )
        )

    await collect(coroutines, workers=2)
    LOGGER.info(
        "Vuln: %s from group %s fixed. integrates_vms_historic: %s, integrates_vms: %s",
        vuln_id,
        group_name,
        "FIXED" if historic_inconsistent else "OK",
        "FIXED" if vms_inconsistent else "OK",
    )


async def main() -> None:
    target: Target = "s3" if FI_ENVIRONMENT == "production" else "local"
    loaders = get_new_context()
    async with _init_vulns_reader(target) as reader:
        await collect(
            [_fix_vuln_inconsistencies(loaders, row) for row in reader],
            workers=8,
        )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER.info("\n%s\n%s", execution_time, finalization_time)
