# type: ignore


"""Populates OpenSearch with all the new packages from active groups"""

import asyncio
import logging
import logging.config
import time
from datetime import (
    timedelta,
)

from aioextensions import (
    collect,
    run,
)
from aiohttp.client_exceptions import (
    ClientConnectorError,
    ClientPayloadError,
    ServerDisconnectedError,
    ServerTimeoutError,
)
from botocore.exceptions import (
    ClientError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
)
from more_itertools import (
    flatten,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.roots.enums import (
    RootStatus,
)
from integrates.db_model.roots.types import (
    GitRoot,
)
from integrates.db_model.toe_packages import (
    update_state,
)
from integrates.db_model.toe_packages.types import (
    RootToePackagesRequest,
    ToePackagesConnection,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.group_access.domain import (
    EMAIL_INTEGRATES,
)
from integrates.organizations.domain import (
    get_all_active_group_names,
)
from integrates.search.client import (
    get_client,
    search_shutdown,
    search_startup,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger(__name__)
NETWORK_ERRORS = (
    asyncio.TimeoutError,
    ClientConnectorError,
    ClientError,
    ClientPayloadError,
    ConnectionError,
    ConnectionResetError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
    ServerDisconnectedError,
    ServerTimeoutError,
)


@retry_on_exceptions(exceptions=NETWORK_ERRORS, max_attempts=3, sleep_seconds=3.0)
async def process_group(group_name: str, loaders: Dataloaders) -> None:
    git_roots = list(
        root
        for root in await loaders.group_roots.load(group_name)
        if isinstance(root, GitRoot) and root.state.status == RootStatus.ACTIVE
    )
    db_root_packages: list[ToePackagesConnection] = await loaders.root_toe_packages.load_many(
        [
            RootToePackagesRequest(
                group_name=group_name,
                root_id=git_root.id,
            )
            for git_root in git_roots
        ],
    )
    packages = list(y.node for y in flatten([x.edges for x in db_root_packages]))
    if not packages:
        return
    updates = [
        update_state(
            current_value=x,
            state=x.state._replace(
                modified_date=(x.state.modified_date + timedelta(seconds=1)),
                modified_by=EMAIL_INTEGRATES,
            ),
        )
        for x in packages
    ]
    await collect(updates, workers=8)
    LOGGER.info(
        "Group processed",
        extra={
            "extra": {
                "group_name": group_name,
                "toe_packages_processed": len(packages),
            },
        },
    )


async def main() -> None:
    loaders = get_new_context()
    active_group_names = sorted(await get_all_active_group_names(loaders))
    await search_startup()
    client = await get_client()
    await client.indices.delete(index="pkgs_index")
    await client.indices.create(index="pkgs_index")
    await collect(
        tuple(process_group(group_name, loaders) for group_name in active_group_names),
        workers=4,
    )
    await search_shutdown()


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER.info("\n%s\n%s", execution_time, finalization_time)
