# type: ignore


import asyncio
import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)
from aiohttp.client_exceptions import (
    ClientConnectorError,
    ClientPayloadError,
    ServerDisconnectedError,
    ServerTimeoutError,
)
from boto3.dynamodb.conditions import (
    Key,
)
from botocore.exceptions import (
    ClientError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
)
from more_itertools import (
    chunked,
)
from opensearchpy.helpers import (
    BulkIndexError,
    async_bulk,
)

from integrates.class_types.types import (
    Item,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.organizations.domain import (
    get_all_active_group_names,
)
from integrates.search.client import (
    get_client,
    search_shutdown,
    search_startup,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger(__name__)
NETWORK_ERRORS = (
    asyncio.TimeoutError,
    ClientConnectorError,
    ClientError,
    ClientPayloadError,
    ConnectionError,
    ConnectionResetError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
    ServerDisconnectedError,
    ServerTimeoutError,
)


async def process_inputs(group_name: str, inputs: tuple[Item, ...]) -> None:
    actions = [
        {
            # Max id length in OpenSearch is 512 (DocWriteRequest.java#L263)
            "_id": "#".join([toe_input["pk"], toe_input["sk"]])[:512],
            "_index": "inputs_index",
            "_op_type": "index",
            "_source": toe_input,
        }
        for toe_input in inputs
    ]

    client = await get_client()
    try:
        await async_bulk(client=client, actions=actions)
    except BulkIndexError as ex:
        for error in ex.errors:
            LOGGER.info("%s %s", group_name, error["index"]["error"]["reason"])


@retry_on_exceptions(exceptions=NETWORK_ERRORS, max_attempts=3, sleep_seconds=3.0)
async def process_group(group_name: str) -> None:
    primary_key = keys.build_key(
        facet=TABLE.facets["toe_input_metadata"],
        values={"group_name": group_name},
    )
    key_structure = TABLE.primary_key
    condition_expression = Key(key_structure.partition_key).eq(primary_key.partition_key) & Key(
        key_structure.sort_key,
    ).begins_with("INPUTS#")
    response = await operations.query(
        condition_expression=condition_expression,
        facets=(TABLE.facets["toe_input_metadata"],),
        table=TABLE,
    )
    inputs = response.items

    await collect(
        tuple(process_inputs(group_name, inputs_chunk) for inputs_chunk in chunked(inputs, 100)),
        workers=4,
    )
    LOGGER.info(
        "Group processed",
        extra={
            "extra": {
                "group_name": group_name,
                "toe_inputs": len(inputs),
            },
        },
    )


async def main() -> None:
    loaders = get_new_context()
    active_group_names = sorted(await get_all_active_group_names(loaders))
    await search_startup()
    await collect(
        tuple(process_group(group_name) for group_name in active_group_names),
        workers=2,
    )
    await search_shutdown()


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER.info("\n%s\n%s", execution_time, finalization_time)
