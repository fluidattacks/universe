"""
Query each organization mailmap to check if there are any duplicate aliases

Start Time:        2024-09-06 at 01:04:07 UTC
Finalization Time: 2024-09-06 at 01:04:12 UTC

"""

import logging
import logging.config
import time
from collections import (
    defaultdict,
)

from aioextensions import (
    run,
)

from integrates.db_model.items import MailmapSubentryItem
from integrates.db_model.mailmap.utils.get import (
    query_all_items,
)
from integrates.db_model.organizations.get import (
    get_all_organizations,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


def find_duplicate_subentries(subentries: tuple[MailmapSubentryItem, ...]) -> list[str]:
    seen = defaultdict(list)
    duplicates = []

    # Iterate over each object in the list
    for subentry in subentries:
        key = (
            subentry["mailmap_subentry_name"],
            subentry["mailmap_subentry_email"],
        )
        seen[key].append(str(subentry))

    # Collect all objects where there is more than one occurrence of the key
    for key, entries in seen.items():
        if len(entries) > 1:
            duplicates.extend(entries)

    return duplicates


async def main() -> None:
    organizations = await get_all_organizations()
    for organization in organizations:
        subentries = await query_all_items(
            facet_name="mailmap_subentry",
            organization_id=organization.id,
        )
        duplicates = find_duplicate_subentries(subentries)
        if len(duplicates) > 0:
            LOGGER_CONSOLE.info(
                "Organization %s - %s have duplicate aliases:\n%s\n\n",
                organization.id,
                organization.name,
                "\n".join(duplicates),
            )


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:        %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
