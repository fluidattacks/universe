"""
Migration to update http open vulnerabilities whose description was changed to
meet new standards.

- F036 Finding was not included because its unique method is a content one (its
specific is a number, not a description).
- Changes specific in open vulnerabilities whose method description was
modified in order to be compliant with new format standards.

Execution Time:    2024-08-13 at 00:09:50 UTC
Finalization Time: 2024-08-13 at 00:18:33 UTC
"""

import csv
import hashlib
import logging
import logging.config
import re
import time
from enum import (
    Enum,
)
from pathlib import (
    Path,
)

import yaml
from aioextensions import (
    collect,
    run,
)

from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.custom_utils.vulnerabilities import (
    get_path_from_integrates_vulnerability,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.enums import (
    Source,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateReason,
    VulnerabilityStateStatus,
    VulnerabilityType,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityMetadataToUpdate,
    VulnerabilityState,
)
from integrates.db_model.vulnerabilities.update import (
    update_historic_entry,
    update_metadata,
)
from integrates.organizations.domain import (
    get_all_active_groups,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")

# These method's descriptions can be taken from translations.
METHODS_WITH_FLAT_DESCRIPTIONS = [
    # F036
    "analyze_content.view_state",
    # F043
    "analyze_headers.cont_sec_pol_missing_obj",
    "analyze_headers.cont_sec_pol_missing_script",
    # F131
    "analyze_headers.strict_transport_low_max_age",
    # F132
    "analyze_headers.x_content_type_options_nosniff",
    # F137
    "analyze_headers.x_permitted_cross_domain_policies",
    # F235
    "analyze_headers.http_server_header_leaked",
]

METHODS_WITH_COMPOSED_DESCRIPTIONS = {
    # F043
    "analyze_headers.cont_sec_pol_hosts_jsonp": {
        "EN": r"(?P<name>[\w]*) hosts JSONP",
        "ES": r"(?P<name>[\w]*) aloja JSONP",
    },
    # F128
    "analyze_headers.set_cookie_httponly": {
        "EN": r"(?P<name>[\w]*) is missing HttpOnly",
        "ES": r"(?P<name>[\w]*) sin HttpOnly",
    },
    # F129
    "analyze_headers.set_cookie_samesite": {
        "EN": r"(?P<name>[\w]*) does not have SameSite correctly initialized",
        "ES": r"(?P<name>[\w]*) no tiene SameSite correctamente inicializado",
    },
    # F130
    "analyze_headers.set_cookie_secure": {
        "EN": r"(?P<name>[\w]*) is missing Secure",
        "ES": r"(?P<name>[\w]*) sin Secure",
    },
}

KWARG_NAME_BY_METHOD = {
    "CONT_SEC_POL_HOSTS_JSONP": "host",
    "SET_COOKIE_HTTPONLY": "cookie_name",
    "SET_COOKIE_SAMESITE": "cookie_name",
    "SET_COOKIE_SECURE": "cookie_name",
}


class LocalesEnum(Enum):
    EN: str = "EN"
    ES: str = "ES"


def get_translations() -> dict:
    translations = {}
    current_dir = Path.cwd()
    translations_path = (
        current_dir / "../../../../skims/static/translations/criteria/vulnerabilities"
        "/HTTP_DESCRIPTIONS.yaml"
    )

    with open(translations_path, encoding="utf-8") as handle:
        for key, data in yaml.safe_load(handle).items():
            translations[key] = {
                locale_code: data[locale_code.lower()]
                for locale in LocalesEnum
                for locale_code in [locale.value]
            }
    return translations


TRANSLATIONS: dict[str, dict[str, str]] = get_translations()


async def get_http_vulns(
    loaders: Dataloaders,
    findings: list[Finding],
    fin_code: str,
) -> list[Vulnerability]:
    vulns = await loaders.finding_vulnerabilities.load_many_chained(
        [fin.id for fin in findings if fin.title.startswith(fin_code)],
    )
    return [
        vuln
        for vuln in vulns
        if vuln.state.status == VulnerabilityStateStatus.VULNERABLE
        and (vuln.state.source == Source.MACHINE or vuln.hacker_email == "machine@fluidattacks.com")
        and vuln.skims_method is not None
        and vuln.type == VulnerabilityType.INPUTS
        and "analyze_headers" in vuln.skims_method
    ]


async def process_vuln(
    vuln: Vulnerability,
    fin_code: str,
    language: str,
) -> tuple[str, int, VulnerabilityState] | None:
    _, where = get_path_from_integrates_vulnerability(
        vuln.state.where,
        VulnerabilityType.INPUTS,
    )
    method = str(vuln.skims_method)
    new_specific = vuln.state.specific
    translation_key = method.split(".", maxsplit=1)[-1].upper()
    if method in METHODS_WITH_FLAT_DESCRIPTIONS:
        new_specific = TRANSLATIONS[translation_key][language]
    elif method in METHODS_WITH_COMPOSED_DESCRIPTIONS and (
        match := re.match(
            METHODS_WITH_COMPOSED_DESCRIPTIONS[method][language],
            vuln.state.specific,
        )
    ):
        name = match.group("name")
        kwargs = {KWARG_NAME_BY_METHOD[translation_key]: name}
        new_specific = TRANSLATIONS[translation_key][language].format(**kwargs)
    else:
        return None

    new_hash = int.from_bytes(
        hashlib.sha256(
            bytes(
                (where + new_specific + fin_code + method),
                encoding="utf-8",
            ),
        ).digest()[:8],
        "little",
    )

    new_state = vuln.state._replace(
        specific=new_specific,
        modified_date=datetime_utils.get_utc_now(),
        reasons=[VulnerabilityStateReason.CONSISTENCY],
        modified_by="lpatino@fluidattacks.com",
    )

    return new_specific, new_hash, new_state


async def process_vulns(
    http_vulns: list[Vulnerability],
    fin_code: str,
    language: str,
) -> tuple[list, list]:
    futures = []
    rows = []
    for vuln in http_vulns:
        if process_result := await process_vuln(vuln, fin_code, language):
            (
                new_specific,
                new_hash,
                new_state,
            ) = process_result

            futures.append(
                update_historic_entry(
                    current_value=vuln,
                    finding_id=vuln.finding_id,
                    entry=new_state,
                    vulnerability_id=vuln.id,
                ),
            )

            futures.append(
                update_metadata(
                    finding_id=vuln.finding_id,
                    metadata=VulnerabilityMetadataToUpdate(hash=new_hash),
                    root_id=vuln.root_id,
                    vulnerability_id=vuln.id,
                ),
            )

            rows.extend(
                [
                    [
                        vuln.group_name,
                        fin_code,
                        vuln.skims_method,
                        new_specific.strip(),
                        vuln.id,
                    ],
                ],
            )
    return futures, rows


async def adjust_group_http_reports(
    loaders: Dataloaders,
    group: str,
    language: str,
    searched_fins: tuple[str, ...],
) -> None:
    LOGGER_CONSOLE.info("Processing %s", group)
    findings = await loaders.group_findings.load(group)
    all_futures = []
    all_rows = []
    for fin_code in searched_fins:
        http_vulns = await get_http_vulns(loaders, findings, fin_code)
        futures, rows = await process_vulns(http_vulns, fin_code, language)
        all_futures.extend(futures)
        all_rows.extend(rows)

    await collect(all_futures, workers=20)

    with open("http_vulns_migration.csv", "a+", encoding="utf-8") as handler:
        writer = csv.writer(handler)
        writer.writerows(all_rows)


async def main() -> None:
    searched_fins = (
        "043",
        "128",
        "129",
        "130",
        "131",
        "132",
        "137",
        "235",
    )
    loaders = get_new_context()
    active_groups = await get_all_active_groups(loaders)
    groups = sorted([(group.name, group.language.value) for group in active_groups])

    for group, language in groups:
        await adjust_group_http_reports(
            loaders,
            group,
            language,
            searched_fins,
        )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
