# type: ignore
"""
Populate field use_secure_access for environments

Start Time: 2023-08-29 at 13:34:48 UTC
Finalization Time: 2023-08-29 at 13:35:29 UTC
"""

import logging
import logging.config
import time
from collections import (
    defaultdict,
)
from datetime import (
    datetime,
)
from typing import (
    TypedDict,
)

from aioextensions import (
    collect,
    run,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.roots.types import (
    Root,
    RootEnvironmentUrl,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


class EnvInfo(TypedDict):
    group_name: str
    roots: list[Root]
    url: str


class EnvKeyData(TypedDict):
    group_name: str
    datetime: datetime


EnvConsistencyData = defaultdict[str, dict[str, EnvKeyData]]


async def update_git_environment_urls(
    root_id: str,
    url: RootEnvironmentUrl,
) -> None:
    url_key = keys.build_key(
        facet=TABLE.facets["root_environment_url"],
        values={"uuid": root_id, "hash": url.id},
    )

    use_secure_access = {"use_secure_access": False}

    await operations.update_item(
        item=use_secure_access,
        key=url_key,
        table=TABLE,
    )


async def _get_root_environment_urls(loaders: Dataloaders, root: Root) -> list[RootEnvironmentUrl]:
    return await loaders.root_environment_urls.load(root.id)


async def _get_environment(
    loaders: Dataloaders,
    group_name: str,
) -> defaultdict[str, EnvInfo]:
    group_roots = await loaders.group_roots.load(group_name)

    for root in group_roots:
        urls = await _get_root_environment_urls(loaders, root)
        if urls:
            for url in urls:
                if url.use_secure_access is None:
                    LOGGER_CONSOLE.info("URL with use_secure_access eq to None: %s", url)
                    await update_git_environment_urls(root.id, url)


async def _get_environment_by_group(
    loaders: Dataloaders,
    all_group_names: list[str],
) -> None:
    await collect(
        [_get_environment(loaders, group_name) for group_name in all_group_names],
        workers=10,
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()

    all_group_names = sorted(await orgs_domain.get_all_group_names(loaders=loaders))
    await _get_environment_by_group(loaders, all_group_names)


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:    %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("%s", execution_time)
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
