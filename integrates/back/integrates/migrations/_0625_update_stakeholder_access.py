"""
Clear ttl field when invitation was used

Execution Time:    2024-11-06 at 16:57:08 UTC
Finalization Time: 2024-11-06 at 17:01:40 UTC
"""

import time

from aioextensions import (
    collect,
    run,
)

from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.organization_access.types import (
    OrganizationAccessMetadataToUpdate,
)
from integrates.db_model.stakeholders.get import (
    get_all_stakeholders,
)
from integrates.db_model.stakeholders.types import (
    Stakeholder,
)
from integrates.organizations.domain import (
    update_organization_access,
)


async def process_stakeholder(stakeholder: Stakeholder) -> None:
    loaders = get_new_context()
    stakeholder_orgs = await loaders.stakeholder_organizations_access.load(stakeholder.email)
    expiring = [
        access
        for access in stakeholder_orgs
        if access.state.invitation
        and access.state.invitation.is_used
        and access.expiration_time is not None
    ]
    await collect(
        update_organization_access(
            access.organization_id,
            access.email,
            OrganizationAccessMetadataToUpdate(
                expiration_time=0,
                state=access.state,
            ),
        )
        for access in expiring
    )


async def main() -> None:
    all_stakeholders = await get_all_stakeholders()
    await collect(
        (process_stakeholder(stakeholder) for stakeholder in all_stakeholders),
        workers=32,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    print("\n%s\n%s", execution_time, finalization_time)
