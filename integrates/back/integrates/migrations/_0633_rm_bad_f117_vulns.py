"""
Migration to ensure all erroneous F117 vulnerabilities caused by the
overly broad application of path rules are removed.

There is a Group whose  Machine F117 finding id is hardcoded due to
containing 2k vulnerabilities marked as SAFE. Despite their status,
these vulnerabilities generate noise. Service team has requested
their removal

Execution Time:    2024-12-04 at 01:49:49 UTC
Finalization Time: 2024-12-04 at 02:03:40 UTC
"""

import csv
import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.enums import (
    Source,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateReason,
    VulnerabilityStateStatus,
    VulnerabilityZeroRiskStatus,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
)
from integrates.organizations.domain import (
    get_all_group_names,
)
from integrates.settings import (
    LOGGING,
)
from integrates.vulnerabilities.domain import (
    remove_vulnerability,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")
EMAIL_INTEGRATES = "integrates@fluidattacks.com"
BINARY_EXTENSIONS = {
    "aar",
    "apk",
    "bin",
    "class",
    "dll",
    "DS_Store",
    "exe",
    "exec",
    "hprof",
    "jar",
    "jasper",
    "pdb",
    "pyc",
    "wasm",
    "js",
}


async def process_vulns(loaders: Dataloaders, finding: Finding, vulns: list[Vulnerability]) -> None:
    rows = []
    vulns_to_be_removed = []

    for vuln in vulns:
        # There are some vulns reported by analysts in Machine findings
        if vuln.state.source != Source.MACHINE or (
            vuln.zero_risk and vuln.zero_risk.status == VulnerabilityZeroRiskStatus.CONFIRMED
        ):
            continue
        file_extension = vuln.state.where.split(".")[-1]

        if file_extension not in BINARY_EXTENSIONS and (
            finding.id == "fcd0fe40-2dd8-4dbc-b4c2-0cc5d31300b7"
            or vuln.state.status == VulnerabilityStateStatus.VULNERABLE
        ):
            vulns_to_be_removed.append(vuln)
    if len(vulns_to_be_removed) == 0:
        return

    try:
        await collect(
            (
                remove_vulnerability(
                    loaders=loaders,
                    finding_id=finding.id,
                    vulnerability_id=_vuln.id,
                    justification=VulnerabilityStateReason.CONSISTENCY,
                    email="lpatino@fluidattacks.com",
                    include_closed_vuln=True,
                )
                for _vuln in vulns_to_be_removed
            ),
            workers=15,
        )
        for _vuln in vulns_to_be_removed:
            rows.append(
                [
                    "SUCCESSFULLY DELETED",
                    _vuln.group_name,
                    _vuln.state.where.split(".")[-1],
                    _vuln.state.status.name,
                    _vuln.state.where,
                ],
            )

    except Exception:  # pylint: disable=broad-except
        for _vuln in vulns_to_be_removed:
            rows.append(
                [
                    "ERROR",
                    _vuln.group_name,
                    _vuln.state.where.split(".")[-1],
                    _vuln.state.status.name,
                    _vuln.state.where,
                ],
            )

    with open("f117_deleted_vulns.csv", "a+", encoding="utf-8") as handler:
        writer = csv.writer(handler)
        writer.writerows(rows)


async def process_group(
    loaders: Dataloaders,
    group_name: str,
    progress: float,
) -> None:
    findings = await loaders.group_findings.load(group_name)

    f_117_machine_findings = [
        finding
        for finding in findings
        if finding.state.source == Source.MACHINE and finding.title[:3] == "117"
    ]

    findings_vulns = await loaders.finding_vulnerabilities.load_many(
        [finding.id for finding in f_117_machine_findings],
    )

    for finding, vulns in zip(f_117_machine_findings, findings_vulns, strict=False):
        await process_vulns(loaders, finding, vulns)

    LOGGER_CONSOLE.info(
        "Group updated",
        extra={
            "extra": {
                "group_name": group_name,
                "progress": str(round(progress, 2)),
            },
        },
    )


async def main() -> None:
    loaders = get_new_context()
    group_names = sorted(await get_all_group_names(loaders=loaders))
    for count, group in enumerate(group_names):
        await process_group(
            loaders=loaders,
            group_name=group,
            progress=count / len(group_names),
        )


if __name__ == "__main__":
    execution_time = time.strftime(
        "Execution Time:    %Y-%m-%d at %H:%M:%S %Z",
    )
    run(main())
    finalization_time = time.strftime(
        "Finalization Time: %Y-%m-%d at %H:%M:%S %Z",
    )
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
