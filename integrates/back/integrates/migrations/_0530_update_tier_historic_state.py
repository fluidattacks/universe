"""
Update state tier in integrates_vms_historic and integrates_vms table.

Start Time:        2024-04-19 at 15:39:32 UTC
Finalization Time: 2024-04-19 at 15:52:36 UTC, extra=None

"""

import asyncio
import logging
import logging.config
import time
from typing import (
    Any,
)

from aioextensions import (
    run,
)
from aiohttp.client_exceptions import (
    ClientConnectorError,
    ClientOSError,
    ClientPayloadError,
    ServerDisconnectedError,
    ServerTimeoutError,
)
from boto3.dynamodb.conditions import (
    Attr,
    Key,
)
from botocore.exceptions import (
    ClientError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
)

from integrates.class_types.types import (
    Item,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    HISTORIC_TABLE,
    TABLE,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.exceptions import (
    UnavailabilityError,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")
NETWORK_ERRORS = (
    asyncio.TimeoutError,
    ClientConnectorError,
    ClientError,
    ClientOSError,
    ClientPayloadError,
    ConnectionError,
    ConnectionResetError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
    ServerDisconnectedError,
    ServerTimeoutError,
    UnavailabilityError,
)


@retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=10,
)
async def get_group_historic_state(
    group_name: str,
) -> tuple[Item, ...]:
    primary_key = keys.build_key(
        facet=TABLE.facets["group_historic_state"],
        values={"name": group_name},
    )
    key_structure = TABLE.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(TABLE.facets["group_historic_state"],),
        table=TABLE,
    )
    return response.items


@retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=10,
)
async def update_historic_state(prev_historic: dict[str, Any]) -> None:
    pk_info = prev_historic["pk"].split("#")
    sk_info = prev_historic["sk"].split("#")

    primary_key = keys.build_key(
        facet=TABLE.facets["group_historic_state"],
        values={
            "name": pk_info[1],
            "iso8601utc": sk_info[-1],
        },
    )

    tier = prev_historic.get("tier")
    if tier == "SQUAD":
        tier = "ADVANCED"
    elif tier == "MACHINE":
        tier = "ESSENTIAL"

    item_to_update = {
        **{k: v for k, v in prev_historic.items() if k not in ("pk", "sk")},
        "tier": tier,
    }

    key_structure = TABLE.primary_key
    await operations.update_item(
        condition_expression=Attr(key_structure.partition_key).exists(),
        item=item_to_update,
        key=primary_key,
        table=TABLE,
    )


@retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=10,
)
async def get_group_state(
    group_name: str,
    organization: str,
) -> tuple[Item, ...]:
    primary_key = keys.build_key(
        facet=HISTORIC_TABLE.facets["group_state"],
        values={
            "name": group_name,
            "organization_id": organization,
            "state": "state",
        },
    )
    key_structure = HISTORIC_TABLE.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(HISTORIC_TABLE.facets["group_state"],),
        table=HISTORIC_TABLE,
    )

    return response.items


@retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=10,
)
async def update_group_state(prev_historic: dict[str, Any]) -> None:
    pk_info = prev_historic["pk"].split("#")
    sk_info = prev_historic["sk"].split("#")

    primary_key = keys.build_key(
        facet=HISTORIC_TABLE.facets["group_state"],
        values={
            "name": pk_info[1],
            "organization_id": pk_info[-1],
            "state": sk_info[1],
            "iso8601utc": sk_info[-1],
        },
    )

    tier = prev_historic.get("tier")
    if tier == "SQUAD":
        tier = "ADVANCED"
    elif tier == "MACHINE":
        tier = "ESSENTIAL"

    item_to_update = {
        **{k: v for k, v in prev_historic.items() if k not in ("pk", "sk")},
        "tier": tier,
    }

    key_structure = HISTORIC_TABLE.primary_key
    await operations.update_item(
        condition_expression=Attr(key_structure.partition_key).exists(),
        item=item_to_update,
        key=primary_key,
        table=HISTORIC_TABLE,
    )


@retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=10,
)
async def get_group(*, group_name: str) -> Item:
    primary_key = keys.build_key(
        facet=TABLE.facets["group_metadata"],
        values={"name": group_name},
    )

    key_structure = TABLE.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(TABLE.facets["group_metadata"],),
        limit=1,
        table=TABLE,
    )
    return response.items[0]


async def process_group(group_name: str) -> None:
    group_item = await get_group(group_name=group_name)
    historics_group_state = await get_group_historic_state(group_name=group_name)
    group_state_historic = await get_group_state(
        group_name=group_name,
        organization=group_item["organization_id"],
    )
    for historic in historics_group_state:
        await update_historic_state(historic)

    for historic in group_state_historic:
        await update_group_state(historic)

    LOGGER_CONSOLE.info(
        "Group processed",
        extra={
            "extra": {
                "group_name": group_name,
                "prev_group_historic_state": historics_group_state,
                "prev_group_state": group_state_historic,
            },
        },
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    all_group_names = sorted(await orgs_domain.get_all_group_names(loaders))
    LOGGER_CONSOLE.info(
        "All group names",
        extra={
            "extra": {
                "total": len(all_group_names),
            },
        },
    )
    for group_name in all_group_names:
        LOGGER_CONSOLE.info(
            "Group",
            extra={
                "extra": {
                    "group_name": group_name,
                },
            },
        )
        await process_group(group_name)


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:        %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
