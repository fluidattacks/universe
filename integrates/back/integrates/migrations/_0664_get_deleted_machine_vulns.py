"""
Populate "technique" field for all vulns that do not have it.
Delete "skims_technique" field for all vulns.
"""

import csv
import logging
import logging.config
import time

from aioextensions import (
    run,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.enums import (
    Source,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateReason,
    VulnerabilityStateStatus,
)
from integrates.organizations.domain import (
    get_all_group_names,
)
from integrates.s3.resource import (
    get_client,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")

TARGET_BUCKET = "integrates.dev"
S3_FOLDER = "machine_deleted_vulns"
LOCAL_FILE_PATH = "vulns_summary.csv"


async def save_to_s3() -> None:
    client = await get_client()
    await client.upload_file(
        LOCAL_FILE_PATH,
        TARGET_BUCKET,
        f"{S3_FOLDER}/{LOCAL_FILE_PATH}",
    )


async def process_group(
    loaders: Dataloaders,
    group_name: str,
) -> None:
    csv_vulns = []
    group_findings = await loaders.group_findings_all.load(group_name)

    total_group_vulns = await loaders.finding_vulnerabilities_all.load_many_chained(
        [
            fin.id
            for fin in group_findings
            if not fin.title.startswith(("066", "234", "237", "379"))
        ],
    )

    deleted_vulns = [
        vuln
        for vuln in total_group_vulns
        if (
            vuln.state.status == VulnerabilityStateStatus.DELETED
            and (
                vuln.state.source == Source.MACHINE
                or vuln.hacker_email == "machine@fluidattacks.com"
            )
            and (
                not vuln.state.reasons
                or any(
                    possible_reason in vuln.state.reasons
                    for possible_reason in [
                        VulnerabilityStateReason.CONSISTENCY,
                        VulnerabilityStateReason.FALSE_POSITIVE,
                    ]
                )
            )
        )
    ]

    csv_vulns.extend(
        [
            [
                vuln.group_name,
                vuln.finding_id,
                vuln.id,
                vuln.skims_method,
                vuln.state.reasons,
                vuln.state.modified_by,
            ]
            for vuln in deleted_vulns
        ],
    )

    with open(LOCAL_FILE_PATH, "a+", encoding="utf-8") as handler:
        writer = csv.writer(handler)
        writer.writerows(csv_vulns)

    LOGGER_CONSOLE.info("Deleted FP vulns found  %s", len(deleted_vulns))
    LOGGER_CONSOLE.info("Group processed  %s", group_name)


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    groups = sorted(await get_all_group_names(loaders))
    for group in groups:
        await process_group(loaders, group)

    await save_to_s3()


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
