"""
Remove reports from old deleted machine methods
Execution Time:    2024-01-26 at 20:05:37 UTC
Finalization Time: 2024-01-26 at 22:05:43 UTC
"""

import csv
import time

from aioextensions import (
    collect,
    run,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.enums import (
    Source,
    StateRemovalJustification,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateReason,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
)
from integrates.findings.domain import (
    remove_finding,
)
from integrates.organizations.domain import (
    get_all_active_group_names,
)
from integrates.vulnerabilities.domain import (
    remove_vulnerability,
)


async def close_machine_vulnerabilities(
    loaders: Dataloaders,
    group: str,
    fin_enum: tuple[str, ...],
) -> None:
    print(f"Processing group {group}")
    findings = await loaders.group_findings.load(group)
    findings_numb = [finding for finding in findings if finding.title.startswith(fin_enum)]
    findings_vulns: list[list[Vulnerability]] = await loaders.finding_vulnerabilities.load_many(
        [finding.id for finding in findings_numb],
    )
    rows = []

    for finding, vulns in zip(findings_numb, findings_vulns, strict=False):
        wrong_vulns = [
            vuln
            for vuln in vulns
            if (vuln.hacker_email == "machine@fluidattacks.com" and vuln.skims_method is not None)
        ]

        if len(wrong_vulns) > 0 and len(wrong_vulns) == len(vulns):
            print(f"Deleting finding with {len(wrong_vulns)} in {group}")
            rows.append(
                [
                    group,
                    finding.title[:3],
                    finding.id,
                    len(wrong_vulns),
                    "FINDING REMOVED",
                ],
            )

            await remove_finding(
                loaders,
                "flagos@fluidattacks.com",
                finding.id,
                StateRemovalJustification.FALSE_POSITIVE,
                Source.MACHINE,
            )

        elif len(wrong_vulns) > 0:
            print(f"Deleting {len(wrong_vulns)} vulns in {group}")
            rows.append(
                [
                    group,
                    finding.title[:3],
                    finding.id,
                    len(wrong_vulns),
                    "VULNS REMOVED",
                ],
            )

            await collect(
                tuple(
                    remove_vulnerability(
                        loaders=loaders,
                        finding_id=vuln.finding_id,
                        vulnerability_id=vuln.id,
                        justification=VulnerabilityStateReason.DUPLICATED,
                        email="flagos@fluidattacks.com",
                        include_closed_vuln=True,
                    )
                    for vuln in wrong_vulns
                ),
                workers=16,
            )

    with open("print_methods_deletion.csv", "a+", encoding="utf-8") as handler:
        writer = csv.writer(handler)
        writer.writerows(rows)


async def main() -> None:
    loaders = get_new_context()
    groups = sorted(await get_all_active_group_names(loaders))
    fin_enums = ("066", "234", "237", "379")
    for group in groups:
        await close_machine_vulnerabilities(loaders, group, fin_enums)


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    print(f"{execution_time}\n{finalization_time}")
