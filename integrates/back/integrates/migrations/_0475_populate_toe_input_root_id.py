"""
Populate the root id in the toe inputs.

Execution Time: 2024-01-22 at 19:57:11 UTC
Finalization Time: 2024-01-22 at 20:03:21 UTC

Execution Time: 2024-01-22 at 23:16:42 UTC
Finalization Time: 2024-01-22 at 23:17:19 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)
from boto3.dynamodb.conditions import (
    Attr,
    Key,
)
from botocore.exceptions import (
    ConnectTimeoutError,
    ReadTimeoutError,
)

from integrates.class_types.types import (
    Item,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.types import (
    PrimaryKey,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")
EMAIL_INTEGRATES = "integrates@fluidattacks.com"


async def _get_toe_inputs_by_group(
    group_name: str,
) -> tuple[Item, ...]:
    facet = TABLE.facets["toe_input_metadata"]
    primary_key = keys.build_key(
        facet=facet,
        values={"group_name": group_name},
    )
    index = None
    key_structure = TABLE.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(
                primary_key.sort_key.replace("#ROOT#COMPONENT#ENTRYPOINT", ""),
            )
        ),
        facets=(TABLE.facets["toe_input_metadata"],),
        index=index,
        table=TABLE,
    )
    return response.items


@retry_on_exceptions(
    exceptions=(ReadTimeoutError, ConnectTimeoutError),
    sleep_seconds=3,
)
async def process_toe_input(item: Item) -> None:
    key_structure = TABLE.primary_key
    state = item.get("state", {})
    await operations.update_item(
        condition_expression=Attr(key_structure.partition_key).exists(),
        item={"root_id": item.get("root_id", state.get("unreliable_root_id", ""))},
        key=PrimaryKey(partition_key=item["pk"], sort_key=item["sk"]),
        table=TABLE,
    )


async def process_group(group_name: str) -> None:
    group_toe_inputs = await _get_toe_inputs_by_group(group_name=group_name)
    await collect(
        tuple(
            process_toe_input(toe_input)
            for toe_input in group_toe_inputs
            if "root_id" not in toe_input
        ),
        workers=1000,
    )
    LOGGER_CONSOLE.info(
        "Group processed",
        extra={
            "extra": {
                "group_name": group_name,
                "group_toe_inputs": len(group_toe_inputs),
            },
        },
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    all_group_names = sorted(await orgs_domain.get_all_group_names(loaders))
    LOGGER_CONSOLE.info(
        "All group names",
        extra={
            "extra": {
                "total": len(all_group_names),
            },
        },
    )
    for count, group_name in enumerate(all_group_names, start=1):
        LOGGER_CONSOLE.info(
            "Group",
            extra={
                "extra": {
                    "group_name": group_name,
                    "count": count,
                },
            },
        )
        await process_group(group_name)


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
