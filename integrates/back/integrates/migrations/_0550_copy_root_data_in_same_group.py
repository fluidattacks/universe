"""
Migration used to copy information between roots
of the same group

Execution Time:    2024-06-05 at 12:50:52 UTC
Finalization Time: 2024-06-05 at 12:51:04 UTC
"""

import itertools
import logging
import logging.config
import time
from operator import (
    attrgetter,
)

from aioextensions import (
    collect,
    run,
)

from integrates.batch import (
    dal as batch_dal,
)
from integrates.batch.enums import (
    Action,
    IntegratesBatchQueue,
)
from integrates.batch_dispatch.move_root import (
    _process_finding,
    _process_toe_input,
    _process_toe_lines,
    _process_unsolved_events,
)
from integrates.custom_utils import (
    filter_vulnerabilities as filter_vulns_utils,
)
from integrates.custom_utils import (
    roots as root_utils,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    roots as roots_model,
)
from integrates.db_model.roots.types import (
    GitRoot,
    Root,
    RootEnvironmentUrlsRequest,
    URLRoot,
)
from integrates.db_model.toe_inputs.types import (
    GroupToeInputsRequest,
)
from integrates.db_model.toe_lines.types import (
    RootToeLinesRequest,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")


async def copy_root_environments(
    loaders: Dataloaders,
    source_root: Root,
    target_root: Root,
) -> None:
    environment_urls = await loaders.root_environment_urls.load(
        RootEnvironmentUrlsRequest(root_id=source_root.id, group_name=source_root.group_name),
    )

    result_environment = await collect(
        tuple(
            roots_model.add_root_environment_url(
                group_name=target_root.group_name,
                root_id=target_root.id,
                url=environment,
            )
            for environment in environment_urls
        ),
    )
    if all(result_environment):
        LOGGER_CONSOLE.info("Copied environment urls", extra=None)
    else:
        LOGGER_CONSOLE.info("Error in processing environment urls", extra=None)


async def copy_toes(
    loaders: Dataloaders,
    source_root: Root,
    target_root: Root,
) -> None:
    if isinstance(source_root, (GitRoot, URLRoot)):
        LOGGER_CONSOLE.info("Updating ToE inputs")
        group_toe_inputs = await loaders.group_toe_inputs.load_nodes(
            GroupToeInputsRequest(group_name=source_root.group_name),
        )
        root_toe_inputs = tuple(
            toe_input for toe_input in group_toe_inputs if toe_input.root_id == source_root.id
        )
        await collect(
            tuple(
                _process_toe_input(
                    loaders,
                    target_root.group_name,
                    target_root.id,
                    toe_input,
                )
                for toe_input in root_toe_inputs
            ),
        )
        await batch_dal.put_action(
            action=Action.REFRESH_TOE_INPUTS,
            entity=target_root.group_name,
            subject=root_utils.EMAIL_INTEGRATES,
            additional_info={"root_ids": [target_root.id]},
            queue=IntegratesBatchQueue.SMALL,
        )
    if isinstance(source_root, GitRoot):
        LOGGER_CONSOLE.info("Updating ToE lines")
        repo_toe_lines = await loaders.root_toe_lines.load_nodes(
            RootToeLinesRequest(group_name=source_root.group_name, root_id=source_root.id),
        )
        await collect(
            tuple(
                _process_toe_lines(
                    loaders,
                    target_root.group_name,
                    target_root.id,
                    toe_lines,
                )
                for toe_lines in repo_toe_lines
            ),
        )
        await batch_dal.put_action(
            action=Action.REFRESH_TOE_LINES,
            attempt_duration_seconds=7200,
            entity=target_root.group_name,
            subject=root_utils.EMAIL_INTEGRATES,
            additional_info={"root_ids": [target_root.id]},
            queue=IntegratesBatchQueue.SMALL,
        )


async def copy_vulnerabilities(loaders: Dataloaders, source_root: Root, target_root: Root) -> None:
    root_vulnerabilities = filter_vulns_utils.filter_non_deleted(
        await loaders.root_vulnerabilities.load(source_root.id),
    )
    vulns_by_finding = itertools.groupby(
        sorted(root_vulnerabilities, key=attrgetter("finding_id")),
        key=attrgetter("finding_id"),
    )
    event_ids = await _process_unsolved_events(
        item_subject=root_utils.EMAIL_INTEGRATES,
        loaders=loaders,
        source_group_name=source_root.group_name,
        source_root_id=source_root.id,
        target_group_name=target_root.group_name,
        target_root_id=target_root.id,
    )
    await collect(
        tuple(
            _process_finding(
                loaders=loaders,
                source_group_name=source_root.group_name,
                target_group_name=target_root.group_name,
                target_root_id=target_root.id,
                source_finding_id=source_finding_id,
                vulns=tuple(vulns),
                item_subject=root_utils.EMAIL_INTEGRATES,
                event_ids=event_ids,
            )
            for source_finding_id, vulns in vulns_by_finding
        ),
        workers=10,
    )
    LOGGER_CONSOLE.info("Vulnerabilities processing completed")


async def copy_root_data(
    loaders: Dataloaders,
    group_name: str,
    source_root_nickname: str,
    target_root_nickname: str,
) -> None:
    group_roots = {root.state.nickname: root for root in await loaders.group_roots.load(group_name)}
    source_root = group_roots[source_root_nickname]
    target_root = group_roots[target_root_nickname]
    await copy_root_environments(loaders, source_root, target_root)
    await copy_toes(loaders, source_root, target_root)
    await copy_vulnerabilities(loaders, source_root, target_root)
    LOGGER_CONSOLE.info("Root processing completed")


async def main() -> None:
    loaders: Dataloaders = get_new_context()

    await copy_root_data(
        loaders,
        "group_name",
        "source_root_nickname",
        "target_root_nickname",
    )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
