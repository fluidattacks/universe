"""
Populate the cvss_v4 field in vulnerabilities.
Execution Time:    2024-05-23 at 07:13:42 UTC
Finalization Time: 2024-05-23 at 07:38:14 UTC
"""

import contextlib
import logging
import logging.config
import time
from typing import (
    Any,
)

from aioextensions import (
    collect,
    run,
)
from botocore.exceptions import (
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
)
from cvss import (
    CVSS4,
)

from integrates.custom_exceptions import (
    VulnNotFound,
)
from integrates.custom_utils.criteria import (
    CRITERIA_VULNERABILITIES,
)
from integrates.custom_utils.cvss import (
    get_criteria_cvss4_vector,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.organizations.domain import (
    get_all_group_names,
)
from integrates.settings import (
    LOGGING,
)
from integrates.vulnerabilities.domain.core import (
    update_severity_score,
)
from integrates.vulnerability_files.utils import (
    get_from_v3,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")


@retry_on_exceptions(
    exceptions=(HTTPClientError, ReadTimeoutError, ConnectTimeoutError),
    sleep_seconds=10,
)
async def process_vuln(
    loaders: Dataloaders,
    finding: Finding,
    vuln: Vulnerability,
    criteria_item: dict[str, Any],
) -> None:
    if vuln.severity_score is None:
        return

    if not any(finding for code in ["011", "393"] if finding.title.startswith(code)):
        return
    cvss4_vector = get_criteria_cvss4_vector(criteria_item)
    if cvss4_vector != vuln.severity_score.cvss_v4:
        LOGGER_CONSOLE.info(
            "Vulnerability not updated",
            extra={
                "extra": {
                    "vuln_id": vuln.id,
                    "cvss_v3": vuln.severity_score.cvss_v3,
                    "old_cvss_v4": vuln.severity_score.cvss_v4,
                    "modified_date": vuln.created_date,
                },
            },
        )
        return

    cvss_v4 = str(get_from_v3(vuln.severity_score.cvss_v3))

    with contextlib.suppress(VulnNotFound):
        await update_severity_score(loaders, vuln.id, vuln.severity_score.cvss_v3, cvss_v4)

    LOGGER_CONSOLE.info(
        "Vulnerability updated",
        extra={
            "extra": {
                "vuln_id": vuln.id,
                "cvss_v3": vuln.severity_score.cvss_v3,
                "old_cvss_v4": vuln.severity_score.cvss_v4,
                "new_cvss_v4": CVSS4(cvss_v4).clean_vector(),
                "code": finding.title[:3],
                "modified_date": vuln.created_date,
            },
        },
    )


async def process_finding(loaders: Dataloaders, finding: Finding) -> None:
    if not any(finding for code in ["011", "393"] if finding.title.startswith(code)):
        return
    vulns = await loaders.finding_vulnerabilities_all.load(finding.id)

    await collect(
        [
            process_vuln(
                loaders,
                finding,
                vuln,
                CRITERIA_VULNERABILITIES[finding.title[:3]],
            )
            for vuln in vulns
        ],
        workers=16,
    )


async def process_group(group_name: str, progress: float) -> None:
    loaders = get_new_context()
    all_findings = await loaders.group_findings_all.load(group_name)

    await collect(
        [process_finding(loaders, finding) for finding in all_findings],
        workers=16,
    )

    LOGGER_CONSOLE.info(
        "Group updated",
        extra={
            "extra": {
                "group_name": group_name,
                "progress": str(round(progress, 2)),
            },
        },
    )


async def main() -> None:
    loaders = get_new_context()
    group_names = sorted(await get_all_group_names(loaders=loaders))

    for count, group in enumerate(group_names):
        await process_group(
            group_name=group,
            progress=count / len(group_names),
        )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
