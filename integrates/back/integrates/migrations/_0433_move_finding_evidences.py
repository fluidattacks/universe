"""
Detect migrated findings with no evidences and then try to add them.

Execution Time:    2023-09-08 at 20:58:11 UTC
Finalization Time: 2023-09-08 at 21:05:15 UTC
"""

import logging
import logging.config
import time
from contextlib import (
    suppress,
)
from io import (
    BytesIO,
)
from itertools import (
    groupby,
)

import aiofiles
from aioextensions import (
    collect,
    run,
)
from starlette.datastructures import (
    UploadFile,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.findings.types import (
    Finding,
    FindingEvidence,
)
from integrates.db_model.roots.enums import (
    RootStateReason,
    RootStatus,
)
from integrates.findings.domain.evidence import (
    EVIDENCE_NAMES,
    download_evidence_file,
    update_evidence,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


async def process_finding_with_not_evidence(
    loaders: Dataloaders,
    target_finding: Finding,
    source_finding: Finding,
) -> None:
    for evidence_id, evidence_name in EVIDENCE_NAMES.items():
        evidence: FindingEvidence | None = getattr(source_finding.evidences, evidence_name)
        if not evidence:
            continue
        loaders.finding.clear(target_finding.id)
        with suppress(Exception):
            file_path = await download_evidence_file(
                source_finding.group_name,
                source_finding.id,
                evidence.url,
            )
            async with aiofiles.open(file_path, "rb") as new_file:
                file_contents = await new_file.read()
                await update_evidence(
                    loaders=loaders,
                    finding_id=target_finding.id,
                    evidence_id=evidence_id,
                    file_object=UploadFile(
                        filename=str(new_file.name),
                        file=BytesIO(file_contents),
                    ),
                    author_email=evidence.author_email,
                    is_draft=evidence.is_draft,
                    description=evidence.description,
                )


def _get_source_finding_for_target_finding(
    target_finding: Finding,
    source_findings_by_title: dict[str, tuple[Finding, ...]],
) -> Finding | None:
    same_type_of_findings = source_findings_by_title.get(target_finding.title)
    if same_type_of_findings is None:
        return None
    return next(
        (
            source_finding
            for source_finding in same_type_of_findings
            if source_finding.creation and target_finding.creation
            if source_finding.creation.modified_by == target_finding.creation.modified_by
            if source_finding.creation.modified_date == target_finding.creation.modified_date
            if source_finding.creation.source == target_finding.creation.source
        ),
        None,
    )


async def process_target_group(
    loaders: Dataloaders,
    target_group_name: str,
    source_findings_by_title: dict[str, tuple[Finding, ...]],
) -> None:
    findings_with_no_evidences = [
        finding
        for finding in await loaders.group_findings.load(target_group_name)
        if not any(finding.evidences)
    ]
    findings_to_process = [
        (target_finding, source_finding)
        for target_finding in findings_with_no_evidences
        if (
            source_finding := _get_source_finding_for_target_finding(
                target_finding,
                source_findings_by_title,
            )
        )
    ]
    await collect(
        [
            process_finding_with_not_evidence(loaders, target_finding, source_finding)
            for target_finding, source_finding in findings_to_process
        ],
        workers=50,
    )
    LOGGER_CONSOLE.info(
        "Target group processed %s %s",
        target_group_name,
        f"{len(findings_to_process)=}",
    )


async def process_group(
    *,
    group_name: str,
    progress: float,
) -> None:
    loaders = get_new_context()
    moved_roots = [
        root
        for root in await loaders.group_roots.load(group_name)
        if root.state.status is RootStatus.INACTIVE
        and root.state.reason == RootStateReason.MOVED_TO_ANOTHER_GROUP
    ]
    if not moved_roots:
        return
    findings = await loaders.group_findings.load(group_name)
    findings_by_title = {
        title: tuple(findings)
        for title, findings in groupby(
            sorted(findings, key=lambda finding: finding.title),
            key=lambda finding: finding.title,
        )
    }
    target_groups = {root.state.other for root in moved_roots if root.state.other}
    for target_group in target_groups:
        await process_target_group(loaders, target_group, findings_by_title)

    LOGGER_CONSOLE.info("Group processed %s %s", group_name, f"{round(progress, 2)!s}")


async def main() -> None:
    loaders = get_new_context()
    group_names = sorted(await orgs_domain.get_all_active_group_names(loaders))
    LOGGER_CONSOLE.info("%s", f"{group_names=}")
    LOGGER_CONSOLE.info("%s", f"{len(group_names)=}")
    for count, group_name in enumerate(group_names):
        LOGGER_CONSOLE.info("Group %s %s", group_name, str(count))
        await process_group(
            group_name=group_name,
            progress=count / len(group_names),
        )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
