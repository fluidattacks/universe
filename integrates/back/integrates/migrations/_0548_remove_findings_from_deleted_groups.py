"""
Remove remaining findings from groups that have been deleted

Start Time: 2024-05-29 at 16:03:32 UTC
Finalization Time: 2024-05-29 at 16:03:48 UTC

Start Time: 2024-05-30 at 19:04:07 UTC
Finalization Time: 2024-05-30 at 19:04:18 UTC

Start Time: 2024-05-30 at 22:44:02 UTC
Finalization Time: 2024-05-30 at 22:44:58 UTC
"""

import logging
import logging.config
import time
from typing import (
    cast,
)

from aioextensions import (
    collect,
    run,
)
from boto3.dynamodb.conditions import (
    Key,
)

from integrates.class_types.types import (
    Item,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model import (
    findings as findings_model,
)
from integrates.db_model import (
    vulnerabilities as vulns_model,
)
from integrates.db_model.findings.enums import (
    FindingStateStatus,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.db_model.findings.utils import (
    format_finding,
)
from integrates.db_model.items import (
    FindingItem,
    VulnerabilityItem,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
)
from integrates.db_model.vulnerabilities.utils import (
    format_vulnerability,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.finding_comments import (
    domain as comments_domain,
)
from integrates.findings import (
    domain as findings_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


def _get_criteria_code(title: str) -> str:
    return title.split(".")[0].strip()


async def _get_findings_by_group(
    group_name: str,
) -> tuple[Item, ...]:
    primary_key = keys.build_key(
        facet=TABLE.facets["finding_metadata"],
        values={"group_name": group_name},
    )
    index = TABLE.indexes["inverted_index"]
    key_structure = index.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.sort_key)
            & Key(key_structure.sort_key).begins_with(primary_key.partition_key)
        ),
        facets=(TABLE.facets["finding_metadata"],),
        index=index,
        table=TABLE,
    )

    return response.items


async def _remove_finding_without_masking(item: Item) -> None:
    await findings_model.remove(
        created_by=item["creation"]["modified_by"],
        finding_code=_get_criteria_code(item["title"]),
        finding_id=item["id"],
        group_name=item["group_name"],
    )
    LOGGER_CONSOLE.info(
        "Finding %s from group %s has been removed",
        item["id"],
        item["group_name"],
    )


async def _get_finding_vulnerabilities(*, finding_id: str) -> tuple[VulnerabilityItem, ...]:
    primary_key = keys.build_key(
        facet=TABLE.facets["vulnerability_metadata"],
        values={"finding_id": finding_id},
    )

    index = TABLE.indexes["inverted_index"]
    key_structure = index.primary_key
    response = await operations.DynamoClient[VulnerabilityItem].query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.sort_key)
            & Key(key_structure.sort_key).begins_with(primary_key.partition_key)
        ),
        facets=(TABLE.facets["vulnerability_metadata"],),
        table=TABLE,
        index=index,
    )

    return response.items


async def _mask_and_remove_vulnerability(
    *,
    email: str,
    finding_id: str,
    vulnerability_item: VulnerabilityItem,
) -> None:
    vulnerability_id: str = vulnerability_item["pk"].split("#")[1]
    try:
        vulnerability = format_vulnerability(vulnerability_item)
        if vulnerability.state.status == VulnerabilityStateStatus.DELETED:
            await vulns_model.update_historic_entry(
                current_value=vulnerability,
                entry=vulnerability.state._replace(
                    modified_by=email,
                    modified_date=datetime_utils.get_utc_now(),
                    status=VulnerabilityStateStatus.MASKED,
                ),
                finding_id=finding_id,
                vulnerability_id=vulnerability.id,
                force_update=True,
            )
        await vulns_model.remove(vulnerability_id=vulnerability.id)
    except KeyError:
        await vulns_model.remove(vulnerability_id=vulnerability_id)

    LOGGER_CONSOLE.info(
        "Vulnerability Removed",
        extra={
            "extra": {
                "vulnerability_id": vulnerability_id,
                "finding_id": finding_id,
            },
        },
    )


async def _mask_and_remove_finding(finding: Finding, email: str) -> None:
    await comments_domain.remove_comments(finding_id=finding.id)
    await findings_domain.remove_all_evidences(finding.id, finding.group_name)

    vulnerabilities = await _get_finding_vulnerabilities(finding_id=finding.id)
    await collect(
        tuple(
            _mask_and_remove_vulnerability(
                email=email,
                finding_id=finding.id,
                vulnerability_item=vulnerability,
            )
            for vulnerability in vulnerabilities
        ),
        workers=8,
    )

    if finding.state.status == FindingStateStatus.DELETED and bool(
        finding.unreliable_indicators.vulnerabilities_summary.open
        + finding.unreliable_indicators.vulnerabilities_summary.closed,
    ):
        await findings_model.update_state(
            current_value=finding.state,
            finding_id=finding.id,
            group_name=finding.group_name,
            state=finding.state._replace(
                modified_by=email,
                modified_date=datetime_utils.get_utc_now(),
                status=FindingStateStatus.MASKED,
            ),
        )

    await findings_model.remove(
        created_by=finding.creation.modified_by if finding.creation else finding.state.modified_by,
        finding_code=finding.get_criteria_code(),
        finding_id=finding.id,
        group_name=finding.group_name,
    )
    LOGGER_CONSOLE.info(
        "Finding masked",
        extra={
            "extra": {
                "finding_id": finding.id,
                "group_name": finding.group_name,
            },
        },
    )


def extract_group_name(item: dict[str, str]) -> str:
    return item["sk"].replace("GROUP#", "")


async def _get_all_groups_names() -> list[str] | None:
    primary_key = keys.build_key(
        facet=TABLE.facets["group_id"],
        values={"name": ""},
    )

    key_structure = TABLE.primary_key
    response = await operations.query(
        condition_expression=(Key(key_structure.partition_key).eq(primary_key.partition_key)),
        facets=(TABLE.facets["group_id"],),
        table=TABLE,
    )

    if not response.items:
        return None

    return [extract_group_name(item) for item in response.items]


async def remove_all_findings_if_group_deleted(group_name: str) -> None:
    loaders = get_new_context()
    group = await loaders.group.load(group_name)
    if group is None:
        all_findings = await _get_findings_by_group(group_name)
        findings_to_mask: list[Finding] = []
        finding_to_remove_without_masking: list[Item] = []
        for item in all_findings:
            try:
                findings_to_mask.append(format_finding(cast(FindingItem, item)))
            except KeyError:
                finding_to_remove_without_masking.append(item)

        await collect(
            tuple(
                _mask_and_remove_finding(finding, "integrates@fluidattacks.com")
                for finding in findings_to_mask
            ),
            workers=10,
        )
        await collect(
            tuple(
                _remove_finding_without_masking(finding)
                for finding in finding_to_remove_without_masking
            ),
            workers=10,
        )
        if all_findings:
            LOGGER_CONSOLE.info(
                "All findings from %s have been removed",
                group_name,
                extra={"extra": {"count": len(all_findings)}},
            )


async def main() -> None:
    all_groups_names = await _get_all_groups_names()
    if all_groups_names is None:
        LOGGER_CONSOLE.info("No groups found")
        return

    await collect(
        tuple(remove_all_findings_if_group_deleted(group_name) for group_name in all_groups_names),
        workers=10,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:    %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("%s", execution_time)
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
