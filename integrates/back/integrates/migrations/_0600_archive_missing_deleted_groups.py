"""
Archive in Redshift the data related to some missing DELETED groups.
Data is taken from restored backup "integrates_vms" tables.

Facets to process:
events
findings
groups
organizations
roots
toe_inputs
toe_lines
vulnerabilities

"""

import hashlib
import logging
import logging.config
import sys
import time
from datetime import (
    datetime,
    timedelta,
)
from itertools import (
    chain,
)

from aioextensions import (
    collect,
    run,
)
from boto3.dynamodb.conditions import (
    Key,
)

from integrates.archive import (
    domain as archive_domain,
)
from integrates.class_types.types import (
    Item,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model import (
    TABLE as INTEGRATES_VMS_TABLE,
)
from integrates.db_model.organizations.utils import (
    remove_org_id_prefix,
)
from integrates.db_model.utils import (
    archive as db_model_archive,
)
from integrates.db_model.utils import (
    get_as_utc_iso_format,
)
from integrates.db_model.vulnerabilities.constants import (
    GROUP_INDEX_METADATA as GROUP_INDEX_METADATA_FACET,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.types import (
    Facet,
    Index,
    PrimaryKey,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger("console")
BACKUP_TABLE_NAME = "vms_backup_aug_01"
BACKUP_TABLE = INTEGRATES_VMS_TABLE._replace(name=BACKUP_TABLE_NAME)
INVERTED_INDEX = BACKUP_TABLE.indexes["inverted_index"]
GSI_5_INDEX = BACKUP_TABLE.indexes["gsi_5"]
EMAIL_INTEGRATES = "integrates@fluidattacks.com"
MASK_STATES: bool = False
GROUP_NAMES: list[str] = []  # MASKED


def encode_for_archive(raw: str) -> str:
    return hashlib.shake_256(raw.encode("utf-8")).hexdigest(10)


async def run_query_on_backup(
    primary_key: PrimaryKey,
    facets: tuple[Facet, ...],
    index: Index | None = None,
) -> tuple[Item, ...]:
    if index:
        key_structure = index.primary_key
        condition_expression = Key(key_structure.partition_key).eq(primary_key.sort_key) & Key(
            key_structure.sort_key,
        ).begins_with(primary_key.partition_key)
    else:
        key_structure = BACKUP_TABLE.primary_key
        condition_expression = Key(key_structure.partition_key).eq(primary_key.partition_key) & Key(
            key_structure.sort_key,
        ).begins_with(primary_key.sort_key)

    response = await operations.query(
        condition_expression=condition_expression,
        facets=facets,
        index=index,
        table=BACKUP_TABLE,
    )

    return response.items


async def _process_group_events(group_name: str) -> None:
    items = await run_query_on_backup(
        primary_key=keys.build_key(
            facet=BACKUP_TABLE.facets["event_metadata"],
            values={"name": group_name},
        ),
        facets=(BACKUP_TABLE.facets["event_metadata"],),
        index=INVERTED_INDEX,
    )
    if not items:
        return

    await db_model_archive(
        items,
        archive_domain.archive_events_metadata,
    )
    LOGGER.info("Processed events: %s", group_name)


def _get_vuln_masked_state_item(vuln_id: str, raw: Item) -> Item | None:
    if raw["status"] != "DELETED":
        return None

    key_structure = BACKUP_TABLE.primary_key
    modified_date_str = get_as_utc_iso_format(
        datetime.fromisoformat(raw["modified_date"]) + timedelta(seconds=1),
    )
    primary_key = keys.build_key(
        facet=BACKUP_TABLE.facets["vulnerability_historic_state"],
        values={
            "id": vuln_id,
            "iso8601utc": modified_date_str,
        },
    )

    return {
        **raw,
        key_structure.partition_key: primary_key.partition_key,
        key_structure.sort_key: primary_key.sort_key,
        "modified_date": modified_date_str,
        "modified_by": EMAIL_INTEGRATES,
        "status": "MASKED",
    }


async def _get_vuln_historic_state(
    metadata_item: Item,
) -> tuple[Item, ...]:
    vuln_id = str(metadata_item["pk"]).split("#")[1]
    state_items = await run_query_on_backup(
        primary_key=keys.build_key(
            facet=BACKUP_TABLE.facets["vulnerability_historic_state"],
            values={"id": vuln_id},
        ),
        facets=(BACKUP_TABLE.facets["vulnerability_historic_state"],),
    )
    if MASK_STATES and (
        masked_state := _get_vuln_masked_state_item(vuln_id=vuln_id, raw=metadata_item["state"])
    ):
        state_items = (*state_items, masked_state)

    return state_items


async def _get_vuln_historic_treatment(
    metadata_item: Item,
) -> tuple[Item, ...]:
    return await run_query_on_backup(
        primary_key=keys.build_key(
            facet=BACKUP_TABLE.facets["vulnerability_historic_treatment"],
            values={"id": str(metadata_item["pk"]).split("#")[1]},
        ),
        facets=(BACKUP_TABLE.facets["vulnerability_historic_treatment"],),
    )


async def _get_vuln_historic_verification(
    metadata_item: Item,
) -> tuple[Item, ...]:
    return await run_query_on_backup(
        primary_key=keys.build_key(
            facet=BACKUP_TABLE.facets["vulnerability_historic_verification"],
            values={"id": str(metadata_item["pk"]).split("#")[1]},
        ),
        facets=(BACKUP_TABLE.facets["vulnerability_historic_verification"],),
    )


async def _get_vuln_historic_zero_risk(
    metadata_item: Item,
) -> tuple[Item, ...]:
    return await run_query_on_backup(
        primary_key=keys.build_key(
            facet=BACKUP_TABLE.facets["vulnerability_historic_zero_risk"],
            values={"id": str(metadata_item["pk"]).split("#")[1]},
        ),
        facets=(BACKUP_TABLE.facets["vulnerability_historic_zero_risk"],),
    )


async def _get_finding_vulns(finding_id: str) -> tuple[Item, ...]:
    return await run_query_on_backup(
        primary_key=keys.build_key(
            facet=BACKUP_TABLE.facets["vulnerability_metadata"],
            values={"finding_id": finding_id},
        ),
        facets=(BACKUP_TABLE.facets["vulnerability_metadata"],),
        index=INVERTED_INDEX,
    )


async def _get_group_findings(group_name: str) -> tuple[Item, ...]:
    return await run_query_on_backup(
        primary_key=keys.build_key(
            facet=BACKUP_TABLE.facets["finding_metadata"],
            values={"group_name": group_name},
        ),
        facets=(BACKUP_TABLE.facets["finding_metadata"],),
        index=INVERTED_INDEX,
    )


async def _process_group_vulns(group_name: str) -> None:
    primary_key = keys.build_key(
        facet=GROUP_INDEX_METADATA_FACET,
        values={"group_name": group_name},
    )
    key_structure = GSI_5_INDEX.primary_key
    response = await operations.query(
        condition_expression=(Key(key_structure.partition_key).eq(primary_key.partition_key)),
        facets=(BACKUP_TABLE.facets["vulnerability_metadata"],),
        table=BACKUP_TABLE,
        index=GSI_5_INDEX,
    )
    metadata_items = response.items
    if not metadata_items:
        finding_metadata_items = await _get_group_findings(group_name)
        metadata_items = tuple(
            chain.from_iterable(
                await collect(
                    [
                        _get_finding_vulns(finding_id=str(finding["pk"]).split("#")[1])
                        for finding in finding_metadata_items
                    ],
                    workers=4,
                ),
            ),
        )
        if not metadata_items:
            LOGGER.warning("Group %s WITHOUT vulns in backup table", group_name)

            return

    await db_model_archive(
        metadata_items,
        archive_domain.archive_vulnerabilities_metadata,
    )

    historic_state_items = tuple(
        chain.from_iterable(
            await collect(
                [_get_vuln_historic_state(metadata_item) for metadata_item in metadata_items],
                workers=8,
            ),
        ),
    )
    await db_model_archive(
        historic_state_items,
        archive_domain.archive_vulnerabilities_state,
    )

    historic_treatment_items = tuple(
        chain.from_iterable(
            await collect(
                [_get_vuln_historic_treatment(metadata_item) for metadata_item in metadata_items],
                workers=8,
            ),
        ),
    )
    await db_model_archive(
        historic_treatment_items,
        archive_domain.archive_vulnerabilities_treatment,
    )

    historic_verification_items = tuple(
        chain.from_iterable(
            await collect(
                [
                    _get_vuln_historic_verification(metadata_item)
                    for metadata_item in metadata_items
                ],
                workers=8,
            ),
        ),
    )
    await db_model_archive(
        historic_verification_items,
        archive_domain.archive_vulnerabilities_verification,
    )

    historic_zero_risk_items = tuple(
        chain.from_iterable(
            await collect(
                [_get_vuln_historic_zero_risk(metadata_item) for metadata_item in metadata_items],
                workers=8,
            ),
        ),
    )
    await db_model_archive(
        historic_zero_risk_items,
        archive_domain.archive_vulnerabilities_zero_risk,
    )

    LOGGER.info("Processed vulnerabilities: %s", group_name)


def _get_finding_masked_state_item(finding_id: str, raw: Item) -> Item | None:
    if raw["status"] != "DELETED":
        return None

    key_structure = BACKUP_TABLE.primary_key
    modified_date_str = get_as_utc_iso_format(
        datetime.fromisoformat(raw["modified_date"]) + timedelta(seconds=1),
    )
    primary_key = keys.build_key(
        facet=BACKUP_TABLE.facets["finding_historic_state"],
        values={
            "id": finding_id,
            "iso8601utc": modified_date_str,
        },
    )

    return {
        **raw,
        key_structure.partition_key: primary_key.partition_key,
        key_structure.sort_key: primary_key.sort_key,
        "modified_date": modified_date_str,
        "modified_by": EMAIL_INTEGRATES,
        "status": "MASKED",
    }


async def _get_finding_historic_state(
    metadata_item: Item,
) -> tuple[Item, ...]:
    finding_id = str(metadata_item["pk"]).split("#")[1]
    state_items = await run_query_on_backup(
        primary_key=keys.build_key(
            facet=BACKUP_TABLE.facets["finding_historic_state"],
            values={"id": finding_id},
        ),
        facets=(BACKUP_TABLE.facets["finding_historic_state"],),
    )
    if MASK_STATES and (
        masked_state := _get_finding_masked_state_item(
            finding_id=finding_id,
            raw=metadata_item["state"],
        )
    ):
        state_items = (*state_items, masked_state)

    return state_items


async def _get_finding_historic_verification(
    metadata_item: Item,
) -> tuple[Item, ...]:
    return await run_query_on_backup(
        primary_key=keys.build_key(
            facet=BACKUP_TABLE.facets["finding_historic_verification"],
            values={"id": str(metadata_item["pk"]).split("#")[1]},
        ),
        facets=(BACKUP_TABLE.facets["finding_historic_verification"],),
    )


async def _process_group_findings(group_name: str) -> None:
    metadata_items = await _get_group_findings(group_name)
    await db_model_archive(
        metadata_items,
        archive_domain.archive_findings_metadata,
    )

    historic_state_items = tuple(
        chain.from_iterable(
            await collect(
                [_get_finding_historic_state(metadata_item) for metadata_item in metadata_items],
                workers=8,
            ),
        ),
    )
    await db_model_archive(
        historic_state_items,
        archive_domain.archive_findings_state,
    )

    historic_verification_items = tuple(
        chain.from_iterable(
            await collect(
                [
                    _get_finding_historic_verification(metadata_item)
                    for metadata_item in metadata_items
                ],
                workers=8,
            ),
        ),
    )
    await db_model_archive(
        historic_verification_items,
        archive_domain.archive_findings_verification,
    )

    LOGGER.info("Processed findings: %s", group_name)


async def _process_group_roots(group_name: str) -> None:
    metadata_items = await run_query_on_backup(
        primary_key=keys.build_key(
            facet=BACKUP_TABLE.facets["git_root_metadata"],
            values={"name": group_name},
        ),
        facets=(
            BACKUP_TABLE.facets["git_root_metadata"],
            BACKUP_TABLE.facets["ip_root_metadata"],
            BACKUP_TABLE.facets["url_root_metadata"],
        ),
        index=INVERTED_INDEX,
    )
    await db_model_archive(
        metadata_items,
        archive_domain.archive_roots_metadata,
    )
    LOGGER.info("Processed roots: %s", group_name)


async def _process_organization(org_id: str) -> None:
    if await get_new_context().organization.load(org_id):
        LOGGER.info("Organization still present on main table: %s", org_id)

        return

    items = await run_query_on_backup(
        primary_key=keys.build_key(
            facet=BACKUP_TABLE.facets["organization_metadata"],
            values={"id": remove_org_id_prefix(org_id)},
        ),
        facets=(BACKUP_TABLE.facets["organization_metadata"],),
    )
    if not items:
        LOGGER.warning("Organization %s NOT found in backup table", org_id)

        return

    state_items = await run_query_on_backup(
        primary_key=keys.build_key(
            facet=BACKUP_TABLE.facets["organization_historic_state"],
            values={"id": remove_org_id_prefix(org_id)},
        ),
        facets=(BACKUP_TABLE.facets["organization_historic_state"],),
    )

    await db_model_archive((*items, *state_items), archive_domain.archive_organization)
    LOGGER.info("Processed organization: %s", org_id)


async def _process_group_toe_inputs(group_name: str) -> None:
    primary_key = keys.build_key(
        facet=BACKUP_TABLE.facets["toe_input_metadata"],
        values={"group_name": group_name},
    )
    key_structure = BACKUP_TABLE.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(
                primary_key.sort_key.replace("#COMPONENT#ENTRYPOINT", ""),
            )
        ),
        facets=(BACKUP_TABLE.facets["toe_input_metadata"],),
        table=BACKUP_TABLE,
    )
    if not response.items:
        return

    await db_model_archive(response.items, archive_domain.archive_group_toe_inputs)
    LOGGER.info("Processed ToE inputs: %s", group_name)


async def _process_group_toe_lines(group_name: str) -> None:
    primary_key = keys.build_key(
        facet=BACKUP_TABLE.facets["toe_lines_metadata"],
        values={"group_name": group_name},
    )
    key_structure = BACKUP_TABLE.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key.replace("#FILENAME", ""))
        ),
        facets=(BACKUP_TABLE.facets["toe_lines_metadata"],),
        table=BACKUP_TABLE,
    )
    if not response.items:
        return

    await db_model_archive(response.items, archive_domain.archive_group_toe_lines)
    LOGGER.info("Processed ToE lines: %s", group_name)


def _get_group_deleted_state_item(group_name: str, raw: Item) -> Item | None:
    if raw["status"] == "DELETED":
        return None

    key_structure = BACKUP_TABLE.primary_key
    modified_date_str = get_as_utc_iso_format(
        datetime.fromisoformat(raw["modified_date"]) + timedelta(seconds=1),
    )
    primary_key = keys.build_key(
        facet=BACKUP_TABLE.facets["group_historic_state"],
        values={
            "name": group_name,
            "iso8601utc": modified_date_str,
        },
    )

    return {
        **raw,
        key_structure.partition_key: primary_key.partition_key,
        key_structure.sort_key: primary_key.sort_key,
        "comments": "Added for data consistency",
        "has_essential": False,
        "has_advanced": False,
        "has_machine": False,
        "has_squad": False,
        "justification": "MIGRATION",
        "modified_date": modified_date_str,
        "modified_by": EMAIL_INTEGRATES,
        "status": "DELETED",
    }


async def _process_group(group_name: str) -> None:
    LOGGER.info("Processing group: %s, %s", group_name, encode_for_archive(group_name))
    items = await run_query_on_backup(
        primary_key=keys.build_key(
            facet=BACKUP_TABLE.facets["group_metadata"],
            values={"name": group_name},
        ),
        facets=(BACKUP_TABLE.facets["group_metadata"],),
    )
    if not items:
        LOGGER.warning("Group %s NOT found in backup table", group_name)

        return

    await _process_group_vulns(group_name)
    await _process_group_findings(group_name)
    await _process_group_events(group_name)
    await _process_group_roots(group_name)
    await _process_organization(items[0]["sk"])
    await _process_group_toe_inputs(group_name)
    await _process_group_toe_lines(group_name)

    metadata_item = items[0]
    state_items = await run_query_on_backup(
        primary_key=keys.build_key(
            facet=BACKUP_TABLE.facets["group_historic_state"],
            values={"name": group_name},
        ),
        facets=(BACKUP_TABLE.facets["group_historic_state"],),
    )
    if MASK_STATES and (
        deleted_state := _get_group_deleted_state_item(
            group_name=group_name,
            raw=metadata_item["state"],
        )
    ):
        metadata_item["state"] = deleted_state
        state_items = (*state_items, deleted_state)

    indicators_items = await run_query_on_backup(
        primary_key=keys.build_key(
            facet=BACKUP_TABLE.facets["group_unreliable_indicators"],
            values={"name": group_name},
        ),
        facets=(BACKUP_TABLE.facets["group_unreliable_indicators"],),
    )
    await db_model_archive(
        (
            metadata_item,
            *state_items,
            *indicators_items,
        ),
        archive_domain.archive_group,
    )
    LOGGER.info("Processed group: %s", group_name)


async def main(*args: str) -> None:
    groups_to_process: list[str] = GROUP_NAMES
    raw_groups: list[str] = args[0].split(",")
    groups_to_process.extend([group.strip() for group in raw_groups if len(group.strip())])

    global BACKUP_TABLE_NAME, BACKUP_TABLE, MASK_STATES
    BACKUP_TABLE_NAME = args[1].strip() or BACKUP_TABLE_NAME
    BACKUP_TABLE = INTEGRATES_VMS_TABLE._replace(name=BACKUP_TABLE_NAME)

    if len(args) > 2 and args[2].strip() == "mask":
        MASK_STATES = True

    entity_to_archive: str = "all"
    if len(args) > 3 and args[3].strip():
        entity_to_archive = args[3].strip().lower()

    for group_name in groups_to_process:
        match entity_to_archive:
            case "vulns":
                await _process_group_vulns(group_name)
            case "findings":
                await _process_group_findings(group_name)
            case "events":
                await _process_group_events(group_name)
            case "roots":
                await _process_group_roots(group_name)
            case "toe_inputs":
                await _process_group_toe_inputs(group_name)
            case "toe_lines":
                await _process_group_toe_lines(group_name)
            case _:
                await _process_group(group_name)


if __name__ == "__main__":
    migration_args = sys.argv[1:]
    LOGGER.info("Arguments passed down to the script: %s", str(migration_args))
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main(*migration_args))
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER.info("\n%s\n%s", execution_time, finalization_time)
