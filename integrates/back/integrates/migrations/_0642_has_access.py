"""
Update has_access state
Execution Time:    2024-12-09 at 13:27:50 UTC
Finalization Time: 2024-12-09 at 13:29:11 UTC
"""

import logging

from aioextensions import collect

from integrates.dataloaders import get_new_context
from integrates.db_model.organization_access import update_metadata
from integrates.db_model.organization_access.types import (
    OrganizationAccess,
    OrganizationAccessMetadataToUpdate,
)
from integrates.db_model.organizations.get import iterate_organizations
from integrates.db_model.organizations.types import Organization
from integrates.migrations.utils import log_time

LOGGER = logging.getLogger("migrations")


async def process_access(access: OrganizationAccess) -> None:
    await update_metadata(
        email=access.email,
        organization_id=access.organization_id,
        metadata=OrganizationAccessMetadataToUpdate(
            state=access.state._replace(
                has_access=True,
            ),
        ),
    )


async def process_organization(organization: Organization) -> None:
    loaders = get_new_context()
    organization_access = [
        access
        for access in await loaders.organization_stakeholders_access.load(
            organization.id,
        )
        if access.state.has_access is None and access.state.invitation is None
    ]
    await collect([process_access(access) for access in organization_access], workers=16)


@log_time(LOGGER)
async def main() -> None:
    async for organization in iterate_organizations():
        await process_organization(organization)


if __name__ == "__main__":
    main()  # type: ignore[unused-coroutine]
