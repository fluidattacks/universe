"""
Distribute okada mailmap entries to other organizations and remove entries
belonging to non-existent organizations

This migration script distributes the okada mailmap entries to their
corresponding organizations based on the email domains. If it finds an
entry belonging to a non-existent organization, determined by its domain, the
entry is removed.

This version ignores domains that don't match any organization and is
case-insensitive, allowing mailmap entries to be moved to organizations
even when the domains are upper-cased or contain generic domains like gmail.

This migration focuses on organizations with alternative email domains and
will remove entries associated with non-existent organizations.

This migration is to be executed locally.

To run the migration you will need:

1. A CSV file mapping each organization name to their email domain(s).
2. To set the path to this CSV file in the ORGANIZATIONS_FILE_PATH variable.

The CSV file must have two columns:

1. "name": The name of the organization
2. "domain": The email domain associated with the organization

If your CSV file uses different column names, you will need to adjust the
script accordingly.

Note: Ensure that the CSV file is properly formatted and contains all necessary
organization-domain mappings before running the script.

Start Time:        2024-08-28 at 20:07:47 UTC
Finalization Time: 2024-08-28 at 20:31:32 UTC

"""

import csv
import logging
import logging.config
import time

from aioextensions import (
    run,
)

from integrates.custom_exceptions import (
    MailmapEntryAlreadyExists,
    MailmapEntryNotFound,
    MailmapOrganizationNotFound,
    MailmapSubentryAlreadyExists,
    MailmapSubentryNotFound,
)
from integrates.mailmap.delete import (
    delete_mailmap_entry_with_subentries,
)
from integrates.mailmap.get import (
    get_mailmap_entries_with_subentries,
)
from integrates.mailmap.update import (
    move_mailmap_entry_with_subentries,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


ORGANIZATION_ID = "okada"
ORGANIZATIONS_FILE_PATH = ""


def build_orgs_domain_name_map(file_path: str) -> dict[str, str]:
    orgs_domain_name_map: dict[str, str] = {}

    with open(file_path, newline="", encoding="utf-8") as csv_file:
        reader = csv.DictReader(csv_file)
        for row in reader:
            domain: str = row["domain"].strip()
            name: str = row["name"]
            if domain:
                orgs_domain_name_map[domain] = name

    return orgs_domain_name_map


async def main() -> None:
    orgs_domain_name_map = build_orgs_domain_name_map(ORGANIZATIONS_FILE_PATH)

    entries_with_subentries = await get_mailmap_entries_with_subentries(
        organization_id=ORGANIZATION_ID,
    )

    moved_entries = 0
    removed_entries = 0
    for entry_with_subentries in entries_with_subentries:
        entry = entry_with_subentries["entry"]
        subentries = entry_with_subentries["subentries"]

        entry_email = entry["mailmap_entry_email"]
        subentries_emails = [subentry["mailmap_subentry_email"] for subentry in subentries]

        emails = [entry_email] + subentries_emails
        lowercased_emails = [email.lower() for email in emails]

        matched_orgs = {
            org_name
            for org_domain, org_name in orgs_domain_name_map.items()
            if any(email.endswith(org_domain) for email in lowercased_emails)
        }

        if len(matched_orgs) == 1:
            matched_org_name = next(iter(matched_orgs))
            LOGGER_CONSOLE.info(
                "Mailmap entry %s with emails %s can be moved to %s\n\n",
                entry_email,
                emails,
                matched_org_name,
            )
            try:
                await move_mailmap_entry_with_subentries(
                    entry_email=entry_email,
                    organization_id=ORGANIZATION_ID,
                    new_organization_id=matched_org_name,
                )
                LOGGER_CONSOLE.info(
                    "Mailmap entry %s moved to %s successfully\n\n",
                    entry_email,
                    matched_org_name,
                )
                moved_entries += 1
            except MailmapOrganizationNotFound as ex:
                LOGGER_CONSOLE.error("%s\n\n", ex)
                LOGGER_CONSOLE.info(
                    "Mailmap entry %s: %s - %s can be removed\n\n",
                    entry_email,
                    subentries_emails,
                    matched_org_name,
                )
                try:
                    await delete_mailmap_entry_with_subentries(
                        entry_email=entry_email,
                        organization_id=ORGANIZATION_ID,
                    )
                    LOGGER_CONSOLE.info(
                        "Mailmap entry %s - %s removed successfully\n\n",
                        entry_email,
                        matched_org_name,
                    )
                    removed_entries += 1
                except (
                    MailmapEntryAlreadyExists,
                    MailmapEntryNotFound,
                    MailmapSubentryAlreadyExists,
                    MailmapSubentryNotFound,
                ):
                    LOGGER_CONSOLE.error("%s\n\n", ex)
            except (
                MailmapEntryAlreadyExists,
                MailmapEntryNotFound,
                MailmapSubentryAlreadyExists,
                MailmapSubentryNotFound,
            ) as ex:
                LOGGER_CONSOLE.error("%s\n\n", ex)

    LOGGER_CONSOLE.info(
        "Distributed entries: %s out of %s\n\n",
        moved_entries,
        len(entries_with_subentries),
    )

    LOGGER_CONSOLE.info(
        "Deleted entries: %s out of %s\n\n",
        removed_entries,
        len(entries_with_subentries),
    )


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:        %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
