"""
Remove toe port historic state in integrates_vms table.

Start Time: 2024-03-08 at 20:52:42 UTC
Finalization Time: 2024-03-08 at 20:52:51 UTC
"""

import asyncio
import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)
from aiohttp.client_exceptions import (
    ClientConnectorError,
    ClientOSError,
    ClientPayloadError,
    ServerDisconnectedError,
    ServerTimeoutError,
)
from boto3.dynamodb.conditions import (
    Key,
)
from botocore.exceptions import (
    ClientError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
)
from more_itertools import (
    chunked,
)

from integrates.class_types.types import (
    Item,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.toe_ports.types import (
    ToePortRequest,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.exceptions import (
    UnavailabilityError,
)
from integrates.dynamodb.types import (
    PageInfo,
    PrimaryKey,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


NETWORK_ERRORS = (
    asyncio.TimeoutError,
    ClientConnectorError,
    ClientError,
    ClientOSError,
    ClientPayloadError,
    ConnectionError,
    ConnectionResetError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
    ServerDisconnectedError,
    ServerTimeoutError,
    UnavailabilityError,
)


batch_delete_item = retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=300,
)(operations.batch_delete_item)


@retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=10,
)
async def _get_toe_ports_by_group(
    group_name: str,
    after: str | None,
) -> tuple[tuple[Item, ...], PageInfo]:
    primary_key = keys.build_key(
        facet=TABLE.facets["toe_port_metadata"],
        values={"group_name": group_name},
    )
    key_structure = TABLE.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(
                primary_key.sort_key.replace("#ROOT#ADDRESS#PORT", ""),
            )
        ),
        facets=(TABLE.facets["toe_port_metadata"],),
        index=None,
        table=TABLE,
        after=after,
    )

    return response.items, response.page_info


@retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=10,
)
async def _get_historic_toe_port(
    request: ToePortRequest,
) -> tuple[Item, ...]:
    primary_key = keys.build_key(
        facet=TABLE.facets["toe_port_historic_state"],
        values={
            "address": request.address,
            "port": request.port,
            "group_name": request.group_name,
            "root_id": request.root_id,
        },
    )
    key_structure = TABLE.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(TABLE.facets["toe_port_historic_state"],),
        table=TABLE,
    )
    return response.items


async def process_historic_toe_port(toe_item: Item) -> None:
    historic_toe_port: tuple[Item, ...] = await _get_historic_toe_port(
        ToePortRequest(
            group_name=toe_item["group_name"],
            address=toe_item["address"],
            port=toe_item["port"],
            root_id=toe_item["root_id"],
        ),
    )
    primary_keys = tuple(
        PrimaryKey(partition_key=item["pk"], sort_key=item["sk"]) for item in historic_toe_port
    )
    for chunked_keys in chunked(primary_keys, 25):
        await batch_delete_item(
            keys=tuple(chunked_keys),
            table=TABLE,
        )


async def process_group(group_name: str, count: int) -> None:
    LOGGER_CONSOLE.info(
        "Group",
        extra={
            "extra": {
                "group_name": group_name,
                "count": count,
            },
        },
    )
    after = None
    counter = 0
    while True:
        group_toe_ports, page_info = await _get_toe_ports_by_group(group_name, after)
        await collect(
            tuple(process_historic_toe_port(item) for item in group_toe_ports),
            workers=32,
        )
        LOGGER_CONSOLE.info(
            "Processing",
            extra={
                "extra": {
                    "group_name": group_name,
                    "count": count,
                    "group_toe_port": len(group_toe_ports),
                },
            },
        )
        counter += len(group_toe_ports)
        after = page_info.end_cursor
        if not page_info.has_next_page:
            break

    LOGGER_CONSOLE.info(
        "Group processed",
        extra={
            "extra": {
                "group_name": group_name,
                "count": count,
                "group_toe_ports": counter,
            },
        },
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    all_group_names = sorted(await orgs_domain.get_all_group_names(loaders))
    LOGGER_CONSOLE.info(
        "All group names",
        extra={
            "extra": {
                "total": len(all_group_names),
            },
        },
    )
    for count, group_name in enumerate(all_group_names, start=1):
        await process_group(group_name, count)


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:        %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
