"""
Queue batch action update_vulns_author on selected roots.

Execution Time:    2024-11-20 at 15:24:33
Finalization Time: 2024-11-20 at 15:26:28, extra=None
"""

import csv
import logging
import logging.config
import time
from itertools import (
    groupby,
)
from operator import (
    attrgetter,
)
from typing import (
    NamedTuple,
)

from aioextensions import (
    run,
)
from more_itertools import (
    chunked,
)

from integrates.batch.dal.put import (
    put_action,
)
from integrates.batch.enums import (
    Action,
    IntegratesBatchQueue,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")
EMAIL_MODIFIED_BY = "integrates@fluidattacks.com"
MAX_ROOTS_PER_JOB = 100


class CsvRow(NamedTuple):
    group_name: str
    root_id: str
    commit_author: str


async def main() -> None:
    csv_rows: list[CsvRow] = []
    with open("file_vulns_author_info.csv", encoding="utf-8") as file:
        csv_file = csv.reader(file)
        for line in csv_file:
            csv_rows.append(CsvRow(line[10], line[11], line[7]))

    LOGGER_CONSOLE.info("Rows on csv: %s", str(len(csv_rows)))

    rows_no_author_commit = [row for row in csv_rows if not row.commit_author]
    LOGGER_CONSOLE.info("Rows NO author commit: %s", str(len(rows_no_author_commit)))
    for group_name, group_rows in groupby(
        sorted(rows_no_author_commit, key=attrgetter("group_name")),
        key=attrgetter("group_name"),
    ):
        root_ids_to_process = list({row.root_id for row in group_rows})
        for chunk in chunked(root_ids_to_process, MAX_ROOTS_PER_JOB):
            result = await put_action(
                action=Action.UPDATE_VULNS_AUTHOR,
                additional_info={"root_ids": list(chunk)},
                entity=group_name,
                queue=IntegratesBatchQueue.SMALL,
                subject=EMAIL_MODIFIED_BY,
            )
            LOGGER_CONSOLE.info(
                "QUEUED: %s %s %s",
                group_name,
                str(chunk),
                result.batch_job_id,
            )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
