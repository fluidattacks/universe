# type: ignore
"""
Change location on AWS CSPM reports add regions, modify toe's and hashes

Execution Time:    2024-01-11 at 03:50:08 UTC
Finalization Time: 2024-01-11 at 05:41:00 UTC, extra=None

"""

import asyncio
import hashlib
import logging
import logging.config
import time
from datetime import (
    datetime,
)

from aioextensions import (
    run,
)
from boto3.dynamodb.conditions import (
    Key,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.enums import (
    Source,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateReason,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityMetadataToUpdate,
)
from integrates.db_model.vulnerabilities.update import (
    update_historic_entry,
    update_metadata,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")

CHECKS_NO_REGIONS = {
    "aws.allows_priv_escalation_by_policies_versions",
    "aws.allows_priv_escalation_by_attach_policy",
    "aws.serves_content_over_insecure_protocols",
    "aws.users_with_password_and_access_keys",
    "aws.admin_policy_attached",
    "aws.full_access_to_ssm",
    "aws.negative_statement",
    "aws.has_permissive_role_policies",
    "aws.user_with_inline_policies",
    "aws.open_passrole",
    "aws.group_with_inline_policies",
    "aws.iam_has_mfa_disabled",
    "aws.root_without_mfa",
    "aws.mfa_disabled_for_users_with_console_password",
    "aws.bucket_policy_has_server_side_encryption_disable",
    "aws.users_with_multiple_access_keys",
    "aws.root_has_access_keys",
    "aws.has_root_active_signing_certificates",
    "aws.policies_attached_to_users",
    "aws.acl_public_buckets",
    "aws.s3_buckets_allow_unauthorized_public_access",
    "aws.has_old_ssh_public_keys",
    "aws.have_old_creds_enabled",
    "aws.have_old_access_keys",
    "aws.s3_has_insecure_transport",
    "aws.iam_has_wildcard_resource_on_write_action",
    "aws.iam_is_policy_miss_configured",
    "aws.permissive_policy",
    "aws.private_buckets_not_blocking_public_acls",
    "aws.public_buckets",
    "aws.not_requires_uppercase",
    "aws.not_requires_lowercase",
    "aws.not_requires_symbols",
    "aws.not_requires_numbers",
    "aws.min_password_len_unsafe",
    "aws.password_reuse_unsafe",
    "aws.password_expiration_unsafe",
    "aws.cft_serves_content_over_http",
    "aws.is_trail_bucket_logging_disabled",
    "aws.cloudfront_has_logging_disabled",
    "aws.cloudtrail_trails_not_multiregion",
    "aws.s3_has_server_access_logging_disabled",
    "aws.cf_distribution_has_logging_disabled",
    "aws.s3_bucket_versioning_disabled",
}


async def process_toe(data) -> None:
    primary_key = keys.build_key(
        facet=TABLE.facets["toe_input_metadata"],
        values={
            "component": data["component"],
            "entry_point": data["entry_point"],
            "group_name": data["group_name"],
            "root_id": data["root_id"],
        },
    )
    key_structure = TABLE.primary_key

    condition_expression = Key(key_structure.partition_key).eq(primary_key.partition_key) & Key(
        key_structure.sort_key,
    ).begins_with("INPUTS#")
    item = {
        "component": data["new_component"],
    }
    await operations.update_item(
        condition_expression=condition_expression,
        item=item,
        key=primary_key,
        table=TABLE,
    )


async def process_vulnerability(vulnerability: Vulnerability, new_where: str, data: dict) -> None:
    new_hash = int.from_bytes(
        hashlib.sha256(
            bytes(
                (
                    new_where
                    + vulnerability.state.specific
                    + vulnerability.finding_id
                    + vulnerability.skims_method
                ),
                encoding="utf-8",
            ),
        ).digest()[:8],
        "little",
    )
    original_vuln = vulnerability
    new_state = vulnerability.state._replace(
        where=new_where,
        modified_date=datetime.utcnow(),
        reasons=[VulnerabilityStateReason.CONSISTENCY],
        modified_by="lsaavedra@fluidattacks.com",
    )

    await update_historic_entry(
        current_value=original_vuln,
        finding_id=vulnerability.finding_id,
        entry=new_state,
        vulnerability_id=vulnerability.id,
    )

    await update_metadata(
        finding_id=vulnerability.finding_id,
        metadata=VulnerabilityMetadataToUpdate(hash=new_hash),
        root_id=vulnerability.root_id,
        vulnerability_id=vulnerability.id,
    )

    await process_toe(data)


async def process_group(semaphore: asyncio.Semaphore, loaders: Dataloaders, group: str) -> None:
    async with semaphore:
        findings = await loaders.group_findings.load(group)
        vulns = await loaders.finding_vulnerabilities.load_many_chained(
            [fin.id for fin in findings],
        )
        machine_vulns = [
            vuln for vuln in vulns if (vuln.state.source == Source.MACHINE) or vuln.skims_method
        ]

        for vuln in machine_vulns:
            if vuln.skims_method is not None:
                check_condition = "aws." in vuln.skims_method
                if (
                    check_condition
                    and vuln.skims_method not in CHECKS_NO_REGIONS
                    and vuln.technique == "CSPM"
                    and (vuln.state.where).split(":")[3] != "us-east-1"
                ):
                    if "::" in vuln.state.where:
                        new = (vuln.state.where).replace("::", ":us-east-1:")

                    else:
                        split = (vuln.state.where).split(":")
                        new = str(":".join(split[:3]) + ":us-east-1:" + ":".join(split[3:]))

                    group_name = group
                    new_split = new.split(" ")
                    new_component = new_split[0]
                    original_component = (vuln.state.where).split(" ")[0]
                    data = {
                        "group_name": group_name,
                        "entry_point": "",
                        "component": original_component,
                        "root_id": vuln.root_id,
                        "new_component": new_component,
                    }

                    LOGGER_CONSOLE.info(
                        "Method: %s method, Where: %s, New where: %s",
                        vuln.skims_method,
                        vuln.state.where,
                        new,
                    )
                    await process_vulnerability(vuln, new, data)


async def main() -> None:
    loaders = get_new_context()
    groups = await orgs_domain.get_all_active_group_names(loaders=loaders)
    semaphore = asyncio.Semaphore(5)

    await asyncio.gather(
        *[
            process_group(semaphore, loaders, group)
            for idx, group in enumerate(groups)
            if idx < 200
        ],
    )
    await asyncio.gather(
        *[
            process_group(semaphore, loaders, group)
            for idx, group in enumerate(groups)
            if 200 >= idx < 400
        ],
    )
    await asyncio.gather(
        *[
            process_group(semaphore, loaders, group)
            for idx, group in enumerate(groups)
            if idx >= 400
        ],
    )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")

    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
