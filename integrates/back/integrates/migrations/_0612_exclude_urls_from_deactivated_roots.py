"""
Update include flag to False on environment urls' state in deactivated roots

Execution Time:     2024-10-24 at 17:19:36 UTC
Finalization Time:  2024-10-24 at 17:20:18 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)

from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    roots as roots_model,
)
from integrates.db_model.roots.enums import (
    RootStatus,
)
from integrates.db_model.roots.types import (
    GitRoot,
    RootEnvironmentUrlsRequest,
)
from integrates.organizations.domain import (
    get_all_group_names,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")
EMAIL_INTEGRATES = "integrates@fluidattacks.com"


async def process_inactive_root(root: GitRoot) -> None:
    loaders = get_new_context()
    root_env_urls = await loaders.root_environment_urls.load(
        RootEnvironmentUrlsRequest(root.id, root.group_name),
    )
    urls_to_exclude = [url for url in root_env_urls if url.state.include]
    await collect(
        [
            roots_model.update_root_environment_url_state(
                current_value=url.state,
                root_id=root.id,
                url_id=url.id,
                group_name=root.group_name,
                state=url.state._replace(
                    modified_date=datetime_utils.get_now(),
                    modified_by=EMAIL_INTEGRATES,
                    include=False,
                ),
            )
            for url in urls_to_exclude
        ],
        workers=4,
    )


async def get_inactive_git_roots(loaders: Dataloaders, group_name: str) -> list[GitRoot]:
    return [
        root
        for root in await loaders.group_roots.load(group_name)
        if isinstance(root, GitRoot) and root.state.status == RootStatus.INACTIVE
    ]


async def process_group(group_name: str, progress: float) -> None:
    loaders = get_new_context()
    inactive_roots = await get_inactive_git_roots(loaders, group_name)
    if not inactive_roots:
        return

    await collect(
        [process_inactive_root(root) for root in inactive_roots],
        workers=4,
    )
    LOGGER_CONSOLE.info(
        "Group updated",
        extra={
            "extra": {
                "group_name": group_name,
                "len_inactive_roots": len(inactive_roots),
                "progress": str(round(progress, 2)),
            },
        },
    )


async def main() -> None:
    loaders = get_new_context()
    group_names = sorted(await get_all_group_names(loaders=loaders))

    for count, group in enumerate(group_names):
        await process_group(
            group_name=group,
            progress=count / len(group_names),
        )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
