"""
Move several git roots from group a to group b in the same organization.

Start Time:        2024-02-14 at 01:49:12 UTC
Finalization Time: 2024-02-14 at 01:50:19 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)

from integrates.batch import (
    dal as batch_dal,
)
from integrates.batch.enums import (  # Product, used before the Any update
    Action,
    IntegratesBatchQueue,
)
from integrates.custom_utils import (
    roots as roots_utils,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.group_access.types import (
    GroupAccessMetadataToUpdate,
    GroupStakeholdersAccessRequest,
)
from integrates.db_model.roots.types import (
    GitRoot,
    IPRoot,
    URLRoot,
)
from integrates.group_access.domain import (
    update,
)
from integrates.roots import (
    domain as roots_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger(__name__)


async def process_group_access(
    loaders: Dataloaders,
    group_name: str,
    target_group: str,
) -> None:
    group_stakeholders = await loaders.group_stakeholders_access.load(
        GroupStakeholdersAccessRequest(group_name=group_name),
    )

    LOGGER_CONSOLE.info(
        "Active users",
        extra={"extra": {"users_len": len(group_stakeholders)}},
    )

    await collect(
        tuple(
            update(
                loaders=loaders,
                email=access.email,
                group_name=target_group,
                metadata=GroupAccessMetadataToUpdate(
                    expiration_time=access.expiration_time,
                    state=access.state,
                ),
            )
            for access in group_stakeholders
        ),
    )


async def process_root(
    loaders: Dataloaders,
    group_name: str,
    root_id: str,
    target_group_name: str,
) -> None:
    email = "integrates@fluidattacks.com"
    new_root_id = await roots_domain.move_root(
        loaders=loaders,
        email=email,
        group_name=group_name,
        root_id=root_id,
        target_group_name=target_group_name,
    )
    await batch_dal.put_action(
        action=Action.MOVE_ROOT,
        entity=group_name,
        subject=email,
        additional_info={
            "target_group_name": target_group_name,
            "target_root_id": new_root_id,
            "source_group_name": group_name,
            "source_root_id": root_id,
        },
        queue=IntegratesBatchQueue.SMALL,
        # product=Product.INTEGRATES, used before the Any update
    )
    root = await roots_utils.get_root(loaders, root_id, group_name)
    if isinstance(root, GitRoot):
        await batch_dal.put_action(
            action=Action.REFRESH_TOE_LINES,
            attempt_duration_seconds=7200,
            entity=group_name,
            subject=email,
            additional_info={"root_ids": [root.id]},
            # product=Product.INTEGRATES, used before the Any update
            queue=IntegratesBatchQueue.SMALL,
        )
    if isinstance(root, (GitRoot, URLRoot)):
        await batch_dal.put_action(
            action=Action.REFRESH_TOE_INPUTS,
            entity=group_name,
            subject=email,
            additional_info={"root_ids": [root.id]},
            # product=Product.INTEGRATES, used before the Any update
            queue=IntegratesBatchQueue.SMALL,
        )
    if isinstance(root, IPRoot):
        await batch_dal.put_action(
            action=Action.REFRESH_TOE_PORTS,
            entity=group_name,
            subject=email,
            additional_info={"root_ids": [root.id]},
            # product=Product.INTEGRATES, used before the Any update
            queue=IntegratesBatchQueue.SMALL,
        )

    LOGGER_CONSOLE.info(
        "Moved root to another group",
        extra={
            "extra": {
                "source_group_name": group_name,
                "old_root_id": root_id,
                "target_group_name": target_group_name,
                "new_root_id": new_root_id,
            },
        },
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    group_name = "group_a"
    target_group_name = "group_b"
    group_roots = tuple(root.id for root in sorted(await loaders.group_roots.load(group_name)))
    LOGGER_CONSOLE.info(
        "Roots to process",
        extra={"extra": {"roots": len(group_roots)}},
    )
    await process_group_access(loaders, group_name, target_group_name)
    await collect(
        tuple(
            process_root(
                loaders=loaders,
                group_name=group_name,
                root_id=root_id,
                target_group_name=target_group_name,
            )
            for root_id in group_roots
        ),
        workers=10,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:    %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("%s", execution_time)
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
