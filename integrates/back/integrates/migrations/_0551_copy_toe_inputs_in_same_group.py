"""
Migration used to copy ToE inputs between roots
of the same group

Execution Time:    2024-06-06 at 12:20:02 UTC
Finalization Time: 2024-06-06 at 12:21:34 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)

from integrates.batch import (
    dal as batch_dal,
)
from integrates.batch.enums import (
    Action,
    IntegratesBatchQueue,
)
from integrates.custom_exceptions import (
    RepeatedToeInput,
)
from integrates.custom_utils import (
    roots as root_utils,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.roots.types import (
    GitRoot,
    Root,
    URLRoot,
)
from integrates.db_model.toe_inputs.types import (
    GroupToeInputsRequest,
    ToeInput,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.dynamodb.exceptions import (
    UnavailabilityError,
)
from integrates.settings import (
    LOGGING,
)
from integrates.toe.inputs import (
    domain as toe_inputs_domain,
)
from integrates.toe.inputs.types import (
    ToeInputAttributesToAdd,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")

toe_inputs_add = retry_on_exceptions(exceptions=(UnavailabilityError,), sleep_seconds=5)(
    toe_inputs_domain.add,
)


async def process_toe_input(
    loaders: Dataloaders,
    source_toe_input: ToeInput,
    target_root: Root,
) -> None:
    attributes_to_add = ToeInputAttributesToAdd(
        attacked_at=source_toe_input.state.attacked_at,
        attacked_by=source_toe_input.state.attacked_by,
        be_present=source_toe_input.state.be_present,
        first_attack_at=source_toe_input.state.first_attack_at,
        has_vulnerabilities=source_toe_input.state.has_vulnerabilities,
        seen_first_time_by=source_toe_input.state.seen_first_time_by,
        seen_at=source_toe_input.state.seen_at,
    )
    try:
        await toe_inputs_add(
            loaders=loaders,
            entry_point=source_toe_input.entry_point,
            environment_id=source_toe_input.environment_id,
            component=source_toe_input.component,
            group_name=target_root.group_name,
            root_id=target_root.id,
            attributes=attributes_to_add,
            is_moving_toe_input=True,
        )
    except RepeatedToeInput as exc:
        LOGGER_CONSOLE.info("Error in adding ToeInput: %s", exc)


async def copy_toe_inputs(
    loaders: Dataloaders,
    source_root: Root,
    target_root: Root,
) -> None:
    if isinstance(source_root, (GitRoot, URLRoot)):
        group_toe_inputs = await loaders.group_toe_inputs.load_nodes(
            GroupToeInputsRequest(group_name=source_root.group_name),
        )
        root_toe_inputs = tuple(
            toe_input for toe_input in group_toe_inputs if toe_input.root_id == source_root.id
        )
        await collect(
            tuple(
                process_toe_input(loaders, toe_input, target_root) for toe_input in root_toe_inputs
            ),
        )
        await batch_dal.put_action(
            action=Action.REFRESH_TOE_INPUTS,
            entity=target_root.group_name,
            subject=root_utils.EMAIL_INTEGRATES,
            additional_info={"root_ids": [target_root.id]},
            queue=IntegratesBatchQueue.SMALL,
        )
        LOGGER_CONSOLE.info("ToE inputs processed")


async def copy_root_data(
    loaders: Dataloaders,
    group_name: str,
    source_root_nickname: str,
    target_root_nickname: str,
) -> None:
    group_roots = {root.state.nickname: root for root in await loaders.group_roots.load(group_name)}
    source_root = group_roots[source_root_nickname]
    target_root = group_roots[target_root_nickname]
    await copy_toe_inputs(loaders, source_root, target_root)
    LOGGER_CONSOLE.info("Root processing completed")


async def main() -> None:
    loaders: Dataloaders = get_new_context()

    await copy_root_data(
        loaders,
        "group_name",
        "source_root_nickname",
        "target_root_nickname",
    )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
