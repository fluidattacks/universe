"""
Populate toe port historic state in integrates_vms_historic table
for toe port with no historic.

Start Time: 2024-03-07 at 18:43:20 UTC
Finalization Time: 2024-03-07 at 18:43:29 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)
from boto3.dynamodb.conditions import (
    Key,
)
from botocore.exceptions import (
    ConnectTimeoutError,
    ReadTimeoutError,
)

from integrates.class_types.types import (
    Item,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    HISTORIC_TABLE,
    TABLE,
)
from integrates.db_model.toe_ports.types import (
    GroupToePortStatesRequest,
    ToePortRequest,
)
from integrates.db_model.utils import (
    get_historic_gsi_2_key,
    get_historic_gsi_3_key,
    get_historic_gsi_sk,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


async def get_toe_ports_by_group(
    group_name: str,
) -> tuple[Item, ...]:
    primary_key = keys.build_key(
        facet=TABLE.facets["toe_port_metadata"],
        values={"group_name": group_name},
    )
    key_structure = TABLE.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(
                primary_key.sort_key.replace("#ROOT#ADDRESS#PORT", ""),
            )
        ),
        facets=(TABLE.facets["toe_port_metadata"],),
        index=None,
        table=TABLE,
    )

    return response.items


async def get_historic_toe_port(
    request: ToePortRequest,
) -> tuple[Item, ...]:
    primary_key = keys.build_key(
        facet=TABLE.facets["toe_port_historic_state"],
        values={
            "address": request.address,
            "port": request.port,
            "group_name": request.group_name,
            "root_id": request.root_id,
        },
    )
    key_structure = TABLE.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(TABLE.facets["toe_port_historic_state"],),
        table=TABLE,
    )

    return response.items


@retry_on_exceptions(
    exceptions=(ReadTimeoutError, ConnectTimeoutError),
    sleep_seconds=3,
)
async def process_historic_toe_port(item: Item, historic_item: Item) -> None:
    item_pk = f"{item['pk']}#{item['sk']}"
    item_sk = get_historic_gsi_sk("state", historic_item["modified_date"])
    group_name = item["group_name"]
    gsi_2_key = get_historic_gsi_2_key(
        HISTORIC_TABLE.facets["toe_port_state"],
        group_name,
        "state",
        historic_item["modified_date"],
    )
    gsi_3_key = get_historic_gsi_3_key(group_name, "state", historic_item["modified_date"])
    gsi_keys = {
        "pk_2": gsi_2_key.partition_key,
        "sk_2": gsi_2_key.sort_key,
        "pk_3": gsi_3_key.partition_key,
        "sk_3": gsi_3_key.sort_key,
    }
    seen_at = historic_item.get("seen_at") or item.get("seen_at")
    seen_first_time_by = historic_item.get("seen_first_time_by") or item.get("seen_first_time_by")
    await operations.put_item(
        facet=HISTORIC_TABLE.facets["toe_port_state"],
        item={
            **(
                item["state"]
                if historic_item["modified_date"] == item["state"]["modified_date"]
                else historic_item
            ),
            "seen_at": seen_at,
            "seen_first_time_by": seen_first_time_by,
            "pk": item_pk,
            "sk": item_sk,
            **gsi_keys,
        },
        table=HISTORIC_TABLE,
    )


async def process_historic_toe_ports(group_name: str, toe_item: Item) -> None:
    historic_toe_ports: tuple[Item, ...] = await get_historic_toe_port(
        ToePortRequest(
            address=toe_item["address"],
            port=toe_item["port"],
            group_name=group_name,
            root_id=toe_item["root_id"],
        ),
    )
    await collect(
        [
            process_historic_toe_port(toe_item, historic_toe_port)
            for historic_toe_port in historic_toe_ports
        ],
    )


async def process_group(group_name: str) -> None:
    group_toe_ports = await get_toe_ports_by_group(group_name)
    await collect(
        tuple(process_historic_toe_ports(group_name, item) for item in group_toe_ports),
        workers=64,
    )
    LOGGER_CONSOLE.info(
        "Group processed",
        extra={
            "extra": {
                "group_name": group_name,
                "group_toe_ports": len(group_toe_ports),
            },
        },
    )

    group_toe_port_states = await get_new_context().group_toe_port_states.load(
        GroupToePortStatesRequest(group_name=group_name),
    )
    print(group_toe_port_states)


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    all_group_names = sorted(await orgs_domain.get_all_group_names(loaders))
    LOGGER_CONSOLE.info(
        "All group names",
        extra={
            "extra": {
                "total": len(all_group_names),
            },
        },
    )
    for count, group_name in enumerate(all_group_names, start=1):
        LOGGER_CONSOLE.info(
            "Group",
            extra={
                "extra": {
                    "group_name": group_name,
                    "count": count,
                },
            },
        )
        await process_group(group_name)


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:        %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
