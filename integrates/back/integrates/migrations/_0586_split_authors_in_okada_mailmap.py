"""
Split okada mailmap authors into multiple authors by using the
`convert_mailmap_subentries_to_new_entry` function

This migration script identifies authors with aliases belonging to multiple
organizations and generates a new author for each group of aliases associated
with a different organization.

The migration will create new authors that will need to be distributed to their
respective organizations in a future migration. It may also generate authors
that should be removed if they belong to organizations that no longer exist.

This migration uses a CSV file that maps email domains to organization names.
The file should be located in the corresponding S3 bucket with the
correct name.

Start Time:        2024-08-29 at 18:03:56 UTC
Finalization Time: 2024-08-29 at 18:03:59 UTC

Start Time:        2024-08-30 at 15:33:08 UTC
Finalization Time: 2024-08-30 at 15:33:13 UTC

"""

import csv
import logging
import logging.config
import os
import time

from aioextensions import (
    run,
)

from integrates.custom_exceptions import (
    ErrorDownloadingFile,
    MailmapEntryAlreadyExists,
    MailmapEntryNotFound,
    MailmapOrganizationNotFound,
    MailmapSubentryAlreadyExists,
    MailmapSubentryNotFound,
)
from integrates.mailmap.get import (
    get_mailmap_entries_with_subentries,
)
from integrates.mailmap.update import (
    convert_mailmap_subentries_to_new_entry,
)
from integrates.s3.operations import (
    download_file,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


ORGANIZATION_ID = "okada"
ORGANIZATIONS_FILE_PATH = "orgs-names-domains-mapping.csv"


async def download_organizations_file(file_path: str) -> None:
    try:
        await download_file(
            file_name="mailmap-resources/orgs-names-domains-mapping.csv",
            file_path=file_path,
            bucket="integrates.dev",
        )
    except ErrorDownloadingFile as ex:
        LOGGER_CONSOLE.error("%s\n\n", ex)


def remove_organizations_file(file_path: str) -> None:
    if os.path.exists(file_path):
        os.remove(file_path)
        LOGGER_CONSOLE.info("Organizations file removed successfully.\n\n")
    else:
        LOGGER_CONSOLE.error("Organizations file doesn't exist.\n\n")


def build_orgs_domain_name_map(file_path: str) -> dict[str, str]:
    orgs_domain_name_map: dict[str, str] = {}

    with open(file_path, newline="", encoding="utf-8") as csv_file:
        reader = csv.DictReader(csv_file)
        for row in reader:
            domain: str = row["domain"].strip().lower()
            name: str = row["name"]
            if domain:
                orgs_domain_name_map[domain] = name

    return orgs_domain_name_map


async def main() -> None:
    await download_organizations_file(ORGANIZATIONS_FILE_PATH)
    orgs_domain_name_map = build_orgs_domain_name_map(ORGANIZATIONS_FILE_PATH)
    remove_organizations_file(ORGANIZATIONS_FILE_PATH)

    entries_with_subentries = await get_mailmap_entries_with_subentries(
        organization_id=ORGANIZATION_ID,
    )

    new_authors = 0
    for entry_with_subentries in entries_with_subentries:
        entry = entry_with_subentries["entry"]
        subentries = entry_with_subentries["subentries"]

        entry_email = entry["mailmap_entry_email"]

        entry_org_name = next(
            (
                org_name
                for org_domain, org_name in orgs_domain_name_map.items()
                if entry_email.lower().endswith(org_domain)
            ),
            "",
        )

        subentries_orgs_names_mapping = {
            org_name: [
                subentry
                for subentry in subentries
                if subentry["mailmap_subentry_email"].lower().endswith(org_domain)
            ]
            for org_domain, org_name in orgs_domain_name_map.items()
            if any(
                subentry["mailmap_subentry_email"].lower().endswith(org_domain)
                for subentry in subentries
            )
        }

        conversion_candidates_orgs_names = list(
            set(subentries_orgs_names_mapping.keys()) - set([entry_org_name]),
        )

        if len(conversion_candidates_orgs_names) > 0:
            for org_name in conversion_candidates_orgs_names:
                org_name_subentries = subentries_orgs_names_mapping[org_name]
                # it's guaranteed to have at least 1 subentry, so [0] is okay
                main_subentry = org_name_subentries[0]
                other_subentries = org_name_subentries[1:]

                LOGGER_CONSOLE.info(
                    "Aliases %s: %s - %s can be converted to a new author\n\n",
                    main_subentry,
                    other_subentries,
                    org_name,
                )

                try:
                    await convert_mailmap_subentries_to_new_entry(
                        entry_email=entry_email,
                        main_subentry=main_subentry,
                        other_subentries=other_subentries,
                        organization_id=ORGANIZATION_ID,
                    )

                    LOGGER_CONSOLE.info(
                        "Aliases %s: %s - %s converted successfully\n\n",
                        main_subentry,
                        other_subentries,
                        org_name,
                    )

                    new_authors += 1

                except (
                    MailmapOrganizationNotFound,
                    MailmapEntryAlreadyExists,
                    MailmapEntryNotFound,
                    MailmapSubentryAlreadyExists,
                    MailmapSubentryNotFound,
                ) as ex:
                    LOGGER_CONSOLE.error("%s\n\n", ex)

    LOGGER_CONSOLE.info(
        "New authors: %s\n\n",
        new_authors,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:        %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
