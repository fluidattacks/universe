"""
Execution Time: 2025-02-04 at 19:07:33 UTC
Finalization Time: 2025-02-04 at 19:07:40 UTC

Deactivate docker images and their packages
for inactive roots.
"""

import logging

from aioextensions import collect

from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.roots.enums import RootStatus
from integrates.db_model.roots.types import GroupDockerImagesRequest, RootDockerImage, RootRequest
from integrates.migrations.utils import log_time
from integrates.organizations import domain as orgs_domain
from integrates.roots import domain as roots_domain

LOGGER = logging.getLogger("migrations")
EMAIL = "integrates@fluidattacks.com"


async def process_docker_image(
    docker_image: RootDockerImage, group_name: str, loaders: Dataloaders
) -> None:
    image_root = await loaders.root.load(RootRequest(group_name, docker_image.root_id))

    if not image_root:
        LOGGER.error(
            "Root not found for Docker image %s in group %s (root_id=%s)",
            docker_image.uri,
            group_name,
            docker_image.root_id,
        )
        return

    if image_root.state.status == RootStatus.INACTIVE:
        await roots_domain.deactivate_root_docker_image(
            loaders=loaders,
            docker_image=docker_image,
            user_email=EMAIL,
        )
        LOGGER.info(
            "Updated Docker image %s state in group %s (root_id=%s)",
            docker_image.uri,
            group_name,
            docker_image.root_id,
        )


async def process_group(group_name: str) -> None:
    loaders = get_new_context()
    images = await loaders.group_docker_images.load(
        GroupDockerImagesRequest(group_name=group_name),
    )

    if not images:
        LOGGER.warning("No Docker images found for group %s", group_name)
        return

    LOGGER.info("Processing group %s with %d images", group_name, len(images))

    await collect([process_docker_image(image, group_name, loaders) for image in images], workers=4)


@log_time(LOGGER)
async def main() -> None:
    LOGGER.info("Starting Docker image migration process")

    loaders = get_new_context()

    group_names = sorted(await orgs_domain.get_all_group_names(loaders))

    await collect(
        [process_group(group_name) for group_name in group_names],
        workers=4,
    )

    LOGGER.info("Docker image migration process completed")


if __name__ == "__main__":
    main()  # type: ignore[unused-coroutine]
