"""
Execution Time:    2024-10-28 at 16:47:09 UTC
Finalization Time: 2024-10-29 at 03:25:57 UTC
Execution Time:    2024-10-30 at 03:35:27 UTC
Finalization Time: 2024-10-30 at 14:10:03 UTC
Execution Time:    2024-10-30 at 14:27:51 UTC
Finalization Time: 2024-10-30 at 19:37:47 UTC
Execution Time:    2024-10-31 at 04:01:58 UTC
Finalization Time: 2024-10-31 at 13:04:29 UTC
Execution Time:    2024-11-01 at 01:46:30 UTC
Finalization Time: 2024-11-01 at 06:17:34 UTC
"""

import sys
import time

from aioextensions import (
    collect,
    in_thread,
    run,
)
from boto3.dynamodb.conditions import (
    Key,
)
from psycopg2 import (  # type: ignore[import-untyped]
    sql,
)
from psycopg2.extensions import (  # type: ignore[import-untyped]
    cursor as cursor_cls,
)

from integrates.class_types.types import (
    Item,
)
from integrates.db_model import (
    TABLE as INTEGRATES_VMS_TABLE,
)
from integrates.db_model.last_sync import (
    get_db_cursor,
)
from integrates.dynamodb import (
    keys,
    operations,
)

BACKUP_TABLE_NAME = ""
BACKUP_TABLE = INTEGRATES_VMS_TABLE._replace(name=BACKUP_TABLE_NAME)


async def get_vulnerability(
    *,
    vulnerability_id: str,
    cursor: cursor_cls,
) -> None:
    primary_key = keys.build_key(
        facet=BACKUP_TABLE.facets["vulnerability_metadata"],
        values={"id": vulnerability_id},
    )

    key_structure = BACKUP_TABLE.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(BACKUP_TABLE.facets["vulnerability_metadata"],),
        limit=1,
        table=BACKUP_TABLE,
    )

    if not response.items:
        return None

    return await update_vulns_metadata(
        cursor=cursor,
        vuln_id=vulnerability_id,
        vuln_dynamodb=response.items[0],
    )


def _update_vulns_metadata(
    *,
    cursor: cursor_cls,
    _id: str,
    where_str: str | None,
    root_id: str | None,
    technique: str | None,
) -> None:
    cursor.execute(
        sql.SQL(
            """
                UPDATE
                    integrates.vulnerabilities_metadata
                SET
                    where_str = %(where_str)s,
                    technique = %(technique)s,
                    root_id = %(root_id)s
                WHERE
                    id = %(id)s;
            """,
        ),
        {
            "id": _id,
            "where_str": where_str,
            "root_id": root_id,
            "technique": technique,
        },
    )


async def update_vulns_metadata(
    *,
    cursor: cursor_cls,
    vuln_id: str,
    vuln_dynamodb: Item,
) -> None:
    root_id = vuln_dynamodb.get("root_id") or ""
    pk_2 = vuln_dynamodb.get("pk_2") or ""
    if not root_id:
        root_id = pk_2.split("#")[1] if pk_2.startswith("ROOT#") else ""
    if not vuln_dynamodb.get("state") or not vuln_dynamodb.get("state", {}).get("where"):
        return
    where_str = (
        vuln_dynamodb["state"]["where"].split(".")[-1].split(" ")[0]
        if vuln_dynamodb["type"] == "LINES" and len(vuln_dynamodb["state"]["where"].split(".")) > 1
        else None
    )
    technique = vuln_dynamodb.get("technique")
    await in_thread(
        _update_vulns_metadata,
        cursor=cursor,
        _id=vuln_id,
        where_str=where_str,
        root_id=root_id,
        technique=technique,
    )


def get_vulns_missing_where_str(cursor: cursor_cls, masked_groups: list[str]) -> list[tuple[str]]:
    cursor.execute(
        sql.SQL(
            """
            SELECT
                v.id
            FROM
                integrates.vulnerabilities_metadata v
                LEFT JOIN integrates.findings_metadata f ON f.id = v.finding_id
                LEFT JOIN integrates.groups_metadata g ON g.id = f.group_name
            WHERE
                v.root_id is NULL
                AND v.technique is NULL
                AND v.where_str is NULL
                AND g.name in ({groups});
            """,
        ).format(
            groups=sql.SQL(", ").join([sql.Literal(group) for group in masked_groups]),
        ),
    )

    return list(cursor.fetchall())


async def main(*args: str) -> None:
    groups_to_process: list[str] = []
    raw_groups: list[str] = args[0].split(",")
    groups_to_process.extend([group.strip() for group in raw_groups if len(group.strip())])
    global BACKUP_TABLE_NAME, BACKUP_TABLE
    BACKUP_TABLE_NAME = args[1].strip() or BACKUP_TABLE_NAME
    BACKUP_TABLE = INTEGRATES_VMS_TABLE._replace(name=BACKUP_TABLE_NAME)
    with get_db_cursor() as cursor:
        vulns_redshift = get_vulns_missing_where_str(cursor, groups_to_process)

        await collect(
            (get_vulnerability(vulnerability_id=vuln[0], cursor=cursor) for vuln in vulns_redshift),
            workers=64,
        )


if __name__ == "__main__":
    migration_args = sys.argv[1:]
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main(*migration_args))
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    print("\n%s\n%s", execution_time, finalization_time)
