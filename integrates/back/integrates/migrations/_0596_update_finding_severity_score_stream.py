"""
Populate the cvss_v4 field in findings.
Execution Time:    2024-09-12 at 21:14:49 UTC
Finalization Time: 2024-09-13 at 11:18:31 UTC
"""

import logging
import logging.config
import time
from decimal import (
    Decimal,
)

from aioextensions import (
    run,
)
from psycopg2 import (  # type: ignore[import-untyped]
    sql,
)
from psycopg2.extensions import (  # type: ignore[import-untyped]
    cursor as cursor_cls,
)

from integrates.db_model.types import (
    SeverityScore,
)
from integrates.migrations._0591_update_severity_score_streams import (
    REDSHIFT_USER,
    db_cursor,
    get_severity_score,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")
EXPLOITABILITY = {
    Decimal("0.91"): "U",
    Decimal("0.94"): "P",
    Decimal("0.97"): "F",
    Decimal("1.0"): "H",
    Decimal("1.0"): "X",
}
INTEGRITY_IMPACT = {
    Decimal("0.00"): "N",
    Decimal("0.22"): "L",
    Decimal("0.275"): "L",  # 2.0
    Decimal("0.56"): "H",
    Decimal("0.66"): "H",  # 2.0
}
CONFIDENTIALITY_REQUIREMENT = {
    Decimal("0.50"): "L",
    Decimal("1.00"): "M",
    Decimal("1.50"): "H",
}
REMEDIATION_LEVEL = {
    Decimal("0.95"): "O",
    Decimal("0.96"): "T",
    Decimal("0.97"): "W",
    Decimal("1.00"): "U",
    Decimal("1.00"): "X",
}
REPORT_CONFIDENCE = {
    Decimal("0.92"): "U",
    Decimal("0.96"): "R",
    Decimal("1.00"): "C",
    Decimal("1.00"): "X",
}
SEVERITY_SCOPE = {
    Decimal("0.0"): "U",
    Decimal("1.0"): "C",
}
ATTACK_VECTOR = {
    Decimal("0.0"): "P",  # 2.0
    Decimal("0.20"): "P",
    Decimal("0.55"): "L",
    Decimal("0.62"): "A",
    Decimal("0.85"): "N",
}
ATTACK_COMPLEXITY = {
    Decimal("0.77"): "L",
    Decimal("0.44"): "H",
    Decimal("0.0"): "H",  # 2.0
}
PRIVILEGES_REQUIRED = {
    Decimal("0.85"): "N",
    Decimal("0.62"): "L",
    Decimal("0.68"): "L",
    Decimal("0.27"): "H",
    Decimal("0.50"): "H",
    Decimal("0.0"): "H",  # 2.0
}
USER_INTERACTION = {
    Decimal("0.0"): "R",  # 2.0
    Decimal("0.62"): "R",
    Decimal("0.85"): "N",
}


def get_findings_missing_score(
    cursor: cursor_cls,
) -> list[tuple[str, str | None]]:
    cursor.execute(
        sql.SQL(
            """
            SELECT
                id, title
            FROM
                integrates.findings_metadata
            WHERE
                severity_temporal_score is NULL
                AND severity_threat_score is NULL;
            """,
        ),
    )

    return list(cursor.fetchall())


def get_findings_table(cursor: cursor_cls, table: str) -> list[tuple[Decimal, ...]]:
    cursor.execute(
        sql.SQL(
            """
            SELECT
                id, attack_vector, attack_complexity, privileges_required,
                user_interaction, severity_scope, confidentiality_impact,
                integrity_impact, availability_impact, exploitability,
                remediation_level, report_confidence,
                confidentiality_requirement, integrity_requirement,
                availability_requirement, modified_attack_vector,
                modified_attack_complexity, modified_privileges_required,
                modified_user_interaction, modified_severity_scope,
                modified_confidentiality_impact, modified_integrity_impact,
                modified_availability_impact
            FROM
                {table};
            """,
        ).format(
            table=sql.Identifier(REDSHIFT_USER, table),
        ),
    )

    return list(cursor.fetchall())


def _get_field(value: Decimal, mapped_values: dict[Decimal, str]) -> str:
    return mapped_values.get(value, "X")


def get_vector_from_metrics(metrics: tuple[Decimal, ...]) -> str:
    vector = (
        f"CVSS:3.1/AV:{_get_field(metrics[0], ATTACK_VECTOR)}"
        f"/AC:{_get_field(metrics[1], ATTACK_COMPLEXITY)}"
        f"/PR:{_get_field(metrics[2], PRIVILEGES_REQUIRED)}"
        f"/UI:{_get_field(metrics[3], USER_INTERACTION)}"
        f"/S:{_get_field(metrics[4], SEVERITY_SCOPE)}"
        f"/C:{_get_field(metrics[5], INTEGRITY_IMPACT)}"
        f"/I:{_get_field(metrics[6], INTEGRITY_IMPACT)}"
        f"/A:{_get_field(metrics[7], INTEGRITY_IMPACT)}"
        f"/E:{_get_field(metrics[8], EXPLOITABILITY)}"
        f"/RL:{_get_field(metrics[9], REMEDIATION_LEVEL)}"
        f"/RC:{_get_field(metrics[10], REPORT_CONFIDENCE)}"
        f"/CR:{_get_field(metrics[11], CONFIDENTIALITY_REQUIREMENT)}"
        f"/IR:{_get_field(metrics[12], CONFIDENTIALITY_REQUIREMENT)}"
        f"/AR:{_get_field(metrics[13], CONFIDENTIALITY_REQUIREMENT)}"
        f"/MAV:{_get_field(metrics[14], ATTACK_VECTOR)}"
        f"/MAC:{_get_field(metrics[15], ATTACK_COMPLEXITY)}"
        f"/MPR:{_get_field(metrics[16], PRIVILEGES_REQUIRED)}"
        f"/MUI:{_get_field(metrics[17], USER_INTERACTION)}"
        f"/MS:{_get_field(metrics[18], SEVERITY_SCOPE)}"
        f"/MC:{_get_field(metrics[19], INTEGRITY_IMPACT)}"
        f"/MI:{_get_field(metrics[20], INTEGRITY_IMPACT)}"
        f"/MA:{_get_field(metrics[21], INTEGRITY_IMPACT)}"
    )
    if metrics[2] == Decimal("0.68") or metrics[2] == Decimal("0.50"):
        vector = vector.replace("MS:U", "MS:C")

    if all(x in vector for x in ["/MC:N", "/MI:N", "/MA:N"]):
        return vector.split("/MC:N")[0]

    return vector


def update_finding(
    cursor: cursor_cls,
    finding: tuple[str, str | None],
    findings_metrics: dict[str, tuple[Decimal, ...]],
) -> None:
    if finding[1] is None:
        return
    if finding[0] not in findings_metrics:
        return
    vector_v3 = get_vector_from_metrics(findings_metrics[finding[0]])
    severity_score = get_severity_score(finding[1], vector_v3)
    if severity_score is None:
        return
    update_table(cursor, finding[0], severity_score, "findings_metadata")


def update_table(
    cursor: cursor_cls,
    _id: str,
    severity_score: SeverityScore,
    table: str,
) -> None:
    cursor.execute(
        sql.SQL(
            """
                UPDATE
                    {table}
                SET
                    severity_cvss_v3 = %(cvss_v3)s
                    severity_cvss_v4 = %(cvss_v4)s
                    severity_temporal_score = %(temporal_score)s
                    severity_threat_score = %(threat_score)s
                WHERE
                    id = %(id)s;
            """,
        ).format(
            table=sql.Identifier(REDSHIFT_USER, table),
        ),
        {
            "id": _id,
            "threat_score": severity_score.threat_score,
            "temporal_score": severity_score.temporal_score,
            "cvss_v3": severity_score.cvss_v3,
            "cvss_v4": severity_score.cvss_v4,
        },
    )


async def main() -> None:
    findings_metrics: dict[str, tuple[Decimal, ...]] = {}
    with db_cursor() as cursor:
        findings = get_findings_table(cursor, "")
        for finding in findings:
            findings_metrics[str(finding[0])] = finding[1:]

        removed_findings = get_findings_missing_score(cursor)
        for _finding in removed_findings:
            update_finding(cursor, _finding, findings_metrics)


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
