"""
Remove resources of groups with deleted state status.

Start Time: 2024-03-08 at 04:34:17 UTC
Finalization Time: 2024-03-08 at 05:21:00 UTC
"""

import asyncio
import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)
from boto3.dynamodb.conditions import (
    Key,
)

from integrates.class_types.types import (
    Item,
)
from integrates.context import (
    FI_AWS_S3_CONTINUOUS_REPOSITORIES,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.groups.enums import (
    GroupStateStatus,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.db_model.organizations.constants import (
    ALL_ORGANIZATIONS_INDEX_METADATA,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.groups import (
    domain as groups_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


async def _get_all_organizations() -> tuple[Item, ...]:
    primary_key = keys.build_key(
        facet=ALL_ORGANIZATIONS_INDEX_METADATA,
        values={"all": "all"},
    )
    index = TABLE.indexes["gsi_2"]
    key_structure = index.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(ALL_ORGANIZATIONS_INDEX_METADATA,),
        table=TABLE,
        index=index,
    )
    return response.items


async def _remove_group_resources(
    *,
    group: Group,
    loaders: Dataloaders,
) -> None:
    email = "integrates@fluidattacks.com"
    group_name = group.name

    await groups_domain.remove_resources(
        loaders=loaders,
        email=email,
        group_name=group_name,
        validate_pending_actions=True,
        subscription=group.state.type,
        has_advanced=False,
        should_notify=False,
    )

    # Delete related cloned repos
    group_roots = await loaders.group_roots.load(group_name)

    if group_roots:
        LOGGER_CONSOLE.info(
            "Roots to be processed",
            extra={
                "extra": {
                    "group_name": group.name,
                    "roots": [(root.id, root.state.nickname) for root in group_roots],
                },
            },
        )

        bucket_path: str = FI_AWS_S3_CONTINUOUS_REPOSITORIES

        await collect(
            [
                asyncio.create_subprocess_exec(
                    "aws",
                    "s3",
                    "rm",
                    f"s3://{bucket_path}/{group_name}/{root.state.nickname}*",
                )
                for root in group_roots
            ],
            workers=8,
        )

    LOGGER_CONSOLE.info(
        "Remove group resources and repos completed",
        extra={
            "extra": {
                "group_name": group.name,
            },
        },
    )


def _find_groups_whit_deleted_state(group: Group) -> bool:
    return group.state.status.value == GroupStateStatus.DELETED.value


async def _verify_groups(*, organization_id: str) -> None:
    loaders = get_new_context()
    groups = await loaders.organization_groups.load(organization_id)
    groups_with_deleted_state = list(filter(_find_groups_whit_deleted_state, groups))

    if groups_with_deleted_state:
        await collect(
            [
                _remove_group_resources(group=group, loaders=loaders)
                for group in groups_with_deleted_state
            ],
            workers=10,
        )


async def main() -> None:
    organizations = await _get_all_organizations()
    organizations_ids = [organization["id"] for organization in organizations]
    await collect(
        [_verify_groups(organization_id=id) for id in organizations_ids],
        workers=10,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:    %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("%s", execution_time)
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
