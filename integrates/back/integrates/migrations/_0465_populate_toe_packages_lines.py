# type: ignore
"""
Start Time:    2023-12-12 at 22:58:43 UTC
Finalization Time: 2023-12-12 at 23:41:00 UTC, extra=None
"""

import logging
import logging.config
import tempfile
import time
from datetime import (
    datetime,
)

import yaml
from aioextensions import (
    collect,
    run,
)

from integrates.class_types.types import (
    Item,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.groups.enums import (
    GroupManaged,
)
from integrates.db_model.roots.enums import (
    RootStatus,
)
from integrates.db_model.roots.types import (
    GitRoot,
)
from integrates.db_model.toe_packages import (
    update_package,
)
from integrates.db_model.toe_packages.types import (
    RootToePackagesRequest,
    ToePackageMetadataToUpdate,
    ToePackagesConnection,
)
from integrates.organizations.domain import (
    get_all_group_names,
)
from integrates.s3.resource import (
    get_client,
)
from integrates.server_async.report_sbom.process import (
    format_sbom_package,
)
from integrates.server_async.utils import (
    get_results_log,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


async def process_json_sbom(json_results: Item, git_root: GitRoot, group_name: str) -> None:
    updates = []
    loaders = get_new_context()
    execution_packages = [
        format_sbom_package(content=package, group_name=group_name, root_id=git_root.id)
        for package in json_results["packages"]
    ]
    root_packages_present: ToePackagesConnection = await loaders.root_toe_packages.load(
        RootToePackagesRequest(
            group_name=group_name,
            root_id=git_root.id,
            be_present=True,
        ),
    )
    execution_dict = {
        (
            pkg.group_name,
            pkg.root_id,
            pkg.id,
        ): pkg
        for pkg in execution_packages
    }
    db_dict = {
        (
            pkg.node.group_name,
            pkg.node.root_id,
            pkg.node.id,
        ): pkg.node
        for pkg in root_packages_present.edges
    }
    for key, execution_pkg in execution_dict.items():
        if (db_package := db_dict.get(key)) and execution_pkg.lines != db_package.lines:
            updates.append(db_dict[key])
            await update_package(
                current_value=db_package,
                package=ToePackageMetadataToUpdate(lines=execution_pkg.lines),
            )
    LOGGER_CONSOLE.info(
        "Packages updated",
        extra={
            "extra": {
                "group": group_name,
                "nickname": git_root.state.nickname,
                "updates": len(updates),
            },
        },
    )


async def process_group(group_name: str) -> None:
    loaders = get_new_context()
    LOGGER_CONSOLE.info("Processing group %s", group_name)
    group = await loaders.group.load(group_name)
    if group and (
        group.state.has_essential is False or group.state.managed == GroupManaged.UNDER_REVIEW
    ):
        LOGGER_CONSOLE.info(
            "Machine services is not included or is under review %s",
            group_name,
        )
        return
    s3_client = await get_client()
    response = await s3_client.list_objects_v2(
        Bucket="machine.data",
        Prefix=f"sbom_configs/{group_name}",
    )
    target_date = datetime(2023, 12, 12)
    file_names = [
        (obj["Key"], obj["LastModified"])
        for obj in response.get("Contents", [])
        if obj["LastModified"].date() == target_date.date()
    ]
    for config_name, _last_modified in file_names:
        with tempfile.NamedTemporaryFile(prefix="integrates_get_sbom_config_", delete=True) as temp:
            await s3_client.download_fileobj(
                "machine.data",
                f"{config_name}",
                temp,
            )

            temp.seek(0)
            config_yaml = yaml.safe_load(temp)
            execution_id = config_yaml["execution_id"]
            root_nickname = config_yaml["namespace"]
            try:
                git_root = next(
                    root
                    for root in await loaders.group_roots.load(group_name)
                    if isinstance(root, GitRoot)
                    and root.state.status == RootStatus.ACTIVE
                    and root.state.nickname == root_nickname
                )
            except StopIteration:
                LOGGER_CONSOLE.warning(
                    "Could not find root for the execution",
                    extra={
                        "extra": {
                            "execution_id": execution_id,
                            "nickname": root_nickname,
                        },
                    },
                )
                return
            json_results = await get_results_log(execution_id)
            if json_results:
                await process_json_sbom(json_results, git_root, group_name)
            return


async def main() -> None:
    loaders = get_new_context()
    groups = await get_all_group_names(loaders)
    await collect((process_group(group) for group in groups), workers=64)


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:    %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("%s", execution_time)
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
