"""
Move VULNERABLE vulns from some findings to another

Execution Time:    2024-06-15 at 02:58:32
Finalization Time: 2024-06-15 at 02:58:42
Execution Time:    2024-06-19 at 17:00:39
Finalization Time: 2024-06-19 at 17:00:51
"""

import logging
import logging.config
import time
import uuid

from aioextensions import (
    collect,
    run,
)

from integrates.batch_dispatch.move_root import (
    _add_evidence,
)
from integrates.custom_utils import (
    cvss as cvss_utils,
)
from integrates.custom_utils import (
    findings as findings_utils,
)
from integrates.custom_utils import (
    vulnerabilities as vulns_utils,
)
from integrates.custom_utils.criteria import (
    CRITERIA_VULNERABILITIES,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    findings as findings_model,
)
from integrates.db_model import (
    vulnerabilities as vulns_model,
)
from integrates.db_model.findings.enums import (
    FindingStateStatus,
)
from integrates.db_model.findings.types import (
    Finding,
    FindingState,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateReason,
    VulnerabilityStateStatus,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityRequest,
)
from integrates.findings.domain.evidence import (
    EVIDENCE_NAMES,
)
from integrates.settings import (
    LOGGING,
)
from integrates.unreliable_indicators.enums import (
    EntityDependency,
)
from integrates.unreliable_indicators.operations import (
    update_unreliable_indicators_by_deps,
)
from integrates.vulnerabilities import (
    domain as vulns_domain,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")
EMAIL_INTEGRATES = "integrates@fluidattacks.com"


async def process_vuln(
    *,
    loaders: Dataloaders,
    vuln: Vulnerability,
    target_finding_id: str,
) -> str:
    LOGGER.info(
        "Processing vuln",
        extra={
            "extra": {
                "vuln_id": vuln.id,
                "target_finding_id": target_finding_id,
            },
        },
    )
    historic_state = await loaders.vulnerability_historic_state.load(
        VulnerabilityRequest(vulnerability_id=vuln.id, finding_id=vuln.finding_id),
    )
    historic_treatment = await loaders.vulnerability_historic_treatment.load(
        VulnerabilityRequest(vulnerability_id=vuln.id, finding_id=vuln.finding_id),
    )
    historic_verification = await loaders.vulnerability_historic_verification.load(
        VulnerabilityRequest(vulnerability_id=vuln.id, finding_id=vuln.finding_id),
    )
    historic_zero_risk = await loaders.vulnerability_historic_zero_risk.load(
        VulnerabilityRequest(vulnerability_id=vuln.id, finding_id=vuln.finding_id),
    )
    new_id = str(uuid.uuid4())
    await vulns_model.add(
        vulnerability=vuln._replace(
            finding_id=target_finding_id,
            id=new_id,
            state=vuln.state,
            verification=vuln.verification,
        ),
    )
    LOGGER.info(
        "Created new vuln",
        extra={
            "extra": {
                "target_finding_id": target_finding_id,
                "new_id": new_id,
            },
        },
    )
    new_vulnerability = await vulns_domain.get_vulnerability(loaders, new_id)
    await vulns_model.update_historic(
        current_value=new_vulnerability,
        historic=tuple(historic_state) or (vuln.state,),
    )
    new_vulnerability = await vulns_domain.get_vulnerability(loaders, new_id, clear_loader=True)
    await vulns_model.update_new_historic(
        current_value=new_vulnerability,
        historic=tuple(historic_state) or (vuln.state,),
    )
    if historic_treatment:
        new_vulnerability = await vulns_domain.get_vulnerability(loaders, new_id, clear_loader=True)
        await vulns_model.update_historic(
            current_value=new_vulnerability,
            historic=tuple(historic_treatment),
        )
        new_vulnerability = await vulns_domain.get_vulnerability(loaders, new_id, clear_loader=True)
        await vulns_model.update_new_historic(
            current_value=new_vulnerability,
            historic=tuple(historic_treatment),
        )
    if historic_verification:
        new_vulnerability = await vulns_domain.get_vulnerability(loaders, new_id, clear_loader=True)
        await vulns_model.update_historic(
            current_value=new_vulnerability,
            historic=tuple(historic_verification),
        )
        new_vulnerability = await vulns_domain.get_vulnerability(loaders, new_id, clear_loader=True)
        await vulns_model.update_new_historic(
            current_value=new_vulnerability,
            historic=tuple(historic_verification),
        )
    if historic_zero_risk:
        new_vulnerability = await vulns_domain.get_vulnerability(loaders, new_id, clear_loader=True)
        await vulns_model.update_historic(
            current_value=new_vulnerability,
            historic=tuple(historic_zero_risk),
        )
        new_vulnerability = await vulns_domain.get_vulnerability(loaders, new_id, clear_loader=True)
        await vulns_model.update_new_historic(
            current_value=new_vulnerability,
            historic=tuple(historic_zero_risk),
        )
    await vulns_utils.close_vulnerability(
        vulnerability=vuln,
        modified_by=EMAIL_INTEGRATES,
        loaders=loaders,
        closing_reason=VulnerabilityStateReason.ROOT_MOVED_TO_ANOTHER_GROUP,
    )
    LOGGER.info(
        "Old vuln closed by exclusion",
        extra={
            "extra": {
                "finding_id": vuln.finding_id,
                "vuln_id": vuln.id,
            },
        },
    )
    return new_id


async def process_finding(
    *,
    loaders: Dataloaders,
    group_name: str,
    source_finding_id: str,
    vulns: tuple[Vulnerability, ...],
) -> None:
    LOGGER.info(
        "Processing finding",
        extra={
            "extra": {
                "group_name": group_name,
                "source_finding_id": source_finding_id,
                "vulns": len(vulns),
            },
        },
    )
    source_finding = await loaders.finding.load(source_finding_id)
    if source_finding is None or findings_utils.is_deleted(source_finding):
        LOGGER.info(
            "Not found",
            extra={"extra": {"finding_id": source_finding_id}},
        )
        return

    target_finding_id = str(uuid.uuid4())
    if source_finding.creation:
        initial_state = FindingState(
            modified_by=source_finding.creation.modified_by,
            modified_date=source_finding.creation.modified_date,
            source=source_finding.creation.source,
            status=FindingStateStatus.CREATED,
        )
    finding_number = "447"
    criteria_vulnerability = CRITERIA_VULNERABILITIES[finding_number]
    await findings_model.add(
        finding=Finding(
            attack_vector_description=(source_finding.attack_vector_description),
            description=source_finding.description,
            group_name=group_name,
            id=target_finding_id,
            state=initial_state,
            recommendation=source_finding.recommendation,
            requirements=source_finding.requirements,
            severity_score=cvss_utils.get_severity_score_from_cvss_vector(
                cvss_utils.get_criteria_cvss_vector(criteria_vulnerability),
                cvss_utils.get_criteria_cvss4_vector(criteria_vulnerability),
            ),
            title=f'{finding_number}. {criteria_vulnerability["es"]["title"]}',
            threat=source_finding.threat,
            unfulfilled_requirements=criteria_vulnerability["requirements"],
        ),
    )
    await collect(
        [
            _add_evidence(
                finding_id=target_finding_id,
                evidence_id=evidence_id,
                loaders=loaders,
                source_finding=source_finding,
                evidence=getattr(source_finding.evidences, evidence_name),
            )
            for evidence_id, evidence_name in EVIDENCE_NAMES.items()
        ],
        workers=1,
    )
    LOGGER.info(
        "Created a new one",
        extra={
            "extra": {
                "group_name": group_name,
                "target_finding_id": target_finding_id,
            },
        },
    )

    target_vuln_ids = await collect(
        tuple(
            process_vuln(
                loaders=loaders,
                vuln=vuln,
                target_finding_id=target_finding_id,
            )
            for vuln in vulns
            if vuln.state.status is VulnerabilityStateStatus.VULNERABLE
            and (
                vuln.state.reasons is None
                or (
                    vuln.state.reasons
                    and VulnerabilityStateReason.EXCLUSION not in vuln.state.reasons
                )
            )
        ),
        workers=32,
    )
    LOGGER.info(
        "Updating finding indicators",
        extra={
            "extra": {
                "group_name": group_name,
                "source_finding_id": source_finding_id,
                "target_finding_id": target_finding_id,
            },
        },
    )
    await collect(
        (
            update_unreliable_indicators_by_deps(
                EntityDependency.move_root,
                finding_ids=[source_finding_id],
                vulnerability_ids=[vuln.id for vuln in vulns],
            ),
            update_unreliable_indicators_by_deps(
                EntityDependency.move_root,
                finding_ids=[target_finding_id],
                vulnerability_ids=list(target_vuln_ids),
            ),
        ),
    )


async def _process_finding(
    loaders: Dataloaders,
    group_name: str,
    source_finding_id: str,
    target_finding_id: str,
    vulns: tuple[Vulnerability, ...],
) -> None:
    target_vuln_ids = await collect(
        tuple(
            process_vuln(
                loaders=loaders,
                vuln=vuln,
                target_finding_id=target_finding_id,
            )
            for vuln in vulns
            if (
                vuln.state.reasons is None
                or (
                    vuln.state.reasons
                    and VulnerabilityStateReason.EXCLUSION not in vuln.state.reasons
                )
            )
        ),
        workers=32,
    )
    LOGGER.info(
        "Updating finding indicators",
        extra={
            "extra": {
                "group_name": group_name,
                "source_finding_id": source_finding_id,
                "target_finding_id": target_finding_id,
            },
        },
    )
    await collect(
        (
            update_unreliable_indicators_by_deps(
                EntityDependency.move_root,
                finding_ids=[source_finding_id],
                vulnerability_ids=[vuln.id for vuln in vulns],
            ),
            update_unreliable_indicators_by_deps(
                EntityDependency.move_root,
                finding_ids=[target_finding_id],
                vulnerability_ids=list(target_vuln_ids),
            ),
        ),
    )


async def main() -> None:
    loaders = get_new_context()

    await _process_finding(
        loaders=loaders,
        group_name="",
        source_finding_id="",
        target_finding_id="",
        vulns=tuple(await loaders.finding_vulnerabilities_all.load("")),
    )

    await _process_finding(
        loaders=loaders,
        group_name="",
        source_finding_id="",
        target_finding_id="",
        vulns=tuple(await loaders.finding_vulnerabilities_all.load("")),
    )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
