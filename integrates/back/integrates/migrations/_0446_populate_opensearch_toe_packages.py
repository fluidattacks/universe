# type: ignore


"""
Populates OpenSearch with all the packages from active groups

Execution Time:    2023-10-03 at 17:00:32 UTC
Finalization Time: 2023-10-03 at 17:16:10 UTC

Execution Time:    2023-11-09 at 03:38:08 UTC
Finalization Time: 2023-11-09 at 03:53:53 UTC

Execution Time:    2023-11-16 at 20:34:22 UTC
Finalization Time: 2023-11-16 at 20:43:40 UTC
"""

import asyncio
import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)
from aiohttp.client_exceptions import (
    ClientConnectorError,
    ClientPayloadError,
    ServerDisconnectedError,
    ServerTimeoutError,
)
from boto3.dynamodb.conditions import (
    Key,
)
from botocore.exceptions import (
    ClientError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
)
from more_itertools import (
    chunked,
)
from opensearchpy.helpers import (
    BulkIndexError,
    async_bulk,
)

from integrates.class_types.types import (
    Item,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.organizations.domain import (
    get_all_active_group_names,
)
from integrates.search.client import (
    get_client,
    search_shutdown,
    search_startup,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger(__name__)
NETWORK_ERRORS = (
    asyncio.TimeoutError,
    ClientConnectorError,
    ClientError,
    ClientPayloadError,
    ConnectionError,
    ConnectionResetError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
    ServerDisconnectedError,
    ServerTimeoutError,
)


async def process_packages(group_name: str, packages: tuple[Item, ...]) -> None:
    actions = [
        {
            "_id": "#".join([package["pk"], package["sk"]]),
            "_index": "packages_index",
            "_op_type": "index",
            "_source": package,
        }
        for package in packages
    ]

    client = await get_client()
    try:
        await async_bulk(client=client, actions=actions)
    except BulkIndexError as ex:
        for error in ex.errors:
            LOGGER.info("%s %s", group_name, error["index"]["error"]["reason"])


@retry_on_exceptions(exceptions=NETWORK_ERRORS, max_attempts=3, sleep_seconds=3.0)
async def process_group(group_name: str) -> None:
    primary_key = keys.build_key(
        facet=TABLE.facets["toe_packages_metadata"],
        values={"group_name": group_name},
    )
    key_structure = TABLE.primary_key
    condition_expression = Key(key_structure.partition_key).eq(primary_key.partition_key) & Key(
        key_structure.sort_key,
    ).begins_with("PACKAGES#")
    response = await operations.query(
        condition_expression=condition_expression,
        facets=(TABLE.facets["toe_packages_metadata"],),
        table=TABLE,
    )
    packages = response.items
    await collect(
        tuple(
            process_packages(group_name, packages_chunk)
            for packages_chunk in chunked(packages, 100)
        ),
        workers=5,
    )
    LOGGER.info(
        "Group processed",
        extra={
            "extra": {
                "group_name": group_name,
                "toe_packages": len(packages),
            },
        },
    )


async def main() -> None:
    loaders = get_new_context()
    active_group_names = sorted(await get_all_active_group_names(loaders))
    await search_startup()
    client = await get_client()
    await client.indices.delete(index="packages_index")
    await client.indices.create(index="packages_index")
    await collect(
        tuple(process_group(group_name) for group_name in active_group_names),
        workers=4,
    )
    await search_shutdown()


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER.info("\n%s\n%s", execution_time, finalization_time)
