# type: ignore
"""
It is not necessary to add the nickname at the beginning of
the vulnerabilities, the nickname was used to identify the origin of the
vulnerabilities, now each vulnerability has the root_id

Execution Time:
Finalization Time:
"""

import logging
import logging.config
import time
from collections.abc import (
    Awaitable,
    Callable,
)

from aioextensions import (
    collect,
    run,
)

from integrates.custom_utils.vulnerabilities import (
    get_path_from_integrates_vulnerability,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.roots.types import (
    GitRoot,
    RootRequest,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityType,
)
from integrates.db_model.vulnerabilities.types import (
    VulnerabilityMetadataToUpdate,
)
from integrates.db_model.vulnerabilities.update import (
    update_metadata,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    groups = await orgs_domain.get_all_active_group_names(loaders=loaders)
    for group in groups:
        LOGGER_CONSOLE.info("Processing group %s", group)
        findings = await loaders.group_drafts_and_findings.load(group)
        vulns = await loaders.finding_vulnerabilities.load_many_chained(
            [fin.id for fin in findings],
        )
        vulns = tuple(vuln for vuln in vulns if vuln.type == VulnerabilityType.LINES)
        roots_dict: dict[str, GitRoot] = {
            root.id: root
            for root in (
                await loaders.root.load_many(
                    [RootRequest(group, vuln) for vuln in {vuln.root_id for vuln in vulns}],
                )
            )
        }
        futures: Callable[[], Awaitable[None]] = []
        for vuln in vulns:
            if vuln.root_id not in roots_dict:
                continue
            if vuln.where.startswith(roots_dict[vuln.root_id].state.nickname):
                new_where = get_path_from_integrates_vulnerability(vuln.where, vuln.type)[1]
                if vuln.where == new_where:
                    continue
                futures = [
                    *futures,
                    update_metadata(
                        finding_id=vuln.finding_id,
                        metadata=VulnerabilityMetadataToUpdate(where=new_where),
                        vulnerability_id=vuln.id,
                    ),
                ]
        LOGGER_CONSOLE.info("%s vulnerabilities will be updated", len(futures))
        await collect(
            futures,
            workers=15,
        )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
