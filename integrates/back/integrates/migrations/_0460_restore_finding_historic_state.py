# type: ignore
"""
Restore historic states and historic verification in findings.

Execution Time:    2023-11-23 at 01:26:43 UTC
Finalization Time: 2023-11-23 at 01:26:59 UTC

Execution Time:    2023-11-23 at 15:43:37 UTC
Finalization Time: 2023-11-23 at 15:43:48 UTC

Execution Time:    2024-06-28 at 02:13:46 UTC
Finalization Time: 2024-06-28 at 02:13:49 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.findings.get import (
    get_historic_state,
    get_historic_verification,
)
from integrates.db_model.findings.types import (
    FindingState,
    FindingVerification,
)
from integrates.db_model.findings.utils import (
    format_state_item,
    format_verification_item,
)
from integrates.db_model.utils import (
    get_as_utc_iso_format,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")
BACKUP_TABLE = TABLE._replace(name="pitr_table")
CURRENT_TABLE = TABLE
GROUP_NAME = "group_name"
ORGANIZATION_ID = "ORG#id"


async def add_historic_state(finding_id: str, state: FindingState) -> None:
    items = []

    state_key = keys.build_key(
        facet=TABLE.facets["finding_historic_state"],
        values={
            "id": finding_id,
            "iso8601utc": get_as_utc_iso_format(state.modified_date),
        },
    )
    state_item = format_state_item(state, state_key=state_key)
    items.append(state_item)
    await operations.batch_put_item(items=tuple(items), table=TABLE)
    LOGGER.info(
        "Historic state added",
        extra={
            "extra": {
                "finding_id": finding_id,
                "state": get_as_utc_iso_format(state.modified_date),
            },
        },
    )


async def process_historic_state(finding_id: str) -> None:
    historic_backup_data = await get_historic_state(finding_id, BACKUP_TABLE)
    historic_current_data = await get_historic_state(finding_id)
    historic_to_update = [
        state for state in historic_backup_data if state not in historic_current_data
    ]

    if not historic_to_update:
        LOGGER.info(
            "Nothing to process",
            extra={
                "extra": {
                    "finding_id": finding_id,
                },
            },
        )
        return

    await collect(tuple(add_historic_state(finding_id, state) for state in historic_to_update))

    LOGGER.info(
        "Historic states processed",
        extra={
            "extra": {
                "finding_id": finding_id,
                "state_items": len(historic_to_update),
            },
        },
    )


async def add_historic_verification(finding_id: str, verification: FindingVerification) -> None:
    items = []
    verification_item = format_verification_item(verification)
    verification_key = keys.build_key(
        facet=TABLE.facets["finding_historic_verification"],
        values={
            "id": finding_id,
            "iso8601utc": get_as_utc_iso_format(verification.modified_date),
        },
    )
    key_structure = TABLE.primary_key
    historic_verification_item = {
        key_structure.partition_key: verification_key.partition_key,
        key_structure.sort_key: verification_key.sort_key,
        **verification_item,
    }
    items.append(historic_verification_item)
    await operations.batch_put_item(items=tuple(items), table=TABLE)
    LOGGER.info(
        "Historic verification added",
        extra={
            "extra": {
                "finding_id": finding_id,
                "state": get_as_utc_iso_format(verification.modified_date),
            },
        },
    )


async def process_historic_verification(finding_id: str) -> None:
    historic_backup_data = await get_historic_verification(finding_id, BACKUP_TABLE)
    historic_current_data = await get_historic_verification(finding_id)
    historic_to_update = [
        verification
        for verification in historic_backup_data
        if verification not in historic_current_data
    ]

    if not historic_to_update:
        LOGGER.info(
            "Nothing to process",
            extra={
                "extra": {
                    "finding_id": finding_id,
                },
            },
        )
        return

    await collect(
        tuple(
            add_historic_verification(finding_id, verification)
            for verification in historic_to_update
        ),
        workers=16,
    )

    LOGGER.info(
        "Historic verification processed",
        extra={
            "extra": {
                "finding_id": finding_id,
                "verification_items": len(historic_to_update),
            },
        },
    )


async def process_finding(finding_id: str) -> None:
    await process_historic_state(finding_id)
    await process_historic_verification(finding_id)


async def restore_all_findings(loaders: Dataloaders) -> None:
    all_findings = await loaders.group_findings_all.load(GROUP_NAME)

    await collect(
        tuple(process_finding(finding.id) for finding in all_findings),
        workers=16,
    )

    LOGGER.info(
        "Findings processed",
        extra={
            "extra": {
                "findings": len(all_findings),
                "findings_ids": [finding.id for finding in all_findings],
            },
        },
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    group_info = await loaders.group.load(GROUP_NAME)
    if group_info:
        await restore_all_findings(loaders)

        LOGGER.info(
            "Group processed",
            extra={
                "extra": {
                    "group_name": group_info.name,
                },
            },
        )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
