# type: ignore
"""
Add the severity score to all findings

Execution Time:    2023-03-01 at 18:41:23 UTC
Finalization Time: 2023-03-01 at 18:44:39 UTC

Second execution accounting for trial supended groups
and the fix in severity score calculation.

Execution Time:    2023-03-31 at 20:41:48 UTC
Finalization Time: 2023-03-31 at 21:07:54 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)

from integrates.custom_utils import (
    cvss as cvss_utils,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    findings as findings_model,
)
from integrates.db_model.findings.types import (
    FindingMetadataToUpdate,
)
from integrates.organizations.domain import (
    get_all_group_names,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    group_names = await get_all_group_names(loaders)
    groups_findings = await loaders.group_drafts_and_findings_all.load_many(group_names)

    total_groups: int = len(group_names)
    for idx, (group_name, group_findings) in enumerate(
        zip(group_names, groups_findings, strict=False),
    ):
        LOGGER_CONSOLE.info(
            "Processing group %s (%s)...",
            group_name,
            f"{idx+1}/{total_groups}",
        )
        futures = [
            findings_model.update_metadata(
                group_name=group_name,
                finding_id=finding.id,
                metadata=FindingMetadataToUpdate(
                    severity_score=cvss_utils.get_severity_score_summary(finding.severity),
                ),
            )
            for finding in group_findings
            if finding.severity_score != cvss_utils.get_severity_score_summary(finding.severity)
        ]
        await collect(futures, workers=15)


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
