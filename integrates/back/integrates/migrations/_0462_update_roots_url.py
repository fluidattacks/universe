# type: ignore
"""
Execution Time:    2023-11-27 at 16:21:54 UTC
Finalization Time: 2023-11-27 at 16:21:57 UTC

Execution Time:    2023-12-12 at 17:28:09 UTC
Finalization Time: 2023-12-12 at 17:28:12 UTC

Execution Time:    2023-12-12 at 17:42:39 UTC
Finalization Time: 2023-12-12 at 17:42:40 UTC

Execution Time:    2024-08-05 at 21:24:20 UTC
Finalization Time: 2024-08-05 at 21:27:21 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.db_model.roots.types import (
    GitRoot,
    GitRootState,
)
from integrates.db_model.roots.update import (
    update_root_state,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")


async def update_roots_url_state(
    *,
    root: GitRoot,
    new_url: str,
) -> None:
    new_state = GitRootState(
        branch=root.state.branch,
        includes_health_check=root.state.includes_health_check,
        modified_by=root.state.modified_by,
        modified_date=root.state.modified_date,
        nickname=root.state.nickname,
        status=root.state.status,
        url=new_url,
        credential_id=root.state.credential_id,
        gitignore=root.state.gitignore,
        other=root.state.other,
        reason=root.state.reason,
        use_vpn=root.state.use_vpn,
        use_ztna=root.state.use_ztna,
    )

    try:
        await update_root_state(
            current_value=root.state,
            group_name=root.group_name,
            root_id=root.id,
            state=new_state,
        )
    except Exception as error:
        LOGGER.error(error)


async def process_roots(
    loaders: Dataloaders,
    group_name: str,
) -> None:
    urls = {"https://gitlab.com/fluidattacks-new/universe.git": "test"}

    roots = await loaders.group_roots.load(group_name)
    git_roots = [root for root in roots if isinstance(root, GitRoot)]
    await collect(
        tuple(
            update_roots_url_state(root=root, new_url=new_url)
            for root in git_roots
            if (new_url := urls.get(root.state.url, None))
        ),
        workers=8,
    )

    LOGGER.info(
        "Roots processed",
        extra={
            "extra": {
                "group_name": group_name,
                "roots": len(git_roots),
            },
        },
    )


async def process_group(
    loaders: Dataloaders,
    group_name: str,
) -> None:
    group: Group = await loaders.group.load(group_name)
    if group is None:
        LOGGER_CONSOLE.info(
            "group NOT processed",
            extra={
                "extra": {
                    "group_name": group_name,
                },
            },
        )
        return

    await process_roots(loaders, group_name)

    LOGGER_CONSOLE.info(
        "Group processed",
        extra={
            "extra": {
                "group_name": group_name,
            },
        },
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    groups = ["unittesting"]

    await collect(
        tuple(process_group(loaders, group) for group in groups),
        workers=8,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
