"""
Remove deleted orgs from stakeholders access list
Execution Time:    2024-11-22 at 18:03:52 UTC
Finalization Time: 2024-11-22 at 18:04:29 UTC

Execution Time: 2024-11-22 at 21:03:53 UTC
Finalization Time: 2024-11-22 at 21:04:24 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)

from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model import (
    organization_access as org_access_model,
)
from integrates.db_model.stakeholders.get import (
    get_all_stakeholders,
)
from integrates.db_model.stakeholders.types import (
    Stakeholder,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


async def _remove(
    *,
    email: str,
    organization_id: str,
) -> None:
    await org_access_model.remove(email=email, organization_id=organization_id)
    LOGGER_CONSOLE.info(
        "Deleted org removed from stakeholder access",
        extra={"extra": {"stakeholder": email, "orgs": organization_id}},
    )


async def process_stakeholder(stakeholder: Stakeholder) -> None:
    loaders = get_new_context()
    try:
        stakeholder_orgs = await loaders.stakeholder_organizations_access.load(stakeholder.email)
    except KeyError as exc:
        LOGGER_CONSOLE.error(
            "Access data inconsistency",
            extra={
                "extra": {
                    "stakeholder": stakeholder.email,
                    "error": str(exc),
                },
            },
        )
        return

    orgs_ids = {access.organization_id for access in stakeholder_orgs}
    removed_org = [org_id for org_id in orgs_ids if not await loaders.organization.load(org_id)]
    if removed_org:
        await collect(
            _remove(email=stakeholder.email, organization_id=org_id) for org_id in removed_org
        )


async def main() -> None:
    all_stakeholders = await get_all_stakeholders()
    await collect(
        (process_stakeholder(stakeholder) for stakeholder in all_stakeholders),
        workers=32,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
