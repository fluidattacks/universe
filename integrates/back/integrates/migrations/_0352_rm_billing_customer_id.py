# type: ignore
"""
Remove billing customer id from DB since this id belongs to Stripe test_mode

Execution Time:    2023-01-16 at 17:13:36 UTC
Finalization Time: 2023-01-16 at 17:13:51 UTC

"""

import logging
import logging.config
import os
import time
from datetime import (
    datetime,
)

import stripe
from aioextensions import (
    run,
)

from integrates.db_model import (
    TABLE,
)
from integrates.dynamodb import (
    operations,
)
from integrates.dynamodb.types import (
    PrimaryKey,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

FI_STRIPE_API_KEY = os.environ["STRIPE_API_KEY"]
stripe.api_key = FI_STRIPE_API_KEY
logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")


async def process_organization(org_id: str, org_name: str, customer_id: str) -> None:
    try:
        data = stripe.Customer.retrieve(customer_id)
        LOGGER_CONSOLE.info(
            "Everything alright with this org!",
            extra={
                "extra": {
                    "org_name": data["name"],
                    "created_at": datetime.utcfromtimestamp(data["created"]),
                },
            },
        )
    except stripe.InvalidRequestError:
        await operations.update_item(
            item={"billing_information": None},
            key=PrimaryKey(
                partition_key=org_id,
                sort_key=f"ORG#{org_name}",
            ),
            table=TABLE,
        )
        LOGGER_CONSOLE.info(
            "Customer id removed!",
            extra={
                "extra": {
                    "org_name": org_name,
                    "org_id": org_id,
                    "id_removed": customer_id,
                },
            },
        )


async def main() -> None:
    async for organization in orgs_domain.iterate_organizations():
        if organization.billing_information:
            await process_organization(
                organization.id,
                organization.name,
                organization.billing_information.customer_id,
            )
        LOGGER_CONSOLE.info(
            "Org without billing info",
            extra={
                "extra": {
                    "org_name": organization.name,
                    "org_id": organization.id,
                },
            },
        )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
