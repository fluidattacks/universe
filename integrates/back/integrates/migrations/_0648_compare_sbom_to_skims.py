"""
Compare SBOM and SKIMS.SCA vulnerable packages

Execution Time:    2025-01-13 at 14:14:05 UTC
Finalization Time: 2025-01-13 at 15:30:20 UTC
"""

import csv
import logging
import logging.config
import time

from aioextensions import (
    run,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.toe_packages.types import GroupToePackagesRequest
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
)
from integrates.organizations.domain import (
    get_all_active_group_names,
)

LOGGER = logging.getLogger("migrations")


async def process_group(
    loaders: Dataloaders,
    group_name: str,
) -> None:
    findings = await loaders.group_findings.load(group_name)
    sca_vulns = await loaders.finding_vulnerabilities.load_many_chained(
        [fin.id for fin in findings if fin.title.startswith(("011", "393"))],
    )
    open_machine_sca_vulns: list[Vulnerability] = [
        vuln
        for vuln in sca_vulns
        if vuln.state.status == VulnerabilityStateStatus.VULNERABLE
        and vuln.hacker_email == "machine@fluidattacks.com"
    ]

    vulnerable_packages_from_skims = {
        (adv.package, adv.vulnerable_version): (adv.cve, vuln.state.where)
        for vuln in open_machine_sca_vulns
        if (adv := vuln.state.advisories)
    }

    group_packages = await loaders.group_toe_packages.load_nodes(
        GroupToePackagesRequest(group_name=group_name),
    )

    vulnerable_packages_from_sbom = {
        (package.name, package.version): (
            [adv.id for adv in package.package_advisories],
            package.vulnerable,
            package.locations[0].path if package.locations else package.found_by,
        )
        for package in group_packages
    }

    packages_comparison = []

    for package, skims_info in vulnerable_packages_from_skims.items():
        if package in vulnerable_packages_from_sbom:
            packages_comparison.append(
                [
                    group_name,
                    "SKIMS",
                    package[0],
                    package[1],
                    skims_info[0],
                    "FOUND on SBOM",
                    vulnerable_packages_from_sbom[package][0],
                    bool(package[1]),
                    skims_info[1],
                ]
            )
        else:
            packages_comparison.append(
                [
                    group_name,
                    "SKIMS",
                    package[0],
                    package[1],
                    skims_info[0],
                    "NOT FOUND ON SBOM",
                    [],
                    False,
                    skims_info[1],
                ]
            )

    for package, sbom_info in vulnerable_packages_from_sbom.items():
        if bool(sbom_info[1]) and package not in vulnerable_packages_from_skims:
            packages_comparison.append(
                [
                    group_name,
                    "SBOM",
                    package[0],
                    package[1],
                    [],
                    "NOT FOUND ON SKIMS",
                    sbom_info[0],
                    True,
                    sbom_info[2],
                ]
            )

    with open("group_vulnerable_packages.csv", "a+", encoding="utf-8") as handler:
        writer = csv.writer(handler)
        writer.writerows(packages_comparison)

    LOGGER.info("Group  %s processed", group_name)


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    all_group_names = sorted(await get_all_active_group_names(loaders=loaders))
    LOGGER.info("Processing %s total groups", len(all_group_names))

    for group in all_group_names:
        await process_group(loaders=loaders, group_name=group)


if __name__ == "__main__":
    execution_time = time.strftime(
        "Execution Time:    %Y-%m-%d at %H:%M:%S UTC",
    )
    run(main())
    finalization_time = time.strftime(
        "Finalization Time: %Y-%m-%d at %H:%M:%S UTC",
    )
    LOGGER.info("%s\n%s", execution_time, finalization_time)
