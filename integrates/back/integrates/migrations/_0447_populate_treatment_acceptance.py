"""
Populate treatment_acceptance attribute for policies that were created
before.

Start Time:        2023-10-04 at 15:38:56 UTC
Finalization Time: 2023-10-04 at 15:39:04 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)
from boto3.dynamodb.conditions import (
    Attr,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.enums import (
    TreatmentStatus,
)
from integrates.db_model.organization_finding_policies.types import (
    OrgFindingPolicy,
)
from integrates.db_model.organizations.types import (
    Organization,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")


async def update(finding_policy_id: str, org_name: str) -> None:
    key_structure = TABLE.primary_key
    metadata_key = keys.build_key(
        facet=TABLE.facets["org_finding_policy_metadata"],
        values={
            "name": org_name,
            "uuid": finding_policy_id,
        },
    )
    item = {"treatment_acceptance": "ACCEPTED_UNDEFINED"}
    await operations.update_item(
        condition_expression=Attr(key_structure.partition_key).exists(),
        item=item,
        key=metadata_key,
        table=TABLE,
    )
    LOGGER_CONSOLE.info(
        "Organization policy processed",
        extra={
            "extra": {
                "Organization Name": org_name,
                "Policy Id": finding_policy_id,
            },
        },
    )


async def process_policy(policy: OrgFindingPolicy) -> None:
    if policy.treatment_acceptance != TreatmentStatus.ACCEPTED_UNDEFINED:
        return

    await update(
        finding_policy_id=policy.id,
        org_name=policy.organization_name,
    )


async def process_organization(
    loaders: Dataloaders,
    organization: Organization,
) -> None:
    org_finding_policies = await loaders.organization_finding_policies.load(organization.name)
    if not org_finding_policies:
        return

    await collect(
        (process_policy(policy) for policy in org_finding_policies),
        workers=16,
    )

    LOGGER_CONSOLE.info(
        "Organization policy processed",
        extra={
            "extra": {
                "Organization Name": organization.name,
            },
        },
    )


async def main() -> None:
    loaders = get_new_context()
    async for organization in orgs_domain.iterate_organizations():
        await process_organization(loaders, organization)


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:        %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
