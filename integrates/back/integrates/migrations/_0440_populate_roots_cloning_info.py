# type: ignore
"""
Populate git root's cloning info with first ok and failed cloning attempts.
This will allow the removal of the historic cloning

Start Time:         2023-09-18 at 21:38:36 UTC
Finalization Time:  2023-09-18 at 22:55:40 UTC
"""

import logging
import logging.config
import time
from datetime import (
    datetime,
)
from typing import (
    NamedTuple,
)

from aioextensions import (
    collect,
    run,
)
from boto3.dynamodb.conditions import (
    Attr,
)

from integrates.custom_exceptions import (
    GroupNotFound,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.roots.types import (
    GitRoot,
    GitRootCloning,
)
from integrates.db_model.utils import (
    get_as_utc_iso_format,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


class CloningInfo(NamedTuple):
    first_failed_cloning: datetime | None = None
    first_successful_cloning: datetime | None = None


async def update_git_root_cloning(
    *,
    cloning_info: CloningInfo,
    current_value: GitRootCloning,
    group_name: str,
    root_id: str,
) -> None:
    key_structure = TABLE.primary_key
    primary_key = keys.build_key(
        facet=TABLE.facets["git_root_metadata"],
        values={"name": group_name, "uuid": root_id},
    )
    await operations.update_item(
        condition_expression=(
            Attr(key_structure.partition_key).exists()
            & Attr("cloning.modified_date").eq(get_as_utc_iso_format(current_value.modified_date))
        ),
        item={
            "cloning.first_failed_cloning": get_as_utc_iso_format(cloning_info.first_failed_cloning)
            if cloning_info.first_failed_cloning
            else None,
            "cloning.first_successful_cloning": get_as_utc_iso_format(
                cloning_info.first_successful_cloning,
            )
            if cloning_info.first_successful_cloning
            else None,
        },
        key=primary_key,
        table=TABLE,
    )


async def get_root_cloning_info(loaders: Dataloaders, root: GitRoot) -> CloningInfo:
    historic_cloning = await loaders.root_historic_cloning.load(root.id)
    first_successful_cloning = min(
        (entry.modified_date for entry in historic_cloning if entry.status == "OK"),
        default=None,
    )
    first_failed_cloning = min(
        (entry.modified_date for entry in historic_cloning if entry.status == "FAILED"),
        default=None,
    )

    return CloningInfo(
        first_failed_cloning=first_failed_cloning,
        first_successful_cloning=first_successful_cloning,
    )


async def process_root(loaders: Dataloaders, root: GitRoot) -> None:
    cloning_info = await get_root_cloning_info(loaders, root)
    LOGGER_CONSOLE.info(
        "Root: %s \t%s %s \t%s",
        root.group_name,
        root.id,
        root.state.nickname,
        str(cloning_info),
    )
    await update_git_root_cloning(
        cloning_info=cloning_info,
        current_value=root.cloning,
        group_name=root.group_name,
        root_id=root.id,
    )


async def process_group(loaders: Dataloaders, group_name: str) -> None:
    group = await loaders.group.load(group_name)
    if not group:
        raise GroupNotFound()

    roots = tuple(
        root
        for root in await loaders.group_roots.load(group_name)
        if isinstance(root, GitRoot)
        and (not root.cloning.first_failed_cloning or not root.cloning.first_successful_cloning)
    )
    await collect(
        tuple(process_root(loaders, root) for root in roots),
        workers=5,
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    all_group_names = sorted(await orgs_domain.get_all_group_names(loaders=loaders))
    await collect(
        tuple(process_group(loaders, group) for group in all_group_names),
        workers=5,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:    %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("%s", execution_time)
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
