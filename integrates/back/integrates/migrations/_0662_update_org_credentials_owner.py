"""
Update organization credentials owner

Execution Time: 2025-02-11 at 20:20:06 UTC
Finalization Time: 2025-02-11 at 20:20:57 UTC
"""

import logging

from aioextensions import collect

from integrates.custom_utils import datetime as datetime_utils
from integrates.dataloaders import get_new_context
from integrates.db_model import (
    credentials as credentials_model,
)
from integrates.db_model.credentials.types import Credentials
from integrates.db_model.organizations.get import get_all_organizations
from integrates.migrations.utils import log_time

LOGGER = logging.getLogger("migrations")

CURRENT_OWNER = ""
NEW_OWNER = ""
MODIFIED_BY = "integrates@fluidattacks.com"


async def _update_credential(credential: Credentials) -> None:
    new_state = credential.state._replace(
        modified_by=MODIFIED_BY, modified_date=datetime_utils.get_utc_now(), owner=NEW_OWNER
    )
    await credentials_model.update_credentials(
        current_value=credential,
        credential_id=credential.id,
        organization_id=credential.organization_id,
        state=new_state,
    )


@log_time(LOGGER)
async def main() -> None:
    all_orgs = {org.name: org.id for org in await get_all_organizations()}

    count = 0
    total_orgs = len(all_orgs)
    for org_name, org_id in all_orgs.items():
        org_credentials = [
            credential
            for credential in await get_new_context().organization_credentials.load(org_id)
            if credential.state.owner == CURRENT_OWNER
        ]
        results = await collect(
            [_update_credential(credential) for credential in org_credentials],
            workers=8,
        )

        for result in results:
            if isinstance(result, Exception):
                LOGGER.error("Error in organization %s: %s", org_name, result)

        org_credentials_after = [
            credential
            for credential in await get_new_context().organization_credentials.load(org_id)
            if credential.state.owner == NEW_OWNER
        ]

        count += 1
        LOGGER.info(
            "(%s/%s) Credentials in %s organization has been processed",
            count,
            total_orgs,
            org_name,
            extra={
                "extra": {
                    "org_credentials": [credential.id for credential in org_credentials],
                    "org_credentials_after": [
                        credential.id for credential in org_credentials_after
                    ],
                }
            },
        )


if __name__ == "__main__":
    main()  # type: ignore[unused-coroutine]
