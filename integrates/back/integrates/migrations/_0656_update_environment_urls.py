"""
Execution Time:    2025-01-29 at 22:43:40 UTC
Finalization Time: 2025-01-29 at 22:43:52 UTC
Execution Time:    2025-01-29 at 22:53:23 UTC
Finalization Time: 2025-01-29 at 22:53:38 UTC
Execution Time:    2025-01-29 at 22:57:13 UTC
Finalization Time: 2025-01-29 at 23:14:50 UTC

Update some environment urls
"""

import logging
from contextlib import suppress

from aioextensions import collect

from integrates.custom_exceptions import (
    RepeatedToeInput,
    ToeInputAlreadyUpdated,
    ToeInputNotFound,
    UnavailabilityError,
    VulnNotFound,
)
from integrates.custom_utils.filter_vulnerabilities import filter_non_deleted
from integrates.custom_utils.roots import get_root_vulnerabilities
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.roots.add import add_root_environment_secret
from integrates.db_model.roots.remove import remove_environment_url_secret
from integrates.db_model.roots.types import (
    GitRoot,
    RootEnvironmentSecretsRequest,
    RootEnvironmentUrl,
    RootEnvironmentUrlRequest,
    RootEnvironmentUrlsRequest,
)
from integrates.db_model.toe_inputs.types import (
    RootToeInputsRequest,
    ToeInput,
    ToeInputRequest,
)
from integrates.db_model.vulnerabilities.enums import VulnerabilityType
from integrates.db_model.vulnerabilities.types import Vulnerability
from integrates.decorators import retry_on_exceptions
from integrates.migrations.utils import log_time
from integrates.roots.domain import format_environment_id, remove_environment_url_id
from integrates.roots.environments import add_root_environment_url
from integrates.toe.inputs.domain import add, update, validate_entrypoint
from integrates.toe.inputs.types import ToeInputAttributesToAdd, ToeInputAttributesToUpdate
from integrates.vulnerabilities.domain.core import update_description
from integrates.vulnerabilities.types import VulnerabilityDescriptionToUpdate

LOGGER = logging.getLogger("migrations")
SUFFIXES: list[str] = []
GROUP_NAME = ""
MODIFIED_BY = ""

toe_inputs_add = retry_on_exceptions(exceptions=(UnavailabilityError,), sleep_seconds=5)(add)
toe_inputs_update = retry_on_exceptions(exceptions=(UnavailabilityError,), sleep_seconds=5)(update)


def format_url(url: str) -> str:
    formatted_url = url
    for suffix in SUFFIXES:
        formatted_url = formatted_url.removesuffix(suffix)

    return formatted_url


async def get_root_env_toe_inputs(*, old_url: str, root_id: str) -> list[ToeInput]:
    loaders = get_new_context()
    root_toe_inputs = await loaders.root_toe_inputs.load_nodes(
        RootToeInputsRequest(group_name=GROUP_NAME, root_id=root_id, paginate=False),
    )

    return [
        toe_input
        for toe_input in root_toe_inputs
        if toe_input.component.startswith(old_url.rstrip("/"))
    ]


async def process_vulnerabilities(
    *,
    environment_url: RootEnvironmentUrl,
    old_root_toe_inputs: list[ToeInput],
    old_url: str,
) -> None:
    loaders = get_new_context()
    vulnerabilities = [
        vuln
        for vuln in filter_non_deleted(await get_root_vulnerabilities(environment_url.root_id))
        if vuln.type == VulnerabilityType.INPUTS
    ]
    await collect(
        _process_vulnerabilities(
            environment_url=environment_url,
            loaders=loaders,
            vulnerabilities=vulnerabilities,
            toe_input=toe_input,
            old_url=old_url,
        )
        for toe_input in old_root_toe_inputs
    )


async def _update_description(
    *,
    environment_url: RootEnvironmentUrl,
    loaders: Dataloaders,
    vulnerability: Vulnerability,
    old_url: str,
) -> None:
    with suppress(VulnNotFound):
        await update_description(
            loaders=loaders,
            vulnerability_id=vulnerability.id,
            description=VulnerabilityDescriptionToUpdate(
                where=vulnerability.state.where.replace(
                    old_url.rstrip("/"),
                    environment_url.url.rstrip("/"),
                    1,
                ),
            ),
            stakeholder_email=MODIFIED_BY,
        )


async def _process_vulnerabilities(
    *,
    environment_url: RootEnvironmentUrl,
    loaders: Dataloaders,
    vulnerabilities: list[Vulnerability],
    toe_input: ToeInput,
    old_url: str,
) -> None:
    url_vulnerabilities = [
        vulnerability
        for vulnerability in vulnerabilities
        if vulnerability.state.where.startswith(old_url.rstrip("/"))
    ]
    vulnerabilities_in_toe = await collect(
        validate_entrypoint(vulnerability, toe_input, loaders)
        for vulnerability in url_vulnerabilities
    )
    await collect(
        _update_description(
            loaders=loaders,
            vulnerability=vulnerability,
            environment_url=environment_url,
            old_url=old_url,
        )
        for vulnerability, vulnerability_in_toe in zip(
            url_vulnerabilities, vulnerabilities_in_toe, strict=False
        )
        if vulnerability_in_toe
    )


async def process_toe_input(
    *,
    root_id: str,
    toe_inputs: list[ToeInput],
    environment_url: RootEnvironmentUrl,
    old_url: str,
) -> None:
    loaders = get_new_context()
    await collect(
        tuple(
            _process_toe_input(
                loaders=loaders,
                root_id=root_id,
                toe_input=toe_input,
                old_url=old_url,
                new_url=environment_url.url,
            )
            for toe_input in toe_inputs
        ),
    )
    await collect(
        tuple(
            update(
                loaders=loaders,
                current_value=current_value,
                attributes=ToeInputAttributesToUpdate(
                    be_present=False,
                ),
                modified_by=MODIFIED_BY,
            )
            for current_value in toe_inputs
            if current_value.component.startswith(old_url.rstrip("/"))
        ),
    )


@retry_on_exceptions(
    exceptions=(ToeInputAlreadyUpdated,),
)
async def _process_toe_input(
    *,
    loaders: Dataloaders,
    root_id: str,
    toe_input: ToeInput,
    new_url: str,
    old_url: str,
) -> None:
    if not toe_input.component.startswith(old_url.rstrip("/")):
        return
    full_new_url = toe_input.component.replace(old_url.rstrip("/"), new_url.rstrip("/"), 1)
    if toe_input.state.seen_at is None:
        return
    attributes_to_add = ToeInputAttributesToAdd(
        attacked_at=toe_input.state.attacked_at,
        attacked_by=toe_input.state.attacked_by,
        be_present=True,
        first_attack_at=toe_input.state.first_attack_at,
        has_vulnerabilities=toe_input.state.has_vulnerabilities,
        seen_first_time_by=toe_input.state.seen_first_time_by,
        seen_at=toe_input.state.seen_at,
    )
    try:
        await toe_inputs_add(
            loaders=loaders,
            entry_point=toe_input.entry_point,
            environment_id=toe_input.environment_id,
            component=full_new_url,
            group_name=GROUP_NAME,
            root_id=root_id,
            attributes=attributes_to_add,
            is_moving_toe_input=True,
        )
    except RepeatedToeInput as exc:
        current_value = await loaders.toe_input.load(
            ToeInputRequest(
                component=full_new_url,
                entry_point=toe_input.entry_point,
                group_name=GROUP_NAME,
                root_id=root_id,
            ),
        )
        attributes_to_update = ToeInputAttributesToUpdate(
            attacked_at=toe_input.state.attacked_at,
            attacked_by=toe_input.state.attacked_by,
            be_present=True,
            first_attack_at=toe_input.state.first_attack_at,
            has_vulnerabilities=toe_input.state.has_vulnerabilities,
            seen_at=toe_input.state.seen_at,
            seen_first_time_by=toe_input.state.seen_first_time_by,
        )
        if current_value:
            await toe_inputs_update(
                loaders=loaders,
                current_value=current_value,
                attributes=attributes_to_update,
                modified_by=MODIFIED_BY,
                is_moving_toe_input=True,
            )
        else:
            raise ToeInputNotFound() from exc


async def move_environment_url(
    *, loaders: Dataloaders, environment_url: RootEnvironmentUrl
) -> None:
    environment_secrets = await loaders.environment_secrets.load(
        RootEnvironmentSecretsRequest(url_id=environment_url.id, group_name=GROUP_NAME),
    )

    await add_root_environment_url(
        loaders=loaders,
        group_name=GROUP_NAME,
        url=format_url(environment_url.url),
        root_id=environment_url.root_id,
        url_type=environment_url.state.url_type,
        user_email=environment_url.state.modified_by,
        should_notify=False,
        cloud_type=environment_url.state.cloud_name,
        use_connection={
            "use_egress": environment_url.state.use_egress,
            "use_vpn": environment_url.state.use_vpn,
            "use_ztna": environment_url.state.use_ztna,
        },
        is_moving_environment=True,
    )
    new_env_url = await get_new_context().environment_url.load(
        RootEnvironmentUrlRequest(
            environment_url.root_id,
            GROUP_NAME,
            format_environment_id(format_url(environment_url.url)),
        )
    )

    await remove_environment_url_id(
        loaders=loaders,
        root_id=environment_url.root_id,
        url_id=environment_url.id,
        user_email=environment_url.state.modified_by,
        group_name=environment_url.group_name,
        should_notify=False,
    )
    await collect(
        tuple(
            remove_environment_url_secret(
                group_name=GROUP_NAME,
                resource_id=environment_url.id,
                secret_key=environment_secret.key,
            )
            for environment_secret in environment_secrets
        ),
    )

    await collect(
        tuple(
            add_root_environment_secret(
                group_name=GROUP_NAME, resource_id=new_env_url.id, secret=environment_secret
            )
            for environment_secret in environment_secrets
        ),
    )

    old_root_toe_inputs = await get_root_env_toe_inputs(
        root_id=new_env_url.root_id,
        old_url=environment_url.url,
    )
    await process_toe_input(
        root_id=new_env_url.root_id,
        toe_inputs=old_root_toe_inputs,
        environment_url=new_env_url,
        old_url=environment_url.url,
    )
    await process_vulnerabilities(
        environment_url=new_env_url,
        old_root_toe_inputs=old_root_toe_inputs,
        old_url=environment_url.url,
    )


@log_time(LOGGER)
async def main() -> None:
    environments_urls: list[RootEnvironmentUrl] = []
    loaders = get_new_context()
    group_roots = [
        root for root in await loaders.group_roots.load(GROUP_NAME) if isinstance(root, GitRoot)
    ]
    for root in group_roots:
        envs = await loaders.root_environment_urls.load(
            RootEnvironmentUrlsRequest(root.id, root.group_name)
        )
        for env in envs:
            if any(env.url.endswith(suffix) for suffix in SUFFIXES):
                environments_urls.append(env)

    await collect(
        (move_environment_url(loaders=loaders, environment_url=_env) for _env in environments_urls),
        workers=1,
    )


if __name__ == "__main__":
    main()  # type: ignore[unused-coroutine]
