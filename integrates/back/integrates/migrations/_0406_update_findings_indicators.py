"""
Update the indicators of the findings in every group.

Execution Time:     2024-01-24 at 11:34:20 UTC
Finalization Time:  2024-01-24 at 11:47:27 UTC
"""

import logging
import logging.config
import sys
import time

from aioextensions import (
    collect,
    run,
)

from integrates.custom_exceptions import (
    IndicatorAlreadyUpdated,
    UnavailabilityError,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    findings as findings_model,
)
from integrates.db_model.findings.types import (
    Finding,
    FindingTreatmentSummary,
    FindingUnreliableIndicatorsToUpdate,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.dynamodb.exceptions import (
    ConditionalCheckFailedException,
)
from integrates.findings import (
    domain as findings_domain,
)
from integrates.organizations.domain import (
    get_all_group_names,
)
from integrates.settings import (
    LOGGING,
)
from integrates.unreliable_indicators.enums import (
    EntityDependency,
)
from integrates.unreliable_indicators.operations import (  # type: ignore[attr-defined]
    _format_unreliable_status,
    _format_unreliable_verification_summary,
    update_unreliable_indicators_by_deps,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")


async def update_old_indicators(findings: list[str]) -> None:
    return await update_unreliable_indicators_by_deps(
        EntityDependency.remove_vulnerability,
        finding_ids=findings,
    )


@retry_on_exceptions(
    exceptions=(UnavailabilityError,),
    sleep_seconds=10,
)
async def process_finding(loaders: Dataloaders, finding: Finding) -> None:
    newest_report_date = await findings_domain.get_newest_vulnerability_report_date(
        loaders,
        finding.id,
    )
    oldest_report_date = await findings_domain.get_oldest_vulnerability_report_date(
        loaders,
        finding.id,
    )
    treatment_summary = await findings_domain.get_treatment_summary(loaders, finding.id)
    total_open_cvssf = await findings_domain.get_total_open_cvssf(loaders, finding.id)
    total_open_cvssf_v4 = await findings_domain.get_total_open_cvssf_v4(loaders, finding.id)
    verification_summary = await findings_domain.get_verification_summary(loaders, finding.id)  # type: ignore[attr-defined]

    to_update = FindingUnreliableIndicatorsToUpdate(
        unreliable_newest_vulnerability_report_date=newest_report_date,
        unreliable_status=_format_unreliable_status(
            await findings_domain.get_status(loaders, finding.id),
        ),
        submitted_vulnerabilities=(
            await findings_domain.get_submitted_vulnerabilities(loaders, finding.id)
        ),
        rejected_vulnerabilities=(
            await findings_domain.get_rejected_vulnerabilities(loaders, finding.id)
        ),
        max_open_severity_score=(
            await findings_domain.get_max_open_severity_score(loaders, finding.id)
        ),
        oldest_vulnerability_report_date=oldest_report_date,
        treatment_summary=FindingTreatmentSummary(
            untreated=treatment_summary.untreated,
            in_progress=treatment_summary.in_progress,
            accepted=treatment_summary.accepted,
            accepted_undefined=treatment_summary.accepted_undefined,
        ),
        newest_vulnerability_report_date=newest_report_date,
        total_open_cvssf=total_open_cvssf,
        total_open_cvssf_v4=total_open_cvssf_v4,
        max_open_severity_score_v4=(
            await findings_domain.get_max_open_severity_score_v4(loaders, finding.id)
        ),
        verification_summary=_format_unreliable_verification_summary(verification_summary),
    )

    try:
        await findings_model.update_unreliable_indicators(
            current_value=None,
            group_name=finding.group_name,
            finding_id=finding.id,
            indicators=to_update,
        )
    except (ConditionalCheckFailedException, IndicatorAlreadyUpdated) as ex:
        LOGGER.error("Finding update error - %s - %s", finding.id, str(ex))


async def process_group(
    loaders: Dataloaders,
    group_name: str,
    count: int,
    total: int,
) -> None:
    group_findings = await loaders.group_findings.load(group_name)
    if not group_findings:
        return

    start = time.time()
    await collect(
        tuple(process_finding(loaders=loaders, finding=finding) for finding in group_findings),
    )
    await update_old_indicators([finding.id for finding in group_findings])
    end = time.time()

    LOGGER_CONSOLE.info(
        "[%s] %s processed in %s (Findings: %s)",
        f"{count / total * 100:.2f} %",
        group_name,
        f"{(end - start) * 1000:.2f} ms",
        len(group_findings),
    )


async def get_groups(loaders: Dataloaders) -> list[str]:
    groups = sorted(await get_all_group_names(loaders))

    # According to the time of the day, it will skip some groups
    letters = ["abcdefg", "hijklmn", "opqrst", "uvwxyz"]
    hour: int = int(time.strftime("%H"))
    minutes: int = int(time.strftime("%M"))
    if hour % 2 == 0:
        letters.pop(0 if minutes < 30 else 1)
    else:
        letters.pop(2 if minutes < 30 else 3)

    forbidden_letters = "".join(letters)
    LOGGER_CONSOLE.info("At %s:%s...", hour, minutes)

    groups = [g for g in groups if g[0].lower() not in forbidden_letters]

    return groups


async def main(group_list: list[str]) -> None:
    loaders: Dataloaders = get_new_context()
    group_names = await get_groups(loaders=loaders) if len(group_list) == 0 else group_list

    LOGGER_CONSOLE.info("Processing %s groups...", len(group_names))
    await collect(
        tuple(
            process_group(
                loaders=loaders,
                group_name=group,
                count=count + 1,
                total=len(group_names),
            )
            for count, group in enumerate(group_names)
        ),
        workers=1,
    )


if __name__ == "__main__":
    groups_to_review: list[str] = []
    if len(sys.argv) > 1:
        raw_groups: list[str] = sys.argv[1].split(",")
        groups_to_review.extend([group.strip() for group in raw_groups if len(group.strip()) > 0])

    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main(groups_to_review))
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
