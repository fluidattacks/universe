# type: ignore

"""
update user roles

Execution Time:    -
Finalization Time: -
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)

from integrates.db_model.stakeholders import (
    get_all_stakeholders,
)
from integrates.db_model.stakeholders.types import (
    Stakeholder,
    StakeholderMetadataToUpdate,
)
from integrates.settings import (
    LOGGING,
)
from integrates.stakeholders import (
    domain as stakeholders_domain,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")

roles_to_update = [
    "user",
]


async def process_user(user: Stakeholder, progress: float) -> None:
    if user.email.endswith("@fluidattacks.com") and user.role in roles_to_update:
        await stakeholders_domain.update(
            email=user.email,
            metadata=StakeholderMetadataToUpdate(
                role="hacker",
            ),
        )

        LOGGER_CONSOLE.info(
            "User updated",
            extra={
                "extra": {
                    "user email": user.email,
                    "progress": round(progress, 2),
                },
            },
        )


async def main() -> None:
    all_stakeholders = await get_all_stakeholders()
    LOGGER_CONSOLE.info(
        "Active users",
        extra={"extra": {"users_len": len(all_stakeholders)}},
    )
    await collect(
        tuple(
            process_user(user=stakeholder, progress=count / len(all_stakeholders))
            for count, stakeholder in enumerate(all_stakeholders)
        ),
        workers=16,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
