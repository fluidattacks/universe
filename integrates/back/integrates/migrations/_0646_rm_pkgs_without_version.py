"""
Remove all the toe packages without version.


Execution Time:    2024-12-30 at 23:40:38 UTC
Finalization Time: 2024-12-31 at 00:07:00 UTC
"""

import logging
import logging.config
from itertools import (
    chain,
)
from typing import (
    Any,
)

from aioextensions import (
    collect,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.toe_packages.remove import remove_package
from integrates.db_model.toe_packages.types import GroupToePackagesRequest, ToePackage
from integrates.migrations.utils import log_time
from integrates.organizations import (
    domain as orgs_domain,
)

LOGGER = logging.getLogger("migrations")


async def process_packages(pkg: ToePackage) -> dict[str, Any]:
    await remove_package(pkg)

    return {
        "group_name": pkg.group_name,
        "package_id": pkg.id,
    }


async def process_group(
    loaders: Dataloaders,
    group_name: str,
    progress: float,
) -> list[dict[str, str]]:
    group_packages = await loaders.group_toe_packages.load_nodes(
        GroupToePackagesRequest(group_name=group_name),
    )
    if not group_packages:
        LOGGER.info("No packages found in group %s", group_name)
        return []

    results = await collect(
        tuple(process_packages(item) for item in group_packages if item.version in {"", "None"}),
        workers=64,
    )
    LOGGER.info(
        "Group processed %s %s %s",
        group_name,
        f"{round(progress, 2)!s}",
        f"{len(group_packages)=}",
    )
    return list(results)


@log_time(LOGGER)
async def main() -> None:
    loaders: Dataloaders = get_new_context()
    all_group_names = sorted(await orgs_domain.get_all_group_names(loaders=loaders))
    LOGGER.info("%s", f"{all_group_names=}")
    LOGGER.info("%s", f"{len(all_group_names)=}")
    results = list(
        chain.from_iterable(
            await collect(
                tuple(
                    process_group(
                        loaders=loaders,
                        group_name=group,
                        progress=count / len(all_group_names),
                    )
                    for count, group in enumerate(all_group_names)
                ),
                workers=1,
            ),
        ),
    )
    LOGGER.info("%s", f"{len(results)=}")


if __name__ == "__main__":
    main()  # type: ignore[unused-coroutine]
