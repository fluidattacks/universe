"""
Remove mailmap data

The services repo was modified so we need to make sure the data is the same
in both the repo and the database. To reuse the migrations we have used and
tested, we decided to remove all the mailmap data and store the new version
of the mailmap later.

Start Time:        2024-06-12 at 21:30:29 UTC
Finalization Time: 2024-06-13 at 00:23:35 UTC

"""

import logging
import logging.config
import time

from aioextensions import (
    run,
)
from pandas import (
    DataFrame,
)

from integrates.custom_exceptions import (
    MailmapEntryNotFound,
    MailmapSubentryNotFound,
)
from integrates.mailmap.delete import (
    delete_mailmap_entry_with_subentries,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")

ORGANIZATION_ID = "ORG#a23457e2-f81f-44a2-867f-230082af676c"
MAILMAP_PATH = ""


def find_unformatted_lines_candidates(raw_mailmap_df: DataFrame) -> None:
    # find rows containing more than one '<' symbol
    unformatted_lines = raw_mailmap_df[raw_mailmap_df["raw_lines"].str.count("<") > 2]
    if not unformatted_lines.empty:
        LOGGER_CONSOLE.info("Unformatted Lines Candidates:")
        for idx, row in unformatted_lines.iterrows():
            raw_line = row["raw_lines"]
            LOGGER_CONSOLE.info("%d: %s", idx, raw_line)


def find_mapping_issues(mailmap_df: DataFrame) -> None:
    # find rows that associate more than one entry_name to one entry_email
    unique_entry_names = mailmap_df.groupby("entry_email")["entry_name"].nunique()
    entries_with_multiple_names = unique_entry_names[unique_entry_names > 1].index.tolist()
    result_df = mailmap_df[mailmap_df["entry_email"].isin(entries_with_multiple_names)]

    if not result_df.empty:
        # print problematic lines
        LOGGER_CONSOLE.info("Multiple Entry Name Mapping Issues:")
        prev_entry_email = ""
        for idx, row in result_df.iterrows():
            entry_email = row["entry_email"]
            new_line = "\n" if entry_email != prev_entry_email else ""
            entry_email = row["entry_email"]
            entry_name = row["entry_name"]
            LOGGER_CONSOLE.info("%s%d: %s - %s", new_line, idx, entry_email, entry_name)
            prev_entry_email = entry_email


def read_mailmap(path: str) -> DataFrame:
    # read mailmap file
    with open(path, encoding="utf-8") as file:
        raw_lines = file.readlines()

    raw_mailmap_df = DataFrame({"raw_lines": raw_lines})

    find_unformatted_lines_candidates(raw_mailmap_df)

    pattern1 = r"(?P<entry_name>.*?) <(?P<entry_email>.*?)>"
    pattern2 = r"(?P<subentry_name>.*?) <(?P<subentry_email>.*?)>"
    pattern = f"{pattern1} {pattern2}"
    extracted_df = raw_mailmap_df["raw_lines"].str.extract(pattern)

    # removes empty line at the end
    filtered_mailmap_df = extracted_df.dropna(axis=0, how="all")

    mailmap_df = filtered_mailmap_df.sort_values(
        by=["entry_email", "entry_name", "subentry_email", "subentry_name"],
    )

    find_mapping_issues(mailmap_df)

    return mailmap_df


async def main() -> None:
    mailmap_df = read_mailmap(MAILMAP_PATH)
    grouped = mailmap_df.groupby("entry_email")
    for entry_email, _ in grouped:
        try:
            await delete_mailmap_entry_with_subentries(
                entry_email=str(entry_email),
                organization_id=ORGANIZATION_ID,
            )
            LOGGER_CONSOLE.info(
                "Mailmap entry removed %s",
                entry_email,
            )
        except (MailmapEntryNotFound, MailmapSubentryNotFound) as ex:
            LOGGER_CONSOLE.error(ex)

    LOGGER_CONSOLE.info("Mailmap data removed successfully")


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:        %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
