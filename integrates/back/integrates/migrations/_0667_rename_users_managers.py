"""
Replace user_manager by organization_manager in organization
stakeholder access data. Replace user_manager by group_manager
in group stakeholder access data.

Execution Time: 2025-02-19 at 20:39:01 UTC
Finalization Time: 2025-02-19 at 20:39:54 UTC
"""

import time
from datetime import UTC, datetime
from typing import Literal, TypeVar

from aioextensions import (
    collect,
    run,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.group_access.types import (
    GroupAccessMetadataToUpdate,
    GroupAccessState,
    GroupInvitation,
)
from integrates.db_model.organization_access.types import (
    OrganizationAccessMetadataToUpdate,
    OrganizationAccessState,
    OrganizationInvitation,
)
from integrates.db_model.stakeholders.get import (
    get_all_stakeholders,
)
from integrates.db_model.stakeholders.types import (
    Stakeholder,
)
from integrates.group_access.domain import (
    update as update_group_access,
)
from integrates.organizations.domain import (
    update_organization_access,
)

EMAIL = "integrates@fluidattacks.com"

T = TypeVar("T", OrganizationInvitation, GroupInvitation)


def _change_invitation_role(
    invitation: T | None,
    entity: Literal["organization", "group"],
) -> T | None:
    if not invitation:
        return None

    if invitation.role == "user_manager":
        if entity == "group":
            return invitation._replace(role="group_manager")

        return invitation._replace(role="organization_manager")

    return invitation


async def _update_organization_access(loaders: Dataloaders, email: str) -> None:
    stakeholder_orgs_access = await loaders.stakeholder_organizations_access.load(email)
    user_manager_accesses = (
        access for access in stakeholder_orgs_access if access.state.role == "user_manager"
    )
    await collect(
        update_organization_access(
            access.organization_id,
            access.email,
            OrganizationAccessMetadataToUpdate(
                expiration_time=access.expiration_time,
                state=OrganizationAccessState(
                    modified_date=datetime.now(UTC),
                    modified_by=EMAIL,
                    role="organization_manager",
                    has_access=access.state.has_access,
                    invitation=_change_invitation_role(access.state.invitation, "organization"),
                ),
            ),
        )
        for access in user_manager_accesses
    )


async def _update_group_access(loaders: Dataloaders, email: str) -> None:
    stakeholder_groups_access = await loaders.stakeholder_groups_access.load(email)
    user_manager_accesses = (
        access for access in stakeholder_groups_access if access.state.role == "user_manager"
    )
    await collect(
        update_group_access(
            loaders,
            access.email,
            access.group_name,
            GroupAccessMetadataToUpdate(
                expiration_time=access.expiration_time,
                state=GroupAccessState(
                    modified_date=datetime.now(UTC),
                    modified_by=EMAIL,
                    role="group_manager",
                    has_access=access.state.has_access,
                    invitation=_change_invitation_role(access.state.invitation, "group"),
                    responsibility=access.state.responsibility,
                    confirm_deletion=access.state.confirm_deletion,
                ),
            ),
        )
        for access in user_manager_accesses
    )


async def process_stakeholder(stakeholder: Stakeholder) -> None:
    loaders = get_new_context()

    await collect(
        [
            _update_organization_access(loaders, stakeholder.email),
            _update_group_access(loaders, stakeholder.email),
        ],
        workers=2,
    )


async def main() -> None:
    all_stakeholders = await get_all_stakeholders()
    await collect(
        (process_stakeholder(stakeholder) for stakeholder in all_stakeholders),
        workers=32,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    print("\n%s\n%s", execution_time, finalization_time)
