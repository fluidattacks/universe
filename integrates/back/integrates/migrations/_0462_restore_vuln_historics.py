# type: ignore
"""
Restore historic states (state, treatment, verification, zero risk) in
vulnerabilities.

Execution Time:    2023-12-04 at 14:12:53 UTC
Finalization Time: 2023-12-04 at 14:23:36 UTC

Execution Time:    2024-06-28 at 02:59:50 UTC
Finalization Time: 2024-06-28 at 03:01:25 UTC
"""

import logging
import logging.config
import time

import simplejson as json
from aioextensions import (
    collect,
    run,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.types import (
    Treatment,
)
from integrates.db_model.utils import (
    get_as_utc_iso_format,
    serialize,
)
from integrates.db_model.vulnerabilities.get import (
    get_historic_state,
    get_historic_treatment,
    get_historic_verification,
    get_historic_zero_risk,
)
from integrates.db_model.vulnerabilities.types import (
    VulnerabilityState,
    VulnerabilityVerification,
    VulnerabilityZeroRisk,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")
BACKUP_TABLE = TABLE._replace(name="back_up_table_name")
CURRENT_TABLE = TABLE
GROUP_NAME = "group_name"
ORGANIZATION_ID = "ORG#id"


async def add_vuln_historic_zero_risk(vuln_id: str, zero_risk: VulnerabilityZeroRisk) -> None:
    items = []
    zero_risk_key = keys.build_key(
        facet=TABLE.facets["vulnerability_historic_zero_risk"],
        values={
            "id": vuln_id,
            "iso8601utc": get_as_utc_iso_format(zero_risk.modified_date),
        },
    )
    key_structure = TABLE.primary_key
    historic_zero_risk_item = {
        key_structure.partition_key: zero_risk_key.partition_key,
        key_structure.sort_key: zero_risk_key.sort_key,
        **json.loads(json.dumps(zero_risk, default=serialize)),
    }
    items.append(historic_zero_risk_item)
    await operations.batch_put_item(items=tuple(items), table=TABLE)
    LOGGER.info(
        "Vuln historic zero risk added",
        extra={
            "extra": {
                "vuln_id": vuln_id,
                "state": get_as_utc_iso_format(zero_risk.modified_date),
            },
        },
    )


async def restore_vuln_historic_zero_risk(vuln_id: str) -> None:
    zero_risk_backup_data = await get_historic_zero_risk(
        vulnerability_id=vuln_id,
        table=BACKUP_TABLE,
    )
    if not zero_risk_backup_data:
        LOGGER.info(
            "No zero risk to process",
            extra={
                "extra": {
                    "vuln_id": vuln_id,
                },
            },
        )
        return

    zero_risk_current_data = await get_historic_zero_risk(vulnerability_id=vuln_id)
    historic_to_update = [
        zero_risk for zero_risk in zero_risk_backup_data if zero_risk not in zero_risk_current_data
    ]

    if not historic_to_update:
        LOGGER.info(
            "Zero risk up to date",
            extra={
                "extra": {
                    "vuln_id": vuln_id,
                },
            },
        )
        return

    LOGGER.info(
        "Historic zero risk to process",
        extra={
            "extra": {
                "vuln_id": vuln_id,
                "items": len(historic_to_update),
            },
        },
    )

    await collect(
        tuple(add_vuln_historic_zero_risk(vuln_id, zero_risk) for zero_risk in historic_to_update),
        workers=5,
    )


async def add_vuln_historic_verification(
    vuln_id: str,
    verification: VulnerabilityVerification,
) -> None:
    items = []
    verification_key = keys.build_key(
        facet=TABLE.facets["vulnerability_historic_verification"],
        values={
            "id": vuln_id,
            "iso8601utc": get_as_utc_iso_format(verification.modified_date),
        },
    )
    key_structure = TABLE.primary_key
    historic_verification_item = {
        key_structure.partition_key: verification_key.partition_key,
        key_structure.sort_key: verification_key.sort_key,
        **json.loads(json.dumps(verification, default=serialize)),
    }
    items.append(historic_verification_item)
    await operations.batch_put_item(items=tuple(items), table=TABLE)
    LOGGER.info(
        "Vuln historic verification added",
        extra={
            "extra": {
                "vuln_id": vuln_id,
                "state": get_as_utc_iso_format(verification.modified_date),
            },
        },
    )


async def restore_vuln_historic_verification(vuln_id: str) -> None:
    verification_backup_data = await get_historic_verification(
        vulnerability_id=vuln_id,
        table=BACKUP_TABLE,
    )
    if not verification_backup_data:
        LOGGER.info(
            "No verification to process",
            extra={
                "extra": {
                    "vuln_id": vuln_id,
                },
            },
        )
        return

    verification_current_data = await get_historic_verification(vulnerability_id=vuln_id)
    historic_to_update = [
        verification
        for verification in verification_backup_data
        if verification not in verification_current_data
    ]

    if not historic_to_update:
        LOGGER.info(
            "Verification up to date",
            extra={
                "extra": {
                    "vuln_id": vuln_id,
                },
            },
        )
        return

    LOGGER.info(
        "Historic verification to process",
        extra={
            "extra": {
                "vuln_id": vuln_id,
                "items": len(historic_to_update),
            },
        },
    )

    await collect(
        tuple(
            add_vuln_historic_verification(vuln_id, verification)
            for verification in historic_to_update
        ),
        workers=5,
    )


async def add_vuln_historic_treatment(vuln_id: str, treatment: Treatment) -> None:
    items = []
    treatment_key = keys.build_key(
        facet=TABLE.facets["vulnerability_historic_treatment"],
        values={
            "id": vuln_id,
            "iso8601utc": get_as_utc_iso_format(treatment.modified_date),
        },
    )
    key_structure = TABLE.primary_key
    historic_treatment_item = {
        key_structure.partition_key: treatment_key.partition_key,
        key_structure.sort_key: treatment_key.sort_key,
        **json.loads(json.dumps(treatment, default=serialize)),
    }
    items.append(historic_treatment_item)
    await operations.batch_put_item(items=tuple(items), table=TABLE)
    LOGGER.info(
        "Vuln historic treatment added",
        extra={
            "extra": {
                "vuln_id": vuln_id,
                "state": get_as_utc_iso_format(treatment.modified_date),
            },
        },
    )


async def restore_vuln_historic_treatment(vuln_id: str) -> None:
    treatment_backup_data = await get_historic_treatment(
        vulnerability_id=vuln_id,
        table=BACKUP_TABLE,
    )
    if not treatment_backup_data:
        LOGGER.info(
            "No treatment to process",
            extra={
                "extra": {
                    "vuln_id": vuln_id,
                },
            },
        )
        return

    treatment_current_data = await get_historic_treatment(vulnerability_id=vuln_id)
    historic_to_update = [
        treatment for treatment in treatment_backup_data if treatment not in treatment_current_data
    ]

    if not historic_to_update:
        LOGGER.info(
            "Treatment up to date",
            extra={
                "extra": {
                    "vuln_id": vuln_id,
                },
            },
        )
        return

    LOGGER.info(
        "Historic treatment to process",
        extra={
            "extra": {
                "vuln_id": vuln_id,
                "items": len(historic_to_update),
            },
        },
    )

    await collect(
        tuple(add_vuln_historic_treatment(vuln_id, treatment) for treatment in historic_to_update),
        workers=5,
    )


async def add_vuln_historic_state(vuln_id: str, state: VulnerabilityState) -> None:
    items = []
    state_key = keys.build_key(
        facet=TABLE.facets["vulnerability_historic_state"],
        values={
            "id": vuln_id,
            "iso8601utc": get_as_utc_iso_format(state.modified_date),
        },
    )
    key_structure = TABLE.primary_key
    historic_state_item = {
        key_structure.partition_key: state_key.partition_key,
        key_structure.sort_key: state_key.sort_key,
        **json.loads(json.dumps(state, default=serialize)),
    }
    items.append(historic_state_item)
    await operations.batch_put_item(items=tuple(items), table=TABLE)
    LOGGER.info(
        "Vuln historic state added",
        extra={
            "extra": {
                "vuln_id": vuln_id,
                "state": get_as_utc_iso_format(state.modified_date),
            },
        },
    )


async def restore_vuln_historic_state(vuln_id: str) -> None:
    historic_state_backup_data = await get_historic_state(
        vulnerability_id=vuln_id,
        table=BACKUP_TABLE,
    )
    historic_state_current_data = await get_historic_state(vulnerability_id=vuln_id)
    historic_to_update = [
        state for state in historic_state_backup_data if state not in historic_state_current_data
    ]
    if not historic_to_update:
        LOGGER.info(
            "No state to process",
            extra={
                "extra": {
                    "vuln_id": vuln_id,
                },
            },
        )
        return

    LOGGER.info(
        "Historic states to process",
        extra={
            "extra": {
                "vuln_id": vuln_id,
                "state_items": len(historic_to_update),
            },
        },
    )

    await collect(
        tuple(add_vuln_historic_state(vuln_id, state) for state in historic_to_update),
        workers=5,
    )


async def process_vulnerability(vuln_id: str) -> None:
    await restore_vuln_historic_state(vuln_id)
    await restore_vuln_historic_treatment(vuln_id)
    await restore_vuln_historic_verification(vuln_id)
    await restore_vuln_historic_zero_risk(vuln_id)


async def restore_all_vulnerabilities(loaders: Dataloaders, finding_id: str) -> None:
    vulnerabilities = await loaders.finding_vulnerabilities_all.load(finding_id)

    if not vulnerabilities:
        return

    await collect(
        tuple(process_vulnerability(vulnerability.id) for vulnerability in vulnerabilities),
        workers=5,
    )

    LOGGER.info(
        "Vulnerabilities processed",
        extra={
            "extra": {
                "finding_id": finding_id,
                "vulns": len(vulnerabilities),
            },
        },
    )


async def process_finding(loaders: Dataloaders, finding_id: str) -> None:
    await restore_all_vulnerabilities(loaders, finding_id)


async def restore_all_findings(loaders: Dataloaders) -> None:
    all_findings = await loaders.group_findings_all.load(GROUP_NAME)

    await collect(
        tuple(process_finding(loaders, finding.id) for finding in all_findings),
        workers=1,
    )

    LOGGER.info(
        "Findings processed",
        extra={
            "extra": {
                "findings": len(all_findings),
                "findings_ids": [finding.id for finding in all_findings],
            },
        },
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    group_info = await loaders.group.load(GROUP_NAME)
    if group_info:
        await restore_all_findings(loaders)

        LOGGER.info(
            "Group processed",
            extra={
                "extra": {
                    "group_name": group_info.name,
                },
            },
        )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
