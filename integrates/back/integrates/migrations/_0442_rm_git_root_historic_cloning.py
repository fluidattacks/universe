"""
Remove all items related to the facet git_root_historic_cloning

Start Time:         2023-09-21 at 22:39:49 UTC
Finalization Time:  2023-09-22 at 00:37:46 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.roots.remove import (
    _remove_root_facets,
)
from integrates.db_model.roots.types import (
    Root,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


async def process_root(root: Root) -> None:
    await _remove_root_facets(
        root_id=root.id,
        facets=(TABLE.facets["git_root_historic_cloning"],),
    )
    LOGGER_CONSOLE.info(
        "Root: %s %s %s",
        root.group_name,
        root.id,
        root.state.nickname,
    )


async def process_group(loaders: Dataloaders, group_name: str) -> None:
    await collect(
        tuple(process_root(root) for root in await loaders.group_roots.load(group_name)),
        workers=5,
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    all_group_names = sorted(await orgs_domain.get_all_group_names(loaders=loaders))
    await collect(
        tuple(process_group(loaders, group) for group in all_group_names),
        workers=5,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:    %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("%s", execution_time)
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
