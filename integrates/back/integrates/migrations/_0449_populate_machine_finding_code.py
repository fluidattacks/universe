"""
Populate facet machine_finding_code

Start Time:    2023-10-11 at 21:40:00 UTC
Finalization Time: 2023-10-11 at 21:42:44 UTC

Start Time:    2023-10-12 at 19:11:07 UTC
Finalization Time: 2023-10-12 at 19:14:04 UTC
"""

import logging
import logging.config
import time
from collections import (
    Counter,
)
from contextlib import (
    suppress,
)

from aioextensions import (
    collect,
    run,
)

from integrates.custom_exceptions import (
    GroupNotFound,
    RepeatedMachineFindingCode,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.constants import (
    MACHINE_EMAIL,
)
from integrates.db_model.findings.add import (
    _add_machine_finding_code,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


async def _add_code(*, finding: Finding) -> None:
    with suppress(RepeatedMachineFindingCode):
        await _add_machine_finding_code(finding=finding)


async def process_group(loaders: Dataloaders, group_name: str) -> None:
    group = await loaders.group.load(group_name)
    if not group:
        raise GroupNotFound()

    machine_findings = [
        finding
        for finding in await loaders.group_findings.load(group_name)
        if finding.creation and finding.creation.modified_by == MACHINE_EMAIL
    ]
    title_counter = Counter(finding.get_criteria_code() for finding in machine_findings)
    duplicates = dict(filter(lambda x: x[1] > 1, title_counter.items()))
    if duplicates:
        LOGGER_CONSOLE.warning("Duplicated finding codes at %s: %s", group_name, duplicates)

    await collect(
        [_add_code(finding=finding) for finding in machine_findings],
        workers=16,
    )
    LOGGER_CONSOLE.info("Processed group %s: %s findings", group_name, len(machine_findings))


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    all_group_names = sorted(await orgs_domain.get_all_group_names(loaders=loaders))
    await collect(
        tuple(process_group(loaders, group) for group in all_group_names),
        workers=4,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:    %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("%s", execution_time)
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
