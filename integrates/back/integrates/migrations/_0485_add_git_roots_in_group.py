"""
Add several git roots in a group.

Start Time:         2024-02-02 at 18:36:38 UTC
Finalization Time:  2024-02-02 at 18:46:25 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    run,
)

from integrates.custom_utils.groups import get_group
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.roots.types import (
    GitRoot,
)
from integrates.roots import (
    domain as roots_domain,
)
from integrates.roots.types import (
    GitRootAttributesToAdd,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger(__name__)


async def process_git_root(
    loaders: Dataloaders,
    group_name: str,
    user_email: str,
    url: str,
    nickname: str,
    credential_id: str,
    branch: str,
) -> None:
    group = await get_group(loaders, group_name)

    try:
        root: GitRoot = await roots_domain.add_git_root(
            loaders=loaders,
            attributes_to_add=GitRootAttributesToAdd(
                branch=branch,
                credentials={"id": credential_id},
                gitignore=[],
                includes_health_check=False,
                url=url,
                nickname=nickname,
            ),
            user_email=user_email,
            group=group,
            required_credentials=True,
        )

        await roots_domain.queue_sync_git_roots(
            loaders=loaders,
            roots=(root,),
            group_name=root.group_name,
            modified_by=user_email,
            group=group,
            queue_on_batch=True,
        )
    except Exception as ex:
        LOGGER_CONSOLE.error(
            "Error adding Git root in the group",
            extra={
                "group_name": group_name,
                "user_email": user_email,
                "url": url,
                "branch": branch,
                "log_type": "Security",
                "exception": ex,
            },
        )

    LOGGER_CONSOLE.info(
        "Added Git root in the group",
        extra={
            "group_name": group_name,
            "branch": branch,
            "url": url,
        },
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    urls = {"url": "branch_name"}
    for attribute, attr_value in urls.items():
        await process_git_root(
            loaders=loaders,
            group_name="okada",
            user_email="integrates@fluidattacks.com",
            url=attribute,
            nickname="",
            credential_id="",
            branch=attr_value,
        )


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:    %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("%s", execution_time)
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
