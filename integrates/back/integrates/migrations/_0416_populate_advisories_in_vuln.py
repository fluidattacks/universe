# type: ignore
"""Populate the advisor data fields in vulnerability."""

import logging
import logging.config
import re
import time

from aioextensions import (
    collect,
    run,
)

from integrates.custom_exceptions import (
    GroupNotFound,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityAdvisory,
)
from integrates.db_model.vulnerabilities.update import (
    update_historic_entry,
)
from integrates.organizations.domain import (
    get_all_group_names,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")


async def process_vulnerability_where(vulnerability: Vulnerability, loader: Dataloaders) -> bool:
    states = await loader.vulnerability_historic_state.load(vulnerability.id)
    pattern = r"^(.*?)\s*\(([^)]*) v(.[\d.]+)[^)]*\)\s*\[(.*?)\]$"
    if not vulnerability.state.advisories or (
        vulnerability.state.advisories and not isinstance(vulnerability.state.advisories.cve, list)
    ):
        if len(states) < 1:
            return False
        for state in reversed(states):
            if match := re.search(pattern, state.where):
                result = (
                    match.group(1),
                    match.group(2),
                    match.group(3),
                    match.group(4).split(","),
                )

                vulnerability_advisory = vulnerability.state._replace(
                    advisories=VulnerabilityAdvisory(
                        package=result[1],
                        vulnerable_version=result[2],
                        cve=result[3],
                    ),
                )
                await update_historic_entry(
                    current_value=vulnerability,
                    finding_id=vulnerability.finding_id,
                    entry=vulnerability_advisory,
                    vulnerability_id=vulnerability.id,
                    force_update=True,
                )
                return True
            if state.advisories and isinstance(state.advisories.cve, list):
                vulnerability_advisory = vulnerability.state._replace(
                    advisories=VulnerabilityAdvisory(
                        package=state.advisories.package,
                        vulnerable_version=(state.advisories.vulnerable_version),
                        cve=state.advisories.cve,
                    ),
                )
                await update_historic_entry(
                    current_value=vulnerability,
                    finding_id=vulnerability.finding_id,
                    entry=vulnerability_advisory,
                    vulnerability_id=vulnerability.id,
                    force_update=True,
                )
                return True
        return False
    return False


async def process_group(
    loaders: Dataloaders,
    group_name: str,
    progress: float,
) -> None:
    group = await loaders.group.load(group_name)
    if not group:
        raise GroupNotFound()
    group_findings = await loaders.group_findings_all.load(group_name)
    findings_filtered = [
        finding
        for finding in group_findings
        if finding.state.status and any(finding.title.startswith(code) for code in ["011", "393"])
    ]

    if not findings_filtered:
        return
    group_vulns = await loaders.finding_vulnerabilities_all.load_many_chained(
        [finding.id for finding in findings_filtered],
    )
    machine_vulns = [
        vuln
        for vuln in group_vulns
        if vuln.hacker_email in ["machine@fluidattacks.com", "kamado@fluidattacks.com"]
    ]

    await collect(
        tuple(process_vulnerability_where(vuln, loaders) for vuln in machine_vulns),
        workers=16,
    )
    LOGGER_CONSOLE.info("Group processed %s %s", group_name, f"{round(progress, 2)!s}")


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    group_names = sorted(await get_all_group_names(loaders=loaders))
    LOGGER_CONSOLE.info("%s", f"{group_names=}")
    LOGGER_CONSOLE.info("%s", f"{len(group_names)=}")
    await collect(
        tuple(
            process_group(
                loaders=loaders,
                group_name=group,
                progress=count / len(group_names),
            )
            for count, group in enumerate(group_names)
        ),
        workers=8,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:    %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("%s", execution_time)
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
