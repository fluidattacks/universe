from datetime import datetime
from decimal import Decimal

from integrates.authz.policy import get_group_level_role, get_user_level_role
from integrates.custom_exceptions import (
    CredentialInUse,
    InvalidFieldLength,
    InvalidGitCredentials,
    InvalidOrganization,
    OrganizationNotFound,
    StakeholderNotInOrganization,
    TrialRestriction,
)
from integrates.custom_utils.stakeholders import get_stakeholder
from integrates.dataloaders import get_new_context
from integrates.db_model.enums import CredentialType
from integrates.db_model.groups.enums import GroupManaged, GroupStateStatus, GroupTier
from integrates.db_model.organization_access.types import (
    OrganizationAccess,
    OrganizationAccessRequest,
    OrganizationAccessState,
    OrganizationInvitation,
)
from integrates.db_model.organizations.types import OrganizationBilling
from integrates.db_model.types import PoliciesToUpdate
from integrates.mailer import groups as groups_mail
from integrates.organizations.domain import (
    add_billing_information,
    add_credentials,
    add_organization,
    add_stakeholder,
    complete_register_for_organization_invitation,
    get_all_active_group_names,
    get_all_deleted_groups,
    get_all_group_names,
    get_all_trial_groups,
    get_credentials,
    get_stakeholder_role,
    get_stakeholders,
    invite_to_organization,
    iterate_organizations_and_groups,
    reject_register_for_organization_invitation,
    remove_credentials,
    remove_organization,
    update_credentials,
    update_policies,
    update_stakeholder_role,
    verify_azure_org,
)
from integrates.organizations.types import CredentialAttributesToAdd, CredentialAttributesToUpdate
from integrates.organizations.utils import get_organization
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    CredentialsFaker,
    CredentialsStateFaker,
    GitRootFaker,
    GitRootStateFaker,
    GroupFaker,
    GroupStateFaker,
    OrganizationAccessFaker,
    OrganizationAccessStateFaker,
    OrganizationFaker,
    PortfolioFaker,
    StakeholderFaker,
    TrialFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import parametrize, raises

ORG_ID = "ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db"
EMAIL = "nonexisting@fluidattacks.com"
EMAIL_2 = "nonexisting@testdomain.com"
EMAIL_MODIFIED = "jdoe@fluidattacks.com"
TEST_PSWD = "a14_T#t$fo2w903om/4fRc1;3Y"
TEST_TKN = "token-1234567890"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name="org1")],
        ),
    )
)
async def test_add_billing_information() -> None:
    # Act
    organization = await get_organization(loaders=get_new_context(), organization_key=ORG_ID)
    await add_billing_information(
        organization,
        OrganizationBilling(
            billing_email=EMAIL,
            customer_id="1234567890",
            last_modified_by=EMAIL_MODIFIED,
            modified_at=datetime.now(),
        ),
    )
    organization_bill = await get_organization(loaders=get_new_context(), organization_key=ORG_ID)

    # Assert
    assert organization_bill
    assert organization_bill.billing_information
    assert organization_bill.billing_information[0].billing_email == EMAIL


@mocks(aws=IntegratesAws())
async def test_add_organization() -> None:
    # Act
    await add_organization(
        loaders=get_new_context(),
        organization_name="testname",
        email=EMAIL_2,
        country="Colombia",
    )

    loaders = get_new_context()
    organization = await get_organization(loaders, "testname")
    access = await loaders.organization_access.load(
        OrganizationAccessRequest(email=EMAIL_2, organization_id=organization.id)
    )

    # Assert
    assert organization.name == "testname"
    assert access
    assert not access.state.invitation
    assert access.state.has_access
    assert access.state.role == "organization_manager"
    assert access.expiration_time is None


@parametrize(
    args=["org_name", "error_message"],
    cases=[
        ["org1", "org1 organization name is already taken"],
        ["org-invalid/", "org-invalid/ organization name is invalid"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(organizations=[OrganizationFaker(id=ORG_ID, name="org1")])
    )
)
async def test_add_organization_fail(org_name: str, error_message: str) -> None:
    # Assert
    with raises(InvalidOrganization(error_message).__class__):
        await add_organization(
            loaders=get_new_context(),
            organization_name=org_name,
            email=EMAIL,
            country="Colombia",
        )


@parametrize(args=["org_name"], cases=[["a"], ["fa"], ["f" * 23], ["neworg" * 10]])
@mocks(aws=IntegratesAws())
async def test_add_organization_fail_len(org_name: str) -> None:
    # Assert
    with raises(InvalidFieldLength):
        await add_organization(
            loaders=get_new_context(),
            organization_name=org_name,
            email=EMAIL,
            country="Colombia",
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organization_access=[OrganizationAccessFaker(organization_id=ORG_ID, email=EMAIL)],
            organizations=[OrganizationFaker(id=ORG_ID, name="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL, enrolled=False)],
            trials=[TrialFaker(email=EMAIL, completed=False)],
        )
    )
)
async def test_add_organization_fail_trial() -> None:
    # Assert
    with raises(TrialRestriction):
        await add_organization(
            loaders=get_new_context(),
            organization_name="orgtest",
            email=EMAIL,
            country="Colombia",
        )


@parametrize(
    args=["role"],
    cases=[
        ["customer_manager"],
        ["resourcer"],
        ["organization_manager"],
        ["user"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name="org1")],
            stakeholders=[
                StakeholderFaker(
                    email=EMAIL_MODIFIED, enrolled=True, is_registered=True, role="customer_manager"
                )
            ],
        )
    )
)
async def test_add_stakeholder(role: str) -> None:
    # Act
    await add_stakeholder(get_new_context(), ORG_ID, EMAIL, role, EMAIL_MODIFIED)
    org_stakeholders = await get_stakeholders(get_new_context(), ORG_ID, EMAIL)

    # Assert
    assert org_stakeholders is not None
    assert org_stakeholders[0].email == EMAIL


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name="test-name-1")],
            groups=[GroupFaker(organization_id=ORG_ID, name="test-gname")],
        ),
    ),
    others=[Mock(groups_mail, "send_mail_access_granted", "async", None)],
)
async def test_complete_register_for_organization_invitation_stakeholder() -> None:
    # Act
    loaders = get_new_context()

    await invite_to_organization(
        loaders=get_new_context(),
        email=EMAIL,
        role="customer_manager",
        organization_name="test-name-1",
        modified_by="integrates@fluidattacks.com",
    )
    if org_access := await loaders.organization_access.load(
        OrganizationAccessRequest(email=EMAIL, organization_id=ORG_ID)
    ):
        await complete_register_for_organization_invitation(loaders, org_access)

    access = await loaders.organization_access.load(
        OrganizationAccessRequest(email=EMAIL, organization_id=ORG_ID)
    )
    stakeholder = await get_stakeholder(loaders, EMAIL)

    # Assert
    assert access
    assert access.state.invitation
    assert access.state.invitation.is_used
    assert access.state.has_access
    assert access.state.role == "customer_manager"
    assert access.expiration_time is None
    assert stakeholder
    assert stakeholder.is_registered
    assert stakeholder.enrolled
    assert await get_user_level_role(loaders, EMAIL) == "hacker"
    assert await get_group_level_role(loaders, EMAIL, "test-gname") == "customer_manager"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name="test-name-1")],
            stakeholders=[StakeholderFaker(email=EMAIL_2, enrolled=True, is_registered=True)],
        ),
    ),
    others=[Mock(groups_mail, "send_mail_access_granted", "async", None)],
)
async def test_complete_register_for_organization_invitation() -> None:
    # Act
    loaders = get_new_context()
    await invite_to_organization(
        loaders=get_new_context(),
        email=EMAIL_2,
        role="user",
        organization_name="test-name-1",
        modified_by="integrates@fluidattacks.com",
    )
    if org_access := await loaders.organization_access.load(
        OrganizationAccessRequest(email=EMAIL_2, organization_id=ORG_ID)
    ):
        await complete_register_for_organization_invitation(loaders, org_access)
    access = await loaders.organization_access.load(
        OrganizationAccessRequest(email=EMAIL_2, organization_id=ORG_ID)
    )

    # Assert
    assert access
    assert access.state.invitation
    assert access.state.invitation.is_used
    assert access.state.has_access
    assert access.state.role == "user"
    assert access.expiration_time is None


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(organization_id=ORG_ID, name="test-group1"),
                GroupFaker(organization_id=ORG_ID, name="test-group2"),
                GroupFaker(organization_id=ORG_ID, name="test-group3"),
                GroupFaker(
                    organization_id=ORG_ID,
                    name="test-group-managed",
                    state=GroupStateFaker(managed=GroupManaged.UNDER_REVIEW),
                ),
            ],
            organizations=[OrganizationFaker(id=ORG_ID, name="org1")],
        ),
    )
)
async def test_get_all_group_names() -> None:
    # Act
    groups = await get_all_group_names(get_new_context())
    active_groups = await get_all_active_group_names(get_new_context())

    # Assert
    assert groups
    assert active_groups
    assert groups == ["test-group-managed", "test-group1", "test-group2", "test-group3"]
    assert active_groups == ["test-group1", "test-group2", "test-group3"]


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(organization_id=ORG_ID, name="test-group1"),
                GroupFaker(organization_id=ORG_ID, name="test-group2"),
                GroupFaker(
                    organization_id=ORG_ID,
                    name="test-group3",
                    state=GroupStateFaker(managed=GroupManaged.TRIAL, tier=GroupTier.FREE),
                ),
            ],
            organizations=[OrganizationFaker(id=ORG_ID, name="org1")],
        ),
    )
)
async def test_get_all_trial_groups() -> None:
    # Act
    groups = await get_all_trial_groups(get_new_context())
    groups_names = [group.name for group in groups]

    # Assert
    assert groups
    assert groups_names == ["test-group3"]


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(organization_id=ORG_ID, name="test-group1"),
                GroupFaker(organization_id=ORG_ID, name="test-group2"),
                GroupFaker(
                    organization_id=ORG_ID,
                    name="test-group-deleted",
                    state=GroupStateFaker(status=GroupStateStatus.DELETED),
                ),
            ],
            organizations=[OrganizationFaker(id=ORG_ID, name="org1")],
        ),
    )
)
async def test_get_all_deleted_groups() -> None:
    # Act
    groups = await get_all_deleted_groups(get_new_context())
    groups_names = [group.name for group in groups]

    # Assert
    assert groups
    assert groups_names == ["test-group-deleted"]


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organization_access=[OrganizationAccessFaker(email=EMAIL, organization_id=ORG_ID)],
            organizations=[OrganizationFaker(id=ORG_ID, name="org1")],
        ),
    )
)
async def test_get_stakeholder_role() -> None:
    # Act
    role = await get_stakeholder_role(
        loaders=get_new_context(),
        email=EMAIL,
        is_registered=True,
        organization_id=ORG_ID,
    )

    # Assert
    assert role == "organization_manager"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(organizations=[OrganizationFaker(id=ORG_ID, name="org1")]),
    )
)
async def test_get_stakeholder_role_fail() -> None:
    # Assert
    with raises(StakeholderNotInOrganization):
        await get_stakeholder_role(
            loaders=get_new_context(),
            email=EMAIL_2,
            is_registered=True,
            organization_id=ORG_ID,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(organization_id=ORG_ID, name="test-group1"),
                GroupFaker(organization_id=ORG_ID, name="test-group2"),
                GroupFaker(organization_id=ORG_ID, name="test-group3"),
            ],
            organizations=[OrganizationFaker(id=ORG_ID, name="org1")],
        ),
    )
)
async def test_iterate_organizations_and_groups() -> None:
    # Assert
    async for _, org_name, org_groups_names in iterate_organizations_and_groups(
        get_new_context(),
    ):
        assert org_name == "org1"
        assert org_groups_names == ["test-group1", "test-group2", "test-group3"]


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organization_access=[OrganizationAccessFaker(organization_id=ORG_ID, email=EMAIL)],
            organizations=[OrganizationFaker(id=ORG_ID, name="org1")],
        ),
    )
)
async def test_reject_register_for_organization_invitation() -> None:
    # Act
    await reject_register_for_organization_invitation(
        OrganizationAccess(
            organization_id=ORG_ID,
            email=EMAIL,
            expiration_time=None,
            state=OrganizationAccessState(
                modified_date=datetime.fromisoformat("2024-11-22T20:07:57+00:00"),
                modified_by=EMAIL_MODIFIED,
                has_access=False,
                invitation=OrganizationInvitation(is_used=False, role="user", url_token=TEST_TKN),
                role="organization_manager",
            ),
        )
    )
    org_stakeholders = await get_stakeholders(get_new_context(), ORG_ID, EMAIL)

    # Assert
    assert org_stakeholders == []


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            credentials=[CredentialsFaker(credential_id="12345-id", organization_id=ORG_ID)],
            groups=[GroupFaker(organization_id=ORG_ID, name="group-1")],
            organization_access=[OrganizationAccessFaker(email=EMAIL, organization_id=ORG_ID)],
            organizations=[OrganizationFaker(id=ORG_ID, name="org1")],
            portfolios=[
                PortfolioFaker(id="portfolio-1", organization_name="org1", groups=set(["group-1"]))
            ],
        ),
    )
)
async def test_remove_organization() -> None:
    # Act
    await remove_organization(
        loaders=get_new_context(),
        organization_id=ORG_ID,
        organization_name="org1",
        modified_by=EMAIL_MODIFIED,
        aws_external_id="8b301b19-3f50-473c-94r1-9b68810b6a63",
    )

    # Assert
    with raises(OrganizationNotFound):
        await get_organization(get_new_context(), ORG_ID)


@parametrize(
    args=["org_access"],
    cases=[
        [
            OrganizationAccessState(
                modified_date=datetime.now(),
                modified_by=EMAIL_MODIFIED,
                has_access=True,
                invitation=OrganizationInvitation(
                    is_used=True, role="customer_manager", url_token=TEST_TKN
                ),
            )
        ],
        [
            OrganizationAccessState(
                modified_date=datetime.now(),
                modified_by=EMAIL_MODIFIED,
                has_access=True,
            )
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organization_access=[
                OrganizationAccessFaker(
                    email=EMAIL,
                    organization_id=ORG_ID,
                    state=OrganizationAccessStateFaker(
                        modified_by=EMAIL_MODIFIED, has_access=True, role="user"
                    ),
                )
            ],
            organizations=[OrganizationFaker(id=ORG_ID, name="org1")],
            stakeholders=[
                StakeholderFaker(email=EMAIL, enrolled=True, is_registered=True),
                StakeholderFaker(email=EMAIL_MODIFIED, enrolled=True, is_registered=True),
            ],
        )
    )
)
async def test_update_stakeholder_role(org_access: OrganizationAccessState) -> None:
    # Act
    loaders = get_new_context()
    await update_stakeholder_role(
        loaders=get_new_context(),
        user_email=EMAIL,
        organization_access=OrganizationAccess(
            email=EMAIL,
            organization_id=ORG_ID,
            state=org_access,
        ),
        organization_id=ORG_ID,
        new_role="customer_manager",
        modified_by=EMAIL_MODIFIED,
    )
    org_stakeholders = await loaders.organization_stakeholders_access.load(ORG_ID)

    # Assert
    assert org_stakeholders is not None
    assert org_stakeholders[0].email == EMAIL
    assert org_stakeholders[0].state.role == "customer_manager"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name="org1")],
        )
    )
)
async def test_update_policies() -> None:
    # Act
    loaders = get_new_context()
    await update_policies(
        loaders=get_new_context(),
        organization_id=ORG_ID,
        organization_name="org1",
        user_email=EMAIL,
        policies_to_update=PoliciesToUpdate(
            days_until_it_breaks=10,
            inactivity_period=30,
            max_acceptance_days=30,
            max_acceptance_severity=Decimal(6.0),
            max_number_acceptances=3,
            min_acceptance_severity=Decimal(3.0),
            min_breaking_severity=Decimal(4.0),
            vulnerability_grace_period=5,
        ),
    )
    organization = await get_organization(loaders, ORG_ID)

    # Assert
    assert organization
    assert organization.policies.days_until_it_breaks == 10
    assert organization.policies.inactivity_period == 30


@parametrize(
    args=["credential"],
    cases=[
        [
            CredentialAttributesToAdd(
                name="credential-1",
                type=CredentialType.SSH,
                key="c3NoLXJzYSBBQUFBQjNOemFDMXljMkVBQUFBREFRQUJBQUFDQVFETy9nPT0=",
                token=TEST_TKN,
                azure_organization="test-azure-organization",
                arn=None,
                user=None,
                password=None,
            )
        ],
        [
            CredentialAttributesToAdd(
                name="credential-2",
                type=CredentialType.AWSROLE,
                arn="arn:aws:iam::123456789012:role/test-role",
                azure_organization="test-azure-organization",
                key=None,
                token=None,
                user=None,
                password=None,
            )
        ],
        [
            CredentialAttributesToAdd(
                name="credential-3",
                type=CredentialType.HTTPS,
                user=EMAIL,
                password=TEST_PSWD,
                azure_organization="test-azure-organization",
                arn=None,
                key=None,
                token=None,
            )
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name="org1")],
            groups=[GroupFaker(organization_id=ORG_ID, name="group-1")],
        ),
    )
)
async def test_add_credentials(credential: CredentialAttributesToAdd) -> None:
    # Act
    verify_azure_org(credential.azure_organization)
    credentials_id = await add_credentials(
        loaders=get_new_context(),
        attributes=credential,
        organization_id=ORG_ID,
        modified_by=EMAIL,
    )
    org_credentials = await get_credentials(get_new_context(), credentials_id, ORG_ID)

    # Assert
    assert credentials_id
    assert org_credentials is not None


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            credentials=[
                CredentialsFaker(
                    credential_id="credential-1-id",
                    organization_id=ORG_ID,
                    state=CredentialsStateFaker(
                        name="credential-1", state_type=CredentialType.SSH, owner=EMAIL
                    ),
                )
            ],
            organizations=[OrganizationFaker(id=ORG_ID, name="org1")],
            stakeholders=[
                StakeholderFaker(email=EMAIL, enrolled=True, is_registered=True),
                StakeholderFaker(email=EMAIL_MODIFIED, enrolled=True, is_registered=True),
            ],
        ),
    )
)
async def test_update_credentials() -> None:
    # Act
    await update_credentials(
        loaders=get_new_context(),
        attributes=CredentialAttributesToUpdate(
            name="new-credential-1",
            type=CredentialType.HTTPS,
            user=EMAIL,
            password=TEST_PSWD,
            key=None,
            token=None,
        ),
        credentials_id="credential-1-id",
        organization_id=ORG_ID,
        modified_by=EMAIL_MODIFIED,
    )
    credentials = await get_credentials(get_new_context(), "credential-1-id", ORG_ID)

    # Assert
    assert credentials
    assert credentials.state.name == "new-credential-1"
    assert credentials.state.type == CredentialType.HTTPS


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            credentials=[CredentialsFaker(credential_id="credential_id_1", organization_id=ORG_ID)],
            organizations=[OrganizationFaker(id=ORG_ID, name="org1")],
        ),
    )
)
async def test_remove_credentials() -> None:
    credentials_initial = await get_credentials(get_new_context(), "credential_id_1", ORG_ID)
    assert credentials_initial

    await remove_credentials(
        loaders=get_new_context(),
        organization_id=ORG_ID,
        credentials_id="credential_id_1",
    )

    with raises(InvalidGitCredentials):
        await get_credentials(get_new_context(), "credential_id_1", ORG_ID)


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name="org1")],
            credentials=[CredentialsFaker(credential_id="credential_id_1", organization_id=ORG_ID)],
            groups=[GroupFaker(organization_id=ORG_ID, name="group1")],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    organization_name="org1",
                    state=GitRootStateFaker(credential_id="credential_id_1"),
                )
            ],
        ),
    )
)
async def test_remove_credentials_in_use() -> None:
    credentials_initial = await get_credentials(get_new_context(), "credential_id_1", ORG_ID)
    assert credentials_initial

    with raises(CredentialInUse):
        await remove_credentials(
            loaders=get_new_context(),
            organization_id=ORG_ID,
            credentials_id="credential_id_1",
        )
