from integrates.custom_exceptions import (
    CredentialAlreadyExists,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.credentials.types import (
    Credentials,
    OauthAzureSecret,
    OauthBitbucketSecret,
    OauthGithubSecret,
    OauthGitlabSecret,
)
from integrates.db_model.enums import (
    CredentialType,
)


async def validate_credentials_name_in_organization(
    loaders: Dataloaders,
    organization_id: str,
    credentials_name: str,
) -> None:
    org_credentials = await loaders.organization_credentials.load(organization_id)
    credentials_names = {credentials.state.name.strip() for credentials in org_credentials}
    if credentials_name.strip() in credentials_names:
        raise CredentialAlreadyExists()


async def get_owner_credentials_oauth(
    loaders: Dataloaders,
    organization_id: str,
    user_email: str,
    secret_type: (
        type[OauthAzureSecret]
        | type[OauthBitbucketSecret]
        | type[OauthGithubSecret]
        | type[OauthGitlabSecret]
    ),
) -> Credentials | None:
    org_credentials = await loaders.organization_credentials.load(organization_id)
    credentials = [
        credential
        for credential in org_credentials
        if credential.state.type is CredentialType.OAUTH
        and isinstance(credential.secret, secret_type)
        and credential.state.owner.lower() == user_email.lower()
    ]

    return credentials[0] if credentials else None


async def validate_credentials_oauth(
    loaders: Dataloaders,
    organization_id: str,
    user_email: str,
    secret_type: (
        type[OauthAzureSecret]
        | type[OauthBitbucketSecret]
        | type[OauthGithubSecret]
        | type[OauthGitlabSecret]
    ),
) -> None:
    credentials = await get_owner_credentials_oauth(
        loaders,
        organization_id,
        user_email,
        secret_type,
    )
    if credentials:
        raise CredentialAlreadyExists()
