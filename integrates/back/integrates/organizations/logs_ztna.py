import gzip
import json
import logging
import logging.config
from datetime import (
    UTC,
    datetime,
)
from itertools import (
    chain,
)

from aioextensions import (
    collect,
    in_thread,
)
from botocore.exceptions import (
    ClientError,
)

from integrates.context import (
    FI_AWS_S3_PATH_PREFIX,
    FI_AWS_S3_ZTNA_LOGS,
)
from integrates.custom_exceptions import (
    FileNotFound,
    UnavailabilityError,
)
from integrates.custom_utils.validations import (
    is_fluid_staff,
)
from integrates.dynamodb.types import (
    PageInfo,
)
from integrates.organizations.types import (
    ZtnaHttpConnection,
    ZtnaHttpEdge,
    ZtnaHttpLogs,
    ZtnaNetworkConnection,
    ZtnaNetworkEdge,
    ZtnaNetworkLogs,
    ZtnaSessionConnection,
    ZtnaSessionEdge,
    ZtnaSessionLogs,
)
from integrates.s3.operations import (
    get_object_content,
)
from integrates.s3.resource import (
    get_client,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger(__name__)


def get_email(
    objective_email: str,
    target_email: str,
) -> str:
    if is_fluid_staff(objective_email) and not is_fluid_staff(target_email):
        return "help@fluidattacks.com"

    return objective_email.replace("non_identity@", "machine@")


async def list_log_files(
    *,
    name: str,
    bucket: str,
    max_keys: int,
    continuation_token: str | None = None,
) -> tuple[list[str], str]:
    client = await get_client()
    try:
        if continuation_token:
            resp = await client.list_objects_v2(
                Bucket=bucket,
                Prefix=f"{FI_AWS_S3_PATH_PREFIX}{name}",
                MaxKeys=min(max_keys, 200),
                ContinuationToken=continuation_token,
            )
        else:
            resp = await client.list_objects_v2(
                Bucket=bucket,
                Prefix=f"{FI_AWS_S3_PATH_PREFIX}{name}",
                MaxKeys=min(max_keys, 200),
            )

        return (
            [item["Key"].replace(FI_AWS_S3_PATH_PREFIX, "") for item in resp.get("Contents", [])],
            resp.get("NextContinuationToken", ""),
        )
    except ClientError as exc:
        LOGGER.error(
            "Failed to get logs",
            extra={
                "extra": {
                    "exception": exc,
                    "file_name": name,
                    "continuation_token": continuation_token,
                },
            },
        )
        raise UnavailabilityError() from exc


async def get_log_content(file_name: str, bucket: str) -> bytes | None:
    if not any(file_name.endswith(file) for file in [".gz", ".json"]):
        return None

    try:
        return await get_object_content(object_key=file_name, bucket=bucket)
    except (ClientError, FileNotFound):
        return None


def get_prefix(start_date: datetime, end_date: datetime) -> str:
    start_str = start_date.astimezone(tz=UTC).strftime("%Y%m%dT%H%M%S")
    end_str = end_date.astimezone(tz=UTC).strftime("%Y%m%dT%H%M%S")
    for index, (start, end) in enumerate(zip(start_str, end_str, strict=False)):
        if start == end:
            continue
        return start_str[:index]

    return ""


def get_decompress(log_file: bytes) -> list:
    try:
        decompressed_data = gzip.decompress(data=log_file).decode("latin-1")
    except gzip.BadGzipFile:
        decompressed_data = log_file.decode("latin-1")

    return [json.loads(line) for line in decompressed_data.splitlines()]


async def get_logs(
    *,
    organization_name: str,
    log_type: str,
    continuation_token: str | None,
    start_date: datetime,
    end_date: datetime,
    max_keys: int,
) -> tuple[list, str]:
    logs, continuation = await list_log_files(
        name=(f"ztna/{organization_name}/{log_type}/{get_prefix(start_date, end_date)}"),
        continuation_token=continuation_token,
        bucket=FI_AWS_S3_ZTNA_LOGS,
        max_keys=max_keys,
    )

    encode_logs = await collect(
        [get_log_content(file_name=filename, bucket=FI_AWS_S3_ZTNA_LOGS) for filename in logs],
        workers=32,
    )

    return (
        list(
            chain.from_iterable(
                await collect(
                    [
                        in_thread(get_decompress, log_file)
                        for log_file in encode_logs
                        if log_file is not None
                    ],
                    workers=32,
                ),
            ),
        ),
        continuation,
    )


async def get_network_logs(
    *,
    organization_name: str,
    continuation_token: str | None,
    start_date: datetime,
    end_date: datetime,
    max_keys: int,
    email: str,
) -> ZtnaNetworkConnection:
    all_logs, continuation = await get_logs(
        organization_name=organization_name,
        log_type="network",
        continuation_token=continuation_token,
        start_date=start_date,
        end_date=end_date,
        max_keys=max_keys,
    )

    all_logs_edges = tuple(
        ZtnaNetworkEdge(
            node=ZtnaNetworkLogs(
                date=datetime.fromisoformat(log["Datetime"]).astimezone(tz=UTC),
                email=get_email(log["Email"], email),
            ),
            cursor=continuation,
        )
        for log in all_logs
    )

    return ZtnaNetworkConnection(
        edges=tuple(
            sorted(
                [edge for edge in all_logs_edges if start_date <= edge.node.date <= end_date],
                key=lambda edge: edge.node.date,
            ),
        ),
        page_info=PageInfo(has_next_page=bool(continuation), end_cursor=continuation),
    )


async def get_http_logs(
    *,
    organization_name: str,
    continuation_token: str | None,
    start_date: datetime,
    end_date: datetime,
    max_keys: int,
    email: str,
) -> ZtnaHttpConnection:
    all_logs, continuation = await get_logs(
        organization_name=organization_name,
        log_type="http",
        continuation_token=continuation_token,
        start_date=start_date,
        end_date=end_date,
        max_keys=max_keys,
    )

    all_logs_edges = tuple(
        ZtnaHttpEdge(
            node=ZtnaHttpLogs(
                date=datetime.fromisoformat(log["Datetime"]).astimezone(tz=UTC),
                email=get_email(log["Email"], email),
                destination_ip=log["DestinationIP"],
                source_ip=log["SourceIP"],
            ),
            cursor=continuation,
        )
        for log in all_logs
        if str(log["PolicyName"]).lower() == organization_name.lower()
    )

    return ZtnaHttpConnection(
        edges=tuple(
            sorted(
                [edge for edge in all_logs_edges if start_date <= edge.node.date <= end_date],
                key=lambda edge: edge.node.date,
            ),
        ),
        page_info=PageInfo(has_next_page=bool(continuation), end_cursor=continuation),
    )


async def get_session_logs(
    *,
    organization_name: str,
    continuation_token: str | None,
    start_date: datetime,
    end_date: datetime,
    max_keys: int,
    email: str,
) -> ZtnaSessionConnection:
    all_logs, continuation = await get_logs(
        organization_name=organization_name,
        log_type="session",
        continuation_token=continuation_token,
        start_date=start_date,
        end_date=end_date,
        max_keys=max_keys,
    )

    all_logs_edges = tuple(
        ZtnaSessionEdge(
            node=ZtnaSessionLogs(
                end_session_date=datetime.fromisoformat(log["SessionEndTime"]).astimezone(
                    tz=UTC,
                ),
                email=get_email(log["Email"], email),
                start_session_date=datetime.fromisoformat(log["SessionStartTime"]).astimezone(
                    tz=UTC,
                ),
            ),
            cursor=continuation,
        )
        for log in all_logs
    )

    return ZtnaSessionConnection(
        edges=tuple(
            sorted(
                [
                    edge
                    for edge in all_logs_edges
                    if start_date <= edge.node.start_session_date <= end_date
                    and start_date <= edge.node.end_session_date <= end_date
                ],
                key=lambda edge: edge.node.start_session_date,
            ),
        ),
        page_info=PageInfo(has_next_page=bool(continuation), end_cursor=continuation),
    )


async def get_all_logs(
    *,
    organization_name: str,
    start_date: datetime,
    end_date: datetime,
    log_type: str,
) -> list[str]:
    all_logs, continuation = await get_logs(
        organization_name=organization_name,
        log_type=log_type,
        continuation_token=None,
        start_date=start_date,
        end_date=end_date,
        max_keys=200,
    )

    while continuation:
        logs = await get_logs(
            organization_name=organization_name,
            log_type=log_type,
            continuation_token=continuation,
            start_date=start_date,
            end_date=end_date,
            max_keys=200,
        )
        continuation = logs[1]
        all_logs += logs[0]

    return all_logs
