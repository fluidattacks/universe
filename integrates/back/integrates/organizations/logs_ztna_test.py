from datetime import datetime

from integrates import organizations, s3
from integrates.organizations.logs_ztna import (
    get_all_logs,
    get_http_logs,
    get_network_logs,
    get_session_logs,
)
from integrates.organizations.types import ZtnaHttpLogs, ZtnaNetworkLogs, ZtnaSessionLogs
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb, IntegratesS3
from integrates.testing.fakers import OrganizationFaker, StakeholderFaker
from integrates.testing.mocks import Mock, mocks

ORG_ID = "ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db"
EMAIL = "jdoe@test.com"
ORG_NAME = "org1"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            stakeholders=[StakeholderFaker(email=EMAIL, enrolled=True)],
        ),
        s3=IntegratesS3(autoload=True),
    ),
    others=[
        Mock(s3.operations, "FI_AWS_S3_PATH_PREFIX", "function", ""),
        Mock(
            organizations.logs_ztna,
            "get_object_content",
            "async",
            b'{"AccountID":"","Action":"","Datetime":"2024-11-25T00:03:29Z","DestinationIP":"","DestinationPort":"","DetectedProtocol":"","DeviceID":"","DeviceName":"","Email":"jdoe@test.com","OverrideIP":"","OverridePort":0,"PolicyID":"","PolicyName":"","SNI":"","SessionID":"","SourceIP":"","SourceInternalIP":"","SourcePort":3333,"Transport":"udp","UserID":""}\n{"AccountID":"","Action":"","Datetime":"2024-11-25T12:03:59Z","DestinationIP":"","DestinationPort":53,"DetectedProtocol":"","DeviceID":"","DeviceName":"","Email":"jdoe@test.com","OverrideIP":"","OverridePort":0,"PolicyID":"","PolicyName":"","SNI":"","SessionID":"","SourceIP":"","SourceInternalIP":"","SourcePort":3333,"Transport":"udp","UserID":""}',
        ),
        Mock(organizations.logs_ztna, "FI_AWS_S3_PATH_PREFIX", "function", ""),
    ],
)
async def test_get_network_logs() -> None:
    # Act
    network_logs = await get_network_logs(
        organization_name=ORG_NAME,
        continuation_token=None,
        start_date=datetime.fromisoformat("2024-11-24T00:00:00+00:00"),
        end_date=datetime.fromisoformat("2024-11-26T00:00:00+00:00"),
        max_keys=100,
        email=EMAIL,
    )

    # Assert
    assert len(network_logs.edges) == 2
    assert isinstance(network_logs.edges[0].node, ZtnaNetworkLogs)
    assert network_logs.edges[0].node.email == EMAIL
    assert network_logs.edges[1].node.email == network_logs.edges[0].node.email
    assert network_logs.edges[0].node.date == datetime.fromisoformat("2024-11-25T00:03:29Z")
    assert network_logs.edges[1].node.date == datetime.fromisoformat("2024-11-25T12:03:59Z")


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            stakeholders=[StakeholderFaker(email=EMAIL, enrolled=True)],
        ),
        s3=IntegratesS3(autoload=True),
    ),
    others=[
        Mock(s3.operations, "FI_AWS_S3_PATH_PREFIX", "function", ""),
        Mock(
            organizations.logs_ztna,
            "get_object_content",
            "async",
            b'{"AccountID":"","Action":"bypass","BlockedFileHash":"","BlockedFileName":"","BlockedFileReason":"","BlockedFileSize":0,"BlockedFileType":"","Datetime":"2024-12-04T22:26:59Z","DestinationIP":"10.10.10.24","DestinationPort":80,"DeviceID":"","DeviceName":"","DownloadedFileNames":[],"Email":"jdoe@test.com","FileInfo":{"files":[]},"HTTPHost":"","HTTPMethod":"UNKNOWN","HTTPStatusCode":0,"HTTPVersion":"UNK","IsIsolated":false,"PolicyID":"","PolicyName":"org1","Referer":"","RequestID":"","SessionID":"","SourceIP":"44.44.44.24","SourceInternalIP":"","SourcePort":8081,"URL":"","UntrustedCertificateAction":"none","UploadedFileNames":[],"UserAgent":"","UserID":""}',
        ),
        Mock(organizations.logs_ztna, "FI_AWS_S3_PATH_PREFIX", "function", ""),
    ],
)
async def test_get_http_logs() -> None:
    # Act
    http_logs = await get_http_logs(
        organization_name=ORG_NAME,
        continuation_token=None,
        start_date=datetime.fromisoformat("2024-12-01T00:00:00+00:00"),
        end_date=datetime.fromisoformat("2024-12-05T00:00:00+00:00"),
        max_keys=100,
        email=EMAIL,
    )

    # Assert
    assert len(http_logs.edges) == 1
    assert isinstance(http_logs.edges[0].node, ZtnaHttpLogs)
    assert http_logs.edges[0].node.email == EMAIL
    assert http_logs.edges[0].node.date == datetime.fromisoformat("2024-12-04T22:26:59Z")
    assert http_logs.edges[0].node.destination_ip == "10.10.10.24"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            stakeholders=[StakeholderFaker(email=EMAIL, enrolled=True)],
        ),
        s3=IntegratesS3(autoload=True),
    ),
    others=[
        Mock(s3.operations, "FI_AWS_S3_PATH_PREFIX", "function", ""),
        Mock(
            organizations.logs_ztna,
            "get_object_content",
            "async",
            (
                b'{"AccountID":"","BytesReceived":4945,"BytesSent":669,"ClientTCPHandshakeDurationMs":1,'
                b'"ClientTLSCipher":"","ClientTLSHandshakeDurationMs":0,"ClientTLSVersion":"none",'
                b'"ConnectionCloseReason":"PROXY_CONN_REFUSED","ConnectionReuse":false,"DestinationTunnelID":"",'
                b'"DetectedProtocol":"","DeviceID":"","DeviceName":"","EgressColoName":"","EgressIP":"0.0.0.0",'
                b'"EgressPort":0,"EgressRuleID":"","EgressRuleName":"default - egress",'
                b'"Email":"jdoe@test.com","IngressColoName":"IAD","Offramp":"","OriginIP":"10.10.10.24",'
                b'"OriginPort":80,"OriginTLSCertificateIssuer":"","OriginTLSCertificateValidationResult":"NONE",'
                b'"OriginTLSCipher":"","OriginTLSHandshakeDurationMs":0,"OriginTLSVersion":"none","Protocol":"TCP",'
                b'"RuleEvaluationDurationMs":0,"SessionEndTime":"2023-12-11T20:24:42Z","SessionID":"100000001",'
                b'"SessionStartTime":"2023-12-11T20:24:40Z","SourceIP":"3.3.3.24","SourceInternalIP":"",'
                b'"SourcePort":421,"UserID":"","VirtualNetworkID":""}\n{"AccountID":"","BytesReceived":4945,'
                b'"BytesSent":661,"ClientTCPHandshakeDurationMs":2,"ClientTLSCipher":"",'
                b'"ClientTLSHandshakeDurationMs":0,"ClientTLSVersion":"none",'
                b'"ConnectionCloseReason":"PROXY_CONN_REFUSED","ConnectionReuse":false,"DestinationTunnelID":"",'
                b'"DetectedProtocol":"","DeviceID":"","DeviceName":"","EgressColoName":"","EgressIP":"0.0.0.0",'
                b'"EgressPort":0,"EgressRuleID":"","EgressRuleName":"default - egress",'
                b'"Email":"jdoe@test.com","IngressColoName":"IAD","Offramp":"","OriginIP":"10.10.10.24","OriginPort":80,"'
                b'OriginTLSCertificateIssuer":"","OriginTLSCertificateValidationResult":"NONE","OriginTLSCipher":"",'
                b'"OriginTLSHandshakeDurationMs":0,"OriginTLSVersion":"none","Protocol":"TCP",'
                b'"RuleEvaluationDurationMs":0,"SessionEndTime":"2023-12-11T20:24:42Z","SessionID":"100000001",'
                b'"SessionStartTime":"2023-12-11T20:24:41Z","SourceIP":"3.3.3.24","SourceInternalIP":"",'
                b'"SourcePort":422,"UserID":"","VirtualNetworkID":""}\n{"AccountID":"","BytesReceived":4945,'
                b'"BytesSent":665,"ClientTCPHandshakeDurationMs":1,"ClientTLSCipher":"",'
                b'"ClientTLSHandshakeDurationMs":0,"ClientTLSVersion":"none",'
                b'"ConnectionCloseReason":"PROXY_CONN_REFUSED","ConnectionReuse":false,"DestinationTunnelID":"",'
                b'"DetectedProtocol":"","DeviceID":"","DeviceName":"","EgressColoName":"","EgressIP":"0.0.0.0",'
                b'"EgressPort":0,"EgressRuleID":"","EgressRuleName":"default - egress",'
                b'"Email":"jdoe@test.com","IngressColoName":"IAD","Offramp":"","OriginIP":"10.10.10.24","OriginPort":80,'
                b'"OriginTLSCertificateIssuer":"","OriginTLSCertificateValidationResult":"NONE",'
                b'"OriginTLSCipher":"","OriginTLSHandshakeDurationMs":0,"OriginTLSVersion":"none",'
                b'"Protocol":"TCP","RuleEvaluationDurationMs":0,"SessionEndTime":"2023-12-11T20:24:42Z",'
                b'"SessionID":"100000001","SessionStartTime":"2023-12-11T20:24:41Z","SourceIP":"3.3.3.24",'
                b'"SourceInternalIP":"","SourcePort":422,"UserID":"","VirtualNetworkID":""}'
            ),
        ),
        Mock(organizations.logs_ztna, "FI_AWS_S3_PATH_PREFIX", "function", ""),
    ],
)
async def test_get_session_logs() -> None:
    # Act
    session_logs = await get_session_logs(
        organization_name=ORG_NAME,
        continuation_token=None,
        start_date=datetime.fromisoformat("2023-12-10T00:00:00+00:00"),
        end_date=datetime.fromisoformat("2023-12-12T00:00:00+00:00"),
        max_keys=100,
        email=EMAIL,
    )

    # Assert
    assert len(session_logs.edges) == 3
    assert isinstance(session_logs.edges[0].node, ZtnaSessionLogs)
    assert session_logs.edges[0].node.email == EMAIL
    assert session_logs.edges[0].node.start_session_date == datetime.fromisoformat(
        "2023-12-11T20:24:40Z"
    )
    assert session_logs.edges[0].node.end_session_date == datetime.fromisoformat(
        "2023-12-11T20:24:42Z"
    )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            stakeholders=[StakeholderFaker(email=EMAIL, enrolled=True)],
        ),
        s3=IntegratesS3(autoload=True),
    ),
    others=[
        Mock(s3.operations, "FI_AWS_S3_PATH_PREFIX", "function", ""),
        Mock(
            organizations.logs_ztna,
            "get_object_content",
            "async",
            b'{"AccountID":"","Action":"bypass","BlockedFileHash":"","BlockedFileName":"","BlockedFileReason":"","BlockedFileSize":0,"BlockedFileType":"","Datetime":"2024-12-04T22:26:59Z","DestinationIP":"10.10.10.24","DestinationPort":80,"DeviceID":"","DeviceName":"","DownloadedFileNames":[],"Email":"jdoe@test.com","FileInfo":{"files":[]},"HTTPHost":"","HTTPMethod":"UNKNOWN","HTTPStatusCode":0,"HTTPVersion":"UNK","IsIsolated":false,"PolicyID":"","PolicyName":"org1","Referer":"","RequestID":"","SessionID":"","SourceIP":"44.44.44.24","SourceInternalIP":"","SourcePort":8081,"URL":"","UntrustedCertificateAction":"none","UploadedFileNames":[],"UserAgent":"","UserID":""}',
        ),
        Mock(organizations.logs_ztna, "FI_AWS_S3_PATH_PREFIX", "function", ""),
    ],
)
async def test_get_all_logs() -> None:
    # Act
    all_logs = await get_all_logs(
        organization_name=ORG_NAME,
        start_date=datetime.fromisoformat("2024-12-04T00:00:00+00:00"),
        end_date=datetime.fromisoformat("2024-12-05T00:00:00+00:00"),
        log_type="http",
    )

    # Assert
    assert len(all_logs) == 1
