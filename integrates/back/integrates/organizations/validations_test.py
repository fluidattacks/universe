from integrates.custom_exceptions import CredentialAlreadyExists
from integrates.dataloaders import get_new_context
from integrates.db_model.credentials.types import OauthGithubSecret
from integrates.db_model.enums import CredentialType
from integrates.organizations.validations import (
    validate_credentials_name_in_organization,
    validate_credentials_oauth,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import CredentialsFaker, CredentialsStateFaker, OrganizationFaker
from integrates.testing.mocks import mocks
from integrates.testing.utils import raises

ORG_ID = "ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db"
CRED_NAME = "credential-name"
EMAIL = "jdoe@fluidattacks.com"
TEST_ACCESS_TKN = "test-token-github"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            credentials=[
                CredentialsFaker(
                    credential_id="12345-id",
                    organization_id=ORG_ID,
                    secret=OauthGithubSecret(access_token=TEST_ACCESS_TKN),
                    state=CredentialsStateFaker(state_type=CredentialType.OAUTH, owner=EMAIL),
                )
            ],
            organizations=[OrganizationFaker(id=ORG_ID, name="org-1")],
        ),
    )
)
async def test_validate_credentials_oauth_fail() -> None:
    # Assert
    with raises(CredentialAlreadyExists):
        await validate_credentials_oauth(get_new_context(), ORG_ID, EMAIL, OauthGithubSecret)


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            credentials=[
                CredentialsFaker(
                    credential_id="12345-id",
                    organization_id=ORG_ID,
                    secret=OauthGithubSecret(access_token=TEST_ACCESS_TKN),
                    state=CredentialsStateFaker(
                        state_type=CredentialType.OAUTH, owner=EMAIL, name=CRED_NAME
                    ),
                )
            ],
            organizations=[OrganizationFaker(id=ORG_ID, name="org-1")],
        ),
    )
)
async def test_validate_credentials_name_in_organization_fail() -> None:
    # Assert
    with raises(CredentialAlreadyExists):
        await validate_credentials_name_in_organization(get_new_context(), ORG_ID, CRED_NAME)
