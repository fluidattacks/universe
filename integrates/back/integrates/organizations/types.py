from datetime import (
    datetime,
)
from typing import (
    NamedTuple,
)

from integrates.db_model.enums import (
    CredentialType,
)
from integrates.dynamodb.types import (
    PageInfo,
)


class CredentialAttributesToAdd(NamedTuple):
    arn: str | None
    name: str
    key: str | None
    token: str | None
    type: CredentialType
    user: str | None
    password: str | None
    is_pat: bool | None = False
    azure_organization: str | None = None


class CredentialAttributesToUpdate(NamedTuple):
    name: str | None
    key: str | None
    token: str | None
    type: CredentialType | None
    user: str | None
    password: str | None
    is_pat: bool | None = False
    azure_organization: str | None = None


class ZtnaNetworkLogs(NamedTuple):
    date: datetime
    email: str


class ZtnaNetworkEdge(NamedTuple):
    node: ZtnaNetworkLogs
    cursor: str


class ZtnaNetworkConnection(NamedTuple):
    edges: tuple[ZtnaNetworkEdge, ...]
    page_info: PageInfo


class ZtnaHttpLogs(NamedTuple):
    date: datetime
    destination_ip: str
    email: str
    source_ip: str


class ZtnaHttpEdge(NamedTuple):
    node: ZtnaHttpLogs
    cursor: str


class ZtnaHttpConnection(NamedTuple):
    edges: tuple[ZtnaHttpEdge, ...]
    page_info: PageInfo


class ZtnaSessionLogs(NamedTuple):
    email: str
    end_session_date: datetime
    start_session_date: datetime


class ZtnaSessionEdge(NamedTuple):
    node: ZtnaSessionLogs
    cursor: str


class ZtnaSessionConnection(NamedTuple):
    edges: tuple[ZtnaSessionEdge, ...]
    page_info: PageInfo
