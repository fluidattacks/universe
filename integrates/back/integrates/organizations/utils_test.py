from integrates.custom_exceptions import InvalidBase64SshKey
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.credentials.types import (
    AWSRoleSecret,
    HttpsPatSecret,
    HttpsSecret,
    SshSecret,
)
from integrates.organizations.utils import (
    format_credentials_secret_type,
    format_credentials_ssh_key,
    get_organization_roots,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GitRootFaker,
    GitRootStateFaker,
    GroupFaker,
    IPRootFaker,
    IPRootStateFaker,
    OrganizationFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

ORG_ID = "ORG#c30cb3ac-9a0c9-cd2a-d5b7-6a61d6b6f5b5f"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(organization_id=ORG_ID, name="group1")],
            organizations=[OrganizationFaker(id=ORG_ID, name="org-1")],
            roots=[
                GitRootFaker(
                    id="root1",
                    group_name="group1",
                    organization_name="org-1",
                    state=GitRootStateFaker(branch="main", nickname="root1"),
                ),
                IPRootFaker(
                    id="root2",
                    group_name="group1",
                    organization_name="org-1",
                    state=IPRootStateFaker(nickname="root2"),
                ),
            ],
        ),
    )
)
async def test_get_organization_roots() -> None:
    loaders: Dataloaders = get_new_context()

    # Act
    organization_roots = await get_organization_roots(loaders, ORG_ID)

    # Assert
    assert organization_roots
    assert len(organization_roots) == 2


def test_format_credentials_secret_type() -> None:
    # Act
    result = format_credentials_secret_type(
        {"type": "HTTPS", "user": "jdoe@fluidattacks.com", "password": "1234"}
    )
    result_pat = format_credentials_secret_type(
        {"type": "HTTPS", "token": "token", "user": "jdoe@fluidattacks.com", "password": "1234"}
    )
    result_aws = format_credentials_secret_type(
        {"type": "AWSROLE", "arn": "arn:aws:iam::123456789012:role/my-role"}
    )
    result_ssh = format_credentials_secret_type(
        {"type": "SSH", "key": "c3NoLXJzYSBBQUFBQjNOemFDMXljMkVBQUFBREFRQUJBQUFDQVFETy9nPT0K"}
    )

    # Assert
    assert isinstance(result, HttpsSecret)
    assert isinstance(result_pat, HttpsPatSecret)
    assert isinstance(result_aws, AWSRoleSecret)
    assert isinstance(result_ssh, SshSecret)


def test_format_credentials_secret_type_fail() -> None:
    # Assert
    with raises(InvalidBase64SshKey):
        format_credentials_secret_type(
            {"type": "SSH", "key": "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOMqqnkVzrmsq "}
        )


@parametrize(args=["key", "expected_key"], cases=[["VGVzdCBTU0g=", "VGVzdCBTU0gK"]])
def test_format_credential_key(key: str, expected_key: str) -> None:
    assert format_credentials_ssh_key(ssh_key=key) == expected_key
    assert format_credentials_ssh_key(ssh_key=expected_key) == expected_key
