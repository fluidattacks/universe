import asyncio
import contextlib
import functools
import inspect
import logging
import logging.config
import time
from asyncio import (
    sleep,
)
from collections.abc import (
    Awaitable,
    Callable,
    Iterator,
)
from typing import (
    Any,
    ParamSpec,
    TypeVar,
    cast,
)

from aioextensions import (
    collect,
    schedule,
)
from graphql import (
    GraphQLError,
)
from graphql.type.definition import (
    GraphQLResolveInfo,
)
from starlette.requests import (
    Request,
)

from integrates import (
    authz,
)
from integrates.class_types.types import (
    Item,
)
from integrates.custom_exceptions import (
    ExpiredToken,
    FindingNotFound,
    InvalidAuthorization,
    InvalidPositiveArgument,
    OnlyCorporateEmails,
    OrganizationNotFound,
    StakeholderNotInOrganization,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.custom_utils import (
    templates as templates_utils,
)
from integrates.custom_utils import (
    validations_deco,
)
from integrates.custom_utils.personal_email import (
    is_personal_email,
)
from integrates.custom_utils.validations import (
    is_fluid_staff,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.db_model.organizations.types import (
    Organization,
)
from integrates.db_model.organizations.utils import (
    add_org_id_prefix,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
)
from integrates.forces.utils import (
    is_forces_user,
)
from integrates.group_access.domain import (
    get_group_access,
)
from integrates.organization_access import (
    domain as orgs_access,
)
from integrates.sessions import (
    domain as sessions_domain,
)
from integrates.sessions import (
    function,
)
from integrates.sessions import (
    utils as sessions_utils,
)
from integrates.settings import (
    JWT_COOKIE_NAME,
    LOGGING,
)
from integrates.verify.enums import (
    AuthenticationLevel,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)
UNAUTHORIZED_ROLE_MSG = "Security: Unauthorized role attempted to perform operation"
UNAUTHORIZED_OWNER_MSG = "Security: Unauthorized owner attempted to perform operation"
UNAVAILABLE_FINDING_MSG = "Security: The user attempted to operate on an unavailable finding"
NEW_UNAUTHORIZED_ROLE_MSG = "Unauthorized role attempted to perform operation"
NEW_UNAUTHORIZED_OWNER_MSG = "Unauthorized owner attempted to perform operation"
NEW_UNAVAILABLE_FINDING_MSG = "The user attempted to operate on an unavailable finding"

# Typing
TVar = TypeVar("TVar")
TFun = TypeVar("TFun", bound=Callable[..., Any])
P = ParamSpec("P")


@validations_deco.validate_finding_id_deco("identifier")
async def _resolve_from_finding_id(*, context: Any, identifier: str) -> str | None:
    finding_loader = context.loaders.finding
    finding: Finding | None = await finding_loader.load(identifier)
    if finding:
        return finding.group_name
    raise FindingNotFound()


async def _resolve_from_vuln_id(context: Any, identifier: str) -> str | None:
    loaders = context.loaders
    vulnerability: Vulnerability = await loaders.vulnerability.load(identifier)
    group_name = await _resolve_from_finding_id(
        context=context,
        identifier=vulnerability.finding_id,
    )
    if group_name:
        return group_name
    raise FindingNotFound()


def authenticate_session(func: TFun) -> TFun:
    @functools.wraps(func)
    async def authenticate_and_call(*args: Request, **kwargs: str) -> Any:
        request = args[0]
        try:
            await sessions_domain.get_jwt_content(request)
            return await func(*args, **kwargs)
        except (ExpiredToken, InvalidAuthorization):
            response = templates_utils.login(request)
            response.delete_cookie(key=JWT_COOKIE_NAME)
            return response

    return cast(TFun, authenticate_and_call)


def concurrent_decorators(
    *decorators: Callable[..., Any],
) -> Callable[[TFun], TFun]:
    """
    Decorator to fusion many decorators which will be executed concurrently.

    Either:
    - All decorators succeed and the decorated function is called,
    - Any decorator fail and the error is propagated to the caller.

    In the second case the propagated error is guaranteed to come from the
    first task to raise.
    """
    if len(decorators) <= 1:
        raise ValueError("Expected at least 2 decorators as arguments")

    def decorator(func: TFun) -> TFun:
        @functools.wraps(func)
        async def wrapper(*args: str, **kwargs: str) -> Awaitable[str] | str:
            tasks = create_tasks(decorators, func, *args, **kwargs)
            success = await execute_tasks(tasks)
            return await handle_results(success, func, *args, **kwargs)

        return cast(TFun, wrapper)

    return decorator


def create_tasks(
    decorators: tuple[Callable[..., Any], ...],
    func: TFun,
    *args: str,
    **kwargs: str,
) -> Iterator[Any]:
    @functools.wraps(func)
    async def dummy(*_: str, **__: str) -> bool:
        """Dummy function to mimic `func`."""
        return True

    tasks = iter(
        list(
            map(
                schedule,
                [dec(dummy)(*args, **kwargs) for dec in decorators],
            ),
        ),
    )

    return tasks


async def execute_tasks(tasks: Iterator) -> list[Any]:
    success = []
    try:
        for task in tasks:
            task_result = await task
            success.append(task_result.result())  # may rise
    finally:
        # If two or more decorators raised exceptions let's propagate
        # only the first to arrive and cancel the remaining ones
        # to avoid an ugly traceback, also because if one failed
        # there is no purpose in letting the remaining ones run
        cancel_remaining_tasks(tasks)

    return success


def cancel_remaining_tasks(tasks: Iterator) -> None:
    for task in tasks:
        task.cancel()


async def handle_results(success: list[Any], func: TFun, *args: str, **kwargs: str) -> Any:
    # If everything succeed let's call the decorated function
    if success and all(success):
        if asyncio.iscoroutinefunction(func):
            return await func(*args, **kwargs)
        return func(*args, **kwargs)

    # May never happen as decorators raise something on errors
    # But it's nice to have a default value here
    raise RuntimeError("Decorators did not success")


def delete_kwargs(attributes: set[str]) -> Callable[[TVar], TVar]:
    """
    Decorator to delete function's kwargs.
    Useful to perform api migration.
    """

    def wrapped(func: TVar) -> TVar:
        _func = cast(Callable[..., Any], func)

        @functools.wraps(_func)
        def decorated(*args: str, **kwargs: str) -> Callable:
            kwargs = {key: val for key, val in kwargs.items() if key not in attributes}
            return _func(*args, **kwargs)

        return cast(TVar, decorated)

    return wrapped


def _get_context(args: tuple[str, ...], kwargs: Item) -> Any:
    if len(args) > 1 and hasattr(args[1], "context"):
        # Called from resolver function
        return args[1].context
    # Called from custom function
    return kwargs["info"].context


def enforce_group_level_auth_async(func: TVar) -> TVar:
    """Enforce authorization using the group-level role."""
    _func = cast(Callable[..., Any], func)

    @functools.wraps(_func)
    async def verify_and_call(*args: str, **kwargs: str) -> str:
        context = _get_context(args, kwargs)
        user_data = await sessions_domain.get_jwt_content(context)
        subject = user_data["user_email"]
        object_ = await resolve_group_name(context, args, kwargs)
        action = f"{_func.__module__}.{_func.__qualname__}".replace(".", "_")

        if not object_:
            LOGGER.error(
                "Unable to identify group name",
                extra={
                    "extra": {
                        "action": action,
                        "subject": subject,
                    },
                },
            )

        loaders = context.loaders
        enforcer = await authz.get_group_level_enforcer(loaders, subject)
        if not enforcer(object_, action):
            logs_utils.cloudwatch_log(
                context,
                NEW_UNAUTHORIZED_ROLE_MSG,
                extra={"log_type": "Security"},
            )
            raise GraphQLError("Access denied")

        if asyncio.iscoroutinefunction(_func):
            return await _func(*args, **kwargs)

        return _func(*args, **kwargs)

    return cast(TVar, verify_and_call)


def enforce_organization_level_auth_async(func: TVar) -> TVar:
    """Enforce authorization using the organization-level role."""
    _func = cast(Callable[..., Any], func)

    @functools.wraps(_func)
    async def verify_and_call(*args: Any, **kwargs: str) -> Callable:
        context = _get_context(args, kwargs)
        organization_identifier = str(
            kwargs.get("organization_id")
            or kwargs.get("organization_name")
            or (getattr(args[0], "organization_id", None) if args else None)
            or add_org_id_prefix(args[0].id),
        )
        loaders = context.loaders
        organization: Organization = await loaders.organization.load(organization_identifier)
        organization_id = organization.id
        user_data = await sessions_domain.get_jwt_content(context)
        subject = user_data["user_email"]
        action = f"{_func.__module__}.{_func.__qualname__}".replace(".", "_")

        if not organization_id:
            LOGGER.error(
                "Unable to identify organization to check permissions",
                extra={
                    "action": action,
                    "subject": subject,
                },
            )
        enforcer = await authz.get_organization_level_enforcer(loaders, subject)
        if not enforcer(organization_id, action):
            logs_utils.cloudwatch_log(
                context,
                NEW_UNAUTHORIZED_ROLE_MSG,
                extra={"log_type": "Security"},
            )
            raise GraphQLError("Access denied")
        if asyncio.iscoroutinefunction(_func):
            return await _func(*args, **kwargs)

        return _func(*args, **kwargs)

    return cast(TVar, verify_and_call)


def enforce_user_level_auth_async(func: TVar) -> TVar:
    """Enforce authorization using the user-level role."""
    _func = cast(Callable[..., Any], func)

    @functools.wraps(_func)
    async def verify_and_call(*args: str, **kwargs: str) -> Callable:
        context = _get_context(args, kwargs)
        user_data = await sessions_domain.get_jwt_content(context)
        subject = user_data["user_email"]
        action = f"{_func.__module__}.{_func.__qualname__}".replace(".", "_")

        loaders = context.loaders
        enforcer = await authz.get_user_level_enforcer(loaders, subject)
        if not enforcer(action):
            logs_utils.cloudwatch_log(
                context,
                NEW_UNAUTHORIZED_ROLE_MSG,
                extra={"log_type": "Security"},
            )
            raise GraphQLError("Access denied")
        return await _func(*args, **kwargs)

    return cast(TVar, verify_and_call)


def enforce_owner(
    authentication_level: AuthenticationLevel,
) -> Callable[[TVar], TVar]:
    """Enforce authorization using the owner attribute."""

    def wrapper(func: TVar) -> TVar:
        _func = cast(Callable[..., Any], func)

        @functools.wraps(_func)
        async def verify_and_call(*args: str, **kwargs: str) -> Callable:
            context = _get_context(args, kwargs)
            loaders = context.loaders
            state = getattr(args[0], "state", None) if args else None
            owner = getattr(state, "owner", None)

            user_data = await sessions_domain.get_jwt_content(context)
            subject = user_data["user_email"]

            if owner != subject:
                if authentication_level == AuthenticationLevel.GROUP:
                    group_name = await resolve_group_name(context, args, kwargs)
                    await _verify_group_level_authorization(group_name, subject, loaders, context)
                elif authentication_level == AuthenticationLevel.USER:
                    await _verify_user_level_authorization(subject, loaders, context)
                else:
                    raise GraphQLError("Authentication level not found")

            if asyncio.iscoroutinefunction(_func):
                return await _func(*args, **kwargs)

            return _func(*args, **kwargs)

        return cast(TVar, verify_and_call)

    return wrapper


async def _verify_user_level_authorization(subject: str, loaders: Any, context: Any) -> None:
    stakeholder_level_role = await authz.get_user_level_role(loaders=loaders, email=subject)
    if stakeholder_level_role != "admin":
        logs_utils.cloudwatch_log(
            context,
            NEW_UNAUTHORIZED_OWNER_MSG,
            extra={"log_type": "Security"},
        )
        raise GraphQLError("Access denied")


async def _verify_group_level_authorization(
    group_name: str,
    subject: str,
    loaders: Any,
    context: Any,
) -> None:
    stakeholder_access = await get_group_access(
        loaders=loaders,
        group_name=group_name,
        email=subject,
    )
    stakeholder_group_level_role = stakeholder_access.state.role
    if stakeholder_group_level_role == "user":
        logs_utils.cloudwatch_log(
            context,
            NEW_UNAUTHORIZED_OWNER_MSG,
            extra={"log_type": "Security"},
        )
        raise GraphQLError("Access denied")


def rename_kwargs(mapping: dict[str, str]) -> Callable[[TVar], TVar]:
    """
    Decorator to rename function's kwargs.
    Useful to perform breaking changes,
    with backwards compatibility.
    """

    def wrapped(func: TVar) -> TVar:
        _func = cast(Callable[..., Any], func)

        def rename(kwargs: dict[str, str]) -> dict[str, str]:
            return {mapping.get(key, key): val for key, val in kwargs.items()}

        if asyncio.iscoroutinefunction(_func):

            @functools.wraps(_func)
            async def async_wrapper(*args: str, **kwargs: str) -> Callable:
                return await _func(*args, **rename(kwargs))

            return cast(TVar, async_wrapper)

        @functools.wraps(_func)
        def wrapper(*args: Any, **kwargs: Any) -> Any:
            return _func(*args, **rename(kwargs))

        return cast(TVar, wrapper)

    return wrapped


def observable(func: TVar) -> TVar:
    """
    Decorator for adding observability to graphql function resolvers.

    It enables ariadne instrumentation for OpenTelemetry to capture resolvers
    as spans.

    e.g.

    ```
    @observable
    @ENTITY.field("myField")
    async def resolve(parent, info, **kwargs):
        ...
    ```
    """
    _func = cast(Callable[..., Any], func)

    _func.__observable__ = True  # type: ignore[attr-defined]

    if asyncio.iscoroutinefunction(_func):

        @functools.wraps(_func)
        async def async_wrapper(*args: str, **kwargs: str) -> Callable:
            return await _func(*args, **kwargs)

        return cast(TVar, async_wrapper)

    @functools.wraps(_func)
    def wrapper(*args: Any, **kwargs: Any) -> Any:
        return _func(*args, **kwargs)

    return cast(TVar, wrapper)


def require_attribute(attribute: str) -> Callable[[TVar], TVar]:
    def wrapper(func: TVar) -> TVar:
        _func = cast(Callable[..., Any], func)

        @functools.wraps(_func)
        async def resolve_and_call(*args: str, **kwargs: str) -> Callable:
            context = _get_context(args, kwargs)
            store = sessions_domain.get_request_store(context)
            group_name = await resolve_group_name(context, args, kwargs)
            loaders = context.loaders
            group: Group = await loaders.group.load(group_name)

            # Unique ID for this decorator function
            context_store_key: str = function.get_id(
                require_attribute,
                attribute,
                group_name,
            )

            # Within the context of one request we only need to check this once
            # Future calls to this decorator will be passed through
            if not store[context_store_key]:
                enforcer = authz.get_group_service_attributes_enforcer(group)
                if not enforcer(attribute):
                    raise GraphQLError("Access denied")
            store[context_store_key] = True
            return await _func(*args, **kwargs)

        return cast(TVar, resolve_and_call)

    return wrapper


def require_attribute_internal(attribute: str) -> Callable[[TVar], TVar]:
    def wrapper(func: TVar) -> TVar:
        _func = cast(Callable[..., Any], func)

        @functools.wraps(_func)
        async def resolve_and_call(*args: str, **kwargs: str) -> Callable:
            context = _get_context(args, kwargs)
            store = sessions_domain.get_request_store(context)
            group_name = await resolve_group_name(context, args, kwargs)
            group = await context.loaders.group.load(group_name)
            context_store_key = function.get_id(require_attribute_internal, attribute, group_name)

            if not store[context_store_key]:
                enforcer = authz.get_group_service_attributes_enforcer(group)
                user_data = await sessions_domain.get_jwt_content(context)
                subject = user_data["user_email"]
                if enforcer(attribute) and (not is_fluid_staff(subject) or is_forces_user(subject)):
                    raise GraphQLError(attribute)
            store[context_store_key] = True
            return await _func(*args, **kwargs)

        return cast(TVar, resolve_and_call)

    return wrapper


# Factory functions
REQUIRE_CONTINUOUS = require_attribute("is_continuous")
REQUIRE_ADVANCED = require_attribute("has_advanced")
REQUIRE_ASM = require_attribute("has_asm")
REQUIRE_SERVICE_BLACK = require_attribute("has_service_black")
REQUIRE_SERVICE_WHITE = require_attribute("has_service_white")
REQUIRE_REPORT_VULNERABILITIES = require_attribute("can_report_vulnerabilities")
REQUIRE_REQUEST_ZERO_RISK = require_attribute("can_request_zero_risk")
REQUIRE_IS_NOT_UNDER_REVIEW = require_attribute_internal("is_under_review")


def require_continuous(func: TVar) -> TVar:
    return REQUIRE_CONTINUOUS(func)


def require_service_black(func: TVar) -> TVar:
    return REQUIRE_SERVICE_BLACK(func)


def require_service_white(func: TVar) -> TVar:
    return REQUIRE_SERVICE_WHITE(func)


@validations_deco.validate_finding_id_deco("finding_id")
async def _get_finding_id(*, context: Any, finding_id: str) -> None:
    finding_loader = context.loaders.finding
    if finding_loader:
        await finding_loader.load(finding_id)
    else:
        raise FindingNotFound()


def require_finding_access(func: TVar) -> TVar:
    """
    Require_finding_access decorator.
    Verifies that the current user has access to a given finding
    """
    _func = cast(Callable[..., Any], func)

    @functools.wraps(_func)
    async def verify_and_call(*args: str, **kwargs: str) -> Callable:
        context = _get_context(args, kwargs)
        if "finding_id" in kwargs:
            finding_id = kwargs["finding_id"]
        elif "draft_id" in kwargs:
            finding_id = kwargs["draft_id"]
        elif "identifier" in kwargs:
            finding_id = kwargs["identifier"]
        else:
            vulnerability: Vulnerability = await context.loaders.vulnerability.load(
                kwargs["vuln_uuid"],
            )
            finding_id = vulnerability.finding_id
        try:
            await _get_finding_id(context=context, finding_id=finding_id)
        except FindingNotFound:
            logs_utils.cloudwatch_log(
                context,
                NEW_UNAVAILABLE_FINDING_MSG,
                extra={"log_type": "Security"},
            )
            raise

        return await _func(*args, **kwargs)

    return cast(TVar, verify_and_call)


def require_asm(func: TVar) -> TVar:
    return REQUIRE_ASM(func)


def require_advanced(func: TVar) -> TVar:
    return REQUIRE_ADVANCED(func)


def require_report_vulnerabilities(func: TVar) -> TVar:
    return REQUIRE_REPORT_VULNERABILITIES(func)


def require_is_not_under_review(func: TVar) -> TVar:
    return REQUIRE_IS_NOT_UNDER_REVIEW(func)


def require_request_zero_risk(func: TVar) -> TVar:
    return REQUIRE_REQUEST_ZERO_RISK(func)


def require_corporate_email(func: TVar) -> TVar:
    """
    Verifies the domain on the email address does not belong to a free email
    service
    """
    _func = cast(Callable[..., Any], func)

    @functools.wraps(_func)
    async def verify_and_call(*args: str, **kwargs: str) -> Callable:
        context = _get_context(args, kwargs)
        user_data = await sessions_domain.get_jwt_content(context)

        if await is_personal_email(user_data["user_email"]):
            raise OnlyCorporateEmails()

        return await _func(*args, **kwargs)

    return cast(TVar, verify_and_call)


def _create_new_context(action_data: Any, context: Any) -> dict[str, Any]:
    new_context = {
        "action": action_data.field_name if isinstance(action_data, GraphQLResolveInfo) else "",
        "variables": action_data.variable_values
        if isinstance(action_data, GraphQLResolveInfo)
        else "{}",
        "value": context.headers.get("Authorization") or context.cookies.get(JWT_COOKIE_NAME),
    }
    return new_context


def require_login(func: TVar) -> TVar:
    """
    Require_login decorator
    Verifies that the user is logged in with a valid JWT
    """
    _func = cast(Callable[..., Any], func)

    # Unique ID for this decorator function
    context_store_key: str = function.get_id(require_login)

    async def _call_decorated_function(*args: str, **kwargs: str) -> Callable:
        if asyncio.iscoroutinefunction(_func):
            return await _func(*args, **kwargs)
        return _func(*args, **kwargs)

    @functools.wraps(_func)
    async def verify_and_call(*args: Any, **kwargs: str) -> Callable:
        # The underlying request object being served
        context = _get_context(args, kwargs)
        store = sessions_domain.get_request_store(context)

        # Within the context of one request we only need to check this once
        # Future calls to this decorator will be passed trough
        if store[context_store_key]:
            return await _call_decorated_function(*args, **kwargs)

        try:
            action_data: GraphQLResolveInfo = args[1]
            new_context = _create_new_context(action_data, context)
            user_data: Item = await sessions_domain.get_jwt_content(context)
            if sessions_utils.is_api_token(user_data):
                await sessions_domain.verify_jti(
                    context.loaders,
                    user_data["user_email"],
                    new_context,
                    user_data["jti"],
                )
            store[context_store_key] = True
            return await _call_decorated_function(*args, **kwargs)
        except InvalidAuthorization:
            LOGGER.error("Invalid authorization")
            raise GraphQLError("Login required") from None

    return cast(TVar, verify_and_call)


def require_organization_access(func: TVar) -> TVar:
    """
    Decorator
    Verifies that the user trying to fetch information belongs to the
    organization
    """
    _func = cast(Callable[..., Any], func)

    @functools.wraps(_func)
    async def verify_and_call(*args: str, **kwargs: str) -> Callable:
        context = _get_context(args, kwargs)
        organization_identifier = str(
            kwargs.get("identifier")
            or kwargs.get("organization_id")
            or kwargs.get("organization_name")
            or (getattr(args[0], "organization_id", None) if args else None),
        )

        user_data = await sessions_domain.get_jwt_content(context)
        user_email = user_data["user_email"]
        loaders = context.loaders
        organization: Organization | None = await loaders.organization.load(organization_identifier)
        if not organization:
            raise OrganizationNotFound()
        organization_id = organization.id
        role, has_access = await collect(
            [
                authz.get_organization_level_role(loaders, user_email, organization_id),
                orgs_access.has_access(loaders, organization_id, user_email),
            ],
        )

        if role != "admin" and not has_access:
            logs_utils.cloudwatch_log(
                context,
                "Attempted to access the organization without permission",
                extra={
                    "user_email": user_email,
                    "organization_identifier": organization_identifier,
                    "log_type": "Security",
                },
            )
            raise StakeholderNotInOrganization()
        return await _func(*args, **kwargs)

    return cast(TVar, verify_and_call)


async def _resolve_group_name_args(context: Any, args: Any) -> str | None:
    group_name_extractor = {
        Vulnerability: lambda x: x.group_name,
        Group: lambda x: x.name,
        dict: lambda x: x.get("name") or x.get("project_name"),
    }
    if not args or not args[0]:
        return None
    if hasattr(args[0], "group_name"):
        return args[0].group_name
    if "finding_id" in args[0]:
        return await _resolve_from_finding_id(context=context, identifier=args[0]["finding_id"])

    return group_name_extractor.get(type(args[0]), lambda x: None)(args[0])


async def _resolve_group_from_vuln_id(context: Any, kwargs: Any) -> str | None:
    vuln_id_names = (
        "vuln_id",
        "vulnerability_id",
        "vuln_uuid",
        "vulnerability_uuid",
    )
    for name in vuln_id_names:
        if name in kwargs:
            return await _resolve_from_vuln_id(context, kwargs[name])

    return None


async def _resolve_group_from_finding_id(context: Any, kwargs: Any) -> str | None:
    finding_id_names = ("finding_id", "draft_id")
    for name in finding_id_names:
        if name in kwargs:
            return await _resolve_from_finding_id(context=context, identifier=kwargs[name])

    return None


async def _resolve_group_name_kwargs(context: Any, kwargs: Any) -> str | None:
    if "group_name" in kwargs:
        return kwargs["group_name"]

    if solved_finding_id := await _resolve_group_from_finding_id(context, kwargs):
        return solved_finding_id

    if solved_vuln_id := await _resolve_group_from_vuln_id(context, kwargs):
        return solved_vuln_id
    return ""


async def resolve_group_name(
    context: Any,
    args: Any,
    kwargs: Any,
) -> str:
    """Get group name based on args passed."""
    name = await _resolve_group_name_args(context=context, args=args)
    if not name:
        name = await _resolve_group_name_kwargs(context=context, kwargs=kwargs)

    if isinstance(name, str):
        name = name.lower()
    return str(name)


def retry_on_exceptions(
    *,
    exceptions: tuple[type[Exception], ...],
    max_attempts: int = 5,
    sleep_seconds: float = 0,
) -> Callable[[Callable[P, TVar]], Callable[P, TVar]]:
    def decorator(func: Callable[P, TVar]) -> Callable:
        if asyncio.iscoroutinefunction(func):

            @functools.wraps(func)
            async def wrapper(*args: P.args, **kwargs: P.kwargs) -> TVar:
                for _ in range(max_attempts - 1):
                    with contextlib.suppress(*exceptions):
                        return await func(*args, **kwargs)

                    await sleep(sleep_seconds)
                return await func(*args, **kwargs)

        else:

            @functools.wraps(func)
            def wrapper(*args: P.args, **kwargs: P.kwargs) -> TVar:
                for _ in range(max_attempts - 1):
                    with contextlib.suppress(*exceptions):
                        return func(*args, **kwargs)

                    time.sleep(sleep_seconds)
                return func(*args, **kwargs)

        return wrapper

    return decorator


def turn_args_into_kwargs(func: TVar) -> TVar:
    """
    Turn function's positional-arguments into keyword-arguments.

    Very useful when you want to keep an strongly typed signature in your
    function while using another decorators from this module that work on
    keyword arguments only for backwards compatibility reasons.

    This avoids functions with a typeless **parameters, and then 50 lines
    unpacking the arguments and casting them to the expected types.
    """
    _func = cast(Callable[..., Any], func)

    @functools.wraps(_func)
    async def newfunc(*args: Any, **kwargs: Any) -> Any:
        # The first two arguments are django's self, and info references
        #   They can be safely left intact
        args_as_kwargs = dict(zip(inspect.getfullargspec(_func).args[2:], args[2:], strict=False))
        return await _func(*args[0:2], **args_as_kwargs, **kwargs)

    return cast(TVar, newfunc)


def validate_connection(func: TVar) -> TVar:
    """Decorator to verify the connections"""
    _func = cast(Callable[..., Any], func)

    @functools.wraps(_func)
    async def verify_and_call(*args: Any, **kwargs: Any) -> Any:
        first = kwargs.get("first")
        if first is not None and first < 0:
            raise InvalidPositiveArgument(arg="first")
        return await _func(*args, **kwargs)

    return cast(TVar, verify_and_call)
