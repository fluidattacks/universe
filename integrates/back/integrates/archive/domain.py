import logging
import logging.config
from collections.abc import Iterable

from aioextensions import in_thread
from snowflake.connector.cursor import SnowflakeCursor

from integrates.class_types.types import Item
from integrates.settings import LOGGING

from . import events as events_ops
from . import findings as findings_ops
from . import groups as groups_ops
from . import organizations as orgs_ops
from . import roots as roots_ops
from . import toe_inputs as toe_inputs_ops
from . import toe_lines as toe_lines_ops
from . import vulnerabilities as vulns_ops

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)


async def archive_event(cursor: SnowflakeCursor, items: Iterable[Item]) -> None:
    metadata_item = next(
        item
        for item in items
        if item["pk"].startswith("EVENT#") and item["sk"].startswith("GROUP#")
    )
    await in_thread(
        events_ops.insert_bulk_metadata,
        cursor=cursor,
        items=(metadata_item,),
    )
    LOGGER.info(
        "Event archived",
        extra={
            "group_name": metadata_item["sk"].split("#")[1],
            "event_id": metadata_item["pk"].split("#")[1],
        },
    )


async def archive_events_metadata(cursor: SnowflakeCursor, items: Iterable[Item]) -> None:
    _items = tuple(
        item
        for item in items
        if item["pk"].startswith("EVENT#") and item["sk"].startswith("GROUP#")
    )
    if not _items:
        return

    await in_thread(
        events_ops.insert_bulk_metadata,
        cursor=cursor,
        items=_items,
    )
    LOGGER.info(
        "Events metadata archived",
        extra={"extra": {"len": len(_items)}},
    )


async def archive_finding(cursor: SnowflakeCursor, items: Iterable[Item]) -> None:
    metadata_item = next(
        item for item in items if item["pk"].startswith("FIN#") and item["sk"].startswith("GROUP#")
    )
    state: Item = metadata_item["state"]
    if state["status"] == "DELETED":
        return

    await in_thread(
        findings_ops.insert_finding,
        cursor=cursor,
        item=metadata_item,
    )
    state_items = tuple(item for item in items if item["sk"].startswith("STATE#"))
    if state_items:
        await in_thread(
            findings_ops.insert_historic_state,
            cursor=cursor,
            historic_state=state_items,
        )

    verification_items = tuple(item for item in items if item["sk"].startswith("VERIFICATION#"))
    if verification_items:
        findings_ops.insert_historic_verification(
            cursor=cursor,
            historic_verification=verification_items,
        )
        findings_ops.insert_historic_verification_vuln_ids(
            cursor=cursor,
            historic_verification=verification_items,
        )

    LOGGER.info(
        "Finding archived",
        extra={
            "extra": {
                "finding_id": metadata_item["pk"].split("#")[1],
                "group_name": metadata_item["sk"].split("#")[1],
            },
        },
    )


async def archive_findings_metadata(cursor: SnowflakeCursor, items: Iterable[Item]) -> None:
    _items = tuple(
        item for item in items if item["pk"].startswith("FIN#") and item["sk"].startswith("GROUP#")
    )
    if not _items:
        return

    await in_thread(
        findings_ops.insert_bulk_metadata,
        cursor=cursor,
        items=_items,
    )
    LOGGER.info(
        "Findings metadata archived",
        extra={"extra": {"len": len(_items)}},
    )


async def archive_findings_state(cursor: SnowflakeCursor, items: Iterable[Item]) -> None:
    _items = tuple(item for item in items if item["sk"].startswith("STATE#"))
    if not _items:
        return

    await in_thread(
        findings_ops.insert_historic_state,
        cursor=cursor,
        historic_state=_items,
    )
    LOGGER.info(
        "Findings state archived",
        extra={"extra": {"len": len(_items)}},
    )


async def archive_findings_verification(cursor: SnowflakeCursor, items: Iterable[Item]) -> None:
    _items = tuple(item for item in items if item["sk"].startswith("VERIFICATION#"))
    if not _items:
        return

    await in_thread(
        findings_ops.insert_historic_verification,
        cursor=cursor,
        historic_verification=_items,
    )
    await in_thread(
        findings_ops.insert_historic_verification_vuln_ids,
        cursor=cursor,
        historic_verification=_items,
    )
    LOGGER.info(
        "Findings verification archived",
        extra={"extra": {"len": len(_items)}},
    )


async def archive_group(cursor: SnowflakeCursor, items: Iterable[Item]) -> None:
    metadata_item = next(
        (
            item
            for item in items
            if item["pk"].startswith("GROUP#") and item["sk"].startswith("ORG#")
        ),
        None,
    )
    if metadata_item:
        await in_thread(
            groups_ops.insert_group,
            cursor=cursor,
            item=metadata_item,
        )

    state_items = tuple(item for item in items if item["sk"].startswith("STATE#"))
    if state_items:
        await in_thread(
            groups_ops.insert_historic_state,
            cursor=cursor,
            historic_state=state_items,
        )

    unreliable_indicators_item = next(
        (
            item
            for item in items
            if item["sk"].startswith("GROUP#") and item["sk"].endswith("#UNRELIABLEINDICATORS")
        ),
        None,
    )
    if unreliable_indicators_item:
        await in_thread(
            groups_ops.insert_code_languages,
            cursor=cursor,
            unreliable_indicators=unreliable_indicators_item,
        )

    if metadata_item:
        LOGGER.info(
            "Group archived",
            extra={
                "extra": {
                    "group_name": metadata_item["pk"].split("#")[1],
                    "organization_id": metadata_item["sk"].split("#")[1],
                },
            },
        )


async def archive_organization(cursor: SnowflakeCursor, items: Iterable[Item]) -> None:
    metadata_item = next(
        (item for item in items if item["pk"].startswith("ORG#") and item["sk"].startswith("ORG#")),
        None,
    )
    if metadata_item:
        await in_thread(
            orgs_ops.insert_organization,
            cursor=cursor,
            item=metadata_item,
        )
    state_items = tuple(item for item in items if item["sk"].startswith("STATE#"))
    if state_items:
        await in_thread(
            orgs_ops.insert_historic_state,
            cursor=cursor,
            historic_state=state_items,
        )

    if metadata_item:
        LOGGER.info(
            "Organization archived",
            extra={
                "extra": {
                    "organization_id": metadata_item["pk"].split("#")[1],
                    "organization_name": metadata_item["sk"].split("#")[1],
                },
            },
        )


async def archive_root(cursor: SnowflakeCursor, items: Iterable[Item]) -> None:
    metadata_item = next(
        (
            item
            for item in items
            if item["pk"].startswith("ROOT#")
            and not item["pk"].startswith("ROOT#URL#")
            and item["sk"].startswith("GROUP#")
        ),
        None,
    )
    if not metadata_item:
        return

    await in_thread(
        roots_ops.insert_root,
        cursor=cursor,
        item=metadata_item,
    )

    LOGGER.info(
        "Root archived",
        extra={
            "extra": {
                "group_name": metadata_item["sk"].split("#")[1],
                "root_id": metadata_item["pk"].split("#")[1],
            },
        },
    )


async def archive_roots_metadata(cursor: SnowflakeCursor, items: Iterable[Item]) -> None:
    _items = tuple(
        item
        for item in items
        if item["pk"].startswith("ROOT#")
        and not item["pk"].startswith("ROOT#URL#")
        and item["sk"].startswith("GROUP#")
    )
    if not _items:
        return

    await in_thread(
        roots_ops.insert_bulk_metadata,
        cursor=cursor,
        items=_items,
    )
    LOGGER.info(
        "Roots metadata archived",
        extra={"extra": {"len": len(_items)}},
    )


async def archive_group_toe_inputs(cursor: SnowflakeCursor, items: Iterable[Item]) -> None:
    metadata_items = [
        item
        for item in items
        if item["pk"].startswith("GROUP#") and item["sk"].startswith("INPUTS#")
    ]
    if not metadata_items:
        return

    await in_thread(
        toe_inputs_ops.insert_bulk_metadata,
        cursor=cursor,
        items=metadata_items,
    )
    LOGGER.info(
        "Group ToE inputs archived",
        extra={
            "extra": {
                "group_name": metadata_items[0]["pk"].split("#")[1],
                "len": len(metadata_items),
            },
        },
    )


async def archive_group_toe_lines(cursor: SnowflakeCursor, items: Iterable[Item]) -> None:
    metadata_items = [
        item
        for item in items
        if item["pk"].startswith("GROUP#") and item["sk"].startswith("LINES#")
    ]
    if not metadata_items:
        return

    await in_thread(
        toe_lines_ops.insert_bulk_metadata,
        cursor=cursor,
        items=metadata_items,
    )
    LOGGER.info(
        "Group ToE lines archived",
        extra={
            "extra": {
                "group_name": metadata_items[0]["pk"].split("#")[1],
                "len": len(metadata_items),
            },
        },
    )


async def archive_vulnerabilities_metadata(cursor: SnowflakeCursor, items: Iterable[Item]) -> None:
    _items = tuple(
        item for item in items if item["pk"].startswith("VULN#") and item["sk"].startswith("FIN#")
    )
    if not _items:
        return

    await in_thread(
        vulns_ops.insert_bulk_metadata,
        cursor=cursor,
        items=_items,
    )
    LOGGER.info(
        "Vulnerabilities metadata archived",
        extra={"extra": {"len": len(_items)}},
    )


async def archive_vulnerabilities_state(
    cursor: SnowflakeCursor,
    items: Iterable[Item],
) -> None:
    _items = tuple(item for item in items if item["sk"].startswith("STATE#"))
    if not _items:
        return

    await in_thread(
        vulns_ops.insert_historic_state,
        cursor=cursor,
        historic_state=_items,
    )
    LOGGER.info(
        "Vulnerabilities state archived",
        extra={"extra": {"len": len(_items)}},
    )


async def archive_vulnerabilities_treatment(
    cursor: SnowflakeCursor,
    items: Iterable[Item],
) -> None:
    _items = tuple(item for item in items if item["sk"].startswith("TREATMENT#"))
    if not _items:
        return

    await in_thread(
        vulns_ops.insert_historic_treatment,
        cursor=cursor,
        historic_treatment=_items,
    )
    LOGGER.info(
        "Vulnerabilities treatment archived",
        extra={"extra": {"len": len(_items)}},
    )


async def archive_vulnerabilities_verification(
    cursor: SnowflakeCursor,
    items: Iterable[Item],
) -> None:
    _items = tuple(item for item in items if item["sk"].startswith("VERIFICATION#"))
    if not _items:
        return

    await in_thread(
        vulns_ops.insert_historic_verification,
        cursor=cursor,
        historic_verification=_items,
    )
    LOGGER.info(
        "Vulnerabilities verification archived",
        extra={"extra": {"len": len(_items)}},
    )


async def archive_vulnerabilities_zero_risk(
    cursor: SnowflakeCursor,
    items: Iterable[Item],
) -> None:
    _items = tuple(item for item in items if item["sk"].startswith("ZERORISK#"))
    if not _items:
        return

    await in_thread(
        vulns_ops.insert_historic_zero_risk,
        cursor=cursor,
        historic_zero_risk=_items,
    )
    LOGGER.info(
        "Vulnerabilities zero_risk archived",
        extra={"extra": {"len": len(_items)}},
    )


async def archive_vulnerability(cursor: SnowflakeCursor, items: Iterable[Item]) -> None:
    metadata_item = [
        item for item in items if item["pk"].startswith("VULN#") and item["sk"].startswith("FIN#")
    ]
    if metadata_item:
        state: Item = metadata_item[0]["state"]
        if state["status"] == "DELETED":
            return

        await in_thread(
            vulns_ops.insert_vulnerability,
            cursor=cursor,
            item=metadata_item[0],
        )
    state_items = tuple(item for item in items if item["sk"].startswith("STATE#"))
    if state_items:
        await in_thread(
            vulns_ops.insert_historic_state,
            cursor=cursor,
            historic_state=state_items,
        )

    treatment_items = tuple(item for item in items if item["sk"].startswith("TREATMENT#"))
    if treatment_items:
        vulns_ops.insert_historic_treatment(
            cursor=cursor,
            historic_treatment=treatment_items,
        )

    verification_items = tuple(item for item in items if item["sk"].startswith("VERIFICATION#"))
    if verification_items:
        vulns_ops.insert_historic_verification(
            cursor=cursor,
            historic_verification=verification_items,
        )

    zero_risk_items = tuple(item for item in items if item["sk"].startswith("ZERORISK#"))
    if zero_risk_items:
        vulns_ops.insert_historic_zero_risk(
            cursor=cursor,
            historic_zero_risk=zero_risk_items,
        )

    if metadata_item:
        LOGGER.info(
            "Vulnerability archived",
            extra={
                "extra": {
                    "finding_id": metadata_item[0]["sk"].split("#")[1],
                    "vulnerability_id": metadata_item[0]["pk"].split("#")[1],
                },
            },
        )
