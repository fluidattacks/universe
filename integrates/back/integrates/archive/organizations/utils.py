from datetime import datetime

from integrates.archive.utils import mask_email
from integrates.class_types.types import Item

ORGANIZATION_ID_PREFIX = "ORG#"


def _remove_org_id_prefix(organization_id: str) -> str:
    return organization_id.lstrip(ORGANIZATION_ID_PREFIX)


def format_row_metadata(
    item: Item,
) -> Item:
    return {
        "id": _remove_org_id_prefix(item["id"]),
        "country": item.get("country"),
        "created_by": mask_email(item.get("created_by", "")),
        "created_date": datetime.fromisoformat(item["created_date"])
        if item.get("created_date")
        else None,
        "name": item["name"],
    }


def format_row_state(state: Item) -> Item:
    return {
        "id": _remove_org_id_prefix(state["pk"]),
        "modified_by": mask_email(state["modified_by"]),
        "modified_date": datetime.fromisoformat(state["modified_date"]),
        "pending_deletion_date": datetime.fromisoformat(state["pending_deletion_date"])
        if state.get("pending_deletion_date")
        else None,
        "status": state["status"],
    }
