from snowflake.connector.cursor import SnowflakeCursor

from integrates.archive.operations import execute_snowflake
from integrates.archive.queries import SQL_MERGE_INTO_HISTORIC, SQL_MERGE_INTO_METADATA
from integrates.archive.utils import (
    format_merge_query,
    format_query_values_and_template,
    get_query_fields,
)
from integrates.class_types.types import Item

from .initialize import METADATA_TABLE, STATE_TABLE
from .types import MetadataTableRow, StateTableRow
from .utils import format_row_metadata, format_row_state


def insert_historic_state(
    *,
    cursor: SnowflakeCursor,
    historic_state: tuple[Item, ...],
) -> None:
    sql_fields = get_query_fields(StateTableRow)
    formatted_rows = [format_row_state(item) for item in historic_state]
    sql_values, sql_values_template = format_query_values_and_template(formatted_rows)
    sql_query = format_merge_query(
        sql_template=SQL_MERGE_INTO_HISTORIC,
        table_name=STATE_TABLE,
        sql_fields=sql_fields,
        sql_values_template=sql_values_template,
    )
    execute_snowflake(cursor, sql_query, sql_values)


def insert_metadata(
    *,
    cursor: SnowflakeCursor,
    item: Item,
) -> None:
    sql_fields = get_query_fields(MetadataTableRow)
    formatted_rows = [format_row_metadata(item)]
    sql_values, sql_values_template = format_query_values_and_template(formatted_rows)
    sql_query = format_merge_query(
        sql_template=SQL_MERGE_INTO_METADATA,
        table_name=METADATA_TABLE,
        sql_fields=sql_fields,
        sql_values_template=sql_values_template,
    )
    execute_snowflake(cursor, sql_query, sql_values)


def insert_organization(
    *,
    cursor: SnowflakeCursor,
    item: Item,
) -> None:
    insert_metadata(cursor=cursor, item=item)
    insert_historic_state(
        cursor=cursor,
        historic_state=(
            {
                "pk": item["pk"],
                "sk": f'STATE#{item["state"]["modified_date"]}',
                **item["state"],
            },
        ),
    )
