from snowflake.connector.cursor import SnowflakeCursor

from integrates.archive.client import snowflake_db_cursor
from integrates.archive.operations import execute_snowflake
from integrates.archive.utils import SCHEMA_NAME

METADATA_TABLE: str = "vulnerabilities_metadata"
STATE_TABLE: str = "vulnerabilities_state"
TREATMENT_TABLE: str = "vulnerabilities_treatment"
VERIFICATION_TABLE: str = "vulnerabilities_verification"
ZERO_RISK_TABLE: str = "vulnerabilities_zero_risk"


def _initialize_metadata_table(cursor: SnowflakeCursor) -> None:
    execute_snowflake(
        cursor,
        f"""
        CREATE TABLE IF NOT EXISTS {SCHEMA_NAME}.{METADATA_TABLE} (
            id STRING,
            custom_severity NUMBER,
            finding_id STRING NOT NULL,
            root_id STRING,
            severity_base_score NUMBER(3,1),
            severity_cvss_v3 STRING,
            severity_cvss_v4 STRING,
            severity_cvssf NUMBER(7,3),
            severity_temporal_score NUMBER(3,1),
            severity_threat_score NUMBER(3,1),
            skims_method STRING,
            technique STRING,
            type STRING NOT NULL,
            where_str STRING,

            UNIQUE (id),
            PRIMARY KEY (id)
        )
        """,
    )


def _initialize_state_table(cursor: SnowflakeCursor) -> None:
    execute_snowflake(
        cursor,
        f"""
        CREATE TABLE IF NOT EXISTS {SCHEMA_NAME}.{STATE_TABLE} (
            id STRING,
            modified_date TIMESTAMP_TZ NOT NULL,
            modified_by STRING NOT NULL,
            source STRING NOT NULL,
            status STRING NOT NULL,

            PRIMARY KEY (id, modified_date),
            FOREIGN KEY (id) REFERENCES {SCHEMA_NAME}.{METADATA_TABLE}(id)
        )
        """,
    )


def _initialize_treatment_table(cursor: SnowflakeCursor) -> None:
    execute_snowflake(
        cursor,
        f"""
        CREATE TABLE IF NOT EXISTS {SCHEMA_NAME}.{TREATMENT_TABLE} (
            id STRING,
            modified_date TIMESTAMP_TZ NOT NULL,
            accepted_until TIMESTAMP_TZ,
            acceptance_status STRING,
            status STRING NOT NULL,

            PRIMARY KEY (id, modified_date),
            FOREIGN KEY (id) REFERENCES {SCHEMA_NAME}.{METADATA_TABLE}(id)
        )
        """,
    )


def _initialize_verification_table(cursor: SnowflakeCursor) -> None:
    execute_snowflake(
        cursor,
        f"""
        CREATE TABLE IF NOT EXISTS {SCHEMA_NAME}.{VERIFICATION_TABLE} (
            id STRING,
            modified_date TIMESTAMP_TZ NOT NULL,
            status STRING NOT NULL,

            PRIMARY KEY (id, modified_date),
            FOREIGN KEY (id) REFERENCES {SCHEMA_NAME}.{METADATA_TABLE}(id)
        )
        """,
    )


def _initialize_zero_risk_table(cursor: SnowflakeCursor) -> None:
    execute_snowflake(
        cursor,
        f"""
        CREATE TABLE IF NOT EXISTS {SCHEMA_NAME}.{ZERO_RISK_TABLE} (
            id STRING,
            modified_date TIMESTAMP_TZ NOT NULL,
            status STRING NOT NULL,

            PRIMARY KEY (id, modified_date),
            FOREIGN KEY (id) REFERENCES {SCHEMA_NAME}.{METADATA_TABLE}(id)
        )
        """,
    )


def initialize_tables() -> None:
    with snowflake_db_cursor() as cursor:
        _initialize_metadata_table(cursor)
        _initialize_state_table(cursor)
        _initialize_treatment_table(cursor)
        _initialize_verification_table(cursor)
        _initialize_zero_risk_table(cursor)
