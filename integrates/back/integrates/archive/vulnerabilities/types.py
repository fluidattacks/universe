from dataclasses import dataclass
from datetime import datetime
from decimal import Decimal


@dataclass(frozen=True)
class MetadataTableRow:
    id: str
    custom_severity: int | None
    finding_id: str
    root_id: str | None
    severity_base_score: Decimal | None
    severity_cvss_v3: str | None
    severity_cvss_v4: str | None
    severity_cvssf: Decimal | None
    severity_temporal_score: Decimal | None
    severity_threat_score: Decimal | None
    skims_method: str | None
    technique: str | None
    type: str
    where_str: str | None


@dataclass(frozen=True)
class StateTableRow:
    id: str
    modified_by: str
    modified_date: datetime
    source: str
    status: str


@dataclass(frozen=True)
class TreatmentTableRow:
    id: str
    modified_date: datetime
    status: str
    accepted_until: datetime | None
    acceptance_status: str | None


@dataclass(frozen=True)
class VerificationTableRow:
    id: str
    modified_date: datetime
    status: str


@dataclass(frozen=True)
class ZeroRiskTableRow:
    id: str
    modified_date: datetime
    status: str
