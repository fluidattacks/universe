from snowflake.connector.cursor import SnowflakeCursor

from integrates.archive.operations import execute_snowflake
from integrates.archive.queries import SQL_MERGE_INTO_HISTORIC, SQL_MERGE_INTO_METADATA
from integrates.archive.utils import (
    format_merge_query,
    format_query_values_and_template,
    get_query_fields,
)
from integrates.class_types.types import Item

from .initialize import (
    METADATA_TABLE,
    STATE_TABLE,
    TREATMENT_TABLE,
    VERIFICATION_TABLE,
    ZERO_RISK_TABLE,
)
from .types import (
    MetadataTableRow,
    StateTableRow,
    TreatmentTableRow,
    VerificationTableRow,
    ZeroRiskTableRow,
)
from .utils import (
    format_row_metadata,
    format_row_state,
    format_row_treatment,
    format_row_verification,
    format_row_zero_risk,
)


def insert_bulk_metadata(
    *,
    cursor: SnowflakeCursor,
    items: tuple[Item, ...],
) -> None:
    sql_fields = get_query_fields(MetadataTableRow)
    formatted_rows = [format_row_metadata(item) for item in items]
    sql_values, sql_values_template = format_query_values_and_template(formatted_rows)
    sql_query = format_merge_query(
        sql_template=SQL_MERGE_INTO_METADATA,
        table_name=METADATA_TABLE,
        sql_fields=sql_fields,
        sql_values_template=sql_values_template,
    )
    execute_snowflake(cursor, sql_query, sql_values)


def insert_metadata(
    *,
    cursor: SnowflakeCursor,
    item: Item,
) -> None:
    sql_fields = get_query_fields(MetadataTableRow)
    formatted_rows = [format_row_metadata(item)]
    sql_values, sql_values_template = format_query_values_and_template(formatted_rows)
    sql_query = format_merge_query(
        sql_template=SQL_MERGE_INTO_METADATA,
        table_name=METADATA_TABLE,
        sql_fields=sql_fields,
        sql_values_template=sql_values_template,
    )
    execute_snowflake(cursor, sql_query, sql_values)


def insert_historic_state(
    *,
    cursor: SnowflakeCursor,
    historic_state: tuple[Item, ...],
) -> None:
    sql_fields = get_query_fields(StateTableRow)
    formatted_rows = [format_row_state(item) for item in historic_state]
    sql_values, sql_values_template = format_query_values_and_template(formatted_rows)
    sql_query = format_merge_query(
        sql_template=SQL_MERGE_INTO_HISTORIC,
        table_name=STATE_TABLE,
        sql_fields=sql_fields,
        sql_values_template=sql_values_template,
    )
    execute_snowflake(cursor, sql_query, sql_values)


def insert_historic_treatment(
    *,
    cursor: SnowflakeCursor,
    historic_treatment: tuple[Item, ...],
) -> None:
    sql_fields = get_query_fields(TreatmentTableRow)
    formatted_rows = [format_row_treatment(item) for item in historic_treatment]
    sql_values, sql_values_template = format_query_values_and_template(formatted_rows)
    sql_query = format_merge_query(
        sql_template=SQL_MERGE_INTO_HISTORIC,
        table_name=TREATMENT_TABLE,
        sql_fields=sql_fields,
        sql_values_template=sql_values_template,
    )
    execute_snowflake(cursor, sql_query, sql_values)


def insert_historic_verification(
    *,
    cursor: SnowflakeCursor,
    historic_verification: tuple[Item, ...],
) -> None:
    sql_fields = get_query_fields(VerificationTableRow)
    formatted_rows = [format_row_verification(item) for item in historic_verification]
    sql_values, sql_values_template = format_query_values_and_template(formatted_rows)
    sql_query = format_merge_query(
        sql_template=SQL_MERGE_INTO_HISTORIC,
        table_name=VERIFICATION_TABLE,
        sql_fields=sql_fields,
        sql_values_template=sql_values_template,
    )
    execute_snowflake(cursor, sql_query, sql_values)


def insert_historic_zero_risk(
    *,
    cursor: SnowflakeCursor,
    historic_zero_risk: tuple[Item, ...],
) -> None:
    sql_fields = get_query_fields(ZeroRiskTableRow)
    formatted_rows = [format_row_zero_risk(item) for item in historic_zero_risk]
    sql_values, sql_values_template = format_query_values_and_template(formatted_rows)
    sql_query = format_merge_query(
        sql_template=SQL_MERGE_INTO_HISTORIC,
        table_name=ZERO_RISK_TABLE,
        sql_fields=sql_fields,
        sql_values_template=sql_values_template,
    )
    execute_snowflake(cursor, sql_query, sql_values)


def insert_vulnerability(
    *,
    cursor: SnowflakeCursor,
    item: Item,
) -> None:
    insert_metadata(cursor=cursor, item=item)
    if "state" in item:
        insert_historic_state(
            cursor=cursor,
            historic_state=(
                {
                    "pk": item["pk"],
                    "sk": f'STATE#{item["state"]["modified_date"]}',
                    **item["state"],
                },
            ),
        )
    if "treatment" in item:
        insert_historic_treatment(
            cursor=cursor,
            historic_treatment=(
                {
                    "pk": item["pk"],
                    "sk": f'TREATMENT#{item["treatment"]["modified_date"]}',
                    **item["treatment"],
                },
            ),
        )
    if "verification" in item:
        insert_historic_verification(
            cursor=cursor,
            historic_verification=(
                {
                    "pk": item["pk"],
                    "sk": (f'VERIFICATION#{item["verification"]["modified_date"]}'),
                    **item["verification"],
                },
            ),
        )
    if "zero_risk" in item:
        insert_historic_zero_risk(
            cursor=cursor,
            historic_zero_risk=(
                {
                    "pk": item["pk"],
                    "sk": f'ZERORISK#{item["zero_risk"]["modified_date"]}',
                    **item["zero_risk"],
                },
            ),
        )
