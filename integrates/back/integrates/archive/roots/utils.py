from datetime import datetime

from integrates.archive.utils import mask_by_encoding
from integrates.class_types.types import Item


def format_row_code_languages(
    item: Item,
) -> list[Item]:
    if not item.get("unreliable_indicators"):
        return []

    unreliable_indicators = item["unreliable_indicators"]
    unreliable_code_languages = unreliable_indicators.get("unreliable_code_languages", [])
    root_id = item["pk"].split("#")[1]

    return [
        {
            "id": mask_by_encoding(root_id + language_item["language"]),
            "language": language_item["language"],
            "loc": int(language_item["loc"]),
            "root_id": root_id,
        }
        for language_item in unreliable_code_languages
    ]


def format_row_metadata(
    item: Item,
) -> Item:
    root_id = item["pk"].split("#")[1]
    created_date = datetime.fromisoformat(item["created_date"]) if "created_date" in item else None
    organization_name = item["pk_2"].split("#")[1] if "pk_2" in item else None
    item_type = str(item["type"]).upper() if "type" in item else None

    return {
        "id": root_id,
        "created_date": created_date,
        "group_name": mask_by_encoding(item["sk"].split("#")[1]),
        "organization_name": organization_name,
        "type": item_type,
    }
