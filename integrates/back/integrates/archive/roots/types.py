from dataclasses import dataclass


@dataclass(frozen=True)
class CodeLanguagesTableRow:
    id: str
    language: str
    loc: int
    root_id: str


@dataclass(frozen=True)
class MetadataTableRow:
    id: str
    created_date: str
    group_name: str
    organization_name: str
    type: str
