from datetime import datetime

from integrates.archive.utils import mask_by_encoding
from integrates.class_types.types import Item


def format_row_metadata(
    item: Item,
) -> Item:
    state_item: Item = {**item, **item.get("state", {})}

    return {
        "id": mask_by_encoding(item["sk"]),
        "attacked_at": datetime.fromisoformat(state_item["attacked_at"])
        if state_item.get("attacked_at")
        else None,
        "attacked_by": state_item["attacked_by"],
        "attacked_lines": int(state_item["attacked_lines"]),
        "be_present": bool(state_item["be_present"]),
        "be_present_until": datetime.fromisoformat(state_item["be_present_until"])
        if state_item.get("be_present_until")
        else None,
        "first_attack_at": datetime.fromisoformat(state_item["first_attack_at"])
        if state_item.get("first_attack_at")
        else None,
        "group_name": mask_by_encoding(item["group_name"]),
        "has_vulnerabilities": bool(state_item.get("has_vulnerabilities", False)),
        "loc": int(state_item["loc"]),
        "modified_date": datetime.fromisoformat(state_item["modified_date"]),
        "root_id": item["root_id"],
        "seen_at": datetime.fromisoformat(state_item["seen_at"]),
        "seen_first_time_by": item.get("seen_first_time_by"),
        "sorts_risk_level": int(state_item["sorts_risk_level"]),
    }
