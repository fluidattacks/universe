import re
from datetime import datetime

from integrates.archive.utils import mask_by_encoding
from integrates.class_types.types import Item


def format_row_metadata(
    item: Item,
) -> Item:
    state_item: Item = {**item, **item.get("state", {})}
    root_id: str = item.get("root_id", state_item.get("unreliable_root_id", ""))
    if not root_id:
        root_id_search = re.search(r"INPUTS#ROOT#(.*?)#COMPONENT", item["sk"])
        root_id = root_id_search.group(1) if root_id_search else ""

    return {
        "id": mask_by_encoding(item["sk"]),
        "attacked_at": datetime.fromisoformat(state_item["attacked_at"])
        if state_item.get("attacked_at")
        else None,
        "attacked_by": state_item.get("attacked_by"),
        "be_present": bool(state_item.get("be_present")),
        "be_present_until": datetime.fromisoformat(state_item["be_present_until"])
        if state_item.get("be_present_until")
        else None,
        "first_attack_at": datetime.fromisoformat(state_item["first_attack_at"])
        if state_item.get("first_attack_at")
        else None,
        "group_name": mask_by_encoding(item["group_name"]),
        "has_vulnerabilities": bool(state_item.get("has_vulnerabilities")),
        "root_id": root_id,
        "seen_at": datetime.fromisoformat(state_item["seen_at"])
        if state_item.get("seen_at")
        else None,
        "seen_first_time_by": state_item["seen_first_time_by"],
    }
