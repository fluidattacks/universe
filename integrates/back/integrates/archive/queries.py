SQL_MERGE_INTO_METADATA = """
    MERGE INTO {schema}.{table} as a
    USING (
        SELECT * FROM (
            VALUES
            {values_template}
        ) virtual_table ({fields})
    ) source
    ON a.id = source.id
    WHEN NOT MATCHED THEN INSERT ({fields})
    VALUES ({source_fields});
"""

SQL_MERGE_INTO_HISTORIC = """
    MERGE INTO {schema}.{table} as a
    USING (
        SELECT * FROM (
            VALUES
            {values_template}
        ) virtual_table ({fields})
    ) source
    ON a.id = source.id
        AND a.modified_date = source.modified_date
    WHEN NOT MATCHED THEN INSERT ({fields})
    VALUES ({source_fields});
"""


SQL_MERGE_INTO_VERIFICATION_VULNS_IDS = """
    MERGE INTO {schema}.{table} as a
    USING (
        SELECT * FROM (
            VALUES
            {values_template}
        ) virtual_table ({fields})
    ) source
    ON a.id = source.id
        AND a.modified_date = source.modified_date
        AND a.vulnerability_id = source.vulnerability_id
    WHEN NOT MATCHED THEN INSERT ({fields})
    VALUES ({source_fields});
"""
