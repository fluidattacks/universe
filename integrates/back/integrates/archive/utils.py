import contextlib
import functools
import hashlib
import time
from collections.abc import Callable
from dataclasses import fields
from typing import Any, TypeVar, cast

from integrates.class_types.types import Item

T = TypeVar("T")


FLUID_IDENTIFIER: str = "@fluidattacks.com"
SCHEMA_NAME: str = "integrates"


def format_indexed_row(index: int, row: Item) -> Item:
    return {f"{k}_{index}": v for k, v in row.items()}


def format_indexed_rows(values: list[Item]) -> list[Item]:
    return [format_indexed_row(index, item) for index, item in enumerate(values)]


def format_query_values_and_template(values: list[Item]) -> tuple[Item, str]:
    formatted_indexed = format_indexed_rows(values)

    sql_values_dict = {k: v for item in formatted_indexed for k, v in item.items()}

    raw_placeholders = [[f"%({k})s" for k in row.keys()] for row in formatted_indexed]
    raw_template_list = [f'({",".join(row)})' for row in raw_placeholders]
    sql_values_template = ",\n".join(raw_template_list)

    return sql_values_dict, sql_values_template


def format_merge_query(
    *,
    sql_template: str,
    table_name: str,
    sql_fields: list[str],
    sql_values_template: str,
) -> str:
    fields_str = ", ".join(sql_fields)
    source_fields_str = ", ".join([f"source.{field}" for field in sql_fields])

    return sql_template.format(
        schema=SCHEMA_NAME,
        table=table_name,
        fields=fields_str,
        values_template=sql_values_template,
        source_fields=source_fields_str,
    )


def get_query_fields(table_row_class: Any) -> list[str]:
    return list(f.name for f in fields(table_row_class))


def mask_email(email: str) -> str | None:
    return email if email and email.endswith(FLUID_IDENTIFIER) else ""


def mask_by_encoding(raw: str) -> str:
    return hashlib.shake_256(raw.encode("utf-8")).hexdigest(10)


def retry_on_exceptions(
    *,
    exceptions: tuple[type[Exception], ...],
    max_attempts: int = 5,
    sleep_seconds: float = 0,
) -> Callable[[T], T]:
    def decorator(func: T) -> T:
        _func = cast(Callable[..., Any], func)

        @functools.wraps(_func)
        def wrapper(*args: Any, **kwargs: Any) -> Any:
            for _ in range(max_attempts - 1):
                with contextlib.suppress(*exceptions):
                    return _func(*args, **kwargs)

                time.sleep(sleep_seconds)

            return _func(*args, **kwargs)

        return cast(T, wrapper)

    return decorator
