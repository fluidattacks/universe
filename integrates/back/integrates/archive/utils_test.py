from integrates.archive.queries import SQL_MERGE_INTO_METADATA
from integrates.archive.utils import (
    format_indexed_rows,
    format_merge_query,
    format_query_values_and_template,
)
from integrates.class_types.types import Item
from integrates.testing.utils import parametrize


def normalize_sql(sql: str) -> str:
    return " ".join(sql.split())


@parametrize(
    args=["values", "expected"],
    cases=[
        [
            [{"field1": "value1", "field2": 1}],
            [{"field1_0": "value1", "field2_0": 1}],
        ],
        [
            [
                {"field1": "value1", "field2": 1},
                {"field1": "value2", "field2": 2},
                {"field1": "value3", "field2": 3},
            ],
            [
                {"field1_0": "value1", "field2_0": 1},
                {"field1_1": "value2", "field2_1": 2},
                {"field1_2": "value3", "field2_2": 3},
            ],
        ],
    ],
)
def test_format_indexed_rows(values: list[Item], expected: list[Item]) -> None:
    result = format_indexed_rows(values)
    assert result == expected


@parametrize(
    args=["values", "expected"],
    cases=[
        [
            [{"field1": "value1", "field2": 1}],
            ({"field1_0": "value1", "field2_0": 1}, "(%(field1_0)s,%(field2_0)s)"),
        ],
        [
            [
                {"field1": "value1", "field2": 1},
                {"field1": "value2", "field2": 2},
                {"field1": "value3", "field2": 3},
            ],
            (
                {
                    "field1_0": "value1",
                    "field2_0": 1,
                    "field1_1": "value2",
                    "field2_1": 2,
                    "field1_2": "value3",
                    "field2_2": 3,
                },
                (
                    "(%(field1_0)s,%(field2_0)s),\n"
                    "(%(field1_1)s,%(field2_1)s),\n"
                    "(%(field1_2)s,%(field2_2)s)"
                ),
            ),
        ],
    ],
)
def test_format_query_values_and_template(values: list[Item], expected: tuple[Item, str]) -> None:
    result = format_query_values_and_template(values)
    assert result == expected


def test_format_merge_query() -> None:
    sql_template = SQL_MERGE_INTO_METADATA
    table_name = "test_table"
    sql_fields = ["id", "name", "age"]
    sql_values_template = "(%(id)s,%(name)s,%(age)s)"
    expected_query = """
        MERGE INTO integrates.test_table as a
        USING (
            SELECT * FROM (
                VALUES
                (%(id)s,%(name)s,%(age)s)
            ) virtual_table (id, name, age)
        ) source
        ON a.id = source.id
        WHEN NOT MATCHED THEN INSERT (id, name, age)
        VALUES (source.id, source.name, source.age);
    """

    result = format_merge_query(
        sql_template=sql_template,
        table_name=table_name,
        sql_fields=sql_fields,
        sql_values_template=sql_values_template,
    )

    assert normalize_sql(result) == normalize_sql(expected_query)
