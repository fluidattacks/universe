from snowflake.connector.cursor import SnowflakeCursor

from integrates.archive.client import snowflake_db_cursor
from integrates.archive.utils import SCHEMA_NAME

METADATA_TABLE: str = "findings_metadata"
STATE_TABLE: str = "findings_state"
VERIFICATION_TABLE: str = "findings_verification"
VERIFICATION_VULN_IDS_TABLE: str = "findings_verification_vuln_ids"


def _initialize_metadata_table(cursor: SnowflakeCursor) -> None:
    cursor.execute(
        f"""
        CREATE TABLE IF NOT EXISTS {SCHEMA_NAME}.{METADATA_TABLE} (
            id STRING,
            cvss_version STRING,
            group_name STRING NOT NULL,
            hacker_email STRING NOT NULL,
            requirements STRING NOT NULL,
            severity_base_score NUMBER(3,1),
            severity_cvss_v3 STRING,
            severity_cvss_v4 STRING,
            severity_cvssf NUMBER(7,3),
            severity_temporal_score NUMBER(3,1),
            severity_threat_score NUMBER(3,1),
            sorts STRING NOT NULL,
            title STRING NOT NULL,

            UNIQUE (id),
            PRIMARY KEY (id)
        )
        """,
    )


def _initialize_state_table(cursor: SnowflakeCursor) -> None:
    cursor.execute(
        f"""
        CREATE TABLE IF NOT EXISTS {SCHEMA_NAME}.{STATE_TABLE} (
            id STRING,
            modified_date TIMESTAMP_TZ NOT NULL,
            modified_by STRING NOT NULL,
            justification STRING NOT NULL,
            source STRING NOT NULL,
            status STRING NOT NULL,

            PRIMARY KEY (id, modified_date),
            FOREIGN KEY (id) REFERENCES {SCHEMA_NAME}.{METADATA_TABLE}(id)
        )
        """,
    )


def _initialize_verification_table(cursor: SnowflakeCursor) -> None:
    cursor.execute(
        f"""
        CREATE TABLE IF NOT EXISTS {SCHEMA_NAME}.{VERIFICATION_TABLE} (
            id STRING,
            modified_date TIMESTAMP_TZ NOT NULL,
            status STRING NOT NULL,

            PRIMARY KEY (id, modified_date),
            FOREIGN KEY (id) REFERENCES {SCHEMA_NAME}.{METADATA_TABLE}(id)
        )
        """,
    )


def _initialize_verification_vuln_ids_table(
    cursor: SnowflakeCursor,
) -> None:
    cursor.execute(
        f"""
        CREATE TABLE IF NOT EXISTS {SCHEMA_NAME}.{VERIFICATION_VULN_IDS_TABLE}
        (
            id STRING,
            modified_date TIMESTAMP_TZ NOT NULL,
            vulnerability_id STRING NOT NULL,

            PRIMARY KEY (id, modified_date, vulnerability_id),
            FOREIGN KEY (id) REFERENCES {SCHEMA_NAME}.{METADATA_TABLE}(id)
        )
        """,
    )


def initialize_tables() -> None:
    with snowflake_db_cursor() as cursor:
        _initialize_metadata_table(cursor)
        _initialize_state_table(cursor)
        _initialize_verification_table(cursor)
        _initialize_verification_vuln_ids_table(cursor)
