from dataclasses import dataclass
from datetime import datetime


@dataclass(frozen=True)
class CodeLanguagesTableRow:
    id: str
    group_name: str
    language: str
    loc: int


@dataclass(frozen=True)
class MetadataTableRow:
    id: str
    created_by: str
    created_date: datetime
    language: str
    name: str
    organization_id: str
    sprint_duration: int | None
    sprint_start_date: datetime | None


@dataclass(frozen=True)
class StateTableRow:
    id: str
    comments: str | None
    has_essential: bool
    has_advanced: bool
    justification: str | None
    managed: str
    modified_by: str
    modified_date: datetime
    pending_deletion_date: datetime | None
    service: str | None
    status: str
    tier: str
    type: str
