from datetime import datetime

from integrates.archive.utils import mask_by_encoding, mask_email
from integrates.class_types.types import Item


def format_row_code_languages(
    unreliable_indicators: Item,
) -> list[Item]:
    group_name = str(unreliable_indicators["pk"]).split("#")[1]
    unreliable_code_languages = unreliable_indicators.get("code_languages", [])

    return [
        {
            "id": mask_by_encoding(group_name + language_item["language"]),
            "group_name": mask_by_encoding(group_name),
            "language": language_item["language"],
            "loc": int(language_item["loc"]),
        }
        for language_item in unreliable_code_languages
    ]


def format_row_metadata(
    item: Item,
) -> Item:
    return {
        "id": mask_by_encoding(item["name"]),
        "created_by": mask_email(item.get("created_by", "")),
        "created_date": datetime.fromisoformat(item["created_date"])
        if item.get("created_date")
        else None,
        "language": item["language"],
        "name": mask_by_encoding(item["name"]),
        "organization_id": item["organization_id"],
        "sprint_duration": int(item["sprint_duration"]) if item.get("sprint_duration") else None,
        "sprint_start_date": datetime.fromisoformat(item["sprint_start_date"])
        if item.get("sprint_start_date")
        else None,
    }


def _format_state_managed(managed: bool | str) -> str:
    if not managed:
        return "NOT_MANAGED"
    if managed is True:
        return "MANAGED"
    return str(managed)


def format_row_state(state: Item) -> Item:
    return {
        "id": mask_by_encoding(str(state["pk"]).split("#")[1]),
        "comments": state.get("comments"),
        "has_essential": bool(state.get("has_essential")),
        "has_advanced": bool(state.get("has_advanced")),
        "justification": state.get("justification"),
        "managed": _format_state_managed(state.get("managed", "")),
        "modified_by": mask_email(state["modified_by"]),
        "modified_date": datetime.fromisoformat(state["modified_date"]),
        "pending_deletion_date": datetime.fromisoformat(state["pending_deletion_date"])
        if state.get("pending_deletion_date")
        else None,
        "service": state.get("service"),
        "status": state["status"],
        "tier": state["tier"] if state.get("tier") else "OTHER",
        "type": state["type"],
    }
