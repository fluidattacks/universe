from snowflake.connector.cursor import SnowflakeCursor

from integrates.archive.client import snowflake_db_cursor
from integrates.archive.operations import execute_snowflake
from integrates.archive.utils import SCHEMA_NAME

CODE_LANGUAGES_TABLE = "groups_code_languages"
METADATA_TABLE = "groups_metadata"
STATE_TABLE = "groups_state"


def _initialize_code_languages(
    cursor: SnowflakeCursor,
) -> None:
    execute_snowflake(
        cursor,
        f"""
        CREATE TABLE IF NOT EXISTS {SCHEMA_NAME}.{CODE_LANGUAGES_TABLE} (
            id STRING,
            group_name STRING,
            language STRING,
            loc NUMBER,

            UNIQUE (id),
            PRIMARY KEY (id)
        )
        """,
    )


def _initialize_metadata_table(cursor: SnowflakeCursor) -> None:
    execute_snowflake(
        cursor,
        f"""
        CREATE TABLE IF NOT EXISTS {SCHEMA_NAME}.{METADATA_TABLE} (
            id STRING,
            created_by STRING,
            created_date TIMESTAMP_TZ,
            language STRING,
            name STRING,
            organization_id STRING,
            sprint_duration NUMBER,
            sprint_start_date TIMESTAMP_TZ,

            UNIQUE (id),
            PRIMARY KEY (id)
        )
        """,
    )


def _initialize_state_table(cursor: SnowflakeCursor) -> None:
    execute_snowflake(
        cursor,
        f"""
        CREATE TABLE IF NOT EXISTS {SCHEMA_NAME}.{STATE_TABLE} (
            id STRING,
            comments STRING,
            has_essential BOOLEAN,
            has_advanced BOOLEAN,
            justification STRING,
            managed STRING,
            modified_by STRING,
            modified_date TIMESTAMP_TZ,
            pending_deletion_date TIMESTAMP_TZ,
            service STRING,
            status STRING,
            tier STRING,
            type STRING,

            PRIMARY KEY (id, modified_date),
            FOREIGN KEY (id)
                REFERENCES {SCHEMA_NAME}.{METADATA_TABLE}(id)
        )
        """,
    )


def initialize_tables() -> None:
    with snowflake_db_cursor() as cursor:
        _initialize_metadata_table(cursor)
        _initialize_code_languages(cursor)
        _initialize_state_table(cursor)
