import asyncio
import json as json_lib
from types import SimpleNamespace
from typing import Any


def create_fake_client_response(  # type: ignore[misc]
    status: str,
    json_data: dict[str, Any],
) -> SimpleNamespace:
    """Creates a fake client response object for a JSON object."""
    json_dumps = json_lib.dumps(json_data)  # type: ignore[misc]
    default_headers = {
        "Content-Type": "application/json; charset=utf-8",
        "Content-Length": str(len(json_dumps)),
    }

    def text() -> str:
        return json_dumps

    async def json() -> dict[str, list[str]]:
        await asyncio.sleep(0)
        return json_data  # type: ignore[misc]

    return SimpleNamespace(
        status=status,
        headers=default_headers,
        content_type=default_headers["Content-Type"],
        text=text,
        json=json,
        ok=int(status) < 400,
    )
