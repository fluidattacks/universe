import sys
from re import Pattern
from typing import TypeVar

import pytest
from _pytest.python_api import RaisesContext

E = TypeVar("E", bound=BaseException)


def raises(
    expected_exc: type[E] | tuple[type[E], ...],
    match: str | Pattern[str] | None = None,
) -> RaisesContext[E]:
    """
    Wrapper for pytest.raises. It handles exceptions in tests, e.g.:


    ```python
    with raises(ValueError):
        function_that_raises_value_error()
    ```
    """
    return pytest.raises(expected_exc, match=match)


def get_file_abs_path(filename: str) -> str:
    """Returns the file's absolute path in the `./test_data/<function_name>` directory."""
    root_file = sys._getframe(1).f_code.co_filename
    function_name = sys._getframe(1).f_code.co_name
    module_path = "/".join(root_file.split("/")[:-1])

    return f"{module_path}/test_data/{function_name}/{filename}"
