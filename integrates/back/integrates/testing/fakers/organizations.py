from datetime import (
    datetime,
)

from integrates.db_model.organizations.enums import (
    OrganizationStateStatus,
)
from integrates.db_model.organizations.types import (
    DocumentFile,
    Organization,
    OrganizationBilling,
    OrganizationDocuments,
    OrganizationPaymentMethods,
    OrganizationPriorityPolicy,
    OrganizationState,
)
from integrates.db_model.types import (
    Policies,
)
from integrates.testing.fakers.constants import (
    DATE_2019,
    DATE_2025,
    EMAIL_GENERIC,
)
from integrates.testing.fakers.randoms import (
    random_uuid,
)


def DocumentFileFaker(  # noqa: N802
    *,
    filename: str = "test.pdf",
    modified_date: datetime = DATE_2025,
) -> DocumentFile:
    return DocumentFile(
        file_name=filename,
        modified_date=modified_date,
    )


def OrganizationDocumentsFaker(  # noqa: N802
    *,
    rut: DocumentFile | None = DocumentFileFaker(),
    tax_id: DocumentFile | None = DocumentFileFaker(),
) -> OrganizationDocuments:
    return OrganizationDocuments(
        rut=rut,
        tax_id=tax_id,
    )


def OrganizationPaymentMethodsFaker(  # noqa: N802
    *,
    id: str = random_uuid(),  # noqa: A002
    business_name: str = "Test Business",
    city: str = "Medellin",
    country: str = "Colombia",
    state: str = "Antioquia",
    email: str = EMAIL_GENERIC,
    documents: OrganizationDocuments = OrganizationDocumentsFaker(),
) -> OrganizationPaymentMethods:
    return OrganizationPaymentMethods(
        id=id,
        business_name=business_name,
        email=email,
        country=country,
        state=state,
        city=city,
        documents=documents,
    )


def OrganizationBillingFaker(  # noqa: N802
    *,
    billing_email: str = EMAIL_GENERIC,
    customer_id: str = "123456",
    last_modified_by: str = EMAIL_GENERIC,
    modified_at: datetime = DATE_2019,
) -> OrganizationBilling:
    return OrganizationBilling(
        billing_email=billing_email,
        customer_id=customer_id,
        last_modified_by=last_modified_by,
        modified_at=modified_at,
    )


def OrganizationStateFaker(  # noqa: N802
    *,
    aws_external_id: str = "123456789012",
    status: OrganizationStateStatus = OrganizationStateStatus.ACTIVE,
    modified_by: str = EMAIL_GENERIC,
    modified_date: datetime = DATE_2019,
    pending_deletion_date: datetime | None = None,
) -> OrganizationState:
    return OrganizationState(
        aws_external_id=aws_external_id,
        status=status,
        modified_by=modified_by,
        modified_date=modified_date,
        pending_deletion_date=pending_deletion_date,
    )


def OrganizationFaker(  # noqa: N802
    *,
    created_by: str = EMAIL_GENERIC,
    created_date: datetime | None = DATE_2019,
    id: str = random_uuid(),  # noqa: A002
    name: str = "test-org",
    payment_methods: list[OrganizationPaymentMethods] | None = None,
    policies: Policies = Policies(
        modified_by=EMAIL_GENERIC,
        modified_date=DATE_2019,
    ),
    priority_policies: list[OrganizationPriorityPolicy] | None = None,
    state: OrganizationState = OrganizationStateFaker(),
    country: str = "Colombia",
    vulnerabilities_pathfile: str | None = None,
    billing_information: list[OrganizationBilling] | None = None,
) -> Organization:
    return Organization(
        created_by=created_by,
        created_date=created_date,
        id=id,
        name=name,
        payment_methods=payment_methods,
        policies=policies,
        priority_policies=priority_policies,
        state=state,
        country=country,
        vulnerabilities_pathfile=vulnerabilities_pathfile,
        billing_information=billing_information,
    )
