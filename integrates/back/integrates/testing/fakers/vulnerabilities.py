from datetime import (
    datetime,
)
from decimal import (
    Decimal,
)

from integrates.db_model.enums import (
    Source,
    TreatmentStatus,
)
from integrates.db_model.types import (
    SeverityScore,
    Treatment,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilitySeverityProposalStatus,
    VulnerabilityStateReason,
    VulnerabilityStateStatus,
    VulnerabilityTechnique,
    VulnerabilityType,
    VulnerabilityVerificationStatus,
    VulnerabilityZeroRiskStatus,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityAdvisory,
    VulnerabilityAuthor,
    VulnerabilitySeverityProposal,
    VulnerabilityState,
    VulnerabilityTool,
    VulnerabilityUnreliableIndicators,
    VulnerabilityVerification,
    VulnerabilityZeroRisk,
)
from integrates.testing.fakers.constants import (
    DATE_2019,
    EMAIL_FLUIDATTACKS_HACKER,
    EMAIL_FLUIDATTACKS_UNKNOWN,
    EMAIL_GENERIC,
)
from integrates.testing.fakers.randoms import random_uuid


def VulnerabilityAdvisoryFaker(  # noqa: N802
    *,
    cve: list[str] = [
        "CVE-2021-44228",
        "CVE-2021-44832",
        "CVE-2021-45046",
        "CVE-2021-45105",
    ],
    package: str = "org.apache.logging.log4j:log4j-core",
    vulnerable_version: str = "2.13.2",
    epss: int | None = 50,
) -> VulnerabilityAdvisory:
    return VulnerabilityAdvisory(
        cve=cve,
        package=package,
        vulnerable_version=vulnerable_version,
        epss=epss,
    )


def SeverityScoreFaker(  # noqa: N802
    base_score: Decimal = Decimal("5.3"),
    temporal_score: Decimal = Decimal("4.8"),
    cvss_v3: str = "CVSS:3.1/AV:L/AC:H/PR:L/UI:N/S:C/C:L/I:L/A:L"
    "/E:P/RL:O/CR:L/AR:H/MAV:N/MAC:H/MPR:H/MUI:R/MS:U/MC:L"
    "/MA:L",
    cvss_v4: str = "CVSS:4.0/AV:L/AC:H/AT:N/PR:L/UI:N/VC:L/VI:L/"
    "VA:L/SC:L/SI:L/SA:L/MAV:N/MAC:H/MPR:H/MUI:A/MVC:L/"
    "MVA:L/MSC:N/MSI:N/MSA:N/CR:L/AR:H/E:P",
    cvssf_v4: Decimal = Decimal("0.014"),
    threat_score: Decimal = Decimal("0.9"),
    cvssf: Decimal = Decimal("3.031"),
) -> SeverityScore:
    return SeverityScore(
        base_score=base_score,
        temporal_score=temporal_score,
        cvss_v3=cvss_v3,
        cvssf=cvssf,
        cvssf_v4=cvssf_v4,
        threat_score=threat_score,
        cvss_v4=cvss_v4,
    )


def VulnerabilitySeverityProposalFaker(  # noqa: N802
    severity_score: SeverityScore = SeverityScoreFaker(),
    modified_by: str = EMAIL_FLUIDATTACKS_HACKER,
    modified_date: datetime = DATE_2019,
    status: VulnerabilitySeverityProposalStatus = VulnerabilitySeverityProposalStatus.REQUESTED,
    justification: str | None = None,
) -> VulnerabilitySeverityProposal:
    return VulnerabilitySeverityProposal(
        severity_score=severity_score,
        modified_by=modified_by,
        modified_date=modified_date,
        status=status,
        justification=justification,
    )


def VulnerabilityStateFaker(  # noqa: N802
    *,
    modified_by: str = EMAIL_GENERIC,
    modified_date: datetime = DATE_2019,
    source: Source = Source.ASM,
    specific: str = "99",
    status: VulnerabilityStateStatus = VulnerabilityStateStatus.VULNERABLE,
    where: str = "package.json",
    commit: str | None = None,
    advisories: VulnerabilityAdvisory | None = None,
    reasons: list[VulnerabilityStateReason] | None = None,
    other_reason: str | None = None,
    tool: VulnerabilityTool | None = None,
    proposed_severity: VulnerabilitySeverityProposal | None = None,
) -> VulnerabilityState:
    return VulnerabilityState(
        modified_by=modified_by,
        modified_date=modified_date,
        source=source,
        specific=specific,
        status=status,
        where=where,
        commit=commit,
        advisories=advisories,
        reasons=reasons,
        other_reason=other_reason,
        tool=tool,
        proposed_severity=proposed_severity,
    )


def VulnerabilityVerificationFaker(  # noqa: N802
    *,
    modified_by: str = EMAIL_GENERIC,
    modified_date: datetime = DATE_2019,
    status: VulnerabilityVerificationStatus = VulnerabilityVerificationStatus.REQUESTED,
    event_id: str | None = None,
    justification: str | None = None,
) -> VulnerabilityVerification:
    return VulnerabilityVerification(
        modified_by=modified_by,
        modified_date=modified_date,
        status=status,
        event_id=event_id,
        justification=justification,
    )


def VulnerabilityZeroRiskFaker(  # noqa: N802
    *,
    comment_id: str = "123456",
    modified_by: str = EMAIL_FLUIDATTACKS_UNKNOWN,
    modified_date: datetime = DATE_2019,
    status: VulnerabilityZeroRiskStatus = VulnerabilityZeroRiskStatus.CONFIRMED,
) -> VulnerabilityZeroRisk:
    return VulnerabilityZeroRisk(
        comment_id=comment_id,
        modified_by=modified_by,
        modified_date=modified_date,
        status=status,
    )


def VulnerabilityFaker(  # noqa: N802
    *,
    author: VulnerabilityAuthor | None = VulnerabilityAuthor(
        author_email=EMAIL_GENERIC,
        commit="d2a38fd760ebb5e5bc4ed73571c909f8a5b1b389",
        commit_date=DATE_2019,
    ),
    created_by: str = EMAIL_GENERIC,
    created_date: datetime = DATE_2019,
    finding_id: str = random_uuid(),
    group_name: str = "test-group",
    hacker_email: str = EMAIL_FLUIDATTACKS_HACKER,
    id: str = random_uuid(),  # noqa: A002
    organization_name: str = "test-org",
    root_id: str = random_uuid(),
    severity_score: SeverityScore | None = SeverityScoreFaker(),
    state: VulnerabilityState = VulnerabilityStateFaker(),
    stream: list[str] | None = None,
    type: VulnerabilityType = (  # noqa: A002
        VulnerabilityType.LINES
    ),
    tags: list[str] | None = ["tag1", "tag2"],
    technique: VulnerabilityTechnique | None = VulnerabilityTechnique.PTAAS,
    treatment: Treatment | None = Treatment(
        modified_date=DATE_2019,
        status=TreatmentStatus.UNTREATED,
        assigned="developer@entry.com",
    ),
    unreliable_indicators: VulnerabilityUnreliableIndicators = (
        VulnerabilityUnreliableIndicators(
            unreliable_last_reattack_requester="test1@gmail.com",
            unreliable_last_requested_reattack_date=DATE_2019,
            unreliable_reattack_cycles=1,
            unreliable_report_date=DATE_2019,
            unreliable_treatment_changes=1,
        )
    ),
    verification: VulnerabilityVerification | None = None,
    zero_risk: VulnerabilityZeroRisk | None = None,
    vuln_machine_hash: int | None = None,
    vuln_skims_description: str | None = None,
    vuln_skims_method: str | None = None,
    cwe_ids: list[str] | None = None,
) -> Vulnerability:
    return Vulnerability(
        author=author,
        created_by=created_by,
        created_date=created_date,
        cwe_ids=cwe_ids,
        finding_id=finding_id,
        group_name=group_name,
        hacker_email=hacker_email,
        id=id,
        organization_name=organization_name,
        root_id=root_id,
        severity_score=severity_score,
        state=state,
        technique=technique,
        type=type,
        tags=tags,
        treatment=treatment,
        unreliable_indicators=unreliable_indicators,
        verification=verification,
        zero_risk=zero_risk,
        hash=vuln_machine_hash,
        skims_description=vuln_skims_description,
        skims_method=vuln_skims_method,
        stream=stream,
    )
