from datetime import (
    datetime,
)

from integrates.db_model.events.enums import (
    EventSolutionReason,
    EventStateStatus,
    EventType,
)
from integrates.db_model.events.types import (
    Event,
    EventEvidence,
    EventEvidences,
    EventState,
)
from integrates.testing.fakers.constants import (
    DATE_2019,
    EMAIL_FLUIDATTACKS_HACKER,
)
from integrates.testing.fakers.randoms import (
    random_uuid,
)


def EventEvidencesFaker(  # noqa: N802
    *,
    file_1: EventEvidence | None = EventEvidence(
        file_name="file_1.csv",
        modified_date=DATE_2019,
    ),
    image_1: EventEvidence | None = EventEvidence(
        file_name="image_1.png",
        modified_date=DATE_2019,
    ),
    image_2: EventEvidence | None = EventEvidence(
        file_name="image_2.png",
        modified_date=DATE_2019,
    ),
    image_3: EventEvidence | None = EventEvidence(
        file_name="image_3.png",
        modified_date=DATE_2019,
    ),
    image_4: EventEvidence | None = EventEvidence(
        file_name="image_4.png",
        modified_date=DATE_2019,
    ),
    image_5: EventEvidence | None = EventEvidence(
        file_name="image_5.png",
        modified_date=DATE_2019,
    ),
    image_6: EventEvidence | None = EventEvidence(
        file_name="image_6.png",
        modified_date=DATE_2019,
    ),
) -> EventEvidences:
    return EventEvidences(
        file_1=file_1,
        image_1=image_1,
        image_2=image_2,
        image_3=image_3,
        image_4=image_4,
        image_5=image_5,
        image_6=image_6,
    )


def EventStateFaker(  # noqa: N802
    *,
    modified_by: str = EMAIL_FLUIDATTACKS_HACKER,
    modified_date: datetime = DATE_2019,
    status: EventStateStatus = EventStateStatus.OPEN,
    comment_id: str | None = None,
    other: str | None = None,
    reason: EventSolutionReason | None = None,
) -> EventState:
    return EventState(
        modified_by=modified_by,
        modified_date=modified_date,
        status=status,
        comment_id=comment_id,
        other=other,
        reason=reason,
    )


def EventFaker(  # noqa: N802
    *,
    client: str = "test-org",
    created_by: str = EMAIL_FLUIDATTACKS_HACKER,
    created_date: datetime = DATE_2019,
    description: str = "test-event",
    event_date: datetime = DATE_2019,
    evidences: EventEvidences = EventEvidencesFaker(),
    group_name: str = "test-group",
    hacker: str = EMAIL_FLUIDATTACKS_HACKER,
    id: str = random_uuid(),  # noqa: A002
    state: EventState = EventStateFaker(),
    type: EventType = (  # noqa: A002
        EventType.ENVIRONMENT_ISSUES
    ),
    environment_url: str | None = "https://test-environment-url.com",
    root_id: str = random_uuid(),
    n_holds: int | None = None,
) -> Event:
    return Event(
        client=client,
        created_by=created_by,
        created_date=created_date,
        description=description,
        event_date=event_date,
        evidences=evidences,
        group_name=group_name,
        hacker=hacker,
        id=id,
        state=state,
        type=type,
        environment_url=environment_url,
        root_id=root_id,
        n_holds=n_holds,
    )
