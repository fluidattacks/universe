from datetime import (
    datetime,
)

from integrates.db_model.trials.enums import (
    TrialReason,
)
from integrates.db_model.trials.types import (
    Trial,
)
from integrates.testing.fakers.constants import (
    DATE_2024,
    DATE_2025,
    EMAIL_GENERIC,
)


def TrialFaker(  # noqa: N802
    *,
    email: str = EMAIL_GENERIC,
    completed: bool = False,
    extension_date: datetime | None = DATE_2025,
    extension_days: int = 0,
    start_date: datetime | None = DATE_2024,
    reason: TrialReason | None = TrialReason.DEVELOP_SECURE,
) -> Trial:
    return Trial(
        email=email,
        completed=completed,
        extension_date=extension_date,
        extension_days=extension_days,
        start_date=start_date,
        reason=reason,
    )
