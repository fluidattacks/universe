from datetime import datetime
from decimal import Decimal

from integrates.db_model.forces.types import (
    ExecutionVulnerabilities,
    ExploitResult,
    ForcesExecution,
)
from integrates.testing.fakers.constants import DATE_2019
from integrates.testing.fakers.randoms import random_uuid


def ExecutionVulnerabilitiesFaker(  # noqa: N802
    num_of_accepted_vulnerabilities: int = 4,
    num_of_open_vulnerabilities: int = 20,
    num_of_closed_vulnerabilities: int = 10,
    num_of_open_managed_vulnerabilities: int = 5,
    num_of_open_unmanaged_vulnerabilities: int = 3,
    open: list[ExploitResult] | None = None,  # noqa: A002
    closed: list[ExploitResult] | None = None,
    accepted: list[ExploitResult] | None = None,
    num_of_vulns_in_exploits: int | None = None,
    num_of_vulns_in_integrates_exploits: int | None = None,
    num_of_vulns_in_accepted_exploits: int | None = None,
) -> ExecutionVulnerabilities:
    return ExecutionVulnerabilities(
        num_of_accepted_vulnerabilities=num_of_accepted_vulnerabilities,
        num_of_open_vulnerabilities=num_of_open_vulnerabilities,
        num_of_closed_vulnerabilities=num_of_closed_vulnerabilities,
        num_of_open_managed_vulnerabilities=num_of_open_managed_vulnerabilities,
        num_of_open_unmanaged_vulnerabilities=num_of_open_unmanaged_vulnerabilities,
        open=open,
        closed=closed,
        accepted=accepted,
        num_of_vulns_in_exploits=num_of_vulns_in_exploits,
        num_of_vulns_in_integrates_exploits=num_of_vulns_in_integrates_exploits,
        num_of_vulns_in_accepted_exploits=num_of_vulns_in_accepted_exploits,
    )


def ForcesExecutionFaker(  # noqa: N802
    *,
    id: str = random_uuid(),  # noqa: A002
    group_name: str = "test-group",
    execution_date: datetime = DATE_2019,
    commit: str = "5a721992",
    repo: str = "repository1",
    branch: str = "master",
    kind: str = "dynamic",
    exit_code: str = "1",
    strictness: str = "lax",
    origin: str = "https://test3.com",
    vulnerabilities: ExecutionVulnerabilities = ExecutionVulnerabilitiesFaker(),
    grace_period: int | None = 0,
    severity_threshold: Decimal | None = Decimal("0.0"),
    days_until_it_breaks: int | None = None,
) -> ForcesExecution:
    return ForcesExecution(
        id=id,
        group_name=group_name,
        execution_date=execution_date,
        commit=commit,
        repo=repo,
        branch=branch,
        kind=kind,
        exit_code=exit_code,
        strictness=strictness,
        origin=origin,
        vulnerabilities=vulnerabilities,
        grace_period=grace_period,
        severity_threshold=severity_threshold,
        days_until_it_breaks=days_until_it_breaks,
    )
