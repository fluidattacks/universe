from datetime import (
    datetime,
)
from typing import (
    Literal,
)

from integrates.db_model.roots.enums import (
    RootCloningStatus,
    RootCriticality,
    RootEnvironmentCloud,
    RootEnvironmentUrlStateStatus,
    RootEnvironmentUrlType,
    RootMachineStatus,
    RootStateReason,
    RootStatus,
    RootType,
)
from integrates.db_model.roots.types import (
    GitRoot,
    GitRootCloning,
    GitRootMachine,
    GitRootState,
    IPRoot,
    IPRootState,
    RootEnvironmentUrl,
    RootEnvironmentUrlState,
    RootUnreliableIndicators,
    Secret,
    SecretState,
    URLRoot,
    URLRootState,
)
from integrates.roots.domain import format_environment_id
from integrates.testing.fakers.constants import (
    DATE_2019,
    EMAIL_FLUIDATTACKS_HACKER,
    EMAIL_FLUIDATTACKS_MACHINE,
    EMAIL_GENERIC,
)
from integrates.testing.fakers.randoms import (
    random_uuid,
)


def GitRootMachineFaker(  # noqa: N802
    *,
    modified_by: str = EMAIL_FLUIDATTACKS_MACHINE,
    modified_date: datetime = DATE_2019,
    reason: str = "Cloned successfully",
    status: RootMachineStatus = RootMachineStatus.SUCCESS,
    commit: str = "00000",
) -> GitRootMachine:
    return GitRootMachine(
        modified_by=modified_by,
        modified_date=modified_date,
        reason=reason,
        status=status,
        commit=commit,
    )


def GitRootCloningFaker(  # noqa: N802
    *,
    modified_by: str = EMAIL_FLUIDATTACKS_HACKER,
    modified_date: datetime = DATE_2019,
    reason: str = "Cloned successfully",
    status: RootCloningStatus = RootCloningStatus.FAILED,
    commit: str | None = None,
    failed_count: int = 0,
    first_failed_cloning: datetime | None = None,
    first_successful_cloning: datetime | None = None,
) -> GitRootCloning:
    return GitRootCloning(
        failed_count=failed_count,
        first_failed_cloning=first_failed_cloning,
        first_successful_cloning=first_successful_cloning,
        modified_by=modified_by,
        modified_date=modified_date,
        reason=reason,
        status=status,
        commit=commit,
    )


def GitRootStateFaker(  # noqa: N802
    *,
    branch: str = "master",
    credential_id: str | None = None,
    criticality: RootCriticality = RootCriticality.LOW,
    includes_health_check: bool = False,
    modified_by: str = EMAIL_FLUIDATTACKS_HACKER,
    modified_date: datetime = DATE_2019,
    nickname: str = "test-root",
    status: RootStatus = RootStatus.ACTIVE,
    url: str = "https://gitlab.com/fluidattacks/nickname1",
    gitignore: list[str] = [],
    use_egress: bool = False,
    use_vpn: bool = False,
    use_ztna: bool = False,
) -> GitRootState:
    return GitRootState(
        branch=branch,
        credential_id=credential_id,
        criticality=criticality,
        includes_health_check=includes_health_check,
        modified_by=modified_by,
        modified_date=modified_date,
        nickname=nickname,
        status=status,
        url=url,
        gitignore=gitignore,
        use_egress=use_egress,
        use_vpn=use_vpn,
        use_ztna=use_ztna,
    )


def GitRootFaker(  # noqa: N802
    *,
    cloning: GitRootCloning = GitRootCloningFaker(),
    created_by: str = EMAIL_FLUIDATTACKS_HACKER,
    created_date: datetime = DATE_2019,
    group_name: str = "test-group",
    id: str = random_uuid(),  # noqa: A002
    organization_name: str = "test-org",
    state: GitRootState = GitRootStateFaker(),
    type: Literal[RootType.GIT] = (  # noqa: A002
        RootType.GIT
    ),
    unreliable_indicators: RootUnreliableIndicators = (RootUnreliableIndicators()),
    machine: GitRootMachine | None = None,
) -> GitRoot:
    return GitRoot(
        cloning=cloning,
        created_by=created_by,
        created_date=created_date,
        group_name=group_name,
        id=id,
        organization_name=organization_name,
        state=state,
        type=type,
        unreliable_indicators=unreliable_indicators,
        machine=machine,
    )


def UrlRootStateFaker(  # noqa: N802
    *,
    host: str = "app.fluidattacks.com",
    modified_by: str = EMAIL_FLUIDATTACKS_HACKER,
    modified_date: datetime = DATE_2019,
    nickname: str = "test-root",
    path: str = "",
    port: str = "443",
    protocol: str = "https",
    status: RootStatus = RootStatus.ACTIVE,
    excluded_sub_paths: list[str] | None = None,
    other: str | None = None,
    query: str | None = None,
    reason: RootStateReason | None = None,
) -> URLRootState:
    return URLRootState(
        host=host,
        modified_by=modified_by,
        modified_date=modified_date,
        nickname=nickname,
        path=path,
        port=port,
        protocol=protocol,
        status=status,
        excluded_sub_paths=excluded_sub_paths,
        other=other,
        query=query,
        reason=reason,
    )


def UrlRootFaker(  # noqa: N802
    *,
    created_by: str = EMAIL_FLUIDATTACKS_HACKER,
    created_date: datetime = DATE_2019,
    group_name: str = "test-group",
    root_id: str = random_uuid(),
    organization_name: str = "test-org",
    state: URLRootState = UrlRootStateFaker(),
    unreliable_indicators: RootUnreliableIndicators = (RootUnreliableIndicators()),
) -> URLRoot:
    return URLRoot(
        created_by=created_by,
        created_date=created_date,
        group_name=group_name,
        id=root_id,
        organization_name=organization_name,
        state=state,
        type=RootType.URL,
        unreliable_indicators=unreliable_indicators,
    )


def RootEnvironmentUrlStateFaker(  # noqa: N802
    *,
    modified_by: str = EMAIL_GENERIC,
    modified_date: datetime = DATE_2019,
    status: RootEnvironmentUrlStateStatus = (RootEnvironmentUrlStateStatus.CREATED),
    url_type: RootEnvironmentUrlType = RootEnvironmentUrlType.URL,
    cloud_name: RootEnvironmentCloud | None = None,
    use_egress: bool = False,
    use_vpn: bool = False,
    use_ztna: bool = False,
    include: bool = True,
    is_production: bool | None = True,
) -> RootEnvironmentUrlState:
    return RootEnvironmentUrlState(
        modified_by=modified_by,
        modified_date=modified_date,
        status=status,
        url_type=url_type,
        cloud_name=cloud_name,
        use_egress=use_egress,
        use_vpn=use_vpn,
        use_ztna=use_ztna,
        include=include,
        is_production=is_production,
    )


def RootEnvironmentUrlFaker(  # noqa: N802
    *,
    url: str = "https://fluidattacks.com",
    id: str | None = None,  # noqa: A002
    root_id: str = random_uuid(),
    group_name: str = "test-group",
    state: RootEnvironmentUrlState = RootEnvironmentUrlStateFaker(),
) -> RootEnvironmentUrl:
    root_env_id = format_environment_id(url) if id is None else id
    return RootEnvironmentUrl(
        url=url,
        id=root_env_id,
        root_id=root_id,
        group_name=group_name,
        state=state,
    )


def IPRootStateFaker(  # noqa: N802
    *,
    address: str = "192.168.0.1",
    modified_by: str = EMAIL_FLUIDATTACKS_HACKER,
    modified_date: datetime = DATE_2019,
    nickname: str = "test-root",
    status: RootStatus = RootStatus.ACTIVE,
    reason: RootStateReason | None = None,
) -> IPRootState:
    return IPRootState(
        address=address,
        modified_by=modified_by,
        modified_date=modified_date,
        nickname=nickname,
        status=status,
        reason=reason,
    )


def IPRootFaker(  # noqa: N802
    *,
    created_by: str = EMAIL_FLUIDATTACKS_HACKER,
    created_date: datetime = DATE_2019,
    group_name: str = "test-group",
    id: str = random_uuid(),  # noqa: A002
    organization_name: str = "test-org",
    state: IPRootState = IPRootStateFaker(),
    type: Literal[RootType.IP] = (  # noqa: A002
        RootType.IP
    ),
    unreliable_indicators: RootUnreliableIndicators = RootUnreliableIndicators(),
) -> IPRoot:
    return IPRoot(
        created_by=created_by,
        created_date=created_date,
        group_name=group_name,
        id=id,
        organization_name=organization_name,
        state=state,
        type=type,
        unreliable_indicators=unreliable_indicators,
    )


def SecretStateFaker(  # noqa: N802
    *,
    owner: str = EMAIL_GENERIC,
    modified_by: str = EMAIL_GENERIC,
    modified_date: datetime = DATE_2019,
    description: str | None = None,
) -> SecretState:
    return SecretState(
        owner=owner,
        modified_by=modified_by,
        modified_date=modified_date,
        description=description,
    )


def SecretFaker(  # noqa: N802
    *,
    key: str = "key",
    value: str = "value",
    created_at: datetime = DATE_2019,
    state: SecretState = SecretStateFaker(),
) -> Secret:
    return Secret(
        key=key,
        value=value,
        created_at=created_at,
        state=state,
    )
