from datetime import (
    datetime,
)

from integrates.db_model.roots.enums import (
    RootDockerImageStateStatus,
)
from integrates.db_model.roots.types import (
    DockerImageHistory,
    RootDockerImage,
    RootDockerImageState,
)
from integrates.testing.fakers.constants import DATE_2019, EMAIL_FLUIDATTACKS_HACKER
from integrates.testing.fakers.randoms import (
    random_uuid,
)


def DockerImageHistoryFaker(  # noqa: N802
    *,
    created: datetime = DATE_2019,
    created_by: str = EMAIL_FLUIDATTACKS_HACKER,
    empty_layer: bool = False,
    comment: str | None = None,
) -> DockerImageHistory:
    return DockerImageHistory(
        created=created,
        created_by=created_by,
        empty_layer=empty_layer,
        comment=comment,
    )


def RootDockerImageStateFaker(  # noqa: N802
    *,
    modified_by: str = EMAIL_FLUIDATTACKS_HACKER,
    modified_date: datetime = DATE_2019,
    status: RootDockerImageStateStatus = RootDockerImageStateStatus.CREATED,
    history: list[DockerImageHistory] = [DockerImageHistoryFaker()],
    layers: list[str] = [
        "sha256:4d4d7e7a3c27bb3c7b93dfc12d80d7d67ed8eae7b5c31abec1f6bf63b4e81d47",
        "sha256:67c56b2c4b989d87e372bf53162cfe23cd79b9d79a572dc49d6f2a2423e4d136",
    ],
    credential_id: str | None = None,
    digest: str | None = "sha256:fe6a5f3f5555b3c6bfb5b6e0e7639db8b8c2272d0848f214d7340d92521bce2d",
    include: bool = True,
) -> RootDockerImageState:
    return RootDockerImageState(
        modified_by=modified_by,
        modified_date=modified_date,
        status=status,
        history=history,
        layers=layers,
        credential_id=credential_id,
        digest=digest,
        include=include,
    )


def RootDockerImageFaker(  # noqa: N802
    *,
    uri: str = "docker.io/test_image",
    root_id: str = random_uuid(),
    group_name: str = "test-group",
    state: RootDockerImageState = RootDockerImageStateFaker(),
    created_at: datetime = DATE_2019,
    created_by: str = EMAIL_FLUIDATTACKS_HACKER,
) -> RootDockerImage:
    return RootDockerImage(
        uri=uri,
        root_id=root_id,
        group_name=group_name,
        state=state,
        created_at=created_at,
        created_by=created_by,
    )
