from datetime import datetime, timedelta

from integrates.db_model.marketplace.enums import (
    AWSMarketplacePricingDimension,
    AWSMarketplaceSubscriptionStatus,
)
from integrates.db_model.marketplace.types import (
    AWSMarketplaceSubscription,
    AWSMarketplaceSubscriptionEntitlement,
    AWSMarketplaceSubscriptionState,
)
from integrates.testing.fakers.constants import (
    DATE_2024,
)


def AWSMarketplaceSubscriptionStateFaker(  # noqa: N802
    *,
    entitlements: list[AWSMarketplaceSubscriptionEntitlement] = [
        AWSMarketplaceSubscriptionEntitlement(
            dimension=AWSMarketplacePricingDimension.GROUPS,
            expiration_date=(datetime.now() + timedelta(days=365)),
            value=2,
        )
    ],
    has_free_trial: bool = True,
    modified_date: datetime = DATE_2024,
    status: AWSMarketplaceSubscriptionStatus = AWSMarketplaceSubscriptionStatus.ACTIVE,
    private_offer_id: str | None = None,
) -> AWSMarketplaceSubscriptionState:
    return AWSMarketplaceSubscriptionState(
        entitlements=entitlements,
        has_free_trial=has_free_trial,
        modified_date=modified_date,
        status=status,
        private_offer_id=private_offer_id,
    )


def AWSMarketplaceSubscriptionFaker(  # noqa: N802
    *,
    aws_customer_id: str = "xYwVpnKesFR",
    created_at: datetime = DATE_2024,
    state: AWSMarketplaceSubscriptionState = AWSMarketplaceSubscriptionStateFaker(),
    aws_account_id: str | None = None,
    user: str | None = None,
) -> AWSMarketplaceSubscription:
    return AWSMarketplaceSubscription(
        aws_customer_id=aws_customer_id,
        created_at=created_at,
        state=state,
        aws_account_id=aws_account_id,
        user=user,
    )
