from typing import (
    Any,
    NamedTuple,
)

from graphql.language.ast import (
    OperationDefinitionNode,
)
from graphql.pyutils.path import (
    Path,
)
from graphql.type.definition import (
    GraphQLResolveInfo,
)
from graphql.type.schema import (
    GraphQLSchema,
)

from integrates.class_types.types import Item
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.sessions.domain import (
    get_jwt_content,
)
from integrates.sessions.function import (
    get_id,
)


class MockedOperationContext(NamedTuple):
    name: str
    operation_type: str
    query: str
    variables: Item


class MockedInfoContext(NamedTuple):
    headers: dict[str, Any]  # type: ignore[misc]
    loaders: Dataloaders
    operation: MockedOperationContext
    store: dict[str, Any]  # type: ignore[misc]


def GraphQLResolveInfoFaker(  # type: ignore[misc] # noqa: N802
    user_email: str,
    first_name: str = "",
    last_name: str = "",
    decorators: list[list[Any]] = [],
) -> GraphQLResolveInfo:
    decos = {  # type: ignore[misc]
        get_id(*decorator): True  # type: ignore[misc]
        for decorator in decorators  # type: ignore[misc]
    }
    context = MockedInfoContext(  # type: ignore[misc]
        headers={},  # type: ignore[misc]
        loaders=get_new_context(),
        operation=MockedOperationContext(
            name="GraphQL Operation",
            operation_type="query",
            query="",
            variables={},  # type: ignore[misc]
        ),
        store={  # type: ignore[misc]
            **decos,
            get_id(get_jwt_content): {  # type: ignore[misc]
                "user_email": user_email,
                "first_name": first_name,
                "last_name": last_name,
            },
        },
    )

    return GraphQLResolveInfo(
        field_name="",
        field_nodes=[],
        schema=GraphQLSchema(),
        fragments={},
        operation=OperationDefinitionNode(),
        root_value=None,
        return_type=None,  # type: ignore[arg-type]
        parent_type=None,  # type: ignore[arg-type]
        path=Path(prev="", key="", typename=None),  # type: ignore[misc]
        variable_values={},  # type: ignore[misc]
        is_awaitable=lambda _: False,  # type: ignore[misc]
        context=context,  # type: ignore[misc]
    )
