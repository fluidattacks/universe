from datetime import (
    datetime,
)

from integrates.db_model.toe_lines.types import (
    SortsSuggestion,
    ToeLine,
    ToeLineState,
)
from integrates.testing.fakers.constants import (
    DATE_2019,
    EMAIL_FLUIDATTACKS_HACKER,
    EMAIL_GENERIC,
)
from integrates.testing.fakers.randoms import (
    random_uuid,
)


def ToeLinesStateFaker(  # noqa: N802
    *,
    attacked_at: datetime | None = None,
    attacked_by: str = EMAIL_FLUIDATTACKS_HACKER,
    attacked_lines: int = 0,
    be_present: bool = True,
    be_present_until: datetime | None = None,
    comments: str = "",
    first_attack_at: datetime | None = None,
    has_vulnerabilities: bool = False,
    last_author: str = EMAIL_GENERIC,
    last_commit: str = "0000000000000000000000000000000000000000",
    last_commit_date: datetime = DATE_2019,
    loc: int = 100,
    modified_by: str = EMAIL_FLUIDATTACKS_HACKER,
    modified_date: datetime = DATE_2019,
    seen_at: datetime = DATE_2019,
    sorts_risk_level: int = 0,
    sorts_priority_factor: int | None = None,
    sorts_risk_level_date: datetime | None = None,
    sorts_suggestions: list[SortsSuggestion] | None = None,
) -> ToeLineState:
    return ToeLineState(
        attacked_at=attacked_at,
        attacked_by=attacked_by,
        attacked_lines=attacked_lines,
        be_present=be_present,
        be_present_until=be_present_until,
        comments=comments,
        first_attack_at=first_attack_at,
        has_vulnerabilities=has_vulnerabilities,
        last_author=last_author,
        last_commit=last_commit,
        last_commit_date=last_commit_date,
        loc=loc,
        modified_by=modified_by,
        modified_date=modified_date,
        seen_at=seen_at,
        sorts_risk_level=sorts_risk_level,
        sorts_priority_factor=sorts_priority_factor,
        sorts_risk_level_date=sorts_risk_level_date,
        sorts_suggestions=sorts_suggestions,
    )


def ToeLinesFaker(  # noqa: N802
    *,
    filename: str = "test.md",
    group_name: str = "test-group",
    root_id: str = random_uuid(),
    state: ToeLineState = ToeLinesStateFaker(),
    seen_first_time_by: str | None = None,
) -> ToeLine:
    return ToeLine(
        filename=filename,
        group_name=group_name,
        root_id=root_id,
        state=state,
        seen_first_time_by=seen_first_time_by,
    )
