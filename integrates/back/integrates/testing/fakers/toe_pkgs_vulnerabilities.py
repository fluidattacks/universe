from integrates.db_model.toe_packages.types import (
    ToePackageVulnerability,
)
from integrates.testing.fakers.randoms import (
    random_uuid,
)


def ToePackageVulnerabilityFaker(  # noqa: N802
    *,
    root_id: str = random_uuid(),
    name: str = "com.nimbusds:nimbus-jose-jwt",
    version: str = "8.3",
    vuln_id: str = random_uuid(),
) -> ToePackageVulnerability:
    return ToePackageVulnerability(
        root_id=root_id,
        name=name,
        version=version,
        vuln_id=vuln_id,
    )
