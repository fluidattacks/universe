from datetime import (
    datetime,
)

from integrates.db_model.credentials.types import (
    AWSRoleSecret,
    Credentials,
    CredentialsState,
    HttpsPatSecret,
    HttpsSecret,
    Secret,
    SshSecret,
)
from integrates.db_model.enums import (
    CredentialType,
)
from integrates.testing.fakers.constants import (
    DATE_2019,
    EMAIL_GENERIC,
)
from integrates.testing.fakers.randoms import (
    random_uuid,
)


def CredentialsStateFaker(  # noqa: N802
    *,
    modified_date: datetime = DATE_2019,
    modified_by: str = EMAIL_GENERIC,
    name: str = "credential",
    state_type: CredentialType = CredentialType.SSH,
    is_pat: bool = False,
    owner: str = EMAIL_GENERIC,
    azure_organization: str | None = None,
) -> CredentialsState:
    return CredentialsState(
        modified_by=modified_by,
        modified_date=modified_date,
        name=name,
        type=state_type,
        is_pat=is_pat,
        owner=owner,
        azure_organization=azure_organization,
    )


def CredentialsFaker(  # noqa: N802
    *,
    credential_id: str = random_uuid(),
    organization_id: str = random_uuid(),
    state: CredentialsState = CredentialsStateFaker(),
    secret: Secret = SshSecret(key=""),
) -> Credentials:
    def _get_type() -> CredentialType:
        if isinstance(secret, SshSecret):
            return CredentialType.SSH
        if isinstance(
            secret,
            (HttpsSecret, HttpsPatSecret),
        ):
            return CredentialType.HTTPS
        if isinstance(secret, AWSRoleSecret):
            return CredentialType.AWSROLE

        return CredentialType.OAUTH

    return Credentials(
        id=credential_id,
        organization_id=organization_id,
        state=state._replace(type=_get_type(), is_pat=isinstance(secret, HttpsPatSecret)),
        secret=secret,
    )
