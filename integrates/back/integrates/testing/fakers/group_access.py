from datetime import (
    datetime,
)

from integrates.db_model.group_access.types import (
    GroupAccess,
    GroupAccessState,
    GroupConfirmDeletion,
    GroupInvitation,
)
from integrates.testing.fakers.constants import (
    DATE_2019,
    EMAIL_GENERIC,
)


def GroupAccessStateFaker(  # noqa: N802
    *,
    modified_date: datetime = DATE_2019,
    modified_by: str = EMAIL_GENERIC,
    confirm_deletion: GroupConfirmDeletion | None = None,
    has_access: bool = True,
    invitation: GroupInvitation | None = None,
    responsibility: str | None = None,
    role: str | None = None,
) -> GroupAccessState:
    return GroupAccessState(
        modified_date=modified_date,
        modified_by=modified_by,
        confirm_deletion=confirm_deletion,
        has_access=has_access,
        invitation=invitation,
        responsibility=responsibility,
        role=role,
    )


def GroupAccessFaker(  # noqa: N802
    *,
    email: str = EMAIL_GENERIC,
    group_name: str = "test-group",
    state: GroupAccessState = GroupAccessStateFaker(),
    expiration_time: int | None = None,
    domain: str | None = None,
) -> GroupAccess:
    return GroupAccess(
        email=email,
        group_name=group_name,
        state=state,
        expiration_time=expiration_time,
        domain=domain,
    )
