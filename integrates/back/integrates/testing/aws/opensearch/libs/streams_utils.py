# mypy: ignore-errors
"""
Utils for integrates.streams.search.

These utils must be imported from the integrates.streams project once streams become a dependency
of the backend.
"""

from datetime import UTC, datetime
from typing import Any

from boto3.dynamodb.types import TypeDeserializer

from integrates.testing.aws.opensearch.libs.streams_types import Record, StreamEvent


def deserialize_dynamodb_json(item: dict[str, Any]) -> dict[str, Any]:
    """Deserializes a DynamoDB JSON into a python dictionary"""
    deserializer = TypeDeserializer()

    return {key: deserializer.deserialize(value) for key, value in item.items()}


def format_record(record: dict[str, Any]) -> Record:
    """
    Formats the record into a NamedTuple

    https://docs.aws.amazon.com/amazondynamodb/latest/APIReference/API_streams_Record.html
    """
    return Record(
        event_name=StreamEvent[record["eventName"]],
        new_image=(
            deserialize_dynamodb_json(record["dynamodb"]["NewImage"])
            if "NewImage" in record["dynamodb"]
            else None
        ),
        old_image=(
            deserialize_dynamodb_json(record["dynamodb"]["OldImage"])
            if "OldImage" in record["dynamodb"]
            else None
        ),
        pk=record["dynamodb"]["Keys"]["pk"]["S"],
        sequence_number=record["dynamodb"]["SequenceNumber"],
        sk=record["dynamodb"]["Keys"]["sk"]["S"],
    )


def format_vulns(records: tuple[Record, ...]) -> tuple[Record, ...]:
    formatted = []

    for record in records:
        if record.event_name in {StreamEvent.INSERT, StreamEvent.MODIFY} and record.new_image:
            # Needed as it doesn't fit in OpenSearch long data type (2^63)
            if "hash" in record.new_image:
                record.new_image["hash"] = str(record.new_image["hash"])
            # Needed as legacy ids were mapped to a numeric type, not uuids
            if "zero_risk" in record.new_image:
                record.new_image["zero_risk"]["comment_id"] = 0
        formatted.append(record)
    return tuple(formatted)


def format_date(obj: dict, date_key: str) -> None:
    if date_key in obj and obj[date_key] is not None:
        obj[date_key] = datetime.fromisoformat(obj[date_key]).astimezone(tz=UTC).isoformat()


def format_findings(records: list[Record]) -> list[Record]:
    formatted = []

    for record in records:
        ind_key = "unreliable_indicators"
        old_date_key = "oldest_vulnerability_report_date"
        new_date_key = "newest_vulnerability_report_date"

        if (
            record.event_name in {StreamEvent.INSERT, StreamEvent.MODIFY}
            and record.new_image
            and ind_key in record.new_image
        ):
            # Date must be in ISO format
            format_date(record.new_image[ind_key], old_date_key)
            format_date(record.new_image[ind_key], new_date_key)

        formatted.append(record)
    return formatted
