from collections.abc import Awaitable, Callable, Generator, Iterator
from contextlib import contextmanager
from dataclasses import dataclass
from typing import Any, TypeVar
from unittest.mock import patch

from aiobotocore.endpoint import convert_to_response_dict
from botocore.awsrequest import AWSResponse
from botocore.retries.standard import RetryContext

from integrates.testing.aws.opensearch.libs.fake_asyncopensearch import AsyncFakeOpenSearch

T = TypeVar("T")
R = TypeVar("R")


@dataclass
class _PatchedAWSReponseContent:
    """Patched version of `botocore.awsrequest.AWSResponse.content`"""

    content: bytes | Awaitable[bytes]

    def __await__(self) -> Iterator[bytes]:
        async def _generate_async() -> bytes:
            if isinstance(self.content, Awaitable):
                return await self.content
            return self.content

        return _generate_async().__await__()  # type: ignore[misc]

    def decode(self, encoding: str) -> str:
        assert isinstance(self.content, bytes)
        return self.content.decode(encoding)


class PatchedAWSResponse:
    """Patched version of `botocore.awsrequest.AWSResponse`"""

    def __init__(self, response: AWSResponse) -> None:
        self._response = response
        self.status_code = response.status_code

        self.headers = response.headers
        self.headers["x-amz-crc32"] = None
        self.url = response.url
        self.content = _PatchedAWSReponseContent(response.content)
        self._content = self.content
        self.raw = response.raw  # type: ignore[misc]
        self.text = response.text
        if not hasattr(self.raw, "raw_headers"):  # type: ignore[misc]
            self.raw.raw_headers = {}  # type: ignore[misc]


class PatchedRetryContext(RetryContext):
    """Patched version of `botocore.retries.standard.RetryContext`"""

    def __init__(  # type: ignore[misc]
        self,
        *args: Any,
        **kwargs: Any,
    ) -> None:
        if kwargs.get("http_response"):  # type: ignore[misc]
            kwargs["http_response"] = PatchedAWSResponse(  # type: ignore[misc]
                kwargs["http_response"],  # type: ignore[misc]
            )
        super().__init__(*args, **kwargs)  # type: ignore[misc]


def _factory(
    original: Callable[[AWSResponse, T], Awaitable[R]],
) -> Callable[[AWSResponse, T], Awaitable[R]]:
    async def patched_convert_to_response_dict(http_response: AWSResponse, operation_model: T) -> R:
        return await original(
            PatchedAWSResponse(http_response),  # type: ignore[arg-type]
            operation_model,
        )

    return patched_convert_to_response_dict


@contextmanager
def patch_aio() -> Generator[None, None, None]:
    """
    Patch for Moto library
    Inspired by a GitHub discussion
    (https://github.com/aio-libs/aiobotocore/issues/755).
    """
    with (
        patch(  # type: ignore[misc]
            "aiobotocore.endpoint.convert_to_response_dict",
            new=_factory(  # type: ignore[misc]
                convert_to_response_dict,  # type: ignore[arg-type, misc]
            ),
        ),
        patch(  # type: ignore[misc]
            "botocore.retries.standard.RetryContext",
            new=PatchedRetryContext,  # type: ignore[misc]
        ),
    ):
        yield


@contextmanager
def patch_opensearch() -> Generator[None, None, None]:
    with (
        patch("integrates.search.client.AsyncOpenSearch", new=AsyncFakeOpenSearch),  # type: ignore[misc]
    ):
        yield
