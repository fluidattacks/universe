import asyncio
import json

from integrates.batch import dal as batch_dal
from integrates.batch.types import BatchProcessing
from integrates.class_types.types import Item
from integrates.context import FI_LLM_SCAN_DB_MODEL_PATH
from integrates.db_model import credentials as credentials_model
from integrates.db_model import event_comments as event_comments_model
from integrates.db_model import events as events_model
from integrates.db_model import finding_comments as finding_comments_model
from integrates.db_model import findings as findings_model
from integrates.db_model import forces as forces_model
from integrates.db_model import group_access as group_access_model
from integrates.db_model import groups as groups_model
from integrates.db_model import hook as hook_model
from integrates.db_model import integration_repositories as integration_repositories_model
from integrates.db_model import jira_installations as jira_installations_model
from integrates.db_model import marketplace as marketplace_model
from integrates.db_model import notifications as notifications_model
from integrates.db_model import organization_access as organization_access_model
from integrates.db_model import organization_finding_policies as organization_finding_policies_model
from integrates.db_model import organizations as organizations_model
from integrates.db_model import portfolios as portfolios_model
from integrates.db_model import roots as roots_model
from integrates.db_model import stakeholders as stakeholders_model
from integrates.db_model import toe_inputs as toe_inputs_model
from integrates.db_model import toe_lines as toe_lines_model
from integrates.db_model import toe_packages as toe_packages_model
from integrates.db_model import toe_ports as toe_ports_model
from integrates.db_model import trials as trials_model
from integrates.db_model import vulnerabilities as vulnerabilities_model
from integrates.db_model.credentials.types import Credentials
from integrates.db_model.event_comments.types import EventComment
from integrates.db_model.events.types import Event
from integrates.db_model.finding_comments.types import FindingComment
from integrates.db_model.findings.types import Finding
from integrates.db_model.forces.types import ForcesExecution
from integrates.db_model.group_access.types import GroupAccess, GroupAccessMetadataToUpdate
from integrates.db_model.groups.types import Group
from integrates.db_model.hook.types import GroupHook
from integrates.db_model.hook.utils import format_group_hook_payload
from integrates.db_model.integration_repositories.types import OrganizationIntegrationRepository
from integrates.db_model.jira_installations.types import JiraSecurityInstall
from integrates.db_model.mailmap.types import MailmapEntry
from integrates.db_model.mailmap.utils.create import create_entry_item
from integrates.db_model.marketplace.types import AWSMarketplaceSubscription
from integrates.db_model.notifications.types import Notification
from integrates.db_model.organization_access.types import (
    OrganizationAccess,
    OrganizationAccessMetadataToUpdate,
)
from integrates.db_model.organization_finding_policies.types import OrgFindingPolicy
from integrates.db_model.organizations.types import Organization
from integrates.db_model.portfolios.types import Portfolio
from integrates.db_model.roots.types import Root, RootDockerImage, RootEnvironmentUrl
from integrates.db_model.stakeholders.types import Stakeholder, StakeholderMetadataToUpdate
from integrates.db_model.toe_inputs.types import ToeInput, ToeInputMetadataToUpdate, ToeInputUpdate
from integrates.db_model.toe_lines.types import ToeLine
from integrates.db_model.toe_packages.types import ToePackage, ToePackageVulnerability
from integrates.db_model.toe_ports.types import ToePort
from integrates.db_model.trials.types import Trial
from integrates.db_model.types import CodeSnippetLLM, PoliciesToUpdate, VulnerabilityRecordLLM
from integrates.db_model.vulnerabilities.types import Vulnerability
from integrates.dynamodb.operations import batch_put_item
from integrates.dynamodb.tables import load_tables
from integrates.testing.aws.dynamodb.types import (
    EventHistoricStates,
    FindingHistoricStates,
    FindingHistoricVerifications,
    FindingIndicatorsUpdate,
    GroupUnreliableIndicatorsToUpdate,
    IntegratesDynamodb,
    OrganizationUnreliableIndicatorsToUpdate,
    RootEnvironmentSecretsToUpdate,
    RootSecretsToUpdate,
)


async def action(item: BatchProcessing) -> None:
    await batch_dal.put_action_to_dynamodb(
        action=item.action_name,
        entity=item.entity,
        subject=item.subject,
        time=item.time,
        additional_info=item.additional_info,
        queue=item.queue,
        attempt_duration_seconds=item.attempt_duration_seconds,
        batch_job_id=item.batch_job_id,
        dependent_actions=item.dependent_actions,
    )


async def llm_scan(items: list[CodeSnippetLLM | VulnerabilityRecordLLM]) -> None:
    with open(FI_LLM_SCAN_DB_MODEL_PATH, encoding="utf-8") as file:
        table = load_tables(json.load(file))[0]  # type: ignore

    await batch_put_item(table=table, items=tuple(items))


async def aws_subscription(item: AWSMarketplaceSubscription) -> None:
    await marketplace_model.add_marketplace_subscription(subscription=item)


async def docker_images(item: RootDockerImage) -> None:
    await roots_model.add_root_docker_image(
        image=item,
        group_name=item.group_name,
        root_id=item.root_id,
    )


async def stakeholder(item: Stakeholder) -> None:
    await stakeholders_model.update_metadata(
        email=item.email,
        metadata=StakeholderMetadataToUpdate(
            access_tokens=item.access_tokens,
            aws_customer_id=item.aws_customer_id,
            enrolled=item.enrolled,
            first_name=item.first_name,
            is_concurrent_session=item.is_concurrent_session,
            is_registered=item.is_registered,
            jira_client_key=item.jira_client_key,
            last_login_date=item.last_login_date,
            last_name=item.last_name,
            legal_remember=item.legal_remember,
            phone=item.phone,
            registration_date=item.registration_date,
            role=item.role,
            session_key=item.session_key,
            session_token=item.session_token,
            subject=item.subject,
            tours=item.tours,
        ),
    )

    await stakeholders_model.update_state(user_email=item.email, state=item.state)


async def notification(item: Notification) -> None:
    await notifications_model.add(notification=item)


async def organization_access(item: OrganizationAccess) -> None:
    await organization_access_model.update_metadata(
        email=item.email,
        metadata=OrganizationAccessMetadataToUpdate(state=item.state),
        organization_id=item.organization_id,
    )


async def organization_finding_policy(item: OrgFindingPolicy) -> None:
    await organization_finding_policies_model.add(policy=item)


async def jira_installation(item: JiraSecurityInstall) -> None:
    await jira_installations_model.add(jira_install=item)


async def organization(item: Organization) -> None:
    await organizations_model.add(organization=item)


async def credentials(item: Credentials) -> None:
    await credentials_model.add(credential=item)


async def organization_integration_repository(
    item: OrganizationIntegrationRepository,
) -> None:
    await integration_repositories_model.update_unreliable_repositories(repository=item)


async def group_access(item: GroupAccess) -> None:
    await group_access_model.update_metadata(
        email=item.email,
        group_name=item.group_name,
        metadata=GroupAccessMetadataToUpdate(state=item.state),
    )


async def group(item: Group) -> None:
    await groups_model.add(group=item)

    if item.policies:
        await groups_model.update_policies(
            group_name=item.name,
            modified_by=item.policies.modified_by,
            modified_date=item.policies.modified_date,
            organization_id=item.organization_id,
            policies=PoliciesToUpdate(
                max_acceptance_days=(item.policies.max_acceptance_days),
                max_acceptance_severity=(item.policies.max_acceptance_severity),
                max_number_acceptances=(item.policies.max_number_acceptances),
            ),
        )


async def finding(item: Finding) -> None:
    await findings_model.add(finding=item)
    await findings_model.add_historic_verification(finding=item)


async def finding_comment(item: FindingComment) -> None:
    await finding_comments_model.add(finding_comment=item)


async def finding_historic_states(item: FindingHistoricStates) -> None:
    await asyncio.gather(
        *[
            findings_model.update_state(
                current_value=item.finding.state,
                group_name=item.finding.group_name,
                finding_id=item.finding.id,
                state=state,
            )
            for state in item.states
        ]
    )


async def finding_historic_verifications(item: FindingHistoricVerifications) -> None:
    await asyncio.gather(
        *[
            findings_model.update_verification(
                current_value=item.finding.verification,
                group_name=item.finding.group_name,
                finding_id=item.finding.id,
                verification=verification,
            )
            for verification in item.verifications
        ]
    )


async def finding_unreliable_indicators(item: FindingIndicatorsUpdate) -> None:
    await findings_model.update_unreliable_indicators(
        current_value=item.finding.unreliable_indicators,
        group_name=item.finding.group_name,
        finding_id=item.finding.id,
        indicators=item.indicators,
    )


async def forces_execution(item: ForcesExecution) -> None:
    await forces_model.add(forces_execution=item)


async def root(item: Root) -> None:
    await roots_model.add(root=item)


async def root_environment_secret(item: RootEnvironmentSecretsToUpdate) -> None:
    await roots_model.add_root_environment_secret(
        group_name=item.group_name,
        resource_id=item.resource_id,
        secret=item.secret,
    )


async def root_environment_url(item: RootEnvironmentUrl) -> None:
    await roots_model.add_root_environment_url(
        group_name=item.group_name,
        root_id=item.root_id,
        url=item,
    )


async def root_secret(item: RootSecretsToUpdate) -> None:
    await roots_model.add_secret(
        resource_id=item.resource_id,
        secret=item.secret,
    )


async def vulnerability(item: Vulnerability) -> None:
    await vulnerabilities_model.add(vulnerability=item)
    await vulnerabilities_model.update_new_historic(current_value=item, historic=(item.state,))
    if item.treatment is not None:
        await vulnerabilities_model.update_new_historic(
            current_value=item, historic=(item.treatment,)
        )


async def toe_input(item: ToeInput) -> None:
    await toe_inputs_model.add(toe_input=item)

    await toe_inputs_model.add_historic(toe_input=item)

    await toe_inputs_model.update_state(
        update_info=ToeInputUpdate(
            current_value=item,
            new_state=item.state,
            metadata=ToeInputMetadataToUpdate(
                clean_attacked_at=False,
                clean_be_present_until=False,
                clean_first_attack_at=False,
                clean_seen_at=False,
            ),
        ),
    )


async def toe_lines(item: ToeLine) -> None:
    await toe_lines_model.add(toe_lines=item)
    await toe_lines_model.add_historic(toe_lines=item)


async def toe_packages(item: ToePackage) -> None:
    await toe_packages_model.add(toe_package=item)


async def toe_pkgs_vulnerabilities(item: ToePackageVulnerability) -> None:
    await toe_packages_model.add_package_vulnerability(toe_package=item)


async def toe_port(item: ToePort) -> None:
    await toe_ports_model.add(toe_port=item)
    await toe_ports_model.add_historic(toe_port=item)


async def event(item: Event) -> None:
    await events_model.add(event=item)


async def event_historic_states(item: EventHistoricStates) -> None:
    await asyncio.gather(
        *[
            events_model.update_state(
                current_value=item.event,
                group_name=item.event.group_name,
                state=state,
            )
            for state in item.states
        ]
    )


async def mailmap_entry(item: MailmapEntry) -> None:
    await create_entry_item(entry=item, organization_id=str(item["organization_id"]))  # type: ignore[misc]


async def event_comment(item: EventComment) -> None:
    await event_comments_model.add(event_comment=item)


async def trial(item: Trial) -> None:
    await trials_model.add(trial=item)


async def portfolio(item: Portfolio) -> None:
    await portfolios_model.update(portfolio=item)


async def group_hook(item: GroupHook) -> None:
    hook: Item = {
        "hook_events": item.hook_events,
        "entry_point": item.entry_point,
        "token": item.token,
        "token_header": item.token_header,
    }
    await hook_model.add_hook(
        hook=format_group_hook_payload(hook),
        group_name=item.group_name,
        user_email=item.state.modified_by,
        hook_id=item.id,
        modified_date=item.state.modified_date,
    )


async def group_unreliable_indicators(item: GroupUnreliableIndicatorsToUpdate) -> None:
    await groups_model.update_unreliable_indicators(
        group_name=item.group_name,
        indicators=item.indicators,
    )


async def organization_unreliable_indicators(
    item: OrganizationUnreliableIndicatorsToUpdate,
) -> None:
    await organizations_model.update_unreliable_indicators(
        organization_id=item.organization_id,
        organization_name=item.organization_name,
        indicators=item.indicators,
    )


async def main(data: IntegratesDynamodb = IntegratesDynamodb()) -> None:
    await asyncio.gather(
        *[action(item) for item in data.actions],
        *[aws_subscription(item) for item in data.aws_subscriptions],
        *[credentials(item) for item in data.credentials],
        *[docker_images(item) for item in data.docker_images],
        *[event_comment(item) for item in data.event_comments],
        *[event(item) for item in data.events],
        *[finding(item) for item in data.findings],
        *[finding_comment(item) for item in data.finding_comments],
        *[finding_unreliable_indicators(item) for item in data.finding_unreliable_indicators],
        *[forces_execution(item) for item in data.forces_execution],
        *[group_access(item) for item in data.group_access],
        *[group_hook(item) for item in data.group_hook],
        *[group(item) for item in data.groups],
        *[jira_installation(item) for item in data.jira_installations],
        *[notification(item) for item in data.notifications],
        *[organization_access(item) for item in data.organization_access],
        *[
            organization_integration_repository(item)
            for item in data.organization_integration_repositories
        ],
        *[organization_finding_policy(item) for item in data.organization_finding_policies],
        *[organization(item) for item in data.organizations],
        *[mailmap_entry(item) for item in data.mailmap_entries],
        *[portfolio(item) for item in data.portfolios],
        *[root_environment_secret(item) for item in data.root_environment_secrets],
        *[root_environment_url(item) for item in data.root_environment_urls],
        *[root_secret(item) for item in data.root_secrets],
        *[root(item) for item in data.roots],
        *[stakeholder(item) for item in data.stakeholders],
        *[toe_input(item) for item in data.toe_inputs],
        *[toe_lines(item) for item in data.toe_lines],
        *[toe_packages(item) for item in data.toe_packages],
        *[toe_pkgs_vulnerabilities(item) for item in data.toe_pkgs_vulnerabilities],
        *[toe_port(item) for item in data.toe_ports],
        *[trial(item) for item in data.trials],
        *[vulnerability(item) for item in data.vulnerabilities],
        *([llm_scan(data.llm_scan)] if data.llm_scan else []),  # type: ignore
    )

    await asyncio.gather(
        *[group_unreliable_indicators(item) for item in data.group_unreliable_indicators],
        *[
            organization_unreliable_indicators(item)
            for item in data.organization_unreliable_indicators
        ],
    )

    await asyncio.gather(
        *[event_historic_states(item) for item in data.event_historic_states],
        *[finding_historic_states(item) for item in data.finding_historic_states],
        *[finding_historic_verifications(item) for item in data.finding_historic_verifications],
    )
