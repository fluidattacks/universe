from integrates.testing.aws.dynamodb.core import (
    init,
)

__all__ = [
    "init",
]
