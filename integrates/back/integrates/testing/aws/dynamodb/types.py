from typing import (
    Literal,
    NamedTuple,
    TypeAlias,
)

from types_aiobotocore_dynamodb.service_resource import (
    DynamoDBServiceResource,
    Table,
)
from types_aiobotocore_dynamodb.type_defs import (
    CreateTableInputServiceResourceCreateTableTypeDef,
)

from integrates.batch.types import (
    BatchProcessing,
)
from integrates.db_model.credentials.types import (
    Credentials,
)
from integrates.db_model.event_comments.types import (
    EventComment,
)
from integrates.db_model.events.types import (
    Event,
    EventState,
)
from integrates.db_model.finding_comments.types import (
    FindingComment,
)
from integrates.db_model.findings.types import (
    Finding,
    FindingState,
    FindingUnreliableIndicatorsToUpdate,
    FindingVerification,
)
from integrates.db_model.forces.types import ForcesExecution
from integrates.db_model.group_access.types import (
    GroupAccess,
)
from integrates.db_model.groups.types import (
    Group,
    GroupUnreliableIndicators,
)
from integrates.db_model.hook.types import (
    GroupHook,
)
from integrates.db_model.integration_repositories.types import (
    OrganizationIntegrationRepository,
)
from integrates.db_model.jira_installations.types import (
    JiraSecurityInstall,
)
from integrates.db_model.mailmap.types import MailmapEntry
from integrates.db_model.marketplace.types import AWSMarketplaceSubscription
from integrates.db_model.notifications.types import (
    Notification,
)
from integrates.db_model.organization_access.types import (
    OrganizationAccess,
)
from integrates.db_model.organization_finding_policies.types import (
    OrgFindingPolicy,
)
from integrates.db_model.organizations.types import (
    Organization,
    OrganizationUnreliableIndicators,
)
from integrates.db_model.portfolios.types import (
    Portfolio,
)
from integrates.db_model.roots.types import (
    Root,
    RootDockerImage,
    RootEnvironmentUrl,
    Secret,
)
from integrates.db_model.stakeholders.types import (
    Stakeholder,
)
from integrates.db_model.toe_inputs.types import (
    ToeInput,
)
from integrates.db_model.toe_lines.types import (
    ToeLine,
)
from integrates.db_model.toe_packages.types import (
    ToePackage,
    ToePackageVulnerability,
)
from integrates.db_model.toe_ports.types import (
    ToePort,
)
from integrates.db_model.trials.types import (
    Trial,
)
from integrates.db_model.types import CodeSnippetLLM, VulnerabilityRecordLLM
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityState,
)

DynamoDbTableName = Literal[
    "fi_async_processing",
    "integrates_vms_historic",
    "integrates_vms",
    "llm_scan",
]

DynamoDbResource: TypeAlias = DynamoDBServiceResource  # type: ignore[misc]

DynamoDbTable: TypeAlias = Table  # type: ignore[misc]

TableSchemas: dict[DynamoDbTableName, CreateTableInputServiceResourceCreateTableTypeDef] = {
    "fi_async_processing": {
        "TableName": "fi_async_processing",
        "TableClass": "STANDARD",
        "BillingMode": "PAY_PER_REQUEST",
        "DeletionProtectionEnabled": True,
        "KeySchema": [
            {"AttributeName": "pk", "KeyType": "HASH"},
        ],
        "AttributeDefinitions": [
            {"AttributeName": "action_name", "AttributeType": "S"},
            {"AttributeName": "entity", "AttributeType": "S"},
            {"AttributeName": "pk", "AttributeType": "S"},
        ],
        "GlobalSecondaryIndexes": [
            {
                "IndexName": "gsi-1",
                "KeySchema": [
                    {"AttributeName": "action_name", "KeyType": "HASH"},
                    {"AttributeName": "entity", "KeyType": "RANGE"},
                ],
                "Projection": {"ProjectionType": "ALL"},
            },
        ],
    },
    "integrates_vms_historic": {
        "TableName": "integrates_vms_historic",
        "TableClass": "STANDARD_INFREQUENT_ACCESS",
        "BillingMode": "PAY_PER_REQUEST",
        "DeletionProtectionEnabled": True,
        "KeySchema": [
            {"AttributeName": "pk", "KeyType": "HASH"},
            {"AttributeName": "sk", "KeyType": "RANGE"},
        ],
        "AttributeDefinitions": [
            {"AttributeName": "pk", "AttributeType": "S"},
            {"AttributeName": "sk", "AttributeType": "S"},
            {"AttributeName": "pk_2", "AttributeType": "S"},
            {"AttributeName": "sk_2", "AttributeType": "S"},
            {"AttributeName": "pk_3", "AttributeType": "S"},
            {"AttributeName": "sk_3", "AttributeType": "S"},
        ],
        "GlobalSecondaryIndexes": [
            {
                "IndexName": "gsi_2",
                "KeySchema": [
                    {"AttributeName": "pk_2", "KeyType": "HASH"},
                    {"AttributeName": "sk_2", "KeyType": "RANGE"},
                ],
                "Projection": {"ProjectionType": "ALL"},
            },
            {
                "IndexName": "gsi_3",
                "KeySchema": [
                    {"AttributeName": "pk_3", "KeyType": "HASH"},
                    {"AttributeName": "sk_3", "KeyType": "RANGE"},
                ],
                "Projection": {"ProjectionType": "ALL"},
            },
        ],
    },
    "integrates_vms": {
        "TableName": "integrates_vms",
        "TableClass": "STANDARD",
        "BillingMode": "PAY_PER_REQUEST",
        "DeletionProtectionEnabled": True,
        "StreamSpecification": {
            "StreamEnabled": True,
            "StreamViewType": "NEW_AND_OLD_IMAGES",
        },
        "KeySchema": [
            {"AttributeName": "pk", "KeyType": "HASH"},
            {"AttributeName": "sk", "KeyType": "RANGE"},
        ],
        "AttributeDefinitions": [
            {"AttributeName": "pk", "AttributeType": "S"},
            {"AttributeName": "sk", "AttributeType": "S"},
            {"AttributeName": "pk_hash", "AttributeType": "S"},
            {"AttributeName": "sk_hash", "AttributeType": "S"},
            {"AttributeName": "pk_2", "AttributeType": "S"},
            {"AttributeName": "sk_2", "AttributeType": "S"},
            {"AttributeName": "pk_3", "AttributeType": "S"},
            {"AttributeName": "sk_3", "AttributeType": "S"},
            {"AttributeName": "pk_4", "AttributeType": "S"},
            {"AttributeName": "sk_4", "AttributeType": "S"},
            {"AttributeName": "pk_5", "AttributeType": "S"},
            {"AttributeName": "sk_5", "AttributeType": "S"},
            {"AttributeName": "pk_6", "AttributeType": "S"},
            {"AttributeName": "sk_6", "AttributeType": "S"},
        ],
        "GlobalSecondaryIndexes": [
            {
                "IndexName": "inverted_index",
                "KeySchema": [
                    {"AttributeName": "sk", "KeyType": "HASH"},
                    {"AttributeName": "pk", "KeyType": "RANGE"},
                ],
                "Projection": {"ProjectionType": "ALL"},
            },
            {
                "IndexName": "gsi_hash",
                "KeySchema": [
                    {"AttributeName": "pk_hash", "KeyType": "HASH"},
                    {"AttributeName": "sk_hash", "KeyType": "RANGE"},
                ],
                "Projection": {"ProjectionType": "ALL"},
            },
            {
                "IndexName": "gsi_2",
                "KeySchema": [
                    {"AttributeName": "pk_2", "KeyType": "HASH"},
                    {"AttributeName": "sk_2", "KeyType": "RANGE"},
                ],
                "Projection": {"ProjectionType": "ALL"},
            },
            {
                "IndexName": "gsi_3",
                "KeySchema": [
                    {"AttributeName": "pk_3", "KeyType": "HASH"},
                    {"AttributeName": "sk_3", "KeyType": "RANGE"},
                ],
                "Projection": {"ProjectionType": "ALL"},
            },
            {
                "IndexName": "gsi_4",
                "KeySchema": [
                    {"AttributeName": "pk_4", "KeyType": "HASH"},
                    {"AttributeName": "sk_4", "KeyType": "RANGE"},
                ],
                "Projection": {"ProjectionType": "ALL"},
            },
            {
                "IndexName": "gsi_5",
                "KeySchema": [
                    {"AttributeName": "pk_5", "KeyType": "HASH"},
                    {"AttributeName": "sk_5", "KeyType": "RANGE"},
                ],
                "Projection": {"ProjectionType": "ALL"},
            },
            {
                "IndexName": "gsi_6",
                "KeySchema": [
                    {"AttributeName": "pk_6", "KeyType": "HASH"},
                    {"AttributeName": "sk_6", "KeyType": "RANGE"},
                ],
                "Projection": {"ProjectionType": "ALL"},
            },
        ],
    },
    "llm_scan": {
        "TableName": "llm_scan",
        "TableClass": "STANDARD",
        "BillingMode": "PAY_PER_REQUEST",
        "DeletionProtectionEnabled": True,
        "StreamSpecification": {
            "StreamEnabled": True,
            "StreamViewType": "NEW_AND_OLD_IMAGES",
        },
        "KeySchema": [
            {"AttributeName": "pk", "KeyType": "HASH"},
            {"AttributeName": "sk", "KeyType": "RANGE"},
        ],
        "AttributeDefinitions": [
            {"AttributeName": "pk", "AttributeType": "S"},
            {"AttributeName": "sk", "AttributeType": "S"},
        ],
        "GlobalSecondaryIndexes": [
            {
                "IndexName": "inverted_index",
                "KeySchema": [
                    {"AttributeName": "sk", "KeyType": "HASH"},
                    {"AttributeName": "pk", "KeyType": "RANGE"},
                ],
                "Projection": {"ProjectionType": "ALL"},
            },
        ],
    },
}


class GroupUnreliableIndicatorsToUpdate(NamedTuple):
    group_name: str
    indicators: GroupUnreliableIndicators


class EventHistoricStates(NamedTuple):
    event: Event
    states: list[EventState]


class FindingHistoricStates(NamedTuple):
    finding: Finding
    states: list[FindingState]


class VulnerabilityHistoricStates(NamedTuple):
    vulnerability: Vulnerability
    states: list[VulnerabilityState]


class FindingHistoricVerifications(NamedTuple):
    finding: Finding
    verifications: list[FindingVerification]


class FindingIndicatorsUpdate(NamedTuple):
    finding: Finding
    indicators: FindingUnreliableIndicatorsToUpdate


class OrganizationUnreliableIndicatorsToUpdate(NamedTuple):
    organization_id: str
    organization_name: str
    indicators: OrganizationUnreliableIndicators


class RootEnvironmentSecretsToUpdate(NamedTuple):
    group_name: str
    resource_id: str
    secret: Secret


class RootSecretsToUpdate(NamedTuple):
    resource_id: str
    secret: Secret


class IntegratesDynamodb(NamedTuple):
    actions: list[BatchProcessing] = []
    aws_subscriptions: list[AWSMarketplaceSubscription] = []
    credentials: list[Credentials] = []
    docker_images: list[RootDockerImage] = []
    event_comments: list[EventComment] = []
    event_historic_states: list[EventHistoricStates] = []
    events: list[Event] = []
    finding_comments: list[FindingComment] = []
    finding_historic_states: list[FindingHistoricStates] = []
    finding_historic_verifications: list[FindingHistoricVerifications] = []
    finding_unreliable_indicators: list[FindingIndicatorsUpdate] = []
    findings: list[Finding] = []
    forces_execution: list[ForcesExecution] = []
    group_access: list[GroupAccess] = []
    group_hook: list[GroupHook] = []
    group_unreliable_indicators: list[GroupUnreliableIndicatorsToUpdate] = []
    groups: list[Group] = []
    jira_installations: list[JiraSecurityInstall] = []
    mailmap_entries: list[MailmapEntry] = []
    llm_scan: list[CodeSnippetLLM | VulnerabilityRecordLLM] = []
    notifications: list[Notification] = []
    organization_access: list[OrganizationAccess] = []
    organization_finding_policies: list[OrgFindingPolicy] = []
    organization_integration_repositories: list[OrganizationIntegrationRepository] = []
    organization_unreliable_indicators: list[OrganizationUnreliableIndicatorsToUpdate] = []
    organizations: list[Organization] = []
    portfolios: list[Portfolio] = []
    root_environment_secrets: list[RootEnvironmentSecretsToUpdate] = []
    root_environment_urls: list[RootEnvironmentUrl] = []
    root_secrets: list[RootSecretsToUpdate] = []
    roots: list[Root] = []
    stakeholders: list[Stakeholder] = []
    toe_inputs: list[ToeInput] = []
    toe_lines: list[ToeLine] = []
    toe_packages: list[ToePackage] = []
    toe_pkgs_vulnerabilities: list[ToePackageVulnerability] = []
    toe_ports: list[ToePort] = []
    trials: list[Trial] = []
    vulnerabilities: list[Vulnerability] = []
