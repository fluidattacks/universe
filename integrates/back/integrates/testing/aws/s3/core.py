from collections.abc import Awaitable, Callable
from typing import ParamSpec, TypeVar

from aioextensions import collect

import integrates.testing.aws.s3.populate as s3_populate
from integrates.s3.resource import get_client
from integrates.testing.aws.s3.types import IntegratesS3

T = TypeVar("T")
P = ParamSpec("P")

BUCKETS = [
    "integrates.dev",
    "integrates",
    "integrates.continuous-repositories",
    "logpush.fluidattacks.com",
    "machine.data",
]


async def _create_bucket(bucket_name: str) -> None:
    client = await get_client()
    await client.create_bucket(Bucket=bucket_name)


async def init(s3: IntegratesS3, func: Callable[P, Awaitable[T]]) -> None:
    await collect(
        [_create_bucket(bucket) for bucket in BUCKETS],
    )

    if s3.autoload:
        await s3_populate.main(func)
