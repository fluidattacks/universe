from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.db_model.trials.enums import (
    TrialStatus,
)
from integrates.db_model.trials.types import (
    Trial,
)

FREE_TRIAL_DAYS = 21


def get_status(trial: Trial) -> TrialStatus:
    if trial.extension_date:
        if trial.completed:
            return TrialStatus.EXTENDED_ENDED
        return TrialStatus.EXTENDED

    if trial.completed:
        return TrialStatus.TRIAL_ENDED
    return TrialStatus.TRIAL


def get_days_since_expiration(trial: Trial) -> int:
    if not trial.completed:
        return 0

    if trial.extension_date:
        return datetime_utils.get_days_since(trial.extension_date) - trial.extension_days
    if trial.start_date:
        return datetime_utils.get_days_since(trial.start_date) - FREE_TRIAL_DAYS

    return 0


def get_remaining_days(trial: Trial) -> int:
    days = 0

    if trial.extension_date:
        days = trial.extension_days - datetime_utils.get_days_since(trial.extension_date)
    elif trial.start_date:
        days = FREE_TRIAL_DAYS - datetime_utils.get_days_since(trial.start_date)

    return max(0, days)
