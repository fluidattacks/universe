import integrates
from integrates.resources.domain import upload_file
from integrates.testing.aws import IntegratesAws, IntegratesS3
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import parametrize


@parametrize(
    args=["file_info", "group_name"],
    cases=[
        ["test_file.txt", "group1"],
        ["barely-compliant-filename-.xyz.tar.gz", "group2"],
    ],
)
@mocks(
    aws=IntegratesAws(
        s3=IntegratesS3(autoload=True),
    ),
    others=[Mock(integrates.s3.operations, "FI_AWS_S3_PATH_PREFIX", "function", "")],
)
async def test_upload_file(file_info: str, group_name: str) -> None:
    sign = await upload_file(file_info, group_name)
    assert "url" in sign
    assert "fields" in sign
    assert sign["fields"]["key"] == f"resources/{group_name}/{file_info}"
    assert sign["url"] == "https://s3.amazonaws.com/integrates.dev"  # type:ignore
