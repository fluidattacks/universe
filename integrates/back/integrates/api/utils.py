import re

from integrates.api.types import Operation


def get_first_operation(raw_query: str | None, variables: dict[str, str] | None) -> Operation:
    query: str = (
        " ".join(
            raw_query.replace("\n", " ").replace("\t", " ").strip().split(),
        )
        if raw_query
        else ""
    )
    pattern = re.compile(r"(query|mutation|subscription)\s+([a-zA-Z]\w*)")
    match = pattern.search(query)

    if not match:
        return Operation(
            name="GraphQL Operation",
            operation_type="query",
            query=query or "",
            variables=variables or {},
        )

    return Operation(
        name=match.group(2),
        operation_type=match.group(1),
        query=query or "",
        variables=variables or {},
    )
