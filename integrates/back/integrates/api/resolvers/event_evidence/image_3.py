from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.events.types import (
    EventEvidence,
    EventEvidences,
)

from .schema import (
    EVENT_EVIDENCE,
)


@EVENT_EVIDENCE.field("image3")
def resolve(
    parent: EventEvidences,
    _info: GraphQLResolveInfo,
    **_kwargs: None,
) -> EventEvidence | None:
    image_3 = parent.image_3
    return image_3
