from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.events.types import (
    EventEvidences,
)

from .schema import (
    EVENT_EVIDENCE,
)


@EVENT_EVIDENCE.field("image2")
def resolve(
    parent: EventEvidences,
    _info: GraphQLResolveInfo,
    **_kwargs: None,
) -> object:
    image_2 = parent.image_2
    return image_2
