from graphql.type.definition import GraphQLResolveInfo

from integrates.db_model.roots.types import RootEnvironmentUrl

from .schema import GIT_ENVIRONMENT_URL


@GIT_ENVIRONMENT_URL.field("isProduction")
async def resolve(parent: RootEnvironmentUrl, info: GraphQLResolveInfo, **__: None) -> bool | None:
    return parent.state.is_production
