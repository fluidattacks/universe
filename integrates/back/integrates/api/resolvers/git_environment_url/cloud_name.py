from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.roots.types import (
    RootEnvironmentUrl,
)

from .schema import (
    GIT_ENVIRONMENT_URL,
)


@GIT_ENVIRONMENT_URL.field("cloudName")
def resolve(parent: RootEnvironmentUrl, _info: GraphQLResolveInfo, **__: None) -> str | None:
    return parent.state.cloud_name
