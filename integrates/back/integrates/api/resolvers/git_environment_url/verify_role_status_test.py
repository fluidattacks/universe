from integrates.api.resolvers.git_environment_url.verify_role_status import resolve
from integrates.testing.fakers import GraphQLResolveInfoFaker, RootEnvironmentUrlFaker

ADMIN = "admin@gmail.com"


async def test_resolve_verify_role_status() -> None:
    info = GraphQLResolveInfoFaker(user_email=ADMIN)
    parent = RootEnvironmentUrlFaker()
    await resolve(parent=parent, info=info)
