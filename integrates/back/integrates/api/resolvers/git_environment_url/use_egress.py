from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.roots.types import (
    RootEnvironmentUrl,
)

from .schema import (
    GIT_ENVIRONMENT_URL,
)


@GIT_ENVIRONMENT_URL.field("useEgress")
def resolve(parent: RootEnvironmentUrl, _info: GraphQLResolveInfo, **__: None) -> bool:
    return parent.state.use_egress if parent.state.use_egress else False
