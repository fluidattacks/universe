from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.roots.types import (
    RootEnvironmentUrl,
)
from integrates.decorators import (
    enforce_group_level_auth_async,
)
from integrates.roots.domain import (
    get_environment_url_secrets,
)

from .schema import (
    GIT_ENVIRONMENT_URL,
)


@GIT_ENVIRONMENT_URL.field("hasSecrets")
@enforce_group_level_auth_async
async def resolve(parent: RootEnvironmentUrl, info: GraphQLResolveInfo, **__: None) -> bool:
    loaders: Dataloaders = info.context.loaders
    secrets = await get_environment_url_secrets(loaders, parent)
    return len(secrets) > 0
