from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils.findings import (
    is_finding_released,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.db_model.vulnerabilities.types import (
    VulnerabilityRequest,
)
from integrates.findings import (
    domain as findings_domain,
)
from integrates.findings.types import (
    Tracking,
)

from .schema import (
    FINDING,
)


@FINDING.field("tracking")
async def resolve(parent: Finding, info: GraphQLResolveInfo, **_kwargs: None) -> list[Tracking]:
    if not is_finding_released(parent):
        return []

    loaders: Dataloaders = info.context.loaders
    finding_vulns_loader = loaders.finding_vulnerabilities_released_nzr
    vulns = await finding_vulns_loader.load(parent.id)
    vulnerability_requests = tuple(
        VulnerabilityRequest(vulnerability_id=vuln.id, finding_id=vuln.finding_id) for vuln in vulns
    )
    vulns_state = await loaders.vulnerability_historic_state.load_many(vulnerability_requests)
    vulns_treatment = await loaders.vulnerability_historic_treatment.load_many(
        vulnerability_requests,
    )

    return findings_domain.get_tracking_vulnerabilities(
        vulns_state=vulns_state,
        vulns_treatment=vulns_treatment,
    )
