from datetime import datetime

from integrates.api.resolvers.finding.consulting import resolve
from integrates.db_model.finding_comments.enums import CommentType
from integrates.decorators import enforce_group_level_auth_async, require_attribute
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    EMAIL_FLUIDATTACKS_ADMIN,
    EMAIL_FLUIDATTACKS_HACKER,
    EMAIL_FLUIDATTACKS_REVIEWER,
    FindingCommentFaker,
    FindingFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    StakeholderFaker,
    random_uuid,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize

FINDING_ID = random_uuid()


@parametrize(
    args=["user_email"],
    cases=[
        [EMAIL_FLUIDATTACKS_HACKER],
        [EMAIL_FLUIDATTACKS_REVIEWER],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(),
            ],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_HACKER,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_REVIEWER,
                    state=GroupAccessStateFaker(has_access=True, role="reviewer"),
                ),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_HACKER),
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_REVIEWER),
            ],
            findings=[FindingFaker(id=FINDING_ID)],
            finding_comments=[
                FindingCommentFaker(
                    finding_id=FINDING_ID,
                    id="4345534341",
                    comment_type=CommentType.OBSERVATION,
                    content="This is a test observation",
                    email=EMAIL_FLUIDATTACKS_ADMIN,
                    full_name="test one",
                    creation_date=datetime.fromisoformat("2019-11-11T11:11:11+00:00"),
                    parent_id="0",
                ),
                FindingCommentFaker(
                    finding_id=FINDING_ID,
                    id="42343431",
                    comment_type=CommentType.COMMENT,
                    content="This is a test comment",
                    email=EMAIL_FLUIDATTACKS_ADMIN,
                    full_name="test one",
                    creation_date=datetime.fromisoformat("2019-05-28T20:09:37+00:00"),
                    parent_id="0",
                ),
            ],
        ),
    )
)
async def test_finding_consulting(user_email: str) -> None:
    finding = FindingFaker(id=FINDING_ID)
    info = GraphQLResolveInfoFaker(
        user_email=user_email,
        decorators=[
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", finding.group_name],
        ],
    )
    result = await resolve(
        parent=finding,
        info=info,
        group_name=finding.group_name,
    )
    assert len(result) == 1
    assert result[0] == {
        "content": "This is a test comment",
        "created": "2019/05/28 15:09:37",
        "email": EMAIL_FLUIDATTACKS_ADMIN,
        "fullname": "test one",
        "id": "42343431",
        "modified": "2019/05/28 15:09:37",
        "parent": "0",
    }
