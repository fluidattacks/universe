from integrates.api.resolvers.finding.organization_name import resolve
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    EMAIL_FLUIDATTACKS_HACKER,
    FindingFaker,
    GraphQLResolveInfoFaker,
    GroupFaker,
    OrganizationFaker,
    random_uuid,
)
from integrates.testing.mocks import mocks

FINDING_ID = random_uuid()
ORG_ID = random_uuid()


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(organization_id=ORG_ID)],
            organizations=[OrganizationFaker(name="test-org", id=ORG_ID)],
            findings=[FindingFaker(id=FINDING_ID)],
        ),
    )
)
async def test_finding_organization_name() -> None:
    info = GraphQLResolveInfoFaker(user_email=EMAIL_FLUIDATTACKS_HACKER)
    parent = FindingFaker(id=FINDING_ID)
    result = await resolve(
        parent=parent,
        info=info,
    )
    assert result == "test-org"
