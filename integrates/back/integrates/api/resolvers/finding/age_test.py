from datetime import datetime

from integrates.api.resolvers.finding.age import resolve
from integrates.testing.fakers import (
    EMAIL_FLUIDATTACKS_ADMIN,
    FindingFaker,
    FindingUnreliableIndicatorsFaker,
    GraphQLResolveInfoFaker,
)
from integrates.testing.utils import freeze_time, parametrize


@freeze_time("2024-01-30T00:00:00+00:00")
@parametrize(
    args=["oldest_vulnerability_report_date", "expected_output"],
    cases=[
        [datetime.fromisoformat("2024-01-30T00:00:00+00:00"), 0],
        [datetime.fromisoformat("2024-01-29T00:00:00+00:00"), 1],
        [datetime.fromisoformat("2024-01-01T00:00:00+00:00"), 29],
        [datetime.fromisoformat("2023-01-30T00:00:00+00:00"), 365],
    ],
)
def test_finding_age(oldest_vulnerability_report_date: datetime, expected_output: int) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_FLUIDATTACKS_ADMIN,
    )
    parent = FindingFaker(
        unreliable_indicators=FindingUnreliableIndicatorsFaker(
            oldest_vulnerability_report_date=oldest_vulnerability_report_date
        ),
    )
    result = resolve(
        parent=parent,
        _info=info,
    )
    assert result == expected_output
