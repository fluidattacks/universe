from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.findings.types import (
    Finding,
)

from .schema import (
    FINDING,
)


@FINDING.field("treatmentSummary")
def resolve(parent: Finding, _info: GraphQLResolveInfo, **_kwargs: None) -> dict[str, int]:
    return parent.unreliable_indicators.treatment_summary._asdict()
