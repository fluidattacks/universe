from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.api.resolvers.types import (
    Requirement,
)
from integrates.custom_utils.criteria import (
    CRITERIA_REQUIREMENTS,
)
from integrates.db_model.findings.types import (
    Finding,
)

from .schema import (
    FINDING,
)


@FINDING.field("unfulfilledRequirements")
def resolve(
    parent: Finding,
    _info: GraphQLResolveInfo,
    **_kwargs: None,
) -> list[Requirement]:
    return [
        Requirement(
            id=requirement_id,
            summary=CRITERIA_REQUIREMENTS[requirement_id]["en"]["summary"],
            title=CRITERIA_REQUIREMENTS[requirement_id]["en"]["title"],
        )
        for requirement_id in parent.unfulfilled_requirements
        if requirement_id in CRITERIA_REQUIREMENTS
    ]
