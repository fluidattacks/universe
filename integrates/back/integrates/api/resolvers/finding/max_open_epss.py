from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.findings.types import (
    Finding,
)

from .schema import (
    FINDING,
)


@FINDING.field("maxOpenEPSS")
def resolve(parent: Finding, _info: GraphQLResolveInfo, **_kwargs: None) -> int:
    return parent.unreliable_indicators.max_open_epss
