from typing import (
    Any,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.db_model.vulnerabilities.types import (
    FindingVulnerabilitiesRequest,
    VulnerabilitiesConnection,
)

from .schema import (
    FINDING,
)


@FINDING.field("vulnerabilitiesToReattackConnection")
async def resolve(
    parent: Finding,
    info: GraphQLResolveInfo,
    **kwargs: Any,
) -> VulnerabilitiesConnection:
    loaders: Dataloaders = info.context.loaders

    return await loaders.finding_vulnerabilities_to_reattack_c.load(
        FindingVulnerabilitiesRequest(
            finding_id=parent.id,
            after=kwargs.get("after"),
            first=kwargs.get("first"),
            paginate=True,
        ),
    )
