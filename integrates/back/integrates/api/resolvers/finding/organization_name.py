from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_exceptions import (
    InvalidParameter,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.db_model.organizations.types import (
    Organization,
)
from integrates.organizations.utils import (
    get_organization,
)

from .schema import (
    FINDING,
)


@FINDING.field("organizationName")
async def resolve(
    parent: Finding,
    info: GraphQLResolveInfo,
) -> str:
    loaders: Dataloaders = info.context.loaders
    group = await loaders.group.load(parent.group_name)
    if group is None:
        raise InvalidParameter()
    organization: Organization = await get_organization(loaders, group.organization_id)
    return organization.name
