from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.db_model.findings.types import (
    Finding,
)

from .schema import (
    FINDING,
)


@FINDING.field("lastStateDate")
def resolve(
    parent: Finding,
    _info: GraphQLResolveInfo,
    **_kwargs: None,
) -> str:
    return datetime_utils.get_as_str(parent.state.modified_date)
