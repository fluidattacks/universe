from datetime import datetime

from integrates.api.resolvers.finding.evidence import resolve
from integrates.db_model.finding_comments.enums import CommentType
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    EMAIL_FLUIDATTACKS_ADMIN,
    FindingCommentFaker,
    FindingEvidencesFaker,
    FindingFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    StakeholderFaker,
    random_uuid,
)
from integrates.testing.mocks import mocks

FINDING_ID = random_uuid()


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker()],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_ADMIN,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
            ],
            stakeholders=[StakeholderFaker(email=EMAIL_FLUIDATTACKS_ADMIN)],
            findings=[FindingFaker(id=FINDING_ID)],
            finding_comments=[
                FindingCommentFaker(
                    finding_id=FINDING_ID,
                    id="4345534341",
                    comment_type=CommentType.OBSERVATION,
                    content="This is a test observation",
                    email=EMAIL_FLUIDATTACKS_ADMIN,
                    full_name="test one",
                    creation_date=datetime.fromisoformat("2019-11-11T11:11:11+00:00"),
                    parent_id="0",
                ),
                FindingCommentFaker(
                    finding_id=FINDING_ID,
                    id="42343431",
                    comment_type=CommentType.COMMENT,
                    content="This is a test comment",
                    email=EMAIL_FLUIDATTACKS_ADMIN,
                    full_name="test one",
                    creation_date=datetime.fromisoformat("2019-05-28T20:09:37+00:00"),
                    parent_id="0",
                ),
            ],
        ),
    )
)
async def test_finding_evidence() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_FLUIDATTACKS_ADMIN,
    )
    parent = FindingFaker(evidences=FindingEvidencesFaker())
    result = await resolve(
        parent=parent,
        info=info,
    )
    assert result == {
        "animation": {
            "author_email": "hacker@fluidattacks.com",
            "date": "2019-04-10 07:59:52",
            "description": "animation",
            "is_draft": False,
            "url": "group_name-finding_id-evidence_id",
        },
        "evidence_1": {
            "author_email": "hacker@fluidattacks.com",
            "date": "2019-04-10 07:59:52",
            "description": "evidence1",
            "is_draft": False,
            "url": "group_name-finding_id-evidence_id",
        },
        "evidence_2": {
            "author_email": "hacker@fluidattacks.com",
            "date": "2019-04-10 07:59:52",
            "description": "evidence2",
            "is_draft": False,
            "url": "group_name-finding_id-evidence_id",
        },
        "evidence_3": {
            "author_email": "hacker@fluidattacks.com",
            "date": "2019-04-10 07:59:52",
            "description": "evidence3",
            "is_draft": False,
            "url": "group_name-finding_id-evidence_id",
        },
        "evidence_4": {
            "author_email": "hacker@fluidattacks.com",
            "date": "2019-04-10 07:59:52",
            "description": "evidence4",
            "is_draft": False,
            "url": "group_name-finding_id-evidence_id",
        },
        "evidence_5": {
            "author_email": "hacker@fluidattacks.com",
            "date": "2019-04-10 07:59:52",
            "description": "evidence5",
            "is_draft": False,
            "url": "group_name-finding_id-evidence_id",
        },
        "exploitation": {
            "author_email": "hacker@fluidattacks.com",
            "date": "2019-04-10 07:59:52",
            "description": "exploitation",
            "is_draft": False,
            "url": "group_name-finding_id-evidence_id",
        },
    }
