from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils.findings import (
    get_formatted_evidence,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.findings.domain.evidence import (
    filter_drafts,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .schema import (
    FINDING,
)


@FINDING.field("evidence")
async def resolve(
    parent: Finding,
    info: GraphQLResolveInfo,
    **_kwargs: None,
) -> dict[str, dict[str, str]]:
    user_data = await sessions_domain.get_jwt_content(info.context)

    return await filter_drafts(
        email=user_data["user_email"],
        evidences=get_formatted_evidence(parent),
        group_name=parent.group_name,
        loaders=info.context.loaders,
    )
