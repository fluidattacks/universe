from graphql.type.definition import GraphQLResolveInfo
from opensearchpy.helpers import query

from integrates.db_model.findings.types import Finding
from integrates.search.operations import get_unique_terms

from .schema import FINDING


@FINDING.field("locations")
async def resolve(
    parent: Finding,
    _info: GraphQLResolveInfo,
    **_kwargs: None,
) -> list[str]:
    results = await get_unique_terms(
        index="vulns_index",
        field="state.where.keyword",
        conditions=query.Bool(
            must=[query.Terms(state__status__keyword=["VULNERABLE", "SAFE"])],
            must_not=[query.Terms(zero_risk__status__keyword=["REQUESTED", "CONFIRMED"])],
            filter=[
                query.Match(group_name={"query": parent.group_name, "operator": "and"}),
                query.Match(sk={"query": f"FIN#{parent.id}", "operator": "and"}),
            ],
        ),
    )

    return results
