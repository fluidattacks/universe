from decimal import (
    Decimal,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.findings.types import (
    Finding,
)

from .schema import (
    FINDING,
)


@FINDING.field("maxOpenSeverityScore")
def resolve(parent: Finding, _info: GraphQLResolveInfo, **_kwargs: None) -> Decimal:
    return parent.unreliable_indicators.max_open_severity_score
