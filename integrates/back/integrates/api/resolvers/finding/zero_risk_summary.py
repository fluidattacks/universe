from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.findings.types import (
    Finding,
    FindingZeroRiskSummary,
)
from integrates.decorators import (
    enforce_group_level_auth_async,
)

from .schema import (
    FINDING,
)


@FINDING.field("zeroRiskSummary")
@enforce_group_level_auth_async
def resolve(parent: Finding, _info: GraphQLResolveInfo, **_kwargs: None) -> FindingZeroRiskSummary:
    return parent.unreliable_indicators.unreliable_zero_risk_summary
