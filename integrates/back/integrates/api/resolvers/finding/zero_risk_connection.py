from typing import Any, cast

from graphql.type.definition import GraphQLResolveInfo

from integrates.api.inputs.core import VulnerabilityFiltersInput
from integrates.db_model.findings.types import Finding
from integrates.db_model.vulnerabilities.types import VulnerabilitiesConnection
from integrates.db_model.vulnerabilities.utils import get_vulns_from_opensearch
from integrates.decorators import enforce_group_level_auth_async

from .schema import FINDING


@FINDING.field("zeroRiskConnection")
@enforce_group_level_auth_async
async def resolve(
    parent: Finding,
    _info: GraphQLResolveInfo,
    **kwargs: Any,
) -> VulnerabilitiesConnection:
    after: str | None = kwargs.get("after")
    first: int = kwargs.get("first") or 10
    filters = cast(VulnerabilityFiltersInput, kwargs)
    sort_by = kwargs.get("sort_by")

    return await get_vulns_from_opensearch(
        group_name=parent.group_name,
        finding_id=parent.id,
        after=after,
        first=first,
        sort_by=sort_by,
        filters={
            **filters,
            "state": ["SAFE", "VULNERABLE"],
            "zero_risk": ["REQUESTED", "CONFIRMED"],
        },
    )
