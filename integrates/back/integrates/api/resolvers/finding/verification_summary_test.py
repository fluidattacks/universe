from integrates.api.resolvers.finding.verification_summary import resolve
from integrates.db_model.findings.types import (
    FindingVerificationSummary,
)
from integrates.testing.fakers import (
    EMAIL_FLUIDATTACKS_HACKER,
    FindingFaker,
    FindingUnreliableIndicatorsFaker,
    GraphQLResolveInfoFaker,
    random_uuid,
)

FINDING_ID = random_uuid()


def test_finding_verification_summary() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_FLUIDATTACKS_HACKER,
    )
    parent = FindingFaker(
        id=FINDING_ID,
        unreliable_indicators=FindingUnreliableIndicatorsFaker(
            verification_summary=FindingVerificationSummary(
                on_hold=1,
                requested=2,
                verified=3,
            )
        ),
    )
    result = resolve(
        parent=parent,
        _info=info,
    )
    assert result == {
        "on_hold": 1,
        "requested": 2,
        "verified": 3,
    }
