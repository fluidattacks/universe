from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils import (
    findings as findings_utils,
)
from integrates.db_model.findings.types import (
    Finding,
)

from .schema import (
    FINDING,
)


@FINDING.field("verified")
def resolve(parent: Finding, _info: GraphQLResolveInfo, **_kwargs: None) -> bool:
    return findings_utils.is_verified(parent.unreliable_indicators.verification_summary)
