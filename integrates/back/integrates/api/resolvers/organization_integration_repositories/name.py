from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.integration_repositories.types import (
    OrganizationIntegrationRepository,
)

from .schema import (
    ORGANIZATION_INTEGRATION_REPOSITORIES,
)


@ORGANIZATION_INTEGRATION_REPOSITORIES.field("name")
def resolve(
    parent: OrganizationIntegrationRepository,
    _info: GraphQLResolveInfo,
) -> str:
    return parent.name if parent.name is not None else ""
