from urllib.parse import (
    unquote_plus,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.integration_repositories.types import (
    OrganizationIntegrationRepository,
)

from .schema import (
    ORGANIZATION_INTEGRATION_REPOSITORIES,
)


@ORGANIZATION_INTEGRATION_REPOSITORIES.field("url")
def resolve(
    parent: OrganizationIntegrationRepository,
    _info: GraphQLResolveInfo,
) -> str:
    return unquote_plus(parent.url)
