from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.roots.types import (
    RootDockerImage,
)

from .schema import (
    ROOT_DOCKER_IMAGE,
)


@ROOT_DOCKER_IMAGE.field("include")
def resolve(parent: RootDockerImage, _info: GraphQLResolveInfo, **__: None) -> bool:
    return parent.state.include
