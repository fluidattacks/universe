from datetime import (
    datetime,
)
from typing import (
    NamedTuple,
)


class GitRootCloningStatus(NamedTuple):
    group_name: str
    message: str
    modified_date: datetime
    root_nickname: str
    status: str
    commit: str | None = None
