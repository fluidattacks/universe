from datetime import (
    datetime,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.toe_inputs.types import (
    ToeInput,
)

from .schema import (
    TOE_INPUT,
)


@TOE_INPUT.field("seenAt")
def resolve(parent: ToeInput, _info: GraphQLResolveInfo, **_kwargs: None) -> datetime | None:
    return parent.state.seen_at
