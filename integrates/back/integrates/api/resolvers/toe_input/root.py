from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.roots.types import (
    Root,
    RootRequest,
)
from integrates.db_model.toe_inputs.types import (
    ToeInput,
)

from .schema import (
    TOE_INPUT,
)


@TOE_INPUT.field("root")
async def resolve(
    parent: ToeInput,
    info: GraphQLResolveInfo,
    **_kwargs: None,
) -> Root | None:
    loaders: Dataloaders = info.context.loaders
    if parent.root_id:
        root = await loaders.root.load(RootRequest(parent.group_name, parent.root_id))

        return root

    return None
