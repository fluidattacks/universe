from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.mailmap.types import (
    MailmapEntryWithSubentries,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_organization_level_auth_async,
    require_login,
)
from integrates.mailmap import (
    get_mailmap_entry_with_subentries,
)

from .schema import (
    QUERY,
)


@QUERY.field("mailmapEntryWithSubentries")
@concurrent_decorators(
    require_login,
    enforce_organization_level_auth_async,
)
async def resolve(
    _parent: None,
    _info: GraphQLResolveInfo,
    entry_email: str,
    organization_id: str,
) -> MailmapEntryWithSubentries:
    entry_with_subentries = await get_mailmap_entry_with_subentries(
        entry_email=entry_email,
        organization_id=organization_id,
    )
    return entry_with_subentries
