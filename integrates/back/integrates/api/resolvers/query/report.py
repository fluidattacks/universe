from typing import (
    Any,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.class_types.types import (
    Item,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_login,
)
from integrates.reports.request_report import (
    request_report,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .schema import (
    QUERY,
)


@QUERY.field("report")
@concurrent_decorators(require_login, enforce_group_level_auth_async, require_is_not_under_review)
async def resolve(
    _parent: None,
    info: GraphQLResolveInfo,
    group_name: str,
    verification_code: str,
    **kwargs: Any,
) -> Item:
    user_info: dict[str, str] = await sessions_domain.get_jwt_content(info.context)
    user_email: str = user_info["user_email"]
    loaders: Dataloaders = info.context.loaders
    request_result = await request_report(
        loaders=loaders,
        user_email=user_email,
        group_name=group_name,
        verification_code=verification_code,
        **kwargs,
    )

    return {"success": request_result}
