from decimal import Decimal
from typing import Any

from integrates.api.resolvers.query.unfulfilled_standard_report_url import resolve
from integrates.custom_exceptions import InvalidParameter, InvalidStandardId, RequiredNewPhoneNumber
from integrates.db_model.groups.types import (
    GroupUnreliableIndicators,
    UnfulfilledStandard,
)
from integrates.db_model.organizations.types import (
    OrganizationStandardCompliance,
    OrganizationUnreliableIndicators,
)
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute_internal,
    require_is_not_under_review,
    require_login,
)
from integrates.reports.enums import (
    ReportType,
)
from integrates.testing.aws import (
    GroupUnreliableIndicatorsToUpdate,
    IntegratesAws,
    IntegratesDynamodb,
    OrganizationUnreliableIndicatorsToUpdate,
)
from integrates.testing.fakers import (
    EMAIL_FLUIDATTACKS_GROUP_MANAGER,
    EMAIL_FLUIDATTACKS_SERVICE_FORCES,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationAccessFaker,
    OrganizationAccessStateFaker,
    OrganizationFaker,
    StakeholderFaker,
    random_uuid,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

GROUP_NAME = "test-group"
ORG_ID = random_uuid()
DEFAULT_DECORATORS: list[list[Any]] = [
    [require_login],
    [require_is_not_under_review],
    [enforce_group_level_auth_async],
    [require_attribute_internal, "is_under_review", GROUP_NAME],
]


@parametrize(
    args=["email", "report_type", "unfulfilled_standards", "expected_output"],
    cases=[
        [
            EMAIL_FLUIDATTACKS_GROUP_MANAGER,
            ReportType.PDF,
            ["bsimm"],
            "/reports/pdf/",
        ],
        [
            EMAIL_FLUIDATTACKS_GROUP_MANAGER,
            ReportType.CSV,
            ["bsimm"],
            f"unfulfilled-standards-{GROUP_NAME}",
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(
                    name=GROUP_NAME,
                    organization_id=ORG_ID,
                ),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=EMAIL_FLUIDATTACKS_GROUP_MANAGER,
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            group_unreliable_indicators=[
                GroupUnreliableIndicatorsToUpdate(
                    group_name=GROUP_NAME,
                    indicators=GroupUnreliableIndicators(
                        unfulfilled_standards=[
                            UnfulfilledStandard(
                                name="bsimm",
                                unfulfilled_requirements=["155", "159", "273"],
                            )
                        ],
                    ),
                )
            ],
            organizations=[
                OrganizationFaker(id=ORG_ID),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=EMAIL_FLUIDATTACKS_GROUP_MANAGER,
                    state=OrganizationAccessStateFaker(
                        has_access=True,
                        role="group_manager",
                    ),
                )
            ],
            organization_unreliable_indicators=[
                OrganizationUnreliableIndicatorsToUpdate(
                    organization_id=ORG_ID,
                    organization_name="test-org",
                    indicators=OrganizationUnreliableIndicators(
                        compliance_level=Decimal("0.8"),
                        compliance_weekly_trend=Decimal("0.02"),
                        estimated_days_to_full_compliance=Decimal("3"),
                        standard_compliances=[
                            OrganizationStandardCompliance(
                                standard_name="bsimm",
                                compliance_level=Decimal("0.3"),
                            )
                        ],
                    ),
                )
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_GROUP_MANAGER),
            ],
        ),
    )
)
async def test_unfulfilled_standard_report_url(
    email: str, report_type: ReportType, unfulfilled_standards: list[str], expected_output: str
) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=DEFAULT_DECORATORS,
    )
    result = await resolve(
        _parent=None,
        info=info,
        group_name=GROUP_NAME,
        verification_code="123",
        unfulfilled_standards=unfulfilled_standards,
        report_type=report_type.value,
    )
    assert expected_output in result


@parametrize(
    args=["email", "report_type", "unfulfilled_standards"],
    cases=[
        [
            EMAIL_FLUIDATTACKS_GROUP_MANAGER,
            ReportType.PDF,
            [],
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(
                    name=GROUP_NAME,
                    organization_id=ORG_ID,
                ),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=EMAIL_FLUIDATTACKS_GROUP_MANAGER,
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            organizations=[
                OrganizationFaker(id=ORG_ID),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=EMAIL_FLUIDATTACKS_GROUP_MANAGER,
                    state=OrganizationAccessStateFaker(
                        has_access=True,
                        role="group_manager",
                    ),
                )
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_GROUP_MANAGER),
            ],
        ),
    )
)
async def test_invalid_unfulfilled_standards(
    email: str, report_type: ReportType, unfulfilled_standards: list[str]
) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=DEFAULT_DECORATORS,
    )
    with raises(InvalidParameter):
        await resolve(
            _parent=None,
            info=info,
            group_name=GROUP_NAME,
            verification_code="123",
            unfulfilled_standards=unfulfilled_standards,
            report_type=report_type.value,
        )


@parametrize(
    args=["email", "report_type", "unfulfilled_standards"],
    cases=[
        [
            EMAIL_FLUIDATTACKS_GROUP_MANAGER,
            ReportType.PDF,
            ["test_standard_error"],
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(
                    name=GROUP_NAME,
                    organization_id=ORG_ID,
                ),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=EMAIL_FLUIDATTACKS_GROUP_MANAGER,
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            organizations=[
                OrganizationFaker(id=ORG_ID),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=EMAIL_FLUIDATTACKS_GROUP_MANAGER,
                    state=OrganizationAccessStateFaker(
                        has_access=True,
                        role="group_manager",
                    ),
                )
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_GROUP_MANAGER),
            ],
        ),
    )
)
async def test_invalid_standard_id(
    email: str, report_type: ReportType, unfulfilled_standards: list[str]
) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=DEFAULT_DECORATORS,
    )
    with raises(InvalidStandardId):
        await resolve(
            _parent=None,
            info=info,
            group_name=GROUP_NAME,
            verification_code="123",
            unfulfilled_standards=unfulfilled_standards,
            report_type=report_type.value,
        )


@parametrize(
    args=["email", "report_type", "unfulfilled_standards"],
    cases=[
        [
            EMAIL_FLUIDATTACKS_GROUP_MANAGER,
            ReportType.PDF,
            [],
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(
                    name=GROUP_NAME,
                    organization_id=ORG_ID,
                ),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=EMAIL_FLUIDATTACKS_GROUP_MANAGER,
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            organizations=[
                OrganizationFaker(id=ORG_ID),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=EMAIL_FLUIDATTACKS_GROUP_MANAGER,
                    state=OrganizationAccessStateFaker(
                        has_access=True,
                        role="group_manager",
                    ),
                )
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_GROUP_MANAGER, phone=None),
            ],
        ),
    )
)
async def test_get_unfulfilled_standard_report_url_number_required(
    email: str, report_type: ReportType, unfulfilled_standards: list[str]
) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=DEFAULT_DECORATORS,
    )
    with raises(RequiredNewPhoneNumber):
        await resolve(
            _parent=None,
            info=info,
            group_name=GROUP_NAME,
            verification_code="123",
            unfulfilled_standards=unfulfilled_standards,
            report_type=report_type.value,
        )


@parametrize(
    args=["email", "report_type", "unfulfilled_standards"],
    cases=[
        [
            EMAIL_FLUIDATTACKS_SERVICE_FORCES,
            ReportType.PDF,
            [],
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(
                    name=GROUP_NAME,
                    organization_id=ORG_ID,
                ),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=EMAIL_FLUIDATTACKS_GROUP_MANAGER,
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            organizations=[
                OrganizationFaker(id=ORG_ID),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=EMAIL_FLUIDATTACKS_GROUP_MANAGER,
                    state=OrganizationAccessStateFaker(
                        has_access=True,
                        role="group_manager",
                    ),
                )
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_GROUP_MANAGER, phone=None),
            ],
        ),
    )
)
async def test_get_unfulfilled_standard_report_url_fail(
    email: str, report_type: ReportType, unfulfilled_standards: list[str]
) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=DEFAULT_DECORATORS,
    )
    with raises(Exception, match="Access denied"):
        await resolve(
            _parent=None,
            info=info,
            group_name=GROUP_NAME,
            verification_code="123",
            unfulfilled_standards=unfulfilled_standards,
            report_type=report_type.value,
        )
