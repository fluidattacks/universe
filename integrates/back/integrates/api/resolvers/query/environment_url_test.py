from typing import Any

from integrates.api.resolvers.query.environment_url import resolve
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute_internal,
    require_is_not_under_review,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GitRootFaker,
    GraphQLResolveInfoFaker,
    RootEnvironmentUrlFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks

ADMIN = "admin@gmail.com"
URL_ID = "url1"
GIT_ROOT_ID = "root1"
GROUP_NAME = "group1"

DEFAULT_DECORATORS: list[list[Any]] = [
    [require_login],
    [require_is_not_under_review],
    [enforce_group_level_auth_async],
    [require_attribute_internal, "is_under_review", GROUP_NAME],
]


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[StakeholderFaker(email=ADMIN, role="admin")],
            roots=[GitRootFaker(id=GIT_ROOT_ID)],
            root_environment_urls=[
                RootEnvironmentUrlFaker(root_id=GIT_ROOT_ID, id=URL_ID, group_name=GROUP_NAME)
            ],
        )
    ),
)
async def test_resolve_git_environment_url() -> None:
    info = GraphQLResolveInfoFaker(user_email=ADMIN, decorators=DEFAULT_DECORATORS)
    kwargs = {"group_name": GROUP_NAME}
    url = await resolve(None, info, url_id=URL_ID, **kwargs)
    assert url
