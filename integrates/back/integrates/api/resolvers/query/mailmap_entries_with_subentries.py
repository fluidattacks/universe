from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.mailmap.types import (
    MailmapEntryWithSubentries,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_organization_level_auth_async,
    require_login,
)
from integrates.mailmap import (
    get_mailmap_entries_with_subentries,
)

from .schema import (
    QUERY,
)


@QUERY.field("mailmapEntriesWithSubentries")
@concurrent_decorators(
    require_login,
    enforce_organization_level_auth_async,
)
async def resolve(
    _parent: None,
    _info: GraphQLResolveInfo,
    organization_id: str,
) -> list[MailmapEntryWithSubentries]:
    entries_with_subentries = await get_mailmap_entries_with_subentries(
        organization_id=organization_id,
    )
    return entries_with_subentries
