from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.roots.get import (
    get_git_environment_url_by_id,
)
from integrates.db_model.roots.types import (
    RootEnvironmentUrl,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_login,
)

from .schema import (
    QUERY,
)


@QUERY.field("environmentUrl")
@concurrent_decorators(
    require_login,
    require_is_not_under_review,
    enforce_group_level_auth_async,
)
async def resolve(
    _parent: None,
    _info: GraphQLResolveInfo,
    url_id: str,
    **kwargs: str,
) -> RootEnvironmentUrl | None:
    group_name = str(kwargs["group_name"]).lower()
    url = await get_git_environment_url_by_id(url_id=url_id, group_name=group_name)
    return url
