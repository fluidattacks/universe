from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.class_types.types import (
    Item,
)
from integrates.decorators import (
    require_login,
)

from .schema import (
    QUERY,
)


@QUERY.field("billing")
@require_login
def resolve(_parent: None, _info: GraphQLResolveInfo, **_kwargs: str) -> Item:
    return {}
