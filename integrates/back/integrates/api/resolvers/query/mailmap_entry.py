from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.mailmap.types import (
    MailmapEntry,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_organization_level_auth_async,
    require_login,
)
from integrates.mailmap import (
    get_mailmap_entry,
)

from .schema import (
    QUERY,
)


@QUERY.field("mailmapEntry")
@concurrent_decorators(
    require_login,
    enforce_organization_level_auth_async,
)
async def resolve(
    _parent: None,
    _info: GraphQLResolveInfo,
    entry_email: str,
    organization_id: str,
) -> MailmapEntry:
    entry = await get_mailmap_entry(
        entry_email=entry_email,
        organization_id=organization_id,
    )
    return entry
