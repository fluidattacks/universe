from integrates.api.resolvers.query.resources import resolve
from integrates.db_model.groups.types import GroupFile
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_asm,
    require_attribute,
    require_attribute_internal,
    require_is_not_under_review,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize

ORG_ID = "ORG#40f6da5f-4f21-4168-b8fa-9f2ce3d4dd93"


@parametrize(
    args=["email"],
    cases=[
        ["admin@gmail.com"],
        ["hacker@gmail.com"],
        ["user@gmail.com"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(
                    organization_id=ORG_ID,
                    files=[
                        GroupFile(
                            description="Test file 1",
                            file_name="test1.pdf",
                            modified_by="admin@gmail.com",
                            modified_date=None,
                        ),
                        GroupFile(
                            description="Test file 2",
                            file_name="test2.pdf",
                            modified_by="user@gmail.com",
                            modified_date=None,
                        ),
                    ],
                ),
            ],
            stakeholders=[
                StakeholderFaker(email="admin@gmail.com"),
                StakeholderFaker(email="hacker@gmail.com"),
                StakeholderFaker(email="user@gmail.com"),
            ],
            group_access=[
                GroupAccessFaker(
                    email="admin@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
                GroupAccessFaker(
                    email="hacker@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
                GroupAccessFaker(
                    email="user@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                ),
            ],
            organizations=[
                OrganizationFaker(id=ORG_ID),
            ],
        ),
    )
)
async def test_get_resources_with_files(email: str) -> None:
    group_name = "test-group"
    expected_resources = {
        "files": [
            {
                "description": "Test file 1",
                "file_name": "test1.pdf",
                "uploader": "admin@gmail.com",
                "upload_date": None,
            },
            {
                "description": "Test file 2",
                "file_name": "test2.pdf",
                "uploader": "user@gmail.com",
                "upload_date": None,
            },
        ],
        "group_name": group_name,
    }

    resources = await resolve(
        None,
        info=GraphQLResolveInfoFaker(
            user_email=email,
            decorators=[
                [require_login],
                [enforce_group_level_auth_async],
                [require_is_not_under_review],
                [require_attribute_internal, "is_under_review", group_name],
                [require_asm],
                [require_attribute, "has_asm", group_name],
            ],
        ),
        group_name=group_name,
    )

    assert resources == expected_resources


@parametrize(
    args=["email"],
    cases=[
        ["admin@gmail.com"],
        ["hacker@gmail.com"],
        ["user@gmail.com"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(organization_id=ORG_ID, files=[]),
            ],
            stakeholders=[
                StakeholderFaker(email="admin@gmail.com"),
                StakeholderFaker(email="hacker@gmail.com"),
                StakeholderFaker(email="user@gmail.com"),
            ],
            group_access=[
                GroupAccessFaker(
                    email="admin@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
                GroupAccessFaker(
                    email="hacker@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
                GroupAccessFaker(
                    email="user@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                ),
            ],
            organizations=[
                OrganizationFaker(id=ORG_ID),
            ],
        ),
    )
)
async def test_get_resources_without_files(email: str) -> None:
    group_name = "test-group"
    expected_resources = {"files": None, "group_name": group_name}

    resources = await resolve(
        None,
        info=GraphQLResolveInfoFaker(
            user_email=email,
            decorators=[
                [require_login],
                [enforce_group_level_auth_async],
                [require_is_not_under_review],
                [require_attribute_internal, "is_under_review", group_name],
                [require_asm],
                [require_attribute, "has_asm", group_name],
            ],
        ),
        group_name=group_name,
    )

    assert resources == expected_resources
