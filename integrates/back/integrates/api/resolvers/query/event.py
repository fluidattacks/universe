from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils.events import (
    get_event,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.events.types import (
    Event,
    EventRequest,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    rename_kwargs,
    require_asm,
    require_is_not_under_review,
    require_login,
)

from .schema import (
    QUERY,
)


@QUERY.field("event")
@rename_kwargs({"identifier": "event_id"})
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_asm,
)
@rename_kwargs({"event_id": "identifier"})
async def resolve(_parent: None, info: GraphQLResolveInfo, group_name: str, **kwargs: str) -> Event:
    event_id: str = kwargs["identifier"]
    loaders: Dataloaders = info.context.loaders

    return await get_event(loaders, EventRequest(event_id=event_id, group_name=group_name.lower()))
