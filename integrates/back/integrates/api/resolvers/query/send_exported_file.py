from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.batch import (
    dal as batch_dal,
)
from integrates.batch.enums import (
    Action,
    IntegratesBatchQueue,
)
from integrates.class_types.types import (
    Item,
)
from integrates.custom_exceptions import (
    ReportAlreadyRequested,
    RequestedReportError,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_login,
)
from integrates.notifications.reports import (
    add_report_notification,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .schema import (
    QUERY,
)


@QUERY.field("sendExportedFile")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_is_not_under_review,
)
async def resolve(
    _parent: None,
    info: GraphQLResolveInfo,
    group_name: str,
) -> Item:
    user_info: dict[str, str] = await sessions_domain.get_jwt_content(info.context)
    user_email: str = user_info["user_email"]

    existing_actions = await batch_dal.get_actions_by_name(
        action_name=Action.SEND_EXPORTED_FILE,
        entity=group_name,
    )
    if list(
        filter(
            lambda x: x.subject.lower() == user_email.lower()
            and x.action_name == Action.SEND_EXPORTED_FILE,
            existing_actions,
        ),
    ):
        raise ReportAlreadyRequested()

    notification = await add_report_notification(
        report_format="CSV",
        report_name=f"DevSecOps report ({group_name})",
        user_email=user_email,
    )
    success: bool = (
        await batch_dal.put_action(
            action=Action.SEND_EXPORTED_FILE,
            entity=group_name,
            subject=user_email,
            additional_info={"notification_id": notification.id},
            attempt_duration_seconds=7200,
            queue=IntegratesBatchQueue.SMALL,
        )
    ).success
    if not success:
        raise RequestedReportError()
    return {"success": success}
