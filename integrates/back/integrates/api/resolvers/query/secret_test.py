from integrates.api.resolvers.query.secret import resolve
from integrates.db_model.roots.types import Secret
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute_internal,
    require_login,
)
from integrates.testing.aws import (
    IntegratesAws,
    IntegratesDynamodb,
    RootEnvironmentSecretsToUpdate,
    RootSecretsToUpdate,
)
from integrates.testing.fakers import (
    GitRootFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    IPRootFaker,
    OrganizationFaker,
    RootEnvironmentUrlFaker,
    SecretFaker,
    SecretStateFaker,
    StakeholderFaker,
    UrlRootFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises
from integrates.verify.enums import ResourceType

# Allowed
ADMIN = "admin@fluidattacks.com"
CUSTOMER_MANAGER = "customer_manager@fluidattacks.com"
RESOURCER = "resourcer@fluidattacks.com"
ARCHITECT = "architect@fluidattacks.com"
GROUP_MANAGER = "group_manager@fluidattacks.com"
USER = "user@fluidattacks.com"
HACKER = "hacker@fluidattacks.com"

GROUP_NAME = "group1"
ROOT_ID = "root1"
IP_ID = "root2"
URL_ID = "root3"


@parametrize(
    args=["email", "secret_key", "resource_id"],
    cases=[
        ["user@fluidattacks.com", "Test secret user", ROOT_ID],
        ["admin@fluidattacks.com", "Test secret admin", ROOT_ID],
        ["group_manager@fluidattacks.com", "Test secret user", ROOT_ID],
        ["resourcer@fluidattacks.com", "Test secret admin", ROOT_ID],
        ["hacker@fluidattacks.com", "Test secret user", ROOT_ID],
        ["customer_manager@fluidattacks.com", "Test secret admin", ROOT_ID],
        ["architect@fluidattacks.com", "Test secret user", ROOT_ID],
        ["user@fluidattacks.com", "Test secret user IPRoot", IP_ID],
        ["user@fluidattacks.com", "Test secret user URLRoot", URL_ID],
        ["admin@fluidattacks.com", "Test secret admin IPRoot", IP_ID],
        ["admin@fluidattacks.com", "Test secret admin URLRoot", URL_ID],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=ADMIN),
                StakeholderFaker(email=CUSTOMER_MANAGER),
                StakeholderFaker(email=RESOURCER),
                StakeholderFaker(email=GROUP_MANAGER),
                StakeholderFaker(email=USER),
                StakeholderFaker(email=HACKER),
                StakeholderFaker(email=ARCHITECT),
            ],
            group_access=[
                GroupAccessFaker(
                    email=ADMIN,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="admin"),
                ),
                GroupAccessFaker(
                    email=CUSTOMER_MANAGER,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="customer_manager"),
                ),
                GroupAccessFaker(
                    email=RESOURCER,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="resourcer"),
                ),
                GroupAccessFaker(
                    email=GROUP_MANAGER,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="group_manager"),
                ),
                GroupAccessFaker(
                    email=USER,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="user"),
                ),
                GroupAccessFaker(
                    email=HACKER,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="hacker"),
                ),
                GroupAccessFaker(
                    email=ARCHITECT,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="architect"),
                ),
            ],
            roots=[
                GitRootFaker(group_name=GROUP_NAME, id=ROOT_ID),
                IPRootFaker(group_name=GROUP_NAME, id=IP_ID),
                UrlRootFaker(group_name=GROUP_NAME, root_id=URL_ID),
            ],
            root_secrets=[
                RootSecretsToUpdate(
                    resource_id=ROOT_ID,
                    secret=SecretFaker(
                        key="Test secret admin",
                        value="Test value",
                        state=SecretStateFaker(description="Test description", owner=ADMIN),
                    ),
                ),
                RootSecretsToUpdate(
                    resource_id=ROOT_ID,
                    secret=SecretFaker(
                        key="Test secret user",
                        value="Test value",
                        state=SecretStateFaker(description="Test description", owner=USER),
                    ),
                ),
                RootSecretsToUpdate(
                    resource_id=IP_ID,
                    secret=SecretFaker(
                        key="Test secret admin IPRoot",
                        value="Test value",
                        state=SecretStateFaker(description="Test description", owner=ADMIN),
                    ),
                ),
                RootSecretsToUpdate(
                    resource_id=IP_ID,
                    secret=SecretFaker(
                        key="Test secret user IPRoot",
                        value="Test value",
                        state=SecretStateFaker(description="Test description", owner=USER),
                    ),
                ),
                RootSecretsToUpdate(
                    resource_id=URL_ID,
                    secret=SecretFaker(
                        key="Test secret admin URLRoot",
                        value="Test value",
                        state=SecretStateFaker(description="Test description", owner=ADMIN),
                    ),
                ),
                RootSecretsToUpdate(
                    resource_id=URL_ID,
                    secret=SecretFaker(
                        key="Test secret user URLRoot",
                        value="Test value",
                        state=SecretStateFaker(description="Test description", owner=USER),
                    ),
                ),
            ],
        ),
    ),
)
async def test_shoud_query_root_secret(email: str, secret_key: str, resource_id: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )

    secret: Secret = await resolve(
        _parent=None,
        info=info,
        group_name=GROUP_NAME,
        secret_key=secret_key,
        resource_id=resource_id,
        resource_type=ResourceType.ROOT,
    )

    assert secret
    assert secret.key == secret_key
    assert secret.value == "Test value"
    assert secret.state.description == "Test description"


@parametrize(
    args=["email", "secret_key"],
    cases=[
        ["user@fluidattacks.com", "Test secret user environment"],
        ["admin@fluidattacks.com", "Test secret admin environment"],
        ["group_manager@fluidattacks.com", "Test secret user environment"],
        ["resourcer@fluidattacks.com", "Test secret admin environment"],
        ["hacker@fluidattacks.com", "Test secret user environment"],
        ["customer_manager@fluidattacks.com", "Test secret admin environment"],
        ["architect@fluidattacks.com", "Test secret user environment"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=ADMIN),
                StakeholderFaker(email=CUSTOMER_MANAGER),
                StakeholderFaker(email=RESOURCER),
                StakeholderFaker(email=GROUP_MANAGER),
                StakeholderFaker(email=USER),
                StakeholderFaker(email=HACKER),
                StakeholderFaker(email=ARCHITECT),
            ],
            group_access=[
                GroupAccessFaker(
                    email=ADMIN,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="admin"),
                ),
                GroupAccessFaker(
                    email=CUSTOMER_MANAGER,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="customer_manager"),
                ),
                GroupAccessFaker(
                    email=RESOURCER,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="resourcer"),
                ),
                GroupAccessFaker(
                    email=GROUP_MANAGER,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="group_manager"),
                ),
                GroupAccessFaker(
                    email=USER,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="user"),
                ),
                GroupAccessFaker(
                    email=HACKER,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="hacker"),
                ),
                GroupAccessFaker(
                    email=ARCHITECT,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="architect"),
                ),
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(id="url1", root_id="root1", group_name=GROUP_NAME)
            ],
            root_environment_secrets=[
                RootEnvironmentSecretsToUpdate(
                    group_name=GROUP_NAME,
                    resource_id="url1",
                    secret=SecretFaker(
                        key="Test secret admin environment",
                        state=SecretStateFaker(
                            owner=RESOURCER,
                            description="Test description",
                        ),
                    ),
                ),
                RootEnvironmentSecretsToUpdate(
                    group_name=GROUP_NAME,
                    resource_id="url1",
                    secret=SecretFaker(
                        key="Test secret user environment",
                        state=SecretStateFaker(
                            owner=USER,
                            description="Test description",
                        ),
                    ),
                ),
            ],
        ),
    ),
)
async def test_shoud_query_environment_url_secret(email: str, secret_key: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )

    secret: Secret = await resolve(
        _parent=None,
        info=info,
        group_name=GROUP_NAME,
        secret_key=secret_key,
        resource_id="url1",
        resource_type=ResourceType.URL,
    )

    assert secret
    assert secret.key == secret_key
    assert secret.value == "value"
    assert secret.state.description == "Test description"


@parametrize(
    args=["email", "secret_key", "resource_id", "resource_type"],
    cases=[
        ["user@fluidattacks.com", "Test secret admin", "", ResourceType.ROOT],
        ["user@fluidattacks.com", "Test secret user environment", "", ResourceType.URL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=USER),
            ],
            roots=[
                GitRootFaker(group_name=GROUP_NAME, id=ROOT_ID),
            ],
            root_secrets=[
                RootSecretsToUpdate(
                    resource_id=ROOT_ID,
                    secret=SecretFaker(
                        key="Test secret admin",
                        value="Test value",
                        state=SecretStateFaker(description="Test description", owner=ADMIN),
                    ),
                )
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(id="url1", root_id=URL_ID, group_name=GROUP_NAME)
            ],
            root_environment_secrets=[
                RootEnvironmentSecretsToUpdate(
                    group_name=GROUP_NAME,
                    resource_id="url1",
                    secret=SecretFaker(
                        key="Test secret user environment",
                        state=SecretStateFaker(
                            owner=USER,
                            description="Test description",
                        ),
                    ),
                ),
            ],
        ),
    ),
)
async def test_shoud_fail_ownership_for_query_secret(
    email: str, secret_key: str, resource_id: str, resource_type: str
) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )

    with raises(Exception, match="Access denied"):
        await resolve(
            _parent=None,
            info=info,
            group_name=GROUP_NAME,
            secret_key=secret_key,
            resource_id=resource_id,
            resource_type=resource_type,
        )
