from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils.criteria import (
    CRITERIA_VULNERABILITIES,
)
from integrates.db_model.criteria.types import (
    CriteriaConnection,
    CriteriaEdge,
)
from integrates.db_model.criteria.utils import (
    format_vulnerabilities_criteria,
    prepare_vulns_info,
)
from integrates.decorators import (
    require_login,
)
from integrates.dynamodb.types import (
    PageInfo,
)

from .schema import (
    QUERY,
)


@QUERY.field("criteriaConnection")
@require_login
def resolve(
    _parent: None,
    _info: GraphQLResolveInfo,
    first: int | None,
    after: str,
) -> CriteriaConnection:
    vulns_info = CRITERIA_VULNERABILITIES
    (
        sliced_vulns,
        end_cursor,
        has_next_page,
    ) = prepare_vulns_info(first=first, after=after, vulns_info=vulns_info)
    edges = tuple(
        CriteriaEdge(
            cursor=key,
            node=format_vulnerabilities_criteria(finding_number=key, item=item),
        )
        for key, item in sliced_vulns.items()
    )

    return CriteriaConnection(
        edges=edges,
        page_info=PageInfo(
            end_cursor=end_cursor,
            has_next_page=has_next_page,
        ),
        total=len(sliced_vulns),
    )
