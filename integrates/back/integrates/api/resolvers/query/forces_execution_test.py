from typing import Any

from integrates.api.resolvers.query.forces_execution import resolve
from integrates.custom_exceptions import ExecutionNotFound
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_asm,
    require_attribute,
    require_attribute_internal,
    require_is_not_under_review,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    ForcesExecutionFaker,
    GraphQLResolveInfoFaker,
    GroupFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import raises

GROUP_NAME = "group"

DEFAULT_DECORATORS: list[list[Any]] = [
    [require_login],
    [enforce_group_level_auth_async],
    [require_is_not_under_review],
    [require_asm],
    [require_attribute_internal, "is_under_review", GROUP_NAME],
    [require_attribute, "has_asm", GROUP_NAME],
]

USER = "admin@gmail.com"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME)],
            stakeholders=[StakeholderFaker(email=USER, role="admin")],
            forces_execution=[ForcesExecutionFaker(id="exec1", group_name=GROUP_NAME)],
        )
    )
)
async def test_resolve_forces_execution() -> None:
    info = GraphQLResolveInfoFaker(user_email=USER, decorators=DEFAULT_DECORATORS)
    kwargs = {"execution_id": "exec1", "group_name": GROUP_NAME}
    result = await resolve(None, info=info, **kwargs)
    assert result


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME)],
            stakeholders=[StakeholderFaker(email=USER, role="admin")],
            forces_execution=[ForcesExecutionFaker(id="exec1", group_name=GROUP_NAME)],
        )
    )
)
async def test_resolve_forces_execution_not_found() -> None:
    info = GraphQLResolveInfoFaker(user_email=USER, decorators=DEFAULT_DECORATORS)
    kwargs = {"execution_id": "another_exect", "group_name": GROUP_NAME}
    with raises(ExecutionNotFound):
        await resolve(None, info=info, **kwargs)
