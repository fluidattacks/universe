from integrates.api.resolvers.query.list_user_groups import resolve
from integrates.decorators import (
    enforce_user_level_auth_async,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks

DEFAULT_DECORATORS = [
    [require_login],
    [enforce_user_level_auth_async],
]

ADMIN = "admin@gmail.com"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(name="group1"),
                GroupFaker(name="group2"),
                GroupFaker(name="group3"),
            ],
            stakeholders=[
                StakeholderFaker(email=ADMIN),
            ],
            group_access=[
                GroupAccessFaker(
                    email=ADMIN,
                    group_name="group1",
                ),
                GroupAccessFaker(
                    email=ADMIN,
                    group_name="group2",
                ),
                GroupAccessFaker(
                    email=ADMIN,
                    group_name="group3",
                ),
            ],
        )
    )
)
async def resolve_list_user_groups() -> None:
    kwargs = {"user_email": ADMIN}
    info = GraphQLResolveInfoFaker(user_email=ADMIN, decorators=DEFAULT_DECORATORS)
    groups = await resolve(None, info=info, **kwargs)
    assert groups
    assert len(groups) == 3
