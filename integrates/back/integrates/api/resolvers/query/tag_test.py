from integrates.api.resolvers.query.tag import resolve
from integrates.decorators import (
    require_login,
)
from integrates.schedulers.update_portfolios import update_portfolios
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    GroupStateFaker,
    OrganizationAccessFaker,
    OrganizationAccessStateFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1", name="testorg")],
            stakeholders=[StakeholderFaker(email="test@test.test", role="user")],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id="org1",
                    email="test@test.test",
                    state=OrganizationAccessStateFaker(role="user"),
                )
            ],
            group_access=[
                GroupAccessFaker(
                    group_name="group1",
                    email="test@test.test",
                    state=GroupAccessStateFaker(role="user"),
                ),
                GroupAccessFaker(
                    group_name="group2",
                    email="test@test.test",
                    state=GroupAccessStateFaker(role="user"),
                ),
                GroupAccessFaker(
                    group_name="group3",
                    email="test@test.test",
                    state=GroupAccessStateFaker(role="user"),
                ),
            ],
            groups=[
                GroupFaker(
                    name="group1", organization_id="org1", state=GroupStateFaker(tags={"test2"})
                ),
                GroupFaker(
                    name="group2", organization_id="org1", state=GroupStateFaker(tags={"test2"})
                ),
                GroupFaker(name="group3", organization_id="org1"),
            ],
        ),
    ),
)
async def test_tag_query() -> None:
    info = GraphQLResolveInfoFaker(user_email="test@test.test", decorators=[[require_login]])
    await update_portfolios()

    result = await resolve(_parent=None, info=info, organization_id="ORG#org1", tag="test2")

    assert result.id == "test2"
    assert result.groups == {"group1", "group2"}
    assert result.organization_name == "testorg"
