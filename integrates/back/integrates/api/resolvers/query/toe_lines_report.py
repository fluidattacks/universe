from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.batch import (
    dal as batch_dal,
)
from integrates.batch.enums import (
    Action,
    IntegratesBatchQueue,
)
from integrates.class_types.types import (
    Item,
)
from integrates.custom_exceptions import (
    ReportAlreadyRequested,
    RequestedReportError,
    RequiredNewPhoneNumber,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_login,
)
from integrates.notifications.reports import (
    add_report_notification,
)
from integrates.reports.request_report import (
    filter_unique_report,
)
from integrates.sessions.domain import (
    get_jwt_content,
)
from integrates.stakeholders.utils import (
    get_international_format_phone_number,
)
from integrates.verify.operations import (
    check_verification,
)

from .schema import (
    QUERY,
)


@QUERY.field("toeLinesReport")
@concurrent_decorators(
    require_login,
    require_is_not_under_review,
    enforce_group_level_auth_async,
)
async def resolve(
    _parent: None,
    info: GraphQLResolveInfo,
    group_name: str,
    verification_code: str,
    **_kwargs: None,
) -> Item:
    loaders: Dataloaders = info.context.loaders
    user_info: dict[str, str] = await get_jwt_content(info.context)
    stakeholder_email: str = user_info["user_email"]
    stakeholder = await loaders.stakeholder.load(stakeholder_email)
    user_phone = stakeholder.phone if stakeholder else None
    if not user_phone:
        raise RequiredNewPhoneNumber()

    existing_actions = await batch_dal.get_actions_by_name(
        action_name=Action.REPORT,
        entity=group_name,
    )
    if list(
        filter(
            lambda x: x.subject.lower() == stakeholder_email.lower()
            and filter_unique_report(
                old_additional_info=x.additional_info,
                new_type="TOE_LINES",
                new_treatments=set(),
                new_states=set(),
                new_verifications=set(),
                new_closing_date=None,
                new_finding_title="",
                new_start_closing_date=None,
            ),
            existing_actions,
        ),
    ):
        raise ReportAlreadyRequested()

    await check_verification(
        recipient=get_international_format_phone_number(user_phone),
        code=verification_code,
    )

    notification = await add_report_notification(
        report_format="CSV",
        report_name=f"ToE lines ({group_name})",
        user_email=stakeholder_email,
    )
    success: bool = (
        await batch_dal.put_action(
            action=Action.REPORT,
            entity=group_name,
            subject=stakeholder_email,
            additional_info={
                "notification_id": notification.id,
                "report_type": "TOE_LINES",
            },
            attempt_duration_seconds=7200,
            queue=IntegratesBatchQueue.SMALL,
        )
    ).success
    if not success:
        raise RequestedReportError()

    return {"success": success}
