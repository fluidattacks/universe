from datetime import datetime, timedelta

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_exceptions import (
    InvalidParameter,
    RequiredNewPhoneNumber,
)
from integrates.custom_utils.reports import sign_url
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.notifications.types import ReportStatus
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_login,
)
from integrates.notifications.reports import add_report_notification, update_report_notification
from integrates.reports import (
    domain as reports_domain,
)
from integrates.reports.enums import (
    ReportType,
)
from integrates.sessions import (
    domain as sessions_domain,
)
from integrates.stakeholders.utils import (
    get_international_format_phone_number,
)
from integrates.verify import (
    operations as verify_operations,
)

from .schema import (
    QUERY,
)


@QUERY.field("unfulfilledStandardReportUrl")
@concurrent_decorators(require_login, enforce_group_level_auth_async, require_is_not_under_review)
async def resolve(
    _parent: None,
    info: GraphQLResolveInfo,
    group_name: str,
    verification_code: str,
    **kwargs: str,
) -> str:
    loaders: Dataloaders = info.context.loaders
    user_info: dict[str, str] = await sessions_domain.get_jwt_content(info.context)
    stakeholder_email: str = user_info["user_email"]
    stakeholder = await loaders.stakeholder.load(stakeholder_email)
    user_phone = stakeholder.phone if stakeholder else None
    if not user_phone:
        raise RequiredNewPhoneNumber()

    await verify_operations.check_verification(
        recipient=get_international_format_phone_number(user_phone),
        code=verification_code,
    )
    report_type = ReportType(kwargs["report_type"])
    unfulfilled_standards = (
        set(kwargs["unfulfilled_standards"]) if "unfulfilled_standards" in kwargs else None
    )
    if not unfulfilled_standards and unfulfilled_standards is not None:
        raise InvalidParameter("unfulfilledStandards")

    notification = await add_report_notification(
        report_format=report_type.value,
        report_name=f"Compliance ({group_name})",
        user_email=stakeholder_email,
    )
    report_filename, report_size = await reports_domain.get_unfulfilled_standard_report(
        loaders=loaders,
        report_type=report_type,
        group_name=group_name,
        stakeholder_email=stakeholder_email,
        unfulfilled_standards=unfulfilled_standards,
    )
    await update_report_notification(
        expiration_time=(datetime.now() + timedelta(days=7)),
        notification_id=notification.id,
        s3_file_path=f"reports/{report_filename}",
        size=report_size,
        status=ReportStatus.READY,
        user_email=stakeholder_email,
    )
    return await sign_url(report_filename, expire_seconds=60)
