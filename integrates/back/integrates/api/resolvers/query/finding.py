from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils.findings import (
    get_finding,
    is_finding_released,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    rename_kwargs,
    require_asm,
    require_is_not_under_review,
    require_login,
)

from .schema import (
    QUERY,
)


@enforce_group_level_auth_async
async def _get_draft(loaders: Dataloaders, _info: GraphQLResolveInfo, **kwargs: str) -> Finding:
    finding_id: str = kwargs["finding_id"]
    return await get_finding(loaders, finding_id)


@QUERY.field("finding")
@rename_kwargs({"identifier": "finding_id"})
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_asm,
    require_is_not_under_review,
)
@require_login
async def resolve(_parent: None, info: GraphQLResolveInfo, **kwargs: str) -> Finding:
    finding_id: str = kwargs["finding_id"]
    loaders: Dataloaders = info.context.loaders
    finding = await get_finding(loaders, finding_id)
    if not is_finding_released(finding):
        return await _get_draft(loaders, info, **kwargs)

    return finding
