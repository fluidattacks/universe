from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.mailmap.types import (
    MailmapSubentry,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_organization_level_auth_async,
    require_login,
)
from integrates.mailmap import (
    get_mailmap_entry_subentries,
)

from .schema import (
    QUERY,
)


@QUERY.field("mailmapEntrySubentries")
@concurrent_decorators(
    require_login,
    enforce_organization_level_auth_async,
)
async def resolve(
    _parent: None,
    _info: GraphQLResolveInfo,
    entry_email: str,
    organization_id: str,
) -> list[MailmapSubentry]:
    subentries = await get_mailmap_entry_subentries(
        entry_email=entry_email,
        organization_id=organization_id,
    )
    return subentries
