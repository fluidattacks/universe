from decimal import (
    Decimal,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.constants import (
    DEFAULT_MAX_SEVERITY,
)
from integrates.db_model.organizations.types import (
    Organization,
)

from .schema import (
    ORGANIZATION,
)


@ORGANIZATION.field("maxAcceptanceSeverity")
def resolve(
    parent: Organization,
    _info: GraphQLResolveInfo,
    **_kwargs: None,
) -> Decimal:
    return (
        parent.policies.max_acceptance_severity
        if parent.policies.max_acceptance_severity is not None
        else DEFAULT_MAX_SEVERITY
    )
