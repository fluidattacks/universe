from integrates.api.resolvers.organization.credential import resolve
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    CredentialsFaker,
    CredentialsStateFaker,
    GraphQLResolveInfoFaker,
    OrganizationAccessFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks

ORG_ID = "ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db"
CRED_ID = "credential-test-id"
CRED_NAME = "credential-test-name"
USER_EMAIL = "jdoe@orgtest.com"
ORG_NAME = "orgtest"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            credentials=[
                CredentialsFaker(
                    credential_id=CRED_ID,
                    organization_id=ORG_ID,
                    state=CredentialsStateFaker(modified_by=USER_EMAIL, name=CRED_NAME),
                )
            ],
            stakeholders=[StakeholderFaker(email=USER_EMAIL, enrolled=True)],
            organization_access=[OrganizationAccessFaker(organization_id=ORG_ID, email=USER_EMAIL)],
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
        )
    )
)
async def test_organization_credential() -> None:
    # Act
    parent = OrganizationFaker(id=ORG_ID, name=ORG_NAME)
    info = GraphQLResolveInfoFaker(user_email=USER_EMAIL)

    result = await resolve(parent=parent, info=info, id=CRED_ID)

    # Assert
    assert result
    assert result.id == CRED_ID
    assert result.organization_id == ORG_ID
    assert result.state.name == CRED_NAME
