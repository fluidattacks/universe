from integrates.api.resolvers.organization.user_role import resolve
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    OrganizationAccessFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks

ORG_ID = "ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db"
USER_EMAIL = "jdoe@orgtest.com"
ORG_NAME = "orgtest"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[StakeholderFaker(email=USER_EMAIL, enrolled=True)],
            organization_access=[OrganizationAccessFaker(organization_id=ORG_ID, email=USER_EMAIL)],
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
        )
    ),
)
async def test_organization_user_role() -> None:
    # Act
    parent = OrganizationFaker(id=ORG_ID, name=ORG_NAME)
    info = GraphQLResolveInfoFaker(user_email=USER_EMAIL)

    result = await resolve(parent=parent, info=info)

    # Assert
    assert result
    assert result == "organization_manager"
