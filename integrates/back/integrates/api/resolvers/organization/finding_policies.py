from datetime import (
    datetime,
)
from typing import (
    NamedTuple,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.enums import (
    TreatmentStatus,
)
from integrates.db_model.organization_finding_policies.types import (
    OrgFindingPolicy,
)
from integrates.db_model.organizations.types import (
    Organization,
)

from .schema import (
    ORGANIZATION,
)


class OrgFindingPolicyApi(NamedTuple):
    id: str
    last_status_update: datetime
    name: str
    status: str
    tags: set[str]
    treatment_acceptance: TreatmentStatus


def _format_policies_for_resolver(
    finding_policies: list[OrgFindingPolicy],
) -> list[OrgFindingPolicyApi]:
    return [
        OrgFindingPolicyApi(
            id=policy.id,
            last_status_update=policy.state.modified_date,
            name=policy.name,
            status=policy.state.status.value,
            tags=set(policy.tags),
            treatment_acceptance=policy.treatment_acceptance,
        )
        for policy in finding_policies
    ]


@ORGANIZATION.field("findingPolicies")
async def resolve(
    parent: Organization,
    info: GraphQLResolveInfo,
    **_kwargs: None,
) -> list[OrgFindingPolicyApi]:
    loaders: Dataloaders = info.context.loaders
    finding_policies = await loaders.organization_finding_policies.load(parent.name)

    return _format_policies_for_resolver(finding_policies)
