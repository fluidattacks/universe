from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.class_types.types import (
    Item,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.organizations.types import (
    Organization,
)
from integrates.trials import (
    getters as trials_getters,
)

from .schema import (
    ORGANIZATION,
)


@ORGANIZATION.field("trial")
async def resolve(
    parent: Organization,
    info: GraphQLResolveInfo,
    **_kwargs: None,
) -> Item | None:
    loaders: Dataloaders = info.context.loaders
    trial = await loaders.trial.load(parent.created_by)

    if trial:
        return {
            "completed": trial.completed,
            "extension_date": trial.extension_date or "",
            "extension_days": trial.extension_days,
            "start_date": trial.start_date or "",
            "state": trials_getters.get_status(trial),
        }
    return None
