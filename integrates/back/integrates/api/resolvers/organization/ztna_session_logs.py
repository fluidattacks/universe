from datetime import (
    UTC,
    datetime,
)
from typing import (
    TypedDict,
    Unpack,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils.datetime import (
    get_utc_now,
)
from integrates.db_model.organizations.types import (
    Organization,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_organization_level_auth_async,
    require_login,
)
from integrates.organizations.logs_ztna import (
    get_session_logs,
)
from integrates.organizations.types import (
    ZtnaSessionConnection,
)
from integrates.sessions.domain import (
    get_jwt_content,
)

from .schema import (
    ORGANIZATION,
)


class ZtnaSessionLogsArgs(TypedDict):
    start_date: datetime
    end_date: datetime
    first: int
    after: str | None


@ORGANIZATION.field("ztnaSessionLogs")
@concurrent_decorators(
    enforce_organization_level_auth_async,
    require_login,
)
async def resolve(
    parent: Organization,
    info: GraphQLResolveInfo,
    **kwargs: Unpack[ZtnaSessionLogsArgs],
) -> ZtnaSessionConnection:
    end_date = kwargs.get("end_date", get_utc_now())
    return await get_session_logs(
        organization_name=parent.name,
        continuation_token=kwargs.get("after"),
        start_date=kwargs["start_date"].astimezone(tz=UTC),
        end_date=end_date.astimezone(tz=UTC),
        max_keys=kwargs.get("first", 100),
        email=(await get_jwt_content(info.context))["user_email"],
    )
