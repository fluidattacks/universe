from integrates.api.resolvers.organization.priority_policies import resolve
from integrates.db_model.organizations.types import OrganizationPriorityPolicy
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import OrganizationAccessFaker, OrganizationFaker, StakeholderFaker
from integrates.testing.mocks import mocks

ORG_ID = "ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db"
USER_EMAIL = "jdoe@orgtest.com"
ORG_NAME = "orgtest"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[StakeholderFaker(email=USER_EMAIL, enrolled=True)],
            organization_access=[OrganizationAccessFaker(organization_id=ORG_ID, email=USER_EMAIL)],
            organizations=[
                OrganizationFaker(
                    created_by=USER_EMAIL,
                    id=ORG_ID,
                    name=ORG_NAME,
                    priority_policies=[
                        OrganizationPriorityPolicy(policy="CSPM", value=100),
                        OrganizationPriorityPolicy(policy="SAST", value=-700),
                    ],
                )
            ],
        )
    )
)
async def test_priority_policies() -> None:
    # Act
    parent = OrganizationFaker(
        created_by=USER_EMAIL,
        id=ORG_ID,
        name=ORG_NAME,
        priority_policies=[
            OrganizationPriorityPolicy(policy="CSPM", value=100),
            OrganizationPriorityPolicy(policy="SAST", value=-700),
        ],
    )
    result = resolve(parent=parent, _info=None)

    # Assert
    assert result
    assert result == [
        OrganizationPriorityPolicy(policy="CSPM", value=100),
        OrganizationPriorityPolicy(policy="SAST", value=-700),
    ]
