from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.organizations.types import (
    Organization,
)

from .schema import (
    ORGANIZATION,
)


@ORGANIZATION.field("id")
def resolve(
    parent: Organization,
    _info: GraphQLResolveInfo,
    **_kwargs: None,
) -> str:
    return parent.id
