from datetime import (
    datetime,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.billing import (
    authors as billing_authors,
)
from integrates.billing.types import (
    OrganizationBilling,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.db_model.organizations.types import (
    Organization,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_organization_level_auth_async,
    require_login,
)

from .schema import (
    ORGANIZATION,
)


@ORGANIZATION.field("billing")
@concurrent_decorators(
    enforce_organization_level_auth_async,
    require_login,
)
async def resolve(
    parent: Organization,
    info: GraphQLResolveInfo,
    **kwargs: datetime,
) -> OrganizationBilling:
    return await billing_authors.get_organization_billing(
        date=kwargs.get("date", datetime_utils.get_now()),
        org=parent,
        loaders=info.context.loaders,
    )
