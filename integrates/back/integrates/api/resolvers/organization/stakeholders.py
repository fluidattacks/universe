from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.organizations.types import (
    Organization,
)
from integrates.db_model.stakeholders.types import (
    Stakeholder,
)
from integrates.decorators import (
    enforce_organization_level_auth_async,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .schema import (
    ORGANIZATION,
)


@ORGANIZATION.field("stakeholders")
@enforce_organization_level_auth_async
async def resolve(
    parent: Organization,
    info: GraphQLResolveInfo,
    **_kwargs: None,
) -> list[Stakeholder]:
    loaders: Dataloaders = info.context.loaders
    # The store is needed to resolve stakeholder's role
    request_store = sessions_domain.get_request_store(info.context)
    request_store["entity"] = "ORGANIZATION"
    request_store["organization_id"] = parent.id

    user_data = await sessions_domain.get_jwt_content(info.context)
    user_email = user_data["user_email"]
    stakeholders = await orgs_domain.get_stakeholders(
        loaders,
        parent.id,
        user_email,
    )

    return stakeholders
