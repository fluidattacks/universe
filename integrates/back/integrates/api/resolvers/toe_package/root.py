from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils import (
    roots as roots_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.roots.types import (
    Root,
)
from integrates.db_model.toe_packages.types import (
    ToePackage,
)

from .schema import (
    TOE_PACKAGE,
)


@TOE_PACKAGE.field("root")
async def resolve(
    parent: ToePackage,
    info: GraphQLResolveInfo,
    **_kwargs: None,
) -> Root:
    loaders: Dataloaders = info.context.loaders
    root = await roots_utils.get_root(loaders, parent.root_id, parent.group_name)

    return root
