from graphql import (
    GraphQLResolveInfo,
)

from integrates.db_model.groups.types import (
    GroupAzureIssues,
)
from integrates.groups import (
    azure_issues_integration,
)

from .schema import (
    AZURE_ISSUES_INTEGRATION,
)


@AZURE_ISSUES_INTEGRATION.field("organizationNames")
async def resolve(
    parent: GroupAzureIssues,
    _info: GraphQLResolveInfo,
) -> list[str]:
    return await azure_issues_integration.get_organization_names(parent)
