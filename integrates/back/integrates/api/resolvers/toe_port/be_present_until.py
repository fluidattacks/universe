from datetime import (
    datetime,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.toe_ports.types import (
    ToePort,
)
from integrates.decorators import (
    enforce_group_level_auth_async,
)

from .schema import (
    TOE_PORT,
)


@TOE_PORT.field("bePresentUntil")
@enforce_group_level_auth_async
def resolve(parent: ToePort, _info: GraphQLResolveInfo, **_kwargs: None) -> datetime | None:
    return parent.state.be_present_until
