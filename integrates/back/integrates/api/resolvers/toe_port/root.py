from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.roots.types import (
    Root,
    RootRequest,
)
from integrates.db_model.toe_ports.types import (
    ToePort,
)

from .schema import (
    TOE_PORT,
)


@TOE_PORT.field("root")
async def resolve(
    parent: ToePort,
    info: GraphQLResolveInfo,
    **_kwargs: None,
) -> Root | None:
    loaders: Dataloaders = info.context.loaders
    if parent.root_id:
        root = await loaders.root.load(RootRequest(parent.group_name, parent.root_id))

        return root

    return None
