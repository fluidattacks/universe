from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.toe_ports.types import (
    ToePort,
)

from .schema import (
    TOE_PORT,
)


@TOE_PORT.field("bePresent")
def resolve(parent: ToePort, _info: GraphQLResolveInfo, **_kwargs: None) -> bool | None:
    return parent.state.be_present
