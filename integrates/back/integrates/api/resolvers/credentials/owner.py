from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.credentials.types import (
    Credentials,
)

from .schema import (
    CREDENTIALS,
)


@CREDENTIALS.field("owner")
def resolve(parent: Credentials, _info: GraphQLResolveInfo) -> str:
    return parent.state.owner
