from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.credentials.types import (
    Credentials,
)

from .schema import (
    CREDENTIALS,
)


@CREDENTIALS.field("azureOrganization")
def resolve(parent: Credentials, _info: GraphQLResolveInfo) -> str | None:
    return parent.state.azure_organization
