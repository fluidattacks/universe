from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.credentials.types import (
    Credentials,
)

from .schema import (
    CREDENTIALS,
)


@CREDENTIALS.field("isPat")
def resolve(parent: Credentials, _info: GraphQLResolveInfo) -> bool:
    return parent.state.is_pat
