from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.credentials.types import (
    Credentials,
)
from integrates.oauth.common import (
    get_oauth_type,
)

from .schema import (
    CREDENTIALS,
)


@CREDENTIALS.field("oauthType")
def resolve(parent: Credentials, _info: GraphQLResolveInfo) -> str:
    return get_oauth_type(parent)
