from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.credentials.types import (
    Credentials,
)
from integrates.db_model.organizations.types import (
    Organization,
)
from integrates.decorators import (
    require_organization_access,
)

from .schema import (
    CREDENTIALS,
)


@CREDENTIALS.field("organization")
@require_organization_access
async def resolve(
    parent: Credentials,
    info: GraphQLResolveInfo,
) -> Organization | None:
    loaders: Dataloaders = info.context.loaders
    organization = await loaders.organization.load(parent.organization_id)
    return organization
