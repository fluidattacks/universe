from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.roots.types import (
    URLRoot,
)

from .schema import (
    URL_ROOT,
)


@URL_ROOT.field("path")
def resolve(parent: URLRoot, _info: GraphQLResolveInfo) -> str:
    return parent.state.path
