from gitlab.v4.objects import (
    ProjectMemberAll,
)
from graphql import (
    GraphQLResolveInfo,
)

from integrates.groups import (
    gitlab_issues_integration,
)

from .schema import (
    GITLAB_ISSUES_INTEGRATION,
)


@GITLAB_ISSUES_INTEGRATION.field("projectMembers")
async def resolve(
    parent: dict,
    info: GraphQLResolveInfo,
    *,
    gitlab_project: str,
) -> list[ProjectMemberAll]:
    return await gitlab_issues_integration.get_project_members(
        gitlab_project=gitlab_project,
        group_name=parent["group_name"],
        loaders=info.context.loaders,
    )
