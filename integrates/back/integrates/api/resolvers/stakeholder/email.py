from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.stakeholders.types import (
    Stakeholder,
)

from .schema import (
    STAKEHOLDER,
)


@STAKEHOLDER.field("email")
def resolve(
    parent: Stakeholder,
    _info: GraphQLResolveInfo,
    **_kwargs: None,
) -> str | None:
    if isinstance(parent, dict):
        email = parent["email"]
    else:
        email = parent.email
    return email
