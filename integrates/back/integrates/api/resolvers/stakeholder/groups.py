from aioextensions import (
    collect,
)
from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils import (
    groups as groups_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.db_model.stakeholders.types import (
    Stakeholder,
)
from integrates.groups import (
    domain as groups_domain,
)

from .schema import (
    STAKEHOLDER,
)


@STAKEHOLDER.field("groups")
async def resolve(
    parent: Stakeholder,
    info: GraphQLResolveInfo,
    **_kwargs: None,
) -> list[Group]:
    loaders: Dataloaders = info.context.loaders
    email = parent.email
    active, inactive = await collect(
        [
            groups_domain.get_groups_by_stakeholder(loaders, email),
            groups_domain.get_groups_by_stakeholder(loaders, email, active=False),
        ],
    )
    stakeholder_group_names: list[str] = active + inactive
    groups = await collect(
        [
            groups_utils.get_group(loaders, stakeholder_group_name)
            for stakeholder_group_name in stakeholder_group_names
        ],
    )

    return groups_utils.filter_active_groups(groups)
