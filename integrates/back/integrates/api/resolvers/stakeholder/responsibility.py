from graphql.type.definition import GraphQLResolveInfo

from integrates.custom_utils.access import format_invitation_state
from integrates.dataloaders import Dataloaders
from integrates.db_model.group_access.types import GroupAccessRequest
from integrates.db_model.organization_access.enums import OrganizationInvitiationState
from integrates.db_model.stakeholders.types import Stakeholder
from integrates.sessions import domain as sessions_domain

from .schema import STAKEHOLDER


@STAKEHOLDER.field("responsibility")
async def resolve(
    parent: Stakeholder,
    info: GraphQLResolveInfo,
    **_kwargs: None,
) -> str | None:
    request_store = sessions_domain.get_request_store(info.context)
    entity = request_store.get("entity")
    loaders: Dataloaders = info.context.loaders

    if entity == "GROUP":
        if group_access := await loaders.group_access.load(
            GroupAccessRequest(group_name=request_store["group_name"], email=parent.email),
        ):
            invitation_state = format_invitation_state(
                invitation=group_access.state.invitation,
                is_registered=parent.is_registered,
            )
            if group_access:
                return (
                    group_access.state.invitation.responsibility
                    if group_access.state.invitation
                    and invitation_state == OrganizationInvitiationState.PENDING
                    else group_access.state.responsibility
                )

        return None

    return None
