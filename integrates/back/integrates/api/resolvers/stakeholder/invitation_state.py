from graphql.type.definition import GraphQLResolveInfo

from integrates.custom_utils.access import format_invitation_state
from integrates.dataloaders import Dataloaders
from integrates.db_model.group_access.types import GroupAccessRequest
from integrates.db_model.organization_access.types import OrganizationAccessRequest
from integrates.db_model.stakeholders.types import Stakeholder
from integrates.sessions import domain as sessions_domain

from .schema import STAKEHOLDER


@STAKEHOLDER.field("invitationState")
async def resolve(
    parent: Stakeholder,
    info: GraphQLResolveInfo,
    **_kwargs: None,
) -> str | None:
    loaders: Dataloaders = info.context.loaders
    request_store = sessions_domain.get_request_store(info.context)
    entity = request_store.get("entity")

    if entity == "GROUP":
        group_name = request_store["group_name"]
        group_access = await loaders.group_access.load(
            GroupAccessRequest(group_name=group_name, email=parent.email)
        )
        return format_invitation_state(
            invitation=group_access.state.invitation if group_access else None,
            is_registered=parent.is_registered,
        )

    if entity == "ORGANIZATION":
        organization_id = request_store["organization_id"]
        organization_access = await loaders.organization_access.load(
            OrganizationAccessRequest(organization_id=organization_id, email=parent.email)
        )
        return format_invitation_state(
            invitation=organization_access.state.invitation if organization_access else None,
            is_registered=parent.is_registered,
        )

    return None
