from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import Dataloaders
from integrates.db_model.organization_access.types import OrganizationAccessRequest
from integrates.db_model.stakeholders.types import Stakeholder
from integrates.group_access.domain import get_stakeholder_role as get_group_stakeholder_role
from integrates.organizations.domain import (
    get_stakeholder_role as get_organization_stakeholder_role,
)
from integrates.sessions import domain as sessions_domain

from .schema import STAKEHOLDER


@STAKEHOLDER.field("role")
async def resolve(
    parent: Stakeholder,
    info: GraphQLResolveInfo,
    **_kwargs: None,
) -> str | None:
    loaders: Dataloaders = info.context.loaders
    stakeholder_role: str = ""
    request_store = sessions_domain.get_request_store(info.context)
    entity = request_store.get("entity")

    if entity == "GROUP":
        group_name = request_store["group_name"]
        stakeholder_role = await get_group_stakeholder_role(
            loaders, parent.email, group_name, parent.is_registered
        )

    if entity == "ORGANIZATION":
        organization_id = request_store["organization_id"]
        if await loaders.organization_access.load(
            OrganizationAccessRequest(organization_id=organization_id, email=parent.email),
        ):
            stakeholder_role = await get_organization_stakeholder_role(
                loaders, parent.email, parent.is_registered, organization_id
            )

    return stakeholder_role if stakeholder_role else None
