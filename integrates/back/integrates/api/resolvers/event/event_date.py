from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils.datetime import (
    get_as_str,
)
from integrates.db_model.events.types import (
    Event,
)

from .schema import (
    EVENT,
)


@EVENT.field("eventDate")
def resolve(
    parent: Event,
    _info: GraphQLResolveInfo,
    **_kwargs: None,
) -> str:
    return get_as_str(parent.event_date)
