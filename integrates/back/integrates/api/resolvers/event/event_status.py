from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.events.types import (
    Event,
)

from .schema import (
    EVENT,
)


@EVENT.field("eventStatus")
def resolve(
    parent: Event,
    _info: GraphQLResolveInfo,
    **_kwargs: None,
) -> str:
    event_status = parent.state.status.value
    return event_status
