from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.events.types import (
    Event,
)
from integrates.decorators import (
    enforce_group_level_auth_async,
)

from .schema import (
    EVENT,
)


@EVENT.field("hacker")
@enforce_group_level_auth_async
def resolve(
    parent: Event,
    _info: GraphQLResolveInfo,
    **_kwargs: None,
) -> str:
    analyst = parent.hacker
    return analyst
