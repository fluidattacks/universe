from integrates.api.resolvers.event.root import resolve
from integrates.db_model.events.enums import EventStateStatus
from integrates.db_model.roots.types import GitRoot, Root
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute,
    require_attribute_internal,
    require_is_not_under_review,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    EventFaker,
    EventStateFaker,
    GitRootFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize


@parametrize(
    args=["email"],
    cases=[
        ["admin@gmail.com"],
        ["hacker@gmail.com"],
        ["reattacker@gmail.com"],
        ["user@gmail.com"],
        ["group_manager@gmail.com"],
        ["vulnerability_manager@gmail.com"],
        ["resourcer@gmail.com"],
        ["reviewer@gmail.com"],
        ["customer_manager@fluidattacks.com"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            events=[
                EventFaker(
                    id="418900971",
                    root_id="root1",
                    state=EventStateFaker(status=EventStateStatus.CREATED),
                ),
            ],
            groups=[
                GroupFaker(organization_id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email="admin@gmail.com"),
                StakeholderFaker(email="hacker@gmail.com"),
                StakeholderFaker(email="reattacker@gmail.com"),
                StakeholderFaker(email="user@gmail.com"),
                StakeholderFaker(email="group_manager@gmail.com"),
                StakeholderFaker(email="vulnerability_manager@gmail.com"),
                StakeholderFaker(email="resourcer@gmail.com"),
                StakeholderFaker(email="reviewer@gmail.com"),
                StakeholderFaker(email="customer_manager@fluidattacks.com"),
            ],
            group_access=[
                GroupAccessFaker(
                    email="admin@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
                GroupAccessFaker(
                    email="hacker@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
                GroupAccessFaker(
                    email="reattacker@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="reattacker"),
                ),
                GroupAccessFaker(
                    email="user@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                ),
                GroupAccessFaker(
                    email="group_manager@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
                GroupAccessFaker(
                    email="vulnerability_manager@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="vulnerability_manager"),
                ),
                GroupAccessFaker(
                    email="resourcer@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="resourcer"),
                ),
                GroupAccessFaker(
                    email="reviewer@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="reviewer"),
                ),
                GroupAccessFaker(
                    email="customer_manager@fluidattacks.com",
                    state=GroupAccessStateFaker(has_access=True, role="customer_manager"),
                ),
            ],
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            roots=[GitRootFaker(id="root1", organization_name="org1")],
        ),
    )
)
async def test_get_event_root(email: str) -> None:
    event_id: str = "418900971"
    event_id_root: str = "root1"
    event = EventFaker(
        id=event_id,
        root_id=event_id_root,
        state=EventStateFaker(status=EventStateStatus.CREATED),
    )
    expected_root = GitRootFaker(id=event_id_root, organization_name="org1")

    root: Root = await resolve(
        parent=event,
        info=GraphQLResolveInfoFaker(
            user_email=email,
            decorators=[
                [require_login],
                [enforce_group_level_auth_async],
                [require_is_not_under_review],
                [require_attribute_internal, "is_under_review", "test-group"],
                [require_attribute, "has_asm", "test-group"],
            ],
        ),
    )

    assert isinstance(root, GitRoot) and root == expected_root
