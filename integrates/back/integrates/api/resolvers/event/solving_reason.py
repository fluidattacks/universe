from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.events.enums import (
    EventSolutionReason,
    EventStateStatus,
)
from integrates.db_model.events.types import (
    Event,
)

from .schema import (
    EVENT,
)


@EVENT.field("solvingReason")
def resolve(
    parent: Event,
    _info: GraphQLResolveInfo,
    **_kwargs: None,
) -> EventSolutionReason | None:
    return parent.state.reason if parent.state.status == EventStateStatus.SOLVED else None
