from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_exceptions import (
    InvalidParameter,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.events.types import (
    Event,
)
from integrates.db_model.organizations.types import (
    Organization,
)
from integrates.organizations.utils import (
    get_organization,
)

from .schema import (
    EVENT,
)


@EVENT.field("organization")
async def resolve(
    parent: Event,
    info: GraphQLResolveInfo,
) -> str:
    loaders: Dataloaders = info.context.loaders
    group = await loaders.group.load(parent.group_name)
    if group is None:
        raise InvalidParameter()
    organization: Organization = await get_organization(loaders, group.organization_id)
    return organization.name
