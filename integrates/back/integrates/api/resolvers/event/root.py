from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.events.types import (
    Event,
)
from integrates.db_model.roots.types import (
    Root,
    RootRequest,
)

from .schema import (
    EVENT,
)


@EVENT.field("root")
async def resolve(
    parent: Event,
    info: GraphQLResolveInfo,
    **_kwargs: None,
) -> Root | None:
    loaders: Dataloaders = info.context.loaders
    if parent.root_id:
        root = await loaders.root.load(RootRequest(parent.group_name, parent.root_id))

        return root

    return None
