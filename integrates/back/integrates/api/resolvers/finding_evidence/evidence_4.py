from graphql.type.definition import (
    GraphQLResolveInfo,
)

from .schema import (
    FINDING_EVIDENCE,
)


@FINDING_EVIDENCE.field("evidence4")
def resolve(
    parent: dict[str, dict[str, str]],
    _info: GraphQLResolveInfo,
    **_kwargs: None,
) -> dict[str, str]:
    return parent["evidence_4"]
