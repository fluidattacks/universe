from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.billing import (
    domain as billing_domain,
)
from integrates.billing.types import (
    OrganizationBilling,
    PaymentMethod,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.organizations import (
    utils as orgs_utils,
)

from .schema import (
    ORGANIZATION_BILLING,
)


@ORGANIZATION_BILLING.field("paymentMethods")
async def resolve(
    parent: OrganizationBilling,
    info: GraphQLResolveInfo,
    **_kwargs: None,
) -> list[PaymentMethod]:
    loaders: Dataloaders = info.context.loaders
    organization = await orgs_utils.get_organization(loaders, parent.organization)

    return await billing_domain.list_organization_payment_methods(
        org=organization,
        limit=100,
    )
