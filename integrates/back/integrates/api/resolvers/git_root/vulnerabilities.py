from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils.filter_vulnerabilities import (
    filter_non_zero_risk,
    filter_released_vulns,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.roots.types import (
    Root,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
)

from .schema import (
    GIT_ROOT,
)


@GIT_ROOT.field("vulnerabilities")
async def resolve(parent: Root, info: GraphQLResolveInfo) -> list[Vulnerability]:
    loaders: Dataloaders = info.context.loaders
    root_vulnerabilities = await loaders.root_vulnerabilities.load(parent.id)
    return filter_released_vulns(filter_non_zero_risk(root_vulnerabilities))
