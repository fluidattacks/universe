from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils.groups import get_group
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.credentials.types import (
    Credentials,
    CredentialsRequest,
)
from integrates.db_model.roots.types import (
    GitRoot,
)

from .schema import (
    GIT_ROOT,
)


@GIT_ROOT.field("credentials")
async def resolve(parent: GitRoot, info: GraphQLResolveInfo) -> Credentials | None:
    loaders: Dataloaders = info.context.loaders
    if parent.state.credential_id:
        group = await get_group(loaders, parent.group_name)
        request = CredentialsRequest(
            id=parent.state.credential_id,
            organization_id=group.organization_id,
        )
        return await loaders.credentials.load(request)

    return None
