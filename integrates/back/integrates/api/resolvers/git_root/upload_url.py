from graphql.type.definition import GraphQLResolveInfo

from integrates.db_model.roots.types import GitRoot
from integrates.decorators import enforce_group_level_auth_async
from integrates.roots.s3_mirror import get_upload_url

from .schema import GIT_ROOT


@GIT_ROOT.field("uploadUrl")
@enforce_group_level_auth_async
async def resolve(parent: GitRoot, _: GraphQLResolveInfo) -> str | None:
    return await get_upload_url(group_name=parent.group_name, root_nickname=parent.state.nickname)
