from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.api.resolvers.git_root_cloning_status.types import (
    GitRootCloningStatus,
)
from integrates.db_model.roots.types import (
    GitRoot,
)

from .schema import (
    GIT_ROOT,
)


@GIT_ROOT.field("cloningStatus")
def resolve(parent: GitRoot, _info: GraphQLResolveInfo) -> GitRootCloningStatus:
    return GitRootCloningStatus(
        commit=parent.cloning.commit,
        group_name=parent.group_name,
        message=parent.cloning.reason,
        modified_date=parent.cloning.modified_date,
        root_nickname=parent.state.nickname,
        status=parent.cloning.status.value,
    )
