from datetime import (
    datetime,
)
from typing import (
    NamedTuple,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.roots.types import (
    GitRoot,
)

from .schema import (
    GIT_ROOT,
)


class GitRootMachineStatus(NamedTuple):
    message: str
    modified_date: datetime
    status: str
    commit: str | None = None


@GIT_ROOT.field("machineStatus")
def resolve(parent: GitRoot, _info: GraphQLResolveInfo) -> GitRootMachineStatus | None:
    return (
        GitRootMachineStatus(
            message=parent.machine.reason,
            modified_date=parent.machine.modified_date,
            status=parent.machine.status,
            commit=parent.machine.commit,
        )
        if parent.machine
        else None
    )
