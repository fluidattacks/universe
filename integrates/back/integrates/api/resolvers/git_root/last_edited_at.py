from datetime import (
    datetime,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.roots.types import (
    GitRoot,
)

from .schema import (
    GIT_ROOT,
)


@GIT_ROOT.field("lastEditedAt")
def resolve(parent: GitRoot, _info: GraphQLResolveInfo) -> datetime:
    return parent.state.modified_date
