from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.roots.types import (
    Root,
)
from integrates.db_model.vulnerabilities.constants import (
    RELEASED_FILTER_STATUSES,
    ZR_FILTER_STATUSES,
)
from integrates.db_model.vulnerabilities.types import (
    RootVulnerabilitiesRequest,
    VulnerabilitiesConnection,
)

from .schema import (
    GIT_ROOT,
)


@GIT_ROOT.field("vulnerabilitiesConnection")
async def resolve(
    parent: Root,
    info: GraphQLResolveInfo,
    after: str | None = None,
    first: int | None = None,
) -> VulnerabilitiesConnection:
    loaders: Dataloaders = info.context.loaders
    connection = await loaders.root_vulnerabilities_connection.load(
        RootVulnerabilitiesRequest(root_id=parent.id, after=after, first=first, paginate=True),
    )
    filtered_edges = [
        edge
        for edge in connection.edges
        if edge.node.state.status in RELEASED_FILTER_STATUSES
        and (edge.node.zero_risk is None or edge.node.zero_risk.status not in ZR_FILTER_STATUSES)
    ]

    return VulnerabilitiesConnection(
        edges=tuple(edge for edge in filtered_edges),
        page_info=connection.page_info,
    )
