from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.roots.types import (
    GitRoot,
    RootDockerImage,
    RootDockerImageRequest,
)

from .schema import (
    GIT_ROOT,
)


@GIT_ROOT.field("dockerImage")
async def resolve(
    parent: GitRoot,
    info: GraphQLResolveInfo,
    *,
    uri: str,
) -> RootDockerImage | None:
    loaders: Dataloaders = info.context.loaders

    return await loaders.docker_image.load(
        RootDockerImageRequest(root_id=parent.id, group_name=parent.group_name, uri_id=uri),
    )
