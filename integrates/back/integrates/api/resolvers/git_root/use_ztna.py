from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.roots.types import (
    GitRoot,
)

from .schema import (
    GIT_ROOT,
)


@GIT_ROOT.field("useZtna")
def resolve(parent: GitRoot, _info: GraphQLResolveInfo) -> bool:
    return parent.state.use_ztna
