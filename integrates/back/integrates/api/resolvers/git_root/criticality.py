from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.roots.enums import (
    RootCriticality,
)
from integrates.db_model.roots.types import (
    GitRoot,
)

from .schema import (
    GIT_ROOT,
)


@GIT_ROOT.field("criticality")
def resolve(parent: GitRoot, _info: GraphQLResolveInfo) -> RootCriticality | None:
    return parent.state.criticality
