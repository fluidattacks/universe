from integrates.api.resolvers.group.forces_token import resolve
from integrates.dataloaders import get_new_context
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute_internal,
    require_login,
)
from integrates.testing.aws import (
    IntegratesAws,
    IntegratesDynamodb,
)
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationAccessFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks

ORG_ID = "ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db"
TEST_TKN = "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJjaXBABCXYZ"
GROUP_NAME = "grouptest"
USER_EMAIL = "jdoe@fluidattacks.com"
ORG_NAME = "orgtest"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(agent_token=TEST_TKN, name=GROUP_NAME, organization_id=ORG_ID)],
            group_access=[
                GroupAccessFaker(
                    email=USER_EMAIL,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="customer_manager"),
                )
            ],
            stakeholders=[
                StakeholderFaker(email=USER_EMAIL, enrolled=True, role="customer_manager")
            ],
            organization_access=[OrganizationAccessFaker(organization_id=ORG_ID, email=USER_EMAIL)],
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
        )
    )
)
async def test_group_forces_token() -> None:
    # Act
    loaders = get_new_context()
    parent = await loaders.group.load(GROUP_NAME)
    info = GraphQLResolveInfoFaker(
        user_email=USER_EMAIL,
        decorators=[
            [enforce_group_level_auth_async],
            [require_login],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )

    result = await resolve(parent=parent, info=info, _info=info, group_name=GROUP_NAME)

    # Assert
    assert result
    assert result == TEST_TKN
