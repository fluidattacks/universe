from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.groups.types import (
    Group,
)

from .schema import (
    GROUP,
)


@GROUP.field("groupContext")
def resolve(
    parent: Group,
    _info: GraphQLResolveInfo,
) -> str | None:
    return parent.context
