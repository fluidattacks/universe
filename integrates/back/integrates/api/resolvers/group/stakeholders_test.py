from integrates.api.resolvers.group.stakeholders import resolve
from integrates.dataloaders import get_new_context
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute,
    require_attribute_internal,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationAccessFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks

ORG_ID = "ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db"
GROUP_NAME = "grouptest"
USER_EMAIL = "jdoe@orgtest.com"
EMAIL = "fluid@test.com"
ORG_NAME = "orgtest"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            group_access=[
                GroupAccessFaker(
                    email=USER_EMAIL,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="group_manager"),
                ),
                GroupAccessFaker(
                    email=EMAIL,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="vulnerability_manager"),
                ),
            ],
            stakeholders=[
                StakeholderFaker(email=USER_EMAIL, enrolled=True),
                StakeholderFaker(email=EMAIL, enrolled=True),
            ],
            organization_access=[
                OrganizationAccessFaker(organization_id=ORG_ID, email=USER_EMAIL),
                OrganizationAccessFaker(organization_id=ORG_ID, email=EMAIL),
            ],
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
        )
    )
)
async def test_group_stakeholders() -> None:
    # Act
    loaders = get_new_context()
    parent = await loaders.group.load(GROUP_NAME)
    info = GraphQLResolveInfoFaker(
        user_email=USER_EMAIL,
        decorators=[
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", GROUP_NAME],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )

    result = await resolve(parent=parent, info=info, group_name=GROUP_NAME)

    # Assert
    assert result
    assert len(result) == 2
    assert sorted(stakeholder.email for stakeholder in result) == [EMAIL, USER_EMAIL]
