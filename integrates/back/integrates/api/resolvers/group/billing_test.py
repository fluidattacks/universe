from integrates.api.resolvers.group.billing import resolve
from integrates.billing import domain as billing_domain
from integrates.billing.types import Price
from integrates.db_model.organizations.types import (
    OrganizationDocuments,
    OrganizationPaymentMethods,
)
from integrates.decorators import enforce_group_level_auth_async, require_login
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    DATE_2025,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationAccessFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import Mock, mocks

ORG_ID = "ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db"
USER_EMAIL = "jdoe@orgtest.com"
GROUP_NAME = "grouptest"
ORG_NAME = "orgtest"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[StakeholderFaker(email=USER_EMAIL, enrolled=True)],
            organization_access=[OrganizationAccessFaker(organization_id=ORG_ID, email=USER_EMAIL)],
            groups=[GroupFaker(organization_id=ORG_ID, name=GROUP_NAME)],
            group_access=[
                GroupAccessFaker(
                    email=USER_EMAIL,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="group_manager"),
                ),
            ],
            organizations=[
                OrganizationFaker(
                    id=ORG_ID,
                    name=ORG_NAME,
                    payment_methods=[
                        OrganizationPaymentMethods(
                            id="org-payment-method",
                            business_name=ORG_NAME,
                            email=USER_EMAIL,
                            country="US",
                            state="TX",
                            city="Austin",
                            documents=OrganizationDocuments(rut=None, tax_id=None),
                        )
                    ],
                )
            ],
        )
    ),
    others=[
        Mock(
            billing_domain,
            "get_prices",
            "async",
            {
                "advanced": Price("price_id_123", "USD", 1500),
                "essential": Price("price_id_456", "USD", 2000),
            },
        )
    ],
)
async def test_group_billing() -> None:
    # Act
    parent = GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)
    info = GraphQLResolveInfoFaker(
        user_email=USER_EMAIL, decorators=[[require_login], [enforce_group_level_auth_async]]
    )

    result = await resolve(parent=parent, info=info, date=DATE_2025, group_name=GROUP_NAME)

    # Assert
    assert result
    assert result.authors == ()
    assert result.costs_authors == 0
    assert result.costs_base == 20
    assert result.costs_total == 20
    assert result.number_authors == 0
