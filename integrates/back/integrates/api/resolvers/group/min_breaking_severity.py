from decimal import (
    Decimal,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.organizations import (
    utils as orgs_utils,
)

from .schema import (
    GROUP,
)


@GROUP.field("minBreakingSeverity")
async def resolve(
    parent: Group,
    info: GraphQLResolveInfo,
    **_kwargs: None,
) -> Decimal | None:
    if parent.policies:
        return parent.policies.min_breaking_severity

    loaders: Dataloaders = info.context.loaders
    organization = await orgs_utils.get_organization(loaders, parent.organization_id)

    return organization.policies.min_breaking_severity
