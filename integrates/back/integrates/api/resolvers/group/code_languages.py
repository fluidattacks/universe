from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.groups.types import (
    Group,
    GroupUnreliableIndicators,
)
from integrates.db_model.types import (
    CodeLanguage,
)
from integrates.decorators import (
    concurrent_decorators,
    require_asm,
    require_is_not_under_review,
)

from .schema import (
    GROUP,
)


@GROUP.field("codeLanguages")
@concurrent_decorators(
    require_is_not_under_review,
    require_asm,
)
async def resolve(
    parent: Group,
    info: GraphQLResolveInfo,
    **_kwargs: None,
) -> list[CodeLanguage] | None:
    loaders: Dataloaders = info.context.loaders
    group_indicators: GroupUnreliableIndicators = await loaders.group_unreliable_indicators.load(
        parent.name,
    )
    return group_indicators.code_languages
