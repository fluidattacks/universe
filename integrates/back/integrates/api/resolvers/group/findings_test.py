from integrates.api.resolvers.group.findings import resolve
from integrates.db_model.findings.types import (
    FindingVulnerabilitiesSummary,
)
from integrates.decorators import (
    require_attribute,
    require_attribute_internal,
    require_is_not_under_review,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb, IntegratesOpensearch
from integrates.testing.fakers import (
    FindingFaker,
    FindingUnreliableIndicatorsFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
    random_uuid,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize

ORG_ID = random_uuid()
GROUP_NAME = "groupname"

ROLES = [
    "admin",
    "hacker",
    "reviewer",
    "resourcer",
    "customer_manager",
    "vulnerability_manager",
    "group_manager",
    "user",
]

FINDING_TITLE = "001. SQL injection - C Sharp SQL API"


@parametrize(
    args=["role"],
    cases=[[role] for role in ROLES],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(organization_id=ORG_ID, name=GROUP_NAME)],
            group_access=[
                GroupAccessFaker(
                    email=f"{role}@fluidattacks.com",
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role=role),
                )
                for role in ROLES
            ],
            stakeholders=[
                StakeholderFaker(
                    email=f"{role}@fluidattacks.com",
                )
                for role in ROLES
            ],
            organizations=[OrganizationFaker(name="test-org", id=ORG_ID)],
            findings=[
                FindingFaker(
                    id=random_uuid(),
                    group_name=GROUP_NAME,
                    unreliable_indicators=FindingUnreliableIndicatorsFaker(
                        vulnerabilities_summary=FindingVulnerabilitiesSummary(closed=1, submitted=1)
                    ),
                ),
                FindingFaker(
                    id=random_uuid(),
                    group_name=GROUP_NAME,
                    unreliable_indicators=FindingUnreliableIndicatorsFaker(
                        vulnerabilities_summary=FindingVulnerabilitiesSummary(submitted=1)
                    ),
                ),
                FindingFaker(
                    id=random_uuid(),
                    group_name=GROUP_NAME,
                    unreliable_indicators=FindingUnreliableIndicatorsFaker(
                        vulnerabilities_summary=FindingVulnerabilitiesSummary(closed=1)
                    ),
                ),
                FindingFaker(
                    id=random_uuid(),
                    group_name=GROUP_NAME,
                    unreliable_indicators=FindingUnreliableIndicatorsFaker(
                        vulnerabilities_summary=FindingVulnerabilitiesSummary(open=1)
                    ),
                ),
            ],
        ),
        opensearch=IntegratesOpensearch(autoload=True),
    )
)
async def test_finding(role: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=f"{role}@fluidattacks.com",
        decorators=[
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    parent = GroupFaker(name=GROUP_NAME)
    result = await resolve(
        parent,
        info,
    )
    assert len(result) == 4
    for finding in result:
        assert finding.title == FINDING_TITLE


EXTERNAL_ROLES = [
    "vulnerability_manager",
    "group_manager",
    "user",
]


@parametrize(
    args=["role"],
    cases=[[role] for role in ROLES],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(organization_id=ORG_ID, name="group2")],
            group_access=[
                GroupAccessFaker(
                    email=f"{role}@fluidattacks.com",
                    group_name="group2",
                    state=GroupAccessStateFaker(role=role),
                )
                for role in ROLES
            ],
            stakeholders=[
                StakeholderFaker(
                    email=f"{role}@fluidattacks.com",
                )
                for role in ROLES
            ],
            organizations=[OrganizationFaker(name="test-org", id=ORG_ID)],
            findings=[
                FindingFaker(
                    id=random_uuid(),
                    group_name="group2",
                    unreliable_indicators=FindingUnreliableIndicatorsFaker(
                        vulnerabilities_summary=FindingVulnerabilitiesSummary(
                            closed=70, submitted=1
                        )
                    ),
                ),
                FindingFaker(
                    id=random_uuid(),
                    group_name="group2",
                    unreliable_indicators=FindingUnreliableIndicatorsFaker(
                        vulnerabilities_summary=FindingVulnerabilitiesSummary(closed=3)
                    ),
                ),
            ],
        ),
        opensearch=IntegratesOpensearch(autoload=True),
    )
)
async def test_finding_duplicated(role: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=f"{role}@fluidattacks.com",
        decorators=[
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", "group2"],
            [require_attribute, "has_asm", "group2"],
        ],
    )
    parent = GroupFaker(name="group2")
    result = await resolve(
        parent,
        info,
    )
    assert len(result) == 2
    for finding in result:
        assert finding.title == FINDING_TITLE


@parametrize(
    args=["role"],
    cases=[[role] for role in EXTERNAL_ROLES],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(organization_id=ORG_ID, name="group3")],
            group_access=[
                GroupAccessFaker(
                    email=f"{role}@company.com",
                    group_name="group3",
                    state=GroupAccessStateFaker(role=role),
                )
                for role in EXTERNAL_ROLES
            ],
            stakeholders=[
                StakeholderFaker(
                    email=f"{role}@company.com",
                )
                for role in ROLES
            ],
            organizations=[OrganizationFaker(name="test-org", id=ORG_ID)],
            findings=[
                FindingFaker(
                    id=random_uuid(),
                    group_name="group3",
                    unreliable_indicators=FindingUnreliableIndicatorsFaker(
                        vulnerabilities_summary=FindingVulnerabilitiesSummary(submitted=30)
                    ),
                )
            ],
        ),
        opensearch=IntegratesOpensearch(autoload=True),
    )
)
async def test_finding_external_roles_no_access(role: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=f"{role}@company.com",
        decorators=[
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", "group3"],
            [require_attribute, "has_asm", "group3"],
        ],
    )
    parent = GroupFaker(name="group3")
    result = await resolve(
        parent,
        info,
    )
    assert not result
