from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.groups.types import (
    Group,
)

from .schema import (
    GROUP,
)


@GROUP.field("subscription")
def resolve(
    parent: Group,
    _info: GraphQLResolveInfo,
) -> str:
    return str(parent.state.type.value).lower()
