from integrates.api.resolvers.group.git_roots import _get_must_filters
from integrates.testing.utils import parametrize


@parametrize(
    args=["input_args", "expected"],
    cases=[
        [{}, []],
        [{"cloning_status": "FAILED"}, [{"cloning.status": "FAILED"}, {"state.status": "ACTIVE"}]],
        [{"state": "ACTIVE"}, [{"state.status": "ACTIVE"}]],
        [
            {"cloning_status": "OK", "state": "ACTIVE"},
            [{"state.status": "ACTIVE"}, {"cloning.status": "OK"}],
        ],
        [
            {"cloning_status": "UNKNOWN", "state": "INACTIVE"},
            [{"state.status": "INACTIVE"}, {"cloning.status": "UNKNOWN"}],
        ],
    ],
)
def test_get_must_filters(input_args: dict[str, str], expected: list[dict[str, str]]) -> None:
    assert (
        _get_must_filters(
            cloning_status=input_args.get("cloning_status", ""),
            state=input_args.get("state", ""),
        )
        == expected
    )
