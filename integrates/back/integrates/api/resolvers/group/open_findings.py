from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.groups.types import (
    Group,
    GroupUnreliableIndicators,
)
from integrates.decorators import (
    require_asm,
)

from .schema import (
    GROUP,
)


@GROUP.field("openFindings")
@require_asm
async def resolve(
    parent: Group,
    info: GraphQLResolveInfo,
    **_kwargs: None,
) -> int | None:
    loaders: Dataloaders = info.context.loaders
    group_name: str = parent.name
    group_indicators: GroupUnreliableIndicators = await loaders.group_unreliable_indicators.load(
        group_name,
    )
    return int(group_indicators.open_findings if group_indicators.open_findings else 0)
