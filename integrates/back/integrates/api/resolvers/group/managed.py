from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.groups.enums import (
    GroupManaged,
)
from integrates.db_model.groups.types import (
    Group,
)

from .schema import (
    GROUP,
)


@GROUP.field("managed")
def resolve(
    parent: Group,
    _info: GraphQLResolveInfo,
) -> GroupManaged:
    return parent.state.managed
