from integrates.api.resolvers.group.git_environment_urls import resolve
from integrates.decorators import require_attribute_internal
from integrates.testing.aws import (
    IntegratesAws,
    IntegratesDynamodb,
)
from integrates.testing.fakers import (
    CredentialsFaker,
    CredentialsStateFaker,
    GitRootFaker,
    GitRootStateFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationAccessFaker,
    OrganizationFaker,
    RootEnvironmentUrlFaker,
    StakeholderFaker,
    random_uuid,
)
from integrates.testing.mocks import mocks

ORG_ID = "ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db"
ROOT_ID = "38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
CRED_ID = "credential-test-id"
GROUP_NAME = "grouptest"
USER_EMAIL = "jdoe@orgtest.com"
ORG_NAME = "orgtest"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            credentials=[
                CredentialsFaker(
                    credential_id=CRED_ID,
                    organization_id=ORG_ID,
                    state=CredentialsStateFaker(modified_by=USER_EMAIL, name="cred-test-name"),
                )
            ],
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            group_access=[
                GroupAccessFaker(
                    email=USER_EMAIL,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="user"),
                )
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(id=random_uuid(), root_id=ROOT_ID, group_name=GROUP_NAME)
            ],
            stakeholders=[StakeholderFaker(email=USER_EMAIL, enrolled=True)],
            organization_access=[OrganizationAccessFaker(organization_id=ORG_ID, email=USER_EMAIL)],
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            roots=[
                GitRootFaker(
                    id=ROOT_ID,
                    group_name=GROUP_NAME,
                    state=GitRootStateFaker(credential_id=CRED_ID, nickname="git1"),
                )
            ],
        )
    )
)
async def test_group_git_environment_urls() -> None:
    # Act
    parent = GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)
    info = GraphQLResolveInfoFaker(
        user_email=USER_EMAIL,
        decorators=[[require_attribute_internal, "is_under_review", GROUP_NAME]],
    )

    result = await resolve(parent=parent, info=info, group_name=GROUP_NAME)

    # Assert
    assert result
    assert len(result) == 1
    assert result[0].url == "https://fluidattacks.com"
    assert result[0].root_id == ROOT_ID
