from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.groups.types import (
    Group,
    GroupUnreliableIndicators,
)
from integrates.decorators import (
    require_is_not_under_review,
)

from .schema import (
    GROUP,
)


@GROUP.field("compliance")
@require_is_not_under_review
async def resolve(
    parent: Group,
    info: GraphQLResolveInfo,
    **_kwargs: None,
) -> GroupUnreliableIndicators:
    loaders: Dataloaders = info.context.loaders
    group_indicators: GroupUnreliableIndicators = await loaders.group_unreliable_indicators.load(
        parent.name,
    )
    return group_indicators
