from typing import TypedDict, Unpack

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.db_model.roots.enums import RootDockerImageStateStatus
from integrates.db_model.roots.types import (
    GroupDockerImagesRequest,
    RootDockerImage,
)

from .schema import (
    GROUP,
)


class Kwargs(TypedDict, total=False):
    include: bool


def filter_images(images: list[RootDockerImage], include: bool | None) -> list[RootDockerImage]:
    filtered_images = [
        image for image in images if image.state.status is not RootDockerImageStateStatus.DELETED
    ]

    if include is not None:
        filtered_images = [image for image in filtered_images if image.state.include is include]

    return filtered_images


@GROUP.field("dockerImages")
async def resolve(
    parent: Group, info: GraphQLResolveInfo, **kwargs: Unpack[Kwargs]
) -> list[RootDockerImage]:
    include = kwargs.get("include")
    loaders: Dataloaders = info.context.loaders
    images = await loaders.group_docker_images.load(
        GroupDockerImagesRequest(group_name=parent.name),
    )

    return filter_images(images, include)
