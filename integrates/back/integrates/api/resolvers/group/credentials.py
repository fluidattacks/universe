from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.credentials.types import (
    Credentials,
    CredentialsRequest,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.db_model.roots.types import (
    GitRoot,
)

from .schema import (
    GROUP,
)


@GROUP.field("credentials")
async def resolve(
    parent: Group,
    info: GraphQLResolveInfo,
    **_kwargs: None,
) -> list[Credentials | None]:
    loaders: Dataloaders = info.context.loaders
    group_roots = await loaders.group_roots.load(parent.name)
    group_credential_ids = {
        root.state.credential_id
        for root in group_roots
        if isinstance(root, GitRoot) and root.state.credential_id
    }
    group_credentials = await loaders.credentials.load_many(
        tuple(
            CredentialsRequest(
                id=credential_id,
                organization_id=parent.organization_id,
            )
            for credential_id in group_credential_ids
        ),
    )

    return group_credentials
