from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.db_model.hook.types import (
    GroupHook,
)
from integrates.decorators import (
    require_is_not_under_review,
)

from .schema import (
    GROUP,
)


@GROUP.field("hook")
@require_is_not_under_review
async def resolve(parent: Group, info: GraphQLResolveInfo, **_kwargs: None) -> list[GroupHook]:
    loaders: Dataloaders = info.context.loaders
    group_name: str = parent.name

    return await loaders.group_hook.load(group_name)
