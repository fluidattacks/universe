from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.db_model.organizations.types import (
    Organization,
)
from integrates.organizations.utils import (
    get_organization,
)

from .schema import (
    GROUP,
)


@GROUP.field("organization")
async def resolve(
    parent: Group,
    info: GraphQLResolveInfo,
) -> str:
    loaders: Dataloaders = info.context.loaders
    org_id = parent.organization_id
    organization: Organization = await get_organization(loaders, org_id)
    organization_name = organization.name

    return organization_name
