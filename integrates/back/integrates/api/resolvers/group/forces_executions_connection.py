from typing import (
    Any,
    cast,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.class_types.types import (
    Item,
)
from integrates.custom_utils.forces import (
    format_forces_to_resolve,
)
from integrates.db_model.forces.types import (
    ExecutionEdge,
    ExecutionsConnection,
)
from integrates.db_model.forces.utils import (
    format_forces_execution,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.db_model.items import (
    ForcesExecutionItem,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_is_not_under_review,
    require_login,
)
from integrates.search.enums import (
    Sort,
)
from integrates.search.operations import (
    SearchParams,
    search,
)

from .schema import (
    GROUP,
)


def executions_filter(**kwargs: str) -> dict[str, str]:
    exec_must_filters: list[dict[str, str]] = must_filter(**kwargs)
    exec_must_match_prefix_filters: list[dict[str, str]] = must_match_prefix_filter(**kwargs)
    exec_must_range_filters: list[dict[str, str]] = must_range_filter(**kwargs)

    filters: Item = {
        "must_filters": exec_must_filters,
        "must_match_prefix_filters": exec_must_match_prefix_filters,
        "must_range_filters": exec_must_range_filters,
    }

    return filters


def must_filter(**kwargs: str) -> list[dict[str, str]]:
    must_filters = []

    if execution_type := kwargs.get("type"):
        must_filters.append({"kind": str(execution_type).upper()})

    if strictness := kwargs.get("strictness"):
        must_filters.append({"strictness": str(strictness).upper()})

    if exit_code := kwargs.get("exit_code"):
        must_filters.append({"exit_code": str(exit_code)})

    return must_filters


def must_match_prefix_filter(**kwargs: str) -> list[dict[str, str]]:
    must_match_filters = []

    if repo := kwargs.get("git_repo"):
        must_match_filters.append({"repo": str(repo)})

    return must_match_filters


def must_range_filter(**kwargs: Any) -> list[dict[str, str]]:
    must_range_filters: list[Item] = []

    if from_date := kwargs.get("from_date"):
        must_range_filters.append({"execution_date": {"gte": str(from_date.date())}})

    if to_date := kwargs.get("to_date"):
        must_range_filters.append({"execution_date": {"lte": str(to_date.date())}})

    if status := kwargs.get("status"):
        must_range_filters.append(
            {
                "vulnerabilities.num_of_open_vulnerabilities": {"gt": 0}
                if str(status).lower() == "vulnerable"
                else {"lte": 0},
            },
        )

    return must_range_filters


def exact_match_filter(group_name: str, **kwargs: str) -> dict[str, str]:
    exact_match_filters = {"group_name": group_name}

    if repo := kwargs.get("git_repo_exact_filter"):
        exact_match_filters["repo.keyword"] = str(repo)

    return exact_match_filters


@GROUP.field("forcesExecutionsConnection")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_asm,
)
async def resolve(
    parent: Group,
    _info: GraphQLResolveInfo,
    **kwargs: Any,
) -> ExecutionsConnection:
    group_name: str = parent.name
    executions_filters: Item = executions_filter(**kwargs)

    first = kwargs.get("first") or 10
    query = kwargs.get("search")
    after = kwargs.get("after")

    results = await search(
        SearchParams(
            after=after,
            limit=first,
            query=query,
            type_query="phrase_prefix",
            must_filters=executions_filters["must_filters"],
            must_match_prefix_filters=executions_filters["must_match_prefix_filters"],
            range_filters=executions_filters["must_range_filters"],
            exact_filters=exact_match_filter(group_name=group_name, **kwargs),
            index_value="executions_index",
            sort_by=[
                {"execution_date": {"order": Sort.DESCENDING.value}},
                {"pk.keyword": {"order": Sort.DESCENDING.value}},
            ],
        ),
    )

    forces_executions = tuple(
        format_forces_execution(cast(ForcesExecutionItem, item)) for item in results.items
    )
    executions_formatted = [format_forces_to_resolve(execution) for execution in forces_executions]
    return ExecutionsConnection(
        edges=tuple(
            ExecutionEdge(
                cursor=results.page_info.end_cursor,
                node=execution,
            )
            for execution in executions_formatted
        ),
        page_info=results.page_info,
        total=results.total,
    )
