from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates import (
    authz,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.decorators import (
    enforce_group_level_auth_async,
)

from .schema import (
    GROUP,
)


@GROUP.field("serviceAttributes")
@enforce_group_level_auth_async
def resolve(
    parent: Group,
    _info: GraphQLResolveInfo,
    **_kwargs: None,
) -> list[str]:
    return sorted(authz.get_group_service_attributes(parent))
