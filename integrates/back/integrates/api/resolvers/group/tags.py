from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.groups.types import (
    Group,
)

from .schema import (
    GROUP,
)


@GROUP.field("tags")
def resolve(
    parent: Group,
    _info: GraphQLResolveInfo,
) -> list[str] | None:
    return list(parent.state.tags) if parent.state.tags else None
