from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.groups.enums import (
    GroupStateStatus,
)
from integrates.db_model.groups.types import (
    Group,
)

from .schema import (
    GROUP,
)


@GROUP.field("hasForces")
def resolve(
    parent: Group,
    _info: GraphQLResolveInfo,
) -> bool:
    return parent.state.status == GroupStateStatus.ACTIVE
