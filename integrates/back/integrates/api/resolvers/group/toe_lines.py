from typing import (
    Any,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.class_types.types import (
    Item,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.db_model.items import ToeLinesItem
from integrates.db_model.toe_lines.types import (
    ToeLineEdge,
    ToeLinesConnection,
)
from integrates.db_model.toe_lines.utils import (
    format_toe_lines,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    rename_kwargs,
    require_is_not_under_review,
    validate_connection,
)
from integrates.search.operations import (
    SearchClient,
    SearchParams,
)

from .schema import (
    GROUP,
)


@GROUP.field("toeLines")
@rename_kwargs(
    {
        "from_modified_date": "from_last_commit_date",
        "to_modified_date": "to_last_commit_date",
    },
)
@concurrent_decorators(
    enforce_group_level_auth_async,
    validate_connection,
    require_is_not_under_review,
)
async def resolve(
    parent: Group,
    _info: GraphQLResolveInfo,
    **kwargs: Any,
) -> ToeLinesConnection:
    toe_lines_filters: Item = toe_lines_filter(**kwargs)

    results = await SearchClient[ToeLinesItem].search(
        SearchParams(
            after=kwargs.get("after"),
            exact_filters={"group_name": parent.name},
            must_filters=toe_lines_filters["must_filters"],
            must_match_prefix_filters=toe_lines_filters["must_match_filters"],
            range_filters=toe_lines_filters["must_range_filters"],
            index_value="lines_index",
            limit=kwargs.get("first") or 10,
        ),
    )

    toe_lines = tuple(format_toe_lines(result) for result in results.items)
    response = ToeLinesConnection(
        edges=tuple(
            ToeLineEdge(
                cursor=results.page_info.end_cursor,
                node=toe_line,
            )
            for toe_line in toe_lines
        ),
        page_info=results.page_info,
        total=results.total,
    )

    return response


def toe_lines_filter(**kwargs: str) -> Item:
    vulns_must_filters: list[dict[str, str]] = must_filter(**kwargs)
    vulns_must_match_prefix_filters: list[dict[str, str]] = must_match_prefix_filter(**kwargs)
    exec_must_range_filters: list[Item] = must_range_filter(**kwargs)

    filters: Item = {
        "must_filters": vulns_must_filters,
        "must_match_filters": vulns_must_match_prefix_filters,
        "must_range_filters": exec_must_range_filters,
    }

    return filters


def get_items_to_filter(
    filters: Item,
    kwargs: Any,
    parameter: str | None = None,
    range_condition: str | None = None,
) -> list[dict[str, str]]:
    items_to_filter = [
        {
            (field if path == "common" else f"{path}.{field}"): (
                {range_condition: filter_value} if range_condition else filter_value
            ),
        }
        for path, fields in filters.items()
        for field in fields
        if (filter_value := kwargs.get(f"{parameter}_{field}" if parameter else field))
        not in [None, ""]
    ]
    return items_to_filter


def must_filter(**kwargs: str) -> list[dict[str, str]]:
    filters: Item = {
        "common": ["root_id"],
        "state": ["be_present", "has_vulnerabilities"],
    }
    must_filters = get_items_to_filter(filters, kwargs)

    return must_filters


def must_match_prefix_filter(**kwargs: str) -> list[dict[str, str]]:
    filters: Item = {
        "common": ["filename"],
        "state": ["attacked_by", "comments", "last_commit", "last_author"],
    }

    must_match_filters = get_items_to_filter(filters, kwargs)

    return must_match_filters


def must_range_filter(**kwargs: str) -> list[dict[str, str]]:
    from_to_filters: Item = {
        "state": [
            "seen_at",
            "first_attack_at",
            "attacked_at",
            "be_present_until",
            "last_commit_date",
        ],
    }

    min_max_filters: Item = {
        "state": [
            "loc",
            "attacked_lines",
            "sorts_priority_factor",
            "sorts_risk_level",
        ],
    }

    must_range_filters: list[Item] = [
        *get_items_to_filter(from_to_filters, kwargs, "from", "gte"),
        *get_items_to_filter(min_max_filters, kwargs, "min", "gte"),
        *get_items_to_filter(from_to_filters, kwargs, "to", "lte"),
        *get_items_to_filter(min_max_filters, kwargs, "max", "lte"),
    ]

    return must_range_filters
