from integrates.api.resolvers.group.forces_vulnerabilities import resolve
from integrates.db_model.enums import Source
from integrates.db_model.findings.enums import FindingStatus
from integrates.db_model.findings.types import FindingVulnerabilitiesSummary
from integrates.decorators import require_attribute_internal
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    FindingFaker,
    FindingStateFaker,
    FindingUnreliableIndicatorsFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationAccessFaker,
    OrganizationFaker,
    StakeholderFaker,
    VulnerabilityFaker,
    VulnerabilityStateFaker,
    random_uuid,
)
from integrates.testing.mocks import mocks

ORG_ID = "ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db"
FIN_ID = "4222865126-2120-6701-9896-28519594d737"
GROUP_NAME = "grouptest"
USER_EMAIL = "jdoe@orgtest.com"
ORG_NAME = "orgtest"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            findings=[
                FindingFaker(
                    id=FIN_ID,
                    group_name=GROUP_NAME,
                    state=FindingStateFaker(modified_by=USER_EMAIL, source=Source.ANALYST),
                    unreliable_indicators=FindingUnreliableIndicatorsFaker(
                        unreliable_status=FindingStatus.VULNERABLE,
                        vulnerabilities_summary=FindingVulnerabilitiesSummary(open=2, closed=0),
                    ),
                )
            ],
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            group_access=[
                GroupAccessFaker(
                    email=USER_EMAIL,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="group_manager"),
                )
            ],
            stakeholders=[StakeholderFaker(email=USER_EMAIL, enrolled=True)],
            organization_access=[OrganizationAccessFaker(organization_id=ORG_ID, email=USER_EMAIL)],
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            vulnerabilities=[
                VulnerabilityFaker(
                    id=random_uuid(),
                    created_by=USER_EMAIL,
                    finding_id=FIN_ID,
                    organization_name=ORG_NAME,
                    group_name=GROUP_NAME,
                    state=VulnerabilityStateFaker(modified_by=USER_EMAIL, specific="10"),
                ),
                VulnerabilityFaker(
                    id=random_uuid(),
                    created_by=USER_EMAIL,
                    finding_id=FIN_ID,
                    organization_name=ORG_NAME,
                    group_name=GROUP_NAME,
                    state=VulnerabilityStateFaker(modified_by=USER_EMAIL, specific="99"),
                ),
            ],
        )
    )
)
async def test_group_forces_vulnerabilities() -> None:
    # Act
    parent = GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)
    info = GraphQLResolveInfoFaker(
        user_email=USER_EMAIL,
        decorators=[
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )

    result = await resolve(
        parent=parent, info=info, group_name=GROUP_NAME, after=None, first=None, state="VULNERABLE"
    )

    # Assert
    assert result
    assert len(result.edges) == 2
    assert sorted([vuln.node.state.specific for vuln in result.edges]) == ["10", "99"]
