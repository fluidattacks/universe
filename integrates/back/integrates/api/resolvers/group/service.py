from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.groups.types import (
    Group,
)

from .schema import (
    GROUP,
)


@GROUP.field("service")
def resolve(
    parent: Group,
    _info: GraphQLResolveInfo,
) -> str | None:
    if parent.state.service:
        return parent.state.service.value
    return None
