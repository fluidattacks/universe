from integrates.api.resolvers.group.credentials import resolve
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    CredentialsFaker,
    CredentialsStateFaker,
    GitRootFaker,
    GitRootStateFaker,
    GraphQLResolveInfoFaker,
    GroupFaker,
    OrganizationAccessFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks

ORG_ID = "ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db"
ROOT_ID = "38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
CRED_ID = "credential-test-id"
CRED_NAME = "credential-test-name"
GROUP_NAME = "grouptest"
USER_EMAIL = "jdoe@orgtest.com"
ORG_NAME = "orgtest"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            credentials=[
                CredentialsFaker(
                    credential_id=CRED_ID,
                    organization_id=ORG_ID,
                    state=CredentialsStateFaker(modified_by=USER_EMAIL, name=CRED_NAME),
                )
            ],
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            stakeholders=[StakeholderFaker(email=USER_EMAIL, enrolled=True)],
            organization_access=[OrganizationAccessFaker(organization_id=ORG_ID, email=USER_EMAIL)],
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            roots=[
                GitRootFaker(
                    id=ROOT_ID,
                    group_name=GROUP_NAME,
                    state=GitRootStateFaker(credential_id=CRED_ID, nickname="git1"),
                )
            ],
        )
    )
)
async def test_group_credentials() -> None:
    # Act
    parent = GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)
    info = GraphQLResolveInfoFaker(user_email=USER_EMAIL)

    result = await resolve(parent=parent, info=info)

    # Assert
    assert result
    assert result[0]
    assert result[0].id == CRED_ID
    assert result[0].state.name == CRED_NAME
