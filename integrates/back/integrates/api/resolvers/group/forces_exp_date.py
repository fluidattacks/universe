from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_exceptions import (
    ExpiredForcesToken,
    ExpiredToken,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_login,
)
from integrates.forces import (
    domain as forces_domain,
)

from .schema import (
    GROUP,
)


@GROUP.field("forcesExpDate")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
)
def resolve(
    parent: Group,
    _info: GraphQLResolveInfo,
    **_kwargs: None,
) -> str | None:
    if not isinstance(parent.agent_token, str):
        return None

    try:
        return str(forces_domain.get_expiration_date(parent.agent_token).date())
    except ExpiredToken as exc:
        raise ExpiredForcesToken() from exc
