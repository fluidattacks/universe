import logging
from typing import (
    NotRequired,
    TypedDict,
    Unpack,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.class_types.types import (
    Item,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.db_model.items import ToePackagesItem
from integrates.db_model.toe_packages.types import (
    ToePackage,
    ToePackageEdge,
    ToePackagesConnection,
)
from integrates.db_model.toe_packages.utils import (
    format_toe_package,
)
from integrates.decorators import (
    concurrent_decorators,
    require_is_not_under_review,
    validate_connection,
)
from integrates.search.enums import (
    Sort,
)
from integrates.search.operations import (
    SearchClient,
    SearchParams,
)

from .schema import (
    GROUP,
)

LOGGER = logging.getLogger(__name__)


def safe_format_toe_package(item: ToePackagesItem) -> ToePackage | None:
    try:
        return format_toe_package(item)
    except KeyError as ex:
        LOGGER.error(("Missing key: %s", ex))
        return None


class ResolverArgs(TypedDict):
    after: NotRequired[str | None]
    be_present: NotRequired[bool | None]
    feature_flag: bool
    first: NotRequired[int | None]
    name: NotRequired[str | None]
    outdated: NotRequired[bool | None]
    path: NotRequired[str | None]
    platform: NotRequired[str | None]
    root_id: NotRequired[str | None]
    search: NotRequired[str | None]
    vulnerable: NotRequired[bool | None]


@GROUP.field("toePackages")
@concurrent_decorators(validate_connection, require_is_not_under_review)
async def resolve(
    parent: Group,
    _: GraphQLResolveInfo,
    **kwargs: Unpack[ResolverArgs],
) -> ToePackagesConnection:
    def _get_must_filters() -> list[Item]:
        must_filters: list[Item] = [{"be_present": True}]

        if root_id := kwargs.get("root_id"):
            must_filters.append({"root_id": root_id})

        vulnerable = kwargs.get("vulnerable")
        if vulnerable is not None:
            must_filters.append({"vulnerable": vulnerable})

        outdated = kwargs.get("outdated")
        if outdated is not None:
            must_filters.append({"outdated": outdated})

        return must_filters

    def _get_must_match_prefix_filters() -> list[dict[str, str]]:
        must_match_filters = []

        if name := kwargs.get("name"):
            must_match_filters.append({"name": name})

        if platform := kwargs.get("platform"):
            must_match_filters.append({"platform": platform})

        return must_match_filters

    def _get_should_filters() -> list[Item]:
        should_filters = {}

        platforms = kwargs.get("platforms")
        if isinstance(platforms, list) and platforms:
            should_filters["platform"] = " OR ".join(platforms)

        return [should_filters]

    first = kwargs.get("first")
    results = await SearchClient[ToePackagesItem].search(
        SearchParams(
            after=kwargs.get("after"),
            exact_filters={"group_name": parent.name},
            index_value="pkgs_index",
            limit=first if first is not None else 10,
            must_filters=_get_must_filters(),
            must_match_prefix_filters=_get_must_match_prefix_filters(),
            query=kwargs.get("search"),
            should_filters=_get_should_filters(),
            sort_by=[
                {"has_related_vulnerabilities": {"order": Sort.DESCENDING.value}},
                {"vulnerable": {"order": Sort.DESCENDING.value}},
                {"outdated": {"order": Sort.DESCENDING.value}},
                {"name.keyword": {"order": Sort.ASCENDING.value}},
            ],
            type_query="phrase_prefix",
        ),
    )
    edges = tuple(
        ToePackageEdge(
            cursor=results.page_info.end_cursor,
            node=result,
        )
        for item in results.items
        if (result := safe_format_toe_package(item)) is not None
    )

    return ToePackagesConnection(
        edges=edges,
        page_info=results.page_info,
        total=results.total,
    )
