from datetime import datetime

from integrates.api.resolvers.group.events import resolve
from integrates.dataloaders import get_new_context
from integrates.db_model.events.enums import EventStateStatus
from integrates.db_model.events.types import Event
from integrates.db_model.roots.enums import (
    RootEnvironmentCloud,
    RootEnvironmentUrlStateStatus,
    RootEnvironmentUrlType,
)
from integrates.db_model.roots.types import RootEnvironmentUrl, RootEnvironmentUrlState
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute,
    require_attribute_internal,
    require_is_not_under_review,
)
from integrates.findings.domain.events import manage_cspm_environment_events
from integrates.roots.domain import format_environment_id
from integrates.sessions import domain as sessions_domain
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    EventFaker,
    EventStateFaker,
    GitRootFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    RootEnvironmentUrlFaker,
    RootEnvironmentUrlStateFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize


@parametrize(
    args=["email"],
    cases=[
        ["admin@gmail.com"],
        ["hacker@gmail.com"],
        ["reattacker@gmail.com"],
        ["user@gmail.com"],
        ["group_manager@gmail.com"],
        ["vulnerability_manager@gmail.com"],
        ["resourcer@gmail.com"],
        ["reviewer@gmail.com"],
        ["customer_manager@fluidattacks.com"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            events=[
                EventFaker(
                    id="418900971",
                    description="Resolver test1",
                    root_id="root1",
                    state=EventStateFaker(status=EventStateStatus.CREATED),
                    n_holds=0,
                ),
                EventFaker(
                    id="418900980",
                    description="Resolver test2",
                    root_id="root1",
                    state=EventStateFaker(status=EventStateStatus.CREATED),
                    n_holds=0,
                ),
            ],
            groups=[
                GroupFaker(organization_id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email="admin@gmail.com"),
                StakeholderFaker(email="hacker@gmail.com"),
                StakeholderFaker(email="reattacker@gmail.com"),
                StakeholderFaker(email="user@gmail.com"),
                StakeholderFaker(email="group_manager@gmail.com"),
                StakeholderFaker(email="vulnerability_manager@gmail.com"),
                StakeholderFaker(email="resourcer@gmail.com"),
                StakeholderFaker(email="reviewer@gmail.com"),
                StakeholderFaker(email="customer_manager@fluidattacks.com"),
            ],
            group_access=[
                GroupAccessFaker(
                    email="admin@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
                GroupAccessFaker(
                    email="hacker@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
                GroupAccessFaker(
                    email="reattacker@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="reattacker"),
                ),
                GroupAccessFaker(
                    email="user@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                ),
                GroupAccessFaker(
                    email="group_manager@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
                GroupAccessFaker(
                    email="vulnerability_manager@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="vulnerability_manager"),
                ),
                GroupAccessFaker(
                    email="resourcer@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="resourcer"),
                ),
                GroupAccessFaker(
                    email="reviewer@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="reviewer"),
                ),
                GroupAccessFaker(
                    email="customer_manager@fluidattacks.com",
                    state=GroupAccessStateFaker(has_access=True, role="customer_manager"),
                ),
            ],
            organizations=[
                OrganizationFaker(id="org1"),
            ],
        ),
    )
)
async def test_get_group_events(email: str) -> None:
    expected_events = [
        EventFaker(
            id="418900971",
            description="Resolver test1",
            root_id="root1",
            state=EventStateFaker(status=EventStateStatus.CREATED),
            n_holds=0,
        ),
        EventFaker(
            id="418900980",
            description="Resolver test2",
            root_id="root1",
            state=EventStateFaker(status=EventStateStatus.CREATED),
            n_holds=0,
        ),
    ]

    events: list[Event] = await resolve(
        GroupFaker(organization_id="org1"),
        info=GraphQLResolveInfoFaker(
            user_email=email,
            decorators=[
                [enforce_group_level_auth_async],
                [require_is_not_under_review],
                [require_attribute_internal, "is_under_review", "test-group"],
                [require_attribute, "has_asm", "test-group"],
            ],
        ),
    )

    assert events == expected_events


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            events=[
                EventFaker(
                    id="123456789",
                    description=(
                        "Failed to request session token: The environment "
                        "arn:aws:iam::123456789012:role/CSPMRole is invalid or misconfigured. "
                        "Please check the credentials permissions and configuration."
                    ),
                    environment_url="arn:aws:iam::123456789012:role/CSPMRole",
                    state=EventStateFaker(status=EventStateStatus.OPEN),
                ),
            ],
            groups=[
                GroupFaker(organization_id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email="admin@gmail.com"),
            ],
            group_access=[
                GroupAccessFaker(
                    email="admin@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            roots=[GitRootFaker(id="git-root-1", organization_name="org1")],
            root_environment_urls=[
                RootEnvironmentUrlFaker(
                    url="arn:aws:iam::123456789012:role/CSPMRole",
                    root_id="git-root-1",
                    group_name="test-group",
                    state=RootEnvironmentUrlStateFaker(
                        modified_by="admin@gmail.com",
                        modified_date=datetime.fromisoformat("2020-11-19T13:37:10+00:00"),
                        status=RootEnvironmentUrlStateStatus.CREATED,
                        url_type=RootEnvironmentUrlType.CSPM,
                        cloud_name=RootEnvironmentCloud.AWS,
                    ),
                )
            ],
        ),
    )
)
async def test_cspm_manage_events() -> None:
    test_email = "admin@gmail.com"
    info = GraphQLResolveInfoFaker(
        user_email=test_email,
        decorators=[
            [enforce_group_level_auth_async],
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", "test-group"],
            [require_attribute, "has_asm", "test-group"],
        ],
    )

    events: list[Event] = await resolve(GroupFaker(organization_id="org1"), info=info)

    assert len(events) == 1 and events[0].state.status == EventStateStatus.OPEN

    parent = RootEnvironmentUrl(
        url="arn:aws:iam::123456789012:role/CSPMRole",
        id=format_environment_id("https://test.com/"),
        root_id="git-root-1",
        group_name="test-group",
        state=RootEnvironmentUrlState(
            modified_by="admin@gmail.com",
            modified_date=datetime.fromisoformat("2020-11-19T13:37:10+00:00"),
            status=RootEnvironmentUrlStateStatus.CREATED,
            url_type=RootEnvironmentUrlType.CSPM,
            cloud_name=RootEnvironmentCloud.AWS,
        ),
    )

    user_info: dict[str, str] = await sessions_domain.get_jwt_content(info.context)
    await manage_cspm_environment_events(
        loaders=get_new_context(),
        env_url=parent,
        is_role_valid=True,
        user_info=user_info,
    )
    info.context.loaders.group_events.clear_all()
    info.context.loaders.event.clear_all()
    events_after_cspsm: list[Event] = await resolve(GroupFaker(organization_id="org1"), info=info)

    assert len(events_after_cspsm) == 1
    assert events_after_cspsm[0].state.status == EventStateStatus.SOLVED
