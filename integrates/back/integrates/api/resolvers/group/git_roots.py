from typing import (
    NamedTuple,
    TypedDict,
    Unpack,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.class_types.types import (
    Item,
)
from integrates.custom_utils.validations_deco import (
    validate_all_fields_length_deco,
    validate_fields_deco,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.db_model.items import GitRootItem
from integrates.db_model.roots.types import (
    GitRootEdge,
    GitRootsConnection,
)
from integrates.db_model.roots.utils import (
    format_git_root,
)
from integrates.search.operations import (
    SearchClient,
    SearchParams,
)

from .schema import (
    GROUP,
)


class Kwargs(TypedDict, total=False):
    after: str
    branch: str
    cloning_status: str
    first: int
    includes_health_check: bool
    nickname: str
    search: str
    state: str


class FilterInfo(NamedTuple):
    attr_name: str


def _get_exact_filters(group_name: str) -> Item:
    exact_filters = {
        "sk.keyword": f"GROUP#{group_name}",
        "type.keyword": "Git",
    }

    return exact_filters


def _get_must_filters(**kwargs: Unpack[Kwargs]) -> list[dict]:
    has_cloning_status_filter = False
    must_filters = []
    entity_attr_filters: dict[str, FilterInfo] = {
        "includes_health_check": FilterInfo(
            attr_name="state.includes_health_check",
        ),
        "state": FilterInfo(
            attr_name="state.status",
        ),
        "cloning_status": FilterInfo(
            attr_name="cloning.status",
        ),
    }
    for filter_name, info in entity_attr_filters.items():
        if filter_value := kwargs.get(filter_name):
            must_filters.append({info.attr_name: filter_value})
            if filter_name == "cloning_status":
                has_cloning_status_filter = True

    if has_cloning_status_filter:
        has_state_filter = [
            _filter for _filter in must_filters if entity_attr_filters["state"].attr_name in _filter
        ]
        if not has_state_filter:
            must_filters.append({entity_attr_filters["state"].attr_name: "ACTIVE"})
    return must_filters


def _get_must_match_prefix_filters(**kwargs: Unpack[Kwargs]) -> list[dict]:
    must_match_prefix_filters = []
    entity_attr_filters: dict[str, FilterInfo] = {
        "nickname": FilterInfo(
            attr_name="state.nickname",
        ),
        "branch": FilterInfo(
            attr_name="state.branch",
        ),
    }
    for filter_name, info in entity_attr_filters.items():
        if filter_value := kwargs.get(filter_name):
            must_match_prefix_filters.append({info.attr_name: filter_value})
    return must_match_prefix_filters


@GROUP.field("gitRoots")
@validate_all_fields_length_deco(max_length=300)
@validate_fields_deco(
    [
        "after",
        "branch",
        "nickname",
        "search",
    ],
)
async def resolve(
    parent: Group,
    _info: GraphQLResolveInfo,
    **kwargs: Unpack[Kwargs],
) -> GitRootsConnection:
    group_name = parent.name
    results = await SearchClient[GitRootItem].search(
        SearchParams(
            after=kwargs.get("after"),
            exact_filters=_get_exact_filters(group_name),
            must_filters=_get_must_filters(**kwargs),
            must_match_prefix_filters=_get_must_match_prefix_filters(**kwargs),
            index_value="roots_index",
            type_query="phrase_prefix",
            limit=kwargs.get("first") or 100,
            query=kwargs.get("search"),
        ),
    )
    return GitRootsConnection(
        edges=tuple(
            GitRootEdge(
                cursor=results.page_info.end_cursor,
                node=format_git_root(item),
            )
            for item in results.items
        ),
        page_info=results.page_info,
        total=results.total,
    )
