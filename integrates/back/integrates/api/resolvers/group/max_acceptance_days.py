from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.groups.types import (
    Group,
)
from integrates.vulnerabilities.domain.validations import (
    get_policy_max_acceptance_days,
)

from .schema import (
    GROUP,
)


@GROUP.field("maxAcceptanceDays")
async def resolve(
    parent: Group,
    info: GraphQLResolveInfo,
    **_kwargs: None,
) -> int | None:
    return await get_policy_max_acceptance_days(
        loaders=info.context.loaders,
        group_name=parent.name,
    )
