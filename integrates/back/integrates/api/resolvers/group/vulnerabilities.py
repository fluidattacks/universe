import logging
from typing import (
    Any,
)

from graphql import (
    GraphQLResolveInfo,
)

from integrates.class_types.types import (
    Item,
)
from integrates.custom_utils.vulnerabilities import (
    get_inverted_state_converted,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.db_model.items import VulnerabilityItem
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
    VulnerabilityTechnique,
    VulnerabilityZeroRiskStatus,
)
from integrates.db_model.vulnerabilities.types import (
    VulnerabilitiesConnection,
    Vulnerability,
    VulnerabilityEdge,
)
from integrates.db_model.vulnerabilities.utils import (
    format_vulnerability,
)
from integrates.decorators import (
    require_is_not_under_review,
)
from integrates.search.operations import (
    SearchClient,
    SearchParams,
)
from integrates.treatment.utils import get_inverted_treatment_converted
from integrates.vulnerabilities.domain.core import (
    get_vulns_filters_by_root_nickname,
)

from .schema import (
    GROUP,
)

LOGGER = logging.getLogger(__name__)


def get_vulnerability(item: VulnerabilityItem, info: GraphQLResolveInfo) -> Vulnerability:
    vulnerability = format_vulnerability(item)
    if (
        info.context.operation.name == "ForcesGetGroupLocations"
        and vulnerability.technique == VulnerabilityTechnique.PTAAS
    ):
        return vulnerability._replace(technique=VulnerabilityTechnique.MPT)

    return vulnerability


@GROUP.field("vulnerabilities")
@require_is_not_under_review
async def resolve(
    parent: Group,
    info: GraphQLResolveInfo,
    **kwargs: Any,
) -> VulnerabilitiesConnection:
    group_name = parent.name
    loaders: Dataloaders = info.context.loaders
    vulnerabilities_filters: Item = await vulnerabilities_filter(loaders, group_name, **kwargs)

    results = await SearchClient[VulnerabilityItem].search(
        SearchParams(
            after=kwargs.get("after"),
            exact_filters={"group_name": parent.name},
            index_value="vulns_index",
            limit=kwargs.get("first") or 10,
            must_filters=vulnerabilities_filters["must_filters"],
            must_not_filters=vulnerabilities_filters["must_not_filters"],
            query=kwargs.get("search"),
            range_filters=vulnerabilities_filters["must_range_filters"],
            should_and_filters=vulnerabilities_filters["should_and_filters"],
            should_match_prefix_filters=vulnerabilities_filters["should_match_prefix_filters"],
            type_query="phrase_prefix",
        ),
    )

    vulnerabilities = tuple(get_vulnerability(item, info) for item in results.items)

    return VulnerabilitiesConnection(
        edges=tuple(
            VulnerabilityEdge(
                cursor=results.page_info.end_cursor,
                node=vulnerability,
            )
            for vulnerability in vulnerabilities
        ),
        page_info=results.page_info,
        total=results.total,
    )


async def vulnerabilities_filter(loaders: Dataloaders, group_name: str, **kwargs: Any) -> Item:
    vulns_must_filters: list[dict[str, str]] = must_filter(**kwargs)
    vulns_should_match_prefix_filters: list[dict[str, str]] = should_match_prefix_filter(**kwargs)
    vulns_must_not_filters: list[dict[str, str]] = must_not_filter(**kwargs)
    vulns_must_range_filters = must_range_filter(**kwargs)
    vulns_should_and_filter = await should_and_filter(loaders, group_name, **kwargs)

    if zero_risk := kwargs.get("zero_risk"):
        vulns_must_filters.append({"zero_risk.status": zero_risk})
    else:
        vulns_must_not_filters.append({"zero_risk.status": VulnerabilityZeroRiskStatus.REQUESTED})

    filters: Item = {
        "must_filters": vulns_must_filters,
        "must_not_filters": vulns_must_not_filters,
        "must_range_filters": vulns_must_range_filters,
        "should_and_filters": vulns_should_and_filter,
        "should_match_prefix_filters": vulns_should_match_prefix_filters,
    }

    return filters


def must_filter(**kwargs: Any) -> list[dict[str, str]]:
    must_filters = []

    if technique := kwargs.get("technique"):
        must_filters.append({"technique": str(technique).upper()})

    if vulnerability_type := kwargs.get("type"):
        must_filters.append({"type": str(vulnerability_type).upper()})

    if state := kwargs.get("state"):
        must_filters.append({"state.status": get_inverted_state_converted(str(state).upper())})

    if treatment := kwargs.get("treatment"):
        must_filters.append(
            {"treatment.status": get_inverted_treatment_converted(str(treatment).upper())},
        )

    if verification := kwargs.get("verification_status"):
        if verification != "NotRequested":
            must_filters.append({"verification.status": str(verification).upper()})

    if external_bts := kwargs.get("external_bug_tracking_system"):
        must_filters.append({"bug_tracking_system_url": external_bts})

    return must_filters


def should_match_prefix_filter(**kwargs: Any) -> list[dict[str, str]]:
    should_match_prefix_filters = []

    if root := kwargs.get("root"):
        should_match_prefix_filters.append({"state.where": str(root)})

    return should_match_prefix_filters


def must_not_filter(**kwargs: Any) -> list[Item]:
    must_not_filters: list[Item] = [
        {"state.status": VulnerabilityStateStatus.DELETED.value},
        {"state.status": VulnerabilityStateStatus.MASKED.value},
        {"state.status": VulnerabilityStateStatus.REJECTED.value},
        {"state.status": VulnerabilityStateStatus.SUBMITTED.value},
        {"zero_risk.status": VulnerabilityZeroRiskStatus.CONFIRMED.value},
    ]
    if verification := kwargs.get("verification_status"):
        if verification == "NotRequested":
            must_not_filters.append({"verification.status": "REQUESTED"})
            must_not_filters.append({"verification.status": "ON_HOLD"})
            must_not_filters.append({"state.status": VulnerabilityStateStatus.SAFE.value})

    return must_not_filters


def must_range_filter(**kwargs: Any) -> list[dict[str, str]]:
    must_range_filters: list[Item] = []

    if closed_after := kwargs.get("closed_after"):
        must_range_filters.append(
            {"unreliable_indicators.unreliable_closing_date": {"gt": str(closed_after.date())}},
        )

    if closed_before := kwargs.get("closed_before"):
        must_range_filters.append(
            {"unreliable_indicators.unreliable_closing_date": {"lt": str(closed_before.date())}},
        )

    if reported_after := kwargs.get("reported_after"):
        must_range_filters.append(
            {"unreliable_indicators.unreliable_report_date": {"gt": str(reported_after.date())}},
        )

    if reported_before := kwargs.get("reported_before"):
        must_range_filters.append(
            {"unreliable_indicators.unreliable_report_date": {"lt": str(reported_before.date())}},
        )

    if severity_rating := kwargs.get("severity_rating"):
        filter_params = {
            "CRITICAL": {"gte": 9.0},
            "HIGH": {"gte": 7.0, "lt": 9.0},
            "MEDIUM": {"gte": 4.0, "lt": 7.0},
            "LOW": {"gte": 0.1, "lt": 4.0},
            "NONE": {"lt": 0.1},
        }
        must_range_filters.append({"severity_score.threat_score": filter_params[severity_rating]})

    return must_range_filters


async def should_and_filter(
    loaders: Dataloaders,
    group_name: str,
    **kwargs: Any,
) -> list[dict[str, str]]:
    if root := kwargs.get("root"):
        return await get_vulns_filters_by_root_nickname(loaders, group_name, root)

    return []
