from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.toe_lines.types import (
    SortsSuggestion,
    ToeLine,
)

from .schema import (
    TOE_LINES,
)


@TOE_LINES.field("sortsSuggestions")
def resolve(
    parent: ToeLine,
    _info: GraphQLResolveInfo,
    **_kwargs: None,
) -> list[SortsSuggestion] | None:
    return parent.state.sorts_suggestions
