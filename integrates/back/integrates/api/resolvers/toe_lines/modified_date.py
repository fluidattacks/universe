from datetime import (
    datetime,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.toe_lines.types import (
    ToeLine,
)

from .schema import (
    TOE_LINES,
)


@TOE_LINES.field("modifiedDate")
def resolve(parent: ToeLine, _info: GraphQLResolveInfo, **_kwargs: None) -> datetime:
    return parent.state.last_commit_date
