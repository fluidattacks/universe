from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.toe_lines.types import (
    ToeLine,
)

from .schema import (
    TOE_LINES,
)


@TOE_LINES.field("hasVulnerabilities")
def resolve(parent: ToeLine, _info: GraphQLResolveInfo, **_kwargs: None) -> bool | None:
    return parent.state.has_vulnerabilities
