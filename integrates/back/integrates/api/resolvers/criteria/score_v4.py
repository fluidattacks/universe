from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.criteria.types import (
    Criteria,
    CriteriaScoreV4,
)

from .schema import (
    CRITERIA,
)


@CRITERIA.field("scoreV4")
def resolve(
    parent: Criteria,
    _info: GraphQLResolveInfo,
    **_kwargs: None,
) -> CriteriaScoreV4:
    return parent.score_v4
