from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.roots.types import (
    IPRoot,
)

from .schema import (
    IP_ROOT,
)


@IP_ROOT.field("address")
def resolve(parent: IPRoot, _info: GraphQLResolveInfo) -> str:
    return parent.state.address
