from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.roots.types import (
    Root,
)

from .schema import (
    IP_ROOT,
)


@IP_ROOT.field("state")
def resolve(parent: Root, _info: GraphQLResolveInfo) -> str:
    return parent.state.status
