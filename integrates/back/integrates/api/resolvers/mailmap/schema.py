from ariadne import (
    ObjectType,
)

MAILMAP_ENTRY = ObjectType("MailmapEntry")

MAILMAP_SUBENTRY = ObjectType("MailmapSubentry")

MAILMAP_ENTRY_WITH_SUBENTRIES = ObjectType("MailmapEntryWithSubentries")

MAILMAP_ENTRIES_CONNECTION = ObjectType("MailmapEntriesConnection")
