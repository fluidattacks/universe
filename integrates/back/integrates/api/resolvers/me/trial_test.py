from integrates.api.resolvers.me.trial import resolve
from integrates.db_model.trials.enums import TrialStatus
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    DATE_2024,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationAccessFaker,
    OrganizationFaker,
    StakeholderFaker,
    TrialFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import freeze_time

ORG_ID = "ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db"
USER_EMAIL = "jdoe@orgtest.com"
ORG_NAME = "orgtest"


@freeze_time("2024-04-15T05:00:00+00:00")
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[StakeholderFaker(email=USER_EMAIL, enrolled=True, role="group_manager")],
            organization_access=[OrganizationAccessFaker(organization_id=ORG_ID, email=USER_EMAIL)],
            organizations=[OrganizationFaker(created_by=USER_EMAIL, id=ORG_ID, name=ORG_NAME)],
            trials=[TrialFaker(email=USER_EMAIL, extension_date=None, start_date=DATE_2024)],
            groups=[GroupFaker(name="grouptest", organization_id=ORG_ID)],
            group_access=[
                GroupAccessFaker(
                    email=USER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
        )
    ),
)
async def test_get_me_trial() -> None:
    # Act
    parent = {"user_email": USER_EMAIL}
    info = GraphQLResolveInfoFaker(
        user_email=USER_EMAIL,
    )

    result = await resolve(parent=parent, info=info)

    # Assert
    assert result
    assert not result["completed"]
    assert not result["extension_date"]
    assert result["extension_days"] == 0
    assert result["start_date"] == DATE_2024
    assert result["state"] == TrialStatus.TRIAL
    assert result["group_role"] == "group_manager"


@freeze_time("2024-04-15T05:00:00+00:00")
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[StakeholderFaker(email=USER_EMAIL, enrolled=True)],
            organization_access=[OrganizationAccessFaker(organization_id=ORG_ID, email=USER_EMAIL)],
            organizations=[OrganizationFaker(created_by=USER_EMAIL, id=ORG_ID, name=ORG_NAME)],
            trials=[
                TrialFaker(
                    email=USER_EMAIL, extension_date=None, start_date=DATE_2024, completed=True
                )
            ],
        )
    ),
)
async def test_get_me_trial_completed() -> None:
    # Act
    parent = {"user_email": USER_EMAIL}
    info = GraphQLResolveInfoFaker(
        user_email=USER_EMAIL,
    )

    result = await resolve(parent=parent, info=info)

    # Assert
    assert result
    assert result["completed"]
    assert not result["group_role"]
