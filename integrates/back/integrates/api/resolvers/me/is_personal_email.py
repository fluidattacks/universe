from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils.personal_email import (
    is_personal_email,
)

from .schema import (
    ME,
)


@ME.field("isPersonalEmail")
async def resolve(parent: dict[str, str], _info: GraphQLResolveInfo) -> bool:
    email = str(parent["user_email"])

    return await is_personal_email(email)
