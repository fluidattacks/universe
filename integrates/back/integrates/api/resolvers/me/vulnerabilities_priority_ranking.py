from typing import (
    Any,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils import (
    analytics,
)
from integrates.custom_utils.access import get_stakeholder_groups_names
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.items import VulnerabilityItem
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
    VulnerabilityZeroRiskStatus,
)
from integrates.db_model.vulnerabilities.types import (
    VulnerabilitiesConnection,
    VulnerabilityEdge,
)
from integrates.db_model.vulnerabilities.utils import (
    format_vulnerability,
)
from integrates.decorators import (
    require_login,
)
from integrates.dynamodb.types import (
    PageInfo,
)
from integrates.search.enums import (
    Sort,
)
from integrates.search.operations import (
    SearchClient,
    SearchParams,
)

from .schema import (
    ME,
)


@ME.field("vulnerabilitiesPriorityRanking")
@require_login
async def resolve(
    parent: dict[str, str],
    info: GraphQLResolveInfo,
    **kwargs: Any,
) -> VulnerabilitiesConnection:
    loaders: Dataloaders = info.context.loaders
    group_name = kwargs.get("group_name")
    treatment = kwargs.get("treatment")
    treatment_filter = "treatment.status"
    email = parent["user_email"]
    must_filters = [
        {"state.status": VulnerabilityStateStatus.VULNERABLE},
        {treatment_filter: treatment} if treatment else {},
    ]
    must_not_filters: list[dict[str, Any]] = [
        {"zero_risk.status": VulnerabilityZeroRiskStatus.CONFIRMED},
        {"zero_risk.status": VulnerabilityZeroRiskStatus.REQUESTED},
    ]

    active_groups = await get_stakeholder_groups_names(loaders, email, True)

    if not active_groups:
        return VulnerabilitiesConnection(
            edges=tuple(),
            page_info=PageInfo(has_next_page=False, end_cursor=""),
            total=0,
        )

    results = await SearchClient[VulnerabilityItem].search(
        SearchParams(
            exact_filters={"group_name": group_name if group_name else active_groups},
            must_filters=must_filters,
            must_not_filters=must_not_filters,
            sort_by=[
                {"unreliable_indicators.unreliable_priority": {"order": Sort.DESCENDING.value}},
                {"severity_score.threat_score": {"order": Sort.DESCENDING.value}},
            ],
            index_value="vulns_index",
            limit=50,
        ),
    )
    await analytics.mixpanel_track(email, "ViewPriorityVulnerabilityRanking")

    return VulnerabilitiesConnection(
        edges=tuple(
            VulnerabilityEdge(
                cursor=results.page_info.end_cursor,
                node=format_vulnerability(item),
            )
            for item in results.items
        ),
        page_info=results.page_info,
        total=results.total,
    )
