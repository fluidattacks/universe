from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates import (
    authz,
)
from integrates.dataloaders import (
    Dataloaders,
)

from .schema import (
    ME,
)


@ME.field("role")
async def resolve(parent: dict[str, str], info: GraphQLResolveInfo, **_kwargs: str) -> str:
    loaders: Dataloaders = info.context.loaders
    user_email = parent["user_email"]
    return await authz.get_user_level_role(loaders, user_email)
