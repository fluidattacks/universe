from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils.filter_vulnerabilities import (
    filter_non_zero_risk,
    filter_open_vulns,
)
from integrates.db_model.items import VulnerabilityItem
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
)
from integrates.db_model.vulnerabilities.utils import (
    format_vulnerability,
)
from integrates.search.operations import (
    SearchClient,
    SearchParams,
)

from .schema import (
    ME,
)


@ME.field("vulnerabilitiesAssigned")
async def resolve(parent: dict[str, str], _info: GraphQLResolveInfo) -> list[Vulnerability]:
    email = parent["user_email"]

    results = await SearchClient[VulnerabilityItem].search(
        SearchParams(
            must_filters=[{"treatment.assigned": email}],
            index_value="vulns_index",
            limit=1000,
        ),
    )

    vulnerabilities = filter_non_zero_risk(
        [format_vulnerability(result) for result in results.items],
    )

    return filter_non_zero_risk(filter_open_vulns(vulnerabilities))
