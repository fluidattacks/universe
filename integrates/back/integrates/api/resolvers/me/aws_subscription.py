from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.marketplace.types import (
    AWSMarketplaceSubscription,
)

from .schema import (
    ME,
)


@ME.field("awsSubscription")
async def resolve(parent: dict, info: GraphQLResolveInfo) -> AWSMarketplaceSubscription | None:
    loaders: Dataloaders = info.context.loaders
    user_email = str(parent["user_email"])
    stakeholder = await loaders.stakeholder.load(user_email)
    subscription: AWSMarketplaceSubscription | None = None
    if stakeholder and stakeholder.aws_customer_id is not None:
        subscription = await loaders.aws_marketplace_subscriptions.load(stakeholder.aws_customer_id)

    return subscription
