from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils.stakeholders import (
    get_stakeholder,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.stakeholders.types import (
    NotificationsPreferences,
)
from integrates.mailer import (
    utils,
)

from .schema import (
    ME,
)


@ME.field("notificationsPreferences")
async def resolve(
    parent: dict[str, str],
    info: GraphQLResolveInfo,
    **_kwargs: None,
) -> NotificationsPreferences:
    loaders: Dataloaders = info.context.loaders
    email = str(parent["user_email"])
    stakeholder = await get_stakeholder(loaders, email)

    return stakeholder.state.notifications_preferences._replace(
        available=await utils.get_available_notifications(loaders, email),
    )
