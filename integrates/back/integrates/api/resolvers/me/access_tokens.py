from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils.stakeholders import (
    get_stakeholder,
)
from integrates.db_model.stakeholders.types import (
    AccessTokens,
)

from .schema import (
    ME,
)


@ME.field("accessTokens")
async def resolve(parent: dict, info: GraphQLResolveInfo) -> list[AccessTokens]:
    user_email = str(parent["user_email"])
    stakeholder = await get_stakeholder(info.context.loaders, user_email)

    return stakeholder.access_tokens
