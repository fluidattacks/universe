from datetime import datetime
from decimal import Decimal

from integrates.api.resolvers.me.organizations import resolve
from integrates.db_model.organizations.enums import OrganizationStateStatus
from integrates.db_model.organizations.types import Organization, OrganizationState
from integrates.db_model.types import Policies
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    OrganizationAccessFaker,
    OrganizationFaker,
    OrganizationStateFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize

ORG_ID = "ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db"
VULNERABILITY_MANAGER = "vulnerability_manager@gmail.com"
ORG_MANAGER = "organization_manager@gmail.com"
ORG_NAME = "orgtest"


@parametrize(
    args=["email", "expected_result"],
    cases=[
        [
            "vulnerability_manager@gmail.com",
            [
                Organization(
                    created_by="jane@entry.com",
                    created_date=(datetime.fromisoformat("2019-04-10T12:59:52+00:00")),
                    id=ORG_ID,
                    name=ORG_NAME,
                    policies=Policies(
                        modified_date=(datetime.fromisoformat("2019-04-10T12:59:52+00:00")),
                        modified_by="jane@entry.com",
                        inactivity_period=90,
                        max_acceptance_days=None,
                        max_acceptance_severity=Decimal("10.0"),
                        max_number_acceptances=None,
                        min_acceptance_severity=Decimal("0.0"),
                        min_breaking_severity=None,
                        vulnerability_grace_period=None,
                        days_until_it_breaks=None,
                        max_number_of_expected_contributors=None,
                    ),
                    state=OrganizationState(
                        aws_external_id=("123456789012"),
                        status=OrganizationStateStatus.ACTIVE,
                        modified_by="jane@entry.com",
                        modified_date=(datetime.fromisoformat("2019-04-10T12:59:52+00:00")),
                        pending_deletion_date=None,
                    ),
                    country="Colombia",
                    jira_associated_id=None,
                    priority_policies=[],
                    billing_information=None,
                    vulnerabilities_pathfile=None,
                    payment_methods=[],
                ),
            ],
        ],
        [
            "organization_manager@gmail.com",
            [
                Organization(
                    created_by="jane@entry.com",
                    created_date=(datetime.fromisoformat("2019-04-10T12:59:52+00:00")),
                    id=ORG_ID,
                    name=ORG_NAME,
                    policies=Policies(
                        modified_date=(datetime.fromisoformat("2019-04-10T12:59:52+00:00")),
                        modified_by="jane@entry.com",
                        inactivity_period=90,
                        max_acceptance_days=None,
                        max_acceptance_severity=Decimal("10.0"),
                        max_number_acceptances=None,
                        min_acceptance_severity=Decimal("0.0"),
                        min_breaking_severity=None,
                        vulnerability_grace_period=None,
                        days_until_it_breaks=None,
                        max_number_of_expected_contributors=None,
                    ),
                    state=OrganizationState(
                        aws_external_id=("123456789012"),
                        status=OrganizationStateStatus.ACTIVE,
                        modified_by="jane@entry.com",
                        modified_date=(datetime.fromisoformat("2019-04-10T12:59:52+00:00")),
                        pending_deletion_date=None,
                    ),
                    country="Colombia",
                    jira_associated_id=None,
                    priority_policies=[],
                    billing_information=None,
                    vulnerabilities_pathfile=None,
                    payment_methods=[],
                ),
                Organization(
                    created_by="jane@entry.com",
                    created_date=(datetime.fromisoformat("2019-04-10T12:59:52+00:00")),
                    id="ORG#org3",
                    name="orgtest3",
                    policies=Policies(
                        modified_date=(datetime.fromisoformat("2019-04-10T12:59:52+00:00")),
                        modified_by="jane@entry.com",
                        inactivity_period=90,
                        max_acceptance_days=None,
                        max_acceptance_severity=Decimal("10.0"),
                        max_number_acceptances=None,
                        min_acceptance_severity=Decimal("0.0"),
                        min_breaking_severity=None,
                        vulnerability_grace_period=None,
                        days_until_it_breaks=None,
                        max_number_of_expected_contributors=None,
                    ),
                    state=OrganizationState(
                        aws_external_id=("123456789012"),
                        status=OrganizationStateStatus.ACTIVE,
                        modified_by="jane@entry.com",
                        modified_date=(datetime.fromisoformat("2019-04-10T12:59:52+00:00")),
                        pending_deletion_date=None,
                    ),
                    country="Colombia",
                    jira_associated_id=None,
                    priority_policies=[],
                    billing_information=None,
                    vulnerabilities_pathfile=None,
                    payment_methods=[],
                ),
            ],
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id=ORG_ID, name=ORG_NAME),
                OrganizationFaker(
                    id="ORG#org2",
                    name="orgtest2",
                    state=OrganizationStateFaker(
                        status=OrganizationStateStatus.DELETED,
                    ),
                ),
                OrganizationFaker(id="ORG#org3", name="orgtest3"),
            ],
            stakeholders=[
                StakeholderFaker(email=VULNERABILITY_MANAGER, enrolled=True),
                StakeholderFaker(email=ORG_MANAGER, enrolled=True),
            ],
            organization_access=[
                OrganizationAccessFaker(organization_id=ORG_ID, email=VULNERABILITY_MANAGER),
                OrganizationAccessFaker(organization_id=ORG_ID, email=ORG_MANAGER),
                OrganizationAccessFaker(organization_id="ORG#org2", email=VULNERABILITY_MANAGER),
                OrganizationAccessFaker(organization_id="ORG#org3", email=ORG_MANAGER),
            ],
        )
    )
)
async def test_me_organizations(email: str, expected_result: list[Organization]) -> None:
    # Act
    parent = {"user_email": email}
    info = GraphQLResolveInfoFaker(user_email=email)

    result = await resolve(parent=parent, info=info)

    # Assert
    assert result == expected_result
