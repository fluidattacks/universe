from typing import Any

from graphql.type.definition import GraphQLResolveInfo

from integrates.custom_utils.access import get_stakeholder_groups_names
from integrates.dataloaders import Dataloaders
from integrates.db_model.items import VulnerabilityItem
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilitySeverityProposalStatus,
    VulnerabilityStateStatus,
    VulnerabilityZeroRiskStatus,
)
from integrates.db_model.vulnerabilities.types import VulnerabilitiesConnection, VulnerabilityEdge
from integrates.db_model.vulnerabilities.utils import format_vulnerability
from integrates.decorators import require_login
from integrates.dynamodb.types import PageInfo
from integrates.search.operations import SearchClient, SearchParams

from .schema import ME


@ME.field("vulnerabilitiesSeverityRequests")
@require_login
async def resolve(
    parent: dict[str, str],
    info: GraphQLResolveInfo,
    **kwargs: Any,
) -> VulnerabilitiesConnection:
    loaders: Dataloaders = info.context.loaders

    email = parent["user_email"]

    must_filters: list[dict[str, Any]] = [
        {"state.status": VulnerabilityStateStatus.VULNERABLE},
        {"state.proposed_severity.status": VulnerabilitySeverityProposalStatus.REQUESTED},
    ]
    must_not_filters: list[dict[str, Any]] = [
        {"zero_risk.status": VulnerabilityZeroRiskStatus.CONFIRMED},
        {"zero_risk.status": VulnerabilityZeroRiskStatus.REQUESTED},
    ]

    active_groups = await get_stakeholder_groups_names(loaders, email, True)

    if not active_groups:
        return VulnerabilitiesConnection(
            edges=tuple(),
            page_info=PageInfo(has_next_page=False, end_cursor=""),
            total=0,
        )

    results = await SearchClient[VulnerabilityItem].search(
        SearchParams(
            after=kwargs.get("after"),
            exact_filters={"group_name": active_groups},
            must_filters=must_filters,
            must_not_filters=must_not_filters,
            index_value="vulns_index",
            limit=kwargs.get("first") or 100,
        ),
    )

    return VulnerabilitiesConnection(
        edges=tuple(
            VulnerabilityEdge(
                cursor=results.page_info.end_cursor,
                node=format_vulnerability(item),
            )
            for item in results.items
        ),
        page_info=results.page_info,
        total=results.total,
    )
