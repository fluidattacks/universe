from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils.stakeholders import (
    has_all_invitation_pending,
)

from .schema import (
    ME,
)


@ME.field("hasAllInvitationsPending")
async def resolve(parent: dict[str, str], info: GraphQLResolveInfo) -> bool:
    email = str(parent["user_email"])

    return await has_all_invitation_pending(info.context.loaders, email)
