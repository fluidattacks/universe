from os import (
    path,
)

from ariadne.explorer import (
    ExplorerGraphiQL,
)
from ariadne.explorer.default_query import (
    escape_default_query,
)
from ariadne.explorer.graphiql import (
    DEFAULT_QUERY,
)
from ariadne.explorer.template import (
    render_template,
)


def read_template(template: str) -> str:
    template_dir = path.dirname(path.abspath(__file__))
    template_path = path.join(template_dir, template)
    with open(template_path, encoding="utf-8") as file:
        return file.read()


class IntegratesAPIExplorer(ExplorerGraphiQL):
    """Custom ExplorerGraphiQL class."""

    def __init__(
        self,
        title: str = "Ariadne GraphQL",
        explorer_plugin: bool = False,
        default_query: str = DEFAULT_QUERY,
    ) -> None:
        super().__init__(
            title=title,
            explorer_plugin=explorer_plugin,
            default_query=default_query,
        )
        template = read_template("template.html")
        template_vars = {
            "title": title,
            "enable_explorer_plugin": explorer_plugin,
            "default_query": escape_default_query(default_query),
        }
        self.parsed_html = render_template(template, template_vars)
