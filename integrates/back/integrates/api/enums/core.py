from enum import Enum


class VulnerabilitySort(str, Enum):
    ID: str = "_id"
    WHERE: str = "state.where.keyword"
    SPECIFIC: str = "state.specific.keyword"
    STATE: str = "state.status.keyword"
    TECHNIQUE: str = "technique.keyword"
    CVSSF_SCORE: str = "severity_score.cvssf"
    CVSSF_V4_SCORE: str = "severity_score.cvssf_v4"
    PRIORITY_SCORE: str = "unreliable_indicators.unreliable_priority"
    TREATMENT_STATUS: str = "treatment.status.keyword"
    TREATMENT_ASSIGNED: str = "treatment.assigned.keyword"
    REPORT_DATE: str = "unreliable_indicators.unreliable_report_date"
    VERIFICATION: str = "verification.status.keyword"
    ZERO_RISK: str = "zero_risk.status.keyword"


class OrderSort(str, Enum):
    ASC: str = "ASC"
    DESC: str = "DESC"
