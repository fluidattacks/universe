from collections.abc import AsyncGenerator

from graphql import (
    GraphQLResolveInfo,
)

from integrates.custom_exceptions import (
    AutomaticFixError,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_is_not_under_review,
)
from integrates.vulnerabilities.fixes.generate import (
    get_suggested_fix,
)
from integrates.vulnerabilities.fixes.types import (
    SuggestedFixFunction,
)

from .schema import (
    SUBSCRIPTION,
)


@SUBSCRIPTION.source("getSuggestedFix")
async def generator(
    _parent: None,
    info: GraphQLResolveInfo,
    vulnerability_id: str,
    vulnerable_function: str,
    vulnerable_line_content: str,
    vulnerable_code_imports: None | str,
    feature_preview: bool = False,
) -> AsyncGenerator[str, None]:
    loaders: Dataloaders = info.context.loaders
    try:
        result = await get_suggested_fix(
            loaders=loaders,
            vulnerable_code=SuggestedFixFunction(
                vulnerability_id=vulnerability_id,
                code_imports=vulnerable_code_imports,
                function=vulnerable_function,
                vulnerable_line_content=vulnerable_line_content,
            ),
            stream=True,
            feature_preview=feature_preview,
        )
        if result and isinstance(result, AsyncGenerator):
            async for line in result:
                yield line
    except Exception as exc:
        raise AutomaticFixError(
            kind="suggested",
            vuln_id=vulnerability_id,
        ) from exc


@SUBSCRIPTION.field("getSuggestedFix")
@concurrent_decorators(
    enforce_group_level_auth_async,
    require_is_not_under_review,
)
def resolve(count: str, _info: GraphQLResolveInfo, **_kwargs: str) -> str:
    return count
