from integrates.api.subscriptions.get_suggested_fix import resolve
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute_internal,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    FindingFaker,
    GraphQLResolveInfoFaker,
    GroupFaker,
    OrganizationFaker,
    VulnerabilityFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import raises


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(organization_id="org1")],
            organizations=[OrganizationFaker(id="org1")],
            findings=[FindingFaker(id="34c60b94-e664-4acd-b90e-ef4a3b09a217")],
            vulnerabilities=[
                VulnerabilityFaker(
                    id="4d2854e5-9909-4742-a501-afee0ae4b854",
                    finding_id="34c60b94-e664-4acd-b90e-ef4a3b09a217",
                )
            ],
        )
    )
)
async def test_get_suggested_fix_access_denied() -> None:
    info = GraphQLResolveInfoFaker(
        user_email="randommail@test.test",
        decorators=[
            [enforce_group_level_auth_async],
            [require_attribute_internal, "is_under_review", "test-group"],
        ],
    )
    with raises(Exception, match="Access denied"):
        await resolve(
            count="",
            _info=info,
            info=info,
            vulnerability_id="4d2854e5-9909-4742-a501-afee0ae4b854",
            vulnerable_function="",
            vulnerable_line_content="",
            vulnerable_code_imports="",
        )
