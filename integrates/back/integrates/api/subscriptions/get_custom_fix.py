from collections.abc import AsyncGenerator
from typing import (
    cast,
)

from graphql import (
    GraphQLResolveInfo,
)

from integrates.custom_exceptions import (
    AutomaticFixError,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_is_not_under_review,
)
from integrates.vulnerabilities.fixes.generate import (
    get_custom_fix,
)

from .schema import (
    SUBSCRIPTION,
)


@SUBSCRIPTION.source("getCustomFix")
def generator(
    _obj: None,
    info: GraphQLResolveInfo,
    vulnerability_id: str,
    feature_preview: bool = False,
) -> AsyncGenerator[str, None]:
    loaders: Dataloaders = info.context.loaders
    try:
        return cast(
            AsyncGenerator[str, None],
            get_custom_fix(
                loaders,
                vulnerability_id,
                stream=True,
                feature_preview=feature_preview,
            ),
        )
    except Exception as exc:
        raise AutomaticFixError(
            kind="custom",
            vuln_id=vulnerability_id,
        ) from exc


@SUBSCRIPTION.field("getCustomFix")
@concurrent_decorators(
    enforce_group_level_auth_async,
    require_is_not_under_review,
)
def resolve(count: str, _info: GraphQLResolveInfo, **_kwargs: str) -> str:
    return count
