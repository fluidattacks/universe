from typing import NamedTuple

from integrates.class_types.types import Item
from integrates.custom_exceptions import CustomBaseException
from integrates.dynamodb.exceptions import DynamoDbBaseException

APP_EXCEPTIONS = (CustomBaseException, DynamoDbBaseException)


class Operation(NamedTuple):
    name: str
    operation_type: str
    query: str
    variables: Item


class UserMetadata(NamedTuple):
    user_agent: str
    user_email: str
    group_name: str
    roles: list[str]


class Context(NamedTuple):
    operation: Operation
    user_metadata: UserMetadata
    decoded_body: str
