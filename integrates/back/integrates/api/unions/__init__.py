from ariadne import (
    UnionType,
)

from .notification import (
    NOTIFICATION,
)
from .root import (
    ROOT,
)

UNIONS: tuple[UnionType, ...] = (
    NOTIFICATION,
    ROOT,
)
