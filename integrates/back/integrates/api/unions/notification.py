from ariadne import (
    UnionType,
)
from graphql.type.definition import (
    GraphQLAbstractType,
    GraphQLResolveInfo,
)

from integrates.db_model.notifications.types import (
    Notification,
    ReportNotification,
)

NOTIFICATION = UnionType("Notification")


@NOTIFICATION.type_resolver
def resolve_notification_type(
    result: Notification,
    _info: GraphQLResolveInfo,
    _return_type: GraphQLAbstractType,
) -> str | None:
    if isinstance(result, ReportNotification):
        return "ReportNotification"
    return None
