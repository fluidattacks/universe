from inspect import (
    iscoroutinefunction,
)
from typing import (
    Any,
)

from ariadne.types import (
    Extension,
    Resolver,
)
from graphql import (
    GraphQLResolveInfo,
)
from graphql.pyutils import (
    is_awaitable,
)


class LowercaseGroupNameExtension(Extension):
    def resolve(
        self,
        next_: Resolver,
        obj: Any,
        info: GraphQLResolveInfo,
        **kwargs: dict,
    ) -> Any:
        def set_name() -> None:
            for name in ["group_name", "organization_name"]:
                _set_name(name)

        def _set_name(name: str) -> None:
            if kwargs:
                _name = kwargs.get(name)
                if _name and isinstance(_name, str) and not _name.islower():
                    kwargs[name] = _name.lower()

        if not iscoroutinefunction(next_):
            set_name()
            result = next_(obj, info, **kwargs)
            return result

        async def resolve_async() -> Any:
            set_name()
            result = await next_(obj, info, **kwargs)
            if is_awaitable(result):
                result = await result
            return result

        return resolve_async()
