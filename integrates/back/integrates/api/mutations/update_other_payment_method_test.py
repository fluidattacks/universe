import os
from io import BytesIO
from typing import (
    Any,
)

import aiofiles
from starlette.datastructures import Headers, UploadFile

from integrates.api.mutations.update_other_payment_method import mutate
from integrates.custom_exceptions import InvalidParameter
from integrates.decorators import (
    enforce_organization_level_auth_async,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    OrganizationAccessFaker,
    OrganizationFaker,
    OrganizationPaymentMethodsFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import parametrize, raises
from integrates.tickets import domain as tickets_domain

# Allowed users
ADMIN_EMAIL = "admin@gmail.com"
ORG_MANAGER_EMAIL = "organization_manager@gmail.com"

# General parameters
ORGTEST_ID = "ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db"

DEFAULT_DECORATORS: list[list[Any]] = [
    [require_login],
    [enforce_organization_level_auth_async],
]


async def read_file(file_name: str) -> UploadFile:
    path: str = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    filename: str = os.path.join(path, f"test_files/other_payment_method/{file_name}")
    async with aiofiles.open(filename, "rb") as test_file:
        headers = Headers(headers={"content_type": "text/x-yaml"})
        file_contents = await test_file.read()
        return UploadFile(
            filename=str(test_file.name),
            headers=headers,
            file=BytesIO(file_contents),
        )


@parametrize(
    args=["country", "state", "city", "email"],
    cases=[
        ["Colombia", "Antioquia", "Medellin", ADMIN_EMAIL],
        ["Estados Unidos", "Arizona", "Tempe", ORG_MANAGER_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=ADMIN_EMAIL),
                StakeholderFaker(email=ORG_MANAGER_EMAIL),
            ],
            organization_access=[
                OrganizationAccessFaker(organization_id=ORGTEST_ID, email=ADMIN_EMAIL),
                OrganizationAccessFaker(organization_id=ORGTEST_ID, email=ORG_MANAGER_EMAIL),
            ],
            organizations=[
                OrganizationFaker(
                    id=ORGTEST_ID,
                    payment_methods=[
                        OrganizationPaymentMethodsFaker(
                            id="38eb8f25-7945-4173-ab6e-0af4ad8b7ef3", email=ADMIN_EMAIL
                        ),
                        OrganizationPaymentMethodsFaker(
                            id="38eb8f25-7945-4173-ab6e-0af4ad8b7ef3", email=ORG_MANAGER_EMAIL
                        ),
                    ],
                )
            ],
        )
    ),
    others=[Mock(tickets_domain, "request_other_payment_methods", "async", None)],
)
async def test_update_other_payment_method_success(
    country: str, state: str, city: str, email: str
) -> None:
    business_name: str = "test business"

    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=DEFAULT_DECORATORS,
    )
    file = await read_file("test-pdf.pdf")
    rut = file if country.lower() == "colombia" else None
    tax_id = file if country.lower() != "colombia" else None
    await mutate(
        _parent=None,
        info=info,
        tax_id=tax_id,
        rut=rut,
        organization_id=ORGTEST_ID,
        business_name=business_name,
        email=email,
        country=country,
        state=state,
        city=city,
        payment_method_id="38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
    )


@parametrize(
    args=["country", "state", "city"],
    cases=[
        [
            "Colombia",
            "Antioquia",
            "Medellin",
        ],
        [
            "Estados Unidos",
            "Arizona",
            "Tempe",
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=ADMIN_EMAIL),
                StakeholderFaker(email=ORG_MANAGER_EMAIL),
            ],
            organization_access=[
                OrganizationAccessFaker(organization_id=ORGTEST_ID, email=ADMIN_EMAIL),
                OrganizationAccessFaker(organization_id=ORGTEST_ID, email=ORG_MANAGER_EMAIL),
            ],
            organizations=[OrganizationFaker(id=ORGTEST_ID)],
        )
    )
)
async def test_update_other_payment_method(country: str, state: str, city: str) -> None:
    email: str = "test@test.com"
    business_name: str = "test business"

    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=DEFAULT_DECORATORS,
    )
    file = await read_file("test-pdf.pdf")
    rut = file if country.lower() == "colombia" else None
    tax_id = file if country.lower() != "colombia" else None
    with raises(Exception, match="Access denied"):
        await mutate(
            _parent=None,
            info=info,
            tax_id=tax_id,
            rut=rut,
            organization_id=ORGTEST_ID,
            business_name=business_name,
            email=email,
            country=country,
            state=state,
            city=city,
            payment_method_id="38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
        )


@parametrize(
    args=["country", "state", "city"],
    cases=[
        [
            "Colombia",
            "Antioquia",
            "Medellin",
        ],
        [
            "Estados Unidos",
            "Arizona",
            "Tempe",
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=ADMIN_EMAIL, enrolled=True),
                StakeholderFaker(
                    email=ORG_MANAGER_EMAIL, enrolled=True, role="organization_manager"
                ),
            ],
            organization_access=[
                OrganizationAccessFaker(organization_id=ORGTEST_ID, email=ADMIN_EMAIL),
                OrganizationAccessFaker(organization_id=ORGTEST_ID, email=ORG_MANAGER_EMAIL),
            ],
            organizations=[OrganizationFaker(id=ORGTEST_ID)],
        )
    ),
    others=[Mock(tickets_domain, "request_other_payment_methods", "async", None)],
)
async def test_update_other_payment_method_fail(country: str, state: str, city: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=ORG_MANAGER_EMAIL,
        decorators=DEFAULT_DECORATORS,
    )

    with raises(InvalidParameter("Invalid rut or tax id").__class__):
        await mutate(
            _parent=None,
            info=info,
            tax_id=None,
            rut=None,
            organization_id=ORGTEST_ID,
            business_name="test name",
            email=ORG_MANAGER_EMAIL,
            country=country,
            state=state,
            city=city,
            payment_method_id="38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
        )
