from graphql import GraphQLError

from integrates.api.mutations.update_group_access_info import mutate
from integrates.custom_exceptions import InvalidMarkdown
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_asm,
    require_attribute,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import raises

GROUP_NAME = "test-group"
USER_EMAIL = "user@gmail.com"
FINDING_ID = "c265b055-5f30-444f-ad88-1fefab65d59d"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=USER_EMAIL, role="user"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=USER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
        ),
    )
)
async def test_update_group_access_info_success() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=USER_EMAIL,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    result = await mutate(
        _=None,
        info=info,
        group_name=GROUP_NAME,
        group_context="New group context",
    )
    assert result.success


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=USER_EMAIL, role="user"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
        ),
    )
)
async def test_update_group_access_info_permission_denied() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=USER_EMAIL,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    with raises(GraphQLError, match="Access denied"):
        await mutate(
            _=None,
            info=info,
            group_name=GROUP_NAME,
            group_context="New group context",
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=USER_EMAIL, role="user"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=USER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
        ),
    )
)
async def test_update_group_access_info_invalid_markdown() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=USER_EMAIL,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    with raises(InvalidMarkdown):
        await mutate(
            _=None,
            info=info,
            group_name=GROUP_NAME,
            group_context="Invalid \x00 markdown",
        )
