from graphql import (
    GraphQLResolveInfo,
)

from integrates.class_types.types import (
    Item,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.db_model.hook.utils import (
    format_group_hook_payload,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_login,
)
from integrates.groups.domain import (
    add_group_hook,
)
from integrates.groups.hook_notifier import (
    check_hook,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("addHook")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_is_not_under_review,
)
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    group_name: str,
    hook: dict[str, Item],
) -> SimplePayload:
    user_data = await sessions_domain.get_jwt_content(info.context)
    user_email = user_data["user_email"]
    await check_hook(event="New hook Added", group=group_name, hook=hook)
    await add_group_hook(
        loaders=info.context.loaders,
        group_name=group_name,
        user_email=user_email,
        hook=format_group_hook_payload(hook),
    )

    logs_utils.cloudwatch_log(
        info.context,
        "Create Hook",
        extra={
            "group_name": group_name,
            "log_type": "Security",
        },
    )

    return SimplePayload(success=True)
