from datetime import datetime

from integrates.api.mutations.update_evidence_description import mutate
from integrates.db_model.findings.types import FindingEvidence, FindingEvidences
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_asm,
    require_attribute,
    require_finding_access,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    FindingFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import raises

GROUP_NAME = "test-group"
USER_EMAIL = "user@gmail.com"
FINDING_ID = "c265b055-5f30-444f-ad88-1fefab65d59d"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=USER_EMAIL, role="user"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=USER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
            ],
            findings=[
                FindingFaker(
                    id=FINDING_ID,
                    group_name=GROUP_NAME,
                    evidences=FindingEvidences(
                        evidence1=FindingEvidence(
                            description="Initial description",
                            modified_date=datetime.fromisoformat("2023-01-01T00:00:00+00:00"),
                            url="https://evidence.com/evidence1",
                            author_email="user@gmail.com",
                            is_draft=False,
                        )
                    ),
                ),
            ],
        ),
    )
)
async def test_update_evidence_description_success() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=USER_EMAIL,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
            [require_finding_access],
        ],
    )
    result = await mutate(
        _=None,
        info=info,
        finding_id=FINDING_ID,
        evidence_id="evidence_route_1",
        description="Updated description",
    )
    assert result.success


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=USER_EMAIL, role="user"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=USER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
            ],
        ),
    )
)
async def test_update_evidence_description_invalid_finding() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=USER_EMAIL,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
            [require_finding_access],
        ],
    )
    with raises(Exception):
        await mutate(
            _=None,
            info=info,
            finding_id="invalid_finding",
            evidence_id="evidence_route_1",
            description="Updated description",
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=USER_EMAIL, role="user"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=USER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
            ],
            findings=[
                FindingFaker(
                    id=FINDING_ID,
                    group_name=GROUP_NAME,
                    evidences=FindingEvidences(
                        evidence1=FindingEvidence(
                            description="Initial description",
                            modified_date=datetime.fromisoformat("2023-01-01T00:00:00+00:00"),
                            url="https://evidence.com/evidence1",
                            author_email="user@gmail.com",
                            is_draft=False,
                        )
                    ),
                ),
            ],
        ),
    )
)
async def test_update_evidence_description_empty() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=USER_EMAIL,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
            [require_finding_access],
        ],
    )
    result = await mutate(
        _=None,
        info=info,
        finding_id=FINDING_ID,
        evidence_id="evidence_route_1",
        description="",
    )
    assert result.success
