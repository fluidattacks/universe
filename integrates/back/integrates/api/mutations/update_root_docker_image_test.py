from integrates.api.mutations.update_root_docker_image import mutate
from integrates.dataloaders import get_new_context
from integrates.db_model.credentials.types import HttpsSecret
from integrates.db_model.roots.types import RootDockerImageRequest
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute,
    require_attribute_internal,
    require_login,
)
from integrates.roots import validations
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    CredentialsFaker,
    GitRootFaker,
    GraphQLResolveInfoFaker,
    GroupFaker,
    OrganizationFaker,
    RootDockerImageFaker,
    RootDockerImageStateFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import parametrize

ADMIN_EMAIL = "admin@gmail.com"
ROOT_ID = "63298a73-9dff-46cf-b42d-9b2f01a56690"
GROUP_NAME = "grouptest"
URI = "fluidattacks/forces:latest"


@parametrize(
    args=["credential"],
    cases=[
        [
            "test-credential2",
        ],
        [
            None,
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=ADMIN_EMAIL, role="admin")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                ),
            ],
            docker_images=[
                RootDockerImageFaker(
                    root_id=ROOT_ID,
                    group_name=GROUP_NAME,
                    created_by=ADMIN_EMAIL,
                    uri=URI,
                    state=RootDockerImageStateFaker(credential_id="test-credential"),
                )
            ],
            credentials=[
                CredentialsFaker(
                    credential_id="test-credential",
                    organization_id="ORG#org1",
                    secret=HttpsSecret(user=ADMIN_EMAIL, password="pass"),  # noqa: S106
                ),
                CredentialsFaker(
                    credential_id="test-credential2",
                    organization_id="ORG#org1",
                    secret=HttpsSecret(user=ADMIN_EMAIL, password="pass2"),  # noqa: S106
                ),
            ],
        ),
    ),
    others=[
        Mock(
            validations,
            "get_image_manifest",
            "async",
            (
                {
                    "Digest": "sha256:fe6a5f3f5555b3c6bfb5b6e0e"
                    "7639db8b8c2272d0848f214d7340d92521bf42d",
                    "Layers": [
                        "sha256:de44b265507ae44b212defcb50694d666f136b35c1090d9709068bc861bb2d64",
                        "sha256:add2cfa32b4d238ee12f15bfd6aa93ccfe9642fd196c8547875c8283b2368d7a",
                    ],
                    "LayersData": [
                        {
                            "MIMEType": "application/vnd.oci.image.layer.v1.tar+gzip",
                            "Digest": "sha256:de44b265507ae44b212defcb50694d666f136b35"
                            "c1090d9709068bc861bb2d64",
                            "Size": 29751968,
                            "Annotations": None,
                        },
                        {
                            "MIMEType": "application/vnd.oci.image.layer.v1.tar+gzip",
                            "Digest": "sha256:add2cfa32b4d238ee12f15bfd6aa93ccfe9642fd"
                            "196c8547875c8283b2368d7a",
                            "Size": 1217,
                            "Annotations": None,
                        },
                    ],
                }
            ),
        )
    ],
)
async def test_update_docker_image_credentials(credential: str | None) -> None:
    loaders = get_new_context()
    info = GraphQLResolveInfoFaker(
        user_email=ADMIN_EMAIL,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_service_white", GROUP_NAME],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )

    if credential:
        result = await mutate(
            _parent=None,
            info=info,
            user_email=ADMIN_EMAIL,
            group_name=GROUP_NAME,
            uri=URI,
            root_id=ROOT_ID,
            credentials={"id": "test-credential2"},
        )
    else:
        result = await mutate(
            _parent=None,
            info=info,
            user_email=ADMIN_EMAIL,
            group_name=GROUP_NAME,
            uri=URI,
            root_id=ROOT_ID,
        )
    assert result.success is True

    image = await loaders.docker_image.load(RootDockerImageRequest(ROOT_ID, GROUP_NAME, URI))
    assert image
    assert image.state.credential_id == credential
