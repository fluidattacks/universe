from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.api.types import (
    APP_EXCEPTIONS,
)
from integrates.batch import (
    dal as batch_dal,
)
from integrates.batch.enums import (
    Action,
    IntegratesBatchQueue,
)
from integrates.custom_exceptions import (
    PriorityScoreUpdateAlreadyRequested,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.organizations.types import (
    OrganizationPriorityPolicy,
)
from integrates.decorators import (
    enforce_organization_level_auth_async,
)
from integrates.organizations import (
    utils as orgs_utils,
)
from integrates.organizations_priority_policies.domain import (
    update_priority_policies,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("updateOrganizationPriorityPolicies")
@enforce_organization_level_auth_async
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    priority_policies: list[dict[str, str | int]],
    **kwargs: str,
) -> SimplePayload:
    try:
        loaders: Dataloaders = info.context.loaders
        organization_id = kwargs.pop("organization_id")
        user_info = await sessions_domain.get_jwt_content(info.context)
        organization = await orgs_utils.get_organization(loaders, organization_id)
        existing_actions = await batch_dal.get_actions_by_name(
            action_name=Action.UPDATE_ORGANIZATION_PRIORITY_POLICIES,
            entity=organization.name,
        )
        updated_policies = [
            OrganizationPriorityPolicy(
                policy=str(priority_policy["policy"]),
                value=int(priority_policy["value"]),
            )
            for priority_policy in priority_policies
        ]
        if len(existing_actions) > 0:
            raise PriorityScoreUpdateAlreadyRequested()

        await update_priority_policies(
            loaders=loaders,
            organization_id=organization_id,
            updated_policies=updated_policies,
        )
        if updated_policies != organization.priority_policies:
            await batch_dal.put_action(
                action=Action.UPDATE_ORGANIZATION_PRIORITY_POLICIES,
                entity=organization.name,
                subject=user_info["user_email"],
                additional_info={"priority_policies": priority_policies},
                queue=IntegratesBatchQueue.SMALL,
            )

        logs_utils.cloudwatch_log(
            info.context,
            "Updated priority policies in the organization",
            extra={
                "organization_name": organization.name,
                "organization_id": organization_id,
                "log_type": "Security",
            },
        )
    except APP_EXCEPTIONS:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to update priority policies in the organization",
            extra={
                "organization_name": organization.name,
                "organization_id": organization_id,
                "log_type": "Security",
            },
        )
        raise

    return SimplePayload(success=True)
