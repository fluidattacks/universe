from decimal import Decimal

from aioextensions import collect

from integrates.api.mutations.update_groups_policies import mutate
from integrates.custom_utils.groups import get_group
from integrates.dataloaders import get_new_context
from integrates.decorators import enforce_organization_level_auth_async
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationAccessFaker,
    OrganizationAccessStateFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks

ORG_MANAGER = "organization_manager@gmail.com"
ORGANIZATION_ID = "ORG#dc9757f3-fdd8-4cd0-abf8-db9d6dbbe39e"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORGANIZATION_ID)],
            groups=[
                GroupFaker(name="group1", organization_id=ORGANIZATION_ID),
                GroupFaker(name="group2", organization_id=ORGANIZATION_ID),
                GroupFaker(name="group3", organization_id=ORGANIZATION_ID),
            ],
            stakeholders=[
                StakeholderFaker(email=ORG_MANAGER),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name="group1",
                    email=ORG_MANAGER,
                    state=GroupAccessStateFaker(role="organization_manager"),
                ),
                GroupAccessFaker(
                    group_name="group2",
                    email=ORG_MANAGER,
                    state=GroupAccessStateFaker(role="organization_manager"),
                ),
                GroupAccessFaker(
                    group_name="group3",
                    email=ORG_MANAGER,
                    state=GroupAccessStateFaker(role="organization_manager"),
                ),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORGANIZATION_ID,
                    email=ORG_MANAGER,
                    state=OrganizationAccessStateFaker(role="organization_manager"),
                ),
            ],
        ),
    ),
)
async def test_update_group_policies() -> None:
    kwargs = {
        "max_acceptance_days": 30,
        "max_acceptance_severity": 6.9,
        "max_number_acceptances": 2,
        "min_acceptance_severity": 0.0,
        "min_breaking_severity": 7.0,
        "user": "email",
        "vulnerability_grace_period": 61,
        "days_until_it_breaks": 100,
    }

    info = GraphQLResolveInfoFaker(
        user_email=ORG_MANAGER,
        decorators=[[enforce_organization_level_auth_async]],
    )
    await mutate(
        _parent=None,
        info=info,
        groups=["group1", "group2"],
        organization_id=ORGANIZATION_ID,
        **kwargs,
    )
    loaders = get_new_context()
    groups = await collect(
        [
            get_group(loaders, stakeholder_group_name)
            for stakeholder_group_name in ["group1", "group2"]
        ],
    )

    for group in groups:
        assert group
        assert group.policies
        assert group.policies.max_acceptance_days == 30
        assert group.policies.max_acceptance_severity == Decimal("6.9")
        assert group.policies.max_number_acceptances == 2
        assert group.policies.min_acceptance_severity == Decimal("0.0")
        assert group.policies.min_breaking_severity == Decimal("7.0")
        assert group.policies.vulnerability_grace_period == 61
        assert group.policies.days_until_it_breaks == 100

    group_3 = await get_group(loaders, "group3")
    assert group_3
    assert not group_3.policies
