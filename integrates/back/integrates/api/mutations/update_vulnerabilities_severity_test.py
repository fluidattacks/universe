from decimal import Decimal

from integrates.api.mutations.update_vulnerabilities_severity import mutate
from integrates.custom_exceptions import InvalidStatusForSeverityUpdate, VulnNotInFinding
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.findings.types import FindingUnreliableIndicators
from integrates.db_model.types import SeverityScore
from integrates.db_model.vulnerabilities.enums import VulnerabilityStateStatus
from integrates.decorators import enforce_group_level_auth_async, require_login
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    FindingFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    StakeholderFaker,
    VulnerabilityFaker,
    VulnerabilityStateFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

CVSS_V3 = "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H"
CVSS_V4 = "CVSS:4.0/AV:N/AC:L/AT:N/PR:N/UI:N/VC:H/VI:H/VA:H/SC:N/SI:N/SA:N"


@parametrize(
    args=["user_email"],
    cases=[
        ["admin@gmail.com"],
        ["architect@fluidattacks.com"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker()],
            findings=[
                FindingFaker(
                    id="3c475384-834c-47b0-ac71-a41a022e401c",
                    unreliable_indicators=FindingUnreliableIndicators(
                        max_open_severity_score=Decimal("5.7"),
                        max_open_severity_score_v4=Decimal("6.9"),
                        total_open_cvssf=Decimal("21.112"),
                        total_open_priority=Decimal("0.50"),
                    ),
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    id="be09edb7-cd5c-47ed-bee4-97c645acdce10",
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.SUBMITTED),
                ),
                VulnerabilityFaker(
                    id="be09edb7-cd5c-47ed-bee4-97c645acdce11",
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.SUBMITTED),
                ),
            ],
            stakeholders=[
                StakeholderFaker(email="architect@fluidattacks.com"),
                StakeholderFaker(email="admin@gmail.com"),
            ],
            group_access=[
                GroupAccessFaker(
                    email="architect@fluidattacks.com",
                    state=GroupAccessStateFaker(has_access=True, role="architect"),
                ),
                GroupAccessFaker(
                    email="admin@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
        ),
    )
)
async def test_update_vulnerabilities_severity(user_email: str) -> None:
    # Arrange
    loaders: Dataloaders = get_new_context()
    finding_id: str = "3c475384-834c-47b0-ac71-a41a022e401c"
    vulnerability_ids = [
        "be09edb7-cd5c-47ed-bee4-97c645acdce10",
        "be09edb7-cd5c-47ed-bee4-97c645acdce11",
    ]
    info = GraphQLResolveInfoFaker(
        user_email=user_email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
        ],
    )
    severity_score = SeverityScore(
        base_score=Decimal("10.0"),
        temporal_score=Decimal("10.0"),
        cvss_v3=CVSS_V3,
        cvss_v4=CVSS_V4,
        threat_score=Decimal("9.3"),
        cvssf=Decimal("4096.0"),
        cvssf_v4=Decimal("1552.094"),
    )

    # Act
    await mutate(
        _=None,
        info=info,
        cvss_vector=severity_score.cvss_v3,
        finding_id=finding_id,
        vulnerability_ids=vulnerability_ids,
        cvss_4_vector=str(severity_score.cvss_v4),
    )

    # Assert
    for vulnerability_id in vulnerability_ids:
        vulnerability = await loaders.vulnerability.load(vulnerability_id)
        assert vulnerability
        assert vulnerability.severity_score == severity_score

    finding = await loaders.finding.load(finding_id)
    assert finding


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker()],
            findings=[
                FindingFaker(
                    id="3c475384-834c-47b0-ac71-a41a022e401c",
                    unreliable_indicators=FindingUnreliableIndicators(
                        max_open_severity_score=Decimal("5.7"),
                        max_open_severity_score_v4=Decimal("6.9"),
                        total_open_cvssf=Decimal("21.112"),
                        total_open_priority=Decimal("0.50"),
                    ),
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    id="be09edb7-cd5c-47ed-bee4-97c645acdce10",
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.SUBMITTED),
                ),
                VulnerabilityFaker(
                    id="be09edb7-cd5c-47ed-bee4-97c645acdce11",
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.SUBMITTED),
                ),
            ],
            stakeholders=[
                StakeholderFaker(email="architect@fluidattacks.com"),
            ],
            group_access=[
                GroupAccessFaker(
                    email="architect@fluidattacks.com",
                    state=GroupAccessStateFaker(has_access=True, role="architect"),
                ),
            ],
        ),
    )
)
async def test_update_one_vulnerability_severity() -> None:
    # Arrange
    loaders: Dataloaders = get_new_context()
    user = "architect@fluidattacks.com"
    finding_id: str = "3c475384-834c-47b0-ac71-a41a022e401c"
    vulnerability_ids = [
        "be09edb7-cd5c-47ed-bee4-97c645acdce10",
    ]
    info = GraphQLResolveInfoFaker(
        user_email=user,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
        ],
    )
    severity_score = SeverityScore(
        base_score=Decimal("2.6"),
        temporal_score=Decimal("2.5"),
        cvss_v3="CVSS:3.1/AV:A/AC:H/PR:L/UI:N/S:U/C:L/I:N/A:N/E:P/RL:U/RC:C",
        cvss_v4="CVSS:4.0/AV:N/AC:L/AT:N/PR:N/UI:A/VC:N/VI:N/VA:N/SC:L/SI:L/SA:N",
        threat_score=Decimal("5.1"),
        cvssf=Decimal("0.125"),
        cvssf_v4=Decimal("4.595"),
    )

    # Act
    await mutate(
        _=None,
        info=info,
        cvss_vector=severity_score.cvss_v3,
        finding_id=finding_id,
        vulnerability_ids=vulnerability_ids,
        cvss_4_vector=str(severity_score.cvss_v4),
    )

    # Assert
    for vulnerability_id in vulnerability_ids:
        vulnerability = await loaders.vulnerability.load(vulnerability_id)
        assert vulnerability
        assert vulnerability.severity_score == severity_score

    finding = await loaders.finding.load(finding_id)
    assert finding


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker()],
            findings=[
                FindingFaker(
                    id="3c475384-834c-47b0-ac71-a41a022e401c",
                    unreliable_indicators=FindingUnreliableIndicators(
                        max_open_severity_score=Decimal("5.7"),
                        max_open_severity_score_v4=Decimal("6.9"),
                        total_open_cvssf=Decimal("21.112"),
                        total_open_priority=Decimal("0.50"),
                    ),
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    id="be09edb7-cd5c-47ed-bee4-97c645acdce10",
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.SUBMITTED),
                ),
                VulnerabilityFaker(
                    id="be09edb7-cd5c-47ed-bee4-97c645acdce11",
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.SUBMITTED),
                ),
            ],
            stakeholders=[
                StakeholderFaker(email="architect@fluidattacks.com"),
            ],
            group_access=[
                GroupAccessFaker(
                    email="architect@fluidattacks.com",
                    state=GroupAccessStateFaker(has_access=True, role="architect"),
                ),
            ],
        ),
    )
)
async def test_update_vulnerabilities_severity_to_lower() -> None:
    # Arrange
    loaders: Dataloaders = get_new_context()
    user = "architect@fluidattacks.com"
    finding_id: str = "3c475384-834c-47b0-ac71-a41a022e401c"
    vulnerability_ids = [
        "be09edb7-cd5c-47ed-bee4-97c645acdce10",
        "be09edb7-cd5c-47ed-bee4-97c645acdce11",
    ]
    info = GraphQLResolveInfoFaker(
        user_email=user,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
        ],
    )
    severity_score = SeverityScore(
        base_score=Decimal("2.6"),
        temporal_score=Decimal("2.5"),
        cvss_v3="CVSS:3.1/AV:A/AC:H/PR:L/UI:N/S:U/C:L/I:N/A:N/E:P/RL:U/RC:C",
        cvss_v4="CVSS:4.0/AV:A/AC:H/AT:N/PR:L/UI:N/VC:L/VI:N/VA:N/SC:N/SI:N/SA:N/E:P",
        threat_score=Decimal("1.2"),
        cvssf=Decimal("0.125"),
        cvssf_v4=Decimal("0.021"),
    )

    # Act
    await mutate(
        _=None,
        info=info,
        cvss_vector=severity_score.cvss_v3,
        finding_id=finding_id,
        vulnerability_ids=vulnerability_ids,
        cvss_4_vector=str(severity_score.cvss_v4),
    )

    # Assert
    for vulnerability_id in vulnerability_ids:
        vulnerability = await loaders.vulnerability.load(vulnerability_id)
        assert vulnerability
        assert vulnerability.severity_score == severity_score

    finding = await loaders.finding.load(finding_id)
    assert finding


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker()],
            findings=[
                FindingFaker(
                    id="3c475384-834c-47b0-ac71-a41a022e401c",
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    id="be09edb7-cd5c-47ed-bee4-97c645acdce10",
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401d",
                ),
            ],
            stakeholders=[
                StakeholderFaker(email="architect@fluidattacks.com"),
            ],
            group_access=[
                GroupAccessFaker(
                    email="architect@fluidattacks.com",
                    state=GroupAccessStateFaker(has_access=True, role="architect"),
                ),
            ],
        ),
    )
)
async def test_update_vulnerabilities_severity_vuln_fail() -> None:
    # Arrange
    user = "architect@fluidattacks.com"
    finding_id: str = "3c475384-834c-47b0-ac71-a41a022e401c"
    vulnerability_ids = [
        "be09edb7-cd5c-47ed-bee4-97c645acdce10",
        "be09edb7-cd5c-47ed-bee4-97c645acdce11",
    ]
    info = GraphQLResolveInfoFaker(
        user_email=user,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
        ],
    )
    severity_score = SeverityScore(
        base_score=Decimal("10.0"),
        temporal_score=Decimal("10.0"),
        cvss_v3=CVSS_V3,
        cvss_v4=CVSS_V4,
        threat_score=Decimal("9.3"),
        cvssf=Decimal("4096.0"),
        cvssf_v4=Decimal("1552.094"),
    )

    # Act
    with raises(VulnNotInFinding):
        await mutate(
            _=None,
            info=info,
            cvss_vector=severity_score.cvss_v3,
            finding_id=finding_id,
            vulnerability_ids=vulnerability_ids,
            cvss_4_vector=str(severity_score.cvss_v4),
        )


@parametrize(
    args=["user_email"],
    cases=[
        ["customer_manager@gmail.com"],
        ["resourcer@gmail.com"],
        ["reviewer@gmail.com"],
        ["service_forces@gmail.com"],
        ["user@gmail.com"],
        ["group_manager@gmail.com"],
        ["vulnerability_manager@gmail.com"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker()],
            findings=[
                FindingFaker(
                    id="3c475384-834c-47b0-ac71-a41a022e401c",
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    id="be09edb7-cd5c-47ed-bee4-97c645acdce10",
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                ),
                VulnerabilityFaker(
                    id="be09edb7-cd5c-47ed-bee4-97c645acdce11",
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                ),
            ],
            stakeholders=[
                StakeholderFaker(email="customer_manager@gmail.com"),
                StakeholderFaker(email="resourcer@gmail.com"),
                StakeholderFaker(email="reviewer@gmail.com"),
                StakeholderFaker(email="service_forces@gmail.com"),
                StakeholderFaker(email="user@gmail.com"),
                StakeholderFaker(email="jdoe@fluidattacks.com"),
                StakeholderFaker(email="vulnerability_manager@gmail.com"),
            ],
            group_access=[
                GroupAccessFaker(
                    email="jdoe@fluidattacks.com",
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
                GroupAccessFaker(
                    email="customer_manager@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="customer_manager"),
                ),
                GroupAccessFaker(
                    email="resourcer@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="resourcer"),
                ),
                GroupAccessFaker(
                    email="reviewer@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="reviewer"),
                ),
                GroupAccessFaker(
                    email="service_forces@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="service_forces"),
                ),
                GroupAccessFaker(
                    email="user@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                ),
                GroupAccessFaker(
                    email="vulnerability_manager@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="vulnerability_manager"),
                ),
            ],
        ),
    )
)
async def test_update_vulnerabilities_severity_fail(user_email: str) -> None:
    # Arrange
    finding_id: str = "3c475384-834c-47b0-ac71-a41a022e401c"
    vulnerability_ids = [
        "be09edb7-cd5c-47ed-bee4-97c645acdce10",
        "be09edb7-cd5c-47ed-bee4-97c645acdce11",
    ]
    info = GraphQLResolveInfoFaker(
        user_email=user_email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
        ],
    )
    severity_score = SeverityScore(
        base_score=Decimal("10.0"),
        temporal_score=Decimal("10.0"),
        cvss_v3=CVSS_V3,
        cvss_v4=CVSS_V4,
        threat_score=Decimal("9.3"),
        cvssf=Decimal("4096.0"),
        cvssf_v4=Decimal("1552.094"),
    )

    # Act
    with raises(Exception, match="Access denied"):
        await mutate(
            _=None,
            info=info,
            cvss_vector=severity_score.cvss_v3,
            finding_id=finding_id,
            vulnerability_ids=vulnerability_ids,
            cvss_4_vector=str(severity_score.cvss_v4),
        )


@parametrize(
    args=["user_email"],
    cases=[
        ["admin@gmail.com"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker()],
            findings=[
                FindingFaker(
                    id="3c475384-834c-47b0-ac71-a41a022e401c",
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    id="be09edb7-cd5c-47ed-bee4-97c645acdce10",
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.SAFE),
                ),
                VulnerabilityFaker(
                    id="be09edb7-cd5c-47ed-bee4-97c645acdce11",
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.SAFE),
                ),
            ],
            stakeholders=[
                StakeholderFaker(email="admin@gmail.com"),
            ],
            group_access=[
                GroupAccessFaker(
                    email="admin@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
        ),
    ),
)
async def test_update_vulnerabilities_severity_invalid_status(user_email: str) -> None:
    # Arrange
    finding_id: str = "3c475384-834c-47b0-ac71-a41a022e401c"
    vulnerability_ids = [
        "be09edb7-cd5c-47ed-bee4-97c645acdce10",
        "be09edb7-cd5c-47ed-bee4-97c645acdce11",
    ]
    info = GraphQLResolveInfoFaker(
        user_email=user_email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
        ],
    )
    severity_score = SeverityScore(
        base_score=Decimal("10.0"),
        temporal_score=Decimal("10.0"),
        cvss_v3=CVSS_V3,
        cvss_v4=CVSS_V4,
        threat_score=Decimal("9.3"),
        cvssf=Decimal("4096.0"),
        cvssf_v4=Decimal("1552.094"),
    )

    # Act
    with raises(InvalidStatusForSeverityUpdate):
        await mutate(
            _=None,
            info=info,
            cvss_vector=severity_score.cvss_v3,
            finding_id=finding_id,
            vulnerability_ids=vulnerability_ids,
            cvss_4_vector=str(severity_score.cvss_v4),
        )
