from typing import Any

from graphql import GraphQLError

from integrates.api.mutations.add_finding_consult import mutate
from integrates.custom_exceptions import InvalidCommentParent, InvalidDraftConsult
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.db_model.findings.enums import FindingStatus
from integrates.db_model.findings.types import FindingVulnerabilitiesSummary
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_advanced,
    require_asm,
    require_attribute,
    require_attribute_internal,
    require_is_not_under_review,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    FindingFaker,
    FindingUnreliableIndicatorsFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import parametrize, raises

# Allowed users
ADMIN_EMAIL = "admin@gmail.com"
REVIEWER_EMAIL = "reviewer@gmail.com"

# Not allowed users
CUSTOMER_MANAGER_EMAIL = "customer_manager@gmail.com"
HACKER_EMAIL = "hacker@gmail.com"
REATTACKER_EMAIL = "reattacker@gmail.com"
RESOURCER_EMAIL = "resourcer@gmail.com"
USER_EMAIL = "user@gmail.com"
GROUP_MANAGER_EMAIL = "group_manager@gmail.com"
VULNERABILITY_MANAGER_EMAIL = "vulnerability_manager@gmail.com"
EXECUTIVE_EMAIL = "executive@gmail.com"

# General parameters
GROUP_NAME = "group1"
ERROR_MESSAGE = "Access denied"


DEFAULT_DECORATORS: list[list[Any]] = [
    [require_login],
    [require_is_not_under_review],
    [enforce_group_level_auth_async],
    [require_asm],
    [require_advanced],
    [require_attribute, "has_advanced", GROUP_NAME],
    [require_attribute, "has_asm", GROUP_NAME],
    [require_attribute_internal, "is_under_review", GROUP_NAME],
]


@parametrize(
    args=["email"],
    cases=[
        [CUSTOMER_MANAGER_EMAIL],
        [HACKER_EMAIL],
        [REATTACKER_EMAIL],
        [USER_EMAIL],
        [GROUP_MANAGER_EMAIL],
        [VULNERABILITY_MANAGER_EMAIL],
        [ADMIN_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=CUSTOMER_MANAGER_EMAIL),
                StakeholderFaker(email=HACKER_EMAIL),
                StakeholderFaker(email=REATTACKER_EMAIL),
                StakeholderFaker(email=USER_EMAIL),
                StakeholderFaker(email=GROUP_MANAGER_EMAIL),
                StakeholderFaker(email=VULNERABILITY_MANAGER_EMAIL),
                StakeholderFaker(email=ADMIN_EMAIL),
            ],
            findings=[
                FindingFaker(id="3c475384-834c-47b0-ac71-a41a022e401c", group_name=GROUP_NAME)
            ],
            groups=[
                GroupFaker(name=GROUP_NAME),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=CUSTOMER_MANAGER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="customer_manager"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=HACKER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=REATTACKER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="reattacker"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=USER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=GROUP_MANAGER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=VULNERABILITY_MANAGER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="vulnerability_manager"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
        )
    )
)
async def test_add_finding_consultant(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=DEFAULT_DECORATORS,
    )
    await mutate(
        _=None,
        info=info,
        user=email,
        content="This is a comment test",
        finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
        type="CONSULT",
        parent_comment="0",
        group_name=GROUP_NAME,
    )


@parametrize(
    args=["email"],
    cases=[
        [ADMIN_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=ADMIN_EMAIL),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME),
            ],
            findings=[
                FindingFaker(id="3c475384-834c-47b0-ac71-a41a022e401c", group_name=GROUP_NAME)
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
        )
    ),
    others=[Mock(logs_utils, "cloudwatch_log", "sync", None)],
)
async def test_add_finding_consultant_reply(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=DEFAULT_DECORATORS,
    )
    await mutate(
        _=None,
        info=info,
        user=email,
        content="This is a reply to a comment test",
        finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
        type="COMMENT",
        parent_comment="0",
    )
    await mutate(
        _=None,
        info=info,
        user=email,
        content="This is a reply to a verification comment test",
        finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
        type="VERIFICATION",
        parent_comment="0",
    )
    with raises(InvalidCommentParent):
        await mutate(
            _=None,
            info=info,
            user=email,
            content="This is invalid reply to a comment test",
            finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
            type="COMMENT",
            parent_comment="1558048727112",
        )


@parametrize(
    args=["email"],
    cases=[
        [RESOURCER_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=RESOURCER_EMAIL),
            ],
            findings=[
                FindingFaker(id="3c475384-834c-47b0-ac71-a41a022e401c", group_name=GROUP_NAME)
            ],
            group_access=[
                GroupAccessFaker(
                    email=RESOURCER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="resourcer"),
                ),
            ],
        )
    )
)
async def test_add_finding_consultant_fail(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=DEFAULT_DECORATORS,
    )
    with raises(Exception, match=ERROR_MESSAGE):
        await mutate(
            _=None,
            info=info,
            user=email,
            content="This is a observation test",
            finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
            type="CONSULT",
            parent_comment="0",
        )


@parametrize(
    args=["email"],
    cases=[
        [HACKER_EMAIL],
        [REATTACKER_EMAIL],
        [USER_EMAIL],
        [VULNERABILITY_MANAGER_EMAIL],
        [ADMIN_EMAIL],
        [EXECUTIVE_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=CUSTOMER_MANAGER_EMAIL),
                StakeholderFaker(email=HACKER_EMAIL),
                StakeholderFaker(email=REATTACKER_EMAIL),
                StakeholderFaker(email=USER_EMAIL),
                StakeholderFaker(email=VULNERABILITY_MANAGER_EMAIL),
                StakeholderFaker(email=EXECUTIVE_EMAIL),
                StakeholderFaker(email=ADMIN_EMAIL),
            ],
            findings=[FindingFaker(id="697510163", group_name=GROUP_NAME)],
            group_access=[
                GroupAccessFaker(
                    email=CUSTOMER_MANAGER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="customer_manager"),
                ),
                GroupAccessFaker(
                    email=HACKER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
                GroupAccessFaker(
                    email=REATTACKER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="reattacker"),
                ),
                GroupAccessFaker(
                    email=USER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                ),
                GroupAccessFaker(
                    email=VULNERABILITY_MANAGER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="vulnerability_manager"),
                ),
                GroupAccessFaker(
                    email=EXECUTIVE_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="executive"),
                ),
                GroupAccessFaker(
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
        )
    )
)
async def test_add_finding_consult_without_advanced(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=DEFAULT_DECORATORS,
    )
    with raises(Exception, match=ERROR_MESSAGE):
        await mutate(
            _=None,
            info=info,
            user=email,
            content="This is a consulting test",
            finding_id="697510163",
            type="CONSULT",
            parent_comment="0",
        )


@parametrize(
    args=["email"],
    cases=[
        [ADMIN_EMAIL],
        [HACKER_EMAIL],
        [REVIEWER_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=HACKER_EMAIL),
                StakeholderFaker(email=REVIEWER_EMAIL),
                StakeholderFaker(email=ADMIN_EMAIL),
            ],
            findings=[FindingFaker(id="697510163", group_name=GROUP_NAME)],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=HACKER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=REVIEWER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="reviewer"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
        )
    )
)
async def test_add_finding_observation_without_advanced(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=DEFAULT_DECORATORS,
    )
    await mutate(
        _=None,
        info=info,
        user=email,
        content="This is a observation test",
        finding_id="697510163",
        type="OBSERVATION",
        parent_comment="0",
    )


@parametrize(
    args=["email"],
    cases=[
        [ADMIN_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=ADMIN_EMAIL),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME),
            ],
            findings=[
                FindingFaker(id="3c475384-834c-47b0-ac71-a41a022e401c", group_name=GROUP_NAME)
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
        )
    )
)
async def test_add_finding_observation_reply(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=DEFAULT_DECORATORS,
    )
    await mutate(
        _=None,
        info=info,
        user=email,
        content="This is a reply to a observation test",
        finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
        type="OBSERVATION",
        parent_comment="0",
        group_name=GROUP_NAME,
    )
    with raises(InvalidCommentParent):
        await mutate(
            _=None,
            info=info,
            user=email,
            content="This is invalid reply to a observation test",
            finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
            type="OBSERVATION",
            parent_comment="1673975237896",
            group_name=GROUP_NAME,
        )


@parametrize(
    args=["email"],
    cases=[
        [ADMIN_EMAIL],
        [HACKER_EMAIL],
        [REATTACKER_EMAIL],
        [USER_EMAIL],
        [VULNERABILITY_MANAGER_EMAIL],
        [CUSTOMER_MANAGER_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(name=GROUP_NAME),
            ],
            stakeholders=[
                StakeholderFaker(email=CUSTOMER_MANAGER_EMAIL),
                StakeholderFaker(email=HACKER_EMAIL),
                StakeholderFaker(email=REATTACKER_EMAIL),
                StakeholderFaker(email=USER_EMAIL),
                StakeholderFaker(email=VULNERABILITY_MANAGER_EMAIL),
                StakeholderFaker(email=ADMIN_EMAIL),
            ],
            findings=[
                FindingFaker(
                    id="3c475384-834c-47b0-ac71-a41a02e4031c",
                    group_name=GROUP_NAME,
                    unreliable_indicators=FindingUnreliableIndicatorsFaker(
                        unreliable_status=FindingStatus.DRAFT,
                        vulnerabilities_summary=FindingVulnerabilitiesSummary(open=0, closed=0),
                    ),
                )
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=CUSTOMER_MANAGER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="customer_manager"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=HACKER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=REATTACKER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="reattacker"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=USER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=VULNERABILITY_MANAGER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="vulnerability_manager"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
        )
    )
)
async def test_add_finding_consult_draft_fail(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=DEFAULT_DECORATORS,
    )
    with raises(InvalidDraftConsult):
        await mutate(
            _=None,
            info=info,
            user=email,
            content="This is a comment test",
            finding_id="3c475384-834c-47b0-ac71-a41a02e4031c",
            type="CONSULT",
            parent_comment="0",
            group_name=GROUP_NAME,
        )


@parametrize(
    args=["email"],
    cases=[
        [RESOURCER_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=RESOURCER_EMAIL),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME),
            ],
            findings=[
                FindingFaker(id="3c475384-834c-47b0-ac71-a41a022e401c", group_name=GROUP_NAME)
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=RESOURCER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="resourcer"),
                ),
            ],
        )
    )
)
async def test_add_finding_observation_fail(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=DEFAULT_DECORATORS,
    )
    with raises(GraphQLError, match="Access denied"):
        await mutate(
            _=None,
            info=info,
            user=email,
            content="This is a reply to a observation test",
            finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
            type="OBSERVATION",
            parent_comment="0",
            group_name=GROUP_NAME,
        )
