from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.api.types import (
    APP_EXCEPTIONS,
)
from integrates.batch import (
    dal as batch_dal,
)
from integrates.batch.enums import (
    Action,
    IntegratesBatchQueue,
)
from integrates.custom_exceptions import (
    PriorityScoreUpdateAlreadyRequested,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.decorators import (
    enforce_organization_level_auth_async,
)
from integrates.organizations import (
    utils as orgs_utils,
)
from integrates.organizations_priority_policies.domain import (
    remove_priority_policy,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("removeOrganizationPriorityPolicy")
@enforce_organization_level_auth_async
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    **kwargs: str,
) -> SimplePayload:
    try:
        loaders: Dataloaders = info.context.loaders
        organization_id = kwargs.pop("organization_id")
        priority_policy = kwargs.pop("priority_policy")
        user_info = await sessions_domain.get_jwt_content(info.context)
        organization = await orgs_utils.get_organization(loaders, organization_id)
        existing_actions = await batch_dal.get_actions_by_name(
            action_name=Action.UPDATE_ORGANIZATION_PRIORITY_POLICIES,
            entity=organization.name,
        )
        if len(existing_actions) > 0:
            raise PriorityScoreUpdateAlreadyRequested()

        await remove_priority_policy(
            loaders=loaders,
            organization_id=organization_id,
            policy_to_remove=priority_policy,
        )
        await batch_dal.put_action(
            action=Action.UPDATE_ORGANIZATION_PRIORITY_POLICIES,
            entity=organization.name,
            subject=user_info["user_email"],
            additional_info={"policy_to_remove": priority_policy},
            queue=IntegratesBatchQueue.SMALL,
        )

        logs_utils.cloudwatch_log(
            info.context,
            "Removed priority policy in the organization",
            extra={
                "organization_name": organization.name,
                "organization_id": organization_id,
                "priority_policy": priority_policy,
                "log_type": "Security",
            },
        )
    except APP_EXCEPTIONS:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to remove priority policy in the organization",
            extra={
                "organization_name": organization.name,
                "organization_id": organization_id,
                "log_type": "Security",
            },
        )
        raise

    return SimplePayload(success=True)
