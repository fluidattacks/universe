from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_exceptions import (
    UnavailabilityError,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.decorators import (
    require_login,
)
from integrates.sessions import (
    domain as sessions_domain,
)
from integrates.stakeholders import (
    domain as stakeholders_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("invalidateAccessToken")
@require_login
async def mutate(_: None, info: GraphQLResolveInfo, **kwargs: str) -> SimplePayload:
    user_info = await sessions_domain.get_jwt_content(info.context)
    token_id = kwargs.get("id")
    try:
        await stakeholders_domain.remove_access_token(
            user_info["user_email"],
            info.context.loaders,
            token_id,
        )
        logs_utils.cloudwatch_log(
            info.context,
            "Invalidated access token",
            extra={
                "user_email": user_info["user_email"],
                "log_type": "Security",
            },
        )
    except UnavailabilityError:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to invalidate access token",
            extra={
                "user_email": user_info["user_email"],
                "log_type": "Security",
            },
        )

    return SimplePayload(success=True)
