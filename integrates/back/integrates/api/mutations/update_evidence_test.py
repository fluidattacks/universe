from io import BytesIO

from starlette.datastructures import Headers, UploadFile

from integrates.api.mutations.update_evidence import mutate
from integrates.custom_utils import (
    files as files_utils,
)
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_asm,
    require_attribute,
    require_finding_access,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    FindingFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import parametrize, raises

ADMIN_EMAIL = "admin@gmail.com"
HACKER_EMAIL = "hacker@fluidattacks.com"
GROUP_NAME = "test-group"
RESOURCER_EMAIL = "resourcer@fluidattacks.com"
CUSTOMER_MANAGER_EMAIL = "customer_manager@fluidattacks.com"
VULNERABILITY_MANAGER_EMAIL = "vulnerability_manager@gmail.com"
USER_EMAIL = "user@gmail.com"


@parametrize(
    args=["email", "finding_id", "evidence_id"],
    cases=[
        [ADMIN_EMAIL, "3c475384-834c-47b0-ac71-a41a022e401c", "evidence_route_1"],
        [HACKER_EMAIL, "3c475384-834c-47b0-ac71-a41a022e401c", "evidence_route_2"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=ADMIN_EMAIL),
                StakeholderFaker(email=HACKER_EMAIL),
            ],
            findings=[
                FindingFaker(id="3c475384-834c-47b0-ac71-a41a022e401c", group_name=GROUP_NAME)
            ],
            organizations=[OrganizationFaker(id="org1")],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=HACKER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
            ],
        )
    ),
    others=[Mock(files_utils, "assert_uploaded_file_mime", "async", True)],
)
async def test_update_evidence_success(email: str, finding_id: str, evidence_id: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
            [require_finding_access],
        ],
    )
    test_file = UploadFile(
        filename=f"test-org-{GROUP_NAME}-testing123.webm",
        file=BytesIO(b"test content"),
        headers=Headers(headers={"content_type": "video/webm"}),
    )
    result = await mutate(
        _=None,
        info=info,
        file=test_file,
        finding_id=finding_id,
        evidence_id=evidence_id,
    )
    assert result.success


@parametrize(
    args=["email"],
    cases=[
        [RESOURCER_EMAIL],
        [USER_EMAIL],
        [CUSTOMER_MANAGER_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=RESOURCER_EMAIL),
                StakeholderFaker(email=USER_EMAIL),
                StakeholderFaker(email=CUSTOMER_MANAGER_EMAIL),
            ],
            findings=[
                FindingFaker(id="3c475384-834c-47b0-ac71-a41a022e401c", group_name=GROUP_NAME)
            ],
            group_access=[
                GroupAccessFaker(
                    email=RESOURCER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="resourcer"),
                ),
                GroupAccessFaker(
                    email=USER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                ),
                GroupAccessFaker(
                    email=CUSTOMER_MANAGER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="customer_manager"),
                ),
            ],
        )
    )
)
async def test_update_evidence_unauthorized(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
            [require_finding_access],
        ],
    )
    file_mock = UploadFile(filename="test.png", file=BytesIO(b"test content"))
    with raises(Exception, match="Access denied"):
        await mutate(
            _=None,
            info=info,
            file=file_mock,
            finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
            evidence_id="evidence_001",
        )


@parametrize(
    args=["email", "finding_id", "evidence_id"],
    cases=[
        [ADMIN_EMAIL, "invalid_finding", "evidence_001"],
        [ADMIN_EMAIL, "3c475384-834c-47b0-ac71-a41a022e401c", "invalid_evidence"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[StakeholderFaker(email=ADMIN_EMAIL)],
            findings=[
                FindingFaker(id="3c475384-834c-47b0-ac71-a41a022e401c", group_name=GROUP_NAME)
            ],
            organizations=[OrganizationFaker(id="org1")],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
        )
    ),
    others=[Mock(files_utils, "assert_uploaded_file_mime", "async", True)],
)
async def test_update_evidence_invalid_ids(email: str, finding_id: str, evidence_id: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
            [require_finding_access],
        ],
    )
    test_file = UploadFile(
        filename=f"test-org-{GROUP_NAME}-testing123.webm",
        file=BytesIO(b"test content"),
        headers=Headers(headers={"content_type": "video/webm"}),
    )
    with raises(Exception):
        await mutate(
            _=None,
            info=info,
            file=test_file,
            finding_id=finding_id,
            evidence_id=evidence_id,
        )
