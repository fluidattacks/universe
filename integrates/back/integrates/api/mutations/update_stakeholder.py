from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.db_model.stakeholders.types import (
    StakeholderMetadataToUpdate,
    StakeholderPhone,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_user_level_auth_async,
    require_login,
)
from integrates.sessions import (
    domain as sessions_domain,
)
from integrates.stakeholders import (
    domain as stakeholders_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("updateStakeholder")
@concurrent_decorators(
    require_login,
    enforce_user_level_auth_async,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    phone: dict[str, str],
) -> SimplePayload:
    user_data = await sessions_domain.get_jwt_content(info.context)
    user_email = user_data["user_email"]

    new_phone = StakeholderPhone(
        national_number=phone["national_number"],
        calling_country_code=phone["calling_country_code"],
        country_code="",
    )

    await stakeholders_domain.update(
        email=user_email,
        metadata=StakeholderMetadataToUpdate(
            phone=new_phone,
        ),
    )

    logs_utils.cloudwatch_log(
        info.context,
        "Update user",
        extra={
            "user_email": user_email,
            "log_type": "Security",
        },
    )

    return SimplePayload(success=True)
