from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.db_model.mailmap.types import (
    MailmapEntry,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_organization_level_auth_async,
    require_login,
)
from integrates.mailmap import (
    create_mailmap_entry,
)

from .payloads.types import (
    MailmapEntryWithSubentriesPayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("createMailmapEntry")
@concurrent_decorators(
    require_login,
    enforce_organization_level_auth_async,
)
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    entry: MailmapEntry,
    organization_id: str,
) -> MailmapEntryWithSubentriesPayload:
    entry_with_subentries = await create_mailmap_entry(
        entry=entry,
        organization_id=organization_id,
    )
    entry_with_subentries_payload = MailmapEntryWithSubentriesPayload(
        success=True,
        entry_with_subentries=entry_with_subentries,
    )

    logs_utils.cloudwatch_log(
        info.context,
        "Created mailmap author",
        extra={
            "entry_email": entry["mailmap_entry_email"],
            "organization_id": organization_id,
        },
    )

    return entry_with_subentries_payload
