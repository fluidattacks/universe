from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.utils import (
    format_policies_to_update,
)
from integrates.decorators import (
    enforce_organization_level_auth_async,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("updateOrganizationPolicies")
@enforce_organization_level_auth_async
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    **kwargs: str,
) -> SimplePayload:
    loaders: Dataloaders = info.context.loaders
    user_data = await sessions_domain.get_jwt_content(info.context)
    user_email = user_data["user_email"]
    organization_id = kwargs.pop("organization_id")
    organization_name = kwargs.pop("organization_name")
    policies_to_update = format_policies_to_update(kwargs)
    await orgs_domain.update_policies(
        loaders,
        organization_id,
        organization_name,
        user_email,
        policies_to_update,
    )
    logs_utils.cloudwatch_log(
        info.context,
        "Updated policies in the organization",
        extra={
            "user_email": user_email,
            "organization_name": organization_name,
            "organization_id": organization_id,
            "log_type": "Security",
        },
    )
    return SimplePayload(success=True)
