from integrates.api.mutations.add_root_docker_image import mutate
from integrates.api.mutations.payloads.types import AddRootDockerImagePayload
from integrates.custom_exceptions import (
    CredentialNotFound,
    InvalidRootType,
    RootDockerImageNotFound,
    UriFormatError,
)
from integrates.dataloaders import get_new_context
from integrates.db_model.credentials.types import HttpsSecret
from integrates.db_model.roots.types import RootDockerImage, RootDockerImageRequest
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute,
    require_attribute_internal,
    require_login,
)
from integrates.jobs_orchestration import create_sbom_config
from integrates.roots import domain as roots_domain
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    CredentialsFaker,
    GitRootFaker,
    GitRootStateFaker,
    GraphQLResolveInfoFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
    ToeLinesFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import parametrize, raises

ADMIN_EMAIL = "admin@gmail.com"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db")],
            stakeholders=[StakeholderFaker(email=ADMIN_EMAIL, role="admin")],
            groups=[
                GroupFaker(
                    name="group1", organization_id="ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db"
                )
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="63298a73-9dff-46cf-b42d-9b2f01a56690",
                    state=GitRootStateFaker(
                        nickname="git_1",
                    ),
                ),
            ],
            toe_lines=[
                ToeLinesFaker(
                    filename="Dockerfile",
                    group_name="group1",
                    root_id="63298a73-9dff-46cf-b42d-9b2f01a56690",
                )
            ],
            credentials=[
                CredentialsFaker(
                    credential_id="cred1",
                    organization_id="ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db",
                    secret=HttpsSecret(user="admin@gmail.com", password="pass"),  # noqa: S106
                )
            ],
        ),
    )
)
async def test_add_docker_image_cred_fail() -> None:
    group_name = "group1"
    root_id = "63298a73-9dff-46cf-b42d-9b2f01a56690"
    credentials: dict = {
        "id": "cred1",
    }

    info = GraphQLResolveInfoFaker(
        user_email=ADMIN_EMAIL,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_service_white", group_name],
            [require_attribute_internal, "is_under_review", group_name],
        ],
    )

    with raises(CredentialNotFound):
        result = await mutate(
            _parent=None,
            info=info,
            user_email=ADMIN_EMAIL,
            credentials=credentials,
            group_name=group_name,
            uri="fluidattacks/forces:latest",
            root_id=root_id,
        )
        assert "errors" in result
        assert result["errors"][0]["message"] == "Exception - Access denied or credential not found"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=ADMIN_EMAIL, role="admin")],
            groups=[GroupFaker(name="group1", organization_id="org1")],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="63298a73-9dff-46cf-b42d-9b2f01a56690",
                    state=GitRootStateFaker(
                        nickname="git_1",
                    ),
                ),
            ],
            toe_lines=[
                ToeLinesFaker(
                    filename="Dockerfile",
                    group_name="group1",
                    root_id="63298a73-9dff-46cf-b42d-9b2f01a56690",
                )
            ],
        ),
    )
)
async def test_add_docker_image_image_not_found() -> None:
    group_name: str = "group1"
    root_id = "63298a73-9dff-46cf-b42d-9b2f01a56690"
    info = GraphQLResolveInfoFaker(
        user_email=ADMIN_EMAIL,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_service_white", group_name],
            [require_attribute_internal, "is_under_review", group_name],
        ],
    )

    with raises(RootDockerImageNotFound):
        result = await mutate(
            _parent=None,
            info=info,
            user_email=ADMIN_EMAIL,
            group_name=group_name,
            uri="fluidattacks/forces:latest1",
            root_id=root_id,
        )

        assert "errors" in result
        assert result["errors"][0]["message"] == "Exception - Root image not found"


@parametrize(
    args=["credentials"],
    cases=[
        [
            {"id": "cred1"},
        ],
        [
            {},
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db")],
            stakeholders=[StakeholderFaker(email=ADMIN_EMAIL, role="admin")],
            groups=[
                GroupFaker(
                    name="group1", organization_id="ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db"
                )
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="63298a73-9dff-46cf-b42d-9b2f01a56690",
                    state=GitRootStateFaker(
                        nickname="git_1",
                    ),
                ),
            ],
            toe_lines=[
                ToeLinesFaker(
                    filename="Dockerfile",
                    group_name="group1",
                    root_id="63298a73-9dff-46cf-b42d-9b2f01a56690",
                )
            ],
            credentials=[
                CredentialsFaker(
                    credential_id="cred1",
                    organization_id="ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db",
                    secret=HttpsSecret(user="admin@gmail.com", password="pass"),  # noqa: S106
                )
            ],
        ),
    ),
    others=[
        Mock(
            create_sbom_config,
            "upload_sbom_config_to_s3",
            "async",
            None,
        ),
        Mock(
            roots_domain,
            "get_image_manifest",
            "async",
            (
                {
                    "Digest": "sha256:fe6a5f3f5555b3c6bfb5b6e0e"
                    "7639db8b8c2272d0848f214d7340d92521bf42d",
                    "Layers": [
                        "sha256:de44b265507ae44b212defcb50694d666f136b35c1090d9709068bc861bb2d64",
                        "sha256:add2cfa32b4d238ee12f15bfd6aa93ccfe9642fd196c8547875c8283b2368d7a",
                    ],
                    "LayersData": [
                        {
                            "MIMEType": "application/vnd.oci.image.layer.v1.tar+gzip",
                            "Digest": "sha256:de44b265507ae44b212defcb50694d666f136b35"
                            "c1090d9709068bc861bb2d64",
                            "Size": 29751968,
                            "Annotations": None,
                        },
                        {
                            "MIMEType": "application/vnd.oci.image.layer.v1.tar+gzip",
                            "Digest": "sha256:add2cfa32b4d238ee12f15bfd6aa93ccfe9642fd"
                            "196c8547875c8283b2368d7a",
                            "Size": 1217,
                            "Annotations": None,
                        },
                    ],
                }
            ),
        ),
    ],
)
async def test_add_docker_image(credentials: dict[str, str]) -> None:
    group_name = "group1"
    root_id = "63298a73-9dff-46cf-b42d-9b2f01a56690"
    uri = "fluidattacks/forces:latest"
    info = GraphQLResolveInfoFaker(
        user_email=ADMIN_EMAIL,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_service_white", group_name],
            [require_attribute_internal, "is_under_review", group_name],
        ],
    )

    result: AddRootDockerImagePayload = await mutate(
        _parent=None,
        info=info,
        user_email=ADMIN_EMAIL,
        group_name=group_name,
        credentials=credentials,
        uri=uri,
        root_id=root_id,
    )
    assert "errors" not in result
    assert result == AddRootDockerImagePayload(
        uri="fluidattacks/forces:latest", sbom_job_queued=True, success=True
    )

    loaders = get_new_context()
    uri = result.uri
    image: RootDockerImage | None = await loaders.docker_image.load(
        RootDockerImageRequest(root_id, group_name, uri)
    )
    assert image
    assert image.state.status == "CREATED"
    assert image.state.digest
    assert image.state.digest.startswith("sha256:")
    if credentials:
        assert image.state.credential_id == credentials["id"]


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=ADMIN_EMAIL, role="admin")],
            groups=[
                GroupFaker(name="group1", organization_id="org1"),
                GroupFaker(name="group2", organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="63298a73-9dff-46cf-b42d-9b2f01a56690",
                    state=GitRootStateFaker(
                        nickname="git_1",
                    ),
                ),
            ],
            toe_lines=[
                ToeLinesFaker(
                    filename="Dockerfile",
                    group_name="group1",
                    root_id="63298a73-9dff-46cf-b42d-9b2f01a56690",
                )
            ],
        ),
    )
)
async def test_add_git_root_invalid_group() -> None:
    group_name: str = "group2"
    root_id = "63298a73-9dff-46cf-b42d-9b2f01a56690"
    credentials: dict = {}
    uri = "fluidattacks/forces:test"

    info = GraphQLResolveInfoFaker(
        user_email=ADMIN_EMAIL,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_service_white", group_name],
            [require_attribute_internal, "is_under_review", group_name],
        ],
    )
    with raises(InvalidRootType):
        result = await mutate(
            _parent=None,
            info=info,
            credentials=credentials,
            user_email=ADMIN_EMAIL,
            group_name=group_name,
            uri=uri,
            root_id=root_id,
        )
        assert "errors" in result
        assert result["errors"][0]["message"] == "Exception - The type of the root is invalid"


@parametrize(
    args=["email"],
    cases=[["hacker@gmail.com"], ["reattacker@gmail.com"]],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=ADMIN_EMAIL, role="admin")],
            groups=[
                GroupFaker(name="group1", organization_id="org1"),
                GroupFaker(name="group2", organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="63298a73-9dff-46cf-b42d-9b2f01a56690",
                    state=GitRootStateFaker(
                        nickname="git_1",
                    ),
                ),
            ],
            toe_lines=[
                ToeLinesFaker(
                    filename="Dockerfile",
                    group_name="group1",
                    root_id="63298a73-9dff-46cf-b42d-9b2f01a56690",
                )
            ],
        ),
    )
)
async def test_add_git_root_access_denied(email: str) -> None:
    group_name: str = "group1"
    root_id = "63298a73-9dff-46cf-b42d-9b2f01a56690"
    credentials: dict = {
        "token": "token",
        "name": "Credentials test",
        "type": "HTTPS",
    }
    uri = "fluidattacks/forces:test"
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_service_white", group_name],
            [require_attribute_internal, "is_under_review", group_name],
        ],
    )

    with raises(Exception, match="Access denied"):
        result = await mutate(
            _parent=None,
            info=info,
            credentials=credentials,
            user_email=email,
            group_name=group_name,
            uri=uri,
            root_id=root_id,
        )
        assert "errors" in result
        assert result["errors"][0]["message"] == "Access denied"


@parametrize(
    args=["email", "uri"],
    cases=[[ADMIN_EMAIL, "docker.io/test_image"]],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=ADMIN_EMAIL, role="admin")],
            groups=[
                GroupFaker(name="group1", organization_id="org1"),
                GroupFaker(name="group2", organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="63298a73-9dff-46cf-b42d-9b2f01a56690",
                    state=GitRootStateFaker(
                        nickname="git_1",
                    ),
                ),
            ],
            toe_lines=[
                ToeLinesFaker(
                    filename="Dockerfile",
                    group_name="group1",
                    root_id="63298a73-9dff-46cf-b42d-9b2f01a56690",
                )
            ],
        ),
    )
)
async def test_add_docker_image_invalid_credentials(
    email: str,
    uri: str,
) -> None:
    group_name: str = "group1"
    root_id = "63298a73-9dff-46cf-b42d-9b2f01a56690"
    credentials: dict = {
        "isPat": False,
        "name": "TEST",
        "type": "HTTPS",
        "user": "x",
        "password": "x",
    }
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_service_white", group_name],
            [require_attribute_internal, "is_under_review", group_name],
        ],
    )
    with raises(CredentialNotFound):
        result = await mutate(
            _parent=None,
            info=info,
            credentials=credentials,
            user_email=email,
            group_name=group_name,
            uri=uri,
            root_id=root_id,
        )
        assert "errors" in result
        assert result["errors"][0]["message"] == "Exception - Access denied or credential not found"


@parametrize(
    args=["email", "uri"],
    cases=[
        [ADMIN_EMAIL, "docker.io/library/imag@name:latest"],
        [
            ADMIN_EMAIL,
            "registry.example.com:abcd/namespace/repo/image:latest",
        ],
        [ADMIN_EMAIL, "registry.example.com/namespace//image:latest"],
        [ADMIN_EMAIL, "docker.io/library/:latest"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=ADMIN_EMAIL, role="admin")],
            groups=[
                GroupFaker(name="group1", organization_id="org1"),
                GroupFaker(name="group2", organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="63298a73-9dff-46cf-b42d-9b2f01a56690",
                    state=GitRootStateFaker(
                        nickname="git_1",
                    ),
                ),
            ],
            toe_lines=[
                ToeLinesFaker(
                    filename="Dockerfile",
                    group_name="group1",
                    root_id="63298a73-9dff-46cf-b42d-9b2f01a56690",
                )
            ],
        ),
    )
)
async def test_add_docker_image_format_incorrect(
    email: str,
    uri: str,
) -> None:
    group_name: str = "group1"
    root_id = "63298a73-9dff-46cf-b42d-9b2f01a56690"
    credentials: dict = {
        "isPat": False,
        "name": "TEST",
        "type": "HTTPS",
        "user": "x",
        "password": "x",
    }
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_service_white", group_name],
            [require_attribute_internal, "is_under_review", group_name],
        ],
    )

    with raises(UriFormatError):
        result = await mutate(
            _parent=None,
            info=info,
            credentials=credentials,
            user_email=email,
            group_name=group_name,
            uri=uri,
            root_id=root_id,
        )
        assert "errors" in result
        assert result["errors"][0]["message"] == "Exception - URI format incorrect"
