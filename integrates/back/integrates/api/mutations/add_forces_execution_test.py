from datetime import datetime
from io import BytesIO

from starlette.datastructures import UploadFile

from integrates.api.mutations.add_forces_execution import mutate
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute_internal,
    require_is_not_under_review,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks

EMAIL_TEST = "admin@fluidattacks.com"
GROUP_NAME = "test-group"
ORG_ID = "ORG#40f6da5f-4f21-4168-b8fa-9f2ce3d4dd93"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id=ORG_ID),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_TEST, role="admin"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id=ORG_ID),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=EMAIL_TEST,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
        ),
    )
)
async def test_add_forces_execution_with_log() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[
            [enforce_group_level_auth_async],
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )
    mock_file = UploadFile(filename="test.log", file=BytesIO(b"test log content"))
    result = await mutate(
        _parent=None,
        info=info,
        group_name=GROUP_NAME,
        json_log='{"test": "data"}',
        log=mock_file,
        additional_param="test",
        execution_id="18c1e735a73243f2ab1ee0757041f80e",
        date=datetime.fromisoformat("2020-02-20T00:00:00+00:00"),
        exit_code="1",
        git_branch="master",
        git_commit="2e7b34c1358db2ff4123c3c76e7fe3bf9f2838f2",
        git_origin="http://origin-test.com",
        git_repo="Repository",
        kind="dynamic",
        strictness="strict",
        grace_period=0,
        severity_threshold=0.0,
        days_until_it_breaks=None,
        managed_vulnerabilities=2,
        un_managed_vulnerabilities=1,
        vulnerabilities={
            "accepted": [
                {
                    "exploitability": 3.1,
                    "kind": "DAST",
                    "state": "ACCEPTED",
                    "where": "HTTP/Implementation",
                    "who": "https://accepted.com/test",
                }
            ],
            "closed": [
                {
                    "exploitability": 3.2,
                    "kind": "DAST",
                    "state": "CLOSED",
                    "where": "HTTP/Implementation",
                    "who": "https://closed.com/test",
                }
            ],
            "open": [
                {
                    "exploitability": 3.3,
                    "kind": "DAST",
                    "state": "OPEN",
                    "where": "HTTP/Implementation",
                    "who": "https://open.com/test",
                }
            ],
        },
    )
    assert result.success
