from integrates.api.mutations.close_vulnerabilities import mutate
from integrates.api.mutations.payloads.types import SimplePayload
from integrates.dataloaders import get_new_context
from integrates.db_model.findings.types import (
    FindingUnreliableIndicators,
    FindingVulnerabilitiesSummary,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateReason,
    VulnerabilityStateStatus,
)
from integrates.decorators import enforce_group_level_auth_async, require_attribute, require_login
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    FindingFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    StakeholderFaker,
    VulnerabilityFaker,
    VulnerabilityStateFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

CVSS_V3 = "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H"
CVSS_V4 = "CVSS:4.0/AV:N/AC:L/AT:N/PR:N/UI:N/VC:H/VI:H/VA:H/SC:N/SI:N/SA:N"


@parametrize(
    args=["email", "vulnerability_id", "finding_id"],
    cases=[
        [
            "hacker@fluidattacks.com",
            "891b1d7f-a0fa-4e3d-834a-d07a9ad4b46f",
            "3c475384-834c-47b0-ac71-a41a022e401c",
        ],
        [
            "reattacker@fluidattacks.com",
            "8e5b7694-1dca-4a1b-afbd-d7701843d97c",
            "3c475384-834c-47b0-ac71-a41a022e401c",
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker()],
            findings=[
                FindingFaker(
                    id="3c475384-834c-47b0-ac71-a41a022e401c",
                    unreliable_indicators=FindingUnreliableIndicators(
                        vulnerabilities_summary=FindingVulnerabilitiesSummary(
                            closed=0,
                        ),
                    ),
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    id="891b1d7f-a0fa-4e3d-834a-d07a9ad4b46f",
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                ),
                VulnerabilityFaker(
                    id="8e5b7694-1dca-4a1b-afbd-d7701843d97c",
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                ),
            ],
            stakeholders=[
                StakeholderFaker(email="hacker@fluidattacks.com"),
                StakeholderFaker(email="reattacker@fluidattacks.com"),
            ],
            group_access=[
                GroupAccessFaker(
                    email="hacker@fluidattacks.com",
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
                GroupAccessFaker(
                    email="reattacker@fluidattacks.com",
                    state=GroupAccessStateFaker(has_access=True, role="reattacker"),
                ),
            ],
        ),
    )
)
async def test_close_vulnerabilities(
    email: str,
    vulnerability_id: str,
    finding_id: str,
) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "can_report_vulnerabilities", "test-group"],
        ],
    )
    loaders = get_new_context()
    vulnerability = await loaders.vulnerability.load(vulnerability_id)
    assert vulnerability
    assert vulnerability.state.status == VulnerabilityStateStatus.VULNERABLE

    finding = await loaders.finding.load(finding_id)
    assert finding is not None

    result: SimplePayload = await mutate(
        _=None,
        info=info,
        finding_id=finding_id,
        vulnerabilities=[vulnerability_id],
        justification=VulnerabilityStateReason.VERIFIED_AS_SAFE,
    )

    assert result.success

    loaders.vulnerability.clear(vulnerability_id)
    vulnerability = await loaders.vulnerability.load(vulnerability_id)
    assert vulnerability
    assert vulnerability.state.status == VulnerabilityStateStatus.SAFE
    assert vulnerability.state.modified_by == email


@parametrize(
    args=["email", "vulnerability_id", "finding_id", "error_message"],
    cases=[
        [
            "admin@fluidattacks.com",
            "3d982277-a737-4046-ab7d-f3a2c4e6ecad",
            "3c475384-834c-47b0-ac71-a41a022e401c",
            "Exception - The vulnerability has not been released",
        ],
        [
            "hacker@fluidattacks.com",
            "2ed00278-4481-4a44-8da4-04fe41751486",
            "3c475384-834c-47b0-ac71-a41a022e401c",
            "Exception - The vulnerability has already been closed",
        ],
        [
            "reattacker@fluidattacks.com",
            "d6136f40-7609-4ac0-9741-9d9829932002",
            "3c475384-834c-47b0-ac71-a41a022e401c",
            "Exception - The vulnerability has not been released",
        ],
        [
            "user@gmail.com",
            "d6136f40-7609-4ac0-9741-9d9829932002",
            "3c475384-834c-47b0-ac71-a41a022e401c",
            "Access denied",
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker()],
            findings=[
                FindingFaker(
                    id="3c475384-834c-47b0-ac71-a41a022e401c",
                    unreliable_indicators=FindingUnreliableIndicators(
                        vulnerabilities_summary=FindingVulnerabilitiesSummary(
                            closed=0,
                        ),
                    ),
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    id="3d982277-a737-4046-ab7d-f3a2c4e6ecad",
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                    state=VulnerabilityStateFaker(
                        status=VulnerabilityStateStatus.SUBMITTED,
                    ),
                ),
                VulnerabilityFaker(
                    id="2ed00278-4481-4a44-8da4-04fe41751486",
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                    state=VulnerabilityStateFaker(
                        status=VulnerabilityStateStatus.SAFE,
                    ),
                ),
                VulnerabilityFaker(
                    id="d6136f40-7609-4ac0-9741-9d9829932002",
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                    state=VulnerabilityStateFaker(
                        status=VulnerabilityStateStatus.REJECTED,
                    ),
                ),
            ],
            stakeholders=[
                StakeholderFaker(email="admin@fluidattacks.com"),
                StakeholderFaker(email="hacker@fluidattacks.com"),
                StakeholderFaker(email="reattacker@fluidattacks.com"),
                StakeholderFaker(email="user@gmail.com"),
            ],
            group_access=[
                GroupAccessFaker(
                    email="admin@fluidattacks.com",
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
                GroupAccessFaker(
                    email="hacker@fluidattacks.com",
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
                GroupAccessFaker(
                    email="reattacker@fluidattacks.com",
                    state=GroupAccessStateFaker(has_access=True, role="reattacker"),
                ),
                GroupAccessFaker(
                    email="user@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                ),
            ],
        ),
    )
)
async def test_close_vulnerabilities_non_vulnerable(
    email: str,
    vulnerability_id: str,
    finding_id: str,
    error_message: str,
) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "can_report_vulnerabilities", "test-group"],
        ],
    )

    with raises(Exception, match=error_message):
        await mutate(
            _=None,
            info=info,
            finding_id=finding_id,
            vulnerabilities=[vulnerability_id],
            justification=VulnerabilityStateReason.VERIFIED_AS_SAFE,
        )
