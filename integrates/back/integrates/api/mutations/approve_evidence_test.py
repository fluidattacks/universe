from datetime import (
    datetime,
)

from integrates.api.mutations.approve_evidence import (
    mutate,
)
from integrates.api.mutations.payloads.types import SimplePayload
from integrates.custom_exceptions import EvidenceNotFound
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.dataloaders import get_new_context
from integrates.db_model.findings.types import FindingEvidence, FindingEvidences
from integrates.decorators import enforce_group_level_auth_async, require_login
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    FindingFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

# Allowed users
REVIEWER_EMAIL = "reviewer@fluidattacks.com"

# General parameters
GROUP_NAME = "group1"
FINDING_ID = "c265b055-5f30-444f-ad88-1fefab65d59d"


@parametrize(
    args=["email"],
    cases=[
        [REVIEWER_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email="admin@gmail.com"),
            ],
            findings=[
                FindingFaker(
                    id="c265b055-5f30-444f-ad88-1fefab65d59d",
                    group_name=GROUP_NAME,
                    evidences=FindingEvidences(
                        evidence1=FindingEvidence(
                            description="evidence1",
                            url=("group1-c265b055-5f30-444f-ad88-1fefab65d59d-evidence1"),
                            is_draft=True,
                            modified_date=datetime.fromisoformat("2020-11-19T13:37:10+00:00"),
                        )
                    ),
                )
            ],
            groups=[
                GroupFaker(name=GROUP_NAME),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=REVIEWER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="reviewer"),
                ),
            ],
        ),
    )
)
async def test_approve_evidence(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
        ],
    )
    loaders = get_new_context()
    finding_before = await loaders.finding.load(FINDING_ID)

    assert finding_before is not None
    assert finding_before.evidences.evidence1 is not None
    assert finding_before.evidences.evidence1.is_draft is True

    result: SimplePayload = await mutate(
        _parent=None,
        info=info,
        evidence_id="evidence_route_1",
        finding_id=FINDING_ID,
    )

    assert result == SimplePayload(success=True)

    loaders.finding.clear_all()
    finding_after = await loaders.finding.load(FINDING_ID)
    assert finding_after is not None
    assert finding_after.evidences.evidence1 is not None
    assert finding_after.evidences.evidence1.is_draft is False

    first_date = datetime_utils.get_as_str(finding_before.evidences.evidence1.modified_date)
    actual_date = datetime_utils.get_as_str(finding_after.evidences.evidence1.modified_date)
    assert first_date != actual_date


@parametrize(
    args=["email"],
    cases=[
        [REVIEWER_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email="admin@gmail.com"),
            ],
            findings=[
                FindingFaker(
                    id="c265b055-5f30-444f-ad88-1fefab65d59d",
                    group_name=GROUP_NAME,
                    evidences=FindingEvidences(
                        evidence1=FindingEvidence(
                            description="evidence1",
                            url=("group1-c265b055-5f30-444f-ad88-1fefab65d59d-evidence1"),
                            is_draft=True,
                            modified_date=datetime.fromisoformat("2020-11-19T13:37:10+00:00"),
                        ),
                        evidence2=FindingEvidence(
                            description="evidence2",
                            url=("group1-c265b055-5f30-444f-ad88-1fefab65d59d-evidence2"),
                            is_draft=False,
                            modified_date=datetime.fromisoformat("2020-11-19T13:37:10+00:00"),
                        ),
                    ),
                )
            ],
            groups=[
                GroupFaker(name=GROUP_NAME),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=REVIEWER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="reviewer"),
                ),
            ],
        ),
    )
)
async def test_approve_evidence_non_existent(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
        ],
    )
    loaders = get_new_context()
    finding = await loaders.finding.load(FINDING_ID)

    assert finding is not None
    assert finding.evidences.animation is None

    with raises(EvidenceNotFound):
        await mutate(
            _parent=None,
            info=info,
            evidence_id="animation",
            finding_id=FINDING_ID,
        )


@parametrize(
    args=["email"],
    cases=[
        [REVIEWER_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email="admin@gmail.com"),
            ],
            findings=[
                FindingFaker(
                    id="c265b055-5f30-444f-ad88-1fefab65d59d",
                    group_name=GROUP_NAME,
                    evidences=FindingEvidences(
                        evidence1=FindingEvidence(
                            description="evidence1",
                            url=("group1-c265b055-5f30-444f-ad88-1fefab65d59d-evidence1"),
                            is_draft=True,
                            modified_date=datetime.fromisoformat("2020-11-19T13:37:10+00:00"),
                        ),
                        evidence2=FindingEvidence(
                            description="evidence2",
                            url=("group1-c265b055-5f30-444f-ad88-1fefab65d59d-evidence2"),
                            is_draft=False,
                            modified_date=datetime.fromisoformat("2020-11-19T13:37:10+00:00"),
                        ),
                    ),
                )
            ],
            groups=[
                GroupFaker(name=GROUP_NAME),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=REVIEWER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="reviewer"),
                ),
            ],
        ),
    )
)
async def test_approve_evidence_already_approved(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
        ],
    )
    loaders = get_new_context()
    finding = await loaders.finding.load(FINDING_ID)

    assert finding is not None
    assert finding.evidences.evidence2 is not None
    assert finding.evidences.evidence2.is_draft is False

    with raises(EvidenceNotFound):
        await mutate(
            _parent=None,
            info=info,
            evidence_id="evidence_route_2",
            finding_id=FINDING_ID,
        )
