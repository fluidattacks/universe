from typing import (
    Any,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.billing import (
    domain as billing_domain,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_organization_level_auth_async,
    require_login,
)
from integrates.organizations import (
    utils as orgs_utils,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("updateCreditCardPaymentMethod")
@concurrent_decorators(
    require_login,
    enforce_organization_level_auth_async,
)
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    **kwargs: Any,
) -> SimplePayload:
    loaders: Dataloaders = info.context.loaders
    organization = await orgs_utils.get_organization(loaders, kwargs["organization_id"])
    success = await billing_domain.update_credit_card_payment_method(
        org=organization,
        payment_method_id=kwargs["payment_method_id"],
        card_expiration_month=kwargs.get("card_expiration_month"),
        card_expiration_year=kwargs.get("card_expiration_year"),
        make_default=kwargs["make_default"],
    )

    logs_utils.cloudwatch_log(
        info.context,
        "Credit card payment method updated"
        if success
        else "Attempt to update credit card payment method failed",
        extra={
            "organization_name": organization.name,
            "log_type": "Security",
        },
    )

    return SimplePayload(success=success)
