from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_login,
)
from integrates.groups import (
    domain as groups_domain,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("unsubscribeFromGroup")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_asm,
)
async def mutate(_: None, info: GraphQLResolveInfo, group_name: str) -> SimplePayload:
    stakeholder_info = await sessions_domain.get_jwt_content(info.context)
    stakeholder_email = stakeholder_info["user_email"]
    loaders: Dataloaders = info.context.loaders
    await groups_domain.unsubscribe_from_group(
        loaders=loaders,
        group_name=group_name,
        email=stakeholder_email,
    )
    logs_utils.cloudwatch_log(
        info.context,
        "Unsubscribed stakeholder in the group",
        extra={
            "group_name": group_name,
            "user_email": stakeholder_email,
            "log_type": "Security",
        },
    )

    return SimplePayload(success=True)
