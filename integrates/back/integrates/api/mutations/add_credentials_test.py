from typing import Any

from integrates.api.mutations.add_credentials import mutate
from integrates.custom_exceptions import (
    CustomBaseException,
    InvalidAWSRoleTrustPolicy,
    InvalidBase64SshKey,
    InvalidCredentialSecret,
    InvalidFieldLength,
    InvalidParameter,
    InvalidReportFilter,
    InvalidSpacesField,
)
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.enums import CredentialType
from integrates.decorators import enforce_organization_level_auth_async, require_login
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    OrganizationAccessFaker,
    OrganizationAccessStateFaker,
    OrganizationFaker,
    OrganizationStateFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

ORG_ID = "ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db"
ADMIN_EMAIL_TEST = "admin@fluidattacks.com"
ORG_MANAGER_EMAIL_TEST = "org_manager@fluidattacks.com"


@parametrize(
    args=["user_email", "credentials"],
    cases=[
        [
            ADMIN_EMAIL_TEST,
            {"name": "cred1", "type": "HTTPS", "token": "token test"},
        ],
        [
            ORG_MANAGER_EMAIL_TEST,
            {
                "name": "cred2",
                "type": "HTTPS",
                "user": "user test",
                "password": "lorem.ipsum,Dolor.w>oiu(p1",
            },
        ],
        [
            ORG_MANAGER_EMAIL_TEST,
            {
                "name": "cred3",
                "type": "SSH",
                "key": ("LS0tLS1CRUdJTiBPUEVOU1NIIFBSSVZBVEUgS0VZLS0tLS0KTUlJCg=="),
            },
        ],
        [
            ORG_MANAGER_EMAIL_TEST,
            {
                "name": "cred4",
                "type": "HTTPS",
                "azure_organization": "orgcred5",
                "is_pat": True,
                "token": "token test",
            },
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            stakeholders=[
                StakeholderFaker(email=ORG_MANAGER_EMAIL_TEST),
                StakeholderFaker(email=ADMIN_EMAIL_TEST),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=ORG_MANAGER_EMAIL_TEST,
                    state=OrganizationAccessStateFaker(
                        has_access=True, role="organization_manager"
                    ),
                ),
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=ADMIN_EMAIL_TEST,
                    state=OrganizationAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
        ),
    )
)
async def test_add_credentials(
    user_email: str,
    credentials: dict,
) -> None:
    # Arrange
    loaders: Dataloaders = get_new_context()

    info = GraphQLResolveInfoFaker(
        user_email=user_email,
        decorators=[
            [require_login],
            [enforce_organization_level_auth_async],
        ],
    )

    # Act
    await mutate(
        _=None,
        info=info,
        organization_id=ORG_ID,
        credentials=credentials,
    )

    # Assert
    org_credentials = await loaders.organization_credentials.load(ORG_ID)
    new_credentials = next(
        (
            credential
            for credential in org_credentials
            if credential.state.name == credentials["name"]
        ),
        None,
    )
    assert new_credentials is not None
    assert new_credentials.state.owner == user_email
    assert new_credentials.state.name == credentials["name"]
    assert new_credentials.state.type == CredentialType[credentials["type"]]
    assert getattr(new_credentials.secret, "token", None) == credentials.get("token")
    assert getattr(new_credentials.secret, "key", None) == credentials.get("key")
    assert getattr(new_credentials.secret, "user", None) == credentials.get("user")
    assert getattr(new_credentials.secret, "password", None) == credentials.get("password")
    assert getattr(new_credentials.state, "is_pat", False) == credentials.get("is_pat", False)
    assert getattr(new_credentials.state, "azure_organization", None) == credentials.get(
        "azure_organization",
    )


@parametrize(
    args=["credentials", "exception"],
    cases=[
        [
            {"name": "cred4", "type": "SSH", "key": "YWJ"},
            InvalidBase64SshKey(),
        ],
        [
            {"name": " ", "type": "SSH", "key": "YWJ"},
            InvalidSpacesField(),
        ],
        [
            {"name": "cred5", "type": "HTTPS", "token": " "},
            InvalidSpacesField(),
        ],
        [
            {
                "name": "cred5",
                "type": "HTTPS",
                "token": "token test",
                "is_pat": True,
            },
            InvalidParameter("azure_organization"),
        ],
        [
            {
                "name": "cred5",
                "type": "HTTPS",
                "token": "token test",
                "is_pat": False,
                "azure_organization": "testorg1",
            },
            InvalidParameter("azure_organization"),
        ],
        [
            {
                "name": "cred5",
                "type": "HTTPS",
                "token": "token test",
                "is_pat": True,
                "azure_organization": "   ",
            },
            InvalidSpacesField(),
        ],
        [
            {
                "name": "cred6",
                "type": "SSH",
                "key": ("LS0tLS1CRUdJTiBPUEVOU1NIIFBSSVZBVEUgS0VZLS0tLS0KTUlJCg=="),
                "is_pat": True,
                "azure_organization": "testorgcred6",
            },
            InvalidCredentialSecret(),
        ],
        [
            {
                "name": "cred6",
                "type": "HTTPS",
                "user": "user test",
                "password": ("lorem.ipsum,Dolor.sit:am3t;t]{3[s.T}/l;u=r<w>oiu(p"),
                "is_pat": True,
                "azure_organization": "testorgcred6",
            },
            InvalidCredentialSecret(),
        ],
        [
            {
                "name": "cred5",
                "type": "HTTPS",
                "user": " ",
                "password": "124",
            },
            InvalidSpacesField(),
        ],
        [
            {
                "name": "cred5",
                "type": "HTTPS",
                "user": "usertest",
                "password": "124",
            },
            InvalidReportFilter("Password should start with a letter"),
        ],
        [
            {
                "name": "cred5",
                "type": "HTTPS",
                "user": "usertest",
                "password": "ttr",
            },
            InvalidReportFilter("Password should include at least one number"),
        ],
        [
            {
                "name": "cred5",
                "type": "HTTPS",
                "user": "usertest",
                "password": "TT1L",
            },
            InvalidReportFilter("Password should include lowercase characters"),
        ],
        [
            {
                "name": "cred5",
                "type": "HTTPS",
                "user": "usertest",
                "password": "tt1l",
            },
            InvalidReportFilter("Password should include uppercase characters"),
        ],
        [
            {
                "name": "cred6",
                "type": "HTTPS",
                "user": "usertest",
                "password": "ttbd3Tl",
            },
            InvalidFieldLength(),
        ],
        [
            {
                "name": "cred7",
                "type": "HTTPS",
                "user": "usertest",
                "password": ("loremripsumlDolornsitlam3txconsectetrttbd3Tl"),
            },
            InvalidReportFilter("Password should include symbols characters"),
        ],
        [
            {
                "name": "cred8",
                "type": "AWSROLE",
                "arn": "arn:aws:iam::123456789012:role/MiRolEjemplo",
            },
            InvalidAWSRoleTrustPolicy(),
        ],
        [
            {
                "type": "HTTPS",
                "token": "token test",
            },
            InvalidParameter("name"),
        ],
        [
            {"name": "cred4", "key": "YWJ"},
            InvalidParameter("type"),
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id=ORG_ID, state=OrganizationStateFaker(aws_external_id="")),
            ],
            stakeholders=[
                StakeholderFaker(email=ORG_MANAGER_EMAIL_TEST),
                StakeholderFaker(email=ADMIN_EMAIL_TEST),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=ORG_MANAGER_EMAIL_TEST,
                    state=OrganizationAccessStateFaker(
                        has_access=True, role="organization_manager"
                    ),
                ),
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=ADMIN_EMAIL_TEST,
                    state=OrganizationAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
        ),
    )
)
async def test_add_credentials_fail(
    credentials: dict[str, Any],
    exception: CustomBaseException,
) -> None:
    # Arrange
    info = GraphQLResolveInfoFaker(
        user_email=ADMIN_EMAIL_TEST,
        decorators=[
            [require_login],
            [enforce_organization_level_auth_async],
        ],
    )

    # Act
    with raises(Exception, match=str(exception)):
        await mutate(
            _=None,
            info=info,
            organization_id=ORG_ID,
            credentials=credentials,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            stakeholders=[
                StakeholderFaker(email="user@gmail.com"),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email="user@gmail.com",
                    state=OrganizationAccessStateFaker(has_access=True, role="user"),
                ),
            ],
        ),
    )
)
async def test_add_credentials_fail_access() -> None:
    # Arrange
    info = GraphQLResolveInfoFaker(
        user_email="user@gmail.com",
        decorators=[
            [require_login],
            [enforce_organization_level_auth_async],
        ],
    )

    # Act
    with raises(Exception, match="Access denied"):
        await mutate(
            _=None,
            info=info,
            organization_id=ORG_ID,
            credentials={"name": "cred4", "type": "SSH", "key": "YWJ"},
        )
