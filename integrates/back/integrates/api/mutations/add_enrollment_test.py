from datetime import datetime, timedelta
from typing import Any

import integrates.decorators
from integrates.api.mutations.add_enrollment import (
    mutate,
)
from integrates.api.mutations.payloads.types import (
    SimplePayload,
)
from integrates.custom_exceptions import (
    InvalidField,
    OnlyCorporateEmails,
    OrganizationNotFound,
)
from integrates.custom_utils import datetime as datetime_utils
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.groups.enums import (
    GroupManaged,
    GroupStateStatus,
    GroupSubscriptionType,
    GroupTier,
)
from integrates.db_model.marketplace.enums import (
    AWSMarketplacePricingDimension,
    AWSMarketplaceSubscriptionStatus,
)
from integrates.db_model.marketplace.types import (
    AWSMarketplaceSubscriptionEntitlement,
)
from integrates.decorators import (
    require_corporate_email,
    require_login,
)
from integrates.testing.aws import (
    IntegratesAws,
    IntegratesDynamodb,
)
from integrates.testing.fakers import (
    EMAIL_GENERIC,
    AWSMarketplaceSubscriptionFaker,
    AWSMarketplaceSubscriptionStateFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    GroupStateFaker,
    OrganizationAccessFaker,
    OrganizationAccessStateFaker,
    OrganizationFaker,
    StakeholderFaker,
    TrialFaker,
    random_uuid,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import freeze_time, parametrize, raises

ORG_ID = random_uuid()

DEFAULT_DECORATORS: list[list[Any]] = [
    [require_login],
    [require_corporate_email],
]


@freeze_time("2022-10-21T15:58:31.280182+00:00")
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(
                    organization_id=ORG_ID,
                    state=GroupStateFaker(
                        has_essential=True,
                        has_advanced=False,
                        managed=GroupManaged.TRIAL,
                    ),
                )
            ],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL_GENERIC,
                    state=GroupAccessStateFaker(role="group_manager"),
                )
            ],
            organizations=[OrganizationFaker(id=ORG_ID)],
            organization_access=[
                OrganizationAccessFaker(
                    email=EMAIL_GENERIC,
                    organization_id=ORG_ID,
                    state=OrganizationAccessStateFaker(role="organization_manager"),
                )
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_GENERIC, is_registered=True),
            ],
        ),
    ),
    others=[Mock(integrates.decorators, "is_personal_email", "async", False)],
)
async def test_add_enrollment() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_GENERIC,
        decorators=DEFAULT_DECORATORS,
    )
    result: SimplePayload = await mutate(
        _=None,
        info=info,
    )
    assert result.success

    loaders = get_new_context()
    trial = await loaders.trial.load(EMAIL_GENERIC)
    stakeholder = await loaders.stakeholder.load(EMAIL_GENERIC)
    assert stakeholder
    assert stakeholder.enrolled is True
    assert trial
    assert trial.start_date
    assert (
        datetime_utils.get_as_utc_iso_format(trial.start_date) == "2022-10-21T15:58:31.280182+00:00"
    )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(
                    organization_id=ORG_ID,
                    state=GroupStateFaker(
                        has_essential=True,
                        has_advanced=False,
                        managed=GroupManaged.TRIAL,
                    ),
                )
            ],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL_GENERIC, state=GroupAccessStateFaker(role="group_manager")
                )
            ],
            organization_access=[
                OrganizationAccessFaker(
                    email=EMAIL_GENERIC,
                    organization_id=ORG_ID,
                    state=OrganizationAccessStateFaker(role="organization_manager"),
                )
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_GENERIC, is_registered=True),
            ],
        ),
    ),
    others=[Mock(integrates.decorators, "is_personal_email", "async", False)],
)
async def test_add_enrollment_invalid_org() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_GENERIC,
        decorators=DEFAULT_DECORATORS,
    )
    with raises(OrganizationNotFound):
        await mutate(
            _=None,
            info=info,
        )


@mocks(
    others=[Mock(integrates.decorators, "is_personal_email", "async", False)],
)
async def test_add_enrollment_invalid_email() -> None:
    info = GraphQLResolveInfoFaker(
        user_email="",
        decorators=DEFAULT_DECORATORS,
    )
    with raises(InvalidField):
        await mutate(
            _=None,
            info=info,
        )


@mocks(
    others=[Mock(integrates.decorators, "is_personal_email", "async", True)],
)
async def test_add_enrollment_invalid_corporate_email() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_GENERIC,
        decorators=DEFAULT_DECORATORS,
    )
    with raises(OnlyCorporateEmails):
        await mutate(
            _=None,
            info=info,
        )


@parametrize(
    args=["email", "has_trial"],
    cases=[
        ["awstrial@company.com", True],
        ["awsnotrial@company.com", False],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            aws_subscriptions=[
                AWSMarketplaceSubscriptionFaker(
                    aws_customer_id="xYwVpnKesFR",
                    state=AWSMarketplaceSubscriptionStateFaker(
                        entitlements=[
                            AWSMarketplaceSubscriptionEntitlement(
                                dimension=AWSMarketplacePricingDimension.GROUPS,
                                expiration_date=(datetime.now() + timedelta(days=365)),
                                value=2,
                            )
                        ],
                        has_free_trial=True,
                        modified_date=datetime.fromisoformat("2024-07-19T10:00:00+00:00"),
                        status=AWSMarketplaceSubscriptionStatus.ACTIVE,
                    ),
                    user="awstrial@company.com",
                ),
                AWSMarketplaceSubscriptionFaker(
                    aws_customer_id="wEMmJjXM3hU",
                    created_at=datetime.fromisoformat("2024-07-19T11:00:00+00:00"),
                    state=AWSMarketplaceSubscriptionStateFaker(
                        entitlements=[
                            AWSMarketplaceSubscriptionEntitlement(
                                dimension=AWSMarketplacePricingDimension.GROUPS,
                                expiration_date=(datetime.now() + timedelta(days=365)),
                                value=5,
                            )
                        ],
                        has_free_trial=False,
                        modified_date=datetime.fromisoformat("2024-07-19T11:00:00+00:00"),
                        status=AWSMarketplaceSubscriptionStatus.ACTIVE,
                    ),
                    user="awsnotrial@company.com",
                ),
            ],
            groups=[
                GroupFaker(
                    created_by="awstrial@company.com",
                    created_date=datetime.fromisoformat("2024-07-19T10:00:00+00:00"),
                    description="AWS Trial Group",
                    name="awstrial",
                    organization_id=ORG_ID,
                    state=GroupStateFaker(
                        has_essential=True,
                        has_advanced=False,
                        managed=GroupManaged.TRIAL,
                        modified_by="awstrial@company.com",
                        modified_date=datetime.fromisoformat("2024-07-19T10:00:00+00:00"),
                        status=GroupStateStatus.ACTIVE,
                        tier=GroupTier.FREE,
                        type=GroupSubscriptionType.CONTINUOUS,
                    ),
                ),
                GroupFaker(
                    created_by="awsnotrial@company.com",
                    created_date=datetime.fromisoformat("2024-07-19T11:00:00+00:00"),
                    description="AWS Trial Group",
                    name="awsnotrial",
                    organization_id=ORG_ID,
                    state=GroupStateFaker(
                        has_essential=True,
                        has_advanced=False,
                        managed=GroupManaged.MANAGED,
                        modified_by="awsnotrial@company.com",
                        modified_date=datetime.fromisoformat("2024-07-19T10:00:00+00:00"),
                        status=GroupStateStatus.ACTIVE,
                        tier=GroupTier.FREE,
                        type=GroupSubscriptionType.CONTINUOUS,
                    ),
                ),
            ],
            group_access=[
                GroupAccessFaker(
                    email="awstrial@company.com",
                    group_name="awstrial",
                    state=GroupAccessStateFaker(
                        modified_by="awstrial@company.com",
                        modified_date=datetime.fromisoformat("2024-07-19T10:00:00+00:00"),
                        has_access=True,
                        role="group_manager",
                    ),
                ),
                GroupAccessFaker(
                    email="awsnotrial@company.com",
                    group_name="awsnotrial",
                    state=GroupAccessStateFaker(
                        modified_by="awsnotrial@company.com",
                        modified_date=datetime.fromisoformat("2024-07-19T11:00:00+00:00"),
                        has_access=True,
                        role="group_manager",
                    ),
                ),
            ],
            organizations=[OrganizationFaker(id=ORG_ID)],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email="awstrial@company.com",
                    state=OrganizationAccessStateFaker(
                        modified_date=datetime.fromisoformat("2024-07-19T10:00:00+00:00"),
                        modified_by="awstrial@company.com",
                        has_access=True,
                        role="organization_manager",
                    ),
                ),
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email="awsnotrial@company.com",
                    state=OrganizationAccessStateFaker(
                        modified_date=datetime.fromisoformat("2024-07-19T11:00:00+00:00"),
                        modified_by="awsnotrial@company.com",
                        has_access=True,
                        role="organization_manager",
                    ),
                ),
            ],
            stakeholders=[
                StakeholderFaker(
                    aws_customer_id="xYwVpnKesFR",
                    enrolled=False,
                    email="awstrial@company.com",
                    first_name="AWS",
                    is_registered=True,
                    last_name="Trial",
                ),
                StakeholderFaker(
                    aws_customer_id="wEMmJjXM3hU",
                    enrolled=False,
                    email="awsnotrial@company.com",
                    first_name="AWS",
                    is_registered=True,
                    last_name="No Trial",
                ),
            ],
            trials=[
                TrialFaker(
                    email="company@company.com",
                    completed=False,
                    extension_date=None,
                    extension_days=0,
                    start_date=datetime.fromisoformat("2022-10-21T15:58:31.280182+00:00"),
                    reason=None,
                )
            ],
        ),
    ),
    others=[Mock(integrates.decorators, "is_personal_email", "async", False)],
)
async def test_add_enrollment_aws_subscription(email: str, has_trial: bool) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=DEFAULT_DECORATORS,
    )
    result: SimplePayload = await mutate(
        _=None,
        info=info,
    )
    assert result.success

    loaders = get_new_context()
    trial = await loaders.trial.load(email)
    assert has_trial == bool(trial)
