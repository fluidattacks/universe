from integrates.api.mutations.add_access_token import mutate
from integrates.api.mutations.payloads.types import UpdateAccessTokenPayload
from integrates.custom_exceptions import InvalidChar, InvalidExpirationTime, TokenCouldNotBeAdded
from integrates.custom_utils import datetime as datetime_utils
from integrates.custom_utils.stakeholders import get_stakeholder
from integrates.dataloaders import get_new_context
from integrates.decorators import require_login
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    EMAIL_FLUIDATTACKS_ADMIN,
    EMAIL_FLUIDATTACKS_CUSTOMER_MANAGER,
    EMAIL_FLUIDATTACKS_GROUP_MANAGER,
    EMAIL_FLUIDATTACKS_HACKER,
    EMAIL_FLUIDATTACKS_REATTACKER,
    EMAIL_FLUIDATTACKS_RESOURCER,
    EMAIL_FLUIDATTACKS_SERVICE_FORCES,
    EMAIL_FLUIDATTACKS_USER,
    EMAIL_FLUIDATTACKS_VULNERABILITY_MANAGER,
    AccessTokensFaker,
    GraphQLResolveInfoFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises


@parametrize(
    args=["email"],
    cases=[
        [EMAIL_FLUIDATTACKS_ADMIN],
        [EMAIL_FLUIDATTACKS_HACKER],
        [EMAIL_FLUIDATTACKS_REATTACKER],
        [EMAIL_FLUIDATTACKS_USER],
        [EMAIL_FLUIDATTACKS_GROUP_MANAGER],
        [EMAIL_FLUIDATTACKS_VULNERABILITY_MANAGER],
        [EMAIL_FLUIDATTACKS_RESOURCER],
        [EMAIL_FLUIDATTACKS_CUSTOMER_MANAGER],
        [EMAIL_FLUIDATTACKS_SERVICE_FORCES],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_ADMIN, access_tokens=[]),
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_HACKER, access_tokens=[]),
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_REATTACKER, access_tokens=[]),
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_USER, access_tokens=[]),
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_GROUP_MANAGER, access_tokens=[]),
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_VULNERABILITY_MANAGER, access_tokens=[]),
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_RESOURCER, access_tokens=[]),
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_CUSTOMER_MANAGER, access_tokens=[]),
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_SERVICE_FORCES, access_tokens=[]),
            ],
        ),
    )
)
async def test_add_access_token(email: str) -> None:
    expiration_time = datetime_utils.get_plus_delta(datetime_utils.get_utc_now(), weeks=8)
    ts_expiration_time = int(expiration_time.timestamp())
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[[require_login]],
    )
    result: UpdateAccessTokenPayload = await mutate(
        _=None,
        info=info,
        expiration_time=ts_expiration_time,
        name="FirstToken",
    )
    assert result.success
    assert result.session_jwt

    loaders = get_new_context()
    stakeholder = await get_stakeholder(loaders, email)
    assert len(stakeholder.access_tokens) == 1
    assert stakeholder.access_tokens[0].last_use is None
    assert stakeholder.access_tokens[0].name == "FirstToken"

    result = await mutate(
        _=None,
        info=info,
        expiration_time=ts_expiration_time,
        name="SecondToken",
    )
    assert result.success
    assert result.session_jwt

    loaders = get_new_context()
    stakeholder = await get_stakeholder(loaders, email)
    assert len(stakeholder.access_tokens) == 2
    assert stakeholder.access_tokens[1].last_use is None
    assert stakeholder.access_tokens[1].name == "SecondToken"


async def test_add_access_token_invalid_time() -> None:
    expiration_time = datetime_utils.get_plus_delta(datetime_utils.get_utc_now(), weeks=30)
    ts_expiration_time = int(expiration_time.timestamp())
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_FLUIDATTACKS_ADMIN,
        decorators=[[require_login]],
    )
    with raises(InvalidExpirationTime):
        await mutate(
            _=None,
            info=info,
            expiration_time=ts_expiration_time,
            name="TokenInvalidTime",
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_ADMIN, access_tokens=[]),
            ],
        ),
    )
)
async def test_add_access_token_invalid_name() -> None:
    expiration_time = datetime_utils.get_plus_delta(datetime_utils.get_utc_now(), weeks=8)
    ts_expiration_time = int(expiration_time.timestamp())
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_FLUIDATTACKS_ADMIN,
        decorators=[[require_login]],
    )
    with raises(InvalidChar):
        await mutate(
            _=None,
            info=info,
            expiration_time=ts_expiration_time,
            name="= Invalid",
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(
                    email=EMAIL_FLUIDATTACKS_ADMIN,
                    access_tokens=[
                        AccessTokensFaker(name="FirstToken"),
                        AccessTokensFaker(name="SecondToken"),
                    ],
                ),
            ],
        ),
    )
)
async def test_add_access_token_invalid_third() -> None:
    expiration_time = datetime_utils.get_plus_delta(datetime_utils.get_utc_now(), weeks=8)
    ts_expiration_time = int(expiration_time.timestamp())
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_FLUIDATTACKS_ADMIN,
        decorators=[[require_login]],
    )
    with raises(TokenCouldNotBeAdded):
        await mutate(
            _=None,
            info=info,
            expiration_time=ts_expiration_time,
            name="ThirdToken",
        )
