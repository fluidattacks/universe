from contextlib import suppress
from datetime import datetime
from typing import Any

from aioextensions import collect
from graphql.type.definition import GraphQLResolveInfo

from integrates.custom_exceptions import RootAlreadyCloning
from integrates.custom_utils import logs as logs_utils
from integrates.custom_utils import vulnerabilities as vulns_utils
from integrates.custom_utils.groups import get_group
from integrates.dataloaders import Dataloaders
from integrates.db_model.vulnerabilities.enums import VulnerabilityStateReason
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_continuous,
    require_is_not_under_review,
    require_login,
)
from integrates.roots import domain as roots_domain
from integrates.roots import update as roots_update
from integrates.sessions import domain as sessions_domain
from integrates.unreliable_indicators.enums import EntityDependency
from integrates.unreliable_indicators.operations import update_unreliable_indicators_by_deps

from .payloads.types import SimplePayload
from .schema import MUTATION


@MUTATION.field("updateGitRoot")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_continuous,
)
async def mutate(_parent: None, info: GraphQLResolveInfo, **kwargs: Any) -> SimplePayload:
    loaders: Dataloaders = info.context.loaders
    root_id: str = kwargs["id"]
    group_name: str = str(kwargs.get("group_name")).lower()
    user_info = await sessions_domain.get_jwt_content(info.context)
    user_email = user_info["user_email"]
    group = await get_group(loaders, group_name)
    root = await roots_update.update_git_root(loaders, user_email, group, **kwargs)
    if exclusions := kwargs.get("gitignore"):
        vulns = await roots_domain.comment_vulnerabilities_by_exclusions(
            loaders=loaders,
            root_id=root_id,
            email=user_email,
            date=datetime.now(),
            exclusions=exclusions,
        )
        await collect(
            [
                vulns_utils.close_vulnerability(
                    loaders=loaders,
                    modified_by=user_email,
                    vulnerability=vuln,
                    closing_reason=VulnerabilityStateReason.EXCLUSION,
                )
                for vuln in vulns
            ],
        )

    if sync_roots := await roots_domain.roots_sync_needed(
        loaders=loaders, root_id=root_id, **kwargs
    ):
        with suppress(RootAlreadyCloning):
            await roots_domain.clone_root_and_update_group_languages(
                loaders=loaders,
                group=group,
                modified_by=user_email,
                root=root,
            )

    if kwargs.get("criticality"):
        findings = await roots_domain.get_root_open_findings(
            loaders=loaders,
            root_id=root_id,
        )
        vulnerabilities = await roots_domain.get_root_open_vulnerabilities(
            loaders=loaders,
            root_id=root_id,
        )
        await update_unreliable_indicators_by_deps(
            EntityDependency.update_git_root,
            finding_ids=[finding.id for finding in findings],
            vulnerability_ids=[vuln.id for vuln in vulnerabilities],
        )

    logs_utils.cloudwatch_log(
        info.context,
        "Updated Git root",
        extra={
            "group_name": group_name,
            "root_id": root_id,
            "root_nickname": root.state.nickname,
            "sync_roots": sync_roots,
            "log_type": "Security",
        },
    )

    return SimplePayload(success=True)
