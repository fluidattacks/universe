from datetime import (
    datetime,
)
from typing import (
    TypedDict,
    Unpack,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.api.types import (
    APP_EXCEPTIONS,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.db_model.roots.types import (
    Secret,
    SecretState,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_login,
)
from integrates.roots import (
    domain as roots_domain,
)
from integrates.sessions import (
    domain as sessions_domain,
)
from integrates.verify.enums import (
    ResourceType,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


class AddSecretArgs(TypedDict):
    description: str | None
    group_name: str
    key: str
    resource_id: str
    resource_type: ResourceType
    value: str


@MUTATION.field("addSecret")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_is_not_under_review,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    **kwargs: Unpack[AddSecretArgs],
) -> SimplePayload:
    user_info = await sessions_domain.get_jwt_content(info.context)
    key = kwargs["key"].strip()
    value = kwargs["value"].strip()
    email = user_info["user_email"].strip()
    description = kwargs.get("description")

    try:
        await roots_domain.add_secret(
            secret=Secret(
                key=key,
                value=value,
                created_at=datetime.now(),
                state=SecretState(
                    owner=email,
                    description=description,
                    modified_by=email,
                    modified_date=datetime.now(),
                ),
            ),
            resource_id=kwargs["resource_id"],
            resource_type=kwargs["resource_type"],
            group_name=kwargs["group_name"],
            loaders=info.context.loaders,
        )
        logs_utils.cloudwatch_log(
            info.context,
            "Added secret",
            extra={
                "group_name": kwargs["group_name"],
                "resource_type": kwargs["resource_type"],
                "resource_id": kwargs["resource_id"],
                "log_type": "Security",
            },
        )
    except APP_EXCEPTIONS:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to add a secret",
            extra={
                "group_name": kwargs["group_name"],
                "resource_type": kwargs["resource_type"],
                "resource_id": kwargs["resource_id"],
                "log_type": "Security",
            },
        )
        raise

    return SimplePayload(success=True)
