from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.custom_utils.groups import get_group
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.hook.enums import (
    HookEvent,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_login,
    require_service_white,
)
from integrates.groups import (
    hook_notifier,
)
from integrates.groups.domain import (
    remove_file,
)
from integrates.roots.domain import (
    get_environment_url,
    remove_environment_url_id,
    remove_related_resources,
    removed_related_events,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("removeEnvironmentUrl")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_service_white,
)
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    group_name: str,
    root_id: str,
    url_id: str,
    **_kwargs: None,
) -> SimplePayload:
    loaders: Dataloaders = info.context.loaders
    user_info = await sessions_domain.get_jwt_content(info.context)
    user_email = user_info["user_email"]
    root_env = await remove_environment_url_id(
        loaders=loaders,
        root_id=root_id,
        url_id=url_id,
        user_email=user_email,
        group_name=group_name,
    )
    logs_utils.cloudwatch_log(
        info.context,
        "Removed git envs of the root",
        extra={
            "url_id": url_id,
            "root_id": root_id,
            "log_type": "Security",
        },
    )
    await hook_notifier.process_hook_event(
        loaders=loaders,
        group_name=group_name,
        info={
            "url_id": url_id,
            "root_id": root_id,
        },
        event=HookEvent.ENVIRONMENT_REMOVED,
    )

    await removed_related_events(loaders, group_name, root_env)

    await remove_related_resources(loaders, info, user_email, root_env)

    env_url = await get_environment_url(loaders, root_id, group_name, url_id)
    if env_url.url.endswith(".apk"):
        group = await get_group(loaders=loaders, group_name=group_name)
        files = group.files
        if files:
            apk_file_exists = any(env_url.url == file.file_name for file in files)
            if apk_file_exists:
                await remove_file(
                    loaders=loaders,
                    email=user_email,
                    file_name=env_url.url,
                    group_name=group_name,
                )

    return SimplePayload(success=True)
