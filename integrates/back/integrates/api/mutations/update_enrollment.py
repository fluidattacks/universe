from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.db_model.trials.enums import (
    TrialReason,
)
from integrates.db_model.trials.types import (
    TrialMetadataToUpdate,
)
from integrates.decorators import (
    concurrent_decorators,
    require_corporate_email,
    require_login,
)
from integrates.sessions import (
    domain as sessions_domain,
)
from integrates.trials import (
    domain as trials_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("updateEnrollment")
@concurrent_decorators(
    require_corporate_email,
    require_login,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    reason: TrialReason,
) -> SimplePayload:
    user_data = await sessions_domain.get_jwt_content(info.context)
    user_email = user_data["user_email"]

    await trials_domain.update_metadata(
        email=user_email,
        metadata=TrialMetadataToUpdate(reason=reason),
    )
    logs_utils.cloudwatch_log(
        info.context,
        "Update enrolled user",
        extra={
            "user_email": user_email,
            "reason": reason,
            "log_type": "Security",
        },
    )

    return SimplePayload(success=True)
