from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_exceptions import (
    MailerClientError,
    UnableToSendMail,
)
from integrates.custom_utils.stakeholders import (
    get_full_name,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_login,
    retry_on_exceptions,
)
from integrates.mailer.trial import (
    new_enrolled_user_mail,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)

send_new_enrolled_user_mail = retry_on_exceptions(
    exceptions=(UnableToSendMail, MailerClientError),
    max_attempts=4,
    sleep_seconds=2,
)(new_enrolled_user_mail)


@MUTATION.field("sendNewEnrolledUser")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_asm,
)
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    group_name: str,
    **_kwargs: None,
) -> SimplePayload:
    user_info: dict[str, str] = await sessions_domain.get_jwt_content(info.context)
    user_email: str = user_info["user_email"]
    full_name: str = get_full_name(user_info)
    loaders: Dataloaders = info.context.loaders

    await send_new_enrolled_user_mail(
        loaders=loaders,
        user_email=user_email,
        full_name=full_name,
        group_name=group_name,
    )

    return SimplePayload(success=True)
