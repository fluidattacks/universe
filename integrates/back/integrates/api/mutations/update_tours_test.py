from integrates.api.mutations.update_tours import mutate
from integrates.decorators import require_login
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import GraphQLResolveInfoFaker, StakeholderFaker
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

ADMIN_EMAIL = "admin@gmail.com"
USER_EMAIL = "user@gmail.com"
VULNERABILITY_MANAGER_EMAIL = "vulnerability_manager@fluidattacks.com"
HACKER_EMAIL = "hacker@fluidattacks.com"


@parametrize(
    args=["email"],
    cases=[
        [ADMIN_EMAIL],
        [USER_EMAIL],
        [VULNERABILITY_MANAGER_EMAIL],
        [HACKER_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=ADMIN_EMAIL),
                StakeholderFaker(email=USER_EMAIL),
                StakeholderFaker(email=VULNERABILITY_MANAGER_EMAIL),
                StakeholderFaker(email=HACKER_EMAIL),
            ],
        )
    )
)
async def test_update_tours_success(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
        ],
    )
    tours = {"new_group": True, "new_root": False, "welcome": True}
    result = await mutate(
        _=None,
        info=info,
        tours=tours,
    )
    assert result.success


@mocks(aws=IntegratesAws(dynamodb=IntegratesDynamodb()))
async def test_update_tours_nonexistent_user() -> None:
    info = GraphQLResolveInfoFaker(
        user_email="nonexistent@email.com",
        decorators=[
            [require_login],
        ],
    )
    tours = {"new_group": True}
    with raises(Exception):
        await mutate(
            _=None,
            info=info,
            tours=tours,
        )
