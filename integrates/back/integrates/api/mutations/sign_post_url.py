import logging
import logging.config
from typing import (
    Any,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_exceptions import (
    ErrorUploadingFileS3,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_is_not_under_review,
    require_login,
)
from integrates.resources import (
    domain as resources_domain,
)

from .payloads.types import (
    SignPostUrlsPayload,
)
from .schema import (
    MUTATION,
)

LOGGER = logging.getLogger(__name__)


@MUTATION.field("signPostUrl")
@concurrent_decorators(
    require_login,
    require_is_not_under_review,
    enforce_group_level_auth_async,
    require_asm,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    group_name: str,
    **parameters: Any,
) -> SignPostUrlsPayload:
    files_data = parameters["files_data_input"]

    signed_url = await resources_domain.upload_file(
        file_info=files_data[0]["file_name"],
        group_name=group_name,
    )

    if signed_url:
        logs_utils.cloudwatch_log(
            info.context,
            "Uploaded file in the group",
            extra={
                "file_name": files_data[0]["file_name"],
                "user_date": files_data[0]["upload_date"],
                "group_name": group_name,
                "log_type": "Security",
            },
        )
    else:
        LOGGER.error("Couldn't generate signed URL", extra={"extra": parameters})
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to add resource files in the group",
            extra={
                "file_name": files_data[0]["file_name"],
                "group_name": group_name,
                "log_type": "Security",
            },
        )
        raise ErrorUploadingFileS3()

    return SignPostUrlsPayload(
        success=True,
        url={
            **signed_url,
            "fields": {
                **signed_url["fields"],
                "credential": signed_url["fields"]["x-amz-credential"],
                "signature": signed_url["fields"]["x-amz-signature"],
                "algorithm": signed_url["fields"]["x-amz-algorithm"],
                "date": signed_url["fields"]["x-amz-date"],
                "securitytoken": signed_url["fields"]["x-amz-security-token"],
            },
        },
    )
