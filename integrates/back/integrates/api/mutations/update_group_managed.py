from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.groups.enums import (
    GroupManaged,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_login,
    turn_args_into_kwargs,
)
from integrates.groups import (
    domain as groups_domain,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("updateGroupManaged")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_asm,
)
@turn_args_into_kwargs
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    comments: str,
    group_name: str,
    managed: GroupManaged,
) -> SimplePayload:
    loaders: Dataloaders = info.context.loaders
    managed = GroupManaged(managed)
    group_name = group_name.lower()
    user_info = await sessions_domain.get_jwt_content(info.context)
    email = user_info["user_email"]

    await groups_domain.update_group_managed(
        loaders=loaders,
        comments=comments,
        email=email,
        group_name=group_name,
        managed=managed,
    )
    logs_utils.cloudwatch_log(
        info.context,
        "Updated managed in the group",
        extra={"group_name": group_name, "log_type": "Security"},
    )

    return SimplePayload(success=True)
