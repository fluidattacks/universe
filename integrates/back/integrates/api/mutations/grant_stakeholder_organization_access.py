import logging
import logging.config
from contextlib import (
    suppress,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates import (
    authz,
)
from integrates.custom_exceptions import (
    InvalidRoleProvided,
    StakeholderHasOrganizationAccess,
    StakeholderNotFound,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.custom_utils.access import format_invitation_state
from integrates.custom_utils.stakeholders import (
    get_stakeholder,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.organization_access.enums import (
    OrganizationInvitiationState,
)
from integrates.db_model.organization_access.types import (
    OrganizationAccessRequest,
)
from integrates.db_model.stakeholders.types import (
    Stakeholder,
)
from integrates.decorators import (
    enforce_organization_level_auth_async,
)
from integrates.group_access.domain import (
    validate_new_invitation_time_limit,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.organizations import (
    utils as orgs_utils,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    GrantStakeholderAccessPayload,
)
from .schema import (
    MUTATION,
)

# Constants
LOGGER = logging.getLogger(__name__)


@MUTATION.field("grantStakeholderOrganizationAccess")
@enforce_organization_level_auth_async
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    **parameters: dict,
) -> GrantStakeholderAccessPayload:
    loaders: Dataloaders = info.context.loaders
    organization_id = str(parameters.get("organization_id"))
    organization = await orgs_utils.get_organization(loaders, organization_id)
    organization_name = organization.name
    requester_data = await sessions_domain.get_jwt_content(info.context)
    requester_email = requester_data["user_email"]

    stakeholder_email = str(parameters.get("user_email")).lower().strip()
    stakeholder_role: str = str(parameters.get("role")).lower()

    if organization_access := await loaders.organization_access.load(
        OrganizationAccessRequest(organization_id=organization_id, email=stakeholder_email),
    ):
        if organization_access.state.has_access:
            raise StakeholderHasOrganizationAccess()

        with suppress(StakeholderNotFound):
            stakeholder: Stakeholder = await get_stakeholder(loaders, stakeholder_email)
            invitation_state = format_invitation_state(
                invitation=organization_access.state.invitation,
                is_registered=stakeholder.is_registered,
            )
            if invitation_state == OrganizationInvitiationState.REGISTERED:
                raise StakeholderHasOrganizationAccess()

        # Too soon to send another email invitation to the same stakeholder
        if organization_access.expiration_time:
            validate_new_invitation_time_limit(organization_access.expiration_time)

    enforcer = await authz.get_organization_level_enforcer(loaders, requester_email)
    if enforcer(organization_id, f"grant_organization_level_role:{stakeholder_role}"):
        await orgs_domain.invite_to_organization(
            loaders=loaders,
            email=stakeholder_email,
            role=stakeholder_role,
            organization_name=organization_name,
            modified_by=requester_email,
        )
    else:
        LOGGER.error(
            "Invalid role provided",
            extra={
                "extra": {
                    "new_stakeholder_role": stakeholder_role,
                    "organization_name": organization_name,
                    "requester_email": stakeholder_email,
                },
            },
        )
        raise InvalidRoleProvided(role=stakeholder_role)

    logs_utils.cloudwatch_log(
        info.context,
        "Granted access in the organization",
        extra={
            "stakeholder_email": stakeholder_email,
            "organization_name": organization_name,
            "stakeholder_role": stakeholder_role,
            "requester_email": requester_email,
            "log_type": "Security",
        },
    )

    return GrantStakeholderAccessPayload(
        success=True,
        granted_stakeholder=Stakeholder(
            email=stakeholder_email,
        ),
    )
