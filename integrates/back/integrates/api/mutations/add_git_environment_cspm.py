from typing import (
    TypedDict,
    Unpack,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.roots.enums import (
    RootEnvironmentUrlStateStatus,
    RootEnvironmentUrlType,
)
from integrates.db_model.roots.types import (
    RootEnvironmentUrlsRequest,
    RootRequest,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_login,
    require_service_white,
)
from integrates.jobs_orchestration.create_machine_config import (
    MachineJobOrigin,
    MachineJobTechnique,
    MachineModulesToExecute,
)
from integrates.jobs_orchestration.jobs import (
    queue_machine_job,
)
from integrates.roots import (
    environments as roots_environments,
)
from integrates.sessions.domain import (
    get_jwt_content,
)

from .payloads.types import (
    AddEnvironmentUrlPayload,
)
from .schema import (
    MUTATION,
)


class GitEnvironmentCspmArgs(TypedDict):
    azure_client_id: str | None
    azure_client_secret: str | None
    azure_tenant_id: str | None
    azure_subscription_id: str | None
    cloud_name: str
    gcp_private_key: str | None
    group_name: str
    root_id: str
    url: str
    use_egress: bool | None
    use_vpn: bool | None
    use_ztna: bool | None


@MUTATION.field("addGitEnvironmentCspm")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_service_white,
    require_is_not_under_review,
)
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    **kwargs: Unpack[GitEnvironmentCspmArgs],
) -> AddEnvironmentUrlPayload:
    loaders: Dataloaders = info.context.loaders
    user_info = await get_jwt_content(info.context)
    group_name = kwargs["group_name"]
    user_email = user_info["user_email"]
    url = kwargs["url"].strip()
    root_id = kwargs["root_id"]
    use_connection = {
        "use_egress": kwargs.get("use_egress"),
        "use_vpn": kwargs.get("use_vpn"),
        "use_ztna": kwargs.get("use_ztna"),
    }
    if any(use_connection.values()):
        raise ValueError("The CSPM environment does not support Egress, VPN, or ZTNA.")
    secrets = {
        "AZURE_CLIENT_ID": kwargs.get("azure_client_id"),
        "AZURE_CLIENT_SECRET": kwargs.get("azure_client_secret"),
        "AZURE_TENANT_ID": kwargs.get("azure_tenant_id"),
        "AZURE_SUBSCRIPTION_ID": kwargs.get("azure_subscription_id"),
        "GCP_PRIVATE_KEY": kwargs.get("gcp_private_key"),
    }
    success = await roots_environments.add_root_environment_cspm(
        loaders=loaders,
        group_name=group_name,
        root_id=root_id,
        url=url,
        user_email=user_email,
        cloud_type=kwargs["cloud_name"],
        secrets=secrets,
        use_connection=use_connection,
        should_notify=True,
    )
    logs_utils.cloudwatch_log(
        info.context,
        "Added CSPM environment and its secrets for the root",
        extra={
            "group_name": group_name,
            "root_id": root_id,
            "log_type": "Security",
        },
    )

    root_env_urls = await get_new_context().root_environment_urls.load(
        RootEnvironmentUrlsRequest(root_id=root_id, group_name=group_name),
    )
    url_ids = [
        env_url.id
        for env_url in root_env_urls
        if env_url.url == url
        and env_url.state.status != RootEnvironmentUrlStateStatus.DELETED
        and env_url.state.url_type == RootEnvironmentUrlType.CSPM
    ]

    if (root := await loaders.root.load(RootRequest(group_name, root_id))) and (
        root_nickname := root.state.nickname
    ):
        await queue_machine_job(
            loaders=loaders,
            group_name=group_name,
            modified_by=user_email,
            root_nicknames={root_nickname},
            modules_to_execute=MachineModulesToExecute(
                APK=False,
                CSPM=True,
                DAST=False,
                SAST=False,
                SCA=False,
            ),
            job_origin=MachineJobOrigin.PLATFORM,
            job_technique=MachineJobTechnique.CSPM,
        )

    return AddEnvironmentUrlPayload(success=success, url_id=url_ids[-1] if url_ids else "")
