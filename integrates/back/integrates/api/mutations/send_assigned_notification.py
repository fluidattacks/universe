from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.api.types import (
    APP_EXCEPTIONS,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.custom_utils.findings import get_finding
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.hook.enums import (
    HookEvent,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_is_not_under_review,
    require_login,
)
from integrates.groups import (
    hook_notifier,
)
from integrates.sessions import (
    domain as sessions_domain,
)
from integrates.vulnerabilities.domain.treatment import (
    validate_and_send_notification_request,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("sendAssignedNotification")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_asm,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    finding_id: str,
    vulnerabilities: list[str],
    **_kwargs: None,
) -> SimplePayload:
    user_info: dict[str, str] = await sessions_domain.get_jwt_content(info.context)
    responsible: str = user_info["user_email"]
    loaders: Dataloaders = info.context.loaders
    try:
        finding = await get_finding(loaders, finding_id)
        await validate_and_send_notification_request(
            loaders=loaders,
            finding=finding,
            responsible=responsible,
            vulnerabilities=vulnerabilities,
        )
        await hook_notifier.process_hook_event(
            loaders=info.context.loaders,
            group_name=finding.group_name,
            info={
                "vulnerabilities": vulnerabilities,
                "finding_id": finding_id,
                "responsible": responsible,
            },
            event=HookEvent.VULNERABILITY_ASSIGNED,
        )
        logs_utils.cloudwatch_log(
            info.context,
            "Notifications pertaining to a change in treatment of vulns have been sent",
            extra={
                "finding_id": finding_id,
                "vuln_ids": vulnerabilities,
                "log_type": "Security",
            },
        )
    except APP_EXCEPTIONS:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to send notifications pertaining to a change in treatment of vulns",
            extra={
                "finding_id": finding_id,
                "vuln_ids": vulnerabilities,
                "log_type": "Security",
            },
        )

        raise

    return SimplePayload(success=True)
