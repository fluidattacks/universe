from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_organization_level_auth_async,
    require_login,
)
from integrates.mailmap import (
    set_mailmap_subentry_as_mailmap_entry,
)

from .payloads.types import (
    MailmapEntryPayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("setMailmapSubentryAsMailmapEntry")
@concurrent_decorators(
    require_login,
    enforce_organization_level_auth_async,
)
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    entry_email: str,
    subentry_email: str,
    subentry_name: str,
    organization_id: str,
) -> MailmapEntryPayload:
    entry = await set_mailmap_subentry_as_mailmap_entry(
        entry_email=entry_email,
        subentry_email=subentry_email,
        subentry_name=subentry_name,
        organization_id=organization_id,
    )
    entry_payload = MailmapEntryPayload(
        success=True,
        entry=entry,
    )

    logs_utils.cloudwatch_log(
        info.context,
        "Set mailmap alias as author",
        extra={
            "entry_email": entry_email,
            "subentry_email": subentry_email,
            "subentry_name": subentry_name,
            "organization_id": organization_id,
        },
    )

    return entry_payload
