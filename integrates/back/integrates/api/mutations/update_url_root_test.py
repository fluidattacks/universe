from integrates.api.mutations.update_url_root import mutate
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute,
    require_attribute_internal,
    require_is_not_under_review,
    require_login,
    require_service_black,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    StakeholderFaker,
    UrlRootFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

ADMIN_EMAIL = "admin@gmail.com"
USER_EMAIL = "user@gmail.com"
VULNERABILITY_MANAGER_EMAIL = "vulnerability_manager@fluidattacks.com"
HACKER_EMAIL = "hacker@fluidattacks.com"
GROUP_NAME = "test-group"
CUSTOMER_MANAGER_EMAIL = "customer_manager@fluidattacks.com"
REATTACKER_EMAIL = "reattacker@fluidattacks.com"
ROOT_ID = "test-root-123"


@parametrize(
    args=["email"],
    cases=[
        [ADMIN_EMAIL],
        [CUSTOMER_MANAGER_EMAIL],
        [VULNERABILITY_MANAGER_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=ADMIN_EMAIL),
                StakeholderFaker(email=CUSTOMER_MANAGER_EMAIL),
                StakeholderFaker(email=VULNERABILITY_MANAGER_EMAIL),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=CUSTOMER_MANAGER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="customer_manager"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=VULNERABILITY_MANAGER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="vulnerability_manager"),
                ),
            ],
            roots=[
                UrlRootFaker(
                    root_id=ROOT_ID,
                    group_name=GROUP_NAME,
                ),
            ],
        )
    )
)
async def test_update_url_root_success(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_service_black],
            [require_attribute, "has_service_black", GROUP_NAME],
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )
    result = await mutate(
        _parent=None,
        info=info,
        group_name=GROUP_NAME,
        root_id=ROOT_ID,
        nickname="new-nickname",
        excluded_sub_paths=["/excluded1", "/excluded2"],
    )
    assert result.success


@parametrize(
    args=["email"],
    cases=[
        [HACKER_EMAIL],
        [REATTACKER_EMAIL],
        [USER_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=HACKER_EMAIL),
                StakeholderFaker(email=REATTACKER_EMAIL),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=HACKER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=REATTACKER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="reattacker"),
                ),
            ],
            roots=[
                UrlRootFaker(
                    root_id=ROOT_ID,
                    group_name=GROUP_NAME,
                ),
            ],
        )
    )
)
async def test_update_url_root_unauthorized(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_service_black],
            [require_attribute, "has_service_black", GROUP_NAME],
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )
    with raises(Exception, match="Access denied"):
        await mutate(
            _parent=None,
            info=info,
            group_name=GROUP_NAME,
            root_id=ROOT_ID,
            nickname="new-nickname",
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=ADMIN_EMAIL),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
            roots=[
                UrlRootFaker(
                    root_id=ROOT_ID,
                    group_name=GROUP_NAME,
                ),
            ],
        )
    )
)
async def test_update_url_root_partial_update() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=ADMIN_EMAIL,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_service_black],
            [require_attribute, "has_service_black", GROUP_NAME],
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )
    result = await mutate(
        _parent=None,
        info=info,
        group_name=GROUP_NAME,
        root_id=ROOT_ID,
        nickname="new-nickname",
    )
    assert result.success
