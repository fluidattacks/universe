from aioextensions import schedule
from graphql.type.definition import GraphQLResolveInfo

from integrates.batch import dal as batch_dal
from integrates.batch.enums import Action, IntegratesBatchQueue
from integrates.batch_dispatch.clone_roots import solve_root_events
from integrates.custom_utils import roots as roots_utils
from integrates.custom_utils.filter_vulnerabilities import filter_non_deleted
from integrates.dataloaders import Dataloaders
from integrates.db_model.events.enums import EventSolutionReason
from integrates.db_model.roots.enums import RootStateReason, RootStatus
from integrates.db_model.roots.types import GitRoot, Root
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    rename_kwargs,
    require_is_not_under_review,
    require_login,
    require_service_black,
    require_service_white,
)
from integrates.roots import domain as roots_domain
from integrates.roots import notifications as roots_notifications
from integrates.sessions import domain as sessions_domain

from .payloads.types import SimplePayload
from .schema import MUTATION


async def _deactivate_root(
    *,
    info: GraphQLResolveInfo,
    root: Root,
    email: str,
    group_name: str,
    reason: RootStateReason,
    other: str | None = None,
) -> None:
    loaders: Dataloaders = info.context.loaders
    other = other if reason == RootStateReason.OTHER else None
    root_vulns = await loaders.root_vulnerabilities.load(root.id)
    root_vulns_non_deleted = filter_non_deleted(root_vulns)

    await roots_domain.deactivate_root(
        loaders=loaders,
        email=email,
        group_name=group_name,
        other=other,
        reason=reason,
        root=root,
    )
    if root.state.status != RootStatus.INACTIVE:
        await roots_domain.queue_actions_on_status_change(
            loaders=loaders,
            email=email,
            group_name=group_name,
            root=root,
            new_status=RootStatus.INACTIVE,
        )

    await batch_dal.put_action(
        action=Action.DEACTIVATE_ROOT,
        queue=IntegratesBatchQueue.SMALL,
        additional_info={"root_ids": [root.id]},
        entity=group_name,
        attempt_duration_seconds=7200,
        subject=email,
    )

    schedule(
        roots_notifications.send_deactivation_notification(
            email=email,
            group_name=group_name,
            loaders=loaders,
            reason=reason.value,
            root=root,
            root_vulns_non_deleted=root_vulns_non_deleted,
            other=other,
        ),
    )


@MUTATION.field("deactivateRoot")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_is_not_under_review,
)
@rename_kwargs({"id": "root_id"})
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    group_name: str,
    root_id: str,
    reason: str,
    other: str | None = None,
) -> SimplePayload:
    user_info: dict[str, str] = await sessions_domain.get_jwt_content(info.context)
    email: str = user_info["user_email"]
    loaders: Dataloaders = info.context.loaders
    root = await roots_utils.get_root(loaders, root_id, group_name)
    root_state_reason = RootStateReason[reason]
    if isinstance(root, GitRoot):
        await require_service_white(_deactivate_root)(
            info=info,
            root=root,
            email=email,
            group_name=group_name,
            reason=root_state_reason,
            other=other,
        )
        await solve_root_events(
            loaders=loaders,
            root=root,
            reason=EventSolutionReason.AFFECTED_RESOURCE_REMOVED_FROM_SCOPE,
            user_info=user_info,
            other="Event resolved by deactivating the root",
        )
    else:
        await require_service_black(_deactivate_root)(
            info=info,
            root=root,
            email=email,
            group_name=group_name,
            reason=root_state_reason,
            other=other,
        )

    return SimplePayload(success=True)
