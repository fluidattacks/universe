from integrates.api.mutations.add_toe_port import (
    mutate,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.toe_ports.types import (
    RootToePortsRequest,
)
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute,
    require_login,
)
from integrates.testing.aws import (
    IntegratesAws,
    IntegratesDynamodb,
)
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    IPRootFaker,
    IPRootStateFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import (
    mocks,
)

from .payloads.types import (
    SimplePayload,
)

EMAIL_TEST = "hacker@fluidattacks.com"
GROUP_NAME = "group1"
ROOT_ID = "root1"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_TEST, role="user"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=EMAIL_TEST,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
            ],
            roots=[
                IPRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=IPRootStateFaker(
                        address="192.168.0.1",
                        nickname="back",
                    ),
                ),
            ],
        ),
    )
)
async def test_add_toe_ports_success() -> None:
    loaders: Dataloaders = get_new_context()
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )

    result = await mutate(
        _parent=None,
        info=info,
        group_name=GROUP_NAME,
        root_id=ROOT_ID,
        address="192.168.0.1",
        port=8080,
    )
    assert result == SimplePayload(success=True)

    root_toe_ports = await loaders.root_toe_ports.load_nodes(
        RootToePortsRequest(group_name=GROUP_NAME, root_id=ROOT_ID),
    )

    assert len(root_toe_ports) == 1
    assert root_toe_ports[0].address == "192.168.0.1"
    assert root_toe_ports[0].port == "8080"
    assert root_toe_ports[0].state.be_present
    assert not root_toe_ports[0].state.has_vulnerabilities
    assert root_toe_ports[0].state.seen_first_time_by == EMAIL_TEST
