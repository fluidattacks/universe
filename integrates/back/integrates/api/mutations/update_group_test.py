from integrates.api.mutations.update_group import mutate
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.groups.enums import (
    GroupService,
    GroupStateJustification,
    GroupSubscriptionType,
    GroupTier,
)
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute,
    require_attribute_internal,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    GroupStateFaker,
    OrganizationAccessFaker,
    OrganizationAccessStateFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

ORG_ID = "ORG#6b110f25-cd46-e94c-2bad-b9e182cf91a8"


@parametrize(
    args=[
        "email",
        "group_name",
        "subscription",
        "has_essential",
        "has_advanced",
        "has_asm",
        "tier",
    ],
    cases=[
        [
            "customer_manager@fluidattacks.com",
            "testgroup",
            "CONTINUOUS",
            True,
            True,
            True,
            "ADVANCED",
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            groups=[
                GroupFaker(
                    name="testgroup",
                    organization_id=ORG_ID,
                    state=GroupStateFaker(service=GroupService.WHITE, tier=GroupTier.OTHER),
                ),
            ],
            stakeholders=[
                StakeholderFaker(email="customer_manager@fluidattacks.com", enrolled=True),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email="customer_manager@fluidattacks.com",
                    state=OrganizationAccessStateFaker(has_access=True, role="customer_manager"),
                ),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name="testgroup",
                    email="customer_manager@fluidattacks.com",
                    state=GroupAccessStateFaker(has_access=True, role="customer_manager"),
                ),
            ],
        ),
    )
)
async def test_update_group(
    email: str,
    group_name: str,
    subscription: GroupSubscriptionType,
    has_essential: bool,
    has_advanced: bool,
    has_asm: bool,
    tier: GroupTier,
) -> None:
    # Arrange
    loaders: Dataloaders = get_new_context()
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", group_name],
            [require_attribute_internal, "is_under_review", group_name],
        ],
    )

    # Act
    await mutate(
        _=None,
        info=info,
        comments="testing update group",
        group_name=group_name,
        reason="NONE",
        has_essential=has_essential,
        has_advanced=has_advanced,
        has_asm=has_asm,
        subscription=subscription,
        tier=tier,
    )

    # Assert
    loaders.group.clear_all()
    group = await loaders.group.load(group_name)
    assert group
    assert group.state.type == subscription
    assert group.state.has_essential == has_essential
    assert group.state.has_advanced == has_advanced
    assert group.state.justification == GroupStateJustification.NONE
    assert group.state.tier == tier


@parametrize(
    args=[
        "email",
        "group_name",
        "service",
        "subscription",
        "has_essential",
        "has_advanced",
        "has_asm",
        "tier",
    ],
    cases=[
        [
            "customer_manager@fluidattacks.com",
            "testgroup",
            "WHITE",
            "CONTINUOUS",
            True,
            True,
            True,
            "ADVANCED",
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            groups=[
                GroupFaker(
                    name="testgroup",
                    organization_id=ORG_ID,
                    state=GroupStateFaker(service=GroupService.BLACK, tier=GroupTier.ESSENTIAL),
                ),
            ],
            stakeholders=[
                StakeholderFaker(email="customer_manager@fluidattacks.com", enrolled=True)
            ],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email="customer_manager@fluidattacks.com",
                    state=OrganizationAccessStateFaker(has_access=True, role="customer_manager"),
                ),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name="testgroup",
                    email="customer_manager@fluidattacks.com",
                    state=GroupAccessStateFaker(has_access=True, role="customer_manager"),
                ),
            ],
        ),
    )
)
async def test_update_group_service(
    email: str,
    group_name: str,
    service: GroupService,
    subscription: GroupSubscriptionType,
    has_essential: bool,
    has_advanced: bool,
    has_asm: bool,
    tier: GroupTier,
) -> None:
    # Arrange
    loaders: Dataloaders = get_new_context()
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", group_name],
            [require_attribute_internal, "is_under_review", group_name],
        ],
    )

    # Act
    await mutate(
        _=None,
        info=info,
        comments="testing update group",
        group_name=group_name,
        service=service,
        reason="NONE",
        has_essential=has_essential,
        has_advanced=has_advanced,
        has_asm=has_asm,
        subscription=subscription,
        tier=tier,
    )

    # Assert
    loaders.group.clear_all()
    group = await loaders.group.load(group_name)
    assert group
    assert group.state.type == subscription
    assert group.state.has_essential == has_essential
    assert group.state.has_advanced == has_advanced
    assert group.state.justification == GroupStateJustification.NONE
    assert group.state.tier == tier
    assert group.state.service == service


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            groups=[GroupFaker(name="testgroup", organization_id=ORG_ID)],
            stakeholders=[StakeholderFaker(email="group_manager@gmail.com", enrolled=True)],
            group_access=[
                GroupAccessFaker(
                    group_name="testgroup",
                    email="group_manager@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
        ),
    )
)
async def test_update_group_unauthorized_role() -> None:
    # Arrange
    info = GraphQLResolveInfoFaker(
        user_email="group_manager@gmail.com",
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", "testgroup"],
            [require_attribute_internal, "is_under_review", "testgroup"],
        ],
    )

    # Act
    with raises(Exception, match="Access denied"):
        await mutate(
            _=None,
            info=info,
            comments="testing update group fail",
            group_name="testgroup",
            reason="NONE",
            has_essential=False,
            has_advanced=True,
            has_asm=True,
            subscription=GroupSubscriptionType.CONTINUOUS,
        )
