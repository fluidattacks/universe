import logging
import logging.config

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates import (
    authz,
)
from integrates.custom_exceptions import (
    InvalidRoleProvided,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_user_level_auth_async,
    require_login,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    AddStakeholderPayload,
)
from .schema import (
    MUTATION,
)

LOGGER = logging.getLogger(__name__)


@MUTATION.field("addStakeholder")
@concurrent_decorators(
    require_login,
    enforce_user_level_auth_async,
)
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    email: str,
    role: str,
) -> AddStakeholderPayload:
    loaders: Dataloaders = info.context.loaders
    email = email.lower().strip()
    user_data = await sessions_domain.get_jwt_content(info.context)
    user_email = user_data["user_email"]
    enforcer = await authz.get_user_level_enforcer(loaders, user_email)

    if not enforcer(f"grant_user_level_role:{role}"):
        LOGGER.error(
            "Invalid role provided",
            extra={
                "extra": {
                    "email": email,
                    "requester_email": user_email,
                    "role": role,
                },
            },
        )
        raise InvalidRoleProvided(role=role)

    await orgs_domain.add_without_group(
        email=email,
        role=role,
    )
    logs_utils.cloudwatch_log(
        info.context,
        "Added stakeholder",
        extra={"new_stakeholder": email, "log_type": "Security"},
    )

    return AddStakeholderPayload(success=True, email=email)
