from typing import Any

from integrates.api.mutations.send_assigned_notification import mutate
from integrates.custom_exceptions import InvalidNotificationRequest
from integrates.db_model.enums import TreatmentStatus
from integrates.db_model.types import Treatment
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_asm,
    require_attribute,
    require_attribute_internal,
    require_is_not_under_review,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    DATE_2019,
    FindingFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    StakeholderFaker,
    VulnerabilityFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import freeze_time, parametrize, raises

ADMIN = "admin@gmail.com"
GROUP_NAME = "group1"
FINDING_ID = "32d4c2c4-411c-4e30-bf33-7efbfe4ef353"
VULN_ID_1 = "vuln1"
VULN_ID_2 = "vuln2"

DEFAULT_DECORATORS: list[list[Any]] = [
    [require_login],
    [enforce_group_level_auth_async],
    [require_is_not_under_review],
    [require_asm],
    [require_attribute_internal, "is_under_review", GROUP_NAME],
    [require_attribute, "has_asm", GROUP_NAME],
]


@parametrize(args=["user_email"], cases=[[ADMIN]])
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME)],
            stakeholders=[
                StakeholderFaker(email=ADMIN),
            ],
            group_access=[
                GroupAccessFaker(
                    email=ADMIN,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="admin"),
                ),
            ],
            findings=[
                FindingFaker(
                    id=FINDING_ID,
                    group_name=GROUP_NAME,
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    finding_id=FINDING_ID,
                    id=VULN_ID_1,
                ),
            ],
        ),
    ),
)
async def test_send_assigned_notification_exception(
    user_email: str,
) -> None:
    info = GraphQLResolveInfoFaker(user_email=user_email, decorators=DEFAULT_DECORATORS)
    with raises(InvalidNotificationRequest, match="Invalid email"):
        await mutate(
            _=None,
            info=info,
            finding_id=FINDING_ID,
            vulnerabilities=[VULN_ID_1],
        )


@freeze_time(DATE_2019.isoformat())
@parametrize(args=["user_email"], cases=[[ADMIN]])
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME)],
            stakeholders=[
                StakeholderFaker(email=ADMIN),
            ],
            group_access=[
                GroupAccessFaker(
                    email=ADMIN,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="admin"),
                ),
            ],
            findings=[
                FindingFaker(
                    id=FINDING_ID,
                    group_name=GROUP_NAME,
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    finding_id=FINDING_ID,
                    id=VULN_ID_1,
                    treatment=Treatment(
                        modified_date=DATE_2019,
                        status=TreatmentStatus.UNTREATED,
                        assigned=ADMIN,
                    ),
                ),
                VulnerabilityFaker(
                    finding_id=FINDING_ID,
                    id=VULN_ID_2,
                    treatment=Treatment(
                        modified_date=DATE_2019,
                        status=TreatmentStatus.IN_PROGRESS,
                        assigned=ADMIN,
                    ),
                ),
            ],
        ),
    ),
)
async def test_send_assigned_notification_success(
    user_email: str,
) -> None:
    info = GraphQLResolveInfoFaker(user_email=user_email, decorators=DEFAULT_DECORATORS)
    payload = await mutate(
        _=None,
        info=info,
        finding_id=FINDING_ID,
        vulnerabilities=[VULN_ID_1, VULN_ID_2],
    )

    assert payload.success
