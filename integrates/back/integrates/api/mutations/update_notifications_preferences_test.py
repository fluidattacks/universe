from decimal import Decimal

from integrates.api.mutations.update_notifications_preferences import mutate
from integrates.custom_exceptions import InvalidCVSSField
from integrates.db_model.stakeholders.types import NotificationsParameters, NotificationsPreferences
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    OrganizationFaker,
    StakeholderFaker,
    StakeholderStateFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import raises

EMAIL_TEST = "admin@fluidattacks.com"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(
                    email=EMAIL_TEST,
                    role="user",
                    state=StakeholderStateFaker(
                        notifications_preferences=NotificationsPreferences(
                            parameters=NotificationsParameters(
                                min_severity=Decimal("5.5"),
                            ),
                            email=["VULNERABILITY_REPORT", "ROOT_UPDATE", "SERVICE_UPDATE"],
                        )
                    ),
                ),
            ],
        ),
    )
)
async def test_update_notifications_preferences_with_new_severity() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
    )
    result = await mutate(
        _=None,
        info=info,
        notifications_preferences={
            "parameters": {"min_severity": "7.0"},
            "email": ["EVENT_REPORT", "VULNERABILITY_REPORT", "ROOT_UPDATE", "SERVICE_UPDATE"],
        },
    )
    assert result.success


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(
                    email=EMAIL_TEST,
                    role="user",
                    state=StakeholderStateFaker(
                        notifications_preferences=NotificationsPreferences(
                            parameters=NotificationsParameters(
                                min_severity=Decimal("5.5"),
                            ),
                            email=["VULNERABILITY_REPORT", "ROOT_UPDATE", "SERVICE_UPDATE"],
                        )
                    ),
                ),
            ],
        ),
    )
)
async def test_update_notifications_preferences_invalid_severity() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
    )
    with raises(InvalidCVSSField):
        await mutate(
            _=None,
            info=info,
            notifications_preferences={
                "parameters": {"min_severity": "invalid"},
                "email": ["EVENT_REPORT", "VULNERABILITY_REPORT", "ROOT_UPDATE", "SERVICE_UPDATE"],
            },
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(
                    email=EMAIL_TEST,
                    role="user",
                    state=StakeholderStateFaker(
                        notifications_preferences=NotificationsPreferences(
                            parameters=NotificationsParameters(
                                min_severity=Decimal("5.5"),
                            ),
                            email=["VULNERABILITY_REPORT", "ROOT_UPDATE", "SERVICE_UPDATE"],
                        )
                    ),
                ),
            ],
        ),
    )
)
async def test_update_notifications_preferences_without_parameters() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
    )
    result = await mutate(
        _=None,
        info=info,
        notifications_preferences={
            "email": ["EVENT_REPORT", "VULNERABILITY_REPORT", "ROOT_UPDATE", "SERVICE_UPDATE"],
        },
    )
    assert result.success
