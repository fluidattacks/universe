from integrates import roots
from integrates.api.mutations.add_toe_lines import mutate
from integrates.custom_utils import datetime as datetime_utils
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.toe_lines.types import RootToeLinesRequest
from integrates.decorators import enforce_group_level_auth_async, require_attribute, require_login
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GitRootFaker,
    GitRootStateFaker,
    GraphQLResolveInfoFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import Mock, mocks

from .payloads.types import SimplePayload

EMAIL_TEST = "admin@fluidattacks.com"
GROUP_NAME = "group1"
ROOT_ID = "root1"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_TEST, role="admin"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(
                        nickname="back",
                    ),
                ),
            ],
        ),
    ),
    others=[Mock(roots.utils, "send_mail_environment", "async", None)],
)
async def test_add_toe_lines_success() -> None:
    loaders: Dataloaders = get_new_context()
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )

    result = await mutate(
        _parent=None,
        info=info,
        group_name=GROUP_NAME,
        root_id=ROOT_ID,
        filename="src/main.py",
        last_author="client@myapp.com",
        last_commit="a4d56f9b8d7c4c9e9f5b12c3a7f8c9e1e2a4b4c6",
        loc=100,
        modified_date=datetime_utils.get_minus_delta(date=datetime_utils.get_utc_now(), hours=2),
    )
    assert result == SimplePayload(success=True)

    root_toe_lines = await loaders.root_toe_lines.load_nodes(
        RootToeLinesRequest(group_name=GROUP_NAME, root_id=ROOT_ID),
    )

    assert len(root_toe_lines) == 1
    assert root_toe_lines[0].filename == "src/main.py"
    assert root_toe_lines[0].state.loc == 100
