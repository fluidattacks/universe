from typing import (
    TypedDict,
    Unpack,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.roots.enums import (
    RootEnvironmentUrlStateStatus,
    RootEnvironmentUrlType,
)
from integrates.db_model.roots.types import (
    RootEnvironmentUrlsRequest,
    RootRequest,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_login,
    require_service_white,
)
from integrates.jobs_orchestration.create_machine_config import (
    MachineJobOrigin,
    MachineJobTechnique,
    MachineModulesToExecute,
)
from integrates.jobs_orchestration.jobs import (
    queue_machine_job,
)
from integrates.roots import (
    environments as roots_environments,
)
from integrates.sessions.domain import (
    get_jwt_content,
)

from .payloads.types import (
    AddEnvironmentUrlPayload,
)
from .schema import (
    MUTATION,
)


class GitEnvironmentArgs(TypedDict):
    group_name: str
    root_id: str
    url: str
    url_type: str
    is_production: bool | None
    use_egress: bool | None
    use_vpn: bool | None
    use_ztna: bool | None


@MUTATION.field("addGitEnvironmentUrl")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_service_white,
    require_is_not_under_review,
)
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    **kwargs: Unpack[GitEnvironmentArgs],
) -> AddEnvironmentUrlPayload:
    loaders: Dataloaders = info.context.loaders
    user_info = await get_jwt_content(info.context)
    group_name = kwargs["group_name"]
    user_email = user_info["user_email"]
    url = kwargs["url"].strip()
    url_type = kwargs["url_type"]
    root_id = kwargs["root_id"]
    use_connection = {
        "use_egress": kwargs.get("use_egress"),
        "use_vpn": kwargs.get("use_vpn"),
        "use_ztna": kwargs.get("use_ztna"),
    }
    is_production = kwargs.get("is_production") or False
    if url_type == "CSPM":
        return AddEnvironmentUrlPayload(success=False, url_id="")

    success = await roots_environments.add_root_environment_url(
        loaders=loaders,
        group_name=group_name,
        root_id=root_id,
        url=url,
        url_type=url_type,
        user_email=user_email,
        should_notify=True,
        use_connection=use_connection,
        is_production=is_production,
    )
    logs_utils.cloudwatch_log(
        info.context,
        "Updated git envs for the root",
        extra={
            "group_name": group_name,
            "root_id": root_id,
            "log_type": "Security",
        },
    )
    root_env_urls = await get_new_context().root_environment_urls.load(
        RootEnvironmentUrlsRequest(root_id=root_id, group_name=group_name),
    )
    url_ids = [
        env_url.id
        for env_url in root_env_urls
        if env_url.url == url and env_url.state.status != RootEnvironmentUrlStateStatus.DELETED
    ]

    if (
        url_type in {RootEnvironmentUrlType.APK, RootEnvironmentUrlType.URL}
        and (root := await loaders.root.load(RootRequest(group_name, root_id)))
        and (root_nickname := root.state.nickname)
    ):
        await queue_machine_job(
            loaders=loaders,
            group_name=group_name,
            modified_by=user_email,
            root_nicknames={root_nickname},
            modules_to_execute=MachineModulesToExecute(
                APK=(url_type == RootEnvironmentUrlType.APK),
                CSPM=False,
                DAST=(url_type == RootEnvironmentUrlType.URL),
                SAST=False,
                SCA=False,
            ),
            job_origin=MachineJobOrigin.PLATFORM,
            job_technique=(
                MachineJobTechnique.DAST
                if url_type == RootEnvironmentUrlType.URL
                else MachineJobTechnique.APK
            ),
        )

    return AddEnvironmentUrlPayload(success=success, url_id=url_ids[-1] if url_ids else "")
