from integrates.api.mutations.verify_stakeholder import mutate
from integrates.custom_exceptions import (
    RequiredNewPhoneNumber,
    RequiredVerificationCode,
    SamePhoneNumber,
)
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
    StakeholderPhoneFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

ADMIN_EMAIL = "admin@gmail.com"
HACKER_EMAIL = "hacker@fluidattacks.com"
GROUP_NAME = "group1"
ORG_ID = "ORG#6b110f25-cd46-e94c-2bad-b9e182cf91a8"


@parametrize(
    args=["new_phone"],
    cases=[[{"calling_country_code": "1", "national_number": "123456"}], [""]],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id=ORG_ID),
            ],
            stakeholders=[
                StakeholderFaker(
                    email=ADMIN_EMAIL,
                    phone=StakeholderPhoneFaker(
                        country_code="US",
                        calling_country_code="1",
                        national_number="1111111111",
                    ),
                )
            ],
            group_access=[
                GroupAccessFaker(
                    email=ADMIN_EMAIL,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="admin"),
                ),
            ],
        )
    )
)
async def test_verify_stakeholder(new_phone: dict[str, str]) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=ADMIN_EMAIL,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
        ],
    )

    result = await mutate(
        _parent=None,
        info=info,
        user_email=ADMIN_EMAIL,
        new_phone=new_phone,
        verification_code="12134",
    )

    assert result.success


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id=ORG_ID),
            ],
            stakeholders=[
                StakeholderFaker(
                    email=ADMIN_EMAIL,
                    phone=StakeholderPhoneFaker(
                        country_code="US",
                        calling_country_code="1",
                        national_number="1111111111",
                    ),
                )
            ],
            group_access=[
                GroupAccessFaker(
                    email=ADMIN_EMAIL,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="admin"),
                ),
            ],
        )
    )
)
async def test_verify_stakeholder_required_verification_code() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=ADMIN_EMAIL,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
        ],
    )
    with raises(RequiredVerificationCode):
        await mutate(
            _parent=None,
            info=info,
            user_email=ADMIN_EMAIL,
            new_phone={"calling_country_code": "1", "national_number": "123456"},
            verification_code=None,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id=ORG_ID),
            ],
            stakeholders=[
                StakeholderFaker(
                    email=ADMIN_EMAIL,
                    phone=StakeholderPhoneFaker(
                        country_code="US",
                        calling_country_code="1",
                        national_number="22222222222",
                    ),
                )
            ],
            group_access=[
                GroupAccessFaker(
                    email=ADMIN_EMAIL,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="admin"),
                ),
            ],
        )
    )
)
async def test_verify_stakeholder_reapeted() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=ADMIN_EMAIL,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
        ],
    )
    with raises(SamePhoneNumber):
        await mutate(
            _parent=None,
            info=info,
            user_email=ADMIN_EMAIL,
            new_phone={"calling_country_code": "1", "national_number": "22222222222"},
            verification_code="12134",
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id=ORG_ID),
            ],
            stakeholders=[StakeholderFaker(email=HACKER_EMAIL, phone=None)],
            group_access=[
                GroupAccessFaker(
                    email=HACKER_EMAIL,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="hacker"),
                ),
            ],
        )
    )
)
async def test_verify_stakeholder_required() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=HACKER_EMAIL,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
        ],
    )
    with raises(RequiredNewPhoneNumber):
        await mutate(
            _parent=None,
            info=info,
            user_email=HACKER_EMAIL,
            new_phone=None,
            verification_code="12134",
        )
