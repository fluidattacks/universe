from ariadne import (
    ObjectType,
)
from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_exceptions import (
    ToePortNotFound,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.toe_ports.types import (
    ToePort,
    ToePortRequest,
)

from .types import (
    UpdateToePortPayload,
)

UPDATE_TOE_PORT_PAYLOAD = ObjectType("UpdateToePortPayload")


@UPDATE_TOE_PORT_PAYLOAD.field("toePort")
async def resolve(
    parent: UpdateToePortPayload,
    info: GraphQLResolveInfo,
    **_kwargs: None,
) -> ToePort:
    loaders: Dataloaders = info.context.loaders
    request = ToePortRequest(
        address=parent.address,
        port=parent.port,
        group_name=parent.group_name,
        root_id=parent.root_id,
    )
    loaders.toe_port.clear(request)
    toe_port = await loaders.toe_port.load(request)
    if toe_port is None:
        raise ToePortNotFound()

    return toe_port
