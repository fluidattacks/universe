from ariadne import (
    ObjectType,
)
from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_exceptions import (
    ToeLinesNotFound,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.toe_lines.types import (
    ToeLine,
    ToeLineRequest,
)

from .types import (
    UpdateToeLinesPayload,
)

UPDATE_TOE_LINES_PAYLOAD = ObjectType("UpdateToeLinesPayload")


@UPDATE_TOE_LINES_PAYLOAD.field("toeLines")
async def resolve(
    parent: UpdateToeLinesPayload,
    info: GraphQLResolveInfo,
    **_kwargs: None,
) -> ToeLine:
    loaders: Dataloaders = info.context.loaders
    request = ToeLineRequest(
        filename=parent.filename,
        group_name=parent.group_name,
        root_id=parent.root_id,
    )
    loaders.toe_lines.clear(request)
    toe_lines = await loaders.toe_lines.load(request)
    if toe_lines is None:
        raise ToeLinesNotFound()

    return toe_lines
