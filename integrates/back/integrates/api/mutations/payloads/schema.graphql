"Generic Payload interface definition"
interface Payload {
  "Did the query succeed?"
  success: Boolean!
}

"Simple Payload type definition"
type SimplePayload implements Payload {
  "Did the query succeed?"
  success: Boolean!
}
"Simple Payload with message type definition"
type SimplePayloadMessage implements Payload {
  "Message result"
  message: String
  "Did the query succeed?"
  success: Boolean!
}

"Add environment url Payload type definition"
type AddEnvironmentUrlPayload implements Payload {
  "Did the query succeed?"
  success: Boolean!
  "Newly added id url"
  urlId: String
}

"Add Docker Image Payload type definition"
type AddRootDockerImagePayload implements Payload {
  "Is the job to generate the SBOM queued?"
  sbomJobQueued: Boolean!
  "Did the query succeed?"
  success: Boolean!
  "Newly added id url"
  uri: String!
}

"Simple Payload type definition"
type AddOrganizationPayload implements Payload {
  "Newly added Organization"
  organization: Organization
  "Did the query succeed?"
  success: Boolean!
}

"Add root Payload type definition"
type AddRootPayload implements Payload {
  "Newly added id root"
  rootId: String
  "Did the query succeed?"
  success: Boolean!
}

"Upload git root file Payload type definition"
type UploadGitRootFilePayload implements Payload {
  "List of lines with errors"
  errorLines: [String!]!
  "Helper message about status of the request"
  message: String!
  "Did the query succeed?"
  success: Boolean!
  "Total error lines"
  totalErrors: Int!
  "Total success lines"
  totalSuccess: Int!
}

"Update access token Payload type definition"
type UpdateAccessTokenPayload implements Payload {
  "New API access token"
  sessionJwt: String!
  "Did the query succeed?"
  success: Boolean!
}

"Download file Payload type definition"
type DownloadFilePayload implements Payload {
  "Did the query succeed?"
  success: Boolean!
  "URL to the file"
  url: String!
}

"Grant stakeholder access Payload type definition"
type GrantStakeholderAccessPayload implements Payload {
  "New Organization/Group Stakeholder"
  grantedStakeholder: Stakeholder
  "Did the query succeed?"
  success: Boolean!
}

"Grant stakeholder access Payload type definition"
type RemoveStakeholderAccessPayload implements Payload {
  "Email address of the former Stakeholder"
  removedEmail: String
  "Did the query succeed?"
  success: Boolean!
}

"Update stakeholder Payload type definition"
type UpdateStakeholderPayload implements Payload {
  "Modified Organization/Group Stakeholder"
  modifiedStakeholder: Stakeholder
  "Did the query succeed?"
  success: Boolean!
}

"Update toe input Payload type definition"
type UpdateToeInputPayload implements Payload {
  "Did the query succeed?"
  success: Boolean!
  "Modified Toe Input"
  toeInput: ToeInput!
}

"Update toe lines Payload type definition"
type UpdateToeLinesPayload implements Payload {
  "Did the query succeed?"
  success: Boolean!
  "Modified Toe Lines"
  toeLines: ToeLines!
}

"Update toe port Payload type definition"
type UpdateToePortPayload implements Payload {
  "Did the query succeed?"
  success: Boolean!
  "Modified Toe Port"
  toePort: ToePort!
}

"Add stakeholder Payload type definition"
type AddStakeholderPayload implements Payload {
  "Email address of the potential new Stakeholder"
  email: String
  "Did the query succeed?"
  success: Boolean!
}

"Simple finding Payload type definition"
type SimpleFindingPayload implements Payload {
  "Updated Finding"
  finding: Finding
  "Did the query succeed?"
  success: Boolean!
}

"Add consult Payload type definition"
type AddConsultPayload implements Payload {
  "Identifier of the new comment"
  commentId: String
  "Did the query succeed?"
  success: Boolean!
}

"Add Event Payload type definition"
type AddEventPayload implements Payload {
  "Identifier of the new Event"
  eventId: String!
  "Did the query succeed?"
  success: Boolean!
}

"Approve draft Payload type definition"
type SimpleGroupPayload implements Payload {
  "Updated Group"
  group: Group
  "Did the query succeed?"
  success: Boolean!
}

"Sign post URLs Payload Payload type definition"
type SignPostUrlsPayload implements Payload {
  "Did the query succeed?"
  success: Boolean!
  "Metadata of the newly uploaded file"
  url: SignedUrlObject!
}

"Signed URL object Payload type definition"
type SignedUrlObject {
  "Signed URL metadata"
  fields: SignedFieldsObject!
  "Signed URL"
  url: String!
}

"""
Signed fields object Payload type definition, for more information about
this type feel free to check the relevant [AWS docs](https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/private-content-signed-urls.html)
"""
type SignedFieldsObject {
  "Encryption Algorithm"
  algorithm: String!
  "Assigned Credential"
  credential: String!
  "Signature creation Date"
  date: String!
  "Key"
  key: String!
  "Access control policy"
  policy: String!
  "Signature Security Token"
  securitytoken: String!
  "Signature"
  signature: String!
}

"Mailmap entry response object"
type MailmapEntryPayload {
  "Mailmap entry object: User primary account"
  entry: MailmapEntry!
  "Request result"
  success: Boolean!
}

"Mailmap entry with subentries response object"
type MailmapEntryWithSubentriesPayload {
  "Mailmap entry with subentries object: User accounts"
  entryWithSubentries: MailmapEntryWithSubentries!
  "Request result"
  success: Boolean!
}

"Import mailmap response object"
type ImportMailmapPayload {
  "List of exceptions messages obtained after this operation"
  exceptionsMessages: [String]!
  "Request result"
  success: Boolean!
  "Number of mailmap entries that were successfully processed"
  successCount: Int!
}
