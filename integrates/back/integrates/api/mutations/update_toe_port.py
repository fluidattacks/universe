from typing import (
    TypedDict,
    Unpack,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.api.types import (
    APP_EXCEPTIONS,
)
from integrates.custom_exceptions import (
    ToePortNotFound,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.toe_ports.types import (
    ToePortRequest,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_login,
)
from integrates.sessions import (
    domain as sessions_domain,
)
from integrates.toe.ports import (
    domain as toe_ports_domain,
)
from integrates.toe.ports.types import (
    ToePortAttributesToUpdate,
)

from .payloads.types import (
    UpdateToePortPayload,
)
from .schema import (
    MUTATION,
)


class UpdateToePortArgs(TypedDict):
    be_present: bool
    address: str
    port: int
    group_name: str
    root_id: str
    has_recent_attack: bool | None


@MUTATION.field("updateToePort")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_asm,
)
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    **kwargs: Unpack[UpdateToePortArgs],
) -> UpdateToePortPayload:
    try:
        user_info = await sessions_domain.get_jwt_content(info.context)
        user_email = user_info["user_email"]
        loaders: Dataloaders = info.context.loaders
        group_name = kwargs["group_name"]
        be_present = kwargs["be_present"]
        address = kwargs["address"]
        port = kwargs["port"]
        root_id = kwargs["root_id"]
        current_value = await loaders.toe_port.load(
            ToePortRequest(
                address=address,
                port=str(port),
                group_name=group_name,
                root_id=root_id,
            ),
        )
        if current_value is None:
            raise ToePortNotFound()

        be_present_to_update = None if be_present is current_value.state.be_present else be_present
        attacked_at_to_update = (
            datetime_utils.get_utc_now() if kwargs.get("has_recent_attack") is True else None
        )
        attacked_by_to_update = None if attacked_at_to_update is None else user_email
        await toe_ports_domain.update(
            current_value=current_value,
            attributes=ToePortAttributesToUpdate(
                attacked_at=attacked_at_to_update,
                attacked_by=attacked_by_to_update,
                be_present=be_present_to_update,
            ),
            modified_by=user_email,
        )
        logs_utils.cloudwatch_log(
            info.context,
            "Updated toe port in the group",
            extra={"group_name": group_name, "log_type": "Security"},
        )
    except APP_EXCEPTIONS:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to update toe port in the group",
            extra={"group_name": group_name, "log_type": "Security"},
        )
        raise

    return UpdateToePortPayload(
        address=address,
        port=str(port),
        group_name=group_name,
        root_id=root_id,
        success=True,
    )
