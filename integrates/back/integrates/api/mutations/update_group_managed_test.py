from integrates.api.mutations.update_group_managed import mutate
from integrates.custom_exceptions import InvalidManagedChange
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_asm,
    require_attribute,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

ADMIN_EMAIL = "admin@gmail.com"
HACKER_EMAIL = "hacker@fluidattacks.com"
GROUP_NAME = "test-group"
RESOURCER_EMAIL = "resourcer@fluidattacks.com"
USER_EMAIL = "user@gmail.com"


@parametrize(
    args=["email", "managed"],
    cases=[
        [ADMIN_EMAIL, "MANAGED"],
        [ADMIN_EMAIL, "UNDER_REVIEW"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=ADMIN_EMAIL),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
        )
    )
)
async def test_update_group_managed_success(email: str, managed: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    result = await mutate(
        _=None,
        info=info,
        comments="Test managed update",
        group_name=GROUP_NAME,
        managed=managed,
    )
    assert result.success


@parametrize(
    args=["email", "managed"],
    cases=[
        [ADMIN_EMAIL, "NOT_MANAGED"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=ADMIN_EMAIL),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
        )
    )
)
async def test_update_group_managed_invalid_changed(email: str, managed: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    with raises(InvalidManagedChange):
        await mutate(
            _=None,
            info=info,
            comments="Test managed update",
            group_name=GROUP_NAME,
            managed=managed,
        )


@parametrize(
    args=["email"],
    cases=[
        [USER_EMAIL],
        [HACKER_EMAIL],
        [RESOURCER_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=USER_EMAIL),
                StakeholderFaker(email=HACKER_EMAIL),
                StakeholderFaker(email=RESOURCER_EMAIL),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=USER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=HACKER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=RESOURCER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="resourcer"),
                ),
            ],
        )
    )
)
async def test_update_group_managed_unauthorized(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    with raises(Exception, match="Access denied"):
        await mutate(
            _=None,
            info=info,
            comments="Test managed update",
            group_name=GROUP_NAME,
            managed="MANAGED",
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=ADMIN_EMAIL),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
        )
    )
)
async def test_update_group_managed_invalid_enum() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=ADMIN_EMAIL,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    with raises(ValueError):
        await mutate(
            _=None,
            info=info,
            comments="Test managed update",
            group_name=GROUP_NAME,
            managed="INVALID_VALUE",
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=ADMIN_EMAIL),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
        )
    )
)
async def test_update_group_managed_nonexistent_group() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=ADMIN_EMAIL,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    with raises(Exception):
        await mutate(
            _=None,
            info=info,
            comments="Test managed update",
            group_name="nonexistent_group",
            managed="MANAGED",
        )
