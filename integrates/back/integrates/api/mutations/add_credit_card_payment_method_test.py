from typing import Any

from integrates.api.mutations.add_credit_card_payment_method import mutate
from integrates.billing import domain as billing_utils
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.decorators import require_is_not_under_review, require_login
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationAccessFaker,
    OrganizationAccessStateFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import parametrize, raises

# Allowed users
ADMIN_EMAIL = "admin@gmail.com"
ORG_MANAGER_EMAIL = "org_manager@gmail.com"

# General parameters
GROUP_NAME = "group3"
ORGTEST_ID = "ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db"


DEFAULT_DECORATORS: list[list[Any]] = [
    [require_login],
    [require_is_not_under_review],
]


@parametrize(
    args=[
        "make_default",
    ],
    cases=[
        [False],
        [True],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=ADMIN_EMAIL),
                StakeholderFaker(email=ORG_MANAGER_EMAIL),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORGTEST_ID,
                    email=ADMIN_EMAIL,
                    state=OrganizationAccessStateFaker(role="admin"),
                ),
                OrganizationAccessFaker(organization_id=ORGTEST_ID, email=ORG_MANAGER_EMAIL),
            ],
            organizations=[OrganizationFaker(id=ORGTEST_ID)],
        )
    ),
    others=[
        Mock(logs_utils, "cloudwatch_log", "sync", None),
    ],
)
async def test_add_credit_card_payment_method(
    make_default: bool,
) -> None:
    user_email = "org_level_user_manage@fluidattacks.com"
    billed_groups = ["group_1", "group_2"]
    info = GraphQLResolveInfoFaker(
        user_email=user_email,
        decorators=DEFAULT_DECORATORS,
    )
    with raises(Exception, match="Access denied"):
        await mutate(
            _parent=None,
            info=info,
            make_default=make_default,
            organization_id=ORGTEST_ID,
            payment_method_id="5674380432",
            billing_email="testing@fluidattacks.com",
            billed_groups=billed_groups,
            user=user_email,
            business_name=None,
            business_id=None,
            country=None,
        )


@parametrize(
    args=["make_default", "email"],
    cases=[
        [False, ADMIN_EMAIL],
        [True, ORG_MANAGER_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=ADMIN_EMAIL, enrolled=True),
                StakeholderFaker(email=ORG_MANAGER_EMAIL, enrolled=True),
            ],
            groups=[
                GroupFaker(name="group_1", organization_id=ORGTEST_ID),
                GroupFaker(name="group_2", organization_id=ORGTEST_ID),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name="group_1",
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
                GroupAccessFaker(
                    group_name="group_2",
                    email=ORG_MANAGER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="organization_manager"),
                ),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORGTEST_ID,
                    email=ADMIN_EMAIL,
                    state=OrganizationAccessStateFaker(has_access=True, role="admin"),
                ),
                OrganizationAccessFaker(
                    organization_id=ORGTEST_ID,
                    email=ORG_MANAGER_EMAIL,
                    state=OrganizationAccessStateFaker(
                        has_access=True, role="organization_manager"
                    ),
                ),
            ],
            organizations=[OrganizationFaker(id=ORGTEST_ID)],
        )
    ),
    others=[
        Mock(logs_utils, "cloudwatch_log", "sync", None),
        Mock(billing_utils, "create_credit_card_payment_method", "async", True),
    ],
)
async def test_add_credit_card_payment_method_success(make_default: bool, email: str) -> None:
    billed_groups = ["group_1", "group_2"]
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=DEFAULT_DECORATORS,
    )
    await mutate(
        _parent=None,
        info=info,
        make_default=make_default,
        organization_id=ORGTEST_ID,
        payment_method_id="5674380432",
        billing_email="testing@fluidattacks.com",
        billed_groups=billed_groups,
        business_name=None,
        business_id=None,
        country=None,
    )
