from typing import Any

from integrates.api.mutations.reject_vulnerabilities_zero_risk import mutate
from integrates.custom_exceptions import VulnNotInFinding
from integrates.dataloaders import get_new_context
from integrates.db_model.finding_comments.enums import CommentType
from integrates.db_model.finding_comments.types import FindingCommentsRequest
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
    VulnerabilityZeroRiskStatus,
)
from integrates.db_model.vulnerabilities.types import (
    GroupVulnerabilitiesRequest,
    VulnerabilityZeroRisk,
)
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute,
    require_login,
    require_report_vulnerabilities,
    require_request_zero_risk,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    DATE_2024,
    FindingFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    StakeholderFaker,
    VulnerabilityFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

# Allowed
ADMIN = "admin@gmail.com"
ARCHITECT = "architect@gmail.com"
REVIEWER = "reviewer@gmail.com"

# Not allowed
HACKER = "hacker@gmail.com"
REATTACKER = "reattacker@gmail.com"
USER = "user@gmail.com"
GROUP_MANAGER = "group_manager@gmail.com"


VULN_ID_1 = "vuln_id_1"
FINDING_ID = "b8ee4984-dbf4-43a9-bfca-58e82eca073f"

GROUP_NAME = "group_name"

DEFAULT_DECORATORS: list[list[Any]] = [
    [require_request_zero_risk],
    [require_login],
    [require_report_vulnerabilities],
    [enforce_group_level_auth_async],
    [require_attribute, "can_request_zero_risk", GROUP_NAME],
    [require_attribute, "can_report_vulnerabilities", GROUP_NAME],
]


@parametrize(
    args=["email"],
    cases=[
        [HACKER],
        [REATTACKER],
        [USER],
        [GROUP_MANAGER],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME)],
            stakeholders=[
                StakeholderFaker(email=HACKER),
                StakeholderFaker(email=REATTACKER),
                StakeholderFaker(email=USER),
                StakeholderFaker(email=GROUP_MANAGER),
            ],
            group_access=[
                GroupAccessFaker(
                    email=HACKER,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="hacker"),
                ),
                GroupAccessFaker(
                    email=REATTACKER,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="reattacker"),
                ),
                GroupAccessFaker(
                    email=USER,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="user"),
                ),
                GroupAccessFaker(
                    email=GROUP_MANAGER,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="group_manager"),
                ),
            ],
            findings=[FindingFaker(id=FINDING_ID, group_name=GROUP_NAME)],
            vulnerabilities=[
                VulnerabilityFaker(id=VULN_ID_1, finding_id=FINDING_ID, group_name=GROUP_NAME)
            ],
        )
    )
)
async def test_reject_vuln_zero_risk_access_denied(
    email: str,
) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=DEFAULT_DECORATORS,
    )
    with raises(Exception, match="Access denied"):
        await mutate(
            _=None,
            info=info,
            finding_id=FINDING_ID,
            justification="Some test justification",
            vulnerabilities=[VULN_ID_1],
        )


@parametrize(
    args=["email"],
    cases=[[ADMIN], [ARCHITECT], [REVIEWER]],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME)],
            stakeholders=[
                StakeholderFaker(email=ADMIN),
                StakeholderFaker(email=ARCHITECT),
                StakeholderFaker(email=REVIEWER),
            ],
            group_access=[
                GroupAccessFaker(
                    email=ADMIN,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="admin"),
                ),
                GroupAccessFaker(
                    email=ARCHITECT,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="architect"),
                ),
                GroupAccessFaker(
                    email=REVIEWER,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="reviewer"),
                ),
            ],
            findings=[FindingFaker(id=FINDING_ID, group_name=GROUP_NAME)],
            vulnerabilities=[VulnerabilityFaker(id=VULN_ID_1, group_name=GROUP_NAME)],
        )
    )
)
async def test_reject_vuln_zero_risk_invalid_finding_id(
    email: str,
) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=DEFAULT_DECORATORS,
    )
    with raises(VulnNotInFinding):
        await mutate(
            _=None,
            info=info,
            finding_id=FINDING_ID,
            justification="Some test justification",
            vulnerabilities=[VULN_ID_1],
        )


@parametrize(
    args=["email"],
    cases=[[ADMIN], [ARCHITECT], [REVIEWER]],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME)],
            stakeholders=[
                StakeholderFaker(email=ADMIN),
                StakeholderFaker(email=ARCHITECT),
                StakeholderFaker(email=REVIEWER),
            ],
            group_access=[
                GroupAccessFaker(
                    email=ADMIN,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="admin"),
                ),
                GroupAccessFaker(
                    email=ARCHITECT,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="architect"),
                ),
                GroupAccessFaker(
                    email=REVIEWER,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="reviewer"),
                ),
            ],
            findings=[FindingFaker(id=FINDING_ID, group_name=GROUP_NAME)],
            vulnerabilities=[
                VulnerabilityFaker(
                    id=VULN_ID_1,
                    group_name=GROUP_NAME,
                    finding_id=FINDING_ID,
                    zero_risk=VulnerabilityZeroRisk(
                        comment_id="comment_id",
                        modified_by=ADMIN,
                        modified_date=DATE_2024,
                        status=VulnerabilityZeroRiskStatus.REQUESTED,
                    ),
                )
            ],
        )
    )
)
async def test_reject_vuln_zero_risk_success(
    email: str,
) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=DEFAULT_DECORATORS,
    )
    justification = "Some test justification"

    await mutate(
        _=None,
        info=info,
        finding_id=FINDING_ID,
        justification=justification,
        vulnerabilities=[VULN_ID_1],
    )

    loaders = get_new_context()

    vulnerability = await loaders.vulnerability.load(VULN_ID_1)
    assert vulnerability
    assert vulnerability.state.status == VulnerabilityStateStatus.VULNERABLE
    assert vulnerability.zero_risk
    assert vulnerability.zero_risk.status == VulnerabilityZeroRiskStatus.REJECTED
    zero_risk_comments = await loaders.finding_comments.load(
        FindingCommentsRequest(comment_type=CommentType.ZERO_RISK, finding_id=FINDING_ID)
    )
    assert zero_risk_comments[-1].finding_id == FINDING_ID
    assert zero_risk_comments[-1].content == justification
    assert zero_risk_comments[-1].comment_type == CommentType.ZERO_RISK
    assert zero_risk_comments[-1].email == email

    new_vulnerable_locations = await loaders.group_vulnerabilities.load(
        GroupVulnerabilitiesRequest(
            group_name=GROUP_NAME,
            state_status=VulnerabilityStateStatus.VULNERABLE,
            paginate=False,
        )
    )
    assert vulnerability in tuple(edge.node for edge in new_vulnerable_locations.edges)
