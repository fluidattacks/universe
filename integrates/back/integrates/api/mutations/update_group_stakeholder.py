import logging
import logging.config

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates import (
    authz,
)
from integrates.authz.validations import (
    validate_fluidattacks_staff_on_group_deco,
    validate_role_fluid_reqs_deco,
)
from integrates.custom_exceptions import (
    InvalidRoleProvided,
    StakeholderNotFound,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.custom_utils.groups import get_group
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_is_not_under_review,
    require_login,
)
from integrates.group_access.domain import (
    exists,
    get_group_access,
)
from integrates.sessions import (
    domain as sessions_domain,
)
from integrates.stakeholders import (
    domain as stakeholders_domain,
)

from .payloads.types import (
    UpdateStakeholderPayload,
)
from .schema import (
    MUTATION,
)

LOGGER = logging.getLogger(__name__)


@validate_fluidattacks_staff_on_group_deco("group", "modified_email", "modified_role")
@validate_role_fluid_reqs_deco(email_field="modified_email", role_field="modified_role")
async def _update_stakeholder(
    *,
    info: GraphQLResolveInfo,
    updated_data: dict[str, str],
    modified_role: str,
    modified_email: str,
    group: Group,
    modified_by: str,
) -> None:
    loaders: Dataloaders = info.context.loaders
    if not await exists(loaders, group.name, modified_email):
        raise StakeholderNotFound()

    group_access = await get_group_access(loaders, group.name, modified_email)
    invitation = group_access.state.invitation
    email = updated_data["email"]
    responsibility = updated_data["responsibility"]
    role = updated_data["role"]
    if invitation and not invitation.is_used:
        await stakeholders_domain.update_invited_stakeholder(
            loaders=loaders,
            email=email,
            responsibility=responsibility,
            role=role,
            invitation=invitation,
            group=group,
            modified_by=modified_by,
        )
    else:
        await authz.grant_group_level_role(
            loaders,
            modified_email,
            group.name,
            modified_role,
            modified_by,
        )
        await stakeholders_domain.update_information(
            info.context,
            updated_data,
            group.name,
            modified_by,
        )


@MUTATION.field("updateGroupStakeholder")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_asm,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    **updated_data: str,
) -> UpdateStakeholderPayload:
    group_name: str = updated_data["group_name"].lower()
    modified_role: str = updated_data["role"]
    modified_email: str = updated_data["email"]
    user_data = await sessions_domain.get_jwt_content(info.context)
    user_email = user_data["user_email"]

    loaders: Dataloaders = info.context.loaders
    group = await get_group(loaders, group_name)
    enforcer = await authz.get_group_level_enforcer(loaders, user_email)

    if not enforcer(group_name, f"grant_group_level_role:{modified_role}"):
        LOGGER.error(
            "Invalid role provided",
            extra={
                "extra": {
                    "modified_user_role": modified_role,
                    "group_name": group_name,
                    "requester_email": user_email,
                },
            },
        )
        raise InvalidRoleProvided(role=modified_role)

    await _update_stakeholder(
        info=info,
        updated_data=updated_data,
        modified_role=modified_role,
        modified_email=modified_email,
        group=group,
        modified_by=user_email,
    )
    logs_utils.cloudwatch_log(
        info.context,
        "Updated stakeholder in the group",
        extra={
            "group_name": group_name,
            "modified_email": modified_email,
            "log_type": "Security",
        },
    )

    return UpdateStakeholderPayload(
        success=True,
        modified_stakeholder={
            "group_name": group_name,
            "email": modified_email,
        },
    )
