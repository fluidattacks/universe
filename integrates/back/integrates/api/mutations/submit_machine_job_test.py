from integrates.api.mutations.payloads.types import SimplePayloadMessage
from integrates.api.mutations.submit_machine_job import mutate
from integrates.batch.enums import Action
from integrates.batch.types import PutActionResult
from integrates.custom_exceptions import FindingNotFound, MachineExecutionAlreadySubmitted
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute,
    require_attribute_internal,
    require_login,
)
from integrates.machine import jobs
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    BatchProcessingFaker,
    FindingFaker,
    FindingStateFaker,
    GitRootFaker,
    GitRootStateFaker,
    GraphQLResolveInfoFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import raises

EMAIL_TEST = "admin@fluidattacks.com"
MACHINE_EMAIL = "machine@fluidattacks.com"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="admin")],
            groups=[GroupFaker(name="group1", organization_id="org1")],
            roots=[GitRootFaker(group_name="group1", id="root1")],
            findings=[
                FindingFaker(
                    group_name="group1",
                    id="8f2f69e7-db80-4123-9871-5b4a382b4fce",
                    state=FindingStateFaker(modified_by=MACHINE_EMAIL),
                    title="001. C-SHARP SQL Injection",
                ),
            ],
        ),
    ),
    others=[Mock(jobs, "put_machine_action", "async", PutActionResult(success=True))],
)
async def test_submit_machine_job() -> None:
    group = "group1"
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", group],
            [require_attribute_internal, "is_under_review", group],
        ],
    )
    mutation_result = await mutate(
        _=None,
        info=info,
        finding_id="8f2f69e7-db80-4123-9871-5b4a382b4fce",
        root_nicknames=None,
    )

    assert mutation_result == SimplePayloadMessage(
        success=True,
        message="Machine execution was successfully queued",
    )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="admin")],
            groups=[GroupFaker(name="group1", organization_id="org1")],
            roots=[GitRootFaker(group_name="group1", id="root1")],
            findings=[
                FindingFaker(
                    group_name="group1",
                    id="8f2f69e7-db80-4123-9871-5b4a382b4fce",
                    state=FindingStateFaker(modified_by="hacker@fluidattacks.com"),
                    title="666. Not a machine finding",
                ),
            ],
        ),
    ),
    others=[Mock(jobs, "put_machine_action", "async", PutActionResult(success=True))],
)
async def test_submit_machine_job_fail() -> None:
    group = "group1"
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", group],
            [require_attribute_internal, "is_under_review", group],
        ],
    )
    with raises(FindingNotFound):
        await mutate(
            _=None,
            info=info,
            finding_id="8f2f69e7-db80-4123-9871-5b4a382b4fce",
            root_nicknames=None,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="admin")],
            groups=[GroupFaker(name="group1", organization_id="org1")],
            roots=[
                GitRootFaker(
                    group_name="group1", id="root1", state=GitRootStateFaker(nickname="root1")
                ),
            ],
            findings=[
                FindingFaker(
                    group_name="group1",
                    id="8f2f69e7-db80-4123-9871-5b4a382b4fce",
                    state=FindingStateFaker(modified_by="hacker@fluidattacks.com"),
                    title="001. Not a machine finding",
                ),
            ],
            actions=[
                BatchProcessingFaker(
                    action_name=Action.EXECUTE_MACHINE,
                    entity="group1",
                    additional_info={
                        "roots": ["root1"],
                        "roots_config_files": ["root1.yaml"],
                    },
                ),
            ],
        ),
    ),
    others=[Mock(jobs, "put_machine_action", "async", PutActionResult(success=True))],
)
async def test_submit_machine_job_fail_2() -> None:
    group = "group1"
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", group],
            [require_attribute_internal, "is_under_review", group],
        ],
    )
    with raises(MachineExecutionAlreadySubmitted):
        await mutate(
            _=None,
            info=info,
            finding_id="8f2f69e7-db80-4123-9871-5b4a382b4fce",
            root_nicknames=None,
        )
