from integrates.api.mutations.add_finding import mutate
from integrates.custom_exceptions import InvalidCVSS4VectorString, InvalidMinTimeToRemediate
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_asm,
    require_attribute,
    require_attribute_internal,
    require_is_not_under_review,
    require_login,
    require_report_vulnerabilities,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

ADMIN_EMAIL = "admin@gmail.com"
GROUP_NAME = "test-group"


@parametrize(
    args=["email"],
    cases=[
        [ADMIN_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=ADMIN_EMAIL),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
        )
    )
)
async def test_add_finding_invalid_cvss_vector(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
            [require_report_vulnerabilities],
            [require_attribute, "can_report_vulnerabilities", GROUP_NAME],
        ],
    )
    with raises(InvalidCVSS4VectorString):
        await mutate(
            _parent=None,
            info=info,
            attack_vector_description="Test attack vector",
            cvss_vector="INVALID_VECTOR",
            description="Test description",
            group_name=GROUP_NAME,
            recommendation="Test recommendation",
            title="001. SQL injection - C Sharp SQL API",
            threat="Authenticated attacker from the Internet.",
            unfulfilled_requirements=["169", "173"],
            min_time_to_remediate=30,
            cvss_4_vector="",
        )


@parametrize(
    args=["email"],
    cases=[
        [ADMIN_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=ADMIN_EMAIL),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
        )
    )
)
async def test_add_finding_success(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
            [require_report_vulnerabilities],
            [require_attribute, "can_report_vulnerabilities", GROUP_NAME],
        ],
    )
    result = await mutate(
        _parent=None,
        info=info,
        attack_vector_description="Test attack vector",
        cvss_vector="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H",
        description="Test description",
        group_name=GROUP_NAME,
        recommendation="Test recommendation",
        title="001. SQL injection - C Sharp SQL API",
        threat="Authenticated attacker from the Internet.",
        unfulfilled_requirements=["169", "173"],
        min_time_to_remediate=None,
        cvss_4_vector="CVSS:4.0/AV:N/AC:L/AT:N/PR:N/UI:N/VC:H/VI:H/VA:H/SC:H/SI:H/SA:H",
    )

    assert result.success


@parametrize(
    args=["email"],
    cases=[
        [ADMIN_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=ADMIN_EMAIL),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
        )
    )
)
async def test_add_finding_invalid_min_time(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
            [require_report_vulnerabilities],
            [require_attribute, "can_report_vulnerabilities", GROUP_NAME],
        ],
    )
    with raises(InvalidMinTimeToRemediate):
        await mutate(
            _parent=None,
            info=info,
            attack_vector_description="Test attack vector",
            cvss_vector="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H",
            description="Test description",
            group_name=GROUP_NAME,
            recommendation="Test recommendation",
            title="Test Finding",
            threat="Test threat",
            unfulfilled_requirements=["REQ.001"],
            min_time_to_remediate=-1,
            cvss_4_vector="",
        )
