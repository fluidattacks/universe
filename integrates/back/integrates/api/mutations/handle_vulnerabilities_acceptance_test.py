from datetime import datetime

from graphql import (
    GraphQLError,
)

from integrates.api.mutations.handle_vulnerabilities_acceptance import (
    mutate,
)
from integrates.custom_exceptions import (
    AcceptanceNotRequested,
    VulnNotFound,
)
from integrates.db_model.enums import (
    AcceptanceStatus,
    TreatmentStatus,
)
from integrates.db_model.types import Treatment
from integrates.db_model.vulnerabilities.enums import VulnerabilityVerificationStatus
from integrates.db_model.vulnerabilities.types import VulnerabilityVerification
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute,
    require_attribute_internal,
    require_is_not_under_review,
    require_login,
)
from integrates.testing.aws import (
    IntegratesAws,
    IntegratesDynamodb,
)
from integrates.testing.fakers import (
    DATE_2019,
    EMAIL_FLUIDATTACKS_ADMIN,
    EMAIL_FLUIDATTACKS_CUSTOMER_MANAGER,
    EMAIL_FLUIDATTACKS_GROUP_MANAGER,
    EMAIL_FLUIDATTACKS_HACKER,
    EMAIL_FLUIDATTACKS_REATTACKER,
    EMAIL_FLUIDATTACKS_RESOURCER,
    EMAIL_FLUIDATTACKS_REVIEWER,
    EMAIL_FLUIDATTACKS_USER,
    EMAIL_FLUIDATTACKS_VULNERABILITY_MANAGER,
    FindingFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
    VulnerabilityFaker,
)
from integrates.testing.mocks import (
    mocks,
)
from integrates.testing.utils import (
    parametrize,
    raises,
)

FINDING_ID = "3c475384-834c-47b0-ac71-a41a022e401c"
ACCEPTED_VULN_ID = "be09edb7-cd5c-47ed-bee4-97c645acdce8"
REJECTED_VULN_ID = "be09edb7-cd5c-47ed-bee4-97c645acdce9"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(organization_id="org1"),
            ],
            findings=[FindingFaker(id=FINDING_ID)],
            vulnerabilities=[
                VulnerabilityFaker(
                    id=ACCEPTED_VULN_ID,
                    finding_id=FINDING_ID,
                    treatment=None,
                    verification=VulnerabilityVerification(
                        modified_by="unittest@fluidattacks.com",
                        modified_date=datetime.fromisoformat("2018-04-09T00:45:11+00:00"),
                        status=VulnerabilityVerificationStatus.REQUESTED,
                    ),
                ),
                VulnerabilityFaker(
                    id=REJECTED_VULN_ID,
                    finding_id=FINDING_ID,
                    treatment=None,
                    verification=VulnerabilityVerification(
                        modified_by="unittest@fluidattacks.com",
                        modified_date=datetime.fromisoformat("2018-04-09T00:45:11+00:00"),
                        status=VulnerabilityVerificationStatus.REQUESTED,
                    ),
                ),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_VULNERABILITY_MANAGER),
            ],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_VULNERABILITY_MANAGER,
                    state=GroupAccessStateFaker(has_access=True, role="vulnerability_manager"),
                ),
            ],
            organizations=[
                OrganizationFaker(id="org1"),
            ],
        ),
    )
)
async def test_handle_vulnerabilities_acceptance_no_treatment() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_FLUIDATTACKS_VULNERABILITY_MANAGER,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", "test-group"],
            [require_attribute, "has_asm", "test-group"],
        ],
    )

    result = await mutate(
        _=None,
        info=info,
        finding_id=FINDING_ID,
        justification="test justification",
        accepted_vulnerabilities=[ACCEPTED_VULN_ID],
        rejected_vulnerabilities=[REJECTED_VULN_ID],
    )

    assert result.success


@parametrize(
    args=["email"],
    cases=[
        [EMAIL_FLUIDATTACKS_GROUP_MANAGER],
        [EMAIL_FLUIDATTACKS_VULNERABILITY_MANAGER],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(organization_id="org1"),
            ],
            findings=[FindingFaker(id=FINDING_ID)],
            vulnerabilities=[
                VulnerabilityFaker(
                    id=ACCEPTED_VULN_ID,
                    finding_id=FINDING_ID,
                ),
                VulnerabilityFaker(
                    id=REJECTED_VULN_ID,
                    finding_id=FINDING_ID,
                ),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_ADMIN),
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_HACKER),
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_REATTACKER),
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_USER),
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_GROUP_MANAGER),
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_VULNERABILITY_MANAGER),
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_RESOURCER),
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_REVIEWER),
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_CUSTOMER_MANAGER),
            ],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_ADMIN,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_HACKER,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_REATTACKER,
                    state=GroupAccessStateFaker(has_access=True, role="reattacker"),
                ),
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_USER,
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                ),
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_GROUP_MANAGER,
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_VULNERABILITY_MANAGER,
                    state=GroupAccessStateFaker(has_access=True, role="vulnerability_manager"),
                ),
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_RESOURCER,
                    state=GroupAccessStateFaker(has_access=True, role="resourcer"),
                ),
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_REVIEWER,
                    state=GroupAccessStateFaker(has_access=True, role="reviewer"),
                ),
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_CUSTOMER_MANAGER,
                    state=GroupAccessStateFaker(has_access=True, role="customer_manager"),
                ),
            ],
            organizations=[
                OrganizationFaker(id="org1"),
            ],
        ),
    )
)
async def test_handle_vulnerabilities_acceptance_without_request(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", "test-group"],
            [require_attribute, "has_asm", "test-group"],
        ],
    )

    # No previous treatment request in this test's mocks
    with raises(AcceptanceNotRequested):
        await mutate(
            _=None,
            info=info,
            finding_id=FINDING_ID,
            justification="test justification",
            accepted_vulnerabilities=[ACCEPTED_VULN_ID],
            rejected_vulnerabilities=[REJECTED_VULN_ID],
        )


@parametrize(
    args=["email"],
    cases=[
        [EMAIL_FLUIDATTACKS_GROUP_MANAGER],
        [EMAIL_FLUIDATTACKS_VULNERABILITY_MANAGER],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(organization_id="org1"),
            ],
            findings=[FindingFaker(id=FINDING_ID)],
            vulnerabilities=[
                VulnerabilityFaker(
                    id=ACCEPTED_VULN_ID,
                    finding_id=FINDING_ID,
                    treatment=Treatment(
                        modified_date=DATE_2019,
                        status=TreatmentStatus.ACCEPTED,
                        acceptance_status=AcceptanceStatus.SUBMITTED,
                        assigned="developer@entry.com",
                    ),
                ),
                VulnerabilityFaker(
                    id=REJECTED_VULN_ID,
                    finding_id=FINDING_ID,
                    treatment=Treatment(
                        modified_date=DATE_2019,
                        status=TreatmentStatus.ACCEPTED,
                        acceptance_status=AcceptanceStatus.SUBMITTED,
                        assigned="developer@entry.com",
                    ),
                ),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_ADMIN),
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_HACKER),
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_REATTACKER),
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_USER),
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_GROUP_MANAGER),
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_VULNERABILITY_MANAGER),
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_RESOURCER),
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_REVIEWER),
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_CUSTOMER_MANAGER),
            ],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_ADMIN,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_HACKER,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_REATTACKER,
                    state=GroupAccessStateFaker(has_access=True, role="reattacker"),
                ),
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_USER,
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                ),
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_GROUP_MANAGER,
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_VULNERABILITY_MANAGER,
                    state=GroupAccessStateFaker(has_access=True, role="vulnerability_manager"),
                ),
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_RESOURCER,
                    state=GroupAccessStateFaker(has_access=True, role="resourcer"),
                ),
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_REVIEWER,
                    state=GroupAccessStateFaker(has_access=True, role="reviewer"),
                ),
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_CUSTOMER_MANAGER,
                    state=GroupAccessStateFaker(has_access=True, role="customer_manager"),
                ),
            ],
            organizations=[
                OrganizationFaker(id="org1"),
            ],
        ),
    )
)
async def test_handle_vulnerabilities_acceptance_vuln_not_found(email: str) -> None:
    nonexistent_vuln_id = "6f023c26-5x10-4ded-aa27-xx563c2206ax"
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", "test-group"],
            [require_attribute, "has_asm", "test-group"],
        ],
    )

    with raises(VulnNotFound):
        await mutate(
            _=None,
            info=info,
            finding_id=FINDING_ID,
            justification="test justification",
            accepted_vulnerabilities=[nonexistent_vuln_id],
            rejected_vulnerabilities=[""],
        )


@parametrize(
    args=["email"],
    cases=[
        [EMAIL_FLUIDATTACKS_HACKER],
        [EMAIL_FLUIDATTACKS_REATTACKER],
        [EMAIL_FLUIDATTACKS_RESOURCER],
        [EMAIL_FLUIDATTACKS_REVIEWER],
        [EMAIL_FLUIDATTACKS_CUSTOMER_MANAGER],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(organization_id="org1"),
            ],
            findings=[FindingFaker(id=FINDING_ID)],
            vulnerabilities=[
                VulnerabilityFaker(
                    id=ACCEPTED_VULN_ID,
                    finding_id=FINDING_ID,
                    treatment=Treatment(
                        modified_date=DATE_2019,
                        status=TreatmentStatus.ACCEPTED,
                        acceptance_status=AcceptanceStatus.SUBMITTED,
                        assigned="developer@entry.com",
                    ),
                ),
                VulnerabilityFaker(
                    id=REJECTED_VULN_ID,
                    finding_id=FINDING_ID,
                    treatment=Treatment(
                        modified_date=DATE_2019,
                        status=TreatmentStatus.ACCEPTED,
                        acceptance_status=AcceptanceStatus.SUBMITTED,
                        assigned="developer@entry.com",
                    ),
                ),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_ADMIN),
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_HACKER),
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_REATTACKER),
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_USER),
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_GROUP_MANAGER),
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_VULNERABILITY_MANAGER),
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_RESOURCER),
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_REVIEWER),
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_CUSTOMER_MANAGER),
            ],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_ADMIN,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_HACKER,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_REATTACKER,
                    state=GroupAccessStateFaker(has_access=True, role="reattacker"),
                ),
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_USER,
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                ),
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_GROUP_MANAGER,
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_VULNERABILITY_MANAGER,
                    state=GroupAccessStateFaker(has_access=True, role="vulnerability_manager"),
                ),
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_RESOURCER,
                    state=GroupAccessStateFaker(has_access=True, role="resourcer"),
                ),
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_REVIEWER,
                    state=GroupAccessStateFaker(has_access=True, role="reviewer"),
                ),
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_CUSTOMER_MANAGER,
                    state=GroupAccessStateFaker(has_access=True, role="customer_manager"),
                ),
            ],
            organizations=[
                OrganizationFaker(id="org1"),
            ],
        ),
    )
)
async def test_handle_vulnerabilities_acceptance_access_denied(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", "test-group"],
            [require_attribute, "has_asm", "test-group"],
        ],
    )

    with raises(GraphQLError, match="Access denied"):
        await mutate(
            _=None,
            info=info,
            finding_id=FINDING_ID,
            justification="test justification",
            accepted_vulnerabilities=[ACCEPTED_VULN_ID],
            rejected_vulnerabilities=[REJECTED_VULN_ID],
        )
