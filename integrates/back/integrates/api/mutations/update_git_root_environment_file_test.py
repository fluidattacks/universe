from integrates.api.mutations.update_git_root_environment_file import (
    mutate,
)
from integrates.custom_utils.groups import get_group
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.groups.types import GroupFile
from integrates.db_model.roots.enums import RootEnvironmentUrlStateStatus, RootEnvironmentUrlType
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute_internal,
    require_login,
)
from integrates.resources import (
    domain as resources_domain,
)
from integrates.roots.domain import format_environment_id
from integrates.testing.aws import (
    IntegratesAws,
    IntegratesDynamodb,
)
from integrates.testing.fakers import (
    GitRootFaker,
    GitRootStateFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    RootEnvironmentUrlFaker,
    RootEnvironmentUrlStateFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import Mock, mocks

from .payloads.types import (
    SimplePayload,
)

EMAIL_TEST = "customer_manager@fluidattacks.com"
GROUP_NAME = "group1"
ROOT_ID = "root1"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1", name="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="customer_manager")],
            groups=[
                GroupFaker(
                    name=GROUP_NAME,
                    organization_id="org1",
                    files=[
                        GroupFile(
                            description="client apk test file",
                            file_name="test.apk",
                            modified_by=EMAIL_TEST,
                        )
                    ],
                )
            ],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL_TEST,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(has_access=True, role="customer_manager"),
                )
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    organization_name="org1",
                    id=ROOT_ID,
                    state=GitRootStateFaker(nickname="back"),
                )
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(
                    url="test.apk",
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=RootEnvironmentUrlStateFaker(
                        status=RootEnvironmentUrlStateStatus.CREATED,
                        url_type=RootEnvironmentUrlType.APK,
                    ),
                )
            ],
        ),
    ),
    others=[
        Mock(resources_domain, "search_file", "async", True),
        Mock(resources_domain, "remove_file", "async", None),
    ],
)
async def test_update_git_root_environment_file() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )

    result = await mutate(
        _=None,
        info=info,
        group_name=GROUP_NAME,
        root_id=ROOT_ID,
        url_id=format_environment_id("test.apk"),
        file_name="test_new_name.apk",
        old_file_name="test.apk",
    )

    assert result == SimplePayload(success=True)

    new_loaders = get_new_context()
    group = await get_group(new_loaders, GROUP_NAME)

    assert group
    assert group.files
    assert group.files[0].file_name == "test_new_name.apk"
