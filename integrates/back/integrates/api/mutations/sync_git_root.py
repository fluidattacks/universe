from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_exceptions import CloningCouldNotBeQueued, InvalidGitRoot
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.roots.types import (
    GitRoot,
    RootRequest,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_login,
    require_service_white,
)
from integrates.roots import (
    domain as roots_domain,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("syncGitRoot")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_service_white,
)
async def mutate(_parent: None, info: GraphQLResolveInfo, **kwargs: str) -> SimplePayload:
    user_info: dict[str, str] = await sessions_domain.get_jwt_content(info.context)
    user_email: str = user_info["user_email"]
    loaders: Dataloaders = info.context.loaders
    group_name = kwargs["group_name"]
    root = await loaders.root.load(RootRequest(group_name, kwargs["root_id"]))
    if not isinstance(root, GitRoot):
        raise InvalidGitRoot()

    queued_job = await roots_domain.queue_sync_git_roots(
        loaders=loaders,
        roots=(root,),
        group_name=root.group_name,
        force=True,
        modified_by=user_email,
        queue_on_batch=True,
        should_queue_machine=True,
        should_queue_sbom=False,
    )
    if queued_job is None:
        raise CloningCouldNotBeQueued()

    logs_utils.cloudwatch_log(
        info.context,
        "Queued a sync clone for the root",
        extra={
            "group_name": group_name,
            "root_id": root.id,
            "root_nickname": root.state.nickname,
            "user_email": user_email,
            "log_type": "Security",
        },
    )

    return SimplePayload(success=True)
