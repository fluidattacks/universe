from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_exceptions import (
    PermissionDenied,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.custom_utils import (
    validations as validations_utils,
)
from integrates.custom_utils.groups import get_group
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.groups.types import (
    GroupMetadataToUpdate,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_is_not_under_review,
    require_login,
)
from integrates.groups import (
    domain as groups_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("updateGroupDisambiguation")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_asm,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    group_name: str,
    **kwargs: str,
) -> SimplePayload:
    loaders: Dataloaders = info.context.loaders
    group_name = group_name.lower()
    group = await get_group(loaders, group_name)
    try:
        disambiguation = validations_utils.validate_markdown(kwargs.get("disambiguation", ""))
        await groups_domain.update_metadata(
            group_name=group_name,
            metadata=GroupMetadataToUpdate(
                disambiguation=disambiguation,
            ),
            organization_id=group.organization_id,
        )
    except PermissionDenied:
        logs_utils.cloudwatch_log(
            info.context,
            "Unauthorized role attempted to update the group disambiguation",
            extra={
                "group_name": group_name,
                "log_type": "Security",
            },
        )
        raise

    return SimplePayload(success=True)
