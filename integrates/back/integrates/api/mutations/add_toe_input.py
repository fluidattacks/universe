from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.api.types import (
    APP_EXCEPTIONS,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.custom_utils import (
    roots as roots_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.roots.types import (
    GitRoot,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_login,
)
from integrates.sessions import (
    domain as sessions_domain,
)
from integrates.toe.inputs import (
    domain as toe_inputs_domain,
)
from integrates.toe.inputs.types import (
    ToeInputAttributesToAdd,
)
from integrates.toe.inputs.validations import (
    validate_environment_url,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("addToeInput")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_asm,
)
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    component: str,
    entry_point: str,
    environment_id: str,
    group_name: str,
    root_id: str,
    **_kwargs: None,
) -> SimplePayload:
    try:
        loaders: Dataloaders = info.context.loaders
        user_data = await sessions_domain.get_jwt_content(info.context)
        user_email = user_data["user_email"]
        root = await roots_utils.get_root(loaders, root_id, group_name)
        environment = ""
        if isinstance(root, GitRoot):
            await validate_environment_url(
                loaders=loaders,
                root_id=root_id,
                group_name=group_name,
                url_id=environment_id,
            )
            environment = environment_id

        await toe_inputs_domain.add(
            loaders=loaders,
            group_name=group_name,
            component=component,
            entry_point=entry_point,
            environment_id=environment,
            root_id=root_id,
            attributes=ToeInputAttributesToAdd(
                be_present=True,
                has_vulnerabilities=False,
                seen_first_time_by=user_email,
            ),
        )
        logs_utils.cloudwatch_log(
            info.context,
            "Added toe input in the group",
            extra={"group_name": group_name, "log_type": "Security"},
        )
    except APP_EXCEPTIONS:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to add toe input in the group",
            extra={"group_name": group_name, "log_type": "Security"},
        )
        raise

    return SimplePayload(success=True)
