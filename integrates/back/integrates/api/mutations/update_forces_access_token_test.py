from integrates.api.mutations.update_forces_access_token import mutate
from integrates.dataloaders import get_new_context
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute_internal,
    require_is_not_under_review,
)
from integrates.sessions.domain import decode_token
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize

from .payloads.types import UpdateAccessTokenPayload

GROUP_NAME = "group1"
ORG_ID = "ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db"


@parametrize(
    args=["email"],
    cases=[
        ["user@gmail.com"],
        ["group_manager@gmail.com"],
        ["vulnerability_manager@gmail.com"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            stakeholders=[
                StakeholderFaker(email="user@gmail.com"),
                StakeholderFaker(email="group_manager@gmail.com"),
                StakeholderFaker(email="vulnerability_manager@gmail.com"),
                StakeholderFaker(email="forces.group1@fluidattacks.com"),
            ],
            group_access=[
                GroupAccessFaker(
                    email="user@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                    group_name=GROUP_NAME,
                ),
                GroupAccessFaker(
                    email="group_manager@gmail.com",
                    state=GroupAccessStateFaker(
                        has_access=True,
                        role="group_manager",
                    ),
                    group_name=GROUP_NAME,
                ),
                GroupAccessFaker(
                    email="vulnerability_manager@gmail.com",
                    state=GroupAccessStateFaker(
                        has_access=True,
                        role="vulnerability_manager",
                    ),
                    group_name=GROUP_NAME,
                ),
                GroupAccessFaker(
                    email="forces.group1@fluidattacks.com",
                    state=GroupAccessStateFaker(
                        has_access=True,
                        role="service_forces",
                    ),
                    group_name=GROUP_NAME,
                ),
            ],
        ),
    )
)
async def test_update_forces_access_token_v1(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
            [enforce_group_level_auth_async],
        ],
    )
    loaders = get_new_context()

    mutation_result: UpdateAccessTokenPayload = await mutate(
        _parent=None,
        info=info,
        group_name=GROUP_NAME,
    )

    forces_user = await loaders.stakeholder.load(
        "forces.group1@fluidattacks.com",
    )

    assert forces_user
    assert mutation_result.success
    assert decode_token(mutation_result.session_jwt)["user_email"] == forces_user.email
