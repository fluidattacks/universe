from datetime import (
    datetime,
)
from operator import (
    attrgetter,
)
from typing import (
    TypedDict,
    Unpack,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.api.types import (
    APP_EXCEPTIONS,
)
from integrates.class_types.types import (
    Item,
)
from integrates.custom_exceptions import (
    InvalidSortsParameters,
    ToeLinesNotFound,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.toe_lines.types import (
    SortsSuggestion,
    ToeLineRequest,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_login,
)
from integrates.roots import (
    domain as roots_domain,
)
from integrates.toe.lines import (
    domain as toe_lines_domain,
)
from integrates.toe.lines.types import (
    ToeLinesAttributesToUpdate,
)
from integrates.toe.lines.validations import (
    validate_sort_risk_level,
    validate_sort_suggestions,
    validate_sorts_risk_level_date,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


class UpdateToeLinesSortsArgs(TypedDict):
    group_name: str
    root_nickname: str
    filename: str
    sorts_risk_level: int | None
    sorts_risk_level_date: datetime | None
    sorts_suggestions: list[Item] | None


def _format_sorts_suggestions(
    suggestions: list[Item],
) -> list[SortsSuggestion]:
    unordered = [
        SortsSuggestion(
            finding_title=item["finding_title"],
            probability=item["probability"],
        )
        for item in suggestions
    ]
    return sorted(unordered, key=attrgetter("probability"), reverse=True)


@MUTATION.field("updateToeLinesSorts")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_asm,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    **kwargs: Unpack[UpdateToeLinesSortsArgs],
) -> SimplePayload:
    loaders: Dataloaders = info.context.loaders
    group_name = kwargs["group_name"]
    sorts_risk_level = kwargs.get("sorts_risk_level")
    sorts_suggestions = kwargs.get("sorts_suggestions")
    sorts_risk_level_date = kwargs.get("sorts_risk_level_date")

    if not sorts_risk_level and not sorts_suggestions and not sorts_risk_level_date:
        raise InvalidSortsParameters.new()

    if sorts_risk_level:
        validate_sort_risk_level(sorts_risk_level)

    if sorts_risk_level_date:
        validate_sorts_risk_level_date(sorts_risk_level_date)

    sorts_suggestions_formatted = None
    if sorts_suggestions:
        sorts_suggestions_formatted = _format_sorts_suggestions(sorts_suggestions)
        validate_sort_suggestions(sorts_suggestions_formatted)

    try:
        roots = await loaders.group_roots.load(group_name)
        root_id = roots_domain.get_root_id_by_nickname(
            kwargs["root_nickname"],
            tuple(roots),
            only_git_roots=True,
        )
        toe_lines = await loaders.toe_lines.load(
            ToeLineRequest(
                filename=kwargs["filename"],
                group_name=group_name,
                root_id=root_id,
            ),
        )
        if toe_lines is None:
            raise ToeLinesNotFound()

        await toe_lines_domain.update(
            toe_lines,
            ToeLinesAttributesToUpdate(
                sorts_risk_level=sorts_risk_level,
                sorts_risk_level_date=sorts_risk_level_date,
                sorts_suggestions=sorts_suggestions_formatted,
            ),
        )
        logs_utils.cloudwatch_log(
            info.context,
            "Updated sorts parameters in the group",
            extra={"group_name": group_name, "log_type": "Security"},
        )
    except APP_EXCEPTIONS:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to update sorts parameters in the group",
            extra={"group_name": group_name, "log_type": "Security"},
        )
        raise

    return SimplePayload(success=True)
