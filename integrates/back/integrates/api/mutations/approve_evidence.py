from graphql import (
    GraphQLResolveInfo,
)

from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_login,
)
from integrates.findings import (
    domain as findings_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("approveEvidence")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
)
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    evidence_id: str,
    finding_id: str,
) -> SimplePayload:
    loaders: Dataloaders = info.context.loaders
    await findings_domain.approve_evidence(
        loaders=loaders,
        evidence_id=evidence_id,
        finding_id=finding_id,
    )
    logs_utils.cloudwatch_log(
        info.context,
        "Approved evidence in the finding",
        extra={
            "evidence_id": evidence_id,
            "finding_id": finding_id,
            "log_type": "Security",
        },
    )

    return SimplePayload(success=True)
