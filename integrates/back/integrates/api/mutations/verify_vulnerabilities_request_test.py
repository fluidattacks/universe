from datetime import datetime

from integrates.api.mutations.verify_vulnerabilities_request import mutate
from integrates.custom_exceptions import InvalidVulnsNumber
from integrates.db_model.vulnerabilities.enums import VulnerabilityVerificationStatus
from integrates.db_model.vulnerabilities.types import VulnerabilityVerification
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_asm,
    require_attribute,
    require_finding_access,
    require_login,
    require_report_vulnerabilities,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    FindingFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    StakeholderFaker,
    VulnerabilityFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

ADMIN_EMAIL = "admin@gmail.com"
HACKER_EMAIL = "hacker@fluidattacks.com"
GROUP_NAME = "test-group"
REVIEWER_EMAIL = "reviewer@fluidattacks.com"
RESOURCER_EMAIL = "resourcer@fluidattacks.com"
EXECUTIVE_EMAIL = "executive@fluidattacks.com"


@parametrize(
    args=["email"],
    cases=[
        [ADMIN_EMAIL],
        [HACKER_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=ADMIN_EMAIL),
                StakeholderFaker(email=HACKER_EMAIL),
            ],
            findings=[
                FindingFaker(id="3c475384-834c-47b0-ac71-a41a022e401c", group_name=GROUP_NAME)
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=HACKER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
            ],
        )
    )
)
async def test_verify_vulnerabilities_request_max_vulns(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
            [require_report_vulnerabilities],
            [require_attribute, "can_report_vulnerabilities", GROUP_NAME],
            [require_finding_access],
        ],
    )
    with raises(InvalidVulnsNumber):
        await mutate(
            _=None,
            info=info,
            finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
            justification="Testing max vulnerabilities",
            open_vulnerabilities=["vuln_id_" + str(i) for i in range(100)],
            closed_vulnerabilities=["vuln_id_" + str(i) for i in range(100, 151)],
        )


@parametrize(
    args=["email"],
    cases=[
        [RESOURCER_EMAIL],
        [EXECUTIVE_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=RESOURCER_EMAIL),
                StakeholderFaker(email=EXECUTIVE_EMAIL),
            ],
            findings=[
                FindingFaker(id="3c475384-834c-47b0-ac71-a41a022e401c", group_name=GROUP_NAME)
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=RESOURCER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="resourcer"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=EXECUTIVE_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="executive"),
                ),
            ],
        )
    )
)
async def test_verify_vulnerabilities_request_unauthorized(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
            [require_report_vulnerabilities],
            [require_attribute, "can_report_vulnerabilities", GROUP_NAME],
            [require_finding_access],
        ],
    )
    with raises(Exception, match="Access denied"):
        await mutate(
            _=None,
            info=info,
            finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
            justification="Testing unauthorized access",
            open_vulnerabilities=["vuln_1"],
            closed_vulnerabilities=["vuln_2"],
        )


@parametrize(
    args=["email"],
    cases=[
        [ADMIN_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=ADMIN_EMAIL),
            ],
            findings=[
                FindingFaker(id="3c475384-834c-47b0-ac71-a41a022e401c", group_name=GROUP_NAME)
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    id="vuln_1",
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                    verification=VulnerabilityVerification(
                        modified_date=datetime.now(),
                        status=VulnerabilityVerificationStatus.REQUESTED,
                    ),
                ),
            ],
        )
    )
)
async def test_verify_vulnerabilities_request_success(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
            [require_report_vulnerabilities],
            [require_attribute, "can_report_vulnerabilities", GROUP_NAME],
            [require_finding_access],
        ],
    )
    result = await mutate(
        _=None,
        info=info,
        finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
        justification="Testing empty vulnerability lists",
        open_vulnerabilities=["vuln_1"],
        closed_vulnerabilities=[],
    )
    assert result.success
