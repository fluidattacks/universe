from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.api.types import (
    APP_EXCEPTIONS,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_continuous,
    require_finding_access,
    require_is_not_under_review,
    require_login,
    require_report_vulnerabilities,
)
from integrates.findings.domain import (
    request_vulnerabilities_verification,
)
from integrates.sessions import (
    domain as sessions_domain,
)
from integrates.unreliable_indicators.enums import (
    EntityDependency,
)
from integrates.unreliable_indicators.operations import (
    update_unreliable_indicators_by_deps,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("requestVulnerabilitiesVerification")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_continuous,
    require_asm,
    require_report_vulnerabilities,
    require_finding_access,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    finding_id: str,
    justification: str,
    vulnerabilities: list[str],
) -> SimplePayload:
    try:
        user_info = await sessions_domain.get_jwt_content(info.context)

        await request_vulnerabilities_verification(
            loaders=info.context.loaders,
            finding_id=finding_id,
            user_info=user_info,
            justification=justification,
            vulnerability_ids=set(vulnerabilities),
            no_unsolved_events=True,
        )
        await update_unreliable_indicators_by_deps(
            EntityDependency.request_vulnerabilities_verification,
            finding_ids=[finding_id],
            vulnerability_ids=vulnerabilities,
        )
        logs_utils.cloudwatch_log(
            info.context,
            "Request vuln verification in the finding",
            extra={
                "finding_id": finding_id,
                "vuln_ids": vulnerabilities,
                "log_type": "Security",
            },
        )
    except APP_EXCEPTIONS:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to request vuln verification in the finding",
            extra={
                "finding_id": finding_id,
                "vuln_ids": vulnerabilities,
                "log_type": "Security",
            },
        )
        raise

    return SimplePayload(success=True)
