from integrates.api.mutations import (
    send_new_enrolled_user,
)
from integrates.api.mutations.send_new_enrolled_user import mutate
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import raises

TEST_EMAIL = "user@gmail.com"
GROUP_NAME = "group1"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME)],
            stakeholders=[
                StakeholderFaker(email=TEST_EMAIL, role="group_manager"),
            ],
            group_access=[
                GroupAccessFaker(
                    email=TEST_EMAIL,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="group_manager"),
                ),
            ],
        ),
    ),
    others=[Mock(send_new_enrolled_user, "send_new_enrolled_user_mail", "async", None)],
)
async def test_send_assigned_notification_success() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=TEST_EMAIL,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    payload = await mutate(
        _parent=None,
        info=info,
        group_name=GROUP_NAME,
    )

    assert payload.success


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME)],
            stakeholders=[
                StakeholderFaker(email=TEST_EMAIL),
            ],
            group_access=[
                GroupAccessFaker(
                    email=TEST_EMAIL,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="customer_manager"),
                ),
            ],
        ),
    ),
)
async def test_send_assigned_notification_exception() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=TEST_EMAIL,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    with raises(Exception, match="Access denied"):
        await mutate(
            _parent=None,
            info=info,
            group_name=GROUP_NAME,
        )
