from integrates.api.mutations.download_billing_file import mutate
from integrates.context import CI_COMMIT_REF_NAME
from integrates.custom_exceptions import ErrorDownloadingFile
from integrates.db_model.organizations.types import (
    OrganizationDocuments,
    OrganizationPaymentMethods,
)
from integrates.decorators import enforce_organization_level_auth_async, require_login
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationAccessFaker,
    OrganizationAccessStateFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import raises

ORG_ID = "ORG#6b110f25-cd46-e94c-2bad-b9e182cf91a8"
BILL_ID = "test-payment-method-id"
ORG_NAME = "orgtest"
GROUP_NAME = "testgroup"
EMAIL_TEST = "testuser@orgtest.com"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(
                    id=ORG_ID,
                    name=ORG_NAME,
                    payment_methods=[
                        OrganizationPaymentMethods(
                            id=BILL_ID,
                            business_name="testname",
                            email=EMAIL_TEST,
                            country="Colombia",
                            state="Cundinamarca",
                            city="Bogota",
                            documents=OrganizationDocuments(rut=None, tax_id=None),
                        )
                    ],
                )
            ],
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, enrolled=True)],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=EMAIL_TEST,
                    state=OrganizationAccessStateFaker(
                        has_access=True, role="organization_manager"
                    ),
                ),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=EMAIL_TEST,
                    state=GroupAccessStateFaker(has_access=True, role="organization_manager"),
                ),
            ],
        ),
    ),
)
async def test_download_billing_file() -> None:
    # Arrange
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[[require_login], [enforce_organization_level_auth_async]],
    )

    # Act
    result = await mutate(
        _=None,
        info=info,
        payment_method_id=BILL_ID,
        organization_id=ORG_ID,
        file_name="test-bill-file.pdf",
    )

    # Assert
    assert result
    assert result.success
    assert (result.url).startswith(
        f"https://s3.amazonaws.com/integrates.dev/{CI_COMMIT_REF_NAME}/resources/billing/orgtest/testname/test-bill-file.pdf"
    )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(
                    id=ORG_ID,
                    name=ORG_NAME,
                    payment_methods=[
                        OrganizationPaymentMethods(
                            id=BILL_ID,
                            business_name="testname",
                            email=EMAIL_TEST,
                            country="Colombia",
                            state="Cundinamarca",
                            city="Bogota",
                            documents=OrganizationDocuments(rut=None, tax_id=None),
                        )
                    ],
                )
            ],
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, enrolled=True)],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=EMAIL_TEST,
                    state=OrganizationAccessStateFaker(
                        has_access=True, role="organization_manager"
                    ),
                ),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=EMAIL_TEST,
                    state=GroupAccessStateFaker(has_access=True, role="organization_manager"),
                ),
            ],
        ),
    ),
)
async def test_download_billing_file_fail() -> None:
    # Arrange
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[[require_login], [enforce_organization_level_auth_async]],
    )

    # Assert
    with raises(ErrorDownloadingFile):
        await mutate(
            _=None,
            info=info,
            payment_method_id="non-existent-payment-method-id",
            organization_id=ORG_ID,
            file_name="test-bill-file.pdf",
        )
