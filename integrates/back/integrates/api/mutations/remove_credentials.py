from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.batch import (
    dal as batch_dal,
)
from integrates.batch.enums import (
    Action,
    IntegratesBatchQueue,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.credentials.types import (
    CredentialsRequest,
    OauthGithubSecret,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_organization_level_auth_async,
    require_login,
)
from integrates.oauth.github import (
    remove_oauth_access,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("removeCredentials")
@concurrent_decorators(
    require_login,
    enforce_organization_level_auth_async,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    credentials_id: str,
    organization_id: str,
) -> SimplePayload:
    loaders: Dataloaders = info.context.loaders
    user_data = await sessions_domain.get_jwt_content(info.context)
    user_email = user_data["user_email"]
    credential = await loaders.credentials.load(
        CredentialsRequest(
            id=credentials_id,
            organization_id=organization_id,
        ),
    )
    await orgs_domain.remove_credentials(
        loaders=loaders,
        organization_id=organization_id,
        credentials_id=credentials_id,
    )

    logs_utils.cloudwatch_log(
        info.context,
        "Removed credentials in the organization",
        extra={
            "organization_id": organization_id,
            "log_type": "Security",
        },
    )

    if credential and isinstance(credential.secret, OauthGithubSecret):
        await remove_oauth_access(
            credential=credential,
        )

    await batch_dal.put_action(
        action=Action.UPDATE_ORGANIZATION_REPOSITORIES,
        queue=IntegratesBatchQueue.SMALL,
        additional_info={"credentials_id": credentials_id},
        entity=organization_id.lower().lstrip("org#"),
        attempt_duration_seconds=7200,
        subject=user_email,
    )

    return SimplePayload(success=True)
