from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.billing import (
    domain as billing_domain,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_organization_level_auth_async,
    require_login,
)
from integrates.organizations import (
    utils as orgs_utils,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("addCreditCardPaymentMethod")
@concurrent_decorators(
    require_login,
    enforce_organization_level_auth_async,
)
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    *,
    organization_id: str,
    make_default: bool,
    payment_method_id: str,
    billing_email: str,
    billed_groups: list[str],
    country: str | None = None,
    business_name: str | None = None,
    business_id: str | None = None,
) -> SimplePayload:
    loaders: Dataloaders = info.context.loaders
    organization = await orgs_utils.get_organization(loaders, organization_id)
    user_info = await sessions_domain.get_jwt_content(info.context)
    user_email = user_info["user_email"]
    success = await billing_domain.create_credit_card_payment_method(
        org=organization,
        user_email=user_email,
        make_default=make_default,
        payment_method_id=payment_method_id,
        country=country,
        billed_groups=billed_groups or None,
        billing_email=billing_email,
        business_name=business_name,
        business_id=business_id,
        loaders=loaders,
    )

    logs_utils.cloudwatch_log(
        info.context,
        "Added Credit Card Payment Method"
        if success
        else "Attempt to add Credit Card Payment Method",
        extra={
            "organization_id": organization_id,
            "user_email": user_email,
            "log_type": "Security",
        },
    )

    return SimplePayload(success=success)
