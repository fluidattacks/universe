from integrates.api.mutations.update_secret import mutate
from integrates.dataloaders import get_new_context
from integrates.db_model.roots.types import RootEnvironmentSecretsRequest
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute_internal,
    require_login,
)
from integrates.testing.aws import (
    IntegratesAws,
    IntegratesDynamodb,
    RootEnvironmentSecretsToUpdate,
)
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    RootEnvironmentUrlFaker,
    SecretFaker,
    SecretStateFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises
from integrates.verify.enums import ResourceType

GROUP_NAME = "group_name"
CUSTOMER_MANAGER = "customer_manager@gmail.com"
USER = "user@gmail.com"
HACKER = "hacker@gmail.com"
REATTACKER = "reattacker@gmail.com"
ARCHITECT = "architect@gmail.com"


@parametrize(
    args=["user_email", "new_value", "key"],
    cases=[
        [CUSTOMER_MANAGER, "new_api_key", "api_key_customer_manager"],
        [USER, "new_api_key_user", "api_key_user"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME)],
            stakeholders=[
                StakeholderFaker(email=CUSTOMER_MANAGER),
                StakeholderFaker(email=USER),
            ],
            group_access=[
                GroupAccessFaker(
                    email=CUSTOMER_MANAGER,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="customer_manager"),
                ),
                GroupAccessFaker(
                    email=USER,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="user"),
                ),
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(id="url1", root_id="root1", group_name=GROUP_NAME)
            ],
            root_environment_secrets=[
                RootEnvironmentSecretsToUpdate(
                    group_name=GROUP_NAME,
                    resource_id="url1",
                    secret=SecretFaker(
                        value="key_123456",
                        key="api_key_customer_manager",
                        state=SecretStateFaker(owner=CUSTOMER_MANAGER),
                    ),
                ),
                RootEnvironmentSecretsToUpdate(
                    group_name=GROUP_NAME,
                    resource_id="url1",
                    secret=SecretFaker(
                        value="key_123456", key="api_key_user", state=SecretStateFaker(owner=USER)
                    ),
                ),
            ],
        ),
    )
)
async def test_update_env_url_secret_success(user_email: str, new_value: str, key: str) -> None:
    loaders = get_new_context()
    secrets = await loaders.environment_secrets.load(
        RootEnvironmentSecretsRequest(url_id="url1", group_name=GROUP_NAME)
    )
    user_secret_value_initial = next(
        secret.value for secret in secrets if secret.state.owner == user_email
    )
    assert user_secret_value_initial == "key_123456"  # noqa: S105

    info = GraphQLResolveInfoFaker(
        user_email=user_email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )
    await mutate(
        _parent=None,
        info=info,
        description="Update secret",
        group_name=GROUP_NAME,
        key=key,
        resource_id="url1",
        resource_type=ResourceType.URL,
        value=new_value,
    )
    updated_secrets = await get_new_context().environment_secrets.load(
        RootEnvironmentSecretsRequest(url_id="url1", group_name=GROUP_NAME)
    )
    assert len(updated_secrets) == len(secrets)
    user_secret_value = next(
        secret.value for secret in updated_secrets if secret.state.owner == user_email
    )
    assert user_secret_value == new_value


@parametrize(
    args=["user_email"],
    cases=[[HACKER], [REATTACKER], [ARCHITECT]],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME)],
            stakeholders=[
                StakeholderFaker(email=HACKER),
                StakeholderFaker(email=REATTACKER),
                StakeholderFaker(email=ARCHITECT),
            ],
            group_access=[
                GroupAccessFaker(
                    email=HACKER,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="hacker"),
                ),
                GroupAccessFaker(
                    email=REATTACKER,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="reattacker"),
                ),
                GroupAccessFaker(
                    email=ARCHITECT,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="architect"),
                ),
            ],
        ),
    )
)
async def test_update_secret_access_denied(user_email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=user_email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )
    with raises(Exception, match="Access denied"):
        await mutate(
            _parent=None,
            info=info,
            description="Update secret",
            group_name=GROUP_NAME,
            key="key",
            resource_id="url1",
            resource_type=ResourceType.URL,
            value="new_value",
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME)],
            stakeholders=[
                StakeholderFaker(email=CUSTOMER_MANAGER),
                StakeholderFaker(email=USER),
            ],
            group_access=[
                GroupAccessFaker(
                    email=CUSTOMER_MANAGER,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="customer_manager"),
                ),
                GroupAccessFaker(
                    email=USER,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="user"),
                ),
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(id="url1", root_id="root1", group_name=GROUP_NAME)
            ],
            root_environment_secrets=[
                RootEnvironmentSecretsToUpdate(
                    group_name=GROUP_NAME,
                    resource_id="url1",
                    secret=SecretFaker(
                        value="key_123456",
                        key="api_key_customer_manager",
                        state=SecretStateFaker(owner=CUSTOMER_MANAGER),
                    ),
                ),
            ],
        ),
    )
)
async def test_update_secret_failed_owner() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=USER,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )
    with raises(Exception, match="Access denied"):
        await mutate(
            _parent=None,
            info=info,
            description="Update secret",
            group_name=GROUP_NAME,
            key="api_key_customer_manager",
            resource_id="url1",
            resource_type=ResourceType.URL,
            value="new_value",
        )
