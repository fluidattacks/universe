from integrates.api.mutations.add_toe_input import (
    mutate,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.toe_inputs.types import (
    RootToeInputsRequest,
)
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute,
    require_login,
)
from integrates.roots.domain import format_environment_id
from integrates.testing.aws import (
    IntegratesAws,
    IntegratesDynamodb,
)
from integrates.testing.fakers import (
    GitRootFaker,
    GitRootStateFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    RootEnvironmentUrlFaker,
    RootEnvironmentUrlStateFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import (
    mocks,
)

from .payloads.types import (
    SimplePayload,
)

EMAIL_TEST = "hacker@fluidattacks.com"
GROUP_NAME = "group1"
ROOT_ID = "root1"

TEST_URL = "https://myapp.com"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_TEST, role="user"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=EMAIL_TEST,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(
                        nickname="back",
                    ),
                ),
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(
                    url=TEST_URL,
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=RootEnvironmentUrlStateFaker(modified_by="hacker@fluidattacks.com"),
                ),
            ],
        ),
    )
)
async def test_add_toe_ports_success() -> None:
    loaders: Dataloaders = get_new_context()
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )

    environment_id = format_environment_id(TEST_URL)
    result = await mutate(
        _parent=None,
        info=info,
        component=TEST_URL,
        entry_point="login",
        environment_id=environment_id,
        group_name=GROUP_NAME,
        root_id=ROOT_ID,
    )
    assert result == SimplePayload(success=True)

    root_toe_inputs = await loaders.root_toe_inputs.load_nodes(
        RootToeInputsRequest(group_name=GROUP_NAME, root_id=ROOT_ID),
    )

    assert len(root_toe_inputs) == 1
    assert root_toe_inputs[0].component == TEST_URL
    assert root_toe_inputs[0].entry_point == "login"
    assert root_toe_inputs[0].environment_id == environment_id
    assert not root_toe_inputs[0].state.has_vulnerabilities
    assert root_toe_inputs[0].state.seen_first_time_by == EMAIL_TEST
