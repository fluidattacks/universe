from integrates.api.mutations.download_file import mutate
from integrates.context import CI_COMMIT_REF_NAME
from integrates.custom_exceptions import ErrorDownloadingFile
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute,
    require_attribute_internal,
    require_login,
)
from integrates.s3 import operations as s3_ops
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationAccessFaker,
    OrganizationAccessStateFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import raises

ORG_ID = "ORG#6b110f25-cd46-e94c-2bad-b9e182cf91a8"
ORG_NAME = "orgtest"
GROUP_NAME = "testgroup"
EMAIL_TEST = "testuser@orgtest.com"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, enrolled=True)],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=EMAIL_TEST,
                    state=OrganizationAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=EMAIL_TEST,
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
        ),
    ),
)
async def test_download_file() -> None:
    # Arrange
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )

    # Act
    result = await mutate(
        _=None,
        info=info,
        group_name=GROUP_NAME,
        files_data_input="test.txt",
    )

    # Assert
    assert result
    assert result.success
    assert (result.url).startswith(
        f"https://s3.amazonaws.com/integrates.dev/{CI_COMMIT_REF_NAME}/resources/testgroup/test.txt"
    )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, enrolled=True)],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=EMAIL_TEST,
                    state=OrganizationAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=EMAIL_TEST,
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
        ),
    ),
    others=[Mock(s3_ops, "sign_url", "async", "")],
)
async def test_download_file_fail() -> None:
    # Arrange
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )

    # Assert
    with raises(ErrorDownloadingFile):
        await mutate(
            _=None,
            info=info,
            group_name=GROUP_NAME,
            files_data_input="test.txt",
        )
