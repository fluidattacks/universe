from time import (
    time,
)

from graphql import (
    GraphQLError,
)
from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_exceptions import (
    InvalidDraftConsult,
    PermissionDenied,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.custom_utils import (
    validations_deco,
)
from integrates.custom_utils.findings import get_finding, is_finding_released
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.finding_comments.enums import (
    CommentType,
)
from integrates.db_model.finding_comments.types import (
    FindingComment,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_advanced,
    require_asm,
    require_is_not_under_review,
    require_login,
)
from integrates.findings import (
    domain as findings_domain,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    AddConsultPayload,
)
from .schema import (
    MUTATION,
)


@require_advanced
async def add_finding_consult(*, info: GraphQLResolveInfo, **parameters: str) -> str:
    return await _add_finding_consult(info, **parameters)


async def add_finding_observation(*, info: GraphQLResolveInfo, **parameters: str) -> str:
    return await _add_finding_consult(info, **parameters)


@validations_deco.validate_fields_deco(["content"])
async def _add_finding_consult(info: GraphQLResolveInfo, **parameters: str) -> str:
    param_type = parameters.get("type", "").lower()
    user_data = await sessions_domain.get_jwt_content(info.context)
    user_email = user_data["user_email"]
    finding_id = str(parameters.get("finding_id"))
    loaders: Dataloaders = info.context.loaders
    finding = await get_finding(loaders, finding_id)
    content = parameters["content"]
    if param_type == "consult" and not is_finding_released(finding):
        raise InvalidDraftConsult()

    comment_id = str(round(time() * 1000))
    comment_data = FindingComment(
        finding_id=finding_id,
        id=comment_id,
        comment_type=CommentType.OBSERVATION if param_type != "consult" else CommentType.COMMENT,
        parent_id=str(parameters.get("parent_comment")),
        creation_date=datetime_utils.get_utc_now(),
        full_name=" ".join([user_data["first_name"], user_data["last_name"]]),
        content=content,
        email=user_email,
    )
    try:
        await findings_domain.add_comment(
            loaders=loaders,
            user_email=user_email,
            comment_data=comment_data,
            finding_id=finding_id,
            group_name=finding.group_name,
        )
    except PermissionDenied:
        logs_utils.cloudwatch_log(
            info.context,
            "Unauthorized role attempted to add observation",
            extra={
                "finding_id": finding_id,
                "log_type": "Security",
            },
        )
        raise GraphQLError("Access denied") from None

    logs_utils.cloudwatch_log(
        info.context,
        "Added comment in the finding",
        extra={
            "comment_id": comment_id,
            "finding_id": finding_id,
            "log_type": "Security",
        },
    )

    return comment_id


@MUTATION.field("addFindingConsult")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_asm,
)
async def mutate(_: None, info: GraphQLResolveInfo, **parameters: str) -> AddConsultPayload:
    if parameters.get("type", "").lower() == "observation":
        comment_id = await add_finding_observation(info=info, **parameters)
    else:
        comment_id = await add_finding_consult(info=info, **parameters)

    return AddConsultPayload(success=True, comment_id=comment_id)
