from datetime import (
    datetime,
)
from typing import (
    TypedDict,
    Unpack,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.api.types import (
    APP_EXCEPTIONS,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.db_model.roots.get import (
    get_secret_by_key,
)
from integrates.db_model.roots.types import (
    Secret,
    SecretState,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    enforce_owner,
    require_is_not_under_review,
    require_login,
)
from integrates.roots import (
    update as roots_update,
)
from integrates.sessions import (
    domain as sessions_domain,
)
from integrates.verify.enums import (
    AuthenticationLevel,
    ResourceType,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


class UpdateSecretArgs(TypedDict):
    description: str | None
    group_name: str
    key: str
    resource_id: str
    resource_type: ResourceType
    value: str


@enforce_owner(AuthenticationLevel.GROUP)
async def _update_secret(
    secret: Secret,
    info: GraphQLResolveInfo,
    resource_id: str,
    resource_type: ResourceType,
    group_name: str,
) -> None:
    try:
        await roots_update.update_secret(
            group_name=group_name,
            resource_id=resource_id,
            resource_type=resource_type,
            new_secret=secret,
            loaders=info.context.loaders,
        )

        logs_utils.cloudwatch_log(
            info.context,
            "Updated secret",
            extra={
                "group_name": group_name,
                "secret_key": secret.key,
                "resource_type": resource_type,
                "log_type": "Security",
            },
        )

    except APP_EXCEPTIONS:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to update a secret",
            extra={
                "group_name": group_name,
                "secret_key": secret.key,
                "resource_type": resource_type,
                "log_type": "Security",
            },
        )
        raise


@MUTATION.field("updateSecret")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_is_not_under_review,
)
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    **kwargs: Unpack[UpdateSecretArgs],
) -> SimplePayload:
    user_info = await sessions_domain.get_jwt_content(info.context)
    key = kwargs["key"]
    new_value = kwargs["value"]
    group_name = kwargs["group_name"]
    resource_id = kwargs["resource_id"]
    resource_type = kwargs["resource_type"]
    new_description = kwargs.get("description")

    current_secret = await get_secret_by_key(
        secret_key=key,
        group_name=group_name,
        resource_id=resource_id,
        resource_type=resource_type,
    )

    await _update_secret(
        Secret(
            key=key,
            value=new_value,
            created_at=current_secret.created_at,
            state=SecretState(
                owner=current_secret.state.owner,
                description=new_description,
                modified_by=user_info["user_email"],
                modified_date=datetime.now(),
            ),
        ),
        info,
        resource_id,
        resource_type,
        group_name=group_name,
    )

    return SimplePayload(success=True)
