from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.api.types import (
    APP_EXCEPTIONS,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_is_not_under_review,
    require_login,
)
from integrates.jobs_orchestration.request_sbom import (
    request_sbom_file,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("requestSbomFile")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_asm,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    group_name: str,
    sbom_format: str,
    file_format: str,
    root_nicknames: list[str] | None = None,
    uris_with_credentials: list[dict[str, str | None]] | None = None,
) -> SimplePayload:
    try:
        loaders = get_new_context()
        user_info = await sessions_domain.get_jwt_content(info.context)

        await request_sbom_file(
            loaders=loaders,
            user_email=user_info["user_email"],
            group_name=group_name,
            root_nicknames=root_nicknames,
            uris_with_credentials=uris_with_credentials,
            file_format=file_format,
            sbom_format_str=sbom_format,
        )
        logs_utils.cloudwatch_log(
            info.context,
            "Request sbom file for root",
            extra={
                "roots": root_nicknames,
                "format": sbom_format,
                "file_format": file_format,
            },
        )
    except APP_EXCEPTIONS:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to request sbom file for root",
            extra={
                "root": root_nicknames,
                "format": sbom_format,
                "file_format": file_format,
            },
        )
        raise

    return SimplePayload(success=True)
