from integrates.api.mutations.request_sbom_file import mutate
from integrates.batch.types import PutActionResult
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute,
    require_attribute_internal,
    require_login,
)
from integrates.jobs_orchestration import request_sbom
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GitRootFaker,
    GitRootStateFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import parametrize, raises

EMAIL_TEST = "user@clientapp.com"
GROUP_NAME = "group1"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="user")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=EMAIL_TEST,
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                ),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME, id="root1", state=GitRootStateFaker(nickname="back")
                )
            ],
        ),
    ),
    others=[Mock(request_sbom, "queue_sbom_job", "async", PutActionResult(success=True))],
)
async def test_request_sbom_file() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", GROUP_NAME],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )
    mutation_result = await mutate(
        _=None,
        info=info,
        group_name=GROUP_NAME,
        sbom_format="fluid",
        file_format="json",
        root_nicknames=["back"],
        uris_with_credentials=None,
    )

    assert mutation_result.success is True


@parametrize(
    args=["email"],
    cases=[
        ["hacker@fluidattacks.com"],
        ["resourcer@test.test"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[
                StakeholderFaker(email="hacker@fluidattacks.com", role="hacker"),
                StakeholderFaker(email="resourcer@test.test", role="resourcer"),
            ],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email="hacker@fluidattacks.com",
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email="resourcer@test.test",
                    state=GroupAccessStateFaker(has_access=True, role="resourcer"),
                ),
            ],
        ),
    )
)
async def test_request_sbom_file_access_denied(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", GROUP_NAME],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )

    with raises(Exception, match="Access denied"):
        await mutate(
            _=None,
            info=info,
            group_name=GROUP_NAME,
            sbom_format="fluid",
            file_format="json",
            root_nicknames=["back"],
            uris_with_credentials=None,
        )
