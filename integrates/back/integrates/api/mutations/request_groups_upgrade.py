from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.decorators import (
    require_login,
)
from integrates.groups import (
    domain as groups_domain,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("requestGroupsUpgrade")
@require_login
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    **kwargs: list[str],
) -> SimplePayload:
    user_info = await sessions_domain.get_jwt_content(info.context)
    email = user_info["user_email"]
    group_names: list[str] = kwargs["group_names"]
    await groups_domain.request_upgrade(
        loaders=info.context.loaders,
        email=email,
        group_names=group_names,
    )

    return SimplePayload(success=True)
