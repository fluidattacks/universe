from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_login,
    require_service_black,
)
from integrates.roots import (
    domain as roots_domain,
)
from integrates.roots.types import (
    UrlRootAttributesToAdd,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    AddRootPayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("addUrlRoot")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_service_black,
    require_is_not_under_review,
)
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    group_name: str,
    nickname: str,
    url: str,
) -> AddRootPayload:
    loaders: Dataloaders = info.context.loaders
    user_info: dict[str, str] = await sessions_domain.get_jwt_content(info.context)
    user_email: str = user_info["user_email"]
    group_name = group_name.lower()

    root = await roots_domain.add_url_root(
        loaders=loaders,
        attributes_to_add=UrlRootAttributesToAdd(
            nickname=nickname,
            url=url,
        ),
        group_name=group_name,
        user_email=user_email,
    )
    logs_utils.cloudwatch_log(
        info.context,
        "Added URL root",
        extra={
            "group_name": group_name,
            "log_type": "Security",
        },
    )

    return AddRootPayload(root_id=root.id, success=True)
