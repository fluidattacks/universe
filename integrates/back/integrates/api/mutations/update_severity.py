from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.api.types import (
    APP_EXCEPTIONS,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.custom_utils.findings import get_finding
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_finding_access,
    require_login,
)
from integrates.findings import (
    domain as findings_domain,
)
from integrates.unreliable_indicators.enums import (
    EntityDependency,
)
from integrates.unreliable_indicators.operations import (
    update_unreliable_indicators_by_deps,
)

from .payloads.types import (
    SimpleFindingPayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("updateSeverity")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_asm,
    require_finding_access,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    finding_id: str,
    cvss_vector: str,
    cvss_4_vector: str = "",
    **_kwargs: None,
) -> SimpleFindingPayload:
    try:
        loaders: Dataloaders = info.context.loaders
        finding = await get_finding(loaders, finding_id)
        vulnerabilities = await findings_domain.get_open_vulnerabilities(loaders, finding_id)

        await findings_domain.update_severity(loaders, finding_id, cvss_vector, cvss_4_vector)
        await update_unreliable_indicators_by_deps(
            EntityDependency.update_severity,
            finding_ids=[finding_id],
            vulnerability_ids=[vuln.id for vuln in vulnerabilities],
        )
        logs_utils.cloudwatch_log(
            info.context,
            "Updated severity in the finding",
            extra={"finding_id": finding_id, "log_type": "Security"},
        )
    except APP_EXCEPTIONS:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to update severity in the finding",
            extra={"finding_id": finding_id, "log_type": "Security"},
        )
        raise

    loaders.finding.clear(finding_id)
    finding = await get_finding(loaders, finding_id)

    return SimpleFindingPayload(finding=finding, success=True)
