from typing import Any

import integrates
from integrates.api.mutations.payloads.types import SignPostUrlsPayload
from integrates.api.mutations.sign_post_url import mutate
from integrates.custom_exceptions import ErrorUploadingFileS3
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_asm,
    require_attribute,
    require_attribute_internal,
    require_is_not_under_review,
    require_login,
)
from integrates.resources import domain as resources_domain
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import parametrize, raises

DEFAULT_DECORATORS: list[list[Any]] = [
    [require_login],
    [require_is_not_under_review],
    [enforce_group_level_auth_async],
    [require_asm],
    [require_attribute_internal, "is_under_review", "test_group"],
    [require_attribute_internal, "is_under_review", ""],
    [require_attribute, "has_asm", "test_group"],
]

GROUP_NAME = "test_group"

# Allowed
ADMIN = "admin@gmail.com"
CUSTOMER_MANAGER = "customer_manager@gmail.com"
GROUP_MANAGER = "group_manager@gmail.com"
USER = "user@gmail.com"
VULNERABILITY_MANAGER = "vulnerability_manager@gmail.com"

# Unauthorized
HACKER = "hacker@gmail.com"
REATTACKER = "reattacker@gmail.com"
ARCHITECT = "architect@gmail.com"


@parametrize(args=["user_email"], cases=[[HACKER], [REATTACKER], [ARCHITECT]])
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME)],
            stakeholders=[
                StakeholderFaker(email=HACKER),
                StakeholderFaker(email=REATTACKER),
                StakeholderFaker(email=ARCHITECT),
            ],
            group_access=[
                GroupAccessFaker(
                    email=HACKER, group_name=GROUP_NAME, state=GroupAccessStateFaker(role="hacker")
                ),
                GroupAccessFaker(
                    email=REATTACKER,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="reattacker"),
                ),
                GroupAccessFaker(
                    email=ARCHITECT,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="architect"),
                ),
            ],
        )
    )
)
async def test_sign_post_url_access_denied(user_email: str) -> None:
    info = GraphQLResolveInfoFaker(user_email=user_email, decorators=DEFAULT_DECORATORS)
    parameters = {
        "files_data_input": [
            {
                "file_name": "test_file.txt",
                "upload_date": "2023-10-01",
            }
        ]
    }

    with raises(Exception, match="Access denied"):
        await mutate(_=None, info=info, group_name=GROUP_NAME, **parameters)


@parametrize(args=["user_email"], cases=[[ADMIN]])
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME)],
            stakeholders=[StakeholderFaker(email=ADMIN)],
            group_access=[
                GroupAccessFaker(
                    email=ADMIN, group_name=GROUP_NAME, state=GroupAccessStateFaker(role="admin")
                )
            ],
        )
    ),
    others=[Mock(resources_domain, "upload_file", "async", {})],
)
async def test_sign_post_url_error_uploading(user_email: str) -> None:
    info = GraphQLResolveInfoFaker(user_email=user_email, decorators=DEFAULT_DECORATORS)
    parameters = {
        "files_data_input": [
            {
                "file_name": "test_file.txt",
                "upload_date": "2023-10-01",
            }
        ]
    }

    with raises(ErrorUploadingFileS3):
        await mutate(_=None, info=info, group_name=GROUP_NAME, **parameters)


@parametrize(
    args=["user_email"],
    cases=[[ADMIN], [CUSTOMER_MANAGER], [GROUP_MANAGER], [USER], [VULNERABILITY_MANAGER]],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME)],
            stakeholders=[
                StakeholderFaker(email=ADMIN),
                StakeholderFaker(email=CUSTOMER_MANAGER),
                StakeholderFaker(email=GROUP_MANAGER),
                StakeholderFaker(email=USER),
                StakeholderFaker(email=VULNERABILITY_MANAGER),
            ],
            group_access=[
                GroupAccessFaker(
                    email=ADMIN, group_name=GROUP_NAME, state=GroupAccessStateFaker(role="admin")
                ),
                GroupAccessFaker(
                    email=CUSTOMER_MANAGER,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="customer_manager"),
                ),
                GroupAccessFaker(
                    email=GROUP_MANAGER,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="group_manager"),
                ),
                GroupAccessFaker(
                    email=USER, group_name=GROUP_NAME, state=GroupAccessStateFaker(role="user")
                ),
                GroupAccessFaker(
                    email=VULNERABILITY_MANAGER,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="vulnerability_manager"),
                ),
            ],
        )
    ),
    others=[Mock(integrates.s3.operations, "FI_AWS_S3_PATH_PREFIX", "function", "")],
)
async def test_sign_post_url_success(
    user_email: str,
) -> None:
    info = GraphQLResolveInfoFaker(user_email=user_email, decorators=DEFAULT_DECORATORS)
    parameters = {
        "files_data_input": [
            {
                "file_name": "test_file.txt",
                "upload_date": "2023-10-01",
            }
        ]
    }
    result = await mutate(_=None, info=info, group_name=GROUP_NAME, **parameters)
    assert isinstance(result, SignPostUrlsPayload)
    assert result.success is True
    assert result.url["url"] == "https://s3.amazonaws.com/integrates.dev"
    assert result.url["fields"]["algorithm"] == "AWS4-HMAC-SHA256"
    assert result.url["fields"]["securitytoken"] == "test"
