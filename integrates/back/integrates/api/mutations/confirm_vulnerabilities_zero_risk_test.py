from typing import Any

from integrates.api.mutations.confirm_vulnerabilities_zero_risk import mutate
from integrates.dataloaders import get_new_context
from integrates.db_model.vulnerabilities.enums import VulnerabilityZeroRiskStatus
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute,
    require_login,
    require_report_vulnerabilities,
    require_request_zero_risk,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    FindingFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    StakeholderFaker,
    VulnerabilityFaker,
    VulnerabilityZeroRiskFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

FINDING_ID = "3c475384-834c-47b0-ac71-a41a022e401c"
VULN_ID_1 = "9e5c61be-aeef-467f-9368-699c3d322f9a"
VULN_ID_2 = "5b85acec-4811-43a8-bc7c-5486a787474d"

# authorized
ADMIN_EMAIL = "admin@gmail.com"
ARCHITECT_EMAIL = "architect@gmail.com"
REVIEWER_EMAIL = "reviewer@gmail.com"

# unauthorized
HACKER_EMAIL = "hacker@gmail.com"
REATTACKER_EMAIL = "reattacker@gmail.com"
USER_EMAIL = "user@gmail.com"
CUSTOMER_MANAGER_EMAIL = "customer_manager@gmail.com"
VULNERABILITY_MANAGER_EMAIL = "vulnerability_manager@gmail.com"
GROUP_MANAGER_EMAIL = "group_manager@gmail.com"
RESOURCER_EMAIL = "resourcer@gmail.com"

DEFAULT_DECORATORS: list[list[Any]] = [
    [require_request_zero_risk],
    [require_login],
    [require_report_vulnerabilities],
    [enforce_group_level_auth_async],
    [require_attribute, "can_request_zero_risk", "test-group"],
    [require_attribute, "can_report_vulnerabilities", "test-group"],
]


@parametrize(
    args=["email"],
    cases=[
        [HACKER_EMAIL],
        [REATTACKER_EMAIL],
        [USER_EMAIL],
        [GROUP_MANAGER_EMAIL],
        [VULNERABILITY_MANAGER_EMAIL],
        [RESOURCER_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker()],
            stakeholders=[
                StakeholderFaker(email=HACKER_EMAIL),
                StakeholderFaker(email=REATTACKER_EMAIL),
                StakeholderFaker(email=USER_EMAIL),
                StakeholderFaker(email=GROUP_MANAGER_EMAIL),
                StakeholderFaker(email=VULNERABILITY_MANAGER_EMAIL),
                StakeholderFaker(email=CUSTOMER_MANAGER_EMAIL),
                StakeholderFaker(email=RESOURCER_EMAIL),
            ],
            group_access=[
                GroupAccessFaker(
                    email=HACKER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
                GroupAccessFaker(
                    email=REATTACKER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="reattacker"),
                ),
                GroupAccessFaker(
                    email=USER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                ),
                GroupAccessFaker(
                    email=GROUP_MANAGER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
                GroupAccessFaker(
                    email=VULNERABILITY_MANAGER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="vulnerability_manager"),
                ),
                GroupAccessFaker(
                    email=CUSTOMER_MANAGER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="customer_manager"),
                ),
                GroupAccessFaker(
                    email=RESOURCER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="resourcer"),
                ),
            ],
            findings=[FindingFaker(id=FINDING_ID)],
        )
    )
)
async def test_confirm_vulnerabilities_zero_risk_access_denied(
    email: str,
) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        first_name="Joe",
        last_name="Doe",
        decorators=DEFAULT_DECORATORS,
    )
    with raises(Exception, match="Access denied"):
        await mutate(
            _=None,
            info=info,
            finding_id=FINDING_ID,
            justification="some generic justification",
            vulnerabilities=[VULN_ID_1],
        )


@parametrize(
    args=["email"],
    cases=[
        [ADMIN_EMAIL],
        [ARCHITECT_EMAIL],
        [REVIEWER_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker()],
            stakeholders=[
                StakeholderFaker(email=ADMIN_EMAIL),
                StakeholderFaker(email=ARCHITECT_EMAIL),
                StakeholderFaker(email=REVIEWER_EMAIL),
            ],
            group_access=[
                GroupAccessFaker(
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
                GroupAccessFaker(
                    email=ARCHITECT_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="architect"),
                ),
                GroupAccessFaker(
                    email=REVIEWER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="reviewer"),
                ),
            ],
            findings=[FindingFaker(id=FINDING_ID)],
            vulnerabilities=[
                VulnerabilityFaker(
                    id=VULN_ID_1,
                    finding_id=FINDING_ID,
                    zero_risk=VulnerabilityZeroRiskFaker(
                        status=VulnerabilityZeroRiskStatus.REQUESTED
                    ),
                ),
                VulnerabilityFaker(
                    id=VULN_ID_2,
                    finding_id=FINDING_ID,
                    zero_risk=VulnerabilityZeroRiskFaker(
                        status=VulnerabilityZeroRiskStatus.REQUESTED
                    ),
                ),
            ],
        )
    )
)
async def test_confirm_vulnerabilities_zero_risk_success(
    email: str,
) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        first_name="Joe",
        last_name="Doe",
        decorators=DEFAULT_DECORATORS,
    )
    vulns = [VULN_ID_1, VULN_ID_2]
    await mutate(
        _=None,
        info=info,
        finding_id=FINDING_ID,
        justification="some generic justification",
        vulnerabilities=vulns,
    )
    loaders = get_new_context()
    for vuln_id in vulns:
        vuln = await loaders.vulnerability.load(vuln_id)
        assert vuln is not None
        assert vuln.zero_risk is not None
        assert vuln.zero_risk.status == VulnerabilityZeroRiskStatus.CONFIRMED
        assert vuln.zero_risk.modified_by == email
