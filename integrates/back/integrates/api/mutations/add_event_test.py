from graphql.error.graphql_error import GraphQLError

from integrates.api.mutations.add_event import mutate
from integrates.api.resolvers.group.events import resolve
from integrates.custom_utils.constants import SCHEDULE_CLONE_GROUPS_ROOTS_EMAIL
from integrates.db_model.events.enums import EventType
from integrates.db_model.events.types import Event
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute,
    require_attribute_internal,
    require_is_not_under_review,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    DATE_2024,
    GitRootFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import raises


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(organization_id="org1")],
            organizations=[OrganizationFaker(id="org1")],
            group_access=[
                GroupAccessFaker(
                    email=SCHEDULE_CLONE_GROUPS_ROOTS_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                )
            ],
            roots=[GitRootFaker(id="test-root")],
            stakeholders=[StakeholderFaker(email=SCHEDULE_CLONE_GROUPS_ROOTS_EMAIL)],
        ),
    )
)
async def test_no_duplication_of_automatic_events() -> None:
    test_group = GroupFaker(organization_id="org1")
    info = GraphQLResolveInfoFaker(
        user_email=SCHEDULE_CLONE_GROUPS_ROOTS_EMAIL,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", "test-group"],
            [require_attribute, "has_asm", "test-group"],
        ],
    )

    events: list[Event] = await resolve(test_group, info=info)
    assert len(events) == 0

    await mutate(
        None,
        info=info,
        detail="504 Bad Gateway",
        group_name=test_group.name,
        event_date=DATE_2024,
        event_type="CLONING_ISSUES",
        root_id="test-root",
    )
    info.context.loaders.group_events.clear_all()
    events2: list[Event] = await resolve(test_group, info=info)
    # Cloning issues event
    assert len(events2) == 1
    assert events2[0].type == EventType.CLONING_ISSUES

    await mutate(
        None,
        info=info,
        detail="504 Bad Gateway",
        group_name=test_group.name,
        event_date=DATE_2024,
        event_type="CLONING_ISSUES",
        root_id="test-root",
    )
    info.context.loaders.group_events.clear_all()
    events3: list[Event] = await resolve(test_group, info=info)
    # Same cloning issues event
    assert events2 == events3


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(organization_id="org1")],
            organizations=[OrganizationFaker(id="org1")],
            group_access=[
                GroupAccessFaker(
                    email="admin@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                )
            ],
            roots=[GitRootFaker(id="test-root")],
            stakeholders=[StakeholderFaker(email="admin@gmail.com", role="admin")],
        ),
    )
)
async def test_add_event_access_denied() -> None:
    email = "outsider@gmail.com"
    test_group = GroupFaker(organization_id="org1")
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", "test-group"],
            [require_attribute, "has_asm", "test-group"],
        ],
    )

    with raises(GraphQLError, match="Access denied"):
        await mutate(
            None,
            info=info,
            detail="504 Bad Gateway",
            group_name=test_group.name,
            event_date=DATE_2024,
            event_type="CLONING_ISSUES",
            root_id="test-root",
        )
