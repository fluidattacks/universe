from integrates import batch, jobs_orchestration, roots
from integrates.api.mutations.add_git_environment import mutate
from integrates.batch.dal.get import get_actions_by_name
from integrates.batch.enums import Action, SkimsBatchQueue
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.roots.types import RootEnvironmentUrlsRequest
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute,
    require_attribute_internal,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GitRootFaker,
    GitRootStateFaker,
    GraphQLResolveInfoFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import Mock, mocks

from .payloads.types import AddEnvironmentUrlPayload

EMAIL_TEST = "admin@fluidattacks.com"
GROUP_NAME = "group1"
ROOT_ID = "root1"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_TEST, role="admin"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(
                        nickname="back",
                    ),
                ),
            ],
        ),
    ),
    others=[
        Mock(
            jobs_orchestration.jobs,
            "generate_batch_action_config",
            "async",
            (
                {"roots": ["root1"], "roots_config_files": ["root1.yaml"]},
                "123456",
                SkimsBatchQueue.SMALL,
            ),
        ),
        Mock(batch.dal.put, "put_action_to_batch", "async", "123456"),
        Mock(roots.utils, "send_mail_environment", "async", None),
    ],
)
async def test_add_git_environment_url_success() -> None:
    loaders: Dataloaders = get_new_context()
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_service_white", GROUP_NAME],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )

    result = await mutate(
        _parent=None,
        info=info,
        cloud_name=None,
        group_name=GROUP_NAME,
        root_id=ROOT_ID,
        url="https://nice-env-test.com",
        url_type="URL",
        use_egress=None,
        use_vpn=None,
        use_ztna=None,
        is_production=True,
    )

    root_env_urls = await loaders.root_environment_urls.load(
        RootEnvironmentUrlsRequest(root_id=ROOT_ID, group_name=GROUP_NAME),
    )
    url_ids = [env_url.id for env_url in root_env_urls]
    assert len(url_ids) == 1
    assert result == AddEnvironmentUrlPayload(success=True, url_id=url_ids[0])

    pending_executions = await get_actions_by_name(
        action_name=Action.EXECUTE_MACHINE,
        entity="group1",
    )
    assert len(pending_executions) == 1


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_TEST, role="admin"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(
                        nickname="back",
                    ),
                ),
            ],
        ),
    ),
)
async def test_add_git_environment_url_failed() -> None:
    """Test that CSPM environments are not successfully added when using this mutation"""
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_service_white", GROUP_NAME],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )

    result = await mutate(
        _parent=None,
        info=info,
        cloud_name=None,
        group_name=GROUP_NAME,
        root_id=ROOT_ID,
        url="arn:aws:iam::205810638802:role/dev",
        url_type="CSPM",
        use_egress=None,
        use_vpn=None,
        use_ztna=None,
        is_production=True,
    )

    assert result == AddEnvironmentUrlPayload(success=False, url_id="")


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_TEST, role="admin"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(
                        nickname="back",
                    ),
                ),
            ],
        ),
    ),
    others=[
        Mock(
            jobs_orchestration.jobs,
            "generate_batch_action_config",
            "async",
            (
                {"roots": ["root1"], "roots_config_files": ["root1.yaml"]},
                "123456",
                SkimsBatchQueue.SMALL,
            ),
        ),
        Mock(batch.dal.put, "put_action_to_batch", "async", "123456"),
        Mock(roots.utils, "send_mail_environment", "async", None),
    ],
)
async def test_add_git_environment_url_machine() -> None:
    loaders: Dataloaders = get_new_context()
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_service_white", GROUP_NAME],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )

    result = await mutate(
        _parent=None,
        info=info,
        cloud_name=None,
        group_name=GROUP_NAME,
        root_id=ROOT_ID,
        url="https://nice-env-test.com",
        url_type="URL",
        use_egress=None,
        use_vpn=None,
        use_ztna=None,
        is_production=True,
    )

    root_env_urls = await loaders.root_environment_urls.load(
        RootEnvironmentUrlsRequest(root_id=ROOT_ID, group_name=GROUP_NAME),
    )
    url_ids = [env_url.id for env_url in root_env_urls]
    assert len(url_ids) == 1
    assert result == AddEnvironmentUrlPayload(success=True, url_id=url_ids[0])

    pending_executions = await get_actions_by_name(
        action_name=Action.EXECUTE_MACHINE,
        entity="group1",
    )
    assert len(pending_executions) == 1
