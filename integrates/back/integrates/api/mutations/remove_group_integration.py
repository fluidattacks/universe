from typing import (
    Literal,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils.groups import get_group
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.groups.types import (
    GroupMetadataToUpdate,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_is_not_under_review,
)
from integrates.groups import (
    domain as groups_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("removeGroupIntegration")
@concurrent_decorators(
    enforce_group_level_auth_async,
    require_is_not_under_review,
)
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    *,
    group_name: str,
    integration: Literal["AZURE_DEVOPS", "GITLAB"],
) -> SimplePayload:
    loaders: Dataloaders = info.context.loaders
    group = await get_group(loaders, group_name)
    if integration == "AZURE_DEVOPS":
        await groups_domain.update_metadata(
            group_name=group_name,
            metadata=GroupMetadataToUpdate(azure_issues=None),
            organization_id=group.organization_id,
        )
    elif integration == "GITLAB":
        await groups_domain.update_metadata(
            group_name=group_name,
            metadata=GroupMetadataToUpdate(gitlab_issues=None),
            organization_id=group.organization_id,
        )
    return SimplePayload(success=True)
