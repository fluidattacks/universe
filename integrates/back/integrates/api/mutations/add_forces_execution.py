from graphql.type.definition import (
    GraphQLResolveInfo,
)
from starlette.datastructures import (
    UploadFile,
)

from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_is_not_under_review,
)
from integrates.forces import (
    domain as forces_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("addForcesExecution")
@concurrent_decorators(
    enforce_group_level_auth_async,
    require_is_not_under_review,
)
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    group_name: str,
    json_log: str,
    log: UploadFile | None = None,
    **parameters: str,
) -> SimplePayload:
    await forces_domain.add_forces_execution(
        group_name=group_name,
        json_log=json_log,
        log=log,
        **parameters,
    )
    logs_utils.cloudwatch_log(
        info.context,
        "Created forces execution in the group",
        extra={
            "group_name": group_name,
            "log_type": "Security",
        },
    )
    return SimplePayload(success=True)
