from typing import Any

from integrates.api.mutations.add_event_consult import mutate
from integrates.custom_exceptions import InvalidCommentParent
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_advanced,
    require_asm,
    require_attribute,
    require_attribute_internal,
    require_is_not_under_review,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    EventFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import parametrize, raises

ADMIN_EMAIL = "admin@gmail.com"
REVIEWER_EMAIL = "reviewer@gmail.com"

CUSTOMER_MANAGER_EMAIL = "customer_manager@gmail.com"
HACKER_EMAIL = "hacker@gmail.com"
REATTACKER_EMAIL = "reattacker@gmail.com"
RESOURCER_EMAIL = "resourcer@gmail.com"
USER_EMAIL = "user@gmail.com"
ORG_MANAGER_EMAIL = "org_manager@gmail.com"
GROUP_MANAGER_EMAIL = "group_manager@gmail.com"
VULNERABILITY_MANAGER_EMAIL = "vulnerability_manager@gmail.com"

# General parameters
GROUP_NAME = "group3"
CONTENT = "This is a test comment"
EVENT_ID: str = "418900971"
PARENT_ID = "0"
INVALID_PARENT_ID = "123"
PARENT_COMMENT = "12345689"
ACCESS_DENIED_ERROR = "Access denied"


DEFAULT_DECORATORS: list[list[Any]] = [
    [require_login],
    [require_is_not_under_review],
    [enforce_group_level_auth_async],
    [require_asm],
    [require_advanced],
    [require_attribute, "has_advanced", "group3"],
    [require_attribute, "has_asm", "group3"],
    [require_attribute_internal, "is_under_review", "group3"],
    [enforce_group_level_auth_async],
]


@parametrize(
    args=["email"],
    cases=[
        [CUSTOMER_MANAGER_EMAIL],
        [HACKER_EMAIL],
        [REATTACKER_EMAIL],
        [RESOURCER_EMAIL],
        [USER_EMAIL],
        [GROUP_MANAGER_EMAIL],
        [VULNERABILITY_MANAGER_EMAIL],
        [REVIEWER_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=CUSTOMER_MANAGER_EMAIL),
                StakeholderFaker(email=HACKER_EMAIL),
                StakeholderFaker(email=REATTACKER_EMAIL),
                StakeholderFaker(email=RESOURCER_EMAIL),
                StakeholderFaker(email=USER_EMAIL),
                StakeholderFaker(email=GROUP_MANAGER_EMAIL),
                StakeholderFaker(email=VULNERABILITY_MANAGER_EMAIL),
                StakeholderFaker(email=REVIEWER_EMAIL),
            ],
            events=[
                EventFaker(
                    id=EVENT_ID,
                    group_name=GROUP_NAME,
                )
            ],
            groups=[
                GroupFaker(name=GROUP_NAME),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=CUSTOMER_MANAGER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="customer_manager"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=HACKER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=REATTACKER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="reattacker"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=USER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=GROUP_MANAGER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=VULNERABILITY_MANAGER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="vulnerability_manager"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=RESOURCER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="resourcer"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=REVIEWER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="reviewer"),
                ),
            ],
        )
    ),
    others=[Mock(logs_utils, "cloudwatch_log", "sync", None)],
)
async def test_add_event_consult_success(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=DEFAULT_DECORATORS,
    )
    await mutate(
        _=None,
        info=info,
        event_id=EVENT_ID,
        parent_comment=PARENT_ID,
        group_name=GROUP_NAME,
        content=CONTENT,
    )


@parametrize(
    args=["email"],
    cases=[
        [CUSTOMER_MANAGER_EMAIL],
        [HACKER_EMAIL],
        [REATTACKER_EMAIL],
        [RESOURCER_EMAIL],
        [USER_EMAIL],
        [ORG_MANAGER_EMAIL],
        [GROUP_MANAGER_EMAIL],
        [VULNERABILITY_MANAGER_EMAIL],
        [REVIEWER_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=ORG_MANAGER_EMAIL),
                StakeholderFaker(email=GROUP_MANAGER_EMAIL),
            ],
            events=[EventFaker(id=EVENT_ID)],
            groups=[
                GroupFaker(name=GROUP_NAME),
            ],
            group_access=[
                GroupAccessFaker(
                    email=ORG_MANAGER_EMAIL,
                    group_name="another_group",
                    state=GroupAccessStateFaker(has_access=True, role="organization_manager"),
                ),
                GroupAccessFaker(
                    email=GROUP_MANAGER_EMAIL,
                    group_name="another_group",
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
        )
    )
)
async def test_add_event_consult_access_denied(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=DEFAULT_DECORATORS,
    )
    with raises(Exception, match=ACCESS_DENIED_ERROR):
        await mutate(
            _=None,
            info=info,
            event_id=EVENT_ID,
            parent_id=PARENT_ID,
            group_name=GROUP_NAME,
            content=CONTENT,
        )


@parametrize(
    args=["email"],
    cases=[
        [ADMIN_EMAIL],
        [USER_EMAIL],
        [ORG_MANAGER_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=ADMIN_EMAIL),
                StakeholderFaker(email=USER_EMAIL),
                StakeholderFaker(email=ORG_MANAGER_EMAIL),
            ],
            events=[EventFaker(id=EVENT_ID)],
            groups=[
                GroupFaker(name=GROUP_NAME),
            ],
            group_access=[
                GroupAccessFaker(
                    email=USER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                ),
                GroupAccessFaker(
                    email=ORG_MANAGER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="organization_manager"),
                ),
                GroupAccessFaker(
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
        )
    )
)
async def test_add_event_comment(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=DEFAULT_DECORATORS,
    )
    with raises(Exception, match=ACCESS_DENIED_ERROR):
        await mutate(
            _=None,
            info=info,
            event_id=EVENT_ID,
            parent_id=PARENT_COMMENT,
            group_name=GROUP_NAME,
            content=CONTENT,
        )


@parametrize(
    args=["email"],
    cases=[
        [REVIEWER_EMAIL],
        [RESOURCER_EMAIL],
        [VULNERABILITY_MANAGER_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=REVIEWER_EMAIL),
                StakeholderFaker(email=RESOURCER_EMAIL),
                StakeholderFaker(email=VULNERABILITY_MANAGER_EMAIL),
            ],
            events=[EventFaker(id=EVENT_ID, group_name=GROUP_NAME)],
            groups=[
                GroupFaker(name=GROUP_NAME),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=VULNERABILITY_MANAGER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="vulnerability_manager"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=RESOURCER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="resourcer"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=REVIEWER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="reviewer"),
                ),
            ],
        )
    )
)
async def test_add_event_consult_not_parent_id(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=DEFAULT_DECORATORS,
    )
    with raises(InvalidCommentParent):
        await mutate(
            _=None,
            info=info,
            event_id=EVENT_ID,
            parent_comment=INVALID_PARENT_ID,
            group_name=GROUP_NAME,
            content=CONTENT,
        )
