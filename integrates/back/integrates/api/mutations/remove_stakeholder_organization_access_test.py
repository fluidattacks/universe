from integrates.api.mutations.remove_stakeholder_organization_access import mutate
from integrates.custom_exceptions import InvalidForcesStakeholder
from integrates.decorators import enforce_organization_level_auth_async
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    OrganizationAccessFaker,
    OrganizationAccessStateFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

ADMIN_EMAIL = "admin@gmail.com"
ORGANIZATION_ADMIN_EMAIL = "organization_admin@gmail.com"
USER_EMAIL = "user@gmail.com"
ORG_MANAGER_EMAIL = "organization_manager@fluidattacks.com"
VULNERABILITY_MANAGER_EMAIL = "vulnerability_manager@fluidattacks.com"
ORG_ID = "ORG#40f6da5f-4f21-4168-b8fa-9f2ce3d4dd93"


@parametrize(
    args=["email"],
    cases=[
        [ORGANIZATION_ADMIN_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=ADMIN_EMAIL),
                StakeholderFaker(email=ORGANIZATION_ADMIN_EMAIL),
                StakeholderFaker(email=USER_EMAIL),
            ],
            organizations=[
                OrganizationFaker(id=ORG_ID),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=ORGANIZATION_ADMIN_EMAIL,
                    state=OrganizationAccessStateFaker(
                        role="admin",
                    ),
                ),
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=USER_EMAIL,
                    state=OrganizationAccessStateFaker(
                        role="user",
                    ),
                ),
            ],
        )
    )
)
async def test_remove_stakeholder_organization_access(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[[enforce_organization_level_auth_async]],
    )
    result = await mutate(
        _parent=None,
        info=info,
        organization_id=ORG_ID,
        user_email=USER_EMAIL,
    )
    assert result.success


@parametrize(
    args=["email"],
    cases=[
        [ORG_MANAGER_EMAIL],
        [VULNERABILITY_MANAGER_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=ORG_MANAGER_EMAIL),
                StakeholderFaker(email=VULNERABILITY_MANAGER_EMAIL),
            ],
            organizations=[
                OrganizationFaker(id=ORG_ID, name="test_org"),
            ],
        )
    )
)
async def test_remove_stakeholder_organization_access_unauthorized(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[[enforce_organization_level_auth_async]],
    )
    with raises(Exception, match="Access denied"):
        await mutate(
            _parent=None,
            info=info,
            organization_id=ORG_ID,
            user_email=USER_EMAIL,
        )


@parametrize(
    args=["email"],
    cases=[
        [ADMIN_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=ADMIN_EMAIL),
            ],
            organizations=[
                OrganizationFaker(id=ORG_ID, name="test_org"),
            ],
        )
    )
)
async def test_remove_stakeholder_organization_access_nonexistent_user(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[[enforce_organization_level_auth_async]],
    )
    with raises(Exception):
        await mutate(
            _parent=None,
            info=info,
            organization_id=ORG_ID,
            user_email="nonexistent@email.com",
        )


@parametrize(
    args=["email"],
    cases=[
        [ADMIN_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=ADMIN_EMAIL),
            ],
        )
    )
)
async def test_remove_stakeholder_organization_access_invalid_org(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[[enforce_organization_level_auth_async]],
    )
    with raises(Exception):
        await mutate(
            _parent=None,
            info=info,
            organization_id="ORG#invalid-org-id",
            user_email=USER_EMAIL,
        )


@parametrize(
    args=["email"],
    cases=[
        [ADMIN_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=ADMIN_EMAIL),
                StakeholderFaker(email="forces.test@fluidattacks.com"),
            ],
            organizations=[
                OrganizationFaker(id=ORG_ID),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=ADMIN_EMAIL,
                    state=OrganizationAccessStateFaker(
                        role="admin",
                    ),
                ),
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email="forces.test@fluidattacks.com",
                ),
            ],
        )
    )
)
async def test_remove_stakeholder_organization_access_forces_user(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[[enforce_organization_level_auth_async]],
    )
    with raises(InvalidForcesStakeholder):
        await mutate(
            _parent=None,
            info=info,
            organization_id=ORG_ID,
            user_email="forces.test@fluidattacks.com",
        )
