from integrates.api.mutations.request_event_verification import (
    mutate,
)
from integrates.dataloaders import get_new_context
from integrates.db_model.event_comments.types import (
    EventCommentsRequest,
)
from integrates.db_model.events.enums import (
    EventStateStatus,
)
from integrates.db_model.events.types import (
    EventRequest,
)
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute,
    require_attribute_internal,
    require_login,
)
from integrates.testing.aws import (
    IntegratesAws,
    IntegratesDynamodb,
)
from integrates.testing.fakers import (
    EventFaker,
    EventStateFaker,
    GitRootFaker,
    GitRootStateFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks

EMAIL_TEST = "hacker@fluidattacks.com"
GROUP_NAME = "group1"
ROOT_ID = "root1"
EVENT_ID = "event_id_1"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="hacker")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL_TEST,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME, id=ROOT_ID, state=GitRootStateFaker(nickname="back")
                ),
            ],
            events=[
                EventFaker(
                    group_name=GROUP_NAME,
                    id=EVENT_ID,
                    state=EventStateFaker(status=EventStateStatus.CREATED),
                )
            ],
        )
    ),
)
async def test_request_event_verification() -> None:
    loaders = get_new_context()
    event = await loaders.event.load(EventRequest(event_id=EVENT_ID, group_name=GROUP_NAME))
    assert event

    event_comments = await loaders.event_comments.load(
        EventCommentsRequest(event_id=EVENT_ID, group_name=GROUP_NAME),
    )
    assert len(event_comments) == 0

    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", GROUP_NAME],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )
    result = await mutate(
        _=None,
        info=info,
        comments="Environment was updated",
        event_id=EVENT_ID,
        group_name=GROUP_NAME,
    )
    assert result.success is True
    new_loaders = get_new_context()
    event_after = await new_loaders.event.load(
        EventRequest(event_id=EVENT_ID, group_name=GROUP_NAME)
    )
    assert event_after

    event_comments_after = await new_loaders.event_comments.load(
        EventCommentsRequest(event_id=EVENT_ID, group_name=GROUP_NAME),
    )
    assert len(event_comments_after) == 1
    assert event_comments_after[0].content == "Environment was updated"
    assert event_after.state.comment_id == event_comments_after[0].id
