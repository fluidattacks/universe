import logging
import logging.config

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils.organizations import (
    get_organization_country,
)
from integrates.decorators import (
    require_corporate_email,
    require_login,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    AddOrganizationPayload,
)
from .schema import (
    MUTATION,
)

# Constants
TRANSACTIONS_LOGGER: logging.Logger = logging.getLogger("transactional")


@MUTATION.field("addOrganization")
@require_login
@require_corporate_email
async def mutate(_parent: None, info: GraphQLResolveInfo, **kwargs: str) -> AddOrganizationPayload:
    user_info = await sessions_domain.get_jwt_content(info.context)
    user_email = user_info["user_email"]
    country = get_organization_country(info.context)
    name = kwargs["name"]

    TRANSACTIONS_LOGGER.info(
        "User attempted to add a new organization",
        extra={
            "user_email": user_email,
            "organization_name": name,
        },
    )
    organization = await orgs_domain.add_organization(
        loaders=info.context.loaders,
        organization_name=name,
        email=user_email,
        country=country,
    )
    TRANSACTIONS_LOGGER.info(
        "Organization was successfully added",
        extra={
            "organization_name": organization.name,
            "organization_id": organization.id,
            "user_email,": user_email,
            "country": country,
        },
    )

    return AddOrganizationPayload(success=True, organization=organization)
