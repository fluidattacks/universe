from integrates.api.mutations.confirm_vulnerabilities import mutate
from integrates.custom_exceptions import PendingEvidenceDraft
from integrates.dataloaders import get_new_context
from integrates.db_model.findings.types import FindingEvidence, FindingEvidences
from integrates.db_model.vulnerabilities.enums import VulnerabilityStateStatus
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute,
    require_login,
    require_report_vulnerabilities,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    DATE_2024,
    FindingFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    StakeholderFaker,
    VulnerabilityFaker,
    VulnerabilityStateFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

HACKER_EMAIL = "hacker@gmail.com"
REATTACKER_EMAIL = "reattacker@gmail.com"
USER_EMAIL = "user@gmail.com"
CUSTOMER_MANAGER_EMAIL = "customer_manager@gmail.com"
VULNERABILITY_MANAGER_EMAIL = "vulnerability_manager@gmail.com"
GROUP_MANAGER = "group_manager@gmail.com"
RESOURCER_EMAIL = "resourcer@gmail.com"

ADMIN_EMAIL = "admin@gmail.com"
REVIEWER_EMAIL = "reviewer@gmail.com"

FINDING_ID = "3c475384-834c-47b0-ac71-a41a022e401c"
VULN_ID_1 = "be09edb7-cd5c-47ed-bee4-97c645acdce10"
VULN_ID_2 = "335681c3-a6b5-4eec-b828-0d56957ba7c5"
TEST_GROUP_NAME = "test-group"


@parametrize(
    args=["user", "vulnerabilities"],
    cases=[
        [
            HACKER_EMAIL,
            ["be09edb7-cd5c-47ed-bee4-97c645acdce10"],
        ],
        [
            REATTACKER_EMAIL,
            ["be09edb7-cd5c-47ed-bee4-97c645acdce10"],
        ],
        [
            USER_EMAIL,
            ["be09edb7-cd5c-47ed-bee4-97c645acdce10"],
        ],
        [
            GROUP_MANAGER,
            ["be09edb7-cd5c-47ed-bee4-97c645acdce10"],
        ],
        [
            VULNERABILITY_MANAGER_EMAIL,
            ["be09edb7-cd5c-47ed-bee4-97c645acdce10"],
        ],
        [
            CUSTOMER_MANAGER_EMAIL,
            ["be09edb7-cd5c-47ed-bee4-97c645acdce10"],
        ],
        [
            RESOURCER_EMAIL,
            ["be09edb7-cd5c-47ed-bee4-97c645acdce10"],
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=HACKER_EMAIL),
                StakeholderFaker(email=REATTACKER_EMAIL),
                StakeholderFaker(email=USER_EMAIL),
                StakeholderFaker(email=GROUP_MANAGER),
                StakeholderFaker(email=VULNERABILITY_MANAGER_EMAIL),
                StakeholderFaker(email=CUSTOMER_MANAGER_EMAIL),
                StakeholderFaker(email=RESOURCER_EMAIL),
            ],
            group_access=[
                GroupAccessFaker(
                    email=HACKER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
                GroupAccessFaker(
                    email=REATTACKER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="reattacker"),
                ),
                GroupAccessFaker(
                    email=USER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                ),
                GroupAccessFaker(
                    email=GROUP_MANAGER,
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
                GroupAccessFaker(
                    email=VULNERABILITY_MANAGER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="vulnerability_manager"),
                ),
                GroupAccessFaker(
                    email=CUSTOMER_MANAGER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="customer_manager"),
                ),
                GroupAccessFaker(
                    email=RESOURCER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="resourcer"),
                ),
            ],
            findings=[FindingFaker(id=FINDING_ID)],
            vulnerabilities=[VulnerabilityFaker(id=VULN_ID_1)],
        )
    )
)
async def test_mutate_access_denied(
    user: str,
    vulnerabilities: list[str],
) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=user,
        decorators=[
            [require_login],
            [require_report_vulnerabilities],
            [enforce_group_level_auth_async],
            [require_attribute, "can_report_vulnerabilities", TEST_GROUP_NAME],
        ],
    )
    with raises(Exception, match="Access denied"):
        await mutate(_=None, info=info, finding_id=FINDING_ID, vulnerabilities=vulnerabilities)


@parametrize(
    args=["email", "vulnerability_ids"],
    cases=[
        [
            ADMIN_EMAIL,
            ["be09edb7-cd5c-47ed-bee4-97c645acdce10"],
        ],
        [
            REVIEWER_EMAIL,
            ["be09edb7-cd5c-47ed-bee4-97c645acdce10"],
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            findings=[
                FindingFaker(
                    id=FINDING_ID,
                    evidences=FindingEvidences(
                        records=FindingEvidence(
                            description="records", url="evidence-record", modified_date=DATE_2024
                        ),
                        evidence1=FindingEvidence(
                            description="evidence1", url="evidence-1", modified_date=DATE_2024
                        ),
                    ),
                )
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    id=VULN_ID_1,
                    finding_id=FINDING_ID,
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.SUBMITTED),
                )
            ],
            stakeholders=[
                StakeholderFaker(email=ADMIN_EMAIL),
                StakeholderFaker(email=REVIEWER_EMAIL),
            ],
            group_access=[
                GroupAccessFaker(
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
                GroupAccessFaker(
                    email=REVIEWER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="reviewer"),
                ),
            ],
        )
    )
)
async def test_mutate_success(email: str, vulnerability_ids: list[str]) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [require_report_vulnerabilities],
            [enforce_group_level_auth_async],
            [require_attribute, "can_report_vulnerabilities", TEST_GROUP_NAME],
        ],
    )
    await mutate(_=None, info=info, finding_id=FINDING_ID, vulnerabilities=vulnerability_ids)
    loaders = get_new_context()

    # STREAMS_REQUIRED
    # findings unreliable indicators update should be asserted but this cannot be done until
    # streams is enabled for the test environment

    for vuln_id in vulnerability_ids:
        vulnerability = await loaders.vulnerability.load(vuln_id)
        assert vulnerability
        assert vulnerability.state.status == VulnerabilityStateStatus.VULNERABLE
        assert vulnerability.state.modified_by == email


@parametrize(
    args=["email", "vulnerability_ids"],
    cases=[
        [
            ADMIN_EMAIL,
            ["be09edb7-cd5c-47ed-bee4-97c645acdce10"],
        ],
        [
            REVIEWER_EMAIL,
            ["be09edb7-cd5c-47ed-bee4-97c645acdce10"],
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            findings=[
                FindingFaker(
                    id=FINDING_ID,
                    evidences=FindingEvidences(
                        evidence1=FindingEvidence(
                            description="records",
                            url="evidence-record",
                            modified_date=DATE_2024,
                            is_draft=True,
                        ),
                    ),
                )
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    id=VULN_ID_1,
                    finding_id=FINDING_ID,
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.SUBMITTED),
                )
            ],
            stakeholders=[
                StakeholderFaker(email=ADMIN_EMAIL),
                StakeholderFaker(email=REVIEWER_EMAIL),
            ],
            group_access=[
                GroupAccessFaker(
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
                GroupAccessFaker(
                    email=REVIEWER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="reviewer"),
                ),
            ],
        )
    )
)
async def test_mutate_fails(email: str, vulnerability_ids: list[str]) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [require_report_vulnerabilities],
            [enforce_group_level_auth_async],
            [require_attribute, "can_report_vulnerabilities", TEST_GROUP_NAME],
        ],
    )
    with raises(PendingEvidenceDraft):
        await mutate(_=None, info=info, finding_id=FINDING_ID, vulnerabilities=vulnerability_ids)
