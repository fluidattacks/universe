from integrates import findings
from integrates.api.mutations.reject_evidence import mutate
from integrates.dataloaders import get_new_context
from integrates.decorators import enforce_group_level_auth_async, require_login
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    FindingEvidenceFaker,
    FindingEvidencesFaker,
    FindingFaker,
    FindingStateFaker,
    GitRootFaker,
    GitRootStateFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import parametrize, raises

from .payloads.types import SimplePayload

EMAIL_TEST = "reviewer@fluidattacks.com"
GROUP_NAME = "group1"
ROOT_ID = "root1"
FINDING_ID = "9a85b711-4b4e-439a-832f-18fb286e5baa"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="reviewer")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL_TEST,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(has_access=True, role="reviewer"),
                ),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(nickname="back"),
                ),
            ],
            findings=[
                FindingFaker(
                    group_name="group1",
                    id=FINDING_ID,
                    state=FindingStateFaker(modified_by="hacker@fluidattacks.com"),
                    title="237. Logging of sensitive data",
                    evidences=FindingEvidencesFaker(evidence1=FindingEvidenceFaker(is_draft=True)),
                ),
            ],
        )
    ),
    others=[Mock(findings.domain.evidence, "send_mail_evidence_rejected", "async", None)],
)
async def test_reject_evidence() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
        ],
    )
    result = await mutate(
        _parent=None,
        info=info,
        evidence_id="evidence_route_1",
        finding_id=FINDING_ID,
        justification="Wrong evidence",
    )
    assert result == SimplePayload(success=True)

    loaders = get_new_context()
    finding_evidence = await loaders.finding.load(FINDING_ID)
    assert finding_evidence is not None
    assert finding_evidence.evidences.evidence1 is not None
    assert finding_evidence.evidences.evidence1.is_draft is True


@parametrize(
    args=["email"],
    cases=[["hacker@test.test"], ["reattacker@test.test"], ["user@test.test"]],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[
                StakeholderFaker(email="hacker@test.test", role="hacker"),
                StakeholderFaker(email="reattacker@test.test", role="reattacker"),
                StakeholderFaker(email="uset@test.test", role="user"),
            ],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email="hacker@test.test",
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email="reattacker@test.test",
                    state=GroupAccessStateFaker(has_access=True, role="reattacker"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email="user@test.test",
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                ),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME, id="root1", state=GitRootStateFaker(nickname="back")
                )
            ],
            findings=[
                FindingFaker(
                    group_name="group1",
                    id=FINDING_ID,
                    state=FindingStateFaker(modified_by="hacker@fluidattacks.com"),
                    title="237. Logging of sensitive data",
                    evidences=FindingEvidencesFaker(evidence1=FindingEvidenceFaker(is_draft=True)),
                ),
            ],
        ),
    )
)
async def test_reject_evidence_access_denied(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
        ],
    )

    with raises(Exception, match="Access denied"):
        await mutate(
            _parent=None,
            info=info,
            evidence_id="evidence_route_1",
            finding_id=FINDING_ID,
            justification="Wrong evidence",
        )
