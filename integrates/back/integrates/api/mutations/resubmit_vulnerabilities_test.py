from typing import Any

from integrates.api.mutations.resubmit_vulnerabilities import mutate
from integrates.custom_exceptions import VulnerabilityHasNotBeenRejected
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.findings.enums import FindingStatus
from integrates.db_model.vulnerabilities.enums import VulnerabilityStateStatus
from integrates.decorators import enforce_group_level_auth_async, require_attribute, require_login
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    EMAIL_FLUIDATTACKS_ADMIN,
    EMAIL_FLUIDATTACKS_CUSTOMER_MANAGER,
    EMAIL_FLUIDATTACKS_GROUP_MANAGER,
    EMAIL_FLUIDATTACKS_HACKER,
    EMAIL_FLUIDATTACKS_REATTACKER,
    EMAIL_FLUIDATTACKS_USER,
    EMAIL_FLUIDATTACKS_VULNERABILITY_MANAGER,
    FindingFaker,
    FindingUnreliableIndicatorsFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    StakeholderFaker,
    VulnerabilityFaker,
    VulnerabilityStateFaker,
    random_uuid,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

FINDING_ID = random_uuid()
VULN_ID1 = random_uuid()
VULN_ID2 = random_uuid()
VULN_ID3 = random_uuid()


DEFAULT_DECORATORS: list[list[Any]] = [
    [require_login],
    [enforce_group_level_auth_async],
    [require_attribute, "can_report_vulnerabilities", "test-group"],
]


@parametrize(
    args=["email"],
    cases=[
        [EMAIL_FLUIDATTACKS_ADMIN],
        [EMAIL_FLUIDATTACKS_HACKER],
        [EMAIL_FLUIDATTACKS_REATTACKER],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_ADMIN),
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_HACKER),
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_REATTACKER),
            ],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_ADMIN,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_HACKER,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_REATTACKER,
                    state=GroupAccessStateFaker(has_access=True, role="reattacker"),
                ),
            ],
            findings=[
                FindingFaker(
                    id=FINDING_ID,
                    unreliable_indicators=FindingUnreliableIndicatorsFaker(
                        unreliable_status=FindingStatus.VULNERABLE
                    ),
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    finding_id=FINDING_ID,
                    id=VULN_ID1,
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.REJECTED),
                ),
            ],
        ),
    )
)
async def test_resubmit_vulnerabilities(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=DEFAULT_DECORATORS,
    )

    await mutate(
        _=None,
        info=info,
        finding_id=FINDING_ID,
        vulnerabilities=[VULN_ID1],
    )

    loaders: Dataloaders = get_new_context()
    vulnerability = await loaders.vulnerability.load(VULN_ID1)
    assert vulnerability
    assert vulnerability.state.status == VulnerabilityStateStatus.SUBMITTED
    assert vulnerability.state.modified_by == email


@parametrize(
    args=["email", "vuln_id"],
    cases=[
        [EMAIL_FLUIDATTACKS_ADMIN, VULN_ID1],
        [EMAIL_FLUIDATTACKS_HACKER, VULN_ID2],
        [EMAIL_FLUIDATTACKS_REATTACKER, VULN_ID3],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_ADMIN),
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_HACKER),
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_REATTACKER),
            ],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_ADMIN,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_HACKER,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_REATTACKER,
                    state=GroupAccessStateFaker(has_access=True, role="reattacker"),
                ),
            ],
            findings=[
                FindingFaker(
                    id=FINDING_ID,
                    unreliable_indicators=FindingUnreliableIndicatorsFaker(
                        unreliable_status=FindingStatus.VULNERABLE
                    ),
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    finding_id=FINDING_ID,
                    id=VULN_ID1,
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.VULNERABLE),
                ),
                VulnerabilityFaker(
                    finding_id=FINDING_ID,
                    id=VULN_ID2,
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.SAFE),
                ),
                VulnerabilityFaker(
                    finding_id=FINDING_ID,
                    id=VULN_ID3,
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.SUBMITTED),
                ),
            ],
        ),
    )
)
async def test_resubmit_vulnerabilities_non_rejected(email: str, vuln_id: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=DEFAULT_DECORATORS,
    )
    with raises(VulnerabilityHasNotBeenRejected):
        await mutate(
            _=None,
            info=info,
            finding_id=FINDING_ID,
            vulnerabilities=[vuln_id],
        )


@parametrize(
    args=["email"],
    cases=[
        [EMAIL_FLUIDATTACKS_USER],
        [EMAIL_FLUIDATTACKS_GROUP_MANAGER],
        [EMAIL_FLUIDATTACKS_VULNERABILITY_MANAGER],
        [EMAIL_FLUIDATTACKS_CUSTOMER_MANAGER],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_USER),
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_GROUP_MANAGER),
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_VULNERABILITY_MANAGER),
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_CUSTOMER_MANAGER),
            ],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_USER,
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                ),
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_GROUP_MANAGER,
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_VULNERABILITY_MANAGER,
                    state=GroupAccessStateFaker(has_access=True, role="vulnerability_manager"),
                ),
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_CUSTOMER_MANAGER,
                    state=GroupAccessStateFaker(has_access=True, role="vulnerability_manager"),
                ),
            ],
            findings=[
                FindingFaker(
                    id=FINDING_ID,
                    unreliable_indicators=FindingUnreliableIndicatorsFaker(
                        unreliable_status=FindingStatus.VULNERABLE
                    ),
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    finding_id=FINDING_ID,
                    id=VULN_ID1,
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.REJECTED),
                ),
            ],
        ),
    )
)
async def test_resubmit_vulnerabilities_access_denied(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=DEFAULT_DECORATORS,
    )
    with raises(Exception, match="Access denied"):
        await mutate(
            _=None,
            info=info,
            finding_id=FINDING_ID,
            vulnerabilities=[VULN_ID1],
        )
