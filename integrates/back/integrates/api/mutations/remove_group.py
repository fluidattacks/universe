import logging
import logging.config

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.api.types import (
    APP_EXCEPTIONS,
)
from integrates.billing.authors import (
    get_unique_authors,
)
from integrates.custom_exceptions import (
    PermissionDenied,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.custom_utils.groups import get_group
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.groups.enums import (
    GroupStateJustification,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_login,
)
from integrates.groups import (
    domain as groups_domain,
)
from integrates.sessions import (
    domain as sessions_domain,
)
from integrates.tickets import (
    domain as tickets_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)

LOGGER = logging.getLogger(__name__)


@MUTATION.field("removeGroup")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_asm,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    group_name: str,
    reason: str,
    **kwargs: str,
) -> SimplePayload:
    loaders: Dataloaders = info.context.loaders
    group_name = group_name.lower()
    user_info = await sessions_domain.get_jwt_content(info.context)
    requester_email = user_info["user_email"]
    group = await get_group(loaders, group_name)
    unique_authors = await get_unique_authors(loaders=loaders, group=group)
    try:
        await groups_domain.remove_group(
            loaders=loaders,
            comments=kwargs.get("comments", ""),
            email=requester_email,
            group_name=group_name,
            justification=GroupStateJustification[reason.upper()],
            unique_authors=unique_authors,
        )
    except PermissionDenied:
        logs_utils.cloudwatch_log(
            info.context,
            "Unauthorized role attempted to remove group",
            extra={"group_name": group_name, "log_type": "Security"},
        )
        raise
    except APP_EXCEPTIONS as ex:
        await tickets_domain.delete_group(
            loaders=loaders,
            deletion_date=datetime_utils.get_utc_now(),
            group=group,
            unique_authors=unique_authors,
            requester_email=requester_email,
            reason=reason.upper(),
            comments=f"Platform exception: {ex}",
            attempt=None,
        )
        LOGGER.exception(
            "Error - could not remove group",
            extra={"extra": {"group_name": group_name, "ex": ex, **kwargs}},
        )
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to remove group",
            extra={"exception": str(ex), "group_name": group_name, "log_type": "Security"},
        )
        raise

    logs_utils.cloudwatch_log(
        info.context, "Removed group", extra={"group_name": group_name, "log_type": "Security"}
    )

    return SimplePayload(success=True)
