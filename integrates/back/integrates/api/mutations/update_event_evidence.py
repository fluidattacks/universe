from graphql.type.definition import (
    GraphQLResolveInfo,
)
from starlette.datastructures import (
    UploadFile,
)

from integrates.custom_utils.datetime import (
    get_now,
)
from integrates.custom_utils.events import (
    get_event,
)
from integrates.custom_utils.groups import get_group
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.events.enums import (
    EventEvidenceId,
)
from integrates.db_model.events.types import (
    EventRequest,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_login,
)
from integrates.events import (
    domain as events_domain,
)
from integrates.organizations.utils import (
    get_organization,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("updateEventEvidence")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_asm,
)
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    event_id: str,
    evidence_type: str,
    file: UploadFile,
    group_name: str,
    **_kwargs: None,
) -> SimplePayload:
    loaders: Dataloaders = info.context.loaders
    evidence_id = EventEvidenceId[evidence_type]
    event = await get_event(loaders, EventRequest(event_id=event_id, group_name=group_name))
    group = await get_group(loaders, event.group_name)
    organization = await get_organization(loaders, group.organization_id)
    await events_domain.validate_evidence(
        group_name=group.name.lower(),
        organization_name=organization.name.lower(),
        evidence_id=evidence_id,
        file=file,
    )

    await events_domain.update_evidence(
        loaders=loaders,
        event_id=event_id,
        evidence_id=evidence_id,
        file=file,
        group_name=group_name,
        update_date=get_now(),
    )
    loaders.event.clear(EventRequest(event_id=event_id, group_name=group_name))

    return SimplePayload(success=True)
