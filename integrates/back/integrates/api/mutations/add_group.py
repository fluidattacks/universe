from typing import (
    Any,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.groups.enums import (
    GroupLanguage,
    GroupService,
    GroupSubscriptionType,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_organization_level_auth_async,
    require_login,
)
from integrates.groups import (
    domain as groups_domain,
)
from integrates.marketplace.core import (
    check_user_subscription_usage_surpass_contract,
    send_metering_records,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


def _get_service(subscription_type: GroupSubscriptionType, **kwargs: str) -> GroupService:
    if kwargs.get("service"):
        service = GroupService[str(kwargs["service"]).upper()]
    else:
        service = (
            GroupService.WHITE
            if subscription_type == GroupSubscriptionType.CONTINUOUS
            else GroupService.BLACK
        )

    return service


@MUTATION.field("addGroup")
@concurrent_decorators(
    require_login,
    enforce_organization_level_auth_async,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    description: str,
    organization_name: str,
    subscription: str = "continuous",
    language: str = "en",
    **kwargs: Any,
) -> SimplePayload:
    loaders: Dataloaders = info.context.loaders
    group_name: str = str(kwargs["group_name"]).lower()
    has_advanced: bool = kwargs.get("has_advanced", False)
    has_essential: bool = kwargs.get("has_essential", False)
    subscription_type = GroupSubscriptionType[subscription.upper()]
    service = _get_service(subscription_type, **kwargs)
    user_data = await sessions_domain.get_jwt_content(info.context)
    email = user_data["user_email"]

    await groups_domain.add_group(
        loaders=loaders,
        description=description,
        email=email,
        group_name=group_name,
        has_essential=has_essential,
        has_advanced=has_advanced,
        language=GroupLanguage[language.upper()],
        organization_name=organization_name,
        service=service,
        subscription=subscription_type,
    )
    logs_utils.cloudwatch_log(
        info.context,
        "Added group",
        extra={
            "group_name": group_name,
            "language": language,
            "subscription": subscription,
            "log_type": "Security",
        },
    )
    if await check_user_subscription_usage_surpass_contract(email=email, loaders=loaders):
        await send_metering_records(email=email, loaders=loaders, value=1)

    return SimplePayload(success=True)
