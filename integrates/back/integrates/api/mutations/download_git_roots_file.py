import logging
import logging.config

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_exceptions import (
    ErrorDownloadingFile,
    ErrorUploadingFileS3,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_is_not_under_review,
    require_login,
)
from integrates.roots.download import (
    get_git_roots_file,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    DownloadFilePayload,
)
from .schema import (
    MUTATION,
)

# Constants
LOGGER = logging.getLogger(__name__)


@MUTATION.field("downloadGitRootsFile")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_asm,
)
async def mutate(_: None, info: GraphQLResolveInfo, group_name: str) -> DownloadFilePayload:
    user_info = await sessions_domain.get_jwt_content(info.context)
    user_email = user_info["user_email"]
    loaders: Dataloaders = info.context.loaders

    try:
        file_path = await get_git_roots_file(loaders, group_name)
        if file_path:
            logs_utils.cloudwatch_log(
                info.context,
                "Downloaded file of the Git Roots",
                extra={
                    "group_name": group_name,
                    "log_type": "Security",
                },
            )
        else:
            raise ErrorDownloadingFile.new()
    except (ErrorDownloadingFile, ErrorUploadingFileS3):
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to download file of the Git Roots",
            extra={
                "group_name": group_name,
                "log_type": "Security",
            },
        )
        msg_log = "An error occurred uploading a Git Roots file to S3 associated to the group"
        LOGGER.error(
            msg_log,
            extra={
                "extra": {
                    "group_name": group_name,
                    "user_email": user_email,
                },
            },
        )
        raise

    return DownloadFilePayload(success=True, url=file_path)
