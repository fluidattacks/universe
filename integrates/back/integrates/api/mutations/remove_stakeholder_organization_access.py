from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.decorators import (
    enforce_organization_level_auth_async,
)
from integrates.forces.validations import (
    validate_non_forces_user,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.organizations import (
    utils as orgs_utils,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("removeStakeholderOrganizationAccess")
@enforce_organization_level_auth_async
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    organization_id: str,
    user_email: str,
) -> SimplePayload:
    user_data = await sessions_domain.get_jwt_content(info.context)
    requester_email = user_data["user_email"]
    validate_non_forces_user(user_email)
    loaders: Dataloaders = info.context.loaders
    organization = await orgs_utils.get_organization(loaders, organization_id)

    await orgs_domain.remove_access(
        organization_id=organization_id,
        email=user_email.lower(),
        modified_by=requester_email,
    )
    logs_utils.cloudwatch_log(
        info.context,
        "Removed stakeholder in the organization",
        extra={
            "organization_name": organization.name,
            "removed stakeholder": user_email,
            "requester_email": requester_email,
            "log_type": "Security",
        },
    )

    return SimplePayload(success=True)
