from datetime import (
    datetime,
)

from integrates.api.mutations.remove_event_evidence import (
    mutate,
)
from integrates.dataloaders import get_new_context
from integrates.db_model.events.enums import (
    EventStateStatus,
)
from integrates.db_model.events.types import (
    EventEvidence,
    EventRequest,
)
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute,
    require_login,
)
from integrates.s3 import (
    operations as s3_ops,
)
from integrates.testing.aws import (
    IntegratesAws,
    IntegratesDynamodb,
)
from integrates.testing.fakers import (
    EventEvidencesFaker,
    EventFaker,
    EventStateFaker,
    GitRootFaker,
    GitRootStateFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import parametrize, raises

TEST_DATE = datetime.fromisoformat("2024-12-10T12:59:52+00:00")
EMAIL_TEST = "hacker@fluidattacks.com"
GROUP_NAME = "group1"
ROOT_ID = "root1"
EVENT_ID = "event_id_1"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="hacker")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL_TEST,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME, id=ROOT_ID, state=GitRootStateFaker(nickname="back")
                ),
            ],
            events=[
                EventFaker(
                    group_name=GROUP_NAME,
                    id=EVENT_ID,
                    evidences=EventEvidencesFaker(
                        image_1=EventEvidence(
                            file_name="test_evidence.png", modified_date=TEST_DATE
                        )
                    ),
                    state=EventStateFaker(status=EventStateStatus.CREATED),
                )
            ],
        )
    ),
    others=[Mock(s3_ops, "remove_file", "async", None)],
)
async def test_remove_event_evidence() -> None:
    event = await get_new_context().event.load(
        EventRequest(event_id=EVENT_ID, group_name=GROUP_NAME)
    )
    assert event
    assert event.evidences.image_1
    assert event.evidences.image_1.file_name == "test_evidence.png"

    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    result = await mutate(
        _parent=None,
        info=info,
        event_id=EVENT_ID,
        evidence_type="IMAGE_1",
        group_name=GROUP_NAME,
    )
    assert result.success is True

    event = await get_new_context().event.load(
        EventRequest(event_id=EVENT_ID, group_name=GROUP_NAME)
    )
    assert event
    assert event.evidences.image_1 is None


@parametrize(
    args=["email"],
    cases=[["reviewer@fluidattacks.com"], ["user@test.test"]],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[
                StakeholderFaker(email="hacker@test.test", role="hacker"),
                StakeholderFaker(email="reattacker@test.test", role="reattacker"),
                StakeholderFaker(email="uset@test.test", role="user"),
            ],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email="reviewer@fluidattacks.com",
                    state=GroupAccessStateFaker(has_access=True, role="reviewer"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email="user@test.test",
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                ),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME, id="root1", state=GitRootStateFaker(nickname="back")
                )
            ],
        ),
    )
)
async def test_remove_event_evidence_access_denied(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )

    with raises(Exception, match="Access denied"):
        await mutate(
            _parent=None,
            info=info,
            event_id=EVENT_ID,
            evidence_type="IMAGE_1",
            group_name=GROUP_NAME,
        )
