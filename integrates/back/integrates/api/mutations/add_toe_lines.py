from datetime import (
    datetime,
)
from typing import (
    TypedDict,
    Unpack,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.api.types import (
    APP_EXCEPTIONS,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_login,
)
from integrates.sessions import (
    domain as sessions_domain,
)
from integrates.toe.lines import (
    domain as toe_lines_domain,
)
from integrates.toe.lines.types import (
    ToeLinesAttributesToAdd,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


class AddToeLinesArgs(TypedDict):
    group_name: str
    root_id: str
    filename: str
    last_author: str
    last_commit: str
    loc: int
    modified_date: datetime


@MUTATION.field("addToeLines")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_asm,
)
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    **kwargs: Unpack[AddToeLinesArgs],
) -> SimplePayload:
    try:
        loaders: Dataloaders = info.context.loaders
        user_data = await sessions_domain.get_jwt_content(info.context)
        user_email = user_data["user_email"]
        group_name = kwargs["group_name"]
        await toe_lines_domain.add(
            loaders=loaders,
            group_name=group_name,
            root_id=kwargs["root_id"],
            filename=kwargs["filename"],
            attributes=ToeLinesAttributesToAdd(
                attacked_lines=0,
                be_present=False,
                last_author=kwargs["last_author"],
                last_commit=kwargs["last_commit"],
                loc=kwargs["loc"],
                last_commit_date=kwargs["modified_date"],
                seen_first_time_by=user_email,
            ),
        )
        logs_utils.cloudwatch_log(
            info.context,
            "Added toe lines in the group",
            extra={"group_name": group_name, "log_type": "Security"},
        )
    except APP_EXCEPTIONS:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to add toe lines in the group",
            extra={"group_name": group_name, "log_type": "Security"},
        )
        raise

    return SimplePayload(success=True)
