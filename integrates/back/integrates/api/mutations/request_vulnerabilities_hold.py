from graphql.type.definition import (
    GraphQLResolveInfo,
)

import integrates.vulnerabilities.domain as vulns_domain
from integrates.api.types import (
    APP_EXCEPTIONS,
)
from integrates.custom_exceptions import (
    EventAlreadyClosed,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.custom_utils.events import (
    get_event,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.events.enums import (
    EventStateStatus,
)
from integrates.db_model.events.types import (
    EventRequest,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_continuous,
    require_finding_access,
    require_is_not_under_review,
    require_login,
    require_report_vulnerabilities,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("requestVulnerabilitiesHold")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_continuous,
    require_asm,
    require_is_not_under_review,
    require_report_vulnerabilities,
    require_finding_access,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    event_id: str,
    group_name: str,
    finding_id: str,
    vulnerabilities: list[str],
) -> SimplePayload:
    loaders: Dataloaders = info.context.loaders
    try:
        user_info = await sessions_domain.get_jwt_content(info.context)
        event = await get_event(loaders, EventRequest(event_id=event_id, group_name=group_name))
        if event.state.status == EventStateStatus.SOLVED:
            raise EventAlreadyClosed()

        await vulns_domain.request_vulnerabilities_hold(
            loaders=loaders,
            event_id=event_id,
            event_type=event.type,
            finding_id=finding_id,
            user_info=user_info,
            vulnerability_ids=set(vulnerabilities),
        )
        logs_utils.cloudwatch_log(
            info.context,
            "Requested vuln reattack hold in the finding",
            extra={
                "event_id": event_id,
                "finding_id": finding_id,
                "group_name": group_name,
                "vuln_ids": vulnerabilities,
                "log_type": "Security",
            },
        )
    except APP_EXCEPTIONS:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to request vuln reattack hold in the finding",
            extra={
                "event_id": event_id,
                "finding_id": finding_id,
                "group_name": group_name,
                "vuln_ids": vulnerabilities,
                "log_type": "Security",
            },
        )
        raise

    return SimplePayload(success=True)
