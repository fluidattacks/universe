from typing import TypedDict, Unpack

from graphql.type.definition import GraphQLResolveInfo

from integrates.custom_utils import logs as logs_utils
from integrates.dataloaders import Dataloaders
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_login,
    require_service_white,
)
from integrates.jobs_orchestration.jobs import queue_sbom_image_job
from integrates.jobs_orchestration.model.types import SbomFormat, SbomJobType
from integrates.roots import domain as roots_domain
from integrates.sessions.domain import get_jwt_content

from .payloads.types import AddRootDockerImagePayload
from .schema import MUTATION


class RootDockerImageArgs(TypedDict):
    credentials: dict[str, str]
    group_name: str
    root_id: str
    uri: str


@MUTATION.field("addRootDockerImage")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_service_white,
    require_is_not_under_review,
)
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    **kwargs: Unpack[RootDockerImageArgs],
) -> AddRootDockerImagePayload:
    loaders: Dataloaders = info.context.loaders
    user_info = await get_jwt_content(info.context)
    group_name = kwargs["group_name"]
    credential_id = kwargs.get("credentials", {}).get("id")
    user_email = user_info["user_email"]
    uri = kwargs["uri"]
    root_id = kwargs["root_id"]
    root_docker_image = await roots_domain.add_root_docker_image(
        loaders=loaders,
        group_name=group_name,
        root_id=root_id,
        credential_id=credential_id,
        uri=uri,
        user_email=user_email,
    )
    is_job_queued = False
    if root_docker_image:
        queue_result = await queue_sbom_image_job(
            loaders=loaders,
            group_name=group_name,
            modified_by=user_email,
            uris_with_credentials=[(root_docker_image.uri, root_docker_image.state.credential_id)],
            job_type=SbomJobType.SCHEDULER,
            sbom_format=SbomFormat.FLUID_JSON,
        )
        is_job_queued = queue_result.success
    logs_utils.cloudwatch_log(
        info.context,
        "Added Docker image",
        extra={"group_name": group_name, "root_id": root_id, "log_type": "Security"},
    )
    return AddRootDockerImagePayload(
        success=bool(root_docker_image), sbom_job_queued=is_job_queued, uri=uri
    )
