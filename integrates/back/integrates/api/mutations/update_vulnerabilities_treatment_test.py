from integrates.api.mutations.update_vulnerabilities_treatment import mutate
from integrates.custom_exceptions import CustomBaseException
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.enums import TreatmentStatus
from integrates.db_model.vulnerabilities.enums import VulnerabilityStateStatus
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_asm,
    require_attribute,
    require_attribute_internal,
    require_is_not_under_review,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    FindingFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
    VulnerabilityFaker,
    VulnerabilityStateFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import freeze_time, parametrize, raises
from integrates.treatment.utils import get_inverted_treatment_converted
from integrates.vulnerabilities import domain as vulns_domain

ORG_ID = "org1"
GROUP_NAME = "test-group"


@freeze_time("2024-12-14T00:00:00")
@parametrize(
    args=["user_email", "treatment_data"],
    cases=[
        [
            "admin@fluidattacks.com",
            {
                "finding_id": "3c475384-834c-47b0-ac71-a41a022e401c",
                "vulnerability_id": "3d982277-a737-4046-ab7d-f3a2c4e6ecad",
                "treatment": "ACCEPTED",
                "justification": "Valid business reason",
                "acceptance_date": "2025-1-3",
                "assigned": "developer@company.com",
            },
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            stakeholders=[
                StakeholderFaker(email="admin@fluidattacks.com"),
                StakeholderFaker(email="group_manager@fluidattacks.com"),
                StakeholderFaker(email="developer@company.com"),
            ],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email="admin@fluidattacks.com",
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email="group_manager@fluidattacks.com",
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email="developer@company.com",
                    state=GroupAccessStateFaker(has_access=True, role="vulnerability_manager"),
                ),
            ],
            findings=[FindingFaker(id="3c475384-834c-47b0-ac71-a41a022e401c")],
            vulnerabilities=[
                VulnerabilityFaker(
                    id="3d982277-a737-4046-ab7d-f3a2c4e6ecad",
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                    state=VulnerabilityStateFaker(
                        status=VulnerabilityStateStatus.VULNERABLE,
                    ),
                ),
                VulnerabilityFaker(
                    id="2ed00278-4481-4a44-8da4-04fe41751486",
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                    state=VulnerabilityStateFaker(
                        status=VulnerabilityStateStatus.SAFE,
                    ),
                ),
            ],
        )
    )
)
async def test_update_vulnerabilities_treatment(
    user_email: str,
    treatment_data: dict[str, str],
) -> None:
    loaders: Dataloaders = get_new_context()
    info = GraphQLResolveInfoFaker(
        user_email=user_email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )

    result = await mutate(_=None, info=info, **treatment_data)

    assert result.success is True

    vuln = await vulns_domain.get_vulnerability(loaders, treatment_data["vulnerability_id"])
    assert vuln.treatment is not None
    assert (
        vuln.treatment.status
        == TreatmentStatus[
            get_inverted_treatment_converted(treatment_data["treatment"].replace(" ", "_").upper())
        ]
    )
    assert vuln.treatment.justification == treatment_data["justification"]
    assert vuln.treatment.assigned == treatment_data["assigned"]


@parametrize(
    args=["user_email", "treatment_data", "exception"],
    cases=[
        [
            "user@fluidattacks.com",
            {
                "finding_id": "463463",
                "vulnerability_id": "3d982277-a737-4046-ab7d-f3a2c4e6ecad",
                "treatment": "ACCEPTED",
                "justification": "test",
                "acceptance_date": "2024-12-31",
            },
            "Assigned not valid",
        ],
        [
            "admin@fluidattacks.com",
            {
                "finding_id": "463463",
                "vulnerability_id": "3d982277-a737-4046-ab7d-f3a2c4e6ecad",
                "treatment": "INVALID_STATUS",
                "justification": "test",
            },
            "'INVALID_STATUS'",
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            stakeholders=[
                StakeholderFaker(email="user@fluidattacks.com"),
                StakeholderFaker(email="admin@fluidattacks.com"),
            ],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email="user@fluidattacks.com",
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email="admin@fluidattacks.com",
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
            findings=[FindingFaker(id="463463", group_name=GROUP_NAME)],
            vulnerabilities=[
                VulnerabilityFaker(
                    id="3d982277-a737-4046-ab7d-f3a2c4e6ecad",
                    finding_id="463463",
                    state=VulnerabilityStateFaker(
                        status=VulnerabilityStateStatus.VULNERABLE,
                    ),
                ),
            ],
        )
    )
)
async def test_update_vulnerabilities_treatment_fail(
    user_email: str,
    treatment_data: dict[str, str],
    exception: CustomBaseException,
) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=user_email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )

    with raises(Exception, match=str(exception)):
        await mutate(_=None, info=info, **treatment_data)
