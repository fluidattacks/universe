from typing import (
    Any,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.api.types import (
    APP_EXCEPTIONS,
)
from integrates.custom_exceptions import (
    InvalidFileType,
)
from integrates.custom_utils import (
    files as files_utils,
)
from integrates.custom_utils import (
    findings as finding_utils,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.custom_utils.groups import get_group
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.enums import (
    TreatmentStatus,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_login,
    require_report_vulnerabilities,
)
from integrates.organizations.utils import (
    get_organization,
)
from integrates.unreliable_indicators.enums import (
    EntityDependency,
)
from integrates.unreliable_indicators.operations import (
    update_unreliable_indicators_by_deps,
)
from integrates.vulnerability_files import (
    domain as vuln_files_domain,
)

from .payloads.types import (
    SimplePayloadMessage,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("uploadFile")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_report_vulnerabilities,
    require_asm,
)
async def mutate(_: None, info: GraphQLResolveInfo, **kwargs: Any) -> SimplePayloadMessage:
    try:
        finding_id = kwargs["finding_id"]
        file_input = kwargs["file"]
        loaders: Dataloaders = info.context.loaders
        finding = await finding_utils.get_finding(loaders, finding_id)
        allowed_mime_type = await files_utils.assert_uploaded_file_mime(
            file_input,
            ["text/x-yaml", "text/plain", "text/html"],
        )
        group = await get_group(loaders, finding.group_name)
        organization = await get_organization(loaders, group.organization_id)
        permanent_acceptance_finding_policy = await finding_utils.get_finding_policy_by_name(
            loaders=loaders,
            finding_name=finding.title,
            organization_name=organization.name,
            treatment=TreatmentStatus.ACCEPTED_UNDEFINED,
        )
        if file_input and allowed_mime_type:
            (
                processed_vulnerabilities,
                message,
            ) = await vuln_files_domain.upload_file(
                info,
                file_input,
                finding_id,
                permanent_acceptance_finding_policy,
                finding.group_name,
            )
        else:
            raise InvalidFileType()

        if not processed_vulnerabilities:
            return SimplePayloadMessage(success=False, message="")

        await update_unreliable_indicators_by_deps(
            EntityDependency.upload_file,
            finding_ids=[finding_id],
            vulnerability_ids=list(processed_vulnerabilities),
        )
        if permanent_acceptance_finding_policy:
            await update_unreliable_indicators_by_deps(
                EntityDependency.handle_finding_policy,
                finding_ids=[finding_id],
                vulnerability_ids=list(processed_vulnerabilities),
            )
        logs_utils.cloudwatch_log(
            info.context,
            "Uploaded vulnerability file in the group",
            extra={
                "finding_id": finding.id,
                "group_name": finding.group_name,
                "log_type": "Security",
            },
        )
    except APP_EXCEPTIONS:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to upload vulnerability file in the group",
            extra={
                "finding_id": finding.id,
                "group_name": finding.group_name,
                "log_type": "Security",
            },
        )
        raise

    return SimplePayloadMessage(success=True, message=message)
