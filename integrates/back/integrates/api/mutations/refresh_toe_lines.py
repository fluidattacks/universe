from aioextensions import (
    collect,
)
from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.api.types import (
    APP_EXCEPTIONS,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.custom_utils.roots import (
    get_active_git_roots,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_login,
)
from integrates.server_async.enqueue import (
    queue_refresh_toe_lines_async,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("refreshToeLines")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_is_not_under_review,
)
async def mutate(_: None, info: GraphQLResolveInfo, group_name: str) -> SimplePayload:
    try:
        loaders: Dataloaders = info.context.loaders
        user_info = await sessions_domain.get_jwt_content(info.context)
        user_email = user_info["user_email"]
        roots = await get_active_git_roots(loaders, group_name)
        await collect(
            [queue_refresh_toe_lines_async(group_name, root.id, user_email) for root in roots],
        )
        logs_utils.cloudwatch_log(
            info.context,
            "Schedule the toe lines refreshing in the group",
            extra={
                "group_name": group_name,
                "log_type": "Security",
            },
        )
    except APP_EXCEPTIONS:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to schedule the toe lines refreshing in the group",
            extra={
                "group_name": group_name,
                "log_type": "Security",
            },
        )
        raise

    return SimplePayload(success=True)
