from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_exceptions import (
    ErrorDownloadingFile,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_is_not_under_review,
    require_login,
)
from integrates.events import (
    domain as events_domain,
)

from .payloads.types import (
    DownloadFilePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("downloadEventFile")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_asm,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    event_id: str,
    file_name: str,
    group_name: str,
    **_kwargs: str,
) -> DownloadFilePayload:
    loaders: Dataloaders = info.context.loaders
    signed_url = await events_domain.get_evidence_link(loaders, event_id, file_name, group_name)
    if signed_url:
        logs_utils.cloudwatch_log(
            info.context,
            "Downloaded file of the event",
            extra={
                "event_id": event_id,
                "log_type": "Security",
            },
        )
    else:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to download file of the event",
            extra={
                "event_id": event_id,
                "log_type": "Security",
            },
        )
        raise ErrorDownloadingFile.new()

    return DownloadFilePayload(success=True, url=signed_url)
