from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.api.types import (
    APP_EXCEPTIONS,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.custom_utils.findings import get_finding
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.enums import (
    TreatmentStatus,
)
from integrates.db_model.types import (
    TreatmentToUpdate,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_is_not_under_review,
    require_login,
)
from integrates.sessions import (
    domain as sessions_domain,
)
from integrates.treatment.utils import get_accepted_until, get_inverted_treatment_converted
from integrates.unreliable_indicators.enums import (
    EntityDependency,
)
from integrates.unreliable_indicators.operations import (
    update_unreliable_indicators_by_deps,
)
from integrates.vulnerabilities import (
    domain as vulns_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("updateVulnerabilitiesTreatment")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_asm,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    finding_id: str,
    vulnerability_id: str,
    **kwargs: str,
) -> SimplePayload:
    try:
        user_info = await sessions_domain.get_jwt_content(info.context)
        user_email: str = user_info["user_email"]
        loaders: Dataloaders = info.context.loaders
        finding = await get_finding(loaders, finding_id)
        justification: str = kwargs["justification"]
        treatment_status = TreatmentStatus[
            get_inverted_treatment_converted(kwargs["treatment"].replace(" ", "_").upper())
        ]
        accepted_until = get_accepted_until(kwargs.get("acceptance_date"), treatment_status)
        assigned = kwargs.get("assigned", "")
        await vulns_domain.update_vulnerabilities_treatment(
            loaders=loaders,
            finding=finding,
            modified_by=user_email,
            vulnerability_id=vulnerability_id,
            treatment=TreatmentToUpdate(
                accepted_until=accepted_until,
                assigned=assigned,
                justification=justification,
                status=treatment_status,
            ),
        )
        await update_unreliable_indicators_by_deps(
            EntityDependency.update_vulnerabilities_treatment,
            finding_ids=[finding_id],
            vulnerability_ids=[vulnerability_id],
        )
        logs_utils.cloudwatch_log(
            info.context,
            "Updated treatment in the vulnerability",
            extra={
                "finding_id": finding_id,
                "group_name": finding.group_name,
                "vulnerability_id": vulnerability_id,
                "treatment_status": treatment_status,
                "log_type": "Security",
            },
        )

    except APP_EXCEPTIONS:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to update treatment in the vulnerability",
            extra={
                "finding_id": finding_id,
                "group_name": finding.group_name,
                "vulnerability_id": vulnerability_id,
                "log_type": "Security",
            },
        )
        raise

    return SimplePayload(success=True)
