from datetime import datetime

from integrates.api.mutations.request_vulnerabilities_hold import mutate
from integrates.custom_exceptions import EventAlreadyClosed
from integrates.db_model.events.enums import EventStateStatus, EventType
from integrates.db_model.vulnerabilities.enums import VulnerabilityVerificationStatus
from integrates.db_model.vulnerabilities.types import VulnerabilityVerification
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_asm,
    require_attribute,
    require_attribute_internal,
    require_continuous,
    require_finding_access,
    require_is_not_under_review,
    require_login,
    require_report_vulnerabilities,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    EventFaker,
    EventStateFaker,
    FindingFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    StakeholderFaker,
    VulnerabilityFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

ADMIN_EMAIL = "admin@gmail.com"
USER_EMAIL = "user@gmail.com"
GROUP_MANAGER_EMAIL = "group_manager@fluidattacks.com"
HACKER_EMAIL = "hacker@fluidattacks.com"
REATTACKER_EMAIL = "reattacker@fluidattacks.com"
GROUP_NAME = "test-group"


@parametrize(
    args=["email"],
    cases=[
        [ADMIN_EMAIL],
        [HACKER_EMAIL],
        [REATTACKER_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=ADMIN_EMAIL),
                StakeholderFaker(email=HACKER_EMAIL),
                StakeholderFaker(email=REATTACKER_EMAIL),
            ],
            findings=[
                FindingFaker(id="3c475384-834c-47b0-ac71-a41a022e401c", group_name=GROUP_NAME)
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    id="vuln1",
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                    verification=VulnerabilityVerification(
                        modified_date=datetime.now(),
                        status=VulnerabilityVerificationStatus.REQUESTED,
                        justification="test justification",
                    ),
                ),
                VulnerabilityFaker(
                    id="vuln2",
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                    verification=VulnerabilityVerification(
                        modified_date=datetime.now(),
                        status=VulnerabilityVerificationStatus.REQUESTED,
                        justification="test justification",
                    ),
                ),
            ],
            events=[
                EventFaker(
                    id="event1",
                    group_name=GROUP_NAME,
                    state=EventStateFaker(status=EventStateStatus.OPEN),
                    type=EventType.AUTHORIZATION_SPECIAL_ATTACK,
                )
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=HACKER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=REATTACKER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="reattacker"),
                ),
            ],
        )
    )
)
async def test_request_vulnerabilities_hold_success(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_continuous],
            [require_attribute, "is_continuous", GROUP_NAME],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
            [require_report_vulnerabilities],
            [require_attribute, "can_report_vulnerabilities", GROUP_NAME],
            [require_finding_access],
        ],
    )
    result = await mutate(
        _=None,
        info=info,
        event_id="event1",
        group_name=GROUP_NAME,
        finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
        vulnerabilities=["vuln1", "vuln2"],
    )
    assert result.success


@parametrize(
    args=["email"],
    cases=[
        [USER_EMAIL],
        [GROUP_MANAGER_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=USER_EMAIL),
                StakeholderFaker(email=GROUP_MANAGER_EMAIL),
            ],
            findings=[
                FindingFaker(id="3c475384-834c-47b0-ac71-a41a022e401c", group_name=GROUP_NAME)
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    id="vuln1",
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                    verification=VulnerabilityVerification(
                        modified_date=datetime.now(),
                        status=VulnerabilityVerificationStatus.REQUESTED,
                        justification="test justification",
                    ),
                ),
            ],
            events=[
                EventFaker(
                    id="event1",
                    group_name=GROUP_NAME,
                    state=EventStateFaker(status=EventStateStatus.OPEN),
                )
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=USER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=GROUP_MANAGER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
        )
    )
)
async def test_request_vulnerabilities_hold_unauthorized(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_continuous],
            [require_attribute, "is_continuous", GROUP_NAME],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
            [require_report_vulnerabilities],
            [require_attribute, "can_report_vulnerabilities", GROUP_NAME],
            [require_finding_access],
        ],
    )
    with raises(Exception, match="Access denied"):
        await mutate(
            _=None,
            info=info,
            event_id="event1",
            group_name=GROUP_NAME,
            finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
            vulnerabilities=["vuln1"],
        )


@parametrize(
    args=["email"],
    cases=[
        [ADMIN_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=ADMIN_EMAIL),
            ],
            findings=[
                FindingFaker(id="3c475384-834c-47b0-ac71-a41a022e401c", group_name=GROUP_NAME)
            ],
            events=[
                EventFaker(
                    id="event1",
                    group_name=GROUP_NAME,
                    state=EventStateFaker(status=EventStateStatus.SOLVED),
                )
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
        )
    )
)
async def test_request_vulnerabilities_hold_closed_event(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_continuous],
            [require_attribute, "is_continuous", GROUP_NAME],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
            [require_report_vulnerabilities],
            [require_attribute, "can_report_vulnerabilities", GROUP_NAME],
            [require_finding_access],
        ],
    )
    with raises(EventAlreadyClosed):
        await mutate(
            _=None,
            info=info,
            event_id="event1",
            group_name=GROUP_NAME,
            finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
            vulnerabilities=["vuln1"],
        )
