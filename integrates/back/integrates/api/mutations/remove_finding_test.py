from integrates import mailer
from integrates.api.mutations.remove_finding import mutate
from integrates.batch.types import PutActionResult
from integrates.custom_exceptions import FindingNotFound, InvalidRemovalFindingState
from integrates.custom_utils import requests as requests_utils
from integrates.custom_utils.findings import get_finding
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.enums import Source
from integrates.db_model.findings.enums import FindingStateStatus
from integrates.db_model.vulnerabilities.enums import VulnerabilityStateStatus
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute,
    require_attribute_internal,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    FindingFaker,
    GitRootFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
    VulnerabilityFaker,
    VulnerabilityStateFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import parametrize, raises

from .payloads.types import SimplePayload

EMAIL_TEST = "admin@fluidattacks.com"
FINDING_ID = "60baa014-f436-4c37-ae9c-e4b8d0ef54e9"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="admin")],
            groups=[GroupFaker(name="group1", organization_id="org1")],
            roots=[GitRootFaker(group_name="group1", id="root1")],
            findings=[FindingFaker(id=FINDING_ID, group_name="group1")],
        ),
    ),
    others=[
        Mock(
            mailer.findings,
            "send_mails_async",
            "async",
            PutActionResult(success=True),
        ),
        Mock(
            requests_utils,
            "get_source_new",
            "sync",
            Source.ASM,
        ),
    ],
)
async def test_remove_finding() -> None:
    loaders: Dataloaders = get_new_context()
    finding = await loaders.finding.load(FINDING_ID)
    assert finding
    assert finding.state.status == FindingStateStatus.CREATED

    group = "group1"
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", group],
            [require_attribute_internal, "is_under_review", group],
        ],
    )
    mutation_result = await mutate(
        _=None,
        info=info,
        finding_id=FINDING_ID,
        justification="NOT_REQUIRED",
    )

    assert mutation_result == SimplePayload(success=True)
    loaders.finding.clear_all()
    finding = await loaders.finding.load(FINDING_ID)
    assert finding
    assert finding.state.status == FindingStateStatus.DELETED

    with raises(FindingNotFound):
        await get_finding(loaders, FINDING_ID)


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email="hacker@fluidattacks.com", role="hacker"),
            ],
            groups=[
                GroupFaker(name="group1", organization_id="org1"),
            ],
            group_access=[
                GroupAccessFaker(
                    email="hacker@fluidattacks.com",
                    group_name="group1",
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
            ],
            roots=[
                GitRootFaker(group_name="group1", id="root1"),
            ],
            findings=[FindingFaker(id=FINDING_ID, group_name="group1")],
            vulnerabilities=[
                VulnerabilityFaker(
                    id="vuln_id_1",
                    root_id="root1",
                    finding_id=FINDING_ID,
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.VULNERABLE),
                ),
            ],
        ),
    ),
    others=[
        Mock(
            requests_utils,
            "get_source_new",
            "async",
            Source.ASM,
        )
    ],
)
async def test_remove_finding_not_authorized_hacker() -> None:
    loaders: Dataloaders = get_new_context()
    finding = await loaders.finding.load(FINDING_ID)
    assert finding
    assert finding.state.status == FindingStateStatus.CREATED
    group = "group1"
    info = GraphQLResolveInfoFaker(
        user_email="hacker@fluidattacks.com",
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", group],
            [require_attribute_internal, "is_under_review", group],
        ],
    )
    with raises(InvalidRemovalFindingState):
        await mutate(
            _=None,
            info=info,
            finding_id=FINDING_ID,
            justification="NO_JUSTIFICATION",
        )


# Not allowed users
CUSTOMER_MANAGER_EMAIL = "customer_manager@gmail.com"
USER_EMAIL = "user@gmail.com"


@parametrize(
    args=["email"],
    cases=[[CUSTOMER_MANAGER_EMAIL], [USER_EMAIL]],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=USER_EMAIL),
                StakeholderFaker(email=CUSTOMER_MANAGER_EMAIL),
            ],
            groups=[
                GroupFaker(name="group1"),
            ],
            group_access=[
                GroupAccessFaker(
                    email=USER_EMAIL,
                    group_name="group1",
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                ),
                GroupAccessFaker(
                    email=CUSTOMER_MANAGER_EMAIL,
                    group_name="group1",
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
        )
    )
)
async def test_remove_finding_not_allowed(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", "group1"],
            [require_attribute_internal, "is_under_review", "group1"],
        ],
    )
    with raises(Exception, match="Access denied"):
        await mutate(
            _=None,
            info=info,
            finding_id=FINDING_ID,
            justification="NO_JUSTIFICATION",
        )
