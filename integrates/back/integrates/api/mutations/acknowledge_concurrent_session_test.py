from integrates.api.mutations.acknowledge_concurrent_session import mutate
from integrates.dataloaders import get_new_context
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import GraphQLResolveInfoFaker, StakeholderFaker
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize

ADMIN = "admin@gmail.com"


@parametrize(args=["user_email"], cases=[[ADMIN]])
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[StakeholderFaker(email=ADMIN, is_concurrent_session=True)],
        ),
    ),
)
async def test_ack_concurrent_session(user_email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=user_email,
    )
    payload = await mutate(
        _=None,
        info=info,
    )
    assert payload.success
    loaders = get_new_context()
    stakeholder = await loaders.stakeholder.load(user_email)
    assert stakeholder
    assert stakeholder.is_concurrent_session is False
