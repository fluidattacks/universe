from integrates.api.mutations.update_toe_input import mutate
from integrates.custom_exceptions import ToeInputNotFound
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.toe_inputs.types import RootToeInputsRequest
from integrates.decorators import enforce_group_level_auth_async, require_attribute, require_login
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GitRootFaker,
    GitRootStateFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
    ToeInputFaker,
    ToeInputStateFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import freeze_time, parametrize, raises

from .payloads.types import UpdateToeInputPayload

GROUP_NAME = "group1"
ROOT_ID = "root1"
COMPONENT = "https://clientapp.com/"
ENTRYPOINT = "login"


@parametrize(
    args=["user_email"],
    cases=[
        ["hacker@gmail.com"],
        ["reattacker@fluidattacks.com"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email="hacker@gmail.com", role="hacker"),
                StakeholderFaker(email="reattacker@fluidattacks.com", role="reattacker"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            group_access=[
                GroupAccessFaker(
                    email="reattacker@fluidattacks.com",
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(has_access=True, role="reattacker"),
                ),
                GroupAccessFaker(
                    email="hacker@gmail.com",
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(
                        nickname="back",
                    ),
                ),
            ],
            toe_inputs=[
                ToeInputFaker(
                    component=COMPONENT,
                    entry_point=ENTRYPOINT,
                    environment_id="env_id",
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=ToeInputStateFaker(
                        be_present=False,
                        attacked_by="other_hacker@fluidattacks.com",
                    ),
                ),
            ],
        ),
    ),
)
@freeze_time("2024-10-14T00:00:00")
async def test_update_toe_input(user_email: str) -> None:
    loaders: Dataloaders = get_new_context()
    toe_inputs = await loaders.root_toe_inputs.load_nodes(
        RootToeInputsRequest(group_name=GROUP_NAME, root_id=ROOT_ID),
    )

    assert len(toe_inputs) == 1
    assert toe_inputs[0].component == COMPONENT
    assert toe_inputs[0].state.attacked_by == "other_hacker@fluidattacks.com"
    assert toe_inputs[0].state.be_present is False

    info = GraphQLResolveInfoFaker(
        user_email=user_email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )

    result = await mutate(
        _parent=None,
        info=info,
        be_present=True,
        component=COMPONENT,
        entry_point=ENTRYPOINT,
        group_name=GROUP_NAME,
        has_recent_attack=True,
        root_id=ROOT_ID,
    )
    assert result == UpdateToeInputPayload(
        component=COMPONENT,
        entry_point=ENTRYPOINT,
        group_name=GROUP_NAME,
        root_id=ROOT_ID,
        success=True,
    )

    toe_inputs_updated = await get_new_context().root_toe_inputs.load_nodes(
        RootToeInputsRequest(group_name=GROUP_NAME, root_id=ROOT_ID),
    )

    assert len(toe_inputs_updated) == 1
    assert toe_inputs_updated[0].component == COMPONENT
    assert toe_inputs_updated[0].state.be_present is True
    toe_input_attacked_at = toe_inputs_updated[0].state.attacked_at
    assert toe_input_attacked_at
    assert (toe_input_attacked_at.year, toe_input_attacked_at.month, toe_input_attacked_at.day) == (
        2024,
        10,
        14,
    )
    assert toe_inputs_updated[0].state.attacked_by == user_email


@parametrize(
    args=["user_email"],
    cases=[
        ["customer_manager@gmail.com"],
        ["user@gmail.com"],
        ["reviewer@gmail.com"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email="customer_manager@gmail.com", role="customer_manager"),
                StakeholderFaker(email="user@gmail.com", role="user"),
                StakeholderFaker(email="reviewer@gmail.com", role="reviewer"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            group_access=[
                GroupAccessFaker(
                    email="customer_manager@gmail.com",
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(has_access=True, role="customer_manager"),
                ),
                GroupAccessFaker(
                    email="user@gmail.com",
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                ),
                GroupAccessFaker(
                    email="reviewer@gmail.com",
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(has_access=True, role="reviewer"),
                ),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME, id=ROOT_ID, state=GitRootStateFaker(nickname="back")
                ),
            ],
            toe_inputs=[
                ToeInputFaker(
                    component=COMPONENT,
                    entry_point=ENTRYPOINT,
                    environment_id="env_id",
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=ToeInputStateFaker(
                        be_present=False,
                        attacked_by="other_hacker@fluidattacks.com",
                    ),
                ),
            ],
        ),
    )
)
async def test_update_toe_input_access_denied(user_email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=user_email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    with raises(Exception, match="Access denied"):
        await mutate(
            _parent=None,
            info=info,
            comments="Updated toe line in group",
            group_name=GROUP_NAME,
            root_id=ROOT_ID,
            filename="test.py",
            attacked_lines=180,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email="hacker@gmail.com", role="hacker")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            group_access=[
                GroupAccessFaker(
                    email="hacker@gmail.com",
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME, id=ROOT_ID, state=GitRootStateFaker(nickname="back")
                ),
            ],
            toe_inputs=[
                ToeInputFaker(
                    component=COMPONENT,
                    entry_point="test",
                    environment_id="env_id",
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=ToeInputStateFaker(),
                ),
            ],
        ),
    ),
)
async def test_update_toe_input_not_found() -> None:
    info = GraphQLResolveInfoFaker(
        user_email="hacker@gmail.com",
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    with raises(ToeInputNotFound):
        await mutate(
            _parent=None,
            info=info,
            be_present=True,
            component=COMPONENT,
            entry_point=ENTRYPOINT,
            group_name=GROUP_NAME,
            has_recent_attack=True,
            root_id=ROOT_ID,
        )
