from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.api.types import (
    APP_EXCEPTIONS,
)
from integrates.batch import (
    dal as batch_dal,
)
from integrates.batch.enums import (
    Action,
    IntegratesBatchQueue,
)
from integrates.batch_dispatch.move_environment import (
    validate_rules,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    rename_kwargs,
    require_is_not_under_review,
    require_login,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("moveEnvironment")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_is_not_under_review,
)
@rename_kwargs(
    {
        "group_name": "source_group_name",
        "target_group_name": "group_name",
    },
)
@concurrent_decorators(enforce_group_level_auth_async, require_is_not_under_review)
@rename_kwargs(
    {
        "group_name": "target_group_name",
        "source_group_name": "group_name",
    },
)
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    **kwargs: dict,
) -> SimplePayload:
    loaders: Dataloaders = info.context.loaders
    user_info = await sessions_domain.get_jwt_content(info.context)
    try:
        user_email = user_info["user_email"]
        additional_info = {
            "source_env_id": kwargs.get("url_id"),
            "target_group_name": kwargs.get("target_group_name"),
            "target_root_id": kwargs.get("target_root_id"),
            "source_group_name": kwargs.get("group_name"),
            "source_root_id": kwargs.get("root_id"),
        }

        root = (await validate_rules(loaders, additional_info, user_email))[0]

        await batch_dal.put_action(
            action=Action.MOVE_ENVIRONMENT,
            entity=root.group_name,
            subject=user_email,
            additional_info=additional_info,
            queue=IntegratesBatchQueue.SMALL,
        )

        await batch_dal.put_action(
            action=Action.REFRESH_TOE_INPUTS,
            entity=root.group_name,
            subject=user_email,
            additional_info={"root_ids": [root.id]},
            queue=IntegratesBatchQueue.SMALL,
        )

        logs_utils.cloudwatch_log(
            info.context,
            "Moved an environment URL to another the root",
            extra={
                **additional_info,
                "log_type": "Security",
            },
        )
    except APP_EXCEPTIONS as exc:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to move an environment URL to another root",
            extra={
                "group_name": kwargs.get("group_name"),
                "root_id": kwargs.get("root_id"),
                "log_type": "Security",
            },
        )
        raise exc
    return SimplePayload(success=True)
