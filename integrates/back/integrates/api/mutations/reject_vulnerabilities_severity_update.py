from aioextensions import (
    collect,
)
from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.api.types import (
    APP_EXCEPTIONS,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_login,
)
from integrates.sessions.domain import get_jwt_content
from integrates.vulnerabilities import (
    domain as vulns_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("rejectVulnerabilitiesSeverityUpdate")
@concurrent_decorators(require_login, enforce_group_level_auth_async)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    finding_id: str,
    vulnerability_ids: list[str],
    justification: str,
) -> SimplePayload:
    try:
        user_info = await get_jwt_content(info.context)
        user_email = user_info["user_email"]
        loaders: Dataloaders = info.context.loaders
        vulnerability_ids_set = set(vulnerability_ids)
        vulnerabilities = await vulns_domain.get_by_finding_and_vuln_ids(
            loaders,
            finding_id,
            vulnerability_ids_set,
        )
        await collect(
            [
                vulns_domain.reject_severity_score_update(
                    loaders=loaders,
                    vulnerability_id=vulnerability.id,
                    modified_by=user_email,
                    justification=justification,
                )
                for vulnerability in vulnerabilities
            ],
            workers=32,
        )

        logs_utils.cloudwatch_log(
            info.context,
            "Severity update rejected for one or more vulnerabilities",
            extra={
                "finding_id": finding_id,
                "vulns_ids": vulnerability_ids_set,
                "log_type": "Security",
            },
        )
    except APP_EXCEPTIONS:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted a severity update rejection for one or more vulnerabilities",
            extra={
                "finding_id": finding_id,
                "vulns_ids": vulnerability_ids_set,
                "log_type": "Security",
            },
        )
        raise

    return SimplePayload(success=True)
