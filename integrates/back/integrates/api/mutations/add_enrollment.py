from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.custom_utils import (
    stakeholders as stakeholders_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.decorators import (
    concurrent_decorators,
    require_corporate_email,
    require_login,
)
from integrates.sessions import (
    domain as sessions_domain,
)
from integrates.stakeholders import (
    domain as stakeholders_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("addEnrollment")
@concurrent_decorators(
    require_corporate_email,
    require_login,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
) -> SimplePayload:
    loaders: Dataloaders = info.context.loaders
    user_data = await sessions_domain.get_jwt_content(info.context)
    user_email = user_data["user_email"]

    await stakeholders_domain.add_enrollment(
        loaders=loaders,
        user_email=user_email,
        full_name=stakeholders_utils.get_full_name(user_data),
    )
    logs_utils.cloudwatch_log(
        info.context,
        "Enrolled user",
        extra={"user_email": user_email, "log_type": "Security"},
    )

    return SimplePayload(success=True)
