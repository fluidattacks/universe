from datetime import datetime
from typing import TypedDict

from integrates.api.enums.core import OrderSort, VulnerabilitySort


class VulnerabilitySortInput(TypedDict):
    field: VulnerabilitySort
    order: OrderSort


class VulnerabilityFiltersInput(TypedDict, total=False):
    assignees: list[str]
    closed_after: datetime
    closed_before: datetime
    reattack: str
    reported_after: datetime
    reported_before: datetime
    search: str
    severity_rating: str
    state: list[str]
    state_not: list[str]
    tags: list[str]
    tags_not: list[str]
    technique: list[str]
    technique_not: list[str]
    treatment: list[str]
    treatment_not: list[str]
    verification: list[str]
    verification_not: list[str]
    where: str
    zero_risk: list[str]
    zero_risk_not: list[str]
