from decimal import (
    Decimal,
)

from integrates.custom_utils.validations_deco import (
    validate_severity_range_deco,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model import (
    organizations as organizations_model,
)
from integrates.db_model.organizations.types import (
    OrganizationMetadataToUpdate,
    OrganizationPriorityPolicy,
)
from integrates.organizations.utils import (
    get_organization,
)


@validate_severity_range_deco("policy.value", min_value=Decimal(-1000), max_value=Decimal(1000))
def _get_priority_policy(
    *,
    policy: OrganizationPriorityPolicy,
) -> OrganizationPriorityPolicy:
    return policy


async def remove_priority_policy(
    *,
    loaders: Dataloaders,
    organization_id: str,
    policy_to_remove: str,
) -> None:
    organization = await get_organization(loaders, organization_id)
    organization_priority_policies = (
        organization.priority_policies if organization.priority_policies else []
    )
    updated_policies_list: list[OrganizationPriorityPolicy] = [
        policy_criteria
        for policy_criteria in organization_priority_policies
        if policy_to_remove not in set(policy_criteria)
    ]

    await organizations_model.update_metadata(
        metadata=OrganizationMetadataToUpdate(priority_policies=updated_policies_list),
        organization_id=organization_id,
        organization_name=organization.name,
    )


async def update_priority_policies(
    *,
    loaders: Dataloaders,
    organization_id: str,
    updated_policies: list[OrganizationPriorityPolicy],
) -> None:
    organization = await get_organization(loaders, organization_id)
    priority_policies: list[OrganizationPriorityPolicy] = [
        _get_priority_policy(policy=policy) for policy in updated_policies
    ]

    if priority_policies != organization.priority_policies:
        await organizations_model.update_metadata(
            metadata=OrganizationMetadataToUpdate(priority_policies=updated_policies),
            organization_id=organization_id,
            organization_name=organization.name,
        )
