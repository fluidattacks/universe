from collections.abc import (
    Awaitable,
    Callable,
)
from contextlib import (
    AsyncExitStack,
)
from typing import (
    Any,
    cast,
)

from boto3 import (
    Session,
)
from opensearchpy import (
    AsyncHttpConnection,
    AsyncOpenSearch,
    AsyncTransport,
    AWSV4SignerAsyncAuth,
    JSONSerializer,
)

from integrates.context import (
    FI_AWS_OPENSEARCH_HOST,
    FI_AWS_REGION_NAME,
    FI_ENVIRONMENT,
)


class SetEncoder(JSONSerializer):
    def default(self, data: Any) -> JSONSerializer:
        if isinstance(data, set):
            return list(data)  # type: ignore
        return JSONSerializer.default(self, data)


SESSION = Session()
CLIENT_OPTIONS = {
    "connection_class": AsyncHttpConnection,
    "hosts": [FI_AWS_OPENSEARCH_HOST],
    "http_auth": AWSV4SignerAsyncAuth(
        credentials=SESSION.get_credentials(),
        region=FI_AWS_REGION_NAME,
        service="es",
    ),
    "http_compress": True,
    "max_retries": 15,
    "timeout": 10,
    "retry_on_status": (429, 502, 503, 504),
    "retry_on_timeout": True,
    "serializer": SetEncoder(),
    "use_ssl": FI_ENVIRONMENT == "production",
    "verify_certs": FI_ENVIRONMENT == "production",
}
NoArgsAsyncCallable = Callable[[], Awaitable[None]]
ClientAsyncCallable = Callable[[], Awaitable[AsyncOpenSearch]]
SearchContextTuple = tuple[NoArgsAsyncCallable, NoArgsAsyncCallable, ClientAsyncCallable]


def create_search_context() -> SearchContextTuple:
    context_stack = None
    client = None

    async def startup() -> None:
        nonlocal context_stack, client

        context_stack = AsyncExitStack()
        client = await context_stack.enter_async_context(
            AsyncOpenSearch(transport_class=AsyncTransport, **CLIENT_OPTIONS),
        )

    async def shutdown() -> None:
        nonlocal context_stack

        if context_stack:
            await context_stack.aclose()

    async def get_client_inner() -> AsyncOpenSearch:
        nonlocal client

        if client is None:
            await startup()

        return cast(AsyncOpenSearch, client)

    return startup, shutdown, get_client_inner


search_startup, search_shutdown, get_client = create_search_context()
