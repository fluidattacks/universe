from enum import (
    Enum,
)


class Channel(str, Enum):
    CALL = "CALL"
    SMS = "SMS"
    WHATSAPP = "WHATSAPP"
    EMAIL = "EMAIL"


class ResourceType(str, Enum):
    URL = "URL"
    ROOT = "ROOT"


class AuthenticationLevel(str, Enum):
    USER = "USER"
    GROUP = "GROUP"
    ORG = "ORG"
