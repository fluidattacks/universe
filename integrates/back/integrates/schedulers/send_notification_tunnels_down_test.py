from integrates.dataloaders import get_new_context
from integrates.organizations.domain import get_all_active_group_names
from integrates.organizations.utils import get_organization
from integrates.schedulers.send_notification_tunnels_down import get_number_of_ztna
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import GitRootFaker, GitRootStateFaker, GroupFaker, OrganizationFaker
from integrates.testing.mocks import mocks


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[GroupFaker(name="group1", organization_id="org1")],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    state=GitRootStateFaker(nickname="back", use_ztna=True),
                )
            ],
        ),
    )
)
async def test_get_number_of_ztna() -> None:
    loaders = get_new_context()
    assert (
        await get_number_of_ztna(
            organization=await get_organization(loaders=loaders, organization_key="ORG#org1"),
            loaders=loaders,
            all_group_names=set(await get_all_active_group_names(loaders)),
        )
        == 1
    )
