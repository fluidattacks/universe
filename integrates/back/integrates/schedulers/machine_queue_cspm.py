from time import (
    sleep,
)

from botocore.exceptions import (
    ClientError,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.roots.enums import (
    RootCloningStatus,
    RootEnvironmentUrlType,
    RootStatus,
)
from integrates.db_model.roots.types import (
    GitRoot,
    Root,
    RootEnvironmentUrlsRequest,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.jobs_orchestration.create_machine_config import (
    MachineJobOrigin,
    MachineJobTechnique,
    MachineModulesToExecute,
)
from integrates.jobs_orchestration.jobs import (
    queue_machine_job,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.schedulers.common import (
    info,
    is_machine_target,
)

SCHEDULE_MODIFIED_BY = "schedule_machine_queue_cspm@fluidattacks.com"


async def _get_root_environment_urls(loaders: Dataloaders, root: Root) -> bool:
    urls = await loaders.root_environment_urls.load(
        RootEnvironmentUrlsRequest(root_id=root.id, group_name=root.group_name),
    )

    for url in urls:
        if url.state.url_type == RootEnvironmentUrlType.CSPM:
            return True
    return False


async def get_machine_groups(loaders: Dataloaders) -> list[str]:
    groups = await orgs_domain.get_all_active_groups(loaders)
    machine_group_names = sorted([group.name for group in groups if is_machine_target(group)])
    return machine_group_names


async def get_roots_to_execute(loaders: Dataloaders, group_name: str) -> set[str]:
    return {
        root.state.nickname
        for root in await loaders.group_roots.load(group_name)
        if (
            isinstance(root, GitRoot)
            and root.state.status == RootStatus.ACTIVE
            and root.cloning.status == RootCloningStatus.OK
            and await _get_root_environment_urls(loaders=loaders, root=root)
        )
    }


@retry_on_exceptions(exceptions=(ClientError,), max_attempts=3, sleep_seconds=2.0)
async def queue_job(loaders: Dataloaders, group_name: str, root_nicknames: set[str]) -> None:
    await queue_machine_job(
        loaders=loaders,
        group_name=group_name,
        modified_by=SCHEDULE_MODIFIED_BY,
        root_nicknames=root_nicknames,
        modules_to_execute=MachineModulesToExecute(
            APK=False,
            DAST=False,
            SAST=False,
            SCA=False,
            CSPM=True,
        ),
        job_origin=MachineJobOrigin.SCHEDULER,
        job_technique=MachineJobTechnique.CSPM,
    )


async def main() -> None:
    loaders = get_new_context()
    machine_group_names = await get_machine_groups(loaders)
    info("%s groups to process", len(machine_group_names))
    for group_name in machine_group_names:
        root_nicknames = await get_roots_to_execute(loaders, group_name)
        if root_nicknames:
            targets = {
                "group_name": group_name,
                "root_nicknames": root_nicknames,
            }
            await queue_job(loaders, group_name, root_nicknames)
            info("Queued Machine CSPM executions for group", extra=targets)
            sleep(5)
