from datetime import datetime

from aioextensions import collect

from integrates import batch
from integrates.batch import dal as batch_dal
from integrates.batch.enums import Action, IntegratesBatchQueue, SkimsBatchQueue
from integrates.batch.types import BatchProcessingToUpdate, DependentAction
from integrates.schedulers import requeue_actions
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    BatchProcessingFaker,
    GitRootFaker,
    GitRootStateFaker,
    GroupFaker,
    OrganizationFaker,
)
from integrates.testing.mocks import Mock, mocks

MACHINE_EMAIL = "machine@fluidattacks.com"

BATCH_JOBS_RESPONSE = (
    {
        "container": {
            "resourceRequirements": [
                {"value": "2", "type": "VCPU"},
                {"value": "3200", "type": "MEMORY"},
            ],
        },
        "jobId": "2c95e12c-8b93-4faf-937f-1f2b34530004",
        "status": "FAILED",
    },
    {
        "container": {
            "resourceRequirements": [
                {"value": "8", "type": "VCPU"},
                {"value": "1800", "type": "MEMORY"},
            ],
        },
        "jobId": "fda5fcbe-8986-4af7-9e54-22a7d8e7981f",
        "status": "FAILED",
    },
    {
        "container": {
            "resourceRequirements": [
                {"value": "2", "type": "VCPU"},
                {"value": "1800", "type": "MEMORY"},
            ],
        },
        "jobId": "6994b21b-4270-4026-8382-27f35fb6a6e7",
        "status": "SUCCEEDED",
    },
    {
        "container": {
            "resourceRequirements": [
                {"value": "2", "type": "VCPU"},
                {"value": "1800", "type": "MEMORY"},
            ],
        },
        "jobId": "342cea18-72b5-49c0-badb-f7e38dd0e273",
        "status": "RUNNING",
    },
    {
        "container": {
            "resourceRequirements": [
                {"value": "1", "type": "VCPU"},
                {"value": "1800", "type": "MEMORY"},
            ],
        },
        "jobId": "42d5b400-89f3-498c-b7ce-cc29d2e7f254",
        "status": "RUNNABLE",
    },
    {
        "container": {
            "resourceRequirements": [
                {"value": "1", "type": "VCPU"},
                {"value": "1800", "type": "MEMORY"},
            ],
        },
        "jobId": "a69675b0-00bc-41c9-a88b-991475fa656a",
        "status": "FAILED",
        "jobName": "machine_group1_reattack_all_123456",
        "stoppedAt": "2024-01-18T17:41:00+00:00",
    },
    {
        "container": {
            "resourceRequirements": [
                {"value": "1", "type": "VCPU"},
                {"value": "1800", "type": "MEMORY"},
            ],
        },
        "jobId": "j69675b0-09bc-13c9-a88b-991475fa656a",
        "status": "FAILED",
        "jobName": "sbom_group8_scheduler_root_123456",
        "stoppedAt": "2024-01-18T17:47:00+00:00",
    },
)


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[GroupFaker(name="group1", organization_id="org1")],
            roots=[
                GitRootFaker(
                    group_name="group1", id="root1", state=GitRootStateFaker(nickname="root1")
                ),
            ],
            actions=[
                BatchProcessingFaker(
                    action_name=Action.EXECUTE_MACHINE,
                    additional_info={
                        "roots": ["nickname1", "nickname2"],
                        "roots_config_files": [
                            "config_nickname1_123.yaml",
                            "config_nickname2_456.yaml",
                        ],
                    },
                    batch_job_id="2c95e12c-8b93-4faf-937f-1f2b34530004",
                    entity="group1",
                    queue=SkimsBatchQueue.SMALL,
                    subject="unittesting@fluidattacks.com",
                    time=datetime.fromisoformat("2024-01-18T17:41:00+00:00"),
                ),
                BatchProcessingFaker(
                    action_name=Action.EXECUTE_MACHINE,
                    additional_info={
                        "roots": ["nickname1", "nickname2"],
                        "roots_config_files": [
                            "config_nickname1_123.yaml",
                            "config_nickname2_456.yaml",
                        ],
                    },
                    batch_job_id="2c95e12c-8b93-4faf-937f-1f2b34530004",
                    entity="group1",
                    queue=SkimsBatchQueue.SMALL,
                    subject="unittesting@fluidattacks.com",
                    time=datetime.fromisoformat("2024-01-18T17:41:00+00:00"),
                ),
                BatchProcessingFaker(
                    action_name=Action.EXECUTE_MACHINE,
                    additional_info={
                        "roots": ["nickname1"],
                        "roots_config_files": ["config_nickname1_123.yaml"],
                    },
                    batch_job_id="fda5fcbe-8986-4af7-9e54-22a7d8e7981f",
                    entity="group2",
                    queue=SkimsBatchQueue.SMALL,
                    subject="unittesting@fluidattacks.com",
                    time=datetime.fromisoformat("2024-01-18T17:42:00+00:00"),
                ),
                BatchProcessingFaker(
                    action_name=Action.EXECUTE_MACHINE,
                    additional_info={
                        "roots": ["nickname1"],
                        "roots_config_files": ["config_nickname1_123.yaml"],
                    },
                    batch_job_id="6994b21b-4270-4026-8382-27f35fb6a6e7",
                    entity="group3",
                    queue=SkimsBatchQueue.SMALL,
                    subject="unittesting@fluidattacks.com",
                    time=datetime.fromisoformat("2024-01-18T17:43:00+00:00"),
                ),
                BatchProcessingFaker(
                    action_name=Action.CLONE_ROOTS,
                    additional_info={"git_root_ids": ["id1", "id2"]},
                    batch_job_id="c8a18de6-2403-461c-9d77-991041a9632a",
                    entity="group4",
                    queue=IntegratesBatchQueue.SMALL,
                    subject="unittesting@fluidattacks.com",
                    time=datetime.fromisoformat("2024-01-18T17:44:00+00:00"),
                    dependent_actions=[
                        DependentAction(
                            action_name=Action.REFRESH_TOE_LINES,
                            additional_info={"root_ids": ["id1", "id2"]},
                            queue=IntegratesBatchQueue.SMALL,
                        )
                    ],
                ),
                BatchProcessingFaker(
                    action_name=Action.REFRESH_TOE_LINES,
                    additional_info={"root_ids": ["id3"]},
                    batch_job_id="342cea18-72b5-49c0-badb-f7e38dd0e273",
                    entity="group5",
                    queue=IntegratesBatchQueue.SMALL,
                    subject="unittesting@fluidattacks.com",
                    time=datetime.fromisoformat("2024-01-18T17:45:00+00:00"),
                ),
                BatchProcessingFaker(
                    action_name=Action.REFRESH_TOE_INPUTS,
                    additional_info={"root_ids": ["id4"]},
                    batch_job_id="42d5b400-89f3-498c-b7ce-cc29d2e7f254",
                    entity="group6",
                    queue=IntegratesBatchQueue.SMALL,
                    subject="unittesting@fluidattacks.com",
                    time=datetime.fromisoformat("2024-01-18T17:46:00+00:00"),
                ),
                BatchProcessingFaker(
                    action_name=Action.EXECUTE_MACHINE,
                    additional_info={
                        "roots": ["nickname1", "nickname2"],
                        "roots_config_files": ["nickname1.yaml", "nickname2.yaml"],
                    },
                    batch_job_id="a69675b0-00bc-41c9-a88b-991475fa656a",
                    entity="group7",
                    queue=SkimsBatchQueue.SMALL,
                    subject="machine@fluidattacks.com",
                    time=datetime.fromisoformat("2024-01-18T17:41:00+00:00"),
                ),
                BatchProcessingFaker(
                    action_name=Action.GENERATE_ROOT_SBOM,
                    additional_info={
                        "root_nicknames": ["nickname1"],
                        "roots_config_files": [
                            "sbom_group1_nickname1_1704067200_config.yaml",
                        ],
                        "sbom_format": "fluid-json",
                        "job_type": "scheduler",
                    },
                    batch_job_id="j69675b0-09bc-13c9-a88b-991475fa656a",
                    entity="group8",
                    queue=SkimsBatchQueue.SMALL,
                    subject="machine@fluidattacks.com",
                    time=datetime.fromisoformat("2024-01-18T17:47:00+00:00"),
                ),
            ],
        ),
    ),
    others=[
        Mock(batch.dal, "describe_jobs", "async", BATCH_JOBS_RESPONSE),
        Mock(batch.dal, "put_action_to_batch", "async", "2507485d-4a2e-4c14-a68b-fbe0c34d5f01"),
    ],
)
async def test_requeue_actions() -> None:
    actions_to_delete: tuple[tuple[Action, str], ...] = (
        (
            Action.EXECUTE_MACHINE,
            "c209ca9c4c0b0e63ff64165c6fe3bd10449e5a5d09c46180ccabdb7ad01461a4",
        ),
        (
            Action.EXECUTE_MACHINE,
            "f74b199a325233c56b267dffe569324311556b66bf7208d8c96f74c70e70ca5b",
        ),
        (
            Action.EXECUTE_MACHINE,
            "4bf35a01a2b13f38eda2814ff433926e9d06bcf41bfc1ff30c3cab844c31c449",
        ),
    )
    running_actions: tuple[tuple[Action, str], ...] = actions_to_delete + (
        (
            Action.REFRESH_TOE_LINES,
            "d0e0fb8e74a5d8589080acf353aeed440d473a130d7cd694f2686a20b724d737",
        ),
    )

    # An active action that will not have any changes
    unchanged_actions = await collect(
        batch_dal.get_action(action_key=pk)
        for pk in [
            "d0e0fb8e74a5d8589080acf353aeed440d473a130d7cd694f2686a20b724d737",
            "3a00ae205dddaabe8f05a9a28fac1bb02f2ff730975677c70346ece6a41cc14a",
        ]
    )
    batch_actions = await batch_dal.get_actions()
    assert len(batch_actions) == 8

    # create jobs in dynamo
    update_actions_result = await collect(
        batch_dal.update_action_to_dynamodb(
            action=action[0],
            action_key=action[1],
            attributes=BatchProcessingToUpdate(
                batch_job_id=str(BATCH_JOBS_RESPONSE[index]["jobId"]),
                running=True,
            ),
        )
        for index, action in enumerate(running_actions)
    )
    assert all(update_actions_result) is True

    await requeue_actions.main()
    actions = await batch_dal.get_actions()
    actions_keys = [action.key for action in actions]
    assert len(actions) == 5
    assert not any(
        action_pk_to_delete in actions_keys for _, action_pk_to_delete in actions_to_delete
    )
    clone_action = next(action for action in actions if action.action_name == Action.CLONE_ROOTS)
    assert clone_action.batch_job_id == "2507485d-4a2e-4c14-a68b-fbe0c34d5f01"
    assert clone_action.running is False
    assert any(unchanged_action in actions for unchanged_action in unchanged_actions)
