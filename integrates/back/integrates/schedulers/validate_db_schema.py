import argparse
import asyncio
import json
import logging
import os
import re
import sys
import time
from asyncio import (
    Lock,
    Queue,
    run,
)
from collections.abc import Callable
from decimal import (
    Decimal,
)
from pathlib import (
    Path,
)
from subprocess import (
    run as subprocess_run,
)
from typing import (
    Optional,
)

from aioextensions import (
    collect,
)
from fa_purity import (
    Result,
)

from integrates.class_types.types import (
    Item,
)
from integrates.db_model import (
    HISTORIC_TABLE,
    TABLE,
)
from integrates.dynamodb.operations import (
    scan_generator,
)
from integrates.dynamodb.resource import (
    dynamo_shutdown,
    dynamo_startup,
)
from integrates.dynamodb.types import (
    Facet,
    Table,
)

LOGGER = logging.getLogger(__name__)
MAX_SCAN_ITER = 10
_TARGET_TABLES = {
    "integrates_vms": TABLE,
    "integrates_vms_historic": HISTORIC_TABLE,
}
METADATA_FAILS_DIR = "validation_failures"
QUEUE_PROCESSORS = 4
CUE_PROCESSORS = 4


class UnknownTableError(Exception):
    pass


class CueValidationError(Exception):
    def __init__(
        self,
        failed_data: bytes,
        schema_path: str,
        stderr: bytes,
        expression: str,
    ) -> None:
        self.failed_data = failed_data
        self.schema_path = schema_path
        self.stderr = stderr
        self.expression = expression
        super().__init__(f"Failed to validate schema: {self.schema_path}")


class InvalidSchemaBasePathError(Exception):
    pass


class JsonDBEncoder(json.JSONEncoder):
    def default(self, o: Decimal | set | str) -> Item | float | int:
        if isinstance(o, Decimal):
            if o % 1 > 0:
                return float(o)
            return int(o)
        if isinstance(o, set):
            return {"S": list(o)}
        return super().default(o)


async def _validate_with_cue(
    schemas_base: Path,
    schema_path: Path,
    data: bytes,
    expression: str,
) -> Result[None, CueValidationError]:
    out = subprocess_run(
        # use of subprocess is safe here as we are not executing any user input
        ["cue", "vet", schema_path, "-d", expression, "-c"],
        input=data,
        capture_output=True,
        check=False,
        cwd=schemas_base,
    )
    if out.returncode != 0:
        return Result.failure(
            CueValidationError(
                failed_data=data,
                schema_path=str(schema_path),
                stderr=out.stderr,
                expression=expression,
            ),
        )
    return Result.success(None)


def _is_key_alias(key: str) -> bool:
    return bool(re.match(r"^[A-Z]+$", key))


def _find_facet(
    facets: dict[str, Facet],
    item_pk: str,
    item_sk: str,
) -> Optional[str]:
    queried_pk_aliases = set(filter(_is_key_alias, item_pk.split("#")))
    queried_sk_aliases = set(filter(_is_key_alias, item_sk.split("#")))
    for facet_name, facet in facets.items():
        design_pk_aliases = set(filter(_is_key_alias, facet.pk_alias.split("#")))
        design_sk_aliases = set(filter(_is_key_alias, facet.sk_alias.split("#")))
        if queried_pk_aliases == design_pk_aliases and queried_sk_aliases == design_sk_aliases:
            return facet_name
    return None


def _split_facets(table: Table, content: list[Item]) -> tuple[dict[str, list[Item]], list[Item]]:
    """Splits the content into facets and finds non-matching items."""
    split_data = {}
    non_matching_items: list[Item] = []

    for item in content:
        facet = _find_facet(table.facets, item["pk"], item["sk"])
        if facet is None:
            non_matching_items.append(item)
            continue
        if facet not in split_data:
            split_data[facet] = [item]
        else:
            split_data[facet].append(item)
    return split_data, non_matching_items


def _build_facet_schema_path(facet_name: str, table: str, schemas_base: Path) -> Path:
    return Path(os.path.join(schemas_base, "tables", table, "facets", f"{facet_name}.cue"))


def _snake_to_pascal(snake_str: str) -> str:
    components = snake_str.split("_")
    return "".join(x.title() for x in components)


def _build_facet_schema_expression(facet_name: str) -> str:
    facet_definition = _snake_to_pascal(facet_name.replace("metadata", ""))
    return f"[...#{facet_definition}Item]"


async def _validate_schema(
    facet_name: str,
    table_name: str,
    items: list[Item],
    schemas_base: Path,
) -> bool:
    schema_path = _build_facet_schema_path(facet_name, table_name, schemas_base=schemas_base)
    expression = _build_facet_schema_expression(facet_name)
    encoded_data = str.encode(json.dumps(items, cls=JsonDBEncoder, ensure_ascii=False))
    result = await _validate_with_cue(
        schemas_base=schemas_base,
        schema_path=schema_path,
        data=encoded_data,
        expression=expression,
    )
    return (
        result.bind(_success_handler(facet_name))
        .alt(_failure_handler(facet_name))
        .to_coproduct()
        .map(lambda _: True, lambda _: False)
    )


def _handle_table_arg(table_name: str) -> Table:
    LOGGER.info("Processing table: %s", table_name)
    if table_name not in _TARGET_TABLES:
        raise UnknownTableError(
            f"Unknown table: {table_name}. Please choose one in: {_TARGET_TABLES.keys()}",
        )
    return _TARGET_TABLES[table_name]


def _handle_schema_base_arg(base: str) -> Path:
    if Path(base).is_dir():
        return Path(base)
    raise InvalidSchemaBasePathError(f"Invalid schema base path: {base}")


def _save_fail_metadata(
    err: CueValidationError,
) -> tuple[str, str]:
    os.makedirs(METADATA_FAILS_DIR, exist_ok=True)
    epoch = str(int(time.time()))
    base_name = f"{err.schema_path.split('/')[-1].split('.')[0]}_{epoch}"
    data_file_path = os.path.join(
        METADATA_FAILS_DIR,
        base_name + ".json",
    )
    err_file_path = os.path.join(
        METADATA_FAILS_DIR,
        base_name + ".err",
    )
    with open(data_file_path, "wb") as data_file, open(err_file_path, "wb") as err_file:
        data_file.write(json.dumps(json.loads(err.failed_data), indent=4).encode())
        err_file.write(err.stderr)
    return data_file_path, err_file_path


def _success_handler(
    facet_name: str,
) -> Callable[[None], Result[None, CueValidationError]]:
    def _handle(succ: None) -> Result[None, CueValidationError]:
        LOGGER.info(
            "\u2705 Facet %s validated successfully.",
            facet_name,
        )
        return Result.success(succ)

    return _handle


def _failure_handler(
    facet_name: str,
) -> Callable[[CueValidationError], Result[None, CueValidationError]]:
    def _handle(err: CueValidationError) -> Result[None, CueValidationError]:
        data_path, err_path = _save_fail_metadata(err)
        LOGGER.error(
            "\u274c Facet %s validated as FAIL. Saved data to %s and error to %s",
            facet_name,
            os.path.abspath(data_path),
            os.path.abspath(err_path),
        )
        return Result.failure(err)

    return _handle


async def _safe_extend_list(lst: list, items: list, lock: Lock) -> None:
    async with lock:
        lst.extend(items)


async def _queue_worker(
    *,
    table: Table,
    schemas_base: Path,
    queue: Queue,
    global_non_matching: list[Item],
    non_matching_lock: Lock,
    global_results: list[bool],
    results_lock: Lock,
) -> None:
    while True:
        scanned_data = await queue.get()
        facets, non_matching = _split_facets(table, scanned_data)
        await _safe_extend_list(global_non_matching, non_matching, non_matching_lock)
        results = list(
            await collect(
                [
                    _validate_schema(
                        facet_name=facet_name,
                        table_name=table.name,
                        items=facet_items,
                        schemas_base=schemas_base,
                    )
                    for facet_name, facet_items in facets.items()
                ],
                workers=CUE_PROCESSORS,
            ),
        )
        await _safe_extend_list(global_results, results, results_lock)
        queue.task_done()


async def _run_table_validation(table: Table, schemas_base: Path) -> None:
    count = 0
    queue: Queue[list[Item]] = Queue()
    global_non_matching: list[Item] = []
    non_matching_lock = Lock()
    results: list[bool] = []
    results_lock = Lock()
    try:
        await dynamo_startup()
        tasks = [
            asyncio.create_task(
                _queue_worker(
                    table=table,
                    schemas_base=schemas_base,
                    queue=queue,
                    global_non_matching=global_non_matching,
                    non_matching_lock=non_matching_lock,
                    global_results=results,
                    results_lock=results_lock,
                ),
            )
            for _ in range(QUEUE_PROCESSORS)
        ]

        async for scanned_data in scan_generator(table=table):
            if count >= MAX_SCAN_ITER:
                break
            queue.put_nowait(scanned_data)
            LOGGER.info("Scanning page  %s", count + 1)
            count += 1

        await queue.join()

        for task in tasks:
            task.cancel()

        await asyncio.gather(*tasks, return_exceptions=True)

    finally:
        await dynamo_shutdown()
    if not all(results):
        sys.exit(1)


async def main() -> None:
    parser = argparse.ArgumentParser()
    parser.add_argument("--table", type=str, required=True)
    parser.add_argument("--schema-base-path", type=str, required=True)
    args = parser.parse_args()
    table = _handle_table_arg(args.table)
    schemas_base = _handle_schema_base_arg(args.schema_base_path)
    await _run_table_validation(table, schemas_base)


if __name__ == "__main__":
    LOGGER.info("Starting schema validation...")
    run(main())
