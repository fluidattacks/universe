from integrates.custom_exceptions import (
    MailerClientError,
    UnableToSendMail,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.custom_utils.registered_domain import (
    get_managers_company_domain,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    stakeholders as stakeholders_model,
)
from integrates.db_model.stakeholders.types import Stakeholder
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.mailer import (
    trial as trial_mail,
)

# Constants
INACTIVE_HOURS = [1, 24, 48]


mail_abandoned_trial = retry_on_exceptions(
    exceptions=(UnableToSendMail, MailerClientError),
    max_attempts=4,
    sleep_seconds=2,
)(trial_mail.send_mail_abandoned_trial)

mail_abandoned_trial_2 = retry_on_exceptions(
    exceptions=(UnableToSendMail, MailerClientError),
    max_attempts=4,
    sleep_seconds=2,
)(trial_mail.send_mail_abandoned_trial_2)

mail_abandoned_trial_3 = retry_on_exceptions(
    exceptions=(UnableToSendMail, MailerClientError),
    max_attempts=4,
    sleep_seconds=2,
)(trial_mail.send_mail_abandoned_trial_3)


async def get_stakeholders_to_notify(loaders: Dataloaders) -> list[tuple[Stakeholder, float]]:
    return [
        (stakeholder, delta_hours)
        for stakeholder in await stakeholders_model.get_all_stakeholders()
        if (
            not stakeholder.enrolled
            and stakeholder.registration_date
            and not (
                await get_managers_company_domain(loaders=loaders, user_email=stakeholder.email)
            )
            and (
                delta_hours := (
                    datetime_utils.get_utc_now() - stakeholder.registration_date
                ).total_seconds()
                // 3600
            )
            and delta_hours in INACTIVE_HOURS
        )
    ]


async def send_abandoned_trial_notification() -> None:
    loaders = get_new_context()
    stakeholders = await get_stakeholders_to_notify(loaders)
    for stakeholder, delta_hours in stakeholders:
        if delta_hours == 1:
            await mail_abandoned_trial(loaders, stakeholder.email)
        if delta_hours == 24:
            await mail_abandoned_trial_2(loaders, stakeholder.email)
        if delta_hours == 48:
            await mail_abandoned_trial_3(loaders, stakeholder.email)


async def main() -> None:
    await send_abandoned_trial_notification()
