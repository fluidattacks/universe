from aioextensions import (
    collect,
)
from aiohttp import (
    ClientConnectorError,
)
from aiohttp.client_exceptions import (
    ClientPayloadError,
)
from botocore.exceptions import (
    ClientError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
)

from integrates.custom_utils.vulnerabilities import (
    is_machine_vuln,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.vulnerabilities.types import (
    FindingVulnerabilitiesRequest,
    Vulnerability,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.dynamodb.exceptions import (
    UnavailabilityError,
)
from integrates.findings.domain import (
    core as finding_domain,
)
from integrates.jobs_orchestration.create_machine_config import (
    MachineModulesToExecute,
)
from integrates.jobs_orchestration.jobs import (
    get_finding_code_from_title,
    get_techniques_to_execute,
    queue_machine_reattack,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.schedulers.common import (
    info,
    is_machine_target,
)
from integrates.vulnerabilities.domain.utils import (
    get_vulnerabilities_paths_by_root,
)

SCHEDULE_MODIFIED_BY = "schedule_machine_queue_re_attacks@fluidattacks.com"


async def get_group_vulns_to_reattack(
    loaders: Dataloaders,
    group_name: str,
) -> tuple[list[str], MachineModulesToExecute, dict[str, list[str]]] | None:
    info("Processing group", extra={"group_name": (group_name)})

    machine_findings = [
        finding
        for finding in await loaders.group_findings.load(group_name)
        if finding.creation
        and finding.creation.modified_by == "machine@fluidattacks.com"
        and get_finding_code_from_title(finding.title)
    ]
    findings_vulns_c = await loaders.finding_vulnerabilities_to_reattack_c.load_many(
        [FindingVulnerabilitiesRequest(finding_id=finding.id) for finding in machine_findings],
    )

    finding_codes_to_reattack: set[str] = set()
    vulns_to_reattack: list[Vulnerability] = []

    for finding, vulns_c in zip(machine_findings, findings_vulns_c, strict=False):
        finding_vulns_to_reattack = [
            edge.node
            for edge in vulns_c.edges
            if is_machine_vuln(edge.node) and not finding_domain.check_hold(edge.node)
        ]
        if not finding_vulns_to_reattack or not (
            title := get_finding_code_from_title(finding.title)
        ):
            continue

        finding_codes_to_reattack.add(title)
        vulns_to_reattack.extend(finding_vulns_to_reattack)

    if not finding_codes_to_reattack or not vulns_to_reattack:
        return None

    modules_to_execute = get_techniques_to_execute(vulns_to_reattack)
    reported_paths_by_root = await get_vulnerabilities_paths_by_root(
        loaders,
        group_name,
        vulns_to_reattack,
    )

    info(
        "Group reattack information",
        extra={
            "group_name": group_name,
            "finding_codes": finding_codes_to_reattack,
            "root_nicknames": list(reported_paths_by_root.keys()),
            "len_vulns": len(vulns_to_reattack),
        },
    )

    return (
        list(finding_codes_to_reattack),
        modules_to_execute,
        reported_paths_by_root,
    )


@retry_on_exceptions(
    exceptions=(
        ClientConnectorError,
        ClientError,
        ClientPayloadError,
        ConnectionResetError,
        ConnectTimeoutError,
        HTTPClientError,
        UnavailabilityError,
        ReadTimeoutError,
    ),
    sleep_seconds=40,
    max_attempts=5,
)
async def queue_group_re_attacks(
    loaders: Dataloaders,
    group_name: str,
) -> None:
    re_attack_info = await get_group_vulns_to_reattack(loaders, group_name)

    if not re_attack_info:
        info("No vulnerabilities to reattack in group %s", group_name)
        return

    batch_action = await queue_machine_reattack(
        loaders=loaders,
        finding_codes=re_attack_info[0],
        group_name=group_name,
        modified_by=SCHEDULE_MODIFIED_BY,
        modules_to_execute=re_attack_info[1],
        reported_paths_by_root=re_attack_info[2],
        clone_before=False,
    )
    info(
        "Reattack queued",
        extra={"batch_action": batch_action, "group_name": group_name},
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    groups = await orgs_domain.get_all_active_groups(loaders)
    machine_group_names = sorted([group.name for group in groups if is_machine_target(group)])
    await collect(
        [queue_group_re_attacks(loaders, group_name) for group_name in machine_group_names],
        workers=8,
    )
