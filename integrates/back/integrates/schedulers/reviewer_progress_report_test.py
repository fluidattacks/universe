from collections import defaultdict
from datetime import datetime
from decimal import Decimal

import pytz

from integrates.custom_utils.datetime import get_report_dates
from integrates.dataloaders import get_new_context
from integrates.db_model.enums import Source
from integrates.db_model.findings.enums import FindingStateStatus, FindingStatus
from integrates.db_model.findings.types import FindingVulnerabilitiesSummary
from integrates.db_model.groups.enums import GroupService
from integrates.db_model.types import SeverityScore
from integrates.db_model.vulnerabilities.enums import VulnerabilityStateStatus, VulnerabilityType
from integrates.db_model.vulnerabilities.types import Vulnerability
from integrates.schedulers.reviewer_progress_report import (
    EmailContent,
    _format_where,
    _get_reported_date_days,
    _load_report_info_by_group,
    main,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    FindingFaker,
    FindingStateFaker,
    FindingUnreliableIndicatorsFaker,
    GitRootFaker,
    GitRootStateFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    GroupStateFaker,
    OrganizationFaker,
    VulnerabilityFaker,
    VulnerabilityStateFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import freeze_time

ORG_ID = "ORG#123"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id=ORG_ID, name="orgtest"),
            ],
            groups=[
                GroupFaker(
                    name="group1",
                    organization_id=ORG_ID,
                    state=GroupStateFaker(service=GroupService.WHITE),
                ),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name="group1",
                    email="hacker@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
            ],
            findings=[
                FindingFaker(
                    id="3c475384-834c-47b0-ac71-a41a022e401c",
                    group_name="group1",
                    state=FindingStateFaker(
                        modified_by="test1@gmail.com",
                        modified_date=datetime.fromisoformat("2017-04-09T05:45:11+00:00"),
                        source=Source.ASM,
                        status=FindingStateStatus.CREATED,
                    ),
                    title="001. SQL injection - C Sharp SQL API",
                    recommendation="Updated recommendation",
                    description="I just have updated the description",
                    severity_score=SeverityScore(
                        base_score=Decimal("4.5"),
                        temporal_score=Decimal("4.1"),
                        cvss_v3=(
                            "CVSS:3.1/AV:P/AC:H/PR:L/UI:N/S:C/C:L/I:L/A:L/"
                            "E:P/RL:O/CR:L/AR:H/MAV:N/MAC:H/MPR:H/MUI:R/MS:U/"
                            "MC:L/MA:L"
                        ),
                        threat_score=Decimal("1.1"),
                        cvssf_v4=Decimal("0.018"),
                        cvss_v4="CVSS:4.0/AV:P/AC:H/AT:N/PR:L/UI:N/VC:L/VI:L/"
                        "VA:L/SC:L/SI:L/SA:L/E:P/AR:H/MAV:N/MAC:H/MPR:H/MUI:P"
                        "/MVC:L/MVA:L",
                        cvssf=Decimal("1.149"),
                    ),
                    threat="Updated threat",
                    attack_vector_description=("This is an updated attack vector"),
                    unreliable_indicators=FindingUnreliableIndicatorsFaker(
                        unreliable_newest_vulnerability_report_date=datetime.fromisoformat(
                            "2020-12-26T05:45:00+00:00"
                        ),
                        unreliable_oldest_open_vulnerability_report_date=(
                            datetime.fromisoformat("2020-02-24T05:45:00+00:00")
                        ),
                        unreliable_oldest_vulnerability_report_date=(
                            datetime.fromisoformat("2018-04-01T05:45:00+00:00")
                        ),
                        unreliable_status=FindingStatus.VULNERABLE,
                        unreliable_where="192.168.1.2",
                        vulnerabilities_summary=FindingVulnerabilitiesSummary(
                            closed=3,
                            open=5,
                            submitted=0,
                            rejected=0,
                            open_critical=0,
                            open_high=1,
                            open_low=2,
                            open_medium=2,
                            open_critical_v3=0,
                            open_high_v3=1,
                            open_low_v3=2,
                            open_medium_v3=2,
                        ),
                    ),
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    created_by="hacker@gmail.com",
                    created_date=datetime.fromisoformat("2018-04-09T05:45:15+00:00"),
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                    group_name="group1",
                    organization_name="orgtest",
                    hacker_email="hacker@gmail.com",
                    id="be09edb7-cd5c-47ed-bee4-97c645acdce10",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    state=VulnerabilityStateFaker(
                        modified_by="reviewer@fluidattacks.com",
                        modified_date=datetime.fromisoformat("2018-04-09T05:45:15+00:00"),
                        source=Source.ASM,
                        specific="9999",
                        status=VulnerabilityStateStatus.REJECTED,
                        where="192.168.1.20",
                    ),
                    type=VulnerabilityType.PORTS,
                ),
                VulnerabilityFaker(
                    created_by="hacker@gmail.com",
                    created_date=datetime.fromisoformat("2018-04-09T05:45:15+00:00"),
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                    group_name="group1",
                    organization_name="orgtest",
                    hacker_email="hacker@gmail.com",
                    id="be09edb7-cd5c-47ed-bee4-97c645acdce11",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    state=VulnerabilityStateFaker(
                        modified_by="hacker@gmail.com",
                        modified_date=datetime.fromisoformat("2018-04-09T05:45:15+00:00"),
                        source=Source.ASM,
                        specific="9999",
                        status=VulnerabilityStateStatus.SUBMITTED,
                        where="192.168.1.20",
                    ),
                    type=VulnerabilityType.PORTS,
                ),
            ],
        ),
    )
)
@freeze_time("2018-04-10T10:00:00.00")
async def test_reviewer_progress_report() -> None:
    await main(datetime.now())

    date_range = get_report_dates(datetime.now()).today
    email_content: EmailContent = {
        "global_info": {
            "oldest_rejected": None,
            "rejected_count": 0,
            "submitted_count": 0,
        },
        "stakeholders_info": defaultdict(lambda: {"rejected_count": 0, "vulnerable_count": 0}),
    }

    await _load_report_info_by_group(
        date_range=date_range,
        email_content=email_content,
        allowed_roles={"hacker"},
        group_name="group1",
    )

    assert email_content["global_info"]["rejected_count"] == 1
    assert email_content["global_info"]["submitted_count"] == 1


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id=ORG_ID),
            ],
            groups=[
                GroupFaker(
                    name="group1",
                    organization_id=ORG_ID,
                ),
            ],
            findings=[
                FindingFaker(
                    group_name="group1",
                    id="finding1",
                    title="Test Finding",
                ),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    state=GitRootStateFaker(nickname="test-repo"),
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    finding_id="finding1",
                    group_name="group1",
                    id="vuln1",
                    organization_name=ORG_ID,
                    root_id="root1",
                    type=VulnerabilityType.LINES,
                    state=VulnerabilityStateFaker(
                        status=VulnerabilityStateStatus.REJECTED,
                        where="src/main.py",
                    ),
                ),
            ],
        ),
    )
)
async def test_format_where() -> None:
    loaders = get_new_context()
    vulnerability = await loaders.vulnerability.load("vuln1")
    root_nicknames = {
        root.id: root.state.nickname for root in await loaders.group_roots.load("group1")
    }

    assert isinstance(vulnerability, Vulnerability)
    formatted_where = _format_where(vulnerability, root_nicknames)
    assert formatted_where == "test-repo/src/main.py"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            groups=[GroupFaker(name="group1", organization_id=ORG_ID)],
            findings=[FindingFaker(group_name="group1", id="finding1")],
            vulnerabilities=[
                VulnerabilityFaker(
                    finding_id="finding1",
                    group_name="group1",
                    id="vuln1",
                    organization_name=ORG_ID,
                    state=VulnerabilityStateFaker(
                        status=VulnerabilityStateStatus.REJECTED,
                    ),
                    created_date=datetime(2023, 1, 1, tzinfo=pytz.UTC),
                ),
            ],
        ),
    )
)
@freeze_time("2023-04-01")
async def test_get_reported_date_days() -> None:
    reported_date = datetime(2023, 1, 1, tzinfo=pytz.UTC)
    days = _get_reported_date_days(reported_date)
    assert days == "90 days ago"

    old_date = datetime(2022, 1, 1, tzinfo=pytz.UTC)
    months = _get_reported_date_days(old_date)
    assert months == "+15 months ago"
