import re
from contextlib import suppress
from itertools import chain
from typing import NamedTuple

from aioextensions import (
    collect,
)

from integrates.archive.client import (
    snowflake_db_cursor,
)
from integrates.custom_exceptions import (
    ToeLinesAlreadyUpdated,
)
from integrates.custom_utils import (
    bugsnag as bugsnag_utils,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.custom_utils.criteria import (
    CRITERIA_VULNERABILITIES,
)
from integrates.custom_utils.exceptions import (
    NETWORK_ERRORS,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    toe_lines as toe_lines_model,
)
from integrates.db_model.roots.types import (
    Root,
)
from integrates.db_model.toe_lines.types import (
    GroupToeLinesRequest,
    SortsSuggestion,
    ToeLine,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityType,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.findings import (
    domain as findings_domain,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.schedulers.common import (
    get_active_toe_lines,
    info,
)

bugsnag_utils.start_scheduler_session()


class LineVulns(NamedTuple):
    full_where: str
    title: str


@retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=10,
    max_attempts=10,
)
async def update_toe_lines(
    current_value: ToeLine,
    sorts_suggestions: list[SortsSuggestion],
) -> None:
    with suppress(ToeLinesAlreadyUpdated):
        await toe_lines_model.update_state(
            current_value=current_value,
            new_state=current_value.state._replace(
                modified_date=datetime_utils.get_utc_now(),
                sorts_suggestions=sorts_suggestions,
            ),
        )


def filter_valid_titles(titles: list[str]) -> list[str]:
    valid_titles: list[str] = []
    vulns_info = CRITERIA_VULNERABILITIES
    for title in titles:
        if re.match(r"^\d{3}\. .+", title):
            try:
                vuln_number: str = title[:3]
                expected_vuln_title: str = vulns_info[vuln_number]["en"]["title"]
                if title == f"{vuln_number}. {expected_vuln_title}":
                    valid_titles.append(title)
            except KeyError:
                continue

    return valid_titles


async def update_sorts_attributes(
    vuln_types_by_file: dict[str, set[str]],
    toe_lines_dict: dict[str, ToeLine],
) -> None:
    updates = []
    with snowflake_db_cursor() as cursor:
        cursor.execute(
            """
            SELECT antecedents, consequents, confidence
            FROM sorts.association_rules
            ORDER BY confidence DESC
            """
        )
        association_rules = cursor.fetchall()

    for filename, findings in vuln_types_by_file.items():
        sorts_suggestions: list[SortsSuggestion] = []
        for rule in association_rules:
            antecedents = rule[0].split(", ")
            consequents = filter_valid_titles(rule[1].split(", "))
            if all(item in findings for item in antecedents):
                for consequent in consequents:
                    suggestion = SortsSuggestion(
                        finding_title=consequent,
                        probability=int(rule[2] * 100),
                    )
                    sorts_suggestions.append(suggestion)
            if len(sorts_suggestions) >= 5:
                break
        if sorts_suggestions:
            updates.append(update_toe_lines(toe_lines_dict[filename], sorts_suggestions))

    await collect(tuple(updates), workers=64)


def _get_vuln_types_by_file(
    vulnerabilities: tuple[Vulnerability, ...],
    roots_nickname_dict: dict[str, str],
    toe_lines_dict: dict[str, ToeLine],
    finding_titles_dict: dict[str, str],
) -> dict[str, set[str]]:
    line_vulnerabilities: list[LineVulns] = []
    for vuln in vulnerabilities:
        if vuln.type == VulnerabilityType.LINES:
            try:
                line_vulnerabilities.append(
                    LineVulns(
                        full_where=f"{roots_nickname_dict[vuln.root_id]}/{vuln.state.where}",
                        title=finding_titles_dict[vuln.finding_id],
                    )
                )
            except KeyError as exc:
                info(
                    f"Error creating LineVulns for vulnerability {vuln.id}",
                    extra={"extra": {"error": exc}},
                )

    vuln_types_by_file: dict[str, set[str]] = {}
    for line_vuln in line_vulnerabilities:
        if line_vuln.full_where in toe_lines_dict:
            if vuln_types_by_file.get(line_vuln.full_where, []):
                vuln_types_by_file[line_vuln.full_where].add(line_vuln.title)
            else:
                vuln_types_by_file[line_vuln.full_where] = set()
                vuln_types_by_file[line_vuln.full_where].add(line_vuln.title)

    return vuln_types_by_file


async def process_group(group_name: str) -> None:
    loaders: Dataloaders = get_new_context()
    roots: list[Root] = await loaders.group_roots.load(group_name)
    roots_nickname_dict: dict[str, str] = {}
    for root in roots:
        roots_nickname_dict[root.id] = root.state.nickname

    toe_lines = await loaders.group_toe_lines.load_nodes(
        GroupToeLinesRequest(group_name=group_name, be_present=True),
    )
    active_toe_lines = get_active_toe_lines(toe_lines, roots)
    toe_lines_dict = {}
    for toe_line in active_toe_lines:
        toe_lines_dict[f"{roots_nickname_dict[toe_line.root_id]}/{toe_line.filename}"] = toe_line

    finding_titles_dict: dict[str, str] = {}
    findings = await loaders.group_findings.load(group_name)
    for finding in findings:
        finding_titles_dict[finding.id] = finding.title

    vulnerabilities: tuple[Vulnerability, ...] = tuple(
        chain.from_iterable(
            await collect(
                tuple(
                    findings_domain.get_all_vulnerabilities(loaders, finding_id)
                    for finding_id in finding_titles_dict.keys()
                ),
                workers=32,
            ),
        ),
    )
    vuln_types_by_file = _get_vuln_types_by_file(
        vulnerabilities,
        roots_nickname_dict,
        toe_lines_dict,
        finding_titles_dict,
    )

    try:
        await update_sorts_attributes(
            vuln_types_by_file,
            toe_lines_dict,
        )
    except NETWORK_ERRORS as exc:
        info(
            f"Group {group_name} could not be updated",
            extra={"extra": {"error": exc}},
        )
    else:
        info(f"Toeline vulnerability suggestions for {group_name} updated")


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    group_names = sorted(await orgs_domain.get_all_active_group_names(loaders))
    info(f"There are {len(group_names)} groups to process")
    for group in group_names:
        info(f"Processing group: {group}")
        await process_group(group)
