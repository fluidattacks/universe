import logging
import os

from aioextensions import collect, run

from integrates.custom_utils.roots import get_active_git_roots
from integrates.dataloaders import get_new_context
from integrates.groups.languages_distribution import (
    get_root_code_languages,
    update_language_indicators,
)
from integrates.organizations import domain as orgs_domain

LOGGER = logging.getLogger(__name__)


async def process_group(group_name: str) -> None:
    loaders = get_new_context()
    valid_roots = await get_active_git_roots(loaders, group_name)
    LOGGER.info(
        "Updating language stats",
        extra={"extra": {"group_name": group_name, "len_valid_roots": len(valid_roots)}},
    )
    roots_languages = await collect(
        [get_root_code_languages(git_root) for git_root in valid_roots],
        workers=os.cpu_count() or 1,
    )
    await update_language_indicators(group_name, valid_roots, roots_languages)


async def main() -> None:
    loaders = get_new_context()
    group_names = sorted(await orgs_domain.get_all_active_group_names(loaders))
    await collect([process_group(group_name) for group_name in group_names], workers=1)


if __name__ == "__main__":
    run(main())
