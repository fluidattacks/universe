from contextlib import suppress

from integrates.context import FI_MANUAL_CLONING_PROJECTS
from integrates.custom_exceptions import (
    CredentialNotFound,
    ErrorUpdatingCredential,
    GroupNotFound,
    InvalidParameter,
    RootAlreadyCloning,
)
from integrates.custom_utils.constants import SCHEDULE_CLONE_GROUPS_ROOTS_EMAIL
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.organizations import domain as orgs_domain
from integrates.roots import domain as roots_domain
from integrates.schedulers.common import is_machine_target


async def _queue_sync_git_roots(
    *,
    loaders: Dataloaders,
    group_name: str,
) -> None:
    with suppress(
        CredentialNotFound,
        ErrorUpdatingCredential,
        GroupNotFound,
        InvalidParameter,
        RootAlreadyCloning,
    ):
        await roots_domain.queue_sync_git_roots(
            loaders=loaders,
            group_name=group_name,
            modified_by=SCHEDULE_CLONE_GROUPS_ROOTS_EMAIL,
            should_queue_machine=True,
        )


async def clone_groups_roots() -> None:
    loaders: Dataloaders = get_new_context()
    groups = await orgs_domain.get_all_active_groups(loaders)
    machine_groups = sorted(
        [
            group.name
            for group in groups
            if is_machine_target(group) and group.name not in FI_MANUAL_CLONING_PROJECTS.split(",")
        ],
    )
    for group in machine_groups:
        await _queue_sync_git_roots(
            loaders=loaders,
            group_name=group,
        )


async def main() -> None:
    await clone_groups_roots()
