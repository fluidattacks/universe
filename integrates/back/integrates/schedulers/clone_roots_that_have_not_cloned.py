from datetime import datetime

import pytz
from aioextensions import collect

from integrates.context import FI_MANUAL_CLONING_PROJECTS
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.roots.enums import RootCloningStatus, RootStatus
from integrates.db_model.roots.types import GitRoot
from integrates.organizations import domain as orgs_domain
from integrates.roots.utils import (
    disgregate_roots_for_queues,
    queue_sync_root_cluster,
    queue_sync_roots_batch,
    update_root_cloning_status,
)
from integrates.schedulers.common import info, is_machine_target

SCHEDULE_MODIFIED_BY = "schedule_clone_roots_not_cloned@fluidattacks.com"


async def get_machine_groups(loaders: Dataloaders) -> list[str]:
    groups = await orgs_domain.get_all_active_groups(loaders)
    machine_groups = sorted(
        [
            group.name
            for group in groups
            if is_machine_target(group) and group.name not in FI_MANUAL_CLONING_PROJECTS.split(",")
        ],
    )
    info(
        "Groups to process",
        extra={"groups": machine_groups, "len_groups": len(machine_groups)},
    )
    return machine_groups


def _is_toe_and_rebase_synced(root: GitRoot) -> bool:
    toe_lines_commit = root.toe_lines.commit if root.toe_lines else None
    rebase_commit = root.rebase.commit if root.rebase else None

    return root.cloning.commit == toe_lines_commit == rebase_commit


async def get_roots_to_execute(
    loaders: Dataloaders, group_name: str
) -> tuple[list[str], list[str]] | None:
    not_cloned_roots = [
        root
        for root in await loaders.group_roots.load(group_name)
        if isinstance(root, GitRoot)
        and root.state.status == RootStatus.ACTIVE
        and root.cloning.status
        in (
            RootCloningStatus.QUEUED,
            RootCloningStatus.CLONING,
            RootCloningStatus.OK,
        )
        and (
            (
                (
                    datetime.utcnow().replace(tzinfo=pytz.UTC)
                    - root.cloning.modified_date.replace(tzinfo=pytz.UTC)
                ).days
                >= 3
            )
            or not _is_toe_and_rebase_synced(root)
        )
    ]
    if not not_cloned_roots:
        return None

    info(
        "Found roots to clone",
        extra={
            "group_name": group_name,
            "roots": [(root.id, root.state.nickname) for root in not_cloned_roots],
        },
    )
    root_ids_batch, root_ids_cluster = disgregate_roots_for_queues(
        queue_on_batch=True,
        roots=not_cloned_roots,
    )

    return root_ids_batch, root_ids_cluster


async def process_group(loaders: Dataloaders, group_name: str) -> None:
    not_cloned_roots = await get_roots_to_execute(loaders, group_name)
    if not not_cloned_roots:
        return

    root_ids_batch, root_ids_cluster = not_cloned_roots

    if root_ids_batch:
        batch_action = await queue_sync_roots_batch(
            group_name=group_name,
            git_root_ids=root_ids_batch,
            modified_by=SCHEDULE_MODIFIED_BY,
            should_queue_machine=False,
            should_queue_sbom=False,
        )
        if batch_action.batch_job_id:
            await collect(
                update_root_cloning_status(
                    loaders=loaders,
                    group_name=group_name,
                    root_id=root_id,
                    status=RootCloningStatus.QUEUED,
                    message=f"With job {batch_action.batch_job_id}",
                    modified_by=SCHEDULE_MODIFIED_BY,
                )
                for root_id in root_ids_batch
            )

    if root_ids_cluster:
        await collect(
            queue_sync_root_cluster(
                group_name=group_name,
                git_root_id=root_id,
                modified_by=SCHEDULE_MODIFIED_BY,
                should_queue_machine=False,
                should_queue_sbom=False,
            )
            for root_id in root_ids_cluster
        )
        await collect(
            update_root_cloning_status(
                loaders=loaders,
                group_name=group_name,
                root_id=root_id,
                status=RootCloningStatus.QUEUED,
                message="On SQS cloning queue",
                modified_by=SCHEDULE_MODIFIED_BY,
            )
            for root_id in root_ids_cluster
        )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    machine_groups = await get_machine_groups(loaders)
    await collect(
        [process_group(loaders, group_name) for group_name in machine_groups],
        workers=8,
    )
