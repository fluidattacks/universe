from aioextensions import (
    collect,
)

from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.custom_utils import (
    organizations as orgs_utils,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.groups.enums import (
    GroupStateJustification,
)
from integrates.db_model.organizations.enums import (
    OrganizationStateStatus,
)
from integrates.db_model.organizations.types import (
    Organization,
    OrganizationState,
)
from integrates.groups import (
    domain as groups_domain,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.schedulers.common import (
    info,
)

SCHEDULE_MODIFIED_BY = "schedule_delete_obsolete_orgs@fluidattacks.com"


async def _remove_group(
    loaders: Dataloaders,
    group_name: str,
) -> None:
    await groups_domain.remove_group(
        loaders=loaders,
        comments="Scheduled removal for organizations.",
        email=SCHEDULE_MODIFIED_BY,
        group_name=group_name,
        justification=GroupStateJustification.OTHER,
        validate_pending_actions=False,
    )


async def _remove_organization(
    *,
    loaders: Dataloaders,
    aws_external_id: str,
    organization_id: str,
    organization_name: str,
) -> None:
    group_names = await orgs_domain.get_group_names(loaders, organization_id)
    await collect(
        [_remove_group(loaders, group_name) for group_name in group_names],
        workers=2,
    )
    await orgs_domain.remove_organization(
        loaders=loaders,
        organization_id=organization_id,
        organization_name=organization_name,
        modified_by=SCHEDULE_MODIFIED_BY,
        aws_external_id=aws_external_id,
    )
    info(f"Organization removed {organization_name}, groups removed: {group_names}")


async def _process_organization(
    organization: Organization,
) -> None:
    if orgs_utils.is_deleted(organization):
        return

    info(f"Working on organization {organization.name}")
    loaders: Dataloaders = get_new_context()
    org_group_names = await orgs_domain.get_group_names(loaders, organization.id)
    org_pending_deletion_date = organization.state.pending_deletion_date
    if len(org_group_names):
        if org_pending_deletion_date:
            await orgs_domain.update_state(
                organization_id=organization.id,
                organization_name=organization.name,
                state=OrganizationState(
                    aws_external_id=organization.state.aws_external_id,
                    modified_by=SCHEDULE_MODIFIED_BY,
                    modified_date=datetime_utils.get_utc_now(),
                    status=OrganizationStateStatus.ACTIVE,
                    pending_deletion_date=None,
                ),
            )

        return

    if not org_pending_deletion_date:
        new_deletion_date = datetime_utils.get_now_plus_delta(days=60)
        await orgs_domain.update_state(
            organization_id=organization.id,
            organization_name=organization.name,
            state=OrganizationState(
                aws_external_id=organization.state.aws_external_id,
                modified_by=SCHEDULE_MODIFIED_BY,
                modified_date=datetime_utils.get_utc_now(),
                status=organization.state.status,
                pending_deletion_date=new_deletion_date,
            ),
        )
        info(f"Organization {organization.name} is set for deletion, date: {new_deletion_date}")

        return

    if org_pending_deletion_date <= datetime_utils.get_utc_now():
        await _remove_organization(
            loaders=loaders,
            aws_external_id=organization.state.aws_external_id,
            organization_id=organization.id,
            organization_name=organization.name,
        )


async def _process_organizations() -> None:
    async for organization in orgs_domain.iterate_organizations():
        await _process_organization(organization)


async def delete_obsolete_orgs() -> None:
    await _process_organizations()


async def main() -> None:
    await delete_obsolete_orgs()
