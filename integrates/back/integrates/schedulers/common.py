import logging
import logging.config
from datetime import (
    timedelta,
)
from typing import (
    Any,
)

from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.db_model.groups.enums import (
    GroupManaged,
    GroupService,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.db_model.roots.enums import (
    RootStatus,
)
from integrates.db_model.roots.types import (
    Root,
)
from integrates.db_model.toe_lines.types import (
    ToeLine,
)
from integrates.settings import (
    LOGGING,
)

# FP: local testing
logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger(__name__)

# Constants
MAX_COMMENT_LENGTH = 500
MAX_DAYS_SINCE_ATTACKED = 90


def info(*args: Any, extra: Any = None) -> None:
    LOGGER.info(*args, extra={"extra": extra})


def error(*args: Any, extra: Any = None) -> None:
    LOGGER.error(*args, extra={"extra": extra})


def exception(*args: Any, extra: Any = None) -> None:
    LOGGER.exception(*args, extra={"extra": extra})


def format_comment(comment: str) -> str:
    if len(comment) > MAX_COMMENT_LENGTH:
        comment = f"{comment[:MAX_COMMENT_LENGTH]}..."

    return comment


def get_active_toe_lines(
    group_toe_lines: list[ToeLine],
    group_roots: list[Root],
) -> list[ToeLine]:
    active_toe_lines: list[ToeLine] = []
    active_root_ids: set[str] = {
        root.id for root in group_roots if root.state.status == RootStatus.ACTIVE
    }
    for toe_line in group_toe_lines:
        if toe_line.root_id in active_root_ids:
            active_toe_lines.append(toe_line)

    return active_toe_lines


def get_sorts_days_since_attacked(toe_line: ToeLine) -> timedelta:
    if toe_line.state.attacked_at is not None:
        return (
            datetime_utils.get_utc_now() - toe_line.state.attacked_at
            if toe_line.state.last_commit_date > toe_line.state.attacked_at
            else timedelta(days=0)
        )

    return timedelta(days=MAX_DAYS_SINCE_ATTACKED)


def is_machine_target(group: Group) -> bool:
    return bool(
        group.state.has_essential
        and group.state.service == GroupService.WHITE
        and group.state.managed != GroupManaged.UNDER_REVIEW,
    )
