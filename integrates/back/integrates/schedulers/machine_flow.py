import os
import time

import requests
from aioextensions import (
    collect,
)

from integrates.custom_utils.roots import (
    get_active_git_roots,
)
from integrates.custom_utils.vulnerabilities import (
    is_machine_vuln,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.vulnerabilities import (
    remove as remove_vuln,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
)
from integrates.roots.domain import (
    queue_sync_git_roots,
)
from integrates.schedulers.common import (
    info,
)

SCHEDULE_MODIFIED_BY = "machine@fluidattacks.com"

STATUS_URL = "https://api.statuspage.io/v1/pages/ssg1m9rm289m"
MACHINE_COMPONENT_ID = "2jx10grc1r99"
STATUS_API_TOKEN = os.environ.get("STATUSPAGE_API_KEY")


class UnexpectedError(Exception):
    pass


def update_status(status: str) -> None:
    url = f"{STATUS_URL}/components/{MACHINE_COMPONENT_ID}.json"
    headers = {
        "Authorization": f"OAuth {STATUS_API_TOKEN}",
        "Content-Type": "application/json",
    }
    data = {"component": {"status": status}}
    response = requests.put(url, headers=headers, json=data, timeout=30)
    if response.status_code == 200:
        info("Status updated successfully.")


async def _delete_vulns(vulns: list[Vulnerability]) -> None:
    # Clear machine vulnerabilities for the next scheduler execution
    await collect(
        (
            remove_vuln(vulnerability_id=vulnerability.id, should_archive=False)
            for vulnerability in vulns
        ),
        workers=32,
    )

    info("Deleted all root open vulnerabilities")


async def _get_vulns(
    group_name: str,
    root_id: str,
) -> list[Vulnerability]:
    loaders: Dataloaders = get_new_context()
    group_findings = [finding.id for finding in await loaders.group_findings.load(group_name)]
    root_existing_vulnerabilities = [
        vuln
        for vuln in await loaders.finding_vulnerabilities.load_many_chained(group_findings)
        if is_machine_vuln(vuln) and vuln.root_id == root_id
    ]

    return root_existing_vulnerabilities


async def execute_machine_flow(group_name: str, root_id: str) -> None:
    loaders: Dataloaders = get_new_context()
    test_root = next(
        (root for root in await get_active_git_roots(loaders, group_name) if root.id == root_id),
        None,
    )
    if not test_root:
        raise UnexpectedError("Could not find the required root")

    existing_vulns = await _get_vulns(group_name, root_id)
    await _delete_vulns(existing_vulns)

    execute_machine_result = await queue_sync_git_roots(
        loaders=loaders,
        group_name=group_name,
        roots=[test_root],
        check_existing_jobs=False,
        force=True,
        should_queue_machine=True,
        should_queue_sbom=False,
        modified_by=SCHEDULE_MODIFIED_BY,
    )

    if not execute_machine_result or not execute_machine_result.success:
        info("Could not queue cloning for test roots")

    # Allow enough time for machine flow to execute and finish
    time.sleep(60 * 20)

    reported_vulns = await _get_vulns(group_name, test_root.id)

    if len(reported_vulns) == 0:
        info("Something went wrong, vulnerabilities were not reported")
        if STATUS_API_TOKEN:
            update_status(status="partial_outage")

    info(
        "Vulnerabilities were reported in the test root",
        extra={"Vulnerabilities amount": len(reported_vulns)},
    )
    if STATUS_API_TOKEN:
        info("Updating machine component status")
        update_status(status="operational")


async def main() -> None:
    await execute_machine_flow("machinetest", "5537d9f7-4f9f-41ed-a42b-29dd200af5b8")
