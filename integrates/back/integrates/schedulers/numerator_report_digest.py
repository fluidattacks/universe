import asyncio
import logging
import logging.config
import os
import resource
from datetime import date, datetime
from decimal import Decimal
from typing import Any

import pytz
from aioextensions import collect
from aiohttp import ClientConnectorError, ClientPayloadError, ServerTimeoutError
from botocore.exceptions import (
    ClientError,
    ConnectionClosedError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
)

from integrates.context import FI_MAIL_COS, FI_MAIL_CTO, FI_TEST_ORGS, FI_TEST_PROJECTS
from integrates.custom_exceptions import (
    GroupNotFound,
    InvalidRegexSearch,
    MailerClientError,
    UnableToSendMail,
)
from integrates.custom_exceptions import UnavailabilityError as CustomUnavailabilityError
from integrates.custom_utils.datetime import DateRange, ReportDates, get_report_dates
from integrates.custom_utils.filter_vulnerabilities import filter_same_values
from integrates.custom_utils.toes import ToeType, get_connection
from integrates.custom_utils.vulnerabilities import get_severity_threat_score
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.findings.enums import FindingVerificationStatus
from integrates.db_model.findings.types import Finding, FindingRequest
from integrates.db_model.toe_inputs.types import ToeInputEnrichedState
from integrates.db_model.toe_lines.types import (
    ToeLine,
    ToeLineEnrichedState,
    ToeLinesConnection,
    ToeLinesEnrichedStatesConnection,
)
from integrates.db_model.toe_ports.types import ToePortState, ToePortStatesConnection
from integrates.db_model.types import Connection
from integrates.db_model.vulnerabilities.enums import VulnerabilityStateStatus
from integrates.db_model.vulnerabilities.types import VulnerabilityRequest
from integrates.decorators import retry_on_exceptions
from integrates.dynamodb.exceptions import UnavailabilityError
from integrates.mailer import groups as groups_mail
from integrates.mailer.types import (
    CommonFields,
    CountField,
    CountReportInfo,
    GroupFields,
    NumeratorReportContext,
    QuintileFields,
    QuintileGroupFields,
    QuintileRange,
)
from integrates.mailer.utils import get_group_emails_by_notification
from integrates.organizations import domain as orgs_domain
from integrates.settings import LOGGING

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)
PAGE_SIZE = 500
Q_LIMIT = 5
Q_PERCENTAGE = 20
DEFAULT_TO_ADD = 1


if os.path.isfile("/sys/fs/cgroup/memory/memory.limit_in_bytes"):
    with open("/sys/fs/cgroup/memory/memory.limit_in_bytes", encoding="utf-8") as limit:
        mem = int(int(limit.read()) * 0.95)
        resource.setrlimit(resource.RLIMIT_AS, (mem, mem))
        LOGGER.info("- Memory limit set to %s", mem)

mail_numerator_report = retry_on_exceptions(
    exceptions=(UnableToSendMail, MailerClientError),
    max_attempts=3,
    sleep_seconds=2,
)(groups_mail.send_mail_numerator_report)


def _validate_date(date_attr: date, date_range: DateRange) -> bool:
    validate_date: bool = date_range.start_date.date() <= date_attr < date_range.end_date.date()
    return validate_date


def _generate_group_quintile_fields() -> QuintileGroupFields:
    fields: QuintileGroupFields = {
        "files_perc": 0,
        "files_total": 0,
        "lines_perc": 0,
        "lines_total": 0,
    }
    return fields


def _generate_quintile_fields() -> QuintileFields:
    fields: QuintileFields = {
        "files": {"verified": 0, "total": 0, "percentage": 0},
        "lines": {"verified": 0, "total": 0, "percentage": 0},
    }
    return fields


def _generate_count_fields() -> CountField:
    fields: CountField = {
        "counter": {"past_day": 0, "today": 0},
    }
    return fields


def _generate_fields() -> CommonFields:
    fields: CommonFields = {
        "enumerated_inputs": _generate_count_fields(),
        "enumerated_ports": _generate_count_fields(),
        "verified_inputs": _generate_count_fields(),
        "verified_ports": _generate_count_fields(),
        "loc": _generate_count_fields(),
        "reattacked": _generate_count_fields(),
        "vulnerable": _generate_count_fields(),
        "sorts_verified_lines": _generate_count_fields(),
        "sorts_verified_lines_priority": _generate_count_fields(),
        "sorts_verified_lines_priority_avg": _generate_count_fields(),
        "submitted": _generate_count_fields(),
        "coverage_q1": _generate_quintile_fields(),
        "coverage_q2": _generate_quintile_fields(),
        "coverage_q3": _generate_quintile_fields(),
        "coverage_q4": _generate_quintile_fields(),
        "coverage_q5": _generate_quintile_fields(),
        "coverage_na": _generate_quintile_fields(),
        "max_cvss": {"vulnerable": Decimal(0.0), "submitted": Decimal(0.0)},
        "groups": {},
    }
    return fields


def _generate_group_fields() -> GroupFields:
    fields: GroupFields = {
        "verified_inputs": 0,
        "verified_ports": 0,
        "enumerated_inputs": 0,
        "enumerated_ports": 0,
        "loc": 0,
        "reattacked": 0,
        "vulnerable": 0,
        "sorts_verified_lines": 0,
        "sorts_verified_lines_priority": 0,
        "sorts_verified_lines_priority_avg": 0,
        "submitted": 0,
        "subscription": "-",
        "coverage_q1": _generate_group_quintile_fields(),
        "coverage_q2": _generate_group_quintile_fields(),
        "coverage_q3": _generate_group_quintile_fields(),
        "coverage_q4": _generate_group_quintile_fields(),
        "coverage_q5": _generate_group_quintile_fields(),
        "coverage_na": _generate_group_quintile_fields(),
    }
    return fields


def _common_write_to_dict_today(
    *,
    content: dict[str, Any],
    user_email: str,
    field: str,
    group: str,
    to_add: int = DEFAULT_TO_ADD,
) -> None:
    if not dict(content[user_email]["groups"]).get(group):
        content[user_email]["groups"][group] = _generate_group_fields()
    content[user_email]["groups"][group][field] += to_add
    content[user_email][field]["counter"]["today"] += to_add


def _common_write_to_dict_yesterday(
    *,
    content: dict[str, Any],
    user_email: str,
    field: str,
    to_add: int = DEFAULT_TO_ADD,
) -> None:
    content[user_email][field]["counter"]["past_day"] += to_add


def _set_max_cvss(
    field: str,
    info: CountReportInfo,
    user_email: str,
) -> None:
    if (
        field.upper()
        in [
            VulnerabilityStateStatus.VULNERABLE.value,
            VulnerabilityStateStatus.SUBMITTED.value,
        ]
        and info.content[user_email]["max_cvss"][field] < info.cvss
    ):
        info.content[user_email]["max_cvss"][field] = info.cvss


def _process_valid_date(
    field: str,
    info: CountReportInfo,
    user_email: str,
    to_add: int = DEFAULT_TO_ADD,
) -> None:
    _common_write_to_dict_today(
        content=info.content,
        user_email=user_email,
        field=field,
        group=info.group,
        to_add=to_add,
    )
    _set_max_cvss(field, info, user_email)


def _process_invalid_date(
    date_report: datetime,
    field: str,
    info: CountReportInfo,
    user_email: str,
    to_add: int = DEFAULT_TO_ADD,
) -> None:
    if _validate_date(date_report.date(), info.report_dates.past_day):
        _common_write_to_dict_yesterday(
            content=info.content,
            user_email=user_email,
            field=field,
            to_add=to_add,
        )


def _common_generate_count_report(
    date_report: datetime | None,
    field: str,
    info: CountReportInfo,
    user_email: str | None,
    to_add: int = DEFAULT_TO_ADD,
) -> None:
    if user_email and user_email in info.allowed_users and date_report:
        is_valid_date = _validate_date(date_report.date(), info.report_dates.today)
        if not info.content.get(user_email):
            info.content[user_email] = _generate_fields()
        if is_valid_date:
            _process_valid_date(
                field=field,
                info=info,
                user_email=user_email,
                to_add=to_add,
            )
        else:
            _process_invalid_date(
                date_report=date_report,
                field=field,
                info=info,
                user_email=user_email,
                to_add=to_add,
            )


async def _finding_reattacked(
    *,
    loaders: Dataloaders,
    finding_id: str,
    group: str,
    report_dates: ReportDates,
    content: dict[str, Any],
    users_email: list[str],
) -> None:
    historic_verification = await loaders.finding_historic_verification.load(
        FindingRequest(finding_id=finding_id, group_name=group),
    )
    for verification in historic_verification:
        if (
            verification.vulnerability_ids
            and verification.status == FindingVerificationStatus.VERIFIED
        ):
            _common_generate_count_report(
                verification.modified_date,
                "reattacked",
                CountReportInfo(
                    allowed_users=users_email,
                    content=content,
                    group=group,
                    report_dates=report_dates,
                ),
                verification.modified_by,
                len(verification.vulnerability_ids),
            )


async def _finding_vulns(
    *,
    loaders: Dataloaders,
    finding: Finding,
    group: str,
    report_dates: ReportDates,
    content: dict[str, Any],
    users_email: list[str],
) -> None:
    vulnerabilities = await loaders.finding_vulnerabilities.load(finding.id)
    for vuln in vulnerabilities:
        loaders.vulnerability_historic_state.clear(
            VulnerabilityRequest(vulnerability_id=vuln.id, finding_id=vuln.finding_id),
        )
        historic_state = await loaders.vulnerability_historic_state.load(
            VulnerabilityRequest(vulnerability_id=vuln.id, finding_id=vuln.finding_id),
        )
        for state in filter_same_values(historic_state):
            if state.status in [
                VulnerabilityStateStatus.VULNERABLE,
                VulnerabilityStateStatus.SUBMITTED,
            ]:
                _common_generate_count_report(
                    state.modified_date,
                    state.status.value.lower(),
                    CountReportInfo(
                        allowed_users=users_email,
                        content=content,
                        group=group,
                        report_dates=report_dates,
                        cvss=get_severity_threat_score(vuln, finding),
                    ),
                    vuln.hacker_email,
                )


async def _finding_content(
    group: str,
    report_dates: ReportDates,
    content: dict[str, Any],
    users_email: list[str],
) -> None:
    findings = await get_new_context().group_findings.load(group)
    loaders = get_new_context()
    for finding in findings:
        await _finding_reattacked(
            loaders=loaders,
            finding_id=finding.id,
            group=group,
            report_dates=report_dates,
            content=content,
            users_email=users_email,
        )
        await _finding_vulns(
            loaders=loaders,
            finding=finding,
            group=group,
            report_dates=report_dates,
            content=content,
            users_email=users_email,
        )
    LOGGER.info("- finding report generated in group %s", group)


def sorts_counter(
    info: CountReportInfo,
    toe: ToeLineEnrichedState,
) -> None:
    if toe.state.sorts_priority_factor != -1 and isinstance(toe.state.sorts_priority_factor, int):
        _common_generate_count_report(
            toe.state.attacked_at,
            "sorts_verified_lines",
            info,
            toe.state.attacked_by,
        )
        _common_generate_count_report(
            toe.state.attacked_at,
            "sorts_verified_lines_priority",
            info,
            toe.state.attacked_by,
            toe.state.sorts_priority_factor,
        )


def toes_counter(
    connection: Connection[ToeInputEnrichedState]
    | ToeLinesEnrichedStatesConnection
    | ToePortStatesConnection
    | ToeLinesConnection,
    info: CountReportInfo,
) -> None:
    for toe_edge in connection.edges:
        toe = toe_edge.node
        match toe:
            case ToeInputEnrichedState():
                _common_generate_count_report(
                    toe.state.seen_at,
                    "enumerated_inputs",
                    info,
                    toe.state.seen_first_time_by,
                    int(not toe.state.attacked_by),
                )
                _common_generate_count_report(
                    toe.state.attacked_at,
                    "verified_inputs",
                    info,
                    toe.state.attacked_by,
                )
            case ToeLineEnrichedState():
                _common_generate_count_report(
                    toe.state.attacked_at,
                    "loc",
                    info,
                    toe.state.attacked_by,
                    toe.state.attacked_lines,
                )
                sorts_counter(info, toe)
            case ToePortState():
                if toe.seen_first_time_by and not toe.attacked_by:
                    _common_generate_count_report(
                        toe.seen_at,
                        "enumerated_ports",
                        info,
                        toe.seen_first_time_by,
                    )
                if toe.attacked_by:
                    _common_generate_count_report(
                        toe.attacked_at,
                        "verified_ports",
                        info,
                        toe.attacked_by,
                    )


def _generated_toes(
    connection: Connection[ToeInputEnrichedState]
    | ToeLinesEnrichedStatesConnection
    | ToePortStatesConnection
    | ToeLinesConnection,
    info: CountReportInfo,
    present_toe_lines: dict[str, list[ToeLine]] | None,
) -> None:
    for toe_edge in connection.edges:
        toe = toe_edge.node
        if isinstance(toe, ToeLine) and present_toe_lines:
            present_toe_lines[info.group].append(toe)


async def _paginated_toes_processing(
    info: CountReportInfo,
    toe_type: ToeType,
    page_size: int = PAGE_SIZE,
    present_toe_lines: dict[str, list[ToeLine]] | None = None,
    all_current_toes: bool = False,
) -> None:
    cursor = None
    loaders = get_new_context()
    while True:
        connection = await get_connection(
            loaders=loaders,
            toe_type=toe_type,
            group_name=info.group,
            cursor=cursor,
            page_size=page_size,
            report_dates=info.report_dates,
            all_current_toes=all_current_toes,
        )
        if connection is None:
            break
        if all_current_toes:
            _generated_toes(connection, info, present_toe_lines)
        else:
            toes_counter(connection, info)

        if not connection.page_info.has_next_page:
            LOGGER.info("- %s report generated in group %s", toe_type.value, info.group)
            break
        cursor = connection.page_info.end_cursor


def _safe_divide(numerator: float, denominator: float) -> float | None:
    try:
        return numerator / denominator
    except (ZeroDivisionError, TypeError, ValueError):
        return None


def _get_average(count: int, observations: int) -> float:
    result = _safe_divide(count, observations)
    return result if result is not None else 0.0


def _generate_field_average(
    content: dict[str, Any],
    user_email: str,
    field_of_avg: str,
    field_of_sum: str,
    field_of_observations: str,
) -> None:
    user_data = content[user_email]
    user_data[field_of_avg]["counter"]["today"] = _get_average(
        count=user_data[field_of_sum]["counter"]["today"],
        observations=user_data[field_of_observations]["counter"]["today"],
    )
    user_data[field_of_avg]["counter"]["past_day"] = _get_average(
        count=user_data[field_of_sum]["counter"]["past_day"],
        observations=user_data[field_of_observations]["counter"]["past_day"],
    )
    for group_name in user_data["groups"]:
        content[user_email]["groups"][group_name][field_of_avg] = _get_average(
            count=user_data["groups"][group_name][field_of_sum],
            observations=user_data["groups"][group_name][field_of_observations],
        )


def _get_total_files(quintile_toe_lines: list[ToeLine]) -> int:
    return len(quintile_toe_lines)


def _get_verified_files(quintile_toe_lines: list[ToeLine]) -> int:
    return len(
        [
            toe_line
            for toe_line in quintile_toe_lines
            if toe_line.state.attacked_lines == toe_line.state.loc
        ],
    )


def _get_verified_lines(quintile_toe_lines: list[ToeLine]) -> int:
    return sum(toe_line.state.attacked_lines for toe_line in quintile_toe_lines)


def _get_total_lines(quintile_toe_lines: list[ToeLine]) -> int:
    return sum(toe_line.state.loc for toe_line in quintile_toe_lines)


def _get_quintile_toe_lines(
    group_toe_lines: list[ToeLine],
    quintile_range: QuintileRange,
) -> list[ToeLine]:
    return [
        toe_line
        for toe_line in group_toe_lines
        if toe_line.state.loc
        and toe_line.state.be_present
        and toe_line.state.sorts_priority_factor is not None
        and quintile_range.start <= toe_line.state.sorts_priority_factor < quintile_range.end
    ]


def _get_na_toe_lines(group_toe_lines: list[ToeLine]) -> list[ToeLine]:
    return [
        toe_line
        for toe_line in group_toe_lines
        if toe_line.state.loc
        and toe_line.state.be_present
        and toe_line.state.sorts_priority_factor in (-1, None)
    ]


def _get_coverage(total: int, verified: int) -> str:
    coverage = _safe_divide(verified, total)
    return f"{int(coverage * 100)}%" if coverage is not None else "N/A"


def _set_coverage(
    *,
    field: str,
    group_name: str,
    quintile: str,
    total: int,
    user_data: dict[str, Any],
    verified: int,
) -> None:
    coverage = _get_coverage(total, verified)

    user_data[f"coverage_{quintile}"][field]["verified"] += verified
    user_data[f"coverage_{quintile}"][field]["total"] += total
    user_data["groups"][group_name][f"coverage_{quintile}"][f"{field}_perc"] = coverage
    user_data["groups"][group_name][f"coverage_{quintile}"][f"{field}_total"] = total


def _set_quintile_info(
    group_name: str,
    quintile: str,
    quintile_toe_lines: list[ToeLine],
    user_data: dict[str, Any],
) -> None:
    total_files = _get_total_files(quintile_toe_lines)
    verified_files = _get_verified_files(quintile_toe_lines)

    total_lines = _get_total_lines(quintile_toe_lines)
    verified_lines = _get_verified_lines(quintile_toe_lines)

    field_values = {
        "files": (total_files, verified_files),
        "lines": (total_lines, verified_lines),
    }

    for field, (total, verified) in field_values.items():
        _set_coverage(
            field=field,
            group_name=group_name,
            quintile=quintile,
            total=total,
            user_data=user_data,
            verified=verified,
        )


def _get_percentage_range(quintile: int) -> QuintileRange:
    return QuintileRange(
        start=Q_PERCENTAGE * (quintile - 1),
        end=Q_PERCENTAGE * quintile,
    )


def _generate_group_coverage(
    user_data: dict[str, Any],
    group_name: str,
    group_toe_lines: list[ToeLine],
) -> None:
    for quintile in range(1, Q_LIMIT + 1):
        q_toe_lines = _get_quintile_toe_lines(group_toe_lines, _get_percentage_range(quintile))
        _set_quintile_info(group_name, f"q{quintile}", q_toe_lines, user_data)
    na_toe_lines = _get_na_toe_lines(group_toe_lines)
    _set_quintile_info(group_name, "na", na_toe_lines, user_data)


def _get_quintile_coverage(
    user_data: dict[str, Any],
    quintile: str,
    quintile_field: str,
) -> str:
    return _get_coverage(
        user_data[f"coverage_{quintile}"][quintile_field]["total"],
        user_data[f"coverage_{quintile}"][quintile_field]["verified"],
    )


def _generate_general_coverage(user_data: dict[str, Any]) -> None:
    for quint in range(5):
        user_data[f"coverage_q{quint + 1}"]["files"]["percentage"] = _get_quintile_coverage(
            user_data,
            f"q{quint + 1}",
            "files",
        )
        user_data[f"coverage_q{quint + 1}"]["lines"]["percentage"] = _get_quintile_coverage(
            user_data,
            f"q{quint + 1}",
            "lines",
        )
    user_data["coverage_na"]["files"]["percentage"] = _get_quintile_coverage(
        user_data,
        "na",
        "files",
    )
    user_data["coverage_na"]["lines"]["percentage"] = _get_quintile_coverage(
        user_data,
        "na",
        "lines",
    )


def _have_data(item: dict[str, Any]) -> bool:
    if all(isinstance(value, int) for value in item.values()):
        return not all(value == 0 for value in item.values())
    return any(
        _have_data(value) for key, value in item.items() if key not in ["groups", "max_cvss"]
    )


async def get_group_data(
    semaphore: asyncio.Semaphore,
    info: CountReportInfo,
    toe_lines: dict[str, list[ToeLine]],
) -> None:
    async with semaphore:
        try:
            toe_lines[info.group] = []
            await asyncio.gather(
                _paginated_toes_processing(info, ToeType.INPUT),
                _paginated_toes_processing(info, ToeType.LINE),
                _paginated_toes_processing(
                    info,
                    ToeType.LINE,
                    present_toe_lines=toe_lines,
                    all_current_toes=True,
                ),
                _paginated_toes_processing(info, ToeType.PORT),
                _finding_content(
                    info.group,
                    info.report_dates,
                    info.content,
                    info.allowed_users,
                ),
            )
        except (
            ClientConnectorError,
            ClientError,
            ClientPayloadError,
            ConnectionClosedError,
            ConnectionResetError,
            ConnectTimeoutError,
            CustomUnavailabilityError,
            GroupNotFound,
            HTTPClientError,
            InvalidRegexSearch,
            MemoryError,
            ReadTimeoutError,
            ServerTimeoutError,
            UnavailabilityError,
        ) as ex:
            LOGGER.info(
                "- Unable to get content in group %s",
                info.group,
                extra={
                    "extra": {
                        "ex": ex,
                    },
                },
            )


async def _process_user_data(
    content: dict[str, Any],
    toe_lines: dict[str, list[ToeLine]],
) -> None:
    for user_email, user_data in content.items():
        _generate_field_average(
            content=content,
            user_email=user_email,
            field_of_avg="sorts_verified_lines_priority_avg",
            field_of_sum="sorts_verified_lines_priority",
            field_of_observations="sorts_verified_lines",
        )
        for group_name, group_data in user_data["groups"].items():
            group_data = await get_new_context().group.load(group_name)
            user_data["groups"][group_name]["subscription"] = (
                "o" if group_data and group_data.state.type == "ONESHOT" else "c"
            )
            _generate_group_coverage(
                user_data=user_data,
                group_name=group_name,
                group_toe_lines=toe_lines[group_name],
            )
        _generate_general_coverage(user_data)


async def _generate_numerator_report(
    report_groups: dict[str, list[str]],
    report_dates: ReportDates,
) -> dict[str, Any]:
    content: dict[str, Any] = {}
    toe_lines: dict[str, list[ToeLine]] = {}
    semaphore = asyncio.Semaphore(8)
    await asyncio.gather(
        *[
            get_group_data(
                semaphore,
                CountReportInfo(
                    allowed_users=users_email,
                    content=content,
                    group=group,
                    report_dates=report_dates,
                ),
                toe_lines,
            )
            for group, users_email in report_groups.items()
        ],
    )
    content = {key: value for key, value in content.items() if _have_data(value)}
    await _process_user_data(content, toe_lines)
    LOGGER.info("- general report successfully generated")
    return content


def get_percent(num_a: float, num_b: float) -> str:
    variation = _safe_divide(num_a, num_b)
    return f"{variation:+.0%}" if variation is not None else "-"


def _generate_count_and_variation(content: dict[str, Any]) -> dict[str, Any]:
    count_and_variation: dict[str, Any] = {
        key: {
            "counter": (count := value["counter"])["today"],
            "variation": get_percent(
                count["today"] - count["past_day"],
                count["past_day"],
            ),
        }
        for key, value in content.items()
        if key != "max_cvss"
    }
    return count_and_variation


async def _send_mail_report(
    content: dict[str, Any],
    report_date: date,
    responsible: str,
) -> None:
    groups_content = content.pop("groups")
    max_cvss = content["max_cvss"]
    coverage = {}
    for quintile in range(5):
        coverage.update({f"coverage_q{quintile + 1}": content.pop(f"coverage_q{quintile + 1}")})
    coverage.update({"coverage_na": content.pop("coverage_na")})
    count_var_report: dict[str, Any] = _generate_count_and_variation(content)
    context: NumeratorReportContext = {
        "count_var_report": count_var_report,
        "coverage": coverage,
        "groups": groups_content,
        "max_cvss": max_cvss,
        "responsible": responsible,
    }
    await mail_numerator_report(
        context=context,
        email_to=[responsible],
        email_cc=[FI_MAIL_COS, FI_MAIL_CTO],
        report_date=report_date,
    )


async def _validate_content(content: dict[str, Any], report_date: date) -> None:
    if content:
        for user_email, user_content in content.items():
            try:
                await _send_mail_report(user_content, report_date, user_email)
            except KeyError:
                LOGGER.info(
                    "- key error, email not sent",
                    extra={"extra": {"email": user_email}},
                )
                continue
    else:
        LOGGER.info("- numerator report NOT sent")


async def send_numerator_report(report_date: datetime) -> dict:
    loaders: Dataloaders = get_new_context()
    group_names = await orgs_domain.get_all_active_group_names(loaders=loaders)
    test_group_names = FI_TEST_PROJECTS.split(",")
    async for _, org_name, org_groups_names in orgs_domain.iterate_organizations_and_groups(
        loaders,
    ):
        for group_name in org_groups_names:
            if (org_name in FI_TEST_ORGS.lower().split(",")) and group_name not in test_group_names:
                test_group_names += org_groups_names
    report_dates = get_report_dates(report_date)
    group_names = [group for group in group_names if group not in test_group_names]
    groups_stakeholders_email: tuple[list[str], ...] = await collect(
        [
            get_group_emails_by_notification(
                loaders=loaders,
                group_name=group_name,
                notification="numerator_digest",
            )
            for group_name in group_names
        ],
    )
    report_groups = {
        group_name: group_emails
        for group_name, group_emails in zip(group_names, groups_stakeholders_email, strict=False)
        if group_emails
    }
    LOGGER.info(
        "- Processing started.",
        extra={
            "report_date": report_dates.today.start_date.date(),
            "total_groups": len(group_names),
            "groups_to_process": len(report_groups),
            "group_names": report_groups.keys(),
        },
    )
    content: dict[str, Any] = await _generate_numerator_report(report_groups, report_dates)
    await _validate_content(content, report_dates.today.start_date.date())
    return content


async def main(
    report_date: datetime = datetime.now(pytz.UTC),
) -> dict:
    return await send_numerator_report(report_date)
