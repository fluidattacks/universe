from integrates import batch, jobs_orchestration
from integrates.batch.dal.get import get_actions_by_name
from integrates.batch.enums import Action, SkimsBatchQueue
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.enums import Source
from integrates.db_model.groups.enums import GroupService
from integrates.db_model.roots.enums import RootCloningStatus
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityTechnique,
    VulnerabilityType,
    VulnerabilityVerificationStatus,
)
from integrates.jobs_orchestration.create_machine_config import MachineModulesToExecute
from integrates.schedulers import machine_queue_re_attacks
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    FindingFaker,
    FindingStateFaker,
    GitRootCloningFaker,
    GitRootFaker,
    GitRootStateFaker,
    GroupFaker,
    GroupStateFaker,
    OrganizationFaker,
    VulnerabilityFaker,
    VulnerabilityStateFaker,
    VulnerabilityVerificationFaker,
)
from integrates.testing.mocks import Mock, mocks

MACHINE_EMAIL = "machine@fluidattacks.com"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[
                GroupFaker(
                    name="group1",
                    organization_id="org1",
                    state=GroupStateFaker(service=GroupService.WHITE),
                ),
                GroupFaker(
                    name="group2",
                    organization_id="org1",
                    state=GroupStateFaker(service=GroupService.WHITE),
                ),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    cloning=GitRootCloningFaker(status=RootCloningStatus.OK),
                    state=GitRootStateFaker(nickname="root1"),
                ),
                GitRootFaker(
                    group_name="group1",
                    id="root2",
                    cloning=GitRootCloningFaker(status=RootCloningStatus.OK),
                    state=GitRootStateFaker(nickname="root2", branch="test"),
                ),
            ],
            findings=[
                FindingFaker(
                    group_name="group1",
                    id="fin_id_1",
                    state=FindingStateFaker(modified_by=MACHINE_EMAIL),
                    title="001. SQL injection - C Sharp SQL API",
                ),
                FindingFaker(
                    group_name="group1",
                    id="fin_id_2",
                    state=FindingStateFaker(modified_by=MACHINE_EMAIL),
                    title="043. Vulnerable Headers",
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    finding_id="fin_id_1",
                    group_name="group1",
                    id="vuln_id_1",
                    organization_name="test_org",
                    root_id="root1",
                    created_by=MACHINE_EMAIL,
                    hacker_email=MACHINE_EMAIL,
                    type=VulnerabilityType.LINES,
                    state=VulnerabilityStateFaker(
                        where="back/src/controller/user/index.js",
                        specific="12",
                        source=Source.MACHINE,
                    ),
                    verification=VulnerabilityVerificationFaker(
                        status=VulnerabilityVerificationStatus.REQUESTED,
                    ),
                    vuln_machine_hash=8061522565195734354,
                    vuln_skims_description="vuln found on root",
                    vuln_skims_method="c_sharp.sql_injection",
                ),
                VulnerabilityFaker(
                    finding_id="fin_id_2",
                    group_name="group1",
                    id="vuln_id_2",
                    organization_name="test_org",
                    root_id="root2",
                    created_by=MACHINE_EMAIL,
                    hacker_email=MACHINE_EMAIL,
                    type=VulnerabilityType.INPUTS,
                    technique=VulnerabilityTechnique.DAST,
                    state=VulnerabilityStateFaker(
                        where="https://myapp.com",
                        specific="csp ausente",
                        source=Source.MACHINE,
                    ),
                    verification=VulnerabilityVerificationFaker(
                        status=VulnerabilityVerificationStatus.REQUESTED,
                    ),
                    vuln_machine_hash=8061522565195734454,
                    vuln_skims_description="CSP ausente",
                    vuln_skims_method="http_headers.csp_header",
                ),
            ],
        ),
    )
)
async def test_machine_queue_re_attacks() -> None:
    """Testing main auxiliary function of scheduler"""
    loaders: Dataloaders = get_new_context()

    reattacks_result = await machine_queue_re_attacks.get_group_vulns_to_reattack(
        loaders,
        "group1",
    )
    assert reattacks_result is not None
    assert set(reattacks_result[0]) == {"F001", "F043"}
    assert reattacks_result[1] == MachineModulesToExecute(
        APK=False,
        DAST=True,
        SAST=True,
        SCA=True,
        CSPM=False,
    )
    assert reattacks_result[2] == {
        "root1": ["back/src/controller/user/index.js"],
        "root2": ["."],
    }

    reattacks_result_2 = await machine_queue_re_attacks.get_group_vulns_to_reattack(
        loaders,
        "group2",
    )

    assert reattacks_result_2 is None


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[
                GroupFaker(
                    name="group1",
                    organization_id="org1",
                    state=GroupStateFaker(service=GroupService.WHITE),
                ),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    cloning=GitRootCloningFaker(status=RootCloningStatus.OK),
                    state=GitRootStateFaker(nickname="root1"),
                ),
            ],
            findings=[
                FindingFaker(
                    group_name="group1",
                    id="fin_id_1",
                    state=FindingStateFaker(modified_by=MACHINE_EMAIL),
                    title="001. SQL injection - C Sharp SQL API",
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    finding_id="fin_id_1",
                    group_name="group1",
                    id="vuln_id_1",
                    organization_name="test_org",
                    root_id="root1",
                    created_by=MACHINE_EMAIL,
                    hacker_email=MACHINE_EMAIL,
                    type=VulnerabilityType.LINES,
                    state=VulnerabilityStateFaker(
                        where="back/src/controller/user/index.js",
                        specific="12",
                        source=Source.MACHINE,
                    ),
                    verification=VulnerabilityVerificationFaker(
                        status=VulnerabilityVerificationStatus.REQUESTED,
                    ),
                    vuln_machine_hash=8061522565195734354,
                    vuln_skims_description="vuln found on root",
                    vuln_skims_method="c_sharp.sql_injection",
                ),
            ],
        ),
    ),
    others=[
        Mock(
            jobs_orchestration.jobs,
            "generate_batch_action_config",
            "async",
            (
                {
                    "roots": ["root1"],
                    "roots_config_files": ["root1.yaml"],
                    "roots_sbom_config_files": ["sbom_root1_config.yaml"],
                },
                "123456",
                SkimsBatchQueue.SMALL,
            ),
        ),
        Mock(batch.dal.put, "put_action_to_batch", "async", "123456"),
    ],
)
async def test_machine_queue_re_attacks_full() -> None:
    """Testing main auxiliary function of scheduler"""
    await machine_queue_re_attacks.main()

    pending_executions = await get_actions_by_name(
        action_name=Action.EXECUTE_MACHINE_SAST,
        entity="group1",
    )
    assert len(pending_executions) == 1
