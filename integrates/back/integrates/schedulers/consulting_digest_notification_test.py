from datetime import datetime

from integrates.db_model.enums import Notification
from integrates.db_model.stakeholders.types import NotificationsPreferences
from integrates.schedulers import consulting_digest_notification
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    EMAIL_FLUIDATTACKS_USER,
    EventCommentFaker,
    EventFaker,
    FindingCommentFaker,
    FindingFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
    StakeholderStateFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import freeze_time


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(organization_id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(
                    email=EMAIL_FLUIDATTACKS_USER,
                    role="user",
                    state=StakeholderStateFaker(
                        notifications_preferences=NotificationsPreferences(
                            email=[Notification.NEW_COMMENT]
                        )
                    ),
                ),
            ],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_USER,
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                ),
            ],
            events=[EventFaker(id="event1")],
            event_comments=[
                EventCommentFaker(
                    creation_date=datetime.fromisoformat("2022-11-24T15:09:37+00:00"),
                    event_id="event1",
                )
            ],
            findings=[FindingFaker(id="3c475384-834c-47b0-ac71-a41a022e401c")],
            finding_comments=[
                FindingCommentFaker(
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                    creation_date=datetime.fromisoformat("2022-11-24T15:09:37+00:00"),
                )
            ],
        )
    ),
    others=[Mock(consulting_digest_notification, "mail_consulting_digest", "async", None)],
)
@freeze_time("2022-11-25T05:00:00.00")
async def test_consulting_digest_notification() -> None:
    groups_data = await consulting_digest_notification.main()

    assert "test-group" in groups_data
    group_data = groups_data["test-group"]
    assert len(group_data["event_comments"]) == 1
    assert len(group_data["event_comments"]["event1"]) == 1
    assert group_data["event_comments"]["event1"][0].content == "Test event comment"
    assert len(group_data["finding_comments"]) == 1
    assert len(group_data["finding_comments"]["001. SQL injection - C Sharp SQL API"]) == 1
    assert (
        group_data["finding_comments"]["001. SQL injection - C Sharp SQL API"][0].content
        == "This is a finding comment test"
    )
