from collections.abc import (
    Awaitable,
    Callable,
)

from aioextensions import (
    collect,
)

from integrates.custom_exceptions import (
    MailerClientError,
    UnableToSendMail,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.mailer import (
    trial as trial_mail,
)
from integrates.mailer.types import (
    TrialEngagementInfo,
)
from integrates.organizations import (
    domain as orgs_domain,
)

MAILER_MAP: dict[int, Callable[[Dataloaders, TrialEngagementInfo], Awaitable[None]]] = {
    3: trial_mail.send_mail_free_trial_2,
    7: trial_mail.send_mail_free_trial_3,
    14: trial_mail.send_mail_free_trial_4,
    18: trial_mail.send_mail_free_trial_5,
    21: trial_mail.send_mail_free_trial_6,
    # Post trial without upgrading
    24: trial_mail.send_mail_free_trial_7,
    27: trial_mail.send_mail_free_trial_8,
}


@retry_on_exceptions(
    exceptions=(UnableToSendMail, MailerClientError),
    max_attempts=4,
    sleep_seconds=2,
)
async def send_mail_decorated(
    send_mail_function: Callable[[Dataloaders, TrialEngagementInfo], Awaitable[None]],
    loaders: Dataloaders,
    trial_info: TrialEngagementInfo,
) -> None:
    await send_mail_function(loaders, trial_info)


async def send_trial_engagement_notification() -> None:
    loaders = get_new_context()
    groups = await orgs_domain.get_all_trial_groups(loaders)
    emails = [group.created_by for group in groups]
    trials = await loaders.trial.load_many(emails)

    await collect(
        tuple(
            send_mail_decorated(
                send_mail_function,
                loaders,
                TrialEngagementInfo(
                    email_to=group.created_by,
                    group_name=group.name,
                    start_date=trial.start_date,
                ),
            )
            for group, trial in zip(groups, trials, strict=False)
            if trial
            and trial.start_date
            and (
                send_mail_function := MAILER_MAP.get(
                    datetime_utils.get_days_since(trial.start_date)
                )
            )
        ),
    )


async def main() -> None:
    await send_trial_engagement_notification()
