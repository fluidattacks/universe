import asyncio
import logging
import os
import re
import tempfile

import aioboto3
import botocore
import yaml
from aioextensions import collect
from botocore.exceptions import ClientError

from integrates.custom_exceptions import (
    BotoErrorException,
    GroupNotFound,
    IndicatorAlreadyUpdated,
    InvalidAWSRoleTrustPolicy,
)
from integrates.custom_utils.exceptions import NETWORK_ERRORS
from integrates.custom_utils.roots import fetch_temp_aws_creds
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.groups.types import Group, GroupUnreliableIndicators
from integrates.db_model.roots.enums import RootStatus
from integrates.db_model.roots.types import (
    GitRoot,
    Root,
    RootEnvironmentUrl,
    RootEnvironmentUrlsRequest,
    RootUnreliableIndicatorsToUpdate,
    Secret,
)
from integrates.db_model.roots.update import update_unreliable_indicators as update_root_indicators
from integrates.decorators import retry_on_exceptions
from integrates.groups import domain as groups_domain
from integrates.organizations import domain as orgs_domain
from integrates.roots.s3_mirror import download_repo

LOGGER = logging.getLogger(__name__)


async def run_boto3_fun(
    cred: list[Secret],
    service: str,
    function: str,
    region: str | None = None,
    parameters: dict[str, object] | None = None,
) -> dict[str, dict | list]:
    try:
        session = aioboto3.Session(
            aws_access_key_id=cred[0].value,
            aws_secret_access_key=cred[1].value,
            aws_session_token=cred[2].value,
        )
        async with session.client(  # type: ignore
            service_name=service,
            region_name=region,
        ) as client:
            return await getattr(client, function)(**(parameters or {}))
    except botocore.exceptions.ClientError as error:
        raise BotoErrorException from error


def arn_no_fluid(tags: list[dict[str, str]], arn: str) -> str | None:
    for tag in tags:
        if tag["Key"] == "NOFLUID":
            return arn
    return None


async def get_iam_resources_with_tag(cred: list[Secret]) -> list[str]:
    arns = []
    try:
        async with aioboto3.Session(
            aws_access_key_id=cred[0].value,
            aws_secret_access_key=cred[1].value,
            aws_session_token=cred[2].value,
        ).client("iam") as iam_client:
            paginator = iam_client.get_paginator("list_users")
            async for response in paginator.paginate():
                for user in response["Users"]:
                    tags_response = await iam_client.list_user_tags(UserName=user["UserName"])
                    if arn := arn_no_fluid(tags_response["Tags"], user["Arn"]):
                        arns.append(arn)

            paginator = iam_client.get_paginator("list_roles")
            async for response in paginator.paginate():
                for role in response["Roles"]:
                    tags_response = await iam_client.list_role_tags(RoleName=role["RoleName"])
                    if arn := arn_no_fluid(tags_response["Tags"], role["Arn"]):
                        arns.append(arn)
    except botocore.exceptions.ClientError as error:
        raise BotoErrorException from error

    return arns


async def get_arns_that_are_excluded(credentials: list[Secret]) -> int:
    if len(credentials) == 0:
        return 0
    arns: list[str] = []
    try:
        arns = await get_iam_resources_with_tag(credentials)
        active_regions = ["us-east-1"]
        response = await run_boto3_fun(
            cred=credentials,
            service="ec2",
            function="describe_regions",
            region="us-east-1",
        )
        if regions := response.get("Regions"):
            active_regions = [region["RegionName"] for region in regions]
        session = aioboto3.Session(
            aws_access_key_id=credentials[0].value,
            aws_secret_access_key=credentials[1].value,
            aws_session_token=credentials[2].value,
        )
        for region in active_regions:
            async with session.client("resourcegroupstaggingapi", region_name=region) as client:
                paginator = client.get_paginator("get_resources")
                async for page in paginator.paginate(TagFilters=[{"Key": "NOFLUID"}]):
                    for resource in page.get("ResourceTagMappingList", []):
                        arns.append(resource["ResourceARN"])
    except (BotoErrorException, ClientError) as exc:
        LOGGER.info("Error getting ARN exclusions", extra={"extra": {"exc": exc}})
    return len(arns)


async def _get_root_environment_urls(loaders: Dataloaders, root: Root) -> list[RootEnvironmentUrl]:
    return await loaders.root_environment_urls.load(
        RootEnvironmentUrlsRequest(root_id=root.id, group_name=root.group_name),
    )


async def count_cloud_exclusions(root: GitRoot, group: Group) -> int:
    loaders: Dataloaders = get_new_context()
    urls = await _get_root_environment_urls(loaders, root)
    if len(urls) == 0:
        return 0
    org = await loaders.organization.load(group.organization_id)
    count = 0
    if org:
        for url in urls:
            if re.match(r"^arn:aws:iam::\d{12}:role\/[\w+=,.@-]+$", url.url):
                try:
                    credentials = await fetch_temp_aws_creds(url.url, org.state.aws_external_id)
                    count += await get_arns_that_are_excluded(credentials)
                except InvalidAWSRoleTrustPolicy as exc:
                    LOGGER.error(
                        "Unable to assume AWS Role for counting exclusions",
                        extra={"extra": {"root_id": root.id, "exc": exc}},
                    )
    return count


def count_fluidattacks_file_exclusions(clone_path: str, repo: str) -> tuple[int, bool]:
    uses_fluid_eac_old_file = False
    fluidattacks_lines_count = 0
    fluidattacks_path = os.path.join(clone_path, repo, ".fluidattacks.yaml")
    fluidattacks_old_path = os.path.join(clone_path, repo, ".fluidattacks")

    if os.path.exists(fluidattacks_path):
        with open(fluidattacks_path, encoding="utf-8") as file:
            data = yaml.safe_load(file)
        sca_block = data.get("sca", [])
        dast_block = data.get("dast", [])
        sca_count = len(sca_block)
        dast_count = sum(len(endpoint.get("target_findings", [])) for endpoint in dast_block)
        fluidattacks_lines_count = sca_count + dast_count
    elif os.path.exists(fluidattacks_old_path):
        uses_fluid_eac_old_file = True
        with open(fluidattacks_old_path, encoding="utf-8") as file:
            fluidattacks_lines_count = sum(
                1 for line in file if line.strip() and line not in {"[SCA]\n", "[DAST]\n"}
            )

    return fluidattacks_lines_count, uses_fluid_eac_old_file


async def _get_valid_roots_and_nicknames(
    loaders: Dataloaders,
    group_name: str,
) -> tuple[list[GitRoot], dict[str, GitRoot]]:
    valid_roots = [
        root
        for root in await loaders.group_roots.load(group_name)
        if isinstance(root, GitRoot) and root.state.status == RootStatus.ACTIVE
    ]
    roots_by_nickname: dict[str, GitRoot] = {root.state.nickname: root for root in valid_roots}
    return valid_roots, roots_by_nickname


@retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=10,
)
async def _update_root_indicators_if_needed(
    repo: str,
    nofluid_count: int,
    roots_by_nickname: dict[str, GitRoot],
    group_name: str,
) -> None:
    if nofluid_count != roots_by_nickname[repo].unreliable_indicators.nofluid_quantity:
        try:
            await update_root_indicators(
                current_value=roots_by_nickname[repo].unreliable_indicators,
                group_name=group_name,
                indicators=RootUnreliableIndicatorsToUpdate(
                    nofluid_quantity=nofluid_count,
                ),
                root_id=roots_by_nickname[repo].id,
            )
            LOGGER.info(
                "Root %s nofluid stats were updated to %s",
                repo,
                nofluid_count,
            )
        except IndicatorAlreadyUpdated:
            LOGGER.info("Root %s nofluid stats were not changed", repo)


async def process_single_repository(
    repo: str,
    clone_path: str,
    valid_roots: list[GitRoot],
    roots_by_nickname: dict[str, GitRoot],
    group: Group,
) -> tuple[int, bool]:
    if repo not in [root.state.nickname for root in valid_roots]:
        LOGGER.warning(
            "Repository has a different name compared to its nickname",
            extra={
                "extra": {
                    "group": group.name,
                    "name": repo,
                    "nicknames": [root.state.nickname for root in valid_roots],
                },
            },
        )
        return 0, False

    no_fluid_count = await count_cloud_exclusions(roots_by_nickname[repo], group)
    result = count_fluidattacks_file_exclusions(clone_path, repo)

    no_fluid_count += result[0]
    uses_fluidattacks_old_eac_file = result[1]

    proc = await asyncio.create_subprocess_exec(
        "grep",
        "-r",
        "--binary-files=without-match",
        "--exclude='*.md'",
        "NOFLUID",
        os.path.join(clone_path, repo),
        stderr=asyncio.subprocess.PIPE,
        stdout=asyncio.subprocess.PIPE,
    )
    stdout, stderr = await proc.communicate()
    if proc.returncode in {0, 1}:
        no_fluid_count += len(stdout.decode().split("\n")) - 1
        await _update_root_indicators_if_needed(repo, no_fluid_count, roots_by_nickname, group.name)
        return no_fluid_count, uses_fluidattacks_old_eac_file

    LOGGER.error(
        "Error running grep over repository",
        extra={
            "extra": {
                "error": stderr.decode(),
                "log": stdout.decode(),
                "group": group.name,
                "root": repo,
            },
        },
    )
    return 0, uses_fluidattacks_old_eac_file


async def _process_repositories(
    valid_roots: list[GitRoot],
    roots_by_nickname: dict[str, GitRoot],
    group: Group,
) -> tuple[int, list[str]]:
    total_count_group = 0
    repos_with_fluidattacks_eac_file = []
    for git_root in valid_roots:
        with tempfile.TemporaryDirectory(
            prefix="integrates_nofluid_", ignore_cleanup_errors=True
        ) as tmpdir:
            repo = await download_repo(
                git_root.group_name,
                git_root.state.nickname,
                tmpdir,
                git_root.state.gitignore,
            )
            if not repo:
                continue

            repo_data = await process_single_repository(
                git_root.state.nickname,
                tmpdir,
                valid_roots,
                roots_by_nickname,
                group,
            )
            total_count_group += repo_data[0]
            if repo_data[1]:
                repos_with_fluidattacks_eac_file.append(git_root.state.nickname)

    return total_count_group, repos_with_fluidattacks_eac_file


@retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=10,
)
async def _update_group_indicators(group_name: str, total_count_group: int) -> None:
    try:
        await groups_domain.update_indicators(
            group_name=group_name,
            indicators=GroupUnreliableIndicators(
                nofluid_quantity=total_count_group,
            ),
        )
    except GroupNotFound as ex:
        LOGGER.error(
            "Error: An error occurred updating nofluid stats",
            extra={"extra": {"group_name": group_name, "ex": ex}},
        )

    LOGGER.info(
        "Group %s nofluid stats were updated to %s",
        group_name,
        total_count_group,
    )


async def process_group(group_name: str) -> tuple[str, list[str]] | None:
    loaders = get_new_context()
    group = await loaders.group.load(group_name)
    if not group:
        return None

    valid_roots, roots_by_nickname = await _get_valid_roots_and_nicknames(loaders, group_name)
    group_data = await _process_repositories(valid_roots, roots_by_nickname, group)
    await _update_group_indicators(group_name, group_data[0])
    if len(group_data[1]) > 0:
        LOGGER.info(
            "Group %s with old eac files on roots: %s",
            group_name,
            group_data[1],
        )
        return group_name, group_data[1]
    return None


async def main() -> None:
    loaders = get_new_context()
    groups_names = await orgs_domain.get_all_active_group_names(loaders)
    results = await collect(
        (process_group(group_name) for group_name in groups_names),
        workers=os.cpu_count() or 1,
    )
    groups_with_eac = {result[0]: result[1] for result in results if result}
    LOGGER.info("Groups with old .fluidattacks file %s", groups_with_eac)
