import logging
from datetime import (
    datetime,
)

from integrates.context import (
    FI_ENVIRONMENT,
    FI_TEST_PROJECTS,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.events import (
    domain as events_domain,
)
from integrates.mailer import (
    events as events_mail,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)


def days_to_date(date: datetime) -> int:
    days = (datetime_utils.get_utc_now() - date).days

    return days


async def send_event_report() -> None:
    loaders: Dataloaders = get_new_context()
    groups_names = await orgs_domain.get_all_active_group_names(loaders)

    if FI_ENVIRONMENT == "production":
        groups_names = [group for group in groups_names if group not in FI_TEST_PROJECTS.split(",")]

    unsolved_events = [
        event for group in groups_names for event in await events_domain.get_unsolved_events(group)
    ]

    events_filtered = [
        event for event in unsolved_events if days_to_date(event.state.modified_date) in [7, 30]
    ]

    if events_filtered:
        for event in events_filtered:
            await events_mail.send_mail_event_report(
                loaders=loaders,
                event=event,
                reminder_notification=True,
            )
    else:
        LOGGER.info("- event report NOT sent")


async def main() -> None:
    await send_event_report()
