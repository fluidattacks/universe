from integrates.dataloaders import get_new_context
from integrates.db_model.findings.types import FindingVulnerabilitiesSummary, FindingZeroRiskSummary
from integrates.db_model.vulnerabilities.enums import VulnerabilityZeroRiskStatus
from integrates.schedulers.update_indicators import update_findings_indicators
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb, IntegratesOpensearch
from integrates.testing.fakers import (
    DATE_2025,
    FindingFaker,
    FindingUnreliableIndicatorsFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationAccessFaker,
    OrganizationFaker,
    StakeholderFaker,
    VulnerabilityFaker,
    VulnerabilityZeroRiskFaker,
    random_uuid,
)
from integrates.testing.mocks import mocks

ORG_ID = "ORG#1o932-q91d-4m01-al58d-33ple6dr2pl"
FIN_NAME = "211. Asymmetric denial of service - ReDoS"
FIN_NAME2 = "379. Inappropriate coding practices - Unnecessary imports"
FIN_ID = "3c475384"
FIN_ID2 = "8e5b7694"
FIN_ID3 = "4b2al92d"
ORG_NAME = "orgtest"
GROUP_NAME = "grouptest"
EMAIL_TEST = "jdoe@fluidattacks.com"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            findings=[
                FindingFaker(
                    id=FIN_ID,
                    group_name=GROUP_NAME,
                    title=FIN_NAME,
                    unreliable_indicators=FindingUnreliableIndicatorsFaker(
                        vulnerabilities_summary=FindingVulnerabilitiesSummary(closed=0, open=2)
                    ),
                ),
                FindingFaker(
                    id=FIN_ID2,
                    group_name=GROUP_NAME,
                    title=FIN_NAME2,
                    unreliable_indicators=FindingUnreliableIndicatorsFaker(
                        unreliable_zero_risk_summary=FindingZeroRiskSummary(
                            confirmed=0, rejected=0, requested=0
                        ),
                        vulnerabilities_summary=FindingVulnerabilitiesSummary(closed=0, open=1),
                    ),
                ),
                FindingFaker(
                    id=FIN_ID3,
                    group_name=GROUP_NAME,
                    unreliable_indicators=FindingUnreliableIndicatorsFaker(
                        unreliable_zero_risk_summary=FindingZeroRiskSummary(
                            confirmed=0, rejected=0, requested=0
                        ),
                        vulnerabilities_summary=FindingVulnerabilitiesSummary(closed=0, open=2),
                    ),
                ),
            ],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, enrolled=True)],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL_TEST,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                )
            ],
            organization_access=[OrganizationAccessFaker(organization_id=ORG_ID, email=EMAIL_TEST)],
            vulnerabilities=[
                VulnerabilityFaker(
                    finding_id=FIN_ID2,
                    group_name=GROUP_NAME,
                    id=random_uuid(),
                    organization_name=ORG_NAME,
                    zero_risk=VulnerabilityZeroRiskFaker(
                        comment_id="Test comment fake",
                        modified_by=EMAIL_TEST,
                        modified_date=DATE_2025,
                        status=VulnerabilityZeroRiskStatus.CONFIRMED,
                    ),
                ),
                VulnerabilityFaker(
                    finding_id=FIN_ID3,
                    group_name=GROUP_NAME,
                    id=random_uuid(),
                    organization_name=ORG_NAME,
                    zero_risk=VulnerabilityZeroRiskFaker(
                        comment_id="Test comment for vulnerability",
                        modified_by=EMAIL_TEST,
                        modified_date=DATE_2025,
                        status=VulnerabilityZeroRiskStatus.CONFIRMED,
                    ),
                ),
                VulnerabilityFaker(
                    finding_id=FIN_ID3,
                    group_name=GROUP_NAME,
                    id=random_uuid(),
                    organization_name=ORG_NAME,
                    zero_risk=VulnerabilityZeroRiskFaker(
                        comment_id="Test comment again",
                        modified_by=EMAIL_TEST,
                        modified_date=DATE_2025,
                        status=VulnerabilityZeroRiskStatus.CONFIRMED,
                    ),
                ),
            ],
        ),
        opensearch=IntegratesOpensearch(autoload=True),
    ),
)
async def test_update_findings_indicators() -> None:
    # Act
    await update_findings_indicators(GROUP_NAME)

    # Assert
    loaders = get_new_context()
    finding1 = await loaders.finding.load(FIN_ID)
    finding2 = await loaders.finding.load(FIN_ID2)
    finding3 = await loaders.finding.load(FIN_ID3)

    # Finding 1, without changes
    assert finding1
    assert finding1.unreliable_indicators.vulnerabilities_summary
    assert finding1.unreliable_indicators.vulnerabilities_summary.closed == 0
    assert finding1.unreliable_indicators.vulnerabilities_summary.open == 2

    # Finding 2, updates vulnerabilities_summary
    assert finding2
    assert finding2.unreliable_indicators.unreliable_zero_risk_summary
    assert finding2.unreliable_indicators.unreliable_zero_risk_summary.confirmed == 1
    assert finding2.unreliable_indicators.unreliable_zero_risk_summary.rejected == 0
    assert finding2.unreliable_indicators.unreliable_zero_risk_summary.requested == 0
    assert finding2.unreliable_indicators.vulnerabilities_summary
    assert finding2.unreliable_indicators.vulnerabilities_summary.closed == 0
    assert finding2.unreliable_indicators.vulnerabilities_summary.open == 0

    # Finding 3, updates zero_risk_summary
    assert finding3
    assert finding3.unreliable_indicators.unreliable_zero_risk_summary
    assert finding3.unreliable_indicators.unreliable_zero_risk_summary.confirmed == 2
    assert finding3.unreliable_indicators.unreliable_zero_risk_summary.rejected == 0
    assert finding3.unreliable_indicators.unreliable_zero_risk_summary.requested == 0
    assert finding3.unreliable_indicators.vulnerabilities_summary
    assert finding3.unreliable_indicators.vulnerabilities_summary.closed == 0
    assert finding3.unreliable_indicators.vulnerabilities_summary.open == 0
