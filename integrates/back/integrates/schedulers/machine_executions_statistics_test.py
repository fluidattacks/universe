from datetime import datetime

from integrates import schedulers
from integrates.schedulers import machine_executions_statistics
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GitRootFaker,
    GitRootStateFaker,
    GroupFaker,
    OrganizationFaker,
)
from integrates.testing.mocks import Mock, mocks

MACHINE_EMAIL = "machine@fluidattacks.com"

BATCH_JOBS_RESPONSE = {
    "jobSummaryList": [
        {
            "jobArn": "aws:batch:12345678",
            "jobId": "123456",
            "jobName": "machine_group1_scheduler_cspm_123456",
            "createdAt": 1733041290000,
            "status": "SUCCEEDED",
        },
        {
            "jobArn": "aws:batch:12345678",
            "jobId": "1234560",
            "jobName": "machine_group1_scheduler_sast_123456",
            "createdAt": 1733041290000,
            "status": "SUCCEEDED",
        },
        {
            "jobArn": "aws:batch:12345678",
            "jobId": "1234569",
            "jobName": "machine_group1_scheduler_all_123456",
            "createdAt": 1733041290000,
            "status": "FAILED",
        },
        {
            "jobArn": "aws:batch:12345678",
            "jobId": "1234568",
            "jobName": "machine_group1_scheduler_sast_123456",
            "createdAt": 1733041290000,
            "status": "FAILED",
        },
        {
            "jobArn": "aws:batch:12345678",
            "jobId": "1234567",
            "jobName": "machine_group1_scheduler_sast_123456",
            "createdAt": 1733041290000,
            "status": "SUCCEEDED",
        },
        {
            "jobArn": "aws:batch:12345678",
            "jobId": "12345670",
            "jobName": "machine_group1_scheduler_dast_123456",
            "createdAt": 1733041290000,
            "status": "SUCCEEDED",
        },
    ],
    "nextToken": None,
}


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(
                    name="group1",
                    organization_id="org1",
                ),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    state=GitRootStateFaker(nickname="root1"),
                ),
            ],
        ),
    ),
    others=[
        Mock(
            schedulers.machine_executions_statistics,
            "list_jobs_paginated",
            "async",
            BATCH_JOBS_RESPONSE,
        )
    ],
)
async def test_machine_executions_statistics() -> None:
    result = await machine_executions_statistics.get_machine_job_statistics(datetime(2024, 12, 3))

    assert result == [
        {"all": 0, "apk": 0, "cspm": 1, "dast": 1, "sast": 2},
        {"all": 1, "apk": 0, "cspm": 0, "dast": 0, "sast": 1},
    ]
