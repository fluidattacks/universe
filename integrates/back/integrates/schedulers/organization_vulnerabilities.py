import io
import logging
import logging.config
import os
import tarfile
import tempfile
from contextlib import (
    suppress,
)

import aiofiles
from aioextensions import (
    collect,
    in_thread,
)
from aiohttp import (
    ClientConnectorError,
)
from aiohttp.client_exceptions import (
    ClientPayloadError,
    ServerTimeoutError,
)
from botocore.exceptions import (
    ClientError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
)

from integrates.context import (
    CI_COMMIT_REF_NAME,
    FI_AWS_S3_MAIN_BUCKET,
    FI_AWS_S3_PATH_PREFIX,
)
from integrates.custom_exceptions import (
    UnavailabilityError as CustomUnavailabilityError,
)
from integrates.custom_utils import (
    analytics as analytics_utils,
)
from integrates.custom_utils import (
    organizations as orgs_utils,
)
from integrates.custom_utils.datetime import (
    get_as_str,
    get_now,
)
from integrates.custom_utils.findings import (
    get_group_findings,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.dynamodb.exceptions import (
    UnavailabilityError,
)
from integrates.organizations.domain import (
    get_all_active_group_names,
    iterate_organizations_and_groups,
    update_pathfile,
)
from integrates.organizations.utils import (
    get_organization,
)
from integrates.reports.it_report import (
    Filters,
    ITReport,
)
from integrates.reports.utils import (
    write_file,
)
from integrates.s3.resource import (
    get_client,
)
from integrates.schedulers.common import (
    info,
)
from integrates.settings.logger import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)
TTL = 21600


async def upload_file(bucket: str, file_path: str, file_name: str) -> None:
    async with aiofiles.open(file_path, mode="rb") as file_object:
        client = await get_client()
        try:
            await client.upload_fileobj(
                io.BytesIO(await file_object.read()),
                bucket,
                file_name.lstrip("/"),
            )
        except ClientError as ex:
            LOGGER.exception(ex, extra={"extra": locals()})
            raise CustomUnavailabilityError() from ex


@retry_on_exceptions(
    exceptions=(
        ClientConnectorError,
        ClientError,
        ClientPayloadError,
        ConnectionResetError,
        ConnectTimeoutError,
        CustomUnavailabilityError,
        HTTPClientError,
        ReadTimeoutError,
        ServerTimeoutError,
        UnavailabilityError,
    ),
    sleep_seconds=40,
    max_attempts=5,
)
async def _get_group_data(*, group_name: str, loaders: Dataloaders) -> list[list[str]]:
    findings = await get_group_findings(group_name=group_name, loaders=loaders)
    report = ITReport(
        data=findings,
        group_name=group_name,
        loaders=loaders,
        filters=Filters(),
        generate_raw_data=True,
    )
    await report.generate_data()

    return report.raw_data


def format_fields(row: list[str]) -> list[str | None]:
    return [None if field == "-" else " ".join(str(field).splitlines()) for field in row]


@retry_on_exceptions(
    exceptions=(
        ClientConnectorError,
        ClientError,
        ClientPayloadError,
        ConnectionResetError,
        ConnectTimeoutError,
        CustomUnavailabilityError,
        HTTPClientError,
        ReadTimeoutError,
        ServerTimeoutError,
        UnavailabilityError,
    ),
    sleep_seconds=40,
    max_attempts=5,
)
async def get_data(*, groups: tuple[str, ...], organization_name: str) -> list[list[str | None]]:
    loaders = get_new_context()
    all_data = await collect(
        tuple(_get_group_data(group_name=group_name, loaders=loaders) for group_name in groups),
        workers=1,
    )
    report = ITReport(
        data=[],
        group_name=organization_name,
        loaders=loaders,
        filters=Filters(),
        generate_raw_data=True,
    )
    await report.generate_data()

    header = report.raw_data
    rows: list[list[str | None]] = []
    for data in all_data:
        if len(data) > 1:
            rows.extend([format_fields(row) for row in data[1:]])
    severity_column = 0
    with suppress(ValueError):
        severity_column = header[0].index("Severity (v4.0)") if len(header) > 0 else severity_column

    rows_ord = tuple(
        sorted(
            rows,
            key=lambda row: float(str(row[severity_column]))
            if analytics_utils.is_decimal(str(row[severity_column]))
            else float("0.0"),
            reverse=True,
        ),
    )
    rows_formatted = [[str(index), *row[1:]] for index, row in enumerate(rows_ord, 1)]

    return [*header, *rows_formatted]


async def main() -> None:
    folder_date = get_as_str(get_now(), date_format="%Y-%m-%d")
    all_group_names = set(await get_all_active_group_names(get_new_context()))
    info(f"All group names: {len(all_group_names)}")
    async for org_id, org_name, org_groups in iterate_organizations_and_groups(get_new_context()):
        if orgs_utils.is_deleted(await get_organization(get_new_context(), org_id)):
            continue

        info(f"Working on organization {org_name}, {len(org_groups)} groups")
        date = get_as_str(get_now(), date_format="%Y-%m-%dT%H-%M-%S")
        rows = await get_data(
            groups=tuple(all_group_names.intersection(org_groups)),
            organization_name=org_name,
        )
        file_name = f"vulnerabilities-{org_name}-{date}"
        csv_filename = f"{file_name}.csv"
        info(f"File: {csv_filename}, rows processed: {len(rows)}")
        with tempfile.TemporaryDirectory(
            prefix="integrates_organization_vulns_",
            ignore_cleanup_errors=True,
        ) as directory:
            await in_thread(
                write_file,
                directory=directory,
                csv_filename=csv_filename,
                rows=rows,
            )

            tar_filename = f"{file_name}.tar.gz"
            with tarfile.open(
                os.path.join(directory, tar_filename),
                "w:gz",
            ) as tar_file:
                tar_file.add(os.path.join(directory, csv_filename), arcname=csv_filename)

            s3_filename = f"{CI_COMMIT_REF_NAME}/reports/organizations/{folder_date}/{tar_filename}"
            await upload_file(
                FI_AWS_S3_MAIN_BUCKET,
                str(tar_file.name),
                f"{FI_AWS_S3_PATH_PREFIX}analytics/{s3_filename}",
            )
            await update_pathfile(
                org_id,
                org_name,
                f"analytics/{s3_filename}",
            )
