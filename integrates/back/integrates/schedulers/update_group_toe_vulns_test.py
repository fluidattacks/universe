from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.enums import Source
from integrates.db_model.toe_inputs.types import GroupToeInputsRequest, ToeInput
from integrates.db_model.toe_lines.types import GroupToeLinesRequest
from integrates.db_model.toe_ports.types import GroupToePortsRequest
from integrates.db_model.vulnerabilities.enums import VulnerabilityType
from integrates.schedulers import update_group_toe_vulns
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    FindingFaker,
    FindingStateFaker,
    GitRootFaker,
    GitRootStateFaker,
    GroupFaker,
    OrganizationFaker,
    ToeInputFaker,
    ToeInputStateFaker,
    ToeLinesFaker,
    ToeLinesStateFaker,
    ToePortFaker,
    ToePortStateFaker,
    VulnerabilityFaker,
    VulnerabilityStateFaker,
)
from integrates.testing.mocks import mocks

ROOT_ID = "root1"
MACHINE_EMAIL = "machine@fluidattacks.com"
HACKER_EMAIL = "hacker@fluidattacks.com"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[GroupFaker(name="group1", organization_id="org1")],
            roots=[
                GitRootFaker(
                    group_name="group1", id=ROOT_ID, state=GitRootStateFaker(nickname="back")
                ),
            ],
            toe_lines=[
                ToeLinesFaker(
                    filename="java_has_print_statements.java",
                    group_name="group1",
                    root_id=ROOT_ID,
                    state=ToeLinesStateFaker(loc=180, be_present=True, has_vulnerabilities=False),
                ),
                ToeLinesFaker(
                    filename="package.json",
                    group_name="group1",
                    root_id=ROOT_ID,
                    state=ToeLinesStateFaker(loc=180, be_present=True, has_vulnerabilities=False),
                ),
            ],
            toe_inputs=[
                ToeInputFaker(
                    component="https://myapp.com",
                    entry_point="login",
                    environment_id="",
                    group_name="group1",
                    root_id=ROOT_ID,
                    state=ToeInputStateFaker(
                        attacked_at=None,
                        be_present=True,
                        first_attack_at=None,
                        has_vulnerabilities=False,
                    ),
                ),
            ],
            toe_ports=[
                ToePortFaker(
                    address="127.0.0.0",
                    port="8080",
                    group_name="group1",
                    root_id=ROOT_ID,
                    state=ToePortStateFaker(be_present=True, has_vulnerabilities=False),
                ),
            ],
            findings=[
                FindingFaker(
                    group_name="group1",
                    id="fin_id_1",
                    state=FindingStateFaker(modified_by=HACKER_EMAIL),
                    title="237. Logging of sensitive data",
                ),
                FindingFaker(
                    group_name="group1",
                    id="fin_id_2",
                    state=FindingStateFaker(modified_by=MACHINE_EMAIL),
                    title="011. Vulnerable dependencies",
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    finding_id="fin_id_1",
                    group_name="group1",
                    id="vuln_id_1",
                    organization_name="test_org",
                    root_id=ROOT_ID,
                    created_by=HACKER_EMAIL,
                    hacker_email=HACKER_EMAIL,
                    type=VulnerabilityType.LINES,
                    state=VulnerabilityStateFaker(
                        where="java_has_print_statements.java", specific="7", source=Source.ANALYST
                    ),
                ),
                VulnerabilityFaker(
                    finding_id="fin_id_2",
                    group_name="group1",
                    id="vuln_id_2",
                    organization_name="test_org",
                    root_id=ROOT_ID,
                    created_by=MACHINE_EMAIL,
                    hacker_email=MACHINE_EMAIL,
                    type=VulnerabilityType.LINES,
                    state=VulnerabilityStateFaker(
                        where="package.json", specific="10", source=Source.MACHINE
                    ),
                ),
                VulnerabilityFaker(
                    finding_id="fin_id_1",
                    group_name="group1",
                    id="vuln_id_3",
                    organization_name="test_org",
                    root_id=ROOT_ID,
                    created_by=HACKER_EMAIL,
                    hacker_email=HACKER_EMAIL,
                    type=VulnerabilityType.INPUTS,
                    state=VulnerabilityStateFaker(
                        where="https://myapp.com", specific="login", source=Source.ANALYST
                    ),
                ),
                VulnerabilityFaker(
                    finding_id="fin_id_1",
                    group_name="group1",
                    id="vuln_id_4",
                    organization_name="test_org",
                    root_id=ROOT_ID,
                    created_by=HACKER_EMAIL,
                    hacker_email=HACKER_EMAIL,
                    type=VulnerabilityType.PORTS,
                    state=VulnerabilityStateFaker(
                        where="127.0.0.0", specific="8080", source=Source.ANALYST
                    ),
                ),
            ],
        ),
    )
)
async def test_update_group_toe_vulns() -> None:
    group_name = "group1"
    loaders: Dataloaders = get_new_context()
    group_toes = (
        await loaders.group_toe_inputs.load_nodes(
            GroupToeInputsRequest(group_name=group_name),
        )
        + await loaders.group_toe_lines.load_nodes(
            GroupToeLinesRequest(group_name=group_name),
        )
        + await loaders.group_toe_ports.load_nodes(
            GroupToePortsRequest(group_name=group_name),
        )
    )
    assert len(group_toes) == 4
    assert all(not toe.state.has_vulnerabilities for toe in group_toes)

    await update_group_toe_vulns.main()

    loaders_new: Dataloaders = get_new_context()
    group_toes_updated = (
        await loaders_new.group_toe_inputs.load_nodes(
            GroupToeInputsRequest(group_name=group_name),
        )
        + await loaders_new.group_toe_lines.load_nodes(
            GroupToeLinesRequest(group_name=group_name),
        )
        + await loaders_new.group_toe_ports.load_nodes(
            GroupToePortsRequest(group_name=group_name),
        )
    )
    assert len(group_toes_updated) == 4
    assert all(toe.state.has_vulnerabilities for toe in group_toes_updated)

    assert all(
        toe.state.first_attack_at and toe.state.attacked_at
        for toe in group_toes_updated
        if isinstance(toe, ToeInput)
    )
