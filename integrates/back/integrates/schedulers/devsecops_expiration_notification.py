import logging
from datetime import (
    datetime,
)
from typing import (
    TypedDict,
)

from aioextensions import (
    collect,
)

from integrates.context import (
    FI_ENVIRONMENT,
    FI_TEST_PROJECTS,
)
from integrates.custom_exceptions import (
    ExpiredForcesToken,
    ExpiredToken,
    InvalidAuthorization,
    MailerClientError,
    OrganizationNotFound,
    UnableToSendMail,
)
from integrates.custom_utils import (
    datetime as date_utils,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.db_model.hook.enums import (
    HookEvent,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.forces import (
    domain as forces_domain,
)
from integrates.groups import (
    hook_notifier,
)
from integrates.mailer.groups import (
    send_mail_devsecops_expiration,
)
from integrates.mailer.types import DevSecOpsExpirationContext, GroupData
from integrates.mailer.utils import (
    get_group_emails_by_notification,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)


class ForcesDataType(TypedDict):
    email_to: tuple[str, ...]
    org_name: str
    exp_date: datetime


logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)

mail_forces_expiration = retry_on_exceptions(
    exceptions=(UnableToSendMail, MailerClientError),
    max_attempts=3,
    sleep_seconds=2,
)(send_mail_devsecops_expiration)


async def get_organization_name(loaders: Dataloaders, organization_id: str) -> str:
    organization = await loaders.organization.load(organization_id)
    if not organization:
        raise OrganizationNotFound()
    return organization.name


def unique_emails(
    groups_data: dict[str, ForcesDataType],
    email_list: tuple[str, ...],
) -> tuple[str, ...]:
    groups_data_copy = groups_data.copy()
    if groups_data_copy:
        email_list += groups_data_copy.popitem()[1]["email_to"]
        return unique_emails(groups_data_copy, email_list)

    return tuple(set(email_list))


def seven_days_left(exp_date: datetime) -> bool:
    days = date_utils.get_days_since(exp_date)
    return days == -7


async def _get_near_expiration_emails(
    *,
    group: Group,
    loaders: Dataloaders,
) -> dict[str, ForcesDataType] | None:
    if not group.agent_token:
        return None

    try:
        exp_date = forces_domain.get_expiration_date(group.agent_token)
    except (ExpiredToken, ExpiredForcesToken, InvalidAuthorization):
        return None

    if seven_days_left(exp_date):
        emails = await get_group_emails_by_notification(
            loaders=loaders,
            group_name=group.name,
            notification="devsecops_expiration",
        )

        if emails:
            data: dict[str, ForcesDataType] = {
                group.name: {
                    "email_to": tuple(emails),
                    "org_name": await get_organization_name(loaders, group.organization_id),
                    "exp_date": exp_date,
                },
            }
            return data

    return None


async def send_devsecops_expiration() -> None:
    loaders: Dataloaders = get_new_context()
    groups = await orgs_domain.get_all_active_groups(loaders)
    groups_data: dict[str, ForcesDataType] = {}

    if FI_ENVIRONMENT == "production":
        groups = [group for group in groups if group.name not in FI_TEST_PROJECTS.split(",")]

    all_data = await collect(
        [_get_near_expiration_emails(group=group, loaders=loaders) for group in groups],
        workers=1,
    )
    filtered_all_data = list(filter(None, all_data))
    for data in filtered_all_data:
        groups_data.update(data)

    for email in unique_emails(dict(groups_data), ()):
        group_context: dict[str, GroupData] = {
            group_name: {
                "org_name": data["org_name"],
                "exp_date": str(data["exp_date"].date()),
            }
            for group_name, data in groups_data.items()
            if email in data["email_to"]
        }

        context: DevSecOpsExpirationContext = {"groups_data": group_context}

        try:
            await mail_forces_expiration(
                loaders=loaders,
                context=context,
                email_to=email,
                email_cc=[],
            )
            for group_name, group_data in context["groups_data"].items():
                await hook_notifier.process_hook_event(
                    loaders=loaders,
                    group_name=group_name,
                    info={"group_data": group_data},
                    event=HookEvent.AGENT_TOKEN_EXPIRATION,
                )
            LOGGER.info(
                "Forces expiration email sent",
                extra={"extra": {"email": email}},
            )
        except KeyError:
            LOGGER.info(
                "Key error, forces expiration email not sent",
                extra={"extra": {"email": email}},
            )
    LOGGER.info("Forces expiration scheduler execution finished.")


async def main() -> None:
    await send_devsecops_expiration()
