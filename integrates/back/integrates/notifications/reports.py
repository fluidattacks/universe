from datetime import (
    datetime,
)

from integrates.db_model import (
    notifications,
)
from integrates.db_model.notifications.get import (
    NotificationLoader,
    NotificationRequest,
)
from integrates.db_model.notifications.types import (
    ReportNotification,
    ReportStatus,
)


async def add_report_notification(
    *,
    report_format: str,
    report_name: str,
    user_email: str,
) -> ReportNotification:
    notified_at = datetime.now()
    notification = ReportNotification(
        format=report_format,
        id=notified_at.isoformat(),
        name=report_name,
        notified_at=notified_at,
        status=ReportStatus.PREPARING,
        type="REPORT",
        user_email=user_email,
    )
    await notifications.add(notification=notification)
    return notification


async def update_report_notification(
    *,
    expiration_time: datetime | None,
    notification_id: str,
    s3_file_path: str | None,
    size: int | None,
    status: ReportStatus,
    user_email: str,
) -> None:
    notification = await NotificationLoader().load(
        NotificationRequest(
            notification_id=notification_id,
            user_email=user_email,
        ),
    )
    if not isinstance(notification, ReportNotification):
        return
    updated_notification = notification._replace(
        size=size,
        status=status,
        s3_file_path=s3_file_path,
        expiration_time=expiration_time,
    )
    await notifications.update(notification=updated_notification)
