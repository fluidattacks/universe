from datetime import datetime

from integrates.db_model.notifications.get import NotificationLoader, NotificationRequest
from integrates.db_model.notifications.types import ReportNotification, ReportStatus
from integrates.notifications.reports import add_report_notification, update_report_notification
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.mocks import mocks
from integrates.testing.utils import freeze_time


@freeze_time("2024-11-14T00:00:00")
@mocks()
async def test_add_report_notification() -> None:
    notification = await add_report_notification(
        report_format="PDF",
        report_name="Test report",
        user_email="test@test.com",
    )
    assert notification == ReportNotification(
        format="PDF",
        id="2024-11-14T00:00:00",
        name="Test report",
        notified_at=datetime.fromisoformat("2024-11-14T00:00:00"),
        status=ReportStatus.PREPARING,
        type="REPORT",
        user_email="test@test.com",
    )


@freeze_time("2024-11-14T00:00:00")
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            notifications=[
                ReportNotification(
                    format="PDF",
                    id="2024-11-14T00:00:00",
                    name="Test report",
                    notified_at=datetime.fromisoformat("2024-11-14T00:00:00"),
                    status=ReportStatus.PREPARING,
                    type="REPORT",
                    user_email="test@test.com",
                ),
            ],
        ),
    )
)
async def test_update_report_notification() -> None:
    await update_report_notification(
        expiration_time=datetime.fromisoformat("2024-11-14T06:00:00"),
        notification_id="2024-11-14T00:00:00",
        s3_file_path="reports/file_name.pdf",
        size=787,
        status=ReportStatus.READY,
        user_email="test@test.com",
    )
    loaded_notification = await NotificationLoader().load(
        NotificationRequest(
            notification_id="2024-11-14T00:00:00",
            user_email="test@test.com",
        ),
    )
    assert loaded_notification == ReportNotification(
        expiration_time=datetime.fromisoformat("2024-11-14T06:00:00"),
        format="PDF",
        id="2024-11-14T00:00:00",
        name="Test report",
        notified_at=datetime.fromisoformat("2024-11-14T00:00:00Z"),
        s3_file_path="reports/file_name.pdf",
        size=787,
        status=ReportStatus.READY,
        type="REPORT",
        user_email="test@test.com",
    )
