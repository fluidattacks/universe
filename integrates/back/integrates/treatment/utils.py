from datetime import UTC, datetime

from integrates import authz
from integrates.custom_exceptions import InvalidAssigned
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.dataloaders import Dataloaders
from integrates.db_model.enums import TreatmentStatus


async def get_valid_assigned(
    *,
    loaders: Dataloaders,
    assigned: str,
    email: str,
    group_name: str,
) -> str:
    is_manager_or_admin = await authz.get_group_level_role(loaders, email, group_name) in {
        "group_manager",
        "customer_manager",
        "vulnerability_manager",
        "admin",
    }
    if not is_manager_or_admin:
        assigned = email
    group_enforcer = await authz.get_group_level_enforcer(loaders, assigned)
    stakeholder = await loaders.stakeholder.load(assigned)
    if not group_enforcer(group_name, "valid_assigned") or (
        stakeholder and not stakeholder.is_registered
    ):
        raise InvalidAssigned()
    user_enforcer = await authz.get_user_level_enforcer(loaders, email)
    if assigned.endswith(authz.FLUID_IDENTIFIER) and not user_enforcer(
        "can_assign_vulnerabilities_to_fluidattacks_staff",
    ):
        raise InvalidAssigned()

    return assigned


def get_accepted_until(
    acceptance_date: str | None,
    treatment_status: TreatmentStatus,
) -> datetime | None:
    if treatment_status != TreatmentStatus.ACCEPTED or not acceptance_date:
        return None

    if len(acceptance_date.split(" ")) == 1:
        today = datetime_utils.get_now_as_str()
        acceptance_date = f"{acceptance_date.split()[0]} {today.split()[1]}"

    return datetime_utils.get_from_str(acceptance_date).astimezone(tz=UTC)


def get_inverted_treatment_converted(treatment: str) -> str:
    if treatment == "NEW":
        return "UNTREATED"

    return treatment
