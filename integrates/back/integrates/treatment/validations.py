from integrates.custom_exceptions import SameValues
from integrates.db_model.enums import (
    AcceptanceStatus,
    TreatmentStatus,
)
from integrates.db_model.types import (
    Treatment,
    TreatmentToUpdate,
)


def validate_same_value(
    *,
    current_treatment: Treatment | None,
    treatment_to_update: TreatmentToUpdate,
    valid_assigned: str,
) -> None:
    if not current_treatment or current_treatment.status != treatment_to_update.status:
        return

    if (
        current_treatment.justification == treatment_to_update.justification
        and current_treatment.assigned == valid_assigned
        and current_treatment.accepted_until == treatment_to_update.accepted_until
    ):
        raise SameValues()

    if current_treatment.acceptance_status == AcceptanceStatus.APPROVED:
        if treatment_to_update.status == TreatmentStatus.ACCEPTED_UNDEFINED:
            raise SameValues()

        if (
            treatment_to_update.status == TreatmentStatus.ACCEPTED
            and current_treatment.accepted_until == treatment_to_update.accepted_until
        ):
            raise SameValues()
