from typing import cast

from integrates.unreliable_indicators.enums import Entity, EntityAttr, EntityDependency, EntityId
from integrates.unreliable_indicators.types import EntityToUpdate

ENTITIES = {
    Entity.finding: {
        "args": {
            EntityId.ids,
        },
        "attrs": {
            EntityAttr.max_open_root_criticality: {
                "dependencies": {
                    EntityDependency.update_git_root,
                },
            },
            EntityAttr.max_open_severity_score: {
                "dependencies": {
                    EntityDependency.confirm_vulnerabilities,
                    EntityDependency.deactivate_root,
                    EntityDependency.move_root,
                    EntityDependency.reject_vulnerabilities_zero_risk,
                    EntityDependency.remove_vulnerability,
                    EntityDependency.request_vulnerabilities_zero_risk,
                    EntityDependency.update_severity,
                    EntityDependency.upload_file,
                    EntityDependency.verify_vulnerabilities_request,
                },
            },
            EntityAttr.max_open_severity_score_v4: {
                "dependencies": {
                    EntityDependency.confirm_vulnerabilities,
                    EntityDependency.deactivate_root,
                    EntityDependency.move_root,
                    EntityDependency.reject_vulnerabilities_zero_risk,
                    EntityDependency.remove_vulnerability,
                    EntityDependency.request_vulnerabilities_zero_risk,
                    EntityDependency.update_severity,
                    EntityDependency.upload_file,
                    EntityDependency.verify_vulnerabilities_request,
                },
            },
            EntityAttr.newest_vulnerability_report_date: {
                "dependencies": {
                    EntityDependency.approve_draft,
                    EntityDependency.confirm_vulnerabilities,
                    EntityDependency.deactivate_root,
                    EntityDependency.move_root,
                    EntityDependency.reject_vulnerabilities_zero_risk,
                    EntityDependency.remove_vulnerability,
                    EntityDependency.request_vulnerabilities_zero_risk,
                    EntityDependency.upload_file,
                    EntityDependency.verify_vulnerabilities_request,
                },
            },
            EntityAttr.oldest_open_vulnerability_report_date: {
                "dependencies": {
                    EntityDependency.approve_draft,
                    EntityDependency.confirm_vulnerabilities,
                    EntityDependency.deactivate_root,
                    EntityDependency.move_root,
                    EntityDependency.reject_vulnerabilities_zero_risk,
                    EntityDependency.remove_vulnerability,
                    EntityDependency.request_vulnerabilities_zero_risk,
                    EntityDependency.upload_file,
                    EntityDependency.verify_vulnerabilities_request,
                },
            },
            EntityAttr.oldest_vulnerability_report_date: {
                "dependencies": {
                    EntityDependency.approve_draft,
                    EntityDependency.confirm_vulnerabilities,
                    EntityDependency.deactivate_root,
                    EntityDependency.move_root,
                    EntityDependency.reject_vulnerabilities_zero_risk,
                    EntityDependency.remove_vulnerability,
                    EntityDependency.request_vulnerabilities_zero_risk,
                    EntityDependency.upload_file,
                    EntityDependency.verify_vulnerabilities_request,
                },
            },
            EntityAttr.rejected_vulnerabilities: {
                "dependencies": {
                    EntityDependency.deactivate_root,
                    EntityDependency.move_root,
                },
            },
            EntityAttr.status: {
                "dependencies": {
                    EntityDependency.deactivate_root,
                    EntityDependency.move_root,
                },
            },
            EntityAttr.submitted_vulnerabilities: {
                "dependencies": {
                    EntityDependency.deactivate_root,
                    EntityDependency.move_root,
                },
            },
            EntityAttr.total_open_cvssf: {
                "dependencies": {
                    EntityDependency.confirm_vulnerabilities,
                    EntityDependency.deactivate_root,
                    EntityDependency.move_root,
                    EntityDependency.reject_vulnerabilities_zero_risk,
                    EntityDependency.remove_vulnerability,
                    EntityDependency.request_vulnerabilities_zero_risk,
                    EntityDependency.update_severity,
                    EntityDependency.upload_file,
                    EntityDependency.verify_vulnerabilities_request,
                },
            },
            EntityAttr.total_open_cvssf_v4: {
                "dependencies": {
                    EntityDependency.confirm_vulnerabilities,
                    EntityDependency.deactivate_root,
                    EntityDependency.move_root,
                    EntityDependency.reject_vulnerabilities_zero_risk,
                    EntityDependency.remove_vulnerability,
                    EntityDependency.request_vulnerabilities_zero_risk,
                    EntityDependency.update_severity,
                    EntityDependency.upload_file,
                    EntityDependency.verify_vulnerabilities_request,
                },
            },
            EntityAttr.treatment_summary: {
                "dependencies": {
                    EntityDependency.confirm_vulnerabilities,
                    EntityDependency.deactivate_root,
                    EntityDependency.handle_finding_policy,
                    EntityDependency.handle_vulnerabilities_acceptance,
                    EntityDependency.move_root,
                    EntityDependency.reject_vulnerabilities_zero_risk,
                    EntityDependency.remove_vulnerability,
                    EntityDependency.request_vulnerabilities_zero_risk,
                    EntityDependency.reset_expired_accepted_findings,
                    EntityDependency.update_vulnerabilities_treatment,
                    EntityDependency.upload_file,
                    EntityDependency.verify_vulnerabilities_request,
                },
            },
            EntityAttr.where: {
                "dependencies": {
                    EntityDependency.confirm_vulnerabilities,
                    EntityDependency.deactivate_root,
                    EntityDependency.move_root,
                    EntityDependency.reject_vulnerabilities_zero_risk,
                    EntityDependency.remove_vulnerability,
                    EntityDependency.request_vulnerabilities_zero_risk,
                    EntityDependency.update_vulnerability_commit,
                    EntityDependency.upload_file,
                    EntityDependency.verify_vulnerabilities_request,
                },
            },
        },
    },
    Entity.root: {
        "args": {
            EntityId.ids,
        },
        "attrs": {
            EntityAttr.last_status_update: {
                "dependencies": {
                    EntityDependency.activate_root,
                    EntityDependency.deactivate_root,
                },
            },
        },
    },
    Entity.vulnerability: {
        "args": {
            EntityId.ids,
        },
        "attrs": {
            EntityAttr.closing_date: {
                "dependencies": {
                    EntityDependency.move_root,
                    EntityDependency.deactivate_root,
                    EntityDependency.upload_file,
                    EntityDependency.verify_vulnerabilities_request,
                },
            },
            EntityAttr.efficacy: {
                "dependencies": {
                    EntityDependency.move_root,
                    EntityDependency.upload_file,
                    EntityDependency.verify_vulnerabilities_request,
                },
            },
            EntityAttr.last_reattack_date: {
                "dependencies": {
                    EntityDependency.move_root,
                    EntityDependency.request_vulnerabilities_zero_risk,
                    EntityDependency.upload_file,
                    EntityDependency.verify_vulnerabilities_request,
                },
            },
            EntityAttr.last_reattack_requester: {
                "dependencies": {
                    EntityDependency.request_vulnerabilities_verification,
                },
            },
            EntityAttr.last_requested_reattack_date: {
                "dependencies": {
                    EntityDependency.move_root,
                    EntityDependency.request_vulnerabilities_verification,
                },
            },
            EntityAttr.priority: {
                "dependencies": {
                    EntityDependency.confirm_vulnerabilities,
                    EntityDependency.confirm_vulnerabilities_zero_risk,
                    EntityDependency.deactivate_root,
                    EntityDependency.move_root,
                    EntityDependency.reject_vulnerabilities_zero_risk,
                    EntityDependency.remove_vulnerability,
                    EntityDependency.request_vulnerabilities_zero_risk,
                    EntityDependency.upload_file,
                    EntityDependency.update_git_root,
                    EntityDependency.update_severity,
                    EntityDependency.update_organization_priority_policies,
                    EntityDependency.update_vulnerabilities_treatment,
                    EntityDependency.update_vulnerability_treatment,
                },
            },
            EntityAttr.reattack_cycles: {
                "dependencies": {
                    EntityDependency.move_root,
                    EntityDependency.request_vulnerabilities_verification,
                },
            },
            EntityAttr.report_date: {
                "dependencies": {
                    EntityDependency.confirm_vulnerabilities,
                    EntityDependency.upload_file,
                },
            },
            EntityAttr.treatment_changes: {
                "dependencies": {
                    EntityDependency.handle_vulnerabilities_acceptance,
                    EntityDependency.handle_finding_policy,
                    EntityDependency.move_root,
                    EntityDependency.request_vulnerabilities_zero_risk,
                    EntityDependency.reset_expired_accepted_findings,
                    EntityDependency.update_vulnerabilities_treatment,
                    EntityDependency.verify_vulnerabilities_request,
                },
            },
        },
    },
}


def get_entities_to_update_by_dependency(
    dependency: EntityDependency,
    **args: list[str],
) -> dict[Entity, EntityToUpdate]:
    entities_to_update = {}
    for name, value in ENTITIES.items():
        attributes_to_update = set()
        for attr, info in cast(dict, value["attrs"]).items():
            if dependency in info["dependencies"]:
                attributes_to_update.add(attr)

        if attributes_to_update:
            entity_args = cast(set, value["args"])
            entity_ids = {
                base_arg: args[f"{name.value}_{base_arg.value}"] for base_arg in entity_args
            }
            entities_to_update[name] = EntityToUpdate(
                entity_ids=entity_ids,
                attributes_to_update=attributes_to_update,
            )

    return entities_to_update
