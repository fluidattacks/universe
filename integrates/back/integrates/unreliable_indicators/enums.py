from enum import Enum


class Entity(str, Enum):
    finding: str = "finding"
    root: str = "root"
    vulnerability: str = "vulnerability"


class EntityId(str, Enum):
    ids: str = "ids"


class EntityAttr(str, Enum):
    efficacy: str = "efficacy"
    closing_date: str = "closing_date"
    solving_date: str = "solving_date"
    last_reattack_date: str = "last_reattack_date"
    last_reattack_requester: str = "last_reattack_requester"
    last_requested_reattack_date: str = "last_requested_reattack_date"
    last_status_update: str = "last_status_update"
    max_open_epss: str = "max_open_epss"
    max_open_root_criticality: str = "max_open_root_criticality"
    max_open_severity_score: str = "max_open_severity_score"
    max_open_severity_score_v4: str = "max_open_severity_score_v4"
    newest_vulnerability_report_date: str = "last_vulnerability_report_date"
    oldest_open_vulnerability_report_date: str = "oldest_open_vulnerability_report_date"
    oldest_vulnerability_report_date: str = "oldest_vulnerability_report_date"
    priority: str = "priority"
    rejected_vulnerabilities: str = "rejected_vulnerabilities"
    report_date: str = "report_date"
    reattack_cycles: str = "reattack_cycles"
    source: str = "source"
    status: str = "status"
    submitted_vulnerabilities: str = "submitted_vulnerabilities"
    total_open_cvssf: str = "total_open_cvssf"
    total_open_cvssf_v4: str = "total_open_cvssf_v4"
    treatment_changes: str = "treatment_changes"
    treatment_summary: str = "treatment_summary"
    vulnerabilities_summary: str = "vulnerabilities_summary"
    where: str = "where"
    zero_risk_summary: str = "zero_risk_summary"


class EntityDependency(str, Enum):
    activate_root: str = "activate_root"
    approve_draft: str = "approve_draft"
    deactivate_root: str = "deactivate_root"
    handle_vulnerabilities_acceptance: str = "handle_vulnerabilities_acceptance"
    handle_finding_policy: str = "handle_finding_policy"
    move_root: str = "move_root"
    confirm_vulnerabilities: str = "confirm_vulnerabilities"
    confirm_vulnerabilities_zero_risk: str = "confirm_vulnerabilities_zero_risk"
    reject_vulnerabilities: str = "reject_vulnerabilities"
    reject_vulnerabilities_zero_risk: str = "reject_vulnerabilities_zero_risk"
    remove_vulnerability: str = "remove_vulnerability"
    request_vulnerabilities_verification: str = "request_vulnerabilities_verification"
    request_vulnerabilities_zero_risk: str = "request_vulnerabilities_zero_risk"
    reset_expired_accepted_findings: str = "reset_expired_accepted_findings"
    resubmit_vulnerabilities: str = "resubmit_vulnerabilities"
    update_git_root: str = "update_git_root"
    update_organization_priority_policies: str = "update_organization_priority_policies"
    update_vulnerabilities_treatment: str = "update_vulnerabilities_treatment"
    update_vulnerability_commit: str = "update_vulnerability_commit"
    update_vulnerability_treatment: str = "update_vulnerability_treatment"
    update_severity: str = "update_severity"
    upload_file: str = "upload_file"
    verify_vulnerabilities_request: str = "verify_vulnerabilities_request"
