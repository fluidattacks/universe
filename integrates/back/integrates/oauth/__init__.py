from authlib.integrations.starlette_client import (
    OAuth,
    StarletteOAuth2App,
)

from integrates.oauth.azure import (
    AZURE_ISSUES_ARGS,
    AZURE_REPOSITORY_ARGS,
)
from integrates.oauth.bitbucket import (
    BITBUCKET_REPOSITORY_ARGS,
)
from integrates.oauth.github import (
    GITHUB_ARGS,
)
from integrates.oauth.gitlab import (
    GITLAB_ISSUES_ARGS,
    GITLAB_REPOSITORY_ARGS,
)


class OAuthTyped(OAuth):
    azure: StarletteOAuth2App
    azure_issues: StarletteOAuth2App
    bitbucket: StarletteOAuth2App
    gitlab: StarletteOAuth2App
    gitlab_issues: StarletteOAuth2App
    github: StarletteOAuth2App


OAUTH = OAuthTyped()
OAUTH.register(**AZURE_ISSUES_ARGS)
OAUTH.register(**AZURE_REPOSITORY_ARGS)
OAUTH.register(**BITBUCKET_REPOSITORY_ARGS)
OAUTH.register(**GITLAB_ISSUES_ARGS)
OAUTH.register(**GITLAB_REPOSITORY_ARGS)
OAUTH.register(**GITHUB_ARGS)


__all__ = [
    "AZURE_ISSUES_ARGS",
    "AZURE_REPOSITORY_ARGS",
    "BITBUCKET_REPOSITORY_ARGS",
    "GITHUB_ARGS",
    "GITLAB_ISSUES_ARGS",
    "GITLAB_REPOSITORY_ARGS",
    "OAUTH",
]
