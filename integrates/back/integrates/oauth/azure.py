import asyncio
import json
import logging
import logging.config
from datetime import datetime

import pytz
from aiohttp import ClientOSError, ClientSession, FormData
from aiohttp.client_exceptions import ClientError

from integrates.context import (
    FI_AZURE_OAUTH2_ISSUES_APP_ID,
    FI_AZURE_OAUTH2_REPOSITORY_APP_ID,
    FI_AZURE_OAUTH2_REPOSITORY_SECRET,
)
from integrates.custom_utils.datetime import get_minus_delta, get_plus_delta, get_utc_now
from integrates.dataloaders import Dataloaders
from integrates.db_model.azure_repositories.types import AccountInfo
from integrates.db_model.credentials.types import Credentials, OauthAzureSecret
from integrates.db_model.roots.types import GitRoot
from integrates.decorators import retry_on_exceptions
from integrates.oauth.utils import get_credential_or_token, update_token
from integrates.organizations.utils import get_organization_roots
from integrates.settings import LOGGING

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)
AZURE_REFRESH_URL = "https://app.vssps.visualstudio.com/oauth2/token"
AZURE_AUTHZ_URL = "https://app.vssps.visualstudio.com/oauth2/authorize"

AZURE_ISSUES_ARGS = {
    "name": "azure_issues",
    "response_type": "Assertion",
    "authorize_url": AZURE_AUTHZ_URL,
    "client_id": FI_AZURE_OAUTH2_ISSUES_APP_ID,
    "client_kwargs": {"scope": "vso.graph vso.project vso.work_write"},
}
AZURE_REPOSITORY_ARGS = {
    "name": "azure",
    "response_type": "Assertion",
    "authorize_url": AZURE_AUTHZ_URL,
    "client_id": FI_AZURE_OAUTH2_REPOSITORY_APP_ID,
    "client_kwargs": {"scope": "vso.project vso.code"},
}


async def get_azure_profile(
    *,
    token: str,
) -> str | None:
    retries: int = 0
    retry: bool = True
    headers: dict[str, str] = {
        "Accept": "application/json",
        "content-type": "application/json",
        "Authorization": f"Bearer {token}",
    }
    async with ClientSession(headers=headers) as session:
        while retry and retries < 5:
            retry = False
            async with session.get(
                "https://app.vssps.visualstudio.com/_apis/profile/profiles/me?api-version=6.0",
            ) as response:
                try:
                    result = await response.json()
                except (
                    json.decoder.JSONDecodeError,
                    ClientError,
                    ClientOSError,
                ) as exc:
                    LOGGER.exception(exc, extra={"extra": locals()})
                    break
                if not response.ok:
                    if retries == 4:
                        LOGGER.error(
                            "Failed to get azure profile",
                            extra={
                                "extra": {
                                    **result,
                                },
                            },
                        )
                    retry = True
                    retries += 1
                    await asyncio.sleep(0.2)
                    continue

                return result["publicAlias"]

    return None


async def get_azure_accounts(
    *,
    token: str,
    public_alias: str,
    as_a_member: bool = False,
) -> tuple[AccountInfo, ...]:
    retries: int = 0
    retry: bool = True
    headers: dict[str, str] = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": f"Bearer {token}",
    }
    account_as = "memberId" if as_a_member else "ownerId"
    async with ClientSession(headers=headers) as session:
        while retry and retries < 5:
            retry = False
            async with session.get(
                f"https://app.vssps.visualstudio.com/_apis/accounts"
                f"?{account_as}={public_alias}&api-version=6.0",
            ) as response:
                try:
                    result = await response.json()
                except (
                    json.decoder.JSONDecodeError,
                    ClientError,
                    ClientOSError,
                ) as exc:
                    LOGGER.exception(exc, extra={"extra": locals()})
                    break

                if not response.ok:
                    if retries == 4:
                        LOGGER.error(
                            "Failed to get azure accounts",
                            extra={"extra": {**result}},
                        )
                    retry = True
                    retries += 1
                    await asyncio.sleep(0.2)
                    continue

                return tuple(
                    AccountInfo(
                        base_url=account["accountUri"],
                        name=account["accountName"],
                    )
                    for account in result["value"]
                )

    return tuple()


async def get_azure_refresh_token(*, code: str, redirect_uri: str, secret: str) -> dict | None:
    request_parameters: dict[str, str] = {
        "client_assertion_type": ("urn:ietf:params:oauth:client-assertion-type:jwt-bearer"),
        "client_assertion": secret,
        "grant_type": "urn:ietf:params:oauth:grant-type:jwt-bearer",
        "assertion": code,
        "redirect_uri": redirect_uri,
    }
    data = FormData()
    for key, value in request_parameters.items():
        data.add_field(key, value)
    retries: int = 0
    retry: bool = True
    async with ClientSession() as session:
        while retry and retries < 5:
            retry = False
            async with session.post(
                AZURE_REFRESH_URL,
                data=data,
            ) as response:
                try:
                    result = await response.json()
                except (
                    json.decoder.JSONDecodeError,
                    ClientError,
                ) as exc:
                    LOGGER.exception(exc, extra={"extra": locals()})
                    break
                if not response.ok:
                    retry = True
                    retries += 1
                    await asyncio.sleep(0.2)
                    continue

                return result

    return None


async def get_fresh_token(
    *,
    redirect_uri: str,
    refresh_token: str,
    secret: str,
) -> dict | None:
    request_parameters: dict[str, str] = {
        "client_assertion_type": "urn:ietf:params:oauth:client-assertion-type:jwt-bearer",
        "grant_type": "refresh_token",
        "client_assertion": secret,
        "assertion": refresh_token,
        "redirect_uri": redirect_uri,
    }
    data = FormData()
    for key, value in request_parameters.items():
        data.add_field(key, value)
    retries = 0
    retry = True
    async with ClientSession() as session:
        while retry and retries < 5:
            retry = False
            async with session.post(AZURE_REFRESH_URL, data=data) as response:
                try:
                    result = await response.json()
                except (
                    json.decoder.JSONDecodeError,
                    ClientError,
                    ClientOSError,
                ) as exc:
                    LOGGER.exception(exc, extra={"extra": locals()})
                    break
                if not response.ok:
                    retry = True
                    retries += 1
                    await asyncio.sleep(0.2)
                    continue

                return result

    return None


@retry_on_exceptions(
    exceptions=(ClientOSError,),
    max_attempts=3,
    sleep_seconds=float("5.5"),
)
async def get_azure_token(*, credential: Credentials, loaders: Dataloaders) -> str | None:
    if not isinstance(credential.secret, OauthAzureSecret):
        return None

    _credential = await get_credential_or_token(credential=credential, loaders=loaders)
    if isinstance(_credential, str):
        return _credential

    credential = _credential
    if not isinstance(credential.secret, OauthAzureSecret):
        return None

    used_by = [
        root.group_name
        for root in await get_organization_roots(loaders, credential.organization_id)
        if isinstance(root, GitRoot) and root.state.credential_id == credential.id
    ]
    result = await get_fresh_token(
        redirect_uri=credential.secret.redirect_uri,
        refresh_token=credential.secret.arefresh_token,
        secret=FI_AZURE_OAUTH2_REPOSITORY_SECRET,
    )

    if result is None:
        LOGGER.error(
            "Failed to refresh azure token",
            extra={"extra": {"credential_id": credential.id, "used_by": used_by}},
        )
        return None

    secret = OauthAzureSecret(
        redirect_uri=credential.secret.redirect_uri,
        arefresh_token=result["refresh_token"],
        access_token=result["access_token"],
        valid_until=get_plus_delta(
            get_minus_delta(get_utc_now(), seconds=60),
            seconds=int(result["expires_in"]),
        ),
    )
    await update_token(
        credential_id=credential.id,
        loaders=loaders,
        organization_id=credential.organization_id,
        secret=secret,
        modified_date=datetime.now(tz=pytz.timezone("UTC")),
    )
    loaders.credentials.clear_all()
    loaders.organization_credentials.clear(credential.organization_id)

    return result["access_token"]
