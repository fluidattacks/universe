import logging
import logging.config
from typing import (
    Literal,
    cast,
)

import stripe
from aioextensions import (
    collect,
)
from stripe import (
    Customer,
    StripeError,
    Subscription,
)
from stripe import (
    PaymentMethod as StripePaymentMethod,
)

from integrates.billing import (
    authors as billing_authors,
)
from integrates.billing.types import (
    ModifyParamsDict,
    Price,
)
from integrates.context import (
    FI_STRIPE_API_KEY,
)
from integrates.custom_exceptions import (
    BillingCustomerHasNoPaymentMethod,
    CouldNotCreatePaymentMethod,
    CouldNotDowngradeSubscription,
    PaymentMethodAlreadyExists,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.db_model.organizations.types import (
    Organization,
)
from integrates.settings import (
    LOGGING,
)

# Constants
LOGGER = logging.getLogger(__name__)
API_VERSION = "2024-06-20"

stripe.api_key = FI_STRIPE_API_KEY
stripe.api_version = API_VERSION
logging.config.dictConfig(LOGGING)


async def attach_payment_method(
    *,
    payment_method_id: str,
    org_billing_customer: str,
) -> stripe.PaymentMethod.Card:
    """Attach a payment method to a Stripe customer"""
    payment_method = await stripe.PaymentMethod.attach_async(
        payment_method_id,
        customer=org_billing_customer,
    )

    if not payment_method.card or not payment_method.card.checks:
        raise CouldNotCreatePaymentMethod()

    if payment_method.card.checks.cvc_check == "fail":
        await stripe.PaymentMethod.detach_async(
            payment_method_id,
        )
        raise CouldNotCreatePaymentMethod()

    return payment_method.card


async def get_prices() -> dict[str, Price]:
    """Get model prices"""
    data = (
        await stripe.Price.list_async(
            lookup_keys=[
                "advanced",
                "essential",
            ],
            active=True,
        )
    ).data

    return {
        price.lookup_key: Price(
            id=price.id,
            currency=price.currency,
            amount=price.unit_amount or 1,
        )
        for price in data
        if price.lookup_key
    }


async def get_customer_payment_methods(
    *,
    customer_id: str,
    limit: int = 100,
) -> list[StripePaymentMethod]:
    """Return list of customer's payment methods"""
    return (
        await stripe.Customer.list_payment_methods_async(
            customer_id,
            type="card",
            limit=limit,
        )
    ).data


async def create_credit_card_payment_method(
    *,
    org: Organization,
    make_default: bool,
    billing_email: str,
    payment_method_id: str,
) -> StripePaymentMethod.Card:
    """Create a credit card payment method and associate it to the customer"""
    if not org.billing_information:
        raise CouldNotCreatePaymentMethod()

    customers = await collect(
        tuple(
            stripe.Customer.retrieve_async(payment_method.customer_id)
            for payment_method in org.billing_information
        ),
    )

    customer = next(
        (customer for customer in customers if customer.email == billing_email),
        None,
    )

    if not customer:
        raise CouldNotCreatePaymentMethod()

    results = await collect(
        [
            get_customer_payment_methods(
                customer_id=customer.id,
                limit=100,
            ),
            stripe.PaymentMethod.retrieve_async(payment_method_id),
        ],
    )

    # Raise exception if payment method already exists for customer
    payment_methods = cast(list[StripePaymentMethod], results[0])
    new_payment_method = cast(StripePaymentMethod, results[1])
    if not new_payment_method.card:
        raise CouldNotCreatePaymentMethod()

    if new_payment_method.card.fingerprint in (
        payment_method.card.fingerprint for payment_method in payment_methods if payment_method.card
    ):
        raise PaymentMethodAlreadyExists()

    # Attach payment method to customer
    try:
        card = await attach_payment_method(
            payment_method_id=payment_method_id,
            org_billing_customer=customer.id,
        )
    except StripeError as ex:
        raise CouldNotCreatePaymentMethod() from ex

    # If payment method is the first one registered or selected as default,
    # then make it default
    if (
        not customer.invoice_settings
        or customer.invoice_settings.default_payment_method
        or make_default
    ):
        await update_default_payment_method(
            payment_method_id=payment_method_id,
            org_billing_customer=customer.id,
        )

    return card


async def update_default_payment_method(
    *,
    payment_method_id: str,
    org_billing_customer: str,
) -> bool:
    """Make a payment method default for a customer"""
    data = await stripe.Customer.modify_async(
        org_billing_customer,
        invoice_settings={"default_payment_method": payment_method_id},
    )
    if not data.invoice_settings:
        return False

    return data.invoice_settings.default_payment_method == payment_method_id


async def update_credit_card_info(
    *,
    payment_method_id: str,
    card_expiration_month: int,
    card_expiration_year: int,
) -> bool:
    data = await stripe.PaymentMethod.modify_async(
        payment_method_id,
        card={
            "exp_month": card_expiration_month,
            "exp_year": card_expiration_year,
        },
    )
    if len(str(card_expiration_year)) == 2:
        card_expiration_year = int(f"20{card_expiration_year}")

    if not data.card:
        raise CouldNotCreatePaymentMethod()

    return (
        card_expiration_month == data.card.exp_month and card_expiration_year == data.card.exp_month
    )


async def get_group_subscriptions(
    *,
    group_name: str,
    org_billing_customer: str,
    status: Literal[
        "active",
        "all",
        "canceled",
        "ended",
        "incomplete",
        "incomplete_expired",
        "past_due",
        "paused",
        "trialing",
        "unpaid",
    ],
) -> list[Subscription]:
    """Return subscriptions for a group"""
    subs = (
        await stripe.Subscription.list_async(
            customer=org_billing_customer,
            limit=100,
            status=status,
        )
    ).data
    return [sub for sub in subs if sub.metadata["group"] == group_name]


async def _get_subscription_usage(
    *,
    subscription: Subscription,
) -> int:
    """Get group advanced usage"""
    date = datetime_utils.get_utc_now()
    return len(
        await billing_authors.get_group_authors(
            date=date,
            group=subscription.metadata["group"],
        ),
    )


async def _pay_advanced_authors_to_date(
    *,
    prices: dict[str, Price],
    subscription: Subscription,
) -> bool:
    """Pay advanced authors to date"""
    authors: int = await _get_subscription_usage(subscription=subscription)
    billing_customer_id = (
        subscription.customer.id
        if isinstance(subscription.customer, Customer)
        else subscription.customer
    )
    customer = await stripe.Customer.retrieve_async(
        billing_customer_id,
    )

    payment_method = (
        customer.invoice_settings.default_payment_method if customer.invoice_settings else None
    )

    if not payment_method:
        raise BillingCustomerHasNoPaymentMethod()

    return (
        await stripe.PaymentIntent.create_async(
            customer=billing_customer_id,
            amount=prices["advanced"].amount * authors,
            currency=prices["advanced"].currency,
            payment_method=payment_method.id
            if isinstance(payment_method, StripePaymentMethod)
            else payment_method,
            confirm=True,
        )
    ).status == "succeeded"


async def update_subscription(
    *,
    subscription: Subscription,
    upgrade: bool,
) -> bool:
    """Upgrade or downgrade a subscription"""
    prices = await get_prices()
    result = True

    if upgrade:
        data = ModifyParamsDict(
            Subscription_items=[
                Subscription.ModifyParamsItem(
                    price=prices["advanced"].id,
                    metadata={
                        "group": subscription.metadata["group"],
                        "name": "advanced",
                        "organization": subscription.metadata["organization"],
                    },
                ),
            ],
            metadata={"subscription": "advanced"},
        )
    else:
        data = ModifyParamsDict(
            Subscription_items=[
                Subscription.ModifyParamsItem(
                    clear_usage=True,
                    deleted=True,
                ),
            ],
            metadata={"subscription": "essential"},
        )

        # Pay advanced authors to date
        result = await _pay_advanced_authors_to_date(
            prices=prices,
            subscription=subscription,
        )

        # Raise exception if payment intent failed
        if not result:
            raise CouldNotDowngradeSubscription()

    # Update subscription
    result = (
        await stripe.Subscription.modify_async(
            subscription.id,
            **stripe.Subscription.ModifyParams(
                items=data["Subscription_items"],
                metadata=data["metadata"],
            ),
        )
    ).status in ("active", "trialing")

    return result


async def report_subscription_usage(
    *,
    subscription: Subscription,
) -> bool:
    """Report group advanced usage to Stripe"""
    timestamp = int(datetime_utils.get_utc_timestamp())
    authors = await _get_subscription_usage(
        subscription=subscription,
    )
    await stripe.SubscriptionItem.create_usage_record_async(
        subscription.id,
        quantity=authors,
        timestamp=timestamp,
        action="set",
    )
    return True
