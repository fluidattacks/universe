from datetime import datetime

import integrates.billing
from integrates.billing import (
    stripe_customers,
)
from integrates.billing.domain import (
    remove_payment_method,
    update_credit_card_payment_method,
    update_other_payment_method,
)
from integrates.billing.types import PaymentMethod
from integrates.custom_exceptions import (
    CouldNotRemovePaymentMethod,
    InvalidBillingCustomer,
    InvalidBillingPaymentMethod,
)
from integrates.dataloaders import get_new_context
from integrates.db_model.organizations.types import (
    DocumentFile,
)
from integrates.organizations.utils import get_organization
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    DocumentFileFaker,
    OrganizationAccessFaker,
    OrganizationAccessStateFaker,
    OrganizationBillingFaker,
    OrganizationDocumentsFaker,
    OrganizationFaker,
    OrganizationPaymentMethodsFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import freeze_time, parametrize, raises

ADMIN = "admin@gmail.com"
ORG_ID = "ORG#6b110f25-cd46-e94c-2bad-b9e182cf91a8"
BILL_ID = "test-payment-method-id"
BILL_ID_OTHER = "test-payment-method-id_other"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(
                    id=ORG_ID,
                    country="US",
                    billing_information=[OrganizationBillingFaker()],
                    payment_methods=[
                        OrganizationPaymentMethodsFaker(
                            id=BILL_ID,
                            business_name="test-org",
                            email=ADMIN,
                            country="US",
                            state="TX",
                            city="Austin",
                            documents=OrganizationDocumentsFaker(rut=None, tax_id=None),
                        ),
                        OrganizationPaymentMethodsFaker(
                            id=BILL_ID_OTHER,
                            business_name="test-org",
                            email=ADMIN,
                            country="US",
                            state="TX",
                            city="Austin",
                            documents=OrganizationDocumentsFaker(),
                        ),
                    ],
                )
            ],
            stakeholders=[StakeholderFaker(email=ADMIN, enrolled=True)],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=ADMIN,
                    state=OrganizationAccessStateFaker(
                        has_access=True, role="organization_manager"
                    ),
                ),
            ],
        )
    ),
    others=[
        Mock(
            integrates.billing.domain,
            "list_organization_payment_methods",
            "async",
            [
                PaymentMethod(
                    id=BILL_ID,
                    fingerprint="",
                    last_four_digits="",
                    expiration_month="",
                    expiration_year="",
                    brand="",
                    default=False,
                    business_name="Fluid",
                    city="Austin",
                    country="US",
                    email="test@fluidattacks.com",
                    state="TX",
                    rut=None,
                    tax_id=None,
                ),
                PaymentMethod(
                    id=BILL_ID_OTHER,
                    fingerprint="xyz789uvw012",
                    last_four_digits="3456",
                    expiration_month="05",
                    expiration_year="23",
                    brand="MasterCard",
                    default=False,
                    business_name="test-org",
                    country="US",
                    state="TX",
                    city="Austin",
                    email=ADMIN,
                    rut=None,
                    tax_id=None,
                ),
            ],
        ),
        Mock(integrates.billing.domain, "_remove_credit_card_payment_method", "async", True),
    ],
)
async def test_remove_other_payment_method() -> None:
    loaders = get_new_context()
    result = await remove_payment_method(
        org=await get_organization(loaders, ORG_ID),
        payment_method_id=BILL_ID,
    )

    assert result


@mocks(aws=IntegratesAws(dynamodb=IntegratesDynamodb(organizations=[OrganizationFaker(id=ORG_ID)])))
async def test_remove_payment_method_fail() -> None:
    loaders = get_new_context()
    with raises(InvalidBillingPaymentMethod):
        await remove_payment_method(
            org=await get_organization(loaders, ORG_ID),
            payment_method_id="e4678f25-9525-9523-be7e-9af4",
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(
                    id=ORG_ID,
                    country="US",
                    billing_information=[OrganizationBillingFaker()],
                    payment_methods=[
                        OrganizationPaymentMethodsFaker(
                            id=BILL_ID,
                            business_name="test-org",
                            email=ADMIN,
                            country="US",
                            state="TX",
                            city="Austin",
                            documents=OrganizationDocumentsFaker(rut=None, tax_id=None),
                        )
                    ],
                )
            ],
            stakeholders=[StakeholderFaker(email=ADMIN, enrolled=True)],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=ADMIN,
                    state=OrganizationAccessStateFaker(
                        has_access=True, role="organization_manager"
                    ),
                ),
            ],
        )
    ),
    others=[
        Mock(
            integrates.billing.domain,
            "list_organization_payment_methods",
            "async",
            [
                PaymentMethod(
                    id=BILL_ID,
                    fingerprint="",
                    last_four_digits="",
                    expiration_month="",
                    expiration_year="",
                    brand="",
                    default=False,
                    business_name="Fluid",
                    city="Austin",
                    country="US",
                    email="test@fluidattacks.com",
                    state="TX",
                    rut=None,
                    tax_id=None,
                )
            ],
        ),
    ],
)
async def test_remove_payment_method_raises_could_not_remove() -> None:
    loaders = get_new_context()
    with raises(CouldNotRemovePaymentMethod):
        await remove_payment_method(
            org=await get_organization(loaders, ORG_ID),
            payment_method_id=BILL_ID,
        )


@parametrize(
    args=["updated_card_expiration_month", "updated_card_expirations_year", "make_default"],
    cases=[
        [1, 2026, False],
        [12, 2026, False],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(
                    id=ORG_ID,
                    country="US",
                    billing_information=[OrganizationBillingFaker()],
                    payment_methods=[
                        OrganizationPaymentMethodsFaker(
                            id=BILL_ID,
                            business_name="test-org",
                            email=ADMIN,
                            country="US",
                            state="TX",
                            city="Austin",
                            documents=OrganizationDocumentsFaker(rut=None, tax_id=None),
                        )
                    ],
                )
            ],
            stakeholders=[StakeholderFaker(email=ADMIN, enrolled=True)],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=ADMIN,
                    state=OrganizationAccessStateFaker(
                        has_access=True, role="organization_manager"
                    ),
                ),
            ],
        )
    ),
    others=[
        Mock(
            integrates.billing.domain,
            "list_organization_payment_methods",
            "async",
            [
                PaymentMethod(
                    id=BILL_ID,
                    fingerprint="",
                    last_four_digits="",
                    expiration_month="",
                    expiration_year="",
                    brand="",
                    default=False,
                    business_name="Fluid",
                    city="Austin",
                    country="US",
                    email="test@fluidattacks.com",
                    state="TX",
                    rut=None,
                    tax_id=None,
                )
            ],
        ),
        Mock(stripe_customers, "update_credit_card_info", "async", True),
    ],
)
@freeze_time("2024-10-14T00:00:00")
async def test_update_credit_card_payment_method(
    updated_card_expiration_month: int, updated_card_expirations_year: int, make_default: bool
) -> None:
    loaders = get_new_context()
    result = await update_credit_card_payment_method(
        org=await get_organization(loaders, ORG_ID),
        payment_method_id=BILL_ID,
        card_expiration_month=updated_card_expiration_month,
        card_expiration_year=updated_card_expirations_year,
        make_default=make_default,
    )

    assert result


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="ORG#id_org", country="US")]
        )
    )
)
async def test_update_credit_card_payment_method_invalid_customer() -> None:
    loaders = get_new_context()
    with raises(InvalidBillingCustomer):
        await update_credit_card_payment_method(
            org=await get_organization(loaders, "ORG#id_org"),
            payment_method_id=BILL_ID,
            card_expiration_month=11,
            card_expiration_year=27,
            make_default=True,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(
                    id="ORG#id_org",
                    country="US",
                    billing_information=[OrganizationBillingFaker()],
                )
            ]
        )
    ),
    others=[
        Mock(
            integrates.billing.domain,
            "list_organization_payment_methods",
            "async",
            [
                PaymentMethod(
                    id=BILL_ID,
                    fingerprint="",
                    last_four_digits="",
                    expiration_month="",
                    expiration_year="",
                    brand="",
                    default=False,
                    business_name="Fluid",
                    city="Austin",
                    country="US",
                    email="test@fluidattacks.com",
                    state="TX",
                    rut=None,
                    tax_id=None,
                )
            ],
        ),
    ],
)
async def test_update_credit_card_payment_method_invalid_payment_method() -> None:
    loaders = get_new_context()
    with raises(InvalidBillingPaymentMethod):
        await update_credit_card_payment_method(
            org=await get_organization(loaders, "ORG#id_org"),
            payment_method_id="e4678f25-9525-9523-be7e-9af4",
            card_expiration_month=11,
            card_expiration_year=27,
            make_default=True,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(
                    id=ORG_ID,
                    country="US",
                    billing_information=[OrganizationBillingFaker()],
                    payment_methods=[
                        OrganizationPaymentMethodsFaker(
                            id=BILL_ID,
                            business_name="test-org",
                            email=ADMIN,
                            country="US",
                            state="TX",
                            city="Austin",
                            documents=OrganizationDocumentsFaker(
                                rut=DocumentFileFaker(
                                    filename="path/to/rut_document.pdf",
                                    modified_date=datetime(2023, 1, 1),
                                ),
                                tax_id=DocumentFileFaker(
                                    filename="path/to/tax_id_document.pdf",
                                    modified_date=datetime(2023, 12, 31),
                                ),
                            ),
                        ),
                        OrganizationPaymentMethodsFaker(
                            id=BILL_ID_OTHER,
                            business_name="test-org",
                            email=ADMIN,
                            country="US",
                            state="TX",
                            city="Austin",
                            documents=OrganizationDocumentsFaker(),
                        ),
                    ],
                )
            ],
            stakeholders=[StakeholderFaker(email=ADMIN, enrolled=True)],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=ADMIN,
                    state=OrganizationAccessStateFaker(
                        has_access=True, role="organization_manager"
                    ),
                ),
            ],
        )
    ),
    others=[
        Mock(
            integrates.billing.domain,
            "list_organization_payment_methods",
            "async",
            [
                PaymentMethod(
                    id=BILL_ID,
                    fingerprint="abc123def456",
                    last_four_digits="7890",
                    expiration_month="12",
                    expiration_year="25",
                    brand="Visa",
                    default=True,
                    business_name="test-org",
                    country="US",
                    state="TX",
                    city="Austin",
                    email="business@example.com",
                    rut=DocumentFile("path/to/rut_document.pdf", datetime(2023, 1, 1)),
                    tax_id=DocumentFile("path/to/tax_id_document.pdf", datetime(2023, 12, 31)),
                ),
                PaymentMethod(
                    id=BILL_ID_OTHER,
                    fingerprint="xyz789uvw012",
                    last_four_digits="3456",
                    expiration_month="05",
                    expiration_year="23",
                    brand="MasterCard",
                    default=False,
                    business_name="test-org",
                    country="US",
                    state="TX",
                    city="Austin",
                    email=ADMIN,
                    rut=None,
                    tax_id=None,
                ),
            ],
        ),
        Mock(integrates.db_model.organizations, "update_metadata", "async", None),
    ],
)
@freeze_time("2024-10-14T00:00:00")
async def test_update_other_payment_method() -> None:
    loaders = get_new_context()

    org = await get_organization(loaders, ORG_ID)
    assert org.payment_methods
    result = await update_other_payment_method(
        org=org,
        documents=org.payment_methods[0].documents,
        payment_method_id=BILL_ID_OTHER,
        business_name=org.name,
        city="Cityville",
        country="Countryland",
        email=ADMIN,
        state="Stateness",
    )

    assert result


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(
                    id=ORG_ID,
                    country="US",
                    billing_information=[OrganizationBillingFaker()],
                    payment_methods=[
                        OrganizationPaymentMethodsFaker(
                            id=BILL_ID,
                            business_name="test-org",
                            email=ADMIN,
                            country="US",
                            state="TX",
                            city="Austin",
                            documents=OrganizationDocumentsFaker(
                                rut=DocumentFileFaker(
                                    filename="path/to/rut_document.pdf",
                                    modified_date=datetime(2023, 1, 1),
                                ),
                                tax_id=DocumentFileFaker(
                                    filename="path/to/tax_id_document.pdf",
                                    modified_date=datetime(2023, 12, 31),
                                ),
                            ),
                        ),
                    ],
                )
            ]
        )
    ),
    others=[
        Mock(
            integrates.billing.domain,
            "list_organization_payment_methods",
            "async",
            [
                PaymentMethod(
                    id=BILL_ID,
                    fingerprint="",
                    last_four_digits="",
                    expiration_month="",
                    expiration_year="",
                    brand="",
                    default=False,
                    business_name="Fluid",
                    city="Austin",
                    country="US",
                    email="test@fluidattacks.com",
                    state="TX",
                    rut=None,
                    tax_id=None,
                )
            ],
        ),
    ],
)
async def test_update_other_payment_method_raises_ex() -> None:
    loaders = get_new_context()
    org = await get_organization(loaders, ORG_ID)
    assert org.payment_methods
    with raises(InvalidBillingPaymentMethod):
        await update_other_payment_method(
            org=org,
            documents=org.payment_methods[0].documents,
            payment_method_id=BILL_ID_OTHER,
            business_name=org.name,
            city="Cityville",
            country="Countryland",
            email=ADMIN,
            state="Stateness",
        )
