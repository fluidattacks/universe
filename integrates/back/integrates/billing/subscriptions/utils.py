from integrates.billing.types import (
    CustomerTokens,
    TreliTokensApiResponse,
)


def format_treli_tokens(
    *,
    tokens: list[TreliTokensApiResponse],
) -> list[CustomerTokens]:
    return [
        CustomerTokens(
            card_token_id=token["cardTokenId"],
            token_id=token["token_id"],
        )
        for token in tokens
    ]
