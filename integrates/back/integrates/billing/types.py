from dataclasses import (
    dataclass,
)
from typing import (
    Literal,
    NamedTuple,
    TypedDict,
)

from stripe import (
    Subscription,
)

from integrates.db_model.groups.enums import (
    GroupTier,
)
from integrates.db_model.organizations.types import (
    DocumentFile,
)


class GroupAuthor(NamedTuple):
    actor: str
    commit: str | None
    groups: frozenset[str]
    organization: str | None
    repository: str | None


class GroupBilling(NamedTuple):
    authors: tuple[GroupAuthor, ...]
    costs_authors: int
    costs_base: int
    costs_total: int
    number_authors: int


class OrganizationActiveGroup(NamedTuple):
    name: str
    tier: GroupTier


class OrganizationAuthor(NamedTuple):
    actor: str
    active_groups: tuple[OrganizationActiveGroup, ...]


class OrganizationBilling(NamedTuple):
    authors: tuple[OrganizationAuthor, ...]
    costs_authors: int
    costs_base: int
    costs_total: int
    number_authors_advanced: int
    number_authors_essential: int
    number_authors_total: int
    number_groups_advanced: int
    number_groups_essential: int
    number_groups_total: int
    organization: str


class PaymentMethod(NamedTuple):
    id: str
    fingerprint: str
    last_four_digits: str
    expiration_month: str
    expiration_year: str
    brand: str
    default: bool
    business_name: str
    city: str
    country: str
    email: str
    state: str
    rut: DocumentFile | None
    tax_id: DocumentFile | None


class Price(NamedTuple):
    id: str
    currency: str
    amount: int


class CustomerTokens(NamedTuple):
    card_token_id: str
    token_id: int


class TreliTokensApiResponse(TypedDict):
    email: str
    cardTokenId: str
    customerId: str
    last: str
    ctype: str
    token_id: int


class ModifyParamsDict(TypedDict):
    Subscription_items: list[Subscription.ModifyParamsItem]
    metadata: dict[str, str]


class ProductDict(TypedDict):
    id: str
    quantity: int
    subscription_period: Literal["month"]
    subscription_period_interval: int
    subscription_price: float


class TreliSubscription:
    action = Literal["pause", "activate", "cancel"]

    class PaginationInfo(TypedDict):
        page: int
        page_size: int
        has_more: bool

    class Items(TypedDict):
        name: str
        quantity: int
        total: str
        id: int
        sku: str

    class MetadataDict(TypedDict):
        value: str
        key: str

    class TreliCustomer(TypedDict):
        fist_name: str
        last_name: str
        company: str
        address_1: str
        address_2: str
        city: str
        state: str
        postcode: str
        country: str
        email: str
        phone: str
        identification: str

    class PaymentObjectDict(TypedDict):
        cardtoken: int
        gateway: Literal["epaycodirect"]
        payment_method: Literal["card"]

    class TreliSubscriptionInfo(TypedDict):
        id: int
        total: str
        total_raw: float
        period: Literal["month"]
        interval: int
        status: Literal["active"]
        items: "TreliSubscription.Items"
        customer: "TreliSubscription.TreliCustomer"
        first_payment: str
        last_payment: str
        free_trial: bool
        currency: Literal["COP"]
        payment_gateway: Literal["epaycodirect"]
        last_payment_url: str | None
        exclude_from_bulk: bool
        meta_data: list[dict[str, str]] | None

    class ListSubscriptionsResponse(TypedDict):
        pagination: "TreliSubscription.PaginationInfo"
        results: list["TreliSubscription.TreliSubscriptionInfo"]

    class BillingAddressDict(TypedDict):
        first_name: str
        last_name: str
        country: str
        company: str
        cedula: int
        address_1: str
        city: str
        state: str
        phone: str

    class CreateTreliParamsDict(TypedDict):
        email: str
        billing_address: "TreliSubscription.BillingAddressDict"
        products: list[ProductDict]
        currency: Literal["COP"]
        payment: "TreliSubscription.PaymentObjectDict"
        meta_data: list["TreliSubscription.MetadataDict"]

    class PaymentResponse(TypedDict):
        success: bool

    class CreateResponse(TypedDict):
        status: Literal["ok", "error"]
        payment_id: int
        subscription_ids: list[int]
        payment_status: Literal["Rechazado", "Aprobado"]
        payment_gateway: Literal["ePayco"]
        payment_response: "TreliSubscription.PaymentResponse"

    class ActionData(TypedDict):
        subscription_id: int
        action: Literal["pause", "activate", "cancel"]

    class BillingAddressUpdateDict(TypedDict, total=False):
        first_name: str
        last_name: str
        country: str
        company: str
        cedula: int
        address_1: str
        city: str
        state: str
        phone: str

    class UpdateData(TypedDict, total=False):
        subscription_id: int
        payment: "TreliSubscription.PaymentObjectDict"
        billing_address: "TreliSubscription.BillingAddressUpdateDict"
        meta_data: list["TreliSubscription.MetadataDict"]


class EPayco:
    class ResponseInfo(TypedDict):
        success: bool
        titleResponse: str
        textResponse: str
        lastAction: str

    class LoginResponse(TypedDict):
        token: str | None
        error: str | None

    class ErrorDetail(TypedDict):
        codeError: int | None
        errorMessage: str | None

    class JsonResponseDetail(TypedDict, total=False):
        status: bool | None
        type: str | None
        message: str | None
        object: Literal["Customer", "token"]
        totalErrors: int | None
        errors: list["EPayco.ErrorDetail"] | None

    class RequestJsonResponse(ResponseInfo, TypedDict):
        data: "EPayco.JsonResponseDetail"


@dataclass
class EPaycoCard:
    last_four_digits: str
    franchise: str

    id: str
    default: bool
    mask: str

    class CardInfo(TypedDict):
        token: str
        franchise: str
        mask: str
        created: str
        default: bool

    class ResponseData(TypedDict):
        status: bool
        message: str

    class SetAsDefaultResponse(EPayco.ResponseInfo, TypedDict):
        data: "EPaycoCard.ResponseData"


@dataclass
class EPaycoCustomer:
    id: str
    email: str
    cards: list[EPaycoCard]
    created: str

    class CreateError(TypedDict):
        status: str
        description: str

    class CreatedData(TypedDict):
        description: str
        customerId: str
        name: str
        email: str

    class CreateDataResponse(TypedDict):
        success: bool
        type: str
        data: "EPaycoCustomer.CreatedData"

    class CreateResponse(EPayco.ResponseInfo, TypedDict):
        data: "EPaycoCustomer.CreateDataResponse"

    class Data(TypedDict):
        id_customer: str
        name: str
        email: str
        cards: list[EPaycoCard.CardInfo]
        created: str

    class GetData(TypedDict):
        data: "EPaycoCustomer.Data"

    class GetResponse(EPayco.ResponseInfo, TypedDict):
        data: "EPaycoCustomer.GetData"

    class UpdateDataResponse(TypedDict):
        success: bool
        type: str
        data: dict[str, str]

    class UpdateResponse(EPayco.ResponseInfo, TypedDict):
        data: "EPaycoCustomer.UpdateDataResponse"
