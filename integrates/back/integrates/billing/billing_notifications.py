import logging
import logging.config
from datetime import (
    datetime,
)

from integrates.billing.authors import (
    get_organization_billing,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.groups.enums import (
    GroupTier,
)
from integrates.db_model.organizations.types import (
    Organization,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger(__name__)


async def _has_groups_on_advanced_plan(
    *,
    organization: Organization,
    loaders: Dataloaders,
) -> bool:
    groups = await loaders.organization_groups.load(organization.id)
    return any(group.state.tier == GroupTier.ADVANCED for group in groups)


async def authors_threshold_reached(
    organization: Organization,
    loaders: Dataloaders,
    threshold: float = 0.9,
) -> bool:
    has_advanced = await _has_groups_on_advanced_plan(
        organization=organization,
        loaders=loaders,
    )

    if not has_advanced:
        return False

    org_billing = await get_organization_billing(
        date=datetime.now(),
        org=organization,
        loaders=loaders,
    )

    if not org_billing.number_authors_advanced:
        LOGGER.warning(
            "No authors in advanced plan",
            extra={
                "extra": {
                    "organization_id": organization.id,
                    "org_name": organization.name,
                },
            },
        )

        return False

    if not organization.policies.max_number_of_expected_contributors:
        return False

    return (
        org_billing.number_authors_advanced
        / organization.policies.max_number_of_expected_contributors
    ) > threshold
