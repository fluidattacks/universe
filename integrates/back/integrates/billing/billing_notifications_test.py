from integrates.billing import (
    billing_notifications,
)
from integrates.billing.billing_notifications import (
    _has_groups_on_advanced_plan,
    authors_threshold_reached,
)
from integrates.billing.types import (
    OrganizationActiveGroup,
    OrganizationAuthor,
    OrganizationBilling,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.groups.enums import (
    GroupTier,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GroupFaker,
    GroupStateFaker,
    OrganizationFaker,
)
from integrates.testing.mocks import Mock, mocks

ORG_ID = "ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id=ORG_ID),
            ],
            groups=[
                GroupFaker(
                    organization_id=ORG_ID,
                    state=GroupStateFaker(tier=GroupTier.ADVANCED),
                ),
                GroupFaker(
                    organization_id=ORG_ID,
                    state=GroupStateFaker(tier=GroupTier.ESSENTIAL),
                    name="testgroup2",
                ),
            ],
        ),
    )
)
async def test__has_groups_on_advanced_plan() -> None:
    loaders = get_new_context()
    organization = await loaders.organization.load(ORG_ID)

    assert organization

    assert await _has_groups_on_advanced_plan(
        organization=organization,
        loaders=loaders,
    )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id=ORG_ID),
            ],
            groups=[
                GroupFaker(
                    organization_id=ORG_ID,
                    state=GroupStateFaker(tier=GroupTier.ADVANCED),
                ),
                GroupFaker(
                    organization_id=ORG_ID,
                    state=GroupStateFaker(tier=GroupTier.ESSENTIAL),
                    name="testgroup2",
                ),
            ],
        ),
    ),
    others=[
        Mock(
            billing_notifications,
            "get_organization_billing",
            "async",
            OrganizationBilling(
                authors=(
                    OrganizationAuthor(
                        actor="jondoe@company.com",
                        active_groups=(
                            OrganizationActiveGroup(
                                name="testgroup",
                                tier=GroupTier.ADVANCED,
                            ),
                        ),
                    ),
                    OrganizationAuthor(
                        actor="janetdoe@company.com",
                        active_groups=(
                            OrganizationActiveGroup(
                                name="testgroup",
                                tier=GroupTier.ADVANCED,
                            ),
                            OrganizationActiveGroup(
                                name="testgroup2",
                                tier=GroupTier.ESSENTIAL,
                            ),
                        ),
                    ),
                ),
                costs_authors=100,
                costs_base=10,
                costs_total=110,
                number_authors_advanced=2,
                number_groups_advanced=1,
                number_groups_essential=1,
                number_groups_total=2,
                number_authors_essential=1,
                number_authors_total=2,
                organization="testorg",
            ),
        )
    ],
)
async def test_authors_threshold_reached() -> None:
    loaders = get_new_context()
    organization = await loaders.organization.load(ORG_ID)

    assert organization
    assert not await authors_threshold_reached(
        organization=organization,
        loaders=loaders,
    )
