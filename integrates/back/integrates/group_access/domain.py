import logging
import logging.config
from contextlib import suppress
from datetime import datetime, timedelta

from aioextensions import collect
from jwcrypto.jwt import JWTExpired

from integrates import authz
from integrates.custom_exceptions import (
    InvalidAuthorization,
    RequestedInvitationTooSoon,
    StakeholderNotFound,
    StakeholderNotInGroup,
)
from integrates.custom_utils import datetime as datetime_utils
from integrates.custom_utils import validations
from integrates.custom_utils.access import format_invitation_state
from integrates.custom_utils.groups import get_group
from integrates.custom_utils.stakeholders import fill_members, get_stakeholder
from integrates.dataloaders import Dataloaders
from integrates.db_model import group_access as group_access_model
from integrates.db_model.group_access.types import (
    GroupAccess,
    GroupAccessMetadataToUpdate,
    GroupAccessRequest,
    GroupAccessState,
    GroupStakeholdersAccessRequest,
)
from integrates.db_model.group_access.utils import merge_group_access_changes
from integrates.db_model.organization_access.enums import OrganizationInvitiationState
from integrates.db_model.stakeholders.types import Stakeholder
from integrates.db_model.vulnerabilities.update import update_assigned_index
from integrates.sessions import domain as sessions_domain
from integrates.settings import LOGGING

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger(__name__)
EMAIL_INTEGRATES = "integrates@fluidattacks.com"


async def get_group_access(loaders: Dataloaders, group_name: str, email: str) -> GroupAccess:
    group_access = await loaders.group_access.load(
        GroupAccessRequest(group_name=group_name, email=email),
    )
    if not group_access:
        raise StakeholderNotInGroup()
    return group_access


async def add_access(
    loaders: Dataloaders,
    email: str,
    group_name: str,
    role: str,
    modified_by: str,
) -> None:
    group_access = await loaders.group_access.load(
        GroupAccessRequest(group_name=group_name, email=email),
    )
    state = (
        group_access.state._replace(
            modified_by=modified_by,
            modified_date=datetime_utils.get_utc_now(),
            has_access=True,
        )
        if group_access
        else GroupAccessState(
            modified_by=modified_by,
            modified_date=datetime_utils.get_utc_now(),
            has_access=True,
        )
    )
    await update(
        loaders=loaders,
        email=email,
        group_name=group_name,
        metadata=GroupAccessMetadataToUpdate(
            state=state,
        ),
    )
    loaders.stakeholder.clear(email)
    loaders.group_access.clear(GroupAccessRequest(group_name=group_name, email=email))

    await authz.grant_group_level_role(loaders, email, group_name, role, modified_by)


async def get_access_by_url_token(loaders: Dataloaders, url_token: str) -> GroupAccess:
    try:
        token_content = sessions_domain.decode_token(url_token)
        group_name: str = token_content["group_name"]
        email: str = token_content["user_email"]
    except (KeyError, JWTExpired) as ex:
        raise InvalidAuthorization() from ex

    return await get_group_access(loaders, group_name, email)


async def get_email_url_token(url_token: str) -> str:
    try:
        token_content = sessions_domain.decode_token(url_token)
        email: str = token_content["user_email"]
    except KeyError as ex:
        raise InvalidAuthorization() from ex

    return email


async def get_stakeholder_async(
    email: str,
    loaders: Dataloaders,
    user_email: str | None,
) -> Stakeholder | None:
    try:
        stakeholder = await get_stakeholder(loaders, email, user_email)
        return stakeholder
    except StakeholderNotFound:
        if not validations.is_fluid_staff(email):
            return Stakeholder(email=email)

    return None


async def collect_stakeholders(
    emails: list[str],
    loaders: Dataloaders,
    user_email: str | None,
) -> list[Stakeholder]:
    email_tasks = [get_stakeholder_async(email, loaders, user_email) for email in emails]
    stakeholders = await collect(email_tasks, workers=8)

    return list(filter(None, stakeholders))


async def get_group_stakeholders(
    loaders: Dataloaders,
    group_name: str,
    user_email: str | None = None,
) -> list[Stakeholder]:
    group_stakeholders = await loaders.group_stakeholders_access.load(
        GroupStakeholdersAccessRequest(group_name=group_name),
    )
    emails = [access.email for access in group_stakeholders]
    stakeholders = await collect_stakeholders(emails, loaders, user_email)
    return stakeholders


async def get_group_stakeholders_emails(
    loaders: Dataloaders,
    group_name: str,
    active: bool = True,
) -> list[str]:
    stakeholders_access = await loaders.group_stakeholders_access.load(
        GroupStakeholdersAccessRequest(group_name=group_name),
    )
    active_stakeholders_email = [
        access.email for access in stakeholders_access if access.state.has_access
    ]
    if active:
        return active_stakeholders_email

    return [
        access.email
        for access in stakeholders_access
        if access.email not in active_stakeholders_email
    ]


async def get_managers(loaders: Dataloaders, group_name: str) -> list[str]:
    stakeholders = await get_group_stakeholders_emails(loaders, group_name, active=True)
    stakeholders_roles = await collect(
        [
            authz.get_group_level_role(loaders, stakeholder, group_name)
            for stakeholder in stakeholders
        ],
    )
    return [
        email
        for email, role in zip(stakeholders, stakeholders_roles, strict=False)
        if role in {"vulnerability_manager", "group_manager"}
    ]


async def get_stakeholders_subscribed_to_consult(
    *,
    loaders: Dataloaders,
    group_name: str,
    comment_type: str,
    is_finding_released: bool = True,
) -> list[str]:
    emails = await get_group_stakeholders_emails(loaders, group_name)
    if comment_type.lower() == "observation" or not is_finding_released:
        roles: tuple[str, ...] = await collect(
            tuple(authz.get_group_level_role(loaders, email, group_name) for email in emails),
            workers=16,
        )
        hackers = [email for email, role in zip(emails, roles, strict=False) if role == "hacker"]

        return hackers

    return emails


async def get_group_members_emails_by_preferences(
    *,
    loaders: Dataloaders,
    group_name: str,
    notification: str | None,
    roles: set[str],
    exclude_trial: bool = False,
    only_fluid_staff: bool = False,
) -> list[str]:
    group = await get_group(loaders, group_name)
    trial = await loaders.trial.load(group.created_by) if "@" in group.created_by else None
    is_trial = trial is not None and not trial.completed
    email_list = await get_stakeholders_email_by_roles(
        loaders=loaders,
        group_name=group_name,
        roles=roles,
    )
    members_data: list[Stakeholder] = []
    await fill_members(loaders, email_list, members_data)
    members_email = [
        member.email
        for member in members_data
        if (not notification or notification in member.state.notifications_preferences.email)
        and not (exclude_trial and is_trial)
        and not (only_fluid_staff and not validations.is_fluid_staff(member.email))
    ]
    return members_email


async def get_stakeholders_email_by_roles(
    *,
    loaders: Dataloaders,
    group_name: str,
    roles: set[str],
) -> list[str]:
    stakeholders = await get_group_stakeholders_emails(loaders, group_name, active=True)
    stakeholder_roles = await collect(
        tuple(
            authz.get_group_level_role(loaders, stakeholder, group_name)
            for stakeholder in stakeholders
        ),
    )
    email_list = [
        str(stakeholder)
        for stakeholder, stakeholder_role in zip(stakeholders, stakeholder_roles, strict=False)
        if stakeholder_role in roles
    ]
    return email_list


async def exists(loaders: Dataloaders, group_name: str, email: str) -> bool:
    if await loaders.group_access.load(GroupAccessRequest(group_name=group_name, email=email)):
        return True
    return False


async def remove_access(loaders: Dataloaders, email: str, group_name: str) -> None:
    all_findings = await loaders.group_findings.load(group_name)
    me_vulnerabilities = await loaders.me_vulnerabilities.load(email)
    findings_ids: set[str] = {finding.id for finding in all_findings}
    group_vulnerabilities = list(
        vulnerability
        for vulnerability in me_vulnerabilities
        if vulnerability.finding_id in findings_ids
    )
    await collect(
        tuple(
            update_assigned_index(
                finding_id=vulnerability.finding_id,
                vulnerability_id=vulnerability.id,
                entry=None,
            )
            for vulnerability in group_vulnerabilities
        ),
        workers=4,
    )

    await group_access_model.remove(email=email, group_name=group_name)
    loaders.group_access.clear(GroupAccessRequest(group_name=group_name, email=email))
    LOGGER.info(
        "Stakeholder removed from group",
        extra={
            "extra": {
                "email": email,
                "group_name": group_name,
            },
        },
    )


async def update(
    loaders: Dataloaders,
    email: str,
    group_name: str,
    metadata: GroupAccessMetadataToUpdate,
) -> None:
    with suppress(StakeholderNotInGroup):
        old_access = await get_group_access(loaders, group_name, email)
        metadata = merge_group_access_changes(old_access, metadata)
    await group_access_model.update_metadata(email=email, group_name=group_name, metadata=metadata)


def validate_new_invitation_time_limit(inv_expiration_time: int) -> bool:
    """
    Validates that new invitations to the same user in the same group/org
    are spaced out by at least one minute to avoid email flooding.
    """
    expiration_date: datetime = datetime_utils.get_from_epoch(inv_expiration_time)
    creation_date: datetime = datetime_utils.get_minus_delta(date=expiration_date, weeks=1)
    current_date: datetime = datetime_utils.get_now()
    if current_date - creation_date < timedelta(minutes=1):
        raise RequestedInvitationTooSoon()
    return True


async def get_stakeholder_role(
    loaders: Dataloaders,
    email: str,
    group_name: str,
    is_registered: bool,
) -> str:
    if not await exists(loaders, group_name, email):
        group_access = GroupAccess(
            email=email,
            group_name=group_name,
            state=GroupAccessState(
                modified_by=EMAIL_INTEGRATES,
                modified_date=datetime_utils.get_utc_now(),
                has_access=False,
            ),
        )
    else:
        group_access = await get_group_access(loaders, group_name, email)
    group_invitation_state = format_invitation_state(
        invitation=group_access.state.invitation,
        is_registered=is_registered,
    )

    return (
        group_access.state.invitation.role
        if group_access.state.invitation
        and group_invitation_state == OrganizationInvitiationState.PENDING
        else await authz.get_group_level_role(loaders, email, group_name)
    )
