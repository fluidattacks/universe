import logging
import logging.config
from collections.abc import Callable, Iterable
from datetime import (
    datetime,
)
from decimal import (
    Decimal,
)
from itertools import (
    chain,
)
from typing import (
    Any,
    NamedTuple,
)

from aioextensions import (
    collect,
)
from aiohttp import (
    ClientConnectorError,
)
from aiohttp.client_exceptions import (
    ClientPayloadError,
    ServerTimeoutError,
)
from botocore.exceptions import (
    ClientError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
)
from more_itertools import (
    chunked,
)
from pyexcelerate import (
    Alignment,
    Color,
    Format,
    Style,
    Workbook,
)

from integrates.custom_exceptions import (
    RootNotFound,
)
from integrates.custom_exceptions import (
    UnavailabilityError as CustomUnavailabilityError,
)
from integrates.custom_utils import (
    cvss as cvss_utils,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.custom_utils import (
    findings as findings_utils,
)
from integrates.custom_utils import (
    roots as roots_utils,
)
from integrates.custom_utils import (
    vulnerabilities as vulns_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.enums import (
    TreatmentStatus,
)
from integrates.db_model.findings.types import (
    Finding,
    FindingRequest,
    FindingVerification,
)
from integrates.db_model.roots.types import (
    GitRoot,
)
from integrates.db_model.types import (
    Treatment,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
    VulnerabilityType,
    VulnerabilityVerificationStatus,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityRequest,
    VulnerabilityVerification,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.dynamodb.exceptions import (
    UnavailabilityError,
)
from integrates.findings.domain import (
    get_filtered_findings,
)
from integrates.reports.types import (
    GroupVulnsReportHeader,
    OrgVulnsReportHeader,
)
from integrates.reports.utils import (
    format_treatment,
    get_filtered_vulnerabilities_max_severity,
    get_filtered_vulnerabilities_min_severity,
    get_first_report_days,
    get_first_treatment,
    get_last_report_days,
    get_metric_translation,
    get_metric_translation_v4,
    get_reattack_date,
    get_reattack_requester,
    get_report_date,
    get_requirements,
    get_severity_level,
)
from integrates.settings.logger import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

EMPTY = "-"
ROW_HEIGHT = 57
RED = Color(255, 52, 53, 1)  # FF3435
WHITE = Color(255, 255, 255, 1)
LOGGER = logging.getLogger(__name__)


class Filters(NamedTuple):
    states: set[VulnerabilityStateStatus] = set(
        [
            VulnerabilityStateStatus["SAFE"],
            VulnerabilityStateStatus["VULNERABLE"],
        ],
    )
    treatments: set[TreatmentStatus] = set(TreatmentStatus)
    verifications: set[VulnerabilityVerificationStatus] = set()
    closing_date: datetime | None = None
    start_closing_date: datetime | None = None
    finding_title: str = ""
    age: int | None = None
    min_severity: Decimal | None = None
    max_severity: Decimal | None = None
    last_report: int | None = None
    min_release_date: datetime | None = None
    max_release_date: datetime | None = None
    location: str = ""


class ITReport:
    """Class to generate IT reports."""

    data: list[Finding] = []
    result_filename = ""
    row = 1

    def __init__(
        self,
        *,
        data: Iterable[Finding],
        group_name: str,
        loaders: Dataloaders,
        filters: Filters = Filters(),
        generate_raw_data: bool = False,
    ) -> None:
        """Initialize variables."""
        self.vulnerability = {
            col_name: index + 1
            for index, col_name in enumerate(
                GroupVulnsReportHeader.labels()
                + (OrgVulnsReportHeader.labels() if generate_raw_data else []),
            )
        }
        self.raw_data: list[list[Any]] = []
        self.workbook: Workbook

        self.row_values: list[str | int | float | datetime | None] = [
            EMPTY for _ in range(len(self.vulnerability) + 1)
        ]
        self.generate_raw_data = generate_raw_data
        self.data = list(data)
        self.loaders = loaders
        self.group_name = group_name
        self.filters = filters
        if self.filters.closing_date or self.filters.start_closing_date:
            self.filters = self.filters._replace(
                states=set(
                    [
                        VulnerabilityStateStatus["SAFE"],
                    ],
                ),
            )
            self.filters = self.filters._replace(treatments=set(TreatmentStatus))
            if self.filters.verifications != set(
                [
                    VulnerabilityVerificationStatus["VERIFIED"],
                ],
            ):
                self.filters = self.filters._replace(verifications=set())

        self.are_all_treatments = len(sorted(self.filters.treatments)) == len(
            sorted(set(TreatmentStatus)),
        )
        self.are_all_verifications = len(self.filters.verifications) == 0

        self.raw_data = [list(self.vulnerability.keys())]
        self.workbook = Workbook()
        self.current_sheet = self.workbook.new_sheet("Data")
        self.parse_template()

    async def generate_data(self) -> None:
        await self.generate(self.data)

    async def generate_file(self) -> None:
        await self.generate(self.data)
        self.style_sheet()
        self.save()

    async def _get_findings_vulnerabilities(
        self,
        findings: Iterable[Finding],
    ) -> list[Vulnerability]:
        finding_vulnerabilities_released_nzr = self.loaders.finding_vulnerabilities_released_nzr
        findings_vulns = chain.from_iterable(
            await collect(
                tuple(
                    finding_vulnerabilities_released_nzr.load_many_chained(chunked_findings)
                    for chunked_findings in chunked([finding.id for finding in findings], 16)
                ),
                workers=4,
            ),
        )

        return list(findings_vulns)

    async def _get_findings_historics_verifications(
        self,
        findings: Iterable[Finding],
    ) -> list[list[FindingVerification]]:
        return await self.loaders.finding_historic_verification.load_many(
            [
                FindingRequest(finding_id=finding.id, group_name=finding.group_name)
                for finding in findings
            ],
        )

    def _get_filtered_max_report_date(
        self,
        vulnerabilities_filtered: list[Vulnerability],
    ) -> list[Vulnerability]:
        if self.filters.max_release_date:
            return [
                vuln
                for vuln in vulnerabilities_filtered
                if get_report_date(vuln) <= self.filters.max_release_date
            ]

        return vulnerabilities_filtered

    def _get_filtered_min_report_date(
        self,
        vulnerabilities_filtered: list[Vulnerability],
    ) -> list[Vulnerability]:
        if self.filters.min_release_date:
            return [
                vuln
                for vuln in vulnerabilities_filtered
                if get_report_date(vuln) >= self.filters.min_release_date
            ]

        return vulnerabilities_filtered

    def filter_closing_date(
        self,
        vulnerabilities_filtered: list[Vulnerability],
    ) -> list[Vulnerability]:
        if self.filters.closing_date:
            vulnerabilities_filtered = [
                vulnerability
                for vulnerability in vulnerabilities_filtered
                if vulnerability.state.modified_date <= self.filters.closing_date
            ]
        return vulnerabilities_filtered

    def filter_start_closing_date(
        self,
        vulnerabilities_filtered: list[Vulnerability],
    ) -> list[Vulnerability]:
        if self.filters.start_closing_date:
            vulnerabilities_filtered = [
                vulnerability
                for vulnerability in vulnerabilities_filtered
                if vulnerability.state.modified_date >= self.filters.start_closing_date
            ]
        return vulnerabilities_filtered

    @staticmethod
    def _get_location(vulnerability: Vulnerability, root_nicknames: dict[str, str]) -> str:
        if (
            vulnerability.type is VulnerabilityType.LINES
            and vulnerability.root_id
            and root_nicknames.get(vulnerability.root_id)
        ):
            return (root_nicknames[vulnerability.root_id] + "/" + vulnerability.state.where).lower()

        return vulnerability.state.where.lower()

    async def filter_location(
        self,
        vulnerabilities_filtered: list[Vulnerability],
    ) -> list[Vulnerability]:
        if self.filters.location:
            roots = await self.loaders.group_roots.load(self.group_name)
            root_nicknames = {root.id: root.state.nickname for root in roots}
            vulnerabilities_filtered = [
                vulnerability
                for vulnerability in vulnerabilities_filtered
                if self._get_location(
                    vulnerability,
                    root_nicknames,
                ).find(self.filters.location.lower())
                >= 0
            ]
        return vulnerabilities_filtered

    @retry_on_exceptions(
        exceptions=(
            ClientConnectorError,
            ClientError,
            ClientPayloadError,
            ConnectionResetError,
            ConnectTimeoutError,
            CustomUnavailabilityError,
            HTTPClientError,
            ReadTimeoutError,
            ServerTimeoutError,
            UnavailabilityError,
        ),
        sleep_seconds=20,
        max_attempts=3,
    )
    async def generate(self, data: Iterable[Finding]) -> None:
        filter_finding_title = data
        filter_age = data
        filter_min_severity = data
        filter_max_severity = data
        filter_last_report = data
        if self.filters.finding_title:
            filter_finding_title = tuple(
                finding for finding in data if finding.title.startswith(self.filters.finding_title)
            )
        if self.filters.age is not None:
            filter_age = tuple(
                finding for finding in data if get_first_report_days(finding) <= self.filters.age
            )
        if self.filters.last_report is not None:
            filter_last_report = tuple(
                finding
                for finding in data
                if get_last_report_days(finding) <= self.filters.last_report
            )
        filter_findings_location = (
            await get_filtered_findings(
                root=self.filters.location,
                group_name=self.group_name,
                loaders=self.loaders,
                technique=None,
                title=None,
                has_internal_role=False,
            )
            if self.filters.location
            else data
        )

        filtered_findings_ids = set.intersection(
            *[
                set(finding.id for finding in filter_finding_title),
                set(finding.id for finding in filter_age),
                set(finding.id for finding in filter_min_severity),
                set(finding.id for finding in filter_max_severity),
                set(finding.id for finding in filter_last_report),
                set(finding.id for finding in filter_findings_location),
            ],
        )

        data = tuple(finding for finding in data if finding.id in filtered_findings_ids)
        findings_vulnerabilities = await self._get_findings_vulnerabilities(data)
        findings_verifications = await self._get_findings_historics_verifications(data)
        finding_data = {finding.id: finding for finding in data}
        finding_verification = {
            finding.id: verification
            for finding, verification in zip(data, findings_verifications, strict=False)
        }

        vulnerabilities_filtered = [
            vulnerability
            for vulnerability in findings_vulnerabilities
            if (
                (
                    vulnerability.treatment
                    and vulnerability.treatment.status in self.filters.treatments
                )
                or (not vulnerability.treatment and self.are_all_treatments)
            )
            and vulnerability.state.status in self.filters.states
            and (
                (
                    vulnerability.verification
                    and vulnerability.verification.status in self.filters.verifications
                )
                or (
                    not vulnerability.verification
                    and VulnerabilityVerificationStatus.NOT_REQUESTED in self.filters.verifications
                )
                or self.are_all_verifications
            )
        ]
        vulnerabilities_filtered = self.filter_closing_date(vulnerabilities_filtered)
        vulnerabilities_filtered = self.filter_start_closing_date(vulnerabilities_filtered)
        vulnerabilities_filtered = await self.filter_location(vulnerabilities_filtered)
        vulnerabilities_filtered = self._get_filtered_min_report_date(vulnerabilities_filtered)
        vulnerabilities_filtered = self._get_filtered_max_report_date(vulnerabilities_filtered)

        if self.filters.min_severity is not None:
            vulnerabilities_filtered = get_filtered_vulnerabilities_min_severity(
                finding_data,
                self.filters.min_severity,
                vulnerabilities_filtered,
            )
        if self.filters.max_severity is not None:
            vulnerabilities_filtered = get_filtered_vulnerabilities_max_severity(
                finding_data,
                self.filters.max_severity,
                vulnerabilities_filtered,
            )
        await self._sort_and_set_vulns_row(
            vulnerabilities_filtered,
            finding_data,
            finding_verification,
        )

    async def _sort_and_set_vulns_row(
        self,
        vulnerabilities_filtered: list[Vulnerability],
        finding_data: dict[str, Finding],
        finding_verification: dict[str, list[FindingVerification]],
    ) -> None:
        vulnerabilities_filtered_and_sorted = sorted(
            vulnerabilities_filtered,
            key=lambda x: vulns_utils.get_severity_threat_score(x, finding_data[x.finding_id]),
            reverse=True,
        )
        vulnerabilities_historics = await collect(
            tuple(
                self._get_vulnerability_data(vulnerability)
                for vulnerability in vulnerabilities_filtered_and_sorted
            ),
            workers=8,
        )
        for vulnerability, historics in zip(
            vulnerabilities_filtered_and_sorted,
            vulnerabilities_historics,
            strict=False,
        ):
            await self.set_vuln_row(
                row=vulnerability,
                finding=finding_data[vulnerability.finding_id],
                historic_verification=historics[1],
                historic_treatment=historics[0],
                finding_verification=finding_verification[vulnerability.finding_id],
            )
            self.row += 1

    def get_row_range(self, row: int) -> list[str]:
        if self.generate_raw_data:
            return [f"A{row}", f"BU{row}"]
        return [f"A{row}", f"BT{row}"]

    def parse_template(self) -> None:
        self.current_sheet.range(*self.get_row_range(self.row)).value = [
            list(self.vulnerability.keys()),
        ]
        self.row += 1

    def save(self) -> None:
        today_date = datetime_utils.get_as_str(
            datetime_utils.get_now(),
            date_format="%Y-%m-%dT%H-%M-%S",
        )
        self.result_filename = f"technical-{self.group_name}-{today_date}.xlsx"
        self.workbook.save(self.result_filename)

    def set_cvss_metrics_cell(
        self,
        *,
        cvss_vector: str,
        cvss_metrics: list[str],
        title_cvss_key: str,
        calculator_url: str,
        get_metric: Callable[[str, dict[str, str]], str],
    ) -> None:
        cvss_vector_dict = {
            metric.split(":")[0]: metric.split(":")[1]
            for metric in cvss_vector.split("/")
            if metric not in {"CVSS:3.0", "CVSS:3.1", "CVSS:4.0"}
        }
        for ind, measure in enumerate(cvss_metrics):
            value = get_metric(measure, cvss_vector_dict)
            self.row_values[self.vulnerability[title_cvss_key] + ind + 1] = value

        cvss_calculator_url = f"{calculator_url}{cvss_vector}"
        cell_content = f'=HYPERLINK("{cvss_calculator_url}", "{cvss_vector}")'
        self.row_values[self.vulnerability[title_cvss_key]] = (
            cvss_calculator_url if self.generate_raw_data else cell_content
        )

    def set_severity_cell(self, finding: Finding, vulnerability: Vulnerability) -> None:
        threat_score = vulns_utils.get_severity_threat_score(vulnerability, finding)
        temporal_score = vulns_utils.get_severity_temporal_score(vulnerability, finding)
        self.row_values[self.vulnerability["Severity (v4.0)"]] = float(threat_score)
        self.row_values[self.vulnerability["Severity (v3.1)"]] = float(temporal_score)
        self.row_values[self.vulnerability["Severity Level"]] = (
            get_severity_level(temporal_score).lstrip("(").rstrip(")")
        )
        self.row_values[self.vulnerability["Severity Level (v4.0)"]] = (
            get_severity_level(threat_score).lstrip("(").rstrip(")")
        )

    def set_advisories_cell(self, vulnerability: Vulnerability) -> None:
        adv = vulnerability.state.advisories
        self.row_values[self.vulnerability["Package"]] = adv.package if adv else EMPTY
        self.row_values[self.vulnerability["Vulnerable Version"]] = (
            adv.vulnerable_version if adv else EMPTY
        )
        self.row_values[self.vulnerability["CVE"]] = ", ".join(adv.cve) if adv else EMPTY
        self.row_values[self.vulnerability["EPSS"]] = (
            f"{adv.epss}%" if (adv and adv.epss) else EMPTY
        )

    async def set_cwe_ids_cell(self, finding: Finding, vulnerability: Vulnerability) -> None:
        cwe_ids = (
            vulnerability.cwe_ids
            if vulnerability.cwe_ids
            else await findings_utils.get_finding_criteria_cwe_ids(title=finding.title)
        )
        cwe_ids_formatted = "\n".join(cvss_utils.parse_cwe_ids(cwe_ids) or [])
        self.row_values[self.vulnerability["CWE ids"]] = cwe_ids_formatted

    async def set_finding_data(self, finding: Finding, vuln: Vulnerability) -> None:
        if not (group := await self.loaders.group.load(self.group_name)):
            return
        group_lang = str(group.language.value).lower()
        finding_requirements = get_requirements(
            lang=group_lang,
            unfulfilled_requirements=finding.unfulfilled_requirements,
        )
        finding_data = {
            "Description": finding.description,
            "Status": vuln.state.status.value,
            "Requirements": finding_requirements,
            "Impact": finding.attack_vector_description,
            "Threat": finding.threat,
            "Recommendation": finding.recommendation,
        }
        for key, value in finding_data.items():
            self.row_values[self.vulnerability[key]] = value

    def set_reattack_data(
        self,
        vuln: Vulnerability,
        historic_verification: tuple[VulnerabilityVerification, ...],
        finding_verification: tuple[FindingVerification, ...],
    ) -> None:
        reattack_requested = None
        reattack_date = None
        reattack_requester = None
        n_requested_reattacks = None
        remediation_effectiveness: str = EMPTY
        if historic_verification:
            vuln_verification: VulnerabilityVerification = historic_verification[-1]
            reattack_requested = (
                vuln_verification.status == VulnerabilityVerificationStatus.REQUESTED
            )
            n_requested_reattacks = len(
                [
                    verification
                    for verification in historic_verification
                    if verification.status == VulnerabilityVerificationStatus.REQUESTED
                ],
            )
            if vuln.state.status == VulnerabilityStateStatus.SAFE and n_requested_reattacks:
                effectiveness: float = 100 / n_requested_reattacks
                remediation_effectiveness = f"{f'{effectiveness:.2f}'.rstrip('0').rstrip('.')}%"
            reattack_date = get_reattack_date(historic_verification)
            reattack_requester = get_reattack_requester(vuln, finding_verification)
        reattack_data = {
            "Pending Reattack": "Yes" if reattack_requested else "No",
            "# Requested Reattacks": n_requested_reattacks or "0",
            "Last requested reattack": reattack_date or EMPTY,
            "Last reattack Requester": reattack_requester or EMPTY,
            "Remediation Effectiveness": remediation_effectiveness,
        }
        for key, value in reattack_data.items():
            self.row_values[self.vulnerability[key]] = value

    def set_row_height(self) -> None:
        self.current_sheet.set_row_style(
            self.row,
            Style(size=ROW_HEIGHT, alignment=Alignment(wrap_text=True)),
        )
        # this makes that the cells for severity get the right format
        self.current_sheet.set_cell_style(
            self.row,
            10,
            Style(
                size=ROW_HEIGHT,
                alignment=Alignment(wrap_text=True),
                format=Format(0.0),
            ),
        )

    def set_treatment_data(
        self,
        vuln: Vulnerability,
        historic_treatment: tuple[Treatment, ...],
    ) -> None:
        first_treatment = get_first_treatment(historic_treatment)
        current_treatment_data: dict[str, str] = {}
        current_treatment_exp_date = EMPTY
        if vuln.treatment:
            if vuln.treatment.accepted_until:
                current_treatment_exp_date = datetime_utils.get_as_str(
                    vuln.treatment.accepted_until,
                )
            current_treatment_data = {
                "Current Treatment": format_treatment(vuln.treatment.status),
                "Current Treatment Moment": (
                    datetime_utils.get_as_str(vuln.treatment.modified_date)
                ),
                "Current Treatment Justification": (vuln.treatment.justification or EMPTY),
                "Current Treatment expiration Moment": (current_treatment_exp_date),
                "Current Assigned": vuln.treatment.assigned or EMPTY,
            }
        first_treatment_data = {
            "First Treatment": EMPTY,
            "First Treatment Moment": EMPTY,
            "First Treatment Justification": EMPTY,
            "First Treatment expiration Moment": EMPTY,
            "First Assigned": EMPTY,
        }
        if first_treatment:
            first_expiration = EMPTY
            if (
                first_treatment.status == TreatmentStatus.ACCEPTED
                and first_treatment.accepted_until
            ):
                first_expiration = datetime_utils.get_as_str(first_treatment.accepted_until)
            first_treatment_data = {
                "First Treatment": format_treatment(first_treatment.status),
                "First Treatment Moment": datetime_utils.get_as_str(first_treatment.modified_date),
                "First Treatment Justification": first_treatment.justification or EMPTY,
                "First Treatment expiration Moment": first_expiration,
                "First Assigned": first_treatment.assigned or EMPTY,
            }

        for key, value in current_treatment_data.items():
            self.row_values[self.vulnerability[key]] = (
                value if vuln.state.status == VulnerabilityStateStatus.VULNERABLE else EMPTY
            )
            first_treatment_key = key.replace("Current", "First")
            kword = self.vulnerability[first_treatment_key]
            self.row_values[kword] = first_treatment_data[first_treatment_key]

    async def _get_historic_treatment(
        self,
        vulnerability_id: str,
        finding_id: str,
    ) -> tuple[Treatment, ...]:
        return tuple(
            await self.loaders.vulnerability_historic_treatment.load(
                VulnerabilityRequest(vulnerability_id=vulnerability_id, finding_id=finding_id),
            ),
        )

    async def _get_historic_verification(
        self,
        vulnerability: Vulnerability,
    ) -> tuple[VulnerabilityVerification, ...]:
        if not vulnerability.verification:
            return tuple()
        return tuple(
            await self.loaders.vulnerability_historic_verification.load(
                VulnerabilityRequest(
                    vulnerability_id=vulnerability.id,
                    finding_id=vulnerability.finding_id,
                ),
            ),
        )

    @retry_on_exceptions(
        exceptions=(
            ClientConnectorError,
            ClientError,
            ClientPayloadError,
            ConnectionResetError,
            ConnectTimeoutError,
            CustomUnavailabilityError,
            HTTPClientError,
            ReadTimeoutError,
            ServerTimeoutError,
            UnavailabilityError,
        ),
        sleep_seconds=20,
        max_attempts=10,
    )
    async def _get_vulnerability_data(
        self,
        vuln: Vulnerability,
    ) -> tuple[
        tuple[Treatment, ...],
        tuple[VulnerabilityVerification, ...],
    ]:
        return (
            await self._get_historic_treatment(vuln.id, vuln.finding_id),
            await self._get_historic_verification(vuln),
        )

    async def set_vuln_row(
        self,
        *,
        row: Vulnerability,
        finding: Finding,
        historic_verification: tuple[VulnerabilityVerification, ...],
        historic_treatment: tuple[Treatment, ...],
        finding_verification: list[FindingVerification],
    ) -> None:
        vuln = self.vulnerability
        specific = row.state.specific

        commit = EMPTY
        if row.state.commit:
            commit = row.state.commit[0:7]

        tags = EMPTY
        if row.tags:
            tags = ", ".join(sorted(row.tags))

        stream = EMPTY
        if row.stream:
            stream = " > ".join(row.stream)

        business_criticality = EMPTY
        if row.custom_severity:
            business_criticality = str(row.custom_severity)

        nickname = EMPTY
        self.row_values[self.vulnerability["Root Branch"]] = EMPTY
        if row.root_id:
            try:
                root = await roots_utils.get_root(self.loaders, row.root_id, finding.group_name)
                nickname = root.state.nickname
                self.row_values[self.vulnerability["Root Branch"]] = (
                    root.state.branch if isinstance(root, GitRoot) else EMPTY
                )
            except RootNotFound as ex:
                LOGGER.exception(
                    ex,
                    extra={
                        "extra": {
                            "finding_id": finding.id,
                            "group_name": row.group_name,
                            "root_id": row.root_id,
                            "vuln_id": row.id,
                        },
                    },
                )

        self.row_values[vuln["#"]] = self.row - 1
        self.row_values[vuln["Related Finding"]] = finding.title
        self.row_values[vuln["Finding Id"]] = finding.id
        self.row_values[vuln["Vulnerability Id"]] = row.id
        self.row_values[vuln["Where"]] = row.state.where
        self.row_values[vuln["Business Criticality"]] = business_criticality
        self.row_values[vuln["Specific"]] = specific
        self.row_values[vuln["Commit Hash"]] = commit
        self.row_values[vuln["Tags"]] = tags
        self.row_values[vuln["Technique"]] = row.technique
        self.row_values[vuln["Stream"]] = stream
        self.row_values[vuln["Root Nickname"]] = nickname
        if self.generate_raw_data:
            self.row_values[vuln["Group"]] = row.group_name

        await self.set_finding_data(finding, row)
        self.set_vuln_temporal_data(row)
        self.set_treatment_data(row, historic_treatment)
        self.set_reattack_data(row, historic_verification, tuple(finding_verification))
        self.set_cvss_metrics_cell(
            cvss_vector=vulns_utils.get_severity_cvss_vector(row, finding),
            cvss_metrics=[
                "AV",
                "AC",
                "PR",
                "UI",
                "S",
                "C",
                "I",
                "A",
                "E",
                "RL",
                "RC",
            ],
            title_cvss_key="CVSSv3.1 string vector",
            calculator_url="https://www.first.org/cvss/calculator/3.1#",
            get_metric=get_metric_translation,
        )
        self.set_cvss_metrics_cell(
            cvss_vector=vulns_utils.get_severity_cvss4_vector(row, finding),
            cvss_metrics=[
                "AV",
                "AC",
                "AT",
                "PR",
                "UI",
                "VC",
                "VI",
                "VA",
                "SC",
                "SI",
                "SA",
                "E",
            ],
            title_cvss_key="CVSSv4.0 string vector",
            calculator_url="https://www.first.org/cvss/calculator/4.0#",
            get_metric=get_metric_translation_v4,
        )
        self.set_severity_cell(finding, row)
        self.set_advisories_cell(row)
        await self.set_cwe_ids_cell(finding, row)

        self.raw_data.append(self.row_values[1:])
        self.current_sheet.range(*self.get_row_range(self.row)).value = [self.row_values[1:]]
        self.set_row_height()

    def set_vuln_temporal_data(self, vuln: Vulnerability) -> None:
        vuln_date = vuln.unreliable_indicators.unreliable_report_date or vuln.created_date
        limit_date = datetime_utils.get_utc_now()
        vuln_close_date: str | datetime = EMPTY
        if vuln.state.status == VulnerabilityStateStatus.SAFE:
            limit_date = vuln_close_date = vuln.state.modified_date
        vuln_age_days = int((limit_date - vuln_date).days) if vuln_date else 0
        external_bts = vuln.bug_tracking_system_url or EMPTY

        vuln_temporal_data: dict[str, str | int | float | datetime | None] = {
            "Report Moment": vuln_date,
            "Age in days": vuln_age_days,
            "Close Moment": vuln_close_date,
            "External BTS": external_bts
            if self.generate_raw_data
            else f'=HYPERLINK("{external_bts}", "{external_bts}")',
        }
        for key, value in vuln_temporal_data.items():
            self.row_values[self.vulnerability[key]] = value

    def style_sheet(self) -> None:
        header = self.current_sheet.range(*self.get_row_range(1))
        header.style.fill.background = RED
        header.style.font.color = WHITE
        header.style.alignment.horizontal = "center"
        header.style.alignment.vertical = "center"
        header.style.alignment.wrap_text = True

        for column, col_width in enumerate(
            GroupVulnsReportHeader.widths()
            + (OrgVulnsReportHeader.widths() if self.generate_raw_data else []),
            start=1,
        ):
            self.current_sheet.set_col_style(
                column,
                Style(size=col_width, alignment=Alignment(wrap_text=True)),
            )
