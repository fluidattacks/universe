= {{group_name}}
:doctype: book




{%if has_unfulfilled_standards %}
=== Normas sin cumplir
El grupo no cumple con los siguientes estándares dado que tiene requisitos
sin cumplir que estan relacionados.
{% else %}
=== Felicidades
Actualmente, el grupo cumple con todos los estándares validados por Fluid
Attacks en su organización.
{% endif %}


{% for unfulfilled_standard in unfulfilled_standards_to_display %}
==== {{unfulfilled_standard.title}}
{{unfulfilled_standard.summary}} +
[cols="2*<",options=header]
|===
|Requisitos sin cumplir|Locación
{% for unfulfilled_requirement in unfulfilled_standard.unfulfilled_requirements %}
    |https://help.fluidattacks.com/portal/en/kb/articles/criteria-requirements-{{unfulfilled_requirement.id}}[{{unfulfilled_requirement.id}}. {{unfulfilled_requirement.title}}] +
|{% for finding in unfulfilled_requirement.findings %}
    {{unfulfilled_requirement.base_url}}/orgs/{{unfulfilled_requirement.org_name}}/groups/{{unfulfilled_requirement.group_name}}/vulns/{{finding.id}}/locations[{{finding.title}}] +
{%- endfor %}
{%- endfor %}
|===
{%- endfor %}

<<< {{fluid_tpl['footer_adoc']}}
