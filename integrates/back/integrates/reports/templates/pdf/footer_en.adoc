image::{{hs_signature_img}}[Signature,180,45]
Carolina Carrasco +
Head of Service +
Fluid Attacks Inc.


image::{{vph_signature_img}}[Signature,180,45]
Andrés Roldán +
VP of Hacking +
https://www.linkedin.com/in/andres-roldan/ +
OSEE, OSCE, OSEP, OSCP, OSWP, GXPN, GPEN, eCPTXv2, eWPTXv2, eCXD, eCRE, eMAPT, eWPT, CRTL, CRTO, CRTE, CRTP, CARTP, CCT-APP, CRT, CPSA, CEH-Master, CCRTA, PNPT, CPTC, CSWAE, CPTE, PenTest+ and CAP. +
Fluid Attacks Inc.

Copyright © {{year}} Fluid Attacks

All rights reserved. This document contains information proprietary
of Fluid Attacks. The client is only allowed to use such information for documentation
purposes and without disclosing its content to third parties because it may contain
ideas, concepts, prices and/or structures propriety of Fluid Attacks.
Its 'proprietary' classification means that this information will only be used by those
who it was meant for. In case of requiring total or partial reproductions it must be done
with express and written authorization of Fluid Attacks.
The rules that fundament the classification of information are articles 72, Cartagena's
agreement 344 of 1993, article 238 of penal code and articles 16 and its following ones
from 256 law of 1996.
