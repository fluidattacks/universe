import logging
import tempfile
from collections.abc import (
    Iterable,
    ValuesView,
)
from datetime import (
    datetime,
)
from decimal import (
    ROUND_CEILING,
    Decimal,
)
from typing import (
    TypedDict,
)

import aiofiles
import jinja2
from aioextensions import (
    collect,
)
from jinja2.utils import (
    select_autoescape,
)

from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.custom_utils.groups import get_group
from integrates.custom_utils.reports import (
    filter_context,
    get_ordinal_ending,
)
from integrates.custom_utils.roots import (
    get_active_git_roots,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.db_model.roots.types import (
    RootEnvironmentUrlsRequest,
)
from integrates.findings import (
    domain as findings_domain,
)
from integrates.groups import (
    domain as groups_domain,
)
from integrates.reports.pdf import (
    CreatorPdf,
)
from integrates.reports.types import (
    CertFindingInfo,
)
from integrates.reports.utils import (
    call,
)
from integrates.s3 import (
    operations as s3_ops,
)
from integrates.settings.logger import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger(__name__)


class CertContext(TypedDict):
    business: str
    business_number: str
    environment_urls: dict[str, str]
    has_advanced: bool
    solution: str
    remediation_table: ValuesView[list[int | str]]
    remediation_table_v4: ValuesView[list[int | str]]
    start_day: str
    start_month: str
    start_year: str
    start_ordinal_ending: str
    remediation_rate: str
    report_day: str
    report_month: str
    report_year: str
    report_ordinal_ending: str
    roots: dict[str, str]
    signature_img: str
    vph_signature_img: str
    total_risk_exposure: Decimal
    words: dict[str, str]


async def format_finding(
    loaders: Dataloaders,
    finding: Finding,
) -> CertFindingInfo:
    closed_vulnerabilities = await findings_domain.get_safe_cvssf_v3(loaders, finding.id)
    total_exposure = await findings_domain.get_total_cvssf_v3(loaders, finding.id)
    severity_levels = await findings_domain.get_severity_levels_info(loaders, finding)

    return CertFindingInfo(
        closed_exposure=closed_vulnerabilities,
        total_exposure=total_exposure,
        severity_levels=severity_levels,
        closed_exposure_v4=await findings_domain.get_safe_cvssf(loaders, finding.id),
        total_exposure_v4=await findings_domain.get_total_cvssf(loaders, finding.id),
        severity_levels_v4=await findings_domain.get_severity_levels_info_v4(loaders, finding),
    )


def _set_percentage(total_vulns: Decimal, closed_vulns: Decimal) -> str:
    if total_vulns != 0:
        percentage = closed_vulns * Decimal("100.0") / total_vulns
        if percentage == percentage.to_integral():
            return f"{percentage.to_integral():.0f}%"
        return f"{percentage.quantize(Decimal('0.1')):.1f}%"
    return "N/A"


def get_created_date(date: datetime | None) -> str:
    if not date:
        return "-"

    return date.strftime("%Y-%m-%d")


async def get_environments_url(
    loaders: Dataloaders,
    root_id: str,
    group_name: str,
) -> list[dict[str, str]]:
    environments_urls = await loaders.root_environment_urls.load(
        RootEnvironmentUrlsRequest(root_id=root_id, group_name=group_name),
    )
    return [
        {f"{env_url.url}": get_created_date(env_url.created_at)}
        for env_url in environments_urls
        if env_url.state.include
    ]


def _format_cvssf(cvssf: Decimal) -> Decimal:
    if cvssf >= Decimal("10.0"):
        return cvssf.to_integral_exact(rounding=ROUND_CEILING)

    updated_cvssf = cvssf.quantize(Decimal("0.1"))
    return (
        updated_cvssf.to_integral()
        if updated_cvssf == updated_cvssf.to_integral()
        else updated_cvssf
    )


def _format_severity_level(
    findings_info: Iterable[CertFindingInfo],
    severity_level: str,
    word: str,
) -> list[int | str]:
    total_vulns = sum(
        getattr(finding.severity_levels, severity_level).total for finding in findings_info
    )
    closed_vulns = sum(
        getattr(finding.severity_levels, severity_level).closed for finding in findings_info
    )
    accepted_vulns = sum(
        getattr(finding.severity_levels, severity_level).accepted for finding in findings_info
    )
    remediation_percentage = _set_percentage(total_vulns, closed_vulns)

    return [
        word,
        total_vulns,
        closed_vulns,
        accepted_vulns,
        remediation_percentage,
    ]


def _format_severity_level_v4(
    findings_info: Iterable[CertFindingInfo],
    severity_level: str,
    word: str,
) -> list[int | str]:
    total_vulns = sum(
        getattr(finding.severity_levels_v4, severity_level).total for finding in findings_info
    )
    closed_vulns = sum(
        getattr(finding.severity_levels_v4, severity_level).closed for finding in findings_info
    )
    accepted_vulns = sum(
        getattr(finding.severity_levels_v4, severity_level).accepted for finding in findings_info
    )
    remediation_percentage = _set_percentage(total_vulns, closed_vulns)

    return [
        word,
        total_vulns,
        closed_vulns,
        accepted_vulns,
        remediation_percentage,
    ]


def make_remediation_table(
    findings_info: Iterable[CertFindingInfo],
    words: dict[str, str],
) -> ValuesView[list[int | str]]:
    critical, high, medium, low = (
        words["vuln_c"],
        words["vuln_h"],
        words["vuln_m"],
        words["vuln_l"],
    )
    remediation_dict = {
        critical: _format_severity_level(findings_info, "critical", critical),
        high: _format_severity_level(findings_info, "high", high),
        medium: _format_severity_level(findings_info, "medium", medium),
        low: _format_severity_level(findings_info, "low", low),
    }

    return remediation_dict.values()


def make_remediation_table_v4(
    findings_info: Iterable[CertFindingInfo],
    words: dict[str, str],
) -> ValuesView[list[int | str]]:
    critical, high, medium, low = (
        words["vuln_c"],
        words["vuln_h"],
        words["vuln_m"],
        words["vuln_l"],
    )
    remediation_dict = {
        critical: _format_severity_level_v4(findings_info, "critical", critical),
        high: _format_severity_level_v4(findings_info, "high", high),
        medium: _format_severity_level_v4(findings_info, "medium", medium),
        low: _format_severity_level_v4(findings_info, "low", low),
    }

    return remediation_dict.values()


def resolve_month_name(lang: str, date: datetime, words: dict[str, str]) -> str:
    if lang.lower() == "en":
        return date.strftime("%B")
    return words[date.strftime("%B").lower()]


class CertificateCreator(CreatorPdf):
    """Class to generate certificates in PDF."""

    cert_context: CertContext

    def __init__(self, *, lang: str, doctype: str, tempdir: str, group: str, user: str) -> None:
        """Class constructor"""
        super().__init__(
            lang=lang,
            doctype=doctype,
            tempdir=tempdir,
            group=group,
            user=user,
            style="certificate",
        )
        self.proj_tpl = f"templates/pdf/certificate_{lang}.adoc"

    async def fill_context(
        self,
        findings: Iterable[Finding],
        group_name: str,
        description: str,
        loaders: Dataloaders,
    ) -> None:
        """Fetch information and fill out the context."""
        words = self.wordlist[self.lang]
        context_findings = await collect([format_finding(loaders, finding) for finding in findings])
        remediation_table = make_remediation_table(context_findings, words)
        group = await get_group(loaders, group_name)
        active_git_roots = await get_active_git_roots(loaders, group_name)
        roots_url_branch = {
            f"{root.state.url} ({root.state.branch})": get_created_date(root.created_date)
            for root in active_git_roots
        }
        environment_urls = await collect(
            [get_environments_url(loaders, root.id, root.group_name) for root in active_git_roots],
        )
        oldest_vuln_date: datetime | None = await groups_domain.get_oldest_finding_date(
            loaders,
            group_name,
        )
        start_date: datetime = (
            min(group.created_date, oldest_vuln_date) if oldest_vuln_date else group.created_date
        )
        current_date = datetime_utils.get_utc_now()

        self.cert_context = {
            "business": group.business_name or "",
            "business_number": group.business_id or "",
            "environment_urls": {
                list(env_url.keys())[0]: list(env_url.values())[0]
                for env_urls in environment_urls
                for env_url in env_urls
                if env_url
            },
            "has_advanced": group.state.has_advanced,
            "remediation_table": remediation_table,
            "remediation_table_v4": make_remediation_table_v4(context_findings, words),
            "start_day": str(start_date.day),
            "start_month": resolve_month_name(self.lang, start_date, words),
            "start_year": str(start_date.year),
            "start_ordinal_ending": get_ordinal_ending(start_date.day),
            "remediation_rate": _set_percentage(
                Decimal(sum(finding.total_exposure_v4 for finding in context_findings)),
                Decimal(sum(finding.closed_exposure_v4 for finding in context_findings)),
            ),
            "report_day": str(current_date.day),
            "report_month": resolve_month_name(self.lang, current_date, words),
            "report_year": str(current_date.year),
            "report_ordinal_ending": get_ordinal_ending(current_date.day),
            "roots": roots_url_branch,
            "solution": description,
            "signature_img": "HoS signature",
            "vph_signature_img": "VPoH signature",
            "total_risk_exposure": _format_cvssf(
                Decimal(sum(finding.total_exposure_v4 for finding in context_findings)),
            ),
            "words": words,
        }

    async def cert(
        self,
        findings: Iterable[Finding],
        group_name: str,
        description: str,
        loaders: Dataloaders,
    ) -> None:
        """Create the template to render and apply the context."""
        await self.fill_context(findings, group_name, description, loaders)
        current_date = datetime_utils.get_as_str(datetime_utils.get_now())
        self.out_name = f"certificate-{self.group_name}-{current_date}.pdf"
        searchpath = self.path
        template_loader = jinja2.FileSystemLoader(searchpath=searchpath)
        template_env = jinja2.Environment(
            loader=template_loader,
            autoescape=select_autoescape(["html", "xml"], default=True),
        )
        template = template_env.get_template(self.proj_tpl)
        tpl_name = f"{self.tpl_dir}{group_name}_CERT.tpl"
        # Fetch signature resource
        with (
            tempfile.NamedTemporaryFile(mode="w+") as file,
            tempfile.NamedTemporaryFile(mode="w+") as vph_file,
        ):
            await s3_ops.download_file(
                "resources/certificate/signature.png",
                file.name,
            )
            self.cert_context["signature_img"] = file.name
            await s3_ops.download_file(
                "resources/certificate/vph_signature.png",
                vph_file.name,
            )
            self.cert_context["vph_signature_img"] = vph_file.name
            render_text = template.render(filter_context(self.cert_context))
            async with aiofiles.open(tpl_name, "wb") as tplfile:
                await tplfile.write(render_text.encode("utf-8"))
            self.create_command(tpl_name, self.out_name)
            await call(
                program=self.command[0],
                command=self.command[1:],
                group_name=self.group_name,
            )
