from decimal import Decimal
from operator import itemgetter
from tempfile import TemporaryDirectory

from aioextensions import collect

from integrates.custom_utils.findings import get_group_findings
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.vulnerabilities.enums import VulnerabilityStateStatus
from integrates.findings.domain.core import get_max_open_severity_score_v4
from integrates.reports.certificate import CertificateCreator
from integrates.reports.report_types.certificate import generate_cert_file
from integrates.s3 import (
    operations as s3_ops,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    FindingFaker,
    GitRootFaker,
    GitRootStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
    VulnerabilityFaker,
    VulnerabilityStateFaker,
)
from integrates.testing.mocks import Mock, mocks

EMAIL_TEST = "user@clientapp.com"
GROUP_NAME = "group1"
ROOT_ID = "root1"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_TEST, role="admin"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(
                        nickname="back",
                    ),
                ),
            ],
            findings=[
                FindingFaker(id="3c475384-834c-47b0-ac71-a41a022e401c", group_name=GROUP_NAME),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    id="891b1d7f-a0fa-4e3d-834a-d07a9ad4b46f",
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                    state=VulnerabilityStateFaker(
                        status=VulnerabilityStateStatus.SAFE,
                    ),
                    group_name=GROUP_NAME,
                ),
                VulnerabilityFaker(
                    id="431b1d7f-a0fa-4e3d-834a-d07a9ad4b46d",
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                    group_name=GROUP_NAME,
                ),
            ],
        ),
    ),
    others=[
        Mock(s3_ops, "download_file", "async", None),
    ],
)
async def test_report_cert() -> None:
    loaders: Dataloaders = get_new_context()

    group = await loaders.group.load(GROUP_NAME)
    group_findings = await get_group_findings(group_name=GROUP_NAME, loaders=loaders)
    group_findings_score = await collect(
        get_max_open_severity_score_v4(loaders, finding.id) for finding in group_findings
    )
    findings_ord = tuple(
        finding
        for finding, _ in sorted(
            zip(group_findings, group_findings_score, strict=False),
            key=itemgetter(1),
            reverse=True,
        )
    )
    assert group
    assert (
        await generate_cert_file(
            loaders=loaders,
            description=group.description,
            findings_ord=findings_ord,
            group_name=group.name,
            lang=str(group.language.value).lower(),
            user_email=EMAIL_TEST,
        )
    ) is not None

    with TemporaryDirectory() as tempdir:
        pdf_maker = CertificateCreator(
            lang=str(group.language.value).lower(),
            doctype="cert",
            tempdir=tempdir,
            group=group.name,
            user=EMAIL_TEST,
        )
        await pdf_maker.cert(
            findings_ord,
            group.name,
            group.description,
            loaders,
        )
        assert pdf_maker.cert_context["remediation_rate"] == "50%"
        assert pdf_maker.cert_context["total_risk_exposure"] == Decimal("0")
        assert pdf_maker.cert_context["roots"] == {
            "https://gitlab.com/fluidattacks/nickname1 (master)": "2019-04-10"
        }
