import logging
import uuid
from typing import (
    TypedDict,
)

import aiofiles
import jinja2
from jinja2.utils import (
    select_autoescape,
)
from matplotlib.pyplot import (
    close,
    savefig,
    subplots,
    tight_layout,
    xticks,
)
from pandas import (
    DataFrame,
    to_datetime,
    to_numeric,
)

from integrates.analytics import domain as analytics_domain
from integrates.custom_utils.reports import filter_context
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.events.enums import (
    EventStateStatus,
)
from integrates.db_model.events.types import (
    Event,
    GroupEventsRequest,
)
from integrates.mailer.events import (
    EVENT_TYPE_FORMAT,
)
from integrates.reports.pdf import (
    CreatorPdf,
)
from integrates.reports.types import (
    Wordlist,
    WordlistItem,
)
from integrates.reports.utils import (
    call,
)
from integrates.settings.logger import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)


# Constants
LOGGER = logging.getLogger(__name__)


class UnresolvedEvents(TypedDict, total=False):
    id: str
    root: str
    date_report: str
    description: str
    type: str


class ExecutiveContext(TypedDict, total=False):
    full_group: str
    has_unresolved_events: bool
    resume_unresolved_events: str
    events_table: UnresolvedEvents
    events: list[UnresolvedEvents]
    risk_over_time_graphic: str


class PDFWordlistEn(Wordlist):
    EVENTS_TABLE_ID = WordlistItem("events_table_id", "ID")
    EVENTS_TABLE_ROOT = WordlistItem("events_table_root", "Root")
    EVENTS_TABLE_DATE_REPORT = WordlistItem("events_table_date_report", "Date report")
    EVENTS_TABLE_DESCRIPTION = WordlistItem("events_table_description", "Description")
    EVENTS_TABLE_TYPE = WordlistItem("events_table_type", "Type")
    RESUME_UNRESOLVED_EVENTS = WordlistItem("resume_unresolved_events", "Latest Unresolved Events")


class ExecutiveCreator(CreatorPdf):
    executive_context: ExecutiveContext
    word_list: dict[str, dict[str, str]] = {}

    def __init__(self, *, lang: str, tempdir: str, group: str, user: str) -> None:
        """Class constructor"""
        super().__init__(
            lang=lang,
            doctype="",
            tempdir=tempdir,
            group=group,
            user=user,
            style="certificate",
        )
        self.tpl_img_path = tempdir
        self.proj_tpl = "templates/pdf/executive_2.adoc"
        self.language_support()

    def language_support(self) -> None:
        self.word_list = {}
        self.language_support_en()

    def language_support_en(self) -> None:
        self.word_list["en"] = dict(zip(PDFWordlistEn.keys(), PDFWordlistEn.labels(), strict=False))

    def make_events_table(
        self, events: list[Event], roots: dict[str, str]
    ) -> list[UnresolvedEvents]:
        return [
            {
                "id": event.id,
                "root": roots.get(event.root_id, "") if event.root_id else "",
                "date_report": event.created_date.strftime("%Y-%m-%d"),
                "description": event.description,
                "type": EVENT_TYPE_FORMAT.get(event.type, ""),
            }
            for event in events
        ]

    async def make_risk_over_time_graphic(self, group_name: str) -> str:
        data = await analytics_domain.get_document(
            document_name="riskOverTime",
            document_type="stackedBarChart",
            entity="group",
            subject=group_name,
        )

        if not isinstance(data, dict):
            return "No Chart Generated"

        columns = data["data"]["columns"]
        colors = data["data"]["colors"]
        y_max = data["axis"]["y"]["max"]
        y_tick_count = data["axis"]["y"]["tick"]["count"] - 1

        column_names = [col[0] for col in columns]
        column_values = list(zip(*[col[1:] for col in columns], strict=False))
        dataframe = DataFrame(column_values, columns=column_names)

        if "date" in dataframe.columns:
            dataframe["date"] = to_datetime(dataframe["date"], format="%Y-%m-%d")

        for column in dataframe.columns:
            if column != "date":
                dataframe[column] = to_numeric(dataframe[column], errors="coerce")

        fig, ax = subplots(figsize=(12, 5))

        fig.patch.set_facecolor("#F5F5F5")

        ax.set_facecolor("#FFFFFF")

        for column in dataframe.columns:
            if column != "date":
                ax.plot(
                    dataframe["date"],
                    dataframe[column],
                    "-o",
                    color=colors[column],
                    label=column,
                    markersize=5,
                )

        gray_color = "#7D7D7D"

        ax.spines["bottom"].set_color(gray_color)
        ax.tick_params(axis="x", colors=gray_color)
        ax.tick_params(axis="y", colors=gray_color)

        ax.spines["top"].set_visible(False)
        ax.spines["left"].set_visible(False)
        ax.spines["right"].set_visible(False)

        ax.set_xlabel("", color=gray_color)
        ax.set_ylabel("CVSSF", color=gray_color)

        ax.grid(axis="y", color=gray_color, linestyle="--", linewidth=0.7)

        y_ticks = [i * (y_max / y_tick_count) for i in range(y_tick_count + 1)]
        ax.set_yticks(y_ticks)
        ax.set_xticks(dataframe["date"])
        ax.set_ylim([0, y_max])

        ax.set_title("Exposure management over time", color=gray_color)
        ax.legend(
            loc="lower center",
            bbox_to_anchor=(0.5, -0.8),
            ncol=3,
            frameon=False,
            markerscale=1,
            handletextpad=0.5,
            facecolor="white",
            edgecolor="none",
        )

        xticks(rotation=-45, color=gray_color)

        tight_layout()

        output_file = f"{self.tpl_img_path}/risk_over_time_{group_name}.png"

        savefig(output_file, bbox_inches="tight", transparent=True, dpi=100)

        close(fig)

        return output_file

    async def fill_context(self, loaders: Dataloaders) -> None:
        words = self.word_list["en"]
        events_group = self.make_events_table(
            [
                event
                for event in await loaders.group_events.load(
                    GroupEventsRequest(group_name=self.group_name)
                )
                if event.state.status is not EventStateStatus.SOLVED
            ],
            {
                root.id: root.state.nickname
                for root in await loaders.group_roots.load(self.group_name)
            },
        )
        risk_over_time_graphic = await self.make_risk_over_time_graphic(self.group_name)

        self.executive_context = {
            "full_group": self.group_name,
            "has_unresolved_events": bool(events_group),
            "resume_unresolved_events": words["resume_unresolved_events"],
            "risk_over_time_graphic": risk_over_time_graphic,
            "events": filter_context(events_group),
            "events_table": {
                "id": words["events_table_id"],
                "root": words["events_table_root"],
                "date_report": words["events_table_date_report"],
                "description": words["events_table_description"],
                "type": words["events_table_type"],
            },
        }

    async def executive(self, loaders: Dataloaders) -> None:
        await self.fill_context(loaders)
        self.out_name = f"{uuid.uuid4()!s}.pdf"
        searchpath = self.path
        template_loader = jinja2.FileSystemLoader(searchpath=searchpath)
        template_env = jinja2.Environment(
            loader=template_loader,
            autoescape=select_autoescape(["html", "xml"], default=True),
        )
        template = template_env.get_template(self.proj_tpl)
        tpl_name = f"{self.tpl_dir}{self.group_name}_EXECUTIVE_2.tpl"
        render_text = template.render(self.executive_context)
        async with aiofiles.open(tpl_name, "wb") as tplfile:
            await tplfile.write(render_text.encode("utf-8"))
        self.create_command(tpl_name, self.out_name)
        await call(
            program=self.command[0],
            command=self.command[1:],
            group_name=self.group_name,
        )
