import logging
from itertools import chain
from typing import TypedDict

import jinja2
from aioextensions import collect
from jinja2.utils import select_autoescape
from more_itertools import chunked

from integrates.custom_exceptions import InvalidStandardId
from integrates.custom_utils.criteria import CRITERIA_COMPLIANCE, CRITERIA_REQUIREMENTS
from integrates.custom_utils.datetime import get_as_str, get_now
from integrates.custom_utils.groups import get_group
from integrates.custom_utils.reports import filter_context
from integrates.dataloaders import Dataloaders
from integrates.db_model.findings.types import Finding
from integrates.db_model.groups.types import GroupUnreliableIndicators
from integrates.db_model.vulnerabilities.enums import VulnerabilityStateStatus
from integrates.organizations.utils import get_organization
from integrates.reports.pdf import CreatorPdf
from integrates.reports.types import UnfulfilledRequirementInfo, UnfulfilledStandardInfo
from integrates.reports.utils import call, get_base_url, get_findings_by_requirement
from integrates.settings.logger import LOGGING

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger(__name__)


class StandardReportContext(TypedDict):
    fluid_tpl: dict[str, str]
    group_name: str
    has_unfulfilled_standards: bool
    unfulfilled_standards_to_display: list[UnfulfilledStandardInfo]
    words: dict[str, str]


async def _get_findings_vulnerabilities(
    findings: list[Finding], loaders: Dataloaders
) -> dict[str, int]:
    _findings_vulns = chain.from_iterable(
        await collect(
            tuple(
                loaders.finding_vulnerabilities_released_nzr.load_many_chained(chunked_findings)
                for chunked_findings in chunked([finding.id for finding in findings], 16)
            ),
            workers=4,
        ),
    )
    findings_vulns = list(_findings_vulns)

    return {
        finding.id: len(
            [
                vuln
                for vuln in list(findings_vulns)
                if vuln.finding_id == finding.id
                and vuln.state.status == VulnerabilityStateStatus.VULNERABLE
            ]
        )
        for finding in findings
    }


class StandardReportCreator(CreatorPdf):
    """Class to generate standards report in PDF."""

    standard_report_context: StandardReportContext

    def __init__(self, *, lang: str, doctype: str, tempdir: str, group: str, user: str) -> None:
        """Class constructor"""
        super().__init__(lang=lang, doctype=doctype, tempdir=tempdir, group=group, user=user)
        self.proj_tpl = f"templates/pdf/unfulfilled_standards_{lang}.adoc"

    async def fill_context(
        self,
        group_name: str,
        lang: str,
        loaders: Dataloaders,
        selected_unfulfilled_standards: set[str] | None = None,
    ) -> None:
        """Fetch information and fill out the context."""
        group = await get_group(loaders, group_name)
        words = self.wordlist[lang]
        group_org = await get_organization(loaders, group.organization_id)
        group_findings = await loaders.group_findings.load(group_name)
        findings_vulns = await _get_findings_vulnerabilities(group_findings, loaders)
        fluid_tpl_content = await self.make_content(words)
        group_indicators: GroupUnreliableIndicators = (
            await loaders.group_unreliable_indicators.load(group_name)
        )

        unfulfilled_standards_to_display = sorted(
            [
                UnfulfilledStandardInfo(
                    standard_id=unfulfilled_standard.name,
                    title=str(CRITERIA_COMPLIANCE[unfulfilled_standard.name]["title"]).upper(),
                    summary=CRITERIA_COMPLIANCE[unfulfilled_standard.name][lang]["summary"],
                    unfulfilled_requirements=[
                        UnfulfilledRequirementInfo(
                            base_url=get_base_url(),
                            group_name=group_name,
                            findings=get_findings_by_requirement(
                                group_findings, findings_vulns, requirement_id
                            ),
                            id=requirement_id,
                            title=CRITERIA_REQUIREMENTS[requirement_id][lang]["title"],
                            org_name=group_org.name,
                            description=CRITERIA_REQUIREMENTS[requirement_id][lang]["description"],
                        )
                        for requirement_id in (unfulfilled_standard.unfulfilled_requirements)
                    ],
                )
                for unfulfilled_standard in (group_indicators.unfulfilled_standards) or []
            ],
            key=lambda standard: standard.title,
        )
        if selected_unfulfilled_standards is not None:
            if len(selected_unfulfilled_standards & CRITERIA_COMPLIANCE.keys()) != len(
                selected_unfulfilled_standards,
            ):
                raise InvalidStandardId()

            unfulfilled_standards_to_display = [
                unfulfilled_standard_info
                for unfulfilled_standard_info in (unfulfilled_standards_to_display)
                if unfulfilled_standard_info.standard_id in selected_unfulfilled_standards
            ]

        has_unfulfilled_standards = bool(group_indicators.unfulfilled_standards)
        self.standard_report_context = {
            "fluid_tpl": fluid_tpl_content,
            "group_name": group.name,
            "has_unfulfilled_standards": has_unfulfilled_standards,
            "unfulfilled_standards_to_display": (unfulfilled_standards_to_display),
            "words": words,
        }

    async def unfulfilled_standards(
        self,
        loaders: Dataloaders,
        group_name: str,
        lang: str,
        unfulfilled_standards: set[str] | None = None,
    ) -> None:
        """Create the template to render and apply the context."""
        await self.fill_context(group_name, lang, loaders, unfulfilled_standards)
        current_date = get_as_str(get_now(), date_format="%Y-%m-%dT%H-%M-%S")
        self.out_name = f"unfulfilled-standards-{group_name}-{current_date}.pdf"
        template_loader = jinja2.FileSystemLoader(searchpath=self.path)
        template_env = jinja2.Environment(
            loader=template_loader,
            autoescape=select_autoescape(["html", "xml"], default=True),
            enable_async=True,
        )
        template = template_env.get_template(self.proj_tpl)
        tpl_name = f"{self.tpl_dir}{group_name}_UN_STANDARDS.tpl"
        render_text = await template.render_async(filter_context(self.standard_report_context))
        with open(tpl_name, "wb") as tplfile:
            tplfile.write(render_text.encode("utf-8"))
        self.create_command(tpl_name, self.out_name)
        await call(
            program=self.command[0],
            command=self.command[1:],
            group_name=self.group_name,
        )
