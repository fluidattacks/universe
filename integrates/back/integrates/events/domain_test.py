import uuid
from datetime import datetime
from io import BytesIO

import aiofiles
from starlette.datastructures import Headers, UploadFile

import integrates.s3.operations
from integrates.custom_exceptions import (
    EventAlreadyClosed,
    EventNotFound,
    EventVerificationAlreadyRequested,
    EventVerificationNotRequested,
    InvalidParameter,
)
from integrates.custom_utils import datetime as datetime_utils
from integrates.custom_utils.constants import SCHEDULE_CLONE_GROUPS_ROOTS_EMAIL
from integrates.custom_utils.events import get_event
from integrates.custom_utils.roots import get_active_git_root
from integrates.dataloaders import get_new_context
from integrates.db_model.event_comments.types import EventCommentsRequest
from integrates.db_model.events.enums import (
    EventEvidenceId,
    EventSolutionReason,
    EventStateStatus,
    EventType,
)
from integrates.db_model.events.types import Event, EventEvidence, EventRequest
from integrates.events.domain import (
    add_cloning_issues_event,
    add_event,
    get_unsolved_events,
    reject_solution,
    remove_event,
    remove_evidence,
    request_verification,
    search_evidence,
    update_event,
    update_evidence,
)
from integrates.events.types import EventAttributesToUpdate
from integrates.s3.operations import file_exists
from integrates.testing.aws import (
    IntegratesAws,
    IntegratesDynamodb,
    IntegratesS3,
)
from integrates.testing.fakers import (
    DATE_2024,
    EMAIL_FLUIDATTACKS_ADMIN,
    EventEvidencesFaker,
    EventFaker,
    EventStateFaker,
    GitRootCloningFaker,
    GitRootFaker,
    GitRootStateFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import freeze_time, get_file_abs_path, parametrize, raises

TEST_DATE = datetime.fromisoformat("2024-12-10T12:59:52+00:00")
EMAIL_TEST = "hacker@fluidattacks.com"
DESCRIPTION_TEST = "description-event-test"
GROUP_NAME = "group1"
ROOT_ID = "root1"
EVENT_ID_RM = "57021045"
EVENT_ID = "57890045"
EVENT_ID_2 = "123456789"


async def read_file(file_name: str, headers: Headers) -> UploadFile:
    """Returns the uploaded file with the given file name"""
    async with aiofiles.open(file_name, "rb") as test_file:
        file_contents = await test_file.read()
        return UploadFile(
            filename=str(test_file.name),
            headers=headers,
            file=BytesIO(file_contents),
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="40f6da5f-4f66-4bf0-825b-a2d9748ad6db"),
            ],
            groups=[
                GroupFaker(
                    name="group1",
                    organization_id="40f6da5f-4f66-4bf0-825b-a2d9748ad6db",
                ),
            ],
            roots=[GitRootFaker(group_name="group1", id="9944225f-4f66-4bf0-825b-a2d9748ad6db")],
        ),
    )
)
async def test_add_event() -> None:
    # Arrange
    loaders = get_new_context()

    # Act

    # unsolved events of the same type could coexist if description is different.
    # Exception: for CLONING_ISSUES
    await add_event(
        loaders=loaders,
        hacker_email="hacker_email",
        group_name="group1",
        detail="Event description",
        event_date=datetime_utils.get_utc_now(),
        root_id="9944225f-4f66-4bf0-825b-a2d9748ad6db",
        event_type=EventType.ENVIRONMENT_ISSUES,
        environment_url="https://test.com",
    )

    await add_event(
        loaders=loaders,
        hacker_email="hacker_email",
        group_name="group1",
        detail="New Event description",
        event_date=datetime_utils.get_utc_now(),
        root_id="9944225f-4f66-4bf0-825b-a2d9748ad6db",
        event_type=EventType.ENVIRONMENT_ISSUES,
        environment_url="https://test.com",
    )

    # Assert
    events_after = await get_unsolved_events("group1")
    assert len(events_after) == 2


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="40f6da5f-4f66-4bf0-825b-a2d9748ad6db"),
            ],
            groups=[
                GroupFaker(
                    name="group1",
                    organization_id="40f6da5f-4f66-4bf0-825b-a2d9748ad6db",
                ),
            ],
            roots=[GitRootFaker(group_name="group1", id="9944225f-4f66-4bf0-825b-a2d9748ad6db")],
            events=[
                EventFaker(
                    id=str(uuid.uuid4()),
                    description="Event description",
                    created_by=SCHEDULE_CLONE_GROUPS_ROOTS_EMAIL,
                    group_name="group1",
                    root_id="9944225f-4f66-4bf0-825b-a2d9748ad6db",
                    type=EventType.CLONING_ISSUES,
                    state=EventStateFaker(status=EventStateStatus.CREATED),
                    environment_url=None,
                ),
            ],
        ),
    )
)
async def test_add_event_not_duplicated_cloning_issues_type() -> None:
    # Arrange
    loaders = get_new_context()

    # Act

    # unsolved events of the same type could coexist if description is different.
    # Exception: for CLONING_ISSUES
    await add_event(
        loaders=loaders,
        hacker_email="hacker_email",
        group_name="group1",
        detail="Event description 2",
        event_date=datetime_utils.get_utc_now(),
        root_id="9944225f-4f66-4bf0-825b-a2d9748ad6db",
        event_type=EventType.CLONING_ISSUES,
        environment_url="https://test.com",
    )

    # Assert
    events_after = await get_unsolved_events("group1")
    assert len(events_after) == 1


@parametrize(
    args=["failed_cloning_msg", "event_msg"],
    cases=[
        [
            "Could not find remote branch",
            "Could not find remote branch main to clone",
        ],
        [
            "Could not resolve host",
            "Could not find the repository or resolve the host",
        ],
        [
            "make sure you have the correct access rights",
            "Invalid credentials",
        ],
        [
            "Invalid credentials",
            "Invalid credentials",
        ],
        [
            "does not exist or you do not have permissions",
            "Repository does not exist or you do not have permissions",
        ],
        [
            "Cloning into '/tmp/integrates_clone_acme_3zsemsa2/915f43a1'..."
            "remote: You may not have access to this repository or it no longer exists "
            "in this workspace. If you think this repository exists and you have access, "
            "make sure you are authenticated."
            "fatal: repository 'https://bitbucket.org/acme/repo_test/' not found",
            "Repository does not exist or you do not have permissions",
        ],
        [
            "fatal: repository 'https://bitbucket.org/acme/repo_test/' not found",
            "Repository not found",
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="40f6da5f-4f66-4bf0-825b-a2d9748ad6db"),
            ],
            groups=[
                GroupFaker(
                    name=GROUP_NAME,
                    organization_id="40f6da5f-4f66-4bf0-825b-a2d9748ad6db",
                ),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    cloning=GitRootCloningFaker(failed_count=4),
                )
            ],
        ),
    )
)
async def test_add_cloning_issues_event_avoid_duplicate(
    failed_cloning_msg: str, event_msg: str
) -> None:
    loaders = get_new_context()
    root = await get_active_git_root(loaders, root_id=ROOT_ID, group_name=GROUP_NAME)
    await add_cloning_issues_event(
        loaders=loaders,
        git_root=root,
        message=failed_cloning_msg,
        branch="main",
    )
    await add_cloning_issues_event(
        loaders=loaders,
        git_root=root,
        message=failed_cloning_msg,
        branch="main",
    )

    events_after = await get_unsolved_events("group1")
    assert len(events_after) == 1
    event_cloning_issues: Event = events_after[0]
    assert event_cloning_issues.type == EventType.CLONING_ISSUES
    assert event_cloning_issues.description == event_msg


@parametrize(
    args=["failed_cloning_msg"],
    cases=[
        ["Unknown message"],
        ["Failed to clone without message"],
        ["unexpected disconnect while reading sideband packet"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="40f6da5f-4f66-4bf0-825b-a2d9748ad6db"),
            ],
            groups=[
                GroupFaker(
                    name=GROUP_NAME,
                    organization_id="40f6da5f-4f66-4bf0-825b-a2d9748ad6db",
                ),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    cloning=GitRootCloningFaker(failed_count=4),
                )
            ],
        ),
    )
)
async def test_add_cloning_issues_msg_not_covered(failed_cloning_msg: str) -> None:
    loaders = get_new_context()
    root = await get_active_git_root(loaders, root_id=ROOT_ID, group_name=GROUP_NAME)
    await add_cloning_issues_event(
        loaders=loaders,
        git_root=root,
        message=failed_cloning_msg,
        branch="main",
    )

    events_after = await get_unsolved_events("group1")
    assert len(events_after) == 0


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(
                    name="group1",
                    organization_id="org1",
                ),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    state=GitRootStateFaker(url="https://gitlab.com/url1"),
                ),
                GitRootFaker(
                    group_name="group1",
                    id="root2",
                    state=GitRootStateFaker(url="https://gitlab.com/url2"),
                ),
                GitRootFaker(
                    group_name="group1",
                    id="root3",
                    state=GitRootStateFaker(url="https://gitlab.com/url3"),
                ),
            ],
            events=[
                EventFaker(
                    id=str(uuid.uuid4()),
                    description="Event description",
                    created_by=SCHEDULE_CLONE_GROUPS_ROOTS_EMAIL,
                    group_name="group1",
                    root_id="root1",
                    type=EventType.ENVIRONMENT_ISSUES,
                    state=EventStateFaker(status=EventStateStatus.CREATED),
                    environment_url=None,
                ),
                EventFaker(
                    id=str(uuid.uuid4()),
                    description="Event description",
                    created_by=SCHEDULE_CLONE_GROUPS_ROOTS_EMAIL,
                    group_name="group1",
                    root_id="root2",
                    type=EventType.ENVIRONMENT_ISSUES,
                    state=EventStateFaker(status=EventStateStatus.OPEN),
                    environment_url=None,
                ),
                EventFaker(
                    id=str(uuid.uuid4()),
                    description="Event description",
                    created_by=SCHEDULE_CLONE_GROUPS_ROOTS_EMAIL,
                    group_name="group1",
                    root_id="root3",
                    type=EventType.ENVIRONMENT_ISSUES,
                    state=EventStateFaker(status=EventStateStatus.VERIFICATION_REQUESTED),
                    environment_url=None,
                ),
            ],
        ),
    )
)
async def test_already_existing_events() -> None:
    events_before = await get_unsolved_events("group1")
    event_root_1 = next(event for event in events_before if event.root_id == "root1")
    event_root_2 = next(event for event in events_before if event.root_id == "root2")
    event_root_3 = next(event for event in events_before if event.root_id == "root3")
    loaders = get_new_context()

    existing_event_root1 = await add_event(
        loaders=loaders,
        hacker_email=SCHEDULE_CLONE_GROUPS_ROOTS_EMAIL,
        group_name="group1",
        detail="Event description",
        event_date=datetime_utils.get_utc_now(),
        root_id="root1",
        event_type=EventType.ENVIRONMENT_ISSUES,
        environment_url=None,
    )

    existing_event_root2 = await add_event(
        loaders=loaders,
        hacker_email=SCHEDULE_CLONE_GROUPS_ROOTS_EMAIL,
        group_name="group1",
        detail="Event description",
        event_date=datetime_utils.get_utc_now(),
        root_id="root2",
        event_type=EventType.ENVIRONMENT_ISSUES,
        environment_url=None,
    )

    existing_event_root3 = await add_event(
        loaders=loaders,
        hacker_email=SCHEDULE_CLONE_GROUPS_ROOTS_EMAIL,
        group_name="group1",
        detail="Event description",
        event_date=datetime_utils.get_utc_now(),
        root_id="root3",
        event_type=EventType.ENVIRONMENT_ISSUES,
        environment_url=None,
    )

    events_after = await get_unsolved_events("group1")

    assert len(events_before) == len(events_after)
    assert existing_event_root1 == event_root_1.id
    assert existing_event_root2 == event_root_2.id
    assert existing_event_root3 == event_root_3.id
    assert (
        next(event for event in events_after if event.id == existing_event_root3).state.status
        == EventStateStatus.CREATED
    )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="hacker")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL_TEST,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME, id=ROOT_ID, state=GitRootStateFaker(nickname="back")
                ),
            ],
            events=[
                EventFaker(
                    group_name=GROUP_NAME,
                    id=EVENT_ID,
                    evidences=EventEvidencesFaker(
                        image_1=EventEvidence(
                            file_name="group1-57890045-evidence_image_1.png",
                            modified_date=TEST_DATE,
                        )
                    ),
                    state=EventStateFaker(status=EventStateStatus.CREATED),
                )
            ],
        ),
        s3=IntegratesS3(autoload=True),
    ),
    others=[Mock(integrates.s3.operations, "FI_AWS_S3_PATH_PREFIX", "function", "")],
)
async def test_remove_event_evidence() -> None:
    loaders = get_new_context()
    event = await loaders.event.load(EventRequest(event_id=EVENT_ID, group_name=GROUP_NAME))
    assert event
    assert event.evidences.image_1
    assert event.evidences.image_1.file_name == "group1-57890045-evidence_image_1.png"

    evidence_name = f"evidences/{event.group_name}/{event.id}/{event.evidences.image_1.file_name}"
    assert await file_exists(evidence_name, "integrates.dev")

    await remove_evidence(
        loaders=loaders,
        evidence_id=EventEvidenceId.IMAGE_1,
        event_id=EVENT_ID,
        group_name=GROUP_NAME,
    )

    event_after = await get_new_context().event.load(
        EventRequest(event_id=EVENT_ID, group_name=GROUP_NAME)
    )
    assert event_after
    assert event_after.evidences.image_1 is None
    assert not await file_exists(evidence_name, "integrates.dev")


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME, id=ROOT_ID, state=GitRootStateFaker(nickname="back")
                ),
            ],
            events=[
                EventFaker(
                    group_name=GROUP_NAME,
                    id=EVENT_ID,
                    state=EventStateFaker(status=EventStateStatus.CREATED),
                )
            ],
        )
    ),
)
async def test_request_event_verification() -> None:
    loaders = get_new_context()
    event = await loaders.event.load(EventRequest(event_id=EVENT_ID, group_name=GROUP_NAME))
    assert event

    event_comments = await loaders.event_comments.load(
        EventCommentsRequest(event_id=EVENT_ID, group_name=GROUP_NAME),
    )
    assert len(event_comments) == 0

    await request_verification(
        loaders=loaders,
        event_id=EVENT_ID,
        comments="Environment was updated",
        group_name=GROUP_NAME,
        stakeholder_email="jdoe@clientapp.com",
        stakeholder_full_name="Jhon Doe",
    )

    new_loaders = get_new_context()
    event_after = await new_loaders.event.load(
        EventRequest(event_id=EVENT_ID, group_name=GROUP_NAME)
    )
    assert event_after

    event_comments_after = await new_loaders.event_comments.load(
        EventCommentsRequest(event_id=EVENT_ID, group_name=GROUP_NAME),
    )
    assert len(event_comments_after) == 1
    assert event_comments_after[0].content == "Environment was updated"
    assert event_after.state.comment_id == event_comments_after[0].id


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME, id=ROOT_ID, state=GitRootStateFaker(nickname="back")
                ),
            ],
            events=[
                EventFaker(
                    group_name=GROUP_NAME,
                    id=EVENT_ID,
                    state=EventStateFaker(status=EventStateStatus.VERIFICATION_REQUESTED),
                ),
                EventFaker(
                    group_name=GROUP_NAME,
                    id=EVENT_ID_2,
                    state=EventStateFaker(status=EventStateStatus.SOLVED),
                ),
            ],
        )
    ),
)
async def test_request_event_verification_failed() -> None:
    loaders = get_new_context()

    with raises(EventVerificationAlreadyRequested):
        await request_verification(
            loaders=loaders,
            event_id=EVENT_ID,
            comments="Environment was updated",
            group_name=GROUP_NAME,
            stakeholder_email="jdoe@clientapp.com",
            stakeholder_full_name="Jhon Doe",
        )

    with raises(EventAlreadyClosed):
        await request_verification(
            loaders=loaders,
            event_id=EVENT_ID_2,
            comments="Environment was updated",
            group_name=GROUP_NAME,
            stakeholder_email="jdoe@clientapp.com",
            stakeholder_full_name="Jhon Doe",
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="hacker")],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL_TEST,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME, id=ROOT_ID, state=GitRootStateFaker(nickname="back")
                ),
            ],
            events=[
                EventFaker(
                    group_name=GROUP_NAME,
                    id=EVENT_ID_RM,
                    state=EventStateFaker(status=EventStateStatus.CREATED),
                    type=EventType.ENVIRONMENT_ISSUES,
                ),
            ],
        ),
        s3=IntegratesS3(autoload=True),
    ),
)
async def test_remove_event() -> None:
    # Act
    loaders = get_new_context()
    await remove_event(event_id=EVENT_ID_RM, group_name=GROUP_NAME)
    loaders.event.clear_all()

    # Assert
    with raises(EventNotFound):
        await get_event(loaders, EventRequest(event_id=EVENT_ID_RM, group_name=GROUP_NAME))
    assert await search_evidence(f"{GROUP_NAME}/{EVENT_ID}") == []


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="hacker")],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL_TEST,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME, id=ROOT_ID, state=GitRootStateFaker(nickname="back")
                ),
            ],
            events=[
                EventFaker(
                    group_name=GROUP_NAME,
                    id=EVENT_ID,
                    state=EventStateFaker(status=EventStateStatus.CREATED),
                    type=EventType.ENVIRONMENT_ISSUES,
                ),
                EventFaker(
                    group_name=GROUP_NAME,
                    id=EVENT_ID_2,
                    state=EventStateFaker(status=EventStateStatus.SOLVED),
                    type=EventType.DATA_UPDATE_REQUIRED,
                ),
            ],
        )
    ),
)
async def test_update_event() -> None:
    # Arrange
    loaders = get_new_context()

    # Act
    await update_event(
        loaders=loaders,
        event_id=EVENT_ID,
        group_name=GROUP_NAME,
        stakeholder_email=EMAIL_TEST,
        attributes=EventAttributesToUpdate(
            event_description=DESCRIPTION_TEST,
            event_type=EventType.DATA_UPDATE_REQUIRED,
        ),
    )
    await update_event(
        loaders=loaders,
        event_id=EVENT_ID_2,
        group_name=GROUP_NAME,
        stakeholder_email=EMAIL_TEST,
        attributes=EventAttributesToUpdate(
            other_solving_reason="This is a test",
            solving_reason=EventSolutionReason.OTHER,
        ),
    )
    loaders.event.clear_all()
    event = await get_event(loaders, EventRequest(event_id=EVENT_ID, group_name=GROUP_NAME))
    event2 = await get_event(loaders, EventRequest(event_id=EVENT_ID_2, group_name=GROUP_NAME))

    # Assert
    assert event
    assert event2
    assert event.description == DESCRIPTION_TEST
    assert event.type == EventType.DATA_UPDATE_REQUIRED
    assert event.state.status == EventStateStatus.CREATED
    assert event2.state.reason == EventSolutionReason.OTHER
    assert event2.state.other == "This is a test"
    assert event2.state.status == EventStateStatus.SOLVED


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="hacker")],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL_TEST,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME, id=ROOT_ID, state=GitRootStateFaker(nickname="back")
                ),
            ],
            events=[
                EventFaker(
                    group_name=GROUP_NAME,
                    id=EVENT_ID,
                    state=EventStateFaker(status=EventStateStatus.CREATED),
                    type=EventType.ENVIRONMENT_ISSUES,
                ),
            ],
        )
    ),
)
async def test_update_event_fail() -> None:
    # Assert
    with raises(InvalidParameter):
        await update_event(
            loaders=get_new_context(),
            event_id=EVENT_ID,
            group_name=GROUP_NAME,
            stakeholder_email=EMAIL_TEST,
            attributes=EventAttributesToUpdate(
                event_description=DESCRIPTION_TEST,
                event_type=EventType.INCORRECT_MISSING_SUPPLIES,
            ),
        )


@freeze_time("2024-04-10T12:59:52+00:00")
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="hacker")],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL_TEST,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME, id=ROOT_ID, state=GitRootStateFaker(nickname="back")
                ),
            ],
            events=[
                EventFaker(
                    group_name=GROUP_NAME,
                    id=EVENT_ID,
                    state=EventStateFaker(status=EventStateStatus.CREATED),
                    type=EventType.ENVIRONMENT_ISSUES,
                ),
            ],
        ),
        s3=IntegratesS3(autoload=True),
    ),
)
async def test_update_evidence() -> None:
    # Asser
    loaders = get_new_context()
    await update_evidence(
        loaders=get_new_context(),
        event_id=EVENT_ID,
        evidence_id=EventEvidenceId.FILE_1,
        file=await read_file(
            file_name=get_file_abs_path("test.txt"),
            headers=Headers(headers={"content_type": "text/plain"}),
        ),
        group_name=GROUP_NAME,
        update_date=DATE_2024,
    )
    event = await get_event(loaders, EventRequest(event_id=EVENT_ID, group_name=GROUP_NAME))

    # Assert
    assert event
    assert event.id == EVENT_ID
    assert event.evidences
    assert event.evidences.file_1
    assert event.evidences.file_1.file_name == "group1_57890045_evidence_file_1.txt"
    assert event.evidences.file_1.modified_date == DATE_2024


@parametrize(
    args=["email", "event_id", "comments"],
    cases=[[EMAIL_FLUIDATTACKS_ADMIN, "418900971", "comment test"]],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(organization_id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_ADMIN, role="admin"),
            ],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_ADMIN,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
            events=[
                EventFaker(
                    id="418900971",
                    state=EventStateFaker(status=EventStateStatus.VERIFICATION_REQUESTED),
                )
            ],
        )
    ),
)
async def test_reject_solution(
    email: str,
    event_id: str,
    comments: str,
) -> None:
    await reject_solution(
        loaders=get_new_context(),
        event_id=event_id,
        comments=comments,
        group_name="test-group",
        stakeholder_email=email,
        stakeholder_full_name=email.split("@")[0],
    )

    loaders = get_new_context()
    event = await loaders.event.load(EventRequest(event_id=event_id, group_name="test-group"))
    assert event
    assert event.state.status == EventStateStatus.CREATED
    event_comments = await loaders.event_comments.load(
        EventCommentsRequest(event_id=event.id, group_name=event.group_name)
    )
    assert event.state.comment_id in {comment.id for comment in event_comments}


@parametrize(
    args=["email", "event_id", "comments"],
    cases=[
        [EMAIL_FLUIDATTACKS_ADMIN, "418900971", "comment test"],
        [EMAIL_FLUIDATTACKS_ADMIN, "418900972", "comment test"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(organization_id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_ADMIN, role="admin"),
            ],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_ADMIN,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
            events=[
                EventFaker(
                    id="418900971",
                    state=EventStateFaker(status=EventStateStatus.SOLVED),
                ),
                EventFaker(
                    id="418900972",
                    state=EventStateFaker(status=EventStateStatus.CREATED),
                ),
            ],
        )
    ),
)
async def test_reject_event_solution_non_requested(
    email: str,
    event_id: str,
    comments: str,
) -> None:
    with raises(
        EventVerificationNotRequested,
        match="Exception - The event verification has not been requested",
    ):
        await reject_solution(
            loaders=get_new_context(),
            event_id=event_id,
            comments=comments,
            group_name="test-group",
            stakeholder_email=email,
            stakeholder_full_name=email.split("@")[0],
        )
