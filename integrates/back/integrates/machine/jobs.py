import json
import logging
import logging.config
import os
from collections.abc import Iterable
from subprocess import SubprocessError
from typing import Any, NamedTuple

import fluidattacks_core.git as git_utils
from more_itertools import chunked

from integrates.batch.dal.get import get_action, get_actions_by_name
from integrates.batch.dal.put import put_action
from integrates.batch.dal.update import update_action_to_dynamodb
from integrates.batch.enums import Action, SkimsBatchQueue
from integrates.batch.types import BatchProcessingToUpdate, DependentAction, PutActionResult
from integrates.custom_utils.roots import get_active_git_roots
from integrates.custom_utils.vulnerabilities import get_path_from_integrates_vulnerability
from integrates.dataloaders import Dataloaders
from integrates.db_model.groups.enums import GroupManaged
from integrates.db_model.roots.types import GitRoot
from integrates.db_model.vulnerabilities.enums import VulnerabilityTechnique, VulnerabilityType
from integrates.db_model.vulnerabilities.types import Vulnerability
from integrates.decorators import retry_on_exceptions
from integrates.jobs_orchestration.create_machine_config import (
    MachineJobOrigin,
    MachineJobTechnique,
    MachineModulesToExecute,
    generate_and_upload_s3_job_configs_sast,
    generate_batch_action_config,
    generate_machine_job_name,
)
from integrates.roots import domain as roots_domain
from integrates.settings.logger import LOGGING


def _json_load(path: str) -> Any:
    with open(path, encoding="utf-8") as file:
        return json.load(file)


logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger(__name__)
FINDINGS: dict[str, dict[str, dict[str, str]]] = _json_load(os.environ["MACHINE_FINDINGS"])
# Assume ~5min/root as average skims efficiency
MAX_ROOTS_PER_JOB = 100
# Aimed to not surpass the dynamodb item max size
MAX_MODIFIED_FILENAMES_PER_JOB = 250


class PostCloningConfig(NamedTuple):
    root: GitRoot
    machine_config_file: str
    sbom_config_file: str


git_get_modified_filenames = retry_on_exceptions(
    exceptions=(
        FileNotFoundError,
        IndexError,
        OSError,
        SubprocessError,
        ValueError,
    ),
)(git_utils.get_modified_filenames)

git_is_commit_in_branch = retry_on_exceptions(
    exceptions=(
        FileNotFoundError,
        IndexError,
        OSError,
        SubprocessError,
        ValueError,
    ),
)(git_utils.is_commit_in_branch)


def get_finding_code_from_title(finding_title: str) -> str | None:
    if (fin_code := f"F{finding_title[:3]}") and fin_code in FINDINGS:
        return fin_code
    return None


async def validate_group_for_execution(loaders: Dataloaders, group_name: str) -> bool:
    group = await loaders.group.load(group_name)

    return (
        group is not None
        and group.state.has_essential
        and group.state.managed != GroupManaged.UNDER_REVIEW
    )


async def has_pending_executions(
    group_name: str,
    root_nicknames: set[str],
) -> bool:
    pending_executions = [
        action
        for action in (
            await get_actions_by_name(
                action_name=Action.EXECUTE_MACHINE,
                entity=group_name,
            )
        )
        if not action.running
    ]

    pending_sast_executions = [
        action
        for action in (
            await get_actions_by_name(
                action_name=Action.EXECUTE_MACHINE_SAST,
                entity=group_name,
            )
        )
        if not action.running
    ]

    return any(
        action
        for action in pending_executions + pending_sast_executions
        for root_nickname in root_nicknames
        if root_nickname in action.additional_info["roots"]
    )


def get_techniques_to_execute(
    vulns: Iterable[Vulnerability],
) -> MachineModulesToExecute:
    is_sast_sca_included = False
    is_apk_included = False
    is_dast_included = False
    is_cspm_included = False

    if any(vuln for vuln in vulns if vuln.type == VulnerabilityType.LINES):
        is_sast_sca_included = True

    input_vulns = [vuln for vuln in vulns if vuln.type == VulnerabilityType.INPUTS]
    for vuln in input_vulns:
        if get_path_from_integrates_vulnerability(vuln.state.where, vuln.type, True)[1].endswith(
            ".apk",
        ):
            is_apk_included = True
        elif vuln.technique == VulnerabilityTechnique.DAST:
            is_dast_included = True
        else:
            is_cspm_included = True

    return MachineModulesToExecute(
        APK=is_apk_included,
        CSPM=is_cspm_included,
        DAST=is_dast_included,
        SAST=is_sast_sca_included,
        SCA=is_sast_sca_included,
    )


async def put_machine_action(
    *,
    loaders: Dataloaders,
    group_name: str,
    modified_by: str,
    modules_to_execute: MachineModulesToExecute,
    root_nicknames: list[str],
    checks: list[str] | None = None,
    roots_included_paths: dict[str, list[str]] | None = None,
    job_origin: MachineJobOrigin = MachineJobOrigin.SCHEDULER,
    job_technique: MachineJobTechnique = MachineJobTechnique.ALL,
) -> PutActionResult:
    if modules_to_execute.SAST:
        (
            additional_info,
            job_name,
            batch_queue,
        ) = await generate_batch_action_config(
            loaders=loaders,
            group_name=group_name,
            root_nicknames=root_nicknames,
            checks=checks,
            roots_included_paths=roots_included_paths,
            modules_to_execute=modules_to_execute,
            job_origin_and_technique=(job_origin, job_technique),
            include_sbom=True,
        )
        action_to_execute = Action.EXECUTE_MACHINE_SAST
    else:
        (
            additional_info,
            job_name,
            batch_queue,
        ) = await generate_batch_action_config(
            loaders=loaders,
            group_name=group_name,
            root_nicknames=root_nicknames,
            checks=checks,
            roots_included_paths=roots_included_paths,
            modules_to_execute=modules_to_execute,
            job_origin_and_technique=(job_origin, job_technique),
        )
        action_to_execute = Action.EXECUTE_MACHINE

    return await put_action(
        action=action_to_execute,
        additional_info=additional_info,
        attempt_duration_seconds=43200,
        entity=group_name,
        queue=batch_queue,
        subject=modified_by,
        job_name=job_name,
    )


async def queue_machine_reattack(
    *,
    loaders: Dataloaders,
    finding_codes: list[str],
    group_name: str,
    modified_by: str,
    modules_to_execute: MachineModulesToExecute,
    reported_paths_by_root: dict[str, list[str]],
    clone_before: bool = True,
) -> PutActionResult | None:
    if (
        not await validate_group_for_execution(loaders, group_name)
        or not finding_codes
        or not reported_paths_by_root
    ):
        return None

    root_nicknames = list(reported_paths_by_root.keys())

    if modules_to_execute.SAST:
        (
            additional_info,
            job_name,
            batch_queue,
        ) = await generate_batch_action_config(
            loaders=loaders,
            group_name=group_name,
            root_nicknames=root_nicknames,
            checks=finding_codes,
            roots_included_paths=reported_paths_by_root,
            modules_to_execute=modules_to_execute,
            job_origin_and_technique=(
                MachineJobOrigin.REATTACK,
                MachineJobTechnique.ALL,
            ),
            include_sbom=True,
        )
        action_to_execute = Action.EXECUTE_MACHINE_SAST
    else:
        (
            additional_info,
            job_name,
            batch_queue,
        ) = await generate_batch_action_config(
            loaders=loaders,
            group_name=group_name,
            root_nicknames=root_nicknames,
            checks=finding_codes,
            roots_included_paths=reported_paths_by_root,
            modules_to_execute=modules_to_execute,
            job_origin_and_technique=(
                MachineJobOrigin.REATTACK,
                MachineJobTechnique.ALL,
            ),
        )
        action_to_execute = Action.EXECUTE_MACHINE

    if clone_before:
        queued_sync = await roots_domain.queue_sync_git_roots(
            loaders=loaders,
            group_name=group_name,
            roots=[
                root
                for root in await get_active_git_roots(loaders, group_name)
                if root.state.nickname in root_nicknames
            ],
            modified_by=modified_by,
            force=True,
            check_existing_jobs=False,
            should_queue_machine=False,
            should_queue_sbom=False,
            queue_on_batch=True,
        )
        if (
            queued_sync
            and queued_sync.batch_job_id
            and queued_sync.dynamo_pk
            and (clone_action := await get_action(action_key=queued_sync.dynamo_pk))
            and await update_action_to_dynamodb(
                action=Action.CLONE_ROOTS,
                action_key=queued_sync.dynamo_pk,
                attributes=BatchProcessingToUpdate(
                    dependent_actions=[
                        *(clone_action.dependent_actions or []),
                        DependentAction(
                            action_name=action_to_execute,
                            additional_info=additional_info,
                            queue=batch_queue,
                            attempt_duration_seconds=43200,
                            job_name=job_name,
                        ),
                    ],
                ),
            )
        ):
            return PutActionResult(
                success=True,
                batch_job_id=queued_sync.batch_job_id,
                dynamo_pk=queued_sync.dynamo_pk,
            )

    return await put_action(
        action=action_to_execute,
        additional_info=additional_info,
        attempt_duration_seconds=43200,
        entity=group_name,
        queue=batch_queue,
        subject=modified_by,
        job_name=job_name,
    )


async def generate_post_cloning_config(
    *,
    loaders: Dataloaders,
    root: GitRoot,
    repo_working_dir: str,
    config_uuid: str,
) -> PostCloningConfig | None:
    included_paths: dict[str, list[str]] | None = None
    if (
        root.machine
        and root.machine.commit
        and await git_is_commit_in_branch(repo_working_dir, root.state.branch, root.machine.commit)
        and (
            modified_filenames := await git_get_modified_filenames(
                repo_working_dir,
                root.machine.commit,
            )
        )
        and len(modified_filenames) <= MAX_MODIFIED_FILENAMES_PER_JOB
    ):
        included_paths = {root.state.nickname: modified_filenames}

    configs_files_sbom = await generate_and_upload_s3_job_configs_sast(
        loaders=loaders,
        batch_job_id=config_uuid,
        group_name=root.group_name,
        root_nicknames=[root.state.nickname],
        root_paths=included_paths,
        modules_to_execute=MachineModulesToExecute(
            APK=False,
            CSPM=False,
            DAST=False,
            SAST=True,
            SCA=True,
        ),
    )
    if not configs_files_sbom:
        return None

    _, machine_config_file, sbom_config_file = configs_files_sbom[0]

    return PostCloningConfig(root, machine_config_file, sbom_config_file)


async def queue_post_cloning_action(
    *,
    group_name: str,
    modified_by: str,
    generated_config_files: tuple[PostCloningConfig, ...],
    config_uuid: str,
) -> PutActionResult:
    if len(generated_config_files) == 0:
        return PutActionResult(success=False)

    additional_info: dict[str, list[str]] = {
        "roots": [],
        "roots_config_files": [],
        "roots_sbom_config_files": [],
    }
    for config_file in generated_config_files:
        additional_info["roots"].append(config_file.root.state.nickname)
        additional_info["roots_config_files"].append(config_file.machine_config_file)
        additional_info["roots_sbom_config_files"].append(config_file.sbom_config_file)

    return await put_action(
        action=Action.EXECUTE_MACHINE_SAST,
        additional_info=additional_info,
        attempt_duration_seconds=43200,
        entity=group_name,
        queue=SkimsBatchQueue.SMALL,
        subject=modified_by,
        job_name=generate_machine_job_name(
            group_name,
            MachineJobOrigin.POSTCLONE.value,
            MachineJobTechnique.SAST.value,
            config_uuid,
        ),
    )


async def queue_machine_job(
    *,
    loaders: Dataloaders,
    group_name: str,
    modified_by: str,
    root_nicknames: set[str],
    modules_to_execute: MachineModulesToExecute,
    checks: list[str] | None = None,
    job_origin: MachineJobOrigin = MachineJobOrigin.SCHEDULER,
    job_technique: MachineJobTechnique = MachineJobTechnique.ALL,
) -> PutActionResult | None:
    if not await validate_group_for_execution(loaders, group_name) or not root_nicknames:
        return None

    roots_to_queue = sorted(root_nicknames)
    for roots_chunk in chunked(roots_to_queue, MAX_ROOTS_PER_JOB):
        queued_execution = await put_machine_action(
            checks=checks,
            loaders=loaders,
            group_name=group_name,
            modified_by=modified_by,
            modules_to_execute=modules_to_execute,
            root_nicknames=roots_chunk,
            job_origin=job_origin,
            job_technique=job_technique,
        )

    return queued_execution
