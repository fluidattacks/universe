from datetime import (
    date,
    datetime,
)

from integrates import (
    authz,
)
from integrates.context import (
    BASE_URL,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.mailer.utils import (
    get_group_emails_by_notification,
    get_organization_name,
)

from .common import (
    send_mails_async,
)
from .types import (
    AssignedVulnerabilityContext,
    ReviewerProgressReportContext,
    TreatmentReportContext,
    UpdatedTreatmentContext,
)


async def send_mail_updated_treatment(
    *,
    loaders: Dataloaders,
    assigned: str,
    finding_id: str,
    finding_title: str,
    group_name: str,
    justification: str,
    treatment: str,
    vulnerabilities: str,
    modified_by: str,
) -> None:
    org_name = await get_organization_name(loaders, group_name)
    stakeholders_email = await get_group_emails_by_notification(
        loaders=loaders,
        group_name=group_name,
        notification="updated_treatment",
    )
    stakeholder_role = await authz.get_group_level_role(loaders, modified_by, group_name)
    email_context: UpdatedTreatmentContext = {
        "assigned": assigned,
        "group": group_name,
        "justification": justification,
        "responsible": modified_by,
        "treatment": treatment,
        "finding": finding_title,
        "user_role": stakeholder_role.replace("_", " "),
        "vulnerabilities": vulnerabilities.splitlines(),
        "finding_link": (f"{BASE_URL}/orgs/{org_name}/groups/{group_name}/vulns/{finding_id}"),
    }
    await send_mails_async(
        loaders=loaders,
        email_to=stakeholders_email,
        context=dict(email_context),
        subject=(
            "A vulnerability treatment has changed to "
            f'{email_context["treatment"]} in [{email_context["group"]}]'
        ),
        template_name="updated_treatment",
    )


async def _get_email_context(
    *,
    loaders: Dataloaders,
    group_name: str,
    modified_by: str | None,
    modified_date: datetime,
    justification: str | None,
    finding_title: str,
    is_approved: bool,
    location: str,
    managers_email: list[str],
    approve_state: str,
    treatment_status: str,
    finding_id: str,
) -> TreatmentReportContext:
    org_name = await get_organization_name(loaders, group_name)
    user_email: str = modified_by if modified_by else ""
    user_role = await authz.get_group_level_role(loaders, user_email, group_name)
    return {
        "date": str(modified_date.date()),
        "group": group_name,
        "responsible": modified_by,
        "justification": justification,
        "finding": finding_title,
        "is_approved": is_approved,
        "vulnerabilities": location.splitlines(),
        "managers_email": managers_email,
        "approve_state": approve_state,
        "treatment_status": treatment_status,
        "user_role": user_role.replace("_", " "),
        "finding_link": (f"{BASE_URL}/orgs/{org_name}/groups/{group_name}/vulns/{finding_id}"),
    }


async def send_mail_treatment_report(
    *,
    loaders: Dataloaders,
    finding_id: str,
    finding_title: str,
    group_name: str,
    justification: str | None,
    managers_email: list[str],
    modified_by: str | None,
    modified_date: datetime,
    location: str,
    email_to: list[str],
    is_approved: bool,
    is_permanent: bool,
) -> None:
    approve_state: str = "has been approved" if is_approved else "has been requested"
    treatment_status: str = "permanent" if is_permanent else "temporary"
    email_context = await _get_email_context(
        loaders=loaders,
        group_name=group_name,
        modified_by=modified_by,
        modified_date=modified_date,
        justification=justification,
        finding_title=finding_title,
        is_approved=is_approved,
        location=location,
        managers_email=managers_email,
        approve_state=approve_state,
        treatment_status=treatment_status,
        finding_id=finding_id,
    )
    await send_mails_async(
        loaders=loaders,
        email_to=email_to,
        context=dict(email_context),
        subject=f"A {treatment_status} acceptance " + f"{approve_state} in [{group_name}]",
        template_name="treatment_report",
    )


async def send_mail_assigned_vulnerability(
    *,
    loaders: Dataloaders,
    email_to: list[str],
    is_finding_released: bool,
    group_name: str = "",
    finding_title: str,
    finding_id: str,
    responsible: str,
    where: str,
) -> None:
    org_name = await get_organization_name(loaders, group_name)

    email_context: AssignedVulnerabilityContext = {
        "finding_title": finding_title,
        "group": group_name,
        "finding_url": (
            f"{BASE_URL}/orgs/{org_name}/groups/{group_name}/"
            f'{"vulns" if is_finding_released else "drafts"}/{finding_id}/'
            "locations"
        ),
        "responsible": responsible,
        "where": where.splitlines(),
    }
    await send_mails_async(
        loaders=loaders,
        email_to=email_to,
        context=dict(email_context),
        subject=(f"Newly assigned vulnerability in [{finding_title}] for [{group_name}]"),
        template_name="vulnerability_assigned",
    )


async def send_mail_reviewer_progress_report(
    *,
    loaders: Dataloaders,
    context: ReviewerProgressReportContext,
    email_to: list[str],
    email_cc: list[str],
    report_date: date,
    responsible: str,
) -> None:
    user_login = str(responsible).split("@", maxsplit=1)[0]
    await send_mails_async(
        loaders=loaders,
        email_to=email_to,
        email_cc=email_cc,
        subject=f"Reviewer progress report {user_login} [{report_date}]",
        context=dict(context),
        template_name="reviewer_progress_report",
    )
