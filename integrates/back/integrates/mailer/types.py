from datetime import (
    date,
    datetime,
    timedelta,
)
from decimal import (
    Decimal,
)
from typing import Any, NotRequired

from typing_extensions import NamedTuple, TypedDict

from integrates.custom_utils.datetime import (
    ReportDates,
)


class TrialEngagementInfo(NamedTuple):
    email_to: str
    group_name: str
    start_date: datetime


# Numerator report typing
class QuintileFieldsData(TypedDict):
    verified: int
    total: int
    percentage: int | str


class QuintileFields(TypedDict):
    files: QuintileFieldsData
    lines: QuintileFieldsData


class QuintileGroupFields(TypedDict):
    files_perc: int | str
    files_total: int | str
    lines_perc: int | str
    lines_total: int | str


class QuintileRange(NamedTuple):
    start: int
    end: int


class MaxCVSSField(TypedDict):
    vulnerable: Decimal
    submitted: Decimal


class CountDayFields(TypedDict):
    past_day: int | float
    today: int | float


class CountField(TypedDict):
    counter: CountDayFields


class CountReportInfo(NamedTuple):
    allowed_users: list[str]
    content: dict[str, Any]
    group: str
    report_dates: ReportDates
    cvss: Decimal = Decimal("0.0")


class GroupFields(TypedDict):
    verified_inputs: int
    verified_ports: int
    enumerated_inputs: int
    enumerated_ports: int
    loc: int
    reattacked: int
    vulnerable: int
    sorts_verified_lines: int
    sorts_verified_lines_priority: int
    sorts_verified_lines_priority_avg: int
    submitted: int
    subscription: str
    coverage_q1: QuintileGroupFields
    coverage_q2: QuintileGroupFields
    coverage_q3: QuintileGroupFields
    coverage_q4: QuintileGroupFields
    coverage_q5: QuintileGroupFields
    coverage_na: QuintileGroupFields


class CommonFields(TypedDict):
    enumerated_inputs: CountField
    enumerated_ports: CountField
    verified_inputs: CountField
    verified_ports: CountField
    loc: CountField
    reattacked: CountField
    vulnerable: CountField
    sorts_verified_lines: CountField
    sorts_verified_lines_priority: CountField
    sorts_verified_lines_priority_avg: CountField
    submitted: CountField
    coverage_q1: QuintileFields
    coverage_q2: QuintileFields
    coverage_q3: QuintileFields
    coverage_q4: QuintileFields
    coverage_q5: QuintileFields
    coverage_na: QuintileFields
    max_cvss: MaxCVSSField
    groups: dict[str, GroupFields]


class AccessGrantedContext(TypedDict):
    responsible: str
    group: str
    group_description: NotRequired[str]
    confirm_access_url: str
    reject_access_url: str
    admin: str
    user_role: str


class GroupReportContext(TypedDict):
    filetype: str
    fname: str
    date: str
    year: str
    time: str
    group_name: str
    subject: str
    template: NotRequired[str]


class GroupAlertContext(TypedDict):
    attempt: bool | None
    created_by: str
    created_date: date
    comments: str
    date: date
    days: int
    group: str
    months: int
    organization: str
    reason: str
    responsible: str
    state: str
    subscription: str
    unique_authors: int


class GroupData(TypedDict):
    org_name: str
    exp_date: str


class DevSecOpsExpirationContext(TypedDict):
    groups_data: dict[str, GroupData]


class PolicyChange(TypedDict):
    from_: Decimal | None
    to: Decimal


class UpdatedPoliciesContext(TypedDict):
    entity_name: str
    entity_type: str
    policies_link: str
    policies_content: dict[str, PolicyChange]
    responsible: str
    date: str
    user_role: NotRequired[str]


class UsersWeeklyReportContext(TypedDict, total=False):
    pass


class ReminderContext(TypedDict, total=False):
    pass


class NumeratorReportContext(TypedDict):
    count_var_report: dict[str, Any]
    coverage: dict[str, Any]
    groups: dict[str, GroupFields]
    max_cvss: MaxCVSSField
    responsible: str


class EventConsultingCommentData(TypedDict):
    date: str
    name: str
    comment: str
    instance_id: str | None


class ConsultingGroupData(TypedDict):
    org_name: str
    event_comments: dict[str, list[EventConsultingCommentData]]
    finding_comments: dict[str, list[EventConsultingCommentData]]


class ConsultingDigestContext(TypedDict):
    groups_data: dict[str, ConsultingGroupData]
    date: str


class EventCommentData(TypedDict):
    name: str
    content: str


class EventData(TypedDict):
    age: int
    description: str
    comments: dict[str, EventCommentData]
    states: list[Any]
    status: str


class GroupEventData(TypedDict):
    events: dict[str, EventData]
    org_name: str


class EventsDigestContext(TypedDict):
    groups_data: dict[str, GroupEventData]
    date: str


class MissingEnvironmentContext(TypedDict):
    group: str
    group_date: int


class ZTNAAlertContext(TypedDict):
    organization_name: str
    status: str
    n_roots: int
    organization_url: str
    date: str


class MissingMemberAlertContext(TypedDict):
    missing_email: str
    platform_url: str


class GroupExpiringFindingsData(TypedDict):
    finding_id: str
    vulnerabilities: dict[str, int]


class TreatmentAlertGroupData(TypedDict):
    org_name: str
    finding_title: list[str]
    group_expiring_findings: list[GroupExpiringFindingsData]


class TreatmentAlertContext(TypedDict):
    groups_data: dict[str, TreatmentAlertGroupData]


class DeleteFindingContext(TypedDict):
    hacker_email: str
    finding_name: str
    finding_id: str
    justification: str
    group: str
    removed_by: str


class BaseVulnerabilityContext(TypedDict):
    finding: str
    finding_url: str
    group: str
    vulns_props: dict[str, dict[str, dict[str, str]]]
    severity_score: Decimal
    severity_level: str


class SubmitVulnerabilityContext(BaseVulnerabilityContext):
    responsible: str


class RejectVulnerabilityContext(BaseVulnerabilityContext):
    reasons: dict[str, str]
    responsible: str


class VulnReportEmailContext(BaseVulnerabilityContext):
    remaining_exposure: int | None
    severity_score_v3: Decimal
    severity_level_v3: str
    state: str
    responsible: set[str]
    is_escape: bool


class OldestRejected(TypedDict):
    datetime: datetime
    days: str
    finding_id: str
    finding_title: str
    group_name: str
    hacker_email: str
    id: str
    org_name: str
    reported_date: str
    specific: str
    where: str


class StakeholderInfo(TypedDict):
    rejected_count: int
    vulnerable_count: int


class GlobalInfo(TypedDict):
    oldest_rejected: OldestRejected | None
    rejected_count: int
    submitted_count: int


class EmailContent(TypedDict):
    stakeholders_info: dict[str, StakeholderInfo]
    global_info: GlobalInfo


class ReviewerProgressReportContext(TypedDict):
    responsible: str
    global_info: GlobalInfo
    stakeholder_info: StakeholderInfo


class TreatmentReportContext(TypedDict):
    date: str
    group: str
    responsible: str | None
    justification: str | None
    finding: str
    is_approved: bool
    vulnerabilities: list[str]
    managers_email: list[str]
    approve_state: str
    treatment_status: str
    user_role: str
    finding_link: str


class NewCommentContext(TypedDict):
    comment: list[str]
    comment_type: str
    comment_url: str
    finding_id: str
    finding_name: str
    parent: str
    group: str
    has_essential: bool
    has_advanced: bool
    user_email: str


class RemediateFindingContext(TypedDict):
    group: str
    organization: str
    finding_name: str
    finding_url: str
    finding_id: str
    user_email: str
    solution_description: list[str]


class FreeTrialStartContext(TypedDict):
    country: str | None
    email: str
    empty_notification_notice: bool
    enrolled_date: str
    enrolled_name: str
    expires_date: str
    policies_link: str
    scope_link: str
    stakeholders_link: str
    reason: str | None
    phone: str | None


class EventReportContext(TypedDict):
    group: str
    event_type: str
    description: str
    event_age: int
    event_url: str
    n_holds: int | None
    reason: str
    reminder_notification: bool
    report_date: str
    root_url: str | None
    state: str


class DeprecationNoticeContext(TypedDict):
    deprecations: dict[str, str]


class RootCloningStatusContext(TypedDict):
    is_failed: bool
    days_to_clone: int | None
    cloning_time_delta: timedelta | None
    scope_url: str
    group: str
    last_clone_date: str
    root_creation_date: str
    root_nickname: str
    root_id: str
    modified_by: str
    report_date: str


class AddedRootContext(TypedDict):
    branch: str
    group: str
    root_nickname: str
    responsible: str
    health_check: str
    root_url: str
    user_role: str
    sla: str
    ztna_required: str
    egress_required: str
    vpn_required: str
    date: str


class UpdatedRootContext(TypedDict):
    group_name: str
    responsible: str
    key_format: dict[str, str]
    new_root_content: dict[str, Any]
    old_state: dict[str, Any]
    root_nickname: str
    date: str
    user_role: str


class PortfolioReportContext(TypedDict):
    is_added: bool
    group_name: str
    responsible: str
    portfolios: str
    report_date: date
    user_role: str


class UpdatedServicesContext(TypedDict):
    group_name: str
    responsible: str
    group_changes: dict[str, Any]
    org_name: str
    report_date: date
    user_role: str


class DevSecOpsAgentTokenContext(TypedDict):
    scope_url: str
    group_name: str
    report_date: str
    responsible: str
    user_role: str
    had_token: bool


class StakeHolderUnsubscribedContext(TypedDict):
    group_name: str
    report_date: str
    user_email: str


class EnvironmentReportContext(TypedDict):
    group_name: str
    responsible: str
    git_root: str
    git_root_url: str
    user_role: str
    urls_added: list[str]
    urls_deleted: list[str]
    report_date: str
    reason: str | None


class UpdatedGroupInformationContext(TypedDict):
    group_name: str
    responsible: str
    group_changes: dict[str, Any]
    report_date: date
    user_role: str


class DeactivatedRootContext(TypedDict):
    group: str
    reason: str | None
    root_age: int
    activated_by: str
    root_nickname: str
    last_root_state: str
    last_clone_date: str
    responsible: str
    vuln_counter: list[tuple[int, tuple[str, int]]]
    vuln_counter_len: int
    user_role: str
    with_sast: bool


class FileReportContext(TypedDict):
    state: str
    group_name: str
    responsible: str
    file_name: str
    file_description: str
    report_date: date
    user_role: str
    uploaded_date: date | None
    uploaded_days_to_date: int | None


class FreeTrialOverContext(TypedDict):
    vulnerabilities_link: str


class TrialFirstScanningContext(TypedDict):
    app_link: str
    cloning_status: str | None
    have_vulnerabilities: bool


class UpdatedTreatmentContext(TypedDict):
    assigned: str
    group: str
    justification: str
    responsible: str
    treatment: str
    finding: str
    user_role: str
    vulnerabilities: list[str]
    finding_link: str


class AssignedVulnerabilityContext(TypedDict):
    finding_title: str
    group: str
    finding_url: str
    responsible: str
    where: list[str]
