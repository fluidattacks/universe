from decimal import Decimal

from aioextensions import collect

from integrates import authz
from integrates.custom_exceptions import OrganizationNotFound
from integrates.custom_utils.groups import get_group
from integrates.custom_utils.validations import is_fluid_staff
from integrates.dataloaders import Dataloaders
from integrates.db_model.findings.types import Finding
from integrates.db_model.groups.types import Group
from integrates.db_model.stakeholders.types import Stakeholder, StakeholderState
from integrates.group_access.domain import get_group_members_emails_by_preferences
from integrates.mailer.preferences import MAIL_PREFERENCES
from integrates.organization_access.domain import get_organization_members_emails_by_preferences


async def get_available_notifications(loaders: Dataloaders, email: str) -> list[str]:
    stakeholder_roles = await get_stakeholder_roles(loaders, email)
    available_notifications_by_template = [
        template.email_preferences
        for template in MAIL_PREFERENCES.values()
        if template.email_preferences
        and (
            any(item in template.roles.group for item in stakeholder_roles["group"])
            or any(item in template.roles.org for item in stakeholder_roles["org"])
        )
    ]
    return sorted(set(available_notifications_by_template))


async def get_group_emails_by_notification(
    *,
    loaders: Dataloaders,
    group_name: str,
    notification: str,
) -> list[str]:
    preferences = MAIL_PREFERENCES[notification]
    group_roles = preferences.roles.group
    return await get_group_members_emails_by_preferences(
        loaders=loaders,
        group_name=group_name,
        notification=preferences.email_preferences.value if preferences.email_preferences else None,
        roles=set(group_roles),
        exclude_trial=preferences.exclude_trial,
        only_fluid_staff=preferences.only_fluid_staff,
    )


async def get_organization_emails_by_notification(
    *,
    loaders: Dataloaders,
    organization_id: str,
    notification: str,
) -> list[str]:
    preferences = MAIL_PREFERENCES[notification]
    organization_roles = preferences.roles.org
    return await get_organization_members_emails_by_preferences(
        loaders=loaders,
        organization_id=organization_id,
        notification=preferences.email_preferences.value if preferences.email_preferences else None,
        roles=set(organization_roles),
        exclude_trial=preferences.exclude_trial,
        only_fluid_staff=preferences.only_fluid_staff,
    )


async def get_org_groups(loaders: Dataloaders, org_id: str) -> list[Group]:
    return await loaders.organization_groups.load(org_id)


async def get_organization_country(loaders: Dataloaders, group_name: str) -> str | None:
    group = await get_group(loaders, group_name)
    organization = await loaders.organization.load(group.organization_id)
    if not organization:
        raise OrganizationNotFound()
    return organization.country


async def get_organization_name(loaders: Dataloaders, group_name: str) -> str:
    group = await get_group(loaders, group_name)
    organization = await loaders.organization.load(group.organization_id)
    if not organization:
        raise OrganizationNotFound()
    return organization.name


async def get_stakeholder_roles(loaders: Dataloaders, email: str) -> dict[str, set[str]]:
    stakeholder_orgs = await loaders.stakeholder_organizations_access.load(email)
    org_roles = await collect(
        [
            authz.get_organization_level_role(loaders, email, org.organization_id)
            for org in stakeholder_orgs
        ],
    )
    org_groups = await collect(
        [get_org_groups(loaders, org.organization_id) for org in stakeholder_orgs],
    )
    group_roles = await collect(
        [
            authz.get_group_level_role(loaders, email, item.name)
            for group in org_groups
            for item in group
        ],
    )
    return {
        "group": set(" ".join(group_roles).split()),
        "org": set(" ".join(org_roles).split()),
    }


async def get_vulnerability_report_group_emails(
    loaders: Dataloaders,
    group_name: str,
    severity_score: Decimal,
    group_findings: list[Finding],
    exclude_fluid_staff: bool,
) -> list[str]:
    group_emails = await get_group_emails_by_notification(
        loaders=loaders,
        group_name=group_name,
        notification="vulnerability_report",
    )
    stakeholders = await loaders.stakeholder.load_many(group_emails)
    return [
        stakeholder.email
        for stakeholder in stakeholders
        if stakeholder
        and not (exclude_fluid_staff and is_fluid_staff(stakeholder.email))
        and should_send_vulnerability_mail(
            severity_score=severity_score,
            stakeholder=stakeholder,
            group_findings=group_findings,
        )
    ]


def should_send_vulnerability_mail(
    *,
    severity_score: Decimal,
    stakeholder: Stakeholder,
    group_findings: list[Finding],
) -> bool:
    state: StakeholderState = stakeholder.state
    return (
        severity_score >= state.notifications_preferences.parameters.min_severity
        or not group_findings
    )


def translate_boolean(value: bool) -> str:
    return "Yes" if value else "No"


def translate_sla(value: bool) -> str:
    return "Applicable" if value else "Not applicable"


async def get_user_phone_number(loaders: Dataloaders, email: str) -> str | None:
    stakeholder = await loaders.stakeholder.load(email)
    mobile = stakeholder.phone if stakeholder and stakeholder.phone else None
    if not mobile:
        return None
    return f"+{mobile.calling_country_code}{mobile.national_number}"
