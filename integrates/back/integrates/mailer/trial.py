from integrates.audit import AuditEvent, add_audit_event
from integrates.context import (
    BASE_URL,
    FI_MAIL_PROFILING,
)
from integrates.custom_utils import (
    analytics,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.roots.enums import (
    RootCloningStatus,
)
from integrates.db_model.trials.enums import (
    TrialReason,
)
from integrates.mailer.types import (
    TrialEngagementInfo,
)
from integrates.mailer.utils import (
    get_organization_country,
    get_organization_name,
    get_user_phone_number,
)

from .common import (
    get_recipient_first_name,
    send_mails_async,
)
from .types import FreeTrialOverContext, FreeTrialStartContext, TrialFirstScanningContext


async def send_mail_abandoned_trial(
    loaders: Dataloaders,
    email_to: str,
) -> None:
    await send_mails_async(
        loaders=loaders,
        email_to=[email_to],
        subject=("You’re just one step away from securing your application 😕"),
        template_name="abandoned_trial",
    )


async def send_mail_abandoned_trial_2(loaders: Dataloaders, email_to: str) -> None:
    await send_mails_async(
        loaders=loaders,
        email_to=[email_to],
        subject=("Don't lose out on this free trial for the security of your apps! ⚠️"),
        template_name="abandoned_trial_2",
    )


async def send_mail_abandoned_trial_3(loaders: Dataloaders, email_to: str) -> None:
    await send_mails_async(
        loaders=loaders,
        email_to=[email_to],
        subject=("Last chance to try Fluid Attacks' AppSec for free! ⏳"),
        template_name="abandoned_trial_3",
    )


async def send_mail_free_trial_over(
    loaders: Dataloaders,
    email_to: list[str],
    group_name: str,
) -> None:
    org_name = await get_organization_name(loaders, group_name)
    context: FreeTrialOverContext = {
        "vulnerabilities_link": (f"{BASE_URL}/orgs/{org_name}/groups/{group_name}/vulns"),
    }
    await send_mails_async(
        loaders=loaders,
        email_to=email_to,
        context=dict(context),
        subject="Your free trial ends today.",
        template_name="free_trial_over",
    )


async def context_mail_free_trial_start(
    loaders: Dataloaders,
    email_to: str,
    full_name: str,
    group_name: str,
    reason: str,
) -> FreeTrialStartContext:
    org_name = await get_organization_name(loaders, group_name)
    org_country = await get_organization_country(loaders, group_name)
    stakeholder_phone = await get_user_phone_number(loaders, email_to)

    context: FreeTrialStartContext = {
        "country": org_country,
        "email": email_to,
        "empty_notification_notice": True,
        "enrolled_date": datetime_utils.get_as_str(
            datetime_utils.get_now(),
            "%Y-%m-%d %H:%M:%S %Z",
        ),
        "enrolled_name": full_name,
        "expires_date": datetime_utils.get_as_str(datetime_utils.get_now_plus_delta(days=21)),
        "policies_link": f"{BASE_URL}/orgs/{org_name}/policies",
        "scope_link": (f"{BASE_URL}/orgs/{org_name}/groups/{group_name}/scope"),
        "stakeholders_link": (f"{BASE_URL}/orgs/{org_name}/groups/{group_name}/stakeholders"),
        "reason": reason if reason != "" else None,
        "phone": stakeholder_phone if stakeholder_phone else None,
    }

    return context


async def send_mail_free_trial_start(
    loaders: Dataloaders,
    email_to: str,
    full_name: str,
    group_name: str,
) -> None:
    context = await context_mail_free_trial_start(
        loaders,
        email_to,
        full_name,
        group_name,
        reason="",
    )
    await analytics.mixpanel_track(email_to, "AutoenrollSuccess")
    add_audit_event(
        AuditEvent(
            author=email_to,
            action="READ",
            metadata={},
            object="Autoenroll.Success",
            object_id="unknown",
        )
    )
    await send_mails_async(
        loaders=loaders,
        email_to=[email_to],
        context=dict(context),
        subject="Your free trial with Fluid Attacks has started 🚀",
        template_name="free_trial",
    )


async def send_mail_free_trial_2(loaders: Dataloaders, info: TrialEngagementInfo) -> None:
    fname = await get_recipient_first_name(loaders, info.email_to)
    await send_mails_async(
        loaders=loaders,
        email_to=[info.email_to],
        subject=f"[{fname}], boost your code security with our IDE plugins 🧩",
        template_name="free_trial_2",
    )


async def send_mail_free_trial_3(loaders: Dataloaders, info: TrialEngagementInfo) -> None:
    await send_mails_async(
        loaders=loaders,
        email_to=[info.email_to],
        subject="A quick check-in on your risk exposure mitigation journey 🔧",
        template_name="free_trial_3",
    )


async def send_mail_free_trial_4(loaders: Dataloaders, info: TrialEngagementInfo) -> None:
    await send_mails_async(
        loaders=loaders,
        email_to=[info.email_to],
        subject="Did you try our CI Agent for secure software deployment? 🔍",
        template_name="free_trial_4",
    )


async def send_mail_free_trial_5(loaders: Dataloaders, info: TrialEngagementInfo) -> None:
    await send_mails_async(
        loaders=loaders,
        email_to=[info.email_to],
        subject=("Don't miss out. Firmly secure your software and users today! 🛡️"),
        template_name="free_trial_5",
    )


async def send_mail_free_trial_6(loaders: Dataloaders, info: TrialEngagementInfo) -> None:
    await send_mails_async(
        loaders=loaders,
        email_to=[info.email_to],
        subject=("Your trial ends, but your journey to security should go on! ✨"),
        template_name="free_trial_6",
    )


async def send_mail_free_trial_7(loaders: Dataloaders, info: TrialEngagementInfo) -> None:
    await send_mails_async(
        loaders=loaders,
        email_to=[info.email_to],
        subject=("Extend your security journey with Fluid Attacks! 🤝"),
        template_name="free_trial_7",
    )


async def send_mail_free_trial_8(loaders: Dataloaders, info: TrialEngagementInfo) -> None:
    await send_mails_async(
        loaders=loaders,
        email_to=[info.email_to],
        subject=("Don't let security vulnerabilities slip through the cracks ☢️"),
        template_name="free_trial_8",
    )


async def send_mail_new_enrolled_user(
    loaders: Dataloaders,
    email_to: str,
    full_name: str,
    group_name: str,
    reason: str | TrialReason,
) -> None:
    context = await context_mail_free_trial_start(
        loaders,
        email_to,
        full_name,
        group_name,
        reason,
    )
    enrolled_email_to: list = FI_MAIL_PROFILING.split(",")
    await send_mails_async(
        loaders=loaders,
        email_to=enrolled_email_to,
        context=dict(context),
        subject=f"New enrolled user [{email_to}] from [{context['country']}]",
        template_name="new_enrolled",
    )


async def send_trial_first_scanning(
    loaders: Dataloaders,
    email_to: list[str],
    have_vulns: bool,
    group_name: str,
    cloning_status: RootCloningStatus | None = None,
) -> None:
    org_name = await get_organization_name(loaders, group_name)
    app_link = f"{BASE_URL}/orgs/{org_name}/groups/{group_name}"
    context: TrialFirstScanningContext = {
        "app_link": f"{app_link}/vulns" if have_vulns else f"{app_link}/scope",
        "cloning_status": cloning_status.value if cloning_status else cloning_status,
        "have_vulnerabilities": have_vulns,
    }
    subject = "We found" if have_vulns else "Could not find"
    await send_mails_async(
        loaders=loaders,
        email_to=email_to,
        subject=f"{subject} vulnerabilities in your repository!",
        context=dict(context),
        template_name="trial_first_scanning",
    )


async def new_enrolled_user_mail(
    *,
    loaders: Dataloaders,
    user_email: str,
    full_name: str,
    group_name: str,
) -> None:
    trial = await loaders.trial.load(user_email)

    if trial and not trial.completed:
        reason = trial.reason if trial.reason else ""
        await send_mail_new_enrolled_user(loaders, user_email, full_name, group_name, reason)
