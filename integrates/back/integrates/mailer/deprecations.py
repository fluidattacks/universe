from integrates.custom_utils.datetime import (
    get_now_plus_delta,
)
from integrates.dataloaders import (
    Dataloaders,
)

from .common import (
    send_mails_async,
)
from .types import DeprecationNoticeContext


async def send_mail_deprecation_notice(
    *,
    loaders: Dataloaders,
    mail_deprecations: dict[str, str],
    email_to: set[str],
) -> None:
    # These mails are meant to anticipate next month's deprecations
    month: str = get_now_plus_delta(weeks=4).strftime("%B")
    email_context: DeprecationNoticeContext = {
        "deprecations": mail_deprecations,
    }
    await send_mails_async(
        loaders=loaders,
        email_to=list(email_to),
        context=dict(email_context),
        subject=f"{month} Deprecation Notice",
        template_name="deprecation_notice",
    )
