import os
from pathlib import Path

from integrates.db_model.enums import (
    Notification,
)
from integrates.mailer.preferences import MAIL_PREFERENCES
from integrates.testing.utils import parametrize


@parametrize(
    args=["expected"],
    cases=[
        [
            ["email_preferences", "exclude_trial", "only_fluid_staff", "roles", "context_type"],
        ],
    ],
)
def test_mail_preferences(expected: list[str]) -> None:
    path: str = os.path.dirname(os.path.abspath(__file__))
    entries = Path(os.path.join(path, "email_templates"))
    notifications = {
        entry.name.removesuffix(".mjml")
        for entry in entries.iterdir()
        if entry.name.endswith(".mjml")
    }
    assert len(notifications) == len(MAIL_PREFERENCES)
    assert sorted(MAIL_PREFERENCES.keys()) == list(MAIL_PREFERENCES.keys())
    for notification in notifications:
        assert list(MAIL_PREFERENCES[notification]._asdict().keys()) == expected
    assert all(
        (item.email_preferences in list(Notification.__members__) or item.email_preferences is None)
        for item in MAIL_PREFERENCES.values()
    )
