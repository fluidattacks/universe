from decimal import Decimal

from integrates.context import BASE_URL, FI_MAIL_CUSTOMER_SUCCESS, FI_MAIL_REVIEWERS
from integrates.custom_utils import validations
from integrates.custom_utils.groups import get_group
from integrates.dataloaders import Dataloaders
from integrates.db_model.enums import Notification, Source, StateRemovalJustification
from integrates.db_model.finding_comments.enums import CommentType
from integrates.db_model.finding_comments.types import FindingComment
from integrates.db_model.findings.types import Finding
from integrates.db_model.groups.types import Group
from integrates.db_model.vulnerabilities.enums import VulnerabilityStateReason
from integrates.group_access import domain as group_access_domain
from integrates.mailer.enums import MailVulnerabilityReportState
from integrates.mailer.utils import (
    get_group_emails_by_notification,
    get_organization_name,
    get_vulnerability_report_group_emails,
)

from .common import send_mails_async
from .types import (
    DeleteFindingContext,
    NewCommentContext,
    RejectVulnerabilityContext,
    RemediateFindingContext,
    SubmitVulnerabilityContext,
    VulnReportEmailContext,
)


async def _get_email_context(
    *,
    loaders: Dataloaders,
    group_name: str,
    group: Group,
    type_: CommentType,
    comment_data: FindingComment,
    is_finding_released: bool,
    finding_id: str,
    finding_title: str,
    user_mail: str,
) -> NewCommentContext:
    org_name = await get_organization_name(loaders, group_name)
    type_fmt = (
        "consulting" if type_ in [CommentType.COMMENT, CommentType.VERIFICATION] else "observation"
    )
    email_context: NewCommentContext = {
        "comment": comment_data.content.splitlines(),
        "comment_type": type_fmt,
        "comment_url": (
            f"{BASE_URL}/orgs/{org_name}/groups/{group_name}/"
            f'{"vulns" if is_finding_released else "drafts"}/{finding_id}/'
            f"{type_fmt}"
        ),
        "finding_id": finding_id,
        "finding_name": finding_title,
        "parent": str(comment_data.parent_id),
        "group": group_name,
        "has_essential": group.state.has_essential,
        "has_advanced": group.state.has_advanced,
        "user_email": user_mail,
    }
    return email_context


async def send_mail_comment(
    *,
    loaders: Dataloaders,
    comment_data: FindingComment,
    user_mail: str,
    finding_id: str,
    finding_title: str,
    recipients: list[str],
    group_name: str,
    is_finding_released: bool,
) -> None:
    group = await get_group(loaders, group_name)
    type_ = comment_data.comment_type
    email_context = await _get_email_context(
        loaders=loaders,
        group_name=group_name,
        group=group,
        type_=type_,
        comment_data=comment_data,
        is_finding_released=is_finding_released,
        finding_id=finding_id,
        finding_title=finding_title,
        user_mail=user_mail,
    )
    members = await loaders.stakeholder.load_many(recipients)
    members_email = [
        member.email
        for member in members
        if member and Notification.NEW_COMMENT in member.state.notifications_preferences.email
    ]
    reviewers = FI_MAIL_REVIEWERS.split(",")
    customer_success_recipients = FI_MAIL_CUSTOMER_SUCCESS.split(",")
    if type_ == CommentType.OBSERVATION:
        await send_mails_async(
            loaders=loaders,
            email_to=[
                *members_email,
                *customer_success_recipients,
                *reviewers,
            ],
            context=dict(email_context),
            subject=f"New observation in [{finding_title}] for [{group_name}]",
            template_name="new_comment",
        )


async def send_mail_remove_finding(
    loaders: Dataloaders,
    finding: Finding,
    justification: StateRemovalJustification,
    removed_by: str,
) -> None:
    justification_dict = {
        StateRemovalJustification.DUPLICATED: "It is duplicated",
        StateRemovalJustification.FALSE_POSITIVE: "It is a false positive",
        StateRemovalJustification.NO_JUSTIFICATION: "",
        StateRemovalJustification.NOT_REQUIRED: "Finding not required",
        StateRemovalJustification.REPORTING_ERROR: "It is a reporting error",
    }
    discoverer_email = finding.get_hacker_email()
    recipients = FI_MAIL_REVIEWERS.split(",")
    mail_context: DeleteFindingContext = {
        "hacker_email": discoverer_email,
        "finding_name": finding.title,
        "finding_id": finding.id,
        "justification": justification_dict[justification],
        "group": finding.group_name,
        "removed_by": removed_by,
    }
    await send_mails_async(
        loaders=loaders,
        email_to=recipients,
        context=dict(mail_context),
        subject=("Type of vulnerability removed [{finding.title}] in [{finding.group_name}]"),
        template_name="delete_finding",
    )


async def send_mail_remediate_finding(
    *,
    loaders: Dataloaders,
    user_email: str,
    finding_id: str,
    finding_name: str,
    group_name: str,
    justification: str,
) -> None:
    org_name = await get_organization_name(loaders, group_name)
    members_email = await get_group_emails_by_notification(
        loaders=loaders,
        group_name=group_name,
        notification="remediate_finding",
    )
    mail_context: RemediateFindingContext = {
        "group": group_name.lower(),
        "organization": org_name,
        "finding_name": finding_name,
        "finding_url": (
            f"{BASE_URL}/orgs/{org_name}/groups/{group_name}/vulns/{finding_id}/locations"
        ),
        "finding_id": finding_id,
        "user_email": user_email,
        "solution_description": justification.splitlines(),
    }
    await send_mails_async(
        loaders=loaders,
        email_to=members_email,
        context=dict(mail_context),
        subject=f"New remediation for [{finding_name}] in [{group_name}]",
        template_name="remediate_finding",
    )


async def _get_email_members(
    *,
    loaders: Dataloaders,
    group_name: str,
) -> list[str]:
    group_members = await group_access_domain.get_group_stakeholders(loaders, group_name)
    recipients = [member.email for member in group_members]
    members = await loaders.stakeholder.load_many(recipients)
    members_email = [
        member.email for member in members if member and validations.is_fluid_staff(member.email)
    ]
    return members_email


async def _get_group_emails(
    *,
    loaders: Dataloaders,
    group_name: str,
    severity_score: Decimal,
    responsible: set[str],
) -> list[str]:
    group_findings = await loaders.group_findings.load(group_name)
    group_emails = await get_vulnerability_report_group_emails(
        loaders=loaders,
        group_name=group_name,
        severity_score=severity_score,
        group_findings=group_findings,
        exclude_fluid_staff=not responsible,
    )
    return group_emails


async def _get_email_context_vuln_report(
    *,
    loaders: Dataloaders,
    group_name: str,
    finding_title: str,
    finding_id: str,
    vulnerabilities_properties: dict[str, dict[str, dict[str, str]]],
    responsible: set[str],
    remaining_exposure: int | None,
    severity_score: Decimal,
    severity_level: str,
    severity_score_v3: Decimal,
    severity_level_v3: str,
    state: MailVulnerabilityReportState,
) -> VulnReportEmailContext:
    org_name = await get_organization_name(loaders, group_name)
    email_context: VulnReportEmailContext = {
        "finding": finding_title,
        "group": group_name.lower(),
        "finding_url": (
            f"{BASE_URL}/orgs/{org_name}/groups/{group_name}/vulns/{finding_id}/locations"
        ),
        "vulns_props": vulnerabilities_properties,
        "responsible": responsible,
        "remaining_exposure": remaining_exposure,
        "severity_score": severity_score,
        "severity_level": severity_level.capitalize(),
        "severity_score_v3": severity_score_v3,
        "severity_level_v3": severity_level_v3.capitalize(),
        "state": str(state.value).lower(),
        "is_escape": False,
    }
    return email_context


async def _check_and_send_escape_email(
    *,
    loaders: Dataloaders,
    group_name: str,
    email_context: VulnReportEmailContext,
    finding_title: str,
    vulnerabilities_properties: dict[str, dict[str, dict[str, str]]],
    state: MailVulnerabilityReportState,
) -> None:
    is_escape = any(
        any(repo_id["source"] == Source.ESCAPE.value for repo_id in repo.values())
        for repo in vulnerabilities_properties.values()
    )
    if is_escape:
        email_context["is_escape"] = True
        members_email = await _get_email_members(
            loaders=loaders,
            group_name=group_name,
        )
        await send_mails_async(
            loaders=loaders,
            email_to=members_email,
            context=dict(email_context),
            subject=(f"{finding_title} {str(state.value).lower()} as escape in ")
            + f"[{group_name}].",
            template_name="vulnerability_report",
        )


async def send_mail_vulnerability_report(
    *,
    loaders: Dataloaders,
    group_name: str = "",
    finding_title: str,
    finding_id: str,
    vulnerabilities_properties: dict[str, dict[str, dict[str, str]]],
    responsible: set[str],
    severity_score: Decimal,
    severity_level: str,
    severity_score_v3: Decimal,
    severity_level_v3: str,
    state: MailVulnerabilityReportState = (MailVulnerabilityReportState.REPORTED),
    remaining_exposure: int | None = None,
) -> None:
    email_context = await _get_email_context_vuln_report(
        loaders=loaders,
        group_name=group_name,
        finding_title=finding_title,
        finding_id=finding_id,
        vulnerabilities_properties=vulnerabilities_properties,
        responsible=responsible,
        remaining_exposure=remaining_exposure,
        severity_score=severity_score,
        severity_level=severity_level,
        severity_score_v3=severity_score_v3,
        severity_level_v3=severity_level_v3,
        state=state,
    )
    group_emails = await _get_group_emails(
        loaders=loaders,
        group_name=group_name,
        severity_score=severity_score,
        responsible=responsible,
    )
    await send_mails_async(
        loaders=loaders,
        email_to=group_emails,
        context=dict(email_context),
        subject=(f"{finding_title} {str(state.value).lower()} in [{group_name}]."),
        template_name="vulnerability_report",
    )
    if responsible and state is MailVulnerabilityReportState.REPORTED:
        await _check_and_send_escape_email(
            loaders=loaders,
            group_name=group_name,
            email_context=email_context,
            finding_title=finding_title,
            vulnerabilities_properties=vulnerabilities_properties,
            state=state,
        )


async def send_mail_reject_vulnerability(
    *,
    loaders: Dataloaders,
    finding: Finding,
    stakeholder_email: str,
    rejection_reasons: set[VulnerabilityStateReason],
    other_reason: str | None,
    vulnerabilities_properties: dict[str, dict[str, dict[str, str]]],
    severity_score: Decimal,
    severity_level: str,
    submitters_emails: set[str],
) -> None:
    org_name = await get_organization_name(loaders, finding.group_name)
    recipients = set(FI_MAIL_REVIEWERS.split(","))
    recipients.add(stakeholder_email)
    recipients.update(submitters_emails)
    explanations: dict[VulnerabilityStateReason, str] = {
        VulnerabilityStateReason.CONSISTENCY: (
            "There are consistency issues with the vulnerabilities, the severity or the evidence"
        ),
        VulnerabilityStateReason.EVIDENCE: "The evidence is insufficient",
        VulnerabilityStateReason.NAMING: (
            "The vulnerabilities should be submitted under another Finding type"
        ),
        VulnerabilityStateReason.OMISSION: ("More data should be gathered before submission"),
        VulnerabilityStateReason.SCORING: "Faulty severity scoring",
        VulnerabilityStateReason.WRITING: "The writing could be improved",
        VulnerabilityStateReason.OTHER: other_reason.capitalize() if other_reason else "",
    }
    reasons: dict[str, str] = {
        str(reason.value).capitalize(): explanation
        for reason, explanation in explanations.items()
        if reason in rejection_reasons
    }
    email_context: RejectVulnerabilityContext = {
        "finding": finding.title,
        "finding_url": (
            f"{BASE_URL}/orgs/{org_name}/groups/{finding.group_name}"
            f"/vulns/{finding.id}/locations"
        ),
        "group": finding.group_name,
        "vulns_props": vulnerabilities_properties,
        "responsible": finding.get_hacker_email(),
        "severity_score": severity_score,
        "severity_level": severity_level,
        "reasons": reasons,
    }
    await send_mails_async(
        loaders=loaders,
        email_to=list(recipients),
        context=dict(email_context),
        subject=(f"Rejected location of [{finding.title}] in [{finding.group_name}]"),
        template_name="vulnerability_rejection",
    )


async def send_mail_submit_vulnerability(
    *,
    loaders: Dataloaders,
    finding: Finding,
    responsible: str,
    vulnerabilities_properties: dict[str, dict[str, dict[str, str]]],
    severity_score: Decimal,
    severity_level: str,
) -> None:
    org_name = await get_organization_name(loaders, finding.group_name)
    recipients = set(FI_MAIL_REVIEWERS.split(","))
    recipients.add(responsible)
    email_context: SubmitVulnerabilityContext = {
        "finding": finding.title,
        "finding_url": (
            f"{BASE_URL}/orgs/{org_name}/groups/{finding.group_name}"
            f"/vulns/{finding.id}/locations"
        ),
        "group": finding.group_name,
        "vulns_props": vulnerabilities_properties,
        "responsible": responsible,
        "severity_score": severity_score,
        "severity_level": severity_level,
    }
    await send_mails_async(
        loaders=loaders,
        email_to=list(recipients),
        context=dict(email_context),
        subject=(f"Submitted location of [{finding.title}] in [{finding.group_name}]"),
        template_name="vulnerability_submission",
    )
