import hashlib
import html
import json
import logging
import os
from datetime import datetime

from botocore.exceptions import ClientError

from integrates.custom_exceptions import AcceptanceNotRequested, InvalidParameter
from integrates.custom_utils import findings as findings_utils
from integrates.custom_utils.groups import get_group
from integrates.custom_utils.utils import get_missing_dependency
from integrates.custom_utils.validations_deco import validate_fields_deco
from integrates.custom_utils.vulnerabilities import get_path_from_integrates_vulnerability
from integrates.dataloaders import Dataloaders
from integrates.db_model.enums import AcceptanceStatus, TreatmentStatus
from integrates.db_model.organization_finding_policies.types import OrgFindingPolicy
from integrates.db_model.types import Treatment
from integrates.db_model.vulnerabilities.enums import VulnerabilityType
from integrates.db_model.vulnerabilities.types import Vulnerability
from integrates.organizations.utils import get_organization
from integrates.sqs.resource import get_client

SQS_TASKS_LOGGER = logging.getLogger("sqs_tasks")


def get_hash(specific: str, type_: str, where: str, root_id: str | None = None) -> int:
    # Return a unique identifier according to the business rules
    items: tuple[str, ...] = (specific, type_, where)
    if root_id:
        items = (*items, root_id)
    return hash(items)


async def get_hash_from_machine_vuln(loaders: Dataloaders, vuln: Vulnerability) -> int:
    if vuln.hacker_email != "machine@fluidattacks.com" or vuln.skims_method is None:
        raise InvalidParameter()

    finding = await findings_utils.get_finding(loaders, vuln.finding_id)
    return int.from_bytes(
        hashlib.sha256(
            bytes(
                (
                    get_path_from_integrates_vulnerability(
                        vuln.state.where,
                        vuln.type,
                        ignore_cve=True,
                    )[1]
                    + vuln.state.specific
                    + finding.get_criteria_code()
                    + vuln.skims_method
                    # if you want to add a field to the hash you must
                    # also do it in the skims function
                    # in the sarif builder code
                    + (
                        get_missing_dependency(vuln.state.where)
                        if vuln.skims_method == "python.pip_incomplete_dependencies_list"
                        else ""
                    )
                    + (
                        vuln.state.advisories.package
                        if vuln.state.advisories and vuln.state.advisories.package
                        else ""
                    )
                    + (
                        vuln.state.advisories.vulnerable_version
                        if vuln.state.advisories and vuln.state.advisories.vulnerable_version
                        else ""
                    )
                ),
                encoding="utf-8",
            ),
        ).digest()[:8],
        "little",
    )


async def get_vulnerabilities_paths_by_root(
    loaders: Dataloaders,
    group_name: str,
    vulns: list[Vulnerability],
) -> dict[str, list[str]]:
    root_vuln_paths: dict[str, set[str]] = {}

    for vuln in vulns:
        if vuln.type != VulnerabilityType.LINES or not vuln.root_id:
            continue
        vuln_path = get_path_from_integrates_vulnerability(vuln.state.where, vuln.type, True)[1]
        if vuln.root_id not in root_vuln_paths:
            root_vuln_paths[vuln.root_id] = {vuln_path}
        else:
            root_vuln_paths[vuln.root_id].add(vuln_path)

    # Including roots with only dast vulnerabilities reported
    for vuln in vulns:
        if (
            vuln.type != VulnerabilityType.LINES
            and vuln.root_id
            and not root_vuln_paths.get(vuln.root_id)
        ):
            root_vuln_paths[vuln.root_id] = {"."}

    roots = await loaders.group_roots.load(group_name)

    root_nicknames = {root.id: root.state.nickname for root in roots}

    return {
        root_nicknames[root_id]: list(paths)
        for root_id, paths in root_vuln_paths.items()
        if root_id in root_nicknames
    }


def validate_and_get_hash(
    vuln: Vulnerability,
    validate_root: bool = True,
    from_yaml: bool = False,
) -> int:
    specific = vuln.state.specific
    where = vuln.get_path() if validate_root else vuln.state.where
    # https://gitlab.com/fluidattacks/universe/-/issues/5556#note_725588290
    if from_yaml:
        specific = html.escape(specific, quote=False)
        where = html.escape(where, quote=False)
    return get_hash(
        specific=specific,
        type_=vuln.type.value,
        where=where,
        root_id=vuln.root_id,
    )


def validate_acceptance(vuln: Vulnerability) -> None:
    if vuln.treatment and vuln.treatment.acceptance_status != AcceptanceStatus.SUBMITTED:
        raise AcceptanceNotRequested()


def is_valid_accepted_treatment(
    vulnerability_treatment: Treatment,
) -> bool:
    valid_statuses = [AcceptanceStatus.APPROVED, None]
    if (
        vulnerability_treatment.status == TreatmentStatus.ACCEPTED
        and vulnerability_treatment.acceptance_status in valid_statuses
    ):
        return True
    return False


def is_permanent_acceptance(treatment_value: str) -> bool:
    if treatment_value == "ACCEPTED_UNDEFINED":
        return True
    return False


async def get_organization_finding_policy(
    *,
    loaders: Dataloaders,
    finding_title: str,
    group_name: str,
    treatment: TreatmentStatus,
) -> OrgFindingPolicy | None:
    group = await get_group(loaders, group_name)
    organization = await get_organization(loaders, group.organization_id)
    specific_finding_policy = await findings_utils.get_finding_policy_by_name(
        loaders=loaders,
        finding_name=finding_title,
        organization_name=organization.name,
        treatment=treatment,
    )

    if specific_finding_policy:
        return specific_finding_policy

    # Find a global policy
    return await findings_utils.get_finding_policy_by_name(
        loaders=loaders,
        finding_name="All vulnerability types",
        organization_name=organization.name,
        treatment=treatment,
    )


def format_vulnerability_treatment_data(
    *,
    acceptance: bool,
    accepted_until: datetime | None,
    justification: str,
    today: datetime,
    user_email: str,
    status: TreatmentStatus,
) -> Treatment:
    return Treatment(
        acceptance_status=AcceptanceStatus.APPROVED if acceptance else AcceptanceStatus.REJECTED,
        accepted_until=accepted_until,
        justification=justification,
        modified_date=today,
        modified_by=user_email,
        status=status,
    )


def format_vulnerability_locations(where: list[str]) -> str:
    location_str: str = ""
    for location in set(where):
        location_str += f"{location}\n"
    return location_str


@validate_fields_deco(["tags"])
def format_tags(tags: list[str]) -> list[str]:
    return [html.unescape(tag.strip()) for tag in tags if tag.strip()]


def is_executable(field: str, url: str) -> bool:
    executable_extensions = [
        ".exe",
        ".bat",
        ".cmd",
        ".com",
        ".ps1",
        ".vbs",
        ".wsf",
        ".msi",
        ".dll",
        ".out",
        ".run",
        ".sh",
        ".elf",
        ".app",
        ".command",
        ".pkg",
        ".dmg",
        ".apk",
        ".ipa",
        ".so",
    ]
    for path in (field, url):
        splitted_path = path.split(":", 1)[0]
        _, ext = os.path.splitext(splitted_path.rstrip("/"))
        if ext.lower() in executable_extensions:
            return True

    return False


async def set_snippet_remote(vulnerability_id: str, commit: str | None = None) -> None:
    sqs_client = await get_client()
    try:
        await sqs_client.send_message(
            QueueUrl=("https://sqs.us-east-1.amazonaws.com/205810638802/integrates_snippet"),
            MessageBody=json.dumps(
                {
                    "id": f"{vulnerability_id}_{commit or ''}",
                    "task": "get_snippet",
                    "args": [vulnerability_id, commit],
                },
            ),
        )
    except ClientError:
        SQS_TASKS_LOGGER.error(
            "Attempted to enqueue the task",
            extra={
                "extra": {
                    "vulnerability_id": vulnerability_id,
                    "commit": commit,
                    "status": "FAILED",
                    "task_name": "get_snippet",
                },
            },
        )
