import asyncio
import hashlib
import logging
from contextlib import (
    suppress,
)

from fluidattacks_core.serializers.snippet import (
    Snippet,
    SnippetViewport,
    extract_snippet_function_from_nodes,
    make_snippet,
    make_snippet_function,
)
from fluidattacks_core.serializers.syntax import (
    InvalidFileType,
    get_language_from_path,
    parse_content_tree_sitter,
    query_function_by_name,
)
from git.exc import (
    GitCommandError,
)
from git.repo import (
    Repo,
)
from opensearchpy import NotFoundError
from voyageai.client_async import AsyncClient
from voyageai.error import VoyageError

from integrates.context import FI_VOYAGE_API_KEY
from integrates.custom_exceptions import (
    VulnNotFound,
)
from integrates.custom_utils.findings import get_finding
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.vulnerabilities.add import add_vulnerability_snippet
from integrates.db_model.vulnerabilities.types import (
    VulnerabilityState,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.dynamodb.exceptions import ValidationException
from integrates.search.client import get_client
from integrates.settings.logger import (
    LOGGING,
)
from integrates.vulnerabilities.domain import (
    get_vulnerability,
)
from integrates.vulnerabilities.fixes.bedrock import Tokens, converse_plain
from integrates.vulnerabilities.fixes.types import ConverseMessage

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger(__name__)


def get_vulnerable_line_content(snippet: Snippet) -> str | None:
    lines = snippet.content.split("\n")
    if not snippet.line:
        return None
    focus_line = (snippet.line - (snippet.offset + snippet.line_context + 1)) - 1
    try:
        return lines[int(focus_line)]
    except IndexError:
        return None


async def get_functional_semantics(*, code: str) -> tuple[str | None, str | None, Tokens | None]:
    all_tokens: Tokens = {"inputTokens": 0, "outputTokens": 0, "totalTokens": 0}
    abstract_propose_prompt = ConverseMessage(
        content=(
            f"{code}\nWhat is the purpose of the function in the above code snippet? Please"
            " summarize the answer in one sentence using the following format:\n\n"
            '"Function purpose: <summary>"\nMake sure to describe the primary role of'
            " the function while considering all operations it performs, such as"
            " encryption/decryption, logging, external method calls, data processing,"
            " or returning values. Focus only on functional aspects, not security vulnerabilities."
        ),
        role="user",
    )
    detailed_behavior_prompt = ConverseMessage(
        content=(
            f"{code}\nPlease summarize the functions of the above code snippet by listing"
            " all notable functionalities it performs. Include operations such as"
            " encryption/decryption, key or IV handling, I/O processing, method calls, logging,"
            " exception handling, or any other relevant behaviors. Use the following format:\n\n"
            '"The functions of the code snippet are:\n1. <functionality>\n2. <functionality>\n3.'
            ' <functionality>\n..."\n\nDo not evaluate security aspects or potential'
            " vulnerabilities; only describe the functional behavior of the code."
        ),
        role="user",
    )
    response, tokens = await converse_plain(
        messages=[abstract_propose_prompt], model="us.amazon.nova-pro-v1:0"
    )
    if not response or not tokens:
        return None, None, None
    all_tokens = {key: value + tokens[key] for key, value in all_tokens.items()}  # type: ignore
    abstract_propose = response

    response, tokens = await converse_plain(
        messages=[detailed_behavior_prompt], model="us.amazon.nova-pro-v1:0"
    )
    if not response or not tokens:
        return None, None, None
    detailed_behavior = response
    all_tokens = {key: value + tokens[key] for key, value in all_tokens.items()}  # type: ignore
    return abstract_propose, detailed_behavior, all_tokens


async def store_in_open_search(loaders: Dataloaders, vuln_id: str, snippet: Snippet) -> None:
    if not snippet.is_function:
        return
    vo = AsyncClient(api_key=FI_VOYAGE_API_KEY)
    open_client = await get_client()
    with suppress(NotFoundError):
        await open_client.get("vulnerabilities_candidates", vuln_id)
        return
    vuln = await get_vulnerability(loaders, vuln_id)
    group = await loaders.group.load(vuln.group_name)
    if not group:
        return
    finding = await get_finding(loaders, vuln.finding_id)

    vulnerable_line_content = get_vulnerable_line_content(snippet)
    abstract_propose, detailed_behavior, _ = await get_functional_semantics(code=snippet.content)
    if not abstract_propose or not detailed_behavior:
        return

    snippet_embedding = (
        await retry_on_exceptions(
            exceptions=(VoyageError, UnboundLocalError),
            sleep_seconds=10,
        )(vo.embed)(texts=[snippet.content], model="voyage-code-3")
    ).embeddings[0]
    embeddings = await retry_on_exceptions(
        exceptions=(VoyageError, UnboundLocalError),
        sleep_seconds=10,
    )(vo.embed)(
        texts=[abstract_propose, detailed_behavior],
        model="voyage-3",
    )
    doc_body = {
        "metadata": {
            "organization_id": group.organization_id,
            "vulnerability_id": vuln_id,
            "group": vuln.group_name,
            "finding_id": vuln.finding_id,
            "finding_title": finding.title,
            "criteria_code": finding.get_criteria_code(),
            "specific": vuln.state.specific,
            "where": vuln.state.where,
            "vulnerable_function_code_hash": hashlib.sha3_256(
                snippet.content.encode("utf-8")
            ).hexdigest(),
        },
        "vulnerable_function_code": snippet.content,
        "vulnerable_line_content": vulnerable_line_content,
        "abstract_propose": abstract_propose,
        "detailed_behavior": detailed_behavior,
        "embeddings": {
            "vulnerable_function": snippet_embedding,
            "abstract_propose": embeddings.embeddings[0],
            "detailed_behavior": embeddings.embeddings[1],
        },
    }
    await open_client.index(
        index="vulnerabilities_candidates",
        id=vuln_id,
        body=doc_body,
        timeout=60,
    )


@retry_on_exceptions(exceptions=(VulnNotFound,), max_attempts=3)
async def set_snippet(
    *,
    contents: Snippet,
    vulnerability_id: str,
    loaders: Dataloaders | None = None,
) -> None:
    loaders = loaders or get_new_context()
    try:
        vulnerability = await get_vulnerability(loaders, vulnerability_id, clear_loader=True)
        await add_vulnerability_snippet(
            vulnerability_id=vulnerability.id,
            snippet=contents,
            state_date=vulnerability.state.modified_date,
        )
        asyncio.create_task(  # noqa: RUF006
            store_in_open_search(
                loaders,
                vulnerability_id,
                contents,
            )
        )
    except (ValidationException, VulnNotFound) as ex:
        LOGGER.error(
            "Failed to set vulnerability snippet",
            extra={
                "extra": {
                    "vulnerability_id": vulnerability_id,
                    "ex": str(ex),
                },
            },
        )


def generate_snippet(
    vulnerability_state: VulnerabilityState, repo: Repo, force: bool = False
) -> Snippet | None:
    current_commit = vulnerability_state.commit or "HEAD"
    snippet: Snippet | None = None
    with suppress(GitCommandError, ValueError):
        content = repo.git.show(f"{current_commit}:{vulnerability_state.where}")
        snippet = make_snippet_function(
            file_content=content,
            file_path=vulnerability_state.where,
            viewport=SnippetViewport(
                int(vulnerability_state.specific),
                show_line_numbers=False,
                highlight_line_number=False,
            ),
        )
        if not snippet and force:
            snippet = make_snippet(
                content=content,
                viewport=SnippetViewport(
                    int(vulnerability_state.specific),
                    line_context=8,
                    show_line_numbers=False,
                    highlight_line_number=False,
                ),
            )
    if not snippet:
        return None
    return snippet


async def search_snippet(
    loaders: Dataloaders,
    repo: Repo,
    vulnerability_id: str,
    state_vulnerable: VulnerabilityState,
    state_safe: VulnerabilityState,
) -> Snippet | None:
    language = get_language_from_path(state_safe.where)
    if not language:
        return None
    try:
        file_bytes = repo.git.show(f"{state_safe.commit}:{state_safe.where}").encode("utf-8")
    except (GitCommandError, UnicodeEncodeError):
        return None
    try:
        tree = parse_content_tree_sitter(
            file_bytes,
            language,
        )
    except InvalidFileType:
        return None
    snippet_vulnerable = await loaders.vulnerability_snippet.load(
        (
            vulnerability_id,
            state_vulnerable.modified_date,
        ),
    )
    if (
        not snippet_vulnerable
        or not snippet_vulnerable.function
        or not snippet_vulnerable.function.name
        or not snippet_vulnerable.function.node_type
        or not snippet_vulnerable.function.field_identifier_name
    ):
        return None
    result = query_function_by_name(
        language,
        tree,
        snippet_vulnerable.function.node_type,
        snippet_vulnerable.function.field_identifier_name,
        snippet_vulnerable.function.name,
    )
    if result:
        return extract_snippet_function_from_nodes(
            file_bytes, result, result[0].start_point.row + 1, language=language
        )
    return None
