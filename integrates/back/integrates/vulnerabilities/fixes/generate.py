from collections.abc import AsyncGenerator

from integrates.context import (
    FI_AWS_S3_CONTINUOUS_REPOSITORIES,
    FI_AWS_S3_PATH_PREFIX,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.s3.operations import (
    get_object_content,
)
from integrates.vulnerabilities.fixes.bedrock import (
    converse,
)
from integrates.vulnerabilities.fixes.prompt import (
    generate_custom_fix_prompt,
    generate_suggested_fix_prompt,
)
from integrates.vulnerabilities.fixes.types import (
    FixInputs,
    SuggestedFixFunction,
)
from integrates.vulnerabilities.fixes.utils import (
    get_vulnerability_language,
)
from integrates.vulnerabilities.fixes.validations import (
    get_and_validate_fixable_vulnerability,
)


async def get_suggested_fix(
    *,
    loaders: Dataloaders,
    vulnerable_code: SuggestedFixFunction,
    stream: bool = False,
    feature_preview: bool = False,
) -> AsyncGenerator[str, None] | str | None:
    """Entrypoint function for the suggested fix (add code inline) feature"""
    fix_inputs = await get_and_validate_fixable_vulnerability(
        loaders,
        vulnerable_code.vulnerability_id,
    )

    prompt = generate_suggested_fix_prompt(
        vulnerable_function=vulnerable_code.function,
        finding_code=fix_inputs.finding.title.split(".")[0],
        code_import=vulnerable_code.code_imports,
        vulnerable_line_content=vulnerable_code.vulnerable_line_content,
    )

    suggested_fix = converse(
        prompt,
        feature_preview=feature_preview,
    )
    if stream:
        return suggested_fix

    content_list = []
    async for line in suggested_fix:
        content_list.append(line)
    content = "".join(content_list)

    if content:
        return content
    return None


async def _create_custom_fix(
    fix_inputs: FixInputs,
    *,
    feature_preview: bool,
) -> AsyncGenerator[str, None]:
    object_key = (
        f"{FI_AWS_S3_PATH_PREFIX}{fix_inputs.vulnerability.group_name}"
        f"/{fix_inputs.git_root.state.nickname}/"
        f"{fix_inputs.vulnerability.state.where}"
    )

    prompt = generate_custom_fix_prompt(
        finding_code=fix_inputs.finding.get_criteria_code(),
        file_content=(
            await get_object_content(
                object_key,
                bucket=FI_AWS_S3_CONTINUOUS_REPOSITORIES,
            )
        ).decode(encoding="utf-8"),
        language=get_vulnerability_language(fix_inputs.vulnerability),
        vulnerability_line=int(fix_inputs.vulnerability.state.specific),
    )

    result_str = ""
    async for line in converse(
        prompt,
        feature_preview=feature_preview,
    ):
        result_str += line
        yield line


async def get_custom_fix(
    loaders: Dataloaders,
    vulnerability_id: str,
    *,
    stream: bool = False,
    feature_preview: bool = False,
) -> str | AsyncGenerator[str, None] | None:
    """Entrypoint function for the custom fix (step-by-step guide) feature"""
    fix_inputs = await get_and_validate_fixable_vulnerability(loaders, vulnerability_id)

    result = _create_custom_fix(fix_inputs, feature_preview=feature_preview)
    if stream:
        return result

    content_list = []
    async for line in result:
        content_list.append(line)
    content = "".join(content_list)

    if content:
        return content
    return None
