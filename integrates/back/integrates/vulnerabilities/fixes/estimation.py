import tiktoken

from integrates.vulnerabilities.fixes.types import (
    ConverseMessage,
    GptTokenParameters,
)


def get_token_params() -> GptTokenParameters:
    # Assume a middle point between the o200k encoding and the old encoding
    return GptTokenParameters(
        encoding="cl100k_base",
        tokens_per_message=3,
        tokens_per_name=1,
    )


def get_tokens_per_message(message: ConverseMessage) -> int:
    """
    Return the number of tokens used by a single message.
    While tiktoken is best used to estimate token usage in OpenAI's GPT
    models, it can also give a rough estimate for other models.
    """
    token_params = get_token_params()
    encoding = tiktoken.get_encoding(token_params.encoding)

    num_tokens = token_params.tokens_per_message
    for key, value in message._asdict().items():
        num_tokens += len(encoding.encode(str(value)))
        if key == "role":
            num_tokens += token_params.tokens_per_name
    num_tokens += 3  # every reply is primed with <|start|>assistant<|message|>
    return num_tokens
