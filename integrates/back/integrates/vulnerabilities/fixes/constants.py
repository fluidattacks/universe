REGION = "us-east-1"
# Custom (step by step guide) & Suggested (auto) fix model
BEDROCK_MODEL_ID = "us.anthropic.claude-3-5-sonnet-20241022-v2:0"
# Feature preview model for custom & suggested fixes
EXPERIMENTAL_MODEL_ID = "us.anthropic.claude-3-5-sonnet-20241022-v2:0"
