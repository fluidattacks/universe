from integrates.custom_utils.criteria import (
    CRITERIA_VULNERABILITIES,
)
from integrates.vulnerabilities.fixes.context import (
    generate_vuln_context,
    get_vuln_criteria,
    get_vuln_line,
)
from integrates.vulnerabilities.fixes.snippet import (
    get_vuln_snippet,
)
from integrates.vulnerabilities.fixes.types import (
    ConverseMessage,
)


def generate_custom_fix_prompt(
    finding_code: str,
    file_content: str,
    language: str,
    vulnerability_line: int,
) -> tuple[ConverseMessage, ...]:
    vuln_criteria = get_vuln_criteria(CRITERIA_VULNERABILITIES, finding_code)
    line = get_vuln_line(file_content, vulnerability_line)
    vulnerability_context = generate_vuln_context(vuln_criteria)
    vulnerability_snippet = get_vuln_snippet(file_content, vulnerability_line, language, force=True)

    return (
        ConverseMessage(
            content=(
                "You are an assistant who helps resolve vulnerabilities in "
                "code with a step-by-step guide"
            ),
            role="system",
        ),
        ConverseMessage(content=vulnerability_context, role="user"),
        ConverseMessage(
            content="Interesting. In which language is this vulnerability?",
            role="assistant",
        ),
        ConverseMessage(
            content=f"This is a {language} vulnerability",
            role="user",
        ),
        ConverseMessage(
            content="Ok, show me the relevant code snippet",
            role="assistant",
        ),
        ConverseMessage(
            content=(f"Code: <code>\n```{language}\n{vulnerability_snippet}```" "</code>"),
            role="user",
        ),
        ConverseMessage(
            content="And where is the vulnerability?",
            role="assistant",
        ),
        ConverseMessage(
            content=(f"The vulnerability is in line `{line}` of that" " code snippet"),
            role="user",
        ),
        ConverseMessage(
            content="Alright. What do you want me to do in detail?",
            role="assistant",
        ),
        ConverseMessage(
            content=(
                "# Requirements\n"
                "- Help me solve the vulnerability step by step using the"
                " most secure alternative\n"
                "- In each step you modify a portion of the code\n"
                "- Do not return the completely modified code\n"
                "- Use markdown syntax to generate your response\n"
                "- Do not forget about closing multiline code tags. Remember,"
                " I need a nicely formatted guide\n"
                "- Do not leave comments within the code that I must"
                " complete\n"
                "- Do not restate the steps at the end, avoid redundancy\n"
                "- Implement all required methods"
            ),
            role="user",
        ),
        ConverseMessage(
            content="Certainly!",
            role="assistant",
        ),
    )


def generate_suggested_fix_prompt(
    *,
    finding_code: str,
    vulnerable_function: str,
    vulnerable_line_content: str,
    code_import: str | None = None,
) -> tuple[ConverseMessage, ...]:
    vuln_criteria = get_vuln_criteria(CRITERIA_VULNERABILITIES, finding_code)
    vulnerability_context = generate_vuln_context(vuln_criteria)

    return (
        ConverseMessage(
            content=(
                "You are an assistant who helps resolve vulnerabilities "
                "by rewriting vulnerable code and returning drop-in-place "
                "fixed code. The code should implement all needed "
                "adjustments to remediate the vulnerability"
            ),
            role="system",
        ),
        ConverseMessage(
            content=vulnerability_context,
            role="user",
        ),
        ConverseMessage(
            content="Ok, show me the vulnerable code snippet",
            role="assistant",
        ),
        ConverseMessage(
            content=(f"Vulnerable function: <code>\n{vulnerable_function}\n</code>"),
            role="user",
        ),
        *(
            (
                ConverseMessage(
                    content="Does this function use any imports?",
                    role="assistant",
                ),
                ConverseMessage(
                    content=(
                        "Imports available in the module in which "
                        "the function is located\n"
                        + "<imports>\n"
                        + "\n".join(code_import)
                        + "\n</imports>"
                    ),
                    role="user",
                ),
            )
            if code_import
            else tuple()
        ),
        ConverseMessage(
            content="And where in the code is this vulnerability?",
            role="assistant",
        ),
        ConverseMessage(
            content=(
                "The vulnerability is in the line that contains this"
                " fragment of code: <vulnerability>"
                f"\n{vulnerable_line_content}\n</vulnerability>"
            ),
            role="user",
        ),
        ConverseMessage(
            content="Got it. What do you want me to do in detail?",
            role="assistant",
        ),
        ConverseMessage(
            content=(
                "# Requirements\n"
                "- Fix the code using the most secure alternative\n"
                "- Rewrite the new complete version of the function inside "
                "the <function></function> xml tags, do not use markdown "
                "Fenced Code Blocks. Your answer will be used to replace"
                " the function automatically, if you omit code the function "
                " will not work as expected\n"
                "- You must leave the function signature intact\n"
                "- You should not be brief \n"
                "- Do not leave comments within the code that I must complete"
                " manually \n"
                "- Implement all newly required methods in the steps\n"
                "- Write the new imports in the"
                " <imports></import> xml tag, outside of <function> tags\n"
                "- Avoid creating new classes\n"
            ),
            role="user",
        ),
    )
