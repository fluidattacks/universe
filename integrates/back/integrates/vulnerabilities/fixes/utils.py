from pathlib import (
    Path,
)

from fluidattacks_core.serializers.syntax import (
    TREE_SITTER_SUPPORTED_LANGUAGES,
)

from integrates.custom_exceptions import (
    InvalidFileType,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
)


def get_vulnerability_language(vulnerability: Vulnerability) -> str:
    language = TREE_SITTER_SUPPORTED_LANGUAGES.get(Path(vulnerability.state.where).suffix, "")
    language = language or TREE_SITTER_SUPPORTED_LANGUAGES.get(
        Path(vulnerability.state.where).name, ""
    )
    if not language:
        raise InvalidFileType(detail="file language must be selectable")
    return language
