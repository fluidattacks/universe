from decimal import Decimal
from typing import (
    NamedTuple,
)

from integrates.db_model.enums import (
    Source,
)


class SeverityLevelCount(NamedTuple):
    critical: int
    high: int
    medium: int
    low: int
    total: int


class GroupedVulnerabilitiesInfo(NamedTuple):
    commit_hash: str | None
    severity_score: Decimal
    specific: str
    where: str


class FindingGroupedVulnerabilitiesInfo(NamedTuple):
    grouped_ports_vulnerabilities: tuple[GroupedVulnerabilitiesInfo, ...]
    grouped_lines_vulnerabilities: tuple[GroupedVulnerabilitiesInfo, ...]
    grouped_inputs_vulnerabilities: tuple[GroupedVulnerabilitiesInfo, ...]
    where: str


class Treatments(NamedTuple):
    accepted: int
    accepted_undefined: int
    in_progress: int
    untreated: int


class Verifications(NamedTuple):
    requested: int
    on_hold: int
    verified: int


class VulnerabilityDescriptionToUpdate(NamedTuple):
    commit: str | None = None
    source: Source | None = None
    where: str | None = None
    specific: str | None = None


class ZeroRisk(NamedTuple):
    confirmed: int
    rejected: int
    requested: int


ToolItem = dict[str, str]
