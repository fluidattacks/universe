import logging
import logging.config
import uuid
from datetime import (
    datetime,
)
from typing import (
    Any,
)

from aioextensions import (
    collect,
    schedule,
)
from httpagentparser import (
    simple_detect,
)
from starlette.requests import (
    Request,
)

from integrates.audit import AuditEvent, add_audit_event
from integrates.authz.validations import (
    validate_fluidattacks_staff_on_group_deco,
    validate_role_fluid_reqs_deco,
)
from integrates.context import (
    FI_MAIL_CXO,
)
from integrates.custom_exceptions import (
    InvalidExpirationTime,
    InvalidField,
    InvalidFieldLength,
    MailerClientError,
    OrganizationNotFound,
    RequiredNewPhoneNumber,
    RequiredVerificationCode,
    SamePhoneNumber,
    StakeholderNotFound,
    StakeholderPhoneNumberNotFound,
    TokenCouldNotBeAdded,
    TokenNotFound,
    TrustedDeviceNotFound,
    UnableToSendMail,
)
from integrates.custom_utils import (
    analytics as analytics_utils,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.custom_utils.access import get_stakeholder_groups_names
from integrates.custom_utils.stakeholders import get_stakeholder
from integrates.custom_utils.validations_deco import (
    validate_alphanumeric_field_deco,
    validate_email_address_deco,
    validate_fields_deco,
    validate_fields_length_deco,
    validate_length_deco,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    stakeholders as stakeholders_model,
)
from integrates.db_model import (
    trials as trials_model,
)
from integrates.db_model import (
    vulnerabilities as vulns_model,
)
from integrates.db_model.group_access.types import (
    GroupAccessMetadataToUpdate,
    GroupInvitation,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.db_model.stakeholders.types import (
    AccessTokens,
    NotificationsPreferences,
    StakeholderLogin,
    StakeholderMetadataToUpdate,
    StakeholderPhone,
    StakeholderState,
    StakeholderStateToUpdate,
    StakeholderTours,
    TrustedDevice,
)
from integrates.db_model.trials.types import (
    Trial,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.group_access import (
    domain as group_access_domain,
)
from integrates.mailer.trial import (
    send_mail_free_trial_start,
)
from integrates.organization_access import (
    domain as org_access_domain,
)
from integrates.sessions import (
    domain as sessions_domain,
)
from integrates.sessions import (
    utils as sessions_utils,
)
from integrates.settings import (
    LOGGING,
)
from integrates.stakeholders.utils import (
    get_international_format_phone_number,
)
from integrates.stakeholders.validations import (
    validate_phone_deco,
)
from integrates.trials.domain import (
    should_have_free_trial,
)
from integrates.verify import (
    operations as verify_operations,
)
from integrates.verify.enums import (
    Channel,
)

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger(__name__)
EMAIL_INTEGRATES = "integrates@fluidattacks.com"


async def acknowledge_concurrent_session(email: str) -> None:
    """Acknowledge termination of concurrent session."""
    await stakeholders_model.update_metadata(
        metadata=StakeholderMetadataToUpdate(
            is_concurrent_session=False,
        ),
        email=email,
    )


async def remove(email: str) -> None:
    loaders: Dataloaders = get_new_context()
    me_vulnerabilities = await loaders.me_vulnerabilities.load(email)
    await collect(
        tuple(
            vulns_model.update_assigned_index(
                finding_id=vulnerability.finding_id,
                vulnerability_id=vulnerability.id,
                entry=None,
            )
            for vulnerability in me_vulnerabilities
        ),
        workers=8,
    )
    await stakeholders_model.remove(email=email)
    LOGGER.info(
        "Stakeholder removed from db",
        extra={"extra": {"email": email}},
    )


@validate_length_deco("responsibility", max_length=50)
@validate_alphanumeric_field_deco("responsibility")
async def _update_information(
    *,
    context: Any,
    email: str,
    group_name: str,
    responsibility: str,
    role: str,
    modified_by: str,
) -> None:
    group_access = await group_access_domain.get_group_access(
        loaders=context.loaders,
        group_name=group_name,
        email=email,
    )
    await group_access_domain.update(
        loaders=context.loaders,
        email=email,
        group_name=group_name,
        metadata=GroupAccessMetadataToUpdate(
            state=group_access.state._replace(
                modified_by=modified_by,
                modified_date=datetime_utils.get_utc_now(),
                responsibility=responsibility,
                role=role,
            ),
        ),
    )


async def update_information(
    context: Any,
    modified_data: dict[str, str],
    group_name: str,
    modified_by: str,
) -> None:
    email = modified_data["email"]
    responsibility = modified_data["responsibility"]
    role = modified_data["role"]
    if responsibility:
        try:
            await _update_information(
                context=context,
                email=email,
                group_name=group_name,
                responsibility=responsibility,
                role=role,
                modified_by=modified_by,
            )
        except (InvalidFieldLength, InvalidField) as exc:
            logs_utils.cloudwatch_log(
                context,
                "Attempted to add responsibility to the group bypassing validation",
                extra={
                    "modified_email": email,
                    "group_name": group_name,
                    "log_type": "Security",
                },
            )
            raise exc


async def register(email: str) -> None:
    await stakeholders_model.update_metadata(
        metadata=StakeholderMetadataToUpdate(is_registered=True),
        email=email,
    )


async def register_login(
    user_email: str,
    subject: str,
    request: Request,
) -> None:
    device, browser = simple_detect(request.headers.get("User-Agent"))
    provider = subject.split("#")[0].upper()
    await stakeholders_model.update_login(
        user_email=user_email,
        login=StakeholderLogin(
            modified_by=EMAIL_INTEGRATES,
            modified_date=datetime_utils.get_now(),
            expiration_time=datetime_utils.get_as_epoch(
                datetime_utils.get_now_plus_delta(days=365 * 10),
            ),
            browser=browser,
            device=device,
            ip_address=request.headers.get(
                "cf-connecting-ip",
                request.client.host if request.client else "",
            ),
            country_code=request.headers.get("cf-ipcountry", ""),
            provider=provider,
            subject=subject,
        ),
    )


async def remove_access_token(
    email: str,
    loaders: Dataloaders,
    token_id: str | None = None,
) -> None:
    """Remove access token attribute"""
    if token_id:
        loaders.stakeholder.clear(email)
        stakeholder = await get_stakeholder(loaders, email)
        if next(
            (token for token in stakeholder.access_tokens if token.id == token_id),
            None,
        ):
            await stakeholders_model.update_metadata(
                metadata=StakeholderMetadataToUpdate(
                    access_tokens=[
                        token for token in stakeholder.access_tokens if token.id != token_id
                    ],
                ),
                email=email,
            )
            return

        raise TokenNotFound()

    await stakeholders_model.update_metadata(
        metadata=StakeholderMetadataToUpdate(
            access_tokens=[],
        ),
        email=email,
    )


async def _get_access_tokens(
    *,
    email: str,
    loaders: Dataloaders,
    iat: int,
    name: str,
    token_data: dict[str, str],
    many_tokens: bool,
) -> list[AccessTokens]:
    new_token = [
        AccessTokens(
            id=str(uuid.uuid4()),
            issued_at=iat,
            name=name,
            jti_hashed=token_data["jti_hashed"],
            salt=token_data["salt"],
        ),
    ]
    if not many_tokens:
        return new_token

    loaders.stakeholder.clear(email)
    stakeholder = await get_stakeholder(loaders, email)
    if len(stakeholder.access_tokens) >= 2:
        raise TokenCouldNotBeAdded()

    return stakeholder.access_tokens + new_token


@validate_fields_deco(["name"])
@validate_fields_length_deco(
    ["name"],
    min_length=1,
    max_length=20,
)
async def update_access_token(
    *,
    email: str,
    expiration_time: int,
    loaders: Dataloaders,
    name: str,
    many_tokens: bool = False,
    **kwargs_token: Any,
) -> str:
    """Update access token"""
    token_data = sessions_utils.calculate_hash_token()
    session_jwt = ""

    if sessions_utils.is_valid_expiration_time(expiration_time):
        iat = int(datetime.utcnow().timestamp())
        session_jwt = sessions_domain.encode_token(
            expiration_time=expiration_time,
            payload={
                "user_email": email,
                "jti": token_data["jti"],
                "iat": iat,
                **kwargs_token,
            },
            subject="api_token",
            api=True,
        )
        await stakeholders_model.update_metadata(
            metadata=StakeholderMetadataToUpdate(
                access_tokens=await _get_access_tokens(
                    email=email,
                    loaders=loaders,
                    iat=iat,
                    name=name,
                    token_data=token_data,
                    many_tokens=many_tokens,
                ),
            ),
            email=email,
        )
    else:
        raise InvalidExpirationTime()

    return session_jwt


async def update_legal_remember(email: str, remember: bool) -> None:
    """Remember legal notice acceptance."""
    return await stakeholders_model.update_metadata(
        metadata=StakeholderMetadataToUpdate(
            legal_remember=remember,
        ),
        email=email,
    )


async def link_subject(email: str, subject: str) -> None:
    await stakeholders_model.update_metadata(
        metadata=StakeholderMetadataToUpdate(subject=subject),
        email=email,
    )


async def update_last_login(email: str) -> None:
    return await stakeholders_model.update_metadata(
        metadata=StakeholderMetadataToUpdate(
            last_login_date=datetime_utils.get_utc_now(),
        ),
        email=email,
    )


async def update_notification_preferences(
    loaders: Dataloaders,
    email: str,
    preferences: NotificationsPreferences,
) -> None:
    stakeholder = await get_stakeholder(loaders, email)
    attributes_to_update = StakeholderStateToUpdate(notifications_preferences=preferences)
    await update_state(
        current_value=stakeholder.state,
        attributes=attributes_to_update,
        modified_by=email.lower().strip(),
    )


def match_trusted_devices(new_trust_device: TrustedDevice, trusted_device: TrustedDevice) -> bool:
    return (
        trusted_device.device == new_trust_device.device
        and trusted_device.browser == new_trust_device.browser
        and trusted_device.otp_token_jti == new_trust_device.otp_token_jti
    )


def update_trust_device(
    current_trusted_devices: list[TrustedDevice],
    trusted_device: TrustedDevice,
    is_last_attempt: bool | None = None,
) -> list[TrustedDevice]:
    device_exists = any(
        match_trusted_devices(trusted_device, device) for device in current_trusted_devices
    )

    if device_exists:
        return [
            trusted_device._replace(
                last_attempt=(
                    trusted_device.last_attempt if is_last_attempt else device.last_attempt
                ),
                first_login_date=device.first_login_date,
            )
            if match_trusted_devices(trusted_device, device)
            else device
            for device in current_trusted_devices
        ]

    return current_trusted_devices + [trusted_device]


def remove_trust_device_from_list(
    current_trusted_devices: list[TrustedDevice],
    trusted_device: TrustedDevice,
) -> list[TrustedDevice]:
    device_exists = any(
        match_trusted_devices(trusted_device, device) for device in current_trusted_devices
    )

    if device_exists:
        return [
            device
            for device in current_trusted_devices
            if not match_trusted_devices(trusted_device, device)
        ]

    raise TrustedDeviceNotFound()


async def get_trust_device(
    *,
    loaders: Dataloaders,
    email: str,
    trusted_device: TrustedDevice,
) -> TrustedDevice | None:
    stakeholder = await get_stakeholder(loaders, email)
    for device in stakeholder.state.trusted_devices:
        if match_trusted_devices(trusted_device, device):
            return device
    return None


@validate_length_deco("responsibility", max_length=50)
@validate_alphanumeric_field_deco("responsibility")
@validate_email_address_deco("email")
@validate_role_fluid_reqs_deco("email", "role")
@validate_fluidattacks_staff_on_group_deco("group", "email", "role")
async def update_invited_stakeholder(
    *,
    loaders: Dataloaders,
    email: str,
    responsibility: str,
    role: str,
    invitation: GroupInvitation,
    group: Group,
    modified_by: str,
) -> None:
    group_access = await group_access_domain.get_group_access(loaders, group.name, email)
    new_invitation = invitation._replace(responsibility=responsibility, role=role)
    await group_access_domain.update(
        loaders=loaders,
        email=email,
        group_name=group.name,
        metadata=GroupAccessMetadataToUpdate(
            state=group_access.state._replace(
                modified_by=modified_by,
                modified_date=datetime_utils.get_utc_now(),
                invitation=new_invitation,
                responsibility=responsibility,
                role=role,
            ),
        ),
    )


async def update(*, email: str, metadata: StakeholderMetadataToUpdate) -> None:
    return await stakeholders_model.update_metadata(email=email, metadata=metadata)


mail_free_trial_start = retry_on_exceptions(
    exceptions=(UnableToSendMail, MailerClientError),
    max_attempts=4,
    sleep_seconds=2,
)(send_mail_free_trial_start)


async def add_experience_manager(
    loaders: Dataloaders,
    organization_id: str,
    group_name: str,
) -> None:
    role = "customer_manager"
    await org_access_domain.add_access(
        loaders,
        organization_id,
        FI_MAIL_CXO,
        role,
        EMAIL_INTEGRATES,
    )
    await group_access_domain.add_access(loaders, FI_MAIL_CXO, group_name, role, EMAIL_INTEGRATES)


@validate_email_address_deco("user_email")
async def add_enrollment(
    *,
    loaders: Dataloaders,
    user_email: str,
    full_name: str,
) -> None:
    if await should_have_free_trial(email=user_email, loaders=loaders):
        await trials_model.add(
            trial=Trial(
                completed=False,
                email=user_email,
                extension_date=None,
                extension_days=0,
                start_date=datetime_utils.get_utc_now(),
                reason=None,
            ),
        )

    await update(
        email=user_email,
        metadata=StakeholderMetadataToUpdate(enrolled=True),
    )
    stakeholder_orgs = await loaders.stakeholder_organizations_access.load(user_email)
    organization_id = stakeholder_orgs[0].organization_id
    organization = await loaders.organization.load(organization_id)
    if not organization:
        raise OrganizationNotFound()

    group_name = (await get_stakeholder_groups_names(loaders, user_email, True))[0]

    schedule(mail_free_trial_start(loaders, user_email, full_name, group_name))
    schedule(add_experience_manager(loaders, organization_id, group_name))
    # Fallback event
    await analytics_utils.mixpanel_track(
        user_email,
        "AutoenrollSubmit",
        group=group_name,
        mp_country_code=organization.country,
        organization=organization.name,
        User=full_name,
    )
    add_audit_event(
        AuditEvent(
            author=user_email,
            action="READ",
            metadata={},
            object="Autoenroll.Submit",
            object_id="unknown",
        )
    )


@validate_phone_deco("new_phone")
async def update_mobile(*, email: str, new_phone: StakeholderPhone, verification_code: str) -> None:
    """Update the stakeholder's phone number."""
    await verify_operations.validate_mobile(
        phone_number=get_international_format_phone_number(new_phone),
    )
    country_code = await verify_operations.get_country_code(
        get_international_format_phone_number(new_phone),
    )
    await verify_operations.check_verification(
        recipient=get_international_format_phone_number(new_phone),
        code=verification_code,
    )
    stakeholder_phone = StakeholderPhone(
        calling_country_code=new_phone.calling_country_code,
        country_code=country_code,
        national_number=new_phone.national_number,
    )
    return await stakeholders_model.update_metadata(
        metadata=StakeholderMetadataToUpdate(
            phone=stakeholder_phone,
        ),
        email=email,
    )


async def update_tours(email: str, tours: dict[str, bool]) -> None:
    """New stakeholder workflow acknowledgment."""
    return await stakeholders_model.update_metadata(
        metadata=StakeholderMetadataToUpdate(
            tours=StakeholderTours(
                new_group=tours["new_group"],
                new_root=tours["new_root"],
                welcome=tours["welcome"],
            ),
        ),
        email=email,
    )


@validate_phone_deco("new_phone")
async def verify(
    *,
    channel: Channel,
    loaders: Dataloaders,
    email: str,
    new_phone: StakeholderPhone | None,
    verification_code: str | None,
) -> None:
    """Start a verification process using OTP"""
    stakeholder = await get_stakeholder(loaders, email)
    stakeholder_phone: StakeholderPhone | None = stakeholder.phone
    phone_to_verify = stakeholder_phone if new_phone is None else new_phone

    if channel == Channel.EMAIL:
        await verify_operations.start_verification(
            recipient=email,
            channel=channel,
        )
        return

    if not phone_to_verify:
        raise RequiredNewPhoneNumber()

    if (
        stakeholder_phone is not None
        and new_phone is not None
        and get_international_format_phone_number(stakeholder_phone)
        == get_international_format_phone_number(new_phone)
    ):
        raise SamePhoneNumber()

    if new_phone and phone_to_verify is new_phone:
        await verify_operations.validate_mobile(
            phone_number=get_international_format_phone_number(new_phone),
        )

    if phone_to_verify is new_phone and stakeholder_phone is not None:
        if verification_code is None:
            raise RequiredVerificationCode()

        await verify_operations.check_verification(
            recipient=get_international_format_phone_number(stakeholder_phone),
            code=verification_code,
        )

    await verify_operations.start_verification(
        recipient=get_international_format_phone_number(phone_to_verify),
        channel=channel,
    )


async def exists(loaders: Dataloaders, email: str) -> bool:
    if await loaders.stakeholder.load(email):
        return True
    return False


async def update_state(
    *,
    current_value: StakeholderState,
    attributes: StakeholderStateToUpdate,
    modified_by: str,
    is_last_attempt: bool = False,
) -> None:
    modified_date = datetime_utils.get_utc_now()
    trusted_devices = current_value.trusted_devices

    if trusted_device := attributes.trusted_device:
        trusted_device_format = TrustedDevice(
            ip_address=trusted_device.ip_address,
            device=trusted_device.device,
            browser=trusted_device.browser,
            location=trusted_device.location,
            otp_token_jti=trusted_device.otp_token_jti,
            first_login_date=modified_date,
            last_attempt=modified_date,
            last_login_date=modified_date,
        )
        trusted_devices = update_trust_device(
            current_value.trusted_devices,
            trusted_device_format,
            is_last_attempt,
        )

    new_state = StakeholderState(
        modified_by=modified_by,
        modified_date=datetime_utils.get_utc_now(),
        notifications_preferences=attributes.notifications_preferences
        or current_value.notifications_preferences,
        trusted_devices=trusted_devices,
    )

    await stakeholders_model.update_state(
        user_email=modified_by,
        state=new_state,
    )


async def remove_trusted_device(
    *,
    loaders: Dataloaders,
    trusted_device: TrustedDevice,
    user_email: str,
) -> None:
    stakeholder = await get_stakeholder(loaders, user_email)

    trusted_devices = remove_trust_device_from_list(
        stakeholder.state.trusted_devices,
        trusted_device,
    )

    updated_state = StakeholderState(
        modified_by=user_email,
        modified_date=datetime_utils.get_utc_now(),
        notifications_preferences=stakeholder.state.notifications_preferences,
        trusted_devices=trusted_devices,
    )

    await stakeholders_model.update_state(
        user_email=user_email,
        state=updated_state,
    )


async def update_trusted_devices(
    *,
    loaders: Dataloaders,
    trusted_device: TrustedDevice,
    user_email: str,
    is_last_attempt: bool = True,
) -> None:
    stakeholder = await get_stakeholder(loaders, user_email)

    attributes_to_update = StakeholderStateToUpdate(trusted_device=trusted_device)

    await update_state(
        current_value=stakeholder.state,
        attributes=attributes_to_update,
        is_last_attempt=is_last_attempt,
        modified_by=user_email,
    )


async def get_stakeholder_phone_number(loaders: Dataloaders, user_email: str) -> str:
    stakeholder = await loaders.stakeholder.load(user_email)
    if not stakeholder:
        raise StakeholderNotFound()

    user_phone = stakeholder.phone
    if not user_phone:
        raise StakeholderPhoneNumberNotFound()

    return get_international_format_phone_number(user_phone)


async def get_masked_phone_number(loaders: Dataloaders, user_email: str) -> str:
    try:
        phone_number = await get_stakeholder_phone_number(loaders, user_email)
        masked = "*" * (len(phone_number) - 2) + phone_number[-2:]
        return masked
    except (StakeholderPhoneNumberNotFound, StakeholderNotFound):
        return ""
