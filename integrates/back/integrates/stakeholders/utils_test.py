from integrates.dataloaders import get_new_context
from integrates.db_model.stakeholders.types import StakeholderPhone
from integrates.stakeholders.utils import Phone, get_international_format_phone_number
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import EMAIL_GENERIC, StakeholderFaker
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize

STAKEHOLDER_WITH_FIXED_PHONE = StakeholderPhone(
    country_code="US",
    calling_country_code="1",
    national_number="5551234567",
)


@parametrize(
    args=["mobile", "expected_output"],
    cases=[
        [
            Phone(
                calling_country_code="57",
                national_number="3007654321",
            ),
            "+573007654321",
        ],
        [
            STAKEHOLDER_WITH_FIXED_PHONE,
            "+15551234567",
        ],
    ],
)
async def test_get_international_format_phone_number(
    mobile: Phone | StakeholderPhone,
    expected_output: str,
) -> None:
    assert get_international_format_phone_number(mobile) == expected_output


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[StakeholderFaker(phone=STAKEHOLDER_WITH_FIXED_PHONE)]
        )
    )
)
async def test_get_international_format_phone_number_fromdb() -> None:
    loaders = get_new_context()
    stakeholder = await loaders.stakeholder.load(EMAIL_GENERIC)

    if stakeholder and stakeholder.phone:
        assert get_international_format_phone_number(stakeholder.phone) == "+15551234567"
