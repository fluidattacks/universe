import logging
import logging.config
from collections.abc import (
    Iterable,
)
from datetime import (
    datetime,
)
from time import (
    time,
)

from aioextensions import (
    collect,
    schedule,
)

from integrates.context import (
    FI_ENVIRONMENT,
)
from integrates.custom_exceptions import (
    ReattackNotRequested,
    VulnNotFound,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.custom_utils import (
    validations,
    validations_deco,
)
from integrates.custom_utils import (
    vulnerabilities as vulns_utils,
)
from integrates.custom_utils.findings import get_finding
from integrates.custom_utils.vulnerabilities import (
    is_machine_vuln,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model import (
    findings as findings_model,
)
from integrates.db_model.finding_comments.enums import (
    CommentType,
)
from integrates.db_model.finding_comments.types import (
    FindingComment,
)
from integrates.db_model.findings.enums import (
    FindingVerificationStatus,
)
from integrates.db_model.findings.types import (
    Finding,
    FindingVerification,
)
from integrates.db_model.types import (
    Treatment,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateReason,
    VulnerabilityStateStatus,
)
from integrates.db_model.vulnerabilities.types import (
    FindingVulnerabilitiesRequest,
    FindingVulnerabilitiesZrRequest,
    VulnerabilitiesConnection,
    Vulnerability,
    VulnerabilityState,
)
from integrates.finding_comments import (
    domain as comments_domain,
)
from integrates.findings.domain import (
    core as finding_domain,
)
from integrates.findings.types import (
    Tracking,
)
from integrates.jobs_orchestration.jobs import (
    get_finding_code_from_title,
    get_techniques_to_execute,
    queue_machine_reattack,
)
from integrates.mailer import (
    findings as findings_mail,
)
from integrates.settings import (
    LOGGING,
)
from integrates.vulnerabilities import (
    domain as vulns_domain,
)
from integrates.vulnerabilities.domain.utils import (
    get_vulnerabilities_paths_by_root,
)

from .utils import (
    get_vulnerabilities,
    validate_vulnerabilities,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)


async def get_closed_vulnerabilities(
    loaders: Dataloaders,
    finding_id: str,
) -> list[Vulnerability]:
    connection: VulnerabilitiesConnection = (
        await loaders.finding_vulnerabilities_released_nzr_c.load(
            FindingVulnerabilitiesZrRequest(
                finding_id=finding_id,
                paginate=False,
                state_status=VulnerabilityStateStatus.SAFE,
            ),
        )
    )

    return [edge.node for edge in connection.edges]


async def get_closed_vulnerabilities_len(
    loaders: Dataloaders,
    finding_id: str,
) -> int:
    return len(await get_closed_vulnerabilities(loaders, finding_id))


async def get_submitted_vulnerabilities(loaders: Dataloaders, finding_id: str) -> int:
    drafts = await loaders.finding_vulnerabilities_draft_c.load_nodes(
        FindingVulnerabilitiesRequest(finding_id=finding_id),
    )
    return len(
        [draft for draft in drafts if draft.state.status is VulnerabilityStateStatus.SUBMITTED],
    )


def get_tracking_vulnerabilities(
    vulns_state: Iterable[Iterable[VulnerabilityState]],
    vulns_treatment: Iterable[Iterable[Treatment]],
) -> list[Tracking]:
    """Get tracking vulnerabilities dictionary."""
    states_actions = vulns_utils.get_state_actions(vulns_state)
    treatments_actions = vulns_utils.get_treatment_actions(vulns_treatment)
    tracking_actions = sorted(
        states_actions + treatments_actions,
        key=lambda action: datetime_utils.get_from_str(action.date, "%Y-%m-%d"),
    )

    return [
        Tracking(
            cycle=index,
            date=action.date,
            accepted=action.times if action.action == "ACCEPTED" else 0,
            accepted_undefined=(action.times if action.action == "ACCEPTED_UNDEFINED" else 0),
            assigned=action.assigned,
            justification=action.justification,
            safe=action.times if action.action == "SAFE" else 0,
            vulnerable=action.times if action.action == "VULNERABLE" else 0,
        )
        for index, action in enumerate(tracking_actions)
    ]


async def update_vulnerabilities_verification_state(
    *,
    loaders: Dataloaders,
    vulnerabilities: list[Vulnerability],
    finding: Finding,
    user_info: dict[str, str],
    justification: str,
    is_closing_event: bool = False,
) -> None:
    comment_id = str(round(time() * 1000))
    user_email = user_info["user_email"]
    requester_email: str = user_info["user_email"]
    if is_closing_event:
        requester_email = (
            vulnerabilities[0].unreliable_indicators.unreliable_last_reattack_requester
            or await vulns_domain.get_reattack_requester(loaders, vulnerabilities[0])
            or user_email
        )
    verification = FindingVerification(
        comment_id=comment_id,
        modified_by=requester_email,
        modified_date=datetime_utils.get_utc_now(),
        status=FindingVerificationStatus.REQUESTED,
        vulnerability_ids=set(vuln.id for vuln in vulnerabilities),
    )
    await findings_model.update_verification(
        current_value=finding.verification,
        group_name=finding.group_name,
        finding_id=finding.id,
        verification=verification,
    )
    comment_data = FindingComment(
        finding_id=finding.id,
        comment_type=CommentType.VERIFICATION,
        content=justification,
        parent_id="0",
        id=comment_id,
        email=user_email,
        full_name=" ".join([user_info["first_name"], user_info["last_name"]]),
        creation_date=datetime_utils.get_utc_now(),
    )
    await comments_domain.add(loaders, comment_data)
    await collect(
        [
            vulns_domain.request_verification(vuln, user_email, justification)
            for vuln in vulnerabilities
        ],
    )


# Validate justification length and vet characters in it
@validations_deco.validate_length_deco("justification", min_length=10, max_length=10000)
@validations_deco.validate_fields_deco(["justification"])
async def request_vulnerabilities_verification(
    *,
    loaders: Dataloaders,
    finding_id: str,
    user_info: dict[str, str],
    justification: str,
    vulnerability_ids: set[str],
    is_closing_event: bool = False,
    no_unsolved_events: bool = False,
) -> list[str]:
    user_email = user_info["user_email"]
    finding = await get_finding(loaders, finding_id)
    all_vulnerabilities = await get_vulnerabilities(loaders, finding_id, vulnerability_ids)
    vulnerabilities = await validate_vulnerabilities(
        loaders=loaders,
        vulnerabilities=all_vulnerabilities,
        group_name=finding.group_name,
        is_closing_event=is_closing_event,
        no_unsolved_events=no_unsolved_events,
        vulnerability_ids=vulnerability_ids,
    )

    if not vulnerabilities:
        LOGGER.error(
            "No valid vulnerabilities found to execute reattack",
            extra={
                "extra": {
                    "finding_id": finding_id,
                    "group_name": finding.group_name,
                    "status": finding.state.status,
                },
            },
        )
        raise ReattackNotRequested()

    machine_vulns = [
        vuln
        for vuln in vulnerabilities
        if is_machine_vuln(vuln) and not finding_domain.check_hold(vuln)
    ]

    if (
        FI_ENVIRONMENT == "production"
        and machine_vulns
        and (finding_code := get_finding_code_from_title(finding.title))
    ):
        queued_execution = await queue_machine_reattack(
            loaders=loaders,
            finding_codes=[finding_code],
            group_name=finding.group_name,
            modified_by=user_email,
            modules_to_execute=get_techniques_to_execute(machine_vulns),
            reported_paths_by_root=await get_vulnerabilities_paths_by_root(
                loaders,
                finding.group_name,
                machine_vulns,
            ),
        )
        if not queued_execution or not queued_execution.batch_job_id:
            LOGGER.error(
                "Unable to queue machine execution for reattack",
                extra={
                    "extra": {
                        "finding_id": finding_id,
                        "group_name": finding.group_name,
                        "status": finding.state.status,
                    },
                },
            )
            raise ReattackNotRequested()

    await update_vulnerabilities_verification_state(
        loaders=loaders,
        vulnerabilities=vulnerabilities,
        finding=finding,
        user_info=user_info,
        justification=justification,
        is_closing_event=is_closing_event,
    )

    if any(not finding_domain.check_hold(vuln) for vuln in vulnerabilities):
        schedule(
            findings_mail.send_mail_remediate_finding(
                loaders=loaders,
                user_email=user_email,
                finding_id=finding.id,
                finding_name=finding.title,
                group_name=finding.group_name,
                justification=justification,
            ),
        )

    return [vuln.id for vuln in vulnerabilities]


async def _add_reattack_justification(
    *,
    open_vulns_ids: list[str],
    closed_vulns_ids: list[str],
    vulnerabilities: list[Vulnerability],
    loaders: Dataloaders,
    finding_id: str,
    modified_by: str,
    user_info: dict[str, str],
    justification: str,
) -> None:
    set_open_vulns_ids = set(open_vulns_ids)
    set_closed_vulns_ids = set(closed_vulns_ids)
    open_vulnerabilities = [vuln for vuln in vulnerabilities if vuln.id in set_open_vulns_ids]
    closed_vulnerabilities = [vuln for vuln in vulnerabilities if vuln.id in set_closed_vulns_ids]
    await finding_domain.add_reattack_justification(
        loaders=loaders,
        finding_id=finding_id,
        open_vulnerabilities=open_vulnerabilities,
        closed_vulnerabilities=closed_vulnerabilities,
        comment_type=CommentType.VERIFICATION,
        email=modified_by,
        full_name=" ".join([user_info["first_name"], user_info["last_name"]]),
        observations=justification,
    )


async def _update_verification(
    *,
    loaders: Dataloaders,
    finding_id: str,
    modified_by: str,
    today: datetime,
    vulnerability_ids: list[str],
) -> None:
    finding = await get_finding(loaders, finding_id)
    comment_id = str(round(time() * 1000))
    # Modify the verification state to mark the finding as verified
    verification = FindingVerification(
        comment_id=comment_id,
        modified_by=modified_by,
        modified_date=today,
        status=FindingVerificationStatus.VERIFIED,
        vulnerability_ids=set(vulnerability_ids),
    )
    await findings_model.update_verification(
        current_value=finding.verification,
        group_name=finding.group_name,
        finding_id=finding.id,
        verification=verification,
    )


# Validate justification length and vet characters in it
@validations_deco.validate_length_deco("justification", min_length=10, max_length=10000)
@validations_deco.validate_fields_deco(["justification"])
async def verify_vulnerabilities(
    *,
    finding_id: str,
    user_info: dict[str, str],
    justification: str,
    open_vulns_ids: list[str],
    closed_vulns_ids: list[str],
    vulns_to_close_from_file: list[Vulnerability],
    loaders: Dataloaders,
    verification_reason: VulnerabilityStateReason,
    is_closing_event: bool = False,
    should_add_reattack_comments: bool = True,
) -> None:
    # All vulns must be open before verifying them
    # we will just keep them open or close them
    # in either case, their historic_verification is updated to VERIFIED
    loaders.finding.clear(finding_id)
    vulnerability_ids = open_vulns_ids + closed_vulns_ids
    vulnerabilities = [
        vuln
        for vuln in await loaders.finding_vulnerabilities_all.load(finding_id)
        if vuln.id in vulnerability_ids
    ]
    # Sometimes vulns on hold end up being closed before the event is solved
    # Therefore, this allows these vulns to be auto-verified when it happens
    if not is_closing_event:
        vulnerabilities = [
            validations.validate_reattack_requested(vuln) for vuln in vulnerabilities
        ]
        vulnerabilities = [validations.validate_closed(vuln) for vuln in vulnerabilities]
    for vuln in vulnerabilities:
        validations.validate_released(vuln)

    if not vulnerabilities:
        raise VulnNotFound()

    today = datetime_utils.get_utc_now()
    modified_by = user_info["user_email"]
    await _update_verification(
        loaders=loaders,
        finding_id=finding_id,
        modified_by=modified_by,
        today=today,
        vulnerability_ids=vulnerability_ids,
    )
    if should_add_reattack_comments:
        await _add_reattack_justification(
            open_vulns_ids=open_vulns_ids,
            closed_vulns_ids=closed_vulns_ids,
            vulnerabilities=vulnerabilities,
            loaders=loaders,
            finding_id=finding_id,
            modified_by=modified_by,
            user_info=user_info,
            justification=justification,
        )
    # Modify the verification state to mark all passed vulns as verified
    await collect(
        [
            vulns_domain.mark_as_verified(vuln, modified_by, justification)
            for vuln in vulnerabilities
        ],
    )
    # Open vulns that remain open are not modified in the DB
    # Open vulns that were closed must be persisted to the DB as closed
    await vulns_domain.mark_as_safe(
        user_info=user_info,
        loaders=loaders,
        modified_date=today,
        closed_vulns_ids=closed_vulns_ids,
        vulns_to_close_from_file=vulns_to_close_from_file,
        closing_reason=verification_reason,
    )


async def get_vulnerabilities_with_severity(
    loaders: Dataloaders,
    finding_id: str,
) -> list[Vulnerability]:
    """
    Retrieve all finding released vulns with the severity score populated,
    either from itself or the parent finding.
    """
    finding = await get_finding(loaders, finding_id)
    released_vulns = await loaders.finding_vulnerabilities_released_nzr.load(finding_id)

    return [
        vuln._replace(severity_score=vulns_utils.get_severity_score(vuln, finding))
        for vuln in released_vulns
    ]
