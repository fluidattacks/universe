import logging
import logging.config
from asyncio import gather
from collections.abc import Iterable
from datetime import UTC, datetime
from typing import Any

import pytz
from aioextensions import collect
from fluidattacks_core.git import InvalidParameter as GitInvalidParameter

from integrates import authz
from integrates.custom_exceptions import (
    InvalidAuthorization,
    InvalidCommentParent,
    InvalidGitCredentials,
    InvalidParameter,
    InvalidUrl,
    InvalidVulnerabilityRequirement,
    PermissionDenied,
    RepeatedFindingDescription,
    RepeatedFindingMachineDescription,
    RepeatedFindingThreat,
    RequiredUnfulfilledRequirements,
    RootNotFound,
)
from integrates.custom_utils import datetime as datetime_utils
from integrates.custom_utils import filter_vulnerabilities as filter_vulns_utils
from integrates.custom_utils import machine as machine_utils
from integrates.custom_utils import roots as roots_utils
from integrates.custom_utils import validations, validations_deco
from integrates.custom_utils import vulnerabilities as vulns_utils
from integrates.custom_utils.criteria import CRITERIA_VULNERABILITIES
from integrates.custom_utils.groups import get_group
from integrates.custom_utils.roots import get_unsolved_events_by_root
from integrates.dataloaders import Dataloaders
from integrates.db_model import findings as findings_model
from integrates.db_model.credentials.types import Credentials, CredentialsRequest
from integrates.db_model.events.enums import EventType
from integrates.db_model.finding_comments.enums import CommentType
from integrates.db_model.finding_comments.types import FindingComment
from integrates.db_model.findings.types import Finding, FindingEvidences, FindingMetadataToUpdate
from integrates.db_model.groups.enums import GroupLanguage
from integrates.db_model.roots.types import GitRoot, RootRequest
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilitySeverityProposalStatus,
    VulnerabilityStateReason,
    VulnerabilityStateStatus,
    VulnerabilityType,
)
from integrates.db_model.vulnerabilities.types import (
    FindingVulnerabilitiesRequest,
    FindingVulnerabilitiesZrRequest,
    VulnerabilitiesConnection,
    Vulnerability,
)
from integrates.finding_comments import domain as comments_domain
from integrates.findings import storage as findings_storage
from integrates.roots.utils import ls_remote_root
from integrates.settings import LOGGING
from integrates.settings.various import TIME_ZONE
from integrates.vulnerabilities import domain as vulns_domain
from integrates.vulnerabilities.domain.validations import has_valid_plan

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)


def validate_finding_requirements(title: str, unfulfilled_requirements: list[str]) -> None:
    if not unfulfilled_requirements:
        raise RequiredUnfulfilledRequirements()
    criteria_vulnerability_id = title.split(".")[0].strip()
    criteria_vulnerability = CRITERIA_VULNERABILITIES[criteria_vulnerability_id]
    criteria_vulnerability_requirements: list[str] = (
        criteria_vulnerability["requirements"] if criteria_vulnerability else []
    )
    if not set(unfulfilled_requirements).issubset(criteria_vulnerability_requirements):
        raise InvalidVulnerabilityRequirement()


async def validate_duplicated_finding(
    *,
    loaders: Dataloaders,
    group_name: str,
    title: str,
    description: str,
    threat: str,
    cvss_vector: str,
    modified_by: str | None = None,
    cvss4_vector: str | None = None,
    current_finding: Finding | None = None,
) -> None:
    group = await get_group(loaders, group_name)
    group_findings = await loaders.group_findings.load(group_name)
    same_type_of_findings = (
        [
            finding
            for finding in group_findings
            if finding.id != current_finding.id
            and finding.get_criteria_code() == title.split(".")[0].strip()
        ]
        if current_finding
        else [
            finding
            for finding in group_findings
            if finding.get_criteria_code() == title.split(".")[0].strip()
        ]
    )
    for finding in same_type_of_findings:
        if finding.description.strip() == description.strip() and not (
            current_finding is not None
            and current_finding.description.split() == description.split()
        ):
            raise RepeatedFindingDescription()
        if finding.threat.strip() == threat.strip() and current_finding:
            raise RepeatedFindingThreat()

    validate_machine_description(
        title=title,
        description=description,
        threat=threat,
        cvss_vector=cvss_vector,
        group_language=group.language,
        same_type_of_findings=same_type_of_findings,
        cvss4_vector=cvss4_vector,
        modified_by=modified_by,
    )


def validate_machine_description(
    *,
    title: str,
    description: str,
    threat: str,
    cvss_vector: str,
    group_language: GroupLanguage,
    same_type_of_findings: Iterable[Finding],
    modified_by: str | None = None,
    cvss4_vector: str | None = None,
) -> None:
    criteria_vulnerability: dict[str, Any] = CRITERIA_VULNERABILITIES[title.split(".")[0].strip()]
    duplicated_machine_findings = [
        finding
        for finding in same_type_of_findings
        if machine_utils.has_machine_description(
            criteria_vulnerability=criteria_vulnerability,
            language=group_language.lower(),
            cvss_vector=cvss_vector,
            description=description,
            threat=threat,
            cvss4_vector=cvss4_vector,
        )
        if (
            (finding.creation.modified_by if finding.creation else finding.state.modified_by)
            == modified_by
            if modified_by
            else True
        )
    ]
    if duplicated_machine_findings:
        raise RepeatedFindingMachineDescription()


async def get_vulnerabilities(
    loaders: Dataloaders,
    finding_id: str,
    vulnerability_ids: set[str],
) -> list[Vulnerability]:
    vulnerabilities = await vulns_domain.get_by_finding_and_vuln_ids(
        loaders,
        finding_id,
        vulnerability_ids,
    )
    for vuln in vulnerabilities:
        validations.validate_released(vuln)
    return vulnerabilities


async def _ls_remote_root(*, root: GitRoot, cred: Credentials | None, loaders: Dataloaders) -> str:
    try:
        last_commit, _ = await ls_remote_root(root=root, cred=cred, loaders=loaders)

        return last_commit or ""
    except (
        InvalidAuthorization,
        InvalidGitCredentials,
        InvalidParameter,
        GitInvalidParameter,
        InvalidUrl,
    ):
        LOGGER.warning(
            "Unable to request the last commit info for root",
            extra={
                "extra": {
                    "group": root.group_name,
                    "root": root.id,
                },
            },
        )

    return ""


async def validate_outdated_repository(
    *,
    loaders: Dataloaders,
    vulnerabilities: Iterable[Vulnerability],
    group_name: str,
) -> None:
    filtered_root_ids = {
        vuln.root_id
        for vuln in vulnerabilities
        if vuln.type == VulnerabilityType.LINES and vuln.root_id
    }
    roots = [
        root
        for root_id in filtered_root_ids
        if (root := await loaders.root.load(RootRequest(group_name=group_name, root_id=root_id)))
        is not None
        and isinstance(root, GitRoot)
        and root.state.credential_id
        and root.cloning.commit_date
    ]

    if len(roots) == 0:
        return

    organization = await loaders.organization.load(roots[0].organization_name)
    if organization is None:
        return

    roots_credentials = {
        root.id: cred
        for root in roots
        if root.state.credential_id
        and (
            cred := await loaders.credentials.load(
                CredentialsRequest(
                    id=root.state.credential_id,
                    organization_id=organization.id,
                ),
            )
        )
        is not None
    }

    _commits_by_roots = await collect(
        [
            _ls_remote_root(root=root, cred=roots_credentials[root.id], loaders=loaders)
            for root in roots
        ],
    )
    commits_by_roots = {
        root.id: (
            commit,
            root.cloning.commit_date or datetime.now(),
        )
        for commit, root in zip(_commits_by_roots, roots, strict=False)
    }

    for vuln in vulnerabilities:
        if vuln.root_id and vuln.root_id in commits_by_roots.keys():
            validations.validate_outdated_repository(
                vuln,
                commits_by_roots[vuln.root_id][0],
                commits_by_roots[vuln.root_id][1],
            )


async def validate_vulns_has_not_unsolved_events(
    loaders: Dataloaders,
    group_name: str,
    vulnerabilities: set[str],
    specific_type: EventType | None = None,
) -> None:
    unsolved_events_by_root = await get_unsolved_events_by_root(
        loaders=loaders,
        group_name=group_name,
        specific_type=specific_type,
    )
    roots_with_events = [root_id for root_id, _ in unsolved_events_by_root.items()]
    tasks = [
        validations.validate_unsolved_events(loaders, vuln_id, roots_with_events)
        for vuln_id in vulnerabilities
    ]
    await gather(*tasks)


async def validate_vulnerabilities(
    *,
    loaders: Dataloaders,
    vulnerabilities: Iterable[Vulnerability],
    group_name: str,
    is_closing_event: bool,
    no_unsolved_events: bool = False,
    vulnerability_ids: set[str],
) -> list[Vulnerability]:
    group = await get_group(loaders, group_name)
    has_advanced = "has_advanced" in sorted(authz.get_group_service_attributes(group))
    if len(list(vulnerabilities)) == 0:
        return []

    valid_vulns: list[Vulnerability] = []
    for vuln in vulnerabilities:
        if not vulns_domain.is_verification_already_requested(
            vuln,
            is_closing_event,
        ) and has_valid_plan(vuln, has_advanced):
            valid_vulns.append(vuln)
    if no_unsolved_events:
        await validate_vulns_has_not_unsolved_events(
            loaders=loaders,
            group_name=group_name,
            vulnerabilities=vulnerability_ids,
            specific_type=EventType.CLONING_ISSUES,
        )
    await validate_outdated_repository(
        loaders=loaders,
        vulnerabilities=valid_vulns,
        group_name=group_name,
    )

    return [validations.validate_closed(vuln) for vuln in valid_vulns]


async def remove_all_evidences(finding_id: str, group_name: str) -> None:
    file_names = await findings_storage.search_evidence(f"{group_name}/{finding_id}")
    await collect(
        findings_storage.remove_evidence(f"{group_name}/{finding_id}/{file_name.split('/')[-1]}")
        for file_name in file_names
    )
    metadata = FindingMetadataToUpdate(evidences=FindingEvidences())
    await findings_model.update_metadata(
        group_name=group_name,
        finding_id=finding_id,
        metadata=metadata,
    )


@authz.validate_handle_comment_scope_deco(
    "loaders",
    "comment_data.content",
    "user_email",
    "group_name",
    "comment_data.parent_id",
)
@validations_deco.validate_length_deco("content", max_length=20000)
async def add_comment(
    loaders: Dataloaders,
    user_email: str,
    comment_data: FindingComment,
    finding_id: str,
    group_name: str,
) -> None:
    param_type = comment_data.comment_type
    parent_comment = str(comment_data.parent_id) if comment_data.parent_id else "0"
    if param_type == CommentType.OBSERVATION:
        enforcer = await authz.get_group_level_enforcer(loaders, user_email)
        if not enforcer(group_name, "post_finding_observation"):
            raise PermissionDenied()
    if parent_comment != "0":
        all_finding_comments: list[FindingComment] = await comments_domain.get_unformatted_comments(
            loaders=loaders,
            comment_type=comment_data.comment_type,
            finding_id=finding_id,
        )
        finding_comments = {comment.id for comment in all_finding_comments}
        if parent_comment not in finding_comments:
            raise InvalidCommentParent()
    await comments_domain.add(loaders, comment_data, notify=True)


async def remove_vulnerabilities(
    loaders: Dataloaders,
    finding_id: str,
    justification: VulnerabilityStateReason,
    email: str,
) -> None:
    vulnerabilities = await loaders.finding_vulnerabilities.load(finding_id)
    await collect(
        tuple(
            vulns_domain.remove_vulnerability(
                loaders=loaders,
                finding_id=finding_id,
                vulnerability_id=vulnerability.id,
                justification=justification,
                email=email,
                include_closed_vuln=True,
            )
            for vulnerability in vulnerabilities
        ),
        workers=8,
    )


def get_report_days(report_date: datetime | None) -> int:
    """Gets amount of days from a report date."""
    return (datetime_utils.get_utc_now() - report_date).days if report_date else 0


async def get_status(loaders: Dataloaders, finding_id: str) -> str:
    vulns = await loaders.finding_vulnerabilities_released_nzr.load(finding_id)
    if not vulns:
        return "DRAFT"
    open_vulns = filter_vulns_utils.filter_open_vulns(vulns)
    return "VULNERABLE" if open_vulns else "SAFE"


async def get_finding_open_age(loaders: Dataloaders, finding_id: str) -> int:
    vulns = await loaders.finding_vulnerabilities_released_nzr.load(finding_id)
    open_vulns = filter_vulns_utils.filter_open_vulns(vulns)
    report_dates = vulns_utils.get_report_dates(open_vulns)
    if report_dates:
        oldest_report_date = min(report_dates)
        return (datetime_utils.get_now() - oldest_report_date).days
    return 0


async def get_open_vulnerabilities(
    loaders: Dataloaders,
    finding_id: str,
) -> list[Vulnerability]:
    connection: VulnerabilitiesConnection = (
        await loaders.finding_vulnerabilities_released_nzr_c.load(
            FindingVulnerabilitiesZrRequest(
                finding_id=finding_id,
                paginate=False,
                state_status=VulnerabilityStateStatus.VULNERABLE,
            ),
        )
    )

    return [edge.node for edge in connection.edges]


async def get_all_vulnerabilities(
    loaders: Dataloaders,
    finding_id: str,
) -> list[Vulnerability]:
    return await loaders.finding_vulnerabilities.load(finding_id)


async def get_open_vulnerabilities_len(loaders: Dataloaders, finding_id: str) -> int:
    return len(await get_open_vulnerabilities(loaders, finding_id))


async def get_rejected_vulnerabilities(loaders: Dataloaders, finding_id: str) -> int:
    drafts = await loaders.finding_vulnerabilities_draft_c.load_nodes(
        FindingVulnerabilitiesRequest(finding_id=finding_id),
    )
    return len(
        [draft for draft in drafts if draft.state.status is VulnerabilityStateStatus.REJECTED],
    )


async def get_vulnerabilities_to_reattack(
    loaders: Dataloaders,
    finding_id: str,
) -> list[Vulnerability]:
    finding_vulns = await loaders.finding_vulnerabilities_released_nzr.load(finding_id)
    return filter_vulns_utils.filter_open_vulns(filter_vulns_utils.filter_remediated(finding_vulns))


async def get_vulnerabilities_severity_update_requests(
    loaders: Dataloaders,
    finding_id: str,
) -> list[Vulnerability]:
    finding_vulns = await loaders.finding_vulnerabilities.load(finding_id)
    return [
        vuln
        for vuln in finding_vulns
        if vuln.state.proposed_severity is not None
        and vuln.state.proposed_severity.status == VulnerabilitySeverityProposalStatus.REQUESTED
    ]


async def _get_vuln_nickname(
    loaders: Dataloaders,
    vuln: Vulnerability,
) -> str:
    result: str = f"{vuln.state.where} ({vuln.state.specific})"
    if vuln.type == VulnerabilityType.LINES:
        try:
            if root := await roots_utils.get_root(loaders, vuln.root_id or "", vuln.group_name):
                return f" {root.state.nickname}/{result}"
        except RootNotFound:
            pass
    return result


async def _format_vulnerabilities_nickname(title: str, vulns_nicknames: tuple[str, ...]) -> str:
    result = ""
    if vulns_nicknames:
        result += f"\n\n{title}:\n"
        result += "\n".join([f"  - {vuln_nickname}" for vuln_nickname in vulns_nicknames])
    return result


async def generate_justification(
    loaders: Dataloaders,
    open_vulnerabilities: Iterable[Vulnerability],
    closed_vulnerabilities: Iterable[Vulnerability],
    commit_hash: str | None = None,
    observations: str | None = None,
) -> str:
    justification = (
        datetime.now(tz=UTC).astimezone(tz=pytz.timezone(TIME_ZONE)).strftime("%Y/%m/%d %H:%M")
    )
    commit_msg = f" in commit {commit_hash}" if commit_hash else ""
    observations_msg = f"\n\nObservations:\n  {observations}" if observations else ""
    justification = (
        f"A reattack request was executed on {justification.replace(' ', ' at ')}{commit_msg}."
    )
    vulns_nicknames = await collect(
        [_get_vuln_nickname(loaders, vuln) for vuln in open_vulnerabilities],
        workers=32,
    )
    justification += await _format_vulnerabilities_nickname("Open vulnerabilities", vulns_nicknames)
    vulns_nicknames = await collect(
        [_get_vuln_nickname(loaders, vuln) for vuln in closed_vulnerabilities],
        workers=32,
    )
    justification += await _format_vulnerabilities_nickname(
        "Closed vulnerabilities",
        vulns_nicknames,
    )
    justification += observations_msg
    return justification
