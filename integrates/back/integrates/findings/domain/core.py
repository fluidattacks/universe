import logging
import logging.config
import uuid
from collections.abc import Iterable
from datetime import datetime
from decimal import Decimal
from time import time
from typing import TypedDict, cast

from aioextensions import collect

from integrates import authz
from integrates.class_types.types import Item
from integrates.custom_exceptions import (
    FindingNotFound,
    InvalidCVSS3VectorString,
    InvalidCVSS4VectorString,
    InvalidRemovalFindingState,
    RootNotFound,
)
from integrates.custom_utils import cvss as cvss_utils
from integrates.custom_utils import datetime as datetime_utils
from integrates.custom_utils import filter_vulnerabilities as filter_vulns_utils
from integrates.custom_utils import findings as findings_utils
from integrates.custom_utils import roots as roots_utils
from integrates.custom_utils import validations_deco
from integrates.custom_utils import vulnerabilities as vulns_utils
from integrates.custom_utils.findings import get_finding
from integrates.dataloaders import Dataloaders
from integrates.db_model import findings as findings_model
from integrates.db_model.enums import Source, StateRemovalJustification
from integrates.db_model.finding_comments.enums import CommentType
from integrates.db_model.finding_comments.types import FindingComment
from integrates.db_model.findings.enums import FindingStateStatus, FindingStatus
from integrates.db_model.findings.types import (
    Finding,
    FindingMetadataToUpdate,
    FindingState,
    FindingVulnerabilitiesSummary,
)
from integrates.db_model.findings.utils import format_finding
from integrates.db_model.items import FindingItem
from integrates.db_model.roots.enums import RootCriticality
from integrates.db_model.roots.types import GitRoot, GitRootState, RootRequest
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateReason,
    VulnerabilityStateStatus,
    VulnerabilityTechnique,
    VulnerabilityVerificationStatus,
    VulnerabilityZeroRiskStatus,
)
from integrates.db_model.vulnerabilities.types import FindingVulnerabilitiesZrRequest, Vulnerability
from integrates.finding_comments import domain as comments_domain
from integrates.findings.types import FindingAttributesToAdd, FindingDescriptionToUpdate
from integrates.roots.utils import compare_root_criticalities
from integrates.search.operations import SearchParams, search
from integrates.settings import LOGGING
from integrates.vulnerabilities import domain as vulns_domain
from integrates.vulnerabilities.domain.core import get_vulns_filters_by_root_nickname
from integrates.vulnerabilities.types import Treatments, ZeroRisk

from ..types import SeverityLevelsInfo, SeverityLevelSummary
from .utils import (
    generate_justification,
    get_all_vulnerabilities,
    get_open_vulnerabilities,
    get_open_vulnerabilities_len,
    get_vulnerabilities_to_reattack,
    remove_all_evidences,
    remove_vulnerabilities,
    validate_duplicated_finding,
    validate_finding_requirements,
)

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger(__name__)


class VulnsProperties(TypedDict):
    remaining_exposure: int
    severity_level: str
    severity_level_v3: str
    severity_score_v3: Decimal
    severity_score: Decimal
    vulns_props: dict[str, dict[str, dict[str, str]]]


@validations_deco.validate_fields_deco(
    [
        "attributes.attack_vector_description",
        "attributes.description",
        "attributes.recommendation",
        "attributes.unfulfilled_requirements",
        "attributes.threat",
    ],
)
@validations_deco.validate_fields_length_deco(
    [
        "attributes.unfulfilled_requirements",
    ],
    min_length=1,
    max_length=300,
)
@validations_deco.validate_fields_length_deco(
    [
        "attributes.attack_vector_description",
        "attributes.threat",
    ],
    min_length=1,
    max_length=650,
)
@validations_deco.validate_fields_length_deco(
    [
        "attributes.description",
    ],
    min_length=1,
    max_length=1000,
)
@validations_deco.validate_fields_length_deco(
    [
        "attributes.recommendation",
    ],
    min_length=1,
    max_length=500,
)
async def add_finding(
    *,
    loaders: Dataloaders,
    group_name: str,
    stakeholder_email: str,
    attributes: FindingAttributesToAdd,
) -> Finding:
    findings_utils.is_valid_finding_title(attributes.title)
    validate_finding_requirements(attributes.title, attributes.unfulfilled_requirements)
    if not attributes.cvss_vector:
        raise InvalidCVSS3VectorString.new()
    if not attributes.cvss4_vector:
        raise InvalidCVSS4VectorString.new()

    cvss_utils.validate_cvss_vector(attributes.cvss_vector)
    cvss_utils.validate_cvss4_vector(attributes.cvss4_vector)
    if attributes.source not in (Source.MACHINE, Source.LLM):
        await validate_duplicated_finding(
            loaders=loaders,
            group_name=group_name,
            title=attributes.title,
            description=attributes.description,
            threat=attributes.threat,
            cvss_vector=attributes.cvss_vector,
            cvss4_vector=attributes.cvss4_vector,
            modified_by=stakeholder_email,
        )

    finding = Finding(
        attack_vector_description=attributes.attack_vector_description,
        description=attributes.description,
        group_name=group_name,
        id=str(uuid.uuid4()),
        min_time_to_remediate=attributes.min_time_to_remediate,
        state=FindingState(
            modified_by=stakeholder_email,
            modified_date=datetime_utils.get_utc_now(),
            source=attributes.source,
            status=FindingStateStatus.CREATED,
        ),
        recommendation=attributes.recommendation,
        severity_score=cvss_utils.get_severity_score_from_cvss_vector(
            attributes.cvss_vector,
            attributes.cvss4_vector,
        ),
        title=attributes.title,
        threat=attributes.threat,
        unfulfilled_requirements=sorted(set(attributes.unfulfilled_requirements)),
    )
    await findings_model.add(finding=finding)
    return finding


async def validate_hacker_permissions(
    loaders: Dataloaders, user_email: str, group_name: str, finding_id: str
) -> None:
    vulnerabilities = await loaders.finding_vulnerabilities.load(finding_id)
    has_released_vuln = any(
        vuln
        for vuln in vulnerabilities
        if vuln.state.status
        in {
            VulnerabilityStateStatus.VULNERABLE,
            VulnerabilityStateStatus.SAFE,
        }
    )
    if (
        await authz.get_group_level_role(loaders, user_email, group_name) == "hacker"
        and has_released_vuln
    ):
        raise InvalidRemovalFindingState.new()


async def remove_finding(
    loaders: Dataloaders,
    email: str,
    finding_id: str,
    justification: StateRemovalJustification,
    source: Source,
) -> None:
    finding = await get_finding(loaders, finding_id)
    if finding.state.status == FindingStateStatus.DELETED:
        raise FindingNotFound()

    await validate_hacker_permissions(
        loaders=loaders,
        user_email=email,
        group_name=finding.group_name,
        finding_id=finding_id,
    )
    await remove_vulnerabilities(
        loaders,
        finding.id,
        VulnerabilityStateReason[justification.value],
        email,
    )
    await remove_all_evidences(finding.id, finding.group_name)
    await comments_domain.remove_comments(finding_id=finding_id)
    deletion_state = FindingState(
        justification=justification,
        modified_by=email,
        modified_date=datetime_utils.get_utc_now(),
        source=source,
        status=FindingStateStatus.DELETED,
    )
    await findings_model.update_state(
        current_value=finding.state,
        finding_id=finding.id,
        group_name=finding.group_name,
        state=deletion_state,
    )

    created_by = finding.creation.modified_by if finding.creation else finding.state.modified_by
    finding_code = finding.get_criteria_code()
    if not findings_utils.is_finding_released(finding):
        await findings_model.remove(
            created_by=created_by,
            finding_code=finding_code,
            finding_id=finding.id,
            group_name=finding.group_name,
        )
        return

    # We'll keep the finding in db as DELETED,
    # but without the machine code lock
    await findings_model.remove_machine_finding_code(
        created_by=created_by,
        finding_code=finding_code,
        group_name=finding.group_name,
    )


async def get_last_closed_vulnerability_info(
    loaders: Dataloaders,
    findings: Iterable[Finding],
) -> tuple[int, Vulnerability | None]:
    """Get days since the last closed vulnerability and its metadata."""
    valid_findings_ids = [
        finding.id for finding in findings if not findings_utils.is_deleted(finding)
    ]
    vulns = await loaders.finding_vulnerabilities_released_nzr.load_many_chained(valid_findings_ids)
    closed_vulns = filter_vulns_utils.filter_closed_vulns(vulns)
    closing_vuln_dates = [vulns_utils.get_closing_date(vuln) for vuln in closed_vulns]
    if closing_vuln_dates:
        current_date, date_index = max(
            (v, i) for i, v in enumerate(closing_vuln_dates) if v is not None
        )
        last_closed_vuln: Vulnerability | None = closed_vulns[date_index]
        last_closed_days = (datetime_utils.get_now().date() - current_date).days
    else:
        last_closed_days = 0
        last_closed_vuln = None
    return last_closed_days, last_closed_vuln


async def get_max_open_severity_finding(
    loaders: Dataloaders,
    findings: Iterable[Finding],
) -> tuple[Decimal, Finding | None]:
    open_vulns = await collect(
        get_open_vulnerabilities_len(loaders, finding.id) for finding in findings
    )
    open_findings = [
        finding
        for finding, open_vulns_count in zip(findings, open_vulns, strict=False)
        if open_vulns_count > 0
    ]
    total_severity = await collect(
        get_max_open_severity_score_v4(loaders, finding.id) for finding in open_findings
    )
    if total_severity:
        max_severity, severity_index = max((v, i) for i, v in enumerate(total_severity))
        max_severity_finding: Finding | None = open_findings[severity_index]
    else:
        max_severity = Decimal("0.0")
        max_severity_finding = None

    return max_severity, max_severity_finding


async def get_newest_vulnerability_report_date(
    loaders: Dataloaders,
    finding_id: str,
) -> datetime | None:
    vulns = await loaders.finding_vulnerabilities_released_nzr.load(finding_id)
    report_dates = vulns_utils.get_report_dates(filter_vulns_utils.filter_released_vulns(vulns))

    return max(report_dates) if report_dates else None


async def _is_pending_verification(loaders: Dataloaders, finding_id: str) -> bool:
    return len(await get_vulnerabilities_to_reattack(loaders, finding_id)) > 0


async def get_pending_verification_findings(
    loaders: Dataloaders,
    group_name: str,
) -> list[Finding]:
    """Gets findings pending for verification."""
    findings = await findings_utils.get_group_findings(group_name=group_name, loaders=loaders)
    are_pending_verifications = await collect(
        _is_pending_verification(loaders, finding.id) for finding in findings
    )
    return [
        finding
        for finding, is_pending_verification in zip(
            findings,
            are_pending_verifications,
            strict=False,
        )
        if is_pending_verification
    ]


async def get_treatment_summary(
    loaders: Dataloaders,
    finding_id: str,
) -> Treatments:
    return vulns_domain.get_treatments_count(await get_open_vulnerabilities(loaders, finding_id))


async def get_vulnerabilities_summary(
    loaders: Dataloaders,
    finding_id: str,
) -> FindingVulnerabilitiesSummary:
    finding = await get_finding(loaders, finding_id)
    all_vulns = await loaders.finding_vulnerabilities_released_nzr.load(finding_id)
    vulns = [vuln for vuln in all_vulns if vuln.state.status == VulnerabilityStateStatus.VULNERABLE]

    return FindingVulnerabilitiesSummary(
        closed=len(
            [vuln for vuln in all_vulns if vuln.state.status == VulnerabilityStateStatus.SAFE]
        ),
        open=len(vulns),
        submitted=len(
            [vuln for vuln in all_vulns if vuln.state.status == VulnerabilityStateStatus.SUBMITTED]
        ),
        rejected=len(
            [vuln for vuln in all_vulns if vuln.state.status == VulnerabilityStateStatus.REJECTED]
        ),
        open_critical=get_severity_level_summary(finding, vulns, "critical").total,
        open_high=get_severity_level_summary(finding, vulns, "high").total,
        open_low=get_severity_level_summary(finding, vulns, "low").total,
        open_medium=get_severity_level_summary(finding, vulns, "medium").total,
        open_critical_v3=_get_severity_level_summary_v3(finding, vulns, "critical").total,
        open_high_v3=_get_severity_level_summary_v3(finding, vulns, "high").total,
        open_low_v3=_get_severity_level_summary_v3(finding, vulns, "low").total,
        open_medium_v3=_get_severity_level_summary_v3(finding, vulns, "medium").total,
    )


async def get_zero_risk_summary(
    loaders: Dataloaders,
    finding_id: str,
) -> ZeroRisk:
    return vulns_domain.get_zr_count(await get_all_vulnerabilities(loaders, finding_id))


async def _get_wheres(loaders: Dataloaders, finding_id: str, limit: int | None = None) -> list[str]:
    finding_vulns = await loaders.finding_vulnerabilities_released_nzr.load(finding_id)
    open_vulns = filter_vulns_utils.filter_open_vulns(finding_vulns)
    wheres: list[str] = list(set(vuln.state.where for vuln in open_vulns))
    if limit:
        wheres = wheres[:limit]
    return wheres


async def get_where(loaders: Dataloaders, finding_id: str) -> str:
    """General locations of the Vulnerabilities. It is limited to 20 locations."""
    return ", ".join(sorted(await _get_wheres(loaders, finding_id, limit=20)))


async def has_access_to_finding(loaders: Dataloaders, email: str, finding_id: str) -> bool | None:
    """Verify if the user has access to a finding submission."""
    finding: Finding | None = await loaders.finding.load(finding_id)
    if finding:
        return await authz.has_access_to_group(loaders, email, finding.group_name)
    raise FindingNotFound()


async def mask_finding(loaders: Dataloaders, finding: Finding, email: str) -> None:
    await comments_domain.remove_comments(finding_id=finding.id)
    await remove_all_evidences(finding.id, finding.group_name)

    vulnerabilities = await loaders.finding_vulnerabilities_all.load(finding.id)
    await collect(
        tuple(
            vulns_domain.mask_vulnerability(
                email=email,
                finding_id=finding.id,
                vulnerability=vulnerability,
            )
            for vulnerability in vulnerabilities
        ),
        workers=8,
    )

    if finding.state.status == FindingStateStatus.DELETED and bool(
        finding.unreliable_indicators.vulnerabilities_summary.open
        + finding.unreliable_indicators.vulnerabilities_summary.closed,
    ):
        # Findings in the MASKED state will be archived by Streams
        # for analytics purposes
        await findings_model.update_state(
            current_value=finding.state,
            finding_id=finding.id,
            group_name=finding.group_name,
            state=finding.state._replace(
                modified_by=email,
                modified_date=datetime_utils.get_utc_now(),
                status=FindingStateStatus.MASKED,
            ),
        )

    await findings_model.remove(
        created_by=finding.creation.modified_by if finding.creation else finding.state.modified_by,
        finding_code=finding.get_criteria_code(),
        finding_id=finding.id,
        group_name=finding.group_name,
    )
    LOGGER.info(
        "Finding masked",
        extra={
            "extra": {
                "finding_id": finding.id,
                "group_name": finding.group_name,
            },
        },
    )


async def repo_subtitle(loaders: Dataloaders, vuln: Vulnerability, group_name: str) -> str:
    repo = "Vulnerabilities"
    if vuln.root_id:
        try:
            root = await roots_utils.get_root(loaders, vuln.root_id, group_name)
            nickname = root.state.nickname if isinstance(root.state.nickname, str) else repo
            repo = (
                f"{nickname}/{root.state.branch}"
                if isinstance(root.state, (GitRootState, str))
                else nickname
            )
        except RootNotFound:
            repo = "Vulnerabilities"
    return repo


def get_vulnerability_properties(vuln: Vulnerability, finding: Finding, is_closed: bool) -> Item:
    if is_closed:
        risk_exposure = vulns_utils.get_severity_cvssf_v4(vuln, finding)
        report_date = (
            vuln.unreliable_indicators.unreliable_report_date.date()
            if vuln.unreliable_indicators.unreliable_report_date
            else None
        )
        days_open = (datetime_utils.get_utc_now().date() - report_date).days if report_date else 0
        reattack_requester = vuln.unreliable_indicators.unreliable_last_reattack_requester

        return {
            f"{vuln.state.where}{vuln.state.specific}": {
                "location": vuln.state.where,
                "specific": vuln.state.specific,
                "source": vuln.state.source.value,
                "assigned": vuln.treatment.assigned if vuln.treatment else None,
                "report date": report_date,
                "time to remediate": f"{days_open} calendar days",
                "reattack requester": reattack_requester,
                "reduction in exposure": round(risk_exposure, 1),
            },
        }

    return {
        f"{vuln.state.where}{vuln.state.specific}": {
            "location": vuln.state.where,
            "specific": vuln.state.specific,
            "source": vuln.state.source.value,
        },
    }


async def vulns_properties(
    loaders: Dataloaders,
    finding_id: str,
    vulnerabilities: list[Vulnerability],
    is_closed: bool = False,
) -> Item:
    finding = await get_finding(loaders, finding_id)
    vulns_props: dict[str, dict[str, Item]] = {}

    repos = await collect(
        [repo_subtitle(loaders, vuln, finding.group_name) for vuln in vulnerabilities],
    )

    for vuln, repo in zip(vulnerabilities, repos, strict=False):
        vuln_dict = vulns_props.get(repo, {})
        vuln_dict.update(get_vulnerability_properties(vuln, finding, is_closed))
        vulns_props[repo] = dict(sorted(vuln_dict.items()))

    return vulns_props


def get_remaining_exposure(
    finding: Finding,
    closed_vulnerabilities: Iterable[Vulnerability],
) -> int:
    total_open_exposure = finding.unreliable_indicators.total_open_cvssf_v4
    closed_exposure = sum(
        vulns_utils.get_severity_cvssf_v4(vuln, finding) for vuln in closed_vulnerabilities
    )

    return int(total_open_exposure - Decimal(closed_exposure))


@validations_deco.validate_fields_deco(["description"])
@validations_deco.validate_fields_length_deco(
    [
        "description.description",
    ],
    min_length=1,
    max_length=1000,
)
@validations_deco.validate_fields_length_deco(
    [
        "description.attack_vector_description",
        "description.threat",
    ],
    min_length=1,
    max_length=650,
)
@validations_deco.validate_fields_length_deco(
    [
        "description.recommendation",
    ],
    min_length=1,
    max_length=500,
)
async def update_description(
    loaders: Dataloaders,
    finding_id: str,
    description: FindingDescriptionToUpdate,
) -> None:
    unfulfilled_requirements = (
        None
        if description.unfulfilled_requirements is None
        else sorted(set(description.unfulfilled_requirements))
    )
    if description.title:
        findings_utils.is_valid_finding_title(description.title)

    finding = await get_finding(loaders, finding_id)
    if description.description is not None or description.threat is not None:
        await validate_duplicated_finding(
            loaders=loaders,
            group_name=finding.group_name,
            title=description.title or finding.title,
            description=description.description or finding.description,
            threat=description.threat or finding.threat,
            cvss_vector=finding.severity_score.cvss_v3,
            cvss4_vector=finding.severity_score.cvss_v4,
            current_finding=finding,
            modified_by=finding.creation.modified_by
            if finding.creation
            else finding.state.modified_by,
        )

    if unfulfilled_requirements is not None:
        validate_finding_requirements(
            description.title or finding.title,
            unfulfilled_requirements,
        )

    metadata = FindingMetadataToUpdate(
        attack_vector_description=description.attack_vector_description,
        description=description.description,
        recommendation=description.recommendation,
        sorts=description.sorts,
        threat=description.threat,
        title=description.title,
        unfulfilled_requirements=unfulfilled_requirements,
    )
    await findings_model.update_metadata(
        group_name=finding.group_name,
        finding_id=finding.id,
        metadata=metadata,
    )


async def update_severity(
    loaders: Dataloaders,
    finding_id: str,
    cvss_vector: str,
    cvss4_vector: str,
) -> None:
    cvss_utils.validate_cvss4_vector(cvss4_vector)
    cvss_utils.validate_cvss_vector(cvss_vector)
    finding = await get_finding(loaders, finding_id)
    metadata = FindingMetadataToUpdate(
        severity_score=cvss_utils.get_severity_score_from_cvss_vector(cvss_vector, cvss4_vector),
    )
    await findings_model.update_metadata(
        group_name=finding.group_name,
        finding_id=finding.id,
        metadata=metadata,
    )


async def add_reattack_justification(
    *,
    loaders: Dataloaders,
    finding_id: str,
    open_vulnerabilities: Iterable[Vulnerability],
    closed_vulnerabilities: Iterable[Vulnerability],
    commit_hash: str | None = None,
    comment_type: CommentType = CommentType.COMMENT,
    email: str = "machine@fluidattacks.com",
    full_name: str = "Scanner Services",
    observations: str | None = None,
) -> None:
    justification = await generate_justification(
        loaders,
        open_vulnerabilities,
        closed_vulnerabilities,
        commit_hash,
        observations,
    )
    LOGGER.info(
        "%s Vulnerabilities were verified and found open in finding %s",
        len(list(open_vulnerabilities)),
        finding_id,
    )
    LOGGER.info(
        "%s Vulnerabilities were verified and found closed in finding %s",
        len(list(closed_vulnerabilities)),
        finding_id,
    )
    closed_properties: VulnsProperties | None = None
    if closed_vulnerabilities:
        finding = await get_finding(loaders, finding_id)
        if finding.unreliable_indicators.unreliable_status == FindingStatus.VULNERABLE:
            severity_score = vulns_utils.get_vulnerabilities_max_score(
                finding,
                closed_vulnerabilities,
            )
            severity_score_v3 = vulns_utils.get_vulnerabilities_max_score_v3(
                finding,
                closed_vulnerabilities,
            )
            closed_properties = VulnsProperties(
                remaining_exposure=get_remaining_exposure(finding, closed_vulnerabilities),
                severity_level=cvss_utils.get_severity_level(severity_score),
                severity_score=severity_score,
                severity_score_v3=severity_score_v3,
                severity_level_v3=cvss_utils.get_severity_level(severity_score_v3),
                vulns_props=await vulns_properties(
                    loaders,
                    finding_id,
                    [vuln for vuln in closed_vulnerabilities if vuln is not None],
                    is_closed=True,
                ),
            )
    await comments_domain.add(
        loaders,
        FindingComment(
            finding_id=finding_id,
            id=str(round(time() * 1000)),
            comment_type=comment_type,
            parent_id="0",
            creation_date=datetime_utils.get_utc_now(),
            full_name=full_name,
            content=justification,
            email=email,
        ),
        closed_properties=closed_properties,
    )


async def get_oldest_no_treatment(
    loaders: Dataloaders,
    findings: Iterable[Finding],
) -> dict[str, int | str] | None:
    """Get the finding with oldest "no treatment" vulnerability."""
    vulns = await loaders.finding_vulnerabilities_released_nzr.load_many_chained(
        [finding.id for finding in findings],
    )
    open_vulns = filter_vulns_utils.filter_open_vulns(vulns)
    no_treatment_vulns = filter_vulns_utils.filter_no_treatment_vulns(open_vulns)
    if not no_treatment_vulns:
        return None
    treatment_dates: list[datetime] = [
        vuln.treatment.modified_date for vuln in no_treatment_vulns if vuln.treatment
    ]
    vulns_info = [
        (
            date,
            vuln.finding_id,
        )
        for vuln, date in zip(no_treatment_vulns, treatment_dates, strict=False)
    ]
    oldest_date, oldest_finding_id = min(vulns_info)
    oldest_finding: Finding = next(
        finding for finding in findings if finding.id == oldest_finding_id
    )
    return {
        "oldest_name": str(oldest_finding.title),
        "oldest_age": int((datetime_utils.get_now() - oldest_date).days),
    }


async def get_oldest_open_vulnerability_report_date(
    loaders: Dataloaders,
    finding_id: str,
) -> datetime | None:
    vulns = await loaders.finding_vulnerabilities_released_nzr.load(finding_id)
    open_vulns = filter_vulns_utils.filter_open_vulns(vulns)
    report_dates = vulns_utils.get_oldest_report_dates(open_vulns)

    return min(report_dates) if report_dates else None


async def get_oldest_vulnerability_report_date(
    loaders: Dataloaders,
    finding_id: str,
) -> datetime | None:
    vulns = await loaders.finding_vulnerabilities_released_nzr.load(finding_id)
    released_vulns = filter_vulns_utils.filter_released_vulns(vulns)
    report_dates = vulns_utils.get_oldest_report_dates(released_vulns)

    return min(report_dates) if report_dates else None


def check_hold(vuln: Vulnerability) -> bool:
    return (
        vuln.verification is not None
        and vuln.verification.status == VulnerabilityVerificationStatus.ON_HOLD
    )


async def get_max_open_epss(
    loaders: Dataloaders,
    finding_id: str,
) -> int:
    finding = await loaders.finding.load(finding_id)
    if finding is None or findings_utils.is_deleted(finding):
        return 0

    vulns = await get_open_vulnerabilities(loaders, finding.id)
    if not vulns:
        return 0

    vulns_epss = [
        vuln.state.advisories.epss if vuln.state.advisories and vuln.state.advisories.epss else 0
        for vuln in vulns
    ]

    return max(vulns_epss)


async def get_max_open_root_criticality(
    loaders: Dataloaders,
    finding_id: str,
) -> RootCriticality:
    finding = await loaders.finding.load(finding_id)
    result = RootCriticality.LOW
    if finding is None or findings_utils.is_deleted(finding):
        return result

    vulns = await loaders.finding_vulnerabilities_released_nzr.load(
        finding_id,
    )
    root_ids = frozenset(vuln.root_id for vuln in vulns if vuln.root_id)
    roots = await loaders.root.load_many(
        [
            RootRequest(
                group_name=finding.group_name,
                root_id=root_id,
            )
            for root_id in root_ids
        ],
    )
    git_roots = [root for root in roots if isinstance(root, GitRoot)]
    criticalities = frozenset(
        root.state.criticality for root in git_roots if root.state.criticality
    )
    for criticality in criticalities:
        if criticality is RootCriticality.HIGH:
            result = criticality
            break
        if compare_root_criticalities(result, "lt", criticality):
            result = criticality
    return result


async def get_max_open_severity_score(
    loaders: Dataloaders,
    finding_id: str,
) -> Decimal:
    finding = await loaders.finding.load(finding_id)
    if finding is None or findings_utils.is_deleted(finding):
        return Decimal("0.0")

    vulns = await get_open_vulnerabilities(loaders, finding.id)
    if not vulns:
        return Decimal("0.0")

    vulns_score = [vulns_utils.get_severity_temporal_score(vuln, finding) for vuln in vulns]

    return Decimal(max(vulns_score))


async def get_max_open_severity_score_v4(
    loaders: Dataloaders,
    finding_id: str,
) -> Decimal:
    finding = await loaders.finding.load(finding_id)
    if finding is None or findings_utils.is_deleted(finding):
        return Decimal("0.0")

    vulns = await get_open_vulnerabilities(loaders, finding.id)
    if not vulns:
        return Decimal("0.0")

    vulns_score = list(
        filter(
            None,
            [vulns_utils.get_severity_threat_score(vuln, finding) for vuln in vulns],
        ),
    )

    if not vulns_score:
        return Decimal("0.0")

    return Decimal(max(vulns_score))


async def get_max_released_severity_score(
    loaders: Dataloaders,
    finding_id: str,
) -> Decimal:
    finding = await loaders.finding.load(finding_id)
    if finding is None or findings_utils.is_deleted(finding):
        return Decimal("0.0")

    vulns = await loaders.finding_vulnerabilities_released_nzr.load(finding_id)
    if not vulns:
        return Decimal("0.0")

    vulns_score = [vulns_utils.get_severity_threat_score(vuln, finding) for vuln in vulns]

    return Decimal(max(vulns_score))


async def get_safe_cvssf(
    loaders: Dataloaders,
    finding_id: str,
) -> Decimal:
    finding = await loaders.finding.load(finding_id)
    if finding is None or findings_utils.is_deleted(finding):
        return Decimal("0.0")

    connection = await loaders.finding_vulnerabilities_released_nzr_c.load(
        FindingVulnerabilitiesZrRequest(
            finding_id=finding_id,
            paginate=False,
            state_status=VulnerabilityStateStatus.SAFE,
        ),
    )
    vulns = [edge.node for edge in connection.edges]
    vulns_cvssf = [vulns_utils.get_severity_cvssf_v4(vuln, finding) for vuln in vulns]

    return Decimal(sum(vulns_cvssf))


async def get_safe_cvssf_v3(
    loaders: Dataloaders,
    finding_id: str,
) -> Decimal:
    finding = await loaders.finding.load(finding_id)
    if finding is None or findings_utils.is_deleted(finding):
        return Decimal("0.0")

    connection = await loaders.finding_vulnerabilities_released_nzr_c.load(
        FindingVulnerabilitiesZrRequest(
            finding_id=finding_id,
            paginate=False,
            state_status=VulnerabilityStateStatus.SAFE,
        ),
    )
    vulns = [edge.node for edge in connection.edges]
    vulns_cvssf = [vulns_utils.get_severity_cvssf(vuln, finding) for vuln in vulns]

    return Decimal(sum(vulns_cvssf))


async def get_total_cvssf_v3(
    loaders: Dataloaders,
    finding_id: str,
) -> Decimal:
    finding = await loaders.finding.load(finding_id)
    if finding is None or findings_utils.is_deleted(finding):
        return Decimal("0.0")

    vulns = await loaders.finding_vulnerabilities_released_nzr.load(finding.id)
    vulns_cvssf = [vulns_utils.get_severity_cvssf(vuln, finding) for vuln in vulns]

    return Decimal(sum(vulns_cvssf))


async def get_total_cvssf(
    loaders: Dataloaders,
    finding_id: str,
) -> Decimal:
    finding = await loaders.finding.load(finding_id)
    if finding is None or findings_utils.is_deleted(finding):
        return Decimal("0.0")

    vulns = await loaders.finding_vulnerabilities_released_nzr.load(finding.id)
    vulns_cvssf = [vulns_utils.get_severity_cvssf_v4(vuln, finding) for vuln in vulns]

    return Decimal(sum(vulns_cvssf))


async def get_total_open_cvssf(
    loaders: Dataloaders,
    finding_id: str,
) -> Decimal:
    finding = await loaders.finding.load(finding_id)
    if finding is None or findings_utils.is_deleted(finding):
        return Decimal("0.0")

    vulns = await get_open_vulnerabilities(loaders, finding.id)
    if not vulns:
        return Decimal("0.0")

    vulns_cvssf = [vulns_utils.get_severity_cvssf(vuln, finding) for vuln in vulns]

    return Decimal(sum(vulns_cvssf))


async def get_total_open_cvssf_v4(
    loaders: Dataloaders,
    finding_id: str,
) -> Decimal:
    finding = await loaders.finding.load(finding_id)
    if finding is None or findings_utils.is_deleted(finding):
        return Decimal("0.0")

    vulns = await get_open_vulnerabilities(loaders, finding.id)
    if not vulns:
        return Decimal("0.0")

    vulns_cvssf = [vulns_utils.get_severity_cvssf_v4(vuln, finding) for vuln in vulns]

    return Decimal(sum(vulns_cvssf))


async def has_vulns_with_technique(
    loaders: Dataloaders,
    finding_id: str,
    technique: str | None,
) -> bool:
    if not technique:
        return True

    vulns = await loaders.finding_vulnerabilities_released_nzr.load(finding_id)

    return any(vuln.technique == cast(VulnerabilityTechnique, technique) for vuln in vulns)


async def get_total_open_priority(
    loaders: Dataloaders,
    finding_id: str,
) -> Decimal:
    finding = await loaders.finding.load(finding_id)
    if finding is None or findings_utils.is_deleted(finding):
        return Decimal(0)

    vulns = await get_open_vulnerabilities(loaders, finding.id)
    if not vulns:
        return Decimal(0)

    return Decimal(sum(vulns_utils.get_priority(vuln) for vuln in vulns))


def get_severity_level_summary(
    finding: Finding,
    vulns: Iterable[Vulnerability],
    severity_level: str,
) -> SeverityLevelSummary:
    level_vulns = [
        vuln
        for vuln in vulns
        if cvss_utils.get_severity_level(vulns_utils.get_severity_threat_score(vuln, finding))
        == severity_level
    ]
    treatments_count = vulns_domain.get_treatments_count(level_vulns)

    return SeverityLevelSummary(
        accepted=treatments_count.accepted + treatments_count.accepted_undefined,
        closed=len(filter_vulns_utils.filter_closed_vulns(level_vulns)),
        total=len(level_vulns),
    )


def _get_severity_level_summary_v3(
    finding: Finding,
    vulns: Iterable[Vulnerability],
    severity_level: str,
) -> SeverityLevelSummary:
    level_vulns = [
        vuln
        for vuln in vulns
        if cvss_utils.get_severity_level(vulns_utils.get_severity_temporal_score(vuln, finding))
        == severity_level
    ]
    treatments_count = vulns_domain.get_treatments_count(level_vulns)

    return SeverityLevelSummary(
        accepted=treatments_count.accepted + treatments_count.accepted_undefined,
        closed=len(filter_vulns_utils.filter_closed_vulns(level_vulns)),
        total=len(level_vulns),
    )


async def get_severity_levels_info(
    loaders: Dataloaders,
    finding: Finding,
) -> SeverityLevelsInfo:
    vulns = await loaders.finding_vulnerabilities_released_nzr.load(finding.id)

    return SeverityLevelsInfo(
        critical=_get_severity_level_summary_v3(finding, vulns, "critical"),
        high=_get_severity_level_summary_v3(finding, vulns, "high"),
        medium=_get_severity_level_summary_v3(finding, vulns, "medium"),
        low=_get_severity_level_summary_v3(finding, vulns, "low"),
    )


async def get_severity_levels_info_v4(
    loaders: Dataloaders,
    finding: Finding,
) -> SeverityLevelsInfo:
    vulns = await loaders.finding_vulnerabilities_released_nzr.load(finding.id)

    return SeverityLevelsInfo(
        critical=get_severity_level_summary(finding, vulns, "critical"),
        high=get_severity_level_summary(finding, vulns, "high"),
        medium=get_severity_level_summary(finding, vulns, "medium"),
        low=get_severity_level_summary(finding, vulns, "low"),
    )


def _vulns_must_not_filter(has_internal_role: bool) -> list[Item]:
    must_not_filters: list[Item] = [
        {"state.status": VulnerabilityStateStatus.DELETED.value},
        {"state.status": VulnerabilityStateStatus.MASKED.value},
        {"zero_risk.status": VulnerabilityZeroRiskStatus.CONFIRMED.value},
        {"zero_risk.status": VulnerabilityZeroRiskStatus.REQUESTED.value},
    ]

    if not has_internal_role:
        must_not_filters.append({"state.status": VulnerabilityStateStatus.REJECTED.value})
        must_not_filters.append({"state.status": VulnerabilityStateStatus.SUBMITTED.value})

    return must_not_filters


def _findings_should_range_filters(has_internal_role: bool) -> list[Item]:
    should_range_filters = []

    if not has_internal_role:
        should_range_filters.append(
            {"unreliable_indicators.vulnerabilities_summary.open": {"gt": 0}},
        )
        should_range_filters.append(
            {"unreliable_indicators.vulnerabilities_summary.closed": {"gt": 0}},
        )

    return should_range_filters


def _findings_must_not_filters() -> list[Item]:
    must_not_filters = [
        {
            "state.status": FindingStateStatus.DELETED,
        },
        {
            "state.status": FindingStateStatus.MASKED,
        },
    ]

    return must_not_filters


def _finding_must_match_prefix_filters(title: str | None) -> list[Item]:
    filters = []

    if title:
        filters.append(
            {"title": title},
        )

    return filters


def _vulns_must_filters(
    technique: VulnerabilityTechnique | None,
) -> list[dict[str, VulnerabilityTechnique]]:
    must_filters = []

    if technique:
        must_filters.append(
            {"technique": technique},
        )

    return must_filters


def _vulns_match_prefix_filter(
    root: str | None,
) -> list[dict[str, str]]:
    filters = []

    if root:
        filters.append(
            {"state.where": root},
        )

    return filters


async def get_filtered_findings(
    *,
    group_name: str,
    loaders: Dataloaders,
    root: str | None,
    title: str | None,
    technique: VulnerabilityTechnique | None,
    has_internal_role: bool,
) -> list[Finding]:
    should_and_filters = await get_vulns_filters_by_root_nickname(loaders, group_name, root)
    released_vulns = [{"sk_6.keyword": "*RELEASED#true*"}] if not has_internal_role else None

    results = await search(
        SearchParams(
            exact_filters={"group_name": group_name},
            index_value="findings_index",
            must_not_filters=_findings_must_not_filters(),
            limit=1000,
            must_match_prefix_filters=_finding_must_match_prefix_filters(title),
            should_range_filters=_findings_should_range_filters(has_internal_role),
        ),
    )

    findings = [
        result
        for item in results.items
        if (result := format_finding(cast(FindingItem, item))) is not None
    ]

    if root or technique:
        findings_id: set[str] = set()
        has_next_page: bool = True
        after: str | list[str] | None = None

        while has_next_page:
            results = await search(
                SearchParams(
                    after=after,
                    exact_filters={"group_name": group_name},
                    index_value="vulns_index",
                    limit=100,
                    must_filters=_vulns_must_filters(technique),
                    must_not_filters=_vulns_must_not_filter(has_internal_role),
                    should_and_filters=should_and_filters,
                    should_match_prefix_filters=_vulns_match_prefix_filter(root),
                    wildcard_queries=released_vulns,
                ),
            )

            has_next_page = results.page_info.has_next_page
            after = results.page_info.end_cursor
            findings_id.update(item["sk"].split("#")[1] for item in results.items)

        filtered_findings_by_vulns = [finding for finding in findings if finding.id in findings_id]

        return filtered_findings_by_vulns

    return findings
