# Starlette authz-related views/functions

import asyncio
import logging
import logging.config
import uuid
from datetime import (
    datetime,
)
from html import (
    escape,
)
from json.decoder import (
    JSONDecodeError,
)

from aioextensions import (
    collect,
)
from authlib.integrations.base_client.errors import (
    MismatchingStateError,
    OAuthError,
)
from httpagentparser import (
    simple_detect,
)
from httpx import (
    ConnectTimeout,
    ReadTimeout,
)
from starlette.requests import (
    Request,
)
from starlette.responses import (
    RedirectResponse,
    Response,
)

from integrates.app import (
    utils,
)
from integrates.audit import AuditEvent, add_audit_event
from integrates.context import (
    BASE_URL,
)
from integrates.custom_exceptions import (
    CouldNotVerifyStakeholder,
    FreeTrialAlreadyUsed,
    InvalidAuthorization,
    InvalidField,
    InvalidVerificationCode,
    MailerClientError,
    NewMemberWithExistingDomain,
    OnlyCorporateEmails,
    UnableToSendMail,
)
from integrates.custom_utils import (
    analytics,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.custom_utils import (
    requests as requests_utils,
)
from integrates.custom_utils import (
    templates as templates_utils,
)
from integrates.custom_utils.personal_email import (
    is_personal_email,
)
from integrates.custom_utils.registered_domain import (
    get_managers_company_domain,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    stakeholders as stakeholders_model,
)
from integrates.db_model.group_access.types import (
    GroupAccess,
)
from integrates.db_model.organization_access.types import (
    OrganizationAccess,
)
from integrates.db_model.stakeholders.types import (
    NotificationsPreferences,
    Stakeholder,
    StakeholderMetadataToUpdate,
    StakeholderState,
    TrustedDevice,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.mailer.common import (
    report_login_activity,
)
from integrates.mailer.groups import (
    send_mail_missing_member_alert,
)
from integrates.mailer.types import (
    MissingMemberAlertContext,
)
from integrates.marketplace.core import (
    link_user_marketplace_subscription,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.sessions import (
    domain as sessions_domain,
)
from integrates.sessions.types import (
    UserAccessInfo,
)
from integrates.settings import (
    JWT_COOKIE_NAME,
    JWT_OTP_COOKIE_NAME,
    OTP_COOKIE_AGE,
    SESSION_COOKIE_AGE,
)
from integrates.settings.auth import (
    OAUTH,
)
from integrates.stakeholders import (
    domain as stakeholders_domain,
)
from integrates.verify import (
    operations as verify_operations,
)
from integrates.verify.enums import (
    Channel,
)

LOGGER = logging.getLogger(__name__)
TRANSACTIONS_LOGGER = logging.getLogger("transactional")

SUBSCRIPTIONS = {
    "default": [
        "ACCESS_GRANTED",
        "AGENT_TOKEN",
        "EVENT_DIGEST",
        "EVENT_REPORT",
        "FILE_UPDATE",
        "GROUP_INFORMATION",
        "GROUP_REPORT",
        "NEWSLETTER",
        "NEW_COMMENT",
        "NEW_DRAFT",
        "PORTFOLIO_UPDATE",
        "REMEDIATE_FINDING",
        "REMINDER_NOTIFICATION",
        "ROOT_UPDATE",
        "SERVICE_UPDATE",
        "UNSUBSCRIPTION_ALERT",
        "UPDATED_TREATMENT",
        "VULNERABILITY_ASSIGNED",
        "VULNERABILITY_REPORT",
    ],
    "user": [
        "NEWSLETTER",
        "UPDATED_TREATMENT",
        "VULNERABILITY_ASSIGNED",
        "VULNERABILITY_REPORT",
    ],
    "group_manager": [
        "AGENT_TOKEN",
        "EVENT_DIGEST",
        "EVENT_REPORT",
        "FILE_UPDATE",
        "GROUP_INFORMATION",
        "GROUP_REPORT",
        "NEWSLETTER",
        "REMINDER_NOTIFICATION",
        "ROOT_UPDATE",
        "SERVICE_UPDATE",
        "UNSUBSCRIPTION_ALERT",
        "UPDATED_TREATMENT",
        "VULNERABILITY_REPORT",
    ],
    "organization_manager": [
        "AGENT_TOKEN",
        "EVENT_DIGEST",
        "EVENT_REPORT",
        "FILE_UPDATE",
        "GROUP_INFORMATION",
        "GROUP_REPORT",
        "NEWSLETTER",
        "REMINDER_NOTIFICATION",
        "ROOT_UPDATE",
        "SERVICE_UPDATE",
        "UNSUBSCRIPTION_ALERT",
        "UPDATED_TREATMENT",
        "VULNERABILITY_REPORT",
    ],
    "vulnerability_manager": [
        "EVENT_DIGEST",
        "EVENT_REPORT",
        "NEWSLETTER",
        "UPDATED_TREATMENT",
        "VULNERABILITY_REPORT",
    ],
}

send_missing_member_alert_mail = retry_on_exceptions(
    exceptions=(UnableToSendMail, MailerClientError),
    max_attempts=4,
    sleep_seconds=2,
)(send_mail_missing_member_alert)


async def authz_azure(request: Request) -> Response:
    client = OAUTH.azure
    try:
        user = await utils.get_jwt_userinfo(client, request)
        return await get_auth_response(
            request,
            UserAccessInfo(
                first_name=user["userinfo"]["name"],
                last_name="",
                subject=f'MICROSOFT#{user["userinfo"]["sub"]}',
                # Microsoft's OIDC implementation quirks to be aware of:
                # - Personal accounts only have email.
                # - Active Directory accounts always have UPN, but it doesn't
                #   necessarily match an actual email account.
                # - Active Directory accounts may have email, but it is not
                #   guaranteed to be immutable and verified.
                user_email=str(user["userinfo"].get("email", user.get("upn", ""))).lower(),
                verified=user["userinfo"].get("xms_edov") in [True, "1"],
            ),
        )
    except MismatchingStateError:
        # User went back in browser history to the provider login page
        pass
    except (OAuthError, ConnectTimeout, ReadTimeout):
        LOGGER.exception("OAuth azure login error")
    return templates_utils.login(request)


async def authz_bitbucket(request: Request) -> Response:
    client = OAUTH.bitbucket
    try:
        token = await retry_on_exceptions(
            exceptions=(OAuthError,),
            sleep_seconds=float("0.3"),
        )(client.authorize_access_token)(request)
        user = await utils.get_bitbucket_oauth_userinfo(client, token)
        return await get_auth_response(
            request,
            UserAccessInfo(
                # html escaped to prevent XSS from a bitbucket username
                # https://gitlab.com/fluidattacks/universe/-/issues/13392
                first_name=escape(user["display_name"]),
                last_name="",
                subject=f'BITBUCKET#{user["account_id"]}',
                user_email=user["email"],
                verified=user["is_confirmed"],
            ),
        )
    except MismatchingStateError:
        # User went back in browser history to the provider login page
        pass
    except (OAuthError, JSONDecodeError, InvalidAuthorization):
        LOGGER.exception("OAuth bitbucket login error")
    return templates_utils.login(request)


async def authz_google(request: Request) -> Response:
    client = OAUTH.google
    try:
        user = await utils.get_jwt_userinfo(client, request)
        return await get_auth_response(
            request,
            UserAccessInfo(
                first_name=user["userinfo"]["given_name"],
                last_name=str(user["userinfo"].get("last_name", "")),
                subject=f'GOOGLE#{user["userinfo"]["sub"]}',
                user_email=user["userinfo"]["email"],
                verified=user["userinfo"]["email_verified"],
            ),
        )
    except MismatchingStateError:
        # User went back in browser history to the provider login page
        pass
    except (
        OAuthError,
        ConnectTimeout,
        ReadTimeout,
        InvalidAuthorization,
    ):
        LOGGER.exception("OAuth google login error")
    return templates_utils.login(request)


async def do_azure_login(request: Request) -> Response:
    redirect_uri = requests_utils.get_redirect_url(request, "authz_azure")
    azure = OAUTH.azure
    return await azure.authorize_redirect(request, redirect_uri)


async def do_bitbucket_login(request: Request) -> Response:
    redirect_uri = requests_utils.get_redirect_url(request, "authz_bitbucket")
    bitbucket = OAUTH.bitbucket
    return await bitbucket.authorize_redirect(request, redirect_uri)


async def do_google_login(request: Request) -> Response:
    redirect_uri = requests_utils.get_redirect_url(request, "authz_google")
    google = OAUTH.google
    try:
        return await retry_on_exceptions(
            exceptions=(ConnectTimeout,),
            sleep_seconds=float("0.3"),
        )(google.authorize_redirect)(request, redirect_uri)
    except ConnectTimeout:
        LOGGER.exception("Google login timeout")
        return templates_utils.login(request)


def get_subscriptions(roles: list[str]) -> list[str]:
    if all(rol in SUBSCRIPTIONS for rol in roles):
        all_subscriptions = [SUBSCRIPTIONS.get(item, []) for item in set(roles)]
        return list({item for list in all_subscriptions for item in list})

    return SUBSCRIPTIONS.get("default", [])


async def complete_register(
    *,
    email: str,
    first_name: str,
    last_name: str,
    roles: list[str],
) -> None:
    today = datetime_utils.get_utc_now()
    await collect(
        (
            stakeholders_model.update_metadata(
                email=email,
                metadata=StakeholderMetadataToUpdate(
                    first_name=first_name,
                    last_login_date=today,
                    last_name=last_name,
                    registration_date=today,
                ),
            ),
            stakeholders_model.update_state(
                user_email=email,
                state=StakeholderState(
                    notifications_preferences=NotificationsPreferences(
                        email=get_subscriptions(roles),
                    ),
                    modified_date=today,
                    modified_by=email.strip().lower(),
                    trusted_devices=[],
                ),
            ),
        ),
    )
    await analytics.mixpanel_track(email, "Register")


async def redirect_auth_response(
    request: Request,
    user_info: UserAccessInfo,
    otp_token: str | None = None,
) -> Response:
    response = RedirectResponse(url="/home", status_code=303)
    jwt_token = await sessions_domain.create_session_token(user_info)
    sessions_domain.set_token_in_response(
        response=response,
        token=jwt_token,
        key=JWT_COOKIE_NAME,
        max_age=SESSION_COOKIE_AGE,
    )
    if otp_token:
        sessions_domain.set_token_in_response(
            response=response,
            token=otp_token,
            key=JWT_OTP_COOKIE_NAME,
            max_age=OTP_COOKIE_AGE,
        )
    await sessions_domain.create_session_web(request, user_info.user_email)
    return response


async def process_post_verify_request(
    *,
    loaders: Dataloaders,
    request: Request,
    user_info: UserAccessInfo,
    recipient: str,
) -> Response:
    try:
        otp_token = None
        trusted_device = TrustedDevice(**request.session["trusted_device"])
        form = await request.form()
        otp_values = [str(form.get(f"otp{i}", "")) for i in range(6)]
        entered_otp = "".join(otp_values)
        await verify_operations.check_verification(code=entered_otp, recipient=recipient)
        is_trusted = form.get("trusted_device")

        if is_trusted:
            otp_token = await sessions_domain.create_otp_token(trusted_device)

            await stakeholders_domain.update_trusted_devices(
                loaders=loaders,
                trusted_device=trusted_device,
                user_email=user_info.user_email,
            )

        await report_login_activity(
            loaders=loaders,
            browser=trusted_device.browser,
            ip_address=trusted_device.ip_address,
            location=trusted_device.location,
            operating_system=trusted_device.device,
            report_date=datetime.now(),
            user_email=user_info.user_email,
        )

        return await redirect_auth_response(request, user_info, otp_token)
    except (InvalidVerificationCode, CouldNotVerifyStakeholder):
        channel = request.session["channel"]
        masked_phone = await stakeholders_domain.get_masked_phone_number(
            loaders,
            user_info.user_email,
        )

        return templates_utils.verify_otp(
            request=request,
            entity_name="verify_otp",
            user_email=user_info.user_email,
            channel=channel,
            phone_number=masked_phone,
            show_alert=True,
        )


async def process_get_otp_request(
    *,
    loaders: Dataloaders,
    request: Request,
    user_info: UserAccessInfo,
    channel: Channel,
) -> Response:
    if request.client is None:
        TRANSACTIONS_LOGGER.info(
            "Client not found",
            extra={
                "subject": user_info.subject,
                "user_email": user_info.user_email,
            },
        )
        return templates_utils.login(request)

    headers = request.headers
    device, browser = simple_detect(headers.get("User-Agent"))
    otp_jti = sessions_domain.get_jti_otp_token(request)
    trusted_device = TrustedDevice(
        browser=browser,
        device=device,
        ip_address=headers.get("cf-connecting-ip", request.client.host),
        location=headers.get("cf-ipcountry", ""),
        otp_token_jti=otp_jti,
    )

    user_trusted_device = await stakeholders_domain.get_trust_device(
        loaders=loaders,
        email=user_info.user_email,
        trusted_device=trusted_device,
    )

    if user_trusted_device and user_trusted_device.otp_token_jti == otp_jti:
        await stakeholders_domain.update_trusted_devices(
            loaders=loaders,
            trusted_device=user_trusted_device,
            user_email=user_info.user_email,
            is_last_attempt=False,
        )

        return await redirect_auth_response(request, user_info)

    masked_phone = await stakeholders_domain.get_masked_phone_number(loaders, user_info.user_email)

    channel = channel if masked_phone else Channel.EMAIL
    request.session["trusted_device"] = trusted_device._asdict()
    request.session["channel"] = channel.value

    recipient = user_info.user_email
    if channel in [Channel.SMS, Channel.WHATSAPP]:
        recipient = await stakeholders_domain.get_stakeholder_phone_number(
            loaders=loaders,
            user_email=user_info.user_email,
        )

    await verify_operations.start_verification(channel=channel, recipient=recipient)
    return templates_utils.verify_otp(
        request=request,
        entity_name="verify_otp",
        user_email=user_info.user_email,
        channel=channel.value,
        phone_number=masked_phone,
    )


async def verify_otp(request: Request) -> Response:
    loaders = get_new_context()
    user_info: UserAccessInfo = UserAccessInfo(**request.session["user_info"])
    form = await request.form()
    verify = form.get("verify", False)
    channel = request.session.get("channel", Channel.EMAIL.value)
    form_channel = form.get("channel", channel)

    if request.method == "POST" and verify:
        recipient = user_info.user_email
        if channel in [Channel.SMS, Channel.WHATSAPP]:
            recipient = await stakeholders_domain.get_stakeholder_phone_number(
                loaders=loaders,
                user_email=user_info.user_email,
            )

        return await process_post_verify_request(
            loaders=loaders,
            request=request,
            user_info=user_info,
            recipient=recipient,
        )

    return await process_get_otp_request(
        loaders=loaders,
        request=request,
        user_info=user_info,
        channel=Channel(form_channel),
    )


async def get_auth_response(request: Request, user_info: UserAccessInfo) -> Response:
    session_key = str(uuid.uuid4())
    request.session["session_key"] = session_key
    loaders = get_new_context()

    try:
        await log_stakeholder_in(loaders, user_info, request)
    except OAuthError:
        return templates_utils.login(request)
    except OnlyCorporateEmails:
        return RedirectResponse(url="/SignUp?error=invalidEmail")
    except NewMemberWithExistingDomain:
        return RedirectResponse(url="/SignUp?error=existingDomain")
    except FreeTrialAlreadyUsed:
        return RedirectResponse(url="/SignUp?error=previousTrial")

    request.session["user_info"] = user_info._asdict()
    return await verify_otp(request)


async def handle_existing_domain(
    loaders: Dataloaders,
    user_email: str,
    new_user: bool,
) -> None:
    email_context: MissingMemberAlertContext = {
        "missing_email": user_email,
        "platform_url": f"{BASE_URL}",
    }

    managers_domains = await get_managers_company_domain(loaders=loaders, user_email=user_email)

    if managers_domains:
        TRANSACTIONS_LOGGER.info(
            "New user with existing domain",
            extra={
                "user_email": user_email,
            },
        )

        if new_user:
            await send_missing_member_alert_mail(
                loaders=loaders,
                context=email_context,
                email_to=managers_domains,
            )

        raise NewMemberWithExistingDomain()


async def handle_previous_trial(
    loaders: Dataloaders,
    user_email: str,
) -> None:
    trial = await loaders.trial.load(user_email)
    if trial and trial.completed:
        TRANSACTIONS_LOGGER.info(
            "The user's free trial has already been used.",
            extra={
                "user_email": user_email,
            },
        )

        raise FreeTrialAlreadyUsed()


async def autoenroll_stakeholder(
    loaders: Dataloaders,
    email: str,
    first_name: str,
    last_name: str,
) -> None:
    await orgs_domain.add_without_group(
        email=email,
        role="user",
        is_register_after_complete=True,
    )
    await complete_register(
        email=email,
        first_name=first_name,
        last_name=last_name,
        roles=["organization_manager"],
    )
    await handle_existing_domain(loaders=loaders, user_email=email, new_user=True)
    await handle_previous_trial(loaders=loaders, user_email=email)
    await utils.send_autoenroll_mixpanel_event(loaders, email)


async def invited_stakeholder(
    email: str,
    first_name: str,
    last_name: str,
    orgs_access: list[OrganizationAccess],
    groups_access: list[GroupAccess],
) -> None:
    roles = []
    if len(orgs_access) > 0:
        roles.extend([org.state.invitation.role for org in orgs_access if org.state.invitation])
    if len(groups_access) > 0:
        roles.extend(
            [group.state.invitation.role for group in groups_access if group.state.invitation],
        )
    await complete_register(
        email=email,
        first_name=first_name,
        last_name=last_name,
        roles=roles,
    )
    await analytics.mixpanel_track(email, "InvitedStakeholder")


async def _handle_existing_stakeholder(
    loaders: Dataloaders,
    email: str,
    first_name: str,
    last_name: str,
    stakeholder: Stakeholder,
) -> None:
    orgs_access = await loaders.stakeholder_organizations_access.load(email)
    groups_access = await loaders.stakeholder_groups_access.load(email)
    has_access = len(orgs_access) + len(groups_access) > 0
    if not stakeholder.is_registered:
        await stakeholders_domain.register(email)
    if not stakeholder.registration_date:
        await invited_stakeholder(email, first_name, last_name, orgs_access, groups_access)
    if not has_access:
        await handle_existing_domain(loaders=loaders, user_email=email, new_user=False)
        await handle_previous_trial(loaders=loaders, user_email=email)
    else:
        await utils.send_autoenroll_mixpanel_event(loaders, email, stakeholder)
    await stakeholders_domain.update_last_login(email)


async def _handle_new_stakeholder(
    loaders: Dataloaders,
    email: str,
    first_name: str,
    last_name: str,
) -> None:
    try:
        await autoenroll_stakeholder(loaders, email, first_name, last_name)
    except InvalidField as error:
        raise InvalidAuthorization() from error


async def _handle_stakeholder_subject(
    *,
    email: str,
    stakeholder: Stakeholder,
    user_info: UserAccessInfo,
) -> None:
    if stakeholder.subject:
        if stakeholder.subject != user_info.subject:
            TRANSACTIONS_LOGGER.info(
                "User attempted login from an unlinked provider",
                extra={
                    "attempted_subject": user_info.subject,
                    "current_subject": stakeholder.subject,
                    "user_email": user_info.user_email,
                },
            )
            linked_provider = stakeholder.subject.split("#")[0].lower()
            raise OAuthError(
                description=(
                    "Your email account has already been linked to"
                    f" {linked_provider}."
                    " Sign in using that provider instead."
                ),
            )
    else:
        TRANSACTIONS_LOGGER.info(
            "Linked existing user with subject",
            extra={
                "subject": user_info.subject,
                "user_email": user_info.user_email,
            },
        )
        await stakeholders_domain.link_subject(email, user_info.subject)


async def log_stakeholder_in(
    loaders: Dataloaders,
    user_info: UserAccessInfo,
    request: Request,
) -> None:
    if not user_info.verified:
        TRANSACTIONS_LOGGER.info(
            "Unverified email domain attempted login",
            extra={
                "subject": user_info.subject,
                "user_email": user_info.user_email,
            },
        )
        raise OAuthError(
            description=(
                "You need to verify the email account in the authentication"
                " provider before using the platform."
            ),
        )

    email = user_info.user_email.lower()
    first_name = user_info.first_name[:29]
    last_name = user_info.last_name[:29]
    aws_customer_id: str | None = request.session.pop("aws_customer_id", None)
    stakeholder = await loaders.stakeholder.load(email)

    if stakeholder:
        await _handle_stakeholder_subject(email=email, stakeholder=stakeholder, user_info=user_info)
        TRANSACTIONS_LOGGER.info(
            "User logged in",
            extra={
                "subject": user_info.subject,
                "user_email": user_info.user_email,
            },
        )
        add_audit_event(
            AuditEvent(
                action="CREATE",
                author=email,
                author_role=stakeholder.role,
                metadata=user_info._asdict(),
                object="Session",
                object_id=email,
            )
        )
        await asyncio.gather(
            stakeholders_domain.register_login(email, user_info.subject, request),
            _handle_existing_stakeholder(loaders, email, first_name, last_name, stakeholder),
        )
        if aws_customer_id is not None and stakeholder.aws_customer_id is None:
            await link_user_marketplace_subscription(
                aws_customer_id=aws_customer_id,
                email=email,
                loaders=loaders,
            )
    else:
        if await is_personal_email(email):
            TRANSACTIONS_LOGGER.info(
                "Personal email attempted to register",
                extra={
                    "user_email": email,
                },
            )
            raise OnlyCorporateEmails()

        TRANSACTIONS_LOGGER.info(
            "New user registered",
            extra={
                "subject": user_info.subject,
                "user_email": user_info.user_email,
            },
        )
        await _handle_new_stakeholder(loaders, email, first_name, last_name)
        await stakeholders_domain.link_subject(email, user_info.subject)
        if aws_customer_id is not None:
            await link_user_marketplace_subscription(
                aws_customer_id=aws_customer_id,
                email=email,
                loaders=loaders,
            )
