// Delighted snippet, get Email, Name and Company of the person being surveyed
!(function (e, t, r, n) {
  if (!e[n]) {
    const a = [];
    const i = [
      "survey",
      "reset",
      "config",
      "init",
      "set",
      "get",
      "event",
      "identify",
      "track",
      "page",
      "screen",
      "group",
      "alias",
    ];
    for (const c of i) {
      a[c] =
        a[c] ||
        (function (e) {
          return function () {
            const t = Array.prototype.slice.call(arguments);
            a.push([e, t]);
          };
        })(c);
    }
    a.SNIPPET_VERSION = "1.0.1";
    const o = t.createElement("script");
    o.async = true;
    o.type = "text/javascript";
    o.src =
      "https://d2yyd1h5u9mauk.cloudfront.net/integrations/web/v1/library/" +
      r +
      "/" +
      n +
      ".js";
    const p = t.getElementsByTagName("script")[0];
    p.parentNode.insertBefore(o, p);
  }
})(window, document, "C2IiXJX4CW06goZ8", "delighted");
