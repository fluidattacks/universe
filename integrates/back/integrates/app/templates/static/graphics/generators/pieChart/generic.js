/* global bb */
/* global d3 */

const normalizationFactor = 255;
const luminanceThreshold = 0.03928;
const lowLuminanceDivisor = 12.92;
const gammaCorrection = 2.4;
const redWeight = 0.2126;
const greenWeight = 0.7152;
const blueWeight = 0.0722;
const gammaAdjustment = 0.055;
const gammaDivisor = 1.055;
const transformThreshold = 0.5;
const defaultPaddingRatio = 0.05;

function getLuminance(red, green, blue) {
  const normalizedColors = [ red, green, blue ].map((colorValue) => {
    const normalized = colorValue / normalizationFactor;
    return normalized <= luminanceThreshold ?
      normalized / lowLuminanceDivisor :
      Math.pow((normalized + gammaAdjustment) / gammaDivisor, gammaCorrection);
  });

  const redLuminance = redWeight * normalizedColors[0];
  const greenLuminance = greenWeight * normalizedColors[1];
  const blueLuminance = blueWeight * normalizedColors[2];

  return redLuminance + greenLuminance + blueLuminance;
}

function getTextColor(r, g, b) {
  const luminance = getLuminance(r, g, b);
  return luminance < transformThreshold ? '#FFFFFF' : '#000000';
}

function formatColorLabel() {
  d3.selectAll('.bb-chart-arc text').each((_d, index, textList) => {
    const arcElement = d3.selectAll('.bb-arcs path').nodes()[index];
    if (arcElement) {
      const fillColor = d3.select(arcElement).style('fill');
      const rgbColor = d3.rgb(fillColor);
      const textColor = getTextColor(rgbColor.r, rgbColor.g, rgbColor.b);
      d3.select(textList[index]).style('fill', textColor);
    }
  });
}


function render(dataDocument, height, width) {
  bb.generate({
    ...dataDocument,
    onrendered: () => {
      formatColorLabel();
    },
    bindto: '#root',
    padding: {
      bottom: (dataDocument.paddingRatioBottom ? dataDocument.paddingRatioBottom : defaultPaddingRatio) * height,
      left: (dataDocument.paddingRatioLeft ? dataDocument.paddingRatioLeft : defaultPaddingRatio) * width,
      right: (dataDocument.paddingRatioRight ? dataDocument.paddingRatioRight : defaultPaddingRatio) * width,
      top: (dataDocument.paddingRatioTop ? dataDocument.paddingRatioTop : defaultPaddingRatio) * height,
    },
    size: {
      height,
      width,
    },
  });
}

function load() {
  const args = JSON.parse(document.getElementById('args').textContent.replace(/'/g, '"'));
  const dataDocument = JSON.parse(args.data);

  render(dataDocument, args.height, args.width);
}

window.load = load;
