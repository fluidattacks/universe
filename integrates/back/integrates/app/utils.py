from collections.abc import (
    Iterable,
)
from typing import (
    TypedDict,
)

from aioextensions import (
    collect,
)
from authlib.integrations.starlette_client import (
    OAuthError,
)
from authlib.integrations.starlette_client.apps import (
    StarletteOAuth2App,
)
from httpx import (
    ConnectTimeout,
    ReadTimeout,
)
from starlette.requests import (
    Request,
)

from integrates.audit import AuditEvent, add_audit_event
from integrates.custom_utils import (
    analytics,
)
from integrates.custom_utils.access import get_stakeholder_groups_names
from integrates.custom_utils.personal_email import (
    is_personal_email,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.groups.enums import (
    GroupManaged,
)
from integrates.db_model.stakeholders.types import (
    Stakeholder,
)
from integrates.db_model.trials.types import (
    Trial,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.sessions.types import (
    UserAuthResponse,
)

VALID_MANAGED = [
    GroupManaged.MANAGED,
    GroupManaged.NOT_MANAGED,
    GroupManaged.TRIAL,
]


class BitbucketUserInfo(TypedDict):
    account_id: str
    display_name: str
    email: str
    is_confirmed: bool


async def get_bitbucket_oauth_userinfo(
    client: StarletteOAuth2App,
    token: dict[str, str],
) -> BitbucketUserInfo:
    query_headers = {"Authorization": f"Bearer {token['access_token']}"}
    user_response = await client.get("user", token=token, headers=query_headers)
    user = user_response.json()
    emails_response = await client.get("user/emails", token=token, headers=query_headers)
    emails = emails_response.json()
    email = next(email for email in emails["values"] if email["is_primary"])
    return {
        "account_id": user["account_id"],
        "display_name": user["display_name"],
        "email": email["email"],
        "is_confirmed": email["is_confirmed"],
    }


@retry_on_exceptions(
    exceptions=(ConnectTimeout, OAuthError, ReadTimeout),
    sleep_seconds=float("0.5"),
)
async def get_jwt_userinfo(
    client: StarletteOAuth2App,
    request: Request,
) -> UserAuthResponse:
    return await client.authorize_access_token(
        request,
        # Workaround to support microsoft multi-tenant
        # https://github.com/MicrosoftDocs/azure-docs/issues/38427
        claims_options={} if client.name == "azure" else None,
    )


async def get_group_valid_managed(loaders: Dataloaders, group_name: str) -> bool:
    group = await loaders.group.load(group_name)
    if group:
        return group.state.managed in VALID_MANAGED
    return False


async def get_is_autoenroll_user(
    loaders: Dataloaders,
    email: str,
    stakeholder: Stakeholder | None,
    trial: Trial | None,
) -> bool:
    active_group_names: Iterable[str] = await get_stakeholder_groups_names(loaders, email, True)
    groups_valid_managed = await collect(
        [get_group_valid_managed(loaders, group_name) for group_name in active_group_names],
    )
    unauthorized_user = bool(
        (stakeholder is None)
        or (stakeholder and not stakeholder.enrolled)
        or (trial and trial.completed),
    )
    return unauthorized_user and not any(groups_valid_managed)


async def send_autoenroll_mixpanel_event(
    loaders: Dataloaders,
    email: str,
    stakeholder: Stakeholder | None = None,
) -> None:
    trial = await loaders.trial.load(email)
    if await get_is_autoenroll_user(loaders, email, stakeholder, trial):
        if await is_personal_email(email):
            await analytics.mixpanel_track(email, "AutoenrollCorporateOnly")
            add_audit_event(
                AuditEvent(
                    author=email,
                    action="READ",
                    metadata={},
                    object="Autoenroll.CorporateOnly",
                    object_id="unknown",
                )
            )
        elif trial and trial.completed:
            await analytics.mixpanel_track(email, "AutoenrollAlreadyInTrial")
            add_audit_event(
                AuditEvent(
                    author=email,
                    action="READ",
                    metadata={},
                    object="Autoenroll.AlreadyInTrial",
                    object_id="unknown",
                )
            )
        else:
            await analytics.mixpanel_track(email, "AutoenrollmentWelcome")
            add_audit_event(
                AuditEvent(
                    author=email,
                    action="READ",
                    metadata={},
                    object="Autoenroll.Welcome",
                    object_id="unknown",
                )
            )
    else:
        await analytics.mixpanel_track(email, "CurrentStakeholder")
