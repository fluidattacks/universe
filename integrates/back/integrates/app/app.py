# Starlette app init file

import asyncio
import logging
import logging.config

import bugsnag
from aioextensions import (
    in_thread,
)
from bugsnag.asgi import (
    BugsnagMiddleware,
)
from starlette.applications import (
    Starlette,
)
from starlette.middleware import (
    Middleware,
)
from starlette.middleware.sessions import (
    SessionMiddleware,
)
from starlette.requests import (
    Request,
)
from starlette.responses import (
    HTMLResponse,
    RedirectResponse,
)
from starlette.routing import (
    Mount,
    Route,
    WebSocketRoute,
)
from starlette.staticfiles import (
    StaticFiles,
)

from integrates.api import (
    IntegratesAPI,
)
from integrates.app.views.oauth import (
    do_azure_oauth,
    do_bitbucket_oauth,
    do_github_oauth,
    do_gitlab_oauth,
    oauth_azure,
    oauth_bitbucket,
    oauth_github,
    oauth_gitlab,
)
from integrates.audit import AuditEvent, add_audit_event
from integrates.batch.resources import (
    batch_client_shutdown,
    batch_client_startup,
)
from integrates.context import (
    FI_ENVIRONMENT,
    FI_STARLETTE_SESSION_KEY,
)
from integrates.custom_exceptions import (
    ExpiredToken,
    InvalidAuthorization,
    MailerClientError,
    SecureAccessException,
    StakeholderNotInGroup,
    StakeholderNotInOrganization,
)
from integrates.custom_utils import (
    analytics,
    templates,
)
from integrates.custom_utils.groups import get_group
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.organization_access.types import (
    OrganizationAccess,
    OrganizationAccessRequest,
)
from integrates.decorators import (
    authenticate_session,
)
from integrates.dynamodb.resource import (
    dynamo_shutdown,
    dynamo_startup,
)
from integrates.group_access import (
    domain as group_access_domain,
)
from integrates.groups import (
    azure_issues_integration,
    gitlab_issues_integration,
)
from integrates.groups import (
    domain as groups_domain,
)
from integrates.jira_security_integration import (
    endpoints as jira_integration_endpoints,
)
from integrates.mailer.common import (
    unsubscribe,
)
from integrates.marketplace.client import (
    marketplace_shutdown,
    marketplace_startup,
)
from integrates.marketplace.core import (
    resolve_aws_marketplace_token,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.organizations import (
    utils as orgs_utils,
)
from integrates.organizations.domain import (
    complete_register_for_organization_invitation as register_org_invitation,
)
from integrates.remove_stakeholder import (
    domain as remove_stakeholder_domain,
)
from integrates.s3.resource import (
    s3_shutdown,
    s3_startup,
)
from integrates.search.client import (
    search_shutdown,
    search_startup,
)
from integrates.sessions import (
    domain as sessions_domain,
)
from integrates.settings import (
    DEBUG,
    JWT_COOKIE_NAME,
    LOGGING,
    TEMPLATES_DIR,
)
from integrates.settings.session import (
    SESSION_COOKIE_SAME_SITE,
)
from integrates.sqs.resource import (
    sqs_shutdown,
    sqs_startup,
)
from integrates.telemetry import (
    instrumentation,
)

from .middleware import (
    CustomRequestMiddleware,
)
from .views import (
    auth,
    charts,
    evidence,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)


@authenticate_session
async def app(request: Request) -> HTMLResponse | RedirectResponse:
    """View for authenticated users."""
    user_info = await sessions_domain.get_jwt_content(request)
    email = user_info["user_email"]
    try:
        if FI_ENVIRONMENT == "production":
            await sessions_domain.check_session_web_validity(request, email)
        response = templates.main_app(request)
    except SecureAccessException:
        return await logout(request)
    return response


async def confirm_access(request: Request) -> HTMLResponse:
    url_token = request.path_params.get("url_token")
    loaders: Dataloaders = get_new_context()
    if url_token:
        try:
            group_access = await group_access_domain.get_access_by_url_token(loaders, url_token)
            if not group_access.state.invitation:
                raise InvalidAuthorization()
            await groups_domain.complete_register_for_group_invitation(
                loaders,
                group_access,
                group_access.email,
            )

            group = await get_group(loaders, group_access.group_name)

            org_access = await loaders.organization_access.load(
                OrganizationAccessRequest(
                    organization_id=group.organization_id,
                    email=group_access.email,
                ),
            )

            if org_access and org_access.state.invitation:
                await register_org_invitation(loaders, org_access)

            response = templates.valid_invitation(request, group_access.group_name)
        except (StakeholderNotInGroup, InvalidAuthorization):
            await in_thread(bugsnag.notify, Exception("Invalid token"), severity="warning")
            response = templates.invalid_invitation(request, "Invalid or Expired")
    else:
        response = templates.invalid_invitation(request, "Invalid or Expired")
    return response


async def confirm_deletion(request: Request) -> HTMLResponse:
    url_token = request.path_params.get("url_token")
    loaders: Dataloaders = get_new_context()
    if url_token:
        try:
            user_email: str = await remove_stakeholder_domain.get_email_from_url_token(
                loaders=loaders,
                url_token=url_token,
            )
            if user_email:
                await remove_stakeholder_domain.complete_deletion(email=user_email)
                response = templates.confirm_deletion(request=request)
                response.delete_cookie(key=JWT_COOKIE_NAME)
                response.headers["Clear-Site-Data"] = '"executionContexts", "cache"'
            else:
                await in_thread(
                    bugsnag.notify,
                    Exception("Invalid token"),
                    severity="error",
                )
                response = templates.invalid_confirm_deletion(
                    request=request,
                    error="Invalid or Expired",
                )
        except (StakeholderNotInGroup, InvalidAuthorization):
            response = templates.invalid_confirm_deletion(
                request=request,
                error="Invalid or Expired",
            )
    else:
        response = templates.invalid_confirm_deletion(request=request, error="Invalid or Expired")

    return response


async def confirm_access_organization(request: Request) -> HTMLResponse:
    url_token = request.path_params.get("url_token")
    loaders: Dataloaders = get_new_context()
    if url_token:
        try:
            organization_access = await orgs_domain.get_access_by_url_token(
                loaders,
                url_token,
            )
            if not organization_access.state.invitation:
                raise InvalidAuthorization()
            await orgs_domain.complete_register_for_organization_invitation(
                loaders,
                organization_access,
            )
            organization = await orgs_utils.get_organization(
                loaders,
                organization_access.organization_id,
            )
            response = templates.valid_invitation(request, organization.name)
        except (StakeholderNotInOrganization, InvalidAuthorization):
            await in_thread(bugsnag.notify, Exception("Invalid token"), severity="warning")
            response = templates.invalid_invitation(request, "Invalid or Expired")
    else:
        response = templates.invalid_invitation(request, "Invalid or Expired")
    return response


async def reject_access(request: Request) -> HTMLResponse:
    url_token = request.path_params.get("url_token")
    loaders: Dataloaders = get_new_context()
    if url_token:
        try:
            group_access = await group_access_domain.get_access_by_url_token(loaders, url_token)
            invitation = group_access.state.invitation
            if invitation and invitation.is_used:
                return templates.invalid_invitation(
                    request,
                    "Invalid or Expired",
                    group_access.group_name,
                )
            await groups_domain.reject_register_for_group_invitation(loaders, group_access)
            response = templates.reject_invitation(request, group_access.group_name)
        except (StakeholderNotInGroup, InvalidAuthorization):
            await in_thread(bugsnag.notify, Exception("Invalid token"), severity="warning")
            response = templates.invalid_invitation(request, "Invalid or Expired")
    else:
        response = templates.invalid_invitation(request, "Invalid or Expired")
    return response


async def reject_access_organization(request: Request) -> HTMLResponse:
    url_token = request.path_params.get("url_token")
    loaders: Dataloaders = get_new_context()
    if url_token:
        try:
            organization_access: OrganizationAccess = await orgs_domain.get_access_by_url_token(
                loaders,
                url_token,
            )
            await orgs_domain.reject_register_for_organization_invitation(organization_access)
            organization = await orgs_utils.get_organization(
                loaders,
                organization_access.organization_id,
            )
            response = templates.reject_invitation(request, organization.name)
        except (StakeholderNotInOrganization, InvalidAuthorization):
            await in_thread(bugsnag.notify, Exception("Invalid token"), severity="warning")
            response = templates.invalid_invitation(request, "Invalid or Expired")
    else:
        response = templates.invalid_invitation(request, "Invalid or Expired")
    return response


async def logout(request: Request) -> RedirectResponse:
    """Close a user's active session."""
    try:
        user_info = await sessions_domain.get_jwt_content(request)
    except (ExpiredToken, InvalidAuthorization):
        request.session.clear()
        response = RedirectResponse("/")
        response.delete_cookie(key=JWT_COOKIE_NAME)
        response.headers["Clear-Site-Data"] = '"executionContexts", "cache"'

        return response

    user_email = user_info["user_email"]
    add_audit_event(
        AuditEvent(
            action="DELETE",
            author=user_email,
            metadata={},
            object="Session",
            object_id=user_email,
        ),
    )
    await asyncio.gather(
        sessions_domain.remove_session_token(user_info, user_email),
        sessions_domain.remove_session_key(user_email),
        analytics.mixpanel_track(user_email, "Logout"),
    )

    request.session.clear()
    response = RedirectResponse("/")
    response.delete_cookie(key=JWT_COOKIE_NAME)
    response.headers["Clear-Site-Data"] = '"executionContexts", "cache"'
    return response


async def sign_up(request: Request) -> HTMLResponse:
    if (
        request.method == "POST"
        and request.headers["content-type"] == "application/x-www-form-urlencoded"
    ):
        async with request.form() as form_data:
            if "x-amzn-marketplace-token" in form_data:
                aws_marketplace_token = str(form_data["x-amzn-marketplace-token"])
                customer_id = await resolve_aws_marketplace_token(request, aws_marketplace_token)
                if customer_id is not None:
                    request.session["aws_customer_id"] = customer_id

    return templates.login(request)


async def handle_unsubscribe(request: Request) -> HTMLResponse:
    url_token = request.path_params.get("url_token")
    if not url_token:
        return templates.login(request)

    if request.method == "POST":
        try:
            email = await group_access_domain.get_email_url_token(url_token)
            await unsubscribe(email)
            return templates.unsubscribe(request=request, confirmed=True)
        except (InvalidAuthorization, MailerClientError) as ex:
            await in_thread(
                bugsnag.notify,
                ex,
                severity="warning",
            )
            return templates.login(request)
    return templates.unsubscribe(request=request)


def not_found(request: Request, ex: Exception) -> HTMLResponse:
    LOGGER.error(
        "Resource not found",
        extra={"extra": {"exception": ex, "request": request.url}},
    )
    return templates.error401(request)


def server_error(request: Request, ex: Exception) -> HTMLResponse:
    LOGGER.error(
        "Server error",
        extra={
            "extra": {
                "request_body": request.json,
                "exception": ex,
            },
        },
    )
    return templates.error500(request)


exception_handlers = {404: not_found, 500: server_error}

APP = Starlette(
    debug=DEBUG,
    on_startup=[
        instrumentation.initialize,
        dynamo_startup,
        s3_startup,
        sqs_startup,
        search_startup,
        batch_client_startup,
        marketplace_startup,
    ],
    on_shutdown=[
        dynamo_shutdown,
        search_shutdown,
        s3_shutdown,
        sqs_shutdown,
        batch_client_shutdown,
        marketplace_shutdown,
    ],
    routes=[
        Route("/", templates.login),
        Route("/api", IntegratesAPI),
        WebSocketRoute("/api", IntegratesAPI),
        Route("/authz_azure", auth.authz_azure),
        Route("/dgitlab", do_gitlab_oauth),
        Route("/oauth_gitlab", oauth_gitlab),
        Route("/dgithub", do_github_oauth),
        Route("/oauth_github", oauth_github),
        Route("/dbitbucket", do_bitbucket_oauth),
        Route("/dazure", do_azure_oauth),
        Route("/oauth_azure", oauth_azure),
        Route(
            "/begin_azure_issues_oauth",
            azure_issues_integration.begin_oauth_flow,
            name="begin_azure_issues_oauth",
        ),
        Route(
            "/end_azure_issues_oauth",
            azure_issues_integration.end_oauth_flow,
            name="end_azure_issues_oauth",
        ),
        Route(
            "/begin_gitlab_issues_oauth",
            gitlab_issues_integration.begin_oauth_flow,
            name="begin_gitlab_issues_oauth",
        ),
        Route(
            "/end_gitlab_issues_oauth",
            gitlab_issues_integration.end_oauth_flow,
            name="end_gitlab_issues_oauth",
        ),
        Route("/oauth_bitbucket", oauth_bitbucket),
        Route("/authz_bitbucket", auth.authz_bitbucket),
        Route("/authz_google", auth.authz_google),
        Route("/confirm_access/{url_token:path}", confirm_access),
        Route(
            "/confirm_access_organization/{url_token:path}",
            confirm_access_organization,
        ),
        Route("/confirm_deletion/{url_token:path}", confirm_deletion),
        Route("/dglogin", auth.do_google_login),
        Route("/dalogin", auth.do_azure_login),
        Route("/dblogin", auth.do_bitbucket_login),
        Route("/graphic", charts.graphic),
        Route("/graphic-csv", charts.graphic_csv),
        Route("/graphics-for-group", charts.graphics_for_group),
        Route("/graphics-for-organization", charts.graphics_for_organization),
        Route("/graphics-for-portfolio", charts.graphics_for_portfolio),
        Route("/graphics-report", charts.graphics_report),
        Route("/invalid_invitation", templates.invalid_invitation),
        Route("/logout", logout),
        Route(
            "/orgs/{org_name:str}/groups/"
            "{group_name:str}/{evidence_type:str}/"
            "{finding_id:str}/locations/{file_id:str}",
            templates.login,
        ),
        Route(
            "/orgs/{org_name:str}/groups/"
            "{group_name:str}/events/"
            "{finding_id:str}/evidence/{file_id:str}",
            evidence.get_event_evidence,
        ),
        Route(
            "/orgs/{org_name:str}/groups/"
            "{group_name:str}/vulns/"
            "{finding_id:str}/evidence/{file_id:str}",
            evidence.get_finding_evidence,
        ),
        Route("/reject_access/{url_token:path}", reject_access),
        Route(
            "/reject_access_organization/{url_token:path}",
            reject_access_organization,
        ),
        Route("/SignUp", sign_up, methods=["GET", "POST"]),
        Mount("/jira-security", routes=jira_integration_endpoints.ROUTES),
        Mount(
            "/static",
            StaticFiles(directory=f"{TEMPLATES_DIR}/static"),
            name="static",
        ),
        Mount(
            "/.well-known",
            StaticFiles(directory=f"{TEMPLATES_DIR}/well_known"),
            name="security",
        ),
        Route(
            "/verify_otp",
            endpoint=auth.verify_otp,
            methods=["POST"],
            name="verify_otp",
        ),
        Route(
            "/unsubscribe/{url_token:path}",
            handle_unsubscribe,
            methods=["GET", "POST"],
        ),
        Route("/{full_path:path}", app),
    ],
    middleware=[
        Middleware(BugsnagMiddleware),
        Middleware(
            SessionMiddleware,
            secret_key=FI_STARLETTE_SESSION_KEY,
            same_site=SESSION_COOKIE_SAME_SITE,
            https_only=True,
        ),
        Middleware(CustomRequestMiddleware),
    ],
    exception_handlers=exception_handlers,
)

instrumentation.instrument_app(APP)
