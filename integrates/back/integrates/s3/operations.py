import asyncio
import io
import logging
import logging.config
from contextlib import (
    suppress,
)
from tempfile import (
    _TemporaryFileWrapper as TemporaryFileWrapper,
)

from aiohttp.client_exceptions import (
    ClientPayloadError,
)
from botocore.exceptions import (
    ClientError,
)
from starlette.datastructures import (
    UploadFile,
)
from types_aiobotocore_s3.type_defs import HeadObjectOutputTypeDef

from integrates.context import (
    FI_AWS_S3_MAIN_BUCKET,
    FI_AWS_S3_PATH_PREFIX,
)
from integrates.custom_exceptions import (
    DocumentNotFound,
    ErrorDownloadingFile,
    ErrorUploadingFileS3,
    FileNotFound,
    UnavailabilityError,
)
from integrates.s3.resource import (
    get_client,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)


async def download_file(
    file_name: str,
    file_path: str,
    bucket: str = FI_AWS_S3_MAIN_BUCKET,
    version_id: str | None = None,
) -> None:
    client = await get_client()
    try:
        await client.download_file(
            bucket,
            f"{FI_AWS_S3_PATH_PREFIX}{file_name}",
            file_path,
            ExtraArgs=({"VersionId": version_id}) if version_id else {},
        )
    except ClientPayloadError as exc:
        raise ErrorDownloadingFile() from exc


async def upload_file(
    object_key: str,
    file_path: str,
    bucket: str = FI_AWS_S3_MAIN_BUCKET,
) -> None:
    client = await get_client()
    try:
        await client.upload_file(file_path, bucket, f"{FI_AWS_S3_PATH_PREFIX}{object_key}")
    except ClientError as ex:
        LOGGER.exception(ex, extra={"extra": locals()})
        raise UnavailabilityError() from ex


async def list_files(
    name: str | None = None,
    bucket: str = FI_AWS_S3_MAIN_BUCKET,
) -> list[str]:
    client = await get_client()
    try:
        resp = await client.list_objects_v2(Bucket=bucket, Prefix=f"{FI_AWS_S3_PATH_PREFIX}{name}")

        return [item["Key"].replace(FI_AWS_S3_PATH_PREFIX, "") for item in resp.get("Contents", [])]
    except ClientError as exc:
        LOGGER.warning(exc, extra={"file_name": name})
        raise DocumentNotFound() from exc


async def file_exists(
    object_key: str,
    bucket: str = FI_AWS_S3_MAIN_BUCKET,
    version_id: str | None = None,
) -> bool:
    client = await get_client()
    with suppress(ClientError):
        if version_id:
            await client.head_object(Bucket=bucket, Key=object_key, VersionId=version_id)
        else:
            await client.head_object(Bucket=bucket, Key=object_key)
        return True

    return False


async def remove_file(name: str, bucket: str = FI_AWS_S3_MAIN_BUCKET) -> None:
    client = await get_client()
    try:
        response = await client.delete_object(Bucket=bucket, Key=f"{FI_AWS_S3_PATH_PREFIX}{name}")
        status_code = response["ResponseMetadata"]["HTTPStatusCode"]
        if status_code not in [200, 204]:
            raise UnavailabilityError().new()
    except ClientError as ex:
        LOGGER.exception(ex, extra={"extra": locals()})
        raise UnavailabilityError().new() from ex


async def sign_url(
    file_name: str,
    expire_seconds: int,
    bucket: str = FI_AWS_S3_MAIN_BUCKET,
    response_content_disposition: str | None = None,
) -> str:
    client = await get_client()
    key = f"{FI_AWS_S3_PATH_PREFIX}{file_name}"

    try:
        result = await client.generate_presigned_url(
            ClientMethod="get_object",
            Params={
                "Bucket": bucket,
                "Key": key,
                **(
                    {"ResponseContentDisposition": (response_content_disposition)}
                    if response_content_disposition
                    else {}
                ),
            },
            ExpiresIn=expire_seconds,
            HttpMethod="GET",
        )
        return str(result)
    except ClientError as ex:
        LOGGER.exception(ex, extra={"extra": locals()})
        raise UnavailabilityError() from ex


async def upload_memory_file(
    file_object: object,
    file_name: str,
    bucket: str = FI_AWS_S3_MAIN_BUCKET,
) -> None:
    valid_in_memory_files = (TemporaryFileWrapper, UploadFile)
    if not isinstance(file_object, valid_in_memory_files):
        LOGGER.error("Attempt to upload invalid memory file", extra={"extra": locals()})
        raise ErrorUploadingFileS3().new()

    bytes_object = io.BytesIO(await asyncio.to_thread(file_object.file.read))
    client = await get_client()
    try:
        await client.upload_fileobj(
            bytes_object,
            bucket,
            f"{FI_AWS_S3_PATH_PREFIX}{file_name.lstrip('/')}",
        )
    except ClientError as ex:
        LOGGER.exception(ex, extra={"extra": locals()})
        raise UnavailabilityError().new() from ex


async def sign_upload_url(
    file_name: str,
    expire_seconds: int,
    bucket: str = FI_AWS_S3_MAIN_BUCKET,
) -> dict[str, dict[str, str]]:
    params = {
        "conditions": [
            {"acl": "private"},
            {"bucket": bucket},
            ["starts-with", "$key", f"{FI_AWS_S3_PATH_PREFIX}{file_name}"],
            ["content-length-range", 1, 5368709120],
        ],
    }

    client = await get_client()
    try:
        return await client.generate_presigned_post(
            bucket,
            f"{FI_AWS_S3_PATH_PREFIX}{file_name}",
            Fields=None,
            Conditions=params["conditions"],
            ExpiresIn=expire_seconds,
        )
    except ClientError as ex:
        LOGGER.exception(ex, extra={"extra": locals()})
        raise UnavailabilityError() from ex


async def get_object_content(object_key: str, bucket: str = FI_AWS_S3_MAIN_BUCKET) -> bytes:
    client = await get_client()
    try:
        object_stream = await client.get_object(
            Bucket=bucket,
            Key=f"{FI_AWS_S3_PATH_PREFIX}{object_key}",
        )
        async with object_stream["Body"] as stream:
            return await stream.read()
    except ClientError as ex:
        LOGGER.exception(ex, extra={"extra": locals()})
        raise FileNotFound() from ex


async def file_head(
    object_key: str,
    bucket: str = FI_AWS_S3_MAIN_BUCKET,
    version_id: str | None = None,
) -> HeadObjectOutputTypeDef | None:
    client = await get_client()
    with suppress(ClientError):
        if version_id:
            return await client.head_object(
                Bucket=bucket,
                Key=f"{FI_AWS_S3_PATH_PREFIX}{object_key}",
                VersionId=version_id,
            )
        return await client.head_object(Bucket=bucket, Key=f"{FI_AWS_S3_PATH_PREFIX}{object_key}")

    return None
