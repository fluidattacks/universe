from collections.abc import (
    Awaitable,
    Callable,
)
from contextlib import (
    AsyncExitStack,
)
from typing import Protocol, cast

import aioboto3
from aiobotocore.config import (
    AioConfig,
)
from botocore import (
    UNSIGNED,
)
from types_aiobotocore_s3 import S3Client

from integrates.context import (
    FI_AWS_REGION_NAME,
)

CLIENT_CONFIG = AioConfig(
    # The time in seconds till a timeout exception is thrown when
    # attempting to make a connection. [60]
    connect_timeout=60,
    # Maximum amount of simultaneously opened connections. [10]
    # https://docs.aiohttp.org/en/stable/client_advanced.html#limiting-connection-pool-size
    max_pool_connections=2000,
    # The time in seconds till a timeout exception is thrown when
    # attempting to read from a connection. [60]
    read_timeout=60,
    # https://boto3.amazonaws.com/v1/documentation/api/latest/guide/retries.html
    retries={"max_attempts": 10, "mode": "standard"},
    # Signature version for signing URLs
    # https://boto3.amazonaws.com/v1/documentation/api/1.9.42/guide/s3.html#generating-presigned-urls
    signature_version="s3v4",
)
PUBLIC_CLIENT_CONFIG = AioConfig(
    connect_timeout=15,
    max_pool_connections=2000,
    read_timeout=30,
    retries={"max_attempts": 10, "mode": "standard"},
    signature_version=UNSIGNED,
)
SESSION = aioboto3.Session()


class StartupCallable(Protocol):
    async def __call__(self, *, is_public: bool = False) -> None: ...


ShutdownCallable = Callable[[], Awaitable[None]]
GetClientCallable = Callable[[], Awaitable[S3Client]]
S3Context = tuple[StartupCallable, ShutdownCallable, GetClientCallable]


def create_s3_context() -> S3Context:
    context_stack = None
    client = None

    async def _startup(*, is_public: bool = False) -> None:
        nonlocal context_stack, client

        context_stack = AsyncExitStack()

        client = await context_stack.enter_async_context(
            SESSION.client(
                config=PUBLIC_CLIENT_CONFIG if is_public else CLIENT_CONFIG,
                region_name=FI_AWS_REGION_NAME,
                service_name="s3",
                use_ssl=True,
                verify=True,
            ),
        )

    async def _shutdown() -> None:
        if context_stack:
            await context_stack.aclose()

    async def _get_client() -> S3Client:
        if client is None:
            await s3_startup()
        return cast(S3Client, client)

    return _startup, _shutdown, _get_client


s3_startup, s3_shutdown, get_client = create_s3_context()
