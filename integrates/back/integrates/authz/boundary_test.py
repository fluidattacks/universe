from integrates.authz.boundary import get_group_service_attributes
from integrates.custom_utils.groups import get_group
from integrates.dataloaders import get_new_context
from integrates.db_model.groups.enums import GroupService
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GroupFaker,
    GroupStateFaker,
    OrganizationFaker,
)
from integrates.testing.mocks import mocks


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[
                GroupFaker(
                    name="group1",
                    organization_id="org1",
                    state=GroupStateFaker(service=GroupService.WHITE),
                ),
            ],
        ),
    )
)
async def test_get_group_service_attributes() -> None:
    assert sorted(get_group_service_attributes(await get_group(get_new_context(), "group1"))) == [
        "can_report_vulnerabilities",
        "can_request_zero_risk",
        "has_advanced",
        "has_asm",
        "has_forces",
        "has_service_white",
        "is_continuous",
        "is_fluidattacks_customer",
        "must_only_have_fluidattacks_hackers",
    ]
