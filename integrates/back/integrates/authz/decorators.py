import functools
import logging
import logging.config
from collections.abc import (
    Callable,
)
from typing import (
    Any,
    TypeVar,
    cast,
)

from integrates.authz.operations import (
    check_access,
)
from integrates.authz.types import (
    AuthzRelation,
    AuthzTuple,
)
from integrates.custom_exceptions import (
    OrganizationNotFound,
    StakeholderNotInOrganization,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.db_model.organizations.types import (
    Organization,
)
from integrates.sessions import (
    domain as sessions_domain,
)
from integrates.settings import (
    LOGGING,
)

# Typing
TVar = TypeVar("TVar")

logging.config.dictConfig(LOGGING)


def _get_context(args: tuple[str, ...], kwargs: dict[str, Any]) -> Any:
    if len(args) > 1 and hasattr(args[1], "context"):
        # Called from resolver function
        return args[1].context
    # Called from custom function
    return kwargs["info"].context


def org_access(*, relations: list[AuthzRelation]) -> Callable[[TVar], TVar]:
    """
    Verifies if the user
    is related to the organization
    in at least one of the given relations.
    """

    def decorator(func: TVar) -> TVar:
        _func = cast(Callable[..., Any], func)

        @functools.wraps(_func)
        async def wrapper(*args: Any, **kwargs: Any) -> Any:
            context = _get_context(args, kwargs)
            organization_identifier = str(
                kwargs.get("identifier")
                or kwargs.get("organization_id")
                or kwargs.get("organization_name")
                or (getattr(args[0], "organization_id", None) if args else None),
            )
            user_data = await sessions_domain.get_jwt_content(context)
            user_email = user_data["user_email"]
            loaders = context.loaders
            organization: Organization | None = await loaders.organization.load(
                organization_identifier,
            )
            if not organization:
                raise OrganizationNotFound()
            has_access = await check_access(
                [
                    AuthzTuple(
                        user_type="user",
                        user=user_email,
                        relation=relation,
                        object_type="organization",
                        object=organization.name,
                    )
                    for relation in relations
                ],
            )
            if not has_access:
                logs_utils.cloudwatch_log(
                    context,
                    "Attempted to access the organization without permission",
                    extra={
                        "user_email": user_email,
                        "organization_identifier": organization_identifier,
                        "log_type": "Security",
                    },
                )
                raise StakeholderNotInOrganization()
            return await _func(*args, **kwargs)

        return cast(TVar, wrapper)

    return decorator
