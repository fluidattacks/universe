from integrates.authz.model.services import (
    SERVICE_ATTRIBUTES,
)


def test_model_roles_by_level() -> None:
    assert sorted(SERVICE_ATTRIBUTES.keys()) == [
        "advanced",
        "asm",
        "continuous",
        "forces",
        "is_under_review",
        "report_vulnerabilities",
        "request_zero_risk",
        "service_black",
        "service_white",
    ]
