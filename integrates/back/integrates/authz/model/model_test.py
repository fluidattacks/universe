from integrates.authz.model import (
    GROUP_LEVEL_ROLES,
    ORGANIZATION_LEVEL_ROLES,
    USER_LEVEL_ROLES,
    get_group_level_actions_by_role,
    get_organization_level_actions_by_role,
    get_user_level_actions_by_role,
)
from integrates.authz.model.types import RoleModel
from integrates.testing.utils import parametrize


@parametrize(
    args=["parameter", "expected"],
    cases=[
        [
            GROUP_LEVEL_ROLES,
            [
                "admin",
                "architect",
                "customer_manager",
                "group_manager",
                "hacker",
                "reattacker",
                "resourcer",
                "reviewer",
                "service_forces",
                "user",
                "vulnerability_manager",
            ],
        ],
        [
            USER_LEVEL_ROLES,
            [
                "admin",
                "hacker",
                "user",
            ],
        ],
        [
            ORGANIZATION_LEVEL_ROLES,
            [
                "admin",
                "customer_manager",
                "organization_manager",
                "resourcer",
                "user",
            ],
        ],
    ],
)
def test_model_roles_by_level(parameter: RoleModel, expected: list[str]) -> None:
    assert sorted(parameter.keys()) == expected


@parametrize(
    args=["parameter"],
    cases=[
        [GROUP_LEVEL_ROLES],
        [USER_LEVEL_ROLES],
    ],
)
def test_model_integrity_keys_actions_and_tags(parameter: RoleModel) -> None:
    for value in parameter.values():
        assert sorted(value.keys()) == ["actions", "tags"]


def test_valid_granted_role_organization() -> None:
    # https://help.fluidattacks.com/portal/en/kb/articles/understand-roles#Client_roles
    client_roles = {"user", "organization_manager"}
    for client_role in client_roles:
        for role in ORGANIZATION_LEVEL_ROLES:
            if role not in client_roles:
                assert (
                    f"grant_organization_level_role:{role}"
                    not in get_organization_level_actions_by_role(client_role)
                )


def test_valid_granted_role_user() -> None:
    # https://help.fluidattacks.com/portal/en/kb/articles/understand-roles#Client_roles
    client_roles = {"user"}
    for client_role in client_roles:
        for role in USER_LEVEL_ROLES:
            if role not in client_roles:
                assert f"grant_user_level_role:{role}" not in get_user_level_actions_by_role(
                    client_role
                )


def test_valid_granted_role_group() -> None:
    # https://help.fluidattacks.com/portal/en/kb/articles/understand-roles#Client_roles
    client_roles = {"user", "vulnerability_manager", "group_manager"}
    for client_role in client_roles:
        for role in GROUP_LEVEL_ROLES:
            if role not in client_roles:
                assert f"grant_group_level_role:{role}" not in get_group_level_actions_by_role(
                    client_role
                )
