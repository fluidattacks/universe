from datetime import datetime

from integrates.custom_exceptions import (
    InvalidIpAddressInRoot,
    InvalidPort,
    InvalidToePortAttackedAt,
    RepeatedToePort,
    RootNotFound,
    ToePortNotPresent,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.toe_ports.types import (
    RootToePortsRequest,
    ToePortRequest,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    IPRootFaker,
    IPRootStateFaker,
    OrganizationFaker,
    StakeholderFaker,
    ToePortFaker,
    ToePortStateFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import freeze_time, raises
from integrates.toe.ports.domain import add, update
from integrates.toe.ports.types import (
    ToePortAttributesToAdd,
    ToePortAttributesToUpdate,
)

EMAIL_TEST = "hacker@fluidattacks.com"
GROUP_NAME = "group1"
ROOT_ID = "root1"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_TEST, role="user"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=EMAIL_TEST,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
            ],
            roots=[
                IPRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=IPRootStateFaker(
                        address="192.168.0.1",
                        nickname="back",
                    ),
                ),
            ],
        ),
    )
)
async def test_add_toe_ports_success() -> None:
    loaders: Dataloaders = get_new_context()

    await add(
        loaders=loaders,
        group_name=GROUP_NAME,
        root_id=ROOT_ID,
        address="192.168.0.1",
        port="8080",
        attributes=ToePortAttributesToAdd(
            be_present=True,
            has_vulnerabilities=False,
            seen_first_time_by=EMAIL_TEST,
        ),
        modified_by=EMAIL_TEST,
    )

    root_toe_ports = await loaders.root_toe_ports.load_nodes(
        RootToePortsRequest(group_name=GROUP_NAME, root_id=ROOT_ID),
    )

    assert len(root_toe_ports) == 1
    assert root_toe_ports[0].address == "192.168.0.1"
    assert root_toe_ports[0].port == "8080"
    assert root_toe_ports[0].state.be_present
    assert not root_toe_ports[0].state.has_vulnerabilities
    assert root_toe_ports[0].state.seen_first_time_by == EMAIL_TEST


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_TEST, role="user"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=EMAIL_TEST,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
            ],
            roots=[
                IPRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=IPRootStateFaker(
                        address="192.168.0.1",
                        nickname="back",
                    ),
                ),
            ],
            toe_ports=[
                ToePortFaker(
                    address="192.168.0.1",
                    port="400",
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=ToePortStateFaker(be_present=True),
                )
            ],
        ),
    )
)
async def test_add_toe_ports_already_exist() -> None:
    loaders: Dataloaders = get_new_context()
    with raises(RepeatedToePort):
        await add(
            loaders=loaders,
            group_name=GROUP_NAME,
            root_id=ROOT_ID,
            address="192.168.0.1",
            port="400",
            attributes=ToePortAttributesToAdd(be_present=True),
            modified_by=EMAIL_TEST,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_TEST, role="user"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=EMAIL_TEST,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
            ],
            roots=[],
        ),
    )
)
async def test_add_toe_ports_root_not_found() -> None:
    loaders: Dataloaders = get_new_context()
    with raises(RootNotFound):
        await add(
            loaders=loaders,
            group_name=GROUP_NAME,
            root_id=ROOT_ID,
            address="192.168.0.1",
            port="400",
            attributes=ToePortAttributesToAdd(be_present=True),
            modified_by=EMAIL_TEST,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_TEST, role="user"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=EMAIL_TEST,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
            ],
            roots=[
                IPRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=IPRootStateFaker(
                        address="192.168.0.1",
                        nickname="back",
                    ),
                ),
            ],
        ),
    )
)
async def test_add_toe_ports_ip_does_not_exist() -> None:
    loaders: Dataloaders = get_new_context()
    with raises(InvalidIpAddressInRoot):
        await add(
            loaders=loaders,
            group_name=GROUP_NAME,
            root_id=ROOT_ID,
            address="100.0.0.127",
            port="8080",
            attributes=ToePortAttributesToAdd(be_present=True),
            modified_by=EMAIL_TEST,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_TEST, role="user"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=EMAIL_TEST,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
            ],
            roots=[
                IPRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=IPRootStateFaker(
                        address="192.168.0.1",
                        nickname="back",
                    ),
                ),
            ],
        ),
    )
)
async def test_add_toe_ports_invalid_port() -> None:
    loaders: Dataloaders = get_new_context()
    with raises(InvalidPort):
        await add(
            loaders=loaders,
            group_name=GROUP_NAME,
            root_id=ROOT_ID,
            address="192.168.0.1",
            port="9999999999",
            attributes=ToePortAttributesToAdd(be_present=True),
            modified_by=EMAIL_TEST,
        )


ADRESS = "192.180.0.0"
PORT = "8080"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            roots=[
                IPRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=IPRootStateFaker(
                        nickname="back",
                    ),
                ),
            ],
            toe_ports=[
                ToePortFaker(
                    address=ADRESS,
                    port=PORT,
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=ToePortStateFaker(
                        be_present=False,
                        attacked_by="other_hacker@fluidattacks.com",
                    ),
                ),
            ],
        ),
    ),
)
@freeze_time("2024-10-14T00:00:00")
async def test_update_toe_input() -> None:
    loaders: Dataloaders = get_new_context()
    current_value = await loaders.toe_port.load(
        ToePortRequest(
            address=ADRESS,
            port=str(PORT),
            group_name=GROUP_NAME,
            root_id=ROOT_ID,
        ),
    )
    assert current_value
    assert current_value.state.be_present is False
    assert current_value.state.attacked_by == "other_hacker@fluidattacks.com"

    await update(
        current_value=current_value,
        attributes=ToePortAttributesToUpdate(
            attacked_at=datetime_utils.get_utc_now(),
            attacked_by="new_hacker@fluidattacks.com",
            be_present=True,
        ),
        modified_by="new_hacker@fluidattacks.com",
    )

    toe_port_updated = await get_new_context().toe_port.load(
        ToePortRequest(
            address=ADRESS,
            port=str(PORT),
            group_name=GROUP_NAME,
            root_id=ROOT_ID,
        ),
    )
    assert toe_port_updated
    assert toe_port_updated.state.be_present is True
    toe_port_attacked_at = toe_port_updated.state.attacked_at
    assert toe_port_attacked_at
    assert (toe_port_attacked_at.year, toe_port_attacked_at.month, toe_port_attacked_at.day) == (
        2024,
        10,
        14,
    )
    assert toe_port_updated.state.attacked_by == "new_hacker@fluidattacks.com"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            roots=[
                IPRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=IPRootStateFaker(
                        nickname="back",
                    ),
                ),
            ],
            toe_ports=[
                ToePortFaker(
                    address=ADRESS,
                    port=PORT,
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=ToePortStateFaker(
                        be_present=False,
                        attacked_by="other_hacker@fluidattacks.com",
                    ),
                ),
            ],
        ),
    ),
)
@freeze_time("2024-10-14T00:00:00")
async def test_update_toe_input_not_present() -> None:
    loaders: Dataloaders = get_new_context()
    current_value = await loaders.toe_port.load(
        ToePortRequest(
            address=ADRESS,
            port=str(PORT),
            group_name=GROUP_NAME,
            root_id=ROOT_ID,
        ),
    )
    assert current_value
    with raises(ToePortNotPresent):
        await update(
            current_value=current_value,
            attributes=ToePortAttributesToUpdate(
                attacked_at=datetime_utils.get_utc_now(),
                attacked_by="new_hacker@fluidattacks.com",
                be_present=False,
            ),
            modified_by="new_hacker@fluidattacks.com",
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            roots=[
                IPRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=IPRootStateFaker(
                        nickname="back",
                    ),
                ),
            ],
            toe_ports=[
                ToePortFaker(
                    address=ADRESS,
                    port=PORT,
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=ToePortStateFaker(
                        be_present=False,
                        attacked_by="other_hacker@fluidattacks.com",
                        seen_at=datetime.fromisoformat("2024-12-20T14:30:00+00:00"),
                    ),
                ),
            ],
        ),
    ),
)
@freeze_time("2024-12-30T00:00:00")
async def test_update_toe_input_wrong_attacked_at() -> None:
    loaders: Dataloaders = get_new_context()
    current_value = await loaders.toe_port.load(
        ToePortRequest(
            address=ADRESS,
            port=str(PORT),
            group_name=GROUP_NAME,
            root_id=ROOT_ID,
        ),
    )
    assert current_value
    with raises(InvalidToePortAttackedAt):
        await update(
            current_value=current_value,
            attributes=ToePortAttributesToUpdate(
                attacked_at=datetime.fromisoformat("2024-12-10T14:30:00+00:00"),
                attacked_by="new_hacker@fluidattacks.com",
                be_present=True,
            ),
            modified_by="new_hacker@fluidattacks.com",
        )
