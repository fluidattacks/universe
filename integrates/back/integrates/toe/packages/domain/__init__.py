from .core import (
    get_package,
)

__all__ = [
    "get_package",
]
