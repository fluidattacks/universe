import os
from datetime import (
    datetime,
)
from typing import (
    TypeVar,
)

import simplejson as json

from integrates.custom_exceptions import (
    FileIsExcluded,
    InactiveRoot,
    InvalidGitRoot,
    InvalidToeLinesAttackAt,
    InvalidToeLinesAttackedBy,
    InvalidToeLinesAttackedLines,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.custom_utils import (
    roots as roots_utils,
)
from integrates.custom_utils import (
    validations,
)
from integrates.custom_utils.files import (
    match_files,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model import (
    toe_lines as toe_lines_model,
)
from integrates.db_model.roots.enums import (
    RootStatus,
    RootType,
)
from integrates.db_model.roots.types import (
    GitRoot,
    Root,
)
from integrates.db_model.toe_lines.types import (
    ToeLine,
    ToeLineState,
)
from integrates.toe.lines.constants import (
    CHECKED_FILES,
    MACHINE_SCA_CHECKS,
)
from integrates.toe.lines.utils import (
    get_filename_extension,
)
from integrates.toe.lines.validations import (
    validate_file_name,
    validate_loc,
    validate_modified_date,
)
from integrates.toe.utils import (
    get_has_vulnerabilities,
)

from .types import (
    ToeLinesAttributesToAdd,
    ToeLinesAttributesToUpdate,
)

_T = TypeVar("_T")


def _get_optional_be_present_until(be_present: bool) -> datetime | None:
    return datetime_utils.get_utc_now() if be_present is False else None


def _is_machine_sca_file(filename: str) -> bool:
    file_info = os.path.split(filename)[1]
    return file_info in MACHINE_SCA_CHECKS


def _assign_attacked_lines(filename: str, attributes: ToeLinesAttributesToAdd) -> int:
    if get_filename_extension(filename) in CHECKED_FILES:
        return attributes.loc
    if (
        attributes.attacked_at
        and attributes.last_commit_date
        and attributes.attacked_at <= attributes.last_commit_date
    ):
        return attributes.attacked_lines
    return 0


def _assign_toe_lines(
    *,
    attributes: ToeLinesAttributesToAdd,
    filename: str,
    group_name: str,
    root: Root,
) -> ToeLine:
    be_present_until = attributes.be_present_until or _get_optional_be_present_until(
        attributes.be_present,
    )
    first_attack_at = attributes.first_attack_at or attributes.attacked_at
    has_vulnerabilities = get_has_vulnerabilities(
        attributes.be_present,
        attributes.has_vulnerabilities,
    )
    current_date = datetime_utils.get_utc_now()
    seen_at = attributes.seen_at or current_date
    attacked_at = attributes.attacked_at
    attacked_by = attributes.attacked_by if attributes.attacked_by else ""
    attacked_lines = _assign_attacked_lines(filename, attributes)
    if _is_machine_sca_file(filename):
        attacked_lines = attributes.loc
        attacked_at = seen_at
        attacked_by = "machine@fluidattacks.com"

    if attacked_lines != 0:
        first_attack_at = current_date if not first_attack_at else first_attack_at
        attacked_at = current_date if not attacked_at else attacked_at

    return ToeLine(
        filename=filename,
        group_name=group_name,
        root_id=root.id,
        seen_first_time_by=attributes.seen_first_time_by,
        state=ToeLineState(
            attacked_at=attacked_at,
            attacked_by=attacked_by,
            attacked_lines=attacked_lines,
            be_present=attributes.be_present,
            be_present_until=be_present_until,
            comments=attributes.comments,
            first_attack_at=first_attack_at,
            has_vulnerabilities=has_vulnerabilities,
            modified_by=attributes.seen_first_time_by
            if attributes.seen_first_time_by
            else "machine@fluidattacks.com",
            modified_date=datetime_utils.get_utc_now(),
            last_author=attributes.last_author,
            last_commit=attributes.last_commit,
            last_commit_date=attributes.last_commit_date,
            loc=attributes.loc,
            seen_at=seen_at,
            sorts_risk_level=attributes.sorts_risk_level,
            sorts_priority_factor=attributes.sorts_priority_factor,
        ),
    )


def _validate_add(
    attributes: ToeLinesAttributesToAdd,
    filename: str,
    root: Root,
) -> None:
    if attributes.seen_first_time_by is not None:
        validations.check_email(attributes.seen_first_time_by)
    if root.state.status != RootStatus.ACTIVE:
        raise InactiveRoot()
    if root.type != RootType.GIT:
        raise InvalidGitRoot()
    validations.validate_sanitized_csv_input(attributes.last_author, filename)
    validations.check_email(attributes.last_author)
    validations.check_commit_hash(attributes.last_commit)
    validate_file_name(filename)
    validate_loc(attributes.loc)
    validate_modified_date(attributes.last_commit_date)


async def add(
    *,
    loaders: Dataloaders,
    group_name: str,
    root_id: str,
    filename: str,
    attributes: ToeLinesAttributesToAdd,
    rules_excluded: bool = False,
) -> None:
    root = await roots_utils.get_root(loaders, root_id, group_name)
    if isinstance(root, GitRoot) and tuple(match_files(root.state.gitignore, [filename])):
        raise FileIsExcluded()

    if not rules_excluded:
        _validate_add(attributes, filename, root)

    toe_lines = _assign_toe_lines(
        attributes=attributes,
        filename=filename,
        group_name=group_name,
        root=root,
    )

    await toe_lines_model.add(toe_lines=toe_lines)


def _get_first_attack_at(
    attributes: ToeLinesAttributesToUpdate,
    current_value: ToeLine,
) -> datetime | None:
    first_attack_at = current_value.state.first_attack_at
    if attributes.first_attack_at is not None:
        first_attack_at = attributes.first_attack_at
    elif not current_value.state.first_attack_at and attributes.attacked_at:
        first_attack_at = attributes.attacked_at
    return first_attack_at


def _get_attacked_lines(
    attributes: ToeLinesAttributesToUpdate,
    current_value: ToeLine,
    loc: int,
) -> int:
    last_commit_date = attributes.last_commit_date or current_value.state.last_commit_date
    last_attacked_at = attributes.attacked_at or current_value.state.attacked_at
    if get_filename_extension(current_value.filename) in CHECKED_FILES or _is_machine_sca_file(
        current_value.filename,
    ):
        return loc
    if attributes.attacked_lines is None:
        return (
            current_value.state.attacked_lines if current_value.state.attacked_lines <= loc else 0
        )
    if attributes.attacked_lines != 0 and last_attacked_at and last_commit_date <= last_attacked_at:
        return min(
            attributes.attacked_lines or current_value.state.attacked_lines,
            loc,
        )
    return 0


def _get_be_present_until(
    attributes: ToeLinesAttributesToUpdate,
    current_value: ToeLine,
) -> datetime | None:
    return (
        current_value.state.be_present_until
        if attributes.be_present is None
        else _get_optional_be_present_until(attributes.be_present)
    )


def _value_or(item: _T | None, default: _T) -> _T:
    if item is not None:
        return item
    return default


def new_state_toe_lines(
    attributes: ToeLinesAttributesToUpdate,
    be_present_until: datetime | None,
    current_value: ToeLine,
    has_vulnerabilities: bool,
    loc: int,
) -> ToeLineState:
    modified_by = attributes.modified_by or "machine@fluidattacks.com"
    first_attack_at = _get_first_attack_at(attributes, current_value)
    attacked_at = _value_or(attributes.attacked_at, current_value.state.attacked_at)
    attacked_lines = _get_attacked_lines(attributes, current_value, loc)

    if attacked_lines != 0:
        first_attack_at = datetime_utils.get_utc_now() if not first_attack_at else first_attack_at
        attacked_at = datetime_utils.get_utc_now() if not attacked_at else attacked_at

    return ToeLineState(
        attacked_at=attacked_at,
        attacked_by=_value_or(attributes.attacked_by, current_value.state.attacked_by),
        attacked_lines=attacked_lines,
        be_present=_value_or(attributes.be_present, current_value.state.be_present),
        be_present_until=be_present_until,
        comments=_value_or(attributes.comments, current_value.state.comments),
        last_author=_value_or(attributes.last_author, current_value.state.last_author),
        first_attack_at=first_attack_at,
        has_vulnerabilities=has_vulnerabilities,
        loc=_value_or(attributes.loc, current_value.state.loc),
        last_commit=_value_or(attributes.last_commit, current_value.state.last_commit),
        last_commit_date=_value_or(
            attributes.last_commit_date,
            current_value.state.last_commit_date,
        ),
        modified_by=attributes.attacked_by if attributes.attacked_by else modified_by,
        modified_date=datetime_utils.get_utc_now(),
        seen_at=_value_or(attributes.seen_at, current_value.state.seen_at),
        sorts_risk_level=_value_or(
            attributes.sorts_risk_level,
            current_value.state.sorts_risk_level,
        ),
        sorts_priority_factor=_value_or(
            attributes.sorts_priority_factor,
            current_value.state.sorts_priority_factor,
        ),
        sorts_risk_level_date=_value_or(
            attributes.sorts_risk_level_date,
            current_value.state.sorts_risk_level_date,
        ),
        sorts_suggestions=json.loads(json.dumps(attributes.sorts_suggestions))
        if attributes.sorts_suggestions is not None
        else current_value.state.sorts_suggestions,
    )


def _validate_update(
    current_value: ToeLine,
    attributes: ToeLinesAttributesToUpdate,
) -> None:
    loc = attributes.loc if attributes.loc is not None else current_value.state.loc

    if (attributes.attacked_at is not None and current_value.state.attacked_at is not None) and (
        attributes.attacked_at <= current_value.state.attacked_at
        or attributes.attacked_at > datetime_utils.get_utc_now()
        or attributes.attacked_at < current_value.state.seen_at
    ):
        raise InvalidToeLinesAttackAt()

    if attributes.attacked_lines is not None and not (0 <= attributes.attacked_lines <= loc):
        raise InvalidToeLinesAttackedLines()

    if (
        attributes.attacked_lines is not None
        and attributes.attacked_lines > 0
        and not attributes.attacked_by
    ):
        raise InvalidToeLinesAttackedBy()

    if attributes.comments is not None:
        validations.check_length(payload=attributes.comments, max_length=200)
        validations.validate_sanitized_csv_input(attributes.comments)


async def update(
    current_value: ToeLine,
    attributes: ToeLinesAttributesToUpdate,
    rules_excluded: bool = False,
) -> None:
    if not rules_excluded:
        _validate_update(current_value, attributes)

    loc = attributes.loc if attributes.loc is not None else current_value.state.loc

    be_present_until = _get_be_present_until(attributes, current_value)
    clean_be_present_until = attributes.be_present is not None and be_present_until is None
    current_be_present = (
        current_value.state.be_present if attributes.be_present is None else attributes.be_present
    )
    has_vulnerabilities = (
        get_has_vulnerabilities(current_be_present, attributes.has_vulnerabilities)
        if attributes.has_vulnerabilities is not None or attributes.be_present is not None
        else current_value.state.has_vulnerabilities or False
    )
    new_state = new_state_toe_lines(
        attributes,
        be_present_until,
        current_value,
        has_vulnerabilities,
        loc,
    )
    if clean_be_present_until:
        new_state = new_state._replace(be_present_until=None)
    await toe_lines_model.update_state(
        current_value=current_value,
        new_state=new_state,
    )
