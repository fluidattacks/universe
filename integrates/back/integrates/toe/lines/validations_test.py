from datetime import datetime, timedelta

from integrates.custom_exceptions import (
    InvalidLinesOfCode,
    InvalidModifiedDate,
    InvalidSortsRiskLevelDate,
    InvalidSortsSuggestions,
)
from integrates.custom_utils import datetime as datetime_utils
from integrates.db_model.toe_lines.types import SortsSuggestion
from integrates.testing.utils import raises
from integrates.toe.lines.validations import (
    validate_loc,
    validate_modified_date,
    validate_sort_suggestions,
    validate_sorts_risk_level_date,
)


def test_validate_modified_date() -> None:
    modified_date = datetime_utils.get_now() - timedelta(days=1)
    modified_date_fail = datetime_utils.get_now() + timedelta(days=1)
    validate_modified_date(modified_date)
    with raises(InvalidModifiedDate):
        validate_modified_date(modified_date_fail)


def test_validate_loc() -> None:
    validate_loc(loc=4)
    with raises(InvalidLinesOfCode):
        validate_loc(loc=-4)


def test_validate_sorts_risk_level_date() -> None:
    modified_date = datetime.now() - timedelta(days=1)
    modified_date_fail = datetime.now() + timedelta(days=1)
    validate_sorts_risk_level_date(modified_date)
    with raises(InvalidSortsRiskLevelDate):
        validate_sorts_risk_level_date(modified_date_fail)


async def test_valid_suggestions() -> None:
    with raises(InvalidSortsSuggestions):
        validate_sort_suggestions(
            [
                SortsSuggestion(
                    "060. Insecure service configuration - Host verification",
                    150,
                ),
            ],
        )
    with raises(InvalidSortsSuggestions):
        validate_sort_suggestions(
            [
                SortsSuggestion(
                    "060. Insecure service configuration - Host verification",
                    50,
                ),
                SortsSuggestion(
                    "060. Insecure service configuration - Host verification",
                    60,
                ),
                SortsSuggestion("428. Inappropriate coding practices - invalid file", 70),
                SortsSuggestion("428. Inappropriate coding practices - invalid file", 10),
                SortsSuggestion("428. Inappropriate coding practices - invalid file", 10),
                SortsSuggestion("428. Inappropriate coding practices - invalid file", 10),
            ],
        )
