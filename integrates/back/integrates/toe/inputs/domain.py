import html
from contextlib import (
    suppress,
)
from datetime import (
    datetime,
)

from aioextensions import (
    collect,
)
from urllib3.exceptions import (
    LocationParseError,
)
from urllib3.util.url import (
    Url,
    parse_url,
)

from integrates.custom_exceptions import (
    ToeInputAlreadyUpdated,
    ToeInputNotFound,
    UnavailabilityError,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.custom_utils import (
    roots as roots_utils,
)
from integrates.custom_utils import (
    vulnerabilities as vuln_utils,
)
from integrates.custom_utils.validations_deco import (
    validate_fields_deco,
    validate_sanitized_csv_input_deco,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    toe_inputs as toe_inputs_model,
)
from integrates.db_model import (
    vulnerabilities as vulns_model,
)
from integrates.db_model.roots.enums import (
    RootEnvironmentCloud,
    RootEnvironmentUrlType,
)
from integrates.db_model.roots.types import (
    RootEnvironmentUrl,
    RootEnvironmentUrlsRequest,
)
from integrates.db_model.toe_inputs.types import (
    RootToeInputsRequest,
    ToeInput,
    ToeInputMetadataToUpdate,
    ToeInputRequest,
    ToeInputState,
    ToeInputUpdate,
)
from integrates.db_model.vulnerabilities.constants import (
    RELEASED_FILTER_STATUSES,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateReason,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.toe.inputs.types import (
    ToeInputAttributesToAdd,
    ToeInputAttributesToUpdate,
)
from integrates.toe.inputs.validations import (
    validate_input_state,
    validate_toe_input,
)
from integrates.toe.utils import (
    get_has_vulnerabilities,
)


def _get_optional_be_present_until(be_present: bool) -> datetime | None:
    return datetime_utils.get_utc_now() if be_present is False else None


@validate_sanitized_csv_input_deco(["entry_point"])
@validate_fields_deco(["component"])
async def add(
    *,
    loaders: Dataloaders,
    group_name: str,
    component: str,
    entry_point: str,
    environment_id: str,
    root_id: str,
    attributes: ToeInputAttributesToAdd,
    is_moving_toe_input: bool = False,
) -> None:
    formatted_component = component.strip()
    be_present_until = _get_optional_be_present_until(attributes.be_present)
    first_attack_at = attributes.first_attack_at or attributes.attacked_at
    has_vulnerabilities = get_has_vulnerabilities(
        attributes.be_present,
        attributes.has_vulnerabilities,
    )
    seen_at = attributes.seen_at or first_attack_at or datetime_utils.get_utc_now()
    toe_input = ToeInput(
        component=formatted_component,
        entry_point=entry_point,
        environment_id=environment_id,
        group_name=group_name,
        root_id=root_id,
        state=ToeInputState(
            attacked_at=attributes.attacked_at,
            attacked_by=attributes.attacked_by,
            be_present=attributes.be_present,
            be_present_until=be_present_until,
            first_attack_at=first_attack_at,
            has_vulnerabilities=has_vulnerabilities,
            modified_by=attributes.seen_first_time_by,
            modified_date=datetime_utils.get_utc_now(),
            seen_at=seen_at,
            seen_first_time_by=attributes.seen_first_time_by,
        ),
    )
    if is_moving_toe_input is False:
        root = await roots_utils.get_root(loaders, root_id, group_name)
        await validate_toe_input(loaders=loaders, root=root, toe_input=toe_input)

    await toe_inputs_model.add(toe_input=toe_input)


async def disallow_toe_inputs_for_environment(
    *,
    loaders: Dataloaders,
    group_name: str,
    root_id: str,
    url: str,
    modified_by: str,
    environment_url: RootEnvironmentUrl | None = None,
) -> None:
    toe_inputs = (
        await loaders.root_toe_inputs.load_nodes(
            RootToeInputsRequest(
                group_name=group_name,
                root_id=root_id,
                paginate=False,
                be_present=True,
            ),
        )
        if environment_url is None
        else await get_root_env_toe_inputs(environment_url)
    )
    formatted_environment_url = url if url.endswith("/") else f"{url}/"

    if environment_url is not None:
        vulnerabilities = await roots_utils.get_root_vulnerabilities(root_id)
        await collect(
            [
                modify_vulns_in_input_exclusion(
                    toe_input,
                    vulnerabilities,
                    modified_by,
                    loaders=loaders,
                )
                for toe_input in toe_inputs
            ],
        )

    @retry_on_exceptions(exceptions=(ToeInputAlreadyUpdated, UnavailabilityError))
    async def _disallow_toe_input(
        *,
        component: str,
        entry_point: str,
        group_name: str,
        root_id: str,
    ) -> None:
        toe_input_request = ToeInputRequest(
            component=component,
            entry_point=entry_point,
            group_name=group_name,
            root_id=root_id,
        )
        try:
            toe_input = await loaders.toe_input.load(toe_input_request)
            if not toe_input:
                raise ToeInputNotFound()
            if toe_input.state.be_present:
                await update(
                    loaders=loaders,
                    current_value=toe_input,
                    attributes=ToeInputAttributesToUpdate(be_present=False),
                    modified_by=modified_by,
                )
        except ToeInputAlreadyUpdated:
            loaders.toe_input.clear(toe_input_request)
            raise

    await collect(
        [
            _disallow_toe_input(
                component=toe_input.component,
                entry_point=toe_input.entry_point,
                group_name=toe_input.group_name,
                root_id=toe_input.root_id,
            )
            for toe_input in toe_inputs
            if (
                environment_url is None
                and (
                    toe_input.component
                    if toe_input.component.endswith("/")
                    else f"{toe_input.component}/"
                ).startswith(formatted_environment_url)
            )
            or environment_url is not None
        ],
    )


def _get_host(url: str) -> str:
    return url.split("/")[0].split(":")[0]


def _get_path(url: str) -> str:
    return url.split("/", maxsplit=1)[1] if "/" in url else ""


def _format_component(component: str) -> str:
    return (
        component.strip()
        .replace("https://", "")
        .replace("http://", "")
        .replace("unknown://", "")
        .replace("unknown//", "")
        .replace("www.", "")
    )


def get_reduced_component(component: str, entry_point: str) -> str:
    formatted_component = _format_component(component)
    host = _get_host(formatted_component)
    path = _get_path(formatted_component)
    return f"{host}/{path}/{entry_point}"


def _set_be_present_until(
    attributes: ToeInputAttributesToUpdate,
    current_value: ToeInput,
) -> datetime | None:
    return (
        current_value.state.be_present_until
        if attributes.be_present is None
        else _get_optional_be_present_until(attributes.be_present)
    )


async def update(
    loaders: Dataloaders,
    current_value: ToeInput,
    attributes: ToeInputAttributesToUpdate,
    modified_by: str,
    is_moving_toe_input: bool = False,
) -> None:
    if is_moving_toe_input is False:
        await validate_input_state(loaders, attributes, current_value)

    be_present_until = _set_be_present_until(attributes, current_value)
    current_be_present = (
        current_value.state.be_present if attributes.be_present is None else attributes.be_present
    )
    first_attack_at = current_value.state.first_attack_at
    if attributes.first_attack_at is not None:
        first_attack_at = attributes.first_attack_at
    elif current_value.state.first_attack_at is None and attributes.attacked_at:
        first_attack_at = attributes.attacked_at
    has_vulnerabilities = (
        get_has_vulnerabilities(current_be_present, attributes.has_vulnerabilities)
        if attributes.has_vulnerabilities is not None or attributes.be_present is not None
        else None
    )

    new_state = ToeInputState(
        attacked_at=attributes.attacked_at
        if attributes.attacked_at is not None
        else current_value.state.attacked_at,
        attacked_by=attributes.attacked_by
        if attributes.attacked_by is not None
        else current_value.state.attacked_by,
        be_present=current_be_present,
        be_present_until=be_present_until,
        first_attack_at=first_attack_at
        if first_attack_at is not None
        else current_value.state.first_attack_at,
        has_vulnerabilities=has_vulnerabilities
        if has_vulnerabilities is not None
        else current_value.state.has_vulnerabilities,
        modified_by=modified_by,
        modified_date=datetime_utils.get_utc_now(),
        seen_at=attributes.seen_at
        if attributes.seen_at is not None
        else current_value.state.seen_at,
        seen_first_time_by=attributes.seen_first_time_by
        if attributes.seen_first_time_by is not None
        else current_value.state.seen_first_time_by,
    )
    metadata = ToeInputMetadataToUpdate(
        clean_attacked_at=attributes.clean_attacked_at,
        clean_be_present_until=attributes.be_present is not None and be_present_until is None,
        clean_first_attack_at=attributes.clean_first_attack_at,
        clean_seen_at=attributes.clean_seen_at,
    )

    await toe_inputs_model.update_state(
        ToeInputUpdate(
            current_value=current_value,
            new_state=new_state,
            metadata=metadata,
        ),
    )


def _validate_cspm_environments(
    env_url: RootEnvironmentUrl,
    root_toe_inputs: list[ToeInput],
) -> list[ToeInput]:
    if env_url.state.cloud_name == RootEnvironmentCloud.AWS:
        client_id = env_url.url.split(":")[4]
        return [toe_input for toe_input in root_toe_inputs if client_id in toe_input.component]
    return [toe_input for toe_input in root_toe_inputs if env_url.url in toe_input.component]


async def get_root_env_toe_inputs(
    env_url: RootEnvironmentUrl,
) -> list[ToeInput]:
    loaders = get_new_context()
    _root_toe_inputs = await loaders.root_toe_inputs.load(
        RootToeInputsRequest(
            group_name=env_url.group_name,
            root_id=env_url.root_id,
            after=None,
            be_present=None,
            first=None,
            paginate=False,
        ),
    )
    root_toe_inputs = [edge.node for edge in _root_toe_inputs.edges]

    if env_url.state.url_type != RootEnvironmentUrlType.CSPM:
        return await _get_root_env_toe_inputs(env_url, root_toe_inputs, loaders)

    return _validate_cspm_environments(env_url, root_toe_inputs)


async def _get_root_env_toe_inputs(
    env_url: RootEnvironmentUrl,
    root_toe_inputs: list[ToeInput],
    loaders: Dataloaders,
) -> list[ToeInput]:
    environments = [env_url.url.rstrip("/")]
    if env_url.state.url_type != RootEnvironmentUrlType.URL:
        return await filter_toe_inputs(root_toe_inputs, environments, env_url, loaders)

    env_urls = await get_environment_urls(loaders, env_url)
    if len(env_urls) == 1:
        with suppress(LocationParseError):
            parsed_url = parse_url(env_url.url)
            url = Url(
                scheme=None,
                auth=parsed_url.auth,
                host=parsed_url.host,
                port=parsed_url.port,
                path=parsed_url.path,
                query=parsed_url.query,
                fragment=parsed_url.fragment,
            ).url
            environments = [f"{protocol}{url}".rstrip("/") for protocol in ["http://", "https://"]]

    return await filter_toe_inputs(root_toe_inputs, environments, env_url, loaders)


async def filter_toe_inputs(
    root_toe_inputs: list[ToeInput],
    environments: list[str],
    env_url: RootEnvironmentUrl,
    loaders: Dataloaders,
) -> list[ToeInput]:
    environments_urls = [
        env
        for env in await loaders.root_environment_urls.load(
            RootEnvironmentUrlsRequest(root_id=env_url.root_id, group_name=env_url.group_name),
        )
        if env.id != env_url.id and env.state.include
    ]

    return [
        toe_input
        for toe_input in root_toe_inputs
        if (
            toe_input.component.lower().strip().startswith(env_url.url.rstrip("/"))
            or any(
                toe_input.component.startswith(env)
                and toe_input.state.attacked_by == "machine@fluidattacks.com"
                for env in environments
            )
        )
        and all(
            not toe_input.component.lower().strip().startswith(env.url.rstrip("/"))
            for env in environments_urls
        )
    ]


async def get_environment_urls(
    loaders: Dataloaders,
    environment: RootEnvironmentUrl,
) -> list[RootEnvironmentUrl]:
    environments: list[RootEnvironmentUrl] = []
    for environment_url in await loaders.root_environment_urls.load(
        RootEnvironmentUrlsRequest(root_id=environment.root_id, group_name=environment.group_name),
    ):
        formatted_environment_url = (
            environment_url.url if environment_url.url.endswith("/") else f"{environment_url.url}/"
        ).lower()
        formatted_component = (
            environment.url if environment.url.endswith("/") else f"{environment.url}/"
        ).lower()
        try:
            if parse_url(formatted_environment_url)[1:] == parse_url(formatted_component)[1:]:
                environments.append(environment_url)
        except LocationParseError:
            continue

    if not environments:
        return [environment]

    return environments


async def validate_entrypoint(
    vulnerability: Vulnerability, toe_input: ToeInput, loaders: Dataloaders
) -> bool:
    _, where = vuln_utils.get_path_from_integrates_vulnerability(
        vulnerability_where=vulnerability.state.where,
        vulnerability_type=vulnerability.type,
        ignore_cve=True,
    )

    if not vuln_utils.is_machine_vuln(vulnerability):
        if where == toe_input.component:
            if vulnerability.state.specific == toe_input.entry_point:
                return True

            where = html.unescape(vulnerability.state.where)
            toe = await vuln_utils.validate_vulnerability_in_toe_inputs(
                loaders=loaders,
                vulnerability=vulnerability,
                where=where,
                index=0,
                raises=False,
                be_present=False,
            )
            return (
                toe is not None
                and toe.component == toe_input.component
                and toe.entry_point == toe_input.entry_point
            )
        return False
    return where == toe_input.component


async def modify_vulns_in_input_exclusion(
    toe_input: ToeInput,
    vulnerabilities: list[Vulnerability],
    email: str,
    loaders: Dataloaders,
) -> None:
    vulnerabilities_in_toe = await collect(
        validate_entrypoint(vulnerability, toe_input, loaders) for vulnerability in vulnerabilities
    )
    await collect(
        [
            vuln_utils.close_vulnerability(
                vulnerability,
                email,
                loaders,
                VulnerabilityStateReason.ROOT_OR_ENVIRONMENT_DEACTIVATED,
            )
            for vulnerability_in_toe, vulnerability in zip(
                vulnerabilities_in_toe, vulnerabilities, strict=False
            )
            if vulnerability_in_toe and vulnerability.state.status in RELEASED_FILTER_STATUSES
        ],
        workers=8,
    )
    await collect(
        tuple(
            vulns_model.remove(vulnerability_id=vulnerability.id)
            for vulnerability_in_toe, vulnerability in zip(
                vulnerabilities_in_toe, vulnerabilities, strict=False
            )
            if vulnerability_in_toe and vulnerability.state.status not in RELEASED_FILTER_STATUSES
        ),
        workers=8,
    )
