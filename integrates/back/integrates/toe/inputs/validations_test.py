from integrates.db_model.roots.enums import (
    RootEnvironmentCloud,
)
from integrates.testing.fakers import RootEnvironmentUrlFaker, RootEnvironmentUrlStateFaker
from integrates.testing.utils import parametrize
from integrates.toe.inputs.validations import _validate_arn, _validate_cspm_component


async def test_validate_arn() -> None:
    assert _validate_arn("arn:aws:ec2:us-east-1::snapshot/snap-021656425c8e65")
    assert _validate_arn("arn:aws:ec2:us-east-1::image/ami-0232fsa")
    assert _validate_arn("arn:aws:apigateway:us-east-1::/restapis/f6sfss3i5")
    assert not _validate_arn("arn:aws:apigateway:us-east-1:221323:/restapis/f6eb0r0yi5")


@parametrize(
    args=["component", "env_id", "expected_output"],
    cases=[
        [
            "arn:aws:s3:::skims.testing_bucket",
            "123456",
            True,
        ],
        [
            "arn:aws:route53:::hostedzone/ZOTJMSPMCOE900T",
            "123456",
            True,
        ],
        [
            "arn:aws:iam::aws:policy/PowerUserAccess",
            "123456",
            True,
        ],
        [
            "arn:aws:ec2:us-east-1:123456789012:instance/i-0abcdef1234567890",
            "123456",
            True,
        ],
        [
            "arn:aws:ec2:us-east-1:940589201830:instance/i-0abcdef1234567890",
            "123456",
            False,
        ],
    ],
)
async def test_validate_cspm_component(
    component: str,
    env_id: str,
    expected_output: bool,
) -> None:
    root_env_urls = [
        RootEnvironmentUrlFaker(
            id="123456",
            url="123456789012",
            state=RootEnvironmentUrlStateFaker(cloud_name=RootEnvironmentCloud.AWS),
        )
    ]
    result = _validate_cspm_component(component=component, env_urls=root_env_urls, env_id=env_id)
    assert result == expected_output
