from datetime import datetime

from integrates.custom_exceptions import (
    InvalidRootComponent,
    InvalidRootType,
    InvalidToeInputAttackedAt,
    RepeatedToeInput,
    ToeInputNotPresent,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.roots.enums import RootEnvironmentCloud, RootEnvironmentUrlType
from integrates.db_model.roots.types import RootEnvironmentUrlRequest
from integrates.db_model.toe_inputs.types import (
    RootToeInputsRequest,
    ToeInput,
    ToeInputRequest,
)
from integrates.db_model.vulnerabilities.enums import VulnerabilityStateStatus
from integrates.db_model.vulnerabilities.types import GroupVulnerabilitiesRequest
from integrates.roots.domain import format_environment_id
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    FindingFaker,
    GitRootFaker,
    GitRootStateFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    IPRootFaker,
    IPRootStateFaker,
    OrganizationFaker,
    RootEnvironmentUrlFaker,
    RootEnvironmentUrlStateFaker,
    StakeholderFaker,
    ToeInputFaker,
    ToeInputStateFaker,
    UrlRootFaker,
    UrlRootStateFaker,
    VulnerabilityFaker,
    VulnerabilityStateFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import freeze_time, raises
from integrates.toe.inputs.domain import (
    add,
    disallow_toe_inputs_for_environment,
    modify_vulns_in_input_exclusion,
    update,
)
from integrates.toe.inputs.types import (
    ToeInputAttributesToAdd,
    ToeInputAttributesToUpdate,
)

GROUP_NAME = "group1"
ORG_ID = "org1"
ROOT_ID = "root_id"
EMAIL_TEST = "hacker@fluidattacks.com"
URL = "https://nice-env.net"
URL_ID = format_environment_id(URL)


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_TEST, role="user"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=EMAIL_TEST,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
            ],
            roots=[
                UrlRootFaker(
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=UrlRootStateFaker(
                        host="myapp.com",
                        nickname="back",
                        port="40",
                        protocol="https",
                        path="/login",
                        query=None,
                    ),
                ),
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(
                    url=URL,
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=RootEnvironmentUrlStateFaker(modified_by="hacker@fluidattacks.com"),
                ),
            ],
        ),
    )
)
async def test_add_toe_inputs_success() -> None:
    loaders: Dataloaders = get_new_context()
    test_component = "https://myapp.com:40/login"
    await add(
        loaders=loaders,
        group_name=GROUP_NAME,
        root_id=ROOT_ID,
        component=test_component,
        entry_point="new_user",
        environment_id=format_environment_id(test_component),
        attributes=ToeInputAttributesToAdd(be_present=True),
    )

    root_toe_inputs = await loaders.root_toe_inputs.load_nodes(
        RootToeInputsRequest(group_name=GROUP_NAME, root_id=ROOT_ID),
    )

    assert len(root_toe_inputs) == 1
    assert root_toe_inputs[0].component == test_component
    assert root_toe_inputs[0].entry_point == "new_user"
    assert root_toe_inputs[0].state.be_present
    assert not root_toe_inputs[0].state.has_vulnerabilities

    with raises(RepeatedToeInput):
        await add(
            loaders=loaders,
            group_name=GROUP_NAME,
            root_id=ROOT_ID,
            component=test_component,
            entry_point="new_user",
            environment_id=format_environment_id(test_component),
            attributes=ToeInputAttributesToAdd(be_present=True),
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_TEST, role="user"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=EMAIL_TEST,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
            ],
            roots=[
                UrlRootFaker(
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=UrlRootStateFaker(
                        host=URL,
                        port="443",
                        nickname="back",
                    ),
                ),
                IPRootFaker(
                    group_name=GROUP_NAME,
                    id="root2",
                    state=IPRootStateFaker(
                        address="192.168.0.1",
                        nickname="back",
                    ),
                ),
            ],
        ),
    )
)
async def test_add_toe_inputs_failures() -> None:
    loaders: Dataloaders = get_new_context()
    with raises(InvalidRootComponent):
        await add(
            loaders=loaders,
            group_name=GROUP_NAME,
            root_id=ROOT_ID,
            component=URL,
            entry_point="8080",
            environment_id=URL_ID,
            attributes=ToeInputAttributesToAdd(be_present=True),
        )

    with raises(InvalidRootType):
        await add(
            loaders=loaders,
            group_name=GROUP_NAME,
            root_id="root2",
            component=URL,
            entry_point="login",
            environment_id=URL_ID,
            attributes=ToeInputAttributesToAdd(be_present=True),
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(
                    id=ORG_ID,
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    id="vuln1",
                    finding_id="finding1",
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=VulnerabilityStateFaker(
                        where=URL,
                        specific="user1",
                        status=VulnerabilityStateStatus.SUBMITTED,
                    ),
                ),
                VulnerabilityFaker(
                    id="vuln2",
                    finding_id="finding1",
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=VulnerabilityStateFaker(where=URL),
                    hacker_email="machine@fluidattacks.com",
                ),
            ],
            findings=[
                FindingFaker(
                    id="finding1",
                    group_name=GROUP_NAME,
                ),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id=ORG_ID),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(nickname="testroot"),
                ),
            ],
            toe_inputs=[
                ToeInputFaker(
                    environment_id="",
                    root_id=ROOT_ID,
                    component=URL,
                    group_name=GROUP_NAME,
                    entry_point="user1",
                    state=ToeInputStateFaker(),
                ),
                ToeInputFaker(
                    environment_id="",
                    root_id=ROOT_ID,
                    component=URL,
                    group_name=GROUP_NAME,
                    entry_point="user2",
                    state=ToeInputStateFaker(),
                ),
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(
                    id=URL_ID,
                    url=URL,
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=RootEnvironmentUrlStateFaker(
                        modified_by="hacker@fluidattacks.com",
                        url_type=RootEnvironmentUrlType.URL,
                    ),
                ),
            ],
        ),
    )
)
async def test_disallow_toe_inputs_for_environment_normal() -> None:
    loaders = get_new_context()
    await disallow_toe_inputs_for_environment(
        loaders=loaders,
        group_name=GROUP_NAME,
        root_id=ROOT_ID,
        url=URL,
        modified_by="integrates@fluidattacks.com",
        environment_url=await loaders.environment_url.load(
            RootEnvironmentUrlRequest(root_id=ROOT_ID, group_name=GROUP_NAME, url_id=URL_ID),
        ),
    )

    loaders = get_new_context()
    safe_vulnerabilities = [
        edge.node.id
        for edge in (
            await loaders.group_vulnerabilities.load(
                GroupVulnerabilitiesRequest(
                    group_name=GROUP_NAME,
                    state_status=VulnerabilityStateStatus.SAFE,
                ),
            )
        ).edges
    ]
    assert (
        len(
            await loaders.root_toe_inputs.load_nodes(
                RootToeInputsRequest(
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    be_present=True,
                ),
            ),
        )
    ) == 0
    assert (
        len(
            await loaders.root_toe_inputs.load_nodes(
                RootToeInputsRequest(
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    be_present=False,
                ),
            ),
        )
        == 2
    )
    assert len(safe_vulnerabilities) == 1
    assert "vuln2" in safe_vulnerabilities


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(
                    id=ORG_ID,
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    id="vuln1",
                    finding_id="finding1",
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=VulnerabilityStateFaker(
                        where=URL,
                        specific="user1",
                        status=VulnerabilityStateStatus.SUBMITTED,
                    ),
                ),
                VulnerabilityFaker(
                    id="vuln2",
                    finding_id="finding1",
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=VulnerabilityStateFaker(where=URL),
                    hacker_email="machine@fluidattacks.com",
                ),
            ],
            findings=[
                FindingFaker(
                    id="finding1",
                    group_name=GROUP_NAME,
                ),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id=ORG_ID),
            ],
            roots=[
                UrlRootFaker(
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=UrlRootStateFaker(host="nice-env.net"),
                ),
            ],
            toe_inputs=[
                ToeInputFaker(
                    environment_id="",
                    root_id=ROOT_ID,
                    component="nice-env.net",
                    group_name=GROUP_NAME,
                    entry_point="user1",
                    state=ToeInputStateFaker(),
                ),
            ],
        ),
    )
)
async def test_disallow_toe_inputs_for_root() -> None:
    loaders = get_new_context()
    await disallow_toe_inputs_for_environment(
        loaders=loaders,
        group_name=GROUP_NAME,
        root_id=ROOT_ID,
        url="nice-env.net",
        modified_by="integrates@fluidattacks.com",
    )

    loaders = get_new_context()
    assert (
        len(
            await loaders.root_toe_inputs.load_nodes(
                RootToeInputsRequest(
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    be_present=False,
                ),
            ),
        )
        == 1
    )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(
                    id=ORG_ID,
                ),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id=ORG_ID),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(nickname="testroot"),
                ),
            ],
            toe_inputs=[
                ToeInputFaker(
                    environment_id="",
                    root_id=ROOT_ID,
                    component=URL,
                    group_name=GROUP_NAME,
                    entry_point="user",
                    state=ToeInputStateFaker(),
                ),
                ToeInputFaker(
                    environment_id="",
                    root_id=ROOT_ID,
                    group_name=GROUP_NAME,
                    component="http://nice-env.net",
                    entry_point="user",
                    state=ToeInputStateFaker(),
                ),
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(
                    id=format_environment_id("http://nice-env.net"),
                    url="http://nice-env.net",
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=RootEnvironmentUrlStateFaker(
                        modified_by="hacker@fluidattacks.com",
                        url_type=RootEnvironmentUrlType.URL,
                    ),
                ),
                RootEnvironmentUrlFaker(
                    id=URL_ID,
                    url=URL,
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=RootEnvironmentUrlStateFaker(
                        modified_by="hacker@fluidattacks.com",
                        url_type=RootEnvironmentUrlType.URL,
                    ),
                ),
            ],
        ),
    )
)
async def test_disallow_toe_inputs_for_environment_two_env_url() -> None:
    loaders = get_new_context()
    await disallow_toe_inputs_for_environment(
        loaders=loaders,
        group_name=GROUP_NAME,
        root_id=ROOT_ID,
        url=URL,
        modified_by="integrates@fluidattacks.com",
        environment_url=await loaders.environment_url.load(
            RootEnvironmentUrlRequest(root_id=ROOT_ID, group_name=GROUP_NAME, url_id=URL_ID),
        ),
    )

    loaders = get_new_context()
    assert (
        len(
            await loaders.root_toe_inputs.load_nodes(
                RootToeInputsRequest(
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    be_present=True,
                ),
            ),
        )
    ) == 1
    assert (
        len(
            await loaders.root_toe_inputs.load_nodes(
                RootToeInputsRequest(
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    be_present=False,
                ),
            ),
        )
    ) == 1


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(
                    id=ORG_ID,
                ),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id=ORG_ID),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(nickname="testroot"),
                ),
            ],
            toe_inputs=[
                ToeInputFaker(
                    environment_id="",
                    root_id=ROOT_ID,
                    component=URL,
                    group_name=GROUP_NAME,
                    entry_point="user",
                    state=ToeInputStateFaker(),
                ),
                ToeInputFaker(
                    environment_id="",
                    root_id=ROOT_ID,
                    group_name=GROUP_NAME,
                    component="http://nice-env.net",
                    entry_point="user",
                    state=ToeInputStateFaker(attacked_by="machine@fluidattacks.com"),
                ),
                ToeInputFaker(
                    environment_id="",
                    root_id=ROOT_ID,
                    group_name=GROUP_NAME,
                    component="http://nice-env.net",
                    entry_point="user3",
                    state=ToeInputStateFaker(),
                ),
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(
                    id=URL_ID,
                    url=URL,
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=RootEnvironmentUrlStateFaker(
                        modified_by="hacker@fluidattacks.com",
                        url_type=RootEnvironmentUrlType.URL,
                    ),
                ),
            ],
        ),
    )
)
async def test_disallow_toe_inputs_for_environment_url_machine() -> None:
    loaders = get_new_context()
    await disallow_toe_inputs_for_environment(
        loaders=loaders,
        group_name=GROUP_NAME,
        root_id=ROOT_ID,
        url=URL,
        modified_by="integrates@fluidattacks.com",
        environment_url=await loaders.environment_url.load(
            RootEnvironmentUrlRequest(root_id=ROOT_ID, group_name=GROUP_NAME, url_id=URL_ID),
        ),
    )

    loaders = get_new_context()
    assert (
        len(
            await loaders.root_toe_inputs.load_nodes(
                RootToeInputsRequest(
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    be_present=True,
                ),
            ),
        )
    ) == 1
    assert (
        len(
            await loaders.root_toe_inputs.load_nodes(
                RootToeInputsRequest(
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    be_present=False,
                ),
            ),
        )
    ) == 2


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(
                    id=ORG_ID,
                ),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id=ORG_ID),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(nickname="testroot"),
                ),
            ],
            toe_inputs=[
                ToeInputFaker(
                    environment_id="",
                    root_id=ROOT_ID,
                    component=URL,
                    group_name=GROUP_NAME,
                    entry_point="user",
                    state=ToeInputStateFaker(),
                ),
                ToeInputFaker(
                    environment_id="",
                    root_id=ROOT_ID,
                    group_name=GROUP_NAME,
                    component="http://nice-env.net",
                    entry_point="user",
                    state=ToeInputStateFaker(),
                ),
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(
                    id=URL_ID,
                    url=URL,
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=RootEnvironmentUrlStateFaker(
                        modified_by="hacker@fluidattacks.com",
                        url_type=RootEnvironmentUrlType.URL,
                    ),
                ),
            ],
        ),
    )
)
async def test_disallow_toe_inputs_for_environment_url_non_machine() -> None:
    loaders = get_new_context()
    await disallow_toe_inputs_for_environment(
        loaders=loaders,
        group_name=GROUP_NAME,
        root_id=ROOT_ID,
        url=URL,
        modified_by="integrates@fluidattacks.com",
        environment_url=await loaders.environment_url.load(
            RootEnvironmentUrlRequest(root_id=ROOT_ID, group_name=GROUP_NAME, url_id=URL_ID),
        ),
    )

    loaders = get_new_context()
    assert (
        len(
            await loaders.root_toe_inputs.load_nodes(
                RootToeInputsRequest(
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    be_present=True,
                ),
            ),
        )
    ) == 1
    assert (
        len(
            await loaders.root_toe_inputs.load_nodes(
                RootToeInputsRequest(
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    be_present=False,
                ),
            ),
        )
    ) == 1


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(
                    id=ORG_ID,
                ),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id=ORG_ID),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(nickname="testroot"),
                ),
            ],
            toe_inputs=[
                ToeInputFaker(
                    environment_id=format_environment_id(
                        "arn:aws:iam::123456789012:role/my-role_name"
                    ),
                    root_id=ROOT_ID,
                    component="arn:aws:iam::123456789012:role/my-role_name",
                    entry_point="user",
                    group_name=GROUP_NAME,
                    state=ToeInputStateFaker(),
                ),
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(
                    id=format_environment_id("arn:aws:iam::123456789012:role/my-role_name"),
                    url="arn:aws:iam::123456789012:role/my-role_name",
                    root_id=ROOT_ID,
                    group_name=GROUP_NAME,
                    state=RootEnvironmentUrlStateFaker(
                        modified_by="admin@fluidattacks.com",
                        url_type=RootEnvironmentUrlType.CSPM,
                        cloud_name=RootEnvironmentCloud.AWS,
                    ),
                ),
            ],
        ),
    )
)
async def test_disallow_toe_inputs_for_environment_non_url() -> None:
    loaders = get_new_context()
    await disallow_toe_inputs_for_environment(
        loaders=loaders,
        group_name=GROUP_NAME,
        root_id=ROOT_ID,
        url="arn:aws:iam::123456789012:role/my-role_name",
        modified_by="integrates@fluidattacks.com",
        environment_url=await loaders.environment_url.load(
            RootEnvironmentUrlRequest(
                root_id=ROOT_ID,
                group_name=GROUP_NAME,
                url_id=format_environment_id("arn:aws:iam::123456789012:role/my-role_name"),
            ),
        ),
    )

    loaders = get_new_context()
    assert (
        len(
            await loaders.root_toe_inputs.load_nodes(
                RootToeInputsRequest(
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    be_present=True,
                ),
            ),
        )
    ) == 0
    assert (
        len(
            await loaders.root_toe_inputs.load_nodes(
                RootToeInputsRequest(
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    be_present=False,
                ),
            ),
        )
        == 1
    )


COMPONENT = "https://clientapp.com/"
ENTRYPOINT = "login"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(
                        nickname="back",
                    ),
                ),
            ],
            toe_inputs=[
                ToeInputFaker(
                    component=COMPONENT,
                    entry_point=ENTRYPOINT,
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=ToeInputStateFaker(
                        be_present=False,
                        attacked_by="other_hacker@fluidattacks.com",
                    ),
                ),
            ],
        ),
    ),
)
@freeze_time("2024-10-14T00:00:00")
async def test_update_toe_input() -> None:
    loaders: Dataloaders = get_new_context()
    current_value: ToeInput | None = await loaders.toe_input.load(
        ToeInputRequest(
            component=COMPONENT,
            entry_point=ENTRYPOINT,
            group_name=GROUP_NAME,
            root_id=ROOT_ID,
        ),
    )
    assert current_value
    assert current_value.state.be_present is False
    assert current_value.state.attacked_by == "other_hacker@fluidattacks.com"

    await update(
        loaders=loaders,
        current_value=current_value,
        attributes=ToeInputAttributesToUpdate(
            attacked_at=datetime_utils.get_utc_now(),
            attacked_by="new_hacker@fluidattacks.com",
            be_present=True,
        ),
        modified_by="new_hacker@fluidattacks.com",
    )

    toe_input_updated = await get_new_context().toe_input.load(
        ToeInputRequest(
            component=COMPONENT,
            entry_point=ENTRYPOINT,
            group_name=GROUP_NAME,
            root_id=ROOT_ID,
        ),
    )
    assert toe_input_updated
    assert toe_input_updated.state.be_present is True
    toe_input_attacked_at = toe_input_updated.state.attacked_at
    assert toe_input_attacked_at
    assert (toe_input_attacked_at.year, toe_input_attacked_at.month, toe_input_attacked_at.day) == (
        2024,
        10,
        14,
    )
    assert toe_input_updated.state.attacked_by == "new_hacker@fluidattacks.com"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(
                        nickname="back",
                    ),
                ),
            ],
            toe_inputs=[
                ToeInputFaker(
                    component=COMPONENT,
                    entry_point=ENTRYPOINT,
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=ToeInputStateFaker(
                        be_present=False,
                        attacked_by="other_hacker@fluidattacks.com",
                    ),
                ),
            ],
        ),
    ),
)
@freeze_time("2024-10-14T00:00:00")
async def test_update_toe_input_not_present() -> None:
    loaders: Dataloaders = get_new_context()
    current_value: ToeInput | None = await loaders.toe_input.load(
        ToeInputRequest(
            component=COMPONENT,
            entry_point=ENTRYPOINT,
            group_name=GROUP_NAME,
            root_id=ROOT_ID,
        ),
    )
    assert current_value
    with raises(ToeInputNotPresent):
        await update(
            loaders=loaders,
            current_value=current_value,
            attributes=ToeInputAttributesToUpdate(
                attacked_at=datetime_utils.get_utc_now(),
                attacked_by="new_hacker@fluidattacks.com",
                be_present=False,
            ),
            modified_by="new_hacker@fluidattacks.com",
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(
                        nickname="back",
                    ),
                ),
            ],
            toe_inputs=[
                ToeInputFaker(
                    component=COMPONENT,
                    entry_point=ENTRYPOINT,
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=ToeInputStateFaker(
                        be_present=False,
                        attacked_by="other_hacker@fluidattacks.com",
                        seen_at=datetime.fromisoformat("2024-12-20T14:30:00+00:00"),
                    ),
                ),
            ],
        ),
    ),
)
@freeze_time("2024-12-30T00:00:00")
async def test_update_toe_input_wrong_attacked_at() -> None:
    loaders: Dataloaders = get_new_context()
    current_value: ToeInput | None = await loaders.toe_input.load(
        ToeInputRequest(
            component=COMPONENT,
            entry_point=ENTRYPOINT,
            group_name=GROUP_NAME,
            root_id=ROOT_ID,
        ),
    )
    assert current_value
    with raises(InvalidToeInputAttackedAt):
        await update(
            loaders=loaders,
            current_value=current_value,
            attributes=ToeInputAttributesToUpdate(
                attacked_at=datetime.fromisoformat("2024-12-10T14:30:00+00:00"),
                attacked_by="new_hacker@fluidattacks.com",
                be_present=True,
            ),
            modified_by="new_hacker@fluidattacks.com",
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME, id=ROOT_ID, state=GitRootStateFaker(nickname="back")
                ),
            ],
            findings=[FindingFaker(id="finding1", group_name=GROUP_NAME)],
            vulnerabilities=[
                VulnerabilityFaker(
                    id="vuln1",
                    finding_id="finding1",
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=VulnerabilityStateFaker(where=COMPONENT, specific=f"{ENTRYPOINT} (test)"),
                    hacker_email="other@fluidattacks.com",
                ),
            ],
            toe_inputs=[
                ToeInputFaker(
                    component=COMPONENT,
                    entry_point=ENTRYPOINT,
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=ToeInputStateFaker(
                        be_present=False,
                        attacked_by="other_hacker@fluidattacks.com",
                        seen_at=datetime.fromisoformat("2024-12-20T14:30:00+00:00"),
                    ),
                ),
            ],
        ),
    ),
)
async def test_modify_vulns_in_input_exclusion() -> None:
    loaders = get_new_context()
    toe_input = await loaders.toe_input.load(
        ToeInputRequest(
            component=COMPONENT,
            entry_point=ENTRYPOINT,
            group_name=GROUP_NAME,
            root_id=ROOT_ID,
        ),
    )
    assert toe_input
    await modify_vulns_in_input_exclusion(
        toe_input, await loaders.root_vulnerabilities.load(ROOT_ID), "", loaders
    )
    assert (
        len(
            [
                vuln
                for vuln in await get_new_context().root_vulnerabilities.load(ROOT_ID)
                if vuln.state.status == VulnerabilityStateStatus.SAFE
            ]
        )
        == 1
    )
