from collections.abc import Mapping
from typing import (
    Any,
    TypedDict,
    TypeVar,
)


class BaseTypedDict(TypedDict):
    pass


Item = dict[str, Any]

TItem = TypeVar("TItem", bound=BaseTypedDict)

GenericItem = TypeVar("GenericItem", bound=Mapping[str, Any])
