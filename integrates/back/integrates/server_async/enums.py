from enum import Enum


class TaskResult(Enum):
    FAILED: bool = False
    SUCCEEDED: bool = True


class TaskStatus(str, Enum):
    FAILED: str = "FAILED"
    FINISHED: str = "FINISHED"
    QUEUED: str = "QUEUED"
    STARTED: str = "STARTED"
