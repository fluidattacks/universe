import json
import logging
import logging.config

from botocore.exceptions import (
    ClientError,
)

from integrates.server_async.enums import (
    TaskStatus,
)
from integrates.settings.logger import (
    LOGGING,
)
from integrates.sqs.resource import (
    get_client,
)

logging.config.dictConfig(LOGGING)

SQS_TASKS_LOGGER = logging.getLogger("sqs_tasks")


async def queue_refresh_toe_lines_async(
    group_name: str,
    git_root_id: str,
    user_email: str,
) -> None:
    try:
        sqs_client = await get_client()
        await sqs_client.send_message(
            QueueUrl=("https://sqs.us-east-1.amazonaws.com/205810638802/integrates_refresh"),
            MessageBody=json.dumps(
                {
                    "id": f"{group_name}_{git_root_id}",
                    "task": "refresh_toe_lines",
                    "args": [
                        group_name,
                        git_root_id,
                        user_email,
                    ],
                },
            ),
        )
        SQS_TASKS_LOGGER.info(
            "Enqueue the task",
            extra={
                "extra": {
                    "group_name": group_name,
                    "git_root_id": git_root_id,
                    "status": TaskStatus.QUEUED.value,
                    "task_name": "refresh_toe_lines",
                    "user_email": user_email,
                },
            },
        )
    except ClientError as exc:
        SQS_TASKS_LOGGER.error(
            "Attempted to enqueue the task",
            exc_info=exc,
            extra={
                "extra": {
                    "group_name": group_name,
                    "git_root_id": git_root_id,
                    "status": TaskStatus.FAILED.value,
                    "task_name": "refresh_toe_lines",
                    "user_email": user_email,
                },
            },
        )
