import functools
import inspect
import json
import logging
import logging.config
import traceback
import uuid
from collections.abc import Awaitable, Callable
from typing import ParamSpec

from async_sqs_consumer.queue import Queue
from async_sqs_consumer.worker import Worker

from integrates.audit import AuditContext, add_audit_context
from integrates.batch_dispatch.clone_roots import clone_git_root_simple_args
from integrates.batch_dispatch.refresh_toe_lines import (
    refresh_toe_lines_simple_args,
)
from integrates.class_types.types import Item
from integrates.custom_utils.bugsnag import start_scheduler_session
from integrates.dataloaders import get_new_context
from integrates.db_model.marketplace.types import (
    AWSMarketplaceSubscriptionSNSUpdate,
)
from integrates.jobs_orchestration.model.types import SbomFormat, SbomJobType
from integrates.marketplace.core import process_marketplace_sns_update
from integrates.server_async.enums import TaskResult, TaskStatus
from integrates.server_async.gen_snippets import set_snippet_for_vulnerability
from integrates.server_async.report_machine import process_execution
from integrates.server_async.report_machine.soon import (
    process_vulnerabilities,
    process_vulnerability_llm,
)
from integrates.server_async.report_sbom import process_sbom_execution
from integrates.settings import LOGGING

logging.config.dictConfig(LOGGING)
SQS_TASKS_LOGGER = logging.getLogger("sqs_tasks")

P = ParamSpec("P")


def log_task_to_cloudwatch(
    task: Callable[P, Awaitable[tuple[bool, str | None]]],
) -> Callable[P, Awaitable[None]]:
    @functools.wraps(task)
    async def wrapper(*args: P.args, **kwargs: P.kwargs) -> None:
        task_id = str(uuid.uuid4())
        task_name = task.__name__

        argspec = inspect.getfullargspec(task)
        func_args: Item = {**dict(zip(argspec.args, args[: len(argspec.args)], strict=False))}
        if argspec.varargs is not None:
            func_args.update(
                {
                    argspec.varargs: (
                        args[len(argspec.args) :] if len(args) > len(argspec.args) else tuple()
                    ),
                },
            )
        if argspec.varkw is not None:
            func_args.update({argspec.varkw: kwargs})

        SQS_TASKS_LOGGER.info(
            "Starting task",
            extra={
                "extra": {
                    "status": TaskStatus.STARTED.value,
                    "task_name": task_name,
                    "task_id": task_id,
                    **func_args,
                },
            },
        )
        add_audit_context(AuditContext(mechanism="TASK"))
        success, details = await task(*args, **kwargs)
        SQS_TASKS_LOGGER.info(
            "Finishing task",
            extra={
                "extra": {
                    "details": details,
                    "result": TaskResult(success).name,
                    "status": TaskStatus.FINISHED.value,
                    "task_name": task_name,
                    "task_id": task_id,
                    **func_args,
                },
            },
        )

    return wrapper


worker = Worker(
    max_workers=8,
    queues={
        "report": Queue(
            url=("https://sqs.us-east-1.amazonaws.com/205810638802/integrates_report"),
            max_queue_parallel_messages=20,
            visibility_timeout=1000,
            priority=1,
        ),
        "report_soon": Queue(
            url=("https://sqs.us-east-1.amazonaws.com/205810638802/integrates_report_soon"),
            max_queue_parallel_messages=20,
            visibility_timeout=1000,
            priority=1,
        ),
        "clone": Queue(
            url=("https://sqs.us-east-1.amazonaws.com/205810638802/integrates_clone"),
            max_queue_parallel_messages=10,
            visibility_timeout=3600,
            priority=2,
        ),
        "refresh": Queue(
            url=("https://sqs.us-east-1.amazonaws.com/205810638802/integrates_refresh"),
            max_queue_parallel_messages=10,
            visibility_timeout=1200,
            priority=4,
        ),
        "sbom": Queue(
            url=("https://sqs.us-east-1.amazonaws.com/205810638802/integrates_sbom"),
            max_queue_parallel_messages=20,
            visibility_timeout=1200,
            priority=3,
        ),
        "snippet": Queue(
            url=("https://sqs.us-east-1.amazonaws.com/205810638802/integrates_snippet"),
            max_queue_parallel_messages=10,
            visibility_timeout=1200,
            priority=10,
        ),
        "report_llm": Queue(
            url=("https://sqs.us-east-1.amazonaws.com/205810638802/integrates_llm_report"),
            max_queue_parallel_messages=10,
            visibility_timeout=1200,
            priority=10,
        ),
        "mail_sbom": Queue(
            url=("https://sqs.us-east-1.amazonaws.com/205810638802/integrates_mail_sbom"),
            max_queue_parallel_messages=20,
            visibility_timeout=1200,
            priority=3,
        ),
        "MarketplaceUpdates": Queue(
            url=("https://sqs.us-east-1.amazonaws.com/993047037389/MarketplaceUpdates"),
            priority=1,
        ),
    },
)


@worker.on_event("startup")  # type: ignore
async def start_bugsnag() -> None:
    start_scheduler_session()


@worker.task("clone", queue_name="clone")
@log_task_to_cloudwatch
async def clone(
    group_name: str,
    git_root_id: str,
    should_queue_machine: bool = True,
    user_email: str = "integrates@fluidattacks.com",
    should_queue_sbom: bool = True,
) -> tuple[bool, str | None]:
    success, details = await clone_git_root_simple_args(
        group_name=group_name,
        git_root_id=git_root_id,
        user_email=user_email,
        should_queue_machine=should_queue_machine,
        should_queue_sbom=should_queue_sbom,
    )

    return success, details


@worker.task("refresh_toe_lines", queue_name="refresh")
@log_task_to_cloudwatch
async def refresh_toe_lines(
    group_name: str,
    git_root_id: str,
    user_email: str = "integrates@fluidattacks.com",
) -> tuple[bool, str]:
    success, details = await refresh_toe_lines_simple_args(
        group_name=group_name,
        git_root_id=git_root_id,
        user_email=user_email,
    )

    return success, details


@worker.task("report", queue_name="report")
@log_task_to_cloudwatch
async def report(
    execution_id: str,
    commit: str | None = None,
    commit_date: str | None = None,
    user_email: str = "integrates@fluidattacks.com",
) -> tuple[bool, str]:
    success, details = await process_execution(
        execution_id=execution_id,
        commit=commit,
        commit_date=commit_date,
        user_email=user_email,
    )

    return success, details


@worker.task("sbom", queue_name="sbom")
@log_task_to_cloudwatch
async def sbom(execution_id: str, image_ref: str | None = None) -> tuple[bool, str]:
    return await process_sbom_execution(
        execution_id=execution_id,
        image_ref=image_ref,
        job_type=SbomJobType.SCHEDULER,
        sbom_format=SbomFormat.FLUID_JSON,
    )


@worker.task("report_sbom_and_machine", queue_name="report")
@log_task_to_cloudwatch
async def report_sbom_and_machine(
    execution_id: str,
    sbom_id: str,
    commit: str | None = None,
    commit_date: str | None = None,
    user_email: str = "integrates@fluidattacks.com",
) -> tuple[bool, str]:
    sbom_success, sbom_details = False, "SBOM failed execution."
    machine_success, machine_details = False, "Machine failed execution."

    try:
        sbom_success, sbom_details = await process_sbom_execution(
            execution_id=sbom_id,
            job_type=SbomJobType.SCHEDULER,
            sbom_format=SbomFormat.FLUID_JSON,
        )
    except Exception as e:  # pylint:disable=broad-exception-caught
        SQS_TASKS_LOGGER.info(
            "SBOM result processing failed.",
            extra={
                "extra": {
                    "status": TaskStatus.FAILED.value,
                    "task_name": "report_sbom_and_machine",
                    "execution_id": execution_id,
                    "sbom_id": sbom_id,
                    "error": str(e),
                    "stacktrace": traceback.format_exc(),
                },
            },
        )
    try:
        machine_success, machine_details = await process_execution(
            execution_id=execution_id,
            commit=commit,
            commit_date=commit_date,
            user_email=user_email,
        )
    except Exception as e:  # pylint:disable=broad-exception-caught
        SQS_TASKS_LOGGER.info(
            "Machine result processing failed.",
            extra={
                "extra": {
                    "status": TaskStatus.FAILED.value,
                    "task_name": "report_sbom_and_machine",
                    "execution_id": execution_id,
                    "sbom_id": sbom_id,
                    "error": str(e),
                    "stacktrace": traceback.format_exc(),
                },
            },
        )
        raise (e)

    success = sbom_success and machine_success
    details = f"SBOM: {sbom_details} Machine: {machine_details}"

    return success, details


@worker.task("mail_sbom", queue_name="mail_sbom")
@log_task_to_cloudwatch
async def mail_sbom(
    execution_id: str,
    requester_email: str,
    sbom_format: str,
    notification_id: str | None = None,
    image_ref: str | None = None,
) -> tuple[bool, str]:
    return await process_sbom_execution(
        execution_id=execution_id,
        image_ref=image_ref,
        job_type=SbomJobType.REQUEST,
        email_to=requester_email,
        notification_id=notification_id,
        sbom_format=SbomFormat(sbom_format),
    )


@worker.task("report_soon", queue_name="report_soon")
@log_task_to_cloudwatch
async def report_soon(
    group_name: str,
    git_root_nickname: str,
    finding_code: str,
    vulnerability_file: Item,
) -> tuple[bool, str]:
    return await process_vulnerabilities(
        group_name,
        git_root_nickname,
        finding_code,
        vulnerability_file,
    )


@worker.task("sns_notification", queue_name="MarketplaceUpdates")
@log_task_to_cloudwatch
async def marketplace_sns_notifications(**kwargs: Item) -> tuple[bool, str]:
    loaders = get_new_context()
    message: AWSMarketplaceSubscriptionSNSUpdate = json.loads(str(kwargs["Message"]))
    return await process_marketplace_sns_update(loaders=loaders, message=message)


@worker.task("get_snippet", queue_name="snippet")
@log_task_to_cloudwatch
async def get_snippet(
    vulnerability_id: str,
    commit: str | None = None,
) -> tuple[bool, str]:
    success, details = await set_snippet_for_vulnerability(
        vulnerability_id=vulnerability_id, root_commit=commit
    )

    return success, details


@worker.task("report_llm", queue_name="report_llm")
@log_task_to_cloudwatch
async def report_llm(
    root_id: str,
    where: str,
    snippet_hash: str,
    candidate_id: str,
) -> tuple[bool, str]:
    return await process_vulnerability_llm(
        root_id=root_id,
        where=where,
        snippet_hash=snippet_hash,
        candidate_id=candidate_id,
    )


if __name__ == "__main__":
    worker.start()
