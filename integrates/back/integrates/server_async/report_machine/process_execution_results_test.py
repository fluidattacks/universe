import json
import os
from decimal import Decimal

from fluidattacks_core.serializers.snippet import Snippet

from integrates.custom_utils import roots as roots_utils
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.enums import Source
from integrates.db_model.findings.types import (
    Finding,
    FindingEvidence,
    FindingEvidences,
)
from integrates.db_model.roots.enums import (
    RootEnvironmentCloud,
    RootEnvironmentUrlType,
)
from integrates.db_model.toe_inputs.types import RootToeInputsRequest
from integrates.db_model.types import SeverityScore
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
    VulnerabilityTechnique,
    VulnerabilityType,
    VulnerabilityVerificationStatus,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityAdvisory,
)
from integrates.finding_comments import domain as comments_domain
from integrates.server_async import report_machine
from integrates.server_async.report_machine import (
    process_execution,
)
from integrates.server_async.report_machine.process_execution_results import (
    manage_events,
)
from integrates.server_async.utils import (
    get_results_log,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    DATE_2019,
    FindingFaker,
    FindingStateFaker,
    GitRootFaker,
    GitRootStateFaker,
    GroupFaker,
    GroupStateFaker,
    OrganizationFaker,
    RootEnvironmentUrlFaker,
    RootEnvironmentUrlStateFaker,
    ToeLinesFaker,
    ToeLinesStateFaker,
    VulnerabilityFaker,
    VulnerabilityStateFaker,
    VulnerabilityVerificationFaker,
)
from integrates.testing.mocks import Mock, mocks

ROOT_ID = "root1"
MACHINE_EMAIL = "machine@fluidattacks.com"


def load_sarif_file(sarif_name: str) -> dict:
    with open(os.path.join(os.path.dirname(__file__), "test-data", sarif_name), "rb") as f:
        return json.load(f)


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(name="group1", organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id=ROOT_ID,
                    state=GitRootStateFaker(
                        nickname="back",
                    ),
                ),
            ],
            toe_lines=[
                ToeLinesFaker(
                    filename="MyJar.jar",
                    group_name="group1",
                    root_id=ROOT_ID,
                    state=ToeLinesStateFaker(
                        loc=180,
                    ),
                ),
                ToeLinesFaker(
                    filename="MyJar.class",
                    group_name="group1",
                    root_id=ROOT_ID,
                    state=ToeLinesStateFaker(
                        loc=180,
                    ),
                ),
                ToeLinesFaker(
                    filename="java_has_print_statements.java",
                    group_name="group1",
                    root_id=ROOT_ID,
                    state=ToeLinesStateFaker(
                        loc=180,
                    ),
                ),
                ToeLinesFaker(
                    filename="package.json",
                    group_name="group1",
                    root_id=ROOT_ID,
                    state=ToeLinesStateFaker(
                        loc=180,
                    ),
                ),
            ],
        ),
    ),
    others=[
        Mock(
            report_machine.process_execution_results,
            "get_config",
            "async",
            {
                "namespace": "back",
                "language": "EN",
                "sast": {"include": ["."], "exclude": []},
                "sca": {"include": ["."], "exclude": []},
            },
        ),
        Mock(
            report_machine.process_execution_results,
            "get_results_log",
            "async",
            load_sarif_file("execution_results.sarif"),
        ),
    ],
)
async def test_process_execution() -> None:
    """Testing normal processing of a simple execution"""
    loaders: Dataloaders = get_new_context()
    results = await get_results_log(
        "anything",
        "integrates/back/integrates/server_async/report_machine/test-data/execution_results.sarif",
    )

    if results:
        root = await roots_utils.get_root(loaders, ROOT_ID, "group1")
        await manage_events(results, loaders, "group1", root)

    findings = await loaders.group_findings.load("group1")
    assert not findings

    await process_execution(execution_id="group1_")
    loaders.group_findings.clear("group1")

    findings = await loaders.group_findings.load("group1")
    f117 = next((fin for fin in findings if fin.title.startswith("117")), None)
    f011 = next((fin for fin in findings if fin.title.startswith("011")), None)
    f237 = next((fin for fin in findings if fin.title.startswith("237")), None)
    assert f117 is not None
    assert f011 is not None
    assert f237 is not None

    f117_vulns = await loaders.finding_vulnerabilities.load(f117.id)
    f011_vulns = await loaders.finding_vulnerabilities.load(f011.id)
    f237_vulns = await loaders.finding_vulnerabilities.load(f237.id)
    assert len(f117_vulns) == 2
    assert len(f011_vulns) == 1
    assert len(f237_vulns) == 3

    assert all(
        isinstance(x, Snippet)
        for x in await loaders.vulnerability_snippet.load_many(
            (vuln.id, vuln.state.modified_date) for vuln in f237_vulns
        )
    )

    comments = await comments_domain.get_comments(
        loaders=loaders,
        group_name="group1",
        finding_id=f117.id,
        user_email="machine@fludidattacks.com",
    )
    assert len(comments) == 0


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(name="group1", organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id=ROOT_ID,
                    state=GitRootStateFaker(
                        nickname="back",
                    ),
                ),
            ],
            toe_lines=[
                ToeLinesFaker(
                    filename="java_has_print_statements.java",
                    group_name="group1",
                    root_id=ROOT_ID,
                    state=ToeLinesStateFaker(
                        loc=180,
                    ),
                ),
                ToeLinesFaker(
                    filename="package.json",
                    group_name="group1",
                    root_id=ROOT_ID,
                    state=ToeLinesStateFaker(
                        loc=180,
                    ),
                ),
            ],
            findings=[
                FindingFaker(
                    group_name="group1",
                    id="fin_id_1",
                    state=FindingStateFaker(modified_by=MACHINE_EMAIL),
                    title="237. Logging of sensitive data",
                ),
                FindingFaker(
                    group_name="group1",
                    id="fin_id_2",
                    state=FindingStateFaker(modified_by=MACHINE_EMAIL),
                    title="011. Vulnerable dependencies",
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    finding_id="fin_id_1",
                    group_name="group1",
                    id="vuln_id_1",
                    organization_name="test_org",
                    root_id=ROOT_ID,
                    created_by=MACHINE_EMAIL,
                    hacker_email=MACHINE_EMAIL,
                    type=VulnerabilityType.LINES,
                    state=VulnerabilityStateFaker(
                        where="java_has_print_statements.java",
                        specific="7",
                        source=Source.MACHINE,
                    ),
                    vuln_machine_hash=9326911172693336000,
                    vuln_skims_description=(
                        "print sensitive data in java_has_print_statements.java line 7"
                    ),
                    vuln_skims_method="java.java_has_print_statements",
                ),
                VulnerabilityFaker(
                    finding_id="fin_id_1",
                    group_name="group1",
                    id="vuln_id_2",
                    organization_name="test_org",
                    root_id=ROOT_ID,
                    created_by=MACHINE_EMAIL,
                    hacker_email=MACHINE_EMAIL,
                    type=VulnerabilityType.LINES,
                    state=VulnerabilityStateFaker(
                        where="java_has_print_statements.java",
                        specific="16",
                        source=Source.MACHINE,
                    ),
                    vuln_machine_hash=10243425851358753000,
                    vuln_skims_description=(
                        "print sensitive data in java_has_print_statements.java line 16"
                    ),
                    vuln_skims_method="java.java_has_print_statements",
                ),
                VulnerabilityFaker(
                    finding_id="fin_id_2",
                    group_name="group1",
                    id="vuln_id_3",
                    organization_name="test_org",
                    root_id=ROOT_ID,
                    created_by=MACHINE_EMAIL,
                    hacker_email=MACHINE_EMAIL,
                    type=VulnerabilityType.LINES,
                    state=VulnerabilityStateFaker(
                        where="package.json",
                        specific="10",
                        source=Source.MACHINE,
                        advisories=VulnerabilityAdvisory(
                            cve=[
                                "CVE-2020-28168",
                                "CVE-2021-3749",
                                "GHSA-4w2v-q235-vp99",
                                "GHSA-cph5-m8f7-6c5x",
                            ],
                            package="axios",
                            vulnerable_version="^0.19.2",
                        ),
                    ),
                    vuln_machine_hash=11509000728721770000,
                    vuln_skims_description=("Vulnerable dependency package.json line 10"),
                    vuln_skims_method="java.java_has_print_statements",
                ),
            ],
        ),
    ),
    others=[
        Mock(
            report_machine.process_execution_results,
            "get_config",
            "async",
            {
                "namespace": "back",
                "language": "EN",
                "sast": {"include": ["."], "exclude": []},
                "sca": {"include": ["."], "exclude": [], "use_new_sca": True},
            },
        ),
        Mock(
            report_machine.process_execution_results,
            "get_results_log",
            "async",
            load_sarif_file("persist_results.sarif"),
        ),
    ],
)
async def test_persist_execution_results() -> None:
    loaders: Dataloaders = get_new_context()
    root_vulnerabilities_initial = [
        vuln
        for vuln in await loaders.root_vulnerabilities.load(ROOT_ID)
        if vuln.state.status == VulnerabilityStateStatus.VULNERABLE
    ]

    assert len(root_vulnerabilities_initial) == 3

    await process_execution(execution_id="group1_")
    loaders_2: Dataloaders = get_new_context()
    root_vulnerabilities = [
        vuln
        for vuln in await loaders_2.root_vulnerabilities.load(ROOT_ID)
        if vuln.state.status == VulnerabilityStateStatus.VULNERABLE
    ]

    assert len(root_vulnerabilities) == 3


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1", name="test_org"),
            ],
            groups=[
                GroupFaker(name="group1", organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id=ROOT_ID,
                    state=GitRootStateFaker(
                        nickname="back",
                    ),
                ),
            ],
            toe_lines=[
                ToeLinesFaker(
                    filename="back/src/index.js",
                    group_name="group1",
                    root_id=ROOT_ID,
                    state=ToeLinesStateFaker(
                        loc=180,
                    ),
                ),
                ToeLinesFaker(
                    filename="back/src/controller/user/index.js",
                    group_name="group1",
                    root_id=ROOT_ID,
                    state=ToeLinesStateFaker(
                        loc=180,
                    ),
                ),
            ],
            findings=[
                FindingFaker(
                    group_name="group1",
                    id="fin_id_1",
                    state=FindingStateFaker(modified_by=MACHINE_EMAIL),
                    title="001. SQL injection - C Sharp SQL API",
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    finding_id="fin_id_1",
                    group_name="group1",
                    id="vuln_id_1",
                    organization_name="test_org",
                    root_id=ROOT_ID,
                    created_by=MACHINE_EMAIL,
                    hacker_email=MACHINE_EMAIL,
                    type=VulnerabilityType.LINES,
                    state=VulnerabilityStateFaker(
                        where="back/src/index.js",
                        specific="35",
                        source=Source.MACHINE,
                    ),
                    verification=VulnerabilityVerificationFaker(
                        status=VulnerabilityVerificationStatus.REQUESTED,
                    ),
                    vuln_machine_hash=3112310311844910506,
                    vuln_skims_description=("sql injection in back/src/index.js line 35"),
                    vuln_skims_method="javascript.sql_injection",
                ),
                VulnerabilityFaker(
                    finding_id="fin_id_1",
                    group_name="group1",
                    id="vuln_id_2",
                    organization_name="test_org",
                    root_id=ROOT_ID,
                    created_by=MACHINE_EMAIL,
                    hacker_email=MACHINE_EMAIL,
                    type=VulnerabilityType.LINES,
                    state=VulnerabilityStateFaker(
                        where="back/src/controller/user/index.js",
                        specific="12",
                        source=Source.MACHINE,
                    ),
                    verification=VulnerabilityVerificationFaker(
                        status=VulnerabilityVerificationStatus.REQUESTED,
                    ),
                    vuln_machine_hash=8061522565195734354,
                    vuln_skims_description="vuln found on root",
                    vuln_skims_method="c_sharp.sql_injection",
                ),
            ],
        ),
    ),
    others=[
        Mock(
            report_machine.process_execution_results,
            "get_config",
            "async",
            {
                "namespace": "back",
                "language": "EN",
                "sast": {"include": ["back/src/"], "exclude": []},
                "sca": {"include": ["back/src/"], "exclude": [], "use_new_sca": True},
            },
        ),
        Mock(
            report_machine.process_execution_results,
            "get_results_log",
            "async",
            load_sarif_file("reattack_results.sarif"),
        ),
    ],
)
async def test_process_execution_reattack() -> None:
    """Testing processing of a reattack request"""
    loaders: Dataloaders = get_new_context()
    requested_verification_vuln = await loaders.vulnerability.load("vuln_id_1")
    assert requested_verification_vuln
    assert (
        requested_verification_vuln.verification is not None
        and requested_verification_vuln.verification.status
        is VulnerabilityVerificationStatus.REQUESTED
    )
    closed_vuln = await loaders.vulnerability.load("vuln_id_2")
    assert closed_vuln
    assert (
        closed_vuln.verification is not None
        and closed_vuln.verification.status is VulnerabilityVerificationStatus.REQUESTED
    )

    await process_execution(execution_id="group1_")

    loaders = get_new_context()
    group_findings = await loaders.group_findings.load("group1")
    finding_001: Finding | None = next(
        (finding for finding in group_findings if "001" in finding.title),
        None,
    )
    assert finding_001 is not None

    open_vulnerabilities: tuple[Vulnerability, ...] = tuple(
        vuln
        for vuln in await loaders.finding_vulnerabilities.load(finding_001.id)
        if vuln.state.status == VulnerabilityStateStatus.VULNERABLE
        and vuln.state.source == Source.MACHINE
        and vuln.root_id == ROOT_ID
    )
    submitted_vulnerabilities: tuple[Vulnerability, ...] = tuple(
        vuln
        for vuln in await loaders.finding_vulnerabilities.load(finding_001.id)
        if vuln.state.status == VulnerabilityStateStatus.SUBMITTED
        and vuln.state.source == Source.MACHINE
        and vuln.root_id == ROOT_ID
    )
    requested_verification_vuln = await loaders.vulnerability.load("vuln_id_1")
    assert requested_verification_vuln
    assert (
        requested_verification_vuln.verification is not None
        and requested_verification_vuln.verification.status
        is VulnerabilityVerificationStatus.VERIFIED
    )
    # The execution must close vulnerabilities in the scope
    closed_vuln = await loaders.vulnerability.load("vuln_id_2")
    assert closed_vuln
    assert (
        closed_vuln.verification is not None
        and closed_vuln.verification.status is VulnerabilityVerificationStatus.VERIFIED
    )
    assert len(open_vulnerabilities) == 1
    assert len(submitted_vulnerabilities) == 1
    assert closed_vuln.state.status == VulnerabilityStateStatus.SAFE
    assert closed_vuln.state.commit == "7fd232de194916018c4ba68f5cb6dc595e99df7e"

    comments = await comments_domain.get_comments(
        loaders=loaders,
        group_name="group1",
        finding_id=finding_001.id,
        user_email="machine@fludidattacks.com",
    )
    assert len(comments) == 1
    for comment in comments:
        assert comment.full_name == "Scanner Services"
        assert comment.email == "machine@fluidattacks.com"
        if "Open vulnerabilities" in comment.content:
            assert "back/src/index.js" in comment.content
        elif "Closed vulnerabilities" in comment.content:
            assert "back/src/controller/user/index.js" in comment.content
            assert "back/src/model/user/index.js" in comment.content


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1", name="test_org"),
            ],
            groups=[
                GroupFaker(name="group1", organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id=ROOT_ID,
                    state=GitRootStateFaker(
                        nickname="back",
                    ),
                ),
            ],
            toe_lines=[
                ToeLinesFaker(
                    filename="back/src/index.js",
                    group_name="group1",
                    root_id=ROOT_ID,
                    state=ToeLinesStateFaker(
                        loc=180,
                    ),
                ),
                ToeLinesFaker(
                    filename="front/index.html",
                    group_name="group1",
                    root_id=ROOT_ID,
                    state=ToeLinesStateFaker(
                        loc=180,
                    ),
                ),
            ],
            findings=[
                FindingFaker(
                    group_name="group1",
                    id="fin_id_1",
                    state=FindingStateFaker(modified_by=MACHINE_EMAIL),
                    title="001. SQL injection - C Sharp SQL API",
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    finding_id="fin_id_1",
                    group_name="group1",
                    id="vuln_id_1",
                    organization_name="test_org",
                    root_id=ROOT_ID,
                    created_by=MACHINE_EMAIL,
                    hacker_email=MACHINE_EMAIL,
                    type=VulnerabilityType.LINES,
                    state=VulnerabilityStateFaker(
                        where="back/src/index.js",
                        specific="35",
                        source=Source.MACHINE,
                    ),
                    vuln_machine_hash=3112310311844910506,
                    vuln_skims_description=("sql injection in back/src/index.js line 35"),
                    vuln_skims_method="javascript.sql_injection",
                ),
                VulnerabilityFaker(
                    finding_id="fin_id_1",
                    group_name="group1",
                    id="vuln_id_2",
                    organization_name="test_org",
                    root_id=ROOT_ID,
                    created_by=MACHINE_EMAIL,
                    hacker_email=MACHINE_EMAIL,
                    type=VulnerabilityType.LINES,
                    state=VulnerabilityStateFaker(
                        where="front/index.html",
                        specific="35",
                        source=Source.MACHINE,
                    ),
                    verification=VulnerabilityVerificationFaker(
                        status=VulnerabilityVerificationStatus.REQUESTED,
                    ),
                    vuln_machine_hash=3112329811844910506,
                    vuln_skims_description=("sql injection in front/index.html line 35"),
                    vuln_skims_method="html.sql_injection",
                ),
                VulnerabilityFaker(
                    finding_id="fin_id_1",
                    group_name="group1",
                    id="vuln_id_3",
                    organization_name="test_org",
                    root_id=ROOT_ID,
                    created_by=MACHINE_EMAIL,
                    hacker_email=MACHINE_EMAIL,
                    type=VulnerabilityType.INPUTS,
                    technique=VulnerabilityTechnique.DAST,
                    state=VulnerabilityStateFaker(
                        where="https://localhost:48000 (back)",
                        specific="missing sql protection",
                        source=Source.MACHINE,
                    ),
                    vuln_machine_hash=3112310311844570506,
                    vuln_skims_description=("header can be injected using fake params"),
                    vuln_skims_method="analyze_headers.sql_injection",
                ),
                VulnerabilityFaker(
                    finding_id="fin_id_1",
                    group_name="group1",
                    id="vuln_id_4",
                    organization_name="test_org",
                    root_id=ROOT_ID,
                    created_by=MACHINE_EMAIL,
                    hacker_email=MACHINE_EMAIL,
                    type=VulnerabilityType.INPUTS,
                    technique=VulnerabilityTechnique.DAST,
                    state=VulnerabilityStateFaker(
                        where="https://localhost:4446 (back)",
                        specific="missing sql protection",
                        source=Source.MACHINE,
                    ),
                    vuln_machine_hash=3112310311844834506,
                    vuln_skims_description=("ssl connection can be injected using fake params"),
                    vuln_skims_method="analyze_protocol.sql_injection",
                ),
                VulnerabilityFaker(
                    finding_id="fin_id_1",
                    group_name="group1",
                    id="vuln_id_5",
                    organization_name="test_org",
                    root_id=ROOT_ID,
                    created_by=MACHINE_EMAIL,
                    hacker_email=MACHINE_EMAIL,
                    type=VulnerabilityType.INPUTS,
                    technique=VulnerabilityTechnique.CSPM,
                    state=VulnerabilityStateFaker(
                        where=(
                            "arn:aws:elasticloadbalancing:us-east-1:"
                            "235620189920:listener/app/k8s (back)"
                        ),
                        specific="/wrong_method",
                        source=Source.MACHINE,
                    ),
                    verification=VulnerabilityVerificationFaker(
                        status=VulnerabilityVerificationStatus.REQUESTED,
                    ),
                    vuln_machine_hash=3112310311455630506,
                    vuln_skims_description=("elbv2 can be infected using sql injection"),
                    vuln_skims_method="aws.elbv2_machines_not_using_https",
                ),
            ],
        ),
    ),
    others=[
        Mock(
            report_machine.process_execution_results,
            "get_config",
            "async",
            {
                "namespace": "back",
                "language": "EN",
                "sast": {"include": ["front/index.html"], "exclude": []},
                "sca": {"include": ["front/index.html"], "exclude": [], "use_new_sca": True},
                "dast": {"urls": ["https://localhost:48000/"]},
                "cspm": {
                    "aws_credentials": [
                        {
                            "access_key_id": "test_key",
                            "secret_access_key": "test_credential",
                        },
                    ],
                },
            },
        ),
        Mock(
            report_machine.process_execution_results,
            "get_results_log",
            "async",
            load_sarif_file("no_results.sarif"),
        ),
    ],
)
async def test_process_execution_included_locations() -> None:
    """
    Testing that vulnerabilities are not closed when:
    - location is not included for SAST vulns
    - http connection failed for DAST http reports
    - ssl connection failed for DAST SSL reports
    - cloud request failed for CSPM reports
    """
    loaders_init: Dataloaders = get_new_context()
    f001_initial_vulns: tuple[Vulnerability, ...] = tuple(
        vuln
        for vuln in await loaders_init.finding_vulnerabilities.load("fin_id_1")
        if vuln.state.source == Source.MACHINE
        and vuln.state.status == VulnerabilityStateStatus.VULNERABLE
    )
    assert len(f001_initial_vulns) == 5
    requested_verification_vuln = await loaders_init.vulnerability.load("vuln_id_5")
    assert requested_verification_vuln
    assert (
        requested_verification_vuln.verification is not None
        and requested_verification_vuln.verification.status
        is VulnerabilityVerificationStatus.REQUESTED
    )

    await process_execution(execution_id="group1_")
    loaders: Dataloaders = get_new_context()
    fin_vulns = await loaders.finding_vulnerabilities.load("fin_id_1")
    f001_open_vulns: tuple[Vulnerability, ...] = tuple(
        vuln
        for vuln in fin_vulns
        if vuln.state.source == Source.MACHINE
        and vuln.state.status == VulnerabilityStateStatus.VULNERABLE
    )
    assert len(f001_open_vulns) == 4
    f001_closed_vulns: tuple[Vulnerability, ...] = tuple(
        vuln
        for vuln in fin_vulns
        if vuln.state.source == Source.MACHINE
        and vuln.state.status == VulnerabilityStateStatus.SAFE
    )
    assert len(f001_closed_vulns) == 1
    assert f001_closed_vulns[0].state.where == "front/index.html"
    assert (
        f001_closed_vulns[0].verification is not None
        and f001_closed_vulns[0].verification.status == VulnerabilityVerificationStatus.VERIFIED
    )
    requested_verification_vuln = await loaders.vulnerability.load("vuln_id_5")
    assert requested_verification_vuln
    assert (
        requested_verification_vuln.verification is not None
        and requested_verification_vuln.verification.status
        is VulnerabilityVerificationStatus.VERIFIED
    )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(name="group1", organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id=ROOT_ID,
                    state=GitRootStateFaker(
                        nickname="back",
                    ),
                ),
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(
                    url="https://myapp.com/end_point_1",
                    id="root_env_id_1",
                    root_id=ROOT_ID,
                    group_name="group1",
                    state=RootEnvironmentUrlStateFaker(url_type=RootEnvironmentUrlType.URL),
                ),
                RootEnvironmentUrlFaker(
                    url="arn:aws:iam::235620189920:role/CSPMRole",
                    id="root_env_id_2",
                    root_id=ROOT_ID,
                    group_name="group1",
                    state=RootEnvironmentUrlStateFaker(
                        url_type=RootEnvironmentUrlType.CSPM,
                        cloud_name=RootEnvironmentCloud.AWS,
                    ),
                ),
                RootEnvironmentUrlFaker(
                    url="my_app.apk",
                    id="root_env_id_3",
                    root_id=ROOT_ID,
                    group_name="group1",
                    state=RootEnvironmentUrlStateFaker(url_type=RootEnvironmentUrlType.APK),
                ),
            ],
        ),
    ),
    others=[
        Mock(
            report_machine.process_execution_results,
            "get_config",
            "async",
            {
                "namespace": "back",
                "language": "EN",
                "apk": {"include": ["glob(**/*.apk)"], "exclude": []},
                "dast": {"urls": ["https://myapp.com/end_point_1"]},
                "cspm": {
                    "aws_credentials": [
                        {
                            "access_key_id": "test_key",
                            "secret_access_key": "test_credential",
                        },
                    ],
                },
            },
        ),
        Mock(
            report_machine.process_execution_results,
            "get_results_log",
            "async",
            load_sarif_file("report_inputs.sarif"),
        ),
    ],
)
async def test_inputs_reports() -> None:
    """
    Testing reports for toe inputs:
    - Redirected url is stored using the original url
    - DAST vulns reported on same domain as existing root env url
    - CSPM vulns reported on existing root env cspm (And special cases)
    - APK vulns reported on existing root env apk
    """
    loaders: Dataloaders = get_new_context()
    findings = await loaders.group_findings.load("group1")
    f_128: Finding | None = next((fin for fin in findings if fin.title.startswith("128")), None)
    assert f_128 is None

    await process_execution(execution_id="group1_")
    loaders.group_findings.clear("group1")

    findings = await loaders.group_findings.load("group1")
    f_128 = next((fin for fin in findings if fin.title.startswith("128")), None)
    assert f_128 is not None

    fin_vulns = await loaders.finding_vulnerabilities.load(f_128.id)
    inputs_vulns = [vuln for vuln in fin_vulns if vuln.type == VulnerabilityType.INPUTS]
    assert len(inputs_vulns) == 6

    vuln_endpoints = {vuln.state.where.split()[0] for vuln in inputs_vulns}
    result_components = {
        "http://myapp.com/end_point_1",
        "https://myapp.com/other_endpoint",
        "arn:aws:elasticloadbalancing:us-east-1:235620189920:listener/app/k8s",
        "arn:aws:s3:::fluid_bucket",
        "arn:aws:iam::aws:policy/AdministratorAccess",
        "my_app.apk",
    }
    assert vuln_endpoints == result_components

    root_toe_inputs = await loaders.root_toe_inputs.load_nodes(
        RootToeInputsRequest(
            group_name="group1",
            root_id=ROOT_ID,
        ),
    )
    assert len(root_toe_inputs) == 6
    toe_inputs_components = {toe_input.component for toe_input in root_toe_inputs}

    assert toe_inputs_components == result_components


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(name="group1", organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id=ROOT_ID,
                    state=GitRootStateFaker(
                        nickname="back",
                    ),
                ),
            ],
            toe_lines=[
                ToeLinesFaker(
                    filename="front/vulnerable.html",
                    group_name="group1",
                    root_id=ROOT_ID,
                    state=ToeLinesStateFaker(
                        loc=180,
                    ),
                ),
                ToeLinesFaker(
                    filename="package.json",
                    group_name="group1",
                    root_id=ROOT_ID,
                    state=ToeLinesStateFaker(
                        loc=1800,
                    ),
                ),
            ],
            findings=[
                FindingFaker(
                    group_name="group1",
                    id="fin_id_1",
                    state=FindingStateFaker(modified_by=MACHINE_EMAIL),
                    title="011. Vulnerable Dependencies",
                ),
                FindingFaker(
                    group_name="group1",
                    id="fin_id_2",
                    state=FindingStateFaker(modified_by=MACHINE_EMAIL),
                    title="065. Vulnerable Form Fields",
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    finding_id="fin_id_1",
                    group_name="group1",
                    id="vuln_id_1",
                    organization_name="test_org",
                    root_id=ROOT_ID,
                    created_by=MACHINE_EMAIL,
                    hacker_email=MACHINE_EMAIL,
                    type=VulnerabilityType.LINES,
                    state=VulnerabilityStateFaker(
                        where="package.json",
                        specific="35",
                        source=Source.MACHINE,
                        advisories=VulnerabilityAdvisory(
                            cve=[
                                "CVE-2019-17495",
                                "SNYK-JAVA-IOSPRINGFOX-1075064",
                            ],
                            package="io.springfox:springfox-swagger-ui",
                            vulnerable_version="2.6.1",
                            epss=50,
                        ),
                    ),
                    technique=VulnerabilityTechnique.SCA,
                    vuln_machine_hash=14936026383834395603,
                    vuln_skims_description=("vuln dependency in package.json line 35"),
                    vuln_skims_method="npm.package_json",
                ),
                VulnerabilityFaker(
                    finding_id="fin_id_2",
                    group_name="group1",
                    id="vuln_id_2",
                    organization_name="test_org",
                    root_id=ROOT_ID,
                    created_by=MACHINE_EMAIL,
                    hacker_email=MACHINE_EMAIL,
                    type=VulnerabilityType.LINES,
                    state=VulnerabilityStateFaker(
                        where="front/vulnerable.html",
                        specific="35",
                        status=VulnerabilityStateStatus.SUBMITTED,
                        source=Source.MACHINE,
                    ),
                    vuln_machine_hash=690544542087116944,
                    vuln_skims_description=(
                        "La aplicación no deshabilita el almacenamiento en caché"
                    ),
                    vuln_skims_method="javascript.sql_injection",
                ),
            ],
        ),
    ),
    others=[
        Mock(
            report_machine.process_execution_results,
            "get_config",
            "async",
            {
                "namespace": "back",
                "language": "EN",
                "sast": {"include": ["."], "exclude": []},
                "sca": {"include": ["."], "exclude": []},
            },
        ),
        Mock(
            report_machine.process_execution_results,
            "get_results_log",
            "async",
            load_sarif_file("report_updated.sarif"),
        ),
    ],
)
async def test_update_reported_vulns() -> None:
    """
    Testing update of following attributes in existing reports:
    - Advisories info
    - severity
    - skims_description
    """
    loaders: Dataloaders = get_new_context()
    vuln_f011_v1 = await loaders.vulnerability.load("vuln_id_1")
    assert vuln_f011_v1
    assert vuln_f011_v1.state.where == "package.json"
    assert vuln_f011_v1.state.advisories is not None
    assert vuln_f011_v1.state.advisories.epss == 50
    assert vuln_f011_v1.state.advisories.cve == [
        "CVE-2019-17495",
        "SNYK-JAVA-IOSPRINGFOX-1075064",
    ]

    sast_vuln_initial = await loaders.vulnerability.load("vuln_id_2")
    assert sast_vuln_initial
    assert sast_vuln_initial.state.status == VulnerabilityStateStatus.SUBMITTED
    assert sast_vuln_initial.severity_score
    assert sast_vuln_initial.severity_score.base_score == Decimal("5.3")
    assert (
        sast_vuln_initial.skims_description
        == "La aplicación no deshabilita el almacenamiento en caché"
    )

    await process_execution(execution_id="group1_")
    loaders.vulnerability.clear_all()
    vuln_f011_v2 = await loaders.vulnerability.load("vuln_id_1")
    assert vuln_f011_v2
    assert vuln_f011_v2.state.advisories
    assert vuln_f011_v2.state.advisories.cve == [
        "CVE-2019-17495",
        "SNYK-JAVA-IOSPRINGFOX-1075064",
        "CVE-2019-TEST",
    ]
    assert vuln_f011_v2.state.advisories.epss == 80

    sast_vuln_v2 = await loaders.vulnerability.load("vuln_id_2")
    assert sast_vuln_v2
    assert sast_vuln_v2.state.status == VulnerabilityStateStatus.VULNERABLE
    severity_2 = sast_vuln_v2.severity_score
    assert severity_2 == SeverityScore(
        base_score=Decimal("8.4"),
        temporal_score=Decimal("7.3"),
        cvss_v3=("CVSS:3.1/AV:L/AC:L/PR:L/UI:N/S:C/C:H/I:H/A:N/E:U/RL:O/RC:C"),
        cvssf=Decimal("97.006"),
        cvssf_v4=Decimal("12.126"),
        threat_score=Decimal("5.8"),
        cvss_v4="CVSS:4.0/AV:L/AC:L/AT:N/PR:L/UI:N/VC:H/VI:H/VA:N/SC:L/SI:L/SA:L/E:U",
    )
    assert sast_vuln_v2.skims_description == (
        "La aplicación no deshabilita el almacenamiento en caché, "
        "por lo que la información quedará almacenada en el navegador"
    )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(name="group1", organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id=ROOT_ID,
                    state=GitRootStateFaker(nickname="back"),
                ),
            ],
            toe_lines=[
                ToeLinesFaker(
                    filename="f120/requirements.txt",
                    group_name="group1",
                    root_id=ROOT_ID,
                ),
            ],
        ),
    ),
    others=[
        Mock(
            report_machine.process_execution_results,
            "get_config",
            "async",
            {
                "namespace": "back",
                "language": "EN",
                "sca": {"include": ["."], "exclude": []},
            },
        ),
        Mock(
            report_machine.process_execution_results,
            "get_results_log",
            "async",
            load_sarif_file("report_f120.sarif"),
        ),
    ],
)
async def test_process_f120_reports() -> None:
    """Testing special f120 reports"""
    loaders: Dataloaders = get_new_context()

    findings = await loaders.group_findings.load("group1")
    assert not findings

    await process_execution(execution_id="group1_")
    loaders.group_findings.clear("group1")
    group_findings = await loaders.group_findings.load("group1")
    finding_f120 = next(
        (finding for finding in group_findings if finding.title.startswith("120")),
        None,
    )
    assert finding_f120 is not None

    integrates_vulnerabilities: tuple[Vulnerability, ...] = tuple(
        vuln
        for vuln in await loaders.finding_vulnerabilities.load(finding_f120.id)
        if vuln.state.status == VulnerabilityStateStatus.VULNERABLE
        and vuln.state.source == Source.MACHINE
    )

    assert len(integrates_vulnerabilities) == 2

    status_1 = integrates_vulnerabilities[0].state.status
    where_1 = integrates_vulnerabilities[0].state.where

    assert status_1 == VulnerabilityStateStatus.VULNERABLE
    location_1 = "f120/requirements.txt (missing dependency: urllib3)"
    location_2 = "f120/requirements.txt (missing dependency: botocore)"
    assert where_1 in {location_1, location_2}


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(name="group1", organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id=ROOT_ID,
                    state=GitRootStateFaker(
                        nickname="back",
                    ),
                ),
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(
                    url="https://localhost:48000/",
                    id="root_env_id_1",
                    root_id=ROOT_ID,
                    group_name="group1",
                    state=RootEnvironmentUrlStateFaker(url_type=RootEnvironmentUrlType.URL),
                ),
            ],
            toe_lines=[
                ToeLinesFaker(
                    filename="back/src/index.js",
                    group_name="group1",
                    root_id=ROOT_ID,
                    state=ToeLinesStateFaker(
                        loc=100,
                    ),
                ),
            ],
            findings=[
                FindingFaker(
                    group_name="group1",
                    id="fin_id_1",
                    state=FindingStateFaker(
                        modified_by="hacker@fluidattacks.com",
                        source=Source.ANALYST,
                    ),
                    title="129. Insecure headers",
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    finding_id="fin_id_1",
                    group_name="group1",
                    id="vuln_id_1",
                    organization_name="test_org",
                    root_id=ROOT_ID,
                    created_by="hacker@fluidattacks.com",
                    hacker_email="hacker@fluidattacks.com",
                    type=VulnerabilityType.LINES,
                    state=VulnerabilityStateFaker(
                        where="back/src/index.js",
                        specific="35",
                        source=Source.ANALYST,
                    ),
                ),
                VulnerabilityFaker(
                    finding_id="fin_id_1",
                    group_name="group1",
                    id="vuln_id_2",
                    organization_name="test_org",
                    root_id=ROOT_ID,
                    created_by="hacker@fluidattacks.com",
                    hacker_email="hacker@fluidattacks.com",
                    type=VulnerabilityType.INPUTS,
                    state=VulnerabilityStateFaker(
                        where="http://localhost:48000/",
                        specific="header not set",
                        source=Source.ANALYST,
                    ),
                ),
            ],
        ),
    ),
    others=[
        Mock(
            report_machine.process_execution_results,
            "get_config",
            "async",
            {
                "namespace": "back",
                "language": "EN",
                "sca": {"include": ["."], "exclude": []},
            },
        ),
        Mock(
            report_machine.process_execution_results,
            "get_results_log",
            "async",
            load_sarif_file("analyst_comparison.sarif"),
        ),
    ],
)
async def test_process_vuln_analyst_comparison() -> None:
    """Testing vulnerabilities comparison with analysts reports"""
    loaders: Dataloaders = get_new_context()

    hacker_vulns: tuple[Vulnerability, ...] = tuple(
        vuln
        for vuln in await loaders.finding_vulnerabilities.load("fin_id_1")
        if vuln.state.status == VulnerabilityStateStatus.VULNERABLE
    )
    assert len(hacker_vulns) == 2
    await process_execution(execution_id="group1_")
    loaders = get_new_context()
    group_findings = await loaders.group_findings.load("group1")
    finding_129_machine: Finding | None = next(
        (
            finding
            for finding in group_findings
            if finding.title.startswith("129.")
            and finding.creation
            and finding.creation.modified_by == "machine@fluidattacks.com"
        ),
        None,
    )
    assert finding_129_machine is not None
    machine_vulnerabilities: tuple[Vulnerability, ...] = tuple(
        vuln
        for vuln in await loaders.finding_vulnerabilities.load(finding_129_machine.id)
        if vuln.state.source == Source.MACHINE
    )
    assert len(machine_vulnerabilities) == 3
    draft_machine_vulnerabilities: tuple[Vulnerability, ...] = tuple(
        vuln
        for vuln in await loaders.finding_vulnerabilities.load(finding_129_machine.id)
        if vuln.state.source == Source.MACHINE
        and vuln.state.status == VulnerabilityStateStatus.SUBMITTED
    )
    assert len(draft_machine_vulnerabilities) == 2


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(name="group1", organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id=ROOT_ID,
                    state=GitRootStateFaker(
                        nickname="back",
                    ),
                ),
            ],
            toe_lines=[
                ToeLinesFaker(
                    filename="common/marketplace/infra/roles.tf",
                    group_name="group1",
                    root_id=ROOT_ID,
                    state=ToeLinesStateFaker(
                        loc=200,
                    ),
                ),
            ],
        ),
    ),
    others=[
        Mock(
            report_machine.process_execution_results,
            "get_config",
            "async",
            {
                "namespace": "back",
                "language": "EN",
                "sca": {"include": ["."], "exclude": []},
            },
        ),
        Mock(
            report_machine.process_execution_results,
            "get_results_log",
            "async",
            load_sarif_file("report_non_duplicates.sarif"),
        ),
    ],
)
async def test_non_duplicate_reports() -> None:
    """Testing reports with different hashes are submitted to the DB"""
    loaders: Dataloaders = get_new_context()
    group_findings = await loaders.group_findings.load("group1")
    assert len(group_findings) == 0
    await process_execution(execution_id="group1_")
    loaders.group_findings.clear("group1")
    group_findings = await loaders.group_findings.load("group1")
    fin_005: Finding | None = next(
        (finding for finding in group_findings if "005" in finding.title),
        None,
    )
    assert fin_005 is not None
    f005_vulns: tuple[Vulnerability, ...] = tuple(
        vuln
        for vuln in await loaders.finding_vulnerabilities.load(fin_005.id)
        if vuln.state.source == Source.MACHINE
        and vuln.state.status == VulnerabilityStateStatus.VULNERABLE
    )
    assert len(f005_vulns) == 2
    assert all(vuln.state.where == "common/marketplace/infra/roles.tf" for vuln in f005_vulns)
    assert all(vuln.state.specific == "101" for vuln in f005_vulns)


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(name="group1", organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id=ROOT_ID,
                    state=GitRootStateFaker(nickname="front"),
                ),
            ],
        ),
    ),
    others=[
        Mock(
            report_machine.process_execution_results,
            "get_config",
            "async",
            {
                "namespace": "back",
                "language": "EN",
                "sast": {"include": ["."], "exclude": []},
            },
        )
    ],
)
async def test_process_execution_no_root() -> None:
    """Testing failure of execution due to root not found"""
    result = await process_execution(execution_id="group1_")
    assert "Could not find root for the execution" in result


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(name="group1", organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id=ROOT_ID,
                    state=GitRootStateFaker(nickname="back"),
                ),
            ],
        ),
    ),
    others=[
        Mock(
            report_machine.process_execution_results,
            "get_config",
            "async",
            {
                "namespace": "back",
                "language": "EN",
                "sca": {"include": ["."], "exclude": []},
            },
        ),
        Mock(
            report_machine.process_execution_results,
            "get_results_log",
            "async",
            None,
        ),
    ],
)
async def test_process_execution_no_results() -> None:
    result = await process_execution(execution_id="group1_")
    assert result == (
        False,
        "Could not find execution result",
    )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(
                    name="group1",
                    organization_id="org1",
                    state=GroupStateFaker(has_essential=False),
                ),
            ],
        ),
    )
)
async def test_process_execution_no_machine_services() -> None:
    """Testing failure of execution due to root not found"""
    result = await process_execution(execution_id="group1_")
    assert result == (
        False,
        "Machine services is not included or is under review",
    )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(name="group1", organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id=ROOT_ID,
                    state=GitRootStateFaker(
                        nickname="back",
                    ),
                ),
            ],
            toe_lines=[
                ToeLinesFaker(
                    filename="front/yarn.lock",
                    group_name="group1",
                    root_id=ROOT_ID,
                    state=ToeLinesStateFaker(
                        loc=1800,
                    ),
                ),
                ToeLinesFaker(
                    filename="back/pyproject.toml",
                    group_name="group1",
                    root_id=ROOT_ID,
                    state=ToeLinesStateFaker(
                        loc=1000,
                    ),
                ),
            ],
            findings=[
                FindingFaker(
                    group_name="group1",
                    id="fin_id_1",
                    state=FindingStateFaker(modified_by=MACHINE_EMAIL),
                    title="011. Use of software with known vulnerabilities",
                    evidences=FindingEvidences(
                        evidence1=FindingEvidence(
                            description="evidence1",
                            url=("group1-3c475384-834c-47b0-ac71-a41a022e401c-evidence1"),
                            modified_date=DATE_2019,
                        ),
                    ),
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    finding_id="fin_id_1",
                    group_name="group1",
                    id="vuln_id_1",
                    organization_name="test_org",
                    root_id=ROOT_ID,
                    created_by=MACHINE_EMAIL,
                    hacker_email=MACHINE_EMAIL,
                    type=VulnerabilityType.LINES,
                    state=VulnerabilityStateFaker(
                        where="front/yarn.lock",
                        specific="15",
                        source=Source.MACHINE,
                        advisories=VulnerabilityAdvisory(
                            cve=["CVE-2023-45857", "GHSA-wf5p-g6vw-rhxx"],
                            package="axios",
                            vulnerable_version="1.3.4",
                        ),
                    ),
                    vuln_machine_hash=3169643315503036729,
                    vuln_skims_description=("Vulnerable dependency yarn.lock"),
                    vuln_skims_method="npm.npm_yarn_lock",
                ),
            ],
        ),
    ),
    others=[
        Mock(
            report_machine.process_execution_results,
            "get_config",
            "async",
            {
                "namespace": "back",
                "language": "EN",
                "sca": {"include": ["."], "exclude": [], "use_new_sca": True},
            },
        ),
        Mock(
            report_machine.process_execution_results,
            "get_results_log",
            "async",
            load_sarif_file("new_sca_results.sarif"),
        ),
    ],
)
async def test_process_new_sca_vulns() -> None:
    """Testing processing of new sca vulns, ensuring no duplicates between new and old"""
    loaders = get_new_context()
    group_findings = await loaders.group_findings.load("group1")
    assert len(group_findings) == 1
    finding_011: Finding | None = next(
        (finding for finding in group_findings if "011" in finding.title),
        None,
    )
    assert finding_011 is not None

    open_sca_old_vulnerabilities: tuple[Vulnerability, ...] = tuple(
        vuln
        for vuln in await loaders.finding_vulnerabilities.load(finding_011.id)
        if vuln.state.status == VulnerabilityStateStatus.VULNERABLE
        and vuln.state.source == Source.MACHINE
        and vuln.root_id == ROOT_ID
        and vuln.state.advisories
    )
    assert len(open_sca_old_vulnerabilities) == 1

    await process_execution(execution_id="group1_")

    new_loaders = get_new_context()
    group_findings_titles = [fin.title for fin in await new_loaders.group_findings.load("group1")]
    assert len(group_findings_titles) == 3
    assert set(group_findings_titles) == {
        "011. Use of software with known vulnerabilities",
        "004. Remote command execution",
        "390. Prototype Pollution",
    }
    root_vulnerabilities_after = [
        vuln
        for vuln in await new_loaders.root_vulnerabilities.load(ROOT_ID)
        if vuln.state.status == VulnerabilityStateStatus.VULNERABLE
        and vuln.hacker_email == "machine@fluidattacks.com"
        and vuln.root_id == ROOT_ID
    ]

    open_sca_old_vulnerabilities_summary = [
        (
            vuln.state.where,
            vuln.state.specific,
            vuln.state.advisories.package,
            vuln.state.advisories.vulnerable_version,
        )
        for vuln in root_vulnerabilities_after
        if vuln.finding_id == finding_011.id and vuln.state.advisories
    ]
    assert len(open_sca_old_vulnerabilities_summary) == 1
    assert open_sca_old_vulnerabilities_summary == [("front/yarn.lock", "15", "axios", "1.3.4")]

    open_sca_new_vulnerabilities_summary = {
        (
            vuln.state.where,
            vuln.state.specific,
            vuln.state.advisories.package,
            vuln.state.advisories.vulnerable_version,
        )
        for vuln in root_vulnerabilities_after
        if vuln.finding_id != finding_011.id
        and vuln.technique == VulnerabilityTechnique.SCA
        and vuln.state.advisories
    }

    assert len(open_sca_new_vulnerabilities_summary) == 2

    assert open_sca_new_vulnerabilities_summary == {
        ("front/yarn.lock", "26", "jquery", "2.1.3"),
        ("back/pyproject.toml", "13", "GitPython", "2.1.41"),
    }
