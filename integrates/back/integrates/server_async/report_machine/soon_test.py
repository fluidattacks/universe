import json
import os

from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.enums import Source
from integrates.db_model.findings.types import Finding
from integrates.db_model.vulnerabilities.enums import VulnerabilityStateStatus, VulnerabilityType
from integrates.db_model.vulnerabilities.types import Vulnerability
from integrates.server_async import report_machine
from integrates.server_async.report_machine.soon import (
    process_vulnerabilities,
    process_vulnerability_llm,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    FindingFaker,
    FindingStateFaker,
    GitRootFaker,
    GitRootStateFaker,
    GroupFaker,
    OrganizationFaker,
    ToeLinesFaker,
    ToeLinesStateFaker,
    VulnerabilityFaker,
    VulnerabilityStateFaker,
)
from integrates.testing.mocks import Mock, mocks

ROOT_ID = "root1"
MACHINE_EMAIL = "machine@fluidattacks.com"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[
                GroupFaker(name="group1", organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id=ROOT_ID,
                    state=GitRootStateFaker(nickname="back"),
                ),
            ],
            toe_lines=[
                ToeLinesFaker(
                    filename="back/src/index.js",
                    group_name="group1",
                    root_id=ROOT_ID,
                    state=ToeLinesStateFaker(loc=180),
                ),
            ],
        ),
    )
)
async def test_process_report_soon() -> None:
    """Testing normal processing of report_soon"""
    path: str = os.path.join(os.path.dirname(__file__), "test-data", "report_soon.json")
    with open(path, "rb") as sarif:
        vuln_file = json.load(sarif)

    loaders: Dataloaders = get_new_context()
    group_findings = await loaders.group_findings.load("group1")
    assert len(group_findings) == 0
    result = await process_vulnerabilities("group1", "back", "246", vuln_file)

    assert result == (
        True,
        "Vulnerabilities have been processed",
    )
    loaders.group_findings.clear("group1")
    findings = await loaders.group_findings.load("group1")

    machine_fin_246: Finding | None = next(
        (
            finding
            for finding in findings
            if finding.title.startswith("246")
            and finding.creation
            and finding.creation.modified_by == "machine@fluidattacks.com"
        ),
        None,
    )
    assert machine_fin_246 is not None
    assert machine_fin_246.evidences.evidence5 is not None
    assert machine_fin_246.evidences.evidence1 is None
    f246_vulnerabilities: tuple[Vulnerability, ...] = tuple(
        vuln
        for vuln in await loaders.finding_vulnerabilities.load(machine_fin_246.id)
        if vuln.state.source == Source.MACHINE
        and vuln.state.status == VulnerabilityStateStatus.VULNERABLE
    )
    assert len(f246_vulnerabilities) == 1
    assert f246_vulnerabilities[0] is not None
    assert str(f246_vulnerabilities[0].hash) == "16412987849816290000"
    assert f246_vulnerabilities[0].state.modified_by == "machine@fluidattacks.com"

    result = await process_vulnerabilities("group1", "back", "246", vuln_file)
    assert result == (
        False,
        "Vulnerability exists in the root",
    )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[GroupFaker(name="group1", organization_id="org1")],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id=ROOT_ID,
                    state=GitRootStateFaker(nickname="back"),
                ),
            ],
            toe_lines=[
                ToeLinesFaker(
                    filename="back/src/index.js",
                    group_name="group1",
                    root_id=ROOT_ID,
                    state=ToeLinesStateFaker(loc=180),
                ),
            ],
            findings=[
                FindingFaker(
                    group_name="group1",
                    id="fin_id_1",
                    state=FindingStateFaker(modified_by=MACHINE_EMAIL),
                    title="246. Unsafe configuration",
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    finding_id="fin_id_1",
                    group_name="group1",
                    id="vuln_id_1",
                    organization_name="test_org",
                    root_id=ROOT_ID,
                    created_by=MACHINE_EMAIL,
                    hacker_email=MACHINE_EMAIL,
                    type=VulnerabilityType.LINES,
                    state=VulnerabilityStateFaker(
                        where="back/src/index.js", specific="10", source=Source.MACHINE
                    ),
                    vuln_machine_hash=9326911172693336000,
                    vuln_skims_description=("Unsafe configuration at back/src/index.js"),
                    vuln_skims_method="javascript.test_method",
                ),
            ],
        ),
    )
)
async def test_process_report_soon_existing_finding() -> None:
    """Testing normal processing of report_soon"""
    path: str = os.path.join(os.path.dirname(__file__), "test-data", "report_soon.json")
    with open(path, "rb") as sarif:
        vuln_file = json.load(sarif)

    loaders: Dataloaders = get_new_context()
    group_findings = await loaders.group_findings.load("group1")
    assert len(group_findings) == 1
    fin_vulns = await loaders.finding_vulnerabilities.load(group_findings[0].id)
    assert len(fin_vulns) == 1

    result = await process_vulnerabilities("group1", "back", "246", vuln_file)
    assert result == (
        True,
        "Vulnerabilities have been processed",
    )
    loaders_new: Dataloaders = get_new_context()
    group_findings_new = await loaders_new.group_findings.load("group1")
    assert len(group_findings_new) == 1
    fin_vulns_new = await loaders_new.finding_vulnerabilities.load(group_findings[0].id)
    assert len(fin_vulns_new) == 2


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[GroupFaker(name="group1", organization_id="org1")],
            roots=[
                GitRootFaker(
                    group_name="group1", id=ROOT_ID, state=GitRootStateFaker(nickname="front")
                ),
            ],
        ),
    )
)
async def test_process_report_soon_fail_1() -> None:
    """Testing normal processing of report_soon"""
    vuln_file = {
        "hash": 46412987849816290000,
        "line": "20",
        "path": "back/src/model/user/index.js",
    }
    result = await process_vulnerabilities("group1", "back", "246", vuln_file)

    assert result == (False, "Git root is not found")


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[GroupFaker(name="group1", organization_id="org1")],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id=ROOT_ID,
                    state=GitRootStateFaker(nickname="back"),
                ),
            ],
            toe_lines=[
                ToeLinesFaker(
                    filename="back/src/index.js",
                    group_name="group1",
                    root_id=ROOT_ID,
                    state=ToeLinesStateFaker(loc=180),
                ),
            ],
        ),
    ),
    others=[Mock(report_machine.soon, "upload_evidence", "async", False)],
)
async def test_process_report_soon_evidence_fail() -> None:
    vuln_file = {
        "commit_hash": "992dd830efc76857a252e16bc740409a4d16c0be",
        "cwe_ids": ["CWE-770"],
        "developer": "flagos@fluidattacks.com",
        "hash": 46412987849816290000,
        "line": "20",
        "path": "back/src/model/user/index.js",
        "message": {
            "description": "Property vulnerable",
            "snippet": "  snippet \n hello",
        },
    }

    result = await process_vulnerabilities("group1", "back", "246", vuln_file)

    assert result == (
        False,
        "An error ocurred creating the finding evidence",
    )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[
                GroupFaker(name="group1", organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id=ROOT_ID,
                    state=GitRootStateFaker(nickname="back"),
                ),
            ],
            toe_lines=[
                ToeLinesFaker(
                    filename="back/src/index.js",
                    group_name="group1",
                    root_id=ROOT_ID,
                    state=ToeLinesStateFaker(loc=180),
                ),
            ],
            llm_scan=[
                {  # type: ignore
                    "pk": f"ROOT#{ROOT_ID}#PATH#1",
                    "sk": "SNIPPET#1#CANDIDATE#1",
                    "group_name": "no-group",
                    "root_id": ROOT_ID,
                    "where": "1",
                    "snippet_hash": "1",
                    "specific": "1",
                },
                {  # type: ignore
                    "pk": "PATH#back/src/index.js#SNIPPET#1",
                    "sk": f"ROOT#{ROOT_ID}",
                    "group_name": "group1",
                    "root_id": ROOT_ID,
                    "where": "back/src/index.js",
                    "snippet_hash": "1",
                    "snippet_content": "snippet",
                },
                {  # type: ignore
                    "pk": f"ROOT#{ROOT_ID}#PATH#back/src/index.js",
                    "sk": "SNIPPET#2#CANDIDATE#2",
                    "group_name": "group1",
                    "root_id": ROOT_ID,
                    "where": "back/src/index.js",
                    "snippet_hash": "1",
                    "specific": "2",
                    "suggested_criteria_code": "001",
                    "reason": "reason",
                    "commit": "commit",
                },
            ],
        ),
    )
)
async def test_process_vulnerability_llm() -> None:
    """Testing normal processing of report_soon"""
    # not eixting vulnerability llm
    assert await process_vulnerability_llm(
        root_id=ROOT_ID, where="back/src/index.js", snippet_hash="none", candidate_id="none"
    ) == (False, "Vulnerability is not found")
    await process_vulnerability_llm(
        root_id=ROOT_ID, where="back/src/index.js", snippet_hash="2", candidate_id="2"
    )
    loaders = get_new_context()
    assert len(await loaders.root_vulnerabilities.load(ROOT_ID)) > 0
