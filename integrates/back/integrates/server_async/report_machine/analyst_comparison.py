from collections.abc import (
    Iterable,
)

from integrates.custom_utils.vulnerabilities import (
    is_machine_vuln,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.db_model.roots.types import (
    GitRoot,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
    VulnerabilityType,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
)


async def get_reports_from_analysts(
    *,
    loaders: Dataloaders,
    git_root: GitRoot,
    rule_findings: tuple[Finding, ...],
) -> dict[str, Iterable]:
    findings_ids = [fin.id for fin in rule_findings]

    existing_analyst_vulns: tuple[Vulnerability, ...] = tuple(
        vuln
        for vuln in await loaders.finding_vulnerabilities.load_many_chained(findings_ids)
        if not is_machine_vuln(vuln)
        and vuln.root_id == git_root.id
        and vuln.state.status == VulnerabilityStateStatus.VULNERABLE
    )
    existing_url_reports: list[str] = [
        vuln.get_path().rstrip("/")
        for vuln in existing_analyst_vulns
        if vuln.type == VulnerabilityType.INPUTS
    ]
    existing_lines_reports: list[tuple[str, str]] = [
        (vuln.state.where, vuln.state.specific)
        for vuln in existing_analyst_vulns
        if vuln.type == VulnerabilityType.LINES
    ]

    return {
        "inputs": existing_url_reports,
        "lines": existing_lines_reports,
    }
