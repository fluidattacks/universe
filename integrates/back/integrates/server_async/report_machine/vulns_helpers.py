from integrates.class_types.types import (
    Item,
)
from integrates.custom_utils.files import (
    path_is_include,
)
from integrates.custom_utils.vulnerabilities import (
    get_path_from_integrates_vulnerability,
    is_machine_vuln,
)
from integrates.dataloaders import Dataloaders
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
    VulnerabilityTechnique,
    VulnerabilityType,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
)
from integrates.server_async.report_machine.report_types import ScaNewInfo, VulnState


def _get_vuln_state(vulnerability: Item, state: str) -> str | None:
    if vulnerability["properties"].get("auto_approve", False):
        return "open"
    return state


def get_vuln_machine_hash(vulnerability: Item) -> int:
    return int(vulnerability["fingerprints"]["guid"])


def _get_path_from_sarif_vulnerability(vulnerability: Item) -> str:
    what = vulnerability["locations"][0]["physicalLocation"]["artifactLocation"]["uri"]

    if (
        vulnerability["ruleId"] == "120"
        and vulnerability.get("properties", {}).get("source_method")
        == "python.pip_incomplete_dependencies_list"
        and (dependency := vulnerability["message"].get("properties", {}).get("package"))
    ):
        what = f"{what} (missing dependency: {dependency})"
    return what


def _get_input_url(vuln: Item, repo_nickname: str) -> str:
    url: str
    if vuln["properties"].get("has_redirect", False):
        url = vuln["properties"]["original_url"]
    else:
        url = vuln["locations"][0]["physicalLocation"]["artifactLocation"]["uri"]
    url = url.rstrip("/")
    return f"{url} ({repo_nickname})"


def _get_advisories(vulnerability: Item) -> Item:
    if (
        vulnerability["properties"]["kind"] == "SCA"
        and (
            vulnerability["ruleId"] in {"011", "393"}
            or vulnerability["properties"].get("is_sca_new", False)
        )
        and (props := vulnerability["message"].get("properties"))
    ):
        return {
            "package": props["package"],
            "vulnerable_version": props["vulnerable_version"],
            "cve": props["cve"],
            "epss": props["epss"] if props["epss"].isdigit() else None,
        }

    if reachable_props := vulnerability["properties"].get("reachability_info"):
        return {
            "package": reachable_props["package"],
            "vulnerable_version": reachable_props["vulnerable_version"],
            "cve": reachable_props["cve"],
        }
    return {}


def get_sarif_vuln_info(vuln: Item, repo: str, state: str, commit: str) -> Item:
    specific_attrs = {}
    text_message = vuln["locations"][0]["physicalLocation"]["region"]["message"]["text"]
    if vuln["properties"]["vulnerability_type"] == VulnerabilityType.INPUTS.value:
        specific_attrs.update(
            {
                "field": text_message,
                "stream": vuln["properties"]["stream"],
                "url": _get_input_url(vuln, repo),
            },
        )
    else:
        specific_attrs.update(
            {
                "commit_hash": commit,
                "line": text_message,
                "path": _get_path_from_sarif_vulnerability(vuln),
                "advisories": _get_advisories(vuln),
            },
        )
    return {
        "repo_nickname": repo,
        "state": _get_vuln_state(vuln, state),
        "skims_method": vuln["properties"]["source_method"],
        "skims_description": vuln["message"]["text"],
        "developer": vuln["properties"]["method_developer"],
        "source": "MACHINE",
        "hash": get_vuln_machine_hash(vuln),
        "cwe_ids": vuln["properties"]["cwe_ids"],
        "cvss_v3": vuln["properties"]["cvss"],
        "cvss_v4": vuln["properties"]["cvss_v4"],
        "technique": vuln["properties"]["kind"],
        **specific_attrs,
    }


def get_integrates_vuln_info(
    vuln: Vulnerability,
    root_nickname: str,
    state: VulnState,
    commit: str | None,
) -> Item:
    specific_attrs: Item = {}
    if vuln.type == VulnerabilityType.INPUTS:
        specific_attrs.update(
            {
                "field": vuln.state.specific,
                "stream": ",".join(vuln.stream or []),
                "url": vuln.state.where,
            },
        )
    else:
        specific_attrs.update(
            {
                "commit_hash": commit or vuln.state.commit,
                "line": vuln.state.specific,
                "path": vuln.state.where,
                "advisories": {
                    "package": vuln.state.advisories.package,
                    "vulnerable_version": (vuln.state.advisories.vulnerable_version),
                    "cve": vuln.state.advisories.cve,
                    "epss": vuln.state.advisories.epss,
                }
                if vuln.state.advisories
                else None,
            },
        )
    return {
        "repo_nickname": root_nickname,
        "state": state,
        "skims_method": vuln.skims_method,
        "skims_description": vuln.skims_description,
        "technique": vuln.technique,
        "developer": vuln.developer,
        "source": vuln.state.source.value,
        "hash": vuln.hash,
        "cwe_ids": vuln.cwe_ids,
        "cvss_v3": vuln.severity_score.cvss_v3 if vuln.severity_score else None,
        "cvss_v4": vuln.severity_score.cvss_v4 if vuln.severity_score else None,
        **specific_attrs,
    }


def was_request_successful(
    vuln_path: str,
    vuln_skims_method: str,
    sarif_urls: dict[str, dict],
) -> bool:
    if (
        vuln_skims_method.startswith(("analyze_headers", "analyze_content", "analyze_dns"))
        and (http_responses := sarif_urls["http_responses"])
        and vuln_path in http_responses
    ):
        return http_responses[vuln_path] < 400

    if (
        vuln_skims_method.startswith("analyze_protocol")
        and (ssl_responses := sarif_urls["ssl_responses"])
        and vuln_path in ssl_responses
    ):
        return ssl_responses[vuln_path] is True

    if vuln_skims_method.startswith(("aws", "azure", "gcp")) and (
        cspm_responses := sarif_urls.get("cspm_responses")
    ):
        if vuln_skims_method.startswith("aws"):
            return vuln_skims_method not in cspm_responses["aws"]
        if vuln_skims_method.startswith("azure"):
            return vuln_skims_method not in cspm_responses["azure"]
        if vuln_skims_method.startswith("gcp"):
            return vuln_skims_method not in cspm_responses["gcp"]

    return True


def _was_cspm_env_included_in_execution(vuln: Vulnerability, execution_config: Item) -> bool:
    is_env_included = False
    cspm_config: dict = execution_config.get("cspm", {})
    for platform in ("aws", "azure", "gcp"):
        if str(vuln.skims_method).startswith(platform):
            is_env_included = len(cspm_config.get(f"{platform}_credentials", [])) > 0
            break
    return is_env_included


def _get_sast_or_sca_paths_from_config(
    vuln: Vulnerability,
    execution_config: Item,
) -> tuple[list[str], list[str]]:
    config_key = "sast"
    if vuln.technique == VulnerabilityTechnique.SCA or vuln.state.advisories:
        config_key = "sca"

    included_paths = execution_config.get(config_key, {}).get("include") or []
    excluded_paths = execution_config.get(config_key, {}).get("exclude") or []
    return (included_paths, excluded_paths)


def was_location_included_in_execution(
    vuln: Vulnerability,
    execution_config: Item,
) -> bool:
    """
    Checks if the vuln location was included on the analysis
    configuration file
    """
    is_vuln_included = False
    if vuln.type == VulnerabilityType.LINES:
        included_paths, excluded_paths = _get_sast_or_sca_paths_from_config(vuln, execution_config)
        is_vuln_included = path_is_include(
            (
                get_path_from_integrates_vulnerability(
                    vuln.state.where,
                    vuln.type,
                    ignore_cve=True,
                )[1]
            ).split(" ", maxsplit=1)[0],
            included_paths,
            excluded_paths,
        )
    elif vuln.state.where.endswith("apk"):
        is_vuln_included = execution_config.get("apk", None) is not None
    elif str(vuln.skims_method).startswith(("aws", "azure", "gcp")):
        is_vuln_included = _was_cspm_env_included_in_execution(vuln, execution_config)
    elif vuln.technique == VulnerabilityTechnique.DAST:
        is_vuln_included = execution_config.get("dast", None) is not None

    return is_vuln_included


def get_vuln_info(vuln: dict) -> tuple[str, str, str, str]:
    return (
        vuln["locations"][0]["physicalLocation"]["artifactLocation"]["uri"],
        vuln["locations"][0]["physicalLocation"]["region"]["message"]["text"],
        vuln["message"]["properties"]["package"],
        vuln["message"]["properties"]["vulnerable_version"],
    )


async def get_sca_new_required_info(
    loaders: Dataloaders, group_name: str, root_id: str, sarif_log: Item
) -> ScaNewInfo:
    new_sca_vulns = [
        vuln
        for vuln in sarif_log["runs"][0]["results"]
        if vuln["properties"].get("is_sca_new") is True
    ]

    if not new_sca_vulns:
        return ScaNewInfo(new_sca_vulns_to_report=[])

    sca_old_findings_ids = [
        fin.id
        for fin in await loaders.group_findings.load(group_name)
        if fin.title.startswith(("011", "393"))
    ]
    existing_dependencies_reported: set[tuple[str, str, str, str]] = {
        (
            vuln.state.where,
            vuln.state.specific,
            vuln.state.advisories.package,
            vuln.state.advisories.vulnerable_version,
        )
        for vuln in await loaders.finding_vulnerabilities.load_many_chained(sca_old_findings_ids)
        if is_machine_vuln(vuln)
        and vuln.root_id == root_id
        and vuln.state.status == VulnerabilityStateStatus.VULNERABLE
        and vuln.state.advisories
    }

    new_sca_vulns_to_report = [
        get_vuln_machine_hash(vuln)
        for vuln in new_sca_vulns
        if get_vuln_info(vuln) not in existing_dependencies_reported
    ]

    return ScaNewInfo(new_sca_vulns_to_report=new_sca_vulns_to_report)
