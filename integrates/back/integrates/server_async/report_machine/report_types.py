from typing import Literal, NamedTuple, TypeAlias

VulnState: TypeAlias = Literal["open", "closed", "deleted"]


class ScaNewInfo(NamedTuple):
    new_sca_vulns_to_report: list[int]


class FindingSarifVulnerabilitiesData(NamedTuple):
    fin_code: str
    commit: str
    finding_reported_vulnerabilities: list[dict]
    sarif_error_responses: dict[str, dict[str, str | None | list[str]]]
    new_sca_data: ScaNewInfo | None = None
