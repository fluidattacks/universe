from integrates.db_model.toe_packages.types import ToePackage
from integrates.server_async.report_sbom.process import process_db_packages
from integrates.testing.fakers import ToePackageCoordinatesFaker, ToePackageFaker


def test_process_db_packages_empty_execution() -> None:
    root_id = "root1"
    db_toe_packages = [
        ToePackageFaker(
            group_name="group1",
            root_id=root_id,
            name="aiohttp",
            version="2.1",
            outdated=False,
            licenses=["Apache-1.0"],
            locations=[ToePackageCoordinatesFaker(path="requirements.txt")],
        ),
        ToePackageFaker(
            group_name="group1",
            root_id=root_id,
            name="requets",
            version="1.5.0",
            outdated=False,
            licenses=["Apache-1.0"],
            locations=[ToePackageCoordinatesFaker(path="requirements.txt")],
        ),
    ]
    execution_packages: list[ToePackage] = []
    (
        packages_to_add,
        packages_to_update_not_bp,
        packages_to_update_bp,
    ) = process_db_packages(execution_packages, db_toe_packages)
    assert len(packages_to_add) == 0
    assert len(packages_to_update_not_bp) == 2
    assert len(packages_to_update_bp) == 0


def test_process_db_packages_image() -> None:
    root_id = "root1"
    db_toe_packages = [
        ToePackageFaker(
            group_name="group1",
            root_id=root_id,
            name="aiohttp",
            version="2.1",
            outdated=False,
            licenses=["Apache-1.0"],
            locations=[
                ToePackageCoordinatesFaker(path="requirements.txt"),
                ToePackageCoordinatesFaker(path="/otp/python/aiohttp.dist", image_ref="image1"),
            ],
        ),
        ToePackageFaker(
            group_name="group1",
            root_id=root_id,
            name="requets",
            version="1.5.0",
            outdated=False,
            licenses=["Apache-1.0"],
            locations=[ToePackageCoordinatesFaker(path="requirements.txt")],
        ),
    ]

    # simulate image execution
    execution_packages = [
        ToePackageFaker(
            group_name="group1",
            root_id=root_id,
            name="aiohttp",
            version="2.1",
            outdated=False,
            licenses=["Apache-1.0"],
            locations=[
                ToePackageCoordinatesFaker(path="requirements.txt"),
                ToePackageCoordinatesFaker(path="/otp/python/aiohttp.dist", image_ref="image1"),
            ],
        ),
    ]
    # Simulate that an image report was made but nothing has changed
    (
        packages_to_add,
        packages_to_update_not_bp,
        packages_to_update_bp,
    ) = process_db_packages(execution_packages, db_toe_packages, image_ref="image1")
    assert len(packages_to_add) == 0
    assert len(packages_to_update_not_bp) == 0
    assert len(packages_to_update_bp) == 0


def test_process_db_packages_update_location() -> None:
    root_id = "root1"
    db_toe_packages = [
        ToePackageFaker(
            group_name="group1",
            root_id=root_id,
            name="aiohttp",
            version="2.1",
            outdated=False,
            licenses=["Apache-1.0"],
            locations=[
                ToePackageCoordinatesFaker(path="requirements.txt"),
            ],
        ),
        ToePackageFaker(
            group_name="group1",
            root_id=root_id,
            name="requets",
            version="1.5.0",
            outdated=False,
            licenses=["Apache-1.0"],
            locations=[
                ToePackageCoordinatesFaker(path="requirements.txt"),
                ToePackageCoordinatesFaker(path="requirements.txt", image_ref="image1"),
            ],
        ),
    ]

    # simulate image execution
    execution_packages = [
        ToePackageFaker(
            group_name="group1",
            root_id=root_id,
            name="aiohttp",
            version="2.1",
            outdated=False,
            licenses=["Apache-1.0"],
            locations=[
                ToePackageCoordinatesFaker(path="requirements.txt"),
                ToePackageCoordinatesFaker(path="/otp/python/aiohttp.dist", image_ref="image1"),
            ],
        ),
        ToePackageFaker(
            group_name="group1",
            root_id=root_id,
            name="aiohttp",
            version=">2.0",
            outdated=False,
            licenses=["Apache-1.0"],
            locations=[
                ToePackageCoordinatesFaker(path="requirements.txt"),
                ToePackageCoordinatesFaker(path="/otp/python/aiohttp.dist", image_ref="image1"),
            ],
        ),
    ]
    (
        packages_to_add,
        packages_to_update_not_bp,
        packages_to_update_bp,
    ) = process_db_packages(execution_packages, db_toe_packages, image_ref="image1")
    assert len(packages_to_add) == 0
    assert len(packages_to_update_not_bp) == 0
    assert len(packages_to_update_bp) == 2


def test_process_db_packages_new_package_image() -> None:
    root_id = "root1"
    db_toe_packages = [
        ToePackageFaker(
            group_name="group1",
            root_id=root_id,
            name="requets",
            version="1.5.0",
            outdated=False,
            licenses=["Apache-1.0"],
            locations=[ToePackageCoordinatesFaker(path="requirements.txt")],
        ),
    ]

    # simulate image execution
    execution_packages = [
        ToePackageFaker(
            group_name="group1",
            root_id=root_id,
            name="aiohttp",
            version="2.1",
            outdated=False,
            licenses=["Apache-1.0"],
            locations=[
                ToePackageCoordinatesFaker(path="requirements.txt"),
                ToePackageCoordinatesFaker(path="/otp/python/aiohttp.dist", image_ref="image1"),
            ],
        ),
    ]
    (
        packages_to_add,
        packages_to_update_not_bp,
        packages_to_update_bp,
    ) = process_db_packages(execution_packages, db_toe_packages, image_ref="image1")
    assert len(packages_to_add) == 1
    assert len(packages_to_update_not_bp) == 0
    assert len(packages_to_update_bp) == 0
