import re
from typing import Self

from packageurl import PackageURL
from univers.gem import InvalidRequirementError
from univers.version_range import (
    RANGE_CLASS_BY_SCHEMES,
    InvalidVersionRange,
    VersionConstraint,
    VersionRange,
)
from univers.versions import (
    AlpineLinuxVersion,
    ComposerVersion,
    ConanVersion,
    DebianVersion,
    GolangVersion,
    InvalidVersion,
    MavenVersion,
    NugetVersion,
    PypiVersion,
    RpmVersion,
    RubygemsVersion,
    SemverVersion,
    Version,
)

from integrates.db_model.toe_packages.types import ToePackage


def validate_version(version: str) -> bool:
    if "*" in version or version.endswith(".") or re.match(r"^\d+(,\s*\d+)*$", version):
        return False
    return any(char.isdigit() for char in version)


def get_version_scheme(package: ToePackage) -> Version | None:
    schemes = {
        "type": {
            "apk": AlpineLinuxVersion,
            "cocoapods": SemverVersion,
            "cargo": SemverVersion,
            "composer": ComposerVersion,
            "conan": ConanVersion,
            "deb": DebianVersion,
            "gem": RubygemsVersion,
            "golang": GolangVersion,
            "maven": MavenVersion,
            "npm": SemverVersion,
            "nuget": NugetVersion,
            "pub": SemverVersion,
            "pypi": PypiVersion,
            "rpm": RpmVersion,
            "swift": SemverVersion,
            # versions systems and ranges supported by univers
            # generic
            # apache
            # hex
            # mozilla
            # github
            # ebuild
            # alpm
            # nginx
            # openssl
            # mattermost
        },
    }

    if package.package_url and (
        scheme_from_type := schemes["type"].get(PackageURL.from_string(package.package_url).type)
    ):
        return scheme_from_type

    return None


class ApkVersionRange(VersionRange):
    scheme = "apk"
    version_class = AlpineLinuxVersion

    @classmethod
    def from_native(cls, string: str) -> Self:
        match = re.match(r"([<>=~!^]*)(.*)", string)
        if not match:
            raise ValueError(f"Invalid version range: {string}")
        comparator, version = match.groups()
        version = version.strip()
        return cls(
            constraints=[
                VersionConstraint(comparator=comparator, version=cls.version_class(version)),
            ],
        )


class PubVersionRange(VersionRange):
    """
    Version range class for pub (not supported in univers yet).

    All restrictions and conditions based on:
    https://github.com/dart-lang/pub_semver/blob/master/README.md#semantics
    """

    scheme = "pub"
    version_class = SemverVersion

    vers_by_native_comparators = {
        "<=": "<=",
        ">=": ">=",
        "<": "<",
        ">": ">",
        "=": "=",
    }

    @classmethod
    def from_native(cls, string: str) -> Self:
        constraints = []
        comparator = ""

        for constraint_item in string.split():
            if not re.match(
                r"^[<>=~!^]*\d+(\.\d+)*(-[a-zA-Z0-9]+)?(\+[a-zA-Z0-9]+)?$", constraint_item
            ):
                continue

            if constraint_item.startswith("^"):
                base_version = cls.version_class(constraint_item.lstrip("^"))

                if base_version.major > 0:
                    upper = cls.version_class(f"{base_version.major + 1}.0.0")
                else:
                    upper = cls.version_class(f"0.{base_version.minor + 1}.0")

                upper = cls.version_class(str(upper).split("-")[0].split("+")[0])

                lower = base_version

                constraints.extend(
                    [
                        VersionConstraint(comparator=">=", version=lower),
                        VersionConstraint(comparator="<", version=upper),
                    ]
                )
                continue

            comparator, version = VersionConstraint.split(constraint_item)

            constraints.append(
                VersionConstraint(comparator=comparator, version=cls.version_class(version))
            )

            comparator = ""

        return cls(constraints=constraints)


def convert_to_maven_range(constraints: list[str]) -> str:
    """
    Convert a list of version constraints to a Maven version range format.
    According to this rules:
    https://maven.apache.org/enforcer/enforcer-rules/versionRanges.html
    """
    maven_ranges = []
    maven_range_pattern = re.compile(r"^\[.*\)$")

    for constraint in constraints:
        constraint = constraint.strip()
        if not constraint or constraint.lower() in {"none", "null"}:
            continue

        if maven_range_pattern.match(constraint):
            maven_ranges.append(constraint)
            continue
        match constraint[:2]:
            case "<=":
                version = constraint[2:].strip()
                maven_ranges.append(f"(,{version}]")
            case ">=":
                version = constraint[2:].strip()
                maven_ranges.append(f"[{version},)")
            case _:
                match constraint[:1]:
                    case "<":
                        version = constraint[1:].strip()
                        maven_ranges.append(f"(,{version})")
                    case ">":
                        version = constraint[1:].strip()
                        maven_ranges.append(f"({version},)")
                    case "=":
                        version = constraint[1:].strip()
                        maven_ranges.append(f"[{version}]")
                    case _:
                        maven_ranges.append(f"[{constraint},)")

    return ",".join(maven_ranges)


def _compare_single_constraint(
    version: Version,
    constraint: str,
    scheme: str,
) -> bool:
    version_range: VersionRange | None = {
        **RANGE_CLASS_BY_SCHEMES,
        "apk": ApkVersionRange,
        "pub": PubVersionRange,
    }.get(scheme)

    if not version_range:
        raise ValueError(f"Invalid version scheme: {scheme}")
    try:
        constraint = constraint.strip()
        if scheme in {"maven", "nuget"}:
            constraint = convert_to_maven_range(constraint.split("||"))

        if not any(char.isdigit() for char in constraint):
            return False

        return version in version_range.from_native(constraint)
    except (InvalidVersion, InvalidVersionRange, TypeError, ValueError, InvalidRequirementError):
        return False


def matches_constraint(version: Version, constraint: str, version_scheme: str) -> bool:
    if not constraint:
        return True

    constraints = constraint.split("||")
    return any(
        _compare_single_constraint(version, constraint.strip(), version_scheme)
        for constraint in constraints
    )
