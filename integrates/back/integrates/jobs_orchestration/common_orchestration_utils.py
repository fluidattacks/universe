import asyncio
import tempfile
from enum import Enum

import aiofiles
import yaml

from integrates.context import (
    FI_ENVIRONMENT,
)
from integrates.custom_exceptions import (
    UnavailabilityError,
)
from integrates.db_model.roots.types import GitRoot
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.s3.operations import (
    upload_file,
)


class ConfigType(str, Enum):
    MACHINE = "machine"
    SBOM = "sbom"


@retry_on_exceptions(exceptions=(UnavailabilityError,), max_attempts=3, sleep_seconds=5)
async def upload_config_to_s3(
    root_config: dict, config_type: ConfigType = ConfigType.MACHINE
) -> None:
    yaml_data = yaml.dump(root_config)

    config_types = {
        ConfigType.MACHINE: {"path_suffix": ".yaml", "s3_prefix": "configs"},
        ConfigType.SBOM: {"path_suffix": "_config.yaml", "s3_prefix": "sbom_configs"},
    }

    config_info = config_types[config_type]
    config_path = f"{root_config['execution_id']}{config_info['path_suffix']}"

    if FI_ENVIRONMENT == "production":
        with tempfile.NamedTemporaryFile(delete=True, mode="w", encoding="utf-8") as tmpfile:
            await asyncio.to_thread(tmpfile.write, yaml_data)
            await asyncio.to_thread(tmpfile.flush)
            await upload_file(
                object_key=f"{config_info['s3_prefix']}/{config_path}",
                file_path=tmpfile.name,
                bucket="machine.data",
            )
    else:
        async with aiofiles.open(config_path, "w", encoding="utf-8") as writer:
            await writer.write(yaml_data)


def generate_job_name(
    job_type: str,
    group_name: str,
    job_scope: str,
    job_uuid: str,
) -> str:
    return f"{job_type}_{group_name}_{job_scope}_{job_uuid}"


def get_excluded_paths(git_root: GitRoot) -> list[str]:
    return list(
        exclusion
        for exclusion in sorted(
            (
                "glob(**/.git)",
                *(git_root.state.gitignore if git_root else []),
            ),
        )
        if exclusion != ""
    )
