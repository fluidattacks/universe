from integrates.jobs_orchestration.common_orchestration_utils import (
    generate_job_name,
    get_excluded_paths,
)
from integrates.jobs_orchestration.create_machine_config import (
    MachineJobOrigin,
    MachineJobTechnique,
)
from integrates.jobs_orchestration.model.types import SbomJobScope, SbomJobType
from integrates.testing.fakers import GitRootFaker, GitRootStateFaker


def test_generate_job_name_with_simple_values() -> None:
    job_name = generate_job_name(
        job_type="machine", group_name="test_group", job_scope="repo", job_uuid="123456"
    )
    assert job_name == "machine_test_group_repo_123456"


def test_generate_machine_job_name() -> None:
    group_name = "group1"
    job_origin = MachineJobOrigin.SCHEDULER.value
    job_technique = MachineJobTechnique.ALL.value
    job_uuid = "123e4567-e89b-12d3-a456-426614174000"
    job_scope = f"{job_origin}_{job_technique}"

    result = generate_job_name(
        "machine",
        group_name,
        job_scope,
        job_uuid,
    )

    expected = "machine_group1_scheduler_all_123e4567-e89b-12d3-a456-426614174000"
    assert result == expected


def test_generate_sbom_job_name() -> None:
    group_name = "group1"
    job_uuid = "123e4567-e89b-12d3-a456-426614174000"
    job_scope = f"{SbomJobType.SCHEDULER.value}_{SbomJobScope.ROOT.value}"

    result = generate_job_name(
        job_type="sbom",
        group_name=group_name,
        job_scope=job_scope,
        job_uuid=job_uuid,
    )

    expected = "sbom_group1_scheduler_root_123e4567-e89b-12d3-a456-426614174000"
    assert result == expected


def test_get_excluded_paths_with_gitignore() -> None:
    git_root = GitRootFaker(
        state=GitRootStateFaker(
            gitignore=["**/maven/**", "test/"],
        ),
    )
    result = get_excluded_paths(git_root)
    assert result == ["**/maven/**", "glob(**/.git)", "test/"]


def test_get_excluded_paths_with_empty_gitignore() -> None:
    git_root = GitRootFaker(
        state=GitRootStateFaker(gitignore=[]),
    )
    result = get_excluded_paths(git_root)
    assert result == ["glob(**/.git)"]
