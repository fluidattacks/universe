import logging
import os
import re
import tempfile
import uuid
from collections import (
    defaultdict,
)
from enum import (
    Enum,
)
from typing import (
    Any,
    NamedTuple,
)

import yaml

from integrates.batch.enums import (
    SkimsBatchQueue,
)
from integrates.class_types.types import (
    Item,
)
from integrates.context import (
    FI_ENVIRONMENT,
)
from integrates.custom_exceptions import (
    UnavailabilityError,
)
from integrates.custom_utils.datetime import (
    get_now_minus_delta,
)
from integrates.custom_utils.roots import (
    get_active_git_roots,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.roots.enums import (
    RootEnvironmentCloud,
    RootEnvironmentUrlType,
)
from integrates.db_model.roots.types import (
    GitRoot,
    RootEnvironmentUrl,
    RootEnvironmentUrlsRequest,
    Secret,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.jobs_orchestration.create_sbom_config import (
    generate_root_config as generate_sbom_root_config,
)
from integrates.jobs_orchestration.create_sbom_config import (
    upload_sbom_config_to_s3,
)
from integrates.jobs_orchestration.model.types import SbomFormat
from integrates.mailer.utils import (
    get_organization_name,
)
from integrates.organizations.utils import (
    get_organization,
)
from integrates.roots.domain import (
    get_environment_url_secrets,
)
from integrates.s3.operations import (
    upload_file,
)

logging.basicConfig(
    level=logging.INFO,
    format="[%(levelname)s] %(message)s",
)
LOGGER = logging.getLogger(__name__)


class MachineJobTechnique(str, Enum):
    ALL = "all"
    APK = "apk"
    CSPM = "cspm"
    DAST = "dast"
    SAST = "sast"


class MachineJobOrigin(str, Enum):
    API = "api"
    PLATFORM = "platform"
    POSTCLONE = "postclone"
    REATTACK = "reattack"
    SCHEDULER = "scheduler"


class MachineModulesToExecute(NamedTuple):
    APK: bool = True
    CSPM: bool = True
    DAST: bool = True
    SAST: bool = True
    SCA: bool = True

    @property
    def include_any_dynamic(self) -> bool:
        return self.APK or self.CSPM or self.DAST

    @property
    def include_any_static(self) -> bool:
        return self.SAST or self.SCA


async def get_warp_vnet_name(
    loaders: Dataloaders,
    group_name: str,
    modules_to_execute: MachineModulesToExecute,
) -> str | None:
    if not modules_to_execute.DAST:
        return None

    org_name = await get_organization_name(loaders, group_name)
    roots = await get_active_git_roots(loaders, group_name)
    roots_env_urls = [
        url
        for url in await loaders.root_environment_urls.load_many_chained(
            [RootEnvironmentUrlsRequest(root_id=root.id, group_name=group_name) for root in roots],
        )
        if url.state.include
        and url.state.url_type == RootEnvironmentUrlType.URL
        and (url.state.use_egress or url.state.use_ztna)
    ]

    if any(url for url in roots_env_urls if url.state.use_ztna):
        LOGGER.info("Found group %s that requires ztna in execution", group_name)
        return org_name
    if any(url for url in roots_env_urls if url.state.use_egress):
        LOGGER.info("Found group %s that requires egress-ips in execution", group_name)
        return "egress-ips"
    return None


def generate_machine_job_name(
    group_name: str,
    job_origin: str,
    job_technique: str,
    job_uuid: str,
) -> str:
    return f"machine_{group_name}_{job_origin}_{job_technique}_{job_uuid}"


async def get_group_data(
    loaders: Dataloaders,
    group_name: str,
) -> tuple[bool, str, list[GitRoot]] | None:
    group = await loaders.group.load(group_name)
    if not group:
        return None
    roots = await get_active_git_roots(loaders, group_name)
    is_new_group = group.created_date > get_now_minus_delta(hours=2)
    return (is_new_group, group.language.value, roots)


async def get_root_env_urls(
    loaders: Dataloaders,
    root: GitRoot,
) -> dict[str, list[RootEnvironmentUrl]]:
    env_urls = [
        env_url
        for env_url in await loaders.root_environment_urls.load(
            RootEnvironmentUrlsRequest(root_id=root.id, group_name=root.group_name),
        )
        if env_url.state.include
    ]

    root_env_urls: dict[str, list[RootEnvironmentUrl]] = {
        "dast": [],
        "cspm": [],
        "apk": [],
    }

    for environment_url in env_urls:
        if environment_url.state.url_type == RootEnvironmentUrlType.URL:
            root_env_urls["dast"].append(environment_url)
        elif environment_url.state.url_type == RootEnvironmentUrlType.CSPM:
            root_env_urls["cspm"].append(environment_url)
        elif environment_url.state.url_type == RootEnvironmentUrlType.APK:
            root_env_urls["apk"].append(environment_url)

    return root_env_urls


async def get_root_env_url_secrets(
    loaders: Dataloaders,
    env_url: RootEnvironmentUrl,
) -> dict[str, str]:
    url_secrets: list[Secret] = await get_environment_url_secrets(loaders, env_url)
    return {secret.key: secret.value for secret in url_secrets}


async def get_external_id(loaders: Dataloaders, group_name: str) -> str | None:
    group = await loaders.group.load(group_name)
    if not group:
        return None
    organization = await get_organization(loaders, group.organization_id)
    organization_aws_id = organization.state.aws_external_id

    return organization_aws_id


def get_apk_file_paths(apk_environments: set[str]) -> list[str]:
    return [
        os.path.join("fa_apks_to_analyze", env_url)
        for env_url in apk_environments
        if env_url.endswith(".apk")
    ]


async def get_cspm_config(
    loaders: Dataloaders,
    root_cspm_env_urls: list[RootEnvironmentUrl],
    group_name: str,
) -> dict[str, list]:
    cspm_config: dict[str, list[dict[str, str | None]]] = {
        "aws_credentials": [],
        "azure_credentials": [],
        "gcp_credentials": [],
    }

    azure_required_secrets = {
        "AZURE_CLIENT_ID",
        "AZURE_CLIENT_SECRET",
        "AZURE_TENANT_ID",
        "AZURE_SUBSCRIPTION_ID",
    }

    aws_required_secrets = {
        "AWS_ACCESS_KEY_ID",
        "AWS_SECRET_ACCESS_KEY",
    }

    for environment_url in root_cspm_env_urls:
        if not environment_url.state.cloud_name:
            continue

        if (
            environment_url.state.cloud_name == RootEnvironmentCloud.AWS
            and re.match(
                r"^arn:aws:iam::\d{12}:role\/[\w+=,.@-]+$",
                environment_url.url,
            )
            and (external_id := await get_external_id(loaders, group_name))
        ):
            cspm_config["aws_credentials"].append(
                {
                    "external_id": external_id,
                    "role": environment_url.url,
                },
            )
        elif environment_url.state.cloud_name in {
            RootEnvironmentCloud.AZURE,
            RootEnvironmentCloud.GCP,
            RootEnvironmentCloud.AWS,
        }:
            url_secrets = await get_root_env_url_secrets(loaders, environment_url)
            if azure_required_secrets.issubset(url_secrets.keys()):
                cspm_config["azure_credentials"].append(
                    {
                        "client_id": url_secrets["AZURE_CLIENT_ID"],
                        "client_secret": url_secrets["AZURE_CLIENT_SECRET"],
                        "tenant_id": url_secrets["AZURE_TENANT_ID"],
                        "subscription_id": url_secrets["AZURE_SUBSCRIPTION_ID"],
                    },
                )
            elif aws_required_secrets.issubset(url_secrets.keys()) and not url_secrets[
                "AWS_ACCESS_KEY_ID"
            ].startswith("ASIA"):
                cspm_config["aws_credentials"].append(
                    {
                        "access_key_id": url_secrets["AWS_ACCESS_KEY_ID"],
                        "secret_access_key": url_secrets["AWS_SECRET_ACCESS_KEY"],
                        "session_token": url_secrets.get("AWS_SESSION_TOKEN"),
                    },
                )
            elif "GCP_PRIVATE_KEY" in url_secrets:
                cspm_config["gcp_credentials"].append(
                    {
                        "private_key": url_secrets["GCP_PRIVATE_KEY"],
                    },
                )

    return cspm_config


def _get_static_techniques_config(
    *,
    git_root: GitRoot,
    included_paths: list[str],
    modules_to_execute: MachineModulesToExecute,
) -> dict[str, Any]:
    static_techniques_config: dict[str, dict[str, Any]] = defaultdict(dict)
    excluded_paths = list(
        exclusion
        for exclusion in sorted(
            (
                "glob(**/.git)",
                *(git_root.state.gitignore if git_root else []),
            ),
        )
        if exclusion != ""
    )
    if modules_to_execute.SAST:
        static_techniques_config["sast"] = {
            "include": list(included_paths),
            "exclude": list(excluded_paths),
            "recursion_limit": 1000,
        }

    if modules_to_execute.SCA:
        static_techniques_config["sca"] = {
            "include": list(included_paths),
            "exclude": list(excluded_paths),
            "use_new_sca": True,
        }

    return static_techniques_config


async def _get_dynamic_techniques_config(
    *,
    loaders: Dataloaders,
    group_name: str,
    git_root: GitRoot,
    modules_to_execute: MachineModulesToExecute,
) -> dict[str, Any]:
    dynamic_techniques_config: dict[str, dict[str, Any]] = defaultdict(dict)

    root_env_urls = await get_root_env_urls(loaders, git_root)
    if modules_to_execute.DAST and root_env_urls["dast"]:
        env_urls: set[str] = {environment_url.url for environment_url in root_env_urls["dast"]}
        dynamic_techniques_config["dast"] = {
            "urls": list(env_urls),
            "http_checks": True,
            "ssl_checks": True,
        }

    if modules_to_execute.CSPM and root_env_urls["cspm"]:
        dynamic_techniques_config["cspm"] = await get_cspm_config(
            loaders,
            root_env_urls["cspm"],
            group_name,
        )

    if modules_to_execute.APK and root_env_urls["apk"]:
        apk_urls = {environment_url.url for environment_url in root_env_urls["apk"]}

        apk_files = get_apk_file_paths(apk_urls)
        dynamic_techniques_config["apk"] = {
            "exclude": [],
            "include": list(apk_files),
        }

    return dynamic_techniques_config


async def generate_root_config(
    *,
    loaders: Dataloaders,
    group_name: str,
    is_new_group: bool,
    group_language: str,
    checks: list[str] | None,
    root: GitRoot,
    included_paths: list[str],
    batch_job_id: str,
    modules_to_execute: MachineModulesToExecute,
) -> dict[str, Any]:
    execution_id = f"{group_name}_{root.state.nickname}_{batch_job_id}"

    use_report_soon = {}
    static_techniques_config = {}
    if modules_to_execute.include_any_static:
        static_techniques_config = _get_static_techniques_config(
            git_root=root,
            included_paths=included_paths,
            modules_to_execute=modules_to_execute,
        )
        # Recently added groups should use report soon feature
        if is_new_group:
            use_report_soon = {"use_report_soon": True}

    dynamic_techniques_config = {}
    if modules_to_execute.include_any_dynamic:
        dynamic_techniques_config = await _get_dynamic_techniques_config(
            loaders=loaders,
            group_name=group_name,
            git_root=root,
            modules_to_execute=modules_to_execute,
        )

    # If you change a key in the config file template, remember to change it
    # first in skims config template. Otherwise, all batch jobs will fail
    config = {
        "language": group_language,
        "namespace": root.state.nickname,
        "output": {
            "file_path": f"execution_results/{execution_id}.sarif",
            "format": "SARIF",
        },
        "execution_id": execution_id,
        **use_report_soon,
        **static_techniques_config,
        **dynamic_techniques_config,
    }

    if checks:
        config["checks"] = list(checks)

    return config


async def generate_job_configs(
    *,
    loaders: Dataloaders,
    batch_job_id: str,
    group_name: str,
    root_nicknames: list[str],
    checks: list[str] | None = None,
    root_paths: dict[str, list[str]] | None = None,
    modules_to_execute: MachineModulesToExecute = MachineModulesToExecute(),
) -> list[tuple[str, dict]]:
    group_data = await get_group_data(loaders, group_name)
    if not group_data:
        return []

    root_nicknames_and_configs: list[tuple[str, dict]] = []
    is_new_group, language, roots = group_data

    for root_nickname in root_nicknames:
        root = next(
            (root for root in roots if root.state.nickname == root_nickname),
            None,
        )
        if not root:
            continue

        root_config = await generate_root_config(
            loaders=loaders,
            group_name=group_name,
            is_new_group=is_new_group,
            group_language=language,
            checks=checks,
            root=root,
            included_paths=root_paths.get(root_nickname, ["."]) if root_paths else ["."],
            batch_job_id=batch_job_id,
            modules_to_execute=modules_to_execute,
        )

        root_nicknames_and_configs.append((root_nickname, root_config))

    return root_nicknames_and_configs


async def generate_job_configs_sast(
    *,
    loaders: Dataloaders,
    batch_job_id: str,
    group_name: str,
    root_nicknames: list[str],
    checks: list[str] | None = None,
    root_paths: dict[str, list[str]] | None = None,
    modules_to_execute: MachineModulesToExecute = MachineModulesToExecute(),
) -> list[tuple[str, dict, dict]]:
    group_data = await get_group_data(loaders, group_name)
    if not group_data:
        return []

    root_nicknames_and_configs: list[tuple[str, dict, dict]] = []
    is_new_group, language, roots = group_data

    for root_nickname in root_nicknames:
        root = next(
            (root for root in roots if root.state.nickname == root_nickname),
            None,
        )
        if not root:
            continue

        root_config = await generate_root_config(
            loaders=loaders,
            group_name=group_name,
            is_new_group=is_new_group,
            group_language=language,
            checks=checks,
            root=root,
            included_paths=root_paths.get(root_nickname, ["."]) if root_paths else ["."],
            batch_job_id=batch_job_id,
            modules_to_execute=modules_to_execute,
        )

        root_sbom_config = await generate_sbom_root_config(
            group_name=group_name,
            git_root=root,
            batch_job_id=batch_job_id,
            sbom_format=SbomFormat.FLUID_JSON,
        )

        root_nicknames_and_configs.append((root_nickname, root_config, root_sbom_config))

    return root_nicknames_and_configs


@retry_on_exceptions(exceptions=(UnavailabilityError,), max_attempts=3, sleep_seconds=5)
async def upload_machine_config_to_s3(root_config: dict) -> None:
    yaml_data = yaml.dump(root_config)
    config_path = f"{root_config['execution_id']}.yaml"

    if FI_ENVIRONMENT == "production":
        with tempfile.NamedTemporaryFile(delete=True, mode="w", encoding="utf-8") as tmpfile:
            tmpfile.write(yaml_data)
            tmpfile.flush()
            await upload_file(
                object_key=f"configs/{config_path}",
                file_path=tmpfile.name,
                bucket="machine.data",
            )
    else:
        with open(config_path, "w", encoding="utf-8") as writer:
            writer.write(yaml_data)


async def generate_and_upload_s3_job_configs(
    *,
    loaders: Dataloaders,
    batch_job_id: str,
    group_name: str,
    root_nicknames: list[str],
    checks: list[str] | None = None,
    root_paths: dict[str, list[str]] | None = None,
    modules_to_execute: MachineModulesToExecute = MachineModulesToExecute(),
) -> list[tuple[str, str]]:
    """
    _summary_
    - Generates machine configuration files based on provided parameters.
    - Uploads them to S3 machine.data/config bucket
    - Returns a list of tuples. Each tuple has the root_nickname and
    the path to the configuration yaml file for that root.
    """
    generated_config_paths: list[tuple[str, str]] = []

    root_configs = await generate_job_configs(
        loaders=loaders,
        group_name=group_name,
        root_nicknames=root_nicknames,
        checks=checks,
        root_paths=root_paths,
        batch_job_id=batch_job_id,
        modules_to_execute=modules_to_execute,
    )

    for root_nickname, root_config in root_configs:
        try:
            await upload_machine_config_to_s3(root_config)
            generated_config_paths.append((root_nickname, f"{root_config['execution_id']}.yaml"))
        except UnavailabilityError:
            LOGGER.error("Error uploading machine root config")

    return generated_config_paths


async def generate_and_upload_s3_job_configs_sast(
    *,
    loaders: Dataloaders,
    batch_job_id: str,
    group_name: str,
    root_nicknames: list[str],
    checks: list[str] | None = None,
    root_paths: dict[str, list[str]] | None = None,
    modules_to_execute: MachineModulesToExecute = MachineModulesToExecute(),
) -> list[tuple[str, str, str]]:
    """
    _summary_
    - Generates machine configuration files based on provided parameters.
    - Uploads them to S3 machine.data/config bucket
    - Returns a list of tuples. Each tuple has the root_nickname and
    the path to the configuration yaml file for that root.
    """
    generated_config_paths: list[tuple[str, str, str]] = []

    root_configs = await generate_job_configs_sast(
        loaders=loaders,
        group_name=group_name,
        root_nicknames=root_nicknames,
        checks=checks,
        root_paths=root_paths,
        batch_job_id=batch_job_id,
        modules_to_execute=modules_to_execute,
    )

    for root_nickname, root_config, sbom_config in root_configs:
        try:
            await upload_machine_config_to_s3(root_config)
            await upload_sbom_config_to_s3(sbom_config)
            generated_config_paths.append(
                (
                    root_nickname,
                    f"{root_config['execution_id']}.yaml",
                    f"{sbom_config['execution_id']}_config.yaml",
                )
            )

        except UnavailabilityError:
            LOGGER.error("Error uploading root configs")

    return generated_config_paths


async def generate_batch_action_config(
    *,
    loaders: Dataloaders,
    group_name: str,
    root_nicknames: list[str],
    checks: list[str] | None = None,
    roots_included_paths: dict[str, list[str]] | None = None,
    modules_to_execute: MachineModulesToExecute = MachineModulesToExecute(),
    job_origin_and_technique: tuple[MachineJobOrigin, MachineJobTechnique] = (
        MachineJobOrigin.SCHEDULER,
        MachineJobTechnique.ALL,
    ),
    include_sbom: bool = False,
) -> tuple[Item, str, SkimsBatchQueue]:
    config_file_uuid = str(uuid.uuid4())
    configs_files: list[tuple[str, str]] = []
    configs_files_sbom: list[tuple[str, str, str]] = []

    if include_sbom:
        configs_files_sbom = await generate_and_upload_s3_job_configs_sast(
            loaders=loaders,
            batch_job_id=config_file_uuid,
            group_name=group_name,
            root_nicknames=root_nicknames,
            checks=checks,
            root_paths=roots_included_paths,
            modules_to_execute=modules_to_execute,
        )
    else:
        configs_files = await generate_and_upload_s3_job_configs(
            loaders=loaders,
            batch_job_id=config_file_uuid,
            group_name=group_name,
            root_nicknames=root_nicknames,
            checks=checks,
            root_paths=roots_included_paths,
            modules_to_execute=modules_to_execute,
        )

    additional_info: Item = {
        "roots": [],
        "roots_config_files": [],
    }

    if include_sbom:
        additional_info["roots_sbom_config_files"] = []

        for nickname, machine_config_file, sbom_config_file in configs_files_sbom:
            additional_info["roots"].append(nickname)
            additional_info["roots_config_files"].append(machine_config_file)
            additional_info["roots_sbom_config_files"].append(sbom_config_file)
    else:
        for nickname, config_file in configs_files:
            additional_info["roots"].append(nickname)
            additional_info["roots_config_files"].append(config_file)

    job_name = generate_machine_job_name(
        group_name,
        job_origin_and_technique[0].value,
        job_origin_and_technique[1].value,
        config_file_uuid,
    )

    batch_queue = SkimsBatchQueue.SMALL

    if vnet := await get_warp_vnet_name(loaders, group_name, modules_to_execute):
        additional_info["network_name"] = vnet

    return additional_info, job_name, batch_queue
