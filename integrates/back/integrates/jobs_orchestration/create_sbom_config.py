import logging
import re
import tempfile
import uuid
from collections.abc import (
    Iterable,
)
from typing import (
    Any,
)

import yaml

from integrates.batch.enums import (
    SkimsBatchQueue,
)
from integrates.class_types.types import (
    Item,
)
from integrates.context import (
    FI_ENVIRONMENT,
)
from integrates.custom_exceptions import (
    UnavailabilityError,
)
from integrates.custom_utils.roots import (
    get_active_git_roots,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.credentials.types import (
    AWSRoleSecret,
    Credentials,
    CredentialsRequest,
    HttpsSecret,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.db_model.roots.types import (
    GitRoot,
    GroupDockerImagesRequest,
    RootDockerImage,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.jobs_orchestration.model.types import SbomFormat, SbomJobScope, SbomJobType
from integrates.organizations.utils import (
    get_organization,
)
from integrates.s3.operations import (
    upload_file,
)

logging.basicConfig(
    level=logging.INFO,
    format="[%(levelname)s] %(message)s",
)
LOGGER = logging.getLogger(__name__)


def generate_sbom_job_name(
    *,
    group_name: str,
    job_type: SbomJobType,
    job_scope: SbomJobScope,
    job_uuid: str,
) -> str:
    return f"sbom_{group_name}_{job_type.value}_{job_scope.value}_{job_uuid}"


@retry_on_exceptions(exceptions=(UnavailabilityError,), max_attempts=3, sleep_seconds=5)
async def upload_sbom_config_to_s3(root_config: dict) -> None:
    yaml_data = yaml.dump(root_config)
    config_path = f"{root_config['execution_id']}_config.yaml"

    if FI_ENVIRONMENT == "production":
        with tempfile.NamedTemporaryFile(delete=True, mode="w", encoding="utf-8") as tmpfile:
            tmpfile.write(yaml_data)
            tmpfile.flush()
            await upload_file(
                object_key=f"sbom_configs/{config_path}",
                file_path=tmpfile.name,
                bucket="machine.data",
            )
    else:
        with open(config_path, "w", encoding="utf-8") as writer:
            writer.write(yaml_data)


async def generate_root_config(
    *,
    git_root: GitRoot,
    batch_job_id: str,
    sbom_format: SbomFormat,
    group_name: str,
) -> dict[str, Any]:
    execution_id = f"sbom_{group_name}_{git_root.state.nickname}_{batch_job_id}"
    excluded_paths = list(
        exclusion
        for exclusion in sorted(
            (
                "glob(**/.git/)",
                *(git_root.state.gitignore if git_root else []),
            ),
        )
        if exclusion != ""
    )
    config = {
        "source": git_root.state.nickname,
        "source_type": "dir",
        "execution_id": execution_id,
        "exclude": list(excluded_paths),
        "output": {
            "name": f"execution_results/{execution_id}",
            "format": sbom_format.value,
        },
    }
    return config


async def generate_image_config(
    *,
    image: RootDockerImage,
    batch_job_id: str,
    sbom_format: SbomFormat,
    group_name: str,
    credential: Credentials | None,
    external_id: str | None,
) -> dict[str, Any]:
    cleaned_uri = re.sub(r"[^a-zA-Z0-9\-_]", "_", image.uri)
    execution_id = f"sbom_{group_name}_{cleaned_uri}_{batch_job_id}"
    config = {
        "source": image.uri,
        "source_type": "docker",
        "execution_id": execution_id,
        "output": {
            "name": f"execution_results/{execution_id}",
            "format": sbom_format.value,
        },
    }
    if credential and isinstance(credential.secret, HttpsSecret):
        config.update(
            {
                "docker_credentials": {
                    "username": credential.secret.user,
                    "password": credential.secret.password,
                }
            }
        )

    elif credential and isinstance(credential.secret, AWSRoleSecret):
        config.update(
            {
                "source_type": "ecr",
                "aws_credentials": {
                    "role": credential.secret.arn,
                    "external_id": external_id,
                },
            }
        )
    return config


async def generate_job_root_configs(
    *,
    loaders: Dataloaders,
    group_name: str,
    root_nicknames: Iterable[str],
    batch_job_id: str,
    sbom_format: SbomFormat,
) -> list[tuple[str, dict]]:
    roots = await get_active_git_roots(loaders, group_name)
    if not roots:
        return []
    root_nicknames_and_configs: list[tuple[str, dict]] = []
    for root_nickname in root_nicknames:
        root = next(
            (root for root in roots if root.state.nickname == root_nickname),
            None,
        )
        if not root:
            continue
        root_config = await generate_root_config(
            group_name=group_name,
            git_root=root,
            batch_job_id=batch_job_id,
            sbom_format=sbom_format,
        )
        root_nicknames_and_configs.append((root_nickname, root_config))
    return root_nicknames_and_configs


async def generate_job_image_configs(
    *,
    loaders: Dataloaders,
    group: Group,
    uris_with_credentials: list[tuple[str, str | None]],
    batch_job_id: str,
    sbom_format: SbomFormat,
) -> list[tuple[dict, dict]]:
    uri_to_credential = {
        uri: await loaders.credentials.load(
            CredentialsRequest(credential_id, group.organization_id)
        )
        if credential_id
        else None
        for uri, credential_id in uris_with_credentials
    }
    group_images: list[RootDockerImage] = await loaders.group_docker_images.load(
        GroupDockerImagesRequest(group.name)
    )
    docker_images = [image for image in group_images if image.uri in uri_to_credential]

    organization = await get_organization(loaders, group.organization_id)
    external_id = organization.state.aws_external_id if organization else None

    if not docker_images:
        return []
    roots = await get_active_git_roots(loaders, group.name)
    if not roots:
        return []
    images_and_configs: list[tuple[dict, dict]] = []
    for docker_image in docker_images:
        root = next(
            (root for root in roots if root.id == docker_image.root_id),
            None,
        )
        if not root:
            continue
        credential = uri_to_credential[docker_image.uri]
        image_config = await generate_image_config(
            group_name=group.name,
            batch_job_id=batch_job_id,
            sbom_format=sbom_format,
            image=docker_image,
            credential=credential,
            external_id=external_id,
        )
        image_info = {
            "image_ref": docker_image.uri,
            "root_nickname": root.state.nickname,
        }
        images_and_configs.append((image_info, image_config))
    return images_and_configs


async def generate_and_upload_s3_job_root_configs(
    *,
    loaders: Dataloaders,
    batch_job_id: str,
    group_name: str,
    root_nicknames: Iterable[str],
    sbom_format: SbomFormat,
) -> list[tuple[str, str]]:
    generated_config_paths: list[tuple[str, str]] = []
    root_configs = await generate_job_root_configs(
        loaders=loaders,
        group_name=group_name,
        root_nicknames=root_nicknames,
        batch_job_id=batch_job_id,
        sbom_format=sbom_format,
    )
    for root_nickname, root_config in root_configs:
        try:
            await upload_sbom_config_to_s3(root_config)
            generated_config_paths.append(
                (root_nickname, f"{root_config['execution_id']}_config.yaml")
            )
        except UnavailabilityError:
            LOGGER.error("Error uploading SBOM root config")
    return generated_config_paths


async def generate_and_upload_s3_job_image_configs(
    *,
    loaders: Dataloaders,
    batch_job_id: str,
    group_name: str,
    uris_with_credentials: list[tuple[str, str | None]],
    sbom_format: SbomFormat,
) -> list[tuple[dict, str]]:
    generated_config_paths: list[tuple[dict, str]] = []
    group: Group | None = await loaders.group.load(group_name)
    if not group:
        return []
    image_configs = await generate_job_image_configs(
        loaders=loaders,
        group=group,
        uris_with_credentials=uris_with_credentials,
        batch_job_id=batch_job_id,
        sbom_format=sbom_format,
    )
    for image_details, image_config in image_configs:
        try:
            await upload_sbom_config_to_s3(image_config)
            generated_config_paths.append(
                (image_details, f"{image_config['execution_id']}_config.yaml")
            )
        except UnavailabilityError:
            LOGGER.error("Error uploading SBOM image config")
    return generated_config_paths


async def generate_batch_action_config_for_image(
    *,
    uris_with_credentials: list[tuple[str, str | None]],
    group_name: str,
    loaders: Dataloaders,
    sbom_format: SbomFormat,
    job_type: SbomJobType,
    notification_id_map: dict[str, str] | None = None,
) -> tuple[Item, str, SkimsBatchQueue]:
    config_file_uuid = str(uuid.uuid4())
    configs_files = await generate_and_upload_s3_job_image_configs(
        loaders=loaders,
        batch_job_id=config_file_uuid,
        group_name=group_name,
        uris_with_credentials=uris_with_credentials,
        sbom_format=sbom_format,
    )
    additional_info: Item = {
        "images": [],
        "images_config_files": [],
        "sbom_format": sbom_format.value,
        "job_type": job_type.value,
        "notification_id_map": notification_id_map,
    }
    for image_info, image_config in configs_files:
        additional_info["images"].append(image_info)
        additional_info["images_config_files"].append(image_config)
    job_name = generate_sbom_job_name(
        group_name=group_name,
        job_type=job_type,
        job_scope=SbomJobScope.DOCKER_IMAGE,
        job_uuid=config_file_uuid,
    )
    batch_queue = SkimsBatchQueue.SMALL
    return additional_info, job_name, batch_queue


async def generate_batch_action_config_for_root(
    *,
    loaders: Dataloaders,
    group_name: str,
    root_nicknames: Iterable[str],
    sbom_format: SbomFormat,
    job_type: SbomJobType,
    notification_id_map: dict[str, str] | None = None,
) -> tuple[Item, str, SkimsBatchQueue]:
    config_file_uuid = str(uuid.uuid4())
    configs_files = await generate_and_upload_s3_job_root_configs(
        loaders=loaders,
        batch_job_id=config_file_uuid,
        group_name=group_name,
        root_nicknames=root_nicknames,
        sbom_format=sbom_format,
    )
    additional_info: Item = {
        "root_nicknames": [],
        "roots_config_files": [],
        "sbom_format": sbom_format.value,
        "job_type": job_type.value,
        "notification_id_map": notification_id_map,
    }
    for nickname, config_file in configs_files:
        additional_info["root_nicknames"].append(nickname)
        additional_info["roots_config_files"].append(config_file)
    job_name = generate_sbom_job_name(
        group_name=group_name,
        job_type=job_type,
        job_scope=SbomJobScope.ROOT,
        job_uuid=config_file_uuid,
    )
    batch_queue = SkimsBatchQueue.SMALL
    return additional_info, job_name, batch_queue
