from datetime import datetime

from integrates import batch, jobs_orchestration, roots
from integrates.batch.dal.get import get_actions_by_name
from integrates.batch.enums import Action, SkimsBatchQueue
from integrates.batch.types import BatchProcessing
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.credentials.types import HttpsSecret
from integrates.db_model.enums import Source
from integrates.db_model.groups.enums import GroupService
from integrates.db_model.roots.enums import RootCloningStatus
from integrates.db_model.roots.types import GitRoot
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
    VulnerabilityTechnique,
    VulnerabilityType,
)
from integrates.db_model.vulnerabilities.types import Vulnerability, VulnerabilityState
from integrates.jobs_orchestration.create_machine_config import MachineModulesToExecute
from integrates.jobs_orchestration.jobs import (
    PostCloningConfig,
    generate_post_cloning_config,
    get_finding_code_from_title,
    get_techniques_to_execute,
    has_pending_executions,
    queue_machine_reattack,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    BatchProcessingFaker,
    CredentialsFaker,
    GitRootCloningFaker,
    GitRootFaker,
    GitRootMachineFaker,
    GitRootStateFaker,
    GroupFaker,
    GroupStateFaker,
    OrganizationFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import parametrize

MACHINE_EMAIL = "machine@fluidattacks.com"


@parametrize(
    args=["title", "finding_code"],
    cases=[
        ["416. XAML injection", "F416"],
        ["999. Non existent title", None],
    ],
)
def test_get_finding_code_from_title(title: str, finding_code: str | None) -> None:
    assert get_finding_code_from_title(title) == finding_code


@parametrize(
    args=["group_name", "root_nicknames", "expected"],
    cases=[
        ["group1", {"root1"}, True],
        ["group1", {"root1", "root4"}, True],
        ["group1", {"root1", "root9"}, True],
        ["group2", {"root10"}, False],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(name="group1", organization_id="org1"),
            ],
            actions=[
                BatchProcessingFaker(
                    action_name=Action.EXECUTE_MACHINE,
                    entity="group1",
                    subject="admin@fluidattacks.com",
                    time=datetime.now(),
                    additional_info={
                        "roots": ["root1", "root2", "root3"],
                        "roots_config_files": [
                            "group1_root1_123456a.yaml",
                            "group1_root2_123456b.yaml",
                            "group1_root3_123456c.yaml",
                        ],
                    },
                    batch_job_id=None,
                    queue=SkimsBatchQueue.SMALL,
                    key="key1",
                ),
                BatchProcessingFaker(
                    action_name=Action.EXECUTE_MACHINE,
                    entity="group1",
                    subject="integrates@fluidattacks.com",
                    time=datetime.now(),
                    additional_info={
                        "roots": ["root4"],
                        "roots_config_files": [
                            "group1_root4_123456c.yaml",
                        ],
                    },
                    batch_job_id=None,
                    queue=SkimsBatchQueue.SMALL,
                    key="key2",
                ),
            ],
        ),
    )
)
async def test_has_pending_executions(
    group_name: str,
    root_nicknames: set[str],
    expected: bool,
) -> None:
    result = await has_pending_executions(
        group_name=group_name,
        root_nicknames=root_nicknames,
    )
    assert result == expected


@parametrize(
    args=["vulns", "expected"],
    cases=[
        [
            [
                Vulnerability(
                    created_by="machine@fluidattacks.com",
                    created_date=datetime.now(),
                    finding_id="finding_id",
                    group_name="acme",
                    hacker_email="machine@fluidattacks.com",
                    id="vuln_id",
                    organization_name="org_name",
                    root_id="root_id",
                    state=VulnerabilityState(
                        modified_by="machine@fluidattacks.com",
                        modified_date=datetime.now(),
                        source=Source.MACHINE,
                        specific="14",
                        status=VulnerabilityStateStatus.VULNERABLE,
                        where="path/to/file.rs",
                    ),
                    type=VulnerabilityType.LINES,
                ),
            ],
            MachineModulesToExecute(
                APK=False,
                CSPM=False,
                DAST=False,
                SAST=True,
                SCA=True,
            ),
        ],
        [
            [
                Vulnerability(
                    created_by="machine@fluidattacks.com",
                    created_date=datetime.now(),
                    finding_id="finding_id",
                    group_name="acme",
                    hacker_email="machine@fluidattacks.com",
                    id="vuln_id",
                    organization_name="org_name",
                    root_id="root_id",
                    state=VulnerabilityState(
                        modified_by="machine@fluidattacks.com",
                        modified_date=datetime.now(),
                        source=Source.MACHINE,
                        specific="unknown",
                        status=VulnerabilityStateStatus.VULNERABLE,
                        where="acme.apk",
                    ),
                    type=VulnerabilityType.INPUTS,
                ),
            ],
            MachineModulesToExecute(
                APK=True,
                CSPM=False,
                DAST=False,
                SAST=False,
                SCA=False,
            ),
        ],
        [
            [
                Vulnerability(
                    created_by="machine@fluidattacks.com",
                    created_date=datetime.now(),
                    finding_id="finding_id",
                    group_name="acme",
                    hacker_email="machine@fluidattacks.com",
                    id="vuln_id",
                    organization_name="org_name",
                    root_id="root_id",
                    state=VulnerabilityState(
                        modified_by="machine@fluidattacks.com",
                        modified_date=datetime.now(),
                        source=Source.MACHINE,
                        specific="search",
                        status=VulnerabilityStateStatus.VULNERABLE,
                        where="https://www.acme.com",
                    ),
                    type=VulnerabilityType.INPUTS,
                    technique=VulnerabilityTechnique.DAST,
                ),
            ],
            MachineModulesToExecute(
                APK=False,
                CSPM=False,
                DAST=True,
                SAST=False,
                SCA=False,
            ),
        ],
        [
            [
                Vulnerability(
                    created_by="machine@fluidattacks.com",
                    created_date=datetime.now(),
                    finding_id="finding_id",
                    group_name="acme",
                    hacker_email="machine@fluidattacks.com",
                    id="vuln_id",
                    organization_name="org_name",
                    root_id="root_id",
                    state=VulnerabilityState(
                        modified_by="machine@fluidattacks.com",
                        modified_date=datetime.now(),
                        source=Source.MACHINE,
                        specific="/unknown",
                        status=VulnerabilityStateStatus.VULNERABLE,
                        where=(
                            "arn:aws:elasticloadbalancing:us-east-1:"
                            "235620186666:listener/app/k8s"
                        ),
                    ),
                    type=VulnerabilityType.INPUTS,
                    technique=VulnerabilityTechnique.CSPM,
                ),
            ],
            MachineModulesToExecute(
                APK=False,
                CSPM=True,
                DAST=False,
                SAST=False,
                SCA=False,
            ),
        ],
        [
            [
                Vulnerability(
                    created_by="machine@fluidattacks.com",
                    created_date=datetime.now(),
                    finding_id="finding_id",
                    group_name="acme",
                    hacker_email="machine@fluidattacks.com",
                    id="vuln_id",
                    organization_name="org_name",
                    root_id="root_id",
                    state=VulnerabilityState(
                        modified_by="machine@fluidattacks.com",
                        modified_date=datetime.now(),
                        source=Source.MACHINE,
                        specific="14",
                        status=VulnerabilityStateStatus.VULNERABLE,
                        where="path/to/file.rs",
                    ),
                    type=VulnerabilityType.LINES,
                ),
                Vulnerability(
                    created_by="machine@fluidattacks.com",
                    created_date=datetime.now(),
                    finding_id="finding_id",
                    group_name="acme",
                    hacker_email="machine@fluidattacks.com",
                    id="vuln_id",
                    organization_name="org_name",
                    root_id="root_id",
                    state=VulnerabilityState(
                        modified_by="machine@fluidattacks.com",
                        modified_date=datetime.now(),
                        source=Source.MACHINE,
                        specific="unknown",
                        status=VulnerabilityStateStatus.VULNERABLE,
                        where="acme.apk",
                    ),
                    type=VulnerabilityType.INPUTS,
                ),
                Vulnerability(
                    created_by="machine@fluidattacks.com",
                    created_date=datetime.now(),
                    finding_id="finding_id",
                    group_name="acme",
                    hacker_email="machine@fluidattacks.com",
                    id="vuln_id",
                    organization_name="org_name",
                    root_id="root_id",
                    state=VulnerabilityState(
                        modified_by="machine@fluidattacks.com",
                        modified_date=datetime.now(),
                        source=Source.MACHINE,
                        specific="search",
                        status=VulnerabilityStateStatus.VULNERABLE,
                        where="https://www.acme.com",
                    ),
                    type=VulnerabilityType.INPUTS,
                    technique=VulnerabilityTechnique.DAST,
                ),
                Vulnerability(
                    created_by="machine@fluidattacks.com",
                    created_date=datetime.now(),
                    finding_id="finding_id",
                    group_name="acme",
                    hacker_email="machine@fluidattacks.com",
                    id="vuln_id",
                    organization_name="org_name",
                    root_id="root_id",
                    state=VulnerabilityState(
                        modified_by="machine@fluidattacks.com",
                        modified_date=datetime.now(),
                        source=Source.MACHINE,
                        specific="/unknown",
                        status=VulnerabilityStateStatus.VULNERABLE,
                        where=(
                            "arn:aws:elasticloadbalancing:us-east-1:"
                            "235620186666:listener/app/k8s"
                        ),
                    ),
                    type=VulnerabilityType.INPUTS,
                    technique=VulnerabilityTechnique.CSPM,
                ),
            ],
            MachineModulesToExecute(
                APK=True,
                CSPM=True,
                DAST=True,
                SAST=True,
                SCA=True,
            ),
        ],
    ],
)
def test_get_techniques_to_execute(
    vulns: list[Vulnerability],
    expected: MachineModulesToExecute,
) -> None:
    assert get_techniques_to_execute(vulns) == expected


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(
                    name="group1",
                    organization_id="org1",
                    state=GroupStateFaker(service=GroupService.WHITE),
                ),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    cloning=GitRootCloningFaker(status=RootCloningStatus.OK),
                    state=GitRootStateFaker(nickname="root1", branch="test1"),
                    machine=GitRootMachineFaker(),
                ),
            ],
        ),
    ),
    others=[
        Mock(
            jobs_orchestration.jobs,
            "generate_and_upload_s3_job_configs_sast",
            "async",
            [
                ("root1", "root1.yaml", "sbom_root1_config.yaml"),
            ],
        ),
        Mock(jobs_orchestration.jobs, "git_is_commit_in_branch", "async", True),
        Mock(jobs_orchestration.jobs, "git_get_modified_filenames", "async", ["test.py", "try.py"]),
    ],
)
async def test_generate_post_cloning_config() -> None:
    loaders: Dataloaders = get_new_context()
    roots = [root for root in await loaders.group_roots.load("group1") if isinstance(root, GitRoot)]
    assert len(roots) == 1

    result = await generate_post_cloning_config(
        loaders=loaders,
        root=roots[0],
        repo_working_dir=".",
        config_uuid="123",
    )

    assert result == PostCloningConfig(roots[0], "root1.yaml", "sbom_root1_config.yaml")


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(
                    name="group1",
                    organization_id="org1",
                    state=GroupStateFaker(service=GroupService.WHITE),
                ),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    cloning=GitRootCloningFaker(status=RootCloningStatus.OK),
                    state=GitRootStateFaker(nickname="root1", branch="test1"),
                    machine=GitRootMachineFaker(),
                ),
            ],
        ),
    ),
    others=[
        Mock(
            jobs_orchestration.jobs,
            "generate_batch_action_config",
            "async",
            (
                {
                    "roots": ["root1"],
                    "roots_config_files": ["root1.yaml"],
                },
                "123456",
                SkimsBatchQueue.SMALL,
            ),
        ),
        Mock(batch.dal.put, "put_action_to_batch", "async", "123456"),
    ],
)
async def test_machine_queue_reattack() -> None:
    """Testing machine queue reattack"""
    loaders: Dataloaders = get_new_context()
    job_roots = [
        root for root in await loaders.group_roots.load("group1") if isinstance(root, GitRoot)
    ]
    assert len(job_roots) == 1

    await queue_machine_reattack(
        loaders=loaders,
        finding_codes=["F001"],
        group_name="group1",
        modified_by="machine@fluidattacks.com",
        modules_to_execute=MachineModulesToExecute(
            APK=False,
            CSPM=False,
            DAST=False,
            SAST=False,
            SCA=True,
        ),
        reported_paths_by_root={"root1": ["test.py", "package.json"]},
        clone_before=False,
    )

    pending_executions: list[BatchProcessing] = await get_actions_by_name(
        action_name=Action.EXECUTE_MACHINE,
        entity="group1",
    )
    assert len(pending_executions) == 1


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(
                    name="group1",
                    organization_id="org1",
                    state=GroupStateFaker(service=GroupService.WHITE),
                ),
            ],
            credentials=[
                CredentialsFaker(
                    credential_id="cred1",
                    organization_id="org1",
                    secret=HttpsSecret(user="user@org.com", password="pass"),  # noqa: S106
                )
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    cloning=GitRootCloningFaker(status=RootCloningStatus.OK),
                    state=GitRootStateFaker(nickname="root1", branch="test1"),
                    machine=GitRootMachineFaker(),
                ),
            ],
        ),
    ),
    others=[
        Mock(
            jobs_orchestration.jobs,
            "generate_batch_action_config",
            "async",
            (
                {
                    "roots": ["root1"],
                    "roots_config_files": ["root1.yaml"],
                    "roots_sbom_config_files": ["sbom_root1_config.yaml"],
                },
                "123456",
                SkimsBatchQueue.SMALL,
            ),
        ),
        Mock(batch.dal.put, "put_action_to_batch", "async", "123456"),
        Mock(
            roots.filter,
            "filter_roots_working_creds",
            "async",
            [
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    cloning=GitRootCloningFaker(status=RootCloningStatus.OK),
                    state=GitRootStateFaker(nickname="root1", branch="test1"),
                    machine=GitRootMachineFaker(),
                )
            ],
        ),
    ],
)
async def test_machine_queue_reattack_with_clone() -> None:
    """Testing machine queue reattack with clone job"""
    loaders: Dataloaders = get_new_context()
    job_roots = tuple(
        root for root in await loaders.group_roots.load("group1") if isinstance(root, GitRoot)
    )
    assert len(job_roots) == 1

    await queue_machine_reattack(
        loaders=loaders,
        finding_codes=["F001"],
        group_name="group1",
        modified_by="machine@fluidattacks.com",
        modules_to_execute=MachineModulesToExecute(
            APK=False,
            CSPM=False,
            DAST=False,
            SAST=True,
            SCA=True,
        ),
        reported_paths_by_root={"root1": ["test.py", "package.json"]},
    )

    pending_executions: list[BatchProcessing] = await get_actions_by_name(
        action_name=Action.CLONE_ROOTS,
        entity="group1",
    )
    assert len(pending_executions) == 1
    clone_execution = pending_executions[0]
    assert clone_execution.dependent_actions is not None
    assert len(clone_execution.dependent_actions) == 1
    assert clone_execution.dependent_actions[0].action_name == Action.EXECUTE_MACHINE_SAST
    assert clone_execution.dependent_actions[0].additional_info == {
        "roots": ["root1"],
        "roots_config_files": ["root1.yaml"],
        "roots_sbom_config_files": ["sbom_root1_config.yaml"],
    }
