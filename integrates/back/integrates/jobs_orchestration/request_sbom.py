import logging
import logging.config

from integrates.batch.types import PutActionResult
from integrates.custom_exceptions import (
    SbomFileNotRequested,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.jobs_orchestration.jobs import (
    queue_sbom_image_job,
    queue_sbom_job,
)
from integrates.jobs_orchestration.model.types import SbomFormat, SbomJobType
from integrates.notifications.reports import add_report_notification
from integrates.settings.logger import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)


def get_sbom_format(sbom_format: str, file_format: str) -> SbomFormat:
    match sbom_format, file_format:
        case "cyclone_dx", "json":
            return SbomFormat.CYCLONEDX_JSON
        case "cyclone_dx", "xml":
            return SbomFormat.CYCLONEDX_XML
        case "spdx", "xml":
            return SbomFormat.SPDX_XML
        case "spdx", "json":
            return SbomFormat.SPDX_JSON
        case "fluid", "json":
            return SbomFormat.FLUID_JSON
        case _:
            raise ValueError(f"Invalid format {sbom_format}, {file_format}")


async def create_notification(report_name: str, user_email: str) -> str:
    notification = await add_report_notification(
        report_format="DATA",
        report_name=report_name,
        user_email=user_email,
    )
    return notification.id


async def generate_notification_map(
    items: list[str], group_name: str, user_email: str
) -> dict[str, str]:
    return {
        item: await create_notification(f"SBOM {item} ({group_name})", user_email) for item in items
    }


async def process_root_nicknames(
    root_nicknames: list[str],
    group_name: str,
    user_email: str,
    loaders: Dataloaders,
    sbom_format: SbomFormat,
) -> bool:
    notification_map = await generate_notification_map(root_nicknames, group_name, user_email)

    action_result: PutActionResult = await queue_sbom_job(
        loaders=loaders,
        group_name=group_name,
        root_nicknames=root_nicknames,
        sbom_format=sbom_format,
        modified_by=user_email,
        notification_id_map=notification_map,
        job_type=SbomJobType.REQUEST,
    )

    return action_result.success


async def process_docker_images(
    uris_with_credentials: list[dict[str, str | None]],
    group_name: str,
    user_email: str,
    loaders: Dataloaders,
    sbom_format: SbomFormat,
) -> bool:
    valid_uris = [
        (uri_data["uri"], uri_data.get("credential_id"))
        for uri_data in uris_with_credentials
        if uri_data["uri"]
    ]

    notification_map = await generate_notification_map(
        [uri for uri, _ in valid_uris],
        group_name,
        user_email,
    )

    action_result: PutActionResult = await queue_sbom_image_job(
        loaders=loaders,
        group_name=group_name,
        uris_with_credentials=valid_uris,
        sbom_format=sbom_format,
        modified_by=user_email,
        notification_id_map=notification_map,
        job_type=SbomJobType.REQUEST,
    )

    return action_result.success


async def request_sbom_file(
    *,
    loaders: Dataloaders,
    user_email: str,
    group_name: str,
    root_nicknames: list[str] | None,
    uris_with_credentials: list[dict[str, str | None]] | None,
    file_format: str,
    sbom_format_str: str,
) -> None:
    if not root_nicknames and not uris_with_credentials:
        raise ValueError("Either 'root_nicknames' or 'uris_with_credentials' must be provided.")

    roots_success = docker_success = True
    sbom_format = get_sbom_format(sbom_format_str, file_format)
    if root_nicknames:
        roots_success = await process_root_nicknames(
            root_nicknames, group_name, user_email, loaders, sbom_format
        )
    if uris_with_credentials:
        docker_success = await process_docker_images(
            uris_with_credentials, group_name, user_email, loaders, sbom_format
        )

    failed_processes = [
        process
        for process, success in [
            ("roots sbom job", roots_success),
            ("docker images sbom job", docker_success),
        ]
        if not success
    ]

    if failed_processes:
        error_message = f"Failed to queue the following: {', '.join(failed_processes)}"
        LOGGER.error(
            error_message,
            extra={
                "group_name": group_name,
                "root_nicknames": root_nicknames,
                "uris_with_credentials": uris_with_credentials,
            },
        )
        raise SbomFileNotRequested(error_message)
