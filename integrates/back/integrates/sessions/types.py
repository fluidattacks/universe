from typing import (
    NamedTuple,
    NotRequired,
    TypedDict,
)


class UserAccessInfo(NamedTuple):
    first_name: str
    last_name: str
    subject: str
    user_email: str
    verified: bool


class UserInfo(TypedDict):
    iss: NotRequired[str]
    azp: NotRequired[str]
    aud: NotRequired[str]
    sub: NotRequired[str]
    hd: NotRequired[str]
    email: str
    email_verified: NotRequired[bool]
    at_hash: NotRequired[str]
    nonce: NotRequired[str]
    name: NotRequired[str]
    picture: NotRequired[str]
    given_name: str
    family_name: NotRequired[str]
    locale: NotRequired[str]
    iat: NotRequired[int]
    exp: NotRequired[int]


class UserAuthResponse(TypedDict):
    access_token: NotRequired[str]
    expires_in: NotRequired[int]
    scope: NotRequired[str]
    token_type: NotRequired[str]
    id_token: NotRequired[str]
    expires_at: NotRequired[int]
    userinfo: UserInfo
