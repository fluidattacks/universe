from collections.abc import (
    Iterable,
)

from aiodataloader import (
    DataLoader,
)
from aioextensions import (
    collect,
)
from boto3.dynamodb.conditions import (
    Key,
)

from integrates.custom_exceptions import (
    InvalidBePresentFilterCursor,
)
from integrates.db_model import (
    HISTORIC_TABLE,
    TABLE,
)
from integrates.db_model.historic_items import ToePortStateItem
from integrates.db_model.items import ToePortItem
from integrates.db_model.toe_ports.constants import (
    GSI_2_FACET,
    GSI_2_HISTORIC_STATE_FACET,
)
from integrates.db_model.toe_ports.types import (
    GroupToePortsRequest,
    GroupToePortStatesRequest,
    RootToePortsRequest,
    ToePort,
    ToePortRequest,
    ToePortsConnection,
    ToePortState,
    ToePortStatesConnection,
)
from integrates.db_model.toe_ports.utils import (
    format_state,
    format_toe_port,
    format_toe_port_edge,
    format_toe_port_state_edge,
)
from integrates.dynamodb import (
    conditions,
    keys,
    operations,
)
from integrates.dynamodb.exceptions import (
    ValidationException,
)
from integrates.dynamodb.types import (
    PageInfo,
)


async def _get_toe_ports(
    requests: Iterable[ToePortRequest],
) -> list[ToePort | None]:
    primary_keys = tuple(
        keys.build_key(
            facet=TABLE.facets["toe_port_metadata"],
            values={
                "address": request.address,
                "port": request.port,
                "group_name": request.group_name,
                "root_id": request.root_id,
            },
        )
        for request in requests
    )
    items = await operations.DynamoClient[ToePortItem].batch_get_item(
        keys=primary_keys, table=TABLE
    )

    response = {
        ToePortRequest(
            address=toe_port.address,
            group_name=toe_port.group_name,
            port=toe_port.port,
            root_id=toe_port.root_id,
        ): toe_port
        for toe_port in [format_toe_port(item) for item in items]
    }

    return list(response.get(request) for request in requests)


class ToePortLoader(DataLoader[ToePortRequest, ToePort | None]):
    async def batch_load_fn(self, requests: Iterable[ToePortRequest]) -> list[ToePort | None]:
        return await _get_toe_ports(requests)


async def _get_historic_state(
    request: ToePortRequest,
) -> list[ToePortState]:
    primary_key = keys.build_key(
        facet=HISTORIC_TABLE.facets["toe_port_state"],
        values={
            "address": request.address,
            "port": request.port,
            "group_name": request.group_name,
            "root_id": request.root_id,
            "state": "state",
        },
    )
    key_structure = HISTORIC_TABLE.primary_key
    response = await operations.DynamoClient[ToePortStateItem].query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(HISTORIC_TABLE.facets["toe_port_state"],),
        table=HISTORIC_TABLE,
    )

    return list(map(format_state, response.items))


class ToePortHistoricStateLoader(DataLoader[ToePortRequest, list[ToePortState]]):
    async def batch_load_fn(self, requests: Iterable[ToePortRequest]) -> list[list[ToePortState]]:
        return list(
            await collect(
                tuple(_get_historic_state(request) for request in requests),
                workers=32,
            ),
        )


async def _get_toe_port_states_by_group(
    request: GroupToePortStatesRequest,
) -> ToePortStatesConnection:
    primary_key = keys.build_key(
        facet=GSI_2_HISTORIC_STATE_FACET,
        values={"group_name": request.group_name, "state": "state"},
    )
    index = HISTORIC_TABLE.indexes["gsi_2"]
    key_structure = index.primary_key
    try:
        response = await operations.DynamoClient[ToePortStateItem].query(
            after=request.after,
            condition_expression=(
                Key(key_structure.partition_key).eq(primary_key.partition_key)
                & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
            )
            if not request.start_date or not request.end_date
            else (
                Key(key_structure.partition_key).eq(primary_key.partition_key)
                & Key(key_structure.sort_key).between(
                    primary_key.sort_key + "#" + request.start_date.strftime("%Y-%m-%d"),
                    primary_key.sort_key + "#" + request.end_date.strftime("%Y-%m-%d"),
                )
            ),
            facets=(GSI_2_HISTORIC_STATE_FACET,),
            index=index,
            limit=request.first,
            paginate=request.paginate,
            table=HISTORIC_TABLE,
        )
        connection = ToePortStatesConnection(
            edges=tuple(
                format_toe_port_state_edge(index, item, HISTORIC_TABLE) for item in response.items
            ),
            page_info=response.page_info,
        )
    except ValidationException:
        connection = ToePortStatesConnection(
            edges=tuple(),
            page_info=PageInfo(has_next_page=False, end_cursor=""),
        )
    return connection


class GroupToePortStatesLoader(DataLoader[GroupToePortStatesRequest, ToePortStatesConnection]):
    async def batch_load_fn(
        self,
        requests: Iterable[GroupToePortStatesRequest],
    ) -> list[ToePortStatesConnection]:
        return list(
            await collect(
                tuple(map(_get_toe_port_states_by_group, requests)),
                workers=32,
            ),
        )

    async def load_nodes(self, request: GroupToePortStatesRequest) -> list[ToePortState]:
        connection = await self.load(request)
        return list(edge.node for edge in connection.edges)


async def _get_toe_ports_by_group(
    request: GroupToePortsRequest,
) -> ToePortsConnection:
    if request.be_present is None:
        facet = TABLE.facets["toe_port_metadata"]
        primary_key = keys.build_key(
            facet=facet,
            values={"group_name": request.group_name},
        )
        index = None
        key_structure = TABLE.primary_key
    else:
        facet = GSI_2_FACET
        primary_key = keys.build_key(
            facet=facet,
            values={
                "group_name": request.group_name,
                "be_present": str(request.be_present).lower(),
            },
        )
        index = TABLE.indexes["gsi_2"]
        key_structure = index.primary_key

    try:
        response = await operations.DynamoClient[ToePortItem].query(
            after=request.after,
            condition_expression=(
                Key(key_structure.partition_key).eq(primary_key.partition_key)
                & Key(key_structure.sort_key).begins_with(
                    primary_key.sort_key.replace("#ROOT#ADDRESS#PORT", ""),
                )
            ),
            facets=(TABLE.facets["toe_port_metadata"],),
            filter_expression=conditions.get_filter_expression(request.filters),
            index=index,
            limit=request.first,
            paginate=request.paginate,
            table=TABLE,
        )
    except ValidationException as exc:
        raise InvalidBePresentFilterCursor() from exc
    return ToePortsConnection(
        edges=tuple(format_toe_port_edge(index, item, TABLE) for item in response.items),
        page_info=response.page_info,
    )


class GroupToePortsLoader(DataLoader[GroupToePortsRequest, ToePortsConnection]):
    async def batch_load_fn(
        self,
        requests: Iterable[GroupToePortsRequest],
    ) -> list[ToePortsConnection]:
        return list(
            await collect(
                tuple(map(_get_toe_ports_by_group, requests)),
                workers=32,
            ),
        )

    async def load_nodes(self, request: GroupToePortsRequest) -> list[ToePort]:
        connection = await self.load(request)
        return list(edge.node for edge in connection.edges)


async def _get_toe_ports_by_root(
    request: RootToePortsRequest,
) -> ToePortsConnection:
    if request.be_present is None:
        facet = TABLE.facets["toe_port_metadata"]
        primary_key = keys.build_key(
            facet=facet,
            values={
                "group_name": request.group_name,
                "root_id": request.root_id,
            },
        )
        index = None
        key_structure = TABLE.primary_key
    else:
        facet = GSI_2_FACET
        primary_key = keys.build_key(
            facet=facet,
            values={
                "group_name": request.group_name,
                "be_present": str(request.be_present).lower(),
                "root_id": request.root_id,
            },
        )
        index = TABLE.indexes["gsi_2"]
        key_structure = index.primary_key
    try:
        response = await operations.DynamoClient[ToePortItem].query(
            after=request.after,
            condition_expression=(
                Key(key_structure.partition_key).eq(primary_key.partition_key)
                & Key(key_structure.sort_key).begins_with(primary_key.sort_key.replace("#PORT", ""))
            ),
            facets=(TABLE.facets["toe_port_metadata"],),
            index=index,
            limit=request.first,
            paginate=request.paginate,
            table=TABLE,
        )
        connection = ToePortsConnection(
            edges=tuple(format_toe_port_edge(index, item, TABLE) for item in response.items),
            page_info=response.page_info,
        )
    except ValidationException:
        connection = ToePortsConnection(
            edges=tuple(),
            page_info=PageInfo(has_next_page=False, end_cursor=""),
        )

    return connection


class RootToePortsLoader(DataLoader[RootToePortsRequest, ToePortsConnection]):
    async def batch_load_fn(
        self,
        requests: Iterable[RootToePortsRequest],
    ) -> list[ToePortsConnection]:
        return list(
            await collect(
                tuple(map(_get_toe_ports_by_root, requests)),
                workers=32,
            ),
        )

    async def load_nodes(self, request: RootToePortsRequest) -> list[ToePort]:
        connection = await self.load(request)
        return [edge.node for edge in connection.edges]
