from integrates.db_model import (
    HISTORIC_TABLE,
    TABLE,
)
from integrates.dynamodb.types import (
    Facet,
)

GSI_2_FACET = Facet(
    attrs=TABLE.facets["toe_port_metadata"].attrs,
    pk_alias="GROUP#group_name",
    sk_alias="PORTS#PRESENT#be_present#ROOT#root_id#ADDRESS#address#PORT#port",
)

GSI_2_HISTORIC_STATE_FACET = Facet(
    attrs=HISTORIC_TABLE.facets["toe_port_state"].attrs,
    pk_alias="GROUP#PORTS#ROOT#ADDRESS#PORT#group_name",
    sk_alias="STATE#state#DATE#iso8601utc",
)
