from integrates.dataloaders import get_new_context
from integrates.db_model import toe_ports as toe_ports_model
from integrates.db_model.toe_ports.types import GroupToePortsRequest, ToePortRequest
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GroupFaker,
    IPRootFaker,
    IPRootStateFaker,
    OrganizationFaker,
    ToePortFaker,
    ToePortStateFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize

GROUP_NAME = "group1"
ROOT_ID = "root1"


@parametrize(
    args=["address", "port", "root_id"],
    cases=[
        ["127.0.0.0", "8080", ROOT_ID],
        ["127.0.0.0", "8081", "root2"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            roots=[
                IPRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=IPRootStateFaker(
                        nickname="back",
                    ),
                ),
                IPRootFaker(
                    group_name=GROUP_NAME,
                    id="root2",
                    state=IPRootStateFaker(
                        nickname="port",
                    ),
                ),
            ],
            toe_ports=[
                ToePortFaker(
                    address="127.0.0.0",
                    port="8080",
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=ToePortStateFaker(be_present=True),
                ),
                ToePortFaker(
                    address="127.0.0.0",
                    port="8081",
                    group_name=GROUP_NAME,
                    root_id="root2",
                    state=ToePortStateFaker(be_present=True),
                ),
            ],
        ),
    )
)
async def test_remove_toe_port(
    address: str,
    port: str,
    root_id: str,
) -> None:
    loaders = get_new_context()
    request = ToePortRequest(group_name=GROUP_NAME, address=address, port=port, root_id=root_id)
    toe_port = await loaders.toe_port.load(request)
    assert toe_port
    assert toe_port.address == address
    assert toe_port.port == port
    await toe_ports_model.remove(group_name=GROUP_NAME, address=address, port=port, root_id=root_id)
    toe_port = await get_new_context().toe_port.load(request)
    assert not toe_port


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            roots=[
                IPRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=IPRootStateFaker(
                        nickname="back",
                    ),
                ),
            ],
            toe_ports=[
                ToePortFaker(
                    address="127.0.0.0",
                    port="8080",
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=ToePortStateFaker(be_present=True),
                ),
                ToePortFaker(
                    address="127.0.0.0",
                    port="80",
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=ToePortStateFaker(be_present=True),
                ),
                ToePortFaker(
                    address="127.0.0.1",
                    port="4300",
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=ToePortStateFaker(be_present=True),
                ),
            ],
        ),
    )
)
async def test_remove_group_toe_ports() -> None:
    loaders = get_new_context()
    group_request = GroupToePortsRequest(group_name=GROUP_NAME)
    toe_ports = await loaders.group_toe_ports.load_nodes(group_request)
    assert len(toe_ports) == 3
    await toe_ports_model.remove_group_toe_ports(group_name=GROUP_NAME)
    toe_ports_now = await get_new_context().group_toe_ports.load_nodes(group_request)
    assert len(toe_ports_now) == 0
