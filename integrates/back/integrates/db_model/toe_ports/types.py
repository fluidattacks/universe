from datetime import (
    date,
    datetime,
)
from typing import (
    NamedTuple,
)

from integrates.dynamodb.types import (
    FilterExpression,
    PageInfo,
)


class ToePortState(NamedTuple):
    attacked_at: datetime | None
    attacked_by: str | None
    be_present: bool
    be_present_until: datetime | None
    first_attack_at: datetime | None
    has_vulnerabilities: bool
    modified_by: str | None
    modified_date: datetime | None
    seen_at: datetime | None
    seen_first_time_by: str | None


class ToePort(NamedTuple):
    group_name: str
    address: str
    port: str
    root_id: str
    state: ToePortState


class ToePortEdge(NamedTuple):
    node: ToePort
    cursor: str


class ToePortsConnection(NamedTuple):
    edges: tuple[ToePortEdge, ...]
    page_info: PageInfo


class ToePortStatesEdge(NamedTuple):
    node: ToePortState
    cursor: str


class ToePortStatesConnection(NamedTuple):
    edges: tuple[ToePortStatesEdge, ...]
    page_info: PageInfo


class ToePortRequest(NamedTuple):
    group_name: str
    address: str
    port: str
    root_id: str


class GroupToePortStatesRequest(NamedTuple):
    group_name: str
    after: str | None = None
    first: int | None = None
    paginate: bool = False
    start_date: date | None = None
    end_date: date | None = None


class GroupToePortsRequest(NamedTuple):
    group_name: str
    filters: FilterExpression | None = None
    after: str | None = None
    be_present: bool | None = None
    first: int | None = None
    paginate: bool = False


class RootToePortsRequest(NamedTuple):
    group_name: str
    root_id: str
    after: str | None = None
    be_present: bool | None = None
    first: int | None = None
    paginate: bool = False
