from datetime import datetime

from integrates.db_model import utils as db_model_utils
from integrates.db_model.items import ToePortItem, ToePortStateItem
from integrates.db_model.toe_ports.types import (
    ToePort,
    ToePortEdge,
    ToePortState,
    ToePortStatesEdge,
)
from integrates.dynamodb.types import Index, PrimaryKey, Table
from integrates.dynamodb.utils import get_cursor


def format_state(item: ToePortStateItem) -> ToePortState:
    return ToePortState(
        attacked_at=datetime.fromisoformat(item["attacked_at"])
        if item.get("attacked_at")
        else None,
        attacked_by=item.get("attacked_by"),
        be_present=item["be_present"],
        be_present_until=datetime.fromisoformat(item["be_present_until"])
        if item.get("be_present_until")
        else None,
        first_attack_at=datetime.fromisoformat(item["first_attack_at"])
        if item.get("first_attack_at")
        else None,
        has_vulnerabilities=item["has_vulnerabilities"],
        modified_by=item.get("modified_by"),
        modified_date=datetime.fromisoformat(item["modified_date"])
        if "modified_date" in item
        else None,
        seen_at=datetime.fromisoformat(item["seen_at"]) if item.get("seen_at") else None,
        seen_first_time_by=item.get("seen_first_time_by"),
    )


def format_toe_port_state_edge(
    index: Index | None,
    item: ToePortStateItem,
    table: Table,
) -> ToePortStatesEdge:
    return ToePortStatesEdge(
        node=format_state(item),
        cursor=get_cursor(index, item, table),
    )


def _format_state_item(state: ToePortState) -> ToePortStateItem:
    item: ToePortStateItem = {
        "be_present": state.be_present,
        "has_vulnerabilities": state.has_vulnerabilities,
    }
    if state.seen_first_time_by is not None:
        item["seen_first_time_by"] = state.seen_first_time_by
    if state.modified_by is not None:
        item["modified_by"] = state.modified_by
    if state.attacked_by is not None:
        item["attacked_by"] = state.attacked_by
    if state.attacked_at is not None:
        item["attacked_at"] = db_model_utils.get_as_utc_iso_format(state.attacked_at)
    if state.be_present_until is not None:
        item["be_present_until"] = db_model_utils.get_as_utc_iso_format(state.be_present_until)
    if state.first_attack_at is not None:
        item["first_attack_at"] = db_model_utils.get_as_utc_iso_format(state.first_attack_at)
    if state.seen_at is not None:
        item["seen_at"] = db_model_utils.get_as_utc_iso_format(state.seen_at)
    if state.modified_date is not None:
        item["modified_date"] = db_model_utils.get_as_utc_iso_format(state.modified_date)

    return item


def format_toe_port(
    item: ToePortItem,
) -> ToePort:
    return ToePort(
        group_name=item["group_name"],
        address=item["address"],
        port=item["port"],
        root_id=item["root_id"],
        state=format_state(item["state"]),
    )


def format_toe_port_edge(
    index: Index | None,
    item: ToePortItem,
    table: Table,
) -> ToePortEdge:
    return ToePortEdge(
        node=format_toe_port(item),
        cursor=get_cursor(index, item, table),
    )


def format_toe_port_item(
    primary_key: PrimaryKey,
    gsi_2_key: PrimaryKey,
    toe_port: ToePort,
) -> ToePortItem:
    return {
        "pk": primary_key.partition_key,
        "sk": primary_key.sort_key,
        "sk_2": gsi_2_key.sort_key,
        "pk_2": gsi_2_key.partition_key,
        "address": toe_port.address,
        "group_name": toe_port.group_name,
        "port": toe_port.port,
        "root_id": toe_port.root_id,
        "state": _format_state_item(toe_port.state),
    }


def format_toe_port_state_item(
    primary_key: PrimaryKey,
    gsi_2_key: PrimaryKey,
    toe_port: ToePort,
) -> dict:
    item = {
        "pk": primary_key.partition_key,
        "sk": primary_key.sort_key,
        "sk_2": gsi_2_key.sort_key,
        "pk_2": gsi_2_key.partition_key,
    }

    state = toe_port.state

    if state.seen_first_time_by is not None:
        item["seen_first_time_by"] = state.seen_first_time_by
    if state.modified_by is not None:
        item["modified_by"] = state.modified_by
    if state.attacked_by is not None:
        item["attacked_by"] = state.attacked_by
    if state.attacked_at is not None:
        item["attacked_at"] = db_model_utils.get_as_utc_iso_format(state.attacked_at)
    if state.be_present is not None:
        item["be_present"] = str(state.be_present).lower()
    if state.first_attack_at is not None:
        item["first_attack_at"] = db_model_utils.get_as_utc_iso_format(state.first_attack_at)
    if state.has_vulnerabilities is not None:
        item["has_vulnerabilities"] = str(state.has_vulnerabilities).lower()
    if state.seen_at is not None:
        item["seen_at"] = db_model_utils.get_as_utc_iso_format(state.seen_at)
    if state.modified_date is not None:
        item["modified_date"] = db_model_utils.get_as_utc_iso_format(state.modified_date)

    return item
