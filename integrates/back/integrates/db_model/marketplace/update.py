import simplejson as json
from boto3.dynamodb.conditions import (
    Attr,
)

from integrates.audit import AuditEvent, add_audit_event
from integrates.context import (
    FI_MARKETPLACE_PRODUCT_CODE,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.marketplace.types import (
    AWSMarketplaceSubscription,
    AWSMarketplaceSubscriptionState,
    AWSMarketplaceSubscriptionUpdate,
)
from integrates.db_model.utils import (
    serialize,
)
from integrates.dynamodb import (
    keys,
    operations,
)


async def update_marketplace_subscription_metadata(
    *,
    metadata: AWSMarketplaceSubscriptionUpdate,
    subscription: AWSMarketplaceSubscription,
    delete_none_attrs: bool = False,
) -> None:
    key_structure = TABLE.primary_key
    primary_key = keys.build_key(
        facet=TABLE.facets["aws_marketplace_subscription_metadata"],
        values={
            "aws_customer_id": subscription.aws_customer_id,
            "product_code": FI_MARKETPLACE_PRODUCT_CODE,
        },
    )
    update = json.loads(json.dumps(metadata, default=serialize))
    none_keys = [k for k in update.keys() if update[k] is None]
    if not delete_none_attrs:
        for key in none_keys:
            update.pop(key)
    await operations.update_item(
        condition_expression=Attr(key_structure.sort_key).exists(),
        item=update,
        key=primary_key,
        table=TABLE,
    )
    add_audit_event(
        AuditEvent(
            action="UPDATE",
            author=subscription.aws_customer_id,
            metadata=update,
            object="AWSMarketplaceSubscription",
            object_id=subscription.aws_customer_id,
        )
    )


async def update_marketplace_subscription_state(
    *,
    new_state: AWSMarketplaceSubscriptionState,
    subscription: AWSMarketplaceSubscription,
) -> None:
    key_structure = TABLE.primary_key
    primary_key = keys.build_key(
        facet=TABLE.facets["aws_marketplace_subscription_metadata"],
        values={
            "aws_customer_id": subscription.aws_customer_id,
            "product_code": FI_MARKETPLACE_PRODUCT_CODE,
        },
    )
    update = {"state": json.loads(json.dumps(new_state, default=serialize))}
    await operations.update_item(
        condition_expression=(
            Attr(key_structure.sort_key).exists()
            & Attr("state.modified_date").eq(subscription.state.modified_date.isoformat())
        ),
        item=update,
        key=primary_key,
        table=TABLE,
    )
    add_audit_event(
        AuditEvent(
            action="UPDATE",
            author=subscription.aws_customer_id,
            metadata=update,
            object="AWSMarketplaceSubscription",
            object_id=subscription.aws_customer_id,
        )
    )
