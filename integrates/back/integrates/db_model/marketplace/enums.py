from enum import (
    Enum,
)


class AWSMarketplacePricingDimension(str, Enum):
    AUTHORS: str = "authors"
    GROUPS: str = "groups"


class AWSMarketplaceSubscriptionStatus(str, Enum):
    ACTIVE: str = "ACTIVE"
    EXPIRED: str = "EXPIRED"
    FAILED: str = "FAILED"
    PENDING: str = "PENDING"
