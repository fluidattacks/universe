from datetime import (
    datetime,
)

from integrates.db_model.enums import (
    TreatmentStatus,
)
from integrates.db_model.items import OrgFindingPolicyItem
from integrates.db_model.organization_finding_policies.enums import (
    PolicyStateStatus,
)
from integrates.db_model.organization_finding_policies.types import (
    OrgFindingPolicy,
    OrgFindingPolicyState,
)


def format_organization_finding_policy(
    item: OrgFindingPolicyItem,
) -> OrgFindingPolicy:
    return OrgFindingPolicy(
        id=item.get("id") or item["pk"].split("#")[1],
        name=item["name"],
        organization_name=item.get("organization_name") or item["sk"].split("#")[1],
        state=OrgFindingPolicyState(
            modified_by=item["state"]["modified_by"],
            modified_date=datetime.fromisoformat(item["state"]["modified_date"]),
            status=PolicyStateStatus[item["state"]["status"]],
        ),
        tags=set(item["tags"]) if item.get("tags") else set(),
        treatment_acceptance=TreatmentStatus[item["treatment_acceptance"]],
    )
