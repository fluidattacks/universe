import simplejson as json

from integrates.audit import AuditEvent, add_audit_event
from integrates.class_types.types import (
    Item,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.organization_finding_policies.types import (
    OrgFindingPolicy,
)
from integrates.db_model.utils import (
    get_as_utc_iso_format,
    serialize,
)
from integrates.dynamodb import (
    keys,
    operations,
)


async def add(*, policy: OrgFindingPolicy) -> None:
    key_structure = TABLE.primary_key
    primary_key = keys.build_key(
        facet=TABLE.facets["org_finding_policy_metadata"],
        values={
            "name": policy.organization_name,
            "uuid": policy.id,
        },
    )
    items: list[Item] = []
    metadata_item = {
        key_structure.partition_key: primary_key.partition_key,
        key_structure.sort_key: primary_key.sort_key,
        **json.loads(json.dumps(policy, default=serialize)),
    }
    items.append(metadata_item)
    state_key = keys.build_key(
        facet=TABLE.facets["org_finding_policy_historic_state"],
        values={
            "iso8601utc": get_as_utc_iso_format(policy.state.modified_date),
            "uuid": policy.id,
        },
    )
    historic_state_item = {
        key_structure.partition_key: state_key.partition_key,
        key_structure.sort_key: state_key.sort_key,
        **json.loads(json.dumps(policy.state, default=serialize)),
    }
    items.append(historic_state_item)

    await operations.batch_put_item(items=tuple(items), table=TABLE)
    add_audit_event(
        AuditEvent(
            action="CREATE",
            author=policy.state.modified_by,
            metadata=metadata_item,
            object="OrgFindingPolicy",
            object_id=policy.id,
        )
    )
