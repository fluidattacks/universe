from boto3.dynamodb.conditions import (
    Attr,
)

from integrates.audit import AuditEvent, add_audit_event
from integrates.custom_exceptions import (
    ExecutionAlreadyCreated,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.forces.constants import (
    GSI_2_FACET,
)
from integrates.db_model.forces.types import (
    ForcesExecution,
)
from integrates.db_model.forces.utils import (
    format_forces_item,
)
from integrates.db_model.utils import (
    get_as_utc_iso_format,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.exceptions import (
    ConditionalCheckFailedException,
)


async def add(*, forces_execution: ForcesExecution) -> None:
    key_structure = TABLE.primary_key
    primary_key = keys.build_key(
        facet=TABLE.facets["forces_execution"],
        values={
            "id": forces_execution.id,
            "name": forces_execution.group_name,
        },
    )
    gsi_2_key = keys.build_key(
        facet=GSI_2_FACET,
        values={
            "execution_date": get_as_utc_iso_format(forces_execution.execution_date),
            "name": forces_execution.group_name,
        },
    )
    item = format_forces_item(forces_execution, primary_key=primary_key, gsi_2_key=gsi_2_key)
    condition_expression = Attr(key_structure.partition_key).not_exists()
    try:
        await operations.put_item(
            condition_expression=condition_expression,
            facet=TABLE.facets["forces_execution"],
            item=item,
            table=TABLE,
        )
        add_audit_event(
            AuditEvent(
                action="CREATE",
                author=f"forces.{forces_execution.group_name}@fluidattacks.com",
                metadata=item,
                object="ForcesExecution",
                object_id=forces_execution.id,
            )
        )
    except ConditionalCheckFailedException as ex:
        raise ExecutionAlreadyCreated().new() from ex
