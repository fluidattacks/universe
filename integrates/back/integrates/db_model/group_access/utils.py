from datetime import (
    datetime,
)

import simplejson as json

from integrates.class_types.types import (
    Item,
)
from integrates.custom_exceptions import (
    InvalidParameter,
)
from integrates.db_model.group_access.types import (
    GroupAccess,
    GroupAccessMetadataToUpdate,
    GroupAccessState,
    GroupConfirmDeletion,
    GroupInvitation,
)
from integrates.db_model.items import (
    GroupAccessItem,
    GroupAccessStateItem,
    GroupConfirmDeletionItem,
    GroupInvitationItem,
)
from integrates.db_model.utils import (
    serialize,
)


def format_group_access(item: GroupAccessItem) -> GroupAccess:
    return GroupAccess(
        email=str(item["email"]).lower().strip(),
        group_name=item["group_name"],
        expiration_time=int(item["expiration_time"]) if item.get("expiration_time") else None,
        state=_format_state(item.get("state")),
        domain=item.get("domain"),
    )


def _format_state(item: GroupAccessStateItem | None) -> GroupAccessState:
    if item is None:
        return GroupAccessState(modified_date=None, has_access=False)
    return GroupAccessState(
        modified_by=item.get("modified_by"),
        modified_date=datetime.fromisoformat(item["modified_date"])
        if "modified_date" in item
        else None,
        confirm_deletion=_format_confirm_deletion(item.get("confirm_deletion")),
        has_access=_format_has_access(item.get("has_access")),
        invitation=_format_invitation(item.get("invitation")),
        responsibility=item.get("responsibility"),
        role=item.get("role"),
    )


def _format_confirm_deletion(item: GroupConfirmDeletionItem | None) -> GroupConfirmDeletion | None:
    if item is None:
        return None
    return GroupConfirmDeletion(
        is_used=bool(item["is_used"]),
        url_token=item["url_token"],
    )


def _format_has_access(value: str | bool | None) -> bool:
    return bool(value) if value is not None else False


def _format_invitation(item: GroupInvitationItem | None) -> GroupInvitation | None:
    if item is None:
        return None
    return GroupInvitation(
        is_used=bool(item["is_used"]),
        responsibility=item.get("responsibility"),
        role=item["role"],
        url_token=item["url_token"],
    )


def format_metadata_item(
    email: str,
    group_name: str,
    metadata: GroupAccessMetadataToUpdate,
) -> Item:
    if metadata.state.modified_date is None:
        raise InvalidParameter("modified_date")

    state_item = json.loads(json.dumps(metadata.state, default=serialize))
    user_email = email.lower().strip()
    domain = email.split("@")[1]
    item: Item = {
        "email": user_email,
        "expiration_time": metadata.expiration_time,
        "group_name": group_name,
        "state": state_item,
        "pk_2": f"DOMAIN#{domain}",
        "sk_2": f"USER#{user_email}#GROUP#{group_name}",
        "domain": domain,
    }
    return {
        key: None if not value and value is not False else value
        for key, value in item.items()
        if value is not None
    }


def merge_group_access_changes(
    old_access: GroupAccess,
    changes: GroupAccessMetadataToUpdate,
) -> GroupAccessMetadataToUpdate:
    return GroupAccessMetadataToUpdate(
        state=changes.state,
        expiration_time=old_access.expiration_time
        if changes.expiration_time is None
        else changes.expiration_time,
    )
