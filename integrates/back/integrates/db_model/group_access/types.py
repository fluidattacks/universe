from datetime import (
    datetime,
)
from typing import (
    NamedTuple,
)

from integrates.dynamodb.types import (
    FilterExpression,
)


class GroupConfirmDeletion(NamedTuple):
    is_used: bool
    url_token: str


class GroupInvitation(NamedTuple):
    is_used: bool
    role: str
    url_token: str
    responsibility: str | None = None


class GroupAccessState(NamedTuple):
    modified_date: datetime | None
    has_access: bool
    modified_by: str | None = None
    confirm_deletion: GroupConfirmDeletion | None = None
    invitation: GroupInvitation | None = None
    responsibility: str | None = None
    role: str | None = None


class GroupAccess(NamedTuple):
    email: str
    group_name: str
    state: GroupAccessState
    expiration_time: int | None = None
    domain: str | None = None


class GroupAccessMetadataToUpdate(NamedTuple):
    state: GroupAccessState
    expiration_time: int | None = None


class GroupAccessRequest(NamedTuple):
    email: str
    group_name: str


class GroupStakeholdersAccessRequest(NamedTuple):
    group_name: str
    filters: FilterExpression | None = None
