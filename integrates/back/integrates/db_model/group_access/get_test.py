from integrates.dataloaders import get_new_context
from integrates.db_model.group_access.types import GroupAccessRequest
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import GroupAccessFaker, GroupAccessStateFaker
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize

DEFAULT_DOMAIN = IntegratesDynamodb(
    group_access=[
        GroupAccessFaker(
            email="foo@fluidattacks.com",
            group_name="group1",
        ),
        GroupAccessFaker(
            email="bar@fluidattacks.com",
            group_name="group1",
        ),
        # populate more data here
        GroupAccessFaker(
            email="baz@fluidattacks.com",
            group_name="group1",
        ),
        GroupAccessFaker(
            email="qux@fluidattacks.com",
            group_name="group2",
        ),
        GroupAccessFaker(
            email="quux@fluidattacks.com",
            group_name="group2",
            state=GroupAccessStateFaker(
                has_access=False,
                role="ANALYST",
            ),
        ),
    ]
)


@parametrize(
    args=["requests"],
    cases=[
        [
            [
                GroupAccessRequest(group_name="group1", email="foo@fluidattacks.com"),
                GroupAccessRequest(group_name="group2", email="qux@fluidattacks.com"),
            ],
        ],
    ],
)
@mocks(aws=IntegratesAws(dynamodb=DEFAULT_DOMAIN))
async def test_group_access_dataloader(
    requests: tuple[GroupAccessRequest, ...],
) -> None:
    loaders = get_new_context()
    items = await loaders.group_access.batch_load_fn(requests=requests)
    assert len(items) == len(requests)
    assert set(item.email for item in items if item) == set(request.email for request in requests)
    assert set(item.group_name for item in items if item) == set(
        request.group_name for request in requests
    )


@parametrize(
    args=["emails"],
    cases=[
        [["foo@fluidattacks.com", "bar@fluidattacks.com"]],
        [["baz@fluidattacks.com", "qux@fluidattacks.com"]],
    ],
)
@mocks(aws=IntegratesAws(dynamodb=DEFAULT_DOMAIN))
async def test_group_stakeholders_access_loader(
    emails: tuple[str],
) -> None:
    loaders = get_new_context()
    items = await loaders.stakeholder_groups_access.batch_load_fn(emails=emails)
    assert len(items) == len(emails)
