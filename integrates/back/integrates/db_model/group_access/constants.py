from integrates.db_model import (
    TABLE,
)
from integrates.dynamodb.types import (
    Facet,
)

GSI_2_FACET = Facet(
    attrs=TABLE.facets["group_access"].attrs,
    pk_alias="DOMAIN#domain",
    sk_alias="USER#email#GROUP#name",
)
