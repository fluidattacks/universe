from collections.abc import (
    Iterable,
)

from aiodataloader import (
    DataLoader,
)
from aioextensions import (
    collect,
)
from boto3.dynamodb.conditions import (
    Key,
)

from integrates.db_model import (
    TABLE,
)
from integrates.db_model.group_access.constants import (
    GSI_2_FACET,
)
from integrates.db_model.group_access.types import (
    GroupAccess,
    GroupAccessRequest,
    GroupStakeholdersAccessRequest,
)
from integrates.db_model.group_access.utils import (
    format_group_access,
)
from integrates.db_model.items import GroupAccessItem
from integrates.dynamodb import (
    conditions,
    keys,
    operations,
)


async def _get_group_access(*, requests: Iterable[GroupAccessRequest]) -> list[GroupAccess | None]:
    requests_formatted = [
        request._replace(
            group_name=request.group_name.lower().strip(),
            email=request.email.lower().strip(),
        )
        for request in requests
    ]
    primary_keys = tuple(
        keys.build_key(
            facet=TABLE.facets["group_access"],
            values={
                "email": request.email,
                "name": request.group_name,
            },
        )
        for request in requests_formatted
    )
    items = await operations.DynamoClient[GroupAccessItem].batch_get_item(
        keys=primary_keys, table=TABLE
    )

    response = {
        GroupAccessRequest(
            group_name=group_access.group_name,
            email=group_access.email,
        ): group_access
        for group_access in list(format_group_access(item) for item in items)
    }
    return [response.get(request) for request in requests_formatted]


class GroupAccessLoader(DataLoader[GroupAccessRequest, GroupAccess | None]):
    async def batch_load_fn(
        self,
        requests: Iterable[GroupAccessRequest],
    ) -> list[GroupAccess | None]:
        return await _get_group_access(requests=requests)


async def _get_group_stakeholders_access(
    *,
    access_dataloader: GroupAccessLoader,
    request: GroupStakeholdersAccessRequest,
) -> list[GroupAccess]:
    group_name = request.group_name.lower().strip()
    primary_key = keys.build_key(
        facet=TABLE.facets["group_access"],
        values={
            "name": group_name,
        },
    )

    index = TABLE.indexes["inverted_index"]
    key_structure = index.primary_key
    response = await operations.DynamoClient[GroupAccessItem].query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.sort_key)
            & Key(key_structure.sort_key).begins_with(primary_key.partition_key)
        ),
        facets=(TABLE.facets["group_access"],),
        filter_expression=conditions.get_filter_expression(request.filters),
        table=TABLE,
        index=index,
    )

    access_list: list[GroupAccess] = []
    for item in response.items:
        access = format_group_access(item)
        access_list.append(access)
        access_dataloader.prime(
            GroupAccessRequest(group_name=group_name, email=access.email),
            access,
        )

    return access_list


class GroupStakeholdersAccessLoader(DataLoader[GroupStakeholdersAccessRequest, list[GroupAccess]]):
    def __init__(self, dataloader: GroupAccessLoader) -> None:
        super().__init__()
        self.dataloader = dataloader

    async def batch_load_fn(
        self,
        requests: Iterable[GroupStakeholdersAccessRequest],
    ) -> list[list[GroupAccess]]:
        return list(
            await collect(
                tuple(
                    _get_group_stakeholders_access(
                        access_dataloader=self.dataloader,
                        request=request,
                    )
                    for request in requests
                ),
                workers=32,
            ),
        )


async def _get_stakeholder_groups_access(
    *,
    access_dataloader: GroupAccessLoader,
    email: str,
) -> list[GroupAccess]:
    email = email.lower().strip()
    primary_key = keys.build_key(
        facet=TABLE.facets["group_access"],
        values={
            "email": email,
        },
    )

    key_structure = TABLE.primary_key
    response = await operations.DynamoClient[GroupAccessItem].query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(TABLE.facets["group_access"],),
        table=TABLE,
    )

    access_list: list[GroupAccess] = []
    for item in response.items:
        access = format_group_access(item)
        access_list.append(access)
        access_dataloader.prime(
            GroupAccessRequest(group_name=access.group_name, email=email),
            access,
        )

    return access_list


class StakeholderGroupsAccessLoader(DataLoader[str, list[GroupAccess]]):
    def __init__(self, dataloader: GroupAccessLoader) -> None:
        super().__init__()
        self.dataloader = dataloader

    async def batch_load_fn(self, emails: Iterable[str]) -> list[list[GroupAccess]]:
        return list(
            await collect(
                tuple(
                    _get_stakeholder_groups_access(access_dataloader=self.dataloader, email=email)
                    for email in emails
                ),
                workers=32,
            ),
        )


async def _get_domain_groups_access(
    *,
    email: str,
) -> list[GroupAccess]:
    domain = email.lower().strip().split("@")[1]
    index = TABLE.indexes["gsi_2"]
    facet = GSI_2_FACET
    primary_key = keys.build_key(
        facet=facet,
        values={
            "domain": domain,
        },
    )
    key_structure = index.primary_key
    condition_expression = Key(key_structure.partition_key).eq(primary_key.partition_key) & Key(
        key_structure.sort_key,
    ).begins_with(primary_key.sort_key.replace("#GROUP", ""))
    response = await operations.DynamoClient[GroupAccessItem].query(
        condition_expression=(condition_expression),
        facets=(facet,),
        table=TABLE,
        index=index,
    )

    access_list: list[GroupAccess] = []
    for item in response.items:
        access = format_group_access(item)
        access_list.append(access)

    return access_list


class DomainGroupsAccessLoader(DataLoader[str, list[GroupAccess]]):
    def __init__(
        self,
    ) -> None:
        super().__init__()

    async def batch_load_fn(self, emails: Iterable[str]) -> list[list[GroupAccess]]:
        return list(
            await collect(
                tuple(_get_domain_groups_access(email=email) for email in emails),
                workers=32,
            ),
        )
