import asyncio
from datetime import (
    datetime,
)
from typing import (
    NamedTuple,
    cast,
)

from aiodataloader import (
    DataLoader,
)
from boto3.dynamodb.conditions import (
    Key,
)

from integrates.db_model import (
    TABLE,
)
from integrates.db_model.items import NotificationItem
from integrates.db_model.notifications.types import (
    Notification,
    NotificationType,
    ReportNotification,
    ReportStatus,
)
from integrates.dynamodb import (
    operations,
)
from integrates.dynamodb.types import (
    PrimaryKey,
)


def _format_report_notification(item: NotificationItem) -> Notification:
    return ReportNotification(
        expiration_time=(
            datetime.fromtimestamp(int(item["expiration_time"]))
            if item.get("expiration_time")
            else None
        ),
        format=item["format"],
        id=item["id"],
        name=item["name"],
        notified_at=datetime.fromisoformat(item["notified_at"]),
        s3_file_path=item.get("s3_file_path", item.get("download_url")),
        size=(int(item["size"]) if item.get("size") else None),
        status=ReportStatus(item["status"]),
        type=cast(NotificationType, item["type"]),
        user_email=item["user_email"],
    )


class NotificationRequest(NamedTuple):
    notification_id: str
    user_email: str


class NotificationLoader(DataLoader[NotificationRequest, Notification | None]):
    async def batch_load_fn(
        self,
        requests: list[NotificationRequest],
    ) -> list[Notification | None]:
        items = await operations.DynamoClient[NotificationItem].batch_get_item(
            keys=tuple(
                PrimaryKey(
                    partition_key=f"USER#{request.user_email}",
                    sort_key=f"NOTIFICATION#{request.notification_id}",
                )
                for request in requests
            ),
            table=TABLE,
        )
        notifications = [_format_report_notification(item) for item in items]
        responses = {
            NotificationRequest(
                notification_id=notification.id,
                user_email=notification.user_email,
            ): notification
            for notification in notifications
        }

        return [responses.get(request) for request in requests]


async def _get_user_notifications(*, user_email: str) -> list[Notification]:
    response = await operations.DynamoClient[NotificationItem].query(
        condition_expression=(
            Key("pk").eq(f"USER#{user_email}") & Key("sk").begins_with("NOTIFICATION#")
        ),
        facets=(TABLE.facets["notification_metadata"],),
        forward=False,
        table=TABLE,
    )

    return [
        _format_report_notification(item) for item in response.items if item["type"] == "REPORT"
    ]


class UserNotificationsLoader(DataLoader[str, list[Notification]]):
    async def batch_load_fn(
        self,
        user_emails: list[str],
    ) -> list[list[Notification]]:
        return await asyncio.gather(
            *[_get_user_notifications(user_email=user_email) for user_email in user_emails],
        )
