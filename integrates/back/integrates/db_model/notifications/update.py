from boto3.dynamodb.conditions import (
    Attr,
)

from integrates.audit import AuditEvent, add_audit_event
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.notifications.types import (
    Notification,
)
from integrates.db_model.notifications.utils import (
    format_base_item,
)
from integrates.dynamodb import (
    operations,
)
from integrates.dynamodb.types import (
    PrimaryKey,
)


async def update(*, notification: Notification) -> None:
    item = format_base_item(notification)
    await operations.update_item(
        condition_expression=Attr("pk").exists(),
        item=item,
        key=PrimaryKey(
            partition_key=f"USER#{notification.user_email}",
            sort_key=f"NOTIFICATION#{notification.id}",
        ),
        table=TABLE,
    )
    add_audit_event(
        AuditEvent(
            action="UPDATE",
            author=notification.user_email,
            metadata=item,
            object="Notification",
            object_id=notification.id,
        )
    )
