from boto3.dynamodb.conditions import (
    Attr,
)

from integrates.audit import AuditEvent, add_audit_event
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.notifications.types import (
    Notification,
)
from integrates.db_model.notifications.utils import (
    format_full_item,
)
from integrates.dynamodb import (
    operations,
)


async def add(*, notification: Notification) -> None:
    item = format_full_item(notification)
    await operations.put_item(
        condition_expression=Attr("pk").not_exists(),
        facet=TABLE.facets["notification_metadata"],
        item=item,
        table=TABLE,
    )
    add_audit_event(
        AuditEvent(
            action="CREATE",
            author=notification.user_email,
            metadata=item,
            object="Notification",
            object_id=notification.id,
        )
    )
