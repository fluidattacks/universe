from datetime import datetime

from integrates.dataloaders import get_new_context
from integrates.db_model.enums import Source
from integrates.db_model.findings.types import FindingEvidence, FindingEvidences
from integrates.db_model.vulnerabilities.add import add
from integrates.db_model.vulnerabilities.enums import VulnerabilityStateStatus, VulnerabilityType
from integrates.db_model.vulnerabilities.types import Vulnerability
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    FindingFaker,
    GitRootFaker,
    GitRootStateFaker,
    GroupFaker,
    OrganizationFaker,
    ToeLinesFaker,
    ToeLinesStateFaker,
    VulnerabilityFaker,
    VulnerabilityStateFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

MACHINE_EMAIL = "machine@fluidattacks.com"


@parametrize(
    args=["vulnerability", "finding_id"],
    cases=[
        [
            VulnerabilityFaker(
                finding_id="fin_id_1",
                group_name="group_1",
                id="vuln_id_2",
                organization_name="org_1",
                root_id="root_1",
                created_by=MACHINE_EMAIL,
                hacker_email=MACHINE_EMAIL,
                type=VulnerabilityType.LINES,
                state=VulnerabilityStateFaker(
                    where="java_has_print_statements.java",
                    specific="17",
                    source=Source.MACHINE,
                ),
                vuln_machine_hash=None,
                vuln_skims_description=(
                    "print sensitive data in java_has_print_statements.java line 16"
                ),
                vuln_skims_method="java.java_has_print_statements",
            ),
            "fin_id_1",
        ],
        [
            VulnerabilityFaker(
                finding_id="fin_id_1",
                group_name="group_1",
                id="vuln_id_2",
                organization_name="org_1",
                root_id="root_1",
                created_by=MACHINE_EMAIL,
                hacker_email=MACHINE_EMAIL,
                type=VulnerabilityType.LINES,
                state=VulnerabilityStateFaker(
                    where="java_has_print_statements.java",
                    specific="17",
                    source=Source.MACHINE,
                ),
                vuln_machine_hash=10243425851358753001,
                vuln_skims_description=(
                    "print sensitive data in java_has_print_statements.java line 16"
                ),
                vuln_skims_method="java.java_has_print_statements",
            ),
            "fin_id_1",
        ],
        [
            VulnerabilityFaker(
                finding_id="fin_id_1",
                group_name="group_1",
                id="vuln_id_453",
                organization_name="org_1",
                root_id="root_1",
                created_by=MACHINE_EMAIL,
                hacker_email=MACHINE_EMAIL,
                type=VulnerabilityType.LINES,
                state=VulnerabilityStateFaker(
                    where="test.cs",
                    specific="20",
                    source=Source.MACHINE,
                    status=VulnerabilityStateStatus.VULNERABLE,
                ),
                vuln_machine_hash=9478425851358758888,
            ),
            "fin_id_1",
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org_1"),
            ],
            groups=[
                GroupFaker(organization_id="org_1"),
            ],
            roots=[
                GitRootFaker(
                    id="root_1",
                    state=GitRootStateFaker(),
                ),
            ],
            findings=[
                FindingFaker(
                    id="finding_id_1",
                    evidences=FindingEvidences(
                        evidence1=FindingEvidence(
                            description="evidence1",
                            url=("test_group-evidence_1"),
                            modified_date=datetime.fromisoformat("2020-11-19T13:37:10+00:00"),
                        ),
                    ),
                ),
            ],
            toe_lines=[
                ToeLinesFaker(
                    filename="java_has_print_statements.java",
                    root_id="root_1",
                    state=ToeLinesStateFaker(loc=4324),
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    finding_id="fin_id_1",
                    group_name="group_1",
                    id="vuln_id_1",
                    organization_name="org_1",
                    root_id="root_1",
                    created_by=MACHINE_EMAIL,
                    hacker_email=MACHINE_EMAIL,
                    type=VulnerabilityType.LINES,
                    state=VulnerabilityStateFaker(
                        where="java_has_print_statements.java",
                        specific="16",
                        source=Source.MACHINE,
                        status=VulnerabilityStateStatus.VULNERABLE,
                    ),
                    vuln_machine_hash=10243425851358753000,
                    vuln_skims_description=(
                        "print sensitive data in java_has_print_statements.java line 16"
                    ),
                    vuln_skims_method="java.java_has_print_statements",
                ),
                VulnerabilityFaker(
                    finding_id="fin_id_1",
                    group_name="group_1",
                    id="vuln_id_100",
                    organization_name="org_1",
                    root_id="root_1",
                    created_by=MACHINE_EMAIL,
                    hacker_email=MACHINE_EMAIL,
                    type=VulnerabilityType.LINES,
                    state=VulnerabilityStateFaker(
                        where="test.cs",
                        specific="20",
                        source=Source.MACHINE,
                        status=VulnerabilityStateStatus.SAFE,
                    ),
                    vuln_machine_hash=9478425851358758888,
                ),
            ],
        ),
    ),
)
async def test_add_vulnerability(vulnerability: Vulnerability, finding_id: str) -> None:
    vulns_before = await get_new_context().finding_vulnerabilities.load(finding_id)
    assert len(vulns_before) == 2

    await add(vulnerability=vulnerability)

    vulns_after = await get_new_context().finding_vulnerabilities.load(finding_id)
    assert len(vulns_after) == 3


@parametrize(
    args=["vulnerability", "error_message"],
    cases=[
        [
            VulnerabilityFaker(
                finding_id="fin_id_1",
                group_name="group_1",
                id="vuln_id_1",
                organization_name="org_1",
                root_id="root_1",
                created_by=MACHINE_EMAIL,
                hacker_email=MACHINE_EMAIL,
                type=VulnerabilityType.LINES,
                state=VulnerabilityStateFaker(
                    where="java_has_print_statements.java",
                    specific="16",
                    source=Source.MACHINE,
                ),
                vuln_machine_hash=10243425851358753000,
                vuln_skims_description=(
                    "print sensitive data in java_has_print_statements.java line 16"
                ),
                vuln_skims_method="java.java_has_print_statements",
            ),
            "This vulnerability has already been created",
        ],
        [
            VulnerabilityFaker(
                finding_id="fin_id_1",
                group_name="group_1",
                id="vuln_id_2",
                organization_name="org_1",
                root_id="root_1",
                created_by=MACHINE_EMAIL,
                hacker_email=MACHINE_EMAIL,
                type=VulnerabilityType.LINES,
                state=VulnerabilityStateFaker(
                    where="java_has_print_statements.java",
                    specific="17",
                    source=Source.MACHINE,
                ),
                vuln_machine_hash=10243425851358753000,
                vuln_skims_description=(
                    "print sensitive data in java_has_print_statements.java line 16"
                ),
                vuln_skims_method="java.java_has_print_statements",
            ),
            "This vulnerability has already been created",
        ],
        [
            VulnerabilityFaker(
                finding_id="fin_id_1",
                group_name="group_1",
                id="vuln_id_1",
                organization_name="org_1",
                root_id="root_1",
                created_by=MACHINE_EMAIL,
                hacker_email=MACHINE_EMAIL,
                type=VulnerabilityType.LINES,
                state=VulnerabilityStateFaker(
                    where="java_has_print_statements.java",
                    specific="17",
                    source=Source.MACHINE,
                ),
                vuln_machine_hash=10243425851358753001,
                vuln_skims_description=(
                    "print sensitive data in java_has_print_statements.java line 16"
                ),
                vuln_skims_method="java.java_has_print_statements",
            ),
            "This vulnerability has already been created",
        ],
        [
            VulnerabilityFaker(
                finding_id="fin_id_1",
                group_name="group_1",
                id="vuln_id_1",
                organization_name="org_1",
                root_id="root_1",
                created_by=MACHINE_EMAIL,
                hacker_email=MACHINE_EMAIL,
                type=VulnerabilityType.LINES,
                state=VulnerabilityStateFaker(
                    where="java_has_print_statements.java",
                    specific="17",
                    source=Source.MACHINE,
                ),
                vuln_machine_hash=None,
                vuln_skims_description=(
                    "print sensitive data in java_has_print_statements.java line 16"
                ),
                vuln_skims_method="java.java_has_print_statements",
            ),
            "This vulnerability has already been created",
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            vulnerabilities=[
                VulnerabilityFaker(
                    finding_id="fin_id_1",
                    group_name="group_1",
                    id="vuln_id_1",
                    organization_name="org_1",
                    root_id="root_1",
                    created_by=MACHINE_EMAIL,
                    hacker_email=MACHINE_EMAIL,
                    type=VulnerabilityType.LINES,
                    state=VulnerabilityStateFaker(
                        where="java_has_print_statements.java",
                        specific="16",
                        source=Source.MACHINE,
                    ),
                    vuln_machine_hash=10243425851358753000,
                    vuln_skims_description=(
                        "print sensitive data in java_has_print_statements.java line 16"
                    ),
                    vuln_skims_method="java.java_has_print_statements",
                ),
            ],
        )
    ),
)
async def test_add_existent_vulnerability(vulnerability: Vulnerability, error_message: str) -> None:
    with raises(Exception, match=error_message):
        await add(vulnerability=vulnerability)
