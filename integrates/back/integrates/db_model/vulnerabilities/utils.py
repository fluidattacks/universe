import json
from datetime import datetime
from decimal import Decimal
from typing import TypedDict, TypeVar

from fluidattacks_core.serializers.snippet import Function, Snippet

from integrates.class_types.types import Item
from integrates.custom_exceptions import InvalidParameter, VulnerabilityEntryNotFound
from integrates.db_model.constants import CVSS_V4_DEFAULT
from integrates.db_model.enums import AcceptanceStatus, Source
from integrates.db_model.items import (
    SeverityScoreItem,
    SnippetItem,
    VulnerabilityAdvisoryItem,
    VulnerabilityAuthorItem,
    VulnerabilityItem,
    VulnerabilitySeverityProposalItem,
    VulnerabilitySnippetItem,
    VulnerabilityStateItem,
    VulnerabilityToolItem,
    VulnerabilityUnreliableIndicatorsItem,
    VulnerabilityVerificationItem,
    VulnerabilityZeroRiskItem,
)
from integrates.db_model.types import SeverityScore, Treatment
from integrates.db_model.utils import format_treatment, get_as_utc_iso_format
from integrates.db_model.vulnerabilities.constants import (
    ACCEPTED_TREATMENT_STATUSES,
    GROUP_INDEX_METADATA,
    NEW_ZR_INDEX_METADATA,
    RELEASED_FILTER_STATUSES,
    ZR_FILTER_STATUSES,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilitySeverityProposalStatus,
    VulnerabilityStateReason,
    VulnerabilityStateStatus,
    VulnerabilityTechnique,
    VulnerabilityToolImpact,
    VulnerabilityType,
    VulnerabilityVerificationStatus,
    VulnerabilityZeroRiskStatus,
)
from integrates.db_model.vulnerabilities.types import (
    VulnerabilitiesConnection,
    Vulnerability,
    VulnerabilityAdvisory,
    VulnerabilityAuthor,
    VulnerabilityEdge,
    VulnerabilityHistoricEntry,
    VulnerabilitySeverityProposal,
    VulnerabilityState,
    VulnerabilityTool,
    VulnerabilityUnreliableIndicators,
    VulnerabilityUnreliableIndicatorsToUpdate,
    VulnerabilityVerification,
    VulnerabilityZeroRisk,
)
from integrates.dynamodb import keys
from integrates.dynamodb.types import Index, PrimaryKey, Table
from integrates.dynamodb.utils import get_cursor
from integrates.search.operations import SearchClient, SearchParams

T = TypeVar("T")


def get_current_treatment_converted(treatment: str) -> str:
    if treatment == "UNTREATED":
        return "NEW"

    return treatment


def get_inverted_treatment_converted(treatment: str) -> str:
    if treatment == "NEW":
        return "UNTREATED"

    return treatment


def get_current_state_converted(state: str) -> str:
    if state in {"SAFE", "VULNERABLE"}:
        translation: dict[str, str] = {
            "SAFE": "CLOSED",
            "VULNERABLE": "OPEN",
        }

        return translation[state]

    return state


def get_inverted_state_converted(state: str) -> str:
    if state in {"CLOSED", "OPEN"}:
        translation: dict[str, str] = {
            "CLOSED": "SAFE",
            "OPEN": "VULNERABLE",
        }

        return translation[state]

    return state


def sanitize_state(state: str | None) -> str | None:
    translation: dict[str, str] = {
        "CLOSED": "SAFE",
        "OPEN": "VULNERABLE",
    }
    if state and state.upper() in translation:
        return translation[state.upper()]

    return state


def filter_non_deleted(
    vulnerabilities: list[Vulnerability],
) -> list[Vulnerability]:
    return [
        vuln
        for vuln in vulnerabilities
        if vuln.state.status
        not in {
            VulnerabilityStateStatus.DELETED,
            VulnerabilityStateStatus.MASKED,
        }
    ]


def filter_released_and_non_zero_risk(
    vulnerabilities: list[Vulnerability],
) -> list[Vulnerability]:
    return [
        vuln
        for vuln in vulnerabilities
        if vuln.state.status in RELEASED_FILTER_STATUSES
        and (not vuln.zero_risk or vuln.zero_risk.status not in ZR_FILTER_STATUSES)
    ]


def filter_released_and_zero_risk(
    vulnerabilities: list[Vulnerability],
) -> list[Vulnerability]:
    return [
        vuln
        for vuln in vulnerabilities
        if vuln.state.status in RELEASED_FILTER_STATUSES
        and (vuln.zero_risk and vuln.zero_risk.status in ZR_FILTER_STATUSES)
    ]


def format_author(item: VulnerabilityAuthorItem | None) -> VulnerabilityAuthor | None:
    if not item:
        return None

    return VulnerabilityAuthor(
        author_email=item["author_email"],
        commit=item["commit"],
        commit_date=datetime.fromisoformat(item["commit_date"]),
    )


def format_proposed_severity(
    item: VulnerabilitySeverityProposalItem,
) -> VulnerabilitySeverityProposal:
    return VulnerabilitySeverityProposal(
        modified_by=item["modified_by"],
        modified_date=datetime.fromisoformat(item["modified_date"]),
        severity_score=format_severity(item["severity_score"]),
        status=VulnerabilitySeverityProposalStatus[item["status"]],
    )


def format_severity(item: SeverityScoreItem) -> SeverityScore:
    return SeverityScore(
        base_score=Decimal(item["base_score"]),
        temporal_score=Decimal(item["temporal_score"]),
        cvss_v3=item["cvss_v3"],
        cvssf=Decimal(item["cvssf"]),
        cvss_v4=item["cvss_v4"] if item.get("cvss_v4") else CVSS_V4_DEFAULT,
        threat_score=Decimal(item["threat_score"]) if item.get("threat_score") else Decimal("0"),
        cvssf_v4=Decimal(item["cvssf_v4"]) if item.get("cvssf_v4") else Decimal("0"),
    )


def is_root_mismatch(root_id: str, pk_2: str) -> bool:
    return bool(
        root_id and (not pk_2 or not pk_2.startswith("ROOT#") or pk_2.split("#")[1] != root_id),
    )


def format_vulnerability(item: VulnerabilityItem) -> Vulnerability:
    state = format_state(item["state"])
    treatment = format_treatment(item["treatment"]) if "treatment" in item else None
    verification = format_verification(item["verification"]) if "verification" in item else None
    zero_risk = format_zero_risk(item["zero_risk"]) if "zero_risk" in item else None
    unreliable_indicators = (
        format_unreliable_indicators(item["unreliable_indicators"])
        if "unreliable_indicators" in item
        else VulnerabilityUnreliableIndicators()
    )
    cwe_ids: list[str] | None = sorted(item["cwe_ids"]) if item.get("cwe_ids") else None
    severity_score = (
        format_severity(item["severity_score"]) if item.get("severity_score") is not None else None
    )
    root_id: str = item.get("root_id") or ""
    pk_2: str = item.get("pk_2") or ""
    root_id_mismatch: bool = is_root_mismatch(root_id, pk_2)
    if not root_id:
        root_id = pk_2.split("#")[1] if pk_2.startswith("ROOT#") else ""

    return Vulnerability(
        author=format_author(item.get("author")),
        bug_tracking_system_url=item.get("bug_tracking_system_url"),
        created_by=item["created_by"],
        created_date=datetime.fromisoformat(item["created_date"]),
        custom_severity=(
            int(item["custom_severity"])
            if "custom_severity" in item
            and item["custom_severity"] is not None
            and item["custom_severity"]
            else None
        ),
        cwe_ids=cwe_ids,
        developer=item.get("developer"),
        event_id=item.get("pk_4"),
        finding_id=item["sk"].split("#")[1],
        group_name=item["group_name"],
        hacker_email=item["hacker_email"],
        hash=item.get("hash"),
        id=item["pk"].split("#")[1],
        organization_name=item["organization_name"],
        root_id=root_id,
        root_id_mismatch=root_id_mismatch,
        severity_score=severity_score,
        skims_method=item.get("skims_method"),
        skims_description=item.get("skims_description"),
        state=state,
        stream=item.get("stream"),
        tags=item.get("tags"),
        technique=VulnerabilityTechnique[item["technique"]] if item.get("technique") else None,
        treatment=treatment,
        type=VulnerabilityType[item["type"]],
        unreliable_indicators=unreliable_indicators,
        verification=verification,
        webhook_url=item.get("webhook_url"),
        zero_risk=zero_risk,
    )


def format_vulnerability_edge(
    index: Index | None,
    item: VulnerabilityItem,
    table: Table,
) -> VulnerabilityEdge:
    return VulnerabilityEdge(node=format_vulnerability(item), cursor=get_cursor(index, item, table))


def format_snippet(
    snippet: SnippetItem | VulnerabilitySnippetItem | None = None,
) -> Snippet | None:
    if not snippet or isinstance(snippet, str):
        return None
    function: Function | None = None
    if _function := snippet.get("function"):
        function = Function(
            name=_function.get("name"),
            node_type=_function.get("node_type"),
            field_identifier_name=_function.get("filed_identifier_name"),
        )
    for key, value in snippet.items():
        if isinstance(value, Decimal):
            snippet[key] = int(value)  # type: ignore

    return Snippet(
        content=snippet["content"],
        offset=snippet["offset"],
        line=snippet["line"],
        column=snippet.get("column"),
        columns_per_line=snippet["columns_per_line"],
        line_context=snippet["line_context"],
        highlight_line_number=snippet["highlight_line_number"],
        show_line_numbers=snippet["show_line_numbers"],
        wrap=snippet["wrap"],
        is_function=bool(snippet.get("is_function", False)),
        function=function,
    )


def _format_advisory(
    advisory: VulnerabilityAdvisoryItem | None = None,
) -> VulnerabilityAdvisory | None:
    if not advisory:
        return None
    return VulnerabilityAdvisory(
        cve=list(advisory["cve"]) if isinstance(advisory["cve"], (list, set)) else [],
        package=advisory["package"],
        vulnerable_version=advisory["vulnerable_version"],
        epss=int(advisory["epss"]) if advisory.get("epss") else None,
    )


def format_state(item: VulnerabilityStateItem) -> VulnerabilityState:
    tool = format_tool(item["tool"]) if "tool" in item else None
    proposed_severity = (
        format_proposed_severity(item["proposed_severity"])
        if item.get("proposed_severity") is not None
        else None
    )
    return VulnerabilityState(
        advisories=_format_advisory(item.get("advisories")),
        commit=item.get("commit"),
        modified_by=item["modified_by"],
        modified_date=datetime.fromisoformat(item["modified_date"]),
        other_reason=item.get("other_reason"),
        reasons=[VulnerabilityStateReason[reason] for reason in item["reasons"]]
        if "reasons" in item
        else None,
        source=Source[item["source"]],
        specific=item["specific"],
        status=VulnerabilityStateStatus[item["status"]],
        tool=tool,
        where=item["where"],
        proposed_severity=proposed_severity,
    )


def format_tool(item: VulnerabilityToolItem) -> VulnerabilityTool:
    return VulnerabilityTool(name=item["name"], impact=VulnerabilityToolImpact[item["impact"]])


def format_unreliable_indicators(
    item: VulnerabilityUnreliableIndicatorsItem,
) -> VulnerabilityUnreliableIndicators:
    return VulnerabilityUnreliableIndicators(
        unreliable_closing_date=datetime.fromisoformat(item["unreliable_closing_date"])
        if item.get("unreliable_closing_date")
        else None,
        unreliable_efficacy=item.get("unreliable_efficacy"),
        unreliable_last_reattack_date=datetime.fromisoformat(item["unreliable_last_reattack_date"])
        if item.get("unreliable_last_reattack_date")
        else None,
        unreliable_last_reattack_requester=item.get("unreliable_last_reattack_requester"),
        unreliable_last_requested_reattack_date=datetime.fromisoformat(
            item["unreliable_last_requested_reattack_date"],
        )
        if item.get("unreliable_last_requested_reattack_date")
        else None,
        unreliable_priority=None
        if item.get("unreliable_priority") is None
        else Decimal(item["unreliable_priority"]),
        unreliable_reattack_cycles=None
        if item.get("unreliable_reattack_cycles") is None
        else int(item["unreliable_reattack_cycles"]),
        unreliable_report_date=datetime.fromisoformat(item["unreliable_report_date"])
        if item.get("unreliable_report_date")
        else None,
        unreliable_source=Source[item["unreliable_source"]]
        if item.get("unreliable_source")
        else Source.ASM,
        unreliable_treatment_changes=None
        if item.get("unreliable_treatment_changes") is None
        else int(item["unreliable_treatment_changes"]),
    )


def format_unreliable_indicators_item(
    indicators: VulnerabilityUnreliableIndicators,
) -> Item:
    item = {
        "unreliable_closing_date": (
            get_as_utc_iso_format(indicators.unreliable_closing_date)
            if indicators.unreliable_closing_date
            else None
        ),
        "unreliable_efficacy": indicators.unreliable_efficacy,
        "unreliable_last_reattack_date": (
            get_as_utc_iso_format(indicators.unreliable_last_reattack_date)
            if indicators.unreliable_last_reattack_date
            else None
        ),
        "unreliable_last_reattack_requester": (indicators.unreliable_last_reattack_requester),
        "unreliable_last_requested_reattack_date": (
            get_as_utc_iso_format(indicators.unreliable_last_requested_reattack_date)
            if indicators.unreliable_last_requested_reattack_date
            else None
        ),
        "unreliable_priority": indicators.unreliable_priority,
        "unreliable_reattack_cycles": indicators.unreliable_reattack_cycles,
        "unreliable_report_date": (
            get_as_utc_iso_format(indicators.unreliable_report_date)
            if indicators.unreliable_report_date
            else None
        ),
        "unreliable_source": indicators.unreliable_source,
        "unreliable_treatment_changes": (indicators.unreliable_treatment_changes),
    }

    return {key: value for key, value in item.items() if value is not None}


def format_unreliable_indicators_to_update_item(
    indicators: VulnerabilityUnreliableIndicatorsToUpdate,
) -> Item:
    item = {
        "unreliable_closing_date": (
            get_as_utc_iso_format(indicators.unreliable_closing_date)
            if indicators.unreliable_closing_date
            else None
        ),
        "unreliable_efficacy": indicators.unreliable_efficacy,
        "unreliable_last_reattack_date": (
            get_as_utc_iso_format(indicators.unreliable_last_reattack_date)
            if indicators.unreliable_last_reattack_date
            else None
        ),
        "unreliable_last_reattack_requester": (indicators.unreliable_last_reattack_requester),
        "unreliable_last_requested_reattack_date": (
            get_as_utc_iso_format(indicators.unreliable_last_requested_reattack_date)
            if indicators.unreliable_last_requested_reattack_date
            else None
        ),
        "unreliable_priority": indicators.unreliable_priority,
        "unreliable_reattack_cycles": indicators.unreliable_reattack_cycles,
        "unreliable_report_date": (
            get_as_utc_iso_format(indicators.unreliable_report_date)
            if indicators.unreliable_report_date
            else None
        ),
        "unreliable_treatment_changes": (indicators.unreliable_treatment_changes),
    }
    item = {key: value for key, value in item.items() if value is not None}

    if indicators.clean_unreliable_closing_date:
        item["unreliable_closing_date"] = None
    if indicators.clean_unreliable_last_reattack_date:
        item["unreliable_last_reattack_date"] = None
    if indicators.clean_unreliable_last_requested_reattack_date:
        item["unreliable_last_requested_reattack_date"] = None
    if indicators.clean_unreliable_report_date:
        item["unreliable_report_date"] = None
    return item


def format_verification(item: VulnerabilityVerificationItem) -> VulnerabilityVerification:
    return VulnerabilityVerification(
        event_id=item.get("event_id"),
        modified_by=item.get("modified_by"),
        modified_date=datetime.fromisoformat(item["modified_date"]),
        status=VulnerabilityVerificationStatus[item["status"]],
        justification=item.get("justification"),
    )


def format_zero_risk(item: VulnerabilityZeroRiskItem) -> VulnerabilityZeroRisk:
    return VulnerabilityZeroRisk(
        comment_id=item["comment_id"],
        modified_by=item["modified_by"],
        modified_date=datetime.fromisoformat(item["modified_date"]),
        status=VulnerabilityZeroRiskStatus[item["status"]],
    )


def historic_entry_type_to_str(item: T) -> str:
    if isinstance(item, VulnerabilityState):
        return "state"
    if isinstance(item, Treatment):
        return "treatment"
    if isinstance(item, VulnerabilityVerification):
        return "verification"
    if isinstance(item, VulnerabilityZeroRisk):
        return "zero_risk"

    raise InvalidParameter(str(type(item)))


def get_current_entry(
    entry: VulnerabilityHistoricEntry,
    current_value: Vulnerability,
) -> VulnerabilityHistoricEntry | None:
    if isinstance(entry, VulnerabilityState):
        return current_value.state
    if isinstance(entry, Treatment):
        return current_value.treatment
    if isinstance(entry, VulnerabilityVerification):
        return current_value.verification
    if isinstance(entry, VulnerabilityZeroRisk):
        return current_value.zero_risk

    raise VulnerabilityEntryNotFound()


def get_group_index_key(vulnerability: Vulnerability) -> PrimaryKey:
    return keys.build_key(
        facet=GROUP_INDEX_METADATA,
        values={
            "group_name": vulnerability.group_name,
            "is_zero_risk": str(
                bool(
                    vulnerability.zero_risk
                    and vulnerability.zero_risk.status == VulnerabilityZeroRiskStatus.CONFIRMED,
                ),
            ).lower(),
            "state_status": vulnerability.state.status.lower(),
            "is_accepted": str(
                bool(
                    vulnerability.treatment
                    and vulnerability.treatment.status in ACCEPTED_TREATMENT_STATUSES
                    and vulnerability.treatment.acceptance_status == AcceptanceStatus.APPROVED,
                ),
            ).lower(),
        },
    )


def get_new_group_index_key(
    current_value: Vulnerability,
    entry: VulnerabilityHistoricEntry,
) -> PrimaryKey | None:
    new_group_index_key: PrimaryKey | None = None
    if isinstance(entry, VulnerabilityState):
        new_group_index_key = keys.build_key(
            facet=GROUP_INDEX_METADATA,
            values={
                "group_name": current_value.group_name,
                "is_zero_risk": str(
                    bool(
                        current_value.zero_risk
                        and current_value.zero_risk.status in ZR_FILTER_STATUSES,
                    ),
                ).lower(),
                "state_status": entry.status.lower(),
                "is_accepted": str(
                    bool(
                        current_value.treatment
                        and current_value.treatment.status in ACCEPTED_TREATMENT_STATUSES
                        and current_value.treatment.acceptance_status == AcceptanceStatus.APPROVED,
                    ),
                ).lower(),
            },
        )
    elif isinstance(entry, Treatment):
        new_group_index_key = keys.build_key(
            facet=GROUP_INDEX_METADATA,
            values={
                "group_name": current_value.group_name,
                "is_zero_risk": str(
                    bool(
                        current_value.zero_risk
                        and current_value.zero_risk.status in ZR_FILTER_STATUSES,
                    ),
                ).lower(),
                "state_status": current_value.state.status.lower(),
                "is_accepted": str(
                    bool(
                        entry.status in ACCEPTED_TREATMENT_STATUSES
                        and entry.acceptance_status == AcceptanceStatus.APPROVED,
                    ),
                ).lower(),
            },
        )
    elif isinstance(entry, VulnerabilityZeroRisk):
        new_group_index_key = keys.build_key(
            facet=GROUP_INDEX_METADATA,
            values={
                "group_name": current_value.group_name,
                "is_zero_risk": str(bool(entry.status in ZR_FILTER_STATUSES)).lower(),
                "state_status": current_value.state.status.lower(),
                "is_accepted": str(
                    bool(
                        current_value.treatment
                        and current_value.treatment.status in ACCEPTED_TREATMENT_STATUSES
                        and current_value.treatment.acceptance_status == AcceptanceStatus.APPROVED,
                    ),
                ).lower(),
            },
        )

    return new_group_index_key


def get_zr_index_key_gsi_6(current_value: Vulnerability) -> PrimaryKey:
    return keys.build_key(
        facet=NEW_ZR_INDEX_METADATA,
        values={
            "finding_id": current_value.finding_id,
            "vuln_id": current_value.id,
            "is_deleted": str(
                current_value.state.status is VulnerabilityStateStatus.DELETED,
            ).lower(),
            "is_released": str(current_value.state.status in RELEASED_FILTER_STATUSES).lower(),
            "is_zero_risk": str(
                bool(
                    current_value.zero_risk
                    and current_value.zero_risk.status in ZR_FILTER_STATUSES,
                ),
            ).lower(),
            "state_status": str(current_value.state.status.value).lower(),
            "verification_status": str(
                current_value.verification and current_value.verification.status.value,
            ).lower(),
        },
    )


def get_new_zr_index_key_gsi_6(
    current_value: Vulnerability,
    entry: VulnerabilityHistoricEntry,
) -> PrimaryKey | None:
    new_zr_index_key = None
    if isinstance(entry, VulnerabilityState):
        new_zr_index_key = keys.build_key(
            facet=NEW_ZR_INDEX_METADATA,
            values={
                "finding_id": current_value.finding_id,
                "vuln_id": current_value.id,
                "is_deleted": str(entry.status is VulnerabilityStateStatus.DELETED).lower(),
                "is_released": str(entry.status in RELEASED_FILTER_STATUSES).lower(),
                "is_zero_risk": str(
                    bool(
                        current_value.zero_risk
                        and current_value.zero_risk.status in ZR_FILTER_STATUSES,
                    ),
                ).lower(),
                "state_status": str(entry.status.value).lower(),
                "verification_status": str(
                    current_value.verification and current_value.verification.status.value,
                ).lower(),
            },
        )
    if isinstance(entry, VulnerabilityZeroRisk):
        new_zr_index_key = keys.build_key(
            facet=NEW_ZR_INDEX_METADATA,
            values={
                "finding_id": current_value.finding_id,
                "vuln_id": current_value.id,
                "is_deleted": str(
                    current_value.state.status is VulnerabilityStateStatus.DELETED,
                ).lower(),
                "is_released": str(current_value.state.status in RELEASED_FILTER_STATUSES).lower(),
                "is_zero_risk": str(entry.status in ZR_FILTER_STATUSES).lower(),
                "state_status": str(current_value.state.status.value).lower(),
                "verification_status": str(
                    current_value.verification and current_value.verification.status.value,
                ).lower(),
            },
        )
    if isinstance(entry, VulnerabilityVerification):
        new_zr_index_key = keys.build_key(
            facet=NEW_ZR_INDEX_METADATA,
            values={
                "finding_id": current_value.finding_id,
                "vuln_id": current_value.id,
                "is_deleted": str(
                    current_value.state.status is VulnerabilityStateStatus.DELETED,
                ).lower(),
                "is_released": str(current_value.state.status in RELEASED_FILTER_STATUSES).lower(),
                "is_zero_risk": str(
                    bool(
                        current_value.zero_risk
                        and current_value.zero_risk.status in ZR_FILTER_STATUSES,
                    ),
                ).lower(),
                "state_status": str(current_value.state.status.value).lower(),
                "verification_status": str(entry.status.value).lower(),
            },
        )

    return new_zr_index_key


FilterValue = str | float | list[str] | dict[str, str] | dict[str, float]
FilterRule = dict[str, FilterValue]


class VulnerabilityFiltersInput(TypedDict, total=False):
    assignees: list[str]
    closed_after: datetime
    closed_before: datetime
    reattack: str
    reported_after: datetime
    reported_before: datetime
    search: str
    severity_rating: str
    state: list[str]
    state_not: list[str]
    tags: list[str]
    tags_not: list[str]
    treatment: list[str]
    treatment_not: list[str]
    technique: list[str]
    technique_not: list[str]
    verification: list[str]
    verification_not: list[str]
    where: str
    zero_risk: list[str]
    zero_risk_not: list[str]


def set_states(states: list[str] | str | None) -> list[str]:
    if isinstance(states, str):
        result = [states]
    elif isinstance(states, list):
        result = states
    else:
        return []

    return [get_inverted_state_converted(value) for value in result]


def set_treatments(treatments: list[str] | str | None) -> list[str]:
    if isinstance(treatments, str):
        result = [treatments]
    elif isinstance(treatments, list):
        result = treatments
    else:
        return []

    return [get_inverted_treatment_converted(value) for value in result]


def set_conditions(filters: Item | None) -> VulnerabilityFiltersInput:
    if not filters:
        return {}

    where = filters.get("where")
    verification = filters.get("verification")
    verification_not = filters.get("verification_not")
    zero_risk = filters.get("zero_risk", [])
    if isinstance(zero_risk, str):
        zero_risk = [zero_risk]
    zero_risk_not = filters.get("zero_risk_not", [])
    closed_after = filters.get("closed_after")
    closed_before = filters.get("closed_before")
    reported_after = filters.get("reported_after")
    reported_before = filters.get("reported_before")
    severity_rating = filters.get("severity_rating")
    tags = filters.get("tags")
    tags_not = filters.get("tags_not")

    return VulnerabilityFiltersInput(
        state=set_states(filters.get("state", [])),
        state_not=set_states(filters.get("state_not", [])),
        treatment=set_treatments(filters.get("treatment", [])),
        treatment_not=set_treatments(filters.get("treatment_not", [])),
        technique=set_treatments(filters.get("technique", [])),
        technique_not=set_treatments(filters.get("technique_not", [])),
        zero_risk=zero_risk,
        zero_risk_not=zero_risk_not,
        **({"verification": verification} if verification else {}),
        **({"verification_not": verification_not} if verification_not else {}),
        **({"assignees": filters["assignees"]} if filters.get("assignees") else {}),
        **({"search": filters["search"]} if filters.get("search") else {}),
        **({"where": where} if where else {}),
        **({"closed_after": closed_after} if closed_after else {}),
        **({"closed_before": closed_before} if closed_before else {}),
        **({"reported_after": reported_after} if reported_after else {}),
        **({"reported_before": reported_before} if reported_before else {}),
        **({"severity_rating": severity_rating} if severity_rating else {}),
        **({"tags": tags} if tags else {}),
        **({"tags_not": tags_not} if tags_not else {}),
    )


async def get_vulns_from_opensearch(
    *,
    group_name: str,
    finding_id: str,
    after: str | None,
    first: int,
    sort_by: list[Item] | None,
    filters: Item | None,
) -> VulnerabilitiesConnection:
    conditions = set_conditions(filters)

    results = await SearchClient[VulnerabilityItem].search(
        get_search_params(
            group_name=group_name,
            finding_id=finding_id,
            first=first,
            after=after,
            sort_by=sort_by,
            conditions=conditions,
        )
    )

    vulnerabilities = tuple(format_vulnerability(result) for result in results.items)

    return VulnerabilitiesConnection(
        edges=tuple(
            VulnerabilityEdge(
                cursor=results.page_info.end_cursor,
                node=vulnerability,
            )
            for vulnerability in vulnerabilities
        ),
        page_info=results.page_info,
        total=results.total,
    )


def get_search_params(
    *,
    group_name: str,
    finding_id: str,
    after: str | None,
    first: int,
    sort_by: list[Item] | None,
    conditions: VulnerabilityFiltersInput,
) -> SearchParams:
    return SearchParams(
        index_value="vulns_index",
        limit=first or 10,
        wildcard_queries=_get_rules_wildcard(conditions),
        exact_filters=_get_rules_exact(group_name, conditions),
        must_filters=_get_rules_must(group_name, finding_id),
        must_not_filters=_get_rules_not_must(conditions),
        should_filters=_get_rules_or(conditions),
        minimum_should_match="100%",
        range_filters=_get_range_rules_must(conditions),
        after=(str(json.loads(after)[0]) if after and after[0] == "[" else None),
        sort_by=sort_by or [{"_id": {"order": "desc"}}],
        paginate=True,
    )


def _get_rules_wildcard(conditions: VulnerabilityFiltersInput) -> list[FilterRule]:
    rules_wildcard: list[FilterRule] = []

    if "where" in conditions:
        rules_wildcard.append({"state.where": "*" + conditions["where"] + "*"})

    if "search" in conditions:
        rules_wildcard.append({"state.where": "*" + conditions["search"] + "*"})

    return rules_wildcard


def _get_rules_exact(group_name: str, conditions: VulnerabilityFiltersInput) -> FilterRule:
    rules_exact: FilterRule = {}

    rules_exact["group_name"] = group_name

    if "tags" in conditions and len(conditions["tags"]) > 0:
        rules_exact["tags"] = conditions["tags"]

    return rules_exact


def _get_rules_must(group_name: str, finding_id: str) -> list[FilterRule]:
    rules_must: list[FilterRule] = []

    rules_must.append({"group_name": group_name})
    rules_must.append({"sk": f"FIN#{finding_id}"})

    return rules_must


def _get_rules_or(conditions: VulnerabilityFiltersInput) -> list[FilterRule]:
    rules_or: list[FilterRule] = []

    if "state" in conditions and len(conditions["state"]) > 0:
        rules_or.append({"state.status": " OR ".join(conditions["state"])})
    if "treatment" in conditions and len(conditions["treatment"]) > 0:
        rules_or.append({"treatment.status": " OR ".join(conditions["treatment"])})
    if "technique" in conditions and len(conditions["technique"]) > 0:
        rules_or.append({"technique": " OR ".join(conditions["technique"])})
    if "verification" in conditions and len(conditions["verification"]) > 0:
        rules_or.append(
            {"verification.status": " OR ".join(conditions["verification"])},
        )
    if "zero_risk" in conditions and len(conditions["zero_risk"]) > 0:
        rules_or.append({"zero_risk.status": " OR ".join(conditions["zero_risk"])})
    if "tags" in conditions and len(conditions["tags"]) > 0:
        rules_or.append({"tags": " OR ".join(conditions["tags"])})
    if "assignees" in conditions and len(conditions["assignees"]) > 0:
        rules_or.append({"treatment.assigned": " OR ".join(conditions["assignees"])})

    return rules_or


def _get_rules_not_must(conditions: VulnerabilityFiltersInput) -> list[FilterRule]:
    rules_not_must: list[FilterRule] = []

    rules_not_must.append({"state.status": "DELETED"})
    rules_not_must.append({"state.status": "MASKED"})

    for value in conditions["state_not"] if "state_not" in conditions else []:
        rules_not_must.append({"state.status": value})

    for value in conditions["treatment_not"] if "treatment_not" in conditions else []:
        rules_not_must.append({"treatment.status": value})

    for value in conditions["technique_not"] if "technique_not" in conditions else []:
        rules_not_must.append({"technique": value})

    for value in conditions["verification_not"] if "verification_not" in conditions else []:
        rules_not_must.append({"verification.status": value})

    for value in conditions["zero_risk_not"] if "zero_risk_not" in conditions else []:
        rules_not_must.append({"zero_risk.status": value})

    for value in conditions["tags_not"] if "tags_not" in conditions else []:
        rules_not_must.append({"tags": value})

    return rules_not_must


def _get_range_rules_must(conditions: VulnerabilityFiltersInput) -> list[FilterRule]:
    rules_range: list[FilterRule] = []
    if "closed_after" in conditions:
        rules_range.append(
            {
                "unreliable_indicators.unreliable_closing_date": {
                    "gt": str(conditions["closed_after"].date()),
                }
            },
        )

    if "closed_before" in conditions:
        rules_range.append(
            {
                "unreliable_indicators.unreliable_closing_date": {
                    "lt": str(conditions["closed_before"].date()),
                },
            },
        )

    if "reported_after" in conditions:
        rules_range.append(
            {
                "unreliable_indicators.unreliable_report_date": {
                    "gt": str(conditions["reported_after"].date()),
                },
            },
        )

    if "reported_before" in conditions:
        rules_range.append(
            {
                "unreliable_indicators.unreliable_report_date": {
                    "lt": str(conditions["reported_before"].date()),
                },
            },
        )

    if "severity_rating" in conditions:
        filter_params = {
            "CRITICAL": {"gte": 9.0},
            "HIGH": {"gte": 7.0, "lt": 9.0},
            "MEDIUM": {"gte": 4.0, "lt": 7.0},
            "LOW": {"gte": 0.1, "lt": 4.0},
            "NONE": {"lt": 0.1},
        }
        rules_range.append(
            {"severity_score.threat_score": filter_params[conditions["severity_rating"]]},
        )

    return rules_range
