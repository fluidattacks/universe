import logging
import logging.config
from decimal import (
    Decimal,
)

import simplejson as json
from boto3.dynamodb.conditions import (
    Attr,
)

from integrates.audit import AuditEvent, add_audit_event
from integrates.custom_exceptions import (
    ErrorUpdatingCredential,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.credentials.constants import (
    OWNER_INDEX_FACET,
)
from integrates.db_model.credentials.types import (
    Credentials,
    CredentialsMetadataToUpdate,
    CredentialsState,
)
from integrates.db_model.credentials.utils import (
    validate_secret,
)
from integrates.db_model.utils import (
    get_as_utc_iso_format,
    serialize,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.exceptions import (
    ConditionalCheckFailedException,
)
from integrates.settings.logger import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger(__name__)


async def update_credentials(
    *,
    current_value: Credentials,
    organization_id: str,
    credential_id: str,
    state: CredentialsState,
    metadata: CredentialsMetadataToUpdate = CredentialsMetadataToUpdate(),
) -> None:
    validate_secret(
        state,
        metadata.secret or current_value.secret,
    )
    key_structure = TABLE.primary_key
    credential_key = keys.build_key(
        facet=TABLE.facets["credentials_metadata"],
        values={
            "organization_id": organization_id,
            "id": credential_id,
        },
    )
    state_item = json.loads(json.dumps(state, default=serialize))
    metadata_item = {
        key: value
        for key, value in json.loads(
            json.dumps(metadata, default=serialize),
            parse_float=Decimal,
        ).items()
        if value is not None
    }
    gsi_2_index = TABLE.indexes["gsi_2"]
    gsi_2_key = keys.build_key(
        facet=OWNER_INDEX_FACET,
        values={
            "owner": state.owner,
            "id": credential_id,
        },
    )
    credential_item = {
        **metadata_item,
        "state": state_item,
        gsi_2_index.primary_key.partition_key: gsi_2_key.partition_key,
        gsi_2_index.primary_key.sort_key: gsi_2_key.sort_key,
    }
    try:
        await operations.update_item(
            condition_expression=(
                Attr(key_structure.partition_key).exists()
                & Attr("state.modified_date").eq(
                    get_as_utc_iso_format(current_value.state.modified_date),
                )
            ),
            item=credential_item,
            key=credential_key,
            table=TABLE,
        )
        add_audit_event(
            AuditEvent(
                action="UPDATE",
                author=state.modified_by,
                metadata=credential_item,
                object="Credential",
                object_id=credential_id,
            )
        )
    except ConditionalCheckFailedException as exc:
        raise ErrorUpdatingCredential() from exc
