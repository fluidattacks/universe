from decimal import (
    Decimal,
)

CVSS_V3_DEFAULT = "CVSS:3.1/AV:P/AC:H/PR:H/UI:R/S:U/C:N/I:N/A:N"
CVSS_V4_DEFAULT = "CVSS:4.0/AV:N/AC:L/AT:N/PR:N/UI:N/VC:N/VI:N/VA:N/SC:N/SI:N/SA:N"
DEFAULT_MAX_ACCEPTANCE_DAYS = 999
DEFAULT_MAX_SEVERITY = Decimal("10.0")
DEFAULT_MIN_SEVERITY = Decimal("0.0")
DEFAULT_VULNERABILITY_GRACE_PERIOD = 0
DEFAULT_INACTIVITY_PERIOD = 90
MACHINE_EMAIL = "machine@fluidattacks.com"
MIN_INACTIVITY_PERIOD = 21
POLICIES_FORMATTED = {
    "days_until_it_breaks": (
        "Number of days until vulnerabilities are considered technical debt "
        "and do not break the build"
    ),
    "inactivity_period": (
        "Number of days to remove a stakeholder from the organization due to inactivity"
    ),
    "max_acceptance_days": (
        "Maximum number of calendar days a finding can be temporarily accepted"
    ),
    "max_acceptance_severity": (
        "Maximum temporary CVSS score for the range within which a finding can be accepted"
    ),
    "min_breaking_severity": (
        "Minimum CVSS score of an open "
        "vulnerability for the DevSecOps agent "
        "to break the build in strict mode"
    ),
    "min_acceptance_severity": (
        "Minimum temporary CVSS score for the range within which a finding can be accepted"
    ),
    "vulnerability_grace_period": (
        "Grace period in days where newly "
        "reported vulnerabilities won't break the build (DevSecOps only)"
    ),
    "max_number_acceptances": ("Maximum number of times a finding can be temporarily accepted"),
}
