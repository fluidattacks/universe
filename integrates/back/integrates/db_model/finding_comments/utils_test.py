from integrates.db_model import TABLE
from integrates.db_model.finding_comments.types import FindingComment
from integrates.db_model.finding_comments.utils import (
    format_finding_comment_item,
    format_finding_comments,
)
from integrates.db_model.items import FindingCommentItem
from integrates.dynamodb import keys
from integrates.testing.fakers import FindingCommentFaker, FindingCommentItemFaker
from integrates.testing.utils import parametrize


@parametrize(
    args=["finding_comment"],
    cases=[
        [FindingCommentFaker()],
        [FindingCommentFaker(full_name=None)],
    ],
)
def test_format_finding_comments_item(
    finding_comment: FindingComment,
) -> None:
    primary_key = keys.build_key(
        facet=TABLE.facets["finding_comment"],
        values={
            "id": finding_comment.id,
            "finding_id": finding_comment.finding_id,
        },
    )
    item = format_finding_comment_item(finding_comment, primary_key=primary_key)
    assert item["comment_type"] == finding_comment.comment_type.value
    assert item["content"] == finding_comment.content
    assert item["email"] == finding_comment.email
    assert item["finding_id"] == finding_comment.finding_id
    assert item["id"] == finding_comment.id
    assert item["parent_id"] == finding_comment.parent_id


@parametrize(
    args=["item"],
    cases=[[FindingCommentItemFaker()]],
)
def test_format_finding_comments(
    item: FindingCommentItem,
) -> None:
    finding_comment = format_finding_comments(item)
    assert item["comment_type"] == finding_comment.comment_type.value
    assert item["content"] == finding_comment.content
    assert item["email"] == finding_comment.email
    assert item["finding_id"] == finding_comment.finding_id
    assert item["id"] == finding_comment.id
    assert item["parent_id"] == finding_comment.parent_id
