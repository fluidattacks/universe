from boto3.dynamodb.conditions import (
    Attr,
)

from integrates.audit import AuditEvent, add_audit_event
from integrates.custom_exceptions import (
    RepeatedComment,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.finding_comments.types import (
    FindingComment,
)
from integrates.db_model.finding_comments.utils import (
    format_finding_comment_item,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.exceptions import (
    ConditionalCheckFailedException,
)


async def add(*, finding_comment: FindingComment) -> None:
    key_structure = TABLE.primary_key
    primary_key = keys.build_key(
        facet=TABLE.facets["finding_comment"],
        values={
            "id": finding_comment.id,
            "finding_id": finding_comment.finding_id,
        },
    )
    item = format_finding_comment_item(finding_comment, primary_key=primary_key)

    condition_expression = Attr(key_structure.partition_key).not_exists()
    try:
        await operations.put_item(
            condition_expression=condition_expression,
            facet=TABLE.facets["finding_comment"],
            item=item,
            table=TABLE,
        )
        add_audit_event(
            AuditEvent(
                action="CREATE",
                author=finding_comment.email,
                metadata=item,
                object="FindingComment",
                object_id=finding_comment.id,
            )
        )
    except ConditionalCheckFailedException as ex:
        raise RepeatedComment() from ex
