from datetime import (
    datetime,
)
from typing import (
    NamedTuple,
)

from integrates.db_model.finding_comments.enums import (
    CommentType,
)


class FindingComment(NamedTuple):
    comment_type: CommentType
    content: str
    creation_date: datetime
    email: str
    finding_id: str
    id: str
    parent_id: str
    full_name: str | None = None


class FindingCommentsRequest(NamedTuple):
    comment_type: CommentType
    finding_id: str
