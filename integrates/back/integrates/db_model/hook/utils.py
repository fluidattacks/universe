from datetime import (
    datetime,
)
from typing import cast

import simplejson as json

from integrates.class_types.types import (
    Item,
)
from integrates.db_model.hook.enums import (
    HookEvent,
    HookStatus,
)
from integrates.db_model.hook.types import (
    GroupHook,
    GroupHookPayload,
    HookState,
)
from integrates.db_model.items import HookItem
from integrates.db_model.utils import (
    serialize,
)
from integrates.dynamodb.types import PrimaryKey


def format_group_hook(item: HookItem) -> GroupHook:
    return GroupHook(
        id=item["id"],
        hook_events=cast(set[HookEvent], set(item["hook_events"])),
        group_name=item["group_name"],
        entry_point=item["entry_point"],
        name=item.get("name", "Name"),
        token=item["token"],
        token_header=item["token_header"],
        state=HookState(
            modified_by=item["state"]["modified_by"],
            modified_date=datetime.fromisoformat(item["state"]["modified_date"]),
            status=HookStatus[item["state"]["status"]],
        ),
    )


def format_group_hook_payload(item: Item) -> GroupHookPayload:
    return GroupHookPayload(
        hook_events=set(item["hook_events"]),
        entry_point=item["entry_point"],
        name=item.get("name", "Name"),
        token=item["token"],
        token_header=item["token_header"],
    )


def format_group_hook_item_to_update(group_hook: GroupHookPayload, state: HookState) -> Item:
    return {
        "entry_point": group_hook.entry_point,
        "name": group_hook.name,
        "token": group_hook.token,
        "token_header": group_hook.token_header,
        "hook_events": group_hook.hook_events,
        "state": json.loads(json.dumps(state, default=serialize)),
    }


def format_new_group_hook_item(*, group_hook: GroupHook, primary_key: PrimaryKey) -> HookItem:
    return {
        "pk": primary_key.partition_key,
        "sk": primary_key.sort_key,
        "id": group_hook.id,
        "group_name": group_hook.group_name,
        "entry_point": group_hook.entry_point,
        "name": group_hook.name,
        "token": group_hook.token,
        "token_header": group_hook.token_header,
        "hook_events": cast(set[str], group_hook.hook_events),
        "state": json.loads(json.dumps(group_hook.state, default=serialize)),
    }
