from datetime import (
    datetime,
)

from boto3.dynamodb.conditions import (
    Attr,
)

from integrates.audit import AuditEvent, add_audit_event
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.hook.enums import (
    HookStatus,
)
from integrates.db_model.hook.types import (
    GroupHook,
    GroupHookPayload,
    HookState,
)
from integrates.db_model.hook.utils import (
    format_new_group_hook_item,
)
from integrates.dynamodb import (
    keys,
    operations,
)


async def add_hook(
    group_name: str,
    user_email: str,
    hook: GroupHookPayload,
    hook_id: str,
    modified_date: datetime,
) -> None:
    key_structure = TABLE.primary_key
    primary_key = keys.build_key(
        facet=TABLE.facets["hook_metadata"],
        values={
            "id": hook_id,
            "group_name": group_name,
        },
    )
    state = HookState(
        modified_by=user_email,
        modified_date=modified_date,
        status=HookStatus.ACTIVE,
    )
    new_hook = GroupHook(
        id=hook_id,
        group_name=group_name,
        hook_events=hook.hook_events,
        entry_point=hook.entry_point,
        name=hook.name,
        token=hook.token,
        token_header=hook.token_header,
        state=state,
    )

    item = format_new_group_hook_item(group_hook=new_hook, primary_key=primary_key)

    condition_expression = Attr(key_structure.partition_key).not_exists()

    await operations.put_item(
        condition_expression=condition_expression,
        facet=TABLE.facets["hook_metadata"],
        item=item,
        table=TABLE,
    )
    add_audit_event(
        AuditEvent(
            action="CREATE",
            author=user_email,
            metadata=item,
            object="Hook",
            object_id=hook_id,
        )
    )
