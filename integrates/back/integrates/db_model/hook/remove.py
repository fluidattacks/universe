from integrates.audit import AuditEvent, add_audit_event
from integrates.db_model import (
    TABLE,
)
from integrates.dynamodb import (
    keys,
    operations,
)


async def remove(group_name: str, hook_id: str) -> None:
    primary_key = keys.build_key(
        facet=TABLE.facets["hook_metadata"],
        values={
            "id": hook_id,
            "group_name": group_name,
        },
    )
    await operations.delete_item(key=primary_key, table=TABLE)
    add_audit_event(
        AuditEvent(
            action="DELETE",
            author="unknown",
            metadata={},
            object="Hook",
            object_id=hook_id,
        )
    )
