from integrates.db_model import TABLE
from integrates.db_model.compliance.types import ComplianceUnreliableIndicators
from integrates.db_model.compliance.utils import (
    format_compliance_standard,
    format_unreliable_indicators,
    format_unreliable_indicators_item,
)
from integrates.db_model.items import ComplianceStandardItem, ComplianceUnreliableIndicatorsItem
from integrates.dynamodb import keys
from integrates.testing.fakers import ComplianceUnreliableIndicatorsFaker
from integrates.testing.utils import parametrize


@parametrize(
    args=["indicators"],
    cases=[
        [ComplianceUnreliableIndicatorsFaker(n_standards=0)],
        [ComplianceUnreliableIndicatorsFaker(n_standards=2)],
        [ComplianceUnreliableIndicatorsFaker(n_standards=5)],
        [ComplianceUnreliableIndicatorsFaker(n_standards=10)],
        [ComplianceUnreliableIndicatorsFaker(n_standards=20)],
    ],
)
def test_format_unreliable_indicators_item(
    indicators: ComplianceUnreliableIndicators,
) -> None:
    primary_key = keys.build_key(
        facet=TABLE.facets["compliance_unreliable_indicators"],
        values={},
    )
    item = format_unreliable_indicators_item(indicators, primary_key)
    if indicators.standards:
        assert len(item["standards"]) == len(indicators.standards)


@parametrize(
    args=["item"],
    cases=[
        [
            {
                "worst_organization_compliance_level": 1.00,
                "standard_name": "bsimm",
                "avg_organization_compliance_level": 1.00,
                "best_organization_compliance_level": 1.00,
            }
        ],
        [
            {
                "worst_organization_compliance_level": 0.71,
                "standard_name": "capec",
                "avg_organization_compliance_level": 0.86,
                "best_organization_compliance_level": 1.00,
            }
        ],
        [
            {
                "worst_organization_compliance_level": 0.93,
                "standard_name": "cis",
                "avg_organization_compliance_level": 0.96,
                "best_organization_compliance_level": 1.00,
            }
        ],
    ],
)
def test_format_compliance_standard(item: ComplianceStandardItem) -> None:
    standard = format_compliance_standard(item=item)
    assert standard._asdict() == item


@parametrize(
    args=["item"],
    cases=[
        [
            {
                "sk": "COMPLIANCE#UNRELIABLEINDICATOR",
                "pk": "COMPLIANCE#UNRELIABLEINDICATOR",
                "standards": [
                    {
                        "worst_organization_compliance_level": 1.00,
                        "standard_name": "bsimm",
                        "avg_organization_compliance_level": 1.00,
                        "best_organization_compliance_level": 1.00,
                    },
                    {
                        "worst_organization_compliance_level": 0.71,
                        "standard_name": "capec",
                        "avg_organization_compliance_level": 0.86,
                        "best_organization_compliance_level": 1.00,
                    },
                    {
                        "worst_organization_compliance_level": 0.93,
                        "standard_name": "cis",
                        "avg_organization_compliance_level": 0.96,
                        "best_organization_compliance_level": 1.00,
                    },
                ],
            }
        ],
    ],
)
def test_format_unreliable_indicators(
    item: ComplianceUnreliableIndicatorsItem,
) -> None:
    indicators = format_unreliable_indicators(item=item)
    if indicators.standards is not None:
        assert len(indicators.standards) == len(item["standards"])
