from integrates.db_model import (
    TABLE,
)
from integrates.db_model.compliance.types import (
    ComplianceUnreliableIndicators,
)
from integrates.db_model.compliance.utils import (
    format_unreliable_indicators_item,
)
from integrates.dynamodb import (
    keys,
    operations,
)


async def update_unreliable_indicators(
    *,
    indicators: ComplianceUnreliableIndicators,
) -> None:
    primary_key = keys.build_key(
        facet=TABLE.facets["compliance_unreliable_indicators"],
        values={},
    )
    unreliable_indicators = format_unreliable_indicators_item(indicators, primary_key)
    await operations.update_item(
        item={
            key: value for key, value in unreliable_indicators.items() if key not in ["pk", "sk"]
        },
        key=primary_key,
        table=TABLE,
    )
