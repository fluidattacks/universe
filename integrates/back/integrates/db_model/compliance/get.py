from collections.abc import (
    Iterable,
)

from aiodataloader import (
    DataLoader,
)
from aioextensions import (
    collect,
)
from boto3.dynamodb.conditions import (
    Key,
)

from integrates.db_model import (
    TABLE,
)
from integrates.db_model.compliance.types import (
    ComplianceUnreliableIndicators,
)
from integrates.db_model.compliance.utils import (
    format_unreliable_indicators,
)
from integrates.db_model.items import (
    ComplianceUnreliableIndicatorsItem,
)
from integrates.dynamodb import (
    keys,
    operations,
)


async def _get_compliance_unreliable_indicators() -> ComplianceUnreliableIndicators:
    key_structure = TABLE.primary_key
    primary_key = keys.build_key(
        facet=TABLE.facets["compliance_unreliable_indicators"],
        values={},
    )
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).eq(primary_key.sort_key)
        ),
        facets=(TABLE.facets["compliance_unreliable_indicators"],),
        table=TABLE,
    )
    if not response.items:
        return ComplianceUnreliableIndicators()

    raw_item: ComplianceUnreliableIndicatorsItem = {
        "pk": response.items[0]["pk"],
        "sk": response.items[0]["sk"],
        "standards": response.items[0]["standards"],
    }
    return format_unreliable_indicators(raw_item)


class ComplianceUnreliableIndicatorsLoader(DataLoader[str, ComplianceUnreliableIndicators]):
    async def batch_load_fn(self, ids: Iterable[str]) -> list[ComplianceUnreliableIndicators]:
        return list(
            await collect(
                tuple(_get_compliance_unreliable_indicators() for _ in ids),
                workers=32,
            ),
        )
