from integrates.db_model.compliance.types import (
    ComplianceStandard,
    ComplianceUnreliableIndicators,
)
from integrates.db_model.items import (
    ComplianceStandardItem,
    ComplianceUnreliableIndicatorsItem,
)
from integrates.dynamodb.types import (
    PrimaryKey,
)


def format_compliance_standard(
    item: ComplianceStandardItem,
) -> ComplianceStandard:
    return ComplianceStandard(
        avg_organization_compliance_level=item["avg_organization_compliance_level"],
        best_organization_compliance_level=item["best_organization_compliance_level"],
        standard_name=item["standard_name"],
        worst_organization_compliance_level=item["worst_organization_compliance_level"],
    )


def format_compliance_standard_item(
    standard: ComplianceStandard,
) -> ComplianceStandardItem:
    return {
        "avg_organization_compliance_level": (standard.avg_organization_compliance_level),
        "best_organization_compliance_level": (standard.best_organization_compliance_level),
        "standard_name": standard.standard_name,
        "worst_organization_compliance_level": (standard.worst_organization_compliance_level),
    }


def format_unreliable_indicators(
    item: ComplianceUnreliableIndicatorsItem,
) -> ComplianceUnreliableIndicators:
    return ComplianceUnreliableIndicators(
        standards=[
            format_compliance_standard(standard_compliance)
            for standard_compliance in item["standards"]
        ]
        if "standards" in item and item["standards"] is not None
        else None,
    )


def format_unreliable_indicators_item(
    indicators: ComplianceUnreliableIndicators,
    key: PrimaryKey,
) -> ComplianceUnreliableIndicatorsItem:
    item: ComplianceUnreliableIndicatorsItem = {
        "pk": key.partition_key,
        "sk": key.sort_key,
    }
    if indicators.standards:
        item["standards"] = [
            format_compliance_standard_item(standard) for standard in indicators.standards
        ]
    return item
