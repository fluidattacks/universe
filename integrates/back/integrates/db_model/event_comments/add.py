import simplejson as json
from boto3.dynamodb.conditions import (
    Attr,
)

from integrates.audit import AuditEvent, add_audit_event
from integrates.custom_exceptions import (
    RepeatedComment,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.event_comments.types import (
    EventComment,
)
from integrates.db_model.utils import (
    serialize,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.exceptions import (
    ConditionalCheckFailedException,
)


async def add(*, event_comment: EventComment) -> None:
    key_structure = TABLE.primary_key
    primary_key = keys.build_key(
        facet=TABLE.facets["event_comment"],
        values={
            "id": event_comment.id,
            "event_id": event_comment.event_id,
            "group_name": event_comment.group_name,
        },
    )
    item = {
        key_structure.partition_key: primary_key.partition_key,
        key_structure.sort_key: primary_key.sort_key,
        **json.loads(json.dumps(event_comment, default=serialize)),
    }
    condition_expression = Attr(key_structure.partition_key).not_exists()
    try:
        await operations.put_item(
            condition_expression=condition_expression,
            facet=TABLE.facets["event_comment"],
            item=item,
            table=TABLE,
        )
        add_audit_event(
            AuditEvent(
                action="CREATE",
                author=event_comment.email,
                metadata=item,
                object="EventComment",
                object_id=event_comment.id,
            )
        )
    except ConditionalCheckFailedException as ex:
        raise RepeatedComment() from ex
