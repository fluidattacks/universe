import simplejson
from boto3.dynamodb.conditions import (
    Attr,
)

from integrates.audit import AuditEvent, add_audit_event
from integrates.custom_exceptions import (
    TrialRestriction,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.trials.types import (
    Trial,
)
from integrates.db_model.utils import (
    serialize,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.exceptions import (
    ConditionalCheckFailedException,
)


async def add(*, trial: Trial) -> None:
    key_structure = TABLE.primary_key
    key = keys.build_key(
        facet=TABLE.facets["trial_metadata"],
        values={"all": "all", "email": trial.email},
    )
    item = {
        key_structure.partition_key: key.partition_key,
        key_structure.sort_key: key.sort_key,
        **simplejson.loads(simplejson.dumps(trial, default=serialize)),
    }

    try:
        await operations.put_item(
            condition_expression=(Attr(key_structure.partition_key).not_exists()),
            facet=TABLE.facets["trial_metadata"],
            item=item,
            table=TABLE,
        )
        add_audit_event(
            AuditEvent(
                action="CREATE",
                author=trial.email,
                metadata=item,
                object="Trial",
                object_id=trial.email,
            )
        )
    except ConditionalCheckFailedException as ex:
        raise TrialRestriction() from ex
