import simplejson
from boto3.dynamodb.conditions import (
    Attr,
)

from integrates.audit import AuditEvent, add_audit_event
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.trials.types import (
    TrialMetadataToUpdate,
)
from integrates.db_model.utils import (
    serialize,
)
from integrates.dynamodb import (
    keys,
    operations,
)


async def update_metadata(
    *,
    email: str,
    metadata: TrialMetadataToUpdate,
) -> None:
    key_structure = TABLE.primary_key
    key = keys.build_key(
        facet=TABLE.facets["trial_metadata"],
        values={"all": "all", "email": email},
    )
    metadata_dict = simplejson.loads(simplejson.dumps(metadata, default=serialize))
    item = {k: v for k, v in metadata_dict.items() if v is not None}

    await operations.update_item(
        condition_expression=Attr(key_structure.partition_key).exists(),
        item=item,
        key=key,
        table=TABLE,
    )
    add_audit_event(
        AuditEvent(
            action="UPDATE",
            author=email,
            metadata=item,
            object="Trial",
            object_id=email,
        )
    )
