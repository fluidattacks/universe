from integrates.custom_exceptions import EventAlreadyCreated
from integrates.db_model.events.add import (
    add,
)
from integrates.db_model.events.get import _get_event
from integrates.db_model.events.types import (
    Event,
    EventRequest,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import EventFaker
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

ROOT_ID = "900e7f46-7071-4111-b582-1194046b8c59"
EVENT_ID = "33b0a756-4bca-4c0b-9366-db6089c2927b"


@parametrize(
    args=["event"],
    cases=[
        [EventFaker()],
    ],
)
@mocks()
async def test_add_event(*, event: Event) -> None:
    await add(event=event)
    fetched_event = await _get_event(
        request=EventRequest(event_id=event.id, group_name=event.group_name)
    )
    assert fetched_event == event


@parametrize(
    args=["event"],
    cases=[
        [EventFaker(root_id=ROOT_ID, id=EVENT_ID)],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(events=[EventFaker(root_id=ROOT_ID, id=EVENT_ID)]),
    )
)
async def test_add_event_already_exists(*, event: Event) -> None:
    with raises(EventAlreadyCreated):
        await add(event=event)
