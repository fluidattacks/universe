from boto3.dynamodb.conditions import (
    Key,
)

from integrates.archive.domain import (
    archive_event,
)
from integrates.audit import AuditEvent, add_audit_event
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.utils import (
    archive,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.types import (
    PrimaryKey,
)


async def remove(*, event_id: str) -> None:
    primary_key = keys.build_key(
        facet=TABLE.facets["event_metadata"],
        values={
            "id": event_id,
        },
    )
    key_structure = TABLE.primary_key
    condition_expression = Key(key_structure.partition_key).eq(primary_key.partition_key)
    response = await operations.query(
        condition_expression=condition_expression,
        facets=(
            TABLE.facets["event_metadata"],
            TABLE.facets["event_historic_state"],
        ),
        table=TABLE,
    )
    if not response.items:
        return

    await archive(response.items, archive_event)
    keys_to_delete = set(
        PrimaryKey(
            partition_key=item[TABLE.primary_key.partition_key],
            sort_key=item[TABLE.primary_key.sort_key],
        )
        for item in response.items
    )
    await operations.batch_delete_item(
        keys=tuple(keys_to_delete),
        table=TABLE,
    )
    add_audit_event(
        AuditEvent(
            action="DELETE",
            author="unknown",
            metadata={},
            object="Event",
            object_id=event_id,
        )
    )
