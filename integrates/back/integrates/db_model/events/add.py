from integrates.audit import AuditEvent, add_audit_event
from integrates.class_types.types import BaseTypedDict
from integrates.custom_exceptions import (
    EventAlreadyCreated,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.events.types import (
    Event,
)
from integrates.db_model.events.utils import (
    format_event_item,
    format_event_state_item,
    get_gsi_2_key,
)
from integrates.db_model.utils import (
    get_as_utc_iso_format,
)
from integrates.dynamodb import (
    keys,
    operations,
)


async def add(*, event: Event) -> None:
    items: list[BaseTypedDict] = []
    primary_key = keys.build_key(
        facet=TABLE.facets["event_metadata"],
        values={
            "id": event.id,
            "name": event.group_name,
        },
    )
    gsi_2_key = get_gsi_2_key(event.group_name, event.state)

    item_in_db = await operations.get_item(
        facets=(TABLE.facets["event_metadata"],),
        key=primary_key,
        table=TABLE,
    )
    if item_in_db:
        raise EventAlreadyCreated.new()

    item = format_event_item(event, primary_key=primary_key, gsi_2_key=gsi_2_key)
    items.append(item)

    state_key = keys.build_key(
        facet=TABLE.facets["event_historic_state"],
        values={
            "id": event.id,
            "iso8601utc": get_as_utc_iso_format(event.state.modified_date),
        },
    )
    historic_state_item = format_event_state_item(event.state, state_key=state_key)
    items.append(historic_state_item)

    await operations.batch_put_item(items=tuple(items), table=TABLE)
    add_audit_event(
        AuditEvent(
            action="CREATE",
            author=event.created_by,
            metadata=item,
            object="Event",
            object_id=event.id,
        )
    )
