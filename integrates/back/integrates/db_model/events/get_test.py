from collections.abc import Iterable
from datetime import timedelta

from integrates.dataloaders import get_new_context
from integrates.db_model.events.enums import EventStateStatus
from integrates.db_model.events.types import EventRequest, GroupEventsRequest
from integrates.db_model.vulnerabilities.enums import VulnerabilityVerificationStatus
from integrates.testing.aws import EventHistoricStates, IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    EventFaker,
    EventStateFaker,
    VulnerabilityFaker,
    VulnerabilityVerificationFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize

ROOT_ID = "900e7f46-7071-4111-b582-1194046b8c59"
EVENT_ID = "33b0a756-4bca-4c0b-9366-db6089c2927b"
GROUP_NAME = "unittesting"
EVENT_ID = "418900971"

DEFAULT_EVENT = EventFaker(
    root_id=ROOT_ID,
    id=EVENT_ID,
    state=EventStateFaker(
        status=EventStateStatus.CREATED,
    ),
)


@parametrize(
    args=["requests"],
    cases=[[[GroupEventsRequest(group_name="group1"), GroupEventsRequest(group_name="group2")]]],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            events=[
                EventFaker(group_name="group1", id="1"),
                EventFaker(group_name="group1", id="2"),
                EventFaker(group_name="group1", id="3"),
                EventFaker(group_name="group1", id="4"),
                EventFaker(group_name="group2", id="5"),
                EventFaker(group_name="group2", id="6"),
            ],
        ),
    )
)
async def test_get_group_events(requests: Iterable[GroupEventsRequest]) -> None:
    loaders = get_new_context()
    events = await loaders.group_events.load_many(requests)
    assert len(events) == 2
    assert len(events[0]) == 4
    assert len(events[1]) == 2


@parametrize(
    args=["req"],
    cases=[
        [GroupEventsRequest(group_name=GROUP_NAME)],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            events=[DEFAULT_EVENT],
            vulnerabilities=[
                VulnerabilityFaker(
                    id="1",
                    verification=VulnerabilityVerificationFaker(
                        status=VulnerabilityVerificationStatus.ON_HOLD,
                        event_id=EVENT_ID,
                    ),
                ),
                VulnerabilityFaker(
                    id="2",
                    verification=VulnerabilityVerificationFaker(
                        status=VulnerabilityVerificationStatus.ON_HOLD,
                        event_id=EVENT_ID,
                    ),
                ),
            ],
        ),
    )
)
async def test_get_event_with_n_holds(req: GroupEventsRequest) -> None:
    loaders = get_new_context()
    events = await loaders.group_events.load(req)
    for event in events:
        assert event is not None
        assert event.n_holds == 2


@parametrize(
    args=["req"],
    cases=[
        [EventRequest(event_id=EVENT_ID, group_name=GROUP_NAME)],
    ],
)
@mocks()
async def test_get_none_events(req: EventRequest) -> None:
    loaders = get_new_context()
    response = await loaders.event.load(req)
    assert response is None


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            events=[DEFAULT_EVENT],
            event_historic_states=[
                EventHistoricStates(
                    event=DEFAULT_EVENT,
                    states=[
                        EventStateFaker(
                            modified_date=DEFAULT_EVENT.created_date,
                            status=EventStateStatus.CREATED,
                        ),
                        EventStateFaker(
                            modified_date=DEFAULT_EVENT.created_date + timedelta(days=2),
                            status=EventStateStatus.OPEN,
                        ),
                    ],
                )
            ],
        ),
    )
)
@parametrize(
    args=["req"],
    cases=[
        [EventRequest(event_id=EVENT_ID, group_name=GROUP_NAME)],
    ],
)
async def test_get_event_historic_state(req: EventRequest) -> None:
    # increases coverage but len and status of historic states should be asserted
    # cannot be done until streams is enabled for mocks
    loaders = get_new_context()
    response = await loaders.event_historic_state.load(req)
    assert response is not None
