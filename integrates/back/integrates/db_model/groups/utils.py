from datetime import (
    datetime,
)

from integrates.db_model.groups.constants import (
    GROUP_NAME_PREFIX,
)
from integrates.db_model.groups.enums import (
    GroupLanguage,
    GroupManaged,
    GroupService,
    GroupStateJustification,
    GroupStateStatus,
    GroupSubscriptionType,
    GroupTier,
)
from integrates.db_model.groups.types import (
    Group,
    GroupAzureIssues,
    GroupFile,
    GroupGitLabIssues,
    GroupState,
    GroupTreatmentSummary,
    GroupUnreliableIndicators,
    UnfulfilledStandard,
)
from integrates.db_model.items import (
    GroupAzureIssuesItem,
    GroupFileItem,
    GroupGitLabIssuesItem,
    GroupItem,
    GroupStateItem,
    GroupTreatmentSummaryItem,
    GroupUnreliableIndicatorsItem,
)
from integrates.db_model.organizations.utils import (
    add_org_id_prefix,
    format_policies,
)
from integrates.db_model.types import (
    CodeLanguage,
)
from integrates.db_model.utils import (
    get_first_day_iso_date,
)


def format_files(files: list[GroupFileItem]) -> list[GroupFile]:
    return [
        GroupFile(
            description=file["description"],
            file_name=file["file_name"],
            modified_by=file["modified_by"],
            modified_date=datetime.fromisoformat(file["modified_date"])
            if file.get("modified_date")
            else None,
        )
        for file in files
    ]


def format_azure_issues(azure_issues: GroupAzureIssuesItem | None) -> GroupAzureIssues | None:
    if azure_issues is None:
        return None
    return GroupAzureIssues(
        assigned_to=azure_issues.get("assigned_to"),
        azure_organization=azure_issues.get("azure_organization"),
        azure_project=azure_issues.get("azure_project"),
        connection_date=(
            datetime.fromisoformat(azure_issues["connection_date"])
            if "connection_date" in azure_issues
            else None
        ),
        issue_automation_enabled=azure_issues.get("issue_automation_enabled", False),
        redirect_uri=azure_issues["redirect_uri"],
        refresh_token=azure_issues["refresh_token"],
        tags=azure_issues.get("tags"),
    )


def format_gitlab_issues(
    gitlab_issues: GroupGitLabIssuesItem | None,
) -> GroupGitLabIssues | None:
    if gitlab_issues is None:
        return None
    return GroupGitLabIssues(
        assignee_ids=gitlab_issues.get("assignee_ids"),
        connection_date=(
            datetime.fromisoformat(gitlab_issues["connection_date"])
            if "connection_date" in gitlab_issues
            else None
        ),
        redirect_uri=gitlab_issues["redirect_uri"],
        refresh_token=gitlab_issues["refresh_token"],
        gitlab_project=gitlab_issues.get("gitlab_project"),
        issue_automation_enabled=gitlab_issues.get("issue_automation_enabled", False),
        labels=gitlab_issues.get("labels"),
    )


def format_group(item: GroupItem) -> Group:
    return Group(
        created_by=item["created_by"],
        created_date=datetime.fromisoformat(item["created_date"]),
        agent_token=item.get("agent_token"),
        azure_issues=format_azure_issues(item.get("azure_issues")),
        business_id=item.get("business_id"),
        business_name=item.get("business_name"),
        context=item.get("context"),
        description=item["description"],
        disambiguation=item.get("disambiguation"),
        files=format_files(item["files"]) if item.get("files") else None,
        gitlab_issues=format_gitlab_issues(item.get("gitlab_issues")),
        language=GroupLanguage[item["language"]],
        name=item["name"],
        organization_id=add_org_id_prefix(item["organization_id"]),
        policies=format_policies(item["policies"]) if item.get("policies") else None,
        sprint_duration=int(item.get("sprint_duration", 1)),
        sprint_start_date=datetime.fromisoformat(item["sprint_start_date"])
        if item.get("sprint_start_date")
        else get_first_day_iso_date(),
        state=format_state(item["state"]),
    )


def format_unreliable_indicators(item: GroupUnreliableIndicatorsItem) -> GroupUnreliableIndicators:
    return GroupUnreliableIndicators(
        closed_vulnerabilities=int(item["closed_vulnerabilities"])
        if "closed_vulnerabilities" in item
        else None,
        code_languages=[
            CodeLanguage(language=language["language"], loc=int(language["loc"]))
            for language in item["code_languages"]
        ]
        if "code_languages" in item
        else None,
        exposed_over_time_cvssf=item.get("exposed_over_time_cvssf"),
        exposed_over_time_month_cvssf=item.get("exposed_over_time_month_cvssf"),
        exposed_over_time_year_cvssf=item.get("exposed_over_time_year_cvssf"),
        last_closed_vulnerability_days=int(item["last_closed_vulnerability_days"])
        if "last_closed_vulnerability_days" in item
        else None,
        last_closed_vulnerability_finding=item.get("last_closed_vulnerability_finding"),
        max_open_severity=item.get("max_open_severity"),
        max_open_severity_finding=item.get("max_open_severity_finding"),
        max_severity=item.get("max_severity"),
        mean_remediate=item.get("mean_remediate"),
        mean_remediate_critical_severity=item.get("mean_remediate_critical_severity"),
        mean_remediate_high_severity=item.get("mean_remediate_high_severity"),
        mean_remediate_low_severity=item.get("mean_remediate_low_severity"),
        mean_remediate_medium_severity=item.get("mean_remediate_medium_severity"),
        nofluid_quantity=int(item["nofluid_quantity"]) if "nofluid_quantity" in item else None,
        open_findings=int(item["open_findings"]) if "open_findings" in item else None,
        open_vulnerabilities=int(item["open_vulnerabilities"])
        if "open_vulnerabilities" in item
        else None,
        remediated_over_time=item.get("remediated_over_time"),
        remediated_over_time_30=item.get("remediated_over_time_30"),
        remediated_over_time_90=item.get("remediated_over_time_90"),
        remediated_over_time_cvssf=item.get("remediated_over_time_cvssf"),
        remediated_over_time_cvssf_30=item.get("remediated_over_time_cvssf_30"),
        remediated_over_time_cvssf_90=item.get("remediated_over_time_cvssf_90"),
        remediated_over_time_month=item.get("remediated_over_time_month"),
        remediated_over_time_month_cvssf=item.get("remediated_over_time_month_cvssf"),
        remediated_over_time_year=item.get("remediated_over_time_year"),
        remediated_over_time_year_cvssf=item.get("remediated_over_time_year_cvssf"),
        treatment_summary=format_treatment_summary(item["treatment_summary"])
        if item.get("treatment_summary")
        else None,
        unfulfilled_standards=[
            UnfulfilledStandard(
                name=standard["name"],
                unfulfilled_requirements=standard["unfulfilled_requirements"],
            )
            for standard in item["unfulfilled_standards"]
        ]
        if "unfulfilled_standards" in item
        else None,
    )


def format_unreliable_indicators_item(
    indicators: GroupUnreliableIndicators,
) -> GroupUnreliableIndicatorsItem:
    item = {
        "closed_vulnerabilities": indicators.closed_vulnerabilities,
        "code_languages": [
            format_code_language(code_language) for code_language in indicators.code_languages
        ]
        if indicators.code_languages
        else None,
        "exposed_over_time_cvssf": indicators.exposed_over_time_cvssf,
        "exposed_over_time_month_cvssf": indicators.exposed_over_time_month_cvssf,
        "exposed_over_time_year_cvssf": indicators.exposed_over_time_year_cvssf,
        "last_closed_vulnerability_days": indicators.last_closed_vulnerability_days,
        "last_closed_vulnerability_finding": indicators.last_closed_vulnerability_finding,
        "max_open_severity": indicators.max_open_severity,
        "max_open_severity_finding": indicators.max_open_severity_finding,
        "max_severity": indicators.max_severity,
        "mean_remediate": indicators.mean_remediate,
        "mean_remediate_critical_severity": indicators.mean_remediate_critical_severity,
        "mean_remediate_high_severity": indicators.mean_remediate_high_severity,
        "mean_remediate_low_severity": indicators.mean_remediate_low_severity,
        "mean_remediate_medium_severity": indicators.mean_remediate_medium_severity,
        "nofluid_quantity": indicators.nofluid_quantity,
        "open_findings": indicators.open_findings,
        "open_vulnerabilities": indicators.open_vulnerabilities,
        "remediated_over_time": indicators.remediated_over_time,
        "remediated_over_time_30": indicators.remediated_over_time_30,
        "remediated_over_time_90": indicators.remediated_over_time_90,
        "remediated_over_time_cvssf": indicators.remediated_over_time_cvssf,
        "remediated_over_time_cvssf_30": indicators.remediated_over_time_cvssf_30,
        "remediated_over_time_cvssf_90": indicators.remediated_over_time_cvssf_90,
        "remediated_over_time_month": indicators.remediated_over_time_month,
        "remediated_over_time_month_cvssf": indicators.remediated_over_time_month_cvssf,
        "remediated_over_time_year": indicators.remediated_over_time_year,
        "remediated_over_time_year_cvssf": indicators.remediated_over_time_year_cvssf,
        "treatment_summary": format_treatment_summary_item(indicators.treatment_summary)
        if indicators.treatment_summary
        else None,
        "unfulfilled_standards": [
            format_unfulfilled_standard_item(unfulfilled_standard)
            for unfulfilled_standard in indicators.unfulfilled_standards
        ]
        if indicators.unfulfilled_standards
        else None,
    }
    item = {key: value for key, value in item.items() if value is not None}
    return item  # type: ignore[return-value]


def format_state_managed(managed: bool | str) -> GroupManaged:
    if not managed:
        return GroupManaged.NOT_MANAGED
    if managed is True:
        return GroupManaged.MANAGED
    return GroupManaged[managed]


def format_state(state: GroupStateItem) -> GroupState:
    return GroupState(
        comments=state.get("comments"),
        has_essential=state["has_essential"],
        has_advanced=state["has_advanced"],
        managed=format_state_managed(state["managed"]),
        justification=GroupStateJustification[state["justification"]]
        if state.get("justification")
        else None,
        modified_by=state["modified_by"],
        modified_date=datetime.fromisoformat(state["modified_date"]),
        payment_id=state["payment_id"] if state.get("payment_id") else None,
        pending_deletion_date=datetime.fromisoformat(state["pending_deletion_date"])
        if state.get("pending_deletion_date")
        else None,
        service=GroupService[state["service"]] if state.get("service") else None,
        status=GroupStateStatus[state["status"]],
        tags=set(state["tags"]) if state.get("tags") else None,
        tier=GroupTier[state["tier"]] if state.get("tier") else GroupTier.OTHER,
        type=GroupSubscriptionType[state["type"]],
    )


def format_treatment_summary(treatment_data: GroupTreatmentSummaryItem) -> GroupTreatmentSummary:
    return GroupTreatmentSummary(
        accepted=int(treatment_data["accepted"]),
        accepted_undefined=int(treatment_data["accepted_undefined"]),
        in_progress=int(treatment_data["in_progress"]),
        untreated=int(treatment_data.get("new", treatment_data.get("untreated", 0))),  # type: ignore[call-overload]
    )


def format_treatment_summary_item(
    treatment_data: GroupTreatmentSummary,
) -> dict[str, int]:
    return {
        "accepted": treatment_data.accepted,
        "accepted_undefined": treatment_data.accepted_undefined,
        "in_progress": treatment_data.in_progress,
        "untreated": treatment_data.untreated,
    }


def format_code_language(
    code_language: CodeLanguage,
) -> dict[str, str | int]:
    return {
        "language": code_language.language,
        "loc": code_language.loc,
    }


def format_unfulfilled_standard_item(
    unfulfilled_standard: UnfulfilledStandard,
) -> dict[str, str | list[str]]:
    return {
        "name": unfulfilled_standard.name,
        "unfulfilled_requirements": (unfulfilled_standard.unfulfilled_requirements),
    }


def remove_group_name_prefix(group_name: str) -> str:
    return group_name.lstrip(GROUP_NAME_PREFIX)
