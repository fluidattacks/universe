from collections.abc import (
    Iterable,
)

from aiodataloader import (
    DataLoader,
)
from aioextensions import (
    collect,
)
from boto3.dynamodb.conditions import (
    Key,
)

from integrates.db_model import (
    TABLE,
)
from integrates.db_model.groups.types import (
    Group,
    GroupUnreliableIndicators,
)
from integrates.db_model.groups.utils import (
    format_group,
    format_unreliable_indicators,
    remove_group_name_prefix,
)
from integrates.db_model.items import GroupItem, GroupUnreliableIndicatorsItem
from integrates.db_model.organizations.utils import (
    remove_org_id_prefix,
)
from integrates.dynamodb import (
    keys,
    operations,
)


async def _get_group(*, group_name: str) -> Group | None:
    primary_key = keys.build_key(
        facet=TABLE.facets["group_metadata"],
        values={"name": group_name},
    )

    key_structure = TABLE.primary_key
    response = await operations.DynamoClient[GroupItem].query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(TABLE.facets["group_metadata"],),
        limit=1,
        table=TABLE,
    )

    if not response.items:
        return None

    return format_group(response.items[0])


class GroupLoader(DataLoader[str, Group | None]):
    async def batch_load_fn(self, group_names: Iterable[str]) -> list[Group | None]:
        return list(
            await collect(
                tuple(_get_group(group_name=group_name) for group_name in group_names),
                workers=32,
            ),
        )


async def _get_group_unreliable_indicators(
    *,
    group_names: Iterable[str],
) -> list[GroupUnreliableIndicators]:
    primary_keys = tuple(
        keys.build_key(
            facet=TABLE.facets["group_unreliable_indicators"],
            values={"name": group_name},
        )
        for group_name in group_names
    )
    items = await operations.DynamoClient[GroupUnreliableIndicatorsItem].batch_get_item(
        keys=primary_keys, table=TABLE
    )

    response = {
        remove_group_name_prefix(item["pk"]): format_unreliable_indicators(item) for item in items
    }

    return list(response.get(group_name, GroupUnreliableIndicators()) for group_name in group_names)


class GroupUnreliableIndicatorsLoader(DataLoader[str, GroupUnreliableIndicators]):
    async def batch_load_fn(self, group_names: Iterable[str]) -> list[GroupUnreliableIndicators]:
        return await _get_group_unreliable_indicators(group_names=group_names)


async def _get_organization_groups(
    *,
    group_dataloader: GroupLoader,
    organization_id: str,
) -> list[Group]:
    primary_key = keys.build_key(
        facet=TABLE.facets["group_metadata"],
        values={"organization_id": remove_org_id_prefix(organization_id)},
    )

    index = TABLE.indexes["inverted_index"]
    key_structure = index.primary_key
    response = await operations.DynamoClient[GroupItem].query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.sort_key)
            & Key(key_structure.sort_key).begins_with(primary_key.partition_key)
        ),
        facets=(TABLE.facets["group_metadata"],),
        table=TABLE,
        index=index,
    )

    groups: list[Group] = []
    for item in response.items:
        group = format_group(item)
        groups.append(group)
        group_dataloader.prime(group.name, group)

    return groups


class OrganizationGroupsLoader(DataLoader[str, list[Group]]):
    def __init__(self, dataloader: GroupLoader) -> None:
        super().__init__()
        self.dataloader = dataloader

    async def batch_load_fn(self, organization_ids: Iterable[str]) -> list[list[Group]]:
        return list(
            await collect(
                tuple(
                    _get_organization_groups(
                        group_dataloader=self.dataloader,
                        organization_id=organization_id,
                    )
                    for organization_id in organization_ids
                ),
                workers=32,
            ),
        )
