import json

from integrates.class_types.types import (
    Item,
)
from integrates.db_model.items import JiraInstallItem, JiraInstallStateItem
from integrates.db_model.jira_installations.types import (
    JiraInstallState,
    JiraSecurityInstall,
)
from integrates.db_model.utils import (
    serialize,
)


def format_jira_install(item: JiraInstallItem) -> JiraSecurityInstall:
    return JiraSecurityInstall(
        associated_email=item["associated_email"],
        base_url=item["base_url"],
        client_key=item["client_key"],
        cloud_id=item["cloud_id"],
        description=item["description"],
        display_url=item["display_url"],
        event_type=item["event_type"],
        installation_id=item["installation_id"],
        key=item["key"],
        plugins_version=item["plugins_version"],
        product_type=item["product_type"],
        public_key=item["public_key"],
        server_version=item["server_version"],
        shared_secret=item["shared_secret"],
        state=format_jira_state(item["state"]),
    )


def format_jira_state(item: JiraInstallStateItem) -> JiraInstallState:
    return JiraInstallState(
        modified_by=item["modified_by"],
        modified_date=item["modified_date"],
        used_api_token=item["used_api_token"],
        used_jira_jwt=item["used_jira_jwt"],
    )


def format_jira_update(new_item: Item) -> Item:
    item: Item = json.loads(json.dumps(new_item, default=serialize))

    return {
        key: None if not value and value is not False else value
        for key, value in item.items()
        if value is not None
    }
