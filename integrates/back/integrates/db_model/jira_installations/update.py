import simplejson
from boto3.dynamodb.conditions import (
    Attr,
)

from integrates.audit import AuditEvent, add_audit_event
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.jira_installations.types import (
    JiraInstallState,
)
from integrates.db_model.jira_installations.utils import (
    format_jira_update,
)
from integrates.db_model.utils import serialize
from integrates.dynamodb import (
    keys,
    operations,
)


async def update_associate_email(
    *,
    associated_email: str,
    base_url: str,
    client_key: str,
) -> None:
    associated_email = associated_email.lower().strip()
    key_structure = TABLE.primary_key
    primary_key = keys.build_key(
        facet=TABLE.facets["jira_install"],
        values={
            "client_key": client_key,
            "base_url": base_url,
        },
    )
    condition_expression = Attr(key_structure.partition_key).exists()
    item = format_jira_update(new_item={"associated_email": associated_email})

    await operations.update_item(
        condition_expression=condition_expression,
        item=item,
        key=primary_key,
        table=TABLE,
    )
    add_audit_event(
        AuditEvent(
            action="UPDATE",
            author=associated_email,
            metadata=item,
            object="JiraSecurityInstall",
            object_id=f"CLIENT_KEY#{client_key}#SITE#{base_url}",
        )
    )


async def update_shared_secret(
    *,
    shared_secret: str,
    base_url: str,
    client_key: str,
) -> None:
    key_structure = TABLE.primary_key
    primary_key = keys.build_key(
        facet=TABLE.facets["jira_install"],
        values={
            "client_key": client_key,
            "base_url": base_url,
        },
    )
    condition_expression = Attr(key_structure.partition_key).exists()
    item = format_jira_update(new_item={"shared_secret": shared_secret})

    await operations.update_item(
        condition_expression=condition_expression,
        item=item,
        key=primary_key,
        table=TABLE,
    )
    add_audit_event(
        AuditEvent(
            action="UPDATE",
            author=base_url,
            metadata=item,
            object="JiraSecurityInstall",
            object_id=f"CLIENT_KEY#{client_key}#SITE#{base_url}",
        )
    )


async def update_jira_install_state(
    *,
    client_key: str,
    base_url: str,
    state: JiraInstallState,
) -> None:
    key_structure = TABLE.primary_key
    primary_key = keys.build_key(
        facet=TABLE.facets["jira_install"],
        values={
            "client_key": client_key,
            "base_url": base_url,
        },
    )
    state_item = simplejson.loads(simplejson.dumps(state, default=serialize))
    state_item = {
        key: None if not value and value is not False else value
        for key, value in state_item.items()
        if value is not None
    }
    condition_expression = Attr(key_structure.partition_key).exists()
    item = {"state": state_item}

    await operations.update_item(
        condition_expression=condition_expression,
        item=item,
        key=primary_key,
        table=TABLE,
    )
    add_audit_event(
        AuditEvent(
            action="UPDATE",
            author=state.modified_by,
            metadata=item,
            object="JiraSecurityInstall",
            object_id=f"CLIENT_KEY#{client_key}#SITE#{base_url}",
        )
    )
