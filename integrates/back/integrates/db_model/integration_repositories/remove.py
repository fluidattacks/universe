from integrates.db_model import (
    TABLE,
)
from integrates.db_model.integration_repositories.types import (
    OrganizationIntegrationRepository,
)
from integrates.dynamodb import (
    operations,
)
from integrates.dynamodb.types import (
    PrimaryKey,
)


async def remove(*, repository: OrganizationIntegrationRepository) -> None:
    primary_key = PrimaryKey(
        partition_key=repository.organization_id,
        sort_key=repository.id,
    )

    await operations.delete_item(
        key=primary_key,
        table=TABLE,
    )
