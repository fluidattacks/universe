from datetime import (
    datetime,
)

from integrates.db_model.integration_repositories.types import (
    OrganizationIntegrationRepository,
)
from integrates.db_model.items import OrganizationUnreliableIntegrationRepositoryItem
from integrates.db_model.organizations.utils import (
    add_org_id_prefix,
)


def format_organization_integration_repository(
    item: OrganizationUnreliableIntegrationRepositoryItem,
) -> OrganizationIntegrationRepository:
    return OrganizationIntegrationRepository(
        id=item["sk"],
        organization_id=add_org_id_prefix(item["pk"]),
        branch=item["branch"],
        branches=tuple(item["branches"]) if "branches" in item else None,
        last_commit_date=datetime.fromisoformat(item["last_commit_date"])
        if item.get("last_commit_date")
        else None,
        url=item["url"],
        credential_id=item.get("credential_id"),
        name=item.get("name"),
    )
