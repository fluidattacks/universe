from integrates.db_model.mailmap.create import (
    create_mailmap_entry,
    create_mailmap_entry_with_subentries,
    create_mailmap_subentry,
)
from integrates.db_model.mailmap.delete import (
    delete_mailmap_entry,
    delete_mailmap_entry_subentries,
    delete_mailmap_entry_with_subentries,
    delete_mailmap_subentry,
)
from integrates.db_model.mailmap.get import (
    get_mailmap_entries,
    get_mailmap_entries_with_subentries,
    get_mailmap_entry,
    get_mailmap_entry_subentries,
    get_mailmap_entry_with_subentries,
    get_mailmap_records,
    get_mailmap_subentry,
)
from integrates.db_model.mailmap.update import (
    convert_mailmap_subentries_to_new_entry,
    move_mailmap_entry_with_subentries,
    set_mailmap_entry_as_mailmap_subentry,
    set_mailmap_subentry_as_mailmap_entry,
    update_mailmap_entry,
    update_mailmap_subentry,
)

__all__ = [
    "convert_mailmap_subentries_to_new_entry",
    "create_mailmap_entry",
    "create_mailmap_entry_with_subentries",
    "create_mailmap_subentry",
    "delete_mailmap_entry",
    "delete_mailmap_entry_subentries",
    "delete_mailmap_entry_with_subentries",
    "delete_mailmap_subentry",
    "get_mailmap_entries",
    "get_mailmap_entries_with_subentries",
    "get_mailmap_entry",
    "get_mailmap_entry_subentries",
    "get_mailmap_entry_with_subentries",
    "get_mailmap_records",
    "get_mailmap_subentry",
    "move_mailmap_entry_with_subentries",
    "set_mailmap_entry_as_mailmap_subentry",
    "set_mailmap_subentry_as_mailmap_entry",
    "update_mailmap_entry",
    "update_mailmap_subentry",
]
