from integrates.db_model.mailmap.utils.get import (
    get_entry_first_subentry,
    get_entry_item,
    get_subentries_count,
)
from integrates.db_model.mailmap.utils.update import (
    set_entry_as_subentry,
    set_subentry_as_entry,
    update_subentries_primary_key,
)


async def post_process_delete_entry(
    entry_email: str,
    organization_id: str,
) -> None:
    first_subentry_item = await get_entry_first_subentry(
        entry_email=entry_email,
        organization_id=organization_id,
    )
    await set_subentry_as_entry(
        subentry_item=first_subentry_item,
        organization_id=organization_id,
    )
    # Update subentries primary key
    await update_subentries_primary_key(
        old_email=entry_email,
        new_email=first_subentry_item["mailmap_subentry_email"],
        organization_id=organization_id,
    )


async def post_process_delete_subentry(
    entry_email: str,
    organization_id: str,
) -> None:
    subentries_count = await get_subentries_count(
        entry_email=entry_email,
        organization_id=organization_id,
    )
    if subentries_count == 0:  # pragma: no cover
        # logic guarantees that there is at least 1 subentry per entry
        entry_item = await get_entry_item(
            entry_email=entry_email,
            organization_id=organization_id,
        )
        await set_entry_as_subentry(
            entry_item=entry_item,
            organization_id=organization_id,
        )


async def post_process_delete_entry_subentries(
    entry_email: str,
    organization_id: str,
) -> None:
    entry_item = await get_entry_item(
        entry_email=entry_email,
        organization_id=organization_id,
    )
    await set_entry_as_subentry(
        entry_item=entry_item,
        organization_id=organization_id,
    )
