import asyncio
from collections.abc import Awaitable, Callable, Iterable
from typing import (
    Any,
)


async def async_gather(func: Callable[[Any], Awaitable[Any]], iterable: Iterable[Any]) -> list[Any]:
    results = await asyncio.gather(*map(func, iterable))
    return results


def map_list(func: Callable[[Any], Any], iterable: Iterable[Any]) -> list[Any]:
    return list(map(func, iterable))
