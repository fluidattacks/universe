from integrates.audit import AuditEvent, add_audit_event
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.mailmap.utils.get import (
    check_if_entry_not_found,
    check_if_subentry_not_found,
    get_entry_name,
    get_items,
)
from integrates.db_model.mailmap.utils.primary_key import (
    build_entry_primary_key,
    build_subentry_primary_key,
)
from integrates.db_model.mailmap.utils.utils import (
    async_gather,
)
from integrates.dynamodb import (
    operations,
)
from integrates.dynamodb.types import (
    PrimaryKey,
)


async def delete_item(primary_key: PrimaryKey) -> None:
    await operations.delete_item(key=primary_key, table=TABLE)


async def delete_entry_item(
    entry_email: str,
    organization_id: str,
) -> None:
    # Build primary key
    entry_name = await get_entry_name(
        entry_email=entry_email,
        organization_id=organization_id,
    )
    primary_key = build_entry_primary_key(
        entry_email=entry_email,
        entry_name=entry_name,
        organization_id=organization_id,
    )
    # Check if entry doesn't exist
    await check_if_entry_not_found(  # pragma: no cover
        # exception is already raised in `get_entry_name`
        entry_email=entry_email,
        organization_id=organization_id,
    )
    # Delete item
    await delete_item(primary_key=primary_key)
    add_audit_event(
        AuditEvent(
            action="DELETE",
            author="unknown",
            metadata={
                "entry_email": entry_email,
                "organization_id": organization_id,
            },
            object="MailmapEntry",
            object_id=entry_email,
        )
    )


async def delete_subentry_item(
    entry_email: str,
    subentry_email: str,
    subentry_name: str,
    organization_id: str,
    check_if_entry_exists: bool = True,
) -> None:
    # Build primary key
    primary_key = build_subentry_primary_key(
        entry_email=entry_email,
        subentry_email=subentry_email,
        subentry_name=subentry_name,
        organization_id=organization_id,
    )
    if check_if_entry_exists:
        # Check if entry doesn't exist
        await check_if_entry_not_found(  # pragma: no cover
            # exception is already raised in `entry_exists, get_entry_name`
            entry_email=entry_email,
            organization_id=organization_id,
        )
    # Check if subentry doesn't exist
    await check_if_subentry_not_found(
        entry_email=entry_email,
        subentry_email=subentry_email,
        subentry_name=subentry_name,
        organization_id=organization_id,
    )
    # Delete item
    await delete_item(primary_key=primary_key)


async def delete_entry_subentries_items(
    entry_email: str,
    organization_id: str,
    check_if_entry_exists: bool = True,
) -> None:
    # Check if entry doesn't exist
    if check_if_entry_exists:
        await check_if_entry_not_found(  # pragma: no cover
            # exception is already raised in `entry_exists, get_entry_name`
            entry_email=entry_email,
            organization_id=organization_id,
        )
    # Get items
    subentry_items = await get_items(
        facet_name="mailmap_subentry",
        email=entry_email,
        organization_id=organization_id,
    )
    # Delete items
    await async_gather(
        func=lambda subentry_item: delete_subentry_item(
            entry_email=entry_email,
            subentry_email=subentry_item["mailmap_subentry_email"],
            subentry_name=subentry_item["mailmap_subentry_name"],
            organization_id=organization_id,
            check_if_entry_exists=check_if_entry_exists,
        ),
        iterable=subentry_items,
    )
    for subentry_item in subentry_items:
        add_audit_event(
            AuditEvent(
                action="DELETE",
                author="unknown",
                metadata=subentry_item,
                object="MailmapSubentry",
                object_id=subentry_item["mailmap_subentry_email"],
            )
        )
