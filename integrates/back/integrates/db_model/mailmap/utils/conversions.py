from integrates.db_model.items import MailmapEntryItem, MailmapSubentryItem
from integrates.db_model.mailmap.constants import (
    generate_gsi_2_key,
)
from integrates.db_model.mailmap.types import (
    MailmapEntry,
    MailmapEntryWithSubentries,
    MailmapRecord,
    MailmapSubentry,
)
from integrates.db_model.mailmap.utils.primary_key import (
    build_entry_primary_key,
    build_subentry_primary_key,
)
from integrates.db_model.mailmap.utils.utils import (
    map_list,
)
from integrates.dynamodb.types import (
    PrimaryKey,
)


def entry_to_item(
    entry: MailmapEntry,
    primary_key: PrimaryKey,
    organization_id: str,
) -> MailmapEntryItem:
    gsi_2_key = generate_gsi_2_key(
        facet_name="mailmap_entry",
        organization_id=organization_id,
    )
    item: MailmapEntryItem = {
        "pk": primary_key.partition_key,
        "sk": primary_key.sort_key,
        "pk_2": gsi_2_key.partition_key,
        "sk_2": gsi_2_key.sort_key,
        "mailmap_entry_name": entry["mailmap_entry_name"],
        "mailmap_entry_email": entry["mailmap_entry_email"],
    }
    if "mailmap_entry_created_at" in entry:
        item["mailmap_entry_created_at"] = entry["mailmap_entry_created_at"]
    if "mailmap_entry_updated_at" in entry:
        item["mailmap_entry_updated_at"] = entry["mailmap_entry_updated_at"]
    return item


def item_to_entry(
    item: MailmapEntryItem,
    organization_id: str,
) -> MailmapEntry:
    entry = MailmapEntry(
        mailmap_entry_name=item["mailmap_entry_name"],
        mailmap_entry_email=item["mailmap_entry_email"],
        mailmap_entry_created_at=item.get("mailmap_entry_created_at"),
        mailmap_entry_updated_at=item.get("mailmap_entry_updated_at"),
        organization_id=organization_id,
    )
    return entry


def subentry_to_item(
    subentry: MailmapSubentry,
    primary_key: PrimaryKey,
    organization_id: str,
) -> MailmapSubentryItem:
    gsi_2_key = generate_gsi_2_key(
        facet_name="mailmap_subentry",
        organization_id=organization_id,
    )
    item: MailmapSubentryItem = {
        "pk": primary_key.partition_key,
        "sk": primary_key.sort_key,
        "pk_2": gsi_2_key.partition_key,
        "sk_2": gsi_2_key.sort_key,
        "mailmap_subentry_name": subentry["mailmap_subentry_name"],
        "mailmap_subentry_email": subentry["mailmap_subentry_email"],
    }
    if "mailmap_subentry_created_at" in subentry:
        item["mailmap_subentry_created_at"] = subentry["mailmap_subentry_created_at"]
    if "mailmap_subentry_updated_at" in subentry:
        item["mailmap_subentry_updated_at"] = subentry["mailmap_subentry_updated_at"]
    return item


def item_to_subentry(
    item: MailmapSubentryItem,
) -> MailmapSubentry:
    subentry = MailmapSubentry(
        mailmap_subentry_name=item["mailmap_subentry_name"],
        mailmap_subentry_email=item["mailmap_subentry_email"],
        mailmap_subentry_created_at=item.get("mailmap_subentry_created_at"),
        mailmap_subentry_updated_at=item.get("mailmap_subentry_updated_at"),
    )
    return subentry


def items_to_entry_with_subentries(
    entry_item: MailmapEntryItem,
    subentry_items: list[MailmapSubentryItem],
) -> MailmapEntryWithSubentries:
    entry = MailmapEntry(
        mailmap_entry_name=entry_item["mailmap_entry_name"],
        mailmap_entry_email=entry_item["mailmap_entry_email"],
        mailmap_entry_created_at=entry_item.get("mailmap_entry_created_at"),
        mailmap_entry_updated_at=entry_item.get("mailmap_entry_updated_at"),
    )
    subentries = map_list(func=item_to_subentry, iterable=subentry_items)
    entry_with_subentries = MailmapEntryWithSubentries(
        entry=entry,
        subentries=subentries,
    )
    return entry_with_subentries


def items_to_records(
    entry_item: MailmapEntryItem, subentry_items: list[MailmapSubentryItem]
) -> list[MailmapRecord]:
    records = list(
        map(
            lambda subentry_item: MailmapRecord(
                mailmap_entry_name=entry_item["mailmap_entry_name"],
                mailmap_entry_email=entry_item["mailmap_entry_email"],
                mailmap_subentry_name=subentry_item["mailmap_subentry_name"],
                mailmap_subentry_email=subentry_item["mailmap_subentry_email"],
            ),
            subentry_items,
        ),
    )

    return records


def subentry_item_to_entry_item(
    subentry_item: MailmapSubentryItem,
    organization_id: str,
) -> MailmapEntryItem:
    primary_key = build_entry_primary_key(
        entry_email=subentry_item["mailmap_subentry_email"],
        entry_name=subentry_item["mailmap_subentry_name"],
        organization_id=organization_id,
    )
    gsi_2_key = generate_gsi_2_key(
        facet_name="mailmap_entry",
        organization_id=organization_id,
    )
    entry_item: MailmapEntryItem = {
        "pk": primary_key.partition_key,
        "sk": primary_key.sort_key,
        "pk_2": gsi_2_key.partition_key,
        "sk_2": primary_key.partition_key,
        "mailmap_entry_name": subentry_item["mailmap_subentry_name"],
        "mailmap_entry_email": subentry_item["mailmap_subentry_email"],
    }
    if "mailmap_subentry_created_at" in subentry_item:
        entry_item["mailmap_entry_created_at"] = subentry_item["mailmap_subentry_created_at"]
    if "mailmap_subentry_updated_at" in subentry_item:
        entry_item["mailmap_entry_updated_at"] = subentry_item["mailmap_subentry_updated_at"]
    return entry_item


def entry_item_to_subentry_item(
    entry_item: MailmapEntryItem,
    organization_id: str,
) -> MailmapSubentryItem:
    primary_key = build_subentry_primary_key(
        entry_email=entry_item["mailmap_entry_email"],
        subentry_email=entry_item["mailmap_entry_email"],
        subentry_name=entry_item["mailmap_entry_name"],
        organization_id=organization_id,
    )
    gsi_2_key = generate_gsi_2_key(
        facet_name="mailmap_subentry",
        organization_id=organization_id,
    )
    subentry_item: MailmapSubentryItem = {
        "pk": primary_key.partition_key,
        "sk": primary_key.sort_key,
        "mailmap_subentry_name": entry_item["mailmap_entry_name"],
        "mailmap_subentry_email": entry_item["mailmap_entry_email"],
        "pk_2": gsi_2_key.partition_key,
        "sk_2": primary_key.sort_key,
    }
    if "mailmap_entry_created_at" in entry_item:
        subentry_item["mailmap_subentry_created_at"] = entry_item["mailmap_entry_created_at"]
    if "mailmap_entry_updated_at" in entry_item:
        subentry_item["mailmap_subentry_updated_at"] = entry_item["mailmap_entry_updated_at"]
    return subentry_item


def assemble_entry_with_subentries(
    entry: MailmapEntry,
    subentries: list[MailmapSubentry],
) -> MailmapEntryWithSubentries:
    entry_with_subentries = MailmapEntryWithSubentries(entry=entry, subentries=subentries)
    return entry_with_subentries


def entry_to_subentry(
    entry: MailmapEntry,
) -> MailmapSubentry:
    subentry = MailmapSubentry(
        mailmap_subentry_name=entry["mailmap_entry_name"],
        mailmap_subentry_email=entry["mailmap_entry_email"],
        mailmap_subentry_created_at=entry.get("mailmap_entry_created_at"),
        mailmap_subentry_updated_at=entry.get("mailmap_entry_updated_at"),
    )
    return subentry


def subentry_to_entry(
    subentry: MailmapSubentry,
) -> MailmapEntry:
    entry = MailmapEntry(
        mailmap_entry_name=subentry["mailmap_subentry_name"],
        mailmap_entry_email=subentry["mailmap_subentry_email"],
        mailmap_entry_created_at=subentry.get("mailmap_subentry_created_at"),
        mailmap_entry_updated_at=subentry.get("mailmap_subentry_updated_at"),
    )
    return entry
