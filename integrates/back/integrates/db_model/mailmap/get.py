import operator
from functools import (
    reduce,
)

from integrates.db_model.mailmap.types import (
    MailmapEntriesConnection,
    MailmapEntry,
    MailmapEntryWithSubentries,
    MailmapRecord,
    MailmapSubentry,
)
from integrates.db_model.mailmap.utils.conversions import (
    item_to_entry,
    item_to_subentry,
    items_to_entry_with_subentries,
    items_to_records,
)
from integrates.db_model.mailmap.utils.get import (
    check_if_organization_not_found,
    ensure_organization_id,
    get_entry_item,
    get_entry_subentries_items,
    get_items,
    get_subentry_item,
    group_subentries_by_pk,
    query_all_items,
    query_all_items_with_pagination,
)
from integrates.db_model.mailmap.utils.utils import (
    map_list,
)


async def get_mailmap_entries_with_subentries(
    organization_id: str,
) -> list[MailmapEntryWithSubentries]:
    _organization_id = await ensure_organization_id(organization_id)
    await check_if_organization_not_found(_organization_id)
    # Get items
    entry_items = await query_all_items(
        facet_name="mailmap_entry",
        organization_id=_organization_id,
    )
    subentry_items = await query_all_items(
        facet_name="mailmap_subentry",
        organization_id=_organization_id,
    )
    # Organize mailmap subentry items by pk
    subentries_grouped_by_pk = group_subentries_by_pk(
        subentry_items=subentry_items,
    )
    # Convert to objects
    entries_with_subentries = map_list(
        func=lambda entry: items_to_entry_with_subentries(
            entry_item=entry,
            subentry_items=subentries_grouped_by_pk.get(entry["pk"], []),
        ),
        iterable=entry_items,
    )
    return entries_with_subentries


async def get_mailmap_records(
    organization_id: str,
) -> list[MailmapRecord]:
    _organization_id = await ensure_organization_id(organization_id)
    await check_if_organization_not_found(_organization_id)
    # Get items
    entry_items = await query_all_items(
        facet_name="mailmap_entry",
        organization_id=_organization_id,
    )
    subentry_items = await query_all_items(
        facet_name="mailmap_subentry",
        organization_id=_organization_id,
    )
    # Organize mailmap subentry items by pk
    subentries_grouped_by_pk = group_subentries_by_pk(
        subentry_items=subentry_items,
    )
    # Convert to objects
    records_lists = map_list(
        func=lambda entry: items_to_records(
            entry_item=entry,
            subentry_items=subentries_grouped_by_pk.get(entry["pk"], []),
        ),
        iterable=entry_items,
    )
    # Flatten the lists into one list
    records: list[MailmapRecord] = reduce(operator.add, records_lists, [])

    return records


async def get_mailmap_entries(
    after: str,
    first: int,
    organization_id: str,
) -> MailmapEntriesConnection:
    _organization_id = await ensure_organization_id(organization_id)
    await check_if_organization_not_found(_organization_id)
    # Get connection
    connection = await query_all_items_with_pagination(
        _after=after,
        _first=first,
        organization_id=_organization_id,
    )
    return connection


async def get_mailmap_entry_with_subentries(
    entry_email: str,
    organization_id: str,
) -> MailmapEntryWithSubentries:
    _organization_id = await ensure_organization_id(organization_id)
    await check_if_organization_not_found(_organization_id)
    # Get items
    entry_item = await get_entry_item(
        entry_email=entry_email,
        organization_id=_organization_id,
    )
    subentry_items = await get_items(
        facet_name="mailmap_subentry",
        email=entry_email,
        organization_id=_organization_id,
    )
    # Convert to object
    entry_with_subentries = items_to_entry_with_subentries(
        entry_item=entry_item,
        subentry_items=subentry_items,
    )
    return entry_with_subentries


async def get_mailmap_entry(
    entry_email: str,
    organization_id: str,
) -> MailmapEntry:
    _organization_id = await ensure_organization_id(organization_id)
    await check_if_organization_not_found(_organization_id)
    # Get item
    entry_item = await get_entry_item(
        entry_email=entry_email,
        organization_id=_organization_id,
    )
    # Convert to object
    entry = item_to_entry(item=entry_item, organization_id=organization_id)
    return entry


async def get_mailmap_subentry(
    entry_email: str,
    subentry_email: str,
    subentry_name: str,
    organization_id: str,
) -> MailmapSubentry:
    _organization_id = await ensure_organization_id(organization_id)
    await check_if_organization_not_found(_organization_id)
    # Get item
    subentry_item = await get_subentry_item(
        entry_email=entry_email,
        subentry_email=subentry_email,
        subentry_name=subentry_name,
        organization_id=_organization_id,
    )
    # Convert to object
    subentry = item_to_subentry(item=subentry_item)
    return subentry


async def get_mailmap_entry_subentries(
    entry_email: str,
    organization_id: str,
) -> list[MailmapSubentry]:
    _organization_id = await ensure_organization_id(organization_id)
    await check_if_organization_not_found(_organization_id)
    # Get subentry items
    subentry_items = await get_entry_subentries_items(
        entry_email=entry_email,
        organization_id=_organization_id,
    )
    # Convert to objects
    subentries = map_list(func=item_to_subentry, iterable=subentry_items)
    return subentries
