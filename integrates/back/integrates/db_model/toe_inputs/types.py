from datetime import (
    date,
    datetime,
)
from typing import (
    NamedTuple,
)

from integrates.dynamodb.types import (
    FilterExpression,
    PageInfo,
)


class ToeInputState(NamedTuple):
    attacked_at: datetime | None
    attacked_by: str | None
    be_present: bool
    be_present_until: datetime | None
    first_attack_at: datetime | None
    has_vulnerabilities: bool | None
    modified_by: str | None
    modified_date: datetime
    seen_at: datetime | None
    seen_first_time_by: str | None


class ToeInputEnrichedState(NamedTuple):
    component: str
    entry_point: str
    root_id: str
    group_name: str
    state: ToeInputState


class ToeInput(NamedTuple):
    component: str
    entry_point: str
    environment_id: str
    root_id: str
    group_name: str
    state: ToeInputState


class ToeInputEdge(NamedTuple):
    node: ToeInput
    cursor: str


class ToeInputsConnection(NamedTuple):
    edges: tuple[ToeInputEdge, ...]
    page_info: PageInfo
    total: int | None = None


class ToeInputRequest(NamedTuple):
    component: str
    entry_point: str
    group_name: str
    root_id: str


class GroupHistoricToeInputsRequest(NamedTuple):
    group_name: str
    filters: FilterExpression | None = None
    after: str | None = None
    first: int | None = None
    paginate: bool = False
    start_date: date | None = None
    end_date: date | None = None


class GroupToeInputsRequest(NamedTuple):
    group_name: str
    after: str | None = None
    be_present: bool | None = None
    first: int | None = None
    paginate: bool = False


class RootToeInputsRequest(NamedTuple):
    group_name: str
    root_id: str
    after: str | None = None
    be_present: bool | None = None
    first: int | None = None
    paginate: bool = False


class ToeInputMetadataToUpdate(NamedTuple):
    clean_attacked_at: bool = False
    clean_be_present_until: bool = False
    clean_first_attack_at: bool = False
    clean_seen_at: bool = False


class ToeInputUpdate(NamedTuple):
    current_value: ToeInput
    new_state: ToeInputState
    metadata: ToeInputMetadataToUpdate
