import re
from datetime import (
    datetime,
)

from integrates.class_types.types import Item
from integrates.custom_exceptions import InvalidRegexSearch
from integrates.db_model.historic_items import ToeInputStateItem as HistoricToeInputStateItem
from integrates.db_model.items import ToeInputItem, ToeInputStateItem
from integrates.db_model.toe_inputs.types import (
    ToeInput,
    ToeInputEdge,
    ToeInputEnrichedState,
    ToeInputMetadataToUpdate,
    ToeInputState,
)
from integrates.db_model.utils import format_key_from_pk_search, get_as_utc_iso_format
from integrates.dynamodb.types import Index, PrimaryKey, Table
from integrates.dynamodb.utils import get_cursor


def format_toe_input_state(
    item: ToeInputStateItem,
) -> ToeInputState:
    return ToeInputState(
        attacked_at=datetime.fromisoformat(item["attacked_at"])
        if item.get("attacked_at")
        else None,
        attacked_by=item.get("attacked_by"),
        be_present=item.get("be_present", True),
        be_present_until=datetime.fromisoformat(item["be_present_until"])
        if item.get("be_present_until")
        else None,
        first_attack_at=datetime.fromisoformat(item["first_attack_at"])
        if item.get("first_attack_at")
        else None,
        has_vulnerabilities=item.get("has_vulnerabilities"),
        modified_by=item.get("modified_by"),
        modified_date=datetime.fromisoformat(item["modified_date"]),
        seen_at=datetime.fromisoformat(item["seen_at"]) if item.get("seen_at") else None,
        seen_first_time_by=item.get("seen_first_time_by"),
    )


def format_toe_input(
    group_name: str,
    item: ToeInputItem,
) -> ToeInput:
    state_item = item["state"]
    return ToeInput(
        state=format_toe_input_state(state_item),
        component=item["component"],
        entry_point=item["entry_point"],
        environment_id=item.get("environment_id", ""),
        group_name=group_name,
        root_id=item.get("root_id", state_item.get("unreliable_root_id", "")),
    )


def format_toe_input_enriched_state(
    item: HistoricToeInputStateItem,
) -> ToeInputEnrichedState:
    pk_search = re.search(
        r"^GROUP(#.*)?#INPUTS#ROOT(#.*)?#COMPONENT(#.*)?#ENTRYPOINT(#.*)?$",
        item["pk"],
    )
    if not pk_search:
        raise InvalidRegexSearch()
    return ToeInputEnrichedState(
        component=format_key_from_pk_search(pk_search.group(3)),
        entry_point=format_key_from_pk_search(pk_search.group(4)),
        group_name=format_key_from_pk_search(pk_search.group(1)),
        root_id=format_key_from_pk_search(pk_search.group(2)),
        state=format_toe_input_state(item),
    )


def format_toe_input_edge(
    group_name: str,
    index: Index | None,
    item: ToeInputItem,
    table: Table,
) -> ToeInputEdge:
    return ToeInputEdge(
        node=format_toe_input(group_name, item),
        cursor=get_cursor(index, item, table),
    )


def format_toe_input_state_item(state: ToeInputState) -> ToeInputStateItem:
    item: ToeInputStateItem = {
        "be_present": state.be_present,
        "modified_date": get_as_utc_iso_format(state.modified_date),
    }

    if state.attacked_by:
        item["attacked_by"] = state.attacked_by
    if state.attacked_at:
        item["attacked_at"] = get_as_utc_iso_format(state.attacked_at)
    if state.be_present_until:
        item["be_present_until"] = get_as_utc_iso_format(state.be_present_until)
    if state.first_attack_at:
        item["first_attack_at"] = get_as_utc_iso_format(state.first_attack_at)
    if state.has_vulnerabilities:
        item["has_vulnerabilities"] = state.has_vulnerabilities
    if state.modified_by:
        item["modified_by"] = state.modified_by
    if state.seen_at:
        item["seen_at"] = get_as_utc_iso_format(state.seen_at)
    if state.seen_first_time_by:
        item["seen_first_time_by"] = state.seen_first_time_by

    return item


def format_toe_input_item(
    toe_input: ToeInput,
    *,
    primary_key: PrimaryKey,
    gsi_2_key: PrimaryKey,
) -> ToeInputItem:
    return {
        "pk": primary_key.partition_key,
        "sk": primary_key.sort_key,
        "pk_2": gsi_2_key.partition_key,
        "sk_2": gsi_2_key.sort_key,
        "component": toe_input.component,
        "entry_point": toe_input.entry_point,
        "environment_id": toe_input.environment_id,
        "group_name": toe_input.group_name,
        "root_id": toe_input.root_id,
        "state": format_toe_input_state_item(toe_input.state),
    }


def format_toe_input_enriched_state_item(
    toe_input: ToeInput,
    *,
    primary_key: PrimaryKey,
    gsi_2_key: PrimaryKey,
) -> dict:
    item = {
        "pk": primary_key.partition_key,
        "sk": primary_key.sort_key,
        "pk_2": gsi_2_key.partition_key,
        "sk_2": gsi_2_key.sort_key,
    }

    state = toe_input.state

    if state.attacked_by:
        item["attacked_by"] = state.attacked_by
    if state.attacked_at:
        item["attacked_at"] = get_as_utc_iso_format(state.attacked_at)
    if state.modified_by:
        item["modified_by"] = state.modified_by
    if state.modified_date:
        item["modified_date"] = get_as_utc_iso_format(state.modified_date)
    if state.seen_at:
        item["seen_at"] = get_as_utc_iso_format(state.seen_at)
    if state.seen_first_time_by:
        item["seen_first_time_by"] = state.seen_first_time_by

    return item


def format_toe_input_state_metadata_item(
    state_item: Item, metadata: ToeInputMetadataToUpdate
) -> Item:
    if metadata.clean_attacked_at:
        state_item["attacked_at"] = None
    if metadata.clean_be_present_until:
        state_item["be_present_until"] = None
    if metadata.clean_first_attack_at:
        state_item["first_attack_at"] = None
    if metadata.clean_seen_at:
        state_item["seen_at"] = None

    return state_item
