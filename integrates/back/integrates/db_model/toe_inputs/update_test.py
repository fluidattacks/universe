from integrates.db_model.toe_inputs.get import _get_toe_inputs
from integrates.db_model.toe_inputs.types import (
    ToeInputMetadataToUpdate,
    ToeInputRequest,
    ToeInputUpdate,
)
from integrates.db_model.toe_inputs.update import update_state
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import DATE_2024, ToeInputFaker, ToeInputStateFaker
from integrates.testing.mocks import mocks


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            toe_inputs=[
                ToeInputFaker(
                    component="https://test.com",
                    entry_point="user",
                    root_id="root1",
                    group_name="group1",
                    state=ToeInputStateFaker(
                        be_present=True,
                        seen_first_time_by="foo@fluidattacks.com",
                    ),
                ),
            ],
        ),
    )
)
async def test_update_not_present_toe_input() -> None:
    # Arrange
    toe_input = (
        await _get_toe_inputs(
            [
                ToeInputRequest(
                    component="https://test.com",
                    entry_point="user",
                    group_name="group1",
                    root_id="root1",
                ),
            ],
        )
    )[0]
    assert toe_input

    # Act
    await update_state(
        ToeInputUpdate(
            current_value=toe_input,
            new_state=toe_input.state._replace(
                be_present=False,
                seen_at=DATE_2024,
            ),
            metadata=ToeInputMetadataToUpdate(),
        ),
    )

    toe_input_updated = (
        await _get_toe_inputs(
            [
                ToeInputRequest(
                    component="https://test.com",
                    entry_point="user",
                    group_name="group1",
                    root_id="root1",
                ),
            ],
        )
    )[0]
    assert toe_input_updated

    # Assert
    assert not toe_input_updated.state.be_present
    assert toe_input_updated.state.seen_first_time_by == "foo@fluidattacks.com"
