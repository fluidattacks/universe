import simplejson as json
from boto3.dynamodb.conditions import (
    Attr,
)

from integrates.audit import AuditEvent, add_audit_event
from integrates.class_types.types import (
    Item,
)
from integrates.custom_exceptions import (
    ToeInputAlreadyUpdated,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.toe_inputs.constants import (
    GSI_2_FACET,
)
from integrates.db_model.toe_inputs.types import (
    ToeInputUpdate,
)
from integrates.db_model.toe_inputs.utils import (
    format_toe_input_state_metadata_item,
)
from integrates.db_model.utils import (
    get_as_utc_iso_format,
    serialize,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.exceptions import (
    ConditionalCheckFailedException,
)


async def update_state(update_info: ToeInputUpdate) -> None:
    current_value = update_info.current_value
    new_state = update_info.new_state
    metadata = update_info.metadata
    key_structure = TABLE.primary_key
    gsi_2_index = TABLE.indexes["gsi_2"]
    facet = TABLE.facets["toe_input_metadata"]
    toe_input_key = keys.build_key(
        facet=facet,
        values={
            "component": current_value.component,
            "entry_point": current_value.entry_point,
            "group_name": current_value.group_name,
            "root_id": current_value.root_id,
        },
    )
    gsi_2_index = TABLE.indexes["gsi_2"]
    gsi_2_key = keys.build_key(
        facet=GSI_2_FACET,
        values={
            "be_present": str(new_state.be_present).lower(),
            "component": current_value.component,
            "entry_point": current_value.entry_point,
            "group_name": current_value.group_name,
            "root_id": current_value.root_id,
        },
    )

    new_state_item: Item = format_toe_input_state_metadata_item(
        state_item=json.loads(json.dumps(new_state, default=serialize)),
        metadata=metadata,
    )
    new_item = {
        "state": new_state_item,
        gsi_2_index.primary_key.sort_key: gsi_2_key.sort_key,
    }
    condition_expression = Attr(key_structure.partition_key).exists() & Attr(
        "state.modified_date",
    ).eq(get_as_utc_iso_format(current_value.state.modified_date))
    try:
        await operations.update_item(
            condition_expression=condition_expression,
            item=new_item,
            key=toe_input_key,
            table=TABLE,
        )
        add_audit_event(
            AuditEvent(
                action="UPDATE",
                author=update_info.new_state.modified_by or "unknown",
                metadata=new_item,
                object="ToeInput",
                object_id=toe_input_key.sort_key,
            )
        )
    except ConditionalCheckFailedException as ex:
        raise ToeInputAlreadyUpdated() from ex
