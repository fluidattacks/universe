from datetime import (
    datetime,
)
from typing import cast

import simplejson as json

from integrates.class_types.types import (
    Item,
)
from integrates.db_model.items import (
    OrganizationBillingItem,
    OrganizationDocumentsItem,
    OrganizationItem,
    OrganizationPaymentMethodsItem,
    OrganizationPriorityPolicyItem,
    OrganizationStateItem,
    OrganizationUnreliableIndicatorsItem,
    PoliciesItem,
    StandardComplianceItem,
)
from integrates.db_model.organizations.constants import (
    ORGANIZATION_ID_PREFIX,
)
from integrates.db_model.organizations.enums import (
    OrganizationStateStatus,
)
from integrates.db_model.organizations.types import (
    DocumentFile,
    Organization,
    OrganizationBilling,
    OrganizationDocuments,
    OrganizationMetadataToUpdate,
    OrganizationPaymentMethods,
    OrganizationPriorityPolicy,
    OrganizationStandardCompliance,
    OrganizationState,
    OrganizationUnreliableIndicators,
    OrganizationUnreliableIndicatorsToUpdate,
)
from integrates.db_model.types import (
    Policies,
    PoliciesToUpdate,
)
from integrates.db_model.utils import (
    get_as_utc_iso_format,
    serialize,
)


def add_org_id_prefix(organization_id: str) -> str:
    no_prefix_id = remove_org_id_prefix(organization_id)
    return f"{ORGANIZATION_ID_PREFIX}{no_prefix_id}"


def remove_org_id_prefix(organization_id: str) -> str:
    return organization_id.lstrip(ORGANIZATION_ID_PREFIX)


def format_metadata_item(metadata: OrganizationMetadataToUpdate) -> Item:
    item = {
        "country": metadata.country,
        "billing_information": json.loads(
            json.dumps(metadata.billing_information, default=serialize),
        ),
        "payment_methods": [
            json.loads(json.dumps(payment_method, default=serialize))
            for payment_method in metadata.payment_methods
        ]
        if metadata.payment_methods is not None
        else None,
        "priority_policies": [
            json.loads(json.dumps(priority_policy, default=serialize))
            for priority_policy in metadata.priority_policies
        ]
        if metadata.priority_policies is not None
        else None,
        "jira_associated_id": metadata.jira_associated_id,
        "vulnerabilities_pathfile": metadata.vulnerabilities_pathfile,
    }
    return {key: None if not value else value for key, value in item.items() if value is not None}


def format_organization(item: OrganizationItem) -> Organization:
    return Organization(
        created_by=item.get("created_by", ""),
        created_date=datetime.fromisoformat(item["created_date"])
        if item.get("created_date")
        else None,
        billing_information=format_billing_information(item["billing_information"])
        if item.get("billing_information")
        else None,
        country=item.get("country", ""),
        id=add_org_id_prefix(item["id"]),
        name=item["name"],
        payment_methods=format_payment_methods(item.get("payment_methods", [])),
        policies=format_policies(item["policies"]),
        priority_policies=format_priority_policies(item.get("priority_policies", [])),
        state=format_state(item["state"]),
        vulnerabilities_pathfile=item.get("vulnerabilities_pathfile", None),
    )


def format_payment_methods(
    payment_methods: list[OrganizationPaymentMethodsItem],
) -> list[OrganizationPaymentMethods]:
    return [
        OrganizationPaymentMethods(
            id=payment_method.get("id", ""),
            business_name=payment_method.get("business_name", ""),
            documents=format_documents(payment_method["documents"]),
            email=payment_method.get("email", ""),
            country=payment_method.get("country", ""),
            state=payment_method.get("state", ""),
            city=payment_method.get("city", ""),
        )
        for payment_method in payment_methods
    ]


def format_priority_policies(
    priority_policies: list[OrganizationPriorityPolicyItem],
) -> list[OrganizationPriorityPolicy]:
    return [
        OrganizationPriorityPolicy(
            policy=priority_policy.get("policy", ""),
            value=priority_policy.get("value", 0),
        )
        for priority_policy in priority_policies
    ]


def format_policies(policies: PoliciesItem) -> Policies:
    return Policies(
        days_until_it_breaks=int(policies["days_until_it_breaks"])
        if policies.get("days_until_it_breaks") is not None
        else None,
        inactivity_period=int(policies["inactivity_period"])
        if "inactivity_period" in policies
        else None,
        max_acceptance_days=int(policies["max_acceptance_days"])
        if "max_acceptance_days" in policies
        else None,
        max_acceptance_severity=policies.get("max_acceptance_severity"),
        max_number_acceptances=int(policies["max_number_acceptances"])
        if "max_number_acceptances" in policies
        else None,
        min_acceptance_severity=policies.get("min_acceptance_severity"),
        min_breaking_severity=policies.get("min_breaking_severity"),
        modified_date=datetime.fromisoformat(policies["modified_date"]),
        modified_by=policies["modified_by"],
        vulnerability_grace_period=int(policies["vulnerability_grace_period"])
        if "vulnerability_grace_period" in policies
        else None,
        max_number_of_expected_contributors=int(policies["max_number_of_expected_contributors"])
        if "max_number_of_expected_contributors" in policies
        else None,
    )


def format_policies_item(
    modified_by: str,
    modified_date: datetime,
    policies: PoliciesToUpdate,
) -> PoliciesItem:
    item = {
        "days_until_it_breaks": policies.days_until_it_breaks,
        "inactivity_period": policies.inactivity_period,
        "max_acceptance_days": policies.max_acceptance_days,
        "max_acceptance_severity": policies.max_acceptance_severity,
        "max_number_acceptances": policies.max_number_acceptances,
        "min_acceptance_severity": policies.min_acceptance_severity,
        "min_breaking_severity": policies.min_breaking_severity,
        "modified_by": modified_by,
        "modified_date": get_as_utc_iso_format(modified_date),
        "vulnerability_grace_period": policies.vulnerability_grace_period,
    }

    return cast(PoliciesItem, {key: value for key, value in item.items() if value is not None})


def format_state(state: OrganizationStateItem) -> OrganizationState:
    return OrganizationState(
        aws_external_id=state["aws_external_id"],
        status=OrganizationStateStatus[state["status"]],
        modified_by=state["modified_by"],
        modified_date=datetime.fromisoformat(state["modified_date"]),
        pending_deletion_date=datetime.fromisoformat(state["pending_deletion_date"])
        if state.get("pending_deletion_date")
        else None,
    )


def format_documents(documents: OrganizationDocumentsItem) -> OrganizationDocuments:
    return OrganizationDocuments(
        rut=DocumentFile(
            file_name=documents["rut"]["file_name"],
            modified_date=datetime.fromisoformat(documents["rut"]["modified_date"]),
        )
        if documents.get("rut")
        else None,
        tax_id=DocumentFile(
            file_name=documents["tax_id"]["file_name"],
            modified_date=datetime.fromisoformat(documents["tax_id"]["modified_date"]),
        )
        if documents.get("tax_id")
        else None,
    )


def format_standard_compliance(
    item: StandardComplianceItem,
) -> OrganizationStandardCompliance:
    return OrganizationStandardCompliance(
        standard_name=item["standard_name"],
        compliance_level=item["compliance_level"],
    )


def format_standard_compliance_item(
    standard_compliance: OrganizationStandardCompliance,
) -> StandardComplianceItem:
    return {
        "standard_name": standard_compliance.standard_name,
        "compliance_level": standard_compliance.compliance_level,
    }


def format_unreliable_indicators(
    item: OrganizationUnreliableIndicatorsItem,
) -> OrganizationUnreliableIndicators:
    return OrganizationUnreliableIndicators(
        compliance_level=item.get("compliance_level"),
        compliance_weekly_trend=item.get("compliance_weekly_trend"),
        estimated_days_to_full_compliance=item.get("estimated_days_to_full_compliance"),
        standard_compliances=[
            format_standard_compliance(standard_compliance)
            for standard_compliance in item["standard_compliances"]
        ]
        if "standard_compliances" in item
        else None,
        missed_repositories=item.get("missed_repositories", 0),
        covered_repositories=item.get("covered_repositories", 0),
        missed_authors=item.get("missed_authors", 0),
        covered_authors=item.get("covered_authors", 0),
        has_ztna_roots=item.get("has_ztna_roots", False),
    )


def format_unreliable_indicators_item(
    indicators: OrganizationUnreliableIndicators,
) -> Item:
    return {
        "compliance_level": indicators.compliance_level,
        "compliance_weekly_trend": indicators.compliance_weekly_trend,
        "estimated_days_to_full_compliance": (indicators.estimated_days_to_full_compliance),
        "standard_compliances": [
            format_standard_compliance_item(standard_compliance)
            for standard_compliance in indicators.standard_compliances
        ]
        if indicators.standard_compliances is not None
        else None,
    }


def format_unreliable_indicators_item_to_update(
    indicators: OrganizationUnreliableIndicatorsToUpdate,
) -> Item:
    item = {
        "compliance_level": indicators.compliance_level,
        "compliance_weekly_trend": indicators.compliance_weekly_trend,
        "estimated_days_to_full_compliance": (indicators.estimated_days_to_full_compliance),
        "standard_compliances": [
            format_standard_compliance_item(standard_compliance)
            for standard_compliance in indicators.standard_compliances
        ]
        if indicators.standard_compliances is not None
        else None,
        "missed_repositories": indicators.missed_repositories,
        "covered_repositories": indicators.covered_repositories,
        "missed_authors": indicators.missed_authors,
        "covered_authors": indicators.covered_authors,
        "has_ztna_roots": indicators.has_ztna_roots,
    }

    return {key: value for key, value in item.items() if value is not None}


def format_billing_information(
    item: list[OrganizationBillingItem],
) -> list[OrganizationBilling]:
    def _format(_item: OrganizationBillingItem) -> OrganizationBilling:
        return OrganizationBilling(
            billing_email=_item["billing_email"],
            last_modified_by=_item["last_modified_by"],
            customer_id=_item["customer_id"],
            subscription_id=_item.get("subscription_id"),
            billed_groups=_item.get("billed_groups"),
            modified_at=datetime.fromisoformat(_item["modified_at"]),
        )

    return [_format(_item) for _item in item]
