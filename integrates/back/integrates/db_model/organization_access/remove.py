from integrates.audit import AuditEvent, add_audit_event
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.organizations.utils import (
    remove_org_id_prefix,
)
from integrates.dynamodb import (
    keys,
)
from integrates.dynamodb.operations import (
    delete_item,
)


async def remove(*, email: str, organization_id: str) -> None:
    email = email.lower().strip()
    primary_key = keys.build_key(
        facet=TABLE.facets["organization_access"],
        values={
            "email": email,
            "id": remove_org_id_prefix(organization_id),
        },
    )

    await delete_item(key=primary_key, table=TABLE)
    add_audit_event(
        AuditEvent(
            action="DELETE",
            author="unknown",
            metadata={},
            object="OrganizationAccess",
            object_id=f"USER#{email}#ORG#{organization_id}",
        )
    )
