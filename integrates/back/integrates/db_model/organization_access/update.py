from integrates.audit import AuditEvent, add_audit_event
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.organization_access.types import (
    OrganizationAccessMetadataToUpdate,
)
from integrates.db_model.organization_access.utils import (
    format_metadata_item,
)
from integrates.db_model.organizations.utils import (
    remove_org_id_prefix,
)
from integrates.dynamodb import (
    keys,
    operations,
)


async def update_metadata(
    *,
    email: str,
    metadata: OrganizationAccessMetadataToUpdate,
    organization_id: str,
) -> None:
    email = email.lower().strip()
    primary_key = keys.build_key(
        facet=TABLE.facets["organization_access"],
        values={
            "email": email,
            "id": remove_org_id_prefix(organization_id),
        },
    )
    item = format_metadata_item(email=email, metadata=metadata, organization_id=organization_id)
    await operations.update_item(
        item=item,
        key=primary_key,
        table=TABLE,
    )
    add_audit_event(
        AuditEvent(
            action="UPDATE",
            author=metadata.state.modified_by,
            metadata=item,
            object="OrganizationAccess",
            object_id=f"USER#{email}#ORG#{organization_id}",
        )
    )
