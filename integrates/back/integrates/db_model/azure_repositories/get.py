import logging
import logging.config
import shutil
from collections.abc import Iterable
from datetime import UTC, datetime
from tempfile import TemporaryDirectory

import gitlab
import requests
from aiodataloader import DataLoader
from aioextensions import collect, in_thread
from atlassian.bitbucket import Cloud
from azure.devops.client import AzureDevOpsAuthenticationError
from azure.devops.connection import Connection
from azure.devops.exceptions import AzureDevOpsClientRequestError, AzureDevOpsServiceError
from azure.devops.v7_1.git.git_client import GitClient
from azure.devops.v7_1.git.models import GitCommit, GitQueryCommitsCriteria, GitRepository
from dateutil import parser
from fluidattacks_core.git import https_clone
from git.exc import GitError
from git.repo.base import Repo
from github import Auth, Github, GithubRetry, Repository
from github.GithubException import (
    BadCredentialsException,
    GithubException,
    RateLimitExceededException,
)
from gitlab.const import AccessLevel
from gitlab.exceptions import GitlabAuthenticationError, GitlabListError
from msrest.authentication import BasicAuthentication, OAuthTokenAuthentication
from requests.exceptions import ChunkedEncodingError, ConnectTimeout, HTTPError, ReadTimeout
from urllib3.util.url import parse_url

from integrates.context import (
    FI_AZURE_OAUTH2_REPOSITORY_APP_ID,
    FI_BITBUCKET_OAUTH2_REPOSITORY_APP_ID,
)
from integrates.db_model.azure_repositories.constants import MAX_BRANCHES
from integrates.db_model.azure_repositories.types import (
    BasicRepoData,
    CredentialsGitRepositoryCommit,
    GitRepositoryCommit,
)
from integrates.db_model.credentials.types import Credentials, HttpsPatSecret
from integrates.settings import LOGGING

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)
BASE_URL = "https://dev.azure.com"
PROFILE_BASE_URL = "https://app.vssps.visualstudio.com"
DEFAULT_ISO_STR = "2000-01-01T05:00:00+00:00"


async def get_repositories(
    *,
    credentials: Iterable[Credentials],
) -> list[list[GitRepository]]:
    return list(
        await collect(
            tuple(
                in_thread(
                    _get_repositories,
                    base_url=credential.state.azure_organization
                    if credential.state.azure_organization
                    and credential.state.azure_organization.startswith(BASE_URL)
                    else f"{BASE_URL}/{credential.state.azure_organization}",
                    access_token=credential.secret.token
                    if isinstance(credential.secret, HttpsPatSecret)
                    else "",
                    context={
                        "organization_id": credential.organization_id,
                        "credential_name": credential.state.name,
                        "credential_type": credential.state.type,
                        "credential_owner": credential.state.owner,
                    },
                )
                for credential in credentials
            ),
            workers=1,
        ),
    )


def _get_repositories(
    *,
    base_url: str,
    access_token: str,
    context: dict[str, str],
    is_oauth: bool = False,
) -> list[GitRepository]:
    try:
        credentials = BasicAuthentication("", access_token)
        base_url = requests.utils.requote_uri(base_url)
        connection = Connection(base_url=base_url, creds=credentials)
        git_client: GitClient = connection.clients.get_git_client()
        repositories: list[GitRepository] = git_client.get_repositories()

        return [repo for repo in repositories if not repo.is_disabled]
    except (
        AzureDevOpsClientRequestError,
        AzureDevOpsAuthenticationError,
        AzureDevOpsServiceError,
        ChunkedEncodingError,
    ) as exc:
        extra = {
            "extra": {
                "exception": exc,
                "base_url": base_url,
                "is_oauth": is_oauth,
                "context": context,
            },
        }
        if str(exc).endswith("is not authorized to access this resource."):
            LOGGER.error(
                "This azure credential is not valid or doesn't have right permissions",
                extra=extra,
            )

        elif str(exc).startswith("The requested resource requires user auth"):
            LOGGER.error(
                "This azure credential doesn't have enough permissions",
                extra=extra,
            )

        elif str(exc).startswith("A potentially dangerous Request.Path value"):
            LOGGER.error(
                "Invalid path due to unexpected characters",
                extra=extra,
            )

        else:
            LOGGER.error(
                "Error getting azure repo data",
                extra=extra,
            )

        return []


async def get_oauth_repositories(
    *,
    token: str,
    base_urls: tuple[str, ...],
) -> list[list[GitRepository]]:
    return list(
        await collect(
            tuple(
                in_thread(
                    _get_repositories,
                    base_url=base_url,
                    access_token=token,
                    is_oauth=True,
                    context={},
                )
                for base_url in base_urls
            ),
            workers=1,
        ),
    )


async def _get_authors(
    *,
    branch: str,
    url: str,
    token: str,
    oauth_type: str,
) -> tuple[dict, ...]:
    with TemporaryDirectory(prefix="integrates_outside_") as temp_dir:
        folder_to_clone_root, stderr = await https_clone(
            branch=branch,
            password=None,
            repo_url=url,
            temp_dir=temp_dir,
            token=token,
            user=None,
            provider=oauth_type,
        )

        if folder_to_clone_root is None:
            shutil.rmtree(temp_dir, ignore_errors=True)
            LOGGER.info(
                "Failed to clone",
                extra={
                    "extra": {
                        "url": url,
                        "error": stderr,
                    },
                },
            )
            return tuple()

        try:
            repo = Repo(folder_to_clone_root, search_parent_directories=True)
            authors = tuple(
                {
                    "author": commit.author.email.lower(),
                    "date": commit.committed_date,
                }
                for commit in repo.iter_commits()
                if commit.author.email
            )

            shutil.rmtree(temp_dir, ignore_errors=True)
            return authors
        except (GitError, AttributeError) as exc:
            shutil.rmtree(temp_dir, ignore_errors=True)
            LOGGER.error(
                exc,
                extra={
                    "extra": {
                        "url": url,
                    },
                },
            )
            return tuple()


async def get_bitbucket_authors(*, token: str, repo_id: str) -> tuple[dict, ...]:
    oauth2_dict = {
        "client_id": FI_BITBUCKET_OAUTH2_REPOSITORY_APP_ID,
        "token": {"access_token": token},
    }
    bitbucket_cloud = Cloud(oauth2=oauth2_dict, backoff_and_retry=True)
    _workspace, _slug = repo_id.rsplit("#REPOSITORY#", 1)
    try:
        repo = await in_thread(bitbucket_cloud.repositories.get, _workspace, _slug)
    except HTTPError as exc:
        LOGGER.error(
            "Error getting bitbucket repo authors data",
            extra={"extra": {"exception": exc, "repo_id": repo_id}},
        )

        return tuple()

    return await _get_authors(
        branch=repo._BitbucketBase__data["mainbranch"]["name"],
        url=repo._BitbucketBase__data["links"]["clone"][0]["href"],
        token=token,
        oauth_type="BITBUCKET",
    )


async def get_bitbucket_repositories(*, token: str) -> tuple[BasicRepoData, ...]:
    try:
        return await in_thread(_get_bitbucket_repositories, token=token)
    except HTTPError as exc:
        LOGGER.error(
            "Error when requesting bitbucket repositories",
            extra={"extra": {"exception": exc}},
        )
        return tuple()


def _get_bitbucket_repositories(*, token: str) -> tuple[BasicRepoData, ...]:
    repos: list[BasicRepoData] = []
    oauth2_dict = {
        "client_id": FI_BITBUCKET_OAUTH2_REPOSITORY_APP_ID,
        "token": {"access_token": token},
    }
    bitbucket_cloud = Cloud(oauth2=oauth2_dict, backoff_and_retry=True)
    for workspace in bitbucket_cloud.workspaces.each():
        for repo in workspace.repositories.each():
            main_branch = repo._BitbucketBase__data["mainbranch"]["name"]
            default_branch = f'refs/heads/{main_branch.rstrip().lstrip("refs/heads/")}'

            repos.append(
                BasicRepoData(
                    id=(f"{workspace.uuid}#REPOSITORY#{repo.slug}"),
                    remote_url=parse_url(
                        repo._BitbucketBase__data["links"]["clone"][0]["href"],
                    )
                    ._replace(auth=None)
                    .url,
                    ssh_url=repo._BitbucketBase__data["links"]["clone"][1]["href"],
                    web_url=parse_url(repo._BitbucketBase__data["links"]["html"]["href"]).url,
                    branch=default_branch,
                    branches=tuple(),
                    name=repo._BitbucketBase__data["full_name"],
                    last_activity_at=datetime.fromisoformat("2000-01-01T05:00:00+00:00")
                    if repo.updated_on == "never updated"
                    else parser.parse(repo.updated_on).astimezone(UTC),
                ),
            )

    return tuple(repos)


async def get_oauth_repositories_commits(
    *,
    repositories: list[GitRepositoryCommit],
) -> list[list[GitCommit]]:
    repositories_commits = await collect(
        tuple(
            in_thread(
                _get_repositories_commits,
                access_token=repository.access_token,
                repository_id=repository.repository_id,
                project_name=repository.project_name,
                base_url=repository.base_url,
                is_oauth=True,
            )
            for repository in repositories
        ),
        workers=1,
    )
    return list(repositories_commits)


async def get_repositories_commits(
    *,
    repositories: Iterable[CredentialsGitRepositoryCommit],
) -> list[list[GitCommit]]:
    repositories_commits = await collect(
        tuple(
            in_thread(
                _get_repositories_commits,
                access_token=repo.credential.secret.token
                if isinstance(repo.credential.secret, HttpsPatSecret)
                else "",
                repository_id=repo.repository_id,
                base_url=repo.credential.state.azure_organization
                if repo.credential.state.azure_organization
                and repo.credential.state.azure_organization.startswith(BASE_URL)
                else f"{BASE_URL}/{repo.credential.state.azure_organization}",
                project_name=repo.project_name,
            )
            for repo in repositories
        ),
        workers=1,
    )

    return list(repositories_commits)


def _get_repositories_commits(
    *,
    access_token: str,
    repository_id: str,
    project_name: str,
    base_url: str,
    total: bool = False,
    is_oauth: bool = False,
) -> list[GitCommit]:
    credentials: BasicAuthentication | OAuthTokenAuthentication
    if is_oauth:
        credentials = OAuthTokenAuthentication(
            FI_AZURE_OAUTH2_REPOSITORY_APP_ID,
            {"access_token": access_token},
        )
    else:
        credentials = BasicAuthentication("", access_token)
    connection = Connection(base_url=base_url, creds=credentials)
    try:
        git_client: GitClient = connection.clients_v7_1.get_git_client()
        commits: list[GitCommit] = git_client.get_commits(
            search_criteria=GitQueryCommitsCriteria() if total else GitQueryCommitsCriteria(top=1),
            repository_id=repository_id,
            project=project_name,
        )
        return commits
    except (
        AzureDevOpsAuthenticationError,
        AzureDevOpsClientRequestError,
        AzureDevOpsServiceError,
    ) as exc:
        if is_oauth and str(exc.message).startswith("TF400813"):
            raise exc
        LOGGER.error(
            "Error getting azure commit data",
            extra={
                "extra": {
                    "exception": exc,
                    "repository_id": repository_id,
                    "project_name": project_name,
                    "is_oauth": is_oauth,
                    "base_url": base_url,
                },
            },
        )
        return []


async def get_github_repos_commits(
    *,
    token: str,
    repositories: tuple[BasicRepoData, ...],
    credential_id: str,
) -> tuple[tuple[dict, ...], ...]:
    try:
        return await collect(
            tuple(
                _get_github_repos_commits(token=token, repo=repo, credential_id=credential_id)
                for repo in repositories
            ),
            workers=1,
        )
    except BadCredentialsException as exc:
        LOGGER.exception(
            exc,
            extra={"extra": {"credential_id": credential_id}},
        )
    return tuple()


async def _get_github_repos_commits(
    token: str,
    repo: BasicRepoData,
    credential_id: str,
) -> tuple[dict, ...]:
    try:
        return await _get_authors(
            branch=repo.branch.lstrip("refs/heads/"),
            url=parse_url(str(repo.clone_url)).url,
            token=token,
            oauth_type="GITHUB",
        )
    except (TimeoutError, GithubException, ConnectTimeout, RateLimitExceededException) as exc:
        if "Git Repository is empty" in str(exc):
            return tuple()
        LOGGER.error(
            "Error getting github data",
            extra={
                "extra": {
                    "exception": exc,
                    "remaining": (f"Requests Remaining: {Github(token).rate_limiting[0]}"),
                    "credential_id": credential_id,
                },
            },
        )
    return tuple()


async def get_github_repos(*, token: str, credential_id: str) -> tuple[BasicRepoData, ...]:
    try:
        return await in_thread(_get_github_repos, token=token)
    except (BadCredentialsException, GithubException, RateLimitExceededException) as exc:
        LOGGER.error(
            "Error getting github repos data",
            extra={"extra": {"exception": exc, "credential_id": credential_id}},
        )
        return tuple()


def get_github_branches_names(
    repo: Repository.Repository, cred_id: str, token: str
) -> tuple[str, ...]:
    try:
        branches = tuple(branch for branch in repo.get_branches().get_page(0))
        return tuple(branch.name for branch in branches)[:MAX_BRANCHES]
    except (
        BadCredentialsException,
        GithubException,
        RateLimitExceededException,
        ReadTimeout,
    ) as exc:
        LOGGER.error(
            "Error getting github branches data",
            extra={
                "extra": {
                    "exception": exc,
                    "repo_id": str(repo.id),
                    "credential_id": cred_id,
                    "remaining": f"Requests Remaining: {Github(token).rate_limiting[0]}",
                }
            },
        )
    return tuple()


def _get_github_repos(token: str) -> tuple[BasicRepoData, ...]:
    repos = (
        Github(
            auth=Auth.Token(token),
            seconds_between_requests=0.35,
            retry=GithubRetry(total=3),
            per_page=MAX_BRANCHES,
        )
        .get_user()
        .get_repos()
    )

    return tuple(
        BasicRepoData(
            id=str(repo.id),
            remote_url=parse_url(repo.clone_url).url,
            ssh_url=repo.git_url,
            web_url=parse_url(repo.html_url).url,
            branch=("refs/heads/" f'{repo.default_branch.rstrip().lstrip("refs/heads/")}'),
            branches=tuple([f'{repo.default_branch.rstrip().lstrip("refs/heads/")}']),
            name=repo.full_name,
            last_activity_at=repo.updated_at.astimezone(UTC),
            clone_url=repo.clone_url,
            repository=repo,
        )
        for repo in repos
    )


async def get_gitlab_projects(*, token: str, credential_id: str) -> tuple[BasicRepoData, ...]:
    try:
        return await in_thread(_get_gitlab_projects, token=token)
    except (
        GitlabAuthenticationError,
        KeyError,
        GitlabListError,
        ChunkedEncodingError,
    ) as exc:
        LOGGER.error(
            exc,
            extra={"extra": {"credential_id": credential_id}},
        )
    return tuple()


def _get_gitlab_projects(token: str) -> tuple[BasicRepoData, ...]:
    try:
        with gitlab.Gitlab(oauth_token=token) as g_session:
            projects = tuple(
                g_session.projects.list(
                    all=True,
                    min_access_level=AccessLevel.REPORTER.value,
                ),
            )

        return tuple(
            BasicRepoData(
                id=gproject.id,
                remote_url=parse_url(gproject.attributes["http_url_to_repo"]).url,
                ssh_url=gproject.attributes["ssh_url_to_repo"],
                web_url=parse_url(gproject.attributes["web_url"]).url,
                branch=(
                    "refs/heads/"
                    + gproject.attributes["default_branch"].rstrip().lstrip("refs/heads/")
                ),
                branches=tuple(
                    [gproject.attributes["default_branch"].rstrip().lstrip("refs/heads/")],
                ),
                name=str(gproject.path_with_namespace),
                last_activity_at=parser.parse(gproject.attributes["last_activity_at"]).astimezone(
                    UTC,
                ),
            )
            for gproject in projects
            if not bool(gproject.attributes["archived"])
        )
    except ChunkedEncodingError as exc:
        LOGGER.error(exc)
        return tuple()


class OrganizationRepositoriesLoader(DataLoader[Credentials, list[GitRepository]]):
    async def batch_load_fn(
        self,
        credentials: Iterable[Credentials],
    ) -> list[list[GitRepository]]:
        return await get_repositories(credentials=credentials)


class OrganizationRepositoriesCommitsLoader(
    DataLoader[CredentialsGitRepositoryCommit, list[GitCommit]],
):
    async def batch_load_fn(
        self,
        repositories: Iterable[CredentialsGitRepositoryCommit],
    ) -> list[list[GitCommit]]:
        return await get_repositories_commits(repositories=repositories)
