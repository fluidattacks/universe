from typing import (
    NamedTuple,
)

from integrates.dynamodb.types import (
    PageInfo,
)


class CriteriaMainData(NamedTuple):
    description: str
    impact: str
    recommendation: str
    threat: str
    title: str


class CriteriaBaseScore(NamedTuple):
    attack_complexity: str
    attack_vector: str
    availability: str
    confidentiality: str
    integrity: str
    privileges_required: str
    scope: str
    user_interaction: str


class CriteriaTemporalScore(NamedTuple):
    exploit_code_maturity: str
    remediation_level: str
    report_confidence: str


class CriteriaScore(NamedTuple):
    base: CriteriaBaseScore
    temporal: CriteriaTemporalScore


class CriteriaBaseScoreV4(NamedTuple):
    attack_complexity: str
    attack_vector: str
    attack_requirements: str
    availability_va: str
    confidentiality_vc: str
    integrity_vi: str
    availability_sa: str
    confidentiality_sc: str
    integrity_si: str
    privileges_required: str
    user_interaction: str


class CriteriaThreatScoreV4(NamedTuple):
    exploit_maturity: str


class CriteriaScoreV4(NamedTuple):
    base: CriteriaBaseScoreV4
    threat: CriteriaThreatScoreV4


class Criteria(NamedTuple):
    en: CriteriaMainData
    es: CriteriaMainData
    finding_number: str
    remediation_time: str
    requirements: list[str]
    score: CriteriaScore
    score_v4: CriteriaScoreV4


class CriteriaEdge(NamedTuple):
    cursor: str
    node: Criteria


class CriteriaConnection(NamedTuple):
    edges: tuple[CriteriaEdge, ...]
    page_info: PageInfo
    total: int
