from integrates.dataloaders import get_new_context
from integrates.db_model.portfolios.types import (
    PortfolioRequest,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import PortfolioFaker
from integrates.testing.mocks import mocks


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            portfolios=[PortfolioFaker(organization_name="orgtest", id="test-tag")]
        )
    )
)
async def test_get_tag() -> None:
    loaders = get_new_context()
    tag = await loaders.portfolio.load(
        PortfolioRequest(organization_name="orgtest", portfolio_id="test-tag")
    )
    assert tag
    assert tag.unreliable_indicators.last_closing_date == 20
    assert tag.unreliable_indicators.mean_remediate_critical_severity == 0
    assert tag.unreliable_indicators.mean_remediate_high_severity == 0
    assert tag.unreliable_indicators.mean_remediate_low_severity == 0
    assert tag.unreliable_indicators.mean_remediate_medium_severity == 0
    assert tag.unreliable_indicators.mean_remediate == 100
