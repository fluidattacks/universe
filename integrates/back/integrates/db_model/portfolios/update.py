from integrates.audit import AuditEvent, add_audit_event
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.portfolios.types import (
    Portfolio,
)
from integrates.db_model.portfolios.utils import (
    format_portfolio_item_to_update,
)
from integrates.dynamodb import (
    keys,
    operations,
)


async def update(
    *,
    portfolio: Portfolio,
) -> None:
    portfolio_key = keys.build_key(
        facet=TABLE.facets["portfolio_metadata"],
        values={
            "id": portfolio.id,
            "name": portfolio.organization_name,
        },
    )
    portfolio_item = format_portfolio_item_to_update(portfolio)
    await operations.update_item(
        item=portfolio_item,
        key=portfolio_key,
        table=TABLE,
    )
    add_audit_event(
        AuditEvent(
            action="UPDATE",
            author="unknown",
            metadata=portfolio_item,
            object="Portfolio",
            object_id=portfolio.id,
        )
    )
