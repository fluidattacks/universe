from boto3.dynamodb.conditions import Attr

from integrates.audit import AuditEvent, add_audit_event
from integrates.class_types.types import BaseTypedDict
from integrates.custom_exceptions import (
    AlreadyCreated,
    InvalidStateStatus,
    RepeatedMachineFindingCode,
)
from integrates.db_model import HISTORIC_TABLE, TABLE
from integrates.db_model.constants import MACHINE_EMAIL
from integrates.db_model.findings.enums import FindingEvidenceName, FindingStateStatus
from integrates.db_model.findings.types import Finding, FindingEvidence
from integrates.db_model.findings.utils import (
    format_evidence_item,
    format_finding_metadata_item,
    format_state_item,
    format_verification_item,
)
from integrates.db_model.utils import get_as_utc_iso_format
from integrates.dynamodb import keys, operations
from integrates.dynamodb.exceptions import ConditionalCheckFailedException


async def _add_finding_id(*, finding_id: str) -> None:
    key_structure = TABLE.primary_key
    primary_key = keys.build_key(
        facet=TABLE.facets["finding_id"],
        values={"id": finding_id},
    )
    item = {
        key_structure.partition_key: primary_key.partition_key,
        key_structure.sort_key: primary_key.sort_key,
    }
    condition_expression = Attr(key_structure.partition_key).not_exists()
    try:
        await operations.put_item(
            condition_expression=condition_expression,
            facet=TABLE.facets["finding_id"],
            item=item,
            table=TABLE,
        )
    except ConditionalCheckFailedException as ex:
        raise AlreadyCreated() from ex


async def _add_machine_finding_code(*, finding: Finding) -> None:
    created_by = finding.creation.modified_by if finding.creation else finding.state.modified_by
    if created_by != MACHINE_EMAIL:
        return

    finding_code = finding.get_criteria_code()
    primary_key = keys.build_key(
        facet=TABLE.facets["machine_finding_code"],
        values={
            "group_name": finding.group_name,
            "finding_code": finding_code,
        },
    )
    key_structure = TABLE.primary_key
    item = {
        key_structure.partition_key: primary_key.partition_key,
        key_structure.sort_key: primary_key.sort_key,
    }
    condition_expression = Attr(key_structure.partition_key).not_exists()
    try:
        await operations.put_item(
            condition_expression=condition_expression,
            facet=TABLE.facets["machine_finding_code"],
            item=item,
            table=TABLE,
        )
    except ConditionalCheckFailedException as ex:
        raise RepeatedMachineFindingCode() from ex


async def add(*, finding: Finding) -> None:
    if finding.state.status != FindingStateStatus.CREATED:
        raise InvalidStateStatus()

    await _add_finding_id(finding_id=finding.id)
    await _add_machine_finding_code(finding=finding)

    items: list[BaseTypedDict] = []
    state_key = keys.build_key(
        facet=TABLE.facets["finding_historic_state"],
        values={
            "id": finding.id,
            "iso8601utc": get_as_utc_iso_format(finding.state.modified_date),
        },
    )
    state_item = format_state_item(finding.state, state_key=state_key)
    items.append(state_item)

    if finding.verification is not None:
        verification_key = keys.build_key(
            facet=TABLE.facets["finding_historic_verification"],
            values={
                "id": finding.id,
                "iso8601utc": get_as_utc_iso_format(finding.verification.modified_date),
            },
        )
        historic_verification_item = format_verification_item(
            finding.verification, verification_key=verification_key
        )
        items.append(historic_verification_item)

    metadata_key = keys.build_key(
        facet=TABLE.facets["finding_metadata"],
        values={"group_name": finding.group_name, "id": finding.id},
    )
    finding_metadata = format_finding_metadata_item(finding, state_item, metadata_key=metadata_key)
    items.append(finding_metadata)

    await operations.batch_put_item(items=tuple(items), table=TABLE)
    add_audit_event(
        AuditEvent(
            action="CREATE",
            author=finding.state.modified_by,
            metadata=finding_metadata,
            object="Finding",
            object_id=finding.id,
        )
    )


async def add_historic_verification(*, finding: Finding) -> None:
    if finding.verification is not None:
        verification_key = keys.build_key(
            facet=HISTORIC_TABLE.facets["finding_verification"],
            values={
                "id": finding.id,
                "iso8601utc": get_as_utc_iso_format(finding.verification.modified_date),
                "group_name": finding.group_name,
                "verification": "verification",
            },
        )
        historic_verification_item = format_verification_item(
            finding.verification, verification_key=verification_key
        )
        await operations.batch_put_item(
            items=tuple([historic_verification_item]), table=HISTORIC_TABLE
        )


async def add_evidence(
    *,
    evidence_name: FindingEvidenceName,
    evidence: FindingEvidence,
    finding_id: str,
    group_name: str,
) -> None:
    metadata_key = keys.build_key(
        facet=TABLE.facets["finding_metadata"],
        values={"group_name": group_name, "id": finding_id},
    )
    attribute = f"evidences.{evidence_name.value}"
    metadata = {attribute: format_evidence_item(evidence)}
    await operations.update_item(
        condition_expression=Attr(attribute).not_exists(),
        item=metadata,
        key=metadata_key,
        table=TABLE,
    )
    add_audit_event(
        AuditEvent(
            action="UPDATE",
            author=evidence.author_email,
            metadata=metadata,
            object="Finding",
            object_id=finding_id,
        )
    )
