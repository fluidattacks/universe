from __future__ import (
    annotations,
)

from datetime import (
    datetime,
)
from decimal import (
    Decimal,
)
from typing import (
    NamedTuple,
)

from integrates.db_model.enums import (
    Source,
    StateRemovalJustification,
)
from integrates.db_model.findings.enums import (
    DraftRejectionReason,
    FindingSorts,
    FindingStateStatus,
    FindingStatus,
    FindingVerificationStatus,
)
from integrates.db_model.roots.enums import (
    RootCriticality,
)
from integrates.db_model.types import (
    SeverityScore,
)
from integrates.dynamodb.types import (
    PageInfo,
)


class DraftRejection(NamedTuple):
    other: str
    reasons: set[DraftRejectionReason]
    rejected_by: str
    rejection_date: datetime
    submitted_by: str


class FindingState(NamedTuple):
    modified_by: str
    modified_date: datetime
    source: Source
    status: FindingStateStatus
    rejection: DraftRejection | None = None
    justification: StateRemovalJustification = StateRemovalJustification.NO_JUSTIFICATION


class FindingVerification(NamedTuple):
    comment_id: str
    modified_by: str
    modified_date: datetime
    status: FindingVerificationStatus
    vulnerability_ids: set[str] | None = None


class FindingEvidence(NamedTuple):
    description: str
    modified_date: datetime
    url: str
    author_email: str = ""
    is_draft: bool = False


class FindingEvidences(NamedTuple):
    animation: FindingEvidence | None = None
    evidence1: FindingEvidence | None = None
    evidence2: FindingEvidence | None = None
    evidence3: FindingEvidence | None = None
    evidence4: FindingEvidence | None = None
    evidence5: FindingEvidence | None = None
    exploitation: FindingEvidence | None = None
    records: FindingEvidence | None = None


class FindingTreatmentSummary(NamedTuple):
    accepted: int = 0
    accepted_undefined: int = 0
    in_progress: int = 0
    untreated: int = 0


class FindingVerificationSummary(NamedTuple):
    requested: int = 0
    on_hold: int = 0
    verified: int = 0


class FindingZeroRiskSummary(NamedTuple):
    confirmed: int = 0
    rejected: int = 0
    requested: int = 0


class FindingVulnerabilitiesSummary(NamedTuple):
    closed: int = 0
    open: int = 0
    submitted: int = 0
    rejected: int = 0
    open_critical: int = 0
    open_high: int = 0
    open_low: int = 0
    open_medium: int = 0
    open_critical_v3: int = 0
    open_high_v3: int = 0
    open_low_v3: int = 0
    open_medium_v3: int = 0


class FindingUnreliableIndicators(NamedTuple):
    unreliable_newest_vulnerability_report_date: datetime | None = None
    unreliable_oldest_open_vulnerability_report_date: datetime | None = None
    unreliable_oldest_vulnerability_report_date: datetime | None = None
    unreliable_status: FindingStatus = FindingStatus.DRAFT
    unreliable_total_open_cvssf: Decimal = Decimal("0.0")
    unreliable_where: str = ""
    unreliable_zero_risk_summary: FindingZeroRiskSummary = FindingZeroRiskSummary()
    submitted_vulnerabilities: int = 0
    rejected_vulnerabilities: int = 0
    max_open_epss: int = 0
    max_open_root_criticality: RootCriticality = RootCriticality.LOW
    max_open_severity_score: Decimal = Decimal("0.0")
    max_open_severity_score_v4: Decimal = Decimal("0.0")
    newest_vulnerability_report_date: datetime | None = None
    oldest_vulnerability_report_date: datetime | None = None
    treatment_summary: FindingTreatmentSummary = FindingTreatmentSummary()
    total_open_cvssf: Decimal = Decimal("0.0")
    total_open_cvssf_v4: Decimal = Decimal("0.0")
    total_open_priority: Decimal | None = None
    verification_summary: FindingVerificationSummary = FindingVerificationSummary()
    vulnerabilities_summary: FindingVulnerabilitiesSummary = FindingVulnerabilitiesSummary()


class Finding(NamedTuple):
    group_name: str
    id: str
    severity_score: SeverityScore
    state: FindingState
    title: str
    attack_vector_description: str = ""
    creation: FindingState | None = None
    rejected_vulnerabilities: int | None = None
    submitted_vulnerabilities: int | None = None
    description: str = ""
    evidences: FindingEvidences = FindingEvidences()
    min_time_to_remediate: int | None = None
    recommendation: str = ""
    requirements: str = ""
    sorts: FindingSorts = FindingSorts.NO
    threat: str = ""
    unfulfilled_requirements: list[str] = []
    unreliable_indicators: FindingUnreliableIndicators = FindingUnreliableIndicators()
    verification: FindingVerification | None = None

    def get_criteria_code(self) -> str:
        return self.title.split(".")[0].strip()

    def get_hacker_email(self) -> str:
        return self.creation.modified_by if self.creation else self.state.modified_by


class FindingEdge(NamedTuple):
    node: Finding
    cursor: str


class FindingsConnection(NamedTuple):
    edges: tuple[FindingEdge, ...]
    page_info: PageInfo
    total: int | None = None


class FindingEvidenceToUpdate(NamedTuple):
    description: str | None = None
    author_email: str | None = None
    is_draft: bool | None = None
    modified_date: datetime | None = None
    url: str | None = None


class FindingMetadataToUpdate(NamedTuple):
    attack_vector_description: str | None = None
    description: str | None = None
    evidences: FindingEvidences | None = None
    min_time_to_remediate: int | None = None
    recommendation: str | None = None
    requirements: str | None = None
    severity_score: SeverityScore | None = None
    sorts: FindingSorts | None = None
    threat: str | None = None
    title: str | None = None
    unfulfilled_requirements: list[str] | None = None


class FindingUnreliableIndicatorsToUpdate(NamedTuple):
    unreliable_newest_vulnerability_report_date: datetime | None = None
    unreliable_oldest_open_vulnerability_report_date: datetime | None = None
    unreliable_oldest_vulnerability_report_date: datetime | None = None
    unreliable_status: FindingStatus | None = None
    unreliable_total_open_cvssf: Decimal | None = None
    unreliable_where: str | None = None
    unreliable_zero_risk_summary: FindingZeroRiskSummary | None = None
    clean_unreliable_newest_vulnerability_report_date: bool = False
    clean_unreliable_oldest_open_vulnerability_report_date: bool = False
    clean_unreliable_oldest_vulnerability_report_date: bool = False
    submitted_vulnerabilities: int | None = None
    rejected_vulnerabilities: int | None = None
    max_open_epss: int | None = None
    max_open_root_criticality: RootCriticality | None = None
    max_open_severity_score: Decimal | None = None
    max_open_severity_score_v4: Decimal | None = None
    newest_vulnerability_report_date: datetime | None = None
    oldest_vulnerability_report_date: datetime | None = None
    treatment_summary: FindingTreatmentSummary | None = None
    total_open_cvssf: Decimal | None = None
    total_open_cvssf_v4: Decimal | None = None
    total_open_priority: Decimal | None = None
    verification_summary: FindingVerificationSummary | None = None
    vulnerabilities_summary: FindingVulnerabilitiesSummary | None = None


class FindingRequest(NamedTuple):
    finding_id: str
    group_name: str
