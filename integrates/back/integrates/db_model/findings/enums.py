from enum import (
    Enum,
)


class DraftRejectionReason(str, Enum):
    CONSISTENCY: str = "CONSISTENCY"
    EVIDENCE: str = "EVIDENCE"
    NAMING: str = "NAMING"
    OMISSION: str = "OMISSION"
    OTHER: str = "OTHER"
    SCORING: str = "SCORING"
    WRITING: str = "WRITING"


class FindingEvidenceName(str, Enum):
    animation: str = "animation"
    evidence1: str = "evidence1"
    evidence2: str = "evidence2"
    evidence3: str = "evidence3"
    evidence4: str = "evidence4"
    evidence5: str = "evidence5"
    exploitation: str = "exploitation"
    records: str = "records"


class FindingSorts(str, Enum):
    NO: str = "NO"
    YES: str = "YES"


class FindingStateStatus(str, Enum):
    CREATED: str = "CREATED"
    DELETED: str = "DELETED"
    MASKED: str = "MASKED"


class FindingStatus(str, Enum):
    DRAFT: str = "DRAFT"
    SAFE: str = "SAFE"
    VULNERABLE: str = "VULNERABLE"


class FindingVerificationStatus(str, Enum):
    MASKED: str = "MASKED"
    REQUESTED: str = "REQUESTED"
    ON_HOLD: str = "ON_HOLD"
    VERIFIED: str = "VERIFIED"
