from aioextensions import collect
from boto3.dynamodb.conditions import Key

from integrates.archive.domain import archive_root
from integrates.audit import AuditEvent, add_audit_event
from integrates.db_model import TABLE
from integrates.db_model.roots.types import GitRootState, IPRootState, URLRootState
from integrates.db_model.utils import archive
from integrates.dynamodb import keys, operations
from integrates.dynamodb.types import Facet, PrimaryKey


async def remove_environment_url(
    root_id: str,
    group_name: str,
    url_id: str,
) -> None:
    primary_key = keys.build_key(
        facet=TABLE.facets["root_environment_url"],
        values={"uuid": root_id, "hash": url_id, "group_name": group_name},
    )
    await operations.delete_item(key=primary_key, table=TABLE)
    add_audit_event(
        AuditEvent(
            action="DELETE",
            author="unknown",
            metadata={},
            object="RootEnvironment",
            object_id=url_id,
        )
    )


async def remove_environment_url_secret(group_name: str, resource_id: str, secret_key: str) -> None:
    primary_key = keys.build_key(
        facet=TABLE.facets["root_environment_secret"],
        values={
            "group_name": group_name,
            "hash": resource_id,
            "key": secret_key,
        },
    )
    await operations.delete_item(key=primary_key, table=TABLE)
    add_audit_event(
        AuditEvent(
            action="DELETE",
            author="unknown",
            metadata={},
            object="RootEnvironmentSecret",
            object_id=secret_key,
        )
    )


async def remove_root_secret(
    resource_id: str,
    secret_key: str,
) -> None:
    primary_key = keys.build_key(
        facet=TABLE.facets["root_secret"],
        values={"uuid": resource_id, "key": secret_key},
    )

    await operations.delete_item(key=primary_key, table=TABLE)
    add_audit_event(
        AuditEvent(
            action="DELETE",
            author="unknown",
            metadata={},
            object="RootSecret",
            object_id=secret_key,
        )
    )


async def _remove_environment_url_secrets(
    *,
    group_name: str,
    url_id: str,
) -> None:
    key_structure = TABLE.primary_key
    primary_key = keys.build_key(
        facet=TABLE.facets["root_environment_secret"],
        values={"group_name": group_name, "hash": url_id},
    )
    condition_expression = Key(key_structure.partition_key).eq(primary_key.partition_key) & Key(
        key_structure.sort_key,
    ).begins_with(primary_key.sort_key)
    response = await operations.query(
        condition_expression=condition_expression,
        facets=(TABLE.facets["root_environment_secret"],),
        table=TABLE,
    )
    await operations.batch_delete_item(
        keys=tuple(
            PrimaryKey(
                partition_key=item["pk"],
                sort_key=item["sk"],
            )
            for item in response.items
        ),
        table=TABLE,
    )
    for item in response.items:
        add_audit_event(
            AuditEvent(
                action="DELETE",
                author="unknown",
                metadata={},
                object="RootEnvironmentSecret",
                object_id=item["pk"],
            )
        )


async def _remove_all_environment_urls(
    *,
    group_name: str,
    root_id: str,
) -> None:
    facet = TABLE.facets["root_environment_url"]
    primary_key = keys.build_key(
        facet=facet,
        values={"uuid": root_id, "group_name": group_name},
    )
    key_structure = TABLE.primary_key
    condition_expression = Key(key_structure.partition_key).eq(primary_key.partition_key) & Key(
        key_structure.sort_key,
    ).begins_with(primary_key.sort_key)
    response = await operations.query(
        condition_expression=condition_expression,
        facets=(facet,),
        table=TABLE,
    )
    items = response.items
    if not items:
        return
    await collect(
        _remove_environment_url_secrets(group_name=group_name, url_id=item["sk"].split("URL#")[-1])
        for item in items
    )
    await operations.batch_delete_item(
        keys=tuple(
            PrimaryKey(
                partition_key=item["pk"],
                sort_key=item["sk"],
            )
            for item in items
        ),
        table=TABLE,
    )
    for item in response.items:
        add_audit_event(
            AuditEvent(
                action="DELETE",
                author="unknown",
                metadata={},
                object="RootEnvironment",
                object_id=item["pk"],
            )
        )


async def _remove_all_toe_packages(
    *,
    group_name: str,
    root_id: str,
) -> None:
    facet = TABLE.facets["toe_packages_metadata"]
    primary_key = keys.build_key(
        facet=facet,
        values={
            "group_name": group_name,
            "root_id": root_id,
        },
    )
    index = None
    key_structure = TABLE.primary_key
    condition_expression = Key(key_structure.partition_key).eq(primary_key.partition_key) & Key(
        key_structure.sort_key,
    ).begins_with(primary_key.sort_key.replace("#PACKAGE#VERSION", ""))

    response = await operations.query(
        condition_expression=condition_expression,
        facets=(facet,),
        index=index,
        table=TABLE,
    )

    items = response.items
    if not items:
        return

    keys_to_delete = set(
        PrimaryKey(
            partition_key=item[TABLE.primary_key.partition_key],
            sort_key=item[TABLE.primary_key.sort_key],
        )
        for item in items
    )

    await operations.batch_delete_item(
        keys=tuple(keys_to_delete),
        table=TABLE,
    )


async def _remove_all_docker_images(
    *,
    group_name: str,
    root_id: str,
) -> None:
    facet = TABLE.facets["root_docker_image"]
    primary_key = keys.build_key(
        facet=facet,
        values={
            "uuid": root_id,
            "group_name": group_name,
        },
    )
    key_structure = TABLE.primary_key
    condition_expression = Key(key_structure.partition_key).eq(primary_key.partition_key)
    response = await operations.query(
        condition_expression=condition_expression,
        facets=(facet,),
        table=TABLE,
    )
    items = response.items
    if not items:
        return
    await collect(
        _remove_docker_image_related_resources(
            group_name=group_name,
            root_id=root_id,
            uri=item["sk"].split("URI#")[-1],
        )
        for item in items
    )
    keys_to_delete = set(
        PrimaryKey(
            partition_key=item[TABLE.primary_key.partition_key],
            sort_key=item[TABLE.primary_key.sort_key],
        )
        for item in response.items
    )
    await operations.batch_delete_item(
        keys=tuple(keys_to_delete),
        table=TABLE,
    )
    for key in keys_to_delete:
        add_audit_event(
            AuditEvent(
                action="DELETE",
                author="unknown",
                metadata={},
                object="RootDockerImage",
                object_id=key.partition_key,
            )
        )


async def _remove_docker_image_related_resources(
    *,
    group_name: str,
    root_id: str,
    uri: str,
) -> None:
    primary_key = keys.build_key(
        facet=TABLE.facets["root_docker_image_historic_state"],
        values={
            "uuid": root_id,
            "group_name": group_name,
            "uri": uri,
        },
    )
    key_structure = TABLE.primary_key
    condition_expression = Key(key_structure.partition_key).eq(primary_key.partition_key)
    response = await operations.query(
        condition_expression=condition_expression,
        facets=(
            TABLE.facets["root_docker_image_historic_state"],
            TABLE.facets["root_docker_image_layer"],
        ),
        table=TABLE,
    )
    keys_to_delete = set(
        PrimaryKey(
            partition_key=item[TABLE.primary_key.partition_key],
            sort_key=item[TABLE.primary_key.sort_key],
        )
        for item in response.items
    )
    await operations.batch_delete_item(
        keys=tuple(keys_to_delete),
        table=TABLE,
    )


async def _remove_root_facets(*, root_id: str, facets: tuple[Facet, ...]) -> set[PrimaryKey]:
    primary_key = keys.build_key(
        facet=facets[0],
        values={
            "uuid": root_id,
        },
    )
    key_structure = TABLE.primary_key
    condition_expression = Key(key_structure.partition_key).eq(primary_key.partition_key) & Key(
        key_structure.sort_key,
    ).begins_with(primary_key.sort_key)
    response = await operations.query(
        condition_expression=condition_expression,
        facets=facets,
        table=TABLE,
    )
    if not response.items:
        return set()

    await archive(response.items, archive_root)
    keys_to_delete = set(
        PrimaryKey(
            partition_key=item[TABLE.primary_key.partition_key],
            sort_key=item[TABLE.primary_key.sort_key],
        )
        for item in response.items
    )
    await operations.batch_delete_item(
        keys=tuple(keys_to_delete),
        table=TABLE,
    )
    return keys_to_delete


async def remove(
    *,
    group_name: str,
    root_id: str,
    root_state: GitRootState | IPRootState | URLRootState,
) -> None:
    await _remove_all_environment_urls(group_name=group_name, root_id=root_id)
    await _remove_all_docker_images(group_name=group_name, root_id=root_id)
    await _remove_all_toe_packages(group_name=group_name, root_id=root_id)
    await _remove_root_facets(
        root_id=root_id,
        facets=(
            TABLE.facets["git_root_historic_state"],
            TABLE.facets["ip_root_historic_state"],
            TABLE.facets["url_root_historic_state"],
        ),
    )

    if isinstance(root_state, GitRootState):
        current_url_key = keys.build_key(
            facet=TABLE.facets["git_root_metadata"],
            values={
                "name": group_name,
                "uuid": f"URL#{root_state.url}#BRANCH#{root_state.branch}",
            },
        )
        await operations.delete_item(key=current_url_key, table=TABLE)

    removed_secrets = await _remove_root_facets(
        root_id=root_id,
        facets=(TABLE.facets["root_secret"],),
    )
    for secret in removed_secrets:
        add_audit_event(
            AuditEvent(
                action="DELETE",
                author="unknown",
                metadata={},
                object="RootSecret",
                object_id=secret.partition_key,
            )
        )
    await _remove_root_facets(
        root_id=root_id,
        facets=(
            TABLE.facets["git_root_metadata"],
            TABLE.facets["ip_root_metadata"],
            TABLE.facets["url_root_metadata"],
        ),
    )
    add_audit_event(
        AuditEvent(
            action="DELETE",
            author="unknown",
            metadata={},
            object="Root",
            object_id=root_id,
        )
    )
