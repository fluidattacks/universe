from datetime import datetime
from typing import cast

from integrates.db_model import TABLE
from integrates.db_model.items import (
    GitRootCloningItem,
    GitRootItem,
    GitRootMachineItem,
    GitRootRebaseItem,
    GitRootStateItem,
    GitRootToeLinesItem,
    IpRootItem,
    IpRootStateItem,
    MachineExecutionsDateItem,
    RootDockerImageItem,
    RootDockerImageStateItem,
    RootEnvironmentUrlItem,
    RootEnvironmentUrlStateItem,
    RootUnreliableIndicatorsItem,
    SecretStateItem,
    UrlRootItem,
    UrlRootStateItem,
)
from integrates.db_model.roots.constants import GROUP_INDEX_METADATA
from integrates.db_model.roots.enums import (
    RootCloningStatus,
    RootCriticality,
    RootDockerImageStateStatus,
    RootEnvironmentCloud,
    RootEnvironmentUrlStateStatus,
    RootEnvironmentUrlType,
    RootMachineStatus,
    RootRebaseStatus,
    RootStateReason,
    RootStatus,
    RootToeLinesStatus,
    RootType,
)
from integrates.db_model.roots.types import (
    DockerImageHistory,
    GitRoot,
    GitRootCloning,
    GitRootMachine,
    GitRootRebase,
    GitRootState,
    GitRootToeLines,
    IPRoot,
    IPRootState,
    MachineExecutionsDate,
    Root,
    RootDockerImage,
    RootDockerImageState,
    RootEnvironmentUrl,
    RootEnvironmentUrlState,
    RootItem,
    RootUnreliableIndicators,
    SecretState,
    URLRoot,
    URLRootState,
)
from integrates.db_model.types import CodeLanguage
from integrates.db_model.utils import serialize
from integrates.dynamodb import keys


def _parse_datetime(date_str: str | None) -> datetime | None:
    if date_str:
        return datetime.fromisoformat(date_str)
    return None


def _format_unreliable_indicators(item: RootItem) -> RootUnreliableIndicators:
    return (
        format_unreliable_indicators(item["unreliable_indicators"])
        if "unreliable_indicators" in item
        else RootUnreliableIndicators()
    )


def _format_organization_name(item: RootItem) -> str:
    return item["pk_2"].split("#")[1]


def _format_group_name(item: RootItem) -> str:
    return item["sk"].split("#")[1]


def _format_root_id(item: RootItem) -> str:
    return item["pk"].split("#")[1]


def _format_root_state_reason(raw: str | None) -> RootStateReason | None:
    if not raw:
        return None

    try:
        return RootStateReason[raw]
    except KeyError:
        return RootStateReason.OTHER


def _format_machine_executions_date(
    raw: MachineExecutionsDateItem | None,
) -> MachineExecutionsDate | None:
    if raw is None:
        return None

    return MachineExecutionsDate(
        apk=_parse_datetime(raw.get("apk")),
        cspm=_parse_datetime(raw.get("cspm")),
        dast=_parse_datetime(raw.get("dast")),
        sast=_parse_datetime(raw.get("sast")),
        sca=_parse_datetime(raw.get("sca")),
    )


def format_unreliable_indicators(
    raw: RootUnreliableIndicatorsItem,
) -> RootUnreliableIndicators:
    return RootUnreliableIndicators(
        unreliable_code_languages=[
            CodeLanguage(language=item["language"], loc=int(item["loc"]))
            for item in raw["unreliable_code_languages"]
        ]
        if "unreliable_code_languages" in raw
        else [],
        unreliable_last_status_update=_parse_datetime(
            raw.get("unreliable_last_status_update", None),
        ),
        nofluid_quantity=raw.get("nofluid_quantity", None),
    )


def format_git_state(state: GitRootStateItem) -> GitRootState:
    return GitRootState(
        branch=str(state["branch"]).strip(),
        credential_id=state.get("credential_id"),
        criticality=RootCriticality(state.get("criticality")) if "criticality" in state else None,
        gitignore=state["gitignore"],
        includes_health_check=state["includes_health_check"],
        modified_by=state["modified_by"],
        modified_date=datetime.fromisoformat(state["modified_date"]),
        nickname=state["nickname"],
        other=state.get("other"),
        reason=_format_root_state_reason(state.get("reason")),
        status=RootStatus[state["status"]],
        url=str(state["url"]).strip(),
        use_egress=state.get("use_egress", False),
        use_vpn=state.get("use_vpn", False),
        use_ztna=state.get("use_ztna", False),
    )


def format_ip_state(state: IpRootStateItem) -> IPRootState:
    return IPRootState(
        address=state["address"],
        modified_by=state["modified_by"],
        modified_date=datetime.fromisoformat(state["modified_date"]),
        nickname=state["nickname"],
        other=state.get("other"),
        reason=_format_root_state_reason(state.get("reason")),
        status=RootStatus[state["status"]],
    )


def format_url_state(state: UrlRootStateItem) -> URLRootState:
    return URLRootState(
        excluded_sub_paths=state.get("excluded_sub_paths"),
        host=state["host"],
        modified_by=state["modified_by"],
        modified_date=datetime.fromisoformat(state["modified_date"]),
        nickname=state["nickname"],
        other=state.get("other"),
        path=state["path"],
        port=state["port"],
        protocol=state["protocol"],
        query=state.get("query"),
        reason=_format_root_state_reason(state.get("reason")),
        status=RootStatus[state["status"]],
    )


def format_secret_state(state: SecretStateItem) -> SecretState:
    return SecretState(
        owner=state["owner"],
        description=state.get("description"),
        modified_by=state["modified_by"],
        modified_date=datetime.fromisoformat(state["modified_date"]),
    )


def format_cloning(cloning: GitRootCloningItem) -> GitRootCloning:
    return GitRootCloning(
        modified_date=datetime.fromisoformat(cloning["modified_date"]),
        reason=cloning["reason"],
        status=RootCloningStatus(cloning["status"]),
        commit=cloning.get("commit"),
        commit_date=_parse_datetime(cloning.get("commit_date", None)),
        modified_by=cloning.get("modified_by", ""),
        first_successful_cloning=_parse_datetime(cloning.get("first_successful_cloning", None)),
        first_failed_cloning=_parse_datetime(cloning.get("first_failed_cloning", None)),
        last_successful_cloning=_parse_datetime(cloning.get("last_successful_cloning", None)),
        failed_count=cloning.get("failed_count", 0),
        successful_count=cloning.get("successful_count", 0),
    )


def format_machine(raw: GitRootMachineItem | None) -> GitRootMachine | None:
    if raw is None:
        return None

    return GitRootMachine(
        modified_by=raw["modified_by"],
        modified_date=datetime.fromisoformat(raw["modified_date"]),
        reason=raw["reason"],
        status=RootMachineStatus(raw["status"]),
        commit=raw.get("commit"),
        commit_date=_parse_datetime(raw.get("commit_date", None)),
        execution_id=raw.get("execution_id"),
        last_executions_date=_format_machine_executions_date(raw.get("last_executions_date")),
    )


def format_rebase(rebase: GitRootRebaseItem | None) -> GitRootRebase | None:
    if rebase is None:
        return None
    return GitRootRebase(
        modified_by=rebase["modified_by"],
        modified_date=datetime.fromisoformat(rebase["modified_date"]),
        reason=rebase["reason"],
        status=RootRebaseStatus(rebase["status"]),
        commit=rebase.get("commit"),
        commit_date=_parse_datetime(rebase.get("commit_date", None)),
        last_successful_rebase=_parse_datetime(rebase.get("last_successful_rebase", None)),
    )


def format_toe_lines(toe_lines: GitRootToeLinesItem | None) -> GitRootToeLines | None:
    if toe_lines is None:
        return None
    return GitRootToeLines(
        modified_by=toe_lines["modified_by"],
        modified_date=datetime.fromisoformat(toe_lines["modified_date"]),
        reason=toe_lines["reason"],
        status=RootToeLinesStatus(toe_lines["status"]),
        commit=toe_lines.get("commit"),
        commit_date=_parse_datetime(toe_lines.get("commit_date", None)),
        last_successful_refresh=_parse_datetime(toe_lines.get("last_successful_refresh", None)),
    )


def format_git_root(item: GitRootItem) -> GitRoot:
    root_id = _format_root_id(item)
    group_name = _format_group_name(item)
    organization_name = _format_organization_name(item)
    unreliable_indicators = _format_unreliable_indicators(item)
    return GitRoot(
        cloning=format_cloning(item["cloning"]),
        created_by=item["created_by"],
        created_date=datetime.fromisoformat(item["created_date"]),
        group_name=group_name,
        id=root_id,
        organization_name=organization_name,
        machine=format_machine(item.get("machine")),
        rebase=format_rebase(item.get("rebase")),
        state=format_git_state(item["state"]),
        toe_lines=format_toe_lines(item.get("toe_lines")),
        type=RootType.GIT,
        unreliable_indicators=unreliable_indicators,
    )


def format_ip_root(item: IpRootItem) -> IPRoot:
    root_id = _format_root_id(item)
    group_name = _format_group_name(item)
    organization_name = _format_organization_name(item)
    unreliable_indicators = _format_unreliable_indicators(item)
    return IPRoot(
        created_by=item["created_by"],
        created_date=datetime.fromisoformat(item["created_date"]),
        group_name=group_name,
        id=root_id,
        organization_name=organization_name,
        state=format_ip_state(item["state"]),
        type=RootType.IP,
        unreliable_indicators=unreliable_indicators,
    )


def format_url_root(item: UrlRootItem) -> URLRoot:
    root_id = _format_root_id(item)
    group_name = _format_group_name(item)
    organization_name = _format_organization_name(item)
    unreliable_indicators = _format_unreliable_indicators(item)
    return URLRoot(
        created_by=item["created_by"],
        created_date=datetime.fromisoformat(item["created_date"]),
        group_name=group_name,
        id=root_id,
        organization_name=organization_name,
        state=format_url_state(item["state"]),
        type=RootType.URL,
        unreliable_indicators=unreliable_indicators,
    )


def format_root(item: RootItem) -> Root:
    if item["type"] == "Git":
        return format_git_root(cast(GitRootItem, item))

    if item["type"] == "IP":
        return format_ip_root(cast(IpRootItem, item))

    return format_url_root(cast(UrlRootItem, item))


def format_root_environment_url_type(
    raw: str | None,
) -> RootEnvironmentUrlType:
    if not raw:
        return RootEnvironmentUrlType.URL

    return RootEnvironmentUrlType.CSPM if raw == "CLOUD" else RootEnvironmentUrlType[raw]


def format_environment_url(item: RootEnvironmentUrlItem) -> RootEnvironmentUrl:
    return RootEnvironmentUrl(
        url=item["url"],
        id=item["id"],
        created_at=datetime.fromisoformat(item["created_at"]) if "created_at" in item else None,
        created_by=item.get("created_by", None),
        group_name=item["group_name"],
        root_id=item["root_id"],
        state=format_environment_url_state(item["state"]),
    )


def format_environment_url_state(item: RootEnvironmentUrlStateItem) -> RootEnvironmentUrlState:
    return RootEnvironmentUrlState(
        modified_date=datetime.fromisoformat(item["modified_date"]),
        modified_by=item["modified_by"],
        status=RootEnvironmentUrlStateStatus[item["status"]],
        include=item.get("include", True),
        url_type=format_root_environment_url_type(item.get("url_type")),
        cloud_name=RootEnvironmentCloud[item["cloud_name"]] if "cloud_name" in item else None,
        use_egress=item.get("use_egress"),
        use_vpn=item.get("use_vpn"),
        use_ztna=item.get("use_ztna"),
        is_production=item.get("is_production"),
    )


def format_docker_image(item: RootDockerImageItem) -> RootDockerImage:
    return RootDockerImage(
        uri=item["sk"].split("#")[-1],
        created_at=datetime.fromisoformat(item["created_at"]) if "created_at" in item else None,
        created_by=item["created_by"],
        group_name=item["group_name"],
        root_id=item["root_id"],
        state=format_docker_image_state(item["state"]),
    )


def format_docker_image_state(item: RootDockerImageStateItem) -> RootDockerImageState:
    return RootDockerImageState(
        credential_id=item.get("credential_id"),
        modified_date=datetime.fromisoformat(item["modified_date"]),
        modified_by=item["modified_by"],
        status=RootDockerImageStateStatus[item["status"]],
        include=item.get("include", True),
        digest=item.get("digest"),
        history=[
            DockerImageHistory(
                created=datetime.fromisoformat(x["created"]),
                created_by=x.get("created_by"),
                empty_layer=x.get("empty_layer", False),
                comment=x.get("comment"),
            )
            for x in item.get("history", [])
        ],
        layers=list(item["layers"]) if "layers" in item else [],
    )


def valid_item(item: RootItem) -> bool:
    return bool(item.get("created_by") or item.get("group_name") or item.get("created_date"))


def get_gsi_3(*, group_name: str, state: GitRootState) -> dict[str, str]:
    """Index to get all the roots by group name, sorted by status, url and branch."""
    gsi_3_index = TABLE.indexes["gsi_3"]
    gsi_3_key = keys.build_key(
        facet=GROUP_INDEX_METADATA,
        values={
            "name": group_name,
            "status": state.status.lower(),
            "url": state.url,
            "branch": state.branch,
        },
    )

    return {
        gsi_3_index.primary_key.partition_key: gsi_3_key.partition_key,
        gsi_3_index.primary_key.sort_key: gsi_3_key.sort_key,
    }


def get_uniqueness_constraint(*, group_name: str, state: GitRootState) -> dict[str, str]:
    key_structure = TABLE.primary_key
    url_metadata_key = keys.build_key(
        facet=TABLE.facets["git_root_metadata"],
        values={
            "name": group_name,
            "uuid": f"URL#{state.url}#BRANCH#{state.branch}",
        },
    )

    return {
        key_structure.partition_key: url_metadata_key.partition_key,
        key_structure.sort_key: url_metadata_key.sort_key,
        "modified_by": state.modified_by,
        "modified_date": serialize(state.modified_date),
    }
