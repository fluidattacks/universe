from boto3.dynamodb.conditions import Key

from integrates.db_model import TABLE
from integrates.db_model.roots.remove import remove
from integrates.db_model.roots.types import RootItem
from integrates.dynamodb import operations
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import GitRootFaker, GitRootStateFaker, GroupFaker, OrganizationFaker
from integrates.testing.mocks import mocks


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[
                GroupFaker(name="group1", organization_id="org1"),
                GroupFaker(name="group2", organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    id="root1",
                    group_name="group1",
                    state=GitRootStateFaker(url="https://github.com/a/a", branch="main"),
                ),
            ],
        )
    )
)
async def test_root_removed() -> None:
    # Arrange
    group_name = "group1"
    root_id = "root1"
    url = "https://github.com/a/a"
    branch = "main"
    response = await operations.DynamoClient[RootItem].query(
        condition_expression=(Key("sk").eq(f"GROUP#{group_name}") & Key("pk").begins_with("ROOT#")),
        facets=(TABLE.facets["git_root_metadata"],),
        index=TABLE.indexes["inverted_index"],
        table=TABLE,
    )
    assert len(response.items) == 2  # Root and uniqueness constraint

    # Act
    await remove(
        group_name=group_name,
        root_id=root_id,
        root_state=GitRootStateFaker(url=url, branch=branch),
    )

    # Assert
    response = await operations.DynamoClient[RootItem].query(
        condition_expression=(Key("sk").eq(f"GROUP#{group_name}") & Key("pk").begins_with("ROOT#")),
        facets=(TABLE.facets["git_root_metadata"],),
        index=TABLE.indexes["inverted_index"],
        table=TABLE,
    )
    assert len(response.items) == 0
