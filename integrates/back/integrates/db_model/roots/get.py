from collections.abc import Iterable
from datetime import datetime
from itertools import chain
from typing import Self

from aiodataloader import DataLoader
from aioextensions import collect
from boto3.dynamodb.conditions import Key

from integrates.custom_exceptions import (
    InvalidResourceType,
    RootEnvironmentUrlNotFound,
    SecretValueNotFound,
)
from integrates.db_model import HISTORIC_TABLE, TABLE
from integrates.db_model.items import RootDockerImageItem, RootEnvironmentUrlItem
from integrates.db_model.roots.constants import (
    GROUP_ENVS_INDEX_METADATA,
    GROUP_IMAGES_INDEX_METADATA,
    ORG_INDEX_METADATA,
)
from integrates.db_model.roots.enums import RootEnvironmentUrlStateStatus, RootStatus
from integrates.db_model.roots.types import (
    DockerImageLayersRequest,
    GroupDockerImagesRequest,
    GroupEnvironmentUrlsRequest,
    Root,
    RootDockerImage,
    RootDockerImageLayer,
    RootDockerImageRequest,
    RootDockerImagesRequest,
    RootEnvironmentSecretsRequest,
    RootEnvironmentUrl,
    RootEnvironmentUrlRequest,
    RootEnvironmentUrlsRequest,
    RootItem,
    RootRequest,
    RootState,
    Secret,
)
from integrates.db_model.roots.utils import (
    format_docker_image,
    format_environment_url,
    format_root,
    format_secret_state,
    valid_item,
)
from integrates.dynamodb import keys, operations
from integrates.dynamodb.exceptions import ConditionalCheckFailedException, ValidationException
from integrates.verify.enums import ResourceType


async def _get_roots(*, requests: Iterable[RootRequest]) -> list[Root]:
    primary_keys = tuple(
        keys.build_key(
            facet=TABLE.facets["git_root_metadata"],
            values={"name": request.group_name, "uuid": request.root_id},
        )
        for request in requests
    )
    items = await operations.DynamoClient[RootItem].batch_get_item(keys=primary_keys, table=TABLE)

    return [format_root(item) for item in items if valid_item(item)]


class RootLoader(DataLoader[RootRequest, Root | None]):
    async def batch_load_fn(self, requests: Iterable[RootRequest]) -> list[Root | None]:
        roots = {root.id: root for root in await _get_roots(requests=requests)}

        return [roots.get(request.root_id) for request in requests]


async def _get_group_roots(
    *,
    group_name: str,
    root_dataloader: RootLoader,
) -> list[Root]:
    primary_key = keys.build_key(
        facet=TABLE.facets["git_root_metadata"],
        values={"name": group_name},
    )

    index = TABLE.indexes["inverted_index"]
    key_structure = index.primary_key
    response = await operations.DynamoClient[RootItem].query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.sort_key)
            & Key(key_structure.sort_key).begins_with(primary_key.partition_key)
        ),
        facets=(
            TABLE.facets["git_root_metadata"],
            TABLE.facets["ip_root_metadata"],
            TABLE.facets["url_root_metadata"],
        ),
        index=index,
        table=TABLE,
    )

    roots: list[Root] = []
    for item in response.items:
        if not valid_item(item):
            continue
        root = format_root(item)
        roots.append(root)
        root_dataloader.prime(RootRequest(group_name=root.group_name, root_id=root.id), root)

    return roots


class GroupRootsLoader(DataLoader[str, list[Root]]):
    def __init__(self, dataloader: RootLoader) -> None:
        super().__init__()
        self.dataloader = dataloader

    async def load_many_chained(self, group_names: Iterable[str]) -> list[Root]:
        unchained_data = await self.load_many(group_names)
        return list(chain.from_iterable(unchained_data))

    async def batch_load_fn(self, group_names: Iterable[str]) -> list[list[Root]]:
        return list(
            await collect(
                tuple(
                    _get_group_roots(group_name=group_name, root_dataloader=self.dataloader)
                    for group_name in group_names
                ),
                workers=32,
            ),
        )


async def _get_organization_roots(
    *,
    organization_name: str,
    root_dataloader: RootLoader,
) -> list[Root]:
    primary_key = keys.build_key(
        facet=ORG_INDEX_METADATA,
        values={"name": organization_name},
    )

    index = TABLE.indexes["gsi_2"]
    key_structure = index.primary_key
    response = await operations.DynamoClient[RootItem].query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(
            TABLE.facets["git_root_metadata"],
            TABLE.facets["ip_root_metadata"],
            TABLE.facets["url_root_metadata"],
        ),
        index=index,
        table=TABLE,
    )

    roots: list[Root] = []
    for item in response.items:
        if not valid_item(item):
            continue
        root = format_root(item)
        roots.append(root)
        root_dataloader.prime(RootRequest(group_name=root.group_name, root_id=root.id), root)

    return roots


class OrganizationRootsLoader(DataLoader[str, list[Root]]):
    def __init__(self, dataloader: RootLoader) -> None:
        super().__init__()
        self.dataloader = dataloader

    async def batch_load_fn(self, organization_names: Iterable[str]) -> list[list[Root]]:
        return list(
            await collect(
                _get_organization_roots(
                    organization_name=organization_name,
                    root_dataloader=self.dataloader,
                )
                for organization_name in organization_names
            ),
        )


async def _get_historic_state(request: RootRequest) -> list[RootState]:
    primary_key = keys.build_key(
        facet=HISTORIC_TABLE.facets["git_root_state"],
        values={
            "uuid": request.root_id,
            "group_name": request.group_name,
            "state": "state",
        },
    )
    key_structure = HISTORIC_TABLE.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(
            HISTORIC_TABLE.facets["git_root_state"],
            HISTORIC_TABLE.facets["ip_root_state"],
            HISTORIC_TABLE.facets["url_root_state"],
        ),
        table=HISTORIC_TABLE,
    )

    states = [
        RootState(
            modified_by=state["modified_by"],
            modified_date=datetime.fromisoformat(state["modified_date"]),
            nickname=state.get("nickname"),
            other=state.get("other"),
            reason=state.get("reason"),
            status=RootStatus[state["status"]],
            credential_id=state.get("credential_id"),
        )
        for state in response.items
    ]
    return states


class RootHistoricStateLoader(DataLoader[RootRequest, list[RootState]]):
    async def batch_load_fn(self, requests: Iterable[RootRequest]) -> list[list[RootState]]:
        return list(
            await collect(
                tuple(map(_get_historic_state, requests)),
                workers=32,
            )
        )


async def _get_secrets(*, root_id: str, secret_key: str | None = None) -> list[Secret]:
    primary_key = keys.build_key(
        facet=TABLE.facets["root_secret"],
        values={
            "uuid": root_id,
            **({"key": secret_key} if secret_key else {}),
        },
    )
    key_structure = TABLE.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & (
                Key(key_structure.sort_key).eq(primary_key.sort_key)
                if secret_key
                else Key(key_structure.sort_key).begins_with(primary_key.sort_key)
            )
        ),
        facets=(TABLE.facets["root_secret"],),
        table=TABLE,
    )

    return [
        Secret(
            key=item["key"],
            value=item["value"],
            created_at=datetime.fromisoformat(item["created_at"]),
            state=format_secret_state(item["state"]),
        )
        for item in response.items
    ]


class RootSecretsLoader(DataLoader[str, list[Secret]]):
    async def batch_load_fn(
        self,
        root_ids: Iterable[str],
    ) -> list[list[Secret]]:
        return list(await collect(_get_secrets(root_id=root_id) for root_id in root_ids))


async def _get_environment_secrets(*, request: RootEnvironmentSecretsRequest) -> list[Secret]:
    key_structure = TABLE.primary_key
    primary_key = keys.build_key(
        facet=TABLE.facets["root_environment_secret"],
        values={
            "group_name": request.group_name,
            "hash": request.url_id,
        },
    )
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(TABLE.facets["root_environment_secret"],),
        table=TABLE,
    )
    return [
        Secret(
            key=item["key"],
            value=item["value"],
            created_at=datetime.fromisoformat(item["created_at"]),
            state=format_secret_state(item["state"]),
        )
        for item in response.items
    ]


class GitEnvironmentSecretsLoader(DataLoader[RootEnvironmentSecretsRequest, list[Secret]]):
    async def batch_load_fn(
        self,
        requests: Iterable[RootEnvironmentSecretsRequest],
    ) -> list[list[Secret]]:
        return list(
            await collect(
                tuple(_get_environment_secrets(request=request) for request in requests),
                workers=32,
            ),
        )

    async def load_many_chained(
        self,
        requests: Iterable[RootEnvironmentSecretsRequest],
    ) -> list[Secret]:
        unchained_data = await self.load_many(requests)
        return list(chain.from_iterable(unchained_data))


async def _get_group_environment_urls(
    *,
    request: GroupEnvironmentUrlsRequest,
) -> list[RootEnvironmentUrl]:
    index = TABLE.indexes["gsi_2"]
    primary_key = keys.build_key(
        facet=GROUP_ENVS_INDEX_METADATA,
        values={
            "group_name": request.group_name,
        },
    )
    key_structure = index.primary_key
    response = await operations.DynamoClient[RootEnvironmentUrlItem].query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(GROUP_ENVS_INDEX_METADATA,),
        table=TABLE,
        index=index,
    )
    return [format_environment_url(item) for item in response.items]


class GroupEnvironmentUrlsLoader(DataLoader[GroupEnvironmentUrlsRequest, list[RootEnvironmentUrl]]):
    async def load_many_chained(
        self,
        requests: Iterable[GroupEnvironmentUrlsRequest],
    ) -> list[RootEnvironmentUrl]:
        unchained_data = await self.load_many(requests)
        return list(chain.from_iterable(unchained_data))

    async def batch_load_fn(
        self,
        requests: Iterable[GroupEnvironmentUrlsRequest],
    ) -> list[list[RootEnvironmentUrl]]:
        return list(
            await collect(_get_group_environment_urls(request=request) for request in requests),
        )


class GroupEnvironmentUrlsNonDeletedLoader(
    DataLoader[GroupEnvironmentUrlsRequest, list[RootEnvironmentUrl]],
):
    def __init__(self, dataloader: GroupEnvironmentUrlsLoader) -> None:
        super().__init__()
        self.dataloader = dataloader

    def clear(self, key: GroupEnvironmentUrlsRequest) -> Self:
        self.dataloader.clear(key)
        return super().clear(key)

    def clear_all(self) -> Self:
        self.dataloader.clear_all()
        return super().clear_all()

    async def load_many_chained(
        self,
        requests: Iterable[GroupEnvironmentUrlsRequest],
    ) -> list[RootEnvironmentUrl]:
        unchained_data = await self.load_many(requests)
        return list(chain.from_iterable(unchained_data))

    async def batch_load_fn(
        self,
        requests: Iterable[GroupEnvironmentUrlsRequest],
    ) -> list[list[RootEnvironmentUrl]]:
        env_urls_by_groups = await self.dataloader.load_many(requests)
        return list(
            [
                env_url
                for env_url in env_urls_by_group
                if env_url.state.status is not RootEnvironmentUrlStateStatus.DELETED
            ]
            for env_urls_by_group in env_urls_by_groups
        )


async def _get_root_environment_urls(
    *,
    root_id: str,
    group_name: str,
    url_id: str | None = None,
) -> list[RootEnvironmentUrl]:
    primary_key = keys.build_key(
        facet=TABLE.facets["root_environment_url"],
        values={
            "uuid": root_id,
            "group_name": group_name,
            **({"hash": url_id} if url_id else {}),
        },
    )
    key_structure = TABLE.primary_key
    response = await operations.DynamoClient[RootEnvironmentUrlItem].query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & (
                Key(key_structure.sort_key).eq(primary_key.sort_key)
                if url_id
                else Key(key_structure.sort_key).begins_with(primary_key.sort_key)
            )
        ),
        facets=(TABLE.facets["root_environment_url"],),
        table=TABLE,
    )
    return [format_environment_url(item) for item in response.items]


class RootEnvironmentUrlsLoader(DataLoader[RootEnvironmentUrlsRequest, list[RootEnvironmentUrl]]):
    async def load_many_chained(
        self,
        requests: Iterable[RootEnvironmentUrlsRequest],
    ) -> list[RootEnvironmentUrl]:
        unchained_data = await self.load_many(requests)
        return list(chain.from_iterable(unchained_data))

    async def batch_load_fn(
        self,
        requests: Iterable[RootEnvironmentUrlsRequest],
    ) -> list[list[RootEnvironmentUrl]]:
        return list(
            await collect(
                _get_root_environment_urls(root_id=request.root_id, group_name=request.group_name)
                for request in requests
            ),
        )


class RootEnvironmentUrlsNonDeletedLoader(
    DataLoader[RootEnvironmentUrlsRequest, list[RootEnvironmentUrl]],
):
    def __init__(self, dataloader: RootEnvironmentUrlsLoader) -> None:
        super().__init__()
        self.dataloader = dataloader

    def clear(self, key: RootEnvironmentUrlsRequest) -> Self:
        self.dataloader.clear(key)
        return super().clear(key)

    def clear_all(self) -> Self:
        self.dataloader.clear_all()
        return super().clear_all()

    async def load_many_chained(
        self,
        requests: Iterable[RootEnvironmentUrlsRequest],
    ) -> list[RootEnvironmentUrl]:
        unchained_data = await self.load_many(requests)
        return list(chain.from_iterable(unchained_data))

    async def batch_load_fn(
        self,
        requests: Iterable[RootEnvironmentUrlsRequest],
    ) -> list[list[RootEnvironmentUrl]]:
        env_urls_by_roots = await self.dataloader.load_many(requests)
        return list(
            [
                env_url
                for env_url in env_urls_by_root
                if env_url.state.status is not RootEnvironmentUrlStateStatus.DELETED
            ]
            for env_urls_by_root in env_urls_by_roots
        )


async def _get_root_environment_url(
    *,
    root_id: str,
    group_name: str,
    url_id: str,
) -> RootEnvironmentUrl:
    root_environment_urls = await _get_root_environment_urls(
        root_id=root_id,
        group_name=group_name,
        url_id=url_id,
    )
    if not root_environment_urls:
        raise RootEnvironmentUrlNotFound()
    return root_environment_urls[0]


class RootEnvironmentUrlLoader(DataLoader[RootEnvironmentUrlRequest, RootEnvironmentUrl]):
    async def batch_load_fn(
        self,
        requests: Iterable[RootEnvironmentUrlRequest],
    ) -> list[RootEnvironmentUrl]:
        return list(
            await collect(
                _get_root_environment_url(
                    root_id=request.root_id,
                    group_name=request.group_name,
                    url_id=request.url_id,
                )
                for request in requests
            ),
        )


async def _get_root_docker_image_by_uri(
    *,
    requests: Iterable[RootDockerImageRequest],
) -> list[RootDockerImage | None]:
    primary_keys = tuple(
        keys.build_key(
            facet=TABLE.facets["root_docker_image"],
            values={
                "uuid": request.root_id,
                "group_name": request.group_name,
                "uri": request.uri_id,
            },
        )
        for request in requests
    )
    try:
        items = await operations.DynamoClient[RootDockerImageItem].batch_get_item(
            keys=primary_keys, table=TABLE
        )
    except (
        ConditionalCheckFailedException,
        ValidationException,
    ):
        return [None]
    response = {
        (image.root_id, image.group_name, image.uri): image
        for image in [format_docker_image(item) for item in items]
    }
    return [
        response.get((request.root_id, request.group_name, request.uri_id)) for request in requests
    ]


async def _get_root_docker_images(
    *,
    root_id: str,
    group_name: str,
) -> list[RootDockerImage]:
    primary_key = keys.build_key(
        facet=TABLE.facets["root_docker_image"],
        values={
            "uuid": root_id,
            "group_name": group_name,
        },
    )
    key_structure = TABLE.primary_key
    response = await operations.DynamoClient[RootDockerImageItem].query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & (Key(key_structure.sort_key).begins_with(primary_key.sort_key))
        ),
        facets=(TABLE.facets["root_docker_image"],),
        table=TABLE,
    )
    return [format_docker_image(item) for item in response.items]


async def _get_group_docker_images(*, request: GroupDockerImagesRequest) -> list[RootDockerImage]:
    index = TABLE.indexes["gsi_2"]
    primary_key = keys.build_key(
        facet=GROUP_IMAGES_INDEX_METADATA,
        values={
            "group_name": request.group_name,
        },
    )
    key_structure = index.primary_key
    response = await operations.DynamoClient[RootDockerImageItem].query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(GROUP_IMAGES_INDEX_METADATA,),
        table=TABLE,
        index=index,
    )
    return [format_docker_image(item) for item in response.items]


async def get_docker_image_layers(
    group_name: str,
    root_id: str,
    image_uri: str,
) -> list[RootDockerImageLayer]:
    primary_key = keys.build_key(
        facet=TABLE.facets["root_docker_image_layer"],
        values={
            "uuid": root_id,
            "group_name": group_name,
            "uri": image_uri,
        },
    )
    key_structure = TABLE.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & (Key(key_structure.sort_key).begins_with(primary_key.sort_key))
        ),
        facets=(TABLE.facets["root_docker_image_layer"],),
        table=TABLE,
    )
    return [
        RootDockerImageLayer(image_uri=image_uri, digest=item["digest"], size=item["size"])
        for item in response.items
    ]


class RootDockerImageLoader(DataLoader[RootDockerImageRequest, RootDockerImage | None]):
    async def batch_load_fn(
        self,
        requests: Iterable[RootDockerImageRequest],
    ) -> list[RootDockerImage | None]:
        return await _get_root_docker_image_by_uri(requests=requests)


class RootDockerImagesLoader(DataLoader[RootDockerImagesRequest, list[RootDockerImage]]):
    async def load_many_chained(
        self,
        requests: Iterable[RootDockerImagesRequest],
    ) -> list[RootDockerImage]:
        unchained_data = await self.load_many(requests)
        return list(chain.from_iterable(unchained_data))

    async def batch_load_fn(
        self,
        requests: Iterable[RootDockerImagesRequest],
    ) -> list[list[RootDockerImage]]:
        return list(
            await collect(
                _get_root_docker_images(root_id=request.root_id, group_name=request.group_name)
                for request in requests
            ),
        )


class GroupDockerImagesLoader(DataLoader[GroupDockerImagesRequest, list[RootDockerImage]]):
    async def load_many_chained(
        self,
        requests: Iterable[GroupDockerImagesRequest],
    ) -> list[RootDockerImage]:
        unchained_data = await self.load_many(requests)
        return list(chain.from_iterable(unchained_data))

    async def batch_load_fn(
        self,
        requests: Iterable[GroupDockerImagesRequest],
    ) -> list[list[RootDockerImage]]:
        return list(
            await collect(_get_group_docker_images(request=request) for request in requests),
        )


class DockerImageLayersLoader(DataLoader[DockerImageLayersRequest, list[RootDockerImageLayer]]):
    async def batch_load_fn(
        self,
        requests: Iterable[DockerImageLayersRequest],
    ) -> list[list[RootDockerImageLayer]]:
        return list(
            await collect(
                get_docker_image_layers(
                    group_name=request.group_name,
                    root_id=request.root_id,
                    image_uri=request.image_uri,
                )
                for request in requests
            ),
        )


async def get_secret_by_key(
    *,
    secret_key: str,
    group_name: str,
    resource_id: str,
    resource_type: ResourceType,
) -> Secret:
    secret_list: list[Secret] | None = None
    if resource_type == ResourceType.URL:
        secret_list = await _get_environment_secrets(
            request=RootEnvironmentSecretsRequest(group_name=group_name, url_id=resource_id),
        )
    elif resource_type == ResourceType.ROOT:
        secret_list = await _get_secrets(root_id=resource_id, secret_key=secret_key)
    else:
        raise InvalidResourceType()

    if secret_list:
        for secret in secret_list:
            if secret.key == secret_key:
                return secret

    secret_list = None
    raise SecretValueNotFound()


async def get_git_environment_url_by_id(
    *,
    url_id: str,
    group_name: str,
    root_id: str | None = None,
) -> RootEnvironmentUrl | None:
    key_structure = TABLE.primary_key
    primary_key = keys.build_key(
        facet=TABLE.facets["root_environment_url"],
        values={
            "hash": url_id,
            "group_name": group_name,
            **({"uuid": root_id} if root_id else {}),
        },
    )
    index = TABLE.indexes["inverted_index"]
    new_response = await operations.DynamoClient[RootEnvironmentUrlItem].query(
        condition_expression=(
            Key(key_structure.sort_key).eq(primary_key.sort_key)
            & (
                Key(key_structure.partition_key).eq(primary_key.partition_key)
                if root_id
                else Key(key_structure.partition_key).begins_with(primary_key.partition_key)
            )
        ),
        facets=(TABLE.facets["root_environment_url"],),
        table=TABLE,
        index=index,
    )
    items = [format_environment_url(item) for item in new_response.items]
    items = [
        item for item in items if item.state.status is not RootEnvironmentUrlStateStatus.DELETED
    ]
    if not items:
        return None
    return items[0]
