from typing import (
    NamedTuple,
)

from integrates.dynamodb.types import (
    PageInfo,
)


class CriteriaRequirementMainData(NamedTuple):
    description: str
    summary: str
    title: str


class CriteriaRequirementSupport(NamedTuple):
    advanced: bool
    essential: bool


class CriteriaRequirement(NamedTuple):
    category: str
    en: CriteriaRequirementMainData
    es: CriteriaRequirementMainData
    requirement_number: str
    references: list[str]
    supported_in: CriteriaRequirementSupport


class CriteriaRequirementEdge(NamedTuple):
    cursor: str
    node: CriteriaRequirement


class CriteriaRequirementsConnection(NamedTuple):
    edges: tuple[CriteriaRequirementEdge, ...]
    page_info: PageInfo
    total: int
