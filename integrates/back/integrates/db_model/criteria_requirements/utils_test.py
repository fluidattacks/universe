from integrates.custom_exceptions import InvalidCriteriaRange, InvalidParameter
from integrates.db_model.criteria_requirements.utils import (
    format_criteria_requirements,
    prepare_requirements_info,
)
from integrates.testing.utils import parametrize, raises


def test_prepare_requirements_info() -> None:
    requirements_info = {
        "023": {"category": "Session"},
        "024": {"category": "Session"},
        "025": {"category": "Session"},
    }
    first = 2
    after = "023"
    sliced_requirements, end_cursor, has_next_page = prepare_requirements_info(
        first=first, after=after, requirements_info=requirements_info
    )
    assert sliced_requirements == {
        "024": {"category": "Session"},
        "025": {"category": "Session"},
    }
    assert end_cursor == "025"
    assert has_next_page is False


def test_prepare_requirements_info_invalid_criteria_range() -> None:
    requirements_info = {
        "023": {"category": "Session"},
        "024": {"category": "Session"},
        "025": {"category": "Session"},
    }
    first = 0
    after = "023"
    with raises(InvalidCriteriaRange):
        prepare_requirements_info(first=first, after=after, requirements_info=requirements_info)


def test_prepare_requirements_info_invalid_parameter() -> None:
    requirements_info = {
        "023": {"category": "Session"},
        "024": {"category": "Session"},
        "025": {"category": "Session"},
    }
    first = 2
    after = "022"
    with raises(InvalidParameter):
        prepare_requirements_info(first=first, after=after, requirements_info=requirements_info)


@parametrize(
    args=[
        "category",
        "en_description",
        "en_summary",
        "en_title",
        "es_description",
        "es_summary",
        "es_title",
        "requirement_number",
        "references",
        "supported_in",
    ],
    cases=[
        [
            "category",
            "English description",
            "English summary",
            "English title",
            "Spanish description",
            "Spanish summary",
            "Spanish title",
            "1",
            ["ref1", "ref2"],
            {"advanced": True, "essential": False},
        ]
    ],
)
def test_format_criteria_requirements(
    category: str,
    en_description: str,
    en_summary: str,
    en_title: str,
    es_description: str,
    es_summary: str,
    es_title: str,
    requirement_number: str,
    references: list[str],
    supported_in: dict[str, bool],
) -> None:
    item = {
        "category": category,
        "en": {
            "description": en_description,
            "summary": en_summary,
            "title": en_title,
        },
        "es": {
            "description": es_description,
            "summary": es_summary,
            "title": es_title,
        },
        "references": references,
        "supported_in": supported_in,
    }
    criteria_requirement = format_criteria_requirements(
        requirement_number=requirement_number, item=item
    )
    assert criteria_requirement.category == category
    assert criteria_requirement.en.description == en_description
    assert criteria_requirement.en.summary == en_summary
    assert criteria_requirement.en.title == en_title
    assert criteria_requirement.es.description == es_description
    assert criteria_requirement.es.summary == es_summary
    assert criteria_requirement.es.title == es_title
    assert criteria_requirement.requirement_number == requirement_number
    assert criteria_requirement.references == references
    assert criteria_requirement.supported_in.advanced == supported_in["advanced"]
    assert criteria_requirement.supported_in.essential == supported_in["essential"]
