import itertools

from integrates.class_types.types import (
    Item,
)
from integrates.custom_exceptions import (
    InvalidCriteriaRange,
    InvalidParameter,
)
from integrates.db_model.criteria_requirements.types import (
    CriteriaRequirement,
    CriteriaRequirementMainData,
    CriteriaRequirementSupport,
)


def split_requirements(requirements: dict, after: str) -> dict:
    splitted_requirements = {key: item for key, item in requirements.items() if key > after}
    return splitted_requirements


def prepare_requirements_info(
    first: int | None,
    after: str,
    requirements_info: dict,
) -> tuple[dict, str, bool]:
    min_items = 1
    if first is not None and first < min_items:
        expr = f'The "first" field must be greater than {min_items}'
        raise InvalidCriteriaRange(expr=expr)
    if after != "" and requirements_info.get(after) is None:
        raise InvalidParameter(field="after")

    splitted_requirements = split_requirements(requirements=requirements_info, after=after)
    sliced_requirements = (
        splitted_requirements
        if first is None
        else dict(itertools.islice(splitted_requirements.items(), first))
    )
    end_cursor = list(sliced_requirements.keys())[-1]
    has_next_page = (
        len(split_requirements(requirements=splitted_requirements, after=end_cursor)) >= 1
    )
    return sliced_requirements, end_cursor, has_next_page


def format_criteria_requirements(requirement_number: str, item: Item) -> CriteriaRequirement:
    return CriteriaRequirement(
        category=item["category"],
        en=CriteriaRequirementMainData(
            description=item["en"]["description"],
            summary=item["en"]["summary"],
            title=item["en"]["title"],
        ),
        es=CriteriaRequirementMainData(
            description=item["es"]["description"],
            summary=item["es"]["summary"],
            title=item["es"]["title"],
        ),
        requirement_number=requirement_number,
        references=item["references"],
        supported_in=CriteriaRequirementSupport(
            advanced=item["supported_in"]["advanced"],
            essential=item["supported_in"]["essential"],
        ),
    )
