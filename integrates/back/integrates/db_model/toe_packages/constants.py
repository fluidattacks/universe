from integrates.db_model import (
    TABLE,
)
from integrates.dynamodb.types import (
    Facet,
)

GSI_2_FACET = Facet(
    attrs=TABLE.facets["toe_packages_metadata"].attrs,
    pk_alias="GROUP#group_name",
    sk_alias=("PKGS#PRESENT#be_present#VULNERABLE#vulnerable#"),
)
