from decimal import (
    Decimal,
)

import simplejson as json
from aioextensions import (
    collect,
)
from boto3.dynamodb.conditions import (
    Attr,
)

from integrates.audit import AuditEvent, add_audit_event
from integrates.class_types.types import (
    Item,
)
from integrates.custom_exceptions import (
    ToePackageAlreadyUpdated,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.toe_packages.add import (
    add_docker_image_package,
)
from integrates.db_model.toe_packages.constants import (
    GSI_2_FACET,
)
from integrates.db_model.toe_packages.remove import (
    remove_docker_image_package,
)
from integrates.db_model.toe_packages.types import (
    ToePackage,
)
from integrates.db_model.utils import (
    get_as_utc_iso_format,
    serialize,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.exceptions import (
    ConditionalCheckFailedException,
)


async def update_root_docker_image_pkgs_facet(
    current_value: ToePackage,
    package: ToePackage,
) -> None:
    current_locations = current_value.locations
    new_locations = package.locations

    new_locations = [loc for loc in new_locations if loc not in current_locations]
    removed_locations = [loc for loc in current_locations if loc not in new_locations]
    if removed_locations:
        await collect(
            tuple(
                remove_docker_image_package(uri=location.image_ref, toe_package=current_value)
                for location in removed_locations
                if location.image_ref
            ),
        )
    if new_locations:
        await collect(
            tuple(
                add_docker_image_package(uri=location.image_ref, toe_package=current_value)
                for location in new_locations
                if location.image_ref
            ),
        )


async def update_package(
    *,
    current_value: ToePackage,
    package: ToePackage,
) -> None:
    key_structure = TABLE.primary_key
    metadata_key = keys.build_key(
        facet=TABLE.facets["toe_packages_metadata"],
        values={
            "group_name": current_value.group_name,
            "root_id": current_value.root_id,
            "name": current_value.name,
            "version": current_value.version,
        },
    )
    gsi_2_key = keys.build_key(
        facet=GSI_2_FACET,
        values={
            "group_name": current_value.group_name,
            "be_present": str(package.be_present).lower(),
            "vulnerable": str(package.vulnerable).lower(),
        },
    )
    gsi_2_index = TABLE.indexes["gsi_2"]
    new_package_item: Item = json.loads(json.dumps(package, default=serialize), parse_float=Decimal)
    metadata_item = {
        gsi_2_index.primary_key.sort_key: gsi_2_key.sort_key,
        **new_package_item,
    }
    condition_expression = Attr(key_structure.partition_key).exists() & Attr("modified_date").eq(
        get_as_utc_iso_format(current_value.modified_date),
    )
    try:
        await operations.update_item(
            condition_expression=condition_expression,
            item=metadata_item,
            key=metadata_key,
            table=TABLE,
        )
        add_audit_event(
            AuditEvent(
                action="UPDATE",
                author="unknown",
                metadata=metadata_item,
                object="ToePackage",
                object_id=metadata_key.sort_key,
            )
        )
    except ConditionalCheckFailedException as ex:
        raise ToePackageAlreadyUpdated() from ex

    await update_root_docker_image_pkgs_facet(current_value, package)
