from datetime import (
    datetime,
)
from decimal import (
    Decimal,
)
from typing import (
    NamedTuple,
)

from integrates.dynamodb.types import (
    PageInfo,
)


class ToePackageAdvisory(NamedTuple):
    cpes: list[str]
    description: str | None
    epss: Decimal | None
    id: str
    namespace: str
    percentile: Decimal | None
    severity: str
    urls: list[str]
    version_constraint: str | None


class Digest(NamedTuple):
    algorithm: str | None
    value: str | None


class Artifact(NamedTuple):
    url: str
    integrity: Digest | None = None


class ToePackageHealthMetadata(NamedTuple):
    artifact: Artifact | None = None
    authors: str | None = None
    latest_version_created_at: str | None = None
    latest_version: str | None = None


class ToePackageCoordinates(NamedTuple):
    path: str
    scope: str | None = None
    dependency_type: str | None = None
    line: str | None = None
    layer: str | None = None
    image_ref: str | None = None


class GroupToePackagesRequest(NamedTuple):
    group_name: str
    be_present: bool | None = None
    vulnerable: bool | None = None
    after: str | None = None
    first: int | None = None
    paginate: bool = False


class RootToePackagesRequest(NamedTuple):
    group_name: str
    root_id: str
    be_present: bool | None = None
    vulnerable: bool | None = None
    after: str | None = None
    first: int | None = None
    paginate: bool = False


class RootDockerImagePackagesRequest(NamedTuple):
    uri: str
    root_id: str


class ToePackageRequest(NamedTuple):
    group_name: str
    root_id: str
    name: str
    version: str


class ToePackageMetadataToUpdate(NamedTuple):
    be_present: bool | None = None
    lines: list[str] | None = None
    purl: str | None = None
    url: str | None = None
    vulnerable: bool | None = None
    vulnerability_ids: set[str] | None = None
    locations: list[ToePackageCoordinates] | None = None


class ToePackageVulnerabilityInfo(NamedTuple):
    id: str
    cve: list[str]
    severity_score: Decimal


class ToePackage(NamedTuple):
    id: str
    be_present: bool
    group_name: str
    root_id: str
    name: str
    version: str
    type_: str
    language: str
    modified_date: datetime
    locations: list[ToePackageCoordinates]
    found_by: str | None = None
    outdated: bool | None = None
    package_advisories: list[ToePackageAdvisory] = []
    package_url: str | None = None
    platform: str | None = None
    licenses: list[str] | None = None
    url: str | None = None
    vulnerable: bool | None = None
    has_related_vulnerabilities: bool | None = None
    vulnerability_ids: set[str] | None = None
    health_metadata: ToePackageHealthMetadata | None = None


class ToePackageEdge(NamedTuple):
    node: ToePackage
    cursor: str


class ToePackagesConnection(NamedTuple):
    edges: tuple[ToePackageEdge, ...]
    page_info: PageInfo
    total: int | None = None


class ToePackageVulnerabilityRequest(NamedTuple):
    group_name: str
    root_id: str
    name: str
    version: str


class ToePackageVulnerability(NamedTuple):
    root_id: str
    name: str
    version: str
    vuln_id: str


class RootDockerImagePkg(NamedTuple):
    uri: str
    root_id: str
    name: str
    version: str
