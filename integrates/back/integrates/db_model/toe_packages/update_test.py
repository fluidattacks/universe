from integrates.db_model.toe_packages.get import _get_package
from integrates.db_model.toe_packages.types import ToePackageRequest
from integrates.db_model.toe_packages.update import update_package
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import DATE_2025, ToePackageFaker, ToePackageHealthMetadataFaker
from integrates.testing.mocks import mocks


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            toe_packages=[
                ToePackageFaker(
                    group_name="group1",
                    root_id="root1",
                    name="test-package",
                    version="1.5.0",
                    outdated=False,
                    licenses=["Apache-1.0"],
                ),
            ],
        ),
    )
)
async def test_update_packages() -> None:
    # Arrange
    toe_package = (
        await _get_package(
            [
                ToePackageRequest(
                    group_name="group1",
                    name="test-package",
                    version="1.5.0",
                    root_id="root1",
                ),
            ],
        )
    )[0]
    assert toe_package

    # Act
    await update_package(
        current_value=toe_package,
        package=toe_package._replace(
            licenses=["MIT"],
            outdated=True,
            health_metadata=ToePackageHealthMetadataFaker(
                latest_version="5.0.0",
                latest_version_created_at=DATE_2025.isoformat(),
            ),
        ),
    )

    toe_package_updated = (
        await _get_package(
            [
                ToePackageRequest(
                    group_name="group1",
                    name="test-package",
                    version="1.5.0",
                    root_id="root1",
                ),
            ],
        )
    )[0]
    assert toe_package_updated

    # Assert
    assert toe_package_updated.licenses == ["MIT"]
    assert toe_package_updated.outdated is True
    assert toe_package_updated.health_metadata is not None
    assert toe_package_updated.health_metadata.latest_version == "5.0.0"
    assert toe_package_updated.health_metadata.latest_version_created_at == DATE_2025.isoformat()
