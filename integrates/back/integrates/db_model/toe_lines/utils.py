import re
from datetime import datetime

from integrates.custom_exceptions import InvalidRegexSearch
from integrates.db_model.historic_items import ToeLinesStateItem as HistoricToeLinesStateItem
from integrates.db_model.items import SortsSuggestionItem, ToeLinesItem, ToeLinesStateItem
from integrates.db_model.toe_lines.types import (
    SortsSuggestion,
    ToeLine,
    ToeLineEdge,
    ToeLineEnrichedState,
    ToeLineState,
)
from integrates.db_model.utils import format_key_from_pk_search, get_as_utc_iso_format
from integrates.dynamodb.types import Index, PrimaryKey, Table
from integrates.dynamodb.utils import get_cursor


def format_toe_lines_sorts_suggestions(
    suggestions: list[SortsSuggestionItem],
) -> list[SortsSuggestion]:
    return [
        SortsSuggestion(
            finding_title=suggestion["finding_title"],
            probability=int(suggestion["probability"]),
        )
        for suggestion in suggestions
    ]


def format_toe_lines_state(item: ToeLinesStateItem) -> ToeLineState:
    return ToeLineState(
        attacked_at=datetime.fromisoformat(item["attacked_at"])
        if item.get("attacked_at")
        else None,
        attacked_by=item.get("attacked_by", ""),
        attacked_lines=int(item["attacked_lines"]),
        be_present=item["be_present"],
        be_present_until=datetime.fromisoformat(item["be_present_until"])
        if item.get("be_present_until")
        else None,
        comments=item["comments"],
        first_attack_at=datetime.fromisoformat(item["first_attack_at"])
        if item.get("first_attack_at")
        else None,
        has_vulnerabilities=item.get("has_vulnerabilities", False),
        last_author=item["last_author"],
        last_commit=item["last_commit"],
        last_commit_date=datetime.fromisoformat(
            item.get("last_commit_date", item["modified_date"]),
        ),
        loc=int(item["loc"]),
        modified_by=item["modified_by"],
        modified_date=datetime.fromisoformat(item["modified_date"]),
        seen_at=datetime.fromisoformat(item["seen_at"]),
        sorts_risk_level=int(item.get("sorts_risk_level", "-1")),
        sorts_priority_factor=int(item.get("sorts_priority_factor", "-1")),
        sorts_risk_level_date=datetime.fromisoformat(item["sorts_risk_level_date"])
        if item.get("sorts_risk_level_date")
        else None,
        sorts_suggestions=format_toe_lines_sorts_suggestions(item["sorts_suggestions"])
        if item.get("sorts_suggestions")
        else None,
    )


def format_toe_lines(item: ToeLinesItem) -> ToeLine:
    state_item = item["state"]
    return ToeLine(
        filename=item["filename"],
        group_name=item["group_name"],
        root_id=item["root_id"],
        seen_first_time_by=item.get("seen_first_time_by"),
        state=format_toe_lines_state(state_item),
    )


def format_toe_lines_enriched_state(
    item: HistoricToeLinesStateItem,
) -> ToeLineEnrichedState:
    pk_search = re.search(
        r"^GROUP(#.*)?#LINES#ROOT(#.*)?#FILENAME(#.*)?$",
        item["pk"],
    )
    if not pk_search:
        raise InvalidRegexSearch()
    return ToeLineEnrichedState(
        filename=format_key_from_pk_search(pk_search.group(3)),
        group_name=format_key_from_pk_search(pk_search.group(1)),
        root_id=format_key_from_pk_search(pk_search.group(2)),
        state=format_toe_lines_state(item),
    )


def format_toe_lines_edge(
    index: Index | None,
    item: ToeLinesItem,
    table: Table,
) -> ToeLineEdge:
    return ToeLineEdge(node=format_toe_lines(item), cursor=get_cursor(index, item, table))


def format_toe_lines_sorts_suggestions_item(
    suggestions: list[SortsSuggestion],
) -> list[SortsSuggestionItem]:
    return [
        {
            "finding_title": suggestion.finding_title,
            "probability": suggestion.probability,
        }
        for suggestion in suggestions
    ]


def format_toe_lines_state_item(state: ToeLineState) -> ToeLinesStateItem:
    item: ToeLinesStateItem = {
        "attacked_by": state.attacked_by,
        "attacked_lines": state.attacked_lines,
        "be_present": state.be_present,
        "comments": state.comments,
        "has_vulnerabilities": state.has_vulnerabilities,
        "last_author": state.last_author,
        "last_commit": state.last_commit,
        "last_commit_date": get_as_utc_iso_format(state.last_commit_date),
        "loc": state.loc,
        "modified_by": state.modified_by,
        "modified_date": get_as_utc_iso_format(state.modified_date),
        "seen_at": get_as_utc_iso_format(state.seen_at),
        "sorts_risk_level": state.sorts_risk_level,
    }
    if state.attacked_at is not None:
        item["attacked_at"] = get_as_utc_iso_format(state.attacked_at)
    if state.be_present_until is not None:
        item["be_present_until"] = get_as_utc_iso_format(state.be_present_until)
    if state.first_attack_at is not None:
        item["first_attack_at"] = get_as_utc_iso_format(state.first_attack_at)
    if state.sorts_risk_level_date is not None:
        item["sorts_risk_level_date"] = get_as_utc_iso_format(state.sorts_risk_level_date)
    if state.sorts_priority_factor is not None:
        item["sorts_priority_factor"] = state.sorts_priority_factor
    if state.sorts_suggestions is not None:
        item["sorts_suggestions"] = format_toe_lines_sorts_suggestions_item(state.sorts_suggestions)

    return item


def format_toe_lines_item(
    primary_key: PrimaryKey,
    toe_lines: ToeLine,
    gsi_2_key: PrimaryKey,
) -> ToeLinesItem:
    toe_lines_item: ToeLinesItem = {
        "pk": primary_key.partition_key,
        "sk": primary_key.sort_key,
        "filename": toe_lines.filename,
        "group_name": toe_lines.group_name,
        "root_id": toe_lines.root_id,
        "state": format_toe_lines_state_item(toe_lines.state),
        "sk_2": gsi_2_key.sort_key,
        "pk_2": gsi_2_key.partition_key,
    }
    if toe_lines.seen_first_time_by is not None:
        toe_lines_item["seen_first_time_by"] = toe_lines.seen_first_time_by

    return toe_lines_item


def format_toe_lines_enriched_state_item(
    primary_key: PrimaryKey,
    toe_lines: ToeLine,
    gsi_2_key: PrimaryKey,
) -> dict:
    return {
        "pk": primary_key.partition_key,
        "sk": primary_key.sort_key,
        "sk_2": gsi_2_key.sort_key,
        "pk_2": gsi_2_key.partition_key,
        **format_toe_lines_state_item(toe_lines.state),
    }
