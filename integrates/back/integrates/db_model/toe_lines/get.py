from collections.abc import (
    Iterable,
)

from aiodataloader import (
    DataLoader,
)
from aioextensions import (
    collect,
)
from boto3.dynamodb.conditions import (
    Key,
)

from integrates.db_model import (
    HISTORIC_TABLE,
    TABLE,
)
from integrates.db_model.historic_items import ToeLinesStateItem as HistoricToeLinesStateItem
from integrates.db_model.items import ToeLinesItem
from integrates.db_model.toe_lines.constants import (
    GSI_2_FACET,
    GSI_2_HISTORIC_STATE_FACET,
)
from integrates.db_model.toe_lines.types import (
    GroupHistoricToeLinesRequest,
    GroupToeLinesRequest,
    RootToeLinesRequest,
    ToeLine,
    ToeLineEnrichedState,
    ToeLineRequest,
    ToeLinesConnection,
    ToeLinesEnrichedStatesConnection,
)
from integrates.db_model.toe_lines.utils import (
    format_toe_lines,
    format_toe_lines_edge,
    format_toe_lines_enriched_state,
)
from integrates.db_model.utils import (
    format_edges,
)
from integrates.dynamodb import (
    conditions,
    keys,
    operations,
)
from integrates.dynamodb.exceptions import (
    ValidationException,
)
from integrates.dynamodb.types import (
    ItemQueryResponse,
    PageInfo,
)


async def _get_toe_lines(
    requests: Iterable[ToeLineRequest],
) -> list[ToeLine | None]:
    primary_keys = tuple(
        keys.build_key(
            facet=TABLE.facets["toe_lines_metadata"],
            values={
                "group_name": request.group_name,
                "root_id": request.root_id,
                "filename": request.filename,
            },
        )
        for request in requests
    )
    items = await operations.DynamoClient[ToeLinesItem].batch_get_item(
        keys=primary_keys, table=TABLE
    )

    response = {
        ToeLineRequest(
            filename=toe_lines.filename,
            group_name=toe_lines.group_name,
            root_id=toe_lines.root_id,
        ): toe_lines
        for toe_lines in [format_toe_lines(item) for item in items]
    }

    return [response.get(request) for request in requests]


class ToeLinesLoader(DataLoader[ToeLineRequest, ToeLine | None]):
    async def batch_load_fn(self, requests: Iterable[ToeLineRequest]) -> list[ToeLine | None]:
        return await _get_toe_lines(requests)


async def _get_toe_lines_enriched_historic_state_by_group(
    request: GroupHistoricToeLinesRequest,
) -> ToeLinesEnrichedStatesConnection:
    primary_key = keys.build_key(
        facet=GSI_2_HISTORIC_STATE_FACET,
        values={"group_name": request.group_name, "state": "state"},
    )
    index = HISTORIC_TABLE.indexes["gsi_2"]
    key_structure = index.primary_key
    try:
        response = await operations.DynamoClient[HistoricToeLinesStateItem].query(
            after=request.after,
            condition_expression=(
                Key(key_structure.partition_key).eq(primary_key.partition_key)
                & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
            )
            if not request.start_date or not request.end_date
            else (
                Key(key_structure.partition_key).eq(primary_key.partition_key)
                & Key(key_structure.sort_key).between(
                    primary_key.sort_key + "#" + request.start_date.strftime("%Y-%m-%d"),
                    primary_key.sort_key + "#" + request.end_date.strftime("%Y-%m-%d"),
                )
            ),
            facets=(GSI_2_HISTORIC_STATE_FACET,),
            filter_expression=conditions.get_filter_expression(request.filters),
            index=index,
            limit=request.first,
            paginate=request.paginate,
            table=HISTORIC_TABLE,
        )
    except ValidationException:
        response = ItemQueryResponse[HistoricToeLinesStateItem](
            items=tuple(),
            page_info=PageInfo(has_next_page=False, end_cursor=""),
        )

    return ToeLinesEnrichedStatesConnection(
        edges=format_edges(
            index=index,
            formatter=format_toe_lines_enriched_state,
            items=response.items,
            table=HISTORIC_TABLE,
        ),
        page_info=response.page_info,
    )


class GroupToeLinesEnrichedHistoricStateLoader(
    DataLoader[GroupHistoricToeLinesRequest, ToeLinesEnrichedStatesConnection],
):
    async def batch_load_fn(
        self,
        requests: Iterable[GroupHistoricToeLinesRequest],
    ) -> list[ToeLinesEnrichedStatesConnection]:
        return list(
            await collect(
                tuple(
                    map(
                        _get_toe_lines_enriched_historic_state_by_group,
                        requests,
                    ),
                ),
                workers=32,
            ),
        )

    async def load_nodes(
        self,
        request: GroupHistoricToeLinesRequest,
    ) -> list[ToeLineEnrichedState]:
        connection = await self.load(request)
        return [edge.node for edge in connection.edges]


async def _get_toe_lines_by_group(
    request: GroupToeLinesRequest,
) -> ToeLinesConnection:
    if request.be_present is None:
        facet = TABLE.facets["toe_lines_metadata"]
        primary_key = keys.build_key(
            facet=facet,
            values={"group_name": request.group_name},
        )
        index = None
        key_structure = TABLE.primary_key
    else:
        facet = GSI_2_FACET
        primary_key = keys.build_key(
            facet=facet,
            values={
                "group_name": request.group_name,
                "be_present": str(request.be_present).lower(),
            },
        )
        index = TABLE.indexes["gsi_2"]
        key_structure = index.primary_key
    try:
        response = await operations.DynamoClient[ToeLinesItem].query(
            after=request.after,
            condition_expression=(
                Key(key_structure.partition_key).eq(primary_key.partition_key)
                & Key(key_structure.sort_key).begins_with(
                    primary_key.sort_key.replace("#FILENAME", ""),
                )
            ),
            facets=(facet,),
            index=index,
            limit=request.first,
            paginate=request.paginate,
            table=TABLE,
        )
        connection = ToeLinesConnection(
            edges=tuple(format_toe_lines_edge(index, item, TABLE) for item in response.items),
            page_info=response.page_info,
        )
    except ValidationException:
        connection = ToeLinesConnection(
            edges=tuple(),
            page_info=PageInfo(has_next_page=False, end_cursor=""),
        )

    return connection


class GroupToeLinesLoader(DataLoader[GroupToeLinesRequest, ToeLinesConnection]):
    async def batch_load_fn(
        self,
        requests: Iterable[GroupToeLinesRequest],
    ) -> list[ToeLinesConnection]:
        return list(
            await collect(
                tuple(map(_get_toe_lines_by_group, requests)),
                workers=32,
            ),
        )

    async def load_nodes(self, request: GroupToeLinesRequest) -> list[ToeLine]:
        connection = await self.load(request)
        return [edge.node for edge in connection.edges]


async def _get_toe_lines_by_root(
    request: RootToeLinesRequest,
) -> ToeLinesConnection:
    if request.be_present is None:
        facet = TABLE.facets["toe_lines_metadata"]
        primary_key = keys.build_key(
            facet=facet,
            values={
                "group_name": request.group_name,
                "root_id": request.root_id,
            },
        )
        index = None
        key_structure = TABLE.primary_key
    else:
        facet = GSI_2_FACET
        primary_key = keys.build_key(
            facet=facet,
            values={
                "group_name": request.group_name,
                "be_present": str(request.be_present).lower(),
                "root_id": request.root_id,
            },
        )
        index = TABLE.indexes["gsi_2"]
        key_structure = index.primary_key
    try:
        response = await operations.DynamoClient[ToeLinesItem].query(
            after=request.after,
            condition_expression=(
                Key(key_structure.partition_key).eq(primary_key.partition_key)
                & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
            ),
            facets=(facet,),
            index=index,
            limit=request.first,
            paginate=request.paginate,
            table=TABLE,
        )
        connection = ToeLinesConnection(
            edges=tuple(format_toe_lines_edge(index, item, TABLE) for item in response.items),
            page_info=response.page_info,
        )
    except ValidationException:
        connection = ToeLinesConnection(
            edges=tuple(),
            page_info=PageInfo(has_next_page=False, end_cursor=""),
        )

    return connection


class RootToeLinesLoader(DataLoader[RootToeLinesRequest, ToeLinesConnection]):
    async def batch_load_fn(
        self,
        requests: Iterable[RootToeLinesRequest],
    ) -> list[ToeLinesConnection]:
        return list(
            await collect(map(_get_toe_lines_by_root, requests), workers=32),
        )

    async def load_nodes(self, request: RootToeLinesRequest) -> list[ToeLine]:
        connection = await self.load(request)
        return [edge.node for edge in connection.edges]
