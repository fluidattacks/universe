from integrates.db_model import (
    HISTORIC_TABLE,
    TABLE,
)
from integrates.dynamodb.types import (
    Facet,
)

GSI_2_FACET = Facet(
    attrs=TABLE.facets["toe_lines_metadata"].attrs,
    pk_alias="GROUP#group_name",
    sk_alias="LINES#PRESENT#be_present#ROOT#root_id#FILENAME#filename",
)

GSI_2_HISTORIC_STATE_FACET = Facet(
    attrs=HISTORIC_TABLE.facets["toe_lines_state"].attrs,
    pk_alias="GROUP#LINES#ROOT#FILENAME#group_name",
    sk_alias="STATE#state#DATE#iso8601utc",
)
