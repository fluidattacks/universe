from boto3.dynamodb.conditions import Attr

from integrates.audit import AuditEvent, add_audit_event
from integrates.custom_exceptions import InvalidParameter, RepeatedToeLines
from integrates.db_model import HISTORIC_TABLE, TABLE
from integrates.db_model.toe_lines.constants import GSI_2_FACET, GSI_2_HISTORIC_STATE_FACET
from integrates.db_model.toe_lines.types import ToeLine
from integrates.db_model.toe_lines.utils import (
    format_toe_lines_enriched_state_item,
    format_toe_lines_item,
)
from integrates.db_model.utils import get_as_utc_iso_format
from integrates.dynamodb import keys, operations
from integrates.dynamodb.exceptions import ConditionalCheckFailedException


async def add(*, toe_lines: ToeLine) -> None:
    key_structure = TABLE.primary_key
    facet = TABLE.facets["toe_lines_metadata"]
    toe_lines_key = keys.build_key(
        facet=facet,
        values={
            "filename": toe_lines.filename,
            "group_name": toe_lines.group_name,
            "root_id": toe_lines.root_id,
        },
    )
    gsi_2_key = keys.build_key(
        facet=GSI_2_FACET,
        values={
            "be_present": str(toe_lines.state.be_present).lower(),
            "filename": toe_lines.filename,
            "group_name": toe_lines.group_name,
            "root_id": toe_lines.root_id,
        },
    )
    toe_lines_item = format_toe_lines_item(
        primary_key=toe_lines_key,
        toe_lines=toe_lines,
        gsi_2_key=gsi_2_key,
    )
    condition_expression = Attr(key_structure.partition_key).not_exists()
    try:
        await operations.put_item(
            condition_expression=condition_expression,
            facet=facet,
            item=toe_lines_item,
            table=TABLE,
        )
        add_audit_event(
            AuditEvent(
                action="CREATE",
                author=toe_lines.state.modified_by,
                metadata=toe_lines_item,
                object="ToeLine",
                object_id=toe_lines_key.sort_key,
            )
        )
    except ConditionalCheckFailedException as ex:
        raise RepeatedToeLines() from ex


async def add_historic(*, toe_lines: ToeLine) -> None:
    if toe_lines.state.modified_date is None:
        raise InvalidParameter("modified_date")
    key_structure = HISTORIC_TABLE.primary_key
    historic_key = keys.build_key(
        facet=HISTORIC_TABLE.facets["toe_lines_state"],
        values={
            "filename": toe_lines.filename,
            "group_name": toe_lines.group_name,
            "root_id": toe_lines.root_id,
            "iso8601utc": get_as_utc_iso_format(toe_lines.state.modified_date),
        },
    )
    gsi_2_historic_key = keys.build_key(
        facet=GSI_2_HISTORIC_STATE_FACET,
        values={
            "group_name": toe_lines.group_name,
            "iso8601utc": get_as_utc_iso_format(toe_lines.state.modified_date),
            "state": "state",
        },
    )
    historic_item = format_toe_lines_enriched_state_item(
        primary_key=historic_key,
        toe_lines=toe_lines,
        gsi_2_key=gsi_2_historic_key,
    )
    try:
        await operations.put_item(
            condition_expression=Attr(key_structure.sort_key).not_exists(),
            facet=HISTORIC_TABLE.facets["toe_lines_state"],
            item=historic_item,
            table=HISTORIC_TABLE,
        )
    except ConditionalCheckFailedException as ex:
        raise RepeatedToeLines() from ex
