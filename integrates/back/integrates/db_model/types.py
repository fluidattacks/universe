from datetime import (
    datetime,
)
from decimal import (
    Decimal,
)
from typing import (
    Generic,
    NamedTuple,
    NotRequired,
    TypedDict,
    TypeVar,
)

from integrates.db_model.constants import (
    CVSS_V3_DEFAULT,
    CVSS_V4_DEFAULT,
    DEFAULT_INACTIVITY_PERIOD,
    DEFAULT_MAX_SEVERITY,
    DEFAULT_MIN_SEVERITY,
)
from integrates.db_model.enums import (
    AcceptanceStatus,
    TreatmentStatus,
)
from integrates.dynamodb.types import (
    PageInfo,
)

T = TypeVar("T")


class CodeLanguage(NamedTuple):
    language: str
    loc: int


class Policies(NamedTuple):
    modified_date: datetime
    modified_by: str
    days_until_it_breaks: int | None = None
    inactivity_period: int | None = DEFAULT_INACTIVITY_PERIOD
    max_acceptance_days: int | None = None
    max_acceptance_severity: Decimal | None = DEFAULT_MAX_SEVERITY
    max_number_acceptances: int | None = None
    max_number_of_expected_contributors: int | None = None
    min_acceptance_severity: Decimal | None = DEFAULT_MIN_SEVERITY
    min_breaking_severity: Decimal | None = None
    vulnerability_grace_period: int | None = None


class PoliciesToUpdate(NamedTuple):
    days_until_it_breaks: int | None = None
    inactivity_period: int | None = None
    max_acceptance_days: int | None = None
    max_acceptance_severity: Decimal | None = None
    max_number_acceptances: int | None = None
    min_acceptance_severity: Decimal | None = None
    min_breaking_severity: Decimal | None = None
    vulnerability_grace_period: int | None = None


class SeverityScore(NamedTuple):
    base_score: Decimal = Decimal("0.0")
    temporal_score: Decimal = Decimal("0.0")
    cvss_v3: str = CVSS_V3_DEFAULT
    cvssf: Decimal = Decimal("0.0")
    cvssf_v4: Decimal = Decimal("0.0")
    threat_score: Decimal = Decimal("0.0")
    cvss_v4: str = CVSS_V4_DEFAULT


class Treatment(NamedTuple):
    modified_date: datetime
    status: TreatmentStatus
    acceptance_status: AcceptanceStatus | None = None
    accepted_until: datetime | None = None
    justification: str | None = None
    assigned: str | None = None
    modified_by: str | None = None


class TreatmentItem(TypedDict):
    modified_date: str
    status: str
    acceptance_status: NotRequired[str]
    accepted_until: NotRequired[str]
    justification: NotRequired[str]
    assigned: NotRequired[str]
    modified_by: NotRequired[str]


class TreatmentToUpdate(NamedTuple):
    accepted_until: datetime | None
    assigned: str | None
    justification: str
    status: TreatmentStatus
    acceptance_status: AcceptanceStatus | None = None


class Edge(NamedTuple, Generic[T]):
    node: T
    cursor: str


class Connection(NamedTuple, Generic[T]):
    edges: tuple[Edge[T], ...]
    page_info: PageInfo
    total: int | None = None


class VulnerabilityRecordLLM(TypedDict):
    pk: str
    sk: str
    candidate_index: int
    commit: str
    group_name: str
    inputTokens: int
    outputTokens: int
    ranking_score: float
    reason: str
    root_id: str
    snippet_hash: str
    specific: int
    suggested_criteria_code: str
    suggested_finding_title: str
    totalTokens: int
    vulnerability_id_candidate: str
    vulnerable: bool
    where: str


class CodeSnippetLLM(TypedDict):
    pk: str
    sk: str
    commit: str
    end_point: list[int]
    group_name: str
    hash_type: str
    language: str
    root_id: str
    root_nickname: str
    snippet_content: str
    snippet_hash: str
    start_point: list[int]
    where: str
