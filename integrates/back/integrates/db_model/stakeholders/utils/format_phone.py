from integrates.db_model.items import StakeholderPhoneItem
from integrates.db_model.stakeholders.types import (
    StakeholderPhone,
)


def format_phone(item: StakeholderPhoneItem) -> StakeholderPhone:
    return StakeholderPhone(
        calling_country_code=item["calling_country_code"],
        country_code=item["country_code"],
        national_number=item["national_number"],
    )
