from decimal import (
    Decimal,
)

from integrates.db_model.enums import (
    Notification,
)
from integrates.db_model.items import NotificationsPreferencesItem
from integrates.db_model.stakeholders.types import (
    NotificationsParameters,
    NotificationsPreferences,
)


def format_notifications_preferences(
    item: NotificationsPreferencesItem | None,
) -> NotificationsPreferences:
    if not item:
        return NotificationsPreferences(email=[], sms=[], parameters=NotificationsParameters())
    email_preferences: list[str] = []
    sms_preferences: list[str] = []
    parameters_preferences = NotificationsParameters()
    if "email" in item:
        email_preferences = [item for item in item["email"] if item in Notification.__members__]
    if "sms" in item:
        sms_preferences = [item for item in item["sms"] if item in Notification.__members__]
    if "parameters" in item:
        parameters_preferences = NotificationsParameters(
            min_severity=Decimal(item["parameters"]["min_severity"])
        )
    return NotificationsPreferences(
        email=email_preferences,
        sms=sms_preferences,
        parameters=parameters_preferences,
    )
