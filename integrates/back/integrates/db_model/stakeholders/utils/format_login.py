from datetime import (
    datetime,
)

from integrates.db_model.items import StakeholderLoginItem
from integrates.db_model.stakeholders.types import (
    StakeholderLogin,
)


def format_login(item: StakeholderLoginItem | None) -> StakeholderLogin | None:
    if item is not None and all(field in item for field in StakeholderLogin._fields):
        return StakeholderLogin(
            modified_by=item["modified_by"],
            modified_date=datetime.fromisoformat(item["modified_date"]),
            expiration_time=item["expiration_time"],
            browser=item["browser"],
            country_code=item["country_code"],
            device=item["device"],
            ip_address=item["ip_address"],
            provider=item["provider"],
            subject=item["subject"],
        )

    return None
