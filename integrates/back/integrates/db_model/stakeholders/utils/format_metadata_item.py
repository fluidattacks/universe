import simplejson as json

from integrates.class_types.types import (
    Item,
)
from integrates.db_model.stakeholders.types import (
    StakeholderMetadataToUpdate,
)
from integrates.db_model.utils import (
    serialize,
)


def format_metadata_item(metadata: StakeholderMetadataToUpdate) -> Item:
    item: Item = {
        **json.loads(json.dumps(metadata, default=serialize)),
    }

    return {
        key: None if not value and value is not False else value
        for key, value in item.items()
        if value is not None
    }
