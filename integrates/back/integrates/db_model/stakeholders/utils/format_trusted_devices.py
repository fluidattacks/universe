from datetime import (
    datetime,
)

from integrates.db_model.items import TrustedDeviceItem
from integrates.db_model.stakeholders.types import (
    TrustedDevice,
)


def format_trusted_devices(
    items: list[TrustedDeviceItem] | None,
) -> list[TrustedDevice]:
    if not items:
        return []

    trusted_devices = [
        TrustedDevice(
            ip_address=trusted_device.get("ip_address", ""),
            device=trusted_device.get("device", ""),
            location=trusted_device.get("location", ""),
            browser=trusted_device.get("browser", ""),
            otp_token_jti=trusted_device.get("otp_token_jti", ""),
            last_attempt=datetime.fromisoformat(trusted_device["last_attempt"])
            if trusted_device.get("last_attempt")
            else None,
            first_login_date=datetime.fromisoformat(trusted_device["first_login_date"])
            if trusted_device.get("first_login_date")
            else None,
            last_login_date=datetime.fromisoformat(trusted_device["last_login_date"])
            if trusted_device.get("last_login_date")
            else None,
        )
        for trusted_device in items
    ]

    return trusted_devices
