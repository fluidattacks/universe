from integrates.db_model.items import StakeholderSessionTokenItem
from integrates.db_model.stakeholders.types import (
    StakeholderSessionToken,
    StateSessionType,
)


def format_session_token(item: StakeholderSessionTokenItem) -> StakeholderSessionToken:
    return StakeholderSessionToken(
        jti=item["jti"],
        state=StateSessionType[item["state"]],
    )
