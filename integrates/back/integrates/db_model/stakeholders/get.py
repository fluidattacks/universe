from collections.abc import (
    Iterable,
)

from aiodataloader import (
    DataLoader,
)
from boto3.dynamodb.conditions import (
    Key,
)

from integrates.custom_exceptions import (
    ErrorLoadingStakeholders,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.items import StakeholderItem
from integrates.db_model.stakeholders.constants import (
    ALL_STAKEHOLDERS_INDEX_METADATA,
)
from integrates.db_model.stakeholders.types import (
    Stakeholder,
)
from integrates.db_model.stakeholders.utils import (
    format_stakeholder,
)
from integrates.dynamodb import (
    keys,
    operations,
)


async def get_all_stakeholders() -> list[Stakeholder]:
    primary_key = keys.build_key(
        facet=ALL_STAKEHOLDERS_INDEX_METADATA,
        values={"all": "all"},
    )
    index = TABLE.indexes["gsi_2"]
    key_structure = index.primary_key
    response = await operations.DynamoClient[StakeholderItem].query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(ALL_STAKEHOLDERS_INDEX_METADATA,),
        table=TABLE,
        index=index,
    )

    if not response.items:
        raise ErrorLoadingStakeholders()

    return [format_stakeholder(item) for item in response.items]


async def _get_stakeholder_items(
    *,
    emails: list[str],
) -> list[StakeholderItem]:
    primary_keys = tuple(
        keys.build_key(
            facet=TABLE.facets["stakeholder_metadata"],
            values={"email": email},
        )
        for email in emails
    )

    return list(
        await operations.DynamoClient[StakeholderItem].batch_get_item(
            keys=primary_keys, table=TABLE
        )
    )


async def _get_stakeholders_no_fallback(
    *,
    emails: Iterable[str],
) -> list[Stakeholder | None]:
    emails_formatted = [email.lower().strip() for email in emails]
    items = await _get_stakeholder_items(emails=emails_formatted)

    stakeholders: list[Stakeholder | None] = []
    for email in emails_formatted:
        stakeholder = next(
            (
                format_stakeholder(item)
                for item in items
                if (item.get("email") or str(item["pk"]).split("#")[1]) == email
            ),
            None,
        )
        stakeholders.append(stakeholder)
    return stakeholders


class StakeholderLoader(DataLoader[str, Stakeholder | None]):
    async def batch_load_fn(self, emails: Iterable[str]) -> list[Stakeholder | None]:
        return await _get_stakeholders_no_fallback(emails=emails)
