from integrates.custom_exceptions import PortfolioNotFound
from integrates.dataloaders import get_new_context
from integrates.tags.domain import has_access
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GroupAccessFaker,
    GroupFaker,
    OrganizationAccessFaker,
    OrganizationFaker,
    PortfolioFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

ORG_NAME = "orgtest"
ORG_ID = "ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db"
PORTFOLIO_ID = "f66-4a2d9748ad6-543"
ADMIN = "admin@gmail.com"
USER = "user@gmail.com"
GROUP_NAME = "test-group"


@parametrize(
    args=["email", "expected_access"],
    cases=[
        [ADMIN, True],
        [USER, False],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            organization_access=[OrganizationAccessFaker(organization_id=ORG_ID, email=ADMIN)],
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            group_access=[GroupAccessFaker(group_name=GROUP_NAME, email=ADMIN)],
            portfolios=[
                PortfolioFaker(id=PORTFOLIO_ID, organization_name=ORG_NAME, groups={GROUP_NAME})
            ],
            stakeholders=[
                StakeholderFaker(email=ADMIN, role="admin"),
                StakeholderFaker(email=USER),
            ],
        )
    )
)
async def test_has_access_portfolio(email: str, expected_access: bool) -> None:
    subject = f"{ORG_ID}PORTFOLIO#{PORTFOLIO_ID}"
    assert (
        await has_access(
            loaders=get_new_context(),
            email=email,
            subject=subject,
        )
        == expected_access
    )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            organization_access=[OrganizationAccessFaker(organization_id=ORG_ID, email=ADMIN)],
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            group_access=[GroupAccessFaker(group_name=GROUP_NAME, email=ADMIN)],
            portfolios=[PortfolioFaker(id=PORTFOLIO_ID, organization_name=ORG_NAME)],
            stakeholders=[StakeholderFaker(email=ADMIN)],
        )
    )
)
async def test_has_access_portfolio_not_found() -> None:
    unexisting_portfolio_id = "f66-4a2d9748ad6"
    subject = f"{ORG_ID}PORTFOLIO#{unexisting_portfolio_id}"
    with raises(PortfolioNotFound):
        await has_access(
            loaders=get_new_context(),
            email=ADMIN,
            subject=subject,
        )
